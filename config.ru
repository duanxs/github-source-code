# frozen_string_literal: true

# Because Unicorn is such a brilliant piece of software, it wraps older,
# non-Rack versions of Rails in a Rack handler. That way Unicorn
# itself can target Rack and not have to worry about monkey patching
# Rails' dispatcher.
#
# This means we can do the same, and even more.
#
# Starting Rackhub locally:
#
# Unicorn:
#   $ unicorn_rails -c config/unicorn.rb

# Tweak environment
ENV["RACK_ENV"] ||= ENV["RAILS_ENV"]

require File.expand_path("../lib/mysql_failsafe", __FILE__)

# Load our Rails app and Rack wrapper
MysqlFailsafe.failsafe("require config/environment") do
  require File.expand_path("../config/environment", __FILE__)
end
require "permission_cache"
require "experiment_cache"
require "failbot/middleware"
require "flipper/middleware/setup_env"
require "flipper/middleware/memoizer"
require "flipper/middleware/overrider"
require "secure_headers"
require "rack/octotracer"
require "rack/process_utilization"
require "rack/warmup"
require "rack/content_type_cleaner"
require "rack/malformed_request_handler"
require "rack/request_id"
require "rack/request_logger"
require "rack/server_id"

require "rack/common_logger"

module Rack
  class CommonLogger
    def log(env, status, header, began_at)
      # Silence CommonLogger
    end
  end
end

# This should cover the ENTIRE rack stack
# This is used to remove socket queue duration from the nginx
# upstream_response_time measurement.
use Rack::Runtime, "rack"

# Tell Failbot not to include sensitive parameters from uncaught exceptions.
# This needs to come *before* Failbot::Rescuer.
use Rack::Config do |env|
  env["filter_words"] = GitHub.filtered_params
end

use Failbot::Rescuer, :app => "github"

# This middleware resets `GitHub.context`, which stores global context for each
# request. This should happen early in the chain to avoid wiping out any context
# set by other middleware.
use GitHub::ContextMiddleware

use SecureHeaders::Middleware
use GitHub::BufferedStatsMiddleware
use GitHub::InvalidParametersMiddleware
use Rack::OctoTracer,
    tracer: GitHub.tracer,
    enable_if: ->(env) { GitHub.tracing_enabled? },
    include_urls: false
use GitHub::LineProfilerMiddleware, Rails.root if GitHub::LineProfilerMiddleware.enabled?
use GitHub::FlamegraphMiddleware if GitHub::FlamegraphMiddleware.enabled?
use GitHub::AllocationTracerMiddleware, Rails.root, Gem.dir if GitHub::AllocationTracerMiddleware.enabled?

# Records the rack env from the last request so that the kill trap
# introspection can report it to failbot.
use GitHub::RecordRackEnvMiddleware if defined? Unicorn::WorkerTimeout

# Records time each process spends actively processing a request vs. being idle
# waiting on a request.
use Rack::ProcessUtilization, GitHub.host_name, GitHub.current_sha[0, 7],
  :stats => GitHub.stats, :dogstats => GitHub.dogstats

# Records stats about each request. Keep this near the top of this file as
# anything above it won't count toward our request timing metrics.  Depends
# on `Rack::ProcessUtilization` for GC info
use GitHub::Middleware::Stats

# Enable and reset the local per-request cache
use GitHub::Cache::ResetMiddleware

use Platform::Middleware::BatchLoading

# Enable Flipper feature memoization
flipper_block = lambda { GitHub.flipper }
use Flipper::Middleware::SetupEnv, flipper_block
use Flipper::Middleware::Memoizer

# Enable turning features on/off via ?_features= for staff users
staff_users_only = lambda { |env| GitHub::StaffOnlyCookie.read(Rack::Request.new(env).cookies) }
use Flipper::Middleware::Overrider, flipper_block, staff_users_only

# Enable blocking based on IP address
use GitHub::BannedIpsMiddleware

# Enables switching runtime environments from the query string.
#
# Runtime environments supported:
# * github.com (default)
# * enterprise
#
# Set the runtime in the query string:
#   _runtime=enterprise
#
# Switch back:
#   _runtime=production
use GitHub::Runtime::Switcher if Rails.env == "development"

##
# Main GitHub Rails app
#
#   github.com, api.github.com

if GitHub.private_mode_enabled?
  map "/sessions/_auth_request_endpoint" do
    run GitHub::PrivateModeMiddleware
  end
end

map "/" do
  use GitHub::RequestTimerMiddleware

  # Raise a GitHub::TimeoutMiddleware::RequestTimeout error after the
  # configured timeout.
  if !Rails.development? && !Rails.test?
    use GitHub::TimeoutMiddleware
  end

  GitHub::Enterprise::Middleware.mount(self) if GitHub.enterprise?

  # GitHub app middleware pipeline
  use Rack::MalformedRequestHandler
  use Rack::ContentTypeCleaner

  use Rack::ServerId

  # Enable Rack middleware for capturing (or generating) request id's
  use Rack::RequestId

  # Enable Rack middleware for logging requests
  use Rack::RequestLogger

  # Flush accumulated octolytics events at the end of a request
  use GitHub::FlushOctolyticsEventsMiddleware
  # Ensures that batching is enabled and logs the request.
  use GitHub::HydroMiddleware
  use Search::Aggregator::Middleware

  use PermissionCache
  use ExperimentCache

  if GitHub.request_limiting_enabled?
    use GitHub::Limiters::Renderer
  end

  use GitHub::Routers::Api

  use GitHub::ServerTimingMiddleware

  # Serve the Braintree and Zuora webhook endpoints with the above middleware
  if GitHub.billing_enabled?
    map "/billing/stripe/platform" do
      run GitHub::Stripe::WebhooksApp
    end

    map "/billing/stripe/connect" do
      run GitHub::Stripe::WebhooksApp
    end

    map "/billing/docusign/webhook" do
      run GitHub::Docusign::WebhooksApp
    end

    map "/billing/zuora" do
      run GitHub::Zuora::WebhooksApp.app
    end
  end

  if GitHub.request_limiting_enabled?

    # Limiters that actually limit:
    use GitHub::Limiters::AppMiddleware,
      # always limit requests to this path:
      GitHub::Limiters::ByPath.new("/site/limittown"),
      # 20 anonymous req/min by ip and path averaged over two minutes
      GitHub::Limiters::CountAnonymousByIPAndPath.new(limit: 40, ttl: 120)

    # And limiters that only log their results:
    use GitHub::Limiters::AppMiddleware::Disabled,
      # 350 anonymous cpu seconds per ip per minute averaged over two minutes
      GitHub::Limiters::ElapsedAnonymousByIP.new(limit: 700_000, ttl: 120)
  end

  # Gracefully handles, reports ActionController::UnknownHttpMethod exceptions
  if !Rails.development? && !Rails.test?
    use GitHub::Middleware::UnknownHttpMethod
  end

  run GitHub::Application
end

if ENV["RACK_ENV"] != "development" || ENV["PRELOAD"]
  warmup do |app|
    MysqlFailsafe.failsafe("WARMUP /status") do
      client = Rack::MockRequest.new(app)
      if GitHub.rails_6_0?
        # Rails 6.0 produces deprecation warnings which
        # we need to silence to prevent output during boot
        ActiveSupport::Deprecation.silence do
          client.get("/status")
        end
      else
        # Deprecations should not be silenced for other Rails versions
        # so that we catch warnings for Rails versions > 6.0
        client.get("/status")
      end
    end
    GC.start
  end
end
