#!/bin/sh -e
# Usage: script/gack [--gems] [<options>] <pattern>
# Ack GitHub codebase for <pattern>, passing other ack <options> if given. When
# the --gems option is given, also search in vendor/gems.
set -e
PATH=$(dirname $0):$PATH

# show help with no arguments
test $# -eq 0 &&
exec ack --help

# setup ACK_PAGER environment variables if they're not already set
: ${ACK_PAGER:="less -FirSwX"}
: ${ACK_PAGER_COLOR:="less -FirSwX"}
export ACK_PAGER ACK_PAGER_COLOR

# paths searched by ack. results are returned in the order listed here
ACK_PATHS="
    Gemfile
    README.md
    Rakefile
    config.ru

    config
    app
    jobs
    lib
    script
    db
    public

    test/test_helper.rb
    test/jobs
    test/mailers
    test/models
    test/lib
    test/fixtures
    test/integration
    test/helpers
    test/fast
    test/test_helpers
    test/view_models

    vendor/internal-gems
    vendor/gitignore
"

# only add vendor/gems if --gems given on command line
if [ "$1" = '--gems' ]; then
    ACK_PATHS="$ACK_PATHS vendor/gems"
    shift
fi

TYPES="
--type-set scss=.scss
--type-set coffeescript=.coffee
"

# ack w/ args to this script followed by well known paths
exec "$(dirname $0)/ack" \
    --ignore-dir=key_blacklists \
    $TYPES "$@" $ACK_PATHS
