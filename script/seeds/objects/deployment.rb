# frozen_string_literal: true

# Do not require anything here. If you need something else, require it in the method that needs it.
# This makes sure the boot time of our seeds stays low.

module Seeds
  module Objects
    class Deployment
      class << self
        def create_all_variations!(repository:, head_sha:, integration:)
          # Create abandoned deployment
          create(
            repository: repository,
            head_sha: head_sha,
            integration: integration,
            environment: "review-lab",
            transient_environment: true,
            production_environment: false
          )

          # Success -> Inactive = Destroyed
          review_lab_deployment = create(
            repository: repository,
            head_sha: head_sha,
            integration: integration,
            environment: "review-lab",
            transient_environment: true,
            production_environment: false
          )
          create_status(review_lab_deployment, "success", integration: integration)
          create_status(review_lab_deployment, "inactive", integration: integration)

          lab_deployment = create(
            repository: repository,
            head_sha: head_sha,
            integration: integration,
            environment: "lab",
            transient_environment: false,
            production_environment: false
          )
          create_status(lab_deployment, "success", integration: integration)
          create_status(lab_deployment, "inactive", integration: integration)

          # With one status each, but iterate all possible statuses
          ::DeploymentStatus::STATES.each do |state|
            loop_deployment = create(repository: repository, head_sha: head_sha, integration: integration)
            create_status(loop_deployment, state, integration: integration)
          end
        end

        def create(repository:, head_sha:, integration: nil, pull_request: nil,
                   environment: "production", transient_environment: false, production_environment: true)
          raise Objects::ActionFailed, "integration or pull_request must be provided" unless integration || pull_request
          options = {}.tap do |opts|
            opts["sha"]                    = head_sha
            opts["creator"]                = integration.present? ? integration.bot : pull_request.user
            opts["repository"]             = repository
            opts["transient_environment"]  = transient_environment
            opts["production_environment"] = production_environment
            opts["environment"]            = environment
          end
          ::Deployment.create!(options)
        end

        def create_status(deployment, state, integration: nil)
          environment_url = state == "success" ? "https://example.com" : nil
          options = {}.tap do |opts|
            opts["deployment"]      = deployment
            opts["state"]           = state
            opts["creator"]         = integration.present? ? integration.bot : deployment.creator
            opts["log_url"]         = "https://heaven.githubapp.com/1234/logs"
            opts["environment_url"] = environment_url
          end
          ::DeploymentStatus.create!(options)
        end
      end
    end
  end
end
