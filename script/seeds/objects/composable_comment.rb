# frozen_string_literal: true

# Do not require anything here. If you need something else, require it in the method that needs it.
# This makes sure the boot time of our seeds stays low.

module Seeds
  module Objects
    class ComposableComment
      def self.create(integration:, issue:, body: default_body)
        comment = ::ComposableComment.new
        comment.integration = integration
        comment.issue = issue
        comment.components = body["components"]

        unless comment.save
          raise Objects::CreateFailed, "Failed to create comment: #{comment.errors.full_messages.to_sentence}"
        end

        comment
      end

      def self.default_body
        JSON.parse({
          "components": [
            {
              "type": "markdown",
              "text": "**I am the best, most composable comment**",
            },
            {
              "type": "interactive",
              "external_id": "internal-def456",
              "elements": [
                {
                  "type": "button",
                  "value": "apply",
                  "text": "Apply plan",
                },
              ],
            },
          ],
        }.to_json)
      end
    end
  end
end
