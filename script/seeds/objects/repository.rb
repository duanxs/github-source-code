# frozen_string_literal: true

# Do not require anything here. If you need something else, require it in the method that needs it.
# This makes sure the boot time of our seeds stays low.

module Seeds
  module Objects
    class Repository
      attr_reader :repo

      class << self
        def hub_repo
          # We want hub to be owned by github the org, so make that first
          Seeds::Objects::Organization.github(admin: Objects::User.monalisa)
          # Don't make this a class instance var or tests will become flaky
          # as state is not reset between tests
          # This is ok as we short circuit below and return the object if it already exists
          create(owner_name: "github", repo_name: "hub", setup_master: true)
        end

        def create_with_nwo(nwo:, setup_master:, is_public: false)
          if (repo = ::Repository.nwo(nwo))
            repo.setup_git_repository # Ensure this is setup
            return repo
          end

          owner, name = nwo.split("/", 2)
          create(owner_name: owner, repo_name: name, setup_master: setup_master, is_public: is_public)
        end

        def create(owner_name:, repo_name: nil, setup_master:, is_public: false)
          owner = ::Organization.find_by_login(owner_name) || ::User.find_by_login(owner_name) ||
            Seeds::Objects::Organization.create(login: owner_name, admin: Objects::User.monalisa)
          repo_name ||= random_repo_name_for(owner)

          repo = ::Repository.nwo("#{owner_name}/#{repo_name}")
          unless repo
            random_boolean = [true, false].sample
            desc = random_boolean ? "#{Faker::Movies::StarWars.quote}" : ""
            repo = ::Repository.create(
              owner: owner,
              name: repo_name,
              description: desc,
              public: is_public,
              created_by_user_id: owner.id,
            )
            raise Seeds::Objects::CreateFailed, repo.errors.full_messages.to_sentence unless repo.persisted?
          end

          repo.setup_git_repository

          # ensure that any existing repo has the required privacy
          repo.public = is_public
          repo.save!

          # if required, check the master branch has been created
          branch(repo, "master") if setup_master && !owner.bot?

          repo
        end

        def setup_branch(committer:, repo:, branch:)
          committer = committer.organization? ? committer.members.first : committer
          Seeds::Objects::Commit.create(committer: committer, repo: repo, branch_name: branch)
          repo.heads.find(branch)
        end

        def branch(repo, branch)
          repo.heads.find(branch) || setup_branch(committer: repo.owner, repo: repo, branch: branch)
        end

        def restore_premade_repo(location_premade_git:,  owner_name:, repo_name: nil, is_public: false)

          if location_premade_git.include?("://") || location_premade_git.start_with?("git@")
            require "posix/spawn"

            temp_folder = File.join(Dir.tmpdir, SecureRandom.hex(10))
            child = POSIX::Spawn::Child.new("git", "clone", location_premade_git, temp_folder)
            unless child.status.success?
              raise "git clone failed: #{child.err}"
            end
            git_folder = File.join(temp_folder, ".git")
          else
            git_folder = location_premade_git
          end

          repo = create(owner_name: owner_name, repo_name: repo_name, setup_master: false, is_public: is_public)

          repo.dgit_all_routes.map(&:path).each do |dest|
            FileUtils.rm_rf(dest)
            FileUtils.mkdir_p File.dirname(dest)
            FileUtils.cp_r(git_folder, dest)
          end

          FileUtils.rm_rf(temp_folder) if temp_folder != nil

          reader = GitHub::DGit::Routing.hosts_for_network(repo.network.id).first

          GitHub::DGit::Maintenance.recompute_checksums(repo, reader, is_wiki: false, force_dgit_init: true)

          repo
        end

        private

        def random_repo_name_for(owner)
          count = owner.repositories.count
          "#{Faker::Hipster.word}-#{count}"
        end
      end
    end
  end
end
