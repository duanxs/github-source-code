# frozen_string_literal: true

# Do not require anything here. If you need something else, require it in the method that needs it.
# This makes sure the boot time of our seeds stays low.

module Seeds
  module Objects
    class FeatureFlag
      def self.toggle(action:, feature_flag:, actor_name: nil, diagnostic_io: STDOUT)
        feature = ::FlipperFeature.find_by(name: feature_flag)

        if feature.nil?
          diagnostic_io.puts "Creating #{feature_flag}"
          feature = ::FlipperFeature.create(name: feature_flag)
        end

        if actor_name.nil?
          GitHub.flipper[feature_flag].method(action).call
          diagnostic_io.puts "#{feature_flag} globally #{action}d"
          return feature
        end

        actor = if actor_name.start_with?("business:")
          ::Business.find_by(slug: actor_name.gsub("business:", ""))
        else
          GitHub::Resources.find_by_url(actor_name)
        end

        if actor.nil?
          raise Objects::ActionFailed, "Couldn't find the actor #{actor_name}"
        end

        begin
          feature.method(action).call(actor)
          diagnostic_io.puts "#{feature_flag} #{action}d for #{actor_name}"
          feature
        rescue ::Flipper::GateNotFound
          raise Objects::ActionFailed, "Something went wrong when trying to #{action} #{feature_flag} for #{actor_name}"
        end
      end
    end
  end
end
