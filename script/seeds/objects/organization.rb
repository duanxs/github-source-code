# frozen_string_literal: true

# Do not require anything here. If you need something else, require it in the method that needs it.
# This makes sure the boot time of our seeds stays low.

module Seeds
  module Objects
    class Organization
      def self.github(admin: Objects::User.monalisa)
        org = create(login: "github", admin: admin, plan: "business_plus")
        GitHub.trusted_apps_owner_id = nil # De-memoize the ID of the github organization
        org
      end

      def self.create(login:, admin:, plan: "business_plus")
        org = ::Organization.find_by(login: login)
        return org if org

        org = ::Organization.create(
          login: login,
          billing_email: "#{login}@github.test.com",
          plan: plan,
          seats: 1000,
          admin: admin
        )

        unless org.valid? && org.persisted?
          raise Objects::CreateFailed, "Organization failed to create: #{org.errors.full_messages.to_sentence}\n#{org.inspect}"
        end

        team = Seeds::Objects::Team.create!(org: org, name: "Employees")
        team.add_member(admin)
        GitHub::FeatureFlag.team_cache.clear
        org
      end
    end
  end
end
