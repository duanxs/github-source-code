# frozen_string_literal: true

# Do not require anything here. If you need something else, require it in the method that needs it.
# This makes sure the boot time of our seeds stays low.

module Seeds
  module Objects
    class Issue
      def self.create(repo:, actor:, title: "My Issue", body: " #{Faker::Lorem.sentence}")
        issue = ::Issue.create(repository: repo, user: actor, title: title, body: body)
        raise Objects::CreateFailed, issue.errors.full_messages.to_sentence unless issue.valid?
        issue
      end
    end
  end
end
