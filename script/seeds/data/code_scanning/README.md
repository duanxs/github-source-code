# Example repos for local code scanning development

These are example repos for use with `Seeds::Runner::CodeScanning` – they will be restored using `Seeds::Objects::Repository.restore_premade_repo` before further processing that populates a local Turbo-Scan instance with the appropriate data for local testing and development of code scanning features.

Each `.git` folder should contain an additional README that describes what features/characteristics the repo is intended to test.

## How is this repo processed?

The analysis of the code present in this repo is contained in SARIF files, and these are checked into the repository alongside the code in question. The following logic is used to extract the SARIF from the repo and create entries in the turboscan database.

#### The default branch – `^master$`

Iterate over all the commits in the history of the master branch (oldest to newest), and for each commit take any SARIF files present in the tree, concatenate all the `runs` they contain into a single SARIF file, and submit this to `turboscan` with the required metadata. If it is desired that specific commits _not_ have an analysis available, then the SARIF files should just be removed from repo in the specific commit.

#### Protected branches – `^protected_.*$`

Any branch with a prefix of `protected_` in the name will be treated as protected, and the `code_scanning` runner will then apply the appropriate settings when provisioning the repository. When processing the script we iterate over all the commits from the branching point through to the head of the branch, again processing all SARIF files found in each commit tree.

#### Pull request branches `^pr_.*$`

All branches prefixed `pr_` are categorised as intended for a pull request. The script will open a PR against `master`, along with the specific call(s) required to turboscan in order to replicate PR analysis.

- _TODO_ - generalise to permit targeting other branches?
- _TODO_ make the necessary changes so it actually _shows_ PR analysis

#### All other branches

Branches apart from those above are assumed to be there for some other illustrative purpose, and again each commit since the branching point is checked for SARIF files, and if found they're submitted to turboscan with the relevant commit and ref metadata. No protection or opened PRs are applied for such branches.