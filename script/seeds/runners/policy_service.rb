# frozen_string_literal: true

require_relative "../runner"
# Do not require anything else here. If you need something for your runner, put that in `self.run`.
# This makes sure the boot time of our seeds stays low.

module Seeds
  class Runner
    class PolicyService < Seeds::Runner
      def self.help
        <<~HELP
        Creates a .github-private repo with policies that represent different compliance
        and evaluation states

        - enables policies feature flag on owner (user or organization)
        - creates 30 repos to represent policy outcomes
        - shells out to the policy-service repo to create the corresponding seed policy data there

        HELP
      end

      def self.run(options = {})
        puts "\n"

        @mona = Seeds::Objects::User.monalisa
        @owner = Seeds::Objects::Organization.github

        policies = FlipperFeature.create(name: "policies")

        Feature.create(
          public_name: "Policies",
          slug: "policies",
          flipper_feature_id: policies.id,
          description: "Enables policies",
          feedback_category: "other",
          enrolled_by_default: true,
        )

        @owner.enable_feature(:policies)
        @owner.teams.each { |team| team.enable_feature(:policies) }
        @mona.enable_feature(:policies)

        print "Creating/updating .github-private repository for policy service..."
        @repo = Seeds::Objects::Repository.create(
          setup_master: true,
          owner_name: @owner,
          repo_name: ".github-private",
          is_public: false,
        )
        puts " ✅\n\n"

        commit_oid = commit_policy_files

        outcome_repo_ids = (1..30).map { |i| create_org_repo(name: "test_repo_#{i}", is_public: true) }
        outcome_team_ids = (1..30).map { |i| create_org_team(name: "test_team_#{i}") }

        puts "\n"
        puts "Your new .github-private repository is now ready with policies committed:\n"
        puts "    http://#{GitHub.host_name}/#{@repo.nwo}"
        puts ""
        puts "Organization id is #{@owner.id}"
        puts "Repository ids are: #{outcome_repo_ids}\n"
        puts "Team ids are: #{outcome_team_ids}\n"
        puts "\n"

        command = "cd ..; cd policy-service; script/db-seed -commitOid #{commit_oid} -orgId #{@owner.id} -repoId #{@repo.id} -outcomeRepoIDs '#{outcome_repo_ids}' -outcomeTeamIDs '#{outcome_team_ids}'"
        seed_policy_service(command)
      end

      def self.commit_policy_files
        @repo.refs.find("master").append_commit({ message: "Add policy files", committer: @mona }, @mona) do |files|
          Dir["./script/seeds/data/policies/*.yaml"].each do |policy_file|
            files.add("policies/#{File.basename(policy_file)}", File.read(policy_file))
          end
        end

        @repo.refs.find("master").commit.sha
      end

      def self.create_org_repo(name: nil, is_public: false)
        repo = Seeds::Objects::Repository.create(
          owner_name: @owner,
          repo_name: name,
          setup_master: true,
          is_public: is_public,
        )

        repo.id
      end

      def self.create_org_team(name: nil)
        team = Seeds::Objects::Team.create!(
          org: @owner,
          name: name,
          description: "This is a team",
        )
        team.add_member(@mona)

        team.id
      end

      def self.seed_policy_service(command)
        puts command+"\n"
        system(command)
      end
    end
  end
end
