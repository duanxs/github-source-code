# frozen_string_literal: true

require_relative "../runner"
# Do not require anything else here. If you need something for your runner, put that in `self.run`.
# This makes sure the boot time of our seeds stays low.

module Seeds
  class Runner
    class Actions < Seeds::Runner
      class << self
        def help
          <<~EOF
          Seed workflows with workflow runs in github/hub repository

          Seeding
          - Enables Actions/workflow feature flags
          - Creates the Actions GitHub app
          - Creates workflows in the github/hub repository
          - Creates workflow runs with different outcomes for those workflows
          - Creates Actions survey
          EOF
        end

        def run(options = { workflow_run_count: 20 })
          # Delay loading these at it makes the script app slow because it boots github
          require_relative "../../create-launch-github-app"
          require_relative "../../helper.rb"

          enable_feature_flags

          mona = Seeds::Objects::User.monalisa
          hub_repo = Seeds::Objects::Repository.hub_repo
          app = create_and_install_github_app repo: hub_repo

          puts "Creating workflows from template"
          workflows.each do |w|
            w[:workflow_file_path] = create_workflow_from_template actor: mona, repo: hub_repo, template_id: w[:template_id]
          end

          puts "Creating #{options[:workflow_run_count]} runs for each workflow"
          options[:workflow_run_count].times do |i|
            workflows.each do |w|
              workflow_run = create_workflow_run(
                repo: hub_repo,
                app: app,
                name: w[:name],
                actor: mona,
                workflow_file_path: w[:workflow_file_path],
                # Create at least one successful run
                status: i < 1 ? ::CheckRun.statuses[:completed] : nil,
                conclusion: i < 1 ? ::CheckRun.conclusions[:success] : nil
              )
            end
          end

          puts "Creating Actions survey"
          create_survey
        end

        def workflows
          @workflows ||= [
            {
              name: "CI",
              template_id: "blank"
            },
            { name: "Node CI",
              template_id: "nodejs"
            }
          ]
        end

        private

        def enable_feature_flags
          puts "Enable feature flags for Actions"
          [
            :actions_workflow_run_webhook,
          ].each do |feature_flag|
            Seeds::Objects::FeatureFlag.toggle(action: "enable", feature_flag: feature_flag)
          end

          # Register and disable these feature flags
          [
            :actions_disable_scheduled_workflows,
            :actions_disable_status_postbacks,
          ].each do |feature_flag|
            Seeds::Objects::FeatureFlag.toggle(action: "disable", feature_flag: feature_flag)
          end
        end

        def create_and_install_github_app(repo:)
          name = GitHub.launch_github_app_name
          puts "Creating GitHub Actions app with name '#{name}'"


          app = Integration.find_by(name: name)
          if !app.present?
            app = CreateLaunchGitHubApp.new.create_app
          else
            puts "App already exists."
          end

          puts "Installing app on repository"
          installer = repo.owner.organization? ? repo.owner.admins.first : repo.owner
          owner_installation = app.installations_on(repo.owner).first
          if owner_installation.present?
            IntegrationInstallation::Editor.append(
              owner_installation,
              repositories: [repo],
              editor: installer,
            )
          else
            app.install_on(
              repo.owner,
              repositories: [repo],
              installer: installer,
            )
          end

          app
        end

        def create_workflow_from_template(actor:, repo:, template_id:)
          puts "Adding workflow from template #{template_id} to the repo"

          workflow_file_path = ".github/workflows/#{template_id}.yaml"

          Seeds::Objects::Commit.create(
            repo: repo,
            message: "Add workflow #{template_id}",
            committer: actor,
            files: {workflow_file_path => ::Actions::WorkflowTemplates.get_yaml_by_id(template_id, repo, actor)}
          )

          workflow_file_path
        end

        def create_workflow_run(repo:, app:, name:, actor:, workflow_file_path:, event: "push", status: nil, conclusion: nil)
          # Touch file for a commit
          branch_name = "master"
          commit = Seeds::Objects::Commit.create(repo: repo, committer: actor, branch_name: branch_name)

          if event == "push"
            push = Push.create!(
              before: commit.first_parent_oid,
              after: commit.oid,
              ref: "refs/heads/#{branch_name}",
              repository_id: repo.id,
              pusher: actor
            )
          end

          status = status || ::CheckRun.statuses.values.sample
          completed = status == ::CheckRun.statuses[:completed]
          conclusion = conclusion || ::CheckRun.conclusions.values.sample
          success = conclusion == ::CheckRun.conclusions[:success]

          check_suite = Seeds::Objects::CheckSuite.create(
            repo: repo,
            app: app,
            push: push,
            head_branch: branch_name,
            head_sha: push.after,
            attrs: {
              name: name,
              status: status,
              conclusion: conclusion,
              workflow_file_path: workflow_file_path,
              event: event,
              check_runs_rerunnable: false # Individual GitHub Actions check runs are not re-runnable
            })

          # create check_run with steps
          rand(1..4).times.each do |run|
            started_at = Time.now.utc - (rand(1..20).minutes)
            # If the check suite is completed, mark check run as completed, too, otherwise use random status.
            check_run_status = completed ? ::CheckRun.statuses[:completed] : ::CheckRun.statuses.values.sample
            check_run_completed = check_run_status == ::CheckRun.statuses[:completed]
            check_run_conclusion = check_run_completed ? (success ? ::CheckRun.conclusions[:success] : ::CheckRun.conclusions.values.sample) : nil
            check_run = check_suite.check_runs.create!(
              name:  SecureRandom.uuid,
              display_name: "Job #{run}",
              status: check_run_status,
              completed_at: check_run_completed ? Time.now.utc : nil,
              conclusion: check_run_conclusion,
              started_at: started_at,
              number: run
            )

            rand(0..3).times.each do |step|
              check_step_status = check_step_status ? ::CheckRun.statuses[:completed] : ::CheckRun.statuses.values.sample
              check_step_completed = check_step_status == ::CheckRun.statuses[:completed]
              check_step_conclusion = check_step_completed ? (success ? ::CheckRun.conclusions[:success] : ::CheckRun.conclusions.values.sample) : nil
              check_run.steps.build(
                name: "Step #{step}",
                number: step,
                started_at: Time.new(2019, 5, 3, 12, 0, 0).utc,
                completed_at: check_step_completed ? Time.new(2019, 5, 3, 12, 1, 0).utc : nil,
                completed_log_url: "https://logs.github.com/some-unique-slug-step1",
                completed_log_lines: rand(10..30),
                status: check_step_status,
                conclusion: check_step_conclusion
              )
            end

            check_suite.save!

            if completed
              check_suite.update(completed_at: Time.now.utc)

              # Create some usage in billing
              check_suite.check_runs.each do |check_run|
                Billing::Actions::UsageLineItemCreator.create(
                  check_run_id: check_run.id,
                  job_id: SecureRandom.uuid,
                  actor_id: actor.id,
                  repository_id: check_suite.repository.id,
                  duration_in_milliseconds: check_run.seconds_to_completion.in_milliseconds,
                  start_time: check_run.started_at,
                  end_time: check_run.completed_at,
                  owner_id: check_suite.repository.owner.id,
                  job_runtime_environment: "UBUNTU",
                  **::Billing::MeteredBillingBillableOwnerDesignator.attributes_for(check_suite.repository.owner))
              end
            end
          end
        end

        def create_survey
          require "github/transitions/20200227183944_create_actions_survey.rb"

          unless Survey.github_sql.run("SELECT id FROM surveys WHERE slug = :slug", slug: GitHub::Transitions::CreateActionsSurvey::SURVEY_SLUG).value?
            puts "Creating Actions survey"

            transition = GitHub::Transitions::CreateActionsSurvey.new(dry_run: false)
            transition.perform
          else
            puts "Actions survey already exists."
          end
        end
      end
    end
  end
end
