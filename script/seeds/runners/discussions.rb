# frozen_string_literal: true

require_relative "../runner"
# Do not require anything else here. If you need something for your runner, put that in `self.run`.
# This makes sure the boot time of our seeds stays low.

module Seeds
  class Runner
    class Discussions < Seeds::Runner
      def self.help
        <<~HELP
        Seed Discussions into new or preexisting repositories.

        If a repository nwo is provided, ensures that it exists, creating it if not.
        Enables the :discussions_for_repo_users feature flag on the repository and the
        per-repo discussions setting. Creates a set of verified users to use as
        participants. Generates a set of discussions and comments.

        Count parameters may be specified as exact counts (-count variants) or as ranges
        (-min and -max variants). If a range is given, the count will be chosen uniformly
        from that range each time it's used.
        HELP
      end

      class Distribution
        def initialize(min:, max:)
          @min = min
          @max = max
        end

        def next
          if @min == @max
            @min
          else
            @min + rand(@max - @min)
          end
        end

        def self.from_options(options, prefix:, default_min: 0, default_max:)
          count, min, max = ["#{prefix}-count", "#{prefix}-min", "#{prefix}-max"].map { |opt| options[opt] }

          if count && (min || max)
            $stderr.puts "You may only specify one of --#{prefix}-count or --#{prefix}-min and -max."
            nil
          elsif count
            new(min: count, max: count)
          else
            min ||= default_min
            max ||= default_max
            new(min: min, max: max)
          end
        end
      end

      def self.run(options = {})
        comment_dist = Distribution.from_options(options, prefix: "comment", default_min: 0, default_max: 10)
        thread_dist = Distribution.from_options(options, prefix: "thread", default_min: 0, default_max: 5)
        return 1 if [comment_dist, thread_dist].any? { |dist| dist.nil? }

        # Discussion participants.
        participant_count = options["participant-count"]
        create_participants = options["create-participants"]
        puts "Locating #{participant_count} users to use as discussion participants."
        participants = create_participants ? [] : User.has_verified_email.last(participant_count)
        (participants.size + 1).upto(participant_count) do |n|
          participants << Seeds::Objects::User.create(login: "#{Faker::Name.first_name.downcase}#{n}").tap do |user|
            puts "... Created participant @#{user.login}."
          end
        end

        # Repository.
        puts "Setting up repository."
        repo_nwo = options["repo"] || "monalisa/#{Faker::Color.color_name}"
        repo = Seeds::Objects::Repository.create_with_nwo(nwo: repo_nwo, setup_master: true, is_public: true)
        GitHub.flipper[:discussions_for_repo_users].enable(repo)
        repo.update!(has_discussions: true)
        puts "... Set up repository #{repo.nwo}."

        # Discussions.
        categories = repo.discussion_categories
        puts "Generating #{options["discussion-count"]} discussions."
        options["discussion-count"].times do
          discussion = ::Discussion.create!(
            repository: repo,
            user: participants.sample,
            title: Faker::Lorem.question,
            body: Faker::Lorem.paragraphs(number: 1 + rand(3)).join("\n\n"),
            category: categories.sample,
          )

          comment_dist.next.times do
            comment = ::DiscussionComment.create!(
              discussion: discussion,
              user: participants.sample,
              body: Faker::Lorem.paragraphs(number: 1 + rand(2)).join("\n\n"),
            )

            thread_dist.next.times do
              ::DiscussionComment.create!(
                discussion: discussion,
                parent_comment: comment,
                user: participants.sample,
                body: Faker::Lorem.paragraphs(number: 1 + rand(2)).join("\n\n"),
              )
            end
          end

          print "."
          $stdout.flush
        end

        puts "Complete."
      end
    end
  end
end
