# frozen_string_literal: true

require_relative "../runner"
# Do not require anything else here. If you need something for your runner, put that in `self.run`.
# This makes sure the boot time of our seeds stays low.

module Seeds
  class Runner
    class Deployments < Seeds::Runner
      class << self
        def help
          "Create all variations of a deployment and it's status"
        end

        def run(options = {})
          repo = if options[:nwo].present?
            Seeds::Objects::Repository.create_with_nwo(nwo: options[:nwo], setup_master: true, is_public: false)
          else
            Seeds::Objects::Repository.hub_repo
          end

          app_name = options[:integration_slug].present? ? options[:integration_slug] : "Deployment Bot"
          integration = Seeds::Objects::Integration.create(repo: repo, owner: repo.owner, app_name: app_name)
          user = repo.owner.is_a?(Organization) ? repo.owner.admins.first : repo.owner

          pr = if options[:pull_request_number].present?
            PullRequest.with_number_and_repo(options[:pull_request_number], repo)
          else
            Seeds::Objects::PullRequest.create(repo: repo, committer: user)
          end

          if pr.nil?
            raise Runner::RunnerError, "pull request could not be found/created with pr number #{options[:pull_request_number]} on repo #{repo.nwo}"
          end

          Seeds::Objects::Deployment.create_all_variations!(repository: repo, head_sha: pr.head_sha, integration: integration)
        end
      end
    end
  end
end
