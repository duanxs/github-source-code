# frozen_string_literal: true

require_relative "../runner"
# Do not require anything else here. If you need something for your runner, put that in `self.run`.
# This makes sure the boot time of our seeds stays low.

module Seeds
  class Runner
    class ActionsRepos < Seeds::Runner
      ACTIONS_API_URL = "https://api.github.com/orgs/actions/repos"

      def self.help
        <<~HELP
        Copies over a number of repositories from the actions org and instantiates repo actions Marketplace Listings for them
        HELP
      end

      class << self
        def run(options = {})
          user = User.find_by(login: options[:login]) || Seeds::Objects::User.monalisa
          Seeds::Objects::User.two_factor_enable!(user: user)

          puts "Generating PAT for #{user.login}"
          token, access = Seeds::Objects::User.create_pat(user: user, scopes: "repo,admin:repo_hook,delete_repo")

          puts "Making sure there's an integrators agreement in place"
          agreement = Marketplace::Agreement.latest_for_integrators || Seeds::Objects::MarketplaceAgreement.create(signatory_type: :integrator)

          org = Seeds::Objects::Organization.create(login: "actions", admin: user)
          response = Net::HTTP.get_response(URI(ACTIONS_API_URL))
          repos = JSON.parse(response.body).reject { |r| r["archived"] }
          repos = repos.first(options[:count]) if options[:count]
          msg = "Found #{repos.size} repos from the actions org"
          msg += " of the requested #{options[:count]}"
          puts msg

          Dir.mktmpdir do |dir|
            Dir.chdir(dir) do
              run_cmd("ssh-keygen -b 4096 -t rsa -f sshkey -q -N ''")
              run_cmd("eval \"$(ssh-agent -s)\" && ssh-add -K sshkey")
              run_cmd("ssh-keygen -R github.localhost")
              run_cmd("ssh-keygen -R localhost")
              repos.each { |r| puts "=" * 30; copy_repo(user, token, agreement, r) }
            ensure
              run_cmd("eval \"$(ssh-agent -s)\" && ssh-add -d sshkey")
            end
          end
        ensure
          puts "=" * 30
          puts "Deleting access token, if needed"
          access.destroy if access
        end

        private

        # Wrapping this makes it easy to test
        def run_cmd(cmd)
          system(cmd)
        end

        def copy_repo(user, token, agreement, actions_repo)
          puts "Cloning Actions Repo #{actions_repo['clone_url']}"
          repo = Seeds::Objects::Repository.create_with_nwo(nwo: actions_repo["full_name"], setup_master: false, is_public: true)
          FileUtils.mkdir(actions_repo["name"]) # Make sure the path isnt empty, useful for tests
          run_cmd("git clone -q #{actions_repo['clone_url']} #{actions_repo["name"]}")
          puts "Pushing Repo to Local"
          Dir.chdir(actions_repo["name"]) do
            unless run_cmd("git push -f \"#{repo.ssh_url}\"")
              raise RunnerError, "Push failed to complete"
            end
            repo.reset_git_cache
            create_repo_action_and_publish_to_marketplace(repo, user, agreement)
          end
        end

        def create_repo_action_and_publish_to_marketplace(repo, user, agreement)
          # Mock out some data to help tests hit these code paths.
          # This is needed since we dont actually clone the repo
          if Rails.env.test?
            FileUtils.touch("action.yml")
            Seeds::Objects::Commit.create(
              committer: user,
              repo: repo,
              branch_name: "master",
              files: { "action.yml" => "" },
              message: "Add action.yml"
            )
            repo.reset_git_cache
          end

          action_path = if File.exist?("action.yml")
            "action.yml"
          elsif File.exist?("Dockerfile")
            "Dockerfile"
          end
          return unless action_path

          puts "Creating RepositoryAction for #{action_path}"
          action = ::RepositoryAction.find_by(repository: repo, path: action_path) || ::RepositoryAction.create!(name: repo.name, repository: repo, path: action_path)
          action.update_column(:security_email, user.emails.first.email) if action.security_email.blank?

          puts "Signing agreement for that action"
          if action.owned_by_org?
            unless action.org_has_signed_integrator_agreement?(org: action.owner, agreement: agreement)
              action.sign_developer_agreement(actor: user, agreement: agreement, org: action.owner)
            end
          else
            unless action.has_signed_integrator_agreement?(user: user, agreement: agreement)
              action.sign_developer_agreement(actor: user, agreement: agreement)
            end
          end

          puts "Creating a release and publishing on marketplace"
          release = Release.create!(
            repository: action.repository,
            author: user,
            state: :published,
            name: "#{repo.name} Release ##{repo.releases.count + 1}",
            tag_name: "v#{repo.releases.count + 1}.0.0",
          )
          action.repository_action_releases << RepositoryActionRelease.new(release: release, published_on_marketplace: true)
          action.listed!
          action.save!
        end
      end
    end
  end
end
