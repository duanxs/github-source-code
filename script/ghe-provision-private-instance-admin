#!/usr/bin/env safe-ruby
# frozen_string_literal: true
#
#/ Usage: ghe-provision-private-instance-admin [options]
#/
#/ Provision the first administrator of a GitHub Private Instance.
#/
#/ - Creates a new user with the given username and email.
#/ - Promotes the created user as a site administrator.
#/ - Sends the user a welcome email, prompting them to set their password.
#/
#/ Prerequisites:
#/
#/ - This must be be a GitHub Private Instance environment.
#/ - SMTP must already be successfully configured on the instance.
#/ - There must be no existing site administrators on the instance.
#/
#/ OPTIONS:
#/  -h              Show this message.
#/  -y              Bypass the confirmation prompt.
#/  -u USERNAME     The username for the new administrator.
#/  -e EMAIL        The email belonging to the new administrator.

require "optparse"

def help!
  exec "grep ^#/<'#{__FILE__}'|cut -c4-"
end

bypass_warning = false
username       = nil
email          = nil

if ARGV.empty?
  help!
end

ARGV.options do |opts|
  opts.on("-u", "--username=val")     { |val| username = val }
  opts.on("-e", "--email=val")        { |val| email = val }
  opts.on("-y")                       { bypass_warning = true }
  opts.on_tail("-h", "--help")        { help! }
  opts.parse!
end

if ARGV.any?
  STDERR.puts "Unknown arguments: #{ARGV.join(", ")}\n"
  help!
end

if username.nil? && email.nil?
  help!
end

unless bypass_warning
  printf "Do you want to provision the first site administrator with username #{username} and email #{email}? (y/N) "
  exit(0) if STDIN.gets.chomp.downcase != "y"
end

puts "Provisioning the #{username} administrator"
# This part's slow, so wait until we need it:
require File.expand_path("../../config/environment", __FILE__)

begin
  GitHub::PrivateInstanceAdminProvisioner.new(username: username, email: email).run
rescue GitHub::PrivateInstanceAdminProvisioner::Error => error
  abort error.message
end

# vim: set syntax=ruby:
