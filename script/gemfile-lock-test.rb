#!/usr/bin/env ruby
# frozen_string_literal: true
# Usage: gemfile-lock-test.rb
# Run bin/bundle install to verify Gemfile.lock and Gemfile_next.lock are in
# sync

require "open3"

# Require BUNDLER_PERSISTENT_GEMS_AFTER_CLEAN so that we can omit them from
# the list of gems when we check for differences between installs
require_relative "../config/bundler_persistent_gems"

def puts_if_not_silenced(*args)
  puts *args unless ENV["SILENCE_BUNDLER_OUTPUT"] == "1"
end

class BundleInstallComparison
  attr_reader :differences_current, :differences_next

  def initialize
    perform_bundle_install

    installed_gems_rails_current = get_bundle_gem_list({"RAILS_NEXT" => "0"})
    installed_gems_rails_next  = get_bundle_gem_list({"RAILS_NEXT" => "1"})

    @differences_current = differences_between_installs(installed_gems_rails_current, installed_gems_rails_next)
    @differences_next = differences_between_installs(installed_gems_rails_next, installed_gems_rails_current)
  end

  def perform_bundle_install
    puts_if_not_silenced "Running bin/bundle install..."
    env = {}
    # `licensed` causes issues when running bundle install here. We skip
    # license caching to avoid that problem since we're not testing licenses.
    env["SKIP_BUNDLE_LICENSE_CACHE"] = "1"

    # `github-rails-next` build sets RAILS_NEXT=1. This can cause CI to fail
    # because Rails Current is bundled as Rails Next. We set RAILS_NEXT to `0`
    # while we run bundle install. `script/bundle` sets it back to `1` as needed.
    env["RAILS_NEXT"] = "0"

    bundle_install_output, _ = Open3.capture2(env, "bin/bundle install")
    puts_if_not_silenced bundle_install_output
  end

  def get_bundle_gem_list(env)
    bundled_gem_result, _ = Open3.capture2(env, "bin/bundle list")
    bundled_gem_result.split(/\n/)
  end

  def in_persisted_list?(gem)
    gem_name = gem.gsub(/\(.*\Z/, "").strip
    BUNDLER_PERSISTENT_GEMS_AFTER_CLEAN.include?(gem_name)
  end

  def differences_between_installs(gem_list, other_gem_list)
    differences = gem_list - other_gem_list
    bundler_output_list_item_prefix = /\A\s*\*\s*/
    differences = differences.keep_if { |gem| gem =~ bundler_output_list_item_prefix }
    differences = differences.collect { |gem| gem.gsub(bundler_output_list_item_prefix, "") }
    differences = differences.delete_if { |gem| in_persisted_list?(gem) }
    differences
  end

  def has_differences?
    return true if differences_current.any? || differences_next.any?
    false
  end
end

comparison = BundleInstallComparison.new

if comparison.has_differences?
  puts_if_not_silenced "Gemfile.lock and Gemfile_next.lock are out of sync"
  puts "Rails Current:"
  puts comparison.differences_current.collect { |gem_item| " * #{gem_item}" }.join("\n")
  puts ""
  puts "Rails Next:"
  puts comparison.differences_next.collect { |gem_item| " * #{gem_item}" }.join("\n")
  exit 1
else
  puts "Gemfile.lock and Gemfile_next.lock are in sync"
  exit 0
end
