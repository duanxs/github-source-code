#!/usr/bin/env ruby
# frozen_string_literal: true
#/ Usage: create-sponsors.rb <sponsorable-login>
#/ Creates two user sponsors and two corporate sponsors for a sponsorable:
#/  - with verified email, credit card, and monthly plan
#/  - with verified email, credit card, and yearly plan
#/ If sponsorable login is not specified, it just creates users who are ready to sponsor.
#/ OPTIONS:
#/   --help      Show this message
require_relative "../config/environment"
require "faker"

if ARGV.include?("--help")
  system "grep ^#/ '#{__FILE__}' |cut -c4-"
  exit 1
end

unless Rails.env.development?
  abort "This can only be run in development"
end

class CreateSponsors
  # Public: Create sponsors.
  #
  # sponsorable (optional) - The String login of the sponsorable to create sponsors for.
  #
  # Returns an Array[User].
  def call(sponsorable: nil)
    if sponsorable.present?
      possible_sponsorables = SponsorsListing.with_approved_state.pluck(:sponsorable_id)
      sponsorable           = User.where(id: possible_sponsorables).find_by(login: sponsorable)

      raise "Unable to find sponsorable user with login #{sponsorable}" unless sponsorable.present?
    end

    ActiveRecord::Base.transaction do
      puts "Creating sponsors..."
      monthly_user = create_user!(plan_duration: User::MONTHLY_PLAN)
      yearly_user = create_user!(plan_duration: User::YEARLY_PLAN)
      monthly_org = create_org!(user: monthly_user)
      yearly_org  = create_org!(user: yearly_user)

      puts "Creating payment methods..."
      sponsors = [monthly_user, yearly_user, monthly_org, yearly_org]
      sponsors.each { |sponsor| set_credit_card_for!(sponsor) }

      if sponsorable.present?
        puts "Creating sponsorships..."
        sponsors.each do |sponsor|
          create_sponsorship!(sponsor: sponsor, sponsorable: sponsorable)
        end
      end

      sponsors.each { |sponsor| print_sponsor_details(sponsor) }
      if sponsorable.present?
        puts "------"
        puts "Users and organizations are sponsoring #{sponsorable.login}."
        puts "Manage sponsorship:  #{sponsorship_url_for(sponsorable)}"
        puts "------"
      end

      sponsors
    end
  end

  private

  # Private: Create a user with a verified email.
  #
  # plan_duration - The String duration of the user's billing plan (one of User::PLAN_DURATIONS).
  #
  # Returns a User.
  def create_user!(plan_duration:)
    user = User.create!(
      login: "sp-#{Faker::Internet.unique.user_name(separators: %w[-])}",
      email: Faker::Internet.email,
      plan_duration: plan_duration,
      password: GitHub.default_password,
      password_confirmation: GitHub.default_password,
    )

    user.emails.last.verify!

    puts "Created #{user.login}."

    user
  end

  # Private: Create a user with a verified email.
  #
  # user - The organization's admin
  # plan_duration - The String duration of the user's billing plan (one of User::PLAN_DURATIONS).
  #
  # Returns an Organization.
  def create_org!(user:)
    org = Organization.create(
      login: "csp-#{Faker::Games::Pokemon.name.downcase}",
      billing_email: Faker::Internet.email,
      admin: user,
      billing_type: "card",
      plan: GitHub::Plan.free_with_addons,
      plan_duration: user.plan_duration,
    )

    # Populate direct_admin_ids in an org
    org.add_admin(user)

    GitHub.flipper[:corporate_sponsors_credit_card].enable(org.admin)

    puts "Created #{org.login}."

    org
  end

  # Private: Sets up a credit card for a user.
  #
  # user - The user to set up a credit card for.
  #
  # Returns a User.
  def set_credit_card_for!(user)
    customer = Customer.create!(
      name: user.login,
      braintree_customer_id: "123abc",
      zuora_account_id: SecureRandom.hex,
      zuora_account_number: SecureRandom.hex(12),
    )

    payment_method = PaymentMethod.create!(
      customer: customer,
      user: user,
      payment_processor_customer_id: "66170868",
      payment_token: "abc123",
      unique_number_identifier: "def456",
      truncated_number: "411111******1111",
      expiration_month: "04",
      expiration_year: Time.now.year + 2,
      card_type: "Visa",
      region: "California",
      postal_code: "12345",
      country: "USA",
      payment_processor_type: PaymentMethod.zuora_processor_slug,
    )

    customer.payment_method = payment_method
    customer.save!

    CustomerAccount.create!(
      user: user,
      customer: customer,
    )

    user
  end

  # Private: Creates a sponsorship for a user.
  #
  # sponsor - The User or Organization that is sponsoring.
  # sponsorable - The User that is being sponsored.
  #
  # Returns nothing.
  def create_sponsorship!(sponsor:, sponsorable:)
    listing = sponsorable.published_sponsorable_listing
    tier = listing.sponsors_tiers.first

    Sponsors::CreateSponsorship.call(
      tier: tier,
      sponsor: sponsor,
      sponsorable: sponsorable,
      quantity: 1,
      viewer: sponsor,
      privacy_level: "public",
      email_opt_in: true,
    )

    puts "Created sponsorship for #{sponsor.login} -> #{sponsorable.login}"
  end

  # Private: The login URL that redirects to the GitHub Sponsors profile for a user.
  #
  # sponsor - The User or Organization to get the path for.
  #
  # Returns a String.
  def login_url_for(sponsor)
    login = sponsor.user? ? sponsor.login : sponsor.admin.login

    Rails.application.routes.url_helpers.login_url(
      login: login,
      protocol: GitHub.scheme,
      host: GitHub.host_name,
    )
  end

  # Private: Print sponsor's login.
  #
  # sponsor - Sponsor User or Organization.
  #
  # No return.
  def print_sponsor_details(sponsor)
    puts "------"
    puts "Created #{sponsor.type.downcase}: #{sponsor.login}"
    puts "Login here: #{login_url_for(sponsor)}"
  end

  # Private: The sponsorships page url for a user.
  #
  # user - The User to get the path for.
  #
  # Returns a String.
  def sponsorship_url_for(user)
    Rails.application.routes.url_helpers.sponsorable_url(
      id: user.login,
      protocol: GitHub.scheme,
      host: GitHub.host_name,
    )
  end
end

if __FILE__==$0
  CreateSponsors.new.call(sponsorable: ARGV.shift)
end
