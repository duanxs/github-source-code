#!/usr/bin/env safe-ruby
# frozen_string_literal: true

require_relative "../../test/test_helpers/github_test/stats"
require_relative "../../test/test_helpers/github_test/parallel_reporter"

output = File.read(ARGV[0])

reporter = GitHubTest::ParallelReporter.new($stderr)
reporter.generate_final_report(output)

failures = reporter.failures
failing_suites = failures.map { |failure| failure["suite"] }.uniq

$stderr.puts ""
if failures.any?
  $stderr.puts "==> Found failing tests in #{failing_suites.length} suites: #{failing_suites.join(" ")}"
  GitHubTest::ParallelReporter.dump_failures($stderr, failures)
elsif reporter.build_succeeded_without_failures?
  $stderr.puts "==> All tests passed"
else
  $stderr.puts "==> Build Failed"
end

exit_code = reporter.build_succeeded_without_failures? ? 0 : 1

exit! exit_code
