#!/bin/bash
# usage: script/extract-node-modules
# Extracts tar'd npm packages from vendor/npm/*.tar into node_modules/ and
# builds any native extensions. This script is automatically invoked from
# script/bootstrap and npm-bundle. You shouldn't need to run it directly.

set -eu

# the RAILS_ROOT directory
cd $(dirname "$0")/..

# Check for script having crashed mid-run and start over
[ ! -e ./node_modules/.extracting ] || rm -rf ./node_modules
pristine=
[ -e ./node_modules ] || pristine=true
mkdir -p ./node_modules
touch ./node_modules/.extracting

shopt -s nullglob
num_affected=0
cached_packages=":"

extract_updated() {
  if [ $# -eq 0 ]; then
    echo "error: no packages specified!" >&2
    return 1
  fi
  # squelch "tar: Ignoring unknown extended header keyword ..." warnings on Linux
  local os="$(uname -s | tr '[:upper:]' '[:lower:]')"
  local tarflags=""
  if [ "$os" = "linux" ]; then
    tarflags="--warning=no-unknown-keyword"
  fi

  local package package_id package_name package_path
  for package; do
    package_id="${package#./vendor/npm/}"
    package_name="${package_id%@*}"
    cached_packages="${cached_packages}${package_name}:"
    package_path="node_modules/${package_name}"
    if [ ! "$package_path" -nt "$package" ]; then
      echo "extracting ${package_id}"
      : $((num_affected++))
      rm -rf "$package_path"
      tar $tarflags -xzmf "$package" -C "node_modules"
    fi
  done
}

cleanup_outdated() {
  local package_dir package_name
  for package_dir; do
    if [[ "${package_dir##*/}" == @* ]]; then
      cleanup_outdated "$package_dir"/*
      continue
    fi
    package_name="${package_dir#./node_modules/}"
    if [[ $cached_packages != *:"$package_name":* ]]; then
      echo "deleting $package_name"
      : $((num_affected++))
      rm -rf "$package_dir"
    fi
  done
}

regenerate_binstubs() {
  rm -rf ./node_modules/.bin
  mkdir ./node_modules/.bin

  # we can't use `./bin/node` because it hasn't been set up yet
  set -o pipefail
  ./vendor/node/node -e '
    const {dirname,resolve} = require("path")
    process.argv.slice(1).forEach(packageJSON => {
      for (const [name,path] of Object.entries(require(packageJSON).bin || {})) console.log(name, resolve(dirname(packageJSON), path))
    })
  ' "$@" | while read binstub binpath; do
    ln -sf "${binpath}" "./node_modules/.bin/${binstub}"
  done
}

filter_tarballs() {
  if [ -n "${GITHUB_CI:-}" ] && git rev-parse -q --git-dir &>/dev/null; then
    # in CI, make sure to only extract tarballs that are in the git tree
    git ls-files "$@" | sed 's!^!./!'
  else
    echo "$@"
  fi
}

extract_updated $(filter_tarballs ./vendor/npm/{*,*/*}.tgz)
if [ -z "$pristine" ]; then
  cleanup_outdated ./node_modules/*
fi

if [ $(ls -d node_modules/* 2>/dev/null | wc -l) -le 1 ]; then
  echo "error: no modules extracted" >&2
  exit 1
fi

binstubs=( ./node_modules/.bin/* )
if [ "$num_affected" -gt 0 ] || [ "${#binstubs[*]}" -eq 0 ]; then
  echo "regenerating node binstubs"
  regenerate_binstubs ./node_modules/{*,@*/*}/package.json
fi

rm -f ./node_modules/.checksum
rm ./node_modules/.extracting
