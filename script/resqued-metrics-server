#!/usr/bin/env ruby
# frozen_string_literal: true

require "optparse"
require "prometheus_exporter"
require "prometheus_exporter/server"

options = {poll_interval: 1}
optparse = OptionParser.new do |opts|
  opts.banner = "Usage: resqued-metrics-server [-D] --http ADDRESS"

  opts.on("--http ADDRESS", "HTTP address:port where metrics will be served") do |v|
    if v =~ /(.*):(\d+)\z/
      options[:address] = {bind: $1, port: $2.to_i}
    else
      puts "Expected address to be ip:port"
      puts opts
      exit 1
    end
  end

  # Tests want a smaller poll interval
  opts.on("--poll-interval N") do |v|
    options[:poll_interval] = v.to_f
  end

  opts.on("-D", "--[no-]daemon", "Daemonize on startup") do |v|
    options[:daemonize] = v
  end
end

optparse.parse!

if options[:address].nil?
  puts optparse
  exit 1
end

if options[:daemonize]
  if fork
    # grandparent
    exit!
  end
  if fork
    # parent
    Process.setsid
    exit!
  end
  STDIN.reopen "/dev/null"
  STDOUT.reopen "/dev/null", "a"
  STDERR.reopen "/dev/null", "a"
end

trap(:INT) { exit 0 }
trap(:TERM) { exit 0 }

server = PrometheusExporter::Server::WebServer.new(**options[:address])
server.start

workers_gauge = PrometheusExporter::Metric::Gauge.new("aqueduct_workers_count", "number of aqueduct workers")
idle_workers_gauge = PrometheusExporter::Metric::Gauge.new("aqueduct_workers_idle_count", "number of aqueduct workers waiting for a job")
active_workers_gauge = PrometheusExporter::Metric::Gauge.new("aqueduct_workers_active_count", "number of aqueduct workers working on a job")

server.collector.register_metric(workers_gauge)
server.collector.register_metric(idle_workers_gauge)
server.collector.register_metric(active_workers_gauge)

AQUEDUCT_WORKER_RE = /\Aaqueduct.+(Processing|Waiting|Quiesced|Paused)/

loop do
  idle_workers = 0
  active_workers = 0
  `ps -e --no-headers -o cmd`.lines.each do |line|
    if line =~ AQUEDUCT_WORKER_RE
      if $1 == "Processing"
        active_workers += 1
      else
        idle_workers += 1
      end
    end
  end
  workers_gauge.observe(idle_workers + active_workers)
  idle_workers_gauge.observe(idle_workers)
  active_workers_gauge.observe(active_workers)

  GC.start # otherwise `ps`'s output sticks around and this process's RSS may try to eat up a big chunk of what's available.

  sleep(options[:poll_interval])
end
