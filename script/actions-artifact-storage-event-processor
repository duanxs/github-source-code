#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative "../config/environment"
processor = GitHub::StreamProcessors::Actions::ArtifactStorageEventProcessor.new
consumer = GitHub.hydro_consumer(processor.options)

ActionsArtifactStorageEventProcessorError = Class.new(StandardError)

begin
  GitHub::Logger.log(fn: processor.class.name, at: "start")
  GitHub.hydro_client.run(processor, consumer: consumer, shutdown_signals: [:INT, :TERM])
  GitHub::Logger.log(fn: processor.class.name, at: "finish")
rescue => e
  GitHub::Logger.log_exception({ fn: processor.class.name, error: "failed to run stream processor" }, e)
  Failbot.report(ActionsArtifactStorageEventProcessorError.new)
  raise
ensure
  GitHub::Logger.log(fn: processor.class.name, at: "exit", last_error: $!.class.name)
end
