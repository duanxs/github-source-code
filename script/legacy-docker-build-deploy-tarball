#!/bin/bash
# Usage legacy-docker-build-deploy-tarball <branch name> [--upload]

set -xe

branch_name="$1"
if [ -z "$branch_name" ]
then
  echo >&2 "Usage: script/legacy-docker-build-deploy-tarball <branch name> [--upload]"
  exit 1
fi
shift

upload_build=0
if [ "$1" == "--upload" ]
then
  upload_build=1
fi

# Initialize bt.sh for breaking down where we spend time during the build
source "$(dirname "$0")/../vendor/bt/bt.sh"
bt_init
bt_start "entire build"

# Determine the current machine architecture
if [ $(which lsb_release) ]
then
  os="$(lsb_release -sc)"
else
  os="$(uname -sr)"
fi
build_arch="$(echo $os | tr -d ' ' | tr '[:upper:]' '[:lower:]')-$(uname -m)"

workspace="$(pwd)"

# Global cache is outside the docker container, copies to and from this are
# slow
global_cache=/cache/$build_arch
mkdir -p $global_cache

# Local tmp is inside the docker container and per-run state, don't use tmp
# because that's workspace local and on OS X its slow to share files between
# the host and container.
local_tmp=/tmp
mkdir -p $local_tmp

# Output tmp is where to write the result.
output_dir=/output

# If SSH credentials are available from the host, copy them.
if [ -d /secrets/ssh ]; then
  mkdir -p $HOME/.ssh
  chmod 0700 $HOME/.ssh
  cp /secrets/ssh/* $HOME/.ssh
  chmod 0600 $HOME/.ssh/*
fi

# Determine the build_ref commit
commit_sha="$(git rev-parse HEAD)"

# Export a working directory into /build
build_dir="/build"
mkdir -p $build_dir


echo "%%%FOLD {Exporting $commit_sha into $build_dir}%%%"
echo
bt_start "git checkout index"
git checkout-index --all --prefix="$build_dir"/
bt_end "git checkout index"
echo "%%%END FOLD%%%"


# Create `SHA1` with the commit sha
sha1_path="${build_dir}/SHA1"
echo $commit_sha >$sha1_path
# Create `BRANCH_NAME` with the ref name for the staff bar
echo "$branch_name" >"${build_dir}/BRANCH_NAME"

# Bootstrap and bundle deploy the copy
cd $build_dir


# We have no vendor ruby, either restore from the cache or copy the source from
# the workspace, build and cache it.
echo "%%%FOLD {Building Ruby}%%%"
echo
bt_start "ruby build"

ruby_cache=$global_cache/ruby
mkdir -p $ruby_cache

ruby_build_cache="$ruby_cache/v2:$(config/ruby-version).tar"
ruby_build=vendor/ruby

if [ -f "$ruby_build_cache" ]
then
  echo "%%%FOLD {Restoring Ruby from $ruby_build_cache}%%%"
  echo
  bt_start "ruby build cache"
  mkdir -p $ruby_build
  tar -xf $ruby_build_cache -C $ruby_build
  bt_end "ruby build cache"
  echo "%%%END FOLD%%%"
else
  echo "%%%FOLD {Building Ruby from source}%%%"
  echo
  bt_start "ruby build source"
  mkdir -p vendor/ruby
  cp -R $workspace/vendor/ruby/src vendor/ruby
  script/build-ruby
  rm -r vendor/ruby/src vendor/ruby/ruby-build.log
  bt_end "ruby build source"
  echo "%%%END FOLD%%%"

  echo "%%%FOLD {Caching Ruby into $ruby_build_cache}%%%"
  echo
  bt_start "caching ruby build"
  tar -cf $ruby_build_cache --transform="s,^$ruby_build/,," "$ruby_build/"*
  bt_end "caching ruby build"
  echo "%%%END FOLD%%%"

  echo "%%%FOLD {Pruning Ruby build cache}%%%"
  echo
  bt_start "pruning ruby build cache"
  pushd $ruby_cache
  ls -xt . | tail -n +5 | xargs -d\\n -t rm -f
  popd
  bt_end "pruning ruby build cache"
  echo "%%%END FOLD%%%"
fi
ruby_version="$($ruby_build/current/bin/ruby -e 'puts RUBY_VERSION')"

bt_end "ruby build"
echo "%%%END FOLD%%%"


echo "%%%FOLD {Building Node}%%%"
echo
bt_start "node build"

node_cache=$global_cache/node
mkdir -p $node_cache

node_build_cache="$node_cache/v1:$(< config/node-version).tar"
node_build=vendor/node

if [ -f $node_build_cache ]
then
  echo "%%%FOLD {Restoring Node from $node_build_cache}%%%"
  echo
  mkdir -p $node_build
  tar -xf $node_build_cache -C $node_build
  echo "%%%END FOLD%%%"
else
  echo "%%%FOLD {Building Node from source}%%%"
  echo
  bt_start "node build source"
  script/build-node
  rm vendor/node/node-build.log
  bt_end "node build source"
  echo "%%%END FOLD%%%"

  echo "%%%FOLD {Caching Node into $node_build_cache}%%%"
  echo
  bt_start "caching node build"
  tar -cf $node_build_cache --transform="s,^$node_build/,," "$node_build/"*
  bt_end "caching node build"
  echo "%%%END FOLD%%%"

  echo "%%%FOLD {Pruning Node build cache}%%%"
  echo
  bt_start "pruning node build cache"
  pushd $node_cache
  ls -xt . | tail -n +5 | xargs -d\\n -t rm -f
  popd
  bt_end "pruning node build cache"
  echo "%%%END FOLD%%%"
fi

bt_end "node build"
echo "%%%END FOLD%%%"


# speed up compilation on multiple CPU/core machines
if [ $(which nproc) ]
then
  export CPUCOUNT=$(nproc)
elif [ $(which sysctl) ]
then
  export CPUCOUNT=$(sysctl -n hw.ncpu)
fi

if [ "$CPUCOUNT" -gt 1 ]
then
  export MAKEFLAGS="-j$CPUCOUNT $MAKEFLAGS"
fi

echo "%%%FOLD {Running Bootstrap}%%%"
echo
bt_start "running bootstrap"

# Look for a cached gems archive
archive_gems=0
gem_cache=$global_cache/gem_archives
mkdir -p $gem_cache
gems_hash="$(sha1sum Gemfile.lock | tr -s ' ' | cut -d' ' -f1)"
gem_build_cache="$gem_cache/${ruby_version}-${gems_hash}.tar"
gem_build=vendor/gems/$ruby_version
if [ -f "$gem_build_cache" ]
then
  echo "%%%FOLD {Restoring Gems from $gem_build_cache}%%%"
  echo
  bt_start "unarchiving gem build cache"
  mkdir -p $gem_build
  tar -xf $gem_build_cache -C $gem_build
  bt_end "unarchiving gem build cache"
  echo "%%%END FOLD%%%"
else
  archive_gems=1
fi

echo "%%%FOLD {Running \`script/bootstrap\` in $build_dir}%%%"
echo
bt_start "script/bootstrap"
BUNDLE_WITHOUT="development:test:enterprise" RAILS_ENV=production script/bootstrap --local --deployment
bt_end "script/bootstrap"
echo "%%%END FOLD%%%"

# Cache gem archive if we installed this set for the first time
if [ $archive_gems -eq 1 ]
then
  echo "%%%FOLD {Caching Gems into $gem_build_cache}%%%"
  echo
  bt_start "archiving gem build cache"
  tar -cf $gem_build_cache --transform="s,^$gem_build/,," "$gem_build/"*
  bt_end "archiving gem build cache"
  echo "%%%END FOLD%%%"

  echo "%%%FOLD {Pruning Gems build cache}%%%"
  echo
  bt_start "pruning gem build cache"
  pushd $gem_cache
  ls -xt . | tail -n +20 | xargs -d\\n -t rm -f
  popd
  bt_end "pruning gem build cache"
  echo "%%%END FOLD%%%"
fi

bt_end "running bootstrap"
echo "%%%END FOLD%%%"


echo "%%%FOLD {Building Assets}%%%"
echo
bt_start "building assets"
echo "Running \`precompile-assets\` in $build_dir" >&2
RAILS_ENV=production bin/npm run precompile-assets

ls -l public/assets/* | while read _ _ _ _ size _ _ _ file; do
  if [ $size -le 2 ] && [[ "$file" != */assets.lock ]]; then
    echo "Error: $file was generated empty." >&2
    echo "Please ping @github/web-systems if the problem persists." >&2
    exit 1
  fi
done

bt_end "building assets"
echo "%%%END FOLD%%%"


##
# Clean unused directories before creating the tarball

echo "%%%FOLD {Cleaning unused files}%%%"
echo
bt_start "cleaning unused files"

echo "Cleaning unused files and directories from $build_dir" >&2

# Print old size
echo "- size before pruning: $(du -d0 -h . | cut -f1)" >&2

# Prune
grep -v '^#' config/tarball_exclusions | tr -s '\n' | sed -e 's/^/rm -rf /' | bash

date

# Building native extensions is hilarious, we usually end up with _3_ versions
# of any built `.bundle` _and_ the compiled object files before being linked
# together.
#
# This removes any `.{c,o,bundle}`, Makefile and byproduct files from the gem
# dir.
find vendor/gems/current/ \( \
  -name '*.c' -or \
  -name '*.o' -or \
  -name '*.bundle' -or \
  -name Makefile -or \
  -name Makefile.in -or \
  -name CMakeOutput.log -or \
  -name CMakeError.log \
\) -delete

date

find vendor/gems/current/../extensions \( \
  -name gem_make.out -or \
  -name mkmf.log \
\) -delete

date

# Print new size
echo "- size after pruning: $(du -d0 -h . | cut -f1)"

bt_end "cleaning unused files"
echo "%%%END FOLD%%%"

##
# TARBALL!!

# TODO experiment with _not_ gzipping the data?
#
# The question boils down to "what we are optimising for?".
#
# - compressing the archive reduces the amount of data needed to transit between
#   the github-build cluster and the github-afe cluster at the expense of
#   greater cpu usage to decompress
#
# - not compressing the archive increases the data transit but reduces the cpu
#   required to unarchive / install
#
# Compared to the git transport currently in use we're going to use scp to
# transit the archives.
#
# The git transport has more rtt and negotiation required but should transfer
# fewer bytes. Our scp transit will have fewer rtt required but will transfer
# more bytes.
#
# This discussion only relates to the transit used, and ignores the whole-system
# benefits we gain by pre-builting ruby, pre-bootstrapping the app,
# pre-precompiling the assets etc, comparing the transport systems in isolation.

archive_file="$output_dir/${commit_sha}-${build_arch}.tar"
assets_file="$output_dir/assets/${commit_sha}.tar"

echo "%%%FOLD {Archiving $build_dir into $archive_file}%%%"
echo
bt_start "tarball build"
mtime='--mtime=2000-01-01'
owner='--owner=root --group=root'
tar -c -f $archive_file $mtime $owner --mode="ugo=rX" --anchored \
  --exclude="tmp" --exclude="log" --exclude="node_modules"  --exclude="public" *

mkdir -p tmp
touch tmp/.keep
tar -r -f $archive_file $mtime $owner --mode="u=rwx,go=u-w" --exclude="tmp/*" tmp
tar -r -f $archive_file $mtime $owner --mode="ugo=r" tmp/.keep
tar -r -f $archive_file $mtime $owner --mode="u=rwx,go=u-w" --exclude="log/*" log

# These node modules are included because they're used at runtime.
for module in $(cat config/npm-keep-modules); do
  tar -r -f $archive_file $mtime $owner --mode="ugo=rX" "node_modules/$module"
done

# Archive public incrementally. We do this in stages because `public/static`
# has symlinks to resources outside the `public/` dir. We need to resolve them
# as we archive, but `public/` itself contains a few internal symlinks that we
# _don't_ want to resolve. So first we add just the `public/` dir with a mode
# allowing follow-up writes, then everything in `public` except `public/static`,
# then `public/static` with any symlinks resolved.

tar -r -f $archive_file $mtime $owner --mode="ugo=rX" --exclude="public/*" public
tar -r -f $archive_file $mtime $owner --mode="ugo=rX" --exclude="public/static" public/*
tar -r -f $archive_file $mtime $owner --mode="ugo=rX" --dereference public/static

chmod g+w,o+w $archive_file

tar -c -f $assets_file $mtime $owner --mode="ugo=rX" --exclude="public/*" public
tar -r -f $assets_file $mtime $owner --mode="ugo=rX" --exclude="public/static" public/*
tar -r -f $assets_file $mtime $owner --mode="ugo=rX" --dereference public/static

chmod g+w,o+w $assets_file

bt_end "tarball build"
echo "%%%END FOLD%%%"

bt_end "entire build"
bt_cleanup
