#!/bin/bash
# Common CI functions
#
# To use these in your script, add this line:
# source script/ci/common_functions.sh
#
source "$(cd "$(dirname "${BASH_SOURCE[0]}")/../.." && pwd)/vendor/bt/bt.sh"

section() {
  # Get the first 72 characters of the command
  # You can't use substring notation with $@, so we're assigning it first.
  section_name="${@}"
  section_name="${section_name:0:72}"

  named_section "${section_name}" $@
}

named_section() {
  section_name=$1
  shift

  start_fold "${section_name}"
  time_command "${section_name}" $@
  end_fold
}

start_fold() {
  echo "%%%FOLD {$@}%%%"
}

end_fold() {
  echo "%%%END FOLD%%%"
}

time_command() {
  start=$(date +%s)
  name=$1
  shift

  if bt_turned_on; then
    bt_start "${name}"
  fi

  set +e
  time $@
  result=$?
  end=$(date +%s)
  let duration=$end-$start
  set -e

  if bt_turned_on; then
    bt_end "${name}"
  fi

  echo "${name} took ${duration} seconds and exited ${result}."

  return $result
}

bt_turned_on() {
  if [ -n ${BT_INIT+x} ]; then
    # bt_init was called
    return 0
  else
    # bt_init wasn't called
    return 1
  fi
}
