#!/usr/bin/env safe-ruby

# frozen_string_literal: true

require "yaml"

DOTCOM_CONFIG = YAML.load_file("config/plans.yml")
ENTERPRISE_CONFIG = YAML.load_file("config/enterprise-plans.yml")

KEYS_TO_CHECK = ["default_features", "default_limits"]

# By default, assume everything is okay
failed = false

# Run all the linters
KEYS_TO_CHECK.each do |config_key|
  dotcom_keys = DOTCOM_CONFIG[config_key].keys
  enterprise_keys = ENTERPRISE_CONFIG[config_key].keys

  uniq = (dotcom_keys | enterprise_keys) - (dotcom_keys & enterprise_keys)
  uniq.each do |key|
    puts "[FAILURE] A value for #{config_key}->#{key} must be set in both dotcom & enterprise plan configs"
    failed = true
  end
end

# Fail if any differences were found
exit 1 if failed
