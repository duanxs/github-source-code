# frozen_string_literal: true

module DockerHelper
  extend self

  TIMEOUT = 60

  def write_docker_component_failure_annotation!
    return unless macos?

    File.open("/tmp/github-failed-component", "w") do |f|
      f.puts "docker_helper"
    end
  end

  def ensure_docker_running
    # Ensure a working Docker, since we now require Docker to build
    # certain services in tests
    if !docker_running?
      puts ""
      puts ""
      puts ""
      puts "Docker is required to complete bootstrapping for local"
      puts "development, but is not available."

      if !$stdin.tty?
        puts "Please make sure Docker for Mac is running." if macos?
        write_docker_component_failure_annotation!
        exit($?.exitstatus || 1)
      end

      # If we can automatically start it, let's try!
      # In interactive mode, give the user a chance to respond
      if macos? && File.exist?("/Applications/Docker.app")
        system "open", "-a", "Docker"
        puts "Docker for Mac has been automatically opened for you; please wait for"
        puts "the menu bar animation of the packages on the whale to finish"
        puts "before continuing."
        puts ""
        puts "If this is your first time using Docker, you will need to"
        puts "complete the first-time setup manually."

        # Poll for startup and continue if docker becomes available.
        puts
        print "Waiting up to #{TIMEOUT} seconds for docker to start"
        TIMEOUT.times do |t|
          print "."
          $stdout.flush

          if docker_running?
            puts
            puts "Docker started in #{t} seconds!"
            return true
          end
          sleep 1
        end

        # It didn't start, so we should fall through to allow the user to debug
        # or possibly complete first-time setup.
        puts
        puts "Docker didn't seem to start by itself."
        puts
      end

      puts
      puts "Please start the Docker service before continuing, and ensure you"
      puts "wait a minute for it to become available before continuing."

      puts ""
      print "Press enter to continue..."
      $stdout.flush
      $stdin.gets

      if !docker_running?
        puts ""
        puts ""
        puts ""
        puts "Oops, looks like Docker still isn't working properly."
        puts "Please ask for help in #friction if you're not able to"
        puts "get it running yourself!"
        write_docker_component_failure_annotation!
        exit($?.exitstatus || 1)
      end
    end
  end

  def docker_running?
    system("docker ps &>/dev/null")
  end

  def macos?
    RUBY_PLATFORM.to_s.downcase.include?("darwin")
  end
end

if __FILE__ == $0
  DockerHelper.ensure_docker_running
end
