import {promises as fs} from 'fs'
import * as path from 'path'
import {createHash} from 'crypto'

async function checkFile(file: string, sri: string) {
  const fingerPrint = (path.basename(file).match(/^(?:[\w-]+)-([a-f0-9]+).\w+$/) || [])[1]
  if (!fingerPrint) return null
  let contents = ''
  try {
    contents = (await fs.readFile(file, 'utf-8'))
  } catch (e) {
    throw new Error(`Integrities lists file of ${file} but it was unreadable.`)
  }
  const hash = createHash('sha512').update(contents).digest()
  const validateSri = `sha512-${hash.toString('base64')}`
  if (validateSri !== sri) {
    throw new Error(`Integrities lists ${file} with SRI of ${sri} but when checked it was ${validateSri}`)
  }
  const validateFingerPrint = hash.toString('hex').substr(0, fingerPrint.length)
  if (validateFingerPrint.toLowerCase() !== fingerPrint.toLowerCase()) {
    throw new Error(`Integrities lists ${file} with SRI of ${sri}. As a fingerprint this would be ${validateFingerPrint} but the file is named with a fingerprint of ${fingerPrint}`)
  }
}

async function main() {
  const integritiesFile = process.argv[2]
  if (!integritiesFile) throw new Error('must pass in integrities file')
  const workdir = path.dirname(integritiesFile)
  const integritiesContents = await fs.readFile(integritiesFile, 'utf-8')
  const integrities = integritiesContents.split('\n')
    .filter(line => line.length > 0)
    .map(line => line.split(' ').reverse())

  const errors = []
  let passes = 0
  for (const [file, sri] of integrities) {
    try {
      await checkFile(`${workdir}/${file}`, sri)
      passes += 1
    } catch (e) {
      errors.push(e)
    }
  }
  if (errors.length) {
    console.error(...errors.map(e => e.message + '\n\n'))
    throw new Error('integrity verification failed!')
  }
  if (passes === 0) {
    throw new Error('integrity verification failed: 0 files checked')
  }
  console.log(`integrity verification passed on ${passes} files`)
}

main().catch(e => {
  console.error(e)
  process.exit(1)
})
