#!/usr/bin/env safe-ruby
# frozen_string_literal: true
#
#/ Usage: system-role-base  [options]
#/
#/ For each role defined in config/system_roles.yml,
#/ this utility will echo all (role, base_role) pairs.
#/ If the role does not have a base role, an empty string will be printed.
#/ e.g. read
#/      triage read
#/
#/ OPTIONS:
#/   -h, --help         Show this message.

require "yaml"
require "optparse"

def help!
  exec "grep ^#/<'#{__FILE__}'|cut -c4-"
end

ARGV.options do |opts|
  opts.on_tail("-h", "--help") { help! }
  opts.parse!
end

if ARGV.any?
  STDERR.puts "Unknown arguments: #{ARGV.join(", ")}\n"
  help!
end

filepath = File.join(File.dirname(__FILE__), "..", "config", "system_roles.yml")
raw_data = File.read(filepath)
data = YAML.safe_load(raw_data)

roles = data["system_roles"] || {}
roles.each do |role, attrs|
  base = attrs["base"] || ""
  puts "#{role} #{base}"
end
