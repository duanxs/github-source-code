#!/usr/bin/env safe-ruby
# frozen_string_literal: true
#
#/ Usage: ghe-org-membership-update [options]
#/
#/ This utility bulk-updates all organization memberships across a GitHub
#/ Enterprise installation, to either public or private.
#/
#/ OPTIONS:
#/   -h, --help                              Show this message.
#/   -v VISIBILITY, --visibility=VISIBILITY  Required visibility to set for all
#/                                           organization memberships across the
#/                                           installation.
#/                                           Must be either "public" or "private".
#/
#/ Example:
#/
#/   ghe-org-membership-update --visibility=public
#/
require "optparse"

def help!
  exec "grep ^#/ <'#{__FILE__}' | cut -c4-"
end

if ARGV.empty?
  help!
  exit 0
end

membership_visibility = nil

ARGV.options do |opts|
  opts.on("-vVISIBILITY", "--visibility=VISIBILITY") do |visibility|
    membership_visibility = visibility
  end
  opts.on_tail("-h", "--help") { help! }
  opts.parse!
end

unless %w(public private).include?(membership_visibility)
  abort %Q[visibility is required and must be "public" or "private"]
end

printf "Are you sure that you want to make *all* organization memberships #{membership_visibility}? (y/n) "
exit 0 if STDIN.gets.chomp != "y"

require File.expand_path("../../config/environment", __FILE__)

begin
  OrgMembershipUpdater.new(membership_visibility: membership_visibility).run
rescue OrgMembershipUpdater::Error => error
  abort error.message
end

puts "All organization memberships updated to be #{membership_visibility}."

# vim: set syntax=ruby:
