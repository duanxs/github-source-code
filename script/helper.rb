# frozen_string_literal: true
# This file contains multiple helpers that can be re-used across  all executables in `./script`.
# Examples are: gettting a random user, the github ORG, or creating and installing a github app.
require "faker"

WaitError = Class.new(RuntimeError)

def print_progress(i)
  syms = %w{ - \\ | / }
  str = "#{syms[i % syms.length]} [#{i.to_s.rjust(3, " ")}]"
  if i.zero?
    print "Progress: "
    print str
  else
    print "#{8.chr.to_s * (str.length + 1)} #{str}"
  end
end

def wait_for(predicate:, timeout:)
  0.upto(timeout - 1) do |i|
    return if predicate.call
    print_progress(i)
    sleep(1)
  end
  raise WaitError, "Failed to create resource in time."
end

def wait_for_record_creation(klass:, attrs:, timeout: 300)
  puts "[Notice] waiting for #{klass} to be created. (#{attrs})"
  predicate = ->() { klass.exists?(attrs) }
  result = wait_for(predicate: predicate, timeout: timeout)
  puts "" # make sure we have a newline after all the pretty print stuff
rescue WaitError => ex
  puts
  raise WaitError, "[Fatal] failed to create resource [#{klass} (#{attrs})] within #{timeout} seconds."
end

# Create a GitHub App
def create_app(name:, owner:)
  app = Integration.new(
    owner: owner,
    name: name,
    url: "http://example.com",
    public: true,
    default_permissions: {
      "statuses" => :write,
      "contents" => :write,
      "pull_requests" => :write,
      "issues" => :write,
      "checks" => :write,
    },
    default_events: %w(pull_request check_suite check_run),
    hook_attributes: { url: "http://example.com/webhook", secret: "foo123" },
    callback_url: "http://example.com/callback",
  )

  app.save!
  app
end

def install_app(app:, repo:, installer:)
  app.install_on(
    repo.owner,
    repositories: repo,
    installer: installer,
    version: app.latest_version,
  ).installation
end

def create_repo(owner_name: random_user.login, repo_name:, setup_master: true, open_source: true)
  repo = Repository.nwo("#{owner_name}/#{repo_name}")
  return repo if repo

  owner = User.where(login: owner_name).first
  repo_name ||= random_repo_name_for(owner)

  random_boolean = [true, false].sample
  desc = random_boolean ? "#{Faker::Movies::StarWars.quote}" : ""
  repo = Repository.create!(owner: owner,
                            name: repo_name,
                            description: desc,
                            public: open_source,
                            created_by_user_id: owner.id)
  repo.setup_git_repository

  if setup_master && !owner.bot?
    commiter = owner.organization? ? owner.members.first : owner
    create_master_branch(commiter: commiter, repo: repo)
  end

  repo
end

def master_target_oid(repo:)
  repo.refs.find("master").target_oid
end

def create_branch(repo:, branch_name:, user:, oid:)
  repo.heads.create(branch_name, oid, user)
end

def create_push(user:, repo:, branch_name:, file_name:, file_content:, message: "Example commit message for checks", wait_for_creation: true)
  metadata = {
    message: message,
    committer: user,
    author: user,
  }

  ref = repo.heads.find(branch_name) || create_branch(repo: repo, user: user, branch_name: branch_name, oid: master_target_oid(repo: repo))
  after_commit = ref.append_commit(metadata, user) do |changes|
    changes.add(file_name, file_content)
  end

  push_attrs = {
    repository_id: repo.id,
    pusher_id: user.id,
    ref: ref.qualified_name,
    after: after_commit.oid,
  }
  if wait_for_creation
    wait_for_record_creation(klass: Push, attrs: push_attrs)
    Push.find_by(push_attrs)
  end
end

def create_commit(repo:, commiter:, branch_name:, message:, file_name:, file_content:)
  metadata = {
    message: message,
    committer: commiter,
    author: commiter,
  }

  ref = repo.refs.find_or_build("refs/heads/#{branch_name}")

  commit = ref.append_commit(metadata, commiter) do |changes|
    changes.add(file_name, file_content)
  end

  commit
end

def create_master_branch(repo:, commiter:)
  file_name = "README.md"
  file_content = "## #{repo.name.capitalize} Repository"
  message = "Initial commit"

  create_branch(repo: repo, branch_name: "master", user: commiter, oid: GitHub::NULL_OID)
  create_commit(repo: repo, commiter: commiter, branch_name: "master", message: message, file_name: file_name, file_content: file_content)
end

def create_push_with_pr(user:, repo:, branch_name:, file_name:, file_content:, body: "", title: "PR title", message: "Generated commit message", wait_for_creation: true)
  push = create_push(user: user, repo: repo, branch_name: branch_name, file_name: file_name, file_content: file_content, message: message, wait_for_creation: wait_for_creation)
  pr = create_pr(repo: repo, ref: branch_name, user: user, title: title, body: body)
  [push, pr]
end

def create_pr(repo:, ref:, user:, title: "Add Evergreen file", body: "Dummy content for checks!")
  pr = PullRequest.where(repository_id: repo.id, head_ref: ref, user_id: user.id).first
  if !pr
    pr_attributes = {
      user: user,
      base: "master",
      head: ref,
      title: title,
      body: body,
    }

    # Note: All of Linguists' PRs are with a forked head repository
    pr = PullRequest.create_for!(repo, pr_attributes)
  end
  pr
end

def toggle_feature_flag(action:, feature_flag:, username: "")
  system("bin/toggle-feature-flag #{action} #{feature_flag} #{username}")
end

def github_repo
  github_org.repositories.first!
end

def github_org
  Organization.find_by_login("GitHub")
end

def random_user
  User.where(type: "User").order(Arel.sql("RAND()")).first
end

def random_repo_name_for(owner)
  count = owner.repositories.count
  "#{Faker::Hipster.word}-#{count}"
end

def mona
  User.where(login: "monalisa").first
end

def admin_user
  owner.user? ? owner : owner.admins.first
end
