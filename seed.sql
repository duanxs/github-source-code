
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `abilities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `abilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `actor_id` int(11) NOT NULL,
  `actor_type` varchar(40) NOT NULL,
  `action` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `subject_type` varchar(60) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_abilities_by_identity` (`actor_type`,`actor_id`,`subject_type`,`subject_id`,`priority`,`parent_id`),
  KEY `index_abilities_on_priority` (`priority`),
  KEY `index_abilities_on_created_at` (`created_at`),
  KEY `index_abilities_prioritized` (`actor_id`,`actor_type`,`subject_id`,`subject_type`,`priority`,`action`),
  KEY `index_abilities_on_subject_id_and_subject_type_and_action` (`subject_id`,`subject_type`,`action`),
  KEY `index_abilities_on_parent_id` (`parent_id`),
  KEY `index_abilities_on_actor_id_subject_id_action_priority` (`actor_id`,`subject_id`,`actor_type`,`subject_type`,`action`,`priority`),
  KEY `index_abilities_on_si_and_st_and_priority_and_at_and_ai` (`subject_id`,`subject_type`,`priority`,`actor_type`,`actor_id`),
  KEY `index_abilities_on_st_and_at_and_ai_and_action_and_prior_and_si` (`subject_type`,`actor_type`,`actor_id`,`action`,`priority`,`subject_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `abilities_permissions_routing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `abilities_permissions_routing` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cluster` int(11) NOT NULL DEFAULT '0',
  `subject_type` varchar(60) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_abilities_permissions_routing_by_cluster` (`cluster`,`subject_type`),
  KEY `index_abilities_permissions_routing_by_subject_type` (`subject_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `abuse_reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `abuse_reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reporting_user_id` int(11) DEFAULT NULL,
  `reported_user_id` int(11) NOT NULL,
  `reported_content_id` int(11) DEFAULT NULL,
  `reported_content_type` varchar(40) DEFAULT NULL,
  `repository_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `reason` tinyint(4) NOT NULL DEFAULT '0',
  `show_to_maintainer` tinyint(1) NOT NULL DEFAULT '0',
  `resolved` tinyint(1) NOT NULL DEFAULT '0',
  `user_hidden` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_abuse_reports_on_reported_user_id` (`reported_user_id`),
  KEY `index_abuse_reports_on_content_and_reason` (`reported_content_id`,`reported_content_type`,`reason`),
  KEY `index_abuse_reports_on_repo_and_show_to_maintainer_and_resolved` (`repository_id`,`show_to_maintainer`,`resolved`),
  KEY `index_abuse_reports_on_reporting_user_id_and_user_hidden` (`reporting_user_id`,`user_hidden`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `actions_credz_schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actions_credz_schema_migrations` (
  `version` bigint(20) NOT NULL,
  `dirty` tinyint(1) NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `actions_deployer_schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actions_deployer_schema_migrations` (
  `version` bigint(20) NOT NULL,
  `dirty` tinyint(1) NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `actions_payloads_schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actions_payloads_schema_migrations` (
  `version` bigint(20) NOT NULL,
  `dirty` tinyint(1) NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `actions_usage_aggregations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actions_usage_aggregations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `billable_owner_type` varchar(12) NOT NULL,
  `billable_owner_id` int(11) NOT NULL,
  `aggregate_duration_in_milliseconds` bigint(20) NOT NULL,
  `metered_billing_cycle_starts_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `job_runtime_environment` varchar(20) NOT NULL,
  `duration_multiplier` decimal(4,2) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_actions_usage_agg_on_billable_owner_and_usage` (`billable_owner_type`,`billable_owner_id`,`metered_billing_cycle_starts_at`,`owner_id`,`repository_id`,`job_runtime_environment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `actions_usage_line_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actions_usage_line_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `duration_in_milliseconds` int(11) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `synchronization_batch_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `job_id` varchar(36) DEFAULT NULL,
  `job_runtime_environment` varchar(20) NOT NULL,
  `duration_multiplier` decimal(4,2) NOT NULL,
  `billable_owner_type` varchar(12) DEFAULT NULL,
  `billable_owner_id` int(11) DEFAULT NULL,
  `directly_billed` tinyint(1) NOT NULL DEFAULT '1',
  `check_run_id` bigint(11) unsigned DEFAULT NULL,
  `submission_state` enum('unsubmitted','submitted','skipped') NOT NULL DEFAULT 'unsubmitted',
  `submission_state_reason` varchar(24) DEFAULT NULL,
  `workflow_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_actions_usage_line_items_on_job_id` (`job_id`),
  KEY `index_actions_usage_line_items_on_owner_id` (`owner_id`),
  KEY `index_actions_usage_line_items_on_start_time` (`start_time`),
  KEY `index_actions_usage_line_items_on_end_time` (`end_time`),
  KEY `index_on_billable_owner_and_usage` (`billable_owner_type`,`billable_owner_id`,`end_time`,`job_runtime_environment`,`duration_in_milliseconds`,`duration_multiplier`),
  KEY `index_on_synchronization_batch_id_and_end_time` (`synchronization_batch_id`,`end_time`),
  KEY `index_actions_usage_line_items_on_check_run_id` (`check_run_id`),
  KEY `index_actions_usage_line_items_on_submission_state_and_reason` (`submission_state`,`submission_state_reason`),
  KEY `index_on_submission_state_and_created_at` (`submission_state`,`created_at`),
  KEY `index_actions_usage_line_items_on_workflow_id_and_created_at` (`workflow_id`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `acv_contributors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acv_contributors` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `contributor_email` varchar(255) NOT NULL,
  `ignore` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_id_contributor_email` (`repository_id`,`contributor_email`),
  KEY `index_contributor_email_ignore` (`contributor_email`,`ignore`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `advisory_credits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `advisory_credits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ghsa_id` varchar(19) NOT NULL,
  `accepted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `creator_id` int(11) NOT NULL,
  `declined_at` datetime DEFAULT NULL,
  `recipient_id` int(11) NOT NULL,
  `notified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_advisory_credits_on_ghsa_id_and_recipient_id` (`ghsa_id`,`recipient_id`),
  KEY `index_advisory_credits_on_ghsa_id_and_accepted_at` (`ghsa_id`,`accepted_at`),
  KEY `index_advisory_credits_on_recipient_id_and_accepted_at` (`recipient_id`,`accepted_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `application_callback_urls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application_callback_urls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` blob NOT NULL,
  `application_id` int(11) NOT NULL,
  `application_type` varchar(32) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_callback_urls_on_application_and_url` (`application_id`,`application_type`,`url`(2000))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ar_internal_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ar_internal_metadata` (
  `key` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_assignments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_assignments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assignee_id` int(11) NOT NULL,
  `assignee_type` varchar(255) DEFAULT NULL,
  `issue_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_archived_assignments_on_issue_id_and_assignee_id` (`issue_id`,`assignee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_commit_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_commit_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `body` mediumblob,
  `commit_id` varchar(40) NOT NULL,
  `path` varbinary(1024) DEFAULT NULL,
  `repository_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `line` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `formatter` varchar(30) DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `comment_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `comment_hidden_reason` varbinary(1024) DEFAULT NULL,
  `comment_hidden_classifier` varchar(50) DEFAULT NULL,
  `comment_hidden_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_archived_commit_comments_on_repository_id` (`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_deleted_issues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_deleted_issues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `deleted_by_id` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `old_issue_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_archived_deleted_issues_on_repository_id` (`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_deployment_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_deployment_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(25) NOT NULL DEFAULT 'unknown',
  `description` text,
  `target_url` varbinary(1024) DEFAULT NULL,
  `deployment_id` int(11) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `environment_url` text,
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `environment` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_deployment_statuses_on_deployment_id` (`deployment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_deployments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_deployments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` blob,
  `payload` mediumblob,
  `sha` varchar(40) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `ref` varchar(255) DEFAULT NULL,
  `environment` varchar(255) DEFAULT 'production',
  `task` varchar(128) DEFAULT 'deploy',
  `transient_environment` tinyint(1) NOT NULL DEFAULT '0',
  `production_environment` tinyint(1) NOT NULL DEFAULT '0',
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `latest_deployment_status_id` int(11) DEFAULT NULL,
  `latest_environment` varchar(255) DEFAULT NULL,
  `latest_status_state` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_deployments_on_repository_id_and_created_at` (`repository_id`,`created_at`),
  KEY `index_deployments_on_sha` (`sha`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_downloads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_downloads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `content_type` varchar(255) DEFAULT NULL,
  `repository_id` int(11) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `hits` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_downloads_on_repository_id` (`repository_id`),
  KEY `index_downloads_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_gist_backup_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_gist_backup_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gist_id` int(11) NOT NULL,
  `last_backup_at` datetime DEFAULT NULL,
  `last_error_at` datetime DEFAULT NULL,
  `error_count` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `last_backup_checksum` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_archived_gist_backup_statuses_on_gist_id` (`gist_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_gist_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_gist_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `body` mediumblob,
  `gist_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `formatter` varchar(20) DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `comment_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `comment_hidden_reason` varbinary(1024) DEFAULT NULL,
  `comment_hidden_classifier` varchar(50) DEFAULT NULL,
  `comment_hidden_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_archived_gist_comments_on_gist_id` (`gist_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_gist_replicas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_gist_replicas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gist_id` int(11) NOT NULL,
  `host` varchar(255) NOT NULL,
  `checksum` varchar(255) NOT NULL,
  `state` int(11) NOT NULL,
  `read_weight` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_archived_gist_replicas_on_gist_id_and_host` (`gist_id`,`host`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_gists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_gists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `repo_name` varchar(40) DEFAULT NULL,
  `description` blob,
  `public` tinyint(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `delete_flag` tinyint(1) NOT NULL DEFAULT '0',
  `pushed_at` datetime DEFAULT NULL,
  `disabled_by` int(11) DEFAULT NULL,
  `disabling_reason` varchar(20) DEFAULT NULL,
  `disabled_at` datetime DEFAULT NULL,
  `maintenance_status` varchar(20) DEFAULT NULL,
  `last_maintenance_at` datetime DEFAULT NULL,
  `pushed_count` int(11) NOT NULL DEFAULT '0',
  `pushed_count_since_maintenance` int(11) NOT NULL DEFAULT '0',
  `disk_usage` int(11) DEFAULT '0',
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `last_maintenance_attempted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_archived_gists_on_repo_name` (`repo_name`),
  KEY `index_archived_gists_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_hook_config_attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_hook_config_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hook_id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` varbinary(1024) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_archived_hooks_on_hook_id_and_key` (`hook_id`,`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_hook_event_subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_hook_event_subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subscriber_id` int(11) NOT NULL,
  `subscriber_type` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_archived_hook_events_by_subscriber_and_name` (`subscriber_id`,`subscriber_type`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_hooks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_hooks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT '0',
  `confirmed` tinyint(1) unsigned DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `installation_target_type` varchar(40) NOT NULL,
  `installation_target_id` int(11) NOT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `oauth_application_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_archived_hooks_installation_target` (`installation_target_id`,`installation_target_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_issue_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_issue_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `body` mediumblob,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `repository_id` int(11) NOT NULL,
  `formatter` varchar(20) DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `comment_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `comment_hidden_reason` varbinary(1024) DEFAULT NULL,
  `comment_hidden_classifier` varchar(50) DEFAULT NULL,
  `comment_hidden_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_issue_comments_on_repository_id` (`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_issue_event_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_issue_event_details` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `issue_event_id` bigint(11) unsigned NOT NULL,
  `label_id` bigint(20) DEFAULT NULL,
  `label_name` varbinary(1024) DEFAULT NULL,
  `label_color` varchar(6) DEFAULT NULL,
  `label_text_color` varchar(6) DEFAULT NULL,
  `milestone_title` varbinary(1024) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `subject_type` varchar(20) DEFAULT NULL,
  `title_was` varbinary(1024) DEFAULT NULL,
  `title_is` varbinary(1024) DEFAULT NULL,
  `deployment_id` int(11) DEFAULT NULL,
  `ref` varchar(255) DEFAULT NULL,
  `before_commit_oid` char(40) DEFAULT NULL,
  `after_commit_oid` char(40) DEFAULT NULL,
  `pull_request_review_state_was` int(11) DEFAULT NULL,
  `message` mediumblob,
  `pull_request_review_id` int(11) DEFAULT NULL,
  `column_name` varbinary(1024) DEFAULT NULL,
  `previous_column_name` varbinary(1024) DEFAULT NULL,
  `card_id` int(11) DEFAULT NULL,
  `review_request_id` int(11) DEFAULT NULL,
  `performed_by_project_workflow_action_id` int(11) DEFAULT NULL,
  `lock_reason` varchar(30) DEFAULT NULL,
  `milestone_id` int(10) unsigned DEFAULT NULL,
  `deployment_status_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `block_duration_days` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_archived_issue_event_details_on_issue_event_id` (`issue_event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_issue_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_issue_events` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `issue_id` int(11) unsigned DEFAULT NULL,
  `actor_id` int(11) unsigned DEFAULT NULL,
  `repository_id` int(11) unsigned DEFAULT NULL,
  `event` varchar(40) DEFAULT NULL,
  `commit_id` varchar(40) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `commit_repository_id` int(11) unsigned DEFAULT NULL,
  `referencing_issue_id` int(11) unsigned DEFAULT NULL,
  `raw_data` blob,
  `performed_by_integration_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_issue_events_on_repository_id` (`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_issues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_issues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `votes` int(11) DEFAULT '0',
  `issue_comments_count` int(11) DEFAULT '0',
  `number` int(11) DEFAULT '0',
  `position` float DEFAULT '1',
  `title` varbinary(1024) DEFAULT NULL,
  `state` varchar(6) DEFAULT NULL,
  `body` mediumblob,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `closed_at` datetime DEFAULT NULL,
  `pull_request_id` int(11) DEFAULT NULL,
  `milestone_id` int(11) unsigned DEFAULT NULL,
  `assignee_id` int(11) unsigned DEFAULT NULL,
  `contributed_at_timestamp` bigint(20) DEFAULT NULL,
  `contributed_at_offset` mediumint(9) DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `created_by_logbook_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_archived_issues_on_repository_id` (`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_labels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_labels` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varbinary(1024) NOT NULL,
  `color` varchar(10) DEFAULT NULL,
  `repository_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `lowercase_name` varbinary(1024) DEFAULT NULL,
  `description` varbinary(400) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_labels_on_repository_id` (`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_milestones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_milestones` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` mediumblob,
  `description` mediumblob,
  `due_on` datetime DEFAULT NULL,
  `created_by_id` int(11) unsigned NOT NULL,
  `repository_id` int(11) unsigned NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `state` varchar(30) DEFAULT NULL,
  `number` int(11) unsigned DEFAULT '0',
  `open_issue_count` int(11) unsigned DEFAULT '0',
  `closed_issue_count` int(11) unsigned DEFAULT '0',
  `closed_at` timestamp NULL DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_milestones_on_due_on` (`due_on`),
  KEY `index_milestones_on_created_at` (`created_at`),
  KEY `index_milestones_on_created_by_id` (`created_by_id`),
  KEY `index_milestones_on_state` (`state`),
  KEY `index_milestones_on_number` (`number`),
  KEY `index_milestones_on_open_issue_count` (`open_issue_count`),
  KEY `index_milestones_on_closed_issue_count` (`closed_issue_count`),
  KEY `index_milestones_on_repository_id` (`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_project_cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_project_cards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `column_id` int(11) DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `content_type` varchar(20) DEFAULT NULL,
  `content_id` int(11) DEFAULT NULL,
  `note` mediumblob,
  `priority` bigint(20) unsigned DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `hidden_at` datetime DEFAULT NULL,
  `archived_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_archived_project_cards_on_project_id` (`project_id`),
  KEY `index_archived_project_cards_on_content_type_and_content_id` (`content_type`,`content_id`),
  KEY `index_archived_project_cards_on_content_id` (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_project_columns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_project_columns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `name` varbinary(1024) NOT NULL,
  `color` varchar(255) DEFAULT NULL,
  `ordered_card_ids` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `hidden_at` datetime DEFAULT NULL,
  `purpose` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_archived_project_columns_on_project_id` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_project_repository_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_project_repository_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_archived_project_repository_links_on_project_id` (`project_id`),
  KEY `index_archived_project_repository_links_on_repository_id` (`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_project_workflow_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_project_workflow_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_workflow_id` int(11) NOT NULL,
  `action_type` varchar(64) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `last_updater_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_archived_project_workflow_actions_on_project_workflow_id` (`project_workflow_id`),
  KEY `index_archived_project_workflow_actions_on_project_id` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_project_workflows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_project_workflows` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trigger_type` varchar(64) NOT NULL,
  `project_id` int(11) NOT NULL,
  `project_column_id` int(11) DEFAULT NULL,
  `creator_id` int(11) NOT NULL,
  `last_updater_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_archived_project_workflows_on_project_id` (`project_id`),
  KEY `index_archived_project_workflows_on_project_column_id` (`project_column_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varbinary(1024) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `owner_type` varchar(30) NOT NULL,
  `body` mediumblob,
  `creator_id` int(11) NOT NULL,
  `number` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `closed_at` datetime DEFAULT NULL,
  `last_sync_at` datetime DEFAULT NULL,
  `public` tinyint(1) NOT NULL DEFAULT '0',
  `source_kind` varchar(20) DEFAULT NULL,
  `source_id` int(11) DEFAULT NULL,
  `track_progress` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `index_archived_projects_on_owner_id_and_owner_type` (`owner_id`,`owner_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_protected_branches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_protected_branches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `name` varbinary(1024) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `creator_id` int(11) NOT NULL,
  `required_status_checks_enforcement_level` int(11) NOT NULL DEFAULT '0',
  `block_force_pushes_enforcement_level` int(11) NOT NULL DEFAULT '2',
  `block_deletions_enforcement_level` int(11) NOT NULL DEFAULT '2',
  `strict_required_status_checks_policy` tinyint(1) NOT NULL DEFAULT '1',
  `authorized_actors_only` tinyint(1) NOT NULL DEFAULT '0',
  `pull_request_reviews_enforcement_level` int(11) NOT NULL DEFAULT '0',
  `authorized_dismissal_actors_only` tinyint(1) NOT NULL DEFAULT '0',
  `admin_enforced` tinyint(1) NOT NULL DEFAULT '0',
  `dismiss_stale_reviews_on_push` tinyint(1) NOT NULL DEFAULT '0',
  `require_code_owner_review` tinyint(1) NOT NULL DEFAULT '0',
  `signature_requirement_enforcement_level` int(11) NOT NULL DEFAULT '0',
  `required_approving_review_count` tinyint(4) NOT NULL DEFAULT '1',
  `linear_history_requirement_enforcement_level` int(11) NOT NULL DEFAULT '0',
  `allow_force_pushes_enforcement_level` int(11) NOT NULL DEFAULT '0',
  `allow_deletions_enforcement_level` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_archived_protected_branches_on_repository_id_and_name` (`repository_id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_pull_request_review_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_pull_request_review_comments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pull_request_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `repository_id` int(11) NOT NULL,
  `path` varbinary(1024) DEFAULT NULL,
  `commit_id` varchar(40) DEFAULT NULL,
  `diff_hunk` mediumblob,
  `body` mediumblob,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `position` int(11) unsigned DEFAULT NULL,
  `original_commit_id` varchar(40) DEFAULT NULL,
  `original_position` int(11) unsigned DEFAULT NULL,
  `formatter` varchar(20) DEFAULT NULL,
  `original_base_commit_id` varchar(40) DEFAULT NULL,
  `original_start_commit_id` varchar(40) DEFAULT NULL,
  `original_end_commit_id` varchar(40) DEFAULT NULL,
  `blob_position` int(11) unsigned DEFAULT NULL,
  `blob_path` varbinary(1024) DEFAULT NULL,
  `blob_commit_oid` char(40) DEFAULT NULL,
  `left_blob` tinyint(1) NOT NULL DEFAULT '0',
  `state` int(11) NOT NULL DEFAULT '0',
  `pull_request_review_id` int(11) DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `reply_to_id` int(11) DEFAULT NULL,
  `outdated` tinyint(1) NOT NULL DEFAULT '0',
  `comment_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `comment_hidden_reason` varbinary(1024) DEFAULT NULL,
  `comment_hidden_classifier` varchar(50) DEFAULT NULL,
  `comment_hidden_by` int(11) DEFAULT NULL,
  `pull_request_review_thread_id` int(11) DEFAULT NULL,
  `start_position_offset` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_archived_pull_request_review_comments_on_repository_id` (`repository_id`),
  KEY `index_archived_pull_request_review_comments_on_pull_request_id` (`pull_request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_pull_request_review_threads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_pull_request_review_threads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pull_request_id` int(11) DEFAULT NULL,
  `pull_request_review_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `resolver_id` int(11) DEFAULT NULL,
  `resolved_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_archived_pull_request_review_threads_on_pull_request_id` (`pull_request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_pull_request_reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_pull_request_reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pull_request_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `head_sha` char(40) NOT NULL,
  `body` mediumblob,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `submitted_at` datetime DEFAULT NULL,
  `formatter` varchar(20) DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `comment_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `comment_hidden_reason` varbinary(1024) DEFAULT NULL,
  `comment_hidden_classifier` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_archived_pull_request_reviews_on_pull_request_id` (`pull_request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_pull_request_reviews_review_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_pull_request_reviews_review_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pull_request_review_id` int(11) NOT NULL,
  `review_request_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_archived_pr_review_rev_reqs_on_request_id_and_pr_review_id` (`review_request_id`,`pull_request_review_id`),
  KEY `index_archived_pr_review_rev_reqs_on_pr_review_id_and_request_id` (`pull_request_review_id`,`review_request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_pull_request_revisions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_pull_request_revisions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pull_request_id` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `ready` tinyint(1) NOT NULL,
  `base_oid` varchar(64) NOT NULL,
  `head_oid` varchar(64) NOT NULL,
  `revised_at` datetime NOT NULL,
  `force_pushed` tinyint(1) NOT NULL,
  `commits_count` int(11) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `push_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_archived_pull_request_revisions_on_pull_request_id` (`pull_request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_pull_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_pull_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `base_sha` char(40) DEFAULT NULL,
  `head_sha` char(40) DEFAULT NULL,
  `repository_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `base_repository_id` int(11) DEFAULT NULL,
  `head_repository_id` int(11) DEFAULT NULL,
  `base_ref` varbinary(1024) DEFAULT NULL,
  `head_ref` varbinary(1024) DEFAULT NULL,
  `merged_at` datetime DEFAULT NULL,
  `base_user_id` int(11) DEFAULT NULL,
  `head_user_id` int(11) DEFAULT NULL,
  `mergeable` tinyint(1) unsigned DEFAULT NULL,
  `merge_commit_sha` char(40) DEFAULT NULL,
  `contributed_at_timestamp` bigint(20) DEFAULT NULL,
  `contributed_at_offset` mediumint(9) DEFAULT NULL,
  `fork_collab_state` int(11) NOT NULL DEFAULT '0',
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `base_sha_on_merge` char(40) DEFAULT NULL,
  `work_in_progress` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_pull_requests_on_repository_id_and_user_id` (`repository_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_release_assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_release_assets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `release_id` int(11) NOT NULL,
  `uploader_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type` varchar(255) NOT NULL,
  `size` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `uploaded` tinyint(1) DEFAULT NULL,
  `guid` varchar(255) DEFAULT NULL,
  `downloads` int(11) DEFAULT '0',
  `state` int(11) DEFAULT '0',
  `label` varchar(255) DEFAULT NULL,
  `storage_blob_id` int(11) DEFAULT NULL,
  `oid` varchar(255) DEFAULT NULL,
  `storage_provider` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `by_name` (`release_id`,`name`),
  KEY `by_content_type` (`content_type`),
  KEY `by_repository_id_and_guid` (`repository_id`,`guid`),
  KEY `by_state` (`state`),
  KEY `by_uploaded` (`uploaded`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_releases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_releases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varbinary(1024) DEFAULT NULL,
  `tag_name` varbinary(1024) NOT NULL,
  `body` mediumblob,
  `author_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `state` int(11) DEFAULT '0',
  `pending_tag` varbinary(1024) DEFAULT NULL,
  `prerelease` tinyint(1) NOT NULL DEFAULT '0',
  `target_commitish` varchar(255) DEFAULT NULL,
  `published_at` datetime DEFAULT NULL,
  `formatter` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `by_published` (`repository_id`,`published_at`),
  KEY `by_state` (`repository_id`,`state`),
  KEY `by_repo_and_tag` (`repository_id`,`tag_name`(50))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_repositories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_repositories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `owner_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `sandbox` tinyint(1) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `public` tinyint(1) DEFAULT '1',
  `description` mediumblob,
  `homepage` varchar(255) DEFAULT NULL,
  `source_id` int(11) DEFAULT NULL,
  `public_push` tinyint(1) DEFAULT NULL,
  `disk_usage` int(11) DEFAULT '0',
  `locked` tinyint(1) DEFAULT '0',
  `pushed_at` datetime DEFAULT NULL,
  `watcher_count` int(11) DEFAULT '0',
  `public_fork_count` int(11) NOT NULL DEFAULT '1',
  `primary_language_name_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `has_issues` tinyint(1) DEFAULT '1',
  `has_wiki` tinyint(1) DEFAULT '1',
  `has_discussions` tinyint(1) NOT NULL DEFAULT '0',
  `has_downloads` tinyint(1) DEFAULT '1',
  `raw_data` blob,
  `organization_id` int(11) DEFAULT NULL,
  `disabled_at` datetime DEFAULT NULL,
  `disabled_by` int(11) DEFAULT NULL,
  `disabling_reason` varchar(30) DEFAULT NULL,
  `health_status` varchar(30) DEFAULT NULL,
  `pushed_at_usec` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `reflog_sync_enabled` tinyint(1) DEFAULT '0',
  `made_public_at` datetime DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `maintained` tinyint(1) NOT NULL DEFAULT '1',
  `template` tinyint(1) NOT NULL DEFAULT '0',
  `owner_login` varchar(40) DEFAULT NULL,
  `world_writable_wiki` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_archived_repositories_on_updated_at` (`updated_at`),
  KEY `index_archived_repositories_on_owner_id` (`owner_id`),
  KEY `index_archived_repositories_on_owner_login_and_name_and_active` (`owner_login`,`name`,`active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_repository_backup_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_repository_backup_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `last_backup_at` datetime DEFAULT NULL,
  `last_wiki_backup_at` datetime DEFAULT NULL,
  `last_error_at` datetime DEFAULT NULL,
  `last_wiki_error_at` datetime DEFAULT NULL,
  `error_count` int(11) NOT NULL DEFAULT '0',
  `wiki_error_count` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `last_backup_checksum` varchar(255) DEFAULT NULL,
  `last_wiki_backup_checksum` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_archived_repository_backup_statuses_on_repository_id` (`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_repository_checksums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_repository_checksums` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `repository_type` int(11) NOT NULL DEFAULT '0',
  `checksum` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_archived_repository_checksums_on_repository_type` (`repository_id`,`repository_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_repository_replicas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_repository_replicas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `repository_type` int(11) NOT NULL DEFAULT '0',
  `host` varchar(255) NOT NULL,
  `checksum` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_archived_repository_replicas_on_repository_type_and_host` (`repository_id`,`repository_type`,`host`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_repository_sequences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_repository_sequences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `number` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_archived_repository_sequences_on_repository_id` (`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_repository_vulnerability_alerts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_repository_vulnerability_alerts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vulnerability_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `vulnerable_version_range_id` int(11) NOT NULL,
  `show_alert` tinyint(1) NOT NULL DEFAULT '1',
  `vulnerable_manifest_path` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `dismisser_id` int(11) DEFAULT NULL,
  `dismiss_reason` varchar(255) DEFAULT NULL,
  `dismissed_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_repository_wikis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_repository_wikis` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `pushed_at` datetime DEFAULT NULL,
  `maintenance_status` varchar(255) DEFAULT NULL,
  `last_maintenance_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `pushed_count` int(11) NOT NULL DEFAULT '0',
  `pushed_count_since_maintenance` int(11) NOT NULL DEFAULT '0',
  `last_maintenance_attempted_at` datetime DEFAULT NULL,
  `cache_version_number` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_wikis_on_repository_id` (`repository_id`),
  KEY `index_repository_wikis_on_maintenance_status` (`maintenance_status`,`pushed_count_since_maintenance`,`last_maintenance_at`),
  KEY `index_archived_repository_wikis_on_pushed_at` (`pushed_at`),
  KEY `index_archived_repository_wikis_on_last_maintenance_at` (`last_maintenance_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_required_status_checks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_required_status_checks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `protected_branch_id` int(11) NOT NULL,
  `context` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_required_status_checks_on_id_and_context` (`protected_branch_id`,`context`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_review_request_reasons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_review_request_reasons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `review_request_id` int(11) NOT NULL,
  `codeowners_tree_oid` varchar(40) DEFAULT NULL,
  `codeowners_path` varchar(255) DEFAULT NULL,
  `codeowners_line` int(11) DEFAULT NULL,
  `codeowners_pattern` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_archived_review_request_reasons_on_review_request_id` (`review_request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_review_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_review_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reviewer_id` int(11) NOT NULL,
  `pull_request_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `reviewer_type` varchar(64) NOT NULL,
  `dismissed_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_archived_review_requests_on_pull_request_id` (`pull_request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `archived_user_reviewed_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archived_user_reviewed_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pull_request_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `filepath` varbinary(1024) NOT NULL,
  `head_sha` varbinary(40) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `dismissed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_archived_user_reviewed_files_on_pull_request_id` (`pull_request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `artifacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `artifacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `check_run_id` bigint(11) unsigned DEFAULT NULL,
  `source_url` text NOT NULL,
  `name` varbinary(1024) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `size` bigint(20) NOT NULL DEFAULT '0',
  `check_suite_id` bigint(11) unsigned DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_artifacts_on_check_run_id` (`check_run_id`),
  KEY `index_artifacts_on_check_suite_id` (`check_suite_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `asset_activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `bandwidth_down` float NOT NULL DEFAULT '0',
  `bandwidth_up` float NOT NULL DEFAULT '0',
  `byte_hours` float NOT NULL DEFAULT '0',
  `activity_started_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `source_lines` text,
  `source_files` text,
  `asset_type` int(11) NOT NULL DEFAULT '0',
  `repository_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_asset_activities_on_type_owner_started_at_and_repo` (`asset_type`,`owner_id`,`activity_started_at`,`repository_id`),
  KEY `index_asset_activities_on_type_owner_and_repo` (`asset_type`,`owner_id`,`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `asset_actor_activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_actor_activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `bandwidth_down` float NOT NULL DEFAULT '0',
  `bandwidth_up` float NOT NULL DEFAULT '0',
  `activity_started_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `asset_type` int(11) NOT NULL DEFAULT '0',
  `repository_id` int(11) NOT NULL DEFAULT '0',
  `key_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_asset_actor_activities_on_type_owner_actor_key_time_repo` (`asset_type`,`owner_id`,`actor_id`,`key_id`,`activity_started_at`,`repository_id`),
  KEY `index_asset_actor_activities_on_type_owner_repo_and_time` (`asset_type`,`owner_id`,`repository_id`,`activity_started_at`),
  KEY `index_asset_actor_activities_on_type_owner_actor_time_repo` (`asset_type`,`owner_id`,`actor_id`,`activity_started_at`,`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `asset_actor_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_actor_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `bandwidth_down` float NOT NULL DEFAULT '0',
  `bandwidth_up` float NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `asset_type` int(11) NOT NULL DEFAULT '0',
  `key_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_asset_actor_statuses_on_type_owner_actor_and_key` (`asset_type`,`owner_id`,`actor_id`,`key_id`),
  KEY `index_asset_actor_statuses_asset_type_owner_id_bandwidth_down` (`asset_type`,`owner_id`,`bandwidth_down`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `asset_archives`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_archives` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `path_prefix` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_on_asset_id_and_path_prefix` (`asset_id`,`path_prefix`),
  KEY `index_asset_archives_on_created_at` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `asset_references`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_references` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) NOT NULL,
  `uploadable_id` int(11) NOT NULL,
  `uploadable_type` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `by_uploadable` (`uploadable_id`,`uploadable_type`,`asset_id`),
  KEY `by_uploadable_type` (`asset_id`,`uploadable_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `asset_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) DEFAULT NULL,
  `bandwidth_down` float NOT NULL DEFAULT '0',
  `bandwidth_up` float NOT NULL DEFAULT '0',
  `storage` float NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `asset_packs` int(11) NOT NULL DEFAULT '0',
  `notified_state` int(11) NOT NULL DEFAULT '0',
  `notified_at` datetime DEFAULT NULL,
  `data_packs` int(11) NOT NULL DEFAULT '0',
  `asset_type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_asset_statuses_on_asset_type_and_owner_id` (`asset_type`,`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `asset_sync_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_sync_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `marker` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_asset_sync_statuses_on_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oid` varchar(64) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `width` int(11) DEFAULT '0',
  `height` int(11) DEFAULT '0',
  `charset` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_assets_on_oid` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `assignments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assignments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assignee_id` int(11) NOT NULL,
  `assignee_type` varchar(255) DEFAULT NULL,
  `issue_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_assignments_on_issue_id_and_assignee_id` (`issue_id`,`assignee_id`),
  KEY `index_assignments_on_assignee_and_issue_id` (`assignee_id`,`issue_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `attachments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attacher_id` int(11) NOT NULL,
  `asset_id` int(11) NOT NULL,
  `attachable_id` int(11) NOT NULL,
  `attachable_type` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `entity_id` int(11) DEFAULT NULL,
  `entity_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `by_asset` (`asset_id`,`attachable_id`,`attachable_type`),
  KEY `by_attachable` (`attachable_id`,`attachable_type`),
  KEY `by_attacher` (`attacher_id`),
  KEY `index_attachments_on_entity_id_and_entity_type` (`entity_id`,`entity_type`),
  KEY `index_attachments_on_asset_id_and_entity_type` (`asset_id`,`entity_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `attribution_invitations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attribution_invitations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source_id` int(11) NOT NULL,
  `target_id` int(11) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `state` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_attribution_invitations_on_source_id` (`source_id`),
  KEY `index_attribution_invitations_on_creator_id` (`creator_id`),
  KEY `index_attribution_invitations_on_owner_id` (`owner_id`),
  KEY `index_attribution_invitations_on_target_id_and_owner_id` (`target_id`,`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `audit_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audit_entries` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `document_id` varchar(50) NOT NULL,
  `document` json NOT NULL,
  `created_at` datetime(3) NOT NULL,
  `shard_key` varchar(80) NOT NULL,
  `action` varchar(100) GENERATED ALWAYS AS (left(json_unquote(json_extract(`document`,'$.action')),100)) VIRTUAL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_audit_logs_on_document_id` (`document_id`),
  KEY `index_audit_entries_on_shard_key` (`shard_key`),
  KEY `index_audit_entries_on_created_at` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `audit_entries_document_id_vdx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audit_entries_document_id_vdx` (
  `document_id` varchar(50) NOT NULL,
  `keyspace_id` varbinary(128) NOT NULL,
  PRIMARY KEY (`document_id`,`keyspace_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Vitess VSchema';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `audit_entries_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audit_entries_seq` (
  `id` int(11) NOT NULL,
  `next_id` bigint(20) DEFAULT NULL,
  `cache` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='vitess_sequence';
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `audit_log_exports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audit_log_exports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_type` varchar(255) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `phrase` varchar(255) DEFAULT NULL,
  `format` varchar(255) NOT NULL DEFAULT 'json',
  `token` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_audit_log_exports_on_token` (`token`),
  KEY `index_audit_log_exports_on_subject_type_and_subject_id` (`subject_type`,`subject_id`),
  KEY `index_audit_log_exports_on_actor_id` (`actor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `audit_log_git_event_exports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audit_log_git_event_exports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `actor_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `subject_type` varchar(255) DEFAULT NULL,
  `token` varchar(255) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_audit_log_git_event_exports_on_token` (`token`),
  KEY `index_audit_log_git_event_exports_on_actor_id_and_token` (`actor_id`,`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `authenticated_devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authenticated_devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `device_id` varchar(32) NOT NULL,
  `display_name` varbinary(1024) NOT NULL,
  `accessed_at` datetime NOT NULL,
  `approved_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_authenticated_devices_on_device_id_and_user_id` (`device_id`,`user_id`),
  KEY `index_authenticated_devices_on_user_id_and_approved_at` (`user_id`,`approved_at`),
  KEY `index_authenticated_devices_on_approved_at_and_accessed_at` (`approved_at`,`accessed_at`),
  KEY `index_authenticated_devices_on_accessed_at` (`accessed_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `authentication_limit_whitelist_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authentication_limit_whitelist_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `metric` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `note` varchar(255) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `expires_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `authentication_limit_whitelist_entries_on_metric_value_expires` (`metric`,`value`,`expires_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `authentication_records`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authentication_records` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(2) DEFAULT NULL,
  `octolytics_id` varchar(32) NOT NULL,
  `client` varchar(40) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `user_agent` text,
  `flagged_reason` varchar(32) DEFAULT NULL,
  `user_session_id` int(11) DEFAULT NULL,
  `ip_address` varchar(40) NOT NULL,
  `authenticated_device_id` int(11) unsigned DEFAULT NULL,
  `region_name` varchar(64) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_authentication_records_on_user_id_and_created_at` (`user_id`,`created_at`),
  KEY `index_authentication_records_on_user_session_id_and_ip_address` (`user_session_id`,`ip_address`),
  KEY `index_authentication_records_on_created_at` (`created_at`),
  KEY `authentication_records_on_user_id_country_code_and_created_at` (`user_id`,`country_code`,`created_at`),
  KEY `authentication_records_on_user_id_octolytics_id_and_created_at` (`user_id`,`octolytics_id`,`created_at`),
  KEY `index_authentication_records_on_user_id_and_device_id` (`user_id`,`authenticated_device_id`),
  KEY `index_authentication_records_ondevice_id_ip_address_and_user_id` (`authenticated_device_id`,`ip_address`,`user_id`),
  KEY `index_authentication_records_on_ip_address` (`ip_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `authentication_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authentication_tokens` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `authenticatable_id` bigint(20) NOT NULL,
  `authenticatable_type` varchar(40) NOT NULL,
  `hashed_value` varchar(44) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `token_last_eight` char(8) DEFAULT NULL,
  `expires_at_timestamp` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_authentication_tokens_on_hashed_value` (`hashed_value`),
  KEY `index_authentication_tokens_on_authenticatable_id_and_type` (`authenticatable_id`,`authenticatable_type`),
  KEY `index_authentication_tokens_on_expires_at_timestamp` (`expires_at_timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `autosave_checkpoint_authors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autosave_checkpoint_authors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `autosave_checkpoint_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_autosave_checkpoint_authors_on_autosave_checkpoint_id` (`autosave_checkpoint_id`),
  KEY `index_autosave_checkpoint_authors_on_author_id` (`author_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `autosave_checkpoints`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autosave_checkpoints` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `branch` varbinary(1024) NOT NULL,
  `sha` varchar(40) NOT NULL,
  `commit_sha` varchar(40) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_autosave_checkpoints_on_repo_branch_sha_created` (`repository_id`,`branch`,`sha`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `autosaves`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autosaves` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `operation` blob NOT NULL,
  `repository_id` int(11) NOT NULL,
  `branch` varbinary(1024) NOT NULL,
  `author_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `commit_oid` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_autosaves_on_author_id` (`author_id`),
  KEY `index_autosaves_on_repo_branch_commit_created` (`repository_id`,`branch`,`commit_oid`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `avatars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `avatars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) DEFAULT NULL,
  `content_type` varchar(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `owner_id` int(11) NOT NULL,
  `owner_type` varchar(30) NOT NULL,
  `cropped_x` int(11) NOT NULL DEFAULT '0',
  `cropped_y` int(11) NOT NULL DEFAULT '0',
  `cropped_width` int(11) NOT NULL DEFAULT '0',
  `cropped_height` int(11) NOT NULL DEFAULT '0',
  `uploader_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `storage_blob_id` int(11) DEFAULT NULL,
  `oid` varchar(64) NOT NULL DEFAULT '',
  `size` bigint(20) NOT NULL DEFAULT '0',
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `storage_provider` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_avatars_on_owner_id_and_owner_type_and_asset_id` (`owner_id`,`owner_type`,`asset_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `azp_resources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `azp_resources` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `repository_id` varchar(120) COLLATE utf8mb4_bin NOT NULL COMMENT 'stores a global_relay_id that can relate to any github type - see azp_resources.go',
  `entity_id` varchar(120) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'to replace repository_id',
  `environment` varchar(40) COLLATE utf8mb4_bin NOT NULL,
  `locked_at` datetime(6) DEFAULT NULL COMMENT 'the point at which the row was locked for creation',
  `locked_by` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT 'the call that created the resources this row tracks',
  `created_at` datetime(6) DEFAULT NULL,
  `organization_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `tenant_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'to replace organization_name',
  `tenant_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'Stores a GUID for the tenant host',
  `project_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `pipeline_id` int(11) DEFAULT NULL,
  `client_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `private_key` mediumblob,
  PRIMARY KEY (`id`),
  UNIQUE KEY `repository_id` (`repository_id`,`environment`),
  UNIQUE KEY `entity_id` (`entity_id`,`environment`),
  KEY `by_entity_id` (`entity_id`),
  KEY `by_organization_name` (`organization_name`),
  KEY `by_tenant_name` (`tenant_name`),
  KEY `by_tenant_id` (`tenant_id`),
  KEY `by_locked_at` (`locked_at`),
  KEY `by_created_at` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `banned_ip_addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banned_ip_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(40) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_banned_ip_addresses_on_ip` (`ip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `billing_budgets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `billing_budgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_type` varchar(12) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `enforce_spending_limit` tinyint(1) NOT NULL DEFAULT '1',
  `spending_limit_in_subunits` int(11) NOT NULL DEFAULT '0',
  `spending_limit_currency_code` varchar(3) NOT NULL DEFAULT 'USD',
  `product` enum('shared','codespaces') NOT NULL,
  `effective_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_billing_budgets_on_owner_and_product_and_effective_at` (`owner_type`,`owner_id`,`product`,`effective_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `billing_disputes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `billing_disputes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `platform` int(11) NOT NULL,
  `platform_dispute_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `amount_in_subunits` int(11) NOT NULL DEFAULT '0',
  `currency_code` varchar(3) NOT NULL DEFAULT 'USD',
  `reason` varchar(255) NOT NULL DEFAULT 'general',
  `status` varchar(255) NOT NULL DEFAULT 'unknown',
  `refundable` tinyint(1) NOT NULL DEFAULT '0',
  `response_due_by` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `billing_transaction_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_billing_disputes_on_platform_and_platform_dispute_id` (`platform`,`platform_dispute_id`),
  KEY `index_billing_disputes_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `billing_metered_usage_configurations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `billing_metered_usage_configurations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_type` varchar(12) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `enforce_spending_limit` tinyint(1) NOT NULL DEFAULT '1',
  `spending_limit_in_subunits` int(11) NOT NULL DEFAULT '0',
  `spending_limit_currency_code` varchar(3) NOT NULL DEFAULT 'USD',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_billing_metered_usage_configurations_on_owner` (`owner_type`,`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `billing_payouts_ledger_discrepancies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `billing_payouts_ledger_discrepancies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stripe_connect_account_id` int(11) NOT NULL,
  `status` enum('unresolved','resolved') NOT NULL DEFAULT 'unresolved',
  `discrepancy_in_subunits` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_payouts_ledger_discrepancies_on_status_and_created_at` (`status`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `billing_payouts_ledger_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `billing_payouts_ledger_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stripe_connect_account_id` int(11) NOT NULL,
  `transaction_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `transaction_type` int(11) NOT NULL,
  `amount_in_subunits` int(11) NOT NULL DEFAULT '0',
  `currency_code` varchar(3) NOT NULL DEFAULT 'USD',
  `primary_reference_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `additional_reference_ids` text,
  PRIMARY KEY (`id`),
  KEY `index_billing_payouts_ledger_entries_on_primary_reference_id` (`primary_reference_id`),
  KEY `index_ledger_entries_on_acct_id_and_txn_type_and_timestamp` (`stripe_connect_account_id`,`transaction_type`,`transaction_timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `billing_prepaid_metered_usage_refills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `billing_prepaid_metered_usage_refills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_type` varchar(12) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `expires_on` date NOT NULL,
  `amount_in_subunits` int(11) NOT NULL,
  `currency_code` varchar(3) NOT NULL DEFAULT 'USD',
  `zuora_rate_plan_charge_id` varchar(32) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `zuora_rate_plan_charge_number` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_billing_prepaid_metered_usage_refills_on_zuora_rpc_id` (`zuora_rate_plan_charge_id`),
  UNIQUE KEY `index_on_zuora_rate_plan_charge_number` (`zuora_rate_plan_charge_number`),
  KEY `idx_billing_prepaid_metered_usage_refills_on_owner_and_expiry` (`owner_id`,`owner_type`,`expires_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `billing_sales_serve_plan_subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `billing_sales_serve_plan_subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `zuora_subscription_id` varchar(32) NOT NULL,
  `zuora_subscription_number` varchar(32) NOT NULL,
  `billing_start_date` date NOT NULL,
  `zuora_rate_plan_charges` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_billing_sales_serve_plan_subscriptions_on_customer_id` (`customer_id`),
  UNIQUE KEY `index_billing_sales_serve_plan_subscriptions_on_zuora_sub_id` (`zuora_subscription_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `billing_transaction_line_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `billing_transaction_line_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `billing_transaction_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  `amount_in_cents` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `arr_in_cents` int(11) DEFAULT NULL,
  `marketplace_listing_id` int(11) DEFAULT NULL,
  `subscribable_id` int(11) DEFAULT NULL,
  `subscribable_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_billing_transaction_line_items_on_billing_transaction_id` (`billing_transaction_id`),
  KEY `index_billing_transaction_line_items_on_marketplace_listing_id` (`marketplace_listing_id`),
  KEY `index_line_items_on_subscribable_and_created_at` (`subscribable_type`,`subscribable_id`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `billing_transaction_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `billing_transaction_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `billing_transaction_id` int(11) NOT NULL,
  `note` text NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_billing_transaction_notes_on_billing_transaction_id` (`billing_transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `billing_transaction_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `billing_transaction_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `billing_transaction_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `amount_in_cents` int(11) NOT NULL,
  `platform_user` varchar(255) DEFAULT NULL,
  `transaction_source` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_billing_transaction_statuses_on_billing_transaction_id` (`billing_transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `billing_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `billing_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `transaction_type` varchar(80) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `amount_in_cents` int(11) DEFAULT NULL,
  `billing_email_address` varchar(255) DEFAULT NULL,
  `country` varchar(80) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `user_type` varchar(30) DEFAULT NULL,
  `user_login` varchar(40) DEFAULT NULL,
  `user_created_at` datetime DEFAULT NULL,
  `plan_name` varchar(80) DEFAULT NULL,
  `old_plan_name` varchar(80) DEFAULT NULL,
  `plan_price_in_cents` int(11) DEFAULT NULL,
  `renewal_frequency` int(11) DEFAULT NULL,
  `old_renewal_frequency` int(11) DEFAULT NULL,
  `coupon_name` varchar(255) DEFAULT NULL,
  `discount_in_cents` int(11) DEFAULT NULL,
  `platform` int(11) DEFAULT '1',
  `service_ends_at` datetime DEFAULT NULL,
  `sale_transaction_id` varchar(255) DEFAULT NULL,
  `last_status` int(11) DEFAULT NULL,
  `country_of_issuance` varchar(20) DEFAULT NULL,
  `bank_identification_number` int(11) DEFAULT NULL,
  `settlement_batch_id` varchar(255) DEFAULT NULL,
  `last_four` varchar(4) DEFAULT NULL,
  `paypal_email` varchar(255) DEFAULT NULL,
  `payment_type` int(11) DEFAULT NULL,
  `seats_delta` int(11) NOT NULL DEFAULT '0',
  `seats_total` int(11) NOT NULL DEFAULT '0',
  `volume_discount` int(11) NOT NULL DEFAULT '0',
  `asset_packs_delta` int(11) NOT NULL DEFAULT '0',
  `asset_packs_total` int(11) NOT NULL DEFAULT '0',
  `asset_pack_unit_price_in_cents` int(11) NOT NULL DEFAULT '0',
  `prorated_days` int(11) NOT NULL DEFAULT '0',
  `volume_discount_in_cents` int(11) NOT NULL DEFAULT '0',
  `product` varchar(30) NOT NULL DEFAULT 'dotcom',
  `arr_in_cents` int(11) DEFAULT NULL,
  `platform_transaction_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_billing_transactions_on_transaction_id` (`transaction_id`),
  KEY `index_billing_transactions_on_created_at` (`created_at`),
  KEY `index_billing_transactions_on_user_id` (`user_id`),
  KEY `index_billing_transactions_on_sale_transaction_id` (`sale_transaction_id`),
  KEY `index_billing_transactions_on_settlement_batch_id` (`settlement_batch_id`),
  KEY `index_billing_transactions_on_last_status_service_ends_at` (`last_status`,`service_ends_at`),
  KEY `index_billing_transactions_on_platform_transaction_id` (`platform_transaction_id`),
  KEY `index_billing_transactions_on_helphub_conditions` (`user_id`,`transaction_type`,`payment_type`,`last_status`,`asset_packs_total`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `blacklisted_payment_methods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blacklisted_payment_methods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `unique_number_identifier` varchar(255) DEFAULT NULL,
  `paypal_email` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_blacklisted_payment_methods_on_identifier_and_user_id` (`unique_number_identifier`,`user_id`),
  UNIQUE KEY `index_blacklisted_payment_methods_on_paypal_email_and_user_id` (`paypal_email`,`user_id`),
  KEY `index_blacklisted_payment_methods_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `braintree_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `braintree_migrations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `orange_id` int(11) unsigned DEFAULT NULL,
  `blue_id` int(11) unsigned DEFAULT NULL,
  `started_at` datetime DEFAULT NULL,
  `completed_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_braintree_migrations_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `bulk_dmca_takedown_repositories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bulk_dmca_takedown_repositories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) DEFAULT NULL,
  `bulk_dmca_takedown_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `bulk_dmca_takedown_repositories_on_repository_id` (`repository_id`),
  KEY `bulk_dmca_takedown_repositories_on_bulk_id` (`bulk_dmca_takedown_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `bulk_dmca_takedowns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bulk_dmca_takedowns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `disabling_user_id` int(11) NOT NULL,
  `notice_public_url` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `bundled_license_assignments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bundled_license_assignments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enterprise_agreement_number` varchar(128) NOT NULL,
  `business_id` int(11) DEFAULT NULL,
  `email` varchar(320) NOT NULL,
  `subscription_id` varchar(36) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_bundled_license_assignments_on_subscription_id` (`subscription_id`),
  KEY `index_bundled_license_assignments_on_enterprise_agreement_number` (`enterprise_agreement_number`),
  KEY `index_bundled_license_assignments_on_business_id_and_email` (`business_id`,`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `business_member_invitations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `business_member_invitations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `business_id` int(11) NOT NULL,
  `inviter_id` int(11) NOT NULL,
  `invitee_id` int(11) DEFAULT NULL,
  `role` int(11) NOT NULL DEFAULT '0',
  `email` varchar(255) DEFAULT NULL,
  `normalized_email` varchar(255) DEFAULT NULL,
  `accepted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `cancelled_at` datetime DEFAULT NULL,
  `expired_at` datetime DEFAULT NULL,
  `hashed_token` varchar(44) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `business_id__accepted_at__cancelled_at__role__invitee_id` (`business_id`,`accepted_at`,`cancelled_at`,`role`,`invitee_id`),
  KEY `index_business_member_invitations_on_created_at` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `business_organization_invitations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `business_organization_invitations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `business_id` int(11) NOT NULL,
  `inviter_id` int(11) NOT NULL,
  `invitee_id` int(11) NOT NULL,
  `accepted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `canceled_at` datetime DEFAULT NULL,
  `confirmed_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_business_organization_invitations_on_invitee_id` (`invitee_id`),
  KEY `index_business_organization_invitations_on_business_id` (`business_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `business_organization_memberships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `business_organization_memberships` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `business_id` int(11) NOT NULL,
  `organization_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_business_organization_memberships_on_organization_id` (`organization_id`),
  KEY `index_on_business_id_and_organization_id` (`business_id`,`organization_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `business_saml_provider_test_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `business_saml_provider_test_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `business_id` int(11) NOT NULL,
  `sso_url` varchar(255) DEFAULT NULL,
  `issuer` varchar(255) DEFAULT NULL,
  `idp_certificate` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `digest_method` int(11) NOT NULL DEFAULT '0',
  `signature_method` int(11) NOT NULL DEFAULT '0',
  `status` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_business_saml_provider_test_settings_on_user_id_business` (`user_id`,`business_id`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `business_saml_providers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `business_saml_providers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `business_id` int(11) NOT NULL,
  `sso_url` varchar(255) NOT NULL,
  `issuer` varchar(255) DEFAULT NULL,
  `idp_certificate` text NOT NULL,
  `disable_admin_demote` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `digest_method` int(11) NOT NULL DEFAULT '0',
  `signature_method` int(11) NOT NULL DEFAULT '0',
  `secret` varchar(255) NOT NULL,
  `recovery_secret` varchar(255) NOT NULL,
  `recovery_used_bitfield` int(11) NOT NULL DEFAULT '0',
  `recovery_codes_viewed` tinyint(1) NOT NULL DEFAULT '0',
  `provisioning_enabled` tinyint(1) NOT NULL DEFAULT '0',
  `scim_deprovisioning_enabled` tinyint(1) NOT NULL DEFAULT '0',
  `saml_deprovisioning_enabled` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_business_saml_providers_on_business_id` (`business_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `business_user_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `business_user_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `business_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `login` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `index_business_user_accounts_on_business_id_user_id_login` (`business_id`,`user_id`,`login`),
  KEY `index_business_user_accounts_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `businesses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `businesses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varbinary(240) NOT NULL,
  `slug` varchar(60) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `terms_of_service_type` varchar(30) NOT NULL DEFAULT 'Corporate',
  `terms_of_service_company_name` varbinary(240) DEFAULT NULL,
  `terms_of_service_notes` varchar(255) DEFAULT NULL,
  `seats` int(11) NOT NULL DEFAULT '0',
  `billing_term_ends_at` datetime DEFAULT NULL,
  `description` varbinary(640) DEFAULT NULL,
  `website_url` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `can_self_serve` tinyint(1) NOT NULL DEFAULT '1',
  `billing_email` varchar(255) DEFAULT NULL,
  `enterprise_web_business_id` int(11) DEFAULT NULL,
  `staff_owned` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_businesses_on_slug` (`slug`),
  UNIQUE KEY `index_businesses_on_enterprise_web_business_id` (`enterprise_web_business_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `cas_mappings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cas_mappings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `username` varchar(80) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_cas_mappings_on_user_id` (`user_id`),
  UNIQUE KEY `index_cas_mappings_on_username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `certificate_trusters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `certificate_trusters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `certificate_id` int(11) NOT NULL,
  `truster_type` varchar(255) NOT NULL,
  `truster_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_certificate_trusters_on_truster_and_certificate` (`truster_type`,`truster_id`,`certificate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `certificates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `certificates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serial` varbinary(20) NOT NULL,
  `issuer_digest` varbinary(32) NOT NULL,
  `subject_digest` varbinary(32) NOT NULL,
  `ski_digest` varbinary(32) DEFAULT NULL,
  `aki_digest` varbinary(32) DEFAULT NULL,
  `der` blob NOT NULL,
  `fingerprint` varbinary(32) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_certificates_on_fingerprint` (`fingerprint`),
  KEY `index_certificates_on_aki_digest` (`aki_digest`),
  KEY `index_certificates_on_issuer_digest_and_serial` (`issuer_digest`,`serial`),
  KEY `index_certificates_on_subject_digest_and_ski_digest` (`subject_digest`,`ski_digest`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `check_annotations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `check_annotations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varbinary(1024) NOT NULL,
  `warning_level` varchar(255) DEFAULT NULL,
  `message` blob NOT NULL,
  `start_line` int(11) NOT NULL,
  `end_line` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `raw_details` text,
  `title` varbinary(1024) DEFAULT NULL,
  `check_run_id` bigint(11) unsigned DEFAULT NULL,
  `start_column` int(11) DEFAULT NULL,
  `end_column` int(11) DEFAULT NULL,
  `suggested_change` blob,
  `check_suite_id` bigint(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_check_annotations_on_check_run_id` (`check_run_id`),
  KEY `index_check_annotations_on_filename` (`filename`),
  KEY `index_check_annotations_on_check_suite_id` (`check_suite_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `check_runs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `check_runs` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `creator_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `conclusion` int(11) DEFAULT NULL,
  `details_url` text,
  `name` varbinary(1024) DEFAULT NULL,
  `started_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `completed_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `external_id` varchar(255) DEFAULT NULL,
  `title` varbinary(1024) DEFAULT NULL,
  `summary` blob,
  `images` text,
  `text` blob,
  `check_suite_id` bigint(11) unsigned NOT NULL,
  `actions` mediumblob,
  `number` int(11) DEFAULT NULL,
  `completed_log_url` varbinary(1024) DEFAULT NULL,
  `completed_log_lines` int(11) DEFAULT NULL,
  `streaming_log_url` text,
  `display_name` varbinary(1024) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_check_runs_on_check_suite_id_and_created_at` (`check_suite_id`,`created_at`),
  KEY `index_check_runs_on_check_suite_id_and_name_and_completed_at` (`check_suite_id`,`name`,`completed_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `check_steps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `check_steps` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `check_run_id` bigint(11) unsigned NOT NULL,
  `number` int(11) NOT NULL,
  `conclusion` int(11) DEFAULT NULL,
  `name` varbinary(1024) NOT NULL,
  `completed_log_url` text,
  `started_at` datetime DEFAULT NULL,
  `completed_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `external_id` varchar(255) DEFAULT NULL,
  `completed_log_lines` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_check_steps_on_check_run_id_and_number` (`check_run_id`,`number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `check_suites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `check_suites` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `push_id` bigint(20) DEFAULT NULL,
  `repository_id` int(11) NOT NULL,
  `github_app_id` int(11) NOT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `conclusion` int(11) DEFAULT NULL,
  `head_branch` varbinary(1024) DEFAULT NULL,
  `head_sha` varchar(64) NOT NULL,
  `rerequestable` tinyint(1) NOT NULL DEFAULT '1',
  `check_runs_rerunnable` tinyint(1) NOT NULL DEFAULT '1',
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `completed_log_url` varbinary(1024) DEFAULT NULL,
  `name` varbinary(1024) DEFAULT NULL,
  `event` varchar(50) DEFAULT NULL,
  `explicit_completion` tinyint(1) NOT NULL DEFAULT '0',
  `head_repository_id` int(11) DEFAULT NULL,
  `workflow_file_path` varbinary(1024) DEFAULT NULL,
  `external_id` varchar(64) DEFAULT NULL COMMENT 'Supplied by creator to allow external systems to work with check suites idempotently',
  `action` varbinary(400) DEFAULT NULL,
  `started_at` datetime DEFAULT NULL,
  `completed_at` datetime DEFAULT NULL,
  `cancelled_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `by_external_id_per_app` (`repository_id`,`github_app_id`,`external_id`),
  KEY `index_check_suites_on_push_id` (`push_id`),
  KEY `index_check_suites_on_head_sha_and_repository_id` (`head_sha`,`repository_id`),
  KEY `index_check_suites_on_repository_id_and_hidden` (`repository_id`,`hidden`),
  KEY `index_check_suites_on_name` (`name`),
  KEY `index_check_suites_on_github_app_id_repository_id` (`github_app_id`,`repository_id`),
  KEY `by_repo_app_and_creator` (`repository_id`,`github_app_id`,`creator_id`),
  KEY `by_repo_app_and_head_branch` (`repository_id`,`github_app_id`,`head_branch`),
  KEY `by_repo_app_and_event` (`repository_id`,`github_app_id`,`event`),
  KEY `by_repo_app_and_status` (`repository_id`,`github_app_id`,`status`),
  KEY `by_repo_app_and_conclusion` (`repository_id`,`github_app_id`,`conclusion`),
  KEY `by_repo_app_and_name` (`repository_id`,`github_app_id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `client_application_sets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_application_sets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `application_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `by_user_and_application` (`user_id`,`application_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `close_issue_references`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `close_issue_references` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pull_request_id` int(11) NOT NULL,
  `issue_id` int(11) NOT NULL,
  `pull_request_author_id` int(11) NOT NULL,
  `issue_repository_id` int(11) NOT NULL,
  `actor_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `source` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_close_issue_references_on_pull_request_id_and_issue_id` (`pull_request_id`,`issue_id`),
  KEY `idx_close_issue_refs_pr_author_issue_id` (`pull_request_author_id`,`issue_id`),
  KEY `index_close_issue_references_on_issue_id_and_issue_repository_id` (`issue_id`,`issue_repository_id`),
  KEY `index_close_issue_references_on_issue_repository_id` (`issue_repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `code_scanning_alerts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `code_scanning_alerts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `check_annotation_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `alert_number` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `check_run_id` bigint(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_code_scanning_alerts_on_check_annotation_id` (`check_annotation_id`),
  KEY `index_code_scanning_alerts_on_repo_and_number_and_check_run` (`repository_id`,`alert_number`,`check_run_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `code_search_indexed_heads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `code_search_indexed_heads` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `commit_oid` varchar(40) NOT NULL,
  `ref` varbinary(767) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_code_search_indexed_heads_on_repository_ref` (`repository_id`,`ref`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `code_symbol_definitions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `code_symbol_definitions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `symbol` varbinary(767) NOT NULL,
  `path` varbinary(767) NOT NULL,
  `line` varbinary(1024) NOT NULL,
  `docs` mediumblob,
  `kind` varchar(20) NOT NULL,
  `row` int(11) NOT NULL,
  `col` int(11) NOT NULL,
  `language_name` varchar(40) NOT NULL,
  `repository_code_symbol_index_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_code_symbol_defs_on_repo_code_symbol_index_sym_kind_path` (`repository_code_symbol_index_id`,`symbol`,`kind`,`path`,`row`,`col`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `codespace_billing_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `codespace_billing_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `billable_owner_type` varchar(30) NOT NULL,
  `billable_owner_id` int(11) NOT NULL,
  `codespace_owner_type` varchar(30) NOT NULL,
  `codespace_owner_id` int(11) NOT NULL,
  `codespace_guid` char(36) NOT NULL,
  `codespace_plan_name` varchar(90) NOT NULL,
  `codespace_created_at` timestamp NULL DEFAULT NULL,
  `codespace_deleted_at` timestamp NULL DEFAULT NULL,
  `codespace_deprovisioned_at` timestamp NULL DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `repository_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_codespaces_billing_on_plan_name_and_guid` (`codespace_plan_name`,`codespace_guid`),
  KEY `idx_codespace_billing_on_billable_owner` (`billable_owner_type`,`billable_owner_id`),
  KEY `idx_codespace_billing_on_codespace_owner` (`codespace_owner_type`,`codespace_owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `codespace_billing_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `codespace_billing_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codespace_plan_id` int(11) NOT NULL,
  `azure_storage_account_name` varchar(24) NOT NULL,
  `event_id` varchar(36) NOT NULL,
  `payload` json NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_codespace_billing_message_on_event_id` (`event_id`),
  KEY `idx_codespace_billing_message_on_codespace_plan` (`codespace_plan_id`),
  KEY `idx_codespace_billing_message_on_plan_and_created_at` (`codespace_plan_id`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `codespaces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `codespaces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `pull_request_id` int(11) DEFAULT NULL,
  `guid` char(36) DEFAULT NULL,
  `name` varchar(90) NOT NULL,
  `slug` varchar(100) DEFAULT NULL,
  `oid` char(40) NOT NULL,
  `ref` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `last_used_at` datetime DEFAULT NULL,
  `plan_id` int(11) DEFAULT NULL,
  `location` varchar(40) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_codespaces_on_owner_id_and_name` (`owner_id`,`name`),
  UNIQUE KEY `index_codespaces_on_repository_id_and_owner_id_and_name` (`repository_id`,`owner_id`,`name`),
  UNIQUE KEY `index_codespaces_on_name` (`name`),
  UNIQUE KEY `index_codespaces_on_guid_and_owner_id` (`guid`,`owner_id`),
  UNIQUE KEY `index_codespaces_on_slug_and_owner_id` (`slug`,`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `codespaces_compute_usage_line_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `codespaces_compute_usage_line_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `billable_owner_type` varchar(12) NOT NULL,
  `billable_owner_id` int(11) NOT NULL,
  `directly_billed` tinyint(1) NOT NULL DEFAULT '1',
  `unique_billing_identifier` varchar(36) NOT NULL,
  `sku` varchar(20) NOT NULL,
  `duration_multiplier` decimal(4,2) NOT NULL,
  `duration_in_seconds` int(11) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `synchronization_batch_id` int(11) DEFAULT NULL,
  `submission_state` enum('unsubmitted','submitted','skipped') NOT NULL DEFAULT 'unsubmitted',
  `submission_state_reason` varchar(24) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `computed_usage` decimal(16,5) NOT NULL DEFAULT '0.00000',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_on_unique_billing_identifier` (`unique_billing_identifier`),
  KEY `index_codespaces_compute_usage_line_items_on_owner_id` (`owner_id`),
  KEY `index_codespaces_compute_usage_line_items_on_actor_id` (`actor_id`),
  KEY `index_codespaces_compute_usage_line_items_on_start_time` (`start_time`),
  KEY `index_codespaces_compute_usage_line_items_on_end_time` (`end_time`),
  KEY `index_on_synchronization_batch_id` (`synchronization_batch_id`),
  KEY `index_on_billable_owner_and_usage` (`billable_owner_type`,`billable_owner_id`,`duration_in_seconds`),
  KEY `index_on_submission_state_and_reason` (`submission_state`,`submission_state_reason`),
  KEY `index_on_submission_state_and_created_at` (`submission_state`,`created_at`),
  KEY `index_codespaces_compute_usage_line_items_on_sku_computed_usage` (`billable_owner_type`,`billable_owner_id`,`owner_id`,`end_time`,`sku`,`computed_usage`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `codespaces_storage_usage_line_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `codespaces_storage_usage_line_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `billable_owner_type` varchar(12) NOT NULL,
  `billable_owner_id` int(11) NOT NULL,
  `directly_billed` tinyint(1) NOT NULL DEFAULT '1',
  `unique_billing_identifier` varchar(36) NOT NULL,
  `synchronization_batch_id` int(11) DEFAULT NULL,
  `submission_state` enum('unsubmitted','submitted','skipped') NOT NULL DEFAULT 'unsubmitted',
  `submission_state_reason` varchar(24) DEFAULT NULL,
  `size_in_bytes` bigint(20) NOT NULL,
  `duration_in_seconds` int(11) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `computed_usage` decimal(16,3) NOT NULL DEFAULT '0.000',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_on_unique_billing_identifier` (`unique_billing_identifier`),
  KEY `index_codespaces_storage_usage_line_items_on_owner_id` (`owner_id`),
  KEY `index_codespaces_storage_usage_line_items_on_actor_id` (`actor_id`),
  KEY `index_on_synchronization_batch_id` (`synchronization_batch_id`),
  KEY `index_on_billable_owner` (`billable_owner_type`,`billable_owner_id`),
  KEY `index_on_submission_state_and_reason` (`submission_state`,`submission_state_reason`),
  KEY `index_on_submission_state_and_created_at` (`submission_state`,`created_at`),
  KEY `index_codespaces_storage_usage_line_items_on_computed_usage` (`billable_owner_type`,`billable_owner_id`,`owner_id`,`end_time`,`computed_usage`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `cold_networks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cold_networks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `network_id` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_cold_networks_on_network_id` (`network_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `collection_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `collection_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `collection_id` int(11) NOT NULL,
  `content_id` int(11) NOT NULL,
  `content_type` varchar(30) NOT NULL,
  `slug` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_collection_items_on_collection_id` (`collection_id`),
  KEY `index_collection_items_on_content_id_and_content_type` (`content_id`,`content_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `collection_urls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `collection_urls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` text NOT NULL,
  `title` varchar(40) NOT NULL,
  `description` varbinary(1024) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `collection_videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `collection_videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` text NOT NULL,
  `title` varchar(40) NOT NULL,
  `description` varbinary(1024) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `thumbnail_url` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `collections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `collections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(40) NOT NULL,
  `description` varbinary(1024) DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `attribution_url` text,
  `display_name` varchar(100) NOT NULL,
  `image_url` text,
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_collections_on_slug` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `comment_emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment_emails` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` int(11) unsigned DEFAULT NULL,
  `comment_type` varchar(255) DEFAULT NULL,
  `message_id` varchar(255) DEFAULT NULL,
  `created_at` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_comment_emails_on_created_at` (`created_at`),
  KEY `index_comment_emails_on_comment_id_and_comment_type` (`comment_id`,`comment_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `commit_comment_edits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commit_comment_edits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `commit_comment_id` int(11) NOT NULL,
  `edited_at` datetime NOT NULL,
  `editor_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by_id` int(11) DEFAULT NULL,
  `diff` mediumblob,
  `user_content_edit_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_commit_comment_edits_on_user_content_edit_id` (`user_content_edit_id`),
  KEY `index_commit_comment_edits_on_commit_comment_id` (`commit_comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `commit_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commit_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `body` mediumblob,
  `commit_id` varchar(40) NOT NULL,
  `path` varbinary(1024) DEFAULT NULL,
  `repository_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `line` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `formatter` varchar(30) DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `comment_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `comment_hidden_reason` varbinary(1024) DEFAULT NULL,
  `comment_hidden_classifier` varchar(50) DEFAULT NULL,
  `comment_hidden_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_commit_comments_on_repository_id_and_user_id` (`repository_id`,`user_id`),
  KEY `index_commit_comments_on_user_hidden_and_user_id` (`user_hidden`,`user_id`),
  KEY `index_commit_comments_on_repo_commit_ids_and_user_hidden` (`repository_id`,`commit_id`,`user_hidden`),
  KEY `index_commit_comments_on_repo_id_and_user_hidden_and_user_id` (`repository_id`,`user_hidden`,`user_id`),
  KEY `index_commit_comments_on_commit_id` (`commit_id`),
  KEY `index_commit_comments_on_user_id_and_created_at` (`user_id`,`created_at`),
  KEY `index_commit_comments_on_user_id_and_updated_at` (`user_id`,`updated_at`),
  KEY `index_commit_comments_on_user_id_and_repository_id` (`user_id`,`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `commit_contributions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commit_contributions` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `repository_id` int(11) unsigned DEFAULT NULL,
  `commit_count` int(11) unsigned DEFAULT '0',
  `committed_date` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `commit_contrib_user_repo_date` (`user_id`,`repository_id`,`committed_date`),
  KEY `index_commit_contributions_on_user_id_and_committed_date` (`user_id`,`committed_date`),
  KEY `index_commit_contributions_on_repository_id_and_committed_date` (`repository_id`,`committed_date`),
  KEY `index_commit_contributions_on_repository_and_user_and_date` (`repository_id`,`user_id`,`committed_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `commit_mentions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commit_mentions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) unsigned DEFAULT NULL,
  `commit_id` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_commit_mentions_on_commit_id_and_repository_id` (`commit_id`,`repository_id`),
  KEY `index_commit_mentions_on_repository_id` (`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `community_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `community_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `has_code_of_conduct` tinyint(1) DEFAULT NULL,
  `repository_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `has_contributing` tinyint(1) DEFAULT NULL,
  `has_license` tinyint(1) DEFAULT NULL,
  `has_readme` tinyint(1) DEFAULT NULL,
  `has_outside_contributors` tinyint(1) DEFAULT NULL,
  `detected_code_of_conduct` varchar(255) DEFAULT NULL,
  `help_wanted_issues_count` int(11) NOT NULL DEFAULT '0',
  `good_first_issue_issues_count` int(11) NOT NULL DEFAULT '0',
  `has_docs` tinyint(1) NOT NULL DEFAULT '0',
  `has_description` tinyint(1) NOT NULL DEFAULT '0',
  `has_issue_opened_by_non_collaborator` tinyint(1) NOT NULL DEFAULT '0',
  `has_pr_or_issue_template` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_community_profiles_on_repository_id` (`repository_id`),
  KEY `index_community_profiles_on_has_code_of_conduct` (`has_code_of_conduct`),
  KEY `index_community_profiles_on_detected_code_of_conduct` (`detected_code_of_conduct`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varbinary(1024) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_companies_on_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `companies_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companies_users` (
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  UNIQUE KEY `index_companies_users_on_company_id_and_user_id` (`company_id`,`user_id`),
  KEY `index_companies_users_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `composable_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `composable_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `integration_id` int(11) NOT NULL,
  `issue_id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_composable_comments_on_issue_id` (`issue_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `compromised_password_datasources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compromised_password_datasources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `version` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `import_finished_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_compromised_password_datasources_on_name_and_version` (`name`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `compromised_password_datasources_passwords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compromised_password_datasources_passwords` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `compromised_password_id` bigint(20) NOT NULL,
  `compromised_password_datasource_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_on_password_and_datasource` (`compromised_password_id`,`compromised_password_datasource_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `compromised_passwords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compromised_passwords` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sha1_password` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_compromised_passwords_on_sha1_password` (`sha1_password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `configuration_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuration_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `target_id` int(11) NOT NULL,
  `target_type` varchar(30) NOT NULL,
  `updater_id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  `value` varchar(255) NOT NULL,
  `final` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_configuration_entries_on_target_and_name` (`target_id`,`target_type`,`name`),
  KEY `index_on_target_type_and_target_id_and_name_and_value_and_final` (`target_type`,`target_id`,`name`,`value`,`final`),
  KEY `index_configuration_entries_on_name_and_target_type_and_value` (`name`,`target_type`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `content_reference_attachments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_reference_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_reference_id` int(11) DEFAULT NULL,
  `integration_id` int(11) DEFAULT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `title` varbinary(1024) DEFAULT NULL,
  `body` mediumblob,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_on_content_reference_id_and_integration_id` (`content_reference_id`,`integration_id`),
  KEY `index_on_content_reference_id_and_state` (`content_reference_id`,`state`),
  KEY `index_content_reference_attachments_on_integration_id` (`integration_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `content_references`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_references` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `content_id` int(11) NOT NULL,
  `content_type` varchar(30) NOT NULL,
  `reference_hash` varchar(32) NOT NULL,
  `reference` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_on_content_id_and_content_type_and_reference_hash` (`content_id`,`content_type`,`reference_hash`),
  KEY `index_content_references_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `conversations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conversations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `subject_type` varchar(30) NOT NULL,
  `context_id` int(11) NOT NULL,
  `context_type` varchar(30) NOT NULL,
  `state` varchar(30) NOT NULL DEFAULT 'published',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_conversations_on_subject_type_and_subject_id` (`subject_type`,`subject_id`),
  KEY `index_conversations_on_context_id_and_context_type` (`context_id`,`context_type`),
  KEY `index_conversations_on_number` (`number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `coupon_redemptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coupon_redemptions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `coupon_id` int(11) unsigned DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  `expired` tinyint(1) unsigned DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_coupon_redemptions_on_expires_at` (`expires_at`),
  KEY `index_coupon_redemptions_on_user_id_and_coupon_id` (`user_id`,`coupon_id`),
  KEY `index_coupon_redemptions_on_expired` (`expired`),
  KEY `index_coupon_redemptions_on_coupon_id` (`coupon_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `coupons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coupons` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `plan` varchar(255) DEFAULT NULL,
  `discount` float DEFAULT NULL,
  `duration` int(11) unsigned DEFAULT NULL,
  `limit` int(11) unsigned DEFAULT NULL,
  `group` varchar(255) DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  `note` text,
  `staff_actor_only` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_coupons_on_code` (`code`),
  KEY `index_coupons_on_group` (`group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `credentials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credentials` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `value` mediumblob NOT NULL,
  `owner_type` varchar(255) DEFAULT NULL,
  `owner_id` varbinary(1000) DEFAULT NULL,
  `integration` varchar(300) DEFAULT NULL,
  `name` varchar(300) DEFAULT 'DEFAULT',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `visibility` int(11) DEFAULT '0' COMMENT 'See CredentialVisibility for values',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_owner_type_owner_id_integration_name` (`owner_type`,`owner_id`(200),`integration`(200),`name`(200)),
  KEY `by_owner_type_owner_id_visibility` (`owner_type`,`owner_id`(200),`visibility`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `credentials_visibility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credentials_visibility` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `credential_id` bigint(20) NOT NULL,
  `repository_node_id` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `by_credential_id_repository_node_id` (`credential_id`,`repository_node_id`),
  KEY `by_repository_node_id` (`repository_node_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `cross_references`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cross_references` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source_id` int(11) NOT NULL,
  `source_type` varchar(255) NOT NULL,
  `target_id` int(11) NOT NULL,
  `target_type` varchar(255) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `referenced_at` datetime DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_discussion_references_natural_key_unique` (`source_id`,`source_type`,`target_id`,`target_type`),
  KEY `index_discussion_references_on_target_id_and_target_type` (`target_id`,`target_type`),
  KEY `index_cross_references_on_user_hidden_and_actor_id` (`user_hidden`,`actor_id`),
  KEY `index_cross_references_on_actor_id` (`actor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `custom_inboxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_inboxes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varbinary(1024) NOT NULL,
  `query_string` varbinary(1024) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_custom_inboxes_on_user_id_and_name` (`user_id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `custom_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sign_in_message` mediumblob,
  `suspended_message` mediumblob,
  `support_url` text,
  `announcement` mediumblob,
  `singleton_guard` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `auth_provider_name` varchar(50) DEFAULT NULL,
  `sign_out_message` mediumblob,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_custom_messages_on_singleton_guard` (`singleton_guard`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `customer_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `verification_token` varchar(20) NOT NULL,
  `verification_confirmed_at` datetime DEFAULT NULL,
  `verified_by_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_customer_accounts_on_user_id` (`user_id`),
  KEY `index_customer_accounts_on_customer_id` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_customer_id` int(11) DEFAULT NULL,
  `name` varbinary(1024) NOT NULL,
  `external_uuid` varchar(36) NOT NULL,
  `seats` int(11) NOT NULL DEFAULT '0',
  `custom_volume_discount` int(11) NOT NULL DEFAULT '0',
  `braintree_customer_id` varchar(30) DEFAULT NULL,
  `emails` text,
  `billing_extra` text,
  `billing_instructions` text,
  `bill_to` varchar(100) DEFAULT NULL,
  `street_address` varchar(100) DEFAULT NULL,
  `country_code_alpha2` varchar(3) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `vat_code` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `zuora_account_id` varchar(32) DEFAULT NULL,
  `salesforce_account_id` varchar(18) DEFAULT NULL,
  `zuora_account_number` varchar(32) DEFAULT NULL,
  `billing_end_date` datetime DEFAULT NULL,
  `billing_start_date` datetime DEFAULT NULL,
  `billing_attempts` int(11) DEFAULT NULL,
  `billing_type` varchar(20) DEFAULT NULL,
  `billing_email_id` int(11) DEFAULT NULL,
  `term_length` int(11) DEFAULT NULL,
  `bill_cycle_day` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_customers_on_external_uuid` (`external_uuid`),
  UNIQUE KEY `index_customers_on_salesforce_account_id` (`salesforce_account_id`),
  UNIQUE KEY `index_customers_on_zuora_account_number` (`zuora_account_number`),
  KEY `index_customers_on_zuora_account_id` (`zuora_account_id`),
  KEY `index_customers_on_braintree_customer_id` (`braintree_customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `dashboard_notices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboard_notices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `notice_name` varchar(25) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_by_user_and_notice_name` (`user_id`,`notice_name`),
  KEY `by_user_and_active_flag` (`user_id`,`active`),
  KEY `index_dashboard_notices_on_notice_name_and_id_and_user_id` (`notice_name`,`id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `datacenters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datacenters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datacenter` varchar(8) NOT NULL,
  `region` varchar(8) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_datacenters_on_datacenter` (`datacenter`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `deceased_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deceased_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_deceased_users_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `default_integration_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `default_integration_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `resource` varchar(40) NOT NULL,
  `action` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `integration_id` int(11) NOT NULL,
  `integration_version_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_integration_permissions_on_version_and_resource` (`integration_version_id`,`resource`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `delegated_recovery_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `delegated_recovery_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hashed_token` varchar(44) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `token_id` varchar(32) DEFAULT NULL,
  `provider` varchar(255) DEFAULT NULL,
  `confirmed_at` datetime DEFAULT NULL,
  `recovered_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `key_version_id` bigint(20) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_delegated_recovery_tokens_on_token_id` (`token_id`),
  KEY `index_delegated_recovery_tokens_on_token_id_and_user_id` (`token_id`,`user_id`),
  KEY `index_delegated_recovery_tokens_on_confirmed_at_and_created_at` (`confirmed_at`,`created_at`),
  KEY `index_on_user_id_and_provider_and_confirmed_at` (`user_id`,`provider`,`confirmed_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `deleted_discussions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deleted_discussions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `deleted_by_id` int(11) NOT NULL,
  `old_discussion_id` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_deleted_discussions_on_repository_id_and_number` (`repository_id`,`number`),
  KEY `index_deleted_discussions_on_old_discussion_id_and_repository_id` (`old_discussion_id`,`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `deleted_issues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deleted_issues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `deleted_by_id` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `old_issue_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_deleted_issues_on_repository_id_and_number` (`repository_id`,`number`),
  KEY `index_deleted_issues_on_old_issue_id_and_repository_id` (`old_issue_id`,`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `deployment_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deployment_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(25) NOT NULL DEFAULT 'unknown',
  `description` text,
  `target_url` varbinary(1024) DEFAULT NULL,
  `deployment_id` int(11) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `environment_url` text,
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `environment` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_deployment_statuses_on_deployment_id` (`deployment_id`),
  KEY `index_deployment_statuses_on_deployment_id_and_environment` (`deployment_id`,`environment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `deployments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deployments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` blob,
  `payload` mediumblob,
  `sha` varchar(40) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `ref` varchar(255) DEFAULT NULL,
  `environment` varchar(255) DEFAULT 'production',
  `task` varchar(128) DEFAULT 'deploy',
  `transient_environment` tinyint(1) NOT NULL DEFAULT '0',
  `production_environment` tinyint(1) NOT NULL DEFAULT '0',
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `latest_deployment_status_id` int(11) DEFAULT NULL,
  `latest_environment` varchar(255) DEFAULT NULL,
  `latest_status_state` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_deployments_on_repository_id_and_created_at` (`repository_id`,`created_at`),
  KEY `index_deployments_on_sha` (`sha`),
  KEY `index_deployments_on_repository_environment_prod_and_transient` (`repository_id`,`environment`,`production_environment`,`transient_environment`),
  KEY `index_deployments_on_repository_and_sha` (`repository_id`,`sha`),
  KEY `index_deployments_on_repository_latest_env_created_at` (`repository_id`,`latest_environment`,`created_at`),
  KEY `index_deployments_on_sha_repository_latest_env_created_at` (`repository_id`,`sha`,`latest_environment`,`created_at`),
  KEY `index_deployments_on_repository_latest_status_state` (`repository_id`,`latest_environment`,`latest_status_state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `developer_program_memberships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `developer_program_memberships` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `support_email` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `display_badge` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_developer_program_memberships_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `device_authorization_grants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_authorization_grants` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `access_denied` tinyint(1) NOT NULL DEFAULT '0',
  `application_id` int(11) NOT NULL,
  `application_type` varchar(16) NOT NULL,
  `hashed_device_code` varchar(44) DEFAULT NULL,
  `device_code_last_eight` varchar(8) DEFAULT NULL,
  `scopes` text,
  `user_code` varchar(9) NOT NULL,
  `oauth_access_id` int(11) DEFAULT NULL,
  `expires_at` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `ip` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_device_authorization_grants_on_user_code` (`user_code`),
  UNIQUE KEY `index_device_authorization_grants_on_hashed_device_code` (`hashed_device_code`),
  UNIQUE KEY `index_device_authorization_grants_on_oauth_access_id` (`oauth_access_id`),
  KEY `index_device_authorization_grants_on_expires_at` (`expires_at`),
  KEY `index_device_authorization_grants_on_application` (`application_id`,`application_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `dg_abstract_package_dependencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dg_abstract_package_dependencies` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dependent_id` int(11) DEFAULT NULL,
  `package_manager` int(11) DEFAULT NULL,
  `package_name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_abstract_package_dep_uniq_package` (`package_name`,`dependent_id`,`package_manager`),
  KEY `index_dg_abstract_package_dependencies_on_dependent_id` (`dependent_id`),
  KEY `abstract_package_dep_lookups` (`package_name`,`package_manager`,`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `dg_abstract_package_dependency_counts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dg_abstract_package_dependency_counts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_name` varchar(255) NOT NULL,
  `package_manager` int(11) NOT NULL,
  `dependent_count` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_abstract_package_dep_count_uniq` (`package_name`,`package_manager`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `dg_abstract_repository_dependencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dg_abstract_repository_dependencies` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `package_manager` int(11) NOT NULL,
  `package_name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_abstract_repo_dep_uniq_package` (`package_name`,`repository_id`,`package_manager`),
  KEY `index_dg_abstract_repository_dependencies_on_repository_id` (`repository_id`),
  KEY `abstract_repo_dep_lookups` (`package_name`,`package_manager`,`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `dg_abstract_repository_dependency_counts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dg_abstract_repository_dependency_counts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_name` varchar(255) NOT NULL,
  `package_manager` int(11) NOT NULL,
  `dependent_count` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_abstract_repo_dep_count_uniq` (`package_name`,`package_manager`),
  KEY `index_dg_abstract_repository_dependency_counts_on_package_name` (`package_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `dg_ar_internal_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dg_ar_internal_metadata` (
  `key` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `dg_checkpoints`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dg_checkpoints` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `last_checkpointed_id` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_dg_checkpoints_on_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `dg_dep_insights_backfills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dg_dep_insights_backfills` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `github_owner_id` bigint(20) DEFAULT NULL,
  `last_backfilled_at` datetime DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_dg_dep_insights_backfills_on_github_owner_id` (`github_owner_id`),
  KEY `index_dg_dep_insights_backfills_on_last_backfilled_at` (`last_backfilled_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `dg_dependency_specifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dg_dependency_specifications` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `requirements` varchar(255) NOT NULL,
  `dependent_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `scope` int(11) DEFAULT '1',
  `encoded_lower_bound` bigint(20) DEFAULT NULL,
  `encoded_upper_bound` bigint(20) DEFAULT NULL,
  `package_manager` int(11) DEFAULT NULL,
  `package_name` varchar(255) NOT NULL,
  `dependent_type_id` int(11) DEFAULT NULL,
  `package_label` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_dep_spec_on_dependent_id_and_type_and_package_name` (`dependent_id`,`package_name`),
  KEY `index_dg_dependency_specifications_on_dependent_id` (`dependent_id`),
  KEY `index_dg_dependency_specifications_on_package_name` (`package_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `dg_etl_imports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dg_etl_imports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_manager` int(11) DEFAULT NULL,
  `metadata` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `stage` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `dg_failed_manifest_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dg_failed_manifest_messages` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `source` varchar(255) DEFAULT NULL,
  `github_repository_id` int(11) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`id`),
  KEY `index_dg_failed_manifest_messages_on_github_repository_id` (`github_repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `dg_key_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dg_key_values` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `value` blob NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_dg_key_values_on_key` (`key`),
  KEY `index_dg_key_values_on_expires_at` (`expires_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `dg_manifest_dependencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dg_manifest_dependencies` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `manifest_id` int(11) NOT NULL,
  `requirements` varchar(255) NOT NULL,
  `package_name` varchar(255) NOT NULL,
  `scope` int(11) DEFAULT '1',
  `encoded_lower_bound` bigint(20) DEFAULT NULL,
  `encoded_upper_bound` bigint(20) DEFAULT NULL,
  `last_seen_at_revision` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `package_label` varchar(255) DEFAULT NULL,
  `package_manager` int(11) DEFAULT NULL,
  `exact_version` varchar(255) DEFAULT NULL,
  `raw_requirements` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `manifest_dep_spec_package_name` (`manifest_id`,`package_name`),
  KEY `index_dg_manifest_dependencies_on_package_name_and_id` (`package_name`),
  KEY `index_manifest_dep_spec_version_ranges` (`package_name`,`encoded_lower_bound`,`encoded_upper_bound`),
  KEY `index_dg_manifest_dependencies_on_manifest_pkg_mgr_name_version` (`manifest_id`,`package_manager`,`package_name`,`exact_version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `dg_manifests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dg_manifests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) DEFAULT NULL,
  `manifest_type` int(11) NOT NULL,
  `package_manager` int(11) NOT NULL,
  `revision` int(11) NOT NULL DEFAULT '0',
  `latest_git_ref` varchar(255) NOT NULL,
  `last_pushed_at` datetime NOT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_unique_manifests` (`repository_id`,`manifest_type`,`path`,`filename`),
  KEY `index_dg_manifests_on_name` (`name`),
  KEY `index_dg_manifests_on_package_manager_and_name` (`package_manager`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `dg_package_release_dependent_counts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dg_package_release_dependent_counts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `github_owner_id` int(11) NOT NULL,
  `package_release_id` bigint(20) NOT NULL,
  `count` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_dg_package_counts` (`github_owner_id`,`package_release_id`),
  KEY `index_dg_package_release_dependent_counts_on_package_release_id` (`package_release_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `dg_package_release_vuln_counts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dg_package_release_vuln_counts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `package_release_id` bigint(20) NOT NULL,
  `total_count` int(11) NOT NULL DEFAULT '0',
  `critical_count` int(11) NOT NULL DEFAULT '0',
  `high_count` int(11) NOT NULL DEFAULT '0',
  `moderate_count` int(11) NOT NULL DEFAULT '0',
  `low_count` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_dg_package_release_vuln_counts_on_package_release_id` (`package_release_id`),
  KEY `index_dg_package_release_vuln_counts` (`package_release_id`,`total_count`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `dg_package_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dg_package_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `external_id` int(11) DEFAULT NULL,
  `repository_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `encoded` bigint(20) DEFAULT NULL,
  `published_at` datetime DEFAULT NULL,
  `pushed_at` datetime DEFAULT NULL,
  `unpublished_at` datetime DEFAULT NULL,
  `repository_id_certainty` int(11) NOT NULL DEFAULT '0',
  `repository_nwo` varchar(255) DEFAULT NULL,
  `license` varchar(255) DEFAULT NULL,
  `clearly_defined_score` int(11) DEFAULT NULL,
  `package_manager` int(11) DEFAULT NULL,
  `package_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_dg_package_versions_on_package_id_and_name` (`package_id`,`name`),
  KEY `index_dg_package_versions_on_package_id` (`package_id`),
  KEY `index_dg_package_versions_on_name` (`name`),
  KEY `index_dg_package_versions_on_repository_id` (`repository_id`),
  KEY `index_dg_package_versions_on_package_id_and_encoded` (`package_id`,`encoded`),
  KEY `index_dg_package_versions_on_repository_nwo` (`repository_nwo`),
  KEY `index_dg_package_versions_on_license` (`license`),
  KEY `index_dg_package_versions_on_package_name_package_manager_name` (`package_name`,`package_manager`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `dg_packages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dg_packages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `package_manager` int(11) DEFAULT NULL,
  `repository_id` int(11) DEFAULT NULL,
  `repository_id_certainty` int(11) NOT NULL DEFAULT '0',
  `last_published_at` datetime DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `repository_nwo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_dg_packages_on_name_and_package_manager` (`name`,`package_manager`),
  KEY `index_dg_packages_on_last_published_at` (`last_published_at`),
  KEY `index_dg_packages_on_repository_id` (`repository_id`),
  KEY `index_dg_packages_on_package_manager_and_repository_id` (`package_manager`,`repository_id`),
  KEY `index_dg_packages_on_repository_nwo` (`repository_nwo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `dg_repositories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dg_repositories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `github_repository_id` int(11) NOT NULL,
  `public` tinyint(1) DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `nwo` varchar(255) DEFAULT NULL,
  `github_owner_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_dg_repositories_on_github_repository_id` (`github_repository_id`),
  KEY `index_dg_repositories_on_nwo_and_public` (`nwo`,`public`),
  KEY `index_dg_repositories_on_github_repository_id_and_public` (`github_repository_id`,`public`),
  KEY `index_dg_repositories_on_github_owner_id` (`github_owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `dg_schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dg_schema_migrations` (
  `version` varchar(255) NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `dg_star_counts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dg_star_counts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `github_repository_id` int(11) NOT NULL,
  `star_count` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_dg_star_counts_on_github_repository_id` (`github_repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `dg_transitive_package_dependencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dg_transitive_package_dependencies` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `package_id` bigint(20) NOT NULL,
  `dependency_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_dg_transitive_package_dependencies_on_package_id` (`package_id`),
  KEY `index_dg_transitive_package_dependenices_on_deps` (`dependency_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `dg_vulnerable_version_ranges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dg_vulnerable_version_ranges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `github_id` int(11) NOT NULL,
  `package_name` varchar(255) NOT NULL,
  `package_manager` varchar(255) NOT NULL,
  `version_range` varchar(255) NOT NULL,
  `encoded_lower_bound` bigint(20) NOT NULL,
  `encoded_upper_bound` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `severity` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_dg_vulnerable_version_ranges_on_github_id` (`github_id`),
  KEY `index_dg_vuln_version_ranges_on_package_name_and_encoded_bounds` (`package_name`,`encoded_lower_bound`,`encoded_upper_bound`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `disabled_access_reasons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `disabled_access_reasons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dmca_takedown_url` varchar(255) DEFAULT NULL,
  `country_block` varchar(255) DEFAULT NULL,
  `country_block_url` varchar(255) DEFAULT NULL,
  `flagged_item_id` int(11) NOT NULL,
  `flagged_item_type` varchar(255) NOT NULL,
  `disabled_at` datetime NOT NULL,
  `disabled_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_disabled_access_reasons_on_dmca_takedown_url` (`dmca_takedown_url`),
  KEY `index_disabled_access_reasons_on_country_block` (`country_block`,`country_block_url`),
  KEY `index_disabled_access_reasons_on_flagged_item` (`flagged_item_id`,`flagged_item_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `disabled_backups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `disabled_backups` (
  `reason` varchar(255) NOT NULL,
  `disabled_at` datetime NOT NULL,
  `spec` varchar(255) NOT NULL,
  PRIMARY KEY (`spec`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `discussion_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discussion_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `emoji` varbinary(44) NOT NULL DEFAULT ':hash:',
  `name` varbinary(512) NOT NULL,
  `description` mediumblob,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_discussion_categories_on_repository_id_and_name` (`repository_id`,`name`),
  UNIQUE KEY `index_discussion_categories_on_repository_id_and_slug` (`repository_id`,`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `discussion_comment_edits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discussion_comment_edits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `discussion_comment_id` int(11) NOT NULL,
  `edited_at` datetime NOT NULL,
  `editor_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by_id` int(11) DEFAULT NULL,
  `diff` mediumblob,
  PRIMARY KEY (`id`),
  KEY `index_discussion_comment_edits_on_discussion_comment_id` (`discussion_comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `discussion_comment_reactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discussion_comment_reactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `discussion_comment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `content` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_reactions_comment_identity` (`user_id`,`discussion_comment_id`,`content`),
  KEY `index_discussion_comment_reactions_on_discussion_comment_id` (`discussion_comment_id`),
  KEY `reactions_on_discussion_comment_content_created` (`discussion_comment_id`,`content`,`created_at`),
  KEY `reactions_on_discussion_comment_hidden_created` (`discussion_comment_id`,`user_hidden`,`created_at`),
  KEY `comment_reactions_on_user_hidden_and_user_id` (`user_id`,`user_hidden`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `discussion_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discussion_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `discussion_id` int(11) NOT NULL,
  `parent_comment_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `body` mediumblob NOT NULL,
  `user_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `comment_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `comment_hidden_reason` varbinary(1024) DEFAULT NULL,
  `comment_hidden_classifier` varchar(50) DEFAULT NULL,
  `comment_hidden_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `repository_id` int(11) DEFAULT NULL,
  `score` int(11) NOT NULL DEFAULT '0',
  `formatter` varchar(20) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_discussion_comments_on_user_id_and_user_hidden` (`user_id`,`user_hidden`),
  KEY `index_discussion_comments_on_repository_id_and_created_at` (`repository_id`,`created_at`),
  KEY `index_discussion_comments_on_parent_comment_id_and_discussion_id` (`parent_comment_id`,`discussion_id`),
  KEY `idx_discussion_comments_on_user_discussion_user_hidden_deleted` (`user_id`,`discussion_id`,`user_hidden`,`deleted_at`),
  KEY `index_discussion_comments_disc_id_user_id_user_hidden_created_at` (`discussion_id`,`user_id`,`user_hidden`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `discussion_edits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discussion_edits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `discussion_id` int(11) NOT NULL,
  `edited_at` datetime NOT NULL,
  `editor_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by_id` int(11) DEFAULT NULL,
  `diff` mediumblob,
  PRIMARY KEY (`id`),
  KEY `index_discussion_edits_on_discussion_id` (`discussion_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `discussion_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discussion_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `discussion_id` int(11) NOT NULL,
  `actor_id` int(11) DEFAULT NULL,
  `comment_id` int(11) DEFAULT NULL,
  `event_type` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_discussion_events_on_repo_discussion_type_created_at` (`repository_id`,`discussion_id`,`event_type`,`created_at`),
  KEY `index_discussion_events_on_actor_id` (`actor_id`),
  KEY `index_discussion_events_on_comment_id` (`comment_id`),
  KEY `index_on_discussion_id_event_type_comment_id_created_at` (`discussion_id`,`event_type`,`comment_id`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `discussion_post_replies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discussion_post_replies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `body` mediumblob NOT NULL,
  `formatter` varchar(20) DEFAULT NULL,
  `number` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `discussion_post_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_discussion_post_replies_on_discussion_post_id_and_number` (`discussion_post_id`,`number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `discussion_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discussion_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `body` mediumblob NOT NULL,
  `formatter` varchar(20) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `number` int(11) DEFAULT NULL,
  `pinned_at` datetime DEFAULT NULL,
  `pinned_by_user_id` int(11) DEFAULT NULL,
  `private` tinyint(1) NOT NULL DEFAULT '0',
  `title` varbinary(1024) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_discussion_posts_on_team_id_and_number` (`team_id`,`number`),
  KEY `index_discussion_posts_on_team_id_and_pinned_at` (`team_id`,`pinned_at`),
  KEY `index_discussion_posts_on_team_id_and_private` (`team_id`,`private`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `discussion_reactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discussion_reactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `discussion_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `content` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_reactions_identity` (`user_id`,`discussion_id`,`content`),
  KEY `index_discussion_reactions_on_discussion_id` (`discussion_id`),
  KEY `index_reactions_on_discussion_content_created_at` (`discussion_id`,`content`,`created_at`),
  KEY `reactions_on_discussion_user_hidden_created_at` (`discussion_id`,`user_hidden`,`created_at`),
  KEY `index_reactions_on_user_hidden_and_user_id` (`user_id`,`user_hidden`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `discussion_spotlights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discussion_spotlights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `discussion_id` int(11) NOT NULL,
  `spotlighted_by_id` int(11) NOT NULL,
  `position` tinyint(4) NOT NULL DEFAULT '1',
  `emoji` varbinary(44) DEFAULT NULL,
  `preconfigured_color` tinyint(4) DEFAULT NULL,
  `custom_color` varchar(10) DEFAULT NULL,
  `pattern` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_discussion_spotlights_on_repository_id_and_discussion_id` (`repository_id`,`discussion_id`),
  UNIQUE KEY `index_discussion_spotlights_on_repository_id_and_position` (`repository_id`,`position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `discussion_transfers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discussion_transfers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `old_repository_id` int(11) NOT NULL,
  `old_discussion_id` int(11) NOT NULL,
  `old_discussion_number` int(11) NOT NULL,
  `new_repository_id` int(11) NOT NULL,
  `new_discussion_id` int(11) NOT NULL,
  `new_discussion_event_id` int(11) DEFAULT NULL,
  `actor_id` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `reason` varbinary(1024) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_discussion_transfers_on_old_discussion_id` (`old_discussion_id`),
  KEY `index_discussion_transfers_on_new_discussion_id` (`new_discussion_id`),
  KEY `index_discussion_transfers_on_old_repo_and_old_discussion_number` (`old_repository_id`,`old_discussion_number`),
  KEY `index_discussion_transfers_on_new_discussion_event_id` (`new_discussion_event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `discussions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discussions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varbinary(1024) NOT NULL,
  `body` mediumblob,
  `repository_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `chosen_comment_id` int(11) DEFAULT NULL,
  `user_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `comment_count` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `number` int(11) NOT NULL,
  `issue_id` int(11) DEFAULT NULL,
  `converted_at` datetime DEFAULT NULL,
  `score` int(11) NOT NULL DEFAULT '0',
  `error_reason` int(11) NOT NULL DEFAULT '0',
  `state` int(11) NOT NULL DEFAULT '0',
  `discussion_type` int(11) NOT NULL DEFAULT '0',
  `discussion_category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_discussions_on_number_and_repository_id` (`number`,`repository_id`),
  KEY `index_discussions_on_created_at_and_repository_id` (`created_at`,`repository_id`),
  KEY `index_discussions_on_repository_id_and_user_id_and_user_hidden` (`repository_id`,`user_id`,`user_hidden`),
  KEY `index_discussions_on_chosen_comment_id_and_user_id` (`chosen_comment_id`,`user_id`),
  KEY `index_discussions_on_user_id_and_user_hidden` (`user_id`,`user_hidden`),
  KEY `index_discussions_on_issue_id_and_repository_id` (`issue_id`,`repository_id`),
  KEY `index_discussions_on_repo_score_chosen_comment` (`repository_id`,`score`,`chosen_comment_id`),
  KEY `index_discussions_on_repository_id_and_state` (`repository_id`,`state`),
  KEY `idx_discussions_type_repo_chosen_comment_score` (`discussion_type`,`repository_id`,`chosen_comment_id`,`score`),
  KEY `index_discussions_on_repository_id_and_updated_at` (`repository_id`,`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `documentation_builds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documentation_builds` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) unsigned DEFAULT NULL,
  `pusher_id` int(11) unsigned DEFAULT NULL,
  `version` varchar(255) DEFAULT NULL,
  `commit_id` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `output` blob,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `finished_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_documentation_builds_on_repository_id_and_version` (`repository_id`,`version`),
  KEY `index_documentation_builds_on_pusher_id` (`pusher_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `docusign_envelopes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docusign_envelopes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_type` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `envelope_id` varchar(40) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `voided_reason` varchar(140) DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `document_type` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_docusign_envelopes_on_envelope_id` (`envelope_id`),
  KEY `index_docusign_envelopes_on_owner_active_status` (`owner_id`,`owner_type`,`active`,`status`),
  KEY `index_docusign_envelopes_on_status_and_active` (`status`,`active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `docusign_webhooks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docusign_webhooks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fingerprint` varchar(64) NOT NULL,
  `payload` text NOT NULL,
  `processed_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_docusign_webhooks_on_fingerprint` (`fingerprint`),
  KEY `index_docusign_webhooks_on_processed_at` (`processed_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `dotcom_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dotcom_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `token` varchar(65) DEFAULT NULL,
  `login` varchar(40) DEFAULT NULL,
  `last_contributions_sync` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_dotcom_users_on_user_id` (`user_id`),
  KEY `index_dotcom_users_on_last_contributions_sync` (`last_contributions_sync`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `downloads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `downloads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `content_type` varchar(255) DEFAULT NULL,
  `repository_id` int(11) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `hits` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_downloads_on_repository_id` (`repository_id`),
  KEY `index_downloads_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `draft_issues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `draft_issues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varbinary(1024) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `memex_project_item_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_draft_issues_on_memex_project_item_id` (`memex_project_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `duplicate_issues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `duplicate_issues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_id` int(11) NOT NULL,
  `canonical_issue_id` int(11) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `duplicate` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_dupe_issues_on_issue_id_canonical_issue_id` (`issue_id`,`canonical_issue_id`),
  KEY `idx_dupe_issues_on_canonical_issue_id_issue_id_duplicate` (`canonical_issue_id`,`issue_id`,`duplicate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `early_access_memberships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `early_access_memberships` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `member_type` varchar(30) NOT NULL DEFAULT 'User',
  `actor_id` int(11) NOT NULL,
  `survey_id` int(11) NOT NULL,
  `feature_slug` varchar(255) NOT NULL,
  `feature_enabled` tinyint(1) NOT NULL DEFAULT '0',
  `invitation_sent_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_early_access_memberships_member_id_member_type_feature_slug` (`member_id`,`member_type`,`feature_slug`),
  KEY `index_early_access_memberships_on_feature_slug` (`feature_slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `elastomer_index_memos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elastomer_index_memos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `cluster` varchar(255) NOT NULL,
  `version` varchar(255) DEFAULT NULL,
  `read` tinyint(1) NOT NULL DEFAULT '0',
  `write` tinyint(1) NOT NULL DEFAULT '0',
  `primary` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `datacenter` varchar(255) NOT NULL DEFAULT 'default',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_elastomer_index_memos_on_name_and_datacenter` (`name`,`datacenter`),
  KEY `index_elastomer_index_memos_on_datacenter` (`datacenter`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `email_domain_reputation_records`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_domain_reputation_records` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_domain` varchar(255) NOT NULL,
  `mx_exchange` varchar(255) DEFAULT NULL,
  `a_record` varchar(255) DEFAULT NULL,
  `sample_size` int(11) NOT NULL DEFAULT '0',
  `not_spammy_sample_size` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_email_domain_reputation_records_on_a_record` (`a_record`,`mx_exchange`,`email_domain`),
  KEY `index_email_domain_reputation_records_on_email_domain` (`email_domain`),
  KEY `index_email_domain_reputation_records_on_mx_exchange` (`mx_exchange`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `email_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `email_id` int(11) unsigned NOT NULL,
  `role` varchar(255) NOT NULL,
  `public` tinyint(1) unsigned DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_email_roles_on_user_id` (`user_id`),
  KEY `index_email_roles_on_email_id_and_role` (`email_id`,`role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `enterprise_agreements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enterprise_agreements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agreement_id` varchar(128) NOT NULL,
  `business_id` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `seats` int(11) NOT NULL DEFAULT '0',
  `azure_subscription_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_enterprise_agreements_on_agreement_id` (`agreement_id`),
  UNIQUE KEY `index_enterprise_agreements_on_azure_subscription_id` (`azure_subscription_id`),
  KEY `index_enterprise_agreements_on_business_and_category_and_status` (`business_id`,`category`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `enterprise_contributions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enterprise_contributions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `enterprise_installation_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `contributed_on` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_enterprise_contributions_on_user_installation_contributed` (`user_id`,`enterprise_installation_id`,`contributed_on`),
  KEY `index_enterprise_contributions_on_user_id_and_contributed_on` (`user_id`,`contributed_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `enterprise_installation_user_account_emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enterprise_installation_user_account_emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enterprise_installation_user_account_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `email` varchar(255) NOT NULL,
  `primary` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_enterprise_inst_user_account_emails_account_id_and_email` (`enterprise_installation_user_account_id`,`email`),
  KEY `idx_ent_install_ua_emails_on_ent_install_ua_id_primary_email` (`enterprise_installation_user_account_id`,`primary`,`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `enterprise_installation_user_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enterprise_installation_user_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enterprise_installation_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `remote_user_id` int(11) NOT NULL,
  `remote_created_at` datetime NOT NULL,
  `login` varchar(255) NOT NULL,
  `profile_name` varchar(255) DEFAULT NULL,
  `site_admin` tinyint(1) NOT NULL DEFAULT '0',
  `suspended_at` datetime DEFAULT NULL,
  `business_user_account_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_enterprise_inst_user_accounts_inst_id_and_remote_user_id` (`enterprise_installation_id`,`remote_user_id`),
  KEY `index_enterprise_installation_user_accounts_on_installation_id` (`enterprise_installation_id`),
  KEY `idx_ent_installation_user_accounts_on_business_user_account_id` (`business_user_account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `enterprise_installation_user_accounts_uploads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enterprise_installation_user_accounts_uploads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `business_id` int(11) NOT NULL,
  `enterprise_installation_id` int(11) DEFAULT NULL,
  `uploader_id` int(11) DEFAULT NULL,
  `content_type` varchar(40) NOT NULL,
  `size` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `guid` varchar(36) DEFAULT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `storage_blob_id` int(11) DEFAULT NULL,
  `oid` varchar(64) DEFAULT NULL,
  `storage_provider` varchar(30) DEFAULT NULL,
  `sync_state` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_enterprise_inst_user_accounts_uploads_on_business_id` (`business_id`,`guid`),
  KEY `index_enterprise_inst_user_accounts_uploads_on_installation_id` (`enterprise_installation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `enterprise_installations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enterprise_installations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `host_name` varchar(255) NOT NULL,
  `customer_name` varchar(100) NOT NULL,
  `license_hash` varchar(64) NOT NULL,
  `license_public_key` blob NOT NULL,
  `owner_id` int(11) NOT NULL,
  `owner_type` varchar(30) NOT NULL DEFAULT 'User',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `http_only` tinyint(1) NOT NULL DEFAULT '0',
  `version` varchar(12) DEFAULT NULL,
  `server_id` varchar(36) DEFAULT NULL,
  `integration_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_enterprise_installations_on_server_id` (`server_id`),
  KEY `index_enterprise_installations_on_host_name` (`host_name`),
  KEY `index_enterprise_installations_on_owner_id_and_owner_type` (`owner_id`,`owner_type`),
  KEY `enterprise_installations_on_integration_id` (`integration_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ephemeral_notices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ephemeral_notices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `parent_type` varchar(100) NOT NULL,
  `notice_type` int(11) NOT NULL DEFAULT '0',
  `notice_text` varbinary(1024) NOT NULL,
  `link_to` text,
  `link_title` varbinary(1024) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_ephemeral_notice_user_idx` (`parent_id`,`parent_type`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `epoch_commits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `epoch_commits` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `commit_oid` varchar(40) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `version_vector` blob NOT NULL,
  `epoch_id` bigint(20) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_epoch_commits_on_epoch_id_and_created_at` (`epoch_id`,`created_at`),
  KEY `index_epoch_commits_on_repo_commit_oid` (`repository_id`,`commit_oid`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `epoch_operations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `epoch_operations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `operation` blob NOT NULL,
  `author_id` int(11) NOT NULL,
  `epoch_id` bigint(20) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_epoch_operations_on_author_id` (`author_id`),
  KEY `index_epoch_operations_on_epoch_id_and_created_at` (`epoch_id`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `epochs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `epochs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ref` varbinary(1024) NOT NULL,
  `base_commit_oid` varchar(40) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_epochs_on_repo_branch_base_commit_oid` (`repository_id`,`ref`,`base_commit_oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `experiments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `experiments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `percent` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_experiments_on_name` (`name`),
  KEY `index_experiments_on_percent` (`percent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `external_identities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `external_identities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `provider_id` int(11) NOT NULL,
  `provider_type` varchar(40) NOT NULL,
  `guid` varchar(36) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `disabled_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_external_identities_on_guid` (`guid`),
  UNIQUE KEY `index_on_provider_and_user` (`provider_id`,`provider_type`,`user_id`),
  KEY `index_external_identities_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `external_identity_attribute_mappings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `external_identity_attribute_mappings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provider_type` varchar(255) DEFAULT NULL,
  `provider_id` int(11) DEFAULT NULL,
  `scheme` int(11) NOT NULL,
  `target` int(11) NOT NULL,
  `source` text NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index_on_id_attr_mapping_by_provider` (`provider_id`,`provider_type`,`scheme`,`target`),
  KEY `index_on_id_attr_mapping_by_provider` (`provider_id`,`provider_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `external_identity_attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `external_identity_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `external_identity_id` int(11) NOT NULL,
  `scheme` varchar(4) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `metadata_json` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_attributes_on_external_identity_name_and_value` (`external_identity_id`,`scheme`,`name`,`value`),
  KEY `external_identity_attributes_id_name_value_index` (`external_identity_id`,`name`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `external_identity_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `external_identity_sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_session_id` int(11) NOT NULL,
  `external_identity_id` int(11) NOT NULL,
  `expires_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_external_sessions_on_expires_at` (`expires_at`),
  KEY `index_external_sessions_on_user_session_id_and_expires_at` (`user_session_id`,`expires_at`),
  KEY `index_external_identity_sessions_on_external_identity_id` (`external_identity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `feature_enrollments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feature_enrollments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `feature_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `enrolled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `enrollee_type` varchar(40) NOT NULL,
  `enrollee_id` int(11) NOT NULL,
  `last_actor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_on_enrollee_id_and_enrollee_type_and_feature_id` (`enrollee_id`,`enrollee_type`,`feature_id`),
  KEY `index_feature_enrollments_on_feature_id` (`feature_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `features`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `public_name` varchar(40) NOT NULL,
  `slug` varchar(60) NOT NULL,
  `description` mediumblob,
  `feedback_link` text,
  `enrolled_by_default` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `flipper_feature_id` int(11) DEFAULT NULL,
  `legal_agreement_required` tinyint(1) NOT NULL DEFAULT '0',
  `image_link` text,
  `documentation_link` text,
  `published_at` datetime DEFAULT NULL,
  `feedback_category` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_features_on_public_name` (`public_name`),
  UNIQUE KEY `index_features_on_slug` (`slug`),
  UNIQUE KEY `index_features_on_flipper_feature_id` (`flipper_feature_id`),
  KEY `index_features_on_legal_agreement_required` (`legal_agreement_required`),
  KEY `idx_on_published_at_flipper_feature_id_legal_agreement_required` (`published_at`,`flipper_feature_id`,`legal_agreement_required`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `fileservers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fileservers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `host` varchar(255) NOT NULL,
  `fqdn` varchar(255) NOT NULL,
  `embargoed` tinyint(1) NOT NULL DEFAULT '0',
  `online` tinyint(1) NOT NULL DEFAULT '0',
  `evacuating` tinyint(1) NOT NULL DEFAULT '0',
  `quiescing` tinyint(1) NOT NULL DEFAULT '0',
  `datacenter` varchar(20) DEFAULT NULL,
  `rack` varchar(20) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `evacuating_reason` varchar(255) DEFAULT NULL,
  `quiescing_reason` varchar(255) DEFAULT NULL,
  `embargoed_reason` varchar(255) DEFAULT NULL,
  `non_voting` tinyint(1) NOT NULL DEFAULT '0',
  `hdd_storage` tinyint(1) NOT NULL DEFAULT '0',
  `site` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_fileservers_on_host` (`host`),
  UNIQUE KEY `index_fileservers_on_fqdn` (`fqdn`),
  KEY `index_fileservers_by_location` (`datacenter`,`rack`),
  KEY `index_fileservers_on_site_and_rack` (`site`,`rack`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `fine_grained_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fine_grained_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `custom_roles_enabled` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_fine_grained_permissions_on_action` (`action`),
  KEY `index_fine_grained_permissions_on_custom_roles_enabled` (`custom_roles_enabled`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `flipper_features`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flipper_features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `description` blob,
  `github_org_team_id` int(11) DEFAULT NULL,
  `slack_channel` varchar(80) DEFAULT NULL,
  `service_name` varchar(255) DEFAULT NULL,
  `long_lived` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_flipper_features_on_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `flipper_gates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flipper_gates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `flipper_feature_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_flipper_gates_on_flipper_feature_id_and_name_and_value` (`flipper_feature_id`,`name`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `followers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `followers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `following_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_followers_on_user_id_and_following_id` (`user_id`,`following_id`),
  KEY `index_followers_on_following_id_and_created_at` (`following_id`,`created_at`),
  KEY `index_followers_on_user_id_and_created_at` (`user_id`,`created_at`),
  KEY `index_followers_on_created_at_and_following_id` (`created_at`,`following_id`),
  KEY `index_followers_on_user_hidden_and_user_id` (`user_hidden`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `fraud_flagged_sponsors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fraud_flagged_sponsors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sponsors_fraud_review_id` int(11) NOT NULL,
  `sponsor_id` int(11) NOT NULL,
  `matched_current_client_id` varchar(255) DEFAULT NULL,
  `matched_current_ip` varchar(40) DEFAULT NULL,
  `matched_historical_ip` varchar(40) DEFAULT NULL,
  `matched_historical_client_id` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `matched_current_ip_region_and_user_agent` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_review_and_sponsor` (`sponsors_fraud_review_id`,`sponsor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ghvfs_fileservers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ghvfs_fileservers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `host` varchar(255) NOT NULL,
  `online` tinyint(1) NOT NULL DEFAULT '0',
  `embargoed` tinyint(1) NOT NULL DEFAULT '0',
  `evacuating` tinyint(1) NOT NULL DEFAULT '0',
  `datacenter` varchar(20) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_ghvfs_fileservers_unique` (`host`),
  KEY `index_ghvfs_fileservers_datacenter` (`datacenter`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ghvfs_replicas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ghvfs_replicas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `ghvfs_fileserver_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_ghvfs_replicas_repository` (`ghvfs_fileserver_id`,`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `gist_backup_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gist_backup_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gist_id` int(11) NOT NULL,
  `last_backup_at` datetime DEFAULT NULL,
  `last_error_at` datetime DEFAULT NULL,
  `error_count` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `last_backup_checksum` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_gist_backup_statuses_on_gist_id` (`gist_id`),
  KEY `index_gist_backup_statuses_on_last_backup_at` (`last_backup_at`),
  KEY `gist_scheduler` (`last_error_at`,`last_backup_at`,`gist_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `gist_bases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gist_bases` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `repo_name` varchar(255) NOT NULL,
  `incremental_id` bigint(20) NOT NULL,
  `path` varchar(255) NOT NULL,
  `key_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_gist_bases_on_repo_name` (`repo_name`),
  KEY `index_gist_bases_on_key_id` (`key_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `gist_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gist_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `body` mediumblob,
  `gist_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `formatter` varchar(20) DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `comment_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `comment_hidden_reason` varbinary(1024) DEFAULT NULL,
  `comment_hidden_classifier` varchar(50) DEFAULT NULL,
  `comment_hidden_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_gist_comments_on_gist_id` (`gist_id`),
  KEY `index_gist_comments_on_user_id` (`user_id`),
  KEY `index_gist_comments_on_user_hidden_and_user_id` (`user_hidden`,`user_id`),
  KEY `index_gist_comments_on_user_id_and_created_at` (`user_id`,`created_at`),
  KEY `index_gist_comments_on_user_id_and_updated_at` (`user_id`,`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `gist_disabled_backups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gist_disabled_backups` (
  `gist_id` int(11) NOT NULL,
  `gist_repo_name` char(32) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `disabled_at` datetime NOT NULL,
  PRIMARY KEY (`gist_id`),
  UNIQUE KEY `index_gist_disabled_backups_on_gist_repo_name` (`gist_repo_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `gist_incrementals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gist_incrementals` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `previous_id` bigint(20) DEFAULT NULL,
  `repo_name` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `checksum` varchar(255) DEFAULT NULL,
  `audit_log_len` bigint(20) DEFAULT NULL,
  `key_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_gist_incrementals_previous_unique` (`previous_id`),
  KEY `index_gist_on_repo_name` (`repo_name`),
  KEY `index_gist_incrementals_on_key_id` (`key_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `gist_maintenance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gist_maintenance` (
  `repo_name` varchar(255) NOT NULL,
  `status` enum('completed','scheduled','running','retry','error') NOT NULL,
  `scheduled_at` datetime DEFAULT NULL,
  `last_maintenance_at` datetime DEFAULT NULL,
  `incrementals` int(11) DEFAULT NULL,
  PRIMARY KEY (`repo_name`),
  KEY `gist_maintenance_on_last_maintenance_at` (`last_maintenance_at`),
  KEY `gist_maintenance_on_incrementals` (`incrementals`),
  KEY `index_gist_maintenance_on_status_and_scheduled_at` (`status`,`scheduled_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `gist_replicas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gist_replicas` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `gist_id` int(11) NOT NULL,
  `host` varchar(255) NOT NULL,
  `checksum` varchar(255) NOT NULL,
  `state` int(11) NOT NULL,
  `read_weight` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_gist_replicas_on_gist_id_and_host` (`gist_id`,`host`),
  KEY `index_gist_replicas_on_updated_at` (`updated_at`),
  KEY `index_gist_replicas_on_state` (`state`),
  KEY `index_gist_replicas_on_host_and_state_and_gist_id` (`host`,`state`,`gist_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `gist_user_subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gist_user_subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `plan` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_gist_user_subscriptions_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `gists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `repo_name` varchar(40) DEFAULT NULL,
  `description` blob,
  `public` tinyint(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `delete_flag` tinyint(1) NOT NULL DEFAULT '0',
  `pushed_at` datetime DEFAULT NULL,
  `disabled_by` int(11) DEFAULT NULL,
  `disabling_reason` varchar(20) DEFAULT NULL,
  `disabled_at` datetime DEFAULT NULL,
  `maintenance_status` varchar(20) DEFAULT NULL,
  `last_maintenance_at` datetime DEFAULT NULL,
  `pushed_count` int(11) NOT NULL DEFAULT '0',
  `pushed_count_since_maintenance` int(11) NOT NULL DEFAULT '0',
  `disk_usage` int(11) DEFAULT '0',
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `last_maintenance_attempted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_gists_on_repo_name` (`repo_name`),
  KEY `index_gists_on_public` (`public`,`delete_flag`,`updated_at`),
  KEY `index_gists_on_maintenance_status` (`maintenance_status`,`pushed_count_since_maintenance`,`last_maintenance_at`),
  KEY `index_gists_on_user_hidden_and_user_id` (`user_hidden`,`user_id`),
  KEY `index_gists_on_maint_stat_last_maint_attempted_at` (`maintenance_status`,`last_maintenance_attempted_at`),
  KEY `index_gists_on_user_id_and_public_and_delete_flg_and_user_hidden` (`user_id`,`public`,`delete_flag`,`user_hidden`),
  KEY `index_gists_on_parent_and_public_and_deleted` (`parent_id`,`public`,`delete_flag`),
  KEY `index_gists_on_user_id_and_public_and_delete_flag_and_parent_id` (`user_id`,`public`,`delete_flag`,`parent_id`),
  KEY `index_gists_on_user_id_and_delete_flag_and_pushed_at` (`user_id`,`delete_flag`,`pushed_at`),
  KEY `index_gists_on_user_id_and_public_and_delete_flag_and_updated_at` (`user_id`,`public`,`delete_flag`,`updated_at`),
  KEY `index_gists_on_user_id_and_public_and_delete_flag` (`user_id`,`public`,`delete_flag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `git_sizer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `git_sizer` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `repository_type` int(11) unsigned NOT NULL,
  `repository_id` int(11) unsigned NOT NULL,
  `max_blob_size_bytes` bigint(20) unsigned NOT NULL,
  `max_checkout_blob_count` bigint(20) unsigned NOT NULL,
  `max_checkout_blob_size_bytes` bigint(20) unsigned NOT NULL,
  `max_checkout_link_count` bigint(20) unsigned NOT NULL,
  `max_checkout_path_depth` bigint(20) unsigned NOT NULL,
  `max_checkout_path_length` bigint(20) unsigned NOT NULL,
  `max_checkout_submodule_count` bigint(20) unsigned NOT NULL,
  `max_checkout_tree_count` bigint(20) unsigned NOT NULL,
  `max_commit_parent_count` bigint(20) unsigned NOT NULL,
  `max_commit_size_bytes` bigint(20) unsigned NOT NULL,
  `max_history_depth` bigint(20) unsigned NOT NULL,
  `max_tag_depth` bigint(20) unsigned NOT NULL,
  `max_tree_entries` bigint(20) unsigned NOT NULL,
  `reference_count` bigint(20) unsigned NOT NULL,
  `unique_blob_count` bigint(20) unsigned NOT NULL,
  `unique_blob_size_bytes` bigint(20) unsigned NOT NULL,
  `unique_commit_count` bigint(20) unsigned NOT NULL,
  `unique_commit_size_bytes` bigint(20) unsigned NOT NULL,
  `unique_tag_count` bigint(20) unsigned NOT NULL,
  `unique_tree_count` bigint(20) unsigned NOT NULL,
  `unique_tree_entries` bigint(20) unsigned NOT NULL,
  `unique_tree_size_bytes` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_git_sizer_on_type_and_id` (`repository_id`,`repository_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `global_stratocaster_indexes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `global_stratocaster_indexes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `index_key` varchar(32) NOT NULL,
  `value` bigint(20) NOT NULL,
  `modified_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_global_stratocaster_indexes_on_index_key_and_modified_at` (`index_key`,`modified_at`),
  KEY `index_global_stratocaster_indexes_on_index_key_and_value` (`index_key`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `gpg_key_emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpg_key_emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gpg_key_id` int(11) NOT NULL,
  `user_email_id` int(11) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_gpg_key_emails_on_gpg_key_id_and_email` (`gpg_key_id`,`email`),
  KEY `index_gpg_key_emails_on_user_email_id` (`user_email_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `gpg_keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpg_keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `key_id` varbinary(8) NOT NULL,
  `public_key` mediumblob NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `primary_key_id` int(11) DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  `can_sign` tinyint(1) NOT NULL DEFAULT '1',
  `can_encrypt_comms` tinyint(1) NOT NULL DEFAULT '0',
  `can_encrypt_storage` tinyint(1) NOT NULL DEFAULT '0',
  `can_certify` tinyint(1) NOT NULL DEFAULT '0',
  `raw_key` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_gpg_keys_on_user_id_and_key_id` (`user_id`,`key_id`),
  KEY `index_gpg_keys_on_primary_key_id` (`primary_key_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `hidden_profile_timeline_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hidden_profile_timeline_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` int(11) NOT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_hidden_profile_timeline_events_on_keys` (`user_id`,`event_type`,`subject_id`),
  KEY `index_hidden_profile_timeline_events_on_created_at` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `hidden_task_list_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hidden_task_list_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `removable_type` varchar(40) NOT NULL,
  `removable_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_on_user_id_and_removable_type_and_removable_id` (`user_id`,`removable_type`,`removable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `hidden_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hidden_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_hidden_users_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `hook_config_attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hook_config_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hook_id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` varbinary(1024) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_hooks_on_hook_id_and_key` (`hook_id`,`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `hook_event_subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hook_event_subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subscriber_id` int(11) NOT NULL,
  `subscriber_type` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `by_subscriber_and_name` (`subscriber_id`,`subscriber_type`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `hooks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hooks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT '0',
  `confirmed` tinyint(1) unsigned DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `installation_target_type` varchar(40) NOT NULL,
  `installation_target_id` int(11) NOT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `oauth_application_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_hooks_installation_target` (`installation_target_id`,`installation_target_type`),
  KEY `index_hooks_on_oauth_application_id` (`oauth_application_id`),
  KEY `index_hooks_on_name` (`name`),
  KEY `index_hooks_on_active` (`active`),
  KEY `index_hooks_on_installation_target_type` (`installation_target_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `hookshot_ar_internal_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hookshot_ar_internal_metadata` (
  `key` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `hookshot_delivery_configs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hookshot_delivery_configs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `payload_id` bigint(20) DEFAULT NULL,
  `hook_id` bigint(20) DEFAULT NULL,
  `service_name` varchar(255) DEFAULT NULL,
  `config` mediumblob,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `delivery_logs_count` int(11) DEFAULT NULL,
  `repo_id` int(11) DEFAULT NULL,
  `installation_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_hookshot_delivery_configs_on_payload_id` (`payload_id`),
  KEY `index_hookshot_delivery_configs_on_hook_id` (`hook_id`),
  KEY `index_hookshot_delivery_configs_on_created_at` (`created_at`),
  KEY `index_hookshot_delivery_configs_on_delivery_logs_count` (`delivery_logs_count`),
  KEY `index_hookshot_delivery_configs_on_repo_id` (`repo_id`),
  KEY `index_hookshot_delivery_configs_on_installation_id` (`installation_id`),
  KEY `index_hookshot_delivery_configs_on_updated_at` (`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `hookshot_delivery_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hookshot_delivery_logs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `delivery_config_id` bigint(20) DEFAULT NULL,
  `github_request_id` varchar(255) DEFAULT NULL,
  `hook_status` int(11) DEFAULT NULL,
  `hook_message` varchar(255) DEFAULT NULL,
  `delivered_at` datetime DEFAULT NULL,
  `duration` decimal(10,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `guid` varchar(255) DEFAULT NULL,
  `redelivery` tinyint(1) DEFAULT NULL,
  `hook_id` bigint(20) DEFAULT NULL,
  `parent_type` varchar(255) DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `delivery_remote_requests_count` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_hookshot_delivery_logs_on_created_at` (`created_at`),
  KEY `index_hookshot_delivery_logs_on_hook_id` (`hook_id`),
  KEY `index_hookshot_delivery_logs_on_guid` (`guid`),
  KEY `index_hookshot_delivery_logs_on_delivery_config_id` (`delivery_config_id`),
  KEY `index_hookshot_delivery_logs_on_hook_status` (`hook_status`),
  KEY `index_hookshot_delivery_logs_on_redelivery` (`redelivery`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `hookshot_delivery_payloads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hookshot_delivery_payloads` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL,
  `parent_type` varchar(255) DEFAULT NULL,
  `guid` varchar(255) DEFAULT NULL,
  `event` varchar(255) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `payload` longblob,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `delivery_configs_count` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_on_hookshot_delivery_payloads_on_guid_parent_id_and_type` (`guid`,`parent_id`,`parent_type`),
  KEY `index_hookshot_delivery_payloads_on_created_at` (`created_at`),
  KEY `index_hookshot_delivery_payloads_on_event` (`event`),
  KEY `index_hookshot_delivery_payloads_on_updated_at` (`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `hookshot_delivery_queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hookshot_delivery_queue` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `delivery_config_id` bigint(20) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `github_request_id` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `pending_at` datetime DEFAULT NULL,
  `completed_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `hookshot_delivery_remote_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hookshot_delivery_remote_requests` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `delivery_log_id` bigint(20) DEFAULT NULL,
  `request_method` varchar(255) DEFAULT NULL,
  `request_url` varchar(1500) DEFAULT NULL,
  `request_headers` mediumblob,
  `response_code` int(11) DEFAULT NULL,
  `response_headers` mediumblob,
  `response_body` mediumblob,
  `remote_ip` varchar(255) DEFAULT NULL,
  `error` mediumblob,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `adapter` varchar(255) DEFAULT NULL,
  `return_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_hookshot_delivery_remote_requests_on_created_at` (`created_at`),
  KEY `index_hookshot_delivery_remote_requests_on_delivery_log_id` (`delivery_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `hookshot_flipper_features`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hookshot_flipper_features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_hookshot_flipper_features_on_key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `hookshot_flipper_gates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hookshot_flipper_gates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `feature_key` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_hookshot_flipper_gates_on_feature_key_and_key_and_value` (`feature_key`,`key`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `hookshot_schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hookshot_schema_migrations` (
  `version` varchar(255) NOT NULL,
  UNIQUE KEY `hookshot_unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ignore_shas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ignore_shas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) DEFAULT NULL,
  `sha` varchar(255) DEFAULT NULL,
  `applied_flag` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_ignore_shas_on_repository_id` (`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ignored_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ignored_users` (
  `user_id` int(11) unsigned DEFAULT NULL,
  `ignored_id` int(11) unsigned DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  `blocked_from_content_id` int(11) DEFAULT NULL,
  `blocked_from_content_type` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_ignored_users_on_user_id_and_ignored_id_and_expires_at` (`user_id`,`ignored_id`,`expires_at`),
  KEY `index_ignored_users_on_ignored_id` (`ignored_id`),
  KEY `index_ignored_users_on_user_id_and_expires_at` (`user_id`,`expires_at`),
  KEY `index_ignored_users_on_expires_at` (`expires_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `import_item_errors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `import_item_errors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `import_item_id` int(11) NOT NULL,
  `payload_location` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `resource` varchar(255) DEFAULT NULL,
  `field` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_import_item_errors_on_import_item_id` (`import_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `import_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `import_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` varchar(30) NOT NULL,
  `model_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `json_data` mediumblob,
  `model_type` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_import_items_on_repository_id_and_updated_at` (`repository_id`,`updated_at`),
  KEY `index_import_items_on_repository_id_and_status` (`repository_id`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `imports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `creator_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `index_public_gists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `index_public_gists` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `integration_aliases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `integration_aliases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `integration_id` int(11) NOT NULL,
  `slug` varchar(34) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_integration_aliases_on_integration_id` (`integration_id`),
  UNIQUE KEY `index_integration_aliases_on_slug` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `integration_content_references`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `integration_content_references` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(100) NOT NULL,
  `reference_type` int(11) NOT NULL DEFAULT '0',
  `integration_version_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_integration_content_references_on_integration_version_id` (`integration_version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `integration_features`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `integration_features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `body` mediumblob NOT NULL,
  `slug` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_integration_features_on_name` (`name`),
  UNIQUE KEY `index_integration_features_on_slug` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `integration_install_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `integration_install_triggers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `integration_id` int(11) NOT NULL,
  `install_type` int(11) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `deactivated` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `install_triggers_by_integration_id_and_type` (`integration_id`,`install_type`),
  KEY `index_integration_install_triggers_on_install_type` (`install_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `integration_installation_request_repositories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `integration_installation_request_repositories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `integration_installation_request_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_integration_request_repository_by_request_id` (`integration_installation_request_id`),
  KEY `index_integration_request_repository_by_repository_id` (`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `integration_installation_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `integration_installation_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `requester_id` int(11) NOT NULL,
  `integration_id` int(11) NOT NULL,
  `target_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_integration_request_by_requester_id_and_integration_id` (`requester_id`,`integration_id`),
  KEY `index_integration_request_by_requester_id` (`requester_id`),
  KEY `index_integration_request_by_integration_id` (`integration_id`),
  KEY `index_integration_request_by_target_id` (`target_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `integration_installations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `integration_installations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `integration_id` int(11) NOT NULL,
  `target_id` int(11) NOT NULL,
  `target_type` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `integration_version_id` int(11) NOT NULL,
  `outdated` tinyint(1) NOT NULL DEFAULT '0',
  `rate_limit` int(11) DEFAULT NULL,
  `temporary_rate_limit` int(11) DEFAULT NULL,
  `temporary_rate_limit_expires_at` datetime DEFAULT NULL,
  `dynamic_rate_limit` int(11) DEFAULT NULL,
  `contact_email_id` int(11) DEFAULT NULL,
  `subscription_item_id` int(11) DEFAULT NULL,
  `integration_install_trigger_id` int(11) DEFAULT NULL,
  `integration_version_number` int(11) NOT NULL,
  `permissions_cache` json DEFAULT NULL,
  `repository_selection_cache` varchar(8) DEFAULT NULL,
  `user_suspended_by_id` int(11) DEFAULT NULL,
  `user_suspended_at` bigint(20) DEFAULT NULL,
  `integrator_suspended` tinyint(1) NOT NULL DEFAULT '0',
  `integrator_suspended_at` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_installations_on_integration_id_and_target_id` (`integration_id`,`target_id`,`target_type`),
  KEY `index_integration_installations_on_target_id_and_target_type` (`target_id`,`target_type`),
  KEY `index_integration_installations_on_contact_email_id` (`contact_email_id`),
  KEY `index_integration_installations_on_integration_version_id` (`integration_version_id`),
  KEY `index_integration_installations_on_integration_version_number` (`integration_version_number`),
  KEY `index_integration_installations_on_user_suspended_by_id` (`user_suspended_by_id`),
  KEY `index_integration_installations_on_integrator_suspended` (`integrator_suspended`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `integration_keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `integration_keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `integration_id` int(11) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `public_pem` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_integration_keys_on_integration_id` (`integration_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `integration_listing_features`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `integration_listing_features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `integration_listing_id` int(11) NOT NULL,
  `integration_feature_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_integration_listings_on_features` (`integration_listing_id`,`integration_feature_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `integration_listing_language_names`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `integration_listing_language_names` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `integration_listing_id` int(11) NOT NULL,
  `language_name_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_integration_listings_on_languages` (`language_name_id`,`integration_listing_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `integration_listings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `integration_listings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` int(11) NOT NULL DEFAULT '0',
  `blurb` varchar(255) DEFAULT NULL,
  `body` mediumblob,
  `installation_url` varchar(255) DEFAULT NULL,
  `integration_type` varchar(255) NOT NULL,
  `integration_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `pricing_url` varchar(255) DEFAULT NULL,
  `documentation_url` varchar(255) DEFAULT NULL,
  `tos_url` varchar(255) DEFAULT NULL,
  `support_url` varchar(255) DEFAULT NULL,
  `status_url` varchar(255) DEFAULT NULL,
  `privacy_policy_url` varchar(255) DEFAULT NULL,
  `company_url` varchar(255) DEFAULT NULL,
  `learn_more_url` varchar(255) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_integration_listings_on_integration` (`integration_id`,`integration_type`),
  UNIQUE KEY `index_integration_listings_on_slug` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `integration_manifests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `integration_manifests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(40) NOT NULL,
  `name` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `data` json DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_integration_manifests_on_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `integration_single_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `integration_single_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `integration_version_id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_on_integration_version_id_and_path` (`integration_version_id`,`path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `integration_transfers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `integration_transfers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `integration_id` int(11) NOT NULL,
  `requester_id` int(11) NOT NULL,
  `responder_id` int(11) DEFAULT NULL,
  `target_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_integration_transfers_on_integration_id_target_id` (`integration_id`,`target_id`),
  KEY `index_integration_transfers_on_requester_id` (`requester_id`),
  KEY `index_integration_transfers_on_target_id` (`target_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `integration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `integration_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `note` text,
  `number` int(11) DEFAULT '0',
  `integration_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `single_file_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_integration_versions_on_integration_id_and_number` (`integration_id`,`number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `integrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `integrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `owner_type` varchar(32) NOT NULL DEFAULT 'User',
  `bot_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url` text NOT NULL,
  `description` mediumblob,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `public` tinyint(1) NOT NULL DEFAULT '0',
  `slug` varchar(255) NOT NULL DEFAULT '',
  `callback_url` text,
  `single_file_name` varchar(255) DEFAULT NULL,
  `key` varchar(20) DEFAULT NULL,
  `secret` varchar(40) DEFAULT NULL,
  `setup_url` text,
  `bgcolor` varchar(6) NOT NULL DEFAULT 'ffffff',
  `setup_on_update` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `full_trust` tinyint(1) NOT NULL DEFAULT '0',
  `request_oauth_on_install` tinyint(1) NOT NULL DEFAULT '0',
  `marketplace_category_id` int(11) DEFAULT NULL,
  `user_token_expiration` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_integrations_on_slug` (`slug`),
  UNIQUE KEY `index_integrations_on_key` (`key`),
  KEY `index_integrations_on_bot_id` (`bot_id`),
  KEY `index_integrations_on_name` (`name`),
  KEY `index_integrations_on_owner_id_and_owner_type_and_deleted_at` (`owner_id`,`owner_type`,`deleted_at`),
  KEY `index_integrations_on_marketplace_category_id` (`marketplace_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `interaction_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interaction_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `show_blocked_contributors_warning` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_interaction_settings_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `interactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `web_edits` int(11) DEFAULT '0',
  `last_web_edit_at` datetime DEFAULT NULL,
  `pull_requests` int(11) DEFAULT '0',
  `last_pull_request_at` datetime DEFAULT NULL,
  `issues` int(11) DEFAULT '0',
  `last_issue_at` datetime DEFAULT NULL,
  `comments` int(11) DEFAULT '0',
  `last_comment_at` datetime DEFAULT NULL,
  `pushes` int(11) DEFAULT '0',
  `last_pushed_at` datetime DEFAULT NULL,
  `active_sessions` int(11) DEFAULT '0',
  `last_active_session_at` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `wiki_edits` int(11) DEFAULT '0',
  `last_wiki_edit_at` datetime DEFAULT NULL,
  `last_released_at` timestamp NULL DEFAULT NULL,
  `releases` int(11) DEFAULT '0',
  `mac_active_sessions` int(11) DEFAULT '0',
  `last_mac_active_session` datetime DEFAULT NULL,
  `windows_active_sessions` int(11) DEFAULT '0',
  `last_windows_active_session` datetime DEFAULT NULL,
  `last_active_at` datetime DEFAULT NULL,
  `signed_up_at` datetime DEFAULT NULL,
  `mac_desktop_active_sessions` int(11) DEFAULT '0',
  `last_mac_desktop_active_session_at` datetime DEFAULT NULL,
  `windows_desktop_active_sessions` int(11) DEFAULT '0',
  `last_windows_desktop_active_session_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_interactions_on_last_active_session_at` (`last_active_session_at`),
  KEY `index_interactions_on_user_id` (`user_id`),
  KEY `index_interactions_on_last_active_at` (`last_active_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `interactive_component_interactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interactive_component_interactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `interactive_component_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `interacted_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `interactive_component_user_interactions_idx` (`interactive_component_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `interactive_components`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interactive_components` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `container_id` int(11) NOT NULL,
  `container_type` varchar(100) NOT NULL,
  `external_id` varchar(255) NOT NULL,
  `elements` blob NOT NULL,
  `container_order` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `interacted_at` datetime DEFAULT NULL,
  `outdated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_interactive_components_on_container_id_and_container_type` (`container_id`,`container_type`),
  KEY `index_interactive_components_on_outdated_at` (`outdated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `internal_repositories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `internal_repositories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) DEFAULT NULL,
  `business_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_internal_repositories_on_repository_id_and_business_id` (`repository_id`,`business_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ip_whitelist_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ip_whitelist_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_type` varchar(20) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `whitelisted_value` varchar(45) NOT NULL,
  `range_from` varbinary(16) NOT NULL,
  `range_to` varbinary(16) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_ip_whitelist_entries_on_owner_and_active_and_range_values` (`owner_type`,`owner_id`,`active`,`range_from`,`range_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `issue_blob_references`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issue_blob_references` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blob_oid` varchar(40) NOT NULL,
  `commit_oid` varchar(40) NOT NULL,
  `issue_id` int(11) NOT NULL,
  `filepath` varchar(255) NOT NULL,
  `range_start` int(11) NOT NULL,
  `range_end` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_blob_references_on_all_columns` (`issue_id`,`blob_oid`,`commit_oid`,`filepath`,`range_start`,`range_end`),
  KEY `index_issue_blob_references_on_issue_id` (`issue_id`),
  KEY `index_issue_blob_references_on_blob_oid` (`blob_oid`),
  KEY `index_issue_blob_references_on_commit_oid` (`commit_oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `issue_boost_awards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issue_boost_awards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_boost_id` int(11) NOT NULL,
  `pull_request_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `value` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_issue_boost_awards_on_issue_boost_id` (`issue_boost_id`),
  KEY `index_issue_boost_awards_on_user_id` (`user_id`),
  KEY `index_issue_boost_awards_on_pull_request_id` (`pull_request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `issue_boosts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issue_boosts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `issue_id` int(11) NOT NULL,
  `value` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_issue_boosts_on_issue_id` (`issue_id`),
  KEY `index_issue_boosts_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `issue_comment_edits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issue_comment_edits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_comment_id` int(11) NOT NULL,
  `edited_at` datetime NOT NULL,
  `editor_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by_id` int(11) DEFAULT NULL,
  `diff` mediumblob,
  `user_content_edit_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_issue_comment_edits_on_user_content_edit_id` (`user_content_edit_id`),
  KEY `index_issue_comment_edits_on_issue_comment_id` (`issue_comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `issue_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issue_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `body` mediumblob,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `repository_id` int(11) NOT NULL,
  `formatter` varchar(20) DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `comment_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `comment_hidden_reason` varbinary(1024) DEFAULT NULL,
  `comment_hidden_classifier` varchar(50) DEFAULT NULL,
  `comment_hidden_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_issue_comments_on_repository_id_and_created_at` (`repository_id`,`created_at`),
  KEY `index_issue_comments_on_repository_id_and_updated_at` (`repository_id`,`updated_at`),
  KEY `index_issue_comments_on_repository_id_and_issue_id_and_user_id` (`repository_id`,`issue_id`,`user_id`),
  KEY `index_issue_comments_on_user_hidden_and_user_id` (`user_hidden`,`user_id`),
  KEY `index_issue_comments_on_repo_user_id_user_hidden_and_issue_id` (`repository_id`,`user_id`,`user_hidden`,`issue_id`),
  KEY `index_issue_comments_on_issue_id_and_user_hidden` (`issue_id`,`user_hidden`),
  KEY `index_issue_comments_on_user_id_and_created_at` (`user_id`,`created_at`),
  KEY `index_issue_comments_on_user_id_and_updated_at` (`user_id`,`updated_at`),
  KEY `index_issue_comments_on_user_id_and_repository_id` (`user_id`,`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `issue_edits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issue_edits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_id` int(11) NOT NULL,
  `edited_at` datetime NOT NULL,
  `editor_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by_id` int(11) DEFAULT NULL,
  `diff` mediumblob,
  `user_content_edit_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_issue_edits_on_issue_id` (`issue_id`),
  KEY `index_issue_edits_on_user_content_edit_id` (`user_content_edit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `issue_event_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issue_event_details` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `issue_event_id` bigint(11) unsigned NOT NULL,
  `label_id` bigint(20) DEFAULT NULL,
  `label_name` varbinary(1024) DEFAULT NULL,
  `label_color` varchar(6) DEFAULT NULL,
  `label_text_color` varchar(6) DEFAULT NULL,
  `milestone_title` varbinary(1024) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `subject_type` varchar(20) DEFAULT NULL,
  `title_was` varbinary(1024) DEFAULT NULL,
  `title_is` varbinary(1024) DEFAULT NULL,
  `deployment_id` int(11) DEFAULT NULL,
  `ref` varchar(255) DEFAULT NULL,
  `before_commit_oid` char(40) DEFAULT NULL,
  `after_commit_oid` char(40) DEFAULT NULL,
  `pull_request_review_state_was` int(11) DEFAULT NULL,
  `message` mediumblob,
  `pull_request_review_id` int(11) DEFAULT NULL,
  `column_name` varbinary(1024) DEFAULT NULL,
  `previous_column_name` varbinary(1024) DEFAULT NULL,
  `card_id` int(11) DEFAULT NULL,
  `review_request_id` int(11) DEFAULT NULL,
  `performed_by_project_workflow_action_id` int(11) DEFAULT NULL,
  `lock_reason` varchar(30) DEFAULT NULL,
  `milestone_id` int(10) unsigned DEFAULT NULL,
  `deployment_status_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `block_duration_days` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_issue_event_details_on_issue_event_id_and_subject_type` (`issue_event_id`,`subject_type`),
  KEY `index_issue_event_details_on_subject_id_and_subject_type` (`subject_id`,`subject_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `issue_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issue_events` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `issue_id` int(11) unsigned DEFAULT NULL,
  `actor_id` int(11) unsigned DEFAULT NULL,
  `repository_id` int(11) unsigned DEFAULT NULL,
  `event` varchar(40) DEFAULT NULL,
  `commit_id` varchar(40) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `commit_repository_id` int(11) unsigned DEFAULT NULL,
  `referencing_issue_id` int(11) unsigned DEFAULT NULL,
  `raw_data` blob,
  `performed_by_integration_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_issue_events_on_issue_id_and_event_and_commit_id` (`issue_id`,`event`,`commit_id`),
  UNIQUE KEY `index_issue_events_on_issue_id_and_ref_issue_id` (`issue_id`,`event`,`referencing_issue_id`),
  KEY `index_issue_events_on_repository_id_event` (`repository_id`,`event`),
  KEY `index_issue_events_on_repo_id_issue_id_and_event` (`repository_id`,`issue_id`,`event`),
  KEY `index_issue_events_on_issue_id_and_commit_id` (`issue_id`,`commit_id`),
  KEY `index_issue_events_on_actor_id_and_event_and_created_at` (`actor_id`,`event`,`created_at`),
  KEY `index_issue_events_on_repository_id_and_created_at` (`repository_id`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `issue_imports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issue_imports` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) unsigned DEFAULT NULL,
  `importer_id` int(11) unsigned DEFAULT NULL,
  `raw_data` blob,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_issue_imports_on_repository_id_and_created_at` (`repository_id`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `issue_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issue_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source_issue_id` int(11) NOT NULL,
  `target_issue_id` int(11) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `link_type` int(11) NOT NULL,
  `source_repository_id` int(11) NOT NULL,
  `target_repository_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_issue_links_source_target_type` (`source_issue_id`,`target_issue_id`,`link_type`),
  KEY `index_issue_links_source_target_type_and_created_at` (`source_issue_id`,`target_issue_id`,`link_type`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `issue_priorities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issue_priorities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_id` int(11) NOT NULL,
  `milestone_id` int(10) unsigned NOT NULL,
  `priority` bigint(20) unsigned DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_issue_priorities_on_issue_id_and_milestone_id` (`issue_id`,`milestone_id`),
  UNIQUE KEY `index_issue_priorities_on_milestone_id_and_priority` (`milestone_id`,`priority`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `issue_tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issue_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_id` int(11) NOT NULL,
  `task_issue_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `body` varbinary(4095) DEFAULT NULL,
  `completed_at` datetime DEFAULT NULL,
  `mentioned_issue_id` int(11) DEFAULT NULL,
  `group` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_issue_tasks_on_issue_id` (`issue_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `issue_topics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issue_topics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_issue_topics_on_issue_id_and_topic_id` (`issue_id`,`topic_id`),
  KEY `index_issue_topics_on_topic_id_and_issue_id` (`topic_id`,`issue_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `issue_transfers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issue_transfers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `old_repository_id` int(11) NOT NULL,
  `old_issue_id` int(11) NOT NULL,
  `old_issue_number` int(11) NOT NULL,
  `new_repository_id` int(11) NOT NULL,
  `new_issue_id` int(11) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `state` varchar(16) NOT NULL,
  `reason` varbinary(1024) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_issue_transfers_on_old_issue_id` (`old_issue_id`),
  KEY `index_issue_transfers_on_old_repository_id_and_old_issue_number` (`old_repository_id`,`old_issue_number`),
  KEY `index_issue_transfers_on_new_issue_id` (`new_issue_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `issues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `issue_comments_count` int(11) DEFAULT '0',
  `number` int(11) DEFAULT '0',
  `position` float DEFAULT '1',
  `title` varbinary(1024) DEFAULT NULL,
  `state` varchar(6) DEFAULT NULL,
  `body` mediumblob,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `closed_at` datetime DEFAULT NULL,
  `pull_request_id` int(11) DEFAULT NULL,
  `milestone_id` int(11) unsigned DEFAULT NULL,
  `assignee_id` int(11) unsigned DEFAULT NULL,
  `contributed_at_timestamp` bigint(20) DEFAULT NULL,
  `contributed_at_offset` mediumint(9) DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `created_by_logbook_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_issues_on_created_by_logbook_id` (`created_by_logbook_id`),
  KEY `index_issues_on_pull_request_id` (`pull_request_id`),
  KEY `index_issues_on_milestone_id` (`milestone_id`),
  KEY `index_issues_on_user_id_and_state_and_pull_request_id` (`user_id`,`state`,`pull_request_id`),
  KEY `index_issues_on_repository_id_and_number` (`repository_id`,`number`),
  KEY `index_issues_on_user_id_and_created_at` (`user_id`,`created_at`),
  KEY `index_issues_on_user_id_and_user_hidden` (`user_id`,`user_hidden`),
  KEY `index_issues_on_repository_id_and_user_id` (`repository_id`,`user_id`),
  KEY `index_issues_on_repository_id_and_state_and_user_hidden` (`repository_id`,`state`,`user_hidden`),
  KEY `repository_id_and_state_and_pull_request_id_and_user` (`repository_id`,`state`,`pull_request_id`,`user_hidden`,`user_id`),
  KEY `index_issues_on_repository_id_and_pull_request_id_and_created_at` (`repository_id`,`pull_request_id`,`created_at`),
  KEY `index_issues_on_repository_id_and_pull_request_id_and_closed_at` (`repository_id`,`pull_request_id`,`closed_at`),
  KEY `index_issues_on_user_id_and_repository_id` (`user_id`,`repository_id`),
  KEY `index_issues_on_repository_id_and_user_hidden_and_user_id` (`repository_id`,`user_hidden`,`user_id`),
  KEY `repository_id_and_updated_at_and_state_and_pr_id_and_user` (`repository_id`,`updated_at`,`state`,`pull_request_id`,`user_hidden`,`user_id`),
  KEY `repository_id_and_created_at_and_state_and_pr_id_and_user` (`repository_id`,`created_at`,`state`,`pull_request_id`,`user_hidden`,`user_id`,`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `issues_labels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issues_labels` (
  `issue_id` int(11) DEFAULT NULL,
  `label_id` bigint(20) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_issues_labels_on_issue_id_and_label_id` (`issue_id`,`label_id`),
  KEY `index_issues_labels_on_label_id_and_issue_id` (`label_id`,`issue_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `job_posting_credits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_posting_credits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organization_id` int(11) NOT NULL,
  `billing_transaction_id` int(11) DEFAULT NULL,
  `credits_purchased` int(11) NOT NULL,
  `credits_used` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `source` int(11) NOT NULL DEFAULT '0',
  `source_comment` blob,
  PRIMARY KEY (`id`),
  KEY `index_job_posting_credits_on_billing_transaction_id` (`billing_transaction_id`),
  KEY `index_job_posting_credits_on_organization_id` (`organization_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `job_posting_locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_posting_locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_posting_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_job_location_options_on_job_posting_id` (`job_posting_id`),
  KEY `index_job_location_options_on_job_location_id` (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `job_postings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_postings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organization_id` int(11) NOT NULL,
  `remote` tinyint(1) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `kind` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `apply_link` varchar(2048) NOT NULL,
  `description` text,
  `instructions` text,
  `identifier` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `job_posting_credit_id` int(11) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `company_url` varchar(255) DEFAULT NULL,
  `company_logo` varchar(255) DEFAULT NULL,
  `state` int(11) NOT NULL DEFAULT '1',
  `expires_at` datetime NOT NULL,
  `renew_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_job_postings_on_identifier` (`identifier`),
  KEY `index_job_postings_on_organization_id` (`organization_id`),
  KEY `index_job_postings_on_state` (`state`),
  KEY `index_job_postings_on_expires_at` (`expires_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `key_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `key_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key_prefix` varchar(15) NOT NULL,
  `url_template` text NOT NULL,
  `owner_type` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_key_links_on_owner_id_and_owner_type_and_key_prefix` (`owner_id`,`owner_type`,`key_prefix`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `key_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `key_values` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `value` blob NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_key_values_on_key` (`key`),
  KEY `index_key_values_on_expires_at` (`expires_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `key_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `key_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `realm` varchar(255) NOT NULL,
  `version` int(11) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `deprecated` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_versions_on_realm_and_version` (`realm`,`version`),
  KEY `index_key_versions_on_realm_and_active` (`realm`,`active`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `labels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `labels` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varbinary(1024) NOT NULL,
  `color` varchar(10) DEFAULT NULL,
  `repository_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `lowercase_name` varbinary(1024) DEFAULT NULL,
  `description` varbinary(400) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_labels_on_name` (`name`),
  KEY `index_labels_on_repository_id_and_name` (`repository_id`,`name`),
  KEY `index_labels_on_repository_id_and_lowercase_name` (`repository_id`,`lowercase_name`(15))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `language_names`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `language_names` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `linguist_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_language_names_on_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) DEFAULT NULL,
  `language_name_id` int(11) DEFAULT NULL,
  `size` bigint(20) DEFAULT NULL,
  `total_size` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `public` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `index_languages_on_language_name_id` (`language_name_id`),
  KEY `index_languages_on_repository_id` (`repository_id`),
  KEY `index_languages_on_size` (`size`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `last_seen_pull_request_revisions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `last_seen_pull_request_revisions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pull_request_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `last_revision` varbinary(20) NOT NULL,
  `hidden` tinyint(1) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_last_seen_pr_on_pr_id_and_user_id_and_updated_at` (`pull_request_id`,`user_id`,`updated_at`),
  KEY `index_last_seen_pr_on_user_id_and_updated_at` (`user_id`,`updated_at`),
  KEY `index_last_seen_pull_request_revisions_on_created_at` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ldap_group_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ldap_group_members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_dn` varchar(255) NOT NULL,
  `member_dn` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_ldap_group_members_on_group_dn_and_member_dn` (`group_dn`,`member_dn`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ldap_mappings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ldap_mappings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_id` int(11) NOT NULL,
  `subject_type` varchar(255) NOT NULL,
  `dn` varchar(2048) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `last_sync_at` datetime DEFAULT NULL,
  `last_sync_ms` float DEFAULT NULL,
  `sync_status` int(11) DEFAULT NULL,
  `fallback_uid` varchar(255) DEFAULT NULL,
  `dn_hash` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_ldap_mappings_on_identity` (`subject_id`,`subject_type`),
  UNIQUE KEY `index_ldap_mappings_on_identity_and_dn` (`subject_id`,`subject_type`,`dn_hash`),
  KEY `index_ldap_mappings_on_sync_status` (`sync_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `legal_holds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `legal_holds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_legal_holds_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `lfs_references`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lfs_references` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `network_id` int(11) NOT NULL,
  `media_blob_id` int(11) DEFAULT NULL,
  `git_oid` char(40) NOT NULL,
  `lfs_oid` char(64) NOT NULL,
  `unreachable_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_lfs_references_on_git_oid_and_lfs_oid` (`git_oid`,`lfs_oid`),
  KEY `index_lfs_references_on_network_id` (`network_id`),
  KEY `index_lfs_references_on_unreachable_at` (`unreachable_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `mannequin_emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mannequin_emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mannequin_id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `primary` tinyint(1) NOT NULL DEFAULT '0',
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_mannequin_emails_on_mannequin_id` (`mannequin_id`),
  KEY `index_mannequin_emails_on_email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `markdown_components`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `markdown_components` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `container_id` int(11) NOT NULL,
  `container_type` varchar(100) NOT NULL,
  `body` blob NOT NULL,
  `container_order` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_markdown_components_on_container_id_and_container_type` (`container_id`,`container_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `marketplace_agreement_signatures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marketplace_agreement_signatures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `marketplace_agreement_id` int(11) NOT NULL,
  `organization_id` int(11) DEFAULT NULL,
  `signatory_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_mkt_agreement_sigs_on_mkt_agr_id_sig_id` (`marketplace_agreement_id`,`signatory_id`),
  KEY `index_marketplace_agreement_signatures_on_signatory_id` (`signatory_id`),
  KEY `index_mkt_agreement_sigs_on_org_id_mkt_agr_id` (`organization_id`,`marketplace_agreement_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `marketplace_agreements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marketplace_agreements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `body` mediumblob NOT NULL,
  `version` varchar(30) NOT NULL,
  `signatory_type` smallint(2) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_marketplace_agreements_on_signatory_type` (`signatory_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `marketplace_blog_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marketplace_blog_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varbinary(200) NOT NULL,
  `external_post_id` int(11) NOT NULL,
  `url` varchar(175) NOT NULL,
  `description` varbinary(700) DEFAULT NULL,
  `author` varchar(75) NOT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `published_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_marketplace_blog_posts_on_external_post_id` (`external_post_id`),
  KEY `index_marketplace_blog_posts_on_featured_and_published_at` (`featured`,`published_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `marketplace_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marketplace_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `primary_listing_count` int(11) NOT NULL DEFAULT '0',
  `secondary_listing_count` int(11) NOT NULL DEFAULT '0',
  `description` varbinary(1024) DEFAULT NULL,
  `how_it_works` blob,
  `navigation_visible` tinyint(1) NOT NULL DEFAULT '0',
  `parent_category_id` int(11) DEFAULT NULL,
  `acts_as_filter` tinyint(1) NOT NULL DEFAULT '0',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `featured_position` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_marketplace_categories_on_name` (`name`),
  UNIQUE KEY `index_marketplace_categories_on_slug` (`slug`),
  UNIQUE KEY `index_marketplace_categories_on_featured_and_featured_position` (`featured`,`featured_position`),
  KEY `index_marketplace_categories_on_primary_listing_count` (`primary_listing_count`),
  KEY `index_marketplace_categories_on_secondary_listing_count` (`secondary_listing_count`),
  KEY `index_marketplace_categories_on_parent_id_and_navigation_visible` (`parent_category_id`,`navigation_visible`),
  KEY `index_marketplace_categories_on_navigation_visible` (`navigation_visible`),
  KEY `index_marketplace_categories_on_acts_as_filter` (`acts_as_filter`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `marketplace_categories_listings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marketplace_categories_listings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `marketplace_category_id` int(11) NOT NULL,
  `marketplace_listing_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_marketplace_categories_listings_on_category_and_listing` (`marketplace_category_id`,`marketplace_listing_id`),
  KEY `index_marketplace_categories_listings_on_marketplace_listing_id` (`marketplace_listing_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `marketplace_categories_repository_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marketplace_categories_repository_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `marketplace_category_id` int(11) NOT NULL,
  `repository_action_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_marketplace_categories_repository_actions_on_ids` (`marketplace_category_id`,`repository_action_id`),
  KEY `index_marketplace_categories_repository_actions_on_action_id` (`repository_action_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `marketplace_listing_featured_organizations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marketplace_listing_featured_organizations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `marketplace_listing_id` int(11) NOT NULL,
  `organization_id` int(11) NOT NULL,
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_marketplace_listing_featured_organizations_on_listing_org` (`marketplace_listing_id`,`organization_id`),
  KEY `index_marketplace_listing_featured_organizations_on_org_id` (`organization_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `marketplace_listing_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marketplace_listing_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `marketplace_listing_id` int(11) NOT NULL,
  `uploader_id` int(11) NOT NULL,
  `content_type` varchar(40) NOT NULL,
  `size` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `guid` varchar(36) DEFAULT NULL,
  `state` int(11) DEFAULT '0',
  `storage_blob_id` int(11) DEFAULT NULL,
  `oid` varchar(64) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_marketplace_listing_images_on_marketplace_listing_id` (`marketplace_listing_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `marketplace_listing_insights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marketplace_listing_insights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recorded_on` date NOT NULL,
  `marketplace_listing_id` int(11) NOT NULL,
  `pageviews` int(11) NOT NULL DEFAULT '0',
  `visitors` int(11) NOT NULL DEFAULT '0',
  `new_purchases` int(11) NOT NULL DEFAULT '0',
  `new_seats` int(11) NOT NULL DEFAULT '0',
  `upgrades` int(11) NOT NULL DEFAULT '0',
  `upgraded_seats` int(11) NOT NULL DEFAULT '0',
  `downgrades` int(11) NOT NULL DEFAULT '0',
  `downgraded_seats` int(11) NOT NULL DEFAULT '0',
  `cancellations` int(11) NOT NULL DEFAULT '0',
  `cancelled_seats` int(11) NOT NULL DEFAULT '0',
  `installs` int(11) NOT NULL DEFAULT '0',
  `mrr_gained` int(11) NOT NULL DEFAULT '0',
  `mrr_lost` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `mrr_recurring` int(11) NOT NULL DEFAULT '0',
  `landing_uniques` int(11) NOT NULL DEFAULT '0',
  `checkout_uniques` int(11) NOT NULL DEFAULT '0',
  `new_free_subscriptions` int(11) NOT NULL DEFAULT '0',
  `new_paid_subscriptions` int(11) NOT NULL DEFAULT '0',
  `new_free_trial_subscriptions` int(11) NOT NULL DEFAULT '0',
  `free_trial_conversions` int(11) NOT NULL DEFAULT '0',
  `free_trial_cancellations` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_on_marketplace_listing_id_and_recorded_on` (`marketplace_listing_id`,`recorded_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `marketplace_listing_plan_bullets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marketplace_listing_plan_bullets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(255) NOT NULL,
  `marketplace_listing_plan_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_marketplace_listing_plan_bullets_on_marketpl_list_plan_id` (`marketplace_listing_plan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `marketplace_listing_plans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marketplace_listing_plans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `marketplace_listing_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` mediumblob NOT NULL,
  `monthly_price_in_cents` int(11) NOT NULL DEFAULT '0',
  `per_unit` tinyint(1) NOT NULL DEFAULT '0',
  `yearly_price_in_cents` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `unit_name` varchar(30) DEFAULT NULL,
  `state` smallint(2) NOT NULL DEFAULT '2',
  `number` int(11) NOT NULL DEFAULT '0',
  `has_free_trial` tinyint(1) NOT NULL DEFAULT '0',
  `direct_billing` tinyint(1) NOT NULL DEFAULT '0',
  `subscription_rules` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_marketplace_listing_plans_on_name_and_listing_id_and_state` (`name`,`marketplace_listing_id`,`state`),
  KEY `index_listing_plans_on_listing_and_number` (`marketplace_listing_id`,`number`),
  KEY `index_listing_plans_on_listing_and_monthly_price_in_cents` (`marketplace_listing_id`,`monthly_price_in_cents`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `marketplace_listing_screenshots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marketplace_listing_screenshots` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `marketplace_listing_id` int(11) NOT NULL,
  `sequence` int(11) DEFAULT NULL,
  `uploader_id` int(11) NOT NULL,
  `content_type` varchar(255) NOT NULL,
  `size` int(11) NOT NULL,
  `guid` varchar(36) DEFAULT NULL,
  `state` int(11) DEFAULT '0',
  `storage_blob_id` int(11) DEFAULT NULL,
  `oid` varchar(64) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `caption` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_marketplace_listing_screenshots_on_listing_sequence` (`marketplace_listing_id`,`sequence`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `marketplace_listing_supported_languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marketplace_listing_supported_languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `marketplace_listing_id` int(11) NOT NULL,
  `language_name_id` int(11) NOT NULL,
  `sequence` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_marketplace_listing_supported_language_on_listing_sequence` (`marketplace_listing_id`,`sequence`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `marketplace_listings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marketplace_listings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `short_description` varchar(255) DEFAULT NULL,
  `full_description` blob,
  `extended_description` blob,
  `primary_category_id` int(11) DEFAULT NULL,
  `secondary_category_id` int(11) DEFAULT NULL,
  `privacy_policy_url` text,
  `tos_url` text,
  `company_url` text,
  `status_url` text,
  `support_url` text,
  `documentation_url` text,
  `pricing_url` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `bgcolor` varchar(6) NOT NULL DEFAULT 'ffffff',
  `light_text` tinyint(1) NOT NULL DEFAULT '0',
  `learn_more_url` text,
  `installation_url` text,
  `how_it_works` blob,
  `hero_card_background_image_id` int(11) DEFAULT NULL,
  `featured_at` datetime DEFAULT NULL,
  `technical_email` varchar(255) DEFAULT NULL,
  `marketing_email` varchar(255) DEFAULT NULL,
  `finance_email` varchar(255) DEFAULT NULL,
  `direct_billing_enabled` tinyint(1) NOT NULL DEFAULT '0',
  `by_github` tinyint(1) NOT NULL DEFAULT '0',
  `security_email` varchar(255) DEFAULT NULL,
  `listable_type` varchar(255) NOT NULL,
  `listable_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_marketplace_listings_on_slug` (`slug`),
  UNIQUE KEY `index_marketplace_listings_on_listable_id_and_listable_type` (`listable_id`,`listable_type`),
  KEY `index_marketplace_listings_on_primary_category_id_and_state` (`primary_category_id`,`state`),
  KEY `index_marketplace_listings_on_name` (`name`),
  KEY `index_marketplace_listings_on_state_and_featured_at` (`state`,`featured_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `marketplace_order_previews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marketplace_order_previews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `marketplace_listing_id` int(11) NOT NULL,
  `marketplace_listing_plan_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `viewed_at` datetime NOT NULL,
  `email_notification_sent_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `retargeting_notice_triggered_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_marketplace_order_previews_on_user_id_and_listing_id` (`user_id`,`marketplace_listing_id`),
  KEY `index_marketplace_order_previews_on_viewed_at_and_sent_at` (`viewed_at`,`email_notification_sent_at`),
  KEY `index_marketplace_order_previews_retargeting_triggered` (`retargeting_notice_triggered_at`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `media_blobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_blobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oid` varchar(64) DEFAULT NULL,
  `asset_id` int(11) DEFAULT NULL,
  `repository_network_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `size` bigint(20) NOT NULL DEFAULT '0',
  `charset` varchar(255) DEFAULT NULL,
  `originating_repository_id` int(11) DEFAULT NULL,
  `pusher_id` int(11) DEFAULT NULL,
  `last_downloaded_at` datetime DEFAULT NULL,
  `state` int(11) DEFAULT '0',
  `storage_blob_id` int(11) DEFAULT NULL,
  `storage_provider` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_media_blobs_on_oid_and_network_repository_id` (`oid`,`repository_network_id`),
  KEY `index_media_blobs_on_repo_network_id_and_last_downloaded_at` (`repository_network_id`,`last_downloaded_at`),
  KEY `index_media_blobs_on_repo_network_id_state_created_at_and_size` (`repository_network_id`,`state`,`created_at`,`size`),
  KEY `index_media_blobs_on_repository_network_id` (`repository_network_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `media_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_media_statuses_on_owner_id` (`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `media_transitions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_transitions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_network_id` int(11) NOT NULL,
  `old_repository_network_id` int(11) DEFAULT NULL,
  `last_blob_id` int(11) DEFAULT '0',
  `operation` int(11) DEFAULT '0',
  `runs` int(11) DEFAULT '0',
  `run_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `repository_network_owner_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_media_transitions_on_operation_and_id` (`operation`,`id`),
  KEY `index_media_transitions_on_repository_network_id` (`repository_network_id`),
  KEY `index_media_transitions_on_old_repository_network_id` (`old_repository_network_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `memex_project_column_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `memex_project_column_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `memex_project_column_id` int(11) NOT NULL,
  `memex_project_item_id` int(11) NOT NULL,
  `value` mediumblob,
  `creator_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_memex_project_column_values_on_column_and_item` (`memex_project_column_id`,`memex_project_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `memex_project_columns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `memex_project_columns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `memex_project_id` int(11) NOT NULL,
  `name` varbinary(255) NOT NULL,
  `user_defined` tinyint(1) NOT NULL,
  `position` tinyint(4) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `data_type` tinyint(4) NOT NULL,
  `settings` json DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_memex_project_columns_on_memex_project_id_and_position` (`memex_project_id`,`position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `memex_project_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `memex_project_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `memex_project_id` int(11) NOT NULL,
  `content_type` varchar(20) DEFAULT NULL,
  `content_id` int(11) DEFAULT NULL,
  `creator_id` int(11) NOT NULL,
  `priority` bigint(20) unsigned DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `repository_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_memex_project_items_on_memex_project_id_and_priority` (`memex_project_id`,`priority`),
  UNIQUE KEY `index_memex_project_items_on_memex_project_repository_id_content` (`memex_project_id`,`repository_id`,`content_type`,`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `memex_projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `memex_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_type` varchar(30) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `title` varbinary(1024) DEFAULT NULL,
  `description` mediumblob,
  `public` tinyint(1) NOT NULL DEFAULT '0',
  `closed_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_memex_projects_on_number_and_owner_type_and_owner_id` (`number`,`owner_type`,`owner_id`),
  KEY `index_memex_projects_on_owner_closed_at_and_created_at` (`owner_id`,`owner_type`,`closed_at`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `merge_group_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `merge_group_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `merge_queue_id` int(11) NOT NULL,
  `merge_group_id` int(11) NOT NULL,
  `merge_queue_entry_id` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `retries` int(11) NOT NULL DEFAULT '0',
  `head_oid` varchar(40) DEFAULT NULL,
  `head_ref` varbinary(1024) DEFAULT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `merge_group_queue_entry_id` (`merge_group_id`,`merge_queue_entry_id`),
  KEY `queue_id_queue_entry_id` (`merge_queue_id`,`merge_queue_entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `merge_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `merge_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `merge_queue_id` int(11) NOT NULL,
  `base_oid` varchar(40) DEFAULT NULL,
  `locked` tinyint(1) DEFAULT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `merge_group_entries_count` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_merge_groups_on_merge_queue_id_and_locked` (`merge_queue_id`,`locked`),
  KEY `queue_id_state_entry_count` (`merge_queue_id`,`state`,`merge_group_entries_count`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `merge_queue_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `merge_queue_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) DEFAULT NULL,
  `pull_request_id` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `enqueuer_id` int(11) NOT NULL,
  `enqueued_at` datetime NOT NULL,
  `dequeuer_id` int(11) DEFAULT NULL,
  `dequeued_at` datetime DEFAULT NULL,
  `dequeue_reason` int(11) DEFAULT NULL,
  `deploy_started_at` datetime DEFAULT NULL,
  `solo` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `merge_queue_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_merge_queue_entries_on_queue_and_pr_and_enqueued_at` (`merge_queue_id`,`pull_request_id`,`enqueued_at`),
  KEY `index_merge_queue_entries_on_pull_request_id` (`pull_request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `merge_queues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `merge_queues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `branch` varbinary(1024) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_merge_queues_on_repository_id_and_branch` (`repository_id`,`branch`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `metered_usage_exports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metered_usage_exports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `starts_on` date NOT NULL,
  `ends_on` date NOT NULL,
  `filename` varchar(255) NOT NULL,
  `billable_owner_type` varchar(12) NOT NULL,
  `billable_owner_id` int(11) NOT NULL,
  `requester_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_metered_usage_exports_on_filename` (`filename`),
  KEY `idx_metered_usage_exports_billable_owner` (`billable_owner_type`,`billable_owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `migratable_resource_reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migratable_resource_reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `migration_id` int(11) NOT NULL,
  `model_type` varchar(50) NOT NULL,
  `total_count` int(11) NOT NULL,
  `success_count` int(11) NOT NULL,
  `failure_count` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_migratable_resource_reports_on_migration_id` (`migration_id`),
  KEY `index_migratable_resource_reports_on_model_type` (`model_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `migratable_resources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migratable_resources` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `guid` varchar(36) NOT NULL,
  `model_id` bigint(20) DEFAULT NULL,
  `source_url` text NOT NULL,
  `target_url` text,
  `state` int(11) DEFAULT '0',
  `migration_id` int(11) DEFAULT NULL,
  `model_type` varchar(64) NOT NULL,
  `warning` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_migratable_resources_on_guid_and_source_url` (`guid`,`source_url`(511)),
  KEY `index_migratable_resources_on_guid_and_state_and_model_type` (`guid`,`state`,`model_type`),
  KEY `index_migratable_resources_on_guid_and_model_type` (`guid`,`model_type`),
  KEY `index_migratable_resources_on_guid_and_warning` (`guid`,`warning`(1))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `migration_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `migration_id` int(11) NOT NULL,
  `content_type` varchar(255) NOT NULL,
  `size` bigint(20) NOT NULL,
  `guid` varchar(36) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `uploader_id` int(11) DEFAULT NULL,
  `storage_blob_id` int(11) DEFAULT NULL,
  `oid` varchar(64) DEFAULT NULL,
  `storage_provider` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_migration_files_on_migration_id` (`migration_id`),
  KEY `index_migration_files_on_uploader_id` (`uploader_id`),
  KEY `index_migration_files_on_storage_blob_id` (`storage_blob_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `migration_repositories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration_repositories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `migration_id` int(11) DEFAULT NULL,
  `repository_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_migration_repositories_on_migration_id` (`migration_id`),
  KEY `index_migration_repositories_on_repository_id` (`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `migration_timings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration_timings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `migration_id` int(11) NOT NULL,
  `action` int(11) NOT NULL,
  `time_elapsed` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_migration_timings_on_migration_id` (`migration_id`),
  KEY `index_migration_timings_on_action` (`action`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `guid` varchar(255) NOT NULL,
  `state` int(11) NOT NULL,
  `lock_repositories` tinyint(1) NOT NULL DEFAULT '1',
  `creator_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `exclude_attachments` tinyint(1) DEFAULT '0',
  `migratable_resources_count` int(11) DEFAULT NULL,
  `archive_size` bigint(20) DEFAULT NULL,
  `source_product` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_migrations_on_guid` (`guid`),
  KEY `index_migrations_on_owner_id_and_created_at` (`owner_id`,`created_at`),
  KEY `index_migrations_on_owner_id_and_guid` (`owner_id`,`guid`),
  KEY `index_migrations_on_migratable_resources_count` (`migratable_resources_count`),
  KEY `index_migrations_on_archive_size` (`archive_size`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `milestones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `milestones` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` mediumblob,
  `description` mediumblob,
  `due_on` datetime DEFAULT NULL,
  `created_by_id` int(11) unsigned NOT NULL,
  `repository_id` int(11) unsigned NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `state` varchar(30) DEFAULT NULL,
  `number` int(11) unsigned DEFAULT '0',
  `open_issue_count` int(11) unsigned DEFAULT '0',
  `closed_issue_count` int(11) unsigned DEFAULT '0',
  `closed_at` timestamp NULL DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_milestones_on_created_by_id` (`created_by_id`),
  KEY `index_milestones_on_number` (`number`),
  KEY `index_milestones_on_repository_id` (`repository_id`),
  KEY `index_milestones_on_user_hidden_and_created_by_id` (`user_hidden`,`created_by_id`),
  KEY `index_milestones_on_repository_id_and_updated_at` (`repository_id`,`updated_at`),
  KEY `index_milestones_on_repository_id_and_state_and_updated_at` (`repository_id`,`state`,`updated_at`),
  KEY `index_milestones_on_repository_id_and_due_on` (`repository_id`,`due_on`),
  KEY `index_milestones_on_repository_id_and_state_and_due_on` (`repository_id`,`state`,`due_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `mirrors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mirrors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL,
  `repository_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_mirrors_on_repository_id` (`repository_id`),
  KEY `index_mirrors_on_updated_at` (`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `mobile_device_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_device_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `service` tinyint(4) NOT NULL DEFAULT '0',
  `device_token` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `device_name` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_mobile_device_tokens_on_user_id_and_service_and_token` (`user_id`,`service`,`device_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `mobile_push_notification_deliveries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_push_notification_deliveries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mobile_device_token_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `list_type` varchar(64) NOT NULL,
  `list_id` int(11) NOT NULL,
  `thread_type` varchar(64) NOT NULL,
  `thread_id` int(11) NOT NULL,
  `comment_type` varchar(64) NOT NULL,
  `comment_id` int(11) NOT NULL,
  `reason` varchar(40) DEFAULT NULL,
  `state` tinyint(4) NOT NULL DEFAULT '0',
  `state_explanation` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_deliveries_on_comment_and_token` (`comment_type`,`comment_id`,`mobile_device_token_id`),
  KEY `index_deliveries_on_state_and_list_and_thread` (`state`,`list_type`,`list_id`,`thread_type`,`thread_id`),
  KEY `index_deliveries_on_user_and_state` (`user_id`,`state`),
  KEY `index_deliveries_on_updated_at` (`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `mobile_push_notification_schedules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_push_notification_schedules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `day` tinyint(4) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_schedules_on_day_and_user` (`user_id`,`day`),
  KEY `index_schedules_on_user_and_day_and_start_time_and_end_time` (`user_id`,`day`,`start_time`,`end_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `network_privileges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `network_privileges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `noindex` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `require_login` tinyint(1) NOT NULL DEFAULT '0',
  `collaborators_only` tinyint(1) NOT NULL DEFAULT '0',
  `hide_from_discovery` tinyint(1) NOT NULL DEFAULT '0',
  `require_opt_in` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_network_privileges_on_repository_id` (`repository_id`),
  UNIQUE KEY `index_repository_and_hide_from_discovery` (`repository_id`,`hide_from_discovery`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `network_replicas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `network_replicas` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `network_id` int(11) NOT NULL,
  `host` varchar(255) NOT NULL,
  `state` int(11) NOT NULL,
  `read_weight` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_on_host` (`network_id`,`host`),
  KEY `index_network_replicas_on_state` (`state`),
  KEY `index_network_replicas_on_host_and_state_and_network_id` (`host`,`state`,`network_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `newsletter_preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_preferences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `elected_marketing_at` datetime DEFAULT NULL,
  `elected_transactional_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_newsletter_preferences_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `newsletter_subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `kind` varchar(255) NOT NULL,
  `sent_at` datetime DEFAULT NULL,
  `next_delivery_at` datetime DEFAULT NULL,
  `delivery_count` int(11) DEFAULT '0',
  `active` tinyint(1) DEFAULT '0',
  `subscribed_at` datetime DEFAULT NULL,
  `auto_subscribed` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_newsletter_subscriptions_on_user_id_and_name` (`user_id`,`name`),
  KEY `index_newsletter_subscriptions_on_active_and_next_delivery_at` (`active`,`next_delivery_at`),
  KEY `index_newsletter_subscriptions_on_user_id_and_active_and_name` (`user_id`,`active`,`name`),
  KEY `index_newsletter_subscriptions_on_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `non_marketplace_listings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `non_marketplace_listings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `url` text NOT NULL,
  `integration_listing_id` int(11) DEFAULT NULL,
  `integration_feature_id` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `creator_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `enterprise_compatible` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_non_marketplace_listings_on_name` (`name`),
  KEY `index_non_marketplace_listings_on_integration_listing_id` (`integration_listing_id`),
  KEY `index_non_mkt_listings_on_int_feature_id_and_state` (`integration_feature_id`,`state`),
  KEY `index_non_marketplace_listings_on_creator_id` (`creator_id`),
  KEY `index_non_marketplace_listings_on_enterprise_compatible` (`enterprise_compatible`),
  KEY `index_non_marketplace_listings_on_state_and_name` (`state`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `notification_deliveries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification_deliveries` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `delivered_at` datetime NOT NULL,
  `list_id` int(11) NOT NULL,
  `thread_key` varchar(255) NOT NULL,
  `comment_key` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `handler` varchar(255) NOT NULL,
  `reason` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `by_list` (`list_id`,`thread_key`,`comment_key`,`user_id`,`handler`),
  KEY `by_time` (`delivered_at`),
  KEY `by_user` (`user_id`,`list_id`,`thread_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `notification_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification_entries` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `summary_id` int(11) NOT NULL,
  `list_type` varchar(64) NOT NULL DEFAULT 'Repository',
  `list_id` int(11) NOT NULL,
  `thread_key` varchar(80) NOT NULL,
  `unread` tinyint(1) DEFAULT '1',
  `reason` varchar(40) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `last_read_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `by_summary` (`user_id`,`summary_id`),
  UNIQUE KEY `index_notification_entries_for_user_by_thread` (`user_id`,`list_type`,`list_id`,`thread_key`),
  KEY `by_user` (`user_id`,`reason`),
  KEY `by_unread_user` (`user_id`,`unread`,`reason`),
  KEY `index_notification_entries_on_unread_and_updated_at` (`unread`,`updated_at`),
  KEY `index_notification_entries_on_user_id_and_updated_at` (`user_id`,`updated_at`),
  KEY `index_notification_entries_for_list_by_thread` (`list_type`,`list_id`,`thread_key`),
  KEY `index_notification_entries_for_user_by_list` (`user_id`,`list_type`,`list_id`,`reason`),
  KEY `index_notification_entries_for_user_by_unread_list` (`user_id`,`list_type`,`list_id`,`unread`,`reason`),
  KEY `user_id_and_unread_and_thread_key_and_list_type_and_list_id` (`user_id`,`unread`,`thread_key`,`list_type`,`list_id`),
  KEY `user_id_and_unread_and_updated_at` (`user_id`,`unread`,`updated_at`),
  KEY `user_id_and_unread_and_list_type_and_updated_at` (`user_id`,`unread`,`list_type`,`updated_at`),
  KEY `user_id_and_list_type_and_updated_at` (`user_id`,`list_type`,`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `notification_subscription_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification_subscription_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subscription_type` varchar(255) NOT NULL,
  `subscription_id` bigint(11) NOT NULL,
  `event_name` varchar(32) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_events_on_subscription_type_id_and_name` (`subscription_type`,`subscription_id`,`event_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `notification_subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification_subscriptions` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `list_type` varchar(64) NOT NULL DEFAULT 'Repository',
  `list_id` int(11) unsigned NOT NULL,
  `ignored` tinyint(1) unsigned NOT NULL,
  `notified` tinyint(1) unsigned DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_notification_subscriptions_on_user_list_type_and_list_id` (`user_id`,`list_type`,`list_id`),
  KEY `index_notification_subscriptions_on_list_ignored_and_created_at` (`list_type`,`list_id`,`ignored`,`created_at`),
  KEY `index_notification_subscriptions_on_notified_list_type_and_user` (`notified`,`list_type`,`user_id`),
  KEY `index_notification_subscriptions_on_user_list_ignored_created_at` (`user_id`,`list_type`,`ignored`,`created_at`),
  KEY `index_notification_subscriptions_on_notified_and_user_id` (`notified`,`user_id`),
  KEY `index_notification_subscriptions_on_list_ignored_and_user` (`list_type`,`list_id`,`ignored`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `notification_thread_subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification_thread_subscriptions` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `list_type` varchar(64) NOT NULL DEFAULT 'Repository',
  `list_id` int(11) unsigned NOT NULL,
  `ignored` tinyint(1) unsigned NOT NULL,
  `reason` varchar(40) DEFAULT NULL,
  `thread_key` varchar(80) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_on_list_type_and_list_id_and_thread_key_and_user_id` (`list_type`,`list_id`,`thread_key`,`user_id`),
  KEY `index_notification_thread_subscriptions_on_lt_li_thrd_and_ignore` (`list_type`,`list_id`,`thread_key`,`ignored`),
  KEY `user_id_and_ignored_and_list_type_and_list_id_and_thread_key` (`user_id`,`ignored`,`list_type`,`list_id`,`thread_key`),
  KEY `index_on_list_id_user_id_reason_list_type_ignored` (`list_id`,`user_id`,`reason`,`list_type`,`ignored`),
  KEY `index_on_user_id_list_type_ignored` (`user_id`,`list_type`,`ignored`),
  KEY `index_on_list_id_user_id_list_type_ignored` (`list_id`,`user_id`,`list_type`,`ignored`),
  KEY `index_on_user_id_reason_list_type_ignored` (`user_id`,`reason`,`list_type`,`ignored`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `notification_thread_type_subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification_thread_type_subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `list_id` int(11) NOT NULL,
  `list_type` varchar(64) NOT NULL DEFAULT 'Repository',
  `thread_type` varchar(64) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_notification_tts_by_list_and_user` (`list_id`,`list_type`,`user_id`,`thread_type`),
  KEY `index_notification_thread_type_subscriptions_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `notification_user_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification_user_settings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `raw_data` blob,
  `auto_subscribe` tinyint(1) NOT NULL DEFAULT '1',
  `auto_subscribe_teams` tinyint(1) NOT NULL DEFAULT '1',
  `notify_own_via_email` tinyint(1) NOT NULL DEFAULT '0',
  `participating_web` tinyint(1) NOT NULL DEFAULT '0',
  `participating_email` tinyint(1) NOT NULL DEFAULT '1',
  `subscribed_web` tinyint(1) NOT NULL DEFAULT '0',
  `subscribed_email` tinyint(1) NOT NULL DEFAULT '1',
  `notify_comment_email` tinyint(1) NOT NULL DEFAULT '1',
  `notify_pull_request_review_email` tinyint(1) NOT NULL DEFAULT '1',
  `notify_pull_request_push_email` tinyint(1) NOT NULL DEFAULT '1',
  `vulnerability_ui_alert` tinyint(1) NOT NULL DEFAULT '1',
  `vulnerability_web` tinyint(1) NOT NULL DEFAULT '0',
  `vulnerability_email` tinyint(1) NOT NULL DEFAULT '0',
  `vulnerability_cli` tinyint(1) NOT NULL DEFAULT '1',
  `participating_web_push` tinyint(1) NOT NULL DEFAULT '0',
  `continuous_integration_email` tinyint(1) NOT NULL DEFAULT '1',
  `continuous_integration_web` tinyint(1) NOT NULL DEFAULT '0',
  `continuous_integration_failures_only` tinyint(1) NOT NULL DEFAULT '1',
  `direct_mention_mobile_push` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `oauth_accesses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_accesses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `application_id` int(11) NOT NULL,
  `code` varchar(20) DEFAULT NULL,
  `raw_data` blob,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `accessed_at` datetime DEFAULT NULL,
  `hashed_token` varchar(44) DEFAULT NULL,
  `token_last_eight` varchar(8) DEFAULT NULL,
  `fingerprint` varchar(255) DEFAULT NULL,
  `authorization_id` int(11) DEFAULT NULL,
  `application_type` varchar(16) DEFAULT NULL,
  `expires_at_timestamp` bigint(20) DEFAULT NULL,
  `installation_id` bigint(20) DEFAULT NULL,
  `installation_type` varchar(33) DEFAULT NULL,
  `is_application` tinyint(1) GENERATED ALWAYS AS ((`application_id` and 1)) STORED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_oauth_accesses_on_hashed_token` (`hashed_token`),
  UNIQUE KEY `index_oauth_accesses_on_appl_id_and_code_and_appl_type` (`application_id`,`code`,`application_type`),
  UNIQUE KEY `index_oauth_accesses_on_user_id_and_appl_id_and_fingerprint` (`user_id`,`application_id`,`fingerprint`),
  UNIQUE KEY `index_oauth_accesses_on_installation_id_and_installation_type` (`installation_id`,`installation_type`),
  KEY `index_oauth_accesses_on_user_id` (`user_id`),
  KEY `index_oauth_accesses_on_authorization_id` (`authorization_id`),
  KEY `index_oauth_accesses_on_accessed_at_and_created_at` (`accessed_at`,`created_at`),
  KEY `index_oauth_accesses_on_application_id_and_created_at` (`application_id`,`created_at`),
  KEY `index_oauth_accesses_on_expires_at_timestamp` (`expires_at_timestamp`),
  KEY `index_oauth_accesses_on_is_application_accessed_at_created_at` (`is_application`,`accessed_at`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `oauth_application_approvals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_application_approvals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organization_id` int(11) NOT NULL,
  `application_id` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `reason` blob,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `requestor_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_allowed_oauth_applications_by_app_and_org` (`organization_id`,`application_id`),
  KEY `index_allowed_oauth_applications_on_application_id` (`application_id`),
  KEY `index_allowed_oauth_applications_on_organization_id_and_state` (`organization_id`,`state`),
  KEY `index_oauth_application_approvals_on_appl_id_and_state_and_org` (`application_id`,`state`,`organization_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `oauth_application_logos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_application_logos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uploader_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type` varchar(255) NOT NULL,
  `size` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `guid` varchar(36) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `application_id` int(11) DEFAULT NULL,
  `storage_blob_id` int(11) DEFAULT NULL,
  `oid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `by_guid` (`guid`),
  KEY `uploader_id` (`uploader_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `oauth_application_transfers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_application_transfers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `application_id` int(11) NOT NULL,
  `requester_id` int(11) NOT NULL,
  `responder_id` int(11) DEFAULT NULL,
  `target_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_oauth_app_xfer_app_id_target_id` (`application_id`,`target_id`),
  KEY `index_oauth_application_transfers_on_target_id` (`target_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `oauth_applications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_applications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `url` text,
  `callback_url` text,
  `key` varchar(20) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `domain` varchar(100) DEFAULT NULL,
  `rate_limit` int(11) unsigned DEFAULT NULL,
  `full_trust` tinyint(1) unsigned DEFAULT '0',
  `description` text,
  `logo_id` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT '0',
  `raw_data` blob,
  `temporary_rate_limit` mediumint(9) DEFAULT NULL,
  `temporary_rate_limit_expires_at` datetime DEFAULT NULL,
  `bgcolor` varchar(6) NOT NULL DEFAULT 'ffffff',
  `encrypted_secret` varbinary(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_oauth_applications_on_key` (`key`),
  KEY `index_oauth_applications_on_user_id_and_name` (`user_id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `oauth_authorizations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_authorizations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `application_id` int(11) NOT NULL,
  `accessed_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `scopes` text,
  `application_type` varchar(16) DEFAULT NULL,
  `integration_version_number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_oauth_authorizations_on_accessed_at_and_created_at` (`accessed_at`,`created_at`),
  KEY `index_oauth_authorizations_on_user_id_and_appl_id_and_appl_type` (`user_id`,`application_id`,`application_type`),
  KEY `index_oauth_authorizations_on_application_id_and_created_at` (`application_id`,`created_at`),
  KEY `index_authorizations_on_application_id_type_version_number` (`application_id`,`application_type`,`integration_version_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `oauth_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_logs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `application_id` int(11) unsigned DEFAULT NULL,
  `actor_id` int(11) unsigned DEFAULT NULL,
  `repository_id` int(11) unsigned DEFAULT NULL,
  `controller_action` varchar(255) DEFAULT NULL,
  `scopes` varchar(255) DEFAULT NULL,
  `repository_name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_oauth_logs_on_application_id_and_created_at` (`application_id`,`created_at`),
  KEY `index_oauth_logs_on_repository_id_and_created_at` (`repository_id`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ofac_downgrades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ofac_downgrades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `downgrade_on` date DEFAULT NULL,
  `is_complete` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_ofac_downgrades_on_user_id` (`user_id`),
  KEY `index_ofac_downgrades_on_downgrade_on` (`downgrade_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `onboarding_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `onboarding_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_onboarding_events_on_user_id_and_name` (`user_id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `organization_credential_authorizations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organization_credential_authorizations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organization_id` int(11) NOT NULL,
  `credential_id` int(11) NOT NULL,
  `credential_type` varchar(30) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `actor_type` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `revoked_at` datetime DEFAULT NULL,
  `revoked_by_id` int(11) DEFAULT NULL,
  `fingerprint` char(48) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_org_credential_authorizations_on_org_id_and_fingerprint` (`organization_id`,`fingerprint`),
  KEY `index_org_credential_authorizations_on_fingerprint_and_cred_type` (`fingerprint`,`credential_type`),
  KEY `index_org_cred_authorizations_on_org_and_cred_and_revoked_by` (`organization_id`,`credential_id`,`credential_type`,`revoked_by_id`),
  KEY `index_org_credential_authorizations_on_org_id_and_actor_id` (`organization_id`,`actor_id`),
  KEY `index_on_credential_id_and_credential_type_and_revoked_by_id` (`credential_id`,`credential_type`,`revoked_by_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `organization_discussion_post_replies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organization_discussion_post_replies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` int(11) NOT NULL,
  `body` mediumblob NOT NULL,
  `formatter` varchar(20) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `organization_discussion_post_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_org_disc_replies_organization_discussion_post_id_and_number` (`organization_discussion_post_id`,`number`),
  KEY `index_organization_discussion_post_replies_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `organization_discussion_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organization_discussion_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` int(11) NOT NULL,
  `title` varbinary(1024) NOT NULL,
  `body` mediumblob NOT NULL,
  `formatter` varchar(20) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `organization_id` int(11) NOT NULL,
  `pinned_at` datetime DEFAULT NULL,
  `pinned_by_user_id` int(11) DEFAULT NULL,
  `private` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_org_discussion_posts_on_organization_id_and_number` (`organization_id`,`number`),
  KEY `index_organization_discussion_posts_on_user_id` (`user_id`),
  KEY `idx_org_discussion_posts_on_organization_id_and_pinned_at` (`organization_id`,`pinned_at`),
  KEY `idx_org_discussion_posts_on_organization_id_and_private` (`organization_id`,`private`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `organization_domains`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organization_domains` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organization_id` int(11) NOT NULL,
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `domain` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_organization_domains_on_domain_and_organization_id` (`domain`,`organization_id`),
  KEY `index_organization_domains_on_organization_id` (`organization_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `organization_invitation_opt_outs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organization_invitation_opt_outs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organization_id` int(11) NOT NULL,
  `organization_invitation_id` int(11) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `org_invitation_opt_outs_org_id_email` (`organization_id`,`email`),
  KEY `org_invitation_opt_outs_email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `organization_invitations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organization_invitations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organization_id` int(11) NOT NULL,
  `inviter_id` int(11) NOT NULL,
  `invitee_id` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `accepted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `role` int(11) NOT NULL DEFAULT '0',
  `cancelled_at` datetime DEFAULT NULL,
  `normalized_email` varchar(255) DEFAULT NULL,
  `hashed_token` varchar(44) DEFAULT NULL,
  `external_identity_id` int(11) DEFAULT NULL,
  `failed_at` datetime DEFAULT NULL,
  `failed_reason` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_by_org_accepted_at_and_normalized_email` (`organization_id`,`normalized_email`,`accepted_at`),
  UNIQUE KEY `index_organization_invitations_on_hashed_token` (`hashed_token`),
  KEY `index_organization_invitations_on_invitee_id` (`invitee_id`),
  KEY `org_id_and_accepted_at_and_cancelled_at_and_role_and_invitee_id` (`organization_id`,`accepted_at`,`cancelled_at`,`role`,`invitee_id`),
  KEY `index_organization_invitations_on_external_identity_id` (`external_identity_id`),
  KEY `org_id_failed_at_accepted_at_cancelled_at_role_invitee_id` (`organization_id`,`failed_at`,`accepted_at`,`cancelled_at`,`role`,`invitee_id`),
  KEY `index_organization_invitations_on_created_at` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `organization_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organization_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sponsors_update_email` varchar(255) DEFAULT NULL,
  `organization_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_organization_profiles_on_organization_id` (`organization_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `organization_saml_provider_test_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organization_saml_provider_test_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `organization_id` int(11) NOT NULL,
  `sso_url` varchar(255) DEFAULT NULL,
  `issuer` varchar(255) DEFAULT NULL,
  `idp_certificate` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `digest_method` int(11) NOT NULL DEFAULT '0',
  `signature_method` int(11) NOT NULL DEFAULT '0',
  `status` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_org_saml_test_settings_on_user_id_and_organization_id` (`user_id`,`organization_id`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `organization_saml_providers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organization_saml_providers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organization_id` int(11) NOT NULL,
  `sso_url` varchar(255) NOT NULL,
  `issuer` varchar(255) DEFAULT NULL,
  `idp_certificate` text NOT NULL,
  `disable_admin_demote` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `enforced` tinyint(1) NOT NULL DEFAULT '0',
  `digest_method` int(11) NOT NULL DEFAULT '0',
  `signature_method` int(11) NOT NULL DEFAULT '0',
  `secret` varchar(255) NOT NULL,
  `recovery_secret` varchar(255) NOT NULL,
  `recovery_used_bitfield` int(11) NOT NULL DEFAULT '0',
  `recovery_codes_viewed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_organization_saml_providers_on_organization_id` (`organization_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `otp_sms_timings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `otp_sms_timings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timing_key` varchar(85) NOT NULL,
  `provider` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_otp_sms_timings_on_timing_key` (`timing_key`),
  KEY `index_otp_sms_timings_on_created_at` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `package_activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `package_activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `activity_started_at` datetime NOT NULL,
  `bandwidth_down` float NOT NULL DEFAULT '0',
  `bandwidth_up` float NOT NULL DEFAULT '0',
  `source_files` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_package_activities_on_package_id_and_activity_started_at` (`package_id`,`activity_started_at`),
  KEY `index_package_activities_on_owner_id_and_activity_started_at` (`owner_id`,`activity_started_at`),
  KEY `index_package_activities_on_repo_id_and_activity_started_at` (`repository_id`,`activity_started_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `package_download_activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `package_download_activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` int(11) NOT NULL,
  `package_version_id` int(11) NOT NULL,
  `package_download_count` int(11) DEFAULT '0',
  `started_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_package_download_activities_on_pkg_version_and_started_at` (`package_version_id`,`started_at`),
  KEY `index_package_download_activities_on_started_at` (`started_at`),
  KEY `index_package_download_activities_on_pkg_id_and_started_at` (`package_id`,`started_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `package_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `package_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_version_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL DEFAULT '',
  `sha1` varchar(40) DEFAULT NULL,
  `md5` varchar(32) DEFAULT NULL,
  `storage_blob_id` int(11) DEFAULT NULL,
  `uploader_id` int(11) DEFAULT NULL,
  `guid` varchar(36) DEFAULT NULL,
  `oid` varchar(64) DEFAULT NULL,
  `size` int(11) NOT NULL,
  `state` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `sha256` varchar(64) DEFAULT NULL,
  `sri_512` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_package_files_on_package_version_id_and_filename` (`package_version_id`,`filename`),
  UNIQUE KEY `index_package_files_on_guid` (`guid`),
  KEY `index_package_files_on_sha256` (`sha256`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `package_registry_data_transfer_aggregations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `package_registry_data_transfer_aggregations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `registry_package_id` int(11) NOT NULL,
  `registry_package_version_id` int(11) NOT NULL,
  `billable_owner_type` varchar(12) NOT NULL,
  `billable_owner_id` int(11) NOT NULL,
  `aggregate_size_in_bytes` bigint(20) NOT NULL,
  `metered_billing_cycle_starts_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_data_transfer_agg_on_billable_owner_and_usage` (`billable_owner_type`,`billable_owner_id`,`metered_billing_cycle_starts_at`,`owner_id`,`registry_package_id`,`registry_package_version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `package_registry_data_transfer_line_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `package_registry_data_transfer_line_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `registry_package_id` int(11) NOT NULL,
  `registry_package_version_id` int(11) NOT NULL,
  `size_in_bytes` bigint(20) NOT NULL,
  `synchronization_batch_id` int(11) DEFAULT NULL,
  `downloaded_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `billable_owner_type` varchar(12) DEFAULT NULL,
  `billable_owner_id` int(11) DEFAULT NULL,
  `directly_billed` tinyint(1) NOT NULL DEFAULT '1',
  `download_id` varchar(50) NOT NULL,
  `submission_state` enum('unsubmitted','submitted','skipped') NOT NULL DEFAULT 'unsubmitted',
  `submission_state_reason` varchar(24) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_package_registry_data_transfer_line_items_on_download_id` (`download_id`),
  KEY `index_package_registry_data_transfer_line_items_on_owner_id` (`owner_id`),
  KEY `index_package_registry_data_transfer_line_items_on_downloaded_at` (`downloaded_at`),
  KEY `index_on_synchronization_batch_id` (`synchronization_batch_id`),
  KEY `index_on_billable_owner_and_usage` (`billable_owner_type`,`billable_owner_id`,`downloaded_at`,`size_in_bytes`),
  KEY `index_on_billable_owner_and_usage_and_repo` (`billable_owner_type`,`billable_owner_id`,`downloaded_at`,`registry_package_id`,`size_in_bytes`),
  KEY `index_packages_line_items_on_submission_state_and_reason` (`submission_state`,`submission_state_reason`),
  KEY `index_on_submission_state_and_created_at` (`submission_state`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `package_version_package_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `package_version_package_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_version_id` int(11) NOT NULL,
  `package_file_id` int(11) NOT NULL,
  `package_file_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_package_version_package_files_on_version_id_and_file_id` (`package_version_id`,`package_file_id`),
  KEY `index_package_version_package_files_on_file_id` (`package_file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `package_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `package_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `registry_package_id` int(11) NOT NULL,
  `release_id` int(11) DEFAULT NULL,
  `platform` varchar(255) NOT NULL DEFAULT '',
  `version` varchar(255) NOT NULL,
  `commit_oid` varchar(40) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `sha256` varchar(64) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `manifest` mediumblob,
  `author_id` int(11) DEFAULT NULL,
  `pre_release` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `published_via_actions` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_by_id` int(11) DEFAULT NULL,
  `files_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_registry_package_versions_on_package_id_and_version` (`registry_package_id`,`version`,`platform`),
  KEY `index_package_versions_on_release_id` (`release_id`),
  KEY `index_package_versions_on_author_id` (`author_id`),
  KEY `index_package_versions_on_package_pre_release_and_version` (`registry_package_id`,`pre_release`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `page_builds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_builds` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(11) unsigned DEFAULT NULL,
  `page_deployment_id` int(11) DEFAULT NULL,
  `pages_deployment_id` int(11) DEFAULT NULL,
  `raw_data` blob,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `pusher_id` int(11) DEFAULT NULL,
  `commit` char(40) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `error` text,
  `backtrace` text,
  `duration` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_page_builds_on_updated_at` (`updated_at`),
  KEY `index_page_builds_on_page_id_and_updated_at` (`page_id`,`updated_at`),
  KEY `index_page_builds_on_pages_deployment_id_and_updated_at` (`pages_deployment_id`,`updated_at`),
  KEY `index_page_builds_on_page_deployment_id_and_updated_at` (`page_deployment_id`,`updated_at`),
  KEY `index_page_builds_on_page_id_and_commit` (`page_id`,`commit`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `page_certificates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_certificates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(255) NOT NULL,
  `state` int(11) DEFAULT NULL,
  `state_detail` text,
  `expires_at` datetime DEFAULT NULL,
  `challenge_path` text,
  `challenge_response` text,
  `earthsmoke_key_version_id` bigint(20) unsigned DEFAULT NULL,
  `fastly_privkey_id` text,
  `authorization_url` text,
  `certificate_url` text,
  `fastly_certificate_id` varchar(255) DEFAULT NULL,
  `order_url` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_page_certificates_on_domain` (`domain`),
  KEY `index_page_certificates_on_state_and_expires_at` (`state`,`expires_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `page_deployments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_deployments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `ref_name` varbinary(1024) NOT NULL,
  `revision` varchar(40) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `token` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_page_deployments_on_page_id_and_ref_name` (`page_id`,`ref_name`(767)),
  UNIQUE KEY `index_page_deployments_on_page_id_and_token` (`page_id`,`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) DEFAULT NULL,
  `cname` varchar(255) DEFAULT NULL,
  `four_oh_four` tinyint(1) DEFAULT '0',
  `status` varchar(20) DEFAULT NULL,
  `has_public_search` tinyint(1) DEFAULT '0',
  `built_revision` varchar(40) DEFAULT NULL,
  `https_redirect` tinyint(1) NOT NULL DEFAULT '0',
  `hsts_max_age` int(11) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  `source_ref_name` varbinary(1024) DEFAULT NULL,
  `source_subdir` varbinary(1024) DEFAULT NULL,
  `hsts_include_sub_domains` tinyint(1) NOT NULL DEFAULT '0',
  `hsts_preload` tinyint(1) NOT NULL DEFAULT '0',
  `public` tinyint(1) NOT NULL DEFAULT '1',
  `subdomain` varchar(130) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_pages_on_repository_id` (`repository_id`),
  UNIQUE KEY `index_pages_on_unique_cname` (`cname`),
  UNIQUE KEY `index_pages_on_subdomain` (`subdomain`),
  KEY `index_pages_on_source_ref_name` (`source_ref_name`(30)),
  KEY `index_subdomain_on_repository_id` (`subdomain`,`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `pages_embargoed_cnames`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_embargoed_cnames` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `previous_owner_id` int(11) NOT NULL,
  `previous_repository_id` int(11) NOT NULL,
  `cname` varbinary(1024) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_pages_embargoed_cnames_cname` (`cname`),
  KEY `index_pages_embargoed_cnames_previous_owner_id` (`previous_owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `pages_fileservers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_fileservers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `host` varchar(255) NOT NULL,
  `online` tinyint(1) NOT NULL,
  `embargoed` tinyint(1) NOT NULL,
  `evacuating` tinyint(1) NOT NULL DEFAULT '0',
  `disk_free` bigint(20) unsigned NOT NULL DEFAULT '0',
  `disk_used` bigint(20) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `non_voting` tinyint(1) NOT NULL DEFAULT '0',
  `datacenter` varchar(20) DEFAULT NULL,
  `rack` varchar(20) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `inodes_free` bigint(20) unsigned NOT NULL DEFAULT '0',
  `inodes_used` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_pages_fileservers_on_host` (`host`),
  KEY `index_pages_fileservers_by_location` (`datacenter`,`rack`),
  KEY `index_pages_fileservers_on_evacuating` (`evacuating`),
  KEY `index_pages_fileservers_on_online_and_embargoed_and_host` (`online`,`embargoed`,`host`),
  KEY `index_pages_fileservers_on_voting_and_online_and_embarg_and_host` (`non_voting`,`online`,`embargoed`,`host`),
  KEY `index_pages_fileservers_on_voting_and_online_and_embarg_and_df` (`non_voting`,`online`,`embargoed`,`disk_free`),
  KEY `index_pages_fileservers_on_voting_online_embargo_df_inodes` (`non_voting`,`online`,`embargoed`,`disk_free`,`inodes_free`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `pages_partitions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_partitions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `host` varchar(255) NOT NULL,
  `partition` varchar(1) NOT NULL,
  `disk_free` bigint(20) NOT NULL,
  `disk_used` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_pages_partitions_on_host_and_partition` (`host`,`partition`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `pages_replicas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_replicas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `host` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `pages_deployment_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_pages_replicas_on_host` (`host`),
  KEY `index_pages_replicas_on_pages_deployment_id_and_host` (`pages_deployment_id`,`host`),
  KEY `index_pages_replicas_on_page_id_and_host` (`page_id`,`host`),
  KEY `index_pages_replicas_on_page_id_and_pages_deployment_id_and_host` (`page_id`,`pages_deployment_id`,`host`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `pages_routes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `host` varchar(255) NOT NULL,
  `https_behavior` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_pages_routes_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `payloads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payloads` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `body` mediumblob COMMENT 'The body of the persisted payload',
  `compression_type` int(9) NOT NULL DEFAULT '0' COMMENT 'See CompressionType for values',
  `workflow_build_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `by_created_at` (`created_at`),
  KEY `by_workflow_build_id` (`workflow_build_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `payment_methods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_methods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `payment_processor_type` varchar(30) NOT NULL DEFAULT 'zuora',
  `payment_processor_customer_id` varchar(40) NOT NULL,
  `payment_token` varchar(32) NOT NULL,
  `primary` tinyint(1) NOT NULL DEFAULT '1',
  `country` varchar(3) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `truncated_number` varchar(20) DEFAULT NULL,
  `expiration_month` int(11) DEFAULT NULL,
  `expiration_year` int(11) DEFAULT NULL,
  `expiration_reminders` int(11) NOT NULL DEFAULT '0',
  `card_type` varchar(30) DEFAULT NULL,
  `paypal_email` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `unique_number_identifier` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_payment_methods_on_user_id_and_primary` (`user_id`,`primary`),
  KEY `index_payment_methods_on_customer_id_and_primary` (`customer_id`,`primary`),
  KEY `index_payment_methods_on_payment_token` (`payment_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `pending_automatic_installations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pending_automatic_installations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trigger_type` varchar(50) NOT NULL,
  `target_type` varchar(50) NOT NULL,
  `target_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `reason` tinyint(4) NOT NULL DEFAULT '0',
  `installed_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_pending_automatic_installations_on_trigger_and_target` (`trigger_type`,`target_type`,`target_id`),
  KEY `index_pending_automatic_installations_on_target` (`target_type`,`target_id`),
  KEY `index_pending_automatic_installations_on_trigger_type_and_status` (`trigger_type`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `pending_plan_changes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pending_plan_changes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plan` varchar(255) DEFAULT NULL,
  `plan_duration` varchar(255) DEFAULT NULL,
  `seats` int(11) DEFAULT NULL,
  `is_complete` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) DEFAULT NULL,
  `actor_id` int(11) DEFAULT NULL,
  `active_on` date DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `data_packs` int(11) DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_pending_plan_changes_on_user_id_and_is_complete` (`user_id`,`is_complete`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `pending_subscription_item_changes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pending_subscription_item_changes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quantity` int(11) NOT NULL DEFAULT '1',
  `pending_plan_change_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `free_trial` tinyint(1) NOT NULL DEFAULT '0',
  `subscribable_id` int(11) DEFAULT NULL,
  `subscribable_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_pending_subscription_item_changes_on_free_trial` (`free_trial`),
  KEY `index_pend_sub_item_changes_on_pend_plan_change_and_subscribable` (`pending_plan_change_id`,`subscribable_type`,`subscribable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `pending_trade_controls_restrictions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pending_trade_controls_restrictions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `target_id` int(11) NOT NULL,
  `enforcement_on` datetime NOT NULL,
  `reason` enum('organization_admin','organization_billing_manager','organization_member','organization_outside_collaborator','location') NOT NULL,
  `enforcement_status` enum('completed','cancelled','pending') NOT NULL DEFAULT 'pending',
  `last_restriction_threshold` decimal(5,2) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_pending_trade_controls_restrictions_on_target_id` (`target_id`),
  KEY `index_pending_trade_controls_restrictions_on_enforcement_on` (`enforcement_on`),
  KEY `index_pending_trade_controls_restrictions_on_reason` (`reason`),
  KEY `index_enforcement_status_on_date` (`enforcement_status`,`enforcement_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `pending_vulnerabilities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pending_vulnerabilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `external_identifier` varchar(255) DEFAULT NULL,
  `description` blob,
  `status` varchar(12) NOT NULL,
  `platform` varchar(50) DEFAULT NULL,
  `severity` varchar(12) DEFAULT NULL,
  `classification` varchar(255) DEFAULT NULL,
  `external_reference` varchar(255) DEFAULT NULL,
  `created_by_id` int(11) NOT NULL,
  `reviewer_a_id` int(11) DEFAULT NULL,
  `review_a_at` timestamp NULL DEFAULT NULL,
  `reviewer_b_id` int(11) DEFAULT NULL,
  `review_b_at` timestamp NULL DEFAULT NULL,
  `submitted_by_id` int(11) DEFAULT NULL,
  `submitted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `review_notes` mediumblob,
  `vulnerability_id` int(11) DEFAULT NULL,
  `reject_reason` varchar(32) DEFAULT NULL,
  `ghsa_id` varchar(19) NOT NULL,
  `source` varchar(64) DEFAULT NULL,
  `source_identifier` varchar(128) DEFAULT NULL,
  `simulation` tinyint(1) NOT NULL DEFAULT '0',
  `cve_id` varchar(20) DEFAULT NULL,
  `white_source_id` varchar(20) DEFAULT NULL,
  `summary` varbinary(1024) DEFAULT NULL,
  `npm_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_pending_vulnerabilities_on_ghsa_id` (`ghsa_id`),
  UNIQUE KEY `index_pending_vulnerabilities_on_source_and_source_identifier` (`source`,`source_identifier`),
  UNIQUE KEY `index_pending_vulnerabilities_on_npm_id` (`npm_id`),
  KEY `index_pending_vulnerabilities_on_external_identifier` (`external_identifier`),
  KEY `index_pending_vulnerabilities_on_vulnerability_id` (`vulnerability_id`),
  KEY `index_pending_vulnerabilities_on_created_at` (`created_at`),
  KEY `index_pending_vulnerabilities_on_simulation` (`simulation`),
  KEY `index_pending_vulnerabilities_on_cve_id` (`cve_id`),
  KEY `index_pending_vulnerabilities_on_white_source_id` (`white_source_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `pending_vulnerability_references`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pending_vulnerability_references` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pending_vulnerability_id` int(11) NOT NULL,
  `url` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_pending_vulnerability_references_pending_vulnerability_id` (`pending_vulnerability_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `pending_vulnerable_version_ranges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pending_vulnerable_version_ranges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pending_vulnerability_id` int(11) DEFAULT NULL,
  `affects` varchar(100) NOT NULL,
  `requirements` varchar(100) NOT NULL,
  `fixed_in` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `ecosystem` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_pending_vulnerable_version_ranges_on_pending_vuln_id` (`pending_vulnerability_id`),
  KEY `index_pending_vulnerable_version_ranges_on_ecosystem` (`ecosystem`),
  KEY `index_pending_vvrs_on_pending_vulnerability_id` (`pending_vulnerability_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `actor_id` int(11) unsigned NOT NULL,
  `actor_type` varchar(40) NOT NULL,
  `action` int(11) unsigned NOT NULL,
  `subject_id` int(11) unsigned NOT NULL,
  `subject_type` varchar(60) NOT NULL,
  `priority` int(11) unsigned NOT NULL DEFAULT '1',
  `parent_id` int(11) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `ability_id` bigint(20) DEFAULT NULL,
  `expires_at` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_permissions_by_identity` (`actor_type`,`actor_id`,`subject_type`,`subject_id`,`priority`,`parent_id`),
  UNIQUE KEY `index_permissions_by_subject_type_ability` (`subject_type`,`ability_id`),
  KEY `index_permissions_by_subject_actor_type_action_priority` (`subject_id`,`subject_type`,`actor_type`,`action`,`priority`),
  KEY `index_permissions_on_actor_subject_and_action` (`actor_id`,`subject_id`,`actor_type`,`subject_type`,`action`),
  KEY `index_permissions_on_actor_type_and_created_at` (`actor_type`,`created_at`),
  KEY `index_permissions_on_expires_at` (`expires_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `personal_reminders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personal_reminders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time_zone_name` varchar(40) NOT NULL,
  `user_id` int(11) NOT NULL,
  `reminder_slack_workspace_id` int(11) NOT NULL,
  `remindable_id` int(11) NOT NULL,
  `remindable_type` varchar(13) NOT NULL,
  `include_review_requests` tinyint(1) NOT NULL DEFAULT '1',
  `include_team_review_requests` tinyint(1) NOT NULL DEFAULT '0',
  `ignore_after_approval_count` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_personal_reminders_on_user_id_and_remindable` (`user_id`,`remindable_type`,`remindable_id`),
  KEY `index_personal_reminders_on_reminder_slack_workspace_id` (`reminder_slack_workspace_id`),
  KEY `index_personal_reminders_on_remindable_type_and_remindable_id` (`remindable_type`,`remindable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `photo_dna_hits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `photo_dna_hits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uploader_id` int(11) NOT NULL,
  `content_id` int(11) NOT NULL,
  `content_type` varchar(15) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `purged` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_photo_dna_hits_on_uploader_id` (`uploader_id`),
  KEY `index_photo_dna_hits_on_content_id_and_content_type` (`content_id`,`content_type`),
  KEY `index_photo_dna_hits_on_purged_and_created_at` (`purged`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `pinned_issue_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pinned_issue_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_comment_id` int(11) NOT NULL,
  `issue_id` int(11) NOT NULL,
  `pinned_by_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_pinned_issue_comments_on_issue_id` (`issue_id`),
  KEY `index_pinned_issue_comments_on_issue_comment_id` (`issue_comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `pinned_issues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pinned_issues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `issue_id` int(11) NOT NULL,
  `pinned_by_id` int(11) NOT NULL,
  `sort` varchar(126) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_pinned_issues_on_repository_id_and_issue_id` (`repository_id`,`issue_id`),
  KEY `index_pinned_issues_on_repository_id_and_sort` (`repository_id`,`sort`),
  KEY `index_pinned_issues_on_issue_id` (`issue_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `plan_subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plan_subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `braintree_id` varchar(255) DEFAULT NULL,
  `customer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `balance_in_cents` int(11) NOT NULL DEFAULT '0',
  `zuora_subscription_number` varchar(32) DEFAULT NULL,
  `zuora_subscription_id` varchar(32) DEFAULT NULL,
  `billing_start_date` date DEFAULT NULL,
  `zuora_rate_plan_charges` text,
  `apple_receipt_id` text,
  `apple_transaction_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_plan_subscriptions_on_user_id` (`user_id`),
  UNIQUE KEY `index_plan_subscriptions_on_braintree_id` (`braintree_id`),
  UNIQUE KEY `index_plan_subscriptions_on_zuora_subscription_number` (`zuora_subscription_number`),
  UNIQUE KEY `index_plan_subscriptions_on_zuora_subscription_id` (`zuora_subscription_id`),
  UNIQUE KEY `index_plan_subscriptions_on_apple_transaction_id` (`apple_transaction_id`),
  KEY `index_plan_subscriptions_on_customer_id` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `plan_trials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plan_trials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `pending_plan_change_id` int(11) NOT NULL,
  `plan` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_plan_trials_on_user_id_and_plan` (`user_id`,`plan`),
  KEY `index_plan_trials_on_pending_plan_change_id` (`pending_plan_change_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `pre_receive_environments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pre_receive_environments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `image_url` varchar(255) NOT NULL,
  `checksum` varchar(255) DEFAULT NULL,
  `downloaded_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `download_state` int(11) NOT NULL DEFAULT '0',
  `download_message` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_pre_receive_environments_on_name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `pre_receive_hook_targets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pre_receive_hook_targets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hook_id` int(11) NOT NULL,
  `hookable_id` int(11) NOT NULL,
  `hookable_type` varchar(255) NOT NULL,
  `enforcement` int(11) NOT NULL,
  `final` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_pre_receive_hook_hookables_on_hookable` (`hookable_id`,`hookable_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `pre_receive_hooks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pre_receive_hooks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `environment_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `script` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_pre_receive_hooks_on_repository_id_and_script` (`repository_id`,`script`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `prerelease_program_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prerelease_program_members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `member_type` varchar(30) NOT NULL DEFAULT 'User',
  `actor_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_prerelease_program_members_on_member_id_and_member_type` (`member_id`,`member_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `primary_avatars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `primary_avatars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `avatar_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `owner_type` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `cropped_x` int(11) NOT NULL DEFAULT '0',
  `cropped_y` int(11) NOT NULL DEFAULT '0',
  `cropped_width` int(11) NOT NULL DEFAULT '0',
  `cropped_height` int(11) NOT NULL DEFAULT '0',
  `updater_id` int(11) DEFAULT NULL,
  `previous_avatar_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `by_owner` (`owner_id`,`owner_type`),
  KEY `index_primary_avatars_on_avatar_id` (`avatar_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `product_uuids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_uuids` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zuora_product_id` varchar(32) DEFAULT NULL,
  `zuora_product_rate_plan_id` varchar(32) DEFAULT NULL,
  `zuora_product_rate_plan_charge_ids` text,
  `product_type` varchar(32) NOT NULL,
  `product_key` varchar(64) NOT NULL,
  `billing_cycle` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_product_uuids_on_zuora_product_rate_plan_id` (`zuora_product_rate_plan_id`),
  KEY `index_product_uuids_on_product_type_and_product_key` (`product_type`,`product_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `profile_pins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile_pins` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) NOT NULL,
  `pinned_item_id` int(11) NOT NULL,
  `pinned_item_type` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_profile_pins_unique` (`profile_id`,`pinned_item_type`,`pinned_item_id`),
  KEY `index_profile_pins_on_pinned_item_type_and_pinned_item_id` (`pinned_item_type`,`pinned_item_id`),
  KEY `index_profile_pins_on_position_and_profile_id` (`position`,`profile_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `blog` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `hireable` tinyint(1) DEFAULT '0',
  `bio` varbinary(1024) DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `display_staff_badge` tinyint(1) NOT NULL DEFAULT '0',
  `spoken_language_preference_code` char(2) DEFAULT NULL,
  `country_code` char(2) DEFAULT NULL,
  `twitter_username` varchar(15) DEFAULT NULL,
  `readme_opt_in` tinyint(1) NOT NULL DEFAULT '0',
  `mobile_time_zone_name` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_profiles_on_email` (`email`),
  KEY `index_profiles_on_hireable` (`hireable`),
  KEY `index_profiles_on_user_hidden_and_user_id` (`user_hidden`,`user_id`),
  KEY `index_profiles_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `project_cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_cards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `column_id` int(11) DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `content_type` varchar(20) DEFAULT NULL,
  `content_id` int(11) DEFAULT NULL,
  `note` mediumblob,
  `priority` bigint(20) unsigned DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `hidden_at` datetime DEFAULT NULL,
  `archived_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_project_cards_on_column_id_and_priority` (`column_id`,`priority`),
  UNIQUE KEY `index_project_cards_on_project_id_content_type_and_content_id` (`project_id`,`content_type`,`content_id`),
  KEY `index_project_cards_on_project_id_and_column_id_and_created_at` (`project_id`,`column_id`,`created_at`),
  KEY `index_project_cards_on_content_id` (`content_id`),
  KEY `index_project_cards_on_content_type_and_content_id` (`content_type`,`content_id`),
  KEY `index_project_cards_on_creator_id_and_project_id_and_updated_at` (`creator_id`,`project_id`,`updated_at`),
  KEY `index_project_cards_on_column_id_and_archived_at_and_priority` (`column_id`,`archived_at`,`priority`),
  KEY `index_project_cards_on_project_id_and_archived_at` (`project_id`,`archived_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `project_columns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_columns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `name` varbinary(1024) NOT NULL,
  `color` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `hidden_at` datetime DEFAULT NULL,
  `purpose` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `columns_by_project` (`project_id`,`position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `project_repository_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_repository_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_project_repository_links_on_project_id_and_repository_id` (`project_id`,`repository_id`),
  KEY `index_project_repository_links_on_repository_id` (`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `project_workflow_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_workflow_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_workflow_id` int(11) NOT NULL,
  `action_type` varchar(64) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `last_updater_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_project_workflow_actions_on_project_workflow_id` (`project_workflow_id`),
  KEY `index_project_workflow_actions_on_project_id` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `project_workflows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_workflows` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trigger_type` varchar(64) NOT NULL,
  `project_id` int(11) NOT NULL,
  `project_column_id` int(11) DEFAULT NULL,
  `creator_id` int(11) NOT NULL,
  `last_updater_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_project_workflows_on_project_column_id_and_trigger_type` (`project_column_id`,`trigger_type`),
  KEY `index_project_workflows_on_trigger_type_and_project_id` (`trigger_type`,`project_id`),
  KEY `index_project_workflows_on_project_id` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varbinary(1024) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `owner_type` varchar(30) NOT NULL,
  `body` mediumblob,
  `creator_id` int(11) NOT NULL,
  `number` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `closed_at` datetime DEFAULT NULL,
  `last_sync_at` datetime DEFAULT NULL,
  `public` tinyint(1) NOT NULL DEFAULT '0',
  `source_kind` varchar(20) DEFAULT NULL,
  `source_id` int(11) DEFAULT NULL,
  `track_progress` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `projects_by_owner_and_created_at` (`owner_id`,`owner_type`,`created_at`),
  KEY `projects_by_owner_and_updated_at` (`owner_id`,`owner_type`,`updated_at`),
  KEY `projects_by_owner_and_name` (`owner_id`,`owner_type`,`name`(40)),
  KEY `projects_by_owner_id_and_owner_type_and_deleted_at_and_closed_at` (`owner_id`,`owner_type`,`deleted_at`,`closed_at`),
  KEY `projects_by_owner_id_and_owner_type_and_public` (`owner_id`,`owner_type`,`public`),
  KEY `index_projects_on_creator_id` (`creator_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `protected_branches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `protected_branches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `name` varbinary(1024) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `creator_id` int(11) NOT NULL,
  `required_status_checks_enforcement_level` int(11) NOT NULL DEFAULT '0',
  `block_force_pushes_enforcement_level` int(11) NOT NULL DEFAULT '2',
  `block_deletions_enforcement_level` int(11) NOT NULL DEFAULT '2',
  `strict_required_status_checks_policy` tinyint(1) NOT NULL DEFAULT '1',
  `authorized_actors_only` tinyint(1) NOT NULL DEFAULT '0',
  `pull_request_reviews_enforcement_level` int(11) NOT NULL DEFAULT '0',
  `authorized_dismissal_actors_only` tinyint(1) NOT NULL DEFAULT '0',
  `admin_enforced` tinyint(1) NOT NULL DEFAULT '0',
  `dismiss_stale_reviews_on_push` tinyint(1) NOT NULL DEFAULT '0',
  `require_code_owner_review` tinyint(1) NOT NULL DEFAULT '0',
  `signature_requirement_enforcement_level` int(11) NOT NULL DEFAULT '0',
  `required_approving_review_count` tinyint(4) NOT NULL DEFAULT '1',
  `linear_history_requirement_enforcement_level` int(11) NOT NULL DEFAULT '0',
  `allow_force_pushes_enforcement_level` int(11) NOT NULL DEFAULT '0',
  `allow_deletions_enforcement_level` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_protected_branches_on_repository_id_and_name` (`repository_id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `public_keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `public_keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `repository_id` int(11) DEFAULT NULL,
  `key` text,
  `username` varchar(255) NOT NULL,
  `fingerprint` char(48) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `verified_at` datetime DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `verifier_id` int(11) DEFAULT NULL,
  `accessed_at` datetime DEFAULT NULL,
  `created_by` varchar(255) NOT NULL DEFAULT 'user',
  `read_only` tinyint(1) DEFAULT NULL,
  `unverification_reason` varchar(255) DEFAULT NULL,
  `oauth_authorization_id` int(11) DEFAULT NULL,
  `oauth_application_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_public_keys_on_fingerprint` (`fingerprint`),
  KEY `index_public_keys_on_oauth_authorization_id` (`oauth_authorization_id`),
  KEY `index_public_keys_on_user_id` (`user_id`),
  KEY `index_public_keys_on_repository_id` (`repository_id`),
  KEY `index_public_keys_on_accessed_at_and_created_at` (`accessed_at`,`created_at`),
  KEY `index_public_keys_on_repository_id_oauth_application_id` (`repository_id`,`oauth_application_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `public_org_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `public_org_members` (
  `user_id` int(11) DEFAULT NULL,
  `organization_id` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_public_org_members_on_user_id_and_organization_id` (`user_id`,`organization_id`),
  KEY `index_public_org_members_on_organization_id` (`organization_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `pull_request_conflicts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pull_request_conflicts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pull_request_id` int(11) NOT NULL,
  `base_sha` varchar(255) NOT NULL,
  `head_sha` varchar(255) NOT NULL,
  `info` blob,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_pull_request_conflicts_on_pr_id` (`pull_request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `pull_request_imports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pull_request_imports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `import_id` int(11) NOT NULL,
  `pull_request_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_pull_request_imports_pull_request_id` (`pull_request_id`),
  KEY `index_pull_request_imports_import_id` (`import_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `pull_request_review_comment_edits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pull_request_review_comment_edits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pull_request_review_comment_id` int(11) NOT NULL,
  `edited_at` datetime NOT NULL,
  `editor_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by_id` int(11) DEFAULT NULL,
  `diff` mediumblob,
  `user_content_edit_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_pull_request_review_comment_edits_on_user_content_edit_id` (`user_content_edit_id`),
  KEY `index_on_pull_request_review_comment_id` (`pull_request_review_comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `pull_request_review_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pull_request_review_comments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pull_request_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `path` varbinary(1024) DEFAULT NULL,
  `commit_id` varchar(40) DEFAULT NULL,
  `diff_hunk` mediumblob,
  `body` mediumblob,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `position` int(11) unsigned DEFAULT NULL,
  `original_commit_id` varchar(40) DEFAULT NULL,
  `original_position` int(11) unsigned DEFAULT NULL,
  `formatter` varchar(20) DEFAULT NULL,
  `repository_id` int(11) NOT NULL,
  `original_base_commit_id` varchar(40) DEFAULT NULL,
  `original_start_commit_id` varchar(40) DEFAULT NULL,
  `original_end_commit_id` varchar(40) DEFAULT NULL,
  `blob_position` int(11) unsigned DEFAULT NULL,
  `blob_path` varbinary(1024) DEFAULT NULL,
  `blob_commit_oid` char(40) DEFAULT NULL,
  `left_blob` tinyint(1) NOT NULL DEFAULT '0',
  `state` int(11) NOT NULL DEFAULT '0',
  `pull_request_review_id` int(11) DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `reply_to_id` int(11) DEFAULT NULL,
  `outdated` tinyint(1) NOT NULL DEFAULT '0',
  `comment_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `comment_hidden_reason` varbinary(1024) DEFAULT NULL,
  `comment_hidden_classifier` varchar(50) DEFAULT NULL,
  `comment_hidden_by` int(11) DEFAULT NULL,
  `pull_request_review_thread_id` int(11) DEFAULT NULL,
  `start_position_offset` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_pr_review_comments_on_pr_id_and_created_at` (`pull_request_id`,`created_at`),
  KEY `index_pull_request_review_comments_on_repo_id_updated_at` (`repository_id`,`updated_at`),
  KEY `index_pull_request_review_comments_on_repository_id_and_user_id` (`repository_id`,`user_id`),
  KEY `index_pr_review_comments_on_reply_to_id` (`reply_to_id`),
  KEY `index_pull_request_review_comments_on_user_id_and_user_hidden` (`user_id`,`user_hidden`),
  KEY `index_pull_request_review_comments_on_repo_id_state_and_user_id` (`repository_id`,`state`,`user_id`),
  KEY `index_pr_review_comments_on_pr_id_user_hidden_user_id_and_state` (`pull_request_id`,`user_hidden`,`user_id`,`state`),
  KEY `index_pr_review_comments_on_user_hidden_and_user_id` (`user_hidden`,`user_id`),
  KEY `index_pr_review_comments_on_repo_id_and_path_and_created_at` (`repository_id`,`path`(85),`created_at`),
  KEY `index_pull_request_review_comments_on_repo_and_hidden_and_user` (`repository_id`,`user_hidden`,`user_id`),
  KEY `index_pull_request_review_comments_on_user_id_and_created_at` (`user_id`,`created_at`),
  KEY `index_pull_request_review_comments_on_user_id_and_updated_at` (`user_id`,`updated_at`),
  KEY `index_pull_request_review_comments_on_pr_review_thread_id` (`pull_request_review_thread_id`),
  KEY `pull_request_review_id_and_reply_to_id` (`pull_request_review_id`,`reply_to_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `pull_request_review_edits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pull_request_review_edits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pull_request_review_id` int(11) NOT NULL,
  `edited_at` datetime NOT NULL,
  `editor_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by_id` int(11) DEFAULT NULL,
  `diff` mediumblob,
  `user_content_edit_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_pull_request_review_edits_on_user_content_edit_id` (`user_content_edit_id`),
  KEY `index_pull_request_review_edits_on_pull_request_review_id` (`pull_request_review_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `pull_request_review_threads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pull_request_review_threads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pull_request_id` int(11) DEFAULT NULL,
  `pull_request_review_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `resolver_id` int(11) DEFAULT NULL,
  `resolved_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_pull_request_review_threads_on_pr_id_and_pr_review_id` (`pull_request_id`,`pull_request_review_id`),
  KEY `index_pull_request_review_threads_on_pull_request_review_id` (`pull_request_review_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `pull_request_reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pull_request_reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pull_request_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `head_sha` char(40) NOT NULL,
  `body` mediumblob,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `submitted_at` datetime DEFAULT NULL,
  `formatter` varchar(20) DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `comment_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `comment_hidden_reason` varbinary(1024) DEFAULT NULL,
  `comment_hidden_classifier` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_pull_request_reviews_on_user_id_and_submitted_at` (`user_id`,`submitted_at`),
  KEY `index_reviews_pull_request_user_state` (`pull_request_id`,`user_hidden`,`user_id`,`state`),
  KEY `index_pull_request_reviews_on_user_hidden_and_user_id` (`user_hidden`,`user_id`),
  KEY `index_pull_request_reviews_on_user_id_and_user_hidden` (`user_id`,`user_hidden`),
  KEY `index_pull_request_reviews_on_pull_request_id_and_created_at` (`pull_request_id`,`created_at`),
  KEY `index_pull_request_reviews_on_pull_request_id_and_submitted_at` (`pull_request_id`,`submitted_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `pull_request_reviews_review_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pull_request_reviews_review_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pull_request_review_id` int(11) NOT NULL,
  `review_request_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_pr_reviews_review_requests_on_request_id_and_pr_review_id` (`review_request_id`,`pull_request_review_id`),
  KEY `index_pr_reviews_review_requests_on_pr_review_id_and_request_id` (`pull_request_review_id`,`review_request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `pull_request_revisions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pull_request_revisions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pull_request_id` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `ready` tinyint(1) NOT NULL,
  `base_oid` varchar(64) NOT NULL,
  `head_oid` varchar(64) NOT NULL,
  `revised_at` datetime NOT NULL,
  `force_pushed` tinyint(1) NOT NULL,
  `commits_count` int(11) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `push_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_pull_request_revisions_on_pull_and_number` (`pull_request_id`,`number`),
  KEY `index_pull_request_revisions_on_pull_and_base_and_head_and_ready` (`pull_request_id`,`base_oid`,`head_oid`,`ready`),
  KEY `index_pull_request_revisions_on_pull_and_ready_and_number` (`pull_request_id`,`ready`,`number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `pull_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pull_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `base_sha` char(40) DEFAULT NULL,
  `head_sha` char(40) DEFAULT NULL,
  `repository_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `base_repository_id` int(11) DEFAULT NULL,
  `head_repository_id` int(11) DEFAULT NULL,
  `base_ref` varbinary(1024) DEFAULT NULL,
  `head_ref` varbinary(1024) DEFAULT NULL,
  `merged_at` datetime DEFAULT NULL,
  `base_user_id` int(11) DEFAULT NULL,
  `head_user_id` int(11) DEFAULT NULL,
  `mergeable` tinyint(1) unsigned DEFAULT NULL,
  `merge_commit_sha` char(40) DEFAULT NULL,
  `contributed_at_timestamp` bigint(20) DEFAULT NULL,
  `contributed_at_offset` mediumint(9) DEFAULT NULL,
  `fork_collab_state` int(11) NOT NULL DEFAULT '0',
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `base_sha_on_merge` char(40) DEFAULT NULL,
  `work_in_progress` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_pull_requests_on_base_repository_id_and_base_ref` (`base_repository_id`,`base_ref`(767)),
  KEY `index_pull_requests_on_base_repository_id_and_head_ref` (`base_repository_id`,`head_ref`(767)),
  KEY `index_pull_requests_on_repository_id_and_head_ref` (`repository_id`,`head_ref`(767)),
  KEY `index_pull_requests_on_repository_id_and_user_id_and_user_hidden` (`repository_id`,`user_id`,`user_hidden`),
  KEY `index_pull_requests_on_user_hidden_and_user_id` (`user_hidden`,`user_id`),
  KEY `index_pull_requests_on_repository_id_and_head_sha` (`repository_id`,`head_sha`),
  KEY `index_pull_requests_on_user_id_and_created_at` (`user_id`,`created_at`),
  KEY `index_pull_requests_on_user_id_and_repository_id` (`user_id`,`repository_id`),
  KEY `index_pull_requests_on_repository_id_and_updated_at` (`repository_id`,`updated_at`),
  KEY `index_pull_requests_on_repository_id_and_created_at` (`repository_id`,`created_at`),
  KEY `head_repository_id_and_head_ref_and_head_sha_and_repository_id` (`head_repository_id`,`head_ref`,`head_sha`,`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `pushes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pushes` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) DEFAULT NULL,
  `pusher_id` int(11) DEFAULT NULL,
  `before` varchar(40) DEFAULT NULL,
  `after` varchar(40) DEFAULT NULL,
  `ref` varbinary(1024) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_pushes_on_pusher_id` (`pusher_id`),
  KEY `index_pushes_on_repository_id_and_pusher_id_and_created_at` (`repository_id`,`pusher_id`,`created_at`),
  KEY `index_pushes_on_repository_id_and_after` (`repository_id`,`after`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `reactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(30) NOT NULL,
  `user_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `subject_type` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_reactions_identity` (`user_id`,`subject_id`,`subject_type`,`content`),
  KEY `index_reactions_on_subject_content_created_at` (`subject_id`,`subject_type`,`content`,`created_at`),
  KEY `index_reactions_on_user_hidden_and_user_id` (`user_hidden`,`user_id`),
  KEY `subject_id_and_subject_type_and_user_hidden_and_created_at` (`subject_id`,`subject_type`,`user_hidden`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `refresh_tokens` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `refreshable_type` varchar(80) NOT NULL,
  `refreshable_id` int(11) NOT NULL,
  `hashed_token` varchar(80) NOT NULL,
  `expires_at` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_refresh_tokens_on_refreshable_id_and_refreshable_type` (`refreshable_id`,`refreshable_type`),
  UNIQUE KEY `index_refresh_tokens_on_hashed_token_and_expires_at` (`hashed_token`,`expires_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `registry_package_dependencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registry_package_dependencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `registry_package_version_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL,
  `dependency_type` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_registry_package_dependencies_on_package_version_id` (`registry_package_version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `registry_package_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registry_package_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `registry_package_id` int(11) NOT NULL,
  `release_id` int(11) DEFAULT NULL,
  `platform` varchar(255) NOT NULL DEFAULT '',
  `sha1` varchar(40) DEFAULT NULL,
  `version` varchar(255) NOT NULL,
  `commit_oid` varchar(40) DEFAULT NULL,
  `storage_blob_id` int(11) DEFAULT NULL,
  `uploader_id` int(11) DEFAULT NULL,
  `guid` varchar(36) DEFAULT NULL,
  `oid` varchar(64) DEFAULT NULL,
  `size` int(11) NOT NULL,
  `state` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `filename` varchar(255) NOT NULL DEFAULT '',
  `md5` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_reg_package_files_on_reg_pkg_id_and_ver_and_plat_and_fname` (`registry_package_id`,`version`,`platform`,`filename`),
  KEY `index_registry_package_files_on_release_id` (`release_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `registry_package_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registry_package_metadata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_version_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `value` blob,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_registry_package_metadata_on_package_version_id_and_name` (`package_version_id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `registry_package_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registry_package_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `registry_package_id` int(11) NOT NULL,
  `registry_package_version_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_registry_package_tags_on_registry_package_id_and_name` (`registry_package_id`,`name`),
  KEY `index_registry_package_tags_on_name` (`name`),
  KEY `index_registry_package_tags_on_registry_package_version_id` (`registry_package_version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `registry_packages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registry_packages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `package_type` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `registry_package_type` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_registry_package_on_repo_and_name_and_type` (`repository_id`,`name`,`package_type`),
  UNIQUE KEY `index_registry_packages_on_owner_id_name_package_type` (`owner_id`,`name`,`package_type`),
  UNIQUE KEY `index_registry_packages_on_repository_id_and_slug` (`repository_id`,`slug`),
  UNIQUE KEY `index_packages_on_repo_id_and_name_and_registry_package_type` (`repository_id`,`name`,`registry_package_type`),
  UNIQUE KEY `index_registry_packages_on_owner_id_name_registry_package_type` (`owner_id`,`name`,`registry_package_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `release_assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `release_assets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `release_id` int(11) NOT NULL,
  `uploader_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type` varchar(255) NOT NULL,
  `size` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `uploaded` tinyint(1) DEFAULT NULL,
  `guid` varchar(255) DEFAULT NULL,
  `downloads` int(11) DEFAULT '0',
  `state` int(11) DEFAULT '0',
  `label` varchar(255) DEFAULT NULL,
  `storage_blob_id` int(11) DEFAULT NULL,
  `oid` varchar(255) DEFAULT NULL,
  `storage_provider` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `by_name` (`release_id`,`name`),
  KEY `by_repository_id_and_guid` (`repository_id`,`guid`),
  KEY `index_release_assets_on_state_and_release_id` (`state`,`release_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `releases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `releases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varbinary(1024) DEFAULT NULL,
  `tag_name` varbinary(1024) NOT NULL,
  `body` mediumblob,
  `author_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `state` int(11) DEFAULT '0',
  `pending_tag` varbinary(1024) DEFAULT NULL,
  `prerelease` tinyint(1) NOT NULL DEFAULT '0',
  `target_commitish` varchar(255) DEFAULT NULL,
  `published_at` datetime DEFAULT NULL,
  `formatter` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `by_state` (`repository_id`,`state`),
  KEY `by_published` (`repository_id`,`published_at`),
  KEY `by_repo_and_tag` (`repository_id`,`tag_name`(50))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `reminder_delivery_times`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reminder_delivery_times` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` int(11) NOT NULL,
  `day` tinyint(4) NOT NULL,
  `next_delivery_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `schedulable_id` int(11) NOT NULL,
  `schedulable_type` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_reminder_delivery_times_on_schedulable_and_time_and_day` (`schedulable_type`,`schedulable_id`,`time`,`day`),
  KEY `index_reminder_delivery_times_on_next_delivery_at` (`next_delivery_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `reminder_event_subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reminder_event_subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` int(11) NOT NULL,
  `subscriber_id` int(11) NOT NULL,
  `subscriber_type` varchar(16) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `options` varbinary(1024) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_reminder_event_subscriptions_subscriber_and_event_type` (`subscriber_id`,`subscriber_type`,`event_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `reminder_repository_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reminder_repository_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reminder_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_reminder_repository_links_on_reminder_id` (`reminder_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `reminder_slack_workspace_memberships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reminder_slack_workspace_memberships` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `reminder_slack_workspace_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_reminder_slack_user_on_user_id_and_slack_workspace_id` (`user_id`,`reminder_slack_workspace_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `reminder_slack_workspaces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reminder_slack_workspaces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slack_id` varchar(20) NOT NULL,
  `remindable_type` varchar(13) NOT NULL,
  `remindable_id` int(11) NOT NULL,
  `name` varbinary(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_reminder_slack_workspaces_on_remindable_and_slack_id` (`remindable_type`,`remindable_id`,`slack_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `reminder_team_memberships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reminder_team_memberships` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reminder_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_reminder_team_memberships_on_reminder_id_and_team_id` (`reminder_id`,`team_id`),
  KEY `index_reminder_team_memberships_on_team_id` (`team_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `reminders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reminders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `remindable_type` varchar(13) NOT NULL,
  `remindable_id` int(11) NOT NULL,
  `reminder_slack_workspace_id` int(11) NOT NULL,
  `slack_channel` varchar(80) NOT NULL,
  `time_zone_name` varchar(40) NOT NULL,
  `min_age` int(11) NOT NULL DEFAULT '0',
  `min_staleness` int(11) NOT NULL DEFAULT '0',
  `include_unassigned_prs` tinyint(1) NOT NULL DEFAULT '0',
  `include_reviewed_prs` tinyint(1) NOT NULL DEFAULT '0',
  `require_review_request` tinyint(1) NOT NULL DEFAULT '1',
  `ignore_draft_prs` tinyint(1) NOT NULL DEFAULT '1',
  `ignore_after_approval_count` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `ignored_terms` varbinary(1024) DEFAULT NULL,
  `ignored_labels` varbinary(1024) DEFAULT NULL,
  `required_labels` varbinary(1024) DEFAULT NULL,
  `needed_reviews` tinyint(4) NOT NULL DEFAULT '0',
  `slack_channel_id` varchar(20) DEFAULT NULL,
  `team_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_reminders_on_reminder_slack_workspace_id` (`reminder_slack_workspace_id`),
  KEY `index_reminders_on_remindable_type_and_remindable_id` (`remindable_type`,`remindable_id`),
  KEY `index_reminders_on_team_id` (`team_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `render_blobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `render_blobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `size` int(11) NOT NULL,
  `content_type` varchar(255) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `ref` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `storage_blob_id` int(11) NOT NULL,
  `oid` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `repository_type` varchar(32) NOT NULL DEFAULT 'Repository',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_render_blobs_on_name` (`name`),
  KEY `index_render_blobs_on_updated_at` (`updated_at`),
  KEY `index_render_blobs_on_repository_id_and_repository_type` (`repository_id`,`repository_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repositories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repositories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `owner_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `sandbox` tinyint(1) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `public` tinyint(1) DEFAULT '1',
  `description` mediumblob,
  `homepage` varchar(255) DEFAULT NULL,
  `source_id` int(11) DEFAULT NULL,
  `public_push` tinyint(1) DEFAULT NULL,
  `disk_usage` int(11) DEFAULT '0',
  `locked` tinyint(1) DEFAULT '0',
  `pushed_at` datetime DEFAULT NULL,
  `watcher_count` int(11) DEFAULT '0',
  `public_fork_count` int(11) NOT NULL DEFAULT '1',
  `primary_language_name_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `has_issues` tinyint(1) DEFAULT '1',
  `has_wiki` tinyint(1) DEFAULT '1',
  `has_discussions` tinyint(1) NOT NULL DEFAULT '0',
  `has_downloads` tinyint(1) DEFAULT '1',
  `raw_data` blob,
  `organization_id` int(11) DEFAULT NULL,
  `disabled_at` datetime DEFAULT NULL,
  `disabled_by` int(11) DEFAULT NULL,
  `disabling_reason` varchar(30) DEFAULT NULL,
  `health_status` varchar(30) DEFAULT NULL,
  `pushed_at_usec` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `reflog_sync_enabled` tinyint(1) DEFAULT '0',
  `made_public_at` datetime DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `maintained` tinyint(1) NOT NULL DEFAULT '1',
  `template` tinyint(1) NOT NULL DEFAULT '0',
  `owner_login` varchar(40) DEFAULT NULL,
  `world_writable_wiki` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repositories_on_owner_id_and_name_and_active` (`owner_id`,`name`,`active`),
  KEY `index_repositories_on_public_and_watcher_count` (`public`,`watcher_count`),
  KEY `index_repositories_on_primary_language_name_id_and_public` (`primary_language_name_id`,`public`),
  KEY `index_repositories_on_created_at` (`created_at`),
  KEY `index_repositories_on_disabled_at` (`disabled_at`),
  KEY `index_repositories_on_owner_id_and_pushed_at` (`owner_id`,`pushed_at`),
  KEY `index_repositories_on_owner_id_and_made_public_at` (`owner_id`,`made_public_at`),
  KEY `index_repositories_on_user_hidden_and_owner_id` (`user_hidden`,`owner_id`),
  KEY `index_repositories_on_parent_id` (`parent_id`),
  KEY `index_repositories_on_organization_id_and_active_and_public` (`organization_id`,`active`,`public`),
  KEY `index_repositories_on_watcher_count_and_created_at_and_pushed_at` (`watcher_count`,`created_at`,`pushed_at`),
  KEY `index_repositories_on_active_and_updated_at` (`active`,`updated_at`),
  KEY `index_repositories_on_source_id_and_organization_id` (`source_id`,`organization_id`),
  KEY `index_repositories_on_owner_and_parent_and_public_and_source_id` (`owner_id`,`parent_id`,`public`,`source_id`),
  KEY `index_on_public_and_primary_language_name_id_and_parent_id` (`public`,`primary_language_name_id`,`parent_id`),
  KEY `index_repositories_on_template_and_active_and_owner_id` (`template`,`active`,`owner_id`),
  KEY `index_repositories_on_owner_login_and_name_and_active` (`owner_login`,`name`,`active`),
  KEY `owner_and_org_and_name_and_active_and_public_and_disabled_at` (`owner_id`,`organization_id`,`name`,`active`,`public`,`disabled_at`),
  KEY `index_repositories_on_owner_id_and_updated_at` (`owner_id`,`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_action_releases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_action_releases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_action_id` int(11) NOT NULL,
  `release_id` int(11) NOT NULL,
  `published_on_marketplace` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_action_releases_on_release_and_action_id` (`release_id`,`repository_action_id`),
  KEY `index_repository_action_releases_on_repository_action_id` (`repository_action_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(255) NOT NULL,
  `name` varbinary(1024) NOT NULL,
  `description` mediumblob,
  `icon_name` varchar(20) DEFAULT NULL,
  `color` varchar(6) DEFAULT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `repository_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `rank_multiplier` float NOT NULL DEFAULT '1',
  `state` int(11) NOT NULL DEFAULT '0',
  `slug` varchar(255) DEFAULT NULL,
  `security_email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_actions_on_repository_id_and_path` (`repository_id`,`path`),
  UNIQUE KEY `index_repository_actions_on_slug` (`slug`),
  KEY `index_repository_actions_on_repository_id_and_featured` (`repository_id`,`featured`),
  KEY `index_repository_actions_on_rank_multiplier` (`rank_multiplier`),
  KEY `index_repository_actions_on_state_and_slug` (`state`,`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_advisories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_advisories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `publisher_id` int(11) DEFAULT NULL,
  `assignee_id` int(11) DEFAULT NULL,
  `title` varbinary(1024) NOT NULL,
  `description` mediumblob,
  `state` int(11) NOT NULL DEFAULT '0',
  `severity` int(11) DEFAULT NULL,
  `affected_versions` blob,
  `fixed_versions` blob,
  `impact` mediumblob,
  `workarounds` mediumblob,
  `patches` mediumblob,
  `cve_id` varbinary(40) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `published_at` datetime DEFAULT NULL,
  `closed_at` datetime DEFAULT NULL,
  `withdrawn_at` datetime DEFAULT NULL,
  `workspace_repository_id` int(11) DEFAULT NULL,
  `ghsa_id` varchar(19) NOT NULL,
  `package` varbinary(100) DEFAULT NULL,
  `ecosystem` varbinary(50) DEFAULT NULL,
  `body` mediumblob,
  `owner_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_advisories_on_ghsa_id` (`ghsa_id`),
  UNIQUE KEY `index_repository_advisories_on_workspace_repository_id` (`workspace_repository_id`),
  KEY `index_repository_advisories_on_repository_id_and_state` (`repository_id`,`state`),
  KEY `index_repository_advisories_on_owner_and_workspace_repository` (`owner_id`,`workspace_repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_advisory_comment_edits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_advisory_comment_edits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_advisory_comment_id` int(11) NOT NULL,
  `edited_at` datetime NOT NULL,
  `editor_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by_id` int(11) DEFAULT NULL,
  `diff` mediumblob,
  `user_content_edit_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_advisory_comment_edits_on_user_content_edit_id` (`user_content_edit_id`),
  KEY `index_on_pull_request_review_comment_id` (`repository_advisory_comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_advisory_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_advisory_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_advisory_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `body` mediumblob NOT NULL,
  `formatter` varchar(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_repository_advisory_comments_on_repository_advisory_id` (`repository_advisory_id`),
  KEY `index_repository_advisory_comments_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_advisory_edits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_advisory_edits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_advisory_id` int(11) NOT NULL,
  `edited_at` datetime NOT NULL,
  `editor_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by_id` int(11) DEFAULT NULL,
  `diff` mediumblob,
  `user_content_edit_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_advisory_edits_on_user_content_edit_id` (`user_content_edit_id`),
  KEY `index_repository_advisory_edits_on_repository_advisory_id` (`repository_advisory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_advisory_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_advisory_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_advisory_id` int(11) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `event` varchar(40) NOT NULL,
  `changed_attribute` varchar(40) DEFAULT NULL,
  `value_was` varbinary(2014) DEFAULT NULL,
  `value_is` varbinary(2014) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_repository_advisory_events_on_repo_adv_id_and_event` (`repository_advisory_id`,`event`),
  KEY `index_repository_advisory_events_on_actor_id_event_created_at` (`actor_id`,`event`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_backup_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_backup_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `last_backup_at` datetime DEFAULT NULL,
  `last_wiki_backup_at` datetime DEFAULT NULL,
  `last_error_at` datetime DEFAULT NULL,
  `last_wiki_error_at` datetime DEFAULT NULL,
  `error_count` int(11) NOT NULL DEFAULT '0',
  `wiki_error_count` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `last_backup_checksum` varchar(255) DEFAULT NULL,
  `last_wiki_backup_checksum` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_backup_statuses_on_repository_id` (`repository_id`),
  KEY `index_repository_backup_statuses_on_last_backup_at` (`last_backup_at`),
  KEY `index_repository_backup_statuses_on_last_wiki_backup_at` (`last_wiki_backup_at`),
  KEY `backup_scheduler` (`last_error_at`,`last_backup_at`,`repository_id`),
  KEY `wiki_scheduler` (`last_wiki_error_at`,`last_wiki_backup_at`,`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_bases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_bases` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `network_id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `incremental_id` bigint(20) NOT NULL,
  `key_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_repository_bases_on_network_id_and_repository_id` (`network_id`),
  KEY `index_repository_bases_on_key_id` (`key_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_checksums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_checksums` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `repository_type` int(11) NOT NULL DEFAULT '0',
  `checksum` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_checksums_on_repository_type` (`repository_id`,`repository_type`),
  KEY `index_repository_checksums_on_updated_at` (`updated_at`),
  KEY `index_repository_checksums_on_repository_type_and_updated_at` (`repository_type`,`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_clones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_clones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_repository_id` int(11) NOT NULL,
  `clone_repository_id` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `cloning_user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `error_reason_code` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_clones_on_clone_repository_id` (`clone_repository_id`),
  KEY `index_repository_clones_on_template_repository_id` (`template_repository_id`),
  KEY `index_repository_clones_on_cloning_user_id` (`cloning_user_id`),
  KEY `index_repository_clones_on_clone_repository_id_and_state` (`clone_repository_id`,`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_code_symbol_indices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_code_symbol_indices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `commit_oid` varchar(40) NOT NULL,
  `ref` varbinary(767) NOT NULL,
  `page` int(11) NOT NULL DEFAULT '0',
  `status` varchar(10) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_code_symbol_indices_on_repository_ref_commit` (`repository_id`,`ref`,`commit_oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_contribution_graph_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_contribution_graph_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `job_status_id` char(36) DEFAULT NULL,
  `last_indexed_oid` varchar(64) DEFAULT NULL,
  `last_viewed_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `top_contributor_ids` blob,
  `last_indexed_at` datetime DEFAULT NULL,
  `job_enqueued_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_contribution_graph_statuses_on_repository_id` (`repository_id`),
  KEY `index_repository_contribution_graph_statuses_on_last_viewed_at` (`last_viewed_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_dependency_updates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_dependency_updates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `pull_request_id` int(11) DEFAULT NULL,
  `repository_vulnerability_alert_id` int(11) DEFAULT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `reason` int(11) NOT NULL DEFAULT '0',
  `trigger_type` int(11) NOT NULL DEFAULT '0',
  `manifest_path` varchar(255) NOT NULL,
  `package_name` varchar(255) NOT NULL,
  `body` mediumblob,
  `error_body` mediumblob,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `error_title` varbinary(1024) DEFAULT NULL,
  `dry_run` tinyint(1) NOT NULL DEFAULT '0',
  `error_type` varchar(60) DEFAULT NULL,
  `retry` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_pull_request_dependency_updates_on_repo_dep_and_state` (`repository_id`,`manifest_path`,`package_name`,`state`),
  KEY `index_repository_dependency_updates_on_pull_request_id` (`pull_request_id`),
  KEY `index_repository_dependency_updates_on_rva_id` (`repository_vulnerability_alert_id`),
  KEY `index_repository_dependency_updates_on_state_and_created_at` (`state`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_disabled_backups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_disabled_backups` (
  `repository_id` int(11) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `disabled_at` datetime NOT NULL,
  PRIMARY KEY (`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `uploader_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type` varchar(255) NOT NULL,
  `size` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `storage_blob_id` int(11) DEFAULT NULL,
  `oid` varchar(255) DEFAULT NULL,
  `storage_provider` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `by_repository_id` (`repository_id`),
  KEY `by_state` (`state`),
  KEY `by_uploader_id_and_created_at` (`uploader_id`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `uploader_id` int(11) NOT NULL,
  `content_type` varchar(40) NOT NULL,
  `size` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `guid` varchar(36) DEFAULT NULL,
  `state` int(11) DEFAULT '0',
  `storage_blob_id` int(11) DEFAULT NULL,
  `oid` varchar(64) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `role` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_images_on_repository_id_and_guid` (`repository_id`,`guid`),
  KEY `index_repository_images_on_uploader_id` (`uploader_id`),
  KEY `index_repository_images_on_role_and_repository_id_and_guid` (`role`,`repository_id`,`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_imports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_imports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `import_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_imports_repository_id` (`repository_id`),
  KEY `index_repository_imports_import_id` (`import_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_incrementals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_incrementals` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `previous_id` bigint(20) DEFAULT NULL,
  `network_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `checksum` varchar(48) DEFAULT NULL,
  `audit_log_len` bigint(20) DEFAULT NULL,
  `key_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_incrementals_previous_unique` (`previous_id`),
  KEY `index_repository_incrementals_on_key_id` (`key_id`),
  KEY `index_repository_incrementals_on_repository_id_and_checksum` (`repository_id`,`checksum`),
  KEY `index_repository_incrementals_on_ids2` (`network_id`,`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_invitations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_invitations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `inviter_id` int(11) NOT NULL,
  `invitee_id` int(11) DEFAULT NULL,
  `permissions` tinyint(4) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `hashed_token` varchar(44) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_invitations_on_repository_id_and_invitee_id` (`repository_id`,`invitee_id`),
  UNIQUE KEY `index_repository_invitations_on_hashed_token` (`hashed_token`),
  UNIQUE KEY `index_repository_invitations_on_repository_id_and_email` (`repository_id`,`email`),
  KEY `index_repository_invitations_on_invitee_id` (`invitee_id`),
  KEY `index_repository_invitations_on_created_at` (`created_at`),
  KEY `index_repository_invitations_on_role_id` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_licenses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_licenses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) DEFAULT NULL,
  `license_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_licenses_on_repository_id` (`repository_id`),
  KEY `index_repository_licenses_on_license_id` (`license_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_maintenance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_maintenance` (
  `network_id` int(11) NOT NULL,
  `status` enum('completed','scheduled','running','retry','error') NOT NULL,
  `scheduled_at` datetime DEFAULT NULL,
  `last_maintenance_at` datetime DEFAULT NULL,
  `incrementals` int(11) DEFAULT NULL,
  PRIMARY KEY (`network_id`),
  KEY `repository_maintenance_on_last_maintenance_at` (`last_maintenance_at`),
  KEY `repository_maintenance_on_incrementals` (`incrementals`),
  KEY `index_repository_maintenance_on_status_and_scheduled_at` (`status`,`scheduled_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_network_graphs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_network_graphs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `job_status_id` char(36) DEFAULT NULL,
  `network_hash` varchar(64) DEFAULT NULL,
  `built_at` datetime DEFAULT NULL,
  `focus` int(11) DEFAULT '0',
  `meta` mediumblob,
  `commits_index` mediumblob,
  `commits_data` mediumblob,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_network_graphs_on_repository_id` (`repository_id`),
  KEY `index_repository_network_graphs_on_built_at` (`built_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_networks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_networks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `root_id` int(11) NOT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `repository_count` int(11) DEFAULT NULL,
  `disk_usage` int(11) DEFAULT NULL,
  `accessed_at` datetime DEFAULT NULL,
  `maintenance_status` varchar(255) NOT NULL,
  `last_maintenance_at` datetime NOT NULL,
  `pushed_at` datetime DEFAULT NULL,
  `pushed_count` int(11) NOT NULL,
  `pushed_count_since_maintenance` int(11) NOT NULL,
  `disabled_at` datetime DEFAULT NULL,
  `disabled_by` int(11) DEFAULT NULL,
  `disabling_reason` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `moving` tinyint(1) NOT NULL DEFAULT '0',
  `unpacked_size_in_mb` int(11) DEFAULT NULL,
  `last_maintenance_attempted_at` datetime DEFAULT NULL,
  `cache_version_number` int(11) NOT NULL DEFAULT '0',
  `maintenance_retries` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_repository_networks_on_root_id` (`root_id`),
  KEY `index_repository_networks_on_owner_id` (`owner_id`),
  KEY `index_repository_networks_on_created_at` (`created_at`),
  KEY `index_repository_networks_on_pushed_at` (`pushed_at`),
  KEY `index_repository_networks_on_accessed_at` (`accessed_at`),
  KEY `index_repository_networks_on_maintenance_status` (`maintenance_status`,`pushed_count_since_maintenance`,`unpacked_size_in_mb`,`last_maintenance_at`),
  KEY `index_repository_networks_on_maint_status_and_unpacked_size` (`maintenance_status`,`unpacked_size_in_mb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_recommendation_dismissals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_recommendation_dismissals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_repo_rec_dismissals_on_repository_id_and_user_id` (`repository_id`,`user_id`),
  KEY `index_repository_recommendation_dismissals_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_recommendation_opt_outs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_recommendation_opt_outs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_recommendation_opt_outs_on_repository_id` (`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_redirects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_redirects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `repository_name` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_repository_redirects_on_repository_id` (`repository_id`),
  KEY `index_repository_redirects_on_repository_name_and_created_at` (`repository_name`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_replicas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_replicas` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `repository_type` int(11) NOT NULL DEFAULT '0',
  `host` varchar(255) NOT NULL,
  `checksum` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `network_replica_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_replicas_on_repository_type_and_host` (`repository_id`,`repository_type`,`host`),
  KEY `update_time` (`updated_at`),
  KEY `host_only` (`host`),
  KEY `index_repository_replicas_on_network_replica_id` (`network_replica_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_sequences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_sequences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `number` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_sequences_on_repository_id` (`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_topics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_topics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_topics_on_repository_id_and_topic_id` (`repository_id`,`topic_id`),
  KEY `index_repository_topics_on_repository_id_and_state` (`repository_id`,`state`),
  KEY `index_repository_topics_on_topic_id_and_state` (`topic_id`,`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_transfers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_transfers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `requester_id` int(11) NOT NULL,
  `responder_id` int(11) DEFAULT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `target_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_transfers_on_repository_id_and_target_id` (`repository_id`,`target_id`),
  KEY `index_repository_transfers_on_requester_id` (`requester_id`),
  KEY `index_repository_transfers_on_responder_id` (`responder_id`),
  KEY `index_repository_transfers_on_state` (`state`),
  KEY `index_repository_transfers_on_target_id` (`target_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_unlocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_unlocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unlocked_by_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `expires_at` datetime NOT NULL,
  `revoked` tinyint(1) NOT NULL DEFAULT '0',
  `revoked_by_id` int(11) DEFAULT NULL,
  `revoked_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `staff_access_grant_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_repository_unlocks_on_repository_id` (`repository_id`),
  KEY `index_repository_unlocks_on_staff_access_grant_id` (`staff_access_grant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_vulnerability_alerts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_vulnerability_alerts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vulnerability_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `vulnerable_version_range_id` int(11) NOT NULL,
  `show_alert` tinyint(1) NOT NULL DEFAULT '1',
  `vulnerable_manifest_path` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `vulnerable_requirements` varchar(64) DEFAULT NULL,
  `last_detected_at` datetime DEFAULT NULL,
  `dismisser_id` int(11) DEFAULT NULL,
  `dismiss_reason` varchar(255) DEFAULT NULL,
  `dismissed_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_vulnerability_alerts_unique` (`vulnerability_id`,`vulnerable_version_range_id`,`vulnerable_manifest_path`,`repository_id`),
  KEY `index_on_vulnerable_version_range_id` (`vulnerable_version_range_id`),
  KEY `index_on_repository_id_and_show_alert` (`repository_id`,`show_alert`),
  KEY `index_on_repository_id_and_last_detected_at` (`repository_id`,`last_detected_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_wiki_disabled_backups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_wiki_disabled_backups` (
  `repository_id` int(11) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `disabled_at` datetime NOT NULL,
  PRIMARY KEY (`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `repository_wikis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_wikis` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `pushed_at` datetime DEFAULT NULL,
  `maintenance_status` varchar(255) DEFAULT NULL,
  `last_maintenance_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `pushed_count` int(11) NOT NULL DEFAULT '0',
  `pushed_count_since_maintenance` int(11) NOT NULL DEFAULT '0',
  `last_maintenance_attempted_at` datetime DEFAULT NULL,
  `cache_version_number` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_wikis_on_repository_id` (`repository_id`),
  KEY `index_repository_wikis_on_maintenance_status` (`maintenance_status`,`pushed_count_since_maintenance`,`last_maintenance_at`),
  KEY `index_repository_wikis_on_pushed_at` (`pushed_at`),
  KEY `index_repository_wikis_on_last_maintenance_at` (`last_maintenance_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `required_status_checks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `required_status_checks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `protected_branch_id` int(11) NOT NULL,
  `context` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_required_status_checks_on_id_and_context` (`protected_branch_id`,`context`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `reserved_logins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reserved_logins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(40) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_reserved_logins_on_login` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `restorable_custom_email_routings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restorable_custom_email_routings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restorable_id` int(11) NOT NULL,
  `organization_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_restorable_custom_email_routings` (`restorable_id`,`organization_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `restorable_issue_assignments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restorable_issue_assignments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restorable_id` int(11) NOT NULL,
  `issue_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_restorable_issue_assignments` (`restorable_id`,`issue_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `restorable_ldap_team_sync_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restorable_ldap_team_sync_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restorable_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `restorable_ldap_team_member` (`team_id`,`member_id`),
  KEY `index_restorable_ldap_team_sync_users_on_restorable_id` (`restorable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `restorable_memberships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restorable_memberships` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restorable_id` int(11) NOT NULL,
  `subject_type` varchar(255) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `action` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_restorable_memberships` (`restorable_id`,`subject_type`,`subject_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `restorable_organization_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restorable_organization_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restorable_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `organization_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_restorable_organization_users` (`user_id`,`organization_id`,`restorable_id`),
  KEY `index_restorable_organization_users_on_restorable_id` (`restorable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `restorable_repositories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restorable_repositories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restorable_id` int(11) NOT NULL,
  `archived_repository_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_restorable_repositories` (`restorable_id`,`archived_repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `restorable_repository_stars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restorable_repository_stars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restorable_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_restorable_repository_stars` (`restorable_id`,`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `restorable_type_states`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restorable_type_states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restorable_id` int(11) DEFAULT NULL,
  `restorable_type` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_restorable_type_states` (`restorable_id`,`restorable_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `restorable_watched_repositories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restorable_watched_repositories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restorable_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `ignored` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_restorable_watched_repositories` (`restorable_id`,`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `restorables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restorables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_restorables_on_created_at` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `retired_namespaces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `retired_namespaces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_login` varchar(40) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_retired_namespaces_on_owner_login_and_name` (`owner_login`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `review_dismissal_allowances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review_dismissal_allowances` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `protected_branch_id` int(11) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `actor_type` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_review_dismissal_allowances_on_branch_and_actor` (`protected_branch_id`,`actor_id`,`actor_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `review_request_delegation_excluded_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review_request_delegation_excluded_members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `excluded_members_team_user_unique` (`team_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `review_request_reasons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review_request_reasons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `review_request_id` int(11) NOT NULL,
  `codeowners_tree_oid` varchar(40) DEFAULT NULL,
  `codeowners_path` varchar(255) DEFAULT NULL,
  `codeowners_line` int(11) DEFAULT NULL,
  `codeowners_pattern` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_review_request_reasons_on_review_request_id` (`review_request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `review_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reviewer_id` int(11) NOT NULL,
  `pull_request_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `reviewer_type` varchar(64) NOT NULL DEFAULT 'User',
  `dismissed_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_review_requests_on_reviewer_id_and_reviewer_type` (`reviewer_id`,`reviewer_type`),
  KEY `index_review_requests_on_pr_id_and_reviewer_type_and_reviewer_id` (`pull_request_id`,`reviewer_type`,`reviewer_id`),
  KEY `index_review_requests_on_pull_request_id_and_dismissed_at` (`pull_request_id`,`dismissed_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `role_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `fine_grained_permission_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `action` varchar(60) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_role_perms_by_role_and_fgp` (`fine_grained_permission_id`,`role_id`),
  KEY `index_role_permissions_on_role_id_and_action` (`role_id`,`action`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varbinary(255) NOT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `owner_type` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `base_role_id` int(11) DEFAULT NULL,
  `description` varbinary(608) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_roles_on_name_and_owner_id_and_owner_type` (`name`,`owner_id`,`owner_type`),
  KEY `index_roles_on_owner_id_and_owner_type` (`owner_id`,`owner_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `rollup_summaries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rollup_summaries` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `list_type` varchar(64) NOT NULL DEFAULT 'Repository',
  `list_id` int(11) unsigned DEFAULT NULL,
  `raw_data` blob,
  `thread_key` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_rollup_summaries_on_list_type_and_list_id_and_thread_key` (`list_type`,`list_id`,`thread_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `saml_mappings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `saml_mappings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name_id` varchar(255) NOT NULL,
  `name_id_format` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_saml_mappings_on_name_id` (`name_id`),
  UNIQUE KEY `index_saml_mappings_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `saml_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `saml_sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `session_index` varchar(255) DEFAULT NULL,
  `logout_request_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_saml_sessions_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `saved_notification_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `saved_notification_entries` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `summary_id` int(11) NOT NULL,
  `list_type` varchar(64) NOT NULL DEFAULT 'Repository',
  `list_id` int(11) NOT NULL,
  `thread_key` varchar(80) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_saved_notification_entries_on_user_list_and_thread` (`user_id`,`list_type`,`list_id`,`thread_key`),
  KEY `index_saved_notification_entries_on_user_id_and_created_at` (`user_id`,`created_at`),
  KEY `index_saved_notification_entries_on_user_list_and_created_at` (`user_id`,`list_type`,`list_id`,`created_at`),
  KEY `index_saved_notification_entries_on_user_id_and_summary_id` (`user_id`,`summary_id`),
  KEY `index_saved_notification_entries_on_list_and_thread` (`list_id`,`list_type`,`thread_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `saved_replies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `saved_replies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varbinary(1024) NOT NULL,
  `body` mediumblob NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_saved_replies_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `science_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `science_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(85) NOT NULL,
  `event_type` varchar(10) NOT NULL,
  `payload` mediumblob NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_science_events_on_name_and_event_type` (`name`,`event_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `scoped_integration_installations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scoped_integration_installations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `integration_installation_id` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `expires_at` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_scoped_installations_on_integration_installation_id` (`integration_installation_id`),
  KEY `index_scoped_integration_installations_on_created_at` (`created_at`),
  KEY `index_scoped_integration_installations_on_expires_at` (`expires_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `search_index_configurations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_index_configurations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) NOT NULL,
  `index_type` varchar(100) NOT NULL,
  `index_version` int(11) NOT NULL,
  `slice_version` int(11) DEFAULT NULL,
  `version_sha` varchar(40) DEFAULT NULL,
  `is_readable` tinyint(1) NOT NULL DEFAULT '0',
  `is_writable` tinyint(1) NOT NULL DEFAULT '0',
  `is_primary` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `cluster` varchar(100) NOT NULL,
  `slice_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_search_index_configurations_on_fullname` (`fullname`),
  KEY `index_search_index_configurations_on_index_type_and_version` (`index_type`,`index_version`),
  KEY `index_search_index_configurations_on_index_type_and_slice_name` (`index_type`,`slice_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `search_index_template_configurations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_index_template_configurations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(127) NOT NULL,
  `template_type` varchar(100) NOT NULL,
  `template_version` int(11) NOT NULL DEFAULT '0',
  `cluster` varchar(100) NOT NULL,
  `version_sha` varchar(40) DEFAULT NULL,
  `is_writable` tinyint(1) NOT NULL DEFAULT '0',
  `is_primary` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_search_index_template_configurations_on_fullname` (`fullname`),
  KEY `index_search_index_template_configurations_on_template_type` (`template_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sequences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sequences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `context_type` varchar(255) NOT NULL,
  `context_id` int(11) NOT NULL,
  `number` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_sequences_contexts` (`context_type`,`context_id`),
  UNIQUE KEY `index_sequences_all` (`context_type`,`context_id`,`number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `shared_storage_artifact_aggregations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shared_storage_artifact_aggregations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `repository_id` int(11) DEFAULT NULL,
  `aggregate_effective_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `repository_visibility` enum('unknown','public','private') NOT NULL DEFAULT 'unknown',
  `aggregate_size_in_bytes` bigint(20) NOT NULL,
  `previous_aggregation_id` int(11) DEFAULT NULL,
  `synchronization_batch_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `billable_owner_type` varchar(12) DEFAULT NULL,
  `billable_owner_id` int(11) DEFAULT NULL,
  `directly_billed` tinyint(1) NOT NULL DEFAULT '1',
  `submission_state` enum('unsubmitted','submitted','skipped') NOT NULL DEFAULT 'unsubmitted',
  `submission_state_reason` varchar(24) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_shared_storage_artifact_aggregations_on_owner_repo_time` (`owner_id`,`repository_id`,`aggregate_effective_at`),
  KEY `index_on_shared_storage_artifacts_synchronization_batch` (`synchronization_batch_id`),
  KEY `idx_shared_storage_artifact_aggregations_sum_covering` (`billable_owner_type`,`repository_visibility`,`billable_owner_id`,`aggregate_effective_at`,`aggregate_size_in_bytes`),
  KEY `idx_shared_storage_artifact_aggregations_sum_with_owner_covering` (`billable_owner_type`,`repository_visibility`,`billable_owner_id`,`owner_id`,`aggregate_effective_at`,`aggregate_size_in_bytes`),
  KEY `index_shared_storage_aggregations_on_billable_usage` (`synchronization_batch_id`,`directly_billed`,`repository_visibility`,`aggregate_effective_at`,`owner_id`),
  KEY `index_shared_storage_artifact_aggregations_on_repository_id` (`billable_owner_type`,`repository_visibility`,`billable_owner_id`,`aggregate_effective_at`,`repository_id`),
  KEY `index_on_billable_owner_and_usage_and_repo` (`billable_owner_type`,`billable_owner_id`,`aggregate_effective_at`,`repository_id`),
  KEY `idx_shared_storage_artifact_aggregations_previous_aggregation` (`billable_owner_type`,`billable_owner_id`,`directly_billed`,`repository_id`,`owner_id`,`aggregate_effective_at`),
  KEY `index_shared_storage_aggregations_on_submission_state_and_reason` (`submission_state`,`submission_state_reason`),
  KEY `index_on_submission_state_and_created_at` (`submission_state`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `shared_storage_artifact_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shared_storage_artifact_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `repository_id` int(11) DEFAULT NULL,
  `effective_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `source` enum('unknown','actions','gpr') NOT NULL DEFAULT 'unknown',
  `repository_visibility` enum('unknown','public','private') NOT NULL DEFAULT 'unknown',
  `event_type` enum('unknown','add','remove') NOT NULL DEFAULT 'unknown',
  `size_in_bytes` bigint(20) NOT NULL,
  `aggregation_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `billable_owner_type` varchar(12) DEFAULT NULL,
  `billable_owner_id` int(11) DEFAULT NULL,
  `directly_billed` tinyint(1) NOT NULL DEFAULT '1',
  `source_artifact_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_shared_storage_artifact_events_on_owner_repo_time_agg` (`owner_id`,`repository_id`,`effective_at`,`aggregation_id`),
  KEY `index_shared_storage_artifact_events_on_aggregate_fields` (`owner_id`,`billable_owner_type`,`billable_owner_id`,`directly_billed`,`repository_id`,`effective_at`,`aggregation_id`),
  KEY `index_on_fields_for_artifact_expiration` (`source`,`event_type`,`owner_id`,`effective_at`,`repository_id`,`size_in_bytes`),
  KEY `index_shared_storage_artifact_events_on_source_artifact_id` (`source_artifact_id`),
  KEY `index_on_fields_for_stafftools_shared_storage_breakdown` (`owner_id`,`repository_visibility`,`effective_at`,`repository_id`,`source`,`aggregation_id`,`event_type`,`size_in_bytes`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `shared_storage_billable_owners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shared_storage_billable_owners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `billable_owner_type` varchar(12) NOT NULL,
  `billable_owner_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_shared_storage_billable_owners_on_billable_owner_and_owner` (`billable_owner_type`,`billable_owner_id`,`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `showcase_collections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `showcase_collections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `body` text,
  `slug` varchar(255) DEFAULT NULL,
  `published` tinyint(1) DEFAULT '0',
  `featured` tinyint(1) DEFAULT '0',
  `asset_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `showcase_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `showcase_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `collection_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `item_type` varchar(255) DEFAULT NULL,
  `body` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `item_per_collection` (`item_id`,`item_type`,`collection_id`),
  KEY `collection` (`collection_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sign_off_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sign_off_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pull_request_id` int(11) NOT NULL,
  `requester_id` int(11) NOT NULL,
  `requestee_id` int(11) NOT NULL,
  `requestee_type` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_requests_on_requestee_and_pull_request` (`pull_request_id`,`requestee_id`,`requestee_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sign_off_requests_sign_offs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sign_off_requests_sign_offs` (
  `sign_off_request_id` int(11) NOT NULL,
  `sign_off_id` int(11) NOT NULL,
  UNIQUE KEY `index_join_on_request_id_and_sign_off_id` (`sign_off_request_id`,`sign_off_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sign_offs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sign_offs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pull_request_id` int(11) NOT NULL,
  `signer_id` int(11) NOT NULL,
  `commit_id` varchar(40) NOT NULL,
  `message` varchar(255) DEFAULT NULL,
  `cancelled_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_sign_offs_on_pull_request_signer_and_cancelled_at` (`pull_request_id`,`signer_id`,`cancelled_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `site_scoped_integration_installations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_scoped_integration_installations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `integration_id` int(11) NOT NULL,
  `target_id` int(11) NOT NULL,
  `target_type` varchar(25) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `expires_at` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_site_scoped_integration_installations_on_integration_id` (`integration_id`),
  KEY `index_site_scoped_integration_installations_on_target` (`target_id`,`target_type`),
  KEY `index_site_scoped_integration_installations_on_created_at` (`created_at`),
  KEY `index_site_scoped_integration_installations_on_expires_at` (`expires_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site` varchar(12) NOT NULL,
  `region` varchar(8) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_sites_on_site` (`site`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `slotted_counters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slotted_counters` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `record_type` varchar(30) NOT NULL,
  `record_id` int(11) NOT NULL,
  `slot` int(11) NOT NULL DEFAULT '0',
  `count` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `records_and_slots` (`record_type`,`record_id`,`slot`),
  KEY `index_slotted_counters_on_record_type_and_record_id_and_count` (`record_type`,`record_id`,`count`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `spam_corpus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spam_corpus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` blob NOT NULL,
  `spam` tinyint(1) NOT NULL,
  `source_type` varchar(255) DEFAULT NULL,
  `source_id` int(11) DEFAULT NULL,
  `source_user_id` int(11) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_spam_corpus_on_source_type_and_source_id` (`source_type`,`source_id`),
  KEY `index_spam_corpus_on_owner_id` (`owner_id`),
  KEY `index_spam_corpus_on_source_user_id` (`source_user_id`),
  KEY `index_spam_corpus_on_spam` (`spam`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `spam_datasource_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spam_datasource_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `spam_datasource_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `additional_context` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_spam_datasource_entries_on_spam_datasource_id_and_value` (`spam_datasource_id`,`value`(15))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `spam_datasources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spam_datasources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_spam_datasources_on_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `spam_patterns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spam_patterns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pattern` text NOT NULL,
  `class_name` varchar(255) NOT NULL,
  `attribute_name` varchar(255) NOT NULL,
  `log` tinyint(1) NOT NULL DEFAULT '0',
  `queue` tinyint(1) NOT NULL DEFAULT '0',
  `flag` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `match_count` int(11) NOT NULL DEFAULT '0',
  `last_matched_at` datetime DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `false_positives` int(11) NOT NULL DEFAULT '0',
  `last_false_positive_at` datetime DEFAULT NULL,
  `suspend` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_spam_patterns_on_class_name` (`class_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `spam_queue_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spam_queue_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `spam_queue_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_login` varchar(40) NOT NULL,
  `additional_context` mediumblob,
  `spam_source_id` int(11) DEFAULT NULL,
  `spam_source_type` varchar(255) DEFAULT NULL,
  `added_by_user_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `locked_by_user_id` int(11) DEFAULT NULL,
  `locked_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sqe_uniq_user_queue` (`spam_queue_id`,`user_id`),
  KEY `index_spam_queue_entries_on_spam_queue_id_and_locked_at` (`spam_queue_id`,`locked_at`),
  KEY `index_spam_queue_entries_on_spam_queue_id_and_locked_by_user_id` (`spam_queue_id`,`locked_by_user_id`),
  KEY `index_spam_queue_entries_on_spam_queue_id_and_created_at` (`spam_queue_id`,`created_at`),
  KEY `index_spam_queue_entries_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `spam_queues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spam_queues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `spam_queue_by_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `spammy_notification_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spammy_notification_entries` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `summary_id` int(11) NOT NULL,
  `list_type` varchar(64) NOT NULL DEFAULT 'Repository',
  `list_id` int(11) NOT NULL,
  `thread_key` varchar(80) NOT NULL,
  `unread` tinyint(4) DEFAULT '1',
  `reason` varchar(40) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `last_read_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index_on_user_and_list_and_thread` (`user_id`,`list_type`,`list_id`,`thread_key`),
  KEY `index_on_list_and_thread` (`list_type`,`list_id`,`thread_key`),
  KEY `index_on_updated_at` (`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sponsors_activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sponsors_activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sponsorable_id` int(11) NOT NULL,
  `sponsor_id` int(11) DEFAULT NULL,
  `action` int(11) NOT NULL,
  `sponsors_tier_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `old_sponsors_tier_id` int(11) DEFAULT NULL,
  `sponsorable_type` int(11) NOT NULL,
  `sponsor_type` int(11) DEFAULT NULL,
  `timestamp` datetime NOT NULL,
  `matched_sponsorship` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_sponsors_activities_on_sponsorable_and_timestamp` (`sponsorable_id`,`sponsorable_type`,`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sponsors_activity_metrics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sponsors_activity_metrics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `sponsorable_id` int(11) NOT NULL,
  `sponsorable_type` int(11) NOT NULL,
  `metric` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  `recorded_on` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_sponsors_metrics_on_sponsorable_and_metric_and_recorded_on` (`sponsorable_id`,`sponsorable_type`,`metric`,`recorded_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sponsors_criteria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sponsors_criteria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(60) NOT NULL,
  `description` blob NOT NULL,
  `criterion_type` int(11) NOT NULL DEFAULT '0',
  `automated` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `applicable_to` tinyint(4) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_sponsors_criteria_on_slug` (`slug`),
  KEY `index_sponsors_criteria_on_criterion_type` (`criterion_type`),
  KEY `index_sponsors_criteria_on_applicable_to` (`applicable_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sponsors_fraud_reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sponsors_fraud_reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sponsors_listing_id` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `reviewer_id` int(11) DEFAULT NULL,
  `reviewed_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_sponsors_fraud_reviews_on_sponsors_listing_id` (`sponsors_listing_id`),
  KEY `index_sponsors_fraud_reviews_on_state` (`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sponsors_goal_contributions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sponsors_goal_contributions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sponsors_goal_id` int(11) NOT NULL,
  `sponsor_id` int(11) NOT NULL,
  `sponsors_tier_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_sponsors_goal_contributions_on_sponsors_goal_id` (`sponsors_goal_id`),
  KEY `index_sponsors_goal_contributions_on_sponsor_id_and_created_at` (`sponsor_id`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sponsors_goals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sponsors_goals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sponsors_listing_id` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `kind` int(11) NOT NULL DEFAULT '0',
  `target_value` int(11) NOT NULL DEFAULT '0',
  `description` blob,
  `completed_at` datetime DEFAULT NULL,
  `retired_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_sponsors_goals_on_sponsors_listing_id_and_state` (`sponsors_listing_id`,`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sponsors_listing_featured_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sponsors_listing_featured_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sponsors_listing_id` int(11) NOT NULL,
  `featureable_type` int(11) NOT NULL,
  `featureable_id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  `description` tinyblob,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_sponsors_listing_featured_items_unique` (`sponsors_listing_id`,`featureable_type`,`featureable_id`),
  KEY `index_sponsors_listing_featured_items_on_featureable` (`featureable_type`,`featureable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sponsors_listings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sponsors_listings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `sponsorable_type` int(11) NOT NULL,
  `sponsorable_id` int(11) NOT NULL,
  `short_description` tinyblob,
  `full_description` blob,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `published_at` datetime DEFAULT NULL,
  `payout_probation_started_at` datetime DEFAULT NULL,
  `payout_probation_ended_at` datetime DEFAULT NULL,
  `match_disabled` tinyint(1) NOT NULL DEFAULT '0',
  `stripe_authorization_code` varchar(40) DEFAULT NULL,
  `docusign_envelope_id` varchar(40) DEFAULT NULL,
  `docusign_envelope_status` varchar(10) DEFAULT NULL,
  `sponsors_membership_id` int(11) DEFAULT NULL,
  `match_limit_reached_at` datetime DEFAULT NULL,
  `milestone_email_sent_at` datetime DEFAULT NULL,
  `approval_requested_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_sponsors_listings_on_slug` (`slug`),
  UNIQUE KEY `index_sponsors_listings_on_sponsorable_id_and_sponsorable_type` (`sponsorable_id`,`sponsorable_type`),
  UNIQUE KEY `index_sponsors_listings_on_docusign_envelope_id` (`docusign_envelope_id`),
  UNIQUE KEY `index_sponsors_listings_on_sponsors_membership_id` (`sponsors_membership_id`),
  KEY `index_sponsors_listings_on_payout_probation` (`payout_probation_started_at`,`payout_probation_ended_at`),
  KEY `index_sponsors_listings_on_state_and_payout_probation_started` (`state`,`payout_probation_started_at`),
  KEY `index_sponsors_listings_on_state_and_payout_probation` (`state`,`payout_probation_ended_at`,`payout_probation_started_at`),
  KEY `index_sponsors_listings_on_match_disabled_and_created_at` (`match_disabled`,`created_at`),
  KEY `index_sponsors_listings_on_match_disabled_and_published_at` (`match_disabled`,`published_at`),
  KEY `index_sponsors_listings_on_stripe_authorization_code` (`stripe_authorization_code`),
  KEY `index_sponsors_listings_on_state_and_approval_requested_at` (`state`,`approval_requested_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sponsors_memberships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sponsors_memberships` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` int(11) NOT NULL DEFAULT '0',
  `sponsorable_type` int(11) NOT NULL,
  `sponsorable_id` int(11) NOT NULL,
  `survey_id` int(11) DEFAULT NULL,
  `reviewer_id` int(11) DEFAULT NULL,
  `appeal_allowed` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `billing_country` varchar(60) DEFAULT NULL,
  `contact_email_id` int(11) DEFAULT NULL,
  `reviewed_at` datetime DEFAULT NULL,
  `legal_name` varchar(255) DEFAULT NULL,
  `featured_description` varbinary(256) DEFAULT NULL,
  `featured_state` int(11) DEFAULT '0',
  `country_of_residence` char(2) DEFAULT NULL,
  `banned_at` datetime DEFAULT NULL,
  `banned_by_id` int(11) DEFAULT NULL,
  `banned_reason` varbinary(1024) DEFAULT NULL,
  `ignored_at` datetime DEFAULT NULL,
  `fiscal_host` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_sponsors_membership_on_sponsorable_type_and_sponsorable_id` (`sponsorable_id`,`sponsorable_type`),
  KEY `index_sponsors_memberships_on_state_and_sponsorable_type` (`state`,`sponsorable_type`),
  KEY `index_sponsors_memberships_on_contact_email_id` (`contact_email_id`),
  KEY `index_sponsors_memberships_on_reviewed_at` (`reviewed_at`),
  KEY `index_sponsors_memberships_on_featured_state_and_sponsorable_id` (`featured_state`,`sponsorable_id`),
  KEY `index_on_ignored_state_country` (`ignored_at`,`state`,`billing_country`),
  KEY `index_sponsors_memberships_on_fiscal_host_and_state` (`fiscal_host`,`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sponsors_memberships_criteria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sponsors_memberships_criteria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sponsors_criterion_id` int(11) NOT NULL,
  `sponsors_membership_id` int(11) NOT NULL,
  `reviewer_id` int(11) DEFAULT NULL,
  `met` tinyint(1) NOT NULL DEFAULT '0',
  `value` blob,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_on_membership_and_criterion` (`sponsors_membership_id`,`sponsors_criterion_id`),
  KEY `index_sponsors_memberships_criteria_on_sponsors_criterion_id` (`sponsors_criterion_id`),
  KEY `index_sponsors_memberships_criteria_on_reviewer_id` (`reviewer_id`),
  KEY `index_on_membership_and_met` (`sponsors_membership_id`,`met`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sponsors_tiers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sponsors_tiers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sponsors_listing_id` int(11) NOT NULL,
  `name` varbinary(1024) NOT NULL,
  `description` mediumblob NOT NULL,
  `state` smallint(6) NOT NULL DEFAULT '0',
  `monthly_price_in_cents` int(11) NOT NULL DEFAULT '0',
  `yearly_price_in_cents` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `marketplace_listing_plan_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_sponsors_tiers_on_name_and_sponsors_listing_id_and_state` (`name`,`sponsors_listing_id`,`state`),
  KEY `index_sponsors_tiers_on_listing_and_monthly_price_in_cents` (`sponsors_listing_id`,`monthly_price_in_cents`),
  KEY `index_sponsors_tiers_on_name_listing_and_created_at` (`name`,`sponsors_listing_id`,`created_at`),
  KEY `index_sponsors_tiers_on_marketplace_listing_plan_id` (`marketplace_listing_plan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sponsorship_match_bans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sponsorship_match_bans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sponsorable_id` int(11) NOT NULL,
  `sponsorable_type` int(11) NOT NULL,
  `sponsor_id` int(11) NOT NULL,
  `sponsor_type` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_sponsorship_match_bans_on_sponsorable` (`sponsorable_id`,`sponsorable_type`),
  KEY `index_sponsorship_match_bans_on_sponsor` (`sponsor_id`,`sponsor_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sponsorship_newsletter_tiers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sponsorship_newsletter_tiers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sponsorship_newsletter_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `sponsors_tier_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_newsletter_and_tier` (`sponsorship_newsletter_id`,`sponsors_tier_id`),
  KEY `index_newsletter_tiers_on_tier` (`sponsors_tier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sponsorship_newsletters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sponsorship_newsletters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `body` mediumblob NOT NULL,
  `subject` varbinary(1024) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `sponsorable_type` int(11) NOT NULL,
  `sponsorable_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_sponsorship_newsletters_on_author_id` (`author_id`),
  KEY `index_sponsorship_newsletters_on_sponsorable_and_state` (`sponsorable_type`,`sponsorable_id`,`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sponsorships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sponsorships` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `privacy_level` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `sponsor_type` int(11) NOT NULL,
  `sponsor_id` int(11) NOT NULL,
  `is_sponsor_opted_in_to_email` tinyint(1) NOT NULL DEFAULT '0',
  `maintainer_notes` text,
  `is_sponsor_tier_reward_fulfilled` tinyint(1) NOT NULL DEFAULT '0',
  `subscription_item_id` int(11) NOT NULL,
  `is_sponsor_opted_in_to_share_with_fiscal_host` tinyint(1) NOT NULL DEFAULT '0',
  `sponsorable_type` int(11) NOT NULL,
  `sponsorable_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `subscribable_type` int(11) DEFAULT NULL,
  `subscribable_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_sponsorships_sponsor_sponsorable_unique` (`sponsor_id`,`sponsor_type`,`sponsorable_id`,`sponsorable_type`),
  KEY `index_sponsorships_on_privacy_level` (`privacy_level`),
  KEY `index_sponsorships_on_is_sponsor_opted_in_to_email` (`is_sponsor_opted_in_to_email`),
  KEY `index_sponsorships_on_sponsorable_id_and_sponsorable_type` (`sponsorable_id`,`sponsorable_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ssh_certificate_authorities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ssh_certificate_authorities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_type` varchar(8) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `openssh_public_key` text NOT NULL,
  `fingerprint` varbinary(32) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_ssh_certificate_authorities_on_fingerprint` (`fingerprint`),
  KEY `index_ssh_certificate_authorities_on_owner_type_and_owner_id` (`owner_type`,`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `staff_access_grants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff_access_grants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `granted_by_id` int(11) NOT NULL,
  `revoked_by_id` int(11) DEFAULT NULL,
  `staff_access_request_id` int(11) DEFAULT NULL,
  `reason` varchar(255) NOT NULL,
  `revoked_at` datetime DEFAULT NULL,
  `expires_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `accessible_type` varchar(255) DEFAULT NULL,
  `accessible_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_staff_access_grants_on_staff_access_request_id` (`staff_access_request_id`),
  KEY `index_staff_access_grants_on_granted_by_id` (`granted_by_id`),
  KEY `index_staff_access_grants_on_accessible_type_and_accessible_id` (`accessible_type`,`accessible_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `staff_access_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff_access_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `requested_by_id` int(11) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `expires_at` datetime NOT NULL,
  `cancelled_at` datetime DEFAULT NULL,
  `cancelled_by_id` int(11) DEFAULT NULL,
  `denied_at` datetime DEFAULT NULL,
  `denied_by_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `accessible_type` varchar(255) DEFAULT NULL,
  `accessible_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_staff_access_requests_on_requested_by_id` (`requested_by_id`),
  KEY `index_staff_access_requests_on_accessible_type_and_accessible_id` (`accessible_type`,`accessible_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `staff_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `note` text,
  `user_id` int(11) DEFAULT NULL,
  `notable_id` int(11) DEFAULT NULL,
  `notable_type` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_staff_notes_on_user_id` (`user_id`),
  KEY `index_staff_notes_on_notable_id` (`notable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `stafftools_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stafftools_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `starred_gists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `starred_gists` (
  `user_id` int(11) DEFAULT NULL,
  `gist_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_starred_gists_on_user_id_and_gist_id` (`user_id`,`gist_id`),
  KEY `index_starred_gists_on_created_at` (`created_at`),
  KEY `index_starred_gists_on_user_id_and_created_at` (`user_id`,`created_at`),
  KEY `index_starred_gists_on_gist_id_and_user_id` (`gist_id`,`user_id`),
  KEY `index_starred_gists_on_gist_id_and_created_at_and_user_id` (`gist_id`,`created_at`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `stars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `starrable_id` int(11) NOT NULL,
  `starrable_type` varchar(30) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_stars_on_user_id_starrable_id_and_starrable_type` (`user_id`,`starrable_id`,`starrable_type`),
  KEY `index_stars_on_starrable_type_and_created_at` (`starrable_type`,`created_at`),
  KEY `index_stars_on_starrable_id_starrable_type_user_hidden` (`starrable_id`,`starrable_type`,`user_hidden`),
  KEY `index_stars_on_starrable_id_and_created_at` (`starrable_id`,`created_at`),
  KEY `index_stars_on_user_id_and_created_at` (`user_id`,`created_at`),
  KEY `index_stars_on_created_at` (`created_at`),
  KEY `index_stars_on_starrable_id_and_user_hidden` (`starrable_id`,`user_hidden`),
  KEY `index_stars_on_user_hidden_and_user_id` (`user_hidden`,`user_id`),
  KEY `index_stars_on_starrable_id_type_created_user_hidden` (`starrable_id`,`starrable_type`,`created_at`,`user_hidden`),
  KEY `index_stars_on_starrable_id_starrable_type_user_and_user_hidden` (`starrable_id`,`starrable_type`,`user_id`,`user_hidden`),
  KEY `index_stars_on_user_id_and_starrable_id` (`user_id`,`starrable_id`),
  KEY `starrable_type_and_user_hidden_and_created_at_and_starrable_id` (`starrable_type`,`user_hidden`,`created_at`,`starrable_id`),
  KEY `index_stars_on_starrable_id_starrable_type_created_at_user_id` (`starrable_id`,`starrable_type`,`created_at`,`user_id`,`user_hidden`),
  KEY `user_id_and_starrable_type_and_created_at_and_starrable_id` (`user_id`,`starrable_type`,`created_at`,`starrable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statuses` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `state` varchar(20) NOT NULL DEFAULT 'unknown',
  `description` varchar(255) DEFAULT NULL,
  `target_url` blob,
  `sha` char(40) NOT NULL,
  `repository_id` int(11) unsigned NOT NULL,
  `creator_id` int(11) unsigned NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `pull_request_id` int(11) unsigned DEFAULT NULL,
  `context` varchar(255) NOT NULL DEFAULT 'default',
  `oauth_application_id` int(11) DEFAULT NULL,
  `tree_oid` binary(20) DEFAULT NULL,
  `commit_oid` binary(20) DEFAULT NULL,
  `performed_by_integration_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_statuses_on_sha_and_context_and_repository_id` (`sha`,`context`,`repository_id`),
  KEY `index_statuses_on_commit_oid_and_repository_id_and_context` (`commit_oid`,`repository_id`,`context`),
  KEY `index_statuses_on_repository_id_and_created_at_and_context` (`repository_id`,`created_at`,`context`),
  KEY `index_statuses_on_repository_id_and_sha_and_context` (`repository_id`,`sha`,`context`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `storage_blobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `storage_blobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oid` varchar(255) NOT NULL,
  `size` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_storage_blobs_on_oid` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `storage_file_servers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `storage_file_servers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `host` varchar(255) NOT NULL,
  `online` tinyint(1) NOT NULL DEFAULT '0',
  `embargoed` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `non_voting` tinyint(1) NOT NULL DEFAULT '0',
  `datacenter` varchar(20) DEFAULT NULL,
  `rack` varchar(20) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_storage_file_servers_on_host` (`host`),
  KEY `index_storage_file_servers_by_location` (`datacenter`,`rack`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `storage_partitions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `storage_partitions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `storage_file_server_id` int(11) NOT NULL,
  `partition` varchar(1) NOT NULL,
  `disk_free` bigint(20) NOT NULL,
  `disk_used` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_storage_partitions_on_storage_file_server_id_and_partition` (`storage_file_server_id`,`partition`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `storage_purges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `storage_purges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `storage_blob_id` int(11) NOT NULL,
  `purge_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_storage_purges_on_storage_blob_id` (`storage_blob_id`),
  KEY `index_storage_purges_on_purge_at` (`purge_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `storage_references`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `storage_references` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `storage_blob_id` int(11) NOT NULL,
  `uploadable_id` int(11) NOT NULL,
  `uploadable_type` varchar(255) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `by_uploadable` (`uploadable_id`,`uploadable_type`,`storage_blob_id`),
  KEY `by_uploadable_type` (`storage_blob_id`,`uploadable_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `storage_replicas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `storage_replicas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `storage_blob_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `host` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `by_server_blob` (`host`,`storage_blob_id`),
  KEY `index_storage_replicas_on_storage_blob_id` (`storage_blob_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `stratocaster_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stratocaster_events` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `raw_data` mediumblob,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_stratocaster_events_on_updated_at` (`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `stratocaster_indexes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stratocaster_indexes` (
  `index_key` varchar(32) NOT NULL DEFAULT '',
  `value` blob NOT NULL,
  PRIMARY KEY (`index_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
/*!50100 PARTITION BY KEY ()
PARTITIONS 100 */;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `stripe_connect_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stripe_connect_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payable_type` varchar(32) NOT NULL,
  `payable_id` int(11) NOT NULL,
  `stripe_account_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `stripe_account_details` blob,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_stripe_connect_accounts_on_payable_id_and_payable_type` (`payable_id`,`payable_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `stripe_webhooks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stripe_webhooks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kind` int(11) NOT NULL,
  `fingerprint` varchar(64) NOT NULL,
  `payload` text NOT NULL,
  `processed_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `account_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `status` enum('pending','processed','ignored') DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_stripe_webhooks_on_fingerprint` (`fingerprint`),
  KEY `index_stripe_webhooks_on_processed_at` (`processed_at`),
  KEY `index_stripe_webhooks_account_kind_created` (`account_id`,`kind`,`created_at`),
  KEY `index_stripe_webhooks_on_status_and_created_at` (`status`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `subscription_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscription_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_subscription_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `free_trial_ends_on` date DEFAULT NULL,
  `installed_at` datetime DEFAULT NULL,
  `subscribable_id` int(11) DEFAULT NULL,
  `subscribable_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_subscription_items_on_plan_sub_and_subscribable` (`plan_subscription_id`,`subscribable_type`,`subscribable_id`),
  KEY `idx_subscription_items_plan_sub_id_and_installed_at` (`plan_subscription_id`,`installed_at`,`created_at`),
  KEY `index_subscription_items_on_subscribable_and_created_at` (`subscribable_type`,`subscribable_id`,`created_at`),
  KEY `index_subscription_items_on_subscribable_and_updated_at` (`subscribable_type`,`subscribable_id`,`updated_at`),
  KEY `index_subscription_items_on_plan_quantity_subscribable` (`plan_subscription_id`,`quantity`,`subscribable_type`,`subscribable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `successor_invitations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `successor_invitations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `target_id` int(11) NOT NULL,
  `target_type` varchar(40) NOT NULL,
  `inviter_id` int(11) NOT NULL,
  `invitee_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `accepted_at` datetime DEFAULT NULL,
  `declined_at` datetime DEFAULT NULL,
  `canceled_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_successor_invitations_on_target` (`target_id`,`target_type`),
  KEY `index_successor_invitations_on_inviter_id` (`inviter_id`),
  KEY `index_successor_invitations_on_invitee_id` (`invitee_id`),
  KEY `index_successor_invitations_on_accepted_at` (`accepted_at`),
  KEY `index_successor_invitations_on_declined_at` (`declined_at`),
  KEY `index_successor_invitations_on_canceled_at` (`canceled_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `survey_answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `survey_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `choice_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `other_text` varbinary(3000) DEFAULT NULL,
  `survey_group_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_survey_answers_on_choice_id` (`choice_id`),
  KEY `index_survey_answers_on_user_id_and_survey_id` (`user_id`,`survey_id`),
  KEY `index_survey_answers_on_survey_id` (`survey_id`),
  KEY `index_survey_answers_on_survey_group_id` (`survey_group_id`),
  KEY `index_survey_answers_on_question_id` (`question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `survey_choices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `survey_choices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `text` text NOT NULL,
  `short_text` text NOT NULL,
  `display_order` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `index_survey_choices_on_question_id_and_active` (`question_id`,`active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `survey_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `survey_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `survey_questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `survey_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_id` int(11) NOT NULL,
  `text` text NOT NULL,
  `short_text` text NOT NULL,
  `display_order` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `hidden` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_survey_questions_on_survey_id` (`survey_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `surveys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `slug` varchar(255) NOT NULL,
  `respondents_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_surveys_on_slug` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `tabs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `anchor` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `repository_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `task_failures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task_failures` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `task_run_id` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `fingerprint` varchar(255) NOT NULL,
  `level` varchar(255) DEFAULT NULL,
  `name` varbinary(1024) DEFAULT NULL,
  `suite` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `backtrace` text,
  `message` text,
  `status` smallint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_task_failures_on_id_and_fingerprint_unique` (`task_run_id`,`fingerprint`),
  KEY `index_task_failures_on_task_run_id` (`task_run_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `task_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task_logs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `task_run_id` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `finished_scanning_at` datetime DEFAULT NULL,
  `uploader_id` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `name` varbinary(1024) NOT NULL,
  `guid` char(36) NOT NULL,
  `storage_blob_id` int(11) DEFAULT NULL,
  `content_type` varchar(255) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `oid` char(64) DEFAULT NULL,
  `storage_provider` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_task_logs_on_task_run_id` (`task_run_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `task_runs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task_runs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `target_url` text,
  `sha` char(40) NOT NULL,
  `source_ref` varchar(255) DEFAULT NULL,
  `current` tinyint(1) NOT NULL DEFAULT '0',
  `name` varbinary(1024) NOT NULL DEFAULT 'default',
  `repository_id` int(11) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `pull_request_id` int(11) DEFAULT NULL,
  `oauth_application_id` int(11) DEFAULT NULL,
  `tree_oid` char(40) DEFAULT NULL,
  `commit_oid` char(40) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `guid` char(36) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `state` smallint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_task_runs_on_pull_request_id_and_created_at` (`pull_request_id`,`created_at`),
  KEY `index_task_runs_on_source_ref_and_name_and_repository_id` (`source_ref`,`name`(767),`repository_id`),
  KEY `index_task_runs_on_sha_and_name_and_repository_id` (`sha`,`name`(767),`repository_id`),
  KEY `index_task_runs_on_commit_oid_and_repository_id_and_name` (`commit_oid`,`repository_id`,`name`(767)),
  KEY `index_task_runs_on_repository_id_and_created_at_and_name` (`repository_id`,`created_at`,`name`(767)),
  KEY `index_task_runs_on_tree_oid_and_repository_id_and_name_and_state` (`tree_oid`,`repository_id`,`name`(767),`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `team_change_parent_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team_change_parent_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `approved_by_id` int(11) DEFAULT NULL,
  `requester_id` int(11) NOT NULL,
  `requesting_team_type` int(11) NOT NULL DEFAULT '0',
  `parent_team_id` int(11) NOT NULL,
  `child_team_id` int(11) NOT NULL,
  `approved_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_team_change_parent_parent_id_approved_at` (`parent_team_id`,`approved_at`),
  KEY `index_team_change_parent_child_id_approved_at` (`child_team_id`,`approved_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `team_external_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team_external_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `group_id` varchar(50) NOT NULL,
  `group_name` varchar(100) NOT NULL,
  `group_description` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `synced_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_team_external_groups_on_team_id_and_group_id` (`team_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `team_group_mappings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team_group_mappings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tenant_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `group_id` varchar(40) NOT NULL,
  `group_name` varbinary(400) NOT NULL,
  `group_description` varbinary(2048) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `synced_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_team_group_mappings_on_team_id_and_group_id` (`team_id`,`group_id`),
  KEY `index_team_group_mappings_on_tenant_id` (`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `team_invitations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team_invitations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organization_invitation_id` int(11) NOT NULL,
  `inviter_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `role` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_team_invitations_on_organization_invitation_id` (`organization_invitation_id`),
  KEY `index_team_invitations_on_team_id` (`team_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `team_member_delegated_review_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team_member_delegated_review_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `delegated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_team_member_delegated_review_requests_on_team_member` (`team_id`,`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `team_membership_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team_membership_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `requester_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `approved_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_team_membership_requests_on_team_id` (`team_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `team_sync_business_tenants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team_sync_business_tenants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `business_id` int(11) NOT NULL,
  `provider_type` int(11) NOT NULL,
  `provider_id` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `setup_url_template` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `url` text,
  `encrypted_ssws_token` varbinary(1024) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_team_sync_business_tenants_on_business_id` (`business_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `team_sync_tenants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team_sync_tenants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organization_id` int(11) NOT NULL,
  `provider_type` int(11) NOT NULL,
  `provider_id` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `url` text,
  `encrypted_ssws_token` varbinary(1024) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_team_sync_tenants_on_organization_id` (`organization_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organization_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `permission` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `description` blob,
  `creator_id` int(11) DEFAULT NULL,
  `privacy` int(11) NOT NULL DEFAULT '0',
  `tree_path` mediumblob,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `review_request_delegation_enabled` tinyint(1) NOT NULL DEFAULT '0',
  `review_request_delegation_algorithm` int(11) DEFAULT '0',
  `review_request_delegation_member_count` int(11) DEFAULT '1',
  `review_request_delegation_notify_team` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_teams_on_organization_id_and_slug` (`organization_id`,`slug`),
  KEY `index_teams_on_organization_id_and_privacy` (`organization_id`,`privacy`),
  KEY `index_teams_on_tree_path` (`tree_path`(767)),
  KEY `index_teams_on_organization_id_and_deleted` (`organization_id`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `token_scan_result_locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `token_scan_result_locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_scan_result_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `commit_oid` varchar(40) NOT NULL,
  `blob_oid` varchar(40) NOT NULL,
  `path` varbinary(1024) NOT NULL,
  `start_line` int(11) NOT NULL,
  `end_line` int(11) NOT NULL,
  `start_column` int(11) NOT NULL,
  `end_column` int(11) NOT NULL,
  `ignore_token` int(11) NOT NULL DEFAULT '0',
  `blob_paths_processed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_token_scan_result_locations_on_result_and_location` (`token_scan_result_id`,`commit_oid`,`path`,`start_line`,`end_line`,`start_column`,`end_column`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `token_scan_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `token_scan_results` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `token_type` varchar(64) NOT NULL,
  `token_signature` varchar(64) NOT NULL,
  `resolution` int(11) DEFAULT NULL,
  `resolver_id` int(11) DEFAULT NULL,
  `resolved_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_token_scan_results_on_repository_and_type_and_signature` (`repository_id`,`token_type`,`token_signature`),
  KEY `index_token_scan_results_on_repo_and_resolution_and_created_at` (`repository_id`,`resolution`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `token_scan_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `token_scan_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `repository_id` int(11) NOT NULL,
  `scheduled_at` datetime DEFAULT NULL,
  `scanned_at` datetime DEFAULT NULL,
  `scan_state` int(11) NOT NULL DEFAULT '0',
  `retry_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_token_scan_statuses_on_repository_id` (`repository_id`),
  KEY `index_token_scan_statuses_on_scan_state` (`scan_state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `topic_relations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topic_relations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(35) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `relation_type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_topic_relations_on_topic_id_and_name` (`topic_id`,`name`),
  KEY `index_topic_relations_on_name_and_relation_type_and_topic_id` (`name`,`relation_type`,`topic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `topics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(35) NOT NULL,
  `flagged` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `short_description` varchar(255) DEFAULT NULL,
  `description` blob,
  `created_by` varchar(100) DEFAULT NULL,
  `display_name` varchar(50) DEFAULT NULL,
  `released` varchar(25) DEFAULT NULL,
  `wikipedia_url` text,
  `url` text,
  `github_url` text,
  `logo_url` text,
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `stargazer_count` int(11) NOT NULL DEFAULT '0',
  `applied_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_topics_on_name` (`name`),
  KEY `index_topics_on_featured` (`featured`),
  KEY `index_topics_on_short_description` (`short_description`),
  KEY `index_topics_on_flagged_and_name_and_applied_count` (`flagged`,`name`,`applied_count`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `tos_acceptances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tos_acceptances` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `sha` varchar(40) NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_tos_acceptances_on_user_id_and_sha` (`user_id`,`sha`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `trade_controls_restrictions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trade_controls_restrictions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type` enum('unrestricted','partial','full','review','tier_0','tier_1') NOT NULL DEFAULT 'unrestricted',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `trade_restricted_country_code` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_trade_controls_restrictions_on_user_id` (`user_id`),
  KEY `index_trade_controls_restrictions_on_type` (`type`),
  KEY `index_on_trade_restricted_country_code` (`trade_restricted_country_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(30) DEFAULT NULL,
  `current_plan` varchar(30) DEFAULT NULL,
  `old_plan` varchar(30) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `raw_data` blob,
  `billing_transaction_id` int(11) DEFAULT NULL,
  `plan_duration` varchar(30) DEFAULT NULL,
  `next_billed_on` date DEFAULT NULL,
  `old_seats` int(11) NOT NULL DEFAULT '0',
  `current_seats` int(11) NOT NULL DEFAULT '0',
  `asset_packs_total` int(11) NOT NULL DEFAULT '0',
  `asset_packs_delta` int(11) NOT NULL DEFAULT '0',
  `active_listing` tinyint(1) NOT NULL DEFAULT '0',
  `current_subscribable_id` int(11) DEFAULT NULL,
  `current_subscribable_type` int(11) DEFAULT NULL,
  `current_subscribable_quantity` int(11) DEFAULT NULL,
  `old_subscribable_id` int(11) DEFAULT NULL,
  `old_subscribable_type` int(11) DEFAULT NULL,
  `old_subscribable_quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_transactions_on_user_id` (`user_id`),
  KEY `index_transactions_on_timestamp` (`timestamp`),
  KEY `index_transactions_on_billing_transaction_id` (`billing_transaction_id`),
  KEY `index_transactions_on_action_and_timestamp` (`action`,`timestamp`),
  KEY `index_transactions_on_subscribables` (`current_subscribable_id`,`old_subscribable_id`,`current_subscribable_type`,`old_subscribable_type`),
  KEY `index_transactions_on_current_subscribables` (`current_subscribable_id`,`current_subscribable_type`),
  KEY `index_transactions_on_old_subscribables` (`old_subscribable_id`,`old_subscribable_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ts_analyses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ts_analyses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `repository_id` bigint(20) unsigned NOT NULL,
  `repository_nwo` varchar(140) DEFAULT NULL,
  `ref` varchar(255) NOT NULL,
  `commit_oid` varchar(40) NOT NULL,
  `analysis_name` varchar(255) NOT NULL,
  `tool` varchar(255) NOT NULL,
  `tool_version` varchar(255) NOT NULL,
  `environment` json NOT NULL,
  `most_recent` tinyint(1) DEFAULT '0',
  `analysis_complete` tinyint(1) NOT NULL DEFAULT '1',
  `started_at` datetime(6) DEFAULT NULL,
  `build_started_at` datetime(6) DEFAULT NULL,
  `workflow_run_id` int(11) DEFAULT NULL,
  `upload_started_at` datetime(6) DEFAULT NULL,
  `upload_finished_at` datetime(6) DEFAULT NULL,
  `enqueued_to_hydro_at` datetime(6) DEFAULT NULL,
  `processing_started_at` datetime(6) DEFAULT NULL,
  `processing_completed_at` datetime(6) DEFAULT NULL,
  `sarif_url` text,
  `sarif_cleaned` tinyint(1) DEFAULT '0',
  `fixes_cleaned` tinyint(1) DEFAULT '0',
  `alerts_cleaned` tinyint(1) DEFAULT '0',
  `analysis_key` varchar(255) DEFAULT NULL,
  `source_repository_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_analyses_on_repo_id_most_recent_ref_tool` (`repository_id`,`most_recent`,`ref`,`tool`),
  KEY `idx_analyses_on_repo_id_commit_oid` (`repository_id`,`commit_oid`),
  KEY `idx_analyses_on_sarif_cleaned` (`sarif_cleaned`,`most_recent`,`processing_completed_at`),
  KEY `idx_analyses_on_alerts_cleaned` (`alerts_cleaned`,`most_recent`,`processing_completed_at`),
  KEY `idx_analyses_on_fixes_cleaned` (`fixes_cleaned`,`most_recent`,`processing_completed_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ts_enum_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ts_enum_values` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `enum` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_enum_values_on_enum_and_name` (`enum`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ts_logical_alerts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ts_logical_alerts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `repository_id` bigint(20) unsigned NOT NULL,
  `number` int(10) unsigned NOT NULL,
  `rule_id` bigint(20) unsigned NOT NULL,
  `resolution` int(11) NOT NULL DEFAULT '0',
  `resolver_id` int(10) unsigned DEFAULT NULL,
  `resolved_at` datetime(6) DEFAULT NULL,
  `weight` smallint(6) unsigned NOT NULL DEFAULT '0',
  `stable_alert_identifier` binary(21) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_logical_alerts_on_repository_id_and_number` (`repository_id`,`number`),
  UNIQUE KEY `index_logical_alerts_uniq_location` (`repository_id`,`stable_alert_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ts_logical_alerts_seq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ts_logical_alerts_seq` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `repository_id` bigint(20) unsigned NOT NULL,
  `number` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_logical_alerts_seq_on_repository_id` (`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ts_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ts_migrations` (
  `version` bigint(20) NOT NULL,
  `dirty` tinyint(1) NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ts_physical_alerts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ts_physical_alerts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `repository_id` bigint(20) unsigned NOT NULL,
  `logical_alert_id` bigint(20) unsigned DEFAULT NULL,
  `rule_id` bigint(20) unsigned NOT NULL,
  `fingerprint` varchar(255) NOT NULL,
  `file_path` varchar(4096) NOT NULL,
  `start_line` int(10) unsigned DEFAULT NULL,
  `end_line` int(10) unsigned DEFAULT NULL,
  `start_column` int(10) unsigned DEFAULT NULL,
  `end_column` int(10) unsigned DEFAULT NULL,
  `stable_alert_identifier` binary(21) NOT NULL,
  `suppressed` tinyint(1) NOT NULL DEFAULT '0',
  `message` varchar(4096) NOT NULL,
  `severity_level` tinyint(3) unsigned NOT NULL,
  `analysis_id` bigint(20) unsigned NOT NULL,
  `last_seen_analysis_id` bigint(20) unsigned DEFAULT NULL,
  `file_classification` json NOT NULL,
  `has_file_classification` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_physical_alerts_uniq_location` (`repository_id`,`stable_alert_identifier`,`analysis_id`),
  KEY `idx_physical_alerts_on_repo_id_analysis_id` (`repository_id`,`analysis_id`),
  KEY `idx_physical_alerts_on_repo_id_logical_alert_id` (`repository_id`,`logical_alert_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ts_process_errors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ts_process_errors` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `repository_id` bigint(20) NOT NULL,
  `analysis_id` bigint(20) DEFAULT NULL,
  `error_type` tinyint(3) unsigned NOT NULL,
  `sarif_uri` varchar(4096) NOT NULL,
  `json_path` varchar(4096) DEFAULT NULL,
  `message` varchar(4096) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ts_ref_logical_alert_states`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ts_ref_logical_alert_states` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ref` varchar(255) NOT NULL,
  `logical_alert_id` bigint(20) unsigned NOT NULL,
  `eliminated` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_ref_logical_alert_states_on_ref_and_logical_alert_id` (`ref`,`logical_alert_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ts_related_locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ts_related_locations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `repository_id` bigint(20) unsigned NOT NULL,
  `file_path` varchar(4096) NOT NULL,
  `start_line` int(10) unsigned DEFAULT NULL,
  `end_line` int(10) unsigned DEFAULT NULL,
  `start_column` int(10) unsigned DEFAULT NULL,
  `end_column` int(10) unsigned DEFAULT NULL,
  `message` varchar(4096) NOT NULL,
  `replacement_index` int(10) unsigned NOT NULL,
  `physical_alert_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_related_locations_on_physical_alert_id` (`physical_alert_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ts_rule_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ts_rule_tags` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `rule_id` bigint(20) unsigned NOT NULL,
  `tag` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_rule_id_tag` (`rule_id`,`tag`),
  KEY `index_tag` (`tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ts_rules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ts_rules` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `repository_id` bigint(20) unsigned NOT NULL,
  `tool` varchar(255) NOT NULL,
  `sarif_identifier` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `short_description` text,
  `full_description` text,
  `help_uri` varchar(1024) DEFAULT NULL,
  `help` mediumtext,
  `severity_level` tinyint(3) unsigned NOT NULL,
  `properties` json NOT NULL,
  `precision` varchar(255) NOT NULL,
  `precision_level` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `deprecated_ids` json NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repo_tool_sarif` (`repository_id`,`tool`,`sarif_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ts_thread_flow_locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ts_thread_flow_locations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `repository_id` bigint(20) unsigned NOT NULL,
  `file_path` varchar(4096) NOT NULL,
  `start_line` int(10) unsigned DEFAULT NULL,
  `end_line` int(10) unsigned DEFAULT NULL,
  `start_column` int(10) unsigned DEFAULT NULL,
  `end_column` int(10) unsigned DEFAULT NULL,
  `message` varchar(4096) DEFAULT NULL,
  `code_flow_index` int(10) unsigned NOT NULL,
  `thread_flow_index` int(10) unsigned NOT NULL,
  `step_index` int(10) unsigned NOT NULL,
  `physical_alert_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_thread_flow_locations_on_physical_alert_id` (`physical_alert_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ts_timeline_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ts_timeline_events` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `repository_id` bigint(20) unsigned NOT NULL,
  `logical_alert_id` bigint(20) unsigned NOT NULL,
  `event_type` tinyint(3) unsigned NOT NULL,
  `event_timestamp` datetime(6) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `commit_oid` varchar(40) DEFAULT NULL,
  `ref` varchar(255) DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `resolution` tinyint(3) NOT NULL DEFAULT '0',
  `file_path` varchar(4096) DEFAULT NULL,
  `start_line` int(10) unsigned DEFAULT NULL,
  `tool_version` varchar(255) DEFAULT NULL,
  `analysis_id` bigint(20) DEFAULT NULL,
  `environment` json NOT NULL,
  `workflow_run_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_timeline_events_on_logical_id` (`logical_alert_id`),
  KEY `idx_timeline_events_on_analysis_id` (`analysis_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `two_factor_credentials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `two_factor_credentials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `secret` varchar(255) NOT NULL,
  `recovery_used_bitfield` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `sms_number` varchar(255) DEFAULT NULL,
  `delivery_method` varchar(255) NOT NULL DEFAULT 'app',
  `backup_sms_number` varchar(255) DEFAULT NULL,
  `recovery_codes_viewed` tinyint(1) DEFAULT '0',
  `provider` varchar(255) DEFAULT NULL,
  `encrypted_recovery_secret` varbinary(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_two_factor_credentials_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `two_factor_recovery_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `two_factor_recovery_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `otp_verified` tinyint(1) NOT NULL DEFAULT '0',
  `oauth_access_id` int(11) DEFAULT NULL,
  `authenticated_device_id` int(11) DEFAULT NULL,
  `public_key_id` int(11) DEFAULT NULL,
  `request_completed_at` datetime DEFAULT NULL,
  `reviewer_id` int(11) DEFAULT NULL,
  `review_completed_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `requesting_device_id` int(11) DEFAULT NULL,
  `approved_at` datetime DEFAULT NULL,
  `declined_at` datetime DEFAULT NULL,
  `staff_review_requested_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_two_factor_recovery_requests_on_user_id_and_created_at` (`user_id`,`created_at`),
  KEY `index_two_factor_recovery_requests_on_user_id_requesting_device` (`user_id`,`requesting_device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `u2f_registrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `u2f_registrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `key_handle` text NOT NULL,
  `certificate` text,
  `public_key` text NOT NULL,
  `counter` int(11) unsigned NOT NULL,
  `nickname` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `is_webauthn_registration` tinyint(1) NOT NULL DEFAULT '0',
  `webauthn_attestation` blob,
  PRIMARY KEY (`id`),
  KEY `index_u2f_registrations_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `upload_manifest_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `upload_manifest_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `upload_manifest_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `uploader_id` int(11) NOT NULL,
  `name` varbinary(1024) NOT NULL,
  `content_type` varchar(255) NOT NULL,
  `size` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `blob_oid` varchar(40) DEFAULT NULL,
  `oid` varchar(255) DEFAULT NULL,
  `directory` varbinary(1024) DEFAULT NULL,
  `storage_blob_id` int(11) DEFAULT NULL,
  `storage_provider` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_upload_manifest_files_by_upload_manifest_id` (`upload_manifest_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `upload_manifests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `upload_manifests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `uploader_id` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `message` blob,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `branch` varbinary(1024) DEFAULT NULL,
  `commit_oid` varchar(40) DEFAULT NULL,
  `directory` varbinary(1024) DEFAULT NULL,
  `base_branch` varbinary(1024) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `usage_synchronization_batches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usage_synchronization_batches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  `zuora_status_url` varchar(255) DEFAULT NULL,
  `upload_filename` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `partition` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_usage_synchronization_batches_on_status` (`status`),
  KEY `index_usage_sync_batches_on_prod_status_partition` (`product`,`status`,`partition`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `user_assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_assets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type` varchar(20) NOT NULL,
  `size` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `guid` varchar(36) DEFAULT NULL,
  `uploaded` tinyint(1) DEFAULT NULL,
  `state` int(11) DEFAULT '0',
  `storage_blob_id` int(11) DEFAULT NULL,
  `oid` varchar(255) DEFAULT NULL,
  `storage_provider` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_assets_on_guid` (`guid`),
  KEY `index_assets_on_user_id_and_created_at` (`user_id`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `user_content_edits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_content_edits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_content_id` int(11) NOT NULL,
  `user_content_type` varchar(40) NOT NULL,
  `edited_at` datetime NOT NULL,
  `editor_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by_id` int(11) DEFAULT NULL,
  `diff` mediumblob,
  PRIMARY KEY (`id`),
  KEY `index_user_content_edits_on_user_content_typ_and_user_content_id` (`user_content_type`,`user_content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `user_dashboard_pins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_dashboard_pins` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `pinned_item_id` int(11) NOT NULL,
  `pinned_item_type` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_user_dashboard_pins_unique` (`user_id`,`pinned_item_type`,`pinned_item_id`),
  KEY `index_user_dashboard_pins_on_user_id_and_position` (`user_id`,`position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `user_emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `primary` tinyint(1) DEFAULT NULL,
  `state` varchar(255) DEFAULT 'unverified',
  `verification_token` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deobfuscated_email` varchar(255) DEFAULT NULL,
  `verified_at` datetime DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `normalized_domain` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_user_emails_on_user_id_and_primary` (`user_id`,`primary`),
  KEY `index_user_emails_on_email` (`email`),
  KEY `index_user_emails_on_deobfuscated_email` (`deobfuscated_email`),
  KEY `index_user_emails_on_created_at` (`created_at`),
  KEY `index_user_emails_on_user_hidden_and_user_id` (`user_hidden`,`user_id`),
  KEY `index_user_emails_on_normalized_domain_and_user_id` (`normalized_domain`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `user_experiment_displays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_experiment_displays` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `user_experiment_id` int(11) NOT NULL,
  `source` varchar(255) DEFAULT NULL,
  `ignore` tinyint(1) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_experiment_user_index` (`user_experiment_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `user_experiment_enrollments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_experiment_enrollments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `user_experiment_id` int(11) NOT NULL,
  `subgroup` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `subject_id` bigint(20) unsigned DEFAULT NULL,
  `subject_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_experiment_enrollment_experiment_id_subject` (`user_experiment_id`,`subject_id`,`subject_type`),
  KEY `index_user_experiment_enrollment_grouping` (`user_experiment_id`,`subgroup`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `user_experiments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_experiments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `started_at` datetime DEFAULT NULL,
  `finished_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_user_experiments_on_slug` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `user_fact_dimensions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_fact_dimensions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_user_fact_dimensions_on_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `user_facts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_facts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `dimension_id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_user_facts_on_dimension_id_and_value` (`dimension_id`,`value`),
  KEY `index_user_facts_on_user_id_and_dimension_id_and_value` (`user_id`,`dimension_id`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `user_interests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_interests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `interest` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_user_interests_on_user_id_and_interests` (`user_id`,`interest`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `user_labels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_labels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varbinary(1024) NOT NULL,
  `lowercase_name` varbinary(1024) NOT NULL,
  `description` varbinary(400) DEFAULT NULL,
  `color` varchar(10) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_user_labels_on_name` (`name`),
  KEY `index_user_labels_on_user_id_and_name` (`user_id`,`name`),
  KEY `index_user_labels_on_user_id_and_lowercase_name` (`user_id`,`lowercase_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `user_licenses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_licenses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `business_id` int(11) NOT NULL,
  `license_type` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_user_licenses_on_business_id_and_email` (`business_id`,`email`),
  UNIQUE KEY `index_user_licenses_on_user_id_and_business_id` (`user_id`,`business_id`),
  KEY `index_user_licenses_on_business_id_and_license_type` (`business_id`,`license_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `user_note_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_note_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `note_id` int(11) DEFAULT NULL,
  `body` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_user_note_comments_on_note_id` (`note_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `user_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organization_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `body` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_user_notes_on_organization_id` (`organization_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `user_personal_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_personal_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(64) NOT NULL,
  `last_name` varchar(64) NOT NULL DEFAULT '',
  `middle_name` varchar(64) NOT NULL DEFAULT '',
  `region` varchar(64) DEFAULT NULL,
  `city` varchar(64) NOT NULL,
  `country_code` varchar(3) NOT NULL,
  `postal_code` varchar(32) DEFAULT NULL,
  `address1` varchar(128) NOT NULL,
  `address2` varchar(128) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `msft_trade_screening_status` tinyint(4) NOT NULL DEFAULT '0',
  `last_trade_screen_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_user_personal_profiles_on_user_id` (`user_id`),
  KEY `index_on_msft_trade_screening_status` (`msft_trade_screening_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `user_reviewed_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_reviewed_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pull_request_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `filepath` varbinary(1024) NOT NULL,
  `head_sha` varbinary(40) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `dismissed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_reviewed_files_on_pull_user_and_filepath` (`pull_request_id`,`user_id`,`filepath`),
  KEY `index_reviewed_files_on_user_pull_and_dismissed` (`user_id`,`pull_request_id`,`dismissed`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `target_id` int(11) NOT NULL,
  `target_type` varchar(60) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `actor_type` varchar(60) NOT NULL,
  `actor_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_user_roles_actor_and_target` (`actor_id`,`actor_type`,`target_id`,`target_type`),
  KEY `idx_user_roles_actor_role_and_target` (`actor_id`,`actor_type`,`role_id`,`target_id`,`target_type`),
  KEY `index_user_roles_on_target_id_and_target_type` (`target_id`,`target_type`),
  KEY `index_user_roles_on_role_id_and_actor_id_and_actor_type` (`role_id`,`actor_id`,`actor_type`),
  KEY `index_user_roles_on_role_target_type_actor` (`role_id`,`target_id`,`target_type`,`actor_type`,`actor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `user_seen_features`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_seen_features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `feature_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_user_seen_features_on_user_id_and_feature_id` (`user_id`,`feature_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `user_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `ip` varchar(60) DEFAULT NULL,
  `time_zone_name` varchar(60) DEFAULT NULL,
  `user_agent` text,
  `accessed_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `impersonator_id` int(11) DEFAULT NULL,
  `revoked_at` datetime DEFAULT NULL,
  `impersonator_session_id` int(11) DEFAULT NULL,
  `sudo_enabled_at` datetime DEFAULT NULL,
  `hashed_key` char(44) NOT NULL,
  `csrf_token` char(44) NOT NULL,
  `revoked_reason` varchar(50) DEFAULT NULL,
  `secret` char(44) DEFAULT NULL,
  `hashed_private_mode_key` char(44) DEFAULT NULL,
  `hashed_gist_key` varchar(44) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_user_sessions_on_hashed_key` (`hashed_key`),
  UNIQUE KEY `index_user_sessions_on_hashed_private_mode_key` (`hashed_private_mode_key`),
  UNIQUE KEY `index_user_sessions_on_hashed_gist_key` (`hashed_gist_key`),
  KEY `index_user_sessions_on_user_id_and_accessed_at` (`user_id`,`accessed_at`),
  KEY `index_user_sessions_on_accessed_at` (`accessed_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `user_stafftools_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_stafftools_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `stafftools_role_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_user_stafftools_roles_on_user_id` (`user_id`),
  KEY `index_user_stafftools_roles_on_stafftools_role_id` (`stafftools_role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `user_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_statuses` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `emoji` varbinary(44) DEFAULT NULL,
  `message` varbinary(800) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `organization_id` int(11) DEFAULT NULL,
  `limited_availability` tinyint(1) NOT NULL DEFAULT '0',
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_user_statuses_on_user_id` (`user_id`),
  KEY `index_user_statuses_on_organization_id_and_user_id` (`organization_id`,`user_id`),
  KEY `index_user_statuses_on_limited_availability` (`limited_availability`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `user_verticals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_verticals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `vertical_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_user_verticals_on_user_id` (`user_id`),
  KEY `user_verticals` (`vertical_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `user_whitelistings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_whitelistings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `whitelister_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_user_whitelistings_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(40) NOT NULL,
  `crypted_password` varchar(40) DEFAULT NULL,
  `salt` varchar(40) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `remember_token` varchar(40) DEFAULT NULL,
  `remember_token_expires_at` datetime DEFAULT NULL,
  `wants_email` tinyint(1) DEFAULT '1',
  `disabled` tinyint(1) DEFAULT '0',
  `plan` varchar(30) DEFAULT NULL,
  `billed_on` date DEFAULT NULL,
  `auth_token` varchar(255) DEFAULT NULL,
  `upgrade_ignore` varchar(30) DEFAULT NULL,
  `upgrade_accept` int(11) DEFAULT NULL,
  `gh_role` varchar(30) DEFAULT NULL,
  `billing_attempts` int(11) DEFAULT '0',
  `spammy` tinyint(1) DEFAULT '0',
  `last_ip` varchar(40) DEFAULT NULL,
  `plan_duration` varchar(20) DEFAULT NULL,
  `billing_extra` text,
  `gift` tinyint(1) DEFAULT NULL,
  `type` varchar(30) NOT NULL DEFAULT 'User',
  `last_read_broadcast_id` int(11) DEFAULT NULL,
  `raw_data` blob,
  `referral_code` varchar(255) DEFAULT NULL,
  `billing_type` varchar(20) NOT NULL DEFAULT 'card',
  `bcrypt_auth_token` varchar(60) DEFAULT NULL,
  `suspended_at` datetime DEFAULT NULL,
  `organization_billing_email` varchar(255) DEFAULT NULL,
  `gravatar_email` varchar(255) DEFAULT NULL,
  `time_zone_name` varchar(40) DEFAULT NULL,
  `session_fingerprint` varchar(32) DEFAULT NULL,
  `token_secret` varchar(40) DEFAULT NULL,
  `restrict_oauth_applications` tinyint(1) DEFAULT NULL,
  `spammy_reason` text,
  `seats` int(11) NOT NULL DEFAULT '0',
  `split_diff_preferred` tinyint(1) NOT NULL DEFAULT '0',
  `require_email_verification` tinyint(1) NOT NULL DEFAULT '0',
  `warn_private_email` tinyint(1) NOT NULL DEFAULT '0',
  `primary_language_name_id` int(11) DEFAULT NULL,
  `analytics_tracking_id` varchar(32) DEFAULT NULL,
  `report_third_party_analytics` tinyint(1) NOT NULL DEFAULT '1',
  `source_login` varchar(40) DEFAULT NULL,
  `migration_id` int(11) DEFAULT NULL,
  `ofac_flagged` tinyint(1) NOT NULL DEFAULT '0',
  `weak_password_check_result` varbinary(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_users_on_login` (`login`),
  UNIQUE KEY `index_users_on_analytics_tracking_id` (`analytics_tracking_id`),
  KEY `index_users_on_type` (`type`),
  KEY `index_users_on_last_ip` (`last_ip`),
  KEY `index_users_on_plan_and_billing_type_and_type` (`plan`,`billing_type`,`type`),
  KEY `index_users_on_updated_at` (`updated_at`),
  KEY `index_users_on_spammy` (`spammy`),
  KEY `index_users_on_suspended_at` (`suspended_at`),
  KEY `index_users_on_gh_role` (`gh_role`),
  KEY `index_users_on_created_at` (`created_at`),
  KEY `index_users_on_billed_on_and_plan_duration_and_plan_and_disabled` (`billed_on`,`plan_duration`,`plan`,`disabled`),
  KEY `index_users_on_organization_billing_email` (`organization_billing_email`),
  KEY `index_users_on_gravatar_email` (`gravatar_email`),
  KEY `index_users_on_migration_id` (`migration_id`),
  KEY `index_users_on_source_login` (`source_login`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `vss_subscription_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vss_subscription_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payload` text,
  `status` enum('unprocessed','processed','failed') NOT NULL DEFAULT 'unprocessed',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `vulnerabilities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vulnerabilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(12) NOT NULL,
  `platform` varchar(50) DEFAULT NULL,
  `severity` varchar(12) NOT NULL,
  `identifier` varchar(255) DEFAULT NULL,
  `classification` varchar(255) DEFAULT NULL,
  `external_reference` varchar(255) DEFAULT NULL,
  `description` blob,
  `created_by_id` int(11) NOT NULL,
  `published_at` datetime DEFAULT NULL,
  `published_by_id` int(11) DEFAULT NULL,
  `withdrawn_at` datetime DEFAULT NULL,
  `withdrawn_by_id` int(11) DEFAULT NULL,
  `simulation` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `ghsa_id` varchar(19) NOT NULL,
  `source` varchar(64) DEFAULT NULL,
  `source_identifier` varchar(128) DEFAULT NULL,
  `cve_id` varchar(20) DEFAULT NULL,
  `white_source_id` varchar(20) DEFAULT NULL,
  `summary` varbinary(1024) DEFAULT NULL,
  `npm_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_vulnerabilities_on_ghsa_id` (`ghsa_id`),
  UNIQUE KEY `index_vulnerabilities_on_source_and_source_identifier` (`source`,`source_identifier`),
  UNIQUE KEY `index_vulnerabilities_on_npm_id` (`npm_id`),
  KEY `index_vulnerabilities_on_identifier` (`identifier`),
  KEY `index_vulnerabilities_on_platform` (`platform`),
  KEY `index_vulnerabilities_on_severity` (`severity`),
  KEY `index_vulnerabilities_on_published_at` (`published_at`),
  KEY `index_vulnerabilities_on_updated_at` (`updated_at`),
  KEY `index_on_status_and_platform_and_simulation_and_updated_at` (`status`,`platform`,`simulation`,`updated_at`),
  KEY `index_vulnerabilities_on_simulation` (`simulation`),
  KEY `index_vulnerabilities_on_cve_id` (`cve_id`),
  KEY `index_vulnerabilities_on_white_source_id` (`white_source_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `vulnerability_alerting_event_subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vulnerability_alerting_event_subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vulnerability_alerting_event_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `repository_vulnerability_alert_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_user_and_repo_vuln_alert` (`user_id`,`repository_vulnerability_alert_id`),
  KEY `index_vae_user_and_repo_vuln_alert` (`vulnerability_alerting_event_id`,`user_id`,`repository_vulnerability_alert_id`),
  KEY `index_vulnerability_alerting_event_subscriptions_on_rva_id` (`repository_vulnerability_alert_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `vulnerability_alerting_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vulnerability_alerting_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vulnerability_id` int(11) DEFAULT NULL,
  `reason` int(11) NOT NULL DEFAULT '0',
  `actor_id` int(11) DEFAULT NULL,
  `processed_at` datetime DEFAULT NULL,
  `finished_at` datetime DEFAULT NULL,
  `alert_count` int(11) NOT NULL DEFAULT '0',
  `notification_count` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `vulnerability_references`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vulnerability_references` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vulnerability_id` int(11) NOT NULL,
  `url` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_vulnerability_references_on_vulnerability_id` (`vulnerability_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `vulnerable_version_range_alerting_processes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vulnerable_version_range_alerting_processes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vulnerability_alerting_event_id` int(11) NOT NULL,
  `vulnerable_version_range_id` int(11) NOT NULL,
  `processed_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_vae_id_and_vvr_id` (`vulnerability_alerting_event_id`,`vulnerable_version_range_id`),
  KEY `index_vae_id_and_processed_at` (`vulnerability_alerting_event_id`,`processed_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `vulnerable_version_ranges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vulnerable_version_ranges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vulnerability_id` int(11) DEFAULT NULL,
  `affects` varchar(100) NOT NULL,
  `fixed_in` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `requirements` varchar(75) NOT NULL,
  `ecosystem` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_vulnerable_version_ranges_on_affects` (`affects`),
  KEY `index_vulnerable_version_ranges_on_ecosystem` (`ecosystem`),
  KEY `index_vulnerable_version_ranges_on_vulnerability_id` (`vulnerability_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `wal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wal` (
  `id` bigint(2) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `path` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_wal_on_created_at` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `watched_repositories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `watched_repositories` (
  `user_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_watched_repositories_on_user_id_and_repository_id` (`user_id`,`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `web_notifications_transition_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `web_notifications_transition_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `migrated_at` datetime DEFAULT NULL,
  `found` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_web_notifications_transition_log_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `web_push_subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `web_push_subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `endpoint` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `auth` varchar(255) NOT NULL,
  `p256dh` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `user_agent` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_web_push_subscriptions_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `webauthn_user_handles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webauthn_user_handles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `webauthn_user_handle` binary(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_webauthn_user_handles_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `wiki_bases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_bases` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `network_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `incremental_id` bigint(20) NOT NULL,
  `path` varchar(255) NOT NULL,
  `key_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_wiki_bases_on_network_id_and_repository_id` (`network_id`,`repository_id`),
  KEY `index_wiki_bases_on_key_id` (`key_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `wiki_incrementals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_incrementals` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `previous_id` bigint(20) DEFAULT NULL,
  `network_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `checksum` varchar(48) DEFAULT NULL,
  `audit_log_len` bigint(20) DEFAULT NULL,
  `key_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_wiki_incrementals_previous_unique` (`previous_id`),
  KEY `index_wiki_incrementals_on_ids` (`network_id`,`repository_id`),
  KEY `index_wiki_incrementals_on_key_id` (`key_id`),
  KEY `index_wiki_incrementals_on_repository_id_and_checksum` (`repository_id`,`checksum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `wiki_maintenance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wiki_maintenance` (
  `network_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `status` enum('completed','scheduled','running','retry','error') NOT NULL,
  `scheduled_at` datetime DEFAULT NULL,
  `last_maintenance_at` datetime DEFAULT NULL,
  `incrementals` int(11) DEFAULT NULL,
  PRIMARY KEY (`network_id`,`repository_id`),
  KEY `wiki_maintenance_on_last_maintenance_at` (`last_maintenance_at`),
  KEY `wiki_maintenance_on_incrementals` (`incrementals`),
  KEY `index_wiki_maintenance_on_status_and_scheduled_at` (`status`,`scheduled_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `workflow_builds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workflow_builds` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` binary(16) DEFAULT NULL,
  `queued_at` datetime(6) DEFAULT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `started_at` datetime(6) DEFAULT NULL,
  `completed_at` datetime(6) DEFAULT NULL,
  `cloud_build_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'The ID from the build backend for this build',
  `external_build_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT 'The ID from the build backend for this build',
  `repository_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `commit_sha` varchar(40) NOT NULL,
  `workflow_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'Workflow identifier',
  `check_suite_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `request_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `state` int(11) NOT NULL DEFAULT '0' COMMENT 'See WorkflowState for values',
  `workflow_file_path` varchar(255) NOT NULL DEFAULT '.github/main.workflow',
  `reporting_metadata` json DEFAULT NULL COMMENT 'See WorkflowMetadata for values',
  `token_permissions` json DEFAULT NULL COMMENT 'See InstallationPermissions for values',
  `payload_id` bigint(20) unsigned DEFAULT NULL COMMENT 'ID of actions_workflow_payloads.payloads row',
  `workflow_run_id` bigint(20) unsigned DEFAULT '0' COMMENT 'The workflow run database ID',
  `workflow_run_number` bigint(20) unsigned DEFAULT '0' COMMENT 'The workflow run number',
  `delivery_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `installation_id` bigint(20) NOT NULL DEFAULT '0' COMMENT 'Id of the GitHub Actions app installation in the repository',
  `event` varchar(255) NOT NULL DEFAULT '' COMMENT 'The GitHub event that kicked off a build',
  `event_time` datetime(6) DEFAULT NULL,
  `provider` varchar(255) NOT NULL DEFAULT '' COMMENT 'The build provider',
  `checkout_sha` varchar(40) NOT NULL,
  `checkout_ref` varbinary(1024) NOT NULL,
  `event_ref` varbinary(1024) NOT NULL DEFAULT '',
  `rerun` tinyint(1) DEFAULT '0',
  `was_delayed` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `by_check_suite_id` (`check_suite_id`),
  UNIQUE KEY `by_uuid` (`uuid`),
  KEY `by_created_at` (`created_at`),
  KEY `by_repository_id` (`repository_id`),
  KEY `by_provider_queued_at` (`provider`,`queued_at`),
  KEY `by_workflow_id_event_repository_id` (`workflow_id`,`event`,`repository_id`),
  KEY `by_external_build_id` (`external_build_id`),
  KEY `by_cloud_build_id` (`cloud_build_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `workflow_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workflow_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `workflow_build_id` bigint(20) unsigned NOT NULL,
  `external_job_id` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT 'The external AZP Job ID',
  `check_run_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'The GitHub Check Run ID',
  `created_at` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `by_external_job_id` (`external_job_id`),
  KEY `by_workflow_build_id` (`workflow_build_id`),
  KEY `by_check_run_id` (`check_run_id`),
  KEY `by_created_at` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `workflow_runs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workflow_runs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_id` int(11) NOT NULL,
  `check_suite_id` bigint(11) unsigned NOT NULL,
  `run_number` int(11) NOT NULL DEFAULT '0',
  `trigger_id` bigint(20) DEFAULT NULL,
  `trigger_type` varchar(30) DEFAULT NULL,
  `event` varchar(50) DEFAULT NULL,
  `action` varbinary(400) DEFAULT NULL,
  `name` varbinary(1024) DEFAULT NULL,
  `head_branch` varbinary(1024) DEFAULT NULL,
  `head_sha` varchar(64) DEFAULT NULL,
  `workflow_file_path` varbinary(1024) DEFAULT NULL,
  `completed_log_url` varbinary(1024) DEFAULT NULL,
  `external_id` varchar(64) DEFAULT NULL,
  `repository_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_workflow_runs_on_check_suite_id` (`check_suite_id`),
  KEY `index_workflow_runs_on_workflow_and_check_suite_and_run_number` (`workflow_id`,`check_suite_id`,`run_number`),
  KEY `index_workflow_runs_on_repository_id` (`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `workflow_schedules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workflow_schedules` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `schedule_hash` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT 'hash of properties that uniquely identify a schedule, see ScheduleHash',
  `repository_node_id` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `workflow_identifier` varchar(2048) COLLATE utf8mb4_bin NOT NULL COMMENT 'user defined name for a workflow',
  `workflow_file_path` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `environment` varchar(40) COLLATE utf8mb4_bin NOT NULL,
  `schedule` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `scatter_offset` double NOT NULL,
  `next_run_at` datetime(6) NOT NULL COMMENT 'when this should be run next',
  `locked_at` datetime(6) DEFAULT NULL COMMENT 'when a worker picked up the task, used to clear stale locks',
  `locked_by` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'ID unique to worker processing this run',
  `commit_sha` char(40) COLLATE utf8mb4_bin NOT NULL COMMENT 'commit from which schedules were read from workflow file',
  `installation_id` bigint(20) NOT NULL COMMENT 'Id of the GitHub Actions app installation in the repository the workflow is in',
  `actor_node_id` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT 'last person to alter schedules via push or modifying default branch',
  `actor_login` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `by_schedule_hash` (`schedule_hash`) COMMENT 'quickly synchronise schedules as they change',
  KEY `by_repository_node_id` (`repository_node_id`) COMMENT 'synchronisation requires us to find existing state for a repo',
  KEY `by_next_run_at` (`next_run_at`) COMMENT 'we poll to find tasks up next for execution',
  KEY `by_locked_by` (`locked_by`) COMMENT 'we poll to find rows a worker locked',
  KEY `by_locked_at` (`locked_at`) COMMENT 'we poll to identify rows that are assumed abandoned',
  KEY `by_locked_by_environment_next_run_at` (`locked_by`,`environment`,`next_run_at`) COMMENT 'optimises finding rows to work on'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `workflows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workflows` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `name` varbinary(1024) NOT NULL,
  `path` varbinary(1024) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `disabled_at` datetime DEFAULT NULL,
  `enabled_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_workflows_on_repository_id_and_path` (`repository_id`,`path`),
  KEY `index_workflows_on_repository_id_and_state` (`repository_id`,`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `workspace_plans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workspace_plans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `owner_type` varchar(12) NOT NULL,
  `resource_group_id` int(11) DEFAULT NULL,
  `name` varchar(90) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `vscs_target` varchar(21) DEFAULT NULL,
  `resource_provider` varchar(50) DEFAULT 'Microsoft.VSOnline',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_on_resource_group_and_name` (`resource_group_id`,`name`),
  UNIQUE KEY `index_on_owner_rg_rp_and_target` (`owner_id`,`owner_type`,`resource_group_id`,`resource_provider`,`vscs_target`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `workspace_resource_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workspace_resource_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subscription` char(36) NOT NULL,
  `location` varchar(40) NOT NULL,
  `name` varchar(90) NOT NULL,
  `plans_count` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_workspace_resource_groups_on_subscription_and_name` (`subscription`,`name`),
  KEY `index_on_sub_location_and_plans` (`subscription`,`location`,`plans_count`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `workspaces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workspaces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `pull_request_id` int(11) DEFAULT NULL,
  `guid` char(36) DEFAULT NULL,
  `name` varchar(90) NOT NULL,
  `slug` varchar(100) DEFAULT NULL,
  `oid` char(40) NOT NULL,
  `ref` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `last_used_at` datetime DEFAULT NULL,
  `plan_id` int(11) DEFAULT NULL,
  `location` varchar(40) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_workspaces_on_owner_id_and_name` (`owner_id`,`name`),
  UNIQUE KEY `index_workspaces_on_repository_id_and_owner_id_and_name` (`repository_id`,`owner_id`,`name`),
  UNIQUE KEY `index_workspaces_on_name` (`name`),
  UNIQUE KEY `index_workspaces_on_guid_and_owner_id` (`guid`,`owner_id`),
  UNIQUE KEY `index_workspaces_on_slug_and_owner_id` (`slug`,`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `zuora_webhooks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zuora_webhooks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kind` int(11) NOT NULL,
  `account_id` varchar(64) DEFAULT NULL,
  `payload` text NOT NULL,
  `processed_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('pending','processed','ignored','investigating') DEFAULT NULL,
  `investigation_notes` text,
  PRIMARY KEY (`id`),
  KEY `index_zuora_webhooks_on_processed_at` (`processed_at`),
  KEY `index_zuora_webhooks_on_account_id_and_kind` (`account_id`,`kind`),
  KEY `index_zuora_webhooks_on_status_and_created_at` (`status`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;
INSERT INTO `schema_migrations` VALUES ('1'),('10'),('11'),('12'),('13'),('14'),('15'),('16'),('17'),('18'),('19'),('2'),('20'),('20081120012922'),('20081120012923'),('20081121051337'),('20081124230658'),('20081205012100'),('20081208113643'),('20081210172635'),('20081211092208'),('20090102203503'),('20090103013802'),('20090113003557'),('20090126013036'),('20090129001307'),('20090130015237'),('20090205011545'),('20090205030806'),('20090210233821'),('20090226000023'),('20090227230556'),('20090306025144'),('20090316174743'),('20090330125929'),('20090411003151'),('20090411005344'),('20090413223931'),('20090414045146'),('20090414175630'),('20090416163047'),('20090416224037'),('20090512022051'),('20090529004002'),('20090617020912'),('20090708214315'),('20090714080312'),('20090812031617'),('20090826211721'),('20090901180518'),('20090901181816'),('20091014070418'),('20091019152632'),('20091214225218'),('20100108002419'),('20100115224910'),('20100116084606'),('20100118063600'),('20100119061913'),('20100119105906'),('20100122003001'),('20100126213415'),('20100128184312'),('20100212013631'),('20100216065415'),('20100223042954'),('20100329223237'),('20100403221907'),('20100405221501'),('20100406002936'),('20100406025247'),('20100423163359'),('20100423173738'),('20100424120105'),('20100424122126'),('20100424131244'),('20100430011714'),('20100430220654'),('20100505035411'),('20100506143056'),('20100513224302'),('20100530172507'),('20100602085758'),('20100604002623'),('20100605014515'),('20100606021413'),('20100621231558'),('20100622180529'),('20100623002735'),('20100713032430'),('20100723034524'),('20100813190146'),('20100910193000'),('20100912175048'),('20101007200110'),('20101026005313'),('20101029095718'),('20101122231005'),('20101214032857'),('20101221174716'),('20101221192923'),('20101229205516'),('20101230044313'),('20110114063525'),('20110116014146'),('20110117225117'),('20110119004451'),('20110128213315'),('20110131212651'),('20110203092716'),('20110212233709'),('20110214021937'),('20110218203945'),('20110223231051'),('20110225060107'),('20110228225207'),('20110311192443'),('20110316060053'),('20110316111339'),('20110317104149'),('20110323050034'),('20110330013411'),('20110401195509'),('20110401200227'),('20110406011306'),('20110409205102'),('20110414100434'),('20110418200626'),('20110420214253'),('20110506132948'),('20110508130209'),('20110513212959'),('20110524010435'),('20110524195030'),('20110528013121'),('20110601044254'),('20110606164110'),('20110607202437'),('20110609142509'),('20110613192751'),('20110613231146'),('20110614043943'),('20110630050805'),('20110708023814'),('20110708225135'),('20110803054558'),('20110816013828'),('20110816034259'),('20110816174453'),('20110911210622'),('20111009234451'),('20111009234452'),('20111022203454'),('20111025011233'),('20111102051745'),('20111115211108'),('20111121055416'),('20111129175222'),('20111201195338'),('20112031001919'),('20120105183710'),('20120105183729'),('20120106012349'),('20120106012456'),('20120106024422'),('20120106033718'),('20120106035039'),('20120106125534'),('20120106131922'),('20120106134250'),('20120114102252'),('20120118030707'),('20120123235802'),('20120131181628'),('20120203003113'),('20120207164749'),('20120215171317'),('20120221194954'),('20120224225334'),('20120229052722'),('20120305015348'),('20120305015349'),('20120305234923'),('20120306002019'),('20120306101928'),('20120313192749'),('20120315101928'),('20120317101928'),('20120319195112'),('20120319220257'),('20120319224041'),('20120322210942'),('20120322223823'),('20120411001512'),('20120417040633'),('20120417133432'),('20120423032442'),('20120423032624'),('20120424190155'),('20120424204905'),('20120430171351'),('20120507195808'),('20120508005449'),('20120508134453'),('20120509181619'),('20120510041335'),('20120511224714'),('20120517193812'),('20120517205102'),('20120518180433'),('20120525162536'),('20120603142541'),('20120615201933'),('20120615202814'),('20120618144155'),('20120618184213'),('20120619014402'),('20120620191353'),('20120621211617'),('20120622214056'),('20120626000004'),('20120626223812'),('20120628184118'),('20120629181439'),('20120709225446'),('20120716220127'),('20120728052057'),('20120801061752'),('20120808193228'),('20120815225149'),('20120819233330'),('20120820181554'),('20120820222125'),('20120821161300'),('20120831042226'),('20120904170656'),('20120904170728'),('20120906115544'),('20120906173558'),('20120907165109'),('20120910165109'),('20120913233448'),('20120918010746'),('20120918225115'),('20120919211725'),('20120919214611'),('20120926150539'),('20120926150608'),('20121004220345'),('20121015214443'),('20121017204859'),('20121018212202'),('20121019202319'),('20121021030030'),('20121023221751'),('20121024210218'),('20121103192651'),('20121105210906'),('20121105211147'),('20121109071527'),('20121110193224'),('20121110214008'),('20121112075051'),('20121112201611'),('20121112205243'),('20121113045029'),('20121113074548'),('20121113074549'),('20121113100844'),('20121114015612'),('20121114172435'),('20121114181944'),('20121114192041'),('20121114194136'),('20121114200035'),('20121114215817'),('20121114221331'),('20121115215955'),('20121116190441'),('20121117000106'),('20121119175441'),('20121120030114'),('20121124052423'),('20121201002915'),('20121204101628'),('20121207102209'),('20121213001909'),('20121213223722'),('20121217063603'),('20121218164200'),('20121218200108'),('20121221172532'),('20121227164655'),('20130102210725'),('20130107165146'),('20130107223618'),('20130108141920'),('20130108204048'),('20130108224554'),('20130108225148'),('20130109060239'),('20130109102078'),('20130110040521'),('20130110054619'),('20130110163945'),('20130112134950'),('20130117192502'),('20130124144531'),('20130124225133'),('20130129185356'),('20130206142949'),('20130206175817'),('20130206194534'),('20130207070557'),('20130208043447'),('20130214184917'),('20130214233132'),('20130217233132'),('20130217233133'),('20130217233134'),('20130218170539'),('20130219181242'),('20130222232639'),('20130222232708'),('20130223101436'),('20130225152537'),('20130226194043'),('20130227235816'),('20130305021135'),('20130305150352'),('20130305150433'),('20130306055011'),('20130306224921'),('20130308175028'),('20130308224205'),('20130311153151'),('20130311164812'),('20130315193048'),('20130317230723'),('20130320053428'),('20130325194618'),('20130325200214'),('20130325222213'),('20130326041329'),('20130327200717'),('20130329171416'),('20130401122949'),('20130402005618'),('20130403150137'),('20130406103343'),('20130410124351'),('20130410142200'),('20130418199999'),('20130425013038'),('20130428014622'),('20130503005418'),('20130503094731'),('20130507194219'),('20130509014116'),('20130510001431'),('20130512090249'),('20130517132841'),('20130518031223'),('20130520111814'),('20130522125825'),('20130528205147'),('20130530234317'),('20130531194557'),('20130531194558'),('20130602152109'),('20130604021806'),('20130604023700'),('20130606201247'),('20130606202017'),('20130610171649'),('20130613032853'),('20130613193558'),('20130613234958'),('20130614021757'),('20130615001004'),('20130615052054'),('20130615172703'),('20130617055151'),('20130617073603'),('20130617195035'),('20130617211056'),('20130618232911'),('20130624210852'),('20130625232233'),('20130625232252'),('20130627165543'),('20130628202228'),('20130701225817'),('20130701233104'),('20130702001913'),('20130703021402'),('20130708232431'),('20130710204724'),('20130715232749'),('20130716051159'),('20130716200020'),('20130716202409'),('20130717192314'),('20130718194043'),('20130718221718'),('20130724002158'),('20130725183951'),('20130805213411'),('20130805221334'),('20130807185258'),('20130809233037'),('20130812222328'),('20130812224620'),('20130814015549'),('20130815042713'),('20130815171215'),('20130816192019'),('20130819165904'),('20130820060629'),('20130822004115'),('20130822081914'),('20130822235001'),('20130826061541'),('20130827002843'),('20130827203009'),('20130827211249'),('20130902011353'),('20130903061754'),('20130904094215'),('20130904225031'),('20130904225056'),('20130905020207'),('20130905205000'),('20130909163048'),('20130909163439'),('20130909170717'),('20130909172526'),('20130910053252'),('20130910061802'),('20130911215500'),('20130912151354'),('20130912223848'),('20130912232042'),('20130913172323'),('20130913230828'),('20130914001004'),('20130916093900'),('20130916201450'),('20130917002929'),('20130917130000'),('20130918012308'),('20130919004031'),('20130919015354'),('20130919180357'),('20130919180410'),('20130919205137'),('20130923221841'),('20130930153321'),('20130930223159'),('20131001133429'),('20131002180511'),('20131002220229'),('20131003003515'),('20131003153253'),('20131003232651'),('20131004060312'),('20131004165120'),('20131007174743'),('20131008002122'),('20131008151157'),('20131008221032'),('20131009190100'),('20131010163658'),('20131010192952'),('20131011155753'),('20131011162647'),('20131011192701'),('20131011215915'),('20131011233841'),('20131014202421'),('20131015202121'),('20131016002210'),('20131016172319'),('20131016231643'),('20131016232753'),('20131017002410'),('20131017034352'),('20131017180347'),('20131017181403'),('20131018205330'),('20131023001431'),('20131023231344'),('20131025201730'),('20131028184832'),('20131028201843'),('20131028205955'),('20131029031754'),('20131029122603'),('20131030145656'),('20131030214007'),('20131101202410'),('20131104155557'),('20131104232702'),('20131105161819'),('20131105163548'),('20131106220625'),('20131107172334'),('20131107192541'),('20131108215137'),('20131111201110'),('20131111215016'),('20131111220056'),('20131113011755'),('20131114082428'),('20131114184956'),('20131114211033'),('20131114232932'),('20131114233757'),('20131115031519'),('20131115165537'),('20131115213103'),('20131118215144'),('20131118224406'),('20131118232749'),('20131119014740'),('20131119020322'),('20131119021009'),('20131120192933'),('20131121234834'),('20131121235217'),('20131121235504'),('20131126213520'),('20131202163159'),('20131203020439'),('20131204161619'),('20131209093737'),('20131210203818'),('20131211055042'),('20131211173437'),('20131211180047'),('20131211220735'),('20131211223957'),('20131211230853'),('20131212175055'),('20131212185029'),('20131213001938'),('20131213015903'),('20131213143718'),('20131213161407'),('20131213211417'),('20131217213643'),('20131219161600'),('20140102101200'),('20140107041243'),('20140107234853'),('20140109203320'),('20140110185544'),('20140113112837'),('20140113174552'),('20140113175648'),('20140114020141'),('20140114205524'),('20140114210631'),('20140114212804'),('20140115160802'),('20140115171058'),('20140117112350'),('20140117164422'),('20140117170555'),('20140119001248'),('20140120190106'),('20140122161550'),('20140122191011'),('20140123205818'),('20140124003346'),('20140124081022'),('20140125212532'),('20140128174955'),('20140129232354'),('20140130041812'),('20140130165957'),('20140130172731'),('20140131132356'),('20140205211111'),('20140205221451'),('20140206013351'),('20140206211822'),('20140207214955'),('20140209023454'),('20140210205330'),('20140212000144'),('20140212083134'),('20140212083232'),('20140212083314'),('20140212083459'),('20140212101842'),('20140212223308'),('20140213053601'),('20140213053602'),('20140218073735'),('20140218130809'),('20140218201733'),('20140218212442'),('20140218230333'),('20140218235902'),('20140218235903'),('20140219021500'),('20140219145100'),('20140220194941'),('20140220222240'),('20140221142543'),('20140221142643'),('20140222000054'),('20140223203451'),('20140224134117'),('20140226011552'),('20140227152752'),('20140227211337'),('20140228220207'),('20140228223403'),('20140303205220'),('20140304113145'),('20140304134241'),('20140305040746'),('20140306115459'),('20140306115733'),('20140306221241'),('20140307165908'),('20140307170017'),('20140310165635'),('20140311231057'),('20140312015234'),('20140312145632'),('20140314040706'),('20140319141546'),('20140321180558'),('20140324191906'),('20140325141337'),('20140325211148'),('20140326133835'),('20140326163325'),('20140327173205'),('20140327211719'),('20140328121337'),('20140329175620'),('20140331221151'),('20140401022249'),('20140401195730'),('20140402125432'),('20140402170240'),('20140403180701'),('20140404191752'),('20140407142913'),('20140409163627'),('20140410142229'),('20140410160448'),('20140414190952'),('20140415105647'),('20140415150244'),('20140415182923'),('20140415183126'),('20140415200249'),('20140415200558'),('20140415204747'),('20140417041450'),('20140417133056'),('20140417214303'),('20140418020215'),('20140418085614'),('20140422150108'),('20140422153620'),('20140422180214'),('20140422211424'),('20140422214336'),('20140425011714'),('20140425215826'),('20140425224639'),('20140426145956'),('20140427202838'),('20140427203702'),('20140427204207'),('20140427210208'),('20140427210721'),('20140427211248'),('20140427211909'),('20140427215822'),('20140427221623'),('20140427222209'),('20140427223256'),('20140427224213'),('20140427224839'),('20140427225047'),('20140427225442'),('20140428010225'),('20140428073358'),('20140429213651'),('20140429230143'),('20140430164809'),('20140501011157'),('20140501185945'),('20140505211102'),('20140506125409'),('20140506223008'),('20140507201604'),('20140507201810'),('20140514001129'),('20140514204412'),('20140516214406'),('20140520162430'),('20140522173446'),('20140527005506'),('20140527163402'),('20140527185928'),('20140527190719'),('20140528171929'),('20140530225923'),('20140602091747'),('20140602130901'),('20140602210756'),('20140602211552'),('20140603074620'),('20140604172239'),('20140604212317'),('20140605010318'),('20140605174425'),('20140606205906'),('20140608134446'),('20140609122848'),('20140609143453'),('20140609223809'),('20140610153635'),('20140610162405'),('20140610212139'),('20140613143640'),('20140613234950'),('20140615215411'),('20140616034816'),('20140616035253'),('20140617052438'),('20140617220059'),('20140618231559'),('20140620170931'),('20140620171638'),('20140620200909'),('20140624170918'),('20140704163900'),('20140706024539'),('20140707183946'),('20140707221230'),('20140708211632'),('20140708212730'),('20140708224204'),('20140710153525'),('20140711015718'),('20140718004114'),('20140721183145'),('20140722124004'),('20140722184807'),('20140723193850'),('20140724003542'),('20140724220016'),('20140728200252'),('20140728200319'),('20140730184540'),('20140805081411'),('20140807093836'),('20140807210722'),('20140808153703'),('20140808205959'),('20140808212143'),('20140808212144'),('20140810210508'),('20140811120228'),('20140814022831'),('20140814143542'),('20140815135902'),('20140815145304'),('20140816140000'),('20140816152212'),('20140820192749'),('20140821143150'),('20140822091616'),('20140822190053'),('20140824055137'),('20140825074905'),('20140825082409'),('20140825083050'),('20140825190459'),('20140827214940'),('20140827224343'),('20140828100941'),('20140828141338'),('20140828204858'),('20140828204859'),('20140828204860'),('20140828204906'),('20140828213438'),('20140828214017'),('20140829024908'),('20140829205807'),('20140829211812'),('20140901203826'),('20140903220914'),('20140904000112'),('20140905232243'),('20140907183952'),('20140908150739'),('20140909152955'),('20140909211830'),('20140910213843'),('20140910235916'),('20140911003317'),('20140912000000'),('20140912145141'),('20140912172701'),('20140915141207'),('20140915191152'),('20140915204018'),('20140916134957'),('20140917155720'),('20140918061218'),('20140918192325'),('20140925175514'),('20140925212837'),('20140926155920'),('20140926180502'),('20140929163101'),('20140929170632'),('20140930224127'),('20140930232432'),('20141001122639'),('20141002121202'),('20141002202128'),('20141003154627'),('20141006204311'),('20141009174529'),('20141010202302'),('20141011205120'),('20141013135509'),('20141015021647'),('20141023044944'),('20141030175148'),('20141103000000'),('20141106165158'),('20141110000000'),('20141114182905'),('20141114191824'),('20141118145719'),('20141118204511'),('20141119134404'),('20141120224609'),('20141120231723'),('20141125155154'),('20141126010157'),('20141126010833'),('20141126075225'),('20141126101800'),('20141126101801'),('20141201224102'),('20141202234603'),('20141207160816'),('20141209065719'),('20141209100342'),('20141210021338'),('20141216234727'),('20141218004716'),('20141218004808'),('20141222103726'),('20150105034508'),('20150106165351'),('20150108143820'),('20150108214156'),('20150108235713'),('20150113132011'),('20150114235518'),('20150115111033'),('20150115183500'),('20150116210531'),('20150120190205'),('20150121055254'),('20150121183330'),('20150122114800'),('20150123163700'),('20150128091215'),('20150129161513'),('20150129162741'),('20150129183547'),('20150129204709'),('20150130171542'),('20150203204155'),('20150203224008'),('20150206190329'),('20150206190348'),('20150206212905'),('20150212223335'),('20150213192352'),('20150213192732'),('20150217200221'),('20150217223206'),('20150218231036'),('20150220173507'),('20150220212223'),('20150223213542'),('20150224193055'),('20150224214136'),('20150224215023'),('20150225190405'),('20150226181639'),('20150226213554'),('20150227233607'),('20150303044010'),('20150304180251'),('20150305211608'),('20150305212723'),('20150306211212'),('20150309011152'),('20150309011703'),('20150309203024'),('20150311164534'),('20150312014333'),('20150313135000'),('20150318022935'),('20150326170158'),('20150328202146'),('20150331195600'),('20150407141443'),('20150409000821'),('20150410161432'),('20150413231034'),('20150414174900'),('20150414174901'),('20150416223550'),('20150416234044'),('20150424215115'),('20150427172951'),('20150427180047'),('20150427184325'),('20150427213219'),('20150428103900'),('20150428150956'),('20150429203918'),('20150507164010'),('20150507194545'),('20150519200347'),('20150519214247'),('20150519215825'),('20150521235143'),('20150526153007'),('20150608222318'),('20150609210041'),('20150610224412'),('20150617154818'),('20150617163920'),('20150617170602'),('20150624153621'),('20150624154727'),('20150624154757'),('20150624154810'),('20150625195944'),('20150626193223'),('20150626201041'),('20150630200832'),('20150630201523'),('20150702003222'),('20150706193218'),('20150707013501'),('20150707161507'),('20150707193850'),('20150720213135'),('20150720213136'),('20150722145004'),('20150723163231'),('20150723233837'),('20150730170249'),('20150730232305'),('20150730235027'),('20150731204223'),('20150803093746'),('20150803153013'),('20150803233338'),('20150806112418'),('20150807130711'),('20150807215000'),('20150807215006'),('20150810173511'),('20150811200001'),('20150812001423'),('20150812002506'),('20150812133834'),('20150812201404'),('20150813003158'),('20150813204347'),('20150813213347'),('20150813221529'),('20150813233019'),('20150814171134'),('20150820191145'),('20150824150220'),('20150824184944'),('20150824194113'),('20150825194438'),('20150826002158'),('20150827222311'),('20150827222312'),('20150828214748'),('20150831224553'),('20150901054718'),('20150901060010'),('20150901145334'),('20150901185845'),('20150903233206'),('20150908202659'),('20150911050754'),('20150911052453'),('20150911172108'),('20150912151845'),('20150914174409'),('20150915022723'),('20150916144758'),('20150917134636'),('20150921021614'),('20150922192453'),('20150923020254'),('20150923203630'),('20150928114214'),('20150929015547'),('20150929190559'),('20151002223321'),('20151005181322'),('20151006175318'),('20151008210000'),('20151009182108'),('20151012202023'),('20151013210547'),('20151015204512'),('20151016095523'),('20151020183026'),('20151021175855'),('20151022152616'),('20151022152617'),('20151027000000'),('20151027120000'),('20151027173733'),('20151027220626'),('20151028223042'),('20151029172318'),('20151030022305'),('20151102215413'),('20151103030231'),('20151103210911'),('20151105224640'),('20151106205402'),('20151110010108'),('20151110220000'),('20151110220001'),('20151110220046'),('20151111155240'),('20151112060005'),('20151112162038'),('20151116231413'),('20151117171258'),('20151118011754'),('20151119211318'),('20151119220849'),('20151123171258'),('20151123215100'),('20151123220553'),('20151124020758'),('20151124234224'),('20151127145531'),('20151201174213'),('20151201210508'),('20151209211950'),('20151210020304'),('20151214113105'),('20151215004954'),('20151216014637'),('20151216203603'),('20151218193402'),('20151218232243'),('20151229152600'),('20160105175820'),('20160105201458'),('20160105230227'),('20160106054157'),('20160107181213'),('20160108010047'),('20160111175529'),('20160111213807'),('20160112010903'),('20160113133707'),('20160113171238'),('20160113231308'),('20160114184020'),('20160114185358'),('20160119221215'),('20160120040515'),('20160120171219'),('20160121152114'),('20160121221020'),('20160125084815'),('20160125084816'),('20160126005052'),('20160126150419'),('20160127002020'),('20160129230523'),('20160201181114'),('20160202153200'),('20160202174745'),('20160202182621'),('20160202184407'),('20160203014852'),('20160204223142'),('20160204232950'),('20160205032431'),('20160205230321'),('20160206011908'),('20160207165808'),('20160210233002'),('20160211163100'),('20160211191541'),('20160212154744'),('20160215000113'),('20160216230945'),('20160217005905'),('20160218210250'),('20160219233357'),('20160222164711'),('20160222195306'),('20160222203419'),('20160223095003'),('20160223162924'),('20160223194707'),('20160223220412'),('20160223223544'),('20160225012801'),('20160225161152'),('20160225195215'),('20160225195221'),('20160225195222'),('20160225195223'),('20160225195226'),('20160225195227'),('20160226232148'),('20160229181934'),('20160301034527'),('20160301222156'),('20160303050231'),('20160303121845'),('20160303232722'),('20160303234459'),('20160304130200'),('20160304180238'),('20160304180910'),('20160304222735'),('20160304224512'),('20160304224513'),('20160308034154'),('20160308042336'),('20160308213756'),('20160309190138'),('20160310182413'),('20160311134634'),('20160311134636'),('20160311173530'),('20160315023129'),('20160315173530'),('20160315214253'),('20160316181340'),('20160317082559'),('20160317082609'),('20160317225316'),('20160318180123'),('20160322024619'),('20160322200849'),('20160322211508'),('20160322212549'),('20160322232804'),('20160323015847'),('20160323190212'),('20160324145416'),('20160324182946'),('20160324210349'),('20160329164240'),('20160329190350'),('20160329191011'),('20160401014043'),('20160401014044'),('20160401181054'),('20160401190205'),('20160404153533'),('20160404173019'),('20160404174311'),('20160404175142'),('20160404182448'),('20160404213554'),('20160404224503'),('20160405050009'),('20160405144150'),('20160406212107'),('20160407162509'),('20160407173526'),('20160408001546'),('20160408173950'),('20160411192347'),('20160411213123'),('20160413000512'),('20160414082106'),('20160414173223'),('20160414191533'),('20160415192007'),('20160418151813'),('20160418154728'),('20160419173608'),('20160420133704'),('20160420151942'),('20160420173424'),('20160420180758'),('20160425074500'),('20160425181302'),('20160425191627'),('20160427080526'),('20160427110231'),('20160427181435'),('20160427214658'),('20160428143042'),('20160429212610'),('20160502091342'),('20160502125617'),('20160503155211'),('20160503213135'),('20160504211539'),('20160504221306'),('20160504224050'),('20160505201343'),('20160506182827'),('20160509164644'),('20160509183057'),('20160509204214'),('20160510013659'),('20160510021143'),('20160510173654'),('20160510212602'),('20160512173012'),('20160512231330'),('20160513195005'),('20160516182959'),('20160517152822'),('20160519214057'),('20160522155515'),('20160523143303'),('20160523171434'),('20160525164444'),('20160526022342'),('20160528061224'),('20160531092625'),('20160531235929'),('20160603000040'),('20160603100601'),('20160603202314'),('20160606203229'),('20160606205918'),('20160609134459'),('20160609201353'),('20160609214820'),('20160612185418'),('20160614210948'),('20160615144945'),('20160616112128'),('20160616150050'),('20160619181059'),('20160620230642'),('20160623173221'),('20160625000000'),('20160627191240'),('20160628203926'),('20160629202736'),('20160630130707'),('20160630195611'),('20160630214030'),('20160701162959'),('20160701212456'),('20160704065827'),('20160705091852'),('20160705124738'),('20160705162017'),('20160705190408'),('20160706142029'),('20160706152716'),('20160706160738'),('20160706161801'),('20160706202643'),('20160707212831'),('20160708014634'),('20160708162113'),('20160711123932'),('20160711175840'),('20160711192940'),('20160712154128'),('20160712213525'),('20160712223738'),('20160713150017'),('20160714224018'),('20160715222257'),('20160725233950'),('20160726164719'),('20160728182221'),('20160801132722'),('20160802000244'),('20160802200401'),('20160804192147'),('20160808145819'),('20160808234836'),('20160809025316'),('20160811182142'),('20160811210906'),('20160812165553'),('20160813012406'),('20160814150611'),('20160816214936'),('20160817113910'),('20160817135737'),('20160818115622'),('20160818205247'),('20160820115904'),('20160821233107'),('20160822145957'),('20160822195134'),('20160822222616'),('20160823000451'),('20160824234357'),('20160825133954'),('20160825173908'),('20160829175757'),('20160830065508'),('20160830152915'),('20160830180750'),('20160830194252'),('20160831190941'),('20160904131717'),('20160906111848'),('20160906160916'),('20160906184346'),('20160907184208'),('20160907215240'),('20160907215241'),('20160908154607'),('20160908154701'),('20160908154709'),('20160909182020'),('20160913091938'),('20160913091949'),('20160913135552'),('20160913140000'),('20160913141945'),('20160913162837'),('20160914040100'),('20160914070723'),('20160914154937'),('20160914213155'),('20160914215436'),('20160915000000'),('20160915024416'),('20160915084756'),('20160916172336'),('20160916202702'),('20160919152149'),('20160920132456'),('20160920154146'),('20160920184250'),('20160920193928'),('20160922212411'),('20160922212919'),('20160922215239'),('20160922220125'),('20160923152955'),('20160923180427'),('20160923180428'),('20160927202115'),('20160928092600'),('20160928155932'),('20160928192343'),('20160928232815'),('20160929225118'),('20160929233026'),('20160930173003'),('20160930213705'),('20161003225024'),('20161005223120'),('20161006034435'),('20161006144952'),('20161006165830'),('20161010090725'),('20161010214450'),('20161012013941'),('20161013012625'),('20161013201315'),('20161013234633'),('20161015155800'),('20161017084528'),('20161017084529'),('20161017084531'),('20161017084532'),('20161017084534'),('20161017101207'),('20161018021435'),('20161018105835'),('20161018172458'),('20161018225838'),('20161019081728'),('20161019084109'),('20161019124130'),('20161019134000'),('20161019204409'),('20161025192428'),('20161026160843'),('20161027211613'),('20161028184202'),('20161031152604'),('20161031181141'),('20161101180937'),('20161103163153'),('20161104171709'),('20161104194525'),('20161104201354'),('20161107235845'),('20161108024608'),('20161108221247'),('20161109173721'),('20161110211403'),('20161114164148'),('20161114164324'),('20161117043951'),('20161118004317'),('20161119000634'),('20161121141208'),('20161121161416'),('20161123182840'),('20161126065031'),('20161126200319'),('20161128182828'),('20161129184511'),('20161130155312'),('20161202014556'),('20161202191328'),('20161205122847'),('20161206160329'),('20161208095246'),('20161208175333'),('20161208185927'),('20161208205931'),('20161208210255'),('20161209165240'),('20161209223050'),('20161212090731'),('20161212090831'),('20161212090931'),('20161212153041'),('20161212153042'),('20161212222026'),('20161213002019'),('20161213232612'),('20161215044712'),('20161215125201'),('20161215160032'),('20161216045837'),('20161216045838'),('20161219204447'),('20161221024908'),('20161221142825'),('20161229132700'),('20161229220358'),('20170107002545'),('20170110162512'),('20170111191323'),('20170111191333'),('20170111222934'),('20170117105801'),('20170117184721'),('20170119175402'),('20170119195954'),('20170119214236'),('20170125145738'),('20170126045301'),('20170126211524'),('20170127175525'),('20170130230229'),('20170130230230'),('20170202180230'),('20170203024621'),('20170203163419'),('20170203165535'),('20170203224642'),('20170206160034'),('20170206195311'),('20170206221314'),('20170207040327'),('20170207161403'),('20170207201713'),('20170207214105'),('20170208162725'),('20170208191707'),('20170208200104'),('20170208200134'),('20170208200346'),('20170209162320'),('20170209185954'),('20170210124721'),('20170210124722'),('20170210124723'),('20170210124730'),('20170210180955'),('20170212001821'),('20170213012018'),('20170213170109'),('20170213181807'),('20170213212632'),('20170214221028'),('20170215184652'),('20170215202156'),('20170216001743'),('20170216181326'),('20170221205257'),('20170222021036'),('20170222021037'),('20170222195409'),('20170224195546'),('20170227104100'),('20170227193601'),('20170301002340'),('20170301061518'),('20170302040509'),('20170302185928'),('20170303152151'),('20170303152152'),('20170306132942'),('20170307215340'),('20170309152751'),('20170310202300'),('20170311031957'),('20170311045332'),('20170314184052'),('20170314214840'),('20170316012808'),('20170316221551'),('20170318065552'),('20170320192817'),('20170320193351'),('20170321211128'),('20170322073838'),('20170322125021'),('20170322125208'),('20170322125956'),('20170322134938'),('20170322172924'),('20170325232113'),('20170326054151'),('20170327170550'),('20170327182936'),('20170328094133'),('20170328190948'),('20170328190949'),('20170330215348'),('20170331152035'),('20170331172258'),('20170403213447'),('20170404001852'),('20170404190635'),('20170405181903'),('20170406120037'),('20170406143858'),('20170406174934'),('20170406200610'),('20170409164659'),('20170409172032'),('20170409183846'),('20170410191629'),('20170410192235'),('20170410204808'),('20170411155748'),('20170411162239'),('20170411162553'),('20170411185534'),('20170411231104'),('20170413213748'),('20170414020431'),('20170414164729'),('20170414165745'),('20170414172059'),('20170414200143'),('20170418152737'),('20170419170307'),('20170419170826'),('20170419205200'),('20170419213915'),('20170420163641'),('20170421090807'),('20170421150911'),('20170421192013'),('20170424164420'),('20170424190132'),('20170424200144'),('20170424200145'),('20170424210606'),('20170425200003'),('20170426192458'),('20170427153549'),('20170501164209'),('20170501165455'),('20170502223159'),('20170503114709'),('20170503182518'),('20170504194117'),('20170505153241'),('20170505155245'),('20170505172848'),('20170505185350'),('20170509212107'),('20170510224020'),('20170511194617'),('20170512171732'),('20170515205727'),('20170515230347'),('20170516015755'),('20170517172254'),('20170517222425'),('20170517223356'),('20170517223445'),('20170522084446'),('20170522173417'),('20170523133126'),('20170524192754'),('20170524200133'),('20170524204916'),('20170525164658'),('20170525170418'),('20170525174345'),('20170525230607'),('20170526195854'),('20170526204856'),('20170530192813'),('20170531174406'),('20170531192229'),('20170531234628'),('20170602150236'),('20170605212144'),('20170606171720'),('20170606195308'),('20170607010338'),('20170607022303'),('20170608140859'),('20170608144351'),('20170608184232'),('20170613142414'),('20170613194405'),('20170613212440'),('20170614053236'),('20170614183254'),('20170619191434'),('20170620192534'),('20170622001444'),('20170622164824'),('20170622185406'),('20170624015511'),('20170626212027'),('20170628160824'),('20170628213315'),('20170629000813'),('20170704164850'),('20170706135609'),('20170706211916'),('20170707225205'),('20170711114345'),('20170711133903'),('20170711163555'),('20170711170705'),('20170713203941'),('20170714190006'),('20170717142835'),('20170717224728'),('20170718091818'),('20170718184159'),('20170719194428'),('20170719201411'),('20170720152215'),('20170720212206'),('20170721142853'),('20170721185654'),('20170725013726'),('20170725182200'),('20170726182904'),('20170726191325'),('20170728183409'),('20170728200855'),('20170801052950'),('20170801221756'),('20170801233113'),('20170802002633'),('20170802115815'),('20170802133414'),('20170802141819'),('20170802165159'),('20170802182147'),('20170802190502'),('20170803023843'),('20170803164423'),('20170804181425'),('20170807215843'),('20170807221042'),('20170808170204'),('20170809144244'),('20170809202051'),('20170810095116'),('20170810135309'),('20170810141748'),('20170811203555'),('20170811221931'),('20170814160531'),('20170814183409'),('20170815164814'),('20170815183802'),('20170815201412'),('20170816222453'),('20170817153620'),('20170817172032'),('20170817200215'),('20170817203708'),('20170817204053'),('20170818173131'),('20170821153533'),('20170821163531'),('20170821180149'),('20170822162813'),('20170822174957'),('20170823165142'),('20170823174001'),('20170823181228'),('20170823211352'),('20170824005301'),('20170824124317'),('20170824181301'),('20170824230023'),('20170825160855'),('20170825185008'),('20170825185350'),('20170825203352'),('20170828185657'),('20170828192228'),('20170828192751'),('20170830075020'),('20170830172148'),('20170830200923'),('20170831095935'),('20170831102055'),('20170831173804'),('20170831173931'),('20170831174041'),('20170831185615'),('20170901123337'),('20170901144142'),('20170901173145'),('20170904222143'),('20170905170111'),('20170905195926'),('20170906154554'),('20170907102920'),('20170907102935'),('20170908211015'),('20170911191724'),('20170912040652'),('20170912162916'),('20170912165716'),('20170912192354'),('20170913085911'),('20170913151043'),('20170913204015'),('20170914201257'),('20170915204104'),('20170917195533'),('20170920185950'),('20170921184521'),('20170921185714'),('20170922111848'),('20170922130027'),('20170922132041'),('20170924145539'),('20170925141025'),('20170925192903'),('20170926183814'),('20170929154646'),('20170930231414'),('20171001001458'),('20171002082708'),('20171002205208'),('20171002205317'),('20171004180333'),('20171007231713'),('20171010175207'),('20171012183832'),('20171012201228'),('20171013083342'),('20171013160912'),('20171013200200'),('20171016141709'),('20171016172652'),('20171017152032'),('20171018125006'),('20171018152405'),('20171018195848'),('20171019152244'),('20171019203917'),('20171020130631'),('20171020130933'),('20171020183347'),('20171020185148'),('20171023135813'),('20171023170030'),('20171023200911'),('20171024004516'),('20171025225110'),('20171026173114'),('20171026205350'),('20171027153619'),('20171030174143'),('20171031161016'),('20171101091552'),('20171101182309'),('20171101184229'),('20171101200944'),('20171101233011'),('20171102154354'),('20171103133246'),('20171103135902'),('20171103141251'),('20171103143031'),('20171103145819'),('20171103193310'),('20171103205348'),('20171103220500'),('20171106193137'),('20171107041619'),('20171107160646'),('20171107161455'),('20171107191156'),('20171107213959'),('20171108012738'),('20171108152023'),('20171109160517'),('20171109223252'),('20171113135246'),('20171113174633'),('20171113185116'),('20171113211350'),('20171114044100'),('20171114150408'),('20171114183028'),('20171114225044'),('20171114232052'),('20171115135723'),('20171115163700'),('20171115210131'),('20171115214542'),('20171116154721'),('20171117005948'),('20171117135849'),('20171117165843'),('20171121095048'),('20171121151505'),('20171121193525'),('20171121210820'),('20171125142632'),('20171127200945'),('20171127215257'),('20171127221225'),('20171129172912'),('20171129185115'),('20171129202328'),('20171129233335'),('20171130004941'),('20171201143350'),('20171204164320'),('20171205015849'),('20171206190345'),('20171207203341'),('20171207235539'),('20171208133753'),('20171211171154'),('20171212003335'),('20171212190818'),('20171213125419'),('20171213221620'),('20171214124416'),('20171214195000'),('20171217185547'),('20171218191519'),('20171219112142'),('20171221185023'),('20180102190634'),('20180103182342'),('20180103223316'),('20180104202546'),('20180105184801'),('20180108144230'),('20180110201743'),('20180111193141'),('20180117004540'),('20180119185452'),('20180119194429'),('20180119200431'),('20180119203140'),('20180123003713'),('20180125015446'),('20180125222832'),('20180126153927'),('20180130001726'),('20180130012658'),('20180130143955'),('20180130150117'),('20180130150743'),('20180130201842'),('20180131141516'),('20180131162117'),('20180131233142'),('20180201110207'),('20180201180813'),('20180202112243'),('20180202181840'),('20180202204115'),('20180203024747'),('20180206134240'),('20180207162309'),('20180207193622'),('20180208030102'),('20180210013745'),('20180212111244'),('20180212181246'),('20180212202219'),('20180214224349'),('20180215214110'),('20180216083655'),('20180216135323'),('20180216153718'),('20180216212528'),('20180219190621'),('20180220151100'),('20180220233736'),('20180221000807'),('20180221171343'),('20180221200119'),('20180222201550'),('20180226213056'),('20180227145946'),('20180228215506'),('20180301173149'),('20180301222726'),('20180306194145'),('20180307130543'),('20180307162956'),('20180308023922'),('20180308060516'),('20180308233628'),('20180309093714'),('20180309191158'),('20180309210235'),('20180309230912'),('20180312164352'),('20180312183519'),('20180312232535'),('20180313122232'),('20180314212728'),('20180315141302'),('20180315220459'),('20180315220631'),('20180316184935'),('20180316205238'),('20180319141537'),('20180319171830'),('20180319174335'),('20180320170708'),('20180321180112'),('20180322155656'),('20180327152941'),('20180330133752'),('20180330181449'),('20180402182710'),('20180402190009'),('20180402200031'),('20180402202041'),('20180404152816'),('20180404170937'),('20180405002245'),('20180405080219'),('20180405095457'),('20180405101545'),('20180405110110'),('20180406145328'),('20180406203318'),('20180410013313'),('20180410223217'),('20180411231115'),('20180416181308'),('20180418095205'),('20180418151220'),('20180418200947'),('20180419170044'),('20180420094620'),('20180423212137'),('20180424183316'),('20180424200842'),('20180425121101'),('20180425135320'),('20180425135700'),('20180425142837'),('20180425165052'),('20180425221729'),('20180430211031'),('20180430211552'),('20180501173111'),('20180503185812'),('20180503201650'),('20180504140026'),('20180504141534'),('20180504211915'),('20180506152340'),('20180508132908'),('20180509071344'),('20180510201420'),('20180512000036'),('20180514053458'),('20180514063228'),('20180515110509'),('20180515162205'),('20180515170110'),('20180515182203'),('20180515230943'),('20180516222605'),('20180517022716'),('20180518142103'),('20180518145824'),('20180522105709'),('20180523221852'),('20180524220027'),('20180524220241'),('20180525101440'),('20180525115523'),('20180525135550'),('20180529123400'),('20180529125356'),('20180529164856'),('20180531111230'),('20180606142950'),('20180607004816'),('20180608163836'),('20180608223703'),('20180609001222'),('20180611200323'),('20180613200020'),('20180614165911'),('20180614182108'),('20180615204155'),('20180615235123'),('20180618165457'),('20180618201550'),('20180619083942'),('20180619142658'),('20180619161235'),('20180620000707'),('20180620034734'),('20180621171718'),('20180623060831'),('20180627221802'),('20180627233312'),('20180629180536'),('20180702131808'),('20180702222121'),('20180704161710'),('20180705195110'),('20180705224657'),('20180706172720'),('20180706172750'),('20180706173324'),('20180709133900'),('20180709174840'),('20180712204534'),('20180718074844'),('20180718154625'),('20180720163339'),('20180720235739'),('20180724102453'),('20180725021646'),('20180726155936'),('20180726200120'),('20180727154923'),('20180727221621'),('20180730104750'),('20180731095211'),('20180731162434'),('20180731162738'),('20180801111937'),('20180801192209'),('20180801192210'),('20180802073642'),('20180802132633'),('20180802204526'),('20180803114953'),('20180807163001'),('20180808143121'),('20180808175134'),('20180810131841'),('20180810202039'),('20180810221705'),('20180813174312'),('20180814190058'),('20180815004314'),('20180815230034'),('20180816213056'),('20180817145428'),('20180817205720'),('20180820140841'),('20180821172658'),('20180821185148'),('20180822004102'),('20180823203509'),('20180824042833'),('20180824042853'),('20180824042908'),('20180824204125'),('20180824235002'),('20180827220534'),('20180827221942'),('20180827235810'),('20180828155554'),('20180829164908'),('20180831204654'),('20180904174926'),('20180905001913'),('20180906000218'),('20180906140335'),('20180907230201'),('20180910181541'),('20180911140617'),('20180911143938'),('20180911153551'),('20180912202525'),('20180912204218'),('20180912204626'),('20180913192345'),('20180913204540'),('20180914015118'),('20180914171345'),('20180914202230'),('20180915014143'),('20180915015513'),('20180917142446'),('20180917162405'),('20180918032742'),('20180918141815'),('20180918200513'),('20180919000554'),('20180919155817'),('20180919155818'),('20180919155819'),('20180920164147'),('20180924143209'),('20180924192543'),('20180925150421'),('20180927151324'),('20180928144619'),('20181001173122'),('20181001173224'),('20181001183644'),('20181002144902'),('20181002144910'),('20181002144915'),('20181002165350'),('20181002181716'),('20181002185411'),('20181003152800'),('20181004153653'),('20181008210322'),('20181008225354'),('20181008232252'),('20181012094624'),('20181016160558'),('20181016223550'),('20181018093831'),('20181019203659'),('20181022170830'),('20181022213122'),('20181023115806'),('20181023143735'),('20181023234720'),('20181024231447'),('20181025000236'),('20181025173517'),('20181026011521'),('20181029101311'),('20181029195456'),('20181030193025'),('20181031183805'),('20181031205006'),('20181101154847'),('20181101154857'),('20181101193728'),('20181101195942'),('20181102104555'),('20181102104556'),('20181102104557'),('20181102131543'),('20181102160421'),('20181105133108'),('20181107102634'),('20181107224418'),('20181108182147'),('20181113170809'),('20181114190450'),('20181115173542'),('20181116151208'),('20181120170915'),('20181120184635'),('20181121205032'),('20181121205040'),('20181121205044'),('20181123115758'),('20181126211628'),('20181126233702'),('20181127201610'),('20181128230747'),('20181129151144'),('20181129152009'),('20181129164107'),('20181129211438'),('20181130012629'),('20181130110349'),('20181130212104'),('20181203135208'),('20181203202033'),('20181203203528'),('20181204191144'),('20181210180814'),('20181213111411'),('20181214215343'),('20181216141142'),('20181217155300'),('20181218005345'),('20181220230551'),('20190103142546'),('20190103150740'),('20190104185727'),('20190104192452'),('20190104192943'),('20190104193719'),('20190107195734'),('20190109173540'),('20190109174356'),('20190114083647'),('20190114121417'),('20190114160625'),('20190114163635'),('20190114164424'),('20190114184523'),('20190114190509'),('20190115161730'),('20190115163552'),('20190115185227'),('20190115190054'),('20190115220128'),('20190115221633'),('20190116185748'),('20190117151436'),('20190117204322'),('20190118191641'),('20190123161546'),('20190124034126'),('20190124034155'),('20190124200242'),('20190124202820'),('20190125110604'),('20190125115517'),('20190125161703'),('20190125211923'),('20190129203122'),('20190130094452'),('20190131184908'),('20190201185546'),('20190206012825'),('20190207034152'),('20190207144242'),('20190207203036'),('20190209013623'),('20190211192936'),('20190213002645'),('20190213141338'),('20190213155633'),('20190213224954'),('20190214160835'),('20190214205709'),('20190215161604'),('20190215195414'),('20190219093810'),('20190219141736'),('20190219193256'),('20190219201234'),('20190221144131'),('20190221144824'),('20190221175044'),('20190223000122'),('20190225134648'),('20190225213529'),('20190225213658'),('20190225213822'),('20190225232315'),('20190226194623'),('20190227182529'),('20190228083939'),('20190301040904'),('20190305182253'),('20190305192214'),('20190306000405'),('20190306002604'),('20190306204827'),('20190307164638'),('20190307171601'),('20190307175610'),('20190307184043'),('20190307191943'),('20190307192121'),('20190307201055'),('20190307213837'),('20190307214905'),('20190308044042'),('20190308221131'),('20190311103555'),('20190311112110'),('20190311133013'),('20190311175851'),('20190311191746'),('20190312031406'),('20190312085411'),('20190312123435'),('20190312164822'),('20190312171341'),('20190312181631'),('20190312224112'),('20190312230135'),('20190313073717'),('20190313131412'),('20190313200808'),('20190314060907'),('20190314140757'),('20190314181300'),('20190314194633'),('20190314195025'),('20190314203726'),('20190314204723'),('20190315014409'),('20190318162645'),('20190318193137'),('20190318211708'),('20190319070951'),('20190319085431'),('20190319155035'),('20190320153327'),('20190320161338'),('20190320190300'),('20190320205706'),('20190320205722'),('20190321203107'),('20190322185107'),('20190322185108'),('20190325161004'),('20190325231025'),('20190326224441'),('20190327133857'),('20190327205120'),('20190328171038'),('20190328200136'),('20190328213427'),('20190329182117'),('20190329204345'),('20190329204678'),('20190329215155'),('20190401162601'),('20190401174622'),('20190401184735'),('20190401235730'),('20190402004232'),('20190402165945'),('20190402172258'),('20190402213133'),('20190403184633'),('20190403220653'),('20190404190726'),('20190405105823'),('20190405193358'),('20190408095940'),('20190408172257'),('20190408204714'),('20190409085256'),('20190409154313'),('20190409165351'),('20190409191043'),('20190409202901'),('20190409212150'),('20190410172733'),('20190411001142'),('20190411011109'),('20190411145737'),('20190411175350'),('20190411201924'),('20190411204023'),('20190412171958'),('20190415194259'),('20190415204314'),('20190417164809'),('20190417210306'),('20190418175031'),('20190419000356'),('20190419010451'),('20190419163741'),('20190422150145'),('20190422154733'),('20190422160228'),('20190423205637'),('20190423212122'),('20190424012444'),('20190424164918'),('20190425115403'),('20190425184225'),('20190426014858'),('20190426152413'),('20190426195450'),('20190426201229'),('20190426201230'),('20190426203919'),('20190426205657'),('20190429103710'),('20190429193853'),('20190429221328'),('20190501135103'),('20190501135104'),('20190501213224'),('20190503014437'),('20190503152214'),('20190503175949'),('20190503212236'),('20190506111806'),('20190507162549'),('20190507202159'),('20190507211335'),('20190507212231'),('20190507220240'),('20190508055455'),('20190509115900'),('20190509131612'),('20190509195707'),('20190510125732'),('20190510145604'),('20190510163528'),('20190510192844'),('20190510193547'),('20190510201738'),('20190514105951'),('20190517004820'),('20190517191900'),('20190521151027'),('20190522142413'),('20190522193429'),('20190522194414'),('20190522205150'),('20190523232656'),('20190524130641'),('20190528170743'),('20190528210652'),('20190529094610'),('20190529153311'),('20190530040355'),('20190530203305'),('20190531144200'),('20190603165809'),('20190603181444'),('20190604163331'),('20190604174105'),('20190605215114'),('20190606135951'),('20190606153117'),('20190607070319'),('20190607070358'),('20190607070421'),('20190607070441'),('20190610215731'),('20190610220741'),('20190611203754'),('20190613184614'),('20190614161904'),('20190617173508'),('20190618000443'),('20190618090711'),('20190619185957'),('20190620032636'),('20190620090544'),('20190621094845'),('20190624185118'),('20190625144406'),('20190625153835'),('20190625175210'),('20190626165357'),('20190628141808'),('20190628155203'),('20190702211326'),('20190703203316'),('20190716013343'),('20190716181614'),('20190717114031'),('20190717161110'),('20190717162006'),('20190717212730'),('20190718190609'),('20190718215537'),('20190719163156'),('20190721143336'),('20190722204841'),('20190722225004'),('20190723135018'),('20190723211402'),('20190724080331'),('20190724183839'),('20190724201832'),('20190724202000'),('20190724202633'),('20190725164946'),('20190726011937'),('20190726025409'),('20190726145828'),('20190726160230'),('20190726185614'),('20190726191141'),('20190726221607'),('20190729103801'),('20190729113908'),('20190729113918'),('20190729113946'),('20190729172821'),('20190730001457'),('20190730172817'),('20190730191122'),('20190730222812'),('20190730233300'),('20190731193933'),('20190801010042'),('20190801104938'),('20190805153921'),('20190807012601'),('20190807033434'),('20190809225405'),('20190809230645'),('20190813203505'),('20190813210813'),('20190814100542'),('20190814172213'),('20190814214227'),('20190815085421'),('20190815114702'),('20190815153434'),('20190815154446'),('20190816173529'),('20190816174707'),('20190816175233'),('20190816193623'),('20190816193648'),('20190816193656'),('20190816193714'),('20190819121222'),('20190819131935'),('20190820142022'),('20190820170244'),('20190820172521'),('20190821002558'),('20190821141405'),('20190821191805'),('20190822185940'),('20190822191650'),('20190822194706'),('20190822205425'),('20190823141531'),('20190826182645'),('20190826230447'),('20190827000103'),('20190827172355'),('20190828102217'),('20190828153831'),('20190829172529'),('20190829183612'),('20190829225929'),('20190830142446'),('20190903185140'),('20190903211502'),('20190903214305'),('20190904154318'),('20190904191125'),('20190904201848'),('20190905182232'),('20190905224620'),('20190906142228'),('20190906165828'),('20190906165829'),('20190906181235'),('20190909162740'),('20190909174937'),('20190909175419'),('20190909185621'),('20190909194350'),('20190909202554'),('20190909205010'),('20190909214518'),('20190909221516'),('20190910124927'),('20190910144435'),('20190910212617'),('20190911221543'),('20190911221645'),('20190913154959'),('20190916162748'),('20190916174155'),('20190918153735'),('20190918224803'),('20190919021132'),('20190919081618'),('20190919144512'),('20190919151611'),('20190919151842'),('20190919152052'),('20190919152132'),('20190919194508'),('20190919230954'),('20190920173126'),('20190920210151'),('20190922185337'),('20190923061839'),('20190923140800'),('20190923150641'),('20190923182644'),('20190924143826'),('20190924150357'),('20190924150910'),('20190924203039'),('20190924211448'),('20190925225214'),('20190926183534'),('20190926190143'),('20190926203544'),('20190926205203'),('20190927151724'),('20190930161414'),('20190930205543'),('20191001141702'),('20191001143515'),('20191001205616'),('20191002131909'),('20191002191813'),('20191002194729'),('20191003191402'),('20191004210111'),('20191007181009'),('20191007181242'),('20191007211300'),('20191008192040'),('20191008221911'),('20191009125647'),('20191009134033'),('20191009155153'),('20191009180740'),('20191010111409'),('20191010134319'),('20191010154450'),('20191010220915'),('20191011002606'),('20191011170057'),('20191011181019'),('20191011181130'),('20191011181213'),('20191011181241'),('20191011211654'),('20191011225735'),('20191011233419'),('20191011234228'),('20191012002816'),('20191014181656'),('20191015185906'),('20191015185911'),('20191016092057'),('20191016152428'),('20191016155730'),('20191016194928'),('20191016221023'),('20191016235900'),('20191017180052'),('20191017205841'),('20191017224809'),('20191017232759'),('20191021143152'),('20191021162001'),('20191022124901'),('20191022224354'),('20191022225008'),('20191023140033'),('20191023205820'),('20191023215218'),('20191024175818'),('20191024205340'),('20191024211011'),('20191025144448'),('20191025212440'),('20191029022314'),('20191029144852'),('20191029183003'),('20191030003411'),('20191030182059'),('20191101155206'),('20191101210100'),('20191102032008'),('20191104154517'),('20191105140118'),('20191107005926'),('20191107152555'),('20191107195742'),('20191108171845'),('20191108174533'),('20191108174736'),('20191108185012'),('20191113201219'),('20191113201220'),('20191113205349'),('20191115195755'),('20191119014729'),('20191119021032'),('20191121081834'),('20191121110554'),('20191122115716'),('20191122115932'),('20191122204140'),('20191122215109'),('20191125092023'),('20191125234424'),('20191126165220'),('20191126235653'),('20191127170157'),('20191129103849'),('20191202191309'),('20191202193809'),('20191203132159'),('20191203172917'),('20191203190050'),('20191204094734'),('20191205104248'),('20191205181754'),('20191205234451'),('20191206112803'),('20191206220551'),('20191209115822'),('20191209162314'),('20191209190030'),('20191209220234'),('20191209220235'),('20191210092758'),('20191210161538'),('20191210170954'),('20191210185300'),('20191210215333'),('20191211001225'),('20191211020441'),('20191212161128'),('20191212165001'),('20191212165457'),('20191213014136'),('20191213173135'),('20191215175651'),('20191216170150'),('20191217182043'),('20191218103224'),('20191219053401'),('20191219053654'),('20191219053723'),('20191219203647'),('20191219211520'),('20191223172401'),('20191223195326'),('20200103210256'),('20200106163203'),('20200106172241'),('20200106173746'),('20200106182500'),('20200106183738'),('20200106222232'),('20200107195250'),('20200108202413'),('20200109161628'),('20200110173635'),('20200110191146'),('20200110204557'),('20200110223716'),('20200113155209'),('20200114100720'),('20200114135545'),('20200114173855'),('20200115043003'),('20200115150544'),('20200115174129'),('20200115175646'),('20200115205541'),('20200116183724'),('20200116224726'),('20200118031600'),('20200118052940'),('20200120151623'),('20200121170039'),('20200121183804'),('20200121203959'),('20200121213349'),('20200122211459'),('20200123093223'),('20200123093224'),('20200123225115'),('20200124231918'),('20200127192437'),('20200128020905'),('20200128085111'),('20200128143309'),('20200128161247'),('20200128183602'),('20200128184643'),('20200130150612'),('20200130170614'),('20200130204301'),('20200131035503'),('20200203200945'),('20200203211723'),('20200204004528'),('20200204132734'),('20200204143042'),('20200204143328'),('20200204180906'),('20200204190628'),('20200205002613'),('20200205210759'),('20200205230915'),('20200206013901'),('20200206031728'),('20200206083323'),('20200206095438'),('20200206155120'),('20200206210043'),('20200207135412'),('20200207141403'),('20200207195111'),('20200208194923'),('20200210152652'),('20200210170935'),('20200210171736'),('20200210201810'),('20200211114821'),('20200211140408'),('20200211164045'),('20200211170054'),('20200211191204'),('20200212011411'),('20200212200342'),('20200212213027'),('20200212225818'),('20200213134415'),('20200213143213'),('20200213144835'),('20200213223551'),('20200213223552'),('20200214154743'),('20200214155317'),('20200214170853'),('20200214193125'),('20200217021300'),('20200218071915'),('20200218151349'),('20200218180226'),('20200218202108'),('20200219020017'),('20200219130823'),('20200219133233'),('20200219133908'),('20200219221004'),('20200220155126'),('20200221220718'),('20200224135508'),('20200224222727'),('20200225091357'),('20200225231131'),('20200226015545'),('20200226183420'),('20200226223304'),('20200226223451'),('20200226233640'),('20200227195243'),('20200227220621'),('20200228200640'),('20200228201442'),('20200302114119'),('20200303152809'),('20200303173009'),('20200303214402'),('20200304160418'),('20200304173130'),('20200304182425'),('20200305000356'),('20200305174402'),('20200305202303'),('20200309185148'),('20200309195153'),('20200309213901'),('20200309230106'),('20200310010201'),('20200310131803'),('20200310143231'),('20200310221505'),('20200311153324'),('20200312014818'),('20200312155540'),('20200315115956'),('20200316100211'),('20200316105436'),('20200316112519'),('20200316112543'),('20200316112557'),('20200316120416'),('20200316154415'),('20200316202232'),('20200316205340'),('20200318211752'),('20200318213410'),('20200318213420'),('20200318221126'),('20200319005510'),('20200319132135'),('20200319161905'),('20200319171432'),('20200320113019'),('20200320113037'),('20200320194930'),('20200320194932'),('20200324005332'),('20200324191407'),('20200324220539'),('20200326194515'),('20200326231059'),('20200330201647'),('20200330212202'),('20200331180922'),('20200401062428'),('20200401145623'),('20200401145701'),('20200401171316'),('20200402181524'),('20200402223207'),('20200406132047'),('20200406161940'),('20200406172946'),('20200406203951'),('20200406210208'),('20200407002443'),('20200407180620'),('20200408042149'),('20200408133037'),('20200408173900'),('20200409014055'),('20200409192411'),('20200413192855'),('20200414135812'),('20200414182946'),('20200414182958'),('20200415004453'),('20200415140508'),('20200415194921'),('20200415210704'),('20200416181612'),('20200417191720'),('20200420170206'),('20200420170314'),('20200420173335'),('20200420215329'),('20200420223352'),('20200421015954'),('20200421021630'),('20200421045907'),('20200421081327'),('20200421192140'),('20200421204533'),('20200422022959'),('20200422152958'),('20200423203546'),('20200423210755'),('20200427154300'),('20200427160308'),('20200427175022'),('20200428174832'),('20200428210656'),('20200429183453'),('20200429222205'),('20200430094942'),('20200430094943'),('20200430094944'),('20200430185849'),('20200504085005'),('20200504141454'),('20200504174223'),('20200505004730'),('20200505221816'),('20200506172011'),('20200507130353'),('20200507165740'),('20200507191909'),('20200507211455'),('20200508184134'),('20200511104727'),('20200511160409'),('20200511235920'),('20200512100017'),('20200512104000'),('20200512104033'),('20200512104051'),('20200512104150'),('20200512104324'),('20200512104429'),('20200512112354'),('20200513134750'),('20200513161147'),('20200513183627'),('20200513193722'),('20200514064845'),('20200514103859'),('20200514152326'),('20200514184634'),('20200514185038'),('20200515202711'),('20200519103225'),('20200519154713'),('20200520184333'),('20200520190444'),('20200520223114'),('20200521164413'),('20200521223726'),('20200521230120'),('20200526175633'),('20200526225250'),('20200527211358'),('20200528013342'),('20200528212601'),('20200601135845'),('20200601203238'),('20200602154313'),('20200602174129'),('20200604085401'),('20200604135246'),('20200604200037'),('20200609105907'),('20200610180914'),('20200610190017'),('20200612001549'),('20200612003139'),('20200612164520'),('20200612193613'),('20200615202457'),('20200615222328'),('20200616064128'),('20200616135327'),('20200616175016'),('20200616175257'),('20200616210724'),('20200617154858'),('20200617161713'),('20200618202235'),('20200619105319'),('20200619182338'),('20200622125910'),('20200622175820'),('20200622212545'),('20200623091154'),('20200623141616'),('20200623152743'),('20200623190128'),('20200624003957'),('20200624134001'),('20200624193553'),('20200624234640'),('20200628071047'),('20200629134417'),('20200630082444'),('20200630082635'),('20200630082847'),('20200630083009'),('20200630083132'),('20200630150423'),('20200630215816'),('20200630222411'),('20200701161718'),('20200706053055'),('20200706160419'),('20200707075752'),('20200708124515'),('20200710035547'),('20200714230355'),('20200715132426'),('20200715162328'),('20200716172436'),('20200716212050'),('20200716215138'),('20200717130929'),('20200720144100'),('20200720221231'),('20200721022725'),('20200721095835'),('20200721154954'),('20200721160313'),('20200721174037'),('20200721184222'),('20200721211731'),('20200723153643'),('20200724213152'),('20200727200323'),('20200727200344'),('20200727201103'),('20200727202316'),('20200727202934'),('20200730224433'),('21'),('22'),('23'),('24'),('25'),('26'),('27'),('28'),('29'),('3'),('30'),('31'),('32'),('33'),('34'),('35'),('36'),('37'),('38'),('39'),('4'),('40'),('41'),('42'),('43'),('44'),('45'),('46'),('47'),('48'),('49'),('5'),('50'),('51'),('52'),('53'),('54'),('55'),('56'),('57'),('58'),('59'),('6'),('60'),('61'),('62'),('63'),('64'),('65'),('66'),('67'),('68'),('69'),('7'),('70'),('71'),('72'),('73'),('74'),('75'),('76'),('77'),('78'),('79'),('8'),('80'),('81'),('82'),('9');
/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

LOCK TABLES `hookshot_schema_migrations` WRITE;
/*!40000 ALTER TABLE `hookshot_schema_migrations` DISABLE KEYS */;
INSERT INTO `hookshot_schema_migrations` VALUES ('20180322073358'),('20180416201337'),('20180523192742'),('20180523204036'),('20180711191020'),('20180723194423'),('20180724031407'),('20180730175409'),('20180808182112'),('20180814030844'),('20180814042906'),('20180814203344'),('20180821214109'),('20180822191055'),('20180904162233'),('20180904181133'),('20180928185245'),('20190311162852'),('20190628163354'),('20191126211459'),('20200714141126'),('20200714161857');
/*!40000 ALTER TABLE `hookshot_schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

LOCK TABLES `dg_schema_migrations` WRITE;
/*!40000 ALTER TABLE `dg_schema_migrations` DISABLE KEYS */;
INSERT INTO `dg_schema_migrations` VALUES ('20160927021646'),('20160927230301'),('20160929001409'),('20160929064044'),('20160930203351'),('20160930204908'),('20160930205307'),('20161004233010'),('20161007191754'),('20161012005326'),('20161012232222'),('20161014204415'),('20161014214431'),('20161018183333'),('20161018192252'),('20161018194447'),('20161018204558'),('20161018220659'),('20161018225854'),('20161019010842'),('20161019222647'),('20161023231206'),('20161028022454'),('20161028182715'),('20161028210233'),('20161028222745'),('20161102205428'),('20161104234056'),('20161105073648'),('20161107204051'),('20161107225451'),('20161108023924'),('20161115020108'),('20161118174509'),('20161119001746'),('20161119013410'),('20161128222850'),('20161201221854'),('20161209220017'),('20170120003128'),('20170120004447'),('20170124051142'),('20170126012658'),('20170126213335'),('20170131223425'),('20170215030759'),('20170823174504'),('20170823174520'),('20170911185117'),('20170912212445'),('20170913181817'),('20170914000426'),('20170914015823'),('20170925231004'),('20171010225651'),('20171016164606'),('20171020013129'),('20171020171846'),('20171023172119'),('20171024213011'),('20171025162010'),('20171027065938'),('20171027070357'),('20171027070657'),('20171027071038'),('20171027073041'),('20171121002929'),('20171121004231'),('20180123001849'),('20180408164108'),('20180613163528'),('20180615173754'),('20180626140825'),('20180706070201'),('20180719163855'),('20180720212203'),('20180720221059'),('20180803200921'),('20180806211929'),('20180905203908'),('20180924185150'),('20181023194931'),('20181030035221'),('20181121232050'),('20181201050624'),('20190222230541'),('20190319212944'),('20190320162415'),('20190321184534'),('20190326000425'),('20190409183210'),('20190410202853'),('20190411163510'),('20190411163627'),('20190412172909'),('20190415214707'),('20190416175625'),('20190426210849'),('20190426235759'),('20190430142903'),('20190508175310'),('20190508182813'),('20190513171324'),('20190529210616'),('20190729230621'),('20200106211434');
/*!40000 ALTER TABLE `dg_schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

LOCK TABLES `actions_deployer_schema_migrations` WRITE;
/*!40000 ALTER TABLE `actions_deployer_schema_migrations` DISABLE KEYS */;
INSERT INTO `actions_deployer_schema_migrations` VALUES (20200624115437,0);
/*!40000 ALTER TABLE `actions_deployer_schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

LOCK TABLES `actions_credz_schema_migrations` WRITE;
/*!40000 ALTER TABLE `actions_credz_schema_migrations` DISABLE KEYS */;
INSERT INTO `actions_credz_schema_migrations` VALUES (20200508133312,0);
/*!40000 ALTER TABLE `actions_credz_schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

LOCK TABLES `actions_payloads_schema_migrations` WRITE;
/*!40000 ALTER TABLE `actions_payloads_schema_migrations` DISABLE KEYS */;
INSERT INTO `actions_payloads_schema_migrations` VALUES (20200326173158,0);
/*!40000 ALTER TABLE `actions_payloads_schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

LOCK TABLES `ts_migrations` WRITE;
/*!40000 ALTER TABLE `ts_migrations` DISABLE KEYS */;
INSERT INTO `ts_migrations` VALUES (20200730085454,0);
/*!40000 ALTER TABLE `ts_migrations` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

