# frozen_string_literal: true
require_relative 'custom_helpers'

module ERBLint
  module Linters
    class LinkHasHref < Linter
      include LinterRegistry
      include ERBLint::Linters::CustomHelpers

      MESSAGE = "Links should go somewhere, you probably want to use a <button> instead."

      def run(processed_source)
        tags(processed_source).each do |tag|
          next if tag.name != "a"
          next if tag.closing?
          href = possible_attribute_values(tag, "href")
          name = tag.attributes["name"]
          if (!name && href.empty?) || href.include?("#")
            generate_offense(self.class, processed_source, tag)
          end
        end

        is_rule_disabled?(processed_source)
      end
    end
  end
end
