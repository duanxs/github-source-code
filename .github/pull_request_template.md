<!--
Describe your change
-->

<!--
You are still required to have sign-off from someone in a [codeowners](https://github.com/github/engineering-process-docs/blob/master/code-review-team-makeup.md) (`*-reviewers`) team before you deploy your PR to any environment with production data (review-lab/canary/production).

If you're new to deploying `github/github` please read the [deployment docs](https://githubber.com/article/technology/dotcom/deploy/deployment) before attempting to merge anything into master :smile:

:sparkles: **NEW: ROLLOUT ADVISORY BOARD REQUIREMENTS BEFORE DEPLOYMENT** :sparkles:

In addition to the [usual steps](https://githubber.com/article/crafts/engineering/code-review-at-github#how-to-write-a-pr) for creating a PR, include the following sections.
-->

### Risk Assessment
<!--
Think carefully about **how often the changed code is run** and **what the impact would be if that were to happen**. With that in mind, select from one of the following categories:

**No risk**
> Code that is not run in production (e.g. documentation and changes to tests).

**Low risk**
> Code where failures would affect a small proportion of requests/users (e.g. 1%). Failures would affect only a few low-traffic pages (e.g. GitHub Sponsors organization dashboard) or if the change failed we’d be in no worse a situation than we were before the change (e.g. attempting to reduce the number of database queries in a view).

**Medium risk**
> Code where failures would affect a medium proportion of requests/users (e.g. 10%). Failures would affect a significant number of users (e.g. GitHub Actions) but could not bring down the entire site or make it unusable.

**High risk**
> Code where failures would affect most requests/users (e.g. >=50%) or could fail in such a way that the site as a whole is disabled. Failures would affect most users or core functionality (e.g. Pull Requests) or result in problems with configuration of any shared resources (e.g. the database, JS, CSS, Rails boot).

If your change is _not_ high risk, make sure to explain how often the code will be run and if it does not affect production etc.
-->

### Rollout / Rollback Plan
<!--
If your PR is **medium** or **high risk**, describe **how we'll know if something goes wrong with this code in production** and, if possible, **how we'll be able to quickly, safely disable it** in such a case (e.g. Vault variables, science experiments, feature flags, etc.). Document the specific information needed to put the plan into effect e.g if you're using a feature flag, make sure the name of the flag (or a link to its config page) is included.

If you need any help with your risk assessment or rollout / rollback, feel free to browse the [github/rollout-process repo](https://github.com/github/rollout-process) or reach out to [#rollout-advisory-board](https://github.slack.com/archives/CURHZRWGG) on Slack.
-->

### Observability
<!--
_If you have any graphs, dashboards, EXPLAIN statements, etc. please add them in here_
-->
