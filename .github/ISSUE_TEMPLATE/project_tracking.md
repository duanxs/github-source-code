---
name: Project tracking
about: Kickstart a new project with an easy fill-in-the-blank template aimed at getting your feature launched.
title: '[Tracking] '
labels: 'tracking'
assignees: ''

---

## Project TL;DR

<!--
Summarize your project here in a sentence or two, perhaps adding a screenshot or screencast if relevant.
-->

## Project Manifest

### Genre

- [ ] New feature
- [ ] Existing feature improvement
- [ ] Maintenance / Reliability / Technical debt

### Team

- Slack channel: 
- Team @mention handle: 

| Humans     |      |
| :------------- | :------------- |
| Engineering | @username, @username |
| Product | @username |
| Design | @username |
| Engineering manager | @username |

### References

<!--
Link up important content that would help an outside stakeholder become better acquainted with your project.
-->

- Product Opportunity or other spec:
- Project board:
- Docs tracking issue:
- Team post:
- Release issue:

### Try it

<!--
How can folks try your feature out and share feedback? Guide your superfans here.
-->

- Feature flag:
- Feedback issue:

## Due Diligence

- [ ] Open a [Release Plan](https://github.com/github/releases/issues/new/choose)
- [ ] Open a [Safety Review Request](https://github.com/github/safety-council/issues/new?template=feature-review-request.md)
- [ ] Open a [Security Review](https://github.com/github/security-reviews/issues/new)
- [ ] Consult the [Product Engineering Playbook](https://github.com/github/product-engineering/blob/master/playbook.md) for application engineering considerations that may apply to your project

### Risks

<!--
Summarize known project risks, related handling plans, and any links with more context.
-->

### Timeline

<!--
Include a high level timeline for the project. Specific release/rollout dates can be deferred to the release issue.
-->
