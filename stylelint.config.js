module.exports = {
  extends: ['stylelint-config-primer'],
  rules: {
    'primer/borders': true,
    'primer/box-shadow': true,
    'primer/colors': true,
    'primer/spacing': true,
    'primer/typography': true,
    'primer/no-override': [
      true,
      {
        // separating these out gives us better error messages
        bundles: ['utilities', 'core', 'product', 'marketing'],
        ignoreSelectors: [
          // Primer CSS shouldn't be targeting these in the first place
          /^\.octicon[-a-z]*/,
          // same for .is-*...
          '.is-active',
          '.is-dragging',
          '.is-error',
          '.is-failed',
          '.is-failure',
          '.is-queued',
          '.is-pending',
          '.is-uploading',
          // used for responsive overrides
          '.page-responsive'
        ]
      }
    ]
  }
}
