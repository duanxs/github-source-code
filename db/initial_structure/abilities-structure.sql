DROP TABLE IF EXISTS `abilities`;
CREATE TABLE `abilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `actor_id` int(11) NOT NULL,
  `actor_type` varchar(40) NOT NULL,
  `action` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `subject_type` varchar(60) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_abilities_by_identity` (`actor_type`,`actor_id`,`subject_type`,`subject_id`,`priority`,`parent_id`),
  KEY `index_abilities_on_priority` (`priority`),
  KEY `index_abilities_on_created_at` (`created_at`),
  KEY `index_abilities_prioritized` (`actor_id`,`actor_type`,`subject_id`,`subject_type`,`priority`,`action`),
  KEY `index_abilities_on_subject_id_and_subject_type_and_action` (`subject_id`,`subject_type`,`action`),
  KEY `index_abilities_on_parent_id` (`parent_id`),
  KEY `index_abilities_on_subject_id_subject_type_priority` (`subject_id`,`subject_type`,`priority`),
  KEY `index_abilities_on_actor_id_subject_id_action_priority` (`actor_id`,`subject_id`,`actor_type`,`subject_type`,`action`,`priority`),
  KEY `index_abilities_on_si_and_st_and_priority_and_at_and_ai` (`subject_id`,`subject_type`,`priority`,`actor_type`,`actor_id`),
  KEY `index_abilities_on_st_and_at_and_ai_and_action_and_prior_and_si` (`subject_type`,`actor_type`,`actor_id`,`action`,`priority`,`subject_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
