DROP TABLE IF EXISTS `archived_custom_field_entries`;
CREATE TABLE `archived_custom_field_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_type` varchar(40) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varbinary(1024) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `authentication_records`;
CREATE TABLE `authentication_records` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(2) DEFAULT NULL,
  `octolytics_id` varchar(32) DEFAULT NULL,
  `client` varchar(40) DEFAULT NULL,
  `attempt_type` varchar(10) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_authentication_records_on_user_id_and_created_at` (`user_id`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `check_annotations`;
CREATE TABLE `check_annotations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `check_output_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `blob_href` varbinary(1024) NOT NULL,
  `warning_level` varchar(255) DEFAULT NULL,
  `message` text NOT NULL,
  `start_line` int(11) NOT NULL,
  `end_line` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `raw_details` text,
  `title` varchar(255) DEFAULT NULL,
  `check_run_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_check_annotations_on_check_output_id` (`check_output_id`),
  KEY `index_check_annotations_on_check_run_id` (`check_run_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `check_outputs`;
CREATE TABLE `check_outputs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `check_run_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `summary` text,
  `images` text,
  `text` text,
  PRIMARY KEY (`id`),
  KEY `index_check_outputs_on_check_run_id` (`check_run_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `check_runs`;
CREATE TABLE `check_runs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `check_id` int(11) DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `performed_via_app_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `conclusion` int(11) DEFAULT NULL,
  `details_url` text,
  `name` varchar(255) DEFAULT NULL,
  `started_at` datetime DEFAULT NULL,
  `completed_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `external_id` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `summary` text,
  `images` text,
  `text` text,
  PRIMARY KEY (`id`),
  KEY `index_check_runs_on_check_id_and_created_at` (`check_id`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `check_suites`;
CREATE TABLE `check_suites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `push_id` bigint(20) DEFAULT NULL,
  `repository_id` int(11) NOT NULL,
  `github_app_id` int(11) NOT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `pull_request_id` int(11) DEFAULT NULL,
  `branch` varbinary(1024) NOT NULL,
  `sha` varchar(64) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `conclusion` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_check_suites_on_repository_id` (`repository_id`),
  KEY `index_check_suites_on_pull_request_id` (`pull_request_id`),
  KEY `index_check_suites_on_push_id` (`push_id`),
  KEY `index_check_suites_on_branch` (`branch`(50))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `checks`;
CREATE TABLE `checks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `external_url` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `check_suite_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_checks_on_check_suite_id_and_name` (`check_suite_id`,`name`),
  KEY `index_checks_on_check_suite_id` (`check_suite_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `collection_items`;
CREATE TABLE `collection_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `collection_id` int(11) NOT NULL,
  `content_id` int(11) NOT NULL,
  `content_type` varchar(30) NOT NULL,
  `slug` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_collection_items_on_collection_id` (`collection_id`),
  KEY `index_collection_items_on_content_id_and_content_type` (`content_id`,`content_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `collection_urls`;
CREATE TABLE `collection_urls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` text NOT NULL,
  `title` varchar(40) NOT NULL,
  `description` varbinary(1024) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `collection_videos`;
CREATE TABLE `collection_videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` text NOT NULL,
  `title` varchar(40) NOT NULL,
  `description` varbinary(1024) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `collections`;
CREATE TABLE `collections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(40) NOT NULL,
  `description` varbinary(1024) DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `attribution_url` text,
  `display_name` varchar(100) NOT NULL,
  `image_url` text,
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_collections_on_slug` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `custom_field_entries`;
CREATE TABLE `custom_field_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_type` varchar(40) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varbinary(1024) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `field_type` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `custom_field_entries_owner` (`owner_type`,`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `discussion_post_replies`;
CREATE TABLE `discussion_post_replies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `body` mediumblob NOT NULL,
  `formatter` varchar(20) DEFAULT NULL,
  `number` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `discussion_post_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_discussion_post_replies_on_discussion_post_id_and_number` (`discussion_post_id`,`number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `discussion_posts`;
CREATE TABLE `discussion_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` int(11) DEFAULT NULL,
  `body` mediumblob NOT NULL,
  `formatter` varchar(20) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `pinned_at` datetime DEFAULT NULL,
  `pinned_by_user_id` int(11) DEFAULT NULL,
  `private` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `title` varbinary(1024) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_discussion_posts_on_team_id_and_number` (`team_id`,`number`),
  KEY `index_discussion_posts_on_team_id_and_pinned_at` (`team_id`,`pinned_at`),
  KEY `index_discussion_posts_on_team_id_and_private` (`team_id`,`private`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `push_infos`;
CREATE TABLE `push_infos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `endpoint` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_push_infos_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `pushes`;
CREATE TABLE `pushes` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) DEFAULT NULL,
  `pusher_id` int(11) DEFAULT NULL,
  `before` varchar(40) DEFAULT NULL,
  `after` varchar(40) DEFAULT NULL,
  `ref` varbinary(1024) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_pushes_on_pusher_id` (`pusher_id`),
  KEY `index_pushes_on_repository_id_and_pusher_id_and_created_at` (`repository_id`,`pusher_id`,`created_at`),
  KEY `index_pushes_on_repository_id_and_after` (`repository_id`,`after`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `retired_namespaces`;
CREATE TABLE `retired_namespaces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_login` varchar(40) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_retired_namespaces_on_owner_login_and_name` (`owner_login`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `slotted_counters`;
CREATE TABLE `slotted_counters` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `record_type` varchar(30) NOT NULL,
  `record_id` int(11) NOT NULL,
  `slot` int(11) NOT NULL DEFAULT '0',
  `count` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `records_and_slots` (`record_type`,`record_id`,`slot`),
  KEY `index_slotted_counters_on_record_type_and_record_id_and_count` (`record_type`,`record_id`,`count`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `stafftools_roles`;
CREATE TABLE `stafftools_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `statuses`;
CREATE TABLE `statuses` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `state` varchar(20) NOT NULL DEFAULT 'unknown',
  `description` varchar(255) DEFAULT NULL,
  `target_url` blob,
  `sha` char(40) NOT NULL,
  `repository_id` int(11) unsigned NOT NULL,
  `creator_id` int(11) unsigned NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `pull_request_id` int(11) unsigned DEFAULT NULL,
  `context` varchar(255) NOT NULL DEFAULT 'default',
  `oauth_application_id` int(11) DEFAULT NULL,
  `tree_oid` binary(20) DEFAULT NULL,
  `commit_oid` binary(20) DEFAULT NULL,
  `performed_by_integration_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_statuses_on_sha_and_context_and_repository_id` (`sha`,`context`,`repository_id`),
  KEY `index_statuses_on_commit_oid_and_repository_id_and_context` (`commit_oid`,`repository_id`,`context`),
  KEY `index_statuses_on_repository_id_and_created_at_and_context` (`repository_id`,`created_at`,`context`),
  KEY `index_statuses_on_repository_id_and_sha_and_context` (`repository_id`,`sha`,`context`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `user_stafftools_roles`;
CREATE TABLE `user_stafftools_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `stafftools_role_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_user_stafftools_roles_on_user_id` (`user_id`),
  KEY `index_user_stafftools_roles_on_stafftools_role_id` (`stafftools_role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
