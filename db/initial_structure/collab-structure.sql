DROP TABLE IF EXISTS `commit_contributions`;
CREATE TABLE `commit_contributions` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `repository_id` int(11) unsigned DEFAULT NULL,
  `commit_count` int(11) unsigned DEFAULT '0',
  `committed_date` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `commit_contrib_user_repo_date` (`user_id`,`repository_id`,`committed_date`),
  KEY `index_commit_contributions_on_user_id_and_committed_date` (`user_id`,`committed_date`),
  KEY `index_commit_contributions_on_repository_id_and_committed_date` (`repository_id`,`committed_date`),
  KEY `index_commit_contributions_on_repository_and_user_and_date` (`repository_id`,`user_id`,`committed_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
