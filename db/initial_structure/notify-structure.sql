DROP TABLE IF EXISTS `archived_repository_vulnerability_alerts`;
CREATE TABLE `archived_repository_vulnerability_alerts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vulnerability_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `vulnerable_version_range_id` int(11) NOT NULL,
  `show_alert` tinyint(1) NOT NULL DEFAULT '1',
  `vulnerable_manifest_path` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `dismisser_id` int(11) DEFAULT NULL,
  `dismiss_reason` varchar(255) DEFAULT NULL,
  `dismissed_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `pending_vulnerabilities`;
CREATE TABLE `pending_vulnerabilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `external_identifier` varchar(50) DEFAULT NULL,
  `description` blob,
  `status` varchar(12) NOT NULL,
  `platform` varchar(50) DEFAULT NULL,
  `severity` varchar(12) DEFAULT NULL,
  `classification` varchar(255) DEFAULT NULL,
  `external_reference` varchar(255) DEFAULT NULL,
  `created_by_id` int(11) NOT NULL,
  `reviewer_a_id` int(11) DEFAULT NULL,
  `review_a_at` timestamp NULL DEFAULT NULL,
  `reviewer_b_id` int(11) DEFAULT NULL,
  `review_b_at` timestamp NULL DEFAULT NULL,
  `submitted_by_id` int(11) DEFAULT NULL,
  `submitted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `review_notes` mediumblob,
  `vulnerability_id` int(11) DEFAULT NULL,
  `reject_reason` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_pending_vulnerabilities_on_external_identifier` (`external_identifier`),
  KEY `index_pending_vulnerabilities_on_vulnerability_id` (`vulnerability_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `pending_vulnerable_version_ranges`;
CREATE TABLE `pending_vulnerable_version_ranges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pending_vulnerability_id` int(11) DEFAULT NULL,
  `affects` varchar(100) NOT NULL,
  `requirements` varchar(100) NOT NULL,
  `fixed_in` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_pending_vulnerable_version_ranges_on_pending_vuln_id` (`pending_vulnerability_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `repository_vulnerability_alerts`;
CREATE TABLE `repository_vulnerability_alerts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vulnerability_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `vulnerable_version_range_id` int(11) NOT NULL,
  `show_alert` tinyint(1) NOT NULL DEFAULT '1',
  `vulnerable_manifest_path` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `vulnerable_requirements` varchar(64) DEFAULT NULL,
  `last_detected_at` datetime DEFAULT NULL,
  `dismisser_id` int(11) DEFAULT NULL,
  `dismiss_reason` varchar(255) DEFAULT NULL,
  `dismissed_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_vulnerability_alerts_unique` (`vulnerability_id`,`vulnerable_version_range_id`,`vulnerable_manifest_path`,`repository_id`),
  KEY `index_on_vulnerable_version_range_id` (`vulnerable_version_range_id`),
  KEY `index_on_repository_id_and_show_alert` (`repository_id`,`show_alert`),
  KEY `index_on_repository_id_and_last_detected_at` (`repository_id`,`last_detected_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `vulnerabilities`;
CREATE TABLE `vulnerabilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(12) NOT NULL,
  `platform` varchar(50) NOT NULL,
  `severity` varchar(12) NOT NULL,
  `identifier` varchar(50) DEFAULT NULL,
  `classification` varchar(255) DEFAULT NULL,
  `external_reference` varchar(255) DEFAULT NULL,
  `description` blob,
  `created_by_id` int(11) NOT NULL,
  `published_at` datetime DEFAULT NULL,
  `published_by_id` int(11) DEFAULT NULL,
  `withdrawn_at` datetime DEFAULT NULL,
  `withdrawn_by_id` int(11) DEFAULT NULL,
  `simulation` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `vulnerable_version_ranges`;
CREATE TABLE `vulnerable_version_ranges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vulnerability_id` int(11) DEFAULT NULL,
  `affects` varchar(100) NOT NULL,
  `fixed_in` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `requirements` varchar(75) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_vulnerable_version_ranges_on_affects` (`affects`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
