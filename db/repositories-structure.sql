DROP TABLE IF EXISTS `archived_assignments`;
CREATE TABLE `archived_assignments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assignee_id` int(11) NOT NULL,
  `assignee_type` varchar(255) DEFAULT NULL,
  `issue_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_archived_assignments_on_issue_id_and_assignee_id` (`issue_id`,`assignee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `archived_commit_comments`;
CREATE TABLE `archived_commit_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `body` mediumblob,
  `commit_id` varchar(40) NOT NULL,
  `path` varbinary(1024) DEFAULT NULL,
  `repository_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `line` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `formatter` varchar(30) DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `comment_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `comment_hidden_reason` varbinary(1024) DEFAULT NULL,
  `comment_hidden_classifier` varchar(50) DEFAULT NULL,
  `comment_hidden_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_archived_commit_comments_on_repository_id` (`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `archived_deployment_statuses`;
CREATE TABLE `archived_deployment_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(25) NOT NULL DEFAULT 'unknown',
  `description` text,
  `target_url` varbinary(1024) DEFAULT NULL,
  `deployment_id` int(11) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `environment_url` text,
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `environment` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_deployment_statuses_on_deployment_id` (`deployment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `archived_deployments`;
CREATE TABLE `archived_deployments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` blob,
  `payload` mediumblob,
  `sha` varchar(40) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `ref` varchar(255) DEFAULT NULL,
  `environment` varchar(255) DEFAULT 'production',
  `task` varchar(128) DEFAULT 'deploy',
  `transient_environment` tinyint(1) NOT NULL DEFAULT '0',
  `production_environment` tinyint(1) NOT NULL DEFAULT '0',
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `latest_deployment_status_id` int(11) DEFAULT NULL,
  `latest_environment` varchar(255) DEFAULT NULL,
  `latest_status_state` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_deployments_on_repository_id_and_created_at` (`repository_id`,`created_at`),
  KEY `index_deployments_on_sha` (`sha`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `archived_downloads`;
CREATE TABLE `archived_downloads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `content_type` varchar(255) DEFAULT NULL,
  `repository_id` int(11) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `hits` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_downloads_on_repository_id` (`repository_id`),
  KEY `index_downloads_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `archived_issue_comments`;
CREATE TABLE `archived_issue_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `body` mediumblob,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `repository_id` int(11) NOT NULL,
  `formatter` varchar(20) DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `comment_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `comment_hidden_reason` varbinary(1024) DEFAULT NULL,
  `comment_hidden_classifier` varchar(50) DEFAULT NULL,
  `comment_hidden_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_issue_comments_on_repository_id` (`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `archived_issue_event_details`;
CREATE TABLE `archived_issue_event_details` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `issue_event_id` bigint(11) unsigned NOT NULL,
  `label_id` bigint(20) DEFAULT NULL,
  `label_name` varbinary(1024) DEFAULT NULL,
  `label_color` varchar(6) DEFAULT NULL,
  `label_text_color` varchar(6) DEFAULT NULL,
  `milestone_title` varbinary(1024) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `subject_type` varchar(20) DEFAULT NULL,
  `title_was` varbinary(1024) DEFAULT NULL,
  `title_is` varbinary(1024) DEFAULT NULL,
  `deployment_id` int(11) DEFAULT NULL,
  `ref` varchar(255) DEFAULT NULL,
  `before_commit_oid` char(40) DEFAULT NULL,
  `after_commit_oid` char(40) DEFAULT NULL,
  `pull_request_review_state_was` int(11) DEFAULT NULL,
  `message` mediumblob,
  `pull_request_review_id` int(11) DEFAULT NULL,
  `column_name` varbinary(1024) DEFAULT NULL,
  `previous_column_name` varbinary(1024) DEFAULT NULL,
  `card_id` int(11) DEFAULT NULL,
  `review_request_id` int(11) DEFAULT NULL,
  `performed_by_project_workflow_action_id` int(11) DEFAULT NULL,
  `lock_reason` varchar(30) DEFAULT NULL,
  `milestone_id` int(10) unsigned DEFAULT NULL,
  `deployment_status_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `block_duration_days` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_archived_issue_event_details_on_issue_event_id` (`issue_event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `archived_issue_events`;
CREATE TABLE `archived_issue_events` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `issue_id` int(11) unsigned DEFAULT NULL,
  `actor_id` int(11) unsigned DEFAULT NULL,
  `repository_id` int(11) unsigned DEFAULT NULL,
  `event` varchar(40) DEFAULT NULL,
  `commit_id` varchar(40) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `commit_repository_id` int(11) unsigned DEFAULT NULL,
  `referencing_issue_id` int(11) unsigned DEFAULT NULL,
  `raw_data` blob,
  `performed_by_integration_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_issue_events_on_repository_id` (`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `archived_issues`;
CREATE TABLE `archived_issues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `votes` int(11) DEFAULT '0',
  `issue_comments_count` int(11) DEFAULT '0',
  `number` int(11) DEFAULT '0',
  `position` float DEFAULT '1',
  `title` varbinary(1024) DEFAULT NULL,
  `state` varchar(6) DEFAULT NULL,
  `body` mediumblob,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `closed_at` datetime DEFAULT NULL,
  `pull_request_id` int(11) DEFAULT NULL,
  `milestone_id` int(11) unsigned DEFAULT NULL,
  `assignee_id` int(11) unsigned DEFAULT NULL,
  `contributed_at_timestamp` bigint(20) DEFAULT NULL,
  `contributed_at_offset` mediumint(9) DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `created_by_logbook_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_archived_issues_on_repository_id` (`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `archived_labels`;
CREATE TABLE `archived_labels` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varbinary(1024) NOT NULL,
  `color` varchar(10) DEFAULT NULL,
  `repository_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `lowercase_name` varbinary(1024) DEFAULT NULL,
  `description` varbinary(400) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_labels_on_repository_id` (`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `archived_milestones`;
CREATE TABLE `archived_milestones` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` mediumblob,
  `description` mediumblob,
  `due_on` datetime DEFAULT NULL,
  `created_by_id` int(11) unsigned NOT NULL,
  `repository_id` int(11) unsigned NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `state` varchar(30) DEFAULT NULL,
  `number` int(11) unsigned DEFAULT '0',
  `open_issue_count` int(11) unsigned DEFAULT '0',
  `closed_issue_count` int(11) unsigned DEFAULT '0',
  `closed_at` timestamp NULL DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_milestones_on_due_on` (`due_on`),
  KEY `index_milestones_on_created_at` (`created_at`),
  KEY `index_milestones_on_created_by_id` (`created_by_id`),
  KEY `index_milestones_on_state` (`state`),
  KEY `index_milestones_on_number` (`number`),
  KEY `index_milestones_on_open_issue_count` (`open_issue_count`),
  KEY `index_milestones_on_closed_issue_count` (`closed_issue_count`),
  KEY `index_milestones_on_repository_id` (`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `archived_protected_branches`;
CREATE TABLE `archived_protected_branches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `name` varbinary(1024) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `creator_id` int(11) NOT NULL,
  `required_status_checks_enforcement_level` int(11) NOT NULL DEFAULT '0',
  `block_force_pushes_enforcement_level` int(11) NOT NULL DEFAULT '2',
  `block_deletions_enforcement_level` int(11) NOT NULL DEFAULT '2',
  `strict_required_status_checks_policy` tinyint(1) NOT NULL DEFAULT '1',
  `authorized_actors_only` tinyint(1) NOT NULL DEFAULT '0',
  `pull_request_reviews_enforcement_level` int(11) NOT NULL DEFAULT '0',
  `authorized_dismissal_actors_only` tinyint(1) NOT NULL DEFAULT '0',
  `admin_enforced` tinyint(1) NOT NULL DEFAULT '0',
  `dismiss_stale_reviews_on_push` tinyint(1) NOT NULL DEFAULT '0',
  `require_code_owner_review` tinyint(1) NOT NULL DEFAULT '0',
  `signature_requirement_enforcement_level` int(11) NOT NULL DEFAULT '0',
  `required_approving_review_count` tinyint(4) NOT NULL DEFAULT '1',
  `linear_history_requirement_enforcement_level` int(11) NOT NULL DEFAULT '0',
  `allow_force_pushes_enforcement_level` int(11) NOT NULL DEFAULT '0',
  `allow_deletions_enforcement_level` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_archived_protected_branches_on_repository_id_and_name` (`repository_id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `archived_pull_request_review_comments`;
CREATE TABLE `archived_pull_request_review_comments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pull_request_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `repository_id` int(11) NOT NULL,
  `path` varbinary(1024) DEFAULT NULL,
  `commit_id` varchar(40) DEFAULT NULL,
  `diff_hunk` mediumblob,
  `body` mediumblob,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `position` int(11) unsigned DEFAULT NULL,
  `original_commit_id` varchar(40) DEFAULT NULL,
  `original_position` int(11) unsigned DEFAULT NULL,
  `formatter` varchar(20) DEFAULT NULL,
  `original_base_commit_id` varchar(40) DEFAULT NULL,
  `original_start_commit_id` varchar(40) DEFAULT NULL,
  `original_end_commit_id` varchar(40) DEFAULT NULL,
  `blob_position` int(11) unsigned DEFAULT NULL,
  `blob_path` varbinary(1024) DEFAULT NULL,
  `blob_commit_oid` char(40) DEFAULT NULL,
  `left_blob` tinyint(1) NOT NULL DEFAULT '0',
  `state` int(11) NOT NULL DEFAULT '0',
  `pull_request_review_id` int(11) DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `reply_to_id` int(11) DEFAULT NULL,
  `outdated` tinyint(1) NOT NULL DEFAULT '0',
  `comment_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `comment_hidden_reason` varbinary(1024) DEFAULT NULL,
  `comment_hidden_classifier` varchar(50) DEFAULT NULL,
  `comment_hidden_by` int(11) DEFAULT NULL,
  `pull_request_review_thread_id` int(11) DEFAULT NULL,
  `start_position_offset` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_archived_pull_request_review_comments_on_repository_id` (`repository_id`),
  KEY `index_archived_pull_request_review_comments_on_pull_request_id` (`pull_request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `archived_pull_request_review_threads`;
CREATE TABLE `archived_pull_request_review_threads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pull_request_id` int(11) DEFAULT NULL,
  `pull_request_review_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `resolver_id` int(11) DEFAULT NULL,
  `resolved_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_archived_pull_request_review_threads_on_pull_request_id` (`pull_request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `archived_pull_request_reviews`;
CREATE TABLE `archived_pull_request_reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pull_request_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `head_sha` char(40) NOT NULL,
  `body` mediumblob,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `submitted_at` datetime DEFAULT NULL,
  `formatter` varchar(20) DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `comment_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `comment_hidden_reason` varbinary(1024) DEFAULT NULL,
  `comment_hidden_classifier` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_archived_pull_request_reviews_on_pull_request_id` (`pull_request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `archived_pull_request_reviews_review_requests`;
CREATE TABLE `archived_pull_request_reviews_review_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pull_request_review_id` int(11) NOT NULL,
  `review_request_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_archived_pr_review_rev_reqs_on_request_id_and_pr_review_id` (`review_request_id`,`pull_request_review_id`),
  KEY `index_archived_pr_review_rev_reqs_on_pr_review_id_and_request_id` (`pull_request_review_id`,`review_request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `archived_pull_request_revisions`;
CREATE TABLE `archived_pull_request_revisions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pull_request_id` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `ready` tinyint(1) NOT NULL,
  `base_oid` varchar(64) NOT NULL,
  `head_oid` varchar(64) NOT NULL,
  `revised_at` datetime NOT NULL,
  `force_pushed` tinyint(1) NOT NULL,
  `commits_count` int(11) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `push_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_archived_pull_request_revisions_on_pull_request_id` (`pull_request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `archived_pull_requests`;
CREATE TABLE `archived_pull_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `base_sha` char(40) DEFAULT NULL,
  `head_sha` char(40) DEFAULT NULL,
  `repository_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `base_repository_id` int(11) DEFAULT NULL,
  `head_repository_id` int(11) DEFAULT NULL,
  `base_ref` varbinary(1024) DEFAULT NULL,
  `head_ref` varbinary(1024) DEFAULT NULL,
  `merged_at` datetime DEFAULT NULL,
  `base_user_id` int(11) DEFAULT NULL,
  `head_user_id` int(11) DEFAULT NULL,
  `mergeable` tinyint(1) unsigned DEFAULT NULL,
  `merge_commit_sha` char(40) DEFAULT NULL,
  `contributed_at_timestamp` bigint(20) DEFAULT NULL,
  `contributed_at_offset` mediumint(9) DEFAULT NULL,
  `fork_collab_state` int(11) NOT NULL DEFAULT '0',
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `base_sha_on_merge` char(40) DEFAULT NULL,
  `work_in_progress` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_pull_requests_on_repository_id_and_user_id` (`repository_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `archived_releases`;
CREATE TABLE `archived_releases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varbinary(1024) DEFAULT NULL,
  `tag_name` varbinary(1024) NOT NULL,
  `body` mediumblob,
  `author_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `state` int(11) DEFAULT '0',
  `pending_tag` varbinary(1024) DEFAULT NULL,
  `prerelease` tinyint(1) NOT NULL DEFAULT '0',
  `target_commitish` varchar(255) DEFAULT NULL,
  `published_at` datetime DEFAULT NULL,
  `formatter` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `by_published` (`repository_id`,`published_at`),
  KEY `by_state` (`repository_id`,`state`),
  KEY `by_repo_and_tag` (`repository_id`,`tag_name`(50))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `archived_repositories`;
CREATE TABLE `archived_repositories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `owner_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `sandbox` tinyint(1) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `public` tinyint(1) DEFAULT '1',
  `description` mediumblob,
  `homepage` varchar(255) DEFAULT NULL,
  `source_id` int(11) DEFAULT NULL,
  `public_push` tinyint(1) DEFAULT NULL,
  `disk_usage` int(11) DEFAULT '0',
  `locked` tinyint(1) DEFAULT '0',
  `pushed_at` datetime DEFAULT NULL,
  `watcher_count` int(11) DEFAULT '0',
  `public_fork_count` int(11) NOT NULL DEFAULT '1',
  `primary_language_name_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `has_issues` tinyint(1) DEFAULT '1',
  `has_wiki` tinyint(1) DEFAULT '1',
  `has_discussions` tinyint(1) NOT NULL DEFAULT '0',
  `has_downloads` tinyint(1) DEFAULT '1',
  `raw_data` blob,
  `organization_id` int(11) DEFAULT NULL,
  `disabled_at` datetime DEFAULT NULL,
  `disabled_by` int(11) DEFAULT NULL,
  `disabling_reason` varchar(30) DEFAULT NULL,
  `health_status` varchar(30) DEFAULT NULL,
  `pushed_at_usec` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `reflog_sync_enabled` tinyint(1) DEFAULT '0',
  `made_public_at` datetime DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `maintained` tinyint(1) NOT NULL DEFAULT '1',
  `template` tinyint(1) NOT NULL DEFAULT '0',
  `owner_login` varchar(40) DEFAULT NULL,
  `world_writable_wiki` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_archived_repositories_on_updated_at` (`updated_at`),
  KEY `index_archived_repositories_on_owner_id` (`owner_id`),
  KEY `index_archived_repositories_on_owner_login_and_name_and_active` (`owner_login`,`name`,`active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `archived_repository_checksums`;
CREATE TABLE `archived_repository_checksums` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `repository_type` int(11) NOT NULL DEFAULT '0',
  `checksum` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_archived_repository_checksums_on_repository_type` (`repository_id`,`repository_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `archived_repository_replicas`;
CREATE TABLE `archived_repository_replicas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `repository_type` int(11) NOT NULL DEFAULT '0',
  `host` varchar(255) NOT NULL,
  `checksum` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_archived_repository_replicas_on_repository_type_and_host` (`repository_id`,`repository_type`,`host`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `archived_repository_sequences`;
CREATE TABLE `archived_repository_sequences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `number` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_archived_repository_sequences_on_repository_id` (`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `archived_repository_wikis`;
CREATE TABLE `archived_repository_wikis` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `pushed_at` datetime DEFAULT NULL,
  `maintenance_status` varchar(255) DEFAULT NULL,
  `last_maintenance_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `pushed_count` int(11) NOT NULL DEFAULT '0',
  `pushed_count_since_maintenance` int(11) NOT NULL DEFAULT '0',
  `last_maintenance_attempted_at` datetime DEFAULT NULL,
  `cache_version_number` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_wikis_on_repository_id` (`repository_id`),
  KEY `index_repository_wikis_on_maintenance_status` (`maintenance_status`,`pushed_count_since_maintenance`,`last_maintenance_at`),
  KEY `index_archived_repository_wikis_on_pushed_at` (`pushed_at`),
  KEY `index_archived_repository_wikis_on_last_maintenance_at` (`last_maintenance_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `archived_required_status_checks`;
CREATE TABLE `archived_required_status_checks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `protected_branch_id` int(11) NOT NULL,
  `context` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_required_status_checks_on_id_and_context` (`protected_branch_id`,`context`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `archived_review_request_reasons`;
CREATE TABLE `archived_review_request_reasons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `review_request_id` int(11) NOT NULL,
  `codeowners_tree_oid` varchar(40) DEFAULT NULL,
  `codeowners_path` varchar(255) DEFAULT NULL,
  `codeowners_line` int(11) DEFAULT NULL,
  `codeowners_pattern` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_archived_review_request_reasons_on_review_request_id` (`review_request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `archived_review_requests`;
CREATE TABLE `archived_review_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reviewer_id` int(11) NOT NULL,
  `pull_request_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `reviewer_type` varchar(64) NOT NULL,
  `dismissed_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_archived_review_requests_on_pull_request_id` (`pull_request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `assignments`;
CREATE TABLE `assignments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assignee_id` int(11) NOT NULL,
  `assignee_type` varchar(255) DEFAULT NULL,
  `issue_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_assignments_on_issue_id_and_assignee_id` (`issue_id`,`assignee_id`),
  KEY `index_assignments_on_assignee_and_issue_id` (`assignee_id`,`issue_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `commit_comment_edits`;
CREATE TABLE `commit_comment_edits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `commit_comment_id` int(11) NOT NULL,
  `edited_at` datetime NOT NULL,
  `editor_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by_id` int(11) DEFAULT NULL,
  `diff` mediumblob,
  `user_content_edit_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_commit_comment_edits_on_user_content_edit_id` (`user_content_edit_id`),
  KEY `index_commit_comment_edits_on_commit_comment_id` (`commit_comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `commit_comments`;
CREATE TABLE `commit_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `body` mediumblob,
  `commit_id` varchar(40) NOT NULL,
  `path` varbinary(1024) DEFAULT NULL,
  `repository_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `line` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `formatter` varchar(30) DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `comment_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `comment_hidden_reason` varbinary(1024) DEFAULT NULL,
  `comment_hidden_classifier` varchar(50) DEFAULT NULL,
  `comment_hidden_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_commit_comments_on_repository_id_and_user_id` (`repository_id`,`user_id`),
  KEY `index_commit_comments_on_user_hidden_and_user_id` (`user_hidden`,`user_id`),
  KEY `index_commit_comments_on_repo_commit_ids_and_user_hidden` (`repository_id`,`commit_id`,`user_hidden`),
  KEY `index_commit_comments_on_repo_id_and_user_hidden_and_user_id` (`repository_id`,`user_hidden`,`user_id`),
  KEY `index_commit_comments_on_commit_id` (`commit_id`),
  KEY `index_commit_comments_on_user_id_and_created_at` (`user_id`,`created_at`),
  KEY `index_commit_comments_on_user_id_and_updated_at` (`user_id`,`updated_at`),
  KEY `index_commit_comments_on_user_id_and_repository_id` (`user_id`,`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `commit_mentions`;
CREATE TABLE `commit_mentions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) unsigned DEFAULT NULL,
  `commit_id` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_commit_mentions_on_commit_id_and_repository_id` (`commit_id`,`repository_id`),
  KEY `index_commit_mentions_on_repository_id` (`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `community_profiles`;
CREATE TABLE `community_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `has_code_of_conduct` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `repository_id` int(11) NOT NULL,
  `has_contributing` tinyint(1) DEFAULT NULL,
  `has_license` tinyint(1) DEFAULT NULL,
  `has_readme` tinyint(1) DEFAULT NULL,
  `has_outside_contributors` tinyint(1) DEFAULT NULL,
  `detected_code_of_conduct` varchar(255) DEFAULT NULL,
  `help_wanted_issues_count` int(11) NOT NULL DEFAULT '0',
  `good_first_issue_issues_count` int(11) NOT NULL DEFAULT '0',
  `has_docs` tinyint(1) NOT NULL DEFAULT '0',
  `has_description` tinyint(1) NOT NULL DEFAULT '0',
  `has_issue_opened_by_non_collaborator` tinyint(1) NOT NULL DEFAULT '0',
  `has_pr_or_issue_template` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_community_profiles_on_repository_id` (`repository_id`),
  KEY `index_community_profiles_on_has_code_of_conduct` (`has_code_of_conduct`),
  KEY `index_community_profiles_on_detected_code_of_conduct` (`detected_code_of_conduct`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `conversations`;
CREATE TABLE `conversations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `subject_type` varchar(30) NOT NULL,
  `context_id` int(11) NOT NULL,
  `context_type` varchar(30) NOT NULL,
  `state` varchar(30) NOT NULL DEFAULT 'published',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_conversations_on_subject_type_and_subject_id` (`subject_type`,`subject_id`),
  KEY `index_conversations_on_context_id_and_context_type` (`context_id`,`context_type`),
  KEY `index_conversations_on_number` (`number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `cross_references`;
CREATE TABLE `cross_references` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source_id` int(11) NOT NULL,
  `source_type` varchar(255) NOT NULL,
  `target_id` int(11) NOT NULL,
  `target_type` varchar(255) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `referenced_at` datetime DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_discussion_references_natural_key_unique` (`source_id`,`source_type`,`target_id`,`target_type`),
  KEY `index_discussion_references_on_target_id_and_target_type` (`target_id`,`target_type`),
  KEY `index_cross_references_on_user_hidden_and_actor_id` (`user_hidden`,`actor_id`),
  KEY `index_cross_references_on_actor_id` (`actor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `deployment_statuses`;
CREATE TABLE `deployment_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(25) NOT NULL DEFAULT 'unknown',
  `description` text,
  `target_url` varbinary(1024) DEFAULT NULL,
  `deployment_id` int(11) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `environment_url` text,
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `environment` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_deployment_statuses_on_deployment_id` (`deployment_id`),
  KEY `index_deployment_statuses_on_deployment_id_and_environment` (`deployment_id`,`environment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `deployments`;
CREATE TABLE `deployments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` blob,
  `payload` mediumblob,
  `sha` varchar(40) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `ref` varchar(255) DEFAULT NULL,
  `environment` varchar(255) DEFAULT 'production',
  `task` varchar(128) DEFAULT 'deploy',
  `transient_environment` tinyint(1) NOT NULL DEFAULT '0',
  `production_environment` tinyint(1) NOT NULL DEFAULT '0',
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `latest_deployment_status_id` int(11) DEFAULT NULL,
  `latest_environment` varchar(255) DEFAULT NULL,
  `latest_status_state` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_deployments_on_repository_id_and_created_at` (`repository_id`,`created_at`),
  KEY `index_deployments_on_sha` (`sha`),
  KEY `index_deployments_on_repository_environment_prod_and_transient` (`repository_id`,`environment`,`production_environment`,`transient_environment`),
  KEY `index_deployments_on_repository_and_sha` (`repository_id`,`sha`),
  KEY `index_deployments_on_repository_latest_env_created_at` (`repository_id`,`latest_environment`,`created_at`),
  KEY `index_deployments_on_sha_repository_latest_env_created_at` (`repository_id`,`sha`,`latest_environment`,`created_at`),
  KEY `index_deployments_on_repository_latest_status_state` (`repository_id`,`latest_environment`,`latest_status_state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `downloads`;
CREATE TABLE `downloads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `content_type` varchar(255) DEFAULT NULL,
  `repository_id` int(11) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `hits` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_downloads_on_repository_id` (`repository_id`),
  KEY `index_downloads_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `duplicate_issues`;
CREATE TABLE `duplicate_issues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_id` int(11) NOT NULL,
  `canonical_issue_id` int(11) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `duplicate` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_dupe_issues_on_issue_id_canonical_issue_id` (`issue_id`,`canonical_issue_id`),
  KEY `idx_dupe_issues_on_canonical_issue_id_issue_id_duplicate` (`canonical_issue_id`,`issue_id`,`duplicate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `import_item_errors`;
CREATE TABLE `import_item_errors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `import_item_id` int(11) NOT NULL,
  `payload_location` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `resource` varchar(255) DEFAULT NULL,
  `field` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_import_item_errors_on_import_item_id` (`import_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `import_items`;
CREATE TABLE `import_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` varchar(30) NOT NULL,
  `model_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `json_data` mediumblob,
  `model_type` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_import_items_on_repository_id_and_updated_at` (`repository_id`,`updated_at`),
  KEY `index_import_items_on_repository_id_and_status` (`repository_id`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `internal_repositories`;
CREATE TABLE `internal_repositories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) DEFAULT NULL,
  `business_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_internal_repositories_on_repository_id_and_business_id` (`repository_id`,`business_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `issue_blob_references`;
CREATE TABLE `issue_blob_references` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blob_oid` varchar(40) NOT NULL,
  `commit_oid` varchar(40) NOT NULL,
  `issue_id` int(11) NOT NULL,
  `filepath` varchar(255) NOT NULL,
  `range_start` int(11) NOT NULL,
  `range_end` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_blob_references_on_all_columns` (`issue_id`,`blob_oid`,`commit_oid`,`filepath`,`range_start`,`range_end`),
  KEY `index_issue_blob_references_on_issue_id` (`issue_id`),
  KEY `index_issue_blob_references_on_blob_oid` (`blob_oid`),
  KEY `index_issue_blob_references_on_commit_oid` (`commit_oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `issue_comment_edits`;
CREATE TABLE `issue_comment_edits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_comment_id` int(11) NOT NULL,
  `edited_at` datetime NOT NULL,
  `editor_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by_id` int(11) DEFAULT NULL,
  `diff` mediumblob,
  `user_content_edit_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_issue_comment_edits_on_user_content_edit_id` (`user_content_edit_id`),
  KEY `index_issue_comment_edits_on_issue_comment_id` (`issue_comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `issue_comments`;
CREATE TABLE `issue_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `body` mediumblob,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `repository_id` int(11) NOT NULL,
  `formatter` varchar(20) DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `comment_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `comment_hidden_reason` varbinary(1024) DEFAULT NULL,
  `comment_hidden_classifier` varchar(50) DEFAULT NULL,
  `comment_hidden_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_issue_comments_on_repository_id_and_created_at` (`repository_id`,`created_at`),
  KEY `index_issue_comments_on_repository_id_and_updated_at` (`repository_id`,`updated_at`),
  KEY `index_issue_comments_on_repository_id_and_issue_id_and_user_id` (`repository_id`,`issue_id`,`user_id`),
  KEY `index_issue_comments_on_user_hidden_and_user_id` (`user_hidden`,`user_id`),
  KEY `index_issue_comments_on_repo_user_id_user_hidden_and_issue_id` (`repository_id`,`user_id`,`user_hidden`,`issue_id`),
  KEY `index_issue_comments_on_issue_id_and_user_hidden` (`issue_id`,`user_hidden`),
  KEY `index_issue_comments_on_user_id_and_created_at` (`user_id`,`created_at`),
  KEY `index_issue_comments_on_user_id_and_updated_at` (`user_id`,`updated_at`),
  KEY `index_issue_comments_on_user_id_and_repository_id` (`user_id`,`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `issue_edits`;
CREATE TABLE `issue_edits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_id` int(11) NOT NULL,
  `edited_at` datetime NOT NULL,
  `editor_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by_id` int(11) DEFAULT NULL,
  `diff` mediumblob,
  `user_content_edit_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_issue_edits_on_issue_id` (`issue_id`),
  KEY `index_issue_edits_on_user_content_edit_id` (`user_content_edit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `issue_event_details`;
CREATE TABLE `issue_event_details` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `issue_event_id` bigint(11) unsigned NOT NULL,
  `label_id` bigint(20) DEFAULT NULL,
  `label_name` varbinary(1024) DEFAULT NULL,
  `label_color` varchar(6) DEFAULT NULL,
  `label_text_color` varchar(6) DEFAULT NULL,
  `milestone_title` varbinary(1024) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `subject_type` varchar(20) DEFAULT NULL,
  `title_was` varbinary(1024) DEFAULT NULL,
  `title_is` varbinary(1024) DEFAULT NULL,
  `deployment_id` int(11) DEFAULT NULL,
  `ref` varchar(255) DEFAULT NULL,
  `before_commit_oid` char(40) DEFAULT NULL,
  `after_commit_oid` char(40) DEFAULT NULL,
  `pull_request_review_state_was` int(11) DEFAULT NULL,
  `message` mediumblob,
  `pull_request_review_id` int(11) DEFAULT NULL,
  `column_name` varbinary(1024) DEFAULT NULL,
  `previous_column_name` varbinary(1024) DEFAULT NULL,
  `card_id` int(11) DEFAULT NULL,
  `review_request_id` int(11) DEFAULT NULL,
  `performed_by_project_workflow_action_id` int(11) DEFAULT NULL,
  `lock_reason` varchar(30) DEFAULT NULL,
  `milestone_id` int(10) unsigned DEFAULT NULL,
  `deployment_status_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `block_duration_days` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_issue_event_details_on_issue_event_id_and_subject_type` (`issue_event_id`,`subject_type`),
  KEY `index_issue_event_details_on_subject_id_and_subject_type` (`subject_id`,`subject_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `issue_events`;
CREATE TABLE `issue_events` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `issue_id` int(11) unsigned DEFAULT NULL,
  `actor_id` int(11) unsigned DEFAULT NULL,
  `repository_id` int(11) unsigned DEFAULT NULL,
  `event` varchar(40) DEFAULT NULL,
  `commit_id` varchar(40) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `commit_repository_id` int(11) unsigned DEFAULT NULL,
  `referencing_issue_id` int(11) unsigned DEFAULT NULL,
  `raw_data` blob,
  `performed_by_integration_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_issue_events_on_issue_id_and_event_and_commit_id` (`issue_id`,`event`,`commit_id`),
  UNIQUE KEY `index_issue_events_on_issue_id_and_ref_issue_id` (`issue_id`,`event`,`referencing_issue_id`),
  KEY `index_issue_events_on_repository_id_event` (`repository_id`,`event`),
  KEY `index_issue_events_on_repo_id_issue_id_and_event` (`repository_id`,`issue_id`,`event`),
  KEY `index_issue_events_on_issue_id_and_commit_id` (`issue_id`,`commit_id`),
  KEY `index_issue_events_on_actor_id_and_event_and_created_at` (`actor_id`,`event`,`created_at`),
  KEY `index_issue_events_on_repository_id_and_created_at` (`repository_id`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `issue_imports`;
CREATE TABLE `issue_imports` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) unsigned DEFAULT NULL,
  `importer_id` int(11) unsigned DEFAULT NULL,
  `raw_data` blob,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_issue_imports_on_repository_id_and_created_at` (`repository_id`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `issue_links`;
CREATE TABLE `issue_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source_issue_id` int(11) NOT NULL,
  `target_issue_id` int(11) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `link_type` int(11) NOT NULL,
  `source_repository_id` int(11) NOT NULL,
  `target_repository_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_issue_links_source_target_type` (`source_issue_id`,`target_issue_id`,`link_type`),
  KEY `index_issue_links_source_target_type_and_created_at` (`source_issue_id`,`target_issue_id`,`link_type`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `issue_priorities`;
CREATE TABLE `issue_priorities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_id` int(11) NOT NULL,
  `milestone_id` int(10) unsigned NOT NULL,
  `priority` bigint(20) unsigned DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_issue_priorities_on_issue_id_and_milestone_id` (`issue_id`,`milestone_id`),
  UNIQUE KEY `index_issue_priorities_on_milestone_id_and_priority` (`milestone_id`,`priority`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `issues`;
CREATE TABLE `issues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `issue_comments_count` int(11) DEFAULT '0',
  `number` int(11) DEFAULT '0',
  `position` float DEFAULT '1',
  `title` varbinary(1024) DEFAULT NULL,
  `state` varchar(6) DEFAULT NULL,
  `body` mediumblob,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `closed_at` datetime DEFAULT NULL,
  `pull_request_id` int(11) DEFAULT NULL,
  `milestone_id` int(11) unsigned DEFAULT NULL,
  `assignee_id` int(11) unsigned DEFAULT NULL,
  `contributed_at_timestamp` bigint(20) DEFAULT NULL,
  `contributed_at_offset` mediumint(9) DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `created_by_logbook_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_issues_on_created_by_logbook_id` (`created_by_logbook_id`),
  KEY `index_issues_on_pull_request_id` (`pull_request_id`),
  KEY `index_issues_on_milestone_id` (`milestone_id`),
  KEY `index_issues_on_user_id_and_state_and_pull_request_id` (`user_id`,`state`,`pull_request_id`),
  KEY `index_issues_on_repository_id_and_number` (`repository_id`,`number`),
  KEY `index_issues_on_user_id_and_created_at` (`user_id`,`created_at`),
  KEY `index_issues_on_user_id_and_user_hidden` (`user_id`,`user_hidden`),
  KEY `index_issues_on_repository_id_and_user_id` (`repository_id`,`user_id`),
  KEY `index_issues_on_repository_id_and_state_and_user_hidden` (`repository_id`,`state`,`user_hidden`),
  KEY `repository_id_and_state_and_pull_request_id_and_user` (`repository_id`,`state`,`pull_request_id`,`user_hidden`,`user_id`),
  KEY `index_issues_on_repository_id_and_pull_request_id_and_created_at` (`repository_id`,`pull_request_id`,`created_at`),
  KEY `index_issues_on_repository_id_and_pull_request_id_and_closed_at` (`repository_id`,`pull_request_id`,`closed_at`),
  KEY `index_issues_on_user_id_and_repository_id` (`user_id`,`repository_id`),
  KEY `index_issues_on_repository_id_and_user_hidden_and_user_id` (`repository_id`,`user_hidden`,`user_id`),
  KEY `repository_id_and_updated_at_and_state_and_pr_id_and_user` (`repository_id`,`updated_at`,`state`,`pull_request_id`,`user_hidden`,`user_id`),
  KEY `repository_id_and_created_at_and_state_and_pr_id_and_user` (`repository_id`,`created_at`,`state`,`pull_request_id`,`user_hidden`,`user_id`,`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `issues_labels`;
CREATE TABLE `issues_labels` (
  `issue_id` int(11) DEFAULT NULL,
  `label_id` bigint(20) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_issues_labels_on_issue_id_and_label_id` (`issue_id`,`label_id`),
  KEY `index_issues_labels_on_label_id_and_issue_id` (`label_id`,`issue_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `labels`;
CREATE TABLE `labels` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varbinary(1024) NOT NULL,
  `color` varchar(10) DEFAULT NULL,
  `repository_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `lowercase_name` varbinary(1024) DEFAULT NULL,
  `description` varbinary(400) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_labels_on_name` (`name`),
  KEY `index_labels_on_repository_id_and_name` (`repository_id`,`name`),
  KEY `index_labels_on_repository_id_and_lowercase_name` (`repository_id`,`lowercase_name`(15))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) DEFAULT NULL,
  `language_name_id` int(11) DEFAULT NULL,
  `size` bigint(20) DEFAULT NULL,
  `total_size` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `public` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `index_languages_on_language_name_id` (`language_name_id`),
  KEY `index_languages_on_repository_id` (`repository_id`),
  KEY `index_languages_on_size` (`size`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `last_seen_pull_request_revisions`;
CREATE TABLE `last_seen_pull_request_revisions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pull_request_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `last_revision` varbinary(20) NOT NULL,
  `hidden` tinyint(1) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_last_seen_pr_on_pr_id_and_user_id_and_updated_at` (`pull_request_id`,`user_id`,`updated_at`),
  KEY `index_last_seen_pr_on_user_id_and_updated_at` (`user_id`,`updated_at`),
  KEY `index_last_seen_pull_request_revisions_on_created_at` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `marketplace_categories_repository_actions`;
CREATE TABLE `marketplace_categories_repository_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `marketplace_category_id` int(11) NOT NULL,
  `repository_action_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_marketplace_categories_repository_actions_on_ids` (`marketplace_category_id`,`repository_action_id`),
  KEY `index_marketplace_categories_repository_actions_on_action_id` (`repository_action_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `milestones`;
CREATE TABLE `milestones` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` mediumblob,
  `description` mediumblob,
  `due_on` datetime DEFAULT NULL,
  `created_by_id` int(11) unsigned NOT NULL,
  `repository_id` int(11) unsigned NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `state` varchar(30) DEFAULT NULL,
  `number` int(11) unsigned DEFAULT '0',
  `open_issue_count` int(11) unsigned DEFAULT '0',
  `closed_issue_count` int(11) unsigned DEFAULT '0',
  `closed_at` timestamp NULL DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_milestones_on_created_by_id` (`created_by_id`),
  KEY `index_milestones_on_number` (`number`),
  KEY `index_milestones_on_repository_id` (`repository_id`),
  KEY `index_milestones_on_user_hidden_and_created_by_id` (`user_hidden`,`created_by_id`),
  KEY `index_milestones_on_repository_id_and_updated_at` (`repository_id`,`updated_at`),
  KEY `index_milestones_on_repository_id_and_state_and_updated_at` (`repository_id`,`state`,`updated_at`),
  KEY `index_milestones_on_repository_id_and_due_on` (`repository_id`,`due_on`),
  KEY `index_milestones_on_repository_id_and_state_and_due_on` (`repository_id`,`state`,`due_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `mirrors`;
CREATE TABLE `mirrors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL,
  `repository_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_mirrors_on_repository_id` (`repository_id`),
  KEY `index_mirrors_on_updated_at` (`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `network_privileges`;
CREATE TABLE `network_privileges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `noindex` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `require_login` tinyint(1) NOT NULL DEFAULT '0',
  `collaborators_only` tinyint(1) NOT NULL DEFAULT '0',
  `hide_from_discovery` tinyint(1) NOT NULL DEFAULT '0',
  `require_opt_in` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_network_privileges_on_repository_id` (`repository_id`),
  UNIQUE KEY `index_repository_and_hide_from_discovery` (`repository_id`,`hide_from_discovery`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `package_activities`;
CREATE TABLE `package_activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `activity_started_at` datetime NOT NULL,
  `bandwidth_down` float NOT NULL DEFAULT '0',
  `bandwidth_up` float NOT NULL DEFAULT '0',
  `source_files` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_package_activities_on_package_id_and_activity_started_at` (`package_id`,`activity_started_at`),
  KEY `index_package_activities_on_owner_id_and_activity_started_at` (`owner_id`,`activity_started_at`),
  KEY `index_package_activities_on_repo_id_and_activity_started_at` (`repository_id`,`activity_started_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `package_download_activities`;
CREATE TABLE `package_download_activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` int(11) NOT NULL,
  `package_version_id` int(11) NOT NULL,
  `package_download_count` int(11) DEFAULT '0',
  `started_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_package_download_activities_on_pkg_version_and_started_at` (`package_version_id`,`started_at`),
  KEY `index_package_download_activities_on_started_at` (`started_at`),
  KEY `index_package_download_activities_on_pkg_id_and_started_at` (`package_id`,`started_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `package_files`;
CREATE TABLE `package_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_version_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL DEFAULT '',
  `sha1` varchar(40) DEFAULT NULL,
  `md5` varchar(32) DEFAULT NULL,
  `storage_blob_id` int(11) DEFAULT NULL,
  `uploader_id` int(11) DEFAULT NULL,
  `guid` varchar(36) DEFAULT NULL,
  `oid` varchar(64) DEFAULT NULL,
  `size` int(11) NOT NULL,
  `state` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `sha256` varchar(64) DEFAULT NULL,
  `sri_512` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_package_files_on_package_version_id_and_filename` (`package_version_id`,`filename`),
  UNIQUE KEY `index_package_files_on_guid` (`guid`),
  KEY `index_package_files_on_sha256` (`sha256`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `package_version_package_files`;
CREATE TABLE `package_version_package_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_version_id` int(11) NOT NULL,
  `package_file_id` int(11) NOT NULL,
  `package_file_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_package_version_package_files_on_version_id_and_file_id` (`package_version_id`,`package_file_id`),
  KEY `index_package_version_package_files_on_file_id` (`package_file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `package_versions`;
CREATE TABLE `package_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `registry_package_id` int(11) NOT NULL,
  `release_id` int(11) DEFAULT NULL,
  `platform` varchar(255) NOT NULL DEFAULT '',
  `version` varchar(255) NOT NULL,
  `commit_oid` varchar(40) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `sha256` varchar(64) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `manifest` mediumblob,
  `author_id` int(11) DEFAULT NULL,
  `pre_release` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `published_via_actions` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_by_id` int(11) DEFAULT NULL,
  `files_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_registry_package_versions_on_package_id_and_version` (`registry_package_id`,`version`,`platform`),
  KEY `index_package_versions_on_release_id` (`release_id`),
  KEY `index_package_versions_on_author_id` (`author_id`),
  KEY `index_package_versions_on_package_pre_release_and_version` (`registry_package_id`,`pre_release`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `page_builds`;
CREATE TABLE `page_builds` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(11) unsigned DEFAULT NULL,
  `page_deployment_id` int(11) DEFAULT NULL,
  `pages_deployment_id` int(11) DEFAULT NULL,
  `raw_data` blob,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `pusher_id` int(11) DEFAULT NULL,
  `commit` char(40) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `error` text,
  `backtrace` text,
  `duration` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_page_builds_on_updated_at` (`updated_at`),
  KEY `index_page_builds_on_page_id_and_updated_at` (`page_id`,`updated_at`),
  KEY `index_page_builds_on_pages_deployment_id_and_updated_at` (`pages_deployment_id`,`updated_at`),
  KEY `index_page_builds_on_page_deployment_id_and_updated_at` (`page_deployment_id`,`updated_at`),
  KEY `index_page_builds_on_page_id_and_commit` (`page_id`,`commit`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `page_certificates`;
CREATE TABLE `page_certificates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(255) NOT NULL,
  `state` int(11) DEFAULT NULL,
  `state_detail` text,
  `expires_at` datetime DEFAULT NULL,
  `challenge_path` text,
  `challenge_response` text,
  `earthsmoke_key_version_id` bigint(20) unsigned DEFAULT NULL,
  `fastly_privkey_id` text,
  `authorization_url` text,
  `certificate_url` text,
  `fastly_certificate_id` varchar(255) DEFAULT NULL,
  `order_url` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_page_certificates_on_domain` (`domain`),
  KEY `index_page_certificates_on_state_and_expires_at` (`state`,`expires_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `page_deployments`;
CREATE TABLE `page_deployments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `ref_name` varbinary(1024) NOT NULL,
  `revision` varchar(40) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `token` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_page_deployments_on_page_id_and_ref_name` (`page_id`,`ref_name`(767)),
  UNIQUE KEY `index_page_deployments_on_page_id_and_token` (`page_id`,`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) DEFAULT NULL,
  `cname` varchar(255) DEFAULT NULL,
  `four_oh_four` tinyint(1) DEFAULT '0',
  `status` varchar(20) DEFAULT NULL,
  `has_public_search` tinyint(1) DEFAULT '0',
  `built_revision` varchar(40) DEFAULT NULL,
  `https_redirect` tinyint(1) NOT NULL DEFAULT '0',
  `hsts_max_age` int(11) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  `source_ref_name` varbinary(1024) DEFAULT NULL,
  `source_subdir` varbinary(1024) DEFAULT NULL,
  `hsts_include_sub_domains` tinyint(1) NOT NULL DEFAULT '0',
  `hsts_preload` tinyint(1) NOT NULL DEFAULT '0',
  `public` tinyint(1) NOT NULL DEFAULT '1',
  `subdomain` varchar(130) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_pages_on_repository_id` (`repository_id`),
  UNIQUE KEY `index_pages_on_unique_cname` (`cname`),
  UNIQUE KEY `index_pages_on_subdomain` (`subdomain`),
  KEY `index_pages_on_source_ref_name` (`source_ref_name`(30)),
  KEY `index_subdomain_on_repository_id` (`subdomain`,`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `pages_fileservers`;
CREATE TABLE `pages_fileservers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `host` varchar(255) NOT NULL,
  `online` tinyint(1) NOT NULL,
  `embargoed` tinyint(1) NOT NULL,
  `evacuating` tinyint(1) NOT NULL DEFAULT '0',
  `disk_free` bigint(20) unsigned NOT NULL DEFAULT '0',
  `disk_used` bigint(20) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `non_voting` tinyint(1) NOT NULL DEFAULT '0',
  `datacenter` varchar(20) DEFAULT NULL,
  `rack` varchar(20) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `inodes_free` bigint(20) unsigned NOT NULL DEFAULT '0',
  `inodes_used` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_pages_fileservers_on_host` (`host`),
  KEY `index_pages_fileservers_by_location` (`datacenter`,`rack`),
  KEY `index_pages_fileservers_on_evacuating` (`evacuating`),
  KEY `index_pages_fileservers_on_online_and_embargoed_and_host` (`online`,`embargoed`,`host`),
  KEY `index_pages_fileservers_on_voting_and_online_and_embarg_and_host` (`non_voting`,`online`,`embargoed`,`host`),
  KEY `index_pages_fileservers_on_voting_and_online_and_embarg_and_df` (`non_voting`,`online`,`embargoed`,`disk_free`),
  KEY `index_pages_fileservers_on_voting_online_embargo_df_inodes` (`non_voting`,`online`,`embargoed`,`disk_free`,`inodes_free`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `pages_partitions`;
CREATE TABLE `pages_partitions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `host` varchar(255) NOT NULL,
  `partition` varchar(1) NOT NULL,
  `disk_free` bigint(20) NOT NULL,
  `disk_used` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_pages_partitions_on_host_and_partition` (`host`,`partition`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `pages_replicas`;
CREATE TABLE `pages_replicas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `host` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `pages_deployment_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_pages_replicas_on_host` (`host`),
  KEY `index_pages_replicas_on_pages_deployment_id_and_host` (`pages_deployment_id`,`host`),
  KEY `index_pages_replicas_on_page_id_and_host` (`page_id`,`host`),
  KEY `index_pages_replicas_on_page_id_and_pages_deployment_id_and_host` (`page_id`,`pages_deployment_id`,`host`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `pages_routes`;
CREATE TABLE `pages_routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `host` varchar(255) NOT NULL,
  `https_behavior` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_pages_routes_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `protected_branches`;
CREATE TABLE `protected_branches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `name` varbinary(1024) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `creator_id` int(11) NOT NULL,
  `required_status_checks_enforcement_level` int(11) NOT NULL DEFAULT '0',
  `block_force_pushes_enforcement_level` int(11) NOT NULL DEFAULT '2',
  `block_deletions_enforcement_level` int(11) NOT NULL DEFAULT '2',
  `strict_required_status_checks_policy` tinyint(1) NOT NULL DEFAULT '1',
  `authorized_actors_only` tinyint(1) NOT NULL DEFAULT '0',
  `pull_request_reviews_enforcement_level` int(11) NOT NULL DEFAULT '0',
  `authorized_dismissal_actors_only` tinyint(1) NOT NULL DEFAULT '0',
  `admin_enforced` tinyint(1) NOT NULL DEFAULT '0',
  `dismiss_stale_reviews_on_push` tinyint(1) NOT NULL DEFAULT '0',
  `require_code_owner_review` tinyint(1) NOT NULL DEFAULT '0',
  `signature_requirement_enforcement_level` int(11) NOT NULL DEFAULT '0',
  `required_approving_review_count` tinyint(4) NOT NULL DEFAULT '1',
  `linear_history_requirement_enforcement_level` int(11) NOT NULL DEFAULT '0',
  `allow_force_pushes_enforcement_level` int(11) NOT NULL DEFAULT '0',
  `allow_deletions_enforcement_level` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_protected_branches_on_repository_id_and_name` (`repository_id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `pull_request_conflicts`;
CREATE TABLE `pull_request_conflicts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pull_request_id` int(11) NOT NULL,
  `base_sha` varchar(255) NOT NULL,
  `head_sha` varchar(255) NOT NULL,
  `info` blob,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_pull_request_conflicts_on_pr_id` (`pull_request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `pull_request_review_comment_edits`;
CREATE TABLE `pull_request_review_comment_edits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pull_request_review_comment_id` int(11) NOT NULL,
  `edited_at` datetime NOT NULL,
  `editor_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by_id` int(11) DEFAULT NULL,
  `diff` mediumblob,
  `user_content_edit_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_pull_request_review_comment_edits_on_user_content_edit_id` (`user_content_edit_id`),
  KEY `index_on_pull_request_review_comment_id` (`pull_request_review_comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `pull_request_review_comments`;
CREATE TABLE `pull_request_review_comments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pull_request_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `path` varbinary(1024) DEFAULT NULL,
  `commit_id` varchar(40) DEFAULT NULL,
  `diff_hunk` mediumblob,
  `body` mediumblob,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `position` int(11) unsigned DEFAULT NULL,
  `original_commit_id` varchar(40) DEFAULT NULL,
  `original_position` int(11) unsigned DEFAULT NULL,
  `formatter` varchar(20) DEFAULT NULL,
  `repository_id` int(11) NOT NULL,
  `original_base_commit_id` varchar(40) DEFAULT NULL,
  `original_start_commit_id` varchar(40) DEFAULT NULL,
  `original_end_commit_id` varchar(40) DEFAULT NULL,
  `blob_position` int(11) unsigned DEFAULT NULL,
  `blob_path` varbinary(1024) DEFAULT NULL,
  `blob_commit_oid` char(40) DEFAULT NULL,
  `left_blob` tinyint(1) NOT NULL DEFAULT '0',
  `state` int(11) NOT NULL DEFAULT '0',
  `pull_request_review_id` int(11) DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `reply_to_id` int(11) DEFAULT NULL,
  `outdated` tinyint(1) NOT NULL DEFAULT '0',
  `comment_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `comment_hidden_reason` varbinary(1024) DEFAULT NULL,
  `comment_hidden_classifier` varchar(50) DEFAULT NULL,
  `comment_hidden_by` int(11) DEFAULT NULL,
  `pull_request_review_thread_id` int(11) DEFAULT NULL,
  `start_position_offset` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_pr_review_comments_on_pr_id_and_created_at` (`pull_request_id`,`created_at`),
  KEY `index_pull_request_review_comments_on_repo_id_updated_at` (`repository_id`,`updated_at`),
  KEY `index_pull_request_review_comments_on_repository_id_and_user_id` (`repository_id`,`user_id`),
  KEY `index_pr_review_comments_on_reply_to_id` (`reply_to_id`),
  KEY `index_pull_request_review_comments_on_user_id_and_user_hidden` (`user_id`,`user_hidden`),
  KEY `index_pull_request_review_comments_on_repo_id_state_and_user_id` (`repository_id`,`state`,`user_id`),
  KEY `index_pr_review_comments_on_pr_id_user_hidden_user_id_and_state` (`pull_request_id`,`user_hidden`,`user_id`,`state`),
  KEY `index_pr_review_comments_on_user_hidden_and_user_id` (`user_hidden`,`user_id`),
  KEY `index_pr_review_comments_on_repo_id_and_path_and_created_at` (`repository_id`,`path`(85),`created_at`),
  KEY `index_pull_request_review_comments_on_repo_and_hidden_and_user` (`repository_id`,`user_hidden`,`user_id`),
  KEY `index_pull_request_review_comments_on_pr_review_thread_id` (`pull_request_review_thread_id`),
  KEY `index_pull_request_review_comments_on_user_id_and_created_at` (`user_id`,`created_at`),
  KEY `index_pull_request_review_comments_on_user_id_and_updated_at` (`user_id`,`updated_at`),
  KEY `pull_request_review_id_and_reply_to_id` (`pull_request_review_id`,`reply_to_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `pull_request_review_edits`;
CREATE TABLE `pull_request_review_edits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pull_request_review_id` int(11) NOT NULL,
  `edited_at` datetime NOT NULL,
  `editor_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by_id` int(11) DEFAULT NULL,
  `diff` mediumblob,
  `user_content_edit_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_pull_request_review_edits_on_user_content_edit_id` (`user_content_edit_id`),
  KEY `index_pull_request_review_edits_on_pull_request_review_id` (`pull_request_review_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `pull_request_review_threads`;
CREATE TABLE `pull_request_review_threads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pull_request_id` int(11) DEFAULT NULL,
  `pull_request_review_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `resolver_id` int(11) DEFAULT NULL,
  `resolved_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_pull_request_review_threads_on_pr_id_and_pr_review_id` (`pull_request_id`,`pull_request_review_id`),
  KEY `index_pull_request_review_threads_on_pull_request_review_id` (`pull_request_review_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `pull_request_reviews`;
CREATE TABLE `pull_request_reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pull_request_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `head_sha` char(40) NOT NULL,
  `body` mediumblob,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `submitted_at` datetime DEFAULT NULL,
  `formatter` varchar(20) DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `comment_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `comment_hidden_reason` varbinary(1024) DEFAULT NULL,
  `comment_hidden_classifier` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_pull_request_reviews_on_user_id_and_submitted_at` (`user_id`,`submitted_at`),
  KEY `index_reviews_pull_request_user_state` (`pull_request_id`,`user_hidden`,`user_id`,`state`),
  KEY `index_pull_request_reviews_on_user_hidden_and_user_id` (`user_hidden`,`user_id`),
  KEY `index_pull_request_reviews_on_user_id_and_user_hidden` (`user_id`,`user_hidden`),
  KEY `index_pull_request_reviews_on_pull_request_id_and_created_at` (`pull_request_id`,`created_at`),
  KEY `index_pull_request_reviews_on_pull_request_id_and_submitted_at` (`pull_request_id`,`submitted_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `pull_request_reviews_review_requests`;
CREATE TABLE `pull_request_reviews_review_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pull_request_review_id` int(11) NOT NULL,
  `review_request_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_pr_reviews_review_requests_on_request_id_and_pr_review_id` (`review_request_id`,`pull_request_review_id`),
  KEY `index_pr_reviews_review_requests_on_pr_review_id_and_request_id` (`pull_request_review_id`,`review_request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `pull_request_revisions`;
CREATE TABLE `pull_request_revisions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pull_request_id` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `ready` tinyint(1) NOT NULL,
  `base_oid` varchar(64) NOT NULL,
  `head_oid` varchar(64) NOT NULL,
  `revised_at` datetime NOT NULL,
  `force_pushed` tinyint(1) NOT NULL,
  `commits_count` int(11) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `push_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_pull_request_revisions_on_pull_and_number` (`pull_request_id`,`number`),
  KEY `index_pull_request_revisions_on_pull_and_base_and_head_and_ready` (`pull_request_id`,`base_oid`,`head_oid`,`ready`),
  KEY `index_pull_request_revisions_on_pull_and_ready_and_number` (`pull_request_id`,`ready`,`number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `pull_requests`;
CREATE TABLE `pull_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `base_sha` char(40) DEFAULT NULL,
  `head_sha` char(40) DEFAULT NULL,
  `repository_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `base_repository_id` int(11) DEFAULT NULL,
  `head_repository_id` int(11) DEFAULT NULL,
  `base_ref` varbinary(1024) DEFAULT NULL,
  `head_ref` varbinary(1024) DEFAULT NULL,
  `merged_at` datetime DEFAULT NULL,
  `base_user_id` int(11) DEFAULT NULL,
  `head_user_id` int(11) DEFAULT NULL,
  `mergeable` tinyint(1) unsigned DEFAULT NULL,
  `merge_commit_sha` char(40) DEFAULT NULL,
  `contributed_at_timestamp` bigint(20) DEFAULT NULL,
  `contributed_at_offset` mediumint(9) DEFAULT NULL,
  `fork_collab_state` int(11) NOT NULL DEFAULT '0',
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `base_sha_on_merge` char(40) DEFAULT NULL,
  `work_in_progress` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_pull_requests_on_base_repository_id_and_base_ref` (`base_repository_id`,`base_ref`(767)),
  KEY `index_pull_requests_on_base_repository_id_and_head_ref` (`base_repository_id`,`head_ref`(767)),
  KEY `index_pull_requests_on_repository_id_and_head_ref` (`repository_id`,`head_ref`(767)),
  KEY `index_pull_requests_on_repository_id_and_user_id_and_user_hidden` (`repository_id`,`user_id`,`user_hidden`),
  KEY `index_pull_requests_on_user_hidden_and_user_id` (`user_hidden`,`user_id`),
  KEY `index_pull_requests_on_repository_id_and_head_sha` (`repository_id`,`head_sha`),
  KEY `index_pull_requests_on_user_id_and_created_at` (`user_id`,`created_at`),
  KEY `index_pull_requests_on_user_id_and_repository_id` (`user_id`,`repository_id`),
  KEY `index_pull_requests_on_repository_id_and_updated_at` (`repository_id`,`updated_at`),
  KEY `index_pull_requests_on_repository_id_and_created_at` (`repository_id`,`created_at`),
  KEY `head_repository_id_and_head_ref_and_head_sha_and_repository_id` (`head_repository_id`,`head_ref`,`head_sha`,`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `reactions`;
CREATE TABLE `reactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(30) NOT NULL,
  `user_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `subject_type` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_reactions_identity` (`user_id`,`subject_id`,`subject_type`,`content`),
  KEY `index_reactions_on_subject_content_created_at` (`subject_id`,`subject_type`,`content`,`created_at`),
  KEY `index_reactions_on_user_hidden_and_user_id` (`user_hidden`,`user_id`),
  KEY `subject_id_and_subject_type_and_user_hidden_and_created_at` (`subject_id`,`subject_type`,`user_hidden`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `registry_package_dependencies`;
CREATE TABLE `registry_package_dependencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `registry_package_version_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL,
  `dependency_type` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_registry_package_dependencies_on_package_version_id` (`registry_package_version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `registry_package_files`;
CREATE TABLE `registry_package_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `registry_package_id` int(11) NOT NULL,
  `release_id` int(11) DEFAULT NULL,
  `platform` varchar(255) NOT NULL DEFAULT '',
  `sha1` varchar(40) DEFAULT NULL,
  `version` varchar(255) NOT NULL,
  `commit_oid` varchar(40) DEFAULT NULL,
  `storage_blob_id` int(11) DEFAULT NULL,
  `uploader_id` int(11) DEFAULT NULL,
  `guid` varchar(36) DEFAULT NULL,
  `oid` varchar(64) DEFAULT NULL,
  `size` int(11) NOT NULL,
  `state` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `filename` varchar(255) NOT NULL DEFAULT '',
  `md5` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_reg_package_files_on_reg_pkg_id_and_ver_and_plat_and_fname` (`registry_package_id`,`version`,`platform`,`filename`),
  KEY `index_registry_package_files_on_release_id` (`release_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `registry_package_metadata`;
CREATE TABLE `registry_package_metadata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_version_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `value` blob,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_registry_package_metadata_on_package_version_id_and_name` (`package_version_id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `registry_package_tags`;
CREATE TABLE `registry_package_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `registry_package_id` int(11) NOT NULL,
  `registry_package_version_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_registry_package_tags_on_registry_package_id_and_name` (`registry_package_id`,`name`),
  KEY `index_registry_package_tags_on_name` (`name`),
  KEY `index_registry_package_tags_on_registry_package_version_id` (`registry_package_version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `registry_packages`;
CREATE TABLE `registry_packages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `package_type` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `registry_package_type` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_registry_package_on_repo_and_name_and_type` (`repository_id`,`name`,`package_type`),
  UNIQUE KEY `index_registry_packages_on_owner_id_name_package_type` (`owner_id`,`name`,`package_type`),
  UNIQUE KEY `index_registry_packages_on_repository_id_and_slug` (`repository_id`,`slug`),
  UNIQUE KEY `index_packages_on_repo_id_and_name_and_registry_package_type` (`repository_id`,`name`,`registry_package_type`),
  UNIQUE KEY `index_registry_packages_on_owner_id_name_registry_package_type` (`owner_id`,`name`,`registry_package_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `releases`;
CREATE TABLE `releases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varbinary(1024) DEFAULT NULL,
  `tag_name` varbinary(1024) NOT NULL,
  `body` mediumblob,
  `author_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `state` int(11) DEFAULT '0',
  `pending_tag` varbinary(1024) DEFAULT NULL,
  `prerelease` tinyint(1) NOT NULL DEFAULT '0',
  `target_commitish` varchar(255) DEFAULT NULL,
  `published_at` datetime DEFAULT NULL,
  `formatter` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `by_state` (`repository_id`,`state`),
  KEY `by_published` (`repository_id`,`published_at`),
  KEY `by_repo_and_tag` (`repository_id`,`tag_name`(50))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `repositories`;
CREATE TABLE `repositories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `owner_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `sandbox` tinyint(1) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `public` tinyint(1) DEFAULT '1',
  `description` mediumblob,
  `homepage` varchar(255) DEFAULT NULL,
  `source_id` int(11) DEFAULT NULL,
  `public_push` tinyint(1) DEFAULT NULL,
  `disk_usage` int(11) DEFAULT '0',
  `locked` tinyint(1) DEFAULT '0',
  `pushed_at` datetime DEFAULT NULL,
  `watcher_count` int(11) DEFAULT '0',
  `public_fork_count` int(11) NOT NULL DEFAULT '1',
  `primary_language_name_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `has_issues` tinyint(1) DEFAULT '1',
  `has_wiki` tinyint(1) DEFAULT '1',
  `has_discussions` tinyint(1) NOT NULL DEFAULT '0',
  `has_downloads` tinyint(1) DEFAULT '1',
  `raw_data` blob,
  `organization_id` int(11) DEFAULT NULL,
  `disabled_at` datetime DEFAULT NULL,
  `disabled_by` int(11) DEFAULT NULL,
  `disabling_reason` varchar(30) DEFAULT NULL,
  `health_status` varchar(30) DEFAULT NULL,
  `pushed_at_usec` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `reflog_sync_enabled` tinyint(1) DEFAULT '0',
  `made_public_at` datetime DEFAULT NULL,
  `user_hidden` tinyint(4) NOT NULL DEFAULT '0',
  `maintained` tinyint(1) NOT NULL DEFAULT '1',
  `template` tinyint(1) NOT NULL DEFAULT '0',
  `owner_login` varchar(40) DEFAULT NULL,
  `world_writable_wiki` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repositories_on_owner_id_and_name_and_active` (`owner_id`,`name`,`active`),
  KEY `index_repositories_on_public_and_watcher_count` (`public`,`watcher_count`),
  KEY `index_repositories_on_primary_language_name_id_and_public` (`primary_language_name_id`,`public`),
  KEY `index_repositories_on_created_at` (`created_at`),
  KEY `index_repositories_on_disabled_at` (`disabled_at`),
  KEY `index_repositories_on_owner_id_and_pushed_at` (`owner_id`,`pushed_at`),
  KEY `index_repositories_on_owner_id_and_made_public_at` (`owner_id`,`made_public_at`),
  KEY `index_repositories_on_user_hidden_and_owner_id` (`user_hidden`,`owner_id`),
  KEY `index_repositories_on_parent_id` (`parent_id`),
  KEY `index_repositories_on_organization_id_and_active_and_public` (`organization_id`,`active`,`public`),
  KEY `index_repositories_on_watcher_count_and_created_at_and_pushed_at` (`watcher_count`,`created_at`,`pushed_at`),
  KEY `index_repositories_on_active_and_updated_at` (`active`,`updated_at`),
  KEY `index_repositories_on_source_id_and_organization_id` (`source_id`,`organization_id`),
  KEY `index_repositories_on_owner_and_parent_and_public_and_source_id` (`owner_id`,`parent_id`,`public`,`source_id`),
  KEY `index_on_public_and_primary_language_name_id_and_parent_id` (`public`,`primary_language_name_id`,`parent_id`),
  KEY `index_repositories_on_template_and_active_and_owner_id` (`template`,`active`,`owner_id`),
  KEY `index_repositories_on_owner_login_and_name_and_active` (`owner_login`,`name`,`active`),
  KEY `owner_and_org_and_name_and_active_and_public_and_disabled_at` (`owner_id`,`organization_id`,`name`,`active`,`public`,`disabled_at`),
  KEY `index_repositories_on_owner_id_and_updated_at` (`owner_id`,`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `repository_action_releases`;
CREATE TABLE `repository_action_releases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_action_id` int(11) NOT NULL,
  `release_id` int(11) NOT NULL,
  `published_on_marketplace` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_action_releases_on_release_and_action_id` (`release_id`,`repository_action_id`),
  KEY `index_repository_action_releases_on_repository_action_id` (`repository_action_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `repository_actions`;
CREATE TABLE `repository_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(255) NOT NULL,
  `name` varbinary(1024) NOT NULL,
  `description` mediumblob,
  `icon_name` varchar(20) DEFAULT NULL,
  `color` varchar(6) DEFAULT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `repository_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `rank_multiplier` float NOT NULL DEFAULT '1',
  `state` int(11) NOT NULL DEFAULT '0',
  `slug` varchar(255) DEFAULT NULL,
  `security_email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_actions_on_repository_id_and_path` (`repository_id`,`path`),
  UNIQUE KEY `index_repository_actions_on_slug` (`slug`),
  KEY `index_repository_actions_on_repository_id_and_featured` (`repository_id`,`featured`),
  KEY `index_repository_actions_on_rank_multiplier` (`rank_multiplier`),
  KEY `index_repository_actions_on_state_and_slug` (`state`,`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `repository_advisory_comment_edits`;
CREATE TABLE `repository_advisory_comment_edits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_advisory_comment_id` int(11) NOT NULL,
  `edited_at` datetime NOT NULL,
  `editor_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by_id` int(11) DEFAULT NULL,
  `diff` mediumblob,
  `user_content_edit_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_advisory_comment_edits_on_user_content_edit_id` (`user_content_edit_id`),
  KEY `index_on_pull_request_review_comment_id` (`repository_advisory_comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `repository_advisory_edits`;
CREATE TABLE `repository_advisory_edits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_advisory_id` int(11) NOT NULL,
  `edited_at` datetime NOT NULL,
  `editor_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by_id` int(11) DEFAULT NULL,
  `diff` mediumblob,
  `user_content_edit_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_advisory_edits_on_user_content_edit_id` (`user_content_edit_id`),
  KEY `index_repository_advisory_edits_on_repository_advisory_id` (`repository_advisory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `repository_invitations`;
CREATE TABLE `repository_invitations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `inviter_id` int(11) NOT NULL,
  `invitee_id` int(11) DEFAULT NULL,
  `permissions` tinyint(4) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `hashed_token` varchar(44) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_invitations_on_repository_id_and_invitee_id` (`repository_id`,`invitee_id`),
  UNIQUE KEY `index_repository_invitations_on_hashed_token` (`hashed_token`),
  UNIQUE KEY `index_repository_invitations_on_repository_id_and_email` (`repository_id`,`email`),
  KEY `index_repository_invitations_on_invitee_id` (`invitee_id`),
  KEY `index_repository_invitations_on_created_at` (`created_at`),
  KEY `index_repository_invitations_on_role_id` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `repository_licenses`;
CREATE TABLE `repository_licenses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) DEFAULT NULL,
  `license_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_licenses_on_repository_id` (`repository_id`),
  KEY `index_repository_licenses_on_license_id` (`license_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `repository_networks`;
CREATE TABLE `repository_networks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `root_id` int(11) NOT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `repository_count` int(11) DEFAULT NULL,
  `disk_usage` int(11) DEFAULT NULL,
  `accessed_at` datetime DEFAULT NULL,
  `maintenance_status` varchar(255) NOT NULL,
  `last_maintenance_at` datetime NOT NULL,
  `pushed_at` datetime DEFAULT NULL,
  `pushed_count` int(11) NOT NULL,
  `pushed_count_since_maintenance` int(11) NOT NULL,
  `disabled_at` datetime DEFAULT NULL,
  `disabled_by` int(11) DEFAULT NULL,
  `disabling_reason` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `moving` tinyint(1) NOT NULL DEFAULT '0',
  `unpacked_size_in_mb` int(11) DEFAULT NULL,
  `last_maintenance_attempted_at` datetime DEFAULT NULL,
  `cache_version_number` int(11) NOT NULL DEFAULT '0',
  `maintenance_retries` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_repository_networks_on_root_id` (`root_id`),
  KEY `index_repository_networks_on_owner_id` (`owner_id`),
  KEY `index_repository_networks_on_created_at` (`created_at`),
  KEY `index_repository_networks_on_pushed_at` (`pushed_at`),
  KEY `index_repository_networks_on_accessed_at` (`accessed_at`),
  KEY `index_repository_networks_on_maintenance_status` (`maintenance_status`,`pushed_count_since_maintenance`,`unpacked_size_in_mb`,`last_maintenance_at`),
  KEY `index_repository_networks_on_maint_status_and_unpacked_size` (`maintenance_status`,`unpacked_size_in_mb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `repository_redirects`;
CREATE TABLE `repository_redirects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `repository_name` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_repository_redirects_on_repository_id` (`repository_id`),
  KEY `index_repository_redirects_on_repository_name_and_created_at` (`repository_name`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `repository_sequences`;
CREATE TABLE `repository_sequences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `number` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_sequences_on_repository_id` (`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `repository_topics`;
CREATE TABLE `repository_topics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_topics_on_repository_id_and_topic_id` (`repository_id`,`topic_id`),
  KEY `index_repository_topics_on_repository_id_and_state` (`repository_id`,`state`),
  KEY `index_repository_topics_on_topic_id_and_state` (`topic_id`,`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `repository_transfers`;
CREATE TABLE `repository_transfers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `requester_id` int(11) NOT NULL,
  `responder_id` int(11) DEFAULT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `target_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_transfers_on_repository_id_and_target_id` (`repository_id`,`target_id`),
  KEY `index_repository_transfers_on_requester_id` (`requester_id`),
  KEY `index_repository_transfers_on_responder_id` (`responder_id`),
  KEY `index_repository_transfers_on_state` (`state`),
  KEY `index_repository_transfers_on_target_id` (`target_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `repository_unlocks`;
CREATE TABLE `repository_unlocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unlocked_by_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `expires_at` datetime NOT NULL,
  `revoked` tinyint(1) NOT NULL DEFAULT '0',
  `revoked_by_id` int(11) DEFAULT NULL,
  `revoked_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `staff_access_grant_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_repository_unlocks_on_repository_id` (`repository_id`),
  KEY `index_repository_unlocks_on_staff_access_grant_id` (`staff_access_grant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `repository_wikis`;
CREATE TABLE `repository_wikis` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `pushed_at` datetime DEFAULT NULL,
  `maintenance_status` varchar(255) DEFAULT NULL,
  `last_maintenance_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `pushed_count` int(11) NOT NULL DEFAULT '0',
  `pushed_count_since_maintenance` int(11) NOT NULL DEFAULT '0',
  `last_maintenance_attempted_at` datetime DEFAULT NULL,
  `cache_version_number` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_wikis_on_repository_id` (`repository_id`),
  KEY `index_repository_wikis_on_maintenance_status` (`maintenance_status`,`pushed_count_since_maintenance`,`last_maintenance_at`),
  KEY `index_repository_wikis_on_last_maintenance_at` (`last_maintenance_at`),
  KEY `index_repository_wikis_on_pushed_at` (`pushed_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `required_status_checks`;
CREATE TABLE `required_status_checks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `protected_branch_id` int(11) NOT NULL,
  `context` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_required_status_checks_on_id_and_context` (`protected_branch_id`,`context`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `review_dismissal_allowances`;
CREATE TABLE `review_dismissal_allowances` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `protected_branch_id` int(11) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `actor_type` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_review_dismissal_allowances_on_branch_and_actor` (`protected_branch_id`,`actor_id`,`actor_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `review_request_reasons`;
CREATE TABLE `review_request_reasons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `review_request_id` int(11) NOT NULL,
  `codeowners_tree_oid` varchar(40) DEFAULT NULL,
  `codeowners_path` varchar(255) DEFAULT NULL,
  `codeowners_line` int(11) DEFAULT NULL,
  `codeowners_pattern` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_review_request_reasons_on_review_request_id` (`review_request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `review_requests`;
CREATE TABLE `review_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reviewer_id` int(11) NOT NULL,
  `pull_request_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `reviewer_type` varchar(64) NOT NULL DEFAULT 'User',
  `dismissed_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_review_requests_on_reviewer_id_and_reviewer_type` (`reviewer_id`,`reviewer_type`),
  KEY `index_review_requests_on_pr_id_and_reviewer_type_and_reviewer_id` (`pull_request_id`,`reviewer_type`,`reviewer_id`),
  KEY `index_review_requests_on_pull_request_id_and_dismissed_at` (`pull_request_id`,`dismissed_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `tabs`;
CREATE TABLE `tabs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `anchor` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `repository_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `upload_manifests`;
CREATE TABLE `upload_manifests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `uploader_id` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `message` blob,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `branch` varbinary(1024) DEFAULT NULL,
  `commit_oid` varchar(40) DEFAULT NULL,
  `directory` varbinary(1024) DEFAULT NULL,
  `base_branch` varbinary(1024) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
