DROP TABLE IF EXISTS `abilities_permissions_routing`;
CREATE TABLE `abilities_permissions_routing` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cluster` int(11) NOT NULL DEFAULT '0',
  `subject_type` varchar(60) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_abilities_permissions_routing_by_cluster` (`cluster`,`subject_type`),
  KEY `index_abilities_permissions_routing_by_subject_type` (`subject_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `fine_grained_permissions`;
CREATE TABLE `fine_grained_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `custom_roles_enabled` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_fine_grained_permissions_on_action` (`action`),
  KEY `index_fine_grained_permissions_on_custom_roles_enabled` (`custom_roles_enabled`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `actor_id` int(11) unsigned NOT NULL,
  `actor_type` varchar(40) NOT NULL,
  `action` int(11) unsigned NOT NULL,
  `subject_id` int(11) unsigned NOT NULL,
  `subject_type` varchar(60) NOT NULL,
  `priority` int(11) unsigned NOT NULL DEFAULT '1',
  `parent_id` int(11) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `ability_id` bigint(20) DEFAULT NULL,
  `expires_at` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_permissions_by_identity` (`actor_type`,`actor_id`,`subject_type`,`subject_id`,`priority`,`parent_id`),
  UNIQUE KEY `index_permissions_by_subject_type_ability` (`subject_type`,`ability_id`),
  KEY `index_permissions_by_subject_actor_type_action_priority` (`subject_id`,`subject_type`,`actor_type`,`action`,`priority`),
  KEY `index_permissions_on_actor_subject_and_action` (`actor_id`,`subject_id`,`actor_type`,`subject_type`,`action`),
  KEY `index_permissions_on_actor_type_and_created_at` (`actor_type`,`created_at`),
  KEY `index_permissions_on_expires_at` (`expires_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `role_permissions`;
CREATE TABLE `role_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `fine_grained_permission_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `action` varchar(60) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_role_perms_by_role_and_fgp` (`fine_grained_permission_id`,`role_id`),
  KEY `index_role_permissions_on_role_id_and_action` (`role_id`,`action`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varbinary(255) NOT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `owner_type` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `base_role_id` int(11) DEFAULT NULL,
  `description` varbinary(608) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_roles_on_name_and_owner_id_and_owner_type` (`name`,`owner_id`,`owner_type`),
  KEY `index_roles_on_owner_id_and_owner_type` (`owner_id`,`owner_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `target_id` int(11) NOT NULL,
  `target_type` varchar(60) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `actor_type` varchar(60) NOT NULL,
  `actor_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_user_roles_actor_and_target` (`actor_id`,`actor_type`,`target_id`,`target_type`),
  KEY `idx_user_roles_actor_role_and_target` (`actor_id`,`actor_type`,`role_id`,`target_id`,`target_type`),
  KEY `index_user_roles_on_target_id_and_target_type` (`target_id`,`target_type`),
  KEY `index_user_roles_on_role_id_and_actor_id_and_actor_type` (`role_id`,`actor_id`,`actor_type`),
  KEY `index_user_roles_on_role_target_type_actor` (`role_id`,`target_id`,`target_type`,`actor_type`,`actor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
