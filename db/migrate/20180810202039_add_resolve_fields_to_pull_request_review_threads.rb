# frozen_string_literal: true

class AddResolveFieldsToPullRequestReviewThreads < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_column :pull_request_review_threads, :resolver_id, :integer
    add_column :pull_request_review_threads, :resolved_at, :datetime

    add_column :archived_pull_request_review_threads, :resolver_id, :integer
    add_column :archived_pull_request_review_threads, :resolved_at, :datetime
  end
end
