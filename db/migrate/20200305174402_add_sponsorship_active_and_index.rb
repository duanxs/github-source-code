# frozen_string_literal: true

class AddSponsorshipActiveAndIndex < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :sponsorships, :active, :boolean, null: false, default: true
    add_index :sponsorships, [:sponsorable_id, :sponsorable_type]
    remove_index :sponsorships, [:is_sponsor_tier_reward_fulfilled]
  end
end
