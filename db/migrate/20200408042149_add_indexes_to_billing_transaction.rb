# frozen_string_literal: true

class AddIndexesToBillingTransaction < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def up
    add_index :billing_transactions,
      [:user_id, :transaction_type, :payment_type, :last_status, :asset_packs_total, :created_at],
      name: "index_billing_transactions_on_helphub_conditions"
  end

  def down
    remove_index :billing_transactions,
      name: "index_billing_transactions_on_helphub_conditions"
  end
end
