# frozen_string_literal: true

require "github/transitions/20190717212730_fix_push_protected_branch_fgp_and_role_perms"

class FixPushProtectedBranchFgpAndRolePermsTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::FixPushProtectedBranchFgpAndRolePerms.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
