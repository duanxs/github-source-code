# frozen_string_literal: true

class BillingTransactionsLineItemsConcatIndex < GitHub::Migration
  def change
    # new index to fix https://github.com/github/github/issues/87987
    add_index :billing_transaction_line_items, [:marketplace_listing_plan_id, :created_at], name: "index_on_marketplace_listing_plan_id_and_created_at"
  end
end
