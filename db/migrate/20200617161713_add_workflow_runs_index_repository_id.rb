# frozen_string_literal: true

class AddWorkflowRunsIndexRepositoryId < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_index :workflow_runs, [:repository_id],
      name: "index_workflow_runs_on_repository_id"
  end
end
