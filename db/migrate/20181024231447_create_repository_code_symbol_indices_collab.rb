# frozen_string_literal: true

class CreateRepositoryCodeSymbolIndicesCollab < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :repository_code_symbol_indices do |t|
      t.string :commit_oid, null: false, limit: 40
      t.column :ref, "VARBINARY(767)", null: false
      t.integer :page, null: false, default: 0
      t.string :status, null: false, limit: 10
      t.references :repository, null: false
      t.timestamps null: false
    end

    add_index :repository_code_symbol_indices, [:repository_id, :ref, :commit_oid],
      name: "index_repository_code_symbol_indices_on_repository_ref_commit",
      unique: true
  end
end
