# frozen_string_literal: true

class AddStateTrackingToExternalIdentities < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/database-migrations-for-dotcom for tips

  def change
    add_column :external_identities, :disabled_at, :datetime, null: true
    add_column :external_identities, :deleted_at, :datetime, null: true
  end
end
