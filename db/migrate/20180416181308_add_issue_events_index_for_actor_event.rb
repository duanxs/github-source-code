# frozen_string_literal: true

class AddIssueEventsIndexForActorEvent < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def up
    remove_index :issue_events, :actor_id
    add_index :issue_events, [:actor_id, :event, :created_at]
  end

  def down
    remove_index :issue_events, [:actor_id, :event, :created_at]
    add_index :issue_events, :actor_id
  end
end
