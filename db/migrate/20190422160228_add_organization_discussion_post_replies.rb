# frozen_string_literal: true

class AddOrganizationDiscussionPostReplies < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :organization_discussion_post_replies do |t|
      t.integer :number, null: false
      t.mediumblob :body, null: false
      t.string :formatter, limit: 20
      t.belongs_to :user, null: false, index: true
      t.belongs_to :organization_discussion_post, null: false
      t.timestamps null: false
    end

    add_index :organization_discussion_post_replies, [:organization_discussion_post_id, :number],
      unique: true, name: "idx_org_disc_replies_organization_discussion_post_id_and_number"
  end
end
