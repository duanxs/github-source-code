# frozen_string_literal: true

class CreateCompromisedPasswords < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    create_table :compromised_passwords, id: false do |t|
      t.column :id, "bigint NOT NULL AUTO_INCREMENT PRIMARY KEY"
      t.string :sha1_password, limit: 40
      t.index :sha1_password
    end
  end

  def down
    drop_table :compromised_passwords
  end
end
