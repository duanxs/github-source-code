# frozen_string_literal: true

class AddInodesFreeUsedToPagesFileservers < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations for tips
  self.use_connection_class(ApplicationRecord::Repositories)

  def self.up
    add_column :pages_fileservers, :inodes_free, "BIGINT(20) unsigned", default: 0, null: false, if_not_exists: true
    add_column :pages_fileservers, :inodes_used, "BIGINT(20) unsigned", default: 0, null: false, if_not_exists: true

    add_index :pages_fileservers, [:non_voting, :online, :embargoed, :disk_free, :inodes_free], name: "index_pages_fileservers_on_voting_online_embargo_df_inodes", if_not_exists: true
  end

  def self.down
    remove_index :pages_fileservers, name: "index_pages_fileservers_on_voting_online_embargo_df_inodes", if_exists: true

    remove_column :pages_fileservers, :inodes_free, if_exists: true
    remove_column :pages_fileservers, :inodes_used, if_exists: true
  end
end
