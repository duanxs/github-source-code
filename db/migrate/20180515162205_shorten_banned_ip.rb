# frozen_string_literal: true

class ShortenBannedIp < GitHub::Migration
  def change
    # shorten string column to store ipv4 up to 15 chars and ipv6 up to 39 chars
    change_column :banned_ip_addresses, :ip, :string, limit: 40, null: false
  end
end
