# frozen_string_literal: true

class AddDocusignToSponsorsListing < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :sponsors_listings, :docusign_envelope_id, :string, limit: 40, null: true
    add_column :sponsors_listings, :docusign_envelope_status, :string, limit: 10, null: true
  end
end
