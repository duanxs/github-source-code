# frozen_string_literal: true

class MediaBlobsRnIndex < GitHub::Migration
  def change
    add_index :media_blobs, :repository_network_id
  end
end
