# frozen_string_literal: true

class CreateBillingMeteredUsageExport < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :metered_usage_exports do |t|
      t.date :starts_on, null: false
      t.date :ends_on, null: false
      t.string :filename, null: false
      t.belongs_to :billable_owner, polymorphic: { limit: 12 }, null: false
      t.references :requester, references: :users, null: false

      t.timestamps null: false
    end

    add_index :metered_usage_exports, :filename, unique: true
    add_index :metered_usage_exports, [:billable_owner_type, :billable_owner_id], name: "idx_metered_usage_exports_billable_owner"
  end
end
