# frozen_string_literal: true

class DropBraintreeCustomerIdColumnFromUser < GitHub::Migration
  def self.up
    remove_column :users, :braintree_customer_id
  end

  def self.down
    add_column :users, :braintree_customer_id, :string, limit: 30, default: nil
  end
end
