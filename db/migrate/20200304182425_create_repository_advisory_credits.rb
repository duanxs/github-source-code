# frozen_string_literal: true

class CreateRepositoryAdvisoryCredits < GitHub::Migration
  use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :advisory_credits do |t|
      t.string :ghsa_id, limit: 19, null: false
      t.integer :user_id, null: false
      t.datetime :accepted_at
      t.timestamps null: false

      # Used for database-level validation against duplicate credits.
      t.index [:ghsa_id, :user_id], unique: true

      # Used for finding all credits for a given repository advisory or
      # published security advisory.
      t.index [:ghsa_id, :accepted_at]

      # Used for finding all advisories credited to a given user.
      t.index [:user_id, :accepted_at]
    end
  end
end
