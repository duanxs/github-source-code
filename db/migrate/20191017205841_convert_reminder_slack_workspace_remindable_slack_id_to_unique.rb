# frozen_string_literal: true

class ConvertReminderSlackWorkspaceRemindableSlackIdToUnique < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_index :reminder_slack_workspaces, [:remindable_type, :remindable_id, :slack_id], unique: true, name: "index_reminder_slack_workspaces_on_remindable_and_slack_id"
    remove_index :reminder_slack_workspaces, column: [:remindable_type, :remindable_id], name: "index_reminder_slack_workspaces_on_remindable_type_and_id"
  end
end
