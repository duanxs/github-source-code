# frozen_string_literal: true

class AddContentReferencesDataModel < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def up
    create_table :content_references do |t|
      t.references :user
      t.integer :content_id, null: false
      t.string :content_type, null: false, limit: 30
      t.string :reference_hash, limit: 32, null: false
      t.text :reference
      t.timestamps null: false
    end

    add_index :content_references, [:content_id, :content_type, :reference_hash], unique: true, name: "index_on_content_id_and_content_type_and_reference_hash"
    add_index :content_references, :user_id, name: "index_content_references_on_user_id"

    create_table :content_reference_attachments do |t|
      t.references :content_reference
      t.references :integration

      t.integer :state, null: false, default: 0
      t.column :title, "VARBINARY(1024)", null: true
      t.column :body, :mediumblob, null: true

      t.timestamps null: false
    end

    add_index :content_reference_attachments, [:content_reference_id, :integration_id], unique: true, name: "index_on_content_reference_id_and_integration_id"
    add_index :content_reference_attachments, [:content_reference_id, :state], name: "index_on_content_reference_id_and_state"
    add_index :content_reference_attachments, :integration_id, name: "index_content_reference_attachments_on_integration_id"
  end

  def down
    drop_table :content_reference_attachments
    drop_table :content_references
  end
end
