# frozen_string_literal: true

class AddIsSponsorOptedInToShareWithFiscalHostToSponsorship < GitHub::Migration
  extend GitHub::SafeDatabaseMigrationHelper

  self.use_connection_class(ApplicationRecord::Collab)

  def self.up
    add_column :sponsorships, :is_sponsor_opted_in_to_share_with_fiscal_host, :boolean, default: false, null: false, if_not_exists: true
  end

  def self.down
    remove_column :sponsorships, :is_sponsor_opted_in_to_share_with_fiscal_host, if_exists: true
  end
end
