# frozen_string_literal: true

class AddNullConstraintToPendingVulnGhsa < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Notify)

  def change
    change_column_null :pending_vulnerabilities, :ghsa_id, false
  end
end
