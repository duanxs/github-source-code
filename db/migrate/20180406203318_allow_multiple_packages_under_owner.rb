# frozen_string_literal: true

class AllowMultiplePackagesUnderOwner < GitHub::Migration
  extend GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Repositories)

  def self.up
    # Add the non-unique index to allow a package with the same name & type in a different repo, under an owner
    add_index :registry_packages, [:owner_id, :name, :package_type], unique: false, if_not_exists: true

    # Remove the unique index that asserts only one package with a name & type can exist under an owner
    remove_index :registry_packages, name: "index_registry_package_on_owner_and_name_and_type", if_exists: true
  end

  def self.down
    # Remove the non-unique index to allow a package with the same name & type in a different repo, under an owner
    remove_index :registry_packages, [:owner_id, :name, :package_type], unique: false, if_exists: true

    # Add the index that asserts only one package with a name & type can exist under an owner
    add_index :registry_packages, [:owner_id, :name, :package_type], unique: true, name: "index_registry_package_on_owner_and_name_and_type", if_not_exists: true
  end
end
