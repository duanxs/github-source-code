# frozen_string_literal: true

class AddStripeAccountIdToStripeWebhooks < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper

  self.use_connection_class(ApplicationRecord::Ballast)

  def up
    add_column :stripe_webhooks, :account_id, :string, collation: "utf8_bin", if_not_exists: true
    add_index :stripe_webhooks, [:account_id, :kind, :created_at],
      name: "index_stripe_webhooks_account_kind_created", if_not_exists: true
  end

  def down
    remove_column :stripe_webhooks, :account_id, :string, if_exists: true
    remove_index :stripe_webhooks, name: "index_stripe_webhooks_account_kind_created", if_exists: true
  end
end
