# frozen_string_literal: true

class AddExpiresAtTimestamp < GitHub::Migration
  def change
    add_column :authentication_tokens, :expires_at_timestamp, :bigint
    add_index :authentication_tokens, :expires_at_timestamp
  end
end
