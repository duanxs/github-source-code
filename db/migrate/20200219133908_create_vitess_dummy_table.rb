# frozen_string_literal: true

class CreateVitessDummyTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Mysql2)

  def change
    create_table :vitess_dummies do |t|
      t.integer :dummy_number, null: false
      t.integer :dummy_number_nullable, null: true
      t.string :dummy_string, limit: 80

      t.timestamps null: false
    end

    add_index :vitess_dummies, [:dummy_number],
      name: "index_vitess_dummies_dummy_number"
    add_index :vitess_dummies, [:dummy_number, :dummy_string], unique: true,
      name: "index_vitess_dummies_unique"
  end
end
