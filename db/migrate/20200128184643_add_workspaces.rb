# frozen_string_literal: true

class AddWorkspaces < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :workspaces do |t|
      t.belongs_to :repository,   null: false
      t.belongs_to :owner,        class_name: "User", null: false
      t.references :pull_request, null: true
      t.column     :guid,         "char(36)"
      t.string     :name,         null: false, limit: 90
      t.column     :oid,          "char(40) NOT NULL"
      t.string     :ref,          null: false, limit: 255

      t.timestamps null: false

      # This is a constraint we need to enforce on our side to match a
      # constraint on the VSO side.
      t.index [:owner_id, :name], unique: true
      t.index [:repository_id, :owner_id, :name], unique: true
    end
  end
end
