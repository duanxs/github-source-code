# frozen_string_literal: true

class DropIssueMentionsTable < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def up
    drop_table :issue_mentions
  end

  def down
    create_table :issue_mentions do |t|
      t.integer :user_id, null: false
      t.integer :repository_id, null: false
      t.integer :mentionable_id, null: false
      t.string :query_string, limit: 50, null: false
      t.date :mentioned_on, null: false
      t.integer :count, null: false, default: 1

      t.timestamps null: false

      t.index [:user_id, :repository_id, :query_string, :mentioned_on, :mentionable_id], unique: true, name: "index_issue_mentions_on_user_repository_query_date"
      t.index :created_at, name: "index_issue_mentions_on_created_at"
    end
  end
end
