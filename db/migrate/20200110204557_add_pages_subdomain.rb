# frozen_string_literal: true

class AddPagesSubdomain < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Repositories)

  def self.up
    add_column :pages, :subdomain, :string, limit: 63, default: nil, null: true, if_not_exists: true

    add_index :pages, [:subdomain, :repository_id], name: "index_subdomain_on_repository_id", if_not_exists: true
    add_index :pages, :subdomain, unique: true, name: "index_pages_on_subdomain", if_not_exists: true
  end

  def self.down
    remove_column :pages, :subdomain, if_exists: true

    remove_index :pages, name: "index_subdomain_on_repository_id", if_exists: true
    remove_index :pages, name: "index_pages_on_subdomain", if_exists: true
  end
end
