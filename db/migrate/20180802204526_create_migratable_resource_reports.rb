# frozen_string_literal: true

class CreateMigratableResourceReports < GitHub::Migration
  def change
    create_table :migratable_resource_reports do |t|
      t.belongs_to :migration, index: true, null: false
      t.string :model_type, null: false, limit: 50
      t.integer :total_count, null: false
      t.integer :success_count, null: false
      t.integer :failure_count, null: false

      t.timestamps null: false
    end

    add_index :migratable_resource_reports, :model_type
  end
end
