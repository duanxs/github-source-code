# frozen_string_literal: true

class RemoveRepositoryNetworkCacheVersionColumm < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    remove_column :repository_networks,
                  :cache_version,
                  :integer,
                  null: false,
                  default: 0
  end
end
