# frozen_string_literal: true

class CreateIssueTransfers < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :issue_transfers do |t|
      t.belongs_to :old_repository,   null: false
      t.belongs_to :old_issue,        null: false, index: true
      t.integer    :old_issue_number, null: false

      t.belongs_to :new_repository,   null: false
      t.belongs_to :new_issue,        null: false

      t.belongs_to :actor,            null: false
      t.column :state, "varchar(16)", null: false

      t.column :reason, "varbinary(1024)"

      t.timestamps                    null: false
    end

    add_index :issue_transfers, [:old_repository_id, :old_issue_number]
  end
end
