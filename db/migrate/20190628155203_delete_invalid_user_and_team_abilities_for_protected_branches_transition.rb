# frozen_string_literal: true

require "github/transitions/20190628155203_delete_invalid_user_and_team_abilities_for_protected_branches"

class DeleteInvalidUserAndTeamAbilitiesForProtectedBranchesTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::DeleteInvalidUserAndTeamAbilitiesForProtectedBranches.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
