# frozen_string_literal: true

class AddRoleIdToRepoInvite < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_column :repository_invitations, :role_id, :integer
  end
end
