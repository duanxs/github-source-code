# frozen_string_literal: true

class AddCommentHiddenByToDiscussionComment < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :discussion_comments, :comment_hidden_by, :integer, after: :comment_hidden_classifier
  end
end
