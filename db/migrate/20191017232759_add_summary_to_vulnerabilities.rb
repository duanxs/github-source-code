# frozen_string_literal: true

class AddSummaryToVulnerabilities < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Notify)

  def change
    add_column :vulnerabilities, :summary, "VARBINARY(1024)", null: true
    add_column :pending_vulnerabilities, :summary, "VARBINARY(1024)", null: true
  end
end
