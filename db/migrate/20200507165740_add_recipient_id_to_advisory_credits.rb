# frozen_string_literal: true

class AddRecipientIdToAdvisoryCredits < GitHub::Migration
  use_connection_class ApplicationRecord::Collab

  def change
    # The advisory_credits.recipient_id column will replace the user_id column.
    #
    # Ultimately, this column will not allow NULL values but because there are
    # already records in the database, we need to allow NULL until we have run
    # a backfill transition and are double-writing in production.
    add_column :advisory_credits, :recipient_id, :integer, null: true

    # We're beginning to allow NULL values for the user_id column so we're ready
    # to stop writes to this column without an additional migration.
    change_column_null :advisory_credits, :user_id, true

    # These replicate the indexes that involve the existing user_id column.
    # Even though every value in the new recipient_id column is NULL at this
    # point, MySQL will still happily create the unique index below.
    add_index :advisory_credits, [:ghsa_id, :recipient_id], unique: true
    add_index :advisory_credits, [:recipient_id, :accepted_at]
  end
end
