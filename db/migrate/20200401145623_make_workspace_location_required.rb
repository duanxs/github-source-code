# frozen_string_literal: true

class MakeWorkspaceLocationRequired < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    change_column :workspaces, :location, :string, limit: 40, null: false
  end
end
