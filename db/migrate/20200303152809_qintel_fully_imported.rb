# frozen_string_literal: true

class QintelFullyImported < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :compromised_password_datasources, :import_finished_at, :datetime, default: nil
  end
end
