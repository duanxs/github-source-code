# frozen_string_literal: true

class CreateBillingBudgetsTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :billing_budgets do |t|
      t.references :owner, polymorphic: { limit: 12 }, null: false
      t.boolean :enforce_spending_limit, null: false, default: true
      t.integer :spending_limit_in_subunits, null: false, default: 0
      t.string :spending_limit_currency_code, null: false, limit: 3, default: "USD"
      t.column :product, "enum('shared', 'codespaces')", null: false
      t.timestamp :effective_at, null: false, default: -> { "CURRENT_TIMESTAMP" }

      t.timestamps null: false
    end

    add_index :billing_budgets, [:owner_type, :owner_id, :product, :effective_at], unique: true, name: "index_billing_budgets_on_owner_and_product_and_effective_at"
  end
end
