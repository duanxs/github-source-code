# frozen_string_literal: true

class AddTokenScanResultResolution < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Notify)

  def up
    add_column :token_scan_results, :resolution, :integer, null: true, if_not_exists: true
    add_column :token_scan_results, :resolver_id, :integer, null: true, if_not_exists: true
    add_column :token_scan_results, :resolved_at, :datetime, null: true, if_not_exists: true
    add_index :token_scan_results, [:repository_id, :resolution, :created_at], name: "index_token_scan_results_on_repo_and_resolution_and_created_at", if_not_exists: true
  end

  def down
    remove_index :token_scan_results, name: "index_token_scan_results_on_repo_and_resolution_and_created_at", if_exists: true
    remove_column :token_scan_results, :resolution, if_exists: true
    remove_column :token_scan_results, :resolver_id, if_exists: true
    remove_column :token_scan_results, :resolved_at, if_exists: true
  end
end
