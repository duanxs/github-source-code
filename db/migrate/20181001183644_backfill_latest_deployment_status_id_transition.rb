# frozen_string_literal: true

require "github/transitions/20181001183644_backfill_latest_deployment_status_id"

class BackfillLatestDeploymentStatusIdTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::BackfillLatestDeploymentStatusId.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
