# frozen_string_literal: true

class AddBillabilityFieldsToSharedStorageArtifactEvents < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :shared_storage_artifact_events, :billable_owner_type, :string, limit: 12
    add_column :shared_storage_artifact_events, :billable_owner_id, :integer
    add_column :shared_storage_artifact_events, :directly_billed, :boolean, default: true, null: false

    add_index :shared_storage_artifact_events, [:owner_id, :billable_owner_type, :billable_owner_id, :directly_billed, :repository_id, :effective_at, :aggregation_id], name: "index_shared_storage_artifact_events_on_aggregate_fields"
  end
end
