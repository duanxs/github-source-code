# frozen_string_literal: true

class CreateCustomInboxes < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Mysql2)

  def change
    create_table :custom_inboxes do |t|
      t.references :user, null: false
      t.column :name, "VARBINARY(1024)", null: false
      t.column :query_string, "VARBINARY(1024)", null: false
      t.timestamps null: false
    end

    add_index :custom_inboxes, [:user_id, :name], unique: true
  end
end
