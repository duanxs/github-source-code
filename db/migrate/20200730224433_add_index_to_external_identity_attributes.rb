# frozen_string_literal: true

class AddIndexToExternalIdentityAttributes < GitHub::Migration
  def change
    add_index :external_identity_attributes, [:external_identity_id, :name, :value],
    name: "external_identity_attributes_id_name_value_index"
  end
end
