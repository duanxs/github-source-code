# frozen_string_literal: true

class DropMarketplaceListingPlanFromBillingTransactionLineItems < GitHub::Migration
  def up
    remove_index :billing_transaction_line_items, name: :index_on_marketplace_listing_plan_id_and_created_at
    remove_column :billing_transaction_line_items, :marketplace_listing_plan_id
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
