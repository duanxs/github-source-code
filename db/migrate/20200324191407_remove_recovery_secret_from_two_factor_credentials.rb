# frozen_string_literal: true

class RemoveRecoverySecretFromTwoFactorCredentials < GitHub::Migration

  def change
    raise if TwoFactorCredential.where(encrypted_recovery_secret: nil).exists?
    remove_column :two_factor_credentials, :recovery_secret, :string
  end
end
