# frozen_string_literal: true

class AddPlanToWorkspaces < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :workspaces, :plan_id, :int, null: true
  end
end
