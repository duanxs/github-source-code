# frozen_string_literal: true

class UpdateIssueEventIdToBigInt < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Repositories)

  def up
    change_column :issue_events, :id, "bigint(11) unsigned NOT NULL AUTO_INCREMENT"
    change_column :archived_issue_events, :id, "bigint(11) unsigned NOT NULL AUTO_INCREMENT"
    change_column :issue_event_details, :issue_event_id, "bigint(11) unsigned NOT NULL"
    change_column :archived_issue_event_details, :issue_event_id, "bigint(11) unsigned NOT NULL"
  end

  def down
    change_column :issue_events, :id, "int(11) unsigned NOT NULL AUTO_INCREMENT"
    change_column :archived_issue_events, :id, "int(11) unsigned NOT NULL AUTO_INCREMENT"
    change_column :issue_event_details, :issue_event_id, "int(11) unsigned NOT NULL"
    change_column :archived_issue_event_details, :issue_event_id, "int(11) unsigned NOT NULL"
  end
end
