# frozen_string_literal: true

class AddComputedUsageToBillingCodespacesComputeUsageLineItem < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :codespaces_compute_usage_line_items, :computed_usage, :decimal, precision: 16, scale: 5, default: 0, null: false
  end
end
