# frozen_string_literal: true

require "github/transitions/20190107195734_clear_workflow_hashlock"

class ClearWorkflowHashlockTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::ClearWorkflowHashlock.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
