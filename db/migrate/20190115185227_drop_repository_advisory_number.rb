# frozen_string_literal: true

class DropRepositoryAdvisoryNumber < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    remove_column :repository_advisories, :number
  end

  def down
    add_column :repository_advisories, :number, :integer, null: false
  end
end
