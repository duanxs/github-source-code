# frozen_string_literal: true

class AddCommentHiddenByToGistComment < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def change
    add_column :gist_comments, :comment_hidden_by, :integer
    add_column :archived_gist_comments, :comment_hidden_by, :integer
  end
end
