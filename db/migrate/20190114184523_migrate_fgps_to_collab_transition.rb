# frozen_string_literal: true

require "github/transitions/20190114184523_migrate_fgps_to_collab"

class MigrateFgpsToCollabTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    fgp_subject_types.each do |subject_type|
      transition = GitHub::Transitions::MigrateFGPsToCollab.new(dry_run: false, 'subject-type': subject_type)
      transition.perform
    end
  end

  def self.down
  end

  def self.fgp_subject_types
    Organization::Resources.subject_types.map { |p| "#{Organization::Resources::ABILITY_TYPE_PREFIX}/#{p}" } + \
      Repository::Resources.subject_types.map { |p| "#{Repository::Resources::ALL_ABILITY_TYPE_PREFIX}/#{p}" } + \
      Repository::Resources.subject_types.map { |p| "#{Repository::Resources::INDIVIDUAL_ABILITY_TYPE_PREFIX}/#{p}" } + \
      User::Resources.subject_types.map { |p| "#{User::Resources::ABILITY_TYPE_PREFIX}/#{p}" } + \
      Business::Resources.subject_types.map { |p| "#{Business::Resources::ABILITY_TYPE_PREFIX}/#{p}" }
  end
end
