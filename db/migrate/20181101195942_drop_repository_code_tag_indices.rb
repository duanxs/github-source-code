# frozen_string_literal: true

class DropRepositoryCodeTagIndices < GitHub::Migration
  def up
    drop_table :repository_code_tag_indices
  end
end
