# frozen_string_literal: true

class AddIndexUserSessionsAccessedAt < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations for tips

  def change
    add_index :user_sessions, [:accessed_at]
  end
end
