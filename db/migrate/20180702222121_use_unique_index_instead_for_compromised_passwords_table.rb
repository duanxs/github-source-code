# frozen_string_literal: true

class UseUniqueIndexInsteadForCompromisedPasswordsTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    remove_index :compromised_passwords, :sha1_password
    add_index :compromised_passwords, :sha1_password, unique: true
  end
end
