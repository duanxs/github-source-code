# frozen_string_literal: true

class AddIssuesIndexOnRepoIdUserHiddenAndUserId < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_index :issues, [:repository_id, :user_hidden, :user_id]
  end
end
