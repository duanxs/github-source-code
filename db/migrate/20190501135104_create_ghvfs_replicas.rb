# frozen_string_literal: true
class CreateGhvfsReplicas < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def self.up
    create_table :ghvfs_replicas, if_not_exists: true do |t|
      t.belongs_to :repository,          null: false
      t.integer    :ghvfs_fileserver_id, null: false
      t.datetime   :created_at,          null: false
      t.datetime   :updated_at,          null: false
    end

    add_index :ghvfs_replicas, [:ghvfs_fileserver_id, :repository_id], name: "index_ghvfs_replicas_repository", unique: true, if_not_exists: true
  end

  def self.down
    drop_table :ghvfs_replicas, if_exists: true
  end
end
