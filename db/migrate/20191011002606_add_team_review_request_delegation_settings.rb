# frozen_string_literal: true

class AddTeamReviewRequestDelegationSettings < GitHub::Migration
  def change
    add_column :teams, :review_request_delegation_enabled, :boolean, default: false, null: false
    add_column :teams, :review_request_delegation_algorithm, :integer, default: 0
    add_column :teams, :review_request_delegation_member_count, :integer, default: 1
    add_column :teams, :review_request_delegation_notify_team, :boolean, default: true, null: false
  end
end
