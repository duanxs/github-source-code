# frozen_string_literal: true

class AddDeletedAtColumnForIntegrations < GitHub::Migration
  def self.up
    add_column :integrations, :deleted_at, :datetime

    add_index    :integrations, [:owner_id, :deleted_at]
    remove_index :integrations, :owner_id
  end

  def self.down
    add_index    :integrations, :owner_id
    remove_index :integrations, [:owner_id, :deleted_at]

    remove_column :integrations, :deleted_at
  end
end
