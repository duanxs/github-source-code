# frozen_string_literal: true

class RemoveCustomFieldTables < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def self.up
    drop_table :custom_field_entries
    drop_table :archived_custom_field_entries
  end

  def self.down
    create_table :custom_field_entries do |t|
      t.string   :owner_type, null: false, limit: 40
      t.integer  :owner_id, null: false
      t.string   :name, null: false
      t.binary   :value, null: false, limit: 1024
      t.datetime :created_at, null: false
      t.datetime :updated_at, null: false
    end
    add_index :custom_field_entries, [:owner_type, :owner_id]

    create_table :archived_custom_field_entries do |t|
      t.string   :owner_type, null: false, limit: 40
      t.integer  :owner_id, null: false
      t.string   :name, null: false
      t.binary   :value, null: false, limit: 1024
      t.datetime :created_at, null: false
      t.datetime :updated_at, null: false
    end
  end
end
