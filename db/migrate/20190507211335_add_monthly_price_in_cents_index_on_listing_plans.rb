# frozen_string_literal: true

class AddMonthlyPriceInCentsIndexOnListingPlans < GitHub::Migration
  def change
    add_index :marketplace_listing_plans, [:marketplace_listing_id, :monthly_price_in_cents],
      name: "index_listing_plans_on_listing_and_monthly_price_in_cents"
  end
end
