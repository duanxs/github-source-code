# frozen_string_literal: true

class AddSpokenLanguagePreferenceCodeToUsersProfile < GitHub::Migration
  def change
    add_column :profiles, :spoken_language_preference_code, "char(2)"
  end
end
