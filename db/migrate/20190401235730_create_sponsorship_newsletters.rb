# frozen_string_literal: true

class CreateSponsorshipNewsletters < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :sponsorship_newsletters do |t|
      t.integer :maintainer_type, null: false
      t.integer :maintainer_id, null: false
      t.integer :author_id, null: false, index: true
      t.column  :body, :mediumblob, null: false
      t.binary  :subject, limit: 1024, null: false

      t.timestamps null: false
    end

    add_index :sponsorship_newsletters, [:maintainer_type, :maintainer_id], name: "index_sponsorship_newsletters_on_maintainer_type_and_id"
  end
end
