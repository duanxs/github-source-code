# frozen_string_literal: true

class ChangeIssueEventDetailsLabelIdToBigint < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Repositories)

  def up
    change_column :issue_event_details, :label_id, :bigint
    change_column :archived_issue_event_details, :label_id, :bigint
  end

  def down
    change_column :issue_event_details, :label_id, :int
    change_column :archived_issue_event_details, :label_id, :int
  end

end
