# frozen_string_literal: true

class ChangeListingPlanDescriptionToMediumblob < GitHub::Migration
  def up
    change_column :marketplace_listing_plans, :description, :mediumblob, null: false
  end

  def down
    change_column :marketplace_listing_plans, :description, :text, null: false
  end
end
