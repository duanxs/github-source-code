# frozen_string_literal: true

class SpokesFileserversAddSiteColumn < GitHub::Migration
  extend GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Spokes)

  def self.up
    add_column :fileservers, :site, "varchar(20)", null: true, if_not_exists: true
    add_index :fileservers, [:site, :rack], unique: false, if_not_exists: true
  end

  def self.down
    remove_index :fileservers, [:site, :rack], if_exists: true
    remove_column :fileservers, :site, if_exists: true
  end
end
