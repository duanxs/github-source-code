# frozen_string_literal: true

class RemoveDonorFieldsFromSponsorships < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def self.up
    change_column_null :sponsorships, :sponsor_type, false
    change_column_null :sponsorships, :sponsor_id, false

    remove_column :sponsorships, :donor_type, if_exists: true
    remove_column :sponsorships, :donor_id, if_exists: true
    remove_column :sponsorships, :is_donor_opted_in_to_email, if_exists: true
  end

  def self.down
    add_column :sponsorships, :is_donor_opted_in_to_email, :boolean, null: false,
      default: false, if_not_exists: true
    add_column :sponsorships, :donor_id, :int, if_not_exists: true
    add_column :sponsorships, :donor_type, :int, if_not_exists: true

    change_column_null :sponsorships, :sponsor_type, true
    change_column_null :sponsorships, :sponsor_id, true
  end
end
