# frozen_string_literal: true

class DropRepositoryAdvisoryNumberUniqIndex < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    remove_index :repository_advisories, [:repository_id, :number]
  end

  def down
    add_index :repository_advisories, [:repository_id, :number], unique: true
  end
end
