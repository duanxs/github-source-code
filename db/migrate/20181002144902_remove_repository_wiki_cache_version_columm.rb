# frozen_string_literal: true

class RemoveRepositoryWikiCacheVersionColumm < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    remove_column :repository_wikis,
                  :cache_version,
                  :integer,
                  null: false,
                  default: 1
  end
end
