# frozen_string_literal: true

class AddReportToMaintainerToAbuseReports < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :abuse_reports, :show_to_maintainer, :boolean, default: false, null: false
    add_column :abuse_reports, :resolved, :boolean, default: false, null: false
    add_index :abuse_reports, [:repository_id, :show_to_maintainer, :resolved], name: "index_abuse_reports_on_repo_and_show_to_maintainer_and_resolved"
  end
end
