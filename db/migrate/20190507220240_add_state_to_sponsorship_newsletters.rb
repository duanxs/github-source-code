# frozen_string_literal: true

class AddStateToSponsorshipNewsletters < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)
  extend GitHub::SafeDatabaseMigrationHelper

  def self.up
    add_column :sponsorship_newsletters, :state, :int, limit: 2, null: false, default: 0, if_not_exists: true
    remove_index :sponsorship_newsletters, name: "index_sponsorship_newsletters_on_maintainer_type_and_id", if_exists: true
    add_index :sponsorship_newsletters, [:maintainer_type, :maintainer_id, :state], name: "index_sponsorship_newsletters_on_maintainer_and_state", if_not_exists: true
  end

  def self.down
    remove_column :sponsorship_newsletters, :state, if_exists: true
    remove_index :sponsorship_newsletters, name: "index_sponsorship_newsletters_on_maintainer_and_state", if_exists: true
    add_index :sponsorship_newsletters, [:maintainer_type, :maintainer_id], name: "index_sponsorship_newsletters_on_maintainer_type_and_id", if_not_exists: true
  end
end
