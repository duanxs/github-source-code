# frozen_string_literal: true

class AddSourceProductToMigration < GitHub::Migration
  def change
    add_column :migrations, :source_product, "varchar(50)"
  end
end
