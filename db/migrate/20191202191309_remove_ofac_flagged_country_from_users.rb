# frozen_string_literal: true

class RemoveOFACFlaggedCountryFromUsers < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def change
    remove_column :users, :ofac_flagged_country, :integer
  end
end
