# frozen_string_literal: true

class AddSubscriptionItemToSponsorships < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)
  extend GitHub::SafeDatabaseMigrationHelper

  def self.up
    add_column :sponsorships, :subscription_item_id, :integer, null: false, if_not_exists: true
  end

  def self.down
    remove_column :sponsorships, :subscription_item_id, if_exists: true
  end
end
