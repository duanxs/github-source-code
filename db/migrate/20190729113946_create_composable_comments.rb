# frozen_string_literal: true

class CreateComposableComments < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :composable_comments do |t|
      t.references :integration, null: false
      t.references :issue, null: false
      t.integer :position, default: 0, null: false
      t.timestamps null: false
    end

    add_index :composable_comments, [:issue_id]
  end
end
