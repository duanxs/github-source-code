# frozen_string_literal: true

class CreateSpammyNotificationEntries < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Mysql2)

  def self.up
    create_table :spammy_notification_entries, if_not_exists: true do |t|
      t.references :user, null: false
      t.references :summary, null: false
      t.string :list_type, null: false, limit: 64, default: "Repository"
      t.integer :list_id, null: false
      t.string :thread_key, null: false, limit: 80
      t.integer :unread, limit: 1, default: 1
      t.string :reason, limit: 40
      t.datetime :updated_at
      t.datetime :last_read_at
    end

    add_index :spammy_notification_entries, [:user_id, :list_type, :list_id, :thread_key], name: "unique_index_on_user_and_list_and_thread", unique: true, if_not_exists: true
    add_index :spammy_notification_entries, [:list_type, :list_id, :thread_key], name: "index_on_list_and_thread", if_not_exists: true
    add_index :spammy_notification_entries, [:updated_at], name: "index_on_updated_at", if_not_exists: true
  end

  def self.down
    drop_table :spammy_notification_entries
  end
end
