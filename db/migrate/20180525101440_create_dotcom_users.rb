# frozen_string_literal: true

class CreateDotcomUsers < GitHub::Migration
  def change
    create_table :dotcom_users do |t|
      t.integer :user_id
      t.string :token, limit: 65
      t.string :login, limit: 40
      t.datetime :last_contributions_sync
    end

    add_index :dotcom_users, :user_id
    add_index :dotcom_users, :last_contributions_sync
  end
end
