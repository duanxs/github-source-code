# frozen_string_literal: true

class DropFeaturedFromSponsorsMembership < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    remove_column :sponsors_memberships, :featured
  end
end
