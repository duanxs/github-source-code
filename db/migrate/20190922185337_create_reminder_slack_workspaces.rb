# frozen_string_literal: true
class CreateReminderSlackWorkspaces < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :reminder_slack_workspaces do |t|
      t.integer :slack_id, null: false
      t.string :remindable_type, limit: 13, null: false
      t.integer :remindable_id, null: false
      t.column :name, "varbinary(255)", null: false # Slack workspaces can be 255 bytes long and contain Emoji
      t.timestamps null: false

      t.index [:remindable_type, :remindable_id], name: "index_reminder_slack_workspaces_on_remindable_type_and_id"
    end
  end
end
