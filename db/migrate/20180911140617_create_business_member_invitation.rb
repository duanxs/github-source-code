# frozen_string_literal: true

class CreateBusinessMemberInvitation < GitHub::Migration
  def change
    create_table :business_member_invitations do |t|
      t.integer :business_id, null: false
      t.integer :inviter_id, null: false
      t.integer :invitee_id, null: false
      t.integer :role, null: false, default: 0
      t.string :email, null: true
      t.string :normalized_email, null: true
      t.datetime :accepted_at, null: true
      t.datetime :created_at, null: false
      t.datetime :updated_at, null: true
      t.datetime :cancelled_at, null: true
      t.string :hashed_token, null: true, limit: 44
    end

    add_index :business_member_invitations,
      [:business_id, :accepted_at, :cancelled_at, :role, :invitee_id],
      name: "business_id__accepted_at__cancelled_at__role__invitee_id"
  end
end
