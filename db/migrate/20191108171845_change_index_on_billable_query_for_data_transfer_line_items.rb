# frozen_string_literal: true

class ChangeIndexOnBillableQueryForDataTransferLineItems < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    remove_index :package_registry_data_transfer_line_items, name: "index_on_billable_query"
    add_index :package_registry_data_transfer_line_items,
      [
        :billable_owner_type,
        :billable_owner_id,
        :repository_visibility,
        :downloaded_at,
        :downloaded_from,
        :size_in_bytes,
    ],
      name: "index_on_billable_query"
  end

  def down
    remove_index :package_registry_data_transfer_line_items, name: "index_on_billable_query"
    add_index :package_registry_data_transfer_line_items,
      [
        :billable_owner_type,
        :billable_owner_id,
        :repository_visibility,
        :downloaded_from,
        :downloaded_at,
    ],
      name: "index_on_billable_query"
  end
end
