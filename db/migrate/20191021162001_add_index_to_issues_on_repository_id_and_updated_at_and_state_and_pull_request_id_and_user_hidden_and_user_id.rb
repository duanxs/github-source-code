# frozen_string_literal: true

class AddIndexToIssuesOnRepositoryIdAndUpdatedAtAndStateAndPullRequestIdAndUserHiddenAndUserId < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_index :issues, [:repository_id, :updated_at, :state, :pull_request_id, :user_hidden, :user_id], name: "repository_id_and_updated_at_and_state_and_pr_id_and_user"
    remove_index :issues, [:repository_id, :updated_at]
  end
end
