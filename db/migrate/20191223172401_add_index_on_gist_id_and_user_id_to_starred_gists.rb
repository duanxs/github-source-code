# frozen_string_literal: true

class AddIndexOnGistIdAndUserIdToStarredGists < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def up
    remove_index :starred_gists, name: :index_starred_gists_on_gist_id
    add_index :starred_gists, [:gist_id, :user_id], name: :index_starred_gists_on_gist_id_and_user_id
  end

  def down
    remove_index :starred_gists, name: :index_starred_gists_on_gist_id_and_user_id
    add_index :starred_gists, :gist_id, name: :index_starred_gists_on_gist_id
  end
end
