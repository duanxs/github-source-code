# frozen_string_literal: true

class AddBannedContextToSponsorsMemberships < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :sponsors_memberships, :banned_at, :datetime
    add_column :sponsors_memberships, :banned_by_id, :integer
    add_column :sponsors_memberships, :banned_reason, "varbinary(1024)"
  end
end
