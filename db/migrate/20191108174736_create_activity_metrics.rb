# frozen_string_literal: true

class CreateActivityMetrics < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :sponsors_activity_metrics do |t|
      t.timestamps null: false
      t.integer :sponsorable_id, null: false
      t.integer :sponsorable_type, null: false
      t.integer :metric, null: false
      t.integer :value, null: false
      t.date :recorded_on, null: false
    end

    add_index :sponsors_activity_metrics, [:sponsorable_id, :sponsorable_type, :metric, :recorded_on, :value], unique: true, name: "index_metrics_on_sponsorable_and_recordedon_and_metric_and_value"
  end
end
