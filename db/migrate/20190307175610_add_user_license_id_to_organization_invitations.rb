# frozen_string_literal: true

class AddUserLicenseIdToOrganizationInvitations < GitHub::Migration
  def change
    add_column :organization_invitations, :user_license_id, :integer
    add_index :organization_invitations, :user_license_id
  end
end
