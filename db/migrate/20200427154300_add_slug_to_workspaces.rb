# frozen_string_literal: true

class AddSlugToWorkspaces < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :workspaces, :slug, :string, limit: 100, after: :name
    add_index :workspaces, [:slug, :owner_id], unique: true
  end
end
