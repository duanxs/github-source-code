# frozen_string_literal: true

class DropAnnotations < GitHub::Migration
  def change
    drop_table :annotations
  end
end
