# frozen_string_literal: true

class AddIndexOnCreatedAtForScopedIntegrationInstallations < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_index :scoped_integration_installations, :created_at
  end
end
