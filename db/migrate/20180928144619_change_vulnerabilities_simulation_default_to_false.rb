# frozen_string_literal: true

class ChangeVulnerabilitiesSimulationDefaultToFalse < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Notify)

  def up
    change_column_default :vulnerabilities, :simulation, false

    add_index :vulnerabilities, [:status, :platform, :simulation, :updated_at],
      name: "index_on_status_and_platform_and_simulation_and_updated_at"
  end

  def down
    remove_index :vulnerabilities,
      name: "index_on_status_and_platform_and_simulation_and_updated_at"

    change_column_default :vulnerabilities, :simulation, true
  end
end
