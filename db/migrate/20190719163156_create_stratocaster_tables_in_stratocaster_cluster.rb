# frozen_string_literal: true

class CreateStratocasterTablesInStratocasterCluster < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Stratocaster)

  def change
    # This migration doesn't need to run on enterprise because the table
    # will already exist in the right place
    return if GitHub.enterprise?

    create_table :stratocaster_events, id: false do |t|
      t.column :id, "BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY"
      t.mediumblob :raw_data
      t.datetime :updated_at, null: false
    end

    add_index :stratocaster_events, :updated_at
  end
end
