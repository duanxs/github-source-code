# frozen_string_literal: true

class ChangeScienceEventsPayloadToMediumblob < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    change_column :science_events, :payload, :mediumblob
  end
end
