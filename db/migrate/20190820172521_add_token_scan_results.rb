# frozen_string_literal: true

class AddTokenScanResults < GitHub::Migration
  extend GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Notify)

  def self.up
    create_table :token_scan_results do |t|
      t.belongs_to :repository, null: false
      t.timestamps null: false
      t.column :token_type, :string, limit: 64, null: false
      t.column :token_signature, :string, limit: 64, null: false

      t.index [:repository_id, :token_type, :token_signature], unique: true, name: "index_token_scan_results_on_repository_and_type_and_signature"
    end
  end

  def self.down
    drop_table :token_scan_results, if_exists: true
  end
end
