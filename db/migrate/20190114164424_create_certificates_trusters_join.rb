# frozen_string_literal: true

class CreateCertificatesTrustersJoin < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :certificate_trusters do |t|
      t.belongs_to :certificate, index: false, null: false
      t.belongs_to :truster,     index: false, null: false, polymorphic: true

      t.index [:truster_type, :truster_id, :certificate_id], unique: true, name: "index_certificate_trusters_on_truster_and_certificate"
    end
  end
end
