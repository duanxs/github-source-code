# frozen_string_literal: true

class AddMissingLicensingIndices < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    add_index :enterprise_installation_user_account_emails, [:enterprise_installation_user_account_id, :primary, :email], name: :idx_ent_install_ua_emails_on_ent_install_ua_id_primary_email
    remove_index :enterprise_installation_user_account_emails, name: :index_enterprise_installation_user_account_emails_on_account_id
  end

  def down
    add_index :enterprise_installation_user_account_emails, :enterprise_installation_user_account_id, name: :index_enterprise_installation_user_account_emails_on_account_id
    remove_index :enterprise_installation_user_account_emails, name: :idx_ent_install_ua_emails_on_ent_install_ua_id_primary_email
  end
end
