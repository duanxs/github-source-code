# frozen_string_literal: true

class AddActionColumnToCheckSuites < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_column :check_suites, :action, "VARBINARY(400)", null: true
  end
end
