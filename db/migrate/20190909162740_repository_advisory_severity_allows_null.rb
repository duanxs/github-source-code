# frozen_string_literal: true

class RepositoryAdvisorySeverityAllowsNull < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  # severity was defined originally in this migration: db/migrate/20181116151208_create_repository_advisories.rb
  # it has not been updated since
  # Here we want it to allow null
  # more info: https://github.com/github/dsp-security-workflows/issues/770
  def up
    change_column :repository_advisories, :severity,          :integer, null: true
    change_column :repository_advisories, :affected_versions, :blob,    null: true
  end

  def down
    change_column :repository_advisories, :severity,          :integer, null: false
    change_column :repository_advisories, :affected_versions, :blob,    null: false
  end
end
