# frozen_string_literal: true

class CreateMarketplaceContacts < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :marketplace_contacts do |t|
      t.timestamps null: false
      t.integer :listing_id, null: false
      t.integer :user_id, null: false
      t.string :email, null: false
      t.string :name, null: false
      t.string :company, null: false
      t.string :subject
      t.string :title
    end

    add_index :marketplace_contacts, :listing_id
    add_index :marketplace_contacts, :user_id
  end
end
