# frozen_string_literal: true

class AddBillingStartDateToPlanSubscription < GitHub::Migration
  def change
    add_column :plan_subscriptions, :billing_start_date, :date, null: true, default: nil
  end
end
