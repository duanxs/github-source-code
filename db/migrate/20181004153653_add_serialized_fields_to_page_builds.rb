# frozen_string_literal: true

class AddSerializedFieldsToPageBuilds < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    # This is the primary key ID of the User who pushed the code which
    # caused the build to be fired.
    add_column :page_builds, :pusher_id, :integer, null: true

    # This is the SHA-1 of the Git commit which is being used as HEAD to
    # build the site.
    add_column :page_builds, :commit, "char(40)", null: true

    # This is the Page::Build status. There are only a handful of values.
    add_column :page_builds, :status, "varchar(255)", null: true

    # This is the human-readable error message if an error occurred.
    add_column :page_builds, :error, :text, null: true

    # This is a Ruby backtrace of the error.
    add_column :page_builds, :backtrace, :text, null: true

    # This is the duration of the build in milliseconds.
    add_column :page_builds, :duration, :integer, null: true

    # We might want to query on the page_id and commit.
    # SELECT status FROM page_builds WHERE page_id = :page_id AND commit = :commit
    add_index :page_builds, [:page_id, :commit]
  end
end
