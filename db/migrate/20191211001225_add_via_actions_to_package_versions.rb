# frozen_string_literal: true

class AddViaActionsToPackageVersions < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Repositories)

  def up
    add_column :package_versions, :published_via_actions, :boolean, default: false, null: false, if_not_exists: true
  end

  def down
    remove_column :package_versions, :published_via_actions, if_exists: true
  end
end
