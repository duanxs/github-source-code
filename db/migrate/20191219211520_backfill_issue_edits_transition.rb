# frozen_string_literal: true

require "github/transitions/20191219211520_backfill_issue_edits"

class BackfillIssueEditsTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::BackfillIssueEdits.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
