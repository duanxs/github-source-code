# frozen_string_literal: true

class CreateInteractiveComponentInteraction < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :interactive_component_interactions do |t|
      t.references :interactive_component
      t.references :user
      t.datetime :interacted_at, null: false
      t.timestamps null: false
    end

    add_index :interactive_component_interactions, [:interactive_component_id, :user_id], name: "interactive_component_user_interactions_idx"
  end
end
