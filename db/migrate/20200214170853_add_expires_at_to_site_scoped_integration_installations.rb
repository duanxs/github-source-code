# frozen_string_literal: true

class AddExpiresAtToSiteScopedIntegrationInstallations < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :site_scoped_integration_installations, :expires_at, :bigint
    add_index  :site_scoped_integration_installations, :expires_at
  end
end
