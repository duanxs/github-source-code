# frozen_string_literal: true

class CreateMemexProjectColumns < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Memex)

  def change
    create_table :memex_project_columns do |t|
      t.references :memex_project, null: false
      t.column :name, "VARBINARY(255)", null: false
      t.boolean :user_defined, null: false
      t.integer :position, limit: 1, null: false
      t.boolean :visible, null: false
      t.integer :data_type, limit: 1, null: false
      t.json :settings, null: true
      t.references :creator
      t.timestamps null: false
    end
    add_index(
      :memex_project_columns,
      [:memex_project_id, :position],
      unique: true,
      name: "index_memex_project_columns_on_memex_project_id_and_position"
    )
  end
end
