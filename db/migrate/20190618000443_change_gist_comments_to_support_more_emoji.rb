# frozen_string_literal: true

class ChangeGistCommentsToSupportMoreEmoji < GitHub::Migration
  def up
    change_column :gist_comments, :body, :mediumblob
    change_column :archived_gist_comments, :body, :mediumblob
  end

  def down
    change_column :gist_comments, :body, :text
    change_column :archived_gist_comments, :body, :text
  end
end
