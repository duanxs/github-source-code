# frozen_string_literal: true

class CreateDeviceAuthorizationGrants < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :device_authorization_grants, id: false do |t|
      t.column :id, "bigint NOT NULL AUTO_INCREMENT PRIMARY KEY"

      t.boolean :access_denied, default: false, null: false

      t.integer :application_id,   null: false
      t.string  :application_type, null: false, limit: 16 # OauthApplication is the longest type

      t.string :hashed_device_code,     limit: 44, index: { unique: true }
      t.string :device_code_last_eight, limit: 8

      t.text :scopes

      # ABCD-1234
      t.string :user_code, limit: 9, null: false, index: { unique: true }

      t.belongs_to :oauth_access, index: { unique: true }

      t.bigint :expires_at, null: false, index: true

      t.timestamps null: false
    end

    add_index :device_authorization_grants, [:application_id, :application_type],
      name: "index_device_authorization_grants_on_application"
  end
end
