# frozen_string_literal: true

class AddLatestDeploymentStatusColumn < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_column :deployments, :latest_deployment_status_id, :integer
    add_column :archived_deployments, :latest_deployment_status_id, :integer
  end
end
