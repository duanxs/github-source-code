# frozen_string_literal: true

class AddReadmeOptInToProfiles < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Mysql1)

  def change
    add_column :profiles, :readme_opt_in, :boolean, null: false, default: false
  end
end
