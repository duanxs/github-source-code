# frozen_string_literal: true

class AddIndexToMarketplaceOrderPreviewsRetargetingNoticeTriggered < GitHub::Migration
  def change
    add_index :marketplace_order_previews,
      %i[retargeting_notice_triggered_at user_id],
      name: :index_marketplace_order_previews_retargeting_triggered
  end
end
