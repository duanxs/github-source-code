# frozen_string_literal: true

class AddExpiredAtToBusinessMemberInvitations < GitHub::Migration
  def change
    add_column :business_member_invitations, :expired_at, :datetime, after: :cancelled_at, null: true
    add_index :business_member_invitations, :created_at
  end
end
