# frozen_string_literal: true

class AddIndexToComputeForComputedUsageBillableQuery < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_index :codespaces_compute_usage_line_items,
      [:billable_owner_type, :billable_owner_id, :owner_id, :end_time, :sku, :computed_usage],
      name: "index_codespaces_compute_usage_line_items_on_sku_computed_usage"
  end
end
