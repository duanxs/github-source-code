# frozen_string_literal: true

class CreateAbuseReport < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :abuse_reports do |t|
      t.integer :reporting_user_id, null: false
      t.integer :reported_user_id, null: false
      t.integer :reported_content_id, null: false
      t.string :reported_content_type, null: false, limit: 40
      t.integer :repository_id, null: false
      t.string :reported_reason, limit: 30, null: true
      t.timestamps null: false
    end

    add_index :abuse_reports, [:reported_content_id, :reported_content_type, :reported_reason], name: "index_reported_content"
  end
end
