# frozen_string_literal: true

class AddPlanToActionsUsageLineItems < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    add_column :actions_usage_line_items, :plan, :string, limit: 30

    remove_index :actions_usage_line_items, column: :synchronization_batch_id
    add_index :actions_usage_line_items,
      [:synchronization_batch_id, :plan],
      name: "index_actions_usage_line_items_on_synchronization_batch_and_plan"
  end

  def down
    remove_column :actions_usage_line_items, :plan

    add_index :actions_usage_line_items, :synchronization_batch_id
    remove_index :actions_usage_line_items,
      name: "index_actions_usage_line_items_on_synchronization_batch_and_plan"
  end
end
