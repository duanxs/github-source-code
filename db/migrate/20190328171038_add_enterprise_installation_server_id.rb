# frozen_string_literal: true

class AddEnterpriseInstallationServerId < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def self.up
    add_column :enterprise_installations, :server_id, :string, limit: 36, if_not_exists: true # SecureRandom.uuid
    add_index :enterprise_installations, :server_id, unique: true, if_not_exists: true
  end

  def self.down
    remove_index :enterprise_installations, column: [:server_id], if_exists: true
    remove_column :enterprise_installations, :server_id, if_exists: true
  end
end
