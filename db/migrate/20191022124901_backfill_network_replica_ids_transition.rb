# frozen_string_literal: true

require "github/transitions/20191022124901_backfill_network_replica_ids"

class BackfillNetworkReplicaIdsTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::BackfillNetworkReplicaIds.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
