# frozen_string_literal: true

class IndexCrossReferencesOnActorId < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_index(:cross_references, :actor_id)
  end
end
