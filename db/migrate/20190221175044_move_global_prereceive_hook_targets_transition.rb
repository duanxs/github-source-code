# frozen_string_literal: true

require "github/transitions/20190221175044_move_global_prereceive_hook_targets"

class MoveGlobalPrereceiveHookTargetsTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::MoveGlobalPrereceiveHookTargets.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
