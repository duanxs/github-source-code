# frozen_string_literal: true

require "github/transitions/20200124231918_update_follower_threshold_sponsors_criterion"

class UpdateFollowerThresholdSponsorsCriterionTransition < GitHub::Migration
  def self.up
    return unless GitHub.sponsors_enabled?
    return unless Rails.development?

    transition = GitHub::Transitions::UpdateFollowerThresholdSponsorsCriterion.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
