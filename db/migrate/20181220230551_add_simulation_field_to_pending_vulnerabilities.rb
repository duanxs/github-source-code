# frozen_string_literal: true

class AddSimulationFieldToPendingVulnerabilities < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Notify)

  def change
    # simulation denotes a pending vuln used for internal testing purposes
    add_column :pending_vulnerabilities, :simulation, :boolean, default: false, null: false
    # will be searched on so seems useful to have an index on it
    add_index :pending_vulnerabilities,  :simulation, unique: false
    # and also, add the same index to vulnerabilities table, since it has the same search feature and requirements.
    # this keeps the tables as similar as possible
    add_index :vulnerabilities, :simulation, unique: false
  end
end
