# frozen_string_literal: true

class RemoveUnusedActionsLineItemColumns < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    reversible do |dir|
      dir.up do
        remove_index :actions_usage_line_items, name: :index_actions_usage_line_items_on_synchronization_batch_and_plan
      end

      dir.down do
        add_index :actions_usage_line_items, [:synchronization_batch_id, :plan], name: :index_actions_usage_line_items_on_synchronization_batch_and_plan
      end
    end

    remove_column :actions_usage_line_items, :plan, :string, limit: 30
    remove_column :actions_usage_line_items, :billing_type, :string, limit: 20, default: "card"
  end
end
