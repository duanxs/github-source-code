# frozen_string_literal: true

class AddCheckSuiteCancelledAt < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_column :check_suites, :cancelled_at, :datetime
  end
end
