# frozen_string_literal: true

class AddTriggerToWorkflowRuns < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_column :workflow_runs, :trigger_id, :bigint, null: true
    add_column :workflow_runs, :trigger_type, :string, limit: 30, null: true
  end
end
