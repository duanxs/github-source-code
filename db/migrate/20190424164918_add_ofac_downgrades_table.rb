# frozen_string_literal: true

class AddOFACDowngradesTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :ofac_downgrades do |t|
      t.integer :user_id, null: false, index: true
      t.date :downgrade_on, index: true
      t.boolean :is_complete, null: false, default: false
      t.timestamps null: false
    end
  end
end
