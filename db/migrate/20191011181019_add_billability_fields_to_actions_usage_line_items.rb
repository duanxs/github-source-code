# frozen_string_literal: true

class AddBillabilityFieldsToActionsUsageLineItems < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :actions_usage_line_items, :billable_owner_type, :string, limit: 12
    add_column :actions_usage_line_items, :billable_owner_id, :integer
    add_column :actions_usage_line_items, :directly_billed, :boolean, default: true, null: false
  end
end
