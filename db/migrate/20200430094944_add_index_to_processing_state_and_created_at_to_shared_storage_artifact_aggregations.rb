# frozen_string_literal: true

class AddIndexToProcessingStateAndCreatedAtToSharedStorageArtifactAggregations < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_index :shared_storage_artifact_aggregations, [:submission_state, :created_at], name: "index_on_submission_state_and_created_at"
  end
end
