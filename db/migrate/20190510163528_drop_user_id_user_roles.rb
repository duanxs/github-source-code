# frozen_string_literal: true

class DropUserIdUserRoles < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Iam)

  def up
    remove_index :user_roles, name: "idx_user_roles_user_id_role_id_and_target"
    remove_column :user_roles, :user_id
  end

  def down
    add_column :user_roles, :user_id, :integer, null: false
    add_index :user_roles, [:user_id, :role_id, :target_id, :target_type], name: "idx_user_roles_user_id_role_id_and_target"
  end
end
