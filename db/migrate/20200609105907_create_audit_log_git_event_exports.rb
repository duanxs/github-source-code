# frozen_string_literal: true

class CreateAuditLogGitEventExports < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :audit_log_git_event_exports do |t|
      t.integer  :actor_id, null: false
      t.integer  :subject_id, null: false
      t.column   :subject_type, :string, limit: 255
      t.column   :token, :string, null: false, limit: 255, index: { unique: true }
      t.datetime :start, null: false
      t.datetime :end, null: false
      t.datetime :created_at, null: false
      t.datetime :updated_at, null: false
    end

    add_index :audit_log_git_event_exports, [:actor_id, :token]
  end
end
