# frozen_string_literal: true

class AddActionsUsageLineItemsRepositoryVisibilityDefault < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    change_column_default :actions_usage_line_items, :repository_visibility, from: nil, to: "PRIVATE"
  end
end
