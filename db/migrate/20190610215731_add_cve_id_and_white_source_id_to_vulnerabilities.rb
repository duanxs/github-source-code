# frozen_string_literal: true

class AddCveIdAndWhiteSourceIdToVulnerabilities < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Notify)

  def change
    add_column :vulnerabilities, :cve_id, :string, null: true, limit: 20
    add_index :vulnerabilities, :cve_id, unique: false

    add_column :vulnerabilities, :white_source_id, :string, null: true, limit: 20
    add_index :vulnerabilities, :white_source_id, unique: false
  end
end
