# frozen_string_literal: true

class CreateMarketplaceCategoriesRepositoryActionsJoin < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    create_table :marketplace_categories_repository_actions do |t|
      t.belongs_to :marketplace_category, index: false, null: false
      t.belongs_to :repository_action, index: false, null: false
    end

    add_index :marketplace_categories_repository_actions,
      :repository_action_id,
      name: :index_marketplace_categories_repository_actions_on_action_id

    add_index :marketplace_categories_repository_actions,
      %i(marketplace_category_id repository_action_id),
      unique: true,
      name:   :index_marketplace_categories_repository_actions_on_ids
  end
end
