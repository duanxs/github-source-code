# frozen_string_literal: true

class RemoveSecretFromOauthApplications < GitHub::Migration

  def change
    raise if OauthApplication.where(encrypted_secret: nil).exists?
    remove_column :oauth_applications, :secret, :string
  end
end
