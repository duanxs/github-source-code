# frozen_string_literal: true

class CreateKeyLinks < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def self.up
    create_table :key_links do |t|
      t.string     :key_prefix, limit: 15,    null: false
      t.text       :url_template,             null: false
      t.belongs_to :owner, polymorphic: true, null: false
    end
    add_index :key_links, [:owner_id, :owner_type, :key_prefix], unique: true
  end

  def self.down
    drop_table :key_links
  end
end
