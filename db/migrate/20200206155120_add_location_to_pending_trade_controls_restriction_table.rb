# frozen_string_literal: true

class AddLocationToPendingTradeControlsRestrictionTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def up
    execute <<-SQL
        ALTER TABLE pending_trade_controls_restrictions MODIFY reason enum('organization_admin', 'organization_billing_manager', 'organization_member', 'organization_outside_collaborator', 'location') NOT NULL;
    SQL
  end

  def down
    execute <<-SQL
        ALTER TABLE pending_trade_controls_restrictions MODIFY reason enum('organization_admin', 'organization_billing_manager', 'organization_member', 'organization_outside_collaborator') NOT NULL;
    SQL
  end
end
