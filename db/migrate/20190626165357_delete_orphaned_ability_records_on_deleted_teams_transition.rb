# frozen_string_literal: true

require "github/transitions/20190626165357_delete_orphaned_ability_records_on_deleted_teams"

class DeleteOrphanedAbilityRecordsOnDeletedTeamsTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::DeleteOrphanedAbilityRecordsOnDeletedTeams.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
