# frozen_string_literal: true

class AddNullConstraintToRepoAdvisoryGhsaId < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    change_column_null :repository_advisories, :ghsa_id, false
  end
end
