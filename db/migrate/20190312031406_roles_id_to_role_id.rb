# frozen_string_literal: true

class RolesIdToRoleId < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Iam)

  def change
    rename_column :user_roles, :roles_id, :role_id
  end
end
