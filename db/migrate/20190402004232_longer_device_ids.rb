# frozen_string_literal: true

class LongerDeviceIds < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    remove_index :authenticated_devices, name: :index_authenticated_devices_on_user_id_device_id_and_approved_at
    add_index :authenticated_devices, [:user_id, :device_id], unique: true
  end
end
