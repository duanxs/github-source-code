# frozen_string_literal: true

class AddSourceToCloseIssueReferences < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :close_issue_references, :source, :integer, null: false, default: 0
  end
end
