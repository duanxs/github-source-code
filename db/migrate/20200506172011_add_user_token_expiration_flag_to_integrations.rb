# frozen_string_literal: true

class AddUserTokenExpirationFlagToIntegrations < GitHub::Migration
  def change
    add_column :integrations, :user_token_expiration, :boolean, default: false, null: false
  end
end
