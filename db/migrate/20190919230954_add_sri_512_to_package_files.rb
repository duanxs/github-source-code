# frozen_string_literal: true

class AddSri512ToPackageFiles < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_column :package_files, :sri_512, :string, limit: 100
  end
end
