# frozen_string_literal: true

class CreateSponsorsCriteria < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :sponsors_criteria do |t|
      t.string  :slug, limit: 60, null: false, index: { unique: true }
      t.blob    :description, null: false
      t.integer :criterion_type, null: false, default: 0, index: true
      t.boolean :automated, default: false, null: false

      t.timestamps null: false
    end
  end
end
