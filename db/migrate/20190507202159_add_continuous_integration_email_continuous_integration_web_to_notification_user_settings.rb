# frozen_string_literal: true

class AddContinuousIntegrationEmailContinuousIntegrationWebToNotificationUserSettings < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  self.use_connection_class(ApplicationRecord::Mysql2)

  def change
    add_column :notification_user_settings, :continuous_integration_email, :boolean, default: true, null: false
    add_column :notification_user_settings, :continuous_integration_web, :boolean, default: false, null: false
  end
end
