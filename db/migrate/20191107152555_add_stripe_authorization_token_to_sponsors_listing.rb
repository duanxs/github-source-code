# frozen_string_literal: true

class AddStripeAuthorizationTokenToSponsorsListing < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    add_column :sponsors_listings, :stripe_authorization_code, :string, limit: 40
    add_index :sponsors_listings, :stripe_authorization_code
  end

  def down
    remove_index :sponsors_listings, :stripe_authorization_code
    remove_column :sponsors_listings, :stripe_authorization_code
  end
end
