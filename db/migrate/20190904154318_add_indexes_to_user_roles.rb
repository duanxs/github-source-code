# frozen_string_literal: true

class AddIndexesToUserRoles < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Iam)

  def change
    add_index :user_roles, [:target_id, :target_type]
    add_index :user_roles, [:role_id, :actor_id, :actor_type]
    add_index :user_roles, [:role_id, :target_id, :target_type, :actor_type, :actor_id], name: :index_user_roles_on_role_target_type_actor
  end
end
