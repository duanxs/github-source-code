# frozen_string_literal: true

class AddSourceLoginAndMigrationToUser < GitHub::Migration
  def change
    add_column :users, :source_login, "varchar(40)"
    add_column :users, :migration_id, :integer

    add_index :users, :migration_id
  end
end
