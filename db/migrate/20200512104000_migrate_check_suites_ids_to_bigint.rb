# frozen_string_literal: true

class MigrateCheckSuitesIdsToBigint < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def up
    change_column :check_suites, :id, "bigint(11) unsigned NOT NULL AUTO_INCREMENT"
  end

  def down
    change_column :check_suites, :id, "int(11) NOT NULL AUTO_INCREMENT"
  end
end
