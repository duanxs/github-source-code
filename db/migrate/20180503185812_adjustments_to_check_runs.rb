# frozen_string_literal: true

class AdjustmentsToCheckRuns < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def self.up
    remove_index :check_runs, [:check_id, :created_at]
    remove_column :check_runs, :check_id

    remove_column :check_runs, :performed_via_app_id

    change_column :check_runs, :check_suite_id, :integer, null: false
    add_index :check_runs, [:check_suite_id, :created_at]
  end
end
