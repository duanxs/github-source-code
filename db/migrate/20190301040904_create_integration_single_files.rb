# frozen_string_literal: true

class CreateIntegrationSingleFiles < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :integration_single_files do |t|
      t.belongs_to :integration_version, null: false
      t.string     :path,                null: false

      t.timestamps null: false
    end

    add_index :integration_single_files, [:integration_version_id, :path],
      unique: true,
      name: "index_on_integration_version_id_and_path"
  end
end
