# frozen_string_literal: true

class RemoveParentFromScopedIntegrationInstallations < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    remove_columns :scoped_integration_installations, :parent_id, :parent_type
  end
end
