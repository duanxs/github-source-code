# frozen_string_literal: true

class EmojiSupportInCheckRunTitles < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def self.up
    change_column :check_runs, :title, "varbinary(1024)"
  end

  def self.down
    change_column :check_runs, :title, :string
  end
end
