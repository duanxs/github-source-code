# frozen_string_literal: true

class DropAvatarUpgradesTable < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def change
    drop_table :avatar_upgrades do |t|
      t.references :user, null: true, index: { unique: true }
      t.datetime :created_at, null: false
      t.integer :owner_id, null: true
      t.string :owner_type, null: true
      t.integer :retry_count, null: false, default: 0
      t.boolean :upgraded, null: false, default: true

      t.index [:owner_id, :owner_type], unique: true
    end
  end
end
