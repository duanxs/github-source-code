# frozen_string_literal: true

class AddAllowForcePushAndDeletionColumns < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_column :protected_branches, :allow_force_pushes_enforcement_level, :integer, null: false, default: "0", if_not_exists: true
    add_column :archived_protected_branches, :allow_force_pushes_enforcement_level, :integer, null: false, default: "0", if_not_exists: true
    add_column :protected_branches, :allow_deletions_enforcement_level, :integer, null: false, default: "0", if_not_exists: true
    add_column :archived_protected_branches, :allow_deletions_enforcement_level, :integer, null: false, default: "0", if_not_exists: true
  end
end
