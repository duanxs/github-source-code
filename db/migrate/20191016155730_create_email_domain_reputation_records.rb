# frozen_string_literal: true

class CreateEmailDomainReputationRecords < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :email_domain_reputation_records do |t|
      t.string :email_domain, null: false
      t.string :mx_exchange
      t.string :a_record
      t.integer :sample_size, null: false, default: 0
      t.integer :not_spammy_sample_size, null: false, default: 0

      t.timestamps null: false

      t.index [:email_domain], name: :index_email_domain_reputation_records_on_email_domain
      t.index [:mx_exchange], name: :index_email_domain_reputation_records_on_mx_exchange
      t.index [:a_record, :mx_exchange, :email_domain],
        name: :index_email_domain_reputation_records_on_a_record,
        unique: true
    end
  end
end
