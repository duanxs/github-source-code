# frozen_string_literal: true

class UserRolesIndexUserAndTargets < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Iam)

  def change
    add_index :user_roles, [:user_id, :role_id, :target_id, :target_type], name: "idx_user_roles_user_id_role_id_and_target"
  end
end
