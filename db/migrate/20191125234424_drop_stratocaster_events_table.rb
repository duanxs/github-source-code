# frozen_string_literal: true

class DropStratocasterEventsTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Mysql2)

  def up
    return if GitHub.enterprise? # DB never moved in enterprise

    drop_table :stratocaster_events
  end

  def down
    return if GitHub.enterprise? # DB never moved in enterprise

    create_table :stratocaster_events, id: false do |t|
      t.column :id, "BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY"
      t.mediumblob :raw_data
      t.datetime :updated_at, null: false
    end

    add_index :stratocaster_events, :updated_at
  end
end
