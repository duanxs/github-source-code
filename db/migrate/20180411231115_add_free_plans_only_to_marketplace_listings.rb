# frozen_string_literal: true

class AddFreePlansOnlyToMarketplaceListings < GitHub::Migration
  def change
    add_column :marketplace_listings, :free_plans_only, :boolean, null: false, default: false
  end
end
