# frozen_string_literal: true

class AddCheckAnnotationCheckSuiteId < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_column :check_annotations, :check_suite_id, :integer, null: true

    add_index :check_annotations, :check_suite_id
  end
end
