# frozen_string_literal: true

class AddUaToAuthenticationRecords < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_column :authentication_records, :user_agent, :text
  end
end
