# frozen_string_literal: true

class AddOutdatedAtToInteractiveComponents < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :interactive_components, :outdated_at, :datetime, null: true
    add_index :interactive_components, :outdated_at
  end
end
