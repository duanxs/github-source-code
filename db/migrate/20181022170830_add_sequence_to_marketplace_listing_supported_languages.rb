# frozen_string_literal: true

class AddSequenceToMarketplaceListingSupportedLanguages < GitHub::Migration
  def change
    add_column :marketplace_listing_supported_languages, :sequence, :integer
  end
end
