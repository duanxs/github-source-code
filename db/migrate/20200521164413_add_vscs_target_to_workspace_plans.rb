# frozen_string_literal: true

class AddVscsTargetToWorkspacePlans < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    remove_index :workspace_plans, name: :index_on_owner_and_resource_group
    add_column :workspace_plans, :vscs_target, :string, limit: 21
    add_index :workspace_plans, [:owner_id, :owner_type, :resource_group_id, :vscs_target],
      name: :index_on_owner_rg_and_target,
      unique: true
  end

  def down
    remove_index :workspace_plans, name: :index_on_owner_rg_and_target
    remove_column :workspace_plans, :vscs_target
    add_index :workspace_plans, [:owner_id, :owner_type, :resource_group_id],
      name: :index_on_owner_and_resource_group,
      unique: true
  end
end
