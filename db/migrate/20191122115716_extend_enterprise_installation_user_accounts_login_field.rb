# frozen_string_literal: true

class ExtendEnterpriseInstallationUserAccountsLoginField < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    change_column :enterprise_installation_user_accounts, :login, :string, limit: 255, null: false
  end

  def down
    change_column :enterprise_installation_user_accounts, :login, :string, limit: 40, null: false
  end
end
