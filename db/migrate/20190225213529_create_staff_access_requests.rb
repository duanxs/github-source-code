# frozen_string_literal: true

class CreateStaffAccessRequests < GitHub::Migration
  def change
    create_table :staff_access_requests do |t|
        t.integer :requested_by_id, null: false
        t.string :reason, null: false
        t.datetime :expires_at, null: false
        t.datetime :cancelled_at
        t.integer :cancelled_by_id
        t.datetime :denied_at
        t.integer :denied_by_id

        t.timestamps null: false
    end

    add_index :staff_access_requests, :requested_by_id
    add_reference :staff_access_requests, :accessible, polymorphic: true, index: true
  end
end
