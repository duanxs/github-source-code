# frozen_string_literal: true

class AddConfigAsCodeColumnsToTokenScanResultLocations < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Notify)

  def change
    add_column :token_scan_result_locations, :ignore_token, :integer, default: 0, null: false
    add_column :token_scan_result_locations, :blob_paths_processed, :boolean, default: false, null: false
  end
end
