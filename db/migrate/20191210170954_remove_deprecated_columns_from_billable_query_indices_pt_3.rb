# frozen_string_literal: true

class RemoveDeprecatedColumnsFromBillableQueryIndicesPt3 < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    reversible do |dir|
      dir.up do
        remove_index :actions_usage_line_items, name: "index_on_billable_owner_and_usage_v2"
        add_index :actions_usage_line_items,
          [:billable_owner_type, :billable_owner_id, :end_time, :job_runtime_environment, :duration_in_milliseconds, :duration_multiplier],
          name: "index_on_billable_owner_and_usage_v2"
      end

      dir.down do
        remove_index :actions_usage_line_items, name: "index_on_billable_owner_and_usage_v2"
        add_index :actions_usage_line_items,
          [:billable_owner_type, :billable_owner_id, :self_hosted, :end_time, :job_runtime_environment, :duration_in_milliseconds, :duration_multiplier],
          name: "index_on_billable_owner_and_usage_v2"
      end
    end
  end
end
