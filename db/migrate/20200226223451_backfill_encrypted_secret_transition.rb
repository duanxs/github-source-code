# frozen_string_literal: true

require "github/transitions/20200226223304_backfill_encrypted_attribute"

class BackfillEncryptedSecretTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::BackfillEncryptedAttribute.new(
      dry_run: false,
      model: OauthApplication.name,
      source: :secret,
      destination: :encrypted_secret,
    )
    transition.perform
  end

  def self.down
  end
end
