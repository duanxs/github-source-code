# frozen_string_literal: true

class CreateIssueLinks < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Domain::Repositories)

  def change
    create_table :issue_links do |t|
      t.references :source_issue, null: false
      t.references :target_issue, null: false
      t.references :actor, null: false
      t.integer :link_type, null: false
      t.integer :source_repository_id, null: false
      t.integer :target_repository_id, null: false

      t.timestamps null: false
    end

    add_index :issue_links, [:source_issue_id, :target_issue_id, :link_type],
      name: "index_issue_links_source_target_type", unique: true
    add_index :issue_links, [:source_issue_id, :target_issue_id, :link_type, :created_at],
      name: "index_issue_links_source_target_type_and_created_at"
  end
end
