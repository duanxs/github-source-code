# frozen_string_literal: true

class AddCreatorIdAndDeclinedAtToAdvisoryCredits < GitHub::Migration
  use_connection_class ApplicationRecord::Collab

  def change
    change_table :advisory_credits do |t|
      t.integer :creator_id, null: false
      t.datetime :declined_at
    end
  end
end
