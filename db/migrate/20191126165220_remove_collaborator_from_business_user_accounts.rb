# frozen_string_literal: true

class RemoveCollaboratorFromBusinessUserAccounts < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    remove_column :business_user_accounts, :collaborator
  end

  def down
    add_column :business_user_accounts, :collaborator, :boolean, default: false, null: false
  end
end
