# frozen_string_literal: true

class AddOwnerToRepositoryAdvisory < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :repository_advisories, :owner_id, :integer
    add_index :repository_advisories, [:owner_id, :workspace_repository_id],
      name: "index_repository_advisories_on_owner_and_workspace_repository"
  end
end
