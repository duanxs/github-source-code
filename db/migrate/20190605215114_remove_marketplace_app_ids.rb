# frozen_string_literal: true

class RemoveMarketplaceAppIds < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def up
    remove_column :marketplace_listings, :integration_id
    remove_column :marketplace_listings, :oauth_application_id
  end

  def down
    add_reference :marketplace_listings, :integration
    add_reference :marketplace_listings, :oauth_application
  end
end
