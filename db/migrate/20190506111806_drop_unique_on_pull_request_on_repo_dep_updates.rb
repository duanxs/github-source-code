# frozen_string_literal: true

class DropUniqueOnPullRequestOnRepoDepUpdates < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Notify)

  def up
    remove_index :repository_dependency_updates, :pull_request_id
    add_index :repository_dependency_updates, :pull_request_id
  end

  def down
    remove_index :repository_dependency_updates, :pull_request_id
    add_index :repository_dependency_updates, :pull_request_id, unique: true
  end
end
