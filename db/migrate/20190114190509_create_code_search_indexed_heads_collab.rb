# frozen_string_literal: true

class CreateCodeSearchIndexedHeadsCollab < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :code_search_indexed_heads, id: false do |t|
      t.column :id, "bigint NOT NULL AUTO_INCREMENT PRIMARY KEY"
      t.string :commit_oid, null: false, limit: 40
      t.column :ref, "VARBINARY(767)", null: false
      t.references :repository, null: false
      t.timestamps null: false
    end

    add_index :code_search_indexed_heads, [:repository_id, :ref],
      name: "index_code_search_indexed_heads_on_repository_ref",
      unique: true
  end
end
