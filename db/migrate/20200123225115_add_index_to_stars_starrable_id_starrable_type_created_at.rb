# frozen_string_literal: true

class AddIndexToStarsStarrableIdStarrableTypeCreatedAt < GitHub::Migration
  def change
    add_index(:stars, [:starrable_id, :starrable_type , :created_at, :user_id, :user_hidden], name: "index_stars_on_starrable_id_starrable_type_created_at_user_id")
  end
end
