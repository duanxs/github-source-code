# frozen_string_literal: true

class AddOrderUrlToPageCertificates < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/database-migrations-for-dotcom for tips
  self.use_connection_class(ApplicationRecord::Repositories)

  def up
    add_column :page_certificates, :order_url, :text, default: nil, null: true, if_not_exists: true
  end

  def down
    remove_column :page_certificates, :order_url, if_exists: true
  end
end
