# frozen_string_literal: true

class CreateReviewRequestDelegationExcludedMembers < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :review_request_delegation_excluded_members do |t|
      t.integer :user_id, null: false
      t.integer :team_id, null: false

      t.index [:team_id, :user_id], unique: true, name: "excluded_members_team_user_unique"
    end
  end
end
