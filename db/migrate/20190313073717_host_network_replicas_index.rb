# frozen_string_literal: true

class HostNetworkReplicasIndex < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Spokes)

  def change
    add_index :network_replicas, [:host, :state, :network_id], if_not_exists: true
    remove_index :network_replicas, column: [:host, :state], if_exists: true
  end
end
