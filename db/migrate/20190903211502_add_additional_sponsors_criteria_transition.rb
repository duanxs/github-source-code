# frozen_string_literal: true

require "github/transitions/20190903211502_add_additional_sponsors_criteria"

class AddAdditionalSponsorsCriteriaTransition < GitHub::Migration
  def self.up
    return unless GitHub.sponsors_enabled?
    return unless Rails.development?

    transition = GitHub::Transitions::AddAdditionalSponsorsCriteria.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
