# frozen_string_literal: true

class AddIndexToRolesTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Iam)

  def change
    add_index :roles, [:name, :owner_id, :owner_type], unique: true
  end
end
