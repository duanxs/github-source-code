# frozen_string_literal: true

class AddIndexToStatusAndCreatedAtOnRepositoryDependencyUpdates < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Notify)

  def change
    add_index :repository_dependency_updates, [:state, :created_at]
  end
end
