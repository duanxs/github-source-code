# frozen_string_literal: true

class IndexUserLicensesOnBusinessIdLicenseType < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_index :user_licenses, [:business_id, :license_type]
  end
end
