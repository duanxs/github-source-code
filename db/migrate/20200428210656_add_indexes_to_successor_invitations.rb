# frozen_string_literal: true

class AddIndexesToSuccessorInvitations < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_index :successor_invitations, :accepted_at
    add_index :successor_invitations, :declined_at
    add_index :successor_invitations, :canceled_at
  end
end
