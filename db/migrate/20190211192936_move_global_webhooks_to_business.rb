# frozen_string_literal: true

require "github/transitions/20190211175055_move_global_webhooks_to_business"

class MoveGlobalWebhooksToBusiness < GitHub::Migration
  def self.up
    return if !GitHub.single_business_environment? && !Rails.development?
    transition = GitHub::Transitions::MoveGlobalWebhooksToBusiness.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
