# frozen_string_literal: true

class AddErrorTitleToRepositoryDependencyUpdate < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Notify)

  def change
    add_column :repository_dependency_updates, :error_title, :binary, limit: 1024, null: true
  end
end
