# frozen_string_literal: true

class DropLinguistSamplesTable < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def up
    drop_table :linguist_samples
  end

  def down
    create_table :linguist_samples do |t|
      t.string :query, null: true
      t.string :s3_url, null: true
      t.integer :user_id, null: true

      t.timestamps null: true
    end
  end
end
