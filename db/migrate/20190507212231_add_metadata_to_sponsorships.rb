# frozen_string_literal: true

class AddMetadataToSponsorships < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :sponsorships, :maintainer_notes, :text
    add_column :sponsorships, :is_sponsor_tier_reward_fulfilled, :boolean, null: false, default: false

    add_index :sponsorships, :is_sponsor_tier_reward_fulfilled
  end
end
