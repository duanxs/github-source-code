# frozen_string_literal: true

class DropMarketplaceContactTable < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/database-migrations-for-dotcom for tips
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    drop_table :marketplace_contacts
  end
end
