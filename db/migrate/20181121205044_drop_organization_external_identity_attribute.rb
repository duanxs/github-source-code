# frozen_string_literal: true

class DropOrganizationExternalIdentityAttribute < GitHub::Migration
  def self.up
    drop_table :organization_external_identity_attributes, if_exists: true
  end

  def self.down
    create_table :organization_external_identity_attributes, if_not_exists: true do |t|
      t.belongs_to :organization_external_identity, null: false
      t.string     :scheme, limit: 4, null: false
      t.string     :name, null: false
      t.string     :value, null: false
      t.text       :metadata_json
    end

    add_index :organization_external_identity_attributes,
                            [:organization_external_identity_id, :scheme, :name, :value],
                            name: :index_attributes_on_external_identity_name_and_value,
                            unique: true, if_not_exists: true
  end
end
