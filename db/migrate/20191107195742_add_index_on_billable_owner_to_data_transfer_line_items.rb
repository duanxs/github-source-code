# frozen_string_literal: true

class AddIndexOnBillableOwnerToDataTransferLineItems < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_index :package_registry_data_transfer_line_items,
      [
        :billable_owner_type,
        :billable_owner_id,
        :repository_visibility,
        :downloaded_from,
        :downloaded_at,
    ],
      name: "index_on_billable_query"
  end
end
