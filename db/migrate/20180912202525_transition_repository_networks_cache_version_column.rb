# frozen_string_literal: true

require "github/transitions/20180817113900_backfill_repository_networks_cache_version_number.rb"

class TransitionRepositoryNetworksCacheVersionColumn < GitHub::Migration
  def self.up
    return unless GitHub.enterprise? || Rails.development?
    GitHub::Transitions::BackfillRepositoryNetworksCacheVersionNumber.new(dry_run: false).perform
  end

  def self.down
  end
end
