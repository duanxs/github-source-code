# frozen_string_literal: true

class AuthRecordJoinQuery < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_index :authentication_records,
      [:authenticated_device_id, :ip_address, :user_id],
      name: :index_authentication_records_ondevice_id_ip_address_and_user_id
  end
end
