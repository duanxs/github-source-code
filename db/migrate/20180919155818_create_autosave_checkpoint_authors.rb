# frozen_string_literal: true

class CreateAutosaveCheckpointAuthors < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :autosave_checkpoint_authors do |t|
      t.integer  :autosave_checkpoint_id, null: false
      t.belongs_to  :author, null: false
    end
    add_index :autosave_checkpoint_authors, [:autosave_checkpoint_id]
    add_index :autosave_checkpoint_authors, [:author_id]
  end
end
