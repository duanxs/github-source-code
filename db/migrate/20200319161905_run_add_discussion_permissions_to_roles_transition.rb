# frozen_string_literal: true

require "github/transitions/20200319154902_add_discussion_permissions_to_roles"

class RunAddDiscussionPermissionsToRolesTransition < GitHub::Migration
  def up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::AddDiscussionPermissionsToRoles.new(dry_run: false)
    transition.perform
  end

  def down
    # no-op
  end
end
