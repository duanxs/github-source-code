# frozen_string_literal: true

class AddHasDiscussionOnRepository < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_column :repositories, :has_discussions, :boolean, default: false, null: false, after: :has_wiki
    add_column :archived_repositories, :has_discussions, :boolean, default: false, null: false, after: :has_wiki
  end
end
