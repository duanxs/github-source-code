# frozen_string_literal: true

class AddIndexToProcessingStateAndCreatedAtToActionsUsageLineItems < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_index :actions_usage_line_items, [:submission_state, :created_at], name: "index_on_submission_state_and_created_at"
  end
end
