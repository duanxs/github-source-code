# frozen_string_literal: true

class DropIssuesVotes < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    remove_column :issues, :votes, :integer, default: 0
  end
end
