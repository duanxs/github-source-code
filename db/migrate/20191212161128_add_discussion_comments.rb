# frozen_string_literal: true

class AddDiscussionComments < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :discussion_comments do |t|
      t.belongs_to :discussion, null: false
      t.belongs_to :user, null: false
      t.column :body, :mediumblob, null: false
      t.boolean :user_hidden, null: false, default: false
      t.boolean :comment_hidden, null: false, default: false
      t.column :comment_hidden_reason, "VARBINARY(1024)"
      t.string :comment_hidden_classifier, limit: 50
      t.timestamps null: false
    end

    add_index :discussion_comments, [:user_id, :user_hidden]
    add_index :discussion_comments, [:discussion_id, :created_at]
    add_index :discussion_comments, [:user_hidden, :discussion_id]
  end
end
