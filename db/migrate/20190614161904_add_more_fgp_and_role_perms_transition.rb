# frozen_string_literal: true

require "github/transitions/20190614161904_add_more_fgp_and_role_perms"

class AddMoreFgpAndRolePermsTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::AddMoreFgpAndRolePerms.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
