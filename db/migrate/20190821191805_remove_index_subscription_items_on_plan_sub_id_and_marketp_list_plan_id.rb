# frozen_string_literal: true

class RemoveIndexSubscriptionItemsOnPlanSubIdAndMarketpListPlanId < GitHub::Migration
  def up
    remove_index :subscription_items, name: :index_subscription_items_on_plan_sub_id_and_marketp_list_plan_id
    add_index :subscription_items, [:plan_subscription_id, :marketplace_listing_plan_id], name: :index_subscription_items_on_plan_sub_and_list_plan, unique: false
  end

  def down
    remove_index :subscription_items, name: :index_subscription_items_on_plan_sub_and_list_plan
    add_index :subscription_items, [:plan_subscription_id, :marketplace_listing_plan_id], name: :index_subscription_items_on_plan_sub_id_and_marketp_list_plan_id, unique: true
  end
end
