# frozen_string_literal: true

class AdditionalInfoForArtifactsInSharedStorage < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :shared_storage_artifact_events, :source_artifact_id, :int, null: true
  end
end
