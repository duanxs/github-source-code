# frozen_string_literal: true

class CreateCodespaceBillingMessages < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :codespace_billing_messages do |t|
      t.references :codespace_plan, null: false, index: { name: "idx_codespace_billing_message_on_codespace_plan" }
      t.string :azure_storage_account_name, limit: 24, null: false
      t.string :message_id, limit: 36, null: false
      t.json :payload, null: false

      t.timestamps null: false
    end

    add_index :codespace_billing_messages, [:codespace_plan_id, :created_at], name: "idx_codespace_billing_message_on_plan_and_created_at"
    add_index :codespace_billing_messages, [:azure_storage_account_name, :message_id], unique: true, name: "idx_codespace_billing_message_on_account_name_and_message_id"
  end
end
