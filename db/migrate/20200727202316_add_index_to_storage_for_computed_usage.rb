# frozen_string_literal: true

class AddIndexToStorageForComputedUsage < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_index :codespaces_storage_usage_line_items,
      [:billable_owner_type, :billable_owner_id, :owner_id, :end_time, :computed_usage],
      name: "index_codespaces_storage_usage_line_items_on_computed_usage"
  end
end
