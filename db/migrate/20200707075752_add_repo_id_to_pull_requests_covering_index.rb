# frozen_string_literal: true

class AddRepoIdToPullRequestsCoveringIndex < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/database-migrations-for-dotcom for tips

  include GitHub::SafeDatabaseMigrationHelper

  self.use_connection_class(ApplicationRecord::Repositories)

  def up
    remove_index :pull_requests, name: :head_repository_id_and_head_ref_and_head_sha, if_exists: true
    add_index :pull_requests, [:head_repository_id, :head_ref, :head_sha, :repository_id], name: "head_repository_id_and_head_ref_and_head_sha_and_repository_id"
  end

  def down
    remove_index :pull_requests, name: :head_repository_id_and_head_ref_and_head_sha_and_repository_id, if_exists: true
    add_index :pull_requests, [:head_repository_id, :head_ref, :head_sha], name: "head_repository_id_and_head_ref_and_head_sha", if_not_exists: true
  end
end
