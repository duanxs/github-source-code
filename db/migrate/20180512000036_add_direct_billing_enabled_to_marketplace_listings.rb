# frozen_string_literal: true

class AddDirectBillingEnabledToMarketplaceListings < GitHub::Migration
  def change
    add_column :marketplace_listings, :direct_billing_enabled, :boolean, null: false, default: false
  end
end
