# frozen_string_literal: true

class AddCountryOfResidenceToSponsorsMembership < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :sponsors_memberships, :country_of_residence, "char(2)", null: true
  end
end
