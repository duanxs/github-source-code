# frozen_string_literal: true

class RemoveUrlColumnFromIntegrationManifests < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    remove_column :integration_manifests, :url, :text, null: false
  end
end
