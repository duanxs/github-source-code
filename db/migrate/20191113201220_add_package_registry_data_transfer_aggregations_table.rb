# frozen_string_literal: true

class AddPackageRegistryDataTransferAggregationsTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :package_registry_data_transfer_aggregations do |t|
      t.references :owner, null: false
      t.references :registry_package, null: false
      t.references :registry_package_version, null: false
      t.references :billable_owner, polymorphic: { limit: 12 }, null: false
      t.integer :aggregate_size_in_bytes, null: false, limit: 8
      t.timestamp :metered_billing_cycle_starts_at, null: false, default: -> { "CURRENT_TIMESTAMP" }
      t.timestamps null: false

      t.index [
        :billable_owner_type,
        :billable_owner_id,
        :metered_billing_cycle_starts_at,
        :owner_id,
        :registry_package_id,
        :registry_package_version_id,
      ], name: "index_data_transfer_agg_on_billable_owner_and_usage", unique: true
    end
  end
end
