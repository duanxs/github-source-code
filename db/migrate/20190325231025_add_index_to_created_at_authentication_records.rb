# frozen_string_literal: true

class AddIndexToCreatedAtAuthenticationRecords < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_index :authentication_records, [:created_at], name: "index_authentication_records_on_created_at"
  end
end
