# frozen_string_literal: true

class RemovePrimaryFromGitbackupsRepositoryMaintenance < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Gitbackups)

  def change
    add_index    :repository_maintenance, [:status, :scheduled_at]
    remove_index :repository_maintenance, column: [:network_id, :status, :scheduled_at], name: "index_repository_maintenance_fields"
  end
end
