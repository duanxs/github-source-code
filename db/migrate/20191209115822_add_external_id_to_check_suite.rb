# frozen_string_literal: true

class AddExternalIdToCheckSuite < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_column :check_suites,
               :external_id,
               :string,
               null: true,
               limit: 64,
               comment: "Supplied by creator to allow external systems to work with check suites idempotently"

    add_index :check_suites, [:repository_id, :github_app_id, :external_id], name: "by_external_id_per_app", unique: true
  end
end
