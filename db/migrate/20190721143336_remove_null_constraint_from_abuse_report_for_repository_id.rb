# frozen_string_literal: true

class RemoveNullConstraintFromAbuseReportForRepositoryId < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    change_column_null :abuse_reports, :repository_id, true
    change_column_null :abuse_reports, :reporting_user_id, true
  end
end
