# frozen_string_literal: true

class AddFlipperFeatureIdToFeatures < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    change_table(:features) do |t|
      t.remove :prerelease
      t.references :flipper_feature, index: { unique: true }, null: true
    end
  end

  def down
    change_table(:features) do |t|
      t.remove_index :flipper_feature_id
      t.remove :flipper_feature_id
      t.column :prerelease, :boolean, default: true, null: false
    end
  end
end
