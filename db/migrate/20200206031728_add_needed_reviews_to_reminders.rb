# frozen_string_literal: true

class AddNeededReviewsToReminders < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :reminders, :needed_reviews, :integer, null: false, default: 0, limit: 1
  end
end
