# frozen_string_literal: true

class CreateTableRefreshTokens < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :refresh_tokens, id: false do |t|
      t.column :id, "bigint NOT NULL AUTO_INCREMENT PRIMARY KEY"
      t.belongs_to :refreshable, polymorphic: { limit: 80 }, null: false
      t.string :hashed_token, limit: 80, null: false
      t.column :expires_at, "bigint(20)", null: false
      t.timestamps null: false
    end

    add_index :refresh_tokens, [:refreshable_id, :refreshable_type], unique: true
    add_index :refresh_tokens, [:hashed_token, :expires_at], unique: true
  end
end
