# frozen_string_literal: true

class AddSeatsToEnterpriseAgreement < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :enterprise_agreements, :seats, :integer, null: false, default: 0
  end
end
