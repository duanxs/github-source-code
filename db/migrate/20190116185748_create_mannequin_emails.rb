# frozen_string_literal: true

class CreateMannequinEmails < GitHub::Migration
  def change
    create_table :mannequin_emails do |t|
      t.belongs_to :mannequin, null: false
      t.string :email, null: false, limit: 100
      t.timestamps null: false
    end

    add_index :mannequin_emails, :mannequin_id
    add_index :mannequin_emails, :email
  end
end
