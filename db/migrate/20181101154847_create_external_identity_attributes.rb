# frozen_string_literal: true

class CreateExternalIdentityAttributes < GitHub::Migration
  def change
    create_table :external_identity_attributes do |t|
      t.belongs_to :external_identity, null: false
      t.string     :scheme, limit: 4, null: false
      t.string     :name, null: false
      t.string     :value, null: false
      t.text       :metadata_json
    end

    add_index :external_identity_attributes, [:external_identity_id, :scheme, :name, :value], name: :index_attributes_on_external_identity_name_and_value, unique: true
  end
end
