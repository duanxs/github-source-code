# frozen_string_literal: true

class CreateSharedStorageBillableOwnersTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :shared_storage_billable_owners do |t|
      t.belongs_to :owner, null: false
      t.belongs_to :billable_owner, polymorphic: { limit: 12 }, null: false
      t.timestamp :created_at, null: false

      t.index [:billable_owner_type, :billable_owner_id, :owner_id], name: "index_shared_storage_billable_owners_on_billable_owner_and_owner", unique: true
    end
  end
end
