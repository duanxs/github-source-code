# frozen_string_literal: true

class DropUserContentVersionsTable < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def up
    drop_table :user_content_versions
  end

  def down
    create_table :user_content_versions do |t|
      t.integer :versionable_id, null: false
      t.string :versionable_type, null: false
      t.string :event, null: false
      t.integer :actor_id, null: true
      t.mediumblob :previous_object
      t.datetime :created_at, null: false

      t.index [:versionable_id, :versionable_type], name: "user_content_versions_by_versionable"
    end
  end
end
