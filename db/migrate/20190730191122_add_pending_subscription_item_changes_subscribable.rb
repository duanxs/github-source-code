# frozen_string_literal: true

class AddPendingSubscriptionItemChangesSubscribable < GitHub::Migration
  def change
    add_column(:pending_subscription_item_changes, :subscribable_id, :integer)
    add_column(:pending_subscription_item_changes, :subscribable_type, :integer)
    add_index(
      :pending_subscription_item_changes,
      [:pending_plan_change_id, :subscribable_type, :subscribable_id],
      name: "index_pend_sub_item_changes_on_pend_plan_change_and_subscribable",
    )
  end
end
