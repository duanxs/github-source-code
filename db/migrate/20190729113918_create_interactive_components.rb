# frozen_string_literal: true

class CreateInteractiveComponents < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :interactive_components do |t|
      t.integer :container_id, null: false
      t.string :container_type, null: false, limit: 100
      t.string :external_id, null: false
      t.blob :elements, null: false
      t.integer :container_order, null: false
      t.timestamps null: false
      t.datetime :interacted_at, null: true
    end

    add_index :interactive_components, [:container_id, :container_type]
  end
end
