# frozen_string_literal: true

require "github/transitions/20180718074844_backfill_prerelease_program_members"

class BackfillPrereleaseProgramMembersTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::BackfillPrereleaseProgramMembers.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
