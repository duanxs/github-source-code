# frozen_string_literal: true

class AddSponsorsMembershipIdToSponsorsListings < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_reference :sponsors_listings, :sponsors_membership, null: true, index: { unique: true }
  end
end
