# frozen_string_literal: true

class DropPushInfos < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def self.up
    drop_table :push_infos
  end
end
