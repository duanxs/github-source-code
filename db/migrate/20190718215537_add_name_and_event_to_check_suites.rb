# frozen_string_literal: true

class AddNameAndEventToCheckSuites < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_column :check_suites, :name, "varbinary(1024)", null: true
    add_column :check_suites, :event, "varchar(50)", null: true

    add_index :check_suites, :name
  end
end
