# frozen_string_literal: true

class RemoveApprovedAtAttemptTypeFromAuthenticationRecords < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)
  def change
    remove_column :authentication_records, :approved_at, "datetime", null: true
    remove_column :authentication_records, :attempt_type, "varchar(10)", null: true
  end
end
