# frozen_string_literal: true

require "github/transitions/20200408173900_add_custom_role_enabled_to_fg_ps"

class AddCustomRoleEnabledToFgPsTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::AddCustomRoleEnabledToFgPs.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
