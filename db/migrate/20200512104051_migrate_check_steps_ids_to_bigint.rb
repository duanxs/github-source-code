# frozen_string_literal: true

class MigrateCheckStepsIdsToBigint < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def up
    change_column :check_steps, :id, "bigint(11) unsigned NOT NULL AUTO_INCREMENT"
    change_column :check_steps, :check_run_id, "bigint(11) unsigned NOT NULL"
  end

  def down
    change_column :check_steps, :id, "int(11) NOT NULL AUTO_INCREMENT"
    change_column :check_steps, :check_run_id, "int(11) NOT NULL"
  end
end
