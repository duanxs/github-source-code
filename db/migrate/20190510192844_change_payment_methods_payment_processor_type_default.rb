# frozen_string_literal: true

class ChangePaymentMethodsPaymentProcessorTypeDefault < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def up
    change_column_default :payment_methods, :payment_processor_type, "zuora"
  end

  def down
    change_column_default :payment_methods, :payment_processor_type, "braintree"
  end
end
