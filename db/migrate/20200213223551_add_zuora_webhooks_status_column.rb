# frozen_string_literal: true

class AddZuoraWebhooksStatusColumn < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_column :zuora_webhooks, :status, "enum('pending', 'processed', 'ignored')", null: true
    add_index :zuora_webhooks, [:status, :created_at]
  end
end
