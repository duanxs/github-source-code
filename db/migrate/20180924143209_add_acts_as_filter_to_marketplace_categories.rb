# frozen_string_literal: true

class AddActsAsFilterToMarketplaceCategories < GitHub::Migration
  def change
    add_column :marketplace_categories, :acts_as_filter, :boolean, null: false, default: false
    add_index :marketplace_categories, :acts_as_filter
  end
end
