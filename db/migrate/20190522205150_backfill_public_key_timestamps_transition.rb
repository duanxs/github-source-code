# frozen_string_literal: true

require "github/transitions/20190522205150_backfill_public_key_timestamps"

class BackfillPublicKeyTimestampsTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::BackfillPublicKeyTimestamps.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
