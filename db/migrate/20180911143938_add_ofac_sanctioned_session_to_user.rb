# frozen_string_literal: true

class AddOFACSanctionedSessionToUser < GitHub::Migration
  def change
    add_reference :users, :ofac_sanctioned_session, references: :user_sessions
  end
end
