# frozen_string_literal: true

class AddActiveToSponsorsCriteria < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :sponsors_criteria, :active, :boolean, null: false, default: 1
  end
end
