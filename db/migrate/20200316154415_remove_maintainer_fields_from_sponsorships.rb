# frozen_string_literal: true

class RemoveMaintainerFieldsFromSponsorships < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def self.up
    change_column_null :sponsorships, :sponsorable_type, false
    change_column_null :sponsorships, :sponsorable_id, false

    remove_column :sponsorships, :maintainer_type, if_exists: true
    remove_column :sponsorships, :maintainer_id, if_exists: true
  end

  def self.down
    add_column :sponsorships, :maintainer_id, :int, if_exists: true
    add_column :sponsorships, :maintainer_type, :int, if_exists: true

    add_index :sponsorships, [:maintainer_id, :maintainer_type], if_exists: true

    change_column_null :sponsorships, :maintainer_type, true
    change_column_null :sponsorships, :maintainer_id, true
  end
end
