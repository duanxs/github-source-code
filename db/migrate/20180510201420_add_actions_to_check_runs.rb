# frozen_string_literal: true

class AddActionsToCheckRuns < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_column :check_runs, :actions, :mediumblob
  end
end
