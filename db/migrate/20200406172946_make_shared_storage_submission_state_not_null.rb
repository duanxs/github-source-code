# frozen_string_literal: true

class MakeSharedStorageSubmissionStateNotNull < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    existing_type = "enum('unsubmitted', 'submitted', 'skipped')"

    reversible do |dir|
      dir.up do
        change_column :shared_storage_artifact_aggregations, :submission_state, existing_type, null: false, default: "unsubmitted"
      end
      dir.down do
        change_column :shared_storage_artifact_aggregations, :submission_state, existing_type, null: true, default: nil
      end
    end
  end
end
