# frozen_string_literal: true

class CreateEnterpriseContributions < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :enterprise_contributions do |t|
      t.integer :user_id, null: false
      t.integer :enterprise_installation_id, null: false
      t.integer :count, null: false
      t.date :created_on, null: false

      t.timestamps null: false
    end

    add_index :enterprise_contributions, [:user_id, :enterprise_installation_id, :created_on], unique: true, name: "index_enterprise_contributions_on_user_installation_and_created"
    add_index :enterprise_contributions, [:user_id, :created_on]
  end
end
