# frozen_string_literal: true

class DropSlugFromWorkflows < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Ballast)

  def up
    remove_column :workflows, :slug
  end

  def down
    add_column :workflows, :slug, :string, limit: 60, null: true
  end
end
