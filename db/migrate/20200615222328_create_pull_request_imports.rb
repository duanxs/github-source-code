# frozen_string_literal: true

class CreatePullRequestImports < GitHub::Migration
  self.use_connection_class ApplicationRecord::Collab

  def change
    create_table :pull_request_imports do |t|
      t.belongs_to :import, null: false
      t.belongs_to :pull_request, null: false

      t.index(
        [:pull_request_id],
        unique: true,
        name: "index_pull_request_imports_pull_request_id"
      )

      t.index(
        [:import_id],
        name: "index_pull_request_imports_import_id"
      )

      t.timestamps null: false
    end
  end
end
