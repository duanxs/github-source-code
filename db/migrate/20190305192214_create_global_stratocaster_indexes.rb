# frozen_string_literal: true

class CreateGlobalStratocasterIndexes < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Mysql5)

  def change
    create_table :global_stratocaster_indexes do |t|
      t.string :index_key, null: false, limit: 32
      t.integer :value, null: false
      t.datetime :modified_at, null: false
    end

    add_index :global_stratocaster_indexes, [:index_key, :modified_at]
    add_index :global_stratocaster_indexes, [:index_key, :value]
  end
end
