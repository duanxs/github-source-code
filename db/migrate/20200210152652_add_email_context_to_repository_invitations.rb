# frozen_string_literal: true

class AddEmailContextToRepositoryInvitations < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_column :repository_invitations, :email, :string, null: true, default: nil
    add_column :repository_invitations, :hashed_token, :string, null: true, default: nil, limit: 44

    add_index :repository_invitations, :hashed_token, unique: true
    add_index :repository_invitations, [:repository_id, :email], unique: true

    change_column_null :repository_invitations, :invitee_id, true
  end
end
