# frozen_string_literal: true

class AddEnterpriseInstallationUserAccountsUniqueKey < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_index :enterprise_installation_user_accounts,
      [:enterprise_installation_id, :remote_user_id],
      unique: true,
      name: "index_enterprise_inst_user_accounts_inst_id_and_remote_user_id"
  end
end
