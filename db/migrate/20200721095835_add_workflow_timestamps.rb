# frozen_string_literal: true

class AddWorkflowTimestamps < GitHub::Migration
  # See https://engineering-guide.githubapp.com/content/engineering/development-and-ops/dotcom/migrations-and-transitions/database-migrations-for-dotcom/ for tips

  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_column :workflows, :disabled_at, :datetime, null: true
    add_column :workflows, :enabled_at, :datetime, null: true
  end
end
