# frozen_string_literal: true

class RemoveSecurityPolicyFromCommunityProfile < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    remove_column :community_profiles, :has_security_policy, if_exists: true
  end
end
