# frozen_string_literal: true

class RemoveUserIdFromAdvisoryCredits < GitHub::Migration
  use_connection_class ApplicationRecord::Collab

  def up
    change_column_null :advisory_credits, :recipient_id, false

    remove_index :advisory_credits, [:ghsa_id, :user_id]
    remove_index :advisory_credits, [:user_id, :accepted_at]
    remove_column :advisory_credits, :user_id
  end

  def down
    add_column :advisory_credits, :user_id, :integer
    add_index :advisory_credits, [:user_id, :accepted_at]
    add_index :advisory_credits, [:ghsa_id, :user_id], unique: true

    change_column_null :advisory_credits, :recipient_id, true
  end
end
