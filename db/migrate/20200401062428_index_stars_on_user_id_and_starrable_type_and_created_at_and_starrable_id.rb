# frozen_string_literal: true

class IndexStarsOnUserIdAndStarrableTypeAndCreatedAtAndStarrableId < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  include GitHub::SafeDatabaseMigrationHelper

  def change
    remove_index :stars, column: [:user_id, :starrable_type, :created_at], if_exists: true
    add_index :stars, [:user_id, :starrable_type, :created_at, :starrable_id], name: "user_id_and_starrable_type_and_created_at_and_starrable_id", if_not_exists: true
  end
end
