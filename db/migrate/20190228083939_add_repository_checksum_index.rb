# frozen_string_literal: true

class AddRepositoryChecksumIndex < GitHub::Migration
  extend GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Spokes)

  def self.up
    add_index :repository_checksums, [:repository_type, :updated_at], unique: false, if_not_exists: true
  end

  def self.down
    remove_index :repository_checksums, [:repository_type, :updated_at], if_exists: true
  end
end
