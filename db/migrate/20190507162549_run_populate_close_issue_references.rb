# frozen_string_literal: true

require "github/transitions/20190402232159_populate_close_issue_references.rb"

class RunPopulateCloseIssueReferences < GitHub::Migration
  def up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::PopulateCloseIssueReferences.new(dry_run: false,
                                                                       verbose: true)
    transition.perform
  end

  def down
  end
end
