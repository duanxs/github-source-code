# frozen_string_literal: true

class AddIndexToZuoraWebhooks < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations for tips

  self.use_connection_class(ApplicationRecord::Ballast)


  def change
    add_index :zuora_webhooks, [:account_id, :kind]
  end
end
