# frozen_string_literal: true

class RemoveRepoStaffGrantId < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def self.up
    # Column ignored in https://github.com/github/github/pull/109354
    remove_column :repository_unlocks, :repository_staff_grant_id, if_exists: true
  end

  def self.down
    add_column :repository_unlocks, :repository_staff_grant_id, :integer, null: false, if_not_exists: true
  end
end
