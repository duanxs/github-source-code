# frozen_string_literal: true

class AddPackageRegistryDataTransferLineItemsSubmissionState < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :package_registry_data_transfer_line_items, :submission_state, "enum('unsubmitted', 'submitted', 'skipped')", default: nil
    add_column :package_registry_data_transfer_line_items, :submission_state_reason, :string, limit: 24, default: nil
    add_index :package_registry_data_transfer_line_items, [:submission_state, :submission_state_reason],
      name: "index_packages_line_items_on_submission_state_and_reason"
  end
end
