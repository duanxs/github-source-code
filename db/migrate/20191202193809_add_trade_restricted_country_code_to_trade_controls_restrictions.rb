# frozen_string_literal: true

class AddTradeRestrictedCountryCodeToTradeControlsRestrictions < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :trade_controls_restrictions, :trade_restricted_country_code, :string, limit: 16
    add_index :trade_controls_restrictions, :trade_restricted_country_code,
      name: "index_on_trade_restricted_country_code"
  end
end
