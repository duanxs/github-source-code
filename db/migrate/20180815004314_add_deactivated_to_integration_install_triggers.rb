# frozen_string_literal: true

class AddDeactivatedToIntegrationInstallTriggers < GitHub::Migration
  def change
    add_column :integration_install_triggers, :deactivated, :boolean, null: false, default: false
  end
end
