# frozen_string_literal: true

class CreateSponsorsMembershipsCriteria < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :sponsors_memberships_criteria do |t|
      t.integer :sponsors_criterion_id, null: false, index: true
      t.integer :sponsors_membership_id, null: false
      t.integer :reviewer_id, null: true, index: true
      t.boolean :met, default: false, null: false
      t.blob    :value, null: true

      t.timestamps null: false

      t.index [:sponsors_membership_id, :sponsors_criterion_id], unique: true, name: "index_on_membership_and_criterion"
      t.index [:sponsors_membership_id, :met], name: "index_on_membership_and_met"
    end
  end
end
