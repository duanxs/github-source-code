# frozen_string_literal: true

class AddArchivedRepositoryWikiCacheVersionNumberColumn < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_column :archived_repository_wikis,
               :cache_version_number,
               :integer,
               null: false,
               default: 1
  end
end
