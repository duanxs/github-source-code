# frozen_string_literal: true

class CreateRepositoryAdvisoryEvents < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :repository_advisory_events do |t|
      t.belongs_to :repository_advisory, null: false
      t.belongs_to :actor, null: false
      t.column :event, "varchar(40)", null: false
      t.column :changed_attribute, "varchar(40)"
      t.column :value_was, "varbinary(2014)"
      t.column :value_is, "varbinary(2014)"
      t.timestamps null: false
    end

    add_index :repository_advisory_events, [:repository_advisory_id, :event], name: "index_repository_advisory_events_on_repo_adv_id_and_event"
    add_index :repository_advisory_events, [:actor_id, :event, :created_at], name: "index_repository_advisory_events_on_actor_id_event_created_at"
  end
end
