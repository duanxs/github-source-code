# frozen_string_literal: true

class IndexApproveAtAndAccessedAt < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def up
    remove_index :authenticated_devices, :accessed_at
    add_index :authenticated_devices, [:accessed_at, :approved_at]
  end

  def down
    remove_index :authenticated_devices, [:accessed_at, :approved_at]
    add_index :authenticated_devices, :accessed_at
  end
end
