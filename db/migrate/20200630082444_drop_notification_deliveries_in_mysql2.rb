# frozen_string_literal: true

class DropNotificationDeliveriesInMysql2 < GitHub::Migration
  # This migration is part of https://github.com/github/notifications/issues/267
  # and is meant to drop a tablesin the `mysql2` cluster that we've already moved
  # into the new `notifications_deliveries` cluster.
  self.use_connection_class(ApplicationRecord::Mysql2)

  def self.up
    # We don't have multiple database clusters in Enterprise, so there's no need to drop the tables
    return if GitHub.enterprise?

    drop_table :notification_deliveries
  end

  def self.down
    # We don't have multiple database clusters in Enterprise
    return if GitHub.enterprise?

    create_table :notification_deliveries, if_not_exists: true do |t|
      t.datetime :delivered_at, null: false
      t.integer :list_id, null: false
      t.string :thread_key, null: false, limit: 255
      t.string :comment_key, null: false, limit: 255
      t.references :user, null: false
      t.string :handler, null: false, limit: 255
      t.string :reason, null: true, limit: 255
    end
    change_column :notification_deliveries, :id, "bigint(11) NOT NULL AUTO_INCREMENT"
    add_index(
      :notification_deliveries,
      [:list_id, :thread_key, :comment_key, :user_id, :handler],
      unique: true,
      name: "by_list",
      if_not_exists: true
    )
    add_index(
      :notification_deliveries,
      [:delivered_at],
      name: "by_time",
      if_not_exists: true
    )
    add_index(
      :notification_deliveries,
      [:user_id, :list_id, :thread_key],
      name: "by_user",
      if_not_exists: true
    )
  end
end
