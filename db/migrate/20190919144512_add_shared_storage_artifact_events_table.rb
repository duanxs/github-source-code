# frozen_string_literal: true

class AddSharedStorageArtifactEventsTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :shared_storage_artifact_events do |t|
      t.belongs_to :owner, null: false
      t.belongs_to :repository, null: false
      t.timestamp :effective_at, null: false
      t.column :source, "enum('unknown', 'actions', 'gpr')", null: false, default: "unknown"
      t.column :repository_visibility, "enum('unknown', 'public', 'private')", null: false, default: "unknown"
      t.column :event_type, "enum('unknown', 'add', 'remove')", null: false, default: "unknown"
      t.integer :size_in_bytes, limit: 8, null: false
      t.belongs_to :aggregation, default: nil
      t.timestamps null: false

      t.index [:owner_id, :repository_id, :effective_at, :aggregation_id],
        name: "index_shared_storage_artifact_events_on_owner_repo_time_agg"
    end
  end
end
