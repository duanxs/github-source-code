# frozen_string_literal: true

class CreateEnterpriseInstallationUserAccounts < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :enterprise_installation_user_accounts do |t|
      t.belongs_to :enterprise_installation, null: false
      t.timestamps null: false
      t.integer :remote_user_id, null: false
      t.datetime :remote_created_at, null: false
      t.string :login, limit: 40, null: false
      t.string :profile_name, limit: 255, null: true
      t.boolean :site_admin, null: false, default: false
      t.datetime :suspended_at, null: true
    end

    add_index :enterprise_installation_user_accounts,
      [:enterprise_installation_id],
      name: "index_enterprise_installation_user_accounts_on_installation_id"
  end
end
