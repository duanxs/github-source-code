# frozen_string_literal: true

class AddDeploymentLatestEnvironment < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations for tips
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_column :deployments, :latest_environment, :string
    add_column :deployments, :latest_status_state, "varchar(25)"
    add_column :archived_deployments, :latest_environment, :string
    add_column :archived_deployments, :latest_status_state, "varchar(25)"

    add_index :deployments, [:repository_id, :latest_environment, :created_at], name: "index_deployments_on_repository_latest_env_created_at"
    add_index :deployments, [:repository_id, :sha, :latest_environment, :created_at], name: "index_deployments_on_sha_repository_latest_env_created_at"
    add_index :deployments, [:repository_id, :latest_environment, :latest_status_state], name: "index_deployments_on_repository_latest_status_state"
  end
end
