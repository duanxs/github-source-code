# frozen_string_literal: true

class CreateDiscussionCategories < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table(:discussion_categories) do |t|
      t.belongs_to :repository, null: false

      t.column :emoji, "VARBINARY(44)", null: true
      t.column :name, "VARBINARY(512)", null: false
      t.mediumblob :description, null: true

      t.timestamps null: false
    end

    add_index :discussion_categories, [:repository_id, :name], unique: true
  end
end
