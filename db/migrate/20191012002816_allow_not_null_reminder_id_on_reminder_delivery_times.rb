# frozen_string_literal: true

class AllowNotNullReminderIdOnReminderDeliveryTimes < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    change_column :reminder_delivery_times, :reminder_id, :integer, null: true
  end

  def down
    change_column :reminder_delivery_times, :reminder_id, :integer, null: false
  end
end
