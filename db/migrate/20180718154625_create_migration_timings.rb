# frozen_string_literal: true

class CreateMigrationTimings < GitHub::Migration
  def change
    create_table :migration_timings do |t|
      t.belongs_to :migration, index: true, null: false
      t.integer :action, index: true, null: false
      t.integer :time_elapsed, null: false

      t.timestamps null: false
    end
  end
end
