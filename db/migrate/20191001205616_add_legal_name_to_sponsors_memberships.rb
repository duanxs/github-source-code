# frozen_string_literal: true

class AddLegalNameToSponsorsMemberships < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :sponsors_memberships, :legal_name, :string, null: true
  end
end
