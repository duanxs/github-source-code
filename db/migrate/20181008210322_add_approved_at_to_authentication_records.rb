# frozen_string_literal: true

class AddApprovedAtToAuthenticationRecords < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_column :authentication_records, :approved_at, :datetime
  end
end
