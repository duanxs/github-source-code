# frozen_string_literal: true

class AddRankMultiplierToRepositoryActions < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_column :repository_actions, :rank_multiplier, :float, default: 1.0, null: false
    add_index :repository_actions, :rank_multiplier
  end
end
