# frozen_string_literal: true

class AddPurgedColumnAndIndexesToPhotoDnaHits < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :photo_dna_hits, :purged, :boolean, null: false, default: false

    add_index :photo_dna_hits, [:purged, :created_at]
  end
end
