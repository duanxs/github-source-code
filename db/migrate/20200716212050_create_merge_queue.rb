# frozen_string_literal: true

class CreateMergeQueue < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :merge_queues do |t|
      t.references :repository, null: false
      t.column :branch, "VARBINARY(1024)", null: false

      t.timestamps null: false
    end

    add_index :merge_queues, [:repository_id, :branch], unique: true
  end
end
