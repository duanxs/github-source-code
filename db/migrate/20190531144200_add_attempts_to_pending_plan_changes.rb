# frozen_string_literal: true

class AddAttemptsToPendingPlanChanges < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def change
    add_column :pending_plan_changes, :attempts, :integer, null: false, default: 0
  end
end
