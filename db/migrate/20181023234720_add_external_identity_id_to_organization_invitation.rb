# frozen_string_literal: true

class AddExternalIdentityIdToOrganizationInvitation < GitHub::Migration
  def change
    add_reference :organization_invitations, :external_identity, index: true, null: true
  end
end
