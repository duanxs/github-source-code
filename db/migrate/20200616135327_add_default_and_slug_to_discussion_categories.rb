# frozen_string_literal: true

class AddDefaultAndSlugToDiscussionCategories < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    change_table :discussion_categories do |t|
      t.string :slug
      t.boolean :is_default, null: false, default: false
    end

    add_index :discussion_categories, [:repository_id, :slug], unique: true
  end
end
