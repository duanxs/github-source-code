# frozen_string_literal: true

class DropRepositoryHealthChecks < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def up
    drop_table :repository_health_checks
  end

  def down
    create_table :repository_health_checks do |t|
      t.integer :repository_id, null: false
      t.string :name, limit: 32, null: false
      t.string :status, limit: 8, null: false
      t.column :value, :bigint, null: false

      t.timestamps null: true
    end

    add_index :repository_health_checks, :repository_id
  end
end
