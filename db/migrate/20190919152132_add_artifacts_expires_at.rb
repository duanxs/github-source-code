# frozen_string_literal: true

class AddArtifactsExpiresAt < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_column :artifacts, :expires_at, :datetime, null: true
  end
end
