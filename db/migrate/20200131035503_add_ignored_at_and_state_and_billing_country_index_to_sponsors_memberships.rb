# frozen_string_literal: true

class AddIgnoredAtAndStateAndBillingCountryIndexToSponsorsMemberships < GitHub::Migration
  extend GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Collab)

  def self.up
    add_index :sponsors_memberships, [:ignored_at, :state, :billing_country], name: "index_on_ignored_state_country", if_not_exists: true
    remove_index :sponsors_memberships, column: [:ignored_at], if_exists: true
  end

  def self.down
    remove_index :sponsors_memberships, column: [:ignored_at, :state, :billing_country], if_exists: true
    add_index :sponsors_memberships, [:ignored_at], if_not_exists: true
  end
end
