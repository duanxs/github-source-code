# frozen_string_literal: true

class CreateStreamingLogs < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    create_table :streaming_logs do |t|
      t.references :check_run, null: false
      t.text :url, null: false
      t.string :token, null: false
      t.datetime :token_expires_at, null: false
      t.timestamps null: false
    end

    add_index :streaming_logs, :check_run_id, unique: false
  end
end
