# frozen_string_literal: true

class HostGistReplicasIndex < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Spokes)

  def change
    add_index :gist_replicas, [:host, :state, :gist_id], if_not_exists: true
    remove_index :gist_replicas, column: [:host, :state], if_exists: true
  end
end
