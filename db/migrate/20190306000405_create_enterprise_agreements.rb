# frozen_string_literal: true

class CreateEnterpriseAgreements < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :enterprise_agreements do |t|
      t.string  :agreement_id, limit: 128, null: false
      t.integer :business_id, null: false
      t.integer :category, null: false
      t.integer :status, null: false, default: 0
      t.timestamps null: false

      t.index :agreement_id, unique: true
      t.index [:business_id, :category, :status], name: "index_enterprise_agreements_on_business_and_category_and_status"
    end
  end
end
