# frozen_string_literal: true

class CreateEnterpriseInstallationUserAccountEmails < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :enterprise_installation_user_account_emails do |t|
      t.belongs_to :enterprise_installation_user_account, null: false
      t.timestamps null: false
      t.string :email, limit: 255, null: false
      t.boolean :primary, null: false, default: false
    end

    add_index :enterprise_installation_user_account_emails,
      [:enterprise_installation_user_account_id],
      name: "index_enterprise_installation_user_account_emails_on_account_id"

    add_index :enterprise_installation_user_account_emails,
      [:enterprise_installation_user_account_id, :email],
      unique: true,
      name: "index_enterprise_inst_user_account_emails_account_id_and_email"
  end
end
