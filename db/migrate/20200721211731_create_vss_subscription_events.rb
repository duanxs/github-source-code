# frozen_string_literal: true

class CreateVssSubscriptionEvents < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :vss_subscription_events do |t|
      t.text :payload
      t.column :status, "enum('unprocessed', 'processed', 'failed')", null: false, default: "unprocessed"

      t.timestamps null: false
    end
  end
end
