# frozen_string_literal: true

class AddDiscussionsLabels < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :discussions_labels do |t|
      t.column :label_id, "bigint(20)", null: false
      t.integer :discussion_id, null: false
    end

    add_index :discussions_labels, [:discussion_id, :label_id], unique: true
    add_index :discussions_labels, [:label_id, :discussion_id]
  end
end
