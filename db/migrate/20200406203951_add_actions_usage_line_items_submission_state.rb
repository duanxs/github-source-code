# frozen_string_literal: true

class AddActionsUsageLineItemsSubmissionState < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :actions_usage_line_items, :submission_state, "enum('unsubmitted', 'submitted', 'skipped')", default: nil
    add_column :actions_usage_line_items, :submission_state_reason, :string, limit: 24, default: nil
    add_index :actions_usage_line_items, [:submission_state, :submission_state_reason],
      name: "index_actions_usage_line_items_on_submission_state_and_reason"
  end
end
