# frozen_string_literal: true

class AddEnterpriseWebBusinessIdToBusinesses < GitHub::Migration
  def change
    add_column :businesses, :enterprise_web_business_id, :int, null: true

    add_index :businesses, :enterprise_web_business_id, unique: true
  end
end
