# frozen_string_literal: true

class AddOwnerLoginToRepositories < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    [:repositories, :archived_repositories].each do |table_name|
      add_column table_name, :owner_login, :string, {
        null: true, # will eventually be changed to false, post-transition
        limit: 40, # mirrored from `users.login`
      }

      add_index table_name, [:owner_login, :name, :active], unique: false
    end
  end
end
