# frozen_string_literal: true

class AddQuestionIdIndexToSurveyAnswers < GitHub::Migration
  def change
    add_index :survey_answers, :question_id
  end
end
