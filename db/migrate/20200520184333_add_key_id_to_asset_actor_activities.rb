# frozen_string_literal: true

class AddKeyIdToAssetActorActivities < GitHub::Migration
  def up
    add_column :asset_actor_activities , :key_id, :integer, default: 0, null: false

    add_index :asset_actor_activities, [:asset_type, :owner_id, :actor_id, :key_id, :activity_started_at, :repository_id], name: "index_asset_actor_activities_on_type_owner_actor_key_time_repo", unique: true
    add_index :asset_actor_activities, [:asset_type, :owner_id, :actor_id, :activity_started_at, :repository_id], name: "index_asset_actor_activities_on_type_owner_actor_time_repo"
    remove_index :asset_actor_activities, name: "index_asset_actor_activities_on_type_owner_actor_time_and_repo"
  end

  def down
    remove_column :asset_actor_activities , :key_id

    add_index :asset_actor_activities, [:asset_type, :owner_id, :actor_id, :activity_started_at, :repository_id], name: "index_asset_actor_activities_on_type_owner_actor_time_and_repo", unique: true
    remove_index :asset_actor_activities, name: "index_asset_actor_activities_on_type_owner_actor_key_time_repo"
    remove_index :asset_actor_activities, name: "index_asset_actor_activities_on_type_owner_actor_time_repo"
  end
end
