# frozen_string_literal: true

class UserRolesUniqueIndex < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Iam)

  def change
    add_index :user_roles, [:actor_id, :actor_type, :target_id, :target_type], unique: true, name: :idx_user_roles_actor_and_target
  end
end
