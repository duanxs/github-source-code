# frozen_string_literal: true

require "github/transitions/20191210220630_convert_public_ghes_repos_to_internal.rb"

class ConvertPublicGhesReposToInternal < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def change
    # Customers will optionally run this manually for GHES 2.20.
    # transition = GitHub::Transitions::ConvertPublicGhesReposToInternal.new({dry_run: false, verbose: true})
    #transition.run
  end
end
