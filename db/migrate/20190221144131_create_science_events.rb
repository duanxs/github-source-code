# frozen_string_literal: true

class CreateScienceEvents < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :science_events do |t|
      t.string :name, null: false, limit: 85
      t.string :event_type, null: false, limit: 10
      t.text :payload, null: false

      t.timestamps null: false
    end

    add_index :science_events, [:name, :event_type]
  end
end
