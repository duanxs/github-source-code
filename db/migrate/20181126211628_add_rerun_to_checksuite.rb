# frozen_string_literal: true

class AddRerunToChecksuite < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_column :check_suites, :rerequestable, :boolean, default: true, null: false
    add_column :check_suites, :check_runs_rerunnable, :boolean, default: true, null: false
  end
end
