# frozen_string_literal: true

class AddStateTimestampsToRepositoryContributionGraphStatuses < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper

  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :repository_contribution_graph_statuses, :last_indexed_at, :datetime, null: true, if_not_exists: true
    add_column :repository_contribution_graph_statuses, :job_enqueued_at, :datetime, null: true, if_not_exists: true
  end
end
