# frozen_string_literal: true

class AddUserIdIndexToBusinessUserAccounts < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_index :business_user_accounts, :user_id
  end
end
