# frozen_string_literal: true

class AddBillabilityFieldsToSharedStorageArtifactAggregations < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :shared_storage_artifact_aggregations, :billable_owner_type, :string, limit: 12
    add_column :shared_storage_artifact_aggregations, :billable_owner_id, :integer
    add_column :shared_storage_artifact_aggregations, :directly_billed, :boolean, default: true, null: false
  end
end
