# frozen_string_literal: true

class AddOwnerIndexToActionsUsageLineItems < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_index :actions_usage_line_items, [
      :billable_owner_type,
      :billable_owner_id,
      :self_hosted,
      :repository_visibility,
      :end_time,
      :job_runtime_environment,
      :duration_in_milliseconds,
      :duration_multiplier,
    ], name: "index_on_billable_owner_and_usage"
  end
end
