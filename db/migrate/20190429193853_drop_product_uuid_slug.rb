# frozen_string_literal: true

class DropProductUUIDSlug < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def up
    remove_index :product_uuids, column: [:slug]
    remove_column :product_uuids, :slug

    change_column_null :product_uuids, :product_type, false
    change_column_null :product_uuids, :product_key, false
    change_column_null :product_uuids, :billing_cycle, false
  end

  def down
    add_column :product_uuids, :slug, :string, null: true, limit: 64
    add_index :product_uuids, [:slug], unique: true

    change_column_null :product_uuids, :product_type, true
    change_column_null :product_uuids, :product_key, true
    change_column_null :product_uuids, :billing_cycle, true
  end
end
