# frozen_string_literal: true

class AddNullConstraintToVulnGhsa < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Notify)

  def change
    change_column_null :vulnerabilities, :ghsa_id, false
  end
end
