# frozen_string_literal: true

class CreateDocusignEnvelopesTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :docusign_envelopes do |t|
      t.references :owner, polymorphic: true, null: false
      t.string     :envelope_id, limit: 40, null: false, index: true, unique: true
      t.column     :status, "tinyint unsigned", null: false, index: true, default: 0
      t.string     :voided_reason, limit: 140, null: true
      t.column     :active, "tinyint(1) unsigned", null: false, index: true, default: 1
      t.timestamps null: false
    end

    add_index :docusign_envelopes, [:owner_type, :owner_id, :active], name: "index_docusign_envelopes_on_owner_and_active"
  end
end
