# frozen_string_literal: true

class AddUniqueIndexToDownloadIdOnDataTransferLineItems < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    change_column :package_registry_data_transfer_line_items, :download_id, :string, limit: 50, null: false
    add_index :package_registry_data_transfer_line_items, :download_id, unique: true
  end
end
