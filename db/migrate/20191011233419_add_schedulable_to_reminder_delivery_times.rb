# frozen_string_literal: true

class AddSchedulableToReminderDeliveryTimes < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    change_table :reminder_delivery_times do |t|
      t.integer :schedulable_id, null: true
      t.string :schedulable_type, limit: 20, null: true # Must be at least 16 characters

      t.index [:schedulable_type, :schedulable_id, :time, :day], unique: true, name: "index_reminder_delivery_times_on_schedulable_and_time_and_day"
    end
  end
end
