# frozen_string_literal: true

class DropMetroplexEmails < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Mysql1)

  def self.up
    drop_table :metroplex_emails
  end

  def self.down
    create_table :metroplex_emails do |t|
      t.string :message_signature, limit: 255, null: false
      t.string :message_id, limit: 255, null: false
      t.string :recipient, limit: 255, null: false
      t.string :sender, limit: 255, null: false
      t.datetime :received_at, null: true
      t.datetime :parsed_at, null: true
      t.datetime :attachments_processed_at, null: true
      t.datetime :saved_at, null: true
      t.string :url, limit: 255, null: true
      t.string :mail_path, limit: 255, null: true
      t.text :error, null: true
    end

    add_index :metroplex_emails, [:message_signature], unique: true
    add_index :metroplex_emails, [:message_id]
    add_index :metroplex_emails, [:recipient]
    add_index :metroplex_emails, [:sender]
    add_index :metroplex_emails, [:received_at]
  end
end
