# frozen_string_literal: true

class AddUniqueIndexToDocusignEnvelopeId < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :docusign_envelopes, :document_type, "tinyint unsigned", null: false
    remove_index :docusign_envelopes, :envelope_id
    add_index :docusign_envelopes, :envelope_id, unique: true
  end
end
