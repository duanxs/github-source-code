# frozen_string_literal: true

class AddBlockDurationToIssueEventDetails < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_column :issue_event_details, :block_duration_days, :integer
    add_column :archived_issue_event_details, :block_duration_days, :integer
  end
end
