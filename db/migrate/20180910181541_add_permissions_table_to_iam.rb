# frozen_string_literal: true

class AddPermissionsTableToIam < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Iam)

  def up
    create_table :permissions, id: false do |t|
      t.column :id, "bigint NOT NULL AUTO_INCREMENT PRIMARY KEY"
      t.column :actor_id, "int(11) unsigned", null: false
      t.column :actor_type, "varchar(40)", null: false
      t.column :action, "int(11) unsigned", null: false
      t.column :subject_id, "int(11) unsigned", null: false
      t.column :subject_type, "varchar(60)", null: false
      t.column :priority, "int(11) unsigned", null: false, default: 1
      t.column :parent_id, "int(11) unsigned", null: false, default: 0
      t.timestamps null: false
    end

    add_index :permissions, [:actor_type, :actor_id, :subject_type, :subject_id, :priority, :parent_id], name: :index_permissions_by_identity, unique: true
    add_index :permissions, [:subject_id, :subject_type, :actor_type, :action, :priority], name: :index_permissions_by_subject_actor_type_action_priority
  end

  def down
    drop_table :permissions
  end
end
