# frozen_string_literal: true

class RemoveDeprecatedColumnsFromBillableQueryIndicesPt2 < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_index :package_registry_data_transfer_line_items,
      [:billable_owner_type, :billable_owner_id, :downloaded_at, :size_in_bytes],
      name: "index_on_billable_owner_and_usage"
  end
end
