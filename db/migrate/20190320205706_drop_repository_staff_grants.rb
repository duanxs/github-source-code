# frozen_string_literal: true

class DropRepositoryStaffGrants < GitHub::Migration
  # Code using this table was removed in https://github.com/github/github/pull/109354

  def self.up
    drop_table :repository_staff_grants, if_exists: true
  end

  def self.down
    create_table :repository_staff_grants, if_not_exists: true do |t|
      t.belongs_to :granted_by, null: false
      t.belongs_to :repository, null: false
      t.string     :reason,     null: false
      t.datetime   :expires_at, null: false
      t.boolean    :revoked,    null: false, default: false
      t.belongs_to :revoked_by, null: true
      t.datetime   :revoked_at, null: true
      t.timestamps              null: false
      t.belongs_to :repository_staff_grant_request, null: true
    end

    add_index :repository_staff_grants, :repository_id, if_not_exists: true
    add_index :repository_staff_grants, :granted_by_id, if_not_exists: true
  end
end
