# frozen_string_literal: true

class AddReportThirdPartyAnalyticsToUsers < GitHub::Migration
  def change
    add_column :users, :report_third_party_analytics, :boolean, default: true, null: false
  end
end
