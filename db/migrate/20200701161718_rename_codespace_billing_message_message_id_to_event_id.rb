# frozen_string_literal: true

class RenameCodespaceBillingMessageMessageIdToEventId < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    rename_column :codespace_billing_messages, :message_id, :event_id
    remove_index :codespace_billing_messages, name: "idx_codespace_billing_message_on_account_name_and_message_id"
    add_index :codespace_billing_messages, [:event_id], unique: true, name: "idx_codespace_billing_message_on_event_id"
  end
end
