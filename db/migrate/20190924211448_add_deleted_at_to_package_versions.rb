# frozen_string_literal: true

class AddDeletedAtToPackageVersions < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Repositories)

  def up
    add_column :package_versions, :deleted_at, :datetime, null: :true, if_not_exists: true
  end

  def down
    remove_column :package_versions, :deleted_at, if_exists: true
  end
end
