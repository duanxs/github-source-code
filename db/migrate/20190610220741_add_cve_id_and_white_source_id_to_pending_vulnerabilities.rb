# frozen_string_literal: true

class AddCveIdAndWhiteSourceIdToPendingVulnerabilities < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Notify)

  def change
    add_column :pending_vulnerabilities, :cve_id, :string, null: true, limit: 20
    add_index :pending_vulnerabilities, :cve_id, unique: false

    add_column :pending_vulnerabilities, :white_source_id, :string, null: true, limit: 20
    add_index :pending_vulnerabilities, :white_source_id, unique: false
  end
end
