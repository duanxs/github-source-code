# frozen_string_literal: true

class AddUpdatedAtIndexForIssueEvents < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/database-migrations-for-dotcom for tips

  def change
    add_index :issue_events, [:repository_id, :created_at], name: "index_issue_events_on_repository_id_and_created_at"
  end
end
