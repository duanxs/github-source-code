# frozen_string_literal: true

class AddDiscussionsScore < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :discussions, :score, :integer, null: false, default: 0
    add_index :discussions, [:repository_id, :score, :chosen_comment_id],
      name: "index_discussions_on_repo_score_chosen_comment"
  end
end
