# frozen_string_literal: true

class CreateReservedLogin < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    create_table :reserved_logins do |t|
      t.string :login, null: false, limit: 40
      t.timestamps null: false
    end

    add_index :reserved_logins, :login, unique: true
  end
end
