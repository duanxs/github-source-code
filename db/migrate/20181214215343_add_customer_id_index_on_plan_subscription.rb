# frozen_string_literal: true

class AddCustomerIdIndexOnPlanSubscription < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations for tips

  def change
    add_index :plan_subscriptions, :customer_id
  end
end
