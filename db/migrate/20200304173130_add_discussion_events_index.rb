# frozen_string_literal: true

class AddDiscussionEventsIndex < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_index :discussion_events, [:discussion_id, :event_type, :comment_id, :created_at],
      name: "index_on_discussion_id_event_type_comment_id_created_at"
  end
end
