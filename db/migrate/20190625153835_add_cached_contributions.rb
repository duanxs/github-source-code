# frozen_string_literal: true

class AddCachedContributions < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :cached_contributions do |t|
      t.integer :user_id, null: false
      t.date :date, null: false
      t.integer :count, null: false
      t.string :contribution_type, null: false, limit: 60
      t.string :visibility_subject_type, null: true, limit: 40
      t.integer :visibility_subject_id, null: true
    end

    add_index :cached_contributions,
      [:user_id, :date, :contribution_type, :visibility_subject_type, :visibility_subject_id],
      unique: true,
      name: "index_cached_contributions_on_user_date_type_visibility_subject"
  end
end
