# frozen_string_literal: true

class IndexProjectsOnCreatorId < GitHub::Migration
  def change
    add_index(:projects, :creator_id)
  end
end
