# frozen_string_literal: true

class DreamlifterAddCheckStepsColumns < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_column :check_steps, :status, :integer, default: 0, null: false
    add_column :check_steps, :external_id, :string, null: true
    add_column :check_steps, :completed_log_lines, :integer, null: true
  end
end
