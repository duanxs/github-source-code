# frozen_string_literal: true

require "github/transitions/20200427160308_set_ghe_content_analysis_notifications_config"

class SetGheContentAnalysisNotificationsConfigTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::SetGheContentAnalysisNotificationsConfig.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
