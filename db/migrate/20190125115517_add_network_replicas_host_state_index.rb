# frozen_string_literal: true

class AddNetworkReplicasHostStateIndex < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Spokes)

  def change
    add_index :network_replicas, [:host, :state], if_not_exists: true
    remove_index :network_replicas, name: "host_only", if_exists: true
  end
end
