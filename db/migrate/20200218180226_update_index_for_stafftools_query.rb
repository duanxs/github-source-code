# frozen_string_literal: true

class UpdateIndexForStafftoolsQuery < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    remove_index :shared_storage_artifact_events, name: :index_on_fields_for_stafftools_breakdown
    add_index :shared_storage_artifact_events, [:source, :event_type, :owner_id, :effective_at, :repository_id, :size_in_bytes], name: :index_on_fields_for_artifact_expiration
  end

  def down
    remove_index :shared_storage_artifact_events, name: :index_on_fields_for_artifact_expiration
    add_index :shared_storage_artifact_events, [:owner_id, :source, :repository_visibility, :effective_at, :repository_id, :event_type, :size_in_bytes], name: :index_on_fields_for_stafftools_breakdown
  end
end
