# frozen_string_literal: true

class AddVisibilityRuntimeJobIdentifiersToActionsUsageLineItems < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    add_column :actions_usage_line_items, :job_id, :string, limit: 36
    add_column :actions_usage_line_items, :job_runtime_environment, :string, limit: 20
    add_column :actions_usage_line_items, :duration_multiplier, :decimal, precision: 4, scale: 2
    add_column :actions_usage_line_items, :repository_visibility, :string, limit: 30

    remove_index :actions_usage_line_items, [:run_provider, :run_id]
  end

  def down
    remove_column :actions_usage_line_items, :job_id
    remove_column :actions_usage_line_items, :job_runtime_environment
    remove_column :actions_usage_line_items, :duration_multiplier
    remove_column :actions_usage_line_items, :repository_visibility

    add_index :actions_usage_line_items, [:run_provider, :run_id], unique: true
  end
end
