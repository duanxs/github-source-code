# frozen_string_literal: true

class AddPolymorphicOwnerToLabels < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_reference :labels, :owner, polymorphic: { limit: 30 }, index: true

    add_index :labels, [:owner_type, :owner_id, :name]
    add_index :labels, [:owner_type, :owner_id, :lowercase_name]

    add_reference :archived_labels, :owner, polymorphic: { limit: 30 }, index: true
  end
end
