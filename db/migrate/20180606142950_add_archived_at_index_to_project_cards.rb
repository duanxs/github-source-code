# frozen_string_literal: true

class AddArchivedAtIndexToProjectCards < GitHub::Migration
  def change
    add_index :project_cards, [:project_id, :archived_at]
  end
end
