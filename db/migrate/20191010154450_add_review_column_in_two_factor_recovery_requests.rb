# frozen_string_literal: true

class AddReviewColumnInTwoFactorRecoveryRequests < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    add_column :two_factor_recovery_requests, :staff_review_requested_at, :datetime
  end

  def down
    remove_column :two_factor_recovery_requests, :staff_review_requested_at
  end
end
