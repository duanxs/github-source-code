# frozen_string_literal: true

class AuthenticatedDeviceCreatedAt < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_index :authenticated_devices, :accessed_at
  end
end
