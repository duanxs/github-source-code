# frozen_string_literal: true

class MoreGranularLocation < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_column :authentication_records, :region_name, :string, limit: 64
    add_column :authentication_records, :city, :string, limit: 64
  end
end
