# frozen_string_literal: true

class DefaultNullSynchronizationBatchIdForDataTransfer < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    change_column_null :package_registry_data_transfer_line_items, :synchronization_batch_id, true
  end
end
