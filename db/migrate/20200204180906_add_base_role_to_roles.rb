# frozen_string_literal: true

class AddBaseRoleToRoles < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Iam)

  def change
    add_column :roles, :base_role_id, :integer
  end
end
