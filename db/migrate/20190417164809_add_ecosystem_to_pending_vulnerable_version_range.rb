# frozen_string_literal: true

class AddEcosystemToPendingVulnerableVersionRange < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Notify)

  def change
    add_column :pending_vulnerable_version_ranges, :ecosystem, :string, limit: 20, default: nil
    add_index :pending_vulnerable_version_ranges, :ecosystem
  end
end
