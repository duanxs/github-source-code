# frozen_string_literal: true

class AddRatePlanChargesColumnToPlanSubscription < GitHub::Migration
  def change
    add_column :plan_subscriptions, :zuora_rate_plan_charges, :text
  end
end
