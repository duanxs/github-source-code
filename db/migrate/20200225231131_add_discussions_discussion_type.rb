# frozen_string_literal: true

class AddDiscussionsDiscussionType < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :discussions, :discussion_type, :integer, null: false, default: 0
    add_index :discussions, [:discussion_type, :repository_id, :chosen_comment_id, :score],
      name: "idx_discussions_type_repo_chosen_comment_score"
  end
end
