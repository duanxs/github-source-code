# frozen_string_literal: true

class AddSelfHostedColumnToActionsUsageLineItems < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :actions_usage_line_items, :self_hosted, :boolean, default: false, null: false
  end
end
