# frozen_string_literal: true

class ChangeMemexProjectItemPriorityToBigInt < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Memex)

  def up
    change_column :memex_project_items, :priority, "bigint(20) UNSIGNED DEFAULT NULL"
  end

  def down
    change_column :memex_project_items, :priority, "varchar(20) DEFAULT NULL"
  end
end
