# frozen_string_literal: true

class AddDateIndexesToPrReviews < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_index :pull_request_reviews, [:pull_request_id, :created_at]
  end
end
