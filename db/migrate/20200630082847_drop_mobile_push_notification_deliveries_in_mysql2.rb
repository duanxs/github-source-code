# frozen_string_literal: true

class DropMobilePushNotificationDeliveriesInMysql2 < GitHub::Migration
  # This migration is part of https://github.com/github/notifications/issues/267
  # and is meant to drop a table in the `mysql2` cluster that we've already moved
  # into the new `notifications_deliveries` cluster.
  self.use_connection_class(ApplicationRecord::Mysql2)

  def self.up
    # We don't have multiple database clusters in Enterprise, so there's no need to drop the tables
    return if GitHub.enterprise?

    drop_table :mobile_push_notification_deliveries
  end

  def self.down
    # We don't have multiple database clusters in Enterprise
    return if GitHub.enterprise?

    create_table :mobile_push_notification_deliveries, if_not_exists: true do |t|
      t.references :mobile_device_token, null: false
      t.references :user, null: false
      t.string :list_type, null: false, limit: 64
      t.integer :list_id, null: false
      t.string :thread_type, null: false, limit: 64
      t.integer :thread_id, null: false
      t.string :comment_type, null: false, limit: 64
      t.integer :comment_id, null: false
      t.string :reason, limit: 40
      t.integer :state, null: false, default: 0, limit: 1
      t.string :state_explanation
      t.timestamps null: false
    end
    add_index(
      :mobile_push_notification_deliveries,
      [:comment_type, :comment_id, :mobile_device_token_id],
      unique: true,
      name: "index_deliveries_on_comment_and_token",
      if_not_exists: true
    )
    add_index(
      :mobile_push_notification_deliveries,
      [:state, :list_type, :list_id, :thread_type, :thread_id],
      name: "index_deliveries_on_state_and_list_and_thread",
      if_not_exists: true
    )
    add_index(
      :mobile_push_notification_deliveries,
      [:user_id, :state],
      name: "index_deliveries_on_user_and_state",
      if_not_exists: true
    )
    add_index(
      :mobile_push_notification_deliveries,
      :updated_at,
      name: "index_deliveries_on_updated_at",
      if_not_exists: true
    )
  end
end
