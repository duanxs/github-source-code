# frozen_string_literal: true

class CreateReminderEventSubscriptions < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :reminder_event_subscriptions do |t|
      t.integer :event_type, null: false
      t.integer :subscriber_id, null: false
      t.string :subscriber_type, null: false, limit: 16

      t.timestamps null: false

      t.index [:subscriber_id, :subscriber_type, :event_type], unique: true, name: :index_reminder_event_subscriptions_subscriber_and_event_type
    end
  end
end
