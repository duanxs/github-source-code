# frozen_string_literal: true

class AddUserStatusFields < GitHub::Migration
  def change
    add_column :user_statuses, :organization_id, :integer
    add_index :user_statuses, [:organization_id, :user_id]

    add_column :user_statuses, :limited_availability, :boolean, null: false, default: false
    add_index :user_statuses, :limited_availability
  end
end
