# frozen_string_literal: true

class DropRepositoryNetworkLegacyRouting < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    remove_index :repository_networks, column: [:host, :disk_usage], if_exists: true

    remove_column :repository_networks, :host, "varchar(255)", if_exists: true
    remove_column :repository_networks, :partition, "varchar(1)", if_exists: true
    remove_column :repository_networks, :backup_host, "varchar(255)", if_exists: true
  end
end
