# frozen_string_literal: true

class AddMarketplaceListingListable < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations for tips

  def change
    add_reference(:marketplace_listings, :listable, polymorphic: true)
    add_index(:marketplace_listings, [:listable_id, :listable_type], unique: true)
  end
end
