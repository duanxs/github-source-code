# frozen_string_literal: true

require "github/transitions/20190409085256_cleanup_saved_notifications_for_deleted_alerts"

class CleanupSavedNotificationsForDeletedAlertsTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::CleanupSavedNotificationsForDeletedAlerts.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
