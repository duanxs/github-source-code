# frozen_string_literal: true

class UpdatePagesSubdomainSize < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Repositories)

  def self.up
    change_column :pages, :subdomain, :string, limit: 130
  end

  def self.down
    change_column :pages, :subdomain, :string, limit: 63
  end
end
