# frozen_string_literal: true

class CreateEpochs < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :epochs, id: false do |t|
      t.column      :id, "BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY"
      t.column      :ref, "VARBINARY(1024)", null: false
      t.column      :base_commit_oid, "VARCHAR(40)", null: false
      t.references  :repository, null: false
      t.timestamps  null: false
    end
    add_index :epochs, [:repository_id, :ref, :base_commit_oid], name: :index_epochs_on_repo_branch_base_commit_oid, unique: true
  end
end
