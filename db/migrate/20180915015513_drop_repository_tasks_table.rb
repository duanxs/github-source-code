# frozen_string_literal: true

class DropRepositoryTasksTable < GitHub::Migration
  def change
    drop_table :repository_tasks
  end
end
