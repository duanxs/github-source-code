# frozen_string_literal: true

class RemoveIsUtcFromNotificationThreadTypeSubscriptions < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Mysql2)

  def self.up
    remove_column :notification_thread_type_subscriptions, :is_utc, if_exists: true
  end

  def self.down
    add_column :notification_thread_type_subscriptions, :is_utc, :boolean, default: true, null: false, if_not_exists: true

    add_index :notification_thread_type_subscriptions, :is_utc, if_not_exists: true
  end
end
