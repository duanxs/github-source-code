# frozen_string_literal: true

class CreateArInternalMetadataTable < GitHub::Migration
  def up
    # We aren't using this table but because of our prod.sql files we need to
    # add a migration for this so that we can update our regular .sql files.
    #
    # This check is prevent development environments blowing up if the table exists
    #
    # Migration code taken from https://github.com/rails/rails/blob/57a91c4d4232b5a18f1514e82e7bc8d23254cfeb/activerecord/lib/active_record/internal_metadata.rb#L34
    unless self.connection.table_exists?(:ar_internal_metadata)
      key_options = self.connection.internal_string_options_for_primary_key

      create_table :ar_internal_metadata, id: false do |t|
        t.string :key, key_options
        t.string :value
        t.timestamps null: false
      end
    end
  end

  def down
    drop_table :ar_internal_metadata if self.connection.table_exists?(:ar_internal_metadata)
  end
end
