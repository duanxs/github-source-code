# frozen_string_literal: true

class AddAnalyticsTrackingIdToUsers < GitHub::Migration
  def change
    add_column :users, :analytics_tracking_id, :string, limit: 32
    add_index :users, :analytics_tracking_id, unique: true
  end
end
