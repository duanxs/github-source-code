# frozen_string_literal: true

class CreateNonTypedExternalIdentities < GitHub::Migration
  def change
    create_table :external_identities do |t|
      t.belongs_to :user, null: true, default: nil
      t.integer    :provider_id,   null: false
      t.string     :provider_type, null: false, limit: 40
      t.string     :guid, limit: 36, null: false

      t.timestamps null: false
    end

    add_index :external_identities, :guid, unique: true
    add_index :external_identities, [:provider_id, :provider_type, :user_id], name: :index_on_provider_and_user, unique: true
    add_index :external_identities, :user_id
  end
end
