# frozen_string_literal: true
class CreateReminderDeliveryTimes < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :reminder_delivery_times do |t|
      t.integer :reminder_id, null: false
      t.integer :time, null: false
      t.column :day, :tinyint, null: false
      t.datetime :next_delivery_at, null: false

      t.timestamps null: false

      t.index [:reminder_id, :time, :day], unique: true
      t.index [:next_delivery_at]
    end
  end
end
