# frozen_string_literal: true

class UpdateIndexesOnIgnoredUsers < GitHub::Migration
  def up
    add_index :ignored_users, :expires_at
    remove_index :ignored_users, :user_id_and_ignored_id
    add_index :ignored_users, [:user_id, :ignored_id, :expires_at], unique: true
  end

  def down
    remove_index :ignored_users, :expires_at
    add_index :ignored_users, [:user_id, :ignored_id], unique: true
    remove_index :ignored_users, :user_id_and_ignored_id_and_expires_at
  end
end
