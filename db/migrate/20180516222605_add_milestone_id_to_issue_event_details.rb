# frozen_string_literal: true

class AddMilestoneIdToIssueEventDetails < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def up
    add_column :issue_event_details, :milestone_id, "int unsigned"
    change_column :issue_event_details, :issue_event_id, "int unsigned"
    add_column :archived_issue_event_details, :milestone_id, "int unsigned"
    change_column :archived_issue_event_details, :issue_event_id, "int unsigned"
  end
  def down
    remove_column :issue_event_details, :milestone_id
    change_column :issue_event_details, :issue_event_id, "int"
    remove_column :archived_issue_event_details, :milestone_id
    change_column :archived_issue_event_details, :issue_event_id, "int"
  end
end
