# frozen_string_literal: true

class AddWarningToMigratableResources < GitHub::Migration
  def self.up
    add_column :migratable_resources, :warning, :text
    add_index :migratable_resources,
      [:guid, :warning],
      length: { warning: 1 }, if_not_exists: true
  end

  def self.down
    remove_index :migratable_resources, [:guid, :warning], if_exists: true
    remove_column :migratable_resources, :warning
  end
end
