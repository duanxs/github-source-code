# frozen_string_literal: true

class CreateGhvfsFileservers < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def self.up
    create_table :ghvfs_fileservers, if_not_exists: true do |t|
      t.string  :host,       null: false, limit: 255
      t.boolean :online,     null: false, default: false
      t.boolean :embargoed,  null: false, default: false
      t.boolean :evacuating, null: false, default: false
      t.string  :datacenter, limit: 20

      # 45 is the maximum length of an IPv6 address.
      t.string  :ip, limit: 45
    end

    add_index :ghvfs_fileservers, [:datacenter], name: "index_ghvfs_fileservers_datacenter", if_not_exists: true
    add_index :ghvfs_fileservers, [:host], name: "index_ghvfs_fileservers_unique", unique: true, if_not_exists: true
  end

  def self.down
    drop_table :ghvfs_fileservers, if_exists: true
  end
end
