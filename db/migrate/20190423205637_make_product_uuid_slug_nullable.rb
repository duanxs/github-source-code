# frozen_string_literal: true

class MakeProductUUIDSlugNullable < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def change
    change_column_null(:product_uuids, :slug, true)
  end
end
