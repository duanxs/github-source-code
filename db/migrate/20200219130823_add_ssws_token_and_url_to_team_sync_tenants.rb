# frozen_string_literal: true

class AddSswsTokenAndUrlToTeamSyncTenants < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Notify)

  def change
    add_column :team_sync_tenants, :ssws_token, :string, null: true
    add_column :team_sync_tenants, :url, :text, null: true
  end
end
