# frozen_string_literal: true

class CreateBusinessSamlProviderTestSettings < GitHub::Migration
  def change
    create_table :business_saml_provider_test_settings do |t|
      t.integer :user_id, null: false
      t.integer :business_id, null: false
      t.string :sso_url, null: true
      t.string :issuer, null: true
      t.text :idp_certificate, null: true
      t.timestamps null: false
      t.integer :digest_method, null: false, default: 0
      t.integer :signature_method, null: false, default: 0
    end

    add_index :business_saml_provider_test_settings, [:user_id, :business_id, :created_at],
      name: "index_business_saml_provider_test_settings_on_user_id_business"
  end
end
