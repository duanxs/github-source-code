# frozen_string_literal: true

class AddWebPushBooleanToSettings < GitHub::Migration
  extend GitHub::SafeDatabaseMigrationHelper

  self.use_connection_class(ApplicationRecord::Mysql2)

  def self.up
    add_column :notification_user_settings, :participating_web_push, :boolean, default: false, null: false, if_not_exists: true
  end

  def self.down
    remove_column :notification_user_settings, :participating_web_push, if_exists: true
  end
end
