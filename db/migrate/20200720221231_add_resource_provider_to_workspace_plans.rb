# frozen_string_literal: true

class AddResourceProviderToWorkspacePlans < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    remove_index :workspace_plans, name: :index_on_owner_rg_and_target
    add_column :workspace_plans, :resource_provider, :string, default: "Microsoft.VSOnline", limit: 50
    add_index :workspace_plans, [:owner_id, :owner_type, :resource_group_id, :resource_provider, :vscs_target],
              name: :index_on_owner_rg_rp_and_target,
              unique: true
  end

  def down
    remove_index :workspace_plans, name: :index_on_owner_rg_rp_and_target
    remove_column :workspace_plans, :resource_provider
    add_index :workspace_plans, [:owner_id, :owner_type, :resource_group_id, :vscs_target],
              name: :index_on_owner_rg_and_target,
              unique: true
  end
end
