# frozen_string_literal: true

class AddStatusActiveIndexOnDocusignEnvelopes < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper

  self.use_connection_class(ApplicationRecord::Collab)

  def up
    add_index :docusign_envelopes,
      [:owner_id, :owner_type, :active, :status],
      name: "index_docusign_envelopes_on_owner_active_status", if_not_exists: true
    add_index :docusign_envelopes, [:status, :active], if_not_exists: true
    remove_index :docusign_envelopes, :active
    remove_index :docusign_envelopes, column: :status, if_exists: true
    remove_index :docusign_envelopes, name: "index_docusign_envelopes_on_owner_and_active", if_exists: true
  end

  def down
    remove_index :docusign_envelopes, name: "index_docusign_envelopes_on_owner_active_status", if_exists: true
    remove_index :docusign_envelopes, [:status, :active]
    remove_index :docusign_envelopes, [:active, :status]
    add_index :docusign_envelopes, :active
    add_index :docusign_envelopes, :status
    add_index :docusign_envelopes, [:owner_id, :owner_type, :active], name: "index_docusign_envelopes_on_owner_and_active", if_not_exists: true
  end
end
