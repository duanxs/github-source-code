# frozen_string_literal: true

class CreateBillingSalesServePlanSubscriptions < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :billing_sales_serve_plan_subscriptions do |t|
      t.references :customer, null: false, index: { unique: true }
      t.string :zuora_subscription_id, null: false, index: { unique: true, name: "index_billing_sales_serve_plan_subscriptions_on_zuora_sub_id" }, limit: 32
      t.string :zuora_subscription_number, null: false, limit: 32
      t.date :billing_start_date, null: false
      t.text :zuora_rate_plan_charges, null: true

      t.timestamps null: false
    end
  end
end
