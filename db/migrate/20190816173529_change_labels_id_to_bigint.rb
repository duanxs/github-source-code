# frozen_string_literal: true

class ChangeLabelsIdToBigint < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Repositories)

  def up
    change_column :labels, :id, :bigint, auto_increment: true
    change_column :archived_labels, :id, :bigint, auto_increment: true
  end
  def down
    change_column :labels, :id, :int, auto_increment: true
    change_column :archived_labels, :id, :int, auto_increment: true
  end
end
