# frozen_string_literal: true

require "github/transitions/20190815050521_add_initial_sponsors_criteria"

class AddInitialSponsorsCriteriaTransition < GitHub::Migration
  def self.up
    return unless GitHub.sponsors_enabled?
    return unless Rails.development?

    transition = GitHub::Transitions::AddInitialSponsorsCriteria.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
