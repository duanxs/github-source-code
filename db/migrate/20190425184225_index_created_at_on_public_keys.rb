# frozen_string_literal: true

class IndexCreatedAtOnPublicKeys < GitHub::Migration

  def change
    add_index :public_keys, [:accessed_at, :created_at], name: "index_public_keys_on_accessed_at_and_created_at"
  end
end
