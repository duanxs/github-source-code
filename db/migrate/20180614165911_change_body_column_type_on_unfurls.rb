# frozen_string_literal: true

class ChangeBodyColumnTypeOnUnfurls < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def up
    change_column :unfurls, :body, :mediumblob
    remove_index :unfurls, name: "unfurls_by_subject_url_hash_removed_processed_at"
    remove_column :unfurls, :removed
    add_index :unfurls, [:subject_id, :subject_type, :url_hash, :state], name: "unfurls_by_subject_url_hash_state"
  end

  def down
    change_column :unfurls, :body, :text
    add_column :unfurls, :removed, :boolean, default: false, null: false
    remove_index :unfurls, name: "unfurls_by_subject_url_hash_state"
    add_index :unfurls, [:subject_id, :subject_type, :url_hash, :state, :removed], name: "unfurls_by_subject_url_hash_removed_processed_at"
  end
end
