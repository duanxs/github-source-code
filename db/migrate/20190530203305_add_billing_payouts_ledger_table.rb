# frozen_string_literal: true

class AddBillingPayoutsLedgerTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :billing_payouts_ledger_entries do |t|
      t.references :stripe_connect_account, null: false
      t.timestamp :transaction_timestamp, null: false, default: -> { "CURRENT_TIMESTAMP" }
      t.integer :transaction_type, null: false
      t.integer :amount_in_subunits, null: false, default: 0
      t.string :currency_code, limit: 3, null: false, default: "USD"
      t.string :primary_reference_id, null: true, collation: "utf8_bin"
      t.text :additional_reference_ids, null: true

      t.index [:stripe_connect_account_id, :transaction_type], name: "index_billing_payouts_ledger_entries_on_stripe_acct_and_txn_type"
      t.index :primary_reference_id
    end
  end
end
