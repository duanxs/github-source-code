# frozen_string_literal: true

class AddActionsUsageAggregationsTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :actions_usage_aggregations do |t|
      t.references :owner, null: false
      t.references :repository, null: false
      t.references :billable_owner, polymorphic: { limit: 12 }, null: false
      t.integer :aggregate_duration_in_milliseconds, null: false
      t.timestamp :metered_billing_cycle_starts_at, null: false, default: -> { "CURRENT_TIMESTAMP" }
      t.string :job_runtime_environment, limit: 20, null: false
      t.decimal :duration_multiplier, precision: 4, scale: 2, null: false
      t.timestamps null: false

      t.index [
        :billable_owner_type,
        :billable_owner_id,
        :metered_billing_cycle_starts_at,
        :owner_id,
        :repository_id,
        :job_runtime_environment,
      ], name: "index_actions_usage_agg_on_billable_owner_and_usage", unique: true
    end
  end
end
