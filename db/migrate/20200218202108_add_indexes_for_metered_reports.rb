# frozen_string_literal: true

class AddIndexesForMeteredReports < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_index :shared_storage_artifact_aggregations,
      [:billable_owner_type, :billable_owner_id, :aggregate_effective_at, :repository_id],
      name: "index_on_billable_owner_and_usage_and_repo"

    add_index :package_registry_data_transfer_line_items,
      [:billable_owner_type, :billable_owner_id, :downloaded_at, :registry_package_id, :size_in_bytes],
      name: "index_on_billable_owner_and_usage_and_repo"
  end
end
