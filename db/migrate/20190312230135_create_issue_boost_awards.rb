# frozen_string_literal: true

class CreateIssueBoostAwards < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :issue_boost_awards do |t|
      t.belongs_to :issue_boost, null: false
      t.belongs_to :pull_request, null: false
      t.belongs_to :user, null: false
      t.column :value, "tinyint(1) unsigned", null: false, default: 1
      t.timestamps null: false
    end

    add_index :issue_boost_awards, :issue_boost_id
    add_index :issue_boost_awards, :user_id
    add_index :issue_boost_awards, :pull_request_id
  end
end
