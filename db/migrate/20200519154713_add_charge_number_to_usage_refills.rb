# frozen_string_literal: true

class AddChargeNumberToUsageRefills < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :billing_prepaid_metered_usage_refills, :zuora_rate_plan_charge_number, :string, limit: 50, null: true
    add_index :billing_prepaid_metered_usage_refills, :zuora_rate_plan_charge_number, unique: true, name: :index_on_zuora_rate_plan_charge_number
  end
end
