# frozen_string_literal: true

class AddDeletedDiscussions < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :deleted_discussions do |t|
      t.belongs_to :repository, null: false
      t.belongs_to :deleted_by, null: false
      t.belongs_to :old_discussion, null: false
      t.integer :number, null: false
      t.datetime :created_at, null: false
    end

    add_index :deleted_discussions, [:repository_id, :number], unique: true
    add_index :deleted_discussions, [:old_discussion_id, :repository_id]
  end
end
