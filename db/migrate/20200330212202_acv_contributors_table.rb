# frozen_string_literal: true

class AcvContributorsTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :acv_contributors do |t|
      t.references :repository, null: false
      t.string :contributor_email, limit: 255, null: false
      t.boolean :ignore, default: false, null: false

      t.index(
        [:contributor_email, :ignore],
        name: "index_contributor_email_ignore"
      )
      t.index(
        [:repository_id, :contributor_email],
        unique: true,
        name: "index_repository_id_contributor_email"
      )
    end
  end
end
