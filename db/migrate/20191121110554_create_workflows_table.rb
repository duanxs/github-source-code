# frozen_string_literal: true

class CreateWorkflowsTable < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    create_table :workflows do |t|
      t.string :slug, limit: 60, null: false # this will be useful for badges
      t.references :repository, null: false
      t.integer :state, null: false, default: 0 # either active or deleted. This will be mapped to an enum in the ruby model
      t.column :name, "varbinary(1024)", null: false # the name in the workflow file, or the path if not provided
      t.column :path, "varbinary(1024)", null: false # path of the workflow file (e.g. `.github/workflows/main.yml`m)

      t.datetime :deleted_at, null: true
      t.timestamps null: false
    end

    add_index :workflows, [:repository_id, :state]
  end
end
