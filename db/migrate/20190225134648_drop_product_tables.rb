# frozen_string_literal: true

class DropProductTables < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations for tips

  def up
    drop_table :product_details
    drop_table :product_variant_details
    drop_table :product_variant_subscriptions
    drop_table :product_variants
    drop_table :products
  end

  def down
    # There are no longer migrations that create these tables so don't add them
    # back here either.
  end
end
