# frozen_string_literal: true

class CreateUnfurlsTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def self.up
    create_table :unfurls do |t|
      t.references :integration
      t.references :user

      t.integer :subject_id, null: false
      t.string :subject_type, null: false, limit: 30

      t.string :url_hash, limit: 32, null: false
      t.column :title, "VARBINARY(1024)", null: true
      t.datetime :processed_at, null: true
      t.integer :state, null: false, default: 0
      t.boolean :removed, default: false, null: false

      t.text :url
      t.text :body

      t.timestamps null: false
    end

    add_index :unfurls, [:subject_id, :subject_type, :url_hash, :integration_id], unique: true, name: "unfurls_by_subject_url_hash_integration"
    add_index :unfurls, [:subject_id, :subject_type, :url_hash, :state, :removed], name: "unfurls_by_subject_url_hash_removed_processed_at"
  end

  def self.down
    drop_table :unfurls
  end
end
