# frozen_string_literal: true

class CodespacesAddUniqueIndexOnName < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_index :codespaces, [:name], unique: true
  end
end
