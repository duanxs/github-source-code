# frozen_string_literal: true

class GitbackupsWikiIncrementalsUseVarchar < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Gitbackups)

  def change
    change_column :wiki_incrementals, :checksum, "VARCHAR(48)"
  end
end
