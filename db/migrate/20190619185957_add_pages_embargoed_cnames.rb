# frozen_string_literal: true

class AddPagesEmbargoedCnames < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def self.up
    create_table :pages_embargoed_cnames, if_not_exists: true do |t|
      t.integer  :previous_owner_id, null: false
      t.integer  :previous_repository_id, null: false
      t.column  :cname, "varbinary(1024)", null: false
    end

    add_index :pages_embargoed_cnames, [:cname], name: "index_pages_embargoed_cnames_cname", if_not_exists: true
    add_index :pages_embargoed_cnames, [:previous_owner_id], name: "index_pages_embargoed_cnames_previous_owner_id", if_not_exists: true
  end

  def self.down
    drop_table :pages_embargoed_cnames, if_exists: true
  end
end
