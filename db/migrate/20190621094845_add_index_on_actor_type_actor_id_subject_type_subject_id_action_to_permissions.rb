# frozen_string_literal: true

class AddIndexOnActorTypeActorIdSubjectTypeSubjectIdActionToPermissions < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Iam)

  def change
    add_index :permissions, [:actor_id, :subject_id, :actor_type, :subject_type, :action], name: "index_permissions_on_actor_subject_and_action"
  end
end
