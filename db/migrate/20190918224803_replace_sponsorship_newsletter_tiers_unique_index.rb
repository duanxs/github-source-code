# frozen_string_literal: true

class ReplaceSponsorshipNewsletterTiersUniqueIndex < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    remove_index :sponsorship_newsletter_tiers,
      column: [:sponsorship_newsletter_id, :marketplace_listing_plan_id],
      name: :index_newsletter_and_listing_plan,
      unique: true
    add_index :sponsorship_newsletter_tiers,
      [:sponsorship_newsletter_id, :marketplace_listing_plan_id],
      name: :index_newsletter_and_listing_plan,
      unique: false

    change_column_null :sponsorship_newsletter_tiers, :marketplace_listing_plan_id, true
    change_column_null :sponsorship_newsletter_tiers, :sponsors_tier_id, false
  end
end
