# frozen_string_literal: true

class DropBookarkedIssues < GitHub::Migration
  def change
    drop_table :bookmarked_issues
  end
end
