# frozen_string_literal: true

class RegistryPackageOwnerUniqueConstraint < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Repositories)

  def up
    add_index :registry_packages, [:owner_id, :name, :package_type], unique: true, name: :index_registry_packages_on_owner_id_name_package_type
    add_index :registry_packages, [:owner_id, :name, :registry_package_type], unique: true, name: :index_registry_packages_on_owner_id_name_registry_package_type

    remove_index :registry_packages, name: "index_registry_packages_on_owner_id_and_name_and_package_type"
    remove_index :registry_packages, name: "index_packages_on_owner_id_and_name_and_registry_package_type"
  end

  def down
    add_index :registry_packages, [:owner_id, :name, :package_type], name: "index_registry_packages_on_owner_id_and_name_and_package_type"
    add_index :registry_packages, [:owner_id, :name, :registry_package_type], name: "index_packages_on_owner_id_and_name_and_registry_package_type"

    remove_index :registry_packages, name: :index_registry_packages_on_owner_id_name_package_type
    remove_index :registry_packages, name: :index_registry_packages_on_owner_id_name_registry_package_type
  end
end
