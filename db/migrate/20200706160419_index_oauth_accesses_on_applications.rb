# frozen_string_literal: true

class IndexOauthAccessesOnApplications < GitHub::Migration

  def up
    # We use a generated column which is stored on disk to allow us to
    # efficiently determine if the access is associated with a PAT or
    # an application. To do this we generate the column as:
    #
    # application_id AND 1
    #
    # The MySQL logical AND operator compares two expressions and returns
    # true if both of the expressions are true. Since PATs have an
    # application ID of 0, 0 AND 1 will be stored as the value 0
    execute <<-SQL
      ALTER TABLE oauth_accesses ADD COLUMN is_application BOOLEAN GENERATED ALWAYS AS (application_id AND 1) STORED NOT NULL
    SQL
    add_index :oauth_accesses, [:is_application, :accessed_at, :created_at],
      name: "index_oauth_accesses_on_is_application_accessed_at_created_at"
  end

  def down
    remove_index :oauth_accesses,
      name: "index_oauth_accesses_on_is_application_accessed_at_created_at"
    remove_column :oauth_accesses, :is_application
  end
end
