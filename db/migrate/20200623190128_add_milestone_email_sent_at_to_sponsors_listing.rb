# frozen_string_literal: true

class AddMilestoneEmailSentAtToSponsorsListing < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :sponsors_listings, :milestone_email_sent_at, :datetime
  end
end
