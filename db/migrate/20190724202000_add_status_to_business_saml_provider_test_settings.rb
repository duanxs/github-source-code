# frozen_string_literal: true

class AddStatusToBusinessSamlProviderTestSettings < GitHub::Migration
  def change
    add_column :business_saml_provider_test_settings, :status, :string, limit: 20
  end
end
