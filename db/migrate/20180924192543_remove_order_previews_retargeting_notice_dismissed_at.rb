# frozen_string_literal: true

class RemoveOrderPreviewsRetargetingNoticeDismissedAt < GitHub::Migration
  def up
    remove_column :marketplace_order_previews, :retargeting_notice_dismissed_at
  end

  def down
    add_column :marketplace_order_previews, :retargeting_notice_dismissed_at, :datetime, null: true
  end
end
