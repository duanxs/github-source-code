# frozen_string_literal: true

class AddServiceNameToFlipperFeatures < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/database-migrations-for-dotcom for tips

  def change
    add_column :flipper_features, :service_name, :string, null: true, default: nil
  end
end
