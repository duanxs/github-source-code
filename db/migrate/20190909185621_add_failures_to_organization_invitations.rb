# frozen_string_literal: true

class AddFailuresToOrganizationInvitations < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper

  def up
    add_column :organization_invitations, :failed_at, :datetime, null: true, if_not_exists: true
    add_column :organization_invitations, :failed_reason, :integer, null: true, if_not_exists: true
    add_index :organization_invitations,
      [:organization_id, :failed_at, :accepted_at, :cancelled_at, :role, :invitee_id],
      name: :org_id_failed_at_accepted_at_cancelled_at_role_invitee_id, if_not_exists: true
  end

  def down
    remove_index :organization_invitations, name: :org_id_failed_at_accepted_at_cancelled_at_role_invitee_id, if_exists: true
    remove_column :organization_invitations, :failed_at, if_exists: true
    remove_column :organization_invitations, :failed_reason, if_exists: true
  end
end
