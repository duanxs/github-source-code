# frozen_string_literal: true

class WikiIncrementalsIndices < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Gitbackups)

  def change
    add_index :wiki_incrementals, [:repository_id, :checksum]
  end
end
