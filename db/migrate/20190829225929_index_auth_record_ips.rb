# frozen_string_literal: true

class IndexAuthRecordIps < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_index :authentication_records, :ip_address
  end
end
