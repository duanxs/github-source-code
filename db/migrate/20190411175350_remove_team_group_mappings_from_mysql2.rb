# frozen_string_literal: true

class RemoveTeamGroupMappingsFromMysql2 < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Mysql2)

  def self.up
    drop_table :team_group_mappings, if_exists: true
  end

  def self.down
    create_table :team_group_mappings do |t|
      t.integer :tenant_id, null: false
      t.integer :team_id, null: false
      t.string :group_id, null: false, limit: 40
      t.column :group_name, "varbinary(400)", null: false
      t.column :group_description, "varbinary(2048)", null: true
      t.integer :status, null: false, default: 1
      t.datetime :synced_at, null: true
      t.timestamps null: false

      t.index :tenant_id
      t.index [:team_id, :group_id], unique: true
    end
  end
end
