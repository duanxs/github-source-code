# frozen_string_literal: true

require "github/transitions/20180702131808_convert_users_two_factor_required_to_configurable"

class ConvertUsersTwoFactorRequiredToConfigurableTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::ConvertUsersTwoFactorRequiredToConfigurable.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
