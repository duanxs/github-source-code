# frozen_string_literal: true

class IndexRepositoriesOnOwnerIdAndOrgIdAndNameAndActiveAndPublicAndDisabledAt < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def up
    add_index :repositories, [:owner_id, :organization_id, :name, :active, :public, :disabled_at], name: "owner_and_org_and_name_and_active_and_public_and_disabled_at"
  end

  def down
    remove_index :repositories, name: "owner_and_org_and_name_and_active_and_public_and_disabled_at"
  end
end
