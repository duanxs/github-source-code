# frozen_string_literal: true

class AddWorkflowIdToActionsUsageLineItems < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :actions_usage_line_items, :workflow_id, :integer

    add_index :actions_usage_line_items, :workflow_id
  end
end
