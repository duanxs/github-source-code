# frozen_string_literal: true

class AddDeviceReferenceToTwoFactorRecoveryRequest < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    add_reference :two_factor_recovery_requests, :requesting_device, references: :authenticated_device
    # we no longer lookup the current record by the created date, so we'll need
    # this new index and cleanup the old index in the next migration
    add_index :two_factor_recovery_requests, [:user_id, :requesting_device_id],
        name: "index_two_factor_recovery_requests_on_user_id_requesting_device"

    # the review_completed_at column is not well named, going to split it out into two columns
    # for now and then clean up the old column in a separate migration
    add_column :two_factor_recovery_requests, :approved_at, :datetime, null: true
    add_column :two_factor_recovery_requests, :declined_at, :datetime, null: true
  end

  def down
    remove_index :two_factor_recovery_requests, [:user_id, :requesting_device_id]
    remove_reference :two_factor_recovery_requests, :requesting_device, references: :authenticated_device

    remove_column :two_factor_recovery_requests, :approved_at
    remove_column :two_factor_recovery_requests, :declined_at
  end
end
