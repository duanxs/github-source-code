# frozen_string_literal: true

class UpdAuthenticationTokensToBigint < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def up
    change_column :authentication_tokens, :id, "bigint(20) NOT NULL AUTO_INCREMENT"
  end

  def down
    change_column :authentication_tokens, :id, "int(11) NOT NULL AUTO_INCREMENT"
  end
end
