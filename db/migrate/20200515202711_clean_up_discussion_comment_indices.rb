# frozen_string_literal: true

class CleanUpDiscussionCommentIndices < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    # Remove dead/unused indices
    remove_index :discussion_comments, name: "idx_disc_comm_repo_user_id_user_hidden"
    remove_index :discussion_comments, name: "index_discussion_comments_on_score_repo_discussion"

    # Include spammy fields in main index for listing comments:
    add_index :discussion_comments, [:discussion_id, :user_id, :user_hidden, :created_at],
      name: "index_discussion_comments_disc_id_user_id_user_hidden_created_at"
    remove_index :discussion_comments, [:discussion_id, :created_at]
  end

  def down
    add_index :discussion_comments, [:discussion_id, :created_at]
    remove_index :discussion_comments,
      name: "index_discussion_comments_disc_id_user_id_user_hidden_created_at"

    add_index :discussion_comments, [:score, :repository_id, :discussion_id],
      name: "index_discussion_comments_on_score_repo_discussion"
    add_index :discussion_comments, [:repository_id, :user_id, :user_hidden],
      name: "idx_disc_comm_repo_user_id_user_hidden"
  end
end
