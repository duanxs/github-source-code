# frozen_string_literal: true

class ChangeSubscriptionIdToBigint < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Mysql2)

  def self.up
    change_column :notification_subscription_events, :subscription_id, "bigint(11) NOT NULL"
  end

  def self.down
    change_column :notification_subscription_events, :subscription_id, "int(11) NOT NULL"
  end
end
