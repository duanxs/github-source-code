# frozen_string_literal: true

class CreateTradeControlsTravelOverrides < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :trade_controls_travel_overrides do |t|
      t.date :effective_on, null: false
      t.date :expires_on, null: false
      t.references :user, null: false

      t.timestamps null: false

      t.index [:user_id, :effective_on, :expires_on], name: "index_travel_override_user_date_range"
    end
  end
end
