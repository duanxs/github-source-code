# frozen_string_literal: true

class AddSlugAndStateToRepositoryActions < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations for tips
  extend GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Repositories)

  def self.up
    add_column :repository_actions, :state, :integer, null: false, default: 0, if_not_exists: true
    add_column :repository_actions, :slug, :string, null: true, if_not_exists: true

    add_index :repository_actions, :slug, unique: true, if_not_exists: true
    add_index :repository_actions, [:state, :slug], if_not_exists: true
  end

  def self.down
    remove_index :repository_actions, [:state, :slug], if_exists: true
    remove_index :repository_actions, :slug, if_exists: true
    remove_column :repository_actions, :slug, if_exists: true
    remove_column :repository_actions, :state, if_exists: true
  end
end
