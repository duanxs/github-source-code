# frozen_string_literal: true

class CheckSuiteRerunTimestamps < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_column :check_suites, :started_at, :datetime
    add_column :check_suites, :completed_at, :datetime
  end
end
