# frozen_string_literal: true

class AddResourceIdToVsoPlans < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :vso_plans, :resource_id, :string
  end
end
