# frozen_string_literal: true

class DeleteUnusedTables < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations for tips

  def up
    drop_table :comments
    drop_table :commit_records
    drop_table :data_hounds
    drop_table :email_messages
    drop_table :hound_logs
    drop_table :hunts
    drop_table :locations
    drop_table :required_statuses
    drop_table :vendors
  end

  def down
    # We have no intent whatsoever to bring these tables back. They also
    # don't even have any existing migration that creates them either.
  end
end
