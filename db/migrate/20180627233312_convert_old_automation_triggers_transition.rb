# frozen_string_literal: true

require "github/transitions/20180627233312_convert_old_automation_triggers"

class ConvertOldAutomationTriggersTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::ConvertOldAutomationTriggers.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
