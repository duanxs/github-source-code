# frozen_string_literal: true

class AddSignInTrackingOptOutToUsers < GitHub::Migration
  def change
    add_column :users, :opt_out_of_sign_in_tracking, :boolean, default: false, null: false
  end
end
