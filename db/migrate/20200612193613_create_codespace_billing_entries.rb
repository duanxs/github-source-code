# frozen_string_literal: true

class CreateCodespaceBillingEntries < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table(:codespace_billing_entries) do |t|
      t.references :billable_owner, null: false, polymorphic: { limit: 30 }, index: { name: "idx_codespace_billing_on_billable_owner" }
      t.references :codespace_owner, null: false, polymorphic: { limit: 30 }, index: { name: "idx_codespace_billing_on_codespace_owner" }
      t.column :codespace_guid, "char(36)", null: false
      t.string :codespace_plan_name, limit: 90, null: false
      t.timestamp :codespace_created_at
      t.timestamp :codespace_deleted_at
      t.timestamp :codespace_deprovisioned_at
      t.timestamps null: false
    end

    add_index :codespace_billing_entries, [:codespace_plan_name, :codespace_guid], unique: true, name: "idx_codespaces_billing_on_plan_name_and_guid"
  end
end
