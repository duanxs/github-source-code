# frozen_string_literal: true

class DropCheckSuitesRedundantIndex < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def up
    remove_index :check_suites, name: "index_check_suites_on_repository_id"
  end

  def down
    add_index :check_suites, [:repository_id]
  end
end
