# frozen_string_literal: true

class DropColumnRawDataFromCustomers < GitHub::Migration
  def change
    # original migration: db/migrate/20140409163627_add_raw_data_to_customers.rb
    # added type and option flag to allow reversing the migration to restore
    # to the current table definition.
    remove_column :customers, :raw_data, :binary, after: :external_uuid
  end
end
