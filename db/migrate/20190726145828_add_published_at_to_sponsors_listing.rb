# frozen_string_literal: true

class AddPublishedAtToSponsorsListing < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :sponsors_listings, :published_at, :datetime
  end
end
