# frozen_string_literal: true

class CreateSavedNotificationEntriesTable < GitHub::Migration
  extend GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Mysql2)

  def self.up
    create_table :saved_notification_entries, if_not_exists: true do |t|
      t.integer :user_id, null: false
      t.integer :summary_id, null: false
      t.string  :list_type, null: false, default: "Repository", limit: 64
      t.integer :list_id, null: false
      t.string  :thread_key, null: false, limit: 80
      t.datetime :created_at, null: false
    end

    change_column :saved_notification_entries, :id, "bigint(11) NOT NULL AUTO_INCREMENT"

    add_index :saved_notification_entries, [:user_id, :list_type, :list_id, :thread_key], unique: true,
      name: "index_saved_notification_entries_on_user_list_and_thread", if_not_exists: true

    add_index :saved_notification_entries, [:user_id, :created_at],
      name: "index_saved_notification_entries_on_user_id_and_created_at", if_not_exists: true

    add_index :saved_notification_entries, [:user_id, :list_type, :list_id, :created_at],
      name: "index_saved_notification_entries_on_user_list_and_created_at", if_not_exists: true
  end

  def self.down
    drop_table :saved_notification_entries
  end
end
