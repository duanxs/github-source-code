# frozen_string_literal: true

require "github/transitions/20190130094452_disable_invalid_email_hooks"

class DisableInvalidEmailHooksTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::DisableInvalidEmailHooks.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
