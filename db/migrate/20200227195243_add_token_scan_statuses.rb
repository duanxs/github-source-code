# frozen_string_literal: true

class AddTokenScanStatuses < GitHub::Migration
  extend GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Notify)

  def self.up
    create_table :token_scan_statuses do |t|
      t.timestamps null: false
      t.references :repository, null: false, index: { unique: true }
      t.column :scheduled_at, :datetime, null: true
      t.column :scanned_at, :datetime, null: true
      t.column :scan_state, :integer, null: false, default: 0, index: true
    end
  end

  def self.down
    drop_table :token_scan_statuses, if_exists: true
  end
end
