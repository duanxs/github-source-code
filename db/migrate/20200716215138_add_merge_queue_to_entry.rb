# frozen_string_literal: true

class AddMergeQueueToEntry < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    remove_index :merge_queue_entries, name: :index_merge_queue_entry_on_repository_and_state_and_enqueued_at

    # NOTE: need to make it `null: true` for now until we update the code
    add_column :merge_queue_entries, :merge_queue_id, :integer, null: true

    # NOTE: these are non-reversible once we have data
    #       but can be reversed after adding validations and a transition.
    change_column :merge_queue_entries, :repository_id, :integer, null: true

    add_index :merge_queue_entries, [:merge_queue_id, :pull_request_id, :enqueued_at], unique: true,
      name: :index_merge_queue_entries_on_queue_and_pr_and_enqueued_at

    add_index :merge_queue_entries, :pull_request_id
  end

  def down
    remove_index :merge_queue_entries, :pull_request_id
    remove_index :merge_queue_entries, name: :index_merge_queue_entries_on_queue_and_pr_and_enqueued_at
    remove_column :merge_queue_entries, :merge_queue_id

    add_index :merge_queue_entries, [:repository_id, :state, :enqueued_at],
      name: :index_merge_queue_entry_on_repository_and_state_and_enqueued_at
  end
end
