# frozen_string_literal: true

class CreateIntegrationManifests < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :integration_manifests do |t|
      t.string      :code, null: false, limit: 40
      t.text        :url,  null: false
      t.string      :name,  null: false
      t.belongs_to  :owner, class_name: "User",  null: false
      t.belongs_to  :creator, class_name: "User",  null: false
      t.datetime    :created_at, null: false
    end
    add_index :integration_manifests, :code, unique: true
  end
end
