# frozen_string_literal: true

class RemoveReportedReasonFromAbuseReports < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Collab)

  def self.up
    remove_index :abuse_reports, name: "index_reported_content", if_exists: true
    remove_column :abuse_reports, :reported_reason, if_exists: true
  end

  def self.down
    add_column :abuse_reports, :reported_reason, :string, limit: 30, if_not_exists: true
    add_index :abuse_reports, [:reported_content_id, :reported_content_type, :reported_reason], name: "index_reported_content", if_not_exists: true
  end
end
