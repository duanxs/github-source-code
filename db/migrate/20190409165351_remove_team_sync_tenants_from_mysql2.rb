# frozen_string_literal: true

class RemoveTeamSyncTenantsFromMysql2 < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Mysql2)

  def self.up
    drop_table :team_sync_tenants, if_exists: true
  end

  def self.down
    create_table :team_sync_tenants do |t|
      t.integer :organization_id, null: false
      t.integer :provider_type, null: false
      t.string :provider_id, null: false, limit: 100
      t.integer :status, null: false, default: 0
      t.timestamps null: false

      t.index :organization_id, unique: true
    end
  end
end
