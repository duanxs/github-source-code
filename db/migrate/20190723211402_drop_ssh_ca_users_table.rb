# frozen_string_literal: true

class DropSshCaUsersTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def self.up
    drop_table :ssh_certificate_authorities_users
  end

  def self.down
    create_table :ssh_certificate_authorities_users do |t|
      t.belongs_to :ssh_certificate_authority, null: false
      t.belongs_to :user, null: false

      t.index [:user_id, :ssh_certificate_authority_id], unique: true, name: :index_ssh_cas_users_on_user_id_and_ssh_ca_id
    end
  end
end
