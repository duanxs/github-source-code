# frozen_string_literal: true

class AddEnterpriseInstallationOwner < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations for tips

  def change
    add_column :enterprise_installations, :owner_id, :integer, null: true, after: :organization_id
    add_column :enterprise_installations, :owner_type, :string, limit: 30, null: false, default: "User", after: :owner_id
    add_index :enterprise_installations, [:owner_id, :owner_type]
  end
end
