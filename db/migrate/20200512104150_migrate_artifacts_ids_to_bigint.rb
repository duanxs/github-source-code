# frozen_string_literal: true

class MigrateArtifactsIdsToBigint < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def up
    change_column :artifacts, :check_suite_id, "bigint(11) unsigned DEFAULT NULL"
    change_column :artifacts, :check_run_id, "bigint(11) unsigned DEFAULT NULL"
  end

  def down
    change_column :artifacts, :check_suite_id, "int(11) DEFAULT NULL"
    change_column :artifacts, :check_run_id, "int(11) DEFAULT NULL"
  end
end
