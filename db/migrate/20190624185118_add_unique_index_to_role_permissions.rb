# frozen_string_literal: true

class AddUniqueIndexToRolePermissions < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Iam)

  def up
    add_index :role_permissions, [:fine_grained_permission_id, :role_id], unique: true, name: "index_role_perms_by_role_and_fgp"
  end

  def down
    remove_index :role_permissions, name: "index_role_perms_by_role_and_fgp"
  end
end
