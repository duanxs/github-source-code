# frozen_string_literal: true

class CreateCodespacesComputeUsageLineItems < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :codespaces_compute_usage_line_items, id: false do |t|
      t.column        :id, "bigint NOT NULL AUTO_INCREMENT PRIMARY KEY"
      t.belongs_to    :owner, class_name: "User",  null: false
      t.belongs_to    :actor, class_name: "User",  null: false
      t.belongs_to    :repository, class_name: "Repository",  null: false
      t.string        :billable_owner_type, null: false, limit: 12
      t.integer       :billable_owner_id, null: false
      t.boolean       :directly_billed, default: true, null: false
      t.string        :unique_billing_identifier, limit: 36, null: false
      t.string        :sku, limit: 20, null: false
      t.decimal       :duration_multiplier, precision: 4, scale: 2, null: false
      t.integer       :duration_in_seconds, null: false
      t.datetime      :start_time, null: false
      t.datetime      :end_time, null: false
      t.integer       :synchronization_batch_id, null: true
      t.column        :submission_state, "enum('unsubmitted', 'submitted', 'skipped')", null: false, default: "unsubmitted"
      t.string        :submission_state_reason, limit: 24, null: true
      t.timestamps  null: false

      t.index :owner_id
      t.index :actor_id
      t.index :start_time
      t.index :end_time
      t.index :synchronization_batch_id, name: "index_on_synchronization_batch_id"
      t.index :unique_billing_identifier, unique: true, name: "index_on_unique_billing_identifier"
      t.index [:billable_owner_type, :billable_owner_id, :duration_in_seconds], name: "index_on_billable_owner_and_usage"
      t.index [:submission_state, :submission_state_reason], name: "index_on_submission_state_and_reason"
      t.index [:submission_state, :created_at], name: "index_on_submission_state_and_created_at"
    end
  end
end
