# frozen_string_literal: true

require "github/transitions/20180614182108_default_repository_permissions_configuration"

class DefaultRepositoryPermissionsConfigurationTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::DefaultRepositoryPermissionsConfiguration.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
