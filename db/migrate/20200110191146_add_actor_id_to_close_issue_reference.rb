# frozen_string_literal: true

class AddActorIdToCloseIssueReference < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :close_issue_references, :actor_id, :integer, after: :issue_repository_id
  end
end
