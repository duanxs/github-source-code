# frozen_string_literal: true

class AddDateIndexesToIssues < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_index :issues, [:repository_id, :pull_request_id, :created_at]
    add_index :issues, [:repository_id, :pull_request_id, :closed_at]
  end
end
