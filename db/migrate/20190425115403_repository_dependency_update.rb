# frozen_string_literal: true

class RepositoryDependencyUpdate < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Notify)

  def change
    create_table :repository_dependency_updates do |t|
      t.belongs_to :repository, null: false
      t.belongs_to :pull_request, null: true
      t.belongs_to :repository_vulnerability_alert, null: true
      t.integer :state, default: 0, null: false
      t.integer :reason, default: 0, null: false
      t.integer :trigger_type, default: 0, null: false
      t.string :manifest_path, null: false
      t.string :package_name, null: false
      t.mediumblob :body, null: true
      t.mediumblob :error_body, null: true
      t.timestamps null: false
    end

    add_index :repository_dependency_updates,
              [:repository_id, :manifest_path, :package_name, :state],
              name: "index_pull_request_dependency_updates_on_repo_dep_and_state"

    add_index :repository_dependency_updates,
              :pull_request_id,
              unique: true
  end
end
