# frozen_string_literal: true

class IndexIssueEventDetailsOnSubjectIdAndSubjectType < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_index(:issue_event_details, [:subject_id, :subject_type])
  end
end
