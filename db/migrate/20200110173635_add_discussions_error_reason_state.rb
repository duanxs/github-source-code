# frozen_string_literal: true

class AddDiscussionsErrorReasonState < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :discussions, :error_reason, :int, null: false, default: 0

    add_column :discussions, :state, :int, null: false, default: 0
    add_index :discussions, [:repository_id, :state]
  end
end
