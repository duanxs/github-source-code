# frozen_string_literal: true

class RemoveSponsorshipNewsletterTiersMarketplaceListingPlanIdColumn < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    remove_column :sponsorship_newsletter_tiers, :marketplace_listing_plan_id
    remove_index :sponsorship_newsletter_tiers, name: "index_newsletter_and_listing_plan"
  end
end
