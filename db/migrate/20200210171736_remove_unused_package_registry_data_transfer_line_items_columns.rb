# frozen_string_literal: true

class RemoveUnusedPackageRegistryDataTransferLineItemsColumns < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    remove_column :package_registry_data_transfer_line_items, :repository_visibility
    remove_column :package_registry_data_transfer_line_items, :downloaded_from

    remove_index :package_registry_data_transfer_line_items, name: "index_on_billable_query"
  end

  def down
    add_column :package_registry_data_transfer_line_items,
      :repository_visibility,
      "enum('unknown', 'public', 'private')",
      null: false,
      default: "unknown"

    add_column :package_registry_data_transfer_line_items,
      :downloaded_from,
      "enum('unknown', 'actions')",
      null: false,
      default: "unknown"

    add_index :package_registry_data_transfer_line_items,
      [:billable_owner_type, :billable_owner_id, :downloaded_at, :size_in_bytes],
      name: "index_on_billable_query"
  end
end
