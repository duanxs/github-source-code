# frozen_string_literal: true

class CangeIntegrationDescriptionValueToMediumblob < GitHub::Migration
  def up
    change_column :integrations, :description, :mediumblob
  end

  def down
    change_column :integrations, :description, :text
  end
end
