# frozen_string_literal: true

class CreateWorkspaceResourceGroupsTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :workspace_resource_groups do |t|
      t.column :subscription, "char(36)", null: false
      t.string :location, limit: 40, null: false
      t.string :name, limit: 90, null: false
      t.integer :plans_count, default: 0, null: false

      t.timestamps null: false

      t.index [:subscription, :name], unique: true
      t.index [:subscription, :location, :plans_count],
        name: "index_on_sub_location_and_plans"
    end
  end
end
