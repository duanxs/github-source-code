# frozen_string_literal: true

class AddAccountJsonToStripeConnectAccount < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :stripe_connect_accounts, :stripe_account_details, :blob
  end
end
