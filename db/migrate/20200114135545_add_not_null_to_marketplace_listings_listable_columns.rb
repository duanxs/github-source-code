# frozen_string_literal: true

class AddNotNullToMarketplaceListingsListableColumns < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def change
    change_column_null :marketplace_listings, :listable_id, false
    change_column_null :marketplace_listings, :listable_type, false
  end
end
