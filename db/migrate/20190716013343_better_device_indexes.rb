# frozen_string_literal: true

class BetterDeviceIndexes < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_index :authenticated_devices, [:user_id, :approved_at]
    add_index :authenticated_devices, [:device_id, :user_id], unique: true
    remove_index :authenticated_devices, [:user_id, :device_id]
  end
end
