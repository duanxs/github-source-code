# frozen_string_literal: true

class DropUnfurlsTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def self.up
    drop_table :unfurls
  end

  def self.down
    create_table :unfurls do |t|
      t.references :integration
      t.references :user

      t.integer :subject_id, null: false
      t.string :subject_type, null: false, limit: 30
      t.integer :integration_id

      t.string :url_hash, limit: 32, null: false
      t.column :title, "VARBINARY(1024)", null: true
      t.datetime :processed_at, null: true
      t.integer :state, null: false, default: 0

      t.text :url
      t.text :body

      t.timestamps null: false
    end

    add_index :unfurls, [:subject_id, :subject_type, :url_hash, :integration_id], unique: true, name: "unfurls_by_subject_url_hash_integration"
    add_index :unfurls, [:subject_id, :subject_type, :url_hash, :state], name: "unfurls_by_subject_url_hash_state"
  end
end
