# frozen_string_literal: true

class RemovePrimaryFromGitbackupsWikiMaintenance < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Gitbackups)

  def change
    add_index    :wiki_maintenance, [:status, :scheduled_at]
    remove_index :wiki_maintenance, column: [:network_id, :repository_id, :status, :scheduled_at], name: "index_wiki_maintenance_fields"
  end
end
