# frozen_string_literal: true

class AddOldIssueIdToDeletedIssuesTable < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    add_column :deleted_issues, :old_issue_id, :integer, null: false, if_not_exists: true
    add_column :archived_deleted_issues, :old_issue_id, :integer, null: false, if_not_exists: true

    add_index :deleted_issues, [:old_issue_id, :repository_id], if_not_exists: true
  end

  def down
    remove_column :deleted_issues, :old_issue_id, if_exists: true
    remove_column :archived_deleted_issues, :old_issue_id, if_exists: true

    remove_index :deleted_issues, [:old_issue_id, :repository_id], if_exists: true
  end
end
