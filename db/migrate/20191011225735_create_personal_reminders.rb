# frozen_string_literal: true

class CreatePersonalReminders < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :personal_reminders do |t|
      t.string :time_zone_name, limit: 40, null: false # Longest known value is 28, giving some buffer

      t.integer :user_id, null: false
      t.integer :reminder_slack_workspace_id, null: false
      t.integer :remindable_id, null: false
      t.string :remindable_type, limit: 13, null: false

      t.boolean :include_review_requests, default: true, null: false
      t.boolean :include_team_review_requests, default: false, null: false
      t.integer :ignore_after_approval_count, default: 0, null: false

      t.timestamps null: false

      t.index [:reminder_slack_workspace_id]
      t.index [:remindable_type, :remindable_id]
      t.index [:user_id, :remindable_type, :remindable_id], unique: true, name: "index_personal_reminders_on_user_id_and_remindable"
    end
  end
end
