# frozen_string_literal: true

class ChangeIssuesLabelsLabelIdToBigint < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Repositories)

  def up
    change_column :issues_labels, :label_id, :bigint
  end

  def down
    change_column :issues_labels, :label_id, :int
  end
end
