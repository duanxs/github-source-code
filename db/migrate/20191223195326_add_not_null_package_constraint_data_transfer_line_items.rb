# frozen_string_literal: true

class AddNotNullPackageConstraintDataTransferLineItems < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    change_column_null :package_registry_data_transfer_line_items, :registry_package_version_id, false
    change_column_null :package_registry_data_transfer_line_items, :registry_package_id, false
  end
end
