# frozen_string_literal: true

class CreateProfileLinks < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    create_table :profile_links, id: false do |t|
      t.primary_key :id, limit: 8
      t.belongs_to :profile, null: false
      t.integer :url_type, null: false
      t.text :url, null: false
      t.timestamps null: false
    end

    add_index :profile_links, [:profile_id, :url_type], unique: true
  end
end
