# frozen_string_literal: true

class RemoveVerificationStatusFromStripeConnectAccount < GitHub::Migration
  extend GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Collab)

  def self.up
    remove_index :stripe_connect_accounts,
      name: "index_stripe_account_on_payable_and_verification", if_exists: true
    remove_column :stripe_connect_accounts, :verification_status, if_exists: true
  end

  def self.down
    add_column :stripe_connect_accounts, :verification_status, :integer, limit: 1, null: false, default: 0, if_not_exists: true
    add_index :stripe_connect_accounts,
      [:payable_type, :payable_id, :verification_status],
      name: "index_stripe_account_on_payable_and_verification", if_not_exists: true
  end
end
