# frozen_string_literal: true

class CreateIpWhitelistEntries < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :ip_whitelist_entries do |t|
      t.string     :owner_type, limit: 20, null: false # Currently "User" or "Business"
      t.integer    :owner_id, null: false
      t.string     :whitelisted_value, limit: 45, null: false # Single IPv4/IPv6 address or range of addresses in CIDR format
      t.binary     :range_from, limit: 16, null: false # Network byte order representation of first IP in whitelisted_value range
      t.binary     :range_to, limit: 16, null: false # Network byte order representation of last IP in whitelisted_value range
      t.string     :name, null: true
      t.boolean    :active, default: true, null: false

      t.timestamps null: false

      t.index [:owner_type, :owner_id, :active, :range_from, :range_to],
        name: "index_ip_whitelist_entries_on_owner_and_active_and_range_values"
    end
  end
end
