# frozen_string_literal: true

class AddVerificationStatusToStripeConnectAccounts < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :stripe_connect_accounts, :verification_status, :integer, limit: 1, null: false, default: 0
    add_index :stripe_connect_accounts, [:payable_type, :payable_id, :verification_status],
      name: "index_stripe_account_on_payable_and_verification"
  end
end
