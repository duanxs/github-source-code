# frozen_string_literal: true

class RemovePrimaryFromGitbackupsGistMaintenance < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Gitbackups)

  def change
    add_index    :gist_maintenance, [:status, :scheduled_at]
    remove_index :gist_maintenance, column: [:repo_name, :status, :scheduled_at], name: "index_gist_maintenance_fields"
  end
end
