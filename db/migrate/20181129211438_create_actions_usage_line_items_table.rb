# frozen_string_literal: true

class CreateActionsUsageLineItemsTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :actions_usage_line_items, id: false do |t|
      t.column        :id, "bigint NOT NULL AUTO_INCREMENT PRIMARY KEY"
      t.string        :run_provider, limit: 20, null: false
      t.string        :run_id, limit: 36, null: false
      t.belongs_to    :owner, class_name: "User",  null: false
      t.belongs_to    :actor, class_name: "User",  null: false
      t.belongs_to    :repository, class_name: "Repository",  null: false
      t.integer       :duration_in_milliseconds, null: false
      t.datetime      :start_time, null: false
      t.datetime      :end_time, null: false
      t.integer       :synchronization_batch_id, null: true
      t.timestamps  null: false

      t.index [:run_provider, :run_id], unique: true
      t.index :owner_id
      t.index :start_time
      t.index :end_time
      t.index :synchronization_batch_id
    end

    create_table :usage_synchronization_batches do |t|
      t.string        :product, limit: 20, null: false
      t.integer       :status, null: false
      t.string        :zuora_status_url, limit: 255, null: true
      t.string        :upload_filename, limit: 50, null: false
      t.timestamps  null: false

      t.index :status
    end
  end
end
