# frozen_string_literal: true

require "github/transitions/20190906142228_backfill_organization_labels"

class BackfillOrganizationLabelsTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::BackfillOrganizationLabels.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
