# frozen_string_literal: true

class CreateMarketplaceListingFeaturedOrganizations < GitHub::Migration
  def change
    create_table :marketplace_listing_featured_organizations do |t|
      t.belongs_to :marketplace_listing, null: false
      t.belongs_to :organization, null: false
      t.boolean :approved, null: false, default: false
      t.timestamps null: false
    end

    add_index :marketplace_listing_featured_organizations,
      %i(marketplace_listing_id organization_id),
      unique: true,
      name:   :index_marketplace_listing_featured_organizations_on_listing_org

    add_index :marketplace_listing_featured_organizations,
      :organization_id,
      name:   :index_marketplace_listing_featured_organizations_on_org_id
  end
end
