# frozen_string_literal: true

class RemovePlanFromPlanSubscriptions < GitHub::Migration
  def change
    remove_column :plan_subscriptions, :plan, :string, limit: 30
  end
end
