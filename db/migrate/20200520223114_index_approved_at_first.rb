# frozen_string_literal: true

class IndexApprovedAtFirst < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def up
    remove_index :authenticated_devices, [:accessed_at, :approved_at]
    add_index :authenticated_devices, [:approved_at, :accessed_at]
    add_index :authenticated_devices, :accessed_at
  end

  def down
    add_index :authenticated_devices, [:accessed_at, :approved_at]
    remove_index :authenticated_devices, [:approved_at, :accessed_at]
    remove_index :authenticated_devices, :accessed_at
  end
end
