# frozen_string_literal: true

class AddByGitHubToMarketplaceListings < GitHub::Migration
  def change
    add_column :marketplace_listings, :by_github, :boolean, null: false, default: false
  end
end
