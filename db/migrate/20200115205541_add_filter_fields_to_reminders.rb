# frozen_string_literal: true

class AddFilterFieldsToReminders < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :reminders, :ignored_terms, "varbinary(1024)"
    add_column :reminders, :ignored_labels, "varbinary(1024)"
    add_column :reminders, :required_labels, "varbinary(1024)"
  end
end
