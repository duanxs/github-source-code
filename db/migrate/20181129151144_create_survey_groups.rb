# frozen_string_literal: true

class CreateSurveyGroups < GitHub::Migration
  def change
    create_table :survey_groups do |t|
      t.integer :survey_id, null: false
      t.integer :user_id, null: false

      t.timestamps null: false
    end
  end
end
