# frozen_string_literal: true

class AddEmojiSupportToCompanyName < GitHub::Migration
  def up
    change_column :companies, :name, "varbinary(1024)"
  end

  def down
    change_column :companies, :name, "varchar(255)"
  end
end
