# frozen_string_literal: true

class RemoveOauthAccessColumn < GitHub::Migration
  def change
    remove_column :public_keys, :oauth_access_id
  end
end
