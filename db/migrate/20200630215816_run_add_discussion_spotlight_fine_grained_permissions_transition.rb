# frozen_string_literal: true

require "github/transitions/20200630215107_add_discussion_spotlight_fine_grained_permissions.rb"

class RunAddDiscussionSpotlightFineGrainedPermissionsTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::AddDiscussionSpotlightFineGrainedPermissions.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
