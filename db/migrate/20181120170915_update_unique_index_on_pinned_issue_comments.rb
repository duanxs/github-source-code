# frozen_string_literal: true

class UpdateUniqueIndexOnPinnedIssueComments < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    remove_index :pinned_issue_comments, [:issue_id, :issue_comment_id]
    add_index :pinned_issue_comments, :issue_id, unique: true
  end
end
