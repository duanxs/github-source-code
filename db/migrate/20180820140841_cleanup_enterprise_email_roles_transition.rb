# frozen_string_literal: true

require "github/transitions/20180820140841_cleanup_enterprise_email_roles"

class CleanupEnterpriseEmailRolesTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::CleanupEnterpriseEmailRoles.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
