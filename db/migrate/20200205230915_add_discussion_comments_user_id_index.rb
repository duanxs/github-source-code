# frozen_string_literal: true

class AddDiscussionCommentsUserIdIndex < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    add_index :discussion_comments, [:user_id, :discussion_id, :user_hidden],
      name: "idx_discussion_comments_on_user_and_discussion_and_user_hidden"
    remove_index :discussion_comments, [:user_hidden, :discussion_id]
  end

  def down
    add_index :discussion_comments, [:user_hidden, :discussion_id]
    remove_index :discussion_comments,
      name: "idx_discussion_comments_on_user_and_discussion_and_user_hidden"
  end
end
