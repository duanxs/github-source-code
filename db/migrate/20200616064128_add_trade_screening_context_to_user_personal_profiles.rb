# frozen_string_literal: true

class AddTradeScreeningContextToUserPersonalProfiles < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :user_personal_profiles, :msft_trade_screening_status, "enum('not_screened', 're_screening_in_progress', 'no_hit', 'hit_in_review', 'true_match', 'ingestion_error', 'conditions_apply_ssi_d', 'conditions_apply_ssi_d30', 'conditions_apply_ssi_e', 'conditions_apply_ssi_e60', 'conditions_apply_ssi_f', 'conditions_apply_ssi_f14', 'conditions_apply_lic_r', 'conditions_apply_lic_a')", null: false, default: "not_screened", index: { name: "index_on_msft_trade_screening_status" }
    add_column :user_personal_profiles, :last_trade_screen_date, :datetime, null: true
  end
end
