# frozen_string_literal: true

class AddPublicToPages < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_column :pages, :public, :boolean, null: false, default: true
  end
end
