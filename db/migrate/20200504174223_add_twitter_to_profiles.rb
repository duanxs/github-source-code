# frozen_string_literal: true

class AddTwitterToProfiles < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def change
    add_column :profiles, :twitter_username, :string, limit: 15
  end
end
