# frozen_string_literal: true

class AllowNullUserForPaymentMethod < GitHub::Migration
  def change
    change_column(:payment_methods, :user_id, :integer, null: true)
  end
end
