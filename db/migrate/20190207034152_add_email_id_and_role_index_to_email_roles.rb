# frozen_string_literal: true

class AddEmailIdAndRoleIndexToEmailRoles < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper

  def change
    add_index :email_roles, [:email_id, :role]
    remove_index :email_roles, column: :email_id, if_exists: true
  end
end
