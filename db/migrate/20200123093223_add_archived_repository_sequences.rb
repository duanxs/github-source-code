# frozen_string_literal: true

class AddArchivedRepositorySequences < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    create_table :archived_repository_sequences do |t|
      t.references :repository, null: false, index: { unique: true }
      t.integer :number, default: 0, null: false
      t.timestamps null: false
    end
  end
end
