# frozen_string_literal: true

class RemoveRedundantMarketplaceListingIndexes < GitHub::Migration
  def change
    remove_index :marketplace_listing_supported_languages, name: "index_supported_languages_on_listing_id"
    remove_index :marketplace_listing_screenshots, name: "index_marketplace_listing_screenshots_on_marketplace_listing_id"
  end
end
