# frozen_string_literal: true

class AddInvestigatingFieldsToZuoraWebhooks < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    reversible do |dir|
      dir.up do
        execute <<~SQL
          ALTER TABLE zuora_webhooks MODIFY status enum('pending','processed','ignored', 'investigating');
        SQL
      end

      dir.down do
        execute <<~SQL
          ALTER TABLE zuora_webhooks MODIFY status enum('pending','processed','ignored');
        SQL
      end
    end

    add_column :zuora_webhooks, :investigation_notes, :text
  end
end
