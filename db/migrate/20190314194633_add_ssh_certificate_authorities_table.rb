# frozen_string_literal: true

class AddSshCertificateAuthoritiesTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :ssh_certificate_authorities do |t|
      t.column :owner_type, "varchar(8)", null: false # "User" or "Business"
      t.integer :owner_id, null: false

      t.column :openssh_public_key, :text, null: false
      t.binary :fingerprint, null: false, limit: 32
      t.timestamps null: false

      t.index [:fingerprint], unique: true
      t.index [:owner_type, :owner_id]
    end
  end
end
