# frozen_string_literal: true

class CreateDiscussionSpotlights < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :discussion_spotlights do |t|
      t.belongs_to :repository, null: false
      t.belongs_to :discussion, null: false
      t.belongs_to :spotlighted_by, null: false
      t.integer :position, null: false, default: 1, limit: 1 # tinyint
      t.timestamps null: false
    end

    add_index :discussion_spotlights, [:repository_id, :discussion_id], unique: true
    add_index :discussion_spotlights, [:repository_id, :position], unique: true
  end
end
