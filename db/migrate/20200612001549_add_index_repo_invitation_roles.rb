# frozen_string_literal: true

class AddIndexRepoInvitationRoles < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def self.up
    add_index :repository_invitations, :role_id
  end

  def self.down
    remove_index :repository_invitations, :role_id
  end
end
