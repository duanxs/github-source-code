# frozen_string_literal: true

class CreateIntegrationAliases < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :integration_aliases do |t|
      t.belongs_to :integration, index: { unique: true }, null: false

      # See Bot::MAX_SLUG_LENGTH for more details
      t.string :slug, limit: 34, index: { unique: true }, null: false

      t.timestamps null: false
    end
  end
end
