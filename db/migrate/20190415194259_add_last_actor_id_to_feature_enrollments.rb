# frozen_string_literal: true

class AddLastActorIdToFeatureEnrollments < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :feature_enrollments, :last_actor_id, :integer, null: true
  end
end
