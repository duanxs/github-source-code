# frozen_string_literal: true

class ProvideEmojiSupportInChecks < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    change_column :check_runs, :name, "varbinary(1024)"
    change_column :check_annotations, :filename, "varbinary(1024)"
    change_column :check_annotations, :title, "varbinary(1024)"
    change_column :check_annotations, :message, :blob
  end
end
