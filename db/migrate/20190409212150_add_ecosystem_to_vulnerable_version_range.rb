# frozen_string_literal: true

class AddEcosystemToVulnerableVersionRange < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Notify)

  def change
    # Using a limit: 20 on the new ecosystem column.  Reason:
    # the vulnerability.platform column is 50 chars right now.
    # our longest ecosystem, is RubyGems, 8 chars.
    # We might add composer (8 chars), golang/dep (10 chars), cargo, and others
    # but 50 is overkill,  so cutting that down to 20.
    #
    # default is null with this initial add.
    # A later migration will disallow null here, once we transition existing data and code.
    add_column :vulnerable_version_ranges, :ecosystem, :string, limit: 20, default: nil
    # we need a single index on ecosystem for sure
    # Vulnerabilities has a combined index on status, platform, simulation, updated_at which also involves the platfor field
    # that index has marginal value, really only used for stafftools queries, which are infrequent, and still we
    # are dealing with small amounts of data.  keeping the index simple for now, just on the new ecosystem field
    add_index :vulnerable_version_ranges, :ecosystem
  end
end
