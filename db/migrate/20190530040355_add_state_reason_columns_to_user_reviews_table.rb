# frozen_string_literal: true

class AddStateReasonColumnsToUserReviewsTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :user_reviewed_files, :dismissed, :boolean, null: false, default: false
    add_column :archived_user_reviewed_files, :dismissed, :boolean, null: false, default: false

    remove_index :user_reviewed_files, column: [:pull_request_id, :user_id], name: "index_reviewed_files_on_pull_id_and_user_id"
    add_index :user_reviewed_files, [:user_id, :pull_request_id, :dismissed], name: "index_reviewed_files_on_user_pull_and_dismissed"
  end
end
