# frozen_string_literal: true

class AddBioFieldsToBusinesses < GitHub::Migration
  def change
    add_column :businesses, :description, :binary, limit: 640 # 160 char limit x 4 max bytes per char
    add_column :businesses, :website_url, :string
    add_column :businesses, :location, :string
  end
end
