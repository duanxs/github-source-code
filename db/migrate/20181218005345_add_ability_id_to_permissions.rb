# frozen_string_literal: true

class AddAbilityIdToPermissions < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Iam)

  def change
    add_column :permissions, :ability_id, :bigint

    add_index :permissions, [:subject_type, :ability_id], name: :index_permissions_by_subject_type_ability, unique: true
  end
end
