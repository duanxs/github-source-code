# frozen_string_literal: true

class AddDirectBillingToMarketplaceListingPlans < GitHub::Migration
  def change
    add_column :marketplace_listing_plans, :direct_billing, :boolean, null: false, default: false
  end
end
