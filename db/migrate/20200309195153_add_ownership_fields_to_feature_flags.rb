# frozen_string_literal: true

class AddOwnershipFieldsToFeatureFlags < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def change
    add_column :flipper_features, :github_org_team_id, :int, null: true, default: nil
    add_column :flipper_features, :slack_channel, :string, null: true, default: nil, limit: 80
  end
end
