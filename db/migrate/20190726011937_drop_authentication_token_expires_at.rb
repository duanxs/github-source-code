# frozen_string_literal: true

class DropAuthenticationTokenExpiresAt < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def change
    remove_column :authentication_tokens, :expires_at, :datetime, null: false
  end
end
