# frozen_string_literal: true

class RemoveEnterpriseInstallationOrganizationId < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def self.up
    remove_index :enterprise_installations, column: [:organization_id], if_exists: true
    remove_column :enterprise_installations, :organization_id, if_exists: true
    change_column :enterprise_installations, :owner_id, :int, null: false
  end

  def self.down
    change_column :enterprise_installations, :owner_id, :int, null: true
    add_column :enterprise_installations, :organization_id, :int, null: false, after: :license_public_key, if_not_exists: true
    add_index :enterprise_installations, [:organization_id], if_not_exists: true
  end
end
