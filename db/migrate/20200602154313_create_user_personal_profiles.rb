# frozen_string_literal: true

class CreateUserPersonalProfiles < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :user_personal_profiles do |t|
      t.string :first_name, null: false, limit: 64
      t.string :last_name, null: false, default: "", limit: 64
      t.string :middle_name, null: false, default: "", limit: 64
      t.string :region, limit: 64
      t.string :city, null: false, limit: 64
      t.string :country_code, null: false, limit: 3
      t.string :postal_code, null: false, limit: 32
      t.string :address1, null: false, limit: 128
      t.string :address2, limit: 128

      t.belongs_to :user, null: false, index: true
      t.timestamps null: false
    end
  end
end
