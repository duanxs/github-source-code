# frozen_string_literal: true

class IssuePrioritiesUnsignedMilestoneId < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def up
    change_column :issue_priorities, :milestone_id, "int unsigned", null: false
  end
  def down
    change_column :issue_priorities, :milestone_id, "int", null: false
  end
end
