# frozen_string_literal: true

class CreateSponsorsListings < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :sponsors_listings do |t|
      t.string   :slug,  null: false, index: { unique: true }
      t.integer  :state, null: false, default: 0
      t.integer  :sponsorable_type,  null: false
      t.integer  :sponsorable_id,    null: false
      t.tinyblob :short_description
      t.blob     :full_description

      t.timestamps null: false
    end

    add_index :sponsors_listings, [:sponsorable_id, :sponsorable_type], unique: true
  end
end
