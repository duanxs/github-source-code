# frozen_string_literal: true

class AddIndexOnCreatedAtAndActorTypeForPermissions < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Iam)

  def change
    add_index :permissions, [:actor_type, :created_at]
  end
end
