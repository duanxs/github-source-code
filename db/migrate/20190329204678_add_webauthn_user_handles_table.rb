# frozen_string_literal: true

class AddWebauthnUserHandlesTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :webauthn_user_handles do |t|
      t.belongs_to :user, null: false
      t.column :webauthn_user_handle, "binary(64)", null: false

      t.index [:user_id], unique: true
    end
  end
end
