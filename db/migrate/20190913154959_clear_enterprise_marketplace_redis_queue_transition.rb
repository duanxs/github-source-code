# frozen_string_literal: true

require "github/transitions/20190913154959_clear_enterprise_marketplace_redis_queue"

class ClearEnterpriseMarketplaceRedisQueueTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise?
    transition = GitHub::Transitions::ClearEnterpriseMarketplaceRedisQueue.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
