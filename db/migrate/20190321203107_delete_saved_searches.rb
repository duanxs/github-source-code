# frozen_string_literal: true

class DeleteSavedSearches < GitHub::Migration
  def up
    drop_table :saved_searches
  end
end
