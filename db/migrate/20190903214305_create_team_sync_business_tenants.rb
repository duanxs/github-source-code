# frozen_string_literal: true

class CreateTeamSyncBusinessTenants < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Notify)

  def change
    create_table :team_sync_business_tenants do |t|
      t.integer :business_id, null: false
      t.integer :provider_type, null: false
      t.string :provider_id, null: false, limit: 100
      t.integer :status, null: false, default: 0
      t.string :setup_url_template, null: true, limit: 255
      t.timestamps null: false

      t.index :business_id, unique: true
    end
  end
end
