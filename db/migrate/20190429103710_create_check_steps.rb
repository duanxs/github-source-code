# frozen_string_literal: true

class CreateCheckSteps < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    create_table :check_steps do |t|
      t.references :check_run, null: false
      t.integer :number, null: false
      t.integer :conclusion
      t.column :name, "varbinary(1024)", null: false
      t.text :completed_log_url
      t.datetime :started_at
      t.datetime :completed_at
      t.timestamps null: false
    end

    add_index :check_steps, [:check_run_id, :number], unique: true,
      name: "index_check_steps_on_check_run_id_and_number"
  end
end
