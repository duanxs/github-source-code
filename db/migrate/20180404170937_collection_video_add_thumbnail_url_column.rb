# frozen_string_literal: true

class CollectionVideoAddThumbnailUrlColumn < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def self.up
    add_column :collection_videos, :thumbnail_url, :text
  end

  def self.down
    remove_column :collection_videos, :thumbnail_url
  end
end
