# frozen_string_literal: true

class HiddenTaskListItems < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :hidden_task_list_items do |t|
      t.belongs_to :user, null: false
      t.string :removable_type, null: false, limit: 40
      t.integer :removable_id, null: false
      t.timestamp :created_at, null: false
    end

    add_index :hidden_task_list_items, [:user_id, :removable_type, :removable_id], unique: true, name: "index_on_user_id_and_removable_type_and_removable_id"
  end
end
