# frozen_string_literal: true

class CreatePartialReviewsTable < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :pull_request_review_files do |t|
      t.belongs_to :pull_request_review, null: false
      t.belongs_to :pull_request, null: false
      t.column :filepath, "varbinary(1024)", null: false
      t.column :head_sha, "varbinary(40)", null: false
      t.integer :state, null: false
      t.integer :status, null: false
      t.timestamps null: false
    end

    create_table :archived_pull_request_review_files do |t|
      t.belongs_to :pull_request_review, null: false
      t.belongs_to :pull_request, null: false
      t.column :filepath, "varbinary(1024)", null: false
      t.column :head_sha, "varbinary(40)", null: false
      t.integer :state, null: false
      t.integer :status, null: false
      t.timestamps null: false
    end

    add_index :archived_pull_request_review_files, [:pull_request_review_id], name: "index_archived_review_files_on_review_id"
    add_index :pull_request_review_files, [:pull_request_review_id, :state], name: "index_review_files_on_review_id_and_state"
    add_index :pull_request_review_files, [:pull_request_review_id, :filepath], unique: true, name: "index_review_files_on_review_id_and_filepath"
    add_index :pull_request_review_files, [:pull_request_id, :state], name: "index_review_files_on_pull_id_and_state"
  end
end
