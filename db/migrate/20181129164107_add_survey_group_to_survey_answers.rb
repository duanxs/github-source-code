# frozen_string_literal: true

class AddSurveyGroupToSurveyAnswers < GitHub::Migration
  def change
    add_column :survey_answers, :survey_group_id, :integer
    add_index :survey_answers, :survey_group_id
  end
end
