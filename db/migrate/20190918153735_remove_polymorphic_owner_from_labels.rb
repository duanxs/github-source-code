# frozen_string_literal: true

class RemovePolymorphicOwnerFromLabels < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def up
    remove_reference :archived_labels, :owner, polymorphic: { limit: 30 }, index: true

    remove_index :labels, [:owner_type, :owner_id, :name]
    remove_index :labels, [:owner_type, :owner_id, :lowercase_name]

    remove_reference :labels, :owner, polymorphic: { limit: 30 }, index: true
  end

  def down
    add_reference :labels, :owner, polymorphic: { limit: 30 }, index: true

    add_index :labels, [:owner_type, :owner_id, :name]
    add_index :labels, [:owner_type, :owner_id, :lowercase_name]

    add_reference :archived_labels, :owner, polymorphic: { limit: 30 }, index: true
  end
end
