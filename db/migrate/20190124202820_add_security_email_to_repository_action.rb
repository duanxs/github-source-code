# frozen_string_literal: true

class AddSecurityEmailToRepositoryAction < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations for tips
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_column :repository_actions, :security_email, :string, limit: 255, null: true
  end
end
