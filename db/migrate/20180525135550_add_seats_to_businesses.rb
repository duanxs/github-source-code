# frozen_string_literal: true

class AddSeatsToBusinesses < GitHub::Migration
  def change
    add_column :businesses, :seats, :integer, null: false, default: 0, after: :terms_of_service_notes
  end
end
