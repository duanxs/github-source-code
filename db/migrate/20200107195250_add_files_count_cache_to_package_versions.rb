# frozen_string_literal: true

class AddFilesCountCacheToPackageVersions < GitHub::Migration
  include GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Repositories)

  def up
    add_column :package_versions, :files_count, :integer, null: false, default: 0, if_not_exists: true
  end

  def down
    remove_column :package_versions, :files_count, if_exists: true
  end
end
