# frozen_string_literal: true

class CreateBillingMeteredUsageConfiguration < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :billing_metered_usage_configuration do |t|
      t.references :user, null: false, index: { unique: true }
      t.boolean :enforce_spending_limit, null: false, default: true
      t.integer :spending_limit_in_subunits, null: false, default: 0
      t.string :spending_limit_currency_code, null: false, limit: 3, default: "USD"

      t.timestamps null: false
    end
  end
end
