# frozen_string_literal: true

require "github/transitions/20191009155153_delete_aoo_by_priority"

class DeleteAooByPriorityTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::DeleteAooByPriority.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
