# frozen_string_literal: true

class RemovePublicKeyOptions < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations for tips

  def up
    remove_column :public_keys, :options
  end

  def down
    add_column :public_keys, :options, :string
  end
end
