# frozen_string_literal: true

require "github/transitions/20200211114821_backfill_repository_edits"

class BackfillRepositoryEditsTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?

    [
      "CommitComment",
      "IssueComment",
      "PullRequestReview",
      "PullRequestReviewComment",
      "RepositoryAdvisory",
      "RepositoryAdvisoryComment",
    ].each do |type|
      transition = GitHub::Transitions::BackfillRepositoryEdits.new(dry_run: false, type: type, "batch-size": 10000)
      transition.perform
    end
  end

  def self.down
  end
end
