# frozen_string_literal: true

class DropMarketplaceListingPlanFromPendingSubscriptionItemChanges < GitHub::Migration
  def up
    remove_index :pending_subscription_item_changes, name: :index_pending_subscription_item_changes_on_change_id_and_listing
    remove_column :pending_subscription_item_changes, :marketplace_listing_plan_id
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
