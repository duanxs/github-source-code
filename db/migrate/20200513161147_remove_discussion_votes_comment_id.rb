# frozen_string_literal: true

class RemoveDiscussionVotesCommentId < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    remove_index :discussion_votes,
      name: "index_discussion_votes_on_user_discussion_comment"
    remove_column :discussion_votes, :comment_id
    add_index :discussion_votes, [:discussion_id, :user_id], unique: true
  end

  def down
    remove_index :discussion_votes, [:discussion_id, :user_id]
    add_column :discussion_votes, :comment_id, :integer
    add_index :discussion_votes, [:user_id, :discussion_id, :comment_id],
      name: "index_discussion_votes_on_user_discussion_comment"
  end
end
