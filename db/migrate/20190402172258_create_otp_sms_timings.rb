# frozen_string_literal: true

class CreateOtpSmsTimings < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :otp_sms_timings do |t|
      t.string :timing_key, null: false, limit: 85
      t.string :provider, null: false, limit: 50
      t.integer :user_id, null: false

      t.timestamps null: false
    end

    add_index :otp_sms_timings, [:timing_key]
    add_index :otp_sms_timings, [:created_at]
  end
end
