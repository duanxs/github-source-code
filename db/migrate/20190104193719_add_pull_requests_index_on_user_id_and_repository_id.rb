# frozen_string_literal: true

class AddPullRequestsIndexOnUserIdAndRepositoryId < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Repositories)

  def up
    add_index :pull_requests, [:user_id, :repository_id]
  end

  def down
    remove_index :pull_requests, [:user_id, :repository_id]
  end
end
