# frozen_string_literal: true

class CreatePullRequestRevisions < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Domain::Repositories)

  def change
    create_table :pull_request_revisions do |t|
      t.references :pull_request, null: false
      t.integer :number, null: false
      t.boolean :ready, null: false
      t.string :base_oid, null: false, limit: 64
      t.string :head_oid, null: false, limit: 64
      t.datetime :revised_at, null: false
      t.boolean :force_pushed, null: false
      t.integer :commits_count, null: false
      t.integer :actor_id, null: false
      t.integer :push_id, null: true

      t.timestamps null: false
    end

    add_index :pull_request_revisions, [:pull_request_id, :base_oid, :head_oid, :ready], name: "index_pull_request_revisions_on_pull_and_base_and_head_and_ready"
    add_index :pull_request_revisions, [:pull_request_id, :number], name: "index_pull_request_revisions_on_pull_and_number", unique: true
    add_index :pull_request_revisions, [:pull_request_id, :ready, :number], name: "index_pull_request_revisions_on_pull_and_ready_and_number"
  end
end
