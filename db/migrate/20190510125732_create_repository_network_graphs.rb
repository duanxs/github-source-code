# frozen_string_literal: true

class CreateRepositoryNetworkGraphs < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :repository_network_graphs do |t|
      t.integer    :repository_id, null: false
      t.column     :job_status_id, "char(36)"  # SecureRandom.uuid
      t.string     :network_hash,  limit: 64   # Support a sha256 future
      t.datetime   :built_at,      null: true
      t.integer    :focus,         default: 0
      t.mediumblob :meta,          null: true
      t.mediumblob :commits_index, null: true
      t.mediumblob :commits_data,  null: true

      t.timestamps null: false
      t.index :repository_id, unique: true
      t.index :built_at
    end
  end
end
