# frozen_string_literal: true

require "github/transitions/20190328174223_delete_team_external_groups_entries"

class DeleteTeamExternalGroupsEntriesTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::DeleteTeamExternalGroupsEntries.new(dry_run: false)
    transition.perform
  end

  def self.down
    raise ActiveRecord::IrreversibleMigration
  end
end
