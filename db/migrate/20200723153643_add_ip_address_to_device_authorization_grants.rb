# frozen_string_literal: true

class AddIpAddressToDeviceAuthorizationGrants < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :device_authorization_grants, :ip, :string, limit: 40
  end
end
