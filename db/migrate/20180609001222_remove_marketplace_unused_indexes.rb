# frozen_string_literal: true

class RemoveMarketplaceUnusedIndexes < GitHub::Migration
  def change
    remove_index :marketplace_listings, name: "index_marketplace_listings_on_featured_at"
    remove_index :marketplace_listings, name: "index_marketplace_listings_on_primary_cat_and_secondary_cat"
    remove_index :marketplace_listings, name: "index_marketplace_listings_on_secondary_category_id_and_state"
  end
end
