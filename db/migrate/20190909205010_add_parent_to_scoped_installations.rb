# frozen_string_literal: true

class AddParentToScopedInstallations < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :scoped_integration_installations, :parent_type, :string,  null: true, limit: 23
    add_column :scoped_integration_installations, :parent_id,   :integer, null: true

    add_index :scoped_integration_installations, [:parent_type, :parent_id], name: "index_scoped_installations_on_parent"
  end
end
