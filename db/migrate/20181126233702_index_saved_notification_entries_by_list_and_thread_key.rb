# frozen_string_literal: true

class IndexSavedNotificationEntriesByListAndThreadKey < GitHub::Migration
  extend GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Mysql2)

  def self.up
    add_index :saved_notification_entries,
      [:list_id, :list_type, :thread_key],
      name: "index_saved_notification_entries_on_list_and_thread", if_not_exists: true
  end

  def self.down
    remove_index :saved_notification_entries,
      name: "index_saved_notification_entries_on_list_and_thread", if_exists: true
  end
end
