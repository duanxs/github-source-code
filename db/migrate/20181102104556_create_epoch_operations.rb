# frozen_string_literal: true

class CreateEpochOperations < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :epoch_operations, id: false do |t|
      t.column     :id, "BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY"
      t.column     :operation, :blob, null: false
      t.belongs_to :author, null: false
      t.column     :epoch_id, "BIGINT UNSIGNED", null: false
      t.datetime   :created_at, null: false
    end
    add_index :epoch_operations, :author_id
    add_index :epoch_operations, [:epoch_id, :created_at]
  end
end
