# frozen_string_literal: true

class CreateTradeControlsRestrictions < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :trade_controls_restrictions do |t|
      t.belongs_to :user, null: false, foreign_key: false, index: {unique: true}
      t.column :type, "enum('unrestricted', 'partial', 'full')", null: false, default: "unrestricted", index: true
      t.timestamps null: false
    end
  end
end
