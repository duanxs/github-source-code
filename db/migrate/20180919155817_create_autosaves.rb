# frozen_string_literal: true

class CreateAutosaves < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :autosaves, id: false do |t|
      t.column      :id, "bigint NOT NULL AUTO_INCREMENT PRIMARY KEY"
      t.column      :path, "VARBINARY(1024)", null: false
      t.column      :operation, :blob, null: false
      t.references  :repository, null: false
      t.column      :branch, "VARBINARY(1024)", null: false
      t.column      :sha, "VARCHAR(40)", null: false
      t.belongs_to  :author, null: false
      t.timestamps  null: false
    end
    add_index :autosaves, [:repository_id, :branch, :sha, :created_at], name: :index_autosaves_on_repo_branch_sha_created
    add_index :autosaves, [:author_id]
  end
end
