# frozen_string_literal: true

class AddBillingTypeToActionsUsageLineItem < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :actions_usage_line_items, :billing_type, :string, limit: 20, default: "card"
  end
end
