# frozen_string_literal: true

class AddRespondentsCountToSurvey < GitHub::Migration
  def change
    add_column :surveys, :respondents_count, :integer, default: 0, null: false
    add_index :surveys, :slug, unique: true
  end
end
