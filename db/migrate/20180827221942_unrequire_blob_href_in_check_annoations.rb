# frozen_string_literal: true

class UnrequireBlobHrefInCheckAnnoations < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    change_column_null(:check_annotations, :blob_href, true)
  end
end
