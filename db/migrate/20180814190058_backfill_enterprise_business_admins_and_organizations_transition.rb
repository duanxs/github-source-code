# frozen_string_literal: true

require "github/transitions/20180814190058_backfill_enterprise_business_admins_and_organizations"

class BackfillEnterpriseBusinessAdminsAndOrganizationsTransition < GitHub::Migration
  def self.up
    # should only run in GHE >= 2.15
    return unless GitHub.single_business_environment?
    User.reset_column_information
    transition = GitHub::Transitions::BackfillEnterpriseBusinessAdminsAndOrganizations.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
