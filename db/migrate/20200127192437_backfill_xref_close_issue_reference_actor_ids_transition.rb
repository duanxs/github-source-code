# frozen_string_literal: true

require "github/transitions/20200127192437_backfill_xref_close_issue_reference_actor_ids"

class BackfillXrefCloseIssueReferenceActorIdsTransition < GitHub::Migration
  def self.up
    return if !GitHub.enterprise? && !Rails.development?
    transition = GitHub::Transitions::BackfillXrefCloseIssueReferenceActorIds.new(dry_run: false)
    transition.perform
  end

  def self.down
  end
end
