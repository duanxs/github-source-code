# frozen_string_literal: true

class AddBodyToRepositoryAdvisories < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :repository_advisories, :body, :mediumblob, null: true
  end
end
