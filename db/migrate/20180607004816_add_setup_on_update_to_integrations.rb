# frozen_string_literal: true

class AddSetupOnUpdateToIntegrations < GitHub::Migration
  def change
    add_column :integrations, :setup_on_update, :boolean, default: false, null: false

    # piggyback missing NOT NULL on column "public"
    change_column :integrations, :public, :boolean, default: false, null: false
  end
end
