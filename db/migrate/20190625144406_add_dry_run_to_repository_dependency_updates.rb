# frozen_string_literal: true

class AddDryRunToRepositoryDependencyUpdates < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Notify)

  def change
    add_column :repository_dependency_updates, :dry_run, :boolean, default: false, null: false
  end
end
