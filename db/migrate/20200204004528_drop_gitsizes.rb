# frozen_string_literal: true

class DropGitsizes < GitHub::Migration
  extend GitHub::SafeDatabaseMigrationHelper
  self.use_connection_class(ApplicationRecord::Spokes)

  def self.up
    drop_table :gitsizes
  end

  def self.down
    # Does this even make sense? Can you really come back from dropping a table???

    create_table :gitsizes, id: false do |t|
      t.column :id, "INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY"

      t.timestamp :created_timestamp, null: false, default: -> { "CURRENT_TIMESTAMP" }
      t.timestamp :updated_timestamp, null: false, default: -> { "CURRENT_TIMESTAMP" }

      # Type enum: repository 0, wiki 1, gist 2.
      t.column :repository_type, "INT(11) UNSIGNED", null: false

      # Repository network ID, non-NULL for repositories
      t.column :network_id, "INT(11) UNSIGNED"

      # Repository ID, non-NULL for repositories
      t.column :repository_id, "INT(11) UNSIGNED"

      # Gist ID, non-NULL for gists
      t.column :gist_id, "INT(11) UNSIGNED"

      # The size of the largest blob object
      t.column :max_blob_size_bytes, "BIGINT UNSIGNED", null: false

      # Count of maximum number of files in any checkout
      t.column :max_checkout_blob_count, "BIGINT UNSIGNED", null: false

      # The maximum sum of file sizes in any checkout
      t.column :max_checkout_blob_size_bytes, "BIGINT UNSIGNED", null: false

      # The maximum number of symlinks in any checkout
      t.column :max_checkout_link_count, "BIGINT UNSIGNED", null: false

      # The maximum path depth in any checkout
      t.column :max_checkout_path_depth, "BIGINT UNSIGNED", null: false

      # The maximum path length in any checkout
      t.column :max_checkout_path_length, "BIGINT UNSIGNED", null: false

      # The maximum number of submodules in any checkout
      t.column :max_checkout_submodule_count, "BIGINT UNSIGNED", null: false

      # The number of directories in the largest checkout
      t.column :max_checkout_tree_count, "BIGINT UNSIGNED", null: false

      # The most parents of any single commit
      t.column :max_commit_parent_count, "BIGINT UNSIGNED", null: false

      # The size of the largest single commit
      t.column :max_commit_size_bytes, "BIGINT UNSIGNED", null: false

      # The longest chain of commits in history
      t.column :max_history_depth, "BIGINT UNSIGNED", null: false

      # The longest chain of annotated tags pointing at one another
      t.column :max_tag_depth, "BIGINT UNSIGNED", null: false

      # The most entries in any single tree
      t.column :max_tree_entries, "BIGINT UNSIGNED", null: false

      # The total number of references
      t.column :reference_count, "BIGINT UNSIGNED", null: false

      # The total number of distinct blob objects
      t.column :unique_blob_count, "BIGINT UNSIGNED", null: false

      # The total size of all distinct blob objects
      t.column :unique_blob_size_bytes, "BIGINT UNSIGNED", null: false

      # The total number of distinct commit objects
      t.column :unique_commit_count, "BIGINT UNSIGNED", null: false

      # The total size of all commit objects
      t.column :unique_commit_size_bytes, "BIGINT UNSIGNED", null: false

      # The total number of annotated tags
      t.column :unique_tag_count, "BIGINT UNSIGNED", null: false

      # The total number of distinct tree objects
      t.column :unique_tree_count, "BIGINT UNSIGNED", null: false

      # The total number of entries in all distinct tree objects
      t.column :unique_tree_entries, "BIGINT UNSIGNED", null: false

      # The total size of all distinct tree objects
      t.column :unique_tree_size_bytes, "BIGINT UNSIGNED", null: false
    end

    add_index :gitsizes, :gist_id, unique: true
    add_index :gitsizes, [:repository_type, :network_id, :repository_id, :gist_id], name: "index_gitsizes_on_ids", unique: true
  end
end
