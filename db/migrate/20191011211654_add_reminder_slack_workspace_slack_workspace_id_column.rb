# frozen_string_literal: true

class AddReminderSlackWorkspaceSlackWorkspaceIdColumn < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    change_column :reminder_slack_workspaces, :slack_id, :string, limit: 9, null: false
  end

  def down
    change_column :reminder_slack_workspaces, :slack_id, :integer, null: false
  end
end
