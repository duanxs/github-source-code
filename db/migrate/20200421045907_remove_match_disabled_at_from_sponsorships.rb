# frozen_string_literal: true

class RemoveMatchDisabledAtFromSponsorships < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def self.up
    remove_column :sponsorships, :match_disabled_at, if_exists: true
  end

  def self.down
    add_column :sponsorships, :match_disabled_at, :datetime, if_not_exists: true
  end
end
