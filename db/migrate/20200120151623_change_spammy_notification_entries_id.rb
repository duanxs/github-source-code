# frozen_string_literal: true

class ChangeSpammyNotificationEntriesId < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Mysql2)

  def up
    change_column :spammy_notification_entries, :id, "bigint(11) NOT NULL AUTO_INCREMENT"
  end

  def down
    change_column :spammy_notification_entries, :id, "int(11) NOT NULL AUTO_INCREMENT"
  end
end
