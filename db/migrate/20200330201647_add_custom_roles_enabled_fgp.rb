# frozen_string_literal: true

class AddCustomRolesEnabledFgp < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Iam)

  def self.up
    add_column :fine_grained_permissions, :custom_roles_enabled, :boolean, default: false, null: false
    add_index :fine_grained_permissions, :custom_roles_enabled
  end

  def self.down
    remove_index :fine_grained_permissions, :custom_roles_enabled
    remove_column :fine_grained_permissions, :custom_roles_enabled
  end
end
