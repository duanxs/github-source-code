# frozen_string_literal: true

class AddNumberToDiscussions < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    add_column :discussions, :number, :integer, null: false
    add_index :discussions, [:number, :repository_id], unique: true

    add_column :discussions, :issue_id, :integer
    add_index :discussions, [:issue_id, :repository_id]

    remove_column :discussions, :milestone_id
    remove_column :discussions, :state

    add_column :discussion_comments, :repository_id, :integer
    add_index :discussion_comments, [:repository_id, :created_at]
    add_index :discussion_comments, [:repository_id, :user_id, :user_hidden],
      name: "idx_disc_comm_repo_user_id_user_hidden"
  end

  def down
    remove_column :discussion_comments, :repository_id
    add_column :discussions, :state, :integer, null: false, default: 0
    add_column :discussions, :milestone_id, :integer
    remove_column :discussions, :issue_id
    remove_column :discussions, :number
  end
end
