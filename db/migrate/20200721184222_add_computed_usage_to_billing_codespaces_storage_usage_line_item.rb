# frozen_string_literal: true

class AddComputedUsageToBillingCodespacesStorageUsageLineItem < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :codespaces_storage_usage_line_items, :computed_usage, :decimal, precision: 16, scale: 3, default: 0, null: false
  end
end
