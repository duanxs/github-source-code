# frozen_string_literal: true

class AddRepoAppNameInxCheckSuites < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Ballast)

  def change
    add_index :check_suites, [:repository_id, :github_app_id, :name], name: "by_repo_app_and_name"
  end
end
