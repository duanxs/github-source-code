# frozen_string_literal: true

class CreateDiscussionReactions < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :discussion_reactions do |t|
      t.belongs_to :discussion, null: false, index: true
      t.belongs_to :user, null: false
      t.boolean :user_hidden, default: false, null: false
      t.string :content, limit: 30, null: false

      t.timestamps null: false

      t.index(
        [:discussion_id, :content, :created_at],
        name: "index_reactions_on_discussion_content_created_at",
      )
      t.index(
        [:discussion_id, :user_hidden, :created_at],
        name: "reactions_on_discussion_user_hidden_created_at",
      )
      t.index(
        [:user_id, :discussion_id, :content],
        unique: true,
        name: "index_reactions_identity",
      )
      t.index(
        [:user_id, :user_hidden],
        name: "index_reactions_on_user_hidden_and_user_id",
      )
    end
  end
end
