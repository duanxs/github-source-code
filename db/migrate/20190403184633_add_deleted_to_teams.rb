# frozen_string_literal: true

class AddDeletedToTeams < GitHub::Migration
  def change
    add_column :teams, :deleted, :boolean, null: false, default: false
    add_index :teams, [:organization_id, :deleted], name: "index_teams_on_organization_id_and_deleted"
  end
end
