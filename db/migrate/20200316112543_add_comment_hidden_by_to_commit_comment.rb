# frozen_string_literal: true

class AddCommentHiddenByToCommitComment < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips
  self.use_connection_class(ApplicationRecord::Repositories)

  def change
    add_column :commit_comments, :comment_hidden_by, :integer
    add_column :archived_commit_comments, :comment_hidden_by, :integer
  end
end
