# frozen_string_literal: true

class AddPartitionToUsageSynchronizationBatch < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    add_column :usage_synchronization_batches, :partition, :integer
    add_index :usage_synchronization_batches, [:product, :status, :partition], name: "index_usage_sync_batches_on_prod_status_partition"
  end
end
