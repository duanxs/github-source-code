# frozen_string_literal: true

class MakeRepositoryNullableOnSharedStorageArtifactAggregations < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    change_column_null :shared_storage_artifact_aggregations, :repository_id, true
  end
end
