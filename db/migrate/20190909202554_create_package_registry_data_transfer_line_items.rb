# frozen_string_literal: true

class CreatePackageRegistryDataTransferLineItems < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def change
    create_table :package_registry_data_transfer_line_items, id: false do |t|
      t.column            :id, "bigint NOT NULL AUTO_INCREMENT PRIMARY KEY"
      t.belongs_to        :owner, null: false
      t.belongs_to        :actor, null: false
      t.belongs_to        :registry_package, null: false
      t.belongs_to        :registry_package_version, null: false
      t.integer           :size_in_bytes, limit: 8, null: false
      t.integer           :synchronization_batch_id, null: false
      t.timestamp         :downloaded_at, null: false
      t.column            :downloaded_from, "enum('unknown', 'actions')", null: false, default: "unknown", index: { name: "index_on_downloaded_from" }
      t.column            :repository_visibility, "enum('unknown', 'public', 'private')", null: false, default: "unknown", index: { name: "index_on_repository_visibility" }
      t.timestamps null: false

      t.index :owner_id
      t.index :downloaded_at
      t.index :synchronization_batch_id, name: "index_on_synchronization_batch_id"
    end
  end
end
