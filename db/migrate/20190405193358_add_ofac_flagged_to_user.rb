# frozen_string_literal: true

class AddOFACFlaggedToUser < GitHub::Migration
  # See https://githubber.com/article/technology/dotcom/migrations-and-transitions/migrations for tips

  def change
    add_column :users, :ofac_flagged, :boolean, default: false, null: false
  end
end
