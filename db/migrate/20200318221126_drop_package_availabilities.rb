# frozen_string_literal: true

class DropPackageAvailabilities < GitHub::Migration
  self.use_connection_class(ApplicationRecord::Collab)

  def up
    drop_table :package_availabilities
  end

  def down
    create_table :package_availabilities do |t|
      t.integer  :package_type, null: false
      t.integer  :state, limit: 1, null: false
      t.integer  :updated_by, null: false
      t.timestamps null: false
    end

    add_index :package_availabilities, :package_type, unique: true
  end
end
