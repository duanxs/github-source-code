DROP TABLE IF EXISTS `abuse_reports`;
CREATE TABLE `abuse_reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reporting_user_id` int(11) DEFAULT NULL,
  `reported_user_id` int(11) NOT NULL,
  `reported_content_id` int(11) DEFAULT NULL,
  `reported_content_type` varchar(40) DEFAULT NULL,
  `repository_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `reason` tinyint(4) NOT NULL DEFAULT '0',
  `show_to_maintainer` tinyint(1) NOT NULL DEFAULT '0',
  `resolved` tinyint(1) NOT NULL DEFAULT '0',
  `user_hidden` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_abuse_reports_on_reported_user_id` (`reported_user_id`),
  KEY `index_abuse_reports_on_content_and_reason` (`reported_content_id`,`reported_content_type`,`reason`),
  KEY `index_abuse_reports_on_repo_and_show_to_maintainer_and_resolved` (`repository_id`,`show_to_maintainer`,`resolved`),
  KEY `index_abuse_reports_on_reporting_user_id_and_user_hidden` (`reporting_user_id`,`user_hidden`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `actions_usage_aggregations`;
CREATE TABLE `actions_usage_aggregations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `billable_owner_type` varchar(12) NOT NULL,
  `billable_owner_id` int(11) NOT NULL,
  `aggregate_duration_in_milliseconds` bigint(20) NOT NULL,
  `metered_billing_cycle_starts_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `job_runtime_environment` varchar(20) NOT NULL,
  `duration_multiplier` decimal(4,2) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_actions_usage_agg_on_billable_owner_and_usage` (`billable_owner_type`,`billable_owner_id`,`metered_billing_cycle_starts_at`,`owner_id`,`repository_id`,`job_runtime_environment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `actions_usage_line_items`;
CREATE TABLE `actions_usage_line_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `duration_in_milliseconds` int(11) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `synchronization_batch_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `job_id` varchar(36) DEFAULT NULL,
  `job_runtime_environment` varchar(20) NOT NULL,
  `duration_multiplier` decimal(4,2) NOT NULL,
  `billable_owner_type` varchar(12) DEFAULT NULL,
  `billable_owner_id` int(11) DEFAULT NULL,
  `directly_billed` tinyint(1) NOT NULL DEFAULT '1',
  `check_run_id` bigint(11) unsigned DEFAULT NULL,
  `submission_state` enum('unsubmitted','submitted','skipped') NOT NULL DEFAULT 'unsubmitted',
  `submission_state_reason` varchar(24) DEFAULT NULL,
  `workflow_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_actions_usage_line_items_on_job_id` (`job_id`),
  KEY `index_actions_usage_line_items_on_owner_id` (`owner_id`),
  KEY `index_actions_usage_line_items_on_start_time` (`start_time`),
  KEY `index_actions_usage_line_items_on_end_time` (`end_time`),
  KEY `index_on_billable_owner_and_usage` (`billable_owner_type`,`billable_owner_id`,`end_time`,`job_runtime_environment`,`duration_in_milliseconds`,`duration_multiplier`),
  KEY `index_on_synchronization_batch_id_and_end_time` (`synchronization_batch_id`,`end_time`),
  KEY `index_actions_usage_line_items_on_check_run_id` (`check_run_id`),
  KEY `index_actions_usage_line_items_on_submission_state_and_reason` (`submission_state`,`submission_state_reason`),
  KEY `index_on_submission_state_and_created_at` (`submission_state`,`created_at`),
  KEY `index_actions_usage_line_items_on_workflow_id_and_created_at` (`workflow_id`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `acv_contributors`;
CREATE TABLE `acv_contributors` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `contributor_email` varchar(255) NOT NULL,
  `ignore` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_id_contributor_email` (`repository_id`,`contributor_email`),
  KEY `index_contributor_email_ignore` (`contributor_email`,`ignore`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `advisory_credits`;
CREATE TABLE `advisory_credits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ghsa_id` varchar(19) NOT NULL,
  `accepted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `creator_id` int(11) NOT NULL,
  `declined_at` datetime DEFAULT NULL,
  `recipient_id` int(11) NOT NULL,
  `notified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_advisory_credits_on_ghsa_id_and_recipient_id` (`ghsa_id`,`recipient_id`),
  KEY `index_advisory_credits_on_ghsa_id_and_accepted_at` (`ghsa_id`,`accepted_at`),
  KEY `index_advisory_credits_on_recipient_id_and_accepted_at` (`recipient_id`,`accepted_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `application_callback_urls`;
CREATE TABLE `application_callback_urls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` blob NOT NULL,
  `application_id` int(11) NOT NULL,
  `application_type` varchar(32) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_callback_urls_on_application_and_url` (`application_id`,`application_type`,`url`(2000))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `archived_deleted_issues`;
CREATE TABLE `archived_deleted_issues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `deleted_by_id` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `old_issue_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_archived_deleted_issues_on_repository_id` (`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `archived_user_reviewed_files`;
CREATE TABLE `archived_user_reviewed_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pull_request_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `filepath` varbinary(1024) NOT NULL,
  `head_sha` varbinary(40) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `dismissed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_archived_user_reviewed_files_on_pull_request_id` (`pull_request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `attribution_invitations`;
CREATE TABLE `attribution_invitations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source_id` int(11) NOT NULL,
  `target_id` int(11) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `state` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_attribution_invitations_on_source_id` (`source_id`),
  KEY `index_attribution_invitations_on_creator_id` (`creator_id`),
  KEY `index_attribution_invitations_on_owner_id` (`owner_id`),
  KEY `index_attribution_invitations_on_target_id_and_owner_id` (`target_id`,`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `audit_log_git_event_exports`;
CREATE TABLE `audit_log_git_event_exports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `actor_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `subject_type` varchar(255) DEFAULT NULL,
  `token` varchar(255) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_audit_log_git_event_exports_on_token` (`token`),
  KEY `index_audit_log_git_event_exports_on_actor_id_and_token` (`actor_id`,`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `autosave_checkpoint_authors`;
CREATE TABLE `autosave_checkpoint_authors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `autosave_checkpoint_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_autosave_checkpoint_authors_on_autosave_checkpoint_id` (`autosave_checkpoint_id`),
  KEY `index_autosave_checkpoint_authors_on_author_id` (`author_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `autosave_checkpoints`;
CREATE TABLE `autosave_checkpoints` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `branch` varbinary(1024) NOT NULL,
  `sha` varchar(40) NOT NULL,
  `commit_sha` varchar(40) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_autosave_checkpoints_on_repo_branch_sha_created` (`repository_id`,`branch`,`sha`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `autosaves`;
CREATE TABLE `autosaves` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `operation` blob NOT NULL,
  `repository_id` int(11) NOT NULL,
  `branch` varbinary(1024) NOT NULL,
  `author_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `commit_oid` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_autosaves_on_author_id` (`author_id`),
  KEY `index_autosaves_on_repo_branch_commit_created` (`repository_id`,`branch`,`commit_oid`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `billing_budgets`;
CREATE TABLE `billing_budgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_type` varchar(12) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `enforce_spending_limit` tinyint(1) NOT NULL DEFAULT '1',
  `spending_limit_in_subunits` int(11) NOT NULL DEFAULT '0',
  `spending_limit_currency_code` varchar(3) NOT NULL DEFAULT 'USD',
  `product` enum('shared','codespaces') NOT NULL,
  `effective_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_billing_budgets_on_owner_and_product_and_effective_at` (`owner_type`,`owner_id`,`product`,`effective_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `billing_disputes`;
CREATE TABLE `billing_disputes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `platform` int(11) NOT NULL,
  `platform_dispute_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `amount_in_subunits` int(11) NOT NULL DEFAULT '0',
  `currency_code` varchar(3) NOT NULL DEFAULT 'USD',
  `reason` varchar(255) NOT NULL DEFAULT 'general',
  `status` varchar(255) NOT NULL DEFAULT 'unknown',
  `refundable` tinyint(1) NOT NULL DEFAULT '0',
  `response_due_by` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `billing_transaction_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_billing_disputes_on_platform_and_platform_dispute_id` (`platform`,`platform_dispute_id`),
  KEY `index_billing_disputes_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `billing_metered_usage_configurations`;
CREATE TABLE `billing_metered_usage_configurations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_type` varchar(12) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `enforce_spending_limit` tinyint(1) NOT NULL DEFAULT '1',
  `spending_limit_in_subunits` int(11) NOT NULL DEFAULT '0',
  `spending_limit_currency_code` varchar(3) NOT NULL DEFAULT 'USD',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_billing_metered_usage_configurations_on_owner` (`owner_type`,`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `billing_payouts_ledger_discrepancies`;
CREATE TABLE `billing_payouts_ledger_discrepancies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stripe_connect_account_id` int(11) NOT NULL,
  `status` enum('unresolved','resolved') NOT NULL DEFAULT 'unresolved',
  `discrepancy_in_subunits` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_payouts_ledger_discrepancies_on_status_and_created_at` (`status`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `billing_payouts_ledger_entries`;
CREATE TABLE `billing_payouts_ledger_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stripe_connect_account_id` int(11) NOT NULL,
  `transaction_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `transaction_type` int(11) NOT NULL,
  `amount_in_subunits` int(11) NOT NULL DEFAULT '0',
  `currency_code` varchar(3) NOT NULL DEFAULT 'USD',
  `primary_reference_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `additional_reference_ids` text,
  PRIMARY KEY (`id`),
  KEY `index_billing_payouts_ledger_entries_on_primary_reference_id` (`primary_reference_id`),
  KEY `index_ledger_entries_on_acct_id_and_txn_type_and_timestamp` (`stripe_connect_account_id`,`transaction_type`,`transaction_timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `billing_prepaid_metered_usage_refills`;
CREATE TABLE `billing_prepaid_metered_usage_refills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_type` varchar(12) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `expires_on` date NOT NULL,
  `amount_in_subunits` int(11) NOT NULL,
  `currency_code` varchar(3) NOT NULL DEFAULT 'USD',
  `zuora_rate_plan_charge_id` varchar(32) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `zuora_rate_plan_charge_number` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_billing_prepaid_metered_usage_refills_on_zuora_rpc_id` (`zuora_rate_plan_charge_id`),
  UNIQUE KEY `index_on_zuora_rate_plan_charge_number` (`zuora_rate_plan_charge_number`),
  KEY `idx_billing_prepaid_metered_usage_refills_on_owner_and_expiry` (`owner_id`,`owner_type`,`expires_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `billing_sales_serve_plan_subscriptions`;
CREATE TABLE `billing_sales_serve_plan_subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `zuora_subscription_id` varchar(32) NOT NULL,
  `zuora_subscription_number` varchar(32) NOT NULL,
  `billing_start_date` date NOT NULL,
  `zuora_rate_plan_charges` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_billing_sales_serve_plan_subscriptions_on_customer_id` (`customer_id`),
  UNIQUE KEY `index_billing_sales_serve_plan_subscriptions_on_zuora_sub_id` (`zuora_subscription_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `bulk_dmca_takedown_repositories`;
CREATE TABLE `bulk_dmca_takedown_repositories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) DEFAULT NULL,
  `bulk_dmca_takedown_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `bulk_dmca_takedown_repositories_on_repository_id` (`repository_id`),
  KEY `bulk_dmca_takedown_repositories_on_bulk_id` (`bulk_dmca_takedown_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `bulk_dmca_takedowns`;
CREATE TABLE `bulk_dmca_takedowns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `disabling_user_id` int(11) NOT NULL,
  `notice_public_url` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `bundled_license_assignments`;
CREATE TABLE `bundled_license_assignments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enterprise_agreement_number` varchar(128) NOT NULL,
  `business_id` int(11) DEFAULT NULL,
  `email` varchar(320) NOT NULL,
  `subscription_id` varchar(36) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_bundled_license_assignments_on_subscription_id` (`subscription_id`),
  KEY `index_bundled_license_assignments_on_enterprise_agreement_number` (`enterprise_agreement_number`),
  KEY `index_bundled_license_assignments_on_business_id_and_email` (`business_id`,`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `business_user_accounts`;
CREATE TABLE `business_user_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `business_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `login` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `index_business_user_accounts_on_business_id_user_id_login` (`business_id`,`user_id`,`login`),
  KEY `index_business_user_accounts_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `certificate_trusters`;
CREATE TABLE `certificate_trusters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `certificate_id` int(11) NOT NULL,
  `truster_type` varchar(255) NOT NULL,
  `truster_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_certificate_trusters_on_truster_and_certificate` (`truster_type`,`truster_id`,`certificate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `certificates`;
CREATE TABLE `certificates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serial` varbinary(20) NOT NULL,
  `issuer_digest` varbinary(32) NOT NULL,
  `subject_digest` varbinary(32) NOT NULL,
  `ski_digest` varbinary(32) DEFAULT NULL,
  `aki_digest` varbinary(32) DEFAULT NULL,
  `der` blob NOT NULL,
  `fingerprint` varbinary(32) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_certificates_on_fingerprint` (`fingerprint`),
  KEY `index_certificates_on_aki_digest` (`aki_digest`),
  KEY `index_certificates_on_issuer_digest_and_serial` (`issuer_digest`,`serial`),
  KEY `index_certificates_on_subject_digest_and_ski_digest` (`subject_digest`,`ski_digest`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `close_issue_references`;
CREATE TABLE `close_issue_references` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pull_request_id` int(11) NOT NULL,
  `issue_id` int(11) NOT NULL,
  `pull_request_author_id` int(11) NOT NULL,
  `issue_repository_id` int(11) NOT NULL,
  `actor_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `source` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_close_issue_references_on_pull_request_id_and_issue_id` (`pull_request_id`,`issue_id`),
  KEY `idx_close_issue_refs_pr_author_issue_id` (`pull_request_author_id`,`issue_id`),
  KEY `index_close_issue_references_on_issue_id_and_issue_repository_id` (`issue_id`,`issue_repository_id`),
  KEY `index_close_issue_references_on_issue_repository_id` (`issue_repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `code_search_indexed_heads`;
CREATE TABLE `code_search_indexed_heads` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `commit_oid` varchar(40) NOT NULL,
  `ref` varbinary(767) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_code_search_indexed_heads_on_repository_ref` (`repository_id`,`ref`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `code_symbol_definitions`;
CREATE TABLE `code_symbol_definitions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `symbol` varbinary(767) NOT NULL,
  `path` varbinary(767) NOT NULL,
  `line` varbinary(1024) NOT NULL,
  `docs` mediumblob,
  `kind` varchar(20) NOT NULL,
  `row` int(11) NOT NULL,
  `col` int(11) NOT NULL,
  `language_name` varchar(40) NOT NULL,
  `repository_code_symbol_index_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_code_symbol_defs_on_repo_code_symbol_index_sym_kind_path` (`repository_code_symbol_index_id`,`symbol`,`kind`,`path`,`row`,`col`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `codespace_billing_entries`;
CREATE TABLE `codespace_billing_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `billable_owner_type` varchar(30) NOT NULL,
  `billable_owner_id` int(11) NOT NULL,
  `codespace_owner_type` varchar(30) NOT NULL,
  `codespace_owner_id` int(11) NOT NULL,
  `codespace_guid` char(36) NOT NULL,
  `codespace_plan_name` varchar(90) NOT NULL,
  `codespace_created_at` timestamp NULL DEFAULT NULL,
  `codespace_deleted_at` timestamp NULL DEFAULT NULL,
  `codespace_deprovisioned_at` timestamp NULL DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `repository_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_codespaces_billing_on_plan_name_and_guid` (`codespace_plan_name`,`codespace_guid`),
  KEY `idx_codespace_billing_on_billable_owner` (`billable_owner_type`,`billable_owner_id`),
  KEY `idx_codespace_billing_on_codespace_owner` (`codespace_owner_type`,`codespace_owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `codespace_billing_messages`;
CREATE TABLE `codespace_billing_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codespace_plan_id` int(11) NOT NULL,
  `azure_storage_account_name` varchar(24) NOT NULL,
  `event_id` varchar(36) NOT NULL,
  `payload` json NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_codespace_billing_message_on_event_id` (`event_id`),
  KEY `idx_codespace_billing_message_on_codespace_plan` (`codespace_plan_id`),
  KEY `idx_codespace_billing_message_on_plan_and_created_at` (`codespace_plan_id`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `codespaces`;
CREATE TABLE `codespaces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `pull_request_id` int(11) DEFAULT NULL,
  `guid` char(36) DEFAULT NULL,
  `name` varchar(90) NOT NULL,
  `slug` varchar(100) DEFAULT NULL,
  `oid` char(40) NOT NULL,
  `ref` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `last_used_at` datetime DEFAULT NULL,
  `plan_id` int(11) DEFAULT NULL,
  `location` varchar(40) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_codespaces_on_owner_id_and_name` (`owner_id`,`name`),
  UNIQUE KEY `index_codespaces_on_repository_id_and_owner_id_and_name` (`repository_id`,`owner_id`,`name`),
  UNIQUE KEY `index_codespaces_on_name` (`name`),
  UNIQUE KEY `index_codespaces_on_guid_and_owner_id` (`guid`,`owner_id`),
  UNIQUE KEY `index_codespaces_on_slug_and_owner_id` (`slug`,`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `codespaces_compute_usage_line_items`;
CREATE TABLE `codespaces_compute_usage_line_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `billable_owner_type` varchar(12) NOT NULL,
  `billable_owner_id` int(11) NOT NULL,
  `directly_billed` tinyint(1) NOT NULL DEFAULT '1',
  `unique_billing_identifier` varchar(36) NOT NULL,
  `sku` varchar(20) NOT NULL,
  `duration_multiplier` decimal(4,2) NOT NULL,
  `duration_in_seconds` int(11) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `synchronization_batch_id` int(11) DEFAULT NULL,
  `submission_state` enum('unsubmitted','submitted','skipped') NOT NULL DEFAULT 'unsubmitted',
  `submission_state_reason` varchar(24) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `computed_usage` decimal(16,5) NOT NULL DEFAULT '0.00000',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_on_unique_billing_identifier` (`unique_billing_identifier`),
  KEY `index_codespaces_compute_usage_line_items_on_owner_id` (`owner_id`),
  KEY `index_codespaces_compute_usage_line_items_on_actor_id` (`actor_id`),
  KEY `index_codespaces_compute_usage_line_items_on_start_time` (`start_time`),
  KEY `index_codespaces_compute_usage_line_items_on_end_time` (`end_time`),
  KEY `index_on_synchronization_batch_id` (`synchronization_batch_id`),
  KEY `index_on_billable_owner_and_usage` (`billable_owner_type`,`billable_owner_id`,`duration_in_seconds`),
  KEY `index_on_submission_state_and_reason` (`submission_state`,`submission_state_reason`),
  KEY `index_on_submission_state_and_created_at` (`submission_state`,`created_at`),
  KEY `index_codespaces_compute_usage_line_items_on_sku_computed_usage` (`billable_owner_type`,`billable_owner_id`,`owner_id`,`end_time`,`sku`,`computed_usage`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `codespaces_storage_usage_line_items`;
CREATE TABLE `codespaces_storage_usage_line_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `billable_owner_type` varchar(12) NOT NULL,
  `billable_owner_id` int(11) NOT NULL,
  `directly_billed` tinyint(1) NOT NULL DEFAULT '1',
  `unique_billing_identifier` varchar(36) NOT NULL,
  `synchronization_batch_id` int(11) DEFAULT NULL,
  `submission_state` enum('unsubmitted','submitted','skipped') NOT NULL DEFAULT 'unsubmitted',
  `submission_state_reason` varchar(24) DEFAULT NULL,
  `size_in_bytes` bigint(20) NOT NULL,
  `duration_in_seconds` int(11) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `computed_usage` decimal(16,3) NOT NULL DEFAULT '0.000',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_on_unique_billing_identifier` (`unique_billing_identifier`),
  KEY `index_codespaces_storage_usage_line_items_on_owner_id` (`owner_id`),
  KEY `index_codespaces_storage_usage_line_items_on_actor_id` (`actor_id`),
  KEY `index_on_synchronization_batch_id` (`synchronization_batch_id`),
  KEY `index_on_billable_owner` (`billable_owner_type`,`billable_owner_id`),
  KEY `index_on_submission_state_and_reason` (`submission_state`,`submission_state_reason`),
  KEY `index_on_submission_state_and_created_at` (`submission_state`,`created_at`),
  KEY `index_codespaces_storage_usage_line_items_on_computed_usage` (`billable_owner_type`,`billable_owner_id`,`owner_id`,`end_time`,`computed_usage`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `commit_contributions`;
CREATE TABLE `commit_contributions` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `repository_id` int(11) unsigned DEFAULT NULL,
  `commit_count` int(11) unsigned DEFAULT '0',
  `committed_date` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `commit_contrib_user_repo_date` (`user_id`,`repository_id`,`committed_date`),
  KEY `index_commit_contributions_on_user_id_and_committed_date` (`user_id`,`committed_date`),
  KEY `index_commit_contributions_on_repository_id_and_committed_date` (`repository_id`,`committed_date`),
  KEY `index_commit_contributions_on_repository_and_user_and_date` (`repository_id`,`user_id`,`committed_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `composable_comments`;
CREATE TABLE `composable_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `integration_id` int(11) NOT NULL,
  `issue_id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_composable_comments_on_issue_id` (`issue_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `compromised_password_datasources`;
CREATE TABLE `compromised_password_datasources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `version` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `import_finished_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_compromised_password_datasources_on_name_and_version` (`name`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `compromised_password_datasources_passwords`;
CREATE TABLE `compromised_password_datasources_passwords` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `compromised_password_id` bigint(20) NOT NULL,
  `compromised_password_datasource_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_on_password_and_datasource` (`compromised_password_id`,`compromised_password_datasource_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `compromised_passwords`;
CREATE TABLE `compromised_passwords` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sha1_password` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_compromised_passwords_on_sha1_password` (`sha1_password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `deceased_users`;
CREATE TABLE `deceased_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_deceased_users_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `deleted_discussions`;
CREATE TABLE `deleted_discussions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `deleted_by_id` int(11) NOT NULL,
  `old_discussion_id` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_deleted_discussions_on_repository_id_and_number` (`repository_id`,`number`),
  KEY `index_deleted_discussions_on_old_discussion_id_and_repository_id` (`old_discussion_id`,`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `deleted_issues`;
CREATE TABLE `deleted_issues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `deleted_by_id` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `old_issue_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_deleted_issues_on_repository_id_and_number` (`repository_id`,`number`),
  KEY `index_deleted_issues_on_old_issue_id_and_repository_id` (`old_issue_id`,`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `device_authorization_grants`;
CREATE TABLE `device_authorization_grants` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `access_denied` tinyint(1) NOT NULL DEFAULT '0',
  `application_id` int(11) NOT NULL,
  `application_type` varchar(16) NOT NULL,
  `hashed_device_code` varchar(44) DEFAULT NULL,
  `device_code_last_eight` varchar(8) DEFAULT NULL,
  `scopes` text,
  `user_code` varchar(9) NOT NULL,
  `oauth_access_id` int(11) DEFAULT NULL,
  `expires_at` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `ip` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_device_authorization_grants_on_user_code` (`user_code`),
  UNIQUE KEY `index_device_authorization_grants_on_hashed_device_code` (`hashed_device_code`),
  UNIQUE KEY `index_device_authorization_grants_on_oauth_access_id` (`oauth_access_id`),
  KEY `index_device_authorization_grants_on_expires_at` (`expires_at`),
  KEY `index_device_authorization_grants_on_application` (`application_id`,`application_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `discussion_categories`;
CREATE TABLE `discussion_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `emoji` varbinary(44) NOT NULL DEFAULT ':hash:',
  `name` varbinary(512) NOT NULL,
  `description` mediumblob,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_discussion_categories_on_repository_id_and_name` (`repository_id`,`name`),
  UNIQUE KEY `index_discussion_categories_on_repository_id_and_slug` (`repository_id`,`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `discussion_comment_edits`;
CREATE TABLE `discussion_comment_edits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `discussion_comment_id` int(11) NOT NULL,
  `edited_at` datetime NOT NULL,
  `editor_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by_id` int(11) DEFAULT NULL,
  `diff` mediumblob,
  PRIMARY KEY (`id`),
  KEY `index_discussion_comment_edits_on_discussion_comment_id` (`discussion_comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `discussion_comment_reactions`;
CREATE TABLE `discussion_comment_reactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `discussion_comment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `content` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_reactions_comment_identity` (`user_id`,`discussion_comment_id`,`content`),
  KEY `index_discussion_comment_reactions_on_discussion_comment_id` (`discussion_comment_id`),
  KEY `reactions_on_discussion_comment_content_created` (`discussion_comment_id`,`content`,`created_at`),
  KEY `reactions_on_discussion_comment_hidden_created` (`discussion_comment_id`,`user_hidden`,`created_at`),
  KEY `comment_reactions_on_user_hidden_and_user_id` (`user_id`,`user_hidden`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `discussion_comments`;
CREATE TABLE `discussion_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `discussion_id` int(11) NOT NULL,
  `parent_comment_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `body` mediumblob NOT NULL,
  `user_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `comment_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `comment_hidden_reason` varbinary(1024) DEFAULT NULL,
  `comment_hidden_classifier` varchar(50) DEFAULT NULL,
  `comment_hidden_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `repository_id` int(11) DEFAULT NULL,
  `score` int(11) NOT NULL DEFAULT '0',
  `formatter` varchar(20) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_discussion_comments_on_user_id_and_user_hidden` (`user_id`,`user_hidden`),
  KEY `index_discussion_comments_on_repository_id_and_created_at` (`repository_id`,`created_at`),
  KEY `index_discussion_comments_on_parent_comment_id_and_discussion_id` (`parent_comment_id`,`discussion_id`),
  KEY `idx_discussion_comments_on_user_discussion_user_hidden_deleted` (`user_id`,`discussion_id`,`user_hidden`,`deleted_at`),
  KEY `index_discussion_comments_disc_id_user_id_user_hidden_created_at` (`discussion_id`,`user_id`,`user_hidden`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `discussion_edits`;
CREATE TABLE `discussion_edits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `discussion_id` int(11) NOT NULL,
  `edited_at` datetime NOT NULL,
  `editor_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `performed_by_integration_id` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by_id` int(11) DEFAULT NULL,
  `diff` mediumblob,
  PRIMARY KEY (`id`),
  KEY `index_discussion_edits_on_discussion_id` (`discussion_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `discussion_events`;
CREATE TABLE `discussion_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `discussion_id` int(11) NOT NULL,
  `actor_id` int(11) DEFAULT NULL,
  `comment_id` int(11) DEFAULT NULL,
  `event_type` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_discussion_events_on_repo_discussion_type_created_at` (`repository_id`,`discussion_id`,`event_type`,`created_at`),
  KEY `index_discussion_events_on_actor_id` (`actor_id`),
  KEY `index_discussion_events_on_comment_id` (`comment_id`),
  KEY `index_on_discussion_id_event_type_comment_id_created_at` (`discussion_id`,`event_type`,`comment_id`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `discussion_reactions`;
CREATE TABLE `discussion_reactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `discussion_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `content` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_reactions_identity` (`user_id`,`discussion_id`,`content`),
  KEY `index_discussion_reactions_on_discussion_id` (`discussion_id`),
  KEY `index_reactions_on_discussion_content_created_at` (`discussion_id`,`content`,`created_at`),
  KEY `reactions_on_discussion_user_hidden_created_at` (`discussion_id`,`user_hidden`,`created_at`),
  KEY `index_reactions_on_user_hidden_and_user_id` (`user_id`,`user_hidden`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `discussion_spotlights`;
CREATE TABLE `discussion_spotlights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `discussion_id` int(11) NOT NULL,
  `spotlighted_by_id` int(11) NOT NULL,
  `position` tinyint(4) NOT NULL DEFAULT '1',
  `emoji` varbinary(44) DEFAULT NULL,
  `preconfigured_color` tinyint(4) DEFAULT NULL,
  `custom_color` varchar(10) DEFAULT NULL,
  `pattern` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_discussion_spotlights_on_repository_id_and_discussion_id` (`repository_id`,`discussion_id`),
  UNIQUE KEY `index_discussion_spotlights_on_repository_id_and_position` (`repository_id`,`position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `discussion_transfers`;
CREATE TABLE `discussion_transfers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `old_repository_id` int(11) NOT NULL,
  `old_discussion_id` int(11) NOT NULL,
  `old_discussion_number` int(11) NOT NULL,
  `new_repository_id` int(11) NOT NULL,
  `new_discussion_id` int(11) NOT NULL,
  `new_discussion_event_id` int(11) DEFAULT NULL,
  `actor_id` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `reason` varbinary(1024) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_discussion_transfers_on_old_discussion_id` (`old_discussion_id`),
  KEY `index_discussion_transfers_on_new_discussion_id` (`new_discussion_id`),
  KEY `index_discussion_transfers_on_old_repo_and_old_discussion_number` (`old_repository_id`,`old_discussion_number`),
  KEY `index_discussion_transfers_on_new_discussion_event_id` (`new_discussion_event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `discussions`;
CREATE TABLE `discussions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varbinary(1024) NOT NULL,
  `body` mediumblob,
  `repository_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `chosen_comment_id` int(11) DEFAULT NULL,
  `user_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `comment_count` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `number` int(11) NOT NULL,
  `issue_id` int(11) DEFAULT NULL,
  `converted_at` datetime DEFAULT NULL,
  `score` int(11) NOT NULL DEFAULT '0',
  `error_reason` int(11) NOT NULL DEFAULT '0',
  `state` int(11) NOT NULL DEFAULT '0',
  `discussion_type` int(11) NOT NULL DEFAULT '0',
  `discussion_category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_discussions_on_number_and_repository_id` (`number`,`repository_id`),
  KEY `index_discussions_on_created_at_and_repository_id` (`created_at`,`repository_id`),
  KEY `index_discussions_on_repository_id_and_user_id_and_user_hidden` (`repository_id`,`user_id`,`user_hidden`),
  KEY `index_discussions_on_chosen_comment_id_and_user_id` (`chosen_comment_id`,`user_id`),
  KEY `index_discussions_on_user_id_and_user_hidden` (`user_id`,`user_hidden`),
  KEY `index_discussions_on_issue_id_and_repository_id` (`issue_id`,`repository_id`),
  KEY `index_discussions_on_repo_score_chosen_comment` (`repository_id`,`score`,`chosen_comment_id`),
  KEY `index_discussions_on_repository_id_and_state` (`repository_id`,`state`),
  KEY `idx_discussions_type_repo_chosen_comment_score` (`discussion_type`,`repository_id`,`chosen_comment_id`,`score`),
  KEY `index_discussions_on_repository_id_and_updated_at` (`repository_id`,`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `docusign_envelopes`;
CREATE TABLE `docusign_envelopes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_type` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `envelope_id` varchar(40) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `voided_reason` varchar(140) DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `document_type` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_docusign_envelopes_on_envelope_id` (`envelope_id`),
  KEY `index_docusign_envelopes_on_owner_active_status` (`owner_id`,`owner_type`,`active`,`status`),
  KEY `index_docusign_envelopes_on_status_and_active` (`status`,`active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `email_domain_reputation_records`;
CREATE TABLE `email_domain_reputation_records` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_domain` varchar(255) NOT NULL,
  `mx_exchange` varchar(255) DEFAULT NULL,
  `a_record` varchar(255) DEFAULT NULL,
  `sample_size` int(11) NOT NULL DEFAULT '0',
  `not_spammy_sample_size` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_email_domain_reputation_records_on_a_record` (`a_record`,`mx_exchange`,`email_domain`),
  KEY `index_email_domain_reputation_records_on_email_domain` (`email_domain`),
  KEY `index_email_domain_reputation_records_on_mx_exchange` (`mx_exchange`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `enterprise_agreements`;
CREATE TABLE `enterprise_agreements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agreement_id` varchar(128) NOT NULL,
  `business_id` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `seats` int(11) NOT NULL DEFAULT '0',
  `azure_subscription_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_enterprise_agreements_on_agreement_id` (`agreement_id`),
  UNIQUE KEY `index_enterprise_agreements_on_azure_subscription_id` (`azure_subscription_id`),
  KEY `index_enterprise_agreements_on_business_and_category_and_status` (`business_id`,`category`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `enterprise_contributions`;
CREATE TABLE `enterprise_contributions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `enterprise_installation_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `contributed_on` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_enterprise_contributions_on_user_installation_contributed` (`user_id`,`enterprise_installation_id`,`contributed_on`),
  KEY `index_enterprise_contributions_on_user_id_and_contributed_on` (`user_id`,`contributed_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `enterprise_installation_user_account_emails`;
CREATE TABLE `enterprise_installation_user_account_emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enterprise_installation_user_account_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `email` varchar(255) NOT NULL,
  `primary` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_enterprise_inst_user_account_emails_account_id_and_email` (`enterprise_installation_user_account_id`,`email`),
  KEY `idx_ent_install_ua_emails_on_ent_install_ua_id_primary_email` (`enterprise_installation_user_account_id`,`primary`,`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `enterprise_installation_user_accounts`;
CREATE TABLE `enterprise_installation_user_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enterprise_installation_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `remote_user_id` int(11) NOT NULL,
  `remote_created_at` datetime NOT NULL,
  `login` varchar(255) NOT NULL,
  `profile_name` varchar(255) DEFAULT NULL,
  `site_admin` tinyint(1) NOT NULL DEFAULT '0',
  `suspended_at` datetime DEFAULT NULL,
  `business_user_account_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_enterprise_inst_user_accounts_inst_id_and_remote_user_id` (`enterprise_installation_id`,`remote_user_id`),
  KEY `index_enterprise_installation_user_accounts_on_installation_id` (`enterprise_installation_id`),
  KEY `idx_ent_installation_user_accounts_on_business_user_account_id` (`business_user_account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `enterprise_installation_user_accounts_uploads`;
CREATE TABLE `enterprise_installation_user_accounts_uploads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `business_id` int(11) NOT NULL,
  `enterprise_installation_id` int(11) DEFAULT NULL,
  `uploader_id` int(11) DEFAULT NULL,
  `content_type` varchar(40) NOT NULL,
  `size` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `guid` varchar(36) DEFAULT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `storage_blob_id` int(11) DEFAULT NULL,
  `oid` varchar(64) DEFAULT NULL,
  `storage_provider` varchar(30) DEFAULT NULL,
  `sync_state` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_enterprise_inst_user_accounts_uploads_on_business_id` (`business_id`,`guid`),
  KEY `index_enterprise_inst_user_accounts_uploads_on_installation_id` (`enterprise_installation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `ephemeral_notices`;
CREATE TABLE `ephemeral_notices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `parent_type` varchar(100) NOT NULL,
  `notice_type` int(11) NOT NULL DEFAULT '0',
  `notice_text` varbinary(1024) NOT NULL,
  `link_to` text,
  `link_title` varbinary(1024) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_ephemeral_notice_user_idx` (`parent_id`,`parent_type`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `epoch_commits`;
CREATE TABLE `epoch_commits` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `commit_oid` varchar(40) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `version_vector` blob NOT NULL,
  `epoch_id` bigint(20) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_epoch_commits_on_epoch_id_and_created_at` (`epoch_id`,`created_at`),
  KEY `index_epoch_commits_on_repo_commit_oid` (`repository_id`,`commit_oid`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `epoch_operations`;
CREATE TABLE `epoch_operations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `operation` blob NOT NULL,
  `author_id` int(11) NOT NULL,
  `epoch_id` bigint(20) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_epoch_operations_on_author_id` (`author_id`),
  KEY `index_epoch_operations_on_epoch_id_and_created_at` (`epoch_id`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `epochs`;
CREATE TABLE `epochs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ref` varbinary(1024) NOT NULL,
  `base_commit_oid` varchar(40) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_epochs_on_repo_branch_base_commit_oid` (`repository_id`,`ref`,`base_commit_oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `external_identity_attribute_mappings`;
CREATE TABLE `external_identity_attribute_mappings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provider_type` varchar(255) DEFAULT NULL,
  `provider_id` int(11) DEFAULT NULL,
  `scheme` int(11) NOT NULL,
  `target` int(11) NOT NULL,
  `source` text NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index_on_id_attr_mapping_by_provider` (`provider_id`,`provider_type`,`scheme`,`target`),
  KEY `index_on_id_attr_mapping_by_provider` (`provider_id`,`provider_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `feature_enrollments`;
CREATE TABLE `feature_enrollments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `feature_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `enrolled` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `enrollee_type` varchar(40) NOT NULL,
  `enrollee_id` int(11) NOT NULL,
  `last_actor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_on_enrollee_id_and_enrollee_type_and_feature_id` (`enrollee_id`,`enrollee_type`,`feature_id`),
  KEY `index_feature_enrollments_on_feature_id` (`feature_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `features`;
CREATE TABLE `features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `public_name` varchar(40) NOT NULL,
  `slug` varchar(60) NOT NULL,
  `description` mediumblob,
  `feedback_link` text,
  `enrolled_by_default` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `flipper_feature_id` int(11) DEFAULT NULL,
  `legal_agreement_required` tinyint(1) NOT NULL DEFAULT '0',
  `image_link` text,
  `documentation_link` text,
  `published_at` datetime DEFAULT NULL,
  `feedback_category` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_features_on_public_name` (`public_name`),
  UNIQUE KEY `index_features_on_slug` (`slug`),
  UNIQUE KEY `index_features_on_flipper_feature_id` (`flipper_feature_id`),
  KEY `index_features_on_legal_agreement_required` (`legal_agreement_required`),
  KEY `idx_on_published_at_flipper_feature_id_legal_agreement_required` (`published_at`,`flipper_feature_id`,`legal_agreement_required`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `fraud_flagged_sponsors`;
CREATE TABLE `fraud_flagged_sponsors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sponsors_fraud_review_id` int(11) NOT NULL,
  `sponsor_id` int(11) NOT NULL,
  `matched_current_client_id` varchar(255) DEFAULT NULL,
  `matched_current_ip` varchar(40) DEFAULT NULL,
  `matched_historical_ip` varchar(40) DEFAULT NULL,
  `matched_historical_client_id` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `matched_current_ip_region_and_user_agent` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_review_and_sponsor` (`sponsors_fraud_review_id`,`sponsor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `ghvfs_fileservers`;
CREATE TABLE `ghvfs_fileservers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `host` varchar(255) NOT NULL,
  `online` tinyint(1) NOT NULL DEFAULT '0',
  `embargoed` tinyint(1) NOT NULL DEFAULT '0',
  `evacuating` tinyint(1) NOT NULL DEFAULT '0',
  `datacenter` varchar(20) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_ghvfs_fileservers_unique` (`host`),
  KEY `index_ghvfs_fileservers_datacenter` (`datacenter`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `ghvfs_replicas`;
CREATE TABLE `ghvfs_replicas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `ghvfs_fileserver_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_ghvfs_replicas_repository` (`ghvfs_fileserver_id`,`repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `hidden_task_list_items`;
CREATE TABLE `hidden_task_list_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `removable_type` varchar(40) NOT NULL,
  `removable_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_on_user_id_and_removable_type_and_removable_id` (`user_id`,`removable_type`,`removable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `imports`;
CREATE TABLE `imports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `creator_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `integration_aliases`;
CREATE TABLE `integration_aliases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `integration_id` int(11) NOT NULL,
  `slug` varchar(34) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_integration_aliases_on_integration_id` (`integration_id`),
  UNIQUE KEY `index_integration_aliases_on_slug` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `integration_manifests`;
CREATE TABLE `integration_manifests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(40) NOT NULL,
  `name` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `data` json DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_integration_manifests_on_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `integration_single_files`;
CREATE TABLE `integration_single_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `integration_version_id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_on_integration_version_id_and_path` (`integration_version_id`,`path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `interactive_component_interactions`;
CREATE TABLE `interactive_component_interactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `interactive_component_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `interacted_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `interactive_component_user_interactions_idx` (`interactive_component_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `interactive_components`;
CREATE TABLE `interactive_components` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `container_id` int(11) NOT NULL,
  `container_type` varchar(100) NOT NULL,
  `external_id` varchar(255) NOT NULL,
  `elements` blob NOT NULL,
  `container_order` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `interacted_at` datetime DEFAULT NULL,
  `outdated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_interactive_components_on_container_id_and_container_type` (`container_id`,`container_type`),
  KEY `index_interactive_components_on_outdated_at` (`outdated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `ip_whitelist_entries`;
CREATE TABLE `ip_whitelist_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_type` varchar(20) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `whitelisted_value` varchar(45) NOT NULL,
  `range_from` varbinary(16) NOT NULL,
  `range_to` varbinary(16) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_ip_whitelist_entries_on_owner_and_active_and_range_values` (`owner_type`,`owner_id`,`active`,`range_from`,`range_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `issue_boost_awards`;
CREATE TABLE `issue_boost_awards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_boost_id` int(11) NOT NULL,
  `pull_request_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `value` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_issue_boost_awards_on_issue_boost_id` (`issue_boost_id`),
  KEY `index_issue_boost_awards_on_user_id` (`user_id`),
  KEY `index_issue_boost_awards_on_pull_request_id` (`pull_request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `issue_boosts`;
CREATE TABLE `issue_boosts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `issue_id` int(11) NOT NULL,
  `value` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_issue_boosts_on_issue_id` (`issue_id`),
  KEY `index_issue_boosts_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `issue_transfers`;
CREATE TABLE `issue_transfers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `old_repository_id` int(11) NOT NULL,
  `old_issue_id` int(11) NOT NULL,
  `old_issue_number` int(11) NOT NULL,
  `new_repository_id` int(11) NOT NULL,
  `new_issue_id` int(11) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `state` varchar(16) NOT NULL,
  `reason` varbinary(1024) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_issue_transfers_on_old_issue_id` (`old_issue_id`),
  KEY `index_issue_transfers_on_old_repository_id_and_old_issue_number` (`old_repository_id`,`old_issue_number`),
  KEY `index_issue_transfers_on_new_issue_id` (`new_issue_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `key_links`;
CREATE TABLE `key_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key_prefix` varchar(15) NOT NULL,
  `url_template` text NOT NULL,
  `owner_type` varchar(255) NOT NULL,
  `owner_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_key_links_on_owner_id_and_owner_type_and_key_prefix` (`owner_id`,`owner_type`,`key_prefix`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `markdown_components`;
CREATE TABLE `markdown_components` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `container_id` int(11) NOT NULL,
  `container_type` varchar(100) NOT NULL,
  `body` blob NOT NULL,
  `container_order` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_markdown_components_on_container_id_and_container_type` (`container_id`,`container_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `marketplace_blog_posts`;
CREATE TABLE `marketplace_blog_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varbinary(200) NOT NULL,
  `external_post_id` int(11) NOT NULL,
  `url` varchar(175) NOT NULL,
  `description` varbinary(700) DEFAULT NULL,
  `author` varchar(75) NOT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `published_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_marketplace_blog_posts_on_external_post_id` (`external_post_id`),
  KEY `index_marketplace_blog_posts_on_featured_and_published_at` (`featured`,`published_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `merge_group_entries`;
CREATE TABLE `merge_group_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `merge_queue_id` int(11) NOT NULL,
  `merge_group_id` int(11) NOT NULL,
  `merge_queue_entry_id` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `retries` int(11) NOT NULL DEFAULT '0',
  `head_oid` varchar(40) DEFAULT NULL,
  `head_ref` varbinary(1024) DEFAULT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `merge_group_queue_entry_id` (`merge_group_id`,`merge_queue_entry_id`),
  KEY `queue_id_queue_entry_id` (`merge_queue_id`,`merge_queue_entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `merge_groups`;
CREATE TABLE `merge_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `merge_queue_id` int(11) NOT NULL,
  `base_oid` varchar(40) DEFAULT NULL,
  `locked` tinyint(1) DEFAULT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `merge_group_entries_count` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_merge_groups_on_merge_queue_id_and_locked` (`merge_queue_id`,`locked`),
  KEY `queue_id_state_entry_count` (`merge_queue_id`,`state`,`merge_group_entries_count`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `merge_queue_entries`;
CREATE TABLE `merge_queue_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) DEFAULT NULL,
  `pull_request_id` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `enqueuer_id` int(11) NOT NULL,
  `enqueued_at` datetime NOT NULL,
  `dequeuer_id` int(11) DEFAULT NULL,
  `dequeued_at` datetime DEFAULT NULL,
  `dequeue_reason` int(11) DEFAULT NULL,
  `deploy_started_at` datetime DEFAULT NULL,
  `solo` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `merge_queue_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_merge_queue_entries_on_queue_and_pr_and_enqueued_at` (`merge_queue_id`,`pull_request_id`,`enqueued_at`),
  KEY `index_merge_queue_entries_on_pull_request_id` (`pull_request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `merge_queues`;
CREATE TABLE `merge_queues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `branch` varbinary(1024) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_merge_queues_on_repository_id_and_branch` (`repository_id`,`branch`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `metered_usage_exports`;
CREATE TABLE `metered_usage_exports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `starts_on` date NOT NULL,
  `ends_on` date NOT NULL,
  `filename` varchar(255) NOT NULL,
  `billable_owner_type` varchar(12) NOT NULL,
  `billable_owner_id` int(11) NOT NULL,
  `requester_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_metered_usage_exports_on_filename` (`filename`),
  KEY `idx_metered_usage_exports_billable_owner` (`billable_owner_type`,`billable_owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `ofac_downgrades`;
CREATE TABLE `ofac_downgrades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `downgrade_on` date DEFAULT NULL,
  `is_complete` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_ofac_downgrades_on_user_id` (`user_id`),
  KEY `index_ofac_downgrades_on_downgrade_on` (`downgrade_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `organization_discussion_post_replies`;
CREATE TABLE `organization_discussion_post_replies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` int(11) NOT NULL,
  `body` mediumblob NOT NULL,
  `formatter` varchar(20) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `organization_discussion_post_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_org_disc_replies_organization_discussion_post_id_and_number` (`organization_discussion_post_id`,`number`),
  KEY `index_organization_discussion_post_replies_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `organization_discussion_posts`;
CREATE TABLE `organization_discussion_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` int(11) NOT NULL,
  `title` varbinary(1024) NOT NULL,
  `body` mediumblob NOT NULL,
  `formatter` varchar(20) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `organization_id` int(11) NOT NULL,
  `pinned_at` datetime DEFAULT NULL,
  `pinned_by_user_id` int(11) DEFAULT NULL,
  `private` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_org_discussion_posts_on_organization_id_and_number` (`organization_id`,`number`),
  KEY `index_organization_discussion_posts_on_user_id` (`user_id`),
  KEY `idx_org_discussion_posts_on_organization_id_and_pinned_at` (`organization_id`,`pinned_at`),
  KEY `idx_org_discussion_posts_on_organization_id_and_private` (`organization_id`,`private`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `organization_profiles`;
CREATE TABLE `organization_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sponsors_update_email` varchar(255) DEFAULT NULL,
  `organization_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_organization_profiles_on_organization_id` (`organization_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `otp_sms_timings`;
CREATE TABLE `otp_sms_timings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timing_key` varchar(85) NOT NULL,
  `provider` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_otp_sms_timings_on_timing_key` (`timing_key`),
  KEY `index_otp_sms_timings_on_created_at` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `package_registry_data_transfer_aggregations`;
CREATE TABLE `package_registry_data_transfer_aggregations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `registry_package_id` int(11) NOT NULL,
  `registry_package_version_id` int(11) NOT NULL,
  `billable_owner_type` varchar(12) NOT NULL,
  `billable_owner_id` int(11) NOT NULL,
  `aggregate_size_in_bytes` bigint(20) NOT NULL,
  `metered_billing_cycle_starts_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_data_transfer_agg_on_billable_owner_and_usage` (`billable_owner_type`,`billable_owner_id`,`metered_billing_cycle_starts_at`,`owner_id`,`registry_package_id`,`registry_package_version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `package_registry_data_transfer_line_items`;
CREATE TABLE `package_registry_data_transfer_line_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `registry_package_id` int(11) NOT NULL,
  `registry_package_version_id` int(11) NOT NULL,
  `size_in_bytes` bigint(20) NOT NULL,
  `synchronization_batch_id` int(11) DEFAULT NULL,
  `downloaded_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `billable_owner_type` varchar(12) DEFAULT NULL,
  `billable_owner_id` int(11) DEFAULT NULL,
  `directly_billed` tinyint(1) NOT NULL DEFAULT '1',
  `download_id` varchar(50) NOT NULL,
  `submission_state` enum('unsubmitted','submitted','skipped') NOT NULL DEFAULT 'unsubmitted',
  `submission_state_reason` varchar(24) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_package_registry_data_transfer_line_items_on_download_id` (`download_id`),
  KEY `index_package_registry_data_transfer_line_items_on_owner_id` (`owner_id`),
  KEY `index_package_registry_data_transfer_line_items_on_downloaded_at` (`downloaded_at`),
  KEY `index_on_synchronization_batch_id` (`synchronization_batch_id`),
  KEY `index_on_billable_owner_and_usage` (`billable_owner_type`,`billable_owner_id`,`downloaded_at`,`size_in_bytes`),
  KEY `index_on_billable_owner_and_usage_and_repo` (`billable_owner_type`,`billable_owner_id`,`downloaded_at`,`registry_package_id`,`size_in_bytes`),
  KEY `index_packages_line_items_on_submission_state_and_reason` (`submission_state`,`submission_state_reason`),
  KEY `index_on_submission_state_and_created_at` (`submission_state`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `pending_automatic_installations`;
CREATE TABLE `pending_automatic_installations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trigger_type` varchar(50) NOT NULL,
  `target_type` varchar(50) NOT NULL,
  `target_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `reason` tinyint(4) NOT NULL DEFAULT '0',
  `installed_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_pending_automatic_installations_on_trigger_and_target` (`trigger_type`,`target_type`,`target_id`),
  KEY `index_pending_automatic_installations_on_target` (`target_type`,`target_id`),
  KEY `index_pending_automatic_installations_on_trigger_type_and_status` (`trigger_type`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `pending_trade_controls_restrictions`;
CREATE TABLE `pending_trade_controls_restrictions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `target_id` int(11) NOT NULL,
  `enforcement_on` datetime NOT NULL,
  `reason` enum('organization_admin','organization_billing_manager','organization_member','organization_outside_collaborator','location') NOT NULL,
  `enforcement_status` enum('completed','cancelled','pending') NOT NULL DEFAULT 'pending',
  `last_restriction_threshold` decimal(5,2) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_pending_trade_controls_restrictions_on_target_id` (`target_id`),
  KEY `index_pending_trade_controls_restrictions_on_enforcement_on` (`enforcement_on`),
  KEY `index_pending_trade_controls_restrictions_on_reason` (`reason`),
  KEY `index_enforcement_status_on_date` (`enforcement_status`,`enforcement_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `personal_reminders`;
CREATE TABLE `personal_reminders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time_zone_name` varchar(40) NOT NULL,
  `user_id` int(11) NOT NULL,
  `reminder_slack_workspace_id` int(11) NOT NULL,
  `remindable_id` int(11) NOT NULL,
  `remindable_type` varchar(13) NOT NULL,
  `include_review_requests` tinyint(1) NOT NULL DEFAULT '1',
  `include_team_review_requests` tinyint(1) NOT NULL DEFAULT '0',
  `ignore_after_approval_count` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_personal_reminders_on_user_id_and_remindable` (`user_id`,`remindable_type`,`remindable_id`),
  KEY `index_personal_reminders_on_reminder_slack_workspace_id` (`reminder_slack_workspace_id`),
  KEY `index_personal_reminders_on_remindable_type_and_remindable_id` (`remindable_type`,`remindable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `photo_dna_hits`;
CREATE TABLE `photo_dna_hits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uploader_id` int(11) NOT NULL,
  `content_id` int(11) NOT NULL,
  `content_type` varchar(15) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `purged` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_photo_dna_hits_on_uploader_id` (`uploader_id`),
  KEY `index_photo_dna_hits_on_content_id_and_content_type` (`content_id`,`content_type`),
  KEY `index_photo_dna_hits_on_purged_and_created_at` (`purged`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `pinned_issue_comments`;
CREATE TABLE `pinned_issue_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_comment_id` int(11) NOT NULL,
  `issue_id` int(11) NOT NULL,
  `pinned_by_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_pinned_issue_comments_on_issue_id` (`issue_id`),
  KEY `index_pinned_issue_comments_on_issue_comment_id` (`issue_comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `pinned_issues`;
CREATE TABLE `pinned_issues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `issue_id` int(11) NOT NULL,
  `pinned_by_id` int(11) NOT NULL,
  `sort` varchar(126) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_pinned_issues_on_repository_id_and_issue_id` (`repository_id`,`issue_id`),
  KEY `index_pinned_issues_on_repository_id_and_sort` (`repository_id`,`sort`),
  KEY `index_pinned_issues_on_issue_id` (`issue_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `plan_trials`;
CREATE TABLE `plan_trials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `pending_plan_change_id` int(11) NOT NULL,
  `plan` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_plan_trials_on_user_id_and_plan` (`user_id`,`plan`),
  KEY `index_plan_trials_on_pending_plan_change_id` (`pending_plan_change_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `pull_request_imports`;
CREATE TABLE `pull_request_imports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `import_id` int(11) NOT NULL,
  `pull_request_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_pull_request_imports_pull_request_id` (`pull_request_id`),
  KEY `index_pull_request_imports_import_id` (`import_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `refresh_tokens`;
CREATE TABLE `refresh_tokens` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `refreshable_type` varchar(80) NOT NULL,
  `refreshable_id` int(11) NOT NULL,
  `hashed_token` varchar(80) NOT NULL,
  `expires_at` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_refresh_tokens_on_refreshable_id_and_refreshable_type` (`refreshable_id`,`refreshable_type`),
  UNIQUE KEY `index_refresh_tokens_on_hashed_token_and_expires_at` (`hashed_token`,`expires_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `reminder_delivery_times`;
CREATE TABLE `reminder_delivery_times` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` int(11) NOT NULL,
  `day` tinyint(4) NOT NULL,
  `next_delivery_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `schedulable_id` int(11) NOT NULL,
  `schedulable_type` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_reminder_delivery_times_on_schedulable_and_time_and_day` (`schedulable_type`,`schedulable_id`,`time`,`day`),
  KEY `index_reminder_delivery_times_on_next_delivery_at` (`next_delivery_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `reminder_event_subscriptions`;
CREATE TABLE `reminder_event_subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type` int(11) NOT NULL,
  `subscriber_id` int(11) NOT NULL,
  `subscriber_type` varchar(16) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `options` varbinary(1024) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_reminder_event_subscriptions_subscriber_and_event_type` (`subscriber_id`,`subscriber_type`,`event_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `reminder_repository_links`;
CREATE TABLE `reminder_repository_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reminder_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_reminder_repository_links_on_reminder_id` (`reminder_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `reminder_slack_workspace_memberships`;
CREATE TABLE `reminder_slack_workspace_memberships` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `reminder_slack_workspace_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_reminder_slack_user_on_user_id_and_slack_workspace_id` (`user_id`,`reminder_slack_workspace_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `reminder_slack_workspaces`;
CREATE TABLE `reminder_slack_workspaces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slack_id` varchar(20) NOT NULL,
  `remindable_type` varchar(13) NOT NULL,
  `remindable_id` int(11) NOT NULL,
  `name` varbinary(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_reminder_slack_workspaces_on_remindable_and_slack_id` (`remindable_type`,`remindable_id`,`slack_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `reminder_team_memberships`;
CREATE TABLE `reminder_team_memberships` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reminder_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_reminder_team_memberships_on_reminder_id_and_team_id` (`reminder_id`,`team_id`),
  KEY `index_reminder_team_memberships_on_team_id` (`team_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `reminders`;
CREATE TABLE `reminders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `remindable_type` varchar(13) NOT NULL,
  `remindable_id` int(11) NOT NULL,
  `reminder_slack_workspace_id` int(11) NOT NULL,
  `slack_channel` varchar(80) NOT NULL,
  `time_zone_name` varchar(40) NOT NULL,
  `min_age` int(11) NOT NULL DEFAULT '0',
  `min_staleness` int(11) NOT NULL DEFAULT '0',
  `include_unassigned_prs` tinyint(1) NOT NULL DEFAULT '0',
  `include_reviewed_prs` tinyint(1) NOT NULL DEFAULT '0',
  `require_review_request` tinyint(1) NOT NULL DEFAULT '1',
  `ignore_draft_prs` tinyint(1) NOT NULL DEFAULT '1',
  `ignore_after_approval_count` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `ignored_terms` varbinary(1024) DEFAULT NULL,
  `ignored_labels` varbinary(1024) DEFAULT NULL,
  `required_labels` varbinary(1024) DEFAULT NULL,
  `needed_reviews` tinyint(4) NOT NULL DEFAULT '0',
  `slack_channel_id` varchar(20) DEFAULT NULL,
  `team_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_reminders_on_reminder_slack_workspace_id` (`reminder_slack_workspace_id`),
  KEY `index_reminders_on_remindable_type_and_remindable_id` (`remindable_type`,`remindable_id`),
  KEY `index_reminders_on_team_id` (`team_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `repository_advisories`;
CREATE TABLE `repository_advisories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `publisher_id` int(11) DEFAULT NULL,
  `assignee_id` int(11) DEFAULT NULL,
  `title` varbinary(1024) NOT NULL,
  `description` mediumblob,
  `state` int(11) NOT NULL DEFAULT '0',
  `severity` int(11) DEFAULT NULL,
  `affected_versions` blob,
  `fixed_versions` blob,
  `impact` mediumblob,
  `workarounds` mediumblob,
  `patches` mediumblob,
  `cve_id` varbinary(40) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `published_at` datetime DEFAULT NULL,
  `closed_at` datetime DEFAULT NULL,
  `withdrawn_at` datetime DEFAULT NULL,
  `workspace_repository_id` int(11) DEFAULT NULL,
  `ghsa_id` varchar(19) NOT NULL,
  `package` varbinary(100) DEFAULT NULL,
  `ecosystem` varbinary(50) DEFAULT NULL,
  `body` mediumblob,
  `owner_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_advisories_on_ghsa_id` (`ghsa_id`),
  UNIQUE KEY `index_repository_advisories_on_workspace_repository_id` (`workspace_repository_id`),
  KEY `index_repository_advisories_on_repository_id_and_state` (`repository_id`,`state`),
  KEY `index_repository_advisories_on_owner_and_workspace_repository` (`owner_id`,`workspace_repository_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `repository_advisory_comments`;
CREATE TABLE `repository_advisory_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_advisory_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `body` mediumblob NOT NULL,
  `formatter` varchar(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_repository_advisory_comments_on_repository_advisory_id` (`repository_advisory_id`),
  KEY `index_repository_advisory_comments_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `repository_advisory_events`;
CREATE TABLE `repository_advisory_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_advisory_id` int(11) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `event` varchar(40) NOT NULL,
  `changed_attribute` varchar(40) DEFAULT NULL,
  `value_was` varbinary(2014) DEFAULT NULL,
  `value_is` varbinary(2014) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_repository_advisory_events_on_repo_adv_id_and_event` (`repository_advisory_id`,`event`),
  KEY `index_repository_advisory_events_on_actor_id_event_created_at` (`actor_id`,`event`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `repository_clones`;
CREATE TABLE `repository_clones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_repository_id` int(11) NOT NULL,
  `clone_repository_id` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `cloning_user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `error_reason_code` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_clones_on_clone_repository_id` (`clone_repository_id`),
  KEY `index_repository_clones_on_template_repository_id` (`template_repository_id`),
  KEY `index_repository_clones_on_cloning_user_id` (`cloning_user_id`),
  KEY `index_repository_clones_on_clone_repository_id_and_state` (`clone_repository_id`,`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `repository_code_symbol_indices`;
CREATE TABLE `repository_code_symbol_indices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `commit_oid` varchar(40) NOT NULL,
  `ref` varbinary(767) NOT NULL,
  `page` int(11) NOT NULL DEFAULT '0',
  `status` varchar(10) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_code_symbol_indices_on_repository_ref_commit` (`repository_id`,`ref`,`commit_oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `repository_contribution_graph_statuses`;
CREATE TABLE `repository_contribution_graph_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `job_status_id` char(36) DEFAULT NULL,
  `last_indexed_oid` varchar(64) DEFAULT NULL,
  `last_viewed_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `top_contributor_ids` blob,
  `last_indexed_at` datetime DEFAULT NULL,
  `job_enqueued_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_contribution_graph_statuses_on_repository_id` (`repository_id`),
  KEY `index_repository_contribution_graph_statuses_on_last_viewed_at` (`last_viewed_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `repository_imports`;
CREATE TABLE `repository_imports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `import_id` int(11) NOT NULL,
  `repository_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_imports_repository_id` (`repository_id`),
  KEY `index_repository_imports_import_id` (`import_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `repository_network_graphs`;
CREATE TABLE `repository_network_graphs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `job_status_id` char(36) DEFAULT NULL,
  `network_hash` varchar(64) DEFAULT NULL,
  `built_at` datetime DEFAULT NULL,
  `focus` int(11) DEFAULT '0',
  `meta` mediumblob,
  `commits_index` mediumblob,
  `commits_data` mediumblob,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_repository_network_graphs_on_repository_id` (`repository_id`),
  KEY `index_repository_network_graphs_on_built_at` (`built_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `review_request_delegation_excluded_members`;
CREATE TABLE `review_request_delegation_excluded_members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `excluded_members_team_user_unique` (`team_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `science_events`;
CREATE TABLE `science_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(85) NOT NULL,
  `event_type` varchar(10) NOT NULL,
  `payload` mediumblob NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_science_events_on_name_and_event_type` (`name`,`event_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `scoped_integration_installations`;
CREATE TABLE `scoped_integration_installations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `integration_installation_id` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `expires_at` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_scoped_installations_on_integration_installation_id` (`integration_installation_id`),
  KEY `index_scoped_integration_installations_on_created_at` (`created_at`),
  KEY `index_scoped_integration_installations_on_expires_at` (`expires_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `shared_storage_artifact_aggregations`;
CREATE TABLE `shared_storage_artifact_aggregations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `repository_id` int(11) DEFAULT NULL,
  `aggregate_effective_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `repository_visibility` enum('unknown','public','private') NOT NULL DEFAULT 'unknown',
  `aggregate_size_in_bytes` bigint(20) NOT NULL,
  `previous_aggregation_id` int(11) DEFAULT NULL,
  `synchronization_batch_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `billable_owner_type` varchar(12) DEFAULT NULL,
  `billable_owner_id` int(11) DEFAULT NULL,
  `directly_billed` tinyint(1) NOT NULL DEFAULT '1',
  `submission_state` enum('unsubmitted','submitted','skipped') NOT NULL DEFAULT 'unsubmitted',
  `submission_state_reason` varchar(24) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_shared_storage_artifact_aggregations_on_owner_repo_time` (`owner_id`,`repository_id`,`aggregate_effective_at`),
  KEY `index_on_shared_storage_artifacts_synchronization_batch` (`synchronization_batch_id`),
  KEY `idx_shared_storage_artifact_aggregations_sum_covering` (`billable_owner_type`,`repository_visibility`,`billable_owner_id`,`aggregate_effective_at`,`aggregate_size_in_bytes`),
  KEY `idx_shared_storage_artifact_aggregations_sum_with_owner_covering` (`billable_owner_type`,`repository_visibility`,`billable_owner_id`,`owner_id`,`aggregate_effective_at`,`aggregate_size_in_bytes`),
  KEY `index_shared_storage_aggregations_on_billable_usage` (`synchronization_batch_id`,`directly_billed`,`repository_visibility`,`aggregate_effective_at`,`owner_id`),
  KEY `index_shared_storage_artifact_aggregations_on_repository_id` (`billable_owner_type`,`repository_visibility`,`billable_owner_id`,`aggregate_effective_at`,`repository_id`),
  KEY `index_on_billable_owner_and_usage_and_repo` (`billable_owner_type`,`billable_owner_id`,`aggregate_effective_at`,`repository_id`),
  KEY `idx_shared_storage_artifact_aggregations_previous_aggregation` (`billable_owner_type`,`billable_owner_id`,`directly_billed`,`repository_id`,`owner_id`,`aggregate_effective_at`),
  KEY `index_shared_storage_aggregations_on_submission_state_and_reason` (`submission_state`,`submission_state_reason`),
  KEY `index_on_submission_state_and_created_at` (`submission_state`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `shared_storage_artifact_events`;
CREATE TABLE `shared_storage_artifact_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `repository_id` int(11) DEFAULT NULL,
  `effective_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `source` enum('unknown','actions','gpr') NOT NULL DEFAULT 'unknown',
  `repository_visibility` enum('unknown','public','private') NOT NULL DEFAULT 'unknown',
  `event_type` enum('unknown','add','remove') NOT NULL DEFAULT 'unknown',
  `size_in_bytes` bigint(20) NOT NULL,
  `aggregation_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `billable_owner_type` varchar(12) DEFAULT NULL,
  `billable_owner_id` int(11) DEFAULT NULL,
  `directly_billed` tinyint(1) NOT NULL DEFAULT '1',
  `source_artifact_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_shared_storage_artifact_events_on_owner_repo_time_agg` (`owner_id`,`repository_id`,`effective_at`,`aggregation_id`),
  KEY `index_shared_storage_artifact_events_on_aggregate_fields` (`owner_id`,`billable_owner_type`,`billable_owner_id`,`directly_billed`,`repository_id`,`effective_at`,`aggregation_id`),
  KEY `index_on_fields_for_artifact_expiration` (`source`,`event_type`,`owner_id`,`effective_at`,`repository_id`,`size_in_bytes`),
  KEY `index_on_fields_for_stafftools_shared_storage_breakdown` (`owner_id`,`repository_visibility`,`effective_at`,`repository_id`,`source`,`aggregation_id`,`event_type`,`size_in_bytes`),
  KEY `index_shared_storage_artifact_events_on_source_artifact_id` (`source_artifact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `shared_storage_billable_owners`;
CREATE TABLE `shared_storage_billable_owners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `billable_owner_type` varchar(12) NOT NULL,
  `billable_owner_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_shared_storage_billable_owners_on_billable_owner_and_owner` (`billable_owner_type`,`billable_owner_id`,`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `site_scoped_integration_installations`;
CREATE TABLE `site_scoped_integration_installations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `integration_id` int(11) NOT NULL,
  `target_id` int(11) NOT NULL,
  `target_type` varchar(25) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `expires_at` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_site_scoped_integration_installations_on_integration_id` (`integration_id`),
  KEY `index_site_scoped_integration_installations_on_target` (`target_id`,`target_type`),
  KEY `index_site_scoped_integration_installations_on_created_at` (`created_at`),
  KEY `index_site_scoped_integration_installations_on_expires_at` (`expires_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `sponsors_activities`;
CREATE TABLE `sponsors_activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sponsorable_id` int(11) NOT NULL,
  `sponsor_id` int(11) DEFAULT NULL,
  `action` int(11) NOT NULL,
  `sponsors_tier_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `old_sponsors_tier_id` int(11) DEFAULT NULL,
  `sponsorable_type` int(11) NOT NULL,
  `sponsor_type` int(11) DEFAULT NULL,
  `timestamp` datetime NOT NULL,
  `matched_sponsorship` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_sponsors_activities_on_sponsorable_and_timestamp` (`sponsorable_id`,`sponsorable_type`,`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `sponsors_activity_metrics`;
CREATE TABLE `sponsors_activity_metrics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `sponsorable_id` int(11) NOT NULL,
  `sponsorable_type` int(11) NOT NULL,
  `metric` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  `recorded_on` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_sponsors_metrics_on_sponsorable_and_metric_and_recorded_on` (`sponsorable_id`,`sponsorable_type`,`metric`,`recorded_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `sponsors_criteria`;
CREATE TABLE `sponsors_criteria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(60) NOT NULL,
  `description` blob NOT NULL,
  `criterion_type` int(11) NOT NULL DEFAULT '0',
  `automated` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `applicable_to` tinyint(4) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_sponsors_criteria_on_slug` (`slug`),
  KEY `index_sponsors_criteria_on_criterion_type` (`criterion_type`),
  KEY `index_sponsors_criteria_on_applicable_to` (`applicable_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `sponsors_fraud_reviews`;
CREATE TABLE `sponsors_fraud_reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sponsors_listing_id` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `reviewer_id` int(11) DEFAULT NULL,
  `reviewed_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_sponsors_fraud_reviews_on_sponsors_listing_id` (`sponsors_listing_id`),
  KEY `index_sponsors_fraud_reviews_on_state` (`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `sponsors_goal_contributions`;
CREATE TABLE `sponsors_goal_contributions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sponsors_goal_id` int(11) NOT NULL,
  `sponsor_id` int(11) NOT NULL,
  `sponsors_tier_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_sponsors_goal_contributions_on_sponsors_goal_id` (`sponsors_goal_id`),
  KEY `index_sponsors_goal_contributions_on_sponsor_id_and_created_at` (`sponsor_id`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `sponsors_goals`;
CREATE TABLE `sponsors_goals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sponsors_listing_id` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `kind` int(11) NOT NULL DEFAULT '0',
  `target_value` int(11) NOT NULL DEFAULT '0',
  `description` blob,
  `completed_at` datetime DEFAULT NULL,
  `retired_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_sponsors_goals_on_sponsors_listing_id_and_state` (`sponsors_listing_id`,`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `sponsors_listing_featured_items`;
CREATE TABLE `sponsors_listing_featured_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sponsors_listing_id` int(11) NOT NULL,
  `featureable_type` int(11) NOT NULL,
  `featureable_id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  `description` tinyblob,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_sponsors_listing_featured_items_unique` (`sponsors_listing_id`,`featureable_type`,`featureable_id`),
  KEY `index_sponsors_listing_featured_items_on_featureable` (`featureable_type`,`featureable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `sponsors_listings`;
CREATE TABLE `sponsors_listings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `sponsorable_type` int(11) NOT NULL,
  `sponsorable_id` int(11) NOT NULL,
  `short_description` tinyblob,
  `full_description` blob,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `published_at` datetime DEFAULT NULL,
  `payout_probation_started_at` datetime DEFAULT NULL,
  `payout_probation_ended_at` datetime DEFAULT NULL,
  `match_disabled` tinyint(1) NOT NULL DEFAULT '0',
  `stripe_authorization_code` varchar(40) DEFAULT NULL,
  `docusign_envelope_id` varchar(40) DEFAULT NULL,
  `docusign_envelope_status` varchar(10) DEFAULT NULL,
  `sponsors_membership_id` int(11) DEFAULT NULL,
  `match_limit_reached_at` datetime DEFAULT NULL,
  `approval_requested_at` datetime DEFAULT NULL,
  `milestone_email_sent_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_sponsors_listings_on_slug` (`slug`),
  UNIQUE KEY `index_sponsors_listings_on_sponsorable_id_and_sponsorable_type` (`sponsorable_id`,`sponsorable_type`),
  UNIQUE KEY `index_sponsors_listings_on_docusign_envelope_id` (`docusign_envelope_id`),
  UNIQUE KEY `index_sponsors_listings_on_sponsors_membership_id` (`sponsors_membership_id`),
  KEY `index_sponsors_listings_on_payout_probation` (`payout_probation_started_at`,`payout_probation_ended_at`),
  KEY `index_sponsors_listings_on_state_and_payout_probation_started` (`state`,`payout_probation_started_at`),
  KEY `index_sponsors_listings_on_state_and_payout_probation` (`state`,`payout_probation_ended_at`,`payout_probation_started_at`),
  KEY `index_sponsors_listings_on_match_disabled_and_created_at` (`match_disabled`,`created_at`),
  KEY `index_sponsors_listings_on_match_disabled_and_published_at` (`match_disabled`,`published_at`),
  KEY `index_sponsors_listings_on_stripe_authorization_code` (`stripe_authorization_code`),
  KEY `index_sponsors_listings_on_state_and_approval_requested_at` (`state`,`approval_requested_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `sponsors_memberships`;
CREATE TABLE `sponsors_memberships` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` int(11) NOT NULL DEFAULT '0',
  `sponsorable_type` int(11) NOT NULL,
  `sponsorable_id` int(11) NOT NULL,
  `survey_id` int(11) DEFAULT NULL,
  `reviewer_id` int(11) DEFAULT NULL,
  `appeal_allowed` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `billing_country` varchar(60) DEFAULT NULL,
  `contact_email_id` int(11) DEFAULT NULL,
  `reviewed_at` datetime DEFAULT NULL,
  `legal_name` varchar(255) DEFAULT NULL,
  `featured_description` varbinary(256) DEFAULT NULL,
  `featured_state` int(11) DEFAULT '0',
  `country_of_residence` char(2) DEFAULT NULL,
  `banned_at` datetime DEFAULT NULL,
  `banned_by_id` int(11) DEFAULT NULL,
  `banned_reason` varbinary(1024) DEFAULT NULL,
  `ignored_at` datetime DEFAULT NULL,
  `fiscal_host` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_sponsors_membership_on_sponsorable_type_and_sponsorable_id` (`sponsorable_id`,`sponsorable_type`),
  KEY `index_sponsors_memberships_on_state_and_sponsorable_type` (`state`,`sponsorable_type`),
  KEY `index_sponsors_memberships_on_contact_email_id` (`contact_email_id`),
  KEY `index_sponsors_memberships_on_reviewed_at` (`reviewed_at`),
  KEY `index_sponsors_memberships_on_featured_state_and_sponsorable_id` (`featured_state`,`sponsorable_id`),
  KEY `index_on_ignored_state_country` (`ignored_at`,`state`,`billing_country`),
  KEY `index_sponsors_memberships_on_fiscal_host_and_state` (`fiscal_host`,`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `sponsors_memberships_criteria`;
CREATE TABLE `sponsors_memberships_criteria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sponsors_criterion_id` int(11) NOT NULL,
  `sponsors_membership_id` int(11) NOT NULL,
  `reviewer_id` int(11) DEFAULT NULL,
  `met` tinyint(1) NOT NULL DEFAULT '0',
  `value` blob,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_on_membership_and_criterion` (`sponsors_membership_id`,`sponsors_criterion_id`),
  KEY `index_sponsors_memberships_criteria_on_sponsors_criterion_id` (`sponsors_criterion_id`),
  KEY `index_sponsors_memberships_criteria_on_reviewer_id` (`reviewer_id`),
  KEY `index_on_membership_and_met` (`sponsors_membership_id`,`met`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `sponsors_tiers`;
CREATE TABLE `sponsors_tiers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sponsors_listing_id` int(11) NOT NULL,
  `name` varbinary(1024) NOT NULL,
  `description` mediumblob NOT NULL,
  `state` smallint(6) NOT NULL DEFAULT '0',
  `monthly_price_in_cents` int(11) NOT NULL DEFAULT '0',
  `yearly_price_in_cents` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `marketplace_listing_plan_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_sponsors_tiers_on_name_and_sponsors_listing_id_and_state` (`name`,`sponsors_listing_id`,`state`),
  KEY `index_sponsors_tiers_on_listing_and_monthly_price_in_cents` (`sponsors_listing_id`,`monthly_price_in_cents`),
  KEY `index_sponsors_tiers_on_name_listing_and_created_at` (`name`,`sponsors_listing_id`,`created_at`),
  KEY `index_sponsors_tiers_on_marketplace_listing_plan_id` (`marketplace_listing_plan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `sponsorship_match_bans`;
CREATE TABLE `sponsorship_match_bans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sponsorable_id` int(11) NOT NULL,
  `sponsorable_type` int(11) NOT NULL,
  `sponsor_id` int(11) NOT NULL,
  `sponsor_type` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_sponsorship_match_bans_on_sponsorable` (`sponsorable_id`,`sponsorable_type`),
  KEY `index_sponsorship_match_bans_on_sponsor` (`sponsor_id`,`sponsor_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `sponsorship_newsletter_tiers`;
CREATE TABLE `sponsorship_newsletter_tiers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sponsorship_newsletter_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `sponsors_tier_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_newsletter_and_tier` (`sponsorship_newsletter_id`,`sponsors_tier_id`),
  KEY `index_newsletter_tiers_on_tier` (`sponsors_tier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `sponsorship_newsletters`;
CREATE TABLE `sponsorship_newsletters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `body` mediumblob NOT NULL,
  `subject` varbinary(1024) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `sponsorable_type` int(11) NOT NULL,
  `sponsorable_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_sponsorship_newsletters_on_author_id` (`author_id`),
  KEY `index_sponsorship_newsletters_on_sponsorable_and_state` (`sponsorable_type`,`sponsorable_id`,`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `sponsorships`;
CREATE TABLE `sponsorships` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `privacy_level` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `sponsor_type` int(11) NOT NULL,
  `sponsor_id` int(11) NOT NULL,
  `is_sponsor_opted_in_to_email` tinyint(1) NOT NULL DEFAULT '0',
  `maintainer_notes` text,
  `is_sponsor_tier_reward_fulfilled` tinyint(1) NOT NULL DEFAULT '0',
  `subscription_item_id` int(11) NOT NULL,
  `is_sponsor_opted_in_to_share_with_fiscal_host` tinyint(1) NOT NULL DEFAULT '0',
  `sponsorable_type` int(11) NOT NULL,
  `sponsorable_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `subscribable_type` int(11) DEFAULT NULL,
  `subscribable_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_sponsorships_sponsor_sponsorable_unique` (`sponsor_id`,`sponsor_type`,`sponsorable_id`,`sponsorable_type`),
  KEY `index_sponsorships_on_privacy_level` (`privacy_level`),
  KEY `index_sponsorships_on_is_sponsor_opted_in_to_email` (`is_sponsor_opted_in_to_email`),
  KEY `index_sponsorships_on_sponsorable_id_and_sponsorable_type` (`sponsorable_id`,`sponsorable_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `ssh_certificate_authorities`;
CREATE TABLE `ssh_certificate_authorities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_type` varchar(8) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `openssh_public_key` text NOT NULL,
  `fingerprint` varbinary(32) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_ssh_certificate_authorities_on_fingerprint` (`fingerprint`),
  KEY `index_ssh_certificate_authorities_on_owner_type_and_owner_id` (`owner_type`,`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `stripe_connect_accounts`;
CREATE TABLE `stripe_connect_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payable_type` varchar(32) NOT NULL,
  `payable_id` int(11) NOT NULL,
  `stripe_account_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `stripe_account_details` blob,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_stripe_connect_accounts_on_payable_id_and_payable_type` (`payable_id`,`payable_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `successor_invitations`;
CREATE TABLE `successor_invitations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `target_id` int(11) NOT NULL,
  `target_type` varchar(40) NOT NULL,
  `inviter_id` int(11) NOT NULL,
  `invitee_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `accepted_at` datetime DEFAULT NULL,
  `declined_at` datetime DEFAULT NULL,
  `canceled_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_successor_invitations_on_target` (`target_id`,`target_type`),
  KEY `index_successor_invitations_on_inviter_id` (`inviter_id`),
  KEY `index_successor_invitations_on_invitee_id` (`invitee_id`),
  KEY `index_successor_invitations_on_accepted_at` (`accepted_at`),
  KEY `index_successor_invitations_on_declined_at` (`declined_at`),
  KEY `index_successor_invitations_on_canceled_at` (`canceled_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `team_member_delegated_review_requests`;
CREATE TABLE `team_member_delegated_review_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `delegated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_team_member_delegated_review_requests_on_team_member` (`team_id`,`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `trade_controls_restrictions`;
CREATE TABLE `trade_controls_restrictions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type` enum('unrestricted','partial','full','review','tier_0','tier_1') NOT NULL DEFAULT 'unrestricted',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `trade_restricted_country_code` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_trade_controls_restrictions_on_user_id` (`user_id`),
  KEY `index_trade_controls_restrictions_on_type` (`type`),
  KEY `index_on_trade_restricted_country_code` (`trade_restricted_country_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `two_factor_recovery_requests`;
CREATE TABLE `two_factor_recovery_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `otp_verified` tinyint(1) NOT NULL DEFAULT '0',
  `oauth_access_id` int(11) DEFAULT NULL,
  `authenticated_device_id` int(11) DEFAULT NULL,
  `public_key_id` int(11) DEFAULT NULL,
  `request_completed_at` datetime DEFAULT NULL,
  `reviewer_id` int(11) DEFAULT NULL,
  `review_completed_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `requesting_device_id` int(11) DEFAULT NULL,
  `approved_at` datetime DEFAULT NULL,
  `declined_at` datetime DEFAULT NULL,
  `staff_review_requested_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_two_factor_recovery_requests_on_user_id_and_created_at` (`user_id`,`created_at`),
  KEY `index_two_factor_recovery_requests_on_user_id_requesting_device` (`user_id`,`requesting_device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `usage_synchronization_batches`;
CREATE TABLE `usage_synchronization_batches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  `zuora_status_url` varchar(255) DEFAULT NULL,
  `upload_filename` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `partition` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_usage_synchronization_batches_on_status` (`status`),
  KEY `index_usage_sync_batches_on_prod_status_partition` (`product`,`status`,`partition`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `user_dashboard_pins`;
CREATE TABLE `user_dashboard_pins` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `pinned_item_id` int(11) NOT NULL,
  `pinned_item_type` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_user_dashboard_pins_unique` (`user_id`,`pinned_item_type`,`pinned_item_id`),
  KEY `index_user_dashboard_pins_on_user_id_and_position` (`user_id`,`position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `user_labels`;
CREATE TABLE `user_labels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varbinary(1024) NOT NULL,
  `lowercase_name` varbinary(1024) NOT NULL,
  `description` varbinary(400) DEFAULT NULL,
  `color` varchar(10) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_user_labels_on_name` (`name`),
  KEY `index_user_labels_on_user_id_and_name` (`user_id`,`name`),
  KEY `index_user_labels_on_user_id_and_lowercase_name` (`user_id`,`lowercase_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `user_licenses`;
CREATE TABLE `user_licenses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `business_id` int(11) NOT NULL,
  `license_type` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_user_licenses_on_business_id_and_email` (`business_id`,`email`),
  UNIQUE KEY `index_user_licenses_on_user_id_and_business_id` (`user_id`,`business_id`),
  KEY `index_user_licenses_on_business_id_and_license_type` (`business_id`,`license_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `user_personal_profiles`;
CREATE TABLE `user_personal_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(64) NOT NULL,
  `last_name` varchar(64) NOT NULL DEFAULT '',
  `middle_name` varchar(64) NOT NULL DEFAULT '',
  `region` varchar(64) DEFAULT NULL,
  `city` varchar(64) NOT NULL,
  `country_code` varchar(3) NOT NULL,
  `postal_code` varchar(32) DEFAULT NULL,
  `address1` varchar(128) NOT NULL,
  `address2` varchar(128) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `msft_trade_screening_status` tinyint(4) NOT NULL DEFAULT '0',
  `last_trade_screen_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_user_personal_profiles_on_user_id` (`user_id`),
  KEY `index_on_msft_trade_screening_status` (`msft_trade_screening_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `user_reviewed_files`;
CREATE TABLE `user_reviewed_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pull_request_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `filepath` varbinary(1024) NOT NULL,
  `head_sha` varbinary(40) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `dismissed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_reviewed_files_on_pull_user_and_filepath` (`pull_request_id`,`user_id`,`filepath`),
  KEY `index_reviewed_files_on_user_pull_and_dismissed` (`user_id`,`pull_request_id`,`dismissed`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `user_seen_features`;
CREATE TABLE `user_seen_features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `feature_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_user_seen_features_on_user_id_and_feature_id` (`user_id`,`feature_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `vss_subscription_events`;
CREATE TABLE `vss_subscription_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payload` text,
  `status` enum('unprocessed','processed','failed') NOT NULL DEFAULT 'unprocessed',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `webauthn_user_handles`;
CREATE TABLE `webauthn_user_handles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `webauthn_user_handle` binary(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_webauthn_user_handles_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `workspace_plans`;
CREATE TABLE `workspace_plans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `owner_type` varchar(12) NOT NULL,
  `resource_group_id` int(11) DEFAULT NULL,
  `name` varchar(90) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `vscs_target` varchar(21) DEFAULT NULL,
  `resource_provider` varchar(50) DEFAULT 'Microsoft.VSOnline',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_on_resource_group_and_name` (`resource_group_id`,`name`),
  UNIQUE KEY `index_on_owner_rg_rp_and_target` (`owner_id`,`owner_type`,`resource_group_id`,`resource_provider`,`vscs_target`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `workspace_resource_groups`;
CREATE TABLE `workspace_resource_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subscription` char(36) NOT NULL,
  `location` varchar(40) NOT NULL,
  `name` varchar(90) NOT NULL,
  `plans_count` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_workspace_resource_groups_on_subscription_and_name` (`subscription`,`name`),
  KEY `index_on_sub_location_and_plans` (`subscription`,`location`,`plans_count`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `workspaces`;
CREATE TABLE `workspaces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `pull_request_id` int(11) DEFAULT NULL,
  `guid` char(36) DEFAULT NULL,
  `name` varchar(90) NOT NULL,
  `slug` varchar(100) DEFAULT NULL,
  `oid` char(40) NOT NULL,
  `ref` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `last_used_at` datetime DEFAULT NULL,
  `plan_id` int(11) DEFAULT NULL,
  `location` varchar(40) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_workspaces_on_owner_id_and_name` (`owner_id`,`name`),
  UNIQUE KEY `index_workspaces_on_repository_id_and_owner_id_and_name` (`repository_id`,`owner_id`,`name`),
  UNIQUE KEY `index_workspaces_on_name` (`name`),
  UNIQUE KEY `index_workspaces_on_guid_and_owner_id` (`guid`,`owner_id`),
  UNIQUE KEY `index_workspaces_on_slug_and_owner_id` (`slug`,`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
