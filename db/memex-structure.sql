DROP TABLE IF EXISTS `draft_issues`;
CREATE TABLE `draft_issues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varbinary(1024) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `memex_project_item_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_draft_issues_on_memex_project_item_id` (`memex_project_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `memex_project_column_values`;
CREATE TABLE `memex_project_column_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `memex_project_column_id` int(11) NOT NULL,
  `memex_project_item_id` int(11) NOT NULL,
  `value` mediumblob,
  `creator_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_memex_project_column_values_on_column_and_item` (`memex_project_column_id`,`memex_project_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `memex_project_columns`;
CREATE TABLE `memex_project_columns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `memex_project_id` int(11) NOT NULL,
  `name` varbinary(255) NOT NULL,
  `user_defined` tinyint(1) NOT NULL,
  `position` tinyint(4) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `data_type` tinyint(4) NOT NULL,
  `settings` json DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_memex_project_columns_on_memex_project_id_and_position` (`memex_project_id`,`position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `memex_project_items`;
CREATE TABLE `memex_project_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `memex_project_id` int(11) NOT NULL,
  `content_type` varchar(20) DEFAULT NULL,
  `content_id` int(11) DEFAULT NULL,
  `creator_id` int(11) NOT NULL,
  `priority` bigint(20) unsigned DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `repository_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_memex_project_items_on_memex_project_id_and_priority` (`memex_project_id`,`priority`),
  UNIQUE KEY `index_memex_project_items_on_memex_project_repository_id_content` (`memex_project_id`,`repository_id`,`content_type`,`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `memex_projects`;
CREATE TABLE `memex_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_type` varchar(30) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `title` varbinary(1024) DEFAULT NULL,
  `description` mediumblob,
  `public` tinyint(1) NOT NULL DEFAULT '0',
  `closed_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_memex_projects_on_number_and_owner_type_and_owner_id` (`number`,`owner_type`,`owner_id`),
  KEY `index_memex_projects_on_owner_closed_at_and_created_at` (`owner_id`,`owner_type`,`closed_at`,`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
