# frozen_string_literal: true

class Site::HeaderNotifications < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  def indicator_mode
    @indicator_mode ||= current_user.indicator_mode
  end

  def show?
    indicator_mode != :disabled
  end

  def aria_label
    case indicator_mode
    when :global
      "You have unread notifications"
    when :unavailable
      "Notifications are unavailable at the moment."
    when :none
      "You have no unread notifications"
    end
  end

  def ga_click
    state = case indicator_mode
    when :global
      "unread"
    when :unavailable, :none
      "read"
    end

    "Header, go to notifications, icon:#{state}"
  end

  def span_class
    state = case indicator_mode
    when :global
      "unread"
    when :unavailable, :none
      ""
    end

    "js-indicator-modifier mail-status #{state}"
  end
end
