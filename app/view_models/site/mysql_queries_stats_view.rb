# frozen_string_literal: true

class Site::MysqlQueriesStatsView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :tags, :html_id

  def query_count
    queries.count
  end

  def primary_query_count
    queries.count(&:on_primary)
  end

  def cached_query_count
    cache_hits.count
  end

  def query_time
    queries.map(&:duration).inject(0, &:+)
  end

  def digest_count
    digest_stats.size
  end

  def row_count
    digest_stats.map { |digest, stats| stats[:results] }.inject(0, &:+)
  end

  def cached_row_count
    cached_digest_stats.map { |digest, stats| stats[:results] }.inject(0, &:+)
  end

  def similar_queries_count(query)
    uncached_stats = digest_stats.fetch(query.digested_sql, {})
    cached_stats = cached_digest_stats.fetch(query.digested_sql, {})
    uncached_stats[:count].to_i + cached_stats[:count].to_i
  end

  def loader_tag(query)
    query.tags.select { |tag| tag.to_s.starts_with?("loader:") }.join(";")
  end

  def digest_stats
    @digest_stats ||= compute_digest_stats(queries).freeze
  end

  def sorted_digest_stats
    @sorted_digest_stats ||= digest_stats.sort_by { |digest, stats| -stats[:count] }.freeze
  end

  def cached_digest_stats
    @cached_digest_stats ||= compute_digest_stats(cache_hits).freeze
  end

  def sorted_cached_digest_stats
    @sorted_cached_digest_stats ||= cached_digest_stats.sort_by { |digest, stats| -stats[:results] }.freeze
  end

  def sorted_queries
    @sorted_queries ||= begin
      combined = queries + cache_hits
      combined.sort_by(&:start_time).freeze
    end
  end

  def time_class(query)
    case
    when query.duration.nil?
      "lightskyblue"
    when query.duration > 0.100
      "red"
    when query.duration > 0.050
      "palegoldenrod"
    when query.duration > 0.010
      "goldenrod"
    when query.duration > 0.001
      "mediumpurple"
    else
      "grey"
    end
  end

  private

  def queries
    @queries ||= filter_queries_by_tags(GitHub::MysqlInstrumenter.queries).freeze
  end

  def cache_hits
    @cache_hits ||= filter_queries_by_tags(QueryCacheLogSubscriber.cache_hits).freeze
  end

  def compute_digest_stats(queries)
    queries.each_with_object({}) do |query, hash|
      stats = hash.fetch(query.digested_sql) { |digest|
        hash[digest] = { count: 0, time: 0, results: 0 }
      }
      stats[:count] += 1
      stats[:time] += query.duration || 0
      stats[:results] += query.result_count || 0
    end
  end

  def filter_queries_by_tags(queries)
    return queries.dup unless tags.present?

    queries.select { |query| (tags & query.tags) == tags }
  end
end
