# frozen_string_literal: true

class Site::HeaderView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :current_organization
  attr_reader :currently_viewed_user
  attr_reader :current_repository
  attr_reader :is_mobile_request
  attr_reader :params

  # Public: Get a hash of keys / Names all searchable result types.
  # This is called on every page load so value is memoized
  #
  # Returns a hash
  def self.result_types
    return @result_types if instance_variable_defined?(:@result_types)
    @result_types = {
      all: "All",
      repositories: "Repositories",
      code: "Code",
      commits: "Commits",
      issues: "Issues",
      discussions: "Discussions",
      registrypackages: "Packages",
      marketplace: "Marketplace",
      topics: "Topics",
      wikis: "Wikis",
      users: "Users"
    }
  end

  # Public: Get a hash, keyed by searchable result type,
  # with array values of org/repo scopes for each type.
  # This is called on every page load so value is memoized
  #
  # Returns a hash
  def self.scopes
    return @scopes if instance_variable_defined?(:@scopes)
    @scopes = {
      all: ["user", "org", "repo"],
      repositories: only_scope_qualifiers(Search::Queries::RepoQuery::FIELDS),
      code: only_scope_qualifiers(Search::Queries::CodeQuery::FIELDS),
      commits: only_scope_qualifiers(Search::Queries::CommitQuery::FIELDS),
      issues: only_scope_qualifiers(Search::Queries::IssueQuery::FIELDS),
      discussions: only_scope_qualifiers(Search::Queries::DiscussionQuery::FIELDS),
      registrypackages: only_scope_qualifiers(Search::Queries::RegistryPackageQuery::FIELDS),
      marketplace: only_scope_qualifiers(Search::Queries::MarketplaceQuery::FIELDS),
      topics: only_scope_qualifiers(Search::Queries::TopicQuery::FIELDS),
      wikis: only_scope_qualifiers(Search::Queries::WikiQuery::FIELDS),
      users: only_scope_qualifiers(Search::Queries::UserQuery::FIELDS)
    }
  end

  # Public: Get a hash, keyed by searchable result type,
  # with array values of available filters for each type.
  # This is called on every page load so value is memoized
  #
  # Returns a hash
  def self.filters
    return @filters if instance_variable_defined?(:@filters)
    @filters = {
      all: [],
      repositories: excluding_scope_qualifiers(Search::Queries::RepoQuery::FIELDS),
      code: excluding_scope_qualifiers(Search::Queries::CodeQuery::FIELDS),
      commits: excluding_scope_qualifiers(Search::Queries::CommitQuery::FIELDS),
      issues: excluding_scope_qualifiers(Search::Queries::IssueQuery::FIELDS),
      discussions: excluding_scope_qualifiers(Search::Queries::DiscussionQuery::FIELDS),
      registrypackages: excluding_scope_qualifiers(Search::Queries::RegistryPackageQuery::FIELDS),
      marketplace: excluding_scope_qualifiers(Search::Queries::MarketplaceQuery::FIELDS),
      topics: excluding_scope_qualifiers(Search::Queries::TopicQuery::FIELDS),
      wikis: excluding_scope_qualifiers(Search::Queries::WikiQuery::FIELDS),
      users: excluding_scope_qualifiers(Search::Queries::UserQuery::FIELDS)
    }
  end

  def self.only_scope_qualifiers(list)
    list & [:org, :user, :repo]
  end
  private_class_method :only_scope_qualifiers

  def self.excluding_scope_qualifiers(list)
    list.excluding(:org, :user, :repo)
  end
  private_class_method :excluding_scope_qualifiers

  # Public: Get the search path.
  #
  # Returns a string.
  def search_path
    scoped_search_path || unscoped_search_path
  end

  # Public: Get the search path for the current scope.
  #
  # Returns a string.
  def scoped_search_path
    case scope
    when Repository
      urls.repo_search_path(current_repository.owner, current_repository)
    when Organization
      urls.org_search_path(current_organization)
    when User
      urls.search_path(user: currently_viewed_user)
    when Topic
      "/topics/#{current_topic.name}"
    else
      nil
    end
  end

  def query
    case params[:q]
    when Array;  params[:q].join(" ")
    when String; params[:q]
    end
  end

  # Public: Get the unscoped search path.
  #
  # Returns a string.
  def unscoped_search_path
    urls.search_path
  end

  # Public: Get the text to show in the search scope badge.
  #
  # Returns a string.
  def search_scope_badge_text
    case scope
    when Repository
      "This repository"
    when Organization
      "This organization"
    when User
      "This user"
    when Topic
      "This topic"
    end
  end

  # Public: Get the text to show in the jump to suggestion search scope badge.
  #
  # Returns a string.
  def jump_to_search_scope_badge_text
    case scope
    when Repository
      "In this repository"
    when Organization
      "In this organization"
    when User
      "In this user"
    when Topic
      "In this topic"
    else
      "Search"
    end
  end

  def jump_to_search_scope_badge_aria_label
    badge_text = jump_to_search_scope_badge_text
    return jump_to_search_scope_badge_aria_label_global if badge_text == "Search"

    # downcase first letter so it makes sense to screenreader
    badge_text[0].downcase + badge_text[1..-1]
  end

  def jump_to_search_scope_badge_aria_label_global
    "in all of #{GitHub.search_flavor}"
  end

  # Public: Get the text to show in the jump to suggestion global search badge.
  #
  # Returns a String.
  def jump_to_search_global_badge_text
    "All #{GitHub.search_flavor}"
  end

  # Public: Get the text to use for the search's aria label.
  #
  # Returns a string.
  def search_aria_label_text
    case scope
    when Repository
      "Search this repository"
    when Organization
      "Search this organization"
    when User
      "Search this user"
    when Topic
      "Search this topic"
    else
      "Search #{GitHub.search_flavor}"
    end
  end

  # Public: Is the search bar in the header view scoped?
  #
  # Returns a boolean.
  def search_scoped?
    scope.present?
  end

  # Public: Placeholder text for the search box.
  #
  # Returns a String.
  def search_placeholder_text
    if search_scoped?
      scoped_search_placeholder_text
    else
      unscoped_search_placeholder_text
    end
  end

  # Public: Placeholder text for the search box that will perform a scoped search.
  def scoped_search_placeholder_text
    (jump_to_enabled? && current_user) ? "Search or jump to…" : "Search"
  end

  # Public: Placeholder text for the search box that will perform an unscoped search.
  def unscoped_search_placeholder_text
    (jump_to_enabled? && current_user) ? "Search or jump to…" : "Search #{GitHub.search_flavor}"
  end

  def current_topic
    return unless params

    if topic_name = params[:topic_name]
      @current_topic ||= Topic.find_by_name(topic_name)
    end
  end

  def scope
    # Prefer repository over organization if both are present
    if current_repository.present? && current_repository.persisted?
      current_repository
    elsif current_organization.present? && current_organization.persisted?
      current_organization
    elsif currently_viewed_user.present? && currently_viewed_user.persisted?
      currently_viewed_user
    elsif current_topic.present?
      current_topic
    end
  end

  def jump_to_enabled?
    !is_mobile_request
  end

  def result_types
    self.class.result_types
  end

  def scopes
    self.class.scopes
  end

  def filters
    self.class.filters
  end

  def initial_result_type
    return result_types[params[:type].downcase.to_sym] if params && params[:type].present? && result_types.key?(params[:type].downcase.to_sym)
    default_result_type
  end

  private

  def default_result_type
    case scope
    when Repository
      "Code"
    else
      "All"
    end
  end
end
