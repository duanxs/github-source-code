# frozen_string_literal: true

require "set"
require "scientist"

module Branches
  class BranchFinder
    include Scientist
    STALE_BRANCH_THRESHOLD = 3.months

    attr_accessor :repository, :current_user, :query, :limit

    def initialize(repository, current_user, query: nil, limit: nil, page: nil)
      @repository = repository
      @current_user = current_user
      @query = query
      @limit = limit
      @page = [page.to_i, 1].max
    end

    def default_branch
      @default_branch ||= begin
        default_branch_name = repository.default_branch
        branches_by_committer_date.detect { |b| b.ref.name.b == default_branch_name.b }
      end
    end

    def page_offset
      if limit then (@page - 1) * limit
      else 0
      end
    end

    # We need to limit how many rows we want to cursor through to protect us
    # from DoS-ing ourselves here. 50k feels like a nice magic number, given
    # that we spend ~5ms in your_branches per 1,000 rows we need to look at.
    YOUR_BRANCHES_CURSOR_LIMIT = 50_000

    # We're defining 'ownership' of a branch as ones the user has ever pushed
    # to, except the repository's default branch.
    def your_branches
      return [] unless current_user

      @your_branches ||= begin
        cursor = GitHub::SQLCursor.new(
          Push.where(
                repository_id: repository.id,
                pusher_id: current_user.id)
              .where(["ref <> ?", default_branch.ref.qualified_name])
              .where(["after <> ?", GitHub::NULL_OID])
              .order("created_at DESC"),
        )

        branch_index = branches_by_committer_date.index_by { |b| b.ref.qualified_name }
        existing_refs = Set.new(branch_index.keys)

        limit_enum(cursor.each_row.take(YOUR_BRANCHES_CURSOR_LIMIT).select { |push|
          # ensure we don't duplicate branch names:
          existing_refs.delete?(push.ref)
        }.map { |push|
          branch_index.fetch(push.ref)
        })
      end
    end

    def all_branches
      @all_branches ||= limit_enum(non_default_branches_by_committer_date.reverse)
    end

    def active_branches
      @active_branches ||= begin
        min_date = STALE_BRANCH_THRESHOLD.ago
        limit_enum(non_default_branches_by_committer_date.reverse_each.lazy.take_while { |branch|
          branch.committer_date > min_date
        })
      end
    end

    def stale_branches
      @stale_branches ||= begin
        max_date = STALE_BRANCH_THRESHOLD.ago
        limit_enum(non_default_branches_by_committer_date.lazy.take_while { |branch|
          branch.committer_date <= max_date
        })
      end
    end

    def query_branches
      lowercase_query = query.b.downcase
      @query_branches ||=
        limit_enum(branches_by_committer_date.reverse_each.lazy.select { |branch|
          branch.ref.name.b.downcase.include?(lowercase_query)
        })
    end

    def non_default_branches_by_committer_date
      @non_default_branches_by_committer_date ||=
        branches_by_committer_date.reject { |b| b.ref == default_branch&.ref }
    end

    Branch = Struct.new(:ref, :committer_date_string) do
      def committer_date
        @committer_date ||= Time.rfc2822(committer_date_string)
      rescue ArgumentError
        # Invalid committer date string (e.g. "Sun, 7 Feb 2106 06:28:56 -40643156")
        Time.now
      end
    end

    def branches_by_committer_date
      @branches_by_committer_date ||= begin
        repository.refs.to_a # force refs to all load to avoid loading each individually
        raw_branch_names_and_dates.map { |line|
          date, ref_name = line
          if ref = repository.refs.find_all([ref_name.b]).first
            Branch.new(ref, date)
          else
            # Filter out refs that may have been returned from for-each-ref
            # but aren't in the GitRPC refs cache yet.
          end
        }.compact
      end
    end

    def raw_branch_names_and_dates
      repository.rpc.raw_branch_names_and_dates
    end

    # Quacks somewhat like a WillPaginate::Collection, but doesn't know
    # `total_entries` nor `total_pages` count.
    class PaginatedCollection < Array
      attr_reader :current_page, :per_page

      def self.create(page, per_page)
        new(page, per_page).replace yield
      end

      def initialize(page, per_page)
        @current_page = page
        @per_page = per_page
        @has_more = false
      end

      def replace(ary)
        if per_page && ary.size > per_page
          ary = ary.slice(0, per_page)
          @has_more = true
        end
        super(ary)
      end

      def previous_page
        current_page - 1 if current_page > 1
      end

      def next_page
        current_page + 1 if @has_more
      end

      # Preserve pagination info on map
      def map
        return to_enum(__method__) unless block_given?
        dup.replace super
      end
    end

  private
    def limit_enum(enum, skip = page_offset)
      PaginatedCollection.create(@page, limit) do
        if skip > 0
          since_beginning = enum.first(skip + limit + 1)
          since_beginning.slice(skip..-1) || []
        elsif limit
          enum.first(limit + 1)
        else
          enum.to_a
        end
      end
    end
  end
end
