# frozen_string_literal: true

module Branches
  class BranchView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    include ActionView::Helpers::DateHelper
    include ActionView::Helpers::TextHelper
    include CompareHelper

    def initialize(*args)
      super(*args)

      unless @ref.is_a?(Git::Ref)
        raise TypeError, "expected BranchView#ref to be Ref, but was #{@ref.class}"
      end
    end

    # Public: Required Ref model.
    #
    # Returns Ref.
    attr_reader :ref

    attr_reader :commit, :user, :repository, :pull_request, :has_open_pull_request_as_base, :can_push, :ahead_behind, :current_user, :max_diverged

    delegate :name, :name_for_display, to: :ref

    def pull_request # rubocop:disable Lint/DuplicateMethods
      unless is_default?
        @pull_request unless @pull_request.try(:hide_from_user?, current_user)
      end
    end

    def pull_state
      @pull_state ||= if pull_request
        return :open if pull_request.open?
        return :merged if pull_request.merged?
        :closed
      else
        :none
      end
    end

    def pull_number
      pull_request.try(:number)
    end

    def is_default?
      ref.default_branch?
    end

    def delete_path
      urls.destroy_branch_path(repository.owner, repository, name)
    end

    def restore_path
      urls.create_branch_path(repository.owner, repository, name: name, branch: commit.oid)
    end

    def can_delete?
      return false unless can_push
      return false if has_open_pull_request_as_base

      if pull_request
        pull_request.head_ref_deleteable_by?(current_user)
      else
        ref.deleteable?
      end
    end

    def would_be_deletable?
      return false unless can_push
      !is_default?
    end

    def not_deletable_reason
      if has_open_pull_request_as_base
        "You can’t delete this branch because an open pull request depends on it"
      elsif pull_request
        if pull_request.open? || pull_request.other_open_pulls_using_head_ref?
          "You can’t delete this branch because it has an open pull request"
        elsif ref.protected? && ref.protected_branch.block_deletions_enabled?
          "You can’t delete this protected branch."
        else
          "You can’t delete this branch."
        end
      elsif ref.protected? && ref.protected_branch.block_deletions_enabled?
        "You can’t delete this protected branch."
      end
    end

    def compare_path
      urls.compare_path(repository, name, can_push)
    end

    def ahead_behind_count
      return "" if ahead_behind.nil? || is_default?

      ahead, behind = ahead_behind
      compare_ahead_behind_text(ahead, behind, base_branch: repository.default_branch)
    end

    def behind_count
      if ahead_behind
        ahead, behind = ahead_behind
        behind
      end
    end

    def ahead_count
      if ahead_behind
        ahead, behind = ahead_behind
        ahead
      end
    end

    def behind_count_class
      "even" if behind_count == 0 if ahead_behind
    end

    def ahead_count_class
      "even" if ahead_count == 0 if ahead_behind
    end

    def ahead_percent
      scale.call(ahead_count).round(2)
    end

    def behind_percent
      scale.call(behind_count).round(2)
    end

    def author_link
      if user
        data_attributes = helpers.hovercard_data_attributes_for_user(user)
        helpers.link_to user.login, urls.user_path(user), class: "muted-link", data: data_attributes
      else
        commit.author_name
      end
    end

    def combined_status
      # This was eager loaded in Branches::ListView
      commit.combined_status
    end

    private

    def scale
      @scale ||= GitHub::LogScale.scale!(domain: [1, max_diverged], range: [0, 80])
    end
  end
end
