# frozen_string_literal: true

class Orgs::Teams::HeaderView < Orgs::OverviewView
  attr_reader :team, :selected_nav_item, :org

  VALID_NAV_ITEMS = [:discussions, :members, :teams, :repositories, :projects, :settings]

  # Public: Get the CSS class to use for showing the selected state of the
  # specified nav item.
  #
  # nav_item - Name of nav item to get the CSS class for.
  #
  # Returns a string.
  def selected_class_for_nav_item(nav_item)
    if selected_nav_item && !valid_nav_item?(selected_nav_item)
      raise "Selected nav item (#{selected_nav_item.inspect}) is invalid. Valid ones are #{VALID_NAV_ITEMS.inspect}."
    end

    unless valid_nav_item?(nav_item)
      raise "Passed nav item (#{nav_item}) is invalid. Valid ones are #{VALID_NAV_ITEMS.inspect}."
    end

    "selected" if nav_item == selected_nav_item
  end

  private

  # Internal: Is the specified nav item valid?
  #
  # nav_item - Name of nav item to check validity of.
  #
  # Returns a boolean.
  def valid_nav_item?(nav_item)
    VALID_NAV_ITEMS.include?(nav_item)
  end
end
