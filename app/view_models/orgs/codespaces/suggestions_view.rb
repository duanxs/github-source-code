# frozen_string_literal: true

class Orgs::Codespaces::SuggestionsView < AutocompleteView

  attr_reader :member_logins_with_access

  def suggestions
    all_suggestions = super

    # Remove any suggested users who have been already added to the beta.
    all_suggestions.delete_if { |user| member_logins_with_access.include? user.login }
  end
end
