# frozen_string_literal: true

class Orgs::TeamRepositories::AccessibleToMembersView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :team, :member_name, :action_type, :return_to

  include PlatformHelper

  TeamRepositoriesFragment = parse_query <<-'GRAPHQL'
    fragment on Team {
      membersResourcePath
      name
      repositoriesResourcePath
      approveMembershipRequestResourcePath
      repositories(first: $first, orderBy: { field: PERMISSION, direction: DESC }) {
        totalCount
        edges {
          permission
          node {
            nameWithOwner
          }
        }
      }
    }
  GRAPHQL

  class TeamRepository
    attr_reader :action, :name, :owner

    def initialize(node, action)
      @owner, @name = node.name_with_owner.split("/")
      @action = action
    end
  end

  def initialize(**args)
    super(args)

    @team = TeamRepositoriesFragment.new(args[:team])
  end

  def repositories_count
    team.repositories.total_count
  end

  def repositories
    @repositories ||= team.repositories.edges.map { |r| TeamRepository.new(r.node, r.permission) }
  end

  def submit_path
    case action_type
    when "add_member" then team_members_path
    when "approve_request" then approve_request_path
    end
  end

  def repositories_resource_path
    team.repositories_resource_path.to_s
  end

  def team_name
    team.name
  end

  def more_repositories_to_view?
    repositories_count > accessible_repos_limit
  end

  private

  def team_members_path
    team.members_resource_path.to_s
  end

  def approve_request_path
    base_url = "#{team.approve_membership_request_resource_path}?requester=#{member_name}"
    base_url += "&return_to=#{return_to}" if return_to
    base_url
  end

  def accessible_repos_limit
    Orgs::TeamRepositoriesController::ACCESSIBLE_REPOS_LIMIT
  end
end
