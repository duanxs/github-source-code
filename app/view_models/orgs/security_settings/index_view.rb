# frozen_string_literal: true

module Orgs
  module SecuritySettings
    class IndexView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
      attr_reader :organization, :business, :saml_provider, :current_external_identity, :team_sync_setup_flow

      include PlatformHelper

      SamlProviderFragment = parse_query <<-'GRAPHQL'
        fragment on Organization {
          ssoUrl
          issuer
          idpCertificate
          signatureMethod
          digestMethod
          externallyManagedTeams @include(if: $teamSyncEnabled) {
            totalCount
          }
        }
      GRAPHQL

      def saml_provider # rubocop:disable Lint/DuplicateMethods
        case @saml_provider
        when Organization::SamlProvider, Organization::SamlProviderTestSettings
          @saml_provider
        else
          SamlProviderFragment.new(@saml_provider)
        end
      end

      def sso_url
        business&.saml_provider&.sso_url.presence || saml_provider&.sso_url
      end

      def issuer
        business&.saml_provider&.issuer.presence || saml_provider&.issuer
      end

      def idp_certificate
        business&.saml_provider&.idp_certificate.presence || saml_provider&.idp_certificate
      end

      def two_factor_requirement_form_disabled?
        !GitHub.auth.two_factor_org_requirement_allowed? || !current_user.two_factor_authentication_enabled? ||
            two_factor_required_policy?
      end

      def disabled_form_reason
        if !GitHub.auth.two_factor_authentication_enabled?
          "Built-in two-factor authentication is disabled on your instance."
        elsif two_factor_required_policy?
          disabled_by_administrators_link
        elsif GitHub.auth.builtin_auth_fallback?
          "This setting is disabled since an external adapter is configured."
        elsif !current_user.two_factor_authentication_enabled?
          link = helpers.link_to("your account", urls.settings_user_security_path)
          helpers.safe_join(["This setting requires two-factor authentication on ", link, "."])
        end
      end

      delegate :two_factor_requirement_enabled?,
        :two_factor_requirement_disabled?,
        :affiliated_users_with_two_factor_disabled_exist?,
        :enforcing_two_factor_requirement?,
        :two_factor_required_policy?,
        to: :organization

      def two_factor_requirement_disabled_and_needs_enforced?
        two_factor_requirement_disabled? && affiliated_users_with_two_factor_disabled_exist?
      end

      def organization_saml_settings_saved?
        sso_url.present? && idp_certificate.present?
      end

      def business_saml_settings_saved?
        return false unless business && business.saml_provider

        business.saml_provider.sso_url.present? &&
          business.saml_provider.idp_certificate.present?
      end

      def show_saml_settings?
        return true if organization_saml_settings_saved?

        saml_provider && saml_provider.errors.any?
      end

      def saml_enforced?
        organization.external_identity_session_owner.saml_sso_enforced?
      end

      def unlinked_saml_members
        @unlinked_saml_members ||= organization.unlinked_saml_members
      end

      def saml_test_settings
        saml_provider if saml_provider.is_a?(Organization::SamlProviderTestSettings)
      end

      def saml_testing?
        !!saml_test_settings
      end

      def saml_test_failure?
        saml_testing? && saml_test_settings.failure?
      end

      def saml_test_success?
        saml_testing? && saml_test_settings.success?
      end

      def saml_test_errors
        return unless saml_testing?

        saml_test_settings.message ||
          "invalid settings"
      end

      def disallow_saml_enforcement?
        current_external_identity.nil? || business_saml_configured?
      end

      # https://www.w3.org/TR/2002/REC-xmlenc-core-20021210/Overview.html#sec-Alg-MessageAuthentication
      DEFAULT_SIGNATURE_METHOD = SamlProviderAlgorithms::DEFAULT_SIGNATURE_METHOD
      SIGNATURE_METHOD_OPTIONS = {
        "RSA-SHA1"   => "http://www.w3.org/2001/04/xmldsig-more#rsa-sha1",
        "RSA-SHA256" => "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256",
        "RSA-SHA384" => "http://www.w3.org/2001/04/xmldsig-more#rsa-sha384",
        "RSA-SHA512" => "http://www.w3.org/2001/04/xmldsig-more#rsa-sha512",
      }

      def signature_method
        saml_provider.signature_method.to_s
      end

      def signature_method_label
        SIGNATURE_METHOD_OPTIONS.key(signature_method) ||
          SIGNATURE_METHOD_OPTIONS.key(DEFAULT_SIGNATURE_METHOD)
      end

      def saml_signature_method_options
        SIGNATURE_METHOD_OPTIONS
      end

      # https://www.w3.org/TR/2002/REC-xmlenc-core-20021210/Overview.html#sec-Alg-MessageDigest
      DEFAULT_DIGEST_METHOD = SamlProviderAlgorithms::DEFAULT_DIGEST_METHOD
      DIGEST_METHOD_OPTIONS = {
        "SHA1"   => "http://www.w3.org/2000/09/xmldsig#sha1",
        "SHA256" => "http://www.w3.org/2001/04/xmlenc#sha256",
        "SHA384" => "http://www.w3.org/2001/04/xmlenc#sha384",
        "SHA512" => "http://www.w3.org/2001/04/xmlenc#sha512",
      }

      def digest_method
        saml_provider.digest_method.to_s
      end

      def digest_method_label
        DIGEST_METHOD_OPTIONS.key(digest_method) ||
          DIGEST_METHOD_OPTIONS.key(DEFAULT_DIGEST_METHOD)
      end

      def saml_digest_method_options
        DIGEST_METHOD_OPTIONS
      end

      def sso_url_error
        saml_provider && saml_provider.errors[:sso_url].first
      end

      def issuer_error
        saml_provider && saml_provider.errors[:issuer].first
      end

      def idp_certificate_error
        saml_provider && saml_provider.errors[:idp_certificate].first
      end

      def disabled_by_administrators_link
        link = helpers.link_to("required by enterprise administrators", GitHub.business_accounts_help_url)
        helpers.safe_join(["This setting has been ", link, "."])
      end

      def business_saml_configured?
        organization.saml_enabled_on_business?
      end

      def show_team_sync_settings?
        organization.business_plus? && !business_saml_configured?
      end

      def show_automated_security_fix_settings?
        GitHub.dependabot_enabled?
      end

      def supported_team_sync_provider?
        team_sync_provider_candidate&.supported_for_org?(organization)
      end

      def disallow_team_sync_setup?
        current_external_identity.nil?
      end

      def enable_team_sync_settings?
        organization_saml_settings_saved? &&
        supported_team_sync_provider?
      end

      def team_sync_status
        if saml_testing?
          count = organization.async_externally_managed_teams.sync&.count
        else
          count = saml_provider&.externally_managed_teams&.total_count
        end
        if count
          "#{count} #{"team".pluralize(count)} managed in #{team_sync_provider_type_label}."
        end
      end

      def team_sync_activity_link
        query = { q: "actor:github-team-synchronization[bot] action:team.add_member action:team.remove_member" }
        helpers.link_to("View activity", urls.settings_org_audit_log_path(organization, query))
      end

      IDENTITY_PROVIDER_OPTIONS = {
        "unknown" => "Unknown",
        "azuread" => "Azure AD",
        "okta" => "Okta",
      }

      def show_enable_team_sync_button?
        ::TeamSync::SetupFlow::STARTING_STATES.include?(team_sync_setup_flow.status) ||
          team_sync_setup_flow.status.nil?
      end

      def show_team_sync_assignment_review_available?
        ::TeamSync::SetupFlow::APPROVAL_STATE == team_sync_setup_flow.status
      end

      def team_sync_enabled?
        team_sync_setup_flow.tenant.team_sync_enabled?
      end

      def team_sync_provider_type_label
        IDENTITY_PROVIDER_OPTIONS[provider_type]
      end

      def team_sync_provider_id
        team_sync_setup_flow.provider_id
      end

      def show_ssh_cas?
        organization.ssh_enabled?
      end

      def show_ip_whitelisting?
        GitHub.ip_whitelisting_available?
      end

      def installed_app_ip_whitelist_entries
        return IpWhitelistEntry.none unless GitHub.ip_whitelisting_available?
        return @installed_app_ip_whitelist_entries if defined?(@installed_app_ip_whitelist_entries)
        @installed_app_ip_whitelist_entries = IpWhitelistEntry.installed_for(organization).order(whitelisted_value: :asc)
      end

      def installed_app_ip_whitelist_entries_info
        if installed_app_ip_whitelist_entries.any?
          names = installed_app_ip_whitelist_entries.map { |e| e.owner.name }.uniq.to_sentence
          "The following GitHub Apps you have installed define their own IP allow list entries: #{names}"
        else
          "You have no installed GitHub Apps that define their own IP allow list entries."
        end
      end

      def ssh_cas
        SshCertificateAuthority.usable_for(organization)
      end

      def team_sync_install_path
        raise "SAML Provider is required" unless saml_provider

        case provider_type&.to_sym
        when :azuread
          urls.team_sync_install_path(organization)
        when :okta
          if okta_team_sync?
            urls.new_orgs_team_sync_okta_credentials_path(organization)
          else
            raise "Unsupported provider type for issuer #{saml_provider.issuer}"
          end
        else
          raise "Unsupported provider type for issuer #{saml_provider.issuer}"
        end
      end

      def idp_beta?
        team_sync_provider_candidate&.beta?
      end

      def okta_team_sync?
        provider_type&.to_sym == :okta && GitHub.flipper[:okta_team_sync].enabled?(organization)
      end

      def unlinked_members_count
        @unlinked_members_count ||= unlinked_saml_members.size
      end

      # Showing too many will cause the page to timeout. We pick an arbitrary
      # number to cutoff at
      def show_unlinked_members?
        unlinked_members_count <= 25
      end

      private

      # This is the potential team sync provider, based on the SAML issuer
      def team_sync_provider_candidate
        @team_sync_provider_candidate ||= ::TeamSync::Provider.detect(issuer: saml_provider.issuer)
      end

      # Derive type from SAML issuer (instead of tenants that may have
      # been created from older SAML configurations).
      def provider_type
        team_sync_provider_candidate&.type
      end
    end
  end
end
