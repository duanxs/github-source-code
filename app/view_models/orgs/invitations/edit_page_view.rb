# frozen_string_literal: true
class Orgs::Invitations::EditPageView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :organization, :invitee, :existing_invitation, :page, :selected_team, :team_ids, :role

  TEAM_SUGGESTIONS_PAGE_SIZE = 10

  # Public: Determines if the query is an email when there are no user results
  # and it matches the user email regex.
  #
  # Returns true if an email, false if not.
  def email_invitation?
    User.valid_email?(invitee)
  end

  def invitee_name
    email_invitation? ? invitee : invitee.safe_profile_name
  end

  def editing?
    existing_invitation.present?
  end

  def form_method
    editing? ? "put" : "post"
  end

  def submit_button_text
    if GitHub.bypass_org_invites_enabled?
      "Add member"
    else
      if editing?
        "Update invitation"
      else
        "Send invitation"
      end
    end
  end

  def add_or_invite_action_word
    if GitHub.bypass_org_invites_enabled?
      "Add"
    else
      "Invite"
    end
  end

  def form_path
    if editing?
      if email_invitation?
        urls.org_email_invitation_path(organization, email: invitee)
      else
        urls.org_invitation_path(organization, invitee)
      end
    else
      if GitHub.bypass_org_invites_enabled?
        urls.add_member_to_org_path(organization)
      else
        urls.org_invitations_path(organization)
      end
    end
  end

  def role_input_checked?(role_input)
    role_str = role_input.to_s
    if !editing? || existing_invitation.try(:reinstate?)
      role_str == "direct_member"
    else
      role_str == role
    end
  end

  def is_team_selected?(team_id)
    team_ids&.include?(team_id.to_s)
  end

  def teams
    # Start out with the invitation's existing teams if we're editing.
    @teams = editing? ? filter_externally_managed(existing_invitation.teams) : []

    @teams |= teams_scope
    @teams.compact

    if paginate?
      @teams = @teams.paginate(page: page, per_page: TEAM_SUGGESTIONS_PAGE_SIZE)
    end
    @teams
  end

  def selected_team_ids
    return "" unless team_ids
    team_ids.map(&:to_i).uniq.join(",")
  end

  def ldap_teams?
    teams.any? { |team| team.ldap_mapped? }
  end

  def paginate?
    teams_scope.size > TEAM_SUGGESTIONS_PAGE_SIZE
  end

  def button_text
    if GitHub.bypass_org_invites_enabled?
      "Add member"
    else
      "Send invitation"
    end
  end

  def buy_more_text
    if GitHub.bypass_org_invites_enabled?
      "to add this member"
    else
      "to send this invitation"
    end
  end

  def member_param_name
    if GitHub.bypass_org_invites_enabled?
      "member_id"
    else
      "invitee_id"
    end
  end

  def has_seat_for?(pending_cycle: false)
    if email_invitation?
      organization.pending_invitation_for(email: invitee) || !organization.at_seat_limit?(pending_cycle: pending_cycle)
    else
      organization.has_seat_for?(invitee, pending_cycle: pending_cycle)
    end
  end

  def no_seats_left_on_pending_cycle?
    has_seat_for? && !has_seat_for?(pending_cycle: true)
  end

  def edit_invitation_path
    if email_invitation?
      urls.org_edit_email_invitation_path(organization, email: invitee)
    else
      urls.org_edit_invitation_path(organization, invitee)
    end
  end

  def member_privileges_text
    can_repo = organization.members_can_create_repositories?
    can_team = organization.members_can_create_teams?
    return unless can_repo || can_team
    text = "They can also create new "
    text += "teams" if can_team
    text += " and " if can_repo && can_team
    text += "repositories" if can_repo
    text += "."
  end

  def team_members_count(team)
    team.members_scope_count
  end

  def team_repos_count(team)
    team.repositories_scope_count
  end

  private

  def teams_scope
    scope = if organization.adminable_by?(current_user)
      organization.teams
    else
      ids = organization.teams_for(current_user).select(&:admin?).map(&:id)
      organization.teams.where(id: ids)
    end

    filter_externally_managed(scope)
  end

  def filter_externally_managed(teams)
    return teams unless organization.team_sync_enabled?
    tenant = organization.team_sync_tenant
    teams.where.not(id: tenant.team_group_mappings.distinct.pluck(:team_id))
  end
end
