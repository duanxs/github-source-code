# frozen_string_literal: true

class Orgs::Settings::ActionsPermissions
  attr_reader :organization

  def initialize(organization)
    @organization = organization
  end

  def visible?
    GitHub.actions_enabled?
  end

  def configurable?
    permissions_options.count > 1
  end

  def select_list
    @_select_list  ||= [
      {
        heading: "Enable local & third party Actions for this organization",
        description: "This allows all repositories to execute any Action, whether the code for the Action exists within the same repository,
        same organization, or a repository owned by a third party.",
        value: "ALL_ACTIONS",
        disabled: !all_actions_selectable?,
        selected: @organization&.all_action_executions_enabled?,
      },
      {
        heading: "Enable local Actions only for this organization",
        description: "This allows all repositories to execute any Action as long as the code for the Action exists within the same repository.",
        value: "LOCAL_ACTIONS_ONLY",
        disabled: !local_actions_selectable?,
        selected: @organization&.only_local_action_executions_enabled?,
      },
      {
        heading: "Disable Actions for the organization",
        description: "This disallows any Action from running on any repository in the organization.",
        value: "DISABLED",
        disabled: !disabled_actions_selectable?,
        selected: @organization&.all_action_executions_disabled?,
      },
    ]
  end

  private

  def all_actions_selectable?
    permissions_options.include?(Configurable::ActionExecutionCapabilities::ALL_ACTIONS)
  end

  def local_actions_selectable?
    permissions_options.include?(Configurable::ActionExecutionCapabilities::LOCAL_ACTIONS)
  end

  def disabled_actions_selectable?
    permissions_options.include?(Configurable::ActionExecutionCapabilities::DISABLED)
  end

  def permissions_options
    @_options ||= @organization&.available_action_execution_options || []
  end
end
