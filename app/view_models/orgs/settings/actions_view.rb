# frozen_string_literal: true

class Orgs::Settings::ActionsView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :organization

  def show_split_actions_permissions?
    actions_permissions.visible?
  end

  def actions_permissions
    @_capabilities ||= Orgs::Settings::ActionsPermissions.new(organization)
  end

  def runners_view
    Actions::OrgRunnersView.new(settings_owner: organization, current_user: current_user)
  end
end
