# frozen_string_literal: true

class Orgs::ConsumedLicenseView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :login, :email

  def display_name
    login || email
  end

  def url
    urls.user_path(login) if login.present?
  end
end
