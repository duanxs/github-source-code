# frozen_string_literal: true

class Orgs::TeamMembers::IndexPageView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :organization,
              :team,
              :query,
              :role,
              :membership,
              :graphql_team,
              :graphql_org,
              :immediate_members,
              :child_members,
              :members

  def show_team_maintainer_help_ui?
    return false if team.ldap_mapped?
    return false if team.externally_managed?
    return false if current_user.dismissed_notice?("team_maintainers_banner")
    return false unless organization.adminable_by?(current_user)
    return false if team.legacy_owners?
    return false if team.maintainers.any? { |m| !m.suspended? }
    return false if organization.admins.any? { |a| !a.suspended? && team.member?(a) }
    true
  end

  def suggestions_placeholder_text
    "Add a person"
  end

  # Returns the `selected` class for the `select-menu-item` filter options
  # when the provided filter option matches the selected filter.
  def role_filter_select_class(filter)
    "selected" if role_filter_selected == filter
  end

  def identity_provider
    return if team.locally_managed?
    organization&.team_sync_tenant&.provider_label
  end

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def viewer_can_administer_team?
    @team_adminiable ||= team.adminable_by?(current_user)
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def team_locally_managed?
    team.locally_managed?
  end

  def immediate_team_members_count
    @immediate_members_count ||= immediate_members.count
  end

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def no_immediate_team_members?
    @no_immediate_team_members ||= membership == "IMMEDIATE" && !immediate_members.exists?
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def child_team_members_count
    @child_team_members_count ||= child_members.count
  end

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def no_child_team_members?
    @no_child_team_members ||= membership == "CHILD_TEAM" && !child_members.exists?
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def no_members?
    members.empty?
  end

  private

  # Returns a Symbol for the queried role.
  def role_filter_selected
    role&.underscore&.to_sym || :everyone
  end
end
