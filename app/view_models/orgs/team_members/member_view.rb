# frozen_string_literal: true

class Orgs::TeamMembers::MemberView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :team, :member, :email, :invitation, :disable_bulk_actions, :graphql_team, :graphql_org, :graphql_member

  def team_maintainer?
    team.maintainer?(member)
  end

  def org_admin?
    organization.adminable_by?(member)
  end

  def role_change_item_enabled?(can_administer_team, can_administer_org)
    return false if org_admin?
    return true if can_administer_org

    can_administer_team && organization.direct_member?(member)
  end

  def show_remove_button?(leavable, ldap_mapped, adminable)
    return false if member == current_user && (leavable != "LEAVE")
    return false if ldap_mapped
    return false if invited?

    !!adminable
  end

  def invited?
    invitation.present?
  end

  def organization
    team.organization
  end

  def email_invitation?
    invited? && email.present?
  end

  def team_locally_managed?
    team.locally_managed?
  end

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def viewer_can_administer_team?
    @viewer_can_administer_team ||= team.adminable_by?(current_user)
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def member_url
    if org_admin?
      "/orgs/#{organization}/people/#{member}"
    else
      "/#{member}"
    end
  end

  def member_login
    member.login
  end
end
