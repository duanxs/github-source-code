# frozen_string_literal: true

class Orgs::PolicyOutcomes::ShowView < Orgs::Policies::View
  attr_reader :current_organization, :policy_service_response, :resource

  delegate :policy_name, :policy_id, :policy_natural_id, :resource_id, :resource_type,
    to: :outcome

  delegate :id,
    to: :outcome, prefix: true

  delegate :description, :name,
    to: :resource, prefix: true

  def outcome
    policy_service_response.outcome
  end

  def property_results
    outcome.property_results
  end

  def repository_branch_count
    return 0 unless repository_resource?

    resource.heads&.size || 0
  end

  def repository_open_issues_count
    return 0 unless repository_resource?

    resource.open_issue_count_for(current_user) || 0
  end

  def repository_primary_language_name
    return unless repository_resource?

    resource.primary_language&.name
  end

  def repository_open_pull_request_count
    return 0 unless repository_resource?

    resource.open_pull_request_count_for(current_user) || 0
  end

  def repository_stargazer_count
    return 0 unless repository_resource?

    resource.stargazer_count || 0
  end

  def repository_resource?
    resource_type == :RESOURCE_REPOSITORY
  end

  def organization_resource?
    resource_type == :RESOURCE_ORGANIZATION
  end

  def team_resource?
    resource_type == :RESOURCE_TEAM
  end

  def status
    case outcome.status
    when :SUCCESS then "success"
    when :FAILURE then "failure"
    end
  end

  def evaluated_at
    outcome.evaluated_at.to_time
  end

  def resource_path
    if repository_resource?
      urls.repository_path(resource)
    elsif organization_resource?
      urls.user_path(resource)
    elsif team_resource?
      urls.team_path(resource)
    end
  end

  def compliant?
    outcome.status == :SUCCESS
  end

  def non_compliant?
    !compliant?
  end
end
