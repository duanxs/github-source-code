# frozen_string_literal: true

module Orgs
  module AuditLog
    class IndexPageView < ::AuditLog::IndexPageView
      attr_reader :organization

      def page_title
        "Audit log"
      end

      def tips
        @tips ||= Orgs::AuditLog::TipView.new(organization: organization, user: current_user)
      end

      def search_path(options = {})
        query = if options.present?
          { q: build_query_param(options) }
        else
          {}
        end

        urls.settings_org_audit_log_path(organization, query)
      end

      def suggestions_path
        urls.settings_org_audit_log_suggestions_path(organization)
      end

      def export_path
        urls.org_audit_log_export_path(organization, format: :json)
      end

      def export_git_event_path
        urls.org_audit_log_git_event_export_path(organization, format: :json)
      end

      def filters_partial
        "orgs/audit_log/search_filters"
      end

      def normalize(entries)
        AuditLogEntry.for_orgs(entries)
      end

      private

      # Private: The organization scoped audit log ElasticSearch query.
      #
      # Returns Hash
      def es_query
        {
          current_user: current_user,
          org_id: organization.id,
          phrase: query,
          aggregations: true,
          page: page,
          after: after,
          before: before,
        }
      end
    end
  end
end
