# frozen_string_literal: true

module Orgs
  module AuditLog
    class PlatformIndexPageView < ::AuditLog::IndexPageView
      include PlatformHelper

      attr_reader :organization, :graphql_org

      def initialize(**args)
        super(args)

        @graphql_org = args[:graphql_org]
      end

      def page_title
        "Audit log"
      end

      def tips
        @tips ||= Orgs::AuditLog::TipView.new(organization: organization, user: current_user)
      end

      def search_path(options = {})
        query = if options.present?
          { q: build_query_param(options) }
        else
          {}
        end

        urls.settings_org_audit_log_path(organization, query)
      end

      def export_path
        urls.org_audit_log_export_path(organization, format: :json)
      end
    end
  end
end
