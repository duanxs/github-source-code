# frozen_string_literal: true

class Orgs::AuditLog::SuggestionsView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :organization

  def actions
    AuditLogEntry.organization_action_names
  end

  def users
    @users ||= organization.visible_users_for(current_user).order("users.login")
  end

  def repositories
    @repositories ||= organization.visible_repositories_for(current_user)
  end
end
