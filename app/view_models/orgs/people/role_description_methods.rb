# frozen_string_literal: true

module Orgs::People::RoleDescriptionMethods

  # Public: What does each role mean?
  #
  # Returns a string.
  def role_description
    case role.type
    when :admin
      "Owners have full access to teams, settings, and repositories."
    when :direct_member
      "Members can be assigned to teams and collaborate on repos."
    when :outside_collaborator
      "This person isn’t affiliated with the organization but has access to some of its repositories."
    else
      "This person isn’t affiliated with the organization."
    end
  end
end
