# frozen_string_literal: true

class Orgs::People::FailedInvitationToolbarActionsView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :organization
  attr_reader :selected_invitations

  # Get all the IDs of the selected invitations
  #
  # Returns an array
  def selected_invitation_ids
    selected_invitations.map(&:id)
  end

  # Public: Should we show the "Delete invitation" button?
  #
  # Returns a boolean.
  def show_delete_invitation_button?
    return unless logged_in?
    organization.adminable_by?(current_user)
  end

  def redirect_path
    urls.org_failed_invitations_path(organization)
  end

  def active_failed_invitations
    if defined? @failed_invitations
      return @failed_invitations
    end
    @failed_invitations = organization.active_failed_invitations
  end

  def pending_non_manager_invitations
    if defined? @pending_non_manager_invitations
      return @pending_non_manager_invitations
    end
    @pending_non_manager_invitations = organization.pending_non_manager_invitations.
        includes(invitee: :profile)
  end
end
