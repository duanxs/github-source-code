# frozen_string_literal: true

# Requires `organization` method.
module Orgs::People::VerifiedEmailsMethods
  # Public: Should we display the verified domain emails for this person.
  #
  # Returns a Boolean.
  def show_verified_emails?
    return false unless GitHub.domain_verification_enabled?
    return false unless organization.terms_of_service.business_terms_of_service? || organization.terms_of_service.custom?
    organization.plan_supports?(:display_verified_domain_emails)
  end
end
