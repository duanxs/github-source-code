# frozen_string_literal: true

class Orgs::Repositories::IndexPageView < Orgs::OverviewView
  include Users::RepositoryFilteringMethods

  SIDEBAR_MEMBERS_LIMIT = 20

  attr_reader :organization, :current_page, :phrase, :rate_limited, :type_filter, :language, :graphql_org, :viewer

  OrganizationFragment = parse_query <<-'GRAPHQL'
    fragment on Organization {
      viewerIsAMember
      viewerCanReceiveEmailNotifications
      isPublicGithubSponsor
      isAtSeatLimit @include(if: $show_admin_stuff)
      adminInfo @include(if: $show_admin_stuff) {
        interactionAbility {
          limit
        }
      }

      ...Views::Community::OrgInteractionLimitsBanner::Node
      ...Views::Sponsors::SponsorableProfileSponsors::Sponsorable
    }
  GRAPHQL

  ViewerFragment = parse_query <<-'GRAPHQL'
    fragment on User {
      notificationSettings {
        getsParticipatingEmail
        getsWatchingEmail
      }
    }
  GRAPHQL

  def after_initialize
    @graphql_org = OrganizationFragment.new(@graphql_org)
    @viewer = ViewerFragment.new(@viewer)
  end

  # Internal: Returns the count of this org's pinned repositories.
  def total_pins
    organization.pinned_repositories.public_scope.count
  end

  # Public: Returns true if the org has selected any repositories to show on
  # their profile.
  def has_pinned_repositories?
    return @has_pinned_repositories if defined? @has_pinned_repositories
    @has_pinned_repositories = total_pins > 0
  end

  def user
    organization
  end

  def show_standalone_customize_pinned_repositories?
    can_change_pinned_repositories? && !has_pinned_repositories?
  end

  def top_language_path(language_name)
    language_param = EscapeUtils.escape_url(language_name.downcase)
    path = urls.user_path(organization) + "?language=#{language_param}"
    path += "&type=#{type_filter}" if type_filter.present?
    if phrase.present?
      query_param = EscapeUtils.escape_url(phrase)
      path += "&q=#{query_param}"
    end
    path
  end

  def sidebar_member_url(member)
    if show_admin_stuff?
      urls.org_person_path(organization, member)
    else
      urls.user_path(member)
    end
  end

  def type_filters
    valid_type_filters(viewer: current_user, user: organization, include_private: show_new_repository_button?)
  end

  # By default we only want to show the repositories that the org is a direct owner of, which
  # include sources and its forks. If the user actually enters a search query, then we show all
  # results (including forks of org repos by members)
  def repositories
    @repositories ||= if phrase.present? || language.present?
      search_repos_as(current_user, page: current_page, type: type_filter, types: type_filters)
    else
      filter_repos_by_type(repositories_scope, type: type_filter, types: type_filters).
          where(owner_id: organization).
          recently_updated.preload([:mirror, :primary_language, :owner, :parent, :topics]).
          paginate(page: current_page, per_page: Repository.per_page).to_a
    end
  end

  def selected_language
    helpers.get_selected_language(language)
  end

  # Public: Should we show the Top languages for all of this organization's
  # repositories?
  #
  # Returns a Boolean.
  def show_top_languages?
    top_language_names.any?
  end

  # Public: Returns the most used languages for all of this organization's
  # repositories, private and public, sorted by weight.
  def top_language_names
    return @top_language_names if defined? @top_language_names
    @top_language_names = organization.
        repo_language_breakdown(repos_for_current_user, 5).
        sort_by { |lang, weight| -weight }.map { |(lang, weight)| lang }
  end

  def top_languages_cache_key
    ["v1:top_languages", direct_or_team_member?, organization.cache_key]
  end

  def can_create_discussion_post?
    organization.adminable_by?(current_user)
  end

  def latest_discussion_post
    return @latest_discussion_post if defined?(@latest_discussion_post)
    @latest_discussion_post = organization.discussion_posts.visible_to(current_user).most_recent.first
  end

  def most_used_topics_cache_key
    ["v1:most_used_topics", direct_or_team_member?, organization.cache_key]
  end

  # Public: Should we show the most used topics for all of this organization's
  # repositories?
  #
  # Returns a Boolean.
  def show_most_used_topics?
    most_used_topics.size > 1
  end

  # Public: Returns a list of the topic names that have been most applied to this
  # organization's repositories. Only considers repositories accessible to the
  # current user.
  def most_used_topics
    return @most_used_topics if @most_used_topics

    repos_with_topics = repos_for_current_user.joins(:repository_topics).
      merge(RepositoryTopic.applied)
    @most_used_topics = if repos_with_topics.count < 3
      []
    else
      Topic.popular_names_for_repositories(repositories: repos_with_topics, limit: 5)
    end
  end

  def show_topic_management_link?
    direct_or_team_member?
  end

  def show_toolbar?
    # We can always search if there's public repos
    return true if any_public_repositories?
    # If no public repos and no user, we can't access anything
    return false unless current_user
    # Check if we can access any repos.
    !(show_no_repositories_for_admin? || show_no_repositories_for_member? || show_no_repositories_for_non_member?)
  end

  def show_visibility_filters?
    direct_or_team_member?
  end

  def filtering?
    return @filtering if defined? @filtering
    @filtering = filtering_repositories?(type: type_filter, phrase: phrase, language: language,
                                         types: type_filters)
  end

  def show_no_results?
    filtering? && no_repositories?
  end

  def show_no_repositories_for_member?
    no_repositories? && !filtering? && direct_or_team_member? &&
        !adminable_by_current_user?
  end

  def show_no_repositories_for_admin?
    no_repositories? && !filtering? && adminable_by_current_user?
  end

  def show_no_repositories_for_non_member?
    no_repositories? && !filtering? && !direct_or_team_member?
  end

  def show_page_too_high?
    no_repositories? && !filtering? && !first_page?
  end

  # Public: Returns true if this page has no repositories (due to pagination,
  # filtering, permissions, et al).
  def no_repositories?
    return @no_repositories if defined? @no_repositories
    @no_repositories = !repositories.any?
  end

  # Public: Returns true if the organization has at least one public repository.
  def any_public_repositories?
    public_repositories_count > 0
  end

  def sidebar_members
    return @sidebar_members if @sidebar_members

    # Grab public members first to see if we have at least enough for the preview.
    member_ids = organization.public_members.order(:id).limit(SIDEBAR_MEMBERS_LIMIT).pluck(:id)

    # If we don't have enough and allow private members, add those
    if member_ids.size < SIDEBAR_MEMBERS_LIMIT && !organization.limit_to_public_members?(current_user)
      member_ids.concat(organization.member_ids(limit: SIDEBAR_MEMBERS_LIMIT - member_ids.size))
    end

    @sidebar_members = User.where(id: member_ids)
  end

  def sidebar_teams
    return [] unless logged_in?

    organization.visible_teams_for(current_user).limit(3)
  end

  def ajax_paginate?
    query.present?
  end

  # Public: Returns a sorted list of language names that occur in the
  # repositories owned by the organization that the viewer can access.
  def language_names_for_search
    @languages_names ||= language_names_for(repos_for_current_user)
  end

  # Public: Whether we should be showing the language search dropdown
  def show_language_search?
    language_names_for_search.present?
  end

  # Public: Will more than just the search controls be shown in the header area
  # of the org profile?
  def more_than_search_in_toolbar?
    show_new_repository_button? || show_standalone_customize_pinned_repositories?
  end

  def show_new_repository_button?
    can_create_repository?
  end

  # Public: Returns true if the current user can add, remove, and reorder the
  # org's pinned repositories.
  def can_change_pinned_repositories?
    adminable_by_current_user?
  end

  # Public: Returns true if the user is viewing the first page of repositories.
  def first_page?
    current_page.nil? || current_page.to_s == "1"
  end

  # Public: Checks to see if the org level repository interaction limit banner
  # should be displayed.
  #
  # Returns a Boolean.
  def show_interaction_limits_banner?
    return false unless graphql_org.admin_info

    interactions = graphql_org.admin_info.interaction_ability

    !(interactions.limit == "NO_LIMIT")
  end

  # Public: Checks to see if the notification restrictions banner should be shown
  # to this user.
  #
  # Returns a Boolean
  def show_notification_restrictions_banner?
    return false unless viewer
    return false unless graphql_org.viewer_is_a_member
    return false unless settings = viewer.notification_settings

    receives_emails = settings.gets_participating_email? || settings.gets_watching_email?

    !graphql_org.viewer_can_receive_email_notifications? && receives_emails
  end

  def org_at_seat_limit?
    organization.at_seat_limit?
  end

  def user_or_organization_restricted?
    organization.has_full_trade_restrictions? || (current_user&.has_any_trade_restrictions? && !organization.uncharged_account?)
  end

  def show_github_sponsor_badge?
    return false unless GitHub.sponsors_enabled?
    return false unless GitHub.flipper[:corporate_sponsors_credit_card].enabled?(current_user)
    organization.is_public_github_sponsor?
  end

  def show_developer_program_member_badge?
    !GitHub.enterprise? && organization_developer_program_member?
  end

  private

  # Internal: The number of repositories in scope for this request. We cache
  # this value to save the cost of database roundtrips (even if the value is
  # cached in the DB).
  def repositories_count
    @repositories_count ||= repositories.count
  end

  # Internal: returns a repository scope that is suitable for the current user,
  # based on the user's membership within the organization and the number of
  # repositories associated with the user. Takes into consideration large
  # numbers of associated repos to prevent page slowness.
  #
  # Returns an ActiveRecord::Relation.
  def repos_for_current_user
    @repos_for_current_user ||=
      if direct_or_team_member?
        repositories_scope
      else
        organization.public_repositories
      end
  end

  def live_search?
    !Organization::RepositoryFilter.too_many_repos?(repositories_count)
  end

  def can_create_repository?
    return @can_create_repository if defined? @can_create_repository
    @can_create_repository = organization.can_create_repository?(current_user)
  end
end
