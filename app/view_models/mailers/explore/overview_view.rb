# frozen_string_literal: true

module Mailers
  module Explore
    class OverviewView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
      include UsersHelper
      include ActionView::Helpers::NumberHelper
      include OcticonsCachingHelper

      TEMPLATES = {
        one_off:     { offset: 1.month, span: "this month", title: "GitHub Explore the month of %s - %s" },
        daily:       { offset: 1.day,   span: "today",      title: "GitHub Explore today %s"             },
        monthly:     { offset: 1.month, span: "this month", title: "GitHub Explore the month of %s - %s" },
        fortnightly: { offset: 2.weeks, span: "this week",  title: "GitHub Explore the week of %s - %s"  },
        weekly:      { offset: 1.week,  span: "this week",  title: "GitHub Explore the week of %s - %s"  },
      }

      CAMPAIGN_QUERY = {
        utm_source:   "newsletter",
        utm_medium:   "email",
        utm_campaign: "explore-email",
      }

      TRENDING_LIST_LENGTH = 5

      attr_reader :user, :period, :email, :auto_subscribed

      def initialize(user:, period:, email: nil, auto_subscribed: false)
        @user   = user
        @period = (period || :weekly).to_sym
        @email  = email || user.email
        @auto_subscribed = auto_subscribed
      end

      def unsubscribe_token
        @unsubscribe_token ||= NewsletterSubscription.get_unsubscribe_token(user, "explore")
      end

      def github_url(*args)
        URI.join(GitHub.url, *args, self.campaign_query).to_s
      end

      def github_link
        "#{GitHub.url}?#{self.class::CAMPAIGN_QUERY.merge(utm_content: "dotcom").to_query}"
      end

      def unsubscribe_link
        "#{GitHub.url}/email/unsubscribe?#{{ token: unsubscribe_token }.to_query}"
      end

      def campaign_query
        "?#{self.class::CAMPAIGN_QUERY.merge(utm_term: period).to_query}"
      end

      def trending
        Trending.repos(viewer: @user,
                       cache_key: "trending:repos:mailer:#{period}",
                       ttl: 1.day,
                       options: { period: period, limit: trending_list_length, from_job: true })
      end

      # HACK: why do we explicitly need to check for a cache here?
      def trending_cache_exists
        GitHub.cache.exist?("trending:repos:query:#{period}")
      end

      def trending_list_length
        TRENDING_LIST_LENGTH
      end

      def span
        TEMPLATES[period][:span]
      end

      def title
        the_start = start_date.strftime("%b %-d")
        the_end   = end_date.strftime("%b %-d")
        values    = period == :daily ? [the_end] : [the_start, the_end]
        TEMPLATES[period][:title] % values
      end

      def period_subtext
        today_text = end_date.strftime("%b %-e")
        other_text = "%s - %s" % [start_date.strftime("%b %e"), end_date.strftime("%b %e")]
        case period
        when :daily
          helpers.safe_join(["today, ", helpers.content_tag(:strong, today_text, style: "color: #333")]) # rubocop:disable Rails/ViewModelHTML
        when :monthly, :one_off
          helpers.safe_join(["the month of ", helpers.content_tag(:strong, other_text, style: "color: #333")]) # rubocop:disable Rails/ViewModelHTML
        else
          helpers.safe_join(["the week of ", helpers.content_tag(:strong, other_text, style: "color: #333")]) # rubocop:disable Rails/ViewModelHTML
        end
      end

      def start_date
        @start_date ||= user.time_zone.now - TEMPLATES[period][:offset]
      end

      def end_date
        @end_date ||= user.time_zone.now
      end

      def recommendation_reason_sentence(reason)
        case reason
        when PlatformTypes::RepositoryRecommendationReason::TRENDING
          "Trending right now on GitHub"
        when PlatformTypes::RepositoryRecommendationReason::TOPICS
          "Based on topics you're interested in"
        when PlatformTypes::RepositoryRecommendationReason::POPULAR
          "Popular on GitHub"
        when PlatformTypes::RepositoryRecommendationReason::STARRED
          "Based on repositories you’ve starred"
        when PlatformTypes::RepositoryRecommendationReason::FOLLOWED
          "Based on people you follow"
        when PlatformTypes::RepositoryRecommendationReason::VIEWED
          "Based on repositories you’ve viewed"
        when PlatformTypes::RepositoryRecommendationReason::CONTRIBUTED
          "Based on your public repository contributions"
        when PlatformTypes::RepositoryRecommendationReason::OTHER
          "This repository might interest you"
        end
      end
    end
  end
end
