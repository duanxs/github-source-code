# frozen_string_literal: true

module Users
  class MarketplacePendingInstallationsNoticeView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    include HydroHelper

    attr_reader :notice_name

    def initialize(current_user:, user_session:)
      @current_user = current_user
      @notice_name = "marketplace_pending_installations"
    end

    def instrument_notice_displayed_event!
      GlobalInstrumenter.instrument("marketplace_retargeting_notice_displayed",
        hydro_payload.merge({
          client: {
            user: current_user,
          },
        }),
      )
    end

    def hydro_click_attrs
      hydro_click_tracking_attributes("marketplace_retargeting_notice_click", hydro_payload)
    end

    def hydro_dismissed_attrs
      hydro_click_tracking_attributes("marketplace_retargeting_notice_dismissed", hydro_payload)
    end

    def listing_names
      @listing_names ||= pending_installations.map { |item| item.listing&.name }.compact.uniq
    end

    private

    def hydro_payload
      {
        marketplace_listing_ids: pending_installations.map { |item| item.listing&.id },
        marketplace_listing_plan_ids: pending_installations.map { |item| item.subscribable&.id },
        notice_name: notice_name,
      }
    end

    def pending_installations
      @pending_installations ||= current_user.pending_marketplace_installations.
        preload(subscribable: :listing).
        first(10)
    end
  end
end
