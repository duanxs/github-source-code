# frozen_string_literal: true

module Users
  module ContributedRepositoriesMethods
    def repositories_contributed_to(limit: 5)
      # FIXME: This quick exit for the large_bot_account can be removed once we
      # have solved the performance issues surrounding contribution graphs.
      #
      # see https://github.com/github/core-app/issues/84
      return [] if user.large_bot_account?
      @repositories_contributed_to ||= user.
          repositories_contributed_to(viewer: current_user, limit: limit)
    end

    def total_repositories_contributed_to_count
      # FIXME: This quick exit for the large_bot_account can be removed once we
      # have solved the performance issues surrounding contribution graphs.
      #
      # see https://github.com/github/core-app/issues/84
      return 0 if user.large_bot_account?
      @total_repositories_contributed_to_count ||= user.
          total_repositories_contributed_to_count(viewer: current_user)
    end
  end
end
