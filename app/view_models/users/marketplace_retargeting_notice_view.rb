# frozen_string_literal: true

module Users
  class MarketplaceRetargetingNoticeView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    include HydroHelper

    attr_reader :notice_name

    def initialize(current_user:, user_session:)
      @current_user = current_user
      @notice_name = "marketplace_retargeting"
    end

    def instrument_notice_displayed_event!
      GlobalInstrumenter.instrument("marketplace_retargeting_notice_displayed",
        hydro_payload.merge({
          client: {
            user: current_user,
          },
        }),
      )
    end

    def hydro_click_attrs
      hydro_click_tracking_attributes("marketplace_retargeting_notice_click", hydro_payload)
    end

    def hydro_dismissed_attrs
      hydro_click_tracking_attributes("marketplace_retargeting_notice_dismissed", hydro_payload)
    end

    def listing_names
      order_previews.map { |preview| preview.listing.name }.uniq
    end

    private

    def hydro_payload
      {
        marketplace_listing_ids: order_previews.map { |preview| preview.listing&.id },
        marketplace_listing_plan_ids: order_previews.map { |preview| preview.listing_plan&.id },
        notice_name: notice_name,
      }
    end

    def order_previews
      @order_previews ||= current_user.
        marketplace_order_previews.
        with_valid_listing_data.
        order(:viewed_at).
        preload(:listing, :listing_plan).
        first(50)
    end
  end
end
