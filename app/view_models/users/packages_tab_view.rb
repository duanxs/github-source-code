# frozen_string_literal: true

# Controls the packages view for a user,
# e.g., https://github.com/jessicard?tab=packages
class Users::PackagesTabView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :user, :current_user, :phrase, :user_session, :registry_package_owner
end
