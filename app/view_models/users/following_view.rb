# frozen_string_literal: true

module Users
  class FollowingView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :user, :viewer, :current_page

    def me?
      user == viewer
    end

    def page_title
      if me?
        "Who You’re Following"
      else
        title = "#{user}"
        title += " (#{user.profile_name})" if user.profile_name.present?
        "#{title} / Following"
      end
    end

    def following
      @following ||= user.following.filter_spam_for(viewer).
          order("followers.created_at desc").
          simple_paginate(page: current_page, per_page: 51)
    end
  end
end
