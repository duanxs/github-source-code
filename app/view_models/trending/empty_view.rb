# frozen_string_literal: true

class Trending
  class EmptyView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    include TrendingMethods
    attr_reader :list_type, :language

    def requested_language
      known_language? ? strong_language_name : "your choices"
    end

    def strong_language_name
      helpers.content_tag :strong, known_language_name # rubocop:disable Rails/ViewModelHTML
    end
  end
end
