# frozen_string_literal: true

module Events
  class DeploymentView < View
    # deployments not rendered on timeline, just needed to avoid errors
  end
end
