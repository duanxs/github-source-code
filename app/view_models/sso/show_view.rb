# frozen_string_literal: true

module Sso
  class ShowView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :target
    attr_reader :member
    attr_reader :business_user_account

    def page_title
      "#{ member }'s single sign-on details"
    end

    def external_identity
      return nil unless cloud_member?

      return @external_identity if defined?(@external_identity)
      provider = target.external_identity_session_owner.saml_provider
      @external_identity = provider&.external_identities&.linked_to(member)&.first
    end

    def allow_revoke_actions?
      return true if target.is_a?(Business)
      return true if target.business&.owner?(current_user)
      !target.business&.saml_sso_enabled?
    end

    def cloud_member?
      member.is_a?(User)
    end

    def whitelisted_credentials
      @whitelisted_credentials ||=
      begin
        authorized_tokens = authorized_credentials.select(&:using_personal_access_token?)
        authorized_keys = authorized_credentials.select(&:using_public_key?)
        (authorized_tokens + authorized_keys).map(&:credential).compact
      end
    end

    def target_description
      return "organization" if target.is_a?(Organization)
      "enterprise account"
    end

    def revoke_token_warning
      organization_message = if target.is_a?(Organization)
        "this organization’s private resources"
      else
        "private resources of organizations that belong to this enterprise account"
      end

      "Any applications or scripts using this token will no longer be able to access #{organization_message}. You cannot undo this action."
    end

    def unlink_identity_path
      if target.is_a?(Organization)
        urls.org_person_unlink_identity_path(target, member)
      else
        urls.unlink_identity_enterprise_user_account_path(business_user_account)
      end
    end

    def revoke_sso_session_path(session_id)
      if target.is_a?(Organization)
        urls.org_person_revoke_sso_session_path(target, member, session_id)
      else
        urls.revoke_sso_session_enterprise_user_account_path(business_user_account, session_id)
      end
    end

    def revoke_credential_path(token, credential_type)
      if target.is_a?(Organization)
        urls.org_person_revoke_sso_token_path(target, member, token, credential_type: credential_type)
      else
        urls.revoke_sso_token_enterprise_user_account_path(business_user_account, token, credential_type: credential_type)
      end
    end

    def external_identity_group_ids(source: :saml, display_limit: 5)
      return [] unless target.is_a?(Business)

      user_data = case source
      when :saml
        external_identity.saml_user_data
      when :scim
        external_identity.scim_user_data
      end
      group_ids = Platform::Provisioning::GroupsUserDataWrapper.new(user_data).groups

      return group_ids unless group_ids.length > display_limit
      extra_count = group_ids.length - display_limit
      group_ids = group_ids.slice(0, display_limit)
      group_ids << "#{extra_count} more..."
    end

    private

    def authorized_credentials
      return Organization::CredentialAuthorization.none unless cloud_member?

      Organization::CredentialAuthorization.
        by_organization(organization: organizations).
        by_actor(actor: member).
        active
    end

    def organizations
      return [target] if target.is_a?(Organization)

      @organizations ||= (target.organizations.to_a & current_user.owned_organizations)
    end
  end
end
