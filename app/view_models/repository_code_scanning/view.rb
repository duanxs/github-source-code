# frozen_string_literal: true

module RepositoryCodeScanning
  class View < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :query, :repository, :response, :tools, :selected_tool

    def after_initialize
      helpers.extend(TextHelper)
      helpers.extend(ScanningHelper)
    end

    def severity_symbols
      [:ERROR, :WARNING, :NOTE]
    end

    def severity_octicon(rule_severity)
      case rule_severity
      when :NOTE
        "note"
      when :WARNING
        "alert"
      when :ERROR
        "circle-slash"
      else
        "alert"
      end
    end

    def severity_color(rule_severity)
      case rule_severity
      when :NOTE
        "text-gray-light"
      when :WARNING
        "text-orange-light"
      when :ERROR
        "text-red"
      else
        ""
      end
    end

    def result_message_text(result)
      html = GitHub::Goomba::MarkdownPipeline.to_html(result.message_text)
      helpers.strip_tags_and_collapse_whitespace(html)
    end

    def result_title(result)
      if result.rule_short_description.present?
        result.rule_short_description
      else
        # FIXME remove once all results have short descriptions
        result_message_text(result)
      end
    end

    def timestamp_to_time(timestamp)
      Time.at(timestamp.nanos * 10**-9 + timestamp.seconds)
    end

    def result_resolved?(result)
      result.resolution.present? && result.resolution != :NO_RESOLUTION
    end

    def closure_reasons
      {
        FALSE_POSITIVE: "False positive",
        USED_IN_TESTS: "Used in tests",
        WONT_FIX: "Won't fix",
      }
    end

    def closure_reason_description(result)
      closure_reasons[result.resolution].downcase
    end

    def close_path(number: nil, reason:)
      urls.repository_code_scanning_close_path(repository.owner, repository, number: number, reason: reason)
    end

    def reopen_path(number: nil)
      urls.repository_code_scanning_reopen_path(repository.owner, repository, number: number)
    end

    def turboscan_unavailable?
      response.nil? || response.data.nil? || response.error.present?
    end

    def rule_tags
      @rule_tags ||= response&.data&.rule_tags || []
    end

    def index_path(**params)
      urls.repository_code_scanning_results_path(repository.owner, repository, params)
    end

    def display_ref_name(ref_name)
      Git::Ref::COMMON_PREFIXES.each do |prefix|
        if ref_name.start_with?(prefix)
          return ref_name[prefix.size, ref_name.length]
        end
      end
      ref_name
    end

    def result_path(result, ref_name: nil)
      if ref_name.present?
        ref_query = Search::Queries::CodeScanningQuery.stringify([[:ref, ref_name]])
      end
      urls.repository_code_scanning_result_path(repository.owner, repository, number: result.number, query: ref_query)
    end

    def related_location_path(commit_oid:, related_location:)
      params = {
        commit_oid: commit_oid,
        file_path: related_location.location.file_path,
        start_line: related_location.location.start_line,
        end_line: related_location.location.end_line,
        start_column: related_location.location.start_column,
        end_column: related_location.location.end_column,
      }
      urls.repository_code_scanning_related_location_popover_path(repository.owner, repository, params)
    end

    def split_file_path_and_name(filepath)
      helpers.split_file_path_and_name(filepath)
    end

    def blob_path(commit_oid:, file_path:, start_line:, end_line:)
      "/#{repository.name_with_owner}/blob/#{commit_oid}/#{urls.escape_url_branch(file_path)}#L#{start_line}-L#{end_line}"
    end

    def alerts_writable_by_current_user?
      repository.code_scanning_writable_by?(current_user)
    end

    def alerts_readable_by_current_user?
      repository.code_scanning_readable_by?(current_user)
    end

    def feedback_link
      "https://support.github.com/contact?subject=Code+Scanning+Beta+Support&tags=code-scanning-support"
    end

    def tool_names_and_filter_urls
      @tool_names_and_filter_urls ||= tools.sort_by(&:display_name).map do |tool|
        tool_query = Search::Queries::CodeScanningQuery.stringify([[:tool, tool.display_name]])
        {
          name: tool.display_name,
          url: index_path(query: tool_query),
          selected: tool.name.casecmp?(selected_tool&.name),
        }
      end
    end
  end
end
