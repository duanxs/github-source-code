# frozen_string_literal: true

module RepositoryCodeScanning
  class ShowView < ResultView
    include CodeNavigationHelper

    CWE_PATTERN = /cwe(?:[- ]*0*)(\d+)/i.freeze

    def rule_help
      response&.data&.rule_help
    end

    def has_rule_help?
      rule_help.present?
    end

    def has_code_paths
      response&.data&.has_code_paths || false
    end

    def timeline_events
      @timeline_events ||= response&.data&.timeline_events || []
    end

    def ref_commit_locations
      @ref_commit_locations ||= result.instances.uniq(&:ref_name)
    end

    def rule_description
      return @rule_description if defined?(@rule_description)
      @rule_description = if has_rule_help?
        GitHub.cache.fetch(rule_description_cache_key) do
          rule_help_html[:description]
        end
      end
    end

    def rule_description_markdown_without_overview
      return "" unless has_rule_help?
      lines = rule_help.split("\n")
      overview_index = lines.find_index("## Overview")
      if overview_index.present?
        lines[overview_index+1..-1].join("\n").strip
      else
        rule_help
      end
    end
    private :rule_description_markdown_without_overview

    def rule_description_cache_key
      @rule_description_cache_key ||= [
        "code_scanning",
        "rule_description",
        "v1",
        rule_id,
        tool_name,
        Digest::SHA256.hexdigest(rule_help),
      ].join(":")
    end

    def rule_intro
      return @rule_intro if defined?(@rule_intro)
      @rule_intro = if has_rule_help?
        GitHub.cache.fetch(rule_intro_cache_key) do
          rule_help_html[:intro]
        end
      end
    end

    def rule_intro_cache_key
      @rule_intro_cache_key ||= [
        "code_scanning",
        "rule_intro",
        "v1",
        rule_id,
        tool_name,
        Digest::SHA256.hexdigest(rule_help),
      ].join(":")
    end

    def rule_help_html
      return @rule_help_html if defined?(@rule_help_html)

      rule_help_result = {}
      pipeline_result = {}

      description = GitHub::Goomba::CodeScanningRuleHelpMarkdownPipeline.to_html(rule_description_markdown_without_overview, {}, pipeline_result)

      rule_help_result[:description] = description
      rule_help_result[:intro] = pipeline_result[:first_paragraph]

      @rule_help_html = rule_help_result
    end

    def rule_help_expandable?
      return @rule_help_expandable if defined?(@rule_help_expandable)
      @rule_help_expandable = rule_intro.strip != rule_description.strip
    end

    def severity_state_badge_css(rule_severity)
      case rule_severity
      when :WARNING
        "bg-yellow-light text-orange"
      when :ERROR
        "bg-red-light color-red-7"
      else
        "bg-gray text-gray"
      end
    end

    def timeline_users
      @timeline_users ||= User.with_ids(timeline_events.map(&:user_id).compact.uniq).index_by(&:id)
    end

    def safe_user(user_id)
      timeline_users[user_id] || User.ghost
    end

    def blob
      blob_for(location)
    end

    def page_title
      turboscan_unavailable? ?
      "Code scanning alerts · #{repository.name_with_owner}" :
      "#{result_title(result)} · Code scanning alert ##{result&.number} · #{repository.name_with_owner}"
    end

    # Only display events for which we have icons defined and text written
    # - we don't want to display blank events.
    # If you add a new type here, be sure to add a case to timeline_event_message
    # Items in the array are: icon name, icon classes, timeline badge classes
    EVENTS_ICONS_AND_COLORS = {
      TIMELINE_EVENT_TYPE_ALERT_APPEARED_IN_BRANCH: ["git-branch", "text-gray-dark", "bg-dark-gray"],
      TIMELINE_EVENT_TYPE_ALERT_CLOSED_BECAME_FIXED: ["check", "text-gray-dark", "bg-dark-gray"],
      TIMELINE_EVENT_TYPE_ALERT_CLOSED_BY_USER: ["shield-x", "text-white", "bg-red"],
      TIMELINE_EVENT_TYPE_ALERT_CREATED: ["shield", "text-gray-dark", "bg-dark-gray"],
      TIMELINE_EVENT_TYPE_ALERT_REAPPEARED: ["shield", "text-gray-dark", "bg-yellow"],
      TIMELINE_EVENT_TYPE_ALERT_REOPENED_BY_USER: ["primitive-dot", "text-white", "bg-green"],

      # pseudo event type - doesn't map back to data received from Turbo-Scan
      # this is a subtype of TIMELINE_EVENT_TYPE_ALERT_CLOSED_BECAME_FIXED
      TIMELINE_EVENT_TYPE_ALERT_CLOSED_BECAME_FIXED_THIS_REF: ["shield-check", "text-white", "bg-green"],
    }.freeze

    def filtered_timeline_events
      @filtered_timeline_events ||= timeline_events.select do |timeline_event|
        EVENTS_ICONS_AND_COLORS.include?(timeline_event.type)
      end
    end

    def timeline_event_icon_details(timeline_event)
      timeline_event_type = if timeline_event_is_fix_on_current_ref?(timeline_event)
        :TIMELINE_EVENT_TYPE_ALERT_CLOSED_BECAME_FIXED_THIS_REF
      else
        timeline_event.type
      end
      EVENTS_ICONS_AND_COLORS[timeline_event_type]
    end

    def timeline_event_is_fix_on_current_ref?(timeline_event)
      timeline_event.type == :TIMELINE_EVENT_TYPE_ALERT_CLOSED_BECAME_FIXED &&
        timeline_event.ref_name.present? &&
        ref_selected?(timeline_event.ref_name)
    end

    def show_timeline_commit?(timeline_event)
      timeline_event.type != :TIMELINE_EVENT_TYPE_ALERT_APPEARED_IN_BRANCH && timeline_event.commit_oid.present?
    end

    def show_path?(timeline_event)
      timeline_event.file_path.present? &&
        timeline_event.ref_name.present? &&
        (timeline_event.type == :TIMELINE_EVENT_TYPE_ALERT_CREATED || timeline_event.type == :TIMELINE_EVENT_TYPE_ALERT_REAPPEARED)
    end

    def initial_tool_version
      return @initial_tool_version if defined? @initial_tool_version

      created_event = filtered_timeline_events.find { |event| event.type == :TIMELINE_EVENT_TYPE_ALERT_CREATED }
      @initial_tool_version = created_event&.tool_version
    end

    def tool_version_updated?(timeline_event)
      initial_tool_version.present? && timeline_event.tool_version.present? && initial_tool_version != timeline_event.tool_version
    end

    def tag_path(tag)
      tag_query = Search::Queries::CodeScanningQuery.stringify([[:tag, tag]])
      index_path(query: tag_query)
    end

    def ref_selected?(ref)
      commit_location.ref_name == ref
    end

    def code_snippet_path_link
      blob_path(
        commit_oid: commit_location.commit_oid,
        file_path: commit_location.location.file_path,
        start_line: commit_location.location.start_line,
        end_line: commit_location.location.end_line,
      )
    end

    def cwe_id(rule_tag)
      match = CWE_PATTERN.match(rule_tag)
      match.present? ? match[1].to_i : nil
    end

    def rule_tag_hovercard_attributes(rule_tag)
      id = cwe_id(rule_tag)
      if id.present?
        helpers.hovercard_data_attributes_for_cwe(id: id, repository: repository)
      end
    end

    def result_fixed?
      commit_location.present? && commit_location.is_fixed
    end

    def status_badge_title
      if result_resolved?(result)
        "Status: Closed as #{closure_reason_description(result)}"
      elsif result_fixed?
        "Status: Fixed in #{commit_location.ref_name}"
      else
        "Status: Open"
      end
    end

    def status_badge_color
      if result_resolved?(result)
        :red
      elsif result_fixed?
        :purple
      else
        :green
      end
    end

    def status_badge_label
      if result_resolved?(result)
        "Closed"
      elsif result_fixed?
        "Fixed"
      else
        "Open"
      end
    end

    def rule_tag_label(rule_tag)
      # Drop external/cwe/ prefix
      rule_tag = rule_tag.gsub(/\Aexternal\/cwe\/(cwe-[\d]+)\z/, "\\1")

      rule_tag = rule_tag.titleize.downcase

      # Normalize cwe 012 to CWE-12
      rule_tag = rule_tag.gsub(CWE_PATTERN, "CWE-\\1")
    end
  end
end
