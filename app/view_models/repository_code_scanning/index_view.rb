# frozen_string_literal: true

module RepositoryCodeScanning
  class IndexView < View
    attr_reader :ref_names, :page, :per_page, :commit_locations

    SORT_OPTIONS = [
      { label: "Newest", query: "created-desc" },
      { label: "Oldest", query: "created-asc" },
      { label: "Recently updated", query: "updated-desc" },
      { label: "Least recently updated", query: "updated-asc" },
    ].freeze

    def results
      total_count = closed? ? closed_count : open_count
      @results ||= WillPaginate::Collection.create(page, per_page, total_count) do |pager|
        pager.replace(response&.data&.results || [])
      end
    end

    def open_count
      @open_count ||= response&.data&.open_count || 0
    end

    def closed_count
      @closed_count ||= response&.data&.resolved_count || 0
    end

    def rules
      @rules ||= response&.data&.rules || []
    end

    def rule_filters
      rules.uniq(&:id).sort_by(&:id).map { |rule|
        {
          label: rule.short_description,
          id: rule.id,
          selected: query.qualifier_selected?(name: :id, value: rule.id),
          url: index_path(query: query.toggle_qualifier(name: :id, value: rule.id)),
        }
      }.unshift({
        label: "All",
        selected: !query.contains_qualifier?(name: :id),
        url: index_path(query: query.replace_qualifier(name: :id , value: nil)),
      })
    end

    def tag_filters
      rule_tags.sort.map { |tag|
        {
          label: tag,
          selected: query.qualifier_selected?(name: :tag, value: tag),
          url: index_path(query: query.toggle_qualifier(name: :tag, value: tag)),
        }
      }.unshift({
        label: "All",
        selected: !query.contains_qualifier?(name: :tag),
        url: index_path(query: query.replace_qualifier(name: :tag , value: nil)),
      })
    end

    def severity_filters
      severity_symbols.map { |severity_symbol|
        severity = severity_symbol.downcase.to_s
        {
          label: severity.capitalize,
          selected: query.qualifier_selected?(name: :severity, value: severity),
          url: index_path(query: query.toggle_qualifier(name: :severity, value: severity)),
          symbol: severity_symbol,
        }
      }.unshift({
        label: "All",
        selected: !query.contains_qualifier?(name: :severity),
        url: index_path(query: query.replace_qualifier(name: :severity , value: nil)),
      })
    end

    def severity_label(rule_severity)
      case rule_severity
      when :NOTE
        "Note"
      when :WARNING
        "Warning"
      when :ERROR
        "Error"
      else
        "Unknown"
      end
    end

    def closed_path
      index_path(query: query.replace_qualifier(name: :is, value: "closed"))
    end

    def open_path
      index_path(query: query.replace_qualifier(name: :is, value: nil))
    end

    def refs_path(**params)
      urls.repository_code_scanning_results_ref_list_path(repository.owner, repository, { query: query.user_query }.merge(params))
    end

    def rules_path(**params)
      urls.repository_code_scanning_results_rule_list_path(repository.owner, repository, { query: query.user_query }.merge(params))
    end

    def tags_path(**params)
      urls.repository_code_scanning_results_tag_list_path(repository.owner, repository, { query: query.user_query }.merge(params))
    end

    def ref_selected?(ref)
      ref_names.include?(ref)
    end

    def ref_path(ref)
      index_path(query: query.toggle_qualifier(name: :ref, value: ref))
    end

    def closed?
      query.closed?
    end

    def open?
      !query.closed?
    end

    def analysis_exists?
      # Does an analysis exist matching the current alert filters?
      response&.data&.analysis_exists
    end

    def show_pagination?
      results.total_pages > 1
    end

    def selected_sort
      return @selected_sort if defined? @selected_sort

      @selected_sort = SORT_OPTIONS.find { |details| details[:query] == query.sort }
    end

    def sort_options
      SORT_OPTIONS.map { |details|
        {
          label: details[:label],
          selected: selected_sort == details,
          url: index_path(query: query.toggle_qualifier(name: :sort, value: details[:query])),
        }
      }.unshift({
        label: "Most important",
        selected: !query.contains_qualifier?(name: :sort),
        url: index_path(query: query.replace_qualifier(name: :sort, value: nil)),
      })
    end

    def show_workflow_setup?
      !turboscan_unavailable? &&
        results.empty? &&
        !repository.code_scanning_analysis_exists?
    end

    # Methods for the filter bar

    def suggestable_qualifiers
      [
        { value: "tool:", description: "filter by tool" },
        { value: "severity:", description: "filter by severity" },
        { value: "is:", description: "filter by open/closed state" },
        { value: "ref:", description: "filter by branches/tags" },
        { value: "id:", description: "filter by rule" },
        { value: "tag:", description: "filter by tag" },
        { value: "resolution:", description: "filter by closure reason" },
        { value: "sort:", description: "sort by" },
      ]
    end

    def suggestable_tools
      tools.sort_by(&:display_name).map { |tool|
        { value: Search::ParsedQuery.encode_value(tool.display_name), description: tool.name }
      }
    end

    def suggestable_severities
      severity_symbols.map { |severity_symbol| { value: severity_symbol.downcase.to_s } }
    end

    def suggestable_states
      [
        { value: "open", description: "Open alerts" },
        { value: "closed", description: "Closed alerts" },
      ]
    end

    def suggestable_sorts
      SORT_OPTIONS.map { |sort| { value: sort[:query], description: sort[:label] } }
    end

    def suggestable_input_value
      @suggestable_input_value ||= begin
        value = query.query_string&.rstrip
        value.blank? ? "" : "#{value} "
      end
    end

    def suggestable_closure_reasons
      reasons = closure_reasons.map { |key, value| { value: key.to_s.downcase.gsub("_", "-"), description: value } }
      reasons.unshift({ value: "fixed", description: "Fixed"})
    end

    def search_filters
      selected_tool_name = selected_tool&.display_name
      @search_filters ||= [
        {
          label: "Open security alerts",
          query: [[:tool, selected_tool_name], [:is, "open"], [:tag, "security"], [:sort, "created-desc"]],
        },
        {
          label: "Open maintanability alerts",
          query: [[:tool, selected_tool_name], [:is, "open"], [:tag, "maintainability"], [:sort, "created-desc"]]
        },
        {
          label: "Recently closed alerts",
          query: [[:tool, selected_tool_name], [:is, "closed"], [:sort, "updated-desc"]],
        },
        {
          label: "Oldest error alerts",
          query: [[:tool, selected_tool_name], [:is, "open"], [:severity, "error"], [:sort, "created-asc"]],
        },
      ].map do |filter|
        filter.merge(url: index_path(query: Search::Queries::CodeScanningQuery.stringify(filter[:query])))
      end
    end

    def commit_location_for(result)
      commit_locations.fetch(result.number, result.instances.first)
    end

    def index_result_path(result)
      result_path(result, ref_name: commit_location_for(result)&.ref_name)
    end

    def protip
      @protip ||= protips.sample
    end

    def protips
      @protips ||= [
        { text: "You can run CodeQL locally using Visual Studio Code.", link_text: "Learn more", link_href: "https://github.com/github/vscode-codeql#codeql-for-visual-studio-code" },
        { text: "You can run CodeQL locally from the command line.", link_text: "Learn more", link_href: "https://github.com/github/codeql-cli-binaries#codeql-cli" },
        { text: "CodeQL queries are developed by an open-source coalition called the", link_text: "GitHub Security Lab", link_href: "https://securitylab.github.com" },
        { text: "You can upload code scanning analyses from other third-party tools using GitHub Actions.", link_text: "Learn more", link_href: "#{GitHub.help_url}/github/managing-security-vulnerabilities/uploading-a-code-scanning-analysis-to-github" },
        { text: "You can configure CodeQL to run with additional queries.", link_text: "Learn more", link_href: "#{GitHub.help_url}/github/finding-security-vulnerabilities-and-errors-in-your-code/configuring-code-scanning#using-a-custom-configuration" },
        { text: "The libraries and queries that power CodeQL are open-source.", link_text: "Learn more", link_href: "https://github.com/github/codeql" },
      ]
    end

    NO_RESULTS_MESSAGES_OPEN = [
      { heading: "No code scanning alerts here!", text: "Keep up the good work!" },
      { heading: "No code scanning alerts found.", text: "We'll keep watching out for new ones." },
      { heading: "Looking good!", text: "No new code scanning alerts." },
    ]

    def no_results_messages
      @no_results_messages ||= if !analysis_exists?
        no_analysis_message
      elsif closed?
        [{ heading: "No results matched your search.", text: "" }]
      else
        NO_RESULTS_MESSAGES_OPEN
      end
    end

    def no_analysis_message
      docs = helpers.link_to("here", "#{GitHub.help_url}/github/managing-security-vulnerabilities/about-automated-code-scanning")
      no_analysis_text = helpers.safe_join(["For more information about code scanning, see ", docs, "."])
      ref_names&.count != 1 ?
        [{ heading: "These branches haven't been scanned yet.", text: no_analysis_text }] :
        [{ heading: "This branch hasn't been scanned yet.", text: no_analysis_text }]
    end
  end
end
