# frozen_string_literal: true

module RepositoryCodeScanning
  class ResultView < View
    attr_reader :blob_map, :graphql_commit_map, :commit_map, :selected_commit_location, :workflow_run_map

    def result
      response&.data&.result
    end

    def related_locations
      @related_locations ||= response&.data&.related_locations || []
    end

    def location
      commit_location&.location
    end

    def tool_display_name
      selected_tool&.display_name
    end

    def tool_name
      result&.tool&.name
    end

    def rule_query_uri
      response&.data&.query_uri
    end

    def blob_for(location)
      blob_map[location.file_path] if location
    end

    def commit_for(commit_oid)
      commit_map[commit_oid]
    end

    def graphql_commit_for(commit_oid)
      graphql_commit_map[commit_oid]
    end

    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def commit_location
      @commit_location ||= selected_commit_location || result&.instances&.first
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

    def rich_result_message_text
      context = {
        related_locations: related_locations,
        entity: repository,
        commit_oid: commit_location&.commit_oid,
      }
      GitHub::Goomba::CodeScanningMessagePipeline.to_html(result.message_text, context)
    end

    def location_matches_result(candidate)
      location.file_path == candidate.file_path &&
        location.start_line == candidate.start_line &&
        location.end_line == candidate.end_line &&
        location.start_column == candidate.start_column &&
        location.end_column == candidate.end_column
    end

    def snippet_message_classes
      case result.rule_severity
      when :NOTE
        "border-gray-dark"
      when :WARNING
        "code-scanning-alert-warning-message"
      when :ERROR
        "border-red"
      else
        ""
      end
    end

    def snippet_helper(location, commit_oid)
      message = location_matches_result(location) ? rich_result_message_text : nil
      blob = blob_for(location)
      commit = commit_for(commit_oid)
      location_hash = {
        start_line: location.start_line,
        end_line: location.end_line,
        start_column: location.start_column,
        end_column: location.end_column,
      }
      RepositoryScanning::CodeSnippetHelper.new(blob, location_hash, commit: commit, message_text: message, message_classes: snippet_message_classes)
    end

    def code_paths_url
      urls.repository_code_scanning_code_paths_path(repository.owner, repository, number: result.number, ref: commit_location&.ref_name)
    end

    def workflow_run_for_timeline_event(timeline_event)
      workflow_run_map[timeline_event.workflow_run_id]
    end

    def rule_id
      result&.rule_id
    end
  end
end
