# frozen_string_literal: true

module Search

  class QueryHelper

    attr_reader :options

    FIELDS = []
    FIELDS.concat Search::Queries::CodeQuery::FIELDS
    FIELDS.concat Search::Queries::CommitQuery::FIELDS
    FIELDS.concat Search::Queries::DiscussionQuery::FIELDS
    FIELDS.concat Search::Queries::IssueQuery::FIELDS
    FIELDS.concat Search::Queries::RepoQuery::FIELDS
    FIELDS.concat Search::Queries::UserQuery::FIELDS
    FIELDS.concat Search::Queries::RegistryPackageQuery::FIELDS
    FIELDS.concat Search::Queries::TopicQuery::FIELDS
    FIELDS.concat Search::Queries::MarketplaceQuery::FIELDS
    FIELDS.concat Search::Queries::VulnerabilityQuery::FIELDS
    FIELDS.concat Search::Queries::WikiQuery::FIELDS
    if GitHub.enterprise?
      FIELDS.concat [:environment].freeze
    end
    FIELDS.uniq!
    FIELDS.freeze

    # Construct a QueryHelper for managing the various query types we can
    # display on the global search page.
    #
    # phrase  - The search phrase String from the user.
    # type    - The query type String.
    # options - The options Hash passed to the Query constructors.
    #
    def initialize(phrase, type, options)
      @type    = type
      @options = options

      pq = Search::ParsedQuery.new(phrase, FIELDS, options[:current_user])
      @options[:raw_phrase]  = phrase
      @options[:query]       = pq.query
      @options[:qualifiers]  = pq.qualifiers
      if GitHub::Connect.unified_search_enabled?
        @options[:environment] = pq.environment
      end
    end

    # Returns the search type as a String
    def search_type
      set = Set.new(%w[Repositories Code Commits Labels Marketplace Users Issues
        Topics Vulnerabilities Wikis])
      set << "RegistryPackages" if registry_packages_search_enabled?
      set << "Discussions" unless GitHub.enterprise?
      set.include?(@type) ? @type : default_search_type
    end

    # If a search type is not specified, try to intuit what the user wants to
    # search for by looking at the filter fields. The Query type with the
    # most matching filter fields will be chosen.
    #
    # Returns our best guess for the search type as a String.
    def default_search_type
      fields = @options[:qualifiers].keys

      repo_count = (fields & Search::Queries::RepoQuery::FIELDS).length
      issue_count = (fields & Search::Queries::IssueQuery::FIELDS).length

      # Consider valid `is:` values to distinguish between a repo search and an
      # issue search.
      if @options[:qualifiers].key?(:is) && @options[:qualifiers][:is].must?
        values = @options[:qualifiers][:is].must
        repo_count += (values & Search::Queries::RepoQuery::IS_VALUES.values.flatten).length
        issue_count += (values & Search::Queries::IssueQuery::IS_VALUES.values.flatten).length
      end

      choices = [
        ["Repositories", repo_count],
        ["Code",         (fields & Search::Queries::CodeQuery::FIELDS).length],
        ["Commits",      (fields & Search::Queries::CommitQuery::FIELDS).length],
        ["Issues",       issue_count],
        ["Users",        (fields & Search::Queries::UserQuery::FIELDS).length],
        ["RegistryPackages", (fields & Search::Queries::RegistryPackageQuery::FIELDS).length],
        ["Topics",       (fields & Search::Queries::TopicQuery::FIELDS).length],
        ["Wikis",        (fields & Search::Queries::WikiQuery::FIELDS).length],
      ]

      unless GitHub.enterprise?
        choices << ["Discussions", (fields & Search::Queries::DiscussionQuery::FIELDS).length]
      end

      choice = choices.max_by { |name, count| count }
      choice.first
    end

    # The selected query based on the given or inferred query type.
    #
    # Returns a Query.
    def current
      self[search_type]
    end

    # Lookup a Query given the key String. The allowed keys are:
    # 'Repositories', 'Code', 'Commits', 'Issues', 'Marketplace', 'Users', and 'Wikis'.
    #
    # key - The query type key as a String.
    #
    # Returns a Query or nil if the key is invalid.
    def [](key)
      case key
      when "Code"; code_query
      when "Commits"; commit_query
      when "Discussions"; discussion_query
      when "Issues"; issue_query
      when "Labels"; label_query
      when "Marketplace"; marketplace_listing_query
      when "RegistryPackages"; registry_packages_query
      when "Repositories"; repo_query
      when "Topics"; topic_query
      when "Users"; user_query
      when "Wikis"; wiki_query
      end
    end

    # Returns a CodeQuery
    def code_query
      return @code_query if defined? @code_query

      opts = @options.dup
      opts[:request_category] = "web"
      opts[:normalizer] = lambda { |results| Search::CodeResultView.build_batch(results) }
      opts[:dotcom_normalizer] = lambda { |results| Search::DotcomCodeResultView.build_batch(results) }

      @code_query = if opts[:geyser_enabled]
        tags = ["query_scope:unscoped"]
        if opts[:repo_id].present?
          tags = ["query_scope:repository"]
        end
        GitHub.dogstats.increment "search.geyser_dark_ship.query", tags: tags if opts[:dark_ship]
        Search::Queries::GeyserCodeQuery.new(opts)
      else
        dup_qualifiers(opts)
        Search::Queries::CodeQuery.new(opts)
      end
    end

    # Returns a CommitQuery
    def commit_query
      opts = @options.dup
      opts[:dotcom_normalizer] = lambda { |results| results.map! { |h| Search::DotcomCommitResultView.new(h) } }
      dup_qualifiers(opts)

      Search::Queries::CommitQuery.new(opts)
    end

    # Returns a DiscussionQuery
    def discussion_query
      return @discussion_query if defined? @discussion_query

      opts = @options.dup
      opts[:normalizer] = lambda { |results| results.map! { |h| Search::DiscussionResultView.new(h) } }
      dup_qualifiers(opts)

      @discussion_query = Search::Queries::DiscussionQuery.new(opts)
    end

    # Returns an IssueQuery
    def issue_query
      return @issue_query if defined? @issue_query

      opts = @options.dup
      opts[:normalizer] = lambda { |results| results.map! { |h| Search::IssueResultView.new(h) } }
      opts[:dotcom_normalizer] = lambda { |results| results.map! { |h| Search::DotcomIssueResultView.new(h) } }
      dup_qualifiers(opts)

      @issue_query = Search::Queries::IssueQuery.new(opts)
    end

    # Returns an LabelQuery
    def label_query
      return @label_query if defined? @label_query

      opts = @options.dup
      opts[:normalizer] = lambda { |results| results.map! { |h| Search::LabelResultView.new(h) } }
      dup_qualifiers(opts)

      @label_query = Search::Queries::LabelQuery.new(opts)
    end

    # Returns a MarketplaceQuery
    def marketplace_listing_query
      return @marketplace_listing_query if defined? @marketplace_listing_query

      opts = @options.dup
      opts[:type] = "marketplace-tools"
      opts[:normalizer] = lambda do |results|
        results.map! do |h|
          case h["_type"]
          when "non_marketplace_listing"
            Search::NonMarketplaceListingResultView.new(h)
          when "marketplace_listing"
            Search::MarketplaceListingResultView.new(h)
          when "repository_action"
            Search::RepositoryActionResultView.new(h)
          when "github_app"
            Search::GitHubAppResultView.new(h)
          end
        end
      end
      dup_qualifiers(opts)

      @marketplace_listing_query = Search::Queries::MarketplaceQuery.new(opts)
    end

    # Returns a RegistryPackageQuery
    def registry_packages_query
      return @registry_packages_query if defined? @registry_packages_query

      opts = @options.dup
      opts[:normalizer] = lambda { |results| results.map! { |h| Search::RegistryPackageResultView.new(h) } }
      dup_qualifiers(opts)

      @registry_packages_query = Search::Queries::RegistryPackageQuery.new(opts)
    end

    # Returns a RepoQuery
    def repo_query
      return @repo_query if defined? @repo_query

      opts = @options.dup
      opts[:normalizer] = lambda { |results| results.map! { |h| Search::RepoResultView.new(h) } }
      opts[:dotcom_normalizer] = lambda { |results| results.map! { |h| Search::DotcomRepoResultView.new(h) } }
      dup_qualifiers(opts)

      @repo_query = Search::Queries::RepoQuery.new(opts)
    end

    # Returns a UserQuery
    def user_query
      return @user_query if defined? @user_query

      opts = @options.dup
      opts[:normalizer] = lambda { |results| results.map! { |h| Search::UserResultView.new(h) } }
      opts[:dotcom_normalizer] = lambda { |results| results.map! { |h| Search::DotcomUserResultView.new(h) } }
      dup_qualifiers(opts)

      @user_query = Search::Queries::UserQuery.new(opts)
    end

    # Returns a TopicQuery
    def topic_query
      return @topic_query if defined? @topic_query

      opts = @options.dup
      opts[:normalizer] = lambda { |results| results.map! { |h| Search::TopicResultView.new(h) } }
      opts[:dotcom_normalizer] = lambda { |results| results.map! { |h| Search::DotcomTopicResultView.new(h) } }
      dup_qualifiers(opts)

      @topic_query = Search::Queries::TopicQuery.new(opts)
    end

    # Returns a WikiQuery
    def wiki_query
      return @wiki_query if defined? @wiki_query

      opts = @options.dup
      opts[:normalizer] = lambda { |results| results.map! { |h| Search::WikiResultView.new(h) } }
      dup_qualifiers(opts)

      @wiki_query = Search::Queries::WikiQuery.new(opts)
    end

    # Returns a VulnerabilityQuery
    def vulnerability_query
      return @vulnerability_query if defined? @vulnerability_query

      opts = @options.dup
      dup_qualifiers(opts)

      @vulnerability_query = Search::Queries::VulnerabilityQuery.new(opts)
    end

    # Replace the :qualifiers with a duplicate so we don't cross-contaminate queries
    def dup_qualifiers(options)
      return unless options.key? :qualifiers

      qualifiers = options[:qualifiers].dup
      qualifiers.each { |k, v| qualifiers[k] = v.dup }
      options[:qualifiers] = qualifiers
    end

    def registry_packages_search_enabled?
      GitHub.package_registry_enabled?
    end
  end
end  # Search
