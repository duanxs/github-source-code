# frozen_string_literal: true

module Search
  class RepoResultView
    include EscapeHelper

    attr_reader :id, :name, :description, :size, :forks, :followers, :template,
                :language, :pushed, :fork, :public, :repo, :mirror, :archived,
                :help_wanted_issues_count, :good_first_issue_issues_count
    alias :public? :public
    alias :pushed_at :pushed
    alias :mirror? :mirror
    alias :archived? :archived
    alias :template? :template

    # Create a new RepoResultView from a `repository` document hash returned from
    # the ElasticSearch index.
    #
    # hash - Document Hash returned by ElasticSearch
    #
    def initialize(hash)
      raise "Incorrect result type: #{hash['_type'].inspect}" unless "repository" == hash["_type"]

      source = hash["_source"]

      @id          = hash["_id"]
      @repo        = hash["_model"]
      @name        = source["name"]
      @description = source["description"]
      @size        = source["size"].to_i
      @forks       = source["forks"].to_i
      @followers   = source["followers"].to_i
      @language    = Search.language_name_from_id(source["language_id"])
      @fork        = source["fork"]
      @public      = source["public"]
      @pushed      = Time.parse(source["pushed_at"])
      @mirror      = source["mirror"]
      @template    = source["template"]
      @archived    = source["archived"]
      @highlights  = hash["highlight"]
      @license_id  = source["license_id"]
      @help_wanted_issues_count = source["help_wanted_issues_count"].to_i
      @good_first_issue_issues_count = source["good_first_issue_issues_count"].to_i

      @topics = if source["ranked_hashtags"]
        source["ranked_hashtags"].map { |h| h["applied"] }.compact
      end
      @suggested_topics = if source["ranked_hashtags"]
        source["ranked_hashtags"].map { |h| h["suggested"] }.compact
      end
    end

    def good_first_issue_label
      repo.good_first_issue_label
    end

    def help_wanted_label
      repo.help_wanted_label
    end

    def private?
      !public?
    end

    def visibility
      repo.visibility
    end

    def type
      RepositoriesTypeHelper.type(visibility: visibility, mirror: mirror?, archived: archived?,
                                  template: template?)
    end

    # Returns the parent Repository or nil
    def parent
      repo.parent
    end

    # Public: Returns true if we should show the license this repository uses.
    def show_license?
      license && !license.other?
    end

    # Public: Should 'X issues need help' link be shown for the repository.
    #
    # Returns a Boolean.
    def show_issues_needing_help_link?
      repo.has_issues?
    end

    # Public: Returns the name of the License on this Repository or nil.
    def license_name
      license.try(:spdx_id)
    end

    # Returns true if there are highlight fragments for this repository. The
    # highlight fragments contain text from the various repository fields with the
    # relevant search terms surrounded by <em> tags.
    def highlights?
      !@highlights.nil?
    end

    # Return the repo description. The description may or may not contain
    # highlight tags, but either way it has been HTML escaped and is html_safe.
    #
    # Returns an HTML escaped description String.
    def hl_description
      return @hl_description if defined? @hl_description
      @hl_description =
          if highlights? && @highlights.key?("description")
            GitHub::HTML::HighlightedSearchResultPipeline.to_html(@highlights["description"].first)
          else
            formatted = GitHub::HTML::DescriptionPipeline.to_html(@description.to_s)
            HTMLTruncator.new(formatted, 350).to_html(wrap: false)
          end
    end

    # Returns the repo description with emoji but without any links. Good for use
    # nesting the description inside of a link tag, like on mobile.
    #
    # Returns an HTML escaped description String.
    def linkless_description
      formatted = GitHub::Goomba::SimpleDescriptionPipeline.to_html(@description.to_s)
      HTMLTruncator.new(formatted, 350).to_html(wrap: false)
    end

    # Public: Returns true if the repository is owned by an organization.
    def owned_by_organization?
      @repo.owner && @repo.owner.organization?
    end

    # Returns the `login` name of the owner of the current repository.
    def owner
      @repo.owner.login if @repo.owner
    end

    # Returns true if the repository is a fork with a valid parent repo.
    def forked?
      fork && parent.present?
    end

    # Returns the name_with_owner String for the repository. It may or may not
    # contain highlight tags, but either way it has been HTML escaped and is
    # html_safe.
    #
    # Returns an HTML escaped name_with_owner String.
    def name_with_owner
      if highlights? && @highlights.key?("name_with_owner")
        return @highlights["name_with_owner"].first
      end

      hl_name =
          if highlights?
            if @highlights.key?("name")
              @highlights["name"].first
            elsif @highlights.key?("name.camel")
              @highlights["name.camel"].first
            elsif @highlights.key?("name.ngram")
              @highlights["name.ngram"].first
            end
          end
      hl_name = EscapeUtils.escape_html_as_html_safe(@name) if hl_name.nil?
      safe_join([owner, hl_name], "/")
    end

    # Returns the list of suggested topics for this repository.
    #
    # Returns an Array of Strings, empty if none exist.
    def suggested_topics
      @suggested_topics.present? ? @suggested_topics : []
    end

    # Returns the list of topics that have been applied to this repository.
    #
    # Returns an Array of Strings, empty if none exist.
    def topics
      @topics.present? ? @topics : []
    end

    def platform_type_name
      "Repository"
    end

    private

    # Private: Returns a License or nil.
    def license
      return @license if defined? @license

      @license = if @license_id
        License.find_by_id(@license_id.to_i)
      end
    end
  end  # RepoResultView
end  # Search
