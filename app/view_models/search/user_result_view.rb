# frozen_string_literal: true

module Search
  class UserResultView
    include ActionView::Helpers::TagHelper
    include ActionView::Helpers::UrlHelper
    include ApplicationHelper
    include UsersHelper

    attr_reader :id, :login, :name, :location, :repos, :followers, :language, :user

    # Create a new UserResultView from a `user` document hash returned from
    # the ElasticSearch index.
    #
    # hash - Document Hash returned by ElasticSearch
    #
    def initialize(hash)
      doctype = hash["_type"]

      unless "user" == doctype || "organization" == doctype
        raise "Incorrect result type: #{doctype.inspect}"
      end

      @id   = hash["_id"]
      @user = hash["_model"]

      source = hash["_source"]

      @login     = source["login"]
      @name      = source["name"]
      @location  = source["location"]
      @repos     = source["repos"]
      @followers = source["followers"]
      @language  = Search.language_name_from_id(source["language_id"])
      @profile_bio = source["profile_bio"]

      @highlights = hash["highlight"]
    end

    # Returns the user's gravatar email as a String.
    def gravatar_email
      user.gravatar_email
    end

    # Returns the user's profile email, if any, taking into account the user's
    # email privacy preferences.
    #
    # logged_in - is the current viewing user logged in? Users are required to
    #             log in to see another user's email, unless in Enterprise
    def email(logged_in: false)
      user.publicly_visible_email(logged_in: logged_in)
    end

    def profile_bio
      return unless @user.profile
      @user.profile.bio
    end

    # Returns true if there are highlight fragments for this user. The
    # highlight fragments contain text from the various user fields with the
    # relevant search terms surrounded by <em> tags.
    def highlights?
      !@highlights.nil?
    end

    # Return the user login. The login may or may not contain highlight tags,
    # but either way it has been HTML escaped and is html_safe.
    #
    # Returns an HTML escaped login String.
    def hl_login
      return @hl_login if defined? @hl_login
      @hl_login =
          if highlights? && (@highlights.key?("login") || @highlights.key?("login.ngram"))
            (@highlights["login"] || @highlights["login.ngram"]).first
          else
            EscapeUtils.escape_html_as_html_safe(@login)
          end
    end

    # Return the user name. The name may or may not contain highlight tags,
    # but either way it has been HTML escaped and is html_safe.
    #
    # Returns an HTML escaped name String.
    def hl_name
      return @hl_name if defined? @hl_name

      @hl_name =
          if highlights? && @highlights.key?("name")
            @highlights["name"].first
          else
            EscapeUtils.escape_html_as_html_safe(@name.to_s)
          end
    end

    # Return the profile bio.
    #
    # Returns a String.
    def hl_profile_bio
      if highlights? && @highlights.key?("profile_bio")
        @highlights["profile_bio"].first
      else
        @profile_bio
      end
    end
  end  # UserResultView
end  # Search
