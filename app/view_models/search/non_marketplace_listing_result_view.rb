# frozen_string_literal: true

module Search
  class NonMarketplaceListingResultView
    include EscapeHelper

    attr_reader :id, :name, :description, :created, :updated, :url, :category, :state,
                :non_marketplace_listing
    alias :created_at :created
    alias :updated_at :updated

    # Create a new NonMarketplaceListingResultView from a `non_marketplace_listing` document hash
    # returned from the ElasticSearch index.
    #
    # hash - Document Hash returned by ElasticSearch
    #
    def initialize(hash)
      unless "non_marketplace_listing" == hash["_type"]
        raise "Incorrect result type: #{hash['_type'].inspect}"
      end

      source = hash["_source"]

      @id = hash["_id"]
      @non_marketplace_listing = hash["_model"]
      @name = source["name"]
      @description = source["description"]
      @url = source["url"]
      @category = source["category"]
      @state = source["state"]
      @created = Time.parse(source["created_at"])
      @updated = Time.parse(source["updated_at"])
      @highlights = hash["highlight"]
    end

    def logo_url(size: 400)
      if non_marketplace_listing.primary_avatar
        non_marketplace_listing.primary_avatar_url(size)
      elsif non_marketplace_listing.integratable
        non_marketplace_listing.integratable.preferred_avatar_url(size: size)
      else
        non_marketplace_listing.creator.primary_avatar_url(size)
      end
    end

    # Returns true if there are highlight fragments for this listing. The
    # highlight fragments contain text from the various listing fields with the
    # relevant search terms surrounded by <em> tags.
    def highlights?
      !@highlights.nil?
    end

    # Return the listing description. The description may or may not contain
    # highlight tags, but either way it has been HTML escaped and is html_safe.
    #
    # Returns an HTML escaped description String.
    def hl_description
      @hl_description ||=
        if highlights? && @highlights.key?("description")
          GitHub::HTML::HighlightedSearchResultPipeline.to_html(@highlights["description"].first)
        else
          formatted = GitHub::HTML::DescriptionPipeline.to_html(@description.to_s)
          HTMLTruncator.new(formatted, 350).to_html(wrap: false)
        end
    end

    # Returns the listing description with emoji but without any links. Good for use
    # nesting the description inside of a link tag, like on mobile.
    #
    # Returns an HTML escaped description String.
    def linkless_description
      formatted = GitHub::Goomba::SimpleDescriptionPipeline.to_html(@description.to_s)
      HTMLTruncator.new(formatted, 350).to_html(wrap: false)
    end
  end  # NonMarketplaceListingResultView
end  # Search
