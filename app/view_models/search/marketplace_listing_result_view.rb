# frozen_string_literal: true

module Search
  class MarketplaceListingResultView
    include EscapeHelper

    attr_reader :id, :name, :full_description, :created, :updated, :primary_category, :state,
                :marketplace_listing, :secondary_category, :short_description,
                :extended_description, :free, :installation_count
    alias :created_at :created
    alias :updated_at :updated

    # Create a new MarketplaceListingResultView from a `marketplace_listing` document hash
    # returned from the ElasticSearch index.
    #
    # hash - Document Hash returned by ElasticSearch
    #
    def initialize(hash)
      unless "marketplace_listing" == hash["_type"]
        raise "Incorrect result type: #{hash['_type'].inspect}"
      end

      source = hash["_source"]

      @id = hash["_id"]
      @marketplace_listing = hash["_model"]
      @name = source["name"]
      @full_description = source["full_description"]
      @short_description = source["short_description"]
      @extended_description = source["extended_description"]
      @free = source["free"]
      @primary_category = source["primary_category"]
      @secondary_category = source["secondary_category"]
      @state = source["state"]
      @created = Time.parse(source["created_at"])
      @updated = Time.parse(source["updated_at"])
      @highlights = hash["highlight"]
      @installation_count = source["installation_count"]
    end

    def free?
      free
    end

    def logo_url(size: 400)
      marketplace_listing.primary_avatar_url(size)
    end

    def tool_type
      "marketplace_listing"
    end

    # Returns true if there are highlight fragments for this listing. The
    # highlight fragments contain text from the various listing fields with the
    # relevant search terms surrounded by <em> tags.
    def highlights?
      !@highlights.nil?
    end

    # Return the listing name. The name may or may not contain
    # highlight tags, but either way it has been HTML escaped and is html_safe.
    #
    # Returns an HTML escaped name String.
    def hl_name
      @hl_name ||= hl_description("name.ngram", @name)
    end

    # Return the listing short_description. The short_description may or may not contain
    # highlight tags, but either way it has been HTML escaped and is html_safe.
    #
    # Returns an HTML escaped short_description String.
    def hl_short_description
      @hl_short_description ||= hl_description("short_description", @short_description)
    end

    # Return the listing full_description. The full_description may or may not contain
    # highlight tags, but either way it has been HTML escaped and is html_safe.
    #
    # Returns an HTML escaped full_description String.
    def hl_full_description
      @hl_full_description ||= hl_description("full_description", @full_description)
    end

    # Return the listing extended_description. The extended_description may or may not contain
    # highlight tags, but either way it has been HTML escaped and is html_safe.
    #
    # Returns an HTML escaped extended_description String.
    def hl_extended_description
      @hl_extended_description ||= hl_description("extended_description", @extended_description)
    end

    private

    def hl_description(key, value)
      if highlights? && @highlights.key?(key)
        GitHub::HTML::HighlightedSearchResultPipeline.to_html(@highlights[key].first)
      else
        formatted = GitHub::HTML::DescriptionPipeline.to_html(value.to_s)
        HTMLTruncator.new(formatted, 350).to_html(wrap: false)
      end
    end
  end  # MarketplaceListingResultView
end  # Search
