# frozen_string_literal: true
module Search
  class RepositoryActionResultView
    attr_reader :id, :name, :description, :created, :updated, :repository_action,
      :primary_category, :secondary_category, :categories

    # Create a new RepositoryActionResultView from a `repository_action` document hash
    # returned from the ElasticSearch index.
    #
    # hash - Document Hash returned by ElasticSearch
    #
    def initialize(hash)
      unless "repository_action" == hash["_type"]
        raise "Incorrect result type: #{hash['_type'].inspect}"
      end

      source = hash["_source"]
      @id = hash["_id"]
      @repository_action = hash["_model"]
      @name = source["name"]
      @description = source["description"]
      @highlights = hash["highlight"]
      @primary_category = source["primary_category"]
      @secondary_category = source["secondary_category"]
      @categories = source["categories"]
    end

    def tool_type
      "repository_action"
    end

    # Returns true if there are highlight fragments for this action. The
    # highlight fragments contain text from the various action fields with the
    # relevant search terms surrounded by <em> tags.
    def highlights?
      !@highlights.nil?
    end

    # Return the action name. The name may or may not contain
    # highlight tags, but either way it has been HTML escaped and is html_safe.
    #
    # Returns an HTML escaped name String.
    def hl_name
      @hl_name ||= hl_description("name.ngram", @name)
    end

    # Return the action short_description. The short_description may or may not contain
    # highlight tags, but either way it has been HTML escaped and is html_safe.
    #
    # Returns an HTML escaped short_description String.
    def hl_short_description
      @hl_description ||= hl_description("short_description", @description)
    end

    private

    def hl_description(key, value)
      if highlights? && @highlights.key?(key)
        GitHub::HTML::HighlightedSearchResultPipeline.to_html(@highlights[key].first)
      else
        formatted = GitHub::HTML::DescriptionPipeline.to_html(value.to_s)
        HTMLTruncator.new(formatted, 350).to_html(wrap: false)
      end
    end
  end
end
