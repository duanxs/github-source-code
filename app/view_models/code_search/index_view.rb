# frozen_string_literal: true

module CodeSearch
  class IndexView
    # Returns a random plural model name ("Users", "Issues", "Repositories")
    # and the total number of records for that model.
    #
    # Used to tell our users about the EMPHATICALLY VOLUMINOUS
    # amount of data that they're about to search.
    attr_reader :model_name, :model_millions

    def initialize
      @model_millions, @model_name = [
        [GitHub::MarketingStats.repository_millions, "repositories"],
        [GitHub::MarketingStats.user_millions,       "users"],
        [GitHub::MarketingStats.issue_millions,      "issues"],
      ].sample
    end
  end
end
