# frozen_string_literal: true

module RepositoryTokenScanning
  class MailerView < IndexView
    MAX_RESULTS_PER_TYPE = 5

    attr_reader :results

    # Group results by token type, this handles both the case where jobs
    # pass an array of TokenScanResult objects and the service provides
    # an of token group hashes.
    def token_groups
      if results.first.is_a?(Hash)
        results.map do |group|
          type_results = group[:tokens].map { |token| Token.new(token) }
          [group[:type], type_results, group[:count] - type_results.size]
        end
      else
        results.group_by(&:token_type).map do |token_type, type_results|
          [token_type, type_results.take(MAX_RESULTS_PER_TYPE), type_results.size - MAX_RESULTS_PER_TYPE]
        end
      end
    end

    class Token
      attr_reader :id, :first_location

      def initialize(token)
        @id = token[:id]
        @first_location = TokenLocation.new(token[:first_location])
      end

      def found_in_archive?
        false
      end
    end

    class TokenLocation
      attr_reader :commit_oid, :path, :start_line

      def initialize(location)
        @commit_oid = location[:commit_oid]
        @path = location[:path]
        @start_line = location[:start_line]
      end
    end
  end
end
