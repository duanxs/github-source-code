# frozen_string_literal: true

module Actions
  class WorkflowRunItemView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :workflow_run

    delegate :number, to: :pull_request, prefix: true, allow_nil: true

    def trigger
      @_trigger ||= workflow_run.trigger
    end

    def pull_request
      if trigger.is_a? PullRequest
        @_pull_request = trigger
      else
        @_pull_request ||= repository.pull_requests.find_by(
          head_sha: head_sha,
          head_ref: head_branch,
          head_repository_id: head_repository_id,
        )
      end

      @_pull_request = nil if @_pull_request&.user&.spammy?

      @_pull_request
    end

    def workflow_actor
      creator || pusher
    end

    def show_options_menu?
      return true if pull_request.present? || workflow_run.workflow_file_path.present?

      workflow_run.check_suite.cancelable? && repository.writable_by?(current_user)
    end

    def branch_name_relevant?
      ["push", "pull_request"].include? workflow_run.event
    end

    private

    delegate :head_sha, :name, :repository, :head_branch, to: :workflow_run

    def pusher
      workflow_run.check_suite.pusher
    end

    def head_repository_id
      workflow_run.check_suite.head_repository_id
    end

    def creator
      workflow_run.check_suite.creator
    end

    def in_lab
      github_app_id == GitHub.launch_lab_github_app&.id
    end
  end
end
