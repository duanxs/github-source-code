# frozen_string_literal: true

module Actions
  class SelfHostedRunnerView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :runner, :can_manage_runners, :owner_settings

    delegate :name, :offline?, :system_labels, :custom_labels, to: :runner
    delegate :os, :status, :custom_label_ids, to: :runner, private: true
    delegate :runners_path, :settings_owner, :show_labels?, to: :owner_settings

    def readable_status
      status.titlecase
    end

    def readable_os
      case os
        when "macos" then "macOS"
        when "unknown" then "" # don't show anything. Should only occur with staff-shipped old yaml syntax
        else os.titleize
      end
    end

    def icon_color_class
      case status
        when "idle" then "text-green"
        when "active" then "text-yellow"
        else "text-gray-light"
      end
    end

    def runner_id
      runner.id
    end

    def delete_path
      owner_settings.delete_runner_path(id: runner_id, os: os)
    end

    def labels_path
      owner_settings.labels_path(runner_id: runner_id, selected_labels: custom_label_ids)
    end
  end
end
