# frozen_string_literal: true

module Actions
  class StarterWorkflowsView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :category_filter
    attr_reader :repository

    # Change these IDs to adjust what is shown in the popular section
    POPULAR_TEMPLATE_IDS = ["nodejs", "golang", "rust", "pythonpackage", "ruby", "dockerimage"]
    PARTNER_WORKFLOW_TEMPLATE_IDS = ["azure", "aws", "google", "terraform", "tencent", "ibm"]
    NUM_TEMPLATES_BEFORE_EXPANSION = 6

    def primary_language
      repository.primary_language&.name
    end

    def show_automation_section?
      automation_templates.any?
    end

    def show_filter?
      category_filter.present?
    end

    def suggested_templates
      return @suggested_templates if @suggested_templates

      templates = all_templates.map do |template|
        template.add_weight(2)   if template.matches_paths?(paths_to_match)
        template.add_weight(1)   if template.matches_language?(primary_language)

        template if template.weight > 0 && !template.partner
      end.compact

      return [blank_template] unless templates.length > 0

      # Give owner templates some extra weight
      templates.each { |t| t.add_weight(1.5) if t.from_owner? }

      @suggested_templates = templates.sort_by(&:weight).reverse.first(4)
    end

    def filtered_templates(category)
      @filtered_templates = template_data.all.map do |template|
        # templates that show up in the filter view should not show a filter option again.
        template["show_filter"] = false
        ::RepositoryActions::Onboarding::Template.new(template)
      end

      @filtered_templates.reject! do |template|
        template.language != category
      end

      @filtered_templates.shuffle
    end

    def automation_templates
      return @automation_templates if @automation_templates

      @automation_templates = all_templates.select do |template|
        template.categories && template.categories.include?("Automation")
      end
    end

    def partner_templates
      return @partner_templates if @partner_templates

      @partner_templates = all_templates.select do |template|
        PARTNER_WORKFLOW_TEMPLATE_IDS.include? template.id
      end

      azure_template = @partner_templates.find { |t| t.id == "azure" }

      if azure_template
        # Move azure to first spot to test if it increases usage
        @partner_templates.reject! { |t| t.id == azure_template.id }
        @partner_templates = [azure_template] + @partner_templates
      else
        @partner_templates
      end
    end

    def owner_templates
      return @owner_templates if defined?(@owner_templates)

      @owner_templates = all_templates.select do |template|
        template.from_owner? && !suggested_templates.include?(template)
      end
    end

    def show_owner_templates?
      owner_templates.any?
    end

    def ci_templates
      return @ci_templates if @ci_templates

      templates = all_templates

      templates = templates - automation_templates
      templates = templates - suggested_templates
      templates = templates - partner_templates
      templates = templates - owner_templates

      # Put the popular ones first
      popular, other = templates.partition { |t| POPULAR_TEMPLATE_IDS.include? t.id }

      @ci_templates = popular.shuffle + other.shuffle
    end

    private

    def template_data
      @template_data ||= Actions::WorkflowTemplates.new(repository, current_user)
    end

    def all_templates
      return @all_templates if defined?(@all_templates)
      @all_templates = map_json_to_template(template_data.all)
    end

    def blank_template
      all_templates.detect { |template| template.id == "blank" }
    end

    def map_json_to_template(templates)
      templates.map do |template|
        primary_category = template["categories"]&.first
        template["show_filter"] = primary_category && multiple_templates?(primary_category)
        template["partner"] = PARTNER_WORKFLOW_TEMPLATE_IDS.include? template["id"]
        ::RepositoryActions::Onboarding::Template.new(template)
      end.compact
    end

    def multiple_templates?(category)
      @multiple_templates ||= Hash.new do |hash, key|
        hash[key] = key && filtered_templates(key).count > 1
      end

      @multiple_templates[category]
    end

    def paths_to_match
      return [] if !repository.default_oid

      _, tree_entries, _  = repository.tree_entries(repository.default_oid, "")
      tree_entries.map(&:path)
    end
  end
end
