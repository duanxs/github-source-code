# frozen_string_literal: true

class Actions::RepoRunnersView < Actions::RunnersView
  def show_labels?
    true
  end

  def can_manage_runners?
    !unverified_or_spammy_user?
  end

  def add_runner_path
    urls.repository_actions_settings_add_runner_path(repo_params)
  end

  def delete_runner_path(id:, os:)
    urls.repository_actions_settings_delete_runner_modal_path(
      repo_params.merge(id: id, os: os)
    )
  end

  def runners_path
    urls.repository_actions_settings_list_runners_path(repo_params)
  end

  def labels_path(runner_id:, selected_labels:)
    urls.repo_runner_labels_path(repo_params.merge(runner_id: runner_id, applied_labels: selected_labels))
  end

  def bulk_actions_path
    urls.repo_runner_bulk_actions_path(repo_params)
  end

  private

  def unverified_or_spammy_user?
    current_user.spammy? ||
      settings_owner.owner.spammy? ||
      current_user.should_verify_email?
  end

  def repo_params
    {
      user_id: settings_owner.owner_login,
      repository: settings_owner,
    }
  end
end
