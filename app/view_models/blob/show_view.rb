# frozen_string_literal: true

class Blob::ShowView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  include RenderHelper
  include CodeNavigationHelper
  attr_reader :repo, :tree_name, :commit, :blob, :path, :path_for_display, :short_path, :issue_template, :collaborative_editing_is_enabled,
    :valid_legacy_template, :code_nav

  # What kind of page title should we show? I guess some blobs don't have
  # names (but why?).
  #
  # Returns a String.
  def page_title
    if blob.name.blank?
      "#{repo.name_with_owner} at #{h tree_name_for_display}"
    else
      "#{repo.name}/#{blob.display_name} at #{tree_name_for_display} · #{repo.name_with_owner}"
    end
  end

  # The last commit to touch the blob.
  def last_commit
    return @last_commit if defined?(@last_commit)

    last_contribution = contributors.first
    @last_commit      = last_contribution && @repo.commits.find(last_contribution["commit"])
  end

  # How many people have contributed to this file? We're cheating here
  # because sometimes a User will commit with multiple email addresses. So
  # we use the User count if we're showing them all, otherwise we use the
  # email count.
  #
  # Returns an Integer of contributors.
  def contributor_count
    contributor_users.size > 26 ? contributor_emails.size : contributor_users.size
  end

  # How many contributors that aren't Users?
  #
  # Returns an Integer.
  def other_contributors_count
    contributor_count - contributor_users.size
  end

  # Users attributed to blob_contributors' emails, sorted by number of
  # contributions.
  #
  # Returns an Array of Users.
  def contributor_users
    @contributor_users ||= contributor_users!
  end

  def contributor_users!
    commit_count_by_email = Hash[contributors.map { |c| [c["author"].downcase, c["count"]] }]
    users = {}

    User.find_by_emails(contributor_emails).each do |email, user|
      # It's possible that we can get a user with an email address
      # that does not match any of the email addresses from GitRPC
      # due MySQL collation matching and returning users for email
      # addresses that are slightly different than the address we
      # queried for.
      #
      # For example, querying for
      #   aurélien.alriquet@devinci.fr
      # can return a user with an email address of
      #   aurelien.alriquet@devinci.fr
      #
      # but the strings will not match in Ruby.
      #
      # I think these are actually different email addresses
      # in practice, so this kind of "fuzzy" match probably
      # shouldn't result in the user being displayed as a
      # blob contributor, even though they may very well
      # be the same person.
      #
      if user && (count = commit_count_by_email[email.downcase])
        users[user] ||= 0
        users[user] += count
      end
    end

    users.keys.sort_by { |user| users[user] }.reverse
  end

  def contributor_emails
    @contributor_emails ||= contributors.map { |c| c["author"] }
  end

  # Did backend time out and gave partial contributor data?
  def contributors_is_truncated?
    raw_contributors["truncated"]
  end

  # The tooltip we present to users when the edit action is displayed.
  #
  # action - operation to construct a tooltip for (either :edit, or :delete).
  #
  # Returns a String
  def file_action_tooltip(action:)
    raise(ArgumentError, "action must be :edit or :delete") unless [:edit, :delete].include?(action)

    if edit_enabled?
      if can_push?
        "#{action.to_s.capitalize} this file"
      elsif has_fork?
        "#{action.to_s.capitalize} the file in your fork of this project"
      elsif can_fork?
        "Fork this project and #{action} the file"
      end
    elsif logged_in?
      if !branch?
        "You must be on a branch to make or propose changes to this file"
      elsif current_user.must_verify_email?
        "You need to verify your email address to propose changes"
      else
        "You must be able to fork a repository to propose changes"
      end
    else
      "You must be signed in to make or propose changes"
    end
  end

  # Whether this blob refers to a branch.
  #
  # Returns a Boolean.
  def branch?
    repo.heads.exist?(tree_name)
  end

  # Whether this blob is on the default branch.
  #
  # Returns a Boolean.
  def default_branch?
    tree_name == repo.default_branch
  end

  # Should the edit action be displayed in an enabled state?
  def edit_enabled?
    return false unless logged_in? && branch?
    return false if current_user.must_verify_email?
    can_push? || has_fork? || can_fork?
  end

  def can_push?
    return false unless logged_in?

    repo.pushable_by?(current_user, ref: branch? ? tree_name : nil)
  end

  def has_fork?
    return false unless logged_in?
    return false if repo.archived? && current_user == repo.owner
    repo.network_has_fork_for?(current_user)
  end

  def can_fork?
    logged_in? && current_user.can_fork?(repo)
  end

  # Should the blob appear "rendered"? i.e. Using render to display .geojson file
  # as visual map. Even if the blob *can* be rendered, the user may have selected
  # the "source" view, in which case we should *not* `use_render?`.
  def use_render?
    if blob.render_file_type_for_display(:view)
      !source_selected?
    else
      false
    end
  end

  # Determine the current state of the render toggle.
  def toggle_state
    short_path == blob_short_path(blob) ? :source : :rendered
  end

  # Is the "source" option of the render toggle selected?
  def source_selected?
    toggle_state == :source
  end

  # Is the "rendered" option of the render toggle selected?
  def rendered_selected?
    toggle_state == :rendered
  end

  def show_license_meta?
    return false if repo.license.nil? || repo.license.pseudo_license?
    preferred_license = repo.preferred_license
    preferred_license && preferred_license.path == path
  end

  def overriding_global_funding_file?
    path.downcase.include?(FundingLinks::FILENAME.downcase) && repo.overriding_global_funding_file?
  end

  def show_dependabot_configuration_banner?
    return false unless repo.automated_dependency_updates_visible_to?(current_user)

    dependabot_config_file?
  end

  def dependabot_config_file_path
    path if dependabot_config_file?
  end

  def dependabot_config_file?
    return @dependabot_config_file if defined? @dependabot_config_file
    @dependabot_config_file = ::Dependabot.recognized_config_path?(path: path.downcase)
  end

  def show_publish_action_banner?
    return false unless logged_in?
    return false if GitHub.enterprise?
    return false if current_user.dismissed_notice?(UserNotice::PUBLISH_ACTION_FROM_DOCKERFILE_NOTICE)
    return false unless repo.pushable_by?(current_user)
    return false unless repo.listable_action?
    return false if repo.listed_action
    return false unless path == repo.action_at_root&.path

    true
  end

  private

  # The authors who have modified the current blob
  #
  # Returns an array of hashes, one per author who touched the blob.
  # Each hash has 'author' email, last 'commit' oid by that author, and
  # 'count' of total commits by that author
  def contributors
    raw_contributors["data"]
  end

  def raw_contributors
    @raw_contributors ||= repo.rpc.read_blob_contributors(
      commit.oid,
      blob.path,
      co_authors: true, # TODO: remove this option; coauthors are always there now
    )
  end
end
