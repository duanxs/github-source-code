# frozen_string_literal: true

module Commits
  # object that wraps a GitSigning::Verifiable to look like a GraphQL result
  class SignedCommitBadge
    def self.for(verifiable, current_user:)
      if verifiable.has_signature?
        new(verifiable, current_user: current_user)
      else
        nil
      end
    end

    def initialize(verifiable, current_user:)
      @verifiable = verifiable
      @current_user = current_user
    end

    def is_valid?
      @verifiable.verified_signature?
    end

    def signed_by_github?
      @verifiable.signed_by_github?
    end

    class Signer
      def initialize(user, current_user:)
        @user = user
        @current_user = current_user
      end

      def login
        @user.login
      end

      def name
        @user.profile_name
      end

      def avatar_url
        @user.primary_avatar_url(64)
      end

      def is_viewer?
        @user == @current_user
      end
    end

    def signer
      if @verifiable.signer
        Signer.new(@verifiable.signer, current_user: @current_user)
      end
    end

    def typename
      if @verifiable.gpg_signature?
        "GpgSignature"
      elsif @verifiable.smime_signature?
        "SmimeSignature"
      end
    end

    def key_id
      @verifiable.signature_issuer_key_id_hex
    end

    def state
      Platform::Enums::GitSignatureState.coerce_isolated_result(@verifiable.signature_verification_reason)
    end

    class CertificateAttributes
      def initialize(certificate_attributes)
        @certificate_attributes = certificate_attributes || {}
      end

      def common_name
        @certificate_attributes["CN"]
      end

      def email_address
        @certificate_attributes["emailAddress"]
      end

      def organization
        @certificate_attributes["O"]
      end

      def organization_unit
        @certificate_attributes["OU"]
      end
    end

    def subject
      CertificateAttributes.new(@verifiable.signature_certificate_subject)
    end

    def issuer
      CertificateAttributes.new(@verifiable.signature_certificate_issuer)
    end
  end
end
