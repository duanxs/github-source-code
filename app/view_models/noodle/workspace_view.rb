# frozen_string_literal: true

module Noodle
  class WorkspaceView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    include RenderHelper
    attr_reader :owner, :repo, :name, :path, :tree_name, :current_repository

    def noodle_url
      noodle_path = String.new("/@#{owner}/#{repo}")
      noodle_path << "/#{CGI.escape(name)}" if name.present?
      noodle_path << ":#{Addressable::URI.escape(path)}" if path.present?

      @noodle_url ||= GitHub.noodle_editor_host_url + noodle_path
    end

    def page_title
      "Editing #{current_repository.name_with_owner}"
    end

    def identity
      @identity ||= SecureRandom.uuid
    end

  end
end
