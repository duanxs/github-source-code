# frozen_string_literal: true

module Files
  class CloneURLView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    include GitHub::Application.routes.url_helpers

    attr_reader :repository
    attr_reader :type
    attr_reader :is_default
    attr_reader :pushable

    def description
      case type
      when :gitweb
        "(read only) clone"
      when :subversion
        "checkout"
      else
        "clone"
      end
    end

    def protocol
      case type
      when :local
        "Local"
      when :ssh
        "SSH"
      when :http
        secure_http?? "HTTPS" : "HTTP"
      when :gitweb
        "Git"
      when :subversion
        "Subversion"
      end
    end

    def url
      case type
      when :http
        repository.http_url
      when :local
        repository.local_url
      when :ssh
        repository.ssh_url
      when :gitweb
        repository.gitweb_url
      when :subversion
        repository.svn_url
      else
        raise ArgumentError, "unknown type: %p" % type
      end
    end

    # The URL to POST to to save a sticky selection
    def sticky_url
      user_set_protocol_path({
        protocol_selector: type.to_s,
        protocol_type: (pushable ? "push" : "clone"),
      })
    end

    def secure_http?
      !Rails.env.development?
    end
  end
end
