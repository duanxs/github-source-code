# frozen_string_literal: true

module NotificationSubscriptions
  class IndexView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    include PlatformHelper

    attr_reader \
      :unauthorized_saml_targets,
      :unauthorized_saml_organization_ids,
      :unauthorized_ip_whitelisting_targets,
      :unauthorized_ip_whitelisting_organization_ids,
      :sort_direction,
      :reason,
      :reasons,
      :selected_repository

    Root = parse_query <<-'GRAPHQL'
      fragment on Query {
        selectedRepository: node(id: $listId) @include(if: $includeList) {
          ...Views::NotificationSubscriptions::SelectedFilters::Repository
        }

        requester {
          ... on RequestingUser {
            notificationThreadSubscriptions(first: $first, last: $last, after: $after, before: $before, orderBy: $orderBy, reason: $reason, listId: $listId, listType: REPOSITORY) {
              nodes {
                ...Views::NotificationSubscriptions::Index::Subscription

                thread {
                  ... on Issue {
                    repository {
                      organization {
                        databaseId
                      }
                    }
                  }

                  ... on PullRequest {
                    repository {
                      organization {
                        databaseId
                      }
                    }
                  }
                }
              }
              pageInfo {
                hasNextPage
                hasPreviousPage
                ...ApplicationHelper::CursorPaginate
              }
              totalCount
            }
          }
        }
      }
    GRAPHQL

    def initialize(
      graphql_data:,
      unauthorized_saml_targets:,
      unauthorized_saml_organization_ids:,
      unauthorized_ip_whitelisting_targets:,
      unauthorized_ip_whitelisting_organization_ids:,
      sort_direction:,
      reason:,
      reasons:
    )
      @root = Root.new(graphql_data)
      @selected_repository = @root.selected_repository
      @unauthorized_saml_targets = unauthorized_saml_targets
      @unauthorized_saml_organization_ids = unauthorized_saml_organization_ids
      @unauthorized_ip_whitelisting_targets = unauthorized_ip_whitelisting_targets
      @unauthorized_ip_whitelisting_organization_ids = unauthorized_ip_whitelisting_organization_ids
      @sort_direction = sort_direction == "asc" ? :asc : :desc
      @reason = reason
      @reasons = reasons
    end

    def visible_subscriptions
      @visible_subscriptions ||= begin
        subscriptions.nodes.find_all do |subscription|
          !belongs_to_unauthorized_org?(subscription.thread)
        end
      end
    end

    def subscription_count
      subscriptions.total_count
    end

    def paginate?
      page_info.has_next_page || page_info.has_previous_page
    end

    def page_info
      subscriptions.page_info
    end

    def sort_directions
      {
        desc: "Most recently subscribed",
        asc: "Least recently subscribed",
      }
    end

    def sort_direction_text
      sort_directions[sort_direction]
    end

    private

    def belongs_to_unauthorized_org?(thread)
      owner = thread.repository.organization
      belongs_to_saml_org = owner && @unauthorized_saml_organization_ids.include?(owner.database_id)
      belongs_to_ip_whitelisting_org = owner && @unauthorized_ip_whitelisting_organization_ids.include?(owner.database_id)
      belongs_to_saml_org || belongs_to_ip_whitelisting_org
    end

    def subscriptions
      @root.requester.notification_thread_subscriptions
    end
  end
end
