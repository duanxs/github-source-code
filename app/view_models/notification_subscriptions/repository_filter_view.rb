# frozen_string_literal: true

module NotificationSubscriptions
  class RepositoryFilterView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    include PlatformHelper
    include GitHub::Application.routes.url_helpers

    Requester = parse_query <<-'GRAPHQL'
      fragment on RequestingUser {
        notificationThreadSubscriptionLists(first: 250, listType: REPOSITORY) {
          nodes {
            ... on Repository {
              nameWithOwner
              id

              owner {
                avatarUrl
              }
            }
          }
        }
      }
    GRAPHQL

    attr_reader :requester, :params

    def lists
      @lists ||= Requester.new(requester).notification_thread_subscription_lists.nodes
    end

    def sorted_lists
      @sorted_lists ||= lists.sort_by { |list| list.name_with_owner }
    end

    def repository_url(list = nil)
      repository_param = list.present? ? list.id : nil
      notification_subscriptions_path(link_params.merge(repository: repository_param))
    end

    def link_params
      @link_params ||= params.slice(:sort, :reason, :repository)
    end

    def repository_selected?(list)
      params[:repository] == list.id
    end

    def no_repository_selected?
      params[:repository].nil?
    end

    def avatar_url(list)
      list.owner.avatar_url
    end

    def label(list)
      list.name_with_owner
    end

    def reason
      link_params[:reason]
    end

    def sort
      link_params[:sort]
    end
  end
end
