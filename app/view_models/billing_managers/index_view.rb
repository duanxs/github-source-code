# frozen_string_literal: true

class BillingManagers::IndexView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :organization

  def current_or_pending_billing_managers?
    organization.billing_managers.any? || organization.pending_invitations.with_business_role(:billing_manager).any?
  end

  def show_remove_user_completely?(user)
    organization.direct_or_team_member?(user) && (user != current_user)
  end
end
