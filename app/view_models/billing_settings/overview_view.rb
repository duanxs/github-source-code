# frozen_string_literal: true

module BillingSettings
  class OverviewView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    include PlanHelper

    MUNICH_ACTIONS_MINUTES_CHANGE_DATE = GitHub::Billing.timezone.parse("2020-05-14").to_date
    MUNICH_ACTIONS_MINUTES_TRANSITION_END_DATE = MUNICH_ACTIONS_MINUTES_CHANGE_DATE + 1.month

    include GitHub::Application.routes.url_helpers
    attr_reader :account, :apple_iap_subscription_enabled

    def pricing
      @pricing ||= ::Billing::Pricing.new(account: account)
    end

    def show_expired_cloud_trial?(target)
      return false unless target.plan.name == GitHub::Plan::FREE
      trial = Billing::PlanTrial.find_by(user: target, plan: GitHub::Plan::BUSINESS_PLUS)

      trial.present? && trial.expired_within?(Billing::EnterpriseCloudTrial::EXPIRATION_MESSAGE_DURATION)
    end

    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def payment_method
      @payment_method ||= if account.payment_method && account.payment_method.valid_payment_token?
        account.payment_method
      end
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

    def account_has_coupon?
      account.coupon.present?
    end

    def org_is_on_per_seat?
      account.plan.per_seat?
    end

    def change_duration_path
      if account.user?
        upgrade_path(plan_duration: alternative_duration, plan: account.plan)
      elsif !account.plan.per_repository?
        upgrade_path(org: account, target: "organization", plan_duration: alternative_duration, plan: account.plan)
      end
    end

    def user_billing_information
      account.user_personal_profile.presence || account.build_user_personal_profile
    end

    def show_name_address_form?
      GitHub.flipper[:name_address_collection].enabled?(account) && account.user?
    end

    def alternative_duration
      account.plan_duration == User::MONTHLY_PLAN ? User::YEARLY_PLAN : User::MONTHLY_PLAN
    end

    def has_billing_extra?
      account.billing_extra.present?
    end

    def has_vat_code?
      account.vat_code.present?
    end

    def last_payment_date
      account.billing_transactions.sales.last&.created_at.to_date
    end

    def last_payment_amount
      if last_sale = account.billing_transactions.sales.last
        last_transaction_amount = if last_sale.was_refunded?
          -last_sale.refund.amount_in_cents
        else
          last_sale.amount_in_cents
        end

        last_transaction_amount / 100.to_f
      end
    end

    def last_payment_status
      if account.billing_transactions.sales.last
        t = account.billing_transactions.sales.last

        if t.voided? || t.was_refunded? || t.charged_back?
          "refunded"
        elsif t.success?
          "successful"
        else
          "failed"
        end
      end
    end

    def repo_usage
      account.plan.repos - account.owned_private_repositories.count
    end

    def repo_usage_percentage
      if account.owned_private_repositories.count == 0
        0
      elsif account.owned_private_repositories.count > account.plan.repos
        100
      else
        (account.owned_private_repositories.count/account.plan.repos.to_f*100).round 1
      end
    end

    def registry_usage_report
      @registry_usage_report ||= Registry::UsageReport.between(account, account.first_day_in_registry_cycle, Time.now)
    end

    # TODO: Remove `current_user` argument when Sponsors section is no longer
    # behind the `corporate_sponsors_credit_card` feature flag for organizations.
    def show_sponsors?(current_user)
      account.user? || current_user&.corporate_sponsors_credit_card_enabled?
    end

    def show_asset_usage?
      account.data_packs > 0 || account.asset_status.try(:used?)
    end

    def show_new_pricing_callout_for_user?
      account.user? &&
        !account.dismissed_notice?("new_pricing_user") &&
        account.plan.paid? &&
        !account.plan.free_with_addons? &&
        account.billing_transactions.where(plan_name: "pro").empty?
    end

    def show_per_seat_callout?
      !account.invoiced? &&
      account.org_free_plan?
    end

    def show_business_plus_callout?
      account.organization? &&
      !account.invoiced? &&
      !account.plan.business_plus? &&
      !show_per_seat_callout?
    end

    def munich_actions_changes_apply?
      actions_enabled? && account.plan.business?
    end

    def payment_needs_update?
      account.billing_trouble? || account.card_expired?
    end

    def upsell_message_next
      if pro_upgrade?
        "Get private unlimited repositories"
      elsif team_upgrade?
        "Get private repositories and team permissions"
      elsif business_upgrade?
        "Get SAML single-sign-on, 24/5 support, 99.95% Uptime SLA, and more..."
      end
    end

    def pro_upgrade?
      account.can_change_plan_to?(GitHub::Plan.pro)
    end

    alias team_upgrade? show_per_seat_callout?
    alias business_upgrade? show_business_plus_callout?

    def plan_pending_change_indicator
      plan_changing = account.pending_cycle_changing_plan?
      seats_changing = account.pending_cycle_changing_seats?
      duration_changing = account.pending_cycle_changing_duration?
      multiple_changes = [plan_changing, seats_changing, duration_changing].count(true) > 1

      if multiple_changes
        "changes pending"
      elsif plan_changing
        "downgrade pending"
      elsif duration_changing
        "duration change pending"
      elsif seats_changing
        "seats change pending"
      end
    end

    def show_lfs_downgrade?
      return false if account.data_packs.zero?

      show_lfs_upgrade?
    end

    def show_lfs_upgrade?
      return false if account.in_a_sales_managed_business?
      return false if account.invoiced? && !account.business
      true
    end

    def can_update_payment_method?
      !account.invoiced? && !account.has_any_trade_restrictions?
    end

    def invoices
      @_invoices ||= fetch_invoices
    end

    def invoice
      @_invoice ||= invoices.last
    end

    def shared_storage_usage
      usage = Billing::SharedStorageUsage.new(account)
      SharedStorageUsage.new(account, usage: usage)
    end

    def cost_management_enabled?
      Billing::Budget.configurable?(account)
    end

    def metered_billing_enabled?
      actions_enabled? || packages_enabled?
    end
    alias_method :shared_storage_enabled?, :metered_billing_enabled?

    def actions_enabled?
      account.plan_metered_billing_eligible?
    end

    def packages_enabled?
      account.plan_metered_billing_eligible?
    end

    def navigation_tabs
      if account.organization?
        tabs = [
          { name: "Subscriptions",
            path: settings_org_billing_path(organization_id: account.login) },
          { name: "Cost management",
            path: settings_org_billing_tab_path(organization_id: account.login, tab: "cost_management") },
          { name: "Payment information",
            path: settings_org_billing_tab_path(organization_id: account.login, tab: "payment_information") },
        ]
      else
        tabs = [
          { name: "Subscriptions",
            path: settings_user_billing_path },
          { name: "Cost management",
            path: settings_user_billing_tab_path(tab: "cost_management") },
          { name: "Payment information",
            path: settings_user_billing_tab_path(tab: "payment_information") },
        ]
      end

      tabs.delete_at(1) unless cost_management_enabled?
      tabs
    end

    def days_to_next_billing_date
      days_to_date(account.next_billing_date.to_date)
    end

    def days_to_next_metered_billing_date
      days_to_date(account.first_day_in_next_metered_cycle.to_date)
    end

    def days_to_next_lfs_reset_date
      lfs_days_remaining = account.days_remaining_in_lfs_cycle
      if lfs_days_remaining
        pluralize_days_for_display(lfs_days_remaining)
      else
        days_to_next_billing_date
      end
    end

    def days_to_date(date)
      days = (date - GitHub::Billing.timezone.now.to_date).to_i
      pluralize_days_for_display(days)
    end

    def pluralize_days_for_display(days)
      "#{days} #{"day".pluralize(days)}"
    end

    def next_billing_date_after_munich_limits_change
      if account.next_billing_date.to_date > MUNICH_ACTIONS_MINUTES_TRANSITION_END_DATE
        MUNICH_ACTIONS_MINUTES_TRANSITION_END_DATE
      else
        find_next_billing_date(
          starting_with: account.next_billing_date.to_date,
          after: MUNICH_ACTIONS_MINUTES_CHANGE_DATE
        )
      end
    end

    def find_next_billing_date(starting_with:, after:)
      if starting_with >= after
        starting_with
      else
        find_next_billing_date(
          starting_with: starting_with + 1.month,
          after: after
        )
      end
    end

    def munich_actions_minutes_change_date
      MUNICH_ACTIONS_MINUTES_CHANGE_DATE
    end

    def munich_announcement_url
      "#{GitHub.blog_url}/2020-04-14-github-is-now-free-for-teams/"
    end

    def github_usage_edit_options(plan)
      options = []

      if !account.invoiced?
        if pro_upgrade?
          options.push("upgrade-pro")
        elsif account.plan.per_repository?
          options.push("edit-plan")
        elsif team_upgrade?
          options.push("upgrade-team")
        elsif business_upgrade?
          options.push("upgrade-business")
        end

        unless account.org_free_plan? || account.personal_plan? || account.plan.per_repository?
          if plan.is_business_plus?
            options.push("plan-business-plus")
          elsif plan.is_business?
            options.push("plan-business")
          elsif plan.is_pro?
            options.push("plan-pro")
          end
        end

        if org_is_on_per_seat? && !account.has_unlimited_seats?
          options.push("divider")
          options.push("seats-add")
          options.push("seats-remove") if account.has_downgradable_seats?
          options.push("seats-details")
        end

      end

      options
    end

    def cost_management_path
      if account.organization?
        settings_org_billing_tab_path(organization_id: account.login, tab: "cost_management")
      else
        settings_user_billing_tab_path(tab: "cost_management")
      end
    end

    def show_remaining_actions_balance?
      prepaid_usage_calculator.total_purchased_refills_in_cents > 0
    end

    def show_warning?
      prepaid_usage_calculator.amount_remaining_in_cents < 0 && !show_alert?
    end

    def show_alert?
      prepaid_usage_calculator.over_prepaid_limit?
    end

    def refill_amount_remaining
      Billing::Money.new(prepaid_usage_calculator.amount_remaining_in_cents)
    end

    def total_purchased_refills
      Billing::Money.new(prepaid_usage_calculator.total_purchased_refills_in_cents)
    end

    def refill_used_percentage
      prepaid_usage_calculator.percent_used
    end

    def path_for_export
      if account.organization?
        org_metered_exports_path(account)
      else
        metered_exports_path
      end
    end

    def sub_heading
      account.organization? ? "Billing" : "Personal billing"
    end

    def manage_seats_link_text
      text = "Manage seats"
      text += " (#{account.available_seats} unused)" if account.has_downgradable_seats?
      text
    end

    def add_or_remove_link_text
      account.has_downgradable_seats? ? "Remove unused seats" : "Add seats"
    end

    def manage_seats_path
      account.has_downgradable_seats? ? remove_org_seats_path(account) : org_seats_path(account)
    end

    def apple_iap_subscription?
      apple_iap_subscription_enabled && account.user? && account.apple_iap_subscription?
    end

    def actions_and_packages_budget
      @_actions_and_packages_budget ||= account.budget_for(product: :shared)
    end

    def codespaces_enabled?
      account.billing_github_codespaces_enabled?
    end

    def codespaces_budget
      @_codespaces_budget ||= account.budget_for(product: :codespaces)
    end

    def account_has_valid_payment_method?
      account.has_valid_payment_method?
    end

    private

    def fetch_invoices
      return [] if account.customer&.zuora_account_id.nil?

      Billing::ZuoraInvoice.invoices_for_account(account.customer.zuora_account_id)
        .select(&:posted?)
        .sort_by(&:invoice_date)
    rescue Zuorest::HttpError
      []
    end

    def prepaid_usage_calculator
      @_prepaid_usage_calculator ||= Billing::PrepaidUsageCalculator.new(account)
    end
  end
end
