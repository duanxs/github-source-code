# frozen_string_literal: true

module Hooks
  class ShowView < Hooks::BaseView
    CONTENT_TYPES = {
      "application/json" => "json",
      "application/x-www-form-urlencoded" => "form",
    }

    attr_reader :hook

    def status_message
      return "This service has been denied access by an organization administrator." if muted?
      return "This hook is inactive."  unless hook.active?

      case Hookshot::Delivery.status_code_to_label hook.last_status
      when :active
        "Last delivery was successful."
      when :unused
        "This hook has never been triggered."
      when :hookshot_error
        "Last delivery was not successful. An exception occurred."
      else
        "Last delivery was not successful. #{hook.last_status_message}."
      end
    end

    def status_class
      return "mute" if muted?
      return "inactive" unless hook.active?

      Hookshot::Delivery.status_code_to_class hook.last_status
    end

    def base_url
      url = hook.config["url"].to_s
      url = "http://#{url}" unless url =~ %r{\Ahttps?://}
      uri = Addressable::URI.parse(url).normalize
      port = ":#{uri.port}" if uri.port
      "#{uri.scheme}://#{uri.host}#{port}#{uri.path}"
    rescue Addressable::URI::InvalidURIError
      "Could not parse URL"
    end

    def title
      webhook? ? base_url : hook.display_name
    end

    def oauth_application_name
      hook.oauth_application.name
    end

    def webhook?
      hook.webhook?
    end

    def legacy_service?
      hook.legacy_service?
    end

    def services_disabled?
      legacy_service?
    end

    def hook_type
      webhook? ? "webhook" : "service"
    end

    def hook_active_status
      hook.active? ? "enabled" : "disabled"
    end

    def hook_type_title
      hook_type.pluralize.titleize
    end

    def hook_events
      @hook_events ||= Hook::EventRegistry.
        subscribable_by(hook: hook, user: current_user).
        sort_by(&:event_type)
    end

    def content_types
      CONTENT_TYPES
    end

    def content_type
      hook.content_type
    end

    def push_event_only
      hook && hook.events == ["push"]
    end

    def wildcard_event
      hook && hook.events.include?(Hook::WildcardEvent)
    end

    def custom_events
      !push_event_only && !wildcard_event
    end

    def total_grouped_events
      grouped_events.values.inject(0) { |sum, e| sum + e.count }
    end

    def human_name(item)
      item.humanize
    end

    def label_correction(field)
      field.to_s.humanize.sub(/Github/, "GitHub")
    end

    def events_sentence(events = hook.events)
      if events.include?(Hook::WildcardEvent)
        "all events"
      else
        events.to_sentence
      end
    end

    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def muted?
      @muted ||= Hooks::OauthApplicationPolicyView.new(hook: hook).violates_policy?
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

    def hook_disabled?
      (hook.installation_target_type == "Repository") && hook.installation_target.disabled?
    end

    def hook_target
      return unless hook_disabled?
      hook.installation_target.class.name.humanize
    end

    def include_push_option?
      Hook::Event::PushEvent.supports_target?(hook.installation_target)
    end

    def managed_by_oauth_app?
      hook.oauth_application && !hook.editable_by?(current_user)
    end

    def disabled_tooltip
      if !fires_for_push?
        "This service cannot be tested because it doesn't support the 'push' event"
      end
    end

    def test_service_button_classes
      classes = ["btn btn-sm boxed-group-action"]
      classes += %w[tooltipped tooltipped-s] if disabled_tooltip
      classes.join(" ")
    end

    def fires_for_push?
      hook.fires_for_event?(:push)
    end

    def selected_link
      if hook_type.to_sym == :service
        :integration_installations
      else
        :hooks
      end
    end
  end
end
