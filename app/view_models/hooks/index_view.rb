# frozen_string_literal: true

module Hooks
  class IndexView < Hooks::BaseView
    # The org or repo the hooks are installed on
    attr_reader :parent

    def hooks
      @hooks ||= begin
        hooks = parent.hooks.ordered.all
        if load_hook_statuses?
          Hook::StatusLoader.load_statuses(hook_records: hooks, parent: parent)
        else
          hooks
        end
      end
    end

    def webhooks
      @webhooks ||= hooks.select(&:webhook?)
    end

    def load_hook_statuses
      @load_hook_statuses = true
    end

    def each_with_view(hook_collection)
      hook_collection.each do |hook|
        yield hook, Hooks::ShowView.new(hook: hook, current_user: current_user)
      end
    end

    def repository_context_within_an_organization?
      parent.is_a?(Repository) && parent.owner.organization?
    end

    private

    def load_hook_statuses?
      !!@load_hook_statuses
    end
  end
end
