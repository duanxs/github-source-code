# frozen_string_literal: true

module Issues
  class IssueEventView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    include ActionView::Helpers::TagHelper
    include ActionView::Helpers::UrlHelper
    include AvatarHelper
    include BasicHelper
    include IssuesHelper
    include UrlHelper

    attr_reader :repo, :events

    def canonical_issue_noun
      canonical_issue = event.subject
      other_repo = canonical_issue.repository
      link_text = if other_repo == repo
        "##{canonical_issue.number}"
      else
        "#{other_repo.nwo}##{canonical_issue.number}"
      end
      url_args = [other_repo.owner, other_repo, canonical_issue]
      link_url = if canonical_issue.pull_request?
        urls.show_pull_request_path(*url_args)
      else
        urls.issue_path(*url_args)
      end
      helpers.link_to(link_text, link_url, class: "text-bold", title: canonical_issue.title)
    end
  end
end
