# frozen_string_literal: true

module Issues
  class TipView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    include ActionView::Helpers::UrlHelper
    include IssuesHelper
    include TipsHelper

    attr_reader :repo, :parsed_issues_query, :pulls_only

    # An Array of String tips.
    #
    # A tip can have one key:value pair that will auto-link and add that to the
    # query.
    def tips
      tips = [
        "Adding no:label will show everything without a label.",
        "no:milestone will show everything without a milestone.",
        "Add no:assignee to see everything that’s not assigned.",
        "Follow long discussions with comments:>50.",
        "Updated in the last three days: updated:>#{(Date.today - 3.days).to_s(:db)}.",
        "What’s not been updated in a month: updated:<#{(Date.today - 1.month).to_s(:db)}.",
        "Mix and match filters to narrow down what you’re looking for.",
        "Exclude everything labeled <code>bug</code> with -label:bug.", # rubocop:disable Rails/ViewModelHTML
        "Type <kbd>g</kbd> <kbd>i</kbd> on any issue or pull request to go back to the issue listing page.", # rubocop:disable Rails/ViewModelHTML
        "Type <kbd>g</kbd> <kbd>p</kbd> on any issue or pull request to go back to the pull request listing page.", # rubocop:disable Rails/ViewModelHTML
      ]

      if pulls_only && repo
        tips << "Filter pull requests by the default branch with base:#{repo.default_branch}."
      end

      # linked: qualifier tips
      if pulls_only
        tips << "Find all pull requests that aren't related to any open issues with -linked:issue."
      else
        tips << "Find all open issues with in progress development work with linked:pr."
      end

      # User-centric tips.
      if logged_in?
        tips << "Exclude your own issues with -author:#{current_user}."
        tips << "Notify someone on an issue with a mention, like: @#{current_user}."
        tips << "Find everything you created by searching author:#{current_user}."
        tips << "Ears burning? Get @#{current_user} mentions with mentions:#{current_user}."
      end

      # Grab a random team for a few sample filters.
      if logged_in? && repo.try(:in_organization?)
        team = repo.organization.teams_for(current_user).sample
        tips << "Check team mentions with team:#{team.combined_slug}." if team
      end

      # Triage mode.
      if repo && repo.pushable_by?(current_user)
        tips << "Click a checkbox on the left to edit multiple issues at once."
      end

      tips
    end

    # Grab a random rendered tip.
    #
    # Returns a String.
    def selected_tip
      render_link(tips.sample, components: parsed_issues_query) do |components|
        if repo
          issues_path(q: Search::Queries::IssueQuery.stringify(components))
        else
          all_issues_path(q: Search::Queries::IssueQuery.stringify(components))
        end
      end
    end

    # `issues_path` isn't defined in view models, at least not to the extent we
    # can use it unless we give it a hint about the repo and owner id.
    def issues_path(options)
      options.merge!(repository: repo, user_id: repo.owner)
      urls.issues_path(options)
    end

    # `all_issues_path` isn't defined in view models, either.
    def all_issues_path(options)
      urls.all_issues_path(options)
    end

    def current_repository
      repo
    end
  end
end
