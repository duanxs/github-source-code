# frozen_string_literal: true

module Issues
  class IssueListItem < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :issue,
      :repo,
      :draggable,
      :global,
      :hide_milestone,
      :drag_disabled_message,
      :show_drag_controls,
      :hide_checkbox,
      :hide_unread,
      :reference_location

    def initialize(attributes)
      super
      set_pull_targets
    end

    # The PR or the issue object for this item.
    def subject
      issue.pull_request? ? issue.pull_request : issue
    end

    def id
      parts = []
      parts << "issue_#{issue.number}"
      parts << issue.repository.nwo.gsub("/", "_") if global
      parts.join("_")
    end

    def classes
      # FIXME: mt-0 is a hack for broken Box-row styles
      classes = ["Box-row Box-row--focus-gray p-0 mt-0 js-navigation-item js-issue-row"]

      if logged_in?
        classes << (issue.read_by_current_user || hide_unread? ? "" : "Box-row--unread")
      end

      classes << "js-draggable-issue sortable-button-item" if draggable?
      classes.join(" ")
    end

    # Set all the targets for this pull.
    #
    # Returns nothing.
    def set_pull_targets
      if pull_request=issue.pull_request
        pull_request.association(:repository).target = issue.repository
        pull_request.association(:issue).target = issue
      end
    end

    # Public: Is the issue representing a pull request that's a draft?
    def draft?
      issue.pull_request? && issue.pull_request&.draft?
    end

    # Which type of icon should we display for this particular issue?
    #
    # Returns a String.
    def icon
      if issue.state == "locked"
        "lock"
      elsif issue.pull_request
        if issue.pull_request.merged?
          "git-merge"
        else
          "git-pull-request"
        end
      elsif issue.open?
        "issue-opened"
      else
        "issue-closed"
      end
    end

    def icon_class
      if !issue.closed? && draft?
        "text-gray-light"
      else
        status
      end
    end

    def icon_tooltip
      parts = []
      parts << status.capitalize

      if draft?
        parts << "draft"
      end

      parts << type_name
      parts.join(" ")
    end

    # The path to submit data to for an issues search. This exists to support
    # both of our use cases: from a repo page, or from the global issues search
    # page.
    #
    # Returns a String.
    def target_path(options = {})
      if repo
        urls.issues_path(repo.owner, repo, options)
      else
        urls.all_issues_path(options)
      end
    end

    def type_filter
      if issue.pull_request?
        "pr"
      else
        "issue"
      end
    end

    def query_filter
      author = issue.safe_user
      author = author.to_query_filter if author.is_a?(Bot)

      if issue.open?
        "is:#{type_filter} is:open author:#{author}"
      else
        "is:#{type_filter} author:#{author}"
      end
    end

    def type_name
      if issue.pull_request?
        "pull request"
      else
        "issue"
      end
    end

    def status
      if issue.pull_request? && issue.pull_request.merged?
        "merged"
      else
        issue.open? ? "open" : "closed"
      end
    end

    # Does this item have a task list associated with it?
    #
    # Returns a Boolean.
    def task_list?
      subject.task_list?
    end

    def comment_count
      @comment_count ||= if issue.pull_request?
        issue.pull_request.total_comments
      else
        issue.issue_comments_count
      end
    end

    def any_comments?
      !comment_count.zero?
    end

    def linked_xrefs_count
      issue.close_issue_references_count
    end

    def any_linked_xrefs?
      !linked_xrefs_count.zero?
    end

    def xref_octicon
      @xref_octicon ||= issue.pull_request? ? "issue-opened" : "git-pull-request"
    end

    def xref_subject
      @xref_subject ||= issue.pull_request? ? "issue" : "pull request"
    end

    def draggable?
      draggable
    end

    # We might show the controls but still not allow dragging
    def show_drag_controls?
      show_drag_controls
    end

    def hide_milestone?
      hide_milestone
    end

    def hide_checkbox?
      hide_checkbox
    end

    def hide_unread?
      hide_unread
    end

    # Should we load the review status asynchronously?
    def async_review_status?
      GitHub.flipper[:async_review_status].enabled?(issue.repository) &&
        !draft? && issue.pull_request.reviews.submitted.any?
    end
  end
end
