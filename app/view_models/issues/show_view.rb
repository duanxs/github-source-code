# frozen_string_literal: true

module Issues
  class ShowView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :repo

    def milestones
      @milestones ||= repo.milestones
    end

    def labels
      repo.sorted_labels
    end
  end
end
