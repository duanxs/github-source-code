# frozen_string_literal: true

module Biztools
  module RepositoryActions
    class ShowView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
      include PlatformHelper
      include OcticonsCachingHelper

      attr_reader :data, :action

      Query = parse_query <<~"GRAPHQL"
        fragment Root on Query {
          repositoryAction(id: $id) {
            id
            databaseId
            slug
            name
            description
            securityEmail
            color
            iconName
            iconColor
            path
            rankMultiplier
            hasVerifiedOwner
            featured
            isListed
            repository {
              url
              isPrivate
              hasActionAtRoot
              owner {
                login
              }
            }
          }
        }
      GRAPHQL

      delegate :name, :security_email, :slug, :description, :color,
        :path, :rank_multiplier, :icon_name, :icon_color, to: :action, prefix: true

      def initialize(data:, current_user: nil, user_session: nil)
        @data = Query::Root.new(data)
        @action = @data.repository_action
        @current_user = current_user
      end

      def owner_verified?
        action.has_verified_owner
      end

      def elasticsearch_entry
       query = Search::Queries::MarketplaceQuery.new(current_user: @current_user, type: "repository-action", phrase: action.name)
       result = query.execute.results.detect { |r| r["_id"].to_i == action_id }
       return "No entry found" if result.nil?
       result
      end

      def icon(checked)
        checked ? octicon("check-circle", color: "green") : octicon("x-circle", color: "red", height: 16)
      end

      def repository_public_icon
        icon(!action.repository.is_private)
      end

      def repository_has_action_icon
        icon(action.repository.has_action_at_root)
      end

      def metadata_icon
        icon(repository_action.config_from_metadata_file.any?)
      end

      # agreement_icon returns the agreement signature status. If the action is owned by an org,
      # we check for a signature, if the action is owned by a user, we default to false because we
      # can't be sure who is publishing the action.
      def agreement_icon
        has_signed = if repository_action.owned_by_org?
          repository_action.org_has_signed_integrator_agreement?(org: repository_action.owner)
        else
          false
        end

        icon(has_signed)
      end

      def owned_by_org?
        repository_action.owned_by_org?
      end

      def action_id
        action.database_id
      end

      def action_node_id
        action.id
      end

      def action_featured?
        action.featured
      end

      def action_listed?
        action.is_listed
      end

      def action_repository_url
        action.repository.url
      end

      def action_owner_login
        action.repository.owner.login
      end

      private

      def repository_action
        @repository_action ||= RepositoryAction.find(action_id)
      end
    end
  end
end
