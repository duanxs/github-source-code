# frozen_string_literal: true

module RemindersBetaMemberships
  class BetaSignupView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :membership, :adminable_accounts, :selected_account

    def selected?(member)
      selected_account == member
    end

    def already_enabled?
      selected_account.reminders_enabled?
    end

    def member_type(member)
      member&.is_a?(Business) ? "Business" : "User"
    end

    def target_member_description
      return unless membership&.member

      if membership.member.is_a?(Business)
        "your business #{membership.member.name}"
      elsif membership.member.organization?
        "your organization @#{membership.member.login}"
      else
        "your personal account"
      end
    end
  end
end
