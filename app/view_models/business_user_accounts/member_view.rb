# frozen_string_literal: true

module BusinessUserAccounts
  class MemberView < Businesses::QueryView
    # query filters defined for QueryView
    attr_reader :role

    def filter_map
      BusinessesHelper::USER_ACCOUNT_MEMBERSHIP_QUERY_FILTERS
    end
  end
end
