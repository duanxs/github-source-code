# frozen_string_literal: true

module BusinessUserAccounts
  class SidebarView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :business, :user, :business_user_account,
                :organization_count, :installation_count, :collab_repo_count

    def role
      @role ||= Business::Role.new(business, user,
                                   organization_count: organization_count,
                                   installation_count: installation_count,
                                   collab_repo_count: collab_repo_count)
    end

    def user_roles
      @user_roles ||= begin
        user_roles = []
        role.types.each do |type|
          user_roles << {
              name: Business::Role.name_for_type(type),
              description: Business::Role.description_for_type(type, role),
          }
        end

        user_roles
      end
    end

    def sso_status
      if business.saml_provider.external_identities.linked_to(user).exists?
        "SAML identity linked"
      else
        "No SAML identity linked"
      end
    end

    # Public: Should the SAML status for the person be shown? Only if SAML is
    # enabled for this Business
    #
    # Returns a Boolean
    def show_sso_status?
      return false if user.nil?

      business.saml_sso_enabled?
    end

    # Public: Should the 2FA status for the person be shown? Only if this BusinessUserAccount
    # has a dotcom user, and if 2FA is enabled for the authentication system being used.
    #
    # Returns a Boolean
    def show_two_factor_status?
      return false if user.nil?

      GitHub.auth.two_factor_authentication_enabled?
    end

    # Public: get the display name for this account.
    #
    # Returns a String
    def primary_name
      user_name = if user.nil?
        nil
      elsif user.profile_name.blank?
        user.login
      else
        user.profile_name
      end

      user_name || business_user_account&.login
    end
  end
end
