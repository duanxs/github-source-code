# frozen_string_literal: true

module Diff
  class FileView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    include RenderHelper
    include UrlHelper

    attr_reader :diff, :diff_number, :repository, :current_user,
                :show_generated, :hidden_diff_reason, :diff_blob,
                :show_deleted, :blob_a_size, :blob_b_size
    attr_accessor :hidden_render_diff
    alias :hidden_render_diff? :hidden_render_diff
    alias :show_generated? :show_generated
    alias :show_deleted? :show_deleted

    def after_initialize
      @show_generated = false unless defined?(@show_generated)
    end

    def path
      diff.path
    end

    def reviewed?(pull)
      file_review_view(pull).reviewed?
    end

    def dismissed?(pull)
      file_review_view(pull).dismissed?
    end

    def file_review_view(pull)
      @file_review_view ||= FileReviewView.new(pull_request: pull, path: diff.path, user: current_user)
    end

    def has_content?
      hidden_diff_reason.blank? && !diff.text.blank?
    end

    def diff_blob # rubocop:disable Lint/DuplicateMethods
      @diff_blob ||= helpers.diff_blob(diff, repository)
    end

    def diff_head_blob_size
      blob_b_size || 0
    end

    def diff_base_blob_size
      blob_a_size || 0
    end

    def set_diff_blob_sizes(file_list_view)
      # avoid recalculation when unnecessary
      if !file_list_view.blob_sizes.key?(diff.a_blob) ||
         !file_list_view.blob_sizes.key?(diff.b_blob)
        file_list_view.prepare_blob_sizes([diff])
      end
      @blob_a_size = file_list_view.blob_sizes[diff.a_blob]
      @blob_b_size = file_list_view.blob_sizes[diff.b_blob]
    end

    def diff_blob_path(base, head)
      if diff.deleted?
        "/#{base.name_with_owner}/blob/#{diff.a_sha}/#{urls.escape_url_branch(diff.a_path)}"
      else
        "/#{head.name_with_owner}/blob/#{diff.b_sha}/#{urls.escape_url_branch(diff.b_path)}"
      end
    end

    def display_diff_blob_action_buttons?
      diff.a_sha.present? && !diff.submodule?
    end

    def identifier
      return @identifier if defined?(@identifier)
      @identifier = Digest::MD5.hexdigest(path)
    end

    def anchor
      @anchor ||= diff_file_anchor(path)
    end

    def show_full_diff_bar?
      diff.binary? || (diff.changes == 0 && !diff.renamed?)
    end

    def simple_rename?
      diff.renamed? && diff.changes == 0
    end

    def formatted_diffstat(width = 5)
      DiffHelper.format_diffstat_line(diff, width)
    end

    def octicon
      name =
        if diff.status_label == "moved"
          "renamed"
        elsif diff.status_label == "changed"
          "modified"
        else
          diff.status_label
        end
      "diff-#{name}"
    end

    def prepare_for_rendering!
      determine_hidden_diff_reason

      diff.text = nil if generated? || suppress_deletion?
    end

    def determine_hidden_diff_reason
      @hidden_diff_reason = case
      when suppress_deletion?
        :deleted
      when diff.too_big?
        :too_big
      when diff.text.blank?
        nil
      when generated?
        :generated
      end
    end

    def generated?
      !show_generated? && diff_blob.generated?
    end

    def suppress_deletion?
      !show_deleted? && diff.deleted?
    end

    def prose_diff?
      return @prose_diff if defined?(@prose_diff)

      @prose_diff =
        GitHub::Markup.can_render?(path, diff_blob.data) &&
        diff.valid?
    end

    def render_diff?
      return @render_diff if defined?(@render_diff)

      @render_diff = !!diff_blob.render_file_type_for_display(:diff)
    end

    def toggleable?
      !!diff_blob.render_file_type_for_display(:diff) || prose_diff?
    end

    def submodule_file_view
      return @submodule_file_view if defined?(@submodule_file_view)

      @submodule_file_view = Diff::SubmoduleFileView.new(
        repository: repository,
        diff_entry: diff,
      )
    end

    def binary_diff_size
      if diff.modified?
        diff_head_blob_size - diff_base_blob_size
      else
        diff_base_blob_size
      end
    end

    def binary_diff_percentage
      @binary_diff_percentage ||= if diff.added? || diff_base_blob_size == 0
        100
      elsif diff.deleted?
        -100
      else
        (diff_head_blob_size.to_f / diff_base_blob_size.to_f) * 100
      end
    end

    def display_diff_size?
      diff.binary? && (blob_a_size.present? || blob_b_size.present?)
    end

    def display_diff_size_percentage?
      !diff.added? && !diff.deleted? && !diff.renamed? && !diff.copied?
    end
  end
end
