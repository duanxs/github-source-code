# frozen_string_literal: true

module Stafftools
  module Haystack
    # Generates a link to quickly search for exceptions on haystack
    #
    # query - the query string to send
    #
    # Returns a string with a direct search URL
    def haystack_query_link(application = "github", query)
      "https://haystack.githubapp.com/#{application}?q=#{query}"
    end
  end
end
