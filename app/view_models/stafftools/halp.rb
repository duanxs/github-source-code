# frozen_string_literal: true

module Stafftools
  module Halp
    # Generates a Halp search link for a specific email address.
    #
    # email - the address to search for.
    def halp_search_url(criteria)
      query = {filter: criteria.to_s}.to_query
      "https://halp.githubapp.com/searches/new?#{query}"
    end

    def halp_new_discussion_url(email, inbox = "technical")
      query = {bcc: email}.to_query
      "https://halp.githubapp.com/inboxes/#{inbox}/discussions/new?#{query}"
    end
  end
end
