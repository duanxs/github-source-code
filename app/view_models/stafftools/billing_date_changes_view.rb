# frozen_string_literal: true

module Stafftools
  class BillingDateChangesView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :change, :target

    delegate :amount_due, :price_per_day,
      to: :change

    delegate :invoiced?, :payment_method,
      to: :target

    delegate :external_subscription?,
      to: :target, prefix: true

    def current_billing_date
      target.billed_on
    end

    def current_term_end_date
      target.billed_on - 1.day if target.billed_on
    end

    def days_left
      change.days_delta
    end

    def final_price
      amount_due
    end

    def is_charge?
      amount_due.cents > 0
    end

    def original_billing_date
      target.billed_on
    end

    def payment_description
      paypal_payment_description || credit_card_payment_description || default_payment_description
    end

    def short_description
      desc = if is_charge?
        "Charge for the #{days_left.abs} additional #{"day".pluralize(days_left.abs)}"
      else
        "Refund for the remaining #{days_left.abs} #{"day".pluralize(days_left.abs)}"
      end
    end

    private
    def credit_card_payment_description
      "PayPal account" if payment_method && payment_method.paypal?
    end

    def default_payment_description
      "currently configured payment method"
    end

    def paypal_payment_description
      if payment_method && payment_method.credit_card? && payment_method.card_type.present?
        "#{payment_method.card_type} card"
      end
    end
  end
end
