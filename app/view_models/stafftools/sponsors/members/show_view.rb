# frozen_string_literal: true

module Stafftools
  module Sponsors
    module Members
      class ShowView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
        attr_reader :listing

        def can_approve_listing?
          return false unless listing.present?

          listing.approvable_by?(current_user)
        end

        def can_unpublish_listing?
          return false unless listing.present?

          listing.unpublishable_by?(current_user)
        end

        def stripe_account
          listing.stripe_connect_account
        end

        def stripe_account_id
          stripe_account.stripe_account_id
        end

        def docusign_envelope
          listing.active_docusign_envelope
        end

        def docusign_status_text
          if docusign_envelope
            if docusign_envelope.completed?
              "Completed"
            else
              "Sent"
            end
          else
            "Not sent"
          end
        end

        def docusign_status_subtext
          if docusign_skipped?
            "The DocuSign requirement has been skipped for @#{listing.sponsorable.login}"
          elsif docusign_unsent?
            "@#{listing.sponsorable.login} needs to request the appropriate tax document from their " \
            "Sponsorship dashboard by adding their country or region of residency to their GitHub Sponsors settings"
          end
        end

        def docusign_status_octicon
          if docusign_envelope&.completed? || docusign_skipped?
            helpers.octicon("check", class: "text-green mr-1")
          elsif docusign_envelope
            helpers.octicon("clock", class: "color-yellow-7 mr-1")
          else
            helpers.octicon("primitive-dot", class: "text-gray mr-1")
          end
        end

        def docusign_document_type
          return unless docusign_envelope
          docusign_envelope.document_type.to_s.gsub("_", "-").upcase
        end

        def stripe_verified?
          return false unless stripe_account
          stripe_account.verified?
        end

        def stripe_verification_status
          stripe_verified? ? "Verified" : "Unverified"
        end

        def stripe_account_url
          base_url = if Rails.development?
            "https://dashboard.stripe.com/test"
          else
            "https://dashboard.stripe.com"
          end

          "#{base_url}/connect/accounts/#{stripe_account_id}"
        end

        def stripe_disabled_reason
          return nil unless details = stripe_account&.stripe_account_details
          details.dig("requirements", "disabled_reason")
        end

        private

        def docusign_unsent?
          return true if !docusign_envelope
          docusign_envelope.unsent?
        end

        def docusign_skipped?
          return @docusign_skipped if defined?(@docusign_skipped)
          @docusign_skipped = !listing.sponsorable.sponsors_docusign_enabled?
        end
      end
    end
  end
end
