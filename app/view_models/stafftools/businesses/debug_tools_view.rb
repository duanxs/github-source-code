# frozen_string_literal: true

class Stafftools::Businesses::DebugToolsView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :business

  def page_title
    "#{business.name} - debug tools"
  end

  def organizations_requiring_billing_transition
    @_organizations_requiring_billing_transition ||= business.organizations.where.not(billing_type: :invoice)
  end
end
