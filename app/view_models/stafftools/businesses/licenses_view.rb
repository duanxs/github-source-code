# frozen_string_literal: true

class Stafftools::Businesses::LicensesView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :business

  delegate :name, :slug, to: :business

  def unique_user_count
    business.consumed_licenses
  end

  def categories
    {
      "Organizations" => organization_rows,
      "Enterprise installations" => enterprise_installation_rows,
    }
  end

  private

  def organization_rows
    @organization_rows ||= business.organizations.map do |organization|
      license_attributer = Organization::LicenseAttributer.new(organization)

      { label: organization.login, unique_count: license_attributer.unique_count, link_url: urls.stafftools_user_teams_path(organization) }
    end
  end

  def enterprise_installation_rows
    @enterprise_installation_rows ||= business.enterprise_installations.map do |enterprise_installation|
      license_attributer = EnterpriseInstallationLicenseAttributer.new([enterprise_installation.id])

      { label: enterprise_installation.host_name, unique_count: license_attributer.unique_count, link_url: urls.stafftools_enterprise_installation_path(enterprise_installation) }
    end
  end
end
