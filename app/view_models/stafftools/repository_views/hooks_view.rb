# frozen_string_literal: true

module Stafftools
  module RepositoryViews
    class HooksView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents

      attr_reader :current_context

      def hooks
        @hooks ||= begin
          hooks = current_context.hooks.ordered.all
          Hook::StatusLoader.load_statuses(hook_records: hooks, parent: current_context)
        end
      end

      def webhooks
        hooks.select(&:webhook?)
      end

      def services
        hooks.select(&:legacy_service?)
      end

      def service_hooks_supported?
        current_context.is_a?(::Repository)
      end

      def page_title
        if current_context.is_a?(::Repository)
          "#{current_context.name_with_owner} - Hooks"
        else
          "#{current_context.login} - Hooks"
        end
      end
    end
  end
end
