# frozen_string_literal: true

module Stafftools
  module RepositoryViews
    class AdminView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
      include Stafftools::LargeFilesView::PreviewTogglerHelper
      include Stafftools::Sentry

      class UnknownConfigurationSettingSource < StandardError
      end

      attr_reader :repository, :porter_status

      HEALTH_FILE_INDICATORS = {
        present: { octicon: "check", class: "mr-2 text-green" },
        absent: { octicon: "x", class: "mr-2 text-red"},
      }.freeze

      # Listed in menu-display, value format
      DISABLE_REPO_REASONS = [
        ["sensitive data", "sensitive_data"],
        ["size", "size"],
        ["tos", "tos"],
        ["trademark", "trademark"],
      ].freeze

      def page_title
        "#{repository.name_with_owner} - Admin"
      end

      def show_community_health_metrics?
        GitHub.community_profile_enabled? &&
        CommunityProfile.eligible_repository?(repository)
      end

      def readme_indicator
        community_profile.readme? ? HEALTH_FILE_INDICATORS[:present] : HEALTH_FILE_INDICATORS[:absent]
      end

      def license_indicator
        community_profile.license? ? HEALTH_FILE_INDICATORS[:present] : HEALTH_FILE_INDICATORS[:absent]
      end

      def code_of_conduct_indicator
        community_profile.code_of_conduct? ? HEALTH_FILE_INDICATORS[:present] : HEALTH_FILE_INDICATORS[:absent]
      end

      def contributing_indicator
        community_profile.contributing? ? HEALTH_FILE_INDICATORS[:present] : HEALTH_FILE_INDICATORS[:absent]
      end

      def description_indicator
        community_profile.description? ? HEALTH_FILE_INDICATORS[:present] : HEALTH_FILE_INDICATORS[:absent]
      end

      def pr_or_issue_template_indicator
        community_profile.pr_or_issue_template? ? HEALTH_FILE_INDICATORS[:present] : HEALTH_FILE_INDICATORS[:absent]
      end

      def importer_view
        if GitHub.porter_available?
          @importer_view ||= Stafftools::RepositoryViews::ImporterView.new(repository: repository, porter_status: porter_status)
        else
          nil
        end
      end

      def mirror
        repository.mirror
      end

      def mirror_sentry_link
        sentry_query_link(
          [Stafftools::Sentry::GITHUB_USER_PROJECT_ID],
          "repo_id:#{repository.id}"
        )
      end

      def mirror_timestamp
        @mirror_timestamp ||= GitHub.kv.get("mirror-timestamp:#{repository.id}").value { "Unavailable" }
      end

      def mirror_result
        result = GitHub.kv.get("mirror-result:#{repository.id}").value { "failed" }
        return "succeeded" if result == "success"
        result
      end

      def dmca?
        repository.access.dmca?
      end

      def archived?
        repository.archived?
      end

      def disabled?
        repository.access.disabled?
      end

      def disabled_at
        repository.access.disabled_at
      end

      def disabler_login
        return unless repository_access.disabler
        repository_access.disabler.login
      end

      def disabling_reason
        repository.access.disabling_reason
      end

      def invitation_rate_limit_overridden?
        RepositoryInvitationRateLimitOverride.overridden?(repository)
      end

      def invitation_rate_limit_override_expiration
        RepositoryInvitationRateLimitOverride.override_expiration_string
      end

      # force controls
      def setting_source_detail_text(target)
        if target == GitHub
          "the instance level"
        elsif target.kind_of?(::User)
          "the owner level"
        else
          if target.present?
            # We have no idea where it's coming from, this should only happen
            # when we change cascading and need to add another case here.
            # Note that if target is nil, we're just dealing with the default
            # hard-coded value and the setting is not set anywhere.
            boom = UnknownConfigurationSettingSource.new \
              "Unexpected source for setting. setting_source_detail_text \
              needs to be updated to support #{target.class}"
            boom.set_backtrace(caller)
            Failbot.report!(boom, target: target, fatal: "NO (just reporting)")
          end
          "a higher level"
        end
      end

      def force_rejection_inherited_policy?
        repository.force_push_rejection_inherited? && repository.force_push_rejection_policy?
      end

      def force_detail_text
        case repository.force_push_rejection
        when false
          "Allow"
        when "all"
          "Block"
        when "default"
          "Block on the default branch"
        end
      end

      def force_rejection_choices
        choices = [
          ["Allow", ""],
          ["Block", "all"],
          ["Block to the default branch", "default"],
        ]

        if repository.force_push_rejection_local? && repository.force_push_rejection_default_source
          choices << ["Use default from #{setting_source_detail_text(repository.force_push_rejection_default_source)}", "_clear"]
        elsif repository.force_push_rejection_local?
          # There's no setting that will cascade, so we'll use git's default
          choices << ["Clear and use git’s default (Allow)", "_clear"]
        end

        choices
      end

      def ssh_choices
        choices = [
          ["Enabled", "true"],
          ["Disabled", "false"],
        ]

        if repository.ssh_local? && repository.ssh_default_source
          choices << ["Use default from #{setting_source_detail_text(repository.ssh_default_source)}", "_clear"]
        elsif repository.ssh_local?
          # There's no setting that will cascade, so we'll use git's default
          choices << ["Clear and use default (Enabled)", "_clear"]
        end

        choices
      end

      # Maximum object size controls
      def max_object_size_detail_text
        if repository.max_object_size_inherited? ||
          (repository.max_object_size == ::Repository::DEFAULT_MAX_OBJECT_SIZE && !repository.max_object_size_local?)
          return "Inherited default (#{repository.max_object_size}MB)"
        end

        case repository.max_object_size
        when 0
          "Unlimited"
        when 1000
          "1GB"
        else
          "#{repository.max_object_size}MB"
        end
      end

      def max_object_size_choices
        default_source = if repository.max_object_size_inherited?
          " from #{setting_source_detail_text(repository.max_object_size_source)}"
        end

        choices = [
          ["Inherited default#{default_source}", "_clear"],
          ["1MB", 1],
          ["5MB", 5],
          ["10MB", 10],
          ["25MB", 25],
          ["50MB", 50],
          ["100MB", 100],
          ["150MB", 150],
          ["200MB", 200],
          ["250MB", 250],
          ["300MB", 300],
          ["350MB", 350],
          ["500MB", 500],
          ["750MB", 750],
          ["1GB", 1000],
        ]

        if GitHub.unlimited_max_object_size_enabled?
          choices << ["Unlimited", 0]
        end

        choices
      end

      def disable_repo_reason_choices
        DISABLE_REPO_REASONS
      end

      # Repository disk quota controls

      def disk_quota_detail_text(kind)
        quota = repository.disk_quota(kind: kind)
        case quota
        when 0
          "Unlimited"
        when repository.default_disk_quota(kind: kind)
          "Default (#{quota}GB)"
        else
          "#{quota}GB"
        end
      end

      def disk_quota_choices
        [
          ["10GB", 10],
          ["20GB", 20],
          ["50GB", 50],
          ["75GB", 75],
          ["100GB", 100],
          ["200GB", 200],
          ["Unlimited", 0],
        ]
      end

      def disk_quota_octicon(kind)
        case kind
        when :warn
          "bell"
        when :lock
          "shield-lock"
        end
      end

      # graph controls

      def graph_allowed?
        repository.graph_cache_enabled?
      end

      def graph_button_disabled
        return if graph_allowed?
        "disabled"
      end

      def eventer_powered_insights_graphs?
        GitHub.eventer_enabled?
      end

      def graph_button_text
        graph_allowed? ? "Disable" : "Enable"
      end

      def graph_detail_text
        graph_allowed? ? "enabled" : "disabled"
      end

      # public_push controls
      def public_push_button_text
        repository.public_push? ? "Block" : "Allow"
      end

      # public_push controls
      def public_push_detail_text
        repository.public_push? ? "allowed" : "blocked"
      end

      # Allow changing visibility of public unforked repos, and
      # forks where visibility differs from the root repository.
      def show_permission_toggle?
        return true if repository.public? && !repository.fork? && repository.all_forks_count == 0

        return false unless repository.fork?
        repository.public? != repository.root.public?
      end

      def permission_toggle_type
        repository.public? ? "private" : "public"
      end

      # Does owner's billing plan prevent toggling this repo to private?
      def make_private_blocked_on_plan?
        GitHub.billing_enabled? && repository.public? && !repository.fork? && repository.owner.at_private_repo_limit?
      end

      def transferring?
        repository.pending_transfer?
      end

      def transfer_created_at
        repository.pending_transfer.created_at
      end

      def transfer_requester
        repository.pending_transfer.requester
      end

      def transfer_target
        repository.pending_transfer.target
      end

      def anonymous_access_state
        repository.anonymous_git_access_enabled? ? "enabled" : "disabled"
      end

      def toggled_anonymous_access
        !repository.anonymous_git_access_enabled?
      end

      def toggled_anonymous_access_action
        toggled_anonymous_access ? "Enable" : "Disable"
      end

      def toggled_anonymous_access_action_gerund
        toggled_anonymous_access ? "enabling" : "disabling"
      end

      def anonymous_access_locked?
        repository.anonymous_git_access_locked?
      end

      def anonymous_access_locked_policy?
        repository.anonymous_git_access_locked_policy?
      end

      def anonymous_access_locked_disabled
        anonymous_access_locked_policy? ? "disabled" : ""
      end

      def org_interaction_limits_enabled?
        GitHub.interaction_limits_enabled? &&
        repository.owner.organization? &&
        RepositoryInteractionAbility.has_active_limits?(:organization, repository.owner)
      end

      def pending_access_disable_job?
        repository.disable_access_job_status.in? ["started", "pending"]
      end

      def pending_access_enable_job?
        repository.enable_access_job_status.in? ["started", "pending"]
      end

      private

      def repository_access
        @repository_access ||= repository.access
      end

      def git_lfs_configurable
        repository
      end

      def community_profile
        @community_profile ||= repository.community_profile || CommunityProfile.new
      end
    end
  end
end
