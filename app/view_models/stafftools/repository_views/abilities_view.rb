# frozen_string_literal: true

module Stafftools
  module RepositoryViews
    class AbilitiesView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
      include Stafftools::SecurityHelper
      attr_reader :repository

      def page_title
        "#{repository.name_with_owner} - Abilities"
      end

      # The audit logs for this repo.
      #
      # Returns an Array of Hashes.
      def logs
        @logs ||= query_audit(action: "ability.repository", repo_id: repository.id)
      end

      def group_by_ip?
        true
      end
    end
  end
end
