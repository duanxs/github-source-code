# frozen_string_literal: true

module Stafftools
  module RepositoryViews
    module Search
      class CodesearchView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents

        attr_reader :repository
        attr_reader :search_entry
        attr_reader :document_count

        def initialize(*args)
          super(*args)
          @search_entry = nil

          index = Elastomer::Indexes::CodeSearch.new
          @search_entry = index.repository(repository.id)
          @document_count = index.file_count(repository.id)
        end

        def searchable?
          repository.code_is_searchable?
        end

        def reindex?
          return false unless searchable?
          return true  unless search_entry?
          return true  unless public_match?
          return true  unless up_to_date?
          false
        end

        def reindex_reason
          return "The source code is missing from the search index" unless search_entry?
          return "The public flag has been updated"                 unless public_match?
          return "The source code has been recently updated"        unless up_to_date?
          nil
        end

        def purge?
          !searchable? && search_entry?
        end

        def purge_reason
          return "The repository is un-routed on the file servers"   if repository.route.nil?
          return "The repository owner has been flagged as spammy"   if repository.spammy?
          return "The repository has been disabled"                  if !repository.disabled_at.nil?
          return "The repository does not belong to a valid network" if repository.network.nil?
          return "The repository has no source code"                 if repository.empty?

          if repository.fork?
            return "The repository has no unique commits" if repository.untouched_fork?
            return "The repository has fewer stargazers than the root repository" \
                if !repository.popular_fork?
          end

          nil

        rescue GitHub::DGit::UnroutedError
          return "The repository is offline"
        end

        def fork_eligible_for_enabling_code_search?
          return false if !repository.fork?
          return false if repository.code_is_searchable? # already has code search enabled or is popular
          return false if repository.empty?
          return false if repository.untouched_fork? # no unique code to index
          true
        end

        def fork_ineligible_for_enabling_code_search?
          return false if !repository.fork?
          return false if repository.code_is_searchable? # already has code search enabled or is popular
          return false if fork_eligible_for_enabling_code_search? # meets criteria for enabling code search
          true
        end

        def search_entry?
          !@search_entry.nil?
        end

        def public_match?
          return true unless search_entry?
          repository.public == search_entry["public"]
        end

        def up_to_date?
          return unless search_entry?

          repository.default_branch == search_entry["head"] &&
          repository.default_oid    == search_entry["head_ref"]
        end
      end
    end
  end
end
