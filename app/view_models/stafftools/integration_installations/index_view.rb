# frozen_string_literal: true

class Stafftools::IntegrationInstallations::IndexView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :installations
end
