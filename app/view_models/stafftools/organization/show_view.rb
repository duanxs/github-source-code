# frozen_string_literal: true

module Stafftools
  module Organization
    class ShowView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
      attr_reader :organization

      def show_two_factor_requirement_status?
        GitHub.auth.two_factor_authentication_enabled?
      end

      def two_factor_requirement_audit_log_query
        org_query = organization.audit_log_query
        two_factor_requirement_query = \
          "(action:org.enable_two_factor_requirement OR action:org.disable_two_factor_requirement)"

        "(#{org_query} AND #{two_factor_requirement_query})"
      end

      def two_factor_requirement_link_text
        if organization.two_factor_requirement_enabled?
          "2FA required"
        else
          "2FA not required"
        end
      end

      def saml_sso_octicon
        organization.saml_sso_enabled? || organization.business&.saml_sso_enabled? ? "shield-lock" : "x"
      end

      def saml_sso_enforcement_text
        if organization.business&.saml_sso_enabled?
          "SAML SSO enabled on owning enterprise"
        elsif organization.saml_sso_enforced?
          "SAML SSO enforced"
        elsif organization.saml_sso_enabled?
          "SAML SSO enabled"
        else
          "SAML SSO not enabled"
        end
      end

      def saml_sso_enforcement_link
        if organization.saml_sso_enabled? || organization.business&.saml_sso_enabled?
          helpers.link_to(saml_sso_enforcement_text,
            urls.security_stafftools_user_path(organization, anchor: "saml-settings"))
        else
          saml_sso_enforcement_text
        end
      end

      def terms_of_service_audit_log_path
        query = "org_id:#{organization.id} action:*.update_terms_of_service"
        urls.stafftools_audit_log_path(query: query)
      end

      def ssh_cas_link
        helpers.link_to(ssh_cas_text,
          urls.ssh_certificate_authorities_stafftools_user_path(organization))
      end

      def ssh_cas_text
        n = organization.ssh_certificate_authorities.count
        n = "No" if n == 0
        "#{n} SSH CAs configured"
      end

      def ip_whitelisting_link
        helpers.link_to ip_whitelisting_text, urls.ip_whitelisting_stafftools_user_path(organization)
      end

      def ip_whitelisting_text
        enabled = organization.ip_whitelisting_enabled? ? "enabled" : "disabled"
        "IP allow list #{enabled}"
      end
    end
  end
end
