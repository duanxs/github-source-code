# frozen_string_literal: true

module Stafftools
  module User
    class GistsView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents

      attr_reader :user, :gists, :gist_type

      def page_title
        "#{user.login} - #{page_heading}"
      end

      def page_heading
        if gist_type
          "#{gist_type} gists".capitalize
        else
          "Public gists"
        end
      end

      def gists?
        gists.any?
      end

      def paginatable?
        gists.total_pages > 1
      end

      def visibility(gist)
        gist.visibility
      end

      def li_css(gist)
        css = []
        css << (gist.public? ? "public" : "private")
        css << "fork" if gist.fork?
        css.join(" ")
      end

      def span_symbol(gist)
        gist.public? ? "gist" : "gist-secret"
      end

      def route_classes(gist)
        if fs_offline? gist.route
          "route alert"
        else
          "route"
        end
      end

      private
      def fs_offline?(fs)
        return false if GitHub.enterprise?

        @offline_fileservers ||= GitHub::Stats::Site.dead_storage_servers
        @offline_fileservers.include? fs
      end
    end
  end
end
