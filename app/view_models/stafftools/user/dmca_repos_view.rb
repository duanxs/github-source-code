# frozen_string_literal: true

module Stafftools
  module User
    class DMCAReposView < ReposView

      def has_takedowns?
        Stafftools::DisabledRepositories.dmca_takedowns(user).count > 1
      end

      def first_takedown_date
        Time.at(first_takedown[:created_at] / 1000)
      end

      def first_takedown_nwo
        first_takedown[:repo]
      end

      def page_title
        "#{user.login} - DMCA Takedowns"
      end

      def no_repos_message
        "This user has no currently DMCA disabled repos"
      end

      def span_symbol(repo)
        repo.fork? ? "repo-forked" : "repo"
      end

      def takedown_query
        "user_id:#{user.id} data.reason:dmca action:staff.disable_repo"
      end

      def restore_query
        "user_id:#{user.id} action:staff.enable_repo from:\"stafftools/dmca_takedowns#destroy\""
      end

      def all_events_query
        "(#{takedown_query}) OR (#{restore_query})"
      end

      private

      def repos_of_interest
        user.repositories.where("disabled_at IS NOT NULL AND disabling_reason = 'dmca'").order(:disabled_at)
      end

      def first_takedown
        return @first_takedown if defined? @first_takedown
        @first_takedown = Stafftools::DisabledRepositories.dmca_takedowns(user).last
      end
    end
  end
end
