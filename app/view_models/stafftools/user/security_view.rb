# frozen_string_literal: true

module Stafftools
  module User
    class SecurityView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
      include Stafftools::StatusChecklist

      attr_reader :user
      delegate :two_factor_auth_can_be_disabled?, to: :user

      def business_saml_configured?
        user.organization? && user.business&.saml_sso_enabled?
      end

      def show_linked_saml_identity?
        user.business.present? && user.external_identity.present? && user.external_identity.provider.target == user.business
      end

      def show_team_sync_settings?
        user.business_plus?
      end

      def team_sync_enabled?
        !!tenant&.team_sync_enabled?
      end

      def business_team_sync_enabled?
        user.organization? && user.business&.team_sync_enabled?
      end

      def missing_team_sync_integration?
        user.integration_installations.map(&:integration).none?(&:group_syncer_github_app?)
      end

      def org_with_no_admins?
        user.organization? && user.admins.none?
      end

      def team_sync_provider_type_label
        ::Orgs::SecuritySettings::IndexView::IDENTITY_PROVIDER_OPTIONS[tenant&.provider_type]
      end

      def team_sync_provider_id
        tenant&.provider_id
      end

      def page_title
        "#{user.login} - Security"
      end

      def audit_query
        query = "user_id:#{user.id} OR actor_id:#{user.id}"
        if user.organization?
          "((_exists_:org AND org_id:#{user.id}) OR #{query})"
        else
          "(#{query})"
        end
      end

      def show_legacy_log?
        user.staff_notes.any?
      end

      def two_factor_status
        if user.two_factor_authentication_enabled?
          status_li(true, "2FA enabled")
        else
          status_li(false, "2FA not enabled")
        end
      end

      def two_factor_recovery_status
        if user.two_factor_authentication_enabled?
          if user.two_factor_credential.recovery_codes_viewed?
            status_li(true, "2FA recovery codes viewed")
          else
            status_li(:error, "2FA recovery codes not viewed")
          end
        end
      end

      def two_factor_fallback_status
        return nil unless GitHub.two_factor_sms_enabled?

        if user.two_factor_authentication_enabled?
          if user.two_factor_credential.backup_sms_number
            status_li(true, "Fallback number set")
          else
            status_li(:error, "Fallback number not set")
          end
        end
      end

      def is_recovery_request_enabled?
        !GitHub.enterprise?
      end

      def current_recovery_request?
        current_2fa_recovery_request.present?
      end

      def current_2fa_recovery_request
        return nil if !is_recovery_request_enabled?

        @current_2fa_recovery_request ||= TwoFactorRecoveryRequest.find_for_staff_review(user)
      end

      def recovery_request_verified_details
        return nil unless current_recovery_request?
        return nil if current_2fa_recovery_request.review_state == :evidence_missing

        "#{current_2fa_recovery_request.secondary_evidence_method} '#{current_2fa_recovery_request.secondary_evidence_identifier}'"
      end

      def recovery_request_ready_for_review?
        return false unless current_recovery_request?
        return false if current_2fa_recovery_request.review_state == :incomplete
        return false if current_2fa_recovery_request.review_state == :reviewed_by_staff

        true
      end

      def show_two_factor_recovery_email_list?
        return false unless two_factor_recovery_request_actionable?

        review = TwoFactorRecoveryRequestReview.mget([current_2fa_recovery_request.id])
        manual_email_selection_required = review.empty? ? true : review[current_2fa_recovery_request.id]["type"] == "email_status"

        user.emails.many? && manual_email_selection_required
      end

      # The rejection here is handled in two phases
      # First we reject users.noreply.github.com without checking for generic domains.
      # This prevents us from inaccurately removing a primary or backup as generic, and eliminates the potential
      # for that type of email to show up high in the List
      # Then we drop generics once primary and backup have been procured.
      def two_factor_recovery_emails
        mails = user.emails.primary_first.to_a.reject { |t| t.email =~ /users.noreply.github.com\z/ }
        emails = mails[1]&.backup_role? ? mails.shift(2) : mails.shift(1)

        unless mails.empty?
          mails.reject! { |t| UserEmail.generic_domain?(t.email) }
          verified, unverified = mails.partition(&:verified?)
          emails.concat(verified, unverified)
        end

        emails
      end

      def show_two_factor_sms_numbers?
        GitHub.two_factor_sms_enabled? && user.two_factor_authentication_enabled?
      end

      def two_factor_type
        if user.two_factor_authentication_enabled?
          user.two_factor_credential.delivery_method_display
        else
          "disabled"
        end
      end

      def two_factor_sms?
        user.two_factor_credential.configured_with? :sms
      end

      def fallback_number
        user.two_factor_credential.backup_sms_number || "none"
      end

      def fallback_number?
        user.two_factor_credential.backup_sms_number.present?
      end

      # Does this user explicitly have an SMS provider set?
      #
      # Returns a Boolean
      def sms_provider_set?
        user.two_factor_credential.provider.present?
      end

      # The SMS provider that will be used to send SMS messages to this user.
      #
      # Returns a String
      def sms_provider
        if user.two_factor_credential.provider
          user.two_factor_credential.provider
        else
          GitHub::SMS.providers_for_env.first.provider_name.to_s
        end
      end

      # The "other" SMS provider that is not currently being used to send SMS
      # messages to this user.
      #
      # Returns a String
      def other_sms_provider
        if GitHub::SMS.providers_for_env.first.provider_name.to_s == sms_provider
          GitHub::SMS.providers_for_env.last.provider_name.to_s
        else
          GitHub::SMS.providers_for_env.first.provider_name.to_s
        end
      end

      def security_keys
        user.u2f_registrations
      end

      def sessions?
        sessions.any?
      end

      def grouped_sessions
        sessions.group_by { |session| session.session.state }
      end

      def sessions_with_locked_ip
        sessions.uniq { |s| s.ip }.select { |s| AuthenticationLimit.at_any?(web_ip: s.ip) }
      end

      def expiration(data)
        AuthenticationLimit.longest_expiration(data)
      end

      def show_org_application_policy?
        GitHub.oauth_application_policies_enabled? && user.organization?
      end

      def org_restricts_oauth_applications?
        user.organization? && user.restricts_oauth_applications?
      end

      def org_approved_applications
        user.oauth_application_approvals.approved.includes(application: :user)
      end

      def org_applications_pending_approval
        user.oauth_application_approvals.pending_approval.includes(application: :user)
      end

      def org_denied_applications
        user.oauth_application_approvals.denied.includes(application: :user)
      end

      def application_access_policy_audit_query
        return nil unless user.organization?

        audit_query + " AND action:org.*oauth_app*"
      end

      def show_password_randomization?
        return false if GitHub.auth.external?
        return false unless user.user?
        true
      end

      def show_saml_provider?
        user.organization? && (user.saml_sso_enabled? || user.business&.saml_sso_enabled?)
      end

      def show_external_authentication_attributes?
        external_mapping.present?
      end

      def external_authentication_attributes
        return {} unless external_mapping

        case external_mapping
        when SamlMapping
          {
            "NameID" => external_mapping.name_id,
            "NameID format" => external_mapping.name_id_format,
          }
        when CasMapping
          {
            "CAS username" => external_mapping.username,
          }
        when LdapMapping
          {
            "Distinguished name (DN)" => external_mapping.dn,
          }
        end
      end

      def name_id
        external_mapping&.name_id
      end

      def show_name_id_edit?
        show_external_authentication_attributes? && external_mapping.is_a?(SamlMapping)
      end

      def last_admin_of_orgs
        user.affiliated_organizations_with_two_factor_requirement.select do |org|
          org.last_admin?(user)
        end
      end

      def businesses_with_2fa_req
        user.affiliated_businesses_with_two_factor_requirement
      end

      def two_factor_recovery_request_actionable?
        recovery_request_ready_for_review? && !last_admin_of_orgs.present? && !businesses_with_2fa_req.present?
      end

      private

      def external_mapping
        GitHub.auth.external_mapping(user)
      end

      def tenant
        user.try(:team_sync_tenant)
      end

      def sessions
        @sessions ||= user.sessions.map { |s| Session.new s }
      end

      class Session
        attr_accessor :session
        delegate :state, :id, :created_at, :ip, :time_zone_name, to: :session

        def initialize(session = nil)
          @session = session
        end

        def os
          return nil unless session.ua
          [session.ua.platform.name, session.ua.platform.version].join(" ")
        end

        def mobile?
          return false unless session.ua
          !!session.ua.device.mobile?
        end

        def short_os
          return "Unknown" unless session.ua
          case os
          when /iPhone/;        "iOS"
          when /Macintosh/;     "OSX"
          when /Android/;       "Android"
          when "Windows 8";     "Win8"
          when "Windows 7";     "Win7"
          when "Windows Vista"; "Vista"
          when /Windows XP/;    "WinXP"
          when /Windows/;       "Win"
          when /Linux/;         "Linux"
          else;                 os
          end
        end

        def details
          {
            :ID => session.id,
            :State => session.state,
            :RevokedReason => session.revoked_reason,
            :Impersonated => session.impersonated? ? session.impersonator.login : false,
            :Created => session.created_at,
            "Last accessed" => session.accessed_at,
            :Browser => session.ua&.name || "Unknown",
            :OS => os || "Unknown",
            :Mobile => mobile?,
            "User Agent" => session.user_agent,
            :IP => session.ip,
            :TimeZone => session.time_zone_name,
            :Geolocation => session.location.inspect,
          }
        end
      end
    end
  end
end
