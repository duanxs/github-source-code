# frozen_string_literal: true

module Stafftools
  module User
    class AbilitiesView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
      attr_reader :user, :grants

      def page_title
        "#{user.login} - Abilities"
      end
    end
  end
end
