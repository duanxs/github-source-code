# frozen_string_literal: true

module Stafftools
  module User
    class ShowView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
      include ActionView::Helpers::DateHelper
      include Stafftools::Features
      include Stafftools::Sentry
      include Stafftools::Halp
      include Stafftools::Education
      include Stafftools::StatusChecklist
      include Stafftools::Zendesk
      include ActionView::Helpers::NumberHelper
      include StafftoolsHelper

      attr_reader :user

      def page_title
        "#{user.login} - Overview"
      end

      def show_private_contribution_count?
        profile_settings = user.profile_settings
        profile_settings.show_private_contribution_count?
      end

      def account_type_title
        user.class.to_s
      end

      def show_user_notes?
        staff_note_dormancy ||
            dupe_login ||
            dupe_email ||
            no_primary_email ||
            disabled_repos ||
            blacklisted_login
      end

      def show_collab_section?
        return false if user.organization?

        user.member_repositories.exists? || show_recent_comments?
      end

      def show_recent_comments?
        return false if user.organization?

        user.issue_comments.exists? || user.commit_comments.exists? || user.issues.exists?
      end

      def member_repository_count
        user.member_repositories.count
      end

      def dormant_title
        user.dormant? ? "Dormant" : "Active"
      end

      def dormancy_time_period_in_words
        time_period = time_ago_in_words(GitHub.dormancy_threshold.ago).match(/\d.*/)[0]
        time_period == "1 month" ? "month" : time_period
      end

      def show_dormant_indicator?
        GitHub.enterprise? && !user.suspended?
      end

      def staff_note_dormancy
        if !GitHub.enterprise? && user.recent_staff_note?
          status_li(:recent_note, "Recent staff note")
        end
      end

      def dupe_login
        if user.has_duplicate_login?
          num = helpers.pluralize(user.duplicate_login_count, "other user")
          status_li(:error, "Has duplicate login",
                    "This user shares a login with #{num}.")
        end
      end

      def dupe_email
        if user.has_duplicate_email?
          num = helpers.pluralize(user.duplicate_email_count, "other e-mail record")
          status_li(:error, "Has duplicate e-mail",
                    "This user shares an e-mail with #{num}.")
        end
      end

      def no_primary_email
        return unless user.user?
        unless user.has_primary_email?
          status_li(:error, "Has no primary email",
                    "This user has not selected a primary email address.")
        end
      end

      def disabled_repos
        repos = user.repositories.where("disabled_at IS NOT NULL")
        unless repos.empty?
          names = repos.map { |repo| repo.name }.join(", ")
          status_li(:error, "Has disabled repositories", "Disabled repositories: #{names}")
        end
      end

      def blacklisted_login
        status_li(:error, "Has blacklisted login") if user.login_blacklisted?
      end

      def created_at
        user.created_at.to_s
      end

      def developer_program_member
        user.developer_program_member? ? "Yes" : "No"
      end

      def prerelease_agreement?
        !!prerelease_agreement
      end

      def prerelease_agreement
        @prerelease_agreement ||= user.prerelease_agreement
      end

      def total_disk_usage
        total = git_disk_bytes +
            avatar_disk_bytes +
            user_asset_disk_bytes
        number_to_human_size(total)
      end

      def git_disk_usage
        number_to_human_size(git_disk_bytes)
      end

      def avatar_disk_usage
        number_to_human_size(avatar_disk_bytes)
      end

      def user_asset_disk_usage
        number_to_human_size(user_asset_disk_bytes)
      end

      def avatar_disk_bytes
        @avatar_disk_bytes ||= Avatar.storage_disk_usage(owner_type: "User", owner_id: user.id)
      end

      def user_asset_disk_bytes
        @user_asset_disk_bytes ||= UserAsset.storage_disk_usage(user_id: user.id)
      end

      def git_disk_bytes
        # number_to_human_size expects bytes, but disk_usage is reported in
        # kilobytes, so convert it first.
        @git_disk_bytes ||= user.disk_usage * 1024
      end

      def clear_log_disabled?
        e = Stratocaster::Timeline.new(user.events_key(type: :actor_public)).events
        e.empty?
      end

      def staff_notes?
        !staff_notes.empty?
      end

      def staff_notes
        @staff_notes ||= begin
          github = ::User.find_by_login("github")
          conditions = github ? ["user_id <> ?", github.id] : []
          user.staff_notes.where(conditions).order("created_at DESC").to_a
        end
      end

      def new_note
        user.staff_notes.new
      end

      def business_plus_flag
        if user.organization?
          GitHub::Plan.business_plus.titleized_display_name
        else
          helpers.link_to(
              GitHub::Plan.business_plus.titleized_display_name,
              urls.stafftools_user_organization_memberships_path(user, anchor: "enterprise-memberships"))
        end
      end

      def show_business_plus_flag?
        !GitHub.single_business_environment? && user.business_plus?
      end

      def show_owning_business?
        !GitHub.single_business_environment? &&
            user.organization? &&
            user.business
      end

      def ssh_keys
        valid_keys = user.public_keys.to_a.count { |k| k.can_verify_account_ownership? }
        if valid_keys > 0
          icon = helpers.octicon("check", class: "success")
          msg = "#{helpers.pluralize(valid_keys, 'SSH key')}"
        else
          icon = helpers.octicon("x", class: "alert")
          msg = "None of #{helpers.pluralize(user.public_keys.count, 'SSH key')}"
        end
        ssh_keys_link = helpers.link_to(msg, urls.keys_stafftools_user_path(user))
        helpers.safe_join([icon, ssh_keys_link, "can be used for account recovery."], " ")
      end

      def is_recovery_request_enabled?
        !GitHub.enterprise?
      end

      def staff_recovery_review_text
        return nil if !current_recovery_request?

        return "Account recovery request declined" if current_2fa_recovery_request.declined_at.present?
        return "Account recovery request approved" if current_2fa_recovery_request.approved_at.present?

        "Account recovery request pending"
      end

      def current_recovery_request?
        current_2fa_recovery_request.present?
      end

      def current_2fa_recovery_request
        return nil unless is_recovery_request_enabled?

        @current_2fa_recovery_request ||= TwoFactorRecoveryRequest.find_for_staff_review(user)
      end

      def sentry_link
        sentry_query_link(
          [Stafftools::Sentry::DOTCOM_PROJECT_ID],
          "user.username:#{user.name}"
        )
      end

      def halp_user_url
        halp_search_url("login:#{user.name}")
      end

      def halp_email_url
        halp_search_url("from_email:#{user.email}")
      end

      def halp_user_new_discussion_url
        halp_new_discussion_url(user.email_for_halp)
      end

      def halp_org_new_billing_discussion_url
        halp_new_discussion_url(user.billing_email_for_halp)
      end

      def zendesk_user_url
        zendesk_search_url(user.name)
      end


      def zendesk_email_url
        zendesk_search_url("email:#{user.email}")
      end

      def education_app_user_url
        education_search_url(user.id)
      end

      def company_name
        user.company.try(:name) || "None"
      end

      def vertical
        if user.vertical
          user.vertical
        else
          "None"
        end
      end

      def to_date(timestamp)
        if timestamp.blank?
          "None"
        else
          timestamp.to_date
        end
      end

      def default_repository_permission
        return unless user.organization?
        case user.default_repository_permission
        when :admin
          "Admin — Members of this organization can clone, pull, push, and add new collaborators to all repositories."
        when :write
          "Write — Members of this organization can clone, pull, and push all repositories."
        when :read
          "Read — Members of this organization can clone and pull all repositories."
        else
          "None — Members can only clone and pull public repositories."
        end
      end

      # Public: Should we show the button to force the corporate ToS upgrade prompt?
      #
      # Returns a Boolean.
      def show_corporate_tos_prompt_toggle?
        return false if GitHub.enterprise?
        return false unless user.organization?

        terms_of_service = user.terms_of_service
        user.organization? && !terms_of_service.business_terms_of_service? && !terms_of_service.esa_education?
      end

      # Public: Should we show the button to force the ESA+Education ToS upgrade prompt?
      #
      # Returns a Boolean.
      def show_esa_education_tos_prompt_toggle?
        return false if GitHub.enterprise?
        return false unless user.organization?

        user.organization? && !user.terms_of_service.esa_education?
      end
    end
  end
end
