# frozen_string_literal: true

module Stafftools
  module User
    class GistView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
      include Stafftools::Haystack
      include Stafftools::StatusChecklist

      attr_reader :gist

      delegate :user_param, :user, :parent, :fork?, to: :gist

      def page_title
        "Overview for Gist #{gist.name_with_owner}"
      end

      def name
        gist.name_with_owner
      end

      def haystack_link
        haystack_query_link "gist_repo_name:#{gist.repo_name}"
      end

      def dmca?
        return false unless dmca_and_country_blocking_enabled?

        gist.access.dmca?
      end

      def country_blocks?
        return false unless dmca_and_country_blocking_enabled?

        gist.country_blocks && gist.country_blocks.any?
      end

      def country_blocks_sentence
        gist.country_blocks.map { |reason, url| reason }.to_sentence
      end

      def dmca_and_country_blocking_enabled?
        return false if GitHub.enterprise?

        !archived? && gist.active? && gist.user
      end

      def archived?
        gist.is_a?(Archived::Gist)
      end

      def status_message
        if archived?
          "This gist has been archived."
        elsif !gist.active?
          "This gist is queued for archival."
        else
          nil
        end
      end

      def link_to_source?
        !archived? && gist.active?
      end

      def created_at
        gist.created_at.in_time_zone
      end

      def last_push
        if gist.pushed_at.nil?
          "No pushes"
        else
          gist.pushed_at.in_time_zone
        end
      end

      def last_maintenance_at
        gist.last_maintenance_at.try(:in_time_zone) || "never"
      end

      def last_maintenance_status
        gist.maintenance_status.to_s
      end

      def marked_broken?
        gist.maintenance_status == "broken"
      end

      def db_state
        git_repo_db_state gist
      end

      def git_state
        git_repo_git_state gist
      end

      def fs_state
        git_repo_fs_state gist
      end

      # Query to find this Gist in the audit log
      def audit_query
        "data.gist_repo_name:#{gist.repo_name}"
      end
    end
  end
end
