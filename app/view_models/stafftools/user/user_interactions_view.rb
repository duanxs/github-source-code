# frozen_string_literal: true

module Stafftools
  module User
    class UserInteractionsView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
      attr_reader :user

      def blocking_users
        user.ignored_by
      end

      def blocked_users
        user.ignored
      end

      def followed_users
        user.following
      end
    end
  end
end
