# frozen_string_literal: true

class Stafftools::Abilities::ParticipantView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :participant, :grants

  def abilities
    participant.abilities
  end

  def actor?
    participant.actor?
  end

  def description
    participant.ability_description
  end

  def subject?
    participant.subject?
  end
end
