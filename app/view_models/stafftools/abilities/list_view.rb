# frozen_string_literal: true

class Stafftools::Abilities::ListView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :abilities
  attr_reader :only
  attr_reader :title

  def as
    only == :subject ? :actor : :subject
  end

  def sorted_abilities
    abilities.sort_by &:created_at
  end
end
