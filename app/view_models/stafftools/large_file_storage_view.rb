# frozen_string_literal: true
module Stafftools
  class LargeFileStorageView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    include PlatformHelper
    include ActionView::Helpers::NumberHelper

    LfsQuery = parse_query <<-'GRAPHQL'
      fragment on Node {
        ... on Organization {
          login
          stafftoolsInfo {
            lfsRepositories(first: $first, after: $after, last: $last, before: $before) {
              nodes {
                nameWithOwner
                stafftoolsInfo {
                  networkLfsDiskUsage
                }
              }
            }
          }
        }
        ... on User {
          login
          stafftoolsInfo {
            lfsRepositories(first: $first, after: $after, last: $last, before: $before) {
              nodes {
                nameWithOwner
                stafftoolsInfo {
                  networkLfsDiskUsage
                }
              }
            }
          }
        }
      }
    GRAPHQL

    def after_initialize
      @account = LfsQuery.new(@account)
    end

    attr_reader :account, :actor_statuses

    # A formatted JSON hash of repositories and the LFS storage used by them,
    # for sending to users.
    #
    # Returns a Hash.
    def lfs_repos_json
      hash = {}
      nodes = account.stafftools_info.lfs_repositories.nodes

      nodes.each do |node|
        disk_usage = number_to_human_size(node.stafftools_info.network_lfs_disk_usage)
        hash[node.name_with_owner] = disk_usage
      end

      JSON.pretty_generate(hash)
    end

    # A formatted JSON hash of actors and the LFS storage used by them,
    # for sending to users.
    #
    # Returns a Hash.
    def actor_status_json
      hash = {}
      actor_statuses.each do |status|
        login = case
        when status.anonymous_actor?
          "Anonymous"
        when status.user_actor? && status.actor.nil?
          "Deleted user: #{status.actor_id}"
        when status.user_actor?
          status.actor.login
        when status.key_actor? && status.key.nil?
          "Deleted key: #{status.key_id}"
        when status.key_actor? && status.key&.repository
          "Deploy key: #{status.key.repository.nwo}"
        when status.key_actor?
          # This should never be reached, but it's here for completeness.
          "Public key: #{status.key_id}"
        else
          "Unknown actor"
        end

        hash[login] = { bandwidth_down: status.bandwidth_down.round(2), bandwidth_up: status.bandwidth_up.round(2) }
      end

      JSON.pretty_generate(hash)
    end
  end
end
