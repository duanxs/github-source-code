# frozen_string_literal: true

class IntegrationInstallations::SuggestionsView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :target, :integration, :installation, :installation_request
  attr_reader :session
  attr_reader :query
  attr_reader :edit_installed_repositories
  attr_reader :exclude, :skip_installed_on_check
  attr_reader :referer

  delegate :came_from_marketplace?, :marketplace_listing_id, :logo_background_color_style_rule,
           to: :installation_view

  def not_actionable_reason
    check = if !installable?
      # Special case contact email requirement because the Permissions check doesn't cover it.
      integration.installable_on_by(target: target, actor: current_user)
    else
      integration.requestable_on_by(target: target, actor: current_user)
    end

    check.reason
  end

  def installable?
    return @installable if defined?(@installable)
    @installable = integration.installable_on_by?(target: target, actor: current_user)
  end

  def cancel_path
    if came_from_marketplace?
      referer || urls.marketplace_path
    else
      # In the event the user doesn't have any organizations
      # this would have redirected them back to the same page.
      #
      # This will instead take the user to the app homepage.
      #
      # See https://github.com/github/ecosystem-apps/issues/554
      if logged_in? && current_user.organizations.none?
        urls.gh_app_path(integration)
      else
        urls.gh_new_app_installation_path(integration)
      end
    end
  end

  def requestable?
    integration.requestable_on_by?(target: target, actor: current_user)
  end

  def repository_installation_required?
    integration.repository_installation_required?(target)
  end

  # Requestable repositories are those the User do not admin but have access to via an owning Organization
  def requestable_repository_ids
    @requestable_repository_ids ||= integration.requestable_repository_ids_on_by(target: target, actor: current_user, exclude_requested: false)
  end

  def requestable_repositories?
    requestable_repository_ids.any?
  end

  def installable_repository_ids
    @installable_repository_ids ||= integration.installable_repository_ids_on_by(target: target, actor: current_user, exclude_installed: false)
  end

  def installable_repositories?
    installable_repository_ids.any?
  end

  def add_email_form_url
    target.organization? ? urls.organization_emails_path(target) : urls.user_emails_path(target)
  end

  def suggestable_repositories
    Repository.where(id: requestable_repository_ids + installable_repository_ids)
  end

  def suggestions(limit: 100)
    return initial_suggestions unless query

    @suggestions ||= begin
      scope = suggestable_repositories

      if excluded_repository_ids.any?
        scope = scope.where("repositories.id NOT IN (?)", excluded_repository_ids)
      end

      # enforce a maximum cap
      limit = 100 if limit > 100

      exact_match = scope.where("repositories.name = :query", query: query).first

      scope = scope.where("repositories.name like :query", query: "%#{ActiveRecord::Base.sanitize_sql_like(query)}%")
      scope = scope.select([:id, :name, :owner_id, :parent_id, :public, :description, :source_id, :deleted])
      scope = scope.order("repositories.name").limit(limit)

      scope.to_a.prepend(exact_match).uniq.compact
    end
  end

  def initial_suggestions
    @initial_suggestions ||= begin
      scope = suggestable_repositories

      scope.recently_updated.limit(20)
    end
  end

  def suggestions?
    suggestions.any?
  end

  def initial_repository_ids
    return [] if installation && installation.installed_on_all_repositories?
    ids = []
    ids.concat(installation.repository_ids) if installation
    ids.concat(installation_request.repository_ids) if installation_request
    ids
  end

  def excluded_repository_ids
    Array.wrap(exclude)
  end

  def suggestion_label(repo)
    return if target == current_user
    case
    when installed_on?(repo)
      :installed
    when suggestions_permissions[repo.id] == :admin
      :request unless installable_repository?(repo)
    when suggestions_permissions[repo.id] == :read
      :request
    end
  end

  # Public: Whether the integration has been installed on the given repository.
  #
  # If the integration is installed on all repositories and not directly on the
  # given repository, returns false.
  #
  # Returns a Boolean.
  def installed_on?(repo)
    return false if skip_installed_on_check
    return false unless installation.present?
    return false if installation.installed_on_all_repositories?
    installation.repository_ids.include?(repo.id)
  end

  def events
    return @events if defined?(@events)

    @events = []
    return @events if integration.hook.nil?

    @events = integration.hook.events.map do |event|
      Hook::EventRegistry.for_event_type(event).display_name
    end.sort
  end

  def can_target_all_repositories?
    can_install_on_all_repositories? || can_request_on_all_repositories?
  end

  def can_install_on_all_repositories?
    integration.installable_on_all_repositories_by?(target: target, actor: current_user)
  end

  def can_request_on_all_repositories?
    integration.requestable_on_by?(target: target, actor: current_user)
  end

  # Whether this install target radio input should be checked by default
  # as specified by the installation request.
  #
  # Defaults to false.
  def install_target_radio_checked?(install_target)
    # checks if an installation exists with selected repos
    return install_target == :selected if installation_with_selected_repositories? && !installation_request_available?
    # if no installation_request is available, defaults to :all
    return install_target == :all unless installation_request_available?

    case install_target
    when :all
      installation_request.request_all_repositories?
    when :selected
      installation_request.request_some_repositories?
    when :none
      installation_request.request_no_repositories?
    end
  end

  def paid_marketplace_plan_purchased?
    installation_view.paid_marketplace_plan_purchased_by?(target)
  end

  def target_noun
    target.organization? ? "Organization" : "Account"
  end

  def installation_request_available?
    installation_request.present? && (installable? || installation_request.ephemeral? && requestable?)
  end

  def installation_suggestion_available?
    installation_request&.ephemeral?
  end

  def caption
    available_actions = []
    available_actions << "Approve" if installation_request_available?
    available_actions << "Install" if installable?
    available_actions << "Authorize" if integration&.can_request_oauth_on_install?
    available_actions << "Request" if requestable?
    available_actions.to_sentence(two_words_connector: " & ", last_word_connector: ", & ")
  end

  def disabled_caption
    available_actions = []
    available_actions << "Approving"   if installation_request_available?
    available_actions << "Installing"  if installable?
    available_actions << "Authorizing" if integration&.can_request_oauth_on_install?
    available_actions << "Requesting"  if requestable?
    available_actions.join(" & ")
  end

  def installation_requester_name
    if installation_request.ephemeral?
      installation_request.integration.name
    else
      "@#{installation_request.requester.login}"
    end
  end

  def installation_request_verb
    return "requested" unless installation_request
    installation_request.ephemeral? ? "suggested" : "requested"
  end

  def installation_requester_url
    if installation_request.ephemeral?
      urls.gh_app_path(installation_request.integration)
    else
      urls.user_path(installation_request.requester)
    end
  end

  def label_title(text = nil)
    case (text ||= installation_request_verb)
    when "suggested"
      "Suggested for installation"
    when "request"
      "Requesting installation"
    else
      "Selected for installation"
    end
  end

  def installation_requested_at
    return Time.zone.now if installation_request.ephemeral?
    installation_request.created_at
  end

  def edit_installed_repositories?
    edit_installed_repositories && !installation.installed_on_all_repositories?
  end

  def installed_repositories
    return @installed_repositories if defined?(@installed_repositories)

    scope = if target.adminable_by?(current_user)
      installation.repositories
    else
      Repository.with_ids(current_user.associated_repository_ids(min_action: :admin, repository_ids: installation.repository_ids))
    end

    @installed_repositories = scope.includes(:owner, :parent).select(:id, :name, :owner_id, :parent_id, :public, :source_id)
  end

  def installation_with_selected_repositories?
    installation.present? && installation.repository_installation_required? && !installation.installed_on_all_repositories?
  end

  def account_type
    case target
    when Business
      "enterprise account"
    when Organization
      "organization"
    else
      "personal account"
    end
  end

  def multi_user_install?
    target != current_user
  end

  private

  def installable_repository?(repo)
    integration.installable_on_by?(target: target, actor: current_user, repositories: [repo])
  end

  def installation_view
    InstallationView.new(integratable: integration, session: session, current_user: current_user)
  end

  # Internal: Compute permissions on suggestions.
  #
  # Public repositories are assumed to have `:read` permission.
  #
  # Returns a Hash{repo_id Int => action Symbol}.
  def suggestions_permissions
    @suggestions_permissions ||=
      begin
        suggested_repo_ids = suggestions.pluck(:id)

        # establish readable public repositories
        perms = target.repositories.public_scope.
          where(id: suggested_repo_ids).pluck(:id).
          reduce({}) do |perms, id|
            perms[id] = :read
            perms
          end

        # determine action on private repositories
        repo_abilities = Authorization.service.most_capable_collaborator_abilities_from_actor(
          actor: current_user,
          subject_type: Repository,
          subject_ids: suggested_repo_ids,
        )

        repo_abilities.each do |ability|
          perms[ability.subject_id] =
            if ability.can?(:admin)
              :admin
            elsif ability.can?(:read)
              :read
            end
        end

        # override less permission or populate perms with :admin
        # for repos the user has admin on via repo owning org adminship
        Repository.accessible_via_org_admin(current_user, suggested_repo_ids).each do |repo_id|
          perms[repo_id] = :admin
        end

        perms
      end
  end
end
