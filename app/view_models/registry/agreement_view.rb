# frozen_string_literal: true

module Registry
  class AgreementView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :membership, :adminable_accounts, :selected_account

    def selected?(member)
      selected_account == member
    end

    def member_type(member)
      member&.is_a?(Business) ? "Business" : "User"
    end

    def target_member_description
      return unless membership&.member

      if membership.member.is_a?(Business)
        "your enterprise #{membership.member.name}"
      elsif membership.member.organization?
        "your organization @#{membership.member.login}"
      else
        "your personal account"
      end
    end
  end
end
