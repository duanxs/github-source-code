# frozen_string_literal: true

module Registry
  module Packages
    class DownloadGraphView
      attr_reader :height, :name

      def initialize(data:, height:, name:)
        @data = data
        @height = height
        @name = name
        @scale = scale(@data)
      end

      def weeks
        @weeks ||= @data.map do |week|
          week * @scale
        end
      end

      def scale(data)
        max = data.max || 0

        if max > @height
          @height.to_f / max
        else
          1
        end
      end
    end
  end
end
