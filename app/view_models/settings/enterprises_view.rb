# frozen_string_literal: true

module Settings
  class EnterprisesView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents

    # Public: Return a Membership for each associated Business
    #
    # Returns an Array of memberships.
    def memberships
      current_user.businesses.map { |business|
        Membership.new(current_user, business)
      }
    end

    # Provides a membership description and delete confirmation method for a
    # user and organization assocation as well as the organization, number of
    # direct repo memberships in the organization, and organization name.
    class Membership
      include GitHub::Application.routes.url_helpers

      def initialize(user, enterprise, role: nil)
        @user = user
        @enterprise = enterprise
        @name = enterprise.name
        @role = role || determine_role
      end

      attr_reader :enterprise, :name, :user, :role

      def description
        case role
        when :owner
          "owner"
        when :member
          "member"
        when :billing_manager
          "billing manager"
        end
      end

      def show_settings_button?
        role == :owner || role == :billing_manager
      end

      private

      def determine_role
        return :owner if enterprise.owner?(user)
        return :billing_manager if enterprise.billing_manager?(user)
        return :member
      end
    end
  end
end
