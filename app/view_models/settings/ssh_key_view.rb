# frozen_string_literal: true

module Settings
  class SshKeyView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    include CredentialAuthorizationMethods

    attr_reader :ssh_key, :selected, :object, :sso_organizations, :credential_map

    def credential
      ssh_key
    end

    def return_to_path
      urls.settings_user_keys_path
    end

    def permissions_text(ssh_key)
      " — " + (ssh_key.read_only? ? "Read-only" : "Read/write")
    end

    def sso_subjects_label
      "keys"
    end

    def sso_help_path
      "/articles/authorizing-an-ssh-key-for-use-with-a-saml-single-sign-on-organization/"
    end
  end
end
