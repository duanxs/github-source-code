# frozen_string_literal: true

class Settings::DeletedRepositoriesView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :target, :deleted_repos

  def filtered_list
    return @deleted_repos if @deleted_repos.empty?
    @deleted_repos.reject { |repo| !repo.safe_to_restore || deleted_by_staff(repo) || matching_advisories.include?(repo.id) }
  end

  def disclaimer_text
    "It may take up to an hour for repositories to be displayed here. You can only restore repositories that have no forks or have not been forked."
  end

  def help_url
    "#{GitHub.help_url}/articles/restoring-a-deleted-repository"
  end

  private

  # In this situation if it's your repo, and you deleted it then you should see it, staff or not.
  # Otherwise if a staff member deleted your repo, then we should not display it as
  # repo removal by staff should only occur for abuse based reasons and we do not want it to be restorable
  def deleted_by_staff(repo)
    return false if staff_safe(repo)
    repo.deleted_by_staff?
  end

  def staff_safe(repo)
    target_is_organization? ? current_user.staff? : repo.deleted_by == current_user
  end

  def target_is_organization?
    @target.is_a?(Organization)
  end

  def matching_advisories
    unless defined? @matching_advisories
      ids = @deleted_repos.collect { |repo| repo.id }
      @matching_advisories = RepositoryAdvisory.where(workspace_repository_id: ids).find_each.map(&:workspace_repository_id)
    end

    @matching_advisories
  end
end
