# frozen_string_literal: true

class Settings::Organization::Roles::EditView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  include Orgs::RolesHelper
  include Orgs::CustomRoleFormHelper

  include GitHub::Application.routes.url_helpers

  attr_reader :base_role_fgps, :organization, :role

  delegate :base_role, :title_for, :icon_for, to: :base_role_fgps

  def role_user_count
    count = role.user_count
    "#{count} #{'person'.pluralize(count)}"
  end

  def role_org_member_count
    count = role.org_member_count
    "#{count} #{'org member'.pluralize(count)}"
  end

  def role_collaborator_count
    count = role.collaborator_count
    "#{count} #{'outside collaborator'.pluralize(count)}"
  end

  def role_team_count
    count = role.team_count
    "#{count} #{'team'.pluralize(count)}"
  end

  def role_invitation_count
    count = RepositoryInvitation.where(role_id: role.id).count
    "#{count} #{'invitation'.pluralize(count)}"
  end

  # do we need to inform the possibility of the org default role
  # being assigned to org members?
  def show_org_base_role_warning?
    role.org_member_count > 0 && (org_base_role != :none && org_base_role != :read)
  end
end
