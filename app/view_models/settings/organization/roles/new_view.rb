# frozen_string_literal: true

class Settings::Organization::Roles::NewView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  include Orgs::CustomRoleFormHelper

  include GitHub::Application.routes.url_helpers

  attr_reader :base_role_fgps, :organization, :role

  delegate :base_role, :title_for, :icon_for, to: :base_role_fgps
end
