# frozen_string_literal: true

class Settings::Organization::Roles::IndexView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  include Orgs::RolesHelper
  attr_reader :organization

  def is_disabled?(role)
    return false if org_base_role.equal?(:none)
    role.action_rank < Ability::ACTION_RANKING[org_base_role]
  end

  def custom_role_message
    return "You have reached the limit(#{org_role_limit}) of total allowed custom roles." if is_org_role_limit_max?
    return "Your organization base role already has the highest permission" if org_base_role_admin?
    "Create a role by inheriting a pre-defined role and adding permissions to it."
  end

  def is_org_role_limit_max?
    Role.custom_roles_for_org(organization).count >= org_role_limit
  end

  def org_role_limit
    return @org_role_limit if defined?(@org_role_limit)
    @org_role_limit = Role.custom_role_limit_for_org(organization)
  end

  def org_base_role_admin?
    org_base_role.equal?("admin".to_sym)
  end

  def delete_organisation_repository_role_path(role)
    urls.delete_settings_org_repository_roles_path(organization, role.id)
  end

  def role_user_count(role)
    count = role.user_count
    "#{count} #{'person'.pluralize(count)}"
  end

  def role_org_member_count(role)
    count = role.org_member_count
    "#{count} #{'org member'.pluralize(count)}"
  end

  def role_collaborator_count(role)
    count = role.collaborator_count
    "#{count} #{'outside collaborator'.pluralize(count)}"
  end

  def role_team_count(role)
    count = role.team_count
    "#{count} #{'team'.pluralize(count)}"
  end

  def role_invitation_count(role)
    count = RepositoryInvitation.where(role_id: role.id).count
    "#{count} #{'invitation'.pluralize(count)}"
  end
end
