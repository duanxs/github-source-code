# frozen_string_literal: true

module AdvisoryDB
  class AtomView
    attr_reader :view

    def initialize(view)
      @view = view
      @updated_after = 14.days.ago.beginning_of_day
    end

    def cache_key
      "feeds:%s:%s:%s" % [self_url.to_md5, most_recently_updated.ghsa_id, most_recently_updated.updated_at.to_i]
    end

    def feed_id
      ["tag:", GitHub.host_name, ",2008:", view.security_advisory_feed_path].join
    end

    def feed_title
      "GitHub Security Advisory Feed"
    end

    def feed_author
      OpenStruct.new(name: "GitHub")
    end

    def self_url
      view.security_advisory_feed_url(format: :atom)
    end

    def feed_updated_at
      most_recently_updated.updated_at.utc.iso8601
    end

    def advisories
      @advisories ||= disclosed_advisories.map do |advisory|
        advisory = AtomAdvisory.new(advisory)
        advisory.valid? ? advisory : nil
      end.compact
    end

    private

    def most_recently_updated
      @most_recently_updated ||= disclosed_advisories.first || null_advisory
    end

    def null_advisory
      OpenStruct.new(ghsa_id: "N/A", updated_at: Time.now.utc)
    end

    def disclosed_advisories
      @disclosed_advisories ||= SecurityAdvisory.disclosed
                                                .always_visible_publicly
                                                .where("vulnerabilities.updated_at > ?", @updated_after)
                                                .order(updated_at: :desc)
                                                .to_a
    end

    class AtomAdvisory
      attr_reader :advisory

      delegate :ghsa_id, :references, to: :advisory

      def initialize(advisory)
        @advisory = advisory
      end

      def id
        "tag:%s,2008:%s" % [GitHub.host_name, advisory.ghsa_id]
      end

      def title
        advisory.summary
      end

      def categories
        advisory.ecosystems.uniq.map(&:upcase)
      end

      def content
        return @content if defined? @content
        @content = begin
          html_content = GitHub::Goomba::MarkdownPipeline.to_html(advisory.description)
          GitHub::Goomba::NoReferrerPipeline.to_html(html_content)
        rescue EncodingError, TypeError => e
          Failbot.report(e, security_advisory: advisory, description: advisory.description)
          nil
        end
      end

      def vulnerabilities
        @vulnerabilities ||= advisory.vulnerabilities.has_public_ecosystem.map do |vulnerability|
          AtomVulnerability.new(vulnerability)
        end
      end

      def published_at
        advisory.published_at.utc.iso8601
      end

      def updated_at
        advisory.updated_at.utc.iso8601
      end

      def valid?
        content.present? && advisory.published_at.present?
      end
    end

    class AtomVulnerability
      attr_reader :vulnerability

      delegate :severity, :vulnerable_version_range, to: :vulnerability

      def initialize(vulnerability)
        @vulnerability = vulnerability
      end

      def name
        vulnerability.package[:name]
      end

      def ecosystem
        vulnerability.package[:ecosystem].downcase
      end

      # This breaks the facade on the VulnerableVersionRange model for convenience to
      # avoid testing for a nil value for SecurityVulnerability#first_patched_version
      def first_patched_version
        vulnerability.fixed_in
      end
    end
  end
end
