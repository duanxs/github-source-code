# rubocop:disable Style/FrozenStringLiteralComment

class Oauth::AuthorizeIntegrationView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  include ApplicationHelper # provides `coarse_time_ago_in_words`
  attr_reader :application, :approval, :authorization, :previous_version, :params, :redirect_uri, :rate_limited, :session, :events, :form_submission_path, :include_device_warning, :device_authorization
  attr_accessor :show_deleted

  delegate :key, :name, :preferred_bgcolor, to: :application
  delegate :user,                           to: :authorization
  delegate :human_name, :permission_text,   to: :permissions_view

  OCTICON_FOR_PERMISSION = {
    "blocking"               => "x",
    "emails"                 => "mail",
    "external_contributions" => "zap",
    "followers"              => "thumbsup",
    "gpg_keys"               => "verified",
    "keys"                   => "key",
    "starring"               => "star",
    "watching"               => "eye",
    "plan"                   => "briefcase",
  }

  DESCRIPTION_SUFFIX = {
    "blocking"  => "users blocked by you",
    "emails"    => "your email addresses",
    "followers" => "users you follow",
    "gpg_keys"  => "your public GPG keys",
    "keys"      => "your Git SSH keys",
    "starring"  => "your starred repositories",
    "watching"  => "your watched repositories",
    "plan"      => "your subscription plan on GitHub",
  }

  # Array of relevant oauth params
  OAUTH_KEYS = [:client_id, :redirect_uri, :type, :state, :user_code]

  def description(resource)
    return permissions_view.description(resource) unless DESCRIPTION_SUFFIX.key?(resource)

    prefix = permissions[resource] == :write ? "View and manage" : "View"
    prefix + " " + DESCRIPTION_SUFFIX[resource]
  end

  # Public: Hash of oauth params to submit with authorization request.
  #
  # Returns a Hash.
  def oauth_params
    OAUTH_KEYS.each_with_object({}) do |key, hash|
      next if params[key].nil?
      hash[key] = params[key]
    end
  end

  def permissions
    return @_permissions if defined?(@_permissions)
    @_permissions = if existing_integration_version?
      integration_version.user_permissions
    else
      @application.latest_version.user_permissions
    end
  end

  def integration_version
    @_integration_version ||= @authorization.try(:integration_version)
  end

  def octicon_for_permission(permission)
    OCTICON_FOR_PERMISSION.fetch(permission, "file")
  end

  # Public: Boolean if this application already has access.
  #
  # Returns truthy if the application already has access to this user.
  def existing_authorization?
    @authorization.present?
  end

  # Public: Boolean if this authorization has an IntegrationVersion set.
  #
  # Returns a Boolean.
  def existing_integration_version?
    integration_version.present?
  end

  # Public: Boolean if this authorization has an IntegrationVersion for a previous
  # version to diff against
  #
  # Returns a Boolean.
  def existing_previous_version?
    @previous_version.present?
  end

  # Public: Boolean if the application type is of type `Integration`.
  #
  # Returns true if there is an OauthAuthorization's application
  # is of type `Integration`.
  def integration_application_type?
    return false unless existing_authorization?
    @authorization.integration_application_type?
  end

  # Public: Should OAuth access be blocked for this user because of mandatory
  # email verification?
  #
  # Returns a Boolean.
  def block_for_mandatory_email_verification?
    @block_for_mandatory_email_verification ||= begin
      GitHub.dogstats.increment "oauth.email_verification", tags: ["action:checked", "type:#{current_user.signup_timeframe}"]
      if current_user.must_verify_email?
        OauthAccess.instrument_email_verification_required(user: current_user, application_id: application.id)
        return true
      end
      false
    end
  end

  # Public: Determine whether to show the Reauthorization button.
  #
  # Returns a Boolean.
  def rate_limited?
    existing_authorization? && !!@rate_limited
  end

  def application_creation_time_in_words
    if application.created_at > 1.day.ago
      "less than a day"
    else
      coarse_time_ago_in_words(application.created_at)
    end
  end

  def application_user_range
    case application.authorizations.count
    when 0...10
      "Fewer than 10"
    when 10...100
      "Fewer than 100"
    when 100...1000
      "Fewer than 1K"
    else
      "More than 1K"
    end
  end

  def application_redirect_origin
    Addressable::URI.parse(redirect_uri).origin
  end

  def application_owner
    return "GitHub" if application_github_owned?

    if @application.owner.is_a?(Business)
      @application.owner.name
    elsif @application.owner.profile_name.present?
      @application.owner.profile_name
    else
      @application.owner
    end
  end

  def application_github_owned?
    application.github_owned?
  end

  def installations_count
    installed_account_names.size
  end

  def installed_account_names
    return @installed_account_names if defined?(@installed_account_names)

    target_user_ids = @application.installations.with_user(current_user).where(target_type: "User").distinct.pluck(:target_id)
    @installed_account_names = User.with_ids(target_user_ids).order(:login).pluck(:login)
  end

  def existing_installations_message
    builder = []

    GitHub.dogstats.time("authorize_integration.existing_installations_message") do
      if installations_count > 0
        builder << "#{application.name} has been installed on "
        builder << helpers.pluralize(installations_count, "account")
        builder << " you have access to: "
        builder << helpers.content_tag(:strong, installed_account_names.to_sentence) # rubocop:disable Rails/ViewModelHTML
        builder << "."
      else
        builder << "#{application.name} has not been installed on any accounts you have access to."
      end
    end

    helpers.safe_join(builder)
  end

  def diff
    return @_diff if defined?(@_diff)
    @_diff = if existing_previous_version?
      integration_version.diff(previous_version)
    else
      @application.latest_version.diff(integration_version)
    end
  end

  def permissions_added
    filter_user_permissions(diff.permissions_added)
  end

  def permissions_upgraded
    filter_user_permissions(diff.permissions_upgraded)
  end

  def permissions_downgraded
    filter_user_permissions(diff.permissions_downgraded)
  end

  def permissions_removed
    filter_user_permissions(diff.permissions_removed)
  end

  def permissions_unchanged
    filter_user_permissions(diff.permissions_unchanged)
  end

  def filter_user_permissions(permissions)
    permissions.select { |resource, action| User::Resources.subject_types.include?(resource) }
  end

  def device_authorization?
    oauth_params.key?(:user_code) && !oauth_params.key?(:redirect_uri)
  end

  def include_device_warning?
    !!@include_device_warning
  end

  def ip_address
    return @ip_address if defined?(@ip_address)
    @ip_address = @device_authorization.try(:ip)
  end

  def location
    return @location if defined?(@location)
    @location = @device_authorization.try(:location)
  end

  def location_available?
    ip_address.present? || location.present?
  end

  def device_code_created_at
    @device_authorization.created_at.to_formatted_s(:deprecation_mailer)
  end

  private

  def permissions_view
    @_permissions_view ||= ::Integrations::PermissionsView.new(integration: application, form: nil)
  end
end
