# frozen_string_literal: true

class OauthTokens::ScopeView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents

  attr_reader :scope

  RELATED_SCOPES = {
    "write:packages" => ["read:packages", "repo"],
    "delete:packages" => ["read:packages"],
  }

  # Some scopes require related scopes that are not nested. For example,
  # `repo` is required to published a package so we consider it related to
  # `write:packages`.
  #
  # Returns: [String]
  def related_scope_families(scope)
    RELATED_SCOPES.key?(scope) ? RELATED_SCOPES[scope] : []
  end
end
