# frozen_string_literal: true

class OauthTokens::TokenView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  include CredentialAuthorizationMethods

  attr_reader :token, :sso_organizations, :credential_map

  def credential
    token
  end

  def return_to_path
    urls.settings_user_tokens_path
  end

  def sso_subjects_label
    "tokens"
  end

  def sso_help_path
    "/articles/authorizing-a-personal-access-token-for-use-with-a-saml-single-sign-on-organization/"
  end
end
