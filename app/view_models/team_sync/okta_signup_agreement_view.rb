# frozen_string_literal: true

module TeamSync
  class OktaSignupAgreementView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :membership, :adminable_accounts, :selected_account

    def selected?(account)
      selected_account == account
    end

    # Pameter used by AccountMembershipHelper to identify the org or business
    def field_name(account)
      account.is_a?(Business) ? "business" : "account"
    end

    # Value expected by AccountMembershipHelper to identify the org or business
    def field_value(account)
      account.is_a?(Business) ? account.slug : account.login
    end

    def target_member_description
      if membership.member.is_a?(Business)
        "your enterprise account #{membership.member.slug}"
      else
        "your organization @#{membership.member.login}"
      end
    end

    def registered?(account)
      EarlyAccessMembership.okta_team_sync_waitlist.exists?(member: account)
    end

    def enabled?(account)
      account.okta_team_sync_beta_enabled?
    end

    def opted_in_for_marketing_emails?(user)
      NewsletterPreference.marketing?(user: user)
    end
  end
end
