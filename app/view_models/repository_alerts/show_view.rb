# frozen_string_literal: true

module RepositoryAlerts
  class ShowView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    areas_of_responsibility :dependabot_alerts, :dependabot

    OPENER_NAME = "GitHub"
    OPENER_URL = "#{GitHub.dotcom_host_protocol}://#{GitHub.dotcom_host_name}/github"
    DISMISSAL_REASONS = [
      "A fix has already been started",
      "No bandwidth to fix this",
      "Risk is tolerable to this project",
      "This alert is inaccurate or incorrect",
      "Vulnerable code is not actually used",
    ]

    attr_reader :repository, :manifest_path, :package_name, :state

    def after_initialize
      unless state == "open" || state == "closed"
        raise ArgumentError, %(State must be either "open" or "closed".)
      end

      helpers.extend(NetworkHelper)
    end

    def page_title
      "Alerts · #{repository.name_with_owner}"
    end

    def open?
      state == "open"
    end

    def closed?
      state == "closed"
    end

    def alerts
      @alerts ||=
        if open?
          available_alerts.visible.to_a
        else
          available_alerts.dismissed.to_a
        end
    end

    def alert_ids
      alerts.map(&:id)
    end

    def alert_count
      alerts.count
    end

    def missing?
      alerts.none?
    end

    def opener_name
      OPENER_NAME
    end

    def opener_url
      OPENER_URL
    end

    def opened_at
      @opened_at ||= alerts.map(&:created_at).min
    end

    def dismisser
      most_recently_dismissed_alert&.dismisser
    end

    def dismissed_at
      most_recently_dismissed_alert&.dismissed_at
    end

    def suggested_version
      suggested_fix.target_version
    end

    def any_alert_unpatched?
      return @any_alert_unpatched if defined? @any_alert_unpatched
      @any_alert_unpatched = alerts
        .any? { |a| a.vulnerable_version_range.fixed_in.blank? }
    end

    def related_alerts_count
      @related_alerts_count ||= if open?
        available_alerts.dismissed.count
      else
        available_alerts.visible.count
      end
    end

    def related_alerts_available?
      related_alerts_count > 0
    end

    def related_alerts_path
      urls.network_alert_path(
        user_id: repository.owner,
        repository: repository,
        manifest_path: manifest_path,
        package_name: package_name,
        state: other_state,
      )
    end

    def dismissal_reasons
      DISMISSAL_REASONS
    end

    def dismiss_path(reason:)
      urls.dismiss_many_vulnerability_alerts_path(
        user_id: repository.owner,
        repository: repository,
        ids: alert_ids,
        reason: reason,
      )
    end

    def short_manifest_path
      helpers.short_manifest_file_path(manifest_path)
    end

    def manifest_blob_path
      urls.blob_view_path(manifest_path, repository.default_branch, repository)
    end

    def dependency_upgrade_examples
      return [] if suggested_version.nil?

      @dependency_upgrade_examples ||= helpers.dependency_upgrade_examples(
        manifest_path: manifest_path,
        package_name: package_name,
        suggested_version: suggested_version,
      )
    end

    def render_vulnerability_description(vulnerability)
      GitHub::Goomba::MarkdownPipeline.to_html(vulnerability.description)
    rescue EncodingError, TypeError => error
      Failbot.report(error, vulnerability_id: vulnerability.id, description: vulnerability.description)
      nil
    end

    # Does the repository have the Dependabot feature and can the user see it?
    def dependabot_visible?
      repository.automated_security_updates_visible_to?(current_user)
    end

    # Can Dependabot open a PR to resolve this alert?
    def dependabot_resolvable?
      return false unless dependabot_visible?
      return false unless dependabot_supports_manifest?
      return false unless alert_potentially_fixable?
      return false if current_dependency_update&.requested_or_proposed?

      true
    end

    def current_dependency_update
      return @current_dependency_update if defined? @current_dependency_update
      return unless current_dependency_update_id

      @current_dependency_update = repository.dependency_updates.find_by(id: current_dependency_update_id)
    end

    def dependency_graph_enabled?
      return @dependency_graph_enabled if defined? @dependency_graph_enabled
      @dependency_graph_enabled = repository.dependency_graph_enabled?
    end

    private

    def suggested_fix
      @suggested_fix ||= RepositoryVulnerabilityAlert::SuggestedFix.new(alerts)
    end

    def current_dependency_update_id
      suggested_fix.candidate_alert&.current_dependency_update_id
    end

    def alert_potentially_fixable?
      return false if missing?

      open? && suggested_version
    end

    def dependabot_supports_manifest?
      RepositoryDependencyUpdate.manifest_path_supported?(manifest_path)
    end

    def alerts_enabled?
      return @alerts_enabled if defined? @alerts_enabled
      @alerts_enabled = repository.vulnerability_alerts_enabled?
    end

    def available_alerts
      @available_alerts ||= repository.repository_vulnerability_alerts
        .for_manifest_and_package(manifest_path: manifest_path, package_name: package_name)
        .newest_first
        .preload(:vulnerability, :vulnerable_version_range, :dismisser)
    end

    def most_recently_dismissed_alert
      return nil if open?
      return @most_recently_dismissed_alert if defined? @most_recently_dismissed_alert
      @most_recently_dismissed_alert = alerts
        .select { |a| a.dismissed_at }
        .max_by { |a| a.dismissed_at }
    end

    def other_state
      open? ? "closed" : "open"
    end
  end
end
