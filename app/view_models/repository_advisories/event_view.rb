# frozen_string_literal: true

module RepositoryAdvisories
  class EventView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    ICONS = {
      "closed"           => "circle-slash",
      "reopened"         => "primitive-dot",
      "published"        => "shield-check",
      "renamed"          => "pencil",
      "github_withdrawn" => "shield-x",
      "github_published" => "shield-check",
      "cve_requested"    => "shield",
      "cve_assigned"     => "check",
      "cve_not_assigned" => "x",
      "credit_accepted"  => "thumbsup",
      "credit_declined"  => "thumbsdown",
    }
    ICONS.default = "primitive-dot-stroke"
    ICONS.freeze

    COLORS = {
      "closed"           => "red",
      "reopened"         => "green",
      "published"        => "green",
      "cve_assigned"     => "green",
      "cve_not_assigned" => "red",
    }.freeze

    delegate :changed_attribute, :actor, :value_was, :value_is,
      to: :event

    attr_reader :event, :show_credit_action_button

    def visible?
      # We're hiding GitHub "rejection" for now because it's not helpful for a
      # maintainer and carries a negative connotation.
      event.name != "github_rejected"
    end

    def dom_id
      "event-#{event.id}"
    end

    def icon
      ICONS[event.name]
    end

    def badge_color
      case color
      when "red"
        "text-white bg-red"
      when "green"
        "text-white bg-green"
      else
        ""
      end
    end

    def verb
      case event.name
      when "github_rejected"
        "dismissed"
      when "github_withdrawn"
        "withdrew"
      when "github_published"
        "released"
      when "credit_accepted"
        "accepted credit"
      when "credit_declined"
        "declined credit"
      else
        event.name
      end
    end

    def event_name
      event.name
    end

    def timestamp
      event.created_at
    end

    def link_to_global_advisory?
      GitHub.global_advisories_enabled? &&
        (event.github_published? || event.github_withdrawn?) &&
        event.repository_advisory&.vulnerability&.globally_available?
    end

    def global_advisory_path
      urls.global_advisory_path(event.repository_advisory.ghsa_id)
    end

    def advisory_ghsa_id
      event.repository_advisory.vulnerability.ghsa_id
    end

    def octicon
      if icon == "shield-x" || icon == "shield-check"
        styles = "margin-left: 2px"
      end

      helpers.octicon(icon, style: styles)
    end

    def credit_action_button_path
      path_arguments = [
        event.repository_advisory.repository.owner,
        event.repository_advisory.repository,
        event.repository_advisory
      ]

      case event_name
      when "credit_accepted"
        urls.decline_repository_advisory_credit_path(*path_arguments)
      when "credit_declined"
        urls.accept_repository_advisory_credit_path(*path_arguments)
      end
    end

    def credit_action_button_text
      case event_name
      when "credit_accepted"
        "Decline credit"
      when "credit_declined"
        "Accept credit"
      end
    end

    private

    def color
      COLORS[event.name]
    end
  end
end
