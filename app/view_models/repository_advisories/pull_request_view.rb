# frozen_string_literal: true

module RepositoryAdvisories
  class PullRequestView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :pull_request

    delegate :issue,
             :title,
             :number,
             :author,
             :created_at,
             :merged_at,
             :open?,
             :merged?,
             :draft?,
             :total_comments,
             to: :pull_request

    delegate :safe_user, :assignees, to: :issue

    def status_icon_classes
      classes = %w(d-inline-block float-left ml-n4 tooltipped tooltipped-e)

      if merged?
        classes << "text-purple"
      elsif draft?
        classes << "text-gray-light"
      else
        classes << "text-green"
      end

      classes
    end

    def status_icon
      if merged?
        "git-merge"
      else
        "git-pull-request"
      end
    end

    def status_tooltip
      if merged?
        "Merged pull request"
      elsif draft?
        "Open draft pull request"
      else
        "Open pull request"
      end
    end

    def no_comments?
      total_comments.zero?
    end
  end
end
