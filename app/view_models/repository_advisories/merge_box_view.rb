# frozen_string_literal: true

module RepositoryAdvisories
  class MergeBoxView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :advisory

    delegate :repository, :workspace_repository, :open_pull_requests,
      to: :advisory

    def admin?
      return @is_admin if defined? @is_admin
      @is_admin = advisory.adminable_by?(current_user)
    end

    def merge_state
      @merge_state ||=
        if pull_request_merge_states.empty?
          :no_changes
        elsif batch_merge.invalid?
          :invalid
        elsif pull_request_merge_states.include?(:draft)
          :draft
        elsif pull_request_merge_states.include?(:dirty)
          :dirty
        elsif pull_request_merge_states.include?(:unknown)
          :unknown
        else
          :clean
        end
    end

    def mergeable?
      merge_state == :clean
    end

    def viewer_may_merge?
      admin?
    end

    def viewer_can_merge?
      mergeable? && viewer_may_merge?
    end

    def open_pull_request_count
      open_pull_requests.count
    end

    def merge_box_path
      urls.advisory_workspace_merge_box_path(repository.owner, repository, advisory)
    end

    def merge_path
      urls.merge_advisory_workspace_path(repository.owner, repository, advisory)
    end

    def current_head_shas
      open_pull_requests.map(&:head_sha)
    end

    def validation_error_messages
      batch_merge.tap(&:validate).errors.full_messages
    end

    private

    def batch_merge
      @batch_merge ||= advisory.build_batch_merge(actor: current_user)
    end

    def pull_request_merge_states
      @pull_request_merge_states ||= Set.new.tap do |merge_states|
        open_pull_requests.each do |pull_request|
          merge_states << pull_request.merge_state(viewer: current_user).status
        end
      end
    end
  end
end
