# frozen_string_literal: true

module Codespaces
  class RefSelectView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :repository, :selected_ref

    def selected?(ref)
      return false unless selected_ref

      ref.qualified_name.b == selected_ref.qualified_name.b
    end

    def refs
      @refs ||= repository.heads.refs_with_default_first
    end
  end
end
