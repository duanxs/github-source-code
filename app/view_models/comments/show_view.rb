# frozen_string_literal: true

module Comments
  class ShowView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    include CommentsHelper

    attr_reader :comment_data

    attr_reader :comment, :repository, :gist
    # metadata
    attr_reader :type, :position, :tag
    # markup & content
    attr_reader :form_path, :action_text, :dom_id, :timeline_diff_anchor
    # flags
    attr_reader :show_avatar, :admin, :stafftools, :undeleteable

    attr_reader :comment_context

    attr_reader :textarea_id, :required

    attr_reader :head_repository
    attr_reader :preview_repository
    attr_reader :pull_request

    # human-readable descriptions of each potential tag
    # Will be prepended with the tag subject and verb, and appended with
    # the tag object, e.g "This user is *a member of* the foo repository."
    TAG_DESCRIPTIONS = {
      member: "a member of",
      owner: "the owner of",
      collaborator: "been invited to collaborate on",
      contributor: "previously committed to",
      first_time_contributor: "a first-time contributor to",
      first_timer: "first pull request on GitHub",
    }

    def initialize(attrs = {})
      raise ArgumentError, "class_name is deprecated, stop using it" if attrs[:class_name]

      super
    end

    def submitted_at
      if review_comment?
        comment.submitted_at
      else
        comment.created_at
      end
    end

    # Public: Returns true if this is for a pull request review comment
    def review_comment?
      comment.kind_of? PullRequestReviewComment
    end

    # Public: Returns true if this comment already exists.
    #         Generally true for displaying comments in a discussion,
    #         false when displaying the form for creating a new comment.
    def persisted?
      comment.persisted?
    end

    # Public: Returns true if comment submitted via email.
    def email?
      comment.try(:formatter) == :email
    end

    def formatter_class
      if format = comment.try(:formatter)
        "#{format}-format"
      end
    end

    # Public: Returns true if type is a `commit`.
    def commit_comment?
      type == "commit"
    end

    # Public: Returns true if a commit comment has either a position and path.
    def inline_commit_comment?
      comment.respond_to?(:inline?) && comment.inline?
    end

    def show_avatar # rubocop:disable Lint/DuplicateMethods
      return true unless defined?(@show_avatar)
      @show_avatar
    end

    # Public: Returns true if the avatar should be shown.
    def show_avatar?
      !!show_avatar
    end

    # Public: Returns the DOM ID String.
    def dom_id # rubocop:disable Lint/DuplicateMethods
      return @dom_id if defined?(@dom_id)
      comment_dom_id(comment)
    end

    # Public: Returns the comment String CSS class name.
    #
    # Can default two ways for commit comments: inline with the code or in the
    # normal commit timeline.
    def class_name
      @class_name ||=
        if inline_commit_comment?
          "inline-comment"
        elsif commit_comment?
          "commit-comment"
        end
    end

    def classes
      @classes ||= begin
        cl = [class_name]
        cl << "thread-start" if position == 1
        cl << "current-user" if author?
        cl.compact.join " "
      end
    end

    # Public: Returns the String CSS classes for the icon.
    def icon_classes
      if email?
        "mail-read"
      else
        "comment"
      end
    end

    # Public: Returns the String icon tooltip title for email icon.
    def icon_title
      "This comment left via email reply." if email?
    end

    def body
      comment.body
    end

    def body_version
      comment.body_version
    end

    def body_html
      comment.body_html
    end

    def created_at
      comment.created_at
    end

    def edited_at
      comment.edited_at
    end

    def editor
      return "an unknown user" if comment.editor.nil?
      comment.editor.login
    end

    def editor_link
      return "ghost" if comment.editor.nil?
      comment.editor.login
    end

    def edited?
      comment.edited?
    end

    def author
      @_author ||= if comment.user
        comment.user
      elsif comment.try(:safe_user)
        comment.safe_user
      else
        User.ghost
      end
    end

    def admin?
      form_path && admin
    end

    def title?
      comment.respond_to?(:title)
    end

    def title
      comment.title
    end

    def action_text # rubocop:disable Lint/DuplicateMethods
      @action_text ||= "commented"
    end

    def repo_organization
      return unless repo_belongs_to_org?

      repository.organization
    end

    def repo_belongs_to_org?
      repository && repository.in_organization?
    end

    # Public: Returns true if a comment is by the current user.
    def author?
      comment.user == @current_user
    end

    # Does this comment have a body at all? Sometimes issues are filed with only
    # the title.
    def has_body?
      body.present?
    end

    # Currently @mentions are not supported by gist
    def mentions_supported?
      !gist
    end

    # Gists have nothing to reference via #
    def references_supported?
      !gist
    end

    # Public: describe the type of comment
    # The bodies of both issues and pull requests are technically comments, but
    # should be described to the user as "issue" and "pull request"
    # respectively. Other comments, like follow-up comments on issues and pull
    # requests, or Gist commments, are just "comment".
    def comment_type
      if comment.is_a?(Issue)
        if comment.pull_request
          "pull request"
        else
          "issue"
        end
      else
        "comment"
      end
    end

    def is_pending_comment?
      comment.is_a?(PullRequestReviewComment) && comment.pending?
    end

    def show_allow_maintainer_edits_option?
      preview_repository &&
      preview_repository != head_repository &&
      pull_request.head_repository.pushable_by?(current_user) &&
      !(head_repository.fork? && head_repository.in_organization?)
    end

    private

    # Private: Are we logged in?
    def logged_in?
      !!current_user
    end

    # Helper method to guard against the view being called agains a comment
    # type that doesn't support contributor badges. Rather than throwing an
    # undefined method error, just return false
    def authored_by_contributor?(comment)
      comment.authored_by_contributor? if comment.respond_to?(:authored_by_contributor?)
    end

    # Private: the ids of any collaborators on the related repository or gist
    def collaborator_ids
      @collaborator_ids ||=
        if repository
          Set.new repository.all_member_ids
        else
          Set.new
        end
    end

    # Private: the ids of any admins of the related repository or gist
    def admin_ids
      @admin_ids ||=
        if repository
          Set.new repository.admin_ids
        elsif gist && gist.user_id
          Set.new([gist.user_id])
        else
          Set.new
        end
    end
  end
end
