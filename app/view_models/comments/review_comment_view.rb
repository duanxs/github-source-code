# frozen_string_literal: true

module Comments
  class ReviewCommentView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    include ActionView::Helpers::TagHelper
    include CommentsHelper
    include PlatformHelper

    attr_reader :comment

    CommentFragment = parse_query <<-'GRAPHQL'
      fragment on Comment {
        id
        body
        bodyHTML
        bodyVersion
        spammy
        publishedAt
        viewerDidAuthor
        createdViaEmail
        lastEditedAt
        stafftoolsUrl
        authoredBySubjectAuthor

        ... on Updatable {
          viewerCanUpdate
        }

        ... on UpdatableComment {
          viewerCannotUpdateReasons
        }

        ... on Deletable {
          viewerCanSeeDeleteButton
        }

        ... on Minimizable {
          isMinimized
          viewerCanSeeMinimizeButton
        }

        ... on Reportable {
          url
          viewerCanReport
          viewerRelationship
        }

        ... on RepositoryNode {
          repository {
            owner {
              login
            }
            databaseId
            name
            isArchived
            url
            hasIssuesEnabled
            viewerCanSeeCommenterFullName
            organization {
              displayCommenterFullNameEnabled: isFeatureEnabled(name: "display_commenter_full_name")
            }
          }
        }

        ... on PullRequestReviewComment {
          updateResourcePath
          state

          pullRequest {
            state
            number
            viewerCanApplySuggestion
            headRef {
              name
              target {
                oid
              }
            }
            headRepository {
              name
              owner {
                login
              }
            }
          }
          thread {
            path
            diffSide
            startLine
          }
          outdated
          selectionContainsDeletions
        }
        ... on CommitComment {
          updateResourcePath
        }
        ... on TeamDiscussionComment {
          updateResourcePath: resourcePath
        }
        ... on AbuseReportable {
          reportCount
          topReportReason
          lastReportedAt
        }

        author {
          login
          resourcePath
          ...Views::Comments::Actor::Actor
          ...ApplicationHelper::LinkedAvatar
          ...ApplicationHelper::ActorProfileLink::Actor
        }

        ...Views::Comments::CommentHeaderReactionButton::Subject
        ...Views::Comments::EditForm::Comment
        ...Views::Comments::MinimizeCommentForm::Comment
        ...Views::Comments::Reactions::Subject
        ...Views::Comments::RepositoryAuthorAssociation::Comment
        ...Views::Comments::CommentEditHistory::Comment
      }
    GRAPHQL

    def initialize(comment, tab)
      @comment = CommentFragment.new(comment)
      @tab = tab
    end

    def display_dropdown_divider?
      (!archived? && (can_update? || must_verify_email?)) || can_minimize? || can_delete?
    end

    def stafftools_url
      comment.stafftools_url
    end

    def previewable_comment_classes(unread_comment_class, logged_in)
      classes = %w{previewable-edit js-suggested-changes-container js-task-list-container unminimized-comment}
      classes << "js-comment" unless minimized?
      classes << "d-none" if minimized?
      classes << "js-pending-review-comment is-pending" if pending?
      if viewer_did_author_comment?
        classes << "current-user"
      else
        classes << unread_comment_class
      end
      classes << "reorderable-task-lists" if logged_in
      classes << "js-selection-contains-deletions" if selection_contains_deletions?
      classes.join(" ")
    end

    def update_resource_path
      comment.update_resource_path.to_s
    end

    def published_at
      comment.published_at
    end

    def viewer_did_author_comment?
      comment.viewer_did_author?
    end

    def body_version
      comment.body_version
    end

    def body
      comment.body
    end

    def author
      comment.author
    end

    def author_login
      author ? author.login : "ghost"
    end

    def viewer_relationship
      comment.viewer_relationship
    end

    def comment_id
      comment.id
    end

    def viewer_did_author?
      comment.viewer_did_author
    end

    def authored_by_subject_author?
      comment.authored_by_subject_author
    end

    def created_via_email?
      comment.created_via_email?
    end

    def last_edited_at
      comment.last_edited_at
    end

    def spammy?
      comment.spammy?
    end

    def pending?
      return comment.state == "PENDING" if pull_request_review_comment?
      published_at.nil?
    end

    def submitted?
      return comment.state == "SUBMITTED" if pull_request_review_comment?
      published_at.present?
    end

    def body_html
      comment.body_html
    end

    def pull_request
      comment.pull_request
    end

    def show_abuse_reports?
      comment.is_a?(AbuseReportable) && !comment.report_count.nil? && comment.report_count > 0
    end

    def abuse_report_tooltip
      ApplicationController.helpers.abuse_report_tooltip(comment.report_count, comment.top_report_reason, comment.last_reported_at)
    end

    def viewer_can_apply_suggestion?
      pull_request.viewer_can_apply_suggestion? && head_repo && head_ref
    end

    def pull_request_open?
      pull_request.state == "OPEN"
    end

    def head_ref
      pull_request.head_ref
    end

    def head_ref_oid
      head_ref.target.oid
    end

    def thread_path
      comment.thread.path
    end

    def repository
      comment.repository
    end

    def repository_owner_login
      comment.repository.owner.login
    end

    def repository_name
      comment.repository.name
    end

    def repository_id
      comment.repository.database_id
    end

    def head_repo
      pull_request.head_repository
    end

    def pull_request_number
      pull_request.number
    end

    def content_url
      if comment.is_a?(PlatformTypes::PullRequest) || comment.is_a?(PlatformTypes::Issue)
        "#{comment.repository.url}/#{comment.url}"
      else
        comment.url
      end
    end

    def apply_suggested_changes_path
      urls.pull_request_apply_suggestions_path(repository_owner_login, repository_name, pull_request.number)
    end

    def files_tab?
      @tab.presence && @tab.to_s.downcase == "files"
    end

    def selection_contains_deletions?
      return @selection_contains_deletions if defined?(@selection_contains_deletions)
      @selection_contains_deletions = comment.is_a?(PlatformTypes::PullRequestReviewComment) &&
        comment.selection_contains_deletions == true
    end

    def outdated?
      return @outdated if defined?(@outdated)
      @outdated = comment.is_a?(PlatformTypes::PullRequestReviewComment) &&
        comment.outdated == true
    end

    def side
      return @side if defined?(@side)
      @side = comment.is_a?(PlatformTypes::PullRequestReviewComment) &&
        comment.thread.diff_side&.downcase
    end

    def reactable?
      return @reactable if defined?(@reactable)
      @reactable = comment.is_a?(PlatformTypes::Reactable)
    end

    def can_update?
      return @can_update if defined?(@can_update)
      @can_update = comment.is_a?(PlatformTypes::Updatable) && comment.viewer_can_update?
    end

    def can_delete?
      return @can_delete if defined?(@can_delete)
      @can_delete = comment.is_a?(PlatformTypes::Deletable) && comment.viewer_can_see_delete_button?
    end

    def can_report?
      return @can_report if defined?(@can_report)
      @can_report = comment.is_a?(PlatformTypes::Reportable) && comment.viewer_can_report?
    end

    def can_minimize?
      return @can_minimize if defined?(@can_minimize)
      @can_minimize = minimizable? && comment.viewer_can_see_minimize_button?
    end

    def archived?
      return @archived if defined?(@archived)
      @archived = comment.is_a?(PlatformTypes::RepositoryNode) && comment.repository.is_archived?
    end

    def must_verify_email?
      return @must_verify_email if defined?(@must_verify_email)
      @must_verify_email = comment.is_a?(PlatformTypes::UpdatableComment) && comment.viewer_cannot_update_reasons.include?("VERIFIED_EMAIL_REQUIRED")
    end

    def can_view_in_stafftools?
      return @can_view_in_stafftools if defined?(@can_view_in_stafftools)
      @can_view_in_stafftools = !comment.stafftools_url.nil?
    end

    def minimizable?
      return @minimizable if defined?(@minimizable)
      @minimizable = comment.is_a?(PlatformTypes::Minimizable)
    end

    def minimized?
      return @minimized if defined?(@minimized)
      @minimized = minimizable? && comment.is_minimized?
    end

    def pull_request_review_comment?
      return @pull_request_review_comment if defined?(@pull_request_review_comment)
      @pull_request_review_comment = comment.is_a?(PlatformTypes::PullRequestReviewComment)
    end

    def suggested_changes_enabled?
      return @suggested_changes_enabled if defined?(@suggested_changes_enabled)
      @suggested_changes_enabled = pull_request_review_comment? &&
        comment.thread
    end

    def can_open_new_issue?
      return @can_open_new_issue if defined?(@can_open_new_issue)
      @can_open_new_issue =
        pull_request_review_comment? &&
        repository.has_issues_enabled?
    end

    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def can_see_commenter_full_name?
      @viewer_can_see_commenter_full_name ||= pull_request_review_comment? &&
        comment.repository.viewer_can_see_commenter_full_name
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def display_commenter_full_name_enabled_for_org?
      @display_commenter_full_name_enabled_for_org ||= pull_request_review_comment? &&
        repository.organization&.display_commenter_full_name_enabled
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator
  end
end
