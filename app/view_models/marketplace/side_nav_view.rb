# frozen_string_literal: true

class Marketplace::SideNavView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  include PlatformHelper
  include Rails.application.routes.url_helpers

  Query = parse_query <<-'GRAPHQL'
    fragment Root on Query {
      enabledFeatures

      viewer @include(if: $loggedIn) {
        pendingOrders: orderPreviews {
          totalCount
        }
        purchases: marketplaceSubscriptions {
          totalCount
        }
        pendingMarketplaceInstallations {
          totalCount
        }
      }

      manageableListings: marketplaceListings(viewerCanAdmin: true) {
        totalCount
      }

      navigationCategories: marketplaceCategories(forNavigation: true, excludeSubcategories: true) {
        name
        slug
        subCategories(forNavigation: true) {
          name
          slug
        }
      }

      filterCategories: marketplaceCategories(includeCategories: ["free", "paid", "free-trials", "github-enterprise"]) {
        name
        slug
      }
    }
  GRAPHQL

  attr_reader :viewing_pending_orders, :viewing_pending_installations
  alias_method :viewing_pending_orders?, :viewing_pending_orders
  alias_method :viewing_pending_installations?, :viewing_pending_installations
  delegate :category_slug, :tool_type, :verification_state, to: :search_options, prefix: :selected

  def initialize(data:, search_options: Marketplace::SearchOptions.new, use_search_paths: false, viewing_pending_orders: false, viewing_pending_installations: false, current_user: nil, user_session: nil)
    super
    @data = Query::Root.new(data)
    @search_options = search_options
    @use_search_paths = use_search_paths
    @viewing_pending_orders = viewing_pending_orders
    @viewing_pending_installations = viewing_pending_installations
  end

  def tool_type_options
    Marketplace::SearchOptions::LISTING_TYPES
  end

  def verification_state_options
    Marketplace::SearchOptions::VERIFICATION_STATES
  end

  def navigation_categories
    data.navigation_categories.map do |category|
      Category.new(category)
    end
  end

  def filter_categories
    data.filter_categories.map do |category|
      Category.new(category)
    end
  end

  def selected_category_is_a_filter?
    selected_category_slug.present? &&
      filter_categories.any? { |category| category.has_slug?(selected_category_slug) }
  end

  def prefilled_search_path(overrides = {})
    params = search_options.as_params(overrides)
    marketplace_search_path(params)
  end

  def category_or_prefilled_search_path(slug:)
    if use_search_paths?
      prefilled_search_path(category: slug)
    else
      marketplace_category_path(slug)
    end
  end

  def show_user_items?
    data.viewer.present?
  end

  def viewer_has_pending_orders?
    viewer_order_previews_count > 0
  end

  def viewer_has_pending_installations?
    data.enabled_features.include?(PlatformTypes::FeatureFlag::MARKETPLACE_PENDING_INSTALLATIONS) &&
      viewer_pending_installations_count > 0
  end

  def viewer_has_purchases?
    viewer_purchases_count > 0
  end

  def viewer_has_listings?
    viewer_listings_count > 0
  end

  class Category
    delegate :slug, :name, to: :category

    def initialize(category)
      @category = category
    end

    def sub_categories
      category.sub_categories.map { |subcategory| Category.new(subcategory) }
    end

    def has_slug?(other_slug)
      slug == other_slug
    end

    def has_slug_or_children_has_slug?(other_slug)
      has_slug?(other_slug) ||
        sub_categories.any? { |subcategory| subcategory.has_slug?(other_slug) }
    end

    private

    attr_reader :category
  end

  private

  attr_reader :data, :search_options, :use_search_paths
  alias_method :use_search_paths?, :use_search_paths

  def viewer_order_previews_count
    data.viewer&.pending_orders&.total_count || 0
  end

  def viewer_purchases_count
    data.viewer&.purchases&.total_count || 0
  end

  def viewer_listings_count
    data.manageable_listings&.total_count || 0
  end

  def viewer_pending_installations_count
    data.viewer&.pending_marketplace_installations&.total_count || 0
  end
end
