# frozen_string_literal: true

module MarketplacePurchases
  class OrderPreviewView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    include PlatformHelper
    include GitHub::Application.routes.url_helpers

    Query = parse_query <<-'GRAPHQL'
      fragment Root on Query {
        enabledFeatures
        ...Views::MarketplacePurchases::TermsOfServiceNotice::Root
        ...Views::MarketplacePurchases::OrderPreview::Root
      }
      fragment MarketplaceListing on MarketplaceListing {
        ...Views::MarketplacePurchases::TermsOfServiceNotice::MarketplaceListing
        ...Views::MarketplacePurchases::OrderPreview::MarketplaceListing
        slug
        name
        installedForViewer
        viewerHasPurchased
        viewerOrganizationSubscriptionItems(first: 1) {
          totalCount
        }
      }
      fragment MarketplaceListingPlan on MarketplaceListingPlan {
        ...Views::MarketplacePurchases::OrderPreview::MarketplaceListingPlan
        ...Views::MarketplacePurchases::PlanPricing::MarketplaceListingPlan
        name
        id
        hasFreeTrial
        isPaid
        isPerUnit
        isDirectBilling
        unitName
        forUsersOnly
        forOrganizationsOnly
      }
      fragment User on User {
        ...Views::MarketplacePurchases::OrderPreview::User
        ...Views::MarketplacePurchases::PlanPricing::User
        ...Views::MarketplacePurchases::CurrentPlan::User
        name
        login
        paymentMethod {
          isValid
        }
        subscription {
          endDate
          pendingCycle {
            pendingMarketplaceChanges(listingSlug: $listingSlug, first: 1) {
              nodes {
                isCancellation
              }
            }
          }
          planChange(subscribableId: $planId, subscribableQuantity: $quantity) {
            finalPrice
            subscriptionItem(subscribableId: $planId) {
              price(freeTrial: false)
            }
          }
          freeTrialEndDate
          postFreeTrialBillDate
          postTrialProratedTotalPrice(id: $planId, quantity: $quantity)
          eligibleForFreeTrialOnListing(listingSlug: $listingSlug)
          item: subscriptionItem(listingSlug: $listingSlug, active: true)
          subscriptionItem(listingSlug: $listingSlug, active: true) {
            freeTrialEndsOn
            formattedTotalPrice
            quantity
            marketplaceListingPlan: subscribable {
              ... on MarketplaceListingPlan {
                id
                name
                isPerUnit
                unitName
                isDirectBilling
              }
            }
          }
        }
      }

      fragment Organization on Organization {
        ...Views::MarketplacePurchases::OrderPreview::Organization
        ...Views::MarketplacePurchases::PlanPricing::Organization
        ...Views::MarketplacePurchases::CurrentPlan::Organization
        name
        login
        oapWhitelistRequired(listingSlug: $listingSlug)
        paymentMethod {
          isValid
        }
        isInvoiced
        subscription {
          endDate
          pendingCycle {
            pendingMarketplaceChanges(listingSlug: $listingSlug, first: 1) {
              nodes {
                isCancellation
              }
            }
          }
          freeTrialEndDate
          postFreeTrialBillDate
          postTrialProratedTotalPrice(id: $planId, quantity: $quantity)
          planChange(subscribableId: $planId, subscribableQuantity: $quantity) {
            finalPrice
            subscriptionItem(subscribableId: $planId) {
              price(freeTrial: false)
            }
          }
          eligibleForFreeTrialOnListing(listingSlug: $listingSlug)
          subscriptionItem(listingSlug: $listingSlug, active: true) {
            formattedTotalPrice
            quantity
            marketplaceListingPlan: subscribable {
              ... on MarketplaceListingPlan {
                id
                name
                isPerUnit
                isDirectBilling
                unitName
                listing {
                  slug
                }
              }
            }
          }
        }
      }
    GRAPHQL

    attr_reader :data, :listing, :selected_plan, :viewer, :organization, :account, :current_subscription_item, :agreement, :quantity

    def initialize(data:, listing:, selected_plan:, viewer:, organization:, quantity:, current_user: nil, user_session: nil)
      @data = Query::Root.new(data)
      @listing = Query::MarketplaceListing.new(listing)
      @selected_plan = Query::MarketplaceListingPlan.new(selected_plan)
      @viewer = Query::User.new(viewer)
      @organization = Query::Organization.new(organization)
      @quantity = quantity

      @account = @organization || @viewer
      @subscription = @account.subscription
      @current_subscription_item = @subscription.subscription_item
      @current_user = current_user
    end

    def show_already_installed_notice?
      return false if listing.viewer_has_purchased?
      return false if listing.viewer_organization_subscription_items.total_count.nonzero?

      listing.installed_for_viewer?
    end

    def submit_button_show_billing_modal?
      !account_has_valid_payment_method?
    end

    def account_has_valid_payment_method?
      account.payment_method && account.payment_method.is_valid?
    end

    def submit_button_text
      already_purchased? ? "Issue plan changes" : "Complete order and begin installation"
    end

    def plan_change?
      !current_plan || current_plan.id != selected_plan.id || current_subscription_item.quantity != quantity
    end

    def days_left_on_viewer_free_trial
      return unless viewer_has_purchased?
      (viewer.subscription.subscription_item.free_trial_ends_on && viewer.subscription.subscription_item.free_trial_ends_on.to_date - GitHub::Billing.today).to_i
    end

    def viewer_on_free_trial?
      return false unless viewer_has_purchased?
      selected_plan.has_free_trial? && days_left_on_viewer_free_trial.positive?
    end

    def show_pending_cancellation?
      account.subscription.pending_cycle.pending_marketplace_changes.nodes.any?(&:is_cancellation?)
    end

    def viewer_has_purchased?
      !!viewer.subscription.item
    end

    def invoiced_billing_enabled?
      organization.present? && organization.is_invoiced
    end

    def target
      return @target if defined?(@target)
      @target = User.find_by(login: account.login)
    end

    def show_listing_actions?
      if GitHub.flipper[:name_address_collection].enabled?(target) && target.user?
        target.has_personal_profile?
      else
        true
      end
    end

    def show_name_address_form?
      GitHub.flipper[:name_address_collection].enabled?(target) && target.user?
    end

    def form_action_path
      if GitHub.flipper[:name_address_collection].enabled?(target) && target.user?
        return user_profile_update_path unless target.has_personal_profile?
      end

      if already_purchased?
        marketplace_order_upgrade_path(listing.slug, plan_id: selected_plan.id)
      else
        marketplace_order_purchase_path(listing.slug, plan_id: selected_plan.id)
      end
    end

    def total_mp_plans_price
      account.subscription.plan_change.subscription_item.price
    end

    def post_trial_prorated_total_price
      account.subscription.post_trial_prorated_total_price
    end

    def formatted_prorated_total_price
      prorated_total_price.abs.format
    end

    def prorated_total_price
      account.subscription.plan_change.final_price
    end

    def update_plan_button_disabled?
      return true if show_oauth_access_checkbox?

      !plan_change?
    end

    def purchase_plan_button_disabled?
      return true if show_oauth_access_checkbox?
      return true unless can_subscribe_with_current_account?

      already_purchased?
    end

    def can_subscribe_with_current_account?
      return false if selected_plan.for_organizations_only? && account.is_a?(PlatformTypes::User)
      return false if selected_plan.for_users_only? && account.is_a?(PlatformTypes::Organization)

      true
    end

    def selected_plan_account_type_text
      return "a personal account" if selected_plan.for_users_only?
      "an organization account" if selected_plan.for_organizations_only?
    end

    def show_oauth_access_checkbox?
      organization && organization.oap_whitelist_required
    end

    def current_plan
      return nil unless current_subscription_item

      current_subscription_item.marketplace_listing_plan
    end

    def show_current_plan_price?
      !current_plan.is_direct_billing?
    end

    def current_plan_description
      description = []
      description << current_plan.name

      if current_plan.is_per_unit?
        description << "with #{current_plan.unit_name.pluralize(current_subscription_item.quantity)}"
      end

      if viewer_has_purchased? && days_left_on_viewer_free_trial.positive?
        description << "(Free Trial)"
      end

      description.join(" ")
    end

    def current_plan_formatted_total_price
      current_subscription_item.formatted_total_price
    end

    def show_current_plan?
      already_purchased?
    end

    def already_purchased?
      current_subscription_item.present?
    end

    def eligible_for_free_trial?
      selected_plan.has_free_trial? && account.subscription.eligible_for_free_trial_on_listing?
    end

    def billing_modal_path
      if organization
        return_url = marketplace_order_path(listing_slug: listing.slug, plan_id: selected_plan.id, account: organization.login, quantity: quantity)
        org_payment_modal_path(organization.login, return_to: return_url)
      else
        return_url = marketplace_order_path(listing_slug: listing.slug, plan_id: selected_plan.id, quantity: quantity)
        payment_modal_path(return_to: return_url)
      end
    end

    def billing_link
      if organization
        return_url = marketplace_order_url(host: GitHub.host_name, listing_slug: listing.slug, plan_id: selected_plan.id, account: organization.login, quantity: quantity)
        org_payment_url(organization.login, host: GitHub.host_name, return_to: return_url)
      else
        return_url = marketplace_order_url(host: GitHub.host_name, listing_slug: listing.slug, plan_id: selected_plan.id, quantity: quantity)
        payment_url(return_to: return_url)
      end
    end

    def formatted_free_trial_end_date
      format_date(account.subscription.free_trial_end_date)
    end

    def formatted_day_after_trial_ends
      format_date(account.subscription.free_trial_end_date + 1.day)
    end

    def formatted_post_free_trial_bill_date
      format_date(account.subscription.post_free_trial_bill_date)
    end

    def start_date
      format_date(GitHub::Billing.today)
    end

    def end_date
      format_date(account.subscription.end_date)
    end

    private

    # Private: Formats dates for order preview to a consistent format. Example: "Oct 6th"
    def format_date(date)
      date.strftime("%b #{date.day.ordinalize}")
    end

    # Private: Allows the url helpers to work within ViewModel
    def default_url_options
      { host: GitHub.host_name }
    end
  end
end
