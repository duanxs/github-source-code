# frozen_string_literal: true

module Repositories
  module Graph
    class TrafficView
      attr_reader :referrer_domain

      def initialize(repo, user, referrer_domain, options_for_analytics)
        @repo = repo
        @user = user
        @referrer_domain = referrer_domain
        @options_for_analytics = options_for_analytics
      end

      def fetch_data
        return @data_fetched if defined?(@data_fetched)

        GitHub.dogstats.time "graph", tags: ["action:traffic", "type:content"] do
          @content = GitHub.analytics.content(@options_for_analytics).data
          @domains = GitHub.analytics.referrers(@options_for_analytics).data
        end
        @data_fetched = true
      rescue Octolytics::Error => error
        Failbot.report!(error)
        @data_fetched = false
      end

      def data_present?
        @data_fetched && (@content["content"].any? || @domains["referrers"].any?)
      end

      # Remove redundant information from content titles
      def cleaned_content
        nwo = Rails.development? ? "twbs/bootstrap" : @repo.nwo
        clean_regex = /( · #{Regexp.escape(nwo)})?( Wiki)?( · GitHub)?\z/

        @content["content"].take(10).map do |content|
          content["title"].gsub!(clean_regex, "")
          content
        end
      end

      def referrers
        fetch_data
        @domains["referrers"].take(10)
      end

      def show_referrer_paths?
        @referrer_domain.present?
      end

      def paths_data
        return @paths_data if defined?(@paths_data)

        @paths_data = begin
          GitHub.dogstats.time "graph", tags: ["action:traffic", "type:referrer_paths"] do
            options = @options_for_analytics.merge(referrer: @referrer_domain)
            GitHub.analytics.referrer_paths(options).data
          end
        rescue Octolytics::Error => error
          Failbot.report!(error)
          nil
        end
      end

      def paths_data_present?
        paths_data.present?
      end

      def referrer_favicon
        paths_data["favicon_url"]
      end

      def referrer_paths
        paths_data["paths"].take(10)
      end
    end
  end
end
