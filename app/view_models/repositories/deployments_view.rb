# frozen_string_literal: true

module Repositories
  class DeploymentsView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    include ActionView::Helpers::UrlHelper
    include GitHub::Application.routes.url_helpers
    include DeploymentsHelper

    attr_reader :repository

    MAXIMUM_DEPLOYS = 10
    SUCCESS_STATE = "success"

    def initialize(repository, filter)
      @repository = repository

      @all_deploys = Deployment.where(repository_id: @repository.id)
                               .where(["deployments.created_at > ?", 1.month.ago])
                               .reverse_order
                               .limit(MAXIMUM_DEPLOYS)
                               .includes(:statuses)
                               .includes(:creator)

      @production_deploys = @all_deploys.where(production_environment: true,
                                               transient_environment: false)
      @temporary_deploys  = @all_deploys.where(production_environment: false,
                                               transient_environment: true)
      @staging_deploys    = @all_deploys.where(production_environment: false,
                                               transient_environment: false)
      @production_deploys.each do |deploy|
        next if deploy.latest_state != SUCCESS_STATE
        @latest_production_deploy = [deploy]
        break
      end

      @latest_production_deploy ||= if filter == :production
        @production_deploys
      else
        @production_deploys.limit(1)
      end
    end

    def deploys_for(type)
      case type
      when nil                then @all_deploys
      when :production        then @production_deploys
      when :staging           then @staging_deploys
      when :temporary         then @temporary_deploys
      when :latest_production then Array(@latest_production_deploy.first)
      else []
      end
    end

    def deploy_pr(deployment)
      return if deployment.sha.blank?

      @deploy_prs ||= {}
      @deploy_prs[deployment.sha] ||=
        branch_list_view(deployment.sha).branches_with_pull_requests.first.try(:last).try(:first)
      @deploy_prs[deployment.sha] ||=
        PullRequest.where(repository_id: @repository.id,
                          head_sha: deployment.sha)
                   .limit(1)
                   .first
      @deploy_prs[deployment.sha]
    end

    def deploy_ref_compare_url(ref)
      urls.compare_path(@repository, ref)
    end

    def deploy_log(deployment)
      deployment.statuses.first.try(:log_url)
    end

    def deploy_type_label(deployment)
      label = if deployment.production_environment?
        "Production"
      elsif deployment.transient_environment?
        "Temporary"
      else
        "Staging"
      end
    end

    def deployment_success_url(deployment)
      status = deployment.statuses.first
      return unless status
      return if status.state != SUCCESS_STATE
      status.environment_url
    end

    def deploy_ref(deployment)
      return if deployment.sha.blank?

      @deploy_refs ||= {}
      @deploy_refs[deployment.sha] ||=
        branch_list_view(deployment.sha).branches_with_pull_requests.keys.first
      @deploy_refs[deployment.sha] ||= deploy_pr(deployment).try(:head_ref_name)
      @deploy_refs[deployment.sha] ||= deployment.sha
      @deploy_refs[deployment.sha]
    end

    private

    def branch_list_view(deployment_sha)
      return if deployment_sha.blank?

      @branch_list_views ||= {}
      @branch_list_views[deployment_sha] ||= begin
        commit = @repository.commits.find deployment_sha
        Commits::BranchListView.new commit: commit
      end
      @branch_list_views[deployment_sha]
    end
  end
end
