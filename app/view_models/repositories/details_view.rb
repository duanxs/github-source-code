# frozen_string_literal: true

module Repositories
  class DetailsView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    # Owner ids for repositories that should not have the Used By button
    USED_BY_ORG_BLOCKLIST = [
      139426, # angular org
    ].freeze
    # repository ids that should not have the Used By button
    USED_BY_REPO_BLOCKLIST = [
      44124366, # yahoo/elide
    ].freeze

    attr_reader :repository, :repository_is_offline, :collaborative_editing_is_enabled, :tree_name, :show_fml_modal

    def forking_allowed?
      forkability_error.nil?
    end

    # `disallow_oopfs` enabled: If the current user owns the repo then they have
    #   nowhere to fork the repository. The repository cannot be part of a business if it
    #   is owned by the user.
    #
    # `disallow_oopfs` disabled: If the current user owns the repo and is a member of
    #   no orgs then they have nowhere to fork the repository.
    #
    # Note: this method is not exhaustive. If it returns true, there is definitely nowhere to fork.
    #   If it returns false, it means it couldn't definitively rule out the ability to fork.
    #   This heavier calculation is done on and handled via the fork partial.
    def nowhere_to_fork?
      if GitHub.flipper[:disallow_oopfs].enabled?(current_user)
        current_user == repository.owner
      else
        current_user == repository.owner && current_user&.organizations&.empty?
      end
    end

    def forkability_error_message
      return if forking_allowed?
      "Cannot fork because #{forkability_error}."
    end

    def show_sponsor_button?
      !current_user&.has_any_trade_restrictions? && repository.show_sponsor_button?
    end

    def show_fml_modal?
      @show_fml_modal
    end

    # Returns a hash of local variables that govern the behaviour of the "Watch" button on
    # repository pages.
    #
    # Returns Hash<Symbol,Object> with the following keys:
    #   repository_id      - Integer Repository ID.
    #   subscription_type  - Symbol describing the user's subscription status relative to this
    #                        repository. One of (:watching, :releases_only, :ignoring, :none).
    #   show_watcher_count - Boolean determining whether or not to show a count of the number of
    #                        watchers for the repository.
    #   classes            - String containing CSS classes to apply to the top level <details>
    #                        element of menu.
    def subscription_menu_options(subscription_status)
      subscription_type = if !subscription_status&.success?
        nil
      elsif subscription_status.subscribed?
        :watching
      elsif subscription_status.thread_type_only?(Release)
        :releases_only
      elsif subscription_status.participation_only?
        :none
      elsif subscription_status.ignored?
        :ignoring
      end

      {
        repository: {
          id: repository.id,
          name: repository.name,
          owner: repository.owner.login,
          public: repository.public,
        },
        subscription_type: subscription_type,
        show_watcher_count: true,
        inside_flash_alert: false
      }
    end

    private

    # Private: Is the tree a SHA as opposed to a reference name?
    #
    # Since this is just a regex match it could result in some false positives
    # but that's better than false negatives and better than trying to resolve
    # the potential ref.
    def is_tree_probably_a_sha?
      GitRPC::Util.valid_full_sha1?(tree_name)
    end

    def forkability_error
      @forkability_error ||= begin
        if repository.private_fork? && GitHub.flipper[:disallow_nested_private_forks].enabled?(current_user)
          "repository is a fork"
        elsif repository.forking_disabled?
          "forking is disabled"
        elsif repository.locked?
          "repository is locked"
        elsif repository.disabled?
          "repository is disabled"
        elsif repository.access.dmca?
          "repository is unavailable due to DMCA takedown"
        elsif repository_is_offline
          "repository is offline"
        elsif nowhere_to_fork?
          if GitHub.flipper[:disallow_oopfs].enabled?(current_user)
            "you own this repository"
          else
            "you own this repository and are not a member of any organizations"
          end
        elsif repository.empty?
          "repository is empty"
        end
      rescue GitRPC::ConnectionError => e
        Failbot.report! e
        "repository is offline"
      end
    end
  end
end
