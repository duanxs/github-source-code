# frozen_string_literal: true

module Repositories
  class CreateView
    include GitHub::Application.routes.url_helpers
    include UrlHelper

    # defer loading orgs info for current user until they interact with the
    # control if they're a member of more than ORG_COUNT_DEFER_LIMIT orgs
    ORG_COUNT_DEFER_LIMIT = 50

    # Initiate a CreateView object used to control the new repositories page.
    #
    # user - User object.
    # repo - Repository object.
    #
    # Returns the new instance.
    def initialize(user, repo)
      @user = user
      @repo = repo
    end

    attr_reader :user

    # Should the public input be checked?
    #
    # Returns "checked=checked" if so, nil otherwise.
    def public_checked
      if @repo.public?
        "checked=checked"
      end
    end

    # Should the private input be checked?
    #
    # Returns "checked=checked" if so, nil otherwise.
    def private_checked
      if @repo.private?
        "checked=checked"
      end
    end

    # Should the auto init checkbox be checked?
    #
    # Returns "checked=checked" if so, nil otherwise.
    def auto_init_checked
      if @repo.auto_init?
        "checked=checked"
      end
    end

    # Should the given gitignore template choice be selected?
    #
    # template - Template name as String, e.g. "Android".
    #
    # Returns a Boolean.
    def gitignore_checked?(template)
      template == @repo.gitignore_template
    end

    # Name of the selected gitignore template. Defaults to "None".
    #
    # Returns the gitignore template as a String.
    def gitignore_template
      @repo.gitignore_template || "None"
    end

    # Show internal UI if the user is a member of a business that's feature flagged in
    def show_internal_ui?
      @user.businesses.any?
    end

    # Name of the selected license. Defaults to "None".
    #
    # Returns the gitignore template as a String.
    def license_template
      @repo.license_template || "None"
    end

    # Creates a fun suggested name for users
    def suggested_name
      prefix = rand < 0.25 ? "octo-" : ""
      "#{adjectives.sample}-#{prefix}#{nouns.sample}"
    end

    # @return [Hash{Organization => Hash{Symbol => Symbol}}]
    # The keys of the value hash are :access and :allow_public_repos
    def organizations_info
      @organizations_info ||= user.organizations_info_sorted_hash
    end

    def organizations
      organizations_info.keys
    end

    def user_businesses
      user.businesses
    end

    def installable_marketplace_apps_info
      @marketplace_apps ||= user.installable_marketplace_apps_hash
    end

    def defer_organizations_load?
      user.organizations.size > ORG_COUNT_DEFER_LIMIT
    end

    # Public - Does the repository owner need to upgrade plan for private visibility?
    #
    # Return Boolean
    def upgrade_for_private?
      return false unless @repo

      @repo.errors[:visibility].present? &&
        @repo.can_privatize? &&
        !@repo.owner.has_any_trade_restrictions? &&
        !user.has_any_trade_restrictions?
    end

    # Public - Returns path to upgrade for a given owner with return_to param set to new repo path
    # return_to set to path for organization or user based on the owner
    #
    # Return String
    def new_repository_upgrade_path(owner)
      if owner.organization?
        return_path = new_org_repository_path(owner)
        upgrade_path(target: :organization, org: owner, return_to: return_path)
      else
        upgrade_path(return_to: new_repository_path)
      end
    end

    ADJECTIVES = %w[
      animated
      automatic
      bookish
      bug-free
      cautious
      congenial
      crispy
      cuddly
      curly
      didactic
      effective
      expert
      fantastic
      fictional
      fluffy
      friendly
      furry
      fuzzy
      glowing
      ideal
      improved
      jubilant
      laughing
      legendary
      literate
      miniature
      musical
      potential
      probable
      psychic
      redesigned
      refactored
      reimagined
      scaling
      shiny
      silver
      solid
      special
      studious
      stunning
      sturdy
      super
      super-duper
      supreme
      symmetrical
      turbo
      ubiquitous
      upgraded
      urban
      verbose
      vigilant
    ].freeze

    NOUNS = %w[
      adventure
      barnacle
      bassoon
      broccoli
      carnival
      chainsaw
      computing-machine
      couscous
      disco
      dollop
      doodle
      engine
      enigma
      eureka
      fiesta
      fortnight
      funicular
      garbanzo
      giggle
      goggles
      guacamole
      guide
      happiness
      invention
      journey
      lamp
      meme
      memory
      palm-tree
      pancake
      parakeet
      potato
      robot
      rotary-phone
      sniffle
      spoon
      spork
      succotash
      system
      telegram
      train
      tribble
      umbrella
      waddle
      waffle
      winner
    ].freeze

    def adjectives
      ADJECTIVES
    end

    def nouns
      NOUNS
    end

    def repository_license_field
      "repository[license_template]"
    end

    def repository_gitignore_field
      "repository[gitignore_template]"
    end

    # List of gitignore template options available for init'ing a repository.
    def gitignore_templates
      Gitignore.templates
    end

    # List of license file options available for init'ing a repository.
    def licenses
      License.sorted_list
    end

    def licenses_menu_items
      [# Default option
        GitHub::Menu::RadioComponent.new(
          text: "None",
          name: repository_license_field,
          value: "",
          replace_text: "None",
          checked: true,
          input_classes: "js-repo-init-setting-unchecked-menu-option"
        )
      ] + License.sorted_list.map { |license|
        GitHub::Menu::RadioComponent.new(
          text: license.name,
          name: repository_license_field,
          value: license.key,
          replace_text: license.name,
          checked: false
        )
      }
    end
  end
end
