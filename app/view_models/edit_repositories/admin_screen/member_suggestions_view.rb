# frozen_string_literal: true

class EditRepositories::AdminScreen::MemberSuggestionsView < AutocompleteView
  attr_reader :repository

  def suggested_members
    if repository.organization.nil?
      # for user owned repos, do not suggest to add the repo owner as collaborator
      suggestions.to_a.delete_if do |suggestion|
        suggestion.is_a?(User) && suggestion.id == repository.owner.id
      end
    elsif cannot_invite_outside_collaborators?
      #remove outside collaborators from sugestions
      suggestions.to_a.delete_if do |suggestion|
        suggestion.is_a?(User) && !repository.organization.member?(suggestion)
      end
    else
      suggestions
    end
  end

  def org_member_ids
    if org = repository.organization
      org.member_ids(actor_ids: suggested_user_ids)
    else
      []
    end
  end

  def repo_member_ids
    repository.member_ids(actor_ids: suggested_user_ids)
  end

  def repo_team_ids
    suggested_team_ids & repository.actor_ids_for_team_on_repo
  end

  def admin_ids
    if org = repository.organization
      org.admin_ids
    else
      []
    end
  end

  def invitees
    repository.invitees
  end

  def cannot_invite_outside_collaborators?
    repository.cannot_invite_outside_collaborators?(current_user)
  end

  private

  def autocomplete_query
    @autocomplete_query ||= AutocompleteQuery.new(
      current_user,
      query,
      organization: repository.organization,
      repository: repository,
      include_teams: include_teams,
    )
  end

  def suggested_user_ids
    suggested_members.select { |s| s.is_a?(User) }.map(&:id)
  end

  def suggested_team_ids
    suggested_members.select { |s| s.is_a?(Team) }.map(&:id)
  end
end
