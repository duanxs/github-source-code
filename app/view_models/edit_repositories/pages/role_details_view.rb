# frozen_string_literal: true

class EditRepositories::Pages::RoleDetailsView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  include Orgs::RolesHelper
  attr_reader :organization, :repository

  def show_custom_roles?
    custom_roles_supported? && custom_roles_present?
  end

  def permissions_for(role)
    ::FgpMetadata.for_role(role)
  end

  def icon_for(category)
    ::FgpMetadata.icon_for_title(category)
  end
end
