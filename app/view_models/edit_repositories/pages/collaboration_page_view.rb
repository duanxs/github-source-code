# frozen_string_literal: true

class EditRepositories::Pages::CollaborationPageView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :repository

  ADMIN_PERMISSION = "admin".freeze
  WRITE_PERMISSION = "write".freeze
  READ_PERMISSION  = "read".freeze

  def organization
    repository.organization
  end

  # Public: all collaborators on this repository, with profile associations
  # prefilled.
  def collaborators
    @collaborators ||= begin
      member_list = repository.members
      GitHub::PrefillAssociations.for_profiles(member_list)
      member_list
    end
  end

  # Public: every outstanding invitation to this repository.
  def invitations
    @invitations ||= begin
      member_list = repository.repository_invitations.includes(:invitee)
      member_list
    end
  end

  # Public: the current permission for the specified user. If they are a collaborator,
  # this should be one of "admin", "write", "read", "triage", or "maintain".
  def current_permission(user)
    most_capable_actions_or_roles[user]
  end

  # Public: whether or not the user is a member of the org if there is one
  def org_member?(user)
    organization&.member?(user)
  end

  # Public: Should we show a notice about the repository's owner having a
  # default repository permission?
  #
  # Returns a boolean.
  def show_default_repository_permission_notice?
    repository.in_organization? && organization.default_repository_permission != :none
  end

  # Private: The maximum number of addable teams to show to the user. For more
  # information check out https://github.com/github/github/issues/63976#issuecomment-256671702.
  # If we add pagination or something to addable teams, this limit can be removed.
  ADDABLE_TEAM_LIMIT = 2_000

  # Public: Get all the teams that this repository can be added to by the
  # logged-in user.
  #
  # Returns an array of teams.
  def addable_teams
    @addable_teams ||= repository.addable_teams_for(current_user).limit(ADDABLE_TEAM_LIMIT)
  end

  # Public: Get all the teams that this repository is on that are visible to the
  # logged-in user.
  #
  # Returns an array of teams.
  def repository_teams
    @existing_teams ||= repository.visible_teams_for(current_user)
  end

  # Public: Should we show the add team form on page load?
  def show_add_team_form?
    addable_teams.any?
  end

  # Public: Should we show copy reminder for seat count not being changed on public repositories
  #
  # returns Boolean
  def show_public_collaborators_text?
    repository.public? && organization&.plan&.per_seat?
  end

  # Public: Should we show copy reminder for seat count being changed for private repository collaborators?
  #
  # returns Boolean
  def show_private_collaborators_text?
    repository.private? && organization&.plan&.per_seat?
  end

  # Public: Should we hide invites for outside collaborators
  def cannot_invite_outside_collaborators?
    repository.in_organization? &&
      organization.can_restrict_repo_invites? &&
      !organization.members_can_invite_outside_collaborators? &&
      !organization.adminable_by?(current_user)
  end

  def member_invitation_action
    if repository.in_organization?
      if organization.member?(current_user)
        "invite "
      else
        "search for public "
      end
    end
  end

  # Public: The most capable ability action or role for a team on this repository.
  #
  # team - The Team to get the most capable action for.
  #
  # Returns a Symbol.
  def most_capable_action_for_team(team)
    most_capable_action_for_teams[team]
  end

  private

  # Private: a memoized list of IDs for users that have admin access to this repository
  def admins
    @admins ||= Set.new(repository.member_ids(action: :admin))
  end

  # Private: a memoized list of IDs for users that have write access to this repository
  def writers
    @writers ||= Set.new(repository.member_ids(action: :write))
  end

  # Private: a memoized list of IDs for users that have read access to this repository
  def readers
    @readers ||= Set.new(repository.member_ids(action: :read))
  end

  def invitees
    @invitees ||= Set.new(invitations.map(&:invitee).map(&:id))
  end

  # Private: Preload the most capable access levels or roles for collaborators.
  #
  # Returns a Hash{User => Symbol}
  def most_capable_actions_or_roles
    return @most_capable_roles if defined?(@most_capable_roles)

    promises = collaborators.map { |collab| repository.async_action_or_role_level_for(collab) }
    results = Promise.all(promises).sync
    @most_capable_roles = Hash[collaborators.zip(results)]
  end

  # Private: Preloaded most capable actions for this repository's teams.
  #
  # Returns a Hash{Team => Symbol}
  def most_capable_action_for_teams
    return @most_capable_action_for_teams if defined?(@most_capable_action_for_teams)

    promises = repository_teams.map { |team| team.async_most_capable_action_or_role_for(repository) }
    results = Promise.all(promises).sync
    @most_capable_action_for_teams = Hash[repository_teams.zip(results)]
  end
end
