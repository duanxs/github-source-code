# frozen_string_literal: true

class EditRepositories::Pages::MemberToolbarActionsView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :repository, :selected_ids

  def selected_user_ids
    selected_ids[:user_ids] || []
  end

  def selected_invitation_ids
    selected_ids[:invitation_ids] || []
  end

  def selected_team_ids
    selected_ids[:team_ids] || []
  end

  def headcount
    selected_user_ids.count + selected_invitation_ids.count + selected_team_ids.count
  end

  def capitalized_base_role
    base_role.to_s.capitalize
  end

  def below_base_role?(role)
    return false if base_role == :none

    role_name = Role.valid_system_role?(role) ? role.to_sym : Role.for_org(name: role, org: repository.organization).base_role.name.to_sym
    !Role.target_greater_than_other_role?(target: role_name, other_role: base_role)
  end

  def show_custom_roles?
    repository.custom_roles_supported? && org_custom_roles.any?
  end

  def org_custom_roles
    return unless repository.custom_roles_supported?
    repository.organization.custom_roles
  end

  def role_applied_to_message
    "This role will only be applied to Outside Collaborators and Teams."
  end

  private

  def base_role
    repository.organization.default_repository_permission
  end
end
