# frozen_string_literal: true

module Sponsors
  class FeaturedAccountsView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    MAX_ALLOWED_FEATURED = 4096

    attr_reader :accounts_max, :organizations_max

    def params
      {
        accounts_max: accounts_max,
        organizations_max: organizations_max,
      }
    end

    def after_initialize
      @accounts_max = [accounts_max, MAX_ALLOWED_FEATURED].min
      @accounts_max = [accounts_max, 1].max
      @organizations_max = [organizations_max, MAX_ALLOWED_FEATURED].min
      @organizations_max = [organizations_max, 1].max
    end

    def featured_accounts
      @featured_accounts ||= begin
        featured_accounts = SponsorsMembership.where(featured_state: :active)
                                              .includes(:sponsorable)
                                              .map(&:sponsorable)
        featured_orgs = featured_accounts.select(&:organization?)
                                         .sample(organizations_max)
        featured_user_sample_size = [accounts_max - featured_orgs.size, 0].max
        featured_users = featured_accounts.select(&:user?)
                                          .sample(featured_user_sample_size)
        (featured_users + featured_orgs).shuffle
      end
    end

    def description_for(account)
      account&.sponsors_membership&.featured_description
    end

    def sponsoring?(account)
      preloaded_is_sponsoring[account.login]
    end

    def sponsor_text_for(account)
      sponsoring?(account) ? "Sponsoring" : "Sponsor"
    end

    def label_for(account)
      "#{sponsor_text_for(account)} @#{account.login}"
    end

    def icon_name_for(account)
      sponsoring?(account) ? "heart" : "heart-outline"
    end

    def icon_class_for(account)
      sponsoring?(account) ? "icon-sponsoring text-pink" : "icon-sponsor text-pink"
    end

    private

    def preloaded_is_sponsoring
      @preloaded_is_sponsoring ||= featured_accounts.each_with_object({}) do |account, hash|
        hash[account.login] = account.already_sponsored_by?(current_user)
      end
    end
  end
end
