# frozen_string_literal: true

class Sponsors::Accounts::IndexView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :accepted_accounts, :submitted_accounts, :unsubmitted_accounts

  def initialize(sponsors_accounts:, current_user: nil, user_session: nil)
    memberships = SponsorsMembership.includes(sponsorable: :sponsors_listing)
                                    .where(sponsorable: sponsors_accounts)

    @accepted_accounts = memberships.select(&:accepted?).map do |membership|
      Account.new(membership: membership)
    end

    @submitted_accounts = memberships.select(&:submitted?).map do |membership|
      Account.new(membership: membership)
    end

    @unsubmitted_accounts = sponsors_accounts.reject(&:sponsors_membership)
                                             .map do |account|
      Account.new(sponsorable: account)
    end
  end

  class Account
    include ActionView::Helpers::NumberHelper
    include GitHub::Application.routes.url_helpers

    attr_reader :sponsorable

    def initialize(membership: nil, sponsorable: nil)
      @membership = membership
      @sponsorable = sponsorable || membership.sponsorable
    end

    def login
      "#{sponsorable.login}"
    end

    def description
      return sponsorable.sponsors_listing.short_description if has_listing?

      if membership_accepted?
        "You've been accepted to join the program! Set up your GitHub Sponsors profile now."
      elsif no_membership?
        "This account has not applied to join GitHub Sponsors."
      else
        "This GitHub Sponsors profile is waiting to be reviewed by GitHub."
      end
    end

    def sponsors
      @sponsors ||= sponsorable.sponsors.limit(3)
    end

    def sponsors_count
      @sponsors_count ||= sponsorable.sponsors(include_private: true).count
    end

    def funding_per_month
      @funding_per_month ||= "$#{number_with_delimiter(calculate_monthly_funding)}"
    end

    def for_organization?
      sponsorable.organization?
    end

    def has_listing?
      sponsorable.sponsors_listing.present?
    end

    def can_create_listing?
      membership_accepted? && !has_listing?
    end

    def show_sponsors_count?
      has_listing?
    end

    def show_funding_amount?
      has_listing?
    end

    def show_top_sponsors?
      has_listing?
    end

    def card_style
      if can_create_listing?
        "border-blue-light box-shadow-medium"
      end
    end

    def action_button_text
      if has_listing?
        "Dashboard"
      elsif membership_accepted?
        "Join GitHub Sponsors"
      elsif no_membership?
        "Join the waitlist"
      else
        "Manage"
      end
    end

    def action_button_url
      if has_listing?
        sponsorable_dashboard_path(sponsorable.login)
      elsif membership_accepted?
        sponsorable_signup_path(sponsorable.login)
      else
        sponsorable_waitlist_path(sponsorable.login)
      end
    end

    def action_button_style
      base_style = "btn btn btn-block"

      if can_create_listing?
        "#{base_style} btn-primary"
      else
        "#{base_style} btn-outline"
      end
    end

    private

    def no_membership?
      @membership.blank?
    end

    def membership_accepted?
      return false if no_membership?

      @membership.accepted?
    end

    def calculate_monthly_funding
      sponsorable.sponsors_listing.subscription_items.active.reduce(0) do |sum, item|
        sum + item.price(duration: :month) * item.quantity
      end
    end
  end
end
