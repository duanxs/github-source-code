# frozen_string_literal: true

class Sponsors::Tiers::IndexView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  include PlatformHelper
  include GitHub::Application.routes.url_helpers
  include ActionView::Helpers::NumberHelper

  attr_reader :sponsorable
  attr_reader :edit_tier_id
  attr_reader :show_new_form

  def initialize(sponsorable:, tiers:, edit_tier_id: nil, show_new_form: false,
    current_user: nil, user_session: nil)
    @sponsorable = sponsorable
    @tiers = tiers
    @edit_tier_id = edit_tier_id
    @show_new_form = show_new_form
  end

  def total_allowed_tier_count
    SponsorsTier::PUBLISHED_TIER_COUNT_LIMIT_PER_LISTING
  end

  def max_monthly_tier_amount
    "$#{number_with_delimiter(SponsorsTier::MAX_MONTHLY_SPONSORSHIP_AMOUNT, delimiter: ",")}"
  end

  def show_new_form?
    edit_tier_id.present? ? false : show_new_form
  end

  def editing?(tier_id)
    edit_tier_id.to_s == tier_id.to_s
  end

  def sponsorable_login
    sponsorable.login
  end

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def tiers_exist?
    @tiers_exist ||= tiers.any?
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def active_tiers
    tiers.select { |tier| tier.draft? || tier.published? }
  end

  def retired_tiers
    tiers.select(&:retired?).select { |tier| sponsor_count(tier) > 0 }
  end

  def published_tier_count
    published_tiers.size
  end

  def remaining_tier_count
    sponsors_listing.remaining_tier_count
  end

  def draft_tier_count_string
    "#{draft_tier_count} #{"draft tier".pluralize(draft_tier_count)}"
  end

  def published_tier_count_string
    "#{published_tier_count} #{"published tier".pluralize(published_tier_count)}"
  end

  def sponsor_count(tier)
    tier_subscription_counts[tier.id]
  end

  private

  attr_reader :tiers

  def sponsors_listing
    sponsorable.sponsors_listing
  end

  def published_tiers
    tiers.select(&:published?)
  end

  def draft_tiers
    tiers.select(&:draft?)
  end

  def draft_tier_count
    draft_tiers.size
  end

  def tier_subscription_counts
    @tier_subscription_counts ||= sponsorable.tier_subscription_counts
  end
end
