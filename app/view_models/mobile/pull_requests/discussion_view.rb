# frozen_string_literal: true

class Mobile::PullRequests::DiscussionView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  include Mobile::IssueHelper

  attr_reader :pull, :visible_timeline_items, :showing_full_timeline, :pull_node

  def subject
    pull
  end

  def is_draft?
    pull.draft?
  end

  def issue
    pull.issue
  end

  def merge_state
    pull.merge_state(viewer: current_user)
  end

  def state
    pull.merged? ? "merged" : super
  end

  def base_pushable?
    pull.base_repository.pushable_by?(current_user)
  end

  def timeline_items
    @visible_timeline_items
  end

  def base_branch_pushable?
    base_repo_pushable? && authorized_to_update_protected_base_branch?
  end

  def base_repo_pushable?
    pull.base_repository.pushable_by?(current_user)
  end

  def authorized_to_update_protected_base_branch?
    protected_branch = pull.protected_base_branch
    return true unless protected_branch

    protected_branch.authorized?(current_user)
  end

  # Public: Should the merge button be shown?
  #
  # For the merge button to be shown, all three of these conditions must be
  # satisfied:
  #
  # 1. The user must be able to push to the repository.
  # 2. The pull request must be open.
  # 3. The pull request must be unmerged.
  #
  # Returns a boolean.
  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def show_merge_button?
    @show_merge_button ||= (
      pull.base_repository.pushable_by?(current_user) &&
      pull.open? && !pull.merged? &&
      !advisory_workspace?
    )
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def only_merge_commit_allowed?
    repo = pull.repository

    merge_commit_allowed? && !repo.squash_merge_allowed? && !repo.rebase_merge_allowed?
  end

  def only_squash_merge_allowed?
    repo = pull.repository

    !merge_commit_allowed? && repo.squash_merge_allowed? && !repo.rebase_merge_allowed?
  end

  def only_rebase_merge_allowed?
    repo = pull.repository

    !merge_commit_allowed? && !repo.squash_merge_allowed? && repo.rebase_merge_allowed?
  end

  # takes into account the base branch's requirement for linear history
  def merge_commit_allowed?
    pull.repository.merge_commit_allowed? && (pull.protected_base_branch.nil? || !pull.protected_base_branch.required_linear_history_enabled?)
  end

  def advisory_workspace?
    pull.repository.advisory_workspace?
  end

  def advisory_repository
    pull.repository.parent_advisory_repository
  end

  def repository_advisory
    pull.repository.parent_advisory
  end

  def merge_button_disabled?
    [:no_changes, :draft, :dirty, :unknown].include?(merge_state.status)
  end

  def dependency_update
    return unless pull.repository.automated_security_updates_visible_to?(current_user)

    pull.most_recent_vulnerability_dependency_update
  end

  def author_is_dependabot?
    issue.created_by_dependabot?
  end
end
