# frozen_string_literal: true

module Mobile
  module PullRequests
    class IndexPageView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
      include Mobile::TabsHelper

      attr_reader :pulls
      attr_reader :tab

      # Public: CSS class for the specified pull request's state icon.
      #
      # pull - PullRequest to get the CSS class for.
      #
      # Returns a string.
      def state_icon_class(pull)
        if pull.merged?
          "merged"
        elsif pull.closed?
          "closed"
        elsif pull.draft?
          "draft"
        elsif pull.open?
          "opened"
        end
      end

      # Public: Symbol for the specified pull request's state icon.
      #
      # pull - PullRequest to get the symbol for.
      #
      # Returns a string.
      def state_icon_symbol(pull)
        pull.merged? ? "git-merge" : "git-pull-request"
      end

      private

      # Internal: The currently selected tab on the pull requests list.
      #
      # Can be either 'open', 'closed', or 'yours'.
      #
      # Returns a string.
      def current_tab
        tab || "open"
      end
    end
  end
end
