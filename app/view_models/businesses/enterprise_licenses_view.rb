# frozen_string_literal: true

class Businesses::EnterpriseLicensesView
  def self.for_business(business)
    new(
      consumed: business.unique_enterprise_users_count,
      total_available: business.seats,
    )
  end

  attr_reader :consumed, :total_available

  def initialize(consumed:, total_available:)
    @consumed = consumed
    @total_available = total_available
  end

  def maxed_out?
    consumed >= total_available
  end

  def overage_message
    "You have assigned all purchased user licenses."
  end
end
