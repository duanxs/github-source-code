# frozen_string_literal: true

module Businesses::IdentityManagement
  class SingleSignOnCompleteView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :business, :saml_error, :fallback_url
  end
end
