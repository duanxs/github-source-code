# frozen_string_literal: true

class Businesses::IdentityManagement::CredentialAuthorizedView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :business, :return_to, :credential_authorization

  def credential_type
    credential.is_a?(OauthAccess) ? "personal access token" : "SSH key"
  end

  def credential_description
    credential.is_a?(OauthAccess) ? credential.description : credential.title
  end

  def credential_link
    credential.is_a?(OauthAccess) ? urls.settings_user_token_path(id: credential.id) : urls.settings_user_keys_path(id: credential.id)
  end

  def credential
    credential_authorization.credential
  end
end
