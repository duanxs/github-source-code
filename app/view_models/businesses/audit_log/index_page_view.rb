# frozen_string_literal: true

class Businesses::AuditLog::IndexPageView < ::AuditLog::IndexPageView
  attr_reader :business

  def initialize(**args)
    super(args)
    @business = args[:business]
  end

  def tips
    @tips ||= Businesses::AuditLog::TipView.new(business: business, user: current_user)
  end

  def search_title
    if GitHub.single_business_environment?
      return "Recent events" if query.blank?
      return "No events found" if results.total_entries.zero?

      num_actors = results.map { |res| res[:actor] }.uniq.count
      actors = helpers.pluralize(num_actors, "actor")
      events = helpers.pluralize(results.total_entries, "event")

      "#{events} by #{actors}"
    else
      super
    end
  end

  def search_path(options = {})
    query = {}
    query[:q] = build_query_param(options) if options.present?
    urls.settings_audit_log_enterprise_path(business, query)
  end

  def suggestions_path
    urls.settings_audit_log_suggestions_enterprise_path(business)
  end

  def export_path
    urls.settings_audit_log_export_enterprise_path(business, format: :json)
  end

  def export_git_event_path
    urls.settings_audit_log_git_event_export_enterprise_path(business, format: :json)
  end

  def show_export_all?
    audit_logs? && GitHub.audit_log_export_enabled?
  end

  def filters_partial
    "businesses/audit_log/search_filters"
  end

  def normalize(entries)
    if GitHub.single_business_environment?
      super
    else
      AuditLogEntry.for_businesses(entries)
    end
  end

  def page_title
    GitHub.single_business_environment? ? "Audit log" : super
  end

  private

  # Private: The business scoped audit log ElasticSearch query.
  #
  # Returns Hash
  def es_query
    query_hash = {
      current_user: current_user,
      phrase: query,
      aggregations: true,
      page: page,
      after: after,
      before: before,
    }

    query_hash[:business_id] = business.id unless GitHub.single_business_environment?

    query_hash
  end
end
