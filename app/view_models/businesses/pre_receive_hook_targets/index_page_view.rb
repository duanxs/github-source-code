# frozen_string_literal: true

class Businesses::PreReceiveHookTargets::IndexPageView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents

  def enforcement_class(target)
    target.enforcement
  end

  def repository_display(repo)
    repo.nwo
  end

  def audit_log_query(target)
    "data.pre_receive_hook_id:#{target.hook.id} (action:pre_receive_hook.warned_push OR action:pre_receive_hook.rejected_push)"
  end
end
