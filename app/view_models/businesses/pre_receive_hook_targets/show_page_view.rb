# frozen_string_literal: true

class Businesses::PreReceiveHookTargets::ShowPageView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :query, :selected_file, :selected_repository_id
  FILES_LIMIT = 10000

  def repositories
    @repositories = PreReceiveHook::RepositoryFilter.new(current_user, query).results
  end

  def environments
    PreReceiveEnvironment.all
  end

  def files
    return @files if @files

    return @files = [] unless selected_repository_id && (repo = Repository.find_by_id(selected_repository_id))

    branch = repo.default_branch
    sha = repo.ref_to_sha(branch)
    return @files = [] if sha.nil?
    @files = repo.tree_file_list(sha)
  end

  def files_limit_reached
    files.count > FILES_LIMIT
  end

  def repository_display(repo)
    repo.nwo
  end

end
