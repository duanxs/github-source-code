# frozen_string_literal: true

class Businesses::Admins::ListView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :business, :admins

  def admin_infos
    admins.each_with_object([]) do |admin, admins_info|
      admins_info.append({
        user: admin,
        business_user_account: business_user_account_for(admin),
        role: role_for(admin),
      })
    end
  end

  def admins_count
    admins.count
  end

  private

  def business_user_account_for(admin)
    business_user_accounts.find { |user_account| user_account.user == admin }
  end

  def business_user_accounts
    @business_user_accounts ||= BusinessUserAccount.where(user_id: admin_ids, business: business)
  end

  def admin_ids
    @admin_ids ||= admins.pluck(:id)
  end

  def role_for(admin)
    return :owner if owner_ids.include?(admin.id)
    :billing_manager
  end

  def owner_ids
    @owner_ids ||= business.owner_ids
  end
end
