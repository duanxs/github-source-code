# frozen_string_literal: true

class Businesses::Settings::ProjectsView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :business

  def initialize(**args)
    super(args)
    @business = args[:business]
  end

  def organizations_form_path
    urls.update_settings_organization_projects_enterprise_path(business)
  end

  def organizations_button_text
    organizations_selected_option[:heading]
  end

  def organizations_select_list
    @organizations_select_list ||= [
      {
        heading: "No policy",
        description: "Organizations choose whether to allow organization projects.",
        value: "no_policy",
        selected: organizations_value == "no_policy",
      },
      {
        heading: "Enabled",
        description: "Organizations always have organization projects enabled.",
        value: "enabled",
        selected: organizations_value == "enabled",
      },
      {
        heading: "Disabled",
        description: "Organizations never have organization projects enabled.",
        value: "disabled",
        selected: organizations_value == "disabled",
      },
    ]
  end

  def organizations_input_value
    organizations_selected_option[:value]
  end

  def organizations_selected_option
    organizations_select_list.find { |s| s[:selected] }
  end

  def organizations_value
    if !business.organization_projects_policy?
      "no_policy"
    elsif business.organization_projects_enabled?
      "enabled"
    else
      "disabled"
    end
  end

  def organization_projects_setting_organizations_business_path
    urls.organization_projects_organizations_settings_enterprise_path(business)
  end

  def repositories_form_path
    urls.update_settings_repository_projects_enterprise_path(business)
  end

  def repositories_button_text
    repositories_selected_option[:heading]
  end

  def repositories_select_list
    @repositories_select_list ||= [
      {
        heading: "No policy",
        description: "Organizations choose whether to allow repository projects.",
        value: "no_policy",
        selected: repositories_value == "no_policy",
      },
      {
        heading: "Enabled",
        description: "Organizations always have repository projects enabled.",
        value: "enabled",
        selected: repositories_value == "enabled",
      },
      {
        heading: "Disabled",
        description: "Organizations never have repository projects enabled.",
        value: "disabled",
        selected: repositories_value == "disabled",
      },
    ]
  end

  def repositories_input_value
    repositories_selected_option[:value]
  end

  def repositories_selected_option
    repositories_select_list.find { |s| s[:selected] }
  end

  def repositories_value
    if !business.repository_projects_policy?
      "no_policy"
    elsif business.repository_projects_enabled?
      "enabled"
    else
      "disabled"
    end
  end

  def repository_projects_setting_organizations_business_path
    urls.repository_projects_organizations_settings_enterprise_path(business)
  end
end
