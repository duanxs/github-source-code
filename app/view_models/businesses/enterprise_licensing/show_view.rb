# frozen_string_literal: true

class Businesses::EnterpriseLicensing::ShowView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :business, :viewer, :server_license_fetch_failed

  delegate :billing_term_ends_on, :volume_licensing_enabled?,
    :advanced_security_enabled?, to: :business

  def any_license_overage?
     enterprise_licenses_overage? || volume_licenses_overage?
  end

  def enterprise_licenses_overage?
    overage_enterprise_licenses > 0
  end

  def volume_licenses_overage?
    overage_volume_licenses > 0
  end

  def consumed_licenses
    @consumed_licenses ||= business.consumed_licenses
  end

  def total_available_licenses
    @total_available_licenses ||= business.licenses
  end

  def consumed_enterprise_licenses
    @consumed_enterprise_licenses ||= business.unique_enterprise_users_count
  end

  def consumed_enterprise_licenses_percent
    return 0 if available_enterprise_licenses <= 0
    percent = (consumed_enterprise_licenses.to_f / available_enterprise_licenses).round(2)
    (percent * 100).to_i
  end

  def available_enterprise_licenses
    @available_enterprsie_licenses ||= business.seats
  end

  def overage_enterprise_licenses
    consumed_enterprise_licenses - available_enterprise_licenses
  end

  def overage_enterprise_licenses_percent
    return 0 if available_enterprise_licenses <= 0
    percent = (overage_enterprise_licenses.to_f / available_enterprise_licenses).round(2)
    (percent * 100).to_i
  end

  def consumed_volume_licenses
    @consumed_volume_licenses ||= business.unique_volume_users_count
  end

  def consumed_volume_licenses_percent
    return 0 if available_volume_licenses <= 0
    percent = (consumed_volume_licenses.to_f / available_volume_licenses).round(2)
    (percent * 100).to_i
  end

  def available_volume_licenses
    @available_volume_licenses ||= business.volume_license_cap
  end

  def overage_volume_licenses
    consumed_volume_licenses - available_volume_licenses
  end

  def overage_volume_licenses_percent
    return 0 if available_volume_licenses <= 0
    percent = (overage_volume_licenses.to_f / available_volume_licenses).round(2)
    (percent * 100).to_i
  end

  def installations
    @installations ||= business.enterprise_installations.order(:host_name)
  end

  def installations_user_count
    @installations_user_count ||= installations.map do |installation|
      installation.user_accounts.count
    end.sum
  end

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def viewer_is_admin?
    @viewer_is_admin ||= business.owner?(viewer)
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def server_licenses
    @server_licenses ||= fetch_server_licenses
  end

  def fetch_server_licenses
    @server_license_fetch_failed = false
    return [] unless business.enterprise_web_business_id.present?
    GitHub::EnterpriseWeb::License.all(business.enterprise_web_business_id).sort_by(&:expires_at)
  rescue Faraday::Error
    @server_license_fetch_failed = true
    []
  end

  def server_license_seats_descriptor(license)
    if license.unlimited?
      "Unlimited"
    else
      license.seats
    end
  end

  def server_license_features_descriptor(license)
    features = []
    features << "GitHub Private Instance" if license.for_private_instance?
    features << "GitHub Advanced Security" if license.advanced_security_enabled?
    features.empty? ? "" : "(#{features.join(", ")})"
  end
end
