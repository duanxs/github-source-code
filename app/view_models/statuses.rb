# frozen_string_literal: true

# This file is required in rails3 because of the existence of Api::Statuses.
# See https://github.com/github/github/pull/31066
module Statuses
end
