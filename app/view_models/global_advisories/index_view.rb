# frozen_string_literal: true

module GlobalAdvisories
  class IndexView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    areas_of_responsibility :security_advisories

    attr_reader :query, :results

    def after_initialize
      helpers.extend(PackageDependenciesHelper)
    end

    def advisories
      @advisories ||= results.map { |result| result["_model"] }
    end

    def repository_advisories
      @repository_advisories ||= advisories.map(&:repository_advisory).compact
    end

    def repository_advisory_readable?(advisory)
      @readable_repository_advisories ||= Promise.all(repository_advisories.map do |repo_advisory|
        repo_advisory.async_readable_by?(current_user).then do |readable|
          repo_advisory if readable
        end
      end).sync.compact.to_set

      @readable_repository_advisories.include?(advisory.repository_advisory)
    end

    def advisory_title(advisory)
      if repository_advisory_readable?(advisory)
        advisory.repository_advisory.title
      elsif advisory.summary.present?
        advisory.summary
      else
        helpers.truncate(advisory.description, length: 100)
      end
    end

    def index_path(**params)
      urls.global_advisories_path(params)
    end

    def show_path(advisory)
      urls.global_advisory_path(advisory.ghsa_id)
    end

    def ecosystem_filters
      helpers.ecosystems.map { |ecosystem|
        selected = query.qualifier_selected?(name: :ecosystem, value: ecosystem)
        {
          label: helpers.ecosystem_label(ecosystem),
          selected: selected,
          color: helpers.ecosystem_color(ecosystem),
          url: index_path(query: query.replace_qualifier(name: :ecosystem, value: selected ? nil : ecosystem.downcase)),
        }
      }.unshift({
        label: "All ecosystems",
        selected: !query.contains_qualifier?(name: :ecosystem),
        url: index_path(query: query.replace_qualifier(name: :ecosystem , value: nil)),
      })
    end

    def severity_filters
      Vulnerability::SEVERITIES.map { |severity|
        selected = query.qualifier_selected?(name: :severity, value: severity)
        {
          label: severity.capitalize,
          selected: selected,
          severity: severity,
          url: index_path(query: query.replace_qualifier(name: :severity, value: selected ? nil : severity)),
        }
      }.unshift({
        label: "All severities",
        selected: !query.contains_qualifier?(name: :severity),
        url: index_path(query: query.replace_qualifier(name: :severity , value: nil)),
      })
    end

    def sort_directions
      [
        { label: "Newest", query: "created-desc", default: true },
        { label: "Oldest", query: "created-asc" },
        { label: "Recently updated", query: "updated-desc" },
        { label: "Least recently updated", query: "updated-asc" },
      ].map do |sort|
        selected = query.qualifier_selected?(name: :sort, value: sort[:query])
        selected ||= !query.contains_qualifier?(name: :sort) if sort[:default]
        {
          label: sort[:label],
          selected: selected,
          url: index_path(query: query.replace_qualifier(name: :sort, value: selected ? nil : sort[:query])),
        }
      end
    end

    def protip_query
      <<~'GRAPHQL'
        {
          securityAdvisories(orderBy: {field: PUBLISHED_AT, direction: DESC}, first: 10) {
            nodes {
              description
              ghsaId
              summary
              publishedAt
            }
          }
        }
      GRAPHQL
    end

    def protip_url
      "#{GitHub.developer_help_url}/v4/explorer?query=#{UrlHelper.escape_path(protip_query)}"
    end

    def documentation_url
      "#{GitHub.help_url}/github/managing-security-vulnerabilities/browsing-security-vulnerabilities-in-the-github-advisory-database#searching-the-github-advisory-database"
    end
  end
end
