# frozen_string_literal: true

class Integrations::NewFromManifestView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :integration, :manifest, :owner, :manifest_token

  def page_title
    "Create GitHub App"
  end

  def selected_link
    :integrations
  end

  def owner_name
    owner.login
  end
end
