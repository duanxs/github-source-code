# frozen_string_literal: true

class Integrations::PermissionsView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  attr_reader :integration, :form, :hook

  NAME_OVERRIDES = {
    "blocking"                       => "Block another user",
    "emails"                         => "Email addresses",
    "external_contributions"         => "Contributions from Enterprise Server",
    "gpg_keys"                       => "GPG keys",
    "keys"                           => "Git SSH keys",
    "organization_administration"    => "Administration",
    "organization_hooks"             => "Webhooks",
    "organization_plan"              => "Plan",
    "organization_pre_receive_hooks" => "Pre-receive hooks",
    "organization_projects"          => "Projects",
    "organization_user_blocking"     => "Blocking users",
    "organization_self_hosted_runners"     => "Self-hosted runners",
    "organization_secrets"           => "Secrets",
    "repository_hooks"               => "Webhooks",
    "repository_pre_receive_hooks"   => "Pre-receive hooks",
    "repository_projects"            => "Projects",
    "statuses"                       => "Commit statuses",
    "vulnerability_alerts"           => "Dependabot alerts",
  }.freeze

  HOOK_EVENT_OVERRIDES = {
    "organization_projects" => "Organization Projects",
    "repository_projects"   => "Repository Projects",
  }.freeze

  DESCRIPTIONS = {
    "administration"                 => "Repository creation, deletion, settings, teams, and collaborators.",
    "actions"                        => "Workflows, workflow runs and artifacts.",
    "app_config"                     => "",
    "blocking"                       => "View and manage users blocked by the user.",
    "checks"                         => "Checks on code.",
    "composable_comments"            => "Create composable comments within issues and pull requests.",
    "content_references"             => "Get notified of content references, and create content attachments.",
    "contents"                       => "Repository contents, commits, branches, downloads, releases, and merges.",
    "deployments"                    => "Deployments and deployment statuses.",
    "discussions"                    => "Discussions and related comments and labels.",
    "emails"                         => "Manage a user's email addresses.",
    "external_contributions"         => "Send contributions from an Enterprise Server environment.",
    "enterprise_administration"      => "Administer an enterprise account",
    "followers"                      => "A user's followers",
    "gpg_keys"                       => "View and manage a user's GPG keys.",
    "issues"                         => "Issues and related comments, assignees, labels, and milestones.",
    "keys"                           => "Git SSH keys",
    "members"                        => "Organization members and teams.",
    "metadata"                       => "Search repositories, list collaborators, and access repository metadata.",
    "organization_administration"    => "Manage access to an organization.",
    "organization_hooks"             => "Manage the post-receive hooks for an organization.",
    "organization_plan"              => "View an organization's plan.",
    "organization_pre_receive_hooks" => "View and modify enforcement of the pre-receive hooks that are available to an organization.",
    "organization_projects"          => "Manage organization projects, columns, and cards.",
    "organization_self_hosted_runners" => "View and manage Actions self-hosted runners available to an organization.",
    "organization_secrets"           => "Manage organization secrets.",
    "organization_user_blocking"     => "View and manage users blocked by the organization.",
    "packages"                       => "Packages published to the GitHub Package Platform.",
    "pages"                          => "Retrieve Pages statuses, configuration, and builds, as well as create new builds.",
    "plan"                           => "View a user's plan.",
    "pull_requests"                  => "Pull requests and related comments, assignees, labels, milestones, and merges.",
    "repository_hooks"               => "Manage the post-receive hooks for a repository.",
    "repository_pre_receive_hooks"   => "View and modify enforcement of the pre-receive hooks that are available to a repository.",
    "repository_projects"            => "Manage repository projects, columns, and cards.",
    "secrets"                        => "Manage repository secrets.",
    "security_events"                => "View and manage security events like code scanning alerts.",
    "single_file"                    => "Manage just a single file.",
    "starring"                       => "List and manage repositories a user is starring.",
    "statuses"                       => "Commit statuses.",
    "team_discussions"               => "Manage team discussions and related comments.",
    "vulnerability_alerts"           => "Retrieve Dependabot alerts.",
    "watching"                       => "List and change repositories a user is subscribed to.",
    "workflows"                      => "Update GitHub Action workflow files.",
  }.freeze

  INTEGRATOR_EVENT_DESCRIPTIONS = {
    "meta"                           => "When this App is deleted and the associated hook is removed.",
  }.freeze

  REST_PERMISSIONS_BASE_URL = "#{GitHub.developer_help_url}/v3/apps/permissions/".freeze
  GRAPHQL_OBJECT_BASE_URL = "#{GitHub.developer_help_url}/v4/object/".freeze

  RESOURCE_DOCS_URLS = {
    "administration"                 => "#{REST_PERMISSIONS_BASE_URL}#permission-on-administration",
    "blocking"                       => "#{REST_PERMISSIONS_BASE_URL}#permission-on-blocking",
    "checks"                         => "#{REST_PERMISSIONS_BASE_URL}#permission-on-checks",
    "contents"                       => "#{REST_PERMISSIONS_BASE_URL}#permission-on-contents",
    "deployments"                    => "#{REST_PERMISSIONS_BASE_URL}#permission-on-deployments",
    "emails"                         => "#{REST_PERMISSIONS_BASE_URL}#permission-on-emails",
    "followers"                      => "#{REST_PERMISSIONS_BASE_URL}#permission-on-followers",
    "gpg_keys"                       => "#{REST_PERMISSIONS_BASE_URL}#permission-on-gpg-keys",
    "issues"                         => "#{REST_PERMISSIONS_BASE_URL}#permission-on-issues",
    "keys"                           => "#{REST_PERMISSIONS_BASE_URL}#permission-on-keys",
    "members"                        => "#{REST_PERMISSIONS_BASE_URL}#permission-on-members",
    "metadata"                       => "#{REST_PERMISSIONS_BASE_URL}#metadata-permissions",
    "organization_administration"    => "#{REST_PERMISSIONS_BASE_URL}#permission-on-organization-administration",
    "organization_hooks"             => "#{REST_PERMISSIONS_BASE_URL}#permission-on-organization-hooks",
    "organization_pre_receive_hooks" => "#{REST_PERMISSIONS_BASE_URL}#permission-on-organization-pre-receive-hooks",
    "organization_projects"          => "#{REST_PERMISSIONS_BASE_URL}#permission-on-organization-projects",
    "organization_user_blocking"     => "#{REST_PERMISSIONS_BASE_URL}#permission-on-organization-user-blocking",
    "packages"                       => "#{REST_PERMISSIONS_BASE_URL}#permission-on-packages",
    "pages"                          => "#{REST_PERMISSIONS_BASE_URL}#permission-on-pages",
    "pull_requests"                  => "#{REST_PERMISSIONS_BASE_URL}#permission-on-pull-requests",
    "repository_hooks"               => "#{REST_PERMISSIONS_BASE_URL}#permission-on-repository-hooks",
    "repository_pre_receive_hooks"   => "#{REST_PERMISSIONS_BASE_URL}#permission-on-repository-pre-receive-hooks",
    "repository_projects"            => "#{REST_PERMISSIONS_BASE_URL}#permission-on-repository-projects",
    "single_file"                    => "#{REST_PERMISSIONS_BASE_URL}#permission-on-single-file",
    "starring"                       => "#{REST_PERMISSIONS_BASE_URL}#permission-on-starring",
    "statuses"                       => "#{REST_PERMISSIONS_BASE_URL}#permission-on-statuses",
    "team_discussions"               => "#{REST_PERMISSIONS_BASE_URL}#permission-on-team-discussions",
    "team_repositories"              => "#{REST_PERMISSIONS_BASE_URL}#permission-on-team-repositories",
    "vulnerability_alerts"           => "#{GRAPHQL_OBJECT_BASE_URL}repositoryvulnerabilityalert/",
    "watching"                       => "#{REST_PERMISSIONS_BASE_URL}#permission-on-watching",
  }.freeze

  def available_repository_resources
    Repository::Resources.subject_types_for(integration)
  end

  def available_organization_resources
    Organization::Resources.subject_types_for(integration)
  end

  def available_user_resources
    User::Resources.subject_types_for(integration)
  end

  def human_name(resource)
    NAME_OVERRIDES[resource] || resource.humanize
  end

  def human_event_title(resource, event_type)
    return event_type.humanize unless HOOK_EVENT_OVERRIDES.key?(resource)
    "#{event_type} for #{HOOK_EVENT_OVERRIDES[resource]}".humanize
  end

  def description(resource)
    DESCRIPTIONS[resource]
  end

  def resource_docs_url(resource)
    RESOURCE_DOCS_URLS.fetch(resource, REST_PERMISSIONS_BASE_URL)
  end

  def event_docs_url(event)
    anchor = event.name.demodulize.downcase
    "#{GitHub.developer_help_url}/v3/activity/events/types/##{anchor}"
  end

  def granted_permissions
    integration.default_permissions
  end

  def granted_single_file_name
    integration.single_file_name
  end

  def granted_content_references
    integration.default_content_references.keys
  end

  def hook_events_for_resource(resource)
    event_types = Integration::Events.event_types_for_resource(
      resource,
      actor: current_user,
    )
    event_types.inject([]) do |result, event_type|
      result << Integration::Events.hook_event_for_event_type(event_type)
    end
  end

  def available_integrator_events
    Integration::Events::INTEGRATOR_EVENTS.each_with_object([]) do |event_type, events|
      feature_flag = Integration::Events::INTEGRATOR_EVENTS_AND_FEATURE_FLAGS[event_type]

      if feature_flag.nil? || feature_enabled?(feature_flag)
        event = Integration::Events.hook_event_for_event_type(event_type)
        events << event
      end
    end
  end

  def resource_permission_text(resource)
    permission_text(granted_permissions[resource])
  end

  def permission_text(permission)
    case permission
    when :admin
      "Admin"
    when :write
      "Read & write"
    when :read
      "Read-only"
    else
      "No access"
    end
  end

  def resource_permission_icon(resource)
    permission_icon(granted_permissions[resource])
  end

  def permission_icon(permission)
    case permission
    when :admin
      "person"
    when :write
      "pencil"
    when :read
      "eye"
    else
      "circle-slash"
    end
  end

  def event_permission_icon(event)
    event_selected?(event) ? "check" : "x"
  end

  def event_permission_icon_class(event)
    event_selected?(event) ? "text-green" : "text-red"
  end

  def event_selected?(event)
    event_type = event.event_type

    if Integration::Events::INTEGRATOR_EVENTS.include?(event_type)
      integration.integrator_events.include?(event_type)
    else
      integration.default_events.include?(event_type)
    end
  end

  def events_selected?(resource)
    hook_events_for_resource(resource).any? { |event| event_selected?(event) }
  end

  def resource_permission_icon_class(resource)
    permission_icon_class(granted_permissions[resource])
  end

  def permission_icon_class(permission)
    case permission
    when :admin, :write
      "text-green"
    when :read
      ""
    else
      "text-red"
    end
  end

  # Public: whether to show events for these resources.
  #
  # resource  - One or more String representing a supported Integration::Events
  #             resource type. E.g. "issues", or ["issues", "contents"]
  #
  # True if:
  #   * Any of the supplied resources have been selected as a permission.
  #
  # Returns a Boolean.
  def show_events_for_resource?(resources)
    resources = Array.wrap(resources)
    resources.any? do |resource|
      selected_permission?(resource, :read) ||
        selected_permission?(resource, :write)
    end
  end

  def event_types_to_resources
    integration_events.event_types_to_resources
  end

  # Public: Does the given resource have this permission?
  #
  # resource    - String representing the resource: E.g. "metadata", "issues".
  # permission  - Symbol representing the permission: :none, :read or :write.
  #
  # Returns a Boolean.
  def selected_permission?(resource, permission)
    permission == granted_permissions.fetch(resource, :none)
  end

  def readonly?(resource)
    Permissions::ResourceRegistry.readonly_subject_type?(resource)
  end

  def writeonly?(resource)
    Permissions::ResourceRegistry.writeonly_subject_type?(resource)
  end

  def admin_permission_available?(resource)
    Permissions::ResourceRegistry.adminable_subject_type?(resource)
  end

  def hide_content_references?
    !granted_permissions.include?("content_references") && !has_content_references_error?
  end

  def has_content_references_error?
    !integration.errors["content_references.value"].empty?
  end

  # Public: the parent of the given resource as a downcased string.
  #
  # E.g. "issues" => "repository", "members" => "organization" etc.
  #
  # Returns a String.
  def resource_parent(resource)
    "#{Permissions::ResourceRegistry.parent_of(resource)}".downcase
  end

  # Public: is the resource a mandatory permission given the currently granted
  # permissions on this Integration.
  #
  # Returns a Boolean.
  def mandatory_permission_selected?(resource)
    return false unless resource == "metadata"
    ((Repository::Resources.subject_types & granted_permissions.keys) - [resource]).any?
  end

  def human_event_description(event)
    INTEGRATOR_EVENT_DESCRIPTIONS[event.event_type] || event.description
  end

  def active_hook?
    return hook.active? if hook
    !!(integration.hook&.active?)
  end

  private

  def feature_enabled?(feature_flag)
    current_user_feature_enabled?(feature_flag) ||
      current_integration_feature_enabled?(feature_flag)
  end

  def current_integration_feature_enabled?(feature_flag)
    integration && GitHub.flipper[feature_flag].enabled?(integration)
  end

  def integration_events
    @integration_events ||= Integration::Events.new(actor: current_user)
  end
end
