# frozen_string_literal: true

class Integrations::ShowView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  BETA_FEATURE_FLAGS = {
    user_token_expiration: "user_token_expiration_opt_in",
    github_apps_brown_out: "github_apps_brown_out_opt_out",
    device_authorization_grant: "device_authorization_grant_opt_in"
  }

  BETA_FEATURE_MODEL_ATTRIBUTES = {
    "user_token_expiration_opt_in" => :user_token_expiration
  }

  BETA_FEATURE_NAMES = {
    "user_token_expiration_opt_in"   => "User-to-server token expiration",
    "github_apps_brown_out_opt_out"  => "Deprecated events brown-out",
    "device_authorization_grant_opt_in" => "Device authorization flow",
  }

  LAUNCHED_FEATURES = %i[
    user_token_expiration
  ]

  def self.toggle_feature(flag, value, integration:)
    return unless %w[enable disable].include?(value)
    return unless BETA_FEATURE_FLAGS.values.include?(flag)

    global_key = BETA_FEATURE_FLAGS.key(flag)
    return unless GitHub.flipper[global_key].enabled?(integration.owner)

    case value
    when "disable"
      GitHub.flipper[flag.to_sym].disable(integration)
    when "enable"
      GitHub.flipper[flag.to_sym].enable(integration)
    end
  end

  def self.toggle_feature_flash_message(flag, value, integration:)
    feature_name = BETA_FEATURE_NAMES[flag]
    past_tense_value = case value
    when "disable"
      "disabled"
    when "enable"
      "enabled"
    end

    "#{feature_name} is being #{past_tense_value} for #{integration.name}."
  end

  def self.model_owned_opt_in_attribute(flag)
    BETA_FEATURE_MODEL_ATTRIBUTES[flag]
  end

  def self.enable_or_disable_to_boolean(value)
    case value
    when "disable"
      false
    when "enable"
      true
    end
  end

  attr_reader :integration, :page

  delegate :pending_transfer?, :public?, :can_make_internal?, :description,
           :transfer, :hook, to: :integration

  delegate :pricing_url?, :pricing_url, :documentation_url?, :documentation_url,
           :tos_url?, :tos_url, :support_url?, :support_url,
           :status_url?, :status_url, :privacy_policy_url, to: :listing

  def show_features?
    features.any?
  end

  # Public: Determine if "Install app" link should be shown. Full-trust GitHub
  # App installations should never be manually installed by end-users, so
  # "Install app" should never be visible for those apps. Otherwise, checks to
  # make sure the integration is installable anywhere by the current user.
  #
  # Returns a Boolean.
  def hide_install_app_section?
    return true unless Apps::Internal.capable?(:user_installable, app: integration)
    return true unless integration.installable_by?(current_user)
    false
  end

  # Public: Determine if a field for setting the given GitHub App's bgcolor should be shown in the
  # page.
  #
  # Returns a Boolean.
  def allow_editing_bgcolor?
    if GitHub.enterprise?
      integration.primary_avatar.present?
    else
      return false unless integration.primary_avatar

      listing = integration.marketplace_listing
      listing.nil? || !listing.publicly_listed?
    end
  end

  def features
    return [] unless listing && listing.features.any?

    listing.features
  end

  def beta_features?
    BETA_FEATURE_FLAGS.any? do |(global_flag, _opt_in)|
      GitHub.flipper[global_flag].enabled?(integration.owner)
    end
  end

  def beta_feature_available?(flag)
    global_key = BETA_FEATURE_FLAGS.key(flag)
    return true if LAUNCHED_FEATURES.include?(global_key)

    GitHub.flipper[global_key].enabled?(integration.owner)
  end

  def beta_feature_enabled?(flag)
    if (attribute = self.class.model_owned_opt_in_attribute(flag))
      integration[attribute]
    else
      GitHub.flipper[flag].enabled?(integration)
    end
  end

  def beta_feature_toggle_value(flag)
    beta_feature_enabled?(flag) ? "disable" : "enable"
  end

  def beta_feature_toggle_submit_value(flag)
    beta_feature_enabled?(flag) ? "Opt-out" : "Opt-in"
  end

  def show_more_info?
    listing.present?
  end

  def selected_link
    :integrations
  end

  def hook_url
    hook&.url
  end

  def make_internal_tooltip_content
    reason = listing.present? ? "part of the Integrations Directory." : "already installed on other accounts."
    "This integration cannot be made internal since it is #{reason}"
  end

  def events
    integration.default_events.map do |event_type|
      Hook::EventRegistry.for_event_type(event_type)
    end
  end

  def transfer_target
    transfer.target
  end

  def keys_classes
    klasses = []
    klasses << "has-keys" if integration.public_keys.any?
    klasses << "multi-keys" if integration.public_keys.count > 1
    klasses.join(" ")
  end

  private

  def listing
    @listing ||= integration.integration_listing
  end
end
