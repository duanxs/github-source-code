# frozen_string_literal: true

module Notifications
  class SummaryCollection
    #
    # user      - the viewer of the notifications
    # summaries - a collection of RollupSummary#to_summary_hash hashes
    # ignored   - the ignored set of summary ids. Optional.
    #
    def initialize(user:, summaries:, ignored: [])
      @user = user
      @summaries = summaries
      @ignored = ignored

      @user_ids = Set.new

      # A hash dictionary of lists by type:
      # { "Repository" => { 5 => [view, view, ..], 42 => [view, ..] }, "Team" => { 13 => [view] } }
      @list_dictionary = {}
    end

    # Public: Prepares the unique lists from the summaries
    # by wrapping individual summaries in a SummaryView and
    # grouping them by list.
    #
    # Returns an Array of tuples of a list and its views.
    # e.g. [[list, [views], [list, [views]]]
    def views_grouped_by_list
      lists = @summaries.map do |summary|
        view = build_summary_view(summary)

        # Extract commenter ids from the summary
        commenter_ids = view.commenter_structs.map { |s| s.id }
        @user_ids.merge(commenter_ids)
        @user_ids.add(view.commenter_id)

        # Add list view to dictionary
        @list_dictionary[view.list_type] ||= {}
        list_views = @list_dictionary[view.list_type][view.list_id] ||= []
        list_views << view

        List.new(summary_hash: summary)
      end.uniq

      group_views_by_list(lists)
    end

    private

    # A utility class for representing a notification list.
    class List
      attr_accessor :id, :type

      def initialize(summary_hash:)
        @id = summary_hash[:list][:id].to_i
        @type = summary_hash[:list][:type]
      end

      def eql?(other)
        hash.eql?(other.hash)
      end
      alias == eql?

      def hash
        id.hash ^ type.hash
      end
    end

    # Groups the Notification::SummaryView objects by list.
    #
    # lists - an Array of List objects
    #
    # Returns an Array of tuples of a list and its views.
    # e.g. [[list, [views], [list, [views]]]
    def group_views_by_list(lists)
      lists.map do |list|
        list_record = preloaded_lists[list.type][list.id]

        views = summary_views_for(list).each do |view|
          view.list = list_record
          view.user_id_map = users
        end

        [list_record, views]
      end
    end

    # Private: Build a Notifications::SummaryView from supplied data.
    #
    # summary - a RollupSummary#to_summary_hash hash.
    #
    # Returns a Notifications::SummaryView.
    def build_summary_view(summary)
      view = Notifications::SummaryView.new(summary)
      view.current_user = @user
      view.muted = @ignored.include?(Integer(summary[:id]))
      view
    end

    # Private: The Notifications::SummaryView objects for a specific list.
    #
    # list - a List object
    #
    # Returns an Array of Notifications::SummaryView objects.
    def summary_views_for(list)
      @list_dictionary[list.type][list.id]
    end

    # Private: Returns a Hash of preloaded list records indexed by list type.
    def preloaded_lists
      {
        "Repository" => repositories,
        "Team" => teams,
        "User" => users,
        "Organization" => orgs,
      }
    end

    # Private: Returns the collection of repositories in the unique repository ids list.
    def repositories
      @repositories ||= ::Repository.includes(:owner).where(id: list_ids("Repository")).index_by { |r| r.id }
    end

    # Private: Returns the collection of teams in the unique team ids list.
    def teams
      @teams ||= ::Team.includes(organization: :saml_provider).where(id: list_ids("Team")).index_by { |t| t.id }
    end

    # Private: Returns the collection of users in the unique user ids list.
    def users
      return @users if defined?(@users)
      ids = @user_ids + list_ids("User")
      @users = ::User.includes(:primary_user_email).where(id: ids.to_a).index_by { |u| u.id }
    end

    def orgs
      @orgs ||= ::Organization.where(id: list_ids("Organization")).index_by { |o| o.id }
    end

    # Private: Returns an Array of list ids present in the list dictionary for the given list type.
    #
    # list_type - String representing a list type
    #
    def list_ids(list_type)
      @list_dictionary[list_type].try(:keys) || []
    end
  end
end
