# frozen_string_literal: true

module Notifications
  class ListView
    attr_reader :list, :total_count, :unshown_count

    # Represents a notification list object (e.g Repository or Team), and its associated
    # notifications.
    #
    # index_view    - an IndexView object.
    # list          - a Repository or a Team.
    # summary_views - Array of Notifications::SummaryView objects for this list.
    def initialize(index_view, list, summary_views)
      @index_view = index_view
      @list = list
      @summary_views = summary_views
      @total_count = @index_view.per_list_count(@list)
      @unshown_count = total_count - @summary_views.size
    end

    # Public: Returns a String for the type of the list.
    def list_type
      list.class.name.downcase
    end

    # Public: Returns the owning User of the list.
    def list_owner
      fail NotImplementedError
    end

    # Public: Returns a String for the name of the list.
    def name
      @list.name
    end

    # Public: Returns a String for canonical URL of the list.
    def path
      @list.permalink
    end

    # Public: Returns a String that combines the owning user and the list name.
    def name_with_owner
      @list.name_with_owner
    end

    # Public: Yields each Notifications::SummaryView object queried for this list.
    def each(&block)
      @summary_views.each(&block)
    end

    # Public: Returns the Integer number of notifications queried for this list.
    def size
      @summary_views.size
    end

    # Public: Returns a Boolean stating whether there are more notifications for this list.
    def more?
      unshown_count > 0
    end

    # Public: Returns a URL that points notification for just this list.
    #
    # Returns a String.
    def more_url
      fail NotImplementedError
    end

    # Public: Returns a Boolean indicating if pagination links should be shown.
    def show_pagination_links?
      current_list? && total_count > size
    end

    # Internal: Whether or not the user is viewing notifications for just this list.
    private def current_list?
      @index_view.current_list?(@list)
    end
  end
end
