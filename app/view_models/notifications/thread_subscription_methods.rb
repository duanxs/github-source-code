# frozen_string_literal: true

module Notifications
  module ThreadSubscriptionMethods
    def thread
      raise "#thread must be defined on classes that include ThreadSubscriptionMethods"
    end

    def repo
      raise "#repo must be defined on classes that include ThreadSubscriptionMethods"
    end

    def form_action
      case form_state
      when :mute               then "mute"
      when :unsubscribe        then "unsubscribe"
      when :unmute, :subscribe then "subscribe"
      when :unavailable        then "unavailable"
      end
    end

    def octicon_symbol
      case form_state
      when :unmute      then "unmute"
      when :mute        then "mute"
      when :unsubscribe then "mute"
      when :subscribe   then "unmute"
      when :unavailable then "mute"
      end
    end

    def button_text
      case form_state
      when :unmute      then "Subscribe"
      when :mute        then "Unsubscribe"
      when :unsubscribe then "Unsubscribe"
      when :subscribe   then "Subscribe"
      when :unavailable then "Unavailable"
      end
    end

    # Public: Are notifications disabled for discussions in this particular list?
    #
    # Returns a Boolean.
    def disable_discussions_notifications_flag_enabled?
      list.try(:disable_discussions_notifications_flag_enabled?)
    end

    # Public: Are notifications disabled for this thread?
    def notifications_disabled_for_thread?
      thread.is_a?(Discussion) && disable_discussions_notifications_flag_enabled?
    end

    def explanation_text
      case thread_status
      when :unavailable
        "Notifications are unavailable."
      when :subscribed_to_thread_events
        "You’re receiving notifications because you chose custom settings for this thread."
      when :subscribed_to_thread
        "You’re receiving notifications because #{GitHub.newsies.reason_in_words(thread_status_response.reason)}."
      when :subscribed_to_list
        "You’re receiving notifications because you’re watching this repository."
      when :ignoring_thread
        "You’re not receiving notifications from this thread."
      when :ignoring_list
        "You’re ignoring this repository."
      when :disabled
        "Notifications are disabled for #{thread_human_name.pluralize} in this repository."
      else
        "You’re not receiving notifications from this thread."
      end
    end

    def list_id
      @list_id ||= Newsies::List.to_id(list)
    end

    def thread_class
      @thread_class ||= Newsies::Thread.to_type(thread).demodulize
    end

    # Public: Get a human-friendly name for the kind of thread this is.
    #
    # Returns a String like "discussion", "issue", or "pull request".
    def thread_human_name
      if thread.respond_to?(:pull_request?) && thread.pull_request?
        "pull request"
      else
        thread_class.underscore.humanize.downcase
      end
    end

    def thread_id
      @thread_id ||= Newsies::Thread.to_id(thread)
    end

    def thread_status
      @thread_status ||= if thread_status_response.failed? || repo_status_response.failed?
        :unavailable
      elsif notifications_disabled_for_thread?
        :disabled
      elsif thread_status_response.events_only?
        :subscribed_to_thread_events
      elsif repo_status_response.subscribed? && !thread_status_response.ignored?
        :subscribed_to_list
      elsif thread_status_response.subscribed?
        :subscribed_to_thread
      elsif repo_status_response.ignored?
        :ignoring_list
      elsif thread_status_response.ignored?
        :ignoring_thread
      else
        :none
      end
    end

    def subscribed_to_event?(event)
      thread_status == :subscribed_to_thread_events && thread_status_response.events.include?(event)
    end

    def custom_notifications_supported_for_thread?
      subscribable_events.present?
    end

    def subscribable_events
      # treat an issue with a pull_request as a pull request
      is_pull_request = thread.is_a?(PullRequest) || (thread.is_a?(Issue) && thread.pull_request?)
      return ["merged", "closed", "reopened"] if is_pull_request

      return ["closed", "reopened"] if thread.is_a?(Issue)

      return []
    end

    def visible_thread_type
      visible_thread.class.name.underscore.humanize(capitalize: false)
    end

    # What action should selecting "subscribe" from the modal take?
    def modal_subscribe_action
      if repo_status_response.subscribed? && !thread_status_response.valid?
        # The user is already watching the repo, and no thread subscription row exists (for
        # unsubscribing from this thread) that needs to be overwritten, so there is no need to
        # create a thread subscription row.
        #
        # This helps maintain consistency with the fact that outside the modal we don't even present
        # the "Subscribe" option if you're watching the repo, so we don't create a thread
        # subscription row there either.
        :noop
      else
        # Otherwise, we indeed want to create or update a thread subscription row.
        :subscribe
      end
    end

    # What action should selecting "unsubscribe" from the modal take?
    def modal_unsubscribe_action
      if thread_status_response.ignored?
        # if already muting the thread, continue to do so when "unsubscribing"
        :mute
      elsif thread_status_response.mentioned? || repo_status_response.subscribed?
        # if we watching the repo, must mute to "unsubscribe"
        :mute
      else
        :unsubscribe
      end
    end

    private

    def form_state
      @form_state ||= if thread_status_response.failed? || repo_status_response.failed?
        :unavailable
      elsif thread_status_response.ignored?
        :unmute
      elsif thread_status_response.mentioned? || repo_status_response.subscribed?
        :mute
      elsif thread_status_response.subscribed?
        :unsubscribe
      else
        :subscribe
      end
    end

    def repo_status_response
      @repo_status_response ||= GitHub.newsies.subscription_status(current_user, list)
    end

    def thread_status_response
      @thread_status_response ||= GitHub.newsies.subscription_status(current_user, list, thread)
    end

    def visible_thread
      @visible_thread ||= (thread.is_a?(Issue) && thread.pull_request?) ? thread.pull_request : thread
    end
  end
end
