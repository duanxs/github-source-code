# frozen_string_literal: true

module Notifications
  class ExternalIdentitySessionNeededView
    attr_reader :repository

    # Represents a Repository, and its Notifications.
    #
    # repository - A Repository.
    # total_notifications - an Integer number of total notifications for this list.
    def initialize(repository, total_notifications)
      @repository = repository
      @total_notifications = total_notifications
    end

    # Public: Returns the String {user}/{repo} path for the Repository.
    def name_with_owner
      @repository.name_with_owner
    end

    # Public: Returns the Integer number of total notifications for this
    # Repository.
    def total
      @total_notifications
    end

    def organization
      repository.owner
    end
  end
end
