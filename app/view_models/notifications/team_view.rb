# frozen_string_literal: true

module Notifications
  class TeamView < ListView

    # Overrides Notifications::ListView#list_owner.
    def list_owner
      @list.organization
    end

    # Overrides Notifications::ListView#more_url.
    def more_url
      query = { list_type: list_type }
      query[:page] = :next if current_list?
      @index_view.build_url(@list, query)
    end
  end
end
