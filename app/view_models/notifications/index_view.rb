# frozen_string_literal: true

module Notifications
  class IndexView
    include Scientist

    attr_reader \
      :params,
      :controller,
      :user,
      :list,
      :unauthorized_saml_targets,
      :unauthorized_ip_whitelisting_targets

    # Controls the notifications listing / reading view for a logged in user.
    #
    # user                        - The currently logged in user.
    # list                        - The current list if any (optional).
    # controller                  - The controller instance building this view model.
    # unauthorized_saml_targets   - An Array of Organizations and/or Businesses which the user is not
    #                               currently authorized to view notifications from due to external
    #                               identity session enforcement.
    # unauthorized_ip_whitelisting_targets - An Array of Organizations and/or Businesses which the user is not
    #                               currently authorized to view notifications from due to
    #                               IP allow list enforcement.
    # count_limit                 - Optional Integer that specifies number above which the count of
    #                               notifications returned from this class are allowed to be inaccurate
    #                               in order to improve performance. If not provided, we default to the
    #                               accurate counting method.
    def initialize(user:, list: nil, controller:, unauthorized_saml_targets: [], unauthorized_ip_whitelisting_targets: [], count_limit: 10_000)
      @user = user
      @list = list
      @controller = controller
      @params = controller.params
      @unauthorized_saml_targets = unauthorized_saml_targets
      @unauthorized_ip_whitelisting_targets = unauthorized_ip_whitelisting_targets
      @count_limit = count_limit
      @bad_lists = { "Repository" => Set.new, "Team" => Set.new }
    end

    # Public: Should we attempt to show the index view or not. If true, yes.
    # If false, we should let the user know that notifications are unavailable.
    #
    # IMPORTANT: Any new notifications calls should be checked here as well.
    #
    # Returns true or false.
    def success?
      (
        notifications_response.success? &&
        per_list_counts_response.success? &&
        ignored_set_response.success? &&
        unread_count_response.success? &&
        participating_unread_count_response.success? &&
        read_count_response.success?
      )
    end

    def failed?
      !success?
    end

    def inbox_zero?
      success? && (filter.unread || filter.read) && notifications.size == 0
    end

    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def web_notifications_enabled?
      @web_notifications_enabled ||= GitHub.newsies.web_notifications_enabled?(@user)
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

    # Public: Whether you're viewing the notifications for a single list.
    #
    # list - a Repository or Team
    #
    # Returns a Boolean.
    def current_list?(list)
      @list == list
    end

    def unread_count_string
      format_count_response(unread_count_response)
    end

    def read_count_string
      format_count_response(read_count_response)
    end

    def participating_unread_count_string
      format_count_response(participating_unread_count_response)
    end

    def saved_count_string
      format_count_response(saved_count_response)
    end

    # Public: Yields a list view (RepositoryView or TeamView) for every list
    # in the queried notifications.
    def each_list_view
      views_grouped_by_list.each do |list, views|
        if list.nil?
          # do nothing, bring the pain in a slow, throttled db reaper
        elsif allowed_ip_needed?(list)
          total_notifications = per_list_count(list)
          yield AllowedIpNeededView.new(list, total_notifications)
        elsif external_identity_session_needed?(list)
          total_notifications = per_list_count(list)
          yield ExternalIdentitySessionNeededView.new(list, total_notifications)
        else
          view = Notifications::ListViewFactory.build_view(
            index_view: self, list: list, summary_views: views,
          )

          if view.present?
            yield view
          else
            views.each { |view| @bad_lists[view.list_type] << view.list_id.to_i }
          end
        end
      end
    ensure
      cleanup_bad_lists
    end

    # Public: Returns the Array of Notification Summary Hash objects for this
    # user and the current filter parameters.
    def notifications
      notifications_response.value
    end

    # Public: Returns an Array of saved RollupSummary ids.
    def saved_rollup_summary_ids
      @saved_rollup_summary_ids ||= saved_rollup_summary_ids_response.value
    end

    # Public: Returns an array of Repository Invitation objects for this user.
    #
    def repository_invitations
      @repository_invitations ||= @user.received_repository_invitations
    end

    # Public: Returns counts Notifications for a User, with values grouped by
    # Repository.
    def per_list_counts
      @per_list_counts ||= per_list_counts_response.value.sort_by { |repo, count| -count }
    end

    # Public: Returns a Boolean determining if there are #per_list_counts.
    def per_list_counts?
      per_list_counts_response.success? && per_list_counts.present?
    end

    # Public: Returns an Integer count of unread notifications for a repo.
    def per_list_count(repo)
      _, count = per_list_counts_response.value.detect { |(repository, _)| repo == repository }
      count.to_i
    end

    # Public: Returns the time of the most recent Notification.
    def mark_time
      @mark_time ||= [notifications.first[:updated_at].to_i + 1, Time.now.to_i].min
    end

    # Public: Navigation helper. Should we highlight the unread nav item?
    #
    # Returns true to highlight.
    def highlight_unread?
      !@list && filter.unread && !filter.read && !filter.participating && !filter.saved
    end

    # Public: Navigation helper. Should we highlight the read nav item?
    #
    # Returns true to highlight.
    def highlight_read?
      filter.read && !@list && !filter.unread && !filter.participating && !filter.saved
    end

    # Public: Navigation helper. Should we highlight the saved for later nav item?
    #
    # Returns true to highlight.
    def highlight_saved?
      !@list && filter.unread && filter.saved
    end

    # Public: Navigation helper. Should we highlight the participating nav item?
    #
    # Returns true to highlight.
    def highlight_participating?
      !@list && filter.unread && filter.participating
    end

    # Public: Navigation helper. Should we highlight the all notifications nav
    # item?
    #
    # Returns true to highlight.
    def highlight_all?
      !@list && viewing_all?
    end

    # Public: Checks to see if the given Summary has been muted.
    #
    # summary - A Summary Hash (probably from #notifications).
    #
    # Returns a Boolean.
    def muted?(summary)
      ignored_set.include?(Integer(summary[:id]))
    end

    # Public: Checks to see if the given Summary has been saved.
    #
    # summary_id - A RollupSummary id.
    #
    # Returns a Boolean.
    def saved_summary?(summary_id)
      saved_rollup_summary_ids.include?(summary_id.to_i)
    end

    def url(query = nil)
      build_url(nil, query)
    end

    def build_url(list = nil, query = nil)
      prefix = list ? "/#{list.name_with_owner}" : nil
      "#{prefix}/notifications#{url_query(query)}"
    end

    def url_query(query = nil)
      query ||= {}
      if query[:page] == :next
        query[:page] = filter.page + 1
      elsif filter.page > 1
        query[:page] ||= filter.page
      end
      if !filter.unread && !filter.read
        query[:all] = 1
      end
      if filter.read
        query[:filter] == "read"
      end
      if filter.participating
        query[:filter] = "participating"
      end
      query.present? ? "?#{query.to_query}" : ""
    end

    def ignored_set
      ignored_set_response.value
    end

    def filter
      @filter ||= begin
        filter = Newsies::Web::FilterOptions.new
        filter.list = @list
        filter.page = @params[:page]
        filter.read = @params[:filter] == "read"
        filter.unread = @params[:all] != "1" && @params[:filter] != "read"
        filter.participating = @params[:filter] =~ /^participating$/i
        filter.saved = @params[:filter] == "saved"
        filter
      end
    end

    # Internal: Notifications grouped by list.
    #
    # Returns an Array of tuples of a list, and an Array SummaryViews.
    def views_grouped_by_list
      collection = Notifications::SummaryCollection.new(
        user: @user,
        summaries: notifications,
        ignored: ignored_set,
      )
      collection.views_grouped_by_list
    end

    # Internal: Returns boolean to show Saved for later list on saved for later view
    def display_saved_threads?
      filter.saved
    end

    # Internal: Returns Boolean for whether or not to show the tab for read notifications.
    def display_read?
      filter.read
    end

    # Internal: Returns Boolean for whether to show the "Mark all as read" button.
    def display_mark_all_read?
      success? && !inbox_zero? && !display_saved_threads? && !display_read?
    end

    # Internal: The amount of saved notification threads we want to display.
    def saved_threads_limit
      if highlight_saved?
        50
      else
        3
      end
    end

    # Public: Navigation helper. Return a string of either paticipating, read or unread
    # depending on what filter is currently being used.
    # This is used to add the current filter to the repository/team view links.
    #
    # Returns String.
    def active_filter
      return "participating" if filter.participating
      return "read" if filter.read
    end

    # Public: Navigation helper. Returns a boolean as to whether the user is viewing
    # all notifications or not.
    def viewing_all?
      !filter.unread && !filter.read
    end

    private

    def notifications_response
      @notifications_response ||= GitHub.newsies.web.all(@user, filter)
    end

    def ignored_set_response
      @ignored_set_response ||= GitHub.newsies.ignored(@user, notifications)
    end

    def per_list_counts_response
      @per_list_counts_response ||= begin
        GitHub.dogstats.time("newsies.view.per_list_counts") do

          # In this method we're always looking for counts independent of a particular list (so
          # that we can render all lists in the sidebar for navigation).
          list_independent_filter = filter.to_hash.except(:list)

          counts_response = GitHub.newsies.web.counts_by_list(@user, list_independent_filter)

          if counts_response.success?
            GitHub.dogstats.count("newsies.view.per_list_counts.size", counts_response.value.size)
          end

          # Always include the current list in the response so that we can highlight it in the
          # sidebar even if the count is zero.
          if counts_response.success? && @list && !counts_response.value.map(&:first).include?(@list)
            counts_response.value << [@list, 0, 0]
          end

          counts_response
        end
      end
    end

    def unread_count_response
      @unread_count_response ||= begin
        GitHub.dogstats.time("newsies.view.unread_count_response") do
          GitHub.newsies.web.count(
            @user,
            { unread: true, not_spam: true, count_limit: @count_limit })
        end
      end
    end

    def read_count_response
      @read_count_response ||= begin
        GitHub.dogstats.time("newsies.view.read_count_response") do
          GitHub.newsies.web.count(
            @user,
            { read: true, count_limit: @count_limit })
        end
      end
    end

    def participating_unread_count_response
      @participating_unread_count_response ||= GitHub.newsies.web.count(
        @user,
        { unread: true, participating: true, count_limit: @count_limit })
    end

    def saved_count_response
      @saved_count_response ||= begin
        GitHub.dogstats.time("newsies.view.saved_count_response") do
          GitHub.newsies.web.count_saved(@user)
        end
      end
    end

    def saved_rollup_summary_ids_response
      @saved_rollup_summary_ids_response ||= begin
        GitHub.dogstats.time("newsies.view.saved_rollup_summary_ids") do
          summary_ids = notifications.flat_map { |summary| summary[:id] }.compact.uniq
          GitHub.newsies.web.all_saved_by_summary_ids(@user, summary_ids)
        end
      end
    end

    # Private: Determines if the current user is allowed to view notifications
    # for the given list depending on if it is an org list, that org has
    # SAML SSO configured, and the current user does not have an active SSO session.
    #
    # list - A notifications list ActiveRecord object (e.g. Repository)
    #
    # Returns a Boolean.
    def external_identity_session_needed?(list)
      return false if list.owner.user?
      unauthorized_saml_targets.include?(list.owner.external_identity_session_owner)
    end

    # Private: Determines if the current user is allowed to view notifications
    # for the given list depending on if it is an org list, that org has
    # an IP allow list configured, and the current user is not connecting from
    # an allowed IP.
    #
    # list - A notifications list ActiveRecord object (e.g. Repository)
    #
    # Returns a Boolean.
    def allowed_ip_needed?(list)
      return false unless list.owner.organization?
      org = list.owner
      business = org&.business
      unauthorized_ip_whitelisting_targets.include?(org) ||
        unauthorized_ip_whitelisting_targets.include?(business)
    end

    # Private: Enqueues a job to delete notifications for lists that aren't accessible.
    #
    # list_hash - a Hash with the list types as the keys and list ids as the values.
    #             e.g. {"Repository" => Set.new([15,25,19]), "Team" => Set.new([42, 48])}
    #
    # Returns nothing.
    def cleanup_bad_lists
      @bad_lists.each do |type, ids|
        next if ids.empty?

        CleanupListNotificationsJob.perform_later(@user.id, type, ids.to_a)
        GitHub.dogstats.count("newsies.cleanup.inaccessible_lists", ids.length, tags: ["method:cleanup_bad_lists"])
      end
    end

    def format_count_response(response)
      count_result = response.value
      suffix = "+" if @count_limit && !count_result.accurate?
      "#{count_result.to_i}#{suffix}"
    end
  end
end
