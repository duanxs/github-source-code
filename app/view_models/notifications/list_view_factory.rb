# frozen_string_literal: true

module Notifications
  class ListViewFactory
    class UnsupportedList < StandardError; end

    # Builds a list view for a notification list and its notifications.
    #
    # index_view     - an instance of Notifications::IndexView
    # list           - a notifications list ActiveRecord object (e.g. Repository, Team)
    # summary_views  - a collection of Notifications::SummaryView objects
    #
    def self.build_view(index_view:, list:, summary_views:)
      @index_view = index_view
      @summary_views = summary_views

      case list
      when Repository
        build_repository_view(list)
      when Team
        build_team_view(list)
      when User
        build_user_view(list)
      else
        raise UnsupportedList
      end
    end

    def self.build_repository_view(repository)
      deleted  = repository.deleted?
      pullable = repository.pullable_by?(@index_view.user)
      invited  = repository.has_invitation_for?(@index_view.user)

      if !deleted && (pullable || invited)
        RepositoryView.new(@index_view, repository, @summary_views)
      end
    end
    private_class_method :build_repository_view

    def self.build_team_view(team)
      if team.visible_to?(@index_view.user)
        TeamView.new(@index_view, team, @summary_views)
      end
    end
    private_class_method :build_team_view

    def self.build_user_view(user)
      UserView.new(@index_view, user, @summary_views)
    end
    private_class_method :build_user_view
  end
end
