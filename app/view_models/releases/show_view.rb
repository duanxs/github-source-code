# frozen_string_literal: true

module Releases
  class ShowView < Releases::View
    def downloads_for(release)
      release.uploaded_assets
    end

    def downloads_including_tags_count(release)
      count = downloads_for(release).count
      count = count + 2 if release.tagged? # include source code files
      count
    end

    def commits_behind_latest(repository, latest_oid, release_oid)
      GitHub::Comparison.deprecated_build(repository, latest_oid, release_oid).relationship.first
    end
  end
end
