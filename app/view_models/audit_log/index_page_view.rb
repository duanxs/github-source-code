# frozen_string_literal: true

class AuditLog::IndexPageView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
  class MissingFiltersPartialError < StandardError; end

  # View model attributes
  attr_reader :query
  attr_reader :valid_query
  attr_reader :page
  attr_reader :after
  attr_reader :before
  attr_reader :git_export_enabled
  alias_method :git_export_enabled?, :git_export_enabled

  alias_method :valid_query?, :valid_query

  def after_initialize
    # Set @country var from query before we execute it
    @country = init_country
    # Execute query and cache results
    results
    # Everything went well, the query is valid
    @valid_query = true
  rescue Driftwood::TwirpUtil::InvalidArgumentError, ArgumentError
    # User provided an invalid query
    @valid_query = false
  end

  def page_title

  end

  def page_class

  end

  def aggregations_enabled?
    false
  end

  # Public: The title of the current search or the default title
  #
  # Returns a String
  def search_title
    return "The supplied query is invalid" unless valid_query
    if query
      size = driftwood_results? ? results.size : results.total_entries
      "Found #{helpers.number_with_delimiter(size)} #{'event'.pluralize(size)}"
    else
      "Recent events"
    end
  end

  # Public: Whether we're actively searching for something beyond the default
  # results view.
  #
  # Returns a Boolean
  def active_search?
    query.present?
  end

  # Public: Whether to show the export results button.
  #
  # Returns a Boolean.
  def show_export_all?
    audit_logs?
  end

  # Public: The current selected country from the search query.
  #
  # Returns String if country present, false if not.
  def selected_country
    @country
  end

  # Public: Fetch all matching audit log entries matching the organization
  # scoped user query.
  #
  # Returns Array of Audit::Elastic::Hits
  def results
    @results ||= search_query.execute
  end

  def driftwood_results?
    results.is_a?(Driftwood::SearchResults)
  end

  def country_data
    out = {}
    return out unless valid_query?
    if results.aggregations["country_code"].present?
      if driftwood_results?
        JSON.parse(results.aggregations)["country_code"]["buckets"].each do |obj|
          out[obj["key"]] = obj["doc_count"]
        end
      else
        results.aggregations["country_code"].terms.each do |obj|
          out[obj["key"].upcase] = obj["doc_count"]
        end
      end
    end
    out
  end

  # Public: The audit logs for this user (or org).
  #
  # Returns an Array of Audit::Elastic::Hits.
  def audit_logs
    @audit_logs ||= begin
      entries = normalize(results.results)
      GitHub::PrefillAssociations.for_audit_log(entries)
      entries
    end
  end

  # Public: Check if there are audit logs for this user/org/enterprise account.
  #
  def audit_logs?
    valid_query? && audit_logs.any?
  end


  def normalize(entries)
    AuditLogEntry.new_from_array(entries)
  end

  def suggestions_path

  end

  def search_path
    raise NotImplementedError
  end

  def filters_partial
    raise MissingFiltersPartialError, "filters_partial is not set for #{self.class}"
  end

  def build_query_param(replace = {}, append = [])
    components = Search::Queries::AuditLogQuery.parse(query)

    replace.each do |key, value|
      if component = components.assoc(key)
        if value
          component[1] = value
        else
          components.delete(component)
        end
      elsif value
        components << [key, value]
      end
    end

    components.concat(append)

    Search::Queries::AuditLogQuery.stringify(components)
  end

  def tips
    raise NotImplementedError
  end

  def export_path
    raise NotImplementedError
  end

  def export_git_event_path
    raise NotImplementedError
  end

  def next_cursor
    if @results.respond_to?(:after_cursor)
      @results.after_cursor
    end
  end

  def prev_cursor
    if @results.respond_to?(:before_cursor)
      @results.before_cursor
    end
  end

  def prev_page?
    if @results.respond_to?(:has_previous_page?)
      @results.has_previous_page?
    end
  end

  def next_page?
    #Driftwood::Results does not have next_page.present?
    if @results.respond_to?(:has_next_page?)
      @results.has_next_page?
    end
  end

  def prev_page_params
    page_params(before: prev_cursor)
  end

  def next_page_params
    page_params(after: next_cursor)
  end

  def user_time_now(add: 0)
    zone = current_user&.time_zone || Time.zone
    (zone.now + add).strftime("%Y-%m-%dT%H:%M")
  end

  private

  # Private: The organization scoped audit log ElasticSearch query.
  #
  # Returns Hash
  def es_query
    raise NotImplementedError
  end

  def search_query
    @search_query ||= Audit::Driftwood::Query.new_org_business_query(es_query)
  end

  def page_params(after: "", before: "")
    pg_params = []
    pg_params << "q=#{query}" if active_search?
    pg_params << "after=#{after}" unless after.blank?
    pg_params << "before=#{before}" unless before.blank?
    if pg_params.any?
      "?#{pg_params.join('&')}"
    end
  end

  # Private: return country from query
  def init_country
    if search_query.respond_to?(:qualifiers)
      country = search_query.qualifiers[:country]
      country.must.first if country.must?
    else
      country = nil
    end
  end
end
