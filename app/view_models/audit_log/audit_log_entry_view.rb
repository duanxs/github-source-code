# frozen_string_literal: true

module AuditLog
  class AuditLogEntryView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    include ActionView::Helpers::TagHelper
    include CommentsHelper
    include AvatarHelper
    attr_reader :entry, :view

    def initialize(entry, view = nil)
      @entry = entry
      @view = view
    end

    def explanation
      case entry.hit.data[:explanation]
      when "stale"
        "was marked as stale"
      when "new_hire"
        "was unverified during the GitHub onboarding process"
      when "password_changed"
        "due to password changed"
      when "password_reset"
        "due to password reset"
      when "password_randomized"
        "due to password randomized"
      end
    end

    def link_to_blocked_user
      helpers.link_to(entry.hit.data[:blocked_user], "/#{entry.hit.data[:blocked_user]}")
    end

    def display_user_avatar?
      !entry.own_entry? && !entry.user_id.nil? && entry.org_id != entry.user_id
    end

    def team_name
      entry.hit.data[:team] && entry.hit.data[:team].split("/").last
    end

    def visibility
      entry.hit.data[:visibility] || entry.hit.data[:access]
    end

    def previous_visibility
      visibility == "private" ? "public" : "private"
    end

    def team_access
      entry.hit.data[:permission] && Team::PERMISSIONS_TO_ABILITIES[entry.hit.data[:permission]]
    end

    def old_team_access
      entry.hit.data[:old_permission] && Team::PERMISSIONS_TO_ABILITIES[entry.hit.data[:old_permission]]
    end

    def team_name_was
      "#{entry.hit[:org]}/#{entry.hit.data[:name_was]}"
    end

    REMOVE_MEMBER_REASONS_HASH = {
      Organization::RemovedMemberNotification::TWO_FACTOR_REQUIREMENT_NON_COMPLIANCE => "2fa non-compliance",
      Organization::RemovedMemberNotification::SAML_EXTERNAL_IDENTITY_MISSING => "SAML external identity missing",
      Organization::RemovedMemberNotification::TWO_FACTOR_ACCOUNT_RECOVERY => "account recovery due to 2fa lockout",
      Organization::RemovedMemberNotification::USER_ACCOUNT_DELETED => "user account deletion",
    }.with_indifferent_access.freeze

    # returns the reason why a member was removed from an organization
    #
    # Returns nil or a string
    def remove_member_reason
      REMOVE_MEMBER_REASONS_HASH[entry.hit.data["reason"]]
    end

    def remove_member_reason_text
      reason = remove_member_reason
      return "" unless reason.present?

      hit_reason = entry.hit.data["reason"]

      conjunction = "for"
      if hit_reason == Organization::RemovedMemberNotification::USER_ACCOUNT_DELETED
        conjunction = "on"
      end

      "#{conjunction} #{reason}"
    end

    def remove_member_verb_text
      if entry.own_entry?
        "removed themselves"
      else
        "was removed"
      end
    end

    def team_privacy
      normalized_team_privacy(entry.hit.data[:privacy])
    end

    def team_privacy_was
      normalized_team_privacy(entry.hit.data[:privacy_was])
    end

    def link_to_parent_team
      parent_team_id = entry.hit.data[:parent_team_id]

      if parent_team_id.nil?
        "root"
      elsif parent_team = Team.find_by_id(parent_team_id)
        helpers.link_to(parent_team.name, "/orgs/#{entry.hit[:org]}/teams/#{parent_team.name}")
      else
        "[team not found]"
      end
    end

    def link_to_parent_team_was
      parent_team_id_was = entry.hit.data[:parent_team_id_was]

      if parent_team_id_was.nil?
        "root"
      elsif parent_team = Team.find_by_id(parent_team_id_was)
        helpers.link_to(parent_team.name, "/orgs/#{entry.hit[:org]}/teams/#{parent_team.name}")
      else
        "[team not found]"
      end
    end

    def link_to_team
      helpers.link_to(entry.hit.data[:team], "/orgs/#{entry.hit[:org]}/teams/#{team_name}")
    end

    def link_to_actor
      link_to_user(entry.hit[:actor])
    end

    def link_to_user(user = entry.hit[:user])
      helpers.link_to(user, "/#{user}")
    end

    def org?
      entry.hit[:org].present?
    end

    def link_to_org
      helpers.link_to(entry.hit[:org], "/#{entry.hit[:org]}")
    end

    def link_to_project
      if project = entry.project
        helpers.link_to(project.name, urls.project_path(project))
      else
        "##{project_id}"
      end
    end

    def link_to_business(business = entry.hit[:business])
      helpers.link_to(business, urls.enterprise_path(business))
    end

    def renamed_project
      entry.hit.data["old_name"]
    end

    def project_id
      entry.hit.data["project_id"]
    end

    def repo?
      entry.hit[:repo].present?
    end

    def link_to_repo
      return link_to_repo_from_id unless entry.hit[:repo]
      helpers.link_to(entry.hit[:repo], "/#{entry.hit[:repo]}")
    end

    def link_to_repo_from_id
      repo_id = entry.hit[:repo_id]

      if repo_id && repo = Repository.find_by_id(repo_id)
        helpers.link_to(repo.nwo, "/#{repo.nwo}")
      else
        "[unknown repo]"
      end
    end

    def link_to_advisory
      repo = entry.hit[:repo]
      ghsa_id = entry.hit[:ghsa_id] || entry.hit[:repository_advisory]

      if repo
        helpers.link_to(ghsa_id, "/#{repo}/security/advisories/#{ghsa_id}")
      else
        helpers.link_to(ghsa_id, "/advisories/#{ghsa_id}")
      end
    end

    def link_to_advisory_credit_recipient
      link_to_user(entry.hit[:recipient] || "ghost")
    end

    def exported_repos_list
      helpers.safe_join(Array(entry.hit[:repo]), ", ")
    end

    def authorized_actors_list
      actors = entry.hit[:authorized_actors] || []
      return "admins only" unless actors.count > 0
      helpers.safe_join(Array(actors), ", ")
    end

    def migration_id
      entry.hit.data[:migration_id]
    end

    def link_to_repo_permission
      helpers.link_to(
        "#{entry.hit[:user]}'s permission on the #{entry.hit[:repo]} repository",
        urls.repository_permissions_path(entry.hit[:org], entry.hit[:user], *entry.hit[:repo].split("/")),
      )
    end

    def link_to_hook_target?
      # special handling for pre-existing global hook audit log entries
      return true if GitHub.single_business_environment? && entry.hit.data[:hook_type] == "global"
      entry.hit[:repo].present? || entry.hit[:org].present? || entry.hit[:business].present?
    end

    def link_to_repo_or_org_or_business
      if entry.hit[:repo]
        link_to_repo
      elsif entry.hit[:org]
        link_to_org
      elsif entry.hit[:business]
        link_to_business
      elsif entry.hit.data[:hook_type] == "global" && GitHub.single_business_environment?
        # special handling for pre-existing global hook audit log entries
        link_to_business(GitHub.global_business.slug)
      end
    end

    def link_to_oauth_application
      helpers.link_to(entry.hit.data[:oauth_application], "/organizations/#{entry.hit[:org]}/settings/applications/#{entry.hit.data[:oauth_application_id]}")
    end

    def link_to_integration(name = integration_name)
      if integration&.user_owned?
        helpers.link_to(name, "/settings/apps/#{entry.hit.data[:slug]}")
      elsif integration&.organization_owned?
        helpers.link_to(name, "/organizations/#{entry.hit[:org]}/settings/apps/#{entry.hit.data[:slug]}")
      else
        name
      end
    end

    def integration_name
      entry.hit.data.fetch(:name, entry.hit.data[:integration])
    end

    def link_to_integration_transfer_recipient
      helpers.link_to(entry.hit.data[:transfer_to], "/#{entry.hit.data[:transfer_to]}")
    end

    def link_to_repo_hook
      helpers.link_to(safe_hook_name,
        "/#{entry.hit[:repo]}/settings/hooks/#{entry.hit.data[:hook_id]}")
    end

    def link_to_org_hook
      helpers.link_to(safe_hook_name,
        "/organizations/#{entry.hit[:org]}/settings/hooks/#{entry.hit.data[:hook_id]}")
    end

    def link_to_business_hook(business = entry.hit[:business])
      helpers.link_to(safe_hook_name, urls.hook_enterprise_path(business, entry.hit.data[:hook_id]))
    end

    def link_to_hook
      if entry.hit.data[:hook_type] == "repo" && entry.hit.data[:hook_id].present?
        link_to_repo_hook
      elsif entry.hit.data[:hook_type] == "org" && entry.hit.data[:hook_id].present?
        link_to_org_hook
      elsif entry.hit.data[:hook_type] == "business" && entry.hit.data[:hook_id].present?
        link_to_business_hook
      elsif entry.hit.data[:hook_type] == "global" && entry.hit.data[:hook_id].present? && GitHub.single_business_environment?
        # special handling for pre-existing global hook audit log entries
        link_to_business_hook(GitHub.global_business.slug)
      else
        safe_hook_name
      end
    end

    def safe_hook_name
      if entry.hook_name
        entry.hook_name
      else
        "webhook"
      end
    end

    def link_or_show_license_id
      if entry.hit[:business]
        helpers.link_to(entry.hit.data[:license_id], urls.settings_billing_enterprise_path(entry.hit[:business]))
      else
        entry.hit.data[:license_id]
      end
    end

    def link_to_org_label
      return unless org = entry.hit[:org]

      label_name = entry.hit.data[:organization_default_label].to_s
      helpers.link_to(label_name, "/organizations/#{org}/settings/labels")
    end

    def renamed_repo
      if entry.hit[:org].present?
        "#{entry.hit[:org]}/#{entry.hit.data[:old_name]}"
      elsif entry.hit[:user].present?
        "#{entry.hit[:user]}/#{entry.hit.data[:old_name]}"
      else
        entry.hit[:repo]
      end
    end

    def transfered_repo
      if entry.hit[:org].present? && entry.hit.data[:old_user].present?
        entry.hit[:repo].sub("#{entry.hit[:org]}/", "#{entry.hit.data[:old_user]}/")
      else
        entry.hit[:repo]
      end
    end

    def topic
      entry.hit.data[:topic]
    end

    def email
      entry.hit.data[:email]
    end

    def old_email
      entry.hit.data[:old_email]
    end

    def old_email?
      old_email.present?
    end

    # Different models use different keys for auditing the responsible
    # OAuth application.
    def oauth_application_id
      entry.hit.data[:application_id] || entry.hit.data[:oauth_application_id]
    end

    def oauth_application_name
      entry.hit.data[:application_name]
    end

    def oauth_application?
      oauth_application_id && oauth_application_id != OauthApplication::PERSONAL_TOKENS_APPLICATION_ID
    end

    def personal_access_token?
      oauth_application_id && oauth_application_id == OauthApplication::PERSONAL_TOKENS_APPLICATION_ID
    end

    def oauth_type
      if oauth_application?
        "OAuth application"
      elsif personal_access_token?
        "Personal access token"
      end
    end

    def oauth_scopes
      entry.hit.data[:scopes] && entry.hit.data[:scopes].join(", ")
    end

    def hook_active?
      entry.hit.data[:active]
    end

    def hook_active_was?
      entry.hit.data[:active_was]
    end

    def hook_events
      entry.hit.data[:events] && entry.hit.data[:events].join(", ")
    end

    def hook_events_were
      entry.hit.data[:events_were] && entry.hit.data[:events_were].join(", ")
    end

    def plan_duration_change?
      entry.hit.data[:old_plan_duration] != entry.hit.data[:plan_duration]
    end

    def plan_changed?
      entry.hit.data[:old_plan] != entry.hit.data[:plan] &&
        !pricing_model_changed_to_per_seat? && !pricing_model_changed_to_per_repo?
    end

    def pricing_model_changed_to_per_seat?
      entry.hit.data[:old_plan] != entry.hit.data[:plan] &&
        per_seat_plan?(entry.hit.data[:plan])
    end

    def pricing_model_changed_to_per_repo?
      entry.hit.data[:old_plan] != entry.hit.data[:plan] &&
        per_seat_plan?(entry.hit.data[:old_plan])
    end

    def seat_count_changed?
      per_seat_plan?(entry.hit.data[:plan]) && entry.hit.data[:old_seats].to_i != seats
    end

    def per_seat_plan?(plan_name)
      GitHub::Plan.find(plan_name).try(:per_seat?)
    end

    def seat_delta
      seats - entry.hit.data[:old_seats].to_i
    end

    def seat_purchase?
      seats >= entry.hit.data[:old_seats].to_i
    end

    def email_invitation?
      entry.hit[:user].blank? && entry.hit.data[:email].present?
    end

    def link_to_issue
      if issue = entry.issue
        helpers.link_to("#{issue.repository.name_with_owner}##{issue.number}", "/#{issue.repository.name_with_owner}/issues/#{issue.number}")
      end
    end

    def link_to_new_issue
      if issue = entry.issue_transfer&.new_issue
        helpers.link_to("#{issue.repository.name_with_owner}##{issue.number}", "/#{issue.repository.name_with_owner}/issues/#{issue.number}")
      end
    end

    def link_to_issue_comment_author
      if issue_comment = entry.issue_comment
        helpers.link_to(issue_comment.user, "/#{issue_comment.user}")
      end
    end

    def link_to_issue_comment
      if issue_comment = entry.issue_comment
        anchor = comment_dom_id(issue_comment)
        helpers.link_to("issue comment", urls.issue_path(issue_comment.issue, anchor: anchor))
      end
    end

    def link_to_commit_comment_author
      if commit_comment = entry.commit_comment
        helpers.link_to(commit_comment.user, "/#{commit_comment.user}")
      end
    end

    def link_to_commit_comment
      if commit_comment = entry.commit_comment
        id = commit_comment.id || commit_comment.object_id
        #testing for presense of commit_comment.path is a very hacky way to find out whether it's an inline comment or not
        anchor = commit_comment.path ? "r#{id}" : "commitcomment-#{id}"
        helpers.link_to("commit comment", urls.commit_path(commit_comment.commit, commit_comment.repository) + "##{anchor}")
      end
    end

    def link_to_discussion
      if (discussion = entry.discussion) && discussion.repository
        helpers.link_to("discussion", "/#{discussion.repository.name_with_owner}/discussions/#{discussion.number}")
      end
    end

    def link_to_discussion_author
      if discussion = entry.discussion
        helpers.link_to(discussion.author, "/#{discussion.author}")
      end
    end

    def link_to_discussion_comment
      if discussion_comment = entry.discussion_comment
        helpers.link_to(
          "discussion comment",
          urls.discussion_comment_show_path_from(discussion: discussion_comment.discussion, comment_id: discussion_comment.id),
        )
      end
    end

    def link_to_discussion_comment_author
      if discussion_comment = entry.discussion_comment
        helpers.link_to(discussion_comment.author, "/#{discussion_comment.author}")
      end
    end

    def link_to_minimizable_author
      if minimizable = entry.minimizable
        helpers.link_to(minimizable.user, "/#{minimizable.user}")
      end
    end

    def link_to_minimizable
      if minimizable = entry.minimizable
        anchor = comment_dom_id(minimizable)
        case minimizable.class.to_s
        when "CommitComment"
          helpers.link_to(
            "commit comment",
            urls.commit_path(minimizable.commit_id, minimizable.repository) + "##{anchor}",
          )
        when "GistComment"
          gist = minimizable.gist
          helpers.link_to("gist comment", urls.user_gist_path(gist.user_param, gist, anchor: anchor))
        when "IssueComment"
          helpers.link_to("issue comment", urls.issue_path(minimizable.issue, anchor: anchor))
        when "PullRequestReview"
          helpers.link_to(
            "pull request review",
            urls.pull_request_path(minimizable.pull_request) + "##{anchor}",
          )
        when "PullRequestReviewComment"
          helpers.link_to(
            "pull request review comment",
            urls.pull_request_path(minimizable.pull_request) + "##{anchor}",
          )
        when "PullRequestReviewThread"
          helpers.link_to(
            "pull request review comment thread",
            urls.pull_request_path(minimizable.pull_request) + "##{anchor}",
          )
        end
      end
    end

    def admin_enforced?
      entry.hit.data[:admin_enforced]
    end

    def dismiss_stale_reviews_on_push?
      entry.hit.data[:dismiss_stale_reviews_on_push]
    end

    def require_code_owner_review?
      entry.hit.data[:require_code_owner_review]
    end

    def required_status_checks_enforcement_level
      return if entry.hit.data[:required_status_checks_enforcement_level].blank?

      integer_value = entry.hit.data[:required_status_checks_enforcement_level]
      ProtectedBranch::LEVELS.invert[integer_value]
    end

    def pull_request_reviews_enforcement_level
      return if entry.hit.data[:pull_request_reviews_enforcement_level].blank?

      integer_value = entry.hit.data[:pull_request_reviews_enforcement_level]
      ProtectedBranch::LEVELS.invert[integer_value]
    end

    def signature_requirement_enforcement_level
      return if entry.hit.data[:signature_requirement_enforcement_level].blank?

      integer_value = entry.hit.data[:signature_requirement_enforcement_level]
      ProtectedBranch::LEVELS.invert[integer_value]
    end

    def linear_history_requirement_enforcement_level
      return if entry.hit.data[:linear_history_requirement_enforcement_level].blank?

      integer_value = entry.hit.data[:linear_history_requirement_enforcement_level]
      ProtectedBranch::LEVELS.invert[integer_value]
    end

    def allow_force_pushes_enforcement_level
      return if entry.hit.data[:allow_force_pushes_enforcement_level].blank?

      integer_value = entry.hit.data[:allow_force_pushes_enforcement_level]
      ProtectedBranch::LEVELS.invert[integer_value]
    end

    def allow_deletions_enforcement_level
      return if entry.hit.data[:allow_deletions_enforcement_level].blank?

      integer_value = entry.hit.data[:allow_deletions_enforcement_level]
      ProtectedBranch::LEVELS.invert[integer_value]
    end

    def link_to_actor_avatar
      return actor_avatar unless entry.actor?

      helpers.link_to(
        actor_avatar,
        urls.user_path(entry.safe_actor),
        :class => "member-list-avatar tooltipped tooltipped-s",
        "aria-label" => "View #{entry.safe_actor}'s profile",
      )
    end

    def actor_avatar
      avatar_for(entry.safe_actor, 25, class: :avatar)
    end

    def child_avatar
      return unless display_user_avatar?
      avatar_for(entry.safe_user, 14, class: "avatar avatar-child")
    end

    def link_to_member
      helpers.link_to entry.safe_actor_login,
        view.search_path(actor: entry.safe_actor_login),
        :class => "member-link #{'ghost' if entry.actor_deleted?}",
        "data-pjax" => true
    end

    def link_to_org_person_sso(login: entry.safe_actor_login)
      return link_to_user(User.ghost) if entry.actor_deleted?
      return link_to_actor if entry.org.nil?

      sso_page_path = if entry.org.business&.saml_sso_enabled?
        business_user_account = entry.actor.business_user_accounts.find_by(business_id: entry.org.business.id)
        return link_to_actor if business_user_account.nil?
        urls.sso_enterprise_user_account_path(business_user_account.id)
      else
        urls.org_person_sso_path(org: entry.org, person_login: login)
      end

      helpers.link_to login,
        sso_page_path,
        :class => "member-link",
        "data-pjax" => true
    end

    def actor_ip
      return "Unknown IP address" unless entry.actor_ip.present?

      helpers.link_to entry.actor_ip,
        view.search_path(ip: entry.actor_ip),
        "data-pjax" => true
    end

    def geolocation
      return "Unknown location" unless entry.location?
      entry.location.values_at(:city, :region_name, :country_name).compact.join(", ")
    end

    def country
      return "Unknown location" unless entry.location?

      helpers.link_to entry.country.to_s,
        view.search_path(country: entry.country_code),
        "data-pjax" => true
    end

    def level
      entry.hit.data[:level]
    end

    def enterprise_installation_link
      helpers.link_to entry.hit.data[:customer_name], "http#{"s" unless entry.hit.data[:http_only]}://#{entry.hit.data[:enterprise_installation]}/"
    end

    def license_user_or_email
      user = entry.hit[:user]

      if user.present?
        helpers.link_to(user, "/#{user}")
      else
        entry.hit[:email]
      end
    end

    def authorized_credential_type
      case entry.hit.data[:credential_type]
      when "OauthAccess"
        "Personal Access Token"
      when "PublicKey"
        "SSH Key"
      end
    end

    def authorized_credential_fingerprint
      case entry.hit.data[:credential_type]
      when "OauthAccess"
        return "deleted" if authorized_credential.blank?
        "#{ "*" * 5 }#{authorized_credential.token_last_eight}"
      when "PublicKey"
        return "deleted" if credential_authorization.blank?
        "#{ "*" * 5 }#{credential_authorization.fingerprint[38..46]}"
      end
    end

    def prepaid_refill_amount
      Billing::Money.new(entry.hit.data["amount_in_subunits"], entry.hit.data["currency_code"])
    end

    def friendly_action_permission_name(permission)
      case permission
        when Configurable::ActionExecutionCapabilities::ALL_ACTIONS
          "all actions for all repositories".freeze
        when Configurable::ActionExecutionCapabilities::LOCAL_ACTIONS
          "local actions for all repositories".freeze
        when Configurable::ActionExecutionCapabilities::DISABLED
          "disabled".freeze
        when Configurable::ActionExecutionCapabilities::ALL_ACTIONS_FOR_SELECTED
          "all actions for selected repositories".freeze
        when Configurable::ActionExecutionCapabilities::LOCAL_ONLY_FOR_SELECTED
          "local actions for selected repositories".freeze
        else
          permission
      end
    end

    def previous_actions_permission
      friendly_action_permission_name(entry.hit.data[:old_capability])
    end

    def new_actions_permission
      friendly_action_permission_name(entry.hit.data[:new_capability])
    end

    def link_to_org_secrets
      secret_name = entry.hit.data[:key]
      helpers.link_to(secret_name, urls.settings_org_secrets_path(entry.hit[:org]))
    end

    def custom_role_name
      entry.hit.data[:name]
    end

    def custom_role_changed?
      entry.hit.data.has_key?(:changes)
    end

    def previous_custom_role_name
      entry.hit.data.dig(:changes, :old_name)
    end

    def custom_role_base_role
      entry.hit.data[:base_role]
    end

    def previous_custom_role_base_role
      entry.hit.data.dig(:changes, :old_base_role)
    end

    def custom_role_permissions
      entry.hit.data[:role_permissions]
    end

    def previous_custom_role_permissions
      entry.hit.data.dig(:changes, :old_role_permissions)
    end

    private

    def seats
      (entry.hit.data[:seats] || entry.hit.data[:new_seats]).to_i
    end

    def normalized_team_privacy(privacy)
      if privacy == "closed"
        # We eventually plan on introducing an "open" privacy that allows any
        # org member to join the team (whereas "closed" allows any org member to
        # see the team, but they must be added by someone with admin access).
        #
        # Until we introduce "open", the "closed" visibility is misleading, so
        # we say "visible" publicly until then.
        "visible"
      else
        privacy
      end
    end

    def authorized_credential
      return @authorized_credential if defined?(@authorized_credential)
      @authorized_credential =
        case entry.hit[:credential_type]
        when "OauthAccess"
          OauthAccess.find_by_id(entry.hit.data[:credential_id])
        when "PublicKey"
          PublicKey.find_by_id(entry.hit.data[:credential_id])
        end
    end

    def credential_authorization
      @credential_authorization ||= Organization::CredentialAuthorization.where(
        credential_type: entry.hit.data[:credential_type],
        credential_id: entry.hit.data[:credential_id],
      ).first
    end

    def integration
      return @integration if defined?(@integration)
      @integration = Integration.where(id: entry.hit.data[:integration_id]).first
    end
  end
end
