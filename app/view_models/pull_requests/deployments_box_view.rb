# rubocop:disable Style/FrozenStringLiteralComment

module PullRequests
  # Handles anything complicated in the merge button area of a pull
  # request.
  class DeploymentsBoxView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    attr_reader :pull
    attr_reader :additional_container_classes

    IN_PROGRESS_STATES  = %w(pending in_progress queued)
    UNSUCCESSFUL_STATES = %w(failure errored)
    SUCCESS_STATE       = %w(active)
    INACTIVE_STATE      = %w(inactive)

    ALL_STATES = [IN_PROGRESS_STATES, UNSUCCESSFUL_STATES, SUCCESS_STATE, INACTIVE_STATE].flatten

    def can_see_deployments?
      # Don't show the box if you don't have permissions to view deployments
      return false unless pull.repository.writable_by?(current_user)

      # Don't show the box if the repository has never been deployed
      return false if Deployment.where(repository_id: pull.repository.id).empty?

      # Don't show the box if the PR is closed/merged and there were no deploys
      return false if !pull.open? && deploys_for_current_head.empty?

      true
    end

    def container_classes
      "branch-action #{additional_container_classes}"
    end

    def css_class_for_deployment_summary_status
      case deployment_summary_status
      when :success
        "text-green"
      when :in_progress
        "text-yellow"
      when :unsuccessful
        "text-red"
      when :not_deployed
        "text-gray-light"
      end
    end

    def deploy_box_header_for_deployment_summary_status
      case deployment_summary_status
      when :success
        "This branch was successfully deployed"
      when :in_progress
        "This branch is being deployed"
      when :unsuccessful
        "This branch had an error being deployed"
      when :not_deployed
        "This branch has not been deployed"
      when :previously_deployed
        "This branch was previously deployed"
      end
    end

    # Returns a "summary" of the current PR deployment status
    # Can be one of:
    # :success      - The head oid has successful deploys, no pending/queued deploys, no errored deploys that have not been superseded.
    # :in_progress  - There is at least one queued or in_progress deploy, and no errored deploys that have not been superseded.
    # :unsuccessful - There is an errored deployment on at least one environment for this head oid, and there are no later deploys
    #                with the same oid and environment that are in_progress, queued, or successful.
    # :not_deployed - The PR was never deployed.
    # :previously_deployed - All deploys are 'inactive'.
    def deployment_summary_status
      @deployment_summary_status ||= if all_deploys.empty?
        # There are 0 deploys
        :not_deployed
      elsif latest_deploy_per_environment.all? { |deploy| deploy.state == INACTIVE_STATE }
        # All deploys are inactive
        :previously_deployed
      elsif latest_deploy_per_environment.any? { |deploy| UNSUCCESSFUL_STATES.include?(deploy.state) }
        # There is at least one unsuccessful deploy
        :unsuccessful
      elsif latest_deploy_per_environment.any? { |deploy| IN_PROGRESS_STATES.include?(deploy.state) }
        # There is at least one in_progess deploy
        :in_progress
      else
        # All deploys succeeded
        :success
      end
    end

    # A string that summarizes deployments by environment, used in the view
    # E.g. "8 successful, 1 in progress, and 2 failed deployments"
    def deployment_environment_summary_info
      deploys_by_state = latest_deploy_per_environment.group_by(&:state)

      summary_info = []

      ALL_STATES.each do |state|
        next unless (state_deploys = deploys_by_state[state])

        deploy_count = state_deploys.count
        outdated_count = state_deploys.count { |deploy| deploy.sha != pull.head_sha }
        label = state == "failure" ? "failed" : state.humanize(capitalize: false)

        summary_info << [label, deploy_count, outdated_count]
      end

      summary_info
    end

    # Returns an array of the most recent deploys for each deployed environment
    # This list of deploys provides a "current state" of each environment.
    def latest_deploy_per_environment
      @latest_deploy_per_environment ||= all_deploys.group_by(&:environment).map do |environment, deploys|
        deploys.sort_by(&:id).last
      end
    end

    def text_color_for_deployment(deployment)
      case deployment.state
      when "active"
        "text-green"
      when "in_progress", "queued", "pending"
        "text-yellow"
      when "failure", "errored"
        "text-red"
      when "inactive"
        "text-gray-light"
      end
    end

    # Returns an array of all deployments of this PR that are not outdated
    def deploys_for_current_head
      @deploys_for_current_head ||= all_deploys.select do |deploy|
        deploy.sha == pull.head_sha
      end
    end

    # Returns an array of all deployments made to this PR
    def all_deploys
      @all_deploys ||= Deployment.for_pull_request(pull)
    end
  end
end
