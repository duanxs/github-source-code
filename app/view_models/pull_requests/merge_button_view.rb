# frozen_string_literal: true

module PullRequests
  # Handles anything complicated in the merge button area of a pull
  # request.
  class MergeButtonView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    include StatusHelper
    include CommentsHelper

    # An error raised on an unexpected data edge case.
    class TooManyCommits < StandardError; end

    attr_reader :pull
    attr_reader :merge_type
    attr_reader :commit_title
    attr_reader :commit_message

    def channels
      [
        *GitHub::WebSocket::Channels.pull_request_mergeable(pull),
        GitHub::WebSocket::Channels.pull_request_state(pull),
        GitHub::WebSocket::Channels.pull_request_review_state(pull),
      ]
    end

    def show_merge_area?
      pull.open? && !pull.merged?
    end

    # Should the "Update branch" button be shown in the merge area for this PR?
    #
    # "Update branch" is only needed when required status checks are enabled for
    # the PR (so that we can be sure the status checks will still be green when
    # the PR is merged).
    #
    # Returns Boolean
    def can_update_branch?
      return false if logged_in? && current_user.must_verify_email?

      protected_branch = pull.protected_base_branch
      return false unless protected_branch.present?
      return false unless protected_branch.required_status_checks_enabled?
      return false unless protected_branch.strict_required_status_checks_policy?
      return false unless pull.behind_base?

      protected_branch.required_status_checks.any? && head_pushable?
    end

    def advisory_workspace?
      pull.repository.advisory_workspace?
    end

    def advisory_repository
      pull.repository.parent_advisory_repository
    end

    def repository_advisory
      pull.repository.parent_advisory
    end

    def base_branch_pushable?
      return @base_branch_pushable if defined?(@base_branch_pushable)
      @base_branch_pushable = base_repo_pushable? && authorized_to_update_protected_base_branch?
    end

    def base_repo_pushable?
      return @base_repo_pushable if defined?(@base_repo_pushable)
      @base_repo_pushable = pull.base_repository.pushable_by?(current_user)
    end

    def authorized_to_update_protected_base_branch?
      protected_branch = pull.protected_base_branch
      return true unless protected_branch

      protected_branch.authorized?(current_user)
    end

    def enforce_linear_history?
      protected_branch = pull.protected_base_branch
      return false unless protected_branch
      protected_branch.required_linear_history_enabled?
    end

    def head_branch_pushable?
      head_pushable? && authorized_to_update_protected_head_branch?
    end

    def authorized_to_update_protected_head_branch?
      protected_branch = pull.protected_head_branch
      return true unless protected_branch

      protected_branch.authorized?(current_user)
    end

    # Public: Returns true if the head ref of the pull request is protected in such a way that would prevent the current
    # user from directly pushing a merge commit. This accounts for required status checks, required reviewers, or
    # required linear history, and any available admin overrides.
    #
    # Returns: Boolean
    def protection_prohibits_merge_on_head?
      protected_branch = pull.protected_head_branch
      return false unless protected_branch

      protected_branch.requires_status_checks_or_reviews?(current_user) ||
        (!protected_branch.can_override_required_linear_history?(actor: current_user) &&
        protected_branch.required_linear_history_enabled?)
    end

    def pull_request_reviews_required?
      return false if !pull.protected_base_branch

      pull.protected_base_branch.pull_request_reviews_required?
    end

    def head_pushable?
      pull.head_repository && pull.head_repository.owner && pull.head_repository.pushable_by?(current_user, ref: head_ref)
    end

    def head_ref
      pull.head_ref_name
    end

    def display_head_ref
      head_ref.dup.force_encoding("utf-8").scrub!
    end

    def head_repo
      pull.head_repository
    end

    def head_branch_deleteable?
      pull.head_ref_deleteable_by?(current_user) || pull.head_ref_deleteable_after_updating_dependents?(current_user)
    end

    def head_branch_restoreable?
      pull.head_ref_restorable_by?(current_user)
    end

    def fork_deleteable?
      GitHub.dogstats.time("merge_button_view", tags: ["action:fork_deleteable"]) do
        return false unless head_repo_is_fork?
        return false unless pull.closed?
        return false unless head_repo.adminable_by?(current_user)

        !head_repo_has_activity?
      end
    end

    # Returns one of :unknown, :clean, :dirty, :draft, or :unstable.
    def state
      merge_state.status
    end

    # Returns "pending", "success", "error", "failure", or nil if the head commit has no statuses
    def commit_state
      combined_status.state
    end

    # Return the PullRequest::MergeState for the current viewer
    def merge_state
      @merge_state ||= pull.cached_merge_state(viewer: current_user)
    end

    def combined_status
      if required_status_decision_basis_commit.nil? || required_status_decision_basis_commit.sha == pull.head_sha
        pull.combined_status
      else
        # In some cases the required status decision was made based on checks
        # associated with a sha other than the pull's head_sha, in this case we
        # should show the checks/statuses for this alternate sha (usually the
        # PR's merge_commit_sha) so that it matches other merge box elements.
        pull.build_combined_status_for_sha(required_status_decision_basis_commit.sha)
      end
    end

    def required_status_decision_basis_commit
      return @required_status_decision_basis_commit if defined?(@required_status_decision_basis_commit)
      if !merge_state.protected_branch_policy_decision || !merge_state.protected_branch_policy_decision.reasons
        return @required_status_decision_basis_commit = nil
      end
      commits = merge_state.protected_branch_policy_decision.reasons.map(&:basis_commit).compact.uniq
      if commits.size > 1
        # This scenario is unexpected, send a warning so we can debug
        # and just choose an arbitrary commit to return as the basis.
        boom = TooManyCommits.new(commits.map(&:sha).join(" "))
        boom.set_backtrace(caller)
        Failbot.report_trace(boom, pull_request_id: pull_request&.id)
      end
      @required_status_decision_basis_commit = commits.first
    end

    def unstable_message
      case
      when base_repo_pushable?
        "Merge with caution!"
      when combined_status.state == "pending"
        "This branch has pending checks, but can be merged."
      else
        "This branch has failed checks, but can be merged."
      end
    end

    def tasks
      @_tasks ||= PullRequest::Tasks.new(pull)
    end

    # Returns the currently selected merge method.
    def merge_method
      return @merge_method if defined? @merge_method
      @merge_method = if enforce_linear_history?
        pull.protected_base_branch.default_merge_method_for(current_user)
      else
        pull.repository.default_merge_method_for(current_user)
      end
    end

    def default_commit_title
      @commit_title || (merge_method == :squash ? pull.default_squash_commit_title : pull.default_merge_commit_title)
    end

    def default_commit_message
      @commit_message || (merge_method == :squash ? pull.default_squash_commit_message : pull.title)
    end

    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def admin_override_possible?
      @admin_override_possible ||= merge_state.admin_override_possible?
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

    # Does the merge form merge confirmation button require an admin override confirmation?
    #
    # Returns: Boolean
    def can_override_merge_commit?
      admin_override_possible?
    end

    # Does the merge form squash-merge confirmation button require an admin override confirmation?
    #
    # Returns: Boolean
    def can_override_squash_merge?
      admin_override_possible? && !merge_state.blocked_only_by_required_linear_history?
    end

    # Does the merge form rebase confirmation button require an admin override confirmation?
    #
    # Returns: Boolean
    def can_override_rebase_merge?
      admin_override_possible? && !merge_state.blocked_only_by_required_linear_history?
    end

    # Generate CSS classes to be applied to the merge form related to the admin override. These are used to contextually
    # hide and show admin override controls if they only apply to a specific merge method.
    #
    # Returns: String, possibly empty
    def admin_override_classes
      classes = []
      classes << "js-admin-override-merge" if can_override_merge_commit?
      classes << "js-admin-override-squash" if can_override_squash_merge?
      classes << "js-admin-override-rebase" if can_override_rebase_merge?
      classes.join(" ")
    end

    # Disable the merge button entirely, including the merge method dropdown.
    #
    # Returns: Boolean
    def merge_button_disabled?
      [:draft, :dirty, :blocked, :unknown, :behind].include?(state) && !admin_override_possible?
    end

    def merge_button_class
      if [:clean, :has_hooks].include?(state)
        "btn-primary"
      elsif admin_override_possible?
        "btn-danger"
      else
        ""
      end
    end

    def merge_commit_button_class
      return merge_button_class unless enforce_linear_history?

      if admin_override_possible?
        "btn-danger"
      else
        ""
      end
    end

    alias merge_squash_button_class merge_button_class

    def merge_rebase_button_class
      if !pull.rebase_safe?
        ""
      else
        merge_button_class
      end
    end

    # Is the "Create a merge commit" option enabled in the merge method dropdown?
    #
    # Returns: Boolean
    def can_choose_merge_commit?
      case
      when merge_button_disabled?
        false
      when !pull.repository.merge_commit_allowed?
        false
      when enforce_linear_history? && !pull.protected_base_branch.can_override_required_linear_history?(actor: current_user)
        false
      else
        true
      end
    end

    # Is the "Merge pull request" button enabled once chosen?
    #
    # Returns: Boolean
    alias can_perform_merge_commit? can_choose_merge_commit?

    # Is the "Squash and merge" option enabled in the merge method dropdown?
    #
    # Returns: Boolean
    def can_choose_squash_merge?
      case
      when merge_button_disabled?
        false
      when !pull.repository.squash_merge_allowed?
        false
      else
        true
      end
    end

    # Is the "Squash and merge" button enabled once chosen?
    #
    # Returns: Boolean
    alias can_perform_squash_merge? can_choose_squash_merge?

    # Is the "rebase merge" option enabled in the merge method dropdown?
    #
    # Returns: Boolean
    def can_choose_rebase_merge?
      case
      when merge_button_disabled?
        false
      when !pull.repository.rebase_merge_allowed?
        false
      else
        true
      end
    end

    # Is the "Rebase and merge" button enabled once chosen?
    #
    # Returns: Boolean
    def can_perform_rebase_merge?
      case
      when !can_choose_rebase_merge?
        false
      when !pull.rebase_safe?
        false
      else
        true
      end
    end

    def merge_area_status_class
      case merge_method
      when :squash
        "is-squashing"
      when :rebase
        "is-rebasing"
      else
        "is-merging"
      end
    end

    def any_reviews_or_reviewers?
      merge_state.reviews.any? || pull.review_requests.pending.any?
    end

    def requested_changes?
      merge_state.requested_changes?
    end

    def can_add_review?
      current_user != pull.user
    end

    def can_be_quick_approved?(review)
      same_pull?(review) &&
        review.user == current_user &&
        review.changes_requested? &&
        !pull.pending_review_by?(current_user)
    end

    def same_pull?(review)
      pull == review.pull_request
    end

    def review_href(review)
      anchor = "##{comment_dom_id(review)}"

      if same_pull?(review)
        anchor
      else
        pull_request_path = urls.pull_request_path(review.pull_request)
        pull_request_path + anchor
      end
    end

    # Do we care to show review status at all? This is true if the pull request's
    # base branch is protected by reviews, or if the PR has any reviews or reviewers.
    def show_review_status?
      pull_request_reviews_required? || any_reviews_or_reviewers?
    end

    def review_required_as_code_owner?(reviewer)
      return false unless pull.protected_base_branch&.require_code_owner_review?

      pull.codeowners.include?(reviewer)
    end

    def conflict_editor_disabled?
      pull.cross_repo? && !GitHub.cross_repo_conflict_editor_enabled?
    end

    def ci_category
      if pull.repository.has_docker_file?
        "container-ci"
      elsif pull.repository.is_mobile?
        "mobile-ci"
      else
        "continuous-integration"
      end
    end

    private

    def head_repo_is_fork?
      head_repo && head_repo.fork?
    end

    def head_repo_has_activity?
      GitHub.dogstats.time("merge_button_view", tags: ["action:head_repo_has_activity"]) do
        head_repo_has_issues? || head_repo_has_pulls?
      end
    end

    def head_repo_has_pulls?
      head_repo && head_repo.pull_requests.any?
    end

    def head_repo_has_issues?
      head_repo && head_repo.issues.any?
    end
  end
end
