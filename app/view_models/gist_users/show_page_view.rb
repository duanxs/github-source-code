# frozen_string_literal: true

module GistUsers
  # Controls the gist user index view
  # eg https://gist.github.com/defunkt
  class ShowPageView < ViewModel # rubocop:todo ViewComponent/PreferViewComponents
    include TextHelper

    attr_reader :user, :gists, :sidebar_counts, :atom_feed_path, :current_page,
                :total_public_visibility_count, :total_secret_visibility_count,
                :sort_direction, :viewer

    def page_title
      "#{user}’s gists"
    end

    def page_meta_description
      I18n.t("gist_profiles.meta_description", user: user)
    end

    def your_profile?
      viewer == user
    end

    def show_spammy_alert?
      user.spammy? && viewer&.site_admin?
    end

    def has_gists?
      gists.count > 0
    end

    # Public: Yield each Gist and the first associated Blob
    #         Blob can be nil in cases of corrupt/unreadable gists
    def gists_with_first_blob
      Enumerator.new do |yielder|
        gists.each do |gist|
          blob = gist.files.detect { |b| b.present? }
          yielder.yield gist, blob
        end
      end
    end

    def next_label
      if sort_direction == "asc"
        "Newer"
      else
        "Older"
      end
    end

    def previous_label
      if sort_direction == "asc"
        "Older"
      else
        "Newer"
      end
    end

    def search_sort_fields
      %w{created updated}
    end

    def search_sort_labels
      {
        %w{created desc}  => "Recently created",
        %w{created asc}   => "Least recently created",
        %w{updated desc}  => "Recently updated",
        %w{updated asc}   => "Least recently updated",
      }
    end

    def search_active_class(param, val)
      "active" if params[param.to_sym].to_s == val
    end

    def search_sort_directions
      %w{desc asc}
    end

    def profile_name
      user ? user.safe_profile_name : "Anonymous"
    end

    def login
      user ? user.login : "anonymous"
    end

    def forked_count
      sidebar_counts[:forked]
    end

    def show_forked_tab?
      current_page == :forked || forked_count > 0
    end

    def starred_count
      sidebar_counts[:starred]
    end

    def show_starred_tab?
      current_page == :starred || sidebar_counts[:starred] > 0
    end

    def all_count
      sidebar_counts[:all]
    end

    def all_gists?
      current_page == :all
    end

    # What symbols should we trigger highlighting for various tabs?
    def highlights_for_all
      [:all]
    end

    def highlights_for_forked
      [:forked]
    end

    def highlights_for_starred
      [:starred]
    end

    # Show the visibility filters only if we're on our own profile, and there are gists
    def show_filters?
      your_profile? && [total_public_visibility_count, total_secret_visibility_count].max > 0
    end

    def visibility_prefix_path
      case current_page
      when :all
        urls.user_gists_path(user)
      when :forked
        urls.user_forked_gists_path(user)
      when :starred
        urls.user_starred_gists_path(user)
      end
    end

    def scope_name
      case current_page
      when :all
        your_profile? ? "gists" : "public gists"
      when :forked
        "forked gists"
      when :starred
        "starred gists"
      end
    end

    def visibility_name(visibility)
      case visibility
      when "public"
        "Public"
      when "secret"
        "Secret"
      else
        "All"
      end
    end

    def public_visibility_path
      "#{visibility_prefix_path}/public"
    end

    def secret_visibility_path
      "#{visibility_prefix_path}/secret"
    end
  end
end
