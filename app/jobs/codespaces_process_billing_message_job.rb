# frozen_string_literal: true

require "codespaces"

class CodespacesProcessBillingMessageJob < ApplicationJob
  queue_as :codespaces
  locked_by timeout: 1.minute, key: DEFAULT_LOCK_PROC

  def perform(billing_message_id:)
    Codespaces::ProcessBillingMessage.call(
      billing_message_id: billing_message_id
    )
  end
end
