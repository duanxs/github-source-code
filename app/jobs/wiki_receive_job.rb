# frozen_string_literal: true

WikiReceiveJob = LegacyApplicationJob.wrap(GitHub::Jobs::WikiReceive)
