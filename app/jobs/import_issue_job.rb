# frozen_string_literal: true

ImportIssueJob = LegacyApplicationJob.wrap(GitHub::Jobs::ImportIssue)
