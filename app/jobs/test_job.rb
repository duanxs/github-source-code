# frozen_string_literal: true

TestJob = LegacyApplicationJob.wrap(GitHub::Jobs::TestJob)
