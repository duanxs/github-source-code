# frozen_string_literal: true

class BillingFreeTrialReminderJob < ApplicationJob
  areas_of_responsibility :gitcoin
  queue_as :billing

  schedule interval: 24.hours, condition: -> { !GitHub.enterprise? }

  def perform
    GitHub::Billing.send_free_trial_reminders
  end
end
