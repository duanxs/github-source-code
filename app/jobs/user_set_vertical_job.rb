# frozen_string_literal: true

UserSetVerticalJob = LegacyApplicationJob.wrap(GitHub::Jobs::UserSetVertical)
