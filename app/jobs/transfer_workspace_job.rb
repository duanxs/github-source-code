# frozen_string_literal: true

# This job wraps a synchronous call to Repository#transfer_ownership_to
# for Workspace repositories that differs from TransferRepositoryJob:
#
# - Workspace owners do not require notification emails on Transfer complete
# - Workspaces must re-inherit abilities from their parent Repository
#   as an extra step
class TransferWorkspaceJob < ApplicationJob
  areas_of_responsibility :repositories, :security_advisories

  queue_as :workspace_repository_abilities

  def perform(workspace_repository_id)
    workspace_repository = Repository.find(workspace_repository_id)

    return unless workspace_repository.advisory_workspace?

    new_owner = workspace_repository.parent_advisory.owner

    Ability.throttle do
      workspace_repository.throttle do
        workspace_repository.transfer_ownership_to(new_owner, actor: workspace_repository.owner)
      end
    end

    # Re-inherit abilities
    WorkspaceAbilitySetupJob.perform_later(workspace_repository.id)
  end
end
