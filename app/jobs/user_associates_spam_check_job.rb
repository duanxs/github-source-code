# frozen_string_literal: true

UserAssociatesSpamCheckJob = LegacyApplicationJob.wrap(GitHub::Jobs::UserAssociatesSpamCheck)
