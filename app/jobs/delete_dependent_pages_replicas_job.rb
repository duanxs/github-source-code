# frozen_string_literal: true

# DeleteDependentPagesReplicasJob deletes all pages_replicas for a fileserver when it's removed.
# This is triggered presently by running `script/dpages remove <fileserver_hostname>`.
class DeleteDependentPagesReplicasJob < ApplicationJob
  areas_of_responsibility :pages
  queue_as :background_destroy

  # In order to reduce the likelihood that these deletions cause replication lag,
  # we need to delete in very small batches. A large deletion of 1000 or more rows
  # can cause the replicas to lag, so it's best to delete in small batches and throttle.
  BATCH_SIZE = 100

  def perform(pages_fileserver_hostname, *args)
    @pages_fileserver_hostname = pages_fileserver_hostname

    Failbot.push(app: "pages", pages_fileserver_hostname: pages_fileserver_hostname)

    rows_deleted = 0
    loop do
      affected_rows = 0

      Page::Replica.throttle_writes do
        delete = Page::Replica.github_sql.run <<~SQL, host: pages_fileserver_hostname, count: BATCH_SIZE
          DELETE FROM pages_replicas WHERE host = :host LIMIT :count
        SQL

        affected_rows = delete.affected_rows
        rows_deleted += delete.affected_rows
      end

      break if affected_rows == 0
    end

    GitHub.dogstats.histogram("delete_dependent_records", rows_deleted, tags: all_stats_tags)
  end

  def stats_tags
    [
      "model:pages_replicas",
      "pages_fileserver_hostname:#{@pages_fileserver_hostname}",
    ]
  end
end
