# frozen_string_literal: true

class CancelRepoInvitationJob < ApplicationJob
  queue_as :cancel_repo_invitation

  def perform(actor:, invitation:)
    raise  RuntimeError.new("actor is not a repo admin") unless invitation.repository.adminable_by?(actor)
    invitation.cancel!(actor: actor)
  end
end
