# frozen_string_literal: true

CalculateUserStarsCountJob = LegacyApplicationJob.wrap(GitHub::Jobs::CalculateUserStarsCount)
