# frozen_string_literal: true

OrgSurveyAnswersToOctolyticsJob = LegacyApplicationJob.wrap(GitHub::Jobs::OrgSurveyAnswersToOctolytics) do
  self.queue_adapter = :aqueduct
end
