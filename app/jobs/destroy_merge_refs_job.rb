# frozen_string_literal: true

DestroyMergeRefsJob = LegacyApplicationJob.wrap(GitHub::Jobs::DestroyMergeRefs)
