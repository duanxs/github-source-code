# frozen_string_literal: true

class ContributionsTrackPushJob < ApplicationJob
  areas_of_responsibility :user_profile
  queue_as :contributions_track_push

  locked_by timeout: 1.hour, key: DEFAULT_LOCK_PROC

  def perform(push)
    Failbot.push(
      repo: push.repository&.readonly_name_with_owner,
      before: push.before,
      after: push.after,
    )

    CommitContribution.throttle { CommitContribution.track_push!(push) }
  end
end
