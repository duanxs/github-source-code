# frozen_string_literal: true

require "codespaces"

class CodespacesFetchBillingMessagesJob < ApplicationJob
  areas_of_responsibility :codespaces
  queue_as :codespaces
  locked_by timeout: 1.minute, key: DEFAULT_LOCK_PROC

  def perform(azure_storage_account_name:)
    Codespaces::FetchBillingMessages.call(
      azure_storage_account_name: azure_storage_account_name
    )
  end
end
