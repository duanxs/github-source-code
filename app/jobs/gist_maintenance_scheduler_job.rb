# frozen_string_literal: true

GistMaintenanceSchedulerJob = LegacyApplicationJob.wrap(GitHub::Jobs::GistMaintenanceScheduler)
