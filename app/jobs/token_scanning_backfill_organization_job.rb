# frozen_string_literal: true

# Performs organizational scoped state changes for backfills of token scanning:
# Param 1 is organization id
# Param 2 describes the action
#   "queue_scans" - (Default) Queues work for a series of onboarding backfills all existing repos in the org
#   "clear" -  Clears backfill state in the org for a clean slate, or as a precursor to initiating a new scan.
#     Use case of clear is to facilitate load testing/re-testing in production. This is not directly pertinent to any customer scenario.
class TokenScanningBackfillOrganizationJob < ApplicationJob
  areas_of_responsibility :token_scanning
  queue_as :token_scanning

  # Batch size to query and process repos from specified organization
  REPOS_BATCH_SIZE = 50

  # Do not rerun if another backfill is underway for this org
  discard_on(GitHub::Restraint::UnableToLock) do |job, error|
    if error.is_a?(GitRPC::CommandFailed)
      Failbot.report(error, org_id: organization.id)
    end
  end

  def organization
    org_id = self.arguments.first
    ActiveRecord::Base.connected_to(role: :reading) do
      @organization ||= Organization.find_by(id: org_id)
    end
  end

  def action
    @action ||= self.arguments.second || "queue_scans"
  end

  def perform(*args)
    return "no_org" if organization.nil?
    return "unknown action" if (action != "queue_scans" && action != "clear")

    lock_key = "org_token_scanning_lock:" + organization.id.to_s
    max_concurrent_jobs = 1
    lock_ttl = 15.minutes
    restraint.lock!(lock_key, max_concurrent_jobs, lock_ttl) do
      process_repositories
    end
    return "#{action}_completed"
  end

  def process_repositories
    ActiveRecord::Base.connected_to(role: :reading) do
      organization.private_repositories.preload(:token_scan_status).find_in_batches(batch_size: REPOS_BATCH_SIZE) do |repositories_batch|
        TokenScanStatus.throttle do
          if action == "queue_scans"
            repos_to_mark = repositories_batch.select { |repo| repo.token_scanning_enabled? && !repo.token_scan_status }
            ActiveRecord::Base.connected_to(role: :writing) do
              TokenScanStatus.create_status_entries_for_repos!(repos_to_mark)
            end
            log_telemetry_count(repos_to_mark.count)
          elsif action == "clear"
            repos_to_clear = repositories_batch.select { |repo| repo.token_scan_status }
            repo_ids_to_clear = repos_to_clear.collect(&:id)
            ActiveRecord::Base.connected_to(role: :writing) do
              TokenScanStatus.where(repository_id: repo_ids_to_clear).delete_all
            end
            log_telemetry_count(repos_to_clear.count)
          end
        end
      end
    end
  end

  def log_telemetry_count(count)
    GitHub.dogstats.increment("token_scanning_backfill_organization.#{action == "clear" ? "clear_repo" : "mark_repo" }", by: count)
  end

  # Lazily instantiate a restraint lock
  def restraint
    @restraint ||= GitHub::Restraint.new
  end
end
