# frozen_string_literal: true

UpdateSiteStatsJob = LegacyApplicationJob.wrap(GitHub::Jobs::UpdateSiteStats)
