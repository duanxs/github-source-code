# frozen_string_literal: true

class PurgeArchivedAssetsJob < ApplicationJob
  BATCH_SIZE = 100

  areas_of_responsibility :lfs

  queue_as :purge_archived_assets
  schedule interval: 1.hour, condition: -> { !GitHub.enterprise? }
  locked_by timeout: 1.hour, key: ApplicationJob::DEFAULT_LOCK_PROC

  START_TIME = "purge_archive_assets_job.start_time"

  def perform(newest_time: nil)
    epoch_time = Time.now.to_f.to_s
    GitHub.kv.set(START_TIME, epoch_time, expires: 1.day.from_now) if newest_time.nil?

    oldest_time = Time.now - Asset::Archive.expiration_period
    state = :ok

    archives = ActiveRecord::Base.connected_to(role: :reading) do
      Asset::Archive.deletable_since(oldest_time,
        limit: BATCH_SIZE,
        newest_time: newest_time)
    end

    if archives.blank?
      run_time = Time.now.to_f - job_start_time
      GitHub.dogstats.distribution("archived_files.purge_job.dist.time", run_time)

      return
    end

    Asset::Archive.throttle { Asset::Archive.purge(archives.map(&:id)) }
    newest_time = archives.map(&:created_at).sort.first

    # enqueue the same job for the next batch to be processed
    PurgeArchivedAssetsJob.perform_later(newest_time: newest_time)

  rescue Asset::Archive::PurgeError => err
    state = :error
    Failbot.report_user_error(err)
  ensure
    GitHub.dogstats.increment("archived_files.purge_job", tags: ["state:#{state}"])
  end

  private

  def job_start_time
    (GitHub.kv.get(START_TIME).value { Time.now }).to_f
  end
end
