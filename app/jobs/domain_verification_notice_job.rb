# frozen_string_literal: true
class DomainVerificationNoticeJob < ApplicationJob
  queue_as :mailers

  def perform(organization, verified_domain)
    members = ActiveRecord::Base.connected_to(role: :reading) do
                # Getting the member IDs in a separate query is better for performance.
                member_ids = organization.member_ids
                User.where(id: member_ids).
                  joins(:emails).
                  where("
                    user_emails.normalized_domain = :domain
                    AND user_emails.state = 'verified'
                  ", domain: verified_domain).
                  distinct
    end

    members.each do |member|
      OrganizationMailer.domain_verification_notice(member, organization, verified_domain).deliver_later
    end
  end
end
