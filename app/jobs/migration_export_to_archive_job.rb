# frozen_string_literal: true

class MigrationExportToArchiveJob < ApplicationJob
  areas_of_responsibility :migration

  queue_as :gh_migrator

  discard_on StandardError do |job, error|
    job.fail_migration_and_raise(error)
  end

  retry_on GitHub::Migrator::EmptyMigration do |job, error|
    job.fail_migration_and_raise(error)
  end

  retry_on Freno::Throttler::Error

  def perform(migration)
    # Set failbot context
    Failbot.push(migration_id: migration.id)

    # Set migration state to :exporting
    migration.exporting!

    GitHub::Timer.timeout(GitHub.max_gh_migrator_export_time, GitHub::Migrator::TimeoutError) do
      GitHub.migrator.export(migration)
    end

    # Set migration state to :exported
    if ::Migration.export_disabled_for_actor?(migration.creator)
      ::Migration.enable_export_for_actor(migration.creator)
    else
      migration.exported!
    end
  end

  def fail_migration_and_raise(error)
    migration = self.arguments.first
    migration.failed!

    Failbot.report!(error)
    raise error
  end
end
