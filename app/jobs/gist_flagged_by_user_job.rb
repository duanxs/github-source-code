# frozen_string_literal: true

GistFlaggedByUserJob = LegacyApplicationJob.wrap(GitHub::Jobs::GistFlaggedByUser)
