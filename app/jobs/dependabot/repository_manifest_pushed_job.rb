# frozen_string_literal: true

# Builds any new fixes required after a Repository has pushed a diff containing
# manifest changes.
class Dependabot::RepositoryManifestPushedJob < ApplicationJob
  areas_of_responsibility :dependabot
  queue_as :dependabot

  def self.enqueue(repository)
    return unless repository.vulnerability_updates_enabled?

    perform_later(repository.id)
  end

  def perform(repository_id)
    repository = Repository.find_by(id: repository_id)

    return unless repository

    # A manifest push can generate a large volume of RepositoryDependencyUpdate
    # rows, so lets throttle for safety.
    RepositoryDependencyUpdate.throttle do
      RepositoryDependencyUpdate.request_for_repository(repository,
                                                        trigger: :push)
    end
  end
end
