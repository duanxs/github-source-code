# frozen_string_literal: true

# DeleteDependentPagesReplicasSchedulerJob finds all the hosts that exist in pages_replicas
# but don't exist in pages_fileservers, and schedules their background deletion from pages_replicas.
class DeleteDependentPagesReplicasSchedulerJob < ApplicationJob
  areas_of_responsibility :pages
  queue_as :background_destroy

  schedule interval: 1.day, scope: :global
  locked_by key: ->(job) { "delete_dependent_pages_replicas_scheduler" }, timeout: 5.minutes

  def perform(*args)
    return unless GitHub.flipper[:pages_replicas_cleanup_scheduler].enabled?

    Page::Replica.throttle do
      nonextant_hosts = ActiveRecord::Base.connected_to(role: :reading) do
        Page::Replica.github_sql.values <<~SQL
          SELECT DISTINCT(pages_replicas.host) FROM pages_replicas
          WHERE pages_replicas.host NOT IN (SELECT host FROM pages_fileservers)
        SQL
      end

      nonextant_hosts.each { |h| DeleteDependentPagesReplicasJob.perform_later(h) }
    end
  end
end
