# frozen_string_literal: true

UserSignupConfirmationJob = LegacyApplicationJob.wrap(GitHub::Jobs::UserSignupConfirmation)
