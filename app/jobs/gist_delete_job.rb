# frozen_string_literal: true

GistDeleteJob = LegacyApplicationJob.wrap(GitHub::Jobs::GistDelete)
