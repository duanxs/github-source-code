# frozen_string_literal: true

PageBuildJob = LegacyApplicationJob.wrap(GitHub::Jobs::PageBuild)
