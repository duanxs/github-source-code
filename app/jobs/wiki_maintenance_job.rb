# frozen_string_literal: true

WikiMaintenanceJob = LegacyApplicationJob.wrap(GitHub::Jobs::WikiMaintenance)
