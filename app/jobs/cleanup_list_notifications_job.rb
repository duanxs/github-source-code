# frozen_string_literal: true

class CleanupListNotificationsJob < ApplicationJob

    queue_as :notifications

    areas_of_responsibility :notifications

    retry_on_dirty_exit

    BATCH_SIZE = 100

    # Performs permissions checks and unsubscribes a user from notification
    # lists they should no longer be receiving notifications for.
    #
    # user_id   - The user_id to clean up notifications for.
    # list_type - String type of the notifications list class. Supported types
    #             are `Repository` and `Team`.
    # list_ids  - Array of list IDs to query and check permissions against.
    def perform(user_id, list_type, list_ids, options = {})
      return if list_ids.empty?
      return unless user = ActiveRecord::Base.connected_to(role: :reading) { User.find_by_id(user_id) }

      list_ids.each_slice(BATCH_SIZE) do |slice|
        lists = lists_from(slice, list_type: list_type.constantize)

        to_unsubscribe = lists.select { |list| unsubscribe?(list, user) }
        GitHub.newsies.async_delete_all_for_user_and_lists(user.id, to_unsubscribe) unless to_unsubscribe.empty?
      end
    end

    private
    # Internal: Should we execute the `GitHub.newsies.async_delete_all_for_user_and_lists` method?
    #
    # Returns a Boolean.
    def unsubscribe?(list, user)
      return true if list.new_record?

      case list
      when Repository
        list.deleted? || !list.pullable_by?(user)
      when Team
        !list.visible_to?(user)
      else
        false
      end
    end

    # Internal: Gets all lists for the given IDs.  If a list has been deleted, construct
    # a new record with that ID so that we can clear the notifications
    # subscriptions.
    #
    # Returns an Array of list objects.
    def lists_from(list_ids, list_type:)
      lists = ActiveRecord::Base.connected_to(role: :reading) { list_type.where(id: list_ids).index_by(&:id) }
      list_ids.map { |id| lists[id] || list_type.new { |r| r.id = id } }
    end
end
