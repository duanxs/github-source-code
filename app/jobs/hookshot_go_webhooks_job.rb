# frozen_string_literal: true

#
# This job defintion is used from gh/gh to enqueue webhook delivery payloads onto Resque.
# This job should not have any consumers in gh/gh because its
# consumed from hookshot-go.
#
# This setup is only used in GHE envs
#
# areas_of_responsibility :webhook
#
class HookshotGoWebhooksJob
  @queue = "hookshot-go"
end
