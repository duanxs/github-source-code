# frozen_string_literal: true

class UserRenameJob < ApplicationJob
  areas_of_responsibility :user_growth

  queue_as :critical

  def perform(user, new_login, actor)
    return if user.nil?
    user.rename!(new_login, actor: actor)
  end
end
