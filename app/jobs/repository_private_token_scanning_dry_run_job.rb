# frozen_string_literal: true

# runs the repo scan jobs in dry run mode and exempts it from the scheduler
class RepositoryPrivateTokenScanningDryRunJob < RepositoryPrivateTokenScanningJob
  def dry_run?
    true
  end

  def is_tracked_by_scheduler?
    return false
  end
end
