# frozen_string_literal: true

PageCertificateWorkJob = LegacyApplicationJob.wrap(GitHub::Jobs::PageCertificateWork)
