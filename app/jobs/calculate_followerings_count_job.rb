# frozen_string_literal: true

# Public: Recalculates the cached number of followers/followees of users that are followed by or
# follow another user. This is used when a user's spammy or suspended status changes. We hide
# spammy and suspended users, so this number has to match.
#
# The methods that calculate the follower/following count already know to exclude spammy users. We
# just have to run them.
class CalculateFolloweringsCountJob < ApplicationJob
  areas_of_responsibility :user_profile

  queue_as :calculate_followerings_count

  retry_on_dirty_exit

  # Public: Run the job to update follower and following counts.
  #
  # user_id - the Integer database ID of a User record whose counts should be updated
  # cascade - whether or not to update the counts of all the followers and followed users of the
  #           specified user; Boolean, defaults to true
  #
  # Returns nothing.
  def perform(user_id, options = {})
    cascade = options.fetch(:cascade, true)
    user = ActiveRecord::Base.connected_to(role: :reading) { User.find_by_id(user_id) }
    return unless user

    user.followers_count!
    user.following_count!

    if cascade
      GitHub.dogstats.time("follow_count.time", tags: ["follow_aggregate_count:followers"]) do
        user.following.each do |followed_user|
          begin
            followed_user.followers_count!
          rescue ActiveRecord::RecordNotFound => boom
            Failbot.report(boom)
            next
          end
        end
      end

      GitHub.dogstats.time("follow_count.time", tags: ["follow_aggregate_count:following"]) do
        user.followers.each do |follower|
          begin
            follower.following_count!
          rescue ActiveRecord::RecordNotFound => boom
            Failbot.report(boom)
            next
          end
        end
      end
    end
  end
end
