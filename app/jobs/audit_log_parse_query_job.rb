# frozen_string_literal: true

class AuditLogParseQueryJob < ApplicationJob
  areas_of_responsibility :audit_log
  queue_as :audit_log_parse_query

  def perform(query)
    Audit::Driftwood.parse_query(query)
  end
end
