# frozen_string_literal: true

DeleteDependentRecordsJob = LegacyApplicationJob.wrap(GitHub::Jobs::DeleteDependentRecords)
