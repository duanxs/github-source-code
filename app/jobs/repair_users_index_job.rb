# frozen_string_literal: true

RepairUsersIndexJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepairUsersIndex)
