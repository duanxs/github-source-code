# frozen_string_literal: true

class MinimizeAllCommentsOnRepoJob < ApplicationJob
  areas_of_responsibility :community_and_safety
  queue_as :minimize_all_comments_on_repo

  def perform(org, user, actor, minimize_reason)
    user_id = user.id

    org.repositories.pluck(:id).each do |repository_id|
      IssueComment.where(repository: repository_id, user_id: user_id).find_each do |comment|
        comment.set_minimized(actor, minimize_reason, minimize_reason, user)
      end

      CommitComment.where(repository: repository_id, user_id: user_id).find_each do |comment|
        comment.set_minimized(actor, minimize_reason, minimize_reason, user)
      end

      PullRequestReviewComment.joins(:pull_request)
        .where("pull_request_review_comments.user_id = ? and pull_requests.repository_id = ? ", user_id, repository_id)
        .find_each do |comment|
        comment.set_minimized(actor, minimize_reason, minimize_reason, user)
      end
    end
  end
end
