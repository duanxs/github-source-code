# frozen_string_literal: true

PerformPendingPlanChangesJob = LegacyApplicationJob.wrap(GitHub::Jobs::PerformPendingPlanChanges)
