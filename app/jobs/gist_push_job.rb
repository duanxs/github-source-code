# frozen_string_literal: true

GistPushJob = LegacyApplicationJob.wrap(GitHub::Jobs::GistPush)
