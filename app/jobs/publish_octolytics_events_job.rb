# frozen_string_literal: true

PublishOctolyticsEventsJob = LegacyApplicationJob.wrap(GitHub::Jobs::PublishOctolyticsEvents)
