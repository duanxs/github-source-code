# frozen_string_literal: true

AbstractBraintreeJob = LegacyApplicationJob.wrap(GitHub::Jobs::AbstractBraintreeJob)
