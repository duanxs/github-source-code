# frozen_string_literal: true

PostMirrorHookJob = LegacyApplicationJob.wrap(GitHub::Jobs::PostMirrorHook)
