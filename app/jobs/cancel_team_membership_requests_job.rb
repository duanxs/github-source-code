# frozen_string_literal: true

CancelTeamMembershipRequestsJob = LegacyApplicationJob.wrap(GitHub::Jobs::CancelTeamMembershipRequests)
