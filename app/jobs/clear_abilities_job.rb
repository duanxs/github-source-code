# frozen_string_literal: true

ClearAbilitiesJob = LegacyApplicationJob.wrap(GitHub::Jobs::ClearAbilities)
