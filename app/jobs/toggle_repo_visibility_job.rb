# frozen_string_literal: true

ToggleRepoVisibilityJob = LegacyApplicationJob.wrap(GitHub::Jobs::ToggleRepoVisibility)
