# frozen_string_literal: true

IndexSourceCodeJob = LegacyApplicationJob.wrap(GitHub::Jobs::IndexSourceCode)
