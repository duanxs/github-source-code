# frozen_string_literal: true

PurgeStaleRenderBlobsJob = LegacyApplicationJob.wrap(GitHub::Jobs::PurgeStaleRenderBlobs)
