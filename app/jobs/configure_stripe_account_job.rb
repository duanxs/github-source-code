# frozen_string_literal: true

require "stripe"

class ConfigureStripeAccountJob < ApplicationJob
  queue_as :stripe

  RETRYABLE_ERRORS = [
    Stripe::APIConnectionError,
    Stripe::StripeError,
    Sponsors::ConfigureStripeAccount::UpdatePayoutsFailedError,
  ].freeze

  DISCARDABLE_ERRORS = [
    Stripe::PermissionError,
    Stripe::OAuth::InvalidGrantError,
  ].freeze

  retry_on(*RETRYABLE_ERRORS) do |job, error|
    stripe_account, named_args = job.arguments
    Failbot.report(error,
      stripe_connect_account_id: stripe_account.id,
      freeze_payouts: named_args[:freeze_payouts],
      app: "github-external-request",
    )
  end

  discard_on(*DISCARDABLE_ERRORS) do |job, error|
    stripe_account, named_args = job.arguments
    Failbot.report(error,
      stripe_connect_account_id: stripe_account.id,
      freeze_payouts: named_args[:freeze_payouts],
      app: "github-external-request",
    )

    if error.is_a?(Stripe::PermissionError)
      GitHub.dogstats.increment("stripe.permission_error", tags: ["action:toggle_payouts"])
    end
  end

  def perform(stripe_account, freeze_payouts: false)
    Sponsors::ConfigureStripeAccount.call(
      account: stripe_account,
      freeze_payouts: freeze_payouts,
    )
  end
end
