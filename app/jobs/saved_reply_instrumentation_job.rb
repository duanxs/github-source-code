# frozen_string_literal: true

class SavedReplyInstrumentationJob < ApplicationJob

  queue_as :saved_reply_instrumentation

  retry_on_dirty_exit

  def perform(actor_id, saved_reply_id, repository_id, comment_type)
    Failbot.push(
      job: self.class.name,
      actor_id: actor_id,
      saved_reply_id: saved_reply_id,
      repository_id: repository_id,
      comment_type: comment_type,
    )

    actor = User.find_by_id(actor_id)
    saved_reply = SavedReply.find_by_id(saved_reply_id)
    repository = Repository.find_by_id(repository_id)
    return unless actor && saved_reply && repository

    event_payload = octolytics_event_payload(actor, saved_reply, repository, comment_type)

    GitHub.dogstats.increment("comment", tags: ["action:create", "type:using_saved_reply"])
    GitHub.analytics.record(
      "saved_reply_used",
      event_payload[:dimensions],
      event_payload[:measures],
      event_payload[:context],
    )
  end

  private

  def octolytics_event_payload(actor, saved_reply, repository, comment_type)
    payload = Issues::CommentInstrumentationPayload.new(actor: actor, repository: repository, comment_type: comment_type).payload
    payload[:dimensions]["saved_reply_id"] = saved_reply.id

    if saved_reply.user
      payload[:dimensions]["saved_reply_user_id"] = saved_reply.user.id.to_s
      payload[:dimensions]["saved_reply_user_login"] = saved_reply.user.login
    end

    payload
  end
end
