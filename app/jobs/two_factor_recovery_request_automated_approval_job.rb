# frozen_string_literal: true

class TwoFactorRecoveryRequestAutomatedApprovalJob < ApplicationJob
  queue_as :two_factor_recovery

  retry_on ActiveRecord::RecordNotSaved, wait: 30.seconds, attempts: 3

  def perform(request)
    return unless request.review_state == :ready_for_review
    review = TwoFactorRecoveryRequestReview.mget([request.id])
    return if review.empty? || review[request.id]["review_required"]
    user = request.user

    request.approve(User.ghost)

    GitHub.instrument("two_factor_account_recovery.staff_approve", user: user, reason: "Automated approval")

    GlobalInstrumenter.instrument("two_factor_account_recovery.updated",
      action_type: :STAFF_APPROVED,
      user: user,
      evidence_type: request.hydro_evidence_type,
    )
  end
end
