# frozen_string_literal: true

MoveRepoDownloadsJob = LegacyApplicationJob.wrap(GitHub::Jobs::MoveRepoDownloads)
