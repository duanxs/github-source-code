# frozen_string_literal: true

RepairWikisIndexJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepairWikisIndex)
