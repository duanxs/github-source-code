# frozen_string_literal: true

class OnboardAdvancedSecurityBetaJob < ApplicationJob
  queue_as :mailers

  locked_by timeout: 1.hour, key: ->(job) { job.membership_id }
  discard_on ActiveRecord::RecordNotFound

  def perform(*args)
    return unless membership.member.advanced_security_public_beta_enabled? || membership.member.advanced_security_private_beta_enabled?
    return if membership.member.organization? && !membership.member.member?(membership.actor)

    AdvancedSecurityBetaMailer.waitlist_acceptance(membership).deliver_now
  end

  def membership_id
    arguments.first[:membership_id]
  end

  def membership
    @membership ||= ActiveRecord::Base.connected_to(role: :reading) do
      EarlyAccessMembership.find(membership_id)
    end
  end
end
