# frozen_string_literal: true

RepositoryRestoreJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepositoryRestore)
