# frozen_string_literal: true

RepairDiscussionsIndexJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepairDiscussionsIndex)
