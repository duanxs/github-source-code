# frozen_string_literal: true

class IssueTriageJob < ApplicationJob
  class IssueTriageUpdateFailed < StandardError ; end

  queue_as :issue_triage
  retry_on_dirty_exit

  def perform(job_id, issue_ids, current_user_id, params)
    params = params.with_indifferent_access

    status = JobStatus.find!(job_id)
    status.track do
      if current_user = User.find_by_id(current_user_id)
        Issue.find(issue_ids).each do |issue|
          update_issue(issue, current_user, params) if issue.editable_by?(current_user)
        end
      end
    end
  end

  private

  def update_issue(issue, current_user, params)
    GitHub.context.push(actor_id: current_user.id) do
      Audit.context.push(actor_id: current_user.id) do
        # Assign state
        case params[:state]
        when "open"
          return issue.open(current_user)
        when "closed"
          return issue.close(current_user)
        end

        # Assign an assignee
        if params.key?(:assignee)
          issue.assignee = User.find_by_id(params[:assignee])
        end

        if params[:assignees].is_a?(Hash)
          add_ids = params[:assignees].select { |id, value| value == "1" }.map { |id, value| id }
          delete_ids = params[:assignees].select { |id, value| value == "0" }.map { |id, value| id }

          old_assignees = issue.assignees
          new_assignees = old_assignees.reject { |assignee| delete_ids.include?(assignee.id.to_s) }
          new_assignees += User.where(id: add_ids)
          new_assignees = new_assignees.uniq(&:id)
          issue.assignees = new_assignees
        end

        if params[:clear_assignees].present?
          issue.assignees = []
        end

        # Assign a milestone
        if params.key?(:milestone)
          milestone = issue.repository.milestones.find_by_id(params[:milestone])
          issue.milestone = milestone
        end

        # Assign labels
        if params[:labels].is_a?(Hash)
          add_ids = params[:labels].select { |id, value| value == "1" }.map { |id, value| id }
          delete_ids = params[:labels].select { |id, value| value == "0" }.map { |id, value| id }

          issue.add_label_ids(add_ids)
          issue.delete_label_ids(delete_ids)
        end

        begin
          saved = issue.save
        rescue GitHub::Prioritizable::Context::LockedForRebalance
          return report_errors("Triaging issue #{issue.id} failed because milestone #{params[:milestone]} is locked for rebalance.")
        end
        if GitHub.rails_6_0?
          report_errors("Triaging for issue #{issue.id} with invalid #{issue.errors.keys}") unless saved
        else
          report_errors("Triaging for issue #{issue.id} with invalid #{issue.errors.attribute_names}") unless saved
        end
      end
    end
  end

  def report_errors(error_msg)
    Failbot.push(app: "github-user")
    Failbot.report(IssueTriageUpdateFailed.new(error_msg))
  end
end
