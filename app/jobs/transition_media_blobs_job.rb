# frozen_string_literal: true

TransitionMediaBlobsJob = LegacyApplicationJob.wrap(GitHub::Jobs::TransitionMediaBlobs)
