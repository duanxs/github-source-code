# frozen_string_literal: true

ClearSoftBouncesJob = LegacyApplicationJob.wrap(GitHub::Jobs::ClearSoftBounces)
