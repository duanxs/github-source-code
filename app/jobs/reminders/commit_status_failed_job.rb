# frozen_string_literal: true

module Reminders
  class CommitStatusFailedJob < RealTimeJob
    include CiFailed

    def perform(commit_status_id:, repository_id:, sha:, event_at:, transaction_id:)
      statuses = Statuses::Service.current_for_shas(repository_id: repository_id, shas: [sha])

      commit_status = statuses.detect { |status| status.id == commit_status_id }
      return unless commit_status.present?
      return unless commit_status.state == Status::FAILURE

      pull_requests = commit_status.related_pull_requests
      return if pull_requests.empty?

      events = ci_failed_events(pull_requests, commit_status)

      queue_reminder_events(events)
    end

    private

    def target_name(status)
      status.context
    end

    def target_url(status)
      status.target_url
    end
  end
end
