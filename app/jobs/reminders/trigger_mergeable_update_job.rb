# frozen_string_literal: true

module Reminders
  class TriggerMergeableUpdateJob < RealTimeJob
    def self.delay
      Rails.env.development? ? 0 : 15.minutes
    end

    def self.perform_later_with_delay(*args)
      args = args.first
      if delay.zero?
        perform_later(**args)
      else
        set(wait: delay).perform_later(**args)
      end
    end

    locked_by timeout: 30.minutes, key: -> (job) { job.build_lock_key }

    def perform(push_id:, ref:, repo_id:, event_at:, transaction_id:)
      push = Push.find(push_id)
      repository = push.repository

      return unless repository

      user_ids = PersonalReminder.listener_ids_for(repository.owner, event_type: :merge_conflict)

      if user_ids.present?
        pull_requests = PullRequest.open_based_on_ref(repository, push.branch_name).where(user_id: user_ids)
        pull_requests.map(&:enqueue_mergeable_update)
      end
    end

    def build_lock_key
      [
        arguments.first[:ref],
        arguments.first[:repo_id],
      ].join("-")
    end
  end
end
