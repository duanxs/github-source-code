# frozen_string_literal: true

module Reminders
  class MigrateEligibleRemindersJob < ApplicationJob
    areas_of_responsibility :ce_extensibility
    queue_as :reminders

    def perform(organization, workspace_id: nil, channel_id: nil, ignore_team_warnings: false, notify_user_of_problem: true)
      return unless uses_pull_reminders?(organization)

      pull_reminders_client = PullRemindersClient.new(organization)
      pull_reminders_client.mark_as_ready_to_migrate

      pull_reminders_client.team_reminders(channel_id).each do |team_reminder_data|
        MigrateReminderJob.perform_later(organization, team_reminder_data, ignore_team_warnings: ignore_team_warnings, notify_user_of_problem: notify_user_of_problem)
      end
    end

    private
    def uses_pull_reminders?(organization)
      Apps::Internal.integration(:pull_panda).installations.where(target: organization).exists?
    end
  end
end
