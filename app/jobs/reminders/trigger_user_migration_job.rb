# frozen_string_literal: true

module Reminders
  class TriggerUserMigrationJob < ApplicationJob
    areas_of_responsibility :ce_extensibility
    queue_as :reminders

    around_perform :use_database_replica

    def perform(user, workspace_id:, organization:)
      return unless uses_pull_reminders?(organization)

      PullRemindersClient.ready_to_migrate(organization)
      MigratePersonalReminderJob.perform_later(
        organization,
        workspace_id: workspace_id,
        username: user.login,
        ignore_missing: true
      ) unless PersonalReminder.where(remindable: organization, user: user).exists?
    end

    private
    def uses_pull_reminders?(organization)
      Apps::Internal.integration(:pull_panda).installations.where(target: organization).exists?
    end

    def use_database_replica
      ActiveRecord::Base.connected_to(role: :reading) do
        yield
      end
    end
  end
end
