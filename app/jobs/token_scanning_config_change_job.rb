# frozen_string_literal: true

class TokenScanningConfigChangeJob < ApplicationJob
  areas_of_responsibility :token_scanning
  queue_as :token_scanning_config_change

  def perform(*args)
    @unresolved_tokens = git_repo.token_scan_results.unresolved
    @unresolved_token_locations = TokenScanResultLocation.where(token_scan_result_id: @unresolved_tokens)

    excluded_tokens_before = get_excluded_tokens

    apply_config_to_tokens

    excluded_tokens_after = get_excluded_tokens
    excluded_tokens_after_ids = excluded_tokens_after.map { |token| token.id }

    newly_visible_tokens = excluded_tokens_before.select { |token| !excluded_tokens_after_ids.include? token.id }

    # send email with previously ignored tokens that are now exposed to user
    if newly_visible_tokens.any?
      scope = git_repo.config_file ? AccountMailer::TOKEN_SCANNING_SCOPE[:config_change_scoped] : AccountMailer::TOKEN_SCANNING_SCOPE[:config_deleted_scoped]
      users = git_repo.token_scanning_users_to_notify
      return if users.empty?

      AccountMailer.token_scanning_summary(git_repo, newly_visible_tokens, users, scope).deliver_now
    end

    # record telemetry
    if git_repo.config_file
      record_config_changed_stats(newly_visible_tokens.any?)
    else
      record_config_deleted_stats(newly_visible_tokens.any?)
    end

    GitHub.dogstats.count("token_scanning_config_change_newly_visible_tokens_count", newly_visible_tokens.size)
    GitHub.dogstats.count("token_scanning_config_change_excluded_token_count", excluded_tokens_after.size)
    GitHub.dogstats.count("token_scanning_config_change_excluded_locations_count", TokenScanResultLocation.where(token_scan_result_id: excluded_tokens_after).size)
    GitHub.dogstats.count("token_scanning_config_change_unresolved_token_count", @unresolved_tokens.size)
    GitHub.dogstats.count("token_scanning_config_change_unresolved_locations_count", @unresolved_token_locations.size)
  end

  def git_repo
    ActiveRecord::Base.connected_to(role: :reading) do
      @git_repo ||= Repository.find_by_id(self.arguments.first)
    end
  end

  def get_excluded_tokens
    included_tokens = @unresolved_tokens.included
    @unresolved_tokens.select { |token| !included_tokens.exists? token.id }
  end

  # marks tokens based on the config file.
  def apply_config_to_tokens
    TokenScanResultLocation.transaction do
      if git_repo.config_file # config exists; mark locations based on config
        @unresolved_token_locations.each do |token_location|
          if git_repo.token_in_config_excluded_path?(token_location.path)
            token_location.exclude_by_path! if token_location.include?
          else
            token_location.include! if token_location.exclude_by_path?
          end
        end
      else # config was deleted; mark all locations included
        @unresolved_token_locations.update_all "ignore_token = 0"
      end
    end
  end

  def record_config_changed_stats(email_sent)
    GitHub.dogstats.increment("token_scanning_config_changed")

    if email_sent
      GitHub.dogstats.increment("token_scanning_config_changed_email")
    end

    parsed_config = TokenScanningConfigurationFile.new(git_repo.config_file)

    GitHub.dogstats.increment("token_scan_config_pattern_limit_exceeded") if parsed_config.omitted_paths.length > TokenScanningConfigurationFile::MAX_NUMBER_OF_PATTERNS
    GitHub.dogstats.increment("token_scan_config_max_file_size_exceeded") if git_repo.config_file.size > TokenScanningConfigurationFile::MAX_FILE_SIZE
  end

  def record_config_deleted_stats(email_sent)
    GitHub.dogstats.increment("token_scanning_config_deleted")

    if email_sent
      GitHub.dogstats.increment("token_scanning_config_deleted_email")
    end
  end
end
