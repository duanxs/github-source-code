# frozen_string_literal: true

RepositoryRenameJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepositoryRename)
