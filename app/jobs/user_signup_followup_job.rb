# frozen_string_literal: true

class UserSignupFollowupJob < ApplicationJob
  areas_of_responsibility :user_growth
  queue_as :user_signup_followup

  # Only a single unique instance of the job can be concurrently running
  # per verification banner being triggered
  locked_by timeout: 1.hour, key: -> (job) { "user_signup_followup::#{job.arguments[0]}" }

  # Sends a reminder email to verify account after user creation
  # user          - The GlobalID of the user that was created.
  #
  # Returns nothing.

  def perform(user)
    return unless user
    kv_key = "user_id.#{user.id}.user_signup_followup_job"

    if GitHub.kv.exists(kv_key).value!
      return
    else
      GitHub.kv.set(kv_key, "true", expires: 2.days.from_now)
      ApplicationRecord::Domain::KeyValues.throttle do
        user.send_email_verification_reminder
      end
    end
  end
end
