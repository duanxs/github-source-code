# frozen_string_literal: true

class GistTokenScanningJob < RepositoryTokenScanningJob
  areas_of_responsibility :token_scanning
  queue_as :token_scanning

  def job_scope
    "gist"
  end

  def job_visibility
    "public"
  end

  def git_repo
    ActiveRecord::Base.connected_to(role: :reading) do
      @git_repo ||= Gist.find_by_id(self.arguments.first)
    end
  end

  # Get the GitHub URL for a commit/file.
  #
  # commit_sha - A String commit sha1.
  # path       - The path within a repository to the given file.
  #
  # Returns a String URL.
  def blob_url(commit_sha, path)
    git_repo.url(revision: commit_sha)
  end
end
