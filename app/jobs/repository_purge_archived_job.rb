# frozen_string_literal: true

class RepositoryPurgeArchivedJob < ApplicationJob
  queue_as :repository_purge_archived
  retry_on_dirty_exit


  # Only one of these jobs should run at any given time.
  locked_by timeout: 1.hour, key: ApplicationJob::DEFAULT_LOCK_PROC

  schedule interval: 4.hours

  BATCH_SIZE = 10
  MINIMUM_AGE_TO_PURGE = 3.months

  # Scheduled on both github.com and GHE.
  def self.enabled?
    true
  end

  def self.expiration_period
    MINIMUM_AGE_TO_PURGE
  end

  def perform
    SlowQueryLogger.disabled do
      expire_time = Time.now - self.class.expiration_period
      if Archived::Repository.purge_stale_records(BATCH_SIZE, expire_time).any?
        clear_lock
        RepositoryPurgeArchivedJob.perform_later
      end
    end
  end
end
