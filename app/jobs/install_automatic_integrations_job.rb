# frozen_string_literal: true

InstallAutomaticIntegrationsJob = LegacyApplicationJob.wrap(GitHub::Jobs::InstallAutomaticIntegrations)
