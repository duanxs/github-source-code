# frozen_string_literal: true

class QueueCollectMetricsJob < ApplicationJob
  schedule interval: 1.day
  queue_as :collect_metrics

  def self.enabled?
    GitHub.enterprise?
  end

  def perform
    return unless self.class.enabled?
    CollectMetricsJob.perform_later
  end
end
