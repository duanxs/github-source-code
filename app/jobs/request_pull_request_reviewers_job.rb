# frozen_string_literal: true

class RequestPullRequestReviewersJob < ApplicationJob
  queue_as :request_pr_reviewers

  retry_on PullRequest::DetermineCodeownersError, attempts: 1

  def perform(pull_request, user)
    pull_request.request_review_from_codeowners(user, should_save: true)

    if pull_request.repository.pull_request_revisions_enabled?
      pull_request.request_review_from(
        reviewers: pull_request.submitted_reviewers,
        actor: user,
        re_request: true,
        append: true,
        should_save: true
      )

      pull_request.review_requests.deferred.each do |request|
        request.ready!(user: user)
      end
    end
  end
end
