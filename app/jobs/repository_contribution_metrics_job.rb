# frozen_string_literal: true

# Generates Repository contribution graph metrics that power certain insights graphs.
#
# Call `Repository::ContributionGraphStatus#enqueue_metrics_job` to enqueue the job

class RepositoryContributionMetricsJob < ApplicationJob
  areas_of_responsibility :repos
  queue_as :graphs
  locked_by timeout: 1.hour, key: -> (job) { job.arguments[0][:contribution_graph_status].id }

  retry_on ActiveRecord::RecordNotFound, JobStatus::RecordNotFound, wait: :exponentially_longer, attempts: 5 # will retry 4 times over 6 minutes

  def perform(contribution_graph_status:, from_oid:, to_oid:)
    job_status = JobStatus.find!(contribution_graph_status.job_status_id)
    push_failbot_context(contribution_graph_status)

    job_status.track do
      contribution_graph_status.generate_metrics(from_oid: from_oid, to_oid: to_oid)
      contribution_graph_status.update_top_contributors!
    end

    # Hack: Leave enough time for the hydro publisher to be flushed. Flushing is supposed to
    # happen automatically when the job is finished but there appears to be an issue with this
    # with the development publisher.
    sleep(10) if Rails.env.development?
  end

  private

  def push_failbot_context(contribution_graph_status)
    repo_id = contribution_graph_status.repository&.id

    Failbot.push(
      repo_id: repo_id,
      job_status_id: contribution_graph_status.job_status_id,
    )
  end
end
