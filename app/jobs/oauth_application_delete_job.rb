# frozen_string_literal: true

OauthApplicationDeleteJob = LegacyApplicationJob.wrap(GitHub::Jobs::OauthApplicationDelete)
