# frozen_string_literal: true

class MeteredBillingThresholdNotifierJob < ApplicationJob
  areas_of_responsibility :gitcoin

  queue_as :billing

  locked_by timeout: 5.minutes, key: DEFAULT_LOCK_PROC

  def perform(product:, owner:)
    Billing::MeteredThresholdNotifier.new(
      owner: owner,
      product: product,
    ).notify_if_applicable
  end
end
