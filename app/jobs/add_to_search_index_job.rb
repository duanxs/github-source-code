# frozen_string_literal: true

AddToSearchIndexJob = LegacyApplicationJob.wrap(GitHub::Jobs::AddToSearchIndex)
