# frozen_string_literal: true

PullRequestReviewStatsJob = LegacyApplicationJob.wrap(GitHub::Jobs::PullRequestReviewStats)
