# frozen_string_literal: true

class DestroyIntegrationJob < ApplicationJob
  Legacy = LegacyApplicationJob.wrap(GitHub::Jobs::DestroyIntegration)

  discard_on(ActiveRecord::RecordNotFound) do |job, error|
    Failbot.report(error)
  end

  areas_of_responsibility :platform
  queue_as :destroy_integration

  def perform(integration_id)
    Integration.find(integration_id).destroy
  end

end
