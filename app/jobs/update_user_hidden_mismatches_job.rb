# frozen_string_literal: true

UpdateUserHiddenMismatchesJob = LegacyApplicationJob.wrap(GitHub::Jobs::UpdateUserHiddenMismatches)
