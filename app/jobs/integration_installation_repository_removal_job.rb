# frozen_string_literal: true

class IntegrationInstallationRepositoryRemovalJob < ApplicationJob
  areas_of_responsibility :platform

  queue_as :integration_installation_repository_removal

  # Discard the job if the installation is deleted before the job runs
  discard_on ActiveRecord::RecordNotFound

  def perform(installation, repository_id = nil)
    if can_be_uninstalled_automatically?(installation)
      userish     = installation.target
      uninstaller = userish.organization? ? userish.admins.first : userish

      UninstallIntegrationInstallationJob.perform_later(uninstaller.id, installation.id)
    else
      if repository_id
        abilities = installation.abilities(subject_ids: [repository_id])
        Permissions::QueryRouter.dual_delete_app_permissions(abilities: abilities) do
          Ability.where(id: abilities.map(&:id)).delete_all
        end
      end
      GitHub::Jobs::UpdateIntegrationInstallationRateLimit.enqueue(installation.id)
    end
  end

  private

  # Internal: Can this IntegrationInstallation record be uninstalled
  # without user intervention?
  #
  # Returns a Boolean.
  def can_be_uninstalled_automatically?(installation)
    # If it's installed on the target User/Organization
    # we don't want to uninstall since repository selection isn't needed.
    return false if installation.installed_on_all_repositories?

    # If there are organization permissions we
    # don't want to uninstall
    return false unless installation.repository_permissions_only?

    # Ensure there are no repositories
    # remaining on the installation
    installation.repositories.none?
  end
end
