# frozen_string_literal: true

RepairIntegrationsIndexJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepairIntegrationsIndex)
