# frozen_string_literal: true

RepositoryMirrorJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepositoryMirror)
