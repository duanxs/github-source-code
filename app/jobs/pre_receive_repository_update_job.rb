# frozen_string_literal: true

PreReceiveRepositoryUpdateJob = LegacyApplicationJob.wrap(GitHub::Jobs::PreReceiveRepositoryUpdate)
