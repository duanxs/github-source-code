# frozen_string_literal: true

TransferRepositoryJob = LegacyApplicationJob.wrap(GitHub::Jobs::TransferRepository)
