# frozen_string_literal: true

RepairProjectsIndexJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepairProjectsIndex)
