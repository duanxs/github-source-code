# frozen_string_literal: true

class ProcessSponsorsApplicationJob < ApplicationJob
  queue_as :sponsors_application_processing

  locked_by timeout: 1.hour, key: ->(job) { job.arguments[0].id }

  class MembershipAcceptanceError < StandardError; end

  def perform(sponsors_membership)
    return unless sponsors_membership.auto_acceptable?

    result = Sponsors::AcceptSponsorsMembership.call(
      sponsors_membership: sponsors_membership,
      actor: nil,
      automated: true,
    )

    unless result.success?
      raise MembershipAcceptanceError.new \
        "Failed to auto accept application: #{result.errors.to_sentence}"
    end
  end
end
