# frozen_string_literal: true

class UpdateNotificationSummaryJob < ApplicationJob
    queue_as :notifications

    areas_of_responsibility :notifications

    class UpdateNotificationSummaryError < StandardError; end
    retry_on UpdateNotificationSummaryError, wait: :exponentially_longer, attempts: 20
    retry_on_dirty_exit

    def perform(summarizable_class, summarizable_id, enqueue = false, retryable = false, options = {})
      options = options.with_indifferent_access

      summarizable_class = begin
        summarizable_class.constantize
      rescue NameError
        return
      end
      return unless summarizable = summarizable_class.find_by_id(summarizable_id)

      succeeded = RollupSummary.throttle { summarizable.update_notification_summary(enqueue: false) }

      raise UpdateNotificationSummaryError unless succeeded
    end
end
