# frozen_string_literal: true

CalculateTrendingUsersJob = LegacyApplicationJob.wrap(GitHub::Jobs::CalculateTrendingUsers)
