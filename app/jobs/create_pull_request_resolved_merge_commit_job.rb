# frozen_string_literal: true

CreatePullRequestResolvedMergeCommitJob = LegacyApplicationJob.wrap(GitHub::Jobs::CreatePullRequestResolvedMergeCommit)
