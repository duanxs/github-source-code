# frozen_string_literal: true

FeatureShowcasesJob = LegacyApplicationJob.wrap(GitHub::Jobs::FeatureShowcases)
