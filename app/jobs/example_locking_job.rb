# frozen_string_literal: true

class ExampleLockingJob < ApplicationJob
  queue_as :example_locking

  locked_by key: ApplicationJob::DEFAULT_LOCK_PROC, timeout: 5.minutes

  def perform(*args)
    # no-op
  end
end
