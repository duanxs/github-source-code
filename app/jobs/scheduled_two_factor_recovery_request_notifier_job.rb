# frozen_string_literal: true

class ScheduledTwoFactorRecoveryRequestNotifierJob < ApplicationJob
  extend GitHub::HashLock

  queue_as :zendesk

  schedule interval: 1.hour, condition: -> { !GitHub.enterprise? }

  def perform
    recovery_requests = TwoFactorRecoveryRequest.ready_for_review

    return if recovery_requests.empty?

    to_review, to_automate = recovery_requests.partition do |req|
      # Review happens for all requests as UI aspects are dependent upon it.
      review = TwoFactorRecoveryRequestReview.new(req)
      review.perform

      # This flag needs to remain in place as a failsafe
      # In the event that some form of attack is formulated
      # This allows Prodsec to force manual review of all cases
      if GitHub.flipper[:phoenix_two_factor_lockout_request_flow].enabled?(req.user)
        review.review_required?
      else
        true
      end
    end

    to_automate.each { |request| TwoFactorRecoveryRequestAutomatedApprovalJob.perform_later(request) } if to_automate.present?

    zendesk_ticket_user_ids = []

    to_review.each do |request|
      user_id = request.user_id

      next if zendesk_ticket_user_ids.include?(user_id)

      result = submit_zendesk_ticket(user_id)

      next unless result

      zendesk_ticket_user_ids.push(user_id)
      request.update!(staff_review_requested_at: Time.now)
      cleanup_other_requests(request.user, request)
    end unless to_review.empty?
  end

  private

  def submit_zendesk_ticket(user_id)
    locked_out_user = User.find_by_id(user_id)

    return false unless locked_out_user.present?

    email = locked_out_user.type == "User" ? locked_out_user.primary_user_email&.email : nil
    login = locked_out_user.login
    name = locked_out_user.profile&.name

    custom_fields = {
      GitHub.zendesk_fields[:business_plus]   => locked_out_user.business_plus?,
      GitHub.zendesk_fields[:login]           => login,
      GitHub.zendesk_fields[:plan]            => locked_out_user.plan.name,
      GitHub.zendesk_fields[:user_spammy]     => locked_out_user.spammy?,
      GitHub.zendesk_fields[:user_suspended]  => locked_out_user.suspended?,
    }

    staff_review_url = "#{GitHub.stafftools_url}/stafftools/users/#{login}/security"

    subject = "Account request completed"
    body = "#{login} has completed an account recovery request to disable two-factor authentication on their account.\n\nPlease review the details in stafftools: #{staff_review_url}.\n"

    tags = ["automated-2fa-recovery"]

    CreateZendeskTicket.perform_later(
      name.presence || login,
      email.presence || "accountrecovery@noreply.github.com",
      subject,
      body,
      brand_id: GitHub.zendesk_brand_id,
      tags: tags,
      custom_fields: custom_fields,
    )

    true
  end

  def cleanup_other_requests(user, request)
    other_requests = user.two_factor_recovery_requests.where.not(id: request.id)
    other_requests.each do |r|
      r.instrument_ignore
      r.destroy
    end
  end
end
