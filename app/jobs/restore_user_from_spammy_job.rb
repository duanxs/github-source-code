# frozen_string_literal: true

RestoreUserFromSpammyJob = LegacyApplicationJob.wrap(GitHub::Jobs::RestoreUserFromSpammy)
