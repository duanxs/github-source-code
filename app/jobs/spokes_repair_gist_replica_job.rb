# frozen_string_literal: true

SpokesRepairGistReplicaJob = LegacyApplicationJob.wrap(GitHub::Jobs::DgitRepairGistReplica)
