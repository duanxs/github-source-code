# frozen_string_literal: true

class SubmitZuoraUsageFileJob < ApplicationJob
  BUCKET_NAME = "billing-usage-batches"

  areas_of_responsibility :gitcoin

  queue_as :zuora

  locked_by key: ApplicationJob::DEFAULT_LOCK_PROC, timeout: 5.minutes

  RETRYABLE_ERRORS = [
    Aws::S3::Errors::ServiceError,
    Faraday::ConnectionFailed,
    Faraday::SSLError,
    Faraday::TimeoutError,
    Net::OpenTimeout,
  ].freeze

  discard_on(StandardError) do |job, error|
    GitHub.dogstats.increment(
      "zuora.usage_file_error",
      tags: ["exception:#{error.class.name.parameterize}"],
    )

    Failbot.report(error)
  end

  RETRYABLE_ERRORS.each do |error_class|
    retry_on error_class do |job, error|
      Failbot.report(error)
    end
  end

  # Public: Submit a usage synchronization batch to Zuora
  #
  # usage_synchronization_batch - A UsageSynchronizationBatch record for the
  #                               batch file to be submitted
  #
  # Returns nothing
  def perform(usage_synchronization_batch)
    unless usage_synchronization_batch.can_submit?
      GitHub.dogstats.increment(
        "zuora.usage_file_already_submitted",
        tags: ["batch_status:#{usage_synchronization_batch.status}"],
      )

      return
    end

    if import = existing_successful_import(usage_synchronization_batch)
      mark_batch_submitted(usage_synchronization_batch, import.zuora_status_url)

      GitHub.dogstats.increment(
        "zuora.usage_file_import_exists",
        tags: ["import_status:#{import.status}"],
      )

      return
    end

    Tempfile.open(usage_synchronization_batch.upload_filename) do |file|
      s3_client.get_object(
        bucket: BUCKET_NAME,
        key: usage_synchronization_batch.upload_filename,
        response_target: file,
      )

      response = upload_file_to_zuora(file, usage_synchronization_batch.upload_filename)
      handle_response(usage_synchronization_batch, response)
    end
  end

  private

  def existing_successful_import(synchronization_batch)
    Billing::Zuora::Import.successful_imports(name: synchronization_batch.upload_filename).first
  end

  def handle_response(synchronization_batch, response)
    raise GitHub::Zuora::Error.new(response) unless response["success"]

    mark_batch_submitted(synchronization_batch, response["checkImportStatus"])
    GitHub.dogstats.increment("zuora.usage_file_submitted", tags: ["product:#{synchronization_batch.product}"])
  end

  def upload_file_to_zuora(file, upload_filename)
    payload = { file:  Zuorest::UploadIO.new(file.path, "text/csv", upload_filename) }
    headers = { "Content-Type": "multipart/form-data" }
    GitHub.zuorest_client.create_usage(payload, headers)
  end

  def mark_batch_submitted(batch, zuora_status_url)
    batch.status = :submitted
    batch.zuora_status_url = zuora_status_url
    batch.save!
  end

  def s3_client
    GitHub.s3_billing_client
  end
end
