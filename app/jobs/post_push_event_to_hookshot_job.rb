# frozen_string_literal: true

class PostPushEventToHookshotJob < ApplicationJob
  include Hookshot::DeliverJobLogger

  class PayloadTooLarge < Hookshot::HookshotError; end
  PAYLOAD_TOO_LARGE_MSG = "Payload size of %s exceeds limit: %s, will not be delivered to Hookshot.".freeze

  areas_of_responsibility :webhook

  queue_as :post_to_hookshot

  discard_on ActiveRecord::RecordNotFound

  RETRYABLE_ERRORS = [
    Errno::ECONNREFUSED,
    Errno::ECONNRESET,
    Errno::ETIMEDOUT,
    Faraday::ConnectionFailed,
    Faraday::TimeoutError,
    Net::HTTPRequestTimeOut,
    Net::ReadTimeout,
    GitRPC::CommandBusy,
    Aqueduct::Client::RequestError,
  ].freeze

  retry_on(*RETRYABLE_ERRORS, wait: :exponentially_longer, attempts: 5)

  def perform(delivery_payloads, options = {})
    delivery_payloads.map! { |payload| payload.symbolize_keys }
    undelivered_payloads = delivery_payloads.reject { |payload| payload[:delivered] }

    undelivered_payloads.each do |delivery_payload|
      begin
        payload = enrich_payload(delivery_payload)

        if (GitHub.deliver_hooks_via_resque? || GitHub.deliver_hooks_via_aqueduct?) && payload_fits_in_aqueduct?(payload)
          delivery_payload[:delivered_hook_ids] ||= []
          payload[:hooks].each do |hook_config|
            hook_config = hook_config.symbolize_keys
            next if delivery_payload[:delivered_hook_ids].include?(hook_config[:id].to_i)
            hook_payload = payload.dup.tap do |hash|
              hash[:hook] = hook_config
              hash.delete(:hooks)
              hash.delete(:delivered_hook_ids)
            end

            if GitHub.deliver_hooks_via_resque?
              payload = hook_payload.merge(
                github_request_id: GitHub.context[:request_id],
                enqueued_at: (Time.now.to_f * 1_000).round,
              )

              Resque.enqueue(HookshotGoWebhooksJob, payload)
            else
              Hook::DeliverySystem.post_to_aqueduct(hook_payload)
            end
            delivery_payload[:delivered_hook_ids] << hook_config[:id].to_i
          end
        else
          Hook::DeliverySystem.post_payload_to_hookshot(payload)
          delivery_payload[:delivered] = true
        end
      rescue PayloadTooLarge, Hookshot::PayloadTooLarge => e
        if e.is_a?(Hookshot::PayloadTooLarge)
          log_params = {
            error: e,
            parent: delivery_payload[:parent],
            guid: delivery_payload[:guid],
          }

          log_payload_too_large_error(**log_params)
        else
          GitHub::Logger.log_exception(delivery_payload[:guid], e)
        end

        Failbot.report e
      end
    end
  end

  def enrich_payload(delivery_payload)
    webhook_payload = delivery_payload[:payload]

    # Every payload sent to this job has the same `webhook_payload`. Thus we
    # can cache the event between payloads.
    #
    @repo ||= ActiveRecord::Base.connected_to(role: :reading) do
      Repository.find(webhook_payload.dig(:repository, :id))
    end
    @event ||= begin
      event_attrs = {
        repo: @repo,
        before: webhook_payload[:before],
        after: webhook_payload[:after],
        ref: webhook_payload[:ref],
      }

      Hook::Event::PushEvent.new(event_attrs)
    end

    # Special case for Actions which wants a smaller payload As explained above
    # we can cache both sets of git data as they are the same between payloads.
    if use_actions_push_payload?(delivery_payload, @repo)
      payload_generator = Hook::Payload::ActionsPushPayload.new(@event, include_git_data: true)
      @slim_data ||= payload_generator.git_only
      delivery_payload[:payload] = delivery_payload[:payload].merge(@slim_data)
    else
      payload_generator = Hook::Payload::PushPayload.new(@event, include_git_data: true)
      @full_data ||= payload_generator.git_only
      delivery_payload[:payload] = delivery_payload[:payload].merge(@full_data)
    end

    check_payload_size!(delivery_payload)

    delivery_payload
  end

  def check_payload_size!(payload)
    payload_size = payload.to_json.bytesize

    tags = GitHub::TaggingHelper.create_hook_event_tags("push")
    tags += ["is_large_aqueduct_payload:#{GitHub::Aqueduct.is_payload_size_large?(payload_size)}"]
    GitHub.dogstats.histogram("hooks.hookshot_payload.payload_size", payload_size, tags: tags)

    if payload_size > GitHub.hookshot_payload_size_limit
      payload[:hooks].each do
        GitHub.dogstats.histogram("hooks.hookshot_payload.payload_too_large", payload_size, tags: tags)
      end

      raise PayloadTooLarge, (PAYLOAD_TOO_LARGE_MSG % [payload_size, GitHub.hookshot_payload_size_limit])
    end
  end

  def use_actions_push_payload?(delivery_payload, repo)
    delivery_payload[:parent] == "integration-#{GitHub.launch_github_app&.id}"
  end

  def payload_fits_in_aqueduct?(payload)
    !GitHub::Aqueduct.is_payload_size_large?(payload.to_json.bytesize)
  end
end
