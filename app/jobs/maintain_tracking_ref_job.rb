# frozen_string_literal: true

MaintainTrackingRefJob = LegacyApplicationJob.wrap(GitHub::Jobs::MaintainTrackingRef)
