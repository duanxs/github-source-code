# frozen_string_literal: true

UserDeleteJob = LegacyApplicationJob.wrap(GitHub::Jobs::UserDelete)
