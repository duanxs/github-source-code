# frozen_string_literal: true

module TradeControls
  class PerformPastOrganizationEnforcementJob < ApplicationJob
    BATCH_SIZE = 100
    queue_as :trade_controls

    schedule interval: 24.hours, condition: -> { GitHub.billing_enabled? }

    def perform
      # We don't want to run back enfocements until we fully enable the following feature flag.
      if GitHub.flipper[:organziation_enforcements_for_pending].enabled?
        TradeControls::PendingRestriction.past_scheduled_date.in_batches(of: BATCH_SIZE) do |batch_scope|
          TradeControls::PendingRestriction.throttle_with_retry(max_retry_count: 3) do
            batch_scope.each do |restriction|
              OrganizationEnforcementJob.perform_later(restriction)
            end
          end
        end
      end
    end
  end
end
