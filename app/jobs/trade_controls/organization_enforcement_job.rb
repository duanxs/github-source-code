# frozen_string_literal: true

module TradeControls
  # Enforces trade restriction on an organization using its pending
  # restriction record when its 14 day grace period is over.
  class OrganizationEnforcementJob < ApplicationJob
    queue_as :trade_controls

    discard_on ActiveJob::DeserializationError

    def perform(restriction)
      if restriction.pending? && GitHub.flipper[:organziation_enforcements_for_pending].enabled?(restriction.organization)
        log_past_scheduled_date_to_dogstats(restriction.organization) if restriction.past_scheduled_run_date?
        compliance = restriction.compliance
        in_violation = compliance.minor_threshold_violation? || compliance.violation?
        return restriction.cancel unless in_violation
        restriction.run
      end
    rescue ::Faraday::TimeoutError => e
      log_exception_to_dogstats(restriction.organization)
    end

    def is_retry?
      executions > 1
    end

    def log_exception_to_dogstats(organization)
      return if is_retry?

      GitHub.dogstats.increment(
        "pending_trade_controls_restriction.enforce.enforce_not_successful",
        tags: ["organization:#{organization.id}", "reason: Timeout error when cancelling subscription"],
      )
    end

    def log_past_scheduled_date_to_dogstats(organization)
      return if is_retry?

      GitHub.dogstats.increment(
        "pending_trade_controls_restriction.enforce.enforce_past_scheduled_run_date",
        tags: ["organization:#{organization.id}"],
      )
    end
  end
end
