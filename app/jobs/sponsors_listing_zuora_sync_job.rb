# frozen_string_literal: true

class SponsorsListingZuoraSyncJob < ApplicationJob
  queue_as :zuora

  RETRYABLE_ERRORS = [
    Errno::ECONNREFUSED,
    Errno::ECONNRESET,
    Errno::ETIMEDOUT,
    Faraday::ConnectionFailed,
    Faraday::TimeoutError,
    Net::HTTPRequestTimeOut,
    Net::ReadTimeout,
    Zuorest::HttpError,
  ]

  retry_on(*RETRYABLE_ERRORS) do |_job, error|
    Failbot.report(error)
  end

  def perform(sponsors_listing)
    sponsors_listing.sync_to_zuora
  end
end
