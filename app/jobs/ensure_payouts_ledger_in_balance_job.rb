# frozen_string_literal: true

class EnsurePayoutsLedgerInBalanceJob < ApplicationJob
  areas_of_responsibility :gitcoin
  queue_as :billing

  # Public: Perform the job
  #
  # stripe_connect_account - The Billing::StripeConnect::Account for which we're
  #                          checking the ledger balance
  #
  # Returns nothing
  def perform(stripe_connect_account)
    Billing::PayoutsLedgerBalance.new(stripe_connect_account).ensure_in_balance
  end
end
