# frozen_string_literal: true

UpdateTableUserHiddenJob = LegacyApplicationJob.wrap(GitHub::Jobs::UpdateTableUserHidden)
