# frozen_string_literal: true

PageCertificateRenewJob = LegacyApplicationJob.wrap(GitHub::Jobs::PageCertificateRenew)
