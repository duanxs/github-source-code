# frozen_string_literal: true

class RewriteMannequinAssociationsJob < ApplicationJob
  queue_as :rewrite_mannequin_associations

  def perform(source, target, invitation)
    rewriter = MannequinAssociationRewriter.new(source, target)

    rewriter.rewrite!

    invitation.complete!
  end
end
