# frozen_string_literal: true
require "grpc"
require "github/launch_client/deployer"

class SetupRepositoryForActionsJob < ApplicationJob
  map_to_service :actions_experience

  queue_as :actions

  def perform(repository:)
    # Actions must be enabled
    return unless GitHub.actions_enabled?

    # The app must be installed
    return unless repository.actions_app_installed?

    # This should not be done already
    return if has_actions_check_suites?(repository)

    GrpcHelper.rescue_from_grpc_errors("SelfHostedRunners") do
      GitHub::LaunchClient::Deployer.setup_repository(repository)
    end
  end

  def has_actions_check_suites?(repository)
    repository.check_suites.where(github_app_id: GitHub.launch_github_app.id).any?
  end
end
