# frozen_string_literal: true

SpokesRepairNetworkReplicaJob = LegacyApplicationJob.wrap(GitHub::Jobs::DgitRepairNetworkReplica)
