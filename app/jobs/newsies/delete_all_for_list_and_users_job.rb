# frozen_string_literal: true

module Newsies
  # Delete all subscriptions and notifications for a list and a given set of users
  class DeleteAllForListAndUsersJob < MaintenanceBaseJob
    # list_type - String newsies list type
    # list_id   - Integer newsies list id
    # user_ids   - Array of Integer user ids
    def perform(list_type, list_id, user_ids)
      list = List.new(list_type, list_id)

      user_ids.each_slice(BATCH_SIZE) do |batched_user_ids|
        ListSubscription.throttle do
          ListSubscription.for_list(list).for_users(batched_user_ids).delete_all
        end

        ThreadTypeSubscription.throttle do
          ThreadTypeSubscription.for_list(list).for_users(batched_user_ids).delete_all
        end

        ThreadSubscription.for_list(list)
                          .for_users(batched_user_ids)
                          .in_batches(of: BATCH_SIZE) do |batched_scope|

          delete_thread_subscription_events(batched_scope.pluck(:id))
          ThreadSubscription.throttle { batched_scope.delete_all }
        end

        NotificationEntry.for_list(list)
                         .for_users(batched_user_ids)
                         .in_batches(of: BATCH_SIZE) do |scope|
          NotificationEntry.throttle { scope.delete_all }
        end

        SavedNotificationEntry.for_list(list)
                              .for_users(batched_user_ids)
                              .in_batches(of: BATCH_SIZE) do |scope|
          SavedNotificationEntry.throttle { scope.delete_all }
        end
      end
    end
  end
end
