# frozen_string_literal: true

module Newsies
  class DeliverNotificationsJob < ApplicationJob
    include SloHelper
    areas_of_responsibility :notifications

    class JobInterrupted < StandardError; end

    QUEUE_NAME = "deliver_notifications"

    queue_as do
      next "#{self.class::QUEUE_NAME}_low" if self.arguments.last.try(:fetch, :priority, nil) == :low
      self.class::QUEUE_NAME
    end

    retry_on(
      Freno::Throttler::Error,
      JobInterrupted,
      *Resiliency::Response::UnavailableExceptions,
      wait: :exponentially_longer,
      attempts: 20,
    )

    attr_reader :priority

    # A subset of the reasons returned by `reason_to_stop_delivery` that describe a situation in
    # which we should both skip notification delivery and subsequently delete all of user's
    # newsies data for a given list.
    REASONS_TO_PURGE_NEWSIES_DATA = [:newsies_disabled, :list_unreadable]

    # Number of subscribers we want to notify in single throttle block
    BATCH_SIZE = 10

    # Delivers a notification.
    #
    # klass          - String ActiveRecord class name.
    # id             - Integer ID of the record.
    # recipient_ids  - (Optional) Array of User IDs to which we should deliver the notifications. If
    #                  omitted, the default of `nil` will cause us to deliver notifications to all
    #                  users who are subscribed to the given content.
    # reason         - (Optional) String specifying the reason for the message. If omitted, the
    #                  default of `nil` will cause us to deliver notifications with the "mentioned"
    #                  reason.
    # options        - (Optional) Hash<String, Object> specifying job options
    #                  - :enqueued_at - UTC Integer/Float timestamp of the time the job was queued
    #                    (e.g. Time.now.utc.to_f)
    #                  - :enforce_interruption - Boolean for whether or not to respect shutdown
    #                    requests
    #                  - :direct_mention_user_ids - Array<Integer> of users who were mentioned
    #                    individually in the content of the notification.
    def perform(klass, id, recipient_ids = nil, reason = nil, event_time:, enqueued_at: nil, enforce_interruption: false, direct_mention_user_ids: [], priority: :high)
      @priority = priority

      return unless object = klass.constantize.find_by_id(id)
      return unless list = object.notifications_list
      return unless thread = object.notifications_thread

      comment = object.is_a?(PullRequest) ? object.issue : object

      return unless comment_deliverable?(list, thread, comment)

      delivery = GitHub.dogstats.time("newsies.delivery.build") do
        summary = RollupSummary.from(list, thread, comment)
        Delivery.new(summary, comment)
      end

      return unless delivery.list && delivery.thread

      @datadog_tags = []

      explicit_subscribers = recipient_ids&.map do |recipient_id|
        Newsies::Subscriber.new(recipient_id, true, false, reason || "mention")
      end

      GitHub.dogstats.time("newsies.delivery.send") do
        deliver_to_subscribers(
          delivery,
          explicit_subscribers: explicit_subscribers,
          direct_mention_user_ids: direct_mention_user_ids,
          enqueued_at: enqueued_at,
          enforce_interruption: enforce_interruption,
          event_time: event_time,
        )
      end
    end

    private

    # Checks if the list/thread/comment should be delivered as notifications at all
    # or if we should abort delivery early
    def comment_deliverable?(list, thread, comment)
      return false unless list.present? && thread.present? && comment.present?
      return false if comment.notifications_author&.spammy?
      true
    end

    def deliver_to_subscribers(delivery, event_time:, explicit_subscribers: nil, direct_mention_user_ids: [], enqueued_at: nil, enforce_interruption: false)
      delivered = delivered_to_author = 0
      subscribers = GitHub.newsies.subscribers_with_preloaded_settings(
        delivery.list,
        delivery.thread,
        explicit_subscribers,
      ).value!
      author = (delivery.comment || delivery.thread).try(:notifications_author)

      add_subscriber_set_size_datadog_tag(subscribers.length)

      subscribers.each_slice(BATCH_SIZE).with_index do |batched_subscribers, slice_index|
        authorized_subscribers = authorize_subscribers(delivery, batched_subscribers)

        # In this loop, we'll write into the `notifications_entries` and
        # `notifications_deliveries`tables which belong to different domains,
        # so we need to throttle on both classes.
        NotificationEntry.throttle do
          NotificationDelivery.throttle do
            delivery_tracker = tracked_deliveries(delivery, *authorized_subscribers)

            authorized_subscribers.each do |subscriber|
              if deliver_email_to_author?(subscriber, author, explicit_subscribers)
                deliver_to_single_subscriber(
                  delivery,
                  subscriber,
                  delivery_tracker,
                  subscriber_wants_email_for_own_activity: true,
                  enqueued_at: enqueued_at,
                  event_time: event_time,
                )
                delivered += 1
                delivered_to_author += 1
              elsif deliver_regularly?(delivery, subscriber, author, explicit_subscribers)
                deliver_to_single_subscriber(
                  delivery,
                  subscriber,
                  delivery_tracker,
                  subscriber_is_directly_mentioned: direct_mention_user_ids.include?(subscriber.user_id),
                  enqueued_at: enqueued_at,
                  event_time: event_time,
                )
                delivered += 1
              end

              if enforce_interruption
                enforce_interruption!(delivery, delivery_tracker)
              end
            end

            finalize_deliveries(delivery, delivery_tracker)
          end
        end

        # Queue a job to index the notification to ElasticSearch for authorized subscribers
        queue_indexing_job(delivery.summary.id, authorized_subscribers)
      end

      GitHub.dogstats.count("newsies.delivery.delivered", delivered)
      GitHub.dogstats.count("newsies.delivery.delivered_to_author", delivered_to_author)
    end

    # Filters out subscribers who are not authorized to receive this notification delivery
    # Will trigger cleanup if the reason a user is unauthorized is in REASONS_TO_PURGE_NEWSIES_DATA
    #
    # Returns a list of subscribers who may receive the notification
    def authorize_subscribers(delivery, subscribers)
      user_ids_for_which_to_purge_data = []

      authorized_subscribers = subscribers.select do |subscriber|
        next false if subscriber.settings.nil?
        next false if subscriber.settings.user.suspended?

        if (reason = reason_to_stop_delivery?(delivery.list, delivery.thread, subscriber.user))
          if REASONS_TO_PURGE_NEWSIES_DATA.include?(reason)
            user_ids_for_which_to_purge_data << subscriber.user_id
          end
          next false
        end

        true
      end

      if user_ids_for_which_to_purge_data.present?
        GitHub.newsies.async_delete_all_for_list_and_users(delivery.list, user_ids_for_which_to_purge_data)
      end

      authorized_subscribers
    end

    # Returns true if all of the following are true...
    #   - the list of subscribers set explicitly _does not_ include the author
    #   - the given subscriber is also the author
    #   - the subscriber/author has subscribed to their own updates to be
    #   emailed
    #
    # Returns truthy or falsey
    def deliver_email_to_author?(subscriber, author, explicit_subscribers)
      explicit_subscribers ||= []

      author &&
        explicit_subscribers.map(&:user_id).exclude?(author.id) &&
        subscriber_is_author?(subscriber, author) &&
        subscribed_to_own_updates?(subscriber.settings)
    end

    def subscriber_is_author?(subscriber, author)
      subscriber.user && subscriber.user == author
    end

    def subscribed_to_own_updates?(settings)
      settings &&
        settings.participating_email? &&
        settings.notify_own_via_email?
    end

    # Do the notification list/thread still exist in the db?
    def list_and_thread_exist?(list, thread)
      unless list.class.exists?(list.id)
        GitHub.dogstats.increment("newsies.delivery.list_deleted")
        return false
      end

      # some threads (like Commit) are not ActiveRecord models
      return true unless thread.class.respond_to?(:exists?)

      unless thread.class.exists?(thread.id)
        GitHub.dogstats.increment("newsies.delivery.thread_deleted")
        return false
      end

      true
    end

    # Checks whether the given subscriber should be notified.
    #
    # Returns truthy or falsey
    def deliver_regularly?(delivery, subscriber, author, explicit_subscribers)
      return false unless subscribed_to_event?(subscriber, event_name_from_delivery(delivery))

      is_explicitly_set_user = explicit_subscribers && explicit_subscribers.include?(subscriber)
      is_author = subscriber_is_author?(subscriber, author)
      (
        (!is_author || is_explicitly_set_user) &&
        deliver_from_author?(delivery.list, author, subscriber.user)
      )
    end

    # Checks whether the subscriber is subscribed to the specific event we are notifying
    #           about.
    def subscribed_to_event?(subscriber, event_name)
      # if subscriber is not subscribed to specific events, then they are subscribed by default
      return true if subscriber.events.empty?

      # if event name is undefined, then it definitely doesn't match the user's preferences
      return false if event_name.nil?

      # the subscriber is only subscribed if their defined events match the event_name
      subscriber.events.include?(event_name)
    end

    # Determine the event name from the delivery object
    #
    # Currently this only considers IssueEvent events
    def event_name_from_delivery(delivery)
      case delivery.comment
      when IssueEventNotification
        delivery.comment.issue_event.event
      end
    end

    def deliver_to_single_subscriber(delivery, subscriber, tracked, event_time:, subscriber_wants_email_for_own_activity: false, subscriber_is_directly_mentioned: false, enqueued_at: nil)
      options = DeliveryOptions.new(
        delivery: delivery,
        subscriber: subscriber,
        subscriber_wants_email_for_own_activity: subscriber_wants_email_for_own_activity,
        subscriber_is_directly_mentioned: subscriber_is_directly_mentioned,
        deliver_mobile_push_notifications: deliver_mobile_push_notifications?(subscriber.user),
      )

      pending_handlers = options.handlers - tracked.handlers(subscriber.user_id)

      record_ghost_web_notification(options, event_time)

      pending_handlers.each do |handler_key|
        handler_key = handler_key.to_s
        handler = GitHub.newsies.handlers.detect { |h| h.handler_key.to_s == handler_key }

        next unless handler.present?

        GitHub.dogstats.time("newsies.deliver.time", tags: ["handler:#{handler_key}", "thread_type:#{delivery.thread_type}"]) do

          # Some comments (like IssueEventNotification) need to know who they are being delivered to
          # as delivery is happening.
          if delivery.comment.respond_to?(:register_recipient)
            delivery.comment.register_recipient subscriber.settings.id
          end

          delivered = handler.deliver(
            delivery,
            subscriber.settings,
            event_time: event_time,
            reason: options.reason,
            root_job_enqueued_at: enqueued_at,
          )

          next if handler_key == Newsies::HANDLER_MOBILE_PUSH

          tracked.set_handler(subscriber.user_id, handler_key, subscriber.reason)
          if delivered && enqueued_at
            record_time_to_sent(handler_key, subscriber.reason, enqueued_at)
          end
        end
      end
    end

    # Record a metric to log subscribers that don't currently have web notifications
    # enabled. Additionally, if notifications_perform_ghost_writes is enabled this will
    # actually write a notification entry that's hidden from the user, so we can experience
    # the full load.
    #
    # This will allow us to estimate the impact if web notifications were
    # enabled for everybody, or if we started to write web notifications for everybody.
    #
    # options - Newsies::DeliveryOptions
    # event_time - Time
    def record_ghost_web_notification(options, event_time)
      return if options.subscriber_wants_email_for_own_activity
      return if options.handlers.include?("web")

      # Estimate whether this would be an inserted notification or an update
      # If the comment type is the same as the thread type, it's probably a new thread
      is_new_thread = options.delivery.thread_type == options.delivery.comment_type
      perform_write = GitHub.flipper[:notifications_perform_ghost_writes].enabled?

      if perform_write
        # Actually write a notification entry for this user, but with a negative
        # user id. This allows us to feel the load without making it visible to the user
        Newsies::NotificationEntry.insert(
          -1 * options.subscriber.user_id,
          options.delivery.summary,
          reason: options.subscriber.reason,
          event_time: event_time,
        )
      end

      GitHub.dogstats.increment("newsies.web_disabled_for_subscriber", tags: [
        "reason:#{options.subscriber.reason}",
        "email_enabled:#{options.handlers.include?("email")}",
        "is_new_thread:#{is_new_thread}",
        "ghost_notification_written:#{perform_write}",
      ])
    end

    # Checks to see if there is a reason the user should not get a delivery.
    #
    # list   - A Repository or a Team.
    # thread - The object generating the notification (Issue, Commit, DiscussionPost, etc.)
    # user   - The User due to receive a notification.
    #
    # Returns a Symbol (which is truthy) if the delivery should be stopped, false otherwise.
    def reason_to_stop_delivery?(list, thread, user)
      return :newsies_disabled unless user&.newsies_enabled?
      return :list_unreadable unless list_readable?(list, thread, user)
      return :thread_unreadable if thread.respond_to?(:readable_by?) && !thread.readable_by?(user)
      false
    end

    def list_readable?(list, thread, user)
      # This is a hack that overrides User#readable_by? which only returns
      # true if the list and the user are the same object. The user of a Gist is
      # treated as its list. From a notifications perspective, a user should
      # be able to read any other user.
      return true if thread.is_a?(Gist) && list.is_a?(User)
      list.readable_by?(user)
    end

    # Checks to see if the user can receive a notification by the author or
    # repository owner.
    #
    # list   - A Repository.
    # author - The User that created the notification.
    # user   - The User due to receive a notification.
    #
    # Returns true if the user can receive the notification, or false.
    def deliver_from_author?(list, author, user)
      return if !user || !user.newsies_enabled?
      users = [list.owner, author].compact
      ActiveRecord::Base.connected_to(role: :reading) do
        !user.avoid?(*users)
      end
    end

    def tracked_deliveries(delivery, *subscribers)
      store = NotificationDeliveryStore.new(delivery.list, delivery.thread, delivery.comment)
      user_ids = subscribers.map { |sub| sub.user_id }
      TrackedDeliveries.new(store, *user_ids)
    end

    # Mapping of (inclusive) upper bound on time_to_sent_ms to a bucket name
    TIME_TO_SENT_BUCKETS_MS = [
      # upper bound ms, tag name
      [1.second.in_milliseconds,   "bucket:0s-1s"],
      [5.seconds.in_milliseconds,  "bucket:1s-5s"],
      [30.seconds.in_milliseconds, "bucket:5s-30s"],
      [1.minute.in_milliseconds,   "bucket:30s-1m"],
      [3.minutes.in_milliseconds,  "bucket:1m-3m"],
      [5.minutes.in_milliseconds,  "bucket:3m-5m"],
      [10.minutes.in_milliseconds, "bucket:5m-10m"],
      [Float::INFINITY,            "bucket:10m-plus"],
    ]

    def record_time_to_sent(handler_key, reason, enqueued_at)
      time_to_sent_ms = (Time.now.utc.to_f - enqueued_at) * 1000.0
      GitHub.dogstats.timing("newsies.delivery.time_to_sent", time_to_sent_ms, tags: [
        "handler:#{handler_key}",
        "reason:#{reason}",
        "participating:#{Newsies::Reasons::Participating.include?(reason.to_sym)}",
        "priority:#{priority}",
        slo_bucket_tag(TIME_TO_SENT_BUCKETS_MS, time_to_sent_ms),
        *@datadog_tags,
      ])
    end

    # Mapping of (inclusive) upper bound of the number of subscribers to a tag name
    SUBSCRIBER_SET_SIZE_BUCKETS = [
      [10,              "subscriber_set_size:small"],
      [100,             "subscriber_set_size:medium"],
      [1000,            "subscriber_set_size:large"],
      [Float::INFINITY, "subscriber_set_size:x-large"],
    ]

    # Find a tag for the metric with a bucketed number of subscribers. We cannot create a tag
    # for every unique number of subscribers, so this buckets the values so we can estimate
    # distributions.
    def add_subscriber_set_size_datadog_tag(subscriber_set_size)
      @datadog_tags.push(
        SUBSCRIBER_SET_SIZE_BUCKETS
          .find { |(upper_bound_ms, _)| subscriber_set_size <= upper_bound_ms }
          .second,
      )
    end

    # Raises an exception to cause this job to be retried if the Resque worker
    # running this job has been asked to shutdown.
    def enforce_interruption!(delivery, delivery_tracker)
      return unless $resque_worker&.shutdown?

      finalize_deliveries(delivery, delivery_tracker)

      GitHub.dogstats.increment("newsies.delivery.interrupted", tags: @datadog_tags)
      raise JobInterrupted
    end

    # Performs final cleanup in preparation for the delivery job being terminated.
    def finalize_deliveries(delivery, delivery_tracker)
      delivery_tracker.save

      # If the list and thread still exist, we check is the thread was marked as spam and handle accordingly,
      # otherwise it was deleted during delivery so cleanup notifications
      if list_and_thread_exist?(delivery.list, delivery.thread)
        handle_spam(delivery)
      else
        queue_delivery_cleanup(delivery.list, delivery.thread)
      end
    end

    def queue_indexing_job(summary_id, subscribers)
      user_ids = subscribers.map do |subscriber|
        if GitHub.flipper[:notifications_search].enabled?(subscriber.user) || GitHub.flipper[:notifications_index_individual_recipient].enabled?(subscriber.user)
          subscriber.user_id
        end
      end.compact

      # Only queue the job if there is notifications to index
      return unless user_ids.present?

      IndexNotificationForRecipientsJob.perform_later(
        summary_id,
        queued_at: Timestamp.from_time(Time.now),
        user_ids: user_ids
      )
    end

    # If the notification list/thread were deleted during delivery, we need to
    # go back and clean everything up as the notifications shouldn't exist
    #
    # This can happen if there's a race condition between notifications being
    # delivered for a thread and the thread/list being deleted
    def queue_delivery_cleanup(list, thread)
      GitHub.newsies.async_delete_all_for_thread(list, thread)
      GitHub.dogstats.increment("newsies.cleanup.delivery")
    end

    # If the notification thread was marked as spammy during delivery, we need to
    # go back and mark the notifications as spammy
    #
    # This can happen if there's a race condition between notifications being
    # delivered for a thread and the thread being marked as spammy
    def handle_spam(delivery)
      thread = delivery.thread
      thread.try(:reload)

      if thread.try(:spammy?)
        UpdateNotificationsSpamStatusJob.perform_later(
          delivery.list_type,
          delivery.list_id,
          delivery.thread_type,
          delivery.thread_id,
        )
      end
    end

    def deliver_mobile_push_notifications?(user)
      return false unless deliver_mobile_push_notifications_enabled?
      Newsies::MobilePushNotificationSchedule.deliver_to_user?(user)
    end

    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def deliver_mobile_push_notifications_enabled?
      @deliver_mobile_push_notifications_enabled ||= GitHub.flipper[:deliver_mobile_push_notifications].enabled?
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator
  end
end
