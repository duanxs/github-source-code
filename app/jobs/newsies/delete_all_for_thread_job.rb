# frozen_string_literal: true

module Newsies
  # Delete all subscriptions and notifications for a thread
  class DeleteAllForThreadJob < MaintenanceBaseJob

    # Retry a few times if we are checking for the non-existence of a thread, but it exists
    # in case there's a race condition with it's deletion
    class ThreadStillExistsError < StandardError; end
    retry_on ThreadStillExistsError, wait: 3.seconds, attempts: 5

    # list_type   - String newsies list type
    # list_id     - Integer newsies list id
    # thread_type - String newsies thread type
    # thread_id   - Integer newsies thread id
    # options
    #   ensure_thread_deleted: Boolean, default false, should the job check that the thread doesn't exist before doing cleanup?
    #                          This is optional as sometimes when we queue the job we know for sure the thread will
    #                          be deleted, but it may not have happened yet.
    def perform(list_type, list_id, thread_type, thread_id, ensure_thread_deleted: false)
      raise_if_thread_exists?(list_type, list_id, thread_type, thread_id) if ensure_thread_deleted

      list = List.new(list_type, list_id)
      thread = Thread.new(thread_type, thread_id, list: list)
      scopes_by_klass = {
        NotificationEntry => NotificationEntry.for_thread(thread),
        SavedNotificationEntry => SavedNotificationEntry.for_thread(thread),
      }

      scopes_by_klass.each do |klass, scope|
        scope.in_batches(of: BATCH_SIZE) do |batched_scope|
          klass.throttle { batched_scope.delete_all }
        end
      end

      ThreadSubscription.for_thread(thread).in_batches(of: BATCH_SIZE) do |batched_scope|
        delete_thread_subscription_events(batched_scope.pluck(:id))
        ThreadSubscription.throttle { batched_scope.delete_all }
      end

      RollupSummary.throttle {
        RollupSummary.by_thread(list, thread)&.delete
      }

      if GitHub.flipper[:notifications_queue_delete_from_search_indexes_job].enabled?
        Newsies::DeleteFromSearchIndexesJob.perform_later(
          list: { type: list_type, id: list_id },
          thread: { type: thread_type, id: thread_id },
        )
      end
    end

    private

    def raise_if_thread_exists?(list_type, list_id, thread_type, thread_id)
      if thread_type.constantize.find_by_id(thread_id).present?
        raise ThreadStillExistsError, <<~MSG
          Expected thread object to not exist for
            <Newsies::List type=#{list_type.inspect} id=#{list_id.inspect}>
            <Newsies::Thread type=#{thread_type.inspect} id=#{thread_id.inspect}>
          but it exists.
        MSG
      end
    end
  end
end
