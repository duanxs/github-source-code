# frozen_string_literal: true

module Newsies
  class DeliverMobilePushNotificationsJob < ApplicationJob
    include SloHelper

    areas_of_responsibility :notifications
    queue_as :deliver_mobile_push_notifications

    attr_reader :comment, :user_id, :user, :retries, :token_ids, :root_job_enqueued_at

    class ExternalServiceFailure < StandardError; end
    class DeviceDeliveryFailure < StandardError; end

    MAX_RETRIES = 6

    # We wait a random amount of time so that we don't force jobs
    # to wait longer than they have to if more than one job is attempting to
    # grab the lock at the same time.
    retry_on GitHub::Restraint::UnableToLock, wait: -> (executions) { rand(4..10).seconds }, attempts: 10

    # e.g. "Wed, 21 Oct 2015 07:28:00 GMT"
    # as per https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Date#Examples
    DATE_HEADER_FORMAT = "%a, %d %b %Y %H:%M:%S GMT"

    # Map from (inclusive) upper bound of a timing measurement to Datadog tag for SLO bucket.
    SLO_BUCKETS_MS = [
      [1.minute.in_milliseconds,   "bucket:0s-1m"],
      [3.minutes.in_milliseconds,  "bucket:1m-3m"],
      [5.minutes.in_milliseconds,  "bucket:3m-5m"],
      [10.minutes.in_milliseconds, "bucket:5m-10m"],
      [15.minutes.in_milliseconds, "bucket:10m-15m"],
      [Float::INFINITY,            "bucket:15m-plus"],
    ]

    discard_on ActiveJob::DeserializationError

    around_perform do |job, block|
      GitHub::Restraint.new.lock!("mobile-push-comment-#{Newsies::Comment.to_key(job.arguments.first)}", 1, 20.seconds) do
        block.call
      end
    end

    # Attempts to deliver mobile push notifications to the given list of users via Google FCM.
    #
    # comment - PushNotifiable
    # user_id - Integer User ID
    # retries - Integer number of times that this delivery has already been retried.
    # token_ids - Array<Integer> for MobileDeviceTokens that we should filter down to (in addition
    #   to the user filter).
    # root_job_enqueued_at - Integer or Float epoch timestamp for when the first notification
    #   delivery job for the content in question was enqueued. Note that since this job may be a
    #   dependent of other delivery jobs, this might be the time at which an ancestor job was
    #   enqueued.
    #
    # Returns nothing.
    def perform(comment, user_id, retries: 0, token_ids: [], root_job_enqueued_at: nil)
      @comment = comment
      @user_id = user_id
      @user = User.find_by_id(user_id)
      @retries = retries
      @token_ids = token_ids
      @root_job_enqueued_at = root_job_enqueued_at

      unless GitHub.google_fcm_url.present? && GitHub.google_fcm_server_key.present?
        Failbot.report(ArgumentError.new("Missing Google FCM credentials"))
        return
      end

      unless comment.is_a?(PushNotifiable)
        GitHub.dogstats.increment(
          stat_key("missing_interface"),
          tags: ["comment_type:#{comment.class.name}"],
        )
        return
      end

      push_notification = PushNotification.new(
        title: comment.push_notification_title,
        subtitle: comment.push_notification_subtitle,
        body: comment.push_notification_body,
        subject_id: comment.push_notification_subject_id,
        url: comment.push_notification_url,
        type: MobilePushNotificationDelivery::DEFAULT_DELIVERY_REASON
      )

      unless push_notification.valid?
        raise ActiveRecord::RecordInvalid.new
      end

      return if deliverable_tokens.empty?

      client_request(push_notification)

      nil
    end

    private

    # Retrieves the device tokens that we should use to deliver the push notification
    # to the given list of users.
    #
    # Returns Array<MobileDeviceToken> for all the tokens that we should deliver to.
    def deliverable_tokens
      return @deliverable_tokens if defined?(@deliverable_tokens)

      scope = MobileDeviceToken.where(user_id: user_id, service: :fcm).order(id: :asc)
      scope = scope.where(id: token_ids) if token_ids.any?
      tokens = scope.all

      if tokens.empty?
        @deliverable_tokens = []
        return @deliverable_tokens
      end

      newsies_comment = Newsies::Comment.to_object(comment)
      already_delivered_token_ids = MobilePushNotificationDelivery
        .where(
          comment_type: newsies_comment.type,
          comment_id: newsies_comment.id,
          mobile_device_token_id: tokens.map(&:id),
          state: [:delivered, :failed],
        )
        .pluck(:mobile_device_token_id)

      @deliverable_tokens = tokens.reject { |t| already_delivered_token_ids.include?(t.id) }
    end

    # Makes a request using the v1 firebase messaging API
    # https://firebase.google.com/docs/cloud-messaging/send-message
    #
    # Processes detailed response messages from Google FCM.
    #
    # For each token there are four possible outcomes:
    #   1. We successfully delivered to the token, so we do nothing further.
    #   2. The token is invalid, so we delete it.
    #   3. We failed to deliver to the token for some transient reason, so we retry later.
    #   4. We failed to deliver to the token for some permanent reason, so we report the error.
    #
    # push_notification - PushNotification object
    #
    # Returns nothing.
    def client_request(push_notification)
      tokens_hash = deliverable_tokens.index_by(&:device_token)
      results = fcm_client.send_all(
        message: push_notification.google_fcm_v1_payload,
        device_tokens: tokens_hash.keys
      )

      deliveries = results.map do |result|
        token = tokens_hash[result[:token]]
        MobilePushNotificationDelivery.from(comment, token, fcm_response: result)
      end

      failed_deliveries = deliveries.select(&:failed?)
      retryable_deliveries = deliveries.select(&:retrying?)

      handle_failed_deliveries(failed_deliveries)

      if retryable_deliveries.any? && retries < MAX_RETRIES
        retry_later(
          token_ids_to_retry: retryable_deliveries.map(&:mobile_device_token_id),
          ttl: push_notification.ttl,
          # The retryable header information should be the same in each result object
          retry_after_header: retryable_deliveries.first.fcm_response[:retry_after_header],
        )
      else
        retryable_deliveries.each { |d| d.state = :failed }
      end

      record_all_deliveries(deliveries)

      nil
    end

    def retry_later(token_ids_to_retry:, ttl:, retry_after_header:)
      wait = wait_in_minutes(ttl, retry_after_header)
      return if wait.nil?

      self.class.set(wait: wait).perform_later(
        comment,
        user_id,
        retries: retries + 1,
        token_ids: token_ids_to_retry,
        root_job_enqueued_at: root_job_enqueued_at,
      )
    end

    # time_to_live - Integer number of seconds for how long (from the initial notification event)
    #   we should attempt to send this notification.
    # retry_after_header - String value of the "Retry-After" header from the HTTP response from
    #   Google FCM, or nil if it was not specified
    #
    # Returns either ActiveSupport::Duration representing time in minutes to wait before retrying
    # the request, or nil if the request should not be retried.
    def wait_in_minutes(time_to_live, retry_after_header)
      # Attempt to parse either a date or a number of seconds from the Retry-After header as per
      # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Retry-After#Syntax
      retry_after_in_seconds = if retry_after_header
        begin
          difference_in_days = (
            DateTime.strptime(retry_after_header, DATE_HEADER_FORMAT) - DateTime.now
          )
          seconds_in_a_day = 1.day.to_i
          difference_in_days * seconds_in_a_day
        rescue ArgumentError
          retry_after_header.to_i if retry_after_header.match(/\d+/)
        end
      end

      if retry_after_in_seconds && retry_after_in_seconds > time_to_live
        nil
      elsif retry_after_in_seconds
        (retry_after_in_seconds / 60).ceil.minutes
      else
        determine_delay(
          seconds_or_duration_or_algorithm: :exponentially_longer,
          executions: retries,
        ).minutes
      end
    end

    def handle_failed_deliveries(failed_deliveries)
      deliveries_to_invalid_tokens, deliveries_to_valid_tokens = \
        failed_deliveries.partition(&:token_invalid?)

      begin
        destroyed_records = MobileDeviceToken.throttle do
          MobileDeviceToken
            .with_ids(deliveries_to_invalid_tokens.map(&:mobile_device_token_id))
            .destroy_all
        end
        GitHub.dogstats.count(stat_key("token.destroyed"), destroyed_records.length)
      rescue Freno::Throttler::Error
        # We failed to invalidate these tokens which is too bad, but we'll
        # invalidate them next time so don't sweat it.
      end

      deliveries_to_valid_tokens.each do |d|
        message = "MobilePushNotificationDelivery<mobile_device_token_id: #{d.mobile_device_token_id}> " +
                  "failed with #{d.fcm_response[:error]}"
        Failbot.report(DeviceDeliveryFailure.new(message))
      end
    end

    def record_all_deliveries(deliveries)
      begin
        MobilePushNotificationDelivery.throttle do
          MobilePushNotificationDelivery.upsert(deliveries)
        end
      rescue Freno::Throttler::Error
      end

      successful_deliveries = deliveries.select(&:delivered?)
      retried_deliveries = deliveries.select(&:retrying?)
      failed_deliveries = deliveries.select(&:failed?)

      if root_job_enqueued_at && successful_deliveries.any?
        time_since_enqueued_ms = (Time.now.utc.to_f - root_job_enqueued_at) * 1000
        successful_deliveries.each do |delivery|
          DeliveryLogger.log_to_splunk_and_hydro(
            delivery,
            user: user,
            reason: delivery.reason,
            handler_key: :mobile_push,
            root_job_enqueued_at: root_job_enqueued_at,
            state: :delivered,
          )

          GitHub.dogstats.timing(
            stat_key("token.response"),
            time_since_enqueued_ms,
            tags: [
              "state:delivered",
              slo_bucket_tag(SLO_BUCKETS_MS, time_since_enqueued_ms),
            ],
          )
        end
      end

      if retried_deliveries.any?
        GitHub.dogstats.count(
          stat_key("token.response"),
          retried_deliveries.length,
          tags: ["state:retrying"],
        )
      end

      if failed_deliveries.any?
        failed_deliveries.each do |delivery|
          # We don't want to log failed deliveries to hydro, so explictly log only to splunk.
          DeliveryLogger.log_to_splunk(
            delivery,
            user: user,
            reason: delivery.reason,
            handler_key: :mobile_push,
            root_job_enqueued_at: root_job_enqueued_at,
            state: :failed,
          )
        end

        GitHub.dogstats.count(
          stat_key("token.response"),
          failed_deliveries.length,
          tags: ["state:failed"],
        )
      end
    end

    def fcm_client
      @fcm_client ||= Firebase::CloudMessagingClient.new
    end

    def stat_key(suffix)
      "newsies.deliver_mobile_push_notifications_job.#{suffix}"
    end
  end
end
