# frozen_string_literal: true

module Newsies
  class IndexNotificationForRecipientsJob < ApplicationJob
    MAX_REPLICATION_DELAY_WAIT = 2.0

    areas_of_responsibility :notifications
    queue_as :notifications

    retry_on WaitForReplication::DataUnavailable

    # Attempts to index a notification to our ElasticSearch cluster for a given set of users
    #
    # summary_id - Integer RollupSummary ID
    # queued_at  - Integer or Float epoch timestamp for when the index job got queued.
    # user_ids   - Array<Integer> for Users that we should index the notification for.
    #
    # Return nothing.
    def perform(summary_id, user_ids:, queued_at:)
      return unless user_ids.present?
      wait_for_replication!(queued_at)

      rollup_summary = ActiveRecord::Base.connected_to(role: :reading) { RollupSummary.find_by_id(summary_id) }
      return unless rollup_summary.present?

      iterator = NotificationEntry.for_thread(rollup_summary.newsies_thread)
                               .for_users(user_ids)
                               .includes(:rollup_summary)
                               .in_batches(of: 100)

      adapter = Elastomer::Adapters::BulkNotifications.create(iterator)
      index = Elastomer::Indexes::Notifications.new
      index.store(adapter)
    end

    private

    def wait_for_replication!(queued_at)
      WaitForReplication.new(
        queued_at,
        store_name: Elastomer::Adapters::Notification.mysql_cluster,
        max_wait_seconds: MAX_REPLICATION_DELAY_WAIT,
      ).wait!
    end
  end
end
