# frozen_string_literal: true

module Newsies
  # Delete all subscriptions and notifications for a list
  class DeleteAllForListJob < MaintenanceBaseJob
    # list_type - String newsies list type
    # list_id   - Integer newsies list id
    def perform(list_type, list_id)
      list = List.new(list_type, list_id)
      scopes_by_klass = {
        ListSubscription => ListSubscription.for_list(list),
        ThreadTypeSubscription => ThreadTypeSubscription.for_list(list),
        NotificationEntry => NotificationEntry.for_list(list),
        SavedNotificationEntry => SavedNotificationEntry.for_list(list),
      }

      scopes_by_klass.each do |klass, scope|
        scope.in_batches(of: BATCH_SIZE) do |batched_scope|
          klass.throttle { batched_scope.delete_all }
        end
      end

      ThreadSubscription.for_list(list).in_batches(of: BATCH_SIZE) do |batched_scope|
        delete_thread_subscription_events(batched_scope.pluck(:id))
        ThreadSubscription.throttle { batched_scope.delete_all }
      end

      if GitHub.flipper[:notifications_queue_delete_from_search_indexes_job].enabled?
        Newsies::DeleteFromSearchIndexesJob.perform_later(list: { type: list.type, id: list.id })
      end
    end
  end
end
