# frozen_string_literal: true

module Newsies
  class KubernetesDeliverNotificationsJob < DeliverNotificationsJob
    QUEUE_NAME = "kubernetes_deliver_notifications"
  end
end
