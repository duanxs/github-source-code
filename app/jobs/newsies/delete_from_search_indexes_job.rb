# frozen_string_literal: true

module Newsies
  class DeleteFromSearchIndexesJob < ApplicationJob
    areas_of_responsibility :notifications
    queue_as :notifications

    # Public: deletes from search index via a delete by query call.
    #
    # args - Supported keywords and their expected data structure:
    #
    #        list:   { type: String, id: Int }
    #        thread: { type: String, id: Int }
    #
    def perform(**args)
      query = build_query_from_args(**args)

      index = Elastomer::Indexes::Notifications.new
      index.docs.delete_by_query(query, type: "notification")
    rescue Timeout::Error, Elastomer::Client::Error => boom
      Failbot.report boom
    end

    private

    # Private: Builds the ElasticSearch query based on the provided keyword arguments
    #
    # Returns Hash
    # {
    #   query: {
    #     bool: {
    #       must [
    #         { term: { list_type: "Repository" }},
    #         { term: { list_id: 1 }}
    #         ...
    #       ]
    #     }
    #   }
    # }
    #
    def build_query_from_args(list: nil, thread: nil)
      raise ArgumentError, "list filter must be set when using thread filter" if thread.present? && !list.present?

      filters = []
      filters << list_filter(**list) if list.present?
      filters << thread_filter(**thread) if thread.present?

      filters = filters.flatten.compact

      raise ArgumentError, "at least one valid filter must be set" unless filters.present?

      {
        query: {
          bool: {
            must: filters,
          },
        },
      }
    end

    def list_filter(type:, id:)
      raise ArgumentError, "list filter type and id must have a value" unless type.present? && id.present?

      [
        { term: { list_type: type } },
        { term: { list_id: id } },
      ]
    end

    def thread_filter(type:, id:)
      raise ArgumentError, "thread filter type and id must have a value" unless type.present? && id.present?

      [
        { term: { thread_type: type } },
        { term: { thread_id: id } },
      ]
    end
  end
end
