# frozen_string_literal: true

class DocusignWebhookJob < ApplicationJob
  queue_as :docusign

  discard_on(StandardError) do |job, error|
    GitHub.dogstats.increment(
      "docusign.webhook_error",
      tags: ["exception:#{error.class.name.parameterize}"],
    )
    Failbot.report(error)
  end

  # Public: Perform the job
  #
  # docusign_webhook - The Docusign::Webhook model to handle
  #
  # Returns a Boolean
  def perform(docusign_webhook)
    return if docusign_webhook.processed?

    envelope_status = docusign_webhook.envelope_status
    envelope_id = docusign_webhook.envelope_id

    return unless docusign_envelope = DocusignEnvelope.find_by(
      envelope_id: envelope_id,
    )

    docusign_envelope.update!(status: envelope_status)
    docusign_webhook.update!(processed_at: Time.now)
  end
end
