# frozen_string_literal: true

SpokesRecomputeGistChecksumsJob = LegacyApplicationJob.wrap(GitHub::Jobs::DgitRecomputeGistChecksums)
