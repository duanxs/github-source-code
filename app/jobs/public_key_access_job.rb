# frozen_string_literal: true

PublicKeyAccessJob = LegacyApplicationJob.wrap(GitHub::Jobs::PublicKeyAccess)
