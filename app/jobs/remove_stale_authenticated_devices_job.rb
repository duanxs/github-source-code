# frozen_string_literal: true

class RemoveStaleAuthenticatedDevicesJob < ApplicationJob

  queue_as :remove_stale_authenticated_devices
  locked_by key: ApplicationJob::DEFAULT_LOCK_PROC, timeout: 1.day
  schedule interval: 5.minutes, condition: -> { GitHub.sign_in_analysis_enabled? }

  REMOVE_AFTER_DURATION = 12.months
  BATCH_SIZE = 100

  REMOVAL_CONDITIONS = <<-SQL
    accessed_at < :after
  SQL

  def perform
    devices = AuthenticatedDevice.where(REMOVAL_CONDITIONS, {
      after: REMOVE_AFTER_DURATION.ago
    }).limit(BATCH_SIZE)

    loop do
      deleted_count = AuthenticatedDevice.throttle_with_retry(max_retry_count: 8) do
        devices.delete_all
      end
      GitHub.dogstats.count("authenticated_devices", deleted_count, tags: ["action:destroy", "reason:stale"])

      break if deleted_count < BATCH_SIZE
    end
  end
end
