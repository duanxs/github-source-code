# frozen_string_literal: true

SpokesCreateNetworkReplicaJob = LegacyApplicationJob.wrap(GitHub::Jobs::DgitCreateNetworkReplica)
