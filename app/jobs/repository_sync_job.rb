# frozen_string_literal: true

RepositorySyncJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepositorySync)
