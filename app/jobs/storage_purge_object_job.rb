# frozen_string_literal: true

StoragePurgeObjectJob = LegacyApplicationJob.wrap(GitHub::Jobs::StoragePurgeObject)
