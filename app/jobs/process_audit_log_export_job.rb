# frozen_string_literal: true

ProcessAuditLogExportJob = LegacyApplicationJob.wrap(GitHub::Jobs::ProcessAuditLogExport)
