# frozen_string_literal: true

# Job to scan for found tokens to display in the Token Scanning UI
class PrivateTokenScanningJob < TokenScanningJob
  include GitHub::TokenScanning::TokenScanningPostProcessingHelper

  # Pairs of tokens that must be found in the same file path to be
  # reported. This reduces the false positives for loose patterns.
  @@SAME_FILE_TOKEN_PAIRS = Set.new([
    Set.new(["ALICLOUD_ACCESS_KEY", "ALICLOUD_SECRET_KEY"]),
    Set.new(["ALICLOUD_ACCESS_KEY_LEGACY", "ALICLOUD_SECRET_KEY"]),
    Set.new(["AWS_SECRET", "AWS_KEYID"]),
    Set.new(["DATADOG_TOKEN", "DATADOG_NAME_PRESENCE"]),
    Set.new(["HUBSPOT_HAPIKEY", "HUBSPOT_HAPIKEY_NAME_PRESENCE"]),
    Set.new(["MESSAGEBIRD_TOKEN", "MESSAGEBIRD_NAME_PRESENCE"]),
    Set.new(["MESSAGEBIRD_TOKEN_2", "MESSAGEBIRD_NAME_PRESENCE"]),
    Set.new(["PLIVO_AUTH_ID", "PLIVO_AUTH_KEY"]),
  ]).freeze

  @@SAME_FILE_TOKENS = @@SAME_FILE_TOKEN_PAIRS.flatten.freeze

  def self.same_file_token_pairs
    @@SAME_FILE_TOKEN_PAIRS
  end

  def self.same_file_tokens
    @@SAME_FILE_TOKENS
  end

  CRC_CHECK_TOKENS = "MICROSOFT_VSTS_PAT"

  IGNORED_TOKEN_TYPES = Set.new([
    "DATADOG_NAME_PRESENCE", "HUBSPOT_HAPIKEY_NAME_PRESENCE", "MESSAGEBIRD_NAME_PRESENCE"
  ]).freeze

  NOISY_TOKEN_TYPES = Set.new([
    "DATADOG_TOKEN",
    "MICROSOFT_INTERNAL_ACTIVE_DOMAIN_USER_CREDENTIAL",
    "MICROSOFT_CREDENTIAL_PASSWORD",
    "GENERIC_OAUTH_CLIENT_ID_OR_SECRET",
    "MESSAGEBIRD_TOKEN", # Adding token to list to ignore it for private scans until we can evaluate how noisy it is against public repos
    "MESSAGEBIRD_TOKEN_2", # Adding token to list to ignore it for private scans until we can evaluate how noisy it is against public repos
    "MICROSOFT_AZURE_DEPLOYMENT_PASSWORD", # Adding token to list to ignore it for private scans until we can evaluate how noisy it is against public repos
    "MICROSOFT_AZURE_FUNCTION_KEY", # Adding token to list to ignore it for private scans until we can evaluate how noisy it is against public repos
  ]).freeze

  NOISY_FILE_TYPES = Set.new([
    "package.json", "yarn.lock", "composer.lock", "Gopkg.lock"
  ]).freeze

  BATCH_SIZE = 100

  around_perform :track_status

  def max_token_scan_results
    GitHub.configuration_secret_scanning_max_candidate_matches
  end

  # Extensibility point for jobs to control enablement of scan, which can vary by job implementation (e.g. based on visibility of repo)
  def token_scanning_enabled?
    git_repo.token_scanning_enabled? && !git_repo.token_scanning_service_enabled?
  end

  def scan_for_tokens
    # The repack/token-scan can take a long time...
    ::GitRPC.with_timeout(15.minutes) do
      add_blob_locations(token_infos)
    end

    create_results
    TokenPathDiscoveryJob.enqueue_once_per_interval([self.arguments.first], interval: 600) # collect every instance of this job and run at the end of 10 minutes

    "processed"
  end

  def filter_same_file_token_pairs(candidate_tokens)
    pair_candidates, tokens_to_report = candidate_tokens.partition do |token|
      @@SAME_FILE_TOKENS.include?(token.type)
    end

    pair_candidates.group_by(&:path).each_value do |tokens_at_path|
      token_types_at_path = tokens_at_path.map(&:type).to_set
      tokens_at_path.each do |token|
        pair_matches = @@SAME_FILE_TOKEN_PAIRS.any? do |pair|
          pair.include?(token.type) && pair.subset?(token_types_at_path)
        end
        tokens_to_report << token if pair_matches
      end
    end

    tokens_to_report
  end

  def filter_ignored_token_types(candidate_tokens)
    candidate_tokens.reject { |token| IGNORED_TOKEN_TYPES.include?(token.type) }
  end

  # Filtering out tokens and files known to be or contain noisy results
  # We will introduce these tokens and files back when we deem them to be noise proof
  def filter_noisy_tokens_and_files(candidate_tokens)
    tokens_to_report = []

    candidate_tokens.each do |token|
      file_name = File.basename(token.path)
      is_noisy_token = NOISY_TOKEN_TYPES.include?(token.type)
      is_noisy_file = NOISY_FILE_TYPES.include?(file_name)

      if is_noisy_token || is_noisy_file
        GitHub.dogstats.increment("token_scan.filtered_noisy_results", tags: [
          "token_type:#{token.type}",
          "file_name:#{is_noisy_file ? file_name : "" }",
          "noisy_token:#{is_noisy_token}",
          "noisy_file:#{is_noisy_file}",
        ])
      else
        tokens_to_report << token
      end
    end

    tokens_to_report
  end

  def filter_invalid_github_tokens(candidate_tokens)
    github_candidates, tokens_to_report = candidate_tokens.partition do |token|
      "GITHUB" == token.type
    end

    reached_max_limits = candidate_tokens.count >= max_token_scan_results
    github_candidates.each do |token|
      file_name = File.basename(token.path)
      is_noisy_file = NOISY_FILE_TYPES.include?(file_name)
      GitHub.dogstats.increment("token_scan.noisy.github_tokens", tags: [
        "file_name:#{is_noisy_file ? file_name : "" }",
        "scan_scope:#{job_scope}",
        "visibility:#{job_visibility}",
        "max_limit:#{reached_max_limits}"
      ])
    end

    hashed_tokens = github_candidates.each_with_object({}) do |token, hash|
      hash[token.token] = OauthAccess.hash_token(token.token)
    end

    # we log how many github tokens are we actually looking up (the dictionary will ensure there is one entry per token across blobs)
    GitHub.dogstats.count(
      "token_scan.noisy.github_tokens.dedup",
      hashed_tokens.size,
      tags: [
        "scan_scope:#{job_scope}",
        "visibility:#{job_visibility}",
        "max_limit:#{reached_max_limits}"
    ])

    valid_tokens = OauthAccess.where(hashed_token: hashed_tokens.values)
                              .pluck(:hashed_token)
                              .to_set

    github_candidates.each do |token|
      tokens_to_report << token if valid_tokens.include?(hashed_tokens[token.token])
    end

    tokens_to_report
  end

  def filter_invalid_github_ssh_private_keys(candidate_tokens)
    github_candidates, tokens_to_report = candidate_tokens.partition do |token|
      "ARMORED_PEM_PRIVATE_KEY" == token.type
    end

    fingerprints = ssh_key_fingerprints(github_candidates.map(&:token))

    keys = PublicKey
      .where(fingerprint: fingerprints.values.compact)
      .pluck(:fingerprint)
      .to_set

    github_candidates.each do |token|
      tokens_to_report << token if keys.include?(fingerprints[token.token])
    end

    tokens_to_report
  end

  def filter_invalid_vsts_pat_crc_tokens(candidate_tokens)
    crc_check_candidates, tokens_to_report = candidate_tokens.partition do |token|
      CRC_CHECK_TOKENS.eql?(token.type)
    end

    crc_check_candidates.each do |token|
      tokens_to_report << token if valid_vsts_pat_crc?(token.token)
    end

    tokens_to_report
  end

  def tokens_to_create
    tokens = found_tokens
    tokens = filter_same_file_token_pairs(tokens)
    tokens = filter_ignored_token_types(tokens)
    tokens = filter_invalid_github_tokens(tokens)
    tokens = filter_invalid_github_ssh_private_keys(tokens)
    tokens = filter_noisy_tokens_and_files(tokens)
    tokens = filter_invalid_vsts_pat_crc_tokens(tokens)
    tokens
  end

  def create_results
    token_groups = tokens_to_create.group_by do |found_token|
      [found_token.type, found_token.token_hash]
    end

    if dry_run?
      log_dry_run_stats(token_groups)

      # If the scan is a dry run, do not update the db or notify subscribers.
      return
    end

    results_to_notify = []

    token_groups.each_slice(BATCH_SIZE) do |tokens_slice|
      TokenScanResult.throttle do
        tokens_slice.each do |(token_type, token_signature), token_locations|
          result = TokenScanResult.create_from_found_token!(git_repo, token_type, token_signature)
          create_locations(result, token_locations)
          results_to_notify << result if result.notify_subscribers?
        end
      end
    end

    notify_subscribers(results_to_notify)
  end

  def create_locations(result, locations)
    locations.each_slice(BATCH_SIZE) do |locations_slice|
      TokenScanResultLocation.throttle do
        locations_slice.each do |found_token|
          TokenScanResultLocation.create_from_found_token!(result, found_token)
        end
      end
    end
  end

  def notify_subscribers(results)
    return if results.empty?

    scope = is_commit_scoped? ? AccountMailer::TOKEN_SCANNING_SCOPE[:commit_scoped] : AccountMailer::TOKEN_SCANNING_SCOPE[:repo_scoped]
    users = git_repo.token_scanning_users_to_notify
    return if users.empty?

    AccountMailer.token_scanning_summary(git_repo, results, users, scope).deliver_now
  end

  def found_tokens
    @found_tokens ||= token_infos.map { |token_info| GitHub::TokenScanning::FoundToken.new(**token_info) }
  end

  def log_dry_run_stats(token_groups)
    token_groups.each do |(token_type, _), _|
      GitHub.dogstats.increment("secret_scan.dry_run", tags: [
        "scan_scope:#{job_scope}",
        "visibility:#{job_visibility}",
        "token_type:#{token_type}"
      ])

      GitHub::Logger.log message: "Dry run of secret scan on repo #{git_repo.id} completed",
        scan_scope: job_scope,
        visibility: job_visibility,
        token_type: token_type
    end
  end

  def token_infos
    raise "implement me"
  end

  def track_status
    result = yield

    if result == "processed"
      GitHub.kv.set("token_scan_status_#{git_repo.id}", "repository_scanned")
    end

    result
  rescue GitRPC::Timeout, GitRPC::CommandFailed
    GitHub.kv.set("token_scan_status_#{git_repo.id}", "scanning_error")
    raise
  end

  def is_commit_scoped?
    raise "implement me"
  end
end
