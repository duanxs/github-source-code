# frozen_string_literal: true

class ContributionsInstrumentPushJob < ApplicationJob
  areas_of_responsibility :user_profile
  queue_as :contributions_instrument_push

  locked_by timeout: 1.hour, key: DEFAULT_LOCK_PROC

  def perform(push)
    Failbot.push(
      repo: push.repository&.readonly_name_with_owner,
      before: push.before,
      after: push.after,
    )

    Eventer::Push.instrument_push!(push)
  end
end
