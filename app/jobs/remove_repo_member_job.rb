# frozen_string_literal: true

class RemoveRepoMemberJob < ApplicationJob
  queue_as :remove_repo_member

  def perform(member, remover:, repo:)
    repo.remove_member(member, remover)
  end
end
