# frozen_string_literal: true

class ZuoraDeleteCardsFromAccountJob < ApplicationJob
  areas_of_responsibility :gitcoin

  queue_as :zuora

  attr_reader :zuora_account_id

  def perform(zuora_account_id)
    @zuora_account_id = zuora_account_id
    Failbot.push(account_id: zuora_account_id)

    set_default_payment_method_to_null

    Zuorest::Model::PaymentMethod.find_by_account_id(zuora_account_id).each do |payment_method|
      payment_method.delete
    end

  rescue ActiveRecord::ActiveRecordError, NoMethodError, Zuorest::HttpError => e
    Failbot.report!(e)
    raise
  end

  private

  def set_default_payment_method_to_null
    # This is from:
    # https://community.zuora.com/t5/API/How-to-set-a-field-to-NULL-via-REST-API/m-p/15290/highlight/true#M692
    # it's certainly not very RESTy

    GitHub.zuorest_client.update_action(
      {
        "objects": [{
          "fieldsToNull": ["DefaultPaymentMethodId"],
          "Id": zuora_account_id,
        }],
        "type": "Account",
      },
    )
  end
end
