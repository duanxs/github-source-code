# frozen_string_literal: true

UpdateStoragePartitionsJob = LegacyApplicationJob.wrap(GitHub::Jobs::UpdateStoragePartitions)
