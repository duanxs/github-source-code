# frozen_string_literal: true

class DestroyTeamJob < ApplicationJob
  areas_of_responsibility :orgs
  queue_as :destroy_team

  def perform(team_id)
    Team.find(team_id).destroy
  end
end
