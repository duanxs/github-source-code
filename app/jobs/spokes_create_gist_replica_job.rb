# frozen_string_literal: true

SpokesCreateGistReplicaJob = LegacyApplicationJob.wrap(GitHub::Jobs::DgitCreateGistReplica)
