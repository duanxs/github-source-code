# frozen_string_literal: true

class RemoveOauthAppTokensJob < ApplicationJob
  areas_of_responsibility :ecosystem_apps
  queue_as :remove_oauth_app_tokens
  retry_on_dirty_exit

  locked_by timeout: 10.minutes, key: ->(job) {
    job.arguments.first
  }

  # Public: Remove all tokens for an OauthApplication
  #
  # app_id     - The application ID.
  # batch_size - The number of tokens to read from the database per batch.
  # duration   - The amount of time in seconds that each job instance can run
  #              for. After this time is elapsed, if there are still more
  #              batches to process, another job is enqueued to finish the work,
  #              and the current job terminates.
  def perform(app_id, batch_size: 10_000, duration: 60)
    end_at = Time.current + duration

    application = OauthApplication.find_by(id: app_id)
    return false if application.nil?

    authorizations_revoked = 0

    loop do
      authorizations = application.authorizations.limit(batch_size)
      break if authorizations.none?

      authorizations.each do |authorization|
        if authorization.destroy_with_explanation(:oauth_application)
          authorizations_revoked += 1
        end
      end

      break if Time.current >= end_at
    end

    application.instrument :revoke_tokens, tokens_revoked: authorizations_revoked

    if application.authorizations.any?
      clear_lock
      RemoveOauthAppTokensJob.perform_later(app_id, batch_size: batch_size, duration: duration)
    end

    true
  end
end
