# frozen_string_literal: true

class SponsorsTierZuoraSyncJob < ApplicationJob
  queue_as :zuora

  RETRYABLE_ERRORS = [
    Errno::ECONNREFUSED,
    Errno::ECONNRESET,
    Errno::ETIMEDOUT,
    Faraday::ConnectionFailed,
    Faraday::TimeoutError,
    Net::HTTPRequestTimeOut,
    Net::ReadTimeout,
    Zuorest::HttpError,
  ]

  RETRYABLE_ERRORS.each do |err|
    retry_on(err) do |_, error|
      Failbot.report(error)
    end
  end

  def perform(sponsors_tier)
    sponsors_tier.sync_to_zuora
  end
end
