# frozen_string_literal: true

require "codespaces"

class CodespacesSuspendEnvironmentsJob < ApplicationJob
  queue_as :codespaces
  locked_by timeout: 1.minute, key: DEFAULT_LOCK_PROC

  retry_on Codespaces::Client::BadResponseError, wait: :exponentially_longer, attempts: 5 # will retry 4 times over 6 minutes

  def perform(user_id)
    Codespaces::SuspendUserEnvironments.call(User.find(user_id))
  end
end
