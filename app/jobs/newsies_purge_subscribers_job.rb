# frozen_string_literal: true

NewsiesPurgeSubscribersJob = LegacyApplicationJob.wrap(GitHub::Jobs::NewsiesPurgeSubscribers)
