# frozen_string_literal: true

# PageCertificateRemoveExpiredJob deletes certificates for
# all pages sites that expired over 30 days ago
class PageCertificateRemoveExpiredJob < ApplicationJob
  areas_of_responsibility :pages
  queue_as :background_destroy

  # Fastly only accepts up to 1000 non-read requests per hour
  # Since this job will ultimately call DELETE on the Fastly API when
  # the record is destroyed, we limit it to 100 operations per hour
  schedule interval: 1.hour, condition: -> { GitHub.fastly_enabled? && GitHub.pages_custom_domain_https_enabled? }

  SELECT_BATCH_SIZE = 100

  CertificateDeletionError = Class.new(RuntimeError)

  def perform(expiration_date = Time.current.utc - 30.days)
    Failbot.push(app: "pages-certificates")

    deleted = 0

    Page::Certificate.throttle do
      GitHub::Logger.log(fn: self.class.name, at: "begin", expiration_date: expiration_date)
      break unless GitHub.flipper.enabled?(:pages_certificate_remove_expired_job)

      certs = ActiveRecord::Base.connected_to(role: :reading) do
        Page::Certificate
              .where("expires_at < ?", expiration_date)
              .order(id: :asc)
              .limit(SELECT_BATCH_SIZE)
              .pluck(:id, :domain)
      end

      GitHub::Logger.log(fn: self.class.name, at: "search", expiration_date: expiration_date, certificates: certs.size)
      break if certs.empty?

      certs.each do |id, domain|
        # Destroying the record automatically calls out to Fastly
        # to delete the certificate through their Platform API
        Page::Certificate.destroy(id)
        GitHub.dogstats.increment("pages.certificates.delete", { state: "success" })
        deleted += 1
        GitHub::Logger.log(fn: self.class.name, at: "delete", expiration_date: expiration_date, certificate_domain: domain)
      rescue Fastly::CertificateDeletionError => error
        # If the call to Fastly failed, we report it and move on
        # The next time the job runs, this certificate will be tried again automatically
        Failbot.report(
          CertificateDeletionError.new(error, certificate: id, certificate_domain: domain),
          certificate_id: id,
          certificate_domain: domain,
        )
        GitHub.dogstats.increment("pages.certificates.delete", { state: "failure" })
      end
    end

    GitHub::Logger.log(fn: self.class.name, at: "finish", expiration_date: expiration_date, deleted: deleted)
  end
end
