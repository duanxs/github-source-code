# frozen_string_literal: true

class RepositoryDependencyClearDependencies < ApplicationJob
  queue_as :repository_dependencies
  retry_on_dirty_exit

  def perform(repository_id)
    repository = Repository.find_by(id: repository_id)
    return false if repository.nil?

    repository.update_dependency_manifests(clear: true)
    clear_dependencies_mutation(repository)
    true
  end

  def clear_dependencies_mutation(repository)
    mutation = DependencyGraph::ClearDependenciesMutation.new(
      input: {
        repositoryId: repository.id,
      },
    )
    result = mutation.execute
    failed = false
    result.value do
      Failbot.report(result.error, app: "github-dependency-graph", repo_id: repository.id)
      failed = true
    end
  end
end
