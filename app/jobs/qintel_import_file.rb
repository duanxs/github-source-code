# frozen_string_literal: true

class QintelImportFile < ApplicationJob
  queue_as :qintel_import_file

  locked_by timeout: 20.minutes, key: ->(job) {
    args = job.arguments.first
    "#{args[:uuid]}:#{args[:start].to_i}"
  }

  # Rarely occurs when 2 jobs are importing extremely similar files
  # we try to handle this in the job its self but sometimes
  # we can't and the best way to solve this issue is to wait
  # wait until those rows are unlocked
  retry_on(ActiveRecord::Deadlocked, wait: :exponentially_longer, attempts: 5) do |job, error|
    Failbot.report!(error)
  end

  retry_on(GitHub::Restraint::UnableToLock, wait: 5.minutes, attempts: 5) do |job, error|
    Failbot.report!(error)
  end

  # We can process approximately 100k records in less than 5 minutes
  BATCH_SIZE = 100_000

  def perform(uuid:, num_credential_pairs:, start: nil)
    # The key used to store values in KV and also lock
    # restraints
    job_key = "qintel:#{uuid}"

    # We don't want to rely on KV to be able to process
    # the job but if no start value was provided we can
    # attempt to resume the job based on the last save
    # point. If there is no savepoint resort to 0
    start ||= GitHub.kv.get(job_key).value { 0 }.to_i
    final_run = start + BATCH_SIZE >= num_credential_pairs

    GitHub::Restraint.new.lock!(job_key, 1, 20.minutes) do
      return if already_imported?(uuid)

      GitHub::Qintel::CredentialFile.new(hash: {
        fileuuid: uuid,
      }).fetch do |parser|
        import_file(
          job_key: job_key,
          credential_parser: parser,
          start: start,
          final_run: final_run
        )
      end
    end

    queue_next_batch(
      job_key: job_key,
      uuid: uuid,
      start: start,
      num_credential_pairs: num_credential_pairs
    ) unless final_run
  end

  def already_imported?(uuid)
    ActiveRecord::Base.connected_to(role: :reading) do
      already_imported = CompromisedPasswordDatasource.
        where(name: "qintel", version: uuid).
        where.not(import_finished_at: nil)

      if already_imported.exists?
        tags = [
          "datasource:qintel",
          "version:#{uuid}",
        ]
        GitHub.dogstats.increment("qintel.files.already_imported", tags: tags)
        return true
      end
    end
    false
  end

  def import_file(job_key:, credential_parser:, start:, final_run:)
    datasource = CompromisedPasswordDatasource.store_passwords(
      name: "qintel",
      version: credential_parser.uuid,
      sha1_passwords: each_password_digest(credential_parser.parsed(start: start, amount: BATCH_SIZE))
    )

    CompromisedPasswordDatasource.check_for_compromise(
      compromised_records: credential_parser.parsed(start: start, amount: BATCH_SIZE),
      name: "qintel",
      version: credential_parser.uuid,
    )

    if final_run
      datasource.mark_import_as_finished!
      ActiveRecord::Base.connected_to(role: :writing) do
        GitHub.kv.del(job_key)
      end
    end
  end

  def queue_next_batch(job_key:, uuid:, start:, num_credential_pairs:)
    ActiveRecord::Base.connected_to(role: :writing) do
      GitHub.kv.increment(job_key, amount: BATCH_SIZE)
    end
    QintelImportFile.perform_later(
      uuid: uuid,
      num_credential_pairs: num_credential_pairs,
      start: start + BATCH_SIZE
    )
  end

  def each_password_digest(credential_enumerator)
    unless block_given?
      return enum_for(:each_password_digest, credential_enumerator)
    end

    credential_enumerator.each do |username, password|
      next if password.blank?
      yield Digest::SHA1.hexdigest(password)
    end
  end
end
