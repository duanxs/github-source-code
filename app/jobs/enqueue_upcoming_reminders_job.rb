# frozen_string_literal: true
class EnqueueUpcomingRemindersJob < ApplicationJob
  areas_of_responsibility :ce_extensibility
  queue_as :low
  around_perform :set_readonly_db

  schedule interval: 10.minutes, condition: -> { !GitHub.enterprise? }

  def perform
    ReminderDeliveryTime.upcoming.find_each do |delivery_time|
      delivery_target = delivery_time.next_delivery_at
      ProcessReminderJob.set(wait_until: delivery_target).perform_later(delivery_time.schedulable, delivery_target: delivery_target)
    end
  end

  private

  def set_readonly_db
    ActiveRecord::Base.connected_to(role: :reading) do
      yield
    end
  end
end
