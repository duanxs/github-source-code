# frozen_string_literal: true

# Job to scan a repository's full history and store found tokens
class RepositoryPrivateTokenScanningJob < PrivateTokenScanningJob
  def token_infos
    @token_infos ||= find_creds(buffer_results: true, skip_fp: true)
  end

  def job_scope
    "repo"
  end

  def job_visibility
    "private"
  end

  #indicates if this job is tracked by the token scanning job scheduler (for retry and auditing).
  def is_tracked_by_scheduler?
    return true
  end

  def is_commit_scoped?
    false
  end
end
