# frozen_string_literal: true

class StaleCheckJob < ApplicationJob
  areas_of_responsibility :ce_extensibility
  queue_as :stale_check_runs

  # No retry_on because we're running again in 90 seconds.

  schedule interval: 90.seconds
  DURATION_SECONDS = 60

  # Ensure that only one maintenance pass is happening at a time in the unlikely event we run much longer than 60 seconds.
  # Ignore job arguments when generating the key.
  locked_by timeout: 2.minutes, key: ->(job) { "key" }

  def perform
    return unless GitHub.flipper[:k8s_stale_check_jobs].enabled?

    # Run for up to 60 seconds.
    start_time = Time.now
    end_at = start_time.to_f + DURATION_SECONDS

    ActiveRecord::Base.connected_to(role: :reading) do
      GitHub.dogstats.time("checks.stale.time_per_batch") do
        CheckSuite.throttle do
          stale_suites = CheckSuite
            .incomplete_and_older_than_stale_threshold(start_time: start_time)
            .limit(10_000)
          GitHub.dogstats.gauge("checks.stale.batch_size", stale_suites.size)

          stale_suites.each do |check_suite|
            begin
              evaluate_check_suite(start_time, check_suite)
            rescue StandardError => err
              # Report failure but don't stop processing the batch.
              Failbot.report(err.with_redacting!)
            end
            break if Time.now.to_f >= end_at
          end
        end
      end
    end
  end

  private

  # Takes a CheckSuite id and marks the check stale, if the check is older than
  # CheckSuite::DEFAULT_STALE_THRESHOLD (2 weeks).
  def evaluate_check_suite(start_time, check_suite)
    GitHub.dogstats.increment("checks.stale.check_suites_found")
    ActiveRecord::Base.connected_to(role: :reading) do
      # Verify that the check suite isn't completed yet (as a double precaution).
      return if check_suite.completed?

      # Verify that the check suite is older than 2 weeks (as a double precaution).
      return if check_suite.updated_at > (start_time - CheckSuite::DEFAULT_STALE_THRESHOLD)

      check_runs = []

      GitHub.dogstats.time("checks.stale.latest_runs_time") do
        check_runs = check_suite.latest_check_runs do |check_runs|
          check_runs.where.not(status: :completed)
        end
      end

      if check_runs.empty?
        mark_stale_check_suite_complete(check_suite)
      else
        # Otherwise, mark each check run as stale, which would update the check suite (indirectly).
        check_runs.each do |check_run|
          GitHub.dogstats.time("checks.stale.update_time") do
            CheckRun.throttle_writes do
              # There may be old records that don't pass current validations.
              # Skip validation so we can mark them stale anyway.
              check_run.conclusion = :stale
              check_run.status = :completed
              check_run.save!(validate: false)
            end

            GitHub.dogstats.increment("checks.marked_stale")
          end
        end

        if check_suite.explicit_completion
          mark_stale_check_suite_complete(check_suite)
        end
      end
    end
  end

  def mark_stale_check_suite_complete(check_suite)
    # If there are no incomplete check runs, but the check suite is still incomplete
    # then mark the check suite as stale.
    GitHub.dogstats.time("checks.stale.update_time.check_suite_only") do
      CheckSuite.throttle_writes do
        # Temporarily only use #update_columns to ensure no webhook events are created for this case.
        # See https://github.com/github/github/pull/143314#issuecomment-626029535 for details.
        # TODO: change #update_columns to #update! after the rollout.
        check_suite.update_columns(conclusion: :stale, status: :completed)
      end
      GitHub.dogstats.increment("checks.marked_stale")
    end
  end
end
