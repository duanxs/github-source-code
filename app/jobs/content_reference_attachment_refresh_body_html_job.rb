# frozen_string_literal: true

class ContentReferenceAttachmentRefreshBodyHtmlJob < ApplicationJob
  areas_of_responsibility :ce_extensibility
  queue_as :invalidate_content_attachments_cache

  discard_on ActiveRecord::RecordNotFound

  def perform(integration_id)
    content_attachments = ContentReferenceAttachment.for_integration_id(integration_id)
    GitHub.dogstats.count("testing.content_attachments.cache_invalidate", content_attachments.count)
    content_attachments.each(&:refresh_body_html)
  end
end
