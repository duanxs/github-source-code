# frozen_string_literal: true

class DeleteOutdatedInteractiveComponentsJob < ApplicationJob
  areas_of_responsibility :ce_extensibility
  queue_as :interactive_components

  schedule interval: 1.day

  def perform
    InteractiveComponent.where("outdated_at < ?", 1.day.ago).find_in_batches do |group|
      InteractiveComponent.throttle do
        group.each(&:destroy)
      end
    end
  end
end
