# frozen_string_literal: true

NotifySubscriptionStatusChangeJob = LegacyApplicationJob.wrap(GitHub::Jobs::NotifySubscriptionStatusChange) do
  set_max_redelivery_attempts 0
end
