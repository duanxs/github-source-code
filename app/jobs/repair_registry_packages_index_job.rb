# frozen_string_literal: true

RepairRegistryPackagesIndexJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepairRegistryPackagesIndex)
