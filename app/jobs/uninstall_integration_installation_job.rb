# frozen_string_literal: true

UninstallIntegrationInstallationJob = LegacyApplicationJob.wrap(GitHub::Jobs::UninstallIntegrationInstallation)
