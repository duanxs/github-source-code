# frozen_string_literal: true

StorageClusterMaintenanceSchedulerJob = LegacyApplicationJob.wrap(GitHub::Jobs::StorageClusterMaintenanceScheduler)
