# frozen_string_literal: true

class PopulateSponsorsFraudReviewsJob < ApplicationJob
  queue_as :sponsors_application_processing

  schedule interval: 1.day, condition: -> { GitHub.sponsors_enabled? }

  attr_reader :date_range, :amount_in_cents

  def perform(amount_in_cents: 500_00)
    Failbot.push(app: "github-sponsors")

    @date_range = last_payout_date..Date.today
    @amount_in_cents = amount_in_cents

    flaggable_listing_ids.each do |listing_id|
      SponsorsFraudReview.create!(sponsors_listing_id: listing_id)
    end
  end

  private

  def candidate_listing_ids
    Billing::PayoutsLedgerEntry
      .joins(:stripe_connect_account)
      .where(transaction_type: :transfer,
             transaction_timestamp: date_range)
      .group("stripe_connect_accounts.payable_id")
      .having("sum(amount_in_subunits) >= #{amount_in_cents}")
      .select("stripe_connect_accounts.payable_id as listing_id")
      .map(&:listing_id)
  end

  def flaggable_listing_ids
    SponsorsListing
      .joins(<<~SQL)
        LEFT JOIN sponsors_fraud_reviews
        ON sponsors_fraud_reviews.sponsors_listing_id = sponsors_listings.id
        AND sponsors_fraud_reviews.created_at > '#{date_range.begin}'
      SQL
      .where(id: candidate_listing_ids, sponsors_fraud_reviews: { id: nil })
      .pluck(:id)
  end

  def last_payout_date
    start_date = DateTime.now

    payout_date = if start_date.day > SponsorsListing::MONTHLY_PAYOUT_DAY
      start_date
    else
      start_date - 1.month
    end

    Date.new(payout_date.year, payout_date.month, SponsorsListing::MONTHLY_PAYOUT_DAY)
  end
end
