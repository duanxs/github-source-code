# frozen_string_literal: true

class ZuoraWebhookJob < ApplicationJob
  queue_as :zuora

  RETRYABLE_ERRORS = [
    Braintree::UnexpectedError,
    Errno::ECONNRESET,
    Faraday::ConnectionFailed,
    Faraday::SSLError,
    Faraday::TimeoutError,
    GitHub::Restraint::UnableToLock,
    Net::OpenTimeout,
    Stripe::APIError,
    Zuorest::HttpError,
  ].freeze

  discard_on(StandardError) do |job, error|
    GitHub.dogstats.increment(
      "zuora.webhook_error",
      tags: ["category:#{job.type}",
             "responsibility:#{job.responsibility}",
             "exception:#{error.class.name.parameterize}"],
    )
    Failbot.report(error)
  end

  RETRYABLE_ERRORS.each do |retryable|
    retry_on(retryable) do |job, error|
      GitHub.dogstats.increment(
        "zuora.webhook_error",
        tags: ["category:#{job.type}",
               "responsibility:#{job.responsibility}",
               "exception:#{error.class.name.parameterize}"],
      )
      Failbot.report(error)
    end
  end

  retry_on(ActiveRecord::RecordNotUnique, wait: 1.minute, attempts: 2) do |_job, error|
    Failbot.report(error)
  end

  attr_reader :type, :responsibility

  def perform(zuora_webhook)
    @type = zuora_webhook.kind
    @responsibility = zuora_webhook.responsibility

    Failbot.push(type: type, zuora_webhook_id: zuora_webhook.id)
    if GitHub.zuorest_client
      GitHub.zuorest_client.timeout = 60
      GitHub.zuorest_client.open_timeout = 60
    end

    latency_ms = (Time.now.to_f - zuora_webhook.created_at.to_f) * 1_000
    GitHub.dogstats.timing("zuora.webhook_latency", latency_ms.to_i)

    lock(zuora_webhook.id) { zuora_webhook.perform }
  end

  # Internal: Use a GitHub::Restraint to prevent simultaneous updates
  #
  # zuora_webhook_id - The ID of the webhook we're processing
  # block            - The code block to execute with an exclusive lock
  #
  # Returns the value returned by block
  def lock(zuora_webhook_id, &block)
    lock_key = "ZuoraWebhookJob-#{zuora_webhook_id}"

    ret = nil
    restraint.lock!(lock_key, _n = 1, _ttl = 5.minutes) do
      ret = block.call
    end
    ret
  end

  # Internal: The restraint for locking and preventing simultaneous updates
  #
  # Returns GitHub::Restraint
  def restraint
    @restraint ||= GitHub::Restraint.new
  end
end
