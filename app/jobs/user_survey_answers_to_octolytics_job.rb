# frozen_string_literal: true

UserSurveyAnswersToOctolyticsJob = LegacyApplicationJob.wrap(GitHub::Jobs::UserSurveyAnswersToOctolytics) do
  self.queue_adapter = :aqueduct
end
