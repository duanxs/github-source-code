# frozen_string_literal: true

ExperimentEnrollmentToOctolyticsJob = LegacyApplicationJob.wrap(GitHub::Jobs::ExperimentEnrollmentToOctolytics)
