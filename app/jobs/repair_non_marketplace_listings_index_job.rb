# frozen_string_literal: true

RepairNonMarketplaceListingsIndexJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepairNonMarketplaceListingsIndex)
