# frozen_string_literal: true

class ConvertToDiscussionJob < ApplicationJob
  queue_as :convert_to_discussion

  def perform(actor, discussion, issue_originally_open = nil)
    issue = discussion.issue
    return unless issue

    converter = IssueToDiscussionConverter.new(issue, actor: actor,
      issue_originally_open: issue_originally_open)
    converter.finish_conversion
  end
end
