# frozen_string_literal: true

class BulkRepositorySetCommunityHealthFlagsJob < ApplicationJob
  queue_as :detect_community_health_files

  # Public: Queue a community health check job for every public, non-fork
  # repository that is owned a user.
  #
  # user - A User that owns repositories.
  #
  # Returns nothing.
  def perform(user)
    return if GitHub.enterprise?

    # Get all public repositories that are not forks
    repositories = user.public_repositories.where("repositories.parent_id IS NULL")

    repositories.each do |repository|
      CommunityProfile.enqueue_health_check_job(repository)
    end
  end
end
