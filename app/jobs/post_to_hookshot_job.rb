# frozen_string_literal: true

PostToHookshotJob = LegacyApplicationJob.wrap(GitHub::Jobs::PostToHookshot)
