# frozen_string_literal: true

SpokesMoveGistReplicaJob = LegacyApplicationJob.wrap(GitHub::Jobs::DgitMoveGistReplica)
