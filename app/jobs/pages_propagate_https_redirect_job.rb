# frozen_string_literal: true

PagesPropagateHttpsRedirectJob = LegacyApplicationJob.wrap(GitHub::Jobs::PagesPropagateHttpsRedirect)
