# frozen_string_literal: true

OtpSmsTimingCleanupJob = LegacyApplicationJob.wrap(GitHub::Jobs::OtpSmsTimingCleanup)
