# frozen_string_literal: true

class RepositorySetLicenseJob < ApplicationJob
  areas_of_responsibility :repositories
  queue_as :repository_set_license

  # Discard the job if the repository is deleted before the job runs
  discard_on ActiveRecord::RecordNotFound

  def perform(repository)
    repository.set_license
  end
end
