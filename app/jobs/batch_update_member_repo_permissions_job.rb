# frozen_string_literal: true

class BatchUpdateMemberRepoPermissionsJob < ApplicationJob
  queue_as :update_member_repo_permissions

  # Updates org members and/or collaborators permissions/roles and deletes custom role if present and
  # has not dependencies
  #
  # user_roles        - Active records of user/permission or user/role association
  # action            - The permission to update to. Can be :read, :write, :admin, :triage, :maintain, or a custom role
  # organization      - Org object whose members and/or collaborators to be updated
  # role              - Custom role. default: nil
  # context           - Hash of Strings with the users' previous Role {:old_permission, :old_base_role}
  #
  # Returns nothing
  def perform(user_roles, action:, organization:, role: nil, context: {})
    user_roles.each do |user_role|
      repo_id = user_role.try(:subject_id) || user_role.try(:target_id)

      repo = fetch_repo(repo_id, user_roles)
      action = repo.evaluate_action(action)
      user = fetch_user(user_role.actor_id, user_roles)

      repo.update_member(user, action: action.to_sym, context: context)
      raise RuntimeError.new if repo.errors.any?
    end

    if !role.nil? || role&.all_dependencies_updated?
      GitHub.dogstats.increment("orgs_roles.delete.queued_to_delete", tags: ["status:deleted"]) if role.destroy!
    end
  end

  private

  def fetch_user(user_id, user_roles)
    org_users(user_roles)
    @org_users.find { |user| user.id == user_id }
  end

  def fetch_repo(repo_id, user_roles)
    org_repos(user_roles)
    @org_repos.find { |repo| repo.id == repo_id }
  end

  def org_users(user_roles)
    return @org_users if defined?(@org_users)
    @org_users = begin
      User.where(id: user_roles.map(&:actor_id))
    end
  end

  def org_repos(user_roles)
    return @org_repos if defined?(@org_repos)
    @org_repos = begin
      repo_ids = []
      begin
        repo_ids = user_roles.map(&:subject_id)
      rescue NoMethodError
        repo_ids = user_roles.map(&:target_id)
      end
      Repository.where(id: repo_ids)
    end
  end
end
