# frozen_string_literal: true

HandleTeamParentChangesJob = LegacyApplicationJob.wrap(GitHub::Jobs::HandleTeamParentChanges)
