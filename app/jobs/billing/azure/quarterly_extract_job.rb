# frozen_string_literal: true

# This job runs hourly but is a no-op every hour except for the first run after 1am (in the billing
# time zone) on the first day of a quarter. In this non-no-op run it will extract metered billing data
# for customers on an enterprise agreement (< 10 enterprise accounts) and uploads the data to S3.
module Billing
  module Azure
    class QuarterlyExtractJob < ApplicationJob
      areas_of_responsibility :gitcoin

      queue_as :billing

      schedule interval: 1.hour, condition: -> { GitHub.billing_enabled? }

      START_DAY = 27
      HOUR_TO_RUN = 1
      NEXT_RUN_KV_KEY = "billing.azure.quarterly_extract.next_run"

      def perform
        return unless run_governer.run_now?

        start_on = GitHub::Billing.today.beginning_of_quarter - 3.months

        # 3 times for 3 months in a quarter
        data_sets = 3.times.flat_map do |i|
          month_start = (start_on + i.months).change(day: START_DAY)
          month_end = month_start.end_of_month
          timestamp = GitHub::Billing.date_in_timezone(month_end).end_of_day

          export = Billing::Azure::MeteredBillingExport.new(start_on: month_start, end_on: month_end, static_event_timestamp: timestamp)
          export.data
        end

        Billing::Azure::ExtractStorage.store(
          data: build_csv(data_sets),
          filename: "quarterly/github-metered-billing-ending-#{start_on.end_of_quarter.strftime("%Y-%m-%d")}.csv",
          content_type: "text/csv",
        )

        run_governer.record_next_run_time(date: start_on + 6.months, hour: HOUR_TO_RUN)
      end

      private

      def build_csv(data)
        CSV.generate(headers: data.first&.keys, write_headers: true) do |csv|
          data.map(&:values).each do |values|
            csv << values
          end
        end
      end

      def run_governer
        @run_governer ||= Billing::Azure::JobRunGoverner.new(job_key: NEXT_RUN_KV_KEY)
      end
    end
  end
end
