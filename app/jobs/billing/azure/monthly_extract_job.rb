# frozen_string_literal: true

# This job runs hourly but is a no-op every hour except for the first run after 1am (in the billing
# time zone) on the 27th of the month. In this non-no-op run it will extract metered billing data
# for customers on an enterprise agreement (< 10 enterprise accounts) and uploads the data to S3.
module Billing
  module Azure
    class MonthlyExtractJob < ApplicationJob
      areas_of_responsibility :gitcoin

      queue_as :billing

      schedule interval: 1.hour, condition: -> { GitHub.billing_enabled? }

      START_DAY = 27
      END_DAY = START_DAY - 1
      HOUR_TO_RUN = 1
      NEXT_RUN_KV_KEY = "billing.azure.monthly_extract.next_run"

      def perform
        return unless run_governer.run_now?

        start_on = end_on - 1.month + 1.day
        exporter = Billing::Azure::MeteredBillingExport.new(start_on: start_on, end_on: end_on)
        filename = "monthly/github-metered-billing-ending-#{end_on.strftime("%Y-%m-%d")}.json"

        Billing::Azure::ExtractStorage.store(
          data: exporter.data.to_json,
          filename: filename,
          content_type: "application/json",
        )
        Billing::Azure::MonthlyExtractNotifier.new(exporter: exporter, filename: filename).notify

        run_governer.record_next_run_time(date: start_on + 2.months, hour: HOUR_TO_RUN)
      end

      private

      def end_on
        @end_on ||= begin
          base_date = GitHub::Billing.today
          base_date -= 1.month if base_date.day < END_DAY
          base_date.change(day: END_DAY)
        end
      end

      def run_governer
        @run_governer ||= Billing::Azure::JobRunGoverner.new(job_key: NEXT_RUN_KV_KEY)
      end
    end
  end
end
