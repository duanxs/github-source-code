# frozen_string_literal: true

module Billing
  class ScheduleZuoraUsageFileUploadsJob < ApplicationJob
    areas_of_responsibility :gitcoin

    schedule interval: 5.minutes, condition: -> { GitHub.billing_enabled? }

    queue_as :billing

    def perform
      ignored_batch_ids = []

      Billing::UsageSynchronizationBatch.ready_to_submit.find_each do |usage_sync_batch|
        if GitHub.flipper[:schedule_zuora_usage_file_uploads].enabled?
          SubmitZuoraUsageFileJob.perform_later(usage_sync_batch)
        else
          ignored_batch_ids << usage_sync_batch.id
        end
      end

      # In the case of the flipper flag being disabled either before or during this job execution
      # we want to update all of the skipped over UsageSyncBatch records to a new ignored status.
      if ignored_batch_ids.any?
        ActiveRecord::Base.connected_to(role: :writing) do
          Billing::UsageSynchronizationBatch
            .where(id: ignored_batch_ids)
            .update_all(status: :ignored, updated_at: Time.current)
        end
      end
    end
  end
end
