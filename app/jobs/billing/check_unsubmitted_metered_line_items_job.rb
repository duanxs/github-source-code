# frozen_string_literal: true

module Billing
  class CheckUnsubmittedMeteredLineItemsJob < ApplicationJob
    areas_of_responsibility :gitcoin

    queue_as :billing
    schedule interval: 5.minutes, condition: -> { GitHub.billing_enabled? }

    SUBMISSION_THRESHOLD = 24.hours

    METERED_PRODUCT_LINE_ITEMS = {
      actions: -> { ::Billing::ActionsUsageLineItem },
      packages: -> { ::Billing::PackageRegistry::DataTransferLineItem },
      shared_storage: -> { ::Billing::SharedStorage::ArtifactAggregation },
    }

    # Public: Report metrics on the number of unsubmitted line items
    #
    # Returns nothing
    def perform
      METERED_PRODUCT_LINE_ITEMS.each do |product, root_scope_proc|
        unsubmitted_line_items = root_scope_proc.call
          .unsubmitted
          .where("created_at < ?", SUBMISSION_THRESHOLD.ago)
        unsubmitted_count = unsubmitted_line_items.count

        submit_gauge_metric_for(product, unsubmitted_count, type: "count")
        if unsubmitted_count.positive?
          max_age_in_milliseconds = (Time.now.to_i - unsubmitted_line_items.minimum(:created_at).to_i) * 1000
          submit_gauge_metric_for(product, max_age_in_milliseconds, type: "max_age")
        end
      end
    end

    private

    def submit_gauge_metric_for(product, value, type:)
      GitHub.dogstats.gauge("metered_billing.unsubmitted_line_items.#{type}", value, tags: ["product:#{product}"])
    end
  end
end
