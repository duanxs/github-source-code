# frozen_string_literal: true

module Billing
  class AggregateRecalculationJob < ApplicationJob
    queue_as :billing
    def perform(aggregation)
      aggregation.recalculate_aggregate
    end
  end
end
