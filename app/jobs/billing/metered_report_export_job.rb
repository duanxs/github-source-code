# frozen_string_literal: true

class Billing::MeteredReportExportJob < ApplicationJob
  DATE_FORMAT = "%Y-%m-%d"

  areas_of_responsibility :gitcoin

  queue_as :billing

  def perform(requester, billable_owner, days)
    lock(billable_owner, days) do
      begin
        filepath = filepath_for(billable_owner: billable_owner, days: days)
        csv = Billing::MeteredUsageReportGenerator.csv_for(owner: billable_owner, days: days)

        upload_to_s3(csv, filepath)

        report = Billing::MeteredUsageExport.create!(
          requester: requester,
          billable_owner: billable_owner,
          starts_on: GitHub::Billing.today - days.days,
          ends_on: GitHub::Billing.today,
          filename: filepath,
        )
      rescue StandardError => e
        Billing::ReportMailer.metered_export_error(billable_owner, requester).deliver_later
        raise e
      end

      Billing::ReportMailer.metered_export_complete(report).deliver_later
    end
  end

  private

  def filepath_for(billable_owner:, days:)
    [
      billable_owner.class,
      billable_owner.name.underscore,
      "#{SecureRandom.hex(4)}_#{Date.current.strftime(DATE_FORMAT)}_#{days}.csv",
    ].join("/")
  end

  def upload_to_s3(csv, filename)
    object = Aws::S3::Resource.new(client: s3_client).
      bucket(Billing::MeteredUsageExport::BUCKET_NAME).
      object(filename)

    object.put(
      body: csv,
      content_type: "text/csv",
    )
  end

  def s3_client
    GitHub.s3_metered_exports_client
  end

  # Internal: Use a GitHub::Restraint to prevent simultaneous updates
  def lock(billable_owner, days, &block)
    lock_key = "metered-export-#{billable_owner.class.to_s.downcase}-#{billable_owner.id}-#{days}"

    restraint.lock!(lock_key, _max_concurrency = 1, _ttl = 5.minutes, &block)
  end

  # Internal: The restraint for locking and preventing simultaneous updates
  #
  # Returns GitHub::Restraint
  def restraint
    @restraint ||= GitHub::Restraint.new
  end
end
