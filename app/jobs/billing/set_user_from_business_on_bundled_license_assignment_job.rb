# frozen_string_literal: true

class Billing::SetUserFromBusinessOnBundledLicenseAssignmentJob < ApplicationJob
  areas_of_responsibility :gitcoin
  queue_as :billing

  discard_on ActiveRecord::RecordNotFound

  def perform(assignment:)
    return if assignment.user.present?
    return if assignment.business.nil?

    user = business_user_by_verified_email(assignment)
    assignment.update(user_id: user.id) if user.present?
  end

  private

  def business_user_by_verified_email(assignment)
    business = assignment.business
    business_user_account = business.dotcom_users_from_emails([assignment.email]).values.first
    business_user_account&.user
  end
end
