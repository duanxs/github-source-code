# frozen_string_literal: true

module Billing
  module SharedStorage
    class AggregationJob < ApplicationJob
      queue_as :metered_billing

      discard_on(StandardError) do |job, error|
        GitHub.dogstats.increment(
          "billing.shared_storage.aggregation_error",
          tags: ["job:aggregation_job"],
        )

        Failbot.report(error)
      end

      retry_on(GitHub::Restraint::UnableToLock, wait: 5.minutes) do |job, error|
        GitHub.dogstats.increment(
          "billing.shared_storage.aggregation_error",
          tags: ["job:aggregation_job"],
        )

        Failbot.report(error)
      end

      # Perform the AggregationJob for a given user
      #
      # aggregate_fields - the values being aggregated on
      # cutoff - The cutoff time for shared storage artifact events; only events
      #          with an effective_at time before the cutoff will be aggregated
      def perform(cutoff:, **aggregate_fields)
        ArtifactEventAggregator.new(cutoff: cutoff, **aggregate_fields).perform
      end
    end
  end
end
