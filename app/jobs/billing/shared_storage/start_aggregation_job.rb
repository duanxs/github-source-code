# frozen_string_literal: true

module Billing
  module SharedStorage
    class StartAggregationJob < ApplicationJob
      queue_as :billing

      schedule interval: 15.minutes, condition: -> { GitHub.billing_enabled? }

      discard_on(StandardError) do |job, error|
        GitHub.dogstats.increment(
          "billing.shared_storage.aggregation_error",
          tags: ["job:start_aggregation_job"],
        )

        Failbot.report(error)
      end

      # Perform the StartAggregationJob
      def perform
        return unless GitHub.flipper[:shared_storage_aggregation_job].enabled?

        ArtifactEventAggregationStart.new.perform
      end
    end
  end
end
