# frozen_string_literal: true

module Billing
  class CheckBusinessOrganizationTransitionFailuresJob < ApplicationJob
    areas_of_responsibility :gitcoin

    queue_as :billing
    schedule interval: 5.minutes, condition: -> { GitHub.billing_enabled? }

    # Public: Report gauge metric of organization added to business billing transition failures
    #
    # When adding an organization to a business we enqueue a BusinessOrganizationBillingJob to transition the
    # organization's billing type to invoice.  This job reports to a gauge in Datadog when the
    # BusinessOrganizationBillingJob has failures and manual intervention is now necessary.
    #
    # Returns nothing
    def perform
      failed_billing_transitions = Organization.
        joins(:business_membership).
        where.not(billing_type: :invoice).
        where("business_organization_memberships.created_at < ?", 10.minutes.ago)
      failed_billing_transitions_count = failed_billing_transitions.count

      GitHub.dogstats.gauge("billing.business_organization_transition_failures.count", failed_billing_transitions_count)

      if failed_billing_transitions_count.positive?
        max_age_in_milliseconds = (Time.now.to_i - failed_billing_transitions.minimum(:created_at).to_i) * 1000

        GitHub.dogstats.gauge("billing.business_organization_transition_failures.max_age", max_age_in_milliseconds)
      end
    end
  end
end
