# frozen_string_literal: true

module Billing
  # Job to reconcile metered line items actually submitted to Zuora with
  # the various line item tables in the database
  class ReconcileMeteredLineItemsJob < ApplicationJob
    areas_of_responsibility :gitcoin

    queue_as :billing

    RETRYABLE_ERRORS = [
      Aws::S3::Errors::ServiceError,
      Seahorse::Client::NetworkingError,
    ].freeze

    discard_on(StandardError) do |_job, error|
      Failbot.report(error)
    end

    retry_on(*RETRYABLE_ERRORS) do |_job, error|
      Failbot.report(error)
    end

    # Public: Perform line item reconciliation for a given UsageSynchronizationBatch
    #
    # Returns nothing
    def perform(usage_synchronization_batch)
      Zuora::UsageFile::Reconciler.perform(usage_synchronization_batch)
    end
  end
end
