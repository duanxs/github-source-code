# frozen_string_literal: true

module Billing
  class InvoicedMeteredBillingConfigurationUpdateJob < ApplicationJob
    areas_of_responsibility :gitcoin

    queue_as :billing

    locked_by key: ApplicationJob::DEFAULT_LOCK_PROC, timeout: 5.minutes

    def perform(owner, product: :shared)
      return unless Billing::PrepaidMeteredUsageRefill.enabled_for?(owner)

      limit_in_subunits = Billing::PrepaidMeteredUsageRefill.total_active_amount_in_cents_for(owner: owner) *
        Billing::PrepaidMeteredUsageRefill::OVERAGE_LIMIT_MULTIPLIER

      # TODO: update budgets based on the refill's product...
      # See https://github.com/github/gitcoin/issues/4704 for tracking
      owner.budget_for(product: product).update!(
        enforce_spending_limit: true,
        spending_limit_in_subunits: limit_in_subunits.to_i,
      )
    end
  end
end
