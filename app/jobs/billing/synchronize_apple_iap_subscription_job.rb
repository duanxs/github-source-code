# frozen_string_literal: true

module Billing
  class SynchronizeAppleIapSubscriptionJob < ApplicationJob
    class SynchronizeError < StandardError; end

    areas_of_responsibility :gitcoin

    schedule interval: 1.day, condition: -> { GitHub.billing_enabled? }

    queue_as :billing

    attr_accessor :plan_subscription_id

    retry_on GitHub::Restraint::UnableToLock, wait: 1.minute, attempts: 10 do |job, error|
      Failbot.report(error, plan_subscription_id: job.plan_subscription_id)
    end

    BATCH_SIZE = 25

    def perform
      subscription_ids = []

      ActiveRecord::Base.connected_to(role: :reading) do
        subscription_ids = Billing::PlanSubscription.where.not(apple_transaction_id: nil).pluck(:id)
      end

      GitHub.dogstats.count("billing.plan_subscription.apple_iap_job.ids", subscription_ids.size)

      subscription_ids.each_slice(BATCH_SIZE) do |ids|
        subscriptions = Billing::PlanSubscription.where(id: ids)

        subscriptions.each do |subscription|
          # This intended to be used when a GitHub::Restraint::UnableToLock error is raised
          @plan_subscription_id = subscription.id

          Billing::PlanSubscription.throttle_writes_with_retry(max_retry_count: 5) do
            lock(subscription) do
              result = Billing::PlanSubscription::AppleIapSynchronizer.call(subscription)

              if result.success?
                GitHub.dogstats.increment("billing.plan_subscription.apple_iap_job.synchronized")
              else
                GitHub.dogstats.increment("billing.plan_subscription.apple_iap_job.not_synchronized")
                Failbot.report(
                  SynchronizeError.new("Failed to syncrhonize subscription with apple"),
                  plan_subscription: subscription.id,
                  errors: result.error_message,
                )
              end
            end
          end
        end
      end
    end

    private

    # Internal: Use a GitHub::Restraint to prevent simultaneous updates
    def lock(subscription, &block)
      lock_key = "billing.plan_subscription.apple.#{subscription.id}"

      restraint.lock!(lock_key, _max_concurrency = 1, _ttl = 5.minutes, &block)
    end

    # Internal: The restraint for locking and preventing simultaneous updates
    #
    # Returns GitHub::Restraint
    def restraint
      @restraint ||= GitHub::Restraint.new
    end
  end
end
