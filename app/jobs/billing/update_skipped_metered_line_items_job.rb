# frozen_string_literal: true

module Billing
  class UpdateSkippedMeteredLineItemsJob < ApplicationJob
    areas_of_responsibility :gitcoin
    queue_as :billing

    class InvalidProduct < StandardError; end

    locked_by key: ApplicationJob::DEFAULT_LOCK_PROC, timeout: 5.minutes

    # Public: Metered Line item usage classes hash keyed by symbol id
    #
    # Example
    #  {
    #    actions: Billing::ActionsUsageLineItem,
    #    packages: Billing::PackageRegistry::DataTransferLineItem,
    #  }
    #
    # Returns Hash[Symbol] => Class
    def metered_billing_products
      Billing::MeteredBillingProduct.products
    end

    def perform(billable_owner:, product: nil)

      # If no product is supplied, fanout for each product
      unless product
        metered_billing_products.keys.each do |product|
          Billing::UpdateSkippedMeteredLineItemsJob.perform_later(billable_owner: billable_owner, product: product)
        end
        return
      end

      accounting_period_start = GitHub::Billing.now.beginning_of_month
      current_cycle_start = GitHub::Billing.date_in_timezone(billable_owner.first_day_in_metered_cycle)

      # Grab whichever date is later to use as the start date
      start_date = [accounting_period_start, current_cycle_start].max

      line_item_class = metered_billing_products.fetch(product.to_sym) do
        raise InvalidProduct, "#{product} is not a valid product"
      end

      line_items = line_item_class.directly_billed.skipped.
        after(start_date).
        where(billable_owner: billable_owner)

      if line_items.any?
        line_item_flyweight = line_item_class.new
        line_item_class.throttle do
          ActiveRecord::Base.connected_to(role: :writing) do
            line_items.in_batches do |batch|
              ids = batch.pluck(:id)
              batch.update_all(submission_state: :unsubmitted, submission_state_reason: nil)

              ids.each do |id|
                line_item_flyweight.id = id
                line_item_flyweight.publish_metered_line_item_updated_message
              end
            end
          end
        end
      end
    end
  end
end
