# frozen_string_literal: true

module Billing
  class SnapshotLicensesJob < ApplicationJob
    areas_of_responsibility :gitcoin
    queue_as :billing

    discard_on ActiveJob::DeserializationError

    def perform(business)
      GlobalInstrumenter.instrument("business_licenses.snapshot", business: business)
    end
  end
end
