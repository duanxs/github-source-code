# frozen_string_literal: true

class Billing::SetUserFromEmailsOnBundledLicenseAssignmentJob < ApplicationJob
  areas_of_responsibility :gitcoin
  queue_as :billing

  discard_on ActiveRecord::RecordNotFound

  def perform(business:, user:, emails: nil)
    return unless business.member?(user)

    assignment_emails = user.emails.verified.pluck(:email)
    assignment_emails += emails.to_a

    Billing::BundledLicenseAssignment.unassigned.where(
      business_id: business.id,
      email: assignment_emails.compact.uniq
    ).each do |assignment|
      assignment.update(user_id: user.id)
    end
  end
end
