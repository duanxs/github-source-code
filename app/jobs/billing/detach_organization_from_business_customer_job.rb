# frozen_string_literal: true

module Billing
  class DetachOrganizationFromBusinessCustomerJob < ApplicationJob
    queue_as :billing

    areas_of_responsibility :gitcoin

    def perform(business, organization)
      return if organization.plan_subscription.nil? || organization.plan_subscription.customer != business.customer

      if organization.customer.nil?
        organization.update!(customer: Billing::CreateCustomer.perform(organization).customer)
      end

      organization.plan_subscription.update!(customer: organization.customer)
    end
  end
end
