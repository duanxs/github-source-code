# frozen_string_literal: true

class CategorizeIntegrationsJob < ApplicationJob
  queue_as :marketplace
  schedule interval: 24.hours, condition: -> { !GitHub.enterprise? }

  # Read more: https://github.com/github/carolus
  CAROLUS_URL = "https://carolus-production.service.cp1-iad.github.net"

  def perform
    if GitHub.flipper[:integration_classification].enabled?
      # 1. Get GitHub Apps to be categorized
      integrations = Integration.for_classification.limit(20).to_json

      # 2. Send integrations to Carolus
      response = categorize_integrations(integrations)

      # 3. Update DB with new category
      update_marketplace_category_id(response)
    end
  end

  def categorize_integrations(integrations)
    conn = Faraday.new(url: CAROLUS_URL)
    res = conn.post do |req|
      req.url "/classify"
      req.headers["Content-Type"] = "application/json"
      req.body = integrations
    end
    JSON.parse(res.body)
  end

  def update_marketplace_category_id(response)
    response["results"]&.each do |result|
      category_id = Marketplace::Category.find_by(name: result["top_score_key"]).id
      integration = Integration.find_by(slug: result["slug"])
      integration.update(marketplace_category_id: category_id)
    end
  end
end
