# frozen_string_literal: true

# This job only runs on Enterprise Server and should only be enqueued after a
# configuration run happens on an installation.
#
# This job ensures that in case the authentication mode for the Enterprise
# Server installation has changed to one that does not support 2FA
# (currently SAML and CAS), which is determined by
# GitHub.auth.two_factor_org_requirement_allowed?, the global business and any
# organizations with 2FA enforced get the 2FA requirement disabled.
class DisableTwoFactorRequirementJob < ApplicationJob
  areas_of_responsibility :admin_experience

  queue_as :disable_two_factor_requirement

  RETRYABLE_ERRORS = [
    ActiveRecord::ConnectionTimeoutError,
    ActiveRecord::QueryCanceled,
  ].freeze

  RETRYABLE_ERRORS.each do |error|
    retry_on(error) do |job, error|
      Failbot.report(error)
    end
  end

  def perform
    return unless GitHub.enterprise?
    return if GitHub.auth.two_factor_org_requirement_allowed?

    if GitHub.global_business.two_factor_requirement_enabled?
      GitHub.global_business.disable_two_factor_required \
        log_event: true, actor: User.ghost
    end

    orgs_with_two_factor_requirement.find_each do |org|
      org.disable_two_factor_requirement log_event: true, actor: User.ghost
    end
  rescue NameError => e
    Failbot.report(e)
  end

  def orgs_with_two_factor_requirement
    organization_ids = Configuration::Entry.where(
      name: Configurable::TwoFactorRequired::KEY,
      value: Configuration::TRUE, target_type: "User"
    ).pluck(:target_id)
    Organization.with_ids(organization_ids)
  end
end
