# frozen_string_literal: true

RepositoryDependencyManifestChangedJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepositoryDependencyManifestChanged)
