# frozen_string_literal: true

class UpdateBillingTransactionStatusJob < ApplicationJob
  areas_of_responsibility :gitcoin

  queue_as :billing

  def perform(transaction)
    transaction.update_status_from_processor
  end
end
