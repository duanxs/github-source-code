# frozen_string_literal: true

StafftoolsReportJob = LegacyApplicationJob.wrap(GitHub::Jobs::StafftoolsReport)
