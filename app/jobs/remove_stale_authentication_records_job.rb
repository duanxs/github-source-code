# frozen_string_literal: true

class RemoveStaleAuthenticationRecordsJob < ApplicationJob

  queue_as :remove_stale_authentication_records
  locked_by key: ApplicationJob::DEFAULT_LOCK_PROC, timeout: 1.day
  schedule interval: 5.minutes, condition: -> { GitHub.sign_in_analysis_enabled? }

  REMOVE_AFTER_DURATION = 9.months
  BATCH_SIZE = 100

  REMOVAL_CONDITIONS = <<-SQL
    created_at < :after
  SQL

  def perform
    authentication_records = AuthenticationRecord.where(REMOVAL_CONDITIONS, {
      after: REMOVE_AFTER_DURATION.ago
    }).limit(BATCH_SIZE)

    loop do
      deleted_records = AuthenticationRecord.throttle_with_retry(max_retry_count: 8) do
        authentication_records.delete_all
      end
      GitHub.dogstats.count("account_security.authentication_records", deleted_records, tags: ["action:destroy", "explanation:stale"])
      break if deleted_records < BATCH_SIZE
    end
  end
end
