# frozen_string_literal: true

NewsletterDeliveryJob = LegacyApplicationJob.wrap(GitHub::Jobs::NewsletterDelivery)
