# frozen_string_literal: true

class RefreshSponsorsMembershipsCriteriaJob < ApplicationJob
  queue_as :sponsors_application_processing

  schedule interval: 24.hours, condition: -> { GitHub.sponsors_enabled? }

  def perform
    GitHub::SQL::Readonly.new(membership_id_iterator.batches).each do |rows|
      memberships = SponsorsMembership.where(id: rows.flatten)
      bulk_results = SponsorsCriterion::Automated.bulk_results_for(memberships: memberships)

      results = SponsorsCriterion::Automated::BulkWriter.call(
        bulk_results: bulk_results,
        throttle_writes: true,
      )

      # Log failed writes to Haystack for further investigation
      failed_results = results.reject(&:success?)
      failed_results.each do |result|
        Failbot.report(
          RefreshCriteriaFailure.new(
            "Failed to refresh criteria for membership (#{result.sponsors_membership.id})",
          ),
          app: "github-sponsors",
          sponsors_membership_id: result.sponsors_membership.id,
          errors: result.errors,
        )
      end
    end
  end

  class RefreshCriteriaFailure < StandardError; end

  private

  def membership_id_iterator
    iterator = SponsorsMembership.github_sql_batched(start: 0, limit: 100)
    iterator.add <<-SQL
          SELECT id
          FROM sponsors_memberships
          WHERE id > :last
          AND state = 0
          ORDER BY id
          LIMIT :limit
    SQL
    iterator
  end
end
