# frozen_string_literal: true

UserWatchRepoJob = LegacyApplicationJob.wrap(GitHub::Jobs::UserWatchRepo)
