# frozen_string_literal: true

NewsiesAutoSubscribeJob = LegacyApplicationJob.wrap(GitHub::Jobs::NewsiesAutoSubscribe)
