# frozen_string_literal: true

UpdateNetworkMediaBlobsJob = LegacyApplicationJob.wrap(GitHub::Jobs::UpdateNetworkMediaBlobs)
