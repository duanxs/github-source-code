# frozen_string_literal: true

class DisableRepositoryInteractionLimitsJob < ApplicationJob
  areas_of_responsibility :community_and_safety
  queue_as :disable_repository_interaction_limits

  def perform(owner_id, actor_id)
    return unless GitHub.interaction_limits_enabled?

    owner = User.find(owner_id)
    actor = User.find(actor_id)

    owner.repositories.public_scope.each do |repo|
      RepositoryInteractionAbility.disable_all_for(:repository, repo, actor)
    end
  end
end
