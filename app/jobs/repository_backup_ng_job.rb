# frozen_string_literal: true

RepositoryBackupNgJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepositoryBackupNg)
