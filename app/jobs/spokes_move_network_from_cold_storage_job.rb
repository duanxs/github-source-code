# frozen_string_literal: true

SpokesMoveNetworkFromColdStorageJob = LegacyApplicationJob.wrap(GitHub::Jobs::DgitMoveNetworkFromColdStorage)
