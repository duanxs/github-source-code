# frozen_string_literal: true

RemoveOutsideCollaboratorFromOrganizationJob = LegacyApplicationJob.wrap(GitHub::Jobs::RemoveOutsideCollaboratorFromOrganization)
