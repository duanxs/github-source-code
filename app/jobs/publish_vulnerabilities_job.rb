# frozen_string_literal: true

class PublishVulnerabilitiesJob < ApplicationJob
  areas_of_responsibility :security_advisories
  queue_as :publish_vulnerability

  MAX_NUM_VULNS_TO_PUBLISH=20

  def perform(vulnerabilities = collect_vulnerabilities)
    return nil if vulnerabilities.empty?
    vulnerability = vulnerabilities.shift
    publish_vulnerability(vulnerability)

    requeue_in_a_minute(vulnerabilities) unless vulnerabilities.empty?
    vulnerability
  end

  def publish_vulnerability(vulnerability)
    vulnerability.publish(published_by: User.ghost)
    vulnerability.process_alerts
  rescue Vulnerability::PublishVulnerabilityError => e
    # https://haystack.githubapp.com/github/?q=queue:"publish_vulnerability"
    Failbot.report(e, app: "github")
  end

  def requeue_in_a_minute(vulnerabilities)
    PublishVulnerabilitiesJob.set(wait: 1.minute).perform_later(vulnerabilities)
  end

  def collect_vulnerabilities
    Vulnerability.status(:pending)
      .order(created_at: :asc)
      .limit(MAX_NUM_VULNS_TO_PUBLISH)
      .to_a
  end
end
