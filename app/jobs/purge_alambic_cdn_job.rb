# frozen_string_literal: true

PurgeAlambicCdnJob = LegacyApplicationJob.wrap(GitHub::Jobs::PurgeAlambicCdn)
