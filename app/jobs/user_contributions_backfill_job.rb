# frozen_string_literal: true

UserContributionsBackfillJob = LegacyApplicationJob.wrap(GitHub::Jobs::UserContributionsBackfill)
