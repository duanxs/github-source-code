# frozen_string_literal: true

MailchimpSubscribeJob = LegacyApplicationJob.wrap(GitHub::Jobs::MailchimpSubscribe)
