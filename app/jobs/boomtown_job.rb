# frozen_string_literal: true

BoomtownJob = LegacyApplicationJob.wrap(GitHub::Jobs::Boomtown)
