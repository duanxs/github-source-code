# frozen_string_literal: true

SpokesMoveNetworkInDcJob = LegacyApplicationJob.wrap(GitHub::Jobs::DgitMoveNetworkInDc)
