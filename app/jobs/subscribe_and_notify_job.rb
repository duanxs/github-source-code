# frozen_string_literal: true

class SubscribeAndNotifyJob < ApplicationJob
  include Newsies::Reasons

  queue_as :subscribe_and_notify

  # Number of users we want to subscribe in single throttle block
  BATCH_SIZE = 10

  DOG_STATS_PREFIX = "active_job.subscribe_and_notify_job".freeze

  # Discard the job if the subject or author are deleted before the job runs
  discard_on ActiveRecord::RecordNotFound

  # This receives a subject and a list of users and teams who were mentioned in
  # it and need to be subscribed. After they are we trigger deliver any
  # notifications that need to be sent to subscribers
  #
  # Notifications have to be sent after all subscribing is done otherwise we
  # are open to race conditions where mentioned users don't get notified. That's
  # why we're doing both things in the same job here.
  #
  # subject - The mentionable object that triggered this job
  # opts - A hash containing any of the following:
  #   subscriber_reasons_and_ids - A Hash mapping Symbol reasons to Arrays of subscriber IDs
  #   mentioned_user_ids - An array of IDs representing User objects
  #   mentioned_team_ids - An array of IDs representing Team objects
  #   previous_body - The body as it existed in previous_changes before queueing this job
  #   deliver_notifications - Whether or not we should call `#deliver_notifications`
  #   author - A User object of the user who made the change that queued this job, or nil
  #   author_subscribe_reason - The reason to use for the author's subscription, author won't be subscribed if not provided
  #
  def perform(subject, opts = {})
    GitHub.dogstats.increment(DOG_STATS_PREFIX, tags: [
      "subject_type:#{subject.class.to_s.underscore}",
      "has_subscribers:#{opts[:subscriber_reasons_and_ids].present?}",
      "has_mentioned_users:#{opts[:mentioned_user_ids].present?}",
      "has_mentioned_teams:#{opts[:mentioned_team_ids].present?}",
      "has_author:#{opts[:author].present?}",
      "has_author_subscribe_reason:#{opts[:author_subscribe_reason].present?}",
      "has_previous_body:#{opts[:previous_body].present?}",
      "deliver_notifications:#{opts[:deliver_notifications].present?}",
    ])

    # In some cases, users may need to be explicitly subscribed for reasons
    # other than being the author of the subject or being mentioned in its body.
    # For example, a security advisory created on a repository requires that a
    # particular set of users that's equipped to deal with the advisory is
    # automatically subscribed and notified. Passing the reason(s) and
    # subscriber IDs to this job allows us to move a potentially expensive
    # mass subscription operation out of the request and into the background.
    if opts[:subscriber_reasons_and_ids].present?
      opts[:subscriber_reasons_and_ids].each do |reason, user_ids|
        valid_reason = valid_reason_from(reason)
        next unless valid_reason

        users = User.where(id: user_ids)

        if users.any?
          users.each_slice(BATCH_SIZE) do |batched_users|
            Newsies::ThreadSubscription.throttle { subject.subscribe_all(batched_users, valid_reason) }
          end
        end
      end
    end

    mentioned_users = load_mentioned_users(subject, opts)
    mentioned_users.each_slice(BATCH_SIZE) do |batched_users|
      Newsies::ThreadSubscription.throttle { subject.subscribe_mentioned(batched_users, opts[:author]) }
    end

    mentioned_teams = load_mentioned_teams(subject, opts)
    subscribe_mentioned_teams(subject, mentioned_teams, opts[:author]) if mentioned_teams.count

    if opts[:author].present? && opts[:author_subscribe_reason].present?
      Newsies::ThreadSubscription.throttle { subject.subscribe(opts[:author], opts[:author_subscribe_reason].to_sym) }
    end

    if opts[:previous_body]
      update_subscribed_mentions(subject, opts[:previous_body], opts[:deliver_notifications])
    end

    # Don't fire notifications again if we've already sent them in #update_subscribed_mentions
    if opts[:deliver_notifications] && opts[:previous_body].nil?
      subject.deliver_notifications(direct_mention_user_ids: mentioned_users.map(&:id))
    end
  end

  private

  def load_mentioned_users(subject, opts)
    if opts[:mentioned_user_ids].present?
      User.where(id: opts[:mentioned_user_ids])
    elsif opts[:load_mentioned_users]
      subject.mentioned_users
    else
      []
    end
  end

  def load_mentioned_teams(subject, opts)
    if opts[:mentioned_team_ids].present?
      Team.where(id: opts[:mentioned_team_ids])
    elsif opts[:load_mentioned_teams]
      subject.mentioned_teams
    else
      []
    end
  end

  # Subscribe a list of mentioned teams to the given subject.
  #
  # subject - The subscribale subject
  # mentions - Array of teams, or Team scope, that were mentioned.
  # author - The subject's author
  #
  # Returns nothing.
  def subscribe_mentioned_teams(subject, mentioned_teams, author = nil)
    author ||= subject.respond_to?(:user) ? subject.user : nil
    return if author && author.spammy?

    GitHub.dogstats.histogram("#{DOG_STATS_PREFIX}.mentioned_teams", mentioned_teams.size)
    GitHub.dogstats.time("#{DOG_STATS_PREFIX}.subscribe_mentioned_teams") do
      notifications_thread = subject.notifications_thread
      mentioned_teams.each do |mentioned_team|
        team_members = notifications_thread.subscribable_team_members(mentioned_team)
        GitHub.dogstats.histogram("#{DOG_STATS_PREFIX}.mentioned_team.members", team_members.size)
        GitHub.dogstats.time("#{DOG_STATS_PREFIX}.subscribe_mentioned_team") do
          team_members.each_slice(BATCH_SIZE) do |batched_team_members|
            Newsies::ThreadSubscription.throttle do
              notifications_thread.subscribe_all(batched_team_members, "team-mentioned")
            end
          end
        end
      end
    end

    nil
  end

  # Updates the mentioned subscribers for a given subject and its changed body.
  #
  # subject - A mentionable subject
  # previous_body - Array containing the old and new body
  #
  # Returns nothing
  def update_subscribed_mentions(subject, previous_body, deliver_notifications)
    old_text = previous_body.first || subject.body_was
    new_text = previous_body.second || subject.body
    body_context = subject.async_body_context.sync
    mention_diff_helper = GitHub::MentionDiff.new(old_text, new_text, body_context)

    modify_subscriptions(subject, mention_diff_helper)

    if deliver_notifications.nil? || deliver_notifications
      author_id = subject.user_id

      direct_mention_user_ids = direct_mention_ids_to_notify(author_id, mention_diff_helper)
      unless direct_mention_user_ids.empty?
        GitHub.newsies.trigger(
          subject,
          recipient_ids: direct_mention_user_ids,
          direct_mention_user_ids: direct_mention_user_ids,
          event_time: subject.updated_at,
        )
      end

      team_mention_user_ids = team_mention_ids_to_notify(author_id, mention_diff_helper)
      unless team_mention_user_ids.empty?
        GitHub.newsies.trigger(
          subject,
          recipient_ids: team_mention_user_ids,
          reason: "team-mentioned",
          event_time: subject.updated_at,
        )
      end
    end

    nil
  end

  # Returns a list of user ids that should get notifified because of a new direct mention.
  #
  # author_id - Id of the subject's author
  # mention_diff_helper - A GitHub::MentionDiff instance
  #
  # Returns an Array of user ids
  def direct_mention_ids_to_notify(author_id, mention_diff_helper)
    mention_diff_helper.added_users.map(&:id) - [author_id]
  end

  # Returns a list of user ids that should get notifified because of a new team mention.
  #
  # author_id - Id of the subject's author
  # mention_diff_helper - A GitHub::MentionDiff instance
  #
  # Returns an Array of user ids
  def team_mention_ids_to_notify(author_id, mention_diff_helper)
    added_team_mention_ids = mention_diff_helper.added_teams.map(&:id)
    member_ids_of_team_mentions = Team.members_of(added_team_mention_ids, immediate_only: false).pluck(:id)
    # We only want to consider mentioned members that haven't been mentoned directly
    ids_to_ignore = mention_diff_helper.added_users.map(&:id) + [author_id]

    member_ids_of_team_mentions - ids_to_ignore
  end

  # Helper method to update subscriptions for a given subject after changing mentions.
  #
  # subject - A mentionable subject
  # mention_diff_helper - A GitHub::MentionDiff instance
  #
  # Returns an Array of user ids
  def modify_subscriptions(subject, mention_diff_helper)
    mention_diff_helper.added_users.each_slice(BATCH_SIZE) do |batched_users|
      Newsies::ThreadSubscription.throttle { subject.subscribe_mentioned(batched_users) }
    end

    teams = mention_diff_helper.added_teams.uniq.compact
    subscribe_mentioned_teams(subject, teams)

    subject.unsubscribable_users(mention_diff_helper.removed_users).each_slice(BATCH_SIZE) do |batched_users|
      Newsies::ThreadSubscription.throttle do
        batched_users.each do |user|
          subject.unsubscribe(user)
        end
      end
    end

    nil
  end
end
