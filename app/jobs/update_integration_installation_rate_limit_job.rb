# frozen_string_literal: true

UpdateIntegrationInstallationRateLimitJob = LegacyApplicationJob.wrap(GitHub::Jobs::UpdateIntegrationInstallationRateLimit)
