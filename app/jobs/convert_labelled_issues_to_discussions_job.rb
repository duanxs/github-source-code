# frozen_string_literal: true

# Public: Allows bulk conversion of issues to discussions by converting all open issues
# with a specified label.
class ConvertLabelledIssuesToDiscussionsJob < ApplicationJob
  queue_as :convert_labelled_issues_to_discussions

  # Public: Perform the Issue => Discussion conversion.
  #
  # actor - the current User who initiated the conversion
  # label - a Label to use in filtering open Issues to be converted
  # category - an optional DiscussionCategory to place newly created Discussions
  #
  # Returns nothing.
  def perform(actor, label, category: nil)
    issues = label.convertable_issues(actor)

    if issues.any?
      # TODO: create audit log entry about converting this label's issues into discussions
    end

    issues.each do |issue|
      convert_issue_to_discussion(issue, actor: actor, category: category)
    end
  end

  private

  def convert_issue_to_discussion(issue, actor:, category:)
    converter = IssueToDiscussionConverter.new(issue, actor: actor, category: category)
    return false unless converter.prepare_for_conversion
    converter.finish_conversion
  end
end
