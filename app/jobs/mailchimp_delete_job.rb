# frozen_string_literal: true

MailchimpDeleteJob = LegacyApplicationJob.wrap(GitHub::Jobs::MailchimpDelete)
