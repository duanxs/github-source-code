# frozen_string_literal: true

MailchimpDataQualitySyncJob = LegacyApplicationJob.wrap(GitHub::Jobs::MailchimpDataQualitySync)
