# frozen_string_literal: true

class CodespacesOnboardWaitlistUserJob < ApplicationJob
  queue_as :mailers

  locked_by timeout: 1.hour, key: DEFAULT_LOCK_PROC

  def perform(membership)
    return unless membership.member.workspaces_enabled?

    CodespacesMailer.waitlist_acceptance(membership).deliver_now
  end
end
