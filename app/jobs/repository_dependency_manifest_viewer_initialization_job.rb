# frozen_string_literal: true

# A special-case strategy for Manifest Initilization that only detects manifests
# without triggering Dependabot or Security Alert actions.
#
# This is used by the network/dependencies page to dynamically backfill missing
# manifests when user views that page.
#
# Since this is GET request from a public user, it is somewhat unsafe to trigger
# actions like installing Dependabot or raising Vulnerability Alerts. It leaves
# us vulnerable to web crawlers indexing very old repositories triggering actions
# that spam Repository Owners or consume compute resources.
class RepositoryDependencyManifestViewerInitializationJob < RepositoryDependencyManifestInitializationJob
  areas_of_responsibility :dependabot
  queue_as :repository_dependencies

  private

  # When a viewer results in manifests being initialized,
  # we should not scan for Dependencies.
  def scan_dependencies_for_vulnerabilities(repository)
    false
  end
end
