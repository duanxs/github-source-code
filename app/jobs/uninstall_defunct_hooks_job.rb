# frozen_string_literal: true

UninstallDefunctHooksJob = LegacyApplicationJob.wrap(GitHub::Jobs::UninstallDefunctHooks)
