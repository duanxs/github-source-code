# frozen_string_literal: true

class InvalidateExpiredInvitesJob < ApplicationJob
  schedule interval: 1.day

  areas_of_responsibility :community_and_safety

  queue_as :invalidate_expired_invites

  BATCH_SIZE = 100

  def perform
    ActiveRecord::Base.connected_to(role: :reading) do
      OrganizationInvitation.recently_expired.find_in_batches(batch_size: BATCH_SIZE) do |batch|
        ActiveRecord::Base.connected_to(role: :writing) do
          batch.each do |invite|
            OrganizationInvitation.throttle do
              begin
                invite.expire
              rescue ActiveRecord::RecordInvalid, NoMethodError
                next
              end
            end
          end
        end
      end
    end
  end
end
