# frozen_string_literal: true

class DependencyGraphManifestBackfillJob < ApplicationJob
  HAYSTACK_BUCKET = "github-dependency-graph"
  MAX_ATTEMPTS = 5

  queue_as :manifest_backfill
  retry_on_dirty_exit

  locked_by timeout: 15.minutes, key: ->(job) {
    job.arguments.first
  }

  # If we can't find a record we can just discard this message.
  discard_on ActiveRecord::RecordNotFound do |job, error|
    Failbot.report(error,
      app: HAYSTACK_BUCKET,
      desired_manifest_type: job.desired_manifest_type,
      job: "manifest_backfill",
      repository_id: job.repository_id,
      retried: false,
    )
  end

  # Queue for a retry if there's a GitRPC timeout.
  retry_on GitRPC::Timeout, attempts: MAX_ATTEMPTS do |job, error|
    Failbot.report(error,
      app: HAYSTACK_BUCKET,
      desired_manifest_type: job.desired_manifest_type,
      job: "manifest_backfill",
      repository_id: job.repository_id,
      retried: true,
    )
  end

  def perform(repository_id, desired_manifest_types = [])
    desired_manifest_types = Array(desired_manifest_types).map(&:to_sym)

    repository = ActiveRecord::Base.connected_to(role: :reading) do
      Repository.find_by(id: repository_id)
    end

    return unless repository.present?
    return unless repository.default_oid
    return if repository.default_oid == GitHub::NULL_OID
    return if repository.archived?
    return unless repository.dependency_graph_enabled?

    # Detect dependency manifests so they'll be cached in the KV, because `dependency_manifest_blobs` looks for 'em there.
    repository.detect_dependency_manifests

    repository.dependency_manifest_blobs.each do |blob|
      # Skip blobs unless they match the desired_manifest_types we're looking for OR we're accepting all manifests
      next unless DependencyManifestFile.corresponding_manifest_type(path: blob.filename).in?(desired_manifest_types) || desired_manifest_types.empty?
      next if blob.content.empty?

      GlobalInstrumenter.instrument "update_manifest.repository", {
        repository_id: repository.id,
        owner_id: repository.owner_id,
        repository_private: repository.private?,
        repository_fork: repository.fork?,
        repository_nwo: repository.name_with_owner,
        repository_stargazer_count: repository.stargazer_count,
        is_backfill: true,
        manifest_file: {
          filename: blob.filename,
          path: blob.path,
          content: blob.content,
          git_ref: repository.default_oid,
          pushed_at: repository.pushed_at.to_i,
        },
      }
    end
  end
end
