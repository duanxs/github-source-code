# frozen_string_literal: true

class FixDuplicateLabelsJob < ApplicationJob
  queue_as :fix_duplicate_labels

  retry_on_dirty_exit

  def perform(repository_id)
    return unless repository = Repository.find_by(id: repository_id)

    fixer = RepositoryLabelFixer.new(repository)
    fixer.fix!
  end
end
