# frozen_string_literal: true

class OrganizationBulkInviteJob < ApplicationJob
  areas_of_responsibility :user_growth
  queue_as :billing

  def perform(actor, organization, members)
    results = {
      errors: [],
      successes: [],
    }

    members.each do |member|
      results = invite_member_with_reporting(actor, organization, member, results)
    end

    instrument_results(actor, organization, results)

    if results[:errors].any?
      AccountMailer.org_invitation_failed(actor, organization, results[:errors].to_json)
        .deliver_later
    end

    return results
  end

  def invite_member_with_reporting(actor, organization, member, results)
    begin
      invitee = invite_member(actor, organization, member)
      results[:successes] << invitee
    rescue OrganizationInvitation::NoAvailableSeatsError, OrganizationInvitation::TradeControlsError,
           OrganizationInvitation::AlreadyAcceptedError, OrganizationInvitation::InvalidError,
           ActiveRecord::RecordInvalid => ex
      results[:errors] << {
        member: member,
        class: ex.class.to_s,
        message: ex.message,
      }

      log_inactionable_error(actor, organization.id, member, ex)
    end

    results
  end

  def invite_member(actor, organization, member)
    if User.valid_email?(member)
      email = member
    else
      invitee = User.find_by_login(member)
    end

    organization.invite(invitee, email: email, inviter: actor, role: :direct_member)

    return invitee.present? ? invitee : email
  end

  def instrument_results(actor, organization, results)
    email_successes = results[:successes].reject { |member| member.respond_to?(:id) }
    successful_ids = results[:successes].select { |member| member.respond_to?(:id) }
      .map(&:id)

    email_errors = results[:errors].reject { |member| member[:member].respond_to?(:id) }.
      map { |m| m[:member] }
    error_ids = results[:errors].select { |member| member[:member].respond_to?(:id) }.
      map { |m| m[:member].id }

    GlobalInstrumenter.instrument(
      "organization.bulk.invite",
      actor: actor,
      organization: organization,
      successful_user_invites: successful_ids,
      successful_email_invites: email_successes,
      failed_user_invites: error_ids,
      failed_email_invites: email_errors,
    )
  end

  # Log generic errors when failed invites are not created
  def log_inactionable_error(actor, organization_id, member, error)
      return unless [OrganizationInvitation::AlreadyAcceptedError, OrganizationInvitation::InvalidError, ActiveRecord::RecordInvalid].include?(error.class)

      GitHub::Logger.log_exception({
        job: "OrganizationBulkInviteJob",
        method: "invite_member_with_reporting",
        actor: actor,
        member: member,
        organization_id: organization_id,
        error_class: error.class.to_s,
        error_message: error.message,
      }, error)
  end
end
