# frozen_string_literal: true

class SubmitEventToOctolyticsJob < ApplicationJob
  areas_of_responsibility :analytics
  queue_as :analytics

  retry_on Octolytics::ServerError, wait: :exponentially_longer, attempts: 10

  BATCH_SIZE = 150

  def perform(event_id)
    event.to_octolytics.map(&method(:prepare_octolytics_event)).each_slice(BATCH_SIZE) do |octolytics|
      GitHub.analytics.record! octolytics

      GitHub::Logger.log(event_octolytics_length: octolytics.length)
      GitHub.dogstats.count(
        "stratocaster_octolytics.octolytics_length",
        octolytics.length,
        tags: all_stats_tags,
      )
    end
  end

  around_perform do |_, block|
    event_context = {
      stratocaster_event_id: event.id,
      stratocaster_event_type: event.event_type,
      stratocaster_event_action: event.action || Stratocaster::Event::NO_ACTION,
      stratocaster_event_sender_id: event.sender_id,
      stratocaster_event_actor_id: event.actor_id,
      stratocaster_event_repo_id: event.repo_id,
    }

    Failbot.push(event_context)
    GitHub::Logger.log_context(event_context) do
      block.call
    end
  end

  private

  def event
    @event ||= GitHub.stratocaster.get(arguments.first)
  end

  def prepare_octolytics_event(raw_octolytics)
    raw_octolytics
      .yield_self(&method(:transcode_to_utf8))
      .yield_self(&method(:enforce_string_length))
      .yield_self(&method(:validate_actor))
  end

  def transcode_to_utf8(event)
    transcoder = Stratocaster::Octolytics::Utf8Transcoder.new(event)
    transcoder.transcode.tap do |transcoded_event|
      transcoded_event[:dimensions][:import_transcoded_to_utf8] = true if transcoder.transcoded_to_utf8
    end
  end

  def enforce_string_length(event)
    policy = Stratocaster::Octolytics::StringLengthPolicy.new(event)
    policy.enforce.tap do |enforced_event|
      enforced_event[:dimensions][:import_string_truncated] = true if policy.truncated
    end
  end

  def validate_actor(event)
    event.tap do
      if (actor_id = event[:dimensions]["actor_id"])
        event[:dimensions][:actor_hash] = GitHub.analytics.generate_actor_hash(actor_id)
      end
    end
  end
end
