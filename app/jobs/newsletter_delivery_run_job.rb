# frozen_string_literal: true

# Sends any newsletters that need to be run
class NewsletterDeliveryRunJob < ApplicationJob
  areas_of_responsibility :explore
  queue_as :newsletter_delivery_run

  LOCK_KEY = "newsletter-delivery-run"
  MAX_JOBS = 70

  locked_by timeout: 1.hour, key: ApplicationJob::DEFAULT_LOCK_PROC

  retry_on GitHub::Restraint::UnableToLock, wait: 5.minutes

  def perform(sub_ids)
    return unless sub_ids.present?

    restraint = GitHub::Restraint.new
    restraint.lock!(LOCK_KEY, MAX_JOBS, 1.hour) do
      GitHub.dogstats.time("newsletter.jobs.deliver_subscriptions") do
        ActiveRecord::Base.connected_to(role: :reading) do
          NewsletterSubscription.deliver_subscriptions(sub_ids: sub_ids)
        end
      end
    end
  end
end
