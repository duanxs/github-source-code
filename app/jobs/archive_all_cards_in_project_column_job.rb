# frozen_string_literal: true

# ArchiveAllCardsInProjectColumnJob does exactly what the class name says:
# it archives all of the ProjectCards belonging to the provided column. Hooray!
#
# Call `ProjectColumn#archive_all_cards` to enqueue the job

class ArchiveAllCardsInProjectColumnJob < ApplicationJob
  areas_of_responsibility :projects

  queue_as :archive_project_cards

  BATCH_SIZE = 100

  def perform(project_column_id, arguments = {})
    start = Time.current
    queued_at = arguments[:queued_at] || start.to_s
    from_id = arguments[:from_id] || 0
    last_id = false

    GitHub.dogstats.timing_since("job.archive_all_cards_in_project_column.time_enqueued", Time.parse(queued_at))

    return unless column = ProjectColumn.find(project_column_id)
    return unless project = column.project

    GitHub.context.push(project_archive_cards_from_job: project.id)
    Audit.context.push(project_archive_cards_from_job: project.id)

    if GitHub.flipper[:archive_cards_idempotent_job].enabled?
      last_id = column.archive_all_cards_in_batches! timestamp: queued_at, from_id: from_id, batch_size: BATCH_SIZE
      return unless last_id

      ArchiveAllCardsInProjectColumnJob.perform_later project_column_id, { queued_at: queued_at, from_id: last_id }
    else
      column.archive_all_cards!
    end

  ensure
    GitHub.dogstats.timing_since("job.archive_all_cards_in_project_column.time", start)
    # unlock here only if all batches were processed or job ran not in batched mode
    project&.unlock(Project::ProjectLock::CARD_ARCHIVING) unless last_id
  end
end
