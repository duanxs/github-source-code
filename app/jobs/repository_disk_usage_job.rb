# frozen_string_literal: true

RepositoryDiskUsageJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepositoryDiskUsage)
