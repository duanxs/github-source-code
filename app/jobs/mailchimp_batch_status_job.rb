# frozen_string_literal: true

MailchimpBatchStatusJob = LegacyApplicationJob.wrap(GitHub::Jobs::MailchimpBatchStatus)
