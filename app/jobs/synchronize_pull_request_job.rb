# frozen_string_literal: true

class SynchronizePullRequestJob < ApplicationJob
  queue_as :synchronize_pull_request

  retry_on PullRequest::DetermineCodeownersError, attempts: 1

  discard_on ActiveRecord::RecordNotFound do |_, error|
    Failbot.report(error)
  end

  def perform(pull_request_id:, user:, installation:, repo:, forced:, ref:, before:, after:)
    if user && installation
      user.installation = installation
    end

    pull_request = PullRequest.find(pull_request_id)
    pull_request.synchronize!(
      user: user,
      repo: repo,
      forced: forced,
      ref: ref,
      before: before,
      after: after,
    )
  end
end
