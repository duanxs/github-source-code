# frozen_string_literal: true

class UpdateLockedRepositoriesJob < ApplicationJob
  queue_as :critical
  retry_on_dirty_exit

  def perform(user_id)
    GitHub.dogstats.increment "update_locked_repositories"
    GitHub::Logger.log(job: "UpdateLockedRepositories",
                       at: "start",
                       user_id: user_id)
    if user = User.where(id: user_id).first
      user.update_locked_repositories(log_as_part_of_job: "UpdateLockedRepositories")
    end
  end
end
