# frozen_string_literal: true

ConvertToOutsideCollaboratorJob = LegacyApplicationJob.wrap(GitHub::Jobs::ConvertToOutsideCollaborator)
