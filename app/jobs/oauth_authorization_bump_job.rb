# frozen_string_literal: true

class OauthAuthorizationBumpJob < ApplicationJob
  areas_of_responsibility :ecosystem_apps

  queue_as :oauth_authorization_bumps

  def perform(id, unix_timestamp)
    time = Time.at(unix_timestamp)
    now = Time.now.to_i
    valid_until = unix_timestamp + OauthAuthorization::ACCESS_THROTTLING
    ttl = valid_until - now

    # Only set the memcached lock if the ttl would be greater than 0. 0 is
    # default ttl and means forever, which we don't want. Less than zero
    # means the key would be of no use.
    if ttl > 0
      return unless GitHub.cache.add(write_prevention_cache_key(id), time, ttl)
    end

    ActiveRecord::Base.connected_to(role: :reading) do
      if oauth_authorization = OauthAuthorization.find_by_id(id)
        oauth_authorization.bump!(time)
      end
    end
  end

  def write_prevention_cache_key(id)
    "github:jobs:oauth_authorization_bump:#{id}"
  end
end
