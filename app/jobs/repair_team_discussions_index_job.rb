# frozen_string_literal: true

RepairTeamDiscussionsIndexJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepairTeamDiscussionsIndex)
