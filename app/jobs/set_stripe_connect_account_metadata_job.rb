# frozen_string_literal: true

require "stripe"

class SetStripeConnectAccountMetadataJob < ApplicationJob
  queue_as :stripe

  RETRYABLE_ERRORS = [
    Stripe::APIConnectionError,
    Stripe::OAuth::InvalidRequestError,
    Stripe::StripeError,
  ].freeze

  retry_on(*RETRYABLE_ERRORS) do |job, error|
    account = job.arguments.first
    Failbot.report(error,
      sponsors_listing_id: account.payable_id,
      stripe_connect_account_id: account.id,
      app: "github-external-request",
    )
  end

  def perform(stripe_connect_account)
    return unless GitHub.sponsors_enabled?

    membership = stripe_connect_account.payable.sponsorable.sponsors_membership

    url = Rails.application.routes.url_helpers.stafftools_sponsors_find_membership_url(
      id: membership.id,
      host: GitHub.host_name,
    )

    response = Stripe::Account.update(stripe_connect_account.stripe_account_id, {
      metadata: {
        stafftools_url: url,
      },
    })

    stripe_connect_account.update!(stripe_account_details: response.as_json)
  end
end
