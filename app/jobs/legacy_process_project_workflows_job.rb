# frozen_string_literal: true

require "github/hash_lock"

class LegacyProcessProjectWorkflowsJob < ApplicationJob
  class WorkflowError < StandardError; end

  queue_as :project_workflows

  retry_on ProjectWorkflow::MissingIssueObject, wait: :exponentially_longer, attempts: 3

  locked_by timeout: 1.hour, key: ->(job) {
    trigger_type = job.arguments[0]
    trigger_attributes = job.arguments[1]
    key = [trigger_type] + trigger_attributes.stringify_keys.slice("actor_id", "issue_id", "project_card_id").to_a
    key.flatten.map(&:to_s).join(",")
  }

  def perform(trigger_type, trigger_attributes = {})
    start = Time.current
    tags = ["trigger_type:#{trigger_type}"]

    unless ProjectWorkflow::EXTERNAL_TRIGGERS.include?(trigger_type)
      GitHub.dogstats.increment("job.process_project_workflows.invalid_trigger_type", tags: tags)
      return
    end

    # ActiveJob and Resque handle this differently for hash attributes, so we need to try both
    # versions here.
    actor_id = trigger_attributes[:actor_id] || trigger_attributes["actor_id"]
    issue_id = trigger_attributes[:issue_id] || trigger_attributes["issue_id"]
    project_card_id = trigger_attributes[:project_card_id] || trigger_attributes["project_card_id"]

    unless actor_id
      GitHub.dogstats.increment("job.process_project_workflows.missing_actor_id", tags: tags)
      return
    end
    unless issue_id
      GitHub.dogstats.increment("job.process_project_workflows.missing_issue_id", tags: tags)
      return
    end

    delay_ms = 0
    if trigger_attributes.key?("queued_at") || trigger_attributes.key?(:queued_at)
      queued_at = trigger_attributes["queued_at"] || trigger_attributes[:queued_at]
      queued_at = queued_at.kind_of?(String) ? Time.parse(queued_at) : Time.at(queued_at)
      delay_ms = (start - queued_at) * 1_000
    end

    GitHub.dogstats.timing("job.process_project_workflows.time_enqueued", delay_ms, tags: tags)

    ProjectWorkflow.trigger_workflows(trigger_type, { issue_id: issue_id, actor_id: actor_id, project_card_id: project_card_id })

  rescue ActiveRecord::ActiveRecordError => e
    Failbot.report(WorkflowError.new("trigger_type: #{trigger_type}, issue_id: #{issue_id}, actor_id: #{actor_id}, project_card_id: #{project_card_id}"), app: "github-projects")
  ensure
    GitHub.dogstats.timing("job.process_project_workflows.time", (Time.current - start) * 1000, tags: tags)
  end
end
