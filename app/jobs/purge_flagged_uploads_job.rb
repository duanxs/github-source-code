# frozen_string_literal: true

class PurgeFlaggedUploadsJob < ApplicationJob
  areas_of_responsibility :community_and_safety
  queue_as :purge_flagged_uploads

  schedule interval: 1.day, condition: -> { !GitHub.enterprise? }

  def perform
    ActiveRecord::Base.connected_to(role: :writing) do
      PhotoDnaHit.purgeable.in_batches do |hit_batch|
        hit_batch.where.not(uploader_id: uploaders_to_exclude(hit_batch)).each do |hit|
          hit.purge_content!
        end
      end
    end
  end

  private

  def uploaders_to_exclude(batch)
    uploader_ids = batch.pluck(:uploader_id)
    LegalHold.where(user_id: uploader_ids).pluck(:user_id)
  end
end
