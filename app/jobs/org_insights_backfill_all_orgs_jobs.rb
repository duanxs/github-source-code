# frozen_string_literal: true

class OrgInsightsBackfillAllOrgsJobs < ApplicationJob
    extend GitHub::HashLock

    queue_as :org_insights_backfill

    RETRYABLE_ERRORS = [
      ActiveRecord::ConnectionTimeoutError,
      ActiveRecord::QueryCanceled,
    ].freeze

    RETRYABLE_ERRORS.each do |error|
      retry_on(error) do |job, error|
        Failbot.report(error)
      end
    end

    def perform(metrics: [])
      Organization.where(plan: GitHub::Plan::BUSINESS_PLUS).find_each(batch_size: 10) do |org|
        possible_metrics = GitHub::OrgInsights::Metric.subclasses

        metrics = Array.wrap(metrics).select { |m| possible_metrics.include?(m) }
        if metrics.blank?
          metrics = possible_metrics
        end

        metrics.each do |metric_classname|
          mapped_class = GitHub::OrgInsights::Metric[metric_classname]
          GitHub::OrgInsights::Backfill.new(org.id, mapped_class).backfill
        end
      end
    rescue NameError => e
      Failbot.report(e)
    end
end
