# frozen_string_literal: true

require "github/hash_lock"

# LegacyApplicationJob exists to provide an ActiveJob compatible interface to
# legacy RockQueue job classes.
#
# This code should be deleted when RockQueue is no longer supported.
class LegacyApplicationJob < ApplicationJob

  # Create ActiveJob compatible class.
  #
  # Examples
  #
  #   FooJob = LegacyApplicationJob.wrap(GitHub::Jobs::Foo)
  #
  def self.wrap(klass, &blk)
    Class.new(LegacyApplicationJob) do
      if klass.respond_to?(:queue)
        queue_as do
          klass.queue.to_s
        end
      end

      define_singleton_method :rock_queue_class do
        klass
      end

      delegated_singleton_methods = [:enabled?, :enqueue, :clear_all_locks, :areas_of_responsibility]

      delegated_singleton_methods.each do |method|
        if klass.respond_to?(method)
          singleton_class.delegate method, to: :rock_queue_class
        end
      end

      if klass.respond_to?(:schedule_options) && klass.schedule_options.present?
        interval = klass.schedule_options[:interval]
        scope = klass.schedule_options[:scope]

        condition = if klass.respond_to?(:enabled?)
          ->() { klass.enabled? }
        end

        schedule(interval: interval, scope: scope, condition: condition)
      end

      # If it is extended by HashLock
      if klass.singleton_class < GitHub::HashLock
        locked_by key: ->(job) { klass.lock(*job.arguments) }, timeout: klass.lock_timeout
      end

      instance_exec(&blk) if blk
    end.tap do |active_job_klass|
      klass.define_singleton_method(:active_job_class) { active_job_klass }
    end
  end

  # Returns legacy RockQueue job class.
  #
  # Returns GitHub::Jobs::Job.
  def self.rock_queue_class
  end

  # Deprecated: See ApplicationJob.enqueue_once_per_interval.
  def self.enqueue_once_per_interval(args, interval: 60, unique_id: nil)
    unique_id ||= args.join(":")
    run_at = Time.now + interval
    guid = ::Digest::MD5.hexdigest([rock_queue_class.to_s, interval, unique_id].join(":"))
    tags = ["class:#{rock_queue_class.name.underscore}"]
    if GitHub.timed_resque.retry_at(rock_queue_class, args, run_at, 0, guid)
      GitHub.dogstats.increment("job.once_per_interval.queued", tags: tags)
      true
    else
      GitHub.dogstats.increment("job.once_per_interval.duplicate", tags: tags)
      false
    end
  end

  # Deprecated: Use ActiveJob retry_on instead.
  def self.retry_later(job_class, job_args, options = nil)
    guid    = options ? options[:guid] : nil
    retries = options ? options[:retries].to_i : 0
    if retries > 20
      GitHub.dogstats.increment("job.retry_later.retries_exceeded", tags: [
        "class:#{job_class.name.underscore}",
        "retries:#{retries}",
      ])
      return false
    end
    retries = 0 if retries < 0

    retry_at = if retries < 1
      Time.now.utc
    else
      Time.now.utc + retries ** 3 + rand(-retries..retries)
    end

    if self.retry_at(job_class.rock_queue_class, job_args, retry_at, retries: retries, guid: guid)
      GitHub.dogstats.increment("job.retry_later.queued", tags: [
        "class:#{job_class.name.underscore}",
        "retries:#{retries}",
      ])
      return true
    end
    false
  end

  def self.retry_at(job_class, job_args, at, options = {})
    retries = options.fetch(:retries, 0).to_i
    guid = options.fetch(:guid, nil)

    publish_path = nil
    if options.has_key?(:retry_to_hydro)
      if options[:retry_to_hydro]
        publish_path = TimedResque::BufferedTimedResque::HYDRO_PUBLISH_PATH
      else
        publish_path = TimedResque::BufferedTimedResque::REDIS_PUBLISH_PATH
      end
    end

    queued = TimedResque::BufferedTimedResque.retry_at(
      job_class,
      job_args,
      at,
      queue: job_class.queue,
      retries: retries,
      guid: guid,
      publish_path: publish_path,
    )

    if queued
      GitHub.dogstats.increment("job.retry_at.queued", tags: [
        "class:#{job_class.name.underscore}",
        "retries:#{retries}",
      ])
    end

    queued
  end

  # Delegate to RockQueue's perform.
  def perform(*args)
    self.class.rock_queue_class.perform(*args)
  end
end
