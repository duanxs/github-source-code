# frozen_string_literal: true

class RollEarthsmokeKeys < ApplicationJob
  schedule interval: 1.week, condition: -> { GitHub.earthsmoke_enabled? }

  queue_as :roll_earthsmoke_keys

  def self.key_names
    @key_names ||= []
  end

  def perform
    self.class.key_names.each { |name| roll_key(name) }
  end

  def roll_key(name)
    GitHub.earthsmoke.key(name).roll(timeout: 10)
  rescue ::Earthsmoke::Error
    # nothing to do...
  end
end
