# frozen_string_literal: true

RepairIssuesIndexJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepairIssuesIndex)
