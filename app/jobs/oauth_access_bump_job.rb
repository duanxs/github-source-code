# frozen_string_literal: true

OauthAccessBumpJob = LegacyApplicationJob.wrap(GitHub::Jobs::OauthAccessBump)
