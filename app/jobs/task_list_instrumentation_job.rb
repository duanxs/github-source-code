# frozen_string_literal: true

class TaskListInstrumentationJob < ApplicationJob
  queue_as :task_list_instrumentation
  retry_on_dirty_exit

  def perform(actor_id, repository_id, comment_type)
    GitHub.dogstats.time "task_list_instrumentation" do
      Failbot.push(
        job: self.class.name,
        actor_id: actor_id,
        repository_id: repository_id,
        comment_type: comment_type,
      )

      actor = User.find_by_id(actor_id)
      repository = Repository.find_by_id(repository_id)
      return unless actor && repository

      event_payload = octolytics_event_payload(actor, repository, comment_type)

      GitHub.analytics.record(
        "task_list_reordered",
        event_payload[:dimensions],
        event_payload[:measures],
        event_payload[:context],
      )
    end
  end

  private

  def octolytics_event_payload(actor, repository, comment_type)
    Issues::CommentInstrumentationPayload.new(actor: actor, repository: repository, comment_type: comment_type).payload
  end
end
