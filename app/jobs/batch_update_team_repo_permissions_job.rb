# frozen_string_literal: true

# Updates org teams permissions/roles and deletes custom role if present and
# has not dependencies
#
# teams_repos       - Active records of team/permission or team/role association
# action_id         - Organisation admin
# action            - The permission to update to. Can be :read, :write, :admin, :triage, :maintain, or a custom role
# role              - Custom role. default: nil
# context           - Optional Hash of Strings with the teams' previous Role {:old_permission, :old_base_role}
#
# Returns nothing
class BatchUpdateTeamRepoPermissionsJob < ApplicationJob
  queue_as :update_team_repo_permissions

  JOB_SUCCESS_STATUS = [::Team::ModifyRepositoryStatus::SUCCESS, ::Team::ModifyRepositoryStatus::DUPE]

  def perform(teams_repos, actor_id, action:, role: nil, context: {})
    GitHub.context.push(actor_id: actor_id) if actor_id
    teams_repos.each do |team_repo|
      team = team_repo.actor
      repo = team_repo.try(:subject) || team_repo.try(:target)
      team.check_valid_action(action, repo)
      team_modify_repo_status = team.update_repository_permission(repo, action, context: context)
      raise RuntimeError.new(team_modify_repo_status) unless JOB_SUCCESS_STATUS.include?(team_modify_repo_status)
    end

    if !role.nil? || role&.all_dependencies_updated?
      GitHub.dogstats.increment("orgs_roles.delete.queued_to_delete", tags: ["status:deleted"]) if role.destroy!
    end
  end
end
