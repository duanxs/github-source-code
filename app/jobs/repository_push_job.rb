# frozen_string_literal: true

RepositoryPushJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepositoryPush)
