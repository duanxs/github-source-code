# frozen_string_literal: true

class ExportSponsorshipsJob < ApplicationJob
  queue_as :sponsorships_export

  locked_by timeout: 1.hour, key: ->(job) { job.arguments[0].id }

  class SponsorshipsExportError < StandardError; end

  def perform(sponsorable, year:, month:, format:)
    export = SponsorsListing::SponsorshipsExport.new(
      sponsors_listing: sponsorable.sponsors_listing,
      year: year,
      month: month,
      format: format,
    )

    if export.valid?
      SponsorsMailer.sponsorships_export(
        sponsorable: sponsorable,
        filename: export.filename,
        mime_type: export.mime_type,
        export_content: export.fetch_content,
        export_year: export.year,
        export_month: export.month,
      ).deliver_later
    else
      err = "There was an error starting the export: #{export.errors.full_messages.to_sentence}"

      Failbot.report(
        SponsorshipsExportError.new(err),
        app: "github-sponsors",
        sponsorable_login: sponsorable.login,
        export_format: export.format,
        export_year: export.year,
        export_month: export.month,
      )
    end
  end
end
