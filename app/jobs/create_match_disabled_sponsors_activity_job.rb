# frozen_string_literal: true

class CreateMatchDisabledSponsorsActivityJob < ApplicationJob
  queue_as :sponsors_application_processing

  attr_reader :listing

  def perform(listing:)
    # set `@listing` so it can be used for `attr_reader` in other methods
    @listing = listing

    return if listing.blank? && !listing.approved?
    return if sponsorable.sponsorship_match_ineligible_from_age_or_spamminess?
    return if matchable_listings.blank?

    match_disabled_activity_attrs.each do |activity_attrs|
      ::SponsorsActivity.create!(activity_attrs)
    end
  end

  private

  def sponsorable
    @sponsorable ||= listing.sponsorable
  end

  def matchable_listings
    @matchable_listings ||= begin
      sponsorable_ids = sponsorable.sponsorships_as_sponsor.active.pluck(:sponsorable_id)
      listings = SponsorsListing.
        includes(:sponsorable, :sponsors_membership).
        where(
          sponsorable_type: ::User.name,
          sponsorable_id: sponsorable_ids,
        )
      listings.select(&:matchable?)
    end
  end

  def matchable_tier_ids_by_listing_id
    @matchable_tier_ids_by_listing_id ||= begin
      tier_ids = sponsorable.sponsorships_as_sponsor.active.where(
        sponsorable_type: ::User.name,
        sponsorable_id: matchable_listings.pluck(:sponsorable_id),
      ).pluck(:subscribable_id)

      SponsorsTier.where(
        id: tier_ids,
      ).pluck(:sponsors_listing_id, :id).to_h
    end
  end

  def match_disabled_activity_attrs_for(matched_listing)
    {
      timestamp: Time.at(listing.published_at),
      sponsorable_id: matched_listing.sponsorable_id,
      sponsorable_type: ::User.name,
      sponsor_id: sponsorable.id,
      sponsor_type: ::User.name,
      sponsors_tier_id: matchable_tier_ids_by_listing_id[matched_listing.id],
      action: :sponsor_match_disabled,
    }
  end

  def match_disabled_activity_attrs
    @match_disabled_activity_attrs ||= matchable_listings.map do |matchable_listing|
      match_disabled_activity_attrs_for(matchable_listing)
    end
  end
end
