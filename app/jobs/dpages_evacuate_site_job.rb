# frozen_string_literal: true

DpagesEvacuateSiteJob = LegacyApplicationJob.wrap(GitHub::Jobs::DpagesEvacuateSite)
