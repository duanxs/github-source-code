# frozen_string_literal: true

UpdateMarketplaceOrderPreviewJob = LegacyApplicationJob.wrap(GitHub::Jobs::UpdateMarketplaceOrderPreview)
