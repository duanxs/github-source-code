# frozen_string_literal: true

RepositoryUpdateGraphJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepositoryUpdateGraph)
