# frozen_string_literal: true

AssignStafftoolsRoleJob = LegacyApplicationJob.wrap(GitHub::Jobs::AssignStafftoolsRoleJob)
