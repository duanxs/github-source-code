# frozen_string_literal: true

class LegacyStaleChecksScheduledJob < ApplicationJob
  areas_of_responsibility :ce_extensibility
  queue_as :stale_check_runs

  schedule interval: 3.hours

  def perform
    return if GitHub.flipper[:k8s_stale_check_jobs].enabled?
    ActiveRecord::Base.connected_to(role: :reading) do
      CheckSuite
        .incomplete_and_older_than_stale_threshold
        .find_in_batches(batch_size: 500) do |check_suites|
          GitHub.dogstats.time("stale_checks_scheduled_job.time_per_batch") do
            check_suites.each do |check_suite|
              GitHub.dogstats.increment("stale_checks_scheduled_job.stale_check_suites_found")
              LegacyStaleCheckJob.perform_later(check_suite.id)
            end
          end
      end
    end
  end
end
