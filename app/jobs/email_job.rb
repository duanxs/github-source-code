# frozen_string_literal: true

EmailJob = LegacyApplicationJob.wrap(GitHub::Jobs::Email)
