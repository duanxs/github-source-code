# frozen_string_literal: true

class StaleCodeScanningCheckRunsScheduledJob < ApplicationJob
  queue_as :code_scanning

  schedule interval: 1.hour

  MAX_ENQUEUED_JOBS = 1000

  def perform
    enqueued_check_runs = 0

    ActiveRecord::Base.connected_to(role: :reading) do
      find_repositories do |repositories|
        repositories.each do |repository|
          find_stale_check_suites(repository) do |check_suites|
            check_runs = find_stale_check_runs(check_suites)

            check_run_count = check_runs.size
            enqueued_check_runs += check_run_count
            return if enqueued_check_runs > MAX_ENQUEUED_JOBS

            GitHub.dogstats.count("code_scanning.stale_check_runs", check_run_count)

            check_run_ids = check_runs.map(&:id)
            ActiveRecord::Base.connected_to(role: :writing) do
              repository.refresh_code_scanning_status(check_run_ids: check_run_ids)
            end
          end
        end
      end
    end
  end

  def find_repositories
    limit = 1000
    offset = 0
    loop do
      repository_ids = CheckSuite
        .for_app_id(Apps::Internal.integration_id(:code_scanning))
        .limit(limit)
        .offset(offset)
        .pluck(:repository_id)
      return if repository_ids.empty?
      yield Repository.with_ids(repository_ids)
      offset += limit
    end
  end


  def find_stale_check_suites(repository)
    repository.check_suites
      .for_app_id(Apps::Internal.integration_id(:code_scanning))
      .where.not(status: :completed)
      .where("updated_at < ?", Time.zone.now - 1.hour)
      .find_in_batches(batch_size: 100) do |check_suites|
        yield check_suites
      end
  end

  def find_stale_check_runs(check_suites)
    GitHub::PrefillAssociations.prefill_associations(check_suites, [:check_runs])
    check_suites.map(&:check_runs).flatten.delete_if(&:completed?)
  end
end
