# frozen_string_literal: true

class ExpireBusinessAdminInvitationsJob < ApplicationJob
  schedule interval: 1.day

  areas_of_responsibility :admin_experience

  queue_as :invalidate_expired_invites

  BATCH_SIZE = 100

  def perform
    ActiveRecord::Base.connected_to(role: :reading) do
      BusinessMemberInvitation.recently_expired.find_in_batches(batch_size: BATCH_SIZE) do |batch|
        ActiveRecord::Base.connected_to(role: :writing) do
          batch.each do |invitation|
            BusinessMemberInvitation.throttle do
              invitation.expire
            end
          end
        end
      end
    end
  end
end
