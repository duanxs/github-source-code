# frozen_string_literal: true

RetryJobsJob = LegacyApplicationJob.wrap(GitHub::Jobs::RetryJobs)
