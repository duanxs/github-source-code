# frozen_string_literal: true

RepairReposIndexJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepairReposIndex)
