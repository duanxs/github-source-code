# frozen_string_literal: true

SubscribeUserToThreadJob = LegacyApplicationJob.wrap(GitHub::Jobs::SubscribeUserToThread)
