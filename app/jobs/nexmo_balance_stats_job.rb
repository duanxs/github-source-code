# frozen_string_literal: true

NexmoBalanceStatsJob = LegacyApplicationJob.wrap(GitHub::Jobs::NexmoBalanceStats)
