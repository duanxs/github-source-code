# frozen_string_literal: true

PurgeSpammyFromSearchIndexJob = LegacyApplicationJob.wrap(GitHub::Jobs::PurgeSpammyFromSearchIndex)
