# frozen_string_literal: true

class TokenPathDiscoveryJob < ApplicationJob
  include TokenPathHelper

  # number of commits to process in a single gitrpc call
  COMMIT_SAMPLE_SIZE = 100;

  areas_of_responsibility :token_scanning
  queue_as :token_path_discovery

  locked_by timeout: 15.minutes, key: ->(job) {
    job.arguments.first
  }

  around_perform :with_stats

  def perform(*args)
    return "no_repo" if git_repo.nil?

    # We should never reach this state where token scanning is disabled but this job was queued.
    return "no_scan_for_tokens" unless token_scanning_enabled?

    kv_key = "repo_id.#{git_repo.id}.token_path_discovery_job_timeout"

    return "timeout_lock" if GitHub.kv.exists(kv_key).value!

    if unprocessed_scan_results.size > 0
      commits_to_token_scan_result_locations = unprocessed_scan_results.group_by { |result| result[:commit_oid] }
      commits = commits_to_token_scan_result_locations.keys.sample(COMMIT_SAMPLE_SIZE)

      begin
        diff_trees = git_repo.rpc.native_read_diff_toc_multi(commits)
      rescue GitRPC::Timeout => timeout
        GitHub.kv.set(kv_key, "true", expires: 5.minutes.from_now)
        GitHub.dogstats.increment("token_scanning_path_reconciliation_gitrpc_timeout")
        return "timeout"
      end

      result_locations_with_hidden_paths = find_multiple_paths_for_blobs(commits_to_token_scan_result_locations, diff_trees)
      new_token_scan_result_location_records = build_new_token_scan_result_location_records(result_locations_with_hidden_paths)
      saved_locations = []
      if new_token_scan_result_location_records.length > 0
        GitHub.dogstats.count("token_scanning_path_reconciliation_new_included_results_count", new_token_scan_result_location_records.count)
        TokenScanResultLocation.transaction do #batch them into one transaction
          new_token_scan_result_location_records.each do |record|
            saved_locations << TokenScanResultLocation.create_and_check_path(record[:token], record[:location])
          end
        end
      end

      # We need to update only the records we touched.
      TokenScanResultLocation.where(["token_scan_result_id in (?) and commit_oid in (?)", git_repo.token_scan_results.select(:id), commits]).update_all(blob_paths_processed: true)

      results_to_notify = saved_locations.select { |location| location.include? }
      if results_to_notify.count > 0
        GitHub.dogstats.increment("token_scanning_path_reconciliation_new_included_results_found")
        notify_subscribers(TokenScanResult.where(["id in (?)", saved_locations.map { |location| location.token_scan_result_id }]))
      end

      if commits_to_token_scan_result_locations.keys.count > commits.count
        GitHub.dogstats.increment("token_scanning_path_reconciliation_extra_job_enqueued")
        TokenPathDiscoveryJob.perform_later(self.arguments.first)
      end

      "success"
    else
      GitHub.dogstats.increment("token_scanning_path_reconciliation_no_results_to_process")
    end
  end

  # Get all of the TokenScanResultLocation rows from the db that belong to the current repo.
  def unprocessed_scan_results
    # Scope this down to just the current repo using where clause on TokenScanResults that belong to current repo.
    @unprocessed_scan_results ||= TokenScanResultLocation.where(token_scan_result_id: git_repo.token_scan_results.select(:id), blob_paths_processed: false)
  end

  def notify_subscribers(results)
    return if results.empty?

    scope = AccountMailer::TOKEN_SCANNING_SCOPE[:reconciliation_scoped]
    users = git_repo.token_scanning_users_to_notify
    return if users.empty?

    AccountMailer.token_scanning_summary(git_repo, results, users, scope).deliver_now
    GitHub.dogstats.increment("token_scanning_path_reconciliation_new_included_results_email_sent")
  end

  # Collect stats about each attempt of perform
  def with_stats
    time_start = Time.now
    result = yield
    rescue GitRPC::CommandFailed => error
      result = "error"
      # raise again to pass error to `retry_on`
      raise
    rescue GitHub::Restraint::UnableToLock => error
      # Another job is holding the lock at this point.
      result = "lock_unavailable"
      raise
    rescue GitRPC::InvalidRepository => error
      # Repository has been deleted so report as no_repo result
      result = "no_repo"
      raise
    ensure
    time_delta = Time.now - time_start
    errname = error.nil? ? "nil" : error.class.name.underscore

    GitHub.dogstats.increment("token_path_discovery.perform.try", tags: stats_tags +
      [
        "error:#{errname}",
        "try:#{executions}",
        "result:#{result}",
      ]
    )

    GitHub.dogstats.timing("token_path_discovery.perform.time", time_delta,
      tags: stats_tags +
        [
          "error:#{errname}",
          "result:#{result}",
        ]
    )
  end

  # Extensibility point for jobs to control enablement of scan.
  # This check varies with private repos.
  def token_scanning_enabled?
    git_repo.token_scanning_enabled?
  end

  def git_repo
    @git_repo ||= Repository.find_by_id(self.arguments.first)
  end

  # Tags to include in any stats.
  #
  # Returns an Array of Strings.
  def stats_tags
    ["job:#{self.class.name.demodulize.underscore}"]
  end
end
