# frozen_string_literal: true

class AutoAcceptSponsorsApplicationsJob < ApplicationJob
  queue_as :sponsors_application_processing

  schedule interval: 24.hours, condition: -> { GitHub.sponsors_enabled? }

  def perform
    return unless GitHub.flipper[:sponsors_automated_acceptance].enabled?

    Failbot.push(app: "github-sponsors")

    GitHub::SQL::Readonly.new(membership_id_iterator.batches).each do |rows|
      memberships = SponsorsMembership.where(id: rows.flatten)

      memberships.each do |membership|
        next unless membership.auto_acceptable?

        result = SponsorsMembership.throttle_writes_with_retry(max_retry_count: 8) do
          Sponsors::AcceptSponsorsMembership.call(
            sponsors_membership: membership,
            actor: nil,
            automated: true,
          )
        end

        unless result.success?
          Failbot.report(
            AutoAcceptError.new(
              "Failed to auto accept membership (#{result.sponsors_membership.id})",
            ),
            sponsors_membership_id: result.sponsors_membership.id,
            errors: result.errors,
          )
        end
      end
    end
  end

  class AutoAcceptError < StandardError; end

  private

  def membership_id_iterator
    iterator = SponsorsMembership.github_sql_batched(start: 0, limit: 100)
    iterator.add(<<-SQL, automated_ids: automated_criteria_ids, billing_countries: SponsorsMembership::AUTO_ACCEPTABLE_COUNTRIES)
      SELECT DISTINCT sponsors_memberships.id
      FROM sponsors_memberships
      LEFT JOIN sponsors_memberships_criteria
          ON sponsors_memberships_criteria.sponsors_membership_id = sponsors_memberships.id
          AND sponsors_memberships_criteria.sponsors_criterion_id IN :automated_ids
          AND sponsors_memberships_criteria.met = 0
      WHERE sponsors_memberships.ignored_at IS NULL
        AND sponsors_memberships.state = 0
        AND sponsors_memberships.billing_country IN :billing_countries
        AND sponsors_memberships_criteria.id IS NULL
        AND sponsors_memberships.id > :last
        ORDER BY sponsors_memberships.id
        LIMIT :limit
    SQL
    iterator
  end

  def automated_criteria_ids
    @automated_criteria_ids ||= SponsorsCriterion.automated.where(active: true).pluck(:id)
  end
end
