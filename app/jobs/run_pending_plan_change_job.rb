# frozen_string_literal: true

class RunPendingPlanChangeJob < ApplicationJob
  areas_of_responsibility :gitcoin

  queue_as :billing

  locked_by timeout: 5.minutes, key: ApplicationJob::DEFAULT_LOCK_PROC

  discard_on ActiveJob::DeserializationError

  def perform(pending_plan_change)
    if pending_plan_change.incomplete?
      pending_plan_change.run
    end
  end
end
