# frozen_string_literal: true

MailchimpUnsubscribeJob = LegacyApplicationJob.wrap(GitHub::Jobs::MailchimpUnsubscribe)
