# frozen_string_literal: true

HookDeliveryRaceConditionCheckJob = LegacyApplicationJob.wrap(GitHub::Jobs::HookDeliveryRaceConditionCheck)
