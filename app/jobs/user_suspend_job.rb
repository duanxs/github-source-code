# frozen_string_literal: true

UserSuspendJob = LegacyApplicationJob.wrap(GitHub::Jobs::UserSuspend)
