# frozen_string_literal: true

RemoveForksForInaccessibleRepositoriesJob = LegacyApplicationJob.wrap(GitHub::Jobs::RemoveForksForInaccessibleRepositories)
