# frozen_string_literal: true

AssignStafftoolsAccessJob = LegacyApplicationJob.wrap(GitHub::Jobs::AssignStafftoolsAccessJob)
