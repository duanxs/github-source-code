# frozen_string_literal: true

SpokesDiskStatsCacheFillJob = LegacyApplicationJob.wrap(GitHub::Jobs::DgitDiskStatsCacheFill)
