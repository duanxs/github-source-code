# frozen_string_literal: true

UserSignupJob = LegacyApplicationJob.wrap(GitHub::Jobs::UserSignup)
