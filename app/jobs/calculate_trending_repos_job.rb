# frozen_string_literal: true

CalculateTrendingReposJob = LegacyApplicationJob.wrap(GitHub::Jobs::CalculateTrendingRepos)
