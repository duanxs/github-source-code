# frozen_string_literal: true

class UpdateOrganizationRoutingToEligibleEmailJob < ApplicationJob
  queue_as :update_organization_routing_to_eligible_email

  class ServiceUnavailable < RuntimeError; end

  retry_on(ServiceUnavailable, wait: 60.seconds, attempts: 3) do |job, error|
    Failbot.report(error)
  end

  # Public: Finds members in an organization that are no longer able to receive
  # email notifications due to notification restrictions, and updates their
  # routing settings to use an eligible email address if available.
  #
  # organization – the Organization that has enabled notification restrictions.
  #
  # Returns nothing.
  def perform(organization)
    return unless organization.restrict_notifications_to_verified_domains?

    affected_members = organization.members - organization.async_members_without_verified_domain_email.sync
    verified_domains = organization.async_verified_domains.sync.map(&:domain)

    affected_members.each_slice(100) do |member_slice|
      # Get the newsies settings for each member, since we only need to update this
      # for users who receive email notifications.
      newsies_response = ActiveRecord::Base.connected_to(role: :reading) do
        GitHub.newsies.load_user_settings(member_slice.pluck(:id))
      end
      raise ServiceUnavailable unless newsies_response.success?

      newsies_settings = newsies_response.value.index_by(&:user)

      eligible_emails = UserEmail.notifiable.verified.
        where(user_id: member_slice.pluck(:id), normalized_domain: verified_domains).
        group_by(&:user_id)

      member_slice.each do |member|
        if setting = newsies_settings[member]
          next unless receives_email_notifications?(setting)
          next if organization.async_user_can_receive_email_notifications?(member).sync

          eligible_email = eligible_emails[member.id]&.first.email

          next unless eligible_email.present?

          NotificationUserSetting.throttle do
            response = GitHub.newsies.get_and_update_settings(member) do |settings|
              settings.email organization, eligible_email
            end

            raise ServiceUnavailable unless response.success?
          end
        end
      end
    end
  end

  private

  # Private: Does this newsies config specify that the user should receive
  #          email notifications?
  #
  # settings - A Newsies::Settings object.
  #
  # Returns a Boolean.
  def receives_email_notifications?(setting)
    setting.participating_email? || setting.subscribed_email?
  end
end
