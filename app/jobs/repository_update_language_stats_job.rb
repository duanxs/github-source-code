# frozen_string_literal: true

RepositoryUpdateLanguageStatsJob = LegacyApplicationJob.wrap(GitHub::Jobs::RepositoryUpdateLanguageStats)
