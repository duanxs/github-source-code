# frozen_string_literal: true

class CreateAutoInactiveDeploymentStatuses < ApplicationJob
  areas_of_responsibility :deployments_api
  queue_as :create_auto_inactive_deployment_statuses

  def perform(deployment, include_production)
    deployment.throttle do
      deployment.set_previous_environment_deployments_inactive!(include_production: include_production)
    end
  end
end
