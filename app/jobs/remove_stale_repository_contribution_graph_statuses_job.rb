# frozen_string_literal: true

# Delete any Repository::ContributionGraphStatus where the repo's graphs have not been
# viewed in the last 28 days. A callback is configured to delete related graph data
# from Eventer when each record is destroyed.
class RemoveStaleRepositoryContributionGraphStatusesJob < ApplicationJob
  areas_of_responsibility :repos

  queue_as :graphs
  schedule interval: 5.minutes, condition: -> { GitHub.eventer_enabled? }
  locked_by key: ApplicationJob::DEFAULT_LOCK_PROC, timeout: 20.minutes

  def perform
    ActiveRecord::Base.connected_to(role: :reading) do
      Repository::ContributionGraphStatus.expired.find_in_batches do |batch|
        destroy_batch(batch)
      end
    end
  end

  private

  def destroy_batch(graph_statuses)
    Repository::ContributionGraphStatus.throttle_writes_with_retry(max_retry_count: 5) do
      graph_statuses.each(&:destroy)
    end
  end
end
