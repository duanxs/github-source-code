# frozen_string_literal: true

class EnforceActiveUserSessionLimitJob < ApplicationJob
  queue_as :enforce_active_user_session_limit

  locked_by key: ApplicationJob::DEFAULT_LOCK_PROC, timeout: 5.minutes

  BATCH_LIMIT = 1_000

  # Discard the job if the user is deleted before the job runs
  discard_on ActiveRecord::RecordNotFound

  def perform(user)
    return unless user.over_active_session_limit?

    while user.over_active_session_limit?
      ids =
        ActiveRecord::Base.connected_to(role: :reading) do
          # user.sessions.user_facing.order("accessed_at DESC").limit(BATCH_LIMIT).pluck(:id)
          user.sessions.user_facing.
            order("accessed_at DESC").
            limit(BATCH_LIMIT).
            # skip the number of sessions that are allowed
            offset(UserSession::LIMIT).
            pluck(:id)
        end
      ids.each_slice(UserSession::LIMIT) do |session_ids|
        UserSession.throttle do
          UserSession.where(id: session_ids).destroy_all
        end
      end
    end
  end
end
