# frozen_string_literal: true

class UpdateExternalCustomerJob < ApplicationJob
  areas_of_responsibility :gitcoin
  queue_as :billing

  discard_on ActiveRecord::RecordNotFound

  RETRYABLE_ERRORS = [
    Errno::ECONNREFUSED,
    Errno::ECONNRESET,
    Errno::ETIMEDOUT,
    Faraday::ConnectionFailed,
    Faraday::TimeoutError,
    Net::HTTPRequestTimeOut,
    Net::ReadTimeout,
    Zuorest::HttpError,
  ].freeze

  RETRYABLE_ERRORS.each do |error|
    retry_on(error) do
      Failbot.report(error)
    end
  end

  # Public: Update one BillingTransaction's statuses with data from correlating external billing vendor
  def perform(customer)
    # We need to check that they only have one customer account associated
    # because it's possible that multiple users are attached to the same external customer.
    # If they have more than one user, we cannot determine (yet) if the current user is the one
    # that should be updating the customer info.
    #
    customer.update_external_account_name if customer.customer_accounts.one?
  end
end
