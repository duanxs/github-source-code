# frozen_string_literal: true

UpdateNewsletterPreferenceJob = LegacyApplicationJob.wrap(GitHub::Jobs::UpdateNewsletterPreference)
