# frozen_string_literal: true

class RemoveOrgMemberForksJob < RemoveOrgMemberDataJob
  areas_of_responsibility :orgs, :repositories

  queue_as :archive_restore

  # options is used by the base class it is a hash of { "organization_id" => value, "user_id" => value }
  def perform(options)
    org_forks.find_in_batches do |batch|
      inaccessible = inaccessible_org_repos(batch) { |user_fork| user_fork.parent }
      inaccessible.each do |user_fork|
        # It would be nice to refactor this to just call user_fork.remove
        # but the restorable makes that tricky.
        user_fork.throttle do
          # No actor is better than the wrong actor.
          # See https://github.com/github/security/issues/3748.
          user_fork.hide_repository_and_dependents(nil)
          user_fork.publish_removed_repository(nil)
          user_fork.remove_via_job
        end
        user_fork.send_private_fork_deleted_email

        GitHub.dogstats.increment("org.repo.archive_fork")
      end
      restorable.save_repositories(inaccessible)
    end

    # If this org is part of a business, also check accessibility of
    # any forks of repos in other orgs having internal visibility.
    ::RemoveBizUserForksJob.perform_later(biz_id: org.business.id, belonging_to_user_id: user.id) if org.business

    restorable.save_repositories_complete
  end

  private

  def org_forks
    @org_forks ||= Repository.private_forks_for(
                  organization: org, belonging_to_user: user,
                )
  end
end
