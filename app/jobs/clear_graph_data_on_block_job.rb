# frozen_string_literal: true

ClearGraphDataOnBlockJob = LegacyApplicationJob.wrap(GitHub::Jobs::ClearGraphDataOnBlock)
