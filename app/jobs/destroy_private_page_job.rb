# frozen_string_literal: true

# DestroyPrivatePageJob destroy private pages under an organization when billing downgrade plan from business plus.
class DestroyPrivatePageJob < ApplicationJob
  areas_of_responsibility :pages
  queue_as :background_destroy

  class DestroyPrivatePageError < RuntimeError
    attr_reader :organization_id, :failed_unpublish_page_ids

    def initialize(message, organization_id: nil, failed_unpublish_page_ids: nil)
      super(message)
      @organization_id = organization_id
      @failed_unpublish_page_ids = failed_unpublish_page_ids
    end
  end

  retry_on DestroyPrivatePageError, wait: 2.seconds, attempts: 3 do |job, error|
    Failbot.report(
      error,
      organization_id: error.organization_id,
      failed_unpublish_page_ids: error.failed_unpublish_page_ids,
    )
    GitHub.dogstats.increment("pages.private_page.destroy", tags: ["state:failure"])
  end

  def perform(organization)
    private_page_ids = []
    ActiveRecord::Base.connected_to(role: :reading) do
      Repository.where(public: false, parent_id: nil, active: true,  locked: false, owner_id: organization.id).includes(:page).find_each do |repository|
        private_page_ids.push(repository.page.id) if repository.page
      end
    end
    unpublish_pages(organization.id, private_page_ids)
  end

  def unpublish_pages(organization_id, page_ids)
    failed_unpublish_page_ids = []
    page_ids.each do |page_id|
      begin
        Page.throttle { Page.destroy(page_id) }
      rescue ActiveRecord::NotFound
        # Do nothing
      rescue RuntimeError
        failed_unpublish_page_ids.push(page_id)
      end
    end
    if failed_unpublish_page_ids.any?
      raise DestroyPrivatePageError.new(organization_id: organization_id, failed_unpublish_page_ids: failed_unpublish_page_ids, message: "failed to unpublish some private pages.")
    else
      GitHub.dogstats.increment("pages.private_page.destroy", tags: ["state:succeed"])
    end
  end
end
