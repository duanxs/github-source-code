# frozen_string_literal: true

RevokeOrgMembershipAbilitiesJob = LegacyApplicationJob.wrap(GitHub::Jobs::RevokeOrgMembershipAbilities)
