# frozen_string_literal: true

class SecurityIncidentResponseJob < ApplicationJob
  queue_as :spam

  attr_accessor :batch_count, :batch_index, :errors, :users_by_id
  attr_reader :actor, :incident_response_id

  def perform(actor:, id:, incident_responses:)
    @actor = actor
    @incident_response_id = id
    @users_by_id = {}
    preload_users_by_id!(incident_responses)
    @batch_index = 0
    incident_responses.each do |response|
      handle_response(response)
    end
  end

  private

  # Private: Preload the users specified in the incident_responses.
  #
  # incident_responses - Array of hashes with a :user_id or :user_ids key.
  #
  # Assigns @users_by_id
  def preload_users_by_id!(incident_responses)
    user_ids = incident_responses.flat_map do |response|
      user_ids_for_response(response).map do |user_id|
        is_database_id = user_id.to_i.to_s == user_id.to_s
        if is_database_id
          user_id
        else
          begin
            Platform::Helpers::NodeIdentification.from_global_id(user_id.to_s).last
          rescue Platform::Errors::NotFound
            nil
          end
        end
      end
    end
    @batch_count = user_ids.size
    users = User.where(id: user_ids)
    users.each do |user|
      @users_by_id[user.id.to_s] = user
      @users_by_id[user.global_relay_id] = user
    end
  end

  def user_ids_for_response(response)
    Array(response[:user_id]) + Array(response[:users]).map { |u| u[:id] }
  end

  def users_for_response(response)
    Array(response[:user_id]) + Array(response[:users])
  end

  # Private: Parse the response to run all remediations and potentially write a staffnote
  # and notify the user, and publish a SecurityIncidentRemediation event.
  #
  # response - A hash with the structure of Inputs::SecurityIncidentResponse
  def handle_response(response)
    users_for_response(response).each_with_index do |user_data, user_index|
      # batch_index starts at 1 so that knowing if a job is the last one is batch_count == batch_indx
      @batch_index += 1
      @errors = []
      if user_data.is_a?(String)
        user_id = user_data
      else
        user_id = user_data[:id]
        user_template_data = user_data[:data]
      end
      user = users_by_id[user_id.to_s]

      successful_remediations, failed_remediations = run_remediations(user, response, user_template_data)

      GlobalInstrumenter.instrument("security_incident_response.remediation_complete",
        actor: actor,
        account: user,
        account_id: String(user&.global_relay_id || user_id),
        errors: errors,
        failed_remediations: failed_remediations.sort,
        successful_remediations: successful_remediations.sort,
        staffnote: response[:staffnote],
        email_body_template: response[:notify].try(:[], :template),
        incident_response_id: incident_response_id,
        batch_count: batch_count,
        batch_index: batch_index,
        inputs: response.to_json,
      )
    end
  end

  # Private: Run the remediations for a particular user and keep track of which
  # succeed and which fail.
  # When the user was not found or is an employee all remediations are marked as failed.
  #
  # user - The account to do these remediations for.
  # response - A hash with the structure of Inputs::SecurityIncidentResponse
  #
  # Returns successful_remediations and failed_remediations, both arrays of the names of the remediations.
  def run_remediations(user, response, user_template_data)
    failed_remediations = []
    successful_remediations = []

    remediations = remediations_for_response(response)

    add_error_for_missing_or_staff_users!(user, remediations.keys)

    remediations.each do |type, remediation_data|
      # Don't list something like { reset_password: false } as an attempted remediation.
      next unless remediation_data.present?

      # If the user is not found or is a staff member, return all remediations as failed
      # unless we're notifying a staff member, which is allowed.
      if user.nil? || (user&.employee? && type != :notify)
        failed_remediations.push(type)
        next
      end

      # Don't notify or write staffnotes if remediations have failed
      if [:notify, :staffnote].include?(type) && failed_remediations.present?
        failed_remediations.push(type)
        next
      end

      if perform_remediation(user, type, remediation_data, user_template_data)
        successful_remediations.push(type)
      else
        failed_remediations.push(type)
      end
    end

    return successful_remediations, failed_remediations
  end

  def add_error_for_missing_or_staff_users!(user, remediations)
    if user.nil?
      errors << "Account not found."
    end

    if user&.employee? && remediations != [:notify]
      errors << "SIRE can only notify GitHub employees."
    end
  end

  # Private: Gathers the remediations from the response as it may include other data
  # and puts the notify and staffnote remediations at the end so that we can not do them
  # if other remediations failed.
  def remediations_for_response(response)
    remediations = response.except(:notify, :staffnote, :user_id, :users)
    remediations[:notify] = response[:notify] if response[:notify].present?
    remediations[:staffnote] = response[:staffnote] if response[:staffnote].present?
    remediations
  end

  def perform_remediation(user, type, remediation_data, user_template_data)
    case type
    when :notify
      notify_user(user, remediation_data, user_template_data)
    when :remove_repository_recommendations
      remove_repository_recommendations(user, remediation_data)
    when :remove_repository_stars
      remove_repository_stars(user, remediation_data)
    when :reset_password
      reset_password(user)
    when :revoke_oauth_authorizations
      revoke_oauth_authorizations(user, remediation_data)
    when :revoke_oauth_tokens
      revoke_oauth_tokens(user, remediation_data)
    when :staffnote
      write_staffnote(user, remediation_data)
    else
      false
    end
  end

  def write_staffnote(user, staffnote_text)
    return false unless staffnote_text.present?

    StaffNote.create(user: actor, notable: user, note: staffnote_text)

    true
  end

  def notify_user(user, notify_data, user_template_data)
    return false unless notify_data.present?

    notifier = GitHub::SendSecurityIncidentNotification.new(
      from: notify_data[:from],
      subject: notify_data[:subject],
      template: notify_data[:template],
    )
    template_data = { user: user, login: user.login }
    Array(user_template_data || notify_data[:template_data]).each do |data|
      template_data[data[:key].to_sym] = data[:value]
    end
    notifier.perform_for_user(template_data)

    true
  end

  def safe_perform_remediation(user, type, *error_classes)
    begin
      yield
    rescue *error_classes => e
      Failbot.report(e, user_global_relay_id: user.global_relay_id, remediation_type: type)
      false
    end
  end

  def reset_password(user)
    type = :reset_password
    safe_perform_remediation(user, type, ActiveRecord::ActiveRecordError) do
      user.set_random_password(
        actor: actor,
        send_notification: false,
      )
    end
  end

  def revoke_oauth_authorizations(user, application_ids)
    type = :revoke_oauth_authorizations
    safe_perform_remediation(user, type, ActiveRecord::ActiveRecordError) do
      valid_app_ids = user.oauth_authorizations.where(application_id: application_ids).pluck(:application_id)
      if valid_app_ids.size != application_ids.size
        missing_app_ids = (application_ids - valid_app_ids).join(", ")
        errors << "#{type}: User did not have OAuth access for these apps: #{missing_app_ids}."
        return false if valid_app_ids.blank?
      end
      user.revoke_oauth_tokens_for_apps(
        valid_app_ids,
        should_throttle: true,
      )
      true
    end
  end

  def revoke_oauth_tokens(user, token_ids)
    type = :revoke_oauth_tokens
    safe_perform_remediation(user, type, ActiveRecord::ActiveRecordError) do
      valid_token_ids = user.oauth_accesses.where(id: token_ids).pluck(:id)
      if valid_token_ids.size < token_ids.size
        missing_token_ids = (token_ids - valid_token_ids).join(", ")
        errors << "#{type}: User did not have these OAuth Tokens: #{missing_token_ids}."
        return false if valid_token_ids.blank?
      end
      user.revoke_specific_oauth_tokens(
        valid_token_ids,
        should_throttle: true,
      )
      true
    end
  end

  def remove_repository_recommendations(user, all_repository_ids)
    type = :remove_repository_recommendations
    safe_perform_remediation(user, type, ActiveRecord::ActiveRecordError) do
      not_users_repos = []
      all_repository_ids.each_slice(50) do |repository_ids|
        Repository.throttle do
          owned_repositories = user.repositories.where(id: repository_ids)
          owned_repository_ids = owned_repositories.pluck(:id)
          not_users_repos = not_users_repos.concat(repository_ids - owned_repository_ids)

          RepositoryRecommendationOptOut.throttle do
            owned_repositories.each do |repository|
              repository.set_network_privilege(:hide_from_discovery, true)
            end
          end
        end
      end

      if not_users_repos.present?
        errors << "#{type}: User did not own these repositories: #{not_users_repos.join(", ")}."
        return false if not_users_repos.size == all_repository_ids.size
      end

      true
    end
  end

  def remove_repository_stars(user, all_repository_ids)
    type = :remove_repository_stars
    safe_perform_remediation(user, type, ActiveRecord::ActiveRecordError) do
      repos_with_stars = []
      all_repository_ids.each_slice(50) do |repository_ids|
        Repository.throttle do
          repositories = Repository.where(id: repository_ids)

          Star.throttle do
            repositories.each do |repository|
              if user.unstar(repository, actor: actor)
                repos_with_stars << repository.id
              end
            end
          end
        end
      end

      repos_without_stars = all_repository_ids - repos_with_stars

      if repos_without_stars.present?
        errors << "#{type}: User did not have stars for these repositories: #{repos_without_stars.join(", ")}."
        return false if repos_without_stars.size == all_repository_ids.size
      end

      true
    end
  end
end
