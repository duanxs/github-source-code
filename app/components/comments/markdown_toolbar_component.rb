# frozen_string_literal: true
module Comments
  class MarkdownToolbarComponent < ApplicationComponent
    include ClassNameHelper

    attr_reader :textarea_id

    with_content_areas :prepend_buttons, :append_buttons

    def initialize(
      textarea_id:,
      responsive: false
    )
      @textarea_id = textarea_id
      @responsive = responsive
    end

    def responsive?
      @responsive
    end
  end
end
