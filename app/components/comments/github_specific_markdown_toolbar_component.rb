# frozen_string_literal: true
module Comments
  class GitHubSpecificMarkdownToolbarComponent < ApplicationComponent
    attr_reader :textarea_id, :file_chooser_id, :saved_reply_context, :hide_saved_replies, :responsive, :suggestion_button_path, :pull_request, :discussions_enabled, :structured_issue_comment_templates

    def initialize(
      textarea_id:,
      saved_reply_context: nil,
      hide_saved_replies: false,
      allows_suggested_changes: false,
      responsive: false,
      suggestion_button_path: nil,
      pull_request: nil,
      discussions_enabled: false,
      structured_issue_comment_templates: []
    )
      @file_chooser_id = "fc-#{textarea_id}"
      @textarea_id = textarea_id
      @saved_reply_context = saved_reply_context
      @hide_saved_replies = hide_saved_replies
      @allows_suggested_changes = allows_suggested_changes
      @responsive = responsive
      @suggestion_button_path = suggestion_button_path
      @pull_request = pull_request
      @discussions_enabled = discussions_enabled
      @structured_issue_comment_templates = structured_issue_comment_templates
    end

    def show_suggested_changes_button?
      @allows_suggested_changes
    end

    def dismiss_notice_url
      dismiss_notice_path("suggested_changes_onboarding_prompt")
    end

    def responsive?
      @responsive
    end

    def reference_issue_tooltip
      if @discussions_enabled
        "Reference an issue, pull request, or discussion"
      else
        "Reference an issue or pull request"
      end
    end
  end
end
