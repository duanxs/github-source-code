# frozen_string_literal: true
module Comments
  class SavedRepliesMenuComponent < ApplicationComponent
    attr_reader :filter_field_id, :textarea_id, :saved_reply_context, :structured_issue_comment_templates

    def initialize(
      textarea_id:,
      responsive: false,
      saved_reply_context: nil,
      structured_issue_comment_templates: []
    )
      @filter_field_id = "saved-reply-filter-field-#{SecureRandom.hex(4)}"
      @textarea_id = textarea_id
      @responsive = responsive
      @saved_reply_context = saved_reply_context || "none" # Allows us to handle nils coming in from the args
      @structured_issue_comment_templates = structured_issue_comment_templates
    end

    def responsive?
      @responsive
    end
  end
end
