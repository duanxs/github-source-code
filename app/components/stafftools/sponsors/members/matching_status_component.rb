# frozen_string_literal: true

class Stafftools::Sponsors::Members::MatchingStatusComponent < ApplicationComponent
  include ClassNameHelper

  def initialize(listing:)
    @listing = listing
  end

  private

  def render?
    @listing.present?
  end

  def label_text
    @listing.matchable? ? "Matchable" : "Not matchable"
  end

  def label_title
    "Label: #{label_text.downcase} label"
  end

  def label_class
    class_names(
      "Label",
      "Label--outline",
      "text-pink" => @listing.matchable?,
    )
  end

  def label_style
    return "" unless @listing.matchable?

    "border: 1px solid #f9b3dd;"
  end

  def matching_subtitle
    if @listing.matchable?
      matching_subtitle_for_matchable
    elsif @listing.match_disabled?
      "Matching is manually disabled"
    elsif @listing.sponsorable.organization?
      "Ineligible – sponsored organization"
    elsif !@listing.sponsorable.sponsors_membership.joined_waitlist_before_match_deadline?
      "Joined after match deadline"
    elsif @listing.reached_match_limit?
      "Reached match limit on #{@listing.match_limit_reached_at.strftime('%Y-%m-%d')}"
    elsif !@listing.published_in_last_year?
      "Published over a year ago"
    else
      exception = ArgumentError.new("Unhandled reason for why listing is not matchable (#{@listing.id})")

      if Rails.env.production?
        Failbot.report(exception)
        "Unknown reason"
      else
        raise exception
      end
    end
  end

  def matching_subtitle_for_matchable
    if @listing.published_at
      end_date = (@listing.published_at + 1.year).strftime("%Y-%m-%d")
      "Matching period ends on #{end_date}"
    else
      "Matching period has not started"
    end
  end
end
