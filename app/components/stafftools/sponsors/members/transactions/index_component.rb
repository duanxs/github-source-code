# frozen_string_literal: true

class Stafftools::Sponsors::Members::Transactions::IndexComponent < ApplicationComponent
  def initialize(transactions:, sponsorable:)
    @transactions = transactions || []
    @sponsorable = sponsorable
  end
end
