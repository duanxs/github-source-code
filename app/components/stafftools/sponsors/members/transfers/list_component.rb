# frozen_string_literal: true

class Stafftools::Sponsors::Members::Transfers::ListComponent < ApplicationComponent
  def initialize(
    sponsorable:,
    stripe_account:,
    page:,
    limit: ::Billing::Stripe::Transfer::LIMIT,
    starting_after: nil,
    ending_before: nil,
    paginate: true
  )
    @sponsorable    = sponsorable
    @stripe_account = stripe_account
    @page           = page
    @limit          = limit
    @starting_after = starting_after
    @ending_before  = ending_before
    @paginate       = fetch_or_fallback([true, false], paginate, true)
  end

  private

  attr_reader :sponsorable, :stripe_account, :page, :limit, :starting_after, :ending_before, :paginate

  def render?
    sponsorable.present? && stripe_account.present? && page.present? && limit.present?
  end

  def transfers
    @transfers ||= stripe_account.stripe_transfers(
      starting_after,
      ending_before,
      limit: limit,
    )
  end

  def previous_page
    previous = page - 1
    [1, previous].max
  end

  def next_page
    page + 1
  end

  def empty_results?
    transfers.count.zero?
  end

  def first_page?
    page == 1
  end

  def has_next_page?
    transfers.count == limit
  end

  def has_previous_page?
    !first_page?
  end

  def displayed_count
    has_next_page? ? "#{transfers.count}+" : "#{transfers.count}"
  end

  def next_page_url
    return unless has_next_page?

    stafftools_sponsors_member_transfers_path(
      sponsorable,
      starting_after: transfers.last.transfer_id,
      page: next_page
    )
  end

  def previous_page_url
    return if first_page?

    stafftools_sponsors_member_transfers_path(
      sponsorable,
      ending_before: transfers.first.transfer_id,
      page: previous_page
    )
  end

  def show_pagination?
    return false unless paginate
    !empty_results?
  end

  def latest_payout
    return @latest_payout if defined?(@latest_payout)
    @latest_payout = stripe_account.latest_payout
  end
end
