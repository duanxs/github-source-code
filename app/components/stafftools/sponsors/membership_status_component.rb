# frozen_string_literal: true

module Stafftools
  module Sponsors
    class MembershipStatusComponent < ApplicationComponent
      class MembershipStatus
        attr_reader :color, :octicon, :label

        def initialize(color:, octicon:, label:)
          @color = color
          @octicon = octicon
          @label = label
        end

        def text_class
          "text-#{color}"
        end
      end

      def initialize(sponsorable:, membership:)
        @sponsorable = sponsorable
        @membership = membership
      end

      private

      attr_reader :sponsorable, :membership

      def render?
        sponsorable.present? && membership.present?
      end

      def sponsors_listing
        return @sponsors_listing if defined?(@sponsors_listing)
        @sponsors_listing = sponsorable&.sponsors_listing
      end

      def listing_status
        return @listing_status if defined?(@listing_status)
        @listing_status = begin
          if sponsors_listing.nil?
            MembershipStatus.new(color: :red,    octicon: :x,        label: "None")
          elsif sponsors_listing.approved?
            MembershipStatus.new(color: :green,  octicon: :check,    label: "Public")
          elsif sponsors_listing.verified_and_pending_approval?
            MembershipStatus.new(color: :yellow, octicon: :watch,    label: "Needs approval")
          elsif sponsors_listing.draft?
            MembershipStatus.new(color: :yellow, octicon: :pencil,   label: "Draft")
          elsif sponsors_listing.disabled?
            MembershipStatus.new(color: :red,    octicon: :lock,     label: "Disabled")
          else
            MembershipStatus.new(color: :red,    octicon: :question, label: "Unknown")
          end
        end
      end

      def stripe_connect_account
        return @stripe_connect_account if defined?(@stripe_connect_account)
        @stripe_connect_account = sponsors_listing&.stripe_connect_account
      end

      def stripe_account_details
        return @stripe_account_details if defined?(@stripe_account_details)
        @stripe_account_details =
          if account_details = stripe_connect_account&.stripe_account_details
            Stripe::Account.construct_from(account_details)
          end
      end

      def payouts_enabled?
        return @payouts_enabled if defined?(@payouts_enabled)
        @payouts_enabled =
          stripe_account_details.present? && stripe_account_details.payouts_enabled
      end

      def automated_payouts_disabled?
        return @automated_payouts_disabled if defined?(@automated_payouts_disabled)
        @automated_payouts_disabled =
          payouts_enabled? && stripe_connect_account.automated_payouts_disabled?
      end

      def charges_enabled?
        return @charges_enabled if defined?(@charges_enabled)
        @charges_enabled = stripe_account_details.charges_enabled?
      end

      def payout_status
        return @payout_status if defined?(@payout_status)
        @payout_status = begin
          if !payouts_enabled?
            MembershipStatus.new(color: :red,    octicon: :x,     label: "Not enabled")
          elsif !charges_enabled?
            MembershipStatus.new(color: :red, octicon: "circle-slash",  label: "Disabled")
          elsif automated_payouts_disabled?
            MembershipStatus.new(color: :yellow, octicon: :lock,  label: "Manual")
          else
            interval = stripe_account_details.settings&.payouts&.schedule&.interval&.capitalize
            MembershipStatus.new(color: :green,  octicon: :check, label: interval || "Unknown")
          end
        end
      end

      def matching_enabled?
        return @matching_enabled if defined?(@matching_enabled)
        @matching_enabled = sponsors_listing.present? && !sponsors_listing.match_disabled?
      end

      def matching_status
        return @matching_status if defined?(@matching_status)
        @matching_status = begin
          if sponsors_listing.nil?
            MembershipStatus.new(color: :red,    octicon: :x,     label: "Unknown")
          elsif sponsors_listing.match_disabled?
            MembershipStatus.new(color: :yellow, octicon: :lock,  label: "Disabled")
          elsif sponsors_listing.reached_match_limit?
            MembershipStatus.new(color: :green,  octicon: :star,  label: "Reached")
          elsif sponsors_listing.matchable?
            MembershipStatus.new(color: :green,  octicon: :check, label: "On")
          else
            MembershipStatus.new(color: :red,    octicon: :x,     label: "Not matchable")
          end
        end
      end

      def docusign_status
        return @docusign_status if defined?(@docusign_status)
        @docusign_status = begin
          if !sponsorable.sponsors_docusign_enabled?
            MembershipStatus.new(color: :green,  octicon: :skip,  label: "Skipped")
          elsif sponsors_listing&.docusign_voidable?
            MembershipStatus.new(color: :yellow, octicon: :clock, label: "Pending")
          elsif sponsors_listing&.docusign_completed?
            MembershipStatus.new(color: :green,  octicon: :check, label: "Completed")
          else
            MembershipStatus.new(color: :red,    octicon: :x,     label: "Not assigned")
          end
        end
      end
    end
  end
end
