# frozen_string_literal: true

module Stafftools
  module Sponsors
    class ToggleMatchComponent < ApplicationComponent
      def initialize(sponsorable:, listing:, membership:)
        @sponsorable = sponsorable
        @listing     = listing
        @membership  = membership
      end

      private

      attr_reader :sponsorable, :listing, :membership

      def render?
        return false if sponsorable.blank?
        return false if listing.blank?
        return false if membership.blank?
        return false if sponsorable.organization?
        membership.joined_waitlist_before_match_deadline?
      end

      def match_enabled?
        !listing.match_disabled?
      end

      def action
        match_enabled? ? "Disable" : "Enable"
      end

      def title
        "#{action} match globally"
      end

      def body
        "#{action} GitHub Sponsors matching for all sponsorships to this account."
      end

      def form_method
        match_enabled? ? :delete : :post
      end
    end
  end
end
