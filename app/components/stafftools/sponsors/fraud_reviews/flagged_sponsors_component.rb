# frozen_string_literal: true

class Stafftools::Sponsors::FraudReviews::FlaggedSponsorsComponent < ApplicationComponent
  # fraud_review - a SponsorsFraudReview.
  # flagged_records - an Array of FraudFlaggedSponsors.
  def initialize(fraud_review:, flagged_records:)
    @fraud_review     = fraud_review
    @flagged_records = flagged_records
  end

  private

  attr_reader :fraud_review, :flagged_records

  def render?
    fraud_review.present? && flagged_records.any?
  end
end
