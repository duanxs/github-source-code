# frozen_string_literal: true

class Stafftools::Sponsors::FraudReviews::SidebarComponent < ApplicationComponent
  include AvatarHelper

  DATE_FORMAT = "%b %-d, %Y"

  def initialize(listing:)
    @listing = listing
  end

  private

  def render?
    @listing.present?
  end

  def sponsorable
    @sponsorable ||= @listing.sponsorable
  end

  def stripe_connect_account
    @stripe_connect_account ||= @listing.stripe_connect_account
  end

  def sponsorable_login
    sponsorable.login
  end

  def profile_name
    sponsorable.profile&.name
  end

  def profile_bio
    return unless sponsorable.profile_bio.present?
    GitHub::Goomba::ProfileBioPipeline.to_html(sponsorable.profile_bio, {})
  end

  def join_date
    sponsorable.created_at.strftime(DATE_FORMAT)
  end

  def membership_accepted_date
    sponsorable.sponsors_membership&.reviewed_at&.strftime(DATE_FORMAT)
  end

  def listing_published_date
    @listing.published_at&.strftime(DATE_FORMAT)
  end

  def sponsors_count
    sponsorable.sponsorships_as_sponsorable.count
  end

  def has_stripe_account?
    stripe_connect_account.present?
  end

  def stripe_dashboard_url
    stripe_connect_account.stripe_dashboard_url
  end
end
