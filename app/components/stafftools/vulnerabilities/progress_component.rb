# frozen_string_literal: true

module Stafftools
  module Vulnerabilities
    class ProgressComponent < ApplicationComponent
      attr_reader :value, :max_value, :width, :color, :aria_labelledby

      def initialize(value: 0, max_value: 1, width: 100, color: "blue", loading: false, aria_labelledby: nil)
        @value = value
        @max_value = max_value
        @width = width
        @color = color
        @loading = !!loading
        @aria_labelledby = aria_labelledby
      end

      private

      def render?
        value.is_a?(Integer) && value >= 0 &&
          max_value.is_a?(Integer) && max_value > 0 &&
          value <= max_value
      end

      def loading?
        @loading
      end

      def value_percentage
        100 * value / max_value
      end

      def width_percentage
        [value_percentage, 100].min
      end

      def bar_style
        style = "width: #{width}px;"
        style += " opacity: 0.4;" if loading?

        style
      end

      def bar_title
        return "Loading…" if loading?

        "#{value_percentage}%"
      end
    end
  end
end
