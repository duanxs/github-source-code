# frozen_string_literal: true

module Stafftools
  module Vulnerabilities
    class VulnerableVersionRangeComponent < ApplicationComponent
      include PackageDependenciesHelper

      attr_reader :vulnerable_version_range

      delegate :affects, :requirements, :fixed_in,
        to: :vulnerable_version_range

      def initialize(vulnerable_version_range:)
        @vulnerable_version_range = vulnerable_version_range
      end

      private

      def render?
        vulnerable_version_range.present?
      end

      def writable?
        GitHub.writable_vulnerabilities_enabled?
      end

      def ecosystem
        ecosystem_label(vulnerable_version_range.ecosystem.upcase)
      end

      def ecosystem_color
        super(vulnerable_version_range.ecosystem.upcase)
      end

      def dependent_count_path
        dependent_count_stafftools_vulnerability_vulnerable_version_path(
          vulnerability_id: vulnerable_version_range.vulnerability_id,
          id: vulnerable_version_range.id,
        )
      end

      def show_alert_count?
        vulnerable_version_range.has_total_alerts_processed?
      end

      def alert_count
        vulnerable_version_range.total_alerts_processed
      end

      def show_alert_progress?
        currently_processing_alerts?
      end

      def alert_progress_labelledby
        "vulnerable-version-ranges-#{vulnerable_version_range.id}-alerts"
      end

      def alert_progress_path
        alert_progress_stafftools_vulnerability_vulnerable_version_path(
          vulnerability_id: vulnerable_version_range.vulnerability_id,
          id: vulnerable_version_range.id,
        )
      end

      def remove_path
        stafftools_vulnerability_vulnerable_version_path(
          vulnerability_id: vulnerable_version_range.vulnerability_id,
          id: vulnerable_version_range.id,
        )
      end

      def preview_alert_path
        preview_alert_stafftools_vulnerability_vulnerable_version_path(
          vulnerability_id: vulnerable_version_range.vulnerability_id,
          id: vulnerable_version_range.id,
        )
      end

      def alert_processing_disabled?
        !vulnerable_version_range.vulnerability.published? ||
          currently_processing_alerts?
      end

      def currently_processing_alerts?
        vulnerable_version_range.currently_processing_alerts?
      end

      def process_alerts_path
        process_alerts_stafftools_vulnerability_vulnerable_version_path(
          vulnerability_id: vulnerable_version_range.vulnerability_id,
          id: vulnerable_version_range.id,
        )
      end
    end
  end
end
