# frozen_string_literal: true

module Stafftools
  module Vulnerabilities
    class ShowComponent < ApplicationComponent
      attr_reader :vulnerability

      delegate :ghsa_id, :cve_id, :white_source_id,
        :pending?, :published?, :withdrawn?,
        :created_at, :published_at, :withdrawn_at,
        :created_by, :published_by, :withdrawn_by,
        :summary, :severity, :external_reference,
        to: :vulnerability

      def initialize(vulnerability:)
        @vulnerability = vulnerability
      end

      private

      def render?
        vulnerability.present?
      end

      def writable?
        GitHub.writable_vulnerabilities_enabled?
      end

      def edit_path
        edit_stafftools_vulnerability_path(vulnerability)
      end

      def publish_path
        publish_stafftools_vulnerability_path(vulnerability)
      end

      def withdraw_path
        withdraw_stafftools_vulnerability_path(vulnerability)
      end

      def visibility
        vulnerability.simulation? ? "Internal" : "Public"
      end

      def visibility_icon
        vulnerability.simulation? ? "beaker" : "globe"
      end

      def description_html
        GitHub::Goomba::MarkdownPipeline.to_html(vulnerability.description)
      end

      def new_vulnerable_version_range_path
        new_stafftools_vulnerability_vulnerable_version_path(vulnerability)
      end

      def alert_processing_disabled?
        !published? ||
          vulnerable_version_ranges.any?(&:currently_processing_alerts?)
      end

      def process_alerts_path
        create_range_alerts_stafftools_vulnerability_path(vulnerability)
      end

      def vulnerable_version_ranges
        return @vulnerable_version_ranges if defined? @vulnerable_version_ranges

        @vulnerable_version_ranges = vulnerability.vulnerable_version_ranges.to_a
      end

      def source
        case vulnerability.source
        when Stafftools::PendingVulnerabilitiesController::SOURCE_IDENTITY
          "site admin"
        when RepositoryAdvisory::SOURCE_IDENTITY
          "repository advisory"
        when AdvisoryDB::MungerDetection::SOURCE_IDENTITY
          "Munger"
        end
      end

      def repository_advisory
        return nil unless vulnerability.has_repository_advisory_source?

        vulnerability.repository_advisory
      end

      def alerting_events
        return @alerting_events if defined? @alerting_events

        @alerting_events = VulnerabilityAlertingEvent.
          where(vulnerability_id: vulnerability.id).
          order(:id).
          preload(:actor, :vulnerable_version_range_alerting_processes).
          to_a
      end
    end
  end
end
