# frozen_string_literal: true

module Stafftools
  module Vulnerabilities
    class MetadataPairComponent < ApplicationComponent
      HEX_COLOR_PATTERN = %r{\A#(?:[0-9a-f]{3}){1,2}\z}i

      with_content_areas :value_content, :help_content

      attr_reader :label, :value, :icon, :color, :blank_value, :label_id

      def initialize(label:, value: nil, icon: nil, color: nil, blank_value: "None", mono: false, link: false, show_clippy: false, label_id: nil)
        @label = label
        @value = value
        @icon = icon
        @color = color
        @blank_value = blank_value
        @mono = !!mono
        @link = !!link
        @show_clippy = show_clippy
        @label_id = label_id
      end

      private

      def render?
        label.present?
      end

      def mono?
        @mono
      end

      def link?
        @link
      end

      def show_clippy?
        @show_clippy
      end

      def color_class
        HEX_COLOR_PATTERN.match?(color) ? nil : "text-#{color}"
      end

      def color_style
        HEX_COLOR_PATTERN.match?(color) ? "color: #{color};" : nil
      end
    end
  end
end
