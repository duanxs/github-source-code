# frozen_string_literal: true

module Stafftools
  module Vulnerabilities
    class TimelineItemComponent < ApplicationComponent
      include AvatarHelper
      include BotHelper

      with_content_areas :suffix, :float

      attr_reader :actor, :action, :timestamp, :icon, :color

      def initialize(actor:, action:, timestamp:, icon: "primitive-dot", color: nil)
        @actor = actor
        @action = action
        @timestamp = timestamp
        @icon = icon
        @color = color
      end

      private

      def render?
        actor.present? && action.present? && timestamp.present?
      end
    end
  end
end
