# frozen_string_literal: true

module Issues
  class StateComponent < ApplicationComponent
    include OcticonsCachingHelper

    STATES = {
      open: {
        color: :green,
        octicon_name: "issue-opened",
      },
      closed: {
        color: :red,
        octicon_name: "issue-closed",
      },
    }.freeze

    def initialize(state:, size: Primer::StateComponent::SIZE_DEFAULT, **args)
      @state, @size, @args = state, size, args
    end

    private

    def render?
      STATES.keys.include?(@state)
    end

    def octicon_height
      @size == :small ? 14 : 16
    end

    def label
      @state.to_s.capitalize
    end
  end
end
