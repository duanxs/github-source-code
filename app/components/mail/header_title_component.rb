# frozen_string_literal: true

# Creates a centered icon + title for emails. Useful at the top of your email.
class Mail::HeaderTitleComponent < ApplicationComponent
  include ClassNameHelper
  attr_reader :classes, :title, :icon_url

  # See /public/images/email/icon for available icons. Or add your new icon there.
  #
  # We recommend using icons from: https://ghicons.github.com/
  def initialize(title:, icon: nil, classes: nil)
    @classes = class_names("btn", classes)
    @title = title

    if icon
      @icon_url  = image_url(icon)
    end
  end

  private

  def image_url(icon)
    "#{GitHub.mailer_asset_host_url}/images/email/icons/#{icon}"
  end
end
