# frozen_string_literal: true

module Stars
  class ButtonComponent < ApplicationComponent
    include AnalyticsHelper
    include HydroHelper
    include RepositoryAnalyticsHelper
    include UsersHelper

    SHOW_COUNT_DEFAULT = false
    SHOW_COUNT_OPTIONS = [SHOW_COUNT_DEFAULT, true]

    BUTTON_BLOCK_DEFAULT = false
    BUTTON_BLOCK_OPTIONS = [BUTTON_BLOCK_DEFAULT, true]

    def initialize(
      entity:,
      context:,
      starred: nil,
      show_count: SHOW_COUNT_DEFAULT,
      button_block: BUTTON_BLOCK_DEFAULT
    )
      @entity = entity
      @context = context
      @repository = entity.is_a?(Repository) ? entity : nil
      @starred = starred
      @button_block = fetch_or_fallback(BUTTON_BLOCK_OPTIONS, button_block, BUTTON_BLOCK_DEFAULT)

      if @button_block
        @show_count = false
      else
        @show_count = fetch_or_fallback(SHOW_COUNT_OPTIONS, show_count, SHOW_COUNT_DEFAULT)
      end
    end

    private

    def render?
      @entity.present? && @context.present?
    end

    def can_star?
      logged_in? && current_user.can_star?(@entity)
    end

    def is_starred?
      @starred || (logged_in? && current_user.starred?(@entity))
    end
  end
end
