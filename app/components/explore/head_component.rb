# frozen_string_literal: true

module Explore
  class HeadComponent < ApplicationComponent
    def initialize(integrations_directory_enabled:)
      @integrations_directory_enabled = fetch_or_fallback([true, false], integrations_directory_enabled, false)
    end
  end
end
