# frozen_string_literal: true

module Explore
  module Feed
    class RepositoryComponent < ApplicationComponent
      include AvatarHelper
      include ExploreHelper
      include UsersHelper
      include RepositoriesHelper
      include IssuesHelper
      include LabelsHelper
      include DashboardHelper
      include DashboardAnalyticsHelper

      def initialize(repository:, current_visitor:, label: "", recommendation: nil)
        @repository = repository
        @current_visitor = current_visitor
        @label = label
        @recommendation = recommendation

        @good_first_issues = nil
      end

      private

      def render?
        if @recommendation.present?
          @repository.present? && @recommendation.present? && @repository == @recommendation.repository
        else
          @repository.present? && @current_visitor.present?
        end
      end

      def shared_analytics
        return @shared_analytics if defined?(@shared_analytics)

        @shared_analytics = if logged_in? && @recommendation.present?
          {
            repository_id: @repository.id,
            repository_nwo: @repository.nwo,
            banner_dismissable: false,
            reason: @recommendation.reason,
            algorithm_version: @recommendation.algorithm_version,
            score: @recommendation.score,
            banner_shown: false,
            currently_following_users_count: currently_following_users_count,
            currently_starred_repos_count: currently_starred_repos_count,
            recommendation_generated_at: @recommendation.generated_at,
            public: @repository.public?,
          }
        else
          {}
        end
      end

      def good_first_issues
        @good_first_issues ||= ExploreFeed::RepositoryGoodFirstIssue
          .fetch_by_repo_id(@repository.id)
          .recommendable
          .repository_card_issues
      end

      def visible_good_first_issue_ids
        good_first_issues.limit(
          ExploreFeed::RepositoryGoodFirstIssue::Collection::REPOSITORY_CARD_VISIBLE_LIMIT,
        ).map(&:id)
      end

      def open_graph_image_url
        custom_image = @repository.open_graph_image
        if custom_image.present?
          custom_image.storage_external_url(current_user)
        else
          nil
        end
      end

      def show_open_graph_image?
        @repository.async_uses_custom_open_graph_image?.sync && open_graph_image_url.present?
      end

      def current_user_has_starred?
        @repository.stars.exists?(user: current_user)
      end

      def repository_description_html
        GitHub::HTML::DescriptionPipeline.to_html(@repository.description)
      end

      def parsed_issues_query
        [[:is, "open"], [:is, "issue"]]
      end

      def currently_following_users_count
        if logged_in?
          current_user.following_users_count
        else
          0
        end
      end

      def currently_starred_repos_count
        if logged_in?
          current_user.starred_repos_count
        else
          0
        end
      end

      # Used by ExploreHelper#explore_click_tracking_attributes.
      def explore_click_context
        :REPOSITORY_CARD
      end

      # Required for IssuesHelper#issues_search_query, which is used by
      # LabelsHelper#label_with_description.
      #
      # This is nil for consistency with the implementation used by views
      # rendered by ExploreController, which does not override the default
      # implementation in ApplicationController.
      def current_repository
        nil
      end

      # Required for DashboardHelper#instrument_recommendation
      def request_id
        request.env[Rack::RequestId::GITHUB_REQUEST_ID]
      end
    end
  end
end
