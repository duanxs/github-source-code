# frozen_string_literal: true

module Explore
  module Recommendation
    class DeveloperComponent < ApplicationComponent
      include ExploreHelper
      include ::TextHelper # app/helpers/text_helper.rb, not ActionView::Helpers::TextHelper

      def initialize(developers:)
        @developers = developers
      end

      private

      def label
        if @developers.first.reasons&.any?
          "Based on a topic you're interested in"
        else
          "These users are popular on GitHub right now"
        end
      end

      def render?
        @developers&.any?
      end

      def explore_click_context
        :RECOMMENDED_DEVELOPERS_CARD
      end
    end
  end
end
