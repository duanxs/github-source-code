# frozen_string_literal: true

class MergeQueues::ActiveMergeGroupComponent < ApplicationComponent
  def initialize(merge_queue:)
    @merge_queue = merge_queue
  end

  private

  attr_reader :merge_queue

  def render?
    return false unless GitHub.merge_queues_enabled?
    return false if merge_queue.blank?
    return false unless GitHub.flipper[:merge_queue].enabled?(repository)

    active_merge_group.present?
  end

  def repository
    @repository ||= merge_queue.repository
  end

  def active_merge_group
    @active_merge_group ||= merge_queue.active_merge_group
  end

  def entries
    @entries ||= active_merge_group.queue_entries
  end

  def green?
    entries.all?(&:mergeable?)
  end

  def merge_conflicts?
    entries.any?(&:blocked_by_merge_conflicts?)
  end

  def ci_failing?
    entries.any?(&:required_status_failing?)
  end

  def ci_pending?
    entries.any?(&:required_status_pending?)
  end

  def merge_status_class
    if green?
      "text-green"
    elsif merge_conflicts? || ci_failing?
      "text-red"
    else
      "text-yellow"
    end
  end

  def merge_status_icon
    if green?
      "check-circle-fill"
    elsif merge_conflicts? || ci_failing?
      "x-circle-fill"
    else
      "check-circle-fill"
    end
  end

  def merge_status_description
    if merge_conflicts?
      "Merge conflicts with #{repository.default_branch}."
    elsif ci_failing?
      "Checks are failing."
    elsif ci_pending?
      "Checks are pending."
    else
      "All checks passing, no conflicts with #{repository.default_branch}."
    end
  end
end
