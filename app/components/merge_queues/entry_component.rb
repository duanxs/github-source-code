# frozen_string_literal: true

class MergeQueues::EntryComponent < ApplicationComponent
  include AvatarHelper
  include PullRequestsHelper

  def initialize(entry:, repository:, next_page_path: nil)
    @entry = entry
    @repository = repository
    @next_page_path = next_page_path
  end

  private

  attr_reader :entry, :repository, :next_page_path

  def render?
    return false unless GitHub.merge_queues_enabled?
    return false if entry.blank?
    return false if repository.blank?

    GitHub.flipper[:merge_queue].enabled?(repository)
  end

  def pull_request
    entry.pull_request
  end

  def enqueued_by_viewer?
    return false if current_user.blank?
    entry.enqueuer_id == current_user.id
  end

  def status_icon_class
    if entry.mergeable?
      "text-green"
    elsif entry.blocked_by_other_than_pending_required_status?
      "text-red"
    else
      "text-yellow"
    end
  end

  def status_icon
    if entry.mergeable?
      "check-circle-fill"
    elsif entry.blocked_by_other_than_pending_required_status?
      "x-circle-fill"
    else
      "check-circle-fill"
    end
  end

  def branch_label_class
    if entry.mergeable?
      ""
    elsif entry.blocked_by_other_than_pending_required_status?
      "bg-red-light"
    else
      "bg-yellow-light"
    end
  end

  def branch_link_class
    if entry.mergeable?
      ""
    elsif entry.blocked_by_other_than_pending_required_status?
      "text-red"
    else
      "text-yellow"
    end
  end
end
