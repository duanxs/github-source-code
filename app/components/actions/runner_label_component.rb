# frozen_string_literal: true

class Actions::RunnerLabelComponent < ApplicationComponent
  attr_reader :label

  def initialize(label:, selected: false, indeterminate: false)
    @label = label
    @selected = selected
    @indeterminate = indeterminate
  end

  def selected?
    @selected
  end

  def indeterminate?
    @indeterminate
  end
end
