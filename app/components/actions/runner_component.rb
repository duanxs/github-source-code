# frozen_string_literal: true

module Actions
  class RunnerComponent < ApplicationComponent
    delegate :id, :name, :system_labels, :custom_labels, to: :runner
    delegate :os, :status, :custom_label_ids, to: :runner, private: true

    def initialize(
      runner:,
      owner_settings:,
      hidden: false,
      is_child_row: false,
      can_manage_runners: true,
      group_id: 0
    )
      @runner, @owner_settings, @hidden, @is_child_row, @can_manage_runners, @group_id =
        runner, owner_settings, hidden, is_child_row, can_manage_runners, group_id
    end

    def readable_os
      case os
        when "macos" then "macOS"
        when "unknown" then "" # don't show anything. Should only occur with staff-shipped old yaml syntax
        else os.titleize
      end
    end

    def icon_color_class
      case status
        when Actions::Runner::IDLE then :green
        when Actions::Runner::ACTIVE then :yellow
        else :gray_light
      end
    end

    def can_manage_runners?
      @can_manage_runners && @owner_settings.can_manage_runners?
    end

    def delete_path
      @owner_settings.delete_runner_path(id: id, os: os)
    end

    def labels_path
      @owner_settings.labels_path(runner_id: id, selected_labels: custom_label_ids)
    end

    private

    attr_reader :runner
  end
end
