# frozen_string_literal: true

module Actions
  class RunnerGroupsBulkComponent < ApplicationComponent

    def initialize(owner:, owner_settings:, runner_ids:)
      @owner = owner
      @owner_settings = owner_settings
      @runner_ids = runner_ids
    end
  end
end
