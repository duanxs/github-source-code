# frozen_string_literal: true

module Actions
  class RunnerGroupsComponent < ApplicationComponent
    with_content_areas :description

    def initialize(owner:, owner_settings:, runner_groups: [], restricted_plan: false, has_business: false)
      @owner = owner
      @owner_settings = owner_settings
      @runner_groups = runner_groups.sort
      @restricted_plan = restricted_plan
      @has_business = has_business
    end

    def delete_runner_group_path(runner_group:)
      @owner_settings.delete_runner_group_path(id: runner_group.id)
    end

    def bulk_actions_path
      @owner_settings.bulk_runner_groups_path
    end

    def show_public_visiblity_warning?
      # Groups owned by an enterprise cannot be made available to public repos
      return false unless @owner.organization?

      @runner_groups.reject { |g| g.inherited? }.any? { |g| g.visibility == GitHub::LaunchClient::RunnerGroups::GROUP_VISIBILITY_ALL }
    end

    def visibility_for(runner_group)
      target = @owner.organization? ? "repositories" : "organizations"
      parts = [
        GitHub::LaunchClient::RunnerGroups::TO_VISIBILITY_MAP[runner_group.visibility].upcase_first,
        target,
      ]

      if runner_group.visibility == GitHub::LaunchClient::RunnerGroups::GROUP_VISIBILITY_SELECTED
        parts << "(#{runner_group.selected_targets.count})"
      end

      parts.join(" ")
    end
  end
end
