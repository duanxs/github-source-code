# frozen_string_literal: true

module Organizations
  class CreationSurveyComponent < ApplicationComponent
    ALLOW_CUSTOM_OTHER_TEXT_INPUT = ["org_creator_role", "org_purpose", "org_next_seven_days", "org_existing_repo_source"]
    HIDDEN_QUESTION = ["org_existing_repo_source"]

    CHOICES_TYPE = {
      MarketingSurvey::MarketingChoiceComponent::TILE_TYPE => [
        "org_purpose_open_source",
        "org_purpose_education",
        "org_purpose_personal",
        "org_purpose_work",
      ],
      MarketingSurvey::MarketingChoiceComponent::LIST_ITEM_TYPE => [
        "org_creator_role_coding",
        "org_creator_role_managing",
        "org_creator_role_planning",
        "org_creator_role_billing",
        "org_size_0",
        "org_size_1_to_5",
        "org_size_6_to_15",
        "org_size_25_plus",
        "org_existing_repository_yes",
        "org_existing_repository_no",
        "org_next_seven_days_manage_code",
        "org_next_seven_days_collaborate_code",
        "org_next_seven_days_plan",
        "org_next_seven_days_ci_cd",
        "org_next_seven_days_security",
        "org_existing_repo_source_github",
        "org_existing_repo_source_gitlab",
        "org_existing_repo_source_bitbucket",
        "org_existing_repo_source_sourceforge",
        "org_existing_repo_source_local_storage",
      ],
      MarketingSurvey::MarketingChoiceComponent::TEXT_INPUT_TYPE => [
        "org_creator_role_other",
        "org_purpose_other",
        "org_next_seven_days_other",
        "org_existing_repo_source_other"
      ]
    }

    def initialize(survey:, organization:)
      @survey, @organization = survey, organization
    end

    def submit_button_data_attributes
      { ga_click: "Org creation survey, survey submit" }.merge(
        test_selector_hash("org-survey-submit")
      )
    end

    def question_initially_hidden?(question)
      HIDDEN_QUESTION.include?(question.short_text)
    end

    def choice_type(choice)
      CHOICES_TYPE.find { |layout, choices| choices.include?(choice.short_text) }&.first
    end

    def choice_image(choice)
      case choice.short_text
      when "org_creator_role_coding"
        { type: :octicon, name: "code" }
      when "org_creator_role_managing"
        { type: :octicon, name: "file-code" }
      when "org_creator_role_planning"
        { type: :octicon, name: "project" }
      when "org_creator_role_billing"
        { type: :octicon, name: "credit-card" }
      when "org_purpose_open_source"
        { type: :svg, name: "code-to-cloud" }
      when "org_purpose_education"
        { type: :svg, name: "school-work" }
      when "org_purpose_personal"
        { type: :svg, name: "new-user" }
      when "org_purpose_work"
        { type: :svg, name: "see-project-activity" }
      when "org_next_seven_days_manage_code"
        { type: :octicon, name: "gist" }
      when "org_next_seven_days_plan"
        { type: :octicon, name: "checklist" }
      when "org_next_seven_days_collaborate_code"
        { type: :octicon, name: "comment-discussion" }
      when "org_next_seven_days_ci_cd"
        { type: :octicon, name: "workflow" }
      when "org_next_seven_days_security"
        { type: :octicon, name: "shield-check" }
      when "org_existing_repo_source_github"
        { type: :svg, name: "github" }
      when "org_existing_repo_source_gitlab"
        { type: :svg, name: "gitlab" }
      when "org_existing_repo_source_bitbucket"
        { type: :svg, name: "bitbucket" }
      when "org_existing_repo_source_sourceforge"
        { type: :svg, name: "sourceforge" }
      when "org_existing_repo_source_local_storage"
        { type: :octicon, name: "repo-push" }
      end
    end

    def choice_container_classes(choice)
      case choice.short_text
      when "org_creator_role_coding",
        "org_creator_role_managing",
        "org_creator_role_planning",
        "org_creator_role_billing",
        "org_creator_role_other",
        "org_purpose_other",
        "org_next_seven_days_other",
        "org_existing_repo_source_other",
        "org_existing_repo_source_github",
        "org_existing_repo_source_gitlab",
        "org_existing_repo_source_bitbucket",
        "org_existing_repo_source_sourceforge",
        "org_existing_repo_source_local_storage"
          "col-12 col-sm-6 col-md-6"
      when "org_next_seven_days_manage_code",
        "org_next_seven_days_collaborate_code",
        "org_next_seven_days_plan",
        "org_next_seven_days_ci_cd",
        "org_next_seven_days_security"
          "width-full"
      end
    end

    def choice_container_style(choice)
      case choice.short_text
      when "org_existing_repo_source_other"
        "flex-basis: 100%;"
      end
    end

    def choice_input_style(choice)
      case choice.short_text
      when "org_existing_repo_source_other"
        "max-width: 48%;"
      end
    end

    def choices_container_classes(question)
      case question.short_text
      when "org_next_seven_days"
        "d-block"
      else
        "d-flex flex-wrap"
      end
    end

    def choices_add_input_classes(choice)
      case choice.short_text
      when "org_existing_repository_yes"
        "js-show-next-question-trigger"
      end
    end

    def question_description(question)
      case question.short_text
      when "org_creator_role"
        "Tell us about you"
      when "org_size"
        "Tell us about your team"
      end
    end

    def question_container_classes(question)
      case question.short_text
      when "org_creator_role"
        "border-bottom pb-6"
      when "org_existing_repository"
        "js-show-next-question"
      when "org_existing_repo_source"
        "js-hidden-question"
      end
    end

    private

    def render?
      @survey.active_questions.count > 0
    end
  end
end
