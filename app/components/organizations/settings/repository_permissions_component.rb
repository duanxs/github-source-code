# frozen_string_literal: true

class Organizations::Settings::RepositoryPermissionsComponent < ApplicationComponent

  PERMISSIONS = {
    ALL_REPOSITORIES: {
      label: "All repositories",
      description: "Self-hosted runners can be used by public and private repositories",
    },
    PRIVATE_REPOSITORIES: {
      label: "Private repositories",
      description: "Self-hosted runners can only be used by private repositories",
    },
    SELECTED_REPOSITORIES: {
      label: "Selected repositories",
      description: "Self-hosted runners can only be used by specifically selected repositories",
    }
  }

  DEFAULT_PERMISSION_TYPE = :PRIVATE_REPOSITORIES
  SELECTED_REPOSITORIES_TYPE = :SELECTED_REPOSITORIES

  def initialize(access_policy:)
    @permission_type = access_policy.permission_type
  end

end
