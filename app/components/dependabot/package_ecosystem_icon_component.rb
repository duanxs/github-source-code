# frozen_string_literal: true

module Dependabot
  class PackageEcosystemIconComponent < ApplicationComponent

    def initialize(package_ecosystem:)
      @package_ecosystem = package_ecosystem
    end

    def icon_name
      Dependabot.serialize_package_ecosystem(package_ecosystem: @package_ecosystem)
    end
  end
end
