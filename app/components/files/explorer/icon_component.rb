# frozen_string_literal: true

module Files
  module Explorer
    class IconComponent < ApplicationComponent
      def initialize(type:, **args)
        @args = args

        @label, @color, @icon = case type
          when :directory
            ["Directory", "blue-3", "file-directory"]
          when :submodule
            ["Submodule", "blue-3", "file-submodule"]
          when :symlink_directory
            ["Symlink Directory", "blue-3", "file-symlink-directory"]
          when :symlink_file
            ["Symlink File", "gray-light", "file-symlink-file"]
          else
            ["File", "gray-light", "file"]
        end
      end
    end
  end
end
