# frozen_string_literal: true

module Repositories
  class ListItemComponent < ApplicationComponent
    include ApplicationHelper
    include GraphsHelper
    include ::CacheHelper
    include ::TextHelper

    def initialize(repository:, organization: nil, is_registry_enabled: false, responsive: false, **args)
      @repository, @organization, @is_registry_enabled, @responsive, @args = \
        repository, organization, is_registry_enabled, responsive, args
    end

    private

    def render?
      repository.present?
    end

    def pull_request_count
      capped_number_with_delimiter(repository.open_pull_request_count_for(current_user), limit: 5_000)
    end

    def issue_count
      capped_number_with_delimiter(repository.open_issue_count_for(current_user), limit: 5_000)
    end

    def issue_count_needing_help
      return unless community_profile

      @issue_count_needing_help ||= if link_to_help_wanted?
        community_profile.help_wanted_issues_count
      elsif link_to_good_first_issue?
        community_profile.good_first_issue_issues_count
      end
    end

    def issues_needing_help_path
      return unless community_profile

      query = if link_to_help_wanted?
        label = repository.help_wanted_label
        "label:\"#{label.name}\""
      elsif link_to_good_first_issue?
        label = repository.good_first_issue_label
        "label:\"#{label.name}\""
      end

      if query
        query += " is:issue is:open"
        issues_path(repository.owner, repository, q: query)
      end
    end

    def community_profile
      repository.community_profile
    end

    def show_issues_needing_help?
      return false unless repository.has_issues?
      link_to_help_wanted? || link_to_good_first_issue?
    end

    def link_to_help_wanted?
      community_profile && community_profile.help_wanted_issues_count > 0 &&
        repository.help_wanted_label
    end

    def link_to_good_first_issue?
      return false if link_to_help_wanted?

      community_profile && community_profile.good_first_issue_issues_count > 0 &&
        repository.good_first_issue_label
    end

    def topic_path(topic)
      qualifiers = ["topic:#{topic}"]
      qualifiers << "fork:true" if repository.fork?
      qualifiers << "org:#{repository.owner}" if repository.owner.organization?
      search_path(q: qualifiers.join(" "), type: "Repositories")
    end

    def topics
      @topics ||= repository.topics.limit(7).pluck(:name)
    end

    def show_owner_prefix?
      return false unless organization.present?
      repository.owner != organization
    end

    def type
      RepositoriesTypeHelper.type(
        visibility: repository.visibility,
        mirror: repository.mirror,
        archived: repository.archived?,
        template: repository.template?,
      )
    end

    def show_type?
      repository.private? || repository.archived? || repository.mirror
    end

    def component_class_names
      out = []

      out << (repository.public? ? "public" : "private")
      out << (repository.fork? ? "fork" : "source")
      out << "mirror" if repository.mirror
      out << "archived" if repository.archived?

      out.join(" ")
    end

    def overview_cache_key
      version = 22
      "orgs:overview:repo:#{version}:#{repository.id}:#{repository.owner}:#{repository.pushed_at.to_i}:#{repository.updated_at.to_i}:#{responsive}"
    end

    def license
      repository.license
    end

    def show_license?
      license && !license.other?
    end

    def license_name
      license.try(:spdx_id)
    end

    def repository_participation_sparkline_cached?
      # TODO Replace with Rails fragment_exist? when our custom caching overrides are removed.
      controller.read_fragment(repository_participation_sparkline_cache_key(repository)).present?
    end

    attr_reader :repository, :organization, :is_registry_enabled, :responsive
  end
end
