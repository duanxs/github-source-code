# frozen_string_literal: true

module Repositories
  class ConfigurationRepositoryCallToActionComponent < ApplicationComponent
    include ProfilesHelper

    FEEDBACK_LINK = "https://support.github.com/contact/feedback?category=profile&subject=Profile+README"

    attr_reader :readme
    attr_reader :repository
    attr_reader :tree_name
    attr_reader :user

    def initialize(user:, repository:, tree_name: nil)
      @user = user
      @repository = repository
      @tree_name = tree_name || repository.default_branch

      @opted_in = user&.profile_readme_opt_in?
      @private = repository.private?
      @readme = repository.preferred_readme
    end

    def private?
      @private
    end

    def opted_in?
      @opted_in
    end

    def profile_readme_feedback_link
      FEEDBACK_LINK if show_profile_readme_feedback_link? @repository, @user
    end

    def render?
      return false unless user.present?
      return false if repository.owner != user

      repository.user_configuration_repository?
    end
  end
end
