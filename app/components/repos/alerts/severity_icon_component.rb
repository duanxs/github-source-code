# frozen_string_literal: true

module Repos
  module Alerts
    class SeverityIconComponent < ApplicationComponent
      SEVERITY_SCHEME = {
        "critical" => :red,
        "high" =>  :orange,
        "moderate" => :yellow,
        "low"  => :gray_dark
      }

      def initialize(severity:, withdrawn: false, **args)
        @severity = severity&.downcase
        @withdrawn = withdrawn
        @args = args
      end

      def call
        render GitHub::OcticonComponent.new(**{ icon: icon, color: scheme }.merge(@args))
      end

      def render?
        @severity.present?
      end

      private

      def scheme
        SEVERITY_SCHEME[@severity]
      end

      def icon
        if @withdrawn
          "shield-x"
        else
          "shield"
        end
      end
    end
  end
end
