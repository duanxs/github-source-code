# frozen_string_literal: true

module GitHub
  class DialogComponent < ApplicationComponent
    with_content_areas :summary, :alert, :footer, :body

    VARIANTS = {
      default: "Box--overlay",
      narrow: "Box-overlay--narrow",
      wide: "Box-overlay--wide",
    }.freeze

    def initialize(title:, id: nil, src: nil, preload: false, variant: :default, details_classes: nil)
      @title = title
      @id = id
      @src = src
      @preload = preload
      @variant_classes = VARIANTS.fetch(variant, :default)
      @details_classes = details_classes
    end

    private

    attr_reader :title, :id, :src, :preload
  end
end
