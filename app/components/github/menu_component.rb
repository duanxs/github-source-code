# frozen_string_literal: true

module GitHub
  class MenuComponent < ApplicationComponent
    include ClassNameHelper

    with_content_areas :header, :extra, :summary, :body

    ALIGN_DEFAULT = :left
    ALIGN_RIGHT = :right
    ALIGN_OPTIONS = [ALIGN_DEFAULT, ALIGN_RIGHT]

    def initialize(
      items: [],
      title: nil,
      text: nil,
      filterable: false,
      src: nil,
      preload: false,
      align: ALIGN_DEFAULT,
      filter_placeholder: nil
    )
      @items = items
      @title = title
      @text = text
      @filterable = filterable
      @src = src
      @preload = preload
      @align = fetch_or_fallback(ALIGN_OPTIONS, align, ALIGN_DEFAULT)
      @filter_placeholder = filter_placeholder
    end

    def id
      @id ||= SecureRandom.hex(3)
    end

    def default_summary
      GitHub::MenuSummaryComponent.new(
        classes: "btn btn-sm select-menu-button",
        text: replaceable? ? @text : (@text || @title),
        menu: self)
    end

    def default_selection_text
      @items.find { |item| item.checked && item.replace_text.present? }&.replace_text
    end

    def replaceable?
      @replaceable ||= @items.any? { |item| item.replace_text.present? }
    end

    def details_menu_class_names
      class_names(
        "SelectMenu",
        "SelectMenu--hasFilter" => @filterable,
        "right-0" => @align == ALIGN_RIGHT
      )
    end
  end
end
