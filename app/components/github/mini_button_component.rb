# frozen_string_literal: true

module GitHub
  class MiniButtonComponent < ApplicationComponent
    def initialize(**kwargs)
      @kwargs = kwargs
      @kwargs[:variant] = :small
      @kwargs[:tag] = :a
    end

    def call
      render(Primer::ButtonComponent.new(**@kwargs)) { content }
    end
  end
end
