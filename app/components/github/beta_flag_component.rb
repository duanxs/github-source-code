# frozen_string_literal: true

module GitHub
  class BetaFlagComponent < ApplicationComponent
    def initialize(**kwargs)
      @kwargs = kwargs
      @kwargs[:variant] ||= :inline
      @kwargs[:px] ||= 1
      @kwargs[:scheme] = :green_outline
      @kwargs[:title] = "Feature Release Label: Beta"
    end

    def call
      render(Primer::LabelComponent.new(**@kwargs)) { "Beta" }
    end
  end
end
