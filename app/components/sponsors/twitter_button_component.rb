# frozen_string_literal: true

class Sponsors::TwitterButtonComponent < ApplicationComponent
  include SvgHelper

  # https://developer.twitter.com/en/docs/twitter-for-websites/tweet-button/guides/web-intent
  TWITTER_WEB_INTENT_URL = "https://twitter.com/intent/tweet"

  def initialize(
    sponsorable:,
    label: "Tweet it",
    label_class: nil,
    text: "",
    data: {},
    url_params: {},
    render_textarea: false,
    autofocus: false
  )
    @sponsorable = sponsorable
    @label = label
    @label_class = label_class
    @text = text
    @data = data
    @url_params = url_params
    @render_textarea = render_textarea
    @autofocus = autofocus
  end

  private

  # We use query params to know whether a Sponsors profile page view came from Twitter or not
  # and how it was shared (e.g. the sponsorable, the sponsor, etc.)
  #
  #  `sc` - stands for "source" and `t` represents Twitter
  def sponsors_profile_url
    query_params = @url_params.merge(
      Sponsors::TrackingParameters.new(
        source: Sponsors::TrackingParameters::TWITTER_SOURCE,
      ).to_h
    ).to_query

    "https://github.com/sponsors/#{@sponsorable.login}?#{query_params}"
  end
end
