# frozen_string_literal: true

module Sponsors
  module Orgs
    class SponsoringComponent < ApplicationComponent
      include AvatarHelper
      include SponsorsButtonsHelper

      def initialize(sponsor:, sponsorships:)
        @sponsor = sponsor
        @sponsorships = sponsorships
      end

      private

      def render?
        return false unless @sponsor
        return false unless @sponsor.organization?
        current_user&.corporate_sponsors_credit_card_enabled?
      end

      def sponsorships
        return @sponsorships if current_user_is_sponsor_admin?
        @sponsorships.privacy_public
      end

      def sponsorships_count
        @sponsorships_count ||= sponsorships.total_entries
      end

      def show_sponsorships?
        sponsorships.any?
      end

      def sponsoring_overview_text
        if sponsorships_count == 1
          "organization or developer."
        elsif sponsorships_count > 1
          "organizations and developers."
        end
      end

      def current_user_is_sponsor_admin?
        return false unless logged_in?
        @is_sponsor_admin ||= current_user.potential_sponsor_logins.include?(@sponsor.login)
      end

      def sponsorable_bio(sponsorable)
        sponsorable.sponsors_membership.featured_description || sponsorable.profile_bio
      end
    end
  end
end
