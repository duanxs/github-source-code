# frozen_string_literal: true

class Sponsors::Payouts::LastPayoutComponent < ApplicationComponent
  def initialize(payout: nil, sponsorable:)
    @payout = payout
    @sponsorable = sponsorable
  end

  private

  attr_reader :sponsorable

  def render?
    @sponsorable.present?
  end

  def payout?
    @payout.present?
  end

  def payout
    return unless payout?

    # avoid confusion between USD and other currencies using $ by postfixing
    # their three-letter currency code.
    currency = Money::Currency.new(@payout.currency)
    currency_suffix = (currency.id != :usd) && (currency.symbol == "$")
    Billing::Money.new(@payout.amount, @payout.currency)
                  .format(with_currency: currency_suffix)
  end

  def payout_pending?
    return false unless payout?

    @payout.status == "pending"
  end

  def payout_failed?
    return false unless payout?

    @payout.status == "failed"
  end

  def failure_message
    if failure_reasons.include?(@payout.failure_code)
      "Your last payout failed because of incorrect bank account details."
    else
      "Your last payout failed."
    end
  end

  def failure_action
    return unless failure_reasons.include?(@payout.failure_code)

    "update your bank information"
  end

  def payout_formatted_created_at
    Time.at(@payout.created)
        .strftime("%b %d")
  end

  def failure_reasons
    Billing::StripeConnect::Account::PAYOUT_FAILURE_REASONS
  end
end
