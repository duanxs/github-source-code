# frozen_string_literal: true

class Sponsors::Payouts::NextPayoutComponent < ApplicationComponent
  def initialize(sponsorable:, balance: nil, error: nil, needs_tax_form: false)
    @sponsorable = sponsorable
    @balance = balance
    @error = error
    @needs_tax_form = needs_tax_form
  end

  private

  attr_reader :error

  def render?
    @sponsorable.present?
  end

  def balance
    return unless balance?

    amount = @balance.available.sum { |b| b[:amount] }
    balance_currency = @balance.available.first.currency

    # avoid confusion between USD and other currencies using $ by postfixing
    # their three-letter currency code.
    currency = Money::Currency.new(balance_currency)
    currency_suffix = (currency.id != :usd) && (currency.symbol == "$")
    Billing::Money.new(amount, balance_currency)
                  .format(with_currency: currency_suffix)
  end

  def balance?
    return false if @balance.blank?

    @balance.available.any?
  end

  def needs_tax_form?
    @needs_tax_form
  end

  def tax_form_path
    sponsorable_dashboard_path(@sponsorable, anchor: "tax-form")
  end

  def error?
    @error.present?
  end

  def next_payout_date?
    listing.next_payout_date.present?
  end

  def next_payout_date
    listing.next_payout_date
           .strftime("%b %d")
  end

  def probation_days
    SponsorsListing::PAYOUT_PROBATION_DAYS
  end

  def help_link
    "#{GitHub.help_url}/articles/managing-your-payouts-from-github-sponsors#about-payouts-from-github-sponsors"
  end

  def listing
    @listing ||= @sponsorable.sponsors_listing
  end
end
