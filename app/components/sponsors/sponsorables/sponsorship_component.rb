# frozen_string_literal: true

module Sponsors
  module Sponsorables
    class SponsorshipComponent < ApplicationComponent
      def initialize(sponsorable:,
                     sponsor: nil,
                     sponsorship: nil,
                     selected_tier:,
                     privacy_level: "public",
                     opted_in_to_email: true,
                     plan_change: nil)
        @sponsorable = sponsorable
        @sponsor = sponsor
        @sponsorship = sponsorship
        @selected_tier = selected_tier
        @current_tier = @sponsorship&.tier
        @privacy_level = privacy_level
        @opted_in_to_email = opted_in_to_email
        @plan_change = plan_change
      end

      private

      def render?
        logged_in? && @sponsor.present?
      end

      def form_method
        if editing?
          :put
        else
          :post
        end
      end

      def public?
        @privacy_level == "public"
      end

      def private?
        !public?
      end

      def form_initially_disabled?
        return true if @sponsor.id == @sponsorable.id
        return true unless @sponsor.payment_method&.valid?
        return true if editing_but_unchanged_tier?
        false
      end

      def submit_text
        if editing?
          "Update sponsorship"
        else
          "Sponsor #{@sponsorable.login}"
        end
      end

      def disable_with_text
        if editing?
          "Updating sponsorship"
        else
          "Sponsoring #{@sponsorable.login}"
        end
      end

      def disabled_reason
        if @sponsor.id == @sponsorable.id
          "You're previewing your Sponsors checkout page, and can't sponsor yourself"
        elsif !@sponsor.payment_method&.valid?
          "You need a valid payment method"
        elsif editing_but_unchanged_tier?
          "You need to change something to update your sponsorship"
        else
          "You have a pending cancellation"
        end
      end

      def subscription_duration
        @sponsor.plan_duration.downcase
      end

      def prorated_new_plan_price
        @plan_change.price.format
      end

      def total_new_plan_price
        @plan_change.item.price.format
      end

      def plan_change?
        @plan_change&.item.present?
      end

      def editing?
        @current_tier.present?
      end

      def editing_but_unchanged_tier?
        return false unless editing?
        return false unless @selected_tier

        @current_tier.id == @selected_tier.id
      end

      def current_subscription_item
        @sponsor
          .subscription_items
          .detect { |item| item.subscribable == @current_tier }
      end

      def next_billing_date
        date = @sponsor.next_billing_date
        if @sponsor.monthly_plan?
          date.strftime("%b #{date.day.ordinalize}")
        else
          date.strftime("%b #{date.day.ordinalize}, %Y")
        end
      end
    end
  end
end
