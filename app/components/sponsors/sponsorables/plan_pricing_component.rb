# frozen_string_literal: true

module Sponsors
  module Sponsorables
    class PlanPricingComponent < ApplicationComponent
      def initialize(
          sponsorable:,
          sponsor: nil,
          plan_change: nil,
          pending_tier_change: nil
        )
        @sponsorable = sponsorable
        @sponsor = sponsor
        @plan_change = plan_change
        @pending_tier_change = pending_tier_change
      end

      private

      def render?
        return false unless logged_in?
        return false unless @sponsor
        return true if plan_change?

        pending_tier_change?
      end

      # There's no new price if we're not downgrading
      #
      # Returns a Number or Nil
      def new_plan_amount
        if plan_change?
          @plan_change.item.price(trial_price: false)
        else
          @pending_tier_change.price
        end.format
      end

      def next_billing_date
        date = @sponsor.next_billing_date

        format_date(date)
      end

      def plan_duration
        @sponsor.plan_duration.downcase
      end

      def prorated_price
        @plan_change.price.abs.format
      end

      def proration_start_date
        format_date(GitHub::Billing.today)
      end

      def proration_end_date
        date = @sponsor.subscription.service_ends_on.to_time(:utc)
        format_date(date)
      end

      def format_date(date)
        if @sponsor.monthly_plan?
          date.strftime("%b #{date.day.ordinalize}")
        else
          date.strftime("%b #{date.day.ordinalize}, %Y")
        end
      end

      def upgrade?
        return false unless plan_change?

        @plan_change.price.positive?
      end

      def plan_change?
        return false if @plan_change.nil?
        @plan_change.item.present?
      end

      def pending_tier_change?
        return false unless @pending_tier_change

        !@pending_tier_change.cancellation?
      end
    end
  end
end
