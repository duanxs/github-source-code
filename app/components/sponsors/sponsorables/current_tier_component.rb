# frozen_string_literal: true

module Sponsors
  module Sponsorables
    class CurrentTierComponent < ApplicationComponent
      def initialize(sponsorship:)
        @sponsorship = sponsorship
      end

      private

      def render?
        logged_in? && @sponsorship.present?
      end

      def manage_button_path
        sponsorable_sponsorships_path(
          sponsorable,
          sponsor: sponsor.login,
          tier_id: tier.id
        )
      end

      def pending_downgrade?
        return false unless pending_tier_change?
        !pending_tier_change.cancellation?
      end

      def pending_cancellation?
        return false unless pending_tier_change?
        pending_tier_change.cancellation?
      end

      def pending_tier_change
        return @pending_tier_change if defined?(@pending_tier_change)
        @pending_tier_change = @sponsorship.pending_tier_change
      end

      def previous_tier
        return nil unless pending_downgrade?
        @sponsorship.tier
      end

      def pending_tier_change?
        pending_tier_change.present?
      end

      def next_billing_date
        date = sponsor.next_billing_date
        if sponsor.yearly_plan?
          date.strftime("%b #{date.day.ordinalize}, %Y")
        else
          date.strftime("%b #{date.day.ordinalize}")
        end
      end

      def sponsorable
        @sponsorship.sponsorable
      end

      def sponsor
        @sponsorship.sponsor
      end

      def tier
        if pending_downgrade?
          pending_tier_change.subscribable
        else
          @sponsorship.tier
        end
      end
    end
  end
end
