# frozen_string_literal: true

module Sponsors
  module Sponsorables
    class PendingCancellationNoticeComponent < ApplicationComponent
      def initialize(next_billing_date)
        @next_billing_date = next_billing_date
      end
    end
  end
end
