# frozen_string_literal: true

class Sponsors::Dashboard::Profile::FeaturedWorkComponent < ApplicationComponent
  def initialize(sponsorable:)
    @sponsorable = sponsorable
  end

  private

  def featured_repositories
    @featured_repos ||= @sponsorable.sponsors_listing.featured_repos
  end
end
