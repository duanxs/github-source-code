# frozen_string_literal: true

class Sponsors::Dashboard::Profile::FeaturedWorkSearchModalComponent < ApplicationComponent
  def initialize(sponsorable:, query: "")
    @sponsorable = sponsorable
    @query = query
  end

  private

  def render?
    @sponsorable
  end

  def search_description
    "Search #{@sponsorable.login} repositories"
  end

  def featured_repos_limit
    ::SponsorsListingFeaturedItem::FEATURED_REPOS_LIMIT_PER_LISTING
  end

  def featured_repos_remaining
    featured_repos_limit - current_featured_repo_items.count
  end

  def current_featured_repo_items
    @current_featured_repo_items ||= @sponsorable.sponsors_listing.featured_repos
  end

  def show_remaining_in_red?
    featured_repos_remaining <= 0
  end
end
