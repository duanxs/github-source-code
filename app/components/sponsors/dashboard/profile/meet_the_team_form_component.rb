# frozen_string_literal: true

class Sponsors::Dashboard::Profile::MeetTheTeamFormComponent < ApplicationComponent
  def initialize(sponsorable:)
    @sponsorable = sponsorable
  end

  private

  def render?
    @sponsorable&.organization?
  end
end
