# frozen_string_literal: true

class Sponsors::Dashboard::Profile::IntroductionComponent < ApplicationComponent
  def initialize(sponsorable:)
    @sponsorable = sponsorable
  end

  private

  attr_reader :sponsorable

  delegate :csrf_hidden_input_for, to: :helpers

  def render?
    sponsorable.present?
  end


  def owner
    sponsorable.organization? ? "the #{sponsorable.login}" : "your"
  end

  def full_description
    @full_description ||= begin
      params.dig(:sponsors_listing, :full_description) ||
        sponsorable.sponsors_listing.full_description ||
        full_description_placeholder
    end
  end

  def full_description_placeholder
    <<~FD
      Don’t forget to delete this placeholder text 🙃

      Here are some ideas of what you can tell your potential sponsors:
      - [ ] Who are you, and where are you from?
      - [ ] What are you working on?
      - [ ] Why is their sponsorship important? How will you use the funds?

      Hint: You can include images and emojis in your bio!
    FD
  end
end
