# frozen_string_literal: true

class Sponsors::Dashboard::Profile::MeetTheTeamComponent < ApplicationComponent
  include AvatarHelper

  def initialize(sponsorable:)
    @sponsorable = sponsorable
  end

  private

  def render?
    @sponsorable&.organization?
  end

  def featured_users
    @featured_users ||= @sponsorable.sponsors_listing.featured_users
  end
end
