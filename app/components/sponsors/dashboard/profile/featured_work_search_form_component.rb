# frozen_string_literal: true

class Sponsors::Dashboard::Profile::FeaturedWorkSearchFormComponent < ApplicationComponent
  def initialize(sponsorable:)
    @sponsorable = sponsorable
  end

  private

  def render?
    @sponsorable
  end
end
