# frozen_string_literal: true

class Sponsors::Dashboard::Profile::ShortBioComponent < ApplicationComponent
  def initialize(sponsorable:)
    @sponsorable = sponsorable
  end

  private

  def render?
    @sponsorable.present?
  end
end
