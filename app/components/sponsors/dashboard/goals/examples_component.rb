# frozen_string_literal: true

class Sponsors::Dashboard::Goals::ExamplesComponent < ApplicationComponent
  def initialize(sponsorable:)
    @sponsorable = sponsorable
  end

  private

  def render?
    return false if current_user.blank?

    @sponsorable.present?
  end

  def collapsed?
    return @collapsed if defined?(@collapsed)
    @collapsed = @sponsorable.sponsors_listing.goals.exists?
  end

  def button_css_classes
    %{
      btn btn-invisible float-right text-normal text-small text-gray-light
      p-0 js-sponsors-nudge-toggle js-sponsors-nudge
    }.squish
  end

  def examples
    [
      {
        avatar: image_path("modules/site/sponsors/snowtocat.png"),
        login: "@snowtocat",
        goal_title: "have 10 sponsors",
        goal_description: "It would mean the world to me if I had 10 sponsors. 💖",
      },
      {
        avatar: image_path("modules/site/sponsors/jetpacktocat.png"),
        login: "@jetpacktocat",
        goal_title: "earn $50 per month",
        goal_description: %{
          I'll be able to cover my server costs once I'm sponsored for $50 each month!
          It'll mean a lot if you help me achieve this goal.
        }.squish,
      },
      {
        avatar: image_path("modules/site/sponsors/dinocat.png"),
        login: "@dinocat",
        goal_title: "earn $5,000 per month",
        goal_description: "I'll be able to quit my job and work on open source full time!",
      },
    ]
  end
end
