# frozen_string_literal: true

class Sponsors::Dashboard::Goals::DismissibleBannerComponent < ApplicationComponent

  def initialize(selected_tab:)
    @selected_tab = selected_tab
  end

  private

  def render?
    return false if @selected_tab == :goals
    return false if current_user.blank?
    return false if current_user.dismissed_notice?(::User::SPONSORS_GOALS_BANNER)

    activity_notice_hidden?
  end

  def activity_notice_hidden?
    return true if @selected_tab == :activity

    current_user.dismissed_notice?(::User::SPONSORS_ACTIVITY_BANNER)
  end
end
