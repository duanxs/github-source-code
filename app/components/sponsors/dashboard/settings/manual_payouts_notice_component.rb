# frozen_string_literal: true

class Sponsors::Dashboard::Settings::ManualPayoutsNoticeComponent < ApplicationComponent
  def initialize(sponsorable:)
    @membership = sponsorable.sponsors_membership
  end

  private

  def render?
    !@membership.eligible_for_stripe_connect?
  end
end
