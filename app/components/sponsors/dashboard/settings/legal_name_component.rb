# frozen_string_literal: true

class Sponsors::Dashboard::Settings::LegalNameComponent < ApplicationComponent
  def initialize(sponsorable:)
    @sponsorable = sponsorable
    @membership = sponsorable.sponsors_membership
  end

  private

  def render?
    @membership.legal_name_changeable?
  end

  def legal_name_label
    if @sponsorable.user?
      "Full legal name"
    else
      "Organization name"
    end
  end
end
