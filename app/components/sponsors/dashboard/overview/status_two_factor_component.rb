# frozen_string_literal: true

class Sponsors::Dashboard::Overview::StatusTwoFactorComponent < ApplicationComponent
  def initialize(sponsorable:)
    @sponsorable = sponsorable
  end

  private

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def two_factor_set?
    @two_factor_set ||= if @sponsorable.organization?
      @sponsorable.adminable_by?(current_user) && current_user.two_factor_authentication_enabled?
    else
      @sponsorable.two_factor_authentication_enabled?
    end
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def two_factor_recommended?
    @two_factor_recommended ||= begin
      @sponsorable.organization? && !@sponsorable.two_factor_requirement_enabled?
    end
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def two_factor_status_icon
    two_factor_set? ? "check" : "primitive-dot"
  end

  def two_factor_status_color
    two_factor_set? ? "green" : "yellow-7"
  end
end
