# frozen_string_literal: true

class Sponsors::Dashboard::Overview::StatusDocusignComponent < ApplicationComponent
  SIGNING_COMPLETE_EVENT = "signing_complete"

  def initialize(sponsorable:, docusign_event:)
    @sponsorable = sponsorable
    @docusign_event = docusign_event
  end

  private

  def render?
    @sponsorable.sponsors_docusign_enabled?
  end

  def listing
    @listing ||= @sponsorable.sponsors_listing
  end

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def has_country_of_residence?
    @has_country_of_residence ||= @sponsorable.sponsors_membership.country_of_residence.present?
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def has_docusign_envelope?
    @has_docusign_envelope ||= listing.docusign_envelope?
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def docusign_completed?
    @docusign_completed ||= begin
      @docusign_event == SIGNING_COMPLETE_EVENT || listing.docusign_completed?
    end
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def tax_form_status_icon
    if has_country_of_residence? && docusign_completed?
      "check"
    else
      "primitive-dot"
    end
  end

  def tax_form_status_color
    if has_country_of_residence? && docusign_completed?
      "green"
    else
      "yellow-7"
    end
  end

  def residence_status_icon
    has_country_of_residence? ? "check" : "primitive-dot"
  end

  def residence_status_color
    has_country_of_residence? ? "green" : "yellow-7"
  end

  def docusign_status_icon
    if has_docusign_envelope?
      docusign_completed? ? "check" : "clock"
    else
      "primitive-dot"
    end
  end

  def docusign_status_color
    if has_docusign_envelope? && docusign_completed?
      "green"
    else
      "yellow-7"
    end
  end

  def docusign_next_step
    if has_docusign_envelope? && !docusign_completed?
      "Complete the"
    else
      "Fill out a"
    end
  end
end
