# frozen_string_literal: true

class Sponsors::Shared::FeaturedRepositoryComponent < ApplicationComponent
  def initialize(repo:)
    @repo = repo
  end

  private

  def render?
    @repo.public?
  end
end
