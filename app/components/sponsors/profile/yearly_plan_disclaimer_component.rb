# frozen_string_literal: true

class Sponsors::Profile::YearlyPlanDisclaimerComponent < ApplicationComponent
  def initialize(
      sponsor:,
      yearly_price_in_cents: 0
    )
    @sponsor = sponsor
    @yearly_price_in_cents = yearly_price_in_cents.to_i
  end

  private

  def render?
    @sponsor.plan_duration == User::YEARLY_PLAN && @yearly_price_in_cents > 0
  end

  def formatted_yearly_price
    Billing::Money.new(@yearly_price_in_cents).format
  end

  def formatted_next_billing_date
    format_date(@sponsor.next_billing_date.to_time(:utc))
  end

  def format_date(date)
    date.strftime("%b #{date.day.ordinalize}, %Y")
  end
end
