# frozen_string_literal: true

class Sponsors::Profile::TierSelectionComponent < ApplicationComponent
  include AvatarHelper

  def initialize(sponsorable:,
                 sponsor: nil,
                 sponsorship: nil,
                 selected_tier: nil,
                 previewing: false)
    @sponsorable = sponsorable
    @sponsor = sponsor
    @sponsorship = sponsorship
    @selected_tier = selected_tier
    @previewing = previewing
  end

  private

  attr_reader :sponsorable

  delegate :sponsors_listing, to: :sponsorable
  delegate :published_sponsors_tiers, to: :sponsors_listing

  def previewing?
    @previewing
  end

  def no_verified_emails?
    return true unless logged_in?
    return @no_verified_emails if defined?(@no_verified_emails)
    @no_verified_emails = @sponsor.no_verified_emails?
  end

  def current_tier
    return unless @sponsorship

    if pending_downgrade?
      pending_tier_change.subscribable
    else
      @sponsorship.tier
    end
  end

  def previous_tier
    return nil unless pending_downgrade?
    @sponsorship.tier
  end

  def next_billing_date
    date = @sponsor.next_billing_date
    if @sponsor.yearly_plan?
      date.strftime("%b #{date.day.ordinalize}, %Y")
    else
      date.strftime("%b #{date.day.ordinalize}")
    end
  end

  def pending_downgrade?
    return false unless pending_tier_change?
    !pending_tier_change.cancellation?
  end

  def pending_cancellation?
    return @pending_cancellation if defined?(@pending_cancellation)
    @pending_cancellation = pending_tier_change&.cancellation?
  end

  # Fetch any pending plan change items for the
  # current user. This is necessary for rendering the
  # plan_pricing sub-component which displays a pending
  # downgrade or cancellation notice
  def pending_tier_change
    return unless logged_in?
    return unless @sponsorship
    return @pending_tier_change if defined?(@pending_tier_change)

    @pending_tier_change = @sponsorship.pending_tier_change
  end

  def pending_tier_change?
    pending_tier_change.present?
  end
end
