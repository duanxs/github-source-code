# frozen_string_literal: true

class Sponsors::Profile::TierSelectionButtonComponent < ApplicationComponent
  def initialize(
    sponsorable:,
    sponsor:,
    tier:,
    current_tier: nil,
    pending_cancellation: false,
    pending_downgrade: false,
    no_verified_emails: false
  )
    @sponsorable = sponsorable
    @sponsor = sponsor
    @tier = tier
    @current_tier = current_tier
    @pending_cancellation = pending_cancellation
    @pending_downgrade = pending_downgrade
    @no_verified_emails = no_verified_emails
  end

  def form_action
    sponsorable_sponsorships_path(@sponsorable)
  end

  def pending_cancellation?
    @pending_cancellation
  end

  def pending_downgrade?
    @pending_downgrade
  end

  def pending_tier_change?
    pending_cancellation? || pending_downgrade?
  end

  def disabled?
    return true if pending_cancellation?
    return false if is_sponsored_tier?
    pending_tier_change?
  end

  def disabled_reason
    if pending_cancellation?
      "You have a pending cancellation"
    else
      "You have a pending tier change"
    end
  end

  def is_sponsored_tier?
    @tier == @current_tier
  end

  def no_verified_emails?
    @no_verified_emails
  end

  def submit_text
    is_sponsored_tier? ? "Manage" : "Select"
  end

  def login_url
    return_to_url = sponsorable_sponsorships_path(@sponsorable, tier_id: @tier.id, sponsor: @sponsor&.login)
    login_path(return_to: return_to_url)
  end
end
