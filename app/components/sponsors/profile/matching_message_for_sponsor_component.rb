# frozen_string_literal: true

class Sponsors::Profile::MatchingMessageForSponsorComponent < ApplicationComponent
  def initialize(sponsorable:, sponsor:)
    @sponsorable = sponsorable
    @listing = sponsorable.sponsors_listing
    @sponsor = sponsor
  end

  private

  def render?
    return false if @sponsorable.organization?
    return false if @sponsor.organization?

    @listing.matchable?
  end

  def match_eligible?
    @sponsor.eligible_for_sponsorship_match?(sponsorable: @sponsorable)
  end

  def help_url
    "#{GitHub.help_url}/articles/about-github-sponsors#about-the-github-sponsors-matching-fund"
  end
end
