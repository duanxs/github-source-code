# frozen_string_literal: true

class Sponsors::Profile::NewSponsorshipSurveyComponent < ApplicationComponent
  include SponsorsButtonsHelper

  def initialize(sponsorable:, sponsor:, sponsorship: nil)
    @sponsorable = sponsorable
    @sponsor = sponsor
    @sponsorship = sponsorship
  end

  private

  delegate :survey, :discovery_question, :influence_question, :why_sponsor_question,
    to: :sponsor_survey

  def render?
    return false unless @sponsorship
    return false unless @sponsorship.active?
    return false unless @sponsor.present?
    return false unless survey
    return false if sponsor_survey.completed?
    return false if @sponsorship.has_pending_cancellation?

    GlobalInstrumenter.instrument("sponsors.element_displayed", {
      element: "NEW_SPONSORSHIP_SURVEY_CTA",
      actor: current_user,
      sponsor: @sponsor,
      sponsorable: @sponsorable,
    })

    true
  end

  def sponsor_survey
    @sponsor_survey ||= Sponsors::SponsorshipCreatedSurvey.new(
      sponsor: @sponsor,
      sponsorable: @sponsorable,
    )
  end
end
