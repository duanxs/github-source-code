# frozen_string_literal: true

class Sponsors::Profile::SocialSharingComponent < ApplicationComponent
  include SponsorsButtonsHelper

  def initialize(sponsorable:, sponsor:, sponsorship: nil, autofocus: false)
    @sponsorable = sponsorable
    @sponsor = sponsor
    @sponsorship = sponsorship
    @autofocus = autofocus
  end

  private

  def sponsors_profile_url
    "https://github.com/sponsors/#{@sponsorable.login}?sp=#{@sponsor.login}"
  end

  def sponsors_profile_url_for_twitter
    "#{sponsors_profile_url}&sc=t"
  end

  def default_text
    name_for_share = if @sponsorable.profile_twitter_username.present?
      "@#{@sponsorable.profile_twitter_username}"
    else
      @sponsorable.login
    end

    "#{Emoji.find_by_alias("sparkling_heart").raw} I'm sponsoring #{name_for_share} because…"
  end

  def render?
    return false unless logged_in?
    return false unless @sponsorship&.active?

    GlobalInstrumenter.instrument("sponsors.element_displayed", {
      element: "SOCIAL_SHARING_CTA",
      actor: current_user,
      sponsor: @sponsor,
      sponsorable: @sponsorable,
    })

    true
  end
end
