# frozen_string_literal: true

class Sponsors::StripeAccounts::EditComponent < ApplicationComponent
  STATUS_ICON_WIDTH = "20px"

  def initialize(sponsorable:, stripe_connect_url:)
    @sponsorable = sponsorable
    @stripe_connect_url = stripe_connect_url
  end

  private

  def render?
    @sponsorable.present? && @stripe_connect_url.present?
  end

  def listing
    @listing ||= @sponsorable.sponsors_listing
  end

  def current_deadline
    return unless account_has_details?
    return unless deadline = stripe_account_requirements.current_deadline
    Time.at(deadline).strftime("%b %d")
  end

  def items_currently_due?
    return false unless account_has_details?
    stripe_account_requirements.currently_due.any?
  end

  def items_past_due?
    return false unless account_has_details?
    stripe_account_requirements.past_due.any?
  end

  def items_eventually_due?
    return false unless account_has_details?
    stripe_account_requirements.eventually_due.any?
  end

  def payouts_enabled?
    return false unless account_has_details?
    stripe_account_details.payouts_enabled
  end

  def has_payout_option?
    return false unless account_has_details?
    stripe_account_details.external_accounts.data.any?
  end

  def viewer_has_stripe_account?
    stripe_account.present?
  end

  def viewer_has_unverified_payout_option?
    has_payout_option? && !payouts_enabled?
  end

  def aggregate_status_icon
    if stripe_account_verified? && payouts_enabled?
      "check"
    elsif viewer_has_stripe_account?
      "clock"
    else
      "primitive-dot"
    end
  end

  def aggregate_status_icon_color
    if stripe_account_verified? && payouts_enabled?
      "green"
    else
      "yellow-7"
    end
  end

  def stripe_account_status_icon
    if viewer_has_stripe_account?
      "check"
    else
      "primitive-dot"
    end
  end

  def stripe_account_status_icon_color
    if viewer_has_stripe_account?
      "green"
    else
      "yellow-7"
    end
  end

  def stripe_identity_status_icon
    if stripe_account_verified?
      "check"
    else
      "clock"
    end
  end

  def stripe_identity_status_icon_color
    if stripe_account_verified?
      "green"
    else
      "yellow-7"
    end
  end

  def stripe_account_link
    if viewer_has_stripe_account?
      new_sponsorable_stripe_account_path(@sponsorable.login)
    else
      @stripe_connect_url
    end
  end

  def check_stripe_account_details_link
    edit_sponsorable_stripe_account_path(@sponsorable.login)
  end

  def account_has_details?
    stripe_account&.stripe_account_details.present?
  end

  def fetching_account?
    !stripe_account && listing.stripe_authorization_code.present?
  end

  def stripe_account_verified?
    return false unless stripe_account
    stripe_account.verified?
  end

  def show_stripe_link_error?
    !stripe_account && listing.stripe_connect_link_failed?
  end

  def stripe_link_error_message
    return unless show_stripe_link_error?

    case listing.stripe_connect_link_error
    when :duplicate_account_id
      "The Stripe Connect account you tried to link is already in use. Please log out of your existing Stripe account and try again."
    else
      "An error occurred when linking your Stripe Connect account. Please try again."
    end
  end

  def stripe_account
    @stripe_account ||= listing.stripe_connect_account
  end

  def stripe_account_details
    return @stripe_account_details if defined?(@stripe_account_details)

    @stripe_account_details = if account_has_details?
      Stripe::Account.construct_from(stripe_account.stripe_account_details)
    end
  end

  def stripe_account_requirements
    @stripe_account_requirements = stripe_account_details.requirements
  end
end
