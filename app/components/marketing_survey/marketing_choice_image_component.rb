# frozen_string_literal: true

module MarketingSurvey
  class MarketingChoiceImageComponent < ApplicationComponent
    include SvgHelper

    SVG = :svg
    OCTICON = :octicon
    VALID_TYPES = [SVG, OCTICON]
    VALID_NAMES = %w(code-to-cloud school-work new-user see-project-activity github gitlab bitbucket sourceforge)

    def initialize(image:, hidden: false, **kwargs)
      @image, @hidden, @kwargs = image, hidden, kwargs
    end

    def type
      @image[:type]
    end

    def name
      @image[:name]
    end

    def options
      { hidden: @hidden, class: Primer::Classify.call(**@kwargs) }.merge(@kwargs)
    end

    private

    def render?
      return false unless @image.present? && VALID_TYPES.include?(type) && type && name

      if type == SVG
        VALID_NAMES.include?(name)
      else
        true
      end
    end
  end
end
