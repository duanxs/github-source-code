# frozen_string_literal: true

module MarketingSurvey
  class MarketingChoiceComponent < ApplicationComponent
    include ClassNameHelper

    TILE_TYPE = :tile
    LIST_ITEM_TYPE = :list_item
    TEXT_INPUT_TYPE = :text_input
    TYPES = [TILE_TYPE, LIST_ITEM_TYPE, TEXT_INPUT_TYPE]
    TYPE_DEFAULT = TILE_TYPE
    DEFAULT_CONTAINER_CLASSES = "col-12 col-sm-6 col-md-3"

    def initialize(
      choice:,
      question:,
      type: TYPE_DEFAULT,
      questions_with_other: [],
      image: nil,
      container_classes: DEFAULT_CONTAINER_CLASSES,
      add_input_classes: nil,
      container_style: nil,
      input_style: nil
    )
      @choice,
      @question,
      @type,
      @questions_with_other,
      @image,
      @add_input_classes,
      @container_style,
      @input_style = choice, question, type, questions_with_other, image, add_input_classes, container_style, input_style
      @container_classes = container_classes || DEFAULT_CONTAINER_CLASSES
    end

    def container_classes
      class_names(@container_classes, "js-answer" => @type == TILE_TYPE)
    end

    def label_for_attr
      "answers_#{@question.id}_choice_#{@choice.id}"
    end

    def tag_for_question_and_choice
      classes = class_names(
        "js-survey-answer-choice flex-self-center",
        "#{@add_input_classes}" => @add_input_classes.present?,
        "sr-only hx_focus-input" => @type == TILE_TYPE,
        "js-allow-multiple" => @question.accept_multiple_answers?
      )

      if @question.accept_multiple_answers?
        check_box_tag("answers[#{@question.id}][choices][]", @choice.id, false,
                      id: label_for_attr,
                      class: classes,
                      data: {
                        "max-choices" =>  @question.max_acceptable_answers,
                      }.merge(test_selector_hash("marketing-choice-checkbox"))
                    )
      else
        radio_button_tag("answers[#{@question.id}][choice]", @choice.id, false,
                        class: classes,
                        data: test_selector_hash("marketing-choice-radio")
                      )
      end
    end

    private

    def render?
      TYPES.include?(@type)
    end
  end
end
