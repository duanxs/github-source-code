# frozen_string_literal: true

module MarketingSurvey
  class MarketingQuestionComponent < ApplicationComponent
    def initialize(question:, hidden: false, question_description: nil, container_classes: "")
      @question, @hidden, @question_description, @container_classes = question, hidden, question_description, container_classes
    end

    def label_for_attr
      "answers_#{@question.id}"
    end

    def multiple_answer_copy
      if @question.max_acceptable_answers > 10
        "Please select all that apply"
      else
        "Choose up to #{@question.max_acceptable_answers}"
      end
    end
  end
end
