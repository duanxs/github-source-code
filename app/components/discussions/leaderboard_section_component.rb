# frozen_string_literal: true

module Discussions
  class LeaderboardSectionComponent < ApplicationComponent
    include AvatarHelper
    include ClassNameHelper

    with_content_areas :count_icon

    def initialize(title:, counts_by_user:)
      @title = title
      @counts_by_user = counts_by_user
    end

    private

    attr_reader :counts_by_user, :title

    def container_test_selector
      section_name = @title.downcase.parameterize.underscore
      test_selector("discussion_leaderboard_#{section_name}")
    end
  end
end
