# frozen_string_literal: true

module PullRequests
  class ReviewStatusLabelComponent < ApplicationComponent
    def initialize(is_draft:, revisions_count:, **args)
      @is_draft, @revisions_count, @args = is_draft, revisions_count, args

      @args[:color] = "green" unless @is_draft
    end

    private

    def render?
      @revisions_count > 0
    end

    def text
      if @is_draft
        if @revisions_count.to_i > 1
          "Paused"
        else
          "Not started"
        end
      else
        "Active"
      end
    end
  end
end
