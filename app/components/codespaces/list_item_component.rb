# frozen_string_literal: true

class Codespaces::ListItemComponent < ApplicationComponent
  with_collection_parameter :codespace
  with_content_areas :button

  include ClassNameHelper
  include CodespacesHelper

  attr_reader :codespace, :repository_id, :ref, :pull_request_id, :show_unpushed_warning, :codespaces_context, :new_codespace,
    :container_element, :container_class, :link_container_class,
    :delete_button_class, :delete_spinner_class, :wide, :pr_dropdown

  # codespace             - The codespace to be rendered
  # repository_id         - Represents the repository for the page the user is currently viewing. Passed along to the DeleteFormComponent.
  # ref                   - Name of the ref for the page the user is currently viewing. Passed along to the DeleteFormComponent.
  # pull_request_id       - Represents the PR of the page the user is currently viewing. Passed along to the DeleteFormComponent.
  # show_unpushed_warning - Show deletion warning if codespace has unpushed changes in VSCS
  # codespaces_context    - a flag passed around during request/response cycles so we know whether to render all accessible
  #                         codespaces or codespaces for a given repo's ref/pull_request.
  # container_element     - The DOM element type to be rendered as the list item's container. (optional, default: "div")
  # container_class       - A CSS class that will be added to the top container element around this list item. (optional, default: "")
  # link_container_class  - A CSS class that will be added to the container that wraps this list item's link to open the codespace. (optional, default: "")
  # delete_button_class   - A CSS class that will be added to the delete button. (optional, default: "")
  # delete_spinner_class  - A CSS class appended to the waiting icon we'll show while deleting a codespace
  # wide                  - A boolean, is the list item being rendered in its wide version or not. (optional, default: false)
  # pr_dropdown           - A boolean, is the list item being rendered in a PR dropdown (optional, default: false)
  def initialize(
    codespace:,
    repository_id: nil,
    ref: nil,
    pull_request_id: nil,
    show_unpushed_warning: false,
    codespaces_context: nil,
    container_element: "div",
    container_class: "",
    link_container_class: "",
    delete_button_class: "",
    delete_spinner_class: "",
    wide: false,
    pr_dropdown: false
  )

    @codespace, @repository_id, @ref, @pull_request_id, @show_unpushed_warning, @codespaces_context, @container_element,
    @container_class, @link_container_class, @delete_button_class, @delete_spinner_class, @wide, @pr_dropdown =
      codespace, repository_id, ref, pull_request_id, show_unpushed_warning, codespaces_context, container_element,
      container_class, link_container_class, delete_button_class, delete_spinner_class, wide, pr_dropdown
  end

  def wide?
    @wide
  end

  def show_unpushed_warning?
    @show_unpushed_warning
  end
end
