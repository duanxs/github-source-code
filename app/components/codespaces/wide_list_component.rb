# frozen_string_literal: true

class Codespaces::WideListComponent < ApplicationComponent
  include CodespacesHelper

  attr_reader :codespaces, :unauthorized_saml_targets
  # The WideListComponent is used exclusively on the codespaces/index view
  # because the layout differences are too much to contain within the
  # base ListComponent. The primary differences are:
  # * A blankslate is provided with marketing copy for codespaces when a user
  #   has none.
  # * When displaying a list of codespaces the CreateButtonComponent is
  #   positioned above the list of codespaces instead of below.
  def initialize(codespaces:, unauthorized_saml_targets:, unpushed_id: nil)
    @codespaces, @unauthorized_saml_targets, @unpushed_id =
      codespaces, unauthorized_saml_targets, unpushed_id
  end

  def show_unpushed_warning?(codespace)
    codespace.id == @unpushed_id&.to_i
  end

  def show_unauthorized_saml_targets_warning?
    unauthorized_saml_targets.present?
  end
end
