# frozen_string_literal: true

class Codespaces::PromptCreateButtonComponent < ApplicationComponent
  attr_reader :text, :popover_classes, :popover_msg_classes

  # text                - a button text String. (optional, default: "Open in codespace")
  # popover_classes     - Optional styling for the popover
  # popover_msg_classes - Optional styling for the popover message
  def initialize(
    text: "Open in codespace",
    popover_classes: "",
    popover_msg_classes: ""
  )
    @text, @popover_classes, @popover_msg_classes =
      text, popover_classes, popover_msg_classes
  end

  def before_render
    SecureHeaders.append_content_security_policy_directives(
      request,
      connect_src: vscs_locations_endpoints
    )
  end

  def vscs_locations_endpoints
    if helpers.current_user_feature_enabled?(:codespaces_developer)
      Codespaces.vscs_targets.map do |vscs_target|
        Codespaces.vscs_locations_url_for_target(vscs_target)
      end
    else
      [Codespaces::Azure.vso_locations_url(current_user)]
    end
  end
end
