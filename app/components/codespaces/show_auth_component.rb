# frozen_string_literal: true

class Codespaces::ShowAuthComponent < ApplicationComponent

  attr_reader :codespace,
              :github_token,
              :user

  def initialize(codespace:, github_token:, user:)
    @codespace, @github_token, @user = codespace, github_token, user
  end

  def before_render
    SecureHeaders::append_content_security_policy_directives(
      request,
      form_action: form_actions
    )
  end

  private

  def auth_hosts
    if GitHub.flipper[:codespaces_developer].enabled?(user)
      # This flag allows the user to possibly pick from any environment so
      # we allow all environments for CORS.
      Codespaces.vscs_platform_auth_hosts
    else
      # Withouth this flag the codespace will ultimately be provisioned in
      # whatever the user is configured to use (if in the ENV_SPECIFIC_USERS)
      # or the default for the current environment. We don't try to grab this
      # from the codespace itself because the codespace is not necesarily
      # provisioned by the time we render this component.
      [Codespaces.vscs_target_config(user)[:platform_auth_host]]
    end
  end

  def form_actions
    auth_hosts.map do |host|
      "#{codespace.name}.#{host}"
    end.sort
  end
end
