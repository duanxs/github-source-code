# frozen_string_literal: true

module Navigation
  class ExploreComponent < ApplicationComponent
    def initialize(integrations_directory_enabled:)
      @integrations_directory_enabled = fetch_or_fallback([true, false], integrations_directory_enabled, false)
    end

    private

    def show_showcase_link?
      GitHub.showcase_enabled?
    end

    def show_apps_link?
      @integrations_directory_enabled
    end

    def show_activity_link?
      GitHub.enterprise?
    end
  end
end
