# frozen_string_literal: true

module ProfilesHelper
  include HydroHelper
  include PlatformHelper

  def most_recent_collector_with_activity(collector)
    return unless collector

    fetcher = collector.prior_activity_fetcher
    fetcher.collector_with_activity
  end

  def edit_colors_path(user)
    config_repo = user.configuration_repository

    if config_repo.has_profile_configuration_file?
      profile_config = config_repo.profile_configuration_file
      file_edit_path(user.login, config_repo.name, config_repo.default_branch,
                     profile_config.name)
    else
      blob_new_path("", config_repo.default_branch, config_repo) +
        "?filename=#{ProfileConfigurationFile::DEFAULT_NAME}"
    end
  end

  CUSTOM_PROFILE_COLOR_CLASS = "hx_custom-color"
  CUSTOM_PROFILE_HOVER_COLOR_CLASS = "hx_custom-hover-color"
  CUSTOM_PROFILE_BACKGROUND_CLASS = "hx_custom-background"
  CUSTOM_PROFILE_HOVER_BACKGROUND_CLASS = "hx_custom-hover-background"
  CUSTOM_PROFILE_BORDER_CLASS = "hx_custom-border"
  CUSTOM_PROFILE_HOVER_BORDER_CLASS = "hx_custom-hover-border"
  CUSTOM_PROFILE_STROKE_CLASS = "hx_custom-stroke"
  CUSTOM_PROFILE_MARKDOWN_CLASS = "hx_custom-markdown"

  def custom_profile_color_class
    CUSTOM_PROFILE_COLOR_CLASS if config_repo_enabled?
  end

  def custom_profile_hover_color_class
    CUSTOM_PROFILE_HOVER_COLOR_CLASS if config_repo_enabled?
  end

  def custom_profile_background_class
    CUSTOM_PROFILE_BACKGROUND_CLASS if config_repo_enabled?
  end

  def custom_profile_hover_background_class
    CUSTOM_PROFILE_HOVER_BACKGROUND_CLASS if config_repo_enabled?
  end

  def custom_profile_border_class
    CUSTOM_PROFILE_BORDER_CLASS if config_repo_enabled?
  end

  def custom_profile_hover_border_class
    CUSTOM_PROFILE_HOVER_BORDER_CLASS if config_repo_enabled?
  end

  def custom_profile_stroke_class
    CUSTOM_PROFILE_STROKE_CLASS if config_repo_enabled?
  end

  def custom_profile_markdown_class
    CUSTOM_PROFILE_MARKDOWN_CLASS if config_repo_enabled?
  end

  def custom_profile_css(user)
    return unless config_repo_enabled?
    return unless user.has_configuration_repository?

    config_repo = user.configuration_repository
    return unless config_repo.has_profile_configuration_file?

    profile_config = config_repo.profile_configuration_file
    return unless profile_config.specifies_color?

    link_color = profile_config.base_color
    custom_css = %Q(
      .#{CUSTOM_PROFILE_COLOR_CLASS},
      .#{CUSTOM_PROFILE_HOVER_COLOR_CLASS}:hover,
      .#{CUSTOM_PROFILE_MARKDOWN_CLASS} a,
      .#{CUSTOM_PROFILE_HOVER_COLOR_CLASS}:hover .user-status-message-wrapper {
        color: #{link_color} !important;
      }

      .#{CUSTOM_PROFILE_BACKGROUND_CLASS},
      .#{CUSTOM_PROFILE_HOVER_BACKGROUND_CLASS}:hover {
        background-color: #{link_color} !important;
      }

      .#{CUSTOM_PROFILE_HOVER_BACKGROUND_CLASS}:hover {
        color: #fff !important;
      }

      .#{CUSTOM_PROFILE_BORDER_CLASS},
      .#{CUSTOM_PROFILE_HOVER_BORDER_CLASS}:hover { border-color: #{link_color} !important; }

      .#{CUSTOM_PROFILE_STROKE_CLASS} { stroke: #{link_color} !important; }
    )

    content_tag(:style, custom_css, type: "text/css", media: "all")
  end

  # Public: True if the profile readme should be rendered
  #         for this viewer
  def show_profile_readme?
    return false unless this_user

    this_user.profile_readme_visible?
  end

  def show_profile_readme_feedback_link?(repository, user, filename = "README.md")
    return false if repository.nil?
    return false if user.nil?
    return false unless PreferredFile.filename_is_type?(filename: filename, type: :readme)
    return false if repository.owner != user

    repository.user_configuration_repository?
  end

  # How many repositories should we show in a contribution rollup before we
  # collapse the section by default?
  REPO_ROLLUP_COLLAPSE_THRESHOLD = 7

  # Public: Get the given time as an ISO8601 date string in the viewer's time zone.
  #
  # time - UTC DateTime
  #
  # Returns a String like "2018-05-01".
  def timeline_iso8601_date(time)
    time.in_time_zone(Time.zone).to_date.iso8601
  end

  # Public: Get the year for the given DateTime in the current viewer's time zone.
  #
  # started_at - UTC DateTime for the beginning of the contributions collection time range
  #
  # Returns a String like "2018".
  def timeline_year_str(started_at)
    started_at.in_time_zone(Time.zone).strftime("%Y")
  end

  def contributions_date_range_label(earliest_date:, latest_date:)
    if earliest_date > latest_date
      earliest_date, latest_date = latest_date, earliest_date
    end
    earliest_date = earliest_date.in_time_zone(Time.zone)
    latest_date = latest_date.in_time_zone(Time.zone)
    date_format = "%b %-d" # e.g. "May 12"

    if latest_date == earliest_date
      earliest_date.strftime(date_format)
    else
      "#{earliest_date.strftime(date_format)} – #{latest_date.strftime(date_format)}"
    end
  end

  # Public: Determine if the "Built by" list should be shown for a repository in a rollup on the
  # user profile.
  #
  # profile_login - login of the user whose profile is being viewed; String
  # contributor_logins - list of user logins for users who contributed to the repository; Array of
  #                      Strings
  #
  # Returns a Boolean.
  def show_repo_rollup_contributors?(profile_login:, contributor_logins:)
    contributor_logins.any? { |contributor_login| contributor_login != profile_login }
  end

  # Public: Returns a hex color String in the format "#ff00ff" to represent
  # the given percentage.
  #
  # percentage - Integer from 0-100
  # colors - an optional Array of at least 4 color Strings
  #
  # Returns a String.
  def commit_percentage_color(percentage, colors = [])
    if percentage <= 25
      colors[0] || Contribution::Calendar::DEFAULT_COLORS[0]
    elsif percentage <= 50
      colors[1] || Contribution::Calendar::DEFAULT_COLORS[1]
    elsif percentage <= 75
      colors[2] || Contribution::Calendar::DEFAULT_COLORS[2]
    else
      colors[3] || Contribution::Calendar::DEFAULT_COLORS[3]
    end
  end

  def commit_percentage_for_repo(repo_commit_count:, total_commit_count:)
    pct = 100 * repo_commit_count / total_commit_count.to_f
    pct.ceil
  end

  # Public: Get a count of how many repositories were not shown in the issue/pull request rollup.
  #
  # total_repos - how many repositories had contributions in them from the user; integer
  #
  # Returns an integer.
  def repo_count_not_shown_in_rollup(total_repos:)
    [total_repos - ProfilesController::REPOS_PER_ROLLUP_LIMIT, 0].max
  end

  # Public: Get a count of how many contributions were not shown in a rollup.
  #
  # total_contributions - how many contributions there were total; integer
  #
  # Returns an integer.
  def contrib_count_not_shown_in_rollup(total_contributions)
    [total_contributions - ProfilesController::CONTRIBS_PER_REPO_LIMIT, 0].max
  end

  # Public: Determine if the repositories in a rollup in the timeline should be shown by default.
  #
  # total_repos - how many repositories had contributions from the user; integer
  #
  # Returns a Boolean.
  def expand_repo_rollup?(total_repos)
    total_repos < REPO_ROLLUP_COLLAPSE_THRESHOLD
  end

  # Public: Get a short date in the viewer's time zone for display in the user timeline.
  #
  # occurred_at - UTC DateTime for when the contribution was made
  #
  # Returns a String.
  def contribution_short_date(occurred_at)
    occurred_at.in_time_zone(Time.zone).strftime("%b %-d")
  end

  # Public: Get the month, day, and year in the viewer's time zone for display in the user timeline.
  #
  # occurred_at - UTC DateTime for when the contribution was made
  #
  # Returns a String.
  def contribution_long_date(occurred_at)
    occurred_at.in_time_zone(Time.zone).strftime("%B %-d, %Y")
  end

  # Public: Get the month name in the viewer's time zone for a given commit time.
  #
  # occurred_at - UTC DateTime for when the contribution was made
  #
  # Returns a String.
  def commit_contribution_month(occurred_at)
    occurred_at.in_time_zone(Time.zone).strftime("%B")
  end

  # Public: Get the header text for the user profile timeline when it has activity.
  #
  # started_at - UTC DateTime for the earliest date in the contributions collection
  # is_single_day - does the collection span only a day; Boolean
  #
  # Returns HTML.
  def time_range_with_activity_header(started_at:, is_single_day:)
    time = started_at.in_time_zone(Time.zone) # convert to viewer's time zone
    parts = [time.strftime("%B")]
    parts << "#{time.strftime("%-d")}," if is_single_day
    parts << tag.span(time.strftime("%Y"), class: "text-gray")
    safe_join(parts, " ")
  end

  # Public: Get 'alt' attribute text for the image that represents a user's first repository.
  #
  # Returns a String.
  def first_repo_image_alt(user_is_viewer:, user_name:)
    if user_is_viewer
      "Congratulations on your first repository!"
    else
      "#{user_name} created their first repository!"
    end
  end

  # Public: Get 'alt' attribute text for the image that represents a user's first issue on GitHub.
  #
  # Returns a String.
  def first_issue_image_alt(user_is_viewer:, user_name:)
    if user_is_viewer
      "Congratulations on your first issue!"
    else
      "#{user_name} created their first issue!"
    end
  end

  # Public: Get 'alt' attribute text for the image that represents a user's first pull request.
  #
  # Returns a String.
  def first_pull_request_image_alt(user_is_viewer:, user_name:)
    if user_is_viewer
      "Congratulations on your first pull request!"
    else
      "#{user_name} created their first pull request!"
    end
  end

  ContributionCountFragment = parse_query <<-'GRAPHQL'
    fragment on ContributionCount {
      name
      percentage
    }
  GRAPHQL

  # Public: Get a hash of contribution counts or percentages by contribution type.
  #
  # contribution_counts - an Array of ContributionCount GraphQL objects
  # value - a Symbol representing what the value in the resulting hash should be; choose from
  #         :count or :percentage
  #
  # Returns a Hash like:
  #   `{"Issues" => 3, "Commits" => 4, "Pull requests" => 20, "Code review" => 0}`
  def contribution_percentages_by_type(contribution_counts)
    contribution_counts = contribution_counts.map do |contrib|
      ContributionCountFragment.new(contrib)
    end

    values_by_type = contribution_counts.
      map { |contrib| [contrib.name, contrib.percentage] }.to_h

    # Make sure we have all the possible labels
    Contribution::Collector::CONTRIBUTION_COUNT_NAME_MAPPING.each_value do |contrib_type|
      values_by_type[contrib_type] ||= 0
    end

    values_by_type
  end

  UserFragment = parse_query <<-'GRAPHQL'
    fragment User on User {
      login
      name
    }
  GRAPHQL

  # Public: Get a page title for viewing the previous month of a user's profile.
  #
  # user - GraphQL User instance for the user whose profile is being viewed
  # date_range - the currently viewed Range of Dates for the profile
  #
  # Returns a String.
  def show_previous_months_contributions_title(user:, date_range:)
    user = UserFragment::User.new(user)

    previous_month = date_range.begin.prev_month
    title = "#{user.login}"
    title += " (#{user.name})" if user.name.present?
    date = previous_month.end_of_month.to_date
    date_str = date.strftime("%B %Y")
    "#{title} / #{date_str}"
  end

  # Public: Returns the user profile URL for viewing the previous month of contributions, relative
  # to beginning of the current time range.
  #
  # user - the User whose profile is being viewed, or their String login
  # date_range - the currently viewed Range of Dates for the profile
  # org - an Organization or its String login, or nil
  # xhr - whether this is for making an AJAX request or for putting into the URL
  #       for the user to copy and load as a regular HTTP request; Boolean
  #
  # Returns a String.
  def show_previous_months_contributions_path(user:, date_range:, org:, xhr: false)
    previous_month = date_range.begin.prev_month
    from = previous_month.beginning_of_month.to_date
    to = previous_month.end_of_month.to_date
    suffix = "?tab=overview&from=#{from}&to=#{to}"
    suffix += "&include_header=no" if xhr
    suffix += "&org=#{org}" if org.present?
    user_path(user) + suffix
  end

  # Public: Get a title for the "Contribution activity" section of the user profile.
  #
  # org_name - String profile name or login for the organization being used to filter activity;
  #            can be nil
  #
  # Returns a String.
  def contribution_activity_title(org_name:)
    prefix = "Contribution activity"
    suffix = "in #{org_name}" if org_name.present?
    [prefix, suffix].compact.join(" ")
  end

  # Public: Returns a url with the correct date parameters for viewing the specified year on a
  # user's timeline.
  #
  # year - year being viewed; Integer
  # user - User whose profile is being viewed, or their login
  # org - String Organization login or nil
  #
  # Returns a String.
  def contribution_year_url(year:, user:, org:)
    url = user_path(user) + "?tab=overview"
    today = Date.current

    if today.year == year.to_i
      url << "&from=#{today.beginning_of_month}"
      url << "&to=#{today}"
    else
      url << "&from=#{year}-12-01"
      url << "&to=#{year}-12-31"
    end

    url << "&org=#{org}" if org.present?
    url
  end

  # Public: Returns true if the user owns the item with the given owner ID.
  #
  # owner_id - ID of the owner of some record
  # user_id - ID of a User such as the current viewer
  #
  # Returns a Boolean.
  def owned_by_user?(owner_id:, user_id:)
    owner_id == user_id
  end

  # Public: Returns data attributes for tracking clicks on the profile page
  #
  # target - Symbol that maps to hydro.schemas.github.v1.UserProfileClick.EventTarget
  #
  # Returns a hash to be passed as `:data` to `link_to`
  def profile_click_tracking_attrs(target, current_user_id: current_user&.id, profile_user_id: this_user&.id)
    return {} unless profile_user_id

    unless target.in?(Hydro::Schemas::Github::V1::UserProfileClick::EventTarget.constants)
      raise ArgumentError.new("#{target} is not a valid EventTarget value")
    end

    hydro_click_tracking_attributes("user_profile.click",
                                    profile_user_id: profile_user_id,
                                    target: target,
                                    user_id: current_user_id)
  end

  # Public: Generate a link tag for a user profile tab
  #
  # url - where to link to; String
  # tab_name - the name of the tab; String
  # is_selected - whether or not this tab is currently active; Boolean
  #
  # Returns HTML
  def profile_tab_link(url, tab_name:, is_selected:)
    options = is_selected ? { "aria-current" => "page" } : {}
    data_attrs = profile_click_tracking_attrs(:"TAB_#{tab_name.upcase}")

    link_to url, options.merge(class: "UnderlineNav-item #{"selected #{custom_profile_border_class}" if is_selected}", data: data_attrs) do
      yield
    end
  end

  # Public: generate a button that folds a contributions timeline rollup
  #
  # type - either "CATEGORY" or "REPO"
  #
  # Returns HTML
  def profile_rollup_fold_button(type:)
    data = profile_click_tracking_attrs(:"TIMELINE_#{type}_ROLLUP_COLLAPSE")
    content_tag :span, class: "profile-rollup-toggle-closed float-right", aria_label: "Collapse", data: data do
      octicon(:fold)
    end
  end

  # Public: generate a button that unfolds a contributions timeline rollup
  #
  # type - either "CATEGORY" or "REPO"
  #
  # Returns HTML
  def profile_rollup_unfold_button(type:)
    data = profile_click_tracking_attrs(:"TIMELINE_#{type}_ROLLUP_EXPAND")
    content_tag :span, class: "profile-rollup-toggle-open float-right", aria_label: "Expand", data: data do
      octicon(:unfold)
    end
  end

  DateRangeFragment = parse_query <<-'GRAPHQL'
    fragment ContributionsCollection on ContributionsCollection {
      startedAt
      endedAt
      mostRecentCollection {
        startedAt
        endedAt
        hasAnyContributions
      }
    }
  GRAPHQL

  # Public: Returns a date range of a contribution collection GraphQL
  # instance taking the user's time zone into account
  #
  # contrib_collection - GraphQL instance of ContributionCollection
  #
  # Returns DateRange
  def date_range_in_time_zone(contrib_collection)
    contrib_collection = DateRangeFragment::ContributionsCollection.new(contrib_collection)
    start_date = contrib_collection.started_at.in_time_zone(Time.zone).to_date
    end_date = contrib_collection.ended_at.in_time_zone(Time.zone).to_date
    start_date..end_date
  end

  # Public: Returns a String representing the range of months this view spans.
  #
  # contrib_collection - GraphQL instance of ContributionCollection
  #
  # Returns nil if a full year from January to December is spanned.
  def timeline_month_range(contrib_collection)
    date_range = prior_activity_date_range(contrib_collection)

    start_month = date_range.begin.month
    end_month = date_range.end.month

    if start_month == end_month # only viewing one month
      date_range.begin.strftime("%B")
    elsif start_month > 1 || end_month < 12 # not a full year from Jan - Dec
      "#{date_range.begin.strftime("%B")} - #{date_range.end.strftime("%B")}"
    end
  end

  # Public: Returns the time range of prior activity, if the collector
  # before this has activity, then use the next month after the earliest
  # date since there was activity on the earliest date's month
  #
  # contrib_collection - GraphQL instance of ContributionCollection
  #
  # Returns a date range
  def prior_activity_date_range(contrib_collection)
    contrib_collection = DateRangeFragment::ContributionsCollection.new(contrib_collection)
    earliest_date = contrib_collection.most_recent_collection.started_at.in_time_zone(Time.zone)
    latest_date = contrib_collection.ended_at.in_time_zone(Time.zone)

    if contrib_collection.most_recent_collection.has_any_contributions
      no_activity_month = earliest_date.to_date + 1.month
      earliest_date = no_activity_month.in_time_zone(Time.zone)
    end

    earliest_date.to_date..latest_date.to_date
  end

  MutualFollowerLoginFragment = parse_query <<-'GRAPHQL'
    fragment User on User {
      mutualFollowers: mutualFollowers(first: 3) {
        totalCount
        nodes {
          login
          id
          ...HovercardHelper::DataAttributesFragment::User
        }
      }
    }
  GRAPHQL

  # Public: Generates string with links to mutual followers between viewer and user
  #
  # user - GraphQL user instance
  # limit - How many logins to include in the string
  #
  # Returns a string with links to mutual followers
  def mutual_followers_logins(user:, limit: 3)
    user = MutualFollowerLoginFragment::User.new(user)
    followers = user.mutual_followers
    hydro_tracking_attrs = profile_click_tracking_attrs(:MUTUAL_FOLLOWER_LINK)

    follower_logins = followers.nodes.take(limit)&.map do |user|
      link_to "@#{user.login}", user_path(user.login),
        data: {**hovercard_data_attributes_for_user(user).symbolize_keys, **hydro_tracking_attrs.symbolize_keys},
        class: "link-gray-dark"
    end
    return to_sentence(follower_logins) unless followers.total_count > limit

    follower_login_string = to_sentence(follower_logins, last_word_connector: ", ")
    other_follower_count = followers.total_count - limit
    plural_person = "person".pluralize(other_follower_count)
    follower_login_string + " and #{other_follower_count} other #{plural_person} you know."
  end
end
