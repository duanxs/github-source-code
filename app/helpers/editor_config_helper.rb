# frozen_string_literal: true

module EditorConfigHelper
  DEFAULT_TAB_SIZE = 8
  DEFAULT_SOFT_TAB_SIZE = 2
  MIN_TAB_SIZE = 1
  MAX_TAB_SIZE = 12

  def load_blob_editor_configs!(commit, blob)
    return unless commit
    # TODO: GIST!!!
    return unless current_repository.respond_to?(:load_editor_config)
    @editor_configs = current_repository.load_editor_config(commit, [blob.path])
  end

  def load_diff_editor_configs!(diff)
    return unless diff.available?
    # TODO: GIST!!!
    return unless diff.repo.respond_to?(:load_editor_config)
    commit = diff.repo.commits.find(diff.sha2)
    @editor_configs = diff.repo.load_editor_config(commit, diff.to_a.map(&:path))
  end

  def cast_tab_width(n)
    n = n.to_i
    (n >= MIN_TAB_SIZE && n <= MAX_TAB_SIZE) ? n : nil
  end

  def editor_config_indent_style(path)
    @editor_configs ||= {}
    if config = @editor_configs[path]
      config["indent_style"]
    end
  end

  def editor_config_indent_size(path)
    @editor_configs ||= {}
    if config = @editor_configs[path]
      cast_tab_width config["indent_size"]
    end
  end

  def editor_config_tab_width(path)
    @editor_configs ||= {}
    if config = @editor_configs[path]
      cast_tab_width config["tab_width"]
    end
  end

  def user_tab_size
    cast_tab_width params[:ts]
  end

  def tab_size(path)
    user_tab_size || editor_config_tab_width(path) || DEFAULT_TAB_SIZE
  end

  # Does the given chunk of code use soft tabs (spaces) for indentation or hard
  # tabs (\t)? This method tries to figure it out/
  #
  # blob - Blob object
  #
  # Returns "tab" or "space".
  def guess_indent_style(blob)
    blob.data =~ /^\t/m ? "tab" : "space"
  end

  # Tries to figure out the tab size, ie number of spaces per indent or "tab",
  # in a chunk of code.
  #
  # blob - Blob object
  #
  # Returns a Integer.
  def guess_indent_size(blob)
    if guess_indent_style(blob) == "space"
      match = blob.data.match(/^( +)[^*]/im)
      cast_tab_width match ? match[1].length : DEFAULT_SOFT_TAB_SIZE
    else
      DEFAULT_TAB_SIZE
    end
  end

  def blob_editor_wrap_mode(blob)
    blob.language.try(:wrap) ? "on" : "off"
  end

  def blob_editor_indent_style(blob)
    editor_config_indent_style(blob.path) || guess_indent_style(blob)
  end

  def blob_editor_indent_size(blob)
    editor_config_indent_size(blob.path) || guess_indent_size(blob)
  end
end
