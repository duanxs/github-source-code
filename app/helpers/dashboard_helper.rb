# frozen_string_literal: true

module DashboardHelper
  include ExperimentHelper
  include PlatformHelper
  include UrlHelper

  def recent_interactable_tooltip(is_draft:, state:, type:)
    parts = []
    if is_draft
      parts << "Draft"
    else
      parts << state.capitalize
    end
    parts << type
    parts.join(" ")
  end

  def show_user_dashboard_right_column?(recommendations:, show_user_pins:)
    return true if show_user_pins
    return true if recommendations.present? # any recommended repositories

    # any job listings to show
    !GitHub.enterprise? && show_jobs_for_user?(current_user)
  end

  ATOM_CONTENT_TYPES = ["atom", "application/atom+xml"].freeze

  def atom_feed?
    # No request/response when coming from MentionFilter in HTML pipeline
    is_atom_request = respond_to?(:request) && request && request.format.atom?
    is_atom_response = respond_to?(:response) && response &&
      ATOM_CONTENT_TYPES.include?(response.media_type)
    is_atom_request || is_atom_response
  end

  def feed_has_only_one_page_of_events?
    return false if current_page > 1
    event_count < Stratocaster::DEFAULT_PAGE_SIZE
  end

  def show_sso_links_for_recent_activity?
    current_organization.nil? && unauthorized_saml_targets.any?
  end

  def show_ip_whitelisting_hints_for_recent_activity?
    current_organization.nil? && unauthorized_ip_whitelisting_targets.any?
  end

  def display_private_repo_limit_banner?
    return false unless logged_in?

    !current_user.ignored_upgrade? && current_user.at_plan_repo_limit? && !current_user.disabled?
  end

  def display_org_private_repo_limit_banner?
    org_admin? && !current_organization.ignored_upgrade? &&
      current_organization.at_plan_repo_limit? && !current_organization.disabled?
  end

  def login_sentence(logins)
    logins.uniq.to_sentence
  end

  def dashboard_contexts
    @dashboard_contexts ||= [current_user] + organizations_as_contexts
  end

  def deferred_context_loading?
    dashboard_contexts.size > 3
  end

  # Public: Returns true if the current user can change to another context on their dashboard.
  # This happens when a user belongs to an organization.
  def user_can_switch_contexts?
    dashboard_contexts.size > 1
  end

  # Which organizations is the user in
  # plus with which organization the user is a billing manager for
  #
  # Returns an array
  #   can include Organization objects,
  #   also arrays of [Organization, 'billing']
  def organizations_as_contexts
    @organizations_as_contexts ||= begin
      orgs = current_user.organizations.includes(:profile).reject(&:deleted?)
      billing_related_orgs = current_user.billing_manager_organizations.includes(:profile).reject(&:deleted?)
      billing_contexts = (billing_related_orgs - orgs).map { |org| [org, "billing"] }
      (orgs + billing_contexts).sort_by { |e| Array(e).first.login }
    end
  end

  def show_bootcamp?(user)
    !user.dismissed_notice?("bootcamp")
  end

  def instrument_recommendation(user_id, analytics_hash = {})
    analytics_hash = analytics_hash.merge({actor_id: user_id, timestamp: Time.now, request_id: request_id})
    GitHub.instrument("dashboard.discovery_recommendations", analytics_hash)
  end

  # Is the user a billing manager for the supplied orgs?
  #
  # Returns a Boolean.
  def user_is_billing_manager?(user, orgs)
    orgs.any? do |org|
      org.billing_manager_only?(user)
    end
  end

  def dashboard_news_feed_next_page_path
    if current_organization
      organizations_news_feed_path(current_organization, page: current_page + 1)
    else
      dashboard_news_feed_path(page: current_page + 1)
    end
  end

  def render_org_welcome
    if org_admin?
      render partial: "organizations/welcome_owner", locals: { org: current_organization }
    else
      render partial: "organizations/welcome", locals: { org: current_organization }
    end
  end

  TEACHER_ROLE_SHORT_TEXT = "role_teacher"
  STUDENT_ROLE_SHORT_TEXT = "role_student"

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def show_student_developer_pack?
    @show_student_developer_pack ||= !dismissed_student_developer_pack_notice? && (answered_role_as_student || edu_email_eligible?)
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def student_developer_pack_dismissible?
    edu_email_eligible? && !answered_role_as_student
  end

  def dismissed_student_developer_pack_notice?
    return true if current_user.dismissed_notice?(UserNotice::DASHBOARD_STUDENT_PACK_NOTICE) || current_user.student_developer_pack_coupon? || !current_user.is_new?
    false
  end

  private

  def answered_role_as_student
    @answered_role_as_student ||= answered_role_question_as(role_short_text: STUDENT_ROLE_SHORT_TEXT)
  end

  # User has an .edu email and also did not answer the survey question with "Teacher"
  # Trying to avoid showing non-students with .edu emails the student pack CTA
  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def edu_email_eligible?
    @edu_email_eligible ||= current_user.edu_email? && !answered_role_question_as(role_short_text: TEACHER_ROLE_SHORT_TEXT)
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def answered_role_question_as(role_short_text:)
    role_choice = user_identification_survey_role_question&.choices&.find_by(short_text: role_short_text)

    return false unless role_choice.present?

    user_identification_survey.answers_for(current_user).find_by(
      question_id: user_identification_survey_role_question.id,
      choice_id: role_choice.id,
    ).present?
  end

  def user_identification_survey
    @user_identification_survey ||= Survey.find_by_slug("user_identification")
  end

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def user_identification_survey_role_question
    @user_identification_survey_role_question ||= user_identification_survey&.questions&.find_by(short_text: "user_role")
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator
end
