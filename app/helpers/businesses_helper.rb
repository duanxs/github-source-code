# frozen_string_literal: true
module BusinessesHelper
  MEMBERS_QUERY_FILTERS = {
    role: {
      filter: /\brole:(\S+)/.freeze,
      multi: false,
      graphql_enum: Platform::Enums::EnterpriseUserAccountMembershipRole,
    },
    organizations: {
      filter: /\borganization:(\S+)/.freeze,
      multi: true,
    },
    deployment: {
      filter: /\bdeployment:(\S+)/.freeze,
      multi: false,
      graphql_enum: Platform::Enums::EnterpriseUserDeployment,
    },
    license: {
      filter: /\blicense:(\S+)/.freeze,
      multi: false,
      graphql_enum: Platform::Enums::UserLicenseType,
    },
  }

  OUTSIDE_COLLABS_QUERY_FILTERS = {
    visibility: {
      filter: /\bvisibility:(\S+)/.freeze,
      multi: false,
      graphql_enum: Platform::Enums::RepositoryVisibility,
    },
  }

  ADMINS_QUERY_FILTERS = {
    role: {
      filter: /\brole:(\S+)/.freeze,
      multi: false,
    },
  }

  USER_ACCOUNT_MEMBERSHIP_QUERY_FILTERS = {
    role: {
      filter: /\brole:(\S+)/.freeze,
      multi: false,
      graphql_enum: Platform::Enums::EnterpriseUserAccountMembershipRole,
    },
  }

  def self.included(base)
    return unless base.respond_to? :helper_method
    base.helper_method :current_business
  end

  #
  # Controller & View Helpers
  #

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def current_business
    @current_business ||= begin
      business = find_business
      return business if business || !defined?(:current_organization)
      current_organization&.business
    end
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def business_saml_sso_enabled?
    return true if GitHub.private_instance_user_provisioning?
    !GitHub.single_business_environment?
  end

  def parse_query_string(query, graphql: false, filter_map: {})
    return {} if query.nil?

    filters = filter_map.map do |key, options|
      # parse the query string for the given query filter
      filter_result = query.scan(options[:filter]).map do |match|
        value = match[0]
        next value unless options[:graphql_enum]

        enum = options[:graphql_enum].values.values.find { |enum| enum.value == value }
        next unless enum
        graphql ? enum.graphql_name : value
      end

      # remove all filter matches from the query string
      query = query.gsub(options[:filter], "")

      filter_result.compact!
      filter_result = filter_result.last unless options[:multi]
      { key => filter_result }
    end

    # merge the filter results with the query without any of the filters
    # query.split.join(" ") does a strip on all words, removing both
    # internal and external unnecessary whitespace
    filters.reduce({}, &:merge).merge(query: query.split.join(" "))
  end

  def business_user_avatar(user, size = 44, options = {})
    return avatar_for(user, size, options) if user.is_a?(User)
    return avatar_for(user.user, size, options) if user.is_a?(BusinessUserAccount)
    return graphql_avatar_for(user, size, options)
  end

  def business_stafftools_resource_path(business)
    # The enterprise /stafftools routes are not available to site admins in
    # the single global business environment (Enterprise Server), so set
    # the default "/stafftools" path here in that case.
    path = if GitHub.single_business_environment?
      Addressable::URI.parse "/stafftools"
    else
      template = Addressable::Template.new("/stafftools/enterprises/{slug}")
      template.expand slug: business.slug
    end

    path.to_s
  end

  def current_user_can_manage_settings(business)
    business.owner?(current_user) || current_user.site_admin?
  end

  def member_role(organization_role)
    case organization_role.to_s
    when "direct_member"
      "Member"
    when "admin"
      "Owner"
    end
  end

  def service_provider_url
    return GitHub.url if GitHub.private_instance?
    enterprise_url(this_business)
  end

  def saml_consume_url
    return super if GitHub.private_instance?
    idm_saml_consume_enterprise_url(this_business)
  end

  # Check that the support link type and support link are valid. The returned
  # Array will contain any validation errors.
  #
  # type - String representing the support link type ("email" or "url")
  # link - String representing the support link
  #
  # Returns Array.
  def validate_support_link(type, link)
    errors = []
    case type
    when "url"
      unless link =~ URI::DEFAULT_PARSER.make_regexp
        errors << "URL does not look like a URL"
      end
    when "email"
      unless link =~ User::EMAIL_REGEX
        errors << "Email does not look like an email address"
      end
    else
      errors << "Invalid support link type"
    end
    errors
  end

  # Is there an enterprise configuration run in progress?
  #
  # Currently a no-op outside of a GHPI environment. On GHPI this returns true
  # if there is a configuration run in progress, otherwise false.
  #
  # Returns Boolean.
  def config_run_in_progress?
    return false unless GitHub.private_instance?
    return @config_run_in_progress if defined?(@config_run_in_progress)
    @config_run_in_progress = GitHub.enterprise_configuration_updater.config_run_in_progress?
  end

  private

  def find_business
    Business.find_by(slug: slug_param)
  end

  def slug_param
    params[:slug]
  end

  # Returns true if we are in GHPI, but haven't finished the bootstrap flow yet,
  #         false if not on GHPI, or on a fully-bootstrapped GHPI.
  def bootstrap_flow?
    GitHub.private_instance? && !GitHub.private_instance_bootstrapper.bootstrapped?
  end

  # Extracts the parameters coming from a "Test SAML settings" flow,
  # mixing them with the existing controller parameters
  #
  # Returns an ActionController::Parameters hash that includes the
  # existing controller #params and test-related keys used by
  # Businesses::Settings::SecurityView if a test was performed,
  # otherwise just the current #params. params if no test was performed.
  def params_for_saml_test_result
    extra_params = {}
    if flash[:saml_test_result]
      test_settings = Business::SamlProviderTestSettings.most_recent_for(
        user: current_user,
        business: this_business,
        result: flash[:saml_test_result],
      )
      if test_settings
        ActiveRecord::Base.connected_to(role: :writing) do
          test_settings.save!
        end
        extra_params[:test_settings] = "1"
        if test_settings.success?
          extra_params[:saml_testing] = { success: true }
        else
          extra_params[:saml_testing] = { failure: true }
          extra_params[:saml_testing][:message] = test_settings.message
        end

        extra_params[:saml] = {
          sso_url: test_settings.sso_url,
          issuer: test_settings.issuer,
          idp_certificate: test_settings.idp_certificate,
          signature_method: test_settings.signature_method,
          digest_method: test_settings.digest_method,
        }
      end
    end
    extra_params[:current_external_identity] = current_external_identity(target: this_business)

    params.merge extra_params
  end
end
