# frozen_string_literal: true

module CodeNavigationHelper
  CODE_NAV_UI_ONLY_LANGUAGES = (
    %w(HTML+ERB Markdown)
  ).freeze

  # Public: Does this blob support code navigation (jump to definition and find all references).
  # NOTE: This includes CODE_NAV_UI_ONLY_LANGUAGES.
  def blob_supports_code_navigation?(blob)
    return false unless blob
    return false unless aleph_code_navigation_available?

    lang_name = blob&.language&.name
    language_supported?(lang_name) || CODE_NAV_UI_ONLY_LANGUAGES.include?(lang_name)
  end

  # Public: Does this blob support listing symbols (outline of methods and
  # functions defined in the file).
  # NOTE: Does not include languages in CODE_NAV_UI_ONLY_LANGUAGES.
  def blob_supports_listing_symbols?(blob)
    return false unless blob
    return false unless aleph_code_navigation_available?

    language_supported?(blob&.language&.name)
  end

  # Show the code navigation feature announcement banner?
  # NOTE: Does not include languages in CODE_NAV_UI_ONLY_LANGUAGES.
  def show_code_navigation_banner?(viewer:, blob:)
    blob_supports_listing_symbols?(blob) &&
      viewer && !viewer.dismissed_notice?("aleph_code_navigation_banner")
  end

  # Public: Instrument viewing code navigation for a blob
  def instrument_view_code_navigation(viewer:, repo:, blob_supports_listing_symbols:, blob_has_code_symbols:, ref:, language:)
    event = if blob_has_code_symbols
      "symbols_available"
    elsif blob_supports_listing_symbols
      "symbols_unavailable"
    else
      "language_not_supported"
    end

    repo_visibility = repo.public? ? "public" : "private"
    action = "view_blob_#{event}"

    GitHub.dogstats.increment("blob_view", tags: ["visibility:#{repo_visibility}", "language:#{language}", "action:#{action}"])

    # Only instrument in hydro for views where aleph is available and enabled
    return unless aleph_code_navigation_available?
    GlobalInstrumenter.instrument("code_navigation.#{action}", {
      action: action,
      user_id: viewer&.id, # nil for anonymous users
      repository_id: repo.id,
      ref: ref,
      language: language,
    })
  end

  # Public: Helper for hydro click tracking for code navigation links
  def code_navigation_hydro_click_tracking(action, repo:, ref:, language:)
    hydro_click_tracking_attributes("code_navigation.#{action}", {
      action: action,
      repository_id: repo.id,
      ref: ref.dup.force_encoding(Encoding::UTF_8),
      language: language,
    })
  end

  # Language support is defined by the existence of an aleph language feature flag (e.g. "aleph_language_ruby").
  # See https://github.com/github/semantic-code/blob/master/process/new-language-support.md for more information.
  def language_supported?(language_name)
    GitHub.flipper[GitHub::Aleph.convert_language_name(language_name)].exist?
  end
end
