# frozen_string_literal: true

module PlanHelper
  def human_plan_cost(plan, target = nil, currency_code = nil)
    target ||= current_user

    list_price = Billing::Pricing.new(
      account: target,
      plan: plan,
      seats: target.default_seats,
      plan_duration: target.plan_duration,
      coupon: target.coupon,
    ).discounted

    money(list_price, currency_code)
  end

  def full_plan_pricing(account, plan, currency_code = nil)
    "#{human_plan_cost(plan, account, currency_code)}/#{account.plan_duration}"
  end

  # Returns a currency string with two decimal place precision if
  # fractional currency exists, otherwise no decimal places.
  #
  # price - Integer or Float to be converted to currency String
  # discount (optional) - percentage to take off of original price (scale of 0
  # to 100)
  #
  # Returns a String
  def casual_currency(price, discount: 0)
    price = 0 unless price.respond_to? :round
    price = price * (100 - discount)/100.0
    number_to_currency price, precision: (price.round == price) ? 0 : 2
  end

  def payment_using_balance(target, new_plan)
    [target.payment_difference(new_plan, use_balance: true), 0].max
  end

  def increase_or_decrease(target, new_plan)
    target.payment_difference(new_plan) > 0 ? "increase" : "decrease"
  end

  def upgrade_or_downgrade(target, new_plan)
    if new_plan == target.plan
      "change"
    elsif new_plan > target.plan
      "upgrade"
    else
      "downgrade"
    end
  end

  def show_free_org_gated_feature_message?(repository, user)
    GitHub.billing_enabled? &&
      repository.owner.organization? &&
      repository.owner.adminable_by?(user) &&
      repository.owner.plan.free?
  end
end
