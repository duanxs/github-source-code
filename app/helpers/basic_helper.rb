# frozen_string_literal: true

# Basic view helpers, not dependent on any models.  This is shared with the
# Event mustache views.
#
# Requires ActionView::Helpers::TagHelper
module BasicHelper
  # Return the time formatted as a String: 2012-06-12 13:21:52.
  def timestamp(time)
    time.in_time_zone(Time.zone).strftime("%Y-%m-%d %H:%M:%S")
  end

  # Public: Format the given time with the full month name, e.g., May 25, 2016.
  #
  # time - a Time or DateTime
  # omit_current_year - set to true when you want to not include the year when the given time
  #                     is in the current year; defaults to false
  #
  # Returns a String.
  def full_month_date(time, omit_current_year: false)
    viewer_time = time.in_time_zone
    format = if omit_current_year && viewer_time.year == Time.zone.now.year
      "%b %-d"
    else
      "%b %-d, %Y"
    end
    viewer_time.strftime(format)
  end

  def time_ago_in_words_js(time, class_name = nil)
    content_tag :"relative-time", full_month_date(time),
      datetime: time.utc.iso8601,
      class: ["no-wrap", class_name].compact.join(" ")
  end

  # Create a custom time element for the time-elements js to replace with
  # an internationalized format in the browser's locale.
  #
  # time - a DateTime or Time
  # micro - Boolean choice of whether to use micro format; defaults to false;
  #         if true, date string will be like "4h" instead of "about 4 hours"
  #
  # Examples
  #
  #   time_ago_js(Date.new)
  #   # => <time-ago datetime="2014-06-01T13:21:45Z">June 1, 2014</time-time>
  #
  # If js is enabled, the content of the element is replaced with a relative
  # time, like `2 days ago`, at page load.
  #
  # See https://github.com/github/time-elements for implementation details.
  #
  # Returns a <time> element String.
  def time_ago_js(time, micro: false)
    attrs = {
      datetime: time.utc.iso8601,
      class: "no-wrap"
    }

    if micro
      attrs[:format] = "micro"
      content_tag :"time-ago", micro_time_ago(time), attrs
    else
      content_tag :"time-ago", full_month_date(time), attrs
    end
  end

  def micro_time_ago(time)
    seconds_ago = Time.current - time

    if seconds_ago < 1.minute
      "1m"
    elsif seconds_ago >= 1.minute && seconds_ago < 1.hour
      "#{(seconds_ago / 60).floor}m"
    elsif seconds_ago >= 1.hour && seconds_ago < 1.day
      "#{(seconds_ago / 60 / 60).floor}h"
    elsif seconds_ago >= 1.day && seconds_ago < 1.year
      "#{(seconds_ago / 60 / 60 / 24).floor}d"
    elsif seconds_ago >= 1.year
      "#{(seconds_ago / 60 / 60 / 24 / 365).floor}y"
    end
  end

  def date_with_time_tooltip(time)
    content_tag :time, time.strftime("%Y-%m-%d"), title: timestamp(time), class: "no-wrap"
  end

  # Renders either a <local-time> element OR a <relative-time> element
  # depending on if published_on is >= 1 month old. This is a workaround because
  # <relative-time> will always prepend the string "on" on the output, which we don't want.
  #
  def local_or_relative_time_tag(time)
    if 1.month.ago >= time
      content_tag "local-time", nil, datetime: time, month: "short", day: "2-digit", year: "numeric", class: "no-wrap"
    else
      content_tag "relative-time", nil, datetime: time, class: "no-wrap"
    end
  end
end
