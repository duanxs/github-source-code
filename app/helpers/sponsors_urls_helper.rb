# frozen_string_literal: true

module SponsorsUrlsHelper
  def sponsorable_home_path
    total_accounts = current_user.sponsors_enabled_accounts.length
    if total_accounts > 1
      sponsors_accounts_path
    else
      sponsorable_dashboard_path(current_user)
    end
  end
end
