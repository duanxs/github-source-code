# frozen_string_literal: true

module LabelEducationHelper

  # Public: Returns true if the viewer should see a banner explaining how GitHub uses first-time contributor labels
  #
  # Returns a Boolean.
  def show_maintainer_label_education_banner?
    !GitHub.enterprise? &&
      current_repository &&
      logged_in? &&
      !current_repository.private? &&
      current_repository.has_issues? &&
      !current_repository.fork? &&
      !current_repository.spammy? &&
      !current_user.dismissed_notice?("maintainer_label_education_banner") &&
      current_repository.writable? &&
      current_repository.pushable_by?(current_user) &&
      !current_user.blocked_by?(current_repository.owner)
  end

  def help_wanted_label
    current_repository.help_wanted_label || Label.default_for(Label::HELP_WANTED_NAME)
  end

  def good_first_issue_label
    current_repository.good_first_issue_label || Label.default_for(Label::GOOD_FIRST_ISSUE_NAME)
  end
end
