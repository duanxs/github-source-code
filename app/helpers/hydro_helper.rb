# frozen_string_literal: true

module HydroHelper
  extend self

  InvalidPayloadError = Class.new(ArgumentError)
  HYDRO_CLIENT_CONTEXT_PERMITTED_KEYS = [
    "title_length",
    "number_of_similar_issues",
    "position_in_list",
    "title_string_at_time_of_click",
    "id_of_clicked_result",
    "results_shown_at_time_of_click",
    "starting_diff_position",
    "ending_diff_position",
    "line_count",
  ]

  # Given an event type and payload, produce data attributes for use in a view.
  # If the payload HMAC cannot be computed (e.g., because the secret is not set
  # or the data is nil) an empty Hash is returned. This is done instead of
  # returning nil so you can merge in other data attributes you may be using.
  #
  # event_type - a String naming the hydro instrumentation that will listen for this event
  # payload    - a Hash of event data
  #
  # Returns a (potentially empty) Hash of attributes to be used with the Rails data parameter.
  def hydro_click_tracking_attributes(event_type, payload)
    payload = encode_hydro_payload(event_type, payload)
    hmac = hydro_payload_hmac(payload)

    return {} unless hmac

    # NOTE: These become the data-hydro-click and data-hydro-click-hmac attributes in the HTML
    { "hydro-click" => payload, "hydro-click-hmac" => hmac }
  end

  # Given an event type and payload, produce data attributes for use in a view.
  # If the payload HMAC cannot be computed (e.g., because the secret is not set
  # or the data is nil) an empty Hash is returned. This is done instead of
  # returning nil so you can merge in other data attributes you may be using.
  #
  # event_type - a String naming the hydro instrumentation that will listen for this event
  # payload    - a Hash of event data
  #
  # Returns a (potentially empty) Hash of attributes to be used with the Rails data parameter.
  def hydro_view_tracking_attributes(event_type, payload)
    payload = encode_hydro_payload(event_type, payload)
    hmac = hydro_payload_hmac(payload)

    return {} unless hmac

    # NOTE: These become the data-hydro-view and data-hydro-view-hmac attributes in the HTML
    { "hydro-view" => payload, "hydro-view-hmac" => hmac }
  end

  def encode_hydro_payload(event_type, payload)
    payload = payload.merge(originating_url: GitHub.context[:url])
    payload[:user_id] ||= GitHub.context[:actor_id]
    GitHub::JSON.encode(event_type: event_type, payload: payload)
  end

  def decode_hydro_payload(encoded:, hmac:)
    unless SecurityUtils.secure_compare(hmac, hydro_payload_hmac(encoded))
      raise InvalidPayloadError.new("Invalid HMAC")
    end

    begin
      decoded = GitHub::JSON.decode(encoded)
    rescue Yajl::ParseError
      raise InvalidPayloadError.new("Invalid JSON")
    end

    unless decoded.is_a?(Hash)
      raise InvalidPayloadError.new("Expected a Hash, got #{decoded.class}")
    end

    decoded.with_indifferent_access
  end

  # Calculate the HMAC for data using GitHub.hydro_browser_payload_secret. If
  # data is nil or GitHub.hydro_browser_payload_secret is not set, returns nil.
  #
  # data - a String (presumably of JSON) to be authenticated
  #
  # Returns the HMAC as a hex string or nil.
  def hydro_payload_hmac(data)
    unless data.nil? || GitHub.hydro_browser_payload_secret.nil?
      OpenSSL::HMAC.hexdigest("sha256", GitHub.hydro_browser_payload_secret, data)
    end
  end

  # Takes a JSON string, parses it and removes any keys that
  # are not whitelisted (HYDRO_CLIENT_CONTEXT_PERMITTED_KEYS.)
  #
  # client_context_string - a String (presumably of JSON)
  #
  # Returns Hash
  def decode_hydro_client_context(client_context_string)
    return {} if client_context_string.blank?

    begin
      client_context = GitHub::JSON.decode(client_context_string)
    rescue Yajl::ParseError
      return {}
    end

    return {} unless client_context.is_a?(Hash)

    client_context.slice(*HYDRO_CLIENT_CONTEXT_PERMITTED_KEYS).with_indifferent_access
  end
end
