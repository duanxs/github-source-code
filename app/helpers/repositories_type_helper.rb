# frozen_string_literal: true

module RepositoriesTypeHelper
  def self.type(visibility:, mirror:, archived:, template:)
    visibility_label = visibility.capitalize # Public, Private, or Internal
    if visibility.downcase == "public"
      if archived
        "Archived"
      elsif mirror
        "Mirror"
      elsif template
        "Template"
      else
        ""
      end
    elsif archived
      "#{visibility_label} archived"
    elsif mirror
      "#{visibility_label} mirror"
    elsif template
      "#{visibility_label} template"
    else
      visibility_label
    end
  end
end
