# frozen_string_literal: true

module SettingsHelper
  # Creates the check box and hidden tag for notification settings.  The
  # hidden tag forces a sent value in the case that all checkboxes are
  # unchecked.
  #
  # settings - The Newsies::Settings object for the current User.
  # type     - The Symbol type of Notification settings: :important, :mentions,
  #            or :subscribed
  # handler  - The Symbol type of Notification handler: :email or :web.
  #
  # Returns an HTML safe String.

  CONTEXT_DROPDOWN_MIN = 10

  def notification_setting_checkbox(settings, type, handler)
    if settings.present?
      current_settings = settings.send "#{type}_settings"
      field_name = "#{type}_settings[#{handler}]"
      field_id = "#{type}_settings_#{handler}"
      hidden_field_tag(field_name, 0, id: field_id + "_0") +
        check_box_tag(field_name, 1, current_settings.include?(handler.to_s), id: field_id + "_1")
    end
  end

  def show_private_contribution_count?
    profile_settings = current_user.profile_settings
    profile_settings.show_private_contribution_count?
  end

  def pro_badge_enabled?
    profile_settings = current_user.profile_settings
    profile_settings.pro_badge_enabled?
  end

  def acv_badge_enabled?
    current_user.profile_settings.acv_badge_enabled?
  end

  # Creates the checkbox for the notification setting for receiving various email
  # notifications
  #
  # Returns an HTML safe String
  def notify_own_via_email_checkbox(settings)
    notify_checkbox(settings, "notify_own_via_email")
  end

  def notify_comment_checkbox(settings)
    notify_checkbox(settings, "notify_comment_email")
  end

  def notify_pull_request_review_checkbox(settings)
    notify_checkbox(settings, "notify_pull_request_review_email")
  end

  def notify_pull_request_push_checkbox(settings)
    notify_checkbox(settings, "notify_pull_request_push_email")
  end

  def vulnerability_ui_alert_checkbox(settings)
    notify_checkbox(settings, "vulnerability_ui_alert")
  end

  def vulnerability_cli_checkbox(settings)
    notify_checkbox(settings, "vulnerability_cli")
  end

  def vulnerability_web_checkbox(settings)
    notify_checkbox(settings, "vulnerability_web")
  end

  def vulnerability_email_checkbox(settings)
    notify_checkbox(settings, "vulnerability_email")
  end

  def notify_checkbox(settings, name, disabled: false)
    checked  = !settings.nil? && settings.send("#{name}?")
    check_box_tag(name, 1, checked, id: name, disabled: disabled)
  end

  def notification_email_visibility_class(settings)
    # The section "GitHub Actions" under Notifications will not show if the
    # feature is not enabled. We need to check for the feature, in addition to
    # checking for the continuous integration email flag, because the "Email"
    # checkbox under "GitHub Actions" has a default value of **true**.
    # Without this additional check, the "Email notification preferences"
    # section would show even if the user had all the email notifications
    # unchecked in every other section and never saw "GitHub Actions".
    ci_email = settings.continuous_integration_email?

    return "" if settings.participating_email? || settings.subscribed_email? || settings.vulnerability_email? || settings.subscribed_vulnerability_digest?(current_user) || ci_email

    "d-none"
  end

  def notification_email_select_options(emails, current_email, default: "Choose one...")
    current = current_email.try(:downcase) || default
    emails = emails.dup.unshift current
    emails = emails.uniq
    options_for_select(emails, selected: current, disabled: default)
  end

  def security_and_privacy_section_title
    if third_party_analytics_setting_enabled?
      "Account Security and Privacy"
    else
      "Account security"
    end
  end

  # Gets the supported SMS countries.
  #
  # current - A country code string. If specified and country isn't supported,
  #           that country will be included in the returned Hash.
  #
  # Returns a Hash of country codes => country names.
  def sms_supported_countries(current = nil)
    if unsuported = GitHub::SMS::UNSUPPORTED_COUNTRY_CODES[current]
      combined = GitHub::SMS::SUPPORTED_COUNTRIES + unsuported
      combined.sort_by! { |code, name| name }
    else
      GitHub::SMS::SUPPORTED_COUNTRIES
    end
  end

  def org_whitelist_selectable_class
    current_organization.restricts_oauth_applications? ? "is-selectable" : ""
  end

  def key_created_at_description(key)
    parts = []
    if key.created_at
      time = content_tag(:"local-time", key.created_at.strftime("%b %-d, %Y"),
        datetime: key.created_at.utc.iso8601,
        month: "short",
        day: "numeric",
        year: "numeric")

      is_deploy_key = key.kind_of?(PublicKey) && key.repository_key?
      parts << "Added on " << time
      if authorization = key.try(:oauth_authorization)
        user = content_tag("strong") { "@#{authorization.user.login}" }
        if authorization.personal_access_authorization?
          parts << " via personal access token"
          parts << " owned by " << user if is_deploy_key
        else
          app = content_tag("strong") { authorization.application.name }
          parts << " by " << app
          parts << " with authorization from " << user if is_deploy_key
        end
      elsif is_deploy_key && key.creator.present?
        user = content_tag("strong") { "@#{key.creator.login}" }
        parts << " by " << user
      end
    end
    safe_join parts
  end

  # Creates a case-insensitive pattern for use with a regex when the ignore
  # case flag cannot be added to the regex. Currently used for passing the
  # pattern used for HTML5/JavaScript validation when prompting users to enter
  # a login or repository name when deleting/transferring.
  #
  # Example:
  #   case_insensitive_pattern("MiXeD")
  #   # => "[mM][iI][xX][eE][dD]"
  #
  # input - An input string. Currently used for logins, repository names, and
  # name with owner ("owner/repo") values.
  #
  # Returns a String that can be used to do a case-insensitive match for the
  # input value.
  def case_insensitive_pattern(input)
    Regexp.escape(input)
      .gsub(/[a-zA-Z]/) { |ch| "[#{ch.downcase}#{ch.upcase}]" }
      .gsub(/\\([ #-])/, '\1')
  end

  # Creates a case-insensitive pattern to be used for validation when a user is
  # asked to enter their "username or email" as confirmation before deleting
  # their account. Accepts their login or any of their emails.
  #
  # Example:
  #   account_deletion_username_pattern(user)
  #   # => "[mM][aA][xX]|[mM][eE]@[mM][aA][xX].[iI][oO]|[mM][aA][xX]@[mM][eE].[cC][oO][mM]"
  #
  # user - The user being deleted.
  #
  # Returns a String that can be used as a pattern for validation of the
  # "username or email" input.
  def account_deletion_username_pattern(user)
    input_option_patterns = [case_insensitive_pattern(user.login)]
    user.emails.user_entered_emails.each do |email|
      input_option_patterns << case_insensitive_pattern(email.email)
    end
    input_option_patterns.join("|")
  end

  def has_primary_avatar_to_reset?
    !!current_user.primary_avatar
  end

  def selected_link?(link:)
    current_page?(link) || current_page?("#{link}/")
  end

  def switch_context_link(current_context:, target_context:, permission:)
    selected_billing = selected_billing?(current_context: current_context)
    selected_security = selected_security?(current_context: current_context)

    if target_context.is_a?(Business)
      return settings_billing_enterprise_path(target_context) if selected_billing || permission == :billing_manager
      return settings_security_enterprise_path(target_context) if selected_security
      return settings_profile_enterprise_path(target_context)
    elsif target_context.organization?
      return settings_org_billing_path(target_context) if selected_billing || permission == :billing_manager
      return settings_org_security_path(target_context) if selected_security
      return settings_org_profile_path(target_context)
    else
      return settings_user_billing_path if selected_billing
      return settings_user_security_path if selected_security
      return settings_user_profile_path
    end
  end

  def selected_billing?(current_context:)
    selected_link?(link: settings_user_billing_path) || selected_link?(link: settings_org_billing_path(current_context))
  end

  def selected_security?(current_context:)
    selected_link?(link: settings_user_security_path) || selected_link?(link: settings_org_security_path(current_context))
  end

  def available_contexts(current_context:)
    all_contexts = {}

    if current_context.organization?
      all_contexts[current_user] = :user
    end

    current_user.owned_organizations.each { |org| all_contexts[org] = :admin }
    current_user.billing_manager_organizations.each { |org| all_contexts[org] ||= :billing_manager }
    current_user.businesses(membership_type: :admin).each { |business| all_contexts[business] = :admin }
    current_user.businesses(membership_type: :billing_manager).each { |business| all_contexts[business] ||= :billing_manager }
    all_contexts.reject do |context|
      (context.organization? && context.deleted) || context == current_context
    end
  end

  def settings_context_dropdown_attributes(event_context:, target:, target_name: nil, target_id: nil, target_link: nil, ga_attr: {})
    hydro_attributes = hydro_click_tracking_attributes(
      "settings_context_dropdown.click",
      user_id: current_user.id,
      event_context: event_context,
      target: target,
      target_name: target_name,
      target_id: target_id,
      target_link: target_link
    )

    safe_data_attributes(ga_attr.merge(hydro_attributes))
  end

  def tracking_target_type(context)
    case context
      when Organization then :ORG
      when User then :USER
      when Business then :BUSINESS
    end
  end

  def target_context_display_name(context)
    case context
      when User then context.login
      when Business then context.name
    end
  end

  def current_user_default_new_repo_branch
    @current_user_default_new_repo_branch ||= current_user.default_new_repo_branch
  end
end
