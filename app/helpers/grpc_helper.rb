# frozen_string_literal: true

module GrpcHelper
  def self.rescue_from_grpc_errors(service_name)
    raise ArgumentError, "A block must be passed to #{__method__}" unless block_given?
    begin
      GrpcResponse.new(status: 200, value: yield, call_succeeded: true)
    rescue GRPC::DeadlineExceeded => e
      Failbot.report(e)
      GrpcResponse.new(status: 500, call_succeeded: false)
    rescue GRPC::Internal => e
      Failbot.report(e)
      GrpcResponse.new(status: 500, call_succeeded: false)
    rescue GRPC::Unavailable => e
      Failbot.report(e)
      GrpcResponse.new(status: 503, options: { message: "#{service_name} service unavailable" }, call_succeeded: false)
    rescue GRPC::InvalidArgument => e
      # InvalidArgument indicates client specified an invalid argument.
      # Unlike FailedPrecondition it indicates arguments are problematic
      # regardless of the state of the system (i.e. malformed URL)
      GrpcResponse.new(status: 422, options: { message: "Invalid Argument" }, call_succeeded: false)
    rescue GRPC::FailedPrecondition => e
      # FailedPrecondition indicates operation was rejected because the
      # system is not in a state required for the operation's execution.
      GrpcResponse.new(status: 422, options: { message: "Bad request - #{e.details}" }, call_succeeded: false)
    rescue GRPC::NotFound
      GrpcResponse.new(status: 404, call_succeeded: false)
    rescue GRPC::AlreadyExists
      GrpcResponse.new(status: 409, call_succeeded: false)
    rescue GRPC::PermissionDenied => e
      GrpcResponse.new(status: 403, call_succeeded: false, options: { message: "Forbidden" })
    rescue GRPC::Unauthenticated => e
      Failbot.report(e)
      GrpcResponse.new(status: 401, call_succeeded: false)
    rescue GRPC::ResourceExhausted => e
      # ResourceExhausted indicates some resource has been exhausted, perhaps
      # a per-user quota, or perhaps the entire file system is out of space.
      Failbot.report(e)
      GrpcResponse.new(status: 429, call_succeeded: false)
    end
  end

  def self.request_authenticated_url_for(unauthenticated_url:, check_run:, resource_type:)
    check_suite = check_run.check_suite
    repository = check_suite.repository
    GrpcHelper.rescue_from_grpc_errors("Actions") do
      grpc_request = GitHub::Launch::Services::Artifactsexchange::ExchangeURLRequest.new({
        unauthenticated_url: unauthenticated_url.to_s,
        repository_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: repository.global_relay_id),
        resource_type: resource_type,
      })

      GitHub.launch_artifacts_exchange_for_check_suite(check_suite).exchange_url(grpc_request)
    end
  end
end
