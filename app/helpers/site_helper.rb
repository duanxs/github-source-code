# frozen_string_literal: true

module SiteHelper
  # Adds a 'return_to' param to the login url so that successful
  # login sends you right back to where you were.
  def site_nav_login_path(options = {})
    invalid_return_paths = ["/", "/dashboard/logged_out"]
    unless invalid_return_paths.include?(request.path)
      options[:return_to] = coordinated_return_to
    end
    login_path(options)
  end

  def coordinated_return_to
    if request.path == "/join" && params[:setup_organization]
      new_organization_path
    else
      params[:return_to] || request.fullpath
    end
  end

  def home_page?
    request.path == home_path ||
      request.path == "/home"
  end

  def append_query_parameters(base_url, params_hash)
    return base_url if params_hash.empty?

    param_delimiter = if base_url.include?("?")
      "&"
    else
      "?"
    end

    "#{base_url}#{param_delimiter}#{params_hash.to_query}"
  end

  def encode_parameters(param)
    case param
    when Array
      param.map { |element| encode_parameters(element) }
    when Hash
      Hash[param.map { |key, value| [key, encode_parameters(value)] }]
    when String
      if (param.encoding == ::Encoding::US_ASCII || param.encoding == ::Encoding::UTF_8) && param.valid_encoding?
        param
      else
        param.dup.force_encoding(::Encoding::UTF_8).scrub!
      end
    end
  end
end
