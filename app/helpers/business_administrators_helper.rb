# frozen_string_literal: true

module BusinessAdministratorsHelper
  def admin_role_for(role)
    {
      owner: "Owner",
      billing_manager: "Billing manager",
    }.fetch(role.to_sym)
  end

  def admin_role_with_indefinite_article_for(role)
    article = (role == :billing_manager) ? "a" : "an"
    "#{article} #{admin_role_for(role).downcase}"
  end
end
