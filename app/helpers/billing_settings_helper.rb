# frozen_string_literal: true

module BillingSettingsHelper
  include ExperimentHelper

  MUNICH_FEATURE_LISTS = {
    pro: [
      "Required reviewers in private repos",
      "Protected branches in private repos",
      "Repository insights in private repos",
      "Wikis in private repos",
      "Pages in private repos",
      "Code owners in private repos",
      "3,000 minutes for GitHub Actions",
      "2GB of storage for packages"
    ],
    team: [
      "Required reviewers in private repos",
      "Protected branches in private repos",
      "Multiple issue assignees in private repos",
      "Multiple PR assignees in private repos",
      "Repository insights in private repos",
      "Wikis in private repos",
      "Pages in private repos",
      "Code owners in private repos",
      "3,000 minutes for GitHub Actions",
      "2GB of storage for packages",
      "Standard Support"
    ],
    enterprise: [
      "SAML auth",
      "Enterprise support",
      "Advanced auditing",
      "99.95 uptime SLA",
      "50,000 minutes for GitHub Actions",
      "50GB of storage for packages"
    ]
  }

  def available_plans(target, actor: nil)
    plans = if target.organization?
      GitHub::Plan.org_plans
    elsif target.billable?
      GitHub::Plan.user_plans
    else
      []
    end

    upgrade_restriction = ::Billing::PlanChange::LegacyUpgradeRestriction.new(target: target, actor: (actor || target))
    plans = plans.select { |plan| upgrade_restriction.allow_plan?(plan) }

    plans += [@plan] if @plan

    # Add the user's current plan to the list of available plans (if it's a
    # hidden plan that's not already in there). The free_with_addons plan is a
    # special case — don't display that one because users believe they're on
    # the free plan.

    if !plans.include?(target.plan) && !target.plan.free_with_addons?
      plans += [target.plan]
    end

    plans.sort_by { |plan| -plan.cost }
  end

  def show_downgrade_survey?(target, plan)
    return false unless target.organization?
    return false if target.plan.cost < plan.cost
    true
  end

  def upgrade_downgrade_form_action_path(target, plan)
    if target.plan.cost < plan.cost
      target_cc_update_path(target)
    else
      target_downgrade_with_exit_survey_path(target)
    end
  end

  def target_downgrade_with_exit_survey_path(target)
    target.organization? ? org_downgrade_with_exit_survey_path : downgrade_with_exit_survey_path
  end

  def target_cycle_update_path(target)
    target.organization? ? org_cycle_update_path : cycle_update_path
  end

  def target_cc_update_path(target)
    target.organization? ? org_cc_update_path : cc_update_path
  end

  def target_update_credit_card_path(target)
    target.organization? ? org_update_credit_card_path : update_credit_card_path
  end

  def target_billing_path(target)
    if target.is_a?(Business)
      settings_billing_enterprise_path(target)
    elsif target.organization? && target.business.present?
      settings_billing_enterprise_path(target.business)
    elsif target.organization?
      settings_org_billing_path(target)
    else
      settings_user_billing_path
    end
  end

  def target_show_invoice_path(target, invoice_number)
    if target.is_a?(Business)
      show_invoice_enterprise_path(target, invoice_number)
    else
      show_invoice_path(target, invoice_number)
    end
  end

  def target_invoice_signature_path(target, invoice_number)
    if target.is_a?(Business)
      invoice_signature_enterprise_path(target, invoice_number)
    else
      invoice_signature_path(target, invoice_number)
    end
  end

  def target_payment_method_path(target, arg = {})
    if target.is_a?(Business)
      settings_billing_enterprise_path(target, arg)
    elsif target.organization? && target.business.present?
      settings_billing_enterprise_path(target.business, arg)
    elsif target.organization?
      org_payment_path(target, arg)
    else
      payment_path(arg)
    end
  end

  def target_billing_data_plan_path(target, options = {})
    target.organization? ? org_billing_data_plan_path(target, options) : billing_data_plan_path(options)
  end

  def target_billing_upgrade_data_plan_path(target, options = {})
    target.organization? ? org_billing_upgrade_data_plan_path(target, options) : billing_upgrade_data_plan_path(options)
  end

  def target_billing_downgrade_data_plan_path(target, options = {})
    target.organization? ? org_billing_downgrade_data_plan_path(target, options) : billing_downgrade_data_plan_path(options)
  end

  def missing_billing_information_for?(requested_owner)
    upgrade? && !requested_owner.has_valid_payment_method? && !has_payment_details?
  end

  # Public: Boolean if this is an organization is on per-seat pricing and has limited seats
  #
  # target - Organization
  #
  # Returns truthy if the target is an organization on per-seat pricing not on free trial.
  def on_per_seat_pricing?(target)
    target.organization? && target.plan.per_seat? && !target.has_unlimited_seats?
  end

  def payment_details
    @payment_details ||= parse_payment_details
  end

  def parse_payment_details
    return HashWithIndifferentAccess.new unless billing = params[:billing]

    hash = HashWithIndifferentAccess.new \
      actor: current_user

    hash[:billing_extra] = billing[:billing_extra] unless billing[:billing_extra].nil?
    hash[:vat_code] = billing[:vat_code] unless billing[:vat_code].nil?

    if  GitHub.flipper[:name_address_collection].enabled?(current_user)
      hash[:billing_address] = {
        country_code_alpha3: current_user.user_personal_profile&.country&.alpha3,
        region: current_user.user_personal_profile&.region,
        postal_code: current_user.user_personal_profile&.postal_code,
      }
    elsif address = billing[:billing_address]
      hash[:billing_address] = {
        country_code_alpha3: address[:country_code_alpha3],
        region: address[:region],
        postal_code: address[:postal_code],
      }
    end

    if payment_method_id = billing[:zuora_payment_method_id]
      hash[:zuora_payment_method_id] = payment_method_id
    elsif paypal_nonce = billing[:paypal_nonce]
      hash[:paypal_nonce] = paypal_nonce
    elsif credit_card = billing[:credit_card]
      hash[:credit_card] = {
        number: credit_card[:number],
        expiration_month: credit_card[:expiration_month],
        expiration_year: credit_card[:expiration_year],
        cvv: credit_card[:cvv],
      }
    else
      return HashWithIndifferentAccess.new # No valid payment method included.
    end

    hash
  end

  def payment_details_object
    @payment_details_object ||= GitHub::Billing::PaymentDetails.new(payment_details)
  end

  def has_payment_details?
    payment_details_object.valid?
  end

  def no_payment_details?
    !has_payment_details?
  end

  def payment_details_includes_paypal?
    payment_details_object.paypal_details?
  end

  def purchase_button_aria_label(target)
    if can_purchase_data_packs?(target)
      "Purchase"
    else
      "Add a payment method to purchase"
    end
  end

  def can_purchase_data_packs?(target)
    target.has_valid_payment_method? || (target.coupon&.one_hundred_percent_discount?)
  end

  def sanctioned_by_ofac_message
    return "" unless current_user.ofac_sanctioned?

    TradeControls::Notices::Plaintext.user_account_restricted
  end

  def short_plan_name(plan, account_type: nil)
    plan.display_name(account_type).titleize
  end

  def long_plan_name(plan, account_type: nil)
    if !GitHub.flipper[:munich_pricing].enabled? && short_plan_name(plan, account_type: account_type) == "Free"
      "Team for Open Source"
    else
      short_plan_name(plan, account_type: account_type)
    end
  end

  def branded_plan_name(plan, account_type: nil)
    "GitHub #{short_plan_name(plan, account_type: account_type)}"
  end

  def downgrade_survey_for(target)
    if contact_churn_survey_enabled?
      Survey.find_by_slug("per_seat_org_downgrade_text_only")
    elsif target.plan.legacy?
      Survey.find_by_slug("org_downgrade")
    else
      Survey.find_by_slug("per_seat_org_downgrade")
    end
  end

  def upgrade_to_team_as_link?(plan, source, org)
    logged_in? &&
      source == "trial_upgrade" &&
      plan == GitHub::Plan.business_plus &&
      Billing::EnterpriseCloudTrial.new(org).ever_been_in_trial?
  end

  def contact_churn_survey_enabled?
    feature_enabled_globally_or_for_user?(feature_name: :contact_churn_survey)
  end

  def downgrade_reason_survey_question(survey)
    survey.questions.visible.find_by(short_text: "downgrade_reason")
  end

  def contact_opt_in_survey_question(survey)
    survey.questions.visible.find_by(short_text: "contact_opt_in")
  end

  def yes_no_opt_in_choices(question)
    yes = question.choices.find_by(short_text: "yes")
    no = question.choices.find_by(short_text: "no")
    [yes, no]
  end

  # Public: The formatted active_on of a pending change
  #
  # target - account whose pending_cycle we're interested in
  #
  # Examples
  #
  #   pending_change_active_on(user_or_org_instance)
  #   # => "Apr 21, 2020"
  #
  # Returns a String
  def pending_change_active_on(target)
    pending_cycle(target).active_on.strftime("%b %d, %Y")
  end

  # Public: The pending cycle associated with the passed account
  #
  # target - account whose pending_cycle we're interest in
  #
  # Returns an instance of ::Billing::PendingCycle
  def pending_cycle(target)
    @_pending_cycle ||= {}
    @_pending_cycle[target.id] ||= target.pending_cycle
  end

  def downgrade_features_with_counts
    {
      protected_branches: "Protected branches in private repos",
      draft_prs: "Draft PRs in private repos",
      pages: "GitHub Pages in private repos",
      wikis: "Wikis in private repos",
    }
  end

  def can_manage_munich_seats?(target)
    feature_enabled_globally_or_for_user?(feature_name: :manage_munich_seats) &&
     target.organization? &&
     target.plan.per_seat? &&
     target.billing_manageable_by?(current_user) &&
     target.seats <= 5 # since Munich removed the 5 seat requirement for Team plans
  end
end
