# frozen_string_literal: true

module CodespacesHelper

  include HydroHelper

  def open_codespace_attributes(codespace:, target:)
    codespace_analytics \
      codespace: codespace,
      target: target,
      action: "open"
  end

  def create_codespace_attributes(codespace:, target:)
    codespace_analytics \
      codespace: codespace,
      target: target,
      action: "create"
  end

  def destroy_codespace_attributes(codespace:)
    codespace_analytics \
      codespace: codespace,
      target: :CODESPACES_PAGE,
      action: "destroy"
  end

  def is_firefox?
    Browser::Firefox.new(request.env["HTTP_USER_AGENT"]).match?
  end

  def should_disable_firefox?
    is_firefox? && !current_user.deiframed_flow_enabled?
  end

  private

  def codespace_analytics(codespace:, target:, action:)
    payload = {
      ref: codespace.ref,
      repository_id: codespace.repository_id,
      pull_request_id: codespace.pull_request_id,
      target: target,
      user_id: current_user.id,
      codespace_id: codespace.id
    }
    hydro_click_tracking_attributes("codespace_#{action}.click", payload)
  end
end
