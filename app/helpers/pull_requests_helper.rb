# frozen_string_literal: true

module PullRequestsHelper
  include PlatformHelper

  # Arbitrary hard limit on the potential number of destination repositories for
  # a pull request. This is a temporary fix to prevent slow page loads and
  # unicorns when the list of available forks is massive.
  MAX_DESTINATION_REPOS = 500

  PageInfoFragment = parse_query <<-'GRAPHQL'
    fragment PullRequest on PullRequest {
      id
      title
      number
      bodyText
      url

      author {
        login
        avatarUrl(size: 400)
      }
      repository {
        owner { login }
        nameWithOwner
        name
        openGraphImageUrl
        ...RepositoriesHelper::RepositoryTwitterImageFragment::Repository
      }
    }
  GRAPHQL

  def pull_request_page_info(pull)
    pull = PageInfoFragment::PullRequest.new(pull)

    attribution = pull.author ? " by #{pull.author.login}" : ""
    title = "#{pull.title}#{attribution} · Pull Request ##{pull.number} · #{pull.repository.name_with_owner}"

    page_info(
      title:         title,
      selected_link: :repo_pulls,
      stafftools:    stafftools_repository_pull_request_path(
                       pull.repository.owner.login, pull.repository.name, pull.number),
      description:   pull.body_text,
      container_xl: true,
      responsive: responsive_pull_requests_enabled?,
      richweb: {
        title:       title,
        url:         pull.url.to_s,
        description: truncate(pull.body_text, length: 240),
        image:       pull.repository.open_graph_image_url,
        card:        graphql_repository_twitter_image_card(pull.repository),
      },
      dashboard_pinnable_item_id: pull.id,
    )
  end

  ##
  # Review Comments

  # The short path description of a PullRequestReviewComment. This does not
  # include the line number and commit SHA1 like comment_comment_shortline but
  # links to the pull request comment.
  #
  # comment - The PullRequestReviewComment being referenced.
  #
  # Returns a string with a link to the comment on the pull request diff page.
  def review_comment_shortline(comment)
    pull_url    = pull_request_path(comment.pull_request)
    comment_url = pull_url + "/files#r#{comment.id}"

    link_to(content_tag(:code, comment.path), comment_url)
  end

  ##
  # Submit Pull Request Form Helpers

  def new_pull_request_head_ref
    if @comparison
      @comparison.head
    else
      tree_name
    end
  end

  def new_pull_request_base_ref
    if @comparison
      @comparison.qualified_base_ref
    end
  end

  def new_pull_request_range(options = {})
    options[:base_ref] ||= new_pull_request_base_ref
    options[:head_ref] ||= new_pull_request_head_ref

    [options[:base_ref], options[:head_ref]].compact.join("...")
  end

  def show_in_progress_reviewer_message?(pull)
    return false unless logged_in?
    pull.latest_non_pending_review_for(current_user).present? || pull.review_requested_for?(current_user)
  end

  # Build a ranked list of repositories that are potential destinations for the
  # current comparison's pull request. The list is orded by the likeliness that
  # the destination repository is the correct place to send the pull request.
  #
  # The list includes the following repositories when applicable in the order
  # specified:
  #
  #   1. The base repository of the comparison.
  #   2. Each parent of the current repository up to the source repository.
  #   3. The current repository.
  #   4. All remaining repositories in the network sorted by name.
  #
  # Care is taken to ensure that the same repository does not appear more than
  # once in the list and that the current user has access to all repositories.
  def pull_request_destination_repositories
    repos = []

    repos << @comparison.base_repo if @comparison.cross_repository?

    parent = current_repository.parent
    while parent
      if !repos.include?(parent) && parent.visibility == current_repository.visibility || current_repository.internal_fork?
        repos << parent
      end
      parent = parent.parent
    end

    repos << current_repository

    if logged_in?
      repo_ids = current_user.associated_repository_ids(repository_ids: Repository.in_same_network_as(current_repository).ids)
      repos.concat Repository.where(id: repo_ids.first(MAX_DESTINATION_REPOS - repos.size)).with_owner.preload(:owner)
    end
    repos.compact!

    repo = current_repository
    query = Repository.github_sql.new(<<-SQL, repository_id: repo.source_id, visibility: repo.public)
      SELECT id FROM repositories
      WHERE source_id = :repository_id AND public = :visibility AND active = 1 AND deleted = 0
      ORDER BY watcher_count+0 DESC
      LIMIT 200
    SQL

    network_repo_ids = query.values

    if current_repository.private?
      associated_network_repository_ids = current_user.associated_repository_ids(repository_ids: network_repo_ids)
      network_repo_ids &= (associated_network_repository_ids + current_user.internal_repo_ids)
    end

    if network_repo_ids.any? && repos.size < MAX_DESTINATION_REPOS
      network_repos = Repository.with_owner.preload(:owner).where(id: network_repo_ids.first(MAX_DESTINATION_REPOS - repos.size))
      repos.concat network_repos.sort_by(&:name_with_owner)
    end

    repos = repos.uniq.reject { |repo| repo.owner.nil? }
    repos.first(MAX_DESTINATION_REPOS)
  end

  def available_repositories_for(comparison, type)
    if comparison.repo.advisory_workspace?
      type == :base ? [comparison.base_repo] : [comparison.head_repo]
    else
      pull_request_destination_repositories
    end
  end

  def merge_status_summary(context_states)
    downcased_states = context_states.map { |state| state && state.downcase }
    if downcased_states.length == 1
      if StatusCheckRollup::SUCCESS_STATES.include?(downcased_states.first)
        "1 check passed"
      elsif StatusCheckRollup::PENDING_STATES.include?(downcased_states.first)
        "1 check was pending"
      else
        "1 check failed"
      end
    elsif downcased_states.all? { |state| StatusCheckRollup::SUCCESS_STATES.include?(state) }
      "#{downcased_states.count} checks passed"
    else
      successful_count = downcased_states.count { |state| StatusCheckRollup::SUCCESS_STATES.include?(state) }
      "#{successful_count} of #{downcased_states.count} checks passed"
    end
  end

  def branch_label(repository, branch, prepend_login: false, prepend_repo_name: false, expandable: false, extra_classes: "", extra_link_classes: "", link: false, copy_button: false)
    options = { class: "css-truncate-target" }

    content = if repository
      title = "#{repository.owner}/#{repository.name}:#{branch}"
      ref = content_tag(:span, branch, options)
      label = ref
      clipboard_contents = branch

      if prepend_repo_name
        repo = content_tag(:span, repository.name, options)
        clipboard_contents = [repository.name, clipboard_contents].join(":")
        label = safe_join([repo, ref], ":")
      end

      if prepend_login
        options[:class] += " user" unless link
        user = content_tag(:span, repository.owner, options)
        if prepend_repo_name
          clipboard_contents = [repository.owner, clipboard_contents].join("/")
          label = safe_join([user, label], "/")
        else
          clipboard_contents = [repository.owner, branch].join(":")
          label = safe_join([user, ref], ":")
        end
      end

      label
    else
      text = "unknown repository"
      options[:class] = "unknown-repo"
      content_tag(:span, text, options)
    end

    if expandable
      if link
        content_tag(:span, title: title.try { |t| t.dup.force_encoding("utf-8").scrub! }, class: "commit-ref css-truncate user-select-contain expandable #{extra_classes}") do
          link_to(
            content.try { |c| c.force_encoding("utf-8").scrub! },
            tree_path("", branch, repository),
            title: title.try { |t| t.dup.force_encoding("utf-8").scrub! },
            class: "no-underline #{extra_link_classes}",
          )
        end +
        content_tag(:span) do
          if copy_button
            content_tag("clipboard-copy", "class" => "js-clipboard-copy zeroclipboard-link text-gray link-hover-blue", "value" => clipboard_contents.try { |c| c.force_encoding("utf-8").scrub! }, "aria-label" => "Copy", "data-copy-feedback" => "Copied!") do
              octicon("clippy", class: "d-inline-block mx-1 js-clipboard-clippy-icon") +
              octicon("check", class: "js-clipboard-check-icon mx-1 d-inline-block d-none text-green")
            end
          end
        end
      else
        content_tag(:span,
                    content.try { |c| c.force_encoding("utf-8").scrub! },
                    title: title.try { |t| t.dup.force_encoding("utf-8").scrub! },
                    class: "commit-ref css-truncate user-select-contain expandable #{extra_classes}")
      end
    else
      content_tag(:span,
                  content.try { |c| c.force_encoding("utf-8").scrub! },
                  title: title.try { |t| t.dup.force_encoding("utf-8").scrub! },
                  class: extra_classes,
                  )
    end
  end

  def pull_branch_label(pull, base_or_head, expandable: false, extra_classes: "", link: false, copy_button: false)
    unless [:base, :head].include?(base_or_head)
      raise ArgumentError, "base_or_head must be either :base or :head"
    end

    repository    = pull.send("#{base_or_head}_repository")
    branch        = pull.send("#{base_or_head}_ref_name")
    prepend_login = pull.cross_repo?

    classes = "#{extra_classes} #{base_or_head == :base ? "base-ref" : "head-ref"}".squish
    branch_label(repository, branch, prepend_login: prepend_login, prepend_repo_name: pull.in_advisory_workspace?, expandable: expandable, extra_classes: classes, link: link, copy_button: copy_button)
  end

  def ci_url
    apps_feature_path(feature: "continuous-integration")
  end

  def pull_request_commit_path(pull, commit)
    unless pull.is_a?(PullRequest)
      raise TypeError, "expected pull to be a PullRequest, but was #{pull.class}"
    end

    unless commit.is_a?(Commit)
      raise TypeError, "expected commit to be a Commit, but was #{commit.class}"
    end

    base_path = pull_request_path(pull)
    "#{base_path}/commits/#{commit.oid}"
  end

  def pull_request_revision_path(pull, revision)
    unless pull.is_a?(PullRequest)
      raise TypeError, "expected pull to be a PullRequest, but was #{pull.class}"
    end

    unless revision.is_a?(PullRequestRevision)
      raise TypeError, "expected revision to be a PullRequestRevision, but was #{commit.class}"
    end

    base_path = pull_request_path(pull)
    range_oids = [revision.head_oid]
    unless revision.base_oid == pull.base_sha
      range_oids.unshift(revision.base_oid)
    end
    "#{base_path}/files/#{range_oids.join("..")}"
  end

  # Public: Generate url for PR history state.
  #
  # pull_comparison - A PullRequest::Comparison.
  # anchor - Optional String URL anchor (default: nil)
  #
  # Returns String URL path.
  def pull_request_comparison_path(pull_comparison, anchor: nil)
    unless pull_comparison.is_a?(PullRequest::Comparison)
      raise TypeError, "expected pull_comparison to be a PullRequest::Comparison, but was #{pull_comparison.class}"
    end

    base_path = pull_request_path(pull_comparison.pull)

    path = if pull_comparison.range?
      "#{base_path}/files/#{pull_comparison.start_commit.oid}..#{pull_comparison.end_commit.oid}"
    else
      "#{base_path}/files/#{pull_comparison.end_commit.oid}"
    end

    path += "##{anchor}" if anchor
    path
  end

  def pull_request_review_state_classes(review)
    if logged_in? && review
      "is-review-pending" if review.pending?
    end
  end

  def review_timeline_item_classes(review)
    return unless review

    classes = []
    classes << "is-approved" if review.approved?
    classes << "is-rejected" if review.changes_requested?
    classes << "is-pending" if review.pending?
    classes << "is-writer" if review.writer?

    classes.join(" ")
  end

  def review_comments_count(review)
    return 0 unless review
    review.review_comments.with_pending_state.size
  end

  REVIEW_STATE_ICONS = {
    changes_requested: "request-changes",
  }.with_indifferent_access

  def review_state_octicon_name(state)
    REVIEW_STATE_ICONS.fetch(state)
  end

  def review_state_icon(state, options = {})
    octicon(review_state_octicon_name(state), options)
  end

  def show_pull_request_reviews_hint?(pull_request)
    return false unless pull_request.protected_base_branch&.pull_request_reviews_enabled?

    if pull_request.persisted?
      return false unless pull_request.open?

      review_decision = pull_request.cached_merge_state(viewer: current_user).pull_request_review_policy_decision
      return !review_decision.policy_fulfilled?
    end

    true
  end

  def changes_requested_on_pull_request?(pull_request)
    return unless pull_request.protected_base_branch&.pull_request_reviews_enabled?
    return unless pull_request.persisted?

    review_decision = pull_request.cached_merge_state(viewer: current_user).pull_request_review_policy_decision
    review_decision.changes_requested?
  end

  def show_open_with_codespaces_tab?
    current_repository && Codespaces::Policy.should_see_codespaces?(current_repository, current_user)
  end
end
