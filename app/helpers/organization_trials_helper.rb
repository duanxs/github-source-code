# frozen_string_literal: true

module OrganizationTrialsHelper
  module Banners
    EXPIRED = :expired_banner
    COUNTDOWN = :countdown_banner
    WARNING = :warning_banner
  end

  def should_show_enterprise_trial_banner?(organization)
    return false unless organization&.organization?
    return false unless logged_in?

    cloud_trial = Billing::EnterpriseCloudTrial.new(organization)
    return false unless cloud_trial.ever_been_in_trial?
    return false if cloud_trial.expired? && !organization.plan.free? && !organization.plan.free_with_addons?

    organization.adminable_by?(current_user) || organization.billing_manager?(current_user)
  end

  def which_trial_banner(organization)
    return unless should_show_enterprise_trial_banner?(organization)

    key = "billing-#{GitHub::Plan::BUSINESS_PLUS}-trial-show-which-#{organization.id}-#{current_user.id}-#{GitHub::Billing.today}"
    trial =  Billing::EnterpriseCloudTrial.new(organization)

    GitHub.cache.fetch(key) do
      if organization.plan.free? && trial.expired_within?(Billing::EnterpriseCloudTrial::EXPIRATION_MESSAGE_DURATION)
        Banners::EXPIRED
      elsif trial.days_remaining > 3
        Banners::COUNTDOWN
      elsif trial.active?
        Banners::WARNING
      end
    end
  end
end
