# frozen_string_literal: true

module DiffViewHelper
  # Determine the current diff view for the current request.
  #
  # Returns :unified or :split Symbol.
  def diff_view
    if params[:diff] == "split"
      :split
    elsif params[:diff] == "unified"
      :unified
    elsif logged_in? && current_user.split_diff_preferred
      :split
    else
      :unified
    end
  end

  # Detect initial body class for a page that will render a diff.
  #
  # As an optimization, we want the body.split-diff to be set on the initial
  # render if a split diff is going to be shown. Otherwise, the browser needs to
  # wait until JS kicks in and adds the body class. This causes a jarring
  # relayout a few seconds after the initial paint.
  #
  # The downside to this logic is that we have to duplicate when the body class
  # should be added in Ruby and in JS.
  #
  # See pages/diffs/split.js for the other half.
  #
  # Returns "full-width" String or nil.
  def diff_body_class
    if split_diff?
      if !defined?(tab_specified?) || tab_specified?("files")
        "full-width"
      end
    end
  end

  # Generate a URL to switch page to other diff view.
  #
  # view - :unified or :split Symbol
  #
  # Returns String path.
  def toggle_diff_view_path(view)
    raise ArgumentError, "unknown view type: #{view.inspect}" unless [:unified, :split].include?(view)

    options = {}
    options[:diff] = view.to_s
    options[:tab]  = params[:tab] if params[:controller] == "pull_requests" && params[:action] == "show"
    url_with(options)
  end

  # Deprecated: Determine if current diff view is "Split".
  #
  # Prefer checking diff_view directly.
  #
  # Returns true or false.
  def split_diff?
    diff_view == :split
  end

  # Set user's diff preference to the current diff view.
  #
  # The last view the user accesses will be the sticky setting.
  #
  # Exposed for controller filters and actions to touch.
  #
  # Returns nothing.
  def set_current_user_diff_preference
    return unless logged_in?
    current_user.set_diff_preference(diff_view)
    nil
  end
end
