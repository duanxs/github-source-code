# frozen_string_literal: true

module StafftoolsHelper
  def progress_bar(percent, options = {})
    precision = options.fetch(:precision, 0)
    percentage_text = options[:percentage] ?
      content_tag(:span, number_to_percentage(percent, precision: precision), class: "percent") :
      nil

    content_tag(:span,
      safe_join([content_tag(:span,
        GitHub::HTMLSafeString::NBSP,
        class: "progress",
        style: "width: #{percent}%",
      ),
      percentage_text]),
      class: "progress-bar",
    )
  end

  def stafftools_user_audit_log_query(user)
    keys = %w{actor_id user_id}
    keys << "org_id" if user.organization?
    query = keys.reverse.map { |k| "#{k}:#{user.id}" }.join " OR "
    "(#{query})"
  end

  def stafftools_billing_audit_log_path(user)
    actions = %w(
      braintree.*
      billing.*
      account.*
      payment_method.*
      pending_plan_change.*
      pending_subscription_change.*
      plan_subscription.*
      metered_billing_configuration.*
      staff.chargeback_disable
    )
    query = stafftools_user_audit_log_query(user).dup
    query << " AND action:(#{actions.join(" OR ")})"
    stafftools_audit_log_path(query: query)
  end

  # Represent an Array of organization/team members in a standard copyable
  # text format that is suitable for being used from stafftools via a copy
  # button or something similar.
  #
  # members - The Array of organization or team members to be represented in the
  #           standard copyable format.
  #
  # Example
  #   members_as_copyable_text([izuzak, jdennes])
  #   # => "@izuzak - Ivan Žužak\n@jdennes - James Dennes\n"
  #
  # Returns a String that contains copyable text that represents the
  # organization/team members where each line uses the format:
  #   @<login> - <profile name>
  def members_as_copyable_text(members)
    members.each.map do |member|
      entry = "@#{member.login}".dup
      entry << " - #{member.safe_profile_name}" unless member.profile_name.blank?
      entry << "\n"
    end.join("")
  end

  # Helper methods for SSH key activity.

  def last_accessed_after_cutoff?(key)
    cutoff = key.class::ACCESS_CUTOFF_DATE

    if key.created_at && key.created_at > cutoff
      return true
    else
      return false
    end
  end

  def access_cutoff_date(key)
    return key.class::ACCESS_CUTOFF_DATE.strftime("%B %d, %Y")
  end

  # Public: Auto-link any links in the spammy reason
  #
  # Returns "No reason given" if given a blank reason.
  # Else, returns the autolinked reason
  def autolink_spammy_reason(reason)
    return "No reason given" if reason.blank?
    auto_link(reason, mode=:urls, link_attr=nil, skip_tags=nil)
  end

  # Get the "clear cache" URL for a given URL, for use from site admin pages, by
  # adding skipmc=1 and _timeout=clear to the query string params of the URL.
  #
  # url - String representing the URL for which a "clear cache" URL should be
  #       generated.
  #
  # Example
  #   stafftools_clear_cache_url("https://gh.ent/trending?since=daily")
  #   # => "https://gh.ent/trending?_timeout=clear&since=daily&skipmc=1"
  #
  # Returns a String that is the "clear cache" URL for the given URL.
  def stafftools_clear_cache_url(url)
    parsed_url = Addressable::URI.parse(url)
    parsed_url.query_values = (parsed_url.query_values || {}).merge({
      skipmc:   "1",
      _timeout: "clear",
    })
    parsed_url.normalize.to_s
  end


  # used for /vulnerabilities
  def link_to_sort_query(params, field, default = false)
    chevron = ""
    direction = ""
    style = "color: black;"

    default_sorted_field = params[:order_field].nil? && default

    # if we're sorting on the current column, display
    # the appropriate chevron, and have direction set to toggle

    if params[:order_field] == field || default_sorted_field
      # if order_dir is unset, we're in a default field
      # which we will assume is DESC
      if params[:order_direction] == "DESC" || params[:order_direction].nil?
        chevron = "chevron-down"
        direction = "ASC"
      else
        chevron = "chevron-up"
        direction = "DESC"
      end
    else
      # we're not yet sorting on this column; default to DESC
      chevron = "code"
      direction = "DESC"
      style = "transform: rotate(90deg); color: grey;"
    end

    link_to(octicon(chevron, style: style), params.merge(order_field: field, order_direction: direction), alt: "Sort")
  end

  STAFFTOOLS_NOT_AUTHORIZED_HTML = GitHub::HTMLSafeString.make %Q[You're not authorized to perform this action in stafftools. To request access, please <a href="https://githubber.com/article/technology/dotcom/stafftools/stafftools-entitlements.md">follow the GitHubber article</a>.]
  def stafftools_not_authorized_html
    STAFFTOOLS_NOT_AUTHORIZED_HTML
  end

  def stafftools_not_authorized_text
    "You're not authorized to use this feature. To request access, open an issue in the github/security-iam repo."
  end

  # parse controller and action from a path
  def parse_action(path)
    accessing = Rails.application.routes.recognize_path(path)
    # Buttons can return  stafftools/site redirect as a controller action.
    # if this happens, extract the controller / action from the path attribute
    # in the 'accessing' hash
    if accessing[:action] == "redirect"
      ctrl, action = accessing[:path].split("/")
      accessing[:controller] = "stafftools/#{ctrl}"
      accessing[:action] = action
    end
    # convert controller to camelcase from snake case
    # EX: accessing[:controller] = "stafftools/user_assets" => "Stafftools::UserAssetsController"
    controller = accessing[:controller].camelize + "Controller"
    { controller: controller, action: accessing[:action] }
  end

  # These methods are wrappers around the linked_to and button_to methods
  # to conditionally check role based access to a feature and obfuscate
  # or disable that content if permissions are not met
  def stafftools_selected_link_to(*args, &block)
    action = parse_action(args[1])
    if stafftools_action_authorized?(action)
      selected_link_to(*args, &block)
    end
  end

  def stafftools_button_to(*args, &block)
    action = parse_action(args[1])
    if stafftools_action_authorized?(action)
      button_to(*args, &block)
    else
      # Unfortunately this completely disables the button, making the aria useless.
      # We could potentially return a button wrapped in a div which holds the tooltip otherwise
      # we could just remove the aria-label and tooltip classes
      args[2] = args[2].merge({ class: " disabled tooltipped tooltipped-nw", disabled: true, "aria-label": stafftools_not_authorized_text }) { |key, existing, addon| existing + addon }
      button_to(*args, &block)
    end
  end

  # Public: Generates url for repository based on repository id
  #
  # Returns repository URL.
  def link_to_nwo_from_repository_id(id)
    nwo, repo = nil, nil
    ActiveRecord::Base.connected_to(role: :reading) do
      repo = Repository.find(id)
      nwo = repo.name_with_owner
    end

    link_to nwo, repo.permalink, target: :blank
  end

  # Public: Generates a url to search splunk
  #
  # Returns splunk url, or nil for enterprise installations
  def splunk_search_url(search_string)
    return nil if GitHub.enterprise?

    base = "https://splunk.githubapp.com/en-US/app/gh_reference_app/search"
    params = { q: "search #{search_string}" }
    "#{base}?#{params.to_query}"
  end

  # Public: Returns an octicon relating to the vulnerability source
  #
  # Returns an octicon tag, or empty string
  def vulnerability_source_octicon(source)
    case source
    when RepositoryAdvisory::SOURCE_IDENTITY
      octicon("repo")
    when Stafftools::PendingVulnerabilitiesController::SOURCE_IDENTITY
      octicon("person")
    when AdvisoryDB::MungerDetection::SOURCE_IDENTITY
      octicon("issue-reopened")
    else
      ""
    end
  end

  # Public: Returns any extra information about an account's plan around
  # trials or upcoming changes
  #
  # Returns a string (possibly empty)
  def account_plan_extra_info(account)
    cloud_trial = Billing::EnterpriseCloudTrial.new(account)

    if cloud_trial.active?
      "(trial expiring #{cloud_trial.expires_on.strftime("%b %-d %Y")})"
    elsif account.plan.name == GitHub::Plan::FREE && cloud_trial.ever_been_in_trial? && cloud_trial.expired?
      expired_on_copy = cloud_trial.expires_on.present? ? " that expired on #{cloud_trial.expires_on.strftime("%b %-d %Y")}" : ""
      "(was on Enterprise Cloud Trial#{expired_on_copy})"
    elsif account.pending_cycle.has_changes?(including_free_trials: false)
      "(change pending)"
    else
      ""
    end
  end

  # Public: Returns the enterprise-web admin URL for a given enterprise-web business
  #
  # Returns a string
  def enterprise_web_business_url(enterprise_web_business_id)
    "#{GitHub.enterprise_web_admin_url}/staff/businesses/#{enterprise_web_business_id}"
  end

  # Public: Returns the Zuora account url for a given business
  #
  # Returns a string
  def zuora_account_url_for_business(business)
    "#{GitHub.zuora_host}/apps/CustomerAccount.do?method=view&id=#{business.customer.zuora_account_id}"
  end

  # Public: handles the possibility of negative file sizes that number_to_human size doesn't format well
  #
  # Returns a string
  def formatted_usage_breakdown(usage_data)
    human_readable = number_to_human_size(usage_data.abs)
    usage_data.negative? ? "-#{human_readable}" : human_readable
  end

  # Public: Checks for mismatch between billed aggregation and the summed events
  #
  # Returns a string
  def mismatched_aggregation_and_breakdown?(usage)
    usage.total_usage_in_bytes != (usage.aggregated_gpr_usage + usage.aggregated_actions_usage)
  end

  def visibility_octocon(visibility)
    symbol = visibility == "public" ? "repo" : "lock"
    octicon(symbol)
  end
end
