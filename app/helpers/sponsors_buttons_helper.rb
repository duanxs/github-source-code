# frozen_string_literal: true

module SponsorsButtonsHelper
  include HydroHelper

  def sponsors_button_hydro_attributes(button, graphql_user_login)
    hydro_click_tracking_attributes(
      "sponsors.button_click",
      button: button,
      sponsored_developer_login: graphql_user_login,
    )
  end
end
