# frozen_string_literal: true

module BranchesHelper
  def link_to_branch_compare(repo, branch_name, options = {})
    label = options.delete(:label) || branch_name.dup.force_encoding("UTF-8").scrub!
    dest  = compare_path repo, branch_name
    dest  = repository_path repo if branch_name == repo.default_branch

    link_to label, dest, options
  end
end
