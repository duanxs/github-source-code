# frozen_string_literal: true

module Repos::AdvisoriesHelper
  def repository_advisory_page_title(advisory)
    # application_helper.rb#page_title, which receives this string, modifies
    # its input - so we have to ensure this string is not frozen.
    +"#{advisory.title} · Advisory · #{current_repository.name_with_owner}"
  end
end
