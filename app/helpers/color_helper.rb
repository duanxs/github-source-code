# frozen_string_literal: true

module ColorHelper
  def icon_text_class(color)
    calculator = ColorCalculator.new(color)
    if calculator.brightness < 0.45
      "text-gray-dark"
    else
      "text-white"
    end
  end

  # Public: Figures out an appropriate text color given a base color background. Ensures that you
  # don't get black text on a dark red background, etc.
  #
  # Returns a String of a hex color.
  def text_color(color)
    ColorCalculator.new(color).text_color
  end
end
