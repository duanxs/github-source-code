# frozen_string_literal: true

module OauthHelper
  include OcticonsCachingHelper

  # Public: Determine if a field for setting the given OAuth app's bgcolor should be shown in the
  # page.
  #
  # Returns a Boolean.
  def allow_editing_oauth_bgcolor?(application)
    return false if application.new_record?

    if GitHub.enterprise?
      application.primary_avatar.present?
    else
      return false unless application.primary_avatar

      listing = application.marketplace_listing
      listing.nil? || !listing.publicly_listed?
    end
  end

  def oauth_app_name_options(application)
    options = {
      class: "wide",
      group_class: "mt-0",
      hint: "Something users will recognize and trust.",
      required: true,
      error: error_for(application, :name),
    }
    options[:autofocus] = "autofocus" if application.new_record?
    options
  end

  # Public: Image tag for the application's logo. This can be a custom
  # image, but falls back to the application owner's gravatar.
  #
  # application - An OauthApplication (optional, defaults to @application)
  # size        - Integer size of the image (optional, default: 80).
  # options     - A Hash of other options, including:
  #     :height - Height for image
  #     :width  - Width for image
  #     :alt    - Alt attribute for image
  #     :force_app_logo - Set to true to ensure the Marketplace logo, if a Marketplace listing
  #                       exists for the app, is NOT shown
  #
  # Returns an img tag.
  def oauth_application_logo(application = nil, size = 80, options = {})
    application ||= @application
    installation_view = InstallationView.new(integratable: application, session: session,
                                             current_user: current_user)
    listing = application.marketplace_listing
    show_marketplace_logo = unless options[:force_app_logo]
      listing.try(:publicly_listed?) ||
        installation_view.current_user_has_active_subscription_for_marketplace_listing?
    end

    options[:src] = if show_marketplace_logo
      listing.primary_avatar_url(size * 2)
    else
      oauth_application_logo_url(application, size * 2)
    end

    options[:height] ||= size
    options[:width] ||= size
    options[:alt] ||= ""

    options.delete(:force_app_logo)
    tag(:img, options)
  end

  # Public: Get the image url for the application's logo.
  #
  # application - An OauthApplication (optional, defaults to @application)
  # size        - Integer size of the image (optional, default: 80).
  #
  # Returns an img url
  def oauth_application_logo_url(application = nil, size = 80)
    application ||= @application
    application.preferred_avatar_url(size: size)
  end

  def self.has_logo?(application)
    (application.primary_avatar || application.logo).present?
  end

  # Displays a series of spans with tool tips for the Oauth scopes.
  def scopes_description_tooltip_list(scopes)
    scope_and_description =
      if scopes.blank?
        [["public access", "Read your public profile details"]]
      else
        scopes.sort.map { |scope_name|
          scope = Api::AccessControl.scopes[scope_name]
          description = scope ? scope.description.capitalize : "Unknown scope"
          [scope_name, description]
        }
      end

    scope_spans = scope_and_description.map { |title, description|
      content_tag(:span, title,
        title: description)
    }

    content_tag(:em, h("— ") + safe_join(scope_spans, ", "))
  end

  # Displays an unordered list of human OAuth scope descriptions.
  def scopes_description_list(scopes, css_class = "features-list")
    check = octicon("check", class: "text-green mr-1")
    inner = safe_join(
      descriptions_for_scopes(scopes).map do |description|
        content_tag(:div, safe_join([check, description], " "), class: "pl-0 listgroup-item")
      end,
      "\n",
    )

    content_tag(:div, inner, class: css_class)
  end

  # Public: Builds a simple english sentence for describing this set of scopes.
  #
  # scopes - The set of scopes to return english descriptions for.
  #
  # Returns an Array of String sentence fragments.
  def descriptions_for_scopes(scopes)
    if scopes.blank?
      ["Access public information (read-only)"]
    else
      scopes.sort.each.map do |s|
        next unless scope = Api::AccessControl.scopes[s]
        scope.description.capitalize
      end.compact
    end
  end

  # Determine whether the given scopes would allow privileged access to org
  # (i.e., do they provide at least viewing private organization-owned resources
  # or mutating public organization-owned resources?).
  #
  # scopes - Array of String scope names.
  #
  # Returns a Boolean.
  def scopes_include_privileged_org_access?(scopes)
    return false if scopes.blank?

    (scopes - Api::AccessControl::SCOPES_WITHOUT_PRIVILEGED_ORGANIZATION_ACCESS).any?
  end

  # Determine whether to prompt the user to request organization approval for
  # an application.
  #
  # user        - A User.
  # application - An OauthApplication.
  # scopes      - An Array of String scope names defining the application's
  #               level of access for the user.
  #
  # Returns a Boolean.
  def request_organization_approval?(user:, application:, scopes:)
    return false unless GitHub.oauth_application_policies_enabled?

    user.authorizable_organizations.any? &&
      scopes_include_privileged_org_access?(scopes) &&
      !application.organization_policy_exempt?
  end

  def last_authorization_access_description(authorization)
    cutoff = OauthAuthorization::ACCESS_CUTOFF_DATE
    key = if authorization.accessed_at ||
             (authorization.created_at && authorization.created_at > cutoff)
      authorization
    else
      # For old data we have no other option but to look through the collection
      # of accesses and public keys that the authorization is tracking and try
      # to derive a best guess at the last access. This is not 100% accurate
      # since a given access or public key that was recently used may have
      # been deleted, and hence we would not know about it.
      most_recently_created_access =
        authorization.accesses.max_by { |x| x.created_at.to_i }
      most_recently_accessed_access =
        authorization.accesses.max_by { |x| x.accessed_at.to_i }

      most_recently_created_public_key =
        authorization.public_keys.max_by { |x| x.created_at.to_i }
      most_recently_accessed_public_key =
        authorization.public_keys.max_by { |x| x.accessed_at.to_i }

      most_recently_accessed = [
        most_recently_accessed_access,
        most_recently_accessed_public_key,
      ].compact.max_by { |x| x.accessed_at.to_i }

      most_recently_created = [
        most_recently_created_access,
        most_recently_created_public_key,
      ].compact.max_by { |x| x.created_at.to_i }

      if most_recently_accessed && most_recently_accessed.accessed_at
        most_recently_accessed
      else
        most_recently_created
      end
    end

    last_access_description(key, "Oauth authorization")
  end

  # Public: Return the correct path to submit the logo request to
  #
  # app - an OauthApplication obj
  #
  # Returns an string of the relative path
  def destroy_logo_path(app)
    if app.primary_avatar
      settings_user_avatar_path(app.primary_avatar.avatar_id)
    else
      app_settings_path(app)
    end
  end

  # Private: Return the correct settings path for orgs or users
  #
  # app - an OauthApplication obj
  #
  # Returns an string
  def app_settings_path(app)
    if app.user.organization?
      settings_org_application_path(app.user, app)
    else
      settings_user_application_path(app)
    end
  end

  # Public: Return the correct request method to use to submit the destroy
  # logo request
  #
  # app - an OauthApplication obj
  #
  # Returns an string (e.g. put, post)
  def destroy_logo_method(app)
    return "post" if app.primary_avatar
    "put"
  end

  # Public: Return the field name to send for the destroy logo request
  #
  # app - an OauthApplication obj
  #
  # Returns an string of the name of the field
  def destroy_logo_field_name(app)
    return "op" if app.primary_avatar
    "oauth_application[logo_id]"
  end

  # Public: Return the field value to send for the destroy logo request
  #
  # app - an OauthApplication obj
  #
  # Returns an string of the name of the field
  def destroy_logo_field_value(app)
    "destroy" if app.primary_avatar
  end
end
