# frozen_string_literal: true

module ScanningHelper
  def split_file_path_and_name(filepath)
    return ["", ""] if filepath.blank?

    path = File.dirname(filepath)
    path = "" if path == "."
    path += "/" if !path.blank?
    return path, File.basename(filepath)
  end
end
