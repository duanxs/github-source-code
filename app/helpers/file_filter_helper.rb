# frozen_string_literal: true

module FileFilterHelper
  include DiffHelper

  FILE_TYPE_FILTER_PARAM = "file-filters".freeze
  HIDE_DELETED_FILES_FILTER_PARAM = "hide-deleted-files".freeze
  CODEOWNER_PARAM = "owned-by".freeze
  EMPTY_TYPE_SELECTION = [""].freeze
  PARAM_TRUE_VALUE = "true".freeze
  EMPTY_ARRAY = [].freeze

  # File filter param is present but no types are selected
  def all_file_types_unselected?
    params[FILE_TYPE_FILTER_PARAM] == EMPTY_TYPE_SELECTION
  end

  def hide_deleted_files_active?
    params[HIDE_DELETED_FILES_FILTER_PARAM] == PARAM_TRUE_VALUE
  end

  def hide_viewed_files_active?
    return true if !hide_deleted_files_active? && !params[FILE_TYPE_FILTER_PARAM] && !params[CODEOWNER_PARAM]
  end

  # Returns any valid selected file types from the URL params
  #   valid_file_types - Array of file types to allow filtering against
  #
  # Returns Array of Strings or Empty Array
  def sanitized_selected_file_types(valid_file_types:)
    return EMPTY_ARRAY unless params[FILE_TYPE_FILTER_PARAM].instance_of?(Array)
    valid_file_types & params[FILE_TYPE_FILTER_PARAM]
  end

  # Determine if given file type is currently selected via URL params
  #   file_type - String
  #   valid_file_types - Array of file types to allow filtering against
  #
  # Returns boolean
  def is_selected_file_type?(file_type:, valid_file_types:)
    return true if !params[FILE_TYPE_FILTER_PARAM]
    return true if valid_file_types.blank?
    return false if all_file_types_unselected?

    sanitized_selected_types = sanitized_selected_file_types(valid_file_types: valid_file_types)
    sanitized_selected_types.present? ?
      sanitized_selected_types.include?(file_type) : true
  end

  # Determine if diff is currently selected via URL params
  #   path - String path of the diff file
  #   deleted - Boolean indicating whether the diff is a deletion.
  #   valid_file_types - Array of file types to allow filtering against
  #
  # Returns boolean indicating if the diff should be filtered
  def file_filtered?(path:, deleted:, valid_file_types:, codeowners: false, viewed: false)
    if !params[FILE_TYPE_FILTER_PARAM] && !hide_deleted_files_active? && !hide_viewed_files_active? && !codeowners
      return false
    end

    if codeowners
      true
    elsif params[FILE_TYPE_FILTER_PARAM]
      return true if all_file_types_unselected?
      file_type = get_file_type(path)
      is_filtered_by_file_type = !is_selected_file_type?(file_type: file_type, valid_file_types: valid_file_types)
      hide_deleted_files_active? ? (deleted || is_filtered_by_file_type) : is_filtered_by_file_type
    elsif hide_viewed_files_active?
      viewed
    elsif hide_deleted_files_active?
      deleted
    else
      false
    end
  end
end
