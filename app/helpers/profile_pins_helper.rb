# frozen_string_literal: true

module ProfilePinsHelper
  include PlatformHelper

  GistFragment = parse_query <<-'GRAPHQL'
    fragment on Gist {
      owner {
        login
      }
      name
      sha
    }
  GRAPHQL

  GistFileFragment = parse_query <<-'GRAPHQL'
    fragment on GistFile {
      name
    }
  GRAPHQL

  # Public: Get the URL to an image that's a file in a gist.
  #
  # Returns a String like "https://gist.githubusercontent.com/cheshire137/e85204895f59dfc3a002d54191307c9d/raw/bd01533989683b791002b8d03d2da9749edfb557/cheshireface.png".
  def gist_image_url(gist, gist_file)
    gist = GistFragment.new(gist)
    gist_file = GistFileFragment.new(gist_file)

    build_raw_url(type: :gist, route_options: {
      user_id: gist.owner&.login || Gist::ANONYMOUS_USERNAME,
      gist_id: gist.name,
      sha: gist.sha,
      file: gist_file.name,
    })
  end
end
