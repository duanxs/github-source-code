# frozen_string_literal: true

module Mobile
  module IssueHelper
    def author
      issue.safe_user
    end

    def title
      subject.title.blank? ? "Untitled" : subject.title
    end

    def state
      if issue.open? && issue.pull_request? && issue.pull_request&.draft?
        "draft"
      else
        issue.state
      end
    end

    def body_html
      subject.body_html.blank? ? "No description given." : subject.body_html
    end

    def milestone_text
      issue.milestone ? issue.milestone.title : "No milestone"
    end

    def timeline_items
      @timeline_items ||= subject.timeline_for(current_user)
    end

    def edited_at
      issue.edited_at
    end

    def editor
      return "an unknown user" if issue.editor.nil?
      issue.editor.login
    end

    def editor_link
      return "ghost" if issue.editor.nil?
      issue.editor.login
    end

    def edited?
      issue.edited?
    end
  end
end
