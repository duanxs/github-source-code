# frozen_string_literal: true

module Mobile::ApplicationHelper
  # Public: Adds DOM classes for cases in which we may need to override
  # base styles. E.g. Enterprise installs.
  #
  # Returns a string.
  def mobile_body_classes
    [
      ("enterprise" if GitHub.enterprise?),
      "page-responsive",
    ].compact
  end

  # Public: Whether or not we should show the "Mobile version" link while
  # viewing the desktop version.
  #
  # True if:
  #
  # 1. The current action has a mobile view available for it. Some actions won't
  #    have mobile versions (like probably the Releases page), so we don't want
  #    to show a "Switch to mobile version" link there.
  # 2. The user is actually on a mobile device.
  # 3. The page is not responsive.
  # 4. The user has not opted out of the mobile site completely.
  #
  # Returns a boolean.
  def show_mobile_link?
    mobile_view_available? && mobile? && !@page_responsive && !mobile_opt_out_enabled?
  end

  # Public: Whether or not there's a mobile view available for the current
  # action.
  #
  # Returns a boolean.
  def mobile_view_available?
    @mobile_view_available
  end

  # Public: Is the request coming from a mobile device?
  #
  # Returns a boolean.
  def mobile?
    GitHub::Mobile.mobile_user_agent?(request.user_agent.to_s)
  end

  # Public: Should mobile views be used? Yes if the device is mobile and they
  # explicitly don't want the desktop version.
  #
  # True if all of these things:
  #
  # 1. There is a mobile view available for the current controller action *an
  # 2. The user has not explicitly switched to desktop views.
  # 3. The user is on a mobile device *or* has explitly opted into mobile views
  #
  # Returns a boolean.
  def show_mobile_view?
    return false unless mobile_view_available?
    return false if session[:mobile] == false
    return false if mobile_opt_out_enabled?

    session[:mobile].present? || mobile?
  end

  # Is the current user completely opted out of the mobile experience?
  #
  # Returns a boolean.
  def mobile_opt_out_enabled?
    return @mobile_opt_out_enabled if defined? @mobile_opt_out_enabled

    @mobile_opt_out_enabled = logged_in? &&
    current_user.profile_settings.mobile_opt_out_enabled?
  end
end
