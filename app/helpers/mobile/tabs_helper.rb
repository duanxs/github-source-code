# frozen_string_literal: true

# Mixin for non-AJAXy tabs on mobile.
#
# Expects #current_tab to be implemented.
module Mobile
  module TabsHelper
    # Public: CSS class for the specified tab's selected state. 'selected'
    # if the specified tab is selected, empty if not.
    #
    # tab - String for the tab to check.
    #
    # Returns a string.
    def tab_selected_class(tab)
      tab_selected?(tab) ? "selected" : ""
    end

    # Public: Whether or not the specified tab is selected.
    #
    # tab - String for the tab to check.
    #
    # Returns a boolean.
    def tab_selected?(tab)
      current_tab == tab
    end
  end
end
