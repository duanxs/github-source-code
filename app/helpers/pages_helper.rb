# frozen_string_literal: true
module PagesHelper

  # Get pages certificate status for current repository, return nil if pages not configured or certificate not found.
  def get_certificate_status(repository)
    return nil unless repository.page && (repository.page.subdomain || repository.page.cname)
    domain = repository.page.cname ? repository.page.cname : repository.page.subdomain_with_host
    certificate = Page::Certificate.where(domain: domain).last
    return nil unless certificate
    certificate.state.to_sym
  end
end
