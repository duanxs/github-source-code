# frozen_string_literal: true

module Orgs::RemindersHelper
  def self.slack_installation_outdated?(slack_installation)
    slack_installation && !slack_installation.events.include?("reminder")
  end

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def this_slack_installation
    @slack_installation ||= Apps::Internal.integration(:slack)&.installations_on(this_organization)&.first
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def outdated_slack_installation_warning
    if Orgs::RemindersHelper.slack_installation_outdated?(this_slack_installation)
      render "orgs/reminders/outdated", slack_installation: this_slack_installation
    end
  end
end
