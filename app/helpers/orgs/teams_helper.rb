# frozen_string_literal: true

module Orgs::TeamsHelper
  CHILD_INELIGIBILITY_REASONS = {
    "SECRET" => "Is a secret team",
    "ANCESTOR" => "Is an ancestor of this team",
    "PERMISSION" => "Request to move this team",
    "CHILD" => "Already a child of this team",
  }.freeze

  def child_ineligibility_reason(eligibility_status)
    CHILD_INELIGIBILITY_REASONS[eligibility_status]
  end

  def show_team_settings_sidebar?
    return true if GitHub.flipper[:scheduled_reminders_frontend].enabled?(current_organization)
    true
  end

  def team_settings_options(org_login:, team_slug:, selected:)
    settings_options = {
      "General" => {
        url: edit_team_path(org: org_login, team_slug: team_slug),
        selected: false,
      },
      "Code review assignment" => {
        url: edit_team_review_assignment_path(org: org_login, team_slug: team_slug),
        selected: false,
      },
    }

    if defined?(:current_organization) && GitHub.flipper[:scheduled_reminders_frontend].enabled?(current_organization)
      settings_options["Scheduled reminders"] = { url: team_reminders_path(org: org_login, team_slug: team_slug), selected: false }
    end

    settings_options[selected][:selected] = true
    settings_options
  end
end
