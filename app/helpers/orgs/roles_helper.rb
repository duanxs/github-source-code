# frozen_string_literal: true

module Orgs::RolesHelper
  def custom_roles_supported?
    organization.plan_supports?(:custom_roles)
  end

  def custom_roles_present?
    custom_roles.present?
  end

  def custom_roles
    Role.custom_roles_for_org(organization)
  end

  def system_roles
    fine_grained_permissions_supported? ? rank_system_roles(Role.system_repo_roles) : rank_system_roles(Role.primary_system_repo_roles)
  end

  def fine_grained_permissions_supported?
    organization.plan_supports?(:fine_grained_permissions)
  end

  def has_description?(role)
    role.description.present?
  end

  def show_base_role_badge?(role)
    org_base_role.equal?(role.to_sym)
  end

  def icon(role)
    case role.to_sym
    when :read
      "book"
    when :triage
      "checklist"
    when :write
      "pencil"
    when :maintain
      "tools"
    when :admin
      "eye"
    end
  end

  def system_role_description(role)
    case role.to_sym
    when :read
      "Read and clone repositories. Open and comment on issues and pull requests."
    when :triage
      "Read permissions plus manage issues and pull requests."
    when :write
      "Triage permissions plus read, clone and push to repositories."
    when :maintain
      "Write permissions plus manage issues, pull requests and some repository settings."
    when :admin
      "Full access to repositories including sensitive and destructive actions."
    end
  end

  def org_base_role
    organization.default_repository_permission
  end

  private

  def rank_system_roles(roles)
    roles.sort { |a, b| Ability::ACTION_RANKING[a.name.to_sym] <=> Ability::ACTION_RANKING[b.name.to_sym] }
  end
end
