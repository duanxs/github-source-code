# frozen_string_literal: true

module ActionsHelper
  def actions_workflow_path(filters:, workflow: nil)
    actions_filtered_path(filters: filters, replace: { workflow: workflow })
  end

  def actions_filtered_path(filters:, replace: {})
    query = actions_filtered_query(filters: filters, replace: replace)

    return actions_path if query.blank?

    actions_path query: query
  end

  def actions_filtered_query(filters:, replace: {})
    filters = filters.dup

    replace.each_pair do |replace_key, replace_val|
      case replace_val
      when NilClass
        filters.reject! { |comp_key, _| comp_key == replace_key }
      else
        filters.merge!({replace_key => replace_val })
      end
    end

    Search::ParsedQuery.stringify(filters)
  end
end
