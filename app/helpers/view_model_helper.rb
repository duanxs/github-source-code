# frozen_string_literal: true

module ViewModelHelper

  # Internal: Create a new instance of a view model class. Automatically
  # provides a `:current_user` attribute to the view model.
  #
  # view_model_class - A Class inheriting from ViewModel or a ViewModel
  #                    instance. If it's a class, a new instance will be
  #                    created. If it's an instance it'll be dup'd and updated
  #                    with additional attributes.
  # attributes       - A Hash of attributes for the ViewModel or nil.
  #
  # Returns a ViewModel subclass instance.
  def create_view_model(view_model_class, attributes = nil)
    attributes ||= {}
    attributes[:current_user] = current_user
    attributes[:user_session] = user_session

    if view_model_class.is_a? ViewModel
      return view_model_class.dup.update attributes
    end

    view_model_class.new(**attributes)
  end

  def weak_password_used_for_sign_in?
    !!session[::CompromisedPassword::WEAK_PASSWORD_KEY]
  end

  def weak_password_used_for_creation_or_change?
    !!flash[::CompromisedPassword::WEAK_PASSWORD_KEY]
  end

  # Sugar: Render partials for ViewModel subclasses. Automatically
  # provides a `:current_user` attribute to the view model.
  def render_partial_view(partial, view_model_class, attributes = nil, render_options = {})
    render partial_view_params(partial, view_model_class, attributes, render_options) # rubocop:disable GitHub/RailsViewRenderLiteral
  end

  # Sugar: Render partials for ViewModel subclasses to strings. Automatically
  # provides a `:current_user` attribute to the view model.
  def render_partial_view_to_string(partial, view_model_class, attributes = nil, render_options = {})
    render_to_string partial_view_params(partial, view_model_class, attributes, render_options)
  end

  # Sugar: Render templates for ViewModel subclasses. See
  # `render_partial_view` for details.
  def render_template_view(template, view_model_class, attributes = nil, render_options = {})
    if Rails.env.test?
      # This lets us assert on `assigns(:view)` in controller tests.
      @view = create_view_model(view_model_class, attributes)
    end
    render template_view_params(template, view_model_class, attributes, render_options) # rubocop:disable GitHub/RailsViewRenderLiteral
  end

  private

  def partial_view_params(partial, view_model_class, attributes, render_options)
    object     = attributes && attributes.delete(:object)
    collection = attributes && attributes.delete(:collection)

    locals = {
      view: create_view_model(view_model_class, attributes),
    }.merge(render_options.delete(:locals) || {})

    view_params = {
      partial: partial,
      locals: locals,
    }.merge(render_options)
    # default formats to render .html.erb files
    view_params[:formats] ||= [:html]

    set_object(view_params, object)
    set_collection(view_params, collection)

    view_params
  end

  def template_view_params(template, view_model_class, attributes, render_options)
    layout = attributes && attributes.delete(:layout)
    locals = {
      view: create_view_model(view_model_class, attributes),
    }.merge(render_options.delete(:locals) || {})

    render_params = {
      template: template.dup,
      locals: locals,
    }.merge(render_options)
    # default formats to render .html.erb files
    render_params[:formats] ||= [:html]

    set_layout(render_params, layout)
    render_params
  end

  def set_object(view_params, object)
    return unless object
    view_params[:object] = object
  end

  def set_collection(view_params, collection)
    return unless collection
    view_params[:collection] = collection
  end

  def set_layout(render_params, layout)
    return if layout.nil?
    render_params[:layout] = layout
  end
end
