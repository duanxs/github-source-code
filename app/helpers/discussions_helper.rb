# frozen_string_literal: true

module DiscussionsHelper
  DISCUSSIONS_SUPPORT_URL = "https://support.github.com/contact/feedback?category=discussions"

  DISCUSSION_EVENT_ICONS = {
    locked: "lock",
    unlocked: "key",
    answer_marked: "check",
    answer_unmarked: "x",
    transferred: "arrow-right",
  }.freeze

  # Used with `hydro.schemas.github.v1.DiscussionClick` event
  DISCUSSION_CLICK_EVENT_CONTEXTS = [
    :EVENT_CONTEXT_UNKNOWN,
    :DISCUSSIONS_LIST,
    :DISCUSSION_VIEW,
    :NEW_DISCUSSION_VIEW,
  ].freeze

  # Used with `hydro.schemas.github.v1.DiscussionClick` event
  DISCUSSION_CLICK_EVENT_TARGETS = [
    :EVENT_TARGET_UNKNOWN,
    :DISCUSSION_LINK,
    :USER_PROFILE_LINK,
    :NEW_DISCUSSION_LINK,
    :COPY_LINK_MENU_ITEM,
    :QUOTE_REPLY_MENU_ITEM,
    :REFERENCE_IN_NEW_ISSUE_MENU_ITEM,
    :REPORT_CONTENT_MENU_ITEM,
    :COMMUNITY_LINK,
  ].freeze

  # Cap the number of participant avatars displayed in the Discussion index
  DISCUSSION_MAX_DISPLAYED_AVATARS = 3

  def show_discussion_spotlights?
    return true if discussions_zeit_enabled?
    discussion_spotlights_enabled? && current_repository.discussion_spotlights.any?
  end

  def discussions_current_sort
    discussions_search_term_values(:sort).first || "latest"
  end

  # Public: Get a dot for display in the Discussions tab of a repository when the
  # `discussions_activity_indicator` feature is enabled for the repository. The
  # dot's appearance indicates whether there is new activity or not.
  #
  # Returns HTML.
  def discussions_activity_indicator
    return unless current_repository.discussions_activity_indicator_enabled?

    # Last-viewed timestamp will be updated on these pages, so the dot shouldn't show,
    # so we don't need to bother trying to load it.
    return if params[:controller] == "discussions" && %w(show index).include?(params[:action])

    url = discussions_activity_indicator_path(current_repository.owner, current_repository)
    content_tag("include-fragment", src: url) do
      content_tag(:span, nil,
        class: "circle bg-gray-light border border-gray-dark ml-1 d-inline-block discussions-no-activity-indicator")
    end
  end

  # Public: Should we show the wrapper-looking header bar above comments
  # that contains the 'mark as answer' UI? The header bar also shows if
  # the comment has been marked as the answer.
  #
  # discussion_or_comment - a Discussion or DiscussionComment
  # timeline - a DiscussionTimeline
  #
  # Returns a Boolean.
  def show_discussion_mark_answer_header_bar?(discussion_or_comment, timeline:)
    return false unless discussion_or_comment.is_a?(DiscussionComment)
    return false unless timeline.question?
    discussion_or_comment.answer? ||
      timeline.can_unmark_as_answer?(discussion_or_comment) ||
      timeline.render_mark_as_answer?(discussion_or_comment)
  end

  # Public: Get a title for a comment in a question discussion. It will either
  # prompt the user to mark that the comment answers the discussion or detail
  # how the comment has already been marked as the answer.
  #
  # comment - a DiscussionComment
  # timeline - a DiscussionTimeline
  #
  # Returns HTML.
  def discussion_comment_mark_answer_title(comment, timeline:)
    contents = if comment.answer?
      safe_join([
        octicon(:check, class: "mr-2"),
        answer_selected_by_link(comment, timeline: timeline)
      ])
    else
      "Does this comment answer the question?"
    end

    content_tag(:span, contents, class: class_names(
      "mb-2 mb-md-0",
      "text-gray-light" => !comment.answer?
    ))
  end

  # Public: Get a brief summary about who marked the given discussion comment
  # as the answer, if that user is known.
  #
  # comment - a DiscussionComment that is the chosen_comment in its Discussion
  # timeline - a DiscussionTimeline
  #
  # Returns HTML.
  def answer_selected_by_link(comment, timeline:)
    user_who_marked_answer = timeline.chosen_comment_selected_by_user
    if user_who_marked_answer
      safe_join([
        content_tag(:span, "Answer selected by"),
        profile_link(user_who_marked_answer,
          data: discussion_view_click_attrs(comment, target: :USER_PROFILE_LINK),
          class: "d-inline-block text-white")
      ], " ")
    else
      content_tag(:span, "Answer selected")
    end
  end

  def discussion_category_emoji_tag(discussion_category, classes: nil)
    emoji = emoji_for(discussion_category.emoji)
    if emoji
      emoji_tag(emoji, class: "v-align-baseline #{classes}")
    else
      octicon("comment-discussion")
    end
  end

  def discussion_event_badge_color(event_type)
    case event_type.to_sym
    when :locked, :unlocked
      "text-white bg-gray-dark"
    when :answer_marked
      "text-green bg-green-light"
    else
      "text-gray-dark bg-gray"
    end
  end

  def discussion_event_badge_icon(event_type)
    octicon(DISCUSSION_EVENT_ICONS[event_type.to_sym] || "primitive-dot")
  end

  # Public: Get a verb or phrase to describe what a given event group represents.
  #
  # group - a DiscussionEventGroup
  #
  # Returns a String.
  def discussion_event_group_verb(group)
    event_type = group.event_type
    case event_type.to_sym
    when :answer_marked
      any_unmarked = group.events.any?(&:answer_unmarked?)
      if any_unmarked
        "unmarked and re-marked"
      else
        "marked"
      end
    when :answer_unmarked
      any_marked = group.events.any?(&:answer_marked?)
      if any_marked
        "marked then unmarked"
      else
        "unmarked"
      end
    when :locked
      any_unlocked = group.events.any?(&:unlocked?)
      if any_unlocked
        "unlocked and locked"
      else
        "locked"
      end
    when :unlocked
      any_locked = group.events.any?(&:locked?)
      if any_locked
        "locked and unlocked"
      else
        "unlocked"
      end
    else
      event_type.to_s.humanize
    end
  end

  def discussion_event_verb(event_type)
    case event_type.to_sym
    when :answer_marked
      "marked"
    when :answer_unmarked
      "unmarked"
    else
      event_type
    end
  end

  def show_first_time_contributor_discussions_sidebar?
    show_first_time_contributor_sidebar_for?(contribution_type: Discussion)
  end

  def discussions_beta_feedback_org
    if defined?(@discussions_beta_feedback_org)
      return @discussions_beta_feedback_org
    end
    @discussions_beta_feedback_org = Organization.find_by_login("gh-community")
  end

  def discussions_beta_feedback_repo
    if defined?(@discussions_beta_feedback_repo)
      return @discussions_beta_feedback_repo
    end
    @discussions_beta_feedback_repo = if discussions_beta_feedback_org
      repo = discussions_beta_feedback_org.repositories.find_by_name("discussions-feedback")
      repo if repo&.readable_by?(current_user)
    end
  end

  def discussions_beta_feedback_link(text: "Give feedback", classes: nil)
    url = if discussions_beta_feedback_repo
      unless current_repository == discussions_beta_feedback_repo
        discussions_path(discussions_beta_feedback_org, discussions_beta_feedback_repo)
      end
    else
      DISCUSSIONS_SUPPORT_URL
    end
    return unless url

    link_to(
      text,
      url,
      class: "text-small link-gray lh-condensed #{classes}",
      data: test_selector_hash("discussions_beta_feedback_link")
    )
  end

  def discussion_app_logo_link(app)
    link_to(
      image_tag(app.preferred_avatar_url(size: 40),
        alt: "#{app.name}",
        width: 20, height: 20,
        class: "avatar avatar-child rounded-1"),
      app.url,
    )
  end

  def discussion_app_or_bot_author_label(discussion_or_comment)
    if discussion_or_comment.respond_to?(:performed_via_integration) && discussion_or_comment.performed_via_integration
      discussion_app_logo_link(discussion_or_comment.performed_via_integration)
    elsif discussion_or_comment.author.bot? && is_deltaforce?(discussion_or_comment.author)
      avatar_for(GitHub.dependabot_github_app.owner, 20, class: "avatar avatar-child rounded-1")
    end
  end

  def discussion_chosen_comment_check_icon(discussion_or_comment, timeline:)
    return unless discussion_or_comment.respond_to?(:answer?)

    if timeline.question? && discussion_or_comment.answer?
      octicon(:check, height: 32, class: "text-green mt-2")
    end
  end

  def discussion_or_comment_author_avatar_link(discussion_or_comment, is_nested:)
    linked_avatar_for(
      discussion_or_comment.author,
      30,
      img_class: "avatar rounded-1 mr-2",
      link_data: discussion_view_click_attrs(discussion_or_comment, target: :USER_PROFILE_LINK),
    )
  end

  # Public: Get a linked avatar to represent the author of the given discussion or comment.
  #
  # discussion_or_comment - a Discussion or DiscussionComment instance
  # timeline - a DiscussionTimeline instance
  #
  # Returns HTML.
  def discussion_timeline_avatar(discussion_or_comment, timeline:)
    is_nested = discussion_or_comment_nested?(discussion_or_comment)

    content_tag(:div, class: class_names(
      "avatar-parent-child TimelineItem-avatar flex-column flex-items-center",
      "d-none d-md-flex" => !is_nested,
      "left-0 ml-3" => is_nested
    )) do
      safe_join([
        discussion_or_comment_author_avatar_link(discussion_or_comment, is_nested: is_nested),
        discussion_app_or_bot_author_label(discussion_or_comment),
        discussion_chosen_comment_check_icon(discussion_or_comment, timeline: timeline),
      ].compact)
    end
  end

  def show_discussion_child_comments_thread_for?(discussion_or_comment)
    discussion_or_comment.is_a?(DiscussionComment) && discussion_or_comment.top_level_comment?
  end

  def discussions_list_path(**args)
    if current_repository
      discussions_path(current_repository.owner, current_repository, args)
    elsif respond_to?(:this_organization) && this_organization
      org_discussions_path(this_organization, args)
    else
      all_discussions_path(args)
    end
  end

  # Public: Extract filter values for a given search key.
  #
  # search_key - the Symbol key for which to extract values, e.g., :author
  #
  # Examples
  #
  #   # Given discussions_q value of "author:iancanderson"
  #   discussions_search_term_values(:author)
  #   # => ["iancanderson"]
  #
  # Returns an array of values for the key.
  def discussions_search_term_values(search_key, excluded: false)
    keys_and_values = parsed_discussions_query.select do |(key, value, negation)|
      if negation && !excluded
        # search_key, if negated, is in the form "-term".
        # the equivalent parsed_query value would be [:"term", "value", true]
        # as such, we must prepend `key` with a `-` if negation is true to correctly match negated terms
        "-#{key}" == search_key.to_s
      elsif negation || excluded
        negation && excluded && key == search_key
      else
        key == search_key
      end
    end
    keys_and_values.map { |(k, v)| v }
  end

  # Public: Build a discussions search path.
  #
  # replace - Hash of values to override in the current search query
  # category_override - replace all category components with one instance of
  # this override string
  #
  # Examples
  #
  #   # Given discussions_q value of "author:iancanderson"
  #   discussions_search_path(replace: { author: "cheshire137" })
  #   # => "/:owner/:repo/discussions?discussions_q=author:cheshire137"
  #
  #   discussions_search_path(replace: { author: nil })
  #   # => "/:owner/:repo/discussions"
  #
  #   # Given discussions_q value of "category:fun"
  #   discussions_search_path(replace: { category: "boring" })
  #   # => "/:owner/:repo/discussions?discussions_q=category:fun+category:boring"
  #
  #   discussions_search_path(category_override: "boring")
  #   # => "/:owner/:repo/discussions?discussions_q=category:boring"
  #
  # Returns a String relative path
  def discussions_search_path(replace: {}, category_override: nil, append: [])
    query_str = discussions_search_query(replace: replace, category_override: category_override,
      append: append)
    query_params = { discussions_q: query_str }
    discussions_list_path(**query_params.compact)
  end

  def discussions_search_query(replace: {}, category_override: nil, append: [])
    components = parsed_discussions_query.dup

    replace.each_pair do |replace_key, replace_val|
      case replace_val
      when NilClass
        components.reject! { |comp_key, _| comp_key == replace_key }
      when Hash
        components.reject! do |comp_key, comp_val|
          next unless comp_key == replace_key
          next unless replace_val.one?
          replace_val.keys.first == comp_val && replace_val.values.first.nil?
        end
      else
        components << [replace_key, replace_val]
      end
    end

    components += append

    if category_override.present?
      components.reject! { |comp_key, _| comp_key == :category }
      components << [:category, category_override]
    end

    Search::Queries::DiscussionQuery.stringify(components.uniq).presence
  end

  def discussions_author_filter_content_path(**args)
    if current_repository
      author_filter_content_discussions_path(current_repository.owner, current_repository, args)
    elsif this_organization
      org_discussions_author_filter_content_path(this_organization, args)
    end
  end

  def discussion_or_comment_open_new_issue_modal_path(discussion_or_comment, repo_owner:, repo_name:, discussion: nil)
    if discussion_or_comment.is_a?(Discussion)
      open_new_issue_modal_discussion_path(repo_owner, repo_name, discussion_or_comment)
    else
      open_new_issue_modal_discussion_comment_path(repo_owner, repo_name, discussion, discussion_or_comment)
    end
  end

  def discussion_icon(discussion, white_icon: false)
    if discussion.conversation?
      options = { icon_color: white_icon ? :white : :gray }
      component = Discussions::IconConversationComponent
    else
      options = white_icon ? { icon_color: :white } : {}

      component = if discussion.answered?
        Discussions::IconAnsweredComponent
      else
        Discussions::IconUnansweredComponent
      end
    end

    render(component.new(**options))
  end

  def discussion_state_badge(discussion, timeline:, responsive_hide: false)
    icon = discussion_icon(discussion, white_icon: true)
    description = "Conversation"
    bg_class = nil

    if timeline.question?
      if discussion.answered?
        description = "Answered"
        bg_class = "State--purple"
      else
        description = "Unanswered"
        bg_class = "State--green"
      end
    end

    if responsive_hide
      description = content_tag(:span, description, class: "d-none d-md-inline")
    end

    content_tag(:span, safe_join([
      icon,
      description,
    ], " "), class: "State #{bg_class}")
  end

  def discussion_or_comment_nested?(discussion_or_comment)
    discussion_or_comment.is_a?(DiscussionComment) && !discussion_or_comment.top_level_comment?
  end

  def show_discussion_comment_mark_answer_popover?(comment, timeline:)
    return false unless logged_in?
    return false if current_user.dismissed_notice?(UserNotice::DISCUSSION_MARK_ANSWER_NOTICE)

    timeline.is_first_comment_markable_as_answer_on_the_page?(comment.id)
  end

  def show_discussion_or_comment_edit_history?(discussion_or_comment)
    if discussion_or_comment.is_a?(DiscussionComment)
      !discussion_or_comment.wiped? || site_admin?
    else
      # Expect this helper to only be called from the discussion page,
      # and if you're there, you have read access to the repository,
      # which means you should be able to view the edit history
      # on the original post of the discussion.
      true
    end
  end

  def discussion_comment_minimization_reason(comment)
    minimized_reason = comment.comment_hidden_classifier

    if minimized_reason == "spam" && logged_in? && comment.spammy? && comment.user&.spammy? &&
      current_user.site_admin?
      return "This comment and user were marked as spammy."
    end

    case minimized_reason
    when "spam"
      "This comment was marked as spam."
    when "abuse"
      "This comment was marked as disruptive content."
    when "off-topic"
      "This comment was marked as off-topic."
    when "outdated"
      "This comment was marked as outdated."
    when "resolved"
      "This comment was marked as resolved."
    else
      "This comment has been minimized."
    end
  end

  def safe_new_discussion_click_attrs(target:)
    safe_data_attributes(new_discussion_click_attrs(target: target))
  end

  def new_discussion_click_attrs(target:)
    discussion_click_attrs(nil, event_context: :NEW_DISCUSSION_VIEW, target: target)
  end

  # Public: Get HTML-safe Hydro `data` attributes for clicking something on the discussions
  # index page.
  #
  # discussion_or_comment - a Discussion or DiscussionComment; optional
  # target - a Symbol from the `DISCUSSION_CLICK_EVENT_TARGETS` list representing
  #          what was clicked
  #
  # Returns a String.
  def safe_discussions_list_click_attrs(discussion_or_comment, target:)
    safe_data_attributes(discussions_list_click_attrs(discussion_or_comment, target: target))
  end

  # Public: Get Hydro `data` attributes for clicking something on the discussions index page.
  #
  # discussion_or_comment - a Discussion or DiscussionComment; optional
  # target - a Symbol from the `DISCUSSION_CLICK_EVENT_TARGETS` list representing
  #          what was clicked
  #
  # Returns a Hash.
  def discussions_list_click_attrs(discussion_or_comment, target:)
    discussion_click_attrs(discussion_or_comment, event_context: :DISCUSSIONS_LIST, target: target)
  end

  # Public: Get HTML-safe Hydro `data` attributes for clicking something on an individual
  # discussion page.
  #
  # discussion_or_comment - a Discussion or DiscussionComment
  # target - a Symbol from the `DISCUSSION_CLICK_EVENT_TARGETS` list representing
  #          what was clicked
  #
  # Returns a String.
  def safe_discussion_view_click_attrs(discussion_or_comment, target:)
    safe_data_attributes(discussion_view_click_attrs(discussion_or_comment, target: target))
  end

  # Public: Get Hydro `data` attributes for clicking something on an individual discussion page.
  #
  # discussion_or_comment - a Discussion or DiscussionComment
  # target - a Symbol from the `DISCUSSION_CLICK_EVENT_TARGETS` list representing
  #          what was clicked
  #
  # Returns a Hash.
  def discussion_view_click_attrs(discussion_or_comment, target:)
    discussion_click_attrs(discussion_or_comment, event_context: :DISCUSSION_VIEW, target: target)
  end

  # Public: Get Hydro `data` attributes for clicking something discussion-related.
  #
  # discussion_or_comment - a Discussion or DiscussionComment; optional
  # event_context - a Symbol from the `DISCUSSION_CLICK_EVENT_CONTEXTS` list representing
  #                 where on the site the click happened
  # target - a Symbol from the `DISCUSSION_CLICK_EVENT_TARGETS` list representing
  #          what was clicked
  #
  # Returns a Hash.
  def discussion_click_attrs(discussion_or_comment, event_context: :EVENT_CONTEXT_UNKNOWN, target: :EVENT_TARGET_UNKNOWN)
    unless DISCUSSION_CLICK_EVENT_CONTEXTS.include?(event_context)
      raise ArgumentError, "invalid discussion click event context '#{event_context}'"
    end

    unless DISCUSSION_CLICK_EVENT_TARGETS.include?(target)
      raise ArgumentError, "invalid discussion click event target '#{target}'"
    end

    data = {
      event_context: event_context,
      target: target,
      current_repository_id: current_repository&.id,
      discussion_repository_id: discussion_or_comment&.repository_id,
    }

    if discussion_or_comment
      data[:discussion_id] = discussion_or_comment.discussion_id
      data[:discussion_comment_id] = discussion_or_comment.discussion_comment_id
    end

    hydro_click_tracking_attributes("discussions.click", data)
  end

  def is_deltaforce?(user)
    return false unless GitHub.dependabot_github_app.present?

    promise = user.async_integration.then do |integration|
      integration.present? && integration.dependabot_github_app?
    end
    promise.sync
  end

  def discussion_timeline_comment_url(discussion_or_comment, timeline:)
    if discussion_or_comment.is_a?(DiscussionComment)
      "#{discussion_url(timeline.repo_owner, timeline.repo_name, timeline.discussion)}#discussioncomment-#{discussion_or_comment.id}"
    else
      discussion_url(timeline.repo_owner, timeline.repo_name, discussion_or_comment)
    end
  end

  # Public: For a given sort filter value, get the name for hydro tracking
  #
  # sort_filter - Optional String sort filter that would show in the search box, e.g. "created-desc".
  #
  # Returns a String that's a lowercased version of the Hydro sort enum, e.g. "newest"
  def discussion_sort_option_tracking_name(sort_filter = discussions_current_sort)
    return nil unless sort_filter

    if sort_filter == "top"
      "top"
    elsif sort_filter == "latest"
      "newest"
    end
  end

  # Discussion page <title>. This is "Discussions - <user>/<repo>" for index pages
  # or "<discussion.title> - Discussions - <user>/<repo>" for individual discussion pages.
  def discussion_page_title(discussion = nil)
    [("#{discussion.title} · Discussion ##{discussion.number}" if discussion),
      ("Discussions" if !discussion),
      current_repository.name_with_owner,
    ].compact.join(" · ")
  end

  def comment_markdown_link_box
    content_tag(:div, class: "d-flex flex-items-center flex-auto mb-3 mb-md-0") do
      link_to(
        "#{GitHub.guides_url}/features/mastering-markdown/",
        :class => "tabnav-extra p-0 m-0",
        :target => "_blank",
        "data-ga-click" => "Markdown Toolbar, click, help",
      ) do
        safe_join([
          octicon(:markdown, class: "v-align-bottom"),
          "Styling with Markdown is supported",
        ], " ")
      end
    end
  end

  def mark_comments_as_unread!
    content_for(:unread_comment_class, " js-unread-item will-transition-once unread-item")
  end

  def unread_comment_class
    content_for(:unread_comment_class) || ""
  end

  # Group discussion elements by their type, preserving the original order of the
  # items.
  #
  # items - A flat array of any of these objects: IssueComment, CommitComment,
  #         PullRequestReviewComment, or Commit.
  #
  # Returns an Array of items grouped by type. Each element of the returned array
  #   is an array of same-type items.
  def group_discussion(items)
    groups = []

    items.each do |item|
      previous = groups.last.try(:first)
      if item.is_a?(CommitCommentThread) || item.is_a?(DeprecatedPullRequestReviewThread)
        groups << item
      elsif item.is_a?(IssueComment) || item.is_a?(GistComment)
        groups << [item]
      elsif item.is_a?(IssueTimelineProgressiveDisclosureMarker)
        groups << item
      elsif previous.class == item.class
        groups.last << item
      else
        groups << [item]
      end
    end

    groups
  end

  # Checks to see whether a group is of a certain type.
  #
  # group   - Array of timeline items to check the type of.
  # classes - Class or Array of Classes to check the group for.
  #
  # Returns true if the group matches at least one of the classes, false if it
  # does not.
  def group_is_a?(group, *classes)
    classes.flatten.any? do |klass|
      group.respond_to?(:first) && group.first.is_a?(klass)
    end
  end

  def pull_request_commits(timeline_item_group)
    timeline_item_group.map(&:commit).tap do |commits|
      Commit.prefill_users(commits)
    end
  end

  def could_create_discussion_if_verified?
    logged_in? && current_user.no_verified_emails? &&
      User::InteractionAbility.interaction_allowed?(current_user, current_repository)
  end

  def hydro_discussions_filter_tracking_data(filter:, sort:)
    if filter
      enum_value = Hydro::Schemas::Github::V1::DiscussionApplyFilter::Filter.resolve(filter.upcase.to_sym)
      raise ArgumentError.new("Invalid Filter enum value: #{filter}") unless enum_value
    end

    if sort
      enum_value = Hydro::Schemas::Github::V1::DiscussionApplyFilter::Sort.resolve(sort.upcase.to_sym)
      raise ArgumentError.new("Invalid Sort enum value: #{sort}") unless enum_value
    end

    hydro_click_tracking_attributes("discussions.apply_filter",
      repository_id: current_repository&.id,
      sort: sort,
      filter: filter,
    )
  end

  # Public: Should deleting a discussion or one of its comments happen in an
  # AJAX request or a regular HTTP request?
  #
  # discussion_or_comment - a Discussion or DiscussionComment
  #
  # Returns a Boolean. True indicates an AJAX request is fine for deletion.
  def delete_discussion_or_comment_async?(discussion_or_comment)
    if discussion_or_comment.is_a?(Discussion)
      # Do regular HTTP request for deleting a discussion since
      # the user will be redirected to another page.
      false
    else
      # If we have a nested comment, we can delete it asynchronously because
      # it won't have any child comments.
      return true if discussion_or_comment.is_a_child_comment?

      # If we have a top-level comment with no children, can delete
      # asynchronously. Otherwise, want to do a regular HTTP request so
      # the page reloads and we show the ghost user for the wiped parent comment.
      discussion_or_comment.comment_count < 1
    end
  end

  def conditional_tag(name, condition, options = nil, &block)
    if condition == true
      content_tag name, capture(&block), options
    else
      capture(&block)
    end
  end

  def discussions_category_selected?(category_name:)
    parsed_discussions_query.include?([:category, category_name])
  end

  def no_discussions_category_selected?
    parsed_discussions_query.none? { |field, _| field == :category }
  end
end
