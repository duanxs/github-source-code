# frozen_string_literal: true

module OrganizationAnalyticsHelper
  include HydroHelper

  module OrganizationsNew
    FREE_PLAN_CARD = :FREE_PLAN_CARD;
    TEAM_PLAN_CARD = :TEAM_PLAN_CARD;
    ENTERPRISE_PLAN_CARD = :ENTERPRISE_PLAN_CARD;
    REVIEW_SUBSCRIPTION_PLANS = :REVIEW_SUBSCRIPTION_PLANS;
    ENTERPRISE_PLAN_CARD_CONTACT_LINK = :ENTERPRISE_PLAN_CARD_CONTACT_LINK;
    COMPANY_OWNED = :COMPANY_OWNED;
  end

  REFERRAL_KEYS = %i(ref_page ref_cta ref_loc)

  # Provide a wrapper around the standard organiztion_new parameters sent to hydro for analytics
  #
  # event_context - symbol representing the context for the event to be sent to hydro
  # target -  symbol representing the event
  #
  # Returns a Hash
  def organization_new_hydro_click_tracking_attributes(target:)
    hydro_click_tracking_attributes(
      "organization.new.click",
      target: target,
      user_id: current_user&.id,
    )
  end

  def ga_label_with_analytics_tracking_id(ga_string, organization: nil)
    ga_string += ";" if ga_string.last != ";"

    meta_label = "meta: user_id: #{current_user&.analytics_tracking_id}"
    meta_label += ";org_id: #{organization.analytics_tracking_id}" if organization
    meta_label += referral_meta_labels

    "#{ga_string} #{meta_label}"
  end

  def organization_referral_params(additional_params: {})
    params.permit(*REFERRAL_KEYS).reject { |k, v| v.blank? }.merge(additional_params)
  end

  def referral_meta_labels
    return unless defined?(:params)

    meta_label = ""
    meta_label += ";ref_page:#{params[:ref_page]}" if params.has_key?(:ref_page)
    meta_label += ";ref_cta:#{params[:ref_cta]}" if params.has_key?(:ref_cta)
    meta_label += ";ref_loc:#{params[:ref_loc]}" if params.has_key?(:ref_loc)

    meta_label
  end

  def organization_redesign_free_plan_card_data_attributes
    hydro_attributes = organization_new_hydro_click_tracking_attributes(
      target: OrganizationsNew::FREE_PLAN_CARD,
    )

    {
      "ga-click" => ga_label_with_analytics_tracking_id("Signup funnel select org plan,click,text:Choose Team for Open Source;"),
      "ga-ec" => JSON.dump([
        ["ec:addProduct", { name: "TeamForOpenSource", position: 1 }],
        ["ec:setAction", "click", { list: "Signup" }],
      ]),
    }
      .merge(hydro_attributes)
  end

  def organization_redesign_team_plan_card_data_attributes
    hydro_attributes = organization_new_hydro_click_tracking_attributes(
      target: OrganizationsNew::TEAM_PLAN_CARD,
    )

    {
      "ga-click" => ga_label_with_analytics_tracking_id("Signup funnel select org plan,click,text:Choose Team;"),
      "ga-ec" => JSON.dump([
        ["ec:addProduct", { name: "Team", position: 2 }],
        ["ec:setAction", "click", { list: "Signup" }],
      ]),
    }.merge(hydro_attributes)
  end

  def organization_redesign_enterprise_plan_card_data_attributes
    hydro_attributes = organization_new_hydro_click_tracking_attributes(
      target: OrganizationsNew::ENTERPRISE_PLAN_CARD,
    )

    {
      "ga-click" => ga_label_with_analytics_tracking_id("Signup funnel select org plan,click,text:Start your 14-day free trial;"),
      "ga-ec" => JSON.dump([
        ["ec:addProduct", { name: "Enterprise Cloud", position: 3 }],
        ["ec:setAction", "click", { list: "Signup" }],
      ]),
    }.merge(hydro_attributes)
  end

  def organization_new_free_plan_card_data_attributes
    hydro_attributes = organization_new_hydro_click_tracking_attributes(
      target: OrganizationsNew::FREE_PLAN_CARD,
    )

    {"ga-change" => "New pricing, select product, Free;"}
      .merge(hydro_attributes)
  end

  def organization_new_team_plan_card_data_attributes
    hydro_attributes = organization_new_hydro_click_tracking_attributes(
      target: OrganizationsNew::TEAM_PLAN_CARD,
    )

    {
      "ga-change" => "New pricing, select product, Team;",
      "ga-ec" => JSON.dump([
        ["ec:addProduct", { name: "Team", position: 2 }],
        ["ec:setAction", "click", { list: "Signup" }],
      ]),
    }.merge(hydro_attributes)
  end

  def organization_new_enterprise_plan_card_data_attributes
    hydro_attributes = organization_new_hydro_click_tracking_attributes(
      target: OrganizationsNew::ENTERPRISE_PLAN_CARD,
    )

    {
      "ga-change" => "New pricing, select product, Business;",
      "ga-ec" => JSON.dump([
        ["ec:addProduct", { name: "Business", position: 3 }],
        ["ec:setAction", "click", { list: "Signup" }],
      ]),
    }.merge(hydro_attributes)
  end

  def organization_new_enterprise_server_contact_data_attributes
    hydro_attributes = organization_new_hydro_click_tracking_attributes(
      target: OrganizationsNew::ENTERPRISE_PLAN_CARD_CONTACT_LINK,
    )

    {"ga-click" => ga_label_with_analytics_tracking_id("Signup funnel select org plan, click, text:Contact our team;")}
      .merge(hydro_attributes)
  end

  def enterprise_plan_cloud_data_attributes
    {
      "ga-click" => ga_label_with_analytics_tracking_id("Signup funnel select enterprise plan,click,text:Choose Cloud;"),
      "ga-ec" => JSON.dump([
        ["ec:addProduct", { name: "Enterprise Cloud", position: 1 }],
        ["ec:setAction", "click", { list: "Signup" }],
      ]),
    }
  end

  def enterprise_plan_server_data_attributes
    {
      "ga-click" => ga_label_with_analytics_tracking_id("Signup funnel select enterprise plan,click,text:Choose Server;"),
    }
  end

  def organization_plan_comparison_data_attributes(text:)
    {
      "ga-click" => ga_label_with_analytics_tracking_id("Organization plan pricing comparison,click,text:#{text};"),
    }.merge(test_selector_hash("organization-plan-pricing-comparison-button"))
  end
end
