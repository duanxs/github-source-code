# frozen_string_literal: true

module IssueEventsHelper
  include PlatformHelper

  def platform_type_to_event_name(type_name)
    return "renamed" if type_name == "RenamedTitleEvent"
    underscored = type_name.underscore
    underscored[0, underscored.rindex("_")]
  end

  EVENT_ICONS = {
    "labeled"                    => "tag",
    "unlabeled"                  => "tag",
    "milestoned"                 => "milestone",
    "demilestoned"               => "milestone",
    "closed"                     => "circle-slash",
    "merged"                     => "git-merge",
    "assigned"                   => "person",
    "unassigned"                 => "person",
    "review_requested"           => "eye",
    "review_request_removed"     => "x",
    "head_ref_deleted"           => "git-branch",
    "head_ref_restored"          => "git-branch",
    "renamed"                    => "pencil",
    "locked"                     => "lock",
    "unlocked"                   => "key",
    "base_ref_force_pushed"      => "repo-force-push",
    "head_ref_force_pushed"      => "repo-force-push",
    "signoff"                    => "thumbsup",
    "signoff_canceled"           => "dash",
    "base_ref_changed"           => "git-branch",
    "added_to_project"           => "project",
    "moved_columns_in_project"   => "project",
    "removed_from_project"       => "project",
    "converted_note_to_issue"    => "project",
    "comment_deleted"            => "x",
    "marked_as_duplicate"        => "bookmark",
    "unmarked_as_duplicate"      => "bookmark",
    "ready_for_review"           => "eye",
    "pinned"                     => "pin",
    "unpinned"                   => "pin",
  }
  EVENT_ICONS.default = "primitive-dot"
  EVENT_ICONS.freeze

  def icon_for_event(event)
    EVENT_ICONS[event]
  end

  PerformEventTypeSpecificRollup = parse_query <<-'GRAPHQL'
    fragment on TimelineEvent {
      ... on ReviewDismissedEvent {
        pullRequestCommit {
          commit { oid }
        }
      }

      ... on MarkedAsDuplicateEvent {
        canonical {
          ... on Issue { id }
          ... on PullRequest { id }
        }
      }
      ... on UnmarkedAsDuplicateEvent {
        canonical {
          ... on Issue { id }
          ... on PullRequest { id }
        }
      }

      ... on ReviewRequestedEvent {
        reviewRequest {
          codeOwnersFile { id }
        }
      }
    }
  GRAPHQL

  EventsCanRollup = parse_query <<-'GRAPHQL'
    fragment on TimelineEvent {
      createdAt
      actor { login }

      ... on PerformableViaApp {
        viaApp { id }
      }

      ...IssueEventsHelper::PerformEventTypeSpecificRollup
    }
  GRAPHQL

  # Requires: EventsCanRollup on nodes
  def rollup_issue_event_nodes(nodes)
    comparison_event = nodes.first
    nodes.chunk_while { |node_before, node_after|
      events_can_rollup?(event: node_after, comparison_event: comparison_event).tap do |can_rollup|
        comparison_event = node_after unless can_rollup
      end
    }.to_a
  end

  def events_can_rollup?(event:, comparison_event:)
    event = EventsCanRollup.new(event)
    comparison_event = EventsCanRollup.new(comparison_event)

    # Events must be able to be rolled up
    return false unless perform_rollup_for_event_type?(event)

    # Events must have the same rollup key
    return false unless event_type_rollup_key(event) == event_type_rollup_key(comparison_event)

    # Events must have the same actor
    return false unless event.actor&.login == comparison_event.actor&.login

    # Events must have been triggered via the same integration
    if event.is_a?(PlatformTypes::PerformableViaApp) && comparison_event.is_a?(PlatformTypes::PerformableViaApp)
      return false unless event.via_app&.id == comparison_event.via_app&.id
    end

    # Events need to be within a week of each other.
    return false unless (event.created_at - comparison_event.created_at).abs < 7.days.to_i

    # Event specific details allow rolling up
    return false unless perform_event_type_specific_rollup?(
      event: event, comparison_event: comparison_event,
    )

    true
  end

  # Platform type => rollup key
  EVENT_ROLLUP_TYPES = {
    "Platform::Objects::LabeledEvent"               => :label,
    "Platform::Objects::UnlabeledEvent"             => :label,
    "Platform::Objects::AssignedEvent"              => :assignment,
    "Platform::Objects::UnassignedEvent"            => :assignment,
    "Platform::Objects::ReviewRequestedEvent"       => :review_request,
    "Platform::Objects::ReviewRequestRemovedEvent"  => :review_request,
    "Platform::Objects::ReviewDismissedEvent"       => :review_dismissal,
    "Platform::Objects::MilestonedEvent"            => :milestone,
    "Platform::Objects::DemilestonedEvent"          => :milestone,
    "Platform::Objects::MarkedAsDuplicateEvent"     => :duplicate,
    "Platform::Objects::UnmarkedAsDuplicateEvent"   => :duplicate,
    "Platform::Objects::CrossReferencedEvent"       => :cross_reference,
    "Platform::Objects::HeadRefForcePushedEvent"    => :head_ref_force_push,
    "Platform::Objects::BaseRefForcePushedEvent"    => :base_ref_force_push,
    "Platform::Objects::ConnectedEvent"             => :connected,
    "Platform::Objects::DisconnectedEvent"          => :disconnected,
  }.freeze

  def perform_rollup_for_event_type?(event)
    event_type_rollup_key(event)
  end

  def event_type_rollup_key(event)
    EVENT_ROLLUP_TYPES[event.class.type.name]
  end

  def perform_event_type_specific_rollup?(event:, comparison_event:)
    # We know both events have the same rollup key
    event = PerformEventTypeSpecificRollup.new(event)
    comparison_event = PerformEventTypeSpecificRollup.new(comparison_event)

    case comparison_event
    when PlatformTypes::ReviewDismissedEvent
      event.pull_request_commit&.commit&.oid == comparison_event.pull_request_commit&.commit&.oid
    when PlatformTypes::MarkedAsDuplicateEvent
      event.canonical&.id == comparison_event.canonical&.id
    when PlatformTypes::ReviewRequestedEvent
      comparison_code_owners_file_id = comparison_event.review_request&.code_owners_file&.id

      # Review requests with code owners file should only be rolled up with other review
      # requests (and not review request removals) pointing to the same code owners file.
      if comparison_code_owners_file_id
        event.is_a?(PlatformTypes::ReviewRequestedEvent) &&
          comparison_code_owners_file_id == event.review_request&.code_owners_file&.id
      elsif event.is_a?(PlatformTypes::ReviewRequestedEvent)
        !event.review_request&.code_owners_file
      else
        true
      end
    when PlatformTypes::ReviewRequestRemovedEvent
      if event.is_a?(PlatformTypes::ReviewRequestedEvent)
        !event.review_request&.code_owners_file
      else
        true
      end
    else
      true
    end
  end

  def event_safe_actor(event:)
    if event.respond_to?(:event_actor)
      actor = event.event_actor(viewer: current_user)
      return "ghost" if actor.nil?
      return "Repository owner" if actor.ghost?
      return actor.login
    end

    event.safe_actor.login
  end
end
