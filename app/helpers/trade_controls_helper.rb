# frozen_string_literal: true

module TradeControlsHelper
  def trade_controls_user_account_restricted_notice
    TradeControls::Notices.user_account_restricted_generic
  end

  def trade_controls_secret_gist_restricted
    TradeControls::Notices.secret_gist_restricted
  end

  def trade_controls_repo_disabled_notice
    TradeControls::Notices.repo_disabled_generic
  end

  def trade_controls_private_repo_creation_warning
    TradeControls::Notices.free_private_repo_warning
  end

  def trade_controls_user_private_repo_creation_warning
    TradeControls::Notices.free_private_repo_warning
  end

  def trade_controls_organization_account_restricted_mail
    TradeControls::Notices.organization_account_enforcement_mail
  end

  def trade_controls_organization_billing_account_restricted
    TradeControls::Notices.billing_account_restricted
  end

  def trade_controls_organization_repo_disabled_notice
    TradeControls::Notices.organization_owned_repo_disabled_generic
  end

  def trade_controls_organization_repo_disabled_non_admins_notice
    TradeControls::Notices.organization_owned_repo_disabled_for_non_admins_generic
  end

  def trade_controls_organization_invite_restricted_notice
    TradeControls::Notices.org_invite_restricted.html_safe #rubocop:disable Rails/OutputSafety
  end

  def trade_controls_restricted_public_abilities_for_gist
    TradeControls::Notices.restricted_public_abilities_for(type: "gist")
  end

  def trade_controls_restricted_public_abilities_for_repository
    TradeControls::Notices.restricted_public_abilities_for(type: "repository")
  end

  def trade_controls_archived_admin_notice
    TradeControls::Notices.organization_owned_repo_archived_generic
  end

  def trade_controls_archived_non_admins_notice
    TradeControls::Notices.organization_owned_repo_archived_for_non_admins_generic
  end

  def trade_controls_archived_cta_for_admins
    TradeControls::Notices.archived_cta_for_admins.html_safe #rubocop:disable Rails/OutputSafety
  end

  def trade_controls_archived_cta_for_non_admins
    TradeControls::Notices.archived_cta_for_non_admins.html_safe #rubocop:disable Rails/OutputSafety
  end

  def trade_controls_free_org_restricted_notice
    TradeControls::Notices.free_organization_account_restricted
  end

  def should_show_free_org_restricted_banner?(organization)
    return false if current_user.dismissed_notice?(Billing::OFACCompliance::FREE_ORG_NOTICE_FLAG)
    return false unless organization&.organization?
    return false unless GitHub.flipper[:plans_munich].enabled?(organization)
    return false unless organization.uncharged_account?

    # we are currently transitioning how we handle restrictions to a tiered restriction model.
    # We will be removing partial trade restrictions in the future.
    organization.has_partial_trade_restrictions? || organization.has_any_tiered_trade_restrictions?
  end
end
