# frozen_string_literal: true

module OptimizelyHelper

  def optimizely_meta_tags
    tag :meta, name: "optimizely-datafile", content: GitHub.optimizely.raw_datafile
  end

end
