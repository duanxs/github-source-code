# frozen_string_literal: true

module Registry
  module QueryHelper

    QUERYABLE_VISIBILITIES = %w(public private).freeze
    QUERYABLE_ECOSYSTEMS = %w(
      container
      docker
      maven
      npm
      nuget
      rubygems
    ).freeze

    SORT_TO_QUERY_PARAM = {
      "downloads_desc" => %w[downloads desc],
      "downloads_asc" => %w[downloads asc]
    }.freeze

    # Default to returning 30 search results
    PER_PAGE = 30

    # Public: Returns a list of packages based on the search parameters
    # provided. Uses elasticsearch to get search results through
    # Search::Queries::RegistryPackageQuery.
    #
    # Params:
    #  * current_user - The User who made this request.
    #  * user_session - The UserSession of the request.
    #  * owner - User or Organization that owns the package.
    #  * repo_id - The id of a Repository to scope the results to.
    #              Only used by Registry::PackagesController.
    #  * query - a query String
    #  * package_type - ecosystem filter string
    #  * visibility - visibility filter string
    #  * sort - Array of strings, see RegistryPackageQuery for sort fields.
    #
    # Returns: A mixed array of Registry::Package and PackageRegistry::Package.
    def packages_for_query(current_user:, user_session:, owner:, repo_id: nil, query: nil, package_type: nil, visibility: nil, sort: nil, page: 0, per_page: PER_PAGE)
      search = Search::QueryHelper.new(query, "RegistryPackages",
        current_user: current_user,
        user_session: user_session,
        highlight: false,
        owner: owner,
        repo_id: repo_id,
        query: query,
        package_type: package_type,
        visibility: visibility,
        sort: sort,
        per_page: per_page,
        page: page
      )["RegistryPackages"]

      results = search.execute

      package_ids = results.map(&:id)
      rms_ids, ar_ids = package_ids.partition { |id| id.start_with?("rms") }
      rms_ids = rms_ids.map { |str_id| str_id.sub(/rms-/, "").to_i }

      # Only ActiveRecord packages need to be filtered for visibility.
      # Authorization check for containers happens in registry-metadata.
      ar_packages = owner.packages.with_active_versions.where(id: ar_ids)
      ar_packages = filter_packages_by_visibility(ar_packages, viewer: current_user)
      ar_packages_by_id = ar_packages.index_by(&:id)

      collection = []
      # Only fetch container data from RMS if user can see containers UI.
      if GitHub.flipper[:container_registry_ui].enabled?(current_user)
        begin
          rms_packages_by_id = if rms_ids.empty?
            {}
          else
            PackageRegistry::Twirp.metadata_client.get_packages_metadata(user_id: current_user.id, package_ids: rms_ids)
            .map { |p| [p.id, p] }
            .to_h
          end
        rescue PackageRegistry::Twirp::BaseError
          rms_packages_by_id = {}
        end

        collection = package_ids.map do |p_id|
          id_as_int = p_id.sub(/rms-/, "").to_i
          p_id.start_with?("rms") ? rms_packages_by_id[id_as_int] : ar_packages_by_id[id_as_int]
        end.compact
      else
        collection = package_ids.map { |p_id| ar_packages_by_id[p_id.to_i] }.compact
      end

      return results, collection
    end

    private

    # Private: Filters a list of packages to those visible to a viewer.
    #
    # Params:
    #  * packages - an ActiveRecord relation of Registry::Package
    #  * viewer - a User, defaults to current_user
    #
    # Returns: a Array of packages that viewer can see.
    def filter_packages_by_visibility(packages, viewer: current_user)
      repo_ids = packages.pluck(:repository_id).uniq
      repos = Repository.with_ids(repo_ids)
      repo_visibilities = Promise.all(repos.map { |r| r.async_readable_by?(viewer) }).sync
      visible_repo_ids = Set.new(repos.to_a.select.with_index { |repo, i| repo_visibilities[i] }.map(&:id))

      packages.select { |p| visible_repo_ids.include?(p.repository_id) }
    end

    # Private: Are any of the query params present?
    #
    # Returns: Boolean
    def has_package_query_params?
      %w[ecosystem q visibility].any? { |p| params[p].present? }
    end

    # Private: Parses the ecosystem param and returns the string to be used
    # as the `package_type` param in Search::Queries::RegistryPackageQuery.
    # Returns nil for `all` since all package types are returned by default.
    #
    # Returns: String or nil
    def ecosystem_param
      raw = params[:ecosystem]&.downcase
      raw if QUERYABLE_ECOSYSTEMS.include?(raw)
    end

    # Private: Parses the visibility param and returns the string to be used
    # as the `visibility` param in Search::Queries::RegistryPackageQuery.
    #
    # Returns: String or nil
    def visibility_param
      raw = params[:visibility]&.downcase
      raw if QUERYABLE_VISIBILITIES.include?(raw)
    end

    # Private: Parses the sort_by param and returns the string to be used
    # as the `sort` param in Search::Queries::RegistryPackageQuery.
    #
    # Returns: [String] or nil
    def sort_param
      raw = params[:sort_by]&.downcase
      SORT_TO_QUERY_PARAM[raw]
    end
  end
end
