# frozen_string_literal: true

module RepositoriesHelper
  include PlatformHelper
  include RichwebHelper

  module CloneDownloadClick
    module FeatureClicked
      UNKNOWN = :UNKNOWN
      OPEN_IN_DESKTOP = :OPEN_IN_DESKTOP
      OPEN_IN_VISUAL_STUDIO = :OPEN_IN_VISUAL_STUDIO
      OPEN_IN_XCODE = :OPEN_IN_XCODE
      DOWNLOAD_ZIP = :DOWNLOAD_ZIP
      COPY_URL = :COPY_URL
      USE_SSH = :USE_SSH
      USE_HTTPS = :USE_HTTPS
      SHARE = :SHARE
      EMBED = :EMBED
    end
  end

  # How many stars a repo has to have before we show a message to the
  # owner suggesting they add a successor.

  STARGAZERS_THRESHOLD_FOR_SUCCESSOR_PROMPT = 1_000

  # Public: Get the URL to an image that should represent the given repository publicly.
  #
  # repo - a Repository
  #
  # Returns a String.
  def repository_open_graph_image_url(repo)
    if repo.public? && (custom_image = repo.open_graph_image)
      custom_image.storage_external_url(current_user)
    elsif repo.owner
      avatar_url_for(repo.owner, 400)
    else
      avatar_url_for(User.ghost, 400)
    end
  end

  # Public: Given a repository, determine if we should show a "generated from" link for that
  # repository that links back to the template from which it was cloned.
  #
  # repo - a Repository
  #
  # Returns a Boolean.
  def show_link_to_template_repository_for?(repo)
    return false unless template_repo = repo.template_repository
    return false if !logged_in? && !template_repo.public?

    template_repo.readable_by?(current_user)
  end

  # Public: Whether to show the "Use this template" button on a template repository,
  # so the viewer can clone the template to make a new repository.
  #
  # repo - the Repository being viewed
  #
  # Returns a Boolean.
  def show_use_this_template_button?(repo)
    repo.template? && logged_in?
  end

  def default_branch_settings_url_for(repo_owner)
    if repo_owner == current_user
      settings_user_repositories_path
    elsif repo_owner.organization?
      settings_org_repo_defaults_path(repo_owner)
    end
  end

  def show_default_branch_settings_link_for?(repo_owner)
    return true if logged_in? && repo_owner == current_user

    repo_owner.organization? && repo_owner.adminable_by?(current_user)
  end

  RepositoryTwitterImageFragment = parse_query <<-'GRAPHQL'
    fragment Repository on Repository {
      usesCustomOpenGraphImage
    }
  GRAPHQL

  LARGE_TWITTER_CARD_TYPE = "summary_large_image"
  SMALL_TWITTER_CARD_TYPE = "summary"

  def graphql_repository_twitter_image_card(repo)
    repo = RepositoryTwitterImageFragment::Repository.new(repo)

    if repo.uses_custom_open_graph_image?
      LARGE_TWITTER_CARD_TYPE
    else
      SMALL_TWITTER_CARD_TYPE
    end
  end

  # Public: Get the type of twitter:card that best suits the Open Graph image for the given
  # repository. See https://developer.twitter.com/en/docs/tweets/optimize-with-cards/overview/summary
  #
  # repo - a Repository
  #
  # Returns a String.
  def repository_twitter_image_card(repo)
    if repo.public? && repo.open_graph_image
      LARGE_TWITTER_CARD_TYPE
    else
      SMALL_TWITTER_CARD_TYPE
    end
  end

  # Public: Returns the String name of the currently selected language, properly capitalized.
  def get_selected_language(language)
    @all_language_names ||= Linguist::Language.all.map(&:name)
    index = @all_language_names.map(&:downcase).index(language)
    index ? @all_language_names[index] : "All"
  end

  def repo_meta_description
    title = current_repository.name_with_owner
    description = current_repository.description
    meta_description = I18n.t("repos.meta_description", repo: title)
    opengraph_description(title, description, meta_description)
  end

  # Determines whether the tree_name is:
  # - Branch
  # - Tag
  # - Undeterminable
  def tree_type
    candidate_names = ["refs/heads/#{tree_name}".b, "refs/tags/#{tree_name}".b]
    ref = current_repository.refs.find_all(candidate_names).find(&:present?)

    return "branch" if ref&.branch?
    return "tag" if ref&.tag?
    return "tree"
  end

  def clone_download_click_attributes(git_repo:, feature_clicked:)
    attrs = { feature_clicked: feature_clicked }

    if git_repo.is_a?(Repository)
      attrs[:git_repository_type] = :REPOSITORY
      attrs[:repository_id] = git_repo.id
    elsif git_repo.is_a?(Gist)
      attrs[:git_repository_type] = :GIST
      attrs[:gist_id] = git_repo.id
    end

    hydro_click_tracking_attributes("clone_or_download.click", attrs)
  end

  def open_in_desktop_tracking_attributes(git_repo)
    clone_download_click_attributes(
      git_repo: git_repo,
      feature_clicked: CloneDownloadClick::FeatureClicked::OPEN_IN_DESKTOP,
    )
  end

  def xcode_clone_button?
    logged_in? && current_user.xcode_app_enabled? && current_repository.xcode_project?
  end

  def open_in_xcode_tracking_attributes(git_repo)
    clone_download_click_attributes(
      git_repo: git_repo,
      feature_clicked: CloneDownloadClick::FeatureClicked::OPEN_IN_XCODE,
    )
  end

  def open_in_visual_studio_tracking_attributes(git_repo)
    clone_download_click_attributes(
      git_repo: git_repo,
      feature_clicked: CloneDownloadClick::FeatureClicked::OPEN_IN_VISUAL_STUDIO,
    )
  end

  def visual_studio_clone_button?
    logged_in? && current_user.visual_studio_app_enabled?
  end

  def protocol_toggler(protocol)
    feature_clicked = if protocol == "ssh"
      CloneDownloadClick::FeatureClicked::USE_SSH
    elsif protocol == "https"
      CloneDownloadClick::FeatureClicked::USE_HTTPS
    end

    data = if feature_clicked
      clone_download_click_attributes(git_repo: current_repository,
                                      feature_clicked: feature_clicked)
    else
      {}
    end

    button = button_tag("Use #{protocol.upcase}", type: "submit", data: data,
                        class: "btn-link f6 js-toggler-target float-right")

    if logged_in?
      protocol_type = current_repository.pushable_by?(current_user) ? "push" : "clone"
      protocol_path = user_set_protocol_path(protocol_selector: protocol,
                                             protocol_type: protocol_type)
      form_tag(protocol_path, "data-remote" => true) { button }
    else
      button
    end
  end

  RepositoryTypeOcticon = parse_query <<-'GRAPHQL'
    fragment on Repository {
      isFork
      isPrivate
      isMirror
      isTemplate
    }
  GRAPHQL

  RepositoryLabelFragment = parse_query <<-'GRAPHQL'
    fragment on Repository {
      isArchived
      isTemplate
      isMirror
      visibility
      isPrivate
    }
  GRAPHQL

  # Public: Get HTML to appropriately label the given GraphQL repository based on whether it's
  # private, a template, archived, internal, or a mirror.
  #
  # repo    - a Repository GraphQL object; be sure to include
  #           `...RepositoriesHelper::RepositoryLabelFragment` in your GraphQL query
  # classes - optional String of CSS classes to be used in addition to the normal label-styling CSS
  # tooltip - Boolean; indicates if a tooltip should be shown explaining what the label means
  #
  # Returns HTML or nil.
  def graphql_repository_label(repo, classes: nil, tooltip: false)
    repo = RepositoryLabelFragment.new(repo)

    repo_type = RepositoriesTypeHelper.type(
      visibility: repo.visibility,
      mirror: repo.is_mirror?,
      archived: repo.is_archived?,
      template: repo.is_template?,
    )

    label = if tooltip
      repository_tooltip_from(is_archived: repo.is_archived?, is_private: repo.is_private?)
    end

    repository_type_label_from(repo_type, classes: classes, tooltip: label)
  end

  # Public: Get HTML to appropriately label the given ActiveRecord repository based on whether it's
  # private, a template, archived, internal, or a mirror.
  #
  # repo    - a Repository
  # classes - optional String of CSS classes to be used in addition to the normal label-styling CSS
  # tooltip - Boolean; indicates if a tooltip should be shown explaining what the label means
  #
  #  Returns HTML or nil.
  def repository_label(repo, classes: nil, tooltip: false)
    repo_type = RepositoriesTypeHelper.type(
      visibility: repo.visibility,
      mirror: repo.mirror,
      archived: repo.archived?,
      template: repo.template?,
    )

    label = if tooltip
      repository_tooltip_from(is_archived: repo.archived?, is_private: repo.private?)
    end

    repository_type_label_from(repo_type, classes: classes)
  end

  # Public: Get a tooltip message for describing a repository with the given traits.
  #
  # is_archived - Boolean; whether repository has been archived or not
  # is_private  - Boolean; whether repository is private
  #
  # Returns a String or nil.
  def repository_tooltip_from(is_archived:, is_private:)
    if is_archived
      "Read-only."
    elsif is_private
      "Only visible to its members."
    end
  end

  # Public: Get HTML to label the specified repository type.
  #
  # classes - extra CSS classes, optional; String
  # tooltip - an optional String tooltip to show on hover
  #
  # Returns a String of HTML or nil.
  def repository_type_label_from(repo_type, classes: nil, tooltip: nil)
    if repo_type.present?
      content_tag :span, repo_type, class: "Label Label--outline v-align-middle #{classes}", title: tooltip
    end
  end

  # used for handling manifest file paths
  # shown in files/_dependency_alert and network/dependencies

  def short_manifest_file_path(full_manifest_path)
    parts = full_manifest_path.split("/")
    short_path = parts.last(2).join("/")
    short_path.prepend("…/") if parts.count > 2
    short_path
  end

  # used for generating anchored links to multiple manifests
  # within network/dependencies, files/_dependency_alert

  def link_to_network_dependencies_from_alerts(alerts, options = {})
    links_arr = alerts.group_by(&:vulnerable_manifest_path).map do |path, _alerts|
      link_to_network_dependencies_from_path(path, options)
    end

    html_safe_to_sentence(links_arr)
  end

  def link_to_network_dependencies_from_path(path, options = {})
    options[:title] = path

    link_to(short_manifest_file_path(path),
            network_dependencies_path(anchor: manifest_file_path_anchor(path)),
            options)
  end

  def manifest_file_path_anchor(path)
    UrlHelper.escape_path(path)
  end

  def instrument_marketplace_quick_install_view(user, listings)
    GitHub.instrument("marketplace.new_repo_quick_install") do |payload|
      payload[:current_user_id] = user.id
      payload[:action] = :viewed
      payload[:shown_listings] = listings.keys.map { |listing| { listing_id: listing.id } }
    end
  end

  # Public: Should a permissions option be disabled?
  #         We use this to disable an option if a user's current permission is
  #         more capable than the option.
  #
  # current_permission_level - The Integer or Float permission rank.
  # button_action - The Symbol action name for a button.
  #
  # Returns a Boolean.
  def permissions_option_disabled?(current_permission_level, button_action)
    current_permission_level >= Ability::ACTION_RANKING[button_action]
  end

  # Public: Hide the used by section if the fragment has already been cached
  # as an empty html response, which means it has no package associations
  # i.e. the repository is not a package
  def hide_used_by_section?
    cached_fragment = read_fragment(used_by_sidebar_cache_key)
    !cached_fragment.nil? && cached_fragment.blank?
  end

  # Public: Show the billing warning for actions if the owner cannot access
  # actions for billing reasons
  #
  # owner - owner for the purposes of billing
  # current_user - the current user
  def render_actions_billing_warning_if_required(owner:, current_user:)
    if !Billing::ActionsPermission.new(owner).allowed?(public: current_repository.public?)
      render_partial_view "repository_actions/billing_warning", RepositoryActions::BillingWarningView,
        owner: owner,
        current_user: current_user,
        current_repository: current_repository
    end
  end

  def show_billing_notice_when_archiving?(repository:)
    GitHub.billing_enabled? &&
      repository.private? &&
      !repository.fork? &&
      !repository.part_of_unlimited_plan?
  end

  def sidebar_contributors_cache_key
    "sidebar:v3:contributors:list:#{current_repository.id}:#{current_repository.updated_at.to_i}"
  end

  # Public indicates whether a prompt to add a successor should be shown
  #
  # repository: This is the repository being viewed
  #
  # Returns a Boolean
  def show_add_successor_prompt_for_popular_repos?(repository)
    return false if GitHub.enterprise?
    return false unless logged_in?
    return false unless GitHub.flipper[:successor_prompt].enabled?(current_user)
    return false unless repository.owner.user?
    return false unless repository.owner == current_user

    repository.stargazer_count > STARGAZERS_THRESHOLD_FOR_SUCCESSOR_PROMPT &&
        !current_user.dismissed_repository_notice?("popular_repo_successor_prompt", repository_id: repository.id) &&
        !current_user.has_successor?
  end
end
