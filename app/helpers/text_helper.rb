# frozen_string_literal: true

require "erb"
require "html_truncator"

module TextHelper
  include GitHub::HTML
  include Scientist

  # Default context hash for HTML filters.
  def default_html_filter_context
    {
      base_url: base_url,
      entity: current_repository,
    }
  end

  # Internal: Set up the context hash for the SimplePipeline
  #
  # These values are necessary for the RelativeLinkFilter
  # (see lib/github/html/relative_link_filter.rb), which is
  # sometimes used in places other than the web, where all
  # these values and classes are present.
  #
  # Returns a hash with :path, :committish, and :view keys
  def basic_html_context
    path = committish = view = nil

    begin
      path = path_string
      committish = tree_name

      if @view_name
        view = @view_name
      else
        view = case controller
        when TreeController, FilesController
          :tree
        when BlobController
          :blob
        end
        view = :preview if params[:action] == "preview"
      end
    rescue NameError, NoMethodError  # if these classes or methods don't exist
      path = committish = view = nil
    end

    { path: path, committish: committish, view: view }
  end
  private :basic_html_context

  def github_flavored_markdown(text, context = {})
    markdown_pipeline(text, { cache: true }.merge(context))
  end

  # Render HTML from Markdown input. Supports a subset of GitHub Flavored
  # Markdown, excluding task lists and linked mentions.
  #
  #   text - The raw Markdown text as a String
  #   context - HTML filter context hash
  #
  # Returns formatted HTML String.
  def github_simplified_markdown(text, context = {})
    return "" if text.nil?

    if !text.valid_encoding? || text.encoding != Encoding::UTF_8
      text = text.dup.force_encoding("utf-8").scrub!
    end

    markdown = CommonMarker.render_html(text, [:UNSAFE, :GITHUB_PRE_LANG], %i[tagfilter table strikethrough autolink])
    GitHub::HTML::SimplePipeline.to_html(markdown, context)
  end

  # Render HTML from Markdown input. Supports a subset of GitHub Flavored
  # Markdown, excluding task lists and linked mentions.
  #
  #   text - The raw Markdown text as a String
  #   context - HTML filter context hash
  #
  # Returns formatted HTML String.
  def github_card_markdown(text, context = {})
    return "" if text.nil?

    context = default_html_filter_context.merge(context)
    GitHub::Goomba::CardPipeline.to_html(text, context)
  end

  # Process markdown input with our default Markdown pipeline filters. This
  # includes running basic filters for sanitization, image replacement, and
  # mentions.
  #
  #   text - The raw Markdown text as a String
  #   context - HTML filter context hash
  #     cache: whether to cache text result (default: false)
  #     gfm: whether GFM is enabled (default: true)
  #
  # Returns formatted HTML String.
  #
  # See Also
  #   https://help.github.com/articles/github-flavored-markdown/
  def markdown_pipeline(text, context = {})
    return "" if text.nil?

    process = -> {
      context = default_html_filter_context.merge(context)
      GitHub::Goomba::MarkdownPipeline.to_html(text, context)
    }

    if context.fetch(:cache, false)
      key = [
        context.fetch(:gfm, true) ? "gfm" : "mkd",
        text.to_md5,
        "v3",
      ].join(":")

      value = GitHub.cache.fetch(key, &process)
      value
    else
      process.call
    end
  end

  # Sanitize HTML. See GitHub::HTML::SanitizationFilter for
  # more advanced usage.
  #
  #   html      - String or DocumentFragment to filter
  #   whitelist - Sanitize whitelist configuration hash
  #
  # Returns the sanitized HTML as a DocumentFragment.
  def sanitize_filter(html, whitelist = nil)
    SanitizationFilter.call(html, whitelist: whitelist)
  end

  # Strip all tags from the input document.
  #
  # Returns a HTML string. Note that the result is NOT html_safe since it may
  # contain reserved HTML characters.
  def strip_tags(html)
    sanitize_filter(html, SanitizationFilter::FULL).inner_text
  end

  # Strip all tags from the input document and collapse all whitespace into
  # single spaces.
  #
  # Returns a HTML string. Note that, like #strip_tags, above, the result
  # is NOT html_safe since it may contain reserved HTML characters.
  def strip_tags_and_collapse_whitespace(html)
    strip_tags(html).gsub(/\s+/, " ").strip
  end

  # Truncate HTML to max visible characters.
  #
  # html - String HTML or a Nokogiri container node.
  # max  - Maximum number of visible characters to include in the result.
  #
  # Returns the truncated HTML as a String. The markup is guaranteed to have no
  # block elements.
  def truncate_html(html, max)
    HTMLTruncator.new(html, max).to_html(wrap: false)
  end

  # Like link_to but handles markup that may include other <a> tags. When
  # existing <a> tags are detected, all text nodes not already contained in
  # an <a> tag are wrapped in a link to url.
  #
  # If this is rendering a commit message we iterate over the links in it and
  # make sure that links that are URLs within the commit message are updated to
  # point to the commit instead of the URL. We do this in a way that preserves
  # any links that are references to issue numbers of mentions but also handles
  # long URLs that have been truncated.
  #
  # html  - The HTML markup to linkify as a String or DocumentFragment. May
  #         include other <a> tags.
  # url   - The URL destination of the link.
  # attrs - Additional attributes to add to the <a> tag.
  #
  # Returns a String with HTML markup.
  BOOSH = GitHub::HTMLSafeString.make("<<BOOSH>>")
  def link_markup_to(html, url, attrs = {})
    url = url.to_s
    if html.is_a?(String) && !html.include?("<a ")
      link_to(html.html_safe, url, attrs) # rubocop:disable Rails/OutputSafety
    else
      doc = GitHub::HTML.parse(html)
      if doc.at("a")
        ellipses_length = 3
        template = link_to(BOOSH, url, attrs)
        doc.xpath("descendant::text()").each do |node|
          if attrs[:class] == "message"
            anchor_text = doc.search("a").xpath("text()").text
            doc.css("a").each do |link|
              link["href"] = url if link["href"][0..anchor_text.length - ellipses_length] == anchor_text[0..-ellipses_length]
            end
          end
          next if node.xpath("ancestor::a[1]").any?
          linked_text = link_markup_text_node(node, template)
          node.replace(linked_text)
        end
        doc.to_html.html_safe # rubocop:disable Rails/OutputSafety
      else
        link_to(html.to_html.html_safe, url, attrs) # rubocop:disable Rails/OutputSafety
      end
    end
  end

  # Internal: Convert a single text node to a link excluding any leading and
  # trailing space characters.  /cc https://github.com/github/github/issues/8176
  #
  # node     - A nokogiri text node object or a string.
  # template - The link markup template. This is just a string with an <a>
  #            that's wrapped around the text node given.
  #
  # Returns a string of HTML markup.
  def link_markup_text_node(node, template)
    text = node.to_s
    return text if text.strip.empty?
    pre, mid, post = text.match(/\A([ \t\n]+)?(.*?)([ \t\n]+)?\z/m).to_a[1..3]
    mid = template.sub("<<BOOSH>>") { mid.to_s }
    [pre, mid, post].compact.join("")
  end

  def formatted_repo_description(repository)
    formatted_description_string(repository.description)
  end

  def formatted_description_string(description)
    formatted = GitHub::HTML::DescriptionPipeline.to_html(description)
    HTMLTruncator.new(formatted, 350).to_html(wrap: false)
  end

  # Formats numbers less than nine into words
  def number_to_words(number)
    return number if number > 9
    @number_to_words ||= { 0 =>"zero", 1 =>"one",  2 =>"two", 3 =>"three", 4 =>"four",
                           5 =>"five", 6 =>"six", 7 =>"seven", 8=>"eight", 9 =>"nine" }
    @number_to_words[number]
  end

  # Converts an array of strings into a comma-separated string ready for use in
  # a sentence, where each of the individual items is bolded and prevented from
  # wrapping onto multiple lines.
  #
  # items          - An array of strings.
  # truncate_after - The maximum number of items to be displayed, after which
  #                  "and more" will be added.
  #
  # Returns a string of HTML markup.
  def featured_items_string(items, truncate_after: nil)
    items.map! { |item| content_tag(:strong, item, class: "no-wrap") }

    if truncate_after && items.length > truncate_after
      to_sentence(items[0..truncate_after-1] + ["more"])
    else
      to_sentence(items)
    end
  end

  alias_method :formatted_gist_description, :formatted_repo_description

  def strip_tags_inlining_urls(html)
    # Rails' FullSanitizer is basically what we want, but it does not allow us
    # to provide a custom scrubber. (And we'd rather not implement our own
    # sanitizer just to customize the scrubber.) However, we don't have static
    # access to the sanitizer used by the `sanitize` helper method. So
    # instead, we're resolved to grabbing the safe_list_sanitizer directly
    # from the rails-html-sanitizer gem and hoping it continues to be the one
    # that Rails (and GitHub) uses for `sanitize` under the hood.
    Rails::Html::Sanitizer.safe_list_sanitizer.new.sanitize html,
      scrubber: GitHub::TextOnlyWithUrlsScrubber.new
  end
  module_function :strip_tags_inlining_urls
end
