# frozen_string_literal: true

module PasswordStrengthHelper
  # Example sentence
  # Make sure it's at least 15 characters OR at least 8 characters including a number and a lowercase letter. Learn more.

  DOCUMENTATION_LINK =  GitHub::HTMLSafeString.make %Q[<a href="#{GitHub.help_url}/articles/creating-a-strong-password" class="tooltipped tooltipped-s" aria-label="Learn more about strong passwords">Learn more</a>.]
  HUBBER_DOCUMENTATION_LINK = GitHub::HTMLSafeString.make %Q[Read the documentation on <a href="https://githubber.com/category/handbook/for-everyone#passwords-and-software-patching">GitHubber for creating a strong password</a>.]
  LETTER_REQUIREMENT = "and a lowercase letter"
  MINIMUM_VALID_CHARACTERS = GitHub.password_minimum_length
  MORE_THAN_N_CHARACTERS = "at least #{User::PASSPHRASE_LENGTH} characters"
  NUMBER_REQUIREMENT = "including a number"

  def password_strength_sentence(user = nil)
    message = ["Make sure it's"]

    unless user&.enhanced_employee_password_requirements?
      message << tag.span(MORE_THAN_N_CHARACTERS, "data-more-than-n-chars": "")
      message << "OR"
    end
    message << tag.span(minimum_password_characters(user), "data-min-chars": "")
    message << tag.span(NUMBER_REQUIREMENT, "data-number-requirement": "")
    message << (tag.span(LETTER_REQUIREMENT, "data-letter-requirement": "") + ".")

    message << if !GitHub.enterprise? && user&.enhanced_employee_password_requirements?
      HUBBER_DOCUMENTATION_LINK
    else
      DOCUMENTATION_LINK
    end

    safe_join(message, " ")
  end

  def minimum_password_character_count(user = nil)
    if user&.enhanced_employee_password_requirements?
      GitHub.employee_password_minimum_length
    else
      MINIMUM_VALID_CHARACTERS
    end
  end

  def minimum_password_characters(user = nil)
    "at least #{minimum_password_character_count(user)} characters"
  end
end
