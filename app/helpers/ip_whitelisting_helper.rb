# frozen_string_literal: true

module IpWhitelistingHelper
  # Public: Returns the HTML hint that's used when a request comes from an IP
  # that is not allowed to access an account, and we want to tell the user that
  # they need to connect from an allowed IP address to access the resource.
  #
  # target - An Organization or Business representing the owner of an IP allow list.
  # text - A String describing the action being attempted by the user.
  # join_word - A String to join the text argument with the text representation of the target.
  #
  # Returns String.
  def restricted_ip_whitelisting_target_hint(target, text: "", join_word: "within")
    safe_join([
      "Connect from an allowed IP address",
      text,
      (target.is_a?(Business) ? "for organizations #{join_word} the" : "#{join_word} the"),
      content_tag(:strong, target.is_a?(Business) ? target.name : target),
      (target.is_a?(Business) ? "enterprise." : "organization."),
    ], " ")
  end
end
