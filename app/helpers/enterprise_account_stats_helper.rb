# frozen_string_literal: true

module EnterpriseAccountStatsHelper
  # Helpers for tracking SLO stats of Enterprise Account-related
  # controllers/action (under pe_admin_experience AoR), tracked in
  # https://app.datadoghq.com/dashboard/asm-qn5-43g/

  def register_enterprise_account_controller_stats
    real_start = realtime
    yield
  ensure
    real_ms = (realtime - real_start) * 1_000
    GitHub.dogstats.increment("enterprise.request")
    GitHub.dogstats.timing("enterprise.request.time", real_ms)
  end

  private

  def realtime
    Process.clock_gettime(Process::CLOCK_MONOTONIC)
  end
end
