# frozen_string_literal: true

module FundingLinksHelper
  def funding_platform_icon(platform, size: 16)
    if platform == "custom"
      octicon("link", class: "text-gray", alt: platform)
    else
      path = "modules/site/icons/funding_platforms/#{platform}.svg"
      image_tag(path, width: size, height: size, class: "octicon rounded-1 d-block", alt: platform)
    end
  end

  def funding_link(name, url_or_account)
    platform = FundingPlatforms.find(name)
    analytics_attrs = funding_links_attrs(name, url_or_account)
    link_options = { target: "_blank" }.merge(data: analytics_attrs)

    if url = platform.url
      account = url_or_account
      content_tag(:a, link_options.merge(href: "#{url}#{account}")) do
        content_tag(:span) do
          safe_join(["#{url.host}#{url.path}", content_tag(:strong, account)])
        end
      end
    else
      url = url_or_account
      safe_link_to(url, url, link_options)
    end
  end

  def funding_button_attrs
    hydro_click_tracking_attributes(
      "sponsors.repo_funding_links_button_click",
      platforms: current_repository&.funding_links_to_hydro,
      repo_id: current_repository&.id,
      owner_id: current_repository&.owner_id,
      user_id: current_user&.id,
      is_mobile: false)
  end

  def funding_links_attrs(platform_name, account)
    platform = FundingPlatforms.find(platform_name)

    hydro_attributes = hydro_click_tracking_attributes(
      "sponsors.repo_funding_links_link_click",
      platform: FundingPlatforms.to_hydro(platform, account),
      platforms: current_repository&.funding_links_to_hydro,
      repo_id: current_repository&.id,
      owner_id: current_repository&.owner_id,
      user_id: current_user&.id,
    )

    {"ga-click" => "Dashboard, click, Nav menu - item:org-profile context:organization"}
      .merge(hydro_attributes)
  end

  def global_preferred_funding_path(repository)
    preferred_file_path(type: :funding, repository: repository.global_preferred_funding.repository)
  end
end
