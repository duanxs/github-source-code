# frozen_string_literal: true

module CommentsHelper
  include PlatformHelper
  include ActionView::Helpers::DateHelper

  DISCUSSION_DOM_ID_PREFIX = "discussion_r"
  COMMIT_COMMENT_DOM_ID_PREFIX = "r"

  CommentFragment = parse_query <<-'GRAPHQL'
    fragment on Comment {
      ... on Issue {
        databaseId
      }

      ... on IssueComment {
        databaseId
      }

      ... on PullRequest {
        databaseId
      }

      ... on PullRequestReview {
        databaseId
      }

      ... on PullRequestReviewComment {
        databaseId
      }

      ... on CommitComment {
        databaseId
      }

      ... on GistComment {
        databaseId
      }

      ... on TeamDiscussion {
        databaseId
      }

      ... on OrganizationDiscussion {
        databaseId
      }

      ... on RepositoryAdvisory {
        databaseId
      }

      ... on RepositoryAdvisoryComment {
        databaseId
      }
    }
  GRAPHQL

  # Public: Render the new comment form.  Used for the textarea for things like
  # new Issues/PRs, adding a reply to line comment threads, and commenting on Commits.
  #
  # Note that we use an IssueComment here as a "stand in" model to make
  # the form work - but this helper is used for all sorts of comment models, not
  # just IssueComments.
  def previewable_comment_form(body = "", opts = {})
    comment = if opts[:comment_class]
      opts[:comment_class].new
    else
      IssueComment.new
    end
    comment.body = body || ""
    comment.created_at = Time.now
    comment.user = current_user
    opts.reverse_merge!(comment: comment, repository: current_repository, required: true)

    @previewable_comment_id ||= "comment_body_0".dup
    opts[:textarea_id] ||= @previewable_comment_id.succ!

    # FIXME: pass opts to both view model and as locals since the partial hasn't
    # been updated to use the view model instead.
    view = create_view_model Comments::ShowView, opts

    locals = opts.merge(
      view: view,
      asset_types: [:assets, :"repository-files"],
      allows_suggested_changes: allows_suggested_changes(opts),
    )

    render partial: "comments/previewable_comment_form", locals: locals
  end

  def comment_dom_id(comment)
    id = comment.id || comment.object_id
    return "#{DISCUSSION_DOM_ID_PREFIX}#{id}" if comment.is_a?(PullRequestReviewComment)
    return "#{COMMIT_COMMENT_DOM_ID_PREFIX}#{id}" if comment.is_a?(CommitComment)

    "#{comment.class.name.downcase}-#{id}"
  end

  def graphql_comment_dom_id(comment)
    comment = CommentFragment.new(comment)
    return "#{DISCUSSION_DOM_ID_PREFIX}#{comment.database_id}" if comment.is_a?(PlatformTypes::PullRequestReviewComment)
    return "#{COMMIT_COMMENT_DOM_ID_PREFIX}#{comment.database_id}" if comment.is_a?(PlatformTypes::CommitComment)

    "#{comment.__typename.downcase}-#{comment.database_id}"
  end

  def emoji_suggestions_cache_key(use_colon_emoji:, tone:)
    base_key = "emoji-suggestions:aliases:v16:#{tone || 0}"
    use_colon_emoji ? base_key + ":use_colon_emoji" : base_key
  end

  def allows_suggested_changes(opts = {})
    opts.fetch(:allows_suggested_changes) do
      existing_pull = @pull && !@pull.new_record? && @pull.open? && @pull.head_ref_exist?
      existing_pull && (defined?(specified_tab) ? specified_tab == "files" : true)
    end
  end

  def notifiable_block?
    current_repository.owner.is_a?(Organization) && org_block.try(:blocked_from_content)
  end

  # Note that `graphql_blocked_from_commenting?`, below, is the GraphQL equivalent to this
  # and a change in one should be made to the other.
  def blocked_from_commenting?(obj)
    return false if current_repository.private?
    return false if obj.nil?
    return false if current_repository.pushable_by?(current_user)
    blocked_by_owner? || blocked_by_author?(obj.user)
  end

  def blocked_by_owner?(owner_id = current_repository.try(:owner_id))
    return false if !owner_id || !logged_in? || current_user.id == owner_id
    current_user.blocked_by?(owner_id)
  end

  def blocked_by_author?(author)
    logged_in? && current_user.blocked_by?(author)
  end

  BlockedFromCommentingFragment = parse_query <<-'GRAPHQL'
    fragment BlockStatus on IssueOrPullRequest {
      ... on Commentable {
        viewerBlockedByAuthor
      }
      ... on Issue {
        repository {
          isPrivate
          viewerBlockedByOwner
          viewerCanPush
        }
      }
      ... on PullRequest {
        repository {
          isPrivate
          viewerBlockedByOwner
          viewerCanPush
        }
      }
    }
  GRAPHQL

  # This is bad and I feel bad. But it's here so it can be co-located with the equivalent
  # ActiveRecord `blocked_from_commenting?` method above. When all of that method's callers
  # have been converted to GraphQL, we should consider making this a field on the `Commentable`
  # platform interface that `Issues` and `PullRequests` implement.
  def graphql_blocked_from_commenting?(issueish)
    return false if issueish.nil?
    issueish = BlockedFromCommentingFragment::BlockStatus.new(issueish)
    return false if issueish.repository.is_private?
    return false if issueish.repository.viewer_can_push?
    issueish.repository.viewer_blocked_by_owner || issueish.viewer_blocked_by_author
  end

  def blocked_from_commenting_class
    if notifiable_block?
      "text-red"
    else
      ""
    end
  end

  def blocked_from_commenting_octicon
    if notifiable_block?
      "circle-slash"
    else
      "lock"
    end
  end

  def blocked_from_commenting_message
    message = []
    if notifiable_block?
      if org_block.expires_at
        message << "You are blocked for #{distance_of_time_in_words_to_now(org_block.expires_at)} from the #{current_repository.owner.name} organization and cannot comment"
      else
        message << "You are blocked from the #{current_repository.owner.name} organization and cannot comment"
      end

      if org_block.blocked_from_content
        message << " because of "
        message << link_to("this content", org_block.blocked_from_content.async_path_uri.sync.to_s)
      end

      # Right now a CoC is returned even if a repo doesn't have one, we check on the url for
      # a truthy value
      if current_repository.code_of_conduct.url
        message << ". Please read the community's "
        message << link_to("code of conduct", current_repository.code_of_conduct.url.to_s)
      end
    else
      message << "You can't perform this action at this time"
    end
    message << "."
    safe_join(message)
  end

  def blocked_notification_html(org_login, org_email, content_url, coc_url)
    notification = ["A maintainer of the "]
    notification << content_tag(:b, "@#{org_login}")
    notification << " organization has blocked you because of "
    notification << link_to("this content", content_url.to_s)
    notification << "."

    info = []

    notification << " For more information please see "

    if coc_url
      info << link_to("the code of conduct", coc_url.to_s)
    else
      info << link_to("the community guidelines", "#{GitHub.help_url}/articles/github-community-guidelines")
    end

    unless org_email.blank?
      contact = ["contact the maintainer at "]
      contact << mail_to(org_email)

      info << safe_join(contact)
    end

    # We have used safe_join above for all these lines
    # html_safe is fine here
    notification << info.to_sentence(two_words_connector: " or ", last_word_connector: ", or ").html_safe # rubocop:disable Rails/OutputSafety
    notification << "."

    safe_join(notification)
  end

  def org_block
    @block ||= IgnoredUser.where(user_id: current_repository.owner.id, ignored_id: current_user.id).first
  end

  def minimize_reasons_for_select
    if GitHub.enterprise?
      ghe_reasons = Platform::Enums::ReportedContentClassifiers.values.select do |k, v|
        v.environment_visibilities[:enterprise].any?
      end
      minimize_reasons = ghe_reasons.keys
    else
      minimize_reasons = Platform::Enums::ReportedContentClassifiers.graphql_values
    end
    minimize_reasons.map { |s| [s.titleize, s] }
  end

  def abuse_report_tooltip(reports_count, report_reason, last_reported)
    if report_reason.blank?
      "#{pluralize(reports_count, "user")} reported #{time_ago_in_words(last_reported)} ago"
    else
      "#{pluralize(reports_count, "user")} reported as #{report_reason} #{time_ago_in_words(last_reported)} ago"
    end
  end

  def reaction_popover_class(popover_direction)
    popover_offset = ""

    case popover_direction
    when "sw"
      popover_offset = "mr-n1 mt-n1"
    when "ne"
      popover_offset = "ml-2 mb-0"
    end

    "dropdown-menu-#{popover_direction} #{popover_offset}"
  end
end
