# frozen_string_literal: true

module CompareHelper

  # Return an array with several 'sample comparisons' that could be
  # useful to the user as a starting point.
  #
  # We use a pretty simple heuristic: return the newest 20 branches,
  # based on the time of their newest commit, prioritizing them by
  # whether they have Pull Requests or not (we want branches without
  # pull requests, if possible, because they make a better example).
  #
  # This is cached by GitRPC in 1h intervals, but regardless should
  # be pretty fast.
  def sample_comparisons(repository)
    base_tree = repository.default_branch
    interesting = repository.rpc.interesting_branches(base_tree, 5)

    interesting.collect do |timestamp, _, ref|
      {
        label: ref.try { |r| r.force_encoding("utf-8").scrub! },
        range: "#{base_tree}...#{ref}".dup.force_encoding("utf-8"),
        pushed_at: Time.at(timestamp),
      }
    end
  end

  # Public: Generate label for comparison base.
  #
  # If the comparison is cross repository, then the full label including
  # username is shown, "<user>:<ref>". Otherwise just the "<ref>" itself.
  #
  # comparison - A Comparison
  #
  # Returns String.
  def comparison_base_ref_label(comparison)
    if comparison.cross_repository?
      comparison.display_qualified_base_revision
    else
      comparison.display_base_revision
    end
  end

  # Public: Generate label for comparison head.
  #
  # If the comparison is cross repository, then the full label including
  # username is shown, "<user>:<ref>". Otherwise just the "<ref>" itself.
  #
  # comparison - A Comparison
  #
  # Returns String.
  def comparison_head_ref_label(comparison)
    if comparison.cross_repository?
      comparison.display_qualified_head_revision
    else
      comparison.display_head_revision
    end
  end

  # Public: Generate comparison path with base repository.
  #
  # comparison - A Comparison
  # repository - A new base Repository
  #
  # Returns String path.
  def base_repo_comparison_path(comparison, repository, expand: false)
    base = [repository.user.login, comparison.base_ref].join(":")
    dots = comparison.direct_compare? ? ".." : "..."
    compare_path(comparison.repo, "#{base.try(:b)}#{dots}#{comparison.head.try(:b)}", expand)
  end

  # Public: Generate comparison path with head repository.
  #
  # comparison - A Comparison
  # repository - A new head Repository
  #
  # Returns String path.
  def head_repo_comparison_path(comparison, repository, expand: false)
    head = [repository.user.login, comparison.head_ref].join(":")
    dots = comparison.direct_compare? ? ".." : "..."
    compare_path(comparison.repo, "#{comparison.base.try(:b)}#{dots}#{head.try(:b)}", expand)
  end

  # Public: Generate comparison path with new base ref name.
  #
  # comparison - A Comparison
  # base - New String base ref name
  #
  # Returns String path.
  def base_ref_comparison_path(comparison, base, expand: false)
    if comparison.base.to_s.include?(":")
      base = [comparison.base_user_login, base].join(":")
      base = base.b if base
    end

    head = comparison.head.b if comparison.head
    dots = comparison.direct_compare? ? ".." : "..."
    compare_path(comparison.repo, "#{base}#{dots}#{head}", expand)
  end

  # Public: Generate comparison path with new head ref name.
  #
  # comparison - A Comparison
  # head - New String base ref name
  #
  # Returns String path.
  def head_ref_comparison_path(comparison, head, expand: false)
    if comparison.head.to_s.include?(":")
      head = [comparison.head_user_login, head].join(":")
      head = head.b if head
    end

    base = comparison.base.b if comparison.base
    dots = comparison.direct_compare? ? ".." : "..."
    compare_path(comparison.repo, "#{base}#{dots}#{head}", expand)
  end

  # Generate a user-readable string explaining the ahead-behind
  # relationship between two branches.
  #
  # Examples (note the impeccable use of English grammar):
  #
  # compare_ahead_behind_text(4, 0, base_branch: 'foo')
  # #=> '4 commits ahead of foo'
  #
  # compare_ahead_behind_text(4, 2, base_branch: 'foo')
  # #=> '5 commits ahead, 2 commits behind foo'
  #
  # compare_ahead_behind_text(0, 2, base_branch: 'foo')
  # #=> '2 commits behind foo'
  #
  # compare_ahead_behind_text(0, 0, base_branch: 'foo')
  # #=> 'even with foo'
  #
  # Returns a String.
  def compare_ahead_behind_text(ahead, behind, base_branch: nil)
    trailer = "".dup

    message =
      if ahead.nil? || behind.nil?
        ""
      elsif ahead == 0 && behind == 0
        trailer << " with #{base_branch}" if base_branch
        "even" + trailer
      else
        if base_branch
          trailer << " of" if behind == 0
          trailer << " #{base_branch}"
        end

        chunks = []
        chunks << "#{ahead} #{'commit'.pluralize(ahead)} ahead" if ahead > 0
        chunks << "#{behind} #{'commit'.pluralize(behind)} behind" if behind > 0

        chunks.join(", ") + trailer
      end

    message.dup.force_encoding("utf-8").scrub!
  end

  # Generate a user-readable string explaining the ahead-behind
  # relationship for a comparison.
  #
  # comparison - A Comparison object
  # base_branch - Optional String base branch (default: comparison's base ref)
  #
  # Returns a String.
  def comparison_ahead_behind_text(comparison, base_branch: nil)
    behind, ahead = comparison.relationship
    compare_ahead_behind_text(ahead, behind, base_branch: base_branch || comparison.base)
  end

  def render_tabs_on_compare?(comparison)
    comparison.common_ancestor? && comparison.commits.size > 20
  end
end
