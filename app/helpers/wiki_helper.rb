# frozen_string_literal: true

module WikiHelper
  def wiki_data(page, context = nil)
    if context.is_a?(String)
      cache_key = [
        page.cache_key,
        context,
        no_follow?,
        wiki_prefix_on_relative_links?,
      ].join(":")

      cached = GitHub.cache.get(cache_key)
      return cached unless cached.nil?

      data = wiki_data!(page)
      expires_in = data.errored?? 30.minutes : 0
      GitHub.cache.set(cache_key, data, expires_in)
      data
    else
      wiki_data!(page)
    end
  end

  def wiki_data!(page)
    page.data_html({
      no_follow: no_follow?,
      prefix_relative_links: wiki_prefix_on_relative_links?,
    })
  end

  def no_follow?
    params.key?(:version)
  end

  # Relative links break when viewing the Home page (like /github/github/wiki).
  # This will tell the HTML Pipeline to prefix all relative links with "wiki/"
  # if we're viewing from the home page.
  def wiki_prefix_on_relative_links?
    request.path == wikis_path
  end

  def wiki_preview?
    if @wiki_preview.nil?
      @wiki_preview = @page.name == "_Preview"
    end
    @wiki_preview
  end

  def writable_wiki?
    return false unless current_repository_writable?
    if @writable_wiki.nil?
      @writable_wiki = current_repository.wiki_writable_by?(current_user) || false
    end
    @writable_wiki
  end

  # Displays the title of a GitHub::Unsullied::Page. If this is a preview, append a
  # 'preview' suffix to the real wiki name.  Don't worry about sanitizing
  # the title.  Wiki pages with no H1 are limited by supported characters,
  # and titles extracted from H1 are cleaned by the Sanitize gem.
  #
  # page - Optional GitHub::Unsullied::Page instance.  Taken from @page in the controller
  #        by default.
  #
  # Returns the String title for an HTML page.
  def wiki_page_title(page = @page)
    title = page.name == "_Preview" ?
      "#{params[:wiki][:name]} (Preview)" :
      page.title.to_s.dup
    title.strip!
    title
  end

  # Builds a GUID fit for a wiki atom feed.
  #
  # page   - A GitHub::Unsullied::Page instance.
  # commit - A ::Commit describing the commit the page was last updated.
  #
  # Returns a String GUID.
  def wiki_guid(page, commit = page.revision_oid)
    "http#{:s if GitHub.ssl}://%s%s/%s" %
      [GitHub.host_name, wiki_page_path(page), commit]
  end

  # Display wiki help by default if a user is logged in but has never dismissed
  # this help before.
  def autodisplay_wiki_help?
    logged_in? && !current_user.dismissed_notice?("wiki_help")
  end

  def wiki_page_deletable?(page)
    page.name !~ /^home$/i
  end
end
