# frozen_string_literal: true

module OrganizationsHelper
  include ExperimentHelper
  include Scientist
  include PlatformHelper
  include IntegrationManagerHelper
  include BillingSettingsHelper

  # How many stars a repo has to have before we show a message to the last
  # org admin suggesting they add other admins.
  STARGAZERS_THRESHOLD_FOR_SUCCESSOR_PROMPT = 1_000

  MUNICH_TRANSITION_DATE = Time.new(2020, 04, 14)

  # Public: Set the header title to show on smaller screens based on the given Organization.
  #
  # org - an Organization
  def org_header_title(org)
    content_for :header_title do
      link_to(org, user_path(org), class: "Header-link")
    end
  end

  def this_organization_description
    return @this_organization_description if defined?(@this_organization_description)
    @this_organization_description = h(
      GitHub::Goomba::ProfileBioPipeline.to_html(this_organization.profile_bio, {}),
    )
  end

  def this_organization_meta_description
    return @this_organization_meta_description if defined?(@this_organization_meta_description)
    title = this_organization.safe_profile_name
    description = strip_tags(this_organization_description).strip
    meta_description = I18n.t("profiles.meta_description",
      username: title, count: this_organization.public_repositories.count)
    @this_organization_meta_description = h(
      opengraph_description(title, description, meta_description),
    )
  end

  def self.included(base)
    return unless base.respond_to? :helper_method
    base.helper_method :current_organization, :current_team, :current_context
    base.helper_method :org_admin?
  end

  def display_sso_notice?(organization, user)
    return false if organization.nil?
    saml_sso_banner = User::ORGANIZATION_NOTICES[:saml_sso_banner]
    organization.async_notices_for(viewer: user).sync.include?(saml_sso_banner)
  end

  def display_organization_notice?(organization, user, notice_key)
    return false if organization.nil?
    return false unless User::ORGANIZATION_NOTICES.key?(notice_key)
    notice = User::ORGANIZATION_NOTICES[notice_key]
    !user.dismissed_organization_notice?(notice, organization)
  end

  def show_appoint_successor_prompt_for?(repository:, organization: nil)
    return false if GitHub.enterprise?
    return false unless logged_in?
    return false unless GitHub.flipper[:successor_prompt].enabled?(current_user)

    organization ||= repository.owner
    notice = User::ORGANIZATION_NOTICES[:add_successor_prompt]

    repository.stargazer_count > STARGAZERS_THRESHOLD_FOR_SUCCESSOR_PROMPT &&
      !current_user.dismissed_organization_notice?(notice, organization) &&
      organization.last_admin?(current_user)
  end

  #
  # Filters
  #

  # Routes using this filter are available only to members of the
  # current organization.
  def org_members_only
    render_404 if current_organization.nil?
  end

  # Routes using this filter are available only to admins of the
  # current organization.
  def org_admins_only
    render_404 unless org_admin?
  end

  # Routes using this filter are available only to org admins or billing managers
  # of the current organization.
  def org_billing_management_only
    render_404 unless org_billing_manageable?
  end

  def custom_roles_enabled?
    current_organization.plan_supports?(:custom_roles) && GitHub.flipper[:custom_roles].enabled?(current_organization)
  end

  #
  # Controller & View Helpers
  #

  # The current context is a user when viewing a user dashboard,
  # organization when viewing an org dashboard.
  def current_context
    current_organization_for_member_or_billing
  end

  # Returns an organization only if it exists and the current user has
  # access to it.
  def current_organization
    return @current_organization if defined?(@current_organization)

    if id = params[:organization_id] || params[:org] || params[:id]
      if logged_in?
        org = Organization.find_by_login(id)
        @current_organization = org if org && (org.direct_or_team_member?(current_user) || org.adminable_by?(current_user))
      end
    end

    @current_organization
  end

  def target_for_conditional_access
    return :no_target_for_conditional_access unless current_organization
    current_organization
  end

  def show_corporate_tos_banner?
    return false if GitHub.enterprise?
    return false unless org_admin?
    return false if current_user.dismissed_notice?("org_corporate_tos_banner")
    terms_of_service = current_organization.terms_of_service

    # allow excluded plans, non-standard TOS, and 1 member orgs if staff enabled this prompt
    if terms_of_service.corporate_upgrade_prompt_enabled?
      !terms_of_service.business_terms_of_service?
    else
      excluded_plan_names = ["free", "free_with_addons", "pro", "custom300", "hackathon500", "contest", "engineyard", "enterprise"]
      !excluded_plan_names.include?(current_organization.plan.name) &&
      terms_of_service.standard? &&
      current_organization.members_count > 1
    end
  end

  def show_esa_education_tos_banner?
    return false if GitHub.enterprise?
    return false unless org_admin?
    return false if current_user.dismissed_notice?("org_esa_education_tos_banner")

    terms_of_service = current_organization.terms_of_service
    terms_of_service.esa_education_upgrade_prompt_enabled? && !terms_of_service.esa_education?
  end

  # Returns an organization only if it exists and the current user has
  # access to it as a direct or team member or a billing manager.
  def current_organization_for_member_or_manager
    current_organization_for_member_or_billing
  end

  # Returns an organization only if it exists and the current user has
  # access to it as a direct or team member, or a billing manager
  def current_organization_for_member_or_billing
    return @current_organization_for_member_or_billing if defined?(@current_organization_for_member_or_billing)

    if id = params[:organization_id] || params[:org] || params[:id]
      if logged_in?
        org = Organization.find_by_login(id)
        @current_organization_for_member_or_billing = org if org && (org.direct_or_team_member?(current_user) || org.billing_manager?(current_user) || org.adminable_by?(current_user))
      end
    end

    @current_organization_for_member_or_billing
  end

  # Returns the current team.
  def current_team
    @current_team
  end

  #
  # View Helpers
  #

  # Is the logged in user an owner of the current organization?
  def org_admin?(org = current_organization)
    logged_in? && org && org.adminable_by?(current_user)
  end

  def org_apps_manager?
    manages_any_integration?(user: current_user, organization: current_organization)
  end

  def org_billing_manageable?(org = current_organization_for_member_or_billing)
    logged_in? && org && (
      org.adminable_by?(current_user) || org.billing_manager?(current_user)
    )
  end

  def org_plan_when_created
    current_organization.transactions.first { |t| t.action == "signed-up" }.current_plan
  end

  def org_created_with_free_plan?
    org_plan_when_created.free?
  end

  def org_created_with_team_plan?
    org_plan_when_created.business?
  end

  def org_selfserve_cohort
    if org_created_with_free_plan? && current_organization.plan.business?
      "free-to-paid"
    elsif org_created_with_team_plan? && current_organization.plan.business?
      "zero-to-paid"
    elsif org_created_with_free_plan? && current_organization.plan.free?
      "zero-to-free"
    else
      "not self-serve"
    end
  end

  def current_organization_eligible_for_munich_survey?
    return false unless GitHub.flipper[:munich_survey].enabled?(current_user)
    return false unless current_organization
    return false unless current_organization.created_at >= MUNICH_TRANSITION_DATE
    return false if current_organization.plan.free?
    return false if current_organization.plan.business_plus?
    return false if GitHub.enterprise?

    current_organization.created_at < 7.days.ago
  end

  def org_selfserve_survey_url
    if org_selfserve_cohort == "free-to-paid"
      "https://github.surveymonkey.com/r/BLHCZ7J"
    elsif org_selfserve_cohort == "zero-to-paid"
      "https://github.surveymonkey.com/r/F3QGBL3"
    elsif org_selfserve_cohort == "zero-to-free"
      "https://github.surveymonkey.com/r/KFCC99X"
    else
      ""
    end
  end

  def more_seats_link_for_organization(organization, **options)
    more_seats_link(
      Billing::EnterpriseCloudTrial.new(organization).active?,
      organization.login,
      **options,
    )
  end

  def more_seats_link(sales_serve, organization_login, sales_serve_link_text: "Contact sales", self_serve_link_text: "Buy more", self_serve_return_to: nil)
    if sales_serve
      link_to(sales_serve_link_text, ent_contact_url) + " to add more"
    else
      link_to(self_serve_link_text, org_seats_path(organization_login, return_to: self_serve_return_to))
    end
  end

  # Are we in the process of transforming a user into an organization?
  def org_transform?
    params[:transform_user]
  end

  def business_plus_chosen?
    params[:plan] == "business_plus"
  end

  # Under github.com, the organization contact email is called the "Billing
  # Email" while under Enterprise it's called the "Contact Email".
  def org_contact_email_label
    if GitHub.billing_enabled?
      "Billing"
    else
      "Contact"
    end
  end

  NotificationRestrictionBannerQuery = parse_query <<~'GRAPHQL'
    query($id: ID!) {
      node(id: $id) {
        ...Views::Orgs::Domains::VerifiedDomainNotificationBanner::Organization
      }
    }
  GRAPHQL

  # DEPRECATED. Used to load the Organization for use with the
  # orgs/domains/verified_domain_notification_banner partial.
  # Since the repository container layout is used for multiple pages that are not
  # using GraphQL yet, we have to load the organization with this.
  def load_notification_restriction_banner_organization(organization)
    platform_execute(NotificationRestrictionBannerQuery, variables: { id: organization.global_relay_id }).node
  end

  def pending_installation_requests?
    org_admin? && IntegrationInstallationRequest.where(target: current_organization).exists?
  end

  # Internal: Sends a welcome email to creator of organization that meets the criteria:
    # 1. Creator is opt-out of marketing emails
    # 2. Organization is on a free trial for GHE Cloud
  #
  # Returns nothing.
  def send_welcome_email_for_enterprise_cloud_trial(creator, organization)
    return if NewsletterPreference.marketing?(user: creator)
    return unless Billing::PlanTrial.active_for?(organization, organization.plan.name)

    OrganizationMailer.welcome_enterprise_cloud_trial(creator, organization).deliver_later
  end

  # Internal: Sends a welcome email to creator of organization that meets the criteria:
    # 1. Creator is opt-out of marketing emails
    # 2. Organization is on a free trial for GHE Cloud
  #
  # Returns nothing.
  def send_delayed_end_enterprise_cloud_trial_email(creator, organization, delivery_date)
    return if NewsletterPreference.marketing?(user: creator)
    return unless Billing::PlanTrial.active_for?(organization, organization.plan.name)

    OrganizationMailer.end_enterprise_cloud_trial(creator, organization).deliver_later(wait_until: delivery_date)
  end

  def show_multi_user_invite_on_members_page?
    feature_enabled_globally_or_for_user?(feature_name: :multi_invite_members)
  end
end
