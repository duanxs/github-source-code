# frozen_string_literal: true

module AnalyticsHelper
  # Since we embrace CSP, we need a way to trigger javascript calls without
  # javascript. Weird. So we spit out analytics events as meta tags and
  # rely on google-analytics.coffee to send them to Google.
  #
  # Returns an HTML String.
  def analytics_event_meta_tags
    return unless flash[:analytics_events].respond_to?(:map)

    tags = flash[:analytics_events].map do |event|
      event_params = [event["category"], event["action"], event["label"]]
      event_params << event["value"] if event["value"].present?

      tag(:meta,
        :name => "analytics-event",
        :content => event_params.join(", "),
        "data-pjax-transient" => true,
      )
    end

    safe_join(tags)
  end

  def report_third_party_analytics?
    return false unless GitHub.analytics_enabled?
    return false if do_not_track_enabled?

    if logged_in?
      current_user.report_third_party_analytics?
    else
      true
    end
  end

  # Check if a Do Not Track header is sent by the user agent
  # https://www.w3.org/TR/tracking-dnt/#dnt-header-field
  def do_not_track_enabled?
    request.headers["DNT"] == "1"
  end

  def analytics_ec_meta_tags
    if payload = flash[:analytics_ec_payload]
      tag(:meta,
        :name => "analytics-ec-payload",
        :content => JSON.dump(payload),
        "data-pjax-transient" => true,
      )
    end
  end

  # When users view private repos, we don't want to leak the repo name, owner,
  # or file paths to Google Analytics, so we substitute an anonymized path.
  #
  # Returns only the path as a string.
  def analytics_location
    if flash[:analytics_location].present?
      flash[:analytics_location]
    elsif current_repository
      "/<user-name>/<repo-name>/#{params[:controller]}/#{params[:action]}"
    elsif org = current_organization || current_organization_for_member_or_billing and org.login.present?
      request.path.sub(%r{\A/(organizations/|orgs/)?#{Regexp.escape(org.login)}(/|\Z)}i, '/\1<org-login>\2')
    elsif gist_request?
      masked_gist_location
    elsif params[:controller].in?(%w(profiles users)) && params[:user_id] && (user = this_user) && user.login.present?
      request.path.sub(%r{\A/#{Regexp.escape(user.login)}(/|\Z)}i, '/<user-name>\1')
    else
      request.path
    end
  end

  # When users view private repos, we don't want to leak the repo name, owner,
  # or file paths to Google Analytics, so we substitute an anonymized path.
  #
  # Returns an HTML meta tag.
  def analytics_location_meta_tag
    tags = []
    if extra_params = flash[:analytics_location_params]
      tags << tag(:meta,
          :name => "analytics-location-params",
          :content => extra_params.to_query,
          "data-pjax-transient" => true,
      )
    end

    if flash[:analytics_location_query_strip] == "true"
      tags << tag(:meta,
        :name => "analytics-location-query-strip",
        :content => "true",
        "data-pjax-transient" => "true",
      )
    end

    custom_url = if flash[:analytics_location].present?
      flash[:analytics_location]
    elsif current_repository
      "/<user-name>/<repo-name>/#{params[:controller]}/#{params[:action]}"
    elsif org = current_organization || current_organization_for_member_or_billing and org.login.present?
      request.path.sub(%r{\A/(organizations/|orgs/)?#{Regexp.escape(org.login)}(/|\Z)}i, '/\1<org-login>\2')
    elsif gist_request?
      masked_gist_location
    elsif params[:controller].in?(%w(profiles users)) && params[:user_id] && (user = this_user) && user.login.present?
      request.path.sub(%r{\A/#{Regexp.escape(user.login)}(/|\Z)}i, '/<user-name>\1')
    end

    if custom_url
      tags << tag(:meta,
          :name => "analytics-location",
          :content => custom_url,
          "data-pjax-transient" => true,
      )
    end

    octolytics_path = if flash[:override_octolytics_location]
      if custom_url
        custom_url
      elsif flash[:analytics_location_query_strip] == "true"
        request.path
      end
    end

    if octolytics_path
      tags << tag(:meta,
        :name => "octolytics-location",
        :content => octolytics_path,
        "data-pjax-transient" => true,
      )
    end

    if dimension = flash[:analytics_dimension]
      tags << tag(:meta,
          :class => "js-ga-set",
          :name => dimension.fetch("name"),
          :content => dimension.fetch("value"),
          "data-pjax-transient" => true,
      )
    end

    safe_join(tags)
  end

  # Returns the current controller and action. Example: files#disambiguate
  def controller_action_slug
    [params[:controller], params[:action]].join "#"
  end

  # Internal: Retuns the current path with the username and gist id
  # masked.
  #
  # Example:
  #
  #   /jdpace/a6be29075f1917339319/edit => /<user-name>/<gist-id>/edit
  #
  # Returns a String.
  def masked_gist_location
    request.path.dup.tap do |gist_location|
      gist_location.sub! params[:user_id], "<user-name>" if params[:user_id]
      gist_location.sub! params[:gist_id], "<gist-id>" if params[:gist_id]
    end
  end

  def referral_labels(labels)
    labels += "ref_page:#{params[:ref_page]};" if params[:ref_page]
    labels += "ref_cta:#{params[:ref_cta]};" if params[:ref_cta]
    labels += "ref_loc:#{params[:ref_loc]};" if params[:ref_loc]
    labels
  end
end
