# frozen_string_literal: true

module LabelsHelper
  # Public: The name of the label with `g-emoji` HTML tags instead of raw Unicode emoji or
  # colon-style emoji.
  #
  # name - the name of the label, a String
  # skip_cache - Boolean; whether we should skip trying to load the label name from cache
  #
  # Returns a String, potentially containing `g-emoji` HTML tags.
  def html_label_name(name, skip_cache: false)
    renderer = LabelNameRenderer.new(name, skip_cache: skip_cache)
    renderer.to_html
  end

  def can_viewer_convert_current_repos_issues_to_discussions?
    if defined?(@can_viewer_convert_current_repos_issues_to_discussions)
      return @can_viewer_convert_current_repos_issues_to_discussions
    end
    @can_viewer_convert_current_repos_issues_to_discussions =
      current_repository&.can_convert_issues_to_discussions?(current_user)
  end

  def label_issue_count_summary(label)
    count = label.issues_count
    units = if count == 1
      "issue or pull request"
    else
      "issues and pull requests"
    end
    "#{number_with_delimiter(count)} open #{units}"
  end

  def label_with_description(
    label,
    link_classes: nil,
    data_attrs: nil,
    truncate: false,
    url: nil,
    link: true,
    require_saved_label: true,
    skip_cache: false,
    repo_link: false,
    repo_name: nil,
    repo_owner_login: nil
  )
    use_description = label.description.present?
    use_description = false if require_saved_label && !label.persisted?
    link_class = label_link_class(label, classes: link_classes)
    link_style = "background-color: ##{label.color}; color: ##{label.text_color}"
    url ||= issues_search_query(
      replace: { label: nil },
      append: [[:label, label.name]],
      repo_link: repo_link,
      repo_name: repo_name,
      repo_owner_login: repo_owner_login,
    )
    name = html_label_name(label.name, skip_cache: skip_cache)

    inner = if truncate
      content_tag(:span, name, class: "css-truncate-target", style: "max-width: 100%")
    else
      name
    end

    attributes = { class: link_class, style: link_style, data: data_attrs }
    if use_description
      attributes["title"] = label.description
    else
      attributes["title"] = label.name
    end

    if link
      link_to(inner, url, attributes)
    else
      content_tag(:span, inner, attributes)
    end
  end

  def label_link_class(label, classes: nil)
    classes ||= "d-inline-block"
    classes += " IssueLabel" unless classes.include?("IssueLabel")
    link_class = "#{classes}"

    link_class
  end
end
