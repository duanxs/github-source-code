# frozen_string_literal: true

module DownloadsHelper
  def friendly_size(size)
    if size == 0
      h "<1KB"
    elsif size < 1.kilobyte
      "#{size}KB"
    elsif size < 1.megabyte
      "#{sprintf("%.1f", size / 1.kilobyte.to_f)}MB"
    else
      "#{sprintf("%.1f", size / 1.megabyte.to_f)}GB"
    end
  end

  # Takes a complex mime type (application/zip) tries to generate a simple
  # mime type.  The output goals should be one of:
  #
  # unknown, zip, text, media, android, pdf
  #
  # Returns a simple mime type String.
  def simple_mime_type(mime_type)
    mime_type = mime_type.to_s.downcase
    return "zip"     if mime_type =~ /zip|tar/
    return "text"    if mime_type =~ /text/
    return "media"   if mime_type =~ /mpeg|video|audio|image|jpg|jpeg|gif/
    return "android" if mime_type =~ /android/
    return "pdf"     if mime_type =~ /pdf/

    return "unknown"
  end
end
