# frozen_string_literal: true
require "asset_bundles"

module MailerBundleHelper
  # Creates a stylesheet tag for the primer-emails bundle.
  # We do this in a separate helper so we can avoid needing to load all of the normal Web specific helpers into Mailers.
  def primer_email_stylesheet_tag
    options = { }
    options[:media] = "all"

    filename = asset_bundle.expand_bundle_name("primer-emails.css")
    hash = asset_bundle.integrity(filename)
    if hash
      options[:integrity] = hash
    end

    options[:rel] = "stylesheet"
    options[:href] = asset_bundle.bundle_url("primer-emails.css")
    tag(:link, options)
  end
end
