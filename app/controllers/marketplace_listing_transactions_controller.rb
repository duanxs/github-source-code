# frozen_string_literal: true

class MarketplaceListingTransactionsController < ApplicationController
  areas_of_responsibility :marketplace

  layout "marketplace"

  before_action :render_404, unless: :logged_in?
  before_action :marketplace_required

  IndexQuery = parse_query <<-'GRAPHQL'
    query($listingSlug: String!) {
      marketplaceListing(slug: $listingSlug) {
        databaseId
        slug
        viewerCanReadInsights
      }

      ...Views::MarketplaceListingTransactions::Index::Root
    }
  GRAPHQL

  def index
    data = platform_execute(IndexQuery, variables: {
      listingSlug: params[:listing_slug],
    })

    return render_404 unless data.marketplace_listing&.viewer_can_read_insights

    report = Marketplace::ListingTransactionsReport.new \
      listing_id:   data.marketplace_listing.database_id,
      listing_slug: data.marketplace_listing.slug,
      period:       params[:period]

    respond_to do |format|
      format.html do
        render "marketplace_listing_transactions/index",
          locals: { data: data, report: report }
      end

      format.csv do
        response.headers["Content-Type"] = "text/csv"

        send_data \
          report.truncate(false).as_csv,
          type:     "text/csv",
          filename: report.filename
      end
    end
  end

  private

  def target_for_conditional_access
    listing = Marketplace::Listing.find_by_slug(params[:listing_slug])
    return :no_target_for_conditional_access unless listing
    listing.owner
  end
end
