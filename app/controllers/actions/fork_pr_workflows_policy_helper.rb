# frozen_string_literal: true

module Actions::ForkPrWorkflowsPolicyHelper
  def set_fork_pr_workflows_policy(entity, form_policy)
    if form_policy["run_workflows"]
      policy = Configurable::ForkPrWorkflowsPolicy::RUN_WORKFLOWS
      policy |= Configurable::ForkPrWorkflowsPolicy::WRITE_TOKENS if form_policy["write_tokens"]
      policy |= Configurable::ForkPrWorkflowsPolicy::SEND_SECRETS if form_policy["send_secrets"]
      entity.set_fork_pr_workflows_policy(policy: policy, actor: current_user)
    else
      entity.disable_fork_pr_workflows(actor: current_user)
    end

    if entity.errors.any?
      flash[:error] = "Error saving your changes: #{entity.errors.full_messages.to_sentence}"
      redirect_to :back
    else
      flash[:notice] = "Fork pull request workflow settings saved."
      redirect_to :back
    end
  rescue Configurable::ForkPrWorkflowsPolicy::Error
    flash[:error] = "Error saving your changes."
    redirect_to :back
  end
end
