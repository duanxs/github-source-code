# frozen_string_literal: true

class Actions::StatusesController < AbstractRepositoryController
  map_to_service :actions_experience
  statsd_tag_actions

  include ::ActionsControllerMethods

  def index
    statuses = [[:queued, :in_progress, :completed].map { |status| status.to_s }, CheckRun.conclusions.keys].flatten

    respond_to do |format|
      format.html do
        render "actions/statuses/index",
          layout: false,
          locals: {
            statuses: statuses,
            workflow_run_filters: workflow_run_filters,
          }
      end

      format.json do
        render json: statuses
      end
    end
  end
end
