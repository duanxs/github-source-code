# frozen_string_literal: true

class Actions::BranchesController < AbstractRepositoryController
  map_to_service :actions_experience
  statsd_tag_actions

  include ::ActionsControllerMethods

  def index
    branches = current_repository.actions_check_suites.distinct.reorder(:head_branch).pluck(:head_branch).reject(&:blank?)

    respond_to do |format|
      format.html do
        render "actions/branches/index",
          layout: false,
          locals: {
            branches: branches,
            workflow_run_filters: workflow_run_filters,
          }
      end

      format.json do
        render json: branches
      end
    end
  end

  def select
    branches = current_repository.actions_check_suites.distinct.reorder(:head_branch).pluck(:head_branch).reject(&:blank?)

    render "actions/branches/select",
      layout: false,
      locals: {
        branches: branches,
        selected_branch: params[:branch],
      }
  end
end
