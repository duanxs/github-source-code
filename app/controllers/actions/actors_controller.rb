# frozen_string_literal: true

class Actions::ActorsController < AbstractRepositoryController
  map_to_service :actions_experience
  statsd_tag_actions

  include ::ActionsControllerMethods

  def index
    ids = current_repository.user_ids_with_privileged_access(min_action: :write)
    users = User.where(id: ids).includes(:profile).by_login

    respond_to do |format|
      format.html do
        render "actions/actors/index",
          layout: false,
          locals: {
            actors: users,
            selected_actor: workflow_run_filters[:actor],
            workflow_run_filters: workflow_run_filters,
          }
      end

      format.json do
        render json: users.map { |user| { login: user.display_login, displayName: user.profile_name } }
      end
    end
  end
end
