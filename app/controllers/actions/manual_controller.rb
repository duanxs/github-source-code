# frozen_string_literal: true

class Actions::ManualController < AbstractRepositoryController
  include ::ActionsControllerMethods
  include ::ActionsHelper

  before_action :require_push_access, except: [:manual_run_partial]
  before_action :actions_enabled_for_repo?

  def manual_run_partial
    return head :bad_request unless workflow_path.present?
    return head :not_found unless workflow.present?

    render_form
  end

  def trigger
    return head :bad_request unless workflow_path.present?
    return head :bad_request unless branch.present?

    # Check that the workflow exists in the given branch
    return head :not_found unless parsed_workflow.present?

    if inputs && inputs.to_json.length > MYSQL_TEXT_FIELD_LIMIT
      return redirect_to actions_workflow_path(filters: {}, workflow: workflow.name), flash: { error: "Provided inputs are too large." }
    end

    begin
      input_values = parsed_workflow.process_inputs(inputs)
    rescue ArgumentError
      return head :bad_request
    end

    # Trigger event to run the workflow
    current_repository.dispatch_workflow_event(current_user.id, workflow_path, branch, input_values)

    # Workflow run was queued
    redirect_to actions_workflow_path(filters: {}, workflow: workflow.name), flash: { notice: "Workflow run was successfully requested." }
  end

  private

  def workflow_path
    params[:workflow]
  end

  def branch
    params[:branch] || current_repository.default_branch_ref.qualified_name
  end

  def inputs
    params[:inputs]
  end

  def workflow
    @workflow ||= current_repository.workflows.find_by(path: workflow_path)
  end

  def parsed_workflow
    @parsed_workflow ||= Actions::ParsedWorkflow.parse_from_yaml(current_repository, workflow_path, branch)
  end

  def workflow_inputs
    parsed_workflow&.workflow_dispatch_inputs
  end

  def render_form
    render partial: "actions/manual/manual_run_partial", locals: {
      workflow: workflow,
      workflow_in_branch: parsed_workflow.present?,
      selected_branch: branch,
      inputs: workflow_inputs,
      branches: current_repository.heads.refs_with_default_first.map { |branch|
        {
          name: branch.name_for_display,
          value: branch.qualified_name_for_display,
        }
      },
    }
  end

  def require_push_access
    render_404 unless current_user_can_push?
  end
end
