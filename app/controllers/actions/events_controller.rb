# frozen_string_literal: true

class Actions::EventsController < AbstractRepositoryController
  map_to_service :actions_experience
  statsd_tag_actions

  include ::ActionsControllerMethods

  def index
    events = current_repository.actions_check_suites.distinct.reorder(:event).pluck(:event).reject(&:blank?)

    respond_to do |format|
      format.html do
        render "actions/events/index",
          layout: false,
          locals: {
            events: events,
            workflow_run_filters: workflow_run_filters,
          }
      end

      format.json do
        render json: events
      end
    end
  end
end
