# frozen_string_literal: true

class Actions::SurveyController < AbstractRepositoryController
  map_to_service :actions_experience

  include ActionsControllerMethods

  before_action :login_required
  before_action :actions_enabled_for_repo?

  def index
    survey = find_survey
    return head :not_found unless survey

    render "actions/survey/index",
      layout: false,
      locals: {
        survey: survey,
      }
  end

  def dismiss
    return head :not_found unless request.xhr?

    Actions::Survey::dismiss_survey(current_user)

    head :ok
  end

  def answer
    answers = (params[:answers] || []).select { |answer| answer[:choice_id].present? }
    return head :bad_request unless answers.length > 0

    # Mark the survey as answered first. Even if saving the answers might fail, we do not want to prompt
    # the user again, immediately.
    Actions::Survey::answered_survey(current_user)

    # Save answers
    survey = find_survey
    if survey
      SurveyAnswer.save_as_group(current_user.id, survey.id, answers)
    end

    flash[:notice] = "Thank you for your feedback!"
    redirect_to actions_path
  end

  private

  def find_survey
    ::Survey.find_by_slug(Actions::Survey::SURVEY_SLUG)
  end
end
