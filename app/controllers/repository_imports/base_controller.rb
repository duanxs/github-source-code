# frozen_string_literal: true

module RepositoryImports

  # Internal: Repository import controllers all inherit from this controller.
  class BaseController < AbstractRepositoryController
    areas_of_responsibility :migration

    include OrganizationsHelper

    before_action :porter_available!
    before_action :ensure_visible_to_user

    rescue_from Porter::ApiClient::Error do |error|
      failbot(error)

      if Rails.development?
        raise error
      else
        render_porter_unavailable
      end
    end

    private

    # Internal: The repository import for this repository and user.
    #
    # Returns a RepositoryImport.
    def repository_import
      @repository_import ||= RepositoryImport.new(
        repository: current_repository,
        user: current_user,
      )
    end

    # Internal: Block access unless user is logged in, the repository imports
    # feature is enabled, and Porter is available.
    def porter_available!
      unless GitHub.porter_available?
        render_404
      end
    end

    # Internal: Render 404 if there is no current_repository.
    # Redirect back to the repository if user does not have push permission
    # or if the repository was never imported with Porter.
    def ensure_visible_to_user
      if current_repository.nil?
        render_404
      elsif !current_repository.pushable_by?(current_user)
        redirect_to repository_path(current_repository)
      end
    end

    # Internal: Render Porter unavailable view.
    def render_porter_unavailable
      render "repository_imports/porter_unavailable"
    end

    # NB: The `owner` helper method queries based on the :user_id param.
    #     We fall back to querying based on the :owner param for the
    #     POST #create endpoint, which accepts :owner as a request param but
    #     does not have a :user_id param in the URL, as in
    #     `/:user_id/:repository/import`.
    def target_for_conditional_access
      return owner unless owner.nil?
      # Owner might be a User or an Organization, this method returns either.
      org = User.find_by_login(params[:owner])
      return :no_target_for_conditional_access unless org
      org
    end
  end
end
