# frozen_string_literal: true

module RepositoryImports
  class AuthorsController < RepositoryImports::BaseController
    areas_of_responsibility :migration

    # Public: List the authors that were found during the import.
    def index
      if repository_import.authors_found?
        render_template_view "repository_imports/authors/index", RepositoryImports::ShowView,
          repository_import: repository_import
      else
        redirect_to repository_import_path(repository_import.url_params)
      end
    end

    # Public: List suggested github users to map an author to.
    def author_suggestions
      headers["Cache-Control"] = "no-cache, no-store"

      respond_to do |format|
        format.html_fragment do
          render_partial_view "repository_imports/authors/author_suggestions",
            RepositoryImports::Authors::AuthorSuggestionsView, query: params[:q]
        end
      end
    end

    # Public: Update an author with email or name, email, and github login.
    def update
      user = nil
      author_params = {
        author_id: params[:id].to_i,
      }

      if params[:author].present?
        if user = User.find_by_login(params[:author])
          author_params[:email] = user.public_attribution_email
          author_params[:name] = user.profile.try(:name)
          author_params[:login] = user.login
        else
          author_params[:email] = params[:author]
        end
      else
        author_params[:email] = nil
        author_params[:name] = nil
        author_params[:login] = nil
      end

      author = repository_import.update_author(**author_params)

      respond_to do |format|
        format.html do
          render_partial_view "repository_imports/authors/author",
            RepositoryImports::Authors::AuthorView,
            author: author,
            user: user,
            repository_import: repository_import
        end
      end
    end
  end
end
