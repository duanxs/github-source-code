# frozen_string_literal: true

class Sponsors::DashboardsController < ApplicationController
  include Sponsors::AdminableControllerValidations

  prepend_before_action :login_required
  before_action :non_banned_sponsorable_required
  before_action :sponsors_listing_required
  before_action :sponsors_membership_required
  before_action :add_csp_exceptions
  CSP_EXCEPTIONS = {
    form_action: [
      "https://na3.docusign.net",
      "https://demo.docusign.net",
      Sponsors::TwitterButtonComponent::TWITTER_WEB_INTENT_URL,
    ],
    frame_src: [
      "#{GitHub.scheme}://#{GitHub.host_name}",
    ]
  }.freeze

  layout "sponsors"

  def show
    render "sponsors/dashboards/show", locals: {
      sponsorable: sponsorable,
      docusign_event: params[:event],
      stripe_request_domain: "#{request.protocol}#{request.host}",
    }
  end

  private

  def target_for_conditional_access
    return :no_target_for_conditional_access if sponsorable.user?
    sponsorable
  end
end
