# frozen_string_literal: true

class Sponsors::StripeAccountTransitionsController < ApplicationController
  include Sponsors::AdminableControllerValidations

  prepend_before_action :login_required
  before_action :non_banned_sponsorable_required
  before_action :sponsors_listing_required
  before_action :sponsors_membership_required

  def show
    return render_404 unless GitHub.flipper[:duplicate_stripe_remediation].enabled?(sponsorable)
    return head 202 if sponsorable_sponsors_listing.duplicate_stripe_remediation_auth_code.present?

    render html: "Account successfully transitioned!"
  end

  private

  def target_for_conditional_access
    return :no_target_for_conditional_access if sponsorable.user?
    sponsorable
  end
end
