# frozen_string_literal: true

class Sponsors::Payouts::BalancesController < ApplicationController
  include Sponsors::AdminableControllerValidations

  before_action :stripe_account_required

  def show
    # TODO: remove this when rendering ViewComponents in controllers does not
    #       require this.
    response.headers["Content-Type"] = "text/html"

    balance_response = stripe_account.current_balance

    if balance_response.success?
      render Sponsors::Payouts::NextPayoutComponent.new(
        sponsorable: sponsorable,
        balance: balance_response.result,
        needs_tax_form: sponsorable.sponsors_docusign_pending?,
      )
    else
      render Sponsors::Payouts::NextPayoutComponent.new(sponsorable: sponsorable, error: balance_response.error)
    end
  end

  private

  def target_for_conditional_access
    return :no_target_for_conditional_access unless sponsorable
    sponsorable
  end
end
