# frozen_string_literal: true

class Sponsors::StripeAccountsController < ApplicationController
  include Sponsors::AdminableControllerValidations

  before_action :sponsors_listing_required
  before_action :sponsors_membership_required

  layout "sponsors"

  NewQuery = parse_query <<~'GRAPHQL'
    query($sponsorableId: ID!) {
      sponsorable: node(id: $sponsorableId) {
        ... on Actor {
          login
        }
        ... on Sponsorable {
          sponsorsListing {
            stripeConnectAccount {
              stripeAccountId
            }
          }
        }
      }
    }
  GRAPHQL

  def new
    data = platform_execute(NewQuery, variables: {
      sponsorableId: sponsorable_id,
    })

    ar_sponsorable = User.find_by(login: data.sponsorable.login)

    if account = data.sponsorable.sponsors_listing.stripe_connect_account
      link = begin
        Stripe::Account.create_login_link(account.stripe_account_id)
      rescue Stripe::APIConnectionError
        return safe_redirect_to sponsorable_dashboard_path(data.sponsorable.login)
      rescue Stripe::PermissionError => e
        GitHub.dogstats.increment("stripe.permission_error", tags: ["action:generate_link"])
        Failbot.report(e)
        stripe_oauth_url = Billing::StripeConnect::Account::Oauth.authorization_url(
          request_identifier: Billing::StripeConnect::Account::Oauth.generate_request_state(sponsorable_id),
          domain: "#{request.protocol}#{request.host}",
          sponsorable: ar_sponsorable,
        )
        return redirect_to stripe_oauth_url
      end

      url = params[:path].present? ? link["url"] + "##{params[:path]}" : link["url"]

      redirect_to url
    else
      safe_redirect_to sponsorable_dashboard_path(data.sponsorable.login)
    end
  end

  EditQuery = parse_query <<~'GRAPHQL'
    query($sponsorableId: ID!) {
      sponsorable: node(id: $sponsorableId) {
        ... on User {
          databaseId
        }
        ... on Organization {
          databaseId
        }
      }
    }
  GRAPHQL

  def edit
    data = platform_execute(EditQuery, variables: {
      sponsorableId: sponsorable_id,
    })

    sponsorable = User.find_by(id: data.sponsorable.database_id)
    listing = sponsorable.sponsors_listing
    account = listing.stripe_connect_account

    # wait for Stripe account to be created and for details to be saved
    return head 202 if listing.stripe_authorization_code.present? && account&.stripe_account_details.blank?

    stripe_connect_url = Billing::StripeConnect::Account::Oauth.authorization_url(
      request_identifier: Billing::StripeConnect::Account::Oauth.generate_request_state(sponsorable.global_relay_id),
      domain: "#{request.protocol}#{request.host}",
      sponsorable: sponsorable,
    )

    respond_to do |format|
      format.html do
        render Sponsors::StripeAccounts::EditComponent.new(
          stripe_connect_url: stripe_connect_url,
          sponsorable: sponsorable,
        )
      end
    end
  end

  private

  def target_for_conditional_access
    return :no_target_for_conditional_access if sponsorable.user?
    sponsorable
  end
end
