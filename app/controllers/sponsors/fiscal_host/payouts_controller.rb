# frozen_string_literal: true

class Sponsors::FiscalHost::PayoutsController < ApplicationController
  include Sponsors::AdminableControllerValidations

  before_action :sponsors_listing_required
  before_action :sponsors_membership_required
  before_action :fiscal_host_sponsorable_required

  def show
    fiscal_host = SponsorsMembership::FISCAL_HOST_GITHUB_ORGANIZATION.fetch(sponsorable.login)
    timeframe = params[:timeframe].presence || :all
    report = SponsorsListing::PayoutsExport.new(fiscal_host: fiscal_host, timeframe: timeframe)
    send_data(report.as_csv, type: "text/csv", filename: report.filename)
  end

  private

  def target_for_conditional_access
    return :no_target_for_conditional_access unless sponsorable
    sponsorable
  end
end
