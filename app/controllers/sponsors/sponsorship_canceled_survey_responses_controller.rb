# frozen_string_literal: true

class Sponsors::SponsorshipCanceledSurveyResponsesController < ApplicationController
  include Sponsors::SharedControllerMethods

  before_action :sponsorship_canceled_survey_required

  def create
    sponsor_survey = Sponsors::SponsorshipCanceledSurvey.new(
      sponsor: current_user,
      sponsorable: sponsorable,
      submitted_answers: submitted_answers,
    )

    if sponsor_survey.save
      flash[:notice] = "Thanks for helping us improve the GitHub Sponsors program!"
    else
      flash[:error] = "Your survey couldn't be submitted: #{sponsor_survey.errors.full_messages.to_sentence}"
    end

    redirect_to sponsorable_path(sponsorable)
  end

  private

  def sponsorship_canceled_survey_required
    render_404 unless feature_enabled_globally_or_for_current_user?(:sponsorship_canceled_survey)
  end

  def submitted_answers
    return [] unless params[:answers]
    params[:answers].select { |answer| answer[:choice_id].present? }
  end

  def target_for_conditional_access
    :no_target_for_conditional_access
  end
end
