# frozen_string_literal: true

class Sponsors::SponsorsPartialsController < ApplicationController
  include Sponsors::SharedControllerMethods

  before_action :sponsors_listing_required

  before_action :verify_visible_to_viewer
  before_action :non_spammy_user_required

  def show
    render partial: "sponsors/sponsors_partials/show", locals: {
      sponsorable: sponsorable,
      sponsorships: sponsorships_as_sponsorable
        .paginate(page: current_page, per_page: per_page),
    }
  end

  private

  def per_page
    Sponsors::SponsorablesController::SPONSORS_PER_PAGE
  end

  def target_for_conditional_access
    :no_target_for_conditional_access
  end
end
