# frozen_string_literal: true

class Sponsors::SponsorshipCreatedSurveyResponsesController < ApplicationController
  include Sponsors::SharedControllerMethods

  before_action :sponsorable_sponsored_by_current_user_required

  def create
    sponsor_survey = Sponsors::SponsorshipCreatedSurvey.new(
      sponsor: current_user,
      sponsorable: sponsorable,
      submitted_answers: submitted_answers,
    )

    if sponsor_survey.save
      flash[:notice] = "Thanks for completing the survey about why you sponsored #{sponsorable}!"
    else
      flash[:error] = "Your survey couldn't be submitted: #{sponsor_survey.errors.full_messages.to_sentence}"
    end

    redirect_to sponsorable_path(sponsorable)
  end

  private

  def submitted_answers
    params[:answers].select { |answer| answer[:choice_id].present? }
  end

  def target_for_conditional_access
    :no_target_for_conditional_access
  end
end
