# frozen_string_literal: true

class Sponsors::Profile::MeetTheTeamsController < ApplicationController
  include Sponsors::AdminableControllerValidations

  before_action :organization_sponsorable_required

  MEMBERS_LIMIT = 50

  def edit
    render partial: "sponsors/profile/meet_the_teams/edit", locals: partial_locals
  end

  def show
    render partial: "sponsors/profile/meet_the_teams/show", locals: partial_locals
  end

  private

  def partial_locals
    {
      sponsorable:            sponsorable,
      members:                find_members,
      query:                  params[:query],
      current_featured_users: current_featured_users,
      featured_users_limit:   SponsorsListingFeaturedItem::FEATURED_USERS_LIMIT_PER_LISTING,
    }
  end

  def find_members
    query = params[:query]

    members = members_excluding_featured
    members = members.with_prefix("users.login", query.strip) if query.present?

    current_featured_users + members
  end

  def current_featured_users
    @current_featured_users ||= sponsorable
      .sponsors_listing
      .featured_users
      .map(&:featureable)
  end

  def members_excluding_featured
    @members_excluding_featured ||= sponsorable
      .members
      .where.not(id: current_featured_users.map(&:id))
      .limit(MEMBERS_LIMIT)
  end

  def target_for_conditional_access
    return :no_target_for_conditional_access unless sponsorable
    sponsorable
  end
end
