# frozen_string_literal: true

module Sponsors::Embeddable
  extend ActiveSupport::Concern
  include Sponsors::SharedControllerMethods

  included do
    before_action :approved_sponsors_listing_required
    before_action :allow_iframe_embedding, only: :show

    private

    def allow_iframe_embedding
      SecureHeaders.override_x_frame_options(request, "ALLOWALL")
      SecureHeaders.override_content_security_policy_directives(
        request,
        frame_ancestors: [SecureHeaders::CSP::STAR],
      )
    end

    # Disable user sessions on embeddable pages
    def stateless_request?
      true
    end

    def target_for_conditional_access
      :no_target_for_conditional_access
    end
  end
end
