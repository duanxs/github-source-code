# frozen_string_literal: true

class Sponsors::SponsorshipsController < ApplicationController
  include Sponsors::SharedControllerMethods

  SPONSORSHIPS_TO_DISPLAY = 22

  before_action :logged_in_user_required
  before_action :sponsors_listing_required
  before_action :verify_visible_to_viewer
  before_action :non_spammy_user_required
  before_action :require_sponsorable_is_not_sponsor, except: :show
  before_action :valid_sponsor_required, only: [:create, :update]
  before_action :valid_tier_required
  before_action :add_csp_exceptions, only: [:show]

  statsd_tag_actions only: [:show, :create, :update]

  # For editing billing information within a modal and sharing sponsorship to Twitter
  CSP_EXCEPTIONS = {
    img_src: [GitHub.paypal_checkout_url].freeze,
    connect_src: [GitHub.braintreegateway_url, GitHub.braintree_analytics_url].freeze,
    frame_src: [GitHub.zuora_payment_page_server].freeze,
  }.freeze

  rescue_from Sponsors::UpdateSponsorship::ForbiddenError,
              with: :render_404

  rescue_from Sponsors::CreateSponsorship::ForbiddenError,
              Sponsors::CreateSponsorship::UnprocessableError,
              Sponsors::UpdateSponsorship::UnprocessableError,
              Billing::CreateSubscriptionItem::UnprocessableError,
              Billing::UpdateSubscriptionItem::UnprocessableError do |error|
    flash[:error] = error.message
    redirect_to :back
  end

  def show
    render "sponsors/sponsorships/show", locals: {
       sponsor: sponsor,
       listing: sponsorable_sponsors_listing,
       selected_tier: selected_tier,
       sponsorable: sponsorable,
       sponsorship: sponsorship,
       plan_change: plan_change,
       goal: sponsorable_sponsors_listing.active_goal,
       sponsorships: sponsorships_as_sponsorable
         .paginate(page: 1, per_page: SPONSORSHIPS_TO_DISPLAY),
    }
  end

  def create
    Sponsors::CreateSponsorship.call(
      tier: selected_tier,
      sponsor: sponsor,
      sponsorable: sponsorable,
      quantity: 1,
      viewer: current_user,
      privacy_level: params[:privacy_level],
      email_opt_in: params[:email_opt_in] == "on",
    )

    redirect_params = { success: "true" }
    if sponsor != current_user
      redirect_params[:sponsor] = sponsor.login
    end

    redirect_to sponsorable_path(sponsorable, redirect_params)
  end

  def update
    Sponsors::UpdateSponsorship.call(
      tier: selected_tier,
      sponsor: sponsor,
      sponsorable: sponsorable,
      quantity: 1,
      viewer: current_user,
      privacy_level: params[:privacy_level],
      email_opt_in: params[:email_opt_in] == "on",
    )

    redirect_params = {}
    if sponsor != current_user
      redirect_params[:sponsor] = sponsor.login
    end

    flash[:notice] = "Your sponsorship of #{sponsorable_name} has been updated."
    redirect_to sponsorable_path(sponsorable, redirect_params)
  end

  private

  def sponsorable_name
    sponsorable.name || sponsorable.login
  end

  def valid_sponsor_required
    potential_sponsor_logins = current_user.potential_sponsor_logins
    render_404 unless potential_sponsor_logins.include?(sponsor_login)
  end

  def target_for_conditional_access
    :no_target_for_conditional_access
  end

  def selected_tier
    tier = sponsorable_sponsors_listing
      .sponsors_tiers
      .find_by(id: params[:tier_id])

    tier if tier&.readable_by?(sponsor)
  end

  def valid_tier_required
    render_404 if selected_tier.nil?
  end

  def plan_change
    Sponsors::PlanChange.new(
      sponsor: sponsor,
      sponsorship: sponsorship,
      new_tier: selected_tier,
      listing: sponsorable_sponsors_listing,
    )
  end

  def sponsorship
    return @sponsorship if defined? @sponsorship
    potential_sponsorship = sponsor.sponsorship_as_sponsor_for(sponsorable)
    @sponsorship = if potential_sponsorship&.active?
      potential_sponsorship
    else
      nil
    end
  end
end
