# frozen_string_literal: true

class Sponsors::UpdatesController < ApplicationController
  include Sponsors::AdminableControllerValidations
  include TextHelper

  before_action :sponsors_listing_required
  before_action :sponsors_membership_required
  before_action :draft_newsletter_required, only: [:edit, :update]

  layout "sponsors"

  def index
    newsletters = sponsorable.sponsorship_newsletters
      .paginate(page: current_page, per_page: SponsorshipNewsletter::PER_PAGE)
      .includes(sponsors_tiers: [:subscription_items])
      .preload(:author)
      .order("created_at DESC")

    render "sponsors/updates/index", locals: {
      sponsorable: sponsorable,
      newsletters: newsletters,
    }
  end

  def new
    published_tiers, retired_tiers = sponsorable_sponsors_listing
      .sponsors_tiers.where(state: [1, 2])
      .partition { |tier| tier.published? }

    render "sponsors/updates/new", locals: {
      sponsorable: sponsorable,
      sponsors_listing: sponsorable_sponsors_listing,
      published_tiers: published_tiers,
      retired_tiers: retired_tiers,
    }
  end

  def create
    result = Sponsors::CreateSponsorshipNewsletter.call(
      sponsorable: sponsorable,
      author: current_user,
      draft: newsletter_params[:draft] == "1",
      body: newsletter_params[:body].presence,
      subject: newsletter_params[:subject].presence,
      tier_ids: newsletter_params[:tier_ids],
    )

    if result.success?
      return redirect_to sponsorable_dashboard_updates_path(sponsorable)
    end

    flash[:error] = result.error.message
    return redirect_to :back
  end

  def edit
    published_tiers, retired_tiers = sponsorable_sponsors_listing
      .sponsors_tiers.where(state: [1, 2])
      .partition { |tier| tier.published? }

    render "sponsors/updates/edit", locals: {
      newsletter: newsletter,
      sponsorable: sponsorable,
      published_tiers: published_tiers,
      retired_tiers: retired_tiers,
      selected_tiers: newsletter.sponsors_tiers,
    }
  end

  def update
    result = Sponsors::UpdateSponsorshipNewsletter.call(
      newsletter: newsletter,
      subject: newsletter_params[:subject].presence,
      body: newsletter_params[:body].presence,
      draft: newsletter_params[:draft] == "1",
      tier_ids: newsletter_params[:tier_ids],
    )

    if result.success?
      if result.newsletter.published?
        flash[:notice] = "Your update has been published"
        return redirect_to sponsorable_dashboard_updates_path(sponsorable, newsletter)
      else
        flash[:notice] = "Your update has been saved"
        return redirect_to sponsorable_dashboard_updates_path(sponsorable)
      end
    end

    flash[:error] = result.error.message
    redirect_to :back
  end

  protected

  def newsletter_params
    params.require(:newsletter).permit(:subject, :body, :draft, tier_ids: [])
  end

  def draft_newsletter_required
    if newsletter&.published?
      flash[:error] = "You can't update a published email update."
      redirect_to sponsorable_dashboard_updates_path(current_user)
    end
  end

  def newsletter
    @newsletter ||= sponsorable
      .sponsorship_newsletters
      .includes(:sponsors_tiers)
      .find(params[:id])
  end

  private

  def target_for_conditional_access
    return :no_target_for_conditional_access if sponsorable.user?
    sponsorable
  end
end
