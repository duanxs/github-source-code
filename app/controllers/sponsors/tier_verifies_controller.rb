# frozen_string_literal: true

# not named the more sensible `TierPricingChecksController` because some people
# who write adblockers think that all URLs with `pricing` should be blocked
# (which is very silly) and also a `check` means `money` in en_US so that way
# lies sadness too.
class Sponsors::TierVerifiesController < ApplicationController
  include Sponsors::AdminableControllerValidations

  before_action :sponsors_membership_required
  before_action :sponsors_listing_required

  def create
    price_in_cents = params[:value].to_i * 100

    tier = SponsorsTier.new(
      sponsors_listing: sponsorable_sponsors_listing,
      monthly_price_in_cents: price_in_cents,
      yearly_price_in_cents: price_in_cents * 12,
    )

    respond_to do |format|
      format.html_fragment do
        # make sure that the value is not a decimal
        if params[:value].to_f % 1 != 0
          error_message = "must be an integer"
        elsif price_in_cents.zero? || params[:value].to_f < 0
          error_message = "must be at least $1"
        else
          tier.valid? # call the validations
          return head :ok unless tier.errors[:monthly_price_in_cents].any?

          error_message = tier.errors[:monthly_price_in_cents].to_sentence
        end

        render body: "Tier pricing #{error_message}", status: 422, content_type: "text/fragment+html"
      end
    end
  end

  private

  def target_for_conditional_access
    return :no_target_for_conditional_access if sponsorable.user?
    sponsorable
  end
end
