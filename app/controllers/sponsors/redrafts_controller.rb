# frozen_string_literal: true

class Sponsors::RedraftsController < ApplicationController
  include Sponsors::AdminableControllerValidations

  before_action :sponsors_listing_required
  before_action :sponsors_membership_required

  def create
    if sponsorable_sponsors_listing.can_redraft?
      sponsorable_sponsors_listing.redraft!
      flash[:notice] = "Okay, this Sponsors profile has been unpublished."
    else
      flash[:error] = "This Sponsors profile cannot be unpublished."
    end

    redirect_to sponsorable_dashboard_path(sponsorable)
  end

  private

  def target_for_conditional_access
    return :no_target_for_conditional_access if sponsorable.user?
    sponsorable
  end
end
