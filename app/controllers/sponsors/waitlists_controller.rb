# frozen_string_literal: true

class Sponsors::WaitlistsController < ApplicationController
  include Sponsors::SharedControllerMethods

  before_action :non_banned_sponsorable_required
  before_action :login_required
  before_action :sponsorable_adminable_by_current_user_required
  before_action :sponsorable_verified_email_required, only: [:show]
  before_action :validate_fiscal_option, only: :create

  layout "sponsors"

  def show
    if sponsorable.sponsors_listing.present?
      redirect_to sponsorable_dashboard_path(sponsorable.login)
    elsif sponsorable.sponsors_program_member?
      redirect_to sponsorable_signup_path(sponsorable.login)
    else
      billing_email = sponsorable.billing_email if sponsorable.organization?
      render "sponsors/waitlists/show", locals: {
        sponsorable: sponsorable,
        survey: survey,
        survey_questions: survey_questions,
        membership: sponsorable.sponsors_membership,
        possible_emails: sponsorable.emails.user_entered_emails.verified,
        billing_email: billing_email,
      }
    end
  end

  def create
    membership = if sponsorable.user? || params[:fiscal_option] == ::SponsorsMembership::FISCAL_OPTION_BANK
      ::SponsorsMembership.new_with_bank(membership_params)
    else
      ::SponsorsMembership.new_with_fiscal_host(membership_params)
    end

    if membership.save_with_survey_answers(survey_answers)
      if membership.auto_acceptable?
        result = Sponsors::AcceptSponsorsMembership.call(
          sponsors_membership: membership,
          actor: nil,
          automated: true,
        )

        # Send sponsorable to create a profile if they were successfully auto-accepted
        if result.success?
          flash[:notice] = "Congratulations! You've been accepted into GitHub Sponsors. Please create your profile."
          return redirect_to sponsorable_signup_url(sponsorable)
        end
      end

      redirect_to sponsorable_waitlist_path(sponsorable)
    else
      flash[:error] = membership.errors.full_messages.join(", ")
      redirect_to :back
    end
  rescue ::SponsorsMembership::InvalidFiscalHostError => e
    flash[:error] = e.message
    redirect_to :back
  end

  def update
    membership = sponsorable.sponsors_membership

    # ignore if membership belongs to an organization that chose a fiscal host
    if membership.fiscal_host_none? || sponsorable.user?
      if membership.update(
        contact_email_id: params[:contact_email_id],
        billing_country: params[:billing_country],
        country_of_residence: params[:country_of_residence])

        flash[:notice] = "Your GitHub Sponsors waitlist entry has been updated."
      else
        flash[:error] = membership.errors.full_messages.join(", ")
      end
    end

    redirect_to sponsorable_waitlist_path(sponsorable)
  end

  private

  def membership_params
    {
      sponsorable: sponsorable,
      survey: survey,
      contact_email_id: params[:contact_email_id],
      fiscal_host: params[:fiscal_host],
      billing_country: params[:billing_country],
      country_of_residence: params[:country_of_residence],
      fiscal_host_billing_country: params[:fiscal_host_billing_country],
      fiscal_host_name: params[:fiscal_host_name],
    }
  end

  def validate_fiscal_option
    return if sponsorable.user?
    return if fiscal_option_valid?

    flash[:error] = "Please provide either the organization's bank account country/region or select a fiscal host."
    redirect_to sponsorable_waitlist_path(sponsorable)
  end

  def fiscal_option_valid?
    params[:fiscal_option].present? && ::SponsorsMembership::FISCAL_OPTIONS.include?(params[:fiscal_option])
  end

  def survey_answers
    params[:answers].map do |answer|
      # convert choice_id from a hash to the format expected by
      # EarlyAccessMembership#save_with_survey_answers (which doesn't seem
      # equipped to handle multiple radio buttons)
      if answer["choice_id"].respond_to?(:values)
        answer["choice_id"] = answer["choice_id"].values.first
      end
      answer
    end
  end

  def survey
    ActiveRecord::Base.connected_to(role: :writing) do
      @survey ||= if sponsorable.organization?
        ::Sponsors::OrganizationWaitlistSurvey.find_or_create_survey
      else
        ::Sponsors::UserWaitlistSurvey.find_or_create_survey
      end
    end
  end

  def survey_questions
    scope = survey.questions.visible

    if sponsorable.organization?
      scope = scope.where.not(short_text: ::Sponsors::OrganizationWaitlistSurvey::FISCAL_HOST_QUESTION_SLUG)
    end

    scope
  end

  def target_for_conditional_access
    return :no_target_for_conditional_access if sponsorable.user?
    sponsorable
  end
end
