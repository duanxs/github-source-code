# frozen_string_literal: true

class Sponsors::ActivitiesController < ApplicationController
  include Sponsors::AdminableControllerValidations

  before_action :sponsors_membership_required
  before_action :sponsors_listing_required

  PER_PAGE = 50

  def show
    unless current_user.dismissed_notice?(::User::SPONSORS_ACTIVITY_BANNER)
      select_write_database do
        current_user.dismiss_notice(::User::SPONSORS_ACTIVITY_BANNER)
      end
    end

    locals = {
      sponsorable: sponsorable,
      period: params[:period].presence&.to_sym || SponsorsActivity::DEFAULT_PERIOD,
      page: current_page,
      per_page: PER_PAGE,
    }

    if request.xhr?
      render partial: "sponsors/activities/timeline", locals: locals
    else
      render "sponsors/activities/show", locals: locals
    end
  end

  private

  def target_for_conditional_access
    return :no_target_for_conditional_access if sponsorable.user?
    sponsorable
  end

end
