# frozen_string_literal: true

class Sponsors::SettingsController < ApplicationController
  include Sponsors::AdminableControllerValidations

  before_action :sponsors_listing_required
  before_action :sponsors_membership_required

  layout "sponsors"

  def show
    render "sponsors/settings/show", locals: { sponsorable: sponsorable }
  end

  private

  def target_for_conditional_access
    return :no_target_for_conditional_access if sponsorable.user?
    sponsorable
  end
end
