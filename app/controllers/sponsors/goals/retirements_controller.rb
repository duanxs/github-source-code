# frozen_string_literal: true

class Sponsors::Goals::RetirementsController < ApplicationController
  include Sponsors::AdminableControllerValidations

  before_action :sponsors_listing_required

  def create
    if active_goal.present?
      active_goal.retire!
      instrument_event(action: :RETIRED, goal: active_goal)

      flash[:notice] = "Your active goal has been retired."
    end

    redirect_to sponsorable_dashboard_goals_path(sponsorable)
  end

  private

  def active_goal
    sponsorable_sponsors_listing.active_goal
  end

  def instrument_event(action:, goal:)
    GlobalInstrumenter.instrument("sponsors.goal_event", {
      actor: current_user,
      listing: sponsorable_sponsors_listing,
      goal: goal,
      action: action,
    })
  end

  def target_for_conditional_access
    return :no_target_for_conditional_access unless sponsorable
    sponsorable
  end
end
