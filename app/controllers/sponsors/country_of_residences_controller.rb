# frozen_string_literal: true

class Sponsors::CountryOfResidencesController < ApplicationController
  include Sponsors::AdminableControllerValidations

  before_action :sponsors_membership_required

  layout "sponsors"

  def update
    membership = sponsorable.sponsors_membership
    membership.country_of_residence = params[:country_of_residence]

    if membership.save
      flash[:notice] = "Updated country or region of residence to #{membership.country_of_residence}"
    else
      flash[:error] = "Failed to set country: #{membership.errors.full_messages.to_sentence}"
    end

    if params[:return_to]
      safe_redirect_to params[:return_to]
    else
      safe_redirect_to sponsorable_dashboard_settings_path(sponsorable)
    end
  end

  def target_for_conditional_access
    return :no_target_for_conditional_access if sponsorable.user?
    sponsorable
  end
end
