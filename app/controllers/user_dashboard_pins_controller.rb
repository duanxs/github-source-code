# frozen_string_literal: true

class UserDashboardPinsController < ApplicationController
  areas_of_responsibility :dashboard

  before_action :login_required
  before_action :ensure_user_exists
  before_action :require_feature

  IndexQuery = parse_query <<~'GRAPHQL'
    query($login: String!, $dashboardPinsLimit: Int!) {
      user(login: $login) {
        isViewer
        ...Views::UserDashboardPins::Index::User
      }
    }
  GRAPHQL

  def index
    data = platform_execute(IndexQuery, variables: {
      login: params[:user_id],
      dashboardPinsLimit: UserDashboardPin::LIMIT_PER_USER_DASHBOARD,
    })
    user = data.user
    return render_404 unless user && user.is_viewer?

    render partial: "user_dashboard_pins/index", locals: { user: user }
  end

  DestroyQuery = parse_query <<~'GRAPHQL'
    mutation($input: DeleteUserDashboardPinInput!) {
      deleteUserDashboardPin(input: $input) {
        user {
          dashboardPinnedItems {
            totalCount
          }
        }
      }
    }
  GRAPHQL

  def destroy
    data = platform_execute(DestroyQuery, variables: {
      input: {
        itemId: params[:item_id],
      },
    })

    if data.errors.any?
      return render_404 if should_graphql_404?(data)
      flash_graphql_error(data)
    else
      user = data.delete_user_dashboard_pin.user
      flash[:notice] = "Your dashboard pins have been updated."
    end

    redirect_back fallback_location: "/"
  end

  CreateQuery = parse_query <<~'GRAPHQL'
    mutation($input: CreateUserDashboardPinInput!) {
      createUserDashboardPin(input: $input) {
        __typename
      }
    }
  GRAPHQL

  def create
    data = platform_execute(CreateQuery, variables: {
      input: {
        itemId: params[:item_id],
      },
    })

    if data.errors.any?
      return render_404 if should_graphql_404?(data)
      flash_graphql_error(data)
    else
      flash[:notice] = "Your dashboard pins have been updated."
    end

    redirect_back fallback_location: "/"
  end

  DeletePinQuery = parse_query <<~'GRAPHQL'
    mutation(
      $input: DeleteUserDashboardPinInput!,
      $dashboardPinsEnabled: Boolean!,
      $dashboardPinsLimit: Int!
    ) {
      deleteUserDashboardPin(input: $input) {
        user {
          ...Views::Dashboard::UserPinsListItems::User
        }
      }
    }
  GRAPHQL

  ReorderPinsQuery = parse_query <<~'GRAPHQL'
    mutation(
      $input: ReorderDashboardPinsInput!,
      $dashboardPinsEnabled: Boolean!,
      $dashboardPinsLimit: Int!
    ) {
      reorderDashboardPins(input: $input) {
        user {
          ...Views::Dashboard::UserPinsListItems::User
        }
      }
    }
  GRAPHQL

  def update
    variables = {
      dashboardPinsLimit: UserDashboardPin::LIMIT_PER_USER_DASHBOARD,
      dashboardPinsEnabled: true,
      input: {
        userId: this_user.global_relay_id,
      },
    }

    user = if params[:unpin_id] # unpinning an item
      variables[:input][:itemId] = params[:unpin_id]
      data = platform_execute(DeletePinQuery, variables: variables)
      return render_graphql_error(data) if data.errors.any?

      data.delete_user_dashboard_pin.user
    else # reordering pins
      variables[:input][:pinnedItemIds] = params[:pinned_item_ids] || []
      data = platform_execute(ReorderPinsQuery, variables: variables)
      return render_graphql_error(data) if data.errors.any?

      data.reorder_dashboard_pins.user
    end

    respond_to do |format|
      format.html do
        render partial: "dashboard/user_pins_list_items", locals: { user: user }
      end
    end
  end

  private

  def require_feature
    render_404 unless user_dashboard_pins_enabled?
  end

  # Ensure the user specified via the login in the URL is a real User.
  def ensure_user_exists
    return if this_user

    if request.xhr?
      head :not_found
    else
      render_404
    end
  end

  def target_for_conditional_access
    return :no_target_for_conditional_access unless this_user
    this_user
  end

  def this_user
    return @this_user if defined?(@this_user)
    @this_user = User.find_by_login(params[:user_id]) if (params[:user_id] && GitHub::UTF8.valid_unicode3?(params[:user_id]))
  end
end
