# frozen_string_literal: true

class IntegrationListingsController < ApplicationController
  areas_of_responsibility :api

  # With the release of Marketplace, this controller is only used in Enterprise,
  # so we don't need to do GitHub.com conditional access checks.
  skip_before_action :perform_conditional_access_checks, only: %w(
    index
    feature
    install
    learn_more
  )

  before_action :sanitize_referrer
  before_action :redirect_to_marketplace, only: :index, if: :marketplace_enabled?
  before_action :redirect_to_marketplace_category, only: :feature, if: :marketplace_enabled?

  layout "site"

  # Public: The Integrations Directory begins here.
  def index
    feature = IntegrationFeature.at_least_visible.find_by_slug(params[:feature])
    @listings = filtered_listings_for_current_user(feature: feature)

    respond_to do |format|
      format.html do
        if request.xhr? && !pjax?
          headers["Cache-Control"] = "no-cache, no-store"
          render_partial_view "integration_listings/list", IntegrationListings::IndexView,
            page: current_page, query: params[:query]
        else
          render "integration_listings/index"
        end
      end
    end
  end

  # Public: Integration feature page
  def feature
    @feature = IntegrationFeature.at_least_visible.find_by_slug(params[:feature])
    return render_404 unless @feature

    @listings = filtered_listings_for_current_user(feature: @feature)

    render "integration_listings/feature"
  end

  # Public
  def install
    @listing = filtered_listings_for_current_user.find_by_slug(params[:name])
    return render_404 unless @listing

    GitHub.instrument("integration.listing_install_click", {
      dimensions: {
        id: @listing.id,
        cid: cid,
        actor_id: actor_id,
      },
    })

    redirect_to @listing.installation_url
  end

  # Public
  def learn_more
    @listing = filtered_listings_for_current_user.find_by_slug(params[:name])
    return render_404 unless @listing

    GitHub.instrument("integration.listing_learn_more_click", {
      dimensions: {
        id: @listing.id,
        cid: cid,
        actor_id: actor_id,
      },
    })

    session[:last_click_integration_listing_id] = @listing.id
    redirect_to @listing.learn_more_url
  end

  # Internal
  def filtered_listings_for_current_user(feature: nil)
    scope = if feature
      feature.listings.preload(:integration)
    else
      IntegrationListing.preload(:integration)
    end

    scope = if logged_in? && current_user.site_admin?
      scope.draft_and_published
    else
      scope.published
    end

    if params[:query].present?
      like = "%#{ActiveRecord::Base.sanitize_sql_like(params[:query])}%"
      scope = scope.where("`integration_listings`.`name` LIKE ?", like)
    end

    scope.paginate(page: current_page, per_page: 102) # Waiting for better categories, needs to be a multiple of three.
  end

  # Internal
  def actor_id
    current_user.id if logged_in?
  end

  # Internal
  def cid
    if cookies[:_octo] =~ /\A[^.]+\.[^.]+\.\d+\.\d+\z/
      cookies[:_octo].split(".")[2..3].join(".")
    end
  end

  private

  def redirect_to_marketplace
    redirect_to marketplace_path
  end

  MarketplaceCategoryQuery = parse_query <<-'GRAPHQL'
    query($slug: String!) {
      marketplaceCategory(slug: $slug) {
        slug
      }
      integrationCategories(slug: $slug) {
        slug
      }
    }
  GRAPHQL

  def redirect_to_marketplace_category
    data = platform_execute(MarketplaceCategoryQuery, variables: {
      slug: params[:feature] || "",
    })

    if category = data.marketplace_category
      redirect_to marketplace_category_path(category.slug)
    elsif category = data.integration_categories.first
      redirect_to works_with_github_category_path(category.slug)
    end
  end

  MarketplaceListingQuery = parse_query <<-'GRAPHQL'
    query($slug: String!) {
      integrationListing(slug: $slug) {
        marketplaceListing {
          resourcePath
        }
        nonMarketplaceListing {
          category {
            slug
          }
        }
      }
    }
  GRAPHQL
end
