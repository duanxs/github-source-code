# frozen_string_literal: true

class RepositoryNotificationSettingsController < AbstractRepositoryController
  include GitHub::RateLimitedRequest

  areas_of_responsibility :notifications

  before_action :ensure_admin_access
  before_action :sudo_filter, except: [:index]

  layout :repository_layout
  javascript_bundle :settings

  # This configures rate limiting for the :update action
  # At present there is no verification of the email addresses users
  # can configure, which creates a vector for spam and abuse, as
  # discussed in https://github.com/github/platform-health-incidents/issues/217
  # These rate limits are aggressive: only a few successful updates in 24 hours
  # are allowed.
  # To improve UX a little, we only check the rate limit here, without incrementing
  # and only increment after a successful update.
  UPDATE_RATE_LIMIT_TTL = 24.hours
  UPDATE_RATE_LIMIT_MAX = 3
  rate_limit_requests \
    only: :update,
    key: :update_rate_limit_key,
    max: UPDATE_RATE_LIMIT_MAX,
    ttl: UPDATE_RATE_LIMIT_TTL,
    at_limit: :at_rate_limit,
    stealthy: true

  def index
    # immediately render the "edit" form if there is no hook setup yet
    return redirect_to(action: :edit) if current_hook.new_record?

    render "edit_repositories/pages/notifications", locals: { repository: current_repository, hook: current_hook }
  end

  def edit
    render "edit_repositories/pages/edit_notifications", locals: { repository: current_repository, hook: current_hook }
  end

  def update
    if GitHub::SchemaDomain.allowing_cross_domain_transactions { current_hook.update(hook_params) }
      flash[:notice] = "Repository notification settings updated."

      # We explicitly update the rate limit only after a successful update
      # to avoid users getting blocked by entering invalid emails and wanting
      # to fix it
      RateLimiter.incr(rate_limit_key, UPDATE_RATE_LIMIT_TTL)
      redirect_to action: :index
    else
      flash.now[:error] = current_hook.errors.full_messages.to_sentence
      edit
    end
  end

  def destroy
    return redirect_to :back if current_hook.new_record?

    if current_hook.destroy
      flash[:notice] = "Repository notification settings cleared."
      redirect_to action: :index
    else
      flash[:notice] = "There was a problem deleting your repository notification settings."
      redirect_to :back
    end
  end

  def at_rate_limit
    flash.now[:error] = "You've updated this setting too many times today, please try again tomorrow."
    edit
  end

  private

  def current_context
    current_repository
  end

  # find or initialize the Hook model for notification settings with default values
  #
  # Returns an instance of Hook configured for "email"
  def current_hook
    @current_hook ||= current_repository.email_hooks.first ||
                current_repository.email_hooks.new(active: true, events: ["push"], creator: current_user)
  end

  # allowable hook, and (nested hook_config) parameters
  # Note: the view should include a hidden_field_tag for `hook[active]` set to `"0"`
  #       to ensure that a value is passed when unchecked
  def hook_params
    params.require(:hook).permit(
      :active,
      { config: [:address, :secret, :send_from_author] },
    )
  end

  def update_rate_limit_key
    "#{self.class.to_s.underscore}.update_check:#{current_repository.id}"
  end
end
