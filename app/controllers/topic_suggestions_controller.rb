# frozen_string_literal: true

class TopicSuggestionsController < AbstractRepositoryController
  areas_of_responsibility :analytics

  before_action :dotcom_required

  statsd_tag_actions only: :index

  def index
    suggestions = if current_repository.can_manage_topics?(current_user)
      current_repository.suggested_topic_names&.take(5) || []
    else
      []
    end

    async_topics = params[:async_topics].blank? || params[:async_topics] == "true"
    decline_forms_only = !params[:decline_forms_only].blank? && params[:decline_forms_only] == "true"

    respond_to do |format|
      format.html do
        render "topic_suggestions/index", layout: false,
                locals: { repository: current_repository, suggestions: suggestions, async_topics: async_topics, decline_forms_only: decline_forms_only }
      end
    end
  end

  AcceptTopicSuggestionQuery = parse_query <<-'GRAPHQL'
    mutation($input: AcceptTopicSuggestionInput!) {
      updateSuggestion: acceptTopicSuggestion(input: $input)
    }
  GRAPHQL

  def accept
    update_suggestion_with_query(AcceptTopicSuggestionQuery,
      variable_keys: [:name, :repositoryId],
    )
  end

  DeclineTopicSuggestionQuery = parse_query <<-'GRAPHQL'
    mutation($input: DeclineTopicSuggestionInput!) {
      updateSuggestion: declineTopicSuggestion(input: $input)
    }
  GRAPHQL

  def decline
    update_suggestion_with_query(DeclineTopicSuggestionQuery,
      variable_keys: [:name, :repositoryId, :reason],
    )
  end

  private

  def update_suggestion_with_query(query, variable_keys:)
    variables = {input: params[:input].slice(*variable_keys)}
    data = platform_execute(query, variables: variables)

    if data.errors.any?
      error_type = data.errors.details[:updateSuggestion]&.first["type"]
      case error_type
      when "NOT_FOUND", "FORBIDDEN"
        if request.xhr?
          return head :not_found
        else
          return render_404
        end
      end

      return head :unprocessable_entity if request.xhr?

      flash[:error] = data.errors.messages.values.join(", ")
    end

    return head :ok if request.xhr?
    redirect_to :back
  end
end
