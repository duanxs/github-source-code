# frozen_string_literal: true

class RepositorySearchController < AbstractRepositoryController
  statsd_tag_actions only: :index

  include GitHub::RateLimitedRequest

  after_action :count_muted_search, only: [:index]
  after_action :clear_response_for_geyser_dark_ship, only: [:dark_ship]

  before_action :validate_dark_ship_request, only: [:dark_ship]
  before_action :validate_toggle_ems_request, only: [:toggle_ems]
  before_action :limit_search_paging

  layout :repository_layout

  rate_limit_requests \
    only: :index,
    if: :repository_search_rate_limit_filter,
    key: :repository_search_rate_limit_key,
    max: :repository_search_rate_limit_max,
    at_limit: :repository_search_rate_limit_record

  # Search landing page
  def index
    setup_params
    strip_analytics_query_string
    return if redirect_for_repo_qualifier_mismatch
    push_service_mapping_context
    local_attribs = {
      scope: :repository,
      current_repository: current_repository,
      originating_request_id: request_id
    }
    render "codesearch/results", locals: local_attribs
  end

  def count
    setup_params
    push_service_mapping_context
    count = Search::CountView.new(query: type_query, type: @type)
    render html: count.render
  end

  # Special endpoint defined to execute and measure dark_shipped Geyser queries
  def dark_ship
    queries_with_dark_ship = Search::QueryHelper.new(get_query, "Code",
        current_user: current_user,
        remote_ip: request.remote_ip,
        repo_id: current_repository.id,
        search_scope: :standard,
        aggregations: false,
        highlight: true,
        sort: [nil, "desc"],
        page: 1,
        per_page: 10,
        state: nil,
        language: language,
        include_explain_output: false,
        geyser_enabled: true,
        dark_ship: dark_ship_request?,
        request_id: request_id,
        originating_request_id: params[:originating_request_id] # request_id of query that triggered dark ship call
    )

    # force-exec the dark ship Geyser query, perform a
    # placeholder render so we can test after_action cleanup
    queries_with_dark_ship.code_query.execute
    render html: "<p>#{params}</p>" # stripped in after_action anyway
  end

  # Special endpoint to provide custom opt-in/out functionality for EMS beta.
  def toggle_ems
    ems_toggleable_feature = Feature.find_by(slug: :exact_match_search)
    enrolled = if ems_toggleable_feature.enrolled?(current_user)
      ems_toggleable_feature.unenroll(current_user)
      false
    else
      ems_toggleable_feature.enroll(current_user)
      true
    end
    render json: { enrolled: enrolled }
  end

  # overrides default in ApplicationController to
  # condition on query type resolved in method scope
  def logical_service
    if !current_repository.nil? && type == "Code"
      if geyser_enabled?
        "#{GitHub::ServiceMapping::SERVICE_PREFIX}/geyser_code_search_query"
      else
        "#{GitHub::ServiceMapping::SERVICE_PREFIX}/code_search_query"
      end
    else
      super
    end
  end

  private

  # Override RepositoryControllerMethods#privacy_check for search_repository_authzd science experiment
  def privacy_check
    return if current_repository.nil?

    authorized = science "search_repository_authzd" do |e|
      e.context logged_in: logged_in?,
                public: current_repository.public?,
                private_mode_enabled: GitHub.private_mode_enabled?,
                subject: current_repository.nwo,
                actor: current_user&.login
      e.use { current_repository.pullable_by?(current_user) }
      e.try { search_repository_authorized(actor: current_user, subject: current_repository) }
      e.compare { |control, candidate| control == candidate.allow? }

      # Reduce noise of this experiment by ignoring network failures
      e.ignore { |_control, candidate| Authzd.ignore_error_in_science?(candidate) }
    end
    return if authorized

    # If the user is trying to view a private repo after receiveing an invitation,
    # redirect to the associated invitation instead.
    if logged_in? && invitation = current_repository.repository_invitations.where(invitee_id: current_user.id).first
      redirect_to invitation.permalink
      return
    end

    respond_to do |format|
      format.html do
        if logged_in? && current_user.site_admin?
          render "admin/locked_repo"
        else
          render_404
        end
      end

      format.all do
        render_404
      end
    end
  end

  def search_repository_authorized(actor:, subject:)
    ::Permissions::Enforcer.authorize(
      action: :search_repository,
      actor: actor,
      subject: subject,
      context: {
        # There's a corner case where some Enterprise deployments may have private mode
        # enabled which would prevent anonymous users from accessing public repos.
        # https://github.com/github/authzd/issues/752 is a proposal to allow Authzd to
        # be able to pick up these instance-wide configuration flags as queryable attributes.
        # In the meantime, we can toggle this functionality using the existing `considers_anonymous`
        # attribute instead of introducing a new context attribute, which if left unspecified
        # could allow anonymous access to repositiories that shouldn't be accessible in some
        # deployments.
        considers_anonymous: !GitHub.private_mode_enabled?,
      },
    )
  end

  # Returns the selected Query whose results are displayed.
  def type_query
    queries[@type]
  end

  def type
    case params[:type].to_s.downcase
    when "issues";      "Issues"
    when "code";        "Code"
    when "commits";     "Commits"
    when "discussions"
      if GitHub.enterprise?
        default_type
      else
        "Discussions"
      end
    when "registrypackages"; "RegistryPackages"
    when "wikis";       "Wikis"
    else
      default_type
    end
  end

  def default_type
    current_repository.code_is_searchable? ? "Code" : "Issues"
  end

  # Determines the language filter from query parameters.
  #
  # Returns a Linguist::Language or nil if no filter is provided.
  def language
    name = params[:l] if params[:l].present?
    name.kind_of?(String) ? Linguist::Language[name] : nil
  end

  def setup_params
    @limit = 10

    @search          = get_query
    @unscoped_search = get_unscoped_query
    @type            = type
    @order           = params[:o].present? ? params[:o] : "desc"
    @sort            = params[:s]
    @language        = language
    @state           = params[:state]
    @classic_search  = params[:classic].present?

    @order = "desc" unless %w[asc desc].include? @order
    @sort_order = [@sort, @order] if @sort.present?

    queries
  end

  def get_query
    case params[:q]
    when Array;  params[:q].join(" ")
    when String; params[:q]
    end
  end

  def get_unscoped_query
    case params[:unscoped_q]
    when Array;  params[:unscoped_q].join(" ")
    when String; params[:unscoped_q]
    end
  end

  def page
    page = params[:p]
    page.is_a?(String) ? page.to_i : 1
  end

  # see https://github.com/github/github/issues/14818
  class NoRepositoryForSearch < RuntimeError; end;

  def queries
    return @queries if defined? @queries

    if current_repository.nil?
      raise NoRepositoryForSearch
    end

    # Include explain output when staff bar is open
    include_explain_output = staff_bar_enabled? && site_admin_performance_stats_mode_enabled?
    @queries = Search::QueryHelper.new(@search, @type,
        current_user: current_user,
        remote_ip: request.remote_ip,
        repo_id: current_repository.id,
        search_scope: :standard,
        aggregations: false,
        highlight: true,
        sort: @sort_order,
        page: page,
        per_page: @limit,
        state: @state,
        language: @language,
        include_explain_output: include_explain_output,
        geyser_enabled: geyser_enabled?,
        dark_ship: false,
        request_id: request_id,
    )
    @queries.issue_query.aggregations = :state
    @queries.code_query.aggregations = :language_id

    @queries
  end

  def count_muted_search
    if params[:muted]
      GitHub.dogstats.increment "search", tags: ["action:mute"]
    end
  end

  def repository_search_rate_limit_max
    logged_in? ? 30 : 10
  end

  def repository_search_rate_limit_filter
    params[:q].present?
  end

  def repository_search_rate_limit_key
    "search_limiter:#{logged_in? ? current_user.id : request.remote_ip}"
  end

  def repository_search_rate_limit_log_key
    "search-#{index_name}-#{authed_or_anon}"
  end

  def repository_search_rate_limit_record
    GitHub.dogstats.increment("search.ratelimited", {tags: ["index:#{index_name}", "logged_in:#{authed_or_anon}"]})
  end

  def index_name
    return "unknown" if current_repository.nil?
    return "geyser_code_search" if type == "Code" && geyser_enabled?
    queries.current.index.name
  end

  def authed_or_anon
    logged_in? ? "auth" : "anon"
  end

  def limit_search_paging
    return render_404 if page > 100
  end

  # Signal to the QueryHelper whether it should use the geyser variant for code searches.
  # All gates to Geyser for the current context should be checked here.
  def geyser_enabled?
    # This makes a couple of DB calls to look up flipper settings so we'll memoize it in case we call it more than once
    return @geyser_enabled if defined?(@geyser_enabled)
    @geyser_enabled = begin
      # Do not use if Geyser search is disabled site-wide
      GitHub.geyser_search_enabled? &&

      # Do not use if the user is not logged in, is not enrolled in the EMS feature flag,
      # or has opted out of the feature
      enrolled_in_exact_match_search? &&

      # Do not use if the repository is not indexed for Geyser
      current_repository.geyser_indexing_enabled?
    end
  end

  # clean the response body if the call to this controller is a dark ship request
  def clear_response_for_geyser_dark_ship
    if dark_ship_request?
      self.response_body = ""
    end
  end

  # Is this request eligible for dark_ship treatment?
  def dark_ship_request?
    # memoize or return from cached value
    return @dark_ship_enabled if defined?(@dark_ship_enabled)
    @dark_ship_enabled = begin
      GitHub.geyser_search_enabled? &&
        current_repository.geyser_indexing_enabled? &&
        type == "Code" &&
        request.path.include?("search/gds") &&
        action_name == "dark_ship"
    end
  end

  # make sure to 404 on Geyser-enabled, repo-scoped code searches
  # that are also invalid dark ship requests
  def validate_dark_ship_request
    if !dark_ship_request?
      GitHub.dogstats.increment "search.geyser_dark_ship.invalid_query", tags: ["query_scope:repository"]
      return render_404
    end
  end

  def validate_toggle_ems_request
    return render_404 unless request.xhr? && eligible_for_exact_match_search?
  end

  # Redirects to the correct repo search url when a Geyser search is being
  # performed if the search string contains exactly one `repo:` qualifier
  # with a value that 1. does not match the repo in the current URL and 2. is
  # owned by the current user.
  def redirect_for_repo_qualifier_mismatch
    return if current_repository.nil?
    return unless type == "Code" && geyser_enabled?

    qualifier_repo_ids = queries.code_query.transformed_query.select { |node| node.field?(:repo_id) }.collect(&:value)

    return unless qualifier_repo_ids.count == 1
    return if qualifier_repo_ids.first == current_repository.id
    queried_repository = Repository.find(qualifier_repo_ids.first)

    authorized = search_repository_authorized(actor: current_user, subject: queried_repository)
    return unless authorized.allow?

    redirect_to repo_search_path(queried_repository.owner, queried_repository, request.query_parameters)
    true
  end

end
