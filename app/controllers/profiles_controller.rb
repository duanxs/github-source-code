# frozen_string_literal: true

class ProfilesController < ApplicationController
  include ProfilesHelper

  DEFAULT_PAGE_SIZE = 30
  FOLLOWERS_FOLLOWING_PAGE_SIZE = 50
  STARRED_TOPICS_LIMIT = 10
  TIMELINE_CONTRIBUTION_TYPES = %w[CREATED_COMMIT CREATED_ISSUE CREATED_PULL_REQUEST
                                   CREATED_REPOSITORY CREATED_PULL_REQUEST_REVIEW
                                   JOINED_ORGANIZATION JOINED_GITHUB GITHUB_ENTERPRISE].freeze

  # We use a custom timeout to avoid unicorns on user profiles
  # like reported in https://github.com/github/github/issues/72326
  DIFF_TIMEOUT = 2

  areas_of_responsibility :avatars, :orgs, :user_profile

  # As of 2019-04-16, show is averaging around 565 rq/sec, or around 3%
  # of total requests
  set_statsd_sample_rate 0.01, only: :show
  statsd_tag_actions only: [:show, :tab_counts]

  include AvatarHelper
  include Orgs::Invitations::RateLimiting
  include UserContributionsHelper
  include ProjectControllerActions
  include Registry::QueryHelper

  # Once a repository has been expanded, how many issues, PRs, or PR reviews should be shown?
  CONTRIBS_PER_REPO_LIMIT = 25

  # How many repositories should be shown in issue, PR review, and PR rollups
  REPOS_PER_ROLLUP_LIMIT = 25

  around_action :record_profile_stats, only: %w( show )
  skip_before_action :cap_pagination, only: %w( show )

  before_action :require_xhr, only: :tab_counts
  before_action :ensure_user_visible, only: %i(show tab_counts)

  def this_organization
    this_user if this_user&.organization?
  end
  helper_method :this_organization

  def tab_counts
    results = {}

    if this_user.organization?

      # This response structure is a bit odd, but it's implemented this way to keep compatibility
      # with the JS front end code, which used to call out for a GraphQL response.
      results[:teams] = {totalCount: this_user.visible_teams_for(current_user).count} if params[:team].present?
      results[:members] = {totalCount: this_user.visible_user_ids_for(current_user, limit: nil).count} if params[:member].present?
      repository_ids = this_user.visible_repositories_for(
        current_user,
        associated_repository_ids: current_user&.associated_repository_ids
      ).where(owner_id: this_user.id).ids
      if params[:discussion].present? && discussions_enabled?
        results[:discussions] = {
          totalCount: this_user.visible_discussions_for(current_user).count
        }
      end
    elsif logged_in?
      repository_ids = this_user.associated_repository_ids(repository_ids: this_user.repository_ids)
    else
      repository_ids = this_user.repositories.public_scope.ids
    end

    results[:packages] = {totalCount: this_user.packages.joins(:package_versions).merge(Registry::PackageVersion.not_deleted).where(repository_id: repository_ids).distinct.count} if params[:package].present?
    results[:repositories] = {totalCount: repository_ids.count} if params[:repo].present?
    results[:projects] = {totalCount: this_user.visible_projects_for(current_user).open_projects.count} if params[:project].present?

    respond_to do |format|
      format.json do
        render json: {data: results}
      end
    end
  end

  ProjectsIndexQuery = parse_query <<-'GRAPHQL'
    query($projectOwnerId: ID!, $first: Int!, $after: String, $query: String) {
      node(id: $projectOwnerId) {
        ...Views::Users::Tabs::Projects::User
      }
    }
  GRAPHQL

  ShowQuery = parse_query <<-'GRAPHQL'
    query(
      $id: ID!,
      $query: String,
      $first: Int,
      $after: String,
      $last: Int,
      $before: String,
      $selected_organization_login: String!,
      $organization_selected: Boolean!,
      $selected_organization_id: ID,
      $activity_overview_enabled: Boolean!,
      $logged_in: Boolean!,
      $overview_tab: Boolean!,
      $projects_tab: Boolean!,
      $stars_tab: Boolean!,
      $repositories_tab: Boolean!,
      $sponsoring_tab: Boolean!,
      $show_teams: Boolean!,
      $from: DateTime,
      $to: DateTime,
      $timelineFrom: DateTime,
      $timelineTo: DateTime,
      $timelineContributionTypes: [ContributionsCollectionContributionType],
      $is_viewer: Boolean!,
      $stars_order_by: StarOrder,
      $language: String,
      $contribsPerRepoLimit: Int,
      $reposPerRollupLimit: Int,
      $diffTimeout: Int,
      $type: RepositoryType,
      $pinned_item_limit: Int!,
      $mutualFollowersEnabled: Boolean!
    ) {
      ...Views::Users::Tabs::ActivityOverview::RootFragment @include(if: $activity_overview_enabled)
      node(id: $id) {
        ...Views::Users::Show::User
      }
      viewer @include(if: $logged_in)
      organization(login: $selected_organization_login) @include(if: $organization_selected) {
        databaseId
        ...Views::Users::Show::Organization
      }
    }
  GRAPHQL

  RepositoryIndexQuery = parse_query <<-'GRAPHQL'
    query(
      $id: ID!,
      $query: String,
      $first: Int,
      $last: Int,
      $after: String,
      $before: String,
      $language: String,
      $type: RepositoryType,
    ) {
      node(id: $id) {
        ...Views::Users::Tabs::Repositories::RepositoryOwner
      }
    }
  GRAPHQL

  SponsoringIndexQuery = parse_query <<-'GRAPHQL'
    query(
      $id: ID!,
      $first: Int,
      $last: Int,
      $after: String,
      $before: String,
    ) {
      node(id: $id) {
        ...Views::Users::Tabs::Sponsoring::User
      }
    }
  GRAPHQL

  OrganizationShowQuery = parse_query <<-'GRAPHQL'
    query(
      $id: ID!,
      $logged_in: Boolean!,
      $pinned_item_limit: Int!,
      $show_admin_stuff: Boolean!
    ) {
      node(id: $id) {
        ...Orgs::Repositories::IndexPageView::OrganizationFragment
        ...Views::Community::OrgInteractionLimitsBanner::Node
        ...Views::Orgs::Repositories::VerifiedDomains::Fragment
        ...Views::Orgs::Repositories::Index::Organization
        ...Views::Orgs::Repositories::Header::Organization
        ...Views::Sponsors::SponsorableProfileSponsors::Sponsorable
      }
      viewer @include(if: $logged_in) {
        ...Views::Orgs::Repositories::Index::Viewer
        ...Orgs::Repositories::IndexPageView::ViewerFragment
      }
    }
  GRAPHQL

  def show
    return render_404 unless tab_exists_for_user?

    return if render_user_event

    @feed_title = "#{this_user}’s Activity"
    if this_user.organization?
      return render_404 if this_user.deleted?

      @current_organization = this_user
      @current_organization.reset_public_members! if GitHub.cache.skip
      set_hovercard_subject(this_user)
      record_org_profile_visitor_stats
    end

    respond_to do |format|
      format.html {
        # Make sure the browser caches AJAX responses separately from regular
        # HTML responses.
        response.headers["Vary"] = "X-Requested-With"

        hydro_tracking.publish_user_profile_page_view

        if this_user.organization?
          render_organization_profile
        else
          render_user_profile
        end
      }
      format.keys { render plain: this_user.verified_keys.map { |k| "#{k}\n" }.join }
      format.gpg  { render plain: this_user.gpg_keys.primary_keys.keychain }
      format.atom do
        @event_feed_user = this_user
        render "events/index", layout: false
      end
      format.json do
        docs_url = "#{GitHub.developer_help_url}/v3/activity/events/#list-public-events-performed-by-a-user"
        render json: gone_payload(docs_url), status: 410
      end
      format.png do
        redirect_to avatar_url_for(this_user, params[:size])
      end
      format.all do
        head :not_acceptable
      end
    end
  end

  def following_tab?
    params[:tab] == "following"
  end
  helper_method :following_tab?

  def followers_tab?
    params[:tab] == "followers"
  end
  helper_method :followers_tab?

  def stars_tab?
    params[:tab] == "stars"
  end
  helper_method :stars_tab?

  def packages_tab?
    return @packages_tab if defined? @packages_tab
    @packages_tab = params[:tab] == "packages"
  end
  helper_method :packages_tab?

  def repositories_tab?
    params[:tab] == "repositories"
  end
  helper_method :repositories_tab?

  def projects_tab?
    params[:tab] == "projects"
  end
  helper_method :projects_tab?

  def sponsoring_tab?
    return false unless GitHub.sponsors_enabled?
    params[:tab] == "sponsoring"
  end
  helper_method :sponsoring_tab?

  def overview_tab?
    return @overview_tab if defined? @overview_tab
    result = params[:tab] == "overview"
    result = true unless members_tab? || repositories_tab? || packages_tab? || projects_tab? || sponsoring_tab?
    result = false if stars_tab? || followers_tab? || following_tab?
    @overview_tab = result
  end
  helper_method :overview_tab?

  private

  # Render a 404 if there is no user or the user is hidden from the viewer
  def ensure_user_visible
    return if this_user && !this_user.hide_from_user?(current_user)
    render_404
  end

  helper_method :this_user

  def tab_exists_for_user?
    return false if stars_tab? && this_user.organization?
    return false if members_tab? && !this_user.organization?
    return false if sponsoring_tab? && hide_sponsoring_tab?
    return false if packages_tab? && !PackageRegistryHelper.show_packages?
    true
  end

  def hide_sponsoring_tab?
    this_user.organization? && !current_user&.corporate_sponsors_credit_card_enabled?
  end

  # Handle auth specifics for feed requests.
  include GitHub::Authentication::Feed

  def render_organization_profile
    if members_tab?
      redirect_to org_people_path(@current_organization)
    elsif request.xhr? || pjax?
      variables = {
        id: this_user.global_relay_id,
        logged_in: logged_in?,
        pinned_item_limit: ProfilePin::LIMIT_PER_PROFILE,
        show_admin_stuff: this_user.adminable_by?(current_user),
      }
      result = platform_execute(OrganizationShowQuery, variables: variables)
      data = result.node

      # Show the partial with two columns
      render_partial_view("orgs/repositories/columns", Orgs::Repositories::IndexPageView,
        organization: @current_organization,
        current_page: current_page,
        type_filter: params[:type],
        phrase: search_query,
        language: params[:language],
        graphql_org: data
      )
    else
      variables = {
        id: this_user.global_relay_id,
        logged_in: logged_in?,
        pinned_item_limit: ProfilePin::LIMIT_PER_PROFILE,
        show_admin_stuff: this_user.adminable_by?(current_user),
      }
      result = platform_execute(OrganizationShowQuery, variables: variables)
      data = result.node
      viewer = result.viewer
      locals = { data: data, viewer: viewer, sponsoring_tab: sponsoring_tab? }

      if sponsoring_tab?
        locals[:sponsorships] = paged_sponsorships
      end

      # Show the org profile
      render_template_view("orgs/repositories/index", Orgs::Repositories::IndexPageView,
        {
          organization: @current_organization,
          current_page: current_page,
          type_filter: params[:type],
          phrase: search_query,
          rate_limited: org_invite_rate_limited?,
          language: params[:language],
          graphql_org: data,
          viewer: viewer,
        },
        locals: locals
      )
    end
  end

  def search_query
    return @search_query if defined? @search_query
    @search_query = if projects_tab?
      project_index_query
    else
      raw_query = params[:q]
      raw_query if raw_query.is_a?(String)
    end
  end

  def render_user_profile
    if repositories_tab? && request.xhr? && !pjax?
      render_user_repositories_tab
    elsif projects_tab? && request.xhr? && !pjax?
      render_user_projects_tab
    elsif packages_tab? && request.xhr? && !pjax?
      render_user_packages_tab
    elsif sponsoring_tab? && request.xhr? && !pjax?
      render_user_sponsoring_tab
    else
      if (request.xhr? || pjax?) && request.headers["X-PJAX-Container"] != "#js-pjax-container"
        if should_render_year_list?
          render_year_list
        else
          render_contribution_activity
        end
      else
        variables = activity_overview_variables.merge(
          id: this_user.global_relay_id,
          query: search_query,
          activity_overview_enabled: activity_overview_enabled?,
          overview_tab: overview_tab?,
          packages_tab: packages_tab?,
          projects_tab: projects_tab?,
          stars_tab: stars_tab?,
          repositories_tab: repositories_tab?,
          sponsoring_tab: sponsoring_tab?,
          type: repository_type,
          language: params[:language],
          stars_order_by: stars_order_by,
          is_viewer: this_user == current_user,
          timelineContributionTypes: TIMELINE_CONTRIBUTION_TYPES,
          contribsPerRepoLimit: CONTRIBS_PER_REPO_LIMIT,
          reposPerRollupLimit: REPOS_PER_ROLLUP_LIMIT,
          diffTimeout: DIFF_TIMEOUT,
          pinned_item_limit: ProfilePin::LIMIT_PER_PROFILE,
          mutualFollowersEnabled: mutual_followers_enabled?,
        )
        set_graphql_month_date_params_on(variables)
        page_size = if stars_tab?
          Star.per_page
        elsif repositories_tab?
          Repository.per_page
        elsif following_tab? || followers_tab?
          FOLLOWERS_FOLLOWING_PAGE_SIZE
        else
          DEFAULT_PAGE_SIZE
        end
        variables.merge!(graphql_pagination_params(page_size: page_size))
        data = platform_execute(ShowQuery, variables: variables)

        if user = data.node
          locals = {
            data: data,
            user: user,
            viewer: data.viewer,
            org: data.organization,
            starred_topics: this_user.starred_topics(limit: STARRED_TOPICS_LIMIT + 1),
          }

          if followers_tab?
            followers = Following.for(this_user, viewer: current_user, order_by: {field: "followed_at", direction: "DESC"}, type: :follower).paginate(page: current_page, per_page: page_size).includes(user: :profile)

            locals.merge!(
              ar_user: this_user,
              followers: followers,
            )
          end

          if following_tab?
            followings = Following.for(this_user, viewer: current_user, order_by: {field: "followed_at", direction: "DESC"}, type: :followed_user).paginate(page: current_page, per_page: page_size).includes(following: :profile)

            locals.merge!(
              ar_user: this_user,
              followings: followings,
            )
          end

          if overview_tab?
            locals[:collector] = contribution_collector_for_discussions
          end

          locals[:show_acv_badge] = show_acv_badge?

          if packages_tab?
            locals[:packages] = paged_packages
          end

          render "users/show", locals: locals
        else
          render_404
        end
      end
    end
  end

  def contribution_collector_for_discussions
    from, to = month_date_params
    time_range = from..to
    Contribution::Collector.new(
      user: this_user,
      viewer: current_user,
      contribution_classes: [Contribution::AnsweredDiscussion, Contribution::CreatedDiscussion],
      time_range: time_range,
      excluded_organization_ids: unauthorized_organization_ids,
    )
  end

  def render_user_repositories_tab
    variables = activity_overview_variables.merge(
      id: this_user.global_relay_id,
      query: search_query,
      repositories_tab: repositories_tab?,
      type: repository_type,
      language: params[:language],
      is_viewer: this_user == current_user,
    )
    variables.merge!(graphql_pagination_params(page_size: Repository.per_page))
    data = platform_execute(RepositoryIndexQuery, variables: variables)

    render partial: "users/tabs/repositories", locals: {
      data: data,
      user: data.node,
    }
  end

  def render_user_projects_tab
    params[:q] ||= "is:open"

    variables = {
      projectOwnerId: this_user.global_relay_id,
      first: DEFAULT_PAGE_SIZE,
      after: params[:cursor],
      query: project_index_query,
    }
    variables.merge!(graphql_pagination_params(page_size: DEFAULT_PAGE_SIZE))
    result = platform_execute(ProjectsIndexQuery, variables: variables)
    render partial: "users/tabs/projects", locals: {
      query: params[:q],
      user: result.node,
      user_session: user_session,
    }
  end

  def render_user_packages_tab
    render partial: "registry/packages/filtered_packages", locals: {
      owner: current_user,
      packages: paged_packages,
      params: params,
    }
  end

  def render_user_sponsoring_tab
    variables = { id: this_user.global_relay_id }
    variables.merge!(graphql_pagination_params(page_size: DEFAULT_PAGE_SIZE))

    result = platform_execute(SponsoringIndexQuery, variables: variables)

    render partial: "users/tabs/sponsoring", locals: { user: result.node }
  end

  def members_tab?
    params[:tab] == "members"
  end

  def render_user_event
    if request.format && (request.format.atom? || request.format.json?)
      # TODO: store latest event id/timestamp somewhere so that freshness
      #       can be checked without loading the events.
      event = current_events.first
      fresh_when strong_etag: event, template: false
      performed?
    else
      false
    end
  end

  # Private: Actions that can response to atom requests.
  #
  # Returns an Array or Strings.
  def feed_actions
    %w(show)
  end

  def events_timeline_key
    @events_timeline_key ||= "actor:#{this_user.id}:public"
  end

  def hydro_tracking
    HydroTracking.new(
      profile_user: this_user,
      profile_viewer: current_user,
      scoped_organization: scoped_organization,
      tab_param: params[:tab],
      profile_readme_rendered: show_profile_readme?,
    )
  end

  def invite_rate_limited_organization
    if this_user.organization?
      this_user
    end
  end

  def target_for_conditional_access
    return :no_target_for_conditional_access unless this_user.present?
    this_user
  end

  def record_org_profile_visitor_stats
    role = if logged_in?
      if @current_organization.direct_or_team_member?(current_user)
        "member"
      else
        "non_member"
      end
    else
      "anonymous"
    end
    GitHub.dogstats.increment("organization", tags: ["action:profile_view", "role:#{role}"])
  end

  ContributionActivityQuery = parse_query <<-'GRAPHQL'
    query(
      $id: ID!,
      $selected_organization_id: ID,
      $selected_organization_login: String!,
      $timelineFrom: DateTime,
      $timelineTo: DateTime,
      $timelineContributionTypes: [ContributionsCollectionContributionType],
      $organization_selected: Boolean!,
      $contribsPerRepoLimit: Int,
      $reposPerRollupLimit: Int,
      $diffTimeout: Int
    ) {
      node(id: $id) {
        ...Views::Users::Tabs::ContributionActivity::User
      }
      organization(login: $selected_organization_login) @include(if: $organization_selected) {
        databaseId
        ...Views::Users::Tabs::ContributionActivity::Organization
      }
    }
  GRAPHQL

  def render_contribution_activity
    headers["Cache-Control"] = "no-cache, no-store"
    variables = {
      id: this_user.global_relay_id,
      selected_organization_login: params[:org] || "",
      organization_selected: params[:org].present?,
      timelineContributionTypes: TIMELINE_CONTRIBUTION_TYPES,
      contribsPerRepoLimit: CONTRIBS_PER_REPO_LIMIT,
      reposPerRollupLimit: REPOS_PER_ROLLUP_LIMIT,
      diffTimeout: DIFF_TIMEOUT,
    }
    if activity_overview_enabled? && scoped_organization
      variables[:selected_organization_id] = scoped_organization.global_relay_id
    end
    set_graphql_month_date_params_on(variables)
    data = platform_execute(ContributionActivityQuery, variables: variables)
    locals = {
      include_header: params[:include_header] != "no",
      user: data.node,
      org: data.organization,
      collector: contribution_collector_for_discussions
    }

    render partial: "users/tabs/contribution_activity", locals: locals
  end

  YearListQuery = parse_query <<-'GRAPHQL'
    query(
      $id: ID!
      $selected_organization_id: ID,
      $selected_organization_login: String!,
      $timelineFrom: DateTime,
      $timelineTo: DateTime,
      $organization_selected: Boolean!
    ) {
      node(id: $id) {
        ... on User {
          contributionsCollection(organizationID: $selected_organization_id, from: $timelineFrom, to: $timelineTo) {
            ...Views::Users::YearList::ContributionsCollection
          }
        }
      }
      organization(login: $selected_organization_login) @include(if: $organization_selected) {
        ...Views::Users::YearList::Organization
      }
    }
  GRAPHQL

  def render_year_list
    variables = {
      id: this_user.global_relay_id,
      selected_organization_login: params[:org] || "",
      organization_selected: params[:org].present?,
    }
    if activity_overview_enabled? && scoped_organization
      variables[:selected_organization_id] = scoped_organization.global_relay_id
    end
    set_graphql_month_date_params_on(variables)
    data = platform_execute(YearListQuery, variables: variables)
    render partial: "users/year_list", locals: {
      contrib_collection: data.node.contributions_collection, org: data.organization
    }
  end

  def should_render_year_list?
    params[:year_list] == "1"
  end

  STAR_SORT_FIELD_MAPPING = {
    "updated" => "PUSHED_AT",
    "stars" => "STARS",
    "created" => "STARRED_AT",
  }.freeze

  def stars_order_by
    default_field = "STARRED_AT"
    default_direction = "DESC"
    field = STAR_SORT_FIELD_MAPPING[params[:sort]] || default_field
    direction = (params[:direction] || default_direction).upcase

    field = default_field unless STAR_SORT_FIELD_MAPPING.values.include?(field)
    direction = default_direction unless %w[ASC DESC].include?(direction)

    { field: field, direction: direction }
  end

  def repository_type
    return unless params[:type]&.respond_to?(:upcase)

    type = params[:type].upcase
    valid_types = Platform::Enums::RepositoryType.values.values.map(&:graphql_name)
    unless valid_types.include?(type)
      type = nil
      params[:type] = nil
    end
    type
  end

  def mutual_followers_enabled?
    GitHub.flipper[:mutual_followers].enabled?(current_user)
  end

  def show_acv_badge?
    GitHub.flipper[:acv_badge].enabled?(current_user) &&
      this_user.can_have_acv_badge? &&
      this_user.profile_settings.acv_badge_enabled?
  end

  def record_profile_stats
    return yield unless logged_in? && this_user && !this_user.organization?

    before = Time.now
    yield
    duration = Time.now - before

    tags = ShowProfileStats.tags(
      activity_overview_rendered: activity_overview_enabled?,
      subject_user: this_user,
      viewer: current_user,
    )

    GitHub.dogstats.timing("user.profile.request", duration * 1000, tags: tags)
  end

  helper_method :parsed_projects_query
  helper_method :project_index_query

  def paged_packages
    results, packages = packages_for_query(
      current_user: current_user,
      user_session: user_session,
      owner: this_user,
      query: search_query,
      package_type: ecosystem_param,
      visibility: visibility_param,
      sort: sort_param,
      page: current_page,
      per_page: DEFAULT_PAGE_SIZE,
    )

    WillPaginate::Collection.create(current_page, DEFAULT_PAGE_SIZE) do |pager|
      pager.replace(packages)
      pager.total_entries ||= results.total_entries
    end
  end

  def paged_sponsorships
    return @paged_sponsorships if defined?(@paged_sponsorships)

    @paged_sponsorships = @current_organization.
      sponsorships_as_sponsor.
      active.
      includes(sponsorable: [:sponsors_membership, :profile]).
      paginate(page: current_page, per_page: DEFAULT_PAGE_SIZE)
  end
end
