# frozen_string_literal: true

class ProfilePinsController < ApplicationController
  areas_of_responsibility :user_profile

  include UserContributionsHelper

  before_action :login_required, only: %w(reorder_pinned_items pinned_items_modal set_pinned_items)
  before_action :ensure_user_exists,
    only: %w(pinned_items_modal reorder_pinned_items set_pinned_items)

  PinnableItemsQuery = parse_query <<-'GRAPHQL'
    query($id: ID!, $reposLimit: Int!, $reposCursor: String) {
      node(id: $id) {
        ... on ProfileOwner {
          viewerCanChangePinnedItems
          ...Views::ProfilePins::PinnableItems::ProfileOwner
        }
      }
    }
  GRAPHQL

  def pinnable_items
    return render_404 unless this_user

    data = platform_execute(PinnableItemsQuery, variables: {
      id: this_user.global_relay_id,
      reposLimit: 250,
      reposCursor: params[:after],
    })
    profile_owner = data.node
    return head(:forbidden) unless profile_owner.viewer_can_change_pinned_items?

    respond_to do |format|
      format.html do
        render partial: "profile_pins/pinnable_items",
               locals: { profile_owner: profile_owner }
      end
    end
  end

  ReorderPinnedItemsQuery = parse_query <<~'GRAPHQL'
    mutation($input: ReorderProfilePinsInput!, $pinned_item_limit: Int!) {
      reorderProfilePins(input: $input) {
        profileOwner {
          ...Views::ProfilePins::PinnedItems::ProfileOwner
          pinnedItems(first: $pinned_item_limit) {
            nodes {
              ...Views::ProfilePins::PinnedItems::PinnableItem
            }
          }
        }
      }
    }
  GRAPHQL

  def reorder_pinned_items
    data = platform_execute(ReorderPinnedItemsQuery, variables: {
      input: {
        pinnedItemIds: params[:pinned_item_ids] || [],
        profileOwnerId: this_user.global_relay_id,
      },
      pinned_item_limit: ProfilePin::LIMIT_PER_PROFILE,
    })

    if data.errors.any?
      error_type = data.errors.details[:reorderProfilePins]&.first["type"]
      return head(:not_found) if %w[NOT_FOUND SERVICE_UNAVAILABLE].include?(error_type)

      message = data.errors.messages.values.join(", ")
      status = error_type == "FORBIDDEN" ? :forbidden : :unprocessable_entity
      return render plain: message, status: status
    end

    profile_owner = data.reorder_profile_pins.profile_owner
    pinned_items = profile_owner.pinned_items.nodes

    respond_to do |format|
      format.html do
        render partial: "profile_pins/pinned_items",
               locals: { pinned_items: pinned_items, profile_owner: profile_owner }
      end
    end
  end

  PinnedItemsModalQuery = parse_query <<~'GRAPHQL'
    query($id: ID!, $reposLimit: Int!, $reposCursor: String) {
      node(id: $id) {
        ... on ProfileOwner {
          viewerCanChangePinnedItems
          ...Views::ProfilePins::PinnedItemsModal::ProfileOwner
        }
      }
    }
  GRAPHQL

  def pinned_items_modal
    data = platform_execute(PinnedItemsModalQuery, variables: {
      id: this_user.global_relay_id,
      reposLimit: 250,
    })
    profile_owner = data.node
    return head :forbidden unless profile_owner.viewer_can_change_pinned_items?

    respond_to do |format|
      format.html do
        render partial: "profile_pins/pinned_items_modal",
               locals: { profile_owner: profile_owner }
      end
    end
  end

  SetPinnedItemsQuery = parse_query <<~'GRAPHQL'
    mutation($input: SetProfilePinsInput!) {
      setProfilePins(input: $input) {
        profileOwner {
          login
          ... on User {
            isViewer
          }
          pinnedItems {
            totalCount
          }
        }
      }
    }
  GRAPHQL

  def set_pinned_items
    data = platform_execute(SetPinnedItemsQuery, variables: {
      input: {
        pinnedItemIds: params[:pinned_item_ids] || [],
        profileOwnerId: this_user.global_relay_id,
      },
    })

    if data.errors.any?
      error_type = data.errors.details[:setProfilePins]&.first["type"]
      return render_404 if %w[NOT_FOUND SERVICE_UNAVAILABLE].include?(error_type)

      message = data.errors.messages.values.join(", ")
      status = error_type == "FORBIDDEN" ? :forbidden : :unprocessable_entity
      return render plain: message, status: status
    end

    profile_owner = data.set_profile_pins.profile_owner
    pin_count = profile_owner.pinned_items.total_count
    profile_owner_is_viewer = profile_owner.try(:is_viewer?)

    message = if pin_count < 1
      if profile_owner_is_viewer
        "Your popular repositories will now be shown instead of your pins."
      else
        "No repositories or gists are pinned now."
      end
    else
      subject = if profile_owner_is_viewer
        "Your"
      else
        "#{profile_owner.login}’s"
      end
      suffix = pin_count > 1 ? " Drag and drop to reorder them." : nil
      "#{subject} pins have been updated.#{suffix}"
    end

    flash[:notice] = message
    redirect_to user_path(profile_owner.login)
  end

  private

  # Ensure the user specified via the login in the URL is a real User or Organization.
  def ensure_user_exists
    return if this_user

    if request.xhr?
      head :not_found
    else
      render_404
    end
  end

  def target_for_conditional_access
    return :no_target_for_conditional_access unless this_user
    this_user
  end
end
