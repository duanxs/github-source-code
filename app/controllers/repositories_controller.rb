# frozen_string_literal: true

class RepositoriesController < AbstractRepositoryController
  map_to_service :repo_creation, only: [:new, :create, :index, :check_name]
  map_to_service :star, only: [:watchers, :stargazers, :stargazers_you_know]

  CONDITIONAL_ACCESS_BYPASS_ACTIONS = %w(check_name).freeze
  CONDITIONAL_ACCESS_BYPASS_PUBLIC_REPO_ACTIONS = %w(packages_list contributors_list used_by_list sponsors_list environment_status).freeze

  statsd_tag_actions only: [
    :packages_list,
    :contributors_list,
    :used_by_list,
    :sponsors_list,
    :environment_status,
    :create,
    :new
  ]

  include BillingSettingsHelper
  include OrganizationsHelper
  include Settings::SecurityAnalysisSettings

  if GitHub.enterprise?
    skip_before_action :ask_the_gatekeeper, only: [:go_metatag]
    skip_before_action :enforce_private_mode, only: [:go_metatag]
    skip_before_action :privacy_check, only: [:go_metatag]
    skip_before_action :network_privilege_check, only: [:go_metatag]
    before_action :ensure_repository_specified, only: [:go_metatag]
  end

  skip_before_action :authorization_required
  before_action :login_required, only: %w(create check_name)
  before_action :login_required_with_redirect, only: :new
  before_action :content_authorization_required, only: %w(create new)
  before_action :enforce_plan_supports_insights, only: :pulse

  before_action :network_privilege_for_opt_in, only: %w(opt_in_to_view)
  before_action :network_privilege_check, except: %w(opt_in_to_view)

  layout :repository_layout

  def index
    return render_404 if params[:organization_id]

    respond_to do |format|
      format.html do
        if GitHub.enterprise?
          render "repositories/recent", layout: "application"
        else
          redirect_to "/trending"
        end
      end
      format.atom do
        render "repositories/index", layout: false
      end
    end
  end

  def new
    prepare_new_view
    render "repositories/new", layout: "application"
  end

  private def visibility_from_params
    return repository_params[:visibility] if repository_params[:visibility]
    return "public" if repository_params[:public]&.to_s == "true"
    "private"
  end

  def create
    requested_owner = User.find_by_login(params[:owner])

    return render_404 unless requested_owner
    visibility = visibility_from_params
    unless requested_owner.can_create_repository?(current_user, visibility: visibility)
      flash[:error] = "Members of that org can not create #{visibility} repositories."
      prepare_new_view
      return render "repositories/new", layout: "application"
    end

    if missing_billing_information_for?(requested_owner)
      flash[:error] = "Cannot upgrade without any billing information"
      prepare_new_view
      return render "repositories/new", layout: "application"
    end

    # Honor SAML auth for template repo's org _before_ we create a new empty repo.
    if template_repository && !required_external_identity_session_present?(target: template_repository.owner)
      render_external_identity_session_required(target: template_repository.owner)
      return
    end

    owner_has_billing = requested_owner.has_valid_payment_method?
    reflog_data = request_reflog_data(requested_owner, computed_repository_params.to_h, "initial commit")
    result = create_repository(requested_owner, reflog_data)

    if result.success?
      track_ga_event(result.repository, owner_has_billing)

      flash[:just_created] = true
      license = result.repository.license_template
      license = "none" if license.blank?
      GitHub.dogstats.increment("license_picker.pick", tags: ["license:#{license}"])

      unless params[:template_repository_id].blank?
        cloner = CloneTemplateRepository.new(template_repository,
          actor: current_user,
          new_repository: result.repository,
          copy_branches: params[:include_all_branches] == "1",
        )

        unless cloner.clone
          flash[:warn] = "Could not clone: #{cloner.error}"
        end
      end

      if result.template_hook_failure
        flash[:error] = "A pre-receive hook prevented content from being added to your new repository: #{result.template_hook_failure}"
      end

      # Initialize security & analysis settings for the new repo.
      # These settings are managed from the 'Security & analysis' settings pages at repo and org levels.
      initialize_security_settings(requested_owner, result.repository)

      install_selected_marketplace_apps(requested_owner, result.repository)

      if result.repository.user_configuration_repository?
        requested_owner.profile_readme_opt_in = true
        requested_owner.profile.save
      end

      return redirect_to clean_repository_path(result.repository)
    else
      @repository = result.repository
      @owner = @repository&.owner ? @repository.owner : requested_owner

      if !result.privatization_billing_error? || current_user.has_any_trade_restrictions?
        flash.now[:error] = result.error_message.to_s
      end

      return render "repositories/new", layout: "application"
    end
  end

  def install_selected_marketplace_apps(requested_owner, new_repository)
    manual_install = params[:quick_install][requested_owner.login] if params[:quick_install]
    manual_install ||= {}

    manual_install_successes = []
    manual_install_failures = []
    auto_install_successes = []

    Marketplace::Listing.quick_installable_for(requested_owner).each do |listing, auto_install|
      if auto_install
        auto_install_successes << listing
      elsif manual_install[listing.id.to_s].present?
        installation = requested_owner.integration_installations.find_by(integration_id: listing.listable_id)
        install_result = IntegrationInstallation::Editor.perform(
          installation,
          repositories: installation.repositories + [new_repository],
          editor: current_user,
        )

        if install_result.success?
          manual_install_successes << listing
        else
          manual_install_failures << listing
        end
      end
    end

    install_successes = auto_install_successes + manual_install_successes

    if install_successes.any?
      verb = "was".pluralize(install_successes.size)
      flash[:notice] = "#{install_successes.map(&:name).to_sentence} #{verb} installed on this repository"
    end
    if manual_install_failures.any?
      failed_apps = manual_install_failures.map(&:name).to_sentence(two_words_connector: " or ", last_word_connector: " or ")
      flash[:error] = "Unable to install #{failed_apps} on this repository"
    end

    if install_successes.any? || manual_install_failures.any?
      GitHub.instrument("marketplace.new_repo_quick_install") do |payload|
        payload[:action] = :installed
        payload[:current_user_id] = current_user.id
        payload[:repository_id] = new_repository.id
        payload[:auto_install_listings] = auto_install_successes.map { |listing| { listing_id: listing.id } }
        payload[:manual_install_listings] = manual_install_successes.map { |listing| { listing_id: listing.id } }
        payload[:failed_manual_install_listings] = manual_install_failures.map { |listing| { listing_id: listing.id } }
      end
    end
  end

  # Used for the autochecker on the new repository form and from the rename
  # repository form to validate repository names on the fly.
  def check_name
    if params[:owner]
      owner = User.find_by_login(params[:owner])
      return render_404 unless owner
    else
      owner = current_user
    end

    return render_404 unless owner.can_create_repository?(current_user) || can_rename_repository?(owner)

    if params[:current_name].present? &&
      repo = owner.find_repo_by_name(params[:current_name])
      repo.name = params[:value]
    else
      repo = owner.repositories.build(name: params[:value])
    end
    repo.valid?

    respond_to do |format|
      format.html_fragment do
        if repo.errors[:name].any?
          render partial: "repositories/check_name_error",
                 locals: { repository: repo },
                 status: :unprocessable_entity,
                 formats: :html
        elsif params[:value] != repo.name
          render partial: "repositories/check_name_warning",
                 locals: { repository: repo },
                 status: :accepted,
                 formats: :html
        else
          render html: "#{repo.name} is available."
        end
      end
    end
  end

  def watchers
    @watchers_response = current_repository.watchers(current_page, 51)
    @watchers = @watchers_response.value
    render "repositories/watchers"
  end

  StargazersQuery = parse_query <<-'GRAPHQL'
    query(
      $id: ID!,
      $first: Int,
      $last: Int,
      $after: String,
      $before: String,
    ) {
      node(id: $id) {
        ...Views::Repositories::Stargazers::Repository
      }
    }
  GRAPHQL

  def stargazers
    variables = { id: current_repository.global_relay_id }
    variables.merge!(graphql_pagination_params(page_size: Repository.per_page))
    data = platform_execute(StargazersQuery, variables: variables)
    return handle_stargazers_query_error(data) unless data.node
    repository = data.node
    render "repositories/stargazers", locals: { repository: repository }
  end

  private def handle_stargazers_query_error(data)
    # Ignore and return 404 on bad cursor arg: https://github.com/github/github/issues/108919
    return render_404 if data.errors&.details[:node]&.first["type"] == "INVALID_CURSOR_ARGUMENTS"
    # Report and return 500 for any other GQL error
    err = Platform::Errors::Internal.new
    err.set_backtrace(caller)
    error_data = {
      graphql_errors: data.errors&.all,
      url: request.url,
    }
    Failbot.report(err, error_data)
    render_optional_error_file(500)
  end

  def stargazers_you_know
    return redirect_to_login(stargazers_you_know_url) unless logged_in?
    @stargazers_you_know =
        User.following_starred(current_user.id, current_repository.id).simple_paginate(page: current_page, per_page: 50)

    render "repositories/stargazers_you_know"
  end

  def contributors
    redirect_to contributors_graph_url
  end

  def packages_list
    packages = current_repository.packages.joins(:package_versions).merge(Registry::PackageVersion.not_deleted).distinct

    respond_to do |format|
      format.html do
        render partial: "files/sidebar/packages_list", locals: {
          packages: packages,
          items_to_show: params[:items_to_show].to_i,
        }
      end
    end
  end

  def contributors_list
    items_to_show = params[:items_to_show].to_i
    respond_to do |format|
      format.html do
        render partial: "files/sidebar/contributors_list", locals: {
          items_to_show: items_to_show
        }
      end
    end
  end

  SidebarUsedByQuery = parse_query <<-'GRAPHQL'
    query($id: ID!, $dependentType: DependencyGraphDependentType!, $dependentsFirst: Int, $packageId: String) {
      repository: node(id: $id) {
        ...Views::Files::Sidebar::UsedByList::Repository
      }
    }
  GRAPHQL

  DEPENDENTS_LIMIT = 8

  def used_by_list
    respond_to do |format|
      format.html_fragment do
        data = platform_execute(
          SidebarUsedByQuery,
          variables: {
            id: current_repository.global_relay_id,
            dependentsFirst: DEPENDENTS_LIMIT,
            dependentType: PlatformTypes::DependencyGraphDependentType::REPOSITORY,
            packageId: current_repository.used_by_package_id,
          },
        )

        render partial: "files/sidebar/used_by_list", locals: { repository: data.repository, dependents_limit: DEPENDENTS_LIMIT }, formats: :html
      end
    end
  end

  SidebarSponsorsQuery = parse_query <<-'GRAPHQL'
    query($sponsorableUsersIds: [ID!]!, $sponsorableOrgsIds: [ID!]!) {
      ...Views::Files::Sidebar::SponsorsList::Root
    }
  GRAPHQL

  def sponsors_list
    variables = {
      sponsorableUsersIds: current_repository.funding_links.sponsorable_users_ids,
      sponsorableOrgsIds: current_repository.funding_links.sponsorable_orgs_ids
    }
    data = platform_execute(SidebarSponsorsQuery, variables: variables)

    respond_to do |format|
      format.html do
        render partial: "files/sidebar/sponsors_list", locals: {
          block_button: params[:block_button] == "true",
          data: data
        }
      end
    end
  end

  def environment_status
    environment = current_repository.deployments.where(environment: params[:environment]).first

    respond_to do |format|
      format.html do
        render partial: "files/sidebar/environment_status", locals: {environment: environment, environment_name: params[:environment]}
      end
    end
  end

  CommunityContributorsQuery = parse_query <<-'GRAPHQL'
    query(
      $id: ID!,
      $limit: Int!,
      $offset: Int,
    ) {
      node(id: $id) {
        ...Views::Repositories::CommunityContributors::Repository
      }
    }
  GRAPHQL

  def community_contributors
    return render_404 unless community_contributors_enabled?

    limit     = Repository.per_page
    page      = params[:page].to_i > 0 ? params[:page].to_i : 1
    offset    = limit * (page - 1)
    variables = { id: current_repository.global_relay_id, limit: limit, offset: offset }
    data      = platform_execute(CommunityContributorsQuery, variables: variables)
    render "repositories/community_contributors",
      locals: { current_page: page, repository: data.node, per_page: limit }
  end

  def pulse
    respond_to do |format|
      format.html { render "repositories/pulse" }
    end
  end

  def pulse_committer_data
    render json: current_repository.activity_summary(viewer: current_user, period: params[:period]).authors_with_commits
  end

  def pulse_diffstat_summary
    view = Repositories::PulseView.new(current_repository, params[:period], current_user)

    respond_to do |format|
      format.html do
        render partial: "repositories/pulse/diffstat_summary",
               locals: { view: view }
      end
    end
  end

  DeploymentsIndexQuery = parse_query <<-'GRAPHQL'
    query($repo_id: ID!, $activity_log_environments: [String!], $top_ten_environments: [String!]){
      ...Views::Repositories::Deployments::Root
    }
  GRAPHQL

  def deployments
    return render_404 unless current_repository.can_see_deployments?(current_user)

    top_ten_environments = current_repository.frequent_deploy_environments(first: 10)

    variables = {
      repo_id: current_repository.global_relay_id,
      top_ten_environments: top_ten_environments,
    }

    variables[:activity_log_environments] = params[:environment] if params[:environment]

    data = platform_execute(DeploymentsIndexQuery, variables: variables)

    respond_to do |format|
      format.html do
        render "repositories/deployments", locals: { data: data, environment_filter: params[:environment], top_ten_environments: top_ten_environments }
      end
    end
  end

  DeploymentsEnvironmentStateQuery = parse_query <<-'GRAPHQL'
    query($repoId: ID!,$environment: String!) {
      node(id: $repoId) {
        ... on Repository {
          deployments(last: 10, environments: [$environment], orderBy: {field:CREATED_AT, direction:DESC}) {
            edges {
              node {
                state
                ...Views::Repositories::DeploymentsEnvironmentState::EnvironmentState
              }
            }
          }
        }
      }
    }
  GRAPHQL

  def deployments_environment_state
    return render_404 unless request.xhr?

    variables = { repoId: current_repository.global_relay_id, environment: params[:environment] }
    data = platform_execute(DeploymentsEnvironmentStateQuery, variables: variables)
    active_deployments = data.node.deployments.edges.map(&:node).select { |deployment| deployment.state == "ACTIVE" }

    latest_active = active_deployments.first
    return render_404 unless latest_active

    respond_to do |format|
      format.html do
        render partial: "repositories/deployments_environment_state", locals: {
          data: latest_active,
        }
      end
    end
  end

  DeploymentsActivityLogQuery = parse_query <<-'GRAPHQL'
    query(
      $repoId: ID!,
      $environments: [String!],
      $first: Int,
      $last: Int,
      $after: String,
      $before: String
    ){
      ...Views::Repositories::DeploymentsActivityLog::Root
    }
  GRAPHQL

  ACTIVITY_LOG_PAGE_SIZE = 15

  def deployments_activity_log
    top_ten_environments = current_repository.frequent_deploy_environments(first: 10)

    variables = graphql_pagination_params(page_size: ACTIVITY_LOG_PAGE_SIZE).merge({
      repoId: current_repository.global_relay_id,
    })

    variables[:environments] = params[:environment] if params[:environment]

    data = platform_execute(DeploymentsActivityLogQuery, variables: variables)

    respond_to do |format|
      format.html do
        render "repositories/deployments_activity_log", locals: { data: data, environment_filter: params[:environment], top_ten_environments: top_ten_environments }
      end
    end
  end

  FullAssociatedPullsQuery = parse_query <<-'GRAPHQL'
    query($deploymentId: ID!){
      ...Views::Repositories::FullAssociatedPulls::Root
    }
  GRAPHQL

  def full_associated_pulls
    data = platform_execute(FullAssociatedPullsQuery, variables: { deploymentId: params[:deployment_id] })
    return render_404 if data.errors.any?

    respond_to do |format|
      format.html do
        render partial: "repositories/full_associated_pulls", locals: { data: data }
      end
    end
  end

  CompactAssociatedPullsQuery = parse_query <<-'GRAPHQL'
    query($deploymentId: ID!){
      ...Views::Repositories::CompactAssociatedPulls::Root
    }
  GRAPHQL

  def compact_associated_pulls
    data = platform_execute(CompactAssociatedPullsQuery, variables: { deploymentId: params[:deployment_id] })

    respond_to do |format|
      format.html do
        render partial: "repositories/compact_associated_pulls", locals: { data: data }
      end
    end
  end

  # handle requests from the Go language tooling to determine location and type of VCS of a Go import path.
  # Valid requests must have a query string of ?go-get=1 (https://golang.org/cmd/go/#hdr-Remote_import_paths)

  if GitHub.enterprise?
    def go_metatag
      render_404 unless params["go-get"] == "1"
      repopath = "/" + params[:user_id] + "/" + params[:repository]
      prefix = GitHub.host_name + repopath
      reporoot = GitHub.url + repopath + ".git"
      render "repositories/go_metatag", locals: {
        prefix: prefix,
        reporoot: reporoot,
      }, status: 200, layout: false
    end
  end

  def opt_in_to_view
    GitHub.kv.set(opt_in_kv_key, "1", expires: 30.days.from_now)
    GitHub.dogstats.increment("network_privileges.opt_in_to_view")
    safe_redirect_to params[:return_to]
  end

  # Debugging performance around repository layouts
  def no_content
    render "repositories/no_content"
  end

  private

  def template_repository
    if params[:template_repository_id]
      Repository.active.filter_spam_and_disabled_for(current_user).
                 find_by(id: params[:template_repository_id])
    end
  end

  def computed_repository_params
    repository_params.merge auto_init: repository_params[:auto_init] == "1"
  end

  def repository_params
    return ActionController::Parameters.new unless params.key?(:repository)

    permitted_fields = %i[
      name
      description
      homepage
      public
      visibility
      license_template
      gitignore_template
      team_id
      auto_init
      template
    ]
    params.require(:repository).permit(permitted_fields)
  end

  def login_required_with_redirect
    redirect_to_login(request.url) unless logged_in?
  end

  def current_repositories
    @repositories ||= Repository.find_all_public(current_page).
      includes([:mirror, :primary_language, :owner, :parent, :topics])
  end
  helper_method :current_repositories

  def stargazers_you_know_count
    @stargazers_you_know_count ||= current_user.following_starred(current_repository.id).count
  end
  helper_method :stargazers_you_know_count

  # Handle auth specifics for feed requests.
  include GitHub::Authentication::Feed

  # Private: Actions that can response to atom requests.
  #
  # Returns an Array or Strings.
  def feed_actions
    %w(index)
  end

  def upgrade?
    params[:confirm_upgrade] == "on"
  end

  def track_ga_event(repo, owner_has_billing)
    ga_label = {
      target: repo.owner.type,
      billing: owner_has_billing,
      repo: (repo.public? ? "public" : "private"),
    }

    ga_label[:action] = "upgrade" if upgrade?

    analytics_event(
      category: "Repository",
      action: "create",
      label: ga_label,
      redirect: true,
    )
  end

  # Private - Create a new repository for the requested owner
  #
  # requested_owner - a User/Organization object that will own the new repo.
  #
  # Returns a Repository::Creator::Result object indicating the success of the
  # repository creation operation.
  def create_repository(requested_owner, reflog_data)
    Repository.handle_creation(
      current_user,
      requested_owner.login,
      upgrade?,
      computed_repository_params.to_h,
      reflog_data,
      payment_details,
    )
  end

  def content_authorization_required
    authorize_content(:repo)
  end

  def request_reflog_data(owner, repository, via)
    {
      real_ip: request.remote_ip,
      repo_name: "#{owner.login}/#{repository[:name]}",
      user_login: current_user.login,
      user_agent: request.user_agent,
      from: GitHub.context[:from],
      via: via,
    }
  end

  # Bypass conditional access requirements for the specified actions if the
  # current action is exempt.
  def require_conditional_access_checks?
    return false if conditional_access_exempt?
    super
  end

  def target_for_conditional_access
    target = params[:user_id] || params[:organization_id] || params[:owner]
    org = Organization.find_by_login(target)
    return :no_target_for_conditional_access unless org
    org
  end

  def prepare_new_view
    @owner = current_organization || current_user
    @repository = Repository.new(private: @owner.private_repo_by_default?)
  end

  def ensure_repository_specified
    render_404 unless repository_specified?
  end

  def stateless_request?
    action_name == "go_metatag" || super
  end

  def can_rename_repository?(owner)
    return false unless params[:current_name]

    repository = owner.find_repo_by_name(params[:current_name])
    repository&.adminable_by?(current_user)
  end
end
