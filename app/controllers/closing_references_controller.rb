# frozen_string_literal: true

class ClosingReferencesController < AbstractRepositoryController
  include TimelineHelper

  before_action :writable_repository_required
  before_action :content_authorization_required, except: :index
  before_action :require_current_issue_or_pr
  before_action :resource_write_access_required

  VALID_SOURCE_TYPES = ["ISSUE", "PULL_REQUEST"].freeze
  REFERENCE_INDEX_LIMIT = 50

  def update
    references = current_issue_or_pr.close_issue_references.manual
    connected_ids = references.pluck(connected_by_key)

    ids_to_remove = connected_ids - proposed_connection_ids
    ids_to_add = proposed_connection_ids - connected_ids

    refs_to_remove = CloseIssueReference.where(source_key => current_issue_or_pr.id, connected_by_key => ids_to_remove)
    refs_to_remove.destroy_all

    refs_to_add = if pull_request?
      current_repository.issues.where(id: ids_to_add)
    else
      current_repository.pull_requests.where(id: ids_to_add)
    end
    refs_to_add.each do |ref|
      current_issue_or_pr.close_issue_references
        .create(connected_by_key => ref.id, :source => :manual, :actor_id => current_user.id)
    end

    respond_to do |format|
      format.html do
        render partial: "issues/sidebar/show/references", locals: {
          issue: current_issue_or_pr.to_issue,
        }
      end
    end
  end

  def index
    references = current_issue_or_pr.close_issue_references

    manual_connected_ids = references.manual.pluck(connected_by_key)
    xref_connected_ids = references.xref.pluck(connected_by_key)

    all_possible_references =
      CloseIssueReference.possible_closing_references_for(issue_or_pr: current_issue_or_pr,
                                                          viewer: current_user,
                                                          limit: REFERENCE_INDEX_LIMIT)

    manual_reference_ids_at_limit = CloseIssueReference
                                      .where(connected_by_key => all_possible_references.pluck(:id)).manual
                                      .group(connected_by_key)
                                      .having("count(*) >= ?", CloseIssueReference::MAX_MANUAL_REFERENCES)
                                      .pluck(connected_by_key)

    render partial: "issues/sidebar/references_menu_content", locals: {
      issue_or_pr: current_issue_or_pr,
      all_possible_references: all_possible_references,
      manual_reference_ids_at_limit: manual_reference_ids_at_limit,
      existing_xrefs: viewable_xrefed_items(xref_connected_ids),
      existing_manual_references: viewable_xrefed_items(manual_connected_ids),
      max_manual_reference_count: CloseIssueReference::MAX_MANUAL_REFERENCES,
    }
  end

  private

  def current_issue_or_pr
    return @current_issue_or_pr if defined?(@current_issue_or_pr)
    case params[:source_type]
    when "ISSUE"
      @current_issue_or_pr = current_repository.issues.find(params[:source_id].to_i)
    when "PULL_REQUEST"
      @current_issue_or_pr = current_repository.pull_requests.find(params[:source_id].to_i)
    else
      raise ArgumentError.new("Invalid source_type #{params[:source_type]} provided")
    end
  end

  # Note: if we pivot to allowing cross repo references, we will need to handle
  # checking permissions on all of the repos that own the `connection_ids`.
  def resource_write_access_required
    if current_repository.private?
      render_404 unless current_repository.readable_by?(current_user)
    end

    if pull_request?
      return head :unauthorized unless current_repository.pushable_by?(current_user)
    else
      return head :unauthorized unless current_issue_or_pr.labelable_by?(actor: current_user)
    end
  end

  # Note: if we pivot to allowing cross repo references, we will need to handle
  # the target repository content authorization and permissions checks as well.
  def content_authorization_required
    authorize_content(:issue, repo: current_repository)
  end

  def require_current_issue_or_pr
    pull_request? && params[:source_type] == "PULL_REQUEST" ||
      current_issue_or_pr.is_a?(Issue) && params[:source_type] == "ISSUE"
  rescue ActiveRecord::RecordNotFound
    render_404
  end

  def proposed_connection_ids
    [params[:connection_ids]].flatten.compact.map(&:to_i)
  end

  # Define which column on `close_issue_references` to treat as the source,
  # based on params[:source_type]
  def source_key
    pull_request? ? :pull_request_id : :issue_id
  end

  # Define which column on `close_issue_references` is the connected column,
  # based on params[:source_type]
  def connected_by_key
    pull_request? ? :issue_id : :pull_request_id
  end

  # Given a set of issue or PR ids, returns the ones the user is permitted to view
  def viewable_xrefed_items(ids)
    scope = pull_request? ? Issue : PullRequest

    scope.where(id: ids).select do |object|
      next if object.spammy? && !current_user&.staff?
      object.repository.public? || object.repository.visible_and_readable_by?(current_user)
    end
  end

  def pull_request?
    current_issue_or_pr.is_a?(PullRequest)
  end
end
