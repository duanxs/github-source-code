# frozen_string_literal: true

class Businesses::InstallationsController < Businesses::BusinessController

  include IntegrationInstallationsControllerMethods

  before_action :business_admin_required

  def update
    render_404
  end

  private

  def current_context
    this_business
  end
end
