# frozen_string_literal: true

class Businesses::SamlProviderController < Businesses::BusinessController
  include BusinessesHelper

  before_action :business_saml_required
  before_action :business_admin_required
  before_action :saml_provider_required, only: %w(recovery_codes regenerate_recovery_codes update_user_provisioning)
  before_action :enterprise_idp_provisioning_required, only: %w(update_user_provisioning)
  before_action :dotcom_required, only: %w(recovery_codes download_recovery_codes print_recovery_codes regenerate_recovery_codes)

  skip_before_action :private_instance_bootstrap_check, only: %w(update)

  def update
    params[:saml] ||= {}
    if params[:test_settings]
      test_saml_config
    else
      # Attempt to save the provided configuration if there's an existing
      # provider or if we're creating a new provider and the recovery codes have
      # been viewed (and hopefully downloaded).
      # Otherwise, show the recovery codes view, prompting the user to download
      # their recovery codes.
      if this_business.saml_provider.present? ||
         params[:recovery_codes_viewed] ||
         bootstrap_flow?
        save_saml_config
      else
        validate_saml_config_and_show_recovery_codes
      end
    end
  end

  def delete
    this_business.saml_provider&.destroy
    flash[:notice] = "SAML authentication is disabled"
    redirect_to settings_security_enterprise_path(this_business)
  end

  def recovery_codes
    render "businesses/saml_provider/recovery_codes"
  end

  def download_recovery_codes
    codes = if this_business.saml_provider.present?
      this_business.saml_provider.formatted_recovery_codes
    elsif saml_provider_setup_flow.pending?
      tmp_saml_provider.formatted_recovery_codes
    end
    return render_404 if codes.blank?

    send_data codes.join("\r\n"), filename: "github-#{this_business.slug}-recovery-codes.txt"
  end

  def print_recovery_codes
    # The "popup" layout requires this instance variable
    @page_class = "js-print-popup"
    respond_to do |format|
      format.html do
        if this_business.saml_provider.blank?
          if saml_provider_setup_flow.pending?
            new_setup_recovery_codes = tmp_saml_provider.formatted_recovery_codes
          else
            return render_404
          end
        end

        locals = {}.tap do |locals|
          if new_setup_recovery_codes.present?
            locals[:new_setup_recovery_codes] = new_setup_recovery_codes
          end
        end

        render "businesses/saml_provider/print_recovery_codes",
          layout: "popup", locals: locals
      end
    end
  end

  def regenerate_recovery_codes
    provider = this_business.saml_provider
    if provider.present?
      provider.generate_recovery!
      if provider.save
        flash[:notice] = "New SSO recovery codes successfully generated."
      else
        flash[:error] = provider.errors.full_messages.join(", ")
      end
    else
      flash[:error] = "This enterprise account does not have an identity provider set."
    end
    redirect_to settings_saml_provider_recovery_codes_enterprise_path(this_business)
  end

  def update_user_provisioning
    if this_business.saml_provider.update(user_provisioning_params)
      notice = "User provisioning settings saved."

      if saml_deprovisioning_changed?
        this_business.expire_all_enterprise_sessions!(current_user: current_user)
        notice += " Please re-authenticate with your Identity Provider to continue."
      end
      flash[:notice] = notice
    else
      errors = this_business.saml_provider.errors.full_messages.join(", ")
      flash[:error] = "Unable to save your user provisioning settings. #{errors}"
    end

    redirect_to settings_security_enterprise_path(this_business)
  end

  private

  def saml_deprovisioning_changed?
    this_business.saml_provider.previous_changes.has_key?("saml_deprovisioning_enabled") &&
      this_business.saml_provider.saml_deprovisioning_enabled?
  end

  def user_provisioning_params
    return ActionController::Parameters.new if params[:business_saml_provider].blank?

    params.require(:business_saml_provider).permit(:provisioning_enabled, :saml_deprovisioning_enabled)
  end

  def enterprise_idp_provisioning_required
    render_404 unless GitHub.flipper[:enterprise_idp_provisioning].enabled?(this_business)
  end

  def saml_params
    return ActionController::Parameters.new if params[:saml].empty?

    params.require(:saml).permit %i[
      sso_url
      issuer
      idp_certificate
      digest_method
      signature_method
      recovery_secret
    ]
  end

  def validate_saml_config_and_show_recovery_codes
    saml_provider_setup_flow.new_setup saml_params
    tmp_saml_provider.user_that_must_test_settings = current_user
    if tmp_saml_provider.valid?
      render "businesses/saml_provider/recovery_codes", locals: {
        new_setup_recovery_codes: tmp_saml_provider.formatted_recovery_codes,
        new_setup_saml_params: saml_params.merge(recovery_secret: tmp_saml_provider.recovery_secret),
      }
    else
      errors = tmp_saml_provider.errors.full_messages.to_sentence
      flash.now[:error] = "Invalid SAML provider settings. #{errors}"
      render "businesses/settings/security", locals: { params: params }
    end
  end

  def save_saml_config
    new_setup = this_business.saml_provider.blank?
    provider = this_business.saml_provider || this_business.build_saml_provider
    provider.user_that_must_test_settings = current_user
    if new_setup && saml_params[:recovery_secret].present?
      provider.recovery_secret = saml_params[:recovery_secret]
      provider.recovery_used_bitfield = 0
      provider.recovery_codes_viewed = true
    end
    attributes = saml_params.slice \
      :sso_url, :issuer, :idp_certificate, :signature_method, :digest_method
    if provider.update(attributes)
      # Ensure that any possible cached setup flow values are cleared.
      saml_provider_setup_flow.clear_kv_values

      # Existing orgs with Team Sync enabled for their own provider will
      # misbehave (no UI to toggle Team Sync, membership losses upon team
      # mapping changes, etc.) when we add a provider to their Enterprise,
      # so we'd better disable on those orgs when adding a new config.
      #
      # (disabling removes mappings without touching memberships => 🏆)
      if new_setup
        TeamSync::Tenant.not_disabled.where(
          organization_id: this_business.organization_ids,
        ).each(&:disable)
      end
    else
      flash.now[:error] = "Your SAML provider settings could not be saved. #{provider.errors.full_messages.join(", ")}"
      if bootstrap_flow?
        return render "bootstrap_instance/configuration", locals: {
          business: this_business,
          step: "saml-idp",
          params: params,
        }, layout: "bootstrap_instance"
      else
        return render "businesses/settings/security", locals: { params: params }
      end
    end

    saml_provider_setup_flow.clear_kv_values if saml_provider_setup_flow.pending?
    GitHub.private_instance_bootstrapper.saml_idp_configuration.complete! if bootstrap_flow?
    next_step = \
      if bootstrap_flow?
        business_idm_sso_enterprise_path(this_business,
          return_to: bootstrap_instance_configuration_path)
      else
        settings_security_enterprise_path(this_business)
      end
    redirect_to next_step, notice: "Your SAML provider settings were saved"
  end

  def test_saml_config
    # Attempt to save the settings
    test_settings = Business::SamlProviderTestSettings.save_for(
      user: current_user,
      business: this_business,
      settings: saml_params,
    )

    if test_settings.valid?
      options = {
        sp_url: service_provider_url,
        sso_url: test_settings.sso_url,
        assertion_consumer_service_url: saml_consume_url,
        destination: test_settings.sso_url,
        issuer: service_provider_url,
        signature_method: test_settings.signature_method,
        digest_method: test_settings.digest_method,
      }

      authn_request_url = Platform::Authentication::SamlAuthnRequestUrl.new(options)
      authn_request_url.relay_state = initiate_relay_state(authn_request_url.request)

      # We use "validate" as the cookie value in this action to allow the
      # consume endpoint to know that a user was trying to test their
      # provider settings after the SAML SSO post-back.
      cookies.encrypted[:saml_return_to] = "validate"
      cookies.encrypted[:saml_return_to_legacy] = "validate"

      allow_external_redirect_after_post(provider: test_settings)
      redirect_url = authn_request_url.to_s
      render "businesses/identity_management/sso_meta_redirect", locals: { redirect_url: redirect_url }, layout: "redirect"
    else
      test_settings_errors = test_settings.errors.full_messages.join(", ")
      flash.now[:business_saml_test_result] = {
        status: Business::SamlProviderTestSettings::FAILURE,
        message: test_settings_errors,
      }
      flash.now[:error] = "Your SAML provider test settings are invalid"
      view_params = params.merge({
        saml_testing: {
          failure: true,
          message: test_settings.message,
        },
      })
      if bootstrap_flow?
        render "bootstrap_instance/configuration", locals: {
          business: this_business,
          step: "saml-idp",
          params: view_params,
        }, layout: "bootstrap_instance"
      else
        render "businesses/settings/security", locals: {
          params: view_params,
        }
      end
    end
  end

  def saml_provider_required
    render_404 unless this_business.saml_provider.present?
  end

  def initiate_relay_state(authn_request)
    relay_state = Platform::Authentication::SamlRelayState.initiate(
      request_id: authn_request.id,
    )

    # persist the relay state digest for future validation
    cookies.encrypted[:saml_csrf_token] = saml_csrf_cookie(relay_state.digest)
    cookies.encrypted[:saml_csrf_token_legacy] = saml_csrf_cookie((relay_state.digest))

    relay_state.nonce
  end

  # Encrypted cookies have some odd behavior when the exact same object is used as a value
  # This reduces repetition while helping create valid cookies.
  def saml_csrf_cookie(digest)
    {
      value: digest,
      expires: ::Platform::Authentication::SamlRelayState::DEFAULT_EXPIRY,
      secure: request && request.ssl?,
      httponly: true,
      domain: cookie_domain,
    }
  end

  def cookie_domain
    ".#{GitHub.host_name}"
  end

  def saml_provider_setup_flow
    @saml_provider_setup_flow ||= SamlProviderSetupFlow.new(this_business)
  end

  def tmp_saml_provider
    saml_provider_setup_flow.tmp_provider
  end
  helper_method :tmp_saml_provider
end
