# frozen_string_literal: true

class Businesses::AdminsController < Businesses::BusinessController
  include BusinessesHelper

  before_action :business_admin_required
  before_action :business_member_invitations_required, only: %w(pending)

  def suggestions
    headers["Cache-Control"] = "no-cache, no-store"

    respond_to do |format|
      format.html_fragment do
        render_partial_view "businesses/admins/suggestions",
          Businesses::Admins::SuggestionsView,
          business: this_business,
          query: params[:q]
      end
      format.html do
        render_partial_view "businesses/admins/suggestions",
          Businesses::Admins::SuggestionsView,
          business: this_business,
          query: params[:q]
      end
    end
  end

  def index
    query_args = parse_query_string(query_param,
      filter_map: BusinessesHelper::ADMINS_QUERY_FILTERS,
    )
    admins = this_business.admins(query: query_args[:query],
                                  role: query_args[:role])
                          .paginate(page: current_page)

    respond_to do |format|
      format.html do
        if request.xhr?
          headers["Cache-Control"] = "no-cache, no-store"
          render_partial_view "businesses/admins/list", Businesses::Admins::ListView,
            {
              business: this_business,
              admins: admins,
            },
            locals: {
              query: query_param,
              role: role_filter,
            }
        else
          render "businesses/admins/index", locals: {
            query: query_param,
            role: role_filter,
            admins: admins,
          }
        end
      end
    end
  end

  def pending
    pending_admins = this_business.pending_admin_invitations(query: query_param)
                                  .paginate(page: current_page)

    respond_to do |format|
      format.html do
        if request.xhr?
          headers["Cache-Control"] = "no-cache, no-store"
          render partial: "businesses/admins/pending_list", locals: {
            query: query_param,
            pending_admins: pending_admins,
          }
        else
          render "businesses/admins/pending", locals: {
            query: query_param,
            pending_admins: pending_admins,
          }
        end
      end
    end
  end

  def create
    return render_404 unless GitHub.bypass_business_member_invites_enabled?

    errors = []
    if GitHub.site_admin_role_managed_externally?
      errors << Business.external_auth_system_owner_instructions(operation: :add)
    else
      owner = User.find_by login: admin_login_param
      errors << "Could not find user with login: #{admin_login_param}" unless owner.present?

      if owner.present?
        begin
          this_business.add_owner(owner, actor: current_user)
        rescue Business::UserHasTwoFactorDisabledError => error
          errors << error.message
        end
      end
    end

    if errors.any?
      error = errors.first
      respond_to do |wants|
        wants.html do
          flash[:error] = error
          redirect_to admins_enterprise_path(this_business)
        end
        wants.json do
          render json: { error: error }
        end
      end
    else
      respond_to do |wants|
        wants.html do
          redirect_to admins_enterprise_path(this_business),
            notice: "#{admin_login_param} is now an owner"
        end
        wants.json do
          business_user_account = this_business.business_user_account_for(owner)
          admin_list_item = render_to_string \
            partial: "businesses/admins/admin",
            formats: [:html],
            locals: {
              business: this_business,
              admin: owner,
              business_user_account: business_user_account,
              role: :owner,
            }

          render json: { html: admin_list_item }
        end
      end
    end
  end

  def destroy
    errors = []
    if GitHub.site_admin_role_managed_externally?
      errors << Business.external_auth_system_owner_instructions(operation: :remove)
    else
      admin = User.find_by login: admin_login_param
      errors << "Could not find user with login: #{admin_login_param}" unless admin.present?

      if admin.present?
        if this_business.owner?(admin)
          role = :owner
          begin
            this_business.remove_owner(admin, actor: current_user)
          rescue Business::NoAdminsError
            errors << "You cannot remove the last owner of the enterprise account."
          end
        elsif this_business.billing_manager?(admin)
          role = :billing_manager
          this_business.billing.remove_manager(admin, actor: current_user)
        end
      end
    end

    if errors.any?
      error = errors.first
      if request.xhr?
        render plain: error, status: :bad_request
      else
        flash[:error] = error
        redirect_to admins_enterprise_path(this_business)
      end
    else
      if request.xhr?
        head :ok
      else
        if current_user == admin
          # Current user removed themselves as an owner
          redirect_to enterprise_path(this_business),
            notice: "You are no longer #{Business.admin_role_for_message(role)} of #{this_business.name}."
        else
          redirect_to admins_enterprise_path(this_business),
            notice: "You've removed #{admin.login} as #{::Business.admin_role_for_message(role)} of #{this_business.name}."
        end
      end
    end
  end

  def set_role
    errors = []
    new_role = params[:role]&.downcase&.to_sym
    admin = User.find_by login: params[:login]&.to_s
    errors << "Could not find user with login: #{params[:login]&.to_s}" unless admin.present?

    if admin.present?
      unless this_business.owner?(admin) || this_business.billing_manager?(admin)
        errors << "#{admin} is not an administrator of the enterprise account"
      end

      Business.transaction do
        Ability.transaction do
          this_business.change_admin_role(admin, new_role: new_role, actor: current_user)
        end
      rescue Business::UserHasTwoFactorDisabledError, Business::UserNotAnAdminError, Business::NoAdminsError, ArgumentError  => error
        errors << error.message
      end
    end

    if errors.any?
      flash[:error] = errors.first
    else
      flash[:notice] = "#{admin}'s role was set to #{new_role.to_s.humanize}"
    end

    redirect_to admins_enterprise_path(this_business)
  end

  private

  def query_param
    return @query if defined?(@query)
    @query = params[:query]
  end

  def role_filter
    params[:role]
  end
end
