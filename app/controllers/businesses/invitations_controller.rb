# frozen_string_literal: true

class Businesses::InvitationsController < Businesses::BusinessController
  include BusinessesHelper

  before_action :business_admin_required
  before_action :require_user

  def member
    invitations = this_business.pending_member_invitations(login: user.login).
                                paginate(page: current_page)

    render_template_view "businesses/invitations/member",
      Businesses::InvitationsView,
      {
        business: this_business,
        user: user,
      },
      locals: {
        invitations: invitations,
        member: user,
      }
  end

  def admin
    invitations = this_business.pending_admin_invitations(login: user.login)

    render_template_view "businesses/invitations/admin",
      Businesses::InvitationsView,
      {
        business: this_business,
        user: user,
      },
      locals: {
        invitations: invitations,
        member: user,
      }
  end

  def outside_collaborator
    invitations = this_business.
      pending_collaborator_invitations(login: user.login).
      paginate(page: current_page)

    render "businesses/invitations/outside_collaborator", locals: {
      invitations: invitations, member: user,
    }
  end

  private

  def login_param
    return @login if defined?(@login)
    @login = params[:login]
  end

  def user
    @user ||= User.find_by(login: login_param)
  end

  def require_user
    render_404 if user.nil?
  end
end
