# frozen_string_literal: true

class Businesses::TeamsController < Businesses::BusinessController

  before_action :business_admin_required

  def index
    render_template_view "businesses/settings/teams",
      Businesses::Settings::TeamsView,
      business: this_business,
      params: params
  end

  def update_team_discussions_allowed
    enabled = params[:team_discussions_allowed]&.to_s
    validate_setting value: enabled

    message = ""
    case enabled
    when "enabled"
      this_business.allow_team_discussions(true, actor: current_user)
      message = "Team discussions are enabled and enforced for this enterprise."
    when "disabled"
      this_business.disallow_team_discussions(true, actor: current_user)
      message = "Team discussions are disabled and enforced for this enterprise."
    when "no_policy"
      this_business.clear_team_discussions_setting(actor: current_user)
      message = "Team discussions policy removed."
    end

    redirect_to settings_teams_enterprise_path(this_business), notice: message
  end
end
