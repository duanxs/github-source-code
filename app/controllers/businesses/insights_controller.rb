# frozen_string_literal: true

class Businesses::InsightsController < Businesses::BusinessController
  areas_of_responsibility :admin_experience, :insights

  before_action :business_admin_required
  before_action :insights_available_required
  before_action :try_redirect_to_insights, only: [:index]
  before_action :validate_insights_url, only: [:update_base_url]

  def index
    render "businesses/insights/index"
  end

  def settings
    render "businesses/settings/insights"
  end

  def update_base_url
    GitHub.insights_url = params[:insights_url]
    flash[:notice] = "You have set the URL for your GitHub Insights instance successfully."
    redirect_to settings_enterprise_insights_path(this_business)
  end

  private

  def insights_available_required
    render_404 unless GitHub.insights_available?
  end

  def try_redirect_to_insights
    return unless GitHub.insights_url
    redirect_to GitHub.insights_url
  end

  def validate_insights_url
    valid_url = begin
      Addressable::URI.parse(params[:insights_url])&.host&.present?
    rescue Addressable::URI::InvalidURIError
      false
    end

    return if valid_url

    flash[:error] = "Provided url was not valid"
    redirect_to settings_enterprise_insights_path(this_business)
  end
end
