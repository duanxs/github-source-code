# frozen_string_literal: true

class Businesses::DataPacksController < Businesses::BusinessController
  before_action :ensure_business_can_self_serve
  before_action :business_access_required

  def index
    target = this_business.organizations.first
    data_pack_change = Billing::PlanChange::DataPackChange.new(target, total_packs: target.data_packs)
    render "businesses/data_packs/index", locals: {
      data_pack_change: data_pack_change,
      target: target,
    }
  end

  def update
    target = this_business.organizations.find_by(login: params[:organization])
    total_packs = target.data_packs + asset_status_params.fetch(:delta_packs, 0).to_i

    data_pack_updater = Billing::DataPackUpdater.new(target, total_packs: total_packs, actor: current_user)

    if data_pack_updater.update
      flash[:notice] = "Successfully updated your data plan. Thanks!"
      redirect_to settings_billing_enterprise_path(this_business)
    else
      flash[:error] = data_pack_updater.error
      redirect_to :back
    end
  end

  private

  def asset_status_params
    params.require(:asset_status).permit(:delta_packs)
  end

  def ensure_business_can_self_serve
    if !this_business.can_self_serve?
      flash[:error] = "Please contact your GitHub Enterprise account representative to add more data packs to your plan."
      redirect_to settings_billing_enterprise_path(this_business)
    end
  end
end
