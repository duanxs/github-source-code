# frozen_string_literal: true

class Businesses::PreReceiveHooksController < Businesses::BusinessController

  before_action :business_admin_required
  before_action :single_business_environment_required

  def destroy
    @hook = PreReceiveHook.find_by_id(params[:id])
    if @hook.destroy
      flash[:notice] = "Successfully deleted hook"
      redirect_to hooks_enterprise_path(GitHub.global_business)
    else
      flash[:error] = "Error deleting hook"
      redirect_to hooks_enterprise_path(GitHub.global_business)
    end
  end

end
