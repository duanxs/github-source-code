# frozen_string_literal: true

class Businesses::BillingSettingsController < Businesses::BusinessController

  before_action :ensure_billing_enabled
  before_action :business_access_required

  def show
    if params[:tab] == "cost_management"
      return render_404 unless Billing::Budget.configurable?(this_business)
    end

    render "businesses/billing_settings/show", locals: { current_tab: params[:tab] }
  end

  def update_members_can_make_purchases
    return render_404 unless this_business.can_self_serve?

    setting_value = members_can_make_purchases_params[:members_can_make_purchases]&.downcase&.to_s
    case setting_value
    when "enabled"
      this_business.allow_members_can_make_purchases(actor: current_user)
      flash[:notice] = "Organization admins can now make purchases."
    when "disabled"
      this_business.disallow_members_can_make_purchases(actor: current_user, force: true)
      flash[:notice] = "Organization admins can no longer make purchases."
    else
      flash[:error] = "You provided an invalid input value. Please try again."
    end
    redirect_back(fallback_location: settings_billing_enterprise_path(this_business))
  end

  private

  def members_can_make_purchases_params
    @members_can_make_purchases ||= params.require(:business).permit(:members_can_make_purchases)
  end
end
