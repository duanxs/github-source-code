# frozen_string_literal: true

class Businesses::OrganizationInvitationsController < Businesses::BusinessController

  before_action :dotcom_required
  before_action :login_required
  before_action :business_admin_required, only: [
    :new, :suggestions, :create, :confirm_pending, :resend_pending
  ]
  before_action :sudo_filter, only: [:create]
  before_action :require_current_organization, only: [:create, :resend_pending]
  before_action :require_not_accepted_organization_invitation, only: [:show_pending, :accept_pending, :resend_pending]
  before_action :require_pending_organization_invitation, only: [:confirm_pending, :cancel_pending]
  before_action :require_can_accept_organization_invitation, only: [:accept_pending]
  before_action :require_can_cancel_organization_invitation, only: [:cancel_pending]
  before_action :require_can_view_organization_invitation, only: [:show_pending]

  def new
    render "businesses/organization_invitations/new"
  end

  def suggestions
    headers["Cache-Control"] = "no-cache, no-store"

    respond_to do |format|
      format.html_fragment do
        render_partial_view "businesses/organization_invitations/suggestions",
          Businesses::OrganizationInvitations::SuggestionsView,
          orgs_only: true, business: this_business, query: params[:q]
      end
      format.html do
        render_partial_view "businesses/organization_invitations/suggestions",
          Businesses::OrganizationInvitations::SuggestionsView,
          orgs_only: true, business: this_business, query: params[:q]
      end
    end
  end

  def create
    if GitHub.single_business_environment?
      flash[:error] = "Enterprise organization invitations are disabled in this environment."
      return redirect_to :back
    end

    begin
      invitation = BusinessOrganizationInvitation.create! \
        business: this_business, inviter: current_user, invitee: current_organization
    rescue ActiveRecord::RecordInvalid => error
      flash[:error] = error.record.errors.full_messages.first
      return redirect_to :back
    end

    flash[:notice] = <<~NOTICE.squish
      You've invited #{current_organization.name} organization to join #{this_business.name}!
      The organization's administrators will be receiving an email shortly.
      You can check the pending tab to manage the invitation.
    NOTICE
    redirect_to pending_organizations_enterprise_path(this_business)
  end

  def show_pending
    render "businesses/organization_invitations/pending", locals: {
      invitation: pending_organization_invitation,
    }
  end

  def accept_pending
    errors = []
    begin
      pending_organization_invitation.accept(current_user)
    rescue BusinessOrganizationInvitation::InvalidActorError
      errors << "#{current_user.name} cannot accept invitations on behalf of #{current_organization.name}."
    rescue BusinessOrganizationInvitation::InvalidInviterError
      errors << "Please contact #{this_business.name}'s account representative to invite organizations."
    rescue BusinessOrganizationInvitation::AlreadyAcceptedError
      errors << "This invitation has already been accepted."
    rescue BusinessOrganizationInvitation::CanceledError
      errors << "This invitation has been canceled."
    rescue BusinessOrganizationInvitation::AlreadyBusinessMemberError
      errors << "#{current_organization.name} is already part of an enterprise."
    rescue BusinessOrganizationInvitation::InvalidInviteeError
      errors << "Please contact #{current_organization.name}'s account representative to join an enterprise."
    rescue BusinessOrganizationInvitation::InsufficientAvailableSeatsError
      errors << "#{this_business.name} does not have sufficient seats to add #{current_organization.name}."
    end

    if errors.any?
      flash[:error] = errors.first
    else
      flash[:notice] = "The invitation for #{current_organization.name} to join #{this_business.name} has been accepted. Once it is confirmed the organization will be part of #{this_business.name}."
    end

    redirect_to enterprise_path(this_business)
  end

  def cancel_pending
    errors = []
    begin
      pending_organization_invitation.cancel(current_user)
    rescue BusinessOrganizationInvitation::InvalidActorError
      errors << "#{current_user.name} cannot cancel invitations on behalf of #{pending_organization_invitation.invitee.name}."
    rescue BusinessOrganizationInvitation::AlreadyConfirmedError
      errors << "This invitation has already been confirmed."
    rescue BusinessOrganizationInvitation::CanceledError
      errors << "This invitation has already been canceled."
    end

    if errors.any?
      flash[:error] = errors.first
    else
      flash[:notice] = "Invitation for #{current_organization.name} to join #{this_business.name} was declined."
    end

    if params[:return_to_business]
      redirect_to pending_organizations_enterprise_path(this_business)
    else
      redirect_to user_path(current_organization)
    end
  end

  def confirm_pending
    errors = []
    begin
      pending_organization_invitation.confirm(current_user)
    rescue BusinessOrganizationInvitation::AlreadyConfirmedError
      errors << "This invitation has already been confirmed."
    rescue BusinessOrganizationInvitation::CanceledError
      errors << "This invitation has been canceled."
    rescue BusinessOrganizationInvitation::NotYetAcceptedError
      errors << "This invitation has not yet been accepted."
    rescue BusinessOrganizationInvitation::AlreadyBusinessMemberError
      errors << "#{current_organization.name} is already part of an enterprise."
    rescue BusinessOrganizationInvitation::InvalidActorError
      errors << "#{current_user.name} cannot confirm this invitation on behalf of #{this_business.name}."
    rescue BusinessOrganizationInvitation::InvalidInviterError
      errors << "Please contact #{this_business.name}'s account representative to invite organizations."
    rescue BusinessOrganizationInvitation::InvalidInviteeError
      errors << "Please contact #{current_organization.name}'s account representative to join an enterprise."
    end

    if errors.any?
      flash[:error] = errors.first
    else
      flash[:notice] = <<~NOTICE.squish
        #{current_organization.name} has been added to #{this_business.name}.
        The organization's ownership and billing have been transferred to this
        enterprise account.
      NOTICE
    end
    redirect_to enterprise_path(this_business)
  end

  def resend_pending
    BusinessMailer.invited_organization(pending_organization_invitation).deliver_later

    flash[:notice] = "Invitation resent to #{pending_organization_invitation.invitee.name}."
    redirect_to pending_organizations_enterprise_path(this_business)
  end

  private

  def require_current_organization
    return if current_organization.present?

    flash[:error] = "Could not find organization with login: #{params[:organization_login]}"
    return redirect_to :back
  end

  def require_can_view_organization_invitation
    return if current_organization.adminable_by?(current_user)

    pending_organization_invitation_not_found
  end

  def require_can_accept_organization_invitation
    return if current_organization.adminable_by?(current_user)

    flash[:error] = "#{current_user.name} does not have the ability to accept invitations on behalf of #{current_organization.name}."
    redirect_to enterprise_path(this_business)
  end

  def require_can_cancel_organization_invitation
    return if current_organization.adminable_by?(current_user)
    return if this_business.owner?(current_user)

    flash[:error] = "#{current_user.name} does not have the ability to cancel invitations on behalf of #{current_organization.name}."
    if params[:return_to_business]
      redirect_to pending_organizations_enterprise_path(this_business)
    else
      redirect_to user_path(current_organization)
    end
  end

  def require_not_accepted_organization_invitation
    return pending_organization_invitation_not_found if pending_organization_invitation.nil?
    return if !pending_organization_invitation.accepted?

    flash[:error] = "The invitation for #{current_organization.name} to join #{this_business.name} has already been accepted."
    redirect_to enterprise_path(this_business)
  end

  def require_pending_organization_invitation
    pending_organization_invitation || pending_organization_invitation_not_found
  end

  def pending_organization_invitation
    @pending_organization_invitation ||= this_business.pending_organization_invitation_for(current_organization)
  end

  def pending_organization_invitation_not_found
    return render_404 unless this_business
    render "businesses/organization_invitations/not_found", status: :not_found
  end

  def current_organization
    return unless params[:organization_login]
    @org ||= Organization.find_by(login: params.require(:organization_login))
  end
end
