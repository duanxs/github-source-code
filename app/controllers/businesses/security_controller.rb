# frozen_string_literal: true

class Businesses::SecurityController < Businesses::BusinessController
  include BusinessesHelper


  before_action :business_admin_required
  before_action :sudo_filter, except: %i(index two_factor_required_status)

  def index
    render "businesses/settings/security", locals: {
      params: params_for_saml_test_result,
    }
  end

  def update_two_factor_required
    required = params[:two_factor_required]&.to_s
    validate_setting value: required, valid_values: %w(enabled no_policy)

    if required == "enabled" &&
       this_business.name.downcase != params[:verify]&.downcase &&
       this_business.affiliated_users_with_two_factor_disabled_exist?
      flash[:error] = "You must type the name of the enterprise to confirm."
      return redirect_to settings_security_enterprise_path(this_business)
    end

    if !GitHub.auth.two_factor_authentication_enabled?
      flash[:error] = "Built-in two-factor authentication is not enabled on your instance."
      return redirect_to settings_security_enterprise_path(this_business)
    elsif GitHub.auth.builtin_auth_fallback?
      flash[:error] = "Two-factor authentication can't be enforced when both built-in and #{GitHub.auth.name} users are allowed."
      return redirect_to settings_security_enterprise_path(this_business)
    end

    if this_business.updating_two_factor_requirement?
      flash[:error] = "The enterprise account two factor requirement is being updated.  Please wait until the update is completed before changing the setting."
      return redirect_to settings_security_enterprise_path(this_business)
    end

    message = ""
    if required == "no_policy"
      this_business.disable_two_factor_required(actor: current_user, log_event: true)
      message = "Disabled two-factor authentication requirement policy. Individual organizations may enable or disable two-factor authentication."
    else
      if !current_user.two_factor_authentication_enabled?
        flash[:error] = "Two-factor authentication must be enabled on your personal account to require it for the enterprise account."
        return redirect_to settings_security_enterprise_path(this_business)
      else
        count = this_business.organizations_can_enable_two_factor_requirement(false).count
        if count > 0
          flash[:error] = "Enforcing two-factor authentication would remove all admins from #{count} #{"organization".pluralize(count)}."
          return redirect_to settings_security_enterprise_path(this_business)
        end
      end

      EnforceTwoFactorRequirementOnBusinessJob.perform_later(this_business, current_user)
      message = "Enabling two-factor authentication requirement."
    end

    redirect_to settings_security_enterprise_path(this_business), notice: message
  end

  def two_factor_required_status
    status = this_business.enforce_two_factor_requirement_job_status

    return render_404 unless status.present?
    if this_business.updating_two_factor_requirement?
      return head 202
    end

    respond_to do |format|
      format.html do
        render partial: "businesses/settings/two_factor_required_checkbox", locals: {
          business: this_business,
        }
      end
    end
  end

  def ssh_cert_requirement
    if params[:enable_ssh_cert_requirement] == "on"
      this_business.enable_ssh_certificate_requirement(current_user)
      flash[:notice] = "SSH certificate requirement enabled"
    else
      this_business.disable_ssh_certificate_requirement(current_user)
      flash[:notice] = "SSH certificate requirement disabled"
    end

    redirect_to settings_security_enterprise_path(this_business)
  end

  def update_ip_whitelisting_enabled
    if params[:enable_ip_whitelisting] == "on"
      begin
        this_business.enable_ip_whitelisting \
          actor: current_user, actor_ip: request.remote_ip
        flash[:notice] = "IP allow list enabled."
      rescue Configurable::IpWhitelistingEnabled::ActorLockoutError => error
        flash[:error] = error.message
      end
    else
      this_business.disable_ip_whitelisting(actor: current_user)
      flash[:notice] = "IP allow list disabled."
    end
    redirect_to settings_security_enterprise_path(this_business)
  end

  def update_ip_whitelisting_app_access_enabled
    if params[:enable_ip_whitelisting_app_access] == "on"
      this_business.enable_ip_whitelisting_app_access(actor: current_user)
      flash[:notice] = "IP allow list app access enabled."
    else
      this_business.disable_ip_whitelisting_app_access(actor: current_user)
      flash[:notice] = "IP allow list app access disabled."
    end
    redirect_to settings_security_enterprise_path(this_business)
  end
end
