# frozen_string_literal: true

class Businesses::BulkMemberActionsController < Businesses::BusinessController
  areas_of_responsibility :gitcoin

  before_action :dotcom_required
  before_action :business_admin_required

  def show
    render partial: "businesses/bulk_member_actions", locals: {
      selected_users: params[:user_ids] || [],
      business_slug: this_business.slug,
      enterprise_licenses: Businesses::EnterpriseLicensesView.for_business(this_business),
      volume_licenses: Businesses::VolumeLicensesView.for_business(this_business),
    }
  end

  def user_license_types
    license_type = params[:license_type]&.to_sym

    if license_type.nil?
      flash[:error] = "Could not update licenses because no type was specified."
    else
      user_ids = User.where(login: params[:user_logins]).pluck(:id)

      this_business.user_accounts.where(user_id: user_ids).pluck(:user_id).each do |user_id|
        this_business.user_licenses.ensure_exists(user_id: user_id)
      end

      licenses_to_update = this_business.user_licenses.where(user_id: user_ids).where.not(license_type: license_type)

      licenses_to_update.update_all(license_type: license_type)
      flash[:notice] = "Updated the license type to #{license_type} for " \
        "#{user_ids.count} #{"user license".pluralize(user_ids.count)}."
    end

    redirect_to people_enterprise_path(this_business)
  end
end
