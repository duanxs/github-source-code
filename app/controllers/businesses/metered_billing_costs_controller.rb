# frozen_string_literal: true

class Businesses::MeteredBillingCostsController < Businesses::BusinessController
  areas_of_responsibility :gitcoin

  before_action :ensure_billing_enabled
  before_action :business_access_required

  def update
    return render_404 unless Billing::Budget.configurable?(this_business)
    return render_404 unless Billing::Budget.valid_product?(params[:product])

    budget = this_business.budget_for(product: params[:product])
    budget.configure(
      enforce_spending_limit: params[:enforce_spending_limit] == "true",
      limit: params[:spending_limit],
    )

    if budget.errors.any?
      flash[:error] = "Unable to set a spending limit. Please check your payment method and limit"
    else
      flash[:notice] = "Spending limit configuration has been updated"
    end

    redirect_back fallback_location: settings_billing_enterprise_path(this_business)
  end
end
