# frozen_string_literal: true

class Businesses::Actions::RunnerGroupsController < Businesses::BusinessController
  include ::Actions::RunnerGroupsHelper

  map_to_service :actions_runners

  before_action :business_admin_required
  before_action :ensure_actions_enabled
  before_action :ensure_actions_enterprise_runners
  before_action :ensure_tenant_exists, only: :create

  javascript_bundle :settings

  def create
    name = params[:name]
    visibility = params[:visibility]&.to_sym

    if name.nil? || visibility.nil?
      flash[:error] = "Failed to create runner group."
      redirect_to settings_actions_self_hosted_runners_enterprise_path
      return
    end

    selected_organizations = visibility == GitHub::LaunchClient::RunnerGroups::GROUP_VISIBILITY_SELECTED ?
      Array(params[:organization_ids]) : []

    result = create_group_for(this_business, name: name, visibility: visibility, selected_targets: selected_organizations)
    if !result.call_succeeded?
      flash[:error] = "Failed to create runner group."
    else
      flash[:notice] = "Runner group created."
    end

    redirect_to settings_actions_self_hosted_runners_enterprise_path
  end

  def update
    name = params[:name]
    visibility = params[:visibility]&.to_sym

    runner_group_id = params[:id]&.to_i

    if runner_group_id.nil? || name.nil? || visibility.nil?
      flash[:error] = "Failed to update runner group."
      redirect_to settings_actions_self_hosted_runners_enterprise_path
      return
    end

    selected_organizations = visibility == GitHub::LaunchClient::RunnerGroups::GROUP_VISIBILITY_SELECTED ?
      Array(params[:organization_ids]) : []

    result = update_group_for(this_business, id: runner_group_id, name: name, visibility: visibility, selected_targets: selected_organizations)
    if !result.call_succeeded?
      flash[:error] = "Failed to update runner group."
    else
      flash[:notice] = "Runner group updated."
    end

    redirect_to settings_actions_self_hosted_runners_enterprise_path
  end

  def destroy
    runner_group_id = params[:id]&.to_i

    if runner_group_id.nil?
      flash[:error] = "Failed to delete runner group."
      redirect_to settings_actions_self_hosted_runners_enterprise_path
      return
    end

    result = delete_group_for(this_business, id: runner_group_id)
    if !result.call_succeeded?
      flash[:error] = "Failed to delete runner group."
    else
      flash[:notice] = "Runner group deleted."
    end

    redirect_to settings_actions_self_hosted_runners_enterprise_path
  end

  def show_selected_targets
    runner_group_id = params[:id]&.to_i
    runner_group = runner_group_for(this_business, id: runner_group_id) if runner_group_id.present?

    selected_organizations = []
    if runner_group.present?
      selected_organizations = runner_group.selected_targets.map { |identity| identity.global_id }.to_set
    end

    respond_to do |format|
      format.html do
        render Businesses::Actions::OrganizationSelectionComponent.new(
          business: this_business,
          organizations: this_business.organizations,
          selected_organizations: selected_organizations,
          policy_type: Orgs::ActionsSettings::RepositoryItemsController::RUNNER_GROUPS_POLICY,
          policy_id: runner_group_id,
        )
      end
    end
  end

  def bulk_actions
    if runner_ids = params[:runner_ids]
      render Actions::RunnerGroupsBulkComponent.new(
        owner: this_business,
        owner_settings: Actions::EnterpriseRunnersView.new(settings_owner: this_business, current_user: current_user),
        runner_ids: runner_ids,
      )
    else
      head :ok
    end
  end

  def show_menu
    render Actions::RunnerGroupsMenuItemsComponent.new(
      owner: this_business,
      owner_settings: Actions::EnterpriseRunnersView.new(settings_owner: this_business, current_user: current_user),
      runner_groups: Actions::RunnerGroup.for_entity(this_business),
    )
  end

  def update_runners
    runner_group_id = params[:runner_group_id]&.to_i
    runner_ids = Array(params[:runner_ids]).map(&:to_i)

    unless runner_group_id.present? && runner_ids.any?
      flash[:error] = "Failed to move runners."
      redirect_to settings_actions_self_hosted_runners_enterprise_path
      return
    end

    runner_group = add_runners_for(this_business, id: runner_group_id, runners_ids: runner_ids)
    if runner_group.present?
      flash[:notice] = "Moved runners to #{runner_group.name}."
    else
      flash[:error] = "Failed to move runners."
    end

    redirect_to settings_actions_self_hosted_runners_enterprise_path
  end

  private

  def ensure_actions_enabled
    render_404 unless GitHub.actions_enabled?
  end

  def ensure_actions_enterprise_runners
    render_404 unless GitHub.flipper[:actions_enterprise_runners].enabled?(current_user) || GitHub.enterprise?
  end

  def ensure_tenant_exists
    result = GrpcHelper.rescue_from_grpc_errors("EnterpriseTenant") do
      GitHub::LaunchClient::Deployer.setup_tenant(this_business)
    end
    raise Timeout::Error, "Timeout fetching tenant" unless result.call_succeeded?
  end
end
