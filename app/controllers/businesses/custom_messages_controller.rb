# frozen_string_literal: true

class Businesses::CustomMessagesController < Businesses::BusinessController
  include CustomMessagesHelper


  before_action :business_admin_required
  before_action :single_business_environment_required

  def show
    custom_messages = CustomMessages.instance
    @sign_in_message = custom_messages.sign_in_message
    @sign_out_message = custom_messages.sign_out_message
    @suspended_message = custom_messages.suspended_message
    @auth_provider_name = custom_messages.auth_provider_name

    render "businesses/custom_messages/show"
  end

  def create
    create_or_update
  end

  def update
    create_or_update
  end

  def sign_in_message
    @custom_messages = CustomMessages.instance
    render "businesses/custom_messages/sign_in_message"
  end

  def sign_out_message
    @custom_messages = CustomMessages.instance
    render "businesses/custom_messages/sign_out_message"
  end

  def suspended_user_message
    @custom_messages = CustomMessages.instance
    render "businesses/custom_messages/suspended_user_message"
  end

  def authorization_provider_name
    @custom_messages = CustomMessages.instance
    render "businesses/custom_messages/authorization_provider_name",
      locals: { custom_messages: @custom_messages }
  end

  def preview_sign_in_message
    @preview_message = params[:custom_message_preview_value]
    render "sessions/new", layout: authentication_layout
  end

  def preview_sign_out_message
    @preview_message = params[:custom_message_preview_value]
    render "dashboard/logged_out", layout: authentication_layout
  end

  def preview_suspended_message
    @preview_message = params[:custom_message_preview_value]
    @preview_message =  default_suspended_message if @preview_message.blank?
    render "sessions/suspended", layout: authentication_layout
  end

  def preview_authorization_provider_name
    @preview_name = params[:custom_message_preview_value]
    render "dashboard/logged_out", layout: authentication_layout
  end

  def announcement
    @custom_messages = CustomMessages.instance # previewable_comment_form needs this :/
    render "businesses/custom_messages/announcement", locals: {
      announcement: current_announcement,
      expires_at: GitHub.kv.ttl("enterprise:announcement").value { nil }
    }
  end

  def preview_announcement
    render "businesses/custom_messages/preview_announcement", locals: {
      announcement: params[:custom_message_preview_value]
    }
  end

  def set_announcement
    announcement = params[:custom_messages][:announcement]
    expires_at = params[:custom_messages][:announcement_expires_at]
    if announcement&.strip.blank?
      GitHub.kv.del("enterprise:announcement")
      notice = "Cleared the announcement."
    else
      if expires_at.present?
        if valid_expires_at?(expires_at)
          GitHub.kv.set \
            "enterprise:announcement", announcement,
            expires: Date.parse(expires_at).to_time
        else
          flash[:error] = "Announcement expiry date must be a valid date in the future."
          return redirect_to custom_messages_announcement_enterprise_path(this_business)
        end
      else
        GitHub.kv.set("enterprise:announcement", announcement)
      end
      notice = "Successfully set the announcement."
    end

    redirect_to custom_messages_enterprise_path(this_business), notice: notice
  end

  private

  def valid_expires_at?(expires_at)
    Date.parse(expires_at.to_s).to_time.future?
  rescue ArgumentError
    false
  end

  def create_or_update
    @custom_messages = CustomMessages.instance

    if @custom_messages.create_or_update_attributes(custom_messages_params)
      redirect_to custom_messages_enterprise_path(this_business),
        notice: "Successfully updated your custom message."
    else
      render "businesses/custom_messages/show"
    end
  end

  def custom_messages_params
    params.require(:custom_messages).permit %i[
      sign_in_message
      sign_out_message
      suspended_message
      support_url
      auth_provider_name
    ]
  end

  def current_announcement
    return @current_announcement if defined?(@current_announcement)
    @current_announcement = GitHub.kv.get("enterprise:announcement").value!
  end
  helper_method :current_announcement
end
