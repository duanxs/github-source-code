# frozen_string_literal: true

class Businesses::ProjectsController < Businesses::BusinessController

  before_action :business_admin_required

  def index
    render_template_view "businesses/settings/projects",
      Businesses::Settings::ProjectsView,
      business: this_business,
      params: params
  end

  def update_organization_projects_allowed
    allowed = params[:organization_projects_allowed]&.to_s
    validate_setting value: allowed

    message = ""
    case allowed
    when "enabled"
      this_business.enable_organization_projects(actor: current_user, force: true)
      message = "Projects are enabled for all organizations for this enterprise."
    when "disabled"
      this_business.disable_organization_projects(actor: current_user, force: true)
      message = "Projects are disabled for all organizations for this enterprise."
    when "no_policy"
      this_business.clear_organization_projects_setting(actor: current_user)
      message = "Projects policy was removed. Individual organizations may enable or disable projects."
    end

    redirect_to settings_projects_enterprise_path(this_business), notice: message
  end

  def update_repository_projects_allowed
    allowed = params[:repository_projects_allowed]&.to_s
    validate_setting value: allowed

    message = ""
    case allowed
    when "enabled"
      this_business.enable_repository_projects(actor: current_user, force: true)
      message = "Projects are enabled for all repositories for this enterprise."
    when "disabled"
      this_business.disable_repository_projects(actor: current_user, force: true)
      message = "Projects are disabled for all repositories for this enterprise."
    when "no_policy"
      this_business.clear_repository_projects_setting(actor: current_user)
      message = "Repository projects policy was removed. Individual organizations may enable or disable repository projects."
    end

    redirect_to settings_projects_enterprise_path(this_business), notice: message
  end
end
