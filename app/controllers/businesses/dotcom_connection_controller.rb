# frozen_string_literal: true

class Businesses::DotcomConnectionController < Businesses::BusinessController

  before_action :business_admin_required
  before_action :disable_for_fedramp_private_instance
  before_action :single_business_environment_required
  before_action :require_dotcom_connection_enabled
  before_action :require_actions_enabled, only: :change_actions_download_archive

  before_action :add_csp_exceptions, only: :index
  CSP_EXCEPTIONS = {
    form_action: ["#{GitHub.dotcom_host_protocol}://#{GitHub.dotcom_host_name}"],
    preserve_schemes: GitHub.dotcom_host_protocol != "https",
  }.freeze

  FEATURE_ADDED_MESSAGES = {
    "contributions"    => "Contributions sharing with GitHub.com enabled.",
    "search"           => "Search on GitHub.com enabled.",
    "private_search"   => "Private repositories in the connected organization will be included in the GitHub.com search results.",
    "content_analysis" => "Content analysis enabled.",
    "content_analysis_notifications" => "Content analysis notifications enabled.",
    "license_usage_sync" => "User license usage will be uploaded to your Enterprise Cloud account.",
  }

  FEATURE_REMOVED_MESSAGES = {
    "contributions"    => "Contributions sharing with GitHub.com disabled.",
    "search"           => "Search on GitHub.com disabled.",
    "private_search"   => "Private repositories in the connected organization will not be included in the GitHub.com search results.",
    "content_analysis" => "Content analysis disabled.",
    "content_analysis_notifications" => "Content analysis notifications disabled.",
    "license_usage_sync" => "User license usage will not be uploaded to your Enterprise Cloud account.",
  }

  def index
    render "businesses/settings/dotcom_connection", locals: {
      slug: this_business.slug,
    }
  end

  def create
    dotcom_connection.generate_keys
    if token = dotcom_connection.request_authentication_token
      redirect_to dotcom_enterprise_installation_url(token, generate_random_state)
    else
      redirect_error
    end
  rescue GitHub::Connect::Authenticator::ConnectionError
    redirect_error
  end

  def resume
    if token = dotcom_connection.temp_authentication_token
      redirect_to dotcom_enterprise_installation_url(token, current_state)
    else
      redirect_error
    end
  end

  def complete
    if dotcom_connection.temp_authentication_token && state_matches_session?
      ActiveRecord::Base.connected_to(role: :writing) do
        dotcom_connection.create [params[:version], params[:token]].join("."), params[:app_id], params[:installation_id]
        dotcom_connection.update_application_info
      end
      redirect_to admin_settings_dotcom_connection_enterprise_path(GitHub.global_business), notice: "Successfully connected your Enterprise instance to GitHub.com."
    else
      redirect_error
    end
  end

  def destroy
    dotcom_connection.destroy
    GitHub.disable_dotcom_search(current_user)
    GitHub.disable_dotcom_contributions(current_user)
    GitHub.disable_dotcom_user_license_usage_upload(current_user)
    GitHub.disable_ghe_content_analysis(current_user)
    GitHub.disable_ghe_content_analysis_notifications(current_user)
    redirect_to admin_settings_dotcom_connection_enterprise_path(GitHub.global_business), notice: "Successfully disconnected your Enterprise instance from GitHub.com."
  rescue GitHub::Connect::Authenticator::ConnectionError
    redirect_error
  rescue GitHub::Connect::Authenticator::AuthenticationError
    dotcom_connection.reset_tokens
    GitHub.disable_dotcom_search(current_user)
    GitHub.disable_dotcom_contributions(current_user)
    GitHub.disable_dotcom_user_license_usage_upload(current_user)
    GitHub.disable_ghe_content_analysis(current_user)
    GitHub.disable_ghe_content_analysis_notifications(current_user)
    redirect_error
  end

  def change_search
    val = params[:public_search_value]

    if val == "true"
      dotcom_connection.clear_pending_features
      dotcom_connection.add_feature("search")
    elsif val == "false"
      dotcom_connection.clear_pending_features
      dotcom_connection.remove_feature("search")
      dotcom_connection.remove_feature("private_search")
    end

    redirect_to permission_upgrade_or_admin_url
  end

  def change_private_search
    val = params[:private_search_value]

    if val == "true"
      dotcom_connection.clear_pending_features
      dotcom_connection.add_feature("private_search")
    elsif val == "false"
      dotcom_connection.clear_pending_features
      dotcom_connection.remove_feature("private_search")
    end

    redirect_to permission_upgrade_or_admin_url
  end

  def change_actions_download_archive
    val = params[:actions_download_archive_value]

    if val == "true"
      dotcom_connection.clear_pending_features
      dotcom_connection.add_feature("actions_download_archive")
    elsif val == "false"
      dotcom_connection.clear_pending_features
      dotcom_connection.remove_feature("actions_download_archive")
    end

    redirect_to permission_upgrade_or_admin_url
  end


  def change_contributions
    val = params[:contributions_sync_value]

    if val == "true"
      dotcom_connection.clear_pending_features
      dotcom_connection.add_feature("contributions")
    elsif val == "false"
      dotcom_connection.clear_pending_features
      dotcom_connection.authenticator.remove_all_contributions
      dotcom_connection.remove_feature("contributions")
    end

    redirect_to permission_upgrade_or_admin_url
  end

  def change_license_usage_sync
    unless dotcom_connection.owner_type == "business"
      return redirect_to \
        admin_settings_dotcom_connection_enterprise_path(GitHub.global_business),
        flash: {
          error: "This feature may only be enabled when connected to a GitHub.com enterprise account.",
        }
    end

    val = params[:license_usage_sync_value]

    if val == "true"
      dotcom_connection.clear_pending_features
      dotcom_connection.add_feature("license_usage_sync")
    elsif val == "false"
      dotcom_connection.clear_pending_features
      dotcom_connection.remove_feature("license_usage_sync")
    end

    redirect_to permission_upgrade_or_admin_url
  end

  def change_content_analysis
    case params[:content_analysis_value]
    when "enabled_with_notifications"
      dotcom_connection.clear_pending_features
      dotcom_connection.add_feature("content_analysis")
      dotcom_connection.add_feature("content_analysis_notifications")
    when "enabled_without_notifications"
      dotcom_connection.clear_pending_features
      dotcom_connection.add_feature("content_analysis")
      dotcom_connection.remove_feature("content_analysis_notifications")
    when "disabled"
      dotcom_connection.clear_pending_features
      dotcom_connection.remove_feature("content_analysis")
      dotcom_connection.remove_feature("content_analysis_notifications")
    end

    redirect_to permission_upgrade_or_admin_url
  end

  def change_complete
    if state_matches_session?
      ActiveRecord::Base.connected_to(role: :writing) do
        apply_pending_config_changes
      end
      redirect_to admin_settings_dotcom_connection_enterprise_path(GitHub.global_business)
    else
      ActiveRecord::Base.connected_to(role: :writing) do
        dotcom_connection.clear_pending_features
      end
      flash[:notice] = nil
      flash[:error] = params[:error]
      error = GitHub::Connect::ApiError.new(params[:error]) if params[:error]
      redirect_error(error)
    end
  end

  private

  # Private: Dotcom URL for permissions upgrade/downgrade if any is needed,
  #          local (GHE) admin path otherwise
  def permission_upgrade_or_admin_url
    result = dotcom_connection.update_permissions
    if !result["success"]
      dotcom_connection.clear_pending_features
      flash[:error] = "There was an error updating the GitHub Connect settings."
      admin_settings_dotcom_connection_enterprise_path(GitHub.global_business)
    elsif path = result["url"]
      query_args = {
        state: generate_random_state,
        return_to: admin_settings_change_complete_enterprise_url(this_business),
      }
      url = Addressable::URI.new(
        scheme: GitHub.dotcom_host_protocol,
        host: GitHub.dotcom_host_name,
        path: path,
        query: query_args.to_query,
      )
      url.to_s
    else
      apply_pending_config_changes
      admin_settings_dotcom_connection_enterprise_path(GitHub.global_business)
    end
  end

  def apply_pending_config_changes
    change_messages = (
      dotcom_connection.added_features.map   { |f| FEATURE_ADDED_MESSAGES[f] } +
      dotcom_connection.removed_features.map { |f| FEATURE_REMOVED_MESSAGES[f] }
    ).join(" ")
    dotcom_connection.apply_pending_feature_changes(current_user)
    flash[:notice] = change_messages if change_messages.present?
  end

  def require_dotcom_connection_enabled
    render_404 unless GitHub.dotcom_connection_enabled?
  end

  def require_actions_enabled
    render_404 unless GitHub.actions_enabled?
  end


  def redirect_error(error = nil)
    error ||= GitHub::Connect::ServiceUnavailableError.new("API Inaccessible")
    Failbot.report(error , {app: "dotcom-connection"})
    GitHub.stats.increment("dotcom_connection.error") if GitHub.enterprise?
    flash[:error] ||= "Failed to connect to GitHub.com, please try again."
    redirect_to admin_settings_dotcom_connection_enterprise_path(GitHub.global_business)
  end

  def dotcom_connection
    @dotcom_connection ||= begin
      dotcom_connection = ::DotcomConnection.new
      dotcom_connection.actor = current_user
      dotcom_connection
    end
  end

  def dotcom_enterprise_installation_url(token, state)
    new_enterprise_installation_url(host: GitHub.dotcom_host_name, protocol: GitHub.dotcom_host_protocol, state: state, token: token)
  end

  def generate_random_state
    session[:github_connect_state] = SecureRandom.hex(8)
  end

  def current_state
    session[:github_connect_state]
  end

  def state_matches_session?
    params[:state].present? && SecurityUtils.secure_compare(params[:state], session.delete(:github_connect_state))
  end
end
