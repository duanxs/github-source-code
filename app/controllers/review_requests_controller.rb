# frozen_string_literal: true

class ReviewRequestsController < AbstractRepositoryController
  areas_of_responsibility :code_collab, :pull_requests

  statsd_tag_actions only: [:index, :menu]

  include ShowPartial

  before_action :login_required
  before_action :writable_repository_required, except: %w[menu]

  CreateRequestQuery = parse_query <<-'GRAPHQL'
    mutation($input: RequestReviewsInput!) {
      requestReviews(input: $input) {
        pullRequest {
          number
        }
      }
    }
  GRAPHQL

  # Request review for a pull request
  #
  # Expected params:
  #
  #   :reviewer_ids - The User ids you wish to build requests from
  #   :partial - the partial to update
  #
  # Creates new review requests
  # Updates the pull_request with the new reviewers ids.
  #
  # For AJAX requests, JSON is returned with the HTML of the infobar and
  # context pane partials.
  def create
    input = {
      "pullRequestId" => params[:pr_global_id],
      "userIds" => user_reviewer_global_ids,
      "teamIds" => team_reviewer_global_ids,
    }

    if params[:re_request_reviewer_id].present?
      input["union"] = true
      input["reRequest"] = true
    end

    data = platform_execute(CreateRequestQuery, variables: { "input" => input })

    if data.errors.all.any?
      Failbot.report(PullRequest::Error.new("Create Review request failed"), graphql_query_hash: input)
      return render_404
    end

    track_issue_edits_from_project_board(edited_fields: ["reviewers"])

    respond_to do |format|
      format.html do
        pull = current_repository.issues.find_by_number(data.request_reviews.pull_request.number)&.pull_request
        suggestions = show_suggestions?(pull) ? pull.suggested_reviewers : nil
        render partial: "issues/sidebar/show/reviewers", locals: {
          pull_request: pull,
          suggested_reviewers: suggestions,
          deferred_content: false,
        }
      end
    end
  end

  def create_new
    return render_404 unless request.xhr?
    pull = build_pull
    return head 404 unless pull

    suggestions = show_suggestions?(pull) ? pull.suggested_reviewers : nil

    respond_to do |format|
      format.html do
        render partial: "issues/sidebar/new/reviewers", locals: {
          pull: pull,
          suggested_reviewers: suggestions,
          deferred_content: false,
        }
      end
    end
  end

  def re_request_review
    pull = current_repository.issues.find_by_number(params[:id])&.pull_request
    reviewer = User.find(params[:reviewer_id])

    if pull.review_requested_for?(reviewer)
      return redirect_to pull_request_path(pull)
    end

    data = platform_execute(CreateRequestQuery, variables: {
      "input" => {
        "pullRequestId" => pull.global_relay_id,
        "userIds" => [reviewer.global_relay_id],
        "union" => true,
        "reRequest" => true,
      },
    })

    return render_404 if data.errors.all.any?

    redirect_to pull_request_path(pull)
  end

  def menu
    return render_404 unless request.xhr?

    pull =
      if params[:id]
        current_repository.issues.find_by_number(params[:id])&.pull_request
      else
        build_pull
      end

    return head 404 unless pull

    review_requests = pull.pending_review_requests
    possible_reviewers = pull.sorted_reviewers(current_user, requests: review_requests).to_a.uniq

    respond_to do |format|
      format.json do
        payload = {}
        requested_teams = review_requests.teams
        if pull.include_reviewer_suggestions?(user: current_user)
          reviewer_suggestions = pull.suggested_reviewers(requests: review_requests,
                                                          teams: requested_teams)

          # Don't show suggested users twice
          reviewer_suggestion_users = reviewer_suggestions.map(&:user)
          possible_reviewers -= reviewer_suggestion_users
          GitHub::PrefillAssociations.for_user_statuses(reviewer_suggestion_users)

          payload[:suggestions] = reviewer_suggestions.map do |suggestion|
            reviewer_json_payload(pull, suggestion.user).update(description: suggestion.description)
          end
        end

        possible_user_reviewers = possible_reviewers.select { |reviewer| reviewer.is_a?(User) }
        GitHub::PrefillAssociations.for_user_statuses(possible_user_reviewers)

        payload[:users] = possible_reviewers.map do |reviewer|
          reviewer_json_payload(pull, reviewer)
        end

        render json: payload
      end
    end
  end

  private

  def reviewer_json_payload(pull, reviewer)
    type = reviewer.is_a?(Team) ? "team" : "user"
    direct_request = pull.direct_review_request_for(reviewer)
    is_selected = !!direct_request
    is_disabled = is_selected && !pull.review_request_removable?(direct_request)
    name = if reviewer.is_a?(User)
      user_status = reviewer.user_status
      if !user_status&.expired? && user_status&.limited_availability?
        "#{reviewer.profile_name} (busy)"
      else
        reviewer.profile_name
      end
    else
      reviewer.name
    end

    {
      id: reviewer.id,
      type: type,
      class: helpers.avatar_class_names(reviewer),
      selected: is_selected,
      disabled: is_disabled,
      avatar: reviewer.primary_avatar_url(40),
      login: reviewer.to_s,
      name: name,
    }
  end

  # When a review is requested from a suggested user, we still want to show
  # the list of remaining suggestions. Stop showing suggestions when a review
  # request is made from a non-suggested user.
  #
  # Returns true the suggestion list should appear in the sidebar.
  def show_suggestions?(pull)
    suggestion_used? && pull.suggested_reviewers.any?
  end

  def suggestion_used?
    params[:suggested_reviewer_id].present?
  end

  def user_reviewer_global_ids
    User.find(reviewer_ids).map(&:global_relay_id)
  end

  def team_reviewer_global_ids
    current_repository.teams(immediate_only: false).closed.find(team_ids).map(&:global_relay_id)
  end

  def reviewer_ids
    ids = params.fetch(:reviewer_user_ids, []) << params[:suggested_reviewer_id]
    ids << params[:re_request_reviewer_id]
    ids.select(&:present?).uniq
  end

  def team_ids
    return [] unless current_repository.in_organization?

    reviewer_team_ids = params.fetch(:reviewer_team_ids, []).select(&:present?).uniq
    current_repository.teams(immediate_only: false).closed.where(id: reviewer_team_ids).pluck(:id)
  end

  # Build enough of a pull request object to render the requested reviewer
  # sidebar template on the pull request create page.
  #
  # Returns an unsaved PullRequest.
  def build_pull
    pull =
      if params[:range].present?
        comparison = GitHub::Comparison.from_range(current_repository, params[:range])
        if comparison.valid? && comparison.viewable_by?(current_user)
          comparison.build_pull_request(user: current_user)
        end
      else
        current_repository.pull_requests.build(user: current_user)
      end

    return unless pull
    pull.issue = current_repository.issues.build

    if current_repository.pushable_by?(current_user)
      reviewer_ids.each do |id|
        pull.review_requests.build(reviewer_id: id, reviewer_type: "User")
      end

      team_ids.each do |id|
        pull.review_requests.build(reviewer_id: id, reviewer_type: "Team")
      end
    end

    pull
  end

  def route_supports_advisory_workspaces?
    true
  end
end
