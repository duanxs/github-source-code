# frozen_string_literal: true

# Shared controller methods for commiting a change from the web interface
# Depends on `#current_user`, `#current_repository`
module WebCommitControllerMethods
  def self.included(base)
    base.helper_method :commit_message_placeholder
  end

  private

  # Internal: Establish a default @target_repo and lazily fork the current
  # repository if we can't push
  def establish_target_repository(branch:, create_fork:)
    return unless branch
    @target_repo = current_repository
    auto_fork(branch: branch, create_fork: create_fork) unless current_user_can_push_to_ref?(branch)
    if params[:same_repo] && params[:target_branch] != branch
      # if you're creating a branch, check if you have full write permission on the repo
      if @target_repo.pushable_by?(current_user)
        true
      else
        @target_repo = nil
        false
      end
    else
      if @target_repo && @target_repo.pushable_by?(current_user, ref: branch)
        # it is ok still, as long as you have fork collab permissions
        true
      else
        @target_repo = nil
        false
      end
    end
  end

  # Internal: Fork the current repository so the current user
  # can propose a change.
  #
  # branch - the branch for this proposed change - necessary
  #          in case a fork already exists, so this branch
  #          will be pulled over to the old fork
  # create_fork - optional Boolean for creating a fork
  #
  # Also updates the target repository for the file form.
  def auto_fork(branch:, create_fork:)
    repo = current_repository
    user = current_user

    if repo.network_has_fork_for?(user)
      # repo is already forked - ensure the target branch exists
      # the fork may be old and missing newer branches
      ref = repo.heads.find(branch)
      @forked_repo = repo.find_fork_in_network_for_user(user)

      if ref && @forked_repo.exists_on_disk? && !@forked_repo.heads.exist?(branch)
        @forked_repo.fetch_commits_from(ref, user: user)
      end
    elsif create_fork
      # make a new fork
      @forked_repo, @forked_reason = repo.fork(forker: user)
    else
      return
    end

    @target_repo = @forked_repo
  end

  # Internal: Set the commit for the new/edit/delete file form
  #
  # new - is this for a new file?
  #       (optional, defaults to false)
  def set_form_commit(new = false)
    if ref = current_repository.heads.find(tree_name)
      @last_commit = ref.target_oid
    elsif new && current_repository.heads.empty?
      # creating a file in an empty repo is allowed
      @last_commit = ""
    end
  end

  # Internal: Combine message and description from params into a full commit message.
  #
  # action - action being performed (new, edit, delete)
  #
  # Returns the String commit message
  def commit_message_from_request(action)
    message =
      if params[:message].present?
        params[:message]
      elsif params[:placeholder_message].present?
        params[:placeholder_message]
      else
        commit_message_placeholder(action)
      end
    [message, params[:description]].join("\n\n")
  end

  # Public: placeholder text for the commit message
  #
  # action - action being performed (new, edit, or delete)
  #
  # Returns the String commit message placeholder
  def commit_message_placeholder(action)
    send("#{action}_commit_message_placeholder")
  end

  # Internal: commit message placeholder for file edits
  #
  # used by commit_message_placeholder
  def edit_commit_message_placeholder
    filename = @filename.blank? ? path_string : @filename
    "Update #{filename}".dup.force_encoding("UTF-8").scrub!
  end

  # Internal: commit message placeholder for new files
  #
  # used by commit_message_placeholder
  def new_commit_message_placeholder
    filename = @filename.blank? ? "new file" : @filename
    "Create #{filename}".dup.force_encoding("UTF-8").scrub!
  end

  # Internal: commit message placeholder for deleting files
  #
  # used by commit_message_placeholder
  def delete_commit_message_placeholder
    filename = @filename.blank? ? path_string : @filename
    "Delete #{filename}".dup.force_encoding("UTF-8").scrub!
  end

  # Internal: Check for a conflict (repo change)
  #
  # path        - file path for proposed change
  # old_oid     - commit oid when proposed change was submitted
  # current_oid - current commit oid where proposed change will take place
  #               (optional: will default to current branch location)
  #
  # Sets flash error
  # Returns Boolean result
  def change_conflict?(path, old_oid, current_oid = commit_sha)
    return false if params[:quick_pull] # quick pull creates a new branch, so it doesn't matter
    return false unless current_oid != old_oid
    return false unless detect_conflict(path, old_oid, current_oid)

    @contents = params[:value]
    @allow_contents_unchanged = true
    commit = current_repository.commits.find(current_oid)
    user_login = commit&.author&.login || commit&.committer&.login || :Someone
    url = compare_path(current_repository, "#{old_oid}...#{current_oid}")

    flash.now[:error] = \
      "#{user_login} has committed since you started editing. " \
      "<a href='#{url}'>See what changed</a>".html_safe # rubocop:disable Rails/OutputSafety

    return true
  end

  # Internal: Actually check for a conflict
  #
  # A "conflict" is any change to the actual path in question
  #
  # path        - file path for proposed change
  # old_oid     - commit oid when proposed change was submitted
  # current_oid - current commit oid where proposed change will take place
  #
  # Returns true if conflict was detected, false otherwise
  def detect_conflict(path, old_oid, current_oid)
    science "web_commit.detect_conflict" do |e|
      e.context path: path,
                old_oid: old_oid,
                current_oid: current_oid,
                repo_name: current_repository.nwo
      e.use { old_detect_conflict(path, old_oid, current_oid) }
      e.try { new_detect_conflict(path, old_oid, current_oid) }
    end
  end

  def old_detect_conflict(path, old_oid, current_oid)
    return true unless old_oid && current_oid

    repo = current_repository

    GitHub.dogstats.time("web_edit", tags: ["action:detect_conflict"]) do
      old_blob = repo.blob(old_oid, path)
      new_blob = repo.blob(current_oid, path)

      blobs = [old_blob, new_blob].compact.length
      return true  if blobs == 1  # added or removed
      return false if blobs == 0  # non-existent

      old_blob.sha != new_blob.sha
    end
  end

  def new_detect_conflict(path, old_oid, current_oid)
    return true unless old_oid && current_oid

    diff = GitHub::Diff.new(current_repository, old_oid, current_oid)
    diff.requested_paths.any? do |delta_path|
      delta_path == path || delta_path.start_with?(path)
    end
  end

  # Internal: Commit a change (create, update, or destroy)
  #
  # repo    - repo where change is actually made
  # user    - user making change
  # branch  - branch name for this commit
  # old_oid - commit oid when proposed change was submitted
  # files   - Hash of filename => data pairs.
  # message - message to use for the commit
  #
  # Notes:
  # - This really belongs in a model, probably Repository. Refactoring is an iterative process.
  # - This method is nearly identical to PullRequestReviewComment::ApplySuggestedChange#commit_blob_change_to_repo_for_user
  #   These should probably be extracted into a single shared method, but for now,
  #   if making changes, you may need to make them in both places.
  #
  # Returns String branch name when successful, false otherwise
  def commit_change_to_repo_for_user(repo, user, branch, old_oid, files, message)
    return false unless repo.ready_for_writes?

    author_email = params[:author_email]

    # We double check that email belongs to the user in
    # CommitsCollection#create, but we enforce this here as well.
    unless author_email.nil? || user.emails.verified.pluck(:email).include?(author_email)
      return false
    end

    if params[:quick_pull].blank?
      ref = repo.heads.find_or_build(branch)
    elsif params[:same_repo]
      # commit this change to a temp branch and send to pull-request page
      branch   = params[:target_branch].to_s
      branch   = Git::Ref.normalize(branch) || repo.heads.temp_name(topic: "patch", prefix: user.login)
      branch   = repo.heads.temp_name(topic: branch) if repo.heads.exist?(branch)
      ref = repo.heads.find_or_build(branch)
    else
      base_ref = quick_pull_base_ref(params[:quick_pull], branch)
      branch   = repo.heads.temp_name(topic: "patch")
      ref = repo.fetch_commits_from(base_ref, new_ref_name: branch, user: user)
    end

    # use the branch's target_oid, or fall back on the supplied old_oid.
    # both can be nil for an absolutely new file in a new repo.
    parent_oid = ref.target_oid || old_oid

    commit = repo.create_commit(parent_oid,
      message: message,
      author: user,
      files: files,
      author_email: author_email,
      sign: true,
    )

    return false unless commit

    before_oid = ref.target_oid
    ref.update(commit, user, post_receive: !redirect_back_to_pr?)

    if redirect_back_to_pr?
      PullRequest.synchronize_requests_for_ref(
        repo,
        ref.qualified_name,
        user,
        before: before_oid,
        after: commit.oid,
      )
      ref.enqueue_push_job(before_oid, commit.oid, user)
    end

    if user.primary_user_email.email != author_email
      GitHub.dogstats.increment("commit.custom_commit_email", tags: ["email:custom"])
    else
      GitHub.dogstats.increment("commit.custom_commit_email", tags: ["email:primary"])
    end

    ref.name
  rescue Git::Ref::HookFailed => e
    @hook_out = e.message
    false
  rescue Git::Ref::ProtectedBranchUpdateError => e
    @hook_out = "#{ref.name} branch is protected"
    false
  rescue Git::Ref::ComparisonMismatch,
         Git::Ref::InvalidName,
         Git::Ref::UpdateFailed,
         GitRPC::Failure,
         GitRPC::BadGitmodules,
         GitRPC::SymlinkDisallowed,
         GitHub::DGit::UnroutedError,
         GitHub::DGit::InsufficientQuorumError,
         GitHub::DGit::ThreepcFailedToLock
    false
  end

  # Internal: get the base ref for this quick pull
  #
  # base   - String representing the quick-pull base
  #          example: ymendel:cloaked-octo-lana
  # branch - String branch name
  #
  # Returns a Ref
  def quick_pull_base_ref(base, branch)
    return unless base

    repo = nil

    if base == "1"
      repo = current_repository.parent
    else
      user = base.split(":", 2).first
      repo = current_repository.find_fork_in_network_for_user(user)
    end

    return unless repo

    repo.heads.find(branch)
  end

  def increment_quick_pull_error_stats
    if cross_repo_quick_pull?
      GitHub.dogstats.increment("pull_request", tags: ["type:quick", "error:fail", "repo:cross"])
    elsif same_repo_quick_pull?
      GitHub.dogstats.increment("pull_request", tags: ["type:quick", "error:fail", "repo:same"])
    end
  end

  def increment_quick_pull_commit_stats
    if cross_repo_quick_pull?
      GitHub.dogstats.increment("pull_request", tags: ["type:quick", "repo:cross", "action:commit"])
    elsif same_repo_quick_pull?
      GitHub.dogstats.increment("pull_request", tags: ["type:quick", "repo:same", "action:commit"])
    end
  end

  def increment_quick_pull_start_stats
    if cross_repo_quick_pull?
      GitHub.dogstats.increment("pull_request", tags: ["type:quick", "repo:cross", "action:start"])
    elsif same_repo_quick_pull?
      GitHub.dogstats.increment("pull_request", tags: ["type:quick", "repo:same", "action:start"])
    end
  end

  # Internal: Boolean check for whether pull request target repo is different
  # from the current repository
  def cross_repo_quick_pull?
    @target_repo != current_repository
  end

  # Internal: Boolean check for whether pull request base and head branch belong
  # to the same repo
  def same_repo_quick_pull?
    @target_repo == current_repository && !params[:target_branch].blank? && params[:target_branch] != @branch
  end

  # Internal: Boolean check whether :pr param contains a valid pull request url
  def redirect_back_to_pr?
    params[:pr] =~ %r{\A/([\w.-]+)/([\w.-]+)/pull/(\d+)\z}
  end

  # Internal: clean out the params hash
  #
  # Sets blank params to nil
  def clean_params
    params[:quick_pull] = nil if params[:quick_pull]&.blank?
    true
  end
end
