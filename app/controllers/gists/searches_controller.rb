# frozen_string_literal: true

class Gists::SearchesController < Gists::ApplicationController
  before_action :search_query_required, only: :show

  helper_method :language, :search_query, :gist_query

  def new
    render "gists/searches/new"
  end

  def show
    results = gist_query.execute
    render "gists/searches/show", locals: { results: results }
  end

  def quick
    results = gist_quicksearch.execute
    gists = results.map { |hit| hit["_gist"] }
    @owned, @starred = gists.partition { |g| g.owner == current_user }

    render "gists/searches/quick", layout: false
  end

  private

  def search_query_required
    render "gists/searches/new" unless search_query.present?
  end

  def search_query
    params[:q]
  end

  def current_page
    if params[:p].blank? || !params[:p].respond_to?(:to_i)
      1
    else
      params[:p].to_i.abs
    end
  end

  def sort_field_and_direction
    [
      (params[:sort]      || params[:s] || "created"),
      (params[:direction] || params[:o] || "desc"),
    ]
  end

  def language
    @language ||= Linguist::Language[params[:l]]
  end

  def gist_query
    @gist_query ||= Search::Queries::GistQuery.new({
      phrase: search_query,
      current_user: current_user,
      remote_ip: request.remote_ip,
      aggregations: true,
      language: language,
      sort: sort_field_and_direction,
      page: current_page,
      per_page: Search::Query::PER_PAGE,
    })
  end

  def gist_quicksearch
    @quicksearch ||= Search::Queries::GistQuicksearch.new({
      phrase: search_query,
      current_user: current_user,
      remote_ip: request.remote_ip,
      per_page: Search::Query::PER_PAGE,
    })
  end

end
