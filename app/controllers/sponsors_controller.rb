# frozen_string_literal: true

class SponsorsController < ApplicationController
  before_action :add_csp_exceptions, only: [:show]

  FEATURED_ACCOUNTS_SAMPLE_SIZE = 5
  FEATURED_ORGS_SAMPLE_SIZE = 2

  CSP_EXCEPTIONS = {
    frame_src: ["https://www.youtube.com"],
  }

  def show
    listing_accounts = current_user&.sponsors_listing_accounts || []

    render_template_view "sponsors/show", Sponsors::FeaturedAccountsView, {
      accounts_max: FEATURED_ACCOUNTS_SAMPLE_SIZE,
      organizations_max: FEATURED_ORGS_SAMPLE_SIZE,
    }, locals: {
      current_user_unjoined_accounts: unjoined_accounts,
      current_user_unlisted_accounts: unlisted_members_accounts,
      current_user_listing_accounts: listing_accounts,
    }
  end

  def unjoined_accounts
    return [] unless logged_in?

    current_user.sponsors_enabled_accounts
                .reject(&:sponsors_program_member?)
  end

  def unlisted_members_accounts
    return [] unless logged_in?

    current_user.sponsors_enabled_accounts
                .select(&:sponsors_program_member?)
                .reject(&:sponsors_listing)
  end

  def target_for_conditional_access
    :no_target_for_conditional_access
  end
end
