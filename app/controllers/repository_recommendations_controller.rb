# frozen_string_literal: true

class RepositoryRecommendationsController < ApplicationController
  areas_of_responsibility :repository_recommendations

  before_action :login_required
  before_action :discover_repos_dashboard_required

  skip_before_action :perform_conditional_access_checks, only: %w( dismiss_recommendation )

  DismissRecommendationQuery = parse_query <<-'GRAPHQL'
    mutation($input: DismissRepositoryRecommendationInput!) {
      dismissRepositoryRecommendation(input: $input)
    }
  GRAPHQL

  def dismiss_recommendation
    data = platform_execute(DismissRecommendationQuery, variables: {
      input: { repositoryId: params[:repository_id] },
    })

    if data.errors.any?
      error_type = data.errors.details[:dismissRepositoryRecommendation]&.first["type"]
      case error_type
      when "NOT_FOUND", "FORBIDDEN"
        return head :not_found
      else
        return head :unprocessable_entity
      end
    end

    head :ok
  end

  RestoreRecommendationQuery = parse_query <<-'GRAPHQL'
    mutation($input: RestoreRepositoryRecommendationInput!) {
      restoreRepositoryRecommendation(input: $input)
    }
  GRAPHQL

  def restore_recommendation
    data = platform_execute(RestoreRecommendationQuery, variables: {
      input: { repositoryId: params[:repository_id] },
    })

    if data.errors.any?
      error_type = data.errors.details[:restoreRepositoryRecommendation]&.first["type"]
      case error_type
      when "NOT_FOUND", "FORBIDDEN"
        return head :not_found
      else
        return head :unprocessable_entity
      end
    end

    head :ok
  end

  private

  def target_for_conditional_access
    user = User.find_by_login(params[:user_id])
    return :no_target_for_conditional_access unless user
    user
  end
end
