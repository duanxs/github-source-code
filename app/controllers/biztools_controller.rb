# frozen_string_literal: true

class BiztoolsController < ApplicationController
  areas_of_responsibility :stafftools

  before_action :ensure_billing_enabled
  before_action :login_required
  before_action :sudo_filter
  before_action :require_admin_frontend
  before_action :biztools_only
  skip_before_action :cap_pagination
  skip_before_action :perform_conditional_access_checks

  layout "biztools"
  javascript_bundle "biztools"

  private

  # Find the current user from the route
  #
  # user - The login name of the user in question
  def current_account
    return @account if @account

    login = params[:user_id]
    @account = User.where(type: ["User", "Organization"]).find_by_login(login)
  end
  helper_method :current_account

  # Lookup the account, 404 out if we can't find it
  def ensure_user_exists
    render_404 unless current_account
  end
end
