# frozen_string_literal: true
class ReminderSlackWorkspacesController < ApplicationController
  areas_of_responsibility :ce_extensibility

  before_action :login_required
  before_action :check_feature_flag
  before_action :organization

  # This is necessary to send queries to a writable MySQL server since this is a GET route
  around_action :select_write_database, only: :callback

  def authorize
    referring_path = if params[:personal]
      personal_reminders_path
    else
      org_reminders_path(organization)
    end

    connector = ReminderSlackWorkspaceConnector.new(user: current_user, organization: organization)
    authorization_url = connector.authorization_url(
      referring_path: referring_path,
      redirect_url: callback_reminder_slack_workspace_url(organization),
    )

    GitHub.dogstats.increment("reminders.slack_authorize", tags: dogstats_request_tags + ["success:true"])
    redirect_to(authorization_url)
  end

  def callback
    workspace_jwt = params[:workspace].to_s

    if workspace_jwt.blank?
      render_404
      return
    end

    connector = ReminderSlackWorkspaceConnector.new(user: current_user, organization: organization)

    begin
      connector.connect(workspace_jwt)
      GitHub.dogstats.increment("reminders.slack_callback", tags: dogstats_request_tags + ["success:true"])
      flash[:notice] = "Connected Slack workspace #{connector.workspace.name}"
    rescue ReminderSlackWorkspaceConnector::Error => e
      GitHub.dogstats.increment("reminders.slack_callback", tags: dogstats_request_tags + ["error:#{e.code}", "success:false"])

      flash[:error] =
        if e.is_a?(ReminderSlackWorkspaceConnector::InsufficientPermissionsError)
          "Sorry, only organization owners can connect new Slack workspaces"
        elsif e.is_a?(ReminderSlackWorkspaceConnector::SlackIntegrationError)
          GlobalInstrumenter.instrument("user.reminders.integration_error",
            actor: current_user,
            error: e.message,
            organization: organization,
          )
          e.message
        else
          "We had trouble verifying your Slack authentication. Please try again."
        end
    end

    safe_redirect_to(connector.referring_path || org_reminders_path(organization))
  end

  private

  def target_for_conditional_access
    return :no_target_for_conditional_access unless logged_in?
    organization
  end

  def organization
    return unless logged_in?
    @organization ||= current_user.organizations.find_by_login!(params[:organization_id])
  end

  def check_feature_flag
    if !GitHub.flipper[:scheduled_reminders_frontend].enabled?(organization)
      render_404
    end
  end
end
