# frozen_string_literal: true

class EnterpriseInstallationsController < ApplicationController
  include EnterpriseInstallationsHelper
  include EnterpriseAccountStatsHelper

  skip_before_action :perform_conditional_access_checks, only: %w(new)
  before_action :dotcom_required
  before_action :login_required
  before_action :installation_owner_admin_required, except: %w(new)
  before_action :require_valid_installation_token, only: %w(new)
  before_action :require_valid_state, only: %w(new complete upgrade)
  before_action :require_valid_hostname_in_data, only: %w(new)
  before_action :require_owner_integration_installation, only: %w(complete upgrade)
  around_action :register_enterprise_account_controller_stats

  def new
    SecureHeaders.append_content_security_policy_directives(request, {
      form_action: ["#{protocol(installations_data["http_only"])}://#{installations_data["host_name"]}"],
      preserve_schemes: protocol(installations_data["http_only"]) != "https",
    })

    businesses = current_user.businesses(membership_type: :admin).order("businesses.name ASC")
    owned_organizations = current_user.owned_organizations
    enterprise_owned_orgs = Organization.none
    if enable_business_connections?
      owned_organizations = owned_organizations
        .joins("LEFT OUTER JOIN business_organization_memberships ON business_organization_memberships.organization_id = users.id")
        .where("business_organization_memberships.id IS NULL")

      enterprise_owned_orgs = current_user.owned_organizations
        .joins("LEFT OUTER JOIN business_organization_memberships ON business_organization_memberships.organization_id = users.id")
        .where("business_organization_memberships.id IS NOT NULL")
    end

    render "enterprise_installations/new", locals: {
      businesses: businesses,
      owned_organizations: owned_organizations,
      enterprise_owned_orgs: enterprise_owned_orgs,
    }
  end

  def complete
    token = nil
    ActiveRecord::Base.connected_to(role: :writing) {
      token = current_integration_installation.generate_token
      GitHub.kv.del("ghe-install-token-#{token_hash}")
    }

    # this is a hardcoded url because cloud doesn't know the enterprise account
    # slug on the server instance to formulate a full enterprise route.
    # this route will redirect to the global enterprise account on the server
    redirect_to "#{protocol(installation.http_only)}://#{installation.host_name}/admin/dotcom_connection/complete/#{token}?app_id=#{installation.github_app.id}&installation_id=#{current_integration_installation.id}&state=#{params[:state]}"
  end

  def destroy
    installation.destroy

    # Let other pages redirect back when needed (for example, the billing
    # settings page which lists installations too).
    # If the owner still has other installations, it's worth showing them;
    # otherwise, we'd rather go up one level (to its profile).
    if params[:redirect_to_path].present?
      safe_redirect_to params[:redirect_to_path]
    elsif installation.owner.enterprise_installations.any?
      redirect_to installation_owner_installations_path
    else
      redirect_to installation_owner_profile_path
    end
  end

  def upgrade
    SecureHeaders.append_content_security_policy_directives(request, { form_action: ["#{protocol(installation.http_only)}://#{installation.host_name}"], preserve_schemes: installation.http_only })

    result = ActiveRecord::Base.connected_to(role: :writing) do
      EnterpriseInstallation::FeatureUpdater.perform(installation, actor: current_user)
    end

    return_to = installation_server_return_to
    # for legacy cases where a `return_to` parameter isn't supplied
    return_to ||= Addressable::URI.new(
      scheme: protocol(installation.http_only),
      host: installation.host_name,
      path: "/admin/dotcom_connection/change_complete")

    query = {}
    query[:state] = params[:state] if result.success?
    query[:error] = result.error if result.error
    return_to.query_values = query

    redirect_to return_to.to_s
  end

  private

  def target_for_conditional_access
    return :no_target_for_conditional_access unless installation
    installation.owner
  end

  def installation_owner_admin_required
    return if installation.owner.adminable_by?(current_user)

    return_to = installation_server_return_to
    return render_404 unless return_to

    return_to.query_values = { error: "User is not an admin on the connected cloud account" }
    redirect_to return_to.to_s
  end

  def installation
    @installation ||= EnterpriseInstallation.find(params[:id])
  end
  # for EnterpriseInstallationsHelper consumption:
  alias_method :current_installation, :installation

  def installation_owner_installations_path
    if installation.owner.is_a?(Organization)
      organization_enterprise_installations_list_path(installation.owner)
    else
      enterprise_enterprise_installations_path(installation.owner)
    end
  end

  def installation_owner_profile_path
    if installation.owner.is_a?(Organization)
      settings_org_profile_path(installation.owner)
    else
      settings_profile_enterprise_path(installation.owner)
    end
  end

  def require_owner_integration_installation
    return if current_integration_installation

    return_to = installation_server_return_to
    return render_404 unless return_to

    return_to.query_values = { error: "An error was found with your cloud connection.  Please reconnect your server to your cloud account." }
    redirect_to return_to.to_s
  end
end
