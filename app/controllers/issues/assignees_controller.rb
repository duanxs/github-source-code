# frozen_string_literal: true

class Issues::AssigneesController < IssuesController

  before_action :login_required
  before_action :issue_required
  before_action :require_xhr, only: :unassign_self

  skip_before_action :issue_modifiers_only, only: [:update, :unassign_self]
  skip_before_action :writable_repository_required, only: [:update, :unassign_self]

  ReplaceAssigneesMutation = parse_query <<~'GRAPHQL'
    mutation($input: ReplaceAssigneesForAssignableInput!) {
      replaceAssigneesForAssignable(input: $input)
    }
  GRAPHQL

  def update
    assignees = User.where(id: params[:issue][:user_assignee_ids].reject(&:empty?)).map(&:global_relay_id)

    input = {
      assignableId: current_issue.global_relay_id,
      assigneeIds: assignees,
    }

    data = platform_execute(ReplaceAssigneesMutation, variables: { input: input })
    message = data.errors.messages.values.join(", ") if data.errors.any?

    track_issue_edits_from_project_board(edited_fields: ["assignees"]) unless message

    if request.xhr?
      unless message
        respond_to do |format|
          format.html do
            render partial: "issues/sidebar/show/assignees", locals: { issue: current_issue }
          end
        end
      else
        render json: { errors: message }, status: :unprocessable_entity
      end
    else
      flash[:error] = message if message
      redirect_to :back
    end
  end

  def unassign_self
    current_issue.remove_assignees(current_user)

    if current_issue.save
      respond_to do |format|
        format.html do
          render partial: "issues/sidebar/show/assignees", locals: { issue: current_issue }
        end
      end
    else
      render json: { errors: current_issue.errors }, status: :unprocessable_entity
    end
  end
end
