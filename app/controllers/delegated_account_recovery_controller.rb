# frozen_string_literal: true

class DelegatedAccountRecoveryController < ApplicationController
  class DelegatedRecoveryStateError < StandardError; end

  CONFIG_CACHE_TIME = 1.hour
  RECOVER_ACCOUNT_RETURN_REQUEST_FORMAT = "application/x-www-form-urlencoded"
  FACEBOOK = "https://www.facebook.com"
  GITHUB_DEV = "http://github.localhost"

  # Since the spec does not depend on the encrypted data matching and using earthsmoke introduces
  # the case where decryption is more likely to fail, we consider signature_only_success
  # as a successful recovery.
  RECOVERY_SUCCESS_STATES = [
    # legacy_success occurs when tokens are
    :legacy_success, # verified by secrets stored in tweaker

    # success occurs when tokens are verified by secrets stored in earthsmoke
    :success,

    # The signatures on the token were valid but the secret inside the token
    # could not be recovered because earthsmoke is unavailable. We consider this
    # a success state because the signatures are the proof and the secret is
    # just a double check. Signature validation does not depend on earthsmoke.
    :signature_only_success,
  ]

  UNAUTHED_ENDPOINTS = [
    :well_known_config,
    :recover_account_return,
  ]

  include GitHub::RateLimitedRequest
  rate_limit_requests \
    only: [:create, :recover_account_return],
    max: 10,
    ttl: 1.hour,
    key: :delegated_account_recovery_cache_key

  # None of these actions require conditional access checks because they don't
  # involve organizations.
  skip_before_action :perform_conditional_access_checks, only: %w(
    well_known_config
    index
    create
    destroy
    save_token_return
    recover_account_return
  )

  skip_before_action :verify_authenticity_token, only: UNAUTHED_ENDPOINTS + [:save_token_return]

  before_action :sudo_filter, except: UNAUTHED_ENDPOINTS
  before_action :require_xhr, only: :create
  before_action :login_required, except: %w(well_known_config recover_account_return)

  def well_known_config
    headers["Cache-Control"] = "max-age=#{CONFIG_CACHE_TIME}"

    respond_to do |format|
      format.any do
        config = if GitHub.github_as_recovery_provider_enabled?
          Darrrr.account_and_recovery_provider_config
        else
          Darrrr.account_provider_config
        end

        render json: config
      end
    end
  end

  ProviderTokensQuery = parse_query <<-'GRAPHQL'
    query($provider: String!) {
      viewer {
        delegatedRecoveryTokens(first: 100, providers: [$provider]) {
          ...Views::DelegatedAccountRecovery::Index::Tokens
        }
      }
    }
  GRAPHQL

  def index
    begin
      recovery_provider = Darrrr.recovery_provider(params[:provider_origin])
    rescue Darrrr::UnknownProviderError
      return render_404
    end

    data = platform_execute(ProviderTokensQuery,  variables: {
      provider: recovery_provider.issuer,
    })

    SecureHeaders.append_content_security_policy_directives(request, form_action: [recovery_provider.origin])

    render "delegated_account_recovery/index", locals: {
      provider: recovery_provider.issuer,
      endpoint: recovery_provider.save_token,
      tokens_connection: data.viewer.delegated_recovery_tokens,
    }
  end

  AddRecoveryTokenQuery = parse_query <<-'GRAPHQL'
    mutation($provider: RecoveryProvider!) {
      addToken: addRecoveryToken(input: { provider: $provider }) {
        tokenStateUrl
        sealedToken
      }
    }
  GRAPHQL

  def create
    respond_to do |format|
      format.json do
        begin
          recovery_provider = Darrrr.recovery_provider(params[:provider_origin])
        rescue Darrrr::UnknownProviderError
          return head :not_found
        end

        data = platform_execute(AddRecoveryTokenQuery, variables: {
          provider: Platform::Enums::RecoveryProvider.coerce_isolated_result(recovery_provider.origin),
        })

        if data.errors.all.any?
          render json: { success: false }
          return
        end

        state_url = data.add_token.token_state_url.to_s
        session[:delegated_account_recovery_csrf_token] = state_url # acts as CSRF protection

        render json: {
          success: true,
          token: data.add_token.sealed_token,
          state_url: state_url,
        }
      end
    end
  end

  DeleteRecoveryTokenQuery = parse_query <<-'GRAPHQL'
    mutation($id: ID!) {
      deleteToken: deleteRecoveryToken(input: { id: $id }) {
        isDestroyed
      }
    }
  GRAPHQL

  def destroy
    data = platform_execute(DeleteRecoveryTokenQuery, variables: {
      id: params[:id],
    })

    if data.errors.all.empty? && data.delete_token.is_destroyed
      flash[:notice] = "Recovery token revoked."
    else
      flash[:error] = "Could not revoke token."
    end
    redirect_to settings_user_security_path
  end

  ConfirmRecoveryTokenQuery = parse_query <<-'GRAPHQL'
    mutation($tokenId: String!) {
      confirmToken: confirmRecoveryToken(input: { tokenId: $tokenId })
    }
  GRAPHQL

  def save_token_return
    if params[:state] != session[:delegated_account_recovery_csrf_token] || session[:delegated_account_recovery_csrf_token].nil?
      raise ActionController::InvalidAuthenticityToken
    end

    if GitHub.github_as_recovery_provider_enabled?
      session.delete(:delegated_account_recovery_csrf_token)
    end

    case params[:status]
    when "save-success"
      GitHub.dogstats.increment("delegated_account_recovery_token", tags: ["save_token_return:success"])
      # this value is safe because without its presence, the CSRF check at the beginning of this
      # action will fail without it. Also, we generate the session value ourselves.
      begin
        token_id = Addressable::URI.parse(params[:state]).query_values["id"]
      rescue Addressable::URI::InvalidURIError
        flash[:error] = "Unable to authenticate request"
        redirect_to settings_user_security_path
      end

      ActiveRecord::Base.connected_to(role: :writing) do
        data = platform_execute(ConfirmRecoveryTokenQuery, variables: {
          tokenId: token_id,
        })

        if data.errors.all.any?
          flash[:error] = "Unable to confirm recovery token"
        else
          flash[:notice] = "Recovery token successfully saved."
        end
      end
    when "save-failure"
      GitHub.dogstats.increment("delegated_account_recovery_token", tags: ["save_token_return:failure"])
      flash[:error] = "Recovery token could not be saved."
    else
      GitHub.dogstats.increment("delegated_account_recovery_token", tags: ["save_token_return:unknown"])
      flash[:error] = "We received an unexpected response when saving your token."
    end

    redirect_to settings_user_security_path
  end

  VerifyRecoveryToken = parse_query <<-'GRAPHQL'
    mutation($countersignedToken: String!) {
      verifyToken: verifyRecoveryToken(input: { countersignedToken: $countersignedToken }) {
        validationResult
        errorMessage
      }
    }
  GRAPHQL
  def recover_account_return
    unless request.content_type == RECOVER_ACCOUNT_RETURN_REQUEST_FORMAT
      GitHub.dogstats.increment("delegated_account_recovery_token", tags: ["recover_account_return:bad_request_format"])
      render status: 400, plain: "Invalid request format"
      return
    end

    respond_to do |format|
      format.html do
        data = platform_execute(VerifyRecoveryToken, variables: {
          countersignedToken: params[:token],
        })

        if data.errors.all.any?
          render_404
          return
        end

        validation_result = data.verify_token.validation_result
        case Platform::Enums::RecoveryTokenValidationResult.coerce_isolated_input(validation_result)
        when *RECOVERY_SUCCESS_STATES
          anonymous_flash[:notice] = "Almost there! The recovery token submitted from Facebook has been verified. Next, contact GitHub Support to let them know your token is ready for review."
        when :unverifiable_token
          anonymous_flash[:error] = data.verify_token.error_message
        when :secret_user_id_mismatch, :earthsmoke_secret_user_id_mismatch
          anonymous_flash[:error] = "We could not validate the recovery token belongs to you"
        when :no_confirmed_tokens_matched
          anonymous_flash[:error] = "We could not find a corresponding valid token"
        when :earthsmoke_disabled, :bad_legacy_decode, :earthsmoke_server_error
          anonymous_flash[:error] = "We are temporarily unable to verify your token"
        else
          raise RuntimeError, "Unknown result: #{validation_result}"
        end

        redirect_to home_url
      end
      format.any do
        render status: 400, plain: "Invalid request format"
      end
    end
  end

  private def delegated_account_recovery_cache_key
    key = if params[:token]
      begin
        unverified_token = Darrrr.this_account_provider.dangerous_unverified_recovery_token(params[:token])
        unverified_token.token_id.to_hex
      rescue Darrrr::RecoveryTokenSerializationError
        # can be unauthenticated garbage
      end
    end

    key ||= current_user.try(:id) || request.remote_ip
    "delegated_account_recovery:#{key}"
  end
end
