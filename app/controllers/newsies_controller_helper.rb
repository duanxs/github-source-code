# frozen_string_literal: true

# Defines controller helper methods related to displaying Notifications.
module NewsiesControllerHelper
  def self.included(base)
    return unless base.respond_to?(:helper_method)
    base.helper_method :mark_thread_as_read
  end

  private

  # Public: Marks the thread's notification as 'read' for the current user.
  #
  # thread - A thread (either a Commit or an Issue).
  #
  # Returns nothing.
  def mark_thread_as_read(thread)
    return unless logged_in?

    response = GitHub.newsies.web.notification(current_user, thread.notifications_list, thread)

    if response.success? && response.value&.unread?
      ActiveRecord::Base.connected_to(role: :writing) do
        GitHub.newsies.web.mark_thread_read(current_user, thread)
      end
    end
  end
end
