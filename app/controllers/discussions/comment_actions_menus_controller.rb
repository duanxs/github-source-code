# frozen_string_literal: true

class Discussions::CommentActionsMenusController < Discussions::BaseController
  areas_of_responsibility :discussions

  before_action :require_discussion
  before_action :require_readable_discussion_comment

  def show
    if request.xhr?
      render partial: "discussions/comment_actions", locals: {
        timeline: timeline,
        discussion_or_comment: comment,
        form_path: form_path,
        permalink_id: permalink_id,
        is_comment_minimized: false,
      }
    else
      render_404
    end
  end

  private

  def timeline
    render_context = DiscussionTimeline::SingleCommentRenderContext.new(
      discussion,
      comment,
      viewer: current_user,
    )
    DiscussionTimeline.new(render_context: render_context)
  end

  def comment
    discussion.comments.find(params[:comment_id])
  end

  def form_path
    params[:form_path]
  end

  def permalink_id
    params[:permalink_id]
  end

  def require_readable_discussion_comment
    render_404 unless comment&.readable_by?(current_user)
  end
end
