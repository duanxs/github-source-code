# frozen_string_literal: true

class Discussions::PermalinksController < Discussions::BaseController
  include DiscussionsHelper

  before_action :require_discussion

  def show
    permalink_comment_id = extract_permalink_comment_id

    if permalink_comment_id.present?
      render partial: "discussions/collapsible_timeline",
        locals: { timeline: discussion_timeline }
    else
      head 404
    end
  end

  private

  def discussion_timeline
    @discussion_timeline ||= begin
      render_context = DiscussionTimeline::PermalinkRenderContext.new(
        discussion,
        viewer: current_user,
        before_cursor: params[:before],
        after_cursor: params[:after],
        permalink_comment_id: extract_permalink_comment_id,
      )

      DiscussionTimeline.new(render_context: render_context)
    end
  end

  def extract_permalink_comment_id
    permalink_id = params[:anchor]&.match(/\Adiscussioncomment-([0-9]+)\z/i)&.captures&.first
    return unless permalink_id

    permalink_id.to_i
  end
end
