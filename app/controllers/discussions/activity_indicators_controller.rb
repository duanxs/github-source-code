# frozen_string_literal: true

class Discussions::ActivityIndicatorsController < Discussions::BaseController
  before_action :require_activity_indicator_feature

  def show
    render "discussions/activity_indicators/show", layout: false, locals: {
      has_unread: current_repository.has_unread_discussions?(current_user)
    }
  end

  private

  def require_activity_indicator_feature
    render_404 unless current_repository.discussions_activity_indicator_enabled?
  end
end
