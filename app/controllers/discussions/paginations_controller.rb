# frozen_string_literal: true

class Discussions::PaginationsController < Discussions::BaseController
  include DiscussionsHelper

  before_action :require_discussion

  def show
    render partial: "discussions/collapsible_timeline",
      locals: { timeline: discussion_timeline }
  end

  private

  def discussion_timeline
    @discussion_timeline ||= begin
      render_context = DiscussionTimeline::PaginatedRenderContext.new(
        discussion,
        viewer: current_user,
        before_cursor: params[:before],
        after_cursor: params[:after],
      )

      DiscussionTimeline.new(render_context: render_context)
    end
  end
end
