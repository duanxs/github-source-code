# frozen_string_literal: true

class Discussions::SpotlightPreviewsController < Discussions::BaseController
  before_action :login_required
  before_action :require_discussion
  before_action :require_spotlights_feature
  layout false

  def show
    spotlight = DiscussionSpotlight.new(spotlight_params)
    spotlight.discussion = discussion

    render partial: "discussions/spotlight_previews/show", locals: {
      discussion: discussion,
      spotlight: spotlight,
    }
  end

  private

  def spotlight_params
    params.require(:discussion_spotlight).
      permit(:emoji, :preconfigured_color, :pattern)
  end
end
