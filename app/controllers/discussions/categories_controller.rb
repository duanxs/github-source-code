# frozen_string_literal: true

class Discussions::CategoriesController < Discussions::BaseController
  before_action :require_category_feature_flags

  def index
    render "discussion_categories/index",
      locals: {
        discussion_categories: current_repository.discussion_categories.sort
      }
  end

  private

  def require_category_feature_flags
    render_404 unless discussion_categories_enabled? && discussion_categories_edit_enabled?
  end
end
