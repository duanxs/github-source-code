# frozen_string_literal: true

class Marketplace::CategoriesController < ApplicationController
  before_action :marketplace_required

  rescue_from PlatformHelper::InvalidCursorError, with: :render_404

  layout "marketplace"

  include Marketplace::VerificationState

  ShowQuery = parse_query <<-'GRAPHQL'
    query($loggedIn: Boolean!, $searchQuery: String!, $searchType: MarketplaceSearchType!, $verificationState: MarketplaceVerificationState, $categorySlug: String!, $withFreeTrialsOnly: Boolean, $shortDescriptionMax: Int!, $first: Int, $after: String, $last: Int, $before: String) {
      marketplaceCategory(slug: $categorySlug) {
        slug
      }

      ...Views::Marketplace::Searches::Show::Root
    }
  GRAPHQL

  # nb: this endpoint renders the same content as /marketplace?category=:slug
  # we maintain this endpoint for SEO reasons
  def show
    search_query = ""
    category_slug = params[:slug]
    normalized_search_type = Marketplace::SearchOptions.find_normalized_type(params[:type])
    normalized_verification_state = Marketplace::SearchOptions.find_normalized_verification_state(params[:verification])

    variables = {
      searchQuery: search_query,
      withFreeTrialsOnly: nil,
      searchType: graphql_search_type_for(normalized_search_type),
      verificationState: graphql_verification_state_for(normalized_verification_state),
      categorySlug: category_slug,
      loggedIn: logged_in?,
      shortDescriptionMax: Marketplace::Listing::SHORT_DESCRIPTION_MAX_LENGTH,
    }

    variables.merge!(graphql_pagination_params(page_size: Marketplace::SearchesController::RESULTS_PER_PAGE))

    data = platform_execute(ShowQuery, variables: variables)

    search_options = Marketplace::SearchOptions.new(
      category_slug: data.marketplace_category&.slug,
      query: search_query.presence,
      tool_type: normalized_search_type,
      verification_state: graphql_verification_state_for(normalized_verification_state),
    )

    render "marketplace/searches/show", locals: {
      data: data,
      results_per_page: Marketplace::SearchesController::RESULTS_PER_PAGE,
      search_options: search_options,
    }
  end

  private

  def require_conditional_access_checks?
    false
  end

  def graphql_search_type_for(type)
    case type
    when /\Aapps\z/
      "MARKETPLACE"
    when /\Aactions\z/i
      "MARKETPLACE_ACTIONS"
    else
      "MARKETPLACE_TOOLS"
    end
  end
end
