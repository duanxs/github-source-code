# frozen_string_literal: true

class Marketplace::SearchesController < ApplicationController
  statsd_tag_actions only: :show

  areas_of_responsibility :marketplace

  before_action :marketplace_required

  layout "marketplace"

  include Marketplace::VerificationState

  rescue_from PlatformHelper::InvalidCursorError, with: :rescue_invalid_cursor

  ShowQuery = parse_query <<-'GRAPHQL'
    query($loggedIn: Boolean!, $searchQuery: String!, $searchType: MarketplaceSearchType!, $verificationState: MarketplaceVerificationState, $categorySlug: String!, $withFreeTrialsOnly: Boolean, $shortDescriptionMax: Int!, $first: Int, $after: String, $last: Int, $before: String) {
      marketplaceCategory(slug: $categorySlug) {
        slug
      }

      ...Views::Marketplace::Searches::Show::Root
    }
  GRAPHQL

  RESULTS_PER_PAGE = 20

  def show
    search_query = params[:query] || ""
    normalized_search_type = Marketplace::SearchOptions.find_normalized_type(params[:type])
    normalized_verification_state = Marketplace::SearchOptions.find_normalized_verification_state(params[:verification])
    category_slug = params[:category] || ""
    with_free_trials_only = ActiveModel::Type::Boolean.new.cast(params[:with_free_trials_only])

    variables = {
      searchQuery: search_query.to_s,
      withFreeTrialsOnly: with_free_trials_only,
      searchType: graphql_search_type_for(normalized_search_type),
      verificationState: graphql_verification_state_for(normalized_verification_state),
      categorySlug: category_slug.to_s,
      loggedIn: logged_in?,
      shortDescriptionMax: Marketplace::Listing::SHORT_DESCRIPTION_MAX_LENGTH,
    }

    variables.merge!(graphql_pagination_params(page_size: RESULTS_PER_PAGE))

    data = platform_execute(ShowQuery, variables: variables)

    search_options = Marketplace::SearchOptions.new(
      category_slug: data.marketplace_category&.slug,
      query: search_query.presence,
      tool_type: normalized_search_type,
      verification_state: normalized_verification_state,
    )

    render "marketplace/searches/show", locals: {
      data: data,
      results_per_page: RESULTS_PER_PAGE,
      search_options: search_options,
    }
  end

  private

  def graphql_search_type_for(type)
    case type
    when /\Aapps\z/
      "MARKETPLACE"
    when /\Aactions\z/i
      "MARKETPLACE_ACTIONS"
    else
      "MARKETPLACE_TOOLS"
    end
  end

  def require_conditional_access_checks?
    false
  end

  def rescue_invalid_cursor
    flash[:error] = "Invalid search query supplied."
    redirect_to marketplace_search_url(type: :apps)
  end
end
