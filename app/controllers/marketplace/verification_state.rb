# frozen_string_literal: true

module Marketplace::VerificationState
  require_dependency "application_controller/feature_flags_dependency"

  def graphql_verification_state_for(state)
    case state
    when /\Averified\z/i
      "VERIFIED"
    when /\Aunverified\z/i
      "UNVERIFIED"
    end
  end
end
