# frozen_string_literal: true

class Marketplace::PendingInstallations::DismissalsController < ApplicationController
  areas_of_responsibility :marketplace

  before_action :login_required
  before_action :marketplace_required
  before_action :marketplace_pending_installations_required

  layout "marketplace"

  def create
    Marketplace::PendingInstallations::Notice.new(user_id: current_user.id).dismiss

    if request.xhr?
      head :ok
    else
      redirect_to :back
    end
  end

  private

  def require_conditional_access_checks?
    false
  end
end
