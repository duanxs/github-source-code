# frozen_string_literal: true

class Marketplace::Actions::UsableIconsController < ApplicationController
  before_action :login_required

  layout false

  MAX_RESULTS = 50

  def index
    if params[:q]
      icons = RepositoryActions::Icons::NAMES.grep(/#{Regexp.escape(params[:q])}/).first(MAX_RESULTS)
    else
      icons = RepositoryActions::Icons::NAMES.first(MAX_RESULTS)
    end

    render "marketplace/actions/usable_icons/index", formats: :html, locals: { icons: icons }
  end

  private

  def require_conditional_access_checks?
    false
  end
end
