# frozen_string_literal: true

class Marketplace::PendingInstallationsController < ApplicationController
  areas_of_responsibility :marketplace

  before_action :login_required
  before_action :marketplace_required
  before_action :marketplace_pending_installations_required

  layout "marketplace"

  IndexQuery = parse_query <<-'GRAPHQL'
    query($loggedIn: Boolean!) {
      ...Views::Marketplace::PendingInstallations::Index::Root
    }
  GRAPHQL

  def index
    data = platform_execute(IndexQuery, variables: { loggedIn: logged_in? })

    render "marketplace/pending_installations/index", locals: { data: data }
  end

  private

  def require_conditional_access_checks?
    false
  end
end
