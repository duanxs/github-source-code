# frozen_string_literal: true

class Marketplace::Listings::FeaturedCustomersController < ApplicationController
  areas_of_responsibility :marketplace

  layout "marketplace"

  before_action :marketplace_required
  before_action :login_required

  IndexQuery = parse_query <<-'GRAPHQL'
    query($listingSlug: String!) {
      marketplaceListing(slug: $listingSlug) {
        isVerified
        viewerCanEdit
      }
      ...Views::MarketplaceListingFeaturedCustomers::Index::Root
    }
  GRAPHQL

  def index
    data = platform_execute(IndexQuery, variables: {
      listingSlug: params[:listing_slug],
    })

    return render_404 unless data.marketplace_listing&.viewer_can_edit? && data.marketplace_listing&.is_verified?

    render "marketplace_listing_featured_customers/index", locals: { data: data }
  end

  def update
    listing = Marketplace::Listing.find_by(slug: params[:listing_slug])

    return render_404 unless listing && listing.allowed_to_edit?(current_user) && listing&.verified?

    existing_org_logins = Organization.where(id: listing.featured_organizations.pluck(:organization_id)).pluck(:login)
    new_org_logins = params[:marketplace_listing][:featured_organization_login].compact - existing_org_logins

    organizations = Organization.where(login: new_org_logins)
    added_orgs = listing.featured_organizations.create(organizations.map { |org| { organization: org } })

    if added_orgs.any? && listing.publicly_listed?
      MarketplaceMailer.featured_customers_need_review(listing: listing).deliver_later
    end

    redirect_to edit_featured_customers_marketplace_listing_path(listing.slug)
  end

  def destroy
    listing = Marketplace::Listing.find_by(slug: params[:listing_slug])
    return render_404 unless listing && listing.allowed_to_edit?(current_user) && listing&.verified?

    customer = listing.featured_organizations.find(params[:id])
    return render_404 unless customer

    customer.destroy
    flash[:notice] = "Customer removed successfully."

    redirect_to edit_featured_customers_marketplace_listing_path(customer.listing.slug)
  end

  private

  def target_for_conditional_access
    listing = Marketplace::Listing.find_by(slug: params[:listing_slug])
    return :no_target_for_conditional_access unless listing
    listing.owner
  end
end
