# frozen_string_literal: true

class ContentReferenceAttachmentsController < ApplicationController
  areas_of_responsibility :ce_extensibility

  before_action :login_required

  def hide
    return render_404 unless current_attachment.content.async_editable_by?(current_user).sync

    if current_attachment.hidden?
      return head :no_content
    else
      current_attachment.hidden!
      current_attachment.content.async_body_html_refresh.sync

      return head :no_content
    end
  end

  private

  def target_for_conditional_access
    :no_target_for_conditional_access
  end

  def current_attachment
    @current_attachment ||= ContentReferenceAttachment.find(params[:id])
  end
end
