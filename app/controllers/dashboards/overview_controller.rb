# frozen_string_literal: true

class Dashboards::OverviewController < ApplicationController
  skip_before_action :perform_conditional_access_checks

  before_action :enterprise_required

  rescue_from NameError, with: :render_404

  def index
    graphs = [
      { heading: "Pull Requests", metrics: [MetricQuery::PullRequestsCreated, MetricQuery::PullRequestsMerged] },
      { heading: "Issues", metrics: [MetricQuery::IssuesCreated, MetricQuery::IssuesClosed] },
      { heading: "Issue Comments", metrics: [MetricQuery::IssueCommentsCreated] },
      { heading: "Repositories", metrics: [MetricQuery::RepositoriesCreated] },
      { heading: "Users", metrics: [MetricQuery::UsersCreated] },
      { heading: "Organizations", metrics: [MetricQuery::OrganizationsCreated] },
      { heading: "Teams", metrics: [MetricQuery::TeamsCreated] },
    ]

    render "dashboards/overview/index", locals: {
      graphs: graphs,
      period: period,
    }
  end

  def count
    render json: MetricSet.build(params[:metrics], timespan: timespan)
  end

  private

  def period
    @period ||= timespan.period
  end

  def timespan
    @timespan ||= MetricTimespan.for(params.fetch(:period, :week)).new
  end
end
