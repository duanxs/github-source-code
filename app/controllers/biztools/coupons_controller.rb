# frozen_string_literal: true

class Biztools::CouponsController < BiztoolsController
  areas_of_responsibility :stafftools

  before_action :ensure_user_exists, only: [:apply, :revoke]
  before_action \
    :ensure_coupon_exists,
    only: [:show, :destroy, :edit, :update, :report]

  def index
    render "biztools/coupons/index"
  end

  def new
    @coupon = ::Coupon.new
    @coupon.group = params[:group]
    @coupon_type = params[:type] || "value"

    render_new
  end

  def create
    @coupon = ::Coupon.new coupon_params
    if params[:business_plus] == "1"
      @coupon.group = Coupon::SALES_SERVE_GROUP_NAME
      @coupon.plan = GitHub::Plan.business_plus
    end

    if @coupon.save
      flash[:notice] = if @coupon.business_plus_only_coupon?
        "#{business_try_url(code: @coupon.code)} created!"
      else
        "Coupon '#{@coupon.code}' created!"
      end
      if @coupon.group.present?
        redirect_to biztools_coupon_groups_path(@coupon.group)
      else
        redirect_to biztools_coupons_path
      end
    else
      message = if @coupon.errors.any?
        @coupon.errors.full_messages.to_sentence
      else
        "Couldn’t create the coupon - there were some errors."
      end
      flash[:error] = message
      render_new
    end
  end

  def render_new
    render "biztools/coupons/new"
  end

  def destroy
    if @coupon = ::Coupon.find_by_code(params[:id])
      @coupon.destroy
      flash[:notice] = "Coupon destroy!"
    else
      flash[:error] = "Couldn’t find the coupon you want to delete!"
    end

    respond_to do |format|
      format.json { render json: {} }
      format.html { redirect_to biztools_coupons_path }
    end
  end

  def groups
    @coupons  = list_coupons(params[:group], current_page, params[:state], params[:sort])
    @counters = counters(params[:group])
    @state    = params[:state] || "active"
    render "biztools/coupons/groups"
  end

  def search
    if this_coupon
      redirect_to biztools_coupon_path(this_coupon)
    else
      flash[:error] = "Couldn’t find the coupon."
      redirect_to :back
    end
  end

  def show
    state = params[:state] || "active"
    coupon_redemptions = this_coupon
      .coupon_redemptions
      .includes(:user)
      .where(expired: (state == "expired"))
      .paginate(page: current_page)

    render "biztools/coupons/show", locals: {
      view: Biztools::Coupon::ShowView.new(this_coupon, state: state),
      coupon: this_coupon,
      coupon_redemptions: coupon_redemptions,
    }
  end

  def edit
    render "biztools/coupons/edit"
  end

  def update
    if this_coupon.update(coupon_params)
      redirect_to biztools_coupon_path(this_coupon)
    else
      flash.now[:error] = this_coupon.errors.full_messages.to_sentence
      edit
    end
  end

  # Applies a coupon directly onto a user
  def apply
    current_account.update_attribute(:billing_type, "card") if current_account.gift?

    redemption = current_account.redeem_coupon(params[:code],
      allow_reuse: true, actor: current_user)
    if redemption
      flash[:notice] = "Coupon '#{params[:code]}' applied"
    else
      errors = current_account.errors.full_messages.to_sentence.dup
      if current_account.coupon_redemptions.active.any?
        errors << " That account already has a coupon applied: #{biztools_coupon_url(current_account.coupon_redemptions.active.first.coupon)}"
      end
      flash[:error] = errors
    end
    redirect_to :back
  end

  # Removes a user's active coupon
  def revoke
    current_account.expire_active_coupon
    redirect_to :back
  end

  def report
    filename = "coupon-redemptions-#{this_coupon}-#{Date.today}.csv"
    send_data \
      Biztools::CouponReport.new(this_coupon).generate,
      type: "text/csv",
      disposition: "attachment;filename=#{filename}"
  end

  def opensource
    @user = User.find_by_name params[:user_id]
    return render_404 if @user.nil?

    @popular_repository = @user.repositories
                               .where(public: true)
                               .where("pushed_at > #{1.year.ago.to_i}")
                               .joins(:repository_license)
                               .find do |repo|
      next true if repo.stargazer_count > 100
      next true if repo.public_fork_count > 100
      repo.watchers.count > 100
    end

    @public_repos_count = @user.repositories.where(public: true).count
    @private_repos_count = @user.repositories.where(public: false).count

    @public_repos_committers = 0
    @public_repos_commits = 0
    @private_repos_committers = 0
    @private_repos_commits = 0

    @user.repositories.each do |repo|
      activity_summary = repo.activity_summary(viewer: repo.owner)
      if repo.public
        @public_repos_committers += activity_summary.committers.count
        @public_repos_commits += activity_summary.commit_count
      else
        @private_repos_committers += activity_summary.committers.count
        @private_repos_commits += activity_summary.commit_count
      end
    end

    render "biztools/coupons/opensource"
  end

  private

  def this_coupon
    @coupon ||= ::Coupon.find_by_code(params[:id])
  end
  helper_method :this_coupon

  def ensure_coupon_exists
    return render_404 if this_coupon.nil?
  end

  def list_coupons(group, page, state = :active, order = :active_first)
    if (order.try(:to_sym) == :alphabetical)
      order_condition = "code ASC"
    else
      order_condition = "expires_at DESC"
    end

    conditions = build_conditions(group, state || :active)
    ::Coupon.order(order_condition).where(conditions).page(page)
  end

  def counters(group)
    {
      active: ::Coupon.where(build_conditions(group, :active)).count,
      expired: ::Coupon.where(build_conditions(group, :expired)).count,
    }
  end

  def build_conditions(group, state)
    if state.to_sym == :expired
      query = {"expires_at <= ?" => Time.now}
    else
      query = {"expires_at > ?" => Time.now}
    end

    if group
      query["`group` = ?"] = params[:group]
    else
      query["`group` = ''"] = nil
    end

    [query.keys.join(" AND "), *query.values.compact]
  end

  def coupon_params
    params.require(:coupon).permit(*exposed_attributes)
  end

  def exposed_attributes
    [:code, :plan, :discount, :duration, :limit, :group,
      :expires_at, :note, :staff_actor_only]
  end
end
