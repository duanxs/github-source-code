# frozen_string_literal: true

class Biztools::WorksWithGitHubListingsController < BiztoolsController
  areas_of_responsibility :marketplace

  before_action :marketplace_required

  IndexQuery = parse_query <<-'GRAPHQL'
    query($cursor: String, $query: String, $categorySlug: String, $perPage: Int!, $viewerCanAdmin: Boolean!, $creatorLogin: String!, $filteringByCreator: Boolean!) {
      ...Views::Biztools::WorksWithGitHubListings::Index::Root
    }
  GRAPHQL

  IndexPjaxQuery = parse_query <<-'GRAPHQL'
    query($cursor: String, $query: String, $categorySlug: String, $perPage: Int!, $viewerCanAdmin: Boolean!, $creatorLogin: String) {
      ...Views::Biztools::WorksWithGitHubListings::Listings::Root
    }
  GRAPHQL

  def index
    query = params[:query]
    category_slug = params[:category_slug]
    login = params[:login] || ""
    per_page = 20
    variables = {
      query: query,
      categorySlug: category_slug,
      cursor: params[:cursor],
      perPage: per_page,
      viewerCanAdmin: true,
      creatorLogin: login,
    }
    data = if request.xhr?
      platform_execute(IndexPjaxQuery, variables: variables)
    else
      platform_execute(IndexQuery, variables: variables.merge(filteringByCreator: login.present?))
    end

    respond_to do |format|
      format.html do
        if request.xhr?
          render partial: "biztools/works_with_github_listings/listings", locals: { data: data }
        else
          render "biztools/works_with_github_listings/index",
            locals: { data: data, category_slug: category_slug, query: query }
        end
      end
    end
  end

  ShowQuery = parse_query <<-'GRAPHQL'
    query($id: ID!) {
      node(id: $id) {
        ... on NonMarketplaceListing {
          ...Views::Biztools::WorksWithGitHubListings::Show::NonMarketplaceListing
        }
      }
    }
  GRAPHQL

  def show
    data = platform_execute(ShowQuery, variables: { id: params[:id] })
    listing = data.node

    return render_404 unless listing

    render "biztools/works_with_github_listings/show", locals: { listing: listing }
  end

  EditQuery = parse_query <<-'GRAPHQL'
    query($id: ID!) {
      node(id: $id) {
        ... on NonMarketplaceListing {
          ...Views::Biztools::WorksWithGitHubListings::Edit::NonMarketplaceListing
        }
      }
      ...Views::Biztools::WorksWithGitHubListings::Edit::Root
    }
  GRAPHQL

  def edit
    data = platform_execute(EditQuery, variables: { id: params[:id] })
    listing = data.node

    return render_404 unless listing

    render "biztools/works_with_github_listings/edit", locals: { listing: listing, data: data }
  end

  ApproveQuery = parse_query <<-'GRAPHQL'
    mutation($input: ApproveNonMarketplaceListingInput!) {
      approveNonMarketplaceListing(input: $input) {
        nonMarketplaceListing
      }
    }
  GRAPHQL

  def approve
    input = { id: params[:id], message: params[:message] }
    data = platform_execute(ApproveQuery, variables: { input: input })

    check_for_errors(data, mutation: :approveNonMarketplaceListing)
    return if performed?

    redirect_to biztools_works_with_github_listing_path(params[:id])
  end

  RejectQuery = parse_query <<-'GRAPHQL'
    mutation($input: RejectNonMarketplaceListingInput!) {
      rejectNonMarketplaceListing(input: $input) {
        nonMarketplaceListing
      }
    }
  GRAPHQL

  def reject
    input = { id: params[:id], message: params[:message] }
    data = platform_execute(RejectQuery, variables: { input: input })

    check_for_errors(data, mutation: :rejectNonMarketplaceListing)
    return if performed?

    redirect_to biztools_works_with_github_listing_path(params[:id])
  end

  DelistQuery = parse_query <<-'GRAPHQL'
    mutation($input: DelistNonMarketplaceListingInput!) {
      delistNonMarketplaceListing(input: $input) {
        nonMarketplaceListing
      }
    }
  GRAPHQL

  def delist
    input = { id: params[:id], message: params[:message] }
    data = platform_execute(DelistQuery, variables: { input: input })

    check_for_errors(data, mutation: :delistNonMarketplaceListing)
    return if performed?

    redirect_to biztools_works_with_github_listing_path(params[:id])
  end

  UpdateQuery = parse_query <<-'GRAPHQL'
    mutation($input: UpdateNonMarketplaceListingInput!) {
      updateNonMarketplaceListing(input: $input) {
        nonMarketplaceListing
      }
    }
  GRAPHQL

  def update
    input = non_marketplace_listing_params.merge(id: params[:id])
    input[:enterpriseCompatible] = params[:non_marketplace_listing][:enterpriseCompatible] == "on"

    data = platform_execute(UpdateQuery, variables: { input: input })

    check_for_errors(data, mutation: :updateNonMarketplaceListing)
    return if performed?

    redirect_to biztools_works_with_github_listing_path(params[:id])
  end

  private

  def non_marketplace_listing_params
    params.require(:non_marketplace_listing).permit(:name, :description, :url, :categorySlug, :enterpriseCompatible)
  end

  def check_for_errors(data, mutation:)
    if data.errors.any?
      error_type = data.errors.details[mutation]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      flash[:error] = data.errors.messages.values.join(", ")
    end
  end
end
