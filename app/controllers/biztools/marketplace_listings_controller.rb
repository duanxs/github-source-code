# frozen_string_literal: true

class Biztools::MarketplaceListingsController < BiztoolsController
  areas_of_responsibility :marketplace

  before_action :marketplace_required
  before_action :verify_single_integratable_specified, only: [:update]
  before_action :cast_boolean_params, only: :update

  IndexQuery = parse_query <<-'GRAPHQL'
    query($cursor: String, $query: String, $categorySlug: String, $organizationId: ID!, $adminId: ID!, $filteringByUserAdmin: Boolean!, $filteringByOrgOwner: Boolean!, $perPage: Int!, $allStates: Boolean, $state: MarketplaceListingState, $orderBy: MarketplaceListingOrder!) {
      ...Views::Biztools::MarketplaceListings::Index::Root
    }
  GRAPHQL

  IndexXhrQuery = parse_query <<-'GRAPHQL'
    query($cursor: String, $query: String, $categorySlug: String, $organizationId: ID!, $adminId: ID!, $perPage: Int!, $allStates: Boolean, $state: MarketplaceListingState, $orderBy: MarketplaceListingOrder!) {
      ...Views::Biztools::MarketplaceListings::Listings::Root
    }
  GRAPHQL

  def index
    query = params[:query]
    category_slug = params[:category_slug]
    current_state = params[:state] || "VERIFICATION_PENDING_FROM_DRAFT"
    variables = {
      cursor: params[:cursor],
      query: query,
      categorySlug: category_slug,
      filteringByUserAdmin: false,
      filteringByOrgOwner: false,
      organizationId: "",
      adminId: "",
      perPage: 20,
      allStates: true,
      orderBy: { field: "UPDATED_AT", direction: "DESC" },
    }
    variables[:state] = current_state unless current_state.blank?

    if params[:admin_type] == "Organization"
      variables[:organizationId] = params[:admin_id]
      variables[:filteringByOrgOwner] = true
    elsif params[:admin_type] == "User"
      variables[:adminId] = params[:admin_id]
      variables[:filteringByUserAdmin] = true
    end

    data = if request.xhr?
      variables = variables.except(:filteringByUserAdmin, :filteringByOrgOwner)
      platform_execute(IndexXhrQuery, variables: variables)
    else
      platform_execute(IndexQuery, variables: variables)
    end

    respond_to do |format|
      format.html do
        if request.xhr?
          # Pagination
          render partial: "biztools/marketplace_listings/listings",
                 locals: { data: data, current_state: current_state,
                           category_slug: category_slug, query: query }
        else
          render "biztools/marketplace_listings/index",
            locals: { data: data, query: query, category_slug: category_slug,
                      current_state: current_state }
        end
      end
    end
  end

  EditQuery = parse_query <<-'GRAPHQL'
    query($slug: String!) {
      marketplaceListing(slug: $slug) {
        listableIsSponsorable
        owner {
          databaseId
        }
        app {
          databaseId
        }
        adminInfo {
          oauthApplication {
            databaseId
          }
        }
      }
      ...Views::Biztools::MarketplaceListings::Edit::Root
    }
  GRAPHQL

  def edit
    data = platform_execute(EditQuery, variables: { slug: params[:listing_slug] })
    listing = data.marketplace_listing

    return render_404 unless listing && !listing.listable_is_sponsorable?

    user_id = listing.owner.database_id
    integrations = listable_integrations_for(user_id: user_id,
                                             integration: listing.app)
    oauth_apps = listable_oauth_apps_for(user_id: user_id,
                                         oauth_app: listing.admin_info.oauth_application)

    respond_to do |format|
      format.html do
        render "biztools/marketplace_listings/edit",
          locals: { data: data, oauth_apps: oauth_apps,
                    integrations: integrations }
      end
    end
  end

  ShowQuery = parse_query <<-'GRAPHQL'
    query($slug: String!) {
      marketplaceListing(slug: $slug) {
        listableIsSponsorable
        ...Views::Biztools::MarketplaceListings::Show::MarketplaceListing
      }
    }
  GRAPHQL

  def show
    data = platform_execute(ShowQuery, variables: { slug: params[:listing_slug] })
    listing = data.marketplace_listing

    return render_404 unless listing && !listing.listable_is_sponsorable?

    respond_to do |format|
      format.html do
        render "biztools/marketplace_listings/show", locals: { listing: listing }
      end
    end
  end

  ApproveQuery = parse_query <<-'GRAPHQL'
    mutation($input: ApproveMarketplaceListingInput!) {
      approveMarketplaceListing(input: $input) {
        marketplaceListing {
          resourcePath
        }
      }
    }
  GRAPHQL

  def approve
    input = { id: params[:listing_id], message: params[:message] }
    data = platform_execute(ApproveQuery, variables: { input: input })

    if data.errors.any?
      error_type = data.errors.details[:approveMarketplaceListing]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      flash[:error] = data.errors.messages.values.join(", ")
      return redirect_to(marketplace_listing_path(params[:slug]))
    end

    redirect_to data.approve_marketplace_listing.marketplace_listing.resource_path.to_s
  end

  RejectQuery = parse_query <<-'GRAPHQL'
    mutation($input: RejectMarketplaceListingInput!) {
      rejectMarketplaceListing(input: $input) {
        marketplaceListing {
          resourcePath
        }
      }
    }
  GRAPHQL

  def reject
    input = { id: params[:listing_id], state: params[:state], message: params[:message] }
    data = platform_execute(RejectQuery, variables: { input: input })

    if data.errors.any?
      error_type = data.errors.details[:rejectMarketplaceListing]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      flash[:error] = data.errors.messages.values.join(", ")
      return redirect_to(marketplace_listing_path(params[:slug]))
    end

    redirect_to data.reject_marketplace_listing.marketplace_listing.resource_path.to_s
  end

  DelistQuery = parse_query <<-'GRAPHQL'
    mutation($input: DelistMarketplaceListingInput!) {
      delistMarketplaceListing(input: $input) {
        marketplaceListing {
          resourcePath
        }
      }
    }
  GRAPHQL

  def delist
    input = { id: params[:listing_id], message: params[:message] }
    data = platform_execute(DelistQuery, variables: { input: input })

    if data.errors.any?
      error_type = data.errors.details[:delistMarketplaceListing]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      flash[:error] = data.errors.messages.values.join(", ")
      return redirect_to(marketplace_listing_path(params[:slug]))
    end

    redirect_to data.delist_marketplace_listing.marketplace_listing.resource_path.to_s
  end

  UpdateQuery = parse_query <<-'GRAPHQL'
    mutation($input: UpdateMarketplaceListingInput!) {
      updateMarketplaceListing(input: $input) {
        marketplaceListing
      }
    }
  GRAPHQL

  def update
    input = marketplace_listing_params.merge(slug: params[:listing_slug])

    if (id = input[:oauthApplicationDatabaseID]).present?
      input[:oauthApplicationDatabaseID] = id.to_i
    end

    [:featuredAt, :oauthApplicationDatabaseID, :appID].each do |field|
      input[field] = nil if input[field].blank?
    end

    if input[:featuredAt].present?
      input[:featuredAt] = parse_graphql_datetime(input[:featuredAt])
    end

    data = platform_execute(UpdateQuery, variables: { input: input })

    if data.errors.any?
      error_type = data.errors.details[:updateMarketplaceListing]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      message = data.errors.messages.values.join(", ")
      flash[:error] = message
      return redirect_to(biztools_edit_marketplace_listing_path(params[:listing_slug]))
    end

    redirect_to biztools_marketplace_listing_path(params[:listing_slug])
  end

  FeatureQuery = parse_query <<-'GRAPHQL'
    mutation($input: UpdateMarketplaceListingInput!) {
      updateMarketplaceListing(input: $input) {
        marketplaceListing {
          ...Views::Biztools::MarketplaceListings::Listing::MarketplaceListing
        }
      }
    }
  GRAPHQL

  def feature
    input = { slug: params[:listing_slug], featuredAt: Time.zone.now.iso8601 }

    data = platform_execute(FeatureQuery, variables: { input: input })

    if data.errors.any?
      error_type = data.errors.details[:updateMarketplaceListing]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      error = data.errors.messages.values.join(", ")
      return render json: { error: error }, status: :unprocessable_entity
    end

    respond_to do |format|
      format.html do
        if request.xhr?
          render partial: "biztools/marketplace_listings/listing", locals: { listing: data.update_marketplace_listing.marketplace_listing }
        else
          redirect_to(biztools_edit_marketplace_listing_path(params[:listing_slug]))
        end
      end
    end
  end

  def unfeature
    input = { slug: params[:listing_slug], featuredAt: nil }

    data = platform_execute(FeatureQuery, variables: { input: input })

    if data.errors.any?
      error_type = data.errors.details[:updateMarketplaceListing]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      error = data.errors.messages.values.join(", ")
      return render json: { error: error }, status: :unprocessable_entity
    end

    respond_to do |format|
      format.html do
        if request.xhr?
          render partial: "biztools/marketplace_listings/listing", locals: { listing: data.update_marketplace_listing.marketplace_listing }
        else
          redirect_to(biztools_edit_marketplace_listing_path(params[:listing_slug]))
        end
      end
    end
  end

  def reindex
    integration = Marketplace::Listing.find(params[:listing_id])
    if params[:reindex] == "add"
      Search.add_to_search_index("marketplace_listing", integration.id)
      flash[:notice] = "App added to the search index."
    elsif params[:reindex] == "remove"
      RemoveFromSearchIndexJob.perform_later("marketplace_listing", integration.id)
      flash[:notice] = "App removed from the search index."
    end

    redirect_to(biztools_marketplace_listing_path(integration.slug))
  end

  def hook
    listing = Marketplace::Listing.find_by!(slug: params[:listing_id])
    hook = listing.webhook

    return render_404 unless hook.present?

    render(
      "biztools/marketplace_listings/hook",
      locals: { listing: listing, hook: hook, hook_deliveries_query: params[:deliveries_q] }
    )
  end

  private

  def marketplace_listing_params
    params.require(:marketplace_listing).
      permit(:primaryCategoryName, :secondaryCategoryName,
             :oauthApplicationDatabaseID, :appID, :featuredAt,
             :hasDirectBilling, :isByGithub, filterCategories: [])
  end

  def verify_single_integratable_specified
    mkt_params = params[:marketplace_listing]
    if mkt_params[:appID].present? && mkt_params[:oauthApplicationDatabaseID].present?
      flash[:error] = "Cannot specify both an OAuth application and an integration."
      redirect_to biztools_edit_marketplace_listing_path(params[:listing_slug])
    end
  end

  def listable_integrations_for(user_id:, integration:)
    integrations = Integration.not_in_marketplace.where(owner_id: user_id)
    if integration
      integrations = integrations.where("integrations.id <> ?", integration.database_id)
    end
    integrations
  end

  def listable_oauth_apps_for(user_id:, oauth_app:)
    oauth_apps = OauthApplication.not_in_marketplace.where(user_id: user_id)
    if oauth_app
      oauth_apps = oauth_apps.where("oauth_applications.id <> ?", oauth_app.database_id)
    end
    oauth_apps
  end

  def cast_boolean_params
    # we need to cast the value sent in the request to an actual boolean
    # otherwise GraphQL rejects it
    %i(hasDirectBilling isByGithub).each do |attr|
      if (bool_param = params.dig(:marketplace_listing, attr)).present?
          params[:marketplace_listing][attr] = ActiveModel::Type::Boolean
            .new.cast(bool_param)
      end
    end
  end
end
