# frozen_string_literal: true

class Biztools::RepositoryActions::ListingsController < BiztoolsController
  areas_of_responsibility :marketplace

  before_action :actions_required

  DestroyQuery = parse_query <<-'GRAPHQL'
    mutation($input: DelistRepositoryActionInput!) {
      delistRepositoryAction(input: $input) {
        repositoryAction {
          databaseId
          name
        }
      }
    }
  GRAPHQL

  def destroy
    data = platform_execute(DestroyQuery, variables: { input: { id: params[:repository_action_id] } })

    if data.errors.any?
      error_type = data.errors.details[:delistRepositoryAction]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)
      return redirect_to :back, error: data.errors.messages.values.join(", ")
    end

    action = data.delist_repository_action.repository_action

    redirect_to edit_biztools_repository_action_path(id: action.database_id),
      notice: "Okay, #{action.name} has been delisted."
  end

  private

  def actions_required
    render_404 unless GitHub.actions_enabled?
  end
end
