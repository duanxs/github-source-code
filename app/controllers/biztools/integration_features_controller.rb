# frozen_string_literal: true

class Biztools::IntegrationFeaturesController < BiztoolsController
  areas_of_responsibility :marketplace

  before_action :marketplace_required

  IndexQuery = parse_query <<-'GRAPHQL'
    query {
      ...Views::Biztools::IntegrationFeatures::Index::Root
    }
  GRAPHQL

  def index
    data = platform_execute(IndexQuery)

    respond_to do |format|
      format.html do
        render "biztools/integration_features/index", locals: { data: data }
      end
    end
  end
end
