# frozen_string_literal: true

class Biztools::MarketplaceRecommendationsController < BiztoolsController
  areas_of_responsibility :marketplace

  before_action :marketplace_required

  IndexQuery = parse_query <<-'GRAPHQL'
    query {
      ...Views::Biztools::MarketplaceRecommendations::Index::Root
    }
  GRAPHQL

  def index
    data = platform_execute(IndexQuery)

    render "biztools/marketplace_recommendations/index", locals: { data: data }
  end

  def update
    listing_ids = Marketplace::Listing
      .with_state(:verified)
      .where(slug: recommended_slug_params)
      .pluck(:id)

    GitHub.kv.set("marketplace/recommendations", listing_ids.to_json)

    redirect_to biztools_marketplace_recommendations_path
  end

  def destroy
    listing = Marketplace::Listing.find_by(slug: params[:listing_slug])
    return render_404 unless listing && listing.allowed_to_edit?(current_user)

    raw_ids = GitHub.kv.get("marketplace/recommendations").value!
    return render_404 unless raw_ids

    listing_ids = JSON.parse(raw_ids).reject { |id| id == listing.id }
    GitHub.kv.set("marketplace/recommendations", listing_ids.to_json)

    redirect_to biztools_marketplace_recommendations_path
  end

  private

  def recommended_slug_params
    params.require(:marketplace_recommendations).require(:marketplace_listing_slugs)
  end
end
