# frozen_string_literal: true

class NotificationSubscriptionsController < ApplicationController
  statsd_tag_actions only: :index

  before_action :login_required
  before_action :repository_name_filter, only: [:index]

  # Some notifications are not scoped to an organization, so we do conditional checks manually.
  skip_before_action :perform_conditional_access_checks, only: [:index, :destroy, :dismiss_notice]

  include PlatformHelper
  include ActionView::Helpers::TextHelper

  PAGE_SIZE = 25

  IndexQuery = parse_query <<-'GRAPHQL'
    query($first: Int, $last: Int, $after: String, $before: String, $orderBy: NotificationSubscriptionOrder, $reason: NotificationReason, $listId: ID!, $includeList: Boolean!) {
      ...NotificationSubscriptions::IndexView::Root
    }
  GRAPHQL

  # The notification reasons we want to support filtering for in the UI
  REASONS_WHITELIST = Platform::Enums::NotificationReason.values.slice(
    "ASSIGN",
    "AUTHOR",
    "COMMENT",
    "MANUAL",
    "MENTION",
    "REVIEW_REQUESTED",
    "STATE_CHANGE",
    "TEAM_MENTION",
  ).freeze

  def index
    variables = graphql_pagination_params(page_size: PAGE_SIZE)
    sort_direction = params[:sort] == "asc" ? "ASC" : "DESC"
    variables["orderBy"] = { field: "ID", direction: sort_direction }

    variables["includeList"] = params[:repository].present?
    variables["listId"] = params[:repository] || ""

    if params[:reason].present?
      reason = params[:reason].upcase
      if REASONS_WHITELIST.key?(reason)
        variables["reason"] = reason
      end
    end

    data = platform_execute(IndexQuery, variables: variables)

    return render_graphql_error(data) if data.errors.any?

    respond_to do |wants|
      wants.html do
        render(
          "notification_subscriptions/index",
          locals: {
            view:  NotificationSubscriptions::IndexView.new(
              graphql_data: data,
              unauthorized_saml_targets: unauthorized_saml_targets,
              unauthorized_saml_organization_ids: unauthorized_saml_organization_ids,
              unauthorized_ip_whitelisting_targets: unauthorized_ip_whitelisting_targets,
              unauthorized_ip_whitelisting_organization_ids: unauthorized_ip_whitelisting_organization_ids,
              sort_direction: sort_direction.downcase,
              reason: reason,
              reasons: REASONS_WHITELIST.values,
            ),
          })
      end
    end
  end

  SubscribeMutation = parse_query <<-'GRAPHQL'
    mutation($id: ID!) {
      updateSubscription(input: { subscribableId: $id, state: SUBSCRIBED }) {
        subscribable {
          id
        }
      }
    }
  GRAPHQL

  UnsubscribeMutation = parse_query <<-'GRAPHQL'
    mutation($id: ID!) {
      updateSubscription(input: { subscribableId: $id, state: UNSUBSCRIBED }) {
        subscribable {
          id
        }
      }
    }
  GRAPHQL

  MuteMutation = parse_query <<-'GRAPHQL'
    mutation($id: ID!) {
      updateSubscription(input: { subscribableId: $id, state: IGNORED }) {
        subscribable {
          id
        }
      }
    }
  GRAPHQL

  def create
    case params[:reason]
    when "subscribed" then
      result = platform_execute(SubscribeMutation, variables: { id: params[:list_id] })
    when "ignore" then
      result = platform_execute(MuteMutation, variables: { id: params[:list_id] })
    else
      result = platform_execute(UnsubscribeMutation, variables: { id: params[:list_id] })
    end

    if result.errors.all.any?
      render_404
    else
      render json: result
    end
  end

  def destroy
    subscription_ids = Array(params[:subscription_ids]).take(PAGE_SIZE)
    response = GitHub.newsies.delete_thread_subscriptions(
      user_id: current_user.id,
      thread_subscription_ids: subscription_ids,
    )

    if response.success?
      flash[:notice] = "You’ve been unsubscribed from #{pluralize(subscription_ids.length, "thread")}."
    else
      flash[:error] = "Unsubscribe is not available at the moment."
    end

    redirect_to :back
  end

  def dismiss_notice
    current_user.dismiss_notice("notification_thread_subscriptions_notice")
    redirect_to notification_subscriptions_path
  end

  private

  RepoByNameQuery = parse_query <<-'GRAPHQL'
    query($owner: String!, $name: String!) {
      repository(owner: $owner, name: $name) {
        id

        owner {
          databaseId
        }
      }
    }
  GRAPHQL

  # Redirects ?repository_name=foo/bar&... to ?repository={global_relay_id}&...
  # if the repo is accessible. If not accessible or requires conditional access,
  # will redirect without a repository filter and render a flash.
  def repository_name_filter
    return unless params[:repository_name].present?

    filter_params = params.slice(:reason, :repository, :sort).permit(:reason, :repository, :sort).to_h
    owner, name = params[:repository_name].split("/")

    unless owner.present? && name.present?
      flash[:notice] = "To filter subscriptions enter a full repository name for example: twbs/bootstrap."
      return redirect_to notification_subscriptions_path(filter_params.merge(repository: nil))
    end

    data = platform_execute(RepoByNameQuery, variables: { owner: owner, name: name })

    if data.errors.any? || data.repository.nil?
      flash[:notice] = "#{params[:repository_name]} does not exist."
      return redirect_to notification_subscriptions_path(filter_params.merge(repository: nil))
    end

    if unauthorized_ip_whitelisting_organization_ids.include?(data.repository.owner.database_id)
      flash[:notice] = "Viewing subscriptions for #{params[:repository_name]} requires an allowed IP address."
      return redirect_to notification_subscriptions_path(filter_params.merge(repository: nil))
    end

    if unauthorized_saml_organization_ids.include?(data.repository.owner.database_id)
      flash[:notice] = "Viewing subscriptions for #{params[:repository_name]} requires single sign-on."
      return redirect_to notification_subscriptions_path(filter_params.merge(repository: nil))
    end

    redirect_to notification_subscriptions_path(filter_params.merge(repository: data.repository.id))
  end

  def target_for_conditional_access
    return :no_target_for_conditional_access unless logged_in?
    team = typed_object_from_id(
        [Platform::Objects::Team], params[:list_id]
    )
    team.organization
  rescue Platform::Errors::NotFound
    return :no_target_for_conditional_access # team not found
  end
end
