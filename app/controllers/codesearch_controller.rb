# frozen_string_literal: true

class CodesearchController < ApplicationController
  statsd_tag_actions only: :index

  skip_before_action :perform_conditional_access_checks
  before_action :login_required, only: [:index], if: :code_query?

  after_action :count_muted_search, only: [:index]

  after_action :clear_response_for_geyser_dark_ship, only: [:dark_ship]
  before_action :validate_dark_ship_request, only: [:dark_ship]

  include GitHub::RateLimitedRequest

  rate_limit_requests \
    only: :index,
    if: :codesearch_rate_limit_filter,
    key: :codesearch_rate_limit_key,
    log_key: :codesearch_rate_limit_log_key,
    max: :codesearch_rate_limit_max,
    at_limit: :codesearch_rate_limit_record

  def index
    respond_to do |format|
      format.html do

        return render_404 if page > 100

        # Redirect to repository search if that's the context they submitted
        if params[:search_target] == "repository"
          repo = Repository.nwo(params[:nwo])
          return render_404 unless repo && repo.pullable_by?(current_user)

          return redirect_to repo_search_path(repo.owner, repo.name,
            type: params[:type],
            q: get_query,
            ref: params[:ref])

        elsif user = params[:user]
          query = "user:#{user} #{get_query}"
          return redirect_to search_url(q: query)
        end

        # for the view
        @language = language
        @search   = sanitized_query
        @state    = params[:state]
        @package_type = params[:package_type]
        @sort     = get_sort
        @order    = get_order
        @unscoped_search = get_unscoped_query

        if @search.present?
          @type = params[:type] = queries.search_type

          if dotcom_search_in_ghe_enabled? && queries.options.key?(:environment)
            if get_query.include? "environment:github"
              return redirect_to dotcom_search_url(q: @search, type: @type)
            end
          end

          # relies on conditionals applied by logical_service method on base
          push_service_mapping_context

          GitHub.dogstats.increment("search", tags: ["client:mobile"]) if mobile?
          render "codesearch/results", locals: { scope: :global, originating_request_id: request_id }
        else
          @type = type
          render "codesearch/index"
        end
      end
    end
  end

  def advanced_search
    @language = language
    @search   = sanitized_query
    render "codesearch/advanced_search"
  end

  def count
    if code_query? && !logged_in?
      render partial: "codesearch/logged_out_message"
    else
      query = queries.current

      # relies on conditionals applied by logical_service method on base
      push_service_mapping_context

      count = Search::CountView.new(query: query, type: type)
      render html: count.render
    end
  end

  def suggest_location
    query = Search::Queries::CitySuggestQuery.new \
      phrase: get_query,
      country: get_country

    result = query.execute

    respond_to do |format|
      format.json do
        render json: result["options"]
      end
    end
  end

  ExploreTopicsQuery = parse_query <<-'GRAPHQL'
    query($name: String!) {
      topic(name: $name) {
        ...Views::Codesearch::ExploreTopics::Topic
      }
    }
  GRAPHQL

  def explore_topics
    query = queries[queries.search_type]
    topic_name = if query.qualifiers.key?(:topic) && query.qualifiers[:topic].must
      query.qualifiers[:topic].must.first
    end
    return head :ok unless topic_name

    data = platform_execute(ExploreTopicsQuery, variables: { name: topic_name })
    return head :ok if data.errors.any?

    topic = data.topic
    return head :not_found unless topic

    respond_to do |format|
      format.html do
        render partial: "codesearch/explore_topics",
               locals: { topic: topic, query_param: query_param(query) }
      end
    end
  end

  # Special endpoint defined to execute and measure dark_shipped Geyser queries
  def dark_ship
    queries_with_dark_ship = Search::QueryHelper.new(
        get_query, "Code",
        current_user: current_user,
        remote_ip: request.remote_ip,
        search_scope: :global,
        aggregations: false,
        highlight: true,
        sort: [nil, "desc"],
        page: 1,
        per_page: 10,
        state: nil,
        language: language,
        include_explain_output: false,
        geyser_enabled: true,
        dark_ship: true,
        request_id: request_id,
        originating_request_id: params[:originating_request_id] # request_id of query that triggered dark ship call
    )

    # force-exec the dark ship Geyser query, perform a
    # placeholder render so we can test after_action cleanup
    queries_with_dark_ship.code_query.execute
    render html: "<p>#{params}</p>" # stripped in after_action anyway
  end

  # overrides default in ApplicationController to
  # condition on query type resolved in method scope
  def logical_service
    if code_query?
      if dark_ship_request?
        "#{GitHub::ServiceMapping::SERVICE_PREFIX}/geyser_code_search_query"
      else
        "#{GitHub::ServiceMapping::SERVICE_PREFIX}/code_search_query"
      end
    else
      super
    end
  end

private

  # clean the response body if the call to this controller is a dark ship request
  def clear_response_for_geyser_dark_ship
    if dark_ship_request?
      self.response_body = ""
    end
  end

  # Is this request eligible for dark_ship treatment?
  def dark_ship_request?
    # memoize or return from cached value
    return @dark_ship_enabled if defined?(@dark_ship_enabled)
    @dark_ship_enabled = begin
        GitHub.geyser_search_enabled? &&
            type == "Code" &&
            request.path.include?("search/unscoped-gds") &&
            action_name == "dark_ship"
      end
  end

  # make sure to 404 on Geyser-enabled, repo-scoped code searches
  # that are also invalid dark ship requests
  def validate_dark_ship_request
    if !dark_ship_request?
      GitHub.dogstats.increment "search.geyser_dark_ship.invalid_query", tags: ["query_scope:unscoped"]
      return render_404
    end
  end

  def query_param(query)
    query_parts = []
    if query.qualifiers.key?(:org) && query.qualifiers[:org].must
      query_parts << "org:#{query.qualifiers[:org].must.first}"
    end
    if query.qualifiers.key?(:fork) && query.qualifiers[:fork].must
      query_parts << "fork:#{query.qualifiers[:fork].must.first}"
    end
    query_parts.join(" ")
  end

  # Unauthenticated: 10 searches every minute
  # Authenticated:   30 searches every minute
  def codesearch_rate_limit_max
    logged_in? ? 30 : 10
  end

  def codesearch_rate_limit_filter
    get_query.present?
  end

  def codesearch_rate_limit_key
    "search_limiter:#{logged_in? ? current_user.id : request.remote_ip}"
  end

  def codesearch_rate_limit_log_key
    "search-#{index_name}-#{authed_or_anon}"
  end

  def codesearch_rate_limit_record
    key = "search.#{index_name}.ratelimited.#{authed_or_anon}"
    GitHub.dogstats.increment("search.ratelimited", {tags: ["index:#{index_name}", "logged_in:#{authed_or_anon}"]})
  end

  def index_name
    queries.current.index.name
  end

  def authed_or_anon
    logged_in? ? "auth" : "anon"
  end

  def get_query
    case params[:q]
    when Array;  params[:q].join(" ")
    when String; params[:q]
    end
  end

  def get_unscoped_query
    case params[:unscoped_q]
    when Array;  params[:unscoped_q].join(" ")
    when String; params[:unscoped_q]
    end
  end

  def get_country
    country = params[:c]
    case country
    when Array
      country.map { |str| str.upcase }
    when String
      country = country.upcase
      if country.include?(",")
        country.split(",")
      else
        country
      end
    else
      "ANY"
    end
  end

  def page
    current_page(:p)
  end

  def per_page
    Search::Query::PER_PAGE
  end

  def get_sort
    return params[:s] if params[:s].present?

    sort = queries.current.sort
    sort.at(0) if sort.is_a? Array
  end

  def get_order
    if params[:s].present?
      order = params[:o]
      order.present? ? order : "desc"
      order = "desc" unless %w[asc desc].include? order
      order
    else
      sort = queries.current.sort
      sort.at(1) if sort.is_a? Array
    end
  end

  # Determines the language filter from query parameters.
  #
  # Returns a Linguist::Language or nil if no filter is provided.
  def language
    name = params[:l] if params[:l].present?
    name ? Linguist::Language[name] : nil
  end

  def sort
    [get_sort, get_order] if params[:s].present?
  end

  def type
    if params[:type].present?
      case params[:type].to_s.downcase
      when "code";        "Code"
      when "commits";     "Commits"
      when "discussions"
        if GitHub.enterprise?
          "Repositories"
        else
          "Discussions"
        end
      when "issues";      "Issues"
      when "marketplace"
        if marketplace_enabled?
          "Marketplace"
        else
          "Repositories"
        end
      when "registrypackages"; "RegistryPackages"
      when "users";       "Users"
      when "topics";      "Topics"
      when "wikis";       "Wikis"
      else "Repositories"
      end
    end
  end

  def queries
    @queries ||= Search::QueryHelper.new(sanitized_query, type,
        current_user: current_user,
        remote_ip: request.remote_ip,
        aggregations: true,
        highlight: true,
        language: language,
        sort: sort,
        page: page,
        per_page: per_page,
        state: params[:state],
        package_type: params[:package_type],
        user_session: user_session,
    )
  end

  def count_muted_search
    if params[:muted]
      GitHub.dogstats.increment "search", tags: ["action:mute"]
    end
  end

  def dotcom_search_in_ghe_enabled?
    return GitHub::Connect.unified_search_enabled?
  end

  def code_query?
    type && type == "Code"
  end

  def sanitized_query
    if dotcom_search_in_ghe_enabled? && get_query.present?
      get_query.gsub(/\s?environment:\s?(github|local)\s?/, " ").strip
    else
      get_query
    end
  end
end
