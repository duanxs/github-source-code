# frozen_string_literal: true

class MarketplaceListingHooksController < ApplicationController
  areas_of_responsibility :webhook, :marketplace

  before_action :ensure_listing_admin
  before_action :marketplace_required
  before_action :sudo_filter

  layout "marketplace"

  ShowQuery = parse_query <<-'GRAPHQL'
    query($listingSlug: String!) {
      marketplaceListing(slug: $listingSlug) {
        viewerCanEdit
      }
      ...Views::MarketplaceListingHooks::Show::Root
    }
  GRAPHQL

  def show
    data = platform_execute(ShowQuery, variables: {
      listingSlug: params[:listing_slug],
    })

    return render_404 unless data.marketplace_listing && data.marketplace_listing.viewer_can_edit?
    # WebHooks are not yet available via GraphQL
    hook = marketplace_listing.webhook || marketplace_listing.build_webhook(active: true)

    render "marketplace_listing_hooks/show", locals: { hook: hook, data: data }
  end

  def create
    hook = marketplace_listing.build_webhook(active: true)
    hook.track_creator(current_user)

    if hook.update(hook_params)
      flash[:notice] = "Okay, that hook was successfully created. We sent a ping payload to test it out! Read more about it at #{GitHub.developer_help_url}/webhooks/#ping-event."

    else
      flash[:error] = "There was an error setting up your hook: #{hook.errors.full_messages.to_sentence}"
    end

    redirect_to marketplace_listing_hook_path
  end

  def update
    hook = marketplace_listing.webhook
    hook.assign_attributes(hook_params)

    if hook.save
      flash[:notice] = "Okay, the hook was successfully updated."
    else
      flash[:error] = "There was an error updating your hook: #{hook.errors.full_messages.to_sentence}"
    end

    redirect_to marketplace_listing_hook_path
  end

  private

  def hook_params
    params.require(:hook).permit :active,
                                 :content_type,
                                 {events: []},
                                 :insecure_ssl,
                                 :secret,
                                 :url
  end

  def marketplace_listing
    @marketplace_listing ||= Marketplace::Listing.find_by_slug(params[:listing_slug])
  end

  def ensure_listing_admin
    return if current_user&.biztools_user? || current_user&.site_admin?
    return if marketplace_listing && marketplace_listing.adminable_by?(current_user)
    render_404
  end

  def target_for_conditional_access
    listing = Marketplace::Listing.find_by_slug(params[:listing_slug])
    return :no_target_for_conditional_access unless listing
    listing.owner
  end
end
