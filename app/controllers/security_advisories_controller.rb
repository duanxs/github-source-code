# frozen_string_literal: true

class SecurityAdvisoriesController < ApplicationController
  include GitHub::Authentication::Feed

  areas_of_responsibility :security_advisories

  statsd_tag_actions only: :index

  def index
    respond_to do |format|
      format.html {
        render "security_advisories/index", layout: false, formats: [:atom], content_type: :atom
      }
      format.atom { render "security_advisories/index", layout: false }
    end
  end

  private

  # Security advisories are public
  def require_conditional_access_checks?
    false
  end

  def feed_actions
    %w(index)
  end
end
