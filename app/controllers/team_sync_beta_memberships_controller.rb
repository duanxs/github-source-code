# frozen_string_literal: true

class TeamSyncBetaMembershipsController < ApplicationController
  before_action :dotcom_required
  skip_before_action :perform_conditional_access_checks

  def signup
    redirect_to "#{GitHub.help_url}/articles/synchronizing-teams-between-your-identity-provider-and-github"
  end
end
