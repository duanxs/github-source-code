# frozen_string_literal: true

class Stafftools::TeamsController < StafftoolsController
  areas_of_responsibility :orgs, :stafftools

  before_action :ensure_user_exists
  before_action \
    :ensure_team_exists,
    except: [:index, :owners, :add_owner, :direct_members, :outside_collaborators,
             :pending_collaborators, :invitations, :invitation_opt_outs,
             :set_invitation_rate_limit, :reset_invitation_rate_limit, :remove_invitation_opt_out]

  layout "stafftools/organization/security"

  TeamsIndexQuery = parse_query <<-'GRAPHQL'
    query($login: String!) {
      organization(login: $login) {
        ...Views::Stafftools::Teams::Index::Organization
      }
    }
  GRAPHQL

  def index
    query = platform_execute(TeamsIndexQuery, variables: { login: params[:user_id] })

    locals = {
      num_admins:      this_user.admins.count,
      num_members:     this_user.members_count,
      num_outside_collaborators: this_user.outside_collaborators_count,
      num_invitations: this_user.pending_invitations.count,
      num_opted_out_invitations: OrganizationInvitation::OptOut.grouped_by_invitation(org: this_user).count,
      teams: Stafftools::Team.for_organization(this_user, current_page),
      org: query.organization,
    }
    render "stafftools/teams/index", locals: locals
  end

  def database
    render "stafftools/teams/database"
  end

  def owners
    query = [
      this_user.audit_log_query,
      "action:org.*member",
      "(data.permission:admin OR data.old_permission:admin)",
    ]
    locals = fetch_audit_log_teaser query.join(" ")

    locals[:users] = this_user.admins.order("login ASC")
      .paginate(page: current_page)
    locals[:all_org_owners] = this_user.admins.order("login ASC")
      .includes(:profile)

    render "stafftools/teams/owners", locals: locals
  end

  def add_owner
    GitHub.context.push(hide_staff_user: true)
    Audit.context.push(hide_staff_user: true)
    user_to_add_as_owner = User.find_by_login(params[:login])
    reason = params[:reason]
    case
    when reason.nil?
      flash[:error] = "Nice try, you need to provide both a username and a reason"
    when reason.strip.empty?
      flash[:error] = "Nice try, you need to type a meaningful reason"
    when user_to_add_as_owner.nil?
      flash[:error] = "Nice try, there isn't anybody called #{params[:login]}"
    when user_to_add_as_owner.organization?
      flash[:error] = "#{user_to_add_as_owner.login} is an organization"
    when this_user.adminable_by?(user_to_add_as_owner)
      flash[:error] = "#{user_to_add_as_owner.login} already owns #{this_user.login}"
    when this_user.member?(user_to_add_as_owner)
      Audit.context.push(note: reason) do
        this_user.update_member(user_to_add_as_owner, action: :admin, updater: current_user)
      end
      flash[:notice] = "#{user_to_add_as_owner.login} was already a member of #{this_user.login} and they have been promoted to an owner"
    else
      Audit.context.push(note: reason) do
        this_user.add_admin(user_to_add_as_owner, adder: current_user)
      end
      flash[:notice] = "#{user_to_add_as_owner.login} added as owner to #{this_user.login}"
    end
    redirect_to owners_stafftools_user_path(this_user)
  end

  def direct_members
    query = "#{this_user.audit_log_query} action:org.*member"
    locals = fetch_audit_log_teaser query

    locals[:users] = this_user.members.order("login ASC").paginate(page: current_page)

    render "stafftools/teams/direct_members", locals: locals
  end

  def outside_collaborators
    locals = { users: this_user.outside_collaborators.order("login ASC").paginate(page: current_page) }
    render "stafftools/teams/outside_collaborators", locals: locals
  end

  PendingCollabsQuery = parse_query <<-'GRAPHQL'
    query(
      $login: String!,
      $isOccupyingSeat: Boolean,
      $first: Int,
      $last: Int,
      $after: String,
      $before: String
    ) {
      organization(login: $login) {
        ...Views::Stafftools::Teams::PendingCollaborators::Organization
        ...Stafftools::Organization::Teams::PendingCollaboratorsView::OrganizationQuery
      }
    }
  GRAPHQL


  def pending_collaborators
    filter_seats = params[:is_occupying_seat] == "true" ? true : false

    variables = { login: params[:id], isOccupyingSeat: filter_seats }
    variables.merge!(graphql_pagination_params(page_size: 35))

    query = platform_execute(PendingCollabsQuery, variables: variables)

    return render_404 unless query.organization

    org = query.organization
    view = Stafftools::Organization::Teams::PendingCollaboratorsView.new(org: org, filter_seats: filter_seats)

    render "stafftools/teams/pending_collaborators", locals: { org: org, view: view }
  end

  def invitations
    query = [
      this_user.audit_log_query,
      "(action:org.invite_member OR action:org.cancel_invitation OR action:org.rate_limited_invites OR action:org.*_custom_invitation_rate_limit)",
    ]
    locals = fetch_audit_log_teaser query.join(" ")

    locals[:invitations] = this_user.pending_invitations
      .paginate(page: current_page)

    locals[:rate_limit_policy] = OrganizationInvitation::RateLimitPolicy.new(this_user)

    render "stafftools/teams/invitations", locals: locals
  end

  def invitation_opt_outs
    invitation_opt_outs = OrganizationInvitation::OptOut.grouped_by_invitation(org: this_user)

    render "stafftools/teams/invitation_opt_outs", locals: {
      invitation_opt_out_groups: invitation_opt_outs,
    }
  end

  # Remove an opt out for invitations from an organization
  #
  # invitation - the `id` of the invitation that the user opted out from
  def remove_invitation_opt_out
    invite = params[:invitation]
    opt_outs = OrganizationInvitation::OptOut.where(organization_invitation_id: invite)

    if opt_outs.present?
      opt_outs.each do |opt_out|
        opt_out.destroy
      end

      instrument("staff.remove_opt_out", user: this_user, invitation_id: invite)
      flash[:notice] = "Opt outs removed for invitation ##{invite}"
      redirect_to :back
    else
      flash[:error] = "Whoops. There aren't any opt outs for that invitation."
      redirect_to :back
    end
  end

  def set_invitation_rate_limit
    policy = OrganizationInvitation::RateLimitPolicy.new(this_user)

    if policy.set_custom_limit(params[:custom_rate_limit])
      flash[:notice] = "Custom rate limit policy applied!"
    else
      flash[:error] = "There was a problem parsing the rate limit. Please use numbers only, without commas."
    end

    redirect_to invitations_stafftools_user_url(this_user)
  end

  def reset_invitation_rate_limit
    policy = OrganizationInvitation::RateLimitPolicy.new(this_user)

    policy.clear_custom_limit

    flash[:notice] = "Custom rate limit policy removed!"
    redirect_to invitations_stafftools_user_url(this_user)
  end

  def show
    locals = fetch_audit_log_teaser "data.team_id:#{this_team.id}"
    locals[:num_repos]    = this_team.repositories_scope.count
    locals[:num_members]  = this_team.members_scope.count
    locals[:num_requests] = this_team.pending_team_membership_requests.count
    locals[:ancestors] = this_team.ancestors
    locals[:child_teams_count] = this_team.descendants.count
    locals[:external_groups_count] = this_team.group_mappings.count

    render "stafftools/teams/show", locals: locals
  end

  def members
    locals = {
      team_maintainers: this_team.maintainers,
      org_admins: this_user.direct_admins,
    }

    members_scope = this_team.members_scope.order("login ASC").includes(:profile)
    current_page_of_members_scope = members_scope.paginate(page: current_page)

    locals[:all_members] = members_scope
    locals[:current_page_of_members] = current_page_of_members_scope
    locals[:direct_members] = Set.new(this_team
                                        .members_scope(membership: :immediate)
                                        .where(id: current_page_of_members_scope.map(&:id)),
                                     )

    render "stafftools/teams/members", locals: locals
  end

  def external_members
    locals = {
      team_maintainers: this_team.maintainers,
      org_admins: this_user.direct_admins,
    }

    # pull from RPC endpoint
    response = GroupSyncer.client.list_group_members(team_id: this_team.global_relay_id,
                                                     group_id: params[:group_id])
    if response.error
      flash[:error] = "An error occurred while fetching group data, please try again"
      locals[:all_members] = []
      locals[:current_page_of_members] = []
    else
      all_external_members = this_team.matched_external_members(response.data.members)

      current_page_of_members_scope = all_external_members.paginate(page: current_page)

      locals[:all_members] = all_external_members
      locals[:current_page_of_members] = current_page_of_members_scope
    end

    render "stafftools/teams/external_members", locals: locals
  end

  def child_teams
    locals = {
      child_teams: this_team.descendants.paginate(page: current_page),
    }

    render "stafftools/teams/child_teams", locals: locals
  end

  def external_groups
    locals = {
      external_groups: this_team.group_mappings.paginate(page: current_page),
    }

    render "stafftools/teams/external_groups", locals: locals
  end

  def repositories
    render "stafftools/teams/repositories", locals: {
      repos: Stafftools::Repository.for_team(this_team, current_page),
    }
  end

  def requests
    requests = this_team.pending_team_membership_requests
      .includes(requester: :profile)
      .paginate(page: current_page)
    render "stafftools/teams/requests", locals: {requests: requests}
  end

  def sync
    if this_team.externally_managed?
      rpc_response = GroupSyncer.client.sync_team(
        org_id: this_team.organization.global_relay_id,
        team_id: this_team.global_relay_id,
      )

      if rpc_response.error
        flash[:error] = "Team Sync Failed CODE: #{rpc_response.error.code} MSG: #{rpc_response.error.msg}"
      else
        flash[:notice] = "Team Sync Requested for #{this_team.name}"
      end

    else
      flash[:error] = "Team Sync is not configured for #{this_team.name}"
    end

    redirect_to stafftools_user_teams_path(this_team.organization)
  end
end
