# frozen_string_literal: true

class Stafftools::IssuesController < StafftoolsController

  before_action :ensure_repo_exists
  before_action :ensure_issue_exists, except: :index
  before_action :redirect_if_pull_request, only: :show
  before_action :ensure_not_pull_request, except: [:index, :show]

  layout "stafftools/repository/collaboration"

  def index
    @issues = current_repository.issues.order("id DESC").paginate \
      page: params[:page] || 1,
      per_page: 25
    @deleted_issues = DeletedIssue.where(repository_id: current_repository.id).order("id DESC")
    render "stafftools/issues/index"
  end

  def show
    fetch_audit_log_teaser "data.issue_id:#{this_issue.id} OR (data.user_content_id:#{this_issue.id} AND data.user_content_type:#{this_issue.class} AND action:user_content_edit.*)"
    render "stafftools/issues/show"
  end

  def database
    respond_to do |f|
      f.html do
        render "stafftools/issues/database"
      end
    end
  end

  def destroy
    issue_num = this_issue.number

    this_issue.destroy

    instrument \
      "staff.delete_issue",
      user: current_repository.owner,
      repo: current_repository,
      note: "Deleted issue #{current_repository.nwo}##{issue_num}"

    flash[:notice] = "Issue ##{this_issue.number} deleted"
    redirect_to gh_stafftools_repository_issues_path(current_repository)
  end

  def lock
    reason = params[:reason].present? ? params[:reason] : nil
    this_issue.lock(current_user, reason)

    redirect_to gh_stafftools_repository_issues_path(this_issue)
  end

  def unlock
    this_issue.unlock(current_user)

    redirect_to gh_stafftools_repository_issues_path(this_issue)
  end

  private

  def ensure_not_pull_request
    return render_404 if this_issue.pull_request?
  end

  def redirect_if_pull_request
    if this_issue.pull_request?
      redirect_to gh_stafftools_repository_pull_request_path(this_issue)
    end
  end
end
