# frozen_string_literal: true

class Stafftools::BannedIpAddressesController < StafftoolsController

  def index
    ip_addresses = BannedIpAddress.order("updated_at DESC").paginate(page: current_page, per_page: 30)

    render "stafftools/banned_ip_addresses/index", locals: {
      ip_addresses: ip_addresses,
    }
  end

  def search
    render "stafftools/banned_ip_addresses/search", locals: {
      banned_ip: BannedIpAddress.find_by_ip(params[:ip_address]),
    }
  end

  def create
    ip = BannedIpAddress.block_unless_whitelisted(params[:ip_address], params[:reason], current_user)
    if ip.errors.any?
      flash[:error] = ip.errors.full_messages.to_sentence
    else
      flash[:notice] = "IP address banned, but may take #{ban_cache_lag_time_in_words} to take effect"
      GitHub.dogstats.increment("banned_ip.report", tags: ["stafftools:success", "operation:create"])
    end
    redirect_to action: :index
  end

  def ban
    banned_address = BannedIpAddress.find(params[:id])

    if banned_address.ban(params[:reason], current_user)
      flash[:notice] = "IP address banned, but may take #{ban_cache_lag_time_in_words} to take effect"
      GitHub.dogstats.increment("banned_ip.report", tags: ["stafftools:success", "operation:ban"])
    else
      flash[:error] = banned_address.errors.full_messages.to_sentence
    end

    redirect_to action: :index
  end

  def whitelist
    banned_address = BannedIpAddress.find(params[:id])
    if banned_address.whitelist(params[:reason], current_user)
      flash[:notice] = "IP address whitelisted, but may take #{ban_cache_lag_time_in_words} to take effect"
      GitHub.dogstats.increment("banned_ip.report", tags: ["stafftools:success", "operation:whitelist"])
    else
      flash[:error] = "Unable to whitelist IP address"
      GitHub.dogstats.increment("banned_ip.report", tags: ["stafftools:error", "operation:whitelist"])
    end

    redirect_to action: :index
  end

  def destroy
    banned_address = BannedIpAddress.find(params[:id])
    if banned_address.destroy
      flash[:notice] = "Banned IP address record destroyed, but may take #{ban_cache_lag_time_in_words} to take effect"
      GitHub.dogstats.increment("banned_ip.report", tags: ["stafftools:success", "operation:unban"])
    else
      flash[:error] = "Could not allow banned IP address #{params[:id]}"
      GitHub.dogstats.increment("banned_ip.report", tags: ["stafftools:error", "operation:unban"])
    end

    redirect_to action: :index
  end

  private

  def ban_cache_lag_time_in_words
    helpers.distance_of_time_in_words(
      GitHub::BannedIpsMiddleware::TTL,
    )
  end
end
