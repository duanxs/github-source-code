# frozen_string_literal: true

# stafftools for showcase crud
class Stafftools::ShowcaseCollectionsController < StafftoolsController
  include ShowcaseCollectionsControllerMethods
  areas_of_responsibility :explore, :stafftools

  before_action :showcase_disabled
  before_action :find_collection, except: [:index, :new, :create]

  def index
    featured_collections = ::Showcase::Collection.featured
    collections = ::Showcase::Collection.order("published ASC, name ASC").to_a

    render "stafftools/showcase_collections/index", locals: {
      featured_collections: featured_collections,
      collections: collections,
    }
  end

  def show
    items = @collection.items

    render "stafftools/showcase_collections/show", locals: {
      collection: @collection,
      items: items,
    }
  end

  def new
    new_collection(:stafftools)
  end

  def create
    create_collection(:stafftools)
  end

  def edit
    edit_collection(:stafftools)
  end

  def update
    update_collection(:stafftools)
  end

  def destroy
    destroy_collection(:stafftools)
  end
end
