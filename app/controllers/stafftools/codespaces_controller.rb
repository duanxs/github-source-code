# frozen_string_literal: true

class Stafftools::CodespacesController < StafftoolsController
  layout "stafftools/user/content"

  def index
    codespaces = this_user.codespaces.by_recently_used
    render "stafftools/codespaces/index", locals: { codespaces: codespaces }
  end

  def show
    codespace = this_user.codespaces.find_by!(slug: params[:id])
    render "stafftools/codespaces/show", locals: { codespace: codespace }
  end
end
