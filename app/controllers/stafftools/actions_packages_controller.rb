# frozen_string_literal: true

class Stafftools::ActionsPackagesController < StafftoolsController
  areas_of_responsibility :gitcoin, :stafftools

  include ActionView::Helpers::NumberHelper

  before_action :ensure_billing_enabled
  before_action :ensure_user_exists, only: [:show]

  def show
    render "stafftools/actions_packages/show", layout: "stafftools/organization/content"
  end

  private

  def actions_usage
    @_actions_usage ||= Billing::ActionsUsage.new(this_user)
  end
  helper_method :actions_usage

  def package_registry_usage
    @_package_registry_usage ||= Billing::PackageRegistryUsage.new(this_user)
  end
  helper_method :package_registry_usage

  def shared_storage_usage
    @_shared_storage_usage ||= Billing::SharedStorageUsage.new(this_user)
  end
  helper_method :shared_storage_usage

  def metered_billing_permissions
    @_metered_billing_permissions ||= Billing::MeteredBillingPermission.new(this_user)
  end
  helper_method :metered_billing_permissions
end
