# frozen_string_literal: true

class Stafftools::Users::BillingController < StafftoolsController
  areas_of_responsibility :gitcoin

  layout :nav_layout

  before_action :ensure_user_exists

  def history
    view = Stafftools::User::BillingHistoryView.new(user: @user)
    render "stafftools/users/history", locals: { view: view }
  end

  private

  def nav_layout
    case this_user.site_admin_context
    when "organization"
      "stafftools/organization/billing"
    when "user"
      "stafftools/user/billing"
    end
  end
end
