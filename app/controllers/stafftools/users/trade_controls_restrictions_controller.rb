# frozen_string_literal: true

class Stafftools::Users::TradeControlsRestrictionsController < StafftoolsController
  before_action :ensure_user_exists
  before_action :ensure_billing_enabled

  def enforce
    if this_user.has_any_trade_restrictions?
      flash[:error] = "This user has already been trade controls restricted"
      return redirect_back(fallback_location: admin_stafftools_user_path(id: this_user.id))
    end

    event = this_user.trade_controls_restriction.restriction_event(params[:restriction_type])

    if event.blank?
      flash[:error] = "This was not a valid restriction type"
      return redirect_back(fallback_location: admin_stafftools_user_path(id: this_user.id))
    end

    if this_user.trade_controls_restriction.__send__("#{event}!", compliance: compliance)
      flash[:notice] = "Successfully enforced trade controls restrictions for #{this_user}"
    end

    redirect_back(fallback_location: admin_stafftools_user_path(id: this_user.id))
  end

  def override
    unless this_user.has_any_trade_restrictions?
      flash[:error] = "This user is not trade controls restricted"
      return redirect_back(fallback_location: admin_stafftools_user_path(id: this_user.id))
    end

    if this_user.trade_controls_restriction.override!(compliance: compliance)
      flash[:notice] = "Successfully removed trade controls restriction enforcement for #{this_user}."
    else
      flash[:error] = "Error while overriding trade controls restrictions: #{this_user.errors.full_messages.to_sentence}"
    end

    redirect_back(fallback_location: admin_stafftools_user_path(id: this_user.id))
  end

  def update
    unless this_user.has_any_trade_restrictions?
      flash[:error] = "This user is not trade controls restricted"
      return redirect_back(fallback_location: admin_stafftools_user_path(id: this_user.id))
    end

    event = this_user.trade_controls_restriction.restriction_event(params[:restriction_type])

    if event.blank?
      flash[:error] = "This was not a valid restriction type"
      return redirect_back(fallback_location: admin_stafftools_user_path(id: this_user.id))
    end

    if this_user.trade_controls_restriction.__send__("#{event}!", compliance: compliance)
      flash[:notice] = "Successfully updated trade controls restrictions for #{this_user}"
    end

    redirect_back(fallback_location: admin_stafftools_user_path(id: this_user.id))
  end

  def check_violations
    return render_404 unless request.xhr?

    checker = TradeControls::ComplianceChecksDryRun.new(this_user)
    checker.run

    render json: { violations: checker.humanize_violations }
  end

  private

  def compliance
    @compliance ||= TradeControls::Compliance.for(actor: current_user, reason: params[:reason])
  end
end
