# frozen_string_literal: true

class Stafftools::Users::MeteredBillingController < StafftoolsController
  areas_of_responsibility :gitcoin

  layout :nav_layout

  before_action :ensure_user_exists

  def actions
    render "stafftools/users/metered_billing/actions", locals: {
      repositories: aggregator.actions_usage_since_cycle_reset,
    }
  end

  def packages_bandwidth
    render "stafftools/users/metered_billing/packages_bandwidth", locals: {
      registry_packages: aggregator.packages_bandwidth_since_cycle_reset,
    }
  end

  def shared_storage_usage
    render "stafftools/users/metered_billing/shared_storage_usage", locals: {
      aggregated_usage_records: aggregator.shared_storage_usage,
    }
  end

  def actions_artifact_expirations
    render "stafftools/users/metered_billing/shared_storage", locals: {
      repositories: aggregator.upcoming_actions_artifact_storage_expirations,
    }
  end

  def advance_quota_reset_date
    if this_user.advance_metered_cycle_reset_date!
      flash[:notice] = "Successfully moved reset date to #{this_user.reload.first_day_in_next_metered_cycle}"
    else
      flash[:error] = "Failed to advance reset date"
    end

    redirect_to billing_stafftools_user_url(this_user)
  end

  private

  def nav_layout
    case this_user.site_admin_context
    when "organization"
      "stafftools/organization/billing"
    when "user"
      "stafftools/user/billing"
    end
  end

  def aggregator
    @_aggregator = ::Billing::Stafftools::MeteredUsageAggregation.new(owner_id: this_user.id)
  end
end
