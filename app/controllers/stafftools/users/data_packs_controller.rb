# frozen_string_literal: true

class Stafftools::Users::DataPacksController < Stafftools::UsersController
  areas_of_responsibility :stafftools

  def delete
    this_user.reset_data_packs(actor: current_user)
    this_user.update_plan_with_data_packs

    flash[:notice] = "#{this_user.login}'s data subscription has been removed"
    redirect_to :back
  end
end
