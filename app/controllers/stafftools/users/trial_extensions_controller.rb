# frozen_string_literal: true

class Stafftools::Users::TrialExtensionsController < Stafftools::UsersController
  include MarketingMethods

  areas_of_responsibility :stafftools

  def create
    plan_trial = Billing::EnterpriseCloudTrial.new(@user)
    success = plan_trial.extend_trial(extension_length)

    if success
      flash[:notice] = "Trial was successfully extended"
    else
      flash[:error] = "Unable to extend trial"
    end

    analytics_event organization_trial_extension_ga_event_attributes(success, @user, current_user)

    redirect_to :back
  end

  private

  def extension_length
    params[:extension_length].to_i.days
  end
end
