# frozen_string_literal: true

class Stafftools::Users::StaffAccessRequestsController < Stafftools::UsersController
  areas_of_responsibility :stafftools

  def create
    reason = params[:reason]

    if reason.blank?
      flash[:error] = "You must provide a reason for this impersonation."
    else
      request = this_user.staff_access_requests.create(
          reason: reason,
          requested_by: current_user,
        )

      if request
        StaffAccessMailer.impersonation_access_requested(request).deliver_later
        flash[:notice] = "Permission for staff to impersonate #{this_user} has been requested."
      end
    end

    redirect_to overview_stafftools_user_path(this_user)
  end

  def cancel
    begin
      current_request.cancel(current_user)
      flash[:notice] = "Request to impersonate #{this_user} has been cancelled."
    rescue StaffAccessRequest::RequestNotActive => e
      flash[:error] = e.message
    end

    redirect_to overview_stafftools_user_path(this_user)
  end

  private

  def current_request
    @current_request ||= StaffAccessRequest.find(params[:id])
  end
end
