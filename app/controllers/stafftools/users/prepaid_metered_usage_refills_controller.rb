# frozen_string_literal: true

class Stafftools::Users::PrepaidMeteredUsageRefillsController < StafftoolsController
  areas_of_responsibility :gitcoin

  layout "stafftools/organization/billing"

  before_action :require_prepaid_metered_billing
  before_action :enforce_plan_subscription_synchronized

  def index
    render_template_view(
      "stafftools/users/prepaid_metered_usage_refills/index",
      Stafftools::PrepaidMeteredUsageRefillsView,
      owner: this_user,
      page: current_page,
    )
  end

  def new
    prepaid_refill = Billing::PrepaidMeteredUsageRefill.new
    render "stafftools/users/prepaid_metered_usage_refills/new", locals: {
      prepaid_refill: prepaid_refill,
    }
  end

  def create
    prepaid_refill_creator = Billing::PrepaidMeteredUsageRefillCreator.new(
      prepaid_metered_usage_refill_params.merge(
        owner: this_user,
        expires_on: this_user.billed_on,
      ),
    )

    result = prepaid_refill_creator.create

    if result[:success]
      redirect_to stafftools_user_prepaid_metered_usage_refills_path(this_user), notice: "Successfully provisioned prepaid metered usage refill."
    else
      render "stafftools/users/prepaid_metered_usage_refills/new", locals: {
        prepaid_refill: result[:record],
      }
    end
  end

  def destroy
    refill = Billing::PrepaidMeteredUsageRefill.find_by!(id: params[:id], owner: this_user)

    if refill.staff_created?
      refill.destroy
      redirect_to stafftools_user_prepaid_metered_usage_refills_path(this_user)
    else
      render_404
    end
  end

  private

  def require_prepaid_metered_billing
    render_404 unless Billing::PrepaidMeteredUsageRefill.enabled_for?(this_user)
  end

  def enforce_plan_subscription_synchronized
    render "stafftools/users/prepaid_metered_usage_refills/not_synchronized" unless this_user.plan_subscription.present?
  end

  def prepaid_metered_usage_refill_params
    params.require(:billing_prepaid_metered_usage_refill).permit(:amount_in_subunits)
  end
end
