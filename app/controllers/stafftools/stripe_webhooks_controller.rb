# frozen_string_literal: true

class Stafftools::StripeWebhooksController < StafftoolsController
  areas_of_responsibility :gitcoin

  before_action :ensure_billing_enabled
  before_action :lookup_webhook, only: [:ignore, :perform]

  def index
    @webhooks = ::Billing::StripeWebhook.pending.ignoring_recent.order(:created_at)
    render "stafftools/stripe_webhooks/index", locals: { webhooks: @webhooks }
  end

  def ignore
    @webhook.ignored!
    flash[:success] = "Webhook id #{@webhook.id} permanently ignored"
    redirect_back(fallback_location: "stafftools/stripe_webhooks")
  end

  def perform
    begin
      @webhook.perform
      flash[:success] = "Webhook id #{@webhook.id} successfully processed"
    rescue RuntimeError, NoMethodError, Faraday::TimeoutError => e
      Failbot.report(e, webhook_id: @webhook.id)
      flash[:error] = e
    ensure
      redirect_back(fallback_location: "stafftools/stripe_webhooks")
    end
  end

  private

  def lookup_webhook
    @webhook = ::Billing::StripeWebhook.find(params[:stripe_webhook_id])
  rescue ActiveRecord::RecordNotFound => e
    Failbot.report(e, webhook_id: params[:stripe_webhook_id])
    flash[:error] = e
    redirect_back(fallback_location: "stafftools/stripe_webhooks")
  end
end
