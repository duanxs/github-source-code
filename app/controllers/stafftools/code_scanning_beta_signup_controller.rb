# frozen_string_literal: true

module Stafftools
  class CodeScanningBetaSignupController < StafftoolsController
    def index
      memberships = EarlyAccessMembership.
        preload(:member, :actor).
        order(created_at: :asc).
        code_scanning_waitlist.
        joins("INNER JOIN #{::User.table_name} on #{::User.table_name}.id = #{EarlyAccessMembership.table_name}.member_id")

      if params[:query].present?
        memberships = memberships.with_substring("#{::User.table_name}.login", params[:query].strip)
      end

      memberships = memberships.paginate(page: current_page, per_page: 100)

      # Find all the answers for the private repositories survey question
      private_repos_answers = Survey.find_by(slug: "code_scanning").
        questions.
        find_by(short_text: "private_repositories").
        answers.
        preload(:choice).
        where(user_id: memberships.map(&:member_id)).
        index_by(&:user_id)

      render "stafftools/code_scanning_beta_signup/index", locals: {
        memberships: memberships,
        private_repos_answers: private_repos_answers,
      }
    end

    def toggle_private_access(user, enable)
      if user.organization?
        if enable
          user.enable_advanced_security_private_beta
        else
          user.disable_advanced_security_private_beta
        end
      end
    end

    def toggle_public_access(user, enable)
      if enable
        user.enable_advanced_security_public_beta
      else
        user.disable_advanced_security_public_beta
      end
    end

    def toggle_access
      users = ::User.find(params[:user_ids])

      private_beta = params[:private].present?
      enable = params[:enable]

      # don't allow bulk toggling of private repos
      if users.size > 1 && private_beta
        return head :bad_request
      end

      users.each do |user|
        if private_beta
          toggle_private_access(user, enable)
        else
          toggle_public_access(user, enable)
        end
      end

      head :ok
    end
  end
end
