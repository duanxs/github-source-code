# frozen_string_literal: true

class Stafftools::IssueCommentsController < StafftoolsController
  areas_of_responsibility :code_collab, :issues, :pull_requests, :stafftools

  before_action :ensure_issue_exists, only: [:index, :first]
  before_action :ensure_comment_exists, only: [:show, :database]

  layout "stafftools/repository/collaboration"

  def index
    @comments = this_issue.comments.paginate \
      page: params[:page] || 1,
      per_page: 25
    render "stafftools/issue_comments/index"
  end

  def show
    fetch_audit_log_teaser "data.issue_comment_id:#{this_comment.id} OR (data.user_content_id:#{this_comment.id} AND data.user_content_type:#{this_comment.class} AND action:user_content_edit.*)"

    deliveries = Stafftools::UserNotificationDeliveries.for_thread(this_comment)

    notifications_view = Stafftools::RepositoryViews::NotificationsView.new(
      repository: this_comment.repository,
      params: {
        thread: Newsies::Thread.new("Issue", this_comment.try(:issue_id) || this_comment.id).key,
        comment: Newsies::Comment.to_key(this_comment),
      },
    )

    render("stafftools/issue_comments/show", locals: { notification_deliveries: deliveries, notifications_view: notifications_view })
  end

  def first
    fetch_audit_log_teaser "(data.issue_id:#{this_issue.id} _missing_:data.issue_comment_id) OR (data.user_content_id:#{this_issue.id} AND data.user_content_type:#{this_issue.class} AND action:user_content_edit.*)"
    deliveries = Stafftools::UserNotificationDeliveries.for_thread(this_issue)

    notifications_view = Stafftools::RepositoryViews::NotificationsView.new(
      repository: this_issue.repository,
      params: {
        thread: Newsies::Thread.new("Issue", this_issue.id).key,
        comment: Newsies::Comment.to_key(this_issue),
      },
    )
    render("stafftools/issue_comments/first", locals: { notification_deliveries: deliveries, notifications_view: notifications_view })
  end

  def database
    render "stafftools/issue_comments/database"
  end

  private

  def this_comment
    @this_comment ||= this_issue.comments.find_by_id(params[:id])
  end
  helper_method :this_comment

  def ensure_comment_exists
    return render_404 if this_comment.nil?
  end

end
