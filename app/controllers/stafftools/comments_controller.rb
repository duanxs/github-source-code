# frozen_string_literal: true

class Stafftools::CommentsController < StafftoolsController
  areas_of_responsibility :code_collab, :issues, :pull_requests, :stafftools

  before_action :ensure_user_exists, except: [:minimize, :unminimize, :destroy]

  def index
    @comments = Stafftools::RecentComments.for_user this_user

    case this_user.site_admin_context
    when "organization"
      render "stafftools/comments/index", layout: "stafftools/organization/collaboration"
    when "user"
      render "stafftools/comments/index", layout: "stafftools/user/collaboration"
    else
      render_404
    end
  end

  MinimizeCommentMutation = parse_query <<~'GRAPHQL'
    mutation($input: MinimizeCommentInput!) {
      minimizeComment(input: $input)
    }
  GRAPHQL

  def minimize
    return false unless site_admin?

    data = platform_execute(MinimizeCommentMutation, variables: {
      input: {
        subjectId: params[:comment_id],
        reason: params[:minimized_reason],
        classifier: params[:classifier],
        isStaffActor: true,
      },
    })

    if data.errors.all.any?
      message = data.errors.messages.values.join(", ")
      flash[:error] = message
    else
      flash[:notice] = "Comment was successfully minimized."
    end

    redirect_to :back
  end

  UnminimizeCommentMutation = parse_query <<~'GRAPHQL'
    mutation($input: UnminimizeCommentInput!) {
      unminimizeComment(input: $input)
    }
  GRAPHQL

  def unminimize
    return false unless site_admin?

    data = platform_execute(UnminimizeCommentMutation, variables: {
      input: {
        subjectId: params[:comment_id],
        reason: params[:minimized_reason],
        isStaffActor: true,
      },
    })

    if data.errors.all.any?
      message = data.errors.messages.values.join(", ")
      flash[:error] = message
    else
      flash[:notice] = "Comment was successfully unminimized."
    end

    redirect_to :back
  end

  DeleteCommentMutation = parse_query <<~'GRAPHQL'
    mutation($input: DeleteCommentInput!) {
      deleteComment(input: $input) {
        __typename
      }
    }
  GRAPHQL

  def destroy
    return false unless site_admin?

    data = platform_execute(DeleteCommentMutation, variables: {
      input: {
        id: params[:id],
      },
    })

    if data.errors.any?
      message = data.errors.messages.values.join(", ")
      flash[:error] = message
    else
      flash[:notice] = "Comment was deleted."
    end

    redirect_to stafftools_path
  end
end
