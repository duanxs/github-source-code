# frozen_string_literal: true

class Stafftools::RepositoryNotificationsController < StafftoolsController
  areas_of_responsibility :stafftools, :notifications

  before_action :ensure_repo_exists

  layout "stafftools/repository/collaboration"

  def index
    audit_log_query = [
      "data.name:Email",
      "data.hook_type:repo",
      "repo_id:#{current_repository.id}",
    ].join(" AND ")

    view = Stafftools::RepositoryViews::RepositoryNotificationsView.new(
      current_repository: current_repository,
      audit_log_data: fetch_audit_log_teaser(audit_log_query),
    )

    render "stafftools/repository_notifications/index", locals: {
      view: view,
    }
  end
end
