# frozen_string_literal: true

class Stafftools::DomainsController < StafftoolsController
  areas_of_responsibility :stafftools

  before_action :ensure_domain_verification_enabled
  before_action :ensure_user_exists
  before_action :ensure_org_not_user
  before_action :load_domain

  def show
    render "stafftools/users/domain", layout: false, locals: { domain: @domain }
  end

  def verify
    @domain.verify
    @domain.instrument_verify(current_user, true)

    if @domain.errors.any?
      flash[:error] = @domain.errors.full_messages.join(", ")
    else
      flash[:notice] = "You verified the domain #{@domain.domain}."
    end

    redirect_to domains_stafftools_user_path(@user)
  end

  def unverify
    if @domain.required_for_policy_enforcement?
      unless @domain.disable_dependent_policies(actor: current_user)
        @domains.errors.add(:base, "Could not unverify organization domain: failed to disable notification restrictions.")
      end
    end

    if @domain.errors.none?
      @domain.unverify
      @domain.instrument_unverify(current_user)
    end

    if @domain.errors.any?
      flash[:error] = @domain.errors.full_messages.join(", ")
    else
      flash[:notice] = "You unverified the domain #{@domain.domain}."
    end

    redirect_to domains_stafftools_user_path(@user)
  end

  private

  def load_domain
    @domain = typed_object_from_id([Platform::Objects::OrganizationDomain], params[:id])
  rescue Platform::Errors::NotFound
    render_404
  end
end
