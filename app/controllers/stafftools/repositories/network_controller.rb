# frozen_string_literal: true

class Stafftools::Repositories::NetworkController < Stafftools::RepositoriesController
  areas_of_responsibility :repo_networks, :stafftools

end
