# frozen_string_literal: true

class Stafftools::DependencyGraphController < StafftoolsController
  areas_of_responsibility :stafftools

  before_action :ghe_block_pages, except: [:show, :clear_org_dependencies, :redetect_org_dependencies]

  before_action :ensure_org_not_user, only: [:show, :clear_org_dependencies, :redetect_org_dependencies]

  BATCH_SIZE = 100

  def index
    render "stafftools/dependency_graph/index"
  end

  PackagesQuery = parse_query <<-'GRAPHQL'
    query($names: [String!], $packageManager: String, $limit: Int, $first: Int) {
      ...Views::Stafftools::DependencyGraph::Packages::Packages
    }
  GRAPHQL

  UnmappedPackagesQuery = parse_query <<-'GRAPHQL'
    query($names: [String!], $packageManager: String, $limit: Int, $first: Int) {
      ...Views::Stafftools::DependencyGraph::Packages::UnmappedPackages
    }
  GRAPHQL

  def package_results
    query_all = params[:query_all].eql?("true")

    begin
      if query_all
        data = platform_execute(PackagesQuery, variables: variables)
      else
        data = platform_execute(UnmappedPackagesQuery, variables: variables)
      end
    rescue DependencyGraph::Client::TimeoutError
      flash[:error] = "Dependency Graph is taking too long to respond, try again later..."
      redirect_to(action: :index)
      return
    end

    render "stafftools/dependency_graph/packages", locals: { data: data , query_all: query_all, query_name: params[:query_name], query_manager: params[:query_manager]}
  end

  ReassignPackageQuery = parse_query <<-'GRAPHQL'
    mutation($input: DependencyGraphReassignPackageInput!) {
      dependencyGraphReassignPackage(input: $input) {
        clientMutationId
      }
    }
  GRAPHQL

  def assign_package
    data = platform_execute(ReassignPackageQuery, variables: {
      input: {
        "packageManager" => params[:package_manager],
        "packageName" => params[:package_name],
        "targetRepository" => params[:target_repository],
        "clientMutationId" => request_id,
      },
    })

    if data.errors.all.any?
      reason = data.errors.messages.map { |field, messages| "#{field}: #{messages.join(", ")}" }.join("; ")
      flash[:error] = "Unable to assign #{params[:package_name]} to #{params[:target_repository]} (#{reason})"
    else
      flash[:notice] = "Package #{params[:package_name]} has been assigned to #{params[:target_repository]}"
    end

    redirect_to(action: :package_results, query_name: params[:query_name], query_manager: params[:query_manager], query_all: params[:query_all])
  end

  def show
    render "stafftools/dependency_graph/show",
      layout: "stafftools/organization/content",
      locals: {
        owner: this_user,
      }
  end

  def clear_org_dependencies
    ActiveRecord::Base.connected_to(role: :reading) do
      repos_to_clear = Repository.where(owner_id: this_user.id)

      repos_to_clear.in_batches(of: BATCH_SIZE) do |batch|
        batch.each do |repo|
          RepositoryDependencyClearDependencies.perform_later(repo.id) if repo.dependency_graph_enabled?
        end
      end
    end

    flash[:notice] = "Clear dependencies job enqueued for #{this_user.name}"
    redirect_to org_stafftools_dependency_graph_path(this_user)
  end

  def redetect_org_dependencies
    ActiveRecord::Base.connected_to(role: :reading) do
      repos_to_redetect = Repository.where(owner_id: this_user.id)

      repos_to_redetect.in_batches(of: BATCH_SIZE) do |batch|
        batch.each do |repo|
          RepositoryDependencyManifestInitializationJob.perform_later(repo.id) if repo.dependency_graph_enabled?
        end
      end
    end

    flash[:notice] = "Redetect dependencies job enqueued for #{this_user.name}"
    redirect_to org_stafftools_dependency_graph_path(this_user)
  end

  private
  def variables
    variables = {
      names: params[:query_name],
      packageManager: params[:query_manager],
      limit: 100,
      first: 100,
    }

    variables.reject! { |_k, v| v.blank? }
    variables
  end

  # Since GHES doesn't currently support storing Packages locally,
  # we'd like to hide this page from users in order to prevent confusion.
  def ghe_block_pages
    render_404 and return if GitHub.enterprise?
  end
end
