# frozen_string_literal: true

class Stafftools::EmailsController < StafftoolsController
  areas_of_responsibility :email, :stafftools

  before_action :ensure_user_exists
  before_action :dotcom_required, only: [:duplicates]
  before_action \
    :ensure_email_verification_enabled,
    only: [
      :disable_mandatory_email_verification,
      :restore_mandatory_email_verification,
    ]

  layout :new_nav_layout
  private def new_nav_layout
    case this_user.site_admin_context
    when "organization"
      "stafftools/organization/overview"
    when "user"
      "stafftools/user/overview"
    end
  end

  def index
    # ensure email verification actions redirect to staff land
    store_location
    render "stafftools/emails/index"
  end

  def duplicates
    @emails = GitHub::SpamChecker.find_obfuscated_duplicate_emails(this_user)
    render "stafftools/emails/duplicates"
  end

  # Adds an email to the user's account
  def create
    email = params[:email]

    if UserEmail.duplicate?(email)
      flash[:error] = "Error adding '#{email}' - email is already in use"
      redirect_to :back
      return
    end

    new_email = this_user.add_email(email, actor: current_user)
    if new_email.valid?
      flash[:notice] = "'#{new_email}' added to #{this_user}"
    else
      flash[:error] = "Error adding '#{new_email}' - #{new_email.errors.full_messages.to_sentence}."
    end
    redirect_to :back
  end

  # Deletes an email from the user's account.
  # For GitHub.com, if this is the last user-added email, an "unlinked"
  # replacement email is added before the email is deleted.
  def destroy
    email = this_user.emails.find(params[:id])
    message = "'#{email}' removed from #{this_user}".dup
    set_primary_email_status = User::SetPrimaryEmailStatus::SUCCESS

    if email.primary_role?
      replacement = fallback_for_primary_email(this_user)
      # If we have found no suitable replacement then put in a placeholder email.
      if !GitHub.enterprise? && !replacement
        replacement = this_user.add_email("#{email.email}.unlinked", actor: current_user)

        # Disable notifications for the user since the
        # account will essentially be abandoned
        this_user.disable_all_notifications
        instrument("staff.disable_notifications", user: this_user)
      end

      unless replacement
        flash[:error] = "Please add another email before deleting '#{email}'."
        return redirect_to :back
      end

      message << " and replaced with '#{replacement.email}'."

      # If we are making the user's backup email their primary we should
      # notify them that we have opted the in to the "Only allow primary"
      # backup email option. They will need to change their backup email
      # settings if they don't want that going forward.
      if replacement.backup_role?
        message << " User's backup email configuration is now set to 'Only allow primary email'."
      end
      # Set the replacement email as primary since we want to avoid some of
      # the rules enforced by the `UserEmail` model when primary emails are
      # deleted (ex. normally you can't delete a verified primary and have
      # that fallback to an unverfied email as their new priamry
      # email).
      set_primary_email_status = this_user.set_primary_email(replacement)
    end

    if set_primary_email_status.error?
      flash[:error] = set_primary_email_status.error
    elsif this_user.remove_email(email, actor: current_user)
      flash[:notice] = message
    else
      flash[:error] = this_user.errors.full_messages.to_sentence
    end
    redirect_to :back
  end

  def change_email_notifications
    val = params[:value]&.to_s

    if val == "true"
      response = GitHub.newsies.get_and_update_settings this_user do |settings|
        settings.participating_settings << "email"
        settings.subscribed_settings << "email"
      end
    else
      response = GitHub.newsies.get_and_update_settings this_user do |settings|
        settings.participating_settings.delete("email")
        settings.subscribed_settings.delete("email")
      end
    end

    if response.success?
      action = val == "true" ? "enable" : "disable"
      instrument("staff.#{action}_notifications", user: this_user)
      flash[:notice] = "Email notifications #{action}d."
    else
      flash[:error] = "Failed to toggle email notifications."
    end

    redirect_to :back
  end

  # Allow staff to mark a user as exempt from mandatory email verification.
  def disable_mandatory_email_verification
    this_user.disable_mandatory_email_verification(actor: current_user)
    flash[:notice] = "Mandatory email verification disabled for @#{this_user}."
    redirect_to :back
  end

  # Allow staff to restore mandatory email verification for a previously exempt user.
  def restore_mandatory_email_verification
    this_user.restore_mandatory_email_verification(actor: current_user)
    flash[:notice] = "Mandatory email verification restored for @#{this_user}."
    redirect_to :back
  end

  # Sets a specific email, for orgs
  def set_email
    this_user.update target_params
    redirect_to :back, notice: "Updated successfully."
  end

  # Fixes an account that has no primary email
  def repair_primary
    if this_user.repair_primary_email
      flash[:notice] = "Primary email set to #{this_user.primary_user_email}"
    else
      flash[:error] = "Error: #{this_user.errors.full_messages.to_sentence}"
    end

    redirect_to :back
  end

  # Fixes an invalid email
  def repair
    if (email = this_user.emails.find params[:id])
      email.update_attribute :email, "#{Time.now.to_i}@invalid.email.com"
      flash[:notice] = "Email has been repaired"
    else
      flash[:error] = "Email not found"
    end
    redirect_to :back
  end

  # Send the user an email verification request email
  #
  # email_id - The UserEmail id of the address to send to.
  def request_verification
    email = this_user.emails.find_by_email(params[:email]) || this_user.emails.unverified.first

    if email && email.request_verification
      flash[:notice] = "Verification Request sent to #{email}"
    elsif email
      flash[:error] = "Unable to send verification email to #{email}"
    else
      flash[:error] = "This user has no unverified emails"
    end

    redirect_to :back
  end

  def disable_marketing_email
    NewsletterPreference.set_to_transactional(user: this_user)
    flash[:notice] = "Disabled marketing email for #{this_user}"
    redirect_to :back
  end

  private

  def ensure_email_verification_enabled
    return render_404 unless GitHub.email_verification_enabled?
  end

  def fallback_for_primary_email(user)
    # We let the caller handle the case where this is the user's last email,
    # since several things need to happen (register a placeholder email, disable
    # notifications, etc).
    return nil if user.primary_user_email.last_email?
    # When a user has chosen to allow password resets only with their primary
    # email, we set their backup email to match their primary email. So, we
    # don't want to return their "backup email", since it is the same as the
    # user's primary email.
    if user.has_backup_email? && !user.password_reset_with_primary_email_only?
      return user.backup_user_email
    end
    # If the user doesn't have a backup set, then we search for the next
    # available "notifiable email". Notifiable emails prefer verified emails.
    # However, if no verified emails exist (notice we exclude the user's
    # primary/backup email before doing the search), then it falls over to the
    # first available unverified email.
    user.emails.excluding_ids([user.primary_user_email, user.backup_user_email]).notifiable.first
  end

  def target_params
    params.require(:target).permit(
      :profile_email,
      :billing_email,
      :gravatar_email,
    )
  end
end
