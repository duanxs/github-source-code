# frozen_string_literal: true

class Stafftools::AdminRedirectsController < StafftoolsController
  areas_of_responsibility :stafftools

  before_action :ensure_enabled

  def users
    redirect_to stafftools_users_path
  end

  def repositories
    redirect_to stafftools_repositories_path
  end

  def coupons
    redirect_to coupons_path
  end

  if Rails.development?
    skip_before_action :login_required, only: :fake_login
    skip_before_action :require_admin_frontend, only: :fake_login
    skip_before_action :ensure_enabled, only: :fake_login
    skip_before_action :sudo_filter, only: :fake_login

    def fake_login
      redirect_to stafftools_fake_login_path(id: params[:id])
    end
  end

  private

  def ensure_enabled
    redirect_to stafftools_path unless GitHub.admin_enabled?
  end
end
