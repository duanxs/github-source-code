# frozen_string_literal: true

class Stafftools::RepositoryDependabotController < Stafftools::RepositoriesController
  areas_of_responsibility :dependabot, :stafftools

  before_action :ensure_dependabot_available
  before_action :ensure_repo_exists

  layout "stafftools/repository/overview"

  def show
    response = Dependabot::Twirp.update_configs_client.list_update_configs(repository_id: current_repository.id)

    render "stafftools/repository_dependabot/show", locals: { update_configs: response.update_configs }
  rescue Dependabot::Twirp::ServiceUnavailableError
    render "stafftools/repository_dependabot/unavailable"
  rescue Dependabot::Twirp::Error => error
    render "stafftools/repository_dependabot/error", locals: { error: error }
  end

  private

  def ensure_dependabot_available
    render_404 unless GitHub.dependabot_enabled?
  end
end
