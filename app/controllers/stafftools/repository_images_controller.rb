# frozen_string_literal: true

class Stafftools::RepositoryImagesController < StafftoolsController
  areas_of_responsibility :repositories, :stafftools

  before_action :ensure_user_exists

  IndexQuery = parse_query <<-'GRAPHQL'
    query($owner: String!, $name: String!) {
      repository: staffAccessedRepository(owner: $owner, name: $name) {
        ...Views::Stafftools::RepositoryImages::Index::Repository
      }
    }
  GRAPHQL

  def index
    result = platform_execute(IndexQuery, variables: {
      owner: params[:user_id],
      name: params[:repository_id],
    })

    render "stafftools/repository_images/index",
      locals: { repository: result.repository }, layout: "stafftools/repository/overview"
  end

  DestroyQuery = parse_query <<-'GRAPHQL'
    mutation($input: DeleteRepositoryImageInput!) {
      deleteRepositoryImage(input: $input) {
        repository
      }
    }
  GRAPHQL

  def destroy
    image = RepositoryImage.find(params[:id])
    data = platform_execute(DestroyQuery, variables: { input: { id: image.global_relay_id } })

    if data.errors.any?
      error_type = data.errors.details[:deleteRepositoryImage]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      flash[:error] = "Error deleting image: " + data.errors.messages.values.join(", ")
    else
      flash[:notice] = "Successfully deleted image"
    end

    redirect_to stafftools_repository_images_path(params[:user_id], params[:repository_id])
  end
end
