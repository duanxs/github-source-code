# frozen_string_literal: true

class Stafftools::ReceiptsController < StafftoolsController
  areas_of_responsibility :stafftools

  before_action :ensure_billing_enabled
  before_action :ensure_transaction_exists, only: [:create]

  def create
    if transaction.success?
      if BillingNotificationsMailer.receipt(transaction.user, transaction).deliver_later
        flash[:notice] = "Receipt sent to #{transaction.user.billing_email}"
      else
        flash[:error] = "Failed to send receipt to #{transaction.user.billing_email}"
      end
    else
      flash[:error] = "Won’t sent a receipt for a failed transaction."
    end

    redirect_to return_path
  end

  def show
    @receipt = ::Billing::Receipt.new(transaction)

    respond_to do |format|
      format.text do
        render "mailers/billing_notifications/receipt"
      end
      format.pdf do
        send_data(@receipt.to_pdf(show_email_address: false),
          filename: @receipt.pdf_filename,
          type: "application/pdf",
          disposition: "inline",
        )
      end
      format.html do
        send_data(@receipt.to_pdf(show_email_address: false),
          filename: @receipt.pdf_filename,
          type: "application/pdf",
          disposition: "attachment",
        )
      end
    end
  end

  private

  def transaction
    id = params[:id]
    @transaction ||= Billing::BillingTransaction.find_by_transaction_id(id)
  end

  def ensure_transaction_exists
    render_404 unless transaction
  end

  def return_path
    if this_user.present?
      :back
    else
      stafftools_billing_transaction_path(transaction_id: params[:id])
    end
  end
end
