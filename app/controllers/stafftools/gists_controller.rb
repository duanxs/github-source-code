# frozen_string_literal: true

class Stafftools::GistsController < StafftoolsController
  areas_of_responsibility :stafftools, :gist

  before_action :dotcom_required,
    only: [:dmca_takedown, :dmca_restore, :country_block, :remove_country_block]
  before_action :ensure_user_exists, except: [:show, :destroy, :restore]

  def index
    gists = this_user.gists.are_public.order("gists.delete_flag, gists.created_at DESC").paginate page: current_page

    render_template_view "stafftools/gists/index", Stafftools::User::GistsView,
      { gists: gists, user: this_user },
      layout: "stafftools/user/content"
  end

  def archived
    gists = Archived::Gist.where(user_id: this_user.id).paginate page: current_page
    render_template_view "stafftools/gists/archived", Stafftools::User::GistsView,
      { gists: gists, user: this_user, gist_type: "archived" },
      layout: "stafftools/user/content"
  end

  def show
    layout = "stafftools/user/content"
    layout = "stafftools/anonymous_gist" if this_or_archived_gist.anonymous?
    render_template_view "stafftools/gists/show", Stafftools::User::GistView,
      { gist: this_or_archived_gist },
      layout: layout
  end

  def restore
    archived_gist = this_or_archived_gist

    if archived_gist.is_a?(Archived::Gist)
      restored_gist = Gist.restore(archived_gist.id)
      instrument "staff.restore_gist", restored_gist.event_payload

      redirect_to stafftools_user_gist_path(restored_gist.user_param, restored_gist)
    else
      redirect_to :back
    end
  end

  def dmca_takedown
    url = params["takedown_url"]

    if this_gist.access.dmca_takedown(current_user, url)
      flash[:notice] = "Takedown processed"
    else
      flash[:error] = this_gist.errors[:base].to_sentence
    end

    redirect_to :back
  end

  def dmca_restore
    if !this_gist.access.dmca?
      flash[:notice] = "No takedown notice on file"
    elsif this_gist.access.enable(current_user)
      flash[:notice] = "Takedown removed"
    else
      flash[:error] = "Failed to remove DMCA takedown"
    end

    redirect_to :back
  end

  def country_block
    country = params["country_block"]
    url = params["country_block_url"]

    if this_gist.access.country_block(current_user, country, url)
      flash[:notice] = "Country block processed. This may take up to ten minutes to take effect."
    else
      flash[:error] = this_gist.errors[:base].to_sentence
    end

    redirect_to :back
  end

  def remove_country_block
    country = params["country_block"]

    if !this_gist.access.country_block_setup?(country)
      flash[:notice] = "Given country block not registered"
    elsif this_gist.access.remove_country_block(current_user, country)
      flash[:notice] = "Country block removed. This may take up to ten minutes to take effect."
    else
      flash[:error] = "Failed to remove country block"
    end

    redirect_to :back
  end

  def destroy
    this_gist.remove

    instrument "staff.delete_gist", this_gist.event_payload

    flash[:notice] = "Gist deleted"

    redirect_to stafftools_path
  end

  def schedule_backup
    this_gist.async_backup
    flash[:notice] = "Gist backup job scheduled"
    redirect_to :back
  end

  def mark_as_broken
    this_gist.mark_as_broken
    flash[:warn] = "This gist has been marked as broken. No further maintenance jobs will be run for it."
    redirect_to :back
  end

  def schedule_maintenance
    this_gist.update_status :scheduled, last_maintenance_attempted_at: Time.now
    GistMaintenanceJob.set(queue: this_gist.maintenance_queue_name).perform_later(this_gist.id)
    flash[:notice] = "Gist maintenance job enqueued"
    redirect_to :back
  end

  private

  def this_gist
    @gist ||= if this_user
      this_user.gists.find_by_repo_name! params[:id]
    else
      Gist.anonymous.find_by_repo_name! params[:id]
    end
  end

  def this_or_archived_gist
    return @gist_or_archived if defined?(@gist_or_archived)

    @gist_or_archived = this_gist
  rescue ActiveRecord::RecordNotFound => e
    @gist_or_archived = Archived::Gist.where(repo_name: params[:id]).first!
  end
end
