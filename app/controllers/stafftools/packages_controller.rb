# frozen_string_literal: true

class Stafftools::PackagesController < StafftoolsController
  areas_of_responsibility :stafftools

  before_action :ensure_user_exists

  layout "stafftools/repository/overview"

  def index
    render "stafftools/packages/index",
      layout: "stafftools/organization/content",
      locals: {
        owner: this_user,
        packages: this_user.packages,
      }
  end
end
