# frozen_string_literal: true

class Stafftools::GpgKeysController < StafftoolsController
  areas_of_responsibility :stafftools

  before_action :ensure_user_exists
  before_action :ensure_gpg_key_exists, only: [:show, :database]

  layout "stafftools/user/security"

  def index
    fetch_audit_log_teaser "user_id:#{this_user.id} AND action:gpg_key.*"
    render "stafftools/gpg_keys/index"
  end

  def show
    key = this_key.subkey? ? this_key.primary_key : this_key
    fetch_audit_log_teaser "data.key_id:#{key.hex_key_id}"
    render "stafftools/gpg_keys/show"
  end

  def database
    render "stafftools/gpg_keys/database"
  end

  private

  def this_key
    @this_key = this_user.gpg_keys.find(params[:id])
  end
  helper_method :this_key

  def ensure_gpg_key_exists
    return render_404 if this_key.nil?
  end

end
