# frozen_string_literal: true

module Stafftools
  module Businesses
    class MeteredExportsController < BusinessBaseController
      areas_of_responsibility :gitcoin

      def index
        render "stafftools/businesses/metered_exports/index", locals: {
          metered_exports: this_business.metered_usage_exports.preload(:requester).order(created_at: :desc),
        }
      end

      def show
        export = this_business.metered_usage_exports.find(params[:id])

        redirect_to export.generate_expiring_url
      end

      private

      def nav_layout
        case this_user.site_admin_context
        when "organization"
          "stafftools/organization/billing"
        when "user"
          "stafftools/user/billing"
        end
      end
    end
  end
end
