# frozen_string_literal: true
#
module Stafftools
  module Businesses
    class DebugToolsController < BusinessBaseController
      def show
        render "stafftools/businesses/debug_tools/show", locals: {
          this_business: this_business,
          view: Stafftools::Businesses::DebugToolsView.new(business: this_business),
        }
      end

      def sync_billing
        organization = this_business.organizations.find_by(login: params[:organization_login])

        BusinessOrganizationBillingJob.perform_later(this_business, organization)
        flash[:notice] = "Enqueued billing sync job for #{organization.login}"
        redirect_to stafftools_enterprise_debug_tools_path(this_business)
      end
    end
  end
end
