# frozen_string_literal: true

class Stafftools::Businesses::PrepaidMeteredUsageRefillsController < Stafftools::Businesses::BusinessBaseController
  areas_of_responsibility :gitcoin

  before_action :enforce_refills_enabled
  before_action :enforce_plan_subscription_synchronized

  def index
    render_template_view(
      "stafftools/businesses/prepaid_metered_usage_refills/index",
      Stafftools::PrepaidMeteredUsageRefillsView,
      owner: this_business,
      page: current_page,
    )
  end

  def new
    prepaid_refill = Billing::PrepaidMeteredUsageRefill.new
    render "stafftools/businesses/prepaid_metered_usage_refills/new", locals: {
      prepaid_refill: prepaid_refill,
    }
  end

  def create
    prepaid_refill_creator = Billing::PrepaidMeteredUsageRefillCreator.new(
      prepaid_metered_usage_refill_params.merge(
        owner: this_business,
        expires_on: this_business.billed_on,
      ),
    )

    result = prepaid_refill_creator.create

    if result[:success]
      redirect_to stafftools_prepaid_metered_usage_refills_path(this_business), notice: "Successfully provisioned prepaid metered usage refill."
    else
      render "stafftools/businesses/prepaid_metered_usage_refills/new", locals: {
        prepaid_refill: result[:record],
      }
    end
  end

  def destroy
    refill = Billing::PrepaidMeteredUsageRefill.find_by!(id: params[:id], owner: this_business)

    if refill.staff_created?
      refill.destroy
      redirect_to stafftools_prepaid_metered_usage_refills_path(this_business)
    else
      render_404
    end
  end

  private

  def enforce_refills_enabled
    render "stafftools/businesses/prepaid_metered_usage_refills/disabled" unless Billing::PrepaidMeteredUsageRefill.enabled_for?(this_business)
  end

  def enforce_plan_subscription_synchronized
    render "stafftools/businesses/prepaid_metered_usage_refills/not_synchronized" unless this_business&.customer&.sales_serve_plan_subscription.present?
  end

  def prepaid_metered_usage_refill_params
    params.require(:billing_prepaid_metered_usage_refill).permit(:amount_in_subunits)
  end
end
