# frozen_string_literal: true

module Stafftools
  module Businesses
    class EnterpriseAgreementsController < BusinessBaseController
      areas_of_responsibility :gitcoin

      def new
        enterprise_agreement = this_business.enterprise_agreements.new
        render_form(
          enterprise_agreement: enterprise_agreement,
          form_url: stafftools_enterprise_agreements_path(this_business),
        )
      end

      def edit
        enterprise_agreement = this_business.enterprise_agreements.find(params[:id])
        render_form(
          enterprise_agreement: enterprise_agreement,
          form_url: stafftools_enterprise_agreement_path(this_business, enterprise_agreement),
        )
      end

      def create
        enterprise_agreement = this_business.enterprise_agreements.new(enterprise_agreement_params)

        if enterprise_agreement.save
          flash[:notice] = "Enterprise agreement updated."
          redirect_to stafftools_enterprise_path(this_business)
        else
          flash[:error] = "Failed to update enterprise agreement. (#{enterprise_agreement.errors.full_messages.to_sentence})"
          render_form(
            enterprise_agreement: enterprise_agreement,
            form_url: stafftools_enterprise_agreements_path(this_business),
          )
        end
      end

      def update
        enterprise_agreement = this_business.enterprise_agreements.find(params[:id])

        if enterprise_agreement.update(enterprise_agreement_params)
          flash[:notice] = "Enterprise agreement updated."
          redirect_to stafftools_enterprise_path(this_business)
        else
          flash[:error] = "Failed to update enterprise agreement. (#{enterprise_agreement.errors.full_messages.to_sentence})"
          render_form(
            enterprise_agreement: enterprise_agreement,
            form_url: stafftools_enterprise_agreement_path(this_business, enterprise_agreement),
          )
        end
      end

      private

      def render_form(locals)
        render "stafftools/businesses/enterprise_agreements/form", locals: locals.reverse_merge(
          this_business: this_business,
          categories: Billing::EnterpriseAgreement::CATEGORIES,
          statuses: Billing::EnterpriseAgreement::STATUSES,
        )
      end

      def enterprise_agreement_params
        params.require(:enterprise_agreement).permit(
          :agreement_id,
          :category,
          :seats,
          :status,
          :azure_subscription_id,
        )
      end
    end
  end
end
