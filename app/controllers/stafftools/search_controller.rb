# frozen_string_literal: true

class Stafftools::SearchController < StafftoolsController
  areas_of_responsibility :search, :stafftools

  def search
    respond_to do |format|
      format.html do
        searcher = ::Stafftools::Searcher.new(params[:query], current_user)

        if searcher.query.blank?
          redirect_to stafftools_path
          return
        end

        if app = searcher.search_for_oauth_application_by_key.results.oauth_application
          redirect_to stafftools_user_application_path(app.user, app.id)
        end

        if access = searcher.search_for_oauth_access_token.results.oauth_access
          redirect_to stafftools_user_oauth_token_path(access.user, access.id)
        end

        if key = searcher.search_for_public_key.results.public_key
          if key.repository
            # Found a deploy key.
            redirect_to gh_stafftools_repository_path(key.repository)
          else
            redirect_to stafftools_user_path(key.user)
          end

          # Return to stop execution and prevent ElasticSearch errors searching
          # for something that looks like a key fingerprint.
          return
        end

        searcher.search_for_gpg_key
        keys = searcher.results.gpg_keys
        if keys && keys.length == 1
          redirect_to gh_stafftools_user_gpg_key_path(keys.first)
          return
        end

        searcher.search_for_users
        users = searcher.results.users
        deleted_users = searcher.results.deleted_users

        searcher.search_for_businesses
        businesses = searcher.results.businesses

        searcher.search_for_hook
        hook = searcher.results.hook

        if users.empty? && deleted_users.empty?
          searcher.search_for_renamed_users
          renamed_user = searcher.results.renamed_user
        end

        searcher.search_for_repositories
                .search_for_gists
                .search_for_oauth_application
                .search_for_integrations
                .search_for_teams

        repositories = searcher.results.repositories
        gists = searcher.results.gists
        oauth_apps = searcher.results.oauth_apps
        integrations = searcher.results.integrations
        integration_installations = searcher.results.integration_installations
        teams = searcher.results.teams

        # There are some situations where we can take a user straight to the only
        # possible result, lets do it if we can.
        forwardable_results_size = users.length + repositories.length + gists.length + businesses.length + teams.length
        forwardable_result = deleted_users.empty? && oauth_apps.empty? && renamed_user.nil? && integrations.empty? && integration_installations.empty? && hook.nil? && forwardable_results_size == 1

        if forwardable_result
          redirect_to stafftools_user_path(users.first.login) if users.length == 1
          redirect_to gh_stafftools_repository_path(repositories.first) if repositories.length == 1 && !repositories.first.owner.nil?
          redirect_to stafftools_user_gist_path(gists.first.user_param, gists.first) if gists.length == 1
          redirect_to stafftools_enterprise_path(businesses.first) if businesses.length == 1
          redirect_to gh_stafftools_team_path(teams.first) if teams.length == 1
        end

        render "stafftools/search/search", locals: { results: searcher.results, query: searcher.query } unless performed?
      end

      format.json do
        render json: { error: "Not Found" }, status: :not_found
      end
    end
  end

  def audit_log
    if query = params[:query]
      instrument("staff.search_audit_log", query: query)
    else
      instrument("staff.view_audit_log")
    end

    respond_to do |format|
      format.html do
        @page = params[:page]
        @page = 1 unless @page.present?
        @current_user = current_user
        @after = params[:after] || ""
        @before = params[:before] || ""

        es_query = Audit::Driftwood::Query.new_stafftools_query(
          phrase: filtered_query_string,
          current_user: current_user,
          page: @page,
          after: @after,
          before: @before,
        )
        @results = es_query.execute
        @logs = AuditLogEntry.new_from_array(@results)
        collect_audit_log_stats(@logs)

        render "stafftools/search/audit_log"
      end

      format.json do
        es_query = Audit::Driftwood::Query.new_stafftools_query(
          phrase: filtered_query_string,
          current_user: current_user,
          per_page: 5000,
        )
        results = es_query.execute

        unless params[:raw]
          results = AuditLogEntry.new_from_array(results)
          collect_audit_log_stats(results)
          results = results.map { |log| log.metadata(time_zone: ActiveSupport::TimeZone["UTC"]) }
        end

        send_data results.to_json, type: :json, disposition: "attachment"
      end
    end
  end

  def audit_log_advanced_search
    render "stafftools/search/audit_log_advanced_search"
  end

  def cname
    query = (params[:query] || "").strip.
    gsub(/^(http(s)*:\/\/)*(?<domain>.[^\/]*)(\/)*/, '\k<domain>')
    if query.blank?
      redirect_to stafftools_path
      return
    end

    page = Page.find_by_cname(query)
    if page && page.repository
      redirect_to stafftools_repository_pages_path(page.repository)
    elsif page
      flash[:error] = "Invalid page record (ID: #{page.id}) found for '#{query}'."
      redirect_to stafftools_path
    else
      flash[:error] = "No pages found for '#{query}'."
      redirect_to stafftools_path
    end
  end

  def user_assets
    url = (params[:query] || "").strip
    result = AssetScanner.check_url(url)

    if result.success?
      if asset = UserAsset.where(guid: result.match.asset_guid).first
        redirect_to stafftools_user_asset_path(asset)
      else
        flash[:error] = "No image attachment found with GUID #{result.match.asset_guid}."
        redirect_to stafftools_path
      end
    else
      flash[:error] = "Invalid image attachment URL."
      redirect_to stafftools_path
    end
  end

  private

  def filtered_query_string
    @query_string = params[:query]
    @query_string.blank? ? "@timestamp:>#{2.days.ago.strftime('%Y-%m-%d')}" : @query_string
  end

  def collect_audit_log_stats(results)
    return if results.empty?
    last = results.min { |a, b| a.at_timestamp <=> b.at_timestamp }
    log_date = last.at_timestamp
    today = Time.now

    # Calculate how old the last result in the view is
    months_ago = (today.year * 12 + today.month) - (log_date.year * 12 + log_date.month)
    GitHub.dogstats.histogram("audit_log_results.months_old", months_ago, tags: [])
  end
end
