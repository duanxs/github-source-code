# frozen_string_literal: true

class Stafftools::CommitCommentsController < StafftoolsController
  before_action :ensure_comment_exists, only: [:show]

  layout "stafftools/repository/collaboration", except: :destroy

  def show
    render "stafftools/commit_comments/show"
  end

  def destroy
    comment = CommitComment.find_by_id(params[:comment_id])
    comment.destroy

    head 200
  end

  def this_comment
    @this_comment ||= CommitComment.find_by_id(params[:id])
  end
  helper_method :this_comment

  def this_user
    @this_user ||= this_comment.try(:user)
  end
  helper_method :this_user

  def current_repository
    return @current_repository if defined?(@current_repository)
    @current_repository = this_comment.try(:repository)
  end
  helper_method :current_repository

  def ensure_comment_exists
    return render_404 if this_comment.nil?
  end
end
