# frozen_string_literal: true

module Stafftools
  class CodespacesBetaSignupController < StafftoolsController
    def index
      memberships = EarlyAccessMembership.
        preload(:member, :actor).
        order(created_at: :asc).
        workspaces_waitlist


      if params[:query].present?
        memberships = memberships.
          joins("INNER JOIN users on users.id = early_access_memberships.member_id").where("users.login = ?", params[:query].strip)
      end

      memberships = memberships.paginate(page: current_page, per_page: 100)

      render "stafftools/codespaces_beta_signup/index", locals: {
        memberships: memberships,
      }
    end

    def toggle_access
      user = ::User.find(params[:user_id])

      if user.workspaces_enabled?
        user.offboard_from_codespaces_beta!
      else
        user.onboard_into_codespaces_beta!
      end

      head :ok
    end
  end
end
