# frozen_string_literal: true

class Stafftools::SponsorsController < StafftoolsController
  before_action :sponsors_required
  before_action :membership_required
  before_action :listing_required

  protected

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def this_listing
    @this_listing ||= this_sponsorable&.sponsors_listing
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def this_membership
    @this_membership ||= this_sponsorable&.sponsors_membership
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def this_sponsorable
    sponsorable_id = params[:member_id] || params[:id]
    return unless sponsorable_id.present?
    @this_sponsorable ||= User.find_by(login: sponsorable_id)
  end

  def listing_required
    render_404 unless this_listing.present?
  end

  def membership_required
    render_404 unless this_membership.present?
  end

  def stripe_connect_account_required
    render_404 unless this_listing&.stripe_connect_account.present?
  end
end
