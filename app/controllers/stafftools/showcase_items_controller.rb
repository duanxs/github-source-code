# frozen_string_literal: true

class Stafftools::ShowcaseItemsController < StafftoolsController
  include ShowcaseItemsControllerMethods

  areas_of_responsibility :explore, :stafftools

  before_action :showcase_disabled
  before_action :find_collection, except: [:perform_healthcheck]
  before_action :find_item, except: [:create, :perform_healthcheck]
  skip_before_action :verify_authenticity_token, only: [:perform_healthcheck]

  def create
    create_item(:stafftools)
  end

  def update
    update_item(:stafftools)
  end

  def edit
    edit_item(:stafftools)
  end

  def destroy
    destroy_item(:stafftools)
  end
end
