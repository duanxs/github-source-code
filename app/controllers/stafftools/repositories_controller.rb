# frozen_string_literal: true

class Stafftools::RepositoriesController < StafftoolsController
  areas_of_responsibility :stafftools

  map_to_service :repo_info, only: [:index, :database, :disk, :overview, :languages, :show, :topics, :analyze_language]
  map_to_service :repo_deletion, only: [:destroy, :purge_repository]
  map_to_service :repo_state, only: [:lock, :lock_for_migration, :unlock, :archive, :unarchive]
  map_to_service :branch_protection_rule, only: [:change_allow_force_push]

  before_action :ensure_repo_exists, except: [:index]
  before_action :enterprise_required, only: [:index]

  layout :new_nav_layout
  private def new_nav_layout
    case action_name
    when "index"
      "stafftools"
    when "security", "collaborators", "permissions", "deploy_keys"
      "stafftools/repository/security"
    when "issues", "notifications", "events", "alerts", "funding_links", "abuse_reports"
      "stafftools/repository/collaboration"
    when "disk", "releases", "uploads"
      "stafftools/repository/storage"
    else
      "stafftools/repository/overview"
    end
  end

  def index
    index_view = Stafftools::RepositoryViews::IndexView.new(filter: params[:filter], page: current_page)
    repos = index_view.repositories_query
    render "stafftools/repositories/index", locals: { view: index_view, repos: repos }
  end

  def database
    render "stafftools/repositories/database"
  end

  def deploy_keys
    render "stafftools/repositories/deploy_keys"
  end

  def disk
    render "stafftools/repositories/disk"
  end

  def overview
    view = Stafftools::RepositoryViews::ShowView.new repository: current_repository

    ActiveSupport::Notifications.instrument "statistics.memory", data: memory_data do
      render "stafftools/repositories/overview", locals: { view: view }
    end
  end

  def releases
    render "stafftools/repositories/releases"
  end

  def search
    render "stafftools/repositories/search"
  end

  def security
    render "stafftools/repositories/security"
  end

  def uploads
    render "stafftools/repositories/uploads"
  end

  def languages
    render "stafftools/repositories/languages"
  end

  def show

    @counts = {
      releases: current_repository.releases.size,
      uploads: current_repository.downloads.size,
      collabs: current_repository.members.size,
      deploy_keys: current_repository.public_keys.size,
      discussions: current_repository.discussions.size,
      hooks: current_repository.hooks.size,
      network_repos: current_repository.network.repositories.size,
      children: current_repository.children.size,
      issues: current_repository.issues.size,
      pull_requests: current_repository.pull_requests.size,
      projects: current_repository.projects.size,
      protected_branches: current_repository.protected_branches.size,
      invites: current_repository.repository_invitations.size,
    }
    if current_repository.fork?
      @counts[:siblings] = current_repository.parent.children.size - 1
    end

    @error_states = {
      billing: (current_repository.disabled? if GitHub.billing_enabled?),
      disabled: current_repository.access.disabling_reason,
      disk: current_repository.on_disk_status,
      fileserver: current_repository.fileserver_status,
      inactive: !current_repository.active?,
    }

    if current_repository.page
      @last_pages_build = current_repository.page.builds.first
    end

    render "stafftools/repositories/show", layout: "stafftools/repository"
  end

  # View a repo's events (the dashboard feed items)
  def events
    @page_param = params[:events_page]
    render "stafftools/repositories/events"
  end

  def funding_links
    return render_404 unless GitHub.sponsors_enabled?

    render "stafftools/repositories/funding_links", locals: {
      funding_links_disabled: current_repository.funding_links_stafftools_disabled?,
    }
  end

  def funding_links_disable
    return render_404 unless GitHub.sponsors_enabled?

    flash[:notice] = if current_repository.funding_links_stafftools_disabled?
      GitHub.kv.del(current_repository.funding_links_stafftools_kv_prefix)
      "Repository sponsor button re-enabled."
    else
      GitHub.kv.set(current_repository.funding_links_stafftools_kv_prefix, "1")
      "Repository sponsor button disabled."
    end

    redirect_to :back
  end

  AbuseReportsQuery = parse_query <<-'GRAPHQL'
    query(
      $owner: String!,
      $name: String!,
      $first: Int,
      $after: String,
      $last: Int,
      $before: String
    ) {
      repository(owner: $owner, name: $name) {
        ...Views::Stafftools::Repositories::AbuseReports::Repository
      }
    }
  GRAPHQL

  def abuse_reports
    return render_404 unless GitHub.can_report?

    variables = {
      owner: current_repository.owner.login,
      name: current_repository.name,
    }.merge(graphql_pagination_params(page_size: 50))

    data = platform_execute(AbuseReportsQuery, variables: variables)
    return render_404 unless data.repository.present?

    render "stafftools/repositories/abuse_reports",
      locals: { current_repository: current_repository,
                graphql_repo: data.repository,
                report_content_enabled: current_repository.tiered_reporting_explicitly_enabled? }
  end

  TopicsQuery = parse_query <<-'GRAPHQL'
    query($name: String!, $owner: String!) {
      repository: staffAccessedRepository(name: $name, owner: $owner) {
        ...Views::Stafftools::Repositories::Topics::Repository
      }
    }
  GRAPHQL

  def topics
    data = platform_execute(TopicsQuery,
      variables: {owner: params[:user_id], name: params[:id]},
    )

    render "stafftools/repositories/topics", locals: {repository: data.repository}
  end

  # View the notifications a repo has generated
  # If no "user" param is present, user will be nil causing the view to display all notifications
  def notifications
    notification_params = params.to_unsafe_h.with_indifferent_access

    user = User.find_by_id(notification_params[:user].to_i)
    notifications_view = Stafftools::RepositoryViews::NotificationsView.new(
      repository: current_repository, request: request, params: notification_params, user: user)
    render "stafftools/repositories/notifications", locals: { view: notifications_view }
  end

  def collaboration
    # Collaboration is dead! Long live collaboration!
    if current_repository.organization
      redirect_to gh_permissions_stafftools_repository_path(current_repository)
    else
      redirect_to gh_stafftools_repository_collaborators_path(current_repository)
    end
  end

  def permissions
    if (@org = current_repository.organization)
      @abilities = Hash.new { |h, k| h[k] = current_repository.async_most_capable_action_or_role_for(k).sync }

      collab_abilities = Ability.where(
        actor_type: "User",
        subject_id: current_repository.id,
        subject_type: "Repository",
        priority: Ability.priorities[:direct],
      ).pluck(:actor_id)

      team_abilities = Ability.teams_direct_on_repos(
        repo_id: current_repository.id,
      ).pluck(:actor_id)

      user_ids = Team.members_of(team_abilities, immediate_only: false).pluck(:id)
      user_ids += collab_abilities
      user_ids += current_repository.organization.admin_ids

      @users =
        User.where(id: user_ids.uniq)
        .order("login ASC")
        .paginate(page: params[:page])

      if params[:username]
        @users = @users.where(login: params[:username])
      end

      render "stafftools/repositories/permissions"
    else
      redirect_to gh_stafftools_repository_collaborators_path(current_repository)
    end
  end

  # Make this repo go away forever
  def destroy
    current_repository.remove(current_user)
    flash[:notice] = "Repository deleted"
    redirect_to stafftools_path
  end

  # Billing-lock this repo
  def lock
    current_repository.lock_for_billing
    flash[:notice] = "Repository locked"
    redirect_to :back
  end

  # Migration-lock this repo
  def lock_for_migration
    current_repository.lock_for_migration
    flash[:notice] = "Repository locked for migration"
    redirect_to :back
  end

  # Billing-unlock this repo
  def unlock
    current_repository.unlock_including_descendants!
    flash[:notice] = "Repository unlocked"
    redirect_to :back
  end

  # Queue up a fsck job for this repo
  def fsck
    current_repository.async_fsck
    flash[:notice] = "fsck job enqueued"
    redirect_to :back
  end

  def wiki_restore
    current_repository.storage_adapter.restore_wiki
    flash[:notice] = "wiki restored from backup!"
    redirect_to :back
  end

  def pause_repo_invite_limit
    RepositoryInvitationRateLimitOverride.override!(current_repository.id)
    flash[:notice] = "Repository invitation limit overridden for the next 24 hours."
    redirect_to :back
  end

  # Reindex the repo's metadata for search
  def reindex_repository
    current_repository.reindex_repository
    flash[:notice] = "Reindexing #{current_repository.name_with_owner} ..."
    redirect_to :back
  end

  def purge_repository
    current_repository.purge_repository
    flash[:notice] = "Purging #{current_repository.name_with_owner} from the search index ..."
    redirect_to :back
  end

  # Reindex the repo's code for search
  def reindex_code
    current_repository.reindex_code
    flash[:notice] = "Deleted existing documents and enqueued a code reindexing job"
    redirect_to :back
  end

  def purge_code
    current_repository.purge_code
    flash[:notice] = "Purging source code from the search index ..."
    redirect_to :back
  end

  def enable_code_search
    current_repository.code_search_enabled = true
    current_repository.save!
    Search.add_to_search_index("code", current_repository.id)

    flash[:notice] = "Code search has been enabled and is now being indexed."
    redirect_to :back
  end

  UpdateNetworkPrivilegeMutation = parse_query <<~'GRAPHQL'
    mutation($input: UpdateNetworkPrivilegeInput!) {
      updateNetworkPrivilege(input: $input) {
        repository
      }
    }
  GRAPHQL

  def hide_from_google
    platform_execute(UpdateNetworkPrivilegeMutation, variables: {
      input: {
        repositoryId: current_repository.global_relay_id,
        noIndex: params[:no_index] == "1",
      },
    })
    flash[:notice] = "Repository #{current_repository} network privileges updated."
    redirect_to :back
  end

  def hide_from_discovery
    res = platform_execute(UpdateNetworkPrivilegeMutation, variables: {
      input: {
        repositoryId: current_repository.global_relay_id,
        isHiddenFromDiscovery: params[:hide_from_discovery] == "1",
      },
    })

    if res.errors.any?
      flash[:error] = "There was an error updating the network privileges."
    else
      flash[:notice] = "Repository #{current_repository} network privileges updated."
    end
    redirect_to :back
  end

  def require_login
    platform_execute(UpdateNetworkPrivilegeMutation, variables: {
      input: {
        repositoryId: current_repository.global_relay_id,
        requireLogin: params[:require_login] == "1",
      },
    })
    flash[:notice] = "Repository #{current_repository} network privileges updated."
    redirect_to :back
  end

  def require_opt_in
    res = platform_execute(UpdateNetworkPrivilegeMutation, variables: {
      input: {
        repositoryId: current_repository.global_relay_id,
        requireOptIn: params[:require_opt_in] == "1",
      },
    })
    if res.errors.any?
      flash[:error] = "There was an error updating the network privileges."
    else
      flash[:notice] = "Repository #{current_repository} network privileges updated."
    end
    redirect_to :back
  end

  def collaborators_only
    platform_execute(UpdateNetworkPrivilegeMutation, variables: {
      input: {
        repositoryId: current_repository.global_relay_id,
        collaboratorsOnly: params[:collaborators_only] == "1",
      },
    })
    flash[:notice] = "Repository #{current_repository} network privileges updated."
    redirect_to :back
  end

  # Reindex the repo's commits for search
  def reindex_commits
    current_repository.reindex_commits
    flash[:notice] = "Deleted existing documents and enqueued a commits reindexing job"
    redirect_to :back
  end

  def purge_commits
    current_repository.purge_commits
    flash[:notice] = "Purging commits from the search index ..."
    redirect_to :back
  end

  def fix_duplicate_labels
    FixDuplicateLabelsJob.perform_later(current_repository.id)
    flash[:notice] = "Fixing duplicate labels..."
    redirect_to :back
  end

  def fix_issue_transfers
    RetryTransferIssueJob.perform_later(current_repository.id, current_user)
    flash[:notice] = "Transferring stuck issues..."
    redirect_to :back
  end

  # Reindex the repo's issues for search
  def reindex_issues
    purge = params[:purge] == "true" ? true : false
    current_repository.reindex_issues(purge)
    reindex_count = current_repository.issues.where(pull_request_id: nil).count
    flash[:notice] = "Reindexing #{reindex_count} issues ..."
    redirect_to :back
  end

  def purge_issues
    current_repository.purge_issues
    flash[:notice] = "Purging issues from the search index ..."
    redirect_to :back
  end

  # Reindex the repo's projects for search
  def reindex_projects
    purge = params[:purge] == "true" ? true : false
    current_repository.reindex_projects(purge)
    flash[:notice] = "Reindexing #{current_repository.projects.count} projects ..."
    redirect_to :back
  end

  def purge_projects
    current_repository.purge_projects
    flash[:notice] = "Purging projects from the search index ..."
    redirect_to :back
  end

  # Reindex the repo's wiki for search
  def reindex_wiki
    current_repository.reindex_wiki
    flash[:notice] = "Deleted existing documents and enqueued a wiki reindexing job"
    redirect_to :back
  end

  # Purge the repo's wiki from search
  def purge_wiki
    current_repository.purge_wiki
    flash[:notice] = "Purging #{current_repository.name_with_owner} wiki from the search index ..."
    redirect_to :back
  end

  def reindex_discussions
    purge = params[:purge] == "true" ? true : false
    current_repository.reindex_discussions(purge)
    flash[:notice] = "Reindexing #{current_repository.discussions.size} discussions ..."
    redirect_to :back
  end

  def purge_discussions
    current_repository.purge_discussions
    flash[:notice] = "Purging discussions from the search index ..."
    redirect_to :back
  end

  # Enqueue a job to rescan the repo's files and analyze the languages
  def analyze_language
    current_repository.enqueue_analyze_language_breakdown
    flash[:notice] = "Language analysis job enqueued"
    redirect_to :back

  rescue GitHub::DGit::UnroutedError
    flash[:error] = "Repository offline"
    redirect_to :back
  end

  # Purge events related to this repo
  def purge_events
    current_repository.drop_events

    flash[:notice] = "Removed all events"
    redirect_to :back
  end

  # Rebuild CommitContribution data for this repository. Sometimes needed to
  # reindex commits due to failed job or other indexing issue.
  def rebuild_commit_contributions
    GitHub.dogstats.increment("repo", tags: ["action:rebuild_commit_contributions", "type:repo"])

    CommitContribution.backfill(current_repository, reset = true)
    flash[:notice] = "Rebuild commit contributions job enqueued ..."
    redirect_to :back
  end

  def archive
    if current_repository.set_archived
      flash[:notice] = "Repository successfully archived."
    else
      flash[:error] = "Failed to archive repository."
    end

    redirect_to :back
  end

  def unarchive
    if current_repository.unset_archived
      flash[:notice] = "Repository successfully unarchived."
    else
      flash[:error] = "Failed to unarchive repository."
    end

    redirect_to :back
  end

  # Disable a repository at the discretion of an enterprise admin.
  def admin_disable
    # we no longer want to render_404 unless GitHub.enterprise? since this is now a unified path
    reason = GitHub.enterprise? ? "admin" : params[:reason]
    instructions = params[:instructions]

    if !GitHub.enterprise? && params[:staff_note].blank?
      flash[:error] = "Must include a staff note when disabling a repository"
    else
      if current_repository.disable_access(reason, current_user, instructions: instructions)
        flash[:notice] = "Repository has been enqueued to be disabled and the user will be contacted."
        StaffNote.create(user: current_user, notable: this_user, note: params[:staff_note]) unless GitHub.enterprise?
      else
        flash[:error] = "Failed to disable access to repository"
      end
    end

    redirect_to :back
  end


  # Re-enable a repository that has been disabled for a size or tos violation.
  def remove_disable
    JobStatus.create(id: EnableRepositoryAccessJob.job_id(current_repository))

    if EnableRepositoryAccessJob.perform_later(current_repository, current_user)
      flash[:notice] = "Repository has been enqueued to be restored."
    else
      flash[:error] = "Failed to restore access to repository."
    end

    redirect_to :back
  end

  def change_allow_force_push
    val = params[:value].presence || false

    if val == "_clear"
      current_repository.clear_force_push_rejection(current_user)
    else
      current_repository.set_force_push_rejection val, current_user
    end

    flash[:notice] = case val
    when false
      "Force pushing is now allowed."
    when "all"
      "Force pushing is now blocked."
    when "default"
      "Force pushing is now blocked on the default branch."
    when "_clear"
      "Setting cleared. Force pushing will use the default setting."
    end

    redirect_to :back

  rescue GitHub::DGit::UnroutedError
    flash[:error] = "Repository offline"
    redirect_to :back
  end

  def change_ssh_access
    val = params[:value].presence || false

    if val == "_clear"
      current_repository.clear_ssh(current_user)
    else
      val == "true" ? current_repository.enable_ssh(current_user) : current_repository.disable_ssh(current_user)
    end

    flash[:notice] = case val
    when "true" || true
      "Git SSH access now enabled."
    when "false" || false
      "Git SSH access now disabled."
    when "_clear"
      "Setting cleared. Instance default will be used."
    end

    redirect_to :back
  end

  def change_anonymous_git_access
    return render_404 unless GitHub.anonymous_git_access_enabled?

    val = params[:value]&.to_s
    if current_repository.fork?
      flash[:error] = Repository::AnonymousGitAccess::FORK_ERROR
    elsif val == "true"
      current_repository.enable_anonymous_git_access(current_user)
      flash[:notice] = "Anonymous Git read access is now enabled."
    elsif val == "false"
      current_repository.disable_anonymous_git_access(current_user)
      flash[:notice] = "Anonymous Git read access is now disabled."
    else
      flash[:error] = "Failed to change anonymous Git read access."
    end

    redirect_to :back
  end

  def change_anonymous_git_access_locked
    return render_404 unless GitHub.anonymous_git_access_enabled?

    val = params[:value]&.to_s
    if val == "true"
      current_repository.lock_anonymous_git_access(current_user)
      flash[:notice] = "Anonymous Git read access is now locked."
    elsif val == "false"
      current_repository.unlock_anonymous_git_access(current_user)
      flash[:notice] = "Anonymous Git read access is now unlocked."
    else
      flash[:error] = "Failed to change anonymous Git read access locked state."
    end

    redirect_to :back
  end

  # Toggles the "allow git graph" option on this repo
  def toggle_allow_git_graph
    flash[:notice] = if !current_repository.toggle_allow_git_graph
      "Graphing is now enabled"
    else
      "Graphing is now disabled"
    end
    redirect_to :back

  rescue GitHub::DGit::UnroutedError
    flash[:error] = "Repository offline"
    redirect_to :back
  end

  def rebuild_contribution_insight_graphs
    status = Repository::ContributionGraphStatus.for_repository(current_repository)
    if status.running?
      redirect_to :back, flash: {
        error: "Metrics are currently being generated and cannot be rebuilt. Please wait and try again.",
      }
    elsif status.enqueue_rebuild_metrics_job
      redirect_to :back, notice: "Contribution graph data is being rebuilt."
    else
      redirect_to :back, flash: {
        error: "Something went wrong. Please try again.",
      }
    end
  end

  # Toggles the 'public_push' option on this repo
  def toggle_public_push
    return render_404 unless GitHub.public_push_enabled?

    if current_repository.public?
      current_repository.update public_push: !current_repository.public_push
      flash[:notice] = "Public push is #{current_repository.public_push? ? 'now' : 'no longer'} allowed"
    else
      flash[:error] = "Cannot toggle public push on private repo"
    end
    redirect_to :back
  end

  # Switch a fork to be public or private, matching its root repo.
  # Switch a public unforked repository to private.
  def toggle_permission
    if !current_repository.fork? && in_network?
      flash[:error] = "Cannot change permissions on a root repo"
    elsif current_repository.public? == current_repository.root.public? && in_network?
      flash[:error] = "Cannot change a fork’s permissions to be different from its root"
    else
      new_perms = current_repository.public? ? "private" : "public"
      nwo = current_repository.name_with_owner

      current_repository.toggle_visibility(actor: current_user)

      flash[:notice] = "#{nwo} permissions changed to #{new_perms}"
    end
    redirect_to :back
  end

  def toggle_token_scanning
    return render_404 unless current_repository.token_scanning_feature_enabled? || current_repository.show_token_scanning_stafftools_ui?

    enable = params[:enable_token_scanning]
    if enable
      current_repository.enable_token_scanning(actor: this_user, use_staff_key: true)
    else
      current_repository.disable_token_scanning(actor: this_user, use_staff_key: true)
    end

    flash[:notice] = "#{enable ? "Enabled" : "Disabled"} secret scanning on this repository."
    redirect_to :back
  end

  def rescan_for_tokens
    RepositoryPrivateTokenScanningJob.perform_later(current_repository.id)

    flash[:notice] = "The repository is being scanned. Check back later for results."

    redirect_to :back
  end

  def dry_run_secret_scan
    RepositoryPrivateTokenScanningDryRunJob.perform_later(current_repository.id)

    flash[:notice] = "Initiated a dry run of secret scanning for this repository. Subscribers will not be notified and results will not be stored."

    redirect_to :back
  end

  def in_network?
    current_repository.root.all_forks_count > 0
  end

  def change_max_object_size
    val = params[:value].presence

    if val
      if val == "_clear"
        current_repository.clear_max_object_size(current_user)
        flash[:notice] = "Setting cleared. Maximum object size will use the inherited default."
      else
        current_repository.set_max_object_size(val.to_i, @current_user)
        if val.to_i == 0
          flash[:notice] = "Maximum object size updated to unlimited"
        else
          flash[:notice] = "Maximum object size updated to #{val}MB"
        end
      end
    else
      flash[:error] = "Maximum object size value must be a positive integer or zero"
    end

    redirect_to :back
  end

  def change_warn_disk_quota
    val = params[:value].presence
    change_disk_quota(:warn, val)
  end

  def change_lock_disk_quota
    val = params[:value].presence
    change_disk_quota(:lock, val)
  end

  def change_disk_quota(kind, val)
    if val
      v = val.to_i
      current_repository.set_disk_quota(kind: kind, value: v, user: @current_user)
      case v
      when 0
        flash[:notice] = "#{kind.to_s.capitalize} disk quota updated to unlimited"
      when current_repository.default_disk_quota(kind: kind)
        flash[:notice] = "#{kind.to_s.capitalize} disk quota set to default #{v}GB"
      else
        flash[:notice] = "#{kind.to_s.capitalize} disk quota updated to #{v}GB"
      end
    else
      flash[:error] = "#{kind.to_s.capitalize} disk quota value must be a positive integer or zero"
    end

    redirect_to :back
  end

  AdminQuery = parse_query <<-'GRAPHQL'
    query($name: String!, $owner: String!) {
      repository: staffAccessedRepository(name: $name, owner: $owner) {
        ...Views::Stafftools::Repositories::Admin::Repository
      }
    }
  GRAPHQL

  def admin
    if GitHub.porter_available? && !params[:skip_porter]
      @porter_status = Porter::StafftoolsClient.get_import_status(
        repository: current_repository,
        env:        request.env,
      )
    end

    data = platform_execute(AdminQuery,
      variables: { owner: params[:user_id], name: params[:id] },
    )

    @issue_transfers = IssueTransfer.where(old_repository_id: current_repository.id, state: "errored")
    locals = { issue_transfers: @issue_transfers}

    if data
      render "stafftools/repositories/admin", locals: locals.merge({ repository: data.repository })
      return
    end

    render "stafftools/repositories/admin", locals: locals
  end

  def redetect_license
    RepositorySetLicenseJob.perform_later(current_repository)
    flash[:notice] = "License detection job enqueued"
    redirect_to :back
  end

  def redetect_community_health_files
    CommunityProfile.enqueue_health_check_job(current_repository)
    flash[:notice] = "Community health check job enqueued"
    redirect_to :back
  end

  def schedule_backup
    RepositoryBackupNgJob.perform_later(current_repository.id, "repository")
    flash[:notice] = "Repository backup job scheduled"
    redirect_to :back
  end

  def schedule_wiki_backup
    RepositoryBackupNgJob.perform_later(current_repository.id, "wiki")
    flash[:notice] = "Repository wiki backup job scheduled"
    redirect_to :back
  end

  PackageApplicationDependentsQuery = parse_query <<-'GRAPHQL'
    query($repositoryId: ID!) {
      node(id: $repositoryId) {
        ...Views::Stafftools::Repositories::DependencyGraph::PackageAndApplications
      }
    }
  GRAPHQL

  def dependency_graph
    render_404 unless GitHub.dependency_graph_enabled?

    data = nil
    dependents_unavailable = true
    timing = 0.0

    if current_repository.public?
      timing = Benchmark.measure do
        data = platform_execute(PackageApplicationDependentsQuery, variables: {
          "repositoryId" => current_repository.global_relay_id,
        })
      end
      dependents_unavailable = data.errors.all.any?
    end

    manifest_paths = current_repository.public? ? current_repository.dependency_manifest_paths : []

    respond_to do |format|
      format.html do
        render "stafftools/repositories/dependency_graph", locals: {
          timing: timing,
          repository: data&.node,
          current_repo: current_repository,
          dependents_unavailable: dependents_unavailable,
          manifest_files: manifest_paths,
        }
      end
    end
  end

  ReassignPackageQuery = parse_query <<-'GRAPHQL'
    mutation($input: DependencyGraphReassignPackageInput!) {
      dependencyGraphReassignPackage(input: $input) {
        clientMutationId
      }
    }
  GRAPHQL

  def reassign_package
    render_404 unless GitHub.dependency_graph_enabled?

    data = platform_execute(ReassignPackageQuery, variables: {
      input: {
        "packageManager" => params[:package_manager],
        "packageName" => params[:package_name],
        "targetRepository" => params[:target_repository],
        "clientMutationId" => request_id,
      },
    })

    if data.errors.all.any?
      reason = data.errors.messages.map { |field, messages| "#{field}: #{messages.join(", ")}" }.join("; ")
      flash[:error] = "Unable to reassign #{params[:package_name]} to #{params[:target_repository]} (#{reason})"
    else
      flash[:notice] = "Package #{params[:package_name]} has been assigned to #{params[:target_repository]}"
    end

    redirect_to gh_dependency_graph_stafftools_repository_path(current_repository)
  end

  def detect_manifests
    RepositoryDependencyManifestInitializationJob.perform_later(current_repository.id)
    flash[:notice] = "Manifest detection job enqueued"
    redirect_to gh_dependency_graph_stafftools_repository_path(current_repository)
  end

  def clear_dependencies
    RepositoryDependencyClearDependencies.perform_later(current_repository.id)
    flash[:notice] = "Clear dependencies job enqueued"
    redirect_to gh_dependency_graph_stafftools_repository_path(current_repository)
  end

  # Set the Used By repository configuration flag.
  def set_used_by
    if params[:used_by_enabled] == "1"
      flash[:notice] = "Used By is now enabled" if current_repository.enable_used_by(actor: current_user)
    elsif params[:used_by_enabled] == "0"
      flash[:notice] = "Used By is now disabled" if current_repository.disable_used_by(actor: current_user)
    end

    redirect_to :back
  end

  SetInteractionLimitQuery = parse_query <<-'GRAPHQL'
     mutation($input: SetRepositoryInteractionLimitInput!) {
      setRepositoryInteractionLimit(input: $input)
    }
  GRAPHQL

  def set_interaction_limit
    return render_404 unless GitHub.interaction_limits_enabled?

    input_variables = {
      repositoryId: current_repository.global_relay_id,
      limit: params[:interaction_setting],
      expiry: "ONE_WEEK",
      isStaffActor: true,
    }

    results = platform_execute(SetInteractionLimitQuery, variables: { input: input_variables })

    if results.errors.any?
      flash[:error] = results.errors.all.values.flatten.to_sentence
    else
      flash[:notice] = "Repository interaction limit settings saved."
    end

    redirect_to :back
  end

  def wiki_mark_as_broken
    current_repository.repository_wiki.mark_as_broken
    flash[:warn] = "Wiki repository marked broken"
    redirect_to :back
  end

  def wiki_schedule_maintenance
    current_repository.repository_wiki.schedule_maintenance
    flash[:notice] = "Maintenance job enqueued for wiki repository"
    redirect_to :back
  end

  private

  def memory_data
    {
      controller: {
        name: params[:controller],
        action: params[:action],
      },
      user: {
        login: current_user.login,
      },
      statistics: GC.stat,
    }
  end
end
