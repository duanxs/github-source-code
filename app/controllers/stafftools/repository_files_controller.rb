# frozen_string_literal: true

class Stafftools::RepositoryFilesController < StafftoolsController
  areas_of_responsibility :code_collab, :stafftools

  before_action :ensure_repo_exists
  before_action :ensure_file_exists, except: :index

  layout "stafftools/repository/collaboration"

  def index
    @files = RepositoryFile.where(repository_id: current_repository.id).
      order("name ASC").
      limit(25).
      page(params[:page] || 1)
    render "stafftools/repository_files/index"
  end

  def show
    render "stafftools/repository_files/show"
  end

  def database
    render "stafftools/repository_files/database"
  end

  def destroy
    this_file.destroy

    flash[:notice] = "Deleted file '#{this_file.name}'"
    redirect_to gh_stafftools_repository_repository_files_path(current_repository)
  end

  private

  def this_file
    @this_file ||= RepositoryFile.find(params[:id])
  end
  helper_method :this_file

  def ensure_file_exists
    return render_404 if this_file.nil?
  end
end
