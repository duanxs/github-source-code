# frozen_string_literal: true

class Stafftools::DependabotController < StafftoolsController
  areas_of_responsibility :dependabot, :stafftools

  before_action :ensure_user_exists
  before_action :ensure_dependabot_available

  def show
    render "stafftools/dependabot/show",
      layout: "stafftools/organization/content",
      locals: {
        owner: this_user,
      }
  end

  def enable_rollout
    this_user.enable_vulnerability_updates(actor: current_user)

    redirect_back \
      fallback_location: stafftools_dependabot_path(this_user),
      notice: "Opted in to Dependabot Security Updates."
  end

  def disable_rollout
    this_user.disable_vulnerability_updates(actor: current_user)

    redirect_back \
      fallback_location: stafftools_dependabot_path(this_user),
      notice: "Opted out of Dependabot Security Updates."
  end

  private

  def ensure_dependabot_available
    render_404 unless GitHub.dependabot_enabled?
  end
end
