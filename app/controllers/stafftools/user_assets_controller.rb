# frozen_string_literal: true

class Stafftools::UserAssetsController < StafftoolsController
  areas_of_responsibility :code_collab, :stafftools

  before_action :ensure_asset_exists, except: [:send_for_batch_scan_for_user, :send_for_batch_scan_for_repository]

  def show
    render "stafftools/user_assets/show"
  end

  def destroy
    this_asset.purge

    instrument "staff.delete_user_asset", event_payload

    flash[:notice] = "Deleted image attachment with ID #{this_asset.id}."
    redirect_to stafftools_path
  end

  def send_for_scanning
    return render_404 if GitHub.enterprise?
    GlobalInstrumenter.instrument "user_asset.scan_requested", {
      user_asset: this_asset,
    }

    flash[:notice] = "Sent asset for scanning"
    redirect_to stafftools_path
  end

  def send_for_batch_scan_for_user
    user = User.find_by_login(params[:user_id])
    return render_404 if GitHub.enterprise? || user.nil?

    if user.assets.empty?
      return redirect_to stafftools_path
    end

    ScanUploadsJob.perform_later(target: user)

    payload = build_user_scan_payload(user)
    UserAsset.instrument_user_assets_batch_scan(payload)

    flash[:notice] = "Sent all of #{@user.login}'s assets for scanning"
    redirect_to stafftools_path
  end

  def send_for_batch_scan_for_repository
    repo = Repository.find(params[:repository_id])
    return render_404 if GitHub.enterprise? || repo.nil?

    if repo.assets.empty?
      return redirect_to stafftools_path
    end

    ScanUploadsJob.perform_later(target: repo)

    payload = build_repo_scan_payload(repo)
    UserAsset.instrument_repository_assets_batch_scan(payload)

    flash[:notice] = "Sent all of #{repo.name}'s assets for scanning"
    redirect_to stafftools_path
  end

  private

  def build_repo_scan_payload(repo)
    payload = {
        actor: "github-staff",
        repository: repo.name,
        repository_id: repo.id,
        repository_owner: repo.owner.to_s,
        number_of_assets: repo.assets.count
    }

    payload.update(GitHub.guarded_audit_log_staff_actor_entry(current_user))
  end

  def build_user_scan_payload(user)
    payload = {
        actor: "github-staff",
        user: user.login,
        user_id: user.id,
        number_of_assets: user.assets.count
    }

    payload.update(GitHub.guarded_audit_log_staff_actor_entry(current_user))
  end

  def this_asset
    @this_asset ||= UserAsset.find_by_id(params[:id])
  end
  helper_method :this_asset

  def ensure_asset_exists
    render_404 if this_asset.nil?
  end

  def event_payload
    {
      asset_id: this_asset.id,
      asset_guid: this_asset.guid,
    }
  end
end
