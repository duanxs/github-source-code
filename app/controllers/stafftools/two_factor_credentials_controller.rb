# frozen_string_literal: true

class Stafftools::TwoFactorCredentialsController < StafftoolsController
  areas_of_responsibility :stafftools

  before_action :ensure_user_exists
  before_action :ensure_two_factor_enabled
  before_action :ensure_two_factor_configured
  before_action :ensure_sms_configured, only: [:send_test_sms, :sms_log, :sms_logs]

  layout "stafftools/user/security"

  # Send the user a test SMS message to the number they have configured for 2FA
  # The message includes a reference to GitHub Support, since it will be
  # sent manually from stafftools.
  def send_test_sms
    code = "%06X" % rand(16**5)  # get a random six-place hex number
    message = "This is GitHub Support sending a test message: #{code}"
    begin
      instrument("staff.send_2fa_test_message", user: this_user, code: code, sms_number: sms_number)

      GitHub::SMS.send_message(sms_number, message,
        provider: this_user.two_factor_credential.provider
      )
      flash[:notice] = "Message sent#{' to backup' if params[:backup]}! (using code #{code})"
    rescue GitHub::SMS::Error
      flash[:error] = "Message failed to send."
    end
    redirect_to :back
  end

  def sms_log
    render "stafftools/two_factor_credentials/sms_log"
  end

  def sms_logs
    begin
      @log = GitHub::SMS.log_for sms_number
    rescue GitHub::SMS::Error => e
      # Capitalize the first letter only
      @error_message = e.message.slice(0, 1).capitalize + e.message.slice(1..-1)
    end

    locals = {
      log:           @log,
      sms_number:    sms_number,
      error_message: @error_message,
    }
    respond_to do |format|
      format.html do
        render partial: "stafftools/two_factor_credentials/sms_logs", locals: locals
      end
    end
  end

  def check_otp
    otp_code = params[:otp_code]
    timestamp = this_user.two_factor_credential.otp_valid_timestamp otp_code

    if timestamp
      relativity = (timestamp > Time.now) ? "will be" : "was"
      flash[:notice] = "#{otp_code} #{relativity} valid for #{this_user} at #{timestamp.utc}"
    else
      flash[:error] = "#{otp_code} was not valid for #{this_user} within 24 hours of now"
    end

    instrument("staff.check_otp_code", user: this_user, otp_code: otp_code, valid: !!timestamp)

    redirect_to :back
  end

  def change_sms_provider
    provider = GitHub::SMS.get_provider(params[:provider]).provider_name.to_s
    this_user.two_factor_credential.provider = provider
    if this_user.two_factor_credential.save
      flash[:notice] = "SMS provider set to #{provider.humanize}"
    else
      flash[:error] = "Failed to set SMS provider"
    end
    redirect_to :back
  end

  def approve_2fa_removal_request
    id = params[:request_id]
    emails = params[:emails]
    reason = params[:two_factor_recovery_approve_reason]

    return render_404 unless recovery_request = this_user.two_factor_recovery_requests.find_by_id(id)

    recovery_request.approve(current_user, emails)

    instrument("two_factor_account_recovery.staff_approve", user: this_user, reason: reason)

    GlobalInstrumenter.instrument("two_factor_account_recovery.updated",
      action_type: :STAFF_APPROVED,
      user: this_user,
      evidence_type: recovery_request.hydro_evidence_type,
    )

    redirect_to :back
  end

  def decline_2fa_removal_request
    id = params[:request_id]
    reason = params[:two_factor_recovery_decline_reason]

    return render_404 unless recovery_request = this_user.two_factor_recovery_requests.find_by_id(id)

    recovery_request.update!(reviewer: current_user, declined_at: Time.now)

    AccountRecoveryMailer.request_declined_by_staff(this_user).deliver_later

    instrument("two_factor_account_recovery.staff_decline", user: this_user, reason: reason)

    GlobalInstrumenter.instrument("two_factor_account_recovery.updated",
      action_type: :STAFF_DECLINED,
      user: this_user,
      evidence_type: recovery_request.hydro_evidence_type,
    )

    redirect_to :back
  end

  def destroy
    reason = params[:reason]

    if reason.blank?
      flash[:error] = "You must provide a reason for the log"
    elsif !this_user.two_factor_auth_can_be_disabled?
      flash[:error] = <<-STR
        Cannot disable two-factor authentication. @#{this_user} is affiliated
        with at least one organization or enterprise account that requires
        2FA to be enabled.
      STR
    else
      this_user.two_factor_credential.destroy
      instrument("staff.two_factor_disable", user: this_user, note: reason)
      flash[:notice] = "Two-factor authentication disabled for @#{this_user}"
    end

    redirect_to :back
  end

  private

  def ensure_two_factor_enabled
    render_404 unless GitHub.auth.two_factor_authentication_enabled?
  end

  def ensure_two_factor_configured
    render_404 unless this_user.two_factor_credential
  end

  def ensure_sms_configured
    cred = this_user.two_factor_credential
    render_404 unless (cred.sms_enabled? || cred.fallback_enabled?) && sms_number
  end

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def sms_number
    @sms_number ||= begin
      cred = this_user.two_factor_credential
      if params[:backup]
        cred.backup_sms_number if cred.fallback_enabled?
      else
        if cred.sms_enabled?
          cred.sms_number
        elsif cred.fallback_enabled?
          cred.backup_sms_number
        end
      end
    end
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator
end
