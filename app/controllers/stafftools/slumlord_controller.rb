# frozen_string_literal: true

class Stafftools::SlumlordController < StafftoolsController
  areas_of_responsibility :stafftools, :subversion

  before_action :ensure_repo_exists

  layout "stafftools/repository/storage"

  def show
    # Things to do here:
    # 1. View if SVN has been used. (fs_exist? 'svn.history.msgpack')
    # 2. Enable/disable SVN (banhammer)
    # 3. Enable/disable debugging
    # 4. Set the correct support contact for the sample error message shown

    if current_repository.online?
      @svn_in_use = current_repository.svn_in_use?
      @svn_status = current_repository.svn_status
      @svn_blocked = current_repository.svn_blocked?
      @svn_debugging = current_repository.svn_debugging?
      @support_contact = GitHub.support_link_text
    end

    render "stafftools/slumlord/show"
  end

  def toggle_blocked
    current_repository.svn_toggle_blocked
    redirect_to :back
  rescue GitHub::DGit::UnroutedError
    flash[:error] = "Repository offline"
    redirect_to :back
  end

  def toggle_debug
    current_repository.svn_toggle_debugging
    redirect_to :back
  rescue GitHub::DGit::UnroutedError
    flash[:error] = "Repository offline"
    redirect_to :back
  end
end
