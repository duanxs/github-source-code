# frozen_string_literal: true

class Stafftools::HooksController < StafftoolsController
  areas_of_responsibility :webhook

  before_action :context_required
  before_action :hook_view, only: [:show, :toggle_active_status]

  helper_method :current_context, :repo_hooks?

  layout :new_nav_layout
  private def new_nav_layout
    if repo_hooks?
      "stafftools/repository/security"
    else
      "stafftools/organization/security"
    end
  end

  def index
    # TODO: MV this helper once the stafftools view model PR gets merged
    @hooks_view = Stafftools::RepositoryViews::HooksView.new(current_context: current_context)
    render "stafftools/hooks/index"
  end

  def show
    render "stafftools/hooks/show", locals: { hook_deliveries_query: params[:deliveries_q] }
  end

  def toggle_active_status
    hook.toggle!(:active)
    flash[:notice] = "Okay, the webhook was successfully #{hook_view.hook_active_status}."
    redirect_to :back
  end

  private

  def hook_view
    @hook_view = Hooks::ShowView.new hook: hook
  end

  def hook
    @hook ||= current_context.hooks.find(params[:id])
  end

  def current_context
    if repo_hooks?
      current_repository
    else
      # @user needed for Stafftools user layout
      @user ||= Organization.find_by_login(params[:user])
    end
  end

  def context_required
    raise NotFound unless current_context
  end

  def repo_hooks?
    params[:repository].present?
  end
end
