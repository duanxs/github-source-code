# frozen_string_literal: true

class Stafftools::BillingDateChangesController < StafftoolsController
  areas_of_responsibility :stafftools

  before_action :ensure_user_exists
  before_action :ensure_billing_enabled

  layout :new_nav_layout
  private def new_nav_layout
    case this_user.site_admin_context
    when "organization"
      "stafftools/organization/billing"
    when "user"
      "stafftools/user/billing"
    end
  end

  def show
    render_template_view "stafftools/billing_date_changes/show",
      Stafftools::BillingDateChangesView,
      target: @user,
      change: change
  end

  def create
    result = change.perform if valid_billing_change?
    result ||= failure_result

    if result.success?
      flash[:notice] = "Billing date changed to #{@user.billed_on}"
    else
      flash[:error] = result.error_message
    end

    redirect_to billing_stafftools_user_path(@user)
  end

  private

  def valid_billing_change?
    params[:reason].present? || @user.invoiced?
  end

  def failure_result
    GitHub::Billing::Result.failure "reason is required"
  end

  def billing_date
    if params[:billed_on].present?
      Date.strptime params[:billed_on], "%Y-%m-%d"
    elsif params[:new_term_end_date].present?
      Date.strptime(params[:new_term_end_date], "%Y-%m-%d") + 1.day
    end
  rescue ArgumentError
    date = params[:billed_on] || params[:new_term_end_date]
    flash[:error] = "Billing date #{date} is not a valid date"
    nil
  end

  def change
    return unless billing_date.present?
    @change ||= Billing::BillingDateChange.new @user, billing_date,
      actor: current_user,
      reason: params[:reason]
  end
end
