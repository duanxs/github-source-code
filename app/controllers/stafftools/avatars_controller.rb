# frozen_string_literal: true

class Stafftools::AvatarsController < StafftoolsController
  areas_of_responsibility :avatars, :stafftools

  before_action :ensure_user_exists

  layout :new_nav_layout
  private def new_nav_layout
    case this_user.site_admin_context
    when "organization"
      "stafftools/organization/overview"
    when "user"
      "stafftools/user/overview"
    end
  end

  def index
    @avatar_id = params[:avatar_id].to_i
    render "stafftools/avatars/index"
  end

  def undo
    if p = @user.primary_avatar
      if p.undo(p.updater)
        flash[:notice] = "Rolled back to previous avatar."
      else
        flash[:notice] = "Reverted back to gravatar."
      end
    else
      flash[:error] = "Already using gravatar"
    end
    redirect_to :back
  end

  def revert_to_gravatar
    if p = @user.primary_avatar
      p.destroy
    end
    flash[:notice] = "Reverted back to gravatar."
    redirect_to :back
  end

  def purge
    @user.avatar_list.purge_cdn

    flash[:notice] = %(Purged "#{@user.avatar_list.surrogate_key}".)
    redirect_to :back
  end

  def promote
    if avatar = @user.avatars.find_by_id(params[:id])
      PrimaryAvatar.set(avatar, avatar.uploader)
      flash[:notice] = "The #{@user} avatar has changed."
    else
      flash[:error] = "Avatar not found."
    end
    redirect_to :back
  end

  def destroy
    if avatar = @user.avatars.find_by_id(params[:id])
      avatar.destroy
      flash[:notice] = "Avatar deleted."
    else
      flash[:error] = "Avatar not found."
    end
    redirect_to :back
  end

end
