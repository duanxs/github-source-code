# frozen_string_literal: true

class Stafftools::DiscussionsController < StafftoolsController
  areas_of_responsibility :discussions, :stafftools

  before_action :ensure_repo_exists
  before_action :ensure_discussion_exists, except: :index

  layout "stafftools/repository/collaboration"

  def index
    discussions = current_repository.discussions.order("id DESC").paginate(
      page: params[:page] || 1,
      per_page: 25,
    )

    render "stafftools/discussions/index", locals: {
      discussions: discussions,
    }
  end

  def show
    conditions = [
      "data.discussion_id:#{this_discussion.id}",
      "data.user_content_id:#{this_discussion.id} AND data.user_content_type:#{this_discussion.class} AND action:user_content_edit.*",
    ]
    audit_log_query = conditions.map { |cond| "(#{cond})" }.join(" OR ")
    audit_log_data = fetch_audit_log_teaser(audit_log_query)

    deliveries = Stafftools::UserNotificationDeliveries.for_thread(this_discussion)

    notifications_view = Stafftools::RepositoryViews::NotificationsView.new(
      repository: current_repository,
      params: {
        thread: Newsies::Thread.new("Discussion", this_discussion.id).key,
        comment: Newsies::Comment.to_key(this_discussion),
      },
    )

    render "stafftools/discussions/show", locals: {
      notification_deliveries: deliveries,
      notifications_view: notifications_view,
      audit_log_data: audit_log_data,
    }
  end

  def database
    respond_to do |format|
      format.html do
        render "stafftools/discussions/database"
      end
    end
  end

  def destroy
    if this_discussion.destroy
      discussion_num = this_discussion.number

      instrument \
        "staff.delete_discussion",
        user: current_repository.owner,
        repo: current_repository,
        note: "Deleted discussion #{current_repository.nwo}##{discussion_num}"

      flash[:notice] = "Discussion ##{this_discussion.number} deleted"
    else
      flash[:error] = this_discussion.errors.full_messages
    end

    redirect_to gh_stafftools_repository_discussions_path(current_repository)
  end

  def lock
    this_discussion.lock(current_user)

    redirect_to gh_stafftools_repository_discussion_path(this_discussion)
  end

  def unlock
    this_discussion.unlock(current_user)

    redirect_to gh_stafftools_repository_discussion_path(this_discussion)
  end
end
