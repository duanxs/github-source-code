# frozen_string_literal: true

class Stafftools::SiteController < StafftoolsController
  areas_of_responsibility :stafftools

  before_action :ensure_ssh_audit_enabled, only: [:ssh_audit]

  def redirect
    render_404 and return unless user = User.find_by_login(params[:user])

    is_repo = params[:repo] && user.find_repo_by_name(params[:repo])
    subpath = is_repo ? "repositories" : "users"
    url = "/stafftools/#{subpath}/#{user}"
    url += "/#{params[:repo]}" unless params[:repo].blank?
    url += "/#{params[:path]}" unless params[:path].blank?

    redirect_to url
  end

  def ssh_audit
    PublicKey.update_all(verified_at: nil)
    redirect_to stafftools_users_path, notice: "Public key audit has started."
  end

  private

  def ensure_ssh_audit_enabled
    render_404 unless GitHub.ssh_audit_enabled?
  end

end
