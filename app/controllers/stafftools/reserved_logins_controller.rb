# frozen_string_literal: true

class Stafftools::ReservedLoginsController < StafftoolsController
  areas_of_responsibility :community_and_safety, :platform_health, :stafftools

  before_action :ensure_not_enterprise, only: [:create, :destroy]

  def index
    return redirect_to stafftools_reserved_login_path(login: params[:login]) if params[:login]

    reserved_logins = ReservedLogin.order("login ASC").paginate(page: current_page, per_page: 30)
    render "stafftools/reserved_logins/index", locals: { reserved_logins: reserved_logins }
  end

  def create
    Audit.context.push(reason: params[:reason])

    reserve = ReservedLogin.new(login: params[:login])
    if reserve.save
      flash[:notice] = "Login #{params[:login]} reserved."
      redirect_to :back
    else
      flash[:error] = flash[:error] = reserve.errors.messages.values.join(", ")
      redirect_to :back
    end
  end

  def destroy
    login = ReservedLogin.find_by_login(params[:login])

    unless login.destroy
      flash[:error] = login.errors.messages.values.join(", ")
      redirect_to :back
    else
      flash[:notice] = "Login #{params[:login]} unreserved."
      redirect_to :back
    end
  end

  def search
    render "stafftools/reserved_logins/search", locals: {
        reserved_login: ReservedLogin.find_by_login(params[:login]),
    }
  end

  private

  def ensure_not_enterprise
    render_404 if GitHub.enterprise?
  end
end
