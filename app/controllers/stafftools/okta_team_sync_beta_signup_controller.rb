# frozen_string_literal: true

module Stafftools
  class OktaTeamSyncBetaSignupController < StafftoolsController
    areas_of_responsibility :stafftools

    def index
      page_memberships = memberships.paginate(page: current_page, per_page: 500)

      render "stafftools/okta_team_sync_beta_signup/index", locals: { memberships: page_memberships }
    end

    def toggle_access
      member = EarlyAccessMembership.find_by_id(params[:membership_id])&.member
      if member
        if member.okta_team_sync_beta_enabled?
          member.disable_okta_team_sync_beta
        else
          member.enable_okta_team_sync_beta
        end
        respond_to do |format|
          format.all { render status: 200, json: "successfully toggled access" }
        end
      else
        flash[:error] = "Please select an organization or enterprise account."
        redirect_to :back
      end
    end

    private

    def memberships
      @memberships ||= EarlyAccessMembership
        .order("created_at ASC")
        .okta_team_sync_waitlist
    end
  end
end
