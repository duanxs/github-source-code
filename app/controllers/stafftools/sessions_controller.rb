# frozen_string_literal: true

class Stafftools::SessionsController < StafftoolsController
  areas_of_responsibility :enterprise_only, :stafftools

  before_action :require_admin_frontend, except: :back_to_the_login
  before_action :site_admin_only, except: :back_to_the_login
  before_action :ensure_reason, only: [:impersonate, :override_impersonate]
  before_action :ensure_impersonated_user_is_not_site_admin, only: [:impersonate, :override_impersonate]
  before_action :ensure_active_staff_access_grant, only: [:impersonate]

  skip_before_action :ensure_stafftools_authorization, only: [:back_to_the_login] unless GitHub.enterprise?

  def impersonate
    instrument("staff.fake_login", user: impersonated_user, note: impersonation_reason)
    impersonated_login_user impersonated_user
    redirect_to "/"
  end

  # For impersonating users without a staff access grant.
  # Requires "can-impersonate-users-without-permission" stafftools role.
  def override_impersonate
    instrument("staff.override_fake_login", user: impersonated_user, note: impersonation_reason)
    impersonated_login_user impersonated_user
    redirect_to "/"
  end

  # Hitting this URL after using impersonate will restore your previous session.
  def back_to_the_login
    impersonated_user = self.current_user
    former_user = impersonated_logout_user

    redirect_to stafftools_user_url(impersonated_user)

    instrument("staff.exit_fake_login", user: impersonated_user)
  end

  # The fake_login route is a shortcut for dev mode.  It allows for quick user
  # switching via bookmarks, so you don't have to use stafftools impersonate.
  if Rails.development?
    skip_before_action :login_required, only: :fake_login
    skip_before_action :require_admin_frontend, only: :fake_login
    skip_before_action :sudo_filter, only: :fake_login
    around_action :select_write_database, only: :fake_login

    def fake_login
      logout_user(:switched_users)
      fake_user = User.find_by_login(params[:id])
      login_user(fake_user, client: :fake_login)
      redirect_to "/"
    end
  end

  private

  def impersonated_user
    @impersonsated_user ||= User.find_by_login(params[:id])
  end

  def current_access_grant
    @current_access_grant ||= impersonated_user.active_staff_access_grant
  end

  def impersonation_reason
    @impersonation_reason ||= current_access_grant&.reason || params[:reason]
  end

  def ensure_impersonated_user_is_not_site_admin
    if impersonated_user.site_admin?
      flash[:error] = "You cannot impersonate a site admin"
      redirect_to :back
    end
  end

  def ensure_reason
    if impersonation_reason.blank?
      flash[:error] = "You must provide a reason for the log"
      redirect_to :back
    end
  end

  def ensure_active_staff_access_grant
    if current_user.access_grant_required_for_user_impersonation? && current_access_grant.nil?
      flash[:error] = "There is no active grant!"
      redirect_to :back
    end
  end
end
