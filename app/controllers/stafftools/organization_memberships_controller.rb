# frozen_string_literal: true

class Stafftools::OrganizationMembershipsController < StafftoolsController
  areas_of_responsibility :orgs, :stafftools

  before_action :ensure_user_exists
  before_action :ensure_account_is_user

  layout "stafftools/user/collaboration"

  def index
    locals = fetch_audit_log_teaser this_user_query

    locals[:orgs] = this_user.affiliated_organizations_with_roles.sort_by { |o| o[0].login }.to_h
    locals[:orgs_owned] = this_user.owned_organizations.sort_by(&:login)
    locals[:team_counts] = locals[:orgs].keys.reduce({}) do |memo, org|
      memo.update(org => teams_for(org).count)
    end

    if !GitHub.single_business_environment?
      locals[:businesses] = this_user.businesses.sort_by(&:slug)
    end

    render "stafftools/organization_memberships/index", locals: locals
  end

  def show
    org = this_user.organizations.find_by_login params[:id]

    locals = fetch_audit_log_teaser org_query(org)
    locals[:org] = org
    locals[:teams] = teams_for(org).sort_by { |t| t.name }

    render "stafftools/organization_memberships/show", locals: locals
  end

  def remove_user
    org = Organization.find_by_login(params[:id])

    begin
      if this_user.affiliated_with_organization?(org)
        if GitHub.guard_audit_log_staff_actor?
          GitHub.context.push(hide_staff_user: true)
          Audit.context.push(hide_staff_user: true)
        end

        org.remove_any_affiliation(this_user, actor: current_user)
      else
        return render_404
      end

      flash[:notice] = "Removed #{this_user} from the #{org} organization. It may take a few minutes to process."
    rescue Organization::NoAdminsError
      flash[:notice] = <<~STR
        Can't remove #{this_user} from the #{org} organization.
        #{this_user} is the last remaining owner.
      STR
    end

    redirect_to stafftools_user_organization_memberships_path(this_user)
  end

  def enable_two_factor_requirement
    org = Organization.find_by_login(params[:id])
    if org.can_two_factor_requirement_be_enabled?
      GitHub.dogstats.increment "organization", tags: ["action:enable_two_factor_requirement"]
      EnforceTwoFactorRequirementOnOrganizationJob.perform_later(org, current_user)
      flash[:notice] = "Enabling two-factor authentication requirement."
    else
      flash[:error] = "Two-factor authentication requirement cannot be enabled. No organization admins have two-factor authentication enabled."
    end
    redirect_to stafftools_user_organization_memberships_path(this_user)
  end

  def disable_two_factor_requirement
    org = Organization.find_by_login(params[:id])
    GitHub.dogstats.increment "organization", tags: ["action:disable_two_factor_requirement"]

    org.disable_two_factor_requirement(log_event: true, actor: current_user)

    flash[:notice] = "Disabled two-factor authentication requirement."
    redirect_to stafftools_user_organization_memberships_path(this_user)
  end

  private

  ORG_AUDIT_EVENTS = %w(
    action:org.invite_member
    action:org.cancel_invitation
    action:org.add_member
    action:org.update_member
    action:org.remove_member
    action:org.restore_member
    action:org.add_billing_manager
    action:org.remove_billing_manager
  ).join(" OR ")

  TEAM_AUDIT_EVENTS = %w(
    action:team.add_member
    action:team.remove_member
  ).join(" OR ")

  def this_user_query
    "user_id:#{this_user.id} AND (#{ORG_AUDIT_EVENTS})"
  end

  def org_query(org)
    [
      "org_id:#{org.id}",
      "user_id:#{this_user.id}",
      "(#{ORG_AUDIT_EVENTS} OR #{TEAM_AUDIT_EVENTS})",
    ].join(" AND ")
  end

  def teams_for(organization)
    # We can't use User#teams_for because it returns every org team if
    # the user is an owner.  See User#teams_for for details about prefilling
    # TODO replace with a select on abilities JOIN teams
    list = this_user.teams.select { |team| team.organization_id == organization.id }
    list.each { |team| team.association(:organization).target = organization }
  end
end
