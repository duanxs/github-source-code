# frozen_string_literal: true

module Stafftools
  class Sponsors::WaitlistsController < StafftoolsController
    areas_of_responsibility :stafftools

    before_action :cast_boolean_params, only: :show

    PER_PAGE = 100

    ShowQuery = parse_query <<-'GRAPHQL'
      query(
        $filter: SponsorsMembershipFilters,
        $order: SponsorsMembershipOrder,
        $query: String,
        $first: Int,
        $after: String,
        $last: Int,
        $before: String
      ) {
        biztoolsInfo {
          ...Views::Stafftools::Sponsors::Waitlist::MembershipsList::BiztoolsInfo
        }
      }
    GRAPHQL

    def show
      variables = graphql_pagination_params(page_size: PER_PAGE)
      variables = variables.merge({
        query:  show_params[:query],
        filter: show_params[:filter],
        order: show_params[:order],
      })

      data = platform_execute(ShowQuery, variables: variables)
      view_params = {
        biztools_info: data.biztools_info,
        query:         show_params[:query],
        filter:        show_params[:filter],
        order:         show_params[:order],
      }

      respond_to do |format|
        format.html do
          if request.xhr?
            render partial: "stafftools/sponsors/waitlist/memberships_list", locals: view_params
          else
            render "stafftools/sponsors/waitlist/show", locals: view_params
          end
        end
      end
    end

    private

    def show_params
      @show_params ||= params.permit(:query, :utf8, :first, :after, :last,
        :before, filter: [:states, :spammy, :suspended, :type, :ignored, fiscalHost: [], billingCountry: []],
        order: [:field, :direction])
    end

    # cast the param value to actual boolean, otherwise GraphQL rejects it
    def cast_boolean_params
      %i(spammy suspended ignored).each do |attr|
        if (bool_param = show_params.dig(:filter, attr)).present?
          @show_params[:filter][attr] = ActiveModel::Type::Boolean.new.cast(bool_param)
        end
      end
    end
  end
end
