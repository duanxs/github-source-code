# frozen_string_literal: true

class Stafftools::Sponsors::FraudReviews::TransactionsController < Stafftools::SponsorsController
  skip_before_action :membership_required
  skip_before_action :listing_required
  before_action :this_review_required

  TRANSACTION_LIMIT = 10

  def index
    render partial: "stafftools/sponsors/fraud_reviews/transactions/show", locals: {
      sponsorable: sponsorable,
      transactions: transactions,
    }
  end

  private

  def this_review
    @this_review ||= SponsorsFraudReview.find_by(id: params[:fraud_review_id])
  end

  def this_review_required
    render_404 unless this_review.present?
  end

  def sponsors_listing
    @sponsors_listing ||= this_review.sponsors_listing
  end

  def sponsorable
    @sponsorable ||= sponsors_listing.sponsorable
  end

  def transactions
    @transactions ||= begin
      tiers = sponsors_listing.sponsors_tiers
      Transaction.for_subscribables(tiers.to_a)
        .where(action: Stafftools::Sponsors::Members::TransactionsController::SPONSORS_ACTIONS)
        .order(timestamp: :desc)
        .limit(TRANSACTION_LIMIT)
        .paginate(page: 1, per_page: TRANSACTION_LIMIT)
    end
  end
end
