# frozen_string_literal: true

class Stafftools::Sponsors::Waitlist::ExportsController < StafftoolsController
  def create
    report = SponsorsMembership::Export.new(filter: params[:filter] || :all)
    send_data(report.as_csv, type: "text/csv", filename: report.filename)
  end
end
