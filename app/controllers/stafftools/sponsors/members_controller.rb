# frozen_string_literal: true

class Stafftools::Sponsors::MembersController < Stafftools::SponsorsController
  include PlatformHelper

  before_action :membership_required, except: [:index]
  before_action :listing_required, except: [:index, :show, :update]

  INDEX_PER_PAGE = 20

  IndexQuery = parse_query <<-'GRAPHQL'
    query($cursor: String, $query: String, $perPage: Int!, $filterBy: SponsorsListingFilters) {
      biztoolsInfo {
        ...Views::Stafftools::Sponsors::Members::Index::BiztoolsInfo
        ...Views::Stafftools::Sponsors::Members::Listings::BiztoolsInfo
      }
    }
  GRAPHQL

  def index
    query = params[:query]
    current_type = params[:type]
    current_state = case params[:state]
    when nil
      "VERIFIED_AND_PENDING_APPROVAL"
    when "ALL"
      nil
    else
      params[:state]
    end

    filter_by = {}
    filter_by[:states] = [current_state] if current_state.present?
    filter_by[:type] = [current_type] if current_type.present?

    variables = {
      cursor: params[:cursor],
      query: query,
      perPage: INDEX_PER_PAGE,
      filterBy: filter_by,
    }

    data = platform_execute(IndexQuery, variables: variables)

    if data.errors.any?
      error_type = data.errors.details[:biztoolsInfo]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      flash[:error] = data.errors.messages.values.join(", ")
      return redirect_to stafftools_sponsors_members_path
    end

    locals = { data: data.biztools_info, current_state: params[:state], query: query, current_type: current_type }

    respond_to do |format|
      format.html do
        if request.xhr?
          render partial: "stafftools/sponsors/members/listings", locals: locals
        else
          render "stafftools/sponsors/members/index", locals: locals
        end
      end
    end
  end

  def show
    criteria = this_membership.sponsors_memberships_criteria.includes(:sponsors_criterion)

    automated_criteria, manual_criteria = criteria.partition do |criterion|
      criterion.sponsors_criterion.automated?
    end

    hooks = if this_listing
      Hook::StatusLoader.load_statuses(
        hook_records: this_listing.hooks,
        parent: this_listing,
      )
    end

    render_template_view "stafftools/sponsors/members/show",
      Stafftools::Sponsors::Members::ShowView, {
        listing: this_listing,
      },
      layout: "application",
      locals: {
        sponsorable: this_sponsorable,
        listing: this_listing,
        membership: this_membership,
        manual_criteria: manual_criteria,
        automated_criteria: automated_criteria,
        hooks: hooks,
      }
  end

  def update
    if this_membership.update(membership_params)
      flash[:notice] = "Successfully updated this Sponsors membership"
    else
      flash[:error] = this_membership.errors.full_messages.join(", ")
    end
  rescue ::SponsorsMembership::InvalidFiscalHostError => e
    flash[:error] = e.message
  ensure
    redirect_to stafftools_sponsors_member_path(this_sponsorable)
  end

  private

  def membership_params
    params.slice(:fiscal_host, :billing_country, :contact_email_id).permit!
  end
end
