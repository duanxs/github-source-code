# frozen_string_literal: true

class Stafftools::Sponsors::Members::TransfersPreviewPartialsController < Stafftools::SponsorsController
  before_action :stripe_connect_account_required

  TRANSFERS_PREVIEW_LIMIT = 15

  def show
    render Stafftools::Sponsors::Members::Transfers::ListComponent.new(
      sponsorable: this_sponsorable,
      stripe_account: this_listing.stripe_connect_account,
      page: current_page,
      limit: TRANSFERS_PREVIEW_LIMIT,
      paginate: false,
    )
  end
end
