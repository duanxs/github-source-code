# frozen_string_literal: true

class Stafftools::Sponsors::Members::Listing::ApprovalsController < Stafftools::SponsorsController
  CreateQuery = parse_query <<-'GRAPHQL'
    mutation($input: ApproveSponsorsListingInput!) {
      approveSponsorsListing(input: $input) {
        sponsorsListing {
          sponsorable {
            ... on Actor {
              login
              ...UserEntitySerializer::EntityQuery::AnalyticsUser
            }
          }
        }
      }
    }
  GRAPHQL

  def create
    input = { id: this_listing.global_relay_id }
    data = platform_execute(CreateQuery, variables: { input: input }, context: { hydro: true })

    if data.errors.any?
      error_type = data.errors.details[:approveSponsorsListing]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      flash[:error] = data.errors.messages.values.join(", ")
      return redirect_to stafftools_sponsors_member_path(this_sponsorable)
    end

    flash[:success] = "Approved #{this_sponsorable.login}'s GitHub Sponsors profile!'"

    redirect_to stafftools_sponsors_member_path(this_sponsorable)
  end

  DestroyQuery = parse_query <<-'GRAPHQL'
    mutation($input: UnpublishSponsorsListingInput!) {
      unpublishSponsorsListing(input: $input) {
        sponsorsListing {
          sponsorable {
            ... on Actor {
              login
            }
          }
        }
      }
    }
  GRAPHQL

  def destroy
    input = { id: this_listing.global_relay_id, message: params[:message] }
    data = platform_execute(DestroyQuery, variables: { input: input })

    if data.errors.any?
      error_type = data.errors.details[:unpublishSponsorsListing]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      flash[:error] = data.errors.messages.values.join(", ")
      return redirect_to stafftools_sponsors_member_path(this_sponsorable)
    end

    sponsorable = data.unpublish_sponsors_listing.sponsors_listing.sponsorable

    flash[:success] = "Unpublished #{sponsorable.login}'s GitHub Sponsors profile!"

    redirect_to stafftools_sponsors_member_path(this_sponsorable)
  end
end
