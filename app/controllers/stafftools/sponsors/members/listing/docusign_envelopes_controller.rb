# frozen_string_literal: true

class Stafftools::Sponsors::Members::Listing::DocusignEnvelopesController < Stafftools::SponsorsController
  def update
    if active_envelope&.completed?
      active_envelope.update(active: false)
    else
      ::Sponsors::Docusign::ReassignEnvelope.call(
        sponsorable: this_sponsorable,
        envelope: active_envelope,
        template_id: params[:template_id],
        reason: params[:reason],
      )
    end

    flash[:notice] = "Updated DocuSign tax template for #{this_sponsorable.login}"
    redirect_to stafftools_sponsors_member_path(this_sponsorable)
  rescue ::Sponsors::Docusign::VoidEnvelope::InvalidEnvelopeError,
         ::Sponsors::Docusign::CreateEnvelope::InvalidMembershipError,
         ::Sponsors::Docusign::TemplateError => error
    flash[:error] = error.message
    redirect_to stafftools_sponsors_member_path(this_sponsorable)
  rescue DocuSign_eSign::ApiError => error
    report_docusign_error(error)
    increment_error_stats(error_tag: "error:reassign_envelope")

    flash[:error] = error.message
    redirect_to stafftools_sponsors_member_path(this_sponsorable)
  end

  def destroy
    ::Sponsors::Docusign::VoidEnvelope.call \
      envelope: this_listing.active_docusign_envelope,
      reason: params[:voided_reason]
    flash[:notice] = "Voided DocuSign envelope for #{this_sponsorable.login}"
    redirect_to stafftools_sponsors_member_path(this_sponsorable)
  rescue ::Sponsors::Docusign::VoidEnvelope::InvalidEnvelopeError => error
    flash[:error] = error.message
    redirect_to stafftools_sponsors_member_path(this_sponsorable)
  rescue DocuSign_eSign::ApiError => error
    report_docusign_error(error)
    increment_error_stats(error_tag: "error:void_envelope")

    flash[:error] = error.message
    redirect_to stafftools_sponsors_member_path(this_sponsorable)
  end

  private

  def active_envelope
    @active_envelope ||= this_listing.active_docusign_envelope
  end

  def report_docusign_error(error)
    payload = {
      sponsorable_type: this_sponsorable.type,
      sponsorable_id: this_sponsorable.id,
      app: "github-external-request",
    }
    if error_body = error.response_body
      payload[:error_code] = JSON.parse(error_body)["message"]
    end

    Failbot.report(error, payload)
  end

  def increment_error_stats(error_tag:)
    GitHub.dogstats.increment("docusign", tags: ["source:sponsors", error_tag])
  end
end
