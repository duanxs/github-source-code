# frozen_string_literal: true

class Stafftools::Sponsors::Members::Listing::WebhooksController < Stafftools::SponsorsController
  def show
    render "stafftools/sponsors/members/listing/webhooks/show",
      layout: "application",
      locals: {
        sponsorable: this_sponsorable,
        membership: this_membership,
        hook: hook,
        hook_deliveries_query: params[:deliveries_q],
      }
  end

  def toggle_active_status
    hook.toggle!(:active)
    flash[:notice] = "Okay, the webhook was successfully #{hook_active_status}."
    redirect_to :back
  end

  private

  def hook
    hook_id = params[:webhook_id] || params[:id]
    return unless hook_id.present?
    @hook ||= this_listing.hooks.find(hook_id)
  end

  def hook_active_status
    hook.active? ? "enabled" : "disabled"
  end
end
