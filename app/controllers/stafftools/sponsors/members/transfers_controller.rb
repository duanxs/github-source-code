# frozen_string_literal: true

class Stafftools::Sponsors::Members::TransfersController < Stafftools::SponsorsController
  before_action :stripe_connect_account_required
  layout "application"

  def index
    render "stafftools/sponsors/members/transfers/index", locals: {
      sponsorable: this_sponsorable,
      membership: this_membership,
      stripe_account: this_listing.stripe_connect_account,
    }
  end
end
