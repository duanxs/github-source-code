# frozen_string_literal: true

class Stafftools::Sponsors::Members::IgnoresController < Stafftools::SponsorsController
  skip_before_action :listing_required

  def create
    this_membership.ignore!
    flash[:notice] = "Ignored Sponsors profile for #{this_sponsorable.login}."
    redirect_to stafftools_sponsors_member_path(this_membership.sponsorable)
  end

  def destroy
    this_membership.unignore!
    flash[:notice] = "Un-ignored Sponsors profile for #{this_sponsorable.login}."
    redirect_to stafftools_sponsors_member_path(this_membership.sponsorable)
  end
end
