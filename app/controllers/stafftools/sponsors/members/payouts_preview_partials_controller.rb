# frozen_string_literal: true

class Stafftools::Sponsors::Members::PayoutsPreviewPartialsController < Stafftools::SponsorsController
  before_action :stripe_connect_account_required

  PAYOUTS_PREVIEW_LIMIT = 15

  def show
    render Stafftools::Sponsors::Members::Payouts::ListComponent.new(
      stripe_account: this_listing.stripe_connect_account,
      limit: PAYOUTS_PREVIEW_LIMIT,
    )
  end
end
