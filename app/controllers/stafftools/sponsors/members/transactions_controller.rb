# frozen_string_literal: true

class Stafftools::Sponsors::Members::TransactionsController < Stafftools::SponsorsController
  layout "application"

  PER_PAGE = 50
  SPONSORS_ACTIONS = %w(sp_added sp_cancelled sp_changed).freeze

  def index
    render "stafftools/sponsors/members/transactions/index", locals: {
      sponsorable: this_sponsorable,
      membership: this_membership,
      transactions: transactions,
    }
  end

  private

  def tiers
    @tiers ||= this_sponsorable.sponsors_listing.sponsors_tiers
  end

  def transactions
    # Sending in tiers.to_a to the scope prevents it from blowing up due to
    # cross domain tables query errors.
    @transactions ||=
      Transaction.for_subscribables(tiers.to_a).
        where(action: SPONSORS_ACTIONS).
        order(timestamp: :desc).
        paginate(page: current_page, per_page: PER_PAGE)
  end
end
