# frozen_string_literal: true

class Stafftools::Sponsors::FraudReviewsController < Stafftools::SponsorsController
  skip_before_action :membership_required
  skip_before_action :listing_required
  before_action :this_review_required, only: [:show, :update]

  layout "application"

  PER_PAGE = 100

  def index
    render "stafftools/sponsors/fraud_reviews/index", locals: {
      reviews: reviews,
      state: sanitized_state,
    }
  end

  def show
    flagged_records = this_review.fraud_flagged_sponsors.includes(:sponsor)

    render "stafftools/sponsors/fraud_reviews/show", locals: {
      review: this_review,
      listing: sponsors_listing,
      sponsorable: sponsorable,
      membership: membership,
      automated_criteria: automated_criteria,
      pinned_repos: pinned_repos,
      flagged_records: flagged_records,
    }
  end

  def update
    success = case sanitized_state
    when :resolved
      this_review.resolve(actor: current_user)
    when :flagged
      this_review.flag(actor: current_user)
    when :pending
      this_review.revert_to_pending(actor: current_user)
    end

    if success
      flash[:notice] = "Marked as #{sanitized_state}."
    else
      flash[:error] = "Couldn't update review: #{this_review.errors.full_messages.to_sentence}"
    end

    redirect_to stafftools_sponsors_fraud_review_path(this_review)
  end

  private

  def this_review
    @this_review ||= SponsorsFraudReview.find_by(id: params[:id])
  end

  def this_review_required
    render_404 unless this_review.present?
  end

  def sponsors_listing
    @sponsors_listing ||= this_review.sponsors_listing
  end

  def sponsorable
    @sponsorable ||= sponsors_listing.sponsorable
  end

  def membership
    @membership ||= sponsorable.sponsors_membership
  end

  def reviews
    @reviews ||= SponsorsFraudReview
      .where(state: sanitized_state)
      .includes(:reviewer, sponsors_listing: :sponsorable)
      .sort_by { |fr| fr.sponsors_listing.next_payout_date }
      .paginate(page: current_page, per_page: PER_PAGE)
  end

  def automated_criteria
    @automated_criteria ||= membership.automated_criteria
  end

  def pinned_repos
    @pinned_repos ||= sponsorable.pinned_repositories.includes(:owner)
  end

  def sanitized_state
    if SponsorsFraudReview.states.keys.include?(params[:state])
      params[:state].to_sym
    else
      :pending
    end
  end
end
