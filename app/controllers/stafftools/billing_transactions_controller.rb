# frozen_string_literal: true
class Stafftools::BillingTransactionsController < StafftoolsController
  areas_of_responsibility :stafftools

  before_action :ensure_billing_enabled

  def search
    @billing_transaction = Billing::BillingTransaction.
      includes(:live_user).find_by_transaction_id(params[:transaction_id])

    if @billing_transaction && @billing_transaction.live_user.present?
      flash[:notice] = "Transaction found for live user"
      redirect_to stafftools_user_billing_history_path(@billing_transaction.live_user)
    else
      render "stafftools/billing_transactions/search"
    end
  end
end
