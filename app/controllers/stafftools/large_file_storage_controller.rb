# frozen_string_literal: true
class Stafftools::LargeFileStorageController < StafftoolsController
  areas_of_responsibility :repositories, :stafftools

  include ActionView::Helpers::NumberHelper

  before_action :ensure_billing_enabled
  before_action :ensure_user_exists, only: [:show, :edit, :update]

  def index
    sort = params[:sort]
    sort = "bandwidth_down" unless %w(storage bandwidth_down bandwidth_up).include?(sort)

    @statuses = Asset::Status.
      where("asset_type = 0 AND (storage > 0 OR bandwidth_down > 0)").
      order("#{sort} desc").
      includes(:owner).
      limit(30).
      page(current_page)

    render "stafftools/large_file_storage/index"
  end

  LfsShowQuery = parse_query <<-'GRAPHQL'
    query(
      $id: ID!
      $first: Int,
      $last: Int,
      $after: String,
      $before: String
    ) {
      node(id: $id) {
        ...Views::Stafftools::LargeFileStorage::Show::Account
        ...Stafftools::LargeFileStorageView::LfsQuery
      }
    }
  GRAPHQL

  def show
    variables = { id: this_user.global_relay_id }.merge(graphql_pagination_params(page_size: 35))

    data = platform_execute(LfsShowQuery, variables: variables)
    return render_404 unless data.node

    actor_statuses = Asset::ActorStatus.lfs.
      where(owner_id: this_user.id).
      includes([:actor, :owner]).
      order("bandwidth_down desc").
      limit(30).
      page(current_page)

    lfs_networks = Platform::Loaders::LfsNetworksByUsage.load(this_user.id).sync
    view = Stafftools::LargeFileStorageView.new(account: data.node, actor_statuses: actor_statuses)
    locals = { data: data.node, actor_statuses: actor_statuses, view: view, lfs_networks: lfs_networks }

    case this_user.site_admin_context
    when "organization"
      render "stafftools/large_file_storage/show", layout: "stafftools/organization/content", locals: locals
    when "user"
      render "stafftools/large_file_storage/show", layout: "stafftools/user/content", locals: locals
    else
      render_404
    end
  end

  def edit
    total_packs = (params[:packs] || this_user.data_packs).to_i
    @data_pack_change = Billing::PlanChange::DataPackChange.new(this_user,
      total_packs: total_packs)

    # Make sure the browser caches AJAX responses separately from regular HTML responses.
    response.headers["Vary"] = "X-Requested-With"

    respond_to do |format|
      format.json do
        render json: {
          url: stafftools_user_edit_large_file_storage_path(this_user, packs: total_packs),
          selectors: {
            ".unstyled-total-data-packs"       => @data_pack_change.total_packs,
            ".unstyled-total-data-packs-label" => "data pack".pluralize(@data_pack_change.total_packs),
            ".unstyled-total-price"             => @data_pack_change.total_price.format(sign_before_symbol: true),
            ".unstyled-renewal-price"           => @data_pack_change.renewal_price.format,
            ".unstyled-pack-storage-total"      => number_with_delimiter(@data_pack_change.storage_quota.round),
            ".unstyled-pack-bandwidth-total"    => number_with_delimiter(@data_pack_change.bandwidth_quota.round),
          },
        }
      end

      format.html do
        render "stafftools/large_file_storage/edit", layout: "stafftools/user/billing"
      end
    end
  end

  def update
    total_packs = params[:asset_status] && params[:asset_status][:total_packs]
    total_packs ||= this_user.data_packs
    asset_status = this_user.asset_status || this_user.build_asset_status

    asset_status.update_data_packs(quantity: total_packs, actor: current_user)

    flash[:notice] = "Successfully updated data plan."
    redirect_to billing_stafftools_user_path(this_user)
  end

  def bandwidth_for_repo(lfs_networks, nwo)
    return nil if GitHub.enterprise?
    repo = Repository.nwo(nwo)
    lfs_networks && lfs_networks[repo&.network&.id]
  end
  helper_method :bandwidth_for_repo
end
