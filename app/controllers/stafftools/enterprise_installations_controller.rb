# frozen_string_literal: true

class Stafftools::EnterpriseInstallationsController < StafftoolsController
  layout "stafftools/enterprise_installation"

  def index
    installations = EnterpriseInstallation.order("created_at desc").paginate(page: params[:page])
    render "stafftools/enterprise_installations/index",
      layout: "stafftools", locals: { installations: installations }
  end

  def show
    render "stafftools/enterprise_installations/show",
      locals: { installation: this_enterprise_installation }
  end

  def accounts
    render "stafftools/enterprise_installations/accounts",
      locals: {
        installation: this_enterprise_installation,
        accounts: this_enterprise_installation
          .user_accounts
          .includes(:emails)
          .order(login: :asc)
          .paginate(page: current_page)
      }
  end

  def contributions
    render "stafftools/enterprise_installations/contributions",
      locals: {
        installation: this_enterprise_installation,
        contributions: this_enterprise_installation
          .enterprise_contributions.order(updated_at: :desc)
          .group("user_id")
          .paginate(page: current_page)
      }
  end

  def destroy
    this_enterprise_installation.destroy
    redirect_to \
      stafftools_enterprise_installations_path,
      notice: "Enterprise Server installation removed."
  end

  def block
    EnterpriseInstallation.block(this_enterprise_installation.license_hash)
    redirect_to \
      stafftools_enterprise_installation_path(this_enterprise_installation),
      notice: "Enterprise Server installation license blocked."
  end

  def unblock
    EnterpriseInstallation.unblock(this_enterprise_installation.license_hash)
    redirect_to \
      stafftools_enterprise_installation_path(this_enterprise_installation),
      notice: "Enterprise Server installation license unblocked."
  end

  private

  def this_enterprise_installation
    @installation ||= EnterpriseInstallation.find(params[:id])
  end
  helper_method :this_enterprise_installation
end
