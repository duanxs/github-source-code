# frozen_string_literal: true

class Stafftools::PurgatoryController < StafftoolsController
  areas_of_responsibility :stafftools

  def restore
    if params[:lock] == "true"
      repo = Archived::Repository.where(id: params[:id]).first
      repo.lock_excluding_descendants!
    end

    RepositoryRestoreJob.perform_later(params[:id].to_i, current_user.id)
    respond_to do |format|
      format.html do
        if request.xhr?
          render partial: "stafftools/purgatory/restoring", locals: { repo_id: params[:id] }
        else
          redirect_to :back
        end
      end
    end
  end

  def restore_bulk
    return render status: 422, json: { error: "no ids given" } unless params[:ids].is_a?(Array)

    repo_ids = params[:ids].map(&:to_i)
    JobStatus.create(id: BulkRepositoryRestoreJob.job_id(repo_ids))
    BulkRepositoryRestoreJob.perform_later(repo_ids, current_user.id)
    respond_to do |format|
      format.json { render json: {job: {url: job_status_url(BulkRepositoryRestoreJob.job_id(repo_ids))} } }
    end
  end

  def restore_partial
    respond_to do |format|
      format.html do
        render partial: "stafftools/purgatory/restoring", locals: { repo_id: params[:id] }
      end
    end
  end

  def purge
    RepositoryPurgeJob.perform_later(params[:id].to_i)
    respond_to do |format|
      format.html do
        if request.xhr?
          render partial: "stafftools/purgatory/purging", locals: { repo_id: params[:id] }
        else
          redirect_to :back
        end
      end
    end
  end

  def restore_status
    job = GitHub::Jobs::RepositoryRestore.new(params[:id])
    msg = job.safe_message
    render html: msg, status: job.status(msg)
    job.reset_message_if_finished
  end

  def purge_status
    job = GitHub::Jobs::RepositoryPurge.new(params[:id])
    status = job.status

    http = :accepted
    msg = case status
      when :accepted
        "Queued..."
      when :purging
        "Purging data..."
      when :pending
        "Pending..."
      when :ok
        http = :ok
        "Done!"
      when :failed
        http = :ok
        "Failed"
      else
        "Unknown: #{status}"
    end

    render html: msg, status: http
  end
end
