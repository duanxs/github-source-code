# frozen_string_literal: true

class Stafftools::NetworksController < StafftoolsController
  map_to_service :repo_admin
  map_to_service :git_data_integrity, only: [:schedule_maintenance_for_failed, :mark_as_broken, :schedule_maintenance]

  include ActionView::Helpers::TextHelper

  before_action :disable_for_private_instance, only: %w(
    index schedule_maintenance_for_failed
  )
  before_action :ensure_repo_exists, except: [
    :index, :schedule_maintenance, :schedule_maintenance_for_failed
  ]

  NETWORKS_PER_PAGE = 20

  layout :new_nav_layout
  private def new_nav_layout
    if action_name == "index"
      "stafftools"
    else
      "stafftools/repository/collaboration"
    end
  end

  def index
    @status_names = RepositoryNetwork::MAINTENANCE_STATUSES
    @show_status = params[:maintenance_status] || "failed"
    @networks = RepositoryNetwork
      .where(maintenance_status: @show_status)
      .order("last_maintenance_attempted_at ASC")
      .paginate(page: current_page, per_page: NETWORKS_PER_PAGE)
    render "stafftools/networks/index"
  end

  def schedule_maintenance_for_failed
    networks = RepositoryNetwork
      .where(maintenance_status: "failed")
      .order("last_maintenance_attempted_at ASC")
      .limit(NETWORKS_PER_PAGE)
    networks.each do |network|
      network.schedule_maintenance
    end
    flash[:notice] = "Maintenance scheduled for #{pluralize(networks.size, "failed network")}"
    redirect_to :back
  end

  def show
    render "stafftools/networks/show"
  end

  # View the detailed (and often slow) network tree
  def tree
    @repo_map = current_repository.network.full_network_tree
    render "stafftools/networks/tree"
  end

  def children
    @repos = sort_by_owner_login(current_repository.children.preload(:owner))
    @title = "Children"
    render "stafftools/networks/children"
  end

  def siblings
    @repos = sort_by_owner_login(current_repository.parent.children.preload(:owner))
    @repos -= [current_repository]
    @title = "Siblings"
    render "stafftools/networks/children"
  end

  # Makes this repo the root of its network
  def change_root
    current_repository.make_network_root
    flash[:notice] = "Making repository the root of the network…"
    redirect_to :back
  end

  # Disconnect this repo from its network
  def detach
    current_repository.detach
    flash[:notice] = "Detaching…"
    redirect_to :back
  end

  # Extract this repo and all forks from its network
  def extract
    current_repository.extract
    flash[:notice] = "Extracting…"
    redirect_to :back
  end

  # Use experimental new extract code to extract this
  # repo and all forks from its network
  def new_extract
    current_repository.new_extract
    flash[:notice] = "Extracting… please check haystack for errors"
    redirect_to :back
  end

  # Reattach this repo and all forks to its parent network
  def reattach
    current_repository.reattach
    flash[:notice] = "Reattaching…"
    redirect_to :back
  end

  def attach_to
    return render_404 unless GitHub.flipper[:network_family_attach].enabled?(current_user)

    begin
      current_repository.attach_to(params[:attach_to_repo_id])
      flash[:notice] = "Attaching…"
    rescue RuntimeError => e
      flash[:error] = e.message
    end
    redirect_to :back
  end

  # Change a forked repositories parent to a different repository in the same network
  # When a repository is the root of a network of forks the root will need to be reassigned
  # in order to leave the network in a good state when that repository is reparented.
  def reparent
    new_parent = Repository.nwo(params[:new_parent])

    begin
      current_repository.reparent! new_parent
      nwo = current_repository.name_with_owner
      new_nwo = new_parent.name_with_owner
      flash[:notice] = "#{nwo} is now a child of #{new_nwo}"
    rescue ArgumentError, Repository::InvalidAncestryError => e
      flash[:error] = e.message
    end

    redirect_to :back
  end

  # Rebuilds the data used by the network graph
  def rebuild_network_graph
    current_repository.network_graph.force_build
    flash[:notice] = "Network graph rebuild job enqueued"
    redirect_to :back

  rescue GitHub::DGit::UnroutedError
    flash[:error] = "Repository offline"
    redirect_to :back
  end

  # Increment the git cache. Useful as a final step after removing sensitive data.
  # see app/models/repository/rpc_dependency for more information
  def increment_git_cache
    current_repository.increment_cache_version!
    flash[:notice] = "Repository Git cache invalidated"
    redirect_to :back
  end

  def schedule_maintenance
    if current_repository
      current_repository.network.schedule_maintenance
      flash[:notice] = "Network maintenance scheduled for network #{current_repository.network.id}"
    else
      flash[:error] = "The root repository of the network no longer exists"
    end
    redirect_to :back
  end

  def mark_as_broken
    current_repository.network.mark_as_broken
    flash[:warn] = "The repository and all its forks have been marked as broken"
    redirect_to :back
  end

  private

  def sort_by_owner_login(repos)
    repos.sort_by { |r| r.owner.login }
  end
end
