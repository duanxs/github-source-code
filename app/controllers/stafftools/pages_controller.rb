# frozen_string_literal: true

require "github/pages/domain_health_checker"

class Stafftools::PagesController < StafftoolsController
  areas_of_responsibility :pages, :stafftools

  before_action :ensure_pages_enabled
  before_action :ensure_repo_exists
  before_action :ensure_https_enabled, only: [
    :https_status,
    :request_https_certificate,
    :delete_https_certificate,
  ]

  layout "stafftools/repository/storage"

  def show
    render "stafftools/pages/show"
  end

  def status
    @health_check = GitHub::Pages::DomainHealthChecker.new(domain).check

    render "stafftools/pages/status", layout: false
  end

  def https_status
    render_template_view "stafftools/pages/https_status",
      Stafftools::RepositoryViews::PagesHTTPSStatusView, {
        current_repository: current_repository,
        page: current_repository.page,
      }, layout: false
  end

  def create
    pusher = current_repository.gh_pages_rebuilder(current_user)
    ref_name = current_repository.pages_branch

    # If deployment ID was given, build its ref name.
    page_deployment_id = params[:page_deployment_id].to_i
    if page_deployment_id > 0
      deployment = current_repository.page.deployments.where(id: page_deployment_id).first
      ref_name = deployment.ref_name if deployment
    end

    if pusher && current_repository.rebuild_pages(pusher, git_ref_name: ref_name)
      flash[:notice] = "Rebuilding the pages site for #{current_repository.name_with_owner} at ref '#{ref_name}'."
    else
      flash[:error] = "Couldn’t rebuild the pages site for #{current_repository.name_with_owner} at ref '#{ref_name}'."
    end
    redirect_to :back
  end

  def destroy
    page_deployment_id = params[:page_deployment_id].to_i

    if current_repository.page && page_deployment_id > 0
      deployment = current_repository.page.deployments.where(id: page_deployment_id).first
      if deployment
        if deployment.destroy
          flash[:notice] = "Deleted the deployment of #{current_repository.name_with_owner} with ref '#{deployment.ref_name}'."
        else
          flash[:notice] = "Failed to delete the deployment of #{current_repository.name_with_owner} with ref '#{deployment.ref_name}'."
        end
      else
        flash[:error] = "#{current_repository.name_with_owner} does not have a deployment with id '#{page_deployment_id}'."
      end
    else
      flash[:error] = "#{current_repository.name_with_owner} does not have Pages or a page deployment with the given ID."
    end

    redirect_to :back
  end

  def clear_generated_pages
    page_deployment_id = params[:page_deployment_id].to_i

    if current_repository.page && page_deployment_id > 0
      # New hotness: a Page::Deployment!
      deployment = current_repository.page.deployments.where(id: page_deployment_id).first
      if deployment
        deployment.unpublish
        flash[:notice] = "Deleted the generated pages site for #{current_repository.name_with_owner} with ref '#{deployment.ref_name}'."
      else
        flash[:error] = "#{current_repository.name_with_owner} does not have a deployment with id '#{page_deployment_id}'."
      end
    elsif current_repository.page
      # Old & busted: pages replicas relating directly to page
      current_repository.page.unpublish
      flash[:notice] = "Deleted the generated pages site for #{current_repository.name_with_owner}."
    else
      flash[:error] = "#{current_repository.name_with_owner} does not have a generated pages site to delete."
    end

    redirect_to :back
  end

  def clear_domain
    domain = current_repository.page.cname
    if domain
      current_repository.page.clear_cname
      flash[:notice] = "Cleared the #{domain} domain from #{current_repository.name_with_owner}."
    else
      flash[:error] = "No custom domain for #{current_repository.name_with_owner}."
    end
    redirect_to :back
  end

  def request_https_certificate
    current_page = current_repository.page

    domain = current_page.cname
    unless domain
      flash[:error] = "No custom domain for #{current_repository.name_with_owner}."
      redirect_to :back
      return
    end

    cert = current_page.certificate

    if cert.present?
      if cert.usable?
        flash[:notice] = "Certificate already exists for #{domain} and is usable."
      else
        cert.resume_flow
        flash[:notice] = "Certificate already exists for #{domain} in state '#{cert.current_state}'. Resuming the process."
      end
    else
      if current_page.eligible_for_certificate?
        if current_page.create_cname_certificate
          flash[:notice] = "Requesting a certificate for #{domain}. It can take up to an hour to propagate."
        else
          flash[:error] = "Something went wrong issuing a certificate for #{domain}. Please contact the Pages team."
        end
      else
        flash[:error] = "Domain #{domain} is not eligible for HTTPS at this time."
      end
    end

    redirect_to :back
  end

  def delete_https_certificate
    current_page = current_repository.page

    domain = current_page.cname
    unless domain
      flash[:error] = "No custom domain for #{current_repository.name_with_owner}."
      redirect_to :back
      return
    end

    cert = current_page.certificate

    if cert.present?
      cert.destroy
      flash[:notice] = "Certificate for #{domain} destroyed."
    else
      flash[:error] = "No certificate present for #{domain}."
    end

    redirect_to :back
  end

  private

  def ensure_pages_enabled
    render_404 unless GitHub.pages_enabled?
  end

  def ensure_https_enabled
    render_404 unless GitHub.pages_custom_domain_https_enabled?
  end

  def domain
    if current_repository.page && current_repository.page.cname
      current_repository.page.cname
    elsif GitHub.enterprise?
      current_repository.pages_host_name
    else
      subdomain = "#{current_repository.owner.to_s.downcase}"
      "#{subdomain}.#{current_repository.pages_host_name}"
    end
  end
end
