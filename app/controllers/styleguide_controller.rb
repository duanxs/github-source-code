# frozen_string_literal: true

class StyleguideController < ApplicationController
  stylesheet_bundle :site
  areas_of_responsibility :design_systems

  skip_before_action :perform_conditional_access_checks

  def index
    redirect_to "https://styleguide.github.com/"
  end
end
