# frozen_string_literal: true

require "github/pages/domain_health_checker"

class PagesController < AbstractRepositoryController
  areas_of_responsibility :pages

  # Permissions
  before_action :ensure_repo_writable, except: [:status, :https_status, :themes]
  before_action :ensure_repo_write_access, only: [:toggle_preview_deployment]
  before_action :ensure_repo_admin_access, except: [:toggle_preview_deployment, :source]
  before_action :manage_settings_pages_permissions_required, only: :source

  # Feature availability
  before_action :ensure_repo_has_page, except: [:source, :themes, :theme]
  before_action :ensure_https_redirect_available, only: :https_redirect
  before_action :ensure_https_redirect_enabled, only: :https_status
  before_action :ensure_preview_deployments_enabled, only: :toggle_preview_deployment
  before_action :ensure_cname_available, only: :cname
  before_action :ensure_source_available, only: :source
  before_action :ensure_theme_chooser_available, only: [:themes, :theme]

  # status fragment should not be redirected e.g. for staff billing lock
  # note: status is still subject to admin_only restriction
  skip_before_action :ask_the_gatekeeper, only: [:status, :https_status]

  before_action :add_csp_exceptions, only: :themes
  CSP_EXCEPTIONS = {
    frame_src: [GitHub.pages_themes_hostname],
    img_src: [GitHub.pages_themes_hostname],
  }

  javascript_bundle :settings
  include PagesHelper

  # show themes
  def themes
    render_template_view "edit_repositories/admin_screen/pages_themes",
      EditRepositories::AdminScreen::PagesSourceView, {
        repository: current_repository,
        params: params,
      }, layout: "application"
  end

  # Set the Jekyll theme on a page
  # Creates page and sets source if necessary.
  # If no theme present, run JekyllSite creator and try to migrate APG content.
  def theme
    slug = params[:page] && params[:page][:theme_slug]
    new_theme = slug.present? ? GitHub::Pages::JekyllTheme.find(slug) : nil
    if new_theme.nil?
      flash[:error] = %(Unknown theme "#{slug}")
    else
      page = current_repository.page || current_repository.build_page
      apg_branch = page.source_branch # set_source may change the branch
      if !GitHub.flipper[:pages_any_branch].enabled?(current_repository.owner)
        page.set_source(params[:source])
      else
        page.set_source(nil, params[:source], "/")
      end

      if page.theme.present?
        page.write_theme(new_theme.gem, current_user)
      else
        theme_site = GitHub::Pages::JekyllSite.new page
        theme_site.create(new_theme.gem, apg_branch, current_user)
      end

      GitHub.dogstats.increment("pages.theme_chooser", tags:         dogstats_request_tags << "theme:#{new_theme.gem}" <<
          "apg_migrated:#{theme_site && theme_site.apg_migrated? ? "yes" : "no"}")

      message = %(Jekyll theme set to "#{new_theme.name}".).dup
      message << " Automatic Page Generator content migrated." if theme_site && theme_site.apg_migrated?
      flash[:notice] = message
    end
  rescue Git::Ref::ProtectedBranchUpdateError
    flash[:error] = "Unable to commit _config.yml to protected branch."
  ensure
    editor = theme_site && theme_site.edit_content_path
    redirect_to(editor || edit_repository_path(current_repository))
  end

  # set page source
  # allowed on project repo, even when no page exists yet
  # triggers page rebuild if source changed
  def source
    new_source = params[:source]
    if !GitHub.flipper[:pages_any_branch].enabled?(current_repository.owner)
      if ["gh-pages", "master", "master /docs", "none"].include? new_source
        page = current_repository.page || current_repository.build_page

        # Set a subdomain if the page supports subdomains
        # Otherwise this returns `nil`
        page.toggle_subdomain

        old_source = page.source
        page.set_source(new_source)
        if page.source != old_source
          current_repository.rebuild_pages current_user
          flash[:notice] = "GitHub Pages source saved."
        end
      else
        flash[:error] = "Invalid GitHub Pages source."
      end
    else
      new_source_dir = params[:source_dir]
      is_user_pages_repo = current_repository.is_user_pages_repo?
      if !Page::VALID_SUBDIRS.include?(new_source_dir)
        flash[:error] = "Invalid GitHub Pages source folder."
      # Disable delete Pages if current repository is user repo or current repository is a project repository and contains gh-pages branch.
      elsif new_source.nil? && (is_user_pages_repo || (!is_user_pages_repo && current_repository.has_gh_pages_branch?))
        flash[:error] = "Pages cannot be disabled for this repository."
      else
        no_page = current_repository.page.nil?
        page = current_repository.page || current_repository.build_page
        # Set a subdomain if the page supports subdomains
        # Otherwise this returns `nil`
        page.toggle_subdomain
        old_source = page.source_ref_name
        old_source_subdir = page.source_subdir
        if new_source.nil?
          page.clear_source
        else
          page.set_source(nil, new_source, new_source_dir)
        end
        if no_page || page.source_ref_name != old_source || page.source_subdir != old_source_subdir
          current_repository.rebuild_pages current_user
          flash[:notice] = "GitHub Pages source saved."
        end
      end
    end
    redirect_to edit_repository_path(current_repository)
  end

  # render fragment to show pages page status with build or cname error message
  def status
    if current_repository.plan_supports?(:pages) || (current_user.enabled? && current_repository.unsupported_pages?)
      render_template_view "edit_repositories/admin_screen/pages_status",
        EditRepositories::AdminScreen::PagesStatusView, {
          repository: current_repository,
          cname_error: cname_error,
          user: current_user,
        }, layout: false
    else
      render_404
    end
  end

  def certificate_status
    cert_status = get_certificate_status(current_repository)
    return render_404 unless cert_status
    return render_404 if cert_status == :approved && params[:display_approval_state].blank?
    render_template_view "edit_repositories/admin_screen/_pages_certificate_status",
    EditRepositories::AdminScreen::PagesCertificateStatusView, {
      state: cert_status,
      cname: current_repository.page.cname
    }, layout: false
  end

  # render fragment to show pages custom domain https status
  def https_status
    bump_certificate

    render_template_view "edit_repositories/admin_screen/pages_https",
      EditRepositories::AdminScreen::PagesHTTPSView, {
        current_repository: current_repository,
        current_user: current_user,
      }, layout: false
  end

  def cname
    if (cname = current_repository.page.write_cname(params[:cname], current_user))
      if cname.blank?
        flash[:notice] = "Custom domain removed. Please remember to remove any GitHub Pages DNS records for this domain if you do not plan to continue using it with GitHub Pages."
      else
        flash[:notice] = "Custom domain \"#{cname}\" saved."
      end
      GitHub.dogstats.increment "pages", tags: ["action:cname"]
    else
      flash[:notice] = "No changes to custom domain."
    end
  rescue Page::InvalidCNAME => e
    flash[:error] = e.message
  rescue Git::Ref::ProtectedBranchUpdateError
    flash[:error] = "Unable to commit CNAME to protected branch."
  ensure
    redirect_to edit_repository_path(current_repository)
  end

  def https_redirect
    enable = params[:pages_https_redirect] == "1"
    if current_repository.page.update(https_redirect: enable)
      if request.xhr?
        head :ok
      else
        flash[:notice] = "HTTPS redirects " + (enable ? "enabled" : "disabled")
        redirect_to :back
      end
    else
      flash[:error] = current_repository.page.errors.full_messages.to_sentence
    end
  end

  def toggle_preview_deployment
    ref_name = params[:pages_ref_name]

    if ref_name.to_s.empty?
      if request.xhr?
        head :bad_request
      else
        notice[:error] = "Please send a git branch name in the payload."
        redirect_to :back
      end

      return
    end

    if current_repository.pages_branch.to_s == ref_name
      if request.xhr?
        head :bad_request
      else
        notice[:error] = "You may disable the deployment for the source branch in your repository settings."
        redirect_to :back
      end

      return
    end

    existing_deployments = current_repository.page.deployments_for(ref_name)
    if existing_deployments.exists?
      existing_deployments.destroy_all
      if request.xhr?
        respond_to do |format|
          format.json do
            render json: { new_button_value: "Enable GitHub Pages" }
          end
        end
      else
        flash[:notice] = "Your preview site has been deleted."
        redirect_to :back
      end
    else
      current_repository.rebuild_pages(current_user, git_ref_name: ref_name)
      if request.xhr?
        respond_to do |format|
          format.json do
            render json: { new_button_value: "Disable GitHub Pages" }
          end
        end
      else
        flash[:notice] = "Your preview site will begin building shortly."
        redirect_to :back
      end
    end
  end

  def visibility
    return render_404 unless private_pages_enabled?
    return render_404 unless current_repository.plan_supports?(:private_pages)
    # User/org pages aren't currently allowed to have Private pages
    return render_404 if current_repository.is_user_pages_repo?
    # Public repos cannot change their visibility
    return render_404 unless current_repository.private?

    if current_repository.page.update(public: params[:public])
      # This will serve to flush the cache from Fastly for this page, we want to force Fastly to fetch from origin when changing
      # a page visibility from Public to Private or vice-versa
      current_repository.rebuild_pages current_user
      # Publish visibility change event to Hydro
      GlobalInstrumenter.instrument "pages.visibility_change", {
        actor: current_user,
        page: current_repository.page,
        public: current_repository.page.public
      }
      if request.xhr?
        head :ok
      else
        flash[:notice] = "GitHub Page visibility is #{params[:public] == "true" ? "public" : "private"}"
        redirect_to :back
      end
    else
      flash[:error] = current_repository.page.errors.full_messages.to_sentence
      redirect_to edit_repository_path(current_repository)
    end
  end

  private

  def ensure_repo_writable
    render_404 unless current_repository.writable?
  end

  def ensure_repo_write_access
    render_404 unless current_repository.writable_by?(current_user)
  end

  def ensure_repo_admin_access
    render_404 unless current_repository.adminable_by?(current_user)
  end

  def manage_settings_pages_permissions_required
    render_404 unless current_repository.async_can_toggle_page_settings?(current_user).sync
  end

  def ensure_repo_has_page
    render_404 unless repo_has_page?
  end

  def repo_has_page?
    current_repository.has_gh_pages? && current_repository.page
  end

  def ensure_https_redirect_available
    render_404 unless https_redirect_toggleable?
  end

  def https_redirect_toggleable?
    repo_has_page? && current_repository.page.https_redirect_toggleable?
  end

  def ensure_https_redirect_enabled
    render_404 unless GitHub.pages_https_redirect_enabled?
  end

  def ensure_preview_deployments_enabled
    render_404 unless preview_deployments_toggleable?
  end

  def preview_deployments_toggleable?
    current_user_feature_enabled?(:pages_preview_builds) &&
      current_repository.page&.preview_deployments_enabled?
  end

  def ensure_cname_available
    render_404 unless GitHub.pages_custom_cnames?
  end

  def ensure_source_available
    render_404 if current_repository.is_user_pages_repo? &&
      !GitHub.flipper[:pages_any_branch].enabled?(current_repository.owner)
  end

  def ensure_theme_chooser_available
    render_404 if !GitHub.pages_gem_themes_enabled? || current_repository.page&.nojekyll?
  end

  # return cname error message or nil
  # feature flagged
  def cname_error
    message = nil
    if GitHub.pages_custom_cnames?
      # built-in cname validation
      message = current_repository.page.cname_error
      # perform DNS check only if everything else looks peachy
      # note: dns_check may block for up to 2s
      if message.nil? && current_repository.gh_pages_success? && current_repository.page.cname?
        message = dns_check current_repository.page.cname
      end
    end
    message
  end

  # instrumented DNS healthcheck - may block for up to 2s
  def dns_check(cname)
    check = GitHub::Pages::DomainHealthChecker.new(cname)
    return nil if check.valid?

    check.reason.message_with_url
  end

  # If a certificate exists, bump it.
  # If no certificate exists, but the domain is eligible, create it.
  def bump_certificate
    return unless current_repository&.page
    return unless current_repository.page.cname? || current_repository.page.subdomain?

    page = current_repository.page

    if (cert = page.certificate)
      cert.resume_flow if (!cert.usable? || cert.needs_renewal?)
    else
      if page.eligible_for_certificate?
        if page.cname?
          ActiveRecord::Base.connected_to(role: :writing) { page.create_cname_certificate }
        end
        if page.subdomain?
          ActiveRecord::Base.connected_to(role: :writing) { page.create_subdomain_certificate }
        end
      end
    end

    page.reload

    nil
  end
end
