# frozen_string_literal: true

class MarketplaceListingScreenshotsController < ApplicationController
  areas_of_responsibility :marketplace

  before_action :marketplace_required
  before_action :login_required
  before_action :require_xhr, only: [:update, :destroy]

  UpdateQuery = parse_query <<-'GRAPHQL'
    mutation($input: UpdateMarketplaceListingScreenshotInput!) {
      updateMarketplaceListingScreenshot(input: $input) {
        marketplaceListingScreenshot
      }
    }
  GRAPHQL

  ResequenceQuery = parse_query <<-'GRAPHQL'
    mutation($input: ResequenceMarketplaceListingScreenshotInput!) {
      resequenceMarketplaceListingScreenshot(input: $input) {
        marketplaceListingScreenshot
      }
    }
  GRAPHQL

  def update
    input = params[:marketplace_listing_screenshot].
              permit(:caption, :previousScreenshotId).
              merge(id: params[:id])

    if input.has_key?(:previousScreenshotId)
      mutation = :resequenceMarketplaceListingScreenshot
      data = platform_execute(ResequenceQuery, variables: { input: input })
    else
      mutation = :updateMarketplaceListingScreenshot
      data = platform_execute(UpdateQuery, variables: { input: input })
    end

    if data.errors.any?
      error_type = data.errors.details[mutation]&.first["type"]
      return head :not_found if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      message = data.errors.messages.values.join(", ")

      return render json: { error: message }, status: :unprocessable_entity
    end

    head :ok
  end

  DestroyQuery = parse_query <<-'GRAPHQL'
    mutation($input: DeleteMarketplaceListingScreenshotInput!) {
      deleteMarketplaceListingScreenshot(input: $input) {
        marketplaceListing
      }
    }
  GRAPHQL

  def destroy
    data = platform_execute(DestroyQuery, variables: { input: { id: params[:id] } })

    if data.errors.any?
      error_type = data.errors.details[:deleteMarketplaceListingScreenshot]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      message = data.errors.messages.values.join(", ")

      return render json: { error: message }, status: :unprocessable_entity
    end

    head :ok
  end

  private

  def target_for_conditional_access
    screenshot = typed_object_from_id([Platform::Objects::MarketplaceListingScreenshot], params[:id])
    screenshot.listing.owner
  rescue Platform::Errors::NotFound
    return :no_target_for_conditional_access # Marketplace::ListingScreenshot not found
  end
end
