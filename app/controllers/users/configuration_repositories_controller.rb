# frozen_string_literal: true

class Users::ConfigurationRepositoriesController < ApplicationController
  before_action :require_config_repo_enabled
  before_action :login_required

  skip_before_action :perform_conditional_access_checks, only: [:create, :post_profile_readme_opt_in]
  skip_before_action :require_config_repo_enabled, only: [:post_profile_readme_opt_in]

  areas_of_responsibility :pe_repos

  def create
    is_public = params[:public] == "1"
    config_repo = current_user.create_configuration_repository(is_public: is_public,
                                                               reflog_data: reflog_data)

    if config_repo.persisted?
      redirect_to blob_new_path("", config_repo.default_branch, config_repo) +
        "?filename=#{ProfileConfigurationFile::DEFAULT_NAME}"
    else
      flash[:error] = config_repo.errors.full_messages.join(", ")
      redirect_to user_path(current_user)
    end
  end

  def post_profile_readme_opt_in
    current_user.profile_readme_opt_in = true
    current_user.profile.save!

    redirect_to opt_in_redirect_location
  end

private

  def reflog_data
    {
      real_ip: request.remote_ip,
      user_login: current_user.login,
      user_agent: request.user_agent,
      from: GitHub.context[:from],
      via: "create user-configuration repository",
    }
  end

  def require_config_repo_enabled
    render_404 unless config_repo_enabled?
  end

  def opt_in_redirect_location
    config_repo = current_user.configuration_repository
    return user_path current_user if config_repo.nil?

    if config_repo.private?
      # Clicked on "Update now" on config_repo files overview page.
      return edit_repository_path(current_user,
                                  config_repo,
                                  return_to: repository_path(config_repo))
    end

    # Clicked on "Share to Profile" on config_repo files overview page.
    if current_user.profile_readme.nil?
      flash[:notice] = "You've successfully shared your profile README!"
      return repository_path(config_repo)
    end

    user_path current_user
  end

end
