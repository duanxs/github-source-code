# frozen_string_literal: true

class Users::StaffAccessRequestsController <  ApplicationController
  areas_of_responsibility :stafftools
  before_action :login_required
  before_action :ensure_request_is_for_current_user
  before_action :ensure_request_is_active

  def show
    render "users/staff_access_requests/show"
  end

  def accept
    grant = current_request.accept(current_user)

    if grant
      StaffAccessMailer.impersonation_request_accepted(grant).deliver_later
      flash[:notice] = "You have granted GitHub staff access to login as your account until #{grant.expires_at.to_date}."
    end

    redirect_to settings_path
  end

  def deny
    current_request.deny(current_user)
    StaffAccessMailer.impersonation_request_denied(current_request).deliver_later
    flash[:notice] = "You have denied GitHub staff's request to login as your account."
    redirect_to settings_path
  end

  private

  def target_for_conditional_access
    :no_target_for_conditional_access
  end

  helper_method :current_request
  def current_request
    @current_request ||= StaffAccessRequest.find(params[:id])
  end

  def ensure_request_is_for_current_user
    unless current_request.accessible == current_user
      return render_404
    end
  end

  def ensure_request_is_active
    unless current_request.active?
      flash[:error] = StaffAccessRequest::REQUEST_NOT_ACTIVE_MESSAGE
      return redirect_to settings_path
    end
  end
end
