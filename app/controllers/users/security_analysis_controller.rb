# frozen_string_literal: true

class Users::SecurityAnalysisController < ApplicationController
  include Settings::SecurityAnalysisSettings

  def update
    update_security_settings(current_user, params)
    flash[:notice] = "Security settings updated for #{current_user.name}'s repositories."
    redirect_to :back
  end
end
