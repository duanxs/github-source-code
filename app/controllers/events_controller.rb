# frozen_string_literal: true

class EventsController < ApplicationController
  areas_of_responsibility :news_feeds

  skip_before_action :perform_conditional_access_checks

  def index
    respond_to do |format|
      format.html {
        render "events/index", layout: false, formats: [:atom], content_type: :atom
      }
      format.atom { render "events/index", layout: false }
      format.json do
        docs_url = "#{GitHub.developer_help_url}/v3/activity/events/#list-public-events"
        render json: gone_payload(docs_url), status: 410
      end
    end
  end

private
  # Handle auth specifics for feed requests.
  include GitHub::Authentication::Feed

  # Private: Actions that can response to atom requests.
  #
  # Returns an Array of Strings.
  def feed_actions
    %w(index)
  end

  def events_timeline_key
    "public"
  end
end
