# frozen_string_literal: true

class SubscriptionItemsController < ApplicationController
  areas_of_responsibility :marketplace

  before_action :marketplace_required

  before_action :login_required

  DestroyQuery = parse_query <<-'GRAPHQL'
    mutation($id: ID!, $immediatelyCancel: Boolean!) {
      cancelSubscriptionItem(input: { id: $id, immediatelyCancel: $immediatelyCancel }) {
        onFreeTrial
        subscription {
          nextBillingDate
        }
        subscriptionItem {
          subscribable {
            ... on MarketplaceListingPlan {
              name
              listing {
                name
                listableIsSponsorable
              }
            }
            ... on SponsorsTier {
              name
              listing: sponsorsListing {
                name
                listableIsSponsorable
              }
            }
          }
        }
      }
    }
  GRAPHQL

  def destroy
    data = platform_execute(DestroyQuery, variables: { id: params[:id], immediatelyCancel: false })

    if data.errors.any?
      error_type = data.errors.details[:cancelSubscriptionItem]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)
      flash[:error] = data.errors.messages.values.join(", ")
    else
      cancel_subscription_item = data.cancel_subscription_item
      notice = cancellation_notice(cancel_subscription_item)
      flash[:notice] = notice
    end

    redirect_to params[:redirect_to] || :back
  end

  private

  def target_for_conditional_access
    subscription_item = typed_object_from_id([Platform::Objects::SubscriptionItem], params[:id])
    plan_subscription = subscription_item.plan_subscription
    plan_subscription.user
  rescue Platform::Errors::NotFound
    # Billing::SubscriptionItem not found
    :no_target_for_conditional_access
  end

  def cancellation_notice(cancel_subscription_item)
    plan = cancel_subscription_item.subscription_item.subscribable
    plan_name = plan.name
    listing = plan.listing
    listing_name = listing.name

    if listing.listable_is_sponsorable?
      notice = "You've cancelled your sponsorship of #{listing_name} for #{plan_name}."
      extended_notice = " Your sponsorship will be active until your next billing cycle on"
    else
      notice = "You've cancelled your subscription to #{plan_name} for #{listing_name}."
      extended_notice = " This plan change will take effect on"
    end

    if !cancel_subscription_item.on_free_trial?
      next_billing_date = cancel_subscription_item.subscription.next_billing_date.to_s :date
      notice += "#{extended_notice} #{next_billing_date}."
    end

    notice
  end
end
