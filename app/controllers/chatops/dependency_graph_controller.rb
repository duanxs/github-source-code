# frozen_string_literal: true

require "chatops-controller"

module Chatops
  class DependencyGraphController < ApplicationController
    BATCH_SIZE = 100
    include ::Chatops::Controller

    areas_of_responsibility :stafftools
    map_to_service :dependency_graph
    chatops_help "Commands for working with the dependency graph"
    chatops_namespace :dependency_graph

    def require_conditional_access_checks?
      false # this is a staff command
    end

    def verify_authenticity_token?
      false # robots do this
    end

    def validate_repo(nwo_or_id)
      if /\A[0-9]+\Z/.match(nwo_or_id)
        Repository.exists?(nwo_or_id) && nwo_or_id
      else
        repo = Repository.nwo(nwo_or_id)
        repo.present? && repo.id
      end
    end

    chatop :redetect_js,
      /redetect_js (?<repo>.*)?/,
      "redetect_js [repo] - scans the given repository with Vintage" do
        rpc_params = jsonrpc_params.permit(:repo)
        repo = rpc_params.fetch(:repo)
        validated_id = validate_repo(repo)
        if validated_id
          DependencyGraphManifestBackfillJob.perform_now(validated_id, [:vendored_javascript_dependency])
          chatop_send "Redetection complete for #{repo}"
        else # Repository does not exist
          chatop_send "Repository #{repo} does not exist"
        end
      end

    chatop :vintage_onboard_org,
      /vintage_onboard_org (?<org>.*)?/,
      "vintage_onboard_org [org] - enable the vintage/dependency_graph_preview flags for the org and redetect manifests" do
        rpc_params = jsonrpc_params.permit(:org)
        org_name = rpc_params.fetch(:org)
        org = Organization.find_by_name(org_name)
        if org
          GitHub.flipper[:vintage].enable(org)
          GitHub.flipper[:dependency_graph_preview].enable(org)
          ActiveRecord::Base.connected_to(role: :reading) do
            repos_to_redetect = Repository.where(owner_id: org.id)

            repos_to_redetect.in_batches(of: BATCH_SIZE) do |batch|
              batch.each do |repo|
                RepositoryDependencyManifestInitializationJob.perform_later(repo.id) if repo.dependency_graph_enabled?
              end
            end
            chatop_send "Vintage is now enabled for #{org_name}! I have queued up a redetect job for #{repos_to_redetect.count} repos belonging to that organization."
          end
        else # No such org
          chatop_send "No org found with the name '#{org_name}'"
        end
      end

    chatop :vintage_offboard_org,
      /vintage_offboard_org (?<org>.*)?/,
      "vintage_offboard_org [org] - disable the vintage/dependency_graph_preview flags for the org" do
        rpc_params = jsonrpc_params.permit(:org)
        org_name = rpc_params.fetch(:org)
        org = Organization.find_by_name(org_name)
        if org
          GitHub.flipper[:vintage].disable(org)
          GitHub.flipper[:dependency_graph_preview].disable(org)
          chatop_send "The vintage and dependency_graph_preview feature flags are now disabled for #{org_name}."
        else # No such org
          chatop_send "No org found with the name '#{org_name}'"
        end
      end

    chatop :redetect_org,
      /redetect_org (?<org>.*)?/,
      "redetect_org [org] - redetect all manifests for all repos owned by the given org" do
        rpc_params = jsonrpc_params.permit(:org)
        org_name = rpc_params.fetch(:org)
        org = Organization.find_by_name(org_name)
        if org
          ActiveRecord::Base.connected_to(role: :reading) do
            repos_to_redetect = Repository.where(owner_id: org.id)

            repos_to_redetect.in_batches(of: BATCH_SIZE) do |batch|
              batch.each do |repo|
                RepositoryDependencyManifestInitializationJob.perform_later(repo.id) if repo.dependency_graph_enabled?
              end
            end
            chatop_send "Queued redetect job for #{repos_to_redetect.count} repos belonging to #{org_name}"
          end
        else # No such org
          chatop_send "No org found with the name '#{org_name}'"
        end
      end

    chatop :redetect,
      /redetect (?<repo>.*)?/,
      "redetect [repo] - redetect all manifests for the given repo" do
        rpc_params = jsonrpc_params.permit(:repo)
        repo = rpc_params.fetch(:repo)
        validated_id = validate_repo(repo)
        if validated_id
          begin
            RepositoryDependencyManifestInitializationJob.perform_now(validated_id)
            chatop_send "Redetection complete for #{repo}"
          rescue => e
            chatop_send "Error performing RepositoryDependencyManifestInitializationJob: #{e.inspect}"
          end
        else # Repository does not exist
          chatop_send "Repository #{repo} does not exist"
        end
      end

    chatop :clear_org,
      /clear_org (?<org>.*)?/,
      "clear_org [org] - clear all manifests for all repos owned by the given org" do
        rpc_params = jsonrpc_params.permit(:org)
        org_name = rpc_params.fetch(:org)
        org = Organization.find_by_name(org_name)
        if org
          ActiveRecord::Base.connected_to(role: :reading) do
            repos_to_clear = Repository.where(owner_id: org.id)
            repos_to_clear.in_batches(of: BATCH_SIZE) do |batch|
              batch.each do |repo|
                RepositoryDependencyClearDependencies.perform_later(repo.id) if repo.dependency_graph_enabled?
              end
            end
            chatop_send "Queued clear job for #{repos_to_clear.count} repos belonging to #{org_name}"
          end
        else # No such org
          chatop_send "No org found with the name '#{org_name}'"
        end
      end

    chatop :clear,
      /clear (?<repo>.*)?/,
      "clear [repo] - runs the 'clear manifests' action on the given repository" do
        rpc_params = jsonrpc_params.permit(:repo)
        repo = rpc_params.fetch(:repo)
        validated_id = validate_repo(repo)
        if validated_id
          chatop_send "Clearing #{repo}"
          RepositoryDependencyClearDependencies.perform_now(validated_id)
        else # Repository does not exist
          chatop_send "Repository #{repo} does not exist"
        end
      end
  end
end
