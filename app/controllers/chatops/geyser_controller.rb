# frozen_string_literal: true

require "chatops-controller"
require "github_chatops_extensions"
require "github_chatops_extensions/checks"
require "terminal-table"

module Chatops
  class GeyserController < ApplicationController
    ALLOWED_ROOMS = ["#code-search", "#dsp-code-search-ops", "#search-ops"].freeze
    ASSIGNMENTS_HEADINGS = ["ID", "NWO", "Current", "Desired", "Created (UTC)", "Updated (UTC)", "Elapsed"].freeze
    DATE_FORMAT = "%Y-%m-%d %H:%M:%S"
    DEFAULT_STALENESS = Search::Geyser::Requests::GetAssignments::DEFAULT_STALENESS
    KILL_SWITCH_HEADINGS = ["Successful", "Error Message", "Scope", "Current"].freeze
    NULL = "NULL"
    UI_FEATURE_FLAG = "geyser_search_enabled"
    UNCONTROLLED_ACTIONS = [:assignments, :list].freeze
    ALLOWLIST_NAME = "geyser_index"

    include ::Chatops::Controller
    include ::GitHubChatopsExtensions::Checks::Includable::Duo
    include ::GitHubChatopsExtensions::Checks::Includable::Room

    before_action -> { require_in_room(ALLOWED_ROOMS) }, except: :list
    before_action :require_duo_2fa, except: UNCONTROLLED_ACTIONS

    areas_of_responsibility :code_search

    skip_before_action :perform_conditional_access_checks
    skip_before_action :verify_authenticity_token

    chatops_help "Commands for managing the geyser service"

    chatops_namespace :geyser
    chatops_error_response "An error occurred. More information is available [in sentry](https://sentry.io/organizations/github/issues/?query=is%3Aunresolved+app%3Ahubot)"

    chatop :assignments,
           /assignments(?:\s+((?<id>\d+)|(?<nwo>[a-z0-9]+(-[a-z0-9]+)*\/[a-z0-9]+(-[a-z0-9]+)*)|(?<owner>[a-z0-9]+(-[a-z0-9]+)*)))?/,
           "assignments [<id>|<nwo>|<owner>] [--staleness <seconds>] [--collection <collection>] [--partition <partition>] [--cluster <cluster>] [--schema <schema>] [--count] [--lab] - Returns a list of assignments that are stale based on the difference between the job distributor processing time and the worker finish time. Returns the top 100 most stale unless a repository ID, NWO, or owner is specified. The default staleness threshold is #{DEFAULT_STALENESS} seconds unless another value is specified." do
      rpc_params = jsonrpc_params.permit(:id, :nwo, :owner, :staleness, :collection, :partition, :cluster, :schema, :count, :lab)

      repository_ids = repository_ids_from_params(rpc_params)
      lab_scoped = rpc_params[:lab].present?

      options = {
        staleness_gte_seconds: rpc_params.fetch(:staleness, nil)&.to_i,
        collection_name: rpc_params.fetch(:collection, nil),
        search_schema: rpc_params.fetch(:schema, nil),
        partition_name: rpc_params.fetch(:partition, nil),
        cluster_name: rpc_params.fetch(:cluster, nil),
      }.compact

      results = if rpc_params.fetch(:count, false)
        tablerize(nil,
                  [assignments_title_from_params(rpc_params)],
                  get_assignments_count(repository_ids, lab_scoped, options))
      else
        tablerize(assignments_title_from_params(rpc_params),
                  ASSIGNMENTS_HEADINGS,
                  get_assignments(repository_ids, lab_scoped, options))
      end
      chatop_send %Q{```\n#{results}\n```}
    end

    chatop :ingest,
           /ingest(?:\s+((?<id>\d+)|(?<nwo>[a-z0-9]+(-[a-z0-9]+)*\/[a-z0-9]+(-[a-z0-9]+)*)|(?<owner>[a-z0-9]+(-[a-z0-9]+)*)|(?<allowlist>--allowlist)))?/,
           "ingest [<id>|<nwo>|<owner>|--allowlist] [--collection <target_collection>] [--lab] - publish ingest events for the repository or all of the owner's repositories. If --allowlist is specified instead then it publishes events for all repositories in the geyser_index allowlist, minus any in the geyser_denylist" do
      rpc_params = jsonrpc_params.permit(:id, :nwo, :owner, :allowlist, :collection, :lab)

      repository_ids = if rpc_params[:allowlist].present?
        repository_ids_from_allowlist
      else
        repository_ids_from_params(rpc_params)
      end
      lab_scoped = rpc_params[:lab].present?
      collection = rpc_params[:collection]

      submit_job(repository_ids, lab_scoped, collection, :ADMIN_PUSHED)
    end

    chatop :repair,
           /repair(?:\s+((?<id>\d+)|(?<nwo>[a-z0-9]+(-[a-z0-9]+)*\/[a-z0-9]+(-[a-z0-9]+)*)|(?<owner>[a-z0-9]+(-[a-z0-9]+)*)|(?<allowlist>--allowlist)))?/,
           "repair [<id>|<nwo>|<owner>|--allowlist] [--collection <target_collection>] [--lab] - publish repair events for the repository or all of the owner's repositories. If --allowlist is specified instead then it publishes events for all repositories in the geyser_index allowlist, minus any in the geyser_denylist" do
      rpc_params = jsonrpc_params.permit(:id, :nwo, :owner, :allowlist, :collection, :lab)

      repository_ids = if rpc_params[:allowlist].present?
        repository_ids_from_allowlist
      else
        repository_ids_from_params(rpc_params)
      end
      lab_scoped = rpc_params[:lab].present?
      collection = rpc_params[:collection]

      submit_job(repository_ids, lab_scoped, collection, :ADMIN_REPAIR)
    end

    chatop :delete,
           /delete(?:\s+((?<id>\d+)|(?<nwo>[a-z0-9]+(-[a-z0-9]+)*\/[a-z0-9]+(-[a-z0-9]+)*)|(?<owner>[a-z0-9]+(-[a-z0-9]+)*)))?/,
           "delete [<id>|<nwo>|<owner>] [--collection <target_collection>] [--lab] - publish delete events for the repository or all of the owner's repositories." do
      rpc_params = jsonrpc_params.permit(:id, :nwo, :owner, :collection, :lab)

      repository_ids = repository_ids_from_params(rpc_params)
      lab_scoped = rpc_params[:lab].present?
      collection = rpc_params[:collection]

      submit_job(repository_ids, lab_scoped, collection, :ADMIN_DELETED)
    end

    chatop :pause,
           /pause/,
           "pause [--lab]  - continue publishing events, but signal the Geyser Job Distributor to pause consumption, and stop emitting Aqueduct jobs" do
      rpc_params = jsonrpc_params.permit(:scope, :reason, :lab)

      lab_scoped = rpc_params[:lab].present?
      options = {
        scope: rpc_params.fetch(:scope, ""),
        reason: rpc_params.fetch(:reason, ""),
      }.compact

      results = tablerize("Job Distribtor Kill Switch",
                          KILL_SWITCH_HEADINGS,
                          toggle_kill_switch(:PAUSED, lab_scoped, options))

      chatop_send %Q{```\n#{results}\n```}
    end

    chatop :resume,
           /resume/,
           "resume [--lab] - signal the Geyser Job Distributor to resume consuming events and emitting Aqueduct jobs" do
      rpc_params = jsonrpc_params.permit(:scope, :reason, :lab)

      lab_scoped = rpc_params[:lab].present?
      options = {
        scope: rpc_params.fetch(:scope, ""),
        reason: rpc_params.fetch(:reason, ""),
      }.compact

      results = tablerize("Job Distribtor Kill Switch",
                          KILL_SWITCH_HEADINGS,
                          toggle_kill_switch(:ACTIVE, lab_scoped, options))

      chatop_send %Q{```\n#{results}\n```}
    end

    chatop :ui,
           /ui(?:\s+(?<command>(status|disable|enable)))?/,
           "ui [status|disable|enable] - show current status of global geyser_search_enabled feature flag in dotcom, or toggle on (enable) or off (disable). When disabled, Geyser-based UI functionality will be disabled in dotcom but ingest events will still be queued" do
      rpc_params = jsonrpc_params.permit(:command)

      status = GitHub.geyser_search_enabled? ? "enabled" : "disabled"

      case rpc_params[:command].to_s
      when "disable", "enable"
        command_past_tense = "#{rpc_params[:command]}d"
        if status == command_past_tense
          result = "#{UI_FEATURE_FLAG} is already #{command_past_tense}"
        else
          GitHub.context.push(actor: params.fetch(:user)) # Used by chatterbox
          Audit.context.push(actor: params.fetch(:user)) # Used by audit log
          result = if GitHub.flipper[UI_FEATURE_FLAG].public_send(rpc_params[:command].to_sym)
            "#{UI_FEATURE_FLAG} has been #{command_past_tense}"
          else
            "#{UI_FEATURE_FLAG} could not be #{command_past_tense}"
          end
        end
      else
        result = "#{UI_FEATURE_FLAG} is currently #{status}"
      end

      chatop_send result
    end

    chatop :collection,
      /collection\s+(?<command>(create|delete|update))?/,
      "collection [create|delete|update] [--name <collection>] [--scope <search_scope>] [--primary true|false] [--selector <name_suffix>] [--lab] - administrate a Geyser Collection. Not all arguments apply to all subcommands." do
      rpc_params = jsonrpc_params.permit(:command, :name, :selector, :scope, :primary, :lab)

      geyser_admin_exec(:collection, rpc_params)
    end

    chatop :partition,
      /partition\s+(?<command>(create|delete|update))?/,
      "partition [create|delete|update] [--collection <parent_collection>] [--name <partition_name>] [--schema <search_schema>] [--cluster <cluster_name>] [--status <status>] [--searchable true|false] [--writable true|false] [--lab] - administrate a Geyser Partition. Not all arguments apply to all subcommands. If absent, defaults are applied for: searchable, writable, status, schema, cluster" do
      rpc_params = jsonrpc_params.permit(:command, :schema, :name, :status, :searchable, :writable, :collection, :lab)

      geyser_admin_exec(:partition, rpc_params)
    end

    private

    def geyser_admin_exec(command, params)
      subcommand = (params.delete(:command) { |not_found| "" }).to_s.downcase
      lab_scoped = params[:lab].present?

      case command
      when :collection
          message = geyser_collection_admin(subcommand, lab_scoped, params)
      when :partition
          message = geyser_partition_admin(subcommand, lab_scoped, params)
      else
        message = "Invalid entity supplied: #{command} (must be one of: collection, partition)"
      end

      chatop_send message
    end

    def submit_job(repository_ids, lab_scoped, collection = nil, event_type = :CHANGE_UNKNOWN)
      collections = collection ? collection.split(",") : []

      if repository_ids.any?
        case event_type
        when :ADMIN_PUSHED, :ADMIN_REPAIR
          status = JobStatus.create(id: GeyserBulkIngestJob.job_id(repository_ids, collections, lab_scoped))
          GitHub::Logger.log(
            command: json_body,
            event_type: event_type,
            job: "GeyserBulkIngestJob",
            job_id: status.id,
            target_collections: collections,
            lab_scoped: lab_scoped,
            repository_ids: repository_ids
          )
          GeyserBulkIngestJob.perform_later(repository_ids, collections, lab_scoped, params[:room_id], event_type)
        when :ADMIN_DELETED
          # ADMIN_DELETED has it's own job and fixed event type, no need to pass it through
          status = JobStatus.create(id: GeyserBulkDeleteJob.job_id(repository_ids, collections, lab_scoped))
          GitHub::Logger.log(
            command: json_body,
            event_type: event_type,
            job: "GeyserBulkDeleteJob",
            job_id: status.id,
            target_collections: collections,
            lab_scoped: lab_scoped,
            repository_ids: repository_ids
          )
          GeyserBulkDeleteJob.perform_later(repository_ids, collections, lab_scoped, params[:room_id])
        else
          chatop_send "Invalid event type #{event_type} submitted, 0 events published"
          return
        end

        event_type_scope_msg = "#{event_type} events queued in #{lab_scoped ? "lab" : "production"}"
        collection_msg = collection ? " for target Collection(s): #{collection}" : ""
        message = "#{event_type_scope_msg} for #{helpers.pluralize(repository_ids.size, "repository")}#{collection_msg}. Some repositories may be skipped if they are not eligible for submission."
      else
        message = "0 #{event_type} events queued. You must specify at least one valid repository."
      end

      chatop_send message
    end

    def repository_ids_from_params(params)
      if params[:id].present?
        Repository.where(id: params[:id]).pluck(:id)
      elsif params[:nwo].present?
        Repository.with_names_with_owners([params[:nwo]]).pluck(:id)
      elsif params[:owner].present?
        owner_id = User.where(login: params[:owner]).pluck(:id)
        Repository.where(owner_id: owner_id).pluck(:id)
      else
        []
      end
    end

    def repository_ids_from_allowlist
      actor_ids = FlipperFeature.find_by(name: ALLOWLIST_NAME)&.actor_ids_by_class
      return [] if actor_ids.nil?

      repository_ids = []
      owner_ids = []
      actor_ids.each do |actor_class, ids|
        if actor_class == Repository
          repository_ids += ids.map(&:to_i)
        elsif [User, Organization].include?(actor_class)
          owner_ids += ids.map(&:to_i)
        end
      end

      # Backfill any remaining repositories by owner
      repository_ids += Repository.where(owner_id: owner_ids).where.not(id: repository_ids).pluck(:id) if owner_ids.any?

      repository_ids
    end

    def tablerize(title, headings, rows)
      table = Terminal::Table.new(title: title, headings: headings)
      table.rows = rows.any? ? rows : no_results(headings.size)
      table
    end

    def no_results(column_count)
      [[{value: "No results", colspan: column_count}]]
    end

    def time_at_protobuf_timestamp(protobuf_timestamp)
      Time.at(protobuf_timestamp.nanos * 10**-9 + protobuf_timestamp.seconds).utc
    end

    def time_elapsed(start_time, end_time)
      helpers.distance_of_time_in_words(start_time, end_time, include_seconds: true)
    end

    def geyser_collection_admin(subcommand, lab_scoped, params)
      req_env_target = lab_scoped ? "lab" : "production"

      results = case subcommand
      when "create"
        err_msg = "Missing required selector, scope, or primary parameter"
        return err_msg unless params[:selector] && params[:scope] && params[:primary]

        response = Search::Geyser.create_search_collection(
          lab_scoped: lab_scoped,
          payload: {
            selector:     params[:selector],
            search_scope: params[:scope],
            primary:      params[:primary],
          },
        )

        if !response.error.nil?
          "Geyser Collection create response from #{req_env_target}: #{response.error}"
        else
          sc = response.search_collection
          tablerize("Geyser Collection Create Response",
                  ["Name", "Scope", "Primary?", "Env"],
                  [[sc&.name, sc&.search_scope, sc&.primary.to_s, req_env_target]])
        end

      when "delete"
        "Missing required Collection name parameter" unless params[:name]

        response = Search::Geyser.delete_search_collection(
          lab_scoped: lab_scoped,
          payload: { collection_name: params[:name] },
        )

        if !response.error.nil?
          "Geyser Collection delete response from #{req_env_target}: #{response.error}"
        else
          tablerize("Geyser Collection Delete Response",
                  ["Successful?", "Details", "Env"],
                  [[response&.success.to_s, response&.details, req_env_target]])
        end

      when "update"
        "Missing required Collection name parameter" unless params[:name]

        response = Search::Geyser.update_collection(
          lab_scoped: lab_scoped,
          payload: {
            name:    params[:name],
            primary: params[:primary],
          },
        )

        if !response.error.nil?
          "Geyser Collection update response from #{req_env_target}: #{response.error}"
        else
          sc = response.search_collection
          tablerize("Geyser Collection Update Response",
                  ["Name", "Scope", "Primary?", "Env"],
                  [[sc&.name, sc&.search_scope, sc&.primary.to_s, req_env_target]])
        end

      else
        "Invalid subcommand for collection #{subcommand}"
      end

      %Q{```\n#{results}\n```}
    end

    def geyser_partition_admin(subcommand, lab_scoped, params)
      req_env_target = lab_scoped ? "lab" : "production"

      results = case subcommand
      when "create"
        err_msg = "Missing required Collection name or schema parameter"
        return err_msg unless params[:collection] && params[:schema]

        response = Search::Geyser.create_search_partition(
          lab_scoped: lab_scoped,
          payload: {
            collection_name: params[:collection],
            search_schema:   params[:schema],
            search_cluster:  params[:cluster],
            duplicate_of:    "", # TODO: update when Geyser supports Partition migration
          },
        )

        if !response.error.nil?
          "Geyser Partition create response from #{req_env_target}: #{response.error}"
        else
          sp = response.search_partition
          tablerize("Geyser Partition Create Response",
                    ["Name", "Status", "Searchable", "Writable", "Collection", "Schema", "URL", "Env", "Duplicate Of"],
                    [[sp&.name, sp&.status, sp&.searchable.to_s, sp&.writable.to_s, sp&.collection_name,
                     sp&.search_schema, sp&.url, req_env_target, sp&.duplicate_of.to_s]])
        end

      when "delete"
        return "Missing required Partition name parameter" unless params[:name]

        response = Search::Geyser.delete_search_partition(
          lab_scoped: lab_scoped,
          payload: { partition_name: params[:name] },
        )

        if !response.error.nil?
          "Geyser Partition delete response from #{req_env_target}: #{response.error}"
        else
          tablerize("Geyser Partition Delete Response",
                  ["Successful?", "Details", "Env"],
                  [[response&.success.to_s, response&.details, req_env_target]])
        end

      when "update"
        return "Missing required Partition name parameter" unless params[:name]

        # NOTE! This chatop accepts one Partition update at a time! The backing
        # Twirp API accepts an array which are updated atomically in transaction
        # on the Geyser side. Array syntax for a chatop seemed unwieldy vs. future
        # high-level chatops that can submit multiple Partition updates together
        # under the hood (such as "promote Partition X to OPEN and CLOSE Partition Y")
        response = Search::Geyser.update_partitions(
          lab_scoped: lab_scoped,
          payload: {
            name:          params[:name],
            search_schema: params[:schema],
            search_status: params[:status],
            searchable:    params[:searchable],
            writable:      params[:writable],
            duplicate_of:  "", # TODO: update when Geyser supports Partition migration
          },
        )

        if !response.error.nil?
          "Geyser Partition Update response from #{req_env_target}: #{response.error}"
        else
          rows = response.search_partitions&.map do |sp|
            [sp.name, sp.status, sp.searchable.to_s, sp.writable.to_s, sp.collection_name,
             sp.search_schema, sp.url, req_env_target, sp.duplicate_of.to_s]
          end || []
          tablerize("Geyser Partition Create Response",
                    ["Name", "Status", "Searchable", "Writable", "Collection", "Schema", "URL", "Env", "Duplicate Of"],
                    rows)
        end

      else
        "Invalid subcommand for partition #{subcommand}"
      end

      %Q{```\n#{results}\n```}
    end

    def get_assignments(repository_ids, lab_scoped, options)
      response = Search::Geyser.get_assignments(repository_ids, lab_scoped: lab_scoped, options: options)
      response.assignments&.map do |assignment|
        created_at = time_at_protobuf_timestamp(assignment.created_at)
        updated_at = time_at_protobuf_timestamp(assignment.updated_at)
        [
          assignment.repository_id,
          "#{assignment.owner_name}/#{assignment.repository_name}",
          (assignment.current_tree&.value || NULL)[0..6],
          (assignment.desired_tree&.value || NULL)[0..6],
          created_at.strftime(DATE_FORMAT),
          updated_at.strftime(DATE_FORMAT),
          time_elapsed(created_at, updated_at),
        ]
      end || []
    end

    def get_assignments_count(repository_ids, lab_scoped, options)
      response = Search::Geyser.get_assignments(repository_ids, lab_scoped: lab_scoped, options: options)
      # Return as the cell of a single row
      [["#{response.count} assignments"]]
    end

    def toggle_kill_switch(desired_state, lab_scoped, options)
      response = Search::Geyser.toggle_kill_switch(desired_state, lab_scoped: lab_scoped, options: options)
      return [] if response.nil?

      [
        [
          response.success || false,
          response.error_message || "",
          response.scope || "",
          response.current || :UNKNOWN,
        ],
      ]
    end

    def assignments_title_from_params(params)
      "#{assignments_scope_from_params(params)} staler than #{assignments_staleness_from_params(params)}s"
    end

    def assignments_scope_from_params(params)
      scope = params[:id].presence || params[:nwo].presence || params[:owner].presence
      count = params[:count].present?
      if count && scope
        "Count of #{scope} assignments"
      elsif scope
        "#{scope} assignments"
      elsif count
        "Count of assignments"
      else
        "Assignments"
      end
    end

    def assignments_staleness_from_params(params)
       params.fetch(:staleness, DEFAULT_STALENESS).to_i
    end
  end
end
