# frozen_string_literal: true

require "securerandom"
class SessionsController < ApplicationController

  # Window of time in which a verified device verification code is valid.
  VERIFIED_DEVICE_EXPIRY = 1.hour

  include CustomMessagesHelper

  # This controller does not access protected organization resources.
  skip_before_action :perform_conditional_access_checks

  # These actions are rate limited by GitHub::RateLimitedRequest and AuthenticationLimits
  SMS_DELIVERY_ACTIONS = %w(resend_two_factor_sms send_two_factor_fallback_sms)
  OTP_ENTRY_ACTIONS = %w(two_factor_authenticate verified_device_authenticate)

  # Exempt specific actions from the GHPI bootstrap check to allow the
  # initial site administrator to sign in and sign out.
  skip_before_action :private_instance_bootstrap_check, only: %w(new create confirm_logout destroy)

  before_action :clear_weak_password_session_variable, only: :new
  before_action :set_return_to,                     only: :create
  before_action :anonymous_required,                only: :new
  before_action :get_based_sudo_filter,             only: :sudo
  before_action :ensure_two_factor_sms_enabled,     only: %i(resend_two_factor_sms send_two_factor_fallback_sms)
  before_action :authentication_user_required,      only: SMS_DELIVERY_ACTIONS + OTP_ENTRY_ACTIONS
  before_action :login_required,                    only: :sudo_modal
  after_action  :update_login_metadata,             only: %i(create two_factor_authenticate u2f_authenticate two_factor_recover)
  after_action  :clear_browser_cache,               only: :destroy

  layout :authentication_layout

  # GitHub Enterprise specific filters
  if GitHub.enterprise?
    # sessions#create is accessible via a GET route on Enterprise (for CAS),
    # so we need to make sure it's always using the write database:
    around_action :select_write_database, only: :create

    if GitHub.pages_enabled? && GitHub.subdomain_private_mode_enabled?
      # Add GitHub Enterprise pages domain to the "form-action" directive
      # this allows us to redirect back to a pages domain when logging in
      # see https://github.com/w3c/webappsec-csp/issues/8
      before_action :add_csp_exceptions, only: [:new]
      CSP_EXCEPTIONS = { form_action: [GitHub.pages_host_name_v2] }
    end

    # The first run check ensures that the customer creates a user as the first
    # step. For external authentication, this step isn't required. To avoid an
    # infinite redirect loop, we have to skip the check here.
    if GitHub.auth.external?
      skip_before_action :first_run_check
    end
  end

  include GitHub::RateLimitedRequest
  rate_limit_requests \
    only: SMS_DELIVERY_ACTIONS + OTP_ENTRY_ACTIONS,
    max: :two_factor_rate_limit_max,
    ttl: :two_factor_rate_limt_ttl,
    key: :two_factor_rate_limit_key,
    log_key: "2fa",
    at_limit: :two_factor_rate_limit_render,
    render_allow_body: true

  def new
    # Avoid sending the query string to Google Analytics. It might contain a
    # `return_to` param that could be a sensitive URL like a private Gist.
    strip_analytics_query_string

    sanitize_referrer

    headers["Cache-Control"] = "no-cache, no-store"

    if client_id = params[:client_id]
      @application = if client_id =~ Integration::KEY_PATTERN_V1
        Integration.find_by_key(client_id)
      else
        OauthApplication.find_by_key(client_id)
      end
    end

    if @application.nil? && params[:integration]
      @application = Integration.find_by(slug: params[:integration])
    end

    respond_to do |format|
      format.html do
        if GitHub.auth.external? && GitHub.auth.path
          if GitHub.auth.builtin_auth_fallback? && !params[:force_external]
            render "dashboard/logged_out", locals: { index_page: true }
          else
            external_auth_redirect
          end
        else
          render "sessions/new"
        end
      end
    end
  end

  # Overrides `ApplicationController`.
  def authentication_user
    return @authentication_user if defined? @authentication_user
    if logged_in?
      @authentication_user = current_user
      return @authentication_user
    end
    @authentication_user = User.find_by_login(session[:two_factor_user])
    @authentication_user ||= User.find_by_id(session[:verified_device_user_id])
  end

  def external_auth_redirect
    # If we have saml request tracking on, CSRF need to be generated an
    # associated with both client and request. The cookie serves as the
    # client association and the request id to csrf mapping associates
    # the request with csrf token. Together these should ensure we're
    # only accepting requests from the same client.
    #
    # The _legacy cookies are present to work around a bug
    # in Safari 12, where cookies that are marked as
    # SameSite=none are treated as SameSite=strict (https://bugs.webkit.org/show_bug.cgi?id=198181)
    # Since fixes to that bug were not backported to MacOS 10.14, we need to also create
    # cookies without any SameSite attribute at all.
    if GitHub.auth.saml? && GitHub.auth.request_tracking?
      csrf = SecureRandom.urlsafe_base64(80)
      auth_path = GitHub.auth.path(params[:return_to], csrf)
      cookies[:saml_csrf_token] = csrf
      cookies[:saml_csrf_token_legacy] = csrf
      cookies[:saml_return_to] = params[:return_to] if params[:return_to]
      cookies[:saml_return_to_legacy] = params[:return_to] if params[:return_to]
    else
      auth_path = GitHub.auth.path(params[:return_to])
    end

    if GitHub.auth.saml?
      redirect_url = auth_path
      render "site/saml_auth_meta_redirect", locals: { redirect_url: redirect_url }, layout: "redirect"
    else
      redirect_to auth_path
    end
  end

  def verified_device_prompt
    verified_device_user = User.find(session[:verified_device_user_id])
    render "sessions/verified_device_prompt", locals: { verified_device_user: verified_device_user }
  end

  DEVICE_CODE_RATE_LIMIT_OPTIONS = {
    max_tries: 5,
    ttl: 15.minutes,
  }
  def resend_verification_email
    if session[:device_verification_expiration].blank?
      GitHub.dogstats.increment("authenticated_device", tags: ["action:resend", "error:nil_expiration"])
      anonymous_flash[:notice] = "An unexpected error has occurred, please try signing in again."
      return redirect_to_login
    end

    if session[:device_verification_expiration] < Time.now.to_i
      anonymous_flash[:notice] = "Your device verification code has expired."
      return redirect_to_login
    end

    verified_device_user = User.find(session[:verified_device_user_id])
    rate_limit_key = "verification-code-limit:#{session[:verified_device_user_id]}"
    if  RateLimiter.at_limit?(rate_limit_key, DEVICE_CODE_RATE_LIMIT_OPTIONS)
      @at_auth_limit = true
      return render "sessions/new"
    end

    device_name = AuthenticatedDevice.generated_display_name(parsed_useragent)
    AccountMailer.verified_device_verification(verified_device_user, device_name, session[:device_verification_code]).deliver_later
    flash[:notice] = "Your code has been resent"
    redirect_to verified_device_prompt_path
  end

  def verified_device_authenticate
    user_id = session[:verified_device_user_id]
    verified_device_user = User.find_by_id(user_id)

    return redirect_to_login unless verified_device_user

    if AuthenticationLimit.at_any?(verified_device_login: verified_device_user.login)
      @at_auth_limit = true
      return render "sessions/new"
    end

    if session[:device_verification_expiration].blank?
      GitHub.dogstats.increment("authenticated_device", tags: ["action:verification", "error:nil_expiration"])
      anonymous_flash[:notice] = "An unexpected error has occurred, please try signing in again."
      return redirect_to_login
    end

    if session[:device_verification_expiration] < Time.now.to_i
      GitHub.dogstats.increment("authenticated_device", tags: ["action:verification", "error:expired"])
      anonymous_flash[:error] = "Your device verification code has expired."
      return redirect_to_login
    end

    unless SecurityUtils.secure_compare(session[:device_id], current_device_id)
      GitHub.dogstats.increment("authenticated_device", tags: ["action:verification", "error:device_id_mismatch"])
      anonymous_flash[:error] = "Something went wrong, please try signing in again."
      return redirect_to_login
    end

    unless current_device = verified_device_user.authenticated_devices.find_by_device_id(session[:device_id])
      GitHub.dogstats.increment("authenticated_device", tags: ["action:verification", "error:no_device_found"])
      anonymous_flash[:error] = "Something went wrong loading your profile, please try signing in again."
      return redirect_to_login
    end

    if session[:device_verification_code].blank?
      GitHub.dogstats.increment("authenticated_device", tags: ["action:verification", "error:empty_or_nil_verification_code"])
      anonymous_flash[:error] = "Something went wrong validating the verification code, please try signing in again."
      return redirect_to_login
    end

    unless SecurityUtils.secure_compare(session[:device_verification_code], TwoFactorCredential.normalize_otp(params[:otp]))
      if AuthenticationLimit.at_any?(verified_device_login: verified_device_user.login, increment: true, actor_ip: request.remote_ip)
        @at_auth_limit = true
        return render "sessions/new"
      else
        verified_device_user.instrument_unverified_device(:device_verification_failure, current_device)
        flash[:error] = "Incorrect verification code provided."
        return redirect_to verified_device_prompt_path
      end
    end

    start_time = session[:device_verification_expiration] - VERIFIED_DEVICE_EXPIRY.to_i
    time_to_verify = Time.now.to_i - start_time
    GitHub.dogstats.timing("device_verification_timing", time_to_verify * 1000)

    login_user verified_device_user, authenticated_device: current_device, sign_in_verification_method: :verified_device
    verified_device_user.instrument_unverified_device(:device_verification_success, current_device, reason: :verified_sign_in)
    redirect_to_return_to
  end

  def two_factor_prompt
    @user = User.find_by_login session[:two_factor_user]
    return render_404 unless @user

    # Make sure SMS users have their OTP.
    if @user.two_factor_credential.sms_enabled?
      @user.two_factor_credential.send_otp_sms
    end

    render "sessions/two_factor_prompt"
  rescue GitHub::SMS::Error => e
    flash[:error] = sms_delivery_error_message(e.message)
    redirect_to two_factor_recover_prompt_path
  end

  def two_factor_authenticate
    user_login, otp_retry_allowed = get_partially_authenticated_two_factor_session

    return render_404 unless user_login

    if AuthenticationLimit.at_any?(two_factor_login: user_login)
      @at_auth_limit = true
      return render "sessions/new"
    end

    # Normalize the OTP or recovery code
    otp = TwoFactorCredential.normalize_otp(params[:otp])

    # Try treating it like a recovery code if it looks right.
    if TwoFactorCredential.recovery_code?(otp)
      anonymous_flash[:error] = "Recovery code authentication not allowed."
      # prevent a bad bot from getting into an endless loop
      AuthenticationLimit.at_any?(two_factor_login: user_login, increment: true, actor_ip: request.remote_ip)
      return redirect_to_login
    end

    if otp_retry_allowed && user = User.otp_authenticate(user_login, otp)
      instrument_two_factor_challenge_success(user: user, type: "otp")

      login_user user, sign_in_verification_method: :two_factor_user
      GitHub.stats.increment "auth.result.success.web" if GitHub.enterprise?
      redirect_to_return_to
    else
      user = User.find_by_login user_login
      instrument_two_factor_challenge_failure(user: user, type: "otp")
      GitHub.stats.increment "auth.result.failure.two_factor.web" if GitHub.enterprise?

      real_failure = otp =~ TwoFactorCredential::OTP_REGEX &&
                     !user.two_factor_credential.recently_valid_otp?(otp)

      at_limit = AuthenticationLimit.at_any?(
        increment: real_failure,
        actor_ip: request.remote_ip,
        two_factor_login: user_login,
      )

      if at_limit
        AccountMailer.two_factor_brute_force(user).deliver_later
        @at_auth_limit = true
        render "sessions/new"
      elsif user.two_factor_credential.reused_valid_totp?(otp)
        set_partially_authenticated_two_factor_session(user)
        message = ["The two-factor code you entered has already been used or is too old to be used."]
        if user.two_factor_credential.sms_enabled? && otp != user.two_factor_credential.totp.now
          begin
            user.two_factor_credential.send_otp_sms
            message << "A new code has been sent to your phone."
          rescue GitHub::SMS::Error => e
            message << sms_delivery_error_message(e.message)
          end
        end
        flash.now[:error] = message.join(" ")
        render_2fa_prompt(user)
      elsif otp_retry_allowed
        flash.now[:error] = "Two-factor authentication failed."
        set_partially_authenticated_two_factor_session(user)
        render_2fa_prompt(user)
      else
        anonymous_flash[:error] = "Two-factor authentication failed."
        redirect_to_login
      end
    end
  end

  def u2f_prompt
    @user = User.find_by_login session[:two_factor_user]
    return render_404 unless @user && @user.has_registered_security_key?

    render "sessions/security_key"
  end

  def u2f_authenticate
    # Find partially authenticated user.
    user_login, otp_retry_allowed = get_partially_authenticated_two_factor_session
    user = User.find_by_login!(user_login)

    # Check they they got here correctly.
    return render_404 unless user

    # Check that they haven't been sitting on the 2FA screen for days.
    return redirect_to_login unless otp_retry_allowed

    # Check that we've got all the data we need to try authenticating.
    sign_response_json_serialized = params[:response]
    unless sign_response_json_serialized.present? && webauthn_sign_challenge_from_request
      return redirect_to two_factor_prompt_path
    end

    origin = Addressable::URI.new(scheme: request.scheme, host: request.host).to_s
    if user.webauthn_or_u2f_authenticated?(origin, webauthn_sign_challenge_from_request, sign_response_json_serialized)
      instrument_two_factor_challenge_success(user: user, type: "u2f")
      login_user user, sign_in_verification_method: :two_factor_user
      GitHub.stats.increment "auth.result.success.web" if GitHub.enterprise?

      redirect_to_return_to
    else
      instrument_two_factor_challenge_failure(user: user, type: "u2f")
      flash[:error] = "Security key authentication failed."
      set_partially_authenticated_two_factor_session(user)
      redirect_to two_factor_prompt_path
    end
  end

  def two_factor_recover_prompt
    @user = User.find_by_login session[:two_factor_user]
    return render_404 unless @user

    render "sessions/two_factor_recover_prompt"
  end

  def two_factor_recover
    unless user_login = session.delete(:two_factor_user)
      return render_404
    end

    if AuthenticationLimit.at_any?(two_factor_login: user_login)
      @at_auth_limit = true
      render "sessions/new"
    elsif user = User.two_factor_authenticate_with_recovery(user_login, params[:recovery_code])
      instrument_two_factor_recover(user)

      login_user user, sign_in_verification_method: :two_factor_user
      flash[:notice] = "Having trouble with two-factor authentication? You can update your settings here"
      redirect_to settings_user_two_factor_authentication_configuration_path
    else
      AuthenticationLimit.at_any?(two_factor_login: user_login, increment: true, actor_ip: request.remote_ip)
      unauthenticated_user = User.find_by_login(user_login)
      GitHub.dogstats.increment("two_factor", tags: ["action:recover", "result:failure"])
      instrument_two_factor_challenge_failure(user: unauthenticated_user, type: "recovery code")
      anonymous_flash[:error] = "Recovery code authentication failed."
      redirect_to_login
    end
  end

  def create
    if validate_saml_provider_settings?
      repost_saml_response_to_saml_controller
    else
      create_session
    end
  end

  # Checks if it is a post request to saml/consume endpoint in GHPI environment
  # as well as if saml_return_to cookie is set validate
  #
  # Return true if it the validate flow.
  def validate_saml_provider_settings?
    return false unless GitHub.private_instance_user_provisioning?
    return false unless request.post?
    return false unless request.path == "/saml/consume"

    validate_saml_provider_settings_cookie == "validate"
  end

  def validate_saml_provider_settings_cookie
    cookies.encrypted[:saml_return_to] || cookies.encrypted[:saml_return_to_legacy]
  end

  # Redirect incoming SAML responses via POST to enterprise saml/consume endpoint
  # This will reuse the validation logic in saml_controller as well as
  # enable the user_session and other same site cookies to be available
  def repost_saml_response_to_saml_controller
    # sets reposted_saml_response to repost saml response only once and avoid looping login
    session[:reposted_saml_response] = true

    form_data = {
      "SAMLResponse" => params[:SAMLResponse],
      "RelayState" => params[:RelayState],
      "_target" => idm_saml_consume_enterprise_url(GitHub.global_business),
    }

    render_template_view "businesses/identity_management/replay_enforced_request",
      Businesses::IdentityManagement::ReplayEnforcedRequestView,
      { business: GitHub.global_business,
        form_data: form_data,
      },
      layout: "session_authentication"
  end

  def create_session
    # This can happen for external auth modes when return_to is recorded as,
    # for example, "/saml/consume" which would redirect back here as a GET
    # request. Only for saml right now.
    if request.get? && GitHub.auth.saml?
      redirect_to dashboard_url
      return
    end

    # This endpoint is shared by both built-in and saml auth (/login,
    # /saml/consume). This ensures the /saml/consume route is unavailable unless
    # saml auth is configured.
    if request.path == "/saml/consume" && !GitHub.auth.saml?
      render_404 and return
    end

    # While not strictly necessary, we clear out the cookie session context for
    # each login attempt just to be safe.
    persistent_reset_session

    logout_user(:switched_users)
    GitHub.context.push(spamurai_form_signals: spamurai_form_signals)
    GitHub.context.push(visitor_id: current_visitor.id)
    GitHub.context.push(ga_id: params[:ga_id])

    result, attempted_user = GitHub.auth.rails_authenticate(
      request,
      octolytics_id: current_visitor.octolytics_id,
      current_device_id: current_device_id,
    )
    Failbot.push user: result.user.to_s if result.user

    # Record WebAuthn client support stats
    webauthnSupport = if ["supported", "unsupported"].include? params[:"webauthn-support"]
      params[:"webauthn-support"]
    else
      "unknown"
    end
    webauthnIUVPAASupport = if ["supported", "unsupported"].include? params[:"webauthn-iuvpaa-support"]
      params[:"webauthn-iuvpaa-support"]
    else
      "unknown"
    end
    GitHub.dogstats.increment("login_webauthn_support", tags: [
      "webauthn-support:#{webauthnSupport}",
      "webauthn-iuvpaa-support:#{webauthnIUVPAASupport}",
      "success:#{result.success?}",
      "failure_type:#{result.failure_type}",
    ])

    set_has_recent_activity_cookie

    # User is authenticated.
    if result.success?
      login_user(
        result.user,
        authenticated_device: result.authenticated_device,
        sign_in_verification_method: result.sign_in_verification_method,
      )

      set_analytics_dimension(
        name: GoogleAnalytics::Dimensions::HAS_ACCOUNT,
        value: "Logged In",
        redirect: true,
      )
      flash[:stale_session_signedin] = true

      if GitHub.auth.saml? && GitHub.auth.request_tracking?
        request.env["return_to"] = cookies[:saml_return_to] || cookies[:saml_return_to_legacy]
        remove_saml_cookies
      end

      redirect_to_return_to(fallback: "/")
    elsif result.suspended_failure?
      attempted_user && attempted_user.revoke_active_sessions(:suspended)
      return redirect_to suspended_url
    elsif result.weak_password_failure?
      reset = PasswordReset.new(user: result.user, forced_weak_password_reset: true)
      if reset.valid?
        anonymous_flash[:error] = reset.weak_password_reset_message
        current_device = reset.user.authenticated_devices.find_by_device_id(current_device_id)

        if current_device.nil?
          GitHub.dogstats.increment("auth.compromised_password.blocked_sign_in", tags: ["result:unknown_device"])
          redirect_to password_reset_path
        elsif current_device.verified?
          GitHub.dogstats.increment("auth.compromised_password.blocked_sign_in", tags: ["result:verified_device"])
          safe_redirect_to reset.link
        else
          GitHub.dogstats.increment("auth.compromised_password.blocked_sign_in", tags: ["result:unrecognized_device"])
          redirect_to password_reset_path
        end
      else
        GitHub.dogstats.increment("auth.compromised_password.blocked_sign_in", tags: ["result:invalid"])
        Failbot.push(user: reset.user.login)
        anonymous_flash[:error] = "Your password is in a list of passwords commonly used on other websites. You must update your password to continue using GitHub.com. We were unable to find a valid email address on file. Please contact GitHub support to sign in."
        redirect_to_login
      end
    elsif result.unverified_device_failure?
      verification_code = AuthenticatedDevice.generate_device_verification_code
      device_name = AuthenticatedDevice.generated_display_name(parsed_useragent)
      AccountMailer.verified_device_verification(result.user, device_name, verification_code).deliver_later

      session[:device_id] = current_device_id
      session[:verified_device_user_id] = result.user.id
      session[:device_verification_code] = verification_code
      session[:device_verification_expiration] = Time.now.to_i + VERIFIED_DEVICE_EXPIRY.to_i

      result.user.instrument_unverified_device(:device_verification_requested, result.authenticated_device)

      redirect_to verified_device_prompt_path
    # User needs to authenticate with second factor.
    elsif result.two_factor_partial_sign_in?
      set_partially_authenticated_two_factor_session(result.user)
      # We're going to mark the session as "Logged In" even though user hasn't completed 2FA,
      # because at this point we at least know that they have an account.
      set_analytics_dimension(
        name: GoogleAnalytics::Dimensions::HAS_ACCOUNT,
        value: "Logged In",
        redirect: true,
      )

      # We require all of the following conditions:
      # - Webauthn is (maybe) supported in the browser. The following values are
      #   possible, and we err on the side of encouraging security keys. We try
      #   WebAuthn unless the client specifically sends `unsupported`. We have a
      #   reasonable fallback flow for when we guess wrong.
      #   - `supported`
      #   - `unsupported`
      #   - `unknown` (fallback value sent by the login page if feature detection
      #   doesn't work / doesn't work fast enough / JS is disabled)
      #   - (nil)
      # - Security keys are enabled for this request in the current server configuration.
      # - The user has at least one security key registered.
      use_security_key = params[:"webauthn-support"] != "unsupported" &&
        security_key_enabled_for_request? &&
        result.user.has_registered_security_key?
      instrument_two_factor_requested(user: result.user, delivery_options: two_factor_delivery_options(result, use_security_key))

      if use_security_key
        redirect_to u2f_prompt_path
      else
        redirect_to two_factor_prompt_path
      end
    elsif result.external_response_ignored?
      external_auth_redirect

    # Authentication failed and should be tried again.
    else
      message = result.message || "Incorrect username or password."

      remove_saml_cookies if GitHub.auth.saml?

      if result.at_auth_limit_failure?
        # View has special message exceeded auth limits.
        message = nil
        @at_auth_limit = true
      elsif result.password_failure?
        session[:failed_auth_email] = params[:login] if params[:login].match(User::EMAIL_REGEX)
      elsif attempted_user && attempted_user.organization?
        # View has special message for org attempts.
        message = nil
        @tried_signing_into_org = true
      end

      if GitHub.auth.redirect_on_failure?
        anonymous_flash[:message] = message
        redirect_to GitHub.auth.path
      elsif GitHub.auth.saml? || GitHub.auth.cas?
        flash.now[:error] = message
        render "dashboard/logged_out", locals: { index_page: true }
      else
        flash.now[:error] = message

        render "sessions/new"
      end
      # Terminate the worker in the event of an LDAP timeout to prevent any
      # inconsistencies that may occur due to the timeout.
      Process.kill("QUIT", Process.pid) if result.ldap_timeout?
    end

    # We set the session variable here as result.success? || result.two_factor_failure
    # has already been checked in attempt.rb to set compromised_password to be true,
    # it is also to prevent repeating the check in two places in the controller.
    session[::CompromisedPassword::WEAK_PASSWORD_KEY] = result.has_compromised_password
  end

  if GitHub.enterprise?
    # For external auth providers to kick off the auth request
    def external_provider
      render_404
    end
  end

  def confirm_logout
    render "sessions/confirm_logout"
  end

  def destroy
    options = {}.tap do |opts|
      if params[:after_logout].present?
        opts[:redirect_to] = params[:after_logout]
      end
    end

    logout_result = GitHub.auth.rails_logout(request, current_user, **options)

    if logout_result.success?
      clear_weak_password_session_variable
      Rails.logger.info [Time.now, :auth_mode, GitHub.auth_mode, :params, params].inspect
      flash[:stale_session_signedin] = false

      if !logged_in?
        return redirect_to "/"
      else
        @logged_out_user = current_user
        logout_user(:logout)

        # Delete the shared cookie
        if GitHub.enterprise?
          cookies.delete :_gh_render
        end
      end
    end

    anonymous_flash.update(logout_result.flash)
    redirect_to logout_result.redirect_to
  end

  def sudo_modal
    render "sudo/sudo_modal", layout: false
  end

  def sudo
    return head 200 if request.xhr?
    return render_404 if params[:sudo_return_to].nil?
    safe_redirect_to params.delete(:sudo_return_to)
  end

  def in_sudo
    return render_404 unless request.format.json?
    render json: valid_sudo_session?
  end

  def kill_sudo
    return render_404 unless site_admin?
    user_session.expire_sudo
    render plain: "no mas sudo"
  end

  def resend_two_factor_sms
    user = User.find_by_login(session[:two_factor_user])

    if user && user.two_factor_authentication_enabled?
      if user.two_factor_credential.sms_enabled?
        user.two_factor_credential.send_otp_sms(use_alternate_provider = true)
        GitHub.dogstats.increment("two_factor", tags: ["action:resent_otp", "result:success", "two_factor_type:sms"])
      end
      render_code_sent(user)
    else
      render plain: "", status: 403
    end
  rescue GitHub::SMS::Error => e
    render_code_not_sent(user, sms_delivery_error_message(e.message))
  end

  def send_two_factor_fallback_sms
    user = User.find_by_login(session[:two_factor_user])

    if user && user.two_factor_authentication_enabled?
      if user.two_factor_credential.fallback_enabled?
        user.two_factor_credential.send_fallback_sms(:sign_in)
      end
      render_code_sent(user)
    else
      render plain: "", status: 403
    end
  rescue GitHub::SMS::Error => e
    render_code_not_sent(user, sms_delivery_error_message(e.message))
  end

  def suspended
    return redirect_to home_url if logged_in? && !current_user.suspended?

    @message = customized(:suspended_message)
    if @message.blank?
      @message = default_suspended_message
    end

    render "sessions/suspended"
  end

  if GitHub.private_mode_enabled?
    # nginx will redirect to this endpoint if an Enterprise subdomain request
    # isn't authenticated. `touch_user_session_and_device` will fixup the domain on the
    # cookie for us.
    def auth_request_bounce
      unless logged_in?
        return redirect_to_login(params[:return_to])
      end

      redirect_to_return_to
    end
  end

  private

  def anonymous_required
    # Assumes return_to has been set via the set_return_to before_filter.
    redirect_to_return_to if logged_in?
  end

  def sms_delivery_error_message(message)
    "We tried sending an SMS to your configured number, but #{message}." +
    " Please contact support if you continue to have problems."
  end

  def render_2fa_prompt(user)
    @user = user
    render "sessions/two_factor_prompt"
  end

  def render_code_sent(user)
    head :ok
  end

  def render_code_not_sent(user, message)
    render plain: message, status: 422
  end

  def remove_saml_cookies
    cookies.delete(:saml_return_to)
    cookies.delete(:saml_csrf_token)
    cookies.delete(:saml_return_to_legacy)
    cookies.delete(:saml_csrf_token_legacy)
  end

  def set_partially_authenticated_two_factor_session(user)
    session[:two_factor_user] = user.login
    session[:otp_retry_expires_at] = Time.now.to_i + TwoFactorCredential::OTP_RETRY_WINDOW
  end

  def get_partially_authenticated_two_factor_session
    user_login = session.delete(:two_factor_user)
    otp_retry_expires_at = session.delete(:otp_retry_expires_at)
    otp_retry_allowed = otp_retry_expires_at && Time.now.to_i < otp_retry_expires_at
    [user_login, otp_retry_allowed]
  end

  def two_factor_rate_limit_max
    if SMS_DELIVERY_ACTIONS.include?(params[:action])
      5
    elsif OTP_ENTRY_ACTIONS.include?(params[:action])
      10
    end
  end

  def two_factor_rate_limt_ttl
    if SMS_DELIVERY_ACTIONS.include?(params[:action])
      60.minutes
    elsif OTP_ENTRY_ACTIONS.include?(params[:action])
      5.minutes
    end
  end

  def two_factor_rate_limit_key
    ["session_auth", params[:action], authentication_user.id].join(":")
  end

  def two_factor_rate_limit_render
    if SMS_DELIVERY_ACTIONS.include?(params[:action])
      # Use GitHub::SMS::RateLimitError to ensure consistent error messages
      error = GitHub::SMS::RateLimitError.new
      message = sms_delivery_error_message(error.message)
      render plain: message, status: 429
    elsif authentication_user
      message = <<~MSG
      We were unable to authenticate your request because too many codes have been submitted.
      Please wait a few minutes and contact support if you continue to have problems.
      MSG
      render plain: message, status: 429
    else
      render plain: "", status: 403
    end
  end

  # Private: Overrides ApplicationController#verify_authenticity_token?
  #
  # Returns true if we're routing from SAML, and the current auth adaptor is
  # SAML. This is because sessions#create is shared by dotcom built-in auth, and
  # GitHub Cloud saml auth.
  def verify_authenticity_token?
    super && request.path != "/saml/consume"
  end

  # Private: convenience method to populate a hash of available 2FA challenges
  # at the time of a challenge, not necessarily the method that was used
  # to solve the challenge.
  def two_factor_delivery_options(result, use_u2f)
    delivery_options = {
      totp: result.user.two_factor_credential.delivery_method,
    }

    if use_u2f
      delivery_options[:u2f] = true
    end

    if result.user.two_factor_credential.fallback_enabled?
      delivery_options[:backup_sms_number] = true
    end

    delivery_options
  end

  def authentication_user_required
    redirect_to_login unless authentication_user
  end

  # Private: Log the type of two factor request
  # for the given user.
  #
  # Example:
  #
  #   instrument_two_factor_requested(user: current_user, delivery_options: { totp: "app" })
  #
  # Returns nil.
  def instrument_two_factor_requested(user:, delivery_options:)
    user.instrument_two_factor_requested(
      actor_ip: request.remote_ip,
      note: "From #{request.host}",
      delivery_options: delivery_options,
    )
  end

  # Private: Log 2fa recovery code use
  def instrument_two_factor_recover(user)
    GitHub.dogstats.increment("two_factor", tags: ["action:recover", "result:success"])
    user.instrument_two_factor_recover(
      actor_ip: request.remote_ip,
      note: "From #{request.host}",
    )
  end

  # Private: Log 2fa successful challenge responses
  def instrument_two_factor_challenge_success(user:, type:)
    GitHub.dogstats.increment("two_factor", tags: ["action:challenge", "result:success", "two_factor_type:#{type}"])

    user.instrument_two_factor_challenge_success(
      actor_ip: request.remote_ip,
      note: "From #{request.host}",
      two_factor_type: type,
    )
  end

  # Private: Log 2fa challenge failures
  def instrument_two_factor_challenge_failure(user:, type:)
    GitHub.dogstats.increment("two_factor", tags: ["action:challenge", "result:failure", "two_factor_type:#{type}"])

    user.instrument_two_factor_challenge_failure(
      actor_ip: request.remote_ip,
      note: "From #{request.host}",
      two_factor_type: type,
    )
  end
end
