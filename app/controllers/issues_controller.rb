# frozen_string_literal: true

class IssuesController < AbstractRepositoryController

  statsd_tag_actions only: [:create, :index, :lock, :new, :show, :unlock, :update, :show_from_project]

  include ShowPartial
  include IssueUpdate
  include ProgressiveTimeline
  include PrecacheIssues
  include LabelEducationHelper
  include TimelineHelper

  # The following actions do not require conditional access checks:
  # - dashboard: serves `/issues`, not consistently scoped to an organization.
  #   Enforcement may be required but should be done inline.
  # - redirect_to_scoped_org_dashboard: redirects to #dashboard, does not access
  #   protected organization resources before redirecting
  skip_before_action :perform_conditional_access_checks, only: %w(
    dashboard
    redirect_to_scoped_org_dashboard
    pr_review_status
  )

  around_action :track_and_report_render_view_time, only: [:show]
  around_action :track_and_report_graphql_executions, only: [:show]
  around_action :track_and_report_mysql_executions, only: [:show]
  before_action :enabled_new_issues_required, only: %w(new create)
  before_action :login_required,
    except: %w(index new choose show_menu_content show show_partial show_from_project pr_review_status)
  before_action :login_required_redirect_for_public_repo, only: [:new, :choose]
  before_action :writable_repository_required,
    except: %w(index dashboard show show_partial show_menu_content
               redirect_to_scoped_org_dashboard show_from_project pr_review_status)
  before_action :issue_modifiers_only, only: %w(triage update)
  before_action :set_milestone_permission_required, only: %w(set_milestone)
  before_action :issue_required, only: %w(destroy update set_milestone dismiss_first_contribution_prompt show_from_project convert_to_discussion)
  before_action :content_authorization_required, only: %w(create update new)
  before_action :handle_issue_transfer_deletion_or_conversion, only: :show
  skip_before_action :cap_pagination, unless: :robot?
  skip_before_action :authorization_required, only: %w(dashboard redirect_to_scoped_org_dashboard show_menu_content)

  layout :repository_layout

  def index
    self.pulls_only = index_flow.pulls_only

    query = index_flow.query

    self.parsed_issues_query = Search::Queries::IssueQuery.parse(query, current_user)

    return safe_redirect_to(index_flow.redirect_path) if index_flow.needs_redirection?

    query_error  = false
    issues       = []
    open_count   = 0
    closed_count = 0

    begin
      result = Issue::SearchResult.search(
        query:              parsed_issues_query,
        current_user:       current_user,
        remote_ip:          request.remote_ip,
        repo:               current_repository,
        force_pulls:        index_flow.force_pulls?,
        page:               params[:page],
        show_spam_to_staff: GitHub.flipper[:show_spammy_issues_to_staff].enabled?(current_user),
      )

      issues       = result[:issues]
      open_count   = result[:open_count]
      closed_count = result[:closed_count]
    rescue Elastomer::Client::Error => boom
      Failbot.push app: "github-user"
      Failbot.report boom
      query_error  = true
    end

    # disallow robots if labels or milestone is given
    label_or_milestone = parsed_issues_query.assoc(:label) || parsed_issues_query.assoc(:milestone)
    if robot? && label_or_milestone
      return render_404
    end

    reindex_unsynced_pulls(issues, query)

    if pulls_only?
      override_analytics_location "/<user-name>/<repo-name>/pull_requests/index"
    end
    strip_analytics_query_string

    respond_to do |format|
      format.html do
        render "issues/index", locals: {
          issues: issues,
          pulls_only: pulls_only?,
          query: query,
          query_error: query_error,
          open_count: open_count,
          closed_count: closed_count,
          milestones_count: current_repository.milestones.open_milestones.count,
          labels_count: current_repository.labels.count,
        }
      end
    end
  end

  def choose
    issue_templates = current_repository.preferred_issue_templates
    config = issue_templates.issue_template_config

    locals = {
      issue_templates: issue_templates,
      config: config,
    }

    unless issue_templates.valid_templates.any? || config&.contact_links.present?
      safe_new_issue_params = indifferent_params.slice(:permalink, :milestone)
      return redirect_to new_issue_path(current_repository.owner, current_repository, params: safe_new_issue_params)
    end

    respond_to do |format|
      format.html { render "issues/choose", locals: locals }
    end
  end

  def dashboard
    current_organization = Organization.find_by_login(params[:user]) if params[:user]

    unless required_external_identity_session_present?(target: current_organization)
      render_external_identity_session_required(target: current_organization)
      return
    end

    flow = Issue::ControlFlow.new(
      params:       params,
      components:   parsed_issues_query,
      current_path: request.fullpath,
      current_user: current_user,
      exclude_archived: true,
    )
    query = flow.query
    self.pulls_only = flow.pulls_only

    self.parsed_issues_query = Search::Queries::IssueQuery.parse(query, current_user)

    return safe_redirect_to(flow.redirect_path) if flow.needs_redirection?

    result = Issue::SearchResult.search(
      query:             parsed_issues_query,
      current_user:      current_user,
      remote_ip:         request.remote_ip,
      user_session:      user_session,
      page:              params[:page],
    )

    issues       = result[:issues]
    open_count   = result[:open_count]
    closed_count = result[:closed_count]

    if logged_in?
      GitHub.tracer.with_span("issues#dashboard prefills") do |span|
        GitHub::PrefillAssociations.for_issues(issues, current_user: current_user)
        if pulls_only?
          pull_requests = issues.map(&:pull_request).compact
          if pull_requests.any?
            GitHub::PrefillAssociations.for_pulls(pull_requests, issues: issues)
            pull_requests.group_by(&:repository).each do |repo, repo_pull_requests|
              GitHub::PrefillAssociations.for_pulls_protected_branches(repo, repo_pull_requests)
              PullRequest.attach_statuses(repo, repo_pull_requests)
            end
          end
        end
      end
    end
    strip_analytics_query_string

    render "issues/dashboard",
      layout: (pjax? ? "layouts/pjax" : "application"),
      locals: {
        issues: issues,
        query: query,
        pulls_only: pulls_only?,
        open_count: open_count,
        closed_count: closed_count,
      }
  end

  ShowQuery = parse_query <<-'GRAPHQL'
    query(
      $id: ID!,
      $timelineSince: DateTime,
      $timelinePageSize: Int,
      $syntaxHighlightingEnabled: Boolean = false,
      $deferCollapsedThreads: Boolean = false,
      $focusedReviewThread: ID,
      $focusedReviewComment: ID,
      $hasFocusedReviewThread: Boolean = false
      $hasFocusedReviewComment: Boolean = false
      $scopedItemTypes: [IssueTimelineItemsItemType!]
    ) {
      node(id: $id) {
        ...Views::Issues::Show::Issue
      }
    }
  GRAPHQL

  FormButtonQuery = parse_query <<-'GRAPHQL'
    query($id: ID!) {
      node(id: $id) {
        ...Issues::FormButtonsView::FormButtonClosable
      }
    }
  GRAPHQL

  around_action :record_show_metrics, only: [:show]
  private def record_show_metrics
    action_start_time = GitHub::Dogstats.monotonic_time

    yield

    GitHub.dogstats.timing_since("issue.show", action_start_time, tags: [
      "logged_in:#{logged_in?}",
    ])
  end

  def show
    @issue = current_issue

    return render_404 if !@issue
    return render_404 if @issue.hide_from_user?(current_user)
    return render_404 unless @issue.readable_by?(current_user)

    set_hovercard_subject(@issue)

    request.format = :html if request.format.blank? || request.format.nil?

    response.headers["Vary"] = [response.headers["Vary"], "Accept"].compact.join(",")

    request.env["issue.record_id"] = @issue.id

    respond_to do |format|
      format.html do

        # Legacy issues/issue/xxx routes
        if params[:legacy] && params[:id]
          redirect_to issue_path(current_repository.owner, current_repository, current_issue), status: 301
          return
        end

        if @issue.pull_request?
          redirect_to show_pull_request_path(current_repository.owner, current_repository, current_issue)
          return
        end

        render_404 and return if !current_repository.has_issues?

        mark_thread_as_read @issue

        scope_timeline = scoped_issue_timeline_enabled? && @issue.has_scoped_timeline_items?

        issue_node = track_query_execution("show", ["scoped_timeline:#{scope_timeline}"]) do
          issue_node_for(ShowQuery, scope_timeline: scope_timeline)
        end

        discussion_categories = if discussions_enabled? && discussion_categories_enabled?
          current_repository.discussion_categories.sort
        else
          []
        end

        render "issues/show", locals: {
          issue_node: issue_node,
          scope_timeline: scope_timeline,
          discussion_categories: discussion_categories
        }
      end

      format.json do
        render json: { title: @issue.title }
      end
    end
  end

  def new
    fields = PrefilledIssueFields.new(
      params: params,
      repository: current_repository,
      user: current_user,
    )

    @issue = current_repository.issues.new
    @issue.title = fields.title
    @issue.body = fields.body
    @issue.labels = fields.labels
    @issue.projects = fields.projects
    @issue.milestone = fields.milestone
    @issue.assignees = fields.assignees
    @issue.body_template_name = fields.template

    respond_to do |format|
      format.html do
        render "issues/new"
      end
    end
  end

  def create
    if !logged_in? || blocked_by_owner?
      flash[:error] = "You can't perform that action at this time."
      redirect_to current_repository.permalink
      return
    end

    GitHub.context.push(spamurai_form_signals: spamurai_form_signals)
    @issue = build_issue indifferent_params
    saved = false
    begin
      saved = GitHub::SchemaDomain.allowing_cross_domain_transactions do
        Issue.transaction do
          ProjectCard.transaction do
            @issue.save
          end
        end
      end
    rescue GitHub::Prioritizable::Context::LockedForRebalance
      GitHub.dogstats.increment("milestones.exceptions.locked_for_rebalance", { tags: ["context:issues_controller.create"] })
      flash.now[:error] = "Sorry! This milestone is temporarily locked for maintenance. Please try again."
    end

    if saved
      instrument_issue_creation_via_ui(@issue)
      instrument_saved_reply_use(params[:saved_reply_id], "issue")
      path = issue_path(current_repository.owner, current_repository, @issue)
      if request.xhr?
        render json: {url: path}
      else
        redirect_to path
      end
    else
      @labels = current_repository.labels.order("name")
      render "issues/new"
    end
  end

  # Bulk update a set of issues state, assignee, milestone or labels.
  #
  # PUT /github/github/issues/triage
  #
  # issues    - Array of Issue numbers or a String search query.
  # assignee  - A User ID to assign the issues to or blank to unassign.
  # milestone - A Milestone ID to set the issues to or black to unset.
  # labels    - A Hash of Label IDs with values '1' or "0" to add or remove.
  # state     - A string "open" or "closed" to assign to issues.
  def triage
    case params[:issues]
    when String
      issues = Issue::SearchResult.search(
        query:             params[:issues],
        current_user:      current_user,
        remote_ip:         request.remote_ip,
        repo:              current_repository,
      )[:issues]
    when Array
      issues = current_repository.issues.where("issues.number IN (?)", params[:issues]).to_a
    else
      issues = []
    end

    unless current_repository.has_issues?
      issues = issues.select(&:pull_request?)
    end

    # copy over params for IssueTriageJob
    job_params = {}
    job_params[:state] = params[:state] if params.key?(:state)
    job_params[:milestone] = params[:milestone] if params.key?(:milestone)
    job_params[:assignee] = params[:assignee] if params.key?(:assignee)
    job_params[:clear_assignees] = params[:clear_assignees] if params.key?(:clear_assignees)

    if params[:assignees].respond_to?(:each)
      job_params[:assignees] = {}
      params[:assignees].each { |k, v| job_params[:assignees][k] = v }
    end

    if params[:labels].respond_to?(:each)
      job_params[:labels] = {}
      params[:labels].each { |k, v| job_params[:labels][k] = v }
    end

    status = JobStatus.create
    IssueTriageJob.perform_later(status.id, issues.map(&:id), current_user.id, job_params)

    if request.xhr?
      render json: { job: { url: job_status_url(status.id) } }
    else
      redirect_to :back
    end
  end

  FormActionChecksQuery = parse_query <<-'GRAPHQL'
    query($id: ID!) {
      node(id: $id) {
        ...Views::Issues::FormActions::FormActionFragment
      }
    }
  GRAPHQL

  StateButtonChecksQuery = parse_query <<-'GRAPHQL'
    query($id: ID!) {
      node(id: $id) {
        ...Views::Issues::StateButtonWrapper::StateButtonFragment
      }
    }
  GRAPHQL

  TimelineQuery = parse_query <<-'GRAPHQL'
    query(
      $id: ID!,
      $timelineSince: DateTime,
      $timelinePageSize: Int,
      $syntaxHighlightingEnabled: Boolean = false,
      $deferCollapsedThreads: Boolean = false,
      $focusedReviewThread: ID,
      $focusedReviewComment: ID,
      $hasFocusedReviewThread: Boolean = false
      $hasFocusedReviewComment: Boolean = false
      $scopedItemTypes: [IssueTimelineItemsItemType!]
    ) {
      node(id: $id) {
        ...Views::Issues::Timeline::Issue
      }
    }
  GRAPHQL

  def show_partial
    partial = params[:partial]
    return head :not_found unless valid_partial?(partial)

    milestone_partial = "issues/sidebar/new/milestone"
    if request.post? && milestone_partial == partial && params[:milestone] == "new"
      params[:milestone] = create_milestone(params[:milestone_title])
    end

    GitHub.dogstats.time("view", tags: ["subject:issue", "action:show_partial_find"]) do
      @issue = params[:id] ? current_issue : build_issue(indifferent_params)
    end

    return head :not_found if @issue.nil?
    return head :not_found unless @issue.new_record? || @issue.pull_request? || current_repository.has_issues?

    scope_timeline = scoped_issue_timeline_enabled? && ActiveModel::Type::Boolean.new.cast(params[:scope_timeline])

    issue_node = case partial
    when "issues/form_actions"
      issue_node_for(FormActionChecksQuery)
    when "issues/state_button_wrapper"
      issue_node_for(StateButtonChecksQuery)
    when "issues/timeline"
      track_query_execution("timeline", ["scoped_timeline:#{scope_timeline}"]) do
        issue_node_for(TimelineQuery, scope_timeline: scope_timeline)
      end
    end

    discussion_categories = if partial == "issues/sidebar" &&
        current_repository && discussions_enabled? && discussion_categories_enabled?
      current_repository.discussion_categories.sort
    else
      []
    end

    GitHub.dogstats.time("view", tags: ["subject:issue", "action:show_partial_render", "partial:#{partial}"]) do
      respond_to do |format|
        format.html do
          locals = {
            issue: @issue,
            issue_node: issue_node,
            deferred_content: false,
            sticky: params[:sticky] == "true",
            discussion_categories: discussion_categories,
          }

          if partial == "issues/sidebar/project_card_move"
            # The show_columns_menu param can be passed to override the show_columns_menu local being made true by default.
            # Passing false prevents showing the caret for the column dropdown in the projects sidebar.
            # Currently limited to issues/sidebar/project_card_move partial and others will need to be added to this list to use it.
            show_columns_menu = true unless (params.has_key?(:show_columns_menu) && params[:show_columns_menu] != "true")
            locals = locals.merge({ show_columns_menu: show_columns_menu })
          end

          if partial == "issues/timeline"
            locals[:scope_timeline] = scope_timeline
          end

          # rubocop:disable GitHub/RailsControllerRenderLiteral
          render partial: partial, object: @issue, layout: false, locals: locals
        end
        format.json do
          assignees_partial = "issues/sidebar/assignees_menu_content"
          if assignees_partial == partial
            sorted_assignees = @issue.sorted_assignees_list(current_user: current_user)

            user_data = GitHub.dogstats.time("assignees.prepare_users") do
              sorted_assignees.map do |user|
                profile_name = user.profile_name
                user_status = user.user_status
                if !user_status&.expired? && user_status&.limited_availability?
                  profile_name = "#{profile_name} (busy)"
                end

                {
                  id: user.id,
                  name: profile_name,
                  login: user.login,
                  selected: @issue.assigned_to?(user),
                  avatar: user.primary_avatar_url(60),
                  class: helpers.avatar_class_names(user),
                }
              end
            end

            GitHub.dogstats.time("assignees.render") do
              render json: { users: user_data }
            end
          else
            head :bad_request
          end
        end
      end
    end
  end

  def show_menu_content
    partial = params[:partial]
    authorization_required if current_repository

    return head :not_found unless valid_partial?(partial)

    query = params_or_default_query_string
    locals = {
      issue: @issue,
      query: query,
      pulls_only: pulls_only?,
    }

    GitHub.dogstats.time("view", tags: ["subject:issue", "action:show_partial_render"]) do
      respond_to do |format|
        format.html do
          render partial: partial, layout: false, locals: locals # rubocop:disable GitHub/RailsControllerRenderLiteral
        end
      end
    end
  end

  UnmarkAsDuplicateQuery = parse_query <<-'GRAPHQL'
    mutation($input: UnmarkIssueAsDuplicateInput!) {
      unmarkIssueAsDuplicate(input: $input) {
        duplicate {
          __typename # no-op
        }
      }
    }
  GRAPHQL

  def unmark_as_duplicate
    input = { duplicateId: current_issue.global_relay_id, canonicalId: params[:issue_id] }
    data = platform_execute(UnmarkAsDuplicateQuery, variables: { input: input })

    if data.errors.any?
      error_type = data.errors.details[:unmarkIssueAsDuplicate]&.first["type"]
      return head :not_found if %w[NOT_FOUND SERVICE_UNAVAILABLE].include?(error_type)
      return head :forbidden if error_type == "FORBIDDEN"

      message = data.errors.messages.values.join(", ")

      return render json: { error: message }, status: :unprocessable_entity
    end

    head :ok
  end

  def update
    return head 200 unless logged_in? && !blocked_by_owner?
    original_unsafe_params = indifferent_params
    issue_update = true

    if params[:pull_request]
      unsafe_params = original_unsafe_params[:pull_request]
      issue_update = false
    else
      unsafe_params = original_unsafe_params.fetch :issue, {}
    end

    unsafe_params.merge(body: params[:comment]) if params[:comment]
    safe_issue_params = unsafe_params.slice(
      :milestone_id,
      :title,
      :body,
    )

    valid = true
    issue = current_issue
    previous_title = issue.title
    previous_body = issue.body

    # prevent updates from stale data
    if stale_model?(current_issue)
      return render_stale_error(model: current_issue, error: "Could not edit issue. Please try again.", path: issue_path(current_issue))
    end

    if current_repository.pushable_by?(current_user) && safe_issue_params[:milestone_id] == "clear"
       safe_issue_params[:milestone_id] = nil
    else
      safe_issue_params.delete(:milestone_id)
    end

    valid = issue.trigger_change_labels do
      GitHub::SchemaDomain.allowing_cross_domain_transactions do
        issue.update(safe_issue_params.except(:body))
      end
    end

    mark_thread_as_read issue

    partial = params[:partial]

    if operation = TaskListOperation.from(params[:task_list_operation])
      text = operation.call(issue.body)
      valid = issue.update_body(text, current_user) if text
    elsif safe_issue_params[:body]
      valid = issue.update_body(safe_issue_params[:body], current_user)
    end

    if valid
      issue.instrument_hydro_update_event(
        actor: current_user,
        updater: current_user,
        repo: current_repository,
        prev_title: previous_title,
        prev_body: previous_body,
      )
      track_issue_edits_from_project_board(edited_fields: safe_issue_params.keys)
    end

    if request.xhr? && partial && valid_partial?(partial)
      partial = "#{partial}.html.erb"
      respond_to do |format|
        format.html do
          render partial: partial, locals: { issue: issue } # rubocop:disable GitHub/RailsControllerRenderLiteral
        end
      end
    elsif request.xhr?
      respond_to do |format|
        format.json do
          if valid
            render json: issue_update_payload(issue.reload)
          else
            render json: { errors: issue.errors.full_messages }, status: :unprocessable_entity
          end
        end
      end
    else
      respond_to do |format|
        format.html { redirect_to issue_path(issue) }
      end
    end
  end

  DeleteIssueMutation = parse_query <<~'GRAPHQL'
    mutation($input: DeleteIssueInput!) {
      deleteIssue(input: $input) {
        repository {
          resourcePath
        }
      }
    }
  GRAPHQL
  def destroy
    # verify delete because of: https://github.com/github/github/issues/103421
    return render_404 unless params[:verify_delete] == "1"

    input = { issueId: current_issue.global_relay_id }
    data = platform_execute(DeleteIssueMutation, variables: { input: input })

    if data.errors.any?
      flash[:error] = "Issue could not be deleted"
      redirect_to :back
      return
    end

    flash[:notice] = "The issue was successfully deleted."
    redirect_to data.delete_issue.repository.resource_path.to_s + "/issues"
  end

  # Sets the milestone for one or many issues.
  #
  # Expected params:
  #
  #   :milestone - The Milestone's id you wish to set. Optionally "clear" if
  #                you wish to clear the current milestones.
  #   :new_milestone - A title of a new milestone to create.
  #   :issues - An Array of Issue numbers.
  #
  # If :new_milestone is sent, :milestone is ignored and a *new* milestone
  # is created (without a due date) and the issue(s) are assigned to the new
  # milestone.
  #
  # For AJAX requests, JSON is returned with the HTML of the infobar and
  # context pane partials.
  def set_milestone
    if params[:milestone] == "new"
      milestone = create_milestone(params[:milestone_title])
      return head :unprocessable_entity unless milestone
    elsif params[:milestone] == "clear"
      milestone = nil
    else
      milestone = current_repository.milestones.find(params[:milestone])
    end

    issue = current_issue
    if issue.milestone != milestone
      begin
        Issue.transaction do
          issue.milestone = milestone
          issue.save!
        end
      rescue GitHub::Prioritizable::Context::LockedForRebalance
        GitHub.dogstats.increment("milestones.exceptions.locked_for_rebalance", { tags: ["context:issues_controller.set_milestone"] })
        return render status: 503, plain: "Sorry! This milestone is temporarily locked for maintenance. Please try again."
      end
    end

    partial = "issues/sidebar/show/milestone"
    partial = "#{params[:partial]}" if valid_partial?(params[:partial])

    @issue = issue

    respond_to do |format|
      format.html do
        render partial: partial, locals: { issue: issue } # rubocop:disable GitHub/RailsControllerRenderLiteral
      end
    end
  end

  def create_milestone(title)
    return unless current_repository.pushable_by?(current_user)
    return unless title

    milestone = current_repository.milestones.build(title: title)
    milestone.created_by = current_user
    return milestone if milestone.save
  end

  LockIssueMutation = PlatformClient.parse <<-'GRAPHQL'
    mutation($input: LockLockableInput!) {
      lockLockable(input: $input)
    }
  GRAPHQL

  def lock
    lock_reason = params[:reason].present? ? Platform::Enums::Base.convert_string_to_enum_value(params[:reason]) : nil
    input = {
      lockableId: current_issue.global_relay_id,
      lockReason: lock_reason,
    }
    platform_execute(LockIssueMutation, variables: { input: input })

    redirect_to :back
  end

  UnlockIssueMutation = PlatformClient.parse <<-'GRAPHQL'
    mutation($input: UnlockLockableInput!) {
      unlockLockable(input: $input)
    }
  GRAPHQL

  def unlock
    input = { lockableId: current_issue.global_relay_id }
    platform_execute(UnlockIssueMutation, variables: { input: input })

    redirect_to :back
  end

  def convert_to_discussion
    return render_404 unless show_discussions?

    unless current_issue.can_be_converted_by?(current_user)
      flash[:error] = "You cannot convert that issue to a discussion at this time."
      return redirect_to(issue_path(current_issue))
    end

    category = if params[:category_id]
      current_repository.discussion_categories.find_by(id: params[:category_id])
    end

    converter = IssueToDiscussionConverter.new(current_issue, actor: current_user, category: category)

    unless converter.prepare_for_conversion
      message = "Unable to convert this issue to a discussion. "
      if converter.discussion
        message += converter.discussion.errors.full_messages.to_sentence
      else
        message += current_issue.errors.full_messages.to_sentence
      end
      flash[:error] = message
      return redirect_to(issue_path(current_issue))
    end

    ConvertToDiscussionJob.perform_later(current_user, converter.discussion,
      converter.issue_originally_open)

    redirect_to discussion_path(converter.discussion, current_repository, converting: "1")
  end

  def linked_closing_reference
    return render_404 unless current_issue

    close_issue_references = CloseIssueReference.viewable_for(viewer: current_user, issue: current_issue)

    return render_404 unless close_issue_references.size == 1

    if current_issue.pull_request?
      issue = close_issue_references.first.issue
      GlobalInstrumenter.instrument("browser.issue_cross_references.click",
                                    reference_location: params[:reference_location],
                                    user_id: current_user,
                                    issue_id: issue.id,
                                    pull_request_id: current_issue.pull_request_id)

      redirect_to issue_path(issue)
    else
      pr = close_issue_references.first.pull_request
      GlobalInstrumenter.instrument("browser.issue_cross_references.click",
                                    reference_location: params[:reference_location],
                                    user_id: current_user,
                                    issue_id: current_issue.id,
                                    pull_request_id: pr.id)

      redirect_to pull_request_path(pr)
    end
  end

  def redirect_to_scoped_org_dashboard
    if org = Organization.where(login: params[:org]).first
      route = pulls_only? ? all_pulls_path(user: org) : all_issues_path(user: org)
      redirect_to route
    else
      route = pulls_only? ? all_pulls_path : all_issues_path
      redirect_to route
    end
  end

  ShowFromProjectQuery = parse_query <<-'GRAPHQL'
    query($id: ID!) {
      node(id: $id) {
        ...Views::Projects::CardIssueDetails::Comment
      }
    }
  GRAPHQL

  def show_from_project
    variables = { id: current_issue.global_relay_id }
    issue_node = platform_execute(ShowFromProjectQuery, variables: variables).node
    return render_404 unless issue_node

    respond_to do |format|
      format.html do
        render partial: "projects/card_issue_details", locals: { issue: current_issue, issue_node: issue_node }
      end
    end
  end

  def dismiss_first_contribution_prompt
    return head :not_found unless current_issue.show_first_contribution_prompt?(current_user)
    current_issue.dismiss_first_contribution_prompt
    head :ok
  end

  def dismiss_first_contribution_prompt_and_redirect
    return render_404 unless current_issue.show_first_contribution_prompt?(current_user)
    current_issue.dismiss_first_contribution_prompt
    redirect_to community_path
  end

  def pr_review_status
    return render_404 unless current_issue.pull_request?
    return render_404 if current_issue.hide_from_user?(current_user)
    return render_404 unless current_issue.readable_by?(current_user)

    # If we're not SSO'd for the issue, don't render anything
    owner = current_issue.repository.owner
    if owner.organization? && !required_external_identity_session_present?(target: owner)
      return render_404
    end

    render partial: "pull_requests/review_status", locals: { pull_request: current_issue.pull_request }
  end

  private

  def set_milestone_permission_required
    render_404 unless current_issue.can_set_milestone?(current_user)
  end

  def indifferent_params
    params.permit!.to_h.with_indifferent_access
  end

  def parsed_issues_query=(value)
    @parsed_issues_query = value
  end

  def parsed_issues_query
    @parsed_issues_query ||= Search::Queries::IssueQuery.normalize(
      Search::Queries::IssueQuery.parse(params_or_default_query_string, current_user),
    )
  end
  helper_method :parsed_issues_query

  def current_issue
    return @current_issue if defined?(@current_issue)

    @current_issue = current_repository.issues.includes(:labels).find_by_number(params[:id].to_i).tap do |issue|
      # Loads the participant data if it hasn't been loaded already. We load max of 50, since we limit the number of user
      # profiles that are displayed on the show page and don't need to load them all into memory.
      issue&.participants(user_limit: Issue::PARTICIPANT_LOAD_LIMIT)
    end
  end
  helper_method :current_issue
  alias :timeline_owner :current_issue

  def issue_node_for(query, scope_timeline: false)
    timeline_owner_response_for(query: query, id: @issue.global_relay_id, scope_timeline: scope_timeline)&.node
  end

  def pulls_only?
    return @_pulls_only if defined?(@_pulls_only)
    @_pulls_only = ActiveRecord::Type::Boolean.new.deserialize(params[:pulls_only])
  end

  def pulls_only=(value)
    @_pulls_only = value
  end

  def params_or_default_query_string
    params[:q] || "is:#{pulls_only? ? :pr : :issue} is:open "
  end

  # A filter to check whether you have permission to modify this issue or group
  # of issues.
  #
  # Redirects away unless you have access.
  def issue_modifiers_only
    return redirect_to_login unless logged_in?
    return redirect_to "/" unless current_repository

    allowed = if current_issue
      current_issue.editable_by?(current_user)
    else
      current_user_can_push?
    end

    redirect_to "/" unless allowed
  end

  def populate_assignees
    if current_repository
      users = current_repository.available_assignees.limit(GitHub.assignees_list_limit).to_a || []
      users.sort! { |a, b| a.login.downcase <=> b.login.downcase }

      if logged_in?
        users.delete(current_user)
        users.unshift(current_user)
      end

      users
    else
      [current_user]
    end
  end
  helper_method :populate_assignees

  def populate_authors
    if current_repository
      users = populate_assignees
      installations = IntegrationInstallation.with_repository(current_repository).includes(integration: :bot)
      bots = installations.map(&:integration).map(&:bot)
      users.concat(bots)

      users.sort! { |a, b| a.login.downcase <=> b.login.downcase }

      if logged_in?
        users.delete(current_user)
        users.unshift(current_user)
      end

      users
    else
      [current_user]
    end
  end
  helper_method :populate_authors

  def enabled_new_issues_required
    if !current_repository.has_issues? || current_repository.archived?
      return render_404
    end
  end

  def issue_required
    if current_issue.nil?
      render_404
    end
  end

  def handle_issue_transfer_deletion_or_conversion
    return if current_issue

    if show_discussions? && (discussion = Discussion.for_repository(current_repository).with_number(params[:id]).first)
      if discussion.converted_from_issue?
        flash[:notice] = "That issue was converted to a discussion."
      end
      redirect_to discussion_path(discussion)
    elsif deleted_issue = DeletedIssue.find_by(repository_id: current_repository.id, number: params[:id])
      render "issues/deleted", locals: { deleted_issue: deleted_issue }
    elsif issue_transfer = IssueTransfer.find_from(repository: current_repository, number: params[:id])
      transferred_issue = issue_transfer.new_issue
      new_repository = transferred_issue.repository
      if new_repository&.readable_by?(current_user) && !new_repository.hide_from_user?(current_user)
        flash[:notice] = "This issue was transferred here."
        redirect_to issue_path(transferred_issue)
      else
        render "issues/transfer_no_access"
      end
    elsif IssueTransfer.find_by(old_repository_id: current_repository.id, old_issue_number: params[:id])
      render "issues/transfer_no_access", locals: { issue_deleted: true }
    else
      render_404
    end
  end

  def content_authorization_required
    authorize_content(:issue, repo: current_repository)
  end

  def index_flow
    @index_flow ||= Issue::ControlFlow.new(
      params: params,
      repo:   current_repository,
      components: parsed_issues_query,
      current_user: current_user,
      current_path: request.fullpath,
    )
  end

  def route_supports_advisory_workspaces?
    return true if current_issue&.pull_request?
    partial = params[:partial]
    if action_name == "show_partial"
      assignee_partials = ["issues/sidebar/assignees_menu_content", "issues/sidebar/new/assignees"]
      return true if assignee_partials.include?(partial)
    elsif action_name == "show_menu_content"
      return true if ["issues/filters/authors_content", "issues/filters/assigns_content"].include?(partial)
    end
    return false if action_name != "index"
    index_flow.pulls_only
  end

  def reindex_unsynced_pulls(issues, query)
    # only rightmost "is:{open,closed}" clause is observed; if it's "is:open", check for resync
    rightmost_is = parsed_issues_query.reverse.find do |pull|
      pull[0] == :is && %w(open closed).include?(pull[1])
    end.try(:second)

    return unless rightmost_is == "open"

    closed_pulls = issues.map(&:pull_request).compact.select do |pull|
      pull.closed? || pull.merged?
    end

    closed_pulls.each do |pull|
      pull.resynchronize_search_index
    end
  end
end
