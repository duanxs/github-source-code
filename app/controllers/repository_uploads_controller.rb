# frozen_string_literal: true

class RepositoryUploadsController < AbstractRepositoryController
  areas_of_responsibility :code_collab

  include TreeHelper
  layout :repository_layout

  before_action :ensure_valid_branch_name, only: [:index]

  def index
    view = Repositories::UploadView.new(
      current_user: current_user,
      branch: params[:name] || current_repository.default_branch,
      path: params[:path],
      parent_repo: current_repository,
      forked_repo: nil,
      target_branch: nil,
      quick_pull: true)

    render "repository_uploads/index", locals: { view: view }
  end

  def create
    manifest = UploadManifest.where(id: params[:manifest_id], repository_id: current_repository.id).first

    if !manifest
      flash[:error] = "Add some files to include in this commit."
      return redirect_to :back
    end

    # Page reload will display push requirement message.
    if !manifest.repository.pushable_by?(current_user)
      return redirect_to :back
    end

    return head 404 unless manifest.uploader == current_user
    return head 400 unless manifest.state_new?
    return head 400 unless manifest.complete?

    if manifest.files.empty?
      flash[:error] = "Add some files to include in this commit."
      return redirect_to :back
    end

    manifest.update_attribute(:base_branch, params[:quick_pull]) unless direct_edit?

    manifest.state_uploaded!
    status = manifest.schedule_commit(target_branch, commit_message, !direct_edit?)

    render "repository_uploads/processing", locals: {
      status: status,
      redirect_url: redirect_url(manifest),
    }
  end

  def destroy
    file = UploadManifestFile.where(id: params[:file_id], repository_id: current_repository.id).first
    return head 404 unless file
    return head 404 unless file.repository.pushable_by?(current_user)
    return head 404 unless file.uploader == current_user
    return head 400 unless file.manifest.state_new?
    file.cleanup!
    file.destroy
    head 200
  end

  private

  def target_branch
    params[:target_branch] || ""
  end

  def redirect_url(manifest)
    if direct_edit?
      tree_path("", target_branch, current_repository)
    else
      base = params[:quick_pull]
      range = [base, manifest.branch].compact.join("...")
      compare_path(current_repository, range, quick_pull: 1)
    end
  end

  def direct_edit?
    params["commit-choice"] == "direct"
  end

  def commit_message
    message =
      if params[:message].present?
        params[:message]
      else
        "Add files via upload"
      end
    [message, params[:description]].join("\n\n")
  end

  def ensure_valid_branch_name
    unless current_branch_or_tag_name.present?
      flash[:error] = "Select a branch to upload files"
      redirect_to current_repository
    end
  end

  def route_supports_advisory_workspaces?
    true
  end
end
