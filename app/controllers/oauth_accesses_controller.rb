# frozen_string_literal: true

class OauthAccessesController < ApplicationController
  areas_of_responsibility :platform

  # The following actions do not require conditional access checks:
  # - show: redirects to another oauth endpoint and does not access
  #   protected organization resources before redirecting.
  skip_before_action :perform_conditional_access_checks, only: "show"

  before_action :login_required

  def show
    redirect_to settings_oauth_authorization_path(find_access.application.key)
  end

  private
  def find_access
    current_user.oauth_accesses.find(params[:id])
  end
end
