# frozen_string_literal: true

class AvatarsController < ApplicationController
  include AvatarHelper

  areas_of_responsibility :avatars

  # The following actions do not require conditional access checks:
  # - show: serves `/settings/avatars/:id` and shows a user/orgs avatar,
  #   which is public information.
  skip_before_action :perform_conditional_access_checks, only: :show

  before_action :login_required
  before_action :find_avatar, only: [:show, :update]
  before_action :highlight_settings_sidebar, only: [:show]

  def show
    @primary_avatar = @avatar_owner.primary_avatar

    respond_to do |format|
      format.html do
        render "avatars/show", layout: false
      end
    end
  end

  def update
    if params[:op] == "destroy"
      flash[:notice] = "This #{human_avatar_name(@avatar)} has been deleted."
      @avatar.destroy
    else
      save_avatar_coordinates(@avatar)
      PrimaryAvatar.set!(@avatar, current_user, handle_previous_avatar: true)
      increment_avatar_stat
      flash[:notice] = "Your #{human_avatar_name(@avatar)} has been updated.  It may take a few moments to update across the site."
    end

    redirect_to settings_url_for(@avatar_owner)
  end

  private

  def target_for_conditional_access
    avatar = Avatar.find_by_id(params[:id])
    return :no_target_for_conditional_access unless avatar

    owner = avatar.owner

    # TEMPORARY SECURITY BYPASS: see https://github.com/github/github/pull/149465#issuecomment-658748803
    return :no_target_for_conditional_access if owner.instance_of?(Team)

    return owner.creator if owner.is_a?(NonMarketplaceListing)
    return owner.owner if owner.respond_to?(:owner)
    owner
  end

  def save_avatar_coordinates(avatar)
    return unless AVATAR_COORDINATES.all? { |attr| params[attr].present? }
    AVATAR_COORDINATES.each do |attr|
      avatar.send("#{attr}=", params[attr])
    end
    avatar.save!
  end

  AVATAR_COORDINATES = [:cropped_x, :cropped_y, :cropped_width, :cropped_height]

  def find_avatar_owner
    if login = params[:organization]
      @avatar_owner = Organization.find_by_login(login.to_s)
      if !(@avatar_owner && @avatar_owner.avatar_editable_by?(current_user))
        redirect_to "/settings/profile"
        return
      end
    else
      @avatar_owner = current_user
    end
  end

  # When destroying the avatar AssetUploadable#dereference_asset archives the asset
  # in after_destroy. This is considered transitively safe because of the Abilities
  # editable_by? check with current_user.
  def find_avatar
    @avatar = Avatar.find_by_id(params[:id].to_i)
    if !(@avatar && @avatar.editable_by?(current_user))
      redirect_to "/settings/profile"
      return
    end
    @avatar_owner = @avatar.owner
  end

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def current_organization
    @current_organization ||= @avatar_owner && @avatar_owner.organization? && @avatar_owner
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  helper_method :current_organization

  def settings_url_for(owner)
    case owner
    when Organization
      "/organizations/#{owner}/settings/profile"
    when User
      "/settings/profile"
    when OauthApplication
      oauth_app_redirect_path(owner)
    when Integration
      gh_settings_app_path(owner)
    when Marketplace::Listing
      edit_description_marketplace_listing_path(owner)
    when NonMarketplaceListing
      if logged_in? && current_user.can_admin_non_marketplace_listings?
        biztools_edit_works_with_github_listing_path(owner.global_relay_id)
      else
        edit_works_with_github_listing_path(owner.global_relay_id)
      end
    when Team
      edit_team_path(org: owner.organization, team_slug: owner.slug)
    when Business
      settings_profile_enterprise_path(owner)
    else
      raise ArgumentError, "Unknown settings url for avatar owner #{owner.class}"
    end
  end

  def oauth_app_redirect_path(owner)
    if owner.user.organization?
      settings_org_application_path(owner.user, owner)
    else
      settings_user_application_path(owner)
    end
  end

  def highlight_settings_sidebar
    @selected_link = :avatar_settings
  end

  def increment_avatar_stat
    # TODO - remove this stat once the conditional dogstats are populated
    GitHub.dogstats.increment("avatars.created", tags: ["type:#{@avatar.owner_type.to_s.underscore}"])
    case @avatar.owner_type
    when "OauthApplication"
      GitHub.dogstats.increment("avatar", tags: ["action:create", "via:oauth-application", "type:#{@avatar.owner_type.to_s.underscore}"])
    when "Integration"
      GitHub.dogstats.increment("avatar", tags: ["action:create", "via:integration", "type:#{@avatar.owner_type.to_s.underscore}"])
    end
  end
end
