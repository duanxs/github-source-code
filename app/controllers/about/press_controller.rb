# frozen_string_literal: true

class About::PressController < ApplicationController
  areas_of_responsibility :press_feed

  include PressFeedControllerMethods

  skip_before_action :perform_conditional_access_checks

  layout "site"

  def index
    render "about/press/index"
  end
end
