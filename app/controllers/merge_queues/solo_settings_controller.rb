# frozen_string_literal: true

class MergeQueues::SoloSettingsController < AbstractRepositoryController
  before_action :merge_queue_required
  before_action :entry_adminable_by_current_user_required

  def create
    entry.force_solo!
    flash[:notice] = "Successfully forced solo merge for #{entry.pull_request.title}"
    redirect_to :back
  end

  private

  def merge_queue_required
    return render_404 unless merge_queue_enabled?
    return render_404 unless merge_queue
  end

  def entry_adminable_by_current_user_required
    render_404 unless entry.adminable_by?(current_user)
  end

  def merge_queue
    current_repository.default_merge_queue
  end

  def entry
    merge_queue.entries.includes(pull_request: [:issue]).find(params[:entry_id])
  end
end
