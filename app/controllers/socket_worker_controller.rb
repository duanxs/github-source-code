# frozen_string_literal: true

# A stateless controller to serve a SharedWorker bootstrap script.
#
# https://developer.mozilla.org/en-US/docs/Web/API/SharedWorker
#
# The SharedWorker's script must be served from the first-party origin,
# obeying the browser's same-origin policy. So serve a script from the
# github.com origin that imports the worker file from our CDN's origin.
class SocketWorkerController < ApplicationController
  def show
    bundles = AssetBundles.get
    url = bundles.bundle_url("socket-worker.js")
    script = "importScripts('%s')" % url
    render js: script
  end

  def stateless_request?
    true
  end

  private

  def require_conditional_access_checks?
    false
  end
end
