# frozen_string_literal: true

# this is a stubbed implementation of a recovery provider for the Delegated
# Account Recovery flow.
#
# When we act as a recovery provider (TBD) we can make this available to genpop.
#
# For now, it's only purpose is to pretend to be a separate provider for dev/test.
class RecoveryProviderController < ApplicationController

  if GitHub.github_as_recovery_provider_enabled?
    skip_before_action :verify_authenticity_token

    def save_token
      binary_token = Base64.strict_decode64(params[:token])
      account_provider = Darrrr::RecoveryToken.account_provider_issuer(binary_token)

      begin
        Darrrr.this_recovery_provider.validate_recovery_token!(binary_token)
        session[:delegated_account_recovery_test_token] = params[:token]
        redirect_to "#{account_provider.save_token_return}?#{{state: params[:state], status: "save-success"}.to_query}"
      rescue Darrrr::RecoveryTokenError => e
        flash[:error] = "Could not validate recovery token: " + e.message
        if account_provider
          redirect_to "#{account_provider.save_token_return}?#{{state: params[:state], status: "save-failure"}.to_query}"
        else
          # can't figure out where to go? go back!
          redirect_to :back
        end
      end
    end

    # We'll just take the token as a raw value and countersign it until we act
    # as a recovery provider. In this case, GitHub is both the Account and
    # Recovery provider.
    def recover_account
      # 1. Authenticate the user. The exact nature of how the Recovery Provider
      # authenticates the user is beyond the scope of this specification.
      # handled by before filter

      # 2. Select a token to exercise, based on some combination of user choice and input parameters.
      token = Base64.strict_decode64(session[:delegated_account_recovery_test_token]) # For development only

      # 3. Retrieve the configuration for the token issuer as described in Section 2.
      # 4. Create a counter-signed token as described in Section 4 and sign the token according to the algorithm's requirement.
      account_provider = Darrrr::RecoveryToken.account_provider_issuer(token)
      countersigned_recovery_token = Darrrr.this_recovery_provider.countersign_token(token)

      # 5. Redirect the user agent to the "recover-account-return" endpoint defined
      # by the Account Provider configuration. The "application/x-www-form-urlencoded"
      # formatted POST body should include the parameter "token" set to
      # the countersigned token.
      render "recovery_provider/auto_submit", locals: {
        token: countersigned_recovery_token,
        recover_account_return_endpoint: account_provider.recover_account_return,
      }

      # 6. The Recovery Provider should notify the user, via an out-of-band means,
      # that a recovery token has been exercised, for which domain and nickname, if applicable.
      # TODO implement when we become an actual recovery provider
    end

    private

    def require_conditional_access_checks?
      false
    end
  end
end
