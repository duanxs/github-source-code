# frozen_string_literal: true
class RepositoryImportsController < RepositoryImports::BaseController
  areas_of_responsibility :migration

  skip_before_action :ensure_visible_to_user, only: %w(new create)
  skip_before_action :authorization_required, only: %w(new create)
  before_action :login_required, only: :create
  before_action :login_required_with_redirect, only: :new
  before_action :content_authorization_required, only: %w(new create)
  before_action :track_repository_import_attempted, only: :create

  # Public: New repository and import form.
  def new
    @owner = current_organization || current_user
    @repository = Repository.new(private: @owner.organization? && @owner.can_add_private_repo?)
    @initial_url = params[:import_url]
    render "repository_imports/new"
  end

  # Public: Create repository and start import from source repository.
  def create
    unless params.key?(:repository)
      flash[:error] = "Whoops! Something went wrong. Please try again."
      return redirect_to new_repository_import_path
    end

    parameters = repository_params.to_h.merge auto_init: false

    requested_owner = User.find_by_login(params[:owner])

    return render_404 unless requested_owner

    unless requested_owner.can_create_repository?(current_user)
      flash[:error] = "Unable to create repository for #{requested_owner.login}."
      return redirect_to new_repository_import_path
    end

    result = Repository.handle_creation(
      current_user,
      requested_owner.login,
      false,
      parameters,
    )

    if result.success?
      repo = result.repository
      repository_import = RepositoryImport.new(
        repository: repo,
        user: current_user,
      )

      begin
        repository_import.start_import(vcs_url: params[:vcs_url].strip)
      rescue Porter::ApiClient::Error
        flash[:error] = "We can't import from #{params[:vcs_url]}. Please check the URL and try again."
      end

      return redirect_to repository_import_path(repository_import.url_params)
    else
      flash[:error] = result.error_message.to_s
      return redirect_to new_repository_import_path
    end
  end

  # Public: Show the current state of the import.
  def show
    if repository_import.no_importer?
      if repository_import.repository.empty?
        @owner = current_organization || current_user
        @repository = current_repository
        render_template_view "repository_imports/new", *show_view_attributes
      else
        redirect_to repository_path(repository_import.repository)
      end
    elsif repository_import.status?(:none)
      @owner = current_organization || current_user
      @repository = current_repository

      flash_message = "No source repositories were detected at #{repository_import.import_status_hash["vcs_url"]}. Please check the URL and try again."
      render_template_view "repository_imports/new", *show_view_attributes(flash_message: flash_message)

    elsif repository_import.status?(:detecting)
      render_template_view "repository_imports/states/detecting", *show_view_attributes

    elsif repository_import.status?(:auth, :auth_failed)
      render_template_view "repository_imports/states/auth", *show_view_attributes

    elsif repository_import.status?(:choose)
      render_template_view "repository_imports/states/projects", *show_view_attributes

    elsif repository_import.status?(:importing)
      render_template_view "repository_imports/states/importing", *show_view_attributes

    elsif repository_import.status?(:mapping)
      render_template_view "repository_imports/states/mapping", *show_view_attributes

    elsif repository_import.lfs_opt_needed?
      redirect_to repository_import_large_files_path(repository_import.url_params)

    elsif repository_import.status?(:pushing)
      render_template_view "repository_imports/states/pushing", *show_view_attributes

    elsif repository_import.status?(:complete)
      render_template_view "repository_imports/states/complete", *show_view_attributes

    elsif repository_import.status?(:error)
      if repository_import.failed_step == "detection"
        @owner = current_organization || current_user
        @repository = current_repository

        flash_message = "There was an error detecting the repository at #{repository_import.import_status_hash["vcs_url"]}. Please check the URL and try again."
        render_template_view "repository_imports/new", *show_view_attributes(flash_message: flash_message)
      else
        render_template_view "repository_imports/states/error", *show_view_attributes
      end
    else
      redirect_to repository_path(repository_import.repository)
    end
  end

  # Public: Destroy an existing import and start it again with a new vcs_url or
  # simply restart the import if no vcs_url is provided.
  def update
    if params[:vcs_url]
      begin
        repository_import.stop_import
        repository_import.start_import(vcs_url: params[:vcs_url])
      rescue Porter::ApiClient::Error
        flash[:error] = "We can't import from #{params[:vcs_url]}. Please check the URL and try again."
      end
    else
      # Restart the import.
      repository_import.restart_import
    end

    redirect_to repository_import_path(repository_import.url_params)
  end

  def destroy
    # Stop running import.
    repository_import.repository.porter_stopped!
    repository_import.stop_import

    # Redirect to repository
    redirect_to repository_path(repository_import.repository)
  end

  private

  def repository_params
    return ActionController::Parameters.new unless params.key?(:repository)
    params.require(:repository).permit %i[name description public visibility]
  end

  def login_required_with_redirect
    redirect_to_login(request.url) unless logged_in?
  end

  def content_authorization_required
    authorize_content(:repo)
  end

  def show_view_attributes(flash_message = {})
    [RepositoryImports::ShowView,
      {
        repository_import: repository_import,
        start_percent: params[:percent],
        start_status: params[:status].to_s,
        layout: !request.xhr?,
      }.merge(flash_message)]
  end
end
