# frozen_string_literal: true

class Orgs::InstallationsController < Orgs::Controller
  areas_of_responsibility :platform

  include IntegrationInstallationsControllerMethods

  before_action :organization_admin_required
  before_action :ensure_trade_restrictions_allows_org_settings_access

  javascript_bundle :settings

  private

  def current_context
    this_organization
  end
end
