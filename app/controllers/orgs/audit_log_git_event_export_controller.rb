# frozen_string_literal: true

class Orgs::AuditLogGitEventExportController < Orgs::Controller
  include AuditLogExportHelper

  areas_of_responsibility :orgs, :audit_log

  statsd_tag_actions only: :create

  before_action :organization_admin_required, -> { audit_log_git_event_export_required(this_organization) }

  def show
    export = this_organization.audit_log_git_event_exports.find_by!(actor_id: current_user.id, token: params[:token])
    render_audit_log_export(export)
  end

  def create
    options = {
      actor: current_user,
      start: parse_user_time(params[:start]),
      end: parse_user_time(params[:end]),
    }

    export = this_organization.audit_log_git_event_exports.create(options)
    url = org_audit_log_git_event_export_url(token: export.token)

    respond_with_audit_log_export(export: export, export_url: url)
  end

  def parse_user_time(param)
     zone = current_user&.time_zone || Time.zone
     zone.parse(param)
  end
end
