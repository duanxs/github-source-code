# frozen_string_literal: true

class Orgs::IntegrationTransfersController < Orgs::Controller
  areas_of_responsibility :platform

  include IntegrationTransfersControllerMethods

  before_action :organization_admin_required, except: [:destroy]

  private

  def current_context
    this_organization
  end
end
