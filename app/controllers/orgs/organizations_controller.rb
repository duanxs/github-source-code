# frozen_string_literal: true

class Orgs::OrganizationsController < Orgs::Controller
  areas_of_responsibility :orgs

  # The following actions do not require conditional access checks:
  # - search: serves `/orgs/:org/search`, a simple redirect action that
  #   handles enforcement on the receiving end of the redirection.
  skip_before_action :perform_conditional_access_checks, only: %w(
    search
  )

  before_action :organization_admin_required, except: [:search, :top_languages]
  before_action :sudo_filter, except: [:search, :top_languages]

  def application_access
    if restrict_access = params[:restrict_access].presence
      if restrict_access == "on"
        this_organization.enable_oauth_application_restrictions
      else
        this_organization.disable_oauth_application_restrictions
      end
    end

    if request.xhr?
      head :ok
    else
      redirect_to settings_org_oauth_application_policy_path(this_organization)
    end
  end

  def search
    search_params = { q: "org:#{this_organization.login} #{params[:q]}" }
    search_params[:unscoped_q] = params[:unscoped_q] if params[:unscoped_q]

    redirect_to search_path(search_params)
  end

  def top_languages
    respond_to do |format|
      format.html do
        render_partial_view "orgs/organizations/top_languages",
          Orgs::Repositories::IndexPageView,
          organization: this_organization,
          current_page: current_page,
          type_filter: params[:type],
          phrase: params[:q],
          language: params[:language]
      end
    end
  end
end
