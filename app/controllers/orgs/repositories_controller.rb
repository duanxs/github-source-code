# frozen_string_literal: true

class Orgs::RepositoriesController < Orgs::Controller
  areas_of_responsibility :orgs

  # The following actions do not require conditional access checks:
  # - index: serves `/orgs/:org`, a simple redirect action that
  #   handles enforcement on the receiving end of the redirection.
  skip_before_action :perform_conditional_access_checks, only: %w(index)

  def index
    redirect_to "/#{this_organization.login}"
  end
end
