# frozen_string_literal: true

class Orgs::PackagesController < Orgs::Controller
  before_action :check_packages_availability

  include Registry::QueryHelper

  statsd_tag_actions only: :index

  PAGE_SIZE = 30 # 10 rows of 3 columns, 15 rows of 2 columns, 30 rows of 1 column

  def index
    respond_to do |format|
      format.html do
        if request.xhr? || pjax?
          render partial: "registry/packages/filtered_packages", locals: {
            owner: this_organization,
            packages: packages,
            params: params
          }
        else
          render_template_view("orgs/packages/index", Orgs::Packages::IndexView, {
            organization: this_organization,
            phrase: query
          }, locals: { packages: packages })
        end
      end
    end
  end

  private

  def query
    raw_query = params[:q]
    raw_query.strip if raw_query.is_a?(String)
  end

  def packages
    results, packages = packages_for_query(
      current_user: current_user,
      user_session: user_session,
      owner: this_organization,
      query: query,
      package_type: ecosystem_param,
      visibility: visibility_param,
      sort: sort_param,
      page: current_page,
      per_page: PAGE_SIZE,
    )

    WillPaginate::Collection.create(current_page, PAGE_SIZE) do |pager|
      pager.replace(packages)
      pager.total_entries ||= results.total_entries
    end
  end

  def check_packages_availability
    render_404 unless PackageRegistryHelper.show_packages?
  end
end
