# frozen_string_literal: true

class Orgs::TeamRepositoriesController < Orgs::Controller
  areas_of_responsibility :orgs

  statsd_tag_actions only: :index

  layout "team"

  before_action :login_required
  before_action :this_team_required
  before_action :admin_on_team_required, except: [:index, :update]
  before_action :sudo_filter, only: [:create]
  before_action :require_valid_new_permission, only: [:update]
  before_action :require_xhr, only: [:accessible_to_members]

  IndexQuery = parse_query <<-'GRAPHQL'
    query($login: String!, $slug: String!, $first: Int, $last: Int, $before: String, $after: String, $query: String) {
      organization(login: $login) {
        ...Views::Layouts::Team::Organization
        ...Views::Orgs::TeamRepositories::Index::Organization
        team(slug: $slug) {
          ...Views::Orgs::TeamRepositories::Index::Team
        }
      }
    }
  GRAPHQL

  SearchQuery = parse_query <<-'GRAPHQL'
    query($login: String!, $slug: String!, $first: Int, $last: Int, $before: String, $after: String, $query: String) {
      organization(login: $login) {
        team(slug: $slug) {
          ...Views::Orgs::TeamRepositories::List::Team
        }
      }
    }
  GRAPHQL

  REPOSITORY_PAGE_SIZE = 30

  def index
    variables = graphql_pagination_params(page_size: REPOSITORY_PAGE_SIZE)
    variables["login"]             = params[:org] || params[:organization_id]
    variables["slug"]              = params[:team_slug]
    variables["query"]             = params[:query] if params[:query]

    override_analytics_location "/orgs/<org-login>/teams/<team-name>/repositories"
    respond_to do |format|
      format.html do
        response.headers["Vary"] = "X-Requested-With"
        if request.xhr?
          graphql_data = platform_execute SearchQuery, variables: variables
          return render_404 unless graphql_data.organization&.team
          render partial: "orgs/team_repositories/list", locals: { team: graphql_data.organization.team }
        else
          graphql_data = platform_execute IndexQuery, variables: variables
          return render_404 unless graphql_data.organization&.team
          render(
            "orgs/team_repositories/index",
            locals: {
              graphql_org: graphql_data.organization,
              graphql_team: graphql_data.organization.team,
              selected_nav_item: :repositories,
            })
        end
      end
    end
  end

  def suggestions
    respond_to do |format|
      format.html_fragment do
        render_partial_view "orgs/team_repositories/suggestions", Orgs::TeamRepositories::SuggestionsView,
          team: this_team,
          query: params[:q]
      end
    end
  end

  def create
    repo = this_organization.org_repositories.with_name_with_owner(params[:member])

    if repo.nil?
      status = :not_found
    elsif !repo.adminable_by?(current_user)
      status = :unauthorized
    elsif !this_team.has_repository?(repo)
      added_status = this_team.add_repository(repo, :pull).status
      status = :created if added_status == :success
    end

    respond_to do |format|
      format.html do
        case status
        when :not_found
          flash[:error] = "The #{params[:member]} repository was not found"
        when :unauthorized
          flash[:error] = "You do not have sufficient permissions to add the #{repo.name} repository to this team"
        when :created
          flash[:notice] = "The #{repo.name} repository was added to this team"
        end
        redirect_to team_repositories_path(this_team)
      end
    end
  end

  def update
    return render_404 unless this_organization.direct_member?(current_user)

    repo = this_organization.repositories.find_by_id(params[:repository_id])

    # This endpoint is also used by the settings page of user-owned repositories
    # forked from an organization. In order to support user-owned repositories,
    # we try to find the repository via the current user before we fail.
    #
    # See https://github.com/github/github/issues/83808
    unless repo
      found_repo = current_user.repositories.find_by_id(params[:repository_id])
      if found_repo && found_repo.network_owner == this_organization
        repo = found_repo
      end
    end

    # if it fails to find the repo, it could be that the repo is a forked repo, and the current user is not the owner but actually an org admin.
    # NOTE: the adminable_by (below the next block) will ensure that the current_user is indeed an admin
    unless repo
      repo = Repository.where(id: params[:repository_id], organization_id: this_organization.id).where.not(parent_id: nil).first
    end

    return render_404 unless repo && repo.adminable_by?(current_user)

    if request.xhr?
      if this_team.has_repository?(repo)
        this_team.update_repository_permission(repo, params[:permission])
      else
        this_team.add_repository(repo, params[:permission])
      end

      render json: { members_with_higher_access: this_team.members_with_higher_access_to?(repo) }
    else
      inherited_ability = this_team.most_capable_inherited_ability_for_repo(repo)
      inherited_ability_level = Ability.actions[inherited_ability&.action]
      new_ability_level = Ability.actions[new_permission]

      # we clear direct permissions if there is an equal or greater inherited permission
      success = if inherited_ability && inherited_ability_level >= new_ability_level
        this_team.remove_repository_directly(repo) # Revokes direct ability downstream.
        true # Ability#revoke doesn't return status, so default to return true
      elsif this_team.has_repository?(repo)
        this_team.update_repository_permission(repo, params[:permission]).success?
      else
        this_team.add_repository(repo, params[:permission]).success?
      end

      if !success
        flash[:error] = "There was an error updating this team's permissions"
      elsif this_team.descendants?
        flash[:notice] = "#{this_team.name} and it's child teams now have #{new_permission} permissions to #{repo.name_with_owner}"
      else
        flash[:notice] = "#{this_team.name} now has #{new_permission} permissions to #{repo.name_with_owner}"
      end

      redirect_to team_repositories_path(this_team)
    end
  end

  def bulk_remove
    repository_ids = params[:repository_ids]

    repos = this_team.repositories.with_ids(repository_ids)
    repos.each do |repository|
      this_team.remove_repository(repository)
    end

    flash[:notice] = "You’ve removed '#{repos.map(&:name_with_owner).to_sentence}' from this team."
    redirect_to team_repositories_path(this_team)
  end

  AccessibleToMembersQuery = parse_query <<-'GRAPHQL'
    query($login: String!, $slug: String!, $first: Int!) {
      organization(login: $login) {
        team(slug: $slug) {
          ...Orgs::TeamRepositories::AccessibleToMembersView::TeamRepositoriesFragment
          viewerCanAdminister
        }
      }
    }
  GRAPHQL

  ACCESSIBLE_REPOS_LIMIT = 25

  def accessible_to_members
    graphql_data = platform_execute(
      AccessibleToMembersQuery,
      variables: { login: params[:org], slug: params[:team_slug], first: ACCESSIBLE_REPOS_LIMIT },
    )

    return render_404 unless graphql_org = graphql_data.organization
    return render_404 unless graphql_team = graphql_data.organization.team
    return render_404 unless graphql_team.viewer_can_administer

    respond_to do |format|
      format.html do
        render_partial_view "orgs/team_repositories/accessible_to_members", Orgs::TeamRepositories::AccessibleToMembersView,
          team: graphql_team,
          member_name: params[:member],
          action_type: params[:action_type],
          return_to: params[:return_to]
      end
    end
  end

  private

  def require_valid_new_permission
    return if new_permission
    flash[:error] = "A valid permission is required"
    redirect_to team_repositories_path(this_team)
  end

  def new_permission
    repo = this_organization.repositories.find_by_id(params[:repository_id])
    @new_permission ||=
      if Role.org_repo_custom_role?(name: params[:permission], repo: repo)
        params[:permission]
      else
        Repository.permission_to_action(params[:permission]).to_sym
      end
  rescue ArgumentError
    nil
  end
end
