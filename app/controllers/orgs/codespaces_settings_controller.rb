# frozen_string_literal: true

class Orgs::CodespacesSettingsController < Orgs::Controller

  before_action :org_admins_only
  before_action :check_feature_enabled

  include OrganizationsHelper
  include ::Codespaces::OrganizationsDependency

  def index
    render "settings/organization/codespaces/index", locals: { users: members_with_access, max_users: MAX_USERS }
  end

  def toggle_feature
    enabled = ActiveModel::Type::Boolean.new.cast(params[:organization][:organization_codespaces_allowed])
    if enabled
      current_organization.allow_organization_codespaces(actor: current_user)
      current_organization.accept_organization_codespaces_terms(actor: current_user)
    else
      current_organization.disallow_organization_codespaces(actor: current_user)
    end

    respond_to do |format|
      format.html_fragment do
        render partial: "settings/organization/codespaces/settings", locals: { users: members_with_access, max_users: MAX_USERS, flash_error: flash_error }, formats: :html
      end
      format.html do
        redirect_to action: "index"
      end
    end
  end

  def grant_access
    identifier = (params[:identifier] ||= "").strip
    user = current_organization.members.find_by(login: identifier)

    current_members = members_with_access
    status = :ok

    # 'app/assets/modules/github/orgs/codespaces.ts' only renders response HTML to the DOM for a set of known status codes.
    # If you add a new 'status' value to this controller action action, add it to the 'knownServerStatusCodes' array in 'codespaces.ts' too.
    if user.nil?
      flash[:error] = "User not found in this organization."
      status = :bad_request
    elsif current_members.any? { |u| u.login.casecmp?(identifier) }
      flash[:error] = "User has already been added to the beta."
      status = :conflict
    elsif current_members.count >= MAX_USERS
      flash[:error] = "You have already reached the maximum of #{MAX_USERS} users in the Codespaces beta."
      status = :bad_request
    else
      result = Codespaces::Policy.grant_codespace_org_creator!(user, current_organization)
      unless !result.nil? && result.success?
        flash[:error] = "An error occurred when trying to grant access, please try again later."
        status = :internal_server_error
      end
    end

    respond_to do |format|
      format.html_fragment do
        render partial: "settings/organization/codespaces/settings", status: status, locals: { users: members_with_access, max_users: MAX_USERS, flash_error: flash_error }, formats: :html
      end
      format.html do
        redirect_to action: "index"
      end
    end
  end

  def revoke_access
    identifier = (params[:identifier] ||= "").strip

    user = User.find_by(login: identifier)
    status = :ok

    if user.nil?
      flash[:error] = "An error occurred when trying to find this user."
      status = :bad_request
    else
      result = Codespaces::Policy.revoke_codespace_org_creator!(user, current_organization)
      unless !result.nil? && result.success?
        flash[:error] = "An error occurred when trying to revoke access, please try again later."
        status = :bad_request
      end
    end

    respond_to do |format|
      format.html_fragment do
        render partial: "settings/organization/codespaces/settings", status: status, locals: { users: members_with_access, max_users: MAX_USERS, flash_error: flash_error }, formats: :html
      end
      format.html do
        redirect_to action: "index"
      end
    end
  end

  def suggestions
    headers["Cache-Control"] = "no-cache, no-store"
    respond_to do |format|
      format.html_fragment do
        render_partial_view "settings/organization/codespaces/suggestions", Orgs::Codespaces::SuggestionsView, {
          org_members_only: true,
          organization: current_organization,
          query: params[:q],
          member_logins_with_access: members_with_access.pluck(:login)
        }
      end
    end
  end

  private

  def check_feature_enabled
    if !GitHub.flipper[:workspaces].enabled?(current_organization)
      render_404
    end
  end

  # When requests originate from JavaScript, JavaScript displays the flash message.
  # Clear `flash[:error]` so Rails doesn’t _also_ display it.
  def flash_error
    error_message = flash[:error]
    flash[:error] = nil
    error_message
  end
end
