# frozen_string_literal: true

class Orgs::Permissions::ManageIntegrationsController < Orgs::Controller
  areas_of_responsibility :platform

  include OrganizationsHelper

  # TODO: make this part of the Permissions API check and send this to the
  # authorizer
  before_action :organization_admin_required
  before_action :sudo_filter, only: [:grant, :revoke]

  def suggestions
    respond_to do |format|
      format.html_fragment do
        render_partial_view "orgs/permissions/manage_integrations_suggestions", ::Settings::Organization::ManageIntegrationsView,
          organization: this_organization,
          query: params[:q]
      end
    end
  end

  def grant
    # TODO: Do this lookup in the Permissions::Authorizer
    # Don't worry about scoping this to the organization here; the Permissions service can handle that.
    potential_member = User.find_by_login(login_param)
    unless potential_member
      flash[:error] = "Can't grant permission to that person"
      return redirect_to :back
    end

    # check whether the actor has the permission to grant this action for this
    # subject
    decision = ::Permissions::Enforcer.authorize(
      action: :grant_manage_organization_apps,
      actor: current_user,
      subject: this_organization,
    )

    if !decision.allow?
      flash[:error] = result.reason
      return redirect_to :back
    end

    # check whether the action can be granted for this subject
    decision = ::Permissions::Enforcer.authorize(
      action: :grantable_manage_organization_apps,
      actor: potential_member,
      subject: this_organization,
    )
    if !decision.allow?
      flash[:error] = result.reason
      return redirect_to :back
    end

    # Actually grant the permission
    result = ::Permissions::Granter.grant(
      action: :manage_all_apps,
      actor_id: potential_member.id,
      subject_id: this_organization.id,
    )
    if result.failure?
      flash[:error] = result.reason
    elsif result.pending?
      flash[:warn] = "Granting #{potential_member} permission to manage GitHub Apps owned by this organization"
    elsif result.success?
      flash[:notice] = "Granted #{potential_member} permission to manage GitHub Apps owned by this organization"
    end

    unless result.failure?
      this_organization.instrument_github_app_manager(potential_member, action: :grant)
    end

    redirect_to :back
  end

  def revoke
    # TODO: Do this lookup in the Permissions::Authorizer
    # Don't worry about scoping this to the organization here; the Permissions service can handle that.
    potential_member = User.find_by_login(login_param)
    unless potential_member
      flash[:error] = "Can't revoke permission from that person"
      return redirect_to :back
    end

    # If the current_user can `grant_manage_organization_apps` permission, then
    # they can also revoke that permission.
    decision = ::Permissions::Enforcer.authorize(
      action: :grant_manage_organization_apps,
      actor: current_user,
      subject: this_organization,
    )

    if !decision.allow?
      flash[:error] = result.reason
      return redirect_to :back
    end

    result = ::Permissions::Granter.revoke(
      actor_id: potential_member.id,
      action: :manage_all_apps,
      subject_id: this_organization.id,
    )

    if result.failure?
      flash[:error] = result.reason
    elsif result.pending?
      flash[:warn] = "Revoking #{potential_member}'s permission to manage GitHub Apps owned by this organization"
    elsif result.success?
      flash[:notice] = "Revoked #{potential_member}'s permission to manage GitHub Apps owned by this organization"
    end

    unless result.failure?
      this_organization.instrument_github_app_manager(potential_member, action: :revoke)
    end

    redirect_to :back
  end

  private

  def login_param
    params.require(:user_login)
  end
end
