# frozen_string_literal: true

class Orgs::IdentityManagementController < Orgs::Controller
  areas_of_responsibility :orgs

  statsd_tag_actions only: :sso_sign_up

  before_action :dotcom_required
  # ordering of these three before_actions is important.
  # :redirect_to_business_saml_sso needs to come first
  before_action :redirect_to_business_saml_sso, only: [:sso]
  before_action :business_saml_sso_prohibited, except: [:sso]
  before_action :sso_provider_required

  # The following actions do not require conditional access checks:
  # - sso: serves `/orgs/:org/sso`, serves as the prompt to SSO and create the
  #   require external identity session for protected endpoints.
  # - sso_sign_up: serves `/orgs/:org/sso/signup`, same as above but for
  #   users that need to sign up first.
  skip_before_action :perform_conditional_access_checks, only: %w(
    sso
    sso_sign_up
    sso_status
    sso_modal
    sso_complete
  )

  def sso
    allow_external_redirect_after_post(provider: this_organization.saml_provider)

    url = org_idm_saml_initiate_url(this_organization,
      authorization_request: params[:authorization_request],
      return_to: params[:return_to],
    )

    render_template_view "orgs/identity_management/sso",
      Orgs::IdentityManagement::SingleSignOnView,
      view_attrs = {
        organization: this_organization,
        initiate_sso_url: url,
        credential_authorization_request: credential_authorization_request,
      },
      layout: "session_authentication"
  end

  def sso_sign_up
    unless session[:unlinked_external_identity_id]
      redirect_to org_idm_sso_path(this_organization)
      return
    end

    if logged_in?
      unlinked_id = session.delete(:unlinked_external_identity_id)
      unlinked = this_organization.saml_provider.external_identities.find(unlinked_id)
      return_to = session.delete(:sso_return_to) || user_path(this_organization)
      sso_invitation_token = session.delete(:sso_invitation_token)

      ActiveRecord::Base.connected_to(role: :writing) do
        result = Platform::Provisioning::OrganizationIdentityProvisioner.provision_and_add_member \
          organization_id: this_organization.id,
          user_id: current_user.id,
          user_data: unlinked.saml_user_data,
          mapper: Platform::Provisioning::SamlMapper,
          sso_invitation_token: sso_invitation_token

        if result.success?
          update_or_create_external_identity_session result.external_identity,
            expires_at: session[:unlinked_session_expires_at]

          flash[:notice] = "Welcome to the #{ this_organization.safe_profile_name } organization."
        elsif result.two_factor_requirement_not_met_error?
          flash[:notice] = "You must enable two factor authentication before joining the #{ this_organization.safe_profile_name } organization."
          # Restore the previous state so it can be resumed after the 2FA flow.
          session[:sso_return_to] = return_to
          session[:sso_invitation_token] = sso_invitation_token if sso_invitation_token
          session[:return_to] = org_idm_sso_sign_up_path(this_organization)
          session[:unlinked_external_identity_id] = unlinked_id if unlinked_id
          return redirect_to settings_user_two_factor_authentication_intro_path
        else
          flash[:error] = "There was an issue joining the organization: #{ result.errors.full_messages.to_sentence }"
        end
      end

      redirect_to_return_to(fallback: return_to)
    else
      render_template_view "orgs/identity_management/sign_up_via_sso",
        Orgs::IdentityManagement::SingleSignOnView,
        view_attrs = { organization: this_organization },
        layout: "session_authentication"
    end
  end

  # Action: Renders a partial prompting the user to renew their single sign-on
  # session. This will be shown to the user via a dialog when attempting an
  # XHR action with an expired SSO session.
  def sso_modal
    url = org_idm_saml_initiate_url(this_organization, return_to: org_idm_sso_complete_path(this_organization))

    render_template_view "orgs/identity_management/sso_modal",
      Orgs::IdentityManagement::SingleSignOnView,
      view_attrs = {
        organization: this_organization,
        initiate_sso_url: url,
      },
      layout: false
  end

  # Action: The landing page users are redirected to after completing
  # SSO initiated from a client-side modal.
  #
  # Since SSO takes place in a new window, this page is responsible for
  # communicating status with the original window and closing itself.
  #
  # If communication with original window fails, the window will try to
  # automatically redirect to a fallback URL. If that too fails, the user will
  # be asked to click a link directing them to the fallback URL.
  def sso_complete
    render_template_view "orgs/identity_management/sso_complete",
      Orgs::IdentityManagement::SingleSignOnCompleteView,
      view_attrs = {
        organization: this_organization,
        saml_error: flash[:saml_error],
        fallback_url: user_path(this_organization),
      },
      layout: "session_authentication"
  end

  # Action: Returns a JSON response indicating whether or not the current user
  # has a valid single sign on session for this organization.
  def sso_status
    return render_404 unless request.format.json?

    session_present =
      if params[:fakestate] == "promptssomodal"
        false
      else
        required_external_identity_session_present?(target: this_organization)
      end

    render json: session_present
  end

  private

  def sso_provider_required
    render_404 unless this_organization.saml_sso_enabled?
  end

  def business_saml_sso_prohibited
    render_404 if this_organization.business&.saml_sso_enabled?
  end

  def redirect_to_business_saml_sso
    return unless this_organization.business&.saml_sso_enabled?

    redirect_params = request.query_parameters
    # if there's no return to url already set, return the user to the org landing page
    redirect_params[:return_to] ||= user_url(this_organization)
    redirect_to business_idm_sso_enterprise_path(this_organization.business, redirect_params)
  end

  def credential_authorization_request
    return unless token = params[:authorization_request].presence

    Organization::CredentialAuthorization.consume_request(
      target: this_organization,
      token: token,
      actor: current_user,
    )
  end
end
