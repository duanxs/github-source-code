# frozen_string_literal: true

class Orgs::PolicyOutcomesController < Orgs::Controller
  areas_of_responsibility :policy

  include OrganizationsHelper

  before_action :organization_read_required
  before_action :require_policies_feature_enabled
  rescue_from PolicyService::Client::Error, with: :handle_service_unavailable
  rescue_from PolicyService::Client::NotFoundError, with: :render_404
  statsd_tag_actions only: [:show]

  def show
    response = PolicyService.client.get_outcome(
      organization_id: current_organization.id,
      policy_natural_id: params["policy_natural_id"],
      resource_name: params["resource_name"]
    )
    resource = load_resource(response.outcome)

    return render_404 unless resource.present?

    render_template_view("orgs/policy_outcomes/show", Orgs::PolicyOutcomes::ShowView,
      current_organization: current_organization,
      policy_service_response: response,
      resource: resource,
    )
  end

  private

  def load_resource(outcome)
    resource = case outcome&.resource_type
    when :RESOURCE_REPOSITORY
      current_organization.repositories.find_by_id(outcome.resource_id)
    when :RESOURCE_TEAM
      current_organization.teams.find_by_id(outcome.resource_id)
    when :RESOURCE_ORGANIZATION
      if current_organization.id  == outcome.resource_id
        current_organization
      end
    end

    return resource if resource.present? && resource.readable_by?(current_user)
  end

  def handle_service_unavailable(e)
    Failbot.report(e, app: "policy-service")
    render "orgs/policies/service_unavailable", status: :service_unavailable
  end

  def require_policies_feature_enabled
    render_404 unless current_organization.policies_enabled_for?(current_user)
  end
end
