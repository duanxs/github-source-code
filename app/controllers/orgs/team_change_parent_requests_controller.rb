# frozen_string_literal: true
class Orgs::TeamChangeParentRequestsController < Orgs::Controller
  areas_of_responsibility :orgs

  ApproveRequestMutation = parse_query <<-'GRAPHQL'
    mutation($id: ID!) {
      approvePendingTeamChangeParentRequest(input: {requestId: $id}) {
        request {
          approved
          approvedBy {
            login
          }
        }
      }
    }
  GRAPHQL

  def approve
    results = platform_execute(ApproveRequestMutation, variables: { "id" => params[:id] })
    if results.errors.all.any?
      flash[:error] = results.errors.all.messages.values.flatten.join(", ")
    else
      flash[:notice] = "The request was approved."
    end
    redirect_to :back
  end

  CancelRequestMutation = parse_query <<-'GRAPHQL'
    mutation($id: ID!) {
      cancelPendingTeamChangeParentRequest(input: {requestId: $id}) {
        cancelled
      }
    }
  GRAPHQL

  def cancel
    results = platform_execute(CancelRequestMutation, variables: { "id" => params[:id] })
    if results.errors.all.any?
      flash[:error] = results.errors.all.messages.values.flatten.join(", ")
    elsif results.cancel_pending_team_change_parent_request.cancelled
      flash[:notice] = "The request was cancelled."
    end
    redirect_to :back
  end
end
