# frozen_string_literal: true

class Orgs::PoliciesController < Orgs::Controller
  areas_of_responsibility :policy

  include OrganizationsHelper

  before_action :organization_read_required
  before_action :require_policies_feature_enabled
  rescue_from PolicyService::Client::Error, with: :handle_service_error
  rescue_from PolicyService::Client::NotFoundError, with: :render_404
  statsd_tag_actions only: [:index, :show]

  PAGE_SIZE = 25

  def index
    response = PolicyService.client.get_policies(organization_id: current_organization.id, status_filter: status_filter, resource_type_filter: resource_type_filter)

    render_template_view("orgs/policies/index", Orgs::Policies::IndexView,
      current_organization: current_organization,
      policy_service_response: response,
      status_filter: status_filter,
      resource_type_filter: resource_type_filter,
      query: parsed_query
    )
  end

  def show
    page = params[:page]&.to_i || 1
    filter = validate_filter(params[:filter])
    order_by = params[:order_by] || "alphabetical"

    response = PolicyService.client.get_policy(
      organization_id: current_organization.id,
      policy_natural_id: params["policy_natural_id"],
      limit: PAGE_SIZE,
      page: page,
      filter: filter,
      order_by: order_by
    )

    render_template_view("orgs/policies/show", Orgs::Policies::ShowView,
      current_organization: current_organization,
      policy_service_response: response,
      page: page,
      per_page: PAGE_SIZE,
      filter: filter,
      order_by: order_by,
    )
  end

  def evaluate
    response = PolicyService.client.evaluate_policy(organization_id: current_organization.id, policy_natural_id: params["policy_natural_id"])
    redirect_to org_policy_path(policy_natural_id: params["policy_natural_id"]), notice: "A policy evaluation job has been enqueued."
  rescue PolicyService::Client::NotFoundError => e
    render_404
  rescue PolicyService::Client::Error => e
    Failbot.report(e, app: "policy-service")
    redirect_to org_policy_path(policy_natural_id: params["policy_natural_id"]), flash: { error:  "An error occured while trying to evaluate the policy. Try again later." }
  end

  private

  def resource_type_filter
    filter = "RESOURCE_#{parsed_query.resource}".to_sym.upcase
    ::Policies::V1::ResourceType.resolve(filter)
  end

  def status_filter
    if parsed_query.compliant?
      :POLICY_STATUS_SUCCESS
    elsif parsed_query.noncompliant?
      :POLICY_STATUS_FAILURE
    else
      :POLICY_STATUS_UNKNOWN
    end
  end

  def parsed_query
    @parsed_query ||= Search::Queries::PoliciesQuery.new(params[:query])
  end

  def handle_service_error(e)
    Failbot.report(e, app: "policy-service")
    render "orgs/policies/service_unavailable", status: :service_unavailable
  end

  def validate_filter(filter)
    return filter if Set["compliant", "noncompliant"].include?(filter)

    "noncompliant"
  end

  def require_policies_feature_enabled
    render_404 unless current_organization.policies_enabled_for?(current_user)
  end
end
