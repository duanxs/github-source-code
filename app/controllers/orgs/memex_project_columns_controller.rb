# frozen_string_literal: true

class Orgs::MemexProjectColumnsController < Orgs::Controller
  include MemexesHelper

  before_action :login_required
  before_action :require_memex_enabled
  before_action :organization_read_required
  before_action :require_this_memex
  before_action :require_this_column, only: [:update]
  before_action :require_verified_email, only: [:create]
  before_action :require_positive_position, only: [:create, :update]
  before_action :require_valid_data_type, only: [:create]
  before_action :save_default_columns, only: [:create, :update]

  before_action :set_client_uid

  statsd_tag_actions

  def create
    column = this_memex.add_user_defined_column(
      name: create_memex_column_params[:name],
      data_type: create_memex_column_params[:data_type],
      position: create_memex_column_params[:position],
      creator: current_user,
    )

    if column.valid? && column.persisted?
      render(json: camelize_keys(memex_project_column: column.to_hash), status: :created)
    else
      render(json: { errors: column.errors.full_messages }, status: :unprocessable_entity)
    end
  end

  def update
    shown = !this_column.visible && update_memex_column_params.fetch(:visible, nil)
    success = this_memex.update_column(
      this_column,
      name: update_memex_column_params[:name],
      position: update_memex_column_params[:position],
      visible: update_memex_column_params[:visible],
    )

    if success && shown
      items = this_memex.prioritized_memex_project_items
      GitHub::PrefillAssociations.for_memex_project_items(items, columns: [this_column])
      items_with_redactions = MemexProjectItemRedactor.new(viewer: current_user, items: items).items
      render(json: camelize_keys(memex_project_column: this_column.to_hash(items: items_with_redactions)))
    elsif success
      render(json: camelize_keys(memex_project_column: this_column.to_hash))
    else
      render(json: { errors: this_column.errors.full_messages }, status: :unprocessable_entity)
    end
  end

  def create_memex_column_params
    underscored_params.require(:memex_project_column).permit(
      :name,
      :data_type,
      :position,
    )
  end

  def update_memex_column_params
    underscored_params.permit(
      :org,
      :user_id,
      :memex_number,
      :memex_project_column_id,
      :name,
      :visible,
      :position,
    )
  end

  private

  def require_positive_position
    position = if update_request?
      update_memex_column_params[:position]
    else
      create_memex_column_params[:position]
    end

    if position && position.to_i < 1
      render_json_error(error: "Position must be a positive integer", status: :unprocessable_entity)
    end
  end

  def require_this_column
    render_404 unless this_column
  end

  def require_valid_data_type
    data_type = create_memex_column_params[:data_type]
    unless data_type == "text"
      render_json_error(error: "You must provide one of the following supported data_types: text", status: :unprocessable_entity)
    end
  end

  def this_column
    @this_column ||= \
      this_memex.find_column_by_name_or_id(update_memex_column_params[:memex_project_column_id])
  end

  def this_memex
    return @this_memex if defined?(@this_memex)
    @this_memex = this_organization
      .memex_projects
      .includes(:owner)
      .find_by(number: params[:memex_number])
  end

  def save_default_columns
    this_memex.save_default_columns!
    return unless update_request?

    @this_column = this_memex.reload.find_column_by_name_or_id(
      update_memex_column_params[:memex_project_column_id]
    )
  end

  def update_request?
    action_name == "update"
  end
end
