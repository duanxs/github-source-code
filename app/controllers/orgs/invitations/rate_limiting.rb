# frozen_string_literal: true

# Including class must implement #invite_rate_limited_organization
module Orgs::Invitations::RateLimiting
  extend ActiveSupport::Concern

  module ClassMethods
    def setup_org_invite_rate_limiting(only:)
      include GitHub::RateLimitedRequest

      rate_limit_requests \
        only: only,
        if: :org_invite_rate_limit_filter,
        key: :org_invite_rate_limit_key,
        max: :org_invite_rate_limit_max,
        ttl: :org_invite_rate_limit_ttl,
        at_limit: :org_invite_rate_limited,
        render_allow_body: true
    end
  end

  def org_invite_rate_limit_policy
    @org_invite_rate_limit_policy ||= OrganizationInvitation::RateLimitPolicy.new(invite_rate_limited_organization)
  end

  # Whether to rate limit invitations for this organization.
  def org_invite_rate_limit_filter
    # Don't rate limit org invites if they aren't enabled
    return false if GitHub.bypass_org_invites_enabled?

    true
  end

  def org_invite_rate_limit_key
    "orgs/invitations.new:org-#{invite_rate_limited_organization.id}"
  end

  def org_invite_rate_limit_max
    org_invite_rate_limit_policy.limit
  end

  def org_invite_rate_limit_ttl
    org_invite_rate_limit_policy.ttl
  end

  def org_invite_rate_limited?
    return true  if Rails.env.development? && params[:fakestate] == "ratelimited"
    return false unless org_invite_rate_limit_filter

    options = {
      check_without_increment: true,
      max_tries: org_invite_rate_limit_max,
      ttl: org_invite_rate_limit_ttl,
    }
    RateLimiter.at_limit?(org_invite_rate_limit_key, options)
  end
end
