# frozen_string_literal: true

class Orgs::MigrationController < Orgs::Controller
  areas_of_responsibility :orgs

  include OrganizationsHelper

  before_action :login_required
  before_action :organization_admin_required
  before_action :redirect_to_member_privileges

  def index
    render_404
  end

  def customize_member_privileges
    render_404
  end

  def update_member_privileges
    render_404
  end

  def owners_team
    render_404
  end

  private

  # Deprecating the Organization migration pages and functionality -
  # now just redirect the user to the new Organization member
  # privileges page
  def redirect_to_member_privileges
    redirect_to settings_org_member_privileges_path(current_organization)
  end
end
