# frozen_string_literal: true

class Orgs::ActionsSettings::RunnerLabelsController < Orgs::Controller
  map_to_service :actions_runners

  include OrganizationsHelper
  include Actions::RunnersHelper

  before_action :login_required
  before_action :ensure_organization_exists
  before_action :organization_admin_required
  before_action :ensure_trade_restrictions_allows_org_settings_access
  before_action :ensure_can_use_org_runners

  def index
    respond_to do |format|
      format.html do
        render Actions::RunnerLabelsComponent.new(
          owner: current_organization,
          runner_ids: [params[:runner_id]],
          labels: labels_for(current_organization),
          selected_labels: params[:applied_labels]
        )
      end
    end
  end

  def bulk
    runners = Actions::Runner.with_ids(params[:runner_ids], entity: current_organization)
    selected_labels = runners.flat_map(&:label_ids)
    respond_to do |format|
      format.html do
        render Actions::RunnerLabelsComponent.new(
          owner: current_organization,
          runner_ids: runners.map(&:id),
          labels: labels_for(current_organization),
          selected_labels: selected_labels,
          descriptor: "bulk"
        )
      end
    end
  end

  def create
    label = create_label_for(current_organization, name: params[:name])

    respond_to do |wants|
      wants.html do
        if label
          render Actions::RunnerLabelComponent.new(label: label, selected: false)
        else
          errorMessage = "Sorry, there was a problem adding your label."
          render json: { message: errorMessage }, status: :unprocessable_entity
        end
      end
    end
  end

  def update
    labels = Array(params[:labels])
    affected_runner = update_label_for(current_organization, runner_id: params[:runner_id], labels: labels)

    unless affected_runner
      flash[:error] = "Sorry, there was a problem updating your labels"
    end

    if enterprise_runners_enabled?
      # Runner groups are enabled, so we re-render the runner component only, instead of the entire containing box.
      # This avoids expanded runner groups from closing themselves.
      owner_settings = Actions::OrgRunnersView.new(settings_owner: current_organization, current_user: current_user)

      render Actions::RunnerComponent.new(
        runner: affected_runner,
        owner_settings: owner_settings,
        hidden: false,
        is_child_row: true
      )
    else
      redirect_to settings_org_actions_list_runners_path
    end
  end

  def bulk_update
    runners = bulk_update_labels_for(current_organization, runner_ids: Array(params[:runner_ids]), additions: Array(params[:add]), removals: Array(params[:remove]))

    unless runners
      flash[:error] = "Sorry, there was a problem updating your labels"
    end

    redirect_to settings_org_actions_list_runners_path
  end

  private

  def enterprise_runners_enabled?
    GitHub.flipper[:actions_enterprise_runners].enabled?(current_user) || GitHub.enterprise?
  end
end
