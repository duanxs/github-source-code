# frozen_string_literal: true

class Orgs::ActionsSettings::RunnerGroupsController < Orgs::Controller
  include OrganizationsHelper
  include Actions::RunnersHelper
  include Actions::RunnerGroupsHelper

  map_to_service :actions_runners

  before_action :login_required
  before_action :ensure_organization_exists
  before_action :organization_admin_required
  before_action :ensure_trade_restrictions_allows_org_settings_access
  before_action :ensure_can_use_org_runners
  before_action :ensure_actions_enterprise_runners
  before_action :ensure_tenant_exists, only: :create
  before_action :ensure_can_create_runner_groups, only: :create

  javascript_bundle :settings

  def create
    name = params[:name]
    visibility = params[:visibility]&.to_sym

    if name.nil? || visibility.nil?
      flash[:error] = "Failed to create runner group."
      redirect_to settings_org_actions_path
      return
    end

    selected_repositories = visibility == GitHub::LaunchClient::RunnerGroups::GROUP_VISIBILITY_SELECTED ?
      Array(params[:repository_ids]) : []

    result = create_group_for(current_organization, name: name, visibility: visibility, selected_targets: selected_repositories)
    if !result.call_succeeded?
      flash[:error] = "Failed to create runner group."
    else
      flash[:notice] = "Runner group created."
    end

    redirect_to settings_org_actions_path
  end

  def update
    name = params[:name]
    visibility = params[:visibility]&.to_sym

    runner_group_id = params[:id]&.to_i

    if runner_group_id.nil? || visibility.nil?
      flash[:error] = "Failed to update runner group."
      redirect_to settings_org_actions_path
      return
    end

    selected_repositories = visibility == GitHub::LaunchClient::RunnerGroups::GROUP_VISIBILITY_SELECTED ?
      Array(params[:repository_ids]) : []

    result = update_group_for(current_organization, id: runner_group_id, name: name, visibility: visibility, selected_targets: selected_repositories)
    if !result.call_succeeded?
      flash[:error] = "Failed to update runner group."
    else
      flash[:notice] = "Runner group updated."
    end

    redirect_to settings_org_actions_path
  end

  def destroy
    runner_group_id = params[:id]&.to_i

    if runner_group_id.nil?
      flash[:error] = "Failed to delete runner group."
      redirect_to settings_org_actions_path
      return
    end

    result = delete_group_for(current_organization, id: runner_group_id)
    if !result.call_succeeded?
      flash[:error] = "Failed to delete runner group."
    else
      flash[:notice] = "Runner group deleted."
    end

    redirect_to settings_org_actions_path
  end

  def show_selected_targets
    runner_group_id = params[:id]&.to_i
    runner_group = runner_group_for(current_organization, id: runner_group_id) if runner_group_id.present?

    selected_repositories = []
    if runner_group.present?
      selected_target_global_ids = runner_group.selected_targets.map { |identity| identity.global_id }.to_set
      selected_target_ids = selected_target_global_ids.map { |global_id| Platform::Helpers::NodeIdentification.from_global_id(global_id)[1] }
      selected_repositories = current_organization.repositories.where(id: selected_target_ids)
    end

    selected_repository_ids = selected_repositories.map(&:global_relay_id)

    respond_to do |format|
      format.html do
        render Organizations::Settings::RepositorySelectionComponent.new(
          organization: current_organization,
          repositories: selected_repositories,
          selected_repositories: selected_repository_ids,
          policy_type: Orgs::ActionsSettings::RepositoryItemsController::RUNNER_GROUPS_POLICY,
          policy_id: runner_group_id,
        )
      end
    end
  end

  def bulk_actions
    if runner_ids = params[:runner_ids]
      render Actions::RunnerGroupsBulkComponent.new(
        owner: current_organization,
        owner_settings: Actions::OrgRunnersView.new(settings_owner: current_organization, current_user: current_user),
        runner_ids: runner_ids,
      )
    else
      head :ok
    end
  end

  def show_menu
    render Actions::RunnerGroupsMenuItemsComponent.new(
      owner: current_organization,
      owner_settings: Actions::OrgRunnersView.new(settings_owner: current_organization, current_user: current_user),
      runner_groups: Actions::RunnerGroup.for_entity(current_organization),
    )
  end

  def update_runners
    runner_group_id = params[:runner_group_id]&.to_i
    runner_ids = Array(params[:runner_ids]).map(&:to_i)

    unless runner_group_id.present? && runner_ids.any?
      flash[:error] = "Failed to move runners."
      redirect_to settings_org_actions_path
      return
    end

    runner_group = add_runners_for(current_organization, id: runner_group_id, runners_ids: runner_ids)
    if runner_group.present?
      flash[:notice] = "Moved runners to #{runner_group.name}."
    else
      flash[:error] = "Failed to move runners."
    end

    redirect_to settings_org_actions_path
  end

  private

  def ensure_actions_enabled
    render_404 unless GitHub.actions_enabled?
  end

  def ensure_actions_enterprise_runners
    render_404 unless GitHub.flipper[:actions_enterprise_runners].enabled?(current_user) || GitHub.enterprise?
  end

  def ensure_tenant_exists
    result = GrpcHelper.rescue_from_grpc_errors("OrgTenant") do
      GitHub::LaunchClient::Deployer.setup_tenant(current_organization.business) if current_organization.business
      GitHub::LaunchClient::Deployer.setup_tenant(current_organization)
    end
    raise Timeout::Error, "Timeout fetching tenant" unless result.call_succeeded?
  end

  def ensure_can_create_runner_groups
    render_404 unless GitHub.enterprise? || current_organization.plan.business_plus? || current_organization.plan.enterprise?
  end
end
