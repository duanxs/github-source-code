# frozen_string_literal: true

class Orgs::ActionsSettings::RepositoryItemsController < Orgs::Controller
  include OrganizationsHelper
  include Actions::RunnerGroupsHelper
  include Actions::RunnersHelper
  include Actions::SecretsHelper

  before_action :login_required
  before_action :ensure_organization_exists
  before_action :organization_admin_required
  before_action :ensure_trade_restrictions_allows_org_settings_access
  before_action :ensure_can_use_org_runners

  map_to_service :actions_runners

  javascript_bundle :settings

  MAX_PAGES = 100
  PER_PAGE = 250

  RUNNER_GROUPS_POLICY = "RUNNER_GROUPS"
  RUNNERS_POLICY = "RUNNERS"
  SECRETS_POLICY = "SECRETS"
  ACCESS_POLICY = "ACCESS"

  def index
    total_count = current_organization.repositories.size
    page = params[:page].to_i

    return render_404 unless page && page > 0

    selected_repository_ids = []
    policy = params[:policy]
    if policy == RUNNERS_POLICY
      access_policy = access_policy_for(current_organization)
      selected_repositories = access_policy.selected_repositories.map { |identity| identity.global_id }.to_set
      selected_repository_ids = selected_repositories.map { |global_id| Platform::Helpers::NodeIdentification.from_global_id(global_id)[1] }
    elsif policy == RUNNER_GROUPS_POLICY && params[:policy_id].present?
      runner_group_id = params[:policy_id].to_i
      runner_group = runner_group_for(current_organization, id: runner_group_id)
      selected_repositories = runner_group.selected_targets.map { |identity| identity.global_id }.to_set
      selected_repository_ids = selected_repositories.map { |global_id| Platform::Helpers::NodeIdentification.from_global_id(global_id)[1] }
    elsif policy == SECRETS_POLICY
      secret_name = params[:policy_id]
      if secret_name and !secret_name.empty?
        result = GrpcHelper.rescue_from_grpc_errors("Secrets") do
          Credz.fetch_credential(
            app: actions_integration,
            owner: current_organization,
            actor: current_user,
            key: secret_name,
          )
        end
        secret = result.value&.credential
        return render_404 unless secret

        selected_repositories = secret.selected_repositories.map(&:global_id).to_set
        selected_repository_ids = selected_repositories.map { |global_id| Platform::Helpers::NodeIdentification.from_global_id(global_id)[1] }
      end
    elsif policy == ACCESS_POLICY
      selected_repository_ids = current_organization.repositories.with_ids(current_organization.actions_allowed_entities).map(&:global_id).to_set
    end
    # Only include repositories that have not been selected already
    # Selected repositories are shown with the first page
    repositories = current_organization.repositories
    .order(:id)
    .paginate(
      page: page,
      per_page: PER_PAGE,
      total_entries: [total_count, MAX_PAGES * PER_PAGE].min,
    ).where.not(id: selected_repository_ids)

    respond_to do |format|
      format.html do
        render Organizations::Settings::RepositoryItemsComponent.new(
          next_page: page + 1,
          organization: current_organization,
          repositories: repositories,
          selected_repositories: [],
          total_count: current_organization.repositories.size,
          policy_type: policy,
          policy_id: params[:policy_id],
        )
      end
    end
  end

  private

  def actions_integration
    # Always store secrets with the prod app, even in lab.
    @integration ||= GitHub.launch_github_app
  end

end
