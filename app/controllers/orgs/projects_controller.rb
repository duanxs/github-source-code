# frozen_string_literal: true

class Orgs::ProjectsController < Orgs::Controller
  include ProjectControllerActions
  include SharedProjectControllerActions

  statsd_tag_actions only: [:clone, :search_results, :index, :show]

  before_action :organization_read_required, only: %w[new create]
  before_action :require_projects_enabled
  before_action :this_project_required, except: %w[index create new destroy linkable_repositories]
  before_action :project_read_required, except: %w[index create new destroy linkable_repositories]
  before_action :project_write_required, except: %w[index show create new clone destroy activity target_owner_results linkable_repositories]
  before_action :current_user_required, only: [:clone, :target_owner_results]
  before_action :verify_cloning_conditional_access, only: [:clone]
  before_action :set_client_uid
  before_action :set_cache_control_no_store, only: [:show, :search_results]

  layout "org_projects"

  def index
    render_projects_index(owner: this_organization)
  end

  def new
    render_new_project(owner: this_organization)
  end

  def edit
    override_analytics_location "/orgs/<org-login>/projects/<id>/edit"
    render_edit_project(owner: this_organization, project: this_project)
  end

  def show
    override_analytics_location "/orgs/<org-login>/projects/<id>"
    strip_analytics_query_string
    render_show_project(owner: this_organization, project: this_project)
  end

  def search_results
    render_project_search_results(owner: this_organization, project: this_project)
  end

  def activity
    render_project_activity(owner: this_organization, project: this_project)
  end

  def add_cards_link
    render_project_add_cards_link(project: this_project)
  end

  def create
    create_project(owner: this_organization)
  end

  def update
    update_project(owner: this_organization, project: this_project)
  end

  def update_state
    update_project_state(project: this_project, state: params[:state], sync: params[:sync])
  end

  def clone
    clone_project(project: this_project)
  end

  def destroy
    destroy_project(owner: this_organization)
  end

  def repository_results
    if params[:search_in] == "suggestions"
      repos = this_project.repository_suggestions(viewer: current_user, filter: params[:q])
    else
      query = Search::Queries::RepoQuery.new \
          current_user: current_user,
          user_session: user_session,
          remote_ip:    request.remote_ip,
          phrase:       "org:#{this_organization.login} #{params[:q]}"

      repos = query.execute.results.map { |result| result["_model"] }
      repos.select! { |repo| repo.has_issues? }
    end
    respond_to do |format|
      format.html_fragment { render partial: "projects/repository_results", formats: :html, locals: { repositories: repos } }
    end
  end

  def target_owner_results
    render_target_owner_results(project: this_project)
  end

  def linkable_repositories
    render_linkable_repositories(owner: this_organization, project: this_project)
  end

  private

  helper_method :parsed_projects_query

  def this_project
    # We don't load through visible_projects_for here, since permissions are
    # checked in before_actions.
    @this_project ||= this_organization.projects.find_by(number: params[:number])
  end

  def this_project_required
    render_404 if this_project.nil?
  end

  def project_read_required
    render_404 unless this_project.readable_by?(current_user)
  end

  def current_user_required
    render_404 unless logged_in?
  end

  def project_write_required
    return true if this_project.writable_by?(current_user)
    return render_404 unless this_project.readable_by?(current_user)

    # If we can read the project but can't write it, show an informative error
    # message or status code.
    respond_to do |format|
      format.html do
        flash[:error] = "You don't have permission to perform this action on this project."
        redirect_to project_path(this_project)
      end

      format.json do
        head 403
      end
    end
  end
end
