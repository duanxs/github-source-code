# frozen_string_literal: true

class Orgs::OauthApplicationApprovalsController < Orgs::Controller
  areas_of_responsibility :platform, :orgs

  before_action :organization_read_or_outside_collaborator_required, only: [:request_approval]
  before_action :organization_admin_required, only: [:show, :set_state]
  before_action :sudo_filter, except: :request_approval

  javascript_bundle :settings

  def show
    unless this_organization.restricts_oauth_applications?
      redirect_to settings_org_oauth_application_policy_path(this_organization)
      return
    end

    @approval = this_organization.
                  oauth_application_approvals.
                  find_by_application_id(params[:application_id])

    @application = oauth_app

    render_template_view "orgs/oauth_application_approvals/show", Oauth::AuthorizeView,
                         approval: @approval,
                         application: @application
  end

  def request_approval
    this_organization.request_oauth_application_approval(oauth_app, requestor: current_user)

    if request.xhr?
      head 200
    else
      redirect_to org_application_approval_path(this_organization, oauth_app)
    end
  end

  def set_state
    approval = nil

    if state = params[:state].presence
      if state == "approved"
        approval = this_organization.approve_oauth_application(oauth_app, approver: current_user)
        notice = "#{oauth_app.name} is authorized to access this organization’s resources"
      elsif state == "denied"
        approval = this_organization.deny_oauth_application(oauth_app)
        notice = "#{oauth_app.name} is denied access this organization’s resources"
      end

      flash[:notice] = notice if params[:flash]
    end

    if request.xhr?
      render json: { approval_state: approval.try(:state) }, status: 200
    else
      redirect_to org_application_approval_path(this_organization, oauth_app)
    end
  end

  private

  def oauth_app
    @oauth_app ||= OauthApplication.find(params[:application_id])
  end
end
