# frozen_string_literal: true

class Orgs::SecurityAnalysisController < ApplicationController
  include Settings::SecurityAnalysisSettings

  def update
    return render_404 unless current_organization&.adminable_by?(current_user)

    update_security_settings(current_organization, params, current_user)
    flash[:notice] = "Security settings updated for #{current_organization.name}'s repositories."
    redirect_to settings_org_security_analysis_path(current_organization)
  end

  private

  def current_organization
    return @current_organization if defined? @current_organization
    @current_organization = Organization.find_by_login(params[:org] || params[:organization_id])
  end
end
