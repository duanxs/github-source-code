# frozen_string_literal: true

class Orgs::TeamMembersController < Orgs::Controller
  areas_of_responsibility :orgs

  statsd_tag_actions only: [:create, :index, :suggestions]

  layout "team"

  before_action :login_required
  before_action :this_team_required
  before_action :admin_on_team_required, except: [:index]
  before_action :sudo_filter, only: [:destroy]
  before_action :sudo_filter, except: %i(
    index
    destroy
    migrate_to_collaborator
    set_role
    suggestions
  )
  before_action :restrict_externally_managed_team, only: [:destroy, :create]

  before_action only: %i(
    destroy
    set_role
    migrate_to_collaborator
  ) do
    ensure_trade_restrictions_allows_org_member_management(fallback_location: team_members_url(this_organization))
  end

  include Orgs::Invitations::RateLimiting
  setup_org_invite_rate_limiting only: [:create]

  MEMBER_PAGE_SIZE = 30

  IndexQuery = parse_query <<-'GRAPHQL'
    query(
      $login: String!,
      $slug: String!,
      $showAdminStuff: Boolean!,
      $orgHasLimitedSeats: Boolean!
    ) {
      organization(login: $login) {
        ...Views::Layouts::Team::Organization
        ...Views::Orgs::TeamMembers::Index::Organization
        team(slug: $slug) {
          ...Views::Orgs::Teams::MembersToolbarActions::Team
        }
      }
    }
  GRAPHQL

  def index
    set_hovercard_subject(this_organization)

    #FIXME: combine these into one once the GraphQL is gone
    parsed_query = TeamMemberQueryComponents.new(params[:query])
    extra_parsed_query = TeamMemberQueryComponents.new(params[:query], graphql: false)

    data = platform_execute(IndexQuery, variables: graphql_pagination_params(page_size: MEMBER_PAGE_SIZE).merge(
      "login" => params[:org] || params[:organization_id],
      "slug" => params[:team_slug],
      "query" => parsed_query.query,
      "membership" => parsed_query.membership,
      "role" => parsed_query.role,
      "showAdminStuff" => !!this_team&.adminable_by?(current_user),
      "orgHasLimitedSeats" => this_organization.plan.per_seat? && !this_organization.has_unlimited_seats?,
    ))

    members = Team::Membership::ScopeBuilder.new(
      team_id: this_team.id,
      viewer: current_user,
      membership: extra_parsed_query.membership,
      role: extra_parsed_query.role,
      query: extra_parsed_query.query,
      max_members_limit: Organization::MEGA_ORG_MEMBER_THRESHOLD
    ).scope.includes(:profile).paginate(page: params[:page], per_page: MEMBER_PAGE_SIZE)

    immediate_member_scope = Team::Membership::ScopeBuilder.new(
      team_id: this_team.id,
      viewer: current_user,
      membership: :immediate,
      max_members_limit: Organization::MEGA_ORG_MEMBER_THRESHOLD
    ).scope

    child_member_scope = Team::Membership::ScopeBuilder.new(
      team_id: this_team.id,
      viewer: current_user,
      membership: :child_team,
      max_members_limit: Organization::MEGA_ORG_MEMBER_THRESHOLD
    ).scope

    options = {
      organization: this_organization,
      team: this_team,
      query: parsed_query.query,
      membership: parsed_query.membership,
      role: parsed_query.role,
      graphql_team: data.organization.team,
      graphql_org: data.organization,
      members: members,
      immediate_members: immediate_member_scope,
      child_members: child_member_scope,
    }

    override_analytics_location "/orgs/<org-login>/teams/<team-name>/members"
    strip_analytics_query_string

    respond_to do |format|
      format.html do
        response.headers["Vary"] = "X-Requested-With"
        if request.xhr?
          render_partial_view(
            "orgs/team_members/member_table",
            Orgs::TeamMembers::IndexPageView,
            options)
        else
          render_template_view(
            "orgs/team_members/index",
            Orgs::TeamMembers::IndexPageView,
            options,
            locals: {
              graphql_org: data.organization,
              rate_limited: org_invite_rate_limited?,
              selected_nav_item: :members,
            })
        end
      end
      format.json do
        team_members_scope = this_team.descendant_or_self_members
        total = team_members_scope.count
        members = team_members_scope.first(20).map(&:login).sort_by(&:downcase)
        render json: {total: total, members: members}
      end
    end
  end

  def create
    invitation_options = {
      user: User.find_by_login(params[:member]),
      email: User.valid_email?(params[:member]) ? params[:member] : nil,
      inviter: current_user,
    }

    if invitation_options[:user].blank? && invitation_options[:email].blank?
      add_member_status = Team::AddMemberStatus::BLOCKED
      invitation_options[:user] = User.new login: params[:member]
    elsif invitation_options[:email] && email_verification_required?
      # User must have verified email address to send invites to email addresses
      return render_email_verification_required
    else
      result = this_team.add_or_invite_member(**invitation_options)
      if result.is_a?(Team::AddMemberStatus)
        add_member_status = result
      else
        invitation = result
      end
    end

    if result == false
      render_404 and return
    end

    if add_member_status && add_member_status.error?
      member = invitation_options[:user]
      login_or_email = (member && member.login) || invitation_options[:email]

      case add_member_status
      when Team::AddMemberStatus::BLOCKED
        error = "#{login_or_email} is blocked from joining this organization."
      # NOTE - Team::AddMemberStatus::DUPE cannot happen here because it is a success and not an error
      when Team::AddMemberStatus::NO_SEAT
        error = "You have no remaining seats in your organization. Please purchase at least one additional seat before inviting #{login_or_email} to #{this_team.organization.safe_profile_name}."
      when Team::AddMemberStatus::NO_2FA
        error = "#{login_or_email} needs to enable two-factor authentication."
      when Team::AddMemberStatus::NO_PERMISSION
        error = "You do not have permission to invite #{login_or_email}."
      when Team::AddMemberStatus::NO_SAML_SSO
        error = "#{login_or_email} needs to satisfy the SAML SSO requirements for this organization."
      when Team::AddMemberStatus::NOT_USER
        error = "#{login_or_email} is not a human user and cannot join any team."
      when Team::AddMemberStatus::PENDING_CYCLE_NO_SEAT
        error = "Your organization has a pending seat downgrade. Please cancel your pending change before inviting #{login_or_email} to #{this_team.organization.safe_profile_name}."
      when Team::AddMemberStatus::TRADE_CONTROLS_RESTRICTED
        error = add_member_status.message
      else
        error = "Error adding #{login_or_email} to #{this_team.name}."
      end

      flash[:error] = error
    end
    redirect_to team_members_path(this_team)
  end

  def suggestions
    respond_to do |format|
      format.html_fragment do
        render_partial_view "orgs/team_members/suggestions", Orgs::TeamMembers::SuggestionsView,
          organization: this_organization,
          team: this_team,
          query: params[:q],
          include_teams: false
      end
      format.html do
        render_partial_view "orgs/team_members/suggestions", Orgs::TeamMembers::SuggestionsView,
          organization: this_organization,
          team: this_team,
          query: params[:q],
          include_teams: false
      end
    end
  end

  def destroy
    if params[:member]
      members = [User.find_by_login(params[:member])]
    else
      members = User.where(id: params[:team_members_ids].split(",")).to_a
    end

    members.each do |member|
      this_team.remove_member(member)
    end

    if members.length == 1
      flash[:notice] = "You've removed #{members.first} from the team."
    else
      flash[:notice] = "You've removed #{members.length} members from the team."
    end

    if params[:redirect_to_path].present?
      safe_redirect_to params[:redirect_to_path]
    else
      redirect_to :back
    end
  end

  def set_role
    members = this_team.members(actor_ids: params[:team_member_ids].split(","))

    total_changed = 0

    members.each do |member|
      # TODO: consider ignoring team_member_ids for members who are already the desired role
      if !this_organization.admins.include?(member)
        case params[:role]
        when "maintainer"
          this_team.promote_maintainer(member)
          total_changed += 1
        when "member"
          this_team.demote_maintainer(member)
          total_changed += 1
        end
      end
    end

    flash[:notice] = if total_changed > 0
      "#{params[:role] == "maintainer" ? "Promoted" : "Demoted"} #{total_changed} #{"member".pluralize(total_changed)} (Organization owners unaffected)."
    else
      "This action has no effect on Organization owners."
    end

    redirect_to team_members_path(this_team)
  end

  def set_maintainer
    member = this_team.members.find_by_login!(params[:member])

    if params[:maintainer] == "1"
      flash[:notice] = "#{member} is now a team maintainer."
      this_team.promote_maintainer(member)
    else
      flash[:notice] = "#{member} is no longer a team maintainer."
      this_team.demote_maintainer(member)
    end

    redirect_to team_members_path(this_team)
  end

  def migrate_to_collaborator
    members = this_organization.visible_users_for(current_user, actor_ids: params[:member_ids].split(","))
    members.each { |user| this_organization.convert_to_outside_collaborator(user) }

    unless request.xhr?
      flash[:notice] = if members.size == 1
        "Migrated #{members.first.login} to outside collaborator."
      else
        "Migrated #{members.size} people to outside collaborators."
      end
    end

    respond_to do |format|
      format.json { head 204 }
      format.html { redirect_to team_members_path(this_team) }
    end
  end

  private

  def email_verification_required?
    authorization = ContentAuthorizer.authorize(
      current_user, :organization_invitation, :create
    )

    authorization.has_email_verification_error?
  end

  def org_invite_rate_limited
    org_invite_rate_limit_policy.record_rate_limited

    render status: 429, json: {
      message_html: render_to_string(
        partial: "orgs/invitations/rate_limited_message",
      ),
    }
  end

  def restrict_externally_managed_team
    return if this_team.locally_managed?
    flash[:error] = "This team is configured for automatic sychronization, manual changes cannot be applied."
    redirect_to team_members_path(this_team)
  end
end
