# frozen_string_literal: true

class Orgs::TeamDiscussionCommentsController < Orgs::Controller
  areas_of_responsibility :teams

  statsd_tag_actions only: [:create, :show]

  before_action :login_required
  before_action :require_team_discussions_allowed

  after_action TaskListInstrumentUpdateFilter, only: [:update]

  PAGE_SIZE = 5

  IndexQuery = parse_query <<-'GRAPHQL'
    query(
      $login: String!,
      $slug: String!,
      $number: Int!,
      $afterComment: String,
      $beforeComment: String,
      $fromComment: Int,
      $firstNComments: Int,
      $lastNComments: Int
    ) {
      organization(login: $login) {
        team(slug: $slug) {
          discussion(number: $number) {
            ...Views::Orgs::TeamDiscussionComments::Index::TeamDiscussion
          }
        }
      }
    }
  GRAPHQL

  def index
    return render_404 unless request.xhr?

    variables = {
      afterComment: params[:after_comment].presence,
      beforeComment: params[:before_comment].presence,
      login: params[:org] || params[:organization_id],
      slug: params[:team_slug],
      number: params[:discussion_number].to_i,
    }
    variables[paginating_forwards? ? :firstNComments : :lastNComments] = PAGE_SIZE

    respond_to do |format|
      format.html do
        data = platform_execute(IndexQuery, variables: variables)

        return render_404 unless data.organization&.team&.discussion

        render(
          partial: "orgs/team_discussion_comments/index",
          locals: {
            paginating_backwards: paginating_backwards?,
            paginating_forwards: paginating_forwards?,
            discussion: data.organization.team.discussion,
          })
      end
    end
  end

  def show
    anchor = helpers.team_discussion_comment_dom_id(
      discussion_number: params[:discussion_number],
      comment_number: params[:number],
    )

    redirect_to team_discussion_path({
      org: params[:org] || params[:organization_id],
      team_slug: params[:team_slug],
      number: params[:discussion_number],
      from_comment: params[:number],
      anchor: anchor,
    }.merge(notifications_referrer_params))
  end

  CreateMutation = parse_query <<-'GRAPHQL'
    mutation(
      $input: CreateTeamDiscussionCommentInput!,
      $afterComment: String,
      $beforeComment: String,
      $fromComment: Int,
      $firstNComments: Int,
      $lastNComments: Int
    ) {
      createTeamDiscussionComment(input: $input)  {
        teamDiscussionComment {
          discussion {
            number
            ...Views::Orgs::TeamDiscussionComments::List::TeamDiscussion
            ...Views::Orgs::TeamDiscussionComments::CursorInput::TeamDiscussion
          }
        }
      }
    }
  GRAPHQL

  def create
    input = creation_params
    variables = {
      input: input,
      afterComment: params[:after_comment].presence,
      beforeComment: params[:before_comment].presence,
      lastNComments: 50,
    }
    mutation = platform_execute(CreateMutation, variables: variables)
    respond_to do |format|
      format.json do
        return render_graphql_error(mutation) if mutation.errors.any?

        discussion = mutation.create_team_discussion_comment.team_discussion_comment.discussion
        template = render_to_string(
          partial: "orgs/team_discussion_comments/list",
          locals: { discussion: discussion },
          formats: [:html])
        cursor_template = render_to_string(
          partial: "orgs/team_discussion_comments/cursor_input",
          locals: { team_discussion: discussion },
          formats: [:html])
        partials = {
          "#cursor_input_#{discussion.number}" => cursor_template,
          "#comment_timeline_marker_#{discussion.number}" => template,
        }
        render json: { updateContent: partials }
      end
    end
  end

  UpdateQuery = parse_query <<-'GRAPHQL'
    mutation($input: UpdateTeamDiscussionCommentInput!) {
      updateTeamDiscussionComment(input: $input) {
        teamDiscussionComment {
          body
          bodyHTML
          bodyVersion
        }
      }
    }
  GRAPHQL

  def update
    input = update_params

    task_list_update = TaskListOperation.from(params[:task_list_operation]).try(:call, input[:body])
    input[:body] = task_list_update || input[:body]

    data = platform_execute(UpdateQuery, variables: { input: input })

    respond_to do |wants|
      wants.json do
        if data.errors.any?
          stale_model = (
            data.errors.details[:updateTeamDiscussionComment].first["type"] == "STALE_MODEL")

          # Our stale model detection JS requires us to return a JSON payload whose errors are
          # not nested under the "errors" key, unlike the error payload returned by default from
          # `render_graphql_error`.
          return render_graphql_error(data, active_model_errors_format: !stale_model)
        end

        updated_comment = data.update_team_discussion_comment.team_discussion_comment

        render json: {
          source: updated_comment.body,
          body: updated_comment.body_html,
          newBodyVersion: updated_comment.body_version,
        }
      end

      wants.html do
        redirect_to :back
      end
    end
  end

  DestroyQuery = parse_query <<-'GRAPHQL'
    mutation($input: DeleteTeamDiscussionCommentInput!) {
      deleteTeamDiscussionComment(input: $input)
    }
  GRAPHQL

  def destroy
    input = deletion_params
    data = platform_execute(DestroyQuery, variables: { input: input })

    return render_graphql_error(data) if data.errors.any?

    return head(:ok) if request.xhr?

    redirect_to :back
  end

  private

  def creation_params
    params.require(:input).permit %i[body discussionId]
  end

  def update_params
    params.require(:team_discussion_comment).permit %i[body bodyVersion id]
  end

  def deletion_params
    params.require(:input).permit(:id)
  end

  def require_team_discussions_allowed
    return render_404 unless this_organization.team_discussions_allowed?
  end

  def paginating_backwards?
    @paginating_backwards ||= !!params[:before_comment].presence
  end

  def paginating_forwards?
    @paginating_forwards ||= !!params[:after_comment].presence
  end
end
