# frozen_string_literal: true

class Orgs::RolesController < Orgs::Controller
  include OrganizationsHelper
  include BaseHelpers::Helpers

  before_action :login_required
  before_action :ensure_organization_exists
  before_action :organization_admin_required
  before_action :ensure_trade_restrictions_allows_org_settings_access
  before_action :custom_roles_enabled

  def repository_roles
    render_template_view "settings/organization/roles/index",
      ::Settings::Organization::Roles::IndexView,
      organization: current_organization
  end

  def new
    base_role = current_organization.default_repository_permission
    base_role = :read if base_role == :none
    base_role_fgps = RoleFgps.for(base_role: base_role)
    return render_template_view "settings/organization/roles/new",
      ::Settings::Organization::Roles::NewView,
      base_role_fgps: base_role_fgps,
      organization: current_organization
  end

  def edit
    begin
      role = Role.find_by!(id: params[:id], owner_id: current_organization.id, owner_type: current_organization.type)
    rescue ActiveRecord::RecordNotFound
      flash[:error] = "Something went wrong. Could not find the role."
      redirect_to settings_org_repository_roles_path
    else
      base_role_fgps = RoleFgps.for(base_role: role.base_role.name)

      render_template_view "settings/organization/roles/edit",
        ::Settings::Organization::Roles::EditView,
        role: role,
        base_role_fgps: base_role_fgps,
        organization: current_organization
    end
  end

  def create
    custom_role = Role.new(role_params)
    error_message = nil
    begin
      Permissions::CustomRoles.create!(custom_role, fgps: params.dig(:role, :fgps))
    rescue CustomRoleError => e
      error_message = custom_role.errors.full_messages.to_sentence.presence || "error saving additional permissions"
      Failbot.report!(e, app: "github")
    end

    unless error_message
      GitHub.dogstats.increment("custom_role.created", tags: ["result:success"])
      flash[:notice] = "#{custom_role.name} role was successfully created"
      redirect_to settings_org_repository_roles_path
    else
      GitHub.dogstats.increment("custom_role.created", tags: ["result:fail"])
      flash[:error] = error_message
      redirect_to new_settings_org_repository_roles_path
    end
  end

  def update
    error_message = nil
    new_fgps = params.dig(:role, :fgps) || []

    begin
      role = Role.find_by!(id: params[:id], owner_id: current_organization.id, owner_type: current_organization.type)
      Permissions::CustomRoles.update!(role, role_params: role_params, fgps: new_fgps, actor: current_user)
    rescue ActiveRecord::RecordNotFound => e
      error_message = "Something went wrong. Could not update the role at this time."
    rescue CustomRoleError => e
      error_message = role.errors.full_messages.to_sentence.presence || "error saving additional permissions"

      Failbot.report!(e, app: "github")
    end

    unless error_message
      GitHub.dogstats.increment("custom_role.updated", tags: ["result:success"])
      flash[:notice] = "#{role.name} role was successfully updated"
      redirect_to settings_org_repository_roles_path
    else
      GitHub.dogstats.increment("custom_role.updated", tags: ["result:fail"])
      flash[:error] = error_message
      redirect_to edit_settings_org_repository_roles_path
    end
  end

  def destroy
    error_message = nil
    begin
      role = Role.find_by!(id: params[:id], owner_id: current_organization.id, owner_type: "Organization")
      Permissions::CustomRoles.destroy!(role, current_user)
    rescue ActiveRecord::ActiveRecordError, ActiveRecord::RecordNotFound
      error_message = "Something went wrong. Could not delete role at this time."
    end

    unless error_message
      notice_message =
        if role.custom? && !role&.all_dependencies_updated?
          "#{role.name} role was successfully scheduled for deletion. Check again later."
        else
          "#{role.name} role was successfully deleted"
        end
      flash[:notice] = notice_message
    else
      flash[:error] = error_message
    end
    redirect_to settings_org_repository_roles_path
  end

  # Updates the FGP dropdown and summary with the newly selected base role
  def fgps
    role_name = params.dig(:role, :name)
    role = Role.find_by(name: role_name, owner_id: current_organization.id, owner_type: current_organization.type) if role_name
    base_role_fgps = RoleFgps.for(base_role: params.dig(:role, :base))

    return head 400 if base_role_fgps.nil?

    respond_to do |format|
      format.html do
        return render_partial_view "settings/organization/roles/fgps",
          ::Settings::Organization::Roles::NewView,
          organization: current_organization,
          base_role_fgps: base_role_fgps,
          role: role
      end
    end
  end

  # Returns all the FGP metadata associated with a given base role
  def fgp_metadata
    base_role = RoleFgps.for(base_role: params[:base_role])
    render json: base_role.available_fgps.index_by(&:label)
  end

  private

  # Internal: The base Role object selected by the user
  #
  # Returns a Role or nil
  def base_role
    return @base_role if defined?(@base_role)
    @base_role = Role.find_by(name: params[:role][:base])
  end

  def role_params
    strip_params(:name)

    params.
      require(:role).
      permit(:name, :description, :base, fgps: []).
      merge(owner_id: current_organization.id, owner_type: current_organization.type, base_role_id: base_role&.id).
      except(:base, :fgps)
  end

  def custom_roles_enabled
    render_404 unless custom_roles_enabled?
  end

  def strip_params(*keys)
    keys.each { |key| params[:role][key].strip! }
  end
end
