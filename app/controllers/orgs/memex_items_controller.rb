# frozen_string_literal: true

class Orgs::MemexItemsController < Orgs::Controller
  include MemexesHelper
  include MemexItemsControllerActions

  before_action :login_required
  before_action :require_memex_enabled
  before_action :organization_read_required
  before_action :require_this_memex
  before_action :require_verified_email
  before_action :require_priorities, only: [:create, :update]
  before_action :require_valid_content_parameters, only: [:create]
  before_action :require_this_repository, only: [:create]
  before_action :require_this_content, only: [:create]
  before_action :require_this_item, only: [:update]
  before_action :require_issue_or_pull_request, only: [:update, :suggested_assignees, :suggested_labels, :suggested_milestones]
  before_action :require_this_item_suggestions, only: [:suggested_assignees, :suggested_labels, :suggested_milestones]
  before_action :require_actor_can_update_title, only: [:update]
  before_action :require_actor_can_add_assignees, only: [:update, :suggested_assignees]
  before_action :require_actor_can_add_labels, only: [:update, :suggested_labels]
  before_action :require_actor_can_add_milestones, only: [:update, :suggested_milestones]
  before_action :require_previous_item, only: [:update]
  before_action :require_valid_column_value_parameters, only: [:update]
  before_action :require_repository_has_issues, only: [:update]

  before_action :set_client_uid
  before_action :set_cache_control_no_store

  statsd_tag_actions

  def create
    create_memex_item(memex: this_memex)
  end

  def update
    update_memex_item(item: this_item)
  end

  def destroy
    destroy_memex_items(memex: this_memex)
  end

  def index
    items = this_memex.prioritized_memex_project_items.limit(MemexProjectItem::PER_PAGE_LIMIT).to_a
    GitHub::PrefillAssociations.for_memex_project_items(items, columns: this_memex.visible_columns)

    sort_params = underscored_params.fetch(:sorted_by, {})

    serializer = MemexProjectItemSerializer.new(viewer: current_user,
                                                memex: this_memex,
                                                items: items,
                                                sort_column: sort_params[:column_id],
                                                direction: sort_params[:direction])

    result = serializer.result

    render(json: camelize_keys({ memex_project_items: result.items, sorted_by: result.active_sort }))
  end

  def suggested_assignees
    sorted_assignees = this_item_issue.sorted_assignees_list(current_user: current_user).map do |user|
      user.memex_suggestion_hash(selected: this_item_issue.assigned_to?(user))
    end

    render json: camelize_keys(suggestions: sorted_assignees)
  end

  def suggested_labels
    sorted_labels = this_item_issue
      .repository
      .sorted_labels(issue_or_pr: this_item_issue, cache_label_html: true)

    suggestions = sorted_labels.map do |label|
      label.memex_suggestion_hash(selected: this_item_issue.unique_label_ids.include?(label.id))
    end

    render(json: camelize_keys(suggestions: suggestions))
  end

  def suggested_milestones
    open_milestones, closed_milestones = this_item_issue
      .repository
      .available_milestones(current_milestone: this_item_issue.milestone)

    sorted_milestones = [this_item_issue.milestone].compact + open_milestones + closed_milestones

    suggestions = sorted_milestones.map do |milestone|
      milestone.memex_suggestion_hash(selected: this_item_issue.milestone_id == milestone.id)
    end

    render(json: camelize_keys(suggestions: suggestions))
  end

  private

  def this_memex
    return @this_memex if defined?(@this_memex)
    @this_memex = this_organization
      .memex_projects
      .includes(:owner)
      .find_by(number: params[:memex_number])
  end

  def this_repository
    return if create_memex_item_params[:content_type] == DraftIssue.name
    return @this_repository if defined?(@this_repository)
    @this_repository = this_organization.repositories.find_by_id(
      create_memex_item_params[:content][:repository_id]
    )
  end

  def this_item_issue
    return @this_item_issue if defined?(@this_item_issue)
    return if underscored_params[:content_type] == DraftIssue.name

    @this_item_issue = this_item.content.is_a?(PullRequest) ? this_item.content.issue : this_item.content
  end

  def require_actor_can_update_title
    return unless column_to_update&.data_type&.to_sym == :title
    return if this_item.content.is_a?(DraftIssue)

    render_404 unless this_item_issue.editable_by?(current_user)
  end

  def require_issue_or_pull_request
    if action_name == "update"
      return unless [:assignees, :labels, :milestone].include?(column_to_update&.data_type&.to_sym)
    end

    if this_item&.content.is_a?(DraftIssue)
      render_json_error(error: "A content type of Issue or PullRequest is required", status: :unprocessable_entity)
    end
  end

  def require_this_item_suggestions
    return render_404 unless this_item&.content
    return render_404 unless this_item_issue&.repository
    return render_404 unless this_item_issue.repository.readable_by?(current_user)
    return render_404 if current_user.blocked_by?(this_item_issue.repository.owner_id)
    authorize_content(:issue, repo: this_item_issue.repository, action_to_authorize: "update")
  end

  def require_actor_can_add_assignees
    return if action_name == "update" && column_to_update&.data_type&.to_sym != :assignees
    unless this_item_issue.assignable_by?(actor: current_user)
      render_json_error(error: "User does not have permission to assign users", status: :unauthorized)
    end
  end

  def require_repository_has_issues
    return unless [:assignees, :labels, :milestone].include?(column_to_update&.data_type&.to_sym)
    return if this_item.content.is_a?(DraftIssue)

    if this_item.content.is_a?(Issue) && !this_item_issue.repository.has_issues?
      render_json_error(
        error: "Content cannot be updated because issues are disabled for the repository",
        status: :unprocessable_entity
      )
    end
  end

  def require_actor_can_add_labels
    return if action_name == "update" && column_to_update&.data_type&.to_sym != :labels
    unless this_item_issue.labelable_by?(actor: current_user)
      render_json_error(error: "User does not have permission to label items", status: :unauthorized)
    end
  end

  def require_actor_can_add_milestones
    return if action_name == "update" && column_to_update&.data_type&.to_sym != :milestone

    unless this_item_issue.can_set_milestone?(current_user)
      render_json_error(error: "User does not have permission to set milestones", status: :unauthorized)
    end
  end
end
