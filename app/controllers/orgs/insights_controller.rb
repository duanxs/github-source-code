# frozen_string_literal: true

class Orgs::InsightsController < Orgs::Controller
  include PackageDependencies::SharedActions

  areas_of_responsibility :orgs

  statsd_tag_actions only: [:show, :package_dependencies_index, :package_dependencies_licenses_graph, :package_dependencies_security_graph, :package_dependencies_license_menu_content]

  before_action :login_required
  before_action :ensure_access_allowed
  before_action :ensure_packages_dashboard_allowed, only: [:package_dependencies_index, :package_dependencies_security_graph, :package_dependencies_licenses_graph, :package_dependencies_license_menu_content]

  def index
    return render_404 unless validate_time_period_param

    insights_data = GitHub::OrgInsights::InsightsData.new(GitHub.eventer_client, this_organization.id)
    percentages_metrics = %w(PullRequestReviewed IssueCreated PullRequestCreated CommitCreated)
    percentages = insights_data.index_percentages(percentages_metrics, time_period: time_period, report_time: Time.now)

    return render_404 unless percentages.present?

    render "orgs/insights/index", locals: {
      this_organization: this_organization,
      time_period: time_period,
      selected_repos: selected_repos,
      show_packages_dashboard: this_organization.dependency_insights_enabled_for?(current_user),
      percentages: percentages,
    }
  end

  def get_org_repos
    repos =
      if params[:q].present?
        repository_results
      else
        (selected_repos + (repository_results - selected_repos)).uniq
      end

    respond_to do |format|
      format.html_fragment do
        render partial: "orgs/insights/repo_filter", formats: :html, locals: { period: time_period, org_repos: repos, selected_repos: selected_repos }
      end
      format.html do
        render partial: "orgs/insights/repo_filter", locals: { period: time_period, org_repos: repos, selected_repos: selected_repos }
      end
    end
  end

  def package_dependencies_index
    render_package_dependencies_index(this_organization: this_organization)
  end

  def package_dependencies_security_graph
    render_package_dependencies_security_graph(this_organization: this_organization)
  end

  def package_dependencies_licenses_graph
    render_package_dependencies_licenses_graph(this_organization: this_organization)
  end

  def package_dependencies_license_menu_content
    render_package_dependencies_license_menu_content(this_organization: this_organization)
  end

  def show
    return render_404 unless validate_time_period_param && validate_metric_params


    if (selected_repos.length > 0)
      response = collate_repository_data(repos: selected_repos)
    else
      insights_data = GitHub::OrgInsights::InsightsData.new(GitHub.eventer_client, this_organization.id, owner_type: "USER")
      response = insights_data.chart_data(params[:metrics], time_period: time_period, report_time: Time.now, user_zone: Time.zone)
    end

    return render_404 unless response.present?

    render json: response
  end

  def repository_results
    query = Search::Queries::RepoQuery.new \
      current_user: current_user,
      user_session: user_session,
      remote_ip:    request.remote_ip,
      per_page: 20,
      phrase: "org:#{this_organization.login} in:name #{params[:q]}".strip

    if params[:q].present?
      query.sort = ["updated", "desc"]
    end

    repos = query.execute.results.map { |result| result["_model"] }
    return repos
  end

  private

  def user_visible_repos
    @user_visible_repos ||= this_organization.visible_repositories_for(current_user).owned_by(this_organization)
  end

  def selected_repos
    param_repos = params.fetch(:repos, [])

    @selected_repos = user_visible_repos.where(name: param_repos)
  end

  def time_period
    @time_period ||= params.fetch(:period, "week")
  end

  def collate_repository_data(repos:)
    if repos.blank?
      return { metrics: [] , timespan: {}}
    end

    client = GitHub::OrgInsights::InsightsData.new(GitHub.eventer_client, this_organization.id)
    client.collate_data(params[:metrics], repos: repos, time_period: time_period, report_time: Time.now, user_zone: Time.zone)
  end

  def validate_metric_params
    params[:metrics].respond_to?(:all?) &&
    params[:metrics].all? { |m| GitHub::OrgInsights::InsightsData::METRIC_DISPLAY_NAMES[m].present? }
  end

  def validate_time_period_param
    GitHub::OrgInsights::InsightsData::TIME_PERIODS[time_period].present?
  end

  def ensure_access_allowed
    render_404 unless this_organization.insights_enabled? && this_organization.member?(current_user)
  end

  def ensure_packages_dashboard_allowed
    render_404 unless this_organization.dependency_insights_enabled_for?(current_user)
  end
end
