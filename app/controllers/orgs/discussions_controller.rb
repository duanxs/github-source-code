# frozen_string_literal: true

class Orgs::DiscussionsController < Orgs::Controller
  statsd_tag_actions only: [:index]

  before_action :require_discussions_org_list_feature
  javascript_bundle :discussions

  def index
    type_filter = params[:type]

    discussions = load_discussions(
      type_filter: type_filter,
      search_query: raw_discussions_search_query.presence && parsed_discussions_query,
    )

    participants_by_discussion_id = Discussion.participants_by_discussion_id(discussions,
      viewer: current_user)

    render "orgs/discussions/index", locals: {
      discussions: discussions,
      type_filter: type_filter,
      query: sanitized_query_string,
      participants_by_discussion_id: participants_by_discussion_id,
    }
  end

  def author_filter_content
    filtered_authors = Set.new(helpers.discussions_search_term_values(:author))
    repo_ids = this_organization.visible_repositories_for(current_user).
      owned_by(this_organization).pluck(:id)
    users = Discussion.authors_in_repo(repo_ids).filter_spam_for(current_user)
    sorted_users = users.partition do |user|
      (user.is_a?(Bot) && filtered_authors.include?("app/#{user.display_login}")) ||
        filtered_authors.include?(user.login)
    end

    respond_to do |format|
      format.html do
        render partial: "discussions/author_filter_content", locals: {
          users: sorted_users.flatten,
          query: sanitized_query_string,
        }
      end
    end
  end

  private

  def load_discussions(type_filter:, search_query:)
    if params[:discussions_q].present?
      Discussion::SearchResult.search(
        query: parsed_discussions_query,
        page: current_page,
        per_page: DEFAULT_PER_PAGE,
        current_user: current_user,
        remote_ip: request.remote_ip,
        user_session: user_session
      )
    else
      scope = Discussion.includes(:repository).
        paginate(page: params[:page], per_page: DEFAULT_PER_PAGE).
        filter_by_type(type_filter).
        recently_updated_first
      this_organization.visible_discussions_for(current_user, scope: scope)
    end
  end

  def sanitized_query_string
    @sanitized_query_string ||= Search::Queries::DiscussionQuery.
      stringify(parsed_discussions_query)
  end

  def parsed_discussions_query
    @parsed_discussions_query ||= Search::Queries::DiscussionQuery.normalize(
      Search::Queries::DiscussionQuery.parse(raw_discussions_search_query, current_user),
    )
  end
  helper_method :parsed_discussions_query

  def raw_discussions_search_query
    params[:discussions_q].presence || "org:#{this_organization}"
  end

  def require_discussions_org_list_feature
    render_404 unless GitHub.flipper[:discussions_org_list].enabled?(this_organization)
  end
end
