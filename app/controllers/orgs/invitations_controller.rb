# frozen_string_literal: true

class Orgs::InvitationsController < Orgs::Controller
  areas_of_responsibility :orgs

  include SeatsHelper
  include CreationSurveyHelper

  statsd_tag_actions only: %i(accept_pending create show_pending)

  include Orgs::InvitationsControllerMethods
  limit_invitation_roles :admin, :direct_member, :reinstate

  # These actions do not require conditional access checks:
  # - opt_out_confirmation: serves GET `/orgs/:org/opt-out, shows a
  #   confirmation page and allows users to confirm their consent to opt-out of
  #   future invitations.
  # - opt_out: serves POST `/orgs/:org/opt-out`, allows a user to opt out of
  #   invitations from an organization, an anti-abuse operation that shouldn't
  #   require the user to SSO to perform (and may not be possible).
  skip_before_action :perform_conditional_access_checks, only: %w(
    opt_out
    opt_out_confirmation
  )
  skip_before_action :cap_pagination, unless: :robot?

  before_action :login_required, except: [:show_pending, :opt_out_confirmation, :opt_out]
  before_action :find_pending_invitation, only: [:accept_pending, :show_pending, :opt_out_confirmation, :opt_out]
  before_action :organization_admin_required, except: [:accept_pending, :show_pending, :destroy, :reinstate_complete, :reinstate_status, :show_reinstated, :opt_out_confirmation, :opt_out]
  before_action :sudo_filter, except: %i(
    accept_pending
    create_for_new_org
    destroy
    invitee_suggestions
    member_adder_add
    opt_out_confirmation
    opt_out
    reinstate_complete
    reinstate_status
    show_pending
    show_reinstated
  )

  before_action :ensure_two_factor_requirement_is_met, only: [:accept_pending]

  include Orgs::Invitations::RateLimiting
  setup_org_invite_rate_limiting only: [:create, :create_for_new_org]

  def show_pending
    allow_external_redirect_after_post if sso_required_for_joining?
    strip_analytics_query_string
    flash.now[:override_octolytics_location] = true

    if logged_in?
      # The view prompts the user to enable 2FA and/or SAML SSO if required
      external_identity_session = current_external_identity_session if external_identity_session_fresh?
      render_template_view "orgs/invitations/show_pending",
                           Orgs::Invitations::ShowPendingPageView,
                           invitation: pending_invitation,
                           invitation_token: params[:invitation_token],
                           current_external_identity_session: external_identity_session
    else
      render_sign_up_via_invitation invitation: pending_invitation, invitation_token: params[:invitation_token]
    end
  end

  def accept_pending
    saml_session_owner = this_organization.external_identity_session_owner
    if !saml_session_owner.saml_sso_requirement_met_by?(current_user) ||
       (pending_invitation.external_identity.present? &&
         saml_session_owner.saml_provider.saml_provisioning_enabled?)
        render_external_identity_session_required(target: saml_session_owner)
        return
    end

    result = pending_invitation.accept(acceptor: current_user,
                              via_email: params[:via_email].present?)

    # If accepting the invitation fails, redirect and show the error to the user.
    if result.error?
      error = result.error
      if result.status == :does_not_meet_team_requirements
        error = error + "Please contact the person who sent you this invitation."
      end
      flash[:error] = error
      return redirect_to org_show_invitation_path
    end

    flash[:notice] = case pending_invitation.role
    when "direct_member"
      "You are now a member of #{this_organization.safe_profile_name}!"
    when "admin"
      "You are now an owner of #{this_organization.safe_profile_name}!"
    end

    if params[:return_to].present?
      safe_redirect_to params[:return_to],
                       fallback: user_path(this_organization)
    elsif pending_invitation.reinstate?
      redirect_to org_reinstate_status_path
    else
      redirect_to user_path(this_organization)
    end
  end

  def opt_out_confirmation
    render_template_view "orgs/invitations/opt_out_confirmation",
      Orgs::Invitations::OptOutConfirmationView,
      invitation: pending_invitation,
      invitation_token: params[:invitation_token]
  end

  def opt_out
    pending_invitation.opt_out(actor: current_user)

    anonymous_flash[:notice] = "You've opted out of receiving invitations from this organization."
    redirect_to user_path(this_organization)
  end

  def reinstate_status
    restorable_organization_user = Restorable::OrganizationUser.restorable(this_organization, current_user)
    status = restorable_organization_user.job_status
    if status
      render "orgs/invitations/reinstate_status", locals: {
        organization: this_organization,
        status: status,
        continue_path: reinstate_return_to_path,
      }
    else
      redirect_to user_path(this_organization)
    end
  end

  def show_reinstated
    status = JobStatus.find(params[:status])
    return head 404 unless status

    # Data not processed yet, so keep polling.
    unless status.finished?
      return head 202
    end

    respond_to do |format|
      format.html do
        render partial: "orgs/invitations/show_reinstated", locals: {
            organization: this_organization,
            continue_path: reinstate_return_to_path,
            layout: false,
          }
      end
    end
  end

  # GET: Safely redirects using the `return_to` parameter to prevent attackers
  # injecting malicious URLs into the restore process.
  # If `return_to` is not supplied we redirect to the Organization root path
  def reinstate_complete
    safe_redirect_to reinstate_return_to_path
  end

  def create
    invitee = User.find_by_id(params[:invitee_id])

    if invitee.blank? && params[:email].blank?
      return render_404
    end

    team_ids = params[:team_ids] || ""
    team_ids = team_ids.split(",") unless team_ids.is_a?(Array)
    team_ids = team_ids.map(&:to_i).uniq

    inviter = OrganizationInviter.new(
      this_organization,
      actor: current_user,
      role: params[:role],
      invitee: invitee,
      email: params[:email],
      team_ids: team_ids,
    )

    if inviter.invite_user
      notice = "You've invited #{inviter.invitation.email_or_invitee_name} to #{this_organization.safe_profile_name}! They'll be receiving an email shortly."
      if inviter.invitation.email?
        if Rails.development?
          # Convenience for developers to see the email invitation accept URL
          # including its hashed token (only visible in this action)
          notice += " #{org_show_invitation_url(this_organization, invitation_token: inviter.invitation.token, via_email: "1")}"
        end
        flash[:notice] = notice
      else
        flash[:notice] = "#{notice} They can also visit #{user_url(this_organization)} to accept the invitation."
      end

      redirect_to org_pending_invitations_path(this_organization)
    else
      flash[:error] = inviter.error
      redirect_to(inviter.return_path)
    end
  rescue OrganizationInviter::RequiresVerification
    render_email_verification_required
  end

  def bulk_create_for_new_org
    if params[:members].present?
      if GitHub.billing_enabled? && !this_organization.plan.legacy? && params[:members].length > this_organization.available_seats
        old_seat_count = this_organization.seats
        seat_delta = params[:members].length - this_organization.available_seats
        seat_change = Billing::PlanChange::SeatChange.new(this_organization, seats: old_seat_count + seat_delta)
        result = GitHub::Billing.change_seats(this_organization, seats: seat_change.seats, seat_delta: seat_delta, actor: current_user)

        if result.success?
          if this_organization.save
            seat_change_ga_label = [:old_seats, :seats].map { |kind|
              seats_volume_bucket(seat_change.send(kind))
            }.uniq.join(" -> ")

            publish_billing_seat_count_change_for(
              actor: current_user,
              user: this_organization,
              old_seat_count: old_seat_count,
              new_seat_count: seat_change.seats,
            )
            analytics_event({
              category: "Orgs",
              action: "upgrade seats",
              label: seat_change_ga_label,
              value: seat_delta,
              redirect: true,
            })
          end
        end
      end

      OrganizationBulkInviteJob.perform_later(current_user, this_organization, params[:members])

      n = params[:members].length
      flash[:notice] = "You've invited #{n} #{"member".pluralize(n)}. They'll receive their invitation #{"email".pluralize(n)} shortly."
    end

    if redirect_to_organization_creation_survey?(this_organization)
      redirect_to new_org_creation_survey_path(this_organization)
    else
      redirect_to org_pending_invitations_path(this_organization)
    end
  end

  def create_for_new_org
    result = invite_member(params[:member])
    case result
    when :email_verification_required
      render status: 403, json: {
        message_html: render_to_string(
          partial: "orgs/invitations/email_verification_required_message",
        ),
      }
    when OrganizationInvitation::NoAvailableSeatsError
      return head 400
    when ::OrganizationInvitation::TradeControlsError
      return render_trade_restricted_invitee
    when OrganizationInvitation::AlreadyAcceptedError, OrganizationInvitation::InvalidError,
           ActiveRecord::RecordInvalid
      return render_404
    else
      invitation, invitee = result
      render json: {
        filled_seats_percent: this_organization.filled_seats_percent,
        selectors: {
          ".unstyled-total-seats" => this_organization.seats,
          ".unstyled-filled-seats" => this_organization.filled_seats,
        },
        list_item_html: render_to_string(partial: "orgs/invitations/invitation_for_new_org",
          locals: {
            user: invitee,
            invitation: invitation,
          },
        ),
      }
    end
  end

  def destroy
    invitation = this_organization.pending_invitations.find(params[:organization_invitation_id])
    return render_404 unless invitation.cancelable_by?(current_user)

    invitation.cancel(actor: current_user)

    if request.xhr?
      render json: {
        filled_seats_percent: this_organization.filled_seats_percent,
        selectors: {
          ".unstyled-total-seats" => this_organization.seats,
          ".unstyled-filled-seats" => this_organization.filled_seats,
        },
      }
    elsif invitation.invitee == current_user
      flash[:notice] = "You've canceled your invitation to #{this_organization.safe_profile_name}."
      redirect_to "/"
    else
      flash[:notice] = "You've canceled #{invitation.email_or_invitee_name}'s invitation to #{this_organization.safe_profile_name}."
      safe_redirect_to params[:return_to], fallback: org_pending_invitations_path(this_organization)
    end
  end

  def edit
    strip_analytics_query_string
    flash.now[:override_octolytics_location] = true
    invitation_request = OrganizationInvitationRequest.new(
      email: params[:email],
      login: params[:invitee_login],
      organization: this_organization,
      current_user: current_user,
      start_fresh: params[:start_fresh],
    )

    if invitation_request.invalid?
      render "orgs/invitations/uninvitable", locals: invitation_request.to_h
    elsif invitation_request.reinstating_membership?
      render_template_view "orgs/invitations/reinstate",
        Orgs::Invitations::ReinstateView.select_view(invitation_request.invitation),
        invitation_request.to_h
    else
      invite_h = invitation_request.to_h
      team_ids  = params[:team_ids] || []
      role = params[:role] || "direct_member"

      existing = invite_h[:existing_invitation]
      if existing
        # do not re-add the invitation's team ids if we are paging through
        # otherwise you can override any deselection of the original teams
        unless request.query_parameters.has_key?(:team_ids)
          if existing.teams
            team_ids = team_ids + existing.teams.pluck(:id).map(&:to_s)
          end

          selected_team = params[:team]
          if selected_team
            team = Team.with_org_name_and_slug(this_organization.login, selected_team)
            team_ids.push(team.id) if team
          end
        end

        # do not update the role if we are paging through
        # otherwise we can override the user selected role
        unless request.query_parameters.has_key?(:role)
          role = existing.role if existing.role
        end
      end

      render_template_view "orgs/invitations/edit",
        Orgs::Invitations::EditPageView,
        invite_h.merge(selected_team: params[:team], page: params[:page], team_ids: team_ids, role: role)
    end
  end

  def invitee_suggestions
    headers["Cache-Control"] = "no-cache, no-store"

    respond_to do |format|
      format.html_fragment do
        render_partial_view "orgs/invitations/invitee_suggestions",
          Orgs::Invitations::InviteeSuggestionsView,
          include_business_orgs: true,
          organization: this_organization,
          exclude_suspended: true,
          query: params[:q]
      end
      format.html do
        render_partial_view "orgs/invitations/invitee_suggestions",
          Orgs::Invitations::InviteeSuggestionsView,
          organization: this_organization,
          include_business_orgs: true,
          exclude_suspended: true,
          query: params[:q]
      end
    end
  end

  def member_adder_add
    identifier = (params[:identifier] ||= "").strip

    return render_404 unless identifier.present?

    if User.valid_email?(identifier)
      redirect_to org_edit_email_invitation_path(this_organization, email: identifier)
    else
      redirect_to org_edit_invitation_path(this_organization, identifier)
    end
  end

  def update
    invitee = params[:email] || User.find_by_login(params[:invitee_login])
    invitation = if invitee.is_a?(String)
      this_organization.pending_invitation_for(email: invitee)
    else
      this_organization.pending_invitation_for(invitee)
    end

    return render_404 if invitation.nil?

    # After a change in how invites work for trade restricted users this will only
    # return true for fully trade restricted organizations. This can likely be removed!
    if invitation.prevented_by_trade_controls_restrictions?
      flash[:error] = TradeControls::Notices::Plaintext.org_invite_restricted
      return redirect_to :back
    end

    if OrganizationInvitation.valid_role?(params[:role])
      previous_role = invitation.role
      updated_role = params[:role]
      if previous_role == updated_role
        GitHub.dogstats.increment("organization_invitation", tags: ["action:update"])
      elsif previous_role == "reinstate"
        GitHub.dogstats.increment("organization_invitation", tags: ["action:convert_to_invite"])
      elsif updated_role == "reinstate"
        GitHub.dogstats.increment("organization_invitation", tags: ["action:convert_to_reinstate"])
      end
      invitation.role = params[:role]
      invitation.save
    end

    unless params[:role].to_sym == :reinstate
      team_ids = params[:team_ids] || ""
      team_ids = team_ids.split(",") unless team_ids.is_a?(Array)
      team_ids = team_ids.map(&:to_i).uniq
      teams = this_organization.teams.where(id: team_ids).distinct

      teams_to_remove = invitation.teams - teams
      teams_to_remove.each { |team| invitation.remove_team(team) }

      teams_to_add = teams - invitation.teams
      teams_to_add.each { |team| invitation.add_team(team, inviter: current_user) }
    end

    flash[:notice] = "You've successfully updated #{invitation.email_or_invitee_name}'s invitation."

    redirect_to org_pending_invitations_path(this_organization)
  end

  private

  def email_verification_required?
    current_user.requires_verification_to_invite_by_email?
  end

  def org_invite_rate_limited
    org_invite_rate_limit_policy.record_rate_limited

    case action_name
    when "create"
      render "orgs/invitations/rate_limited", status: 429, locals: {
        organization: this_organization,
      }
    when "create_for_new_org"
      render status: 429, json: {
        message_html: render_to_string(
          partial: "orgs/invitations/rate_limited_message",
          formats: [:html],
        ),
      }
    end
  end

  def ensure_two_factor_requirement_is_met
    unless this_organization.two_factor_requirement_met_by?(current_user)
      redirect_to org_show_invitation_path
    end
  end

  def reinstate_return_to_path
    params.fetch(:return_to, user_path(this_organization))
  end

  def organization_is_on_free_plan?
    this_organization.uncharged_account?
  end

  def render_trade_restricted_invitee
    render status: 403, json: {
      message_html: render_to_string(
        partial: "orgs/invitations/invitee_trade_restricted",
        formats: [:html],
      ),
    }
  end

  def invite_member(member)
    if User.valid_email?(member)
      return :email_verification_required if email_verification_required?
      email = member
    else
      invitee = User.find_by_login(member)
    end

    return false unless invitee || email

    begin
      invitation = this_organization.invite(invitee, email: email, inviter: current_user, role: :direct_member)
      return invitation, invitee
    rescue OrganizationInvitation::NoAvailableSeatsError, ::OrganizationInvitation::TradeControlsError,
           OrganizationInvitation::AlreadyAcceptedError, OrganizationInvitation::InvalidError,
           ActiveRecord::RecordInvalid => ex
      return ex
    end
  end
end
