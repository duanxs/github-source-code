# frozen_string_literal: true

module Orgs
  module TeamSync
    class OktaCredentialsController < ::Orgs::Controller

      before_action :require_feature
      before_action :require_okta_identity_provider
      before_action :organization_admin_required
      before_action :require_team_sync_tenant, only: [:edit, :update]

      def new
        okta_credentials = ::TeamSync::OktaCredentials.new

        if this_organization.team_sync_tenant.present?
          render "orgs/team_sync/okta_credentials/edit", locals: { organization: this_organization, okta_credentials: okta_credentials }
          return
        end

        render "orgs/team_sync/okta_credentials/new", locals: { organization: this_organization, okta_credentials: okta_credentials }
      end

      def create
        okta_credentials = ::TeamSync::OktaCredentials.new(okta_credentials_params)
        if !okta_credentials.valid?
          flash.now[:error] = "There were errors with your credentials."
          render "orgs/team_sync/okta_credentials/new", locals: { organization: this_organization, okta_credentials: okta_credentials }
          return
        end

        err = team_sync_setup_flow.initiate_setup(provider_type: provider.type, provider_id: provider.id, plain_ssws_token: okta_credentials.ssws_token, url: okta_credentials.url)
        if err
          flash[:error] = Orgs::TeamSyncController::ERROR_MESSAGES[err] % {err: err, provider_type: provider.type}
          render "orgs/team_sync/okta_credentials/new", locals: { organization: this_organization, okta_credentials: okta_credentials }
          return
        end

        team_sync_setup_flow.tenant.update(status: "ready")

        error_key, twirp_error = team_sync_setup_flow.approve
        if error_key
          team_sync_setup_flow.tenant.update(status: "disabled")

          flash.now[:error] = Orgs::TeamSyncController::ERROR_MESSAGES[error_key] % { err: twirp_error.msg }
          render "orgs/team_sync/okta_credentials/new", locals: { organization: this_organization, okta_credentials: okta_credentials }
          return
        end

        flash[:notice] = "Team synchronization setup has been enabled"
        redirect_to settings_org_security_url(this_organization)
      end

      def edit
        okta_credentials = ::TeamSync::OktaCredentials.new(url: @tenant.url, ssws_token: @tenant.plain_ssws_token)

        render "orgs/team_sync/okta_credentials/edit", locals: { organization: this_organization, okta_credentials: okta_credentials }
      end

      def update
        okta_credentials = ::TeamSync::OktaCredentials.new(okta_credentials_params)
        if !okta_credentials.valid?
          flash.now[:error] = "There were errors with your credentials."
          render "orgs/team_sync/okta_credentials/edit", locals: { organization: this_organization, okta_credentials: okta_credentials }
          return
        end

        if @tenant.update(plain_ssws_token: okta_credentials.ssws_token, url: okta_credentials.url, status: "enabled")
          err = @tenant.register

          if err
            flash.now[:error] = Orgs::TeamSyncController::ERROR_MESSAGES[err] % {err: err, provider_type: provider.type}
            render "orgs/team_sync/okta_credentials/edit", locals: { organization: this_organization, okta_credentials: okta_credentials }
            return
          end
        else
          flash.now[:error] = "There were errors with your credentials."
          render "orgs/team_sync/okta_credentials/edit", locals: { organization: this_organization, okta_credentials: okta_credentials }
          return
        end

        flash[:notice] = "Team synchronization credentials have been updated"
        redirect_to settings_org_security_url(this_organization)
      end

      private

      def okta_credentials_params
        params.require(:team_sync_okta_credentials).permit(:ssws_token, :url)
      end

      def provider
        @provider ||= ::TeamSync::Provider.detect(issuer: this_organization.external_identity_session_owner.saml_provider.issuer)
      end

      def require_okta_identity_provider
        unless provider&.okta?
          flash[:error] = Orgs::TeamSyncController::ERROR_MESSAGES[:invalid_provider_type]
          redirect_to settings_org_security_url(this_organization)
        end
      end

      def require_team_sync_tenant
        @tenant = this_organization.team_sync_tenant
        if @tenant.nil?
          flash[:error] = "The tenant for this organization does not exist."
          render "orgs/team_sync/okta_credentials/new", locals: { organization: this_organization, okta_credentials: ::TeamSync::OktaCredentials.new }
          return
        end
        @tenant
      end

      def team_sync_setup_flow
        @team_sync_setup_flow ||= ::TeamSync::SetupFlow.new(organization: this_organization, actor: current_user)
      end

      def require_feature
        render_404 unless GitHub.flipper[:okta_team_sync].enabled?(this_organization)
      end
    end
  end
end
