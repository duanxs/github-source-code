# frozen_string_literal: true

module Orgs
  module TeamSync
    class GroupMappingsController< ::Orgs::Controller

      include AnalyticsScrubberMethods

      before_action :login_required
      before_action :organization_read_required
      before_action :admin_on_team_required
      before_action :this_team_required

      def list_group_mappings
        tenant = this_organization.team_sync_tenant
        raise ::TeamSync::Tenant::InvalidStatusError, "Tenant not enabled" unless tenant&.team_sync_enabled?
        response = GroupSyncer.client.list_team_mappings(org_id: tenant.organization.global_relay_id, team_id: this_team.global_relay_id)

        if response.error.present?
          GitHub.dogstats.increment("team_sync.list_team_mappings", tags: [
            "result:error", "error:#{response.error.code}", "twirp_service:team_mappings"
          ])
          err = ::TeamSync::ServiceResponseError.new("[#{response.error.code}] #{response.error.msg}: #{response.error.meta}")
          err.set_backtrace(caller)
          Failbot.report!(err)
          render json: {msg: "Error loading status. Try again."}, status: 500
          return
        end

        mappings = response.data.mappings

        if mapping = mappings.first
          status = Team::GroupMapping::STATUS_MAP.key(mapping.status)
          synced_at = Time.at(mapping.last_sync.seconds, mapping.last_sync.nanos) if mapping.last_sync.present?
        end

        respond_to do |format|
          format.html do
            render partial: "orgs/team_sync/team_mappings/group_mappings", locals: { mappings: mappings, status: status, synced_at: synced_at }
          end
          format.html_fragment do
            render partial: "orgs/team_sync/team_mappings/group_mappings", formats: :html, locals: { mappings: mappings, status: status, synced_at: synced_at }
          end
        end
      end
    end
  end
end
