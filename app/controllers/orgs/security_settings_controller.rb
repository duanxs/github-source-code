# frozen_string_literal: true

module Orgs
  class SecuritySettingsController < Controller
    before_action :organization_admin_required
    before_action :sudo_filter, except: %i(index)
    before_action :ensure_trade_restrictions_allows_org_settings_access

    javascript_bundle :settings

    SamlProviderQuery = parse_query <<-'GRAPHQL'
      query($id: ID!, $teamSyncEnabled: Boolean!) {
        node(id: $id) {
          ...Orgs::SecuritySettings::IndexView::SamlProviderFragment
        }
      }
    GRAPHQL

    def index
      if this_organization.business_plus?
        render_template_view "orgs/security_settings/index",
          Orgs::SecuritySettings::IndexView,
          organization: this_organization,
          business: this_organization.business,
          current_user: current_user,
          saml_provider: saml_provider,
          current_external_identity: current_external_identity(target: this_organization),
          team_sync_setup_flow: ::TeamSync::SetupFlow.new(organization: this_organization, actor: current_user)
      else
        render_template_view "orgs/security_settings/index",
          Orgs::SecuritySettings::IndexView,
          organization: this_organization
      end
    end

    def ssh_cert_requirement
      if params[:enable_ssh_cert_requirement] == "on"
        this_organization.enable_ssh_certificate_requirement(current_user)
        flash[:notice] = "SSH certificate requirement enabled"
      else
        this_organization.disable_ssh_certificate_requirement(current_user)
        flash[:notice] = "SSH certificate requirement disabled"
      end

      redirect_to settings_org_security_path(this_organization)
    end

    def update_ip_whitelisting_enabled
      if params[:enable_ip_whitelisting] == "on"
        begin
          this_organization.enable_ip_whitelisting \
            actor: current_user, actor_ip: request.remote_ip
          flash[:notice] = "IP allow list enabled."
        rescue Configurable::IpWhitelistingEnabled::ActorLockoutError => error
          flash[:error] = error.message
        end
      else
        this_organization.disable_ip_whitelisting(actor: current_user)
        flash[:notice] = "IP allow list disabled."
      end
      redirect_to settings_org_security_path(this_organization)
    end

    def update_ip_whitelisting_app_access_enabled
      if params[:enable_ip_whitelisting_app_access] == "on"
        this_organization.enable_ip_whitelisting_app_access(actor: current_user)
        flash[:notice] = "IP allow list app access enabled."
      else
        this_organization.disable_ip_whitelisting_app_access(actor: current_user)
        flash[:notice] = "IP allow list app access disabled."
      end
      redirect_to settings_org_security_path(this_organization)
    end

    def update_automated_security_fixes_opt_out
      if params[:automated_security_fixes_opt_out] == "on"
        this_organization.disable_vulnerability_updates(actor: current_user)

        ::Dependabot::DisableOwnerRepositoriesJob.perform_later(
          this_organization.id, current_user.id
        )

        flash[:notice] = "Opted out of Dependabot security updates."
      else
        this_organization.enable_vulnerability_updates(actor: current_user)
        flash[:notice] = "Opted in to Dependabot security updates."
      end
      redirect_to settings_org_security_path(this_organization)
    end

    private

    def saml_provider
      @saml_provider ||= if flash[:saml_test_result]
        provider = Organization::SamlProviderTestSettings.most_recent_for(
          user: current_user,
          org: this_organization,
          result: flash[:saml_test_result],
        )
        ActiveRecord::Base.connected_to(role: :writing) do
          provider.save!
        end
        provider
      else
        platform_execute(SamlProviderQuery, variables: {
          id: this_organization.global_relay_id,
          teamSyncEnabled: this_organization.team_sync_enabled?,
        }).node
      end
    end
  end
end
