# frozen_string_literal: true

class Orgs::OauthApplicationTransfersController < Orgs::Controller
  areas_of_responsibility :platform

  include OauthApplicationTransfersControllerMethods

  before_action :organization_admin_required, except: [:destroy]

  javascript_bundle :settings

  private

  def current_context
    this_organization
  end
end
