# frozen_string_literal: true

# Internal: A base class for orgs controllers. This class provides a bunch of
# standard filter methods, plus a `this_organization` accessor.
#
# Beyond making general filters available, this controller requires a logged-in
# user and a valid `this_organization` by default.
class Orgs::Controller < ApplicationController
  # Gotta have an org to do org stuff.
  before_action :this_organization_required
  before_action :ensure_visible_to_current_user

  protected

  ################################
  # Organization helpers/filters #
  ################################

  # Internal: Gets the organization we're operating inside based on an
  # `:org` param.
  #
  # Returns an Organization or nil if the org isn't found.
  def this_organization
    return @this_organization if defined? @this_organization
    @this_organization = Organization.find_by_login(params[:org] || params[:organization_id])
  end
  helper_method :this_organization

  def target_for_conditional_access
    return :no_target_for_conditional_access unless this_organization
    this_organization
  end

  # Internal: This before_action renders a standard 404 page if
  # `this_organization` is nil.
  #
  # Returns nothing.
  def this_organization_required
    render_404 if this_organization.nil?
  end

  # Internal: This before_action renders a standard 404 page if
  # `this_organization` was deleted.
  #
  # Returns nothing.
  def ensure_organization_exists
    render_404 if this_organization.try :deleted?
  end

  # Internal: This before_action renders a standard 404 page unless
  # `current_user` is capable of reading `this_organization`.
  #
  # Returns nothing.
  def organization_read_required
    if this_organization.nil? || !this_organization.direct_or_team_member?(current_user)
      render_404
    end
  end

  # Internal: This before_action renders a standard 404 page unless
  # `current_user` is capable of reading `this_organization` or is an outside
  # collaborator on `this_organization`.
  #
  # Returns nothing.
  def organization_read_or_outside_collaborator_required
    if this_organization.nil? ||
        !this_organization.direct_or_team_member?(current_user) &&
        !organization_outside_collaborators?
      render_404
    end
  end

  def organization_outside_collaborators?
    logged_in? && this_organization.user_is_outside_collaborator?(current_user.id)
  end

  # Internal: This before_action renders a standard 404 page unless
  # `current_user` is capable of administering `this_organization`.
  #
  # Returns nothing.
  def organization_admin_required
    if this_organization.nil? || !this_organization.adminable_by?(current_user)
      render_404
    end
  end

  # Internal: This before_action renders a 404 if the organization is marked
  # as spammy (unless the user who created it is the viewer)
  def ensure_visible_to_current_user
    if this_organization.present? && this_organization.hide_from_user?(current_user)
      render_404
    end
  end

  def invite_rate_limited_organization
    this_organization
  end

  ########################
  # Team helpers/filters #
  ########################

  # Internal: Gets the team we're operating inside based on a `:team_slug`
  # param.
  #
  # Returns a Team or nil if the team isn't found.
  def this_team
    return @this_team if defined? @this_team

    slug = params[:team_slug]
    return if slug.blank?

    @this_team = this_organization.
      find_team_by_slug(slug, visible_to: current_user)
  end

  # Internal: This before_action renders a standard 404 page if `this_team` is
  # nil.
  #
  # Returns nothing.
  def this_team_required
    render_404 if this_team.nil?
  end

  # Internal: This before_action renders a standard 404 page unless
  # `current_user` is capable of administering `this_team`.
  def admin_on_team_required
    if this_team.nil? || !this_team.adminable_by?(current_user)
      render_404
    end
  end

  # Internal: This before_action renders a 404 unless current_user can create
  # teams on the organization
  def organization_team_creation_required
    render_404 unless this_organization.can_create_team?(current_user)
  end

  def initialize_hydro_context
    super

    if hydro_context[:enabled] && this_organization
      hydro_context.merge!({
        current_org: this_organization.name,
        current_org_id: this_organization.id,
      })
    end
  end
end
