# frozen_string_literal: true

class Orgs::TopicsController < Orgs::Controller

  before_action :login_required, except: :most_used
  before_action :ensure_trade_restrictions_allows_org_settings_access, unless: :xhr?, except: :most_used

  def most_used
    respond_to do |format|
      format.html_fragment do
        render_partial_view "orgs/topics/most_used",
          Orgs::Repositories::IndexPageView,
          organization: this_organization,
          current_page: current_page,
          type_filter: params[:type],
          phrase: params[:q],
          language: params[:language]
      end
    end
  end

  IndexQuery = parse_query <<-'GRAPHQL'
    query($login: String!, $pageSize: Int!, $cursor: String) {
      viewer {
        organization(login: $login) {
          ...Views::Orgs::Topics::Index::Organization
        }
      }
    }
  GRAPHQL

  ListQuery = parse_query <<-'GRAPHQL'
    query($login: String!, $pageSize: Int!, $cursor: String) {
      viewer {
        organization(login: $login) {
          ...Views::Orgs::Topics::List::Organization
        }
      }
    }
  GRAPHQL

  def index
    query = xhr? ? ListQuery : IndexQuery
    data = platform_execute(query, variables: {
      login: params[:org] || params[:organization_id],
      cursor: params[:cursor],
      pageSize: 15,
    })

    unless org = data.viewer.organization
      return head :not_found if request.xhr?
      return render_404
    end

    respond_to do |format|
      format.html do
        if xhr?
          render partial: "orgs/topics/list", locals: { organization: org }
        else
          render "orgs/topics/index", locals: { organization: org }
        end
      end
    end
  end

  private

  def xhr?
    request.xhr? && !pjax?
  end
end
