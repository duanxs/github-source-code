# frozen_string_literal: true

class Orgs::UserBlocksController < Orgs::Controller
  areas_of_responsibility :orgs

  before_action :login_required
  before_action :organization_admin_required
  before_action :find_target_user, except: [:suggestions]

  def create
    block = create_block

    respond_to do |format|
      format.html do
        if request.xhr?
          render partial: "settings/user_blocks/blocked_users",
            locals: { user_ignore: block }
        else
          redirect_to settings_org_block_users_path(this_organization)
        end
      end
    end
  end

  def create_and_notify
    # validate that the hidden reason is valid if the user selects to hide comments
    if params["hide_comments"] == "on" &&
      !Platform::Enums::ReportedContentClassifiers.values.keys.include?(params["hidden_reason"])
        flash[:error] = "The user has not been blocked. Please specify a valid reason to hide this content."
    else
      block = create_block

      if block.valid?
        notice = "#{@user.login} has been blocked from the #{this_organization.name} organization"
        notice += " for #{@duration} #{"day".pluralize(@duration)}" if @duration

        if params["send_notification"] == "on"
          notice += " and will receive an email notification."
        else
          notice += "."
        end

        # If the user has selected to minimize all comments enqueue the job
        if params["hide_comments"] == "on"
          notice += " Their comments have been hidden as #{Platform::Enums::ReportedContentClassifiers.values[params["hidden_reason"]].value}."
          MinimizeAllCommentsOnRepoJob.perform_later(this_organization, @user, current_user, params[:hidden_reason])
        end

        flash[:notice] = notice
      else
        flash[:error] = "There was a problem blocking this user from this organization."
      end
    end

    redirect_to :back
  end

  def destroy
    if @user
      this_organization.unblock @user, actor: current_user
      flash[:notice] = "User has been successfully unblocked from this organization."
    end

    redirect_to :back
  end

  def suggestions
    search_term = params[:q]

    headers["Cache-Control"] = "no-cache, no-store"

    blockable_users = User.search(search_term, limit: 10).select do |user|
      this_organization.can_block(user)
    end

    respond_to do |format|
      format.html_fragment do
        render partial: "settings/user_blocks/suggestions", formats: :html, locals: {
          suggestions: blockable_users,
        }
      end
      format.html do
        render partial: "settings/user_blocks/suggestions", locals: {
          suggestions: blockable_users,
        }
      end
    end
  end

  private

  # Private: find the target use to be blocked / unblocked
  def find_target_user
    @user = User.find_by_login(params[:login])
  end

  # Private: create a block for the given parameters
  def create_block
    @duration = params[:duration].to_i > 0 ? params[:duration].to_i : nil
    blocked_from_content = typed_object_from_id([Platform::Interfaces::Blockable],
                                                 params[:content_id]) if params[:content_id]

    this_organization.block(@user, actor: current_user,
                                   duration: @duration,
                                   blocked_from_content: blocked_from_content,
                                   send_notification: params[:send_notification] == "on",
                                   minimize_reason: params[:hidden_reason])
  end
end
