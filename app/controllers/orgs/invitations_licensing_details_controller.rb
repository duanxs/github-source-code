# frozen_string_literal: true

class Orgs::InvitationsLicensingDetailsController < Orgs::Controller
  areas_of_responsibility :gitcoin

  before_action :login_required
  before_action :organization_admin_required

  def show
    render "orgs/invitations_licensing_details/show",
      locals: { organization: this_organization, remaining_seats_or_licenses: this_organization.available_seats },
      layout: false
  end
end
