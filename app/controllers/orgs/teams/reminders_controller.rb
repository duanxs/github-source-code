# frozen_string_literal: true

class Orgs::Teams::RemindersController < Orgs::Controller
  include RemindersMethods

  areas_of_responsibility :ce_extensibility

  statsd_tag_actions only: [:create, :update, :index]

  before_action :this_team_required
  before_action :admin_on_team_required
  before_action :set_reminder!, only: [:show, :update, :destroy, :reminder_test]

  before_action { @selected_link = :reminders }

  TeamSettingsQuery = parse_query <<-'GRAPHQL'
    query($login: String!, $query: String, $slug: String!) {
      organization(login: $login) {
        ...Orgs::Teams::FormView::OrgFragment
        ...Views::Layouts::Team::Organization
        ...Views::Orgs::Teams::ParentSearch::Organization
        team(slug: $slug) {
          outboundParentRequests: pendingTeamChangeParentRequests(first: 1, direction: OUTBOUND_CHILD_INITIATED) {
            edges {
              node {
                requestedTeam {
                  name
                }
              }
            }
          }
          parentTeam
          ...Orgs::Teams::FormView::TeamFragment
          ...Views::Orgs::Teams::Edit::TeamFragment
          ...Views::Orgs::Teams::ReviewAssignment::TeamFragment
        }
      }
    }
  GRAPHQL

  def index
    org_data = platform_execute(TeamSettingsQuery,
      variables: {
        "login" => params[:org] || params[:organization_id],
        "slug" => params[:team_slug],
      },
    )

    render(
      "orgs/teams/reminders/index",
      locals: {
        graphql_org: org_data.organization,
        selected_nav_item: :settings,
        team: org_data.organization.team,
        locked_to_team: this_team,
        slack_workspaces: slack_workspaces,
        organization: this_organization,
      },
      layout: "team")
  end

  # The rest of the controller methods are handled in `RemindersMethods`

  private

  def render_edit(reminder)
    org_data = platform_execute(TeamSettingsQuery,
      variables: {
        "login" => params[:org] || params[:organization_id],
        "slug" => params[:team_slug],
      },
    )

    render(
      "orgs/teams/reminders/edit",
      locals: {
        graphql_org: org_data.organization,
        selected_nav_item: :settings,
        team: org_data.organization.team,
        slack_workspaces: slack_workspaces,
        reminder: reminder,
        locked_to_team: this_team,
      },
      layout: "team")
  end

  def scope_reminders(reminder)
    case reminder
    when Hash, ActionController::Parameters
      reminder["team_ids"] = [this_team.id]
      reminder
    when ActiveRecord::Relation
      reminder_ids = ReminderTeamMembership.where(team: this_team).pluck(:reminder_id)
      reminder.where(id: reminder_ids)
    when Reminder
      if reminder.new_record?
        reminder.teams = [this_team]
        return reminder
      end
      return nil unless reminder.team_ids == [this_team.id]
      reminder
    end
  end
end
