# frozen_string_literal: true

class ChecksStatusesController < AbstractRepositoryController
  areas_of_responsibility :checks

  CONDITIONAL_ACCESS_BYPASS_PUBLIC_REPO_ACTIONS = %w(rollup).freeze

  StatusCheckRollupQuery = parse_query <<-'GRAPHQL'
    query($id: ID!) {
      node(id: $id) {
        ... on Commit {
          statusCheckRollup {
            ...Views::Statuses::Icon::Status
          }
        }
      }
    }
  GRAPHQL

  def rollup
    begin
      commit = current_repository.commits.find(params[:ref])
    rescue GitRPC::ObjectMissing
      render_404 and return
    end

    graphql_commit = platform_execute(StatusCheckRollupQuery, variables: { id: commit.global_relay_id }).node
    # A race condition can happen where a commit is removed, renamed, etc
    # Instead of throwing a 500, let's return a 404 which is what happens when the servers catch up and reconcile
    render_404 and return if graphql_commit.nil?
    return head :ok unless graphql_commit.status_check_rollup

    # The icon view adds the default "e", but this prevents potential confusion by defining it here too
    direction = ::DropdownHelper::DROPDOWN_DIRECTIONS.key?(params[:direction]&.to_sym) ? params[:direction] : "e"

    respond_to do |format|
      format.html_fragment do
        render partial: "statuses/icon", formats: :html, locals: {
          status: graphql_commit.status_check_rollup,
          dropdown_direction: direction,
        }
      end
      format.html do
        render partial: "statuses/icon", formats: :html, locals: {
          status: graphql_commit.status_check_rollup,
          dropdown_direction: direction,
        }
      end
    end
  end

  private

  # Bypass conditional access requirements for actions specified in
  # CONDITIONAL_ACCESS_BYPASS_PUBLIC_REPO_ACTIONS.
  def require_conditional_access_checks?
    return false if conditional_access_exempt?
    super
  end
end
