# frozen_string_literal: true

class MarketplacePurchasesController < ApplicationController
  areas_of_responsibility :marketplace

  statsd_tag_actions only: :show

  before_action :marketplace_required
  before_action :login_required, except: :preview
  before_action :require_xhr, only: :preview
  before_action :sign_end_user_agreement, only: [:create, :update]

  layout "marketplace"

  before_action :add_csp_exceptions, only: :new
  # For editing billing information within a modal
  CSP_EXCEPTIONS = {
    img_src: [GitHub.paypal_checkout_url].freeze,
    connect_src: [GitHub.braintreegateway_url, GitHub.braintree_analytics_url].freeze,
    frame_src: [GitHub.zuora_payment_page_server].freeze,
  }.freeze

  DEFAULT_UNIT_QUANTITY = 1
  MAX_UNIT_QUANTITY     = 100_000

  DeletePendingQuery = parse_query <<-'GRAPHQL'
    mutation($id: ID!) {
      deleteMarketplaceOrderPreview(input: {orderPreviewId: $id}) {
        isDestroyed
      }
    }
  GRAPHQL

  def delete_pending
    data = platform_execute(DeletePendingQuery, variables: { id: params[:id] })

    if data.errors.any?
      flash[:error] = "Error removing the pending order. #{data.errors.messages.values.join(", ")}."
    end

    redirect_to marketplace_pending_orders_path
  end

  SelectedPlanQuery = parse_query <<-'GRAPHQL'
    query($planId: ID!) {
      selectedPlan: node(id: $planId) {
        ... on MarketplaceListingPlan {
          databaseId
          isPerUnit
          listing {
            slug
          }
        }
        ...Views::MarketplacePurchases::New::MarketplaceListingPlan
        ...MarketplacePurchases::OrderPreviewView::Query::MarketplaceListingPlan
      }
    }
  GRAPHQL

  NewQuery = parse_query <<-'GRAPHQL'
    query($listingSlug: String!, $planId: ID!, $quantity: Int!, $orgLogin: String!) {
      enabledFeatures
      marketplaceListing(slug: $listingSlug) {
        databaseId
        isArchived
        name
        slug
        viewerIsListingAdmin
        app {
          id
        }
        ...Views::MarketplacePurchases::New::MarketplaceListing
      }
      viewer {
        login
        databaseId
        subscription {
          subscriptionItem(listingSlug: $listingSlug, active: true) {
            viewerCanAdmin
            quantity
          }
        }
        ownedOrganization(login: $orgLogin) {
          login
          databaseId
          subscription {
            subscriptionItem(listingSlug: $listingSlug, active: true) {
              viewerCanAdmin
              quantity
            }
          }
          ...Views::MarketplacePurchases::New::Organization
        }
        ...Views::MarketplacePurchases::New::User
      }
      ...Views::MarketplacePurchases::New::Root
    }
  GRAPHQL

  def new
    selected_plan = platform_execute(SelectedPlanQuery, variables: {
      planId: params[:plan_id],
    }).selected_plan

    return render_404 unless selected_plan &&
      selected_plan.is_a?(PlatformTypes::MarketplaceListingPlan) &&
      selected_plan.listing.slug == params[:listing_slug]

    selected_plan_quantity = unit_quantity(selected_plan)

    # n.b.: `account` parameter is optional,
    #        but GraphQL typecheck won't allow nil here.
    #        Empty string will return a nil account.
    data = platform_execute(NewQuery, variables: {
      listingSlug: params[:listing_slug],
      planId: params[:plan_id],
      orgLogin: params[:account].to_s,
      quantity: selected_plan_quantity,
    })

    return render_404 unless data.marketplace_listing
    set_setup_state_cookie(data.marketplace_listing) unless pjax?

    if data.marketplace_listing.is_archived?
      if current_user_can_admin_listing?(data.marketplace_listing)
        return redirect_to_listing_with_delisted_error(data.marketplace_listing)
      end

      return redirect_to_billing_settings_with_delisted_error(
        marketplace_listing: data.marketplace_listing,
        subscription_item: data.viewer.subscription.subscription_item,
      )
    end

    viewer = data.viewer
    org = viewer.owned_organization
    account = org || viewer
    quantity = current_subscription_quantity(account) || selected_plan_quantity

    # User is trying to modify an org's subscription they don't have permission for:
    if account.subscription.subscription_item && !account.subscription.subscription_item.viewer_can_admin?
      return redirect_to_listing_with_permission_error(data.marketplace_listing)
    end

    # User is trying to purchase on behalf of an org for which they don't have permission:
    unless viewer_can_purchase_on_behalf_of_account?(account)
      return redirect_to_listing_with_permission_error(data.marketplace_listing)
    end

    # Update the order preview/cart data in the background
    UpdateMarketplaceOrderPreviewJob.perform_later(viewer.database_id, data.marketplace_listing.database_id, account.database_id, selected_plan.database_id, quantity, Time.now.to_i)

    strip_analytics_query_string
    render "marketplace_purchases/new", locals: {
      listing: data.marketplace_listing,
      selected_plan: selected_plan,
      viewer: viewer,
      organization: org,
      quantity: quantity,
      data: data,
    }
  end

  IntegratorUpgradeQuery = parse_query <<-'GRAPHQL'
    query($listingSlug: String!, $accountId: Int!, $first: Int!) {
      marketplaceListing(slug: $listingSlug) {
        slug
        plans(first: $first, published: true) {
          nodes {
            id
            number
          }
        }
      }
      account(accountId: $accountId) {
        ... on User {
          login
        }
        ... on Organization {
          login
        }
      }
      viewer {
        login
      }
    }
  GRAPHQL

  def integrator_upgrade
    data = platform_execute(IntegratorUpgradeQuery, variables: {
      listingSlug: params[:listing_slug],
      accountId: params[:account_id].to_i,
      first: Marketplace::ListingPlan::PLAN_LIMIT_PER_LISTING,
    })

    return render_404 unless listing = data.marketplace_listing

    plan = listing.plans.nodes.detect { |plan| plan.number == params[:plan_number].to_i }

    return render_404 unless plan

    redirect_params = {
      listing_slug: listing.slug,
      plan_id: plan.id,
    }
    account = data.account&.login
    redirect_params[:account] = account unless account == data.viewer.login

    redirect_to marketplace_order_path(redirect_params)
  end

  OrderPreviewQuery = parse_query <<-'GRAPHQL'
    query($listingSlug: String!, $planId: ID!, $quantity: Int!, $account: String!) {
      marketplaceListing(slug: $listingSlug) {
        databaseId
        ...MarketplacePurchases::OrderPreviewView::Query::MarketplaceListing
      }
      viewer {
        databaseId
        login
        ownedOrganization(login: $account) {
          databaseId
          login
          ...MarketplacePurchases::OrderPreviewView::Query::Organization
        }
        ...MarketplacePurchases::OrderPreviewView::Query::User
      }
      ...MarketplacePurchases::OrderPreviewView::Query::Root
    }
  GRAPHQL

  def preview
    return head :unauthorized unless logged_in?

    selected_plan = platform_execute(SelectedPlanQuery, variables: {
      planId: params[:plan_id],
    }).selected_plan

    return render_404 unless selected_plan

    quantity = unit_quantity(selected_plan)

    data = platform_execute(OrderPreviewQuery, variables: {
      listingSlug: params[:listing_slug],
      planId: params[:plan_id],
      account: params[:account] || "",
      quantity: quantity,
    })

    listing = data.marketplace_listing

    return render_404 unless listing

    viewer = data.viewer
    org = viewer.owned_organization
    account = org || viewer

    # User is trying to purchase on behalf of an org for which they don't have permission:
    return render_404 unless viewer_can_purchase_on_behalf_of_account?(account)

    # Update the order preview/cart data in the background
    UpdateMarketplaceOrderPreviewJob.perform_later(viewer.database_id, listing.database_id, account.database_id, selected_plan.database_id, quantity, Time.now.to_i)

    respond_to do |format|
      format.html do
        render_partial_view("marketplace_purchases/order_preview", MarketplacePurchases::OrderPreviewView, {
          listing: listing,
          selected_plan: selected_plan,
          viewer: viewer,
          organization: org,
          quantity: quantity,
          data: data,
        })
      end
    end
  end

  CreateQuery = parse_query <<-'GRAPHQL'
    mutation($input: CreateSubscriptionItemInput!) {
      createSubscriptionItem(input: $input) {
        subscriptionItem {
          databaseId
          id
          quantity
          account {
            ... on User {
              databaseId
            }
            ... on Organization {
              databaseId
            }
          }

          marketplaceListingPlan: subscribable {
            ... on MarketplaceListingPlan {
              databaseId

              listing {
                databaseId
                app {
                  slug
                }
                integrationType
                slug
              }
            }
          }
        }
      }
    }
  GRAPHQL

  def create
    return render_404 unless logged_in?

    input = {
      planId: params[:plan_id],
      quantity: params[:quantity].to_i,
      account: params[:account],
      grantOap: params[:grant_oap].present?,
    }
    data = platform_execute(CreateQuery, variables: { input: input })

    if data.errors.any?
      flash[:error] = data.errors.messages.values.join(", ")
      return redirect_to :back
    end

    subscription_item = data.create_subscription_item.subscription_item
    listing_plan = subscription_item.marketplace_listing_plan
    listing = listing_plan.listing

    session[:last_click_marketplace_listing_id] = subscription_item.marketplace_listing_plan.
      listing.database_id
    redirect_url = install_marketplace_listing_path(listing.slug, subscription_item.id)

    GlobalInstrumenter.instrument("marketplace.listing_install", {
      user: current_user,
      listing_id: listing.database_id,
      listing_plan_id: listing_plan.database_id,
      quantity: subscription_item.quantity,
    })

    render "marketplace_listings/meta_refresh_redirect", locals: { redirect_url: redirect_url }, layout: "redirect"
  end

  UpdateQuery = parse_query <<-'GRAPHQL'
    mutation($input: UpdateSubscriptionItemInput!) {
      updateSubscriptionItem(input: $input) {
        __typename # a selection is grammatically necessary here
      }
    }
  GRAPHQL

  def update
    return render_404 unless logged_in?

    input = {
      planId: params[:plan_id],
      quantity: params[:quantity].to_i,
      account: params[:account],
      grantOap: params[:grant_oap].present?,
    }
    data = platform_execute(UpdateQuery, variables: { input: input })

    if data.errors.any?
      error_type = data.errors.details[:updateSubscriptionItem]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      flash[:error] = data.errors.messages.values.join(", ")
      return redirect_to :back
    end

    if current_user.login == params[:account]
      redirect_to settings_user_billing_url.to_s
    else
      redirect_to settings_org_billing_url(params[:account])
    end
  end

  private

  def target_for_conditional_access
    # security issue: conditional access enforcement can be bypassed in integrator_upgrade
    # controller action by injecting params[:account] with an organization ID that does not enforce
    # conditional access
    Organization.find_by_login(params[:account]) || Organization.find_by_id(params[:account_id]) || :no_target_for_conditional_access
  end

  SignEndUserAgreementQuery = parse_query <<-'GRAPHQL'
    mutation($input: SignMarketplaceAgreementInput!) {
      signMarketplaceAgreement(input: $input) {
        marketplaceListing
      }
    }
  GRAPHQL

  def sign_end_user_agreement
    return if params[:marketplace_agreement_id].blank?

    input = { listingID: params[:marketplace_listing_id],
              agreementID: params[:marketplace_agreement_id] }
    data = platform_execute(SignEndUserAgreementQuery, variables: { input: input })

    if data.errors.any?
      flash[:error] = "Could not sign agreement: #{data.errors.messages.values.join(", ")}"
      redirect_to marketplace_listing_path(params[:listing_slug])
    end
  end

  def current_user_can_admin_listing?(marketplace_listing)
    logged_in? && (marketplace_listing.viewer_is_listing_admin? ||
      current_user.can_admin_marketplace_listings?)
  end

  def unit_quantity(selected_plan)
    quantity = [params[:quantity].to_i.abs, MAX_UNIT_QUANTITY].min

    if selected_plan.is_per_unit? && quantity > 0
      quantity
    else
      DEFAULT_UNIT_QUANTITY
    end
  end

  def set_setup_state_cookie(marketplace_listing)
    if params[:state] && marketplace_listing.app
      IntegrationInstallation::SetupStateCookie.create(
        cookie_jar: cookies,
        data: {state: params[:state]},
        integration_id: marketplace_listing.app.id,
        target_id: params[:suggested_target_id],
      )
    end
  end

  def redirect_to_listing_with_permission_error(marketplace_listing)
    flash[:error] = "You do not have permission to set up plans for the specified account."
    redirect_to marketplace_listing_path(marketplace_listing.slug)
  end

  def redirect_to_listing_with_delisted_error(marketplace_listing)
    flash[:error] = "#{marketplace_listing.name} has been removed from GitHub Marketplace."
    redirect_to marketplace_listing_path(marketplace_listing.slug)
  end

  def redirect_to_billing_settings_with_delisted_error(marketplace_listing:, subscription_item:)
    if subscription_item.present?
      flash[:error] = "#{marketplace_listing.name} has been removed from GitHub Marketplace. You can cancel your subscription below."
    else
      flash[:error] = "#{marketplace_listing.name} has been removed from GitHub Marketplace. You can cancel your subscription by navigating to your organization's billing settings."
    end

    redirect_to settings_user_billing_path
  end

  # Ensures the given user/org matches the 'account' URL parameter, if any such URL parameter
  # was set.
  def viewer_can_purchase_on_behalf_of_account?(account)
    return true if params[:account].blank?

    account.login.downcase == params[:account].to_s.downcase
  end

  def current_subscription_quantity(account)
    if account.subscription && account.subscription.subscription_item
      account.subscription.subscription_item.quantity
    end
  end
end
