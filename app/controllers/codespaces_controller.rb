# frozen_string_literal: true

class CodespacesController < ApplicationController
  include ApplicationController::CodespaceDependency

  # The VSCS workbench accepts the following params so we should pass them
  # through to it if we get them.
  ALLOWED_VSCS_PARAMS = %w(ew folder workspace payload dogfoodChannel).freeze

  with_options except: [:index, :new, :repository_select, :ref_select, :create, :toggle_dev_flags] do
    before_action :require_codespace
    before_action :verify_authorization
    before_action :set_codespace_context
  end
  before_action :require_pullable_repo, except: [:index, :new, :repository_select, :create, :destroy, :show, :provisioned, :toggle_dev_flags]
  before_action :exclude_firefox, except: [:index, :destroy, :toggle_dev_flags]

  statsd_tag_actions

  def index
    if request.xhr?
      render_codespaces_list_component
    else
      render "codespaces/index", locals: {
        codespaces: codespaces_for_dashboard,
        unpushed_id: params[:unpushed_id],
        unauthorized_saml_targets: unauthorized_saml_targets,
      }
    end
  end

  def new
    if current_repo
      ref = current_repo.refs.find(params[:ref]) if params[:ref].present?
      ref ||= current_repo.default_branch_ref
    end

    render_template_view "codespaces/new",
      Codespaces::NewView,
      layout: false,
      repository: current_repo,
      ref: ref,
      vscs_target: params[:vscs_target]
  end

  def repository_select
    render_template_view "codespaces/new/repository_select",
      Codespaces::RepositorySelectView,
      layout: false,
      selected_repository: current_repo,
      phrase: params[:q],
      remote_ip: request.remote_ip
  end

  def ref_select
    return render_404 unless current_repo

    ref = current_repo.refs.find(params[:ref]) if params[:ref].present?
    ref ||= current_repo.default_branch_ref

    render_template_view "codespaces/new/ref_select",
      Codespaces::RefSelectView,
      layout: false,
      repository: current_repo,
      selected_ref: ref
  end

  def create
    codespace = Codespaces::Create.call!(
      codespace_params.merge(owner: current_user),
      vscs_target: params[:vscs_target]
    )
    redirect_to codespace_path(codespace)
  rescue Codespaces::GetTargetRepository::UnforkableRepository => e
    flash[:error] = "Codespace could not be created: #{e.message}"
    redirect_to codespaces_path
  rescue Codespaces::Create::InvalidTarget => e
    flash[:error] = "Codespace could not be created: Invalid target"
    redirect_to codespaces_path
  rescue ActiveRecord::RecordInvalid => e
    flash[:error] = "Codespace could not be created: #{e.record.errors.full_messages.to_sentence}"
    redirect_to codespaces_path
  rescue Codespaces::Error => e
    Codespaces::ErrorReporter.report(e, codespace: codespace)

    flash[:error] = "Codespace could not be created"
    redirect_to :back
  end

  def destroy
    # Check for unpushed changes
    if safe_to_destroy?
      # Either we have no unpushed changes OR we have confirmed we want to anyways.
      current_codespace.destroy
      respond_to do |format|
        format.html_fragment do
          headers["Cache-Control"] = "no-cache, no-store"
          render_codespaces_list_component
        end
        format.html do
          flash[:notice] = "Codespace \"#{current_codespace.name}\" deleted"
          redirect_to codespaces_path
        end
      end
    else
      # Warn about unpushed changes!
      respond_to do |format|
        format.html_fragment do
          headers["Cache-Control"] = "no-cache, no-store"
          render_codespaces_list_component(unpushed_id: current_codespace.id)
        end
        format.html do
          flash[:notice] = "Codespace \"#{current_codespace.name}\" has unpushed changes. Please confirm deletion below."
          redirect_to codespaces_path(unpushed_id: current_codespace.id)
        end
      end
    end
  end

  def show
    if current_codespace.provisioned?
      github_token = Codespaces.mint_github_token(current_user, current_codespace, session: user_session)
    end

    current_codespace.mark_used!
    render_codespaces_show(github_token)
  end

  def provisioned
    if current_codespace.provisioned?
      github_token = Codespaces.mint_github_token(current_user, current_codespace, session: user_session)

      if current_user.deiframed_flow_enabled?
        render Codespaces::WorkbenchFormComponent.new \
          codespace: current_codespace,
          github_token: github_token,
          user: current_user
      else
        render_partial_view "codespaces/iframe",
          Codespaces::ShowView,
          {
            codespace: current_codespace,
            passthrough_params: vscs_iframe_params,
            github_token: github_token
          },
          {
            locals: { advance_state: true }
          }
      end

    elsif current_codespace.failed?
      render partial: "codespaces/advance_loading_state",
        locals: { state: "failed" }
    elsif current_codespace.stuck_provisioning?
      render partial: "codespaces/advance_loading_state",
        locals: { state: "stuck" }
    else
      head :accepted
    end
  end

  # Dev Tools endpoint to be used by our VSCS friends to toggle flags. Currently set up to work
  # with one flag, but if we need it in the future we can adjust to support other flags.
  def toggle_dev_flags
    return render_404 unless current_user_feature_enabled?(:codespaces_developer)
    flag = GitHub.flipper[:codespaces_deiframed_flow]
    current_user.deiframed_flow_enabled? ? flag.disable(current_user) : flag.enable(current_user)
    redirect_back(fallback_location: codespaces_path)
  end

  private

  def unauthorized_saml_targets
    Organization.find(unauthorized_organization_ids)
  end

  def safe_to_destroy?
    (params[:force_destroy_id].to_i == current_codespace.id) ||
      !Codespaces::HasUnpushedChanges.call(
        codespace: current_codespace,
        user: current_user
      )
  end

  def codespaces_for_dashboard
    codespaces = Codespaces::Query.new(
      current_user: current_user,
      codespaces_context: Codespaces::Query::ACCESSIBLE_CODESPACES,
      unauthorized_saml_targets: unauthorized_saml_targets).codespaces
    GitHub::PrefillAssociations.for_codespaces_dashboard(codespaces)
    codespaces
  end

  def render_codespaces_list_component(unpushed_id: nil)
    pull_request = PullRequest.find_by_id(codespace_params[:pull_request_id]) if params[:codespace]
    ref = codespace_params[:ref].presence if params[:codespace]
    prompt_at_limit = ActiveModel::Type::Boolean.new.cast(params[:prompt_at_limit].presence || true)

    if params[:wide]
      render Codespaces::WideListComponent.new \
        codespaces: codespaces_for_dashboard,
        unpushed_id: unpushed_id,
        unauthorized_saml_targets: unauthorized_saml_targets
    else
      render Codespaces::ListComponent.new \
        unpushed_id: unpushed_id,
        prompt_at_limit: prompt_at_limit,
        codespaces_context: params[:context].presence,
        repository: current_repo,
        ref: ref,
        pull_request: pull_request,
        event_target: params[:event_target],
        pr_dropdown: params[:pr_dropdown],
        unauthorized_saml_targets: unauthorized_saml_targets
    end
  end

  def codespace_params
    allowed_params = %i(repository_id ref pull_request_id location)
    params.require(:codespace).permit(*allowed_params)
  end

  def vscs_iframe_params
    params.with_defaults(default_iframe_params).slice(*ALLOWED_VSCS_PARAMS).permit!
  end

  def default_iframe_params
    {dogfoodChannel: "insider"}
  end

  def identifier
    params[:identifier]
  end

  def current_repo
    return @current_repo if defined?(@current_repo)
    return unless repo_id = params[:repo]

    repo = Repository.find_by_id(repo_id)

    @current_repo = repo if repo&.pullable_by?(current_user)
  end

  def require_pullable_repo
    render_404 unless current_repo
  end

  def render_codespaces_show(github_token)
    if current_user.deiframed_flow_enabled?
      headers["Cache-Control"] = "no-cache, no-store"
    end

    render_template_view "codespaces/show",
       Codespaces::ShowView,
       layout: "codespaces/fullscreen",
       codespace: current_codespace,
       passthrough_params: vscs_iframe_params,
       github_token: github_token
  end
end
