# frozen_string_literal: true

class Devtools::FeatureFlagsController < DevtoolsController
  areas_of_responsibility :feature_lifecycle

  skip_before_action :cap_pagination, unless: :robot?, only: :show

  # Changes are only instantaneous in the primary datacenter. Other datacenters
  # rely on expiring caches to update their flipper settings.
  TTL_WARNING = "Changes may take up to #{Flipper::Adapters::Memcacheable::TTL} seconds to apply."

  FEATURES_PER_PAGE = 100
  HISTORY_ENTRIES_PER_PAGE = 100
  ACTORS_PER_PAGE = 50
  MAXIMUM_TEAM_SIZE = 25

  def index
    features = FlipperFeature.matches_name_or_description(params[:query]).limit(FEATURES_PER_PAGE).order(name: :asc)
    render "devtools/feature_flags/index", locals: { features: features, search_query: params[:query] }
  end

  def autocomplete
    features = FlipperFeature.matches_name_or_description(params[:q]).limit(FEATURES_PER_PAGE).order(name: :asc)
    render "devtools/feature_flags/autocomplete", formats: :html, layout: false, locals: { features: features }
  end

  def team_autocomplete
    teams = Organization.find(FlipperFeature::GITHUB_ORG_ID)
      .visible_teams_for(current_user)
      .limit(25)

    unless params[:q].blank?
      teams = teams.where("name LIKE ?", "%#{params[:q]}%")
    end

    render "devtools/feature_flags/team_autocomplete", formats: :html, layout: false, locals: {
      teams: teams,
      query: params[:q],
    }
  end

  def service_autocomplete
    services = GitHub::ServiceCatalog.find_services_by_name(name: params[:q])

    render "devtools/feature_flags/service_autocomplete", formats: :html, layout: false, locals: {
      services: services,
      query: params[:q],
    }
  end

  def show
    feature_flag = find_feature
    actor_gates = feature_flag.flipper_gates.actor_gates.paginate(page: current_page, per_page: ACTORS_PER_PAGE)
    render "devtools/feature_flags/show", locals: {
      feature_flag: feature_flag,
      actor_gates: actor_gates,
      big_feature_threshold: FlipperFeature::BIG_FEATURE_WARNING_THRESHOLD,
    }
  end

  def history
    entries = Audit::Driftwood::Query.new_stafftools_query(
      phrase: "data.feature_name:#{find_feature.name}",
      current_user: current_user,
      per_page: HISTORY_ENTRIES_PER_PAGE,
    ).execute

    render partial: "devtools/feature_flags/history", locals: { entries: entries }
  end

  def new
    render "devtools/feature_flags/new", locals: {
      feature: FlipperFeature.new,
    }
  end

  def create
    if feature = FlipperFeature.find_by_name(params[:flipper_feature][:name])
      redirect_to devtools_feature_flag_path(feature), notice: "'#{feature.name}' already exists"
    else
      feature = FlipperFeature.new(flipper_feature_params)
      if feature.save
        redirect_to devtools_feature_flag_path(feature), notice: "'#{feature.name}' has been created"
      else
        flash.now[:error] = "Error creating the feature"
        render "devtools/feature_flags/new", locals: {
          feature: feature,
        }
      end
    end
  end

  def edit
    feature = find_feature
    render "devtools/feature_flags/edit", locals: { feature: feature }
  end

  def update
    feature = find_feature

    if feature.update(flipper_feature_params)
      redirect_to devtools_feature_flag_path(feature), notice: "'#{feature.name}' has been updated"
    else
      flash.now[:error] = "Error updating the feature"
      render "devtools/feature_flags/edit", locals: { feature: feature }
    end
  end

  def destroy
    feature = find_feature
    feature.destroy

    redirect_to devtools_feature_flags_path, notice: "'#{params[:id]}' has been deleted and disabled. #{TTL_WARNING}"
  end

  def enable
    feature = find_feature
    feature.enable

    redirect_to devtools_feature_flag_path(feature), notice: "Enabled '#{feature.name}' for everyone. #{TTL_WARNING}"
  end

  def disable
    feature = find_feature
    feature.disable
    redirect_to devtools_feature_flag_path(feature), notice: "Disabled '#{feature.name}' for everyone. #{TTL_WARNING}"
  end

  def activate_actor_percentage
    feature = find_feature
    feature.enable_percentage_of_actors(params[:percentage])
    redirect_to devtools_feature_flag_path(feature), notice: "Enabled '#{feature.name}' for #{params[:percentage].to_i}% of actors. #{TTL_WARNING}"
  end

  def activate_random_percentage
    feature = find_feature
    feature.enable_percentage_of_time(params[:percentage])
    redirect_to devtools_feature_flag_path(feature), notice: "Enabled '#{feature.name}' #{params[:percentage].to_i}% of the time. #{TTL_WARNING}"
  end

  def add_actor
    feature = find_feature
    render "devtools/feature_flags/add_actor", locals: { feature: feature }
  end

  def add_github_team
    render "devtools/feature_flags/add_github_team", locals: {
      feature: find_feature,
      maximum_team_size: MAXIMUM_TEAM_SIZE,
    }
  end

  def activate_github_team
    feature = find_feature
    team = Team.find_by!(id: params[:flipper_feature][:github_org_team_id], organization_id: FlipperFeature::GITHUB_ORG_ID)

    if team.members_count > MAXIMUM_TEAM_SIZE
      return redirect_to devtools_feature_flag_add_github_team_path(feature), notice: "For performance reasons we don't allow adding teams with more than #{MAXIMUM_TEAM_SIZE} members."
    end

    team.members.each do |member|
      feature.enable(member)
    end

    redirect_to devtools_feature_flag_path(feature), notice: "'#{feature.name}' has been updated. #{TTL_WARNING}"
  end

  def activate_actor
    feature = find_feature

    if params[:enterprise].present? && business = Business.find_by(slug: params[:enterprise].strip)
      feature.enable(business)
    end

    if params[:user].present? && user = User.find_by_name(params[:user].strip)
      feature.enable(user)
    end

    if params[:repository].present? && repository = Repository.with_name_with_owner(params[:repository].strip)
      feature.enable(repository)
    end

    if params[:oauth_application].present? && oauth_application = OauthApplication.find_by_key(params[:oauth_application].strip)
      feature.enable(oauth_application)
    end

    if params[:integration].present? && integration = Integration.where(id: params[:integration].strip).first
      feature.enable(integration)
    end

    if params[:host].present?
      feature.enable(GitHub::FlipperHost.new(params[:host].strip))
    end

    if params[:role].present?
      feature.enable(GitHub::FlipperRole.new(params[:role].strip))
    end

    if params[:flipper_id].present?
      if record = GitHub::FlipperActor.from_flipper_id(params[:flipper_id].strip)
        feature.enable(record)
      end
    end

    redirect_to devtools_feature_flag_path(feature),
      notice: "'#{feature.name}' has been updated. #{TTL_WARNING}"
  end

  def deactivate_actor
    feature = find_feature

    if params[:flipper_id].present?
      if record = GitHub::FlipperActor.from_flipper_id(params[:flipper_id])
        feature.disable(record)

        redirect_to devtools_feature_flag_path(feature),
          notice: "'#{feature.name}' has been updated. #{TTL_WARNING}"
      else
        redirect_to devtools_feature_flag_path(feature),
          notice: "'#{feature.name}' was not updated. Actor could not be found."
      end
    else
      redirect_to devtools_feature_flags_path, notice: "Required parameter flipper_id was missing."
    end
  end

  def activate_group
    feature = find_feature
    feature.enable_group(params[:group])
    redirect_to devtools_feature_flag_path(feature), \
      notice: "Enabled '#{feature.name}' for the '#{params[:group]}' group. #{TTL_WARNING}"
  end

  def deactivate_group
    feature = find_feature
    feature.disable_group(params[:group])
    redirect_to devtools_feature_flag_path(feature), \
      notice: "Removed the '#{params[:group]}' group from '#{feature.name}'. #{TTL_WARNING}"
  end

  private

  def find_feature
    FlipperFeature.find_by_name!(params[:feature_flag_id] || params[:id])
  end

  def flipper_feature_params
    params.require(:flipper_feature).permit(:name, :long_lived, :description, :slack_channel, :github_org_team_id, :service_name)
  end
end
