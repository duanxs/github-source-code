# frozen_string_literal: true

class Devtools::SpamuraiController < DevtoolsController
  before_action :login_required
  before_action :sudo_filter

  def dashboard
    render "devtools/spamurai/dashboard"
  end
end
