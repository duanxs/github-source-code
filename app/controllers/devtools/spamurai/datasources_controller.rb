# frozen_string_literal: true

class Devtools::Spamurai::DatasourcesController < DevtoolsController
  before_action :login_required
  before_action :sudo_filter

  def index
    render "devtools/spamurai/datasources/index",
      locals: {
      spam_datasources: SpamDatasource.all.paginate(page: current_page),
    }
  end

  def show
    spam_datasource = SpamDatasource.find(params[:id])
    entries = spam_datasource.entries.paginate page: current_page
    render "devtools/spamurai/datasources/show", locals: {
      spam_datasource: spam_datasource,
      entries: entries,
    }
  end
end
