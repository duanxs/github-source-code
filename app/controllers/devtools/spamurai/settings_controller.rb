# frozen_string_literal: true

require_dependency "spam"

class Devtools::Spamurai::SettingsController < DevtoolsController
  before_action :login_required
  before_action :sudo_filter

  def show
    render "devtools/spamurai/settings/show"
  end

  def toggle_spam_pattern_checking
    Spam.spam_pattern_checking_enabled? ?
        Spam.disable_spam_pattern_checking(current_user) :
        Spam.enable_spam_pattern_checking(current_user)

    flash[:notice] = "SpamPattern checking #{Spam.spam_pattern_checking_enabled? ? 'enabled' : 'disabled'}"

    redirect_to spamurai_settings_path
  end

  def toggle_spamurai
    Spam.spamurai_enabled? ?
        Spam.disable_spamurai(current_user) :
        Spam.enable_spamurai(current_user)

    flash[:notice] = "Spamurai #{Spam.spamurai_enabled? ? 'enabled' : 'disabled'}"

    redirect_to spamurai_settings_path
  end
end
