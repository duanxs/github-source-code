# frozen_string_literal: true

class Devtools::Spamurai::PatternsController < DevtoolsController
  before_action :login_required
  before_action :sudo_filter

  def index
    if !Spam.spam_pattern_checking_enabled?
      flash.now[:error] = "Spam detection using these patterns is disabled in the current environment."
    end

    filtered_state = params[:state] == "inactive" ? :inactive : :active
    selected_target = params[:target]
    selected_sort = params[:sort]

    render_template_view "devtools/spamurai/patterns/index",
      Devtools::Spamurai::PatternsView,
      per_page: params[:per_page],
      current_page: current_page,
      filtered_state: filtered_state,
      selected_target: selected_target,
      selected_sort: selected_sort
  end

  def new
    render "devtools/spamurai/patterns/new", locals: {
      pattern: SpamPattern.new,
    }
  end

  def create
    sp = SpamPattern.new pattern_params
    regexp = validate_pattern(sp.pattern)

    # We've encountered an error state, return so the flash/redirect can process
    return if regexp.nil?

    sp.pattern = regexp

    if sp.save
      redirect_to spamurai_pattern_path(sp)
    else
      flash.now[:error] = sp.errors.full_messages.to_sentence
      render "devtools/spamurai/patterns/new", locals: {
        pattern: sp,
      }
    end
  end

  def show
    pattern = SpamPattern.find params[:id]
    render "devtools/spamurai/patterns/show", locals: {
      pattern: pattern,
    }
  end

  def update
    sp = SpamPattern.find params[:id]
    # We don't allow updating the pattern for now
    pattern_params.delete :pattern
    sp.update pattern_params

    if sp.errors.any?
      flash.now[:error] = sp.errors.full_messages.to_sentence
      render "devtools/spamurai/patterns/show", locals: {
        pattern: sp,
      }
    else
      flash[:notice] = "Pattern #{sp.id} updated successfully."
      redirect_to spamurai_pattern_path(sp)
    end
  end

  def queues
    @current_data_patterns           = Spam.get_current_patterns("data")
    @current_filename_patterns       = Spam.get_current_patterns("filename")
    @current_gist_patterns           = Spam.get_current_patterns("gist")
    @current_gist_comment_patterns   = Spam.get_current_patterns("gist_comment")
    @current_pages_js_patterns       = Spam.get_current_patterns("pages_js")
    @current_wiki_patterns           = Spam.get_current_patterns("wiki")

    render "devtools/spamurai/patterns/queues"
  end

  def add_queue_pattern
    key = params[:key]
    unless ["commit_comment", "data", "email", "filename", "gist", "issue",
            "issue_comment", "pages_js", "repositories", "wiki"].include? key
      flash[:error] = "Unrecognized key: #{key}"
      redirect_to :back
      return
    end

    pattern = params[:pattern].to_s

    regexp = validate_pattern(pattern)
    # We've encountered an error state, return so the flash/redirect can process
    return if regexp.nil?

    result = Spam.add_current_pattern(key, regexp)
    if result
      GitHub::SpamChecker.notify("%s added pattern '%s' to '%s'" %
                                  [current_user, pattern, key])
      flash[:notice] = "Pattern '%s' added to '%s'" %
                        [pattern, key]
    else
      GitHub::SpamChecker.notify("%s failed to add pattern '%s' to '%s'" %
                                  [current_user, pattern, key])
      flash[:notice] = "Pattern addition failed for '%s' in '%s'", %
                        [pattern, key]
    end

    redirect_to :back
  end

  def remove_queue_pattern
    key = params[:key]
    pattern = params[:pattern]
    result = Spam.remove_current_pattern(key, pattern)
    if result
      GitHub::SpamChecker.notify("%s removed pattern '%s' from '%s'" %
                                  [current_user, pattern, key])
      flash[:notice] = "Pattern '#{pattern}' removed from '#{key}'"
    else
      GitHub::SpamChecker.notify("%s failed to remove pattern '%s' from '%s'" %
                                  [current_user, pattern, key])
      flash[:notice] = "Pattern removal failed for '#{pattern}' in '#{key}'"
    end
    redirect_to :back
  end

  def destroy
    id = params[:id]
    if sp = SpamPattern.find_by_id(id)
      GitHub::SpamChecker.notify("%s removed SpamPattern %d: %s.%s =~ '%s'" %
                                  [current_user, sp.id, sp.class_name,
                                    sp.attribute_name, sp.pattern])
      flash[:notice] = "SpamPattern %d removed: %s.%s =~ '%s'" %
                       [sp.id, sp.class_name, sp.attribute_name, sp.pattern]
      sp.destroy
    else
      GitHub::SpamChecker.notify("%s failed to remove SpamPattern #{id}")
      flash[:notice] = "No such SpamPattern as #{id}"
    end
    redirect_to :back
  end

  # Disable a SpamPattern record
  def disable
    id = params[:id]
    if sp = SpamPattern.find_by_id(id)
      GitHub::SpamChecker.notify("%s disabled SpamPattern %d: %s.%s =~ '%s'" %
                                  [current_user, sp.id, sp.class_name,
                                    sp.attribute_name, sp.pattern])
      flash[:notice] = "SpamPattern %d disabled: %s.%s =~ '%s'" %
                       [sp.id, sp.class_name, sp.attribute_name, sp.pattern]
      sp.disable!
    else
      GitHub::SpamChecker.notify("%s failed to disable SpamPattern #{id} (could not find it)")
      flash[:notice] = "No such SpamPattern as #{id}"
    end
    redirect_to :back
  end

  # Enable a SpamPattern record
  def enable
    id = params[:id]
    if sp = SpamPattern.find_by_id(id)
      GitHub::SpamChecker.notify("%s enabled SpamPattern %d: %s.%s =~ '%s'" %
                                  [current_user, sp.id, sp.class_name,
                                    sp.attribute_name, sp.pattern])
      flash[:notice] = "SpamPattern %d enabled: %s.%s =~ '%s'" %
                       [sp.id, sp.class_name, sp.attribute_name, sp.pattern]
      sp.enable!
    else
      GitHub::SpamChecker.notify("%s failed to enable SpamPattern #{id} (could not find it)")
      flash[:notice] = "No such SpamPattern as #{id}"
    end
    redirect_to :back
  end

  private

  def pattern_params
    params.require(:spam_pattern).permit(:pattern, :class_name, :attribute_name, :flag, :log, :queue, :comment)
  end

  # Private: Validate the staff supplied regexp
  #
  # Returns a Regexp if valid
  # redirects and shows a flash otherwise
  def validate_pattern(pattern)
    # Make this concession to the inevitable confusion I'll have when I paste
    # in a canonical Regexp form into the field and find its ending tags
    # escaped.
    if pattern !~ /\A\/(.+)\/([imx]{0,3})\z/
      flash[:error] = "Unrecognized Regexp format: #{pattern}"
      redirect_to :back
      return
    end

    pattern     = Regexp.last_match[1].dup    # grab a copy before these go away
    flag_string = Regexp.last_match[2].dup

    flags  = 0
    flags |= Regexp::EXTENDED   if flag_string =~ /x/
    flags |= Regexp::IGNORECASE if flag_string =~ /i/
    flags |= Regexp::MULTILINE  if flag_string =~ /m/

    begin
      regexp = Regexp.new pattern, flag_string
    rescue RegexpError => e
      flash[:notice] = "Pattern '#{pattern}' (#{flag_string}) wasn’t a valid Regexp: #{e}"
      redirect_to :back
      return
    end

    regexp
  end
end
