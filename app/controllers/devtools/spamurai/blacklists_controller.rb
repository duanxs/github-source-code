# frozen_string_literal: true

class Devtools::Spamurai::BlacklistsController < DevtoolsController
  before_action :login_required
  before_action :sudo_filter

  def index
    blacklisted_payment_methods = BlacklistedPaymentMethod.
      includes(:user).
      paginate(page: current_page, per_page: 10).
      order("created_at DESC")

    render "devtools/spamurai/blacklists/index", locals: {
      blacklisted_payment_methods: blacklisted_payment_methods,
    }
  end

  def create
    user = User.find_by_login params[:login]
    blacklisted = BlacklistedPaymentMethod.create_from_user(user)

    if blacklisted.persisted?
      blacklisted.users.each do |user|
        BlacklistedPaymentMethod.create_from_user(user)
        user.cancel_subscription
        user.suspend "Using blacklisted payment method"
        user.mark_as_spammy reason: "Blacklisted payment method", hard_flag: true, origin: :devtools
      end

      flash[:notice] = "Payment method was blacklisted"
    else
      flash[:error] = blacklisted.errors.full_messages.to_sentence
    end

    redirect_to spamurai_blacklists_path
  end

  def destroy
    blacklisted = BlacklistedPaymentMethod.find_by_id params[:id]

    if blacklisted.destroy
      flash[:notice] = "payment method no longer blacklisted"
    else
      flash[:error] = blacklisted.errors.full_messages.to_sentence
    end

    redirect_to :back
  end
end
