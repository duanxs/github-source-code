# frozen_string_literal: true

module PressFeedControllerMethods
  extend ActiveSupport::Concern

  PRESS_FEED_URL = "https://press-feed.github.com"

  included do
    helper_method :press_feed_collection
  end

  private

  # The JSON data for press feed collection on the GitHub Pages-based site.
  def press_feed_collection
    GitHub::JSON::CachedFetchRemoteUrl.fetch(
      url: "#{PRESS_FEED_URL}/entries.json",
      cache_key: "about:press_feed_collection:latest",
      default_value: {},
      hash_subkey: "items",
    )
  end
end
