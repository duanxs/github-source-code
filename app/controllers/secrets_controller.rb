# frozen_string_literal: true

class SecretsController < AbstractRepositoryController
  include Actions::SecretsHelper
  include ActionView::Helpers::NumberHelper

  areas_of_responsibility :repo_settings
  map_to_service :actions_experience

  before_action :login_required
  before_action :ensure_admin_access

  javascript_bundle :settings

  def secrets
    render "edit_repositories/pages/secrets", locals: view_locals
  end

  def can_use_org_secrets?
    current_repository.owner.can_use_org_secrets?
  end

  def repo_can_use_org_secrets?
    return false unless can_use_org_secrets?
    return true if current_repository.public?

    plan_name = current_repository.async_actions_plan_owner.sync.plan_name
    return plan_name != ActionsPlanOwner::FREE && plan_name != ActionsPlanOwner::FREE_ORGANIZATION
  end

  def secrets_for_repository
    result = GrpcHelper.rescue_from_grpc_errors("Secrets") do
      Credz.list_repository_secrets(
          app: actions_integration,
          repository: current_repository,
          actor: current_user,
    )
    end

    unless result.call_succeeded?
      flash[:error] = "Failed to load secrets. Please refresh and try again."
    end

    return {
      repository_secrets: Array(result&.value&.repository_secrets).map { |s| map_secret s },
      organization_secrets: Array(result&.value&.organization_secrets).map { |s| map_secret s },
    }
  end

  def remove_secret
    return render_404 unless actions_integration

    result = GrpcHelper.rescue_from_grpc_errors("Secrets") do
      Credz.delete_credential \
        app:  actions_integration,
        owner: current_repository,
        actor: current_user,
        key:  params[:key]
    end

    if !result.value&.success
      flash[:error] = "Failed to delete secret."
    else
      flash[:notice] = "Repository secret deleted."

      GlobalInstrumenter.instrument("repository_secret.remove", {
        repository: current_repository,
        user: current_user,
        name: params[:key],
        state: "DELETED",
      })
    end

    render "edit_repositories/pages/secrets", locals: view_locals, status: result.status
  end

  def update
    id, encoded = github_public_key(current_repository)

    if params[:key_id].to_i != id
      flash[:error] = "Failed to update secret. Please try again."
      redirect_to repository_secrets_path
      return
    end

    if params[:encrypted_value].empty? || params[:key_id].to_i != id
      flash[:error] = "Failed to add secret. Please try again."
      redirect_to repository_secrets_path
      return
    end

    validation = Credz.validate_secret(params[:secret_name], params[:encrypted_value])
    unless validation.succeeded?
      flash[:error] = validation.error
      redirect_to repository_secrets_path
      return
    end

    value = if GitHub.enterprise?
      decrypt_enterprise_secret(params[:encrypted_value])
    else
      Earthsmoke::Embedding.embed(id, Base64.strict_decode64(params[:encrypted_value]))
    end

    encoded_value = Base64.strict_encode64(value)

    result = GrpcHelper.rescue_from_grpc_errors("Secrets") do
      Credz.update_credential \
        app: actions_integration,
        owner: current_repository,
        actor: current_user,
        key: params[:secret_name],
        value: encoded_value
    end

    if !result.call_succeeded?
      flash[:error] = "Failed to update secret."
    else
      flash[:notice] = "Secret updated."
    end

    redirect_to repository_secrets_path
  end

  def edit
      return render_404 unless actions_integration

      result = GrpcHelper.rescue_from_grpc_errors("Secrets") do
        Credz.fetch_credential(
          app: actions_integration,
          owner: current_repository,
          actor: current_user,
          key: params[:secret_name]
        )
      end

      secret = result.value&.credential
      return render_404 unless secret

      render "edit_repositories/pages/secrets/edit", locals: {
        public_key: github_public_key(current_repository),
        secret: secret,
      }
  end

  def new_secret
    return render_404 unless actions_integration

    render "edit_repositories/pages/secrets/new_secret", locals: view_locals
  end

  def add_secret
    return render_404 unless actions_integration

    id, encoded = github_public_key(current_repository)

    if params[:encrypted_value].empty? || params[:key_id].to_i != id
      flash[:error] = "Failed to add secret. Please try again."
      return render "edit_repositories/pages/secrets", locals: view_locals
    end

    validation = Credz.validate_secret(params[:secret_name], params[:encrypted_value])
    unless validation.succeeded?
      flash[:error] = validation.error
      return render "edit_repositories/pages/secrets", locals: view_locals
    end

    value = if GitHub.enterprise?
      decrypt_enterprise_secret(params[:encrypted_value])
    else
      Earthsmoke::Embedding.embed(id, Base64.strict_decode64(params[:encrypted_value]))
    end

    encoded_value = Base64.strict_encode64(value)

    result = GrpcHelper.rescue_from_grpc_errors("Secrets") do
      Credz.store_credential \
        app:   actions_integration,
        owner:  current_repository,
        actor: current_user,
        key:   params[:secret_name],
        value: encoded_value
    end

    if !result.call_succeeded?
      if result.status == 429
        flash[:error] = "Failed to add secret, you've reached the #{number_with_delimiter(Credz::SECRET_REPO_MAX)} secret limit."
      else
        flash[:error] = "Failed to add secret."
      end
    else
      flash[:notice] = "Repository secret added."

      GlobalInstrumenter.instrument("repository_secret.create", {
        repository: current_repository,
        user: current_user,
        name: params[:secret_name],
        state: "CREATED",
      })
    end

    render "edit_repositories/pages/secrets", locals: view_locals, status: result.status
  end

  private

  helper_method :current_repository

  def map_secret(secret)
    return {
      name: secret.name,
      updated_at: secret_last_update(secret),
    }
  end

  def view_locals
    repo_org_secrets = secrets_for_repository
    repository_secret_names = repo_org_secrets[:repository_secrets].map { |s| s[:name] }.to_set
    organization_secret_names = repo_org_secrets[:organization_secrets].map { |s| s[:name] }.to_set

    {
      integration: actions_integration,
      public_key: github_public_key(current_repository),
      can_use_org_secrets: can_use_org_secrets?,
      repo_can_use_org_secrets: repo_can_use_org_secrets?,
      secrets: repo_org_secrets,
      repository_secret_names: repository_secret_names,
      organization_secret_names: organization_secret_names,
      is_owner_admin: current_repository.owner.adminable_by?(current_user),
    }
  end

  def actions_integration
    # Always store secrets with the prod app, even in lab.
    @integration ||= GitHub.launch_github_app
  end
end
