# frozen_string_literal: true

class RegistryTwo::PackageVersionsController < RegistryTwo::Controller

  before_action :redirect_legacy_packages
  before_action :ensure_can_see_containers_ui
  before_action :ensure_supported_ecosystem
  before_action :get_metadata_all_versions, only: [:show]

  def show
    response = ::Permissions::Enforcer.authorize(
      action: :admin_package,
      actor: current_user,
      subject: package,
      context: {
          "subject.owner.id" => package.owner.id,
          "subject.author.id" => package.author.id,
      },
    )

    render "registry_two/package_versions/show", locals: {
      package: @metadata.package,
      package_versions: @metadata.package_versions.paginate({page: params[:page], per_page: 50}),
      viewer_is_admin: response.allow?,
    }
  end

  def delete_package_version
    unless params[:name].casecmp?(params[:verify])
      flash[:error] = "You must type the name of the package to confirm."
      return redirect_to package_versions_two_path
    end

    begin
      client.delete_package_version(
          ecosystem: package.package_type,
          namespace: package.namespace,
          name: package.name,
          user_id: current_user.id,
          mode: :soft,
          version: params[:version],
      )
    rescue PackageRegistry::Twirp::BaseError
      flash[:error] = "The package version could not be deleted. Please try again later."
      return redirect_to package_versions_two_path
    end

    redirect_to package_versions_two_path
  end

  private

  def redirect_legacy_packages
    redirect_to package_versions_path(owner, package.repository, package.id) if LEGACY_ECOSYSTEMS.include?(params[:ecosystem])
  end

  def get_metadata_all_versions
    GitHub::Logger.log(
        fn: "registry_two_package_verisons_controller.get_metadata_all_versions",
        ecosystem: params[:ecosystem],
        namespace: owner.login,
        name: params[:name],
        user_id: current_user&.id,
    )

    @metadata = client.get_package_metadata(ecosystem: params[:ecosystem], namespace: owner.name, name: params[:name], user_id: current_user&.id)

    GitHub::Logger.log(fn: "registry_two_package_versions_controller.get_metadata_all_versions", metadata: @metadata)
    render_404 unless @metadata
  rescue PackageRegistry::Twirp::BaseError => e
    GitHub::Logger.log(fn: "registry_two_package_versions_controller.get_metadata_all_versions", error: e)
    render_404
  end
end
