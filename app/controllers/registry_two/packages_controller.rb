# frozen_string_literal: true

class RegistryTwo::PackagesController < RegistryTwo::Controller

  before_action :redirect_legacy_packages, only: [:show]
  before_action :ensure_can_see_containers_ui, only: [:show]
  before_action :ensure_supported_ecosystem, only: [:show]
  before_action :get_metadata, only: [:show]
  before_action :redirect_latest, only: [:show]

  def index
    # We already have an org_packages path so if we hit this action,
    # it is implied that owner_type is `users`.
    args = "?tab=packages"
    args += "&q=#{params[:q]}" if params[:q].present?

    redirect_to user_path(owner) + args
  end

  def ecosystem_index
    if params[:owner_type] == "orgs"
      redirect_to org_packages_path(owner, ecosystem: params[:ecosystem])
    else
      redirect_to user_path(owner) + "?tab=packages&ecosystem=#{params[:ecosystem]}"
    end
  end

  def show
    package_version = client.get_package_version(ecosystem: params[:ecosystem], user_id: current_user&.id, namespace: owner.name, name: params[:name], version_id: params[:version].to_i)
    return render_404 unless package_version

    download_counts = GitHub.flipper[:package_download_counts].enabled?(owner) ?
      client.get_package_version_download_counts(package_id: package_version.package_id, version_id: params[:version].to_i) :
      PackageRegistry::DownloadCounts::NULL

    render_template_view "registry_two/packages/show", RegistryTwo::Packages::ShowView, owner: owner, package: @metadata.package, package_versions: @metadata.package_versions, package_version: package_version, package_download_counts: download_counts
  end

  private

  def redirect_latest
    GitHub::Logger.log(fn: "registry_two_packages_controller.redirect_latest", version: params[:version].to_i)
    if params[:version].to_i == 0
      GitHub::Logger.log(fn: "registry_two_packages_controller.redirect_latest", no_versions: @metadata.package_versions.blank?)
      return render_404 if @metadata.package_versions.blank?
      GitHub::Logger.log(fn: "registry_two_packages_controller.redirect_latest", latest_version_id: @metadata.package_versions.first.id)
      redirect_to package_two_path(version: @metadata.package_versions.first.id)
    end
  end
end
