# frozen_string_literal: true

class PullRequestReviewCommentsController < GitContentController

  statsd_tag_actions only: :create

  include CommentsHelper
  include DiscussionPreloadHelper
  MISSING_PARENT_ERROR = "The comment you are replying to has been deleted."
  OUT_OF_DATE_DIFF_ERROR = "This diff is outdated, please refresh and try again."

  before_action :login_required
  before_action :load_current_pull_request
  before_action :ensure_current_user_not_blocked, only: [:create]
  before_action :content_authorization_required, only: [:create, :update]

  CreateReviewThreadQuery = parse_query <<-'GRAPHQL'
    mutation($path: String!, $line: Int!, $body: String!, $reviewId: ID!,
      $startCommitOid: GitObjectID!, $endCommitOid: GitObjectID!, $submitReview: Boolean!,
      $baseCommitOid: GitObjectID, $side: DiffSide, $startLine: Int, $startSide: DiffSide) {
      addPullRequestReviewThread(input: {
        path: $path,
        line: $line,
        startLine: $startLine,
        startSide: $startSide,
        body: $body,
        pullRequestReviewId: $reviewId,
        submitReview: $submitReview,
        diffRange: {
          startCommitOid: $startCommitOid,
          endCommitOid: $endCommitOid,
          baseCommitOid: $baseCommitOid
        },
        side: $side
      }) {
        thread {
          comments(first: 1) { nodes { databaseId } }
        }
        errors {
          attribute
          message
        }
      }
    }
  GRAPHQL

  CreateReviewThreadReplyQuery = parse_query <<-'GRAPHQL'
    mutation($body: String!, $reviewId: ID!, $threadId: ID!, $submitReview: Boolean!) {
      addPullRequestReviewThreadReply(input: {
        pullRequestReviewId: $reviewId,
        pullRequestReviewThreadId: $threadId,
        body: $body,
        submitReview: $submitReview,
      }) {
        comment { databaseId }
        errors {
          attribute
          message
        }
      }
    }
  GRAPHQL

  # Create a PullRequestReviewComment, and required PullRequestReview if necessary
  #
  # This action is complicated for a few reasons:
  #
  # * PullRequestReviewComment's require a lot of state to be created correctly
  # * We create these comments from many contexts:
  #
  #   - PR timeline
  #   - PR diff - split diff mode
  #   - PR diff - unified diff mode
  #   - PR diff range - unified diff mode
  #   - PR diff range - split diff mode
  #
  # So five different contexts we have to worry about
  def create
    result = data = comment_database_id = nil

    body = params[:comment] && params[:comment][:body]
    replying = params[:in_reply_to].present?

    review = @pull.pending_review_for(user: current_user, head_sha: params[:comparison_end_oid])

    submit_review = params[:single_comment].present?

    if replying
      parent = find_parent(params[:in_reply_to])
      return handle_missing_parent unless parent

      result = platform_execute(CreateReviewThreadReplyQuery, variables: {
        body: body,
        reviewId: review.global_relay_id,
        threadId: parent.pull_request_review_thread.global_relay_id,
        clientMutationId: request_id,
        submitReview: submit_review,
      })

      data = result.add_pull_request_review_thread_reply
      comment_database_id = data&.comment&.database_id
    else
      variables = {
        path: params[:path],
        line: params[:line].to_i,
        body: body,
        reviewId: review.global_relay_id,
        startCommitOid: params[:comparison_start_oid],
        endCommitOid: params[:comparison_end_oid],
        baseCommitOid: params[:comparison_base_oid],
        side: params[:side].upcase,
        clientMutationId: request_id,
        submitReview: submit_review,
      }

      variables[:startLine] = params[:start_line].to_i if params[:start_line].present?
      variables[:startSide] = params[:start_side].upcase if params[:start_side].present?

      result = platform_execute(CreateReviewThreadQuery, variables: variables)

      data = result.add_pull_request_review_thread
      comment_database_id = data&.thread&.comments&.nodes&.first&.database_id
    end

    algorithm_tag = params[:algorithm] == "ignore-whitespace" ? "algorithm:ignore-whitespace" : "algorithm:vanilla"

    errors = result.errors.all.values.flatten
    errors += data.errors.map(&:message) if data

    if errors.any?
      if data&.errors&.any? { |error| error.attribute == "line" }
        GitHub.dogstats.increment("review.comment.position-error", tags: [algorithm_tag])
      end

      errors.map! do |error|
        /oid is not part of the pull request/.match(error) ? OUT_OF_DATE_DIFF_ERROR : error
      end

      if request.xhr?
        render status: 422, json: { errors: errors.uniq.to_sentence }
      else
        redirect_to pull_request_path(review.pull_request)
      end

      return
    end

    comment = PullRequestReviewComment.find(comment_database_id)

    GitHub.instrument "comment.create", user: current_user

    tags = ["reply:#{replying ? 'true' : 'false' }", algorithm_tag]
    GitHub.dogstats.increment("review.comment.created", tags: tags)

    instrument_saved_reply_use(params[:saved_reply_id], "pull_request_review_comment")
    preload_discussion_group_data([comment.pull_request_review_thread.async_to_deprecated_thread.sync, comment])

    if request.xhr?
      respond_to do |format|
        format.json do
          json = {}

          # Replying to a thread - render just the reply comment to append to the existing thread.
          if replying
            json[:inline_comment] = render_to_string(
              partial: "comments/review_comment",
              formats: [:html],
              locals: {
                comment: preloaded_platform_object(comment),
                dom_id: params[:comment_context] == "discussion" ?
                            "#{CommentsHelper::DISCUSSION_DOM_ID_PREFIX}#{comment.id}" :
                            "#{CommentsHelper::COMMIT_COMMENT_DOM_ID_PREFIX}#{comment.id}",
                comment_context: params[:comment_context],
              },
            )
          else
            threads = DeprecatedPullRequestReviewThread.group_comments(@pull, [comment])
            preload_discussion_group_data(threads + threads.flat_map(&:timeline_children))
            # Render entire thread with line comment form.
            json[:inline_comment_thread] = render_to_string(
              partial: "diff/review_threads",
              formats: [:html],
              object: threads,
            )
          end

          render json: json
        end
      end
    else
      redirect_to_comment_anchor(comment, params[:comment_context])
    end
  end

  def update
    if blocked_from_commenting?(@pull)
      access_denied
      return
    end

    comment = current_comment
    if can_modify_commit_comment?(comment)
      # prevent updates from stale data
      if stale_model?(comment)
        return render_stale_error(model: comment, error: "Could not edit comment. Please try again.", path: pull_request_path(@pull, @pull.repository))
      end

      if operation = TaskListOperation.from(params[:task_list_operation])
        text = operation.call(comment.body)
        comment.update_body(text, current_user) if text
      else
        comment.update_body(params[:pull_request_review_comment][:body], current_user)
      end

      respond_to do |format|
        format.json do
          render json: {
            "source" => comment.body,
            "body" => comment.body_html,
            "newBodyVersion" => comment.body_version,
            "editUrl" => show_comment_edit_history_path(comment.global_relay_id),
          }
        end

        format.html do
          unless comment.valid?
            flash[:error] = "Could not edit comment."
          end

          redirect_to_comment_anchor(comment, params[:comment_context])
        end
      end
    else
      access_denied
    end
  end

  def destroy
    if blocked_from_commenting?(@pull)
      access_denied
      return
    end

    comment = current_comment
    if comment.deleteable_by?(current_user)
      comment.destroy

      respond_to do |format|
        format.html do
          redirect_to pull_request_path(@pull)
        end
        format.json do
          head 200
        end
      end
    else
      access_denied
    end
  end

  def suggestion_button
    selection = if comment_id = params[:comment_id].presence || params[:in_reply_to].presence
      comment = find_parent(comment_id)

      PullRequestReviewComment::SuggestedChangeSelection.from_persisted_comment(comment)
    else
      PullRequestReviewComment::SuggestedChangeSelection.new(
        pull: @pull,
        path: params[:path],
        start_commit_oid: params[:start_commit_oid],
        end_commit_oid: params[:end_commit_oid],
        base_commit_oid: params[:base_commit_oid],
        start_line: params[:start_line].presence&.to_i,
        start_side: params[:start_side],
        line: params[:end_line].to_i,
        side: params[:end_side],
      )
    end

    respond_to do |format|
      format.html do
        render Comments::SuggestionButtonComponent.new(
          outdated: selection.outdated?,
          lines: selection.lines,
          contains_deletions: selection.contains_deletions?,
          disabled: selection.disabled?,
          missing_commit: !selection.valid_commits?,
          empty_selection: selection.lines.empty?,
          pull_request: @pull,
        )
      end
    end
  end

  def actions
    permission = Platform::Authorization::Permission.new(viewer: current_user)
    comment = Platform::Helpers::NodeIdentification.typed_object_from_id(
      [Platform::Objects::PullRequestReviewComment],
      params[:id], permission: permission)
    if comment
      render partial: "comments/review_comment_actions", locals: {
        comment: comment
      }
    else
      render_404
    end
  end

private

  def ensure_current_user_not_blocked
    if blocked_from_commenting?(@pull)
      redirect_to pull_request_path(@pull)
    end
  end

  # Private: Handle the case where parent comment has been deleted out from
  # under a reply attempt
  def handle_missing_parent
    if request.xhr?
      render status: 422, json: {
        errors: [MISSING_PARENT_ERROR],
      }
    else
      flash[:error] = MISSING_PARENT_ERROR
      redirect_to pull_request_path(@pull)
    end
  end

  def find_parent(in_reply_to)
    @pull.review_comments.where(id: in_reply_to).first
  end

  def current_comment
    @current_comment ||= @pull.review_comments.find(params[:id])
  end

  def load_current_pull_request
    @pull = current_repository.issues.find_by_number(params[:pull_id].to_i).try(:pull_request)
    return render_404 unless @pull

    @comparison = @pull.comparison
  end

  def authorized?
    params[:action] == "create" ? (logged_in? && super) : super
  end

  def redirect_to_comment_anchor(comment, comment_context)
    url = case comment_context
    when "discussion"
      "#{pull_request_path(@pull)}#discussion_r#{comment.id}"
    else
      "#{pull_request_path(@pull)}/files#r#{comment.id}"
    end
    redirect_to url
  end

  def content_authorization_required
    authorize_content(:pull_request_comment, repo: current_repository)
  end

  def route_supports_advisory_workspaces?
    true
  end
end
