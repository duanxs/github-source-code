# frozen_string_literal: true

class BusinessesController < Businesses::BusinessController

  PAGE_SIZE = 30

  before_action :require_provisioning_feature_flag, only: %i(unowned_organizations claim_unowned_organization)
  before_action :login_required
  before_action :dotcom_required, only: %i(remove_organization)
  before_action :single_business_environment_required, only: %i(global_business_redirect redirect_to_business_settings)
  before_action :business_access_required, except: %i(global_business_redirect redirect_to_business_settings)
  before_action :business_admin_required, except: %i(global_business_redirect redirect_to_business_settings show)
  before_action :redirect_billing_manager_to_billing_settings, only: %i(show)

  def global_business_redirect
    redirect_to "/enterprises/#{GitHub.global_business.slug}/#{params[:route]}"
  end

  def redirect_to_business_settings
    # substitute the target business settings path for `/admin/:resource`
    # fullpath maintains the url query without needing any additional handling
    redirect_path = request.fullpath.gsub(%r{/admin/[^/]+}i, "/enterprises/#{GitHub.global_business.to_param}/settings/#{params[:group]}")
    redirect_to redirect_path
  end

  def show
    pending_invitation = this_business.pending_admin_invitation_for(current_user)
    organizations = this_business
      .filtered_organizations(viewer: current_user, query: query_param)
      .paginate(page: current_page, per_page: PAGE_SIZE)
    org_ids = organizations.map(&:id)

    respond_to do |format|
      format.html do
        instrument_show
        if request.xhr?
          headers["Cache-Control"] = "no-cache, no-store"
          render partial: "businesses/organizations_list", locals: {
            organizations: organizations,
            can_create_organization: Business::OrganizationPermission.new(this_business, current_user).can_create_organization?,
            organization_counters: organization_counters(org_ids),
            query: query_param,
          }
        else
          render "businesses/show", locals: {
            pending_invitation: pending_invitation,
            organizations: organizations,
            can_create_organization: Business::OrganizationPermission.new(this_business, current_user).can_create_organization?,
            organization_counters: organization_counters(org_ids),
            query: query_param,
          }
        end
      end
    end
  end

  def unowned_organizations
    organizations = this_business.orphaned_organizations
                                 .includes(:saml_provider, :business)
                                 .paginate(page: current_page)

    render "businesses/organizations/orphaned_organizations", locals: {
      organizations: organizations,
    }
  end

  def claim_unowned_organization
    organization = this_business.organizations.find_by(login: login_param)

    if organization.admins.any?
      flash[:error] = "This organization already has at least one active owner."
      redirect_to enterprise_path(this_business)
    else
      # Either update their membership to admin or add them as a new admin
      if organization.direct_member?(current_user)
        organization.update_member(current_user, action: :admin)
      else
        organization.add_admin(current_user)
      end

      flash[:notice] = "Successfully added #{current_user} as an owner of the '#{organization}' organization."
      redirect_to enterprise_path(this_business)
    end
  end

  def people
    query_args = parse_query_string(query_param,
      filter_map: BusinessesHelper::MEMBERS_QUERY_FILTERS,
    )

    pending_member_invitations = this_business
      .pending_member_invitations(query: query_args[:query])

    members = this_business
      .filtered_members(
        current_user,
        query: query_args[:query],
        role: query_args[:role],
        deployment: query_args[:deployment],
        organization_logins: query_args[:organizations],
        license: query_args[:license],
      )
      .paginate(page: current_page)

    respond_to do |format|
      format.html do
        if request.xhr?
          headers["Cache-Control"] = "no-cache, no-store"
          render partial: "businesses/members_list", locals: {
            query: query_param,
            members: members,
          }
        else
          render "businesses/people", locals: {
            query: query_param,
            members: members,
            pending_member_invitations_count: pending_member_invitations.count,
          }
        end
      end
    end
  end

  def org_filter_menu_content
    respond_to do |format|
      format.html do
        render partial: "businesses/people/org_filter_menu_content",
          layout: false, locals: { query: query_param }
      end
    end
  end

  def pending_members
    query_args = parse_query_string(query_param,
      graphql: true,
      filter_map: BusinessesHelper::MEMBERS_QUERY_FILTERS,
    )

    pending_member_invitations = this_business
      .pending_member_invitations(
        query: query_args[:query],
        order_by_field: "created_at",
        order_by_direction: "DESC",
      )
      .includes(:organization)
      .paginate(page: current_page)

    respond_to do |format|
      format.html do
        if request.xhr?
          headers["Cache-Control"] = "no-cache, no-store"
          render partial: "businesses/pending_members_list", locals: {
            business: this_business,
            query: query_param,
            pending_member_invitations: pending_member_invitations,
          }
        else
          render "businesses/pending_members", locals: {
            business: this_business,
            query: query_param,
            pending_member_invitations: pending_member_invitations,
          }
        end
      end
    end
  end

  def outside_collaborators
    query_args = parse_query_string(query_param,
      filter_map: BusinessesHelper::OUTSIDE_COLLABS_QUERY_FILTERS,
    )
    outside_collaborators = this_business
      .filtered_outside_collaborators(
        query: query_args[:query],
        visibility: Array(query_args[:visibility]).map(&:to_sym),
      ).paginate(page: current_page)

    respond_to do |format|
      format.html do
        if request.xhr?
          headers["Cache-Control"] = "no-cache, no-store"
          render partial: "businesses/outside_collaborators_list", locals: {
            query: query_param,
            outside_collaborators: outside_collaborators,
          }
        else
          render "businesses/outside_collaborators", locals: {
            query: query_param,
            outside_collaborators: outside_collaborators,
            pending_collaborators_count: this_business.pending_collaborator_invitations.count,
          }
        end
      end
    end
  end

  def outside_collaborator
    user = User.find_by!(login: login_param)
    business_user_account = this_business.business_user_account_for(user)
    repositories = user.outside_collaborator_repositories(business: this_business)
      .order("repositories.name ASC")
      .paginate(page: current_page, per_page: PAGE_SIZE)
    org_count = this_business.organizations_for_member(user).count
    installation_count = business_user_account&.enterprise_installation_user_accounts&.count.to_i
    pending_invitations_count = this_business.pending_collaborator_invitations(login: login_param).count
    render "businesses/outside_collaborator", locals: {
      user: user,
      business_user_account: business_user_account,
      repositories: repositories,
      organization_count: org_count,
      installation_count: installation_count,
      pending_invitations_count: pending_invitations_count,
    }
  end

  def pending_collaborators
    pending_collaborators = this_business.pending_collaborator_invitations(
      query: query_param,
    ).paginate(page: current_page)

    respond_to do |format|
      format.html do
        if request.xhr?
          headers["Cache-Control"] = "no-cache, no-store"
          render partial: "businesses/pending_collaborators_list", locals: {
            query: query_param,
            pending_collaborators: pending_collaborators,
          }
        else
          render "businesses/pending_collaborators", locals: {
            query: query_param,
            pending_collaborators: pending_collaborators,
          }
        end
      end
    end
  end

  def remove_organization
    return render_404 unless current_user_feature_enabled?(:remove_enterprise_organizations)

    unless org = organization_from_param
      flash[:error] = "Organization #{params[:organization]} does not exist."
      return redirect_to enterprise_path(this_business)
    end

    errors = []
    if GitHub.single_business_environment?
      errors << "Organizations cannot be removed from the global enterprise in this environment."
    end

    unless this_business.owner?(current_user)
      errors << "#{current_user} does not have permission to remove an organization from this enterprise."
    end

    unless this_business.can_self_serve?
      errors << "Please contact sales to modify #{this_business.name}."
    end

    membership = this_business.organization_memberships.where organization: org
    unless membership.any?
      errors << "Organization #{org.login} doesn't belong to #{this_business.name}."
    end

    this_business.remove_organization(org)

    if errors.any?
      flash[:error] = errors.first
    else
      flash[:notice] = "Removed organization #{org.name}."
    end
    redirect_to enterprise_path(this_business)
  end

  def pending_organizations
    created_invitations = this_business
      .organization_invitations
      .with_status(:created)
      .reorder("business_organization_invitations.created_at asc")
    accepted_invitations = this_business
      .organization_invitations
      .with_status(:accepted)
      .reorder("business_organization_invitations.created_at asc")

    render "businesses/organizations/pending_organizations", locals: {
      created_invitations: created_invitations,
      accepted_invitations: accepted_invitations,
    }
  end

  private

  def redirect_billing_manager_to_billing_settings
    return unless this_business.billing_manager?(current_user)
    return if this_business.owner?(current_user)
    return if has_member_business_access? # Also let members of owned orgs bypass the redirect
    redirect_to settings_billing_enterprise_path(this_business)
  end

  def query_param
    return @query if defined?(@query)
    @query = params[:query]
  end

  def login_param
    return @login if defined?(@login)
    @login = params[:login]
  end

  def organization_from_param
    ::Organization.find_by_login params[:organization]
  end

  def organization_counters(org_ids)
    this_business.organizations.where(id: org_ids).each_with_object({}) do |org, counters|
      counters[org.login] = {
        members: org.members.filter_spam_for(current_user).count,
        repos: org.repositories.filter_spam_for(current_user).count,
      }
    end
  end

  def require_provisioning_feature_flag
    render_404 unless GitHub.flipper[:enterprise_idp_provisioning].enabled?(this_business)
  end

  def instrument_show
    GlobalInstrumenter.instrument("enterprise_account.profile_view", {
      enterprise: this_business,
      actor: current_user,
    })
  end
end
