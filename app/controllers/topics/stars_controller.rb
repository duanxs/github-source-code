# frozen_string_literal: true

module Topics
  class StarsController < ApplicationController
    areas_of_responsibility :explore

    skip_before_action :perform_conditional_access_checks
    before_action :authorization_required, except: [:create, :destroy]
    before_action :login_required

    CreateQuery = parse_query <<-'GRAPHQL'
      mutation($input: AddStarInput!) {
        addStar(input: $input) {
          starrable {
            ... on Topic {
              stargazerCount
            }
          }
        }
      }
    GRAPHQL

    def create
      data = platform_execute(CreateQuery, variables: mutate_topic_params)

      render_response_for(data, action: :add_star)
    end

    DestroyQuery = parse_query <<-'GRAPHQL'
      mutation($input: RemoveStarInput!) {
        removeStar(input: $input) {
          starrable {
            ... on Topic {
              stargazerCount
            }
          }
        }
      }
    GRAPHQL

    def destroy
      data = platform_execute(DestroyQuery, variables: mutate_topic_params)

      render_response_for(data, action: :remove_star)
    end

    private

    FindTopicByNameQuery = parse_query <<-'GRAPHQL'
      query($name: String!) {
        topic(name: $name) {
          id
        }
      }
    GRAPHQL

    def topic_global_relay_id
      data = platform_execute(FindTopicByNameQuery, variables: { name: params[:topic_name] })

      data.topic&.id
    end

    def mutate_topic_params
      {
        input: {
          clientMutationId: request_id,
          starrableId: topic_global_relay_id,
          starrableContext: params[:context],
        },
      }
    end

    def render_response_for(data, action:)
      if data.errors.any?
        error_message = data.errors.messages.values.join(", ")

        render json: { message: error_message }, status: :unprocessable_entity
      else
        render json: { count: data.send(action).starrable.stargazer_count }
      end
    end
  end
end
