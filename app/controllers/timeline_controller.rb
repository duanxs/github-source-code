# frozen_string_literal: true

class TimelineController < AbstractRepositoryController
  areas_of_responsibility :code_collab

  ShowQuery = parse_query <<-'GRAPHQL'
    query(
      $id: ID!,
      $before: String,
      $after: String,
      $deferCollapsedThreads: Boolean = false,
      $focusedTimelineItem: ID,
      $focusedReviewThread: ID,
      $focusedReviewComment: ID,
      $hasFocusedReviewThread: Boolean = true,
      $hasFocusedReviewComment: Boolean = true,
      $syntaxHighlightingEnabled: Boolean = true
    ) {
      node(id: $id) {
        ...Views::Timeline::FocusedItem::TimelineOwner
      }
    }
  GRAPHQL

  def show
    return render_404 unless anchor_global_ids[:timeline]

    variables = {
      id: params[:id],
      first: params[:variables][:first].to_i,
      before: params[:variables][:before],
      after: params[:variables][:after],
      focusedTimelineItem: anchor_global_ids[:timeline],
      focusedReviewThread: anchor_global_ids[:review_thread],
      focusedReviewComment: anchor_global_ids[:review_comment],
      hasFocusedReviewThread: anchor_global_ids[:review_thread].present?,
      hasFocusedReviewComment: anchor_global_ids[:review_comment].present?,
      syntaxHighlightingEnabled: syntax_highlighted_diffs_enabled?,
    }

    node = platform_execute(ShowQuery, variables: variables).node

    respond_to do |format|
      format.html do
        render partial: "timeline/focused_item", locals: { timeline_owner: node }
      end
    end
  end

  private

  def object
    return @object if defined?(@object)

    possible_types = [
      Platform::Objects::Issue,
      Platform::Objects::PullRequest,
    ]
    @object = typed_object_from_id(possible_types, params[:id])
  end

  ANCHOR_REGEXPS = [
    /\A(commitcomment)-(\d+)\z/, # comment left on a PR commit from outside PR context
    /\A(commits-pushed)-([0-9a-f]{7})\z/, # "hubot added some commits 19 days ago"
    /\A(pullrequestreview)-(\d+)\z/, # review
    /\A(discussion)_r(\d+)\z/, # individual comment in a review comment thread
    /\A(discussion-diff)-(\d+)(?:[LR]-?\d+)?\z/, # "hubot commented on the diff 19 days ago"
    /\A(diff-for-comment)-(\d+)\z/, # "hubot commented on the diff 19 days ago"
    /\A(event)-(\d+)\z/, # miscellaneous timeline event
    /\A(issuecomment)-(\d+)\z/, # comment left directly on PR/issue
    /\A(ref-commit)-([0-9a-f]{7})\z/, # reference from a commit
    /\A(ref-issue)-(\d+)\z/, # reference from an issue
    /\A(ref-pullrequest)-(\d+)\z/, # reference from a PR
  ]

  def anchor_global_ids
    return @anchor_global_ids if defined?(@anchor_global_ids)

    match = ANCHOR_REGEXPS.each do |regexp|
      match = regexp.match(params[:anchor])
      break match if match
    end

    anchor_type, id = match[1], match[2]
    @anchor_global_ids = map_anchor_to_focus_global_ids(anchor_type, id)
  end

  def map_anchor_to_focus_global_ids(anchor_type, id)
    timeline_global_id = review_thread_global_id = review_comment_global_id = nil

    case anchor_type

    when "commitcomment"
      comment = CommitComment.find(id)
      commit_comment_thread = Platform::Models::PullRequestCommitCommentThread.new(
        object, comment.repository_id, comment.commit_id, comment.path, comment.position
      )
      timeline_global_id = commit_comment_thread.global_relay_id

    when "commits-pushed"
      commit_oid = object.repository.ref_to_sha(id)
      commit = object.repository.commits.find(commit_oid)
      timeline_global_id = Platform::Models::PullRequestCommit.new(object, commit).global_relay_id

    when "pullrequestreview"
      timeline_global_id = PullRequestReview.find(id).global_relay_id

    when "discussion-diff", "diff-for-comment"
      comment = PullRequestReviewComment.find(id)

      timeline_global_id = comment.pull_request_review_thread&.global_relay_id
      review_comment_global_id = comment.global_relay_id

    when "discussion"
      comment = PullRequestReviewComment.find(id)

      if comment.pull_request_review_thread.legacy?
        timeline_global_id = comment.pull_request_review_thread&.global_relay_id
        review_comment_global_id = comment.global_relay_id
      else
        timeline_global_id = comment.pull_request_review_thread&.pull_request_review&.global_relay_id
        review_thread_global_id = comment.pull_request_review_thread&.global_relay_id
        review_comment_global_id = comment.global_relay_id
      end

    when "issuecomment"
      timeline_global_id = IssueComment.find(id).global_relay_id

    when "event"
      timeline_global_id = IssueEvent.find(id).global_relay_id

    when "ref-commit"
      commit_oid = object.repository.ref_to_sha(id)
      event = IssueEvent.where(issue_id: issue_id, commit_id: commit_oid).first
      timeline_global_id = event&.global_relay_id

    when "ref-issue", "ref-pullrequest"
      cross_reference = CrossReference.where(target_id: issue_id, source_id: id).first
      timeline_global_id = cross_reference&.global_relay_id
    end

    {
      timeline: timeline_global_id,
      review_thread: review_thread_global_id,
      review_comment: review_comment_global_id,
    }
  end

  def issue_id
    return @issue_id if defined?(@issue_id)
    @issue_id = object.is_a?(PullRequest) ? object.issue.id : object.id
  end
end
