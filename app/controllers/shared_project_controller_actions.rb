# frozen_string_literal: true

module SharedProjectControllerActions
  def require_projects_enabled
    enabled = if current_repository
      current_repository.repository_projects_enabled?
    elsif this_organization
      this_organization.organization_projects_enabled?
    else
      true
    end
    return render_404 unless enabled
  end

  # When cloning a project from a source, we want to verify that actor has met
  # the required conditional access checks for the potential target owner, in
  # addition to the already-in-place permissions checks for project creation.
  def verify_cloning_conditional_access
    global_id = project_clone_params[:global_target_owner_id]
    owner = Platform::Helpers::NodeIdentification.from_global_id(global_id)
    owner_class_name = owner.first
    owner_id = owner.second.to_i # ["ClassName", "ID"]

    if owner_class_name == "Repository"
      owner_id = Repository.find_by(id: owner_id)&.owner_id
    end

    if unauthorized_ip_whitelisting_organization_ids.include?(owner_id)
      flash[:error] = "You must connect from an allowed IP address"
      return redirect_to project_path(this_project), status: :unauthorized
    end

    if unauthorized_saml_organization_ids.include?(owner_id)
      flash[:error] = "You must authenticate via SAML SSO"
      return redirect_to project_path(this_project), status: :unauthorized
    end
  end
end
