# frozen_string_literal: true

class OauthController < ApplicationController
  areas_of_responsibility :platform

  statsd_tag_actions only: [:authorize, :request_access, :access_token]

  include ControllerMethods::Oauth
  include OauthHelper

  # Ensure that we parse json params before the other filters to ensure that
  # the params hash contains valid data for all requests.
  before_action :parse_json_params

  # Ensure we attmpt to rate limit device authorization requests before making
  # database calls.
  before_action :rate_limit_device_access_token_requests, only: [:access_token]

  before_action :authorization_required,                         only: [:request_access, :authorize]
  before_action :find_application!,                              only: [:request_access, :authorize, :access_token, :request_device_authorization]
  before_action :ensure_application_can_send_callback_requests!, only: [:request_access, :authorize]
  before_action :check_eligibility,                              only: [:request_access, :authorize]
  before_action :validate_requested_scopes,                      only: [:request_access, :authorize]
  before_action :conditional_sudo_filter,                        only: [:authorize]
  before_action :add_csp_exceptions,                             only: [:request_access, :authorize]
  before_action :reject_dangerous_requests,                      only: [:request_access, :authorize]
  before_action :reject_suspended_applications,                  only: [:request_access, :authorize]
  before_action :validate_redirect_uri,                          only: [:request_access, :authorize]
  before_action :reject_invalid_redirect_uri,                    only: [:request_access, :authorize]

  around_action :select_write_database, only: [:authorize, :access_token]

  skip_after_action :block_non_xhr_json_responses, only: [:access_token, :request_device_authorization]

  layout "oauth_authorization"

  include GitHub::RateLimitedRequest
  rate_limit_requests \
    only: [:request_access, :authorize],
    if: :logged_in?,
    max: 50,
    ttl: 1.hour,
    key: :absolute_authorize_rate_limit_key,
    log_key: "oauth-abs-authorize"

  # Public: Authorize an application's request to act on behalf of a user.
  #
  # GET  - Show the user a view to allow/deny this application access.
  #
  # Renders a view or redirects back to the application's callback URL.
  def request_access
    return head :forbidden unless request.get?

    # Is this application allowed to skip user authorization? Immediately grant
    # with the pre-registered scopes and redirect with a code.  Bounty:
    # https://github.com/github/github/issues/117804
    if Apps::Internal.capable?(:can_auto_approve_oauth_authorization, app: @application)
      options = { user_session: user_session }

      if @application.uses_scopes?
        scope = (@application.scopes && @application.scopes.join(","))
        options.merge!(scope: scope, requested_scope: scope)
      end

      @access = @application.grant(
        current_user, options
      )
      GitHub.dogstats.increment("oauth.conversion", tags: ["type:auto"])
      return redirect_back_with_access_code(@access, @redirect_uri)
    end

    strip_analytics_query_string

    if show_sso_selection?(@application)
      more_than_one_target = (unauthorized_saml_organizations.count > 1)
      GitHub.dogstats.increment("oauth.sso.required", tags: ["multiple_targets:#{more_than_one_target}"])

      unauthorized_saml_organizations.each do |org|
        allow_external_redirect_after_post(provider: org.saml_provider)
      end

      locals = {
        application:                     @application,
        authorized_saml_organizations:   authorized_saml_organizations,
        form_data:                       captured_form_data_via_enforcement,
        return_to_url:                   request.url,
        unauthorized_saml_organizations: unauthorized_saml_organizations,
        skip_sso_url:                    skip_sso_url(request.original_url),
      }
      if suggested_org = saml_for_user_session.saml_organizations.find { |o| o.login.downcase == params[:org]&.downcase }
        locals[:suggested_org] = suggested_org
      end

      return render("oauth/identity_management/sso", locals: locals, layout: "session_authentication")
    end

    # Immediately authorize (send an access code) applications that have
    # already been granted access and any of the following are true:
    #   - The new scope request is less permissive than what has already been
    #     authorized.
    #   - The scope parameter was not included.
    #   - The GitHub App is not requesting additional granular user permissions.
    #
    # NOTE: If params[:scope] isn't included in the request, the scopes for
    # what has already been authorized will be used.

    @authorization = current_user.oauth_authorizations.find_by(application: @application)

    # We enforce rate limits for applications that have been previously
    # authorized and will automatically create an access and redirect. Rate
    # limiting prevents unbounded access creation for misconfigured
    # applications that redirect back to the authorize endpoint endlessly.
    unless requesting_prompt?
      if less_permissive? && !authorize_rate_limited? && current_user_matching_request?
        GitHub.dogstats.increment("oauth.conversion", tags: ["type:auto"])
        # If no scopes were requested then we use the set that has been
        # authorized.
        scope = params.has_key?(:scope) ? @scopes : @authorization.scopes
        @access = @application.grant(
          current_user,
          { scope: scope, requested_scope: params[:scope], user_session: user_session },
        )
        return redirect_back_with_access_code(@access, @redirect_uri)
      end
    end

    track_authorize_metrics

    if @application.is_a?(Integration)
      @permissions = @application.default_permissions
      @events      = @application.default_events
      template     = request_access_authorization_template

      render_template_view template, Oauth::AuthorizeIntegrationView,
        authorization: @authorization,
        application:   @application,
        params:        params,
        redirect_uri:  @redirect_uri,
        session:       session,
        scopes:        [],
        permissions:   @permissions,
        events:        @events,
        form_submission_path: oauth_authorize_path,
        rate_limited:  authorize_rate_limited?(increment: false)
    else
      render_template_view "oauth/authorize", Oauth::AuthorizeView,
        authorization: @authorization,
        application: @application,
        params: params,
        redirect_uri: @redirect_uri,
        session: session,
        scopes: @scopes,
        form_submission_path: oauth_authorize_path,
        rate_limited: authorize_rate_limited?(increment: false),
        unauthorized_saml_organizations: unauthorized_saml_organizations,
        unauthorized_ip_whitelisting_organization_ids: unauthorized_ip_whitelisting_organization_ids
    end
  end

  def request_device_authorization
    response_options = DeviceAuthorizationRequest.process(@application, params, request.ip)
    status = :ok

    if response_options.key?(:error)
      status = :bad_request # https://tools.ietf.org/html/rfc6749#section-5.2
      log_data[:error] = response_options[:error]
    end

    render_token_response(response_options, status: status)
  end

  # Public: Authorize an application's request to act on behalf of a user.
  #
  # POST - Handle the form POST to grant this application access to the user by
  #        redirecting to the application's callback URL with an oauth code.
  #
  # Renders a view or redirects back to the application's callback URL.
  def authorize
    if oauth_access_authorized?
      track_authorize_metrics
      @access = @application.grant(
        current_user,
        { scope: @scopes, requested_scope: params[:scope], integration_version_number: params[:integration_version_number], user_session: user_session },
      )
      # Once a user has explicitly authorized an application we clear the limit
      # to minimize collateral damage (ex. a developer testing their OAuth
      # application flow).
      RateLimiter.remove_limit(redirect_loop_authorize_rate_limit_key)

      instrument_integration_listing

      log_data[:verification_code] = secret_last_eight(@access.code)
      redirect_back_with_access_code(@access, @redirect_uri)
    else
      GitHub.dogstats.increment("oauth.conversion", tags: ["error:access_denied"])
      redirect_back_with_error @application,
        error: :access_denied,
        error_description: "The user has denied your application access.",
        error_uri: GitHub.developer_help_url + "/apps/managing-oauth-apps/troubleshooting-authorization-request-errors/#access-denied",
        state: params[:state]
    end
  end

  def access_token
    params[:client_secret] = client_secret # Override before we process
    response_options = OauthAccessTokenRequest.process(@application, log_data, params)

    render_token_response(response_options)
  end

  def success
    render plain: "Success"
  end

  private

  def rate_limit_device_access_token_requests
    return false unless params[:grant_type] == DeviceAuthorizationGrant::GRANT_TYPE

    prefix = "device_auth_token_request"
    suffix = Digest::SHA256.base64digest("#{params[:client_id]}:#{params[:device_code]}")

    rate_limit_key = "#{prefix}:#{suffix}"
    rate_limiter = IncrementalBackoffRateLimiter.check(rate_limit_key, interval: DeviceAuthorizationGrant::INTERVAL)

    return true unless rate_limiter.at_limit?
    GitHub.dogstats.increment("slow_down", tags: ["action:access_token", "subject:device_authorization"])

    response_options = {
      error: :slow_down,
      error_description: "Too many requests have been made in the same timeframe.",
      error_uri: GitHub.developer_help_url,
      interval: rate_limiter.interval
    }

    log_data[:error] = :slow_down
    render_token_response(response_options)
  end

  # We are handling SSO and enforcing external identity
  # sessions elsewhere in the code.
  #
  # This is due to the fact that there isn't a single SSO target.
  #
  # See OauthController#request_access -> `if show_sso_selection?`
  def require_active_external_identity_session?
    false
  end

  # Deliberately opt out of IP allow list enforcement and do enforcement inline.
  def require_whitelisted_ip?
    false
  end

  def should_trigger_auto_install_on_oauth_code_exchange?(application, user)
    application.is_a?(Integration) &&
      GitHub.flipper[:auto_install_apps_on_oauth_code_exchanged].enabled?(application)
  end

  # We are handling SSO and enforcing external identity
  # sessions elsewhere in the code.
  #
  # This is due to the fact that there isn't a single SSO target.
  #
  # See OauthController#request_access -> `if show_sso_selection?`
  def target_for_conditional_access
    :no_target_for_conditional_access
  end

  STATELESS_ACTIONS = %w(access_token request_device_authorization)

  # Allow remote form POSTs to STATELESS_ACTIONS
  def stateless_request?
    STATELESS_ACTIONS.include?(action_name) || super
  end

  def access_denied
    args = params.delete(:allow_signup) == "false" ? { allow_signup: false } : {}

    args.tap do |arg|
      arg[:return_to]  = oauth_request_path(params.permit!.to_hash.symbolize_keys)
      arg[:client_id] = params[:client_id]

      if params[:login].present?
        arg[:login] = params[:login]
      end
    end

    redirect_to login_path(args)
  end

  def parse_json_params
    if request.content_type == Mime[:json]
      data = GitHub::JSON.parse(request.raw_post)
      data.each { |k, v| params[k] = v if !params.key?(k) } if data.is_a?(Hash)
    end
  rescue Yajl::ParseError
    head :bad_request
  end

  def validate_requested_scopes
    @scopes = OauthAccess.normalize_scopes(params[:scope])
  end

  def oauth_access_authorized?
    ["1", "Approve"].include? params[:authorize]
  end

  def refresh_tokens_enabled?
    @application.is_a?(Integration) && @application.user_token_expiration_enabled?
  end

  def redirect_back_with_access_code(access, redirect_uri)
    response_params = {
      code: access.code,
      state: params[:state],
    }
    if @application.github_owned?
      browser_session_id = access.browser_session_id(user_session)
      response_params[:browser_session_id] = browser_session_id
    end

    redirect_back(OauthUtil.redirect_url(redirect_uri, response_params))
  end

  def redirect_back_with_error(app, params)
    redirect_back(OauthUtil.redirect_url(app.callback_url.to_s, params))
  end

  def redirect_back(url)
    redirect_url = url

    if should_meta_redirect?
      render "oauth/meta_refresh_redirect_to_application", locals: { redirect_url: redirect_url }, layout: "redirect"
    else
      redirect_to(redirect_url)
    end
  end

  def should_meta_redirect?
    referrer = begin
      Addressable::URI.parse(request.referrer)
    rescue Addressable::URI::InvalidURIError
      nil
    end
    same_origin_request = referrer && referrer.host == request.host

    # All same origin OAuth controller requests are initiated with a POST that
    # is subject to our `form-action` CSP policy. By checking
    # `same_origin_request` we can force a meta redirect to allow for offsite
    # redirects. These same origin initiated flows include:
    #  * OAuth dance initiated and required a user to login
    #  * OAuth dance initiated and required a user to authorize the application
    return !!same_origin_request
  end

  def find_application
    @application ||= if client_id =~ Integration::KEY_PATTERN_V1
      Integration.find_by_key(client_id)
    else
      OauthApplication.find_by_key(client_id)
    end
  end

  def find_application!
    find_application

    return render_404 if @application.nil?

    case @application
    when Integration
      log_data[:integration_id] = @application.id
    when OauthApplication
      log_data[:oauth_application_id] = @application.id
    end

    @application
  end

  def ensure_application_can_send_callback_requests!
    return unless @application.is_a?(Integration)
    return if @application.can_send_callback_requests?

    return render plain: "This GitHub App must be configured with a callback URL", status: 403
  end

  def get_redirect_uri(app, redirect_uri)
    # Only normalize applications with one callback URL to maintain legacy
    # behavior for existing applications.
    normalize = !app.strict_callback_url_validation?
    if redirect_uri.blank?
      app.try(:callback_url)
    elsif normalize
      Addressable::URI.parse(redirect_uri).normalize.to_s
    else
      redirect_uri
    end
  end

  def reject_suspended_applications
    if @application.suspended?
      redirect_back_with_error @application,
        error: :application_suspended,
        error_description: "Your application has been suspended. #{GitHub.support_link_text}.",
        error_uri: GitHub.developer_help_url + "/apps/managing-oauth-apps/troubleshooting-authorization-request-errors/#application-suspended",
        state: params[:state]
    end
  end

  def validate_redirect_uri
    begin
      @redirect_uri = get_redirect_uri(@application, params[:redirect_uri])
    rescue Addressable::URI::InvalidURIError
      return redirect_back_with_error @application,
        error: :redirect_uri_invalid,
        error_description: "The redirect_uri MUST be a valid URL.",
        error_uri: GitHub.developer_help_url + "/apps/building-oauth-apps/authorization-options-for-oauth-apps/#redirect-urls",
        state: params[:state]
    end
  end

  def reject_invalid_redirect_uri
    unless OauthUtil.valid_redirect_uri?(@application, @redirect_uri)
      redirect_back_with_error @application,
        error: :redirect_uri_mismatch,
        error_description: "The redirect_uri MUST match the registered callback URL for this application.",
        error_uri: GitHub.developer_help_url + "/apps/managing-oauth-apps/troubleshooting-authorization-request-errors/#redirect-uri-mismatch",
        state: params[:state]
    end
  end

  # Private: Prepares the token response in either JSON, XML, or Form values.
  #
  #   # json format
  #   token_response(:json, :access_token => 'foo') # =>
  #     {"access_token":"foo"}
  #   # xml format
  #   token_response(:xml, :access_token => 'foo') # =>
  #     <OAuth><access_token>foo</access_token></OAuth>
  #   # :form is default.
  #   token_response(:access_token => 'foo') # =>
  #     access_token=foo
  #
  # params_or_format - Either a Symbol format (:json, :xml, :form), or
  #                     a Hash of params if :form is desired.
  # params           - Hash of params.
  #
  # Returns String response.
  def token_response(params_or_format, params = nil)
    format = \
      if params.nil?
        params = params_or_format
        :form
      else
        params_or_format
      end
    case format
      when :form then params.to_query
      when :json then GitHub::JSON.encode(params)
      when :xml  then params.to_xml(root: :OAuth,
        dasherize: false, skip_types: true, skip_instruct: true)
    end
  end

  def secret_last_eight(secret)
    secret = secret.to_s
    return secret if secret.length < 8

    secret.to_s[-8..-1]
  end

  def track_authorize_metrics
    metric = request.post? ? "conversions" : "impressions"

    type = if (request.get? && request_organization_approval?(user: current_user, application: @application, scopes: @scopes)) ||
      (request.post? && params[:org_policy])
      "org-policy"
    else
      "simple"
    end

    GitHub.dogstats.increment("oauth", tags: ["metric:#{metric}", "type:#{type}"])
  end

  def client_id
    params[:client_id] || client_id_via_basic_auth
  end

  def client_id_via_basic_auth
    app_creds_via_basic_auth.first
  end

  def client_secret
    params[:client_secret] || client_secret_via_basic_auth
  end

  def client_secret_via_basic_auth
    app_creds_via_basic_auth.second
  end

  def app_creds_via_basic_auth
    return @app_creds if defined?(@app_creds)

    basic = Rack::Auth::Basic::Request.new(request.env)

    @app_creds = if basic.provided? && basic.basic?
      basic.credentials
    else
      []
    end
  end

  def invalid_client_secret?(actual, requested)
    !SecurityUtils.secure_compare(actual.to_s, requested.to_s)
  end

  # Private: Rate limit the authorize action, mostly to minimize unbounded
  # OAuth token creation for misconfigured/poorly coded OAuth applications that
  # endlessly call OauthController#authorize when a failure occurs in their
  # application.
  #
  # increment - The Boolean value that determines if the rate limiter should be
  # incremented.
  #
  # Returns true when this application has exceeded the rate limit and false
  # otherwise.
  def authorize_rate_limited?(increment: true)
    options = {
      max_tries: 10,
      ttl: 1.hour,
      check_without_increment: !increment,
    }

    at_limit = RateLimiter.at_limit?(
      redirect_loop_authorize_rate_limit_key, options
    )

    if increment && at_limit
      GitHub.dogstats.increment("rate_limited", tags: ["action:authorize", "subject:oauth"])
    end
    at_limit
  end

  def redirect_loop_authorize_rate_limit_key
    "oauth:authorize:#{current_user.id}:#{client_id}"
  end

  # Regardless of permissiveness, provide an absolute limit to the number of
  # authorize requests within a reasonable timeframe.
  def absolute_authorize_rate_limit_key
    "oauth-abs:authorize:#{current_user.id}:#{client_id}"
  end

  def add_csp_exceptions
    SecureHeaders.override_x_frame_options(request, SecureHeaders::XFrameOptions::SAMEORIGIN)
    SecureHeaders.append_content_security_policy_directives(
      request,
      frame_ancestors: [SecureHeaders::CSP::SELF],
    )
  end

  def request_access_authorization_template
    # If this is the Learning Lab app, render the custom OAuth approval page
    if !GitHub.enterprise? &&
      (
        @application == Apps::Internal.integration(:github_learning_lab) ||
        @application == Apps::Internal.integration(:galileo)
      )
      return "oauth/authorize_learning_lab"
    end

    # Otherwise, return the regular template
    "oauth/authorize_integration"
  end

  def render_token_response(response_options, status: :ok)
    respond_to do |format|
      format.any(:html, :url_encoded_form) do
        render body: token_response(response_options), content_type: "application/x-www-form-urlencoded", status: status
      end

      format.xml do
        render xml: token_response(:xml, response_options), status: status
      end

      format.json do
        render json: token_response(:json, response_options), status: status
      end
    end
  end

  def requesting_prompt?
    params[:prompt] == "consent"
  end

  def less_permissive?
    return false unless @authorization

    case @authorization.application_type
    when "Integration"
      @authorization.upgradedable_without_user_permission?
    else
      @authorization.safely_less_permissive?(@scopes)
    end
  end

  def current_user_matching_request?
    return true unless params.key?(:login)
    current_user.login == params[:login]
  end

  def skip_sso_url(request_url)
    uri = Addressable::URI.parse(request_url)

    additional = { skip_sso: true }
    uri.query_values = (uri.query_values || {}).merge(additional)

    uri.to_s
  end
end
