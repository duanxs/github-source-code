# frozen_string_literal: true

class PasswordResetsController < ApplicationController
  # The following actions *don't* access protected organization resources
  # so they don't require conditional access checks.
  skip_before_action :perform_conditional_access_checks, only: %w(
    new
    create
    edit
    check_otp
    fallback
    update
  )

  # Exempt specific actions from the GHPI bootstrap check to allow the
  # initial site administrator to set their account password.
  skip_before_action :private_instance_bootstrap_check, only: %w(new create edit update)

  before_action :load_reset, only: [:edit, :update, :check_otp, :fallback]
  before_action :sanitize_analytics_location, :sanitize_hydro_location,
    only: [:edit, :update, :check_otp, :fallback]
  before_action :sanitize_referrer
  before_action :no_external_auth

  layout :authentication_layout

  include GitHub::RateLimitedRequest
  rate_limit_requests \
    only: :create,
    max: 15,
    ttl: 30.minutes,
    key: :password_reset_rate_limit_key,
    at_limit: :password_reset_rate_limit_render

  def new
    @failed_auth_email = session[:failed_auth_email]
    render "password_resets/new"
  end

  def create
    reset = PasswordReset.create user: current_user, email: params[:email]
    if reset.valid?
      GitHub.dogstats.increment("password_reset", tags: ["action:create", "valid:true", "verified_email:#{reset.verified_email?}"])
      render "password_resets/create"
    else
      GitHub.dogstats.increment("password_reset", tags: ["action:create", "valid:false", "error:#{reset.error}"])
      if reset.user&.suspended?
        redirect_to suspended_url
      else
        flash.now[:error] = reset.error_message
        render "password_resets/new"
      end
    end
  end

  def authentication_user
    return @authentication_user if defined? @authentication_user

    # When someone is visiting the password reset page, we need to make sure the
    # authentication user we use is the one who is resetting their password, not
    # the one who is (maybe) logged in. That value is `@user` when set by `edit`
    # in the relevant code path.
    if defined? @user
      @authentication_user = @user
      return @user
    end

    # Fall back to the current user by default.
    @authentication_user = current_user
    return @authentication_user
  end

  SMS_RESEND_LIMIT_OPTIONS = {
    max_tries: 5,
    ttl: 15.minutes,
  }
  def edit
    flash.now[:override_octolytics_location] = true
    if @reset.forced_weak_password_reset?
      flash.now[:error] = @reset.weak_password_reset_message
    end

    @user = @reset.user
    if @reset.verify_two_factor? && @user.two_factor_credential.sms_enabled?
      rate_limit_key = "password-reset-sms-2fa:#{@user.id}"
      unless RateLimiter.at_limit?(rate_limit_key, SMS_RESEND_LIMIT_OPTIONS)
        @reset.user.two_factor_credential.send_otp_sms
      end
    end
    render "password_resets/edit"
  end

  def check_otp
    unless params[:u2f_response].blank? || webauthn_sign_challenge_from_request.blank?
      origin = Addressable::URI.new(scheme: request.scheme, host: request.host).to_s
      unless @reset.verify_u2f(origin, webauthn_sign_challenge_from_request, params[:u2f_response])
        flash[:error] = "Security key authentication failed"
      end

      return redirect_to edit_password_reset_path(@reset.token)
    end

    otp = TwoFactorCredential.normalize_otp(params[:otp])

    if AuthenticationLimit.at_any?(two_factor_login: @reset.user.login, increment: true, actor_ip: request.remote_ip)
      flash[:error] = "Too many incorrect two-factor codes. Try again later."
    elsif !@reset.verify_two_factor(otp)
      flash[:error] = "Invalid two-factor code"
    end

    redirect_to edit_password_reset_path(@reset.token)
  end

  # Send OTP SMS to fallback number.
  def fallback
    return render_404 unless request.xhr?

    unless @reset.user.two_factor_authentication_enabled? && @reset.user.two_factor_credential.fallback_enabled?
      return head :forbidden
    end

    @reset.user.two_factor_credential.send_fallback_sms(:reset)
    head :ok
  end

  def update
    GitHub.context.push(spamurai_form_signals: spamurai_form_signals)

    if @reset.apply(password: params[:password], password_confirmation: params[:password_confirmation])
      anonymous_flash[:notice] = "New password set successfully."

      if forgot_password_email = @reset.user.emails.find_by_email(@reset.email)
        GitHub.dogstats.increment("password_reset", tags: [
          "action:update",
          "valid:true",
          "forced_reset:#{@reset.forced_weak_password_reset?}",
          "verified_email:#{forgot_password_email.verified?}",
        ])

        if GitHub.email_verification_enabled? && forgot_password_email.unverified?
          forgot_password_email.verify!
        end
      end

      if @reset.user.sign_in_analysis_enabled?
        display_name = AuthenticatedDevice.generated_display_name(parsed_useragent)
        current_device = AuthenticatedDevice.upsert!(@reset.user, device_id: current_device_id, display_name: display_name)
        if current_device.verify!(display_name: display_name)
          @reset.user.instrument_unverified_device(:device_verification_success, current_device, reason: :password_reset)
        end
      end

      redirect_to_login
    else
      @user = @reset.user

      GitHub.dogstats.increment("password_reset", tags: [
        "action:update",
        "valid:false",
        "weak_password:#{@user.provided_weak_password?}",
      ])

      if @user.provided_weak_password?
        flash.now[::CompromisedPassword::WEAK_PASSWORD_KEY] = true
      else
        flash.now[:error] = @reset.error_message
      end
      render "password_resets/edit"
    end
  end

  # Can the user to U2F instead of traditional 2FA?
  #
  # Returns booleran.
  def password_reset_u2f_available?
    security_key_enabled_for_request? && @reset.user.has_registered_security_key?
  end
  helper_method :password_reset_u2f_available?

  private
  # Private: Load and validate the password reset token.
  #
  # Sets @reset or redirects to the "new" action if there is an error.
  def load_reset
    @reset = PasswordReset.find_by_token params[:token]
    unless @reset && @reset.valid?
      flash[:error] = "It looks like you clicked on an invalid password reset link. Please try again."
      redirect_to action: :new
    end
  end

  # Private: The String key to use for rate-limiting requests.
  def password_reset_rate_limit_key
    "forgot-password:#{request.remote_ip}"
  end

  # Private: Render a custom response when at the request rate limit.
  def password_reset_rate_limit_render
    flash[:error] = "Too many attempts. Please wait a while and try again."
    render "password_resets/new"
  end

  # Provide a helpful message if a user hits this and the instance is using
  # external auth (without a fallback to built-in auth).
  def no_external_auth
    return if GitHub.auth.allow_builtin_users?
    flash[:error] = "You cannot reset your password because GitHub Enterprise is using an external authentication provider"
    redirect_to "/"
  end

  def sanitize_analytics_location
    override_analytics_location "/password_reset/<password-reset-token>"
  end

  def sanitize_hydro_location
    if hydro_context[:enabled]
      hydro_context.merge!(path: "/password_reset/:token")
    end
  end
end
