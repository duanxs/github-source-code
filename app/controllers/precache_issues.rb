# frozen_string_literal: true

module PrecacheIssues
  include DiscussionPreloadHelper

  # Warms the caches and preloads the associations for all given timeline items.
  # This should be called any time the issue timeline is going to be rendered.
  def precache_issue_timeline(issue, timeline_items)
    issue.prefill_timeline_associations(timeline_items, show_mobile_view: show_mobile_view?)
    preload_discussion_group_data(timeline_items, mobile: show_mobile_view?)
  end
end
