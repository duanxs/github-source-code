# frozen_string_literal: true

module MemexItemsControllerActions
  def create_memex_item(memex:)
    item = memex.build_item(
      creator: current_user,
      draft_issue_title: create_memex_item_params.dig(:content, :title),
      issue_or_pull: this_content
    )

    memex.save_with_lowest_priority!(item) if item.valid?

    if item.persisted?
      prefill_memex_item_associations(item, columns: memex.visible_columns)
      render(
        json: camelize_keys(
          memex_project_item: item.to_hash(columns: memex.visible_columns)
        ),
        status: :created,
      )
    else
      render(json: { errors: item.errors.full_messages }, status: :unprocessable_entity)
    end
  end

  def update_memex_item(item:)
    success = if updating_item_priority?
      prioritization_options = previous_item ? { after: previous_item } : { position: :top }
      item.memex_project.prioritize_dependent!(item, **prioritization_options)
    elsif update_column_value_params
      item.set_column_value(column_to_update, update_column_value_params[:value], current_user)
    else
      true
    end

    if success
      columns = updating_item_priority? ? [] : item.memex_project.visible_columns
      prefill_memex_item_associations(item, columns: columns)
      render(json: camelize_keys(memex_project_item: item.to_hash(columns: columns)))
    else
      render(json: { errors: item.errors.full_messages }, status: :unprocessable_entity)
    end

  rescue GitHub::Prioritizable::Context::LockedForRebalance
    render_json_error(
      error: "This project is undergoing maintenance.",
      status: :service_unavailable
    )
  end

  def destroy_memex_items(memex:)
    old_item = memex
      .memex_project_items
      .find_by(id: destroy_memex_items_params[:memex_project_item_ids].first)
      &.destroy

    head(old_item&.destroyed? ? :ok : :unprocessable_entity)
  end

  def create_memex_item_params
    underscored_params
      .require(:memex_project_item)
      .permit(:content_type, content: [:title, :id, :repository_id])
  end

  def update_memex_item_params
    underscored_params
  end

  def update_column_value_params
    @update_column_value_params = update_memex_item_params[:memex_project_column_values]&.first
  end

  def update_request?
    underscored_params.has_key?(:memex_project_item_id)
  end

  def updating_item_priority?
    update_memex_item_params.has_key?(:previous_memex_project_item_id)
  end

  def destroy_memex_items_params
    underscored_params.permit(:org, :user_id, :memex_number, memex_project_item_ids: [])
  end

  def require_valid_content_parameters
    verify_content_params(create_memex_item_params)
  end

  def require_this_repository
    return if create_memex_item_params[:content_type] == DraftIssue.name
    render_404 unless this_repository&.readable_by?(current_user)
  end

  def require_this_content
    return if create_memex_item_params[:content_type] == DraftIssue.name
    render_404 unless this_content&.readable_by?(current_user)
  end

  def require_this_item
    return render_404 unless this_item&.content
    return if this_item.content.is_a?(DraftIssue)

    issue = this_item.content.is_a?(PullRequest) ? this_item.content.issue : this_item.content

    return render_404 unless issue&.repository
    return render_404 unless issue.readable_by?(current_user)
    return render_404 if current_user.blocked_by?(issue.repository.owner_id)
    authorize_content(:issue, repo: issue.repository)
  end

  def require_priorities
    return if update_request? && !updating_item_priority?

    if this_memex.memex_project_items.exists?(priority: nil)
      render_json_error(error: "This project must be rebalanced.", status: :service_unavailable)
    end
  end

  def require_previous_item
    return unless updating_item_priority?
    return unless update_memex_item_params[:previous_memex_project_item_id].present?
    render_404 unless previous_item
  end

  NEWLINE_REGEX = Regexp.union(["\r\n", "\r", "\n"])

  def require_valid_column_value_parameters
    return unless update_column_value_params.present?

    if update_memex_item_params[:memex_project_column_values].length > 1
      # We explicitly disallow this scenario because we have not yet optimized for it.
      render_json_error(
        error: "You must provide only one column value in each request",
        status: :unprocessable_entity,
      )
      return
    end

    unless column_to_update
      render_json_error(error: "Column not found", status: :not_found)
      return
    end

    case column_to_update.data_type.to_sym
    when :title
      unless update_column_value_params[:value].try(:fetch, :title, nil)
        render_json_error(
          error: "Value must be an object that contains a \"title\" key",
          status: :unprocessable_entity
        )
      end
    when :text
      unless update_column_value_params[:value].is_a?(String)
        render_json_error(
          error: "Value must be a string",
          status: :unprocessable_entity
        )
        return
      end

      if NEWLINE_REGEX.match(update_column_value_params[:value])
        render_json_error(
          error: "Value must not contain a newline",
          status: :unprocessable_entity
        )
      end
    when :assignees, :labels
      ids = update_column_value_params[:value]
      unless ids.is_a?(Array) && ids.all? { |value| value.to_i.positive? }
        render_json_error(
          error: "Value must be an array of ids",
          status: :unprocessable_entity
        )
      end
    when :milestone
      milestone = update_column_value_params[:value]
      unless milestone == "" || milestone.to_i > 0
        render_json_error(
          error: "Value must be a milestone_id or an empty string to clear",
          status: :unprocessable_entity
        )
      end
    else
      render_json_error(
        error: "Updates to a column of type #{column_to_update.data_type} are not supported",
        status: :unprocessable_entity
      )
    end
  end

  private

  def prefill_memex_item_associations(item, columns:)
    GitHub::PrefillAssociations.for_memex_project_items([item], columns: columns)
  end

  def this_item
    return @this_item if defined?(@this_item)
    @this_item = this_memex
      .memex_project_items
      .includes(:content)
      .find_by(id: item_id)
  end

  def previous_item
    return @previous_item if defined?(@previous_item)
    @previous_item = this_memex
      .memex_project_items
      .find_by(id: update_memex_item_params[:previous_memex_project_item_id])
  end

  def this_content
    return @this_content if defined?(@this_content)
    @this_content = find_content(
      repository: this_repository,
      content_type: create_memex_item_params[:content_type],
      content_id: create_memex_item_params[:content][:id],
    )
  end

  def item_id
    case action_name
    when "update"
      update_memex_item_params[:memex_project_item_id]
    when "suggested_assignees", "suggested_labels", "suggested_milestones"
      underscored_params[:memex_project_item_id]
    end
  end

  def column_to_update
    return @column_to_update if defined?(@column_to_update)
    @column_to_update = this_memex.find_column_by_name_or_id(
      update_column_value_params&.fetch(:memex_project_column_id, nil)
    )
  end
end
