# frozen_string_literal: true

class IssueTransfersController < AbstractRepositoryController

  before_action :login_required

  TransferIssueMutation = parse_query <<-'GRAPHQL'
    mutation($issueId: ID!, $repositoryId: ID!) {
      transferIssue(input: { issueId: $issueId, repositoryId: $repositoryId }) {
        issue {
          resourcePath
        }
      }
    }
  GRAPHQL

  def create
    result = platform_execute(TransferIssueMutation, variables: {
      issueId: params[:issue_id],
      repositoryId: params[:repository_id],
    })

    if result.errors.any?
      redirect_to :back
    else
      flash[:notice] = "Issue transfer in progress"
      redirect_to result.transfer_issue.issue.resource_path.to_s
    end
  end
end
