# frozen_string_literal: true

module ComponentKit
  class TemplateController < AbstractRepositoryController
    areas_of_responsibility :ce_extensibility

    before_action :login_required
    before_action :require_xhr
    before_action :ensure_feature_enabled

    def show
      template_path = params[:template_path]
      surface_id = params[:surface_id]

      # TODO: Support more kinds of templates.
      template = IssueCommentTemplates.new(current_repository)[template_path]

      # These errors should never be seen by a user. We should always handle it further up.
      # For example, never give the user the option to load a template if we know it is invalid.
      if surface_id.nil?
        # No id was provided.
        render status: :bad_request, plain: "Missing query parameter: surface_id"
      elsif template.nil?
        # The template doesn't exist.
        # Not found.
        render_404
      elsif !template.valid?
        # The template is invalid.
        render status: :unprocessable_entity, plain: template.errors
      else
        # Happy path: the template exists and is valid.
        render ::AdaptiveCards::AdaptiveCard.new(
          surface_id,
          {
            "$schema" => "http://adaptivecards.io/schemas/adaptive-card.json",
            "type" => "AdaptiveCard",
            "version" => "1.2",
            "body" => template.body
          },
          validate: false
        )
      end
    end

    private

    def ensure_feature_enabled
      return render_404 unless current_repository.structured_issue_comment_templates_enabled?
    end
  end
end
