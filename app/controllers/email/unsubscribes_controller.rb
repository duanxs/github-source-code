# frozen_string_literal: true

class Email::UnsubscribesController < ApplicationController
  areas_of_responsibility :email, :explore

  # Because this controller doesn't deal with protected organization resources,
  # we can safely `skip_before_action` its actions.
  skip_before_action :perform_conditional_access_checks, only: [:show]

  before_action :login_required
  around_action :select_write_database, only: [:show]

  def show
    @subscription = NewsletterSubscription.unsubscribe(params[:token])
    if @subscription.blank?
      redirect_to settings_user_emails_path(anchor: "preferences")
      return
    end
    render "show"
  end
end
