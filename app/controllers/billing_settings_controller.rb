# frozen_string_literal: true

class BillingSettingsController < ApplicationController
  include BillingSettingsHelper
  include OrganizationsHelper
  include SurveyHelper

  # Always render a 404 when billing is disabled.
  # See Application#ensure_billing_enabled.
  before_action :ensure_billing_enabled

  before_action :login_required
  before_action only: [:upgrade] do
    check_ofac_sanctions(target: target)
  end
  before_action :ensure_target
  before_action :ensure_target_is_billable
  before_action :new_pricing_model_check, only: [:plans]

  rescue_from ::Timeout::Error, with: :payment_processor_error

  MAX_PRIVATE_REPOS_FOR_DOWNGRADE_COUNTS = 10

  # Update the VAT ID and extra freeform text that appears on receipts.
  def extra_update
    target.update \
      billing_extra: params[:billing_extra] && params[:billing_extra].strip

    if params[:vat_code] && target.customer.present?
      target.customer.update \
        vat_code: params[:vat_code].strip
    end

    flash[:notice] = "Updated your optional contact information, " +
      "it will show up on your next receipt."

    redirect_to billing_path
  end

  def client_token
    return render_404 unless request.xhr?

    render plain: GitHub::Billing.generate_braintree_client_token.to_s
  end

  UserBillingQuery = parse_query <<-'GRAPHQL'
    query($login: String!) {
    ...Views::BillingSettings::PaymentHistory::Fragment
    }
  GRAPHQL

  def payment_history
    if target.organization?
      login = current_organization_for_member_or_billing.login
    else
      login = target.login
    end

    data = platform_execute UserBillingQuery, variables: {
      login: login,
    }

    render "billing_settings/payment_history", locals: {
      target: target,
      data: data,
    }
  end

  def receipt
    transaction = target.billing_transactions.find_by_transaction_id(params[:id])

    if transaction && transaction.success?
      @receipt = ::Billing::Receipt.new(transaction)
      respond_to do |format|
        format.text do
          render "mailers/billing_notifications/receipt"
        end
        format.pdf do
          send_data(@receipt.to_pdf,
            filename: @receipt.pdf_filename,
            type: "application/pdf",
            disposition: "inline",
          )
        end
        format.html do
          send_data(@receipt.to_pdf,
            filename: @receipt.pdf_filename,
            type: "application/pdf",
            disposition: "attachment",
          )
        end
      end
    else
      render_404
    end
  end

  # If the user has an external customer record (e.g. Braintree customer or
  # Zuora account), then update their existing credit card on file. Otherwise,
  # create a new external customer record with the given credit card details.
  def update_credit_card
    old_seat_count = target.seats

    if no_payment_details?
      flash[:error] = "Please enter your payment information."
      redirect_to :back
    else
      details = payment_details.merge(actor: current_user)

      result = if plan_status.plan_changed?
        ga_params = set_ga_params
        payment_difference = payment_difference_for_new_plan(target, params[:plan])
        change_plan_result = change_plans(target, params[:plan], details)
        publish_billing_plan_changed_for(
          actor: current_user,
          user: target,
          old_seat_count: old_seat_count,
          new_seat_count: target.seats,
          old_plan_name: old_plan_name,
          new_plan_name: new_plan_name,
        ) if change_plan_result.success?
        change_plan_result
      else
        set_payment_method(target, details)
      end

      if result.success?
        updated_or_added = target.has_credit_card? ? "updated" : "added"

        publish_payment_method_changed_for(
          actor: current_user,
          account: target,
          payment_method: target.payment_method,
        )

        if payment_details_includes_paypal?
          flash[:notice] = "Your PayPal account has been successfully added. Thanks!"
        else
          flash[:notice] = "Your credit card has been successfully #{updated_or_added}. Thanks!"
        end
        if plan_status.plan_changed?
          flash[:notice] += " Your request for changing plans to #{target.plan.display_name} was also successful."
          flash[:analytics_location_params] = ga_params
        end
        flash[:notice] += " We are processing your payment." if target.disabled?

        analytics_event_for_plan_change(payment_difference) if plan_status.cost_changed?

        if params[:return_to].present?
          safe_redirect_to params[:return_to], fallback: billing_path
        else
          redirect_to billing_path
        end
      else
        error_message = result.error_message

        GitHub.context.push(error: error_message.to_s)
        Audit.context.push(error: error_message.to_s)

        if error_message.respond_to?(:metadata)
          GitHub.context.push(metadata: error_message.metadata)
          Audit.context.push(metadata: error_message.metadata)
        end

        flash[:error] = error_message.to_s
        redirect_to target_payment_method_path(target)
      end
    end
  end

  # Update plan_duration (monthly or yearly billing cycle).
  def cycle_update
    result = Billing::CycleUpdate.perform(target, params[:plan_duration], actor: current_user)

    if result.success?
      flash[:notice] = "Your request to change to #{target.pending_cycle_plan_duration}ly billing was successful."
    else
      flash[:error] = "#{result.error_message} #{GitHub.support_link_text}."
    end

    redirect_to billing_path
  end

  def plans
    @survey = find_survey
    render "billing_settings/plans"
  end

  def upgrade
    if target.no_verified_emails?
      flash[:error] = "At least one email address must be verified to upgrade your plan"
      return redirect_to target_billing_path(target)
    end

    if target.spammy?
      flash[:error] = "You cannot upgrade your plan because your account has been flagged. If you believe this is a mistake, contact support"
      return redirect_to target_billing_path(target)
    end

    duration = params[:plan_duration] || target.plan_duration
    plan = GitHub::Plan.find(params[:plan])
    plan ||= target.organization? ? GitHub::Plan.business : GitHub::Plan.pro
    plan = MunichPlan.wrap(plan, target: target)

    unless valid_plan_or_duration_change?(plan)
      return redirect_to billing_path
    end

    if target.organization?
      @plan_change = Billing::PlanChange::PerSeatPricingModel.new(
        target,
        plan_duration: duration,
        new_plan: plan,
        seats: seat_count,
      )
    else
      @plan_change = Billing::PlanChange.new \
        target.subscription,
        Billing::Subscription.for_account(
          target,
          plan: plan,
          duration_in_months: duration == User::MONTHLY_PLAN ? 1 : 12,
        )
    end

    if params[:return_to].present?
      @return_to = params[:return_to]
    end

    respond_to do |format|
      format.html do
        render "billing_settings/upgrade"
      end
      format.json do
        render json: upgrade_json(@plan_change)
      end
    end
  end

  # Change plan and apply coupon.
  def cc_update
    ga_params = set_ga_params
    details = payment_details.merge(actor: current_user)
    payment_difference = payment_difference_for_new_plan(target, params[:plan])
    result = change_plans(target, params[:plan], details, params[:plan_duration])
    old_seat_count = target.seats

    if result.success?
      msg = target.pending_cycle_change ? "will be" : "has been"
      flash[:notice] = "The plan change was successful. @#{target.login} #{msg} updated to the #{target.pending_cycle_plan.display_name} #{target.pending_cycle_plan_duration}ly plan."
      flash[:analytics_location_params] = ga_params
      analytics_event_for_plan_change(payment_difference) if plan_status.cost_changed?

      publish_billing_plan_changed_for(
        actor: current_user,
        user: target,
        old_seat_count: old_seat_count,
        new_seat_count: target.seats,
        old_plan_name: old_plan_name,
        new_plan_name: new_plan_name,
      )

      redirect_to billing_path
    else
      flash[:error] = "#{result.error_message} #{GitHub.support_link_text}."

      if target.over_repo_seat_limit?
        redirect_to settings_user_repositories_path
      else
        redirect_to billing_path
      end
    end
  end

  # update user personal profile
  def user_profile_update
    profile = target.user_personal_profile.presence || target.build_user_personal_profile
    if profile.update(user_profile_params)
      flash[:notice] = "Successfully updated payment information."
    else
      flash[:error] = "An error occurred while saving payment information."
    end
    redirect_back(fallback_location: billing_path)
  end

  # Downgrade the plan and ask exit survey
  def downgrade_with_exit_survey
    @plan = GitHub::Plan.find(params[:plan])
    @plan = MunichPlan.new(@plan, target: target)
    @survey = find_survey

    SurveyAnswer.save_as_group(current_user.id, @survey.id, params[:answers])

    cc_update
  end

  def update_email
    target.billing_email = params[:organization][:billing_email]

    if target.save
      flash[:notice] = "Successfully updated billing email for #{target.login}."
    else
      flash[:error] = target.errors.full_messages.to_sentence
    end

    redirect_to settings_org_billing_path(target)
  end

  def target_for_conditional_access
    return :no_target_for_conditional_access unless target
    target
  end

  def downgrade_features_count
    target = case params[:target_type]
    when "organization"
      Organization.find(params[:id])
    when "user"
      User.find(params[:id])
    else
      return head :bad_request
    end

    repo_ids = target.private_repositories.limit(MAX_PRIVATE_REPOS_FOR_DOWNGRADE_COUNTS).pluck(:id)
    results = {}

    downgrade_features_with_counts.each do |feature, label|
      next unless params[feature].present?

      count = count_for_downgrade_feature(feature, repo_ids)

      results[feature] = if count >= MAX_PRIVATE_REPOS_FOR_DOWNGRADE_COUNTS
        "#{MAX_PRIVATE_REPOS_FOR_DOWNGRADE_COUNTS}+"
      else
        # The count in the JSON response will need to be a string
        # in order to support the potential of a "X+" response.
        count.to_s
      end
    end

    render json: { counts: results }
  rescue NoMethodError, NameError, ActiveRecord::RecordNotFound
    render json: { error: "Unable to read the request object" }, status: :not_found
  end

  private

  # Private: get the number of private repos using a given feature.
  #
  # feature - The symbol name of the feature which we are attempting to count
  # repo_ids - An array of repo IDs for which the feature will be counted
  #
  # Returns an integer.
  def count_for_downgrade_feature(feature, repo_ids)
    return 0 if repo_ids.blank?

    case feature
    when :protected_branches
      ProtectedBranch.where(repository_id: repo_ids).count
    when :draft_prs
      PullRequest.where(repository_id: repo_ids, work_in_progress: true).count
    when :pages
      Page.where(repository_id: repo_ids).count
    when :wikis
      RepositoryWiki.where(repository_id: repo_ids).count
    else
      0
    end
  end

  def valid_plan_or_duration_change?(plan)
    if changing_plan?(plan)
      target.can_change_plan_to?(plan)
    else
      changing_duration?
    end
  end

  def user_profile_params
    params.require(:user_personal_profile).permit(
      :first_name,
      :last_name,
      :middle_name,
      :region,
      :city,
      :country_code,
      :postal_code,
      :address1,
      :address2
    )
  end

  def changing_plan?(plan)
    if plan == target.plan && Billing::EnterpriseCloudTrial.new(current_organization).active?
      true
    else
      plan != target.plan
    end
  end

  # Private: Change plans for the target.
  #
  # target          - A User or organization.
  # plan            - String plan name.
  # payment_details - A Hash of payment_details.
  # plan_duration   - (optional) String of "month" or "year".
  #
  # Returns a GitHub::Billing::Result.
  def change_plans(target, plan, payment_details, plan_duration = nil)
    if target.has_billing_record? || plan == "free"
      GitHub::Billing.change_subscription(target, actor: current_user, plan: plan, payment_details: payment_details, plan_duration: plan_duration)
    else
      GitHub::Billing.paid_upgrade(target, plan, payment_details, plan_duration)
    end
  end

  # Private: Update payment method on file for the target.
  #
  # target          - A User or organization.
  # payment_details - A Hash of payment_details.
  #
  # Returns a GitHub::Billing::Result.
  def set_payment_method(target, payment_details)
    timeout(28) do
      if target.has_billing_record?
        GitHub::Billing.update_payment_method(target, payment_details.merge(unlock_billing: true))
      else
        GitHub::Billing.create_customer(target, payment_details)
      end
    end
  end

  def find_survey
    Survey.find_by_slug("org_downgrade")
  end

  helper_method :target
  def billing_path
    if target.organization?
      "/organizations/#{target.name}/settings/billing"
    else
      "/settings/billing"
    end
  end

  def plans_path
    if target.organization?
      "/organizations/#{target.name}/billing/plans"
    else
      "/account/plans"
    end
  end

  def plan_status
    @plan_status ||= Billing::PlanTransition.new(target, params[:plan])
  end

  # This gathers params for Google Analytics params and it has to be
  # called *before* the actual plan change happens, while the tracking
  # flash can only be set *after* the plan change has succeeded.
  def set_ga_params
    { target: plan_status.target_type,
      plan: plan_status.new_plan.name,
      billing: plan_status.billing?,
      action: plan_status.cost_change_label,
    }
  end

  # Ensures that GA custom events get fired:
  # "User: plan upgrade/downgrade"
  # "Orgs: plan upgrade/downgrade"
  def analytics_event_for_plan_change(payment_difference)
    analytics_ec_purchase(target, payment_difference, "Upgrade")
    analytics_event \
      category: (plan_status.organization?? "Orgs" : "User"),
      action: "plan #{plan_status.cost_change_label}",
      label: "#{plan_status.old_plan.display_name} -> #{plan_status.new_plan.display_name}",
      redirect: true
  end

  # Calculate prorated payment difference of switching to a new plan
  #
  # Returns a BigDecimal amount in dollars
  def payment_difference_for_new_plan(target, new_plan_name)
    plan = GitHub::Plan.find(new_plan_name) || target.plan
    plan = MunichPlan.wrap(plan, target: target)

    target.payment_difference plan, use_balance: true
  end

  def ensure_target
    render_404 if params[:target] && target.nil?
  end

  def target
    @target ||= target!
  end

  def target!
    if params[:target] == "organization"
      org = current_organization_for_member_or_billing
      if org && org.billing_manageable_by?(current_user)
        org
      end
    else
      current_user
    end
  end

  def ensure_target_is_billable
    render_404 unless target.billable?
  end

  def new_pricing_model_check
    redirect_to billing_path if target.plan.free? || target.plan.pro? || target.plan.business?
  end

  def changing_duration?
    params[:plan_duration].present? && params[:plan_duration] != target.plan_duration
  end

  def payment_processor_error
    flash[:error] = "There was a problem communicating with our payment provider. Please try again."
    redirect_to :back
  end

  def old_plan_name
    plan_status&.old_plan&.name
  end

  def new_plan_name
    plan_status&.new_plan&.name
  end

  def publish_billing_plan_changed_for(actor:,
                                       user:,
                                       new_seat_count: 0,
                                       old_seat_count: 0,
                                       old_plan_name: "",
                                       new_plan_name:)
    GlobalInstrumenter.instrument(
      "billing.plan_change",
      actor_id: actor.id,
      user_id: user.id,
      old_plan_name: old_plan_name,
      old_seat_count: old_seat_count,
      new_plan_name: new_plan_name,
      new_seat_count: new_seat_count,
    )
  end

  def publish_payment_method_changed_for(actor:, account:, payment_method:)
    GlobalInstrumenter.instrument(
      "billing.payment_method.addition",
      actor_id: actor.id,
      account_id: account.id,
      payment_method_id: payment_method.id,
    )
  end

  def upgrade_json(plan_change)
    view = BillingSettings::ConfirmationView.new(account: target, plan_change: plan_change)

    {
      seats: params[:seats],
      selectors: {
        ".unstyled-renewal-price" => view.renewal_list_price.format,
        ".unstyled-next-plan-price" => view.next_plan_price.format,
        ".unstyled-payment-due" => view.changing_duration? ? view.final_price(github_only: false).format : view.final_price(github_only: true).format,
      },
    }
  end

  def seat_count
    if target.upgrading_from_trial?
      params[:seats] || Organization::LicenseAttributer.new(target).unique_count
    end
  end
end
