# frozen_string_literal: true

class OauthTokensController < ApplicationController
  areas_of_responsibility :platform

  statsd_tag_actions only: :create

  skip_before_action :perform_conditional_access_checks

  before_action :login_required
  before_action :check_eligibility, only: [:new, :create]
  before_action :sudo_filter, except: [:destroy, :index, :remove_authorization]
  before_action :set_cache_control_no_store, only: [:index]

  javascript_bundle :settings

  # When the user selects all scopes and we normalize it down to 3, we want to tell them why
  NORMALIZED_MESSAGE = "Some of the scopes you’ve selected are included in" \
    " other scopes. Only the minimum set of necessary scopes has been saved."

  # It is preferable to synchronously revoke all personal access tokens at once,
  # so that the tokens will already be gone when the user is redirected back to
  # their personal access tokens page. But, if the user has a large number of
  # tokens, we need to do it asynchronously in a job.
  MAXIMUM_TOKENS_REVOKE_ALL = 100

  PER_PAGE = 10

  def index
    # Temporary join used to filter out bad data in the DB where an access has no authorization.
    # Eventually this join can be removed when the bad data in the DB is cleaned up.
    # https://github.com/github/github/issues/117741
    personal_tokens = current_user
      .oauth_accesses.personal_tokens
      .joins(:authorization).order(created_at: :desc)
      .paginate(page: current_page, per_page: PER_PAGE)

    render_template_view "oauth_tokens/index",
      OauthTokens::IndexView,
      tokens: personal_tokens
  end

  def new
    scopes = params[:scopes].to_s.split(",")
    @access = current_user.oauth_accesses.build(description: params[:description], scopes: scopes)
    render "oauth_tokens/new"
  end

  def create
    hash = params[:oauth_access]
    normalized_scopes = normalize_scopes_from(hash[:scopes])
    token, hashed_token = OauthAccess.random_token_pair
    # Build instance before saving it so we track the GTID properly
    # of the save operation.
    last_operations = DatabaseSelector::LastOperations.from_token(token)
    @access = current_user.oauth_accesses.build do |access|
      access.application_id = OauthApplication::PERSONAL_TOKENS_APPLICATION_ID
      access.application_type = OauthApplication::PERSONAL_TOKENS_APPLICATION_TYPE
      access.description = hash[:description]
      access.scopes = normalized_scopes
      access.token_last_eight = token.last(8)
      access.hashed_token = hashed_token
    end

    if scopes_normalized?(hash[:scopes], normalized_scopes)
      flash[:notice] = NORMALIZED_MESSAGE
    end

    if @access.save
      # After saving the new token we want to set the last write timestamp in the cache,
      # so the api DatabaseSelection can use the write DB for newly created tokens and avoid issues
      # due to replication lag.
      last_operations.update_last_write_timestamp

      flash[:new_personal_access] = { id: @access.id, token: token }
      redirect_to settings_user_tokens_path
    else
      render "oauth_tokens/new"
    end
  end

  def show
    @access = current_user.oauth_accesses.personal_tokens.find(params[:id])
    render "oauth_tokens/show"
  end

  def update
    @access = current_user.oauth_accesses.personal_tokens.find(params[:id])
    hash = params[:oauth_access]
    normalized_scopes = normalize_scopes_from(hash[:scopes])
    @access.description = hash[:description]
    @access.scopes = normalized_scopes
    # Older personal access tokens will have a code set that is not needed.
    @access.code = nil

    if scopes_normalized?(hash[:scopes], normalized_scopes)
      flash[:notice] = NORMALIZED_MESSAGE
    end

    if @access.save
      redirect_to settings_user_tokens_path
    else
      render "oauth_tokens/show"
    end
  end

  def regenerate
    access = current_user.oauth_accesses.personal_tokens.find(params[:id])
    access.code = nil
    token = reset_token_for(access)

    if token.present? && access.valid?
      flash[:new_personal_access] = { id: access.id, token: token }
    else
      flash[:error] = "An error ocurred when regenerating this token, please try again"
    end

    redirect_to settings_user_token_path(access)
  end

  def destroy
    access = current_user.oauth_accesses.personal_tokens.find(params[:id])
    access.destroy_with_explanation(:web_user)

    if request.xhr?
      head 200
    else
      redirect_to settings_user_tokens_path
    end
  end

  def revoke_all
    if current_user.oauth_accesses.personal_tokens.count > MAXIMUM_TOKENS_REVOKE_ALL
      current_user.async_revoke_oauth_tokens(:personal_tokens)
      flash[:notice] = "Revoking all personal access tokens"
    else
      current_user.revoke_oauth_tokens(:personal_tokens)
      flash[:notice] = "Revoked all personal access tokens"
    end
    redirect_to settings_user_tokens_path
  end

  def remove_authorization
    token = current_user.oauth_accesses.personal_tokens.find(params[:id])
    org = Organization.find_by_login(params[:org])

    authorization = Organization::CredentialAuthorization.authorization \
      organization: org, credential: token
    return render_404 unless authorization

    authorization.destroy

    redirect_to settings_user_tokens_path,
      notice: "The token is no longer authorized to access #{ org }."
  end

  private

  def scopes_normalized?(original, normalized)
    !original.blank? && !normalized.blank? && (original.length > normalized.length)
  end

  def check_eligibility
    if current_user.must_verify_email?
      flash[:error] = "Creating a personal access token requires a verified email address."
      render_email_verification_required
    end
  end

  # Internal: Normalize the OAuth scopes such that redudant scopes are filtered
  # out. If a site administrator has requested the site_admin scope, allow it.
  #
  # scopes - The string based representation of requested scopes from params
  #
  # Returns an Array of String scopes.
  def normalize_scopes_from(scopes)
    normalized_scopes = OauthAccess.normalize_scopes(scopes)

    if current_user.site_admin? && scopes&.include?("site_admin")
      normalized_scopes << "site_admin"
    end

    # Ensure the correct order and remove any duplicate scopes.
    OauthAccess.normalize_scopes(normalized_scopes, visibility: :all)
  end

  # Internal: Safely resets an access token without raising exceptions when
  # validation errors are encountered on the note/description field.
  #
  # Context: https://github.com/github/ecosystem-apps/issues/988
  def reset_token_for(access)
    access.reset_token
  rescue ActiveRecord::RecordInvalid => e
    raise e unless access.errors.details[:note].any? { |e| e[:error] == :taken }
    ""
  end
end
