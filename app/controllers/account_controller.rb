# frozen_string_literal: true

class AccountController < ApplicationController
  areas_of_responsibility :account_settings

  # The following actions do not require conditional access checks:
  #
  # accept_repository_transfer_request  - Manually enforced based on the
  #                                       repository being transfered
  # leave_repo                          - Does not access protected
  #                                       organization resources
  # read broadcast                      - Does not access protected
  #                                       organization resources
  skip_before_action :perform_conditional_access_checks, only: %w(
    accept_repository_transfer_request
    read_broadcast
    leave_repo
  )

  before_action :login_required
  around_action :select_write_database, only: [:accept_repository_transfer_request]

  def accept_repository_transfer_request
    if !(xfer = RepositoryTransfer.from params[:token])
      flash[:error] = "Oops! That repository transfer has expired."
      return redirect_to "/"
    end

    begin
      if xfer.target.organization?
        # Manual IP allow list enforcement for transfers to an organization that
        # requires access from allowed IP addresses only.
        unless ip_whitelisting_policy_satisfied?(owner: xfer.target)
          return render_forbidden_by_ip_whitelisting_policy(target: xfer.target)
        end

        # Manually enforce a valid external identity session for transfers to an
        # organization that requires its members to authenticate with an identity
        # provider.
        if !required_external_identity_session_present?(target: xfer.target)
         render_external_identity_session_required(target: xfer.target)
         return
        end
      end

      xfer.finish current_user
    rescue ActiveRecord::RecordInvalid => e
      Failbot.report_user_error e, repository_transfer_id: xfer.id
      flash[:error] = "Sorry, this repository transfer can’t be finished."

      return redirect_to "/"
    end

    flash[:notice] = "Moving repository to #{current_user}/#{xfer.repository}. This may take a few minutes."
    redirect_to "/"
  end

  def read_broadcast
    current_user.update(last_read_broadcast_id: params[:id])
    if request.xhr?
      head :ok
    else
      redirect_to :back
    end
  end

  # Allows a user to remove themselves as a collaborator
  # from a repository.
  def leave_repo
    repo = Repository.where(id: params[:repo]).first
    if repo && repo.member?(current_user)
      repo.remove_member(current_user, current_user)
    end

    if request.xhr?
      head 200
    else
      flash[:notice] = "Your collaborator access to the repository was successfully removed."
      redirect_to :back
    end
  end
end
