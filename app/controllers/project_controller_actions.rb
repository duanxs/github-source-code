# frozen_string_literal: true

module ProjectControllerActions
  PAGE_SIZE = 30

  include PlatformHelper
  include ProjectsHelper

  IndexQuery = parse_query <<-'GRAPHQL'
    query($projectOwnerId: ID!, $first: Int!, $after: String, $query: String) {
      node(id: $projectOwnerId) {
        ...Views::Projects::Index::ProjectOwner
      }
    }
  GRAPHQL

  ListQuery = parse_query <<-'GRAPHQL'
    query($projectOwnerId: ID!, $first: Int!, $after: String, $query: String) {
      node(id: $projectOwnerId) {
        ...Views::Projects::List::ProjectOwner
      }
    }
  GRAPHQL

  def render_projects_index(owner:)
    params[:query] ||= "is:open"

    if params[:query].is_a?(Array)
      params[:query] = params[:query].join(" ")
    end

    variables = {
      "projectOwnerId": owner.global_relay_id,
      "first":          PAGE_SIZE,
      "after":          params[:cursor],
      "query":          project_index_query,
    }

    respond_to do |format|
      format.html do
        if request.xhr? && !pjax?
          data = platform_execute(ListQuery, variables: variables)

          if project_owner = data.node
            render partial: "projects/list", locals: {
              project_owner: project_owner,
              query:         params[:query],
            }
          else
            render_404
          end
        else
          data = platform_execute(IndexQuery, variables: variables)

          if project_owner = data.node
            render "projects/index", locals: {
              project_owner: project_owner,
              query:         params[:query],
            }
          else
            render_404
          end
        end
      end
    end
  end

  def render_new_project(owner:)
    project = owner.projects.build

    respond_to do |format|
      format.html do
        render "projects/new", locals: {
          project_owner: owner,
          project: project,
        }
      end
    end
  end

  def render_edit_project(owner:, project:)
    respond_to do |format|
      format.html do
        render "projects/edit", locals: {
          project: project,
        }
      end
    end
  end

  ShowQuery = parse_query <<-'GRAPHQL'
    query($projectOwnerId: ID!, $projectNumber: Int!, $maxColumns: Int!, $archivedStates: [ProjectCardArchivedState]!) {
      viewer: tempNullableViewer {
        ...Views::Projects::Show::Viewer
      }

      node(id: $projectOwnerId) {
        ...Views::Projects::Show::ProjectOwner
      }
    }
  GRAPHQL

  def render_show_project(owner:, project:)
    variables = {
      projectOwnerId: owner.global_relay_id,
      projectNumber: project.number,
      maxColumns: Project::MAX_COLUMNS,
      archivedStates: ["NOT_ARCHIVED"],
    }

    data = platform_execute(ShowQuery, variables: variables)

    if project_owner = data.node
      respond_to do |format|
        format.html do
          render "projects/show", locals: {
            project_owner: project_owner,
            viewer: data.viewer,
          }
        end
        format.json do
          headers["Vary"] = "X-JSON"
          headers["Cache-Control"] = "no-cache, no-store"
          render json: project
        end
      end
    else
      render_404
    end
  end

  CreateProjectQuery = parse_query <<-'GRAPHQL'
    mutation($ownerId: ID!, $name: String!, $body: String, $public: Boolean, $template: ProjectTemplate, $repositoryIds: [ID!]) {
      createProject(input: { ownerId: $ownerId, name: $name, body: $body, public: $public, template: $template, repositoryIds: $repositoryIds }) {
        project {
          resourcePath
        }
      }
    }
  GRAPHQL

  def create_project(owner:)
    variables = {
      ownerId: owner.global_relay_id,
      name: project_params[:name],
      body: project_params[:body],
      template: project_template_param,
      public: project_params[:public] == "true",
      repositoryIds: linked_repository_ids_from_param(owner: owner),
    }

    mutation = platform_execute(CreateProjectQuery, variables: variables)

    if mutation.errors.any?
      @this_project = Project.new(name: project_params[:name], body: project_params[:body])
      flash.now[:error] = mutation.errors.messages.values.join(", ")
      render "projects/new", locals: {
        project_owner: owner,
        project: @this_project,
        selected_template: project_template_param,
        selected_repositories: linked_repositories_from_param(owner: owner),
      }
    else
      project = mutation.create_project.project

      if variables[:template]
        template = ProjectTemplate.template_klass(project_template_param)

        GitHub.dogstats.increment("projects.created_with_template", tags: ["template:#{template.template_key}"])
        flash[:notice] = "Project created from #{template.title} template."
        redirect_to uri_with_add_cards_query(project.resource_path).to_s
      else
        redirect_to project.resource_path.to_s
      end
    end
  end

  def update_project(owner:, project:)
    if project.update(project_params)
      flash[:notice] = "Project updated"
      if params[:redirect_back].present?
        redirect_to :back
      else
        redirect_to project_path(project)
      end
    else
      flash.now[:error] = "Error saving your changes: #{project.errors.full_messages.to_sentence}"
      render "projects/edit", locals: { project: project }
    end
  end

  def update_project_state(project:, state:, sync: nil)
    project.enqueue_resync_workflows if sync == "1"
    case state
    when "open"
      project.open if project.closed?
      flash[:notice] = "Project reopened."
    when "closed"
      project.close unless project.closed?
      flash[:notice] = "Project closed."
    end

    if params[:redirect_back].present?
      redirect_to :back
    else
      head :ok
    end
  end

  CloneProjectQuery = parse_query <<-'GRAPHQL'
    mutation($targetOwnerId: ID!, $sourceId: ID!, $includeWorkflows: Boolean!, $name: String!, $body: String, $public: Boolean) {
      cloneProject(input: { targetOwnerId: $targetOwnerId, sourceId: $sourceId, includeWorkflows: $includeWorkflows, name: $name, body: $body, public: $public }) {
        project {
          resourcePath
        }
        jobStatusId
      }
    }
  GRAPHQL

  def clone_project(project:)
    variables = {
      targetOwnerId: project_clone_params[:global_target_owner_id],
      sourceId: project_clone_params[:global_source_id],
      includeWorkflows: project_clone_params[:include_workflows].to_i == 1,
      name: project_clone_params[:name],
      body: project_clone_params[:body],
    }

    if project_clone_params.has_key?(:public)
      variables[:public] = project_clone_params[:public].to_s == "true"
    end

    mutation = platform_execute(CloneProjectQuery, variables: variables)

    if mutation.errors.any?
      flash[:error] = "Uh oh! Something went wrong."
      redirect_to project_path(project)
    else
      GitHub.dogstats.increment("projects.created_with_template", tags: ["template:clone"])

      job_status_id = mutation.clone_project.job_status_id
      project = mutation.clone_project.project
      render json: { project_url: project.resource_path.to_s, job_status_url: job_status_url(job_status_id) }
    end
  end

  DeleteProjectQuery = parse_query <<-'GRAPHQL'
    mutation($projectId: ID!) {
      deleteProject(input: { projectId: $projectId }) {
        __typename # a selection is grammatically required here
      }
    }
  GRAPHQL

  def destroy_project(owner:)
    # platform_execute handles permissions checking for us but not flow control, so we're
    # still going to be using before_actions
    data = platform_execute(DeleteProjectQuery, variables: { "projectId" => params[:global_id] })

    if data.errors.all.any?
      # Checking the error type here is a temporary workaround. In the future,
      # platform_execute should be able to handle 404ing on a
      # NOT_FOUND by itself.
      if data.errors.details["deleteProject"].any? { |e| %w(NOT_FOUND UNAUTHENTICATED).include?(e["type"]) }
        render_404
      else
        project = typed_object_from_id([Platform::Objects::Project], params[:global_id])

        if project&.readable_by?(current_user)
          respond_to do |format|
            format.html do
              flash[:error] = "You don't have permission to delete this project. #{data.errors.messages.values.join(", ")}."
              redirect_to project_path(project)
            end

            format.json do
              head 403
            end
          end
        else
          render_404
        end
      end
    else
      redirect_to projects_path(owner: owner)
    end
  end

  def render_project_search_results(owner:, project:)
    query = project.set_search_query_for(current_user, query: params[:q])
    respond_to do |format|
      format.html do
        pending_cards = pending_cards_for_search(project)
        search_results = search_result_cards(owner: owner, project: project, query: query, pending_cards: pending_cards)
        render partial: "projects/search_results", locals: {
          project: project,
          cards: search_results[:cards],
          is_initial_search: params[:more].blank?,
          next_page: search_results[:next_page],
          query: query,
          pending_cards_limited: project.cards.pending.count > ProjectCard::PENDING_LIMIT,
          pending_cards: pending_cards,
        }
      end
    end
  end

  def render_project_activity(owner:, project:)
    respond_to do |format|
      format.html do
        options = {
          viewer: current_user,
          page: params[:page] || 1,
          after: params[:after],
          latest_allowed_entry_time: params[:latest_allowed_entry_time],
        }
        render partial: "projects/activity/list", locals: {
          activities: Project::AuditLog.new(project, **options),
        }
      end
    end
  end

  AddCardsLinkQuery = parse_query <<-'GRAPHQL'
    query($projectId: ID!) {
      node(id: $projectId) {
        ...Views::Projects::AddCardsLink::Project
      }
    }
  GRAPHQL

  def render_project_add_cards_link(project:)
    data = platform_execute(AddCardsLinkQuery, variables: { projectId: project.global_relay_id })

    respond_to do |format|
      format.html do
        render partial: "projects/add_cards_link", locals: { project: data.node }
      end
    end
  end

  def render_target_owner_results(project:)
    results = Project.query_valid_owners_for(viewer: current_user, query: params[:q])

    # Filter out any potential owners that may belong to an unauthorized org:
    if unauthorized_organization_ids.any?
      results.reject! do |result|
        filter_id = result.is_a?(Repository) ? result.owner_id : result.id
        unauthorized_organization_ids.include?(filter_id)
      end
    end

    sorted_results = results.sort_by do |target_owner|
      case target_owner
      when User
        [target_owner.login.downcase]
      when Repository
        [target_owner.owner.login.downcase, target_owner.name.downcase]
      end
    end

    current_owner = project.owner

    if sorted_results.include?(current_owner)
      sorted_results.delete(current_owner)
      sorted_results.unshift(current_owner)
      suggested_owner = current_owner
    else
      suggested_owner = nil
    end

    respond_to do |format|
      format.html_fragment { render partial: "projects/target_owner_results", formats: :html, locals: { results: sorted_results, suggested_owner: suggested_owner } }
    end
  end

  def render_linkable_repositories(owner:, project:)
    repos = ProjectRepositoryLink.linkable_repos_for(viewer: current_user, filter: params[:q], owner: owner)
    linked_repository_ids = project&.linked_repository_ids
    repos = repos.where.not(id: linked_repository_ids) if linked_repository_ids&.any?

    respond_to do |format|
      format.html_fragment do
        render partial: "projects/linkable_repositories", formats: :html,
          locals: {
            repositories: repos,
            project: project,
          }
      end
    end
  end

  def render_project_hovercard(project:)
    return render_404 unless project

    render "hovercards/projects/show", locals: { project: project }, layout: false
  end

  private

  def search_result_cards(owner:, project:, query:, pending_cards:)
    return unless logged_in?
    return unless project.writable_by?(current_user)

    parsed_issues_query = parse_issues_query(query, project)

    if owner.is_a?(User)
      has_repo_query = parsed_issues_query.group_by(&:first).keys.include?(:repo)
      unless has_repo_query
        parsed_issues_query = scope_query_to_linked_repos(parsed_issues_query, project)
      end

      parsed_issues_query = scope_query_to_user(parsed_issues_query, owner)
    end

    result_options = {
      per_page:     ProjectCard::PER_PAGE,
      page:         params[:page],
      query:        parsed_issues_query,
      current_user: current_user,
      remote_ip:    request.remote_ip,
      user_session: user_session,
      force_pulls:  false,
      prefill_associations: false, # We prefill below with only what we need
    }

    result_options[:repo] = owner if owner.is_a?(Repository)

    result = Issue::SearchResult.search(**result_options)

    issues = result[:issues]
    issue_ids = issues.map(&:id)

    in_column_matches = project.cards.in_column.for_content_type("Issue").
      where(content_id: issue_ids).pluck(:content_id).
      index_by { |content_id| content_id }

    pending_cards_by_id = pending_cards.index_by(&:content_id)

    issues.delete_if do |issue|
      # Card is already in project.
      in_column_matches[issue.id] ||

      # Card is already shown in triage section.
      (params[:triage] && pending_cards_by_id[issue.id])
    end

    cards = issues.map do |issue|
      if pending_card = pending_cards_by_id[issue.id]
        pending_card
      else
        project.cards.build(content: issue, passed_redaction: true)
      end
    end

    cards = ProjectCardPrefiller.new(project, cards.compact, viewer: current_user).cards

    if owner.is_a?(User)
      # Ensure no issues not owned by the user/org are ever returned. The
      # search results may include issues from other users/orgs if -repo:
      # qualifiers are specified for every single repo owned by the user/org.
      cards.delete_if { |card| card.content.owner != owner }
    end

    { cards: cards, next_page: issues.next_page }
  end

  def project_params
    params.require(:project).permit(:name, :body, :track_progress, :public)
  end

  def linked_repository_ids_from_param(owner:)
    return unless owner.is_a?(User)

    params[:project_repository_link_ids]
  end

  def linked_repositories_from_param(owner:)
    repo_ids = linked_repository_ids_from_param(owner: owner)
    return Repository.none unless repo_ids&.any?

    repo_ids.map { |repo_id| typed_object_from_id([Platform::Objects::Repository], repo_id) }
  end

  def project_template_param
    return unless params[:project_template].present? && ProjectTemplate.valid?(params[:project_template])

    if params[:project_template] != ProjectTemplate::None.template_key
      params[:project_template]
    end
  end

  def project_clone_params
    params.require(:project).permit(:global_target_owner_id, :global_source_id, :include_workflows, :name, :body, :public)
  end

  def parse_issues_query(query, project)
    parsed_issues_query = Search::Queries::IssueQuery.normalize(
      Search::Queries::IssueQuery.parse(query),
    )

    if parsed_issues_query.exclude?([:no, "project"])
      parsed_issues_query << [:project, project.search_slug, true]
    end

    parsed_issues_query
  end

  # Scope query string to all linked repositories
  def scope_query_to_linked_repos(query, project)
    project.linked_repositories.each { |repo| query << [:repo, repo.nwo] }
    query
  end

  # Adjust the query to only search repositories inside the user/organization
  def scope_query_to_user(query, user)
    contains_owned_repo = false
    array_parts, text_parts = query.partition { |term| term.is_a?(Array) }

    array_parts.delete_if do |key, value, negated|
      # Remove all org qualifiers from the query
      next true if key == :org

      if key == :repo
        owner, repo = value.split("/")

        # Remove all repositories without an owner
        next true if repo.blank?
        # Remove all repositories outside not owned by the user/org
        next true if owner.downcase != user.login.downcase

        contains_owned_repo = true unless negated
      end

      false
    end

    # Add org scope unless query targets a repo in the org
    unless contains_owned_repo
      key = user.organization? ? :org : :user
      array_parts << [key, user.login]
    end

    text_parts + array_parts
  end

  def project_index_query
    return @project_index_query if defined?(@project_index_query)
    query = parsed_projects_query.dup
    unless query.find { |(k, v)| k == :sort }
      query << [:sort, "created-desc"]
    end
    @project_index_query = Search::Queries::ProjectQuery.stringify(query)
  end

  def parsed_projects_query
    # Query params can come from projects land (:query) or the user profile (:q)
    # We have to support both here for search to work
    query = params[:query] || params[:q] || "is:open"

    Search::Queries::ProjectQuery.normalize(
      Search::Queries::ProjectQuery.parse(query),
    )
  end

  def pending_cards_for_search(project)
    pending_cards = project.cards.pending.order("created_at DESC").limit(ProjectCard::PENDING_LIMIT)
    pending_cards = ProjectCardRedactor.new(current_user, pending_cards).cards
    ProjectCardPrefiller.new(project, pending_cards, viewer: current_user).cards
  end
end
