# frozen_string_literal: true

module MarketingMethods
  EVENT_TYPES = {
    self_serve: "cloud_trial_upgrade_self_serve",
    sales_serve: "cloud_trial_upgrade_sales_serve",
  }.freeze

  def notify_marketing_of_cloud_trial_upgrade(organization, new_plan, type)
    cloud_trial = Billing::EnterpriseCloudTrial.new(organization)

    if cloud_trial.ever_been_in_trial? && new_plan == GitHub::Plan::BUSINESS_PLUS
      EnterpriseCloudTrialMarketingNotificationJob.perform_later(
        organization_id: organization.id,
        organization_login: organization.login,
        trial_id: cloud_trial.plan_trial_id,
        event_type: EVENT_TYPES.fetch(type),
      )
    end
  end

  def organization_creation_ga_event_attributes(success, organization, actor, is_enterprise_cloud_trial, redirect: true)
    pricing = Billing::Pricing.new(
      plan: organization.plan,
      seats: organization.seats,
      plan_duration: "year",
      coupon: organization.coupon,
    )

    labels = referral_params.compact.merge({
      plan: translate_plan_name(organization.plan),
      type: new_organization_type(organization, is_enterprise_cloud_trial)
    })

    {
      category: "Create organization",
      action: success ? "Success" : "Failure",
      label: base_organization_ga_label_hash(organization, actor, is_enterprise_cloud_trial, pricing).merge(labels),
      value: is_enterprise_cloud_trial ? 0 : pricing.annual_recurring_revenue.to_f,
      redirect: redirect,
    }
  end

  def organization_plan_change_ga_event_attributes(success, organization, actor, old_plan_name, new_plan_name, old_seats, new_seats)
    old_plan = GitHub::Plan.find(old_plan_name)
    new_plan = GitHub::Plan.find(new_plan_name)
    old_plan = MunichPlan.wrap(old_plan, target: organization)
    new_plan = MunichPlan.wrap(new_plan, target: organization)

    new_pricing = Billing::Pricing.new(
      plan: new_plan,
      seats: new_seats,
      plan_duration: "year",
      coupon: organization.coupon,
    )
    old_pricing = Billing::Pricing.new(
      plan: old_plan,
      seats: old_seats,
      plan_duration: "year",
      coupon: organization.coupon,
    )
    transition = new_pricing.annual_recurring_revenue >= old_pricing.annual_recurring_revenue ? "Upgrade" : "Downgrade"
    transition_params = {
      transition: transition,
      currentplan: translate_plan_name(old_plan),
      newplan: translate_plan_name(new_plan),
      oldseats: old_seats,
      newseats: new_seats,
    }

    labels = base_organization_ga_label_hash(organization, actor, false, new_pricing)
      .merge(transition_params)
      .compact

    {
      category: "Change organization",
      action: success ? "Success" : "Failure",
      label: labels,
      value: new_pricing.annual_recurring_revenue.to_f,
      redirect: true,
    }
  end

  def organization_trial_extension_ga_event_attributes(success, organization, actor)
    labels = base_organization_ga_label_hash(organization, actor, true)
      .merge({ transition: "ExtendTrial" })
      .compact

    {
      category: "Change organization",
      action: success ? "Success" : "Failure",
      label: labels,
      value: 0,
      redirect: true,
    }
  end

  private

  def base_organization_ga_label_hash(organization, actor, is_enterprise_cloud_trial, pricing = nil)
    {
      orgid: organization.analytics_tracking_id,
      userid: actor.analytics_tracking_id,
      seats: organization.seats,
      discount: is_enterprise_cloud_trial ? 0 : pricing&.discount.to_f,
    }.merge(referral_params).compact
  end

  def new_organization_type(organization, is_enterprise_cloud_trial)
    if is_enterprise_cloud_trial
      "trial"
    elsif organization.plan&.free?
      "free"
    else
      "paid"
    end
  end

  def translate_plan_name(plan)
    return nil unless plan

    case plan.name
    when "free", "free_with_addons"
      "Free"
    when "business"
      "Team"
    when "business_plus"
      "EnterpriseCloud"
    else
      plan.display_name
    end
  end
end
