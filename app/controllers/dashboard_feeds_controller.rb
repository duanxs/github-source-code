# frozen_string_literal: true

class DashboardFeedsController < ApplicationController
  areas_of_responsibility :news_feeds

  include OrganizationsHelper

  before_action :login_required
  before_action :org_members_only_org_feed, only: %w( show )
  around_action :record_request_time, only: :show

  def show
    return render partial: "events/unavailable" if events_timeline.unavailable?

    view_model = Dashboard::NewsFeedView.new(timeline: events_timeline)

    # Allows us to turn off expensive queries causing the dashboard to load slowly
    # for more information please see https://github.com/github/github/issues/93044.
    starred_repo_ids = if show_star_repo_buttons?
      get_starred_repo_ids(repo_ids: event_repo_ids(view_model))
    end

    followed_user_ids = if show_follow_user_buttons?
      get_followed_user_ids(user_ids: followable_user_ids(view_model))
    end

    sponsored_user_ids = if show_sponsored_user_buttons?
      get_sponsored_user_ids(user_ids: sponsorable_user_ids(view_model))
    end

    sponsorable_memberships = if show_sponsored_user_buttons?
      get_sponsorable_memberships(user_ids: sponsorable_user_ids(view_model))
    end

    locals = {
      followed_user_ids: followed_user_ids,
      following_count: current_user.following_count,
      starred_repo_ids: starred_repo_ids,
      sponsored_user_ids: sponsored_user_ids,
      sponsorable_memberships: sponsorable_memberships,
      view_model: view_model,
    }

    respond_to do |format|
      format.html do
        GitHub.dogstats.batch do
          render partial: "dashboard/news_feed_events", locals: locals
        end
      end
    end
  end

  def record_request_time
    start_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)

    yield

    end_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
    GitHub.dogstats.distribution("dashboard_feeds.dist.time", (end_time - start_time) * 1_000)
  end

  private

  def show_star_repo_buttons?
    current_organization.nil? &&
      !GitHub.flipper[:disable_starred_repos_button_on_dashboard_feed].enabled?(current_user)
  end

  def event_repo_ids(view_model)
    GitHub.dogstats.time("dashboard.get_event_repo_ids.time") do
      view_model.event_repo_ids(page: current_page)
    end
  end

  def show_follow_user_buttons?
    current_organization.nil? &&
      !GitHub.flipper[:disable_followed_users_button_on_dashboard_feed].enabled?(current_user)
  end

  def followable_user_ids(view_model)
    GitHub.dogstats.time("dashboard.get_followable_user_ids.time") do
      view_model.followable_user_ids(page: current_page, viewer: current_user)
    end
  end

  def show_sponsored_user_buttons?
    current_organization.nil? &&
      !GitHub.flipper[:disable_sponsored_users_button_on_dashboard_feed].enabled?(current_user)
  end

  def sponsorable_user_ids(view_model)
    GitHub.dogstats.time("dashboard.get_sponsorable_user_ids.time") do
      view_model.sponsorable_user_ids(page: current_page, viewer: current_user)
    end
  end

  def org_members_only_org_feed
    if params[:org] || params[:organization_id]
      render_404 if current_organization.nil?
    end
  end

  def get_starred_repo_ids(repo_ids:)
    GitHub.dogstats.time("dashboard.get_starred_repo_ids.time") do
      current_user.stars.
        where(starrable_id: repo_ids, starrable_type: Star::STARRABLE_TYPE_REPOSITORY).
        pluck(:starrable_id)
    end
  end

  def get_followed_user_ids(user_ids:)
    GitHub.dogstats.time("dashboard.get_followed_user_ids.time") do
      Following.followed_by(current_user).where(following_id: user_ids).pluck(:following_id)
    end
  end

  def get_sponsored_user_ids(user_ids:)
    GitHub.dogstats.time("dashboard.get_sponsored_user_ids.time") do
      current_user.sponsorships_as_sponsor.where(sponsorable_id: user_ids).pluck(:sponsorable_id)
    end
  end

  def get_sponsorable_memberships(user_ids:)
    GitHub.dogstats.time("dashboard.get_sponsorable_memberships.time") do
      SponsorsMembership.where(sponsorable_id: user_ids).index_by(&:sponsorable_id)
    end
  end

  def events_timeline_key
    if current_organization
      "org:#{current_organization.id}"
    else
      "user:#{current_user.id}"
    end
  end
end
