# frozen_string_literal: true

class RelatedRepositoriesController < ApplicationController
  include RepositoryControllerMethods

  def index
    org_query_phrase = "org:#{current_repository.owner.login}" if current_repository.in_organization?
    query_phrase_prefix = "user:#{current_user.login} #{org_query_phrase}".strip
    query_text = permitted_params[:q]

    query = Search::Queries::RepoQuery.new(
      current_user: current_user,
      user_session: user_session,
      remote_ip: request.remote_ip,
      phrase: "#{query_phrase_prefix} #{query_text}",
    )

    repos = query.execute.results.map { |result| result["_model"] }.select(&:has_issues?)

    respond_to do |format|
      format.html_fragment do
        render partial: "repositories/repository_results", formats: :html, locals: { repositories: repos }
      end
      format.html do
        render partial: "repositories/repository_results", locals: { repositories: repos }
      end
    end
  end

  def permitted_params
    params.permit(:q, :user_id, :repository)
  end
end
