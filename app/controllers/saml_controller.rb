# frozen_string_literal: true

class SamlController < ApplicationController
  skip_before_action :perform_conditional_access_checks
  skip_before_action :first_run_check if GitHub.enterprise?
  before_action :require_enterprise

  # Service provider metadata for identity provider.
  def metadata
    render xml: GitHub.auth.metadata
  end

  private

  def require_enterprise
    render_404 unless GitHub.enterprise?
  end
end
