# frozen_string_literal: true

module ActionsControllerMethods
  FILTER_FIELDS = [:workflow, :actor, :branch, :event, :is]

  # Query string of passed in filters
  def workflow_query
    params[:query]
  end

  # Hash of passed in workflow filters
  def workflow_run_filters
    return @_workflow_run_filters if defined?(@_workflow_run_filters)
    query = workflow_query
    parsed_query = Search::ParsedQuery.parse(query, FILTER_FIELDS)
    filters_array = parsed_query.
      select { |param| param.is_a?(Array) }. # filter out non-qualifier nodes
      map { |param| param.first(2) } # negated params get an extra boolean in the array
    filters = Hash[filters_array]

    if params[:workflow] && !filters[:workflow]
      # Fall back to query string parameter to preserve back-compat
      filters[:workflow] = params[:workflow]
    end

    @_workflow_run_filters = filters
  end

  def selected_workflow_name
    workflow_name = workflow_run_filters[:workflow]
  end

  def selected_workflow
    # Some repositories have multiple active (not_deleted) workflow entities for a given name. Some of those
    # do not have a `path` set. As a work-around we want t favor the workflow entities with a path set, so
    # order by `path` and select the first one.
    current_repository.workflows
      .not_deleted
      .order(path: :desc)
      .find_by(name: selected_workflow_name)
  end

  def workflows
    workflows = current_repository.workflows.not_deleted.most_recent.limit(50)
    # We only support filtering by name, so we show only the workflows with a name
    workflows = workflows.select { |workflow| workflow.name.present? }
    workflows
  end

  def actions_enabled_for_repo?
    render_404 unless GitHub.actions_enabled?
  end
end
