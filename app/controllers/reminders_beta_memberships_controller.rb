# frozen_string_literal: true

class RemindersBetaMembershipsController < ApplicationController
  areas_of_responsibility :ce_extensibility

  include AccountMembershipHelper

  FEATURE_SLUG = "reminders"

  before_action :dotcom_required
  before_action :login_required, :feature_flag_required

  def signup
    render_template_view(
      "reminders_beta_memberships/signup",
      RemindersBetaMemberships::BetaSignupView,
      membership: PrereleaseProgramMember.new,
      adminable_accounts: adminable_accounts,
      selected_account: adminable_accounts.include?(account) ? account : adminable_accounts.first,
    )
  end

  def agree
    GitHub.context.push(spamurai_form_signals: spamurai_form_signals)

    membership = EarlyAccessMembership.new(membership_params)
    pre_release_member   = PrereleaseProgramMember.find_by(member_id: params[:user_id], actor_id: current_user.id)
    pre_release_member ||= PrereleaseProgramMember.new(member_id: params[:user_id], actor_id: current_user.id)

    if current_user.should_verify_email?
      flash[:error] = "Signing up for GitHub scheduled reminders requires a verified email address."
      render_email_verification_required
    elsif membership.save && pre_release_member.save
      GitHub.dogstats.increment("reminders.joined_waitlist")

      GlobalInstrumenter.instrument("user.beta_feature.enroll",
        actor: current_user,
        action: "enroll",
        feature: FEATURE_SLUG,
        organization: membership.member,
      )

      if GitHub.flipper[:scheduled_reminders_auto_enable].enabled?(current_user)
        membership.member.enable_reminders
      end

      if params[:redirect_back]
        safe_redirect_to params[:redirect_back]
      else
        redirect_to reminders_beta_thanks_path(redirect_options)
      end
    else
      field_errors = membership.errors.full_messages + pre_release_member.errors.full_messages
      flash[:error] = "Sorry that didn’t work. #{field_errors.to_sentence}."
      redirect_to reminders_beta_signup_path(redirect_options)
    end
  end

  def thanks
    membership = EarlyAccessMembership.reminders_waitlist.find_by(member: account) if account

    if membership
      render_template_view(
        "reminders_beta_memberships/thanks",
        RemindersBetaMemberships::AgreementView,
        membership: membership,
        adminable_accounts: adminable_accounts,
      )
    else
      redirect_to reminders_beta_signup_path(redirect_options)
    end
  end

  private

  # This feature only supports organizations, not business or personal users
  def adminable_accounts
    adminable_organizations
  end

  def membership_params
    {
      member_id: params[:user_id],
      actor_id: current_user.id,
      feature_slug: FEATURE_SLUG,
      survey: survey,
    }
  end

  def survey
    @survey ||= Survey.find_by(slug: FEATURE_SLUG)
  end

  def redirect_options
    { account: account }
  end

  def feature_flag_required
    unless logged_in? && GitHub.flipper[:scheduled_reminders_signup].enabled?(current_user)
      render_404
    end
  end
end
