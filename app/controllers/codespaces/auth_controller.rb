# frozen_string_literal: true

module Codespaces
  class AuthController < ApplicationController
    include ApplicationController::CodespaceDependency

    before_action :require_codespace
    before_action :verify_authorization
    before_action :set_codespace_context
    before_action :exclude_firefox

    statsd_tag_actions

    def port_forwarding
      pfs_auth_host = current_codespace.vscs_target_config[:pfs_auth_host]
      uri = URI::HTTPS.build(
        host: pfs_auth_host,
        path: "/authenticate-codespace/#{EscapeUtils.escape_uri_component(current_codespace.guid)}",
        query: request.query_parameters.to_query,
      )
      pfs_service_url = uri.to_s

      client = ::Codespaces::ArmClient.new(resource_provider: current_codespace.plan.resource_provider)
      cascade_token = client.fetch_cascade_token(
        vscs_target: current_codespace.vscs_target,
        plan_id: current_codespace.plan.vscs_id,
        user: current_codespace.owner,
        environment_id: current_codespace.guid
      )

      headers["Cache-Control"] = "no-cache, no-store" # ensure we don't cache
      render "codespaces/auth/port_forwarding", locals: {
        pfs_auth_host: pfs_auth_host,
        pfs_service_url: pfs_service_url,
        cascade_token: cascade_token,
      }
    end

    def passthru
      redirect_to codespace_path(current_codespace)
    end

    def mint_cascade_token
      render json: { token: fetch_cascade_token }
    end

    private

    def identifier
      return @identifier if defined?(@identifier)

      @identifier = params[:identifier].presence ||
        params[:codespace_identifier].presence ||
        params[:workspace_identifier].presence ||
        slug_from_url(params[:url])
    end

    def slug_from_url(url)
      return unless url
      uri = URI.parse(url)
      uri.host&.split(".")&.first
    end

    def fetch_cascade_token
      client = ::Codespaces::ArmClient.new(
        timeouts: ::Codespaces::ArmClient::DEFAULT_TIMEOUTS.merge({
          timeout: 8, # leave some time to do other stuff before hitting our 10 secs timeout
        }),
        resource_provider: current_codespace.plan.resource_provider
      )

      client.fetch_cascade_token(
        vscs_target: current_codespace.vscs_target,
        plan_id: current_codespace.plan.vscs_id,
        user: current_codespace.owner,
        environment_id: current_codespace.guid,
        cache: false,
        expires_in: Api::Codespaces::CASCADE_TOKEN_EXPIRATION
      )
    rescue Codespaces::Error => e
      Codespaces::ErrorReporter.report(e, codespace: current_codespace)
      nil
    end
  end
end
