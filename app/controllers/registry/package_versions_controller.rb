# frozen_string_literal: true

class Registry::PackageVersionsController < AbstractRepositoryController

  before_action :ensure_package_version_verification_matches
  before_action :ensure_package_admin

  def destroy
    begin
      package_version.delete!(
        actor: current_user,
        via_actions: false,
        user_agent: "web UI",
      )
    rescue Registry::PackageVersion::PublicVersionDeletionError
      flash[:error] = "Public packages cannot be deleted. Please contact support for assistance."
      return redirect_to all_versions_path
    end

    flash[:package_version_deleted] = package_version.version
    redirect_to all_versions_path
  end

  private

  def ensure_package_version_verification_matches
    unless package_version.package.name.casecmp?(params[:verify])
      flash[:error] = "You must type the name of the package to confirm."
      redirect_to all_versions_path
    end
  end

  def ensure_package_admin
    unless current_repository.adminable_by?(current_user)
      flash[:error] = "You do no have permissions to delete this package."
      return redirect_to all_versions_path
    end
  end

  def package_version
    @package_version ||= current_repository.packages.find(params[:package_id]).package_versions.find(params[:id])
  end

  def all_versions_path
    package_versions_path(package_version.package.owner, package_version.package.repository, package_version.package.id)
  end
end
