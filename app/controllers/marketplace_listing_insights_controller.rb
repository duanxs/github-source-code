# frozen_string_literal: true

class MarketplaceListingInsightsController < ApplicationController
  areas_of_responsibility :marketplace

  layout "marketplace"

  before_action :marketplace_required
  before_action :render_404, unless: :logged_in?

  VALID_PERIODS = %w(day week month alltime)

  DEFAULT_PERIOD = :week

  # Traffic stats are lower due to people with ad blockers installed. We increase the traffic stats to more accurately reflect the real traffic numbers.
  AD_BLOCKER_MULTIPLIER = 1.08

  IndexQuery = parse_query <<-'GRAPHQL'
    query($listingSlug: String!, $period: MarketplaceListingInsightPeriod!) {
      marketplaceListing(slug: $listingSlug) {
        viewerCanReadInsights
      }

      ...Marketplace::ListingInsights::IndexPageView::InsightsQuery::Root
    }
  GRAPHQL

  def index
    variables = {
      listingSlug: params[:listing_slug],
      period: Platform::Enums::MarketplaceListingInsightPeriod.coerce_isolated_result(period_param),
    }

    data = platform_execute(IndexQuery, variables: variables)

    return render_404 unless data.marketplace_listing&.viewer_can_read_insights

    render_template_view "marketplace_listing_insights/index", Marketplace::ListingInsights::IndexPageView,
      { data: data, period: period_param, platform_context: platform_context }
  end

  GraphDataQuery = parse_query <<-'GRAPHQL'
    query($listingSlug: String!, $period: MarketplaceListingInsightPeriod!) {
      marketplaceListing(slug: $listingSlug) {
        viewerCanReadInsights
        insights(period: $period, first: 100) {
          edges {
            node {
              recordedOn
              pageViews
              visitors
            }
          }
        }
      }
    }
  GRAPHQL

  def visitor_graph_data
    variables = {
      listingSlug: params[:listing_slug],
      period: Platform::Enums::MarketplaceListingInsightPeriod.coerce_isolated_result(period_param),
    }

    data = platform_execute(GraphDataQuery, variables: variables)

    return render_404 unless data.marketplace_listing&.viewer_can_read_insights

    listing = data.marketplace_listing
    insights = listing.insights.edges

    graph_data = insights.map do |record|
      results = { total: (record.node.page_views * AD_BLOCKER_MULTIPLIER).to_i, unique: (record.node.visitors * AD_BLOCKER_MULTIPLIER).to_i }

      if period_param == :alltime
        results[:bucket] = record.node.recorded_on.beginning_of_month.strftime("%s")
      else
        results[:bucket] = record.node.recorded_on.strftime("%s")
      end

      results
    end

    total_pageviews = insights.map { |x| x.node.page_views }.inject(0, :+) * AD_BLOCKER_MULTIPLIER
    total_visitors = insights.map { |x| x.node.visitors }.inject(0, :+) * AD_BLOCKER_MULTIPLIER

    render json: { counts: graph_data, summary: { total: total_pageviews.to_i, unique: total_visitors.to_i } }.to_json
  end

  private

  def period_param
    return DEFAULT_PERIOD unless params[:period].present?

    return params[:period].to_sym if VALID_PERIODS.include? params[:period]

    DEFAULT_PERIOD
  end

  def target_for_conditional_access
    listing = Marketplace::Listing.find_by_slug(params[:listing_slug])
    return :no_target_for_conditional_access unless listing
    listing.owner
  end
end
