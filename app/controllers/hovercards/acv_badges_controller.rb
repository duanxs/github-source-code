# frozen_string_literal: true

class Hovercards::AcvBadgesController < ApplicationController
  include Hovercards::ConditionalAccessMethods

  areas_of_responsibility :user_profile
  before_action :require_feature
  before_action :require_xhr

  def show
    return render_404 unless this_user && contribution_count > 0
    render "hovercards/acv_badges/show", locals: {
      user: this_user,
      contribution_count: contribution_count,
      top_repositories: top_repositories,
    }, layout: false
  end

  private

  def this_user
    @user ||= User.find_by(type: "User", login: params[:user_id])
  end

  def contribution_count
    this_user.acv_contribution_count
  end

  def top_repositories
    this_user.top_acv_repositories
  end

  def require_feature
    render_404 unless GitHub.flipper[:acv_badge].enabled?(current_user)
  end

  def two_factor_enforceable
    return :no if action_name == "show"
    super
  end

  def target_for_conditional_access
    :no_target_for_conditional_access
  end
end
