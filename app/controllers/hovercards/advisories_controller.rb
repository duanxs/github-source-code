# frozen_string_literal: true

class Hovercards::AdvisoriesController < ApplicationController
  include Hovercards::ConditionalAccessMethods

  areas_of_responsibility :security_advisories

  before_action :require_xhr

  statsd_tag_actions only: :show

  def show
    return render_404 unless advisory.globally_available?

    render_template_view(
      "hovercards/advisories/show",
      GlobalAdvisories::ShowView,
      advisory: advisory,
      layout: false,
    )
  end

  private

  def advisory
    @advisory ||= Vulnerability.find_by!(ghsa_id: params[:id])
  end

  def two_factor_enforceable
    return :no if action_name == "show"
    super
  end

  def target_for_conditional_access
    :no_target_for_conditional_access
  end
end
