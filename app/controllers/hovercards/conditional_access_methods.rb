# frozen_string_literal: true
module Hovercards::ConditionalAccessMethods
  # opted out to be handled manually in show action to render a custom IP Allowlist interstitial
  def require_whitelisted_ip?
    return false if action_name == "show"
    super
  end

  # opted out to be handled manually in show action to render a custom SAML SSO interstitial
  def require_active_external_identity_session?
    return false if action_name == "show"
    super
  end

  def two_factor_enforce
    return head :forbidden unless request.xhr?
    render "hovercards/2fa", locals: { target: target_for_conditional_access, type: target_type }, layout: false
    set_html_safe
  end
end
