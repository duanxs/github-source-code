# frozen_string_literal: true

class Hovercards::RepositoriesController < ApplicationController
  areas_of_responsibility :user_profile

  statsd_tag_actions only: :show

  before_action :require_xhr, only: :show

  ShowQuery = parse_query <<-'GRAPHQL'
    query($owner: String!, $name: String!) {
      repository(owner: $owner, name: $name) {
        ...Views::Hovercards::Repositories::Show::Repository
        isPrivate
        owner {
          databaseId
        }
      }
    }
  GRAPHQL

  def show
    return render_404 unless this_repository

    render "hovercards/repositories/show", locals: { repository: this_repository }, layout: false
  end

  private

  def this_repository
    graphql_data.repository
  end

  def graphql_data
    return @graphql_data if defined? @graphql_data

    variables = { owner: params[:user_id], name: params[:repository] }
    # See https://github.com/github/ecosystem-api/issues/1546
    context = { nullify_private_repos_for_staff: true }

    @graphql_data = platform_execute(ShowQuery, variables: variables, context: context)
  end

  def target_for_conditional_access
    return :no_target_for_conditional_access unless this_repository&.is_private?

    # security issue: repository.owner does not take into account org-owned private repository forks
    # see https://github.com/github/iam/issues/1623 for more information
    target = User.find(this_repository.owner.database_id)
    return :no_target_for_conditional_access unless target
    target
  end
end
