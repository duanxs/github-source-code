# frozen_string_literal: true

class Hovercards::UsersController < ApplicationController
  areas_of_responsibility :user_profile

  before_action :require_xhr, only: [:show, :show_legacy]

  statsd_tag_actions only: [:show, :show_legacy]

  def show
    show_internal
  end

  def show_legacy
    show_internal
  end

  private

  def show_internal
    return render_404 unless this_user

    tracking_data = {
      actor_id: current_user&.id,
      user_id: this_user.id,
      user_login: this_user.login,
      subject_type: primary_subject&.class&.name,
    }

    render "hovercards/users/show", locals: {
      user: this_user,
      hovercard: hovercard,
      octolytics_tracking_data: tracking_data,
    }, layout: false
  end

  def primary_subject
    return @primary_subject if @primary_subject

    type, id = params[:subject]&.split(":")
    @primary_subject = Hovercard::SUBJECT_PREFIX_MAP[type]&.find_by_id(id)
  end

  def this_user
    return @user if @user

    if action_name == "show"
      user_login = params[:user_id]
      return unless user_login.present?

      @user = User.where(type: "User", login: user_login).first!
    else
      user_id = params[:user_id]&.to_i
      user_login = params[:user_login].presence
      return unless user_id || user_login

      @user = User.where(type: "User").where("login = ? OR id = ?", user_login, user_id).first!
    end
  end

  def hovercard
    @hovercard ||= UserHovercard.new(this_user, primary_subject, viewer: current_user)
  end

  def target_for_conditional_access
    hovercard.subject_organization || :no_target_for_conditional_access
  end
end
