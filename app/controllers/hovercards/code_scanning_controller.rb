# frozen_string_literal: true

class Hovercards::CodeScanningController < AbstractRepositoryController
  before_action :require_xhr
  statsd_tag_actions only: [:show_cwe]

  def show_cwe
    return render_404 unless current_repository.code_scanning_readable_by?(current_user)
    return render_404 unless cwe.present?

    render "hovercards/code_scanning/show_cwe", locals: { cwe: cwe }, layout: false
  end

  private

  # opts out SAML SSO on show_cwe action since it only shows static data that isn't
  # contextualized to the user/repository/organization/business
  def require_whitelisted_ip?
    return false if action_name == "show_cwe"
    super
  end

  # opts out SAML SSO on show_cwe action since it only shows static data that isn't
  # contextualized to the user/repository/organization/business
  def require_active_external_identity_session?
    return false if action_name == "show_cwe"
    super
  end

  # opts out SAML SSO on show_cwe action since it only shows static data that isn't
  # contextualized to the user/repository/organization/business
  def two_factor_enforceable
    return :no if action_name == "show_cwe"
    super
  end

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def cwe
    @cwe ||= cwes[params[:id]]&.merge(id: params[:id])
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def cwes
    @cwes ||= begin
      cwe_path = Rails.root.join("config", "cwe-data.json")
      cwe_content = File.read(cwe_path)
      JSON.parse(cwe_content).with_indifferent_access
    end
  end
end
