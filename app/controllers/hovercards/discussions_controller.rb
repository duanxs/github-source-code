# frozen_string_literal: true

class Hovercards::DiscussionsController < AbstractRepositoryController
  # opted out SAML & IP Allowlist to be handled manually in show action to render a custom SAML SSO interstitial
  include Hovercards::ConditionalAccessMethods

  statsd_tag_actions only: :show

  before_action :require_feature
  before_action :require_xhr
  before_action :require_discussion

  def show
    return render_ip_whitelisting unless ip_whitelisting_policy_satisfied?
    return render_sso unless required_external_identity_session_present?

    comment = if params[:comment_id]
      discussion.comments.find_by_id(params[:comment_id].to_i)
    elsif discussion.chosen_comment
      discussion.chosen_comment
    else
      discussion.latest_comment_for(current_user)
    end

    render "hovercards/discussions/show", locals: {
      discussion: discussion,
      comment: comment,
    }, layout: false
  end

  private

  # used to render in the conditional access enforcement pop-up
  def target_type
    "discussion"
  end

  # only allow relative paths for the return_to option on the SSO link, otherwise link to the
  # user dashboard
  def return_to_path
    return home_path unless params[:current_path]
    parsed = Addressable::URI.parse(params[:current_path])

    if parsed.relative? && parsed.host.nil?
      parsed.userinfo = nil
      parsed.normalize.to_s
    else
      home_path
    end
  end

  def render_ip_whitelisting
    render "hovercards/discussions/ip_whitelisting", layout: false
  end

  def target_for_conditional_access
    return :no_target_for_conditional_access unless current_repository.private?

    current_repository.organization || current_repository.owner
  end

  def render_sso
    render "hovercards/discussions/sso", layout: false
  end

  def require_feature
    render_404 unless show_discussions?
  end

  def discussion
    @discussion ||= Discussion.filter_spam_for(current_user).
      for_repository(current_repository).
      with_number(params[:number].to_i).first
  end

  def require_discussion
    render_404 unless discussion && discussion.readable_by?(current_user)
  end
end
