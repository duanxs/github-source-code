# frozen_string_literal: true

class Hovercards::RepositoryProjectsController < ApplicationController
  # TODO: describe why SAML SSO and IP allowlist were opted-out
  include Hovercards::ConditionalAccessMethods

  include ProjectControllerActions
  areas_of_responsibility :projects

  statsd_tag_actions only: :show

  before_action :require_xhr, only: :show

  def show
    return render_404 unless repository

    project = repository.visible_projects_for(current_user).find_by_number(params[:number].to_i)

    render_project_hovercard(project: project)
  end

  private

  def repository
    return @repository if defined?(@repository)
    @repository = this_user.repositories.find_by_name(params[:repository])
  end

  def this_user
    return @user if defined?(@user)
    @user = User.find_by_login(params[:user_id]) if (params[:user_id] && GitHub::UTF8.valid_unicode3?(params[:user_id]))
  end

  # TODO: describe why 2FA policy was opted out
  def two_factor_enforceable
    return :no if action_name == "show"
    super
  end

  def target_for_conditional_access
    :no_target_for_conditional_access
  end
end
