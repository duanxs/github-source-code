# frozen_string_literal: true

class Hovercards::OrganizationProjectsController < ApplicationController
  include ProjectControllerActions
  areas_of_responsibility :projects

  statsd_tag_actions only: :show

  before_action :require_xhr, only: :show
  before_action :require_organization, only: :show

  def show
    project = this_organization.visible_projects_for(current_user).find_by_number(params[:number].to_i)

    render_project_hovercard(project: project)
  end

  private

  def require_organization
    render_404 unless this_organization
  end

  def this_organization
    return @organization if defined?(@organization)
    @organization = Organization.find_by_login(params[:org]) if (params[:org] && GitHub::UTF8.valid_unicode3?(params[:org]))
  end

  def target_for_conditional_access
    this_organization || :no_target_for_conditional_access
  end
end
