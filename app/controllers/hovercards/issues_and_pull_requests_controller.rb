# frozen_string_literal: true

class Hovercards::IssuesAndPullRequestsController < ApplicationController
  # opted out SAML & IP Allowlist to be handled manually in show action to render a custom SAML SSO interstitial
  include Hovercards::ConditionalAccessMethods

  areas_of_responsibility :user_profile

  statsd_tag_actions only: :show

  before_action :require_xhr, only: :show

  ShowQuery = parse_query <<-'GRAPHQL'
    query($repo_owner: String!, $repo_name: String!, $issue_number: Int!, $comment_id: Int!,
          $show_issue_comment: Boolean!, $show_review_comment: Boolean!, $show_review: Boolean!
          $include_notification_contexts: Boolean!) {
      repository(owner: $repo_owner, name: $repo_name) {
        ...Views::Hovercards::IssuesAndPullRequests::IpWhitelisting::Repository
        ...Views::Hovercards::IssuesAndPullRequests::SSO::Repository

        issueOrPullRequest(number: $issue_number) {
          ...Views::Hovercards::IssuesAndPullRequests::Show::IssueOrPullRequest
        }
        deletedIssue(number: $issue_number) {
          id
        }
        isPrivate
        owner {
          databaseId
        }
      }
    }
  GRAPHQL

  def show
    return render_404 if !this_deleted_issue && !this_issue_or_pr
    return render_ip_whitelisting unless ip_whitelisting_policy_satisfied?
    return render_sso unless required_external_identity_session_present?
    return render json: { message: "This issue was deleted" }, status: :gone if this_deleted_issue

    render "hovercards/issues_and_pull_requests/show",
      locals: { issue_or_pr: this_issue_or_pr, show_subscription_status: show_subscription_status },
      layout: false
  end

  private

  # used to render in the conditional access enforcement pop-up
  def target_type
    this_issue_or_pr.__typename
  end

  def render_ip_whitelisting
    render "hovercards/issues_and_pull_requests/ip_whitelisting",
      locals: { repository: this_repository },
      layout: false
  end

  def render_sso
    render "hovercards/issues_and_pull_requests/sso",
      locals: {
        repository: this_repository,
        return_to_path: return_to_path,
      },
      layout: false
  end

  # only allow relative paths for the return_to option on the SSO link, otherwise link to the
  # user dashboard
  #
  def return_to_path
    return home_path unless params[:current_path]
    parsed = Addressable::URI.parse(params[:current_path])

    if parsed.relative? && parsed.host.nil?
      parsed.userinfo = nil
      parsed.normalize.to_s
    else
      home_path
    end
  end

  def this_deleted_issue
    this_repository&.deleted_issue
  end

  def this_issue_or_pr
    this_repository&.issue_or_pull_request
  end

  def this_repository
    graphql_data.repository
  end

  def graphql_data
    return @graphql_data if defined? @graphql_data
    comment_id = params[:comment_id]

    variables = {
      repo_owner: params[:user_id],
      repo_name: params[:repository],
      issue_number: params[:id].to_i,
      show_issue_comment: comment_id.present? &&
        comment_type == IssueOrPullRequestHovercard::ISSUE_COMMENT_TYPE,
      show_review_comment: comment_id.present? &&
        comment_type == IssueOrPullRequestHovercard::REVIEW_COMMENT_TYPE,
      show_review: comment_id.present? &&
        comment_type == IssueOrPullRequestHovercard::REVIEW_TYPE,
      comment_id: comment_id.present? ? comment_id.to_i : 0,
      include_notification_contexts: show_subscription_status,
    }
    # See https://github.com/github/ecosystem-api/issues/1546
    context = { nullify_private_repos_for_staff: true }
    @graphql_data = platform_execute(ShowQuery, variables: variables, context: context)
  end

  def comment_type
    @comment_type ||= case params[:comment_type]
    when IssueOrPullRequestHovercard::REVIEW_COMMENT_TYPE,
         IssueOrPullRequestHovercard::REVIEW_TYPE
      params[:comment_type]
    else
      IssueOrPullRequestHovercard::ISSUE_COMMENT_TYPE
    end
  end

  def target_for_conditional_access
    return :no_target_for_conditional_access unless this_repository&.is_private?

    # security issue: repository.owner does not take into account org-owned private repository forks
    # see https://github.com/github/iam/issues/1623 for more information
    target = User.find(this_repository.owner.database_id)
    return :no_target_for_conditional_access unless target
    target
  end

  def show_subscription_status
    params[:show_subscription_status] == "true"
  end
end
