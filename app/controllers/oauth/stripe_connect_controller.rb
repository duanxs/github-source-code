# frozen_string_literal: true

class Oauth::StripeConnectController < ApplicationController
  before_action :current_user_required
  before_action :verify_request_identity, only: [:authenticate]
  before_action :redirect_duplicate_requests, only: [:authenticate]

  def authenticate
    sponsors_listing = sponsorable.sponsors_listing

    if sponsors_listing.needs_duplicate_stripe_remediation?
      ActiveRecord::Base.connected_to(role: :writing) do
        sponsors_listing.set_duplicate_stripe_remediation_auth_code(code: params[:code])
      end

      TransitionDuplicateStripeAccountJob.perform_later(sponsors_listing)
    else
      ActiveRecord::Base.connected_to(role: :writing) do
        sponsors_listing.update!(stripe_authorization_code: params[:code])
      end

      SetupStripeConnectAccountJob.perform_later(sponsors_listing)
    end

    safe_redirect_to sponsorable_dashboard_path(sponsorable)
  end

  private

  attr_reader :sponsorable

  def target_for_conditional_access
    :no_target_for_conditional_access
  end

  def verify_request_identity
    @sponsorable ||= current_user

    sponsorable_id =
      Billing::StripeConnect::Account::Oauth.validate_request_state(params[:state])
    @sponsorable = if sponsorable_id.present?
      _, db_id = Platform::Helpers::NodeIdentification.from_global_id(sponsorable_id)
      User.find_by_id(db_id)
    end
    @sponsorable ||= current_user
  rescue Billing::StripeConnect::Account::Oauth::AuthenticationError
    flash[:error] = "There was a problem authenticating the request."
    safe_redirect_to sponsorable_dashboard_path(sponsorable)
  rescue Billing::StripeConnect::Account::Oauth::RequestExpiredError
    flash[:error] = "The request has expired."
    safe_redirect_to sponsorable_dashboard_path(sponsorable)
  end

  def redirect_duplicate_requests
    listing = sponsorable.sponsors_listing
    if existing_code = listing.stripe_authorization_code
      if existing_code == params[:code]
        return safe_redirect_to sponsorable_dashboard_path(current_user)
      end
    end
  end

  def current_user_required
    render_404 unless logged_in?
  end
end
