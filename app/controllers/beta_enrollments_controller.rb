# frozen_string_literal: true

class BetaEnrollmentsController < ApplicationController
  areas_of_responsibility :account_settings

  before_action :login_required
  before_action :ensure_feature

  include OrganizationsHelper

  def create
    if current_user.enable_beta_feature(params[:feature])
      flash[successful_flash_key] = successful_enrollment_message
    else
      flash[failed_flash_key] = failed_enrollment_message
    end

    after_beta_redirect
  end

  def destroy
    if current_user.disable_beta_feature(params[:feature])
      flash[successful_flash_key] = successful_unenrollment_message
    else
      flash[failed_flash_key] = failed_unenrollment_message
    end

    after_beta_redirect
  end

  private

  def successful_flash_key
    :notice
  end

  def failed_flash_key
    :error
  end

  def successful_enrollment_message
    case params[:feature]
    when "windrose_disabled"
      "You're now unenrolled from the beta. It may take a minute to see changes."
    else
      "You're now in the beta! It may take a minute to see changes."
    end
  end

  def failed_enrollment_message
    "Sorry, we couldn't add you to the beta at this time."
  end

  def successful_unenrollment_message
    case params[:feature]
    when "windrose_disabled"
      "You're now in the beta! It may take a minute to see changes."
    else
      "You're now unenrolled from the beta. It may take a minute to see changes."
    end
  end

  def failed_unenrollment_message
    "Sorry, we couldn't remove you from the beta at this time."
  end

  def after_beta_redirect
    case params[:feature]
    when "pull_request_next", "windrose_disabled"
      redirect_to :back
    else
      if params[:return_to].present?
        safe_redirect_to params[:return_to]
      else
        redirect_to home_path
      end
    end
  end

  def ensure_feature
    render_404 unless params[:feature].present?
  end
end
