# frozen_string_literal: true

class InteractiveComponentsController < ApplicationController
  areas_of_responsibility :ce_extensibility

  statsd_tag_actions only: [:button_click, :dismiss]

  include ShowPartial
  include CommentsHelper

  before_action :require_interactive_component
  before_action :require_interactive_component_integration
  before_action :require_interactive_component_integration_installed
  before_action :authorization_required
  before_action :login_required

  def button_click
    begin
      current_interactive_component.process_button_interaction(params[:button], current_user)
    rescue InteractiveComponent::InvalidButtonParamError
      return head :not_found
    end

    GlobalInstrumenter.instrument("interactive_components.button_click",
      actor: current_user,
      app: @current_interactive_component.integration,
      interactive_component: @current_interactive_component,
    )
    head :ok
  end

  def dismiss
    # Dismiss an ephemeral notice.
    #
    # @note Dismisses all similar notices except when the requested notice has already been dismissed.
    ephemeral_notice = current_interactive_component.ephemeral_notices.find_by(user: current_user, id: params[:ephemeral_notice_id])

    current_interactive_component.ephemeral_notices.where(
      user: current_user,
      notice_text: ephemeral_notice.notice_text,
      link_title: ephemeral_notice.link_title,
      link_to: ephemeral_notice.link_to).destroy_all if ephemeral_notice

    head :ok
  end

  private

  def target_for_conditional_access
    return :no_target_for_conditional_access unless current_interactive_component

    target = case current_interactive_component.container.associated_domain
    when InteractiveComponent::Container::ASSOCIATED_DOMAINS[:repo]
      current_interactive_component.container.repository.owner
    end

    target
  end

  def current_interactive_component
    return @current_interactive_component if defined?(@current_interactive_component)

    interactive_component = InteractiveComponent.find(params[:interactive_component_id])
    @current_interactive_component = interactive_component.readable_by?(current_user) ? interactive_component : nil
  end

  def authorized?
    current_interactive_component&.readable_by?(current_user)
  end

  def require_interactive_component
    if current_interactive_component.nil?
      render_404
    end
  end

  def require_interactive_component_integration
    if current_interactive_component.integration.nil?
      render_404
    end
  end

  def require_interactive_component_integration_installed
    domain = current_interactive_component.container.associated_domain

    installations = case domain
    when InteractiveComponent::Container::ASSOCIATED_DOMAINS[:repo]
      IntegrationInstallation
        .with_repository(current_interactive_component.container.repository)
        .where(integration: [current_interactive_component.integration])
    end

    render_404 if installations.empty?
  end
end
