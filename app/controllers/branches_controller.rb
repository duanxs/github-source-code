# frozen_string_literal: true

class BranchesController < GitContentController
  map_to_service :ref
  map_to_service :commit, only: [:pre_mergeable]
  map_to_service :commit_status, only: [:statuses]

  statsd_tag_actions only: [:all, :index, :yours, :active, :stale]

  before_action :skip_robot
  before_action :pushers_only,    only: [:create, :destroy]
  before_action :require_xhr, only: [:statuses]

  skip_before_action :set_path_and_name

  layout :repository_layout

  param_encoding :destroy, :name, "ASCII-8BIT"

  def index
    page :overview
  end

  def yours
    return render_404 unless logged_in?

    page :yours
  end

  def active
    page :active
  end

  def stale
    page :stale
  end

  def all
    page :all
  end

  def page(selected_view)
    view_params = {
      selected_view: selected_view,
      page: params[:page],
      search_query: params[:query].to_s,
      repository: current_repository,
    }

    if :overview == view_params[:selected_view]
      view_model = Branches::OverviewView
      xhr_template = "branches/overview_view"
    else
      view_model = Branches::PaginatedView
      xhr_template = "branches/paginated_view"
    end

    # Make sure the browser caches AJAX responses separately from regular HTML responses.
    response.headers["Vary"] = "X-Requested-With"

    respond_to do |format|
      format.html do
        if request.xhr? && !pjax?
          render_partial_view(xhr_template, view_model, view_params)
        else
          render_template_view("branches/index", view_model, view_params)
        end
      end
    end
  end

  def create
    name = Git::Ref.normalize(params[:name])
    commit_oid = current_repository.ref_to_sha(params[:branch])

    return render_404 if name.blank? || commit_oid.nil?

    branch_exists = false
    success =
      begin
        current_repository.heads.create(name, commit_oid, current_user,
                                        reflog_data: request_reflog_data("web branch create"))
      rescue Git::Ref::ExistsError
        flash[:error] = "Sorry, that branch already exists."
        branch_exists = true
        false
      rescue Git::Ref::HookFailed => e
        flash[:hook_out] = e.message
        flash[:hook_message] = "Branch could not be created."
        false
      rescue Git::Ref::InvalidName, Git::Ref::UpdateFailed
        flash[:error] = "Sorry, that branch name is invalid."
        false
      else
        GitHub.instrument "branch.create", user: current_user
        true
      end

    if request.xhr?
      head success ? 200 : 422
    else
      if success
        flash[:notice] = "Branch created."

        path_binary = params[:path_binary]
        raw_path = Base64.decode64(path_binary).b if path_binary
        branch_path = tree_path(raw_path, name, current_repository)

        redirect_to params[:return_to] || branch_path
      else
        redirect_to params[:return_to] || :back
      end
    end
  end

  def destroy
    ref = current_repository.heads.find(params[:name])

    if ref && ref.exist?
      if ref.deleteable?
        begin
          ref.delete(current_user, reflog_data: request_reflog_data("branches page delete button"))
        rescue Git::Ref::HookFailed => e
          flash[:hook_out] = e.message
          flash[:hook_message] = "Branch could not be deleted."
          status = 422
        else
          GitHub.instrument "branch.delete", user: current_user
          status = 200
        end
      else
        flash[:error] = "Sorry, couldn’t delete that branch."
        status = 403
      end
    else
      flash[:error] = "Sorry, couldn’t delete that branch."
      status = 404
    end

    was_success = 200 == status

    if was_success && CommitContribution.track_commits_in_branch?(repository: current_repository,
                                                                  branch: ref.name)
      CommitContribution.backfill(current_repository, true)
    end

    if request.xhr?
      head status
    else
      flash[:notice] = "Branch deleted." if was_success
      redirect_to params[:return_to] || :back
    end
  end

  # Can we merge two branches? Used on the create new pull request form.
  #
  # - params[:range] - Same style as compare URLs
  #
  # Renders a partial displaying whether the branches can be merged or not.
  def pre_mergeable
    return head(:not_found) if params[:range].blank?

    comparison = GitHub::Comparison.from_range(current_repository, params[:range], limit: 250)

    return head(:not_found) unless comparison.valid?

    merge_commit, error = comparison.compare_repository.commits.create_merge_commit(User.ghost,
      comparison.base_sha, comparison.head_sha)

    if merge_commit
      state = :clean
    elsif [:merge_conflict, :already_merged].include?(error)
      state = :dirty
    else
      state = :error
    end

    respond_to do |format|
      format.html do
        render partial: "branches/pre_mergeability", locals: { state: state }
      end
    end
  end

  # Endpoint for the new-branch ref check
  #
  # - params[:ref] - The proposed ref to validate.
  #
  # Sends back a normalized, unique version of the proposed ref.
  def ref_check
    ref = params[:ref]
    return head :not_acceptable if ref.blank?

    # Normalize.
    ref = Git::Ref.normalize(ref)

    # Ensure there’s no collisions.
    if current_repository.refs.exist?(ref)
      # Send back `master-1` if `master` exists.
      ref = current_repository.refs.temp_name(topic: ref)
    end

    render json: {
      message_html: ref == params[:ref] ? nil : "Will be created as <span class='branch-name'>#{h(ref)}</span>",
      normalized_ref: ref, # For updating the hidden field.
    }
  end

  def statuses
    commit_oid = current_repository.ref_to_sha(params[:name])
    commit = current_repository.commits.find(commit_oid)
    return head :not_found unless commit

    Commit.prefill_combined_statuses([commit], current_repository)
    respond_to do |format|
      format.html do
        view = create_view_model(Statuses::CombinedStatusView, {
          combined_status: commit.combined_status,
          simple_view: true,
        })
        render partial: "statuses/deprecated_combined_branch_status", locals: { view: view }
      end
    end
  end

  private

  # Internal: Keep robots off of these branches page. Nothing interesting
  # for them here.
  def skip_robot
    render_404 if robot?
  end

  def route_supports_advisory_workspaces?
    true
  end
end
