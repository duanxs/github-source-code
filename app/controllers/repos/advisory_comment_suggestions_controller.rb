# frozen_string_literal: true

class Repos::AdvisoryCommentSuggestionsController < AbstractRepositoryController
  areas_of_responsibility :security_advisories

  before_action :login_required
  before_action :check_feature_is_enabled
  before_action :authorize_advisory_comments
  before_action :require_xhr

  def mentions
    respond_to do |format|
      format.json do
        render json: suggester.mentions
      end
    end
  end

  def issues
    issues = suggester.issues.map do |elem|
      { id: elem.id, number: elem.number, title: elem.title }
    end

    respond_to do |format|
      format.json do
        render json: issues
      end
    end
  end

  private

  def suggester
    Suggester::RepositoryAdvisorySuggester.new(
      viewer: current_user,
      repository: current_repository,
      advisory: current_advisory)
  end

  def current_advisory
    @current_advisory ||= current_repository.repository_advisories.find_by!(ghsa_id: params[:id])
  end

  def check_feature_is_enabled
    render_404 unless current_repository.advisories_enabled?
  end

  def authorize_advisory_comments
    render_404 unless current_advisory.writable_by?(current_user)
  end
end
