# frozen_string_literal: true

class Repos::TokenScanningController < AbstractRepositoryController
  include ScanningControllerMethods

  PAGE_SIZE = 25

  areas_of_responsibility :token_scanning

  before_action :login_required
  before_action :check_token_scanning_is_enabled
  before_action :require_token_scan_result, only: [:show, :show_files]
  statsd_tag_actions only: [:index, :show, :show_files, :resolve]

  def index
    render_template_view(
      "repos/token_scanning/index",
      RepositoryTokenScanning::IndexView,
      repository: current_repository,
      current_page: current_page,
      query: Search::Queries::TokenScanningQuery.new(query: params[:query]),
      use_token_scanning_service: use_token_scanning_service?,
      page_size: PAGE_SIZE,
    )
  end

  def show
    commits, graphql_commits = token_scan_result_commits

    render_template_view(
      "repos/token_scanning/show",
      RepositoryTokenScanning::ShowView,
      repository: current_repository,
      result: token_scan_result,
      commits: graphql_commits,
      raw_commits_by_oid: commits.index_by(&:oid),
      base_url: base_url,
    )
  end

  def show_files
    return render_404 if token_scan_result.found_in_archive?

    render_template_view(
      "repos/token_scanning/files",
      RepositoryTokenScanning::ShowView,
      repository: current_repository,
      result: token_scan_result,
      base_url: base_url,
    )
  end

  def resolve
    resolution = params[:resolution].to_s.to_sym

    if use_token_scanning_service?
      resolution = GitHub::TokenScanning::Service::Client.to_resolution(resolution)
      return head :unprocessable_entity unless resolution.present?
    else
      return head :unprocessable_entity unless TokenScanResult.resolutions.key?(resolution)
    end

    ids = Array(params[:id]).take(PAGE_SIZE).map(&:to_i).uniq

    ids.each do |id|
      result = token_scan_result_for_id(id)

      unless result.present?
        flash[:error] = "Couldn’t find the token to update."
        break
      end

      if result.resolved? && resolution != :reopened
        flash[:error] = "The token has already been closed."
        break
      end

      if use_token_scanning_service?
        resolve_from_service(id, resolution)
      else
        result.resolve!(resolution: resolution, actor: current_user)
      end

      GitHub.dogstats.increment("token.scan.result.resolved", tags: [
        "resolution:#{resolution}",
        "tokenType:#{result.token_type}",
      ])
    end

    redirect_to :back
  end

  private

  def token_scan_result
    return @token_scan_result if defined? @token_scan_result

    @token_scan_result = token_scan_result_for_id(params[:id])
  end

  def token_scan_result_for_id(id)
    if use_token_scanning_service?
      token_scan_result_from_service(id)
    else
      current_repository.token_scan_results.find_by_id(id)
    end
  end

  def token_scan_result_from_service(id)
    response = GitHub::TokenScanning::Service::Client.get_token(
      repository_id: current_repository.id,
      token_id: id.to_i,
    )

    if response&.data&.token.present?
      GitHub::TokenScanning::Service::Client.wrap_token(response.data.token, current_repository)
    else
      nil
    end
  end

  def resolve_from_service(id, resolution)
    GitHub::TokenScanning::Service::Client.resolve_token(
      repository_id: current_repository.id,
      token_id: id.to_i,
      resolver_id: current_user.id,
      resolution: resolution,
    )
  end

  def token_scan_result_commits
    commit_oids = if use_token_scanning_service?
      commit_oids = [token_scan_result.first_location.commit_oid]
    else
      token_scan_result.locations.where(ignore_token: "include").group(:commit_oid).pluck(:commit_oid)
    end

    load_commits(commit_oids)
  end

  def check_token_scanning_is_enabled
    render_404 unless current_repository.show_token_scanning_ui?(current_user)
  end

  def require_token_scan_result
    render_404 unless token_scan_result.present?
  end

  def use_token_scanning_service?
    return @use_token_scanning_service if defined? @use_token_scanning_service

    @use_token_scanning_service = current_repository.use_token_scanning_service?(current_user)
  end
end
