# frozen_string_literal: true

class Repos::CodeScanningController < AbstractRepositoryController
  include ScanningControllerMethods

  before_action :login_required
  before_action :check_code_scanning_read, except: %i(related_location_popover reopen close)
  before_action :check_code_scanning_write, only: %i(reopen close)
  statsd_tag_actions only: [:index, :show]

  PAGE_SIZE = 25

  def index
    page = params[:page]&.to_i || 1
    tool = tool_from_query_or_default

    if parsed_query.severity.present?
      rule_severity = GitHub::Turboscan.to_rule_severity(parsed_query.severity)
      if rule_severity.nil?
        parsed_query.remove_qualifier!(name: :severity, value: parsed_query.severity)
      end
    end

    if parsed_query.resolution.present?
      resolution = GitHub::Turboscan.to_resolution_filter(parsed_query.resolution)
      if resolution.nil?
        parsed_query.remove_qualifier!(name: :resolution, value: parsed_query.resolution)
      end
    end

    response = GitHub::Turboscan.results(
      repository_id: current_repository.id,
      rule_severity: rule_severity,
      rule_id: parsed_query.rule_id,
      limit: PAGE_SIZE,
      numeric_page: page,
      resolved_only: parsed_query.closed?,
      sort_order: sort_order,
      ref_names: ref_names,
      rule_tags: parsed_query.tags.presence,
      tool: tool&.name,
      resolution: resolution,
    )

    parsed_query.ensure_qualifier!(name: :tool, value: tool.display_name) if tool.present?
    parsed_query.ensure_qualifier!(name: :is, value: "open")

    commit_locations = {}
    response&.data&.results&.each do |result|
      commit_locations[result.number] = selected_commit_location(result)
    end

    render_template_view(
      "repos/code_scanning/index",
      RepositoryCodeScanning::IndexView,
      repository: current_repository,
      query: parsed_query,
      ref_names: ref_names,
      page: page,
      per_page: PAGE_SIZE,
      response: response,
      tools: tools,
      selected_tool: tool,
      commit_locations: commit_locations,
    )
  end

  def show
    response = GitHub::Turboscan.result(
      repository_id: current_repository.id,
      number: params[:number].to_i,
      ref_name: parsed_query.refs.first,
    )
    result = response&.data&.result

    return render_404 if response&.error&.code == :not_found

    if result.present?
      commit_location = selected_commit_location(result)
      commit_oids = [commit_location.commit_oid] + timeline_event_commit_oids(response)
      commits, graphql_commits = commits_by_oid(commit_oids)
      blob_map = blobs(commit_location.commit_oid, [commit_location.location.file_path])
      workflow_run_map = workflow_runs(response.data.timeline_events)
    end

    tool = result&.tool.present? ? CodeScanning::Tool.from_name(result.tool.name) : nil

    render_template_view(
      "repos/code_scanning/show",
      RepositoryCodeScanning::ShowView,
      repository: current_repository,
      response: response,
      commit_map: commits,
      graphql_commit_map: graphql_commits,
      selected_commit_location: commit_location,
      blob_map: blob_map,
      workflow_run_map: workflow_run_map,
      query: parsed_query,
      tools: tools,
      selected_tool: tool,
    )
  end

  def ref_list
    respond_to do |format|
      format.html do
        render_partial_view(
          "repos/code_scanning/refs_list",
          RepositoryCodeScanning::IndexView,
          repository: current_repository,
          query: parsed_query,
          ref_names: ref_names,
        )
      end

      format.json do
        branch_names = current_repository.heads.refs_with_default_first.map do |branch|
          {
            value: branch.qualified_name,
            description: branch.name_for_display,
          }
        end
        tag_names = current_repository.tags.take(100).map do |tag|
          {
            value: tag.qualified_name,
            description: tag.name_for_display,
          }
        end
        render json: branch_names + tag_names
      end
    end
  end

  def rule_list
    response = GitHub::Turboscan.rules(
      repository_id: current_repository.id,
      tool: tool_from_query_or_default&.name,
    )

    respond_to do |format|
      format.html do
        render_partial_view(
          "repos/code_scanning/rule_list",
          RepositoryCodeScanning::IndexView,
          repository: current_repository,
          query: parsed_query,
          response: response,
        )
      end

      format.json do
        rule_suggestions = response&.data&.rules&.uniq(&:id)&.sort_by(&:id)&.map do |rule|
          { description: rule.short_description, value: rule.id }
        end
        render json: rule_suggestions || []
      end
    end
  end

  def tag_list
    response = GitHub::Turboscan.rule_tags(
      repository_id: current_repository.id,
      tool: tool_from_query_or_default&.name,
    )

    respond_to do |format|
      format.html do
        render_partial_view(
          "repos/code_scanning/tag_list",
          RepositoryCodeScanning::IndexView,
          repository: current_repository,
          query: parsed_query,
          response: response,
        )
      end

      format.json do
        tags = response&.data&.rule_tags&.sort&.map { |id| { value: id } }
        render json: tags || []
      end
    end
  end

  def code_paths
    response = GitHub::Turboscan.code_paths(
      repository_id: current_repository.id,
      number: params[:number].to_i,
      ref_name: parsed_query.refs.first,
    )

    return render_404 if response&.error&.code == :not_found

    if response&.data&.result
      commit_location = selected_commit_location(response.data.result)
      commits, graphql_commits = commits_by_oid([commit_location.commit_oid])
      code_paths = response&.data&.code_paths || []
      blob_paths = [commit_location.location.file_path].concat(
                     code_paths.map(&:steps).flatten.map { |step| step&.location&.file_path },
                   ).uniq
      blob_map = blobs(commit_location.commit_oid, blob_paths)
    end

    render_partial_view(
      "repos/code_scanning/code_paths",
      RepositoryCodeScanning::CodePathsView,
      repository: current_repository,
      response: response,
      commit_map: commits,
      graphql_commit_map: graphql_commits,
      blob_map: blob_map,
      selected_commit_location: commit_location,
    )
  end

  def related_location_popover
    # TODO: how to communicate the location to this controller method?
    # We are currently passing a deconstructed location, but could re-use an
    # existing turboscan endpoint and extract the relevant location, or create
    # a new endpoint that takes a result_number + commit_oid +
    # related_location_index and returns the relevant location

    file_path = params[:file_path]
    commit_oid = params[:commit_oid]

    blob_map = blobs(commit_oid, [file_path])
    blob = blob_map[file_path]

    location = {
      file_path: file_path,
      start_line: params[:start_line].to_i,
      end_line: params[:end_line].to_i,
      start_column: params[:start_column].to_i,
      end_column: params[:end_column].to_i,
    }

    render_partial_view(
      "repos/code_scanning/related_location_popover_content",
      RepositoryCodeScanning::RelatedLocationView,
      repository: current_repository,
      location: location,
      blob: blob,
      commit_oid: commit_oid,
    )
  end

  def close
    resolution = GitHub::Turboscan.to_resolution(params[:reason])
    return head :bad_request if resolution.nil?

    resolve_options = {
      repository_id: current_repository.id,
      resolution: resolution,
      resolver_id: current_user.id,
    }
    alert_numbers = Array(params[:number]).take(PAGE_SIZE).map(&:to_i).uniq

    closed_alert_numbers = []

    alert_numbers.each do |number|
      response = GitHub::Turboscan.resolve_result(resolve_options.merge(number: number))

      if response&.error&.code == :not_found
        flash[:error] = "Couldn’t find the alert to close."
        break
      elsif response.blank? || response.error.present?
        flash[:error] = "There was an issue closing the alert. Please try again."
        break
      else
        closed_alert_numbers << number
      end
    end

    enqueue_code_scanning_annotations_jobs(closed_alert_numbers)

    redirect_to :back
  end

  def reopen
    reopen_options = {
      repository_id: current_repository.id,
      reopener_id: current_user.id,
    }
    alert_numbers = Array(params[:number]).take(PAGE_SIZE).map(&:to_i).uniq

    reopened_alert_numbers = []

    alert_numbers.each do |number|
      response = GitHub::Turboscan.reopen_result(reopen_options.merge(number: number))

      if response&.error&.code == :not_found
        flash[:error] = "Couldn’t find the alert to reopen."
        break
      elsif response.blank? || response.error.present?
        flash[:error] = "There was an issue reopening the alert. Please try again."
        break
      else
        reopened_alert_numbers << number
      end
    end

    enqueue_code_scanning_annotations_jobs(reopened_alert_numbers)

    redirect_to :back
  end

  def setup
    render_template_view(
      "repos/code_scanning/setup",
      RepositoryCodeScanning::View,
      repository: current_repository,
    )
  end

  private

  def parsed_query
    @parsed_query ||= Search::Queries::CodeScanningQuery.new(params[:query])
  end

  def check_code_scanning_read
    render_404 unless current_repository.code_scanning_readable_by?(current_user)
  end

  def check_code_scanning_write
    render_404 unless current_repository.code_scanning_writable_by?(current_user)
  end

  def blobs(commit_oid, blob_paths)
    blob_map = {}
    if commit_oid
      blob_paths_with_commit = blob_paths.map { |blob_path| [commit_oid, blob_path] }
      # Fetch blob_oids and match them to paths
      blob_oids = current_repository.rpc.read_blob_oids(blob_paths_with_commit, skip_bad: true)
      paths_by_oid = Hash[blob_oids.zip(blob_paths)]

      # Fetch blobs by blob_oid and match them to paths
      raw_blobs = current_repository.rpc.read_blobs(blob_oids.select(&:present?))
      raw_blobs.map do |raw_blob|
        blob_path = paths_by_oid[raw_blob["oid"]]
        blob_map[blob_path] = TreeEntry.new(current_repository, raw_blob.merge("path" => blob_path))
      end
    end
    blob_map
  end

  def workflow_runs(timeline_events)
    workflow_run_ids = timeline_events.map(&:workflow_run_id).select(&:nonzero?).uniq
    Actions::WorkflowRun.with_ids(workflow_run_ids).where(repository: current_repository).index_by(&:id)
  end

  def sort_order
    case parsed_query.sort
    when "created-desc" then :CREATED_DESCENDING
    when "created-asc"  then :CREATED_ASCENDING
    when "updated-desc" then :UPDATED_DESCENDING
    when "updated-asc"  then :UPDATED_ASCENDING
    end
  end

  def ref_names
    @ref_names ||= parsed_query.refs.presence || current_repository.default_code_scanning_ref_names
  end

  def selected_commit_location(result)
    commit_locations = result.instances
    # refs that we know that this alert appeared in, respecting the current ref filter
    available_refs = commit_locations.map(&:ref_name) & ref_names
    # high priority refs that this alert appeared in, respecting the current ref filter
    priority_refs = current_repository.default_code_scanning_ref_names & available_refs

    ref_name = priority_refs.present? ? priority_refs.first : available_refs.first
    if ref_name.present?
      ref_location = commit_locations.find { |loc| loc.ref_name == ref_name }
    end
    ref_location || commit_locations.first
  end

  def timeline_event_commit_oids(response)
    if timeline_events = response&.data&.timeline_events
      timeline_events.map(&:commit_oid).reject(&:blank?)
    else
      []
    end
  end

  def tool_names
    @tool_names ||= GitHub::Turboscan.tool_names(repository_id: current_repository.id)&.data&.tools&.map(&:name) || []
  end

  def tools
    @tools ||= tool_names.map { |name| CodeScanning::Tool.from_name(name) }
  end

  def tool_from_query_or_default
    @tool_name_from_query_or_default ||= parsed_query.tool.present? ? CodeScanning::Tool.from_name_or_display_name(parsed_query.tool) : default_tool
  end

  def default_tool
    tools.find { |t| t.name == CodeScanning::Tool.default_tool_name } || tools.sort_by(&:display_name).first
  end

  def enqueue_code_scanning_annotations_jobs(alert_numbers)
    # no need to refresh anything if no alert was changed
    return unless alert_numbers.present?
    current_repository.refresh_code_scanning_status(alert_numbers: alert_numbers)
  end
end
