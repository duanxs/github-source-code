# frozen_string_literal: true

class Repos::ActionsSettings::RunnerLabelsController < AbstractRepositoryController
  include Actions::RunnersHelper

  map_to_service :actions_runners

  before_action :login_required
  before_action :ensure_admin_access

  def index
    respond_to do |format|
      format.html do
        render Actions::RunnerLabelsComponent.new(
          owner: current_repository,
          runner_ids: [params[:runner_id]],
          labels: labels_for(current_repository),
          selected_labels: params[:applied_labels]
        )
      end
    end
  end

  def bulk
    runners = Actions::Runner.with_ids(params[:runner_ids], entity: current_repository)
    selected_labels = runners.flat_map(&:label_ids)
    respond_to do |format|
      format.html do
        render Actions::RunnerLabelsComponent.new(
          owner: current_repository,
          runner_ids: runners.map(&:id),
          labels: labels_for(current_repository),
          selected_labels: selected_labels,
          descriptor: "bulk"
        )
      end
    end
  end

  def create
    label = create_label_for(current_repository, name: params[:name])

    respond_to do |wants|
      wants.html do
        if label
          render Actions::RunnerLabelComponent.new(label: label, selected: false)
        else
          errorMessage = "Sorry, there was a problem adding your label."
          render json: { message: errorMessage }, status: :unprocessable_entity
        end
      end
    end
  end

  def update
    labels = Array(params[:labels])
    affected_runner = update_label_for(current_repository, runner_id: params[:runner_id], labels: labels)

    unless affected_runner
      flash[:error] = "Sorry, there was a problem updating your labels"
    end

    redirect_to repository_actions_settings_list_runners_path
  end

  def bulk_update
    runners = bulk_update_labels_for(current_repository, runner_ids: Array(params[:runner_ids]), additions: Array(params[:add]), removals: Array(params[:remove]))

    unless runners
      flash[:error] = "Sorry, there was a problem updating your labels"
    end

    redirect_to repository_actions_settings_list_runners_path
  end
end
