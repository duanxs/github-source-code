# frozen_string_literal: true

class Repos::AccessController < AbstractRepositoryController

  IndexQuery = parse_query <<-'GRAPHQL'
    query(
      $owner: String!,
      $name: String!,
      $affiliation: CollaboratorAffiliation,
      $query: String,
      $first: Int,
      $after: String,
      $last: Int,
      $before: String
    ) {
      repository(owner: $owner, name: $name) {
        owner {
          __typename
          ... on Organization {
            viewerCanAdminister
          }
        }
        ...Views::Repositories::People::Index::Repository
        ...Views::Repositories::People::PeopleTable::Repository
      }
    }
  GRAPHQL

  def index
    affiliation = params[:affiliation].present? ? params[:affiliation] : "ALL"

    variables = {
      owner: params[:user_id],
      name: params[:repository],
      affiliation: affiliation,
      query: params[:query],
    }
    variables = variables.merge(graphql_pagination_params(page_size: 30))

    data = platform_execute(IndexQuery, variables: variables)

    return render_404 unless data.repository
    return render_404 unless data.repository.owner.__typename == "Organization"
    return render_404 unless data.repository.owner.viewer_can_administer?

    respond_to do |format|
      format.html do
        if request.xhr?
          headers["Cache-Control"] = "no-cache, no-store"
          render partial: "repositories/people/people_table", locals: { repository: data.repository }
        else
          render "repositories/people/index", locals: { repository: data.repository }
        end
      end
    end
  end

  ExportQuery = parse_query <<-'GRAPHQL'
    query($owner: String!, $name: String!) {
      repository(owner: $owner, name: $name) {
        repoAccessExportSupported: planSupports(feature: REPO_ACCESS_EXPORT)
        owner {
          __typename
          ... on Organization {
            viewerCanAdminister
          }
        }
      }
    }
  GRAPHQL

  def export
    variables = {
      owner: params[:user_id],
      name: params[:repository],
    }

    data = platform_execute(ExportQuery, variables: variables)

    return render_404 unless repo = data.repository

    owner = repo.owner
    return render_404 unless owner.__typename == "Organization"
    return render_404 unless owner.viewer_can_administer?
    return render_404 unless repo.repo_access_export_supported?

    report = Repository::AccessReport.new(repository: current_repository, viewer: current_user)

    response.headers["Content-Type"] = "text/csv"

    send_data \
      report.generate_csv,
      type:     "text/csv",
      filename: report.filename
  end
end
