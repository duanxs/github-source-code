# frozen_string_literal: true

class Repos::RestoreController < ApplicationController
  before_action :login_required, :feature_enabled?, :perform_conditional_access_checks, :safe_actor

  def restore
    return render_404 unless safe_repo
    RepositoryRestoreJob.perform_later(params[:id].to_i, current_user.id)
    respond_to do |format|
      format.html do
        if request.xhr?
          render partial: "settings/repo_restoring", locals: { repo_id: params[:id] }
        else
          redirect_to :back
        end
      end
    end
  end

  def restore_partial
    respond_to do |format|
      format.html do
        render partial: "settings/repo_restoring", locals: { repo_id: params[:id] }
      end
    end
  end

  def restore_status
    job = GitHub::Jobs::RepositoryRestore.new(params[:id])
    msg = job.safe_message
    render html: msg, status: job.status(msg)
    job.reset_message_if_finished
  end

  private

  def feature_enabled?
    return render_404 if GitHub.enterprise?
  end

  def repo
    @repo ||= Archived::Repository.where(id: params[:id]).network_safe_restoreable.first
  end

  # In this case we need to be able to find the Live repository if
  # The restore job has completed.
  # Checking for the `Repository` in `owner` allows us to return a "Done!" status for Archived::Repository's that have been restored into the Repository table & removed from `Archived::Repository`.
  def owner
    return @owner if defined?(@owner)
    target_repo = repo.presence || Repository.where(id: params[:id]).first
    @owner = User.find_by_id(target_repo&.owner_id)
  end

  # In this case we want to return false if
  # - There is already a a restore job running for this respond_to
  # - safe_to_restore checks to see if the repo was deleted more
  #   than an hour ago
  # - We only show staff deletedd repos to staff actors
  def safe_repo
    return false unless repo.present?
    return false if repo.has_current_restore_job?
    return false unless repo.safe_to_restore
    return false if repo.deleted_by_staff? && !current_user.staff?
    true
  end

  # Here we check to see if the owner is present for either
  # the Archived::Repository or the newly restored Repository
  # if the owner exists, we then check to see if the
  # owner is the current user or if the owner is an admin of the targeted org
  def safe_actor
    return render_404 unless owner.present?
    return render_404 unless owner.can_restore_repository?(current_user)
  end

  def target_for_conditional_access
    return :no_target_for_conditional_access unless owner
    owner
  end
end
