# rubocop:disable Style/FrozenStringLiteralComment

class Repos::AdvisoriesController < AbstractRepositoryController
  areas_of_responsibility :security_advisories

  before_action :check_feature_is_enabled
  before_action :authorize_advisory_management, except: [:index, :show, :update, :update_body, :show_partial, :edit_history_log, :add_credit, :accept_credit, :decline_credit, :decline_credit_and_block_user]
  before_action :authorize_advisory_writable, only: [:update, :update_body, :show_partial, :edit_history_log, :add_credit]
  before_action :find_advisory, except: [:index, :new, :create]

  statsd_tag_actions only: [:index, :new, :create, :show, :update, :publish, :request_cve, :open_workspace, :workspace, :merge]

  def index
    render_template_view "repos/advisories/index", RepositoryAdvisories::IndexView, {
      current_repository: current_repository,
      state: params[:state] || "draft",
      page: params[:page] || 1,
    }
  end

  def new
    render_template_view(
      "repos/advisories/new",
      RepositoryAdvisories::ShowView,
      repository: current_repository,
      advisory: current_repository.repository_advisories.new,
    )
  end

  def create
    advisory = current_repository.repository_advisories.build(create_advisory_params)

    if advisory.save
      GlobalInstrumenter.instrument("repository_advisory.create", {
        repository_advisory: advisory,
      })

      redirect_to gh_repository_advisory_path(advisory)
    else
      flash.now[:error] = "Advisory could not be created."

      render_template_view(
        "repos/advisories/new",
        RepositoryAdvisories::ShowView,
        { repository: current_repository, advisory: advisory },
        status: :unprocessable_entity,
      )
    end
  end

  PreloadQuery = parse_query <<-'GRAPHQL'
    query($ids: [ID!]!) {
      nodes(ids: $ids) {
        ...PlatformPreloadHelper::BaseFragment
        ...Views::Repos::Advisories::Body
        ...Views::Repos::Advisories::Timeline::Comment
      }
    }
  GRAPHQL

  def preload_advisory_records(advisory)
    records_to_preload = [advisory]
    records_to_preload.concat(advisory.comments.to_a)
    preload_platform_objects(records_to_preload) do |ids|
      platform_execute(PreloadQuery, variables: { ids: ids }).nodes
    end
  end

  def show
    preload_advisory_records(@advisory)

    view = create_view_model(
      RepositoryAdvisories::ShowView,
      repository: current_repository,
      advisory: @advisory,
    )

    if view.readable?
      mark_thread_as_read @advisory
      mark_credit_notification_as_read @advisory
      render "repos/advisories/show_wrapper", locals: { view: view }
    else
      render_404
    end
  end

  def update
    update_successful = false

    if @advisory.cve_request_pending?
      @advisory.errors.add(:base, "This advisory is locked for review by GitHub")
    else
      update_successful = @advisory.handle_update(current_user, update_advisory_params)

      if update_successful
        GlobalInstrumenter.instrument("repository_advisory.update", {
          repository_advisory: @advisory,
          actor: current_user,
          request_curation: @advisory.published?,
        })

        @advisory.notify_socket_subscribers
      end
    end

    respond_to do |format|
      format.html do
        if update_successful
          redirect_to gh_repository_advisory_path(@advisory), notice: "The advisory was updated."
        else
          flash.now[:error] = "Unable to update advisory: #{@advisory.errors.full_messages.join(", ")}"
          show
        end
      end

      format.json do
        if update_successful
          render json: advisory_payload(@advisory)
        else
          render json: { errors: @advisory.errors.full_messages }, status: :unprocessable_entity
        end
      end
    end
  end

  def update_body
    # We no longer support the RepositoryAdvisory#body attribute, so we now
    # prevent this endpoint from being hit if the body attribute is nil.

    if @advisory.body.blank?
      raise ActionController::UnpermittedParameters.new([:body])
    end

    text =
      if operation = TaskListOperation.from(params[:task_list_operation])
        operation.call(@advisory.body)
      else
        params[:repository_advisory][:body]
      end

    @advisory.update_body(text, current_user) if text

    respond_to do |wants|
      wants.html do
        redirect_to :back
      end
      wants.json do
        if @advisory.valid?
          render json: {
            source: @advisory.body,
            body: @advisory.body_html,
            newBodyVersion: @advisory.body_version,
            editUrl: show_comment_edit_history_path(@advisory.global_relay_id),
          }
        else
          render json: { errors: @advisory.errors.full_messages }, status: :unprocessable_entity
        end
      end
    end
  end

  def publish
    if @advisory.workspace_clean? && publish_advisory(current_user)
      redirect_to gh_repository_advisory_path(@advisory), notice: "The advisory was published!"
    else
      redirect_to gh_repository_advisory_path(@advisory), flash: { error: "Sorry, something went wrong. Please try again." }
    end
  end

  def request_cve
    view = create_view_model(
      RepositoryAdvisories::ShowView,
      repository: current_repository,
      advisory: @advisory,
    )

    if view.viewer_can_request_cve?
      GlobalInstrumenter.instrument("repository_advisory.request_cve", {
        repository_advisory: @advisory,
        actor: current_user,
      })

      @advisory.lock_cve_request
      @advisory.add_cve_requested_event(current_user)

      redirect_to gh_repository_advisory_path(@advisory), notice: "A CVE has been requested for this advisory."
    else
      redirect_to gh_repository_advisory_path(@advisory), flash: { error: "Sorry, you can’t request a CVE for this advisory at this time." }
    end
  end

  # assumption is that only users authorized to edit will end up pinging
  # this end point for live updates.
  def show_partial
    valid_partial_path = valid_partial?(params[:partial])

    if valid_partial_path
      if timeline_partial?(params[:partial])
        # if we're rendering comments, we have to
        # preload the query before calling `preloaded_platform_object`
        # in the view template.
        preload_platform_objects(@advisory.comments.to_a) do |ids|
          platform_execute(PreloadQuery, variables: { ids: ids }).nodes
        end
      elsif body_partial?(params[:partial])
        preload_platform_objects([@advisory]) do |ids|
          platform_execute(PreloadQuery, variables: { ids: ids }).nodes
        end
      end

      render_partial_view(
        valid_partial_path,
        RepositoryAdvisories::ShowView,
        repository: current_repository,
        advisory: @advisory,
      )
    else
      render_404
    end
  end

  def open_workspace
    if @advisory.closed?
      redirect_to gh_repository_advisory_path(@advisory), flash: { error: "Sorry, can't be add a repository to a security advisory once it is closed." }
    elsif @advisory.workspace_repository
      redirect_to gh_repository_advisory_path(@advisory), flash: { error: "Sorry, a repository already exists for this security advisory." }
    else
      workspace_repo = RepositoryAdvisory::WorkspaceRepositoryBuilder.perform(@advisory, current_user)

      if workspace_repo.save
        GlobalInstrumenter.instrument("repository_advisory.workspace_open", {
          repository_advisory: @advisory,
          actor: current_user,
        })

        redirect_to gh_repository_advisory_path(@advisory), notice: "Hang tight. We're creating the repository for you."
      else
        redirect_to gh_repository_advisory_path(@advisory), flash: { error: "Sorry, something went wrong. Please try again." }
      end
    end
  end

  def workspace
    return render_404 unless workspace = @advisory.workspace_repository

    if workspace.creating?
      head :accepted
    else
      render_partial_view "repos/advisories/repo_body", RepositoryAdvisories::ShowView,
        user: current_user,
        repository: current_repository,
        advisory: @advisory
    end
  end

  def merge_box
    view = create_view_model(
      RepositoryAdvisories::MergeBoxView,
      advisory: @advisory,
    )

    # explicitly force firing off the merge commit job if needed
    @advisory.enqueue_mergeable_updates

    respond_to do |format|
      format.html do
        if view.merge_state == :unknown
          head :accepted
        else
          render "repos/advisories/merge_box", locals: { view: view }
        end
      end
    end
  end

  def merge
    if stale_merge_submitted?(@advisory, params["head_shas"])
      redirect_to gh_repository_advisory_path(@advisory),
        flash: { error: "At least one pull request was updated. Please try again." }
      return
    end

    batch_merge = @advisory.build_batch_merge(actor: current_user)
    success, results = batch_merge.perform if batch_merge.valid?

    if success
      redirect_to gh_repository_advisory_path(@advisory),
        notice: "The temporary private fork’s pull requests have been merged."
    elsif results
      details = results.map { |pull, error| "##{pull.number} - #{error.fail_message}" }.to_sentence
      redirect_to gh_repository_advisory_path(@advisory),
        flash: { error: "There were problems merging these pull requests: #{details}" }
    else
      redirect_to gh_repository_advisory_path(@advisory),
        flash: { error: "These pull requests cannot be merged. See below for details." }
    end
  end

  def autocomplete_collaborator
    autocomplete_params = { query: params[:q], current_repository: current_repository, organization: current_organization }

    respond_to do |format|
      format.html_fragment do
        render_partial_view "repos/advisories/sidebar/autocomplete", RepositoryAdvisories::AutocompleteView, autocomplete_params
      end
    end
  end

  def add_collaborator
    potential_collaborator = find_collaborator
    return render_404 unless potential_collaborator

    if can_collaborate?(potential_collaborator)
      @advisory.add_collaborator(potential_collaborator)
    end

    @advisory.notify_socket_subscribers

    if request.xhr?
      head :ok
    else
      redirect_to repository_advisory_path(id: @advisory)
    end
  end

  def remove_collaborator
    removed_collab = find_collaborator
    return render_404 unless removed_collab

    @advisory.remove_collaborator(removed_collab)
    @advisory.notify_socket_subscribers

    if request.xhr?
      render html: ""
    else
      redirect_to :back
    end
  end

  def edit_history_log
    respond_to do |format|
      format.html do
        render partial: "repos/advisories/description_edit_history_log", locals: { comment: @advisory }
      end
    end
  end

  def add_credit
    recipient = User.find_by!(login: params.require(:credit_login))
    advisory_credit = @advisory.credits.build(recipient: recipient)

    render_partial_view(
      "repos/advisories/credits/form_row",
      RepositoryAdvisories::CreditView,
      { advisory_credit: advisory_credit, viewer_can_manage: true },
      locals: { index: params.require(:credit_index) },
    )
  end

  def accept_credit
    credit_to_accept = @advisory.credits.find_by!(recipient_id: current_user.id)
    credit_to_accept.accept(actor: current_user)

    redirect_to gh_repository_advisory_path(@advisory), notice: "You successfully accepted credit."
  end

  def decline_credit
    credit_to_decline = @advisory.credits.find_by!(recipient_id: current_user.id)
    credit_to_decline.decline(actor: current_user)

    redirect_to gh_repository_advisory_path(@advisory), notice: "You declined credit."
  end

  def decline_credit_and_block_user
    credit_to_decline = @advisory.credits.find_by!(recipient_id: current_user.id)
    credit_to_decline.decline(actor: current_user)

    user_to_block = credit_to_decline.creator

    if user_to_block
      current_user.block(user_to_block)
      redirect_to gh_repository_advisory_path(@advisory), notice: "You declined credit and blocked #{user_to_block}."
    else
      redirect_to gh_repository_advisory_path(@advisory), notice: "You declined credit."
    end
  end

  private

  # override AbstractRepositoryController#whitelist_from_referrer_sanitization
  #
  # by default, we always set the Referrer-Policy header but this is
  # overriden for public repos via #whitelist_from_referrer_sanitization
  #
  # if we are in the show action, skip the referrer-override unless
  # we're showing a published advisory; if we're showing a published
  # advisory, allow the whitelist_from_referrer_sanitization to proceed.
  def whitelist_from_referrer_sanitization
    if action_name == "show"
      super if find_advisory.published?
    else
      super
    end
  end

  def find_collaborator
    if current_organization
      if team_name = team_collaborator_param
        current_organization.teams.where(name: team_name).first
      else
        User.find_by_login(user_collaborator_param)
      end
    else
      User.find_by_login(user_collaborator_param)
    end
  end

  # If adding a user, that user just needs read access to the repository.
  # If adding a team, the team must belong to the current organization.
  def can_collaborate?(collab)
    return false unless current_repository.readable_by?(collab)

    case collab
    when Team
      # The team being added must belong to the current organization.
      current_organization && (current_organization == collab.organization)
    when User
      # Any user can be added (so long as they can see the repository).
      true
    else
      # We shouldn't get here; only a team or user can be a collaborator.
      false
    end
  end

  def current_organization
    return @current_organization if defined?(@current_organization)

    if current_repository.owner.organization?
      @current_organization = current_repository.owner
    else
      @current_organization = nil
    end
  end

  def find_advisory
    @advisory = current_repository.repository_advisories.find_by!(ghsa_id: params[:id])
  end

  def advisory_payload(advisory)
    {
      title: advisory.title,
      page_title: helpers.repository_advisory_page_title(advisory),
    }
  end

  PARTIAL_MAPPING = {
    "repository_advisory/title" => "repos/advisories/title",
    "repository_advisory/body" => "repos/advisories/body",
    "repository_advisory/timeline" => "repos/advisories/timeline",
    "repository_advisory/member_list" => "repos/advisories/sidebar/member_list",
  }

  def valid_partial?(partial)
    PARTIAL_MAPPING[partial]
  end

  def timeline_partial?(partial)
    partial == "repository_advisory/timeline"
  end

  def body_partial?(partial)
    partial == "repository_advisory/body"
  end

  def authorize_advisory_management
    render_404 unless current_repository.advisory_management_authorized_for?(current_user)
  end

  def authorize_advisory_writable
    render_404 unless find_advisory.writable_by?(current_user)
  end

  def check_feature_is_enabled
    render_404 unless current_repository.advisories_enabled?
  end

  def advisory_params
    params.require(:repository_advisory).permit(
      :title,
      :package,
      :ecosystem,
      :affected_versions,
      :patches,
      :severity,
      :cve_id,
      :description,
      credits_attributes: [
        :_destroy,
        :id,
        :recipient_id,
      ],
    )
  end

  # These are the parameters used when updating a repository advisory.
  #
  # We also iterate over the form parameters for each advisory credit and set
  # the creator_id if the credit is being created. We do this in the controller
  # rather than in the form to prevent spoofing.
  def update_advisory_params
    return @update_advisory_params if @update_advisory_params

    update_advisory_params = advisory_params

    if update_advisory_params[:credits_attributes].present?
      update_advisory_params[:credits_attributes].each_value do |credit_attributes|
        if credit_attributes[:id].blank?
          credit_attributes[:creator_id] = current_user.id
        end
      end
    end

    @update_advisory_params = update_advisory_params
  end

  # These are the parameters used when creating a repository advisory.
  #
  # In addition to the form parameters and advisory credit creator_id values,
  # we set the author_id of the repository advisory itself. We do this in the
  # controller rather than in the form to prevent spoofing.
  def create_advisory_params
    @create_advisory_params ||=
      update_advisory_params.merge(author_id: current_user.id)
  end

  def user_collaborator_param
    params.require(:member)
  end

  def team_collaborator_param
    user_collaborator_param.split("/", 2)[1]
  end

  def stale_merge_submitted?(advisory, submitted_head_shas)
    expected_shas = advisory.open_pull_requests.map(&:head_sha)

    expected_shas.sort != submitted_head_shas.sort
  end

  def publish_advisory(publisher)
    if @advisory.set_published(actor: publisher)
      @advisory.transaction do
        @advisory.submit_for_review
        @advisory.add_publish_event(publisher)
        @advisory.cleanup_workspace(current_user)
      end

      GlobalInstrumenter.instrument("repository_advisory.publish", {
        repository_advisory: @advisory,
        actor: publisher,
      })

      true
    else
      false
    end
  end

  def mark_credit_notification_as_read(advisory)
    credit_for_viewer = logged_in? && advisory.credits.find_by(recipient_id: current_user.id)
    mark_thread_as_read credit_for_viewer if credit_for_viewer
  end
end
