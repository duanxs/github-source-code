# frozen_string_literal: true

class Repos::SecurityController < AbstractRepositoryController
  statsd_tag_actions only: [:overall_count, :overview, :policy]

  def overall_count
    view = NavigationHelper::RepositoryNavigationView.new(current_user, current_repository)

    respond_to do |format|
      format.json do
        if view.security_count == 0
          head :no_content # don't display any count
        else
          render json: { count: view.formatted_security_count }
        end
      end

      format.all do
        render_404
      end
    end
  end

  def overview
    render_template_view(
      "repos/security/overview",
      Repositories::SecurityOverviewView,
      repository: current_repository,
      current_user: current_user,
    )
  end

  def policy
    security_policy = current_repository.security_policy

    if security_policy.exists?
      blob_view = create_view_model(Blob::ShowView, {
        repo: security_policy.repository,
        tree_name: security_policy.default_branch,
        blob: security_policy.file,
      })
    else
      new_file_view = create_view_model(Files::NewFileActionView, {
        repo: current_repository,
        tree_name: current_repository.default_branch,
      })
    end

    render "repos/security/policy", locals: {
      security_policy: security_policy,
      blob_view: blob_view,
      new_file_view: new_file_view,
    }
  end
end
