# frozen_string_literal: true

class Repos::AdvisoryCommentsController < AbstractRepositoryController
  areas_of_responsibility :security_advisories

  before_action :login_required
  before_action :check_feature_is_enabled
  before_action :find_advisory
  before_action :authorize_advisory_comments
  before_action :authorize_advisory_state_change, only: [:create],
    if: :state_change_requested?

  statsd_tag_actions only: [:create, :destroy, :update]

  def create
    comment = nil # For variable scoping

    if params[:comment_and_close].present?
      comment = @advisory.comment_and_close(current_user, params[:body])
    elsif params[:comment_and_reopen].present?
      comment = @advisory.comment_and_reopen(current_user, params[:body])
    else
      comment = @advisory.create_comment(current_user, params[:body])
    end

    if comment && comment.persisted?
      # Comment creation succeeded
      redirect_to comment.permalink
    elsif comment
      # Comment creation failed
      flash[:error] = comment.errors.full_messages.to_sentence
      redirect_to :back
    else
      # Comment creation was not requested
      redirect_to :back
    end
  end

  def update
    comment = @advisory.comments.find_by!(id: params[:comment_id])

    text =
      if operation = TaskListOperation.from(params[:task_list_operation])
        operation.call(comment.body)
      else
        params[:repository_advisory_comment][:body]
      end

    comment.update_body(text, current_user) if text

    respond_to do |wants|
      wants.html do
        redirect_to :back
      end
      wants.json do
        if comment.valid?
          render json: {
            source: comment.body,
            body: comment.body_html,
            newBodyVersion: comment.body_version,
            editUrl: show_comment_edit_history_path(comment.global_relay_id),
          }
        else
          render json: { errors: comment.errors.full_messages }, status: :unprocessable_entity
        end
      end
    end
  end

  def destroy
    comment = @advisory.comments.find_by!(id: params[:comment_id])
    comment.destroy

    if request.xhr?
      head :ok
    else
      redirect_to :back
    end
  end

  private

  def find_advisory
    @advisory = current_repository.repository_advisories.find_by!(ghsa_id: params[:id])
  end

  def check_feature_is_enabled
    render_404 unless current_repository.advisories_enabled?
  end

  def authorize_advisory_comments
    render_404 unless @advisory.writable_by?(current_user)
  end

  def authorize_advisory_state_change
    render_404 unless @advisory.adminable_by?(current_user)
  end

  def state_change_requested?
    params[:comment_and_close].present? || params[:comment_and_reopen].present?
  end
end
