# frozen_string_literal: true

class PendingPlanChangesController < ApplicationController

  before_action :login_required

  UpdateQuery = parse_query <<-'GRAPHQL'
    mutation($input: UpdatePendingPlanChangeInput!) {
      updatePendingPlanChange(input: $input) {
        pendingCycle {
          __typename # a selection is grammatically required here
        }
      }
    }
  GRAPHQL

  def update
    input = {
      login: params[:id],
      cancelDataPacks: !!params[:cancel_data_packs],
      cancelPlan: !!params[:cancel_plan],
      cancelPlanDuration: !!params[:cancel_plan_duration],
      cancelSeats: !!params[:cancel_seats],
    }
    data = platform_execute(UpdateQuery, variables: { input: input })

    if data.errors.any?
      error_type = data.errors.details[:updatePendingPlanChange]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)
      flash[:error] = data.errors.messages.values.join(", ")
    else
      notice = "Successfully cancelled the pending ".dup
      if params[:cancel_data_packs]
        notice << "LFS pack downgrade."
      elsif params[:cancel_plan_duration]
        notice << "plan duration change."
      else
        notice << "plan change."
      end

      flash[:notice] = notice
    end

    redirect_to :back
  end

  def target_for_conditional_access
    target = User.find_by_login(params[:id])
    return :no_target_for_conditional_access unless target
    target
  end
end
