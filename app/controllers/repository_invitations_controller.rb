# frozen_string_literal: true

class RepositoryInvitationsController < AbstractRepositoryController

  skip_before_action :privacy_check, only: [:show]
  skip_before_action :authorization_required

  areas_of_responsibility :repo_settings, :code_collab

  before_action :login_required, except: [:show]
  before_action :find_pending_invitation, only: [:show]
  before_action :scope_invitation, except: [:create, :destroy, :show]
  before_action :sudo_filter, only: [:create, :set_permissions]
  before_action :invitee_is_current_user, only: [:accept, :reject, :block_inviter]
  before_action :ensure_two_factor_requirement_is_met, only: [:accept]

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def current_business
    @_business ||= current_repository&.organization&.business
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def create
    member = User.find_by_login(params[:member])
    handle_member_not_found and return if member.nil?

    result = add_member(member)

    if result[:success]
      respond_to do |wants|
        wants.html do
          flash[:notice] = "Successfully invited #{member}."
          redirect_to edit_repository_path(current_repository)
        end
        wants.json do
          resp = { html: result[:html] }
          org = current_repository.organization
          resp[:seats] = if !org
            current_repository.available_seats
          else
            org.available_seats
          end
          render json: resp
        end
      end
    else
      respond_to do |wants|
        errors = result[:errors]
        error = errors.is_a?(ActiveModel::Errors) ? errors[:base].to_sentence : errors
        if error && error.include?("Rate limit")
          org_message = "If you need to add more users, either wait 24 hours or consider creating an organization
          (#{GitHub.help_url}/articles/what-s-the-difference-between-user-and-organization-accounts/)."
          error = %{
            You have reached the daily limit for the number of invitations you can send for this repository.
            #{ current_repository.organization ? '' : org_message } Please contact support with any questions you might have.
          }
        end
        wants.html do
          flash[:error] = error
          redirect_to edit_repository_path(current_repository)
        end
        wants.json { render json: {error: error } }
      end
    end
  rescue => boom
    respond_to do |wants|
      wants.html { redirect_to edit_repository_path(current_repository) }
      wants.json { render json: {error: "An unknown error occurred"} }
    end
  end

  def destroy
    member = params[:member]
    if User.valid_email?(member)
      invitation = RepositoryInvitation.find_by(email: member, repository_id: current_repository.id)
    else
      invitee = User.find_by_login(member)
      invitation = RepositoryInvitation.find_by(invitee_id: invitee&.id, repository_id: current_repository.id)
    end

    if invitation
      member_label = invitation.email_or_invitee_login
      if invitation.cancel!(actor: current_user)
        flash[:notice] = "#{member_label} is no longer invited to this repository."
        redirect_to repository_access_management_path(user_id: current_repository.owner.login, repository: current_repository.name)
      else
        respond_to do |wants|
          wants.html { redirect_to repository_access_management_path(user_id: current_repository.owner.login, repository: current_repository.name) }
          wants.json { render json: {error: "You are not authorized to delete an invitation."} }
        end
      end
    else
      respond_to do |wants|
        wants.html { redirect_to repository_access_management_path(user_id: current_repository.owner.login, repository: current_repository.name) }
        wants.json { render json: {error: "Invitation for user not found."} }
      end
    end
  end

  def show
    render_404 and return unless current_repository

    if logged_in?

      if pending_invitation.repository.private? &&  pending_invitation.invitee&.has_any_trade_restrictions?
        flash[:trade_controls_user_billing_error] = true
      end
      render_template_view("repository_invitations/show", RepositoryInvitations::ShowView,
        invitation: pending_invitation
      )
    else
      render_sign_up_via_repo_invitation(invitation: pending_invitation, invitation_token: params[:invitation_token])
    end
  end

  def accept
    if repo_invitation.accept!(acceptor: current_user)
      flash[:notice] = "You now have #{permission_string} access to the #{repo_invitation.repository.name_with_owner} repository."
      redirect_to repo_invitation.repository
    else

      repo = repo_invitation.repository
      if repo.trade_restricted?
        flash[:error] = TradeControls::Notices::Plaintext.org_invite_restricted
      elsif repo_invitation.invite_expired?
        # Add extra check to display the specific expired invite error
        flash[:error] = "This invitation has expired."
      else
        flash[:error] = "This invitation is invalid."
      end

      redirect_to current_user
    end
  end

  def reject
    repo_invitation.reject!
    flash[:notice] = "You have declined the invitation to the #{repo_invitation.repository.name_with_owner} repository."
    redirect_to current_user
  end

  def set_permissions
    repo_invitation.set_permissions(params[:permission], current_user)
    head 200
  rescue RepositoryInvitation::InsufficientAbilities
    head 403
  end

  def permission_string
    case repo_invitation.permission_string
    when "read"
      "view"
    when "triage"
      "triage"
    when "write"
      "push"
    when "maintain"
      "maintain"
    when "admin"
      "admin"
    else
      ""
    end
  end

  def block_inviter
    repo_invitation.reject!
    current_user.block(repo_invitation.inviter)
    flash[:notice] = "You have blocked #{repo_invitation.inviter.login}."
    redirect_to settings_user_blocks_path
  end

  private

  def invitee_is_current_user
    return render_404 unless repo_invitation
    return render_404 unless logged_in?

    # email invitations can be accepted by any user that received
    # the link with the correct token
    if repo_invitation.email?
      token = RepositoryInvitation.digest_token(params[:invitation_token])
      return render_404 unless token.present?

      render_404 unless SecurityUtils.secure_compare(repo_invitation.hashed_token, token)
    else
      render_404 unless repo_invitation.invitee == current_user
    end
  end

  def scope_invitation
    unless repo_invitation
      if current_repository
        redirect_to current_repository
      else
        redirect_to current_user
      end
    end
  end

  def repo_invitation
    @invitation ||= RepositoryInvitation.find_by_id(params[:invitation_id])
  end

  def add_member(member)
    result = RepositoryInvitation.invite_to_repo(
      member,
      current_user,
      current_repository
    )
    if result[:success]
      if current_repository.member?(member)
        result[:html] = render_partial_view_to_string(
              "edit_repositories/admin_screen/member",
              EditRepositories::Pages::CollaborationPageView,
              { repository: current_repository },
              object: member,
        )
      else
        result[:html] = render_partial_view_to_string(
              "edit_repositories/admin_screen/invitation",
              EditRepositories::Pages::CollaborationPageView, {},
              object: result[:invitation]
        )
      end
    end
    result
  end

  def handle_member_not_found
    respond_to do |wants|
      wants.html do
        flash[:error] = "User not found"
        redirect_to(edit_repository_path(current_repository))
      end
      wants.json do
        render json: {error: "User not found"}
      end
    end
  end

  def ensure_two_factor_requirement_is_met
    if !repo_invitation.repository.two_factor_requirement_met_by?(current_user)
      redirect_to repository_invitation_path(repo_invitation.invitee, repo_invitation.repository)
    end
  end

  def target_for_conditional_access
    return :no_target_for_conditional_access unless owner || repo_invitation
    return repo_invitation.repository.owner if repo_invitation
    owner
  end

  def find_pending_invitation
    @pending_invitation ||= if email_repo_invitation?
      find_email_invitation
    elsif logged_in?
      find_user_invitation
    end

    @pending_invitation || pending_invitation_not_found
  end
  attr_reader :pending_invitation

  def find_email_invitation
    return unless email_repo_invitation?
    current_repository.repository_invitations.find_by_token(params[:invitation_token])
  end

  def find_user_invitation
    current_user.received_repository_invitations.find_by_repository_id(current_repository.id)
  end

  def pending_invitation_not_found
    if login_required_to_view_invitation?
      login_required
    elsif current_repository.adminable_by?(current_user)
      flash[:notice] = "Repository invitation URLs work for invited users only. You may only share this URL with an invited user."
      redirect_to repository_access_management_path(current_repository.owner, current_repository), status: 301
    elsif current_repository.all_members.include?(current_user)
      redirect_to current_repository, status: 301
    else
      if current_repository.private?
        render_404
      else
        flash[:error] = "Sorry, we couldn't find that repository invitation. It is possible that the invitation was revoked or that you are not logged into the invited account."
        render html: "", layout: true
      end
    end
  end

  def login_required_to_view_invitation?
    !logged_in? && !email_repo_invitation?
  end

  def email_repo_invitation?
    params[:invitation_token].present?
  end

  def render_sign_up_via_repo_invitation(invitation:, invitation_token: nil)
    render "repository_invitations/sign_up_via_repo_invitation",
      locals: {invitation: invitation, invitation_token: invitation_token},
      layout: "session_authentication"
  end
end
