# frozen_string_literal: true

class WebSocketsController < ApplicationController
  before_action :login_required

  def show
    render plain: GitHub::WebSocket.luau_url(current_user, user_session)
  end

  private

  def require_conditional_access_checks?
    false
  end
end
