# frozen_string_literal: true

class MarketplaceListingsController < ApplicationController
  areas_of_responsibility :marketplace

  statsd_tag_actions only: [:index, :show]

  before_action :marketplace_required
  before_action :login_required, only: [:new, :create, :manage, :preview]
  before_action :sudo_filter, only: [:edit, :edit_description, :edit_contact_info]
  before_action :require_xhr, only: :screenshots
  before_action :set_suggested_target_id_cookie, only: :show
  before_action :set_repository_ids_cookie, only: :show

  before_action :add_csp_exceptions, only: [:edit, :show, :edit_description]
  CSP_EXCEPTIONS = {
    connect_src: [Marketplace::ListingScreenshot.storage_s3_hostname, Marketplace::ListingImage.storage_s3_hostname],
  }

  GITHUB_APP_TYPE       = "app".freeze
  OAUTH_APP_TYPE        = "oauth_app".freeze
  PERMITTED_APP_TYPES   = [GITHUB_APP_TYPE, OAUTH_APP_TYPE].freeze

  rescue_from PlatformHelper::InvalidCursorError, with: :render_404

  include AvatarHelper

  layout "marketplace"

  FreeTrialsQuery = parse_query <<-'GRAPHQL'
    query {
      ...Views::MarketplaceListings::FreeTrials::Root
    }
  GRAPHQL

  # Public: The Marketplace free trials page
  def free_trials
    data = platform_execute(FreeTrialsQuery)

    respond_to do |format|
      format.html do
        render "marketplace_listings/free_trials", locals: { data: data }
      end
    end
  end

  ShowQuery = parse_query <<-'GRAPHQL'
    query($listingSlug: String!, $planSelected: Boolean!, $planId: ID!, $languageCount: Int!, $planCount: Int!, $bulletCount: Int!, $screenshotCount: Int!, $loggedIn: Boolean!) {
      marketplaceListing(slug: $listingSlug) {
        isArchived
        name
        listableIsSponsorable
        viewerIsListingAdmin
        app {
          id
        }
      }
      viewer @include(if: $loggedIn) {
        subscription {
          item: subscriptionItem(listingSlug: $listingSlug, active: true) {
            id
          }
        }
      }
      ...Views::MarketplaceListings::Show::Root
    }
  GRAPHQL

  ShowPjaxQuery = parse_query <<-'GRAPHQL'
    query($listingSlug: String!, $planSelected: Boolean!, $planId: ID!, $planCount: Int!, $bulletCount: Int!, $loggedIn: Boolean!) {
      marketplaceListing(slug: $listingSlug) {
        isArchived
        name
        listableIsSponsorable
        viewerIsListingAdmin
      }
      ...Views::MarketplaceListings::PlansAndSelectedPlan::Root
    }
  GRAPHQL

  # Public: Integration detail page
  def show
    data = if pjax?
      platform_execute(ShowPjaxQuery, variables: {
        listingSlug: params[:listing_slug],
        loggedIn: logged_in?,
        bulletCount: Marketplace::ListingPlanBullet::BULLET_LIMIT_PER_LISTING_PLAN,
        planCount: Marketplace::ListingPlan::PLAN_LIMIT_PER_LISTING,
        planId: params[:plan_id] || "",
        planSelected: params[:plan_id].present?,
      })
    else
      platform_execute(ShowQuery, variables: {
        listingSlug: params[:listing_slug],
        loggedIn: logged_in?,
        oneMonthAgo: 1.month.ago.iso8601,
        oneYearAgo: 1.year.ago.iso8601,
        bulletCount: Marketplace::ListingPlanBullet::BULLET_LIMIT_PER_LISTING_PLAN,
        languageCount: Marketplace::Listing::MAX_SUPPORTED_LANGUAGES_COUNT,
        planCount: Marketplace::ListingPlan::PLAN_LIMIT_PER_LISTING,
        planId: params[:plan_id] || "",
        planSelected: params[:plan_id].present?,
        screenshotCount: Marketplace::ListingScreenshot::SCREENSHOT_LIMIT_PER_LISTING,
      })
    end

    marketplace_listing = data.marketplace_listing
    return render_404 unless marketplace_listing
    set_setup_state_cookie(marketplace_listing) unless pjax?

    if marketplace_listing.is_archived? && current_user_cannot_admin_listing?(marketplace_listing)
      return redirect_to_billing_settings_with_delisted_error(
        marketplace_listing: marketplace_listing,
        subscription_item: data.viewer.subscription.item,
      )
    elsif marketplace_listing.listable_is_sponsorable?
      return render_404
    end

    respond_to do |format|
      format.html do
        if pjax?
          render partial: "marketplace_listings/plans_and_selected_plan", locals: { data: data }
        else
          render "marketplace_listings/show", locals: { data: data }
        end
      end
    end
  end

  NEW_QUERY_PAGE_SIZE = 5

  NewQuery = parse_query <<-'GRAPHQL'
    query($oauthCursor: String, $appCursor: String, $actionCursor: String, $pageSize: Int!, $excludeMarketplaceListings: Boolean!) {
      ...Views::MarketplaceListings::New::Root
    }
  GRAPHQL

  OauthApplicationsQuery = parse_query <<-'GRAPHQL'
    query($oauthCursor: String, $pageSize: Int!, $excludeMarketplaceListings: Boolean!) {
      ...Views::MarketplaceListings::OauthApplications::Root
    }
  GRAPHQL

  IntegrationsQuery = parse_query <<-'GRAPHQL'
    query($appCursor: String, $pageSize: Int!, $excludeMarketplaceListings: Boolean!) {
      ...Views::MarketplaceListings::Integrations::Root
    }
  GRAPHQL

  ActionsQuery = parse_query <<-'GRAPHQL'
    query($actionCursor: String, $pageSize: Int!, $excludeMarketplaceListings: Boolean!) {
      ...Views::MarketplaceListings::Actions::Root
    }
  GRAPHQL

  def new
    query = request.xhr? ? new_xhr_query : NewQuery

    data = platform_execute(query, variables: {
      oauthCursor: params[:oauth_cursor],
      appCursor: params[:app_cursor],
      actionCursor: params[:action_cursor],
      excludeMarketplaceListings: true,
      pageSize: NEW_QUERY_PAGE_SIZE,
    })

    respond_to do |format|
      format.html do
        if request.xhr?
          if params[:oauth_cursor]
            render partial: "marketplace_listings/oauth_applications", locals: { data: data }
          elsif params[:app_cursor]
            render partial: "marketplace_listings/integrations", locals: { data: data }
          elsif params[:action_cursor]
            render partial: "marketplace_listings/actions", locals: { data: data }
          end
        else
          render "marketplace_listings/new", locals: { data: data }
        end
      end
    end
  end

  NewWithIntegratableQuery = parse_query <<-'GRAPHQL'
    query {
      ...Views::MarketplaceListings::NewWithIntegratable::Root
    }
  GRAPHQL

  def new_with_integratable
    integratable = find_integratable(params[:type], params[:id])

    unless integratable
      return render_404
    end

    if (listing = integratable.marketplace_listing).present?
      if listing.adminable_by?(current_user)
        return redirect_to edit_marketplace_listing_path(listing)
      end

      return render_404
    end

    unless PERMITTED_APP_TYPES.include?(params[:type])
      flash[:error] = "You must specify which GitHub App or OAuth application you'd like to list."
      return redirect_to marketplace_path
    end

    return render_404 unless integratable && integratable.adminable_by?(current_user)

    data = platform_execute(NewWithIntegratableQuery)

    respond_to do |format|
      format.html do
        render "marketplace_listings/new_with_integratable",
          locals: { data: data, integratable: integratable, integratable_type: params[:type],
                    form_data: {} }
      end
    end
  end

  CreateQuery = parse_query <<-'GRAPHQL'
    mutation($input: CreateMarketplaceListingInput!) {
      createMarketplaceListing(input: $input) {
        marketplaceListing {
          slug
        }
      }
    }
  GRAPHQL

  def create
    input = input_for_creation
    create_data = platform_execute(CreateQuery, variables: { input: input })

    if create_data.errors.any?
      new_with_integratable_data = platform_execute(NewWithIntegratableQuery)
      error_type = create_data.errors.details[:createMarketplaceListing]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      flash[:error] = create_data.errors.messages.values.join(", ")
      integratable = integratable_for_creation
      type = integratable.is_a?(OauthApplication) ? OAUTH_APP_TYPE : GITHUB_APP_TYPE

      return render "marketplace_listings/new_with_integratable",
        locals: { data: new_with_integratable_data, integratable: integratable,
                  integratable_type: type, form_data: input }
    end

    flash[:notice] = "A draft of your Marketplace listing has been saved. You can " +
                     "customize your listing further."
    slug = create_data.create_marketplace_listing.marketplace_listing.slug
    redirect_to edit_marketplace_listing_path(slug)
  end

  EditQuery = parse_query <<-'GRAPHQL'
    query($listingSlug: String!, $loggedIn: Boolean!) {
      enabledFeatures
      marketplaceListing(slug: $listingSlug) {
        viewerCanEdit
        listableIsSponsorable
      }
      ...Views::MarketplaceListings::Edit::Root
      ...Marketplace::Listings::OnboardingPageView::Query::Root
    }
  GRAPHQL

  def edit
    data = platform_execute(EditQuery, variables: {
      listingSlug: params[:listing_slug],
      loggedIn: logged_in?,
    })

    marketplace_listing = data.marketplace_listing
    return render_404 unless marketplace_listing && marketplace_listing.viewer_can_edit?
    return render_404 if marketplace_listing.listable_is_sponsorable?

    view = create_view_model(Marketplace::Listings::OnboardingPageView, { data: data })

    GitHub.instrument("marketplace_onboarding.progress", view.metrics_progress_payload.merge(current_user: current_user))

    respond_to do |format|
      format.html do
        render "marketplace_listings/edit", locals: { data: data, view: view }
      end
    end
  end

  EditDescriptionQuery = parse_query <<-'GRAPHQL'
    query($listingSlug: String!, $languageCount: Int!) {
      enabledFeatures
      marketplaceListing(slug: $listingSlug) {
        viewerCanEdit
        listableIsSponsorable
      }
      ...Views::MarketplaceListings::Edit::Description::Root
    }
  GRAPHQL

  def edit_description
    data = platform_execute(EditDescriptionQuery, variables: {
      listingSlug: params[:listing_slug],
      languageCount: Marketplace::Listing::MAX_SUPPORTED_LANGUAGES_COUNT,
    })

    marketplace_listing = data.marketplace_listing
    return render_404 unless marketplace_listing && marketplace_listing.viewer_can_edit?
    return render_404 if marketplace_listing.listable_is_sponsorable?

    respond_to do |format|
      format.html do
        render "marketplace_listings/edit/description", locals: { data: data }
      end
    end
  end

  EditContactInfoQuery = parse_query <<-'GRAPHQL'
    query($listingSlug: String!)  {
      enabledFeatures
      marketplaceListing(slug: $listingSlug) {
        viewerCanEdit
        listableIsSponsorable
      }
      ...Views::MarketplaceListings::Edit::ContactInfo::Root
    }
  GRAPHQL

  def edit_contact_info
    data = platform_execute(EditContactInfoQuery, variables: {
      listingSlug: params[:listing_slug],
    })

    marketplace_listing = data.marketplace_listing
    return render_404 unless marketplace_listing && marketplace_listing.viewer_can_edit?
    return render_404 if marketplace_listing.listable_is_sponsorable?

    respond_to do |format|
      format.html do
        render "marketplace_listings/edit/contact_info", locals: { data: data }
      end
    end
  end

  UpdateQuery = parse_query <<-'GRAPHQL'
    mutation($input: UpdateMarketplaceListingInput!) {
      updateMarketplaceListing(input: $input) {
        marketplaceListing {
          slug
          logoBackgroundColor
          shortDescription
        }
      }
    }
  GRAPHQL

  def update
    input = marketplace_params.merge(slug: params[:listing_slug])
    data = platform_execute(UpdateQuery, variables: { input: input })

    if data.errors.any?
      error_type = data.errors.details[:updateMarketplaceListing]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      message = data.errors.messages.values.join(", ")

      if request.xhr?
        return render json: { error: message }, status: :unprocessable_entity
      else
        flash[:error] = message
        return redirect_to(edit_marketplace_listing_path(params[:listing_slug]))
      end
    end

    updated_listing = data.update_marketplace_listing.marketplace_listing

    path = update_redirect_path(updated_listing)

    if request.xhr?
      render json: { path: path, bgcolor: updated_listing.logo_background_color, short_description: updated_listing.short_description }
    else
      redirect_to path
    end
  end


  RequestApprovalQuery = parse_query <<-'GRAPHQL'
    mutation($input: RequestMarketplaceListingApprovalInput!) {
      requestMarketplaceListingApproval(input: $input) {
        marketplaceListing {
          resourcePath
        }
      }
    }
  GRAPHQL

  def request_approval
    data = platform_execute(RequestApprovalQuery, variables: { input: { slug: params[:listing_slug] } })

    if data.errors.any?
      error_type = data.errors.details[:requestMarketplaceListingApproval]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      flash[:error] = data.errors.messages.values.join(", ")
      return redirect_to(marketplace_listing_path(params[:listing_slug]))
    end

    flash[:notice] = "Thank you for your submission. We will review your listing and get back to " +
                     "you shortly."
    redirect_to data.request_marketplace_listing_approval.marketplace_listing.resource_path.to_s
  end

  RedraftQuery = parse_query <<-'GRAPHQL'
    mutation($input: RedraftMarketplaceListingInput!) {
      redraftMarketplaceListing(input: $input) {
        marketplaceListing {
          slug
        }
      }
    }
  GRAPHQL

  def redraft
    input = { slug: params[:listing_slug] }
    data = platform_execute(RedraftQuery, variables: { input: input })

    if data.errors.any?
      error_type = data.errors.details[:redraftMarketplaceListing]&.first["type"]
      return render_404 if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)

      flash[:error] = data.errors.messages.values.join(", ")
      return redirect_to(marketplace_listing_path(params[:listing_slug]))
    end

    flash[:notice] = "Your listing is no longer awaiting review from GitHub staff. You may " +
                     "make additional changes and resubmit when ready."
    slug = data.redraft_marketplace_listing.marketplace_listing.slug
    redirect_to edit_marketplace_listing_path(slug)
  end

  ScreenshotsQuery = parse_query <<-'GRAPHQL'
    query($slug: String!) {
      marketplaceListing(slug: $slug) {
        ...Views::MarketplaceListings::Edit::ProductScreenshots::MarketplaceListing
      }
    }
  GRAPHQL

  def screenshots
    data = platform_execute(ScreenshotsQuery, variables: { slug: params[:listing_slug] })

    return render_404 unless data.marketplace_listing

    respond_to do |format|
      format.html do
        render partial: "marketplace_listings/edit/product_screenshots",
               locals: { listing: data.marketplace_listing }
      end
    end
  end

  def preview
    markdown = params[:text]
    context = { base_url: base_url, current_user: current_user }

    html = GitHub.dogstats.time("markdown", tags: ["action:preview"]) do
      GitHub::Goomba::MarkdownPipeline.to_html(markdown, context)
    end

    render html: html
  end

  ManageQuery = parse_query <<-'GRAPHQL'
    query($viewerCanAdmin: Boolean!) {
      marketplaceListings(first: 10, viewerCanAdmin: $viewerCanAdmin) {
        nodes {
          slug
          resourcePath
          viewerCanEdit
        },
        totalCount
      }
      repositoryActions(first: 10, writableOnly: $viewerCanAdmin, filterBy: { state: LISTED }) {
        nodes {
          resourcePath
        }
        totalCount
      }
      ...Views::MarketplaceListings::Manage::Root
    }
  GRAPHQL

  def manage
    data = platform_execute(ManageQuery, variables: { viewerCanAdmin: true })
    listings = data.marketplace_listings
    actions = data.repository_actions

    if (listings.total_count + actions.total_count) > 1
      respond_to do |format|
        format.html do
          render "marketplace_listings/manage", locals: { data: data }
        end
      end
    elsif listings.total_count == 1
      listing = listings.nodes.first

      if listing.viewer_can_edit?
        redirect_to edit_marketplace_listing_path(listing.slug)
      else
        redirect_to listing.resource_path.to_s
      end
    elsif actions.total_count == 1
      action = actions.nodes.first
      redirect_to action.resource_path.to_s
    else
      redirect_to new_marketplace_listing_path
    end
  end

  private

  def marketplace_params
    return {} unless params[:marketplace_listing]
    input = params[:marketplace_listing].
              permit(
                :appID,
                :companyUrl,
                :documentationUrl,
                :extendedDescription,
                :financeEmail,
                :fullDescription,
                :heroCardBackgroundImageDatabaseID,
                :installationUrl,
                :isLightText,
                :marketingEmail,
                :name,
                :oauthApplicationID,
                :pricingUrl,
                :primaryCategoryName,
                :privacyPolicyUrl,
                :secondaryCategoryName,
                :securityEmail,
                :shortDescription,
                :slug,
                :statusUrl,
                :supportUrl,
                :technicalEmail,
                :termsOfServiceUrl,
                :logoBackgroundColor,
                supportedLanguageNames: [],
              )

    if input[:isLightText]
      input[:isLightText] = input[:isLightText] == "true"
    end

    if input[:heroCardBackgroundImageDatabaseID]
      input[:heroCardBackgroundImageDatabaseID] = input[:heroCardBackgroundImageDatabaseID].to_i
    end

    input
  end

  def require_conditional_access_checks?
    action_name != "show"
  end

  def target_for_conditional_access
    if action_name == "create"
      integratable = integratable_for_creation
      return :no_target_for_conditional_access unless integratable
      integratable.owner
    else
      listing = Marketplace::Listing.find_by_slug(params[:listing_slug])
      return :no_target_for_conditional_access unless listing
      listing.owner
    end
  end

  def input_for_creation
    args = marketplace_params
    input = args.slice(:name, :shortDescription, :fullDescription, :privacyPolicyUrl,
                       :supportedLanguageNames, :supportUrl, :primaryCategoryName, :installationUrl)

    if args[:oauthApplicationID]
      input[:oauthApplicationID] = args[:oauthApplicationID].to_i
    elsif args[:appID]
      input[:appID] = args[:appID].to_i
    end

    input
  end

  def integratable_for_creation
    input = input_for_creation

    if input[:oauthApplicationID]
      OauthApplication.find(input[:oauthApplicationID])
    elsif input[:appID]
      Integration.find(input[:appID])
    end
  end

  def find_integratable(type, id)
    if type == OAUTH_APP_TYPE
      OauthApplication.find(id)
    elsif type == GITHUB_APP_TYPE
      Integration.find(id)
    end
  end

  def redirect_to_billing_settings_with_delisted_error(marketplace_listing:, subscription_item:)
    if subscription_item.present?
      flash[:error] = "#{marketplace_listing.name} has been removed from GitHub Marketplace. You can cancel your subscription below."
    else
      flash[:error] = "#{marketplace_listing.name} has been removed from GitHub Marketplace. You can cancel your subscription by navigating to your organization's billing settings."
    end

    redirect_to settings_user_billing_path
  end

  def current_user_cannot_admin_listing?(marketplace_listing)
    !logged_in? || (!marketplace_listing.viewer_is_listing_admin? &&
      !current_user.can_admin_marketplace_listings?)
  end

  def update_redirect_path(listing)
    # Generates the correct url based on where the user is in the edit flow
    if params[:page] == "description"
      return edit_description_marketplace_listing_path(listing.slug)
    end
    if params[:page] == "contact"
      return edit_contact_info_marketplace_listing_path(listing.slug)
    end

    edit_marketplace_listing_path(listing.slug)
  end

  def set_suggested_target_id_cookie
    return unless params[:suggested_target_id].present?

    json_val = JSON.generate(params[:suggested_target_id])
    cookies[:marketplace_suggested_target_id] = { value: json_val, expires: 1.hour }
  end

  def set_repository_ids_cookie
    return unless params[:repository_ids].present?

    json_val = JSON.generate(params[:repository_ids])
    cookies[:marketplace_repository_ids] = { value: json_val, expires: 1.hour }
  end

  def set_setup_state_cookie(marketplace_listing)
    if params[:state] && marketplace_listing.app
      IntegrationInstallation::SetupStateCookie.create(
        cookie_jar: cookies,
        data: {state: params[:state]},
        integration_id: marketplace_listing.app.id,
        target_id: params[:suggested_target_id],
      )
    end
  end

  def new_xhr_query
    if params[:oauth_cursor]
      OauthApplicationsQuery
    elsif params[:app_cursor]
      IntegrationsQuery
    elsif params[:action_cursor]
      ActionsQuery
    end
  end
end
