# frozen_string_literal: true
require "github-launch"

class ChecksController < AbstractRepositoryController
  include ActionsContentPolicy

  areas_of_responsibility :checks
  before_action :require_push_access, except: [:index, :show, :annotations, :checks_state_summary, :check_logs, :step_logs, :live_logs]
  before_action :add_dreamlifter_csp_exceptions, only: [:show, :index]

  ChecksIndexQuery = parse_query <<-'GRAPHQL'
    query($id: ID!, $annotationsLimit: Int!, $after: String) {
      node(id: $id) {
        ...Views::Checks::Show
      }
    }
  GRAPHQL

  around_action :record_index_metrics, only: [:index]
  private def record_index_metrics
    action_start_time = GitHub::Dogstats.monotonic_time

    yield

    GitHub.dogstats.timing_since("checks.index", action_start_time, tags: [
      "mobile:#{!!show_mobile_view?}",
      "logged_in:#{!!logged_in?}",
      "dreamlifter_enabled:#{GitHub.actions_enabled?}",
    ])
  end

  def index
    begin
      commit = current_repository.commits.find(params[:ref])
    rescue GitRPC::ObjectMissing, RepositoryObjectsCollection::InvalidObjectId
      render_404 and return
    end

    @check_suites = current_repository.check_suites.where(head_sha: commit.oid).most_recent

    if params[:check_suite_id] &&
      # Use .detect here not .find so as not to trigger a DB call.
      check_suite = @check_suites.detect { |cs| cs.id == params[:check_suite_id].to_i }
    end

    @default_check_run = if check_suite && !check_suite.latest_check_runs.empty?
      check_suite.latest_check_runs.first
     else
      @check_suites.map(&:latest_check_runs).flatten.first
    end

    if @default_check_run
      variables = { id: @default_check_run.global_relay_id, annotationsLimit: CheckAnnotation::MAX_PER_REQUEST }
      graphql_check_run = platform_execute(ChecksIndexQuery, variables: variables).node
    end

    commit_check_runs = CheckRun.for_sha_and_repository_id(commit.oid, current_repository.id)
    render "checks/show", locals: {
      commit: commit,
      selected_check_run: @default_check_run,
      check_suites: @check_suites,
      blankslate: show_blankslate?(@check_suites, @default_check_run, current_repository),
      workflows_loading: workflows_loading?(@check_suites, @default_check_run),
      graphql_check_run: graphql_check_run,
      commit_check_runs: commit_check_runs,
    }, layout: "repository"
  end

  StreamingLogQuery = parse_query <<-'GRAPHQL'
    query($id: ID!) {
      node(id: $id) {
        ... on CheckRun {
          streamingLog {
            url
          }
        }
      }
    }
  GRAPHQL

  def live_logs
    return render_404 unless logged_in?

    check_run = current_check_run
    return render_404 unless check_run.present?

    graphql_result = platform_execute(StreamingLogQuery, variables: { id: check_run.global_relay_id }).node
    streaming_log = graphql_result.streaming_log
    return render_404 unless streaming_log.present?

    result = GrpcHelper.request_authenticated_url_for(unauthenticated_url: streaming_log.url, check_run: check_run, resource_type: GitHub::Launch::Services::Artifactsexchange::ResourceType::TYPE_STREAMING_LOG)

    if !result.call_succeeded?
      GitHub.dogstats.increment("actions.exchange_url_request.succeeded", tags: ["live_logs"])
      error_message = result.options[:message] || "RPC call failed due to unknown error."
      render json: { success: false, errors: [error_message], data: {}}, status: 500
    else
      GitHub.dogstats.increment("actions.exchange_url_request.failed", tags: ["live_logs"])
      render json: { success: true, errors: [], data: {authenticated_url: result.value.authenticated_url}}, status: :ok
    end
  end

  ChecksLogsQuery = parse_query <<-'GRAPHQL'
    query($id: ID!, $number: Int!) {
      node(id: $id) {
        ... on CheckRun {
          steps (first: 1, number: $number) {
            nodes {
              completedLog {
                url
              }
            }
          }
        }
      }
    }
  GRAPHQL

  def step_logs
    return render_404 unless logged_in?

    check_run = current_check_run
    return render_404 unless request.xhr?
    return render_404 unless check_run.present?

    graphql_check_step = platform_execute(ChecksLogsQuery, variables: { id: check_run.global_relay_id, number: params[:step].to_i }).node
    check_step = graphql_check_step.steps.nodes.first
    return render_404 unless check_step.present?
    return render_404 unless check_step.completed_log.present?

    result = GrpcHelper.request_authenticated_url_for(unauthenticated_url: check_step.completed_log.url, check_run: check_run, resource_type: GitHub::Launch::Services::Artifactsexchange::ResourceType::TYPE_COMPLETED_STEP_LOG)
    if result.call_succeeded?
      GitHub.dogstats.increment("actions.exchange_url_request.succeeded", tags: ["step_logs"])
      redirect_to result.value.authenticated_url, status: 307
    else
      GitHub.dogstats.increment("actions.exchange_url_request.failed", tags: ["step_logs"])
      render status: result.status, plain: "Failed to generate URL to download logs."
    end
  end

  def check_logs
    return render_404 unless logged_in?

    check_run = current_check_run
    return render_404 unless check_run.present?
    return render_404 unless check_run.completed_log_url.present?

    result = GrpcHelper.request_authenticated_url_for(unauthenticated_url: check_run.completed_log_url, check_run: check_run, resource_type: GitHub::Launch::Services::Artifactsexchange::ResourceType::TYPE_COMPLETED_JOB_LOG)
    if result.call_succeeded?
      GitHub.dogstats.increment("actions.exchange_url_request.succeeded", tags: ["check_logs"])
      redirect_to result.value.authenticated_url, status: 307
    else
      GitHub.dogstats.increment("actions.exchange_url_request.failed", tags: ["check_logs"])
      flash[:error] = "Failed to generate URL to download logs."
      redirect_back fallback_location: check_run_path(id: check_run.id)
    end
  end

  ChecksShowQuery = parse_query <<-'GRAPHQL'
    query($id: ID!, $annotationsLimit: Int!, $after: String) {
      node(id: $id) {
        ...Views::Checks::ChecksSummary
      }
    }
  GRAPHQL

  def show
    return redirect_to checks_path(ref: params[:ref]) unless request.xhr?

    begin
      commit = current_repository.commits.find(params[:ref])
    rescue GitRPC::ObjectMissing
      render_404 and return
    end

    commit_check_runs = CheckRun.for_sha_and_repository_id(commit.oid, current_repository.id)
    check_run = commit_check_runs.find_by_id(params[:id])
    return render_404 if check_run.nil?

    pull = current_repository.pull_requests.find_by_id(params[:pull])

    variables = { id: check_run.global_relay_id, annotationsLimit: CheckAnnotation::MAX_PER_REQUEST }

    respond_to do |format|
      format.html do
        graphql_check_run = platform_execute(ChecksShowQuery, variables: variables).node
        render partial: "checks/checks_summary", locals: {
          check_suite: check_run.check_suite,
          check_run: check_run,
          commit: commit,
          pull: pull,
          graphql_check_run: graphql_check_run,
          commit_check_runs: commit_check_runs,
        }
      end
    end
  end

  AnnotationsQuery = parse_query <<-'GRAPHQL'
    query($id: ID!, $annotationsLimit: Int!, $after: String) {
      node(id: $id) {
        ...Views::Checks::ChecksAnnotations
      }
    }
  GRAPHQL

  def annotations
    return render_404 unless request.xhr?

    begin
      commit = current_repository.commits.find(params[:ref])
    rescue GitRPC::ObjectMissing
      render_404 and return
    end

    check_run = CheckRun.for_sha_and_repository_id(commit.oid, current_repository.id).find_by_id(params[:id])
    return render_404 if check_run.nil?

    pull = current_repository.pull_requests.find_by_id(params[:pull])

    variables = { id: check_run.global_relay_id, after: params[:after], annotationsLimit: CheckAnnotation::MAX_PER_REQUEST }
    graphql_check_run = platform_execute(AnnotationsQuery, variables: variables).node

    respond_to do |format|
      format.html do
        render partial: "checks/checks_annotations", locals: {
          pull: pull,
          graphql_check_run: graphql_check_run,
          commit: commit,
        }
      end
    end
  end

  def checks_state_summary
    return render_404 unless request.xhr?

    begin
      commit = current_repository.commits.find(params[:ref])
    rescue GitRPC::ObjectMissing
      render_404 and return
    end

    check_suites = current_repository.check_suites.where(head_sha: commit.oid).most_recent

    respond_to do |format|
      format.html do
        render partial: "checks/checks_state_summary", locals: {
          check_suites: check_suites,
          head_sha: commit.oid,
        }
      end
    end
  end

  private

  def current_check_run
    commit = current_repository.commits.find(params[:ref])
    @check_run ||= CheckRun.for_sha_and_repository_id(commit.oid, current_repository.id).find_by_id(params[:id])
  end

  def show_blankslate?(check_suites, default_check_run, current_repository)
    return false if check_suites.any?
    return false if !default_check_run.nil?
    return false if current_repository.has_apps_that_write_checks?
    true
  end

  def workflows_loading?(check_suites, default_check_run)
    # default_check_run will be nil if no check runs have been created (yet)
    return false if default_check_run

    check_suites.any?(&:actions_app?)
  end

  def require_push_access
    render_404 unless current_user_can_push?
  end
end
