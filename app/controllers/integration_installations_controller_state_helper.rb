# frozen_string_literal: true

module IntegrationInstallationsControllerStateHelper
  def ensure_app_has_not_changed
    return unless current_integration.present? && params[:integration_fingerprint].present?
    if current_integration.fingerprint != params[:integration_fingerprint]
      flash[:warn] = "This App has changed since you last viewed it. Please review and try again."
      return app_changed_since_last_viewed(params[:action])
    end
  end
end
