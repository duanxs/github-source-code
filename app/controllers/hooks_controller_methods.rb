# frozen_string_literal: true

module HooksControllerMethods
  extend ActiveSupport::Concern

  included do
    before_action :sudo_filter, except: :index
    before_action :find_hook, only: [:show, :update, :test, :destroy]
    before_action :ensure_hook_editable, only: [:update, :test]

    helper_method :current_context
  end

  def index
    @hooks_view = Hooks::IndexView.new parent: current_context, current_user: current_user
    if params[:statuses]
      @hooks_view.load_hook_statuses
      respond_to do |format|
        format.html do
          render partial: "hooks/hooks_bucket", locals: {hooks_view: @hooks_view}
        end
      end
    else
      render "hooks/index", locals: {hooks_view: @hooks_view}
    end
  end

  def show
    render_template_view "hooks/show", Hooks::ShowView, hook: @hook
  end

  def new
    @hook = current_context.hooks.new name: hook_type, active: true, events: default_events
    @hook_view = Hooks::ShowView.new hook: @hook, current_user: current_user
    render "hooks/new", locals: {hook: @hook, hook_view: @hook_view}
  end

  def create
    @hook = Hook.new name: hook_type, active: true, installation_target: current_context
    @hook.track_creator(current_user)
    @hook.set_default_events if @hook.legacy_service?

    if @hook.update(hook_params(@hook))
      flash[:notice] = hook_created_notification(@hook)
      if @hook.legacy_service?
        redirect_to installations_path(current_context)
      else
        redirect_to hooks_path(current_context)
      end
    else
      @hook_view = Hooks::ShowView.new hook: @hook, current_user: current_user

      flash.now[:error] = "There was an error setting up your hook: #{@hook.errors.full_messages.to_sentence}"
      render "hooks/new", locals: {hook_view: @hook_view, hook: @hook}
    end
  end

  def update
    params = hook_params(@hook)
    if params["partial_config"]
      @hook.update_existing_config(params["partial_config"])
      @hook.active = params["active"]
    else
      @hook.assign_attributes(params)
    end

    if @hook.save
      flash[:notice] = "Okay, the hook was successfully updated."
      redirect_to hook_path(@hook)
    else
      flash.now[:error] = "There was an error updating your hook: #{@hook.errors.full_messages.to_sentence}"
      render_template_view "hooks/show", Hooks::ShowView, hook: @hook
    end
  end

  def destroy
    @hook.destroy

    flash[:notice] = "Okay, that hook was successfully deleted."
    if @hook.legacy_service?
      redirect_to installations_path(current_context)
    else
      redirect_to hooks_path(current_context)
    end
  end

  def test
    test_payload = Hook::TestPayload.new(@hook)
    if test_payload.deliver
      head :created
    else
      render json: {errors: test_payload.errors.to_sentence}, status: :unprocessable_entity
    end
  end

  def update_pre_receive
    hook_id = params[:id]
    enforcement = params[:enforcement] # GitHub::PreReceiveHookEntry::ENABLED or GitHub::PreReceiveHookEntry::DISABLED
    final = params[:final].present?
    hookable_type = current_context.is_a?(Repository) ? "Repository" : "User"

    target = PreReceiveHookTarget.where(hook_id: hook_id, hookable_type: hookable_type, hookable_id: current_context.id).first
    if target
      target.update(enforcement: enforcement, final: final)
    else
      hook = PreReceiveHook.find_by_id(hook_id)
      target = hook.targets.create(enforcement: enforcement, hookable_type: hookable_type, hookable_id: current_context.id, final: final)
    end

    unless current_context.is_a?(Repository)
      final_message = final ? "and enforced on all repositories" : "and configurable on the repository level"
    end

    if target.enabled?
      flash[:notice] = "Hook is now enabled #{final_message}"
    else
      flash[:notice] = "Hook is now disabled #{final_message}"
    end

    redirect_to hooks_path(current_context)
  end

  private

  def default_events
    %w(push)
  end

  def hook_type
    if params[:service] && Hook::Service.list.key?(params[:service].to_sym) && service_hooks_supported?
      params[:service]
    else
      "web"
    end
  end

  def service_hooks_supported?
    current_context.is_a?(Repository)
  end

  def hook_params(hook)
    return @hook_params unless @hook_params.nil?

    valid_fields = [:active, :url, :content_type, :insecure_ssl, :secret, events: []]
    if hook.legacy_service? && params[:hook]&.key?(:partial_config)
      valid_fields << { partial_config: [*hook.schema_field_names] }
    end

    @hook_params = params.require(:hook).permit(*valid_fields)
  end

  def find_hook
    @hook ||= current_context.hooks.find_by_id(params[:id]).tap do |found_hook|
      raise ApplicationController::NotFound unless found_hook
    end
  end

  def ensure_hook_editable
    raise ApplicationController::NotFound unless @hook.editable_by?(current_user)
  end

  def hook_created_notification(hook)
    notice = "Okay, that hook was successfully created.".dup
    notice << " We sent a ping payload to test it out! Read more about it at #{GitHub.developer_help_url}/webhooks/#ping-event." if hook.webhook?
    notice
  end

  # Private: Returns the correct parent depending on if we're working
  # with org or repo hooks.
  #
  # Returns an Organization or a Repository
  def current_context
    raise NotImplementedError
  end
end
