# frozen_string_literal: true

module IssueUpdate
  include IssuesHelper
  include ViewModelHelper

  def issue_update_payload(item)
    payload = {
      issue_title: item.title,
      source: item.body,
      body: item.body_html,
      newBodyVersion: item.body_version,
      editUrl: show_comment_edit_history_path(item.global_relay_id),
    }

    if item.pull_request?
      payload[:page_title] = pull_request_page_title(item)
      payload[:default_squash_commit_title] = item.pull_request.default_squash_commit_title
    else
      payload[:page_title] = issue_page_title(item)
    end

    if item.respond_to?(:labels)
      payload[:labels] = render_to_string(
        partial: "issues/labels",
        object: item.labels,
        formats: [:html],
      )
    end

    payload
  end
end
