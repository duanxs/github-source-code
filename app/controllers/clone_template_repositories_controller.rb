# frozen_string_literal: true

class CloneTemplateRepositoriesController < AbstractRepositoryController
  areas_of_responsibility :repositories

  before_action :login_required

  def new
    render_new_view
  end

  CloneTemplateRepositoryQuery = parse_query <<~'GRAPHQL'
    mutation($input: CloneTemplateRepositoryInput!) {
      cloneTemplateRepository(input: $input) {
        repository {
          resourcePath
        }
      }
    }
  GRAPHQL

  LookupOwnerQuery = parse_query <<~'GRAPHQL'
    query($login: String!) {
      user(login: $login) {
        id
      }
      organization(login: $login) {
        id
      }
    }
  GRAPHQL

  def create
    data = platform_execute(LookupOwnerQuery, variables: { login: params[:owner] })
    owner = data.user || data.organization
    return render_404 unless owner

    data = platform_execute(CloneTemplateRepositoryQuery, variables: {
      input: {
        repositoryId: current_repository.global_relay_id,
        name: repo_params[:name],
        ownerId: owner.id,
        description: repo_params[:description],
        visibility: repo_params[:visibility].upcase,
        includeAllBranches: params[:include_all_branches] == "1",
      },
    })

    if data.errors.any?
      error_type = data.errors.details[:cloneTemplateRepository]&.first["type"]
      return render_404 if %w[NOT_FOUND SERVICE_UNAVAILABLE].include?(error_type)

      flash[:error] = data.errors.messages.values.join(", ")
      return render_new_view
    end

    new_repo = data.clone_template_repository.repository
    redirect_to new_repo.resource_path.to_s
  end

  private

  def repo_params
    @repo_params ||= params.require(:new_repository).permit(:name, :description, :visibility)
  end

  LookupTemplateRepoQuery = parse_query <<~'GRAPHQL'
    query($owner: String!, $name: String!) {
      repository(owner: $owner, name: $name) {
        isPrivate
        owner {
          databaseId
        }
        ...Views::CloneTemplateRepositories::New::Repository
      }
    }
  GRAPHQL

  def render_new_view
    data = platform_execute(LookupTemplateRepoQuery, variables: {
      owner: params[:user_id],
      name: params[:repository],
    })

    template_repo = data.repository

    owner = current_user
    if template_repo.is_private? && template_repo&.owner.is_a?(PlatformTypes::Organization)
      owner = Organization.find(template_repo.owner.database_id)
    end

    is_private = template_repo.is_private? || current_user.private_repo_by_default?
    new_repository = Repository.new(private: is_private, owner: owner)
    view = Repositories::CreateView.new(current_user, current_repository)

    render "clone_template_repositories/new", locals: {
      template_repo: template_repo,
      new_repository: new_repository,
      owner: owner,
      view: view,
    }
  end
end
