# frozen_string_literal: true

class TopicsController < ApplicationController
  areas_of_responsibility :explore

  statsd_tag_actions only: [:index, :show]

  before_action :add_csp_exceptions, only: [:show]
  before_action :redirect_to_normalized_topic_name, only: [:show]

  layout "site"

  CSP_EXCEPTIONS = {
    frame_src: ["www.youtube.com"],
    img_src: [
      ExploreFeed::Event::FEED_URL,
      ExploreFeed::Spotlight::SPOTLIGHTS_FEED_URL,
    ],
  }

  include RepositoryControllerMethods

  def index
    locals = {
      featured_topics_sample: Topic.not_flagged.curated.with_logo.featured_and_shuffled,
      popular_topics: Topic
        .not_flagged
        .popular_on_public_repositories(Topic::NUMBER_OF_POPULAR_TOPICS),
      topic_results: Topic
        .featured_and_sorted_alphabetically
        .paginate(per_page: Topic.per_page, page: params.fetch(:page, 1)),
    }

    if request.xhr?
      render partial: "topics/featured_topics", locals: locals
    else
      render "topics/index", locals: locals
    end
  end

  def show
    topic_name = params[:topic_name]
    topic = Topic.find_or_build_by_name(topic_name)

    if topic.blank? || topic.flagged?
      render_404
    else
      events = ExploreFeed::Event.all.upcoming_or_current.for_topic(topic.or_alias_source.name)
      spotlights = ExploreFeed::Spotlight.all.current.for_topic(topic.or_alias_source.name)
      marketplace_listings = Marketplace::Listing.with_category(topic_name).verified_and_shuffled
      marketplace_category = Marketplace::Category.for_slug(topic_name).first
      repository_results = repository_query.execute

      locals = {
        sort: params[:s] || "",
        direction: params[:o] || "desc",
        language: params[:l].presence,
        marketplace_listings: marketplace_listings,
        marketplace_category: marketplace_category,
        repository_results: repository_results,
        topic: topic,
        spotlights: spotlights,
        events: events,
      }

      if request.xhr?
        if topic_feed_enabled?
          render partial: "topics/feed/paginated_content", locals: locals
        else
          render partial: "topics/paginated_repositories", locals: locals
        end
      else
        if topic_feed_enabled?
          render "topics/feed/show", locals: locals
        else
          render "topics/show", locals: locals
        end
      end
    end
  end

  RelatedQuery = parse_query <<-'GRAPHQL'
    query($name: String!) {
      topic(name: $name) {
        isFlagged
      }
      ...Views::Topics::Related::Root
    }
  GRAPHQL

  def related
    respond_to do |format|
      format.html do
        data = platform_execute(RelatedQuery, variables: { name: params[:topic] })
        return render_404 if data.topic.blank? || data.topic.is_flagged?
        render partial: "topics/related", locals: { data: data }
      end
    end
  end

  AutocompleteQuery = parse_query <<-'GRAPHQL'
    query($name: String!, $owner: String!, $query: String) {
      repository(name: $name, owner: $owner) {
        ...Views::Topics::Autocomplete::Repository
      }
    }
  GRAPHQL

  def autocomplete
    return head :not_acceptable unless logged_in?

    data = platform_execute(AutocompleteQuery,
      variables: {owner: params[:user_id], name: params[:repository], query: params[:q]},
    )

    return head :not_found if data.errors.any?

    respond_to do |format|
      format.html_fragment do
        render partial: "topics/autocomplete", formats: :html, locals: { repository: data.repository }
      end
    end
  end

  private

  def repository_query
    Search::Queries::RepoQuery.new(
      current_user: current_user,
      user_session: user_session,
      remote_ip: request.remote_ip,
      aggregations: true,
      phrase: repository_query_string,
      page: params.fetch(:page, 1),
      per_page: Repository.per_page,
    )
  end

  def repository_query_string
    query_bits = ["topic:#{params[:topic_name]} fork:true is:public"]
    valid_sort_param = params[:s].present? && params[:s].split.size == 1
    valid_order_param = %w(asc desc).include?(params[:o])
    if valid_sort_param && valid_order_param
      query_bits << "sort:#{params[:s]}-#{params[:o]}"
    elsif valid_sort_param
      query_bits << "sort:#{params[:s]}"
    end
    query_bits << "language:#{params[:l]}" if params[:l].present?
    query_bits << params[:q] if params[:q].present?
    query_bits.join(" ")
  end

  def parsed_issues_query
    [[:is, "open"], [:is, "issue"]]
  end
  helper_method :parsed_issues_query

  def redirect_to_normalized_topic_name
    if Topic.normalize(params[:topic_name]) != params[:topic_name]
      redirect_to("/topics/#{Topic.normalize(params[:topic_name])}", status: 301)
    end
  end
end
