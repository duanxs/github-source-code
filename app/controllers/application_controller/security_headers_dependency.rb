# frozen_string_literal: true

#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#                       Seriously, CC @github/prodsec and @github/dotcom-security
#                       if you need to touch this file
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
module ApplicationController::SecurityHeadersDependency
  extend ActiveSupport::Concern

  included do
    after_action :set_html_safe
    helper_method :html_safe_nonce
  end

  private

    # Overrides default CSP with the preview policy if enabled for current_user
    #
    # Returns nothing.
    def set_security_headers
      additions = Hash.new { |h, key| h[key] = [] }

      if preview_features?
        additions[:report_uri] = [GitHub.browser_errors_url]
      end

      if GitHub::CSP::Policy.dynamic_img_sources.any?
        additions[:img_src] = GitHub::CSP::Policy.dynamic_img_sources
      end

      if GitHub::CSP::Policy.dynamic_connect_src.any?
        additions[:connect_src] = GitHub::CSP::Policy.dynamic_connect_src
      end

      if additions.any?
        SecureHeaders.append_content_security_policy_directives(request, additions)
      end
    end

    def html_safe_nonce
      csrf_token = Base64.strict_encode64(real_csrf_token(session))
      OpenSSL::Digest::SHA1.hexdigest("html-safe-nonce:#{csrf_token}")
    end

    def set_html_safe
      if request.xhr? && response.media_type == "text/html" && response_body.all?(&:html_safe?)
        response.headers["X-HTML-Safe"] = html_safe_nonce
      end
    end

    # Static file policy for static error pages
    #
    # This policy is used in conjuction with policies defined in `meta` tags
    # within individual static files. These files could be served either with or
    # without the CSP header added here via the Rails stack.
    #
    # A policy set in the CSP header can only be identical or restricted further
    # in `meta` tags. Changes to this policy may also need to be reflected in the
    # individual static files. Static files can be automatically updated by running
    # the `script/update_static_csp` script.
    #
    # For now, per the discussion in https://github.com/github/github/pull/37758,
    # a connect-src of `none` cannot be used because it will break safe browsing in
    # Chrome on iOS. This should be restricted further in the meta tags for pages
    # not requiring a connect-src once this bug is resolved.
    #
    SecureHeaders::Configuration.override(:static_file_policy) do |config|
      config.csp = {
        default_src: [SecureHeaders::CSP::NONE],
        script_src: [SecureHeaders::CSP::SELF],
        style_src: [SecureHeaders::CSP::UNSAFE_INLINE],
        img_src: [SecureHeaders::CSP::SELF, SecureHeaders::CSP::DATA_PROTOCOL],
        connect_src: [SecureHeaders::CSP::SELF],
        form_action: [SecureHeaders::CSP::SELF],
        base_uri: [SecureHeaders::CSP::SELF],
      }
    end

    # Package registry policy for package indices and access endpoints
    #
    # This policy is used by endpoints of the package registry. They are only
    # accessed via CLI tooling so they do not need to be as permissive.
    # Additionally, some package managers (looking at you apt) have hard limits
    # on the header value length.
    SecureHeaders::Configuration.override(:package_registry_policy) do |config|
      config.csp = {
        default_src: [SecureHeaders::CSP::NONE],
        script_src: [SecureHeaders::CSP::NONE],
        base_uri: [SecureHeaders::CSP::SELF],
        form_action: [SecureHeaders::CSP::NONE],
        frame_ancestors: [SecureHeaders::CSP::NONE],
        block_all_mixed_content: true,
        sandbox: true,
      }
    end

    # OctoCaptcha policy for displaying FunCaptcha as an iframe on dotcom
    #
    SecureHeaders::Configuration.override(:octocaptcha_policy) do |config|
      script_src = ["https://api.funcaptcha.com", "https://api.arkoselabs.com", "https://cdn.arkoselabs.com", SecureHeaders::CSP::UNSAFE_EVAL]
      if Rails.env.development? || Rails.env.test?
        script_src += ["http://assets.github.localhost", "http://github.localhost"]
      else
        script_src += GitHub::CSP::Policy::SCRIPT_SOURCES
      end

      frame_ancestors = [GitHub.host_name, "*.#{GitHub.host_name}", "*.githubapp.com"]
      frame_ancestors = ["*"] if Rails.env.development?

      config.csp = {
        frame_src: ["https://api.funcaptcha.com", "https://api.arkoselabs.com"],
        default_src: [SecureHeaders::CSP::NONE],
        connect_src: ["https://api.funcaptcha.com", "https://api.arkoselabs.com"],
        style_src: GitHub::CSP::Policy::STYLE_SOURCES,
        script_src: script_src,
        base_uri: [SecureHeaders::CSP::SELF],
        form_action: [SecureHeaders::CSP::NONE],
        frame_ancestors: frame_ancestors,
        block_all_mixed_content: true,
        plugin_types: [],
      }

      config.x_frame_options = SecureHeaders::XFrameOptions::ALLOW_ALL
    end

    # Set Content-Security-Policy header for static files
    #
    # Returns nothing.
    def set_static_file_csp
      SecureHeaders.use_secure_headers_override(request, :static_file_policy)
    end

    # Set Content-Security-Policy header for package registry
    #
    # Returns nothing.
    def set_package_registry_csp
      SecureHeaders.use_secure_headers_override(request, :package_registry_policy)
    end

    # Set Content-Security-Policy header for octocaptcha
    #
    # Returns nothing.
    def set_octocaptcha_csp
      SecureHeaders.use_secure_headers_override(request, :octocaptcha_policy)
    end

    # Set headers telling the browser not to cache this response.
    #
    # Returns nothing.
    def set_cache_control_no_store
      headers["Cache-Control"] = "no-cache, no-store"
    end

    # Configuration override to send the Clear-Site-Data header.
    SecureHeaders::Configuration.override(:clear_browser_cache) do |config|
      config.clear_site_data = [
        SecureHeaders::ClearSiteData::CACHE,
      ]
    end

    # Clears the browser's cache for browsers supporting the Clear-Site-Data
    # header.
    #
    # Returns nothing.
    def clear_browser_cache
      unless parsed_useragent.name == "Chrome"
        SecureHeaders.use_secure_headers_override(request, :clear_browser_cache)
      end
    end

    # Configuration override to opt-out of setting Referrer-Policy header
    SecureHeaders::Configuration.override(:disable_referrer_policy) do |config|
      config.referrer_policy = SecureHeaders::OPT_OUT
    end

    # Opts out of setting the Referrer-Policy header so that default browser
    # behavior occurs (i.e. referrer is sent if linking from https:/github.com
    # to another HTTPS site but not a HTTP site).
    #
    # Returns nothing.
    def disable_referrer_policy
      SecureHeaders.use_secure_headers_override(
        request, :disable_referrer_policy
      )
    end
end
