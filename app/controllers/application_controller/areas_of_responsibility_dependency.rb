# frozen_string_literal: true

# Assigns all requests that hit a given controller to the specified areas of
# responsibility (defined in docs/areas-of-responsibility.md).
module ApplicationController::AreasOfResponsibilityDependency
  extend ActiveSupport::Concern
  include GitHub::AreasOfResponsibility

  def areas_of_responsibility_filter
    env[GitHub::TaggingHelper::PROCESS_AOR_KEY] = areas_of_responsibility
    Audit.context.push(areas_of_responsibility: areas_of_responsibility.map(&:to_s))
    GitHub.context.push(areas_of_responsibility: areas_of_responsibility, codeowners: self.class.codeowners)
    Failbot.push(areas_of_responsibility: areas_of_responsibility, codeowners: self.class.codeowners.to_a)
  end
end
