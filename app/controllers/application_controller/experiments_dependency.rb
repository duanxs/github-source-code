# frozen_string_literal: true

module ApplicationController::ExperimentsDependency
  extend ActiveSupport::Concern

  include ExperimentHelper

  included do
    helper_method :three_ctas_empty_state_for_org_repos_enabled?
    helper_method :repository_import_contextual_help_experiment
  end

  # This dependency houses experiment methods that will be available as controller/helper/filter methods
  # Methods here should deal with specific experiments
  # Any abstractions or shareable methods should be added to ExperimentHelper

  def remove_add_members_interstitial_enabled?
    optimizely_experiment_enabled?(
      experiment: "remove_add_members_interstitial",
      actor: current_user,
      enabled_variations: ["alternative"]
    )
  end

  def three_ctas_empty_state_for_org_repos_enabled?
    return @three_ctas_empty_state_for_org_repos_enabled if defined?(@three_ctas_empty_state_for_org_repos_enabled)
    experiment = GitHub.optimizely.experiment.with_actor(name: :three_ctas_empty_state_for_org_repos, actor: current_user)
    experiment.activate_subject!
    @three_ctas_empty_state_for_org_repos_enabled = experiment.in_variant?(:treatment)
  end

  def repository_import_contextual_help_experiment
    return @repository_import_contextual_help_experiment if defined?(@repository_import_contextual_help_experiment)
    @repository_import_contextual_help_experiment = GitHub.optimizely.experiment.with_actor(
      name: :repository_import_contextual_help,
      actor: current_user
    )
  end

  def track_repository_import_attempted
    GitHub.optimizely.track(
      "submit.repository_imports.create",
      current_user.analytics_tracking_id
    )
  end
end
