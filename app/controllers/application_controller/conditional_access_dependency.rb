# frozen_string_literal: true

# Conditional access controller concern.
#
# Shares functionality used by features that provide conditional access
# to resources, such as external identity session enforcement or IP allow list
# enforcement.
module ApplicationController::ConditionalAccessDependency
  extend ActiveSupport::Concern

  include ::ConditionalAccess::Enforcer
  include ApplicationController::TwoFactorAuthnPolicy

  included do
    helper_method :unauthorized_organization_ids
  end

  # Public: The Organization or Business in the context of the endpoint that
  # is responsible for determining conditional access to the resource.
  #
  # Returns an Organization, Business, or nil.
  def target_for_conditional_access
    message = <<~MSG.squish
      The target_for_conditional_access method must be implemented in
      #{self.class}. This method should return the `User`, `Organization` or `Business`
      that controls access to the resource, otherwise `:no_target_for_conditional_access`.
      The method should never fail opened by ending with return :no_target_for_conditional_access, nor
      use patterns like `nil || :no_target_for_conditional_access`
      Instead, the implementation should explicitly call out the cases where there is no target.
      The contract of this method is governed by safe_target_for_conditional_access.
    MSG
    exception = NotImplementedError.new message
    exception.set_backtrace(caller)

    if Rails.env.production?
      Failbot.report!(exception)
    else
      raise exception
    end
  end

  # Public: Controller action filter to enforce whether any conditional access
  # checks that apply to this request are satisfied.
  #
  # Returns nothing.
  def perform_conditional_access_checks
    # See app/controllers/application_controller/ip_whitelisting_dependency.rb
    if require_whitelisted_ip? && !ip_whitelisting_policy_satisfied?
      return enforce_whitelisted_ip
    end

    # Invokes lib/conditional_access/enforcer.rb
    target = safe_target_for_conditional_access
    if (target != :no_target_for_conditional_access) && GitHub.flipper[:enforce_cap_through_web].enabled?(target)
      failed_policy = enforce_conditional_access_policies
        if failed_policy != :ok
          GitHub::Logger.log(
            fn: "#{self.class.name}#{action_name}",
            class: self.class.name,
            service: "cap",
            failed_policy: failed_policy,
            user_id: current_user&.id,
            url: request.original_url,
          )
        end
        return unless failed_policy == :ok
    end

    # See app/controllers/application_controller/external_sessions_dependency.rb
    require_active_external_identity_session if require_active_external_identity_session?
  end

  # Public: Determine if all conditional access checks are required.
  #
  # By default, this returns true. Override with custom logic to conditionally
  # protect actions. For example, you may only want to enforce conditional
  # access for private repository data and allow all access to public repository
  # data.
  #
  # If instead, you want to skip access checks for a specific feature, implement
  # the equivalent method for the feature in your controller (e.g.
  # `#require_whitelisted_ip?` and `#require_active_external_identity_session?`).
  #
  # Returns Boolean.
  def require_conditional_access_checks?
    true
  end

  # Are all conditional access checks satisfied?
  #
  # Returns Boolean.
  def conditional_access_checks_satisfied?
    external_identity_session_fresh? && ip_whitelisting_policy_satisfied?
  end

  # Public: The IDs of orgs where the user does not meet a conditional access
  # check.
  #
  # Returns Array of Integer.
  def unauthorized_organization_ids
    return @unauthorized_organization_ids if defined? @unauthorized_organization_ids

    @unauthorized_organization_ids = (
      unauthorized_saml_organization_ids +
      unauthorized_ip_whitelisting_organization_ids
    ).compact.uniq
  end

  private

  # policies enabled through Conditional Access Policies framework (CAP)
  # will be enforced through enforce_conditional_access_policies method
  def conditional_access_policies
    [:two_factor]
  end

  # safe_target_for_conditional_access is the gatekeeper for target_for_conditional_access.
  # This method makes sure that target_for_conditional_access complies with the contract.
  #
  # The contract is:
  # - return the owner of the resource, so long it's one of the accepted types [User, Organization, Business]
  # - indicate the circumstances in which there is no target for enforcement by returning :no_target_for_conditional_access
  #
  # this method should never be overridden by includers
  def safe_target_for_conditional_access
    target = target_for_conditional_access
    raise ArgumentError.new("nil is not a valid target_for_conditional_access return value, only User/Organization/Business accepted") if target.nil?

    return target if target == :no_target_for_conditional_access
    return target if target.is_a?(Business) || target.is_a?(Organization)
    return :no_target_for_conditional_access if target.is_a?(User) && (target.user? || target.bot? || target.mannequin?)

    raise ArgumentError.new("unexpected type #{target.class.name} returned by target_for_conditional_access")
  end
end
