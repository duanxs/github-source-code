# frozen_string_literal: true

module ApplicationController::DiscussionsDependency
  extend ActiveSupport::Concern

  included do
    helper_method :show_discussions?
  end

  # Public: Should Discussions-related features be available? Checks both
  # feature flag enablement and a current repository's admin setting.
  #
  # Returns a boolean.
  def show_discussions?
    return @show_discussions if defined?(@show_discussions)
    @show_discussions = current_repository&.show_discussions?
  end
end
