# frozen_string_literal: true

module ApplicationController::PjaxDependency
  extend ActiveSupport::Concern

  included do
    helper_method :pjax_version
    helper_method :pjax?
  end

  # PJAX Asset Version.
  #
  # The client receives the initial version in the first requests
  # layout. Addition PJAX requests return the value in the
  # X-PJAX-Version response header. Any change in the version will
  # cause the pjax request to do a full load of the next page.
  #
  # Returns a MD5 hex String.
  def pjax_version
    @pjax_version ||= compute_pjax_version
  end

  def compute_pjax_version
    csp_policy_str = SecureHeaders.config_for(request).generate_headers["Content-Security-Policy"]

    assets = AssetBundles.get
    parts = [
      "3",
      csp_policy_str,
      *[
        "frameworks.css",
        "github.css",
        "frameworks.js",
        "github.js",
      ].map { |bundle|
        filename = assets.expand_bundle_name(bundle)
        assets.integrity(filename)
      },
    ].compact
    Digest::MD5.hexdigest(parts.join(":"))
  end

  # Enforces the pjax layout at all costs for pjax requests.
  # HACK: this is horrible and @github/josh can tell you why.
  def _layout_for_option(name)
    if name == :default && pjax? && !mobile?
      "layouts/pjax"
    else
      super
    end
  end

  # Always set Vary: X-PJAX
  #
  # Ensures PJAX requests are always cached seperately from their full response
  # counterparts.
  def set_vary_pjax
    response.headers["Vary"] = "X-PJAX"
  end

  # Remove _pjax from params to prevent urls from being generated
  # with _pjax=true. We rely on the X-PJAX header so it doesn't
  # matter.
  def strip_pjax_params
    return unless params.delete(:_pjax)

    # Now bust em request caches
    request.env["QUERY_STRING"] = request.env["QUERY_STRING"]
      .sub(/(^|&)_pjax=[^&]+/, "").sub(/^&/, "")

    request.env.delete("REQUEST_URI")
    request.env.delete("rack.request.query_string")
    request.env.delete("rack.request.query_hash")
    # Rails 2.x
    request.env.delete("action_controller.request.query_parameters")
    # Rails 3.x
    request.env.delete("action_dispatch.request.query_parameters")

    request.instance_variable_set("@original_fullpath", nil)
    request.instance_variable_set("@fullpath", nil)
  end

  # Automatically the X-PJAX-Version on all PJAX responses.
  def set_pjax_version
    if pjax?
      response.headers["X-PJAX-Version"] = pjax_version
    end
  end

  # Set the request url as a response header to pjax. Yes, this sounds dumb
  # but XHR can't figure out which request url a response came from after it
  # follows a redirect.
  def set_pjax_url
    if pjax?
      response.headers["X-PJAX-URL"] = request.url
    end
  end

  # Determines whether a request was made via pjax.
  # Can be used in views.
  #
  # Returns a boolean.
  def pjax?
    request.headers["X-PJAX"]
  end

  # Detect cross-repo pjax redirects and rewrite them to make sure that they
  # trigger a hard refresh in browsers, therefore skipping pjax.
  def sanitize_pjax_redirects
    if 302 == response.response_code && "#js-repo-pjax-container" == request.headers["X-PJAX-Container"]
      # extract "name/owner" pairs from origin/destination URLs
      name_owner_pairs = [request.url, response.location].map { |u| u.to_s.split("/")[3..4].join("/").downcase }
      unless name_owner_pairs.inject(:==)
        response.status = 200
        response.body = ""
        response.headers["X-PJAX-URL"] = response.location
      end
    end
  end
end
