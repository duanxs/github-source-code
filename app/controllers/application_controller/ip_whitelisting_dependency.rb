# frozen_string_literal: true

# IP whitelisting controller concern
module ApplicationController::IpWhitelistingDependency
  extend ActiveSupport::Concern

  included do
    helper_method :unauthorized_ip_whitelisting_organization_ids
    helper_method :unauthorized_ip_whitelisting_targets
  end

  # Public: Enforce whether originating IP address is valid given a possible IP
  # whitelisting policy in place.
  #
  # Returns nothing.
  def enforce_whitelisted_ip
    return if ip_whitelisting_policy_satisfied?

    if request.xhr?
      head :forbidden
    else
      render_forbidden_by_ip_whitelisting_policy
    end
  end

  # Public: Return a 403 Forbidden response with the IP allow list forbidden
  # view when a request is forbidden due to IP allow list enforcement.
  #
  # target - The Business or Organization that is the target for the conditional
  #   access check. Defaults to the object returned by safe_target_for_conditional_access.
  #
  # Returns Boolean.
  def render_forbidden_by_ip_whitelisting_policy(target: safe_target_for_conditional_access)
    if target == :no_target_for_conditional_access
      raise ArgumentError.new("unable to render IP allow list forbidden view due to :no_target_for_conditional_access")
    end
    render "ip_whitelisting/forbidden",
      locals: { target: target, ip: request.remote_ip },
      layout: "application",
      status: :forbidden
    true
  end

  # Public: Is any IP allow list policy in place satisfied for the request?
  #
  # Returns Boolean.
  def ip_whitelisting_policy_satisfied?(owner: safe_target_for_conditional_access, actor: nil)
    return true if owner == :no_target_for_conditional_access
    return true if owner.nil?

    actor ||= current_user
    policy = IpWhitelistingPolicy.new owner: owner, ip: request.remote_ip, actor: actor
    satisfied = policy.satisfied?

    unless satisfied
      # Set log fields to indicate IP allow list enforcement
      log_data.merge!({
        ip_allow_list_policy_unsatisfied: true,
        ip_allow_list_policy_owner_id: owner.id,
        ip_allow_list_policy_owner_type: owner.is_a?(::Business) ? :BUSINESS : :ORG,
        ip_allow_list_policy_actor: actor.to_s,
        ip_allow_list_policy_actor_type: actor.class.name,
        ip_allow_list_policy_actor_bot: actor.try(:bot?),
      })
    end

    satisfied
  end

  # Public: Guard for `#enforce_whitelisted_ip` to determine if enforcement is
  # required.
  #
  # By default, this returns true in environments where IP allow lists are
  # available. Override with custom logic to conditionally protect actions.
  # For example, you may only want to enforce the filter for private repository
  # data and allow all access to public repository data.
  #
  # Returns true if an allowed IP is required.
  def require_whitelisted_ip?
    GitHub.ip_whitelisting_available?
  end

  # Public: Get the IDs of all organizations that the user can't access from
  # the current request's IP address due to IP allow list enforcement.
  #
  # Returns Array of Integer.
  def unauthorized_ip_whitelisting_organization_ids
    ip_whitelisting_for_current_user.protected_organization_ids
  end

  # Public: Finds all protected IP allow list targets, where a target is an
  # `Organization` or `Business`, that the user can't access from the current
  # request's IP address.
  #
  # Returns Array of targets (target being an `Organization` or `Business`).
  def unauthorized_ip_whitelisting_targets
    ip_whitelisting_for_current_user.protected_targets
  end

  def ip_whitelisting_for_current_user
    @ip_whitelisting_for_current_user ||= Platform::Authorization::IpWhitelisting.new \
      user: current_user, ip: request.remote_ip
  end
end
