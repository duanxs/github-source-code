# frozen_string_literal: true

require "rack/octotracer"
require "github/tagging_helper"

module ApplicationController::StatsDependency
  extend ActiveSupport::Concern

  included do
    helper_method :staff_bar_enabled?
    helper_method :stats_ui_enabled?
    helper_method :render_graphql_query_trace?
    helper_method :render_mysql_query_trace?
    helper_method :browser_stats_enabled?
    helper_method :dogstats_request_tags
  end

  protected
    # Public: Check if staff bar should be displayed
    #
    # Returns Boolean.
    def staff_bar_enabled?
      real_user_site_admin? || employee?
    end

    # Public: Check if staff stats should be collected and displayed.
    #
    # Use regular site-admin/employee check rather than the true check so
    # disabling site-admin will turn off stats as well.
    #
    # Returns Boolean.
    def stats_ui_enabled?
      (real_user_site_admin? || employee?) && GitHub.stats_ui_enabled?
    end

    # Check to see if we should render detailed GraphQL query information in the
    # page for debugging purposes.
    #
    # Returns Boolean
    def render_graphql_query_trace?
      return false unless stats_ui_enabled?
      params[:graphql_query_trace] && Platform::GlobalScope.queries.any?
    end

    # Check to see if we should render detailed Mysql query information in the
    # page for debugging purposes.
    #
    # Returns Boolean
    def render_mysql_query_trace?
      return false unless stats_ui_enabled?
      params[:mysql_query_trace]
    end

    # Public: Check if browser stats should be collected
    def browser_stats_enabled?
      GitHub.browser_stats_enabled?
    end

    # Public: Dogstats tags for the current request
    #
    # Use these tags for your GitHub.dogstats calls. We do not add them automatically
    # because of the limitations of DataDog tag values.
    #
    # Returns Array of Strings
    def dogstats_request_tags
      unless defined?(@dogstats_request_tags)
        @dogstats_request_tags = [
          "controller:#{self.class.name.underscore}".freeze,
          "action:#{action_name}".freeze,
        ]
      end

      @dogstats_request_tags.dup
    end

    # Public: Setter for the current request wide statsd sampling rate
    #
    # This sets the sampling rate we use when emitting metrics via statsd.  The
    # default should be fine in most cases, but high traffic controllers or
    # actions may want to set the sampling rate to something fairly small, like
    # 0.01 or 0.001.  This expects the rate to be a float between 1.0 and 0.0.
    class_methods do
      def set_statsd_sample_rate(rate, options = {})
        before_action(options) do
          request.env[GitHub::TaggingHelper::STATSD_SAMPLE_RATE] = rate
        end
      end

      def statsd_tag_actions(options = {})
        before_action(options) do
          request.env[GitHub::TaggingHelper::STATSD_TAG_ACTIONS] = true
        end
      end
    end

    # Public: Getting for current request wide statsd sampling rate
    #
    # This returns the request side statsd sampling rate.  This is intended to
    # be used by individual controllers to get the sampling rate they should use
    # for ad-hoc metrics reporting.
    def statsd_sample_rate
      request.env[GitHub::TaggingHelper::STATSD_SAMPLE_RATE]
    end

    # Instruments rails templates and models for staffbar-enabled requests
    def staff_rails_instrumentation
      # Check the stats cookie directly, since we run before the before_action(:set_site_admin_and_employee_status)
      return yield unless stats_ui_enabled?
      return yield unless defined?(ActionView::Template.template_trace)

      begin
        ActionView::Template.template_trace_reset
        ActionView::Template.template_trace_enabled = true
        yield
      ensure
        ActionView::Template.template_trace_enabled = false
      end
    end

    # Instruments rails templates. Currently used by IssuesController#show
    def track_and_report_render_view_time
      return yield unless defined?(ActionView::Template.template_trace)

      begin
        ActionView::Template.template_trace_reset
        ActionView::Template.template_trace_enabled = true

        yield

        total_render_time = ActionView::Template.template_trace.total_time

        event_tags = ["controller:#{controller_name}", "action:#{action_name}"]
        GitHub.dogstats.distribution("request.render.time", (total_render_time * 1000).round(3), tags: event_tags)
      ensure
        ActionView::Template.template_trace_enabled = false
      end
    end

    # Collects MySQL query info for staff.
    def staff_mysql_instrumentation
      return yield unless stats_ui_enabled?

      QueryCacheLogSubscriber.track = true
      GitHub::MysqlInstrumenter.with_track do
        yield
      end
    ensure
      QueryCacheLogSubscriber.track = false
    end

    # Collects GraphQL query info and reports it to DataDog
    # This only reports generic query info (query_time and query_count) during the given controller action.
    # This does not track detailed info for individual queries
    def track_and_report_graphql_executions
      before_query_count = Platform::GlobalScope.query_count
      before_query_time = Platform::GlobalScope.query_time

      yield

      after_query_count = Platform::GlobalScope.query_count
      after_query_time = Platform::GlobalScope.query_time

      event_tags = ["controller:#{controller_name}", "action:#{action_name}"]

      total_query_time = after_query_time - before_query_time
      GitHub.dogstats.distribution("request.graphql.execution.time", (total_query_time * 1000).round(3), tags: event_tags)

      total_query_count = after_query_count - before_query_count
      GitHub.dogstats.distribution("request.graphql.execution.count", total_query_count, tags: event_tags)
    end

    # Collects MySQL executions info and reports it to DataDog
    # This only reports generic SQL executions (query_time and query_count) during the given controller action.
    # This does not track detailed SQL info for individual query execution.
    def track_and_report_mysql_executions
      before_query_count = GitHub::MysqlInstrumenter.query_count
      before_query_time = GitHub::MysqlInstrumenter.query_time

      yield

      after_query_count = GitHub::MysqlInstrumenter.query_count
      after_query_time = GitHub::MysqlInstrumenter.query_time

      event_tags = ["controller:#{controller_name}", "action:#{action_name}"]

      total_query_time = after_query_time - before_query_time
      GitHub.dogstats.distribution("request.mysql.execution.time", (total_query_time * 1000).round(3), tags: event_tags)

      total_query_count = after_query_count - before_query_count
      GitHub.dogstats.distribution("request.mysql.execution.count", total_query_count, tags: event_tags)
    end

    # Collects GitRPC/Redis/MySQL query info for staff.
    def staff_external_service_profiler
      return yield unless stats_ui_enabled?
      GitRPCLogSubscriber.track = true
      Redis::Client.track       = true
      yield
    ensure
      GitRPCLogSubscriber.track = false
      Redis::Client.track       = false
    end

    def trace_web_request
      Rack::OctoTracer.get_span(request.env).when_enabled do |span|
        span.operation_name = "#{request.request_method} #{params[:controller]}/#{params[:action]}"
        span.set_tag(GitHub::TaggingHelper::COMPONENT_TAG, "rails")
        span.set_tag(GitHub::TaggingHelper::CONTROLLER_TAG, GitHub::TaggingHelper.controller(request.env))
        span.set_tag(GitHub::TaggingHelper::ACTION_TAG, params[:action])
        span.set_tag(GitHub::TaggingHelper::CATEGORY_TAG, GitHub::TaggingHelper.category(request.env))
        span.set_tag(GitHub::TaggingHelper::RAILS_VERSION_TAG, GitHub::TaggingHelper.rails_version)
        span.set_tag(GitHub::TaggingHelper::LOGGED_IN_TAG, GitHub::TaggingHelper.logged_in(request.env))
        span.set_tag(GitHub::TaggingHelper::ROBOT_TAG, GitHub::TaggingHelper.robot_name(request.env))
        span.set_tag(GitHub::TaggingHelper::PJAX_TAG, GitHub::TaggingHelper.pjax(request.env))
      end
    end

    # Record user-specific request context, which we can't easily do in middleware
    def record_stats
      return yield unless logged_in?

      before = Time.now
      yield
      duration = Time.now - before

      # We have to add these tags after the action has been executed, as FlipperSubscriber
      # observes the feature flags that are checked during the processing of the request.
      feature_tags = FlipperSubscriber.tested_features.map { |feature, is_enabled| "#{feature}_enabled:#{!!is_enabled}" }

      # This metric is specifically for tracking timing with and without
      # features enabled down to the controller level.
      #
      # Notes:
      # - This overrides the default `host` tag, since that's not useful for
      #   feature level tracking.
      # - This removes `_controller` from the end of the tag, to better match
      #   what other `request.*` metrics use.

      request_feature_tags = [
        "controller:" + self.class.name.underscore.gsub(/_controller/, ""),
        "site_admin:#{site_admin?}",
        "host:untagged",
      ] + feature_tags

      GitHub.dogstats.timing("request.feature.time", duration * 1000, tags: request_feature_tags)
    end
end
