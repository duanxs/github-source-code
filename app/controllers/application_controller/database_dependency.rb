# frozen_string_literal: true

module ApplicationController::DatabaseDependency
  protected

  def select_write_database
    last_operations = DatabaseSelector::LastOperations.from_session(session)
    ActiveRecord::Base.connected_to(role: :writing) do
      yield
      last_operations.update_last_write_timestamp
    end
  end
end
