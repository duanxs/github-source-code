# frozen_string_literal: true

# External Identity Session controller concern
#
# See Also
#
#   ExternalIdentity model        - app/models/external_identity.rb
#   ExternalIdentitySession model - app/models/external_identity_session.rb
#   UserSession model             - app/models/user_session.rb
#
module ApplicationController::ExternalSessionsDependency
  extend ActiveSupport::Concern

  # Public: Returns the SamlEnforcementPolicy for the current
  # request. This is set during the SSO enforcement filter if the route and
  # organization are enforced.
  attr_accessor :current_saml_enforcement_policy

  included do
    helper_method :current_saml_enforcement_policy
    helper_method :current_external_identity_sessions
    helper_method :authorized_saml_organizations
    helper_method :unauthorized_saml_organization_ids
    helper_method :unauthorized_saml_targets
    helper_method :current_external_identity_session
  end

  # Public: Checks if the required external identity session is present for
  # the current organization or business.
  #
  # If the organization or business does not require an external identity session,
  # short circuits out of the check.
  #
  # An organization requires an active external identity session when SAML SSO
  # is enabled. When enforced, every member must have an external identity and
  # session at all times. When enabled (without enforcement), only members
  # with a registered external identity must have a SAML session at all times.
  #
  # A business always requires an active external identity session when SAML SSO
  # is enabled.  Business SAML providers do not have an enforced flag.
  #
  # Example usage:
  #
  #     if !required_external_identity_session_present?
  #       render_external_identity_session_required
  #       return # abort access
  #     end
  #
  # Example scenarios:
  #
  #     required_external_identity_session_present?(target: nil)
  #     #=> true because no session is required (no organization to enforce)
  #
  #     required_external_identity_session_present?(target: non_saml_org)
  #     #=> true because no session is required (organization doesn't require SAML sessions)
  #     # authorization is enforced via organization membership checks (abilities)
  #
  #     logged_in? #=> false (anonymous)
  #     required_external_identity_session_present?(target: saml_org)
  #     #=> true because user must be logged in (no sessions, but also no org membership access)
  #     # authorization is enforced via organization membership checks (abilities)
  #
  #     saml_org.member?(current_user) #=> false (user isn't a member)
  #     required_external_identity_session_present?(target: saml_org)
  #     #=> true because user must be a member of the organization
  #     # authorization is enforced via organization membership checks (abilities)
  #
  #     saml_org.member?(current_user) #=> true
  #     # user does not have an active SAML session for the organization
  #     required_external_identity_session_present?(target: saml_org)
  #     #=> false, access must be aborted and the user prompted to SSO
  #
  #     saml_org.member?(current_user) #=> true
  #     # user has an active SAML session for the organization
  #     required_external_identity_session_present?(target: saml_org)
  #     #=> true, user meets criteria
  #
  #     saml_business.member?(current_user) #=> true
  #     required_external_identity_session_present?(target: saml_business)
  #     #=> true, SAML authentication is required when business SAML SSO is enabled
  #
  #     saml_business.member?(current_user) #=> false
  #     required_external_identity_session_present?(target: saml_business)
  #     #=> true, SAML authentication is required when business SAML SSO is enabled
  #
  # Returns false if the required active SAML session for the organization or business
  # is not present, or true if it is present or if no session is required.
  def required_external_identity_session_present?(target: safe_target_for_conditional_access)
    return true if target == :no_target_for_conditional_access

    self.current_saml_enforcement_policy = saml_enforcement_policy_for(target)
    return true unless current_saml_enforcement_policy&.enforced?

    target = target&.external_identity_session_owner
    identity = current_external_identity(target: target)

    GitHub.context.push external_identity: identity
    Audit.context.push external_identity: identity

    identity.present?
  end

  # Returns an Array of ExternalIdentitySession records for this
  # UserSession.
  def current_external_identity_sessions
    return [] unless logged_in? && user_session.present?

    @current_external_identity_sessions ||= user_session.
      external_identity_sessions.active.
      includes(external_identity: {provider: :target}).to_a
  end

  # Returns an Array of ExternalIdentity records for this
  # UserSession.
  def current_external_identities
    current_external_identity_sessions.map(&:external_identity).compact
  end

  def saml_for_user_session
    @saml_for_user_session ||= Platform::Authorization::SAML.new(session: user_session)
  end

  # Public: Finds all protected SAML SSO Organizations that the user currently has an
  # active external identity session for.
  def authorized_saml_organizations
    saml_for_user_session.authorized_organizations
  end

  def unauthorized_saml_organization_ids
    saml_for_user_session.protected_organization_ids
  end

  # Public: Finds all protected SAML SSO targets (Organizations, Businesses) that the user currently lacks an
  # active external identity session for.
  def unauthorized_saml_targets
    saml_for_user_session.protected_targets
  end

  # Public: Returns the currently active ExternalIdentity
  # for the given authentication target, or nil otherwise.
  def current_external_identity(target: safe_target_for_conditional_access)
    return nil if target == :no_target_for_conditional_access
    target = target&.external_identity_session_owner
    return nil unless target.present?

    current_external_identities.detect do |identity|
      identity.provider_type == target.saml_provider.class.name &&
      identity.provider_id == target.saml_provider.id
    end
  end

  # Public: Returns the currently active ExternalIdentitySession
  # for the given authentication target, or nil otherwise.
  def current_external_identity_session(target: safe_target_for_conditional_access)
    return nil if target == :no_target_for_conditional_access
    target = target&.external_identity_session_owner
    return nil unless target.present?

    current_external_identity_sessions.detect do |identity_session|
      identity_session.target == target
    end
  end

  # Public: Action filter to enforce required external identity sessions.
  #
  # Returns nothing.
  def require_active_external_identity_session
    return unless GitHub.external_identity_session_enforcement_enabled?
    return true if signed_token_authed? # Signed auth token. No session.
    return true if external_identity_session_fresh?

    if request.xhr?
      head :unauthorized
    else
      render_external_identity_session_required
    end
  end

  # Public: Returns whether the external session for the request is "fresh" or
  # it we should prompt for SAML authentication. External sessions have a hard
  # cut-off expiration. And, sometimes this expiration occurs at an inconvenient
  # time. For example, it is generally more convenient to refresh your external
  # session during a page navigation than it is while posting a comment. So, as
  # expiration nears, we try to determine if it is a good time to refresh their
  # session.
  #
  # Returns true if we should prompt for SAML re-authentication or false
  # otherwise.
  def external_identity_session_fresh?
    # If their SAML session isn't present we need to prompt them
    return false unless required_external_identity_session_present?
    # Because required_external_identity_session_present? is true, we know the user
    # either has a valid external session or they are trying to access a resource that
    # doesn't require an external session. If current_external_identity_session.nil? is
    # true, we know it is the latter and can return early.
    return true if current_external_identity_session.nil?

    session_expiration = current_external_identity_session.expires_at
    session_duration = session_expiration - current_external_identity_session.updated_at

    # Most identity providers expire sessions after 24 hours. But, some
    # providers might expire session in as little as 1 hour. So, we vary the
    # refresh duration based on the length of the session.
    early_refresh_duration = if session_duration <= 2.hours
      5.minutes
    elsif session_duration <= 9.hours
      30.minutes
    else
      1.hour
    end

    eligible = session_expiration < early_refresh_duration.from_now
    if eligible && request.get? && !request.xhr?
      # We should have them refresh their external session.
      false
    else
      # We can let them continue using their currently active external session.
      true
    end
  end

  # Public: Guard to determine if an external identity session is required.
  #
  # By default, this is always on. Override with custom logic to conditionally
  # protect endpoints. For example, you may only want to enforce the filter for
  # private repository data and allow all access to public repository data.
  #
  # Returns true if an external identity session is required.
  def require_active_external_identity_session?
    true
  end

  # Public: Renders the SAML SSO prompt page, blocking access to protected
  # resources.
  #
  # Returns nothing.
  def render_external_identity_session_required(target: safe_target_for_conditional_access)
    if target == :no_target_for_conditional_access
      raise ArgumentError.new("unable to render external identity session required: unexpected :no_target_for_conditional_access")
    end
    target = target.external_identity_session_owner
    allow_external_redirect_after_post(provider: target.saml_provider)

    options = {}
    options[:return_to] = request.url if request.get? #defaults to org page downstream

    case target
    when ::Organization
      GlobalInstrumenter.instrument("sso_page.view", { user: current_user, org: target })

      render_template_view "orgs/identity_management/sso",
        Orgs::IdentityManagement::SingleSignOnView,
        {
          form_data: captured_form_data_via_enforcement,
          organization: target,
          initiate_sso_url: org_idm_saml_initiate_url(target, options),
        },
        layout: "session_authentication"
    when ::Business
      GlobalInstrumenter.instrument("sso_page.view", { user: current_user, business: target })

      render_template_view "businesses/identity_management/sso",
        Businesses::IdentityManagement::SingleSignOnView,
        {
          form_data: captured_form_data_via_enforcement,
          business: target,
          initiate_sso_url: idm_saml_initiate_enterprise_url(target, options),
        },
        layout: "session_authentication"
    end
  end

  # Private: Creates an active ExternalIdentitySession for the current_user
  # tied to the providsioned external identity.
  #
  # If a session already exists, it is updated with the given expiry.
  #
  # external_identity - The ExternalIdentity provisioned during SAML SSO.
  # expires_at - A DateTime specifying when the session should expire
  #
  # Returns an ExternalIdentitySession
  def update_or_create_external_identity_session(external_identity, expires_at:)
    if session = user_session.external_identity_sessions.by_identity(external_identity).active.first
      session.update(expires_at: expires_at)
      session
    else
      user_session.external_identity_sessions.create \
        external_identity: external_identity,
        expires_at: expires_at
    end
  end

  # Private: Find the correct saml enforcement policy for the given authentication target
  #
  # Returns either a Business::SamlEnforcementPolicy or Organization::SamlEnforcementPolicy
  def saml_enforcement_policy_for(target)
    return unless target&.respond_to?(:external_identity_session_owner)

    provider_owner = target.external_identity_session_owner
    case provider_owner
    when ::Organization
      Organization::SamlEnforcementPolicy.new(organization: provider_owner, user: current_user)
    when ::Business
      organization = target.is_a?(::Organization) ? target : nil
      Business::SamlEnforcementPolicy.new(business: provider_owner, organization: organization, user: current_user)
    end
  end

  def captured_form_data_via_enforcement
    return nil if request.get?

    @captured_form_data ||= request.parameters.dup.tap do |p|
      p["_target"] = request.url

      p.delete "action"
      p.delete "controller"
    end
  end

  def saml_provider
    target = safe_target_for_conditional_access
    return nil if target == :no_target_for_conditional_access
    target = target.external_identity_session_owner
    target.try(:saml_provider)
  end

  def allow_external_redirect_after_post(provider: saml_provider)
    return unless provider.present?
    uri = Addressable::URI.parse(provider.sso_url)

    SecureHeaders.append_content_security_policy_directives(
      request,
      form_action: [uri.origin],
    )
  end
end
