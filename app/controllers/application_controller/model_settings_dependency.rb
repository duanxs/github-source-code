# frozen_string_literal: true

# Settings in here deal with things that use
# user facing model Configuration settings ("Advanced Settings")
# Other parts are in lib/github/config
module ApplicationController::ModelSettingsDependency
  extend ActiveSupport::Concern

  included do
    helper_method :user_can_create_organizations?
  end

  # Determines whether this user sees links for creating
  # organizations. Enterprise only.
  def user_can_create_organizations?
    return true if GitHub.enterprise_first_run? && GitHub.user_can_create_organizations?
    return false unless logged_in?
    !current_user.spammy? &&
      (current_user.site_admin? || GitHub.user_can_create_organizations?)
  end
end
