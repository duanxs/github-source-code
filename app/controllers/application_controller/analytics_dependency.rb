# frozen_string_literal: true

module ApplicationController::AnalyticsDependency

  # Define an analytics event, similar to how flash messages are displayed.
  # Lives in harmony with view-side Javascript reporting tools. It's mapped
  # heavily off of Google Analytics Event API: http://bit.ly/Jj1Z8p
  #
  # event - Hash describing the event.
  #   :category - String describing the category of event. Required.
  #   :action   - String describing what happened. Required.
  #   :label    - String labeling the event. Title of the content, etc.
  #   :value    - Number describing a quantatitive value. Time duration, etc.
  #   :redirect - Is this event happening before a redirect? Otherwise it's
  #               assumed the event is happening in the same request that will
  #               render the result.
  #
  # Returns nothing.
  def analytics_event(event = {})
    event[:category] ||= nil
    event[:action] ||= nil
    event[:label] = stringify_ga_label(event[:label])
    event[:value] ||= nil
    event[:redirect] ||= false

    raise "Category is required" if event[:category].blank?
    raise "Action is required" if event[:action].blank?

    flash_store = if event[:redirect]
      flash
    else
      flash.now
    end

    flash_store[:analytics_events] = [] unless flash_store[:analytics_events].kind_of?(Array)
    flash_store[:analytics_events] << event.stringify_keys
  end

  PLAN_TO_PRODUCT = {
    "pro" => { name: "Developer", position: 1 },
    "business" => { name: "Team", position: 2 },
    "business_plus" => { name: "Business", position: 3 },

  }

  # Track a Google Analytics ecommerce purchase event
  #
  # target - User object on whom we're tracking the purchase event.
  # revenue - Numeric revenue in dollars.
  # context - String description context. e.g. "Signup", "Upgrade"
  #
  # Returns nothing.
  def analytics_ec_purchase(target, revenue, context)
    product = PLAN_TO_PRODUCT[target.plan.name]
    id = "#{target.id}-#{Time.now.to_i}"
    revenue = Billing::Money.new(revenue * 100) unless revenue.is_a?(Money)

    if product && revenue > 0
      flash[:analytics_ec_payload] = [
        ["ec:addProduct", product],
        ["ec:setAction", "purchase", {
          "id" => id,
          "revenue" => revenue.format(symbol: false),
          "list" => context,
        }],
      ]
    end
  end

  def set_analytics_dimension(name:, value:, redirect: false)
    dimension = {"name" => name, "value" => value}

    if redirect
      flash[:analytics_dimension] = dimension
    else
      flash.now[:analytics_dimension] = dimension
    end
  end

  # Set a flash which would later on be used in view-side JavaScript to
  # override the URL we send to Google Analytics. Common uses including
  # masking private data and preventing high-cardinality dimensions.
  #
  # url - String of modified URL.
  #
  # Returns nothing.
  def override_analytics_location(url)
    flash.now[:analytics_location] = url
  end

  # Tell JavaScript to strip any parameters from the URL we report to Google
  # Analytics.
  #
  # Returns nothing.
  def strip_analytics_query_string
    flash.now[:analytics_location_query_strip] = "true"
  end

  private

  # Stringify hash object into key/value pairs for event labels. Turns
  # `{:target => "User", :billing => false}` into `target:User; billing:false`.
  #
  # Returns a String.
  def stringify_ga_label(label_content)
    return label_content.presence unless label_content.is_a?(Hash)
    label_content.map { |key, value| "#{key}:#{value}" }.join("; ")
  end
end
