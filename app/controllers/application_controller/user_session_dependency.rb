# frozen_string_literal: true

# User Session controller concern
#
# See Also
#
#   UserSession model - app/models/user_session.rb
#
module ApplicationController::UserSessionDependency
  extend ActiveSupport::Concern

  included do
    helper_method :serving_gist3_standalone?
    helper_method :user_session
    helper_method :session_impersonated?
  end

  # Our SameSite cookies make use of the cookie prefixes spec. As a result,
  # these cookie names have awkward prefixes. So, we define constants to make
  # using them less awkward to reference in code.
  USER_SESSION_COOKIE_SAME_SITE = :"__Host-user_session_same_site"
  GIST_USER_SESSION_COOKIE_SAME_SITE = :"__Host-gist_user_session_same_site"

  protected
    # Public: Provides `authentication_methods` hook for authenticating via
    # user_session.
    #
    # Returns User or nil.
    def login_from_user_session
      if user_session
        user_session.user
      end
    end

    # Are we serving a Gist3 Standalone request?
    # aka gist3.github.com or gist3.github.dev
    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def serving_gist3_standalone?
      @serving_gist3 ||= if GitHub.gist3_domain?
        request.host == GitHub.gist3_host_name
      else
        false
      end
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

    # Overrides default user_session method to lookup
    # the session using the gist specific session cookie and stored hashed key value.
    # The cookie and hashed keys are initially written during the Oauth callback.
    def user_session
      if serving_gist3_standalone?
        gist_user_session
      else
        dotcom_user_session
      end
    end

    # Public: Lookup session record for authenticated user by their
    # `user_session` cookie.
    #
    # Returns a UserSession model or nil.
    def dotcom_user_session
      return @user_session if defined? @user_session

      if stateless_request?
        @user_session = nil
      elsif result = UserSession.authenticate(cookies[:user_session])
        @user_session, key = result
        @same_site_cookie_request = @user_session &&
          SecurityUtils.secure_compare(
            cookies[:user_session].to_s,
            cookies[USER_SESSION_COOKIE_SAME_SITE].to_s,
          )

        touch_user_session_and_device(@user_session)
        # Pass current key to touch the same cookie
        set_session_cookie(@user_session, key)
        if GitHub.subdomain_private_mode_enabled?
          # Auth and retouch the private mode session if necessary
          touch_authenticated_private_mode_user_session @user_session
        end
      end

      @user_session
    end

    def gist_user_session
      return @user_session if defined? @user_session

      if result = UserSession.authenticate(cookies[:gist_user_session], hashed_key_column: :hashed_gist_key)
        @user_session, key = result
        @same_site_cookie_request = @user_session &&
          SecurityUtils.secure_compare(
            cookies[:gist_user_session].to_s,
            cookies[GIST_USER_SESSION_COOKIE_SAME_SITE].to_s,
          )
        touch_user_session_and_device(@user_session)
        # Pass current key to touch the same cookie
        set_session_cookie(@user_session, key, cookie_name: :gist_user_session)

        @user_session
      else
        @user_session = nil
      end
    end

    def user_session_cookies_delete
      if serving_gist3_standalone?
        cookies.delete(:gist_user_session)
        cookies.delete(
          GIST_USER_SESSION_COOKIE_SAME_SITE,
          secure: true,
          domain: nil,
        ) if GitHub.same_site_cookie_enabled?
      else
        cookies.delete(:user_session)
        cookies.delete(
          USER_SESSION_COOKIE_SAME_SITE,
          secure: true,
          domain: nil,
        ) if GitHub.same_site_cookie_enabled?
      end
    end

    # Public: Checks if, during user session lookup, a matching strict SameSite
    # session cookie was submitted along with the normal user session cookie. If
    # a browser doesn't support SameSite cookies it will always submit the
    # strict SameSite cookie, allowing us to be backward compatible. If a
    # browser does support SameSite cookies, we can use the presence/absence of
    # the strict SameSite cookie to determine if this was a same origin request
    # or not (useful for additional CSRF validation).
    #
    # Returns true if this request included our strict SameSite cookie and
    # false otherwise.
    def same_site_cookie_request?
      return true unless GitHub.same_site_cookie_verification_enabled?
      # SameSite cookies are not used for anonymous cookie based sessions.
      return true unless user_session
      !!@same_site_cookie_request
    end

    # Public: Checks if current session is actually a staff user impersonating
    # someone elses session.
    #
    # Returns true or false.
    def session_impersonated?
      user_session && user_session.impersonated?
    end

    # Public: Login user.
    #
    # Creates a new session record, sets the user_session cookie as well as
    # any legacy session cookies. current_user is updated to reflect the user.
    #
    # user - The User
    # sign_in_verification_method: required, reason this sign in is allowed
    #   (can be nil for for password change events)
    # client: the source of the login_user event. This is also called when a
    #   user changes their password or impersonates another user
    # authenticated_device: optional, the device used during sign in or nil
    #
    # Returns user.
    def login_user(user, sign_in_verification_method:, authenticated_device: nil, client: :web)
      tags = ["client:#{client}", "sign_in_verification_method:#{sign_in_verification_method}"]
      GitHub.dogstats.time("login_user_duration", tags: tags) do
        raise TypeError, "can’t login nil user" unless user
        log_data[:user] = user.login

        if user.sign_in_analysis_enabled?
          authenticated_device ||= user.authenticated_devices.find_by_device_id(current_device_id)
          trust_user_device(authenticated_device) if sign_in_verification_method
        end

        # Always reset the cookie store session when logging in a user
        persistent_reset_session

        # If theres already a session, kill it before we switch. The old
        # session would be lost to the aether.
        user_session.revoke(:switched_users) if user_session

        # Generate new key pair for new session
        key, hashed_key = UserSession.random_key_pair
        private_mode_key, hashed_private_mode_key = UserSession.random_key_pair
        @user_session = user.sessions.build(
          hashed_key: hashed_key,
          hashed_private_mode_key: hashed_private_mode_key,
        )

        # ensure device-tracking values are set as cookie objects

        @user_session.assign_attributes_from_request(request)
        @user_session.save
        log_data[:user_session_id] = @user_session.id

        # enqueue job to prune older sessions when over the limit
        EnforceActiveUserSessionLimitJob.perform_later(user) if user.enforce_active_session_limit?

        # Set user session cookie since we created a new one
        set_session_cookie(@user_session, key)

        # Re-set cookies so secure_headers will "Upgrade" cookies set by client to pick up samesite attribute
        cookies[:tz] = cookies.delete(:tz)

        if GitHub.subdomain_private_mode_enabled?
          touch_subdomain_private_mode_user_session(@user_session, private_mode_key)
        end

        self.current_user = user
        authentication_record = establish_history(user, @user_session, authenticated_device, client)

        unless client == :password_changed
          GitHub.dogstats.time("log_login_duration", tags: tags) do
            log_login(sign_in_verification_method, authentication_record)
          end
        end
      end

      self.current_user
    end

    # Internal: create an authentication record for this event
    #
    # Returns the record that was created or nil
    def establish_history(user, user_session, authenticated_device, client)
      GitHub.dogstats.time("establish_history_duration", tags: ["client:#{client}"]) do
        return unless user.sign_in_analysis_enabled?
        return if user_session.impersonated?
        return unless cookies[:_octo] # sign ins will always have this value

        # 2FA sign ins can technically not be associated to a device if the user
        # mucks with the cookies between requests.
        unless client == :web
          return unless authenticated_device
        end

        history_exists = client == :user_session && ActiveRecord::Base.connected_to(role: :reading) do
          user_session.authentication_records.where(ip_address: request.remote_ip).exists?
        end

        return unless !history_exists && !AuthenticationRecord.user_rate_limited?(user)

        tags = [
          "action:establish_history",
          "client:#{client}",
        ]

        begin
          ActiveRecord::Base.connected_to(role: :writing) {
            AuthenticationRecord.create!(
              user: user,
              octolytics_id: cookies[:_octo],
              client: client,
              user_agent: user_session.user_agent,
              user_session: user_session,
              authenticated_device: authenticated_device,
              ip_address: request.remote_ip,
              location: user_session.location,
            )
          }.tap do |authentication_record|
            if client == :web
              tags << "authentication_record_persisted:#{authentication_record&.persisted?}"
            end
            GitHub.dogstats.increment("user_session_auth_records", tags: tags)
          end
        rescue ActiveRecord::RecordInvalid => e
          GitHub.dogstats.increment("user_session_auth_records", tags:
            tags + ["error:#{e.class.name.dasherize}"]
          )
          nil
        end
      end
    end

    # Public: Log user out.
    #
    # Resets session, wipes any user_session cookie and revokes the session
    # state from the database. current_user is reset to nil.
    #
    # reason - Why user is being logged out.
    #          See UserSession:VALID_REVOKE_REASONS for valid keys.
    #
    # Returns nothing.
    def logout_user(reason = :logout)
      user_session_cookies_delete

      if user_session
        sign_in_record = user_session.sign_in_record
        user_session.revoke(reason)
        @user_session = nil

        GitHub.context.push(spamurai_form_signals: spamurai_form_signals)
        GlobalInstrumenter.instrument "user.logout", {
          actor: current_user,
          known_device: sign_in_record&.known_device?,
          known_location: sign_in_record&.known_location?,
        }
      end

      self.current_user = nil
    end

    # Public: Replaces the current user's session with an impersonated session
    # of the target user.
    #
    # user - The User
    #
    # Returns user.
    def impersonated_login_user(user)
      # current user becomes the impersonator
      unless former_user_session = user_session
        raise ArgumentError, "no user to set as impersonator"
      end

      # Generate new key pair for impersonated session
      key, hashed_key = UserSession.random_key_pair
      @user_session = user.sessions.build(
        hashed_key: hashed_key,
        impersonator_session: former_user_session,
      )
      @user_session.assign_attributes_from_request(request)
      @user_session.save!

      # Set user session cookie since we created a new one
      set_session_cookie(@user_session, key)

      self.current_user = user
    end

    # Public: Restore original staff users session and kill impersonated
    # session.
    #
    # Returns former User.
    def impersonated_logout_user
      unless impersonated_user_session = user_session
        raise ArgumentError, "no impersonator user to log out"
      end

      unless former_user_session = impersonated_user_session.impersonator_session
        raise ArgumentError, "no former user to log back in"
      end

      impersonated_user_session.revoke(:unimpersonate)

      @user_session = former_user_session

      # Generate a new key pair for the users old session
      # Kinda weird, but we don't actually know their old unhashed key
      # anymore. Just generate a new one.
      key, hashed_key = UserSession.random_key_pair
      user_session.hashed_key = hashed_key
      user_session.save!

      # Touch user session to set new user_session cookie
      set_session_cookie(@user_session, key)

      self.current_user = former_user_session.user
    end

    # A basic heuristic that tries to answer "do we think this was a human
    # doing things on dotcom excluding AJAX." This includes same site,
    # browser_full/pjax, non-gist requests. We also exclude some noisy requests
    # that make it through this filter by opting-out via @dont_touch_session
    def touch_session?
      # ensure the request category is set before consulting it. The session
      # itself will be memoized in case the order of operations changes.
      request_categorization_filter

      allow_session_touching? &&
        !serving_gist3_standalone? &&
        request.format.try(:html?) &&
        full_navigation_request_category? &&
        same_site_cookie_request?
    end

    # Internal: Trusts the user's device. This is the first time in the
    # login flow that we can fully trust the user has been authenticated
    # and thus their device can now be trusted. We don't verify devices
    # for users that bypass verification due to bounced+unverified emails
    #
    # authenticated_device - the current device
    def trust_user_device(authenticated_device)
      return if session_impersonated?

      unless authenticated_device&.verify!
        GitHub.dogstats.increment("authenticated_device", tags: ["action:verification", "error:attempted_to_trust_nil_device_during_login"])
      end
    end

    # Internal: Touch user_session to note that it has been accessed.
    #
    # Should only be called once per request.
    #
    # user_session - The UserSession
    #
    # Returns nothing.
    def touch_user_session_and_device(user_session)
      raise TypeError, "can’t touch nil user session" unless user_session
      if touch_session?
        if user_session.access(request) && current_user.sign_in_analysis_enabled?
          authenticated_device = current_user.authenticated_devices.find_by_device_id(current_device_id)

          establish_history(current_user, user_session, authenticated_device, :user_session)
          if authenticated_device
            authenticated_device.throttled_touch
          else
            GitHub.dogstats.increment("authenticated_device", tags: ["action:touch", "error:unpersisted_device"])
          end
        end
      end
    end

    # Internal: Set initial session cookie or push current expiry out further
    #
    # user_session - The UserSession
    # key          - Unhashed key String
    # cookie_name  - The cookie to store the user session signed key (optional).
    #
    # Returns nothing.
    def set_session_cookie(user_session, key, cookie_name: :user_session)
      session_cookie = {
        value: key,
        expires: user_session.expire_time,
      }

      if GitHub.dynamic_lab?
        session_cookie[:domain] = GitHub.dynamic_lab_cookie_domain(request.host)
      end
      cookies[cookie_name] = session_cookie

      if GitHub.same_site_cookie_enabled?
        same_site_session_cookie = session_cookie.dup
        same_site_session_cookie.delete(:domain)
        same_site_session_cookie[:same_site] = "Strict"
        cookies["__Host-#{cookie_name}_same_site"] = same_site_session_cookie
      end

      nil
    end

    # Logging a user in with `ApplicationController#login_user` resets the
    # cookie session. However, there are some values stored in the session that
    # we would like to persist after login.
    #
    # return_to                     - The location to redirect the user after the SAML SSO
    #                                 consume action. If this does not persist then features like testing
    #                                 organization SAML provider settings will not work and
    #                                 in some cases users will not be redirected to the
    #                                 correct location after authenticating via SAML SSO.
    # sso_invitation_token          - Represents an organization invitation ID in the
    #                                 database. If this does not persist then we end up with orphaned
    #                                 invitations after a user signs up via SAML SSO.
    # unlinked_external_identity_id - The database ID of an external identity
    #                                 that is not currently linked to a GitHub user account.
    #                                 If this does not persist then a user will
    #                                 be unable to sign up after authenticating
    #                                 via SAML SSO.
    # unlinked_session_expires_at   - The DateTime value representing the
    #                                 expiry of the current user's external
    #                                 identity session. If this does not
    #                                 persist then the user will be required to
    #                                 constantly re-authenticate with SAML SSO.
    PERSISTENT_COOKIE_SESSION_KEYS = [
      :return_to,
      :sso_invitation_token,
      :unlinked_external_identity_id,
      :unlinked_session_expires_at,
      :skip_payment_collection_for_trial,
      ::CompromisedPassword::WEAK_PASSWORD_KEY,
      FlipperSession.session_key,
    ].freeze

    # Internal: reset the user's cookie session, but persists any keys/values in
    # the new session if they are whitelisetd in PERSISTENT_COOKIE_SESSION_KEYS.
    #
    # Returns the new session.
    def persistent_reset_session
      old_session = {}
      PERSISTENT_COOKIE_SESSION_KEYS.each do |key|
        old_session[key] = session[key]
      end

      reset_session

      old_session.each do |key, value|
        session[key] = value
      end

      session
    end

    if GitHub.subdomain_private_mode_enabled?
      # Internal: Touch the the current private_mode_user_session if its
      # authenticated.
      #
      # Should be invoked anytime the main user_session is touched to ensure the
      # subdomain cookie's expiry is pushed into the future as the user accesses
      # the site.
      #
      # user_session - The UserSession of the logged in user.
      #
      # Returns nothing.
      def touch_authenticated_private_mode_user_session(user_session)
        if result = UserSession.authenticate_private_mode(cookies[:private_mode_user_session])
          touch_subdomain_private_mode_user_session(*result)
        elsif user_session
          private_mode_key, hashed_private_mode_key = UserSession.random_key_pair
          user_session.hashed_private_mode_key = hashed_private_mode_key
          ActiveRecord::Base.connected_to(role: :writing) do
            user_session.save!
          end
          touch_subdomain_private_mode_user_session(user_session, private_mode_key)
        end
      end

      # Internal: Set subdomain wide private_mode_user_session cookie. Used on
      # Enterprise to authenticate all non-github.com requests via an nginx
      # authentication proxy.
      #
      # TODO: Link up where nginx is configured.
      def touch_subdomain_private_mode_user_session(user_session, key)
        raise TypeError, "can’t touch nil user session" unless user_session

        cookies[:private_mode_user_session] = {
          value: key,
          expires: user_session.expire_time,
          domain: ".#{GitHub.host_name}",
        }

        nil
      end
    end
end
