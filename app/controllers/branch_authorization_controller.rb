# frozen_string_literal: true

class BranchAuthorizationController < AbstractRepositoryController
  before_action :login_required
  before_action :repository_admin_required

  VALID_TYPES = %w[push dismiss vulnerability].freeze
  VALID_ACTOR_TYPES = %w[user team app].freeze

  def authorized_actor
    actor_type, actor_id = params[:item]&.split("/", 2)
    return head 404 unless VALID_TYPES.include?(params[:type])
    return head 404 unless VALID_ACTOR_TYPES.include?(actor_type)

    actor =
      case actor_type
      when "user"
        User.find_by(id: actor_id)
      when "team"
        Team.find_by(id: actor_id)
      when "app"
        IntegrationInstallation.with_repository(current_repository).
          includes(:integration).joins(:integration).
          where(integrations: { id: actor_id }).first
      end

    filtered_actor = filter_write_actors([actor]).first

    return head 404 if filtered_actor.nil?

    respond_to do |format|
      format.html do
        render partial: "branch_authorization/authorized_actor", locals: {
          actor: filtered_actor,
          form_field_name: params[:type],
        }
      end
    end
  end

  def suggestions
    autocompleteQuery = AutocompleteQuery.new(current_user, params[:q],
      organization: current_repository.organization,
      include_teams: true, repository: current_repository,
      include_integration_installations: include_integration_installations?)
    suggestions = filter_write_actors(autocompleteQuery.suggestions)

    respond_to do |format|
      format.html_fragment do
        render partial: "branch_authorization/suggestions", formats: :html, locals: { suggestions: suggestions }
      end
      format.html do
        render partial: "branch_authorization/suggestions", locals: { suggestions: suggestions }
      end
    end
  end

  private

  def include_integration_installations?
    params[:type] == "push"
  end

  def repository_admin_required
    unless current_repository.adminable_by?(current_user)
      render_404
    end
  end

  def filter_write_actors(actors)
    actors.select do |actor|
      if actor.instance_of?(Team)
        (actor.id_and_ancestor_ids & team_ids_with_write_access).any?
      elsif actor.instance_of?(User)
        user_ids_with_write_access.include? actor.id
      elsif actor.instance_of?(IntegrationInstallation)
        integration_installation_ids_with_write_access.include? actor.id
      end
    end
  end

  def team_ids_with_write_access
    @team_ids_with_write_access ||= current_repository.actor_ids(type: Team, min_action: :write)
  end

  def user_ids_with_write_access
    @user_ids_with_write_access ||= current_repository.actor_ids(type: User, min_action: :write)
  end

  def integration_installation_ids_with_write_access
    @integration_installation_ids_with_write_access ||= begin
      Authorization.service.actor_ids_with_granular_permissions_on(
        actor_type: IntegrationInstallation,
        subject_type: Repository,
        subject_ids: [current_repository.id],
        owner_ids: [current_repository.owner_id],
        permissions: [:contents, :single_file],
        min_action: :write,
      )
    end
  end
end
