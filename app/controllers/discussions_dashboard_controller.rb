# frozen_string_literal: true

class DiscussionsDashboardController < ApplicationController
  before_action :login_required

  # The following actions do not require conditional access checks:
  # - dashboard: serves `/discussions`, not consistently scoped to an organization.
  #   Enforcement may be required but should be done inline.
  skip_before_action :perform_conditional_access_checks, only: %w(index)
  javascript_bundle :discussions

  def index
    search_query = raw_discussions_search_query.presence && parsed_discussions_query
    discussions = if params[:discussions_q].present?
      search_discussions
    else
      load_visible_discussions
    end
    participants_by_discussion_id = Discussion.participants_by_discussion_id(discussions,
      viewer: current_user)

    render "discussions_dashboard/index", locals: {
      discussions: discussions,
      query: sanitized_query_string,
      participants_by_discussion_id: participants_by_discussion_id,
    }
  end

  private

  def sanitized_query_string
    @sanitized_query_string ||= Search::Queries::DiscussionQuery.
      stringify(parsed_discussions_query)
  end

  def parsed_discussions_query
    @parsed_discussions_query ||= Search::Queries::DiscussionQuery.normalize(
      Search::Queries::DiscussionQuery.parse(raw_discussions_search_query, current_user),
    )
  end
  helper_method :parsed_discussions_query

  def raw_discussions_search_query
    if params[:discussions_q].present?
      params[:discussions_q]
    elsif params[:created_by]
      "author:#{current_user}"
    elsif params[:commented]
      "commenter:#{current_user}"
    end
  end

  def search_discussions
    Discussion::SearchResult.search(
      query: parsed_discussions_query,
      page: params[:page],
      per_page: DEFAULT_PER_PAGE,
      current_user: current_user,
      remote_ip: request.remote_ip,
      user_session: user_session
    )
  end

  def load_visible_discussions
    discussions = if params[:commented]
      Discussion.commented_on_and_visible_to(current_user)
    else
      Discussion.authored_by_and_visible_to(current_user)
    end

    discussions = discussions.filter_spam_for(current_user).
      includes(:repository).
      recently_updated_first

    org_ids = unauthorized_organization_ids
    if org_ids.present?
      discussions = Discussion.filter_out_org_discussions(discussions: discussions,
        org_ids: Set.new(org_ids))
    end

    discussions.paginate(page: current_page, per_page: DEFAULT_PER_PAGE)
  end
end
