# frozen_string_literal: true

class BlobController < GitContentController
  # note to future service catalog mappers: please set blob to be the default service once
  # the remaining endpoints have had their owning service identified. This way we'll have a
  # sensible default for future additions. Thanks!
  map_to_service :blob, only: %i(show excerpt raw contributors contributors_list blame new edit
    delete preview create save destroy)

  map_to_service :pull_request_reviews, only: %i(codeowners)

  CONDITIONAL_ACCESS_BYPASS_PUBLIC_REPO_ACTIONS = %w(show raw contributors contributors_list).freeze

  statsd_tag_actions only: [:contributors, :edit, :show, :blame]

  param_encoding :show, :name, "ASCII-8BIT"
  param_encoding :raw, :name, "ASCII-8BIT"
  param_encoding :contributors, :name, "ASCII-8BIT"

  include GitHub::RateLimitedRequest
  include TextHelper
  include WebCommitControllerMethods

  rate_limit_requests \
    only: :raw,
    if: :rate_limited?

  before_action :clean_params
  before_action :add_spamurai_form_signals, only: [:show]
  before_action :redirect_for_missing_branch, only: [:show]
  before_action :require_blob,      only: [:contributors, :contributors_list, :show, :blame, :edit, :delete, :raw, :save, :destroy, :codeowners]
  before_action :ensure_previewing_a_valid_path, only: [:preview]

  before_action :login_or_404,    only: [:new, :edit, :create, :save, :delete, :preview, :destroy]
  before_action :confirm_forking, only: [:new, :edit, :delete]
  before_action :content_authorization_required, only: [:new, :edit, :create, :save, :delete, :destroy]
  before_action :require_xhr, only: [:excerpt]

  layout :repository_layout

  rescue_from GitRPC::InvalidObject, with: :tree_redirect

  rescue_from_timeout only: [:show, :edit] do
    render_timeout
  end

  rescue_from_timeout_without_replay only: [:blame] do
    record_blame_timeout
    render_timeout
  end

  rescue_from GitHub::RefShaPathExtractor::InvalidPath do
    head 400
  end

  FILE_TEMPLATES = {
    code_of_conduct: {
      filename: "CODE_OF_CONDUCT.md",
      contents: lambda { |string| string },
    },
    readme: {
      filename: "README.md",
      contents: lambda { |repo| repo.generate_readme },
    },
    contributing: {
      filename: "CONTRIBUTING.md",
      contents: lambda { |string| string },
    },
    license: {
      filename: "LICENSE",
      contents: lambda { |string| string },
    },
    issue_template: {
      filename: "ISSUE_TEMPLATE.md",
      contents: lambda { |string| string },
    },
    pull_request_template: {
      filename: "PULL_REQUEST_TEMPLATE.md",
      contents: lambda { |string| string },
    },
    repository_funding: {
      filename: FundingLinks::FILENAME,
      contents: lambda { |_| FundingLinks.template },
    },
    workflow_template: {
      filename: ".github/workflows/workflow.yml",
      contents: lambda { |(template_id, repo, user)| Actions::WorkflowTemplates.get_yaml_by_id(template_id, repo, user) },
    },
    security: {
      filename: SecurityPolicy::FILENAME,
      contents: lambda { |_| SecurityPolicy::TEMPLATE },
    },
    code_scanning_workflow_template: {
      filename: ".github/workflows/code-scanning.yml",
      contents: lambda { |(repo, template_id)| repo.code_scanning_workflow_template(template_id) },
    },
    dependabot_template: {
      filename: ".github/dependabot.yml",
      contents: lambda { |_| Dependabot::TEMPLATE },
    },
    other: {
      filename: "",
      contents: lambda { |string| string },
    },
  }

  # These endpoints cause unnecesary, and race-condition riddled updates to the
  # user session, preventing it from timing out. Potentially, from 3rd party sites.
  RACY_ACTIONS = %w(raw show)

  def show
    if params[:path].blank?
      redirect_to tree_url(name: params[:name])
      return
    end

    if params[:raw]
      redirect_to raw_blob_url
      return
    end

    TreeEntry.load_attributes!([current_blob], current_commit.oid)

    if IssueTemplates.valid_path?(path_string)
      issue_templates = IssueTemplates.new(current_repository)
      issue_template = issue_templates[File.basename(path_string)]
    end

    if workflow_yaml_file?
      @blob_fullwidth = false
    end

    if helpers.blob_supports_code_navigation?(current_blob)
      code_nav = CodeNavigation.load(
        current_repository: current_repository,
        current_user: current_user,
        tree_name: tree_name,
        commit_oid: current_commit.oid,
        path: path_string_for_display,
      )
    end

    respond_to do |format|
      format.html do
        render_template_view("blob/show", Blob::ShowView, {
          repo: current_repository,
          tree_name: tree_name,
          commit: current_commit,
          blob: current_blob,
          path: path_string,
          path_for_display: path_string_for_display,
          short_path: params[:short_path],
          issue_template: issue_template,
          code_nav: code_nav,
          valid_legacy_template: IssueTemplates.valid_legacy_template_path?(path_string),
          collaborative_editing_is_enabled: collaborative_editing_enabled?,
        }, locals: { commit: current_commit })
      end
    end
  end

  def codeowners
    ref_name = current_repository.heads.exist?(tree_name) ? tree_name : current_repository.default_branch

    codeowners = Repository::Codeowners.new(current_repository, ref: ref_name, paths: [path_string])

    respond_to do |format|
      format.html do
        render partial: "blob/blob_codeowners", locals: { codeowners: codeowners, path: path_string }
      end
    end
  end

  def excerpt
    sha = params[:oid]
    path = path_string
    mode = params[:mode]

    if path.blank? || mode !~ /\A\d{6}\z/
      head :bad_request
      return
    end
    in_wiki_context = (params[:in_wiki_context] == "true" && current_repository.has_wiki?)
    if in_wiki_context
      diff_repo = current_repository.unsullied_wiki
    else
      diff_repo = current_repository
    end
    tree_entry = TreeEntry.new(diff_repo, {
      "type" => "blob",
      "oid"  => sha,
      "path" => path,
      "mode" => mode,
    })

    unless tree_entry.data
      head :not_found
      return
    end

    respond_to do |format|
      format.html do
        # support old parameter format to prevent failures during feature enabling
        original_last_left = params[:last_left].to_i
        original_last_right = params[:last_right].to_i
        original_left = params[:left].to_i
        original_right = params[:right].to_i
        direction = ["up", "down"].include?(params[:direction]) ? params[:direction] : nil

        dir = direction.nil? ? "unspecified" : direction
        GitHub.dogstats.increment("blob.excerpt", tags: ["direction:#{dir}"])

        view = create_view_model(Blob::DirectionalBlobExcerpt,
          syntax_highlighted_diffs_enabled: syntax_highlighted_diffs_enabled?,
          blob: tree_entry,
          direction: direction,
          original_last_left: original_last_left,
          original_last_right: original_last_right,
          original_left: original_left,
          original_right: original_right,
          left_hunk_size: params[:left_hunk_size].presence.try(:to_i),
          right_hunk_size: params[:right_hunk_size].presence.try(:to_i),
          in_wiki_context: in_wiki_context,
          current_repository: current_repository,
        )

        GlobalInstrumenter.instrument("blob.excerpt", {
          repository: current_repository,
          actor: current_user,
          path: path,
          blob_oid: sha,
          first_line: view.hunk_start,
          last_line: view.hunk_start + view.lines.count - 1,
          direction: params[:direction] || "full",
        })

        render(partial: "blob/directional_excerpt", locals: { view: view })
      end
    end
  end

  def raw
    render_url = GitHub.render_url_for_origin(request.headers["HTTP_ORIGIN"])
    response.headers["Access-Control-Allow-Origin"] = render_url

    if current_blob.git_lfs?
      redirect_to media_domain_url(current_repository)
      return
    end

    redirect_to build_raw_url(type: :repository)
  end

  # Calculating the list of top contributors is done async. This is the header list of
  # contributors on the show blob page.
  def contributors
    respond_to do |format|
      format.html do
        render_partial_view("blob/contributors", Blob::ShowView,
          repo: current_repository,
          tree_name: tree_name,
          commit: current_commit,
          blob: current_blob,
          path: path_string,
          collaborative_editing_is_enabled: collaborative_editing_enabled?
        )
      end
    end
  end

  # This is the popup that lists _all_ contributors for a blob when you click the count
  # on the main blob screen.
  def contributors_list
    respond_to do |format|
      format.html do
        render_partial_view("blob/contributors_list", Blob::ShowView,
          repo: current_repository,
          tree_name: tree_name,
          commit: current_commit,
          blob: current_blob,
          path: path_string,
          collaborative_editing_is_enabled: collaborative_editing_enabled?
        )
      end
    end
  end

  def blame
    @blame_timer = GitHub::SafeTimer.new(GitHub.request_timeout(nil) - 1) # give ourselves one second to clean up
    render_404 unless current_blame

    @blame_rendered_from_cache = true # will be set to false below if cache block is invoked

    Commit.prefill_users(current_blame.commits.values)

    # load editor configs for tab_size helper
    @editor_configs = current_repository.load_editor_config(current_commit, [current_blame.path])

    measure_timings do
      render "blob/blame"
    end
  end

  param_encoding :new, :name, "ASCII-8BIT"

  def new
    set_path_variables(infer_filename: false)
    @branch   = params[:name]

    return render_404 unless establish_target_repository_for_editor_action

    template_type, args = template_from_params
    template = FILE_TEMPLATES.fetch(template_type, :other)

    @filename = params[:filename] || template[:filename]
    @filename = current_repository.funding_links_path if @filename == FundingLinks::FILENAME

    @contents = params[:value] || template[:contents].call(args)
    @allow_contents_unchanged = !!(params[:value] || params[:readme] || params[:code_of_conduct] || params[:license] || params[:workflow_template] || params[:code_scanning_workflow_template])

    if set_form_commit(true)
      if template_type == :code_of_conduct
        flash.now[:notice] = "Your code of conduct template is ready. Please review it below and either commit it to the #{current_repository.default_branch} branch or to a new branch."
        params[:target_branch] = current_repository.heads.temp_name(topic: "add-code-of-conduct")
        params[:quick_pull] = @branch
      elsif template_type == :license
        flash.now[:notice] = "Your license is ready. Please review it below and either commit it to the #{current_repository.default_branch} branch or to a new branch."

        params[:target_branch] = current_repository.heads.temp_name(topic: "add-license")
        params[:quick_pull] = @branch
      end
      instrument(
        "blob.new.page_view",
        target_repository: @target_repo,
      )
      render "blob/new"
    else
      render_404
    end
  end

  param_encoding :edit, :name, "ASCII-8BIT"

  # ?flow=1 set if someone intends to create a new flow file
  def edit
    set_path_variables
    @branch = params[:name]
    @allow_contents_unchanged = true if params[:allow_unchanged]

    if params[:code_of_conduct] || params[:license]
      template_type, args = template_from_params
      template = FILE_TEMPLATES.fetch(template_type, :other)

      @filename = template[:filename]
      @filename = current_repository.funding_links_path if @filename == FundingLinks::FILENAME

      @contents = template[:contents].call(args)
      @allow_contents_unchanged = !!(params[:code_of_conduct] || params[:license])
    end

    issue_templates = IssueTemplates.new(current_repository)
    @issue_template = issue_templates[File.basename(params[:name])]

    return render_404 unless establish_target_repository_for_editor_action

    if set_form_commit
      if current_blob.binary?
        redirect_to tree_url(name: params[:name], path: params[:path])
      else
        instrument(
          "blob.edit.page_view",
          target_repository: @target_repo,
          path: path_string,
        )
        render "blob/edit"
      end
    else
      render_404
    end
  end

  def delete
    @blob_fullwidth = false
    set_path_variables
    @branch   = tree_name
    url = destroy_file_path(current_repository.owner, current_repository, @branch, @path, pr: params[:pr])

    return render_404 unless establish_target_repository_for_editor_action

    if set_form_commit
      render "blob/delete", locals: {
        url: url,
      }
    else
      render_404
    end
  end

  FundingQuery = parse_query <<~'GRAPHQL'
    query($sponsorableUsersIds: [ID!]!, $sponsorableOrgsIds: [ID!]!) {
      ...Views::FundingLinks::Links::Root
    }
  GRAPHQL

  def preview
    return render_404 unless request.post?

    url_base = URI.parse(url_for(action: :show)).path

    funding_data = if funding_file?
      funding_links = FundingLinks.for(blob: preview_contents)
      platform_execute(FundingQuery, variables: {
        sponsorableUsersIds:    funding_links.sponsorable_users_ids,
        sponsorableOrgsIds:     funding_links.sponsorable_orgs_ids,
      })
    end

    respond_to do |format|
      format.html do
        render_partial_view("blob/preview", Blob::PreviewView, {
          funding_data: funding_data,
          repo: current_repository,
          tree_name: tree_name,
          path: path_string,
          blobname: params[:blobname],
          code: preview_contents,
          old_oid: old_oid_for_preview,
          creates_branch: params[:willcreatebranch],
          render_url_base: url_base,
          responsive: true,
        }, layout: false)
      end
    end
  end

  param_encoding :create, :name, "ASCII-8BIT"

  def create
    return redirect_to(url_for(action: "new")) unless request.post?

    branch, path = extract_branch_path(params[:name])
    @branch = branch

    return render_404 unless establish_target_repository_for_commit_action(branch: branch)

    if @target_repo != current_repository
      params[:quick_pull] ||= [current_repository.owner.login, branch].join(":")
    end

    increment_quick_pull_start_stats

    contents  = params[:value] || "".dup
    contents.gsub!(/\r\n/, "\n")

    if params[:new_filename].blank? && params[:filename].blank?
      return new
    end

    if params[:new_filename].present?
      @filename = File.basename(params[:new_filename]).strip
      path      = File.dirname(params[:new_filename]) if params[:new_filename].include? "/"
      file_path = [path, @filename].reject(&:blank?).join("/")
    else
      @filename = params[:filename].strip
      path    ||= params[:path].join("/") if params[:path]
      file_path = [path, @filename].reject(&:blank?).join("/")
    end

    file_path = file_path.strip
    @filename ||= File.basename(file_path)

    message = commit_message_from_request("new")

    unless valid_filename?(nil, file_path, branch)
      increment_quick_pull_error_stats
      return new
    end

    params[:commit] = nil if params[:commit].blank?

    if change_conflict?(file_path, params[:commit])
      increment_quick_pull_error_stats
      return new
    end

    contents += "\n" unless contents.ends_with?("\n")
    unless branch = commit_change_to_repo_for_user(@target_repo, current_user, branch, params[:commit], { file_path => contents }, message)
      @default_filename = @filename

      # Don't show the flash error on hook failures, since we show their output separately
      if @hook_out
        @hook_message = "File could not be created."
      else
        flash[:error] = "File could not be created"
      end

      increment_quick_pull_error_stats
      return new
    end

    blob_action_success_with_repo(
      @target_repo, "new", branch, path,
      payload: {path: file_path}
    )
  end

  param_encoding :save, :name, "ASCII-8BIT"
  param_encoding :save, :new_filename, "ASCII-8BIT"

  def save
    return redirect_to(url_for(action: "edit")) unless request.post?

    branch, path = extract_branch_path(params[:name])
    @branch = branch

    return render_404 unless establish_target_repository_for_commit_action(branch: branch)

    ref = current_repository.heads.find(params[:name])
    return render_404 unless ref

    track_stats = []
    commit = params[:commit]

    if @target_repo != current_repository
      params[:quick_pull] ||= [current_repository.owner.login, branch].join(":")
    else
      track_stats.push "maintainer_pushed" if !current_repository.pushable_by?(current_user)
    end

    increment_quick_pull_start_stats

    message = commit_message_from_request("edit")

    branch          = params[:name]
    new_path_string = params[:new_filename] if params[:new_filename].present?
    new_path_string ||= path_string

    new_path_string = new_path_string.strip
    @filename = File.basename(new_path_string)

    unless valid_filename?(path_string, new_path_string, branch)
      increment_quick_pull_error_stats
      return edit
    end

    if change_conflict?(path_string, commit, ref.target_oid)
      increment_quick_pull_error_stats
      return edit
    end

    check_repository, check_ref = current_repository, ref
    unless check_repository.includes_file?(path_string, check_ref.name)
      base_repository, base_branch = base_repository_and_branch
      base_ref = base_repository.heads.find(base_branch)

      check_repository = base_repository
      check_ref = base_ref
    end

    contents = params[:value]
    contents += "\r\n" unless contents.ends_with?("\n")  # yes, \r\n. The next line will make it \n if applicable
    contents  = check_repository.preserve_line_endings(check_ref.target_oid, path_string, contents)

    if path_string != new_path_string
      files = {
        new_path_string => { from: path_string, contents: contents },
      }
      params[:path] = new_path_string.split("/")
      track_stats.push "edit" if params[:content_changed].present?
      track_stats.push "rename"
    else
      files = { path_string => contents }
    end

    unless branch = commit_change_to_repo_for_user(@target_repo, current_user, branch, commit, files, message)
      # Don't show the flash error on hook failures, since we show their output separately
      if @hook_out.present?
        @hook_message = "File could not be edited."
      else
        flash.now[:error] = "File could not be edited"
      end

      increment_quick_pull_error_stats

      @contents = contents
      @allow_contents_unchanged = true
      return edit
    end

    blob_action_success_with_repo(
      @target_repo, "edit", branch, params[:path],
      stats: track_stats,
      payload: {path: params[:path].join("/")}
    )
  end

  def destroy
    return redirect_to(url_for(action: "delete")) unless request.delete?

    branch, path = extract_branch_path(params[:name])
    @branch = branch

    return render_404 unless establish_target_repository_for_commit_action(branch: branch)

    ref = @target_repo.heads.find(branch)
    return render_404 unless ref

    if @target_repo != current_repository
      params[:quick_pull] ||= [current_repository.owner.login, branch].join(":")
    end

    increment_quick_pull_start_stats

    @filename = params[:filename]
    path      = params[:path].join("/")
    file_path = [path, @filename].reject(&:blank?).join("/")

    message = commit_message_from_request("delete")

    if change_conflict?(file_path, params[:commit], ref.target_oid)
      increment_quick_pull_error_stats
      return delete
    end

    unless branch = commit_change_to_repo_for_user(@target_repo, current_user, branch, params[:commit], { file_path => nil }, message)
      # Don't show the flash error on hook failures, since we show their output separately
      if @hook_out.present?
        @hook_message = "File could not be deleted."
      else
        flash[:error] = "File could not be deleted"
      end

      increment_quick_pull_error_stats
      return delete
    end

    blob_action_success_with_repo(
      @target_repo, "delete", branch,
      redirect_back_to_pr? ? params[:path] : File.dirname(path),
      payload: {path: file_path}
    )
  end

  def blame_not_cached!
    @blame_rendered_from_cache = false
  end

private

  UNCACHED_TAGS = ["rendered_from_cache:false"]
  CACHED_TAGS = ["rendered_from_cache:true"]

  def measure_timings
    start_time = GitHub::Dogstats.monotonic_time

    # set up a temporary subscriber to measure graphql execution so it
    # can be subtracted from our overall timing.
    data_loading_time = 0
    callback = -> (_, start, finish, *) { data_loading_time += (finish - start) }
    ActiveSupport::Notifications.subscribed(callback, "query.graphql") do
      yield
    end

    elapsed_ms = GitHub::Dogstats.duration(start_time)
    data_loading_time_ms = data_loading_time * 1000
    net_elapsed_ms = elapsed_ms - data_loading_time_ms # net == "exclusive of time spent loading data"

    GitHub.dogstats.batch do |stats|
      tags = nil
      if @blame_rendered_from_cache
        tags = CACHED_TAGS
        stats.increment("blame.cached_render_count")
      else
        tags = UNCACHED_TAGS
        stats.increment("blame.uncached_render_count")
      end

      stats.distribution("blame.render.dist.net_time_ms", net_elapsed_ms, tags: tags)
      stats.distribution("blame.render.dist.data_loading_time_ms", data_loading_time_ms, tags: tags)
      stats.distribution("blame.render.dist.lines", current_blame.lines.size)
      stats.distribution("blame.render.dist.commits", current_blame.commits.size)
    end
  end

  # When a blame timeout occurs, record the user and repo+path and log current
  # cardinality for both in datadog.
  def record_blame_timeout
    user_counter = GitHub::Redis::CardinalityCounter.new(domain: "blame_timeout_affected_users", redis: Resque.redis)
    file_counter = GitHub::Redis::CardinalityCounter.new(domain: "blame_timeout_affected_files", redis: Resque.redis)

    u1, u7, u14, f1, f7, f14 = nil
    Resque.redis.pipelined do
      user_counter.add(current_user&.id)
      file_counter.add("#{current_repository.id}:#{path_string}")
      u1  = user_counter.count(since: 1.day.ago)
      u7  = user_counter.count(since: 7.days.ago)
      u14 = user_counter.count(since: 14.days.ago)
      f1  = file_counter.count(since: 1.day.ago)
      f7  = file_counter.count(since: 7.days.ago)
      f14 = file_counter.count(since: 14.days.ago)
    end

    # Record the total number of users and files that have experienced a
    # timeout in the past (1/7/14) days.  This will need to be summed by
    # site in datadog.
    GitHub.dogstats.batch do |stats|
      stats.increment("blame.soft_timeouts")
      stats.gauge("blame.timeout.user_count_1d", u1.value)
      stats.gauge("blame.timeout.user_count_7d", u7.value)
      stats.gauge("blame.timeout.user_count_14d", u14.value)
      stats.gauge("blame.timeout.file_count_1d", f1.value)
      stats.gauge("blame.timeout.file_count_7d", f7.value)
      stats.gauge("blame.timeout.file_count_14d", f14.value)
    end
  end

  # Render an error message where blob content would normally live that the
  # operation timed out.
  def render_timeout
    render_template_view("blob/timeout", Blob::ShowView,
      repo: current_repository,
      tree_name: tree_name,
      commit: current_commit,
      blob: current_blob,
      path: path_string,
      short_path: params[:short_path],
      collaborative_editing_is_enabled: collaborative_editing_enabled?
    )
  end

  helper_method def timeout_if_expired!
    return unless @blame_timer
    if @blame_timer.expired?
      raise Timeout::Error, "proactively timing out before middleware kills process"
    end
  end

  helper_method :current_blob, :blob_view_key, :current_blame

  helper_method def current_issue_template
    return @current_issue_template if defined?(@current_issue_template)

    unless current_repository
      return @current_issue_template = nil
    end

    unless editing_issue_template?
      return @current_issue_template = nil
    end

    issue_templates = IssueTemplates.new(current_repository)
    issue_template_name = File.basename(path_string)
    @current_issue_template = issue_templates[issue_template_name] || IssueTemplate.new(repository: current_repository, filename: File.basename(path_string))
  end

  helper_method def editing_issue_template?
    IssueTemplates.valid_path?(path_string)
  end

  # Can the current user commit to the current branch?
  def can_commit_to_branch?
    return @can_commit_to_branch if defined?(@can_commit_to_branch)
    @can_commit_to_branch = current_repository.can_commit_to_branch?(current_user, @branch)
  end

  # Preserve CRLF or LF line endings in the blob code to prevent the preview
  # diff from marking every line as changed.
  #
  # The form submission always encodes line endings as CRLF, so we detect the
  # blob's original line ending style and convert accordingly.
  #
  # Returns the blob code String to preview.
  def preview_contents
    if previewing_a_new_file?
      params[:code]
    else
      current_repository.preserve_line_endings(old_oid_for_preview, path_string, params[:code])
    end
  rescue GitRPC::InvalidFullOid
    params[:code]
  end

  # Private: determines the old_oid for generating a preview's synthetic commit
  def old_oid_for_preview
    old_oid = params[:commit]
    if old_oid.blank?
      old_oid = current_commit ? current_commit.oid : ""
    end
    old_oid
  end

  # Private: initializes the @path, @filename, and @new_filename_string instance variables
  def set_path_variables(infer_filename: true)
    set_path_variable
    set_filename_variable(infer_filename: infer_filename)
    set_new_filename_string_variable
  end

  # Private: sets @path to params[:path]. If we aren't given a path
  # but we do have a filename, use that.
  def set_path_variable
    @path = params[:path] || [params[:filename]].compact
  end

  # Private: sets @filename to params[:filename].
  # if we aren't given a filename, but we are allowed to infer
  # a filename, extract it from the path. Normally, we don't
  # infer a filename when `#new` is called initially. If #create
  # encounters an error and redisplays #new, we'll have a filename
  #
  # Call *after* set_path_variable, as this depends on @path
  # Cleans up by ensuring it is a stripped, non-null string
  def set_filename_variable(infer_filename: true)
    @filename = params[:filename]
    @filename = @path.try(:last) if infer_filename && @filename.blank?
    @filename ||= ""
    @filename = @filename.strip
  end

  # Private: sets @new_filename_string to params[:new_filename].
  def set_new_filename_string_variable
    @new_filename_string = params[:new_filename]
  end

  # Used in blob/show to render blob contributors inline if they exist in
  # cache. The cache is populated in an async action in this controller.
  def blob_contributors_view_key
    key = [
      "v1.19.#{AvatarHelper::CACHE_VERSION}",
      "contributors",
      current_repository.id,
      current_repository.name_with_owner,
      current_blob.id,
      current_blob.mode,
      path_string,
    ]

    git_content_cache_key :blob_contributors, key
  end
  helper_method :blob_contributors_view_key

  def rate_limited?
    rate = Api::BlobThrottler.new(request.fullpath, request.referrer).rate
    rate.set_response(response.headers)
    rate.at_limit?
  end

  # Returns: nil if there is a current commit and a bad path, false if there is no
  #          current commit and/or a blank path, or an instance of Commit
  def current_blob
    return @current_blob if defined?(@current_blob)

    @current_blob =
      if current_commit.nil? || path_string.blank?
        nil
      else
        current_repository.blob(tree_sha, path_string, blob_limits)
      end

    current_blob_from_base if params[:quick_pull] && !@current_blob

    @current_blob
  end

  def current_blob_from_base
    base_repository, base_branch = base_repository_and_branch
    base_oid = base_repository.heads.find(base_branch).target_oid
    @current_blob = base_repository.blob(base_oid, path_string, blob_limits)
  end

  def require_blob
    render_404 if params[:path].blank? || !current_blob
  end

  def redirect_for_missing_branch
    # See `AbstractRepositoryController#set_path_and_name`
    return if params[:branch].present?

    missing_branch_regex = %r{/blob/(?<branch>[^/]+)/}
    matches = request.path.match(missing_branch_regex)
    return unless matches && matches[:branch] == "master"

    GlobalInstrumenter.instrument "repository.missing_branch_redirect",
      repository: current_repository,
      actor: current_user,
      missing_branch_was_master: true

    redirect_path = request.path.sub(missing_branch_regex, "/blob/#{current_repository.default_branch}/")
    redirect_to redirect_path, notice: "Branch not found, redirected to default branch."
  end

  def ensure_previewing_a_valid_path
    render_404 unless previewing_a_new_file? || previewing_an_existing_file?
  end

  def previewing_a_new_file?
    path_string.nil? || current_repository.includes_directory?(path_string, tree_name)
  end

  def previewing_an_existing_file?
    current_repository.includes_file?(path_string, tree_name)
  end

  def base_repository_and_branch
    base = current_repository.base_branch(params[:name], current_user)
    base_branch, base_user = base.split(":").reverse
    base_repository = base_user ? current_repository.find_fork_in_network_for_user(base_user) : current_repository

    [base_repository, base_branch]
  end

  def current_blame
    @current_blame ||= current_repository.blame(
      current_commit.oid,
      path_string,
      timeout: request_time_left - 2 # Max of ~8 seconds in prod
    )
  end

  def tree_redirect(boom)
    if boom.message =~ /expected blob, got tree/
      redirect_to tree_url(host: GitHub.host_name, path: params[:path], name: params[:name]), status: 301
    else
      render_404
    end
  end

  def establish_target_repository_for_editor_action
    establish_target_repository(branch: @branch, create_fork: true)
  end

  def establish_target_repository_for_commit_action(branch:)
    establish_target_repository(branch: branch, create_fork: false)
  end

  # Internal: Check if filename is acceptable
  #
  # old_path - full path where file used to be
  # new_path - full path where file will be
  # branch   - branch name for this file check
  #
  # Sets flash error
  # Returns Boolean result
  def valid_filename?(old_path, new_path, branch)
    check_existing = old_path != new_path
    validation_message = GitHub::GitFile.validate_path(new_path)
    null_byte = %r{[\x00]}
    slash_or_null_byte = %r{[/\x00]}

    if new_path.include?("\0")
      error = "Your filename contains invalid characters. Please choose a different name and try again."
    else
      filename = File.basename(new_path)
      error = if filename.blank?
        "Please specify a name for your file and try again."
      elsif filename.match(slash_or_null_byte) || new_path.match(null_byte) || %w[. .. .git].include?(filename)
        "Your filename contains invalid characters. Please choose a different name and try again."
      elsif check_existing && current_repository.includes_file?(new_path, branch)
        "A file with the same name already exists. Please choose a different name and try again."
      elsif !current_repository.valid_file_path?(new_path, branch)
        "Sorry, a file exists where you’re trying to create a subdirectory. Choose a new path and try again."
      elsif validation_message
        "That path #{validation_message}. Please choose a different path and try again."
      elsif new_path.length >= 1000
        "Sorry, that file path is too long. Please choose a file path shorter than 1000 characters."
      end
    end

    if error
      flash.now[:error] = error

      @default_filename = filename
      @contents = params[:value]

      return false
    else
      flash.delete(:error)

      return true
    end
  end

  # Internal: Record and handle success for a blob action
  #
  # Increment stat for blob action
  # Redirect to appropriate followup location
  #
  # repo     - repository to use for redirection when finished
  # action   - action that was taken (new, edit, delete)
  # branch   - branch name where action was taken
  # path     - path where action was taken
  # stats    - optional array of stats to increment instead of action
  # payload  - optional additional payload to report in instrumentation
  #
  # Returns nothing
  def blob_action_success_with_repo(repo, action, branch, path, stats: [], payload: {})
    stats << action if stats.empty?
    stats.each { |stat| GitHub.dogstats.increment("blob", tags: ["stat:#{stat}"]) }

    GitHub.instrument "blob.crud", user: current_user

    increment_quick_pull_commit_stats

    instrument(
      "blob.#{action}.succeeded",
      {
        target_repository: repo,
        target_branch: branch,
        action: action,
      }.merge(payload),
    )

    if params[:quick_pull]
      # redirect to new pull request page
      base = params[:quick_pull]
      base = nil if base == "1"
      range = [base, branch].compact.join("...")

      redirect_to compare_path(repo, range) + "?quick_pull=1"
    elsif redirect_back_to_pr?
      redirect_to "#{params[:pr]}/files##{diff_path_anchor(path.join("/"))}"
    elsif redirect_back_to_owner_profile?
      redirect_to user_path(repo.owner)
    else
      path = nil if path.blank? || path == "."

      case action
      when "new"
        redirect_to tree_url(name: branch, path: path)
      when "edit"
        redirect_to url_for(action: "show", path: path, name: branch)
      when "delete"
        flash[:notice] = "File successfully deleted."
        redirect_path = repo.longest_existing_subpath(path, branch)
        redirect_path = nil if redirect_path.empty?
        redirect_to tree_url(name: branch, path: redirect_path)
      end
    end
  end

  def redirect_back_to_owner_profile?
    params[:redirect_to] == "owner_profile"
  end

  def login_or_404
    return if logged_in?
    if ["edit", "delete", "new"].include?(params[:action]) && current_repository.public?
      redirect_to_login(request.url)
    else
      render_404
    end
  end

  # Filter before new, edit, and delete actions
  #
  # A GET request must not make a change, such as creating a fork
  # or even updating an existing fork.
  def confirm_forking
    forking = updating = false
    unless current_user_can_push_to_ref?(params[:name])
      user_fork = current_repository.find_fork_in_network_for_user(current_user)
      forking   = user_fork.blank?
      updating  = !forking && !user_fork.refs.exist?(params[:name])
    end

    if request.get? && (forking || updating)
      render "blob/edit_confirmation"
    end
  end

  def blob_limits
    case params[:action].to_s
    when "raw"
      {truncate: false, limit: 10.megabytes}
    when "show"
      # Only truncate blobs for staff
      if preview_features?
        {truncate: 500.kilobytes, limit: 10.megabytes}
      else
        {truncate: false, limit: 1.megabytes}
      end
    else
      {truncate: false, limit: 1.megabytes}
    end
  end

  # Internal: extract branch and path from `name` parameter
  #
  # This sort of horrid crap is normally handled in RefShaPathExtractor, but it's troublesome
  # when the repo is empty.
  #
  # returns argument when repo has a branch with that name
  #
  # returns [branch, path] when repo has no branches,
  # `branch` being the part of the argument before the first `/` character
  # and `path` being the rest (or the empty string)
  #
  # returns nil otherwise
  def extract_branch_path(name)
    return name if current_repository.heads.exist?(name)

    if current_repository.heads.empty?
      branch, path = name.split("/", 2)
      path ||= ""

      [branch, path]
    end
  end

  def content_authorization_required
    authorize_content(:blob)
  end

  def code_of_conduct
    key = params[:code_of_conduct][:code_of_conduct]
    code_of_conduct = CodeOfConduct.find_by_key(key)
    return "" unless code_of_conduct
    code_of_conduct.generate(params[:code_of_conduct])
  end

  def license
    license = params[:license][:license]
    if license_template = License.find(license)
      hash = {}
      license_template.fields.each do |field|
        field_name = field.key.parameterize separator: "_"

        hash[field_name] = params[:license][field_name.to_sym]
      end

      license_template.generate(field_values: hash)
    else
      ""
    end
  end

  def template_from_params
    case
    when params[:readme]
      [:readme, current_repository]
    when params[:code_of_conduct]
      [:code_of_conduct, code_of_conduct]
    when params[:contributing]
      [:contributing, params[:value]]
    when params[:license]
      [:license, license]
    when params[:issue_template]
      [:issue_template, params[:value]]
    when params[:pull_request_template]
      [:pull_request_template, params[:value]]
    when params[:repository_funding]
      [:repository_funding, params[:value]]
    when params[:workflow_template]
      if GitHub.actions_enabled?
        [:workflow_template, [params[:workflow_template], current_repository, current_user]]
      else
        [:other, ""]
      end
    when params[:security_policy]
      [:security, ""]
    when params[:code_scanning_workflow_template]
      if current_repository.code_scanning_readable_by?(current_user)
        [:code_scanning_workflow_template, [current_repository, params[:code_scanning_workflow_template]]]
      else
        [:other, ""]
      end
    when params[:dependabot_template]
      [:dependabot_template, ""]
    else
      [:other, ""]
    end
  end

  def route_supports_advisory_workspaces?
    true
  end

  # Private: Instrument a blob event with some payload. By default, the payload
  # will include the actor, repository, branch, and whether the actor can commit
  # to the branch.
  #
  # name    - String name of the event.
  # payload - Hash event payload.
  #
  # Returns nothing.
  def instrument(name, payload = {})
    GlobalInstrumenter.instrument(
      name,
      {
        actor: current_user,
        repository: current_repository,
        branch: params[:name],
        can_commit_to_branch: can_commit_to_branch?,
      }.merge(payload),
    )
  end

  # Disallow session touching for a few racy endpoints
  def allow_session_touching?
    RACY_ACTIONS.exclude?(params[:action])
  end

  def funding_file?
    if path_string.blank? # new file
      params[:blobname]&.downcase == FundingLinks::FILENAME.downcase
    else
      funding_links_path = current_repository&.funding_links_path || FundingLinks::PATH
      path_string.downcase == funding_links_path.downcase
    end
  end

  # Bypass conditional access requirements for the specified actions if the
  # current action is exempt.
  def require_conditional_access_checks?
    return false if conditional_access_exempt?
    super
  end
end
