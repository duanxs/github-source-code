# frozen_string_literal: true

class Network::DependabotController < GitContentController
  areas_of_responsibility :dependabot

  skip_before_action :try_to_expand_path
  before_action :ensure_dependabot_available
  before_action :ensure_repo_adminable, only: [:enable]

  layout :repository_layout

  def index
    config_file, update_configs, dependabot_unavailable, dependabot_error = nil

    begin
      response = Dependabot::Twirp.update_configs_client.list_update_configs(repository_id: current_repository.id)
      config_file = response.config_file
      update_configs = response.update_configs
    rescue Dependabot::Twirp::ServiceUnavailableError
      dependabot_unavailable = true
    rescue Dependabot::Twirp::Error => error
      dependabot_error = error
    end

    respond_to do |format|
      format.html do
        render "network/dependabot/index", locals: {
          config_file: config_file,
          update_configs: update_configs || [],
          dependabot_unavailable: dependabot_unavailable,
          dependabot_error: dependabot_error,
        }
      end
    end
  end

  def show
    update_job, update_job_logs, dependabot_unavailable, dependabot_error = nil

    begin
      response = Dependabot::Twirp.update_jobs_client.get_job_logs(
        repository_id: current_repository.id,
        update_job_id: params[:update_job_id].to_i,
      )
      if response.update_job
        update_job = Dependabot::Twirp::UpdateJobStatusDecorator.new(response.update_job)
      end
      update_job_logs = response.update_job_logs
    rescue Dependabot::Twirp::ServiceUnavailableError
      dependabot_unavailable = true
    rescue Dependabot::Twirp::Error => error
      dependabot_error = error
    end

    respond_to do |format|
      format.html do
        render "network/dependabot/show", locals: {
          update_job: update_job,
          update_job_logs: update_job_logs,
          dependabot_unavailable: dependabot_unavailable,
          dependabot_error: dependabot_error,
        }
      end
    end
  end

  def create
    update_job, dependabot_unavailable, dependabot_error = nil

    begin
      response = Dependabot::Twirp.update_configs_client.trigger_update_job(
        repository_id: current_repository.id,
        update_config_id: params.fetch(:update_config_id).to_i
      )
      update_job = response.update_job
    rescue Dependabot::Twirp::ServiceUnavailableError
      dependabot_unavailable = true
    rescue Dependabot::Twirp::Error => error
      dependabot_error = error
    end

    respond_to do |format|
      format.html do
        if dependabot_error
          flash[:error] = dependabot_error.msg
        elsif dependabot_unavailable || update_job.nil?
          flash[:error] = "Failed to check for updated dependencies."
        else
          flash[:notice] = "Started checking for updated dependencies."
        end

        redirect_to action: :index
      end
    end
  end

  def enable
    return redirect_to action: :index if current_repository.dependabot_installed?

    result = AutomaticAppInstallation.trigger(
      type: :button_clicked,
      originator: GitHub.dependabot_github_app,
      actor: current_repository,
      async: false
    ).first

    respond_to do |format|
      format.html do
        if result&.success?
          flash[:notice] = "Dependabot was enabled."
        else
          flash[:error] = "Failed to enable Dependabot."
        end

        redirect_to action: :index
      end
    end
  end

  private

  def ensure_dependabot_available
    unless current_repository.automated_dependency_updates_visible_to?(current_user)
      render_404
    end
  end

  def ensure_repo_adminable
    render_404 unless current_repository.adminable_by?(current_user)
  end
end
