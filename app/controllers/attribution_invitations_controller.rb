# frozen_string_literal: true

class AttributionInvitationsController < ApplicationController
  before_action :login_required

  def accept
    invitation = current_user.targeted_attribution_invitations.find(params[:id])

    if invitation.can_accept?
      invitation.accept!
      flash[:notice] = "The attribution invitation was successfully accepted"
    else
      flash[:error] = invitation.cannot_accept_reason
    end

    redirect_to org_attribution_invitations_path(invitation.owner)
  end

  def reject
    invitation = current_user.targeted_attribution_invitations.find(params[:id])

    if invitation.can_reject?
      invitation.reject!
      flash[:notice] = "The attribution invitation was successfully rejected"
    else
      flash[:error] = invitation.cannot_reject_reason
    end

    redirect_to org_attribution_invitations_path(invitation.owner)
  end

  private

  def target_for_conditional_access
    :no_target_for_conditional_access
  end
end
