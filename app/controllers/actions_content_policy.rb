# frozen_string_literal: true

module ActionsContentPolicy
  extend ActiveSupport::Concern

  DREAMLIFTER_CSP_URLS =
    [
      "https://*.actions.githubusercontent.com",
      "wss://*.actions.githubusercontent.com",
    ]

  # These URLS are used by the Pipelines Devfabric Environment or the Fake Streaming Log service for development
  DREAMLIFTER_DEVELOPMENT_CSP_URLS = DREAMLIFTER_CSP_URLS + [
    "http://localhost:44445",
    "ws://localhost:44444",
    "ws://streaming-logs.localhost:44444",
    "https://pipelines.codedev.ms",
    "wss://pipelines.codedev.ms",
  ]

  # For loading in Dreamlifter logs
  # Usage: before_action :add_dreamlifter_csp_exceptions, only: :show
  def add_dreamlifter_csp_exceptions
    return unless current_repository && GitHub.actions_enabled?

    SecureHeaders.append_content_security_policy_directives(
      request,
      connect_src: dreamlifter_connect_sources,
    )
  end

  def dreamlifter_connect_sources
    return DREAMLIFTER_DEVELOPMENT_CSP_URLS if Rails.env.development?

    DREAMLIFTER_CSP_URLS
  end
end
