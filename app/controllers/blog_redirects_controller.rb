# frozen_string_literal: true

require "#{Rails.root}/config/routes_legacy_blog_post_redirects"

class BlogRedirectsController < ApplicationController
  skip_before_action :perform_conditional_access_checks

  def show
    id = params.require(:id)
    if new_path = LEGACY_BLOG_POST_REDIRECTS[id.to_i]
      redirect_to("#{GitHub.blog_url}/#{new_path}/", status: 301)
    else
      render_404
    end
  end
end
