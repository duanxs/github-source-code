# frozen_string_literal: true

class Workflows::BadgesController < AbstractRepositoryController
  map_to_service :actions_experience

  CONDITIONAL_ACCESS_BYPASS_PUBLIC_REPO_ACTIONS = %w(show).freeze

  before_action :actions_enabled?
  before_action :workflow_name_given?

  def show
    workflow_name = params[:workflow_name]

    app_id = GitHub.launch_github_app.id
    if params[:lab] == "true"
      app_id = GitHub.launch_lab_github_app&.id
    end

    filtered_to_branch = params.has_key?(:branch)
    head_branch = params[:branch] || current_repository.default_branch

    # If an event has been passed in, only get runs triggered by that
    filtered_to_event = params.has_key?(:event)
    event = params[:event]

    # Try to get the latest run for the given workflow, branch, and event
    latest_run = most_recent_run_for(app_id: app_id, workflow_name: workflow_name, head_branch: head_branch, event: event)

    if !latest_run && !filtered_to_branch && !filtered_to_event
      # Check if there are runs for *any* branch and event, if no branch and no event was requested
      latest_run = most_recent_run_for(
        app_id: app_id,
        workflow_name: workflow_name,
      )
    end

    if !latest_run
      # Check if the workflow exists at all
      workflows = current_repository.workflows.active
      workflows = params[:lab] == "true" ? workflows.lab : workflows.prod
      return render_404 unless workflows.find_by(name: workflow_name)
    end

    # At this point we either do have a run, or we at least know that the workflow exists,
    # and can show the no-status badge.
    state = latest_run&.conclusion || latest_run&.status

    respond_to do |format|
      format.svg do
        headers["Cache-Control"] = "max-age=300, private"
        render "workflows/badges/show",
          locals: { workflow: workflow_name, state: state }
      end
    end
  end

  def configure
    return render_404 unless request.xhr?

    respond_to do |format|
      format.html do
        render partial: "workflows/badges/configure_form", locals: {
            workflow_name: params[:workflow_name],
            branch: params[:branch],
            event: params[:event],
          }
      end
    end
  end

  private

  def require_conditional_access_checks?
    return false if conditional_access_exempt?
    super
  end

  def most_recent_run_for(app_id:, workflow_name:, head_branch: nil, event: nil)
    latest_runs = current_repository
      .check_suites
      .for_app_id(app_id)
      .for_workflow(workflow_name)
      .where(conclusion: Workflows::BadgeComponent::SUPPORTED_CONCLUSIONS, head_repository: current_repository)
      .most_recent

    latest_runs = latest_runs.where(head_branch: head_branch) if head_branch

    latest_runs = latest_runs.where(event: event) if event

    latest_runs.first
  end

  def actions_enabled?
    render_404 unless GitHub.actions_enabled?
  end

  def workflow_name_given?
    render_404 unless params[:workflow_name]
  end
end
