# frozen_string_literal: true

class GlobalAdvisoriesController < ApplicationController
  areas_of_responsibility :security_advisories

  include GitHub::RateLimitedRequest

  statsd_tag_actions only: [:index, :show]

  rate_limit_requests(
    only: :index,
    key: :search_rate_limit_key,
    max: :search_rate_limit_max,
    at_limit: :search_rate_limit_record,
  )

  PER_PAGE = 25

  def index
    query = Search::QueryHelper.new(parsed_query, "Vulnerabilities",
      current_user: current_user,
      remote_ip: request.remote_ip,
      page: current_page,
      per_page: PER_PAGE,
      user_session: user_session
    ).vulnerability_query

    results = begin
      query.execute
    rescue StandardError => boom
      Failbot.report(boom)
      Search::Results.empty
    end

    render_template_view(
      "global_advisories/index",
      GlobalAdvisories::IndexView,
      query: query,
      results: results,
    )
  end

  def show
    override_analytics_location "/advisories/<id>"

    advisory = Vulnerability.find_by!(ghsa_id: params[:id])
    return render_404 unless advisory.globally_available?

    render_template_view(
      "global_advisories/show",
      GlobalAdvisories::ShowView,
      advisory: advisory,
    )
  end

  private

  def parsed_query
    Search::Queries::VulnerabilityQuery.stringify(
      Search::Queries::VulnerabilityQuery.parse(params[:query]),
    )
  end

  # Advisories are public
  def require_conditional_access_checks?
    false
  end

  # Unauthenticated and Authenticated: 60 searches every minute
  def search_rate_limit_max
    60
  end

  def search_rate_limit_key
    "advisories_search_limiter:#{logged_in? ? current_user.id : request.remote_ip}"
  end

  def search_rate_limit_record
    GitHub.dogstats.increment("global_advisories.ratelimited", { tags: ["query_present:#{params[:query].present?}", "logged_in:#{logged_in?}"] })
  end
end
