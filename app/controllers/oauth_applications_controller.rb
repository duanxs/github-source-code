# frozen_string_literal: true

class OauthApplicationsController < ApplicationController
  areas_of_responsibility :platform

  statsd_tag_actions only: [:create]

  include OrganizationsHelper
  include OauthApplicationsHelper

  MAX_APPS_FLASH_ERROR_MESSAGE = "You can't create any more OAuth Apps. You've reached the limit for the number of applications owned by this account.".freeze

  before_action :login_required_or_org_admins_only
  before_action :sudo_filter, only: [:reset_secret, :revoke_all_tokens, :transfer]
  before_action :set_cache_control_no_store, only: [:index, :show, :update]
  before_action :check_apps_creation_limit, only: [:new]

  helper_method :beta_features_available?, :current_application

  javascript_bundle :settings

  def index
    case current_context.class.name.downcase
    when "user"
      order = if params[:o] == "used-desc"
        "oauth_authorizations.accessed_at desc"
      elsif params[:o] == "used-asc"
        "oauth_authorizations.accessed_at asc"
      else
        "oauth_applications.name asc"
      end

      @authorizations = current_user.oauth_authorizations.third_party
        .includes(:oauth_application)
        .preload(:public_keys)
        .order(order)
        .limit(15).page(params[:page])

      render "oauth_applications/user"
    when "organization"
      @applications = current_context.oauth_applications.limit(15).page(params[:page])
      @pending_transfers = current_context.inbound_application_transfers
      @authorization_counts =
        OauthAuthorization.where(application_id: @applications.pluck(:id)).group(:application_id).count(:id)
      render "oauth_applications/organization"
    else
      render_404
    end
  end

  def developer
    @pending_transfers = current_user.inbound_application_transfers
    @applications = current_user.oauth_applications.limit(15).page(params[:page])
    @authorization_counts =
      OauthAuthorization.where(application_id: @applications.pluck(:id)).group(:application_id).count(:id)
    render "oauth_applications/developer"
  end

  def new
    @application = populate(params, current_context.oauth_applications.build)
    render "oauth_applications/new"
  end

  def create
    @application = populate(params, current_context.oauth_applications.build)

    if @application.save
      flash[:notice] = "Application created successfully"
      redirect_to oauth_application_path(@application)
    else
      render "oauth_applications/new"
    end
  rescue ::Earthsmoke::Error
    flash[:notice] = "Application creation timed out, please try again."
    render "oauth_applications/new"
  end

  def show
    respond_to do |format|
      format.html do
        @application = find_application
        render "oauth_applications/show"
      end
    end
  end

  def update
    @application = populate(params, find_application)

    if @application.save
      flash[:notice] = "Application updated successfully"
      redirect_to oauth_application_path(@application)
    else
      render "oauth_applications/show"
    end
  end

  def destroy
    @application = find_application

    @application.async_destroy
    flash[:notice] = "Job queued to delete application. It may take a few minutes to complete."

    if request.xhr?
      head 200
    else
      redirect_to oauth_applications_path
    end
  end

  def transfer
    @application = find_application
    target = User.find_by_login!(params[:transfer_to])
    xfer = OauthApplicationTransfer.start(application: @application, target: target, requester: current_user)
    if target.adminable_by?(current_user)
      redirect_to settings_application_transfer_path(target, xfer.id)
    else
      flash[:notice] = "Application transfer request sent to #{target.login}"
      redirect_to oauth_application_path(@application)
    end

  rescue ActiveRecord::RecordInvalid => e
    flash[:error] = "Unable to request application transfer"
    redirect_to oauth_application_path(@application)
  end

  def reset_secret
    @application = find_application
    @application.reset_secret
    flash[:notice] = "Client secret reset successfully"

    if request.xhr?
      head 200
    else
      redirect_to oauth_application_path(@application)
    end
  rescue ::Earthsmoke::Error
    flash[:notice] = "Reset request timed out, please try again."
    if request.xhr?
      head 500
    else
      redirect_to oauth_application_path(@application)
    end
  end

  def revoke_all_tokens
    @application = find_application
    @application.async_revoke_tokens
    flash[:notice] = "Job queued to revoke all user tokens"

    if request.xhr?
      head 200
    else
      redirect_to oauth_application_path(@application)
    end
  end

  def target_for_conditional_access
    target = current_context
    return target unless target.nil?

    # potential security violation: current_context returns nil for anonymous users.
    # remains to be seen if this can be exploited to bypass IP Allowlist / SAML
    #
    # We return temporarily this to avoid the science mismatch that complains about
    # unexpected nil value until the owning team addresses the problem
    # see https://github.com/github/iam/issues/1623 for more info
    :no_target_for_conditional_access
  end

  def advanced
    @application = find_application
    render "oauth_applications/advanced"
  end

  def beta_features
    render "oauth_applications/beta_features"
  end

  private

  def beta_features_available?
    Apps::BetaFeatureComponent::BETA_FEATURES.any? do |_, hash|
      global_flag = hash[:global_flag]
      GitHub.flipper[global_flag].enabled?(current_application.user)
    end
  end

  def current_application
    return @application if defined?(@application)
    @application = find_application
  end

  def populate(params, application)
    hash = params[:oauth_application] || {}
    application.name = hash[:name] if hash[:name]
    application.bgcolor = hash[:bgcolor] if hash[:bgcolor]
    application.url = hash[:url].strip if hash[:url]
    application.description = hash[:description] if hash[:description]
    application.callback_url = hash[:callback_url].strip if hash[:callback_url]
    if hash[:callback_urls]
      application.set_callback_urls(
        hash[:callback_urls].map { |callback_url| callback_url.strip },
      )
    end

    if logo_id = hash.fetch(:logo_id, nil)
      application.logo = OauthApplicationLogo.find_by(id: logo_id, uploader_id: current_user.id)
    end

    application
  end

  def find_application
    current_context.oauth_applications.find(params[:id])
  end

  def check_apps_creation_limit
    if current_context.reached_applications_creation_limit?(application_type: OauthApplication)
      flash[:error] = MAX_APPS_FLASH_ERROR_MESSAGE

      if current_context.is_a? Organization
        redirect_to settings_org_applications_path(current_context)
      else
        redirect_to settings_user_developer_applications_path
      end
    end
  end
end
