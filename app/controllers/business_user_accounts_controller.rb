# frozen_string_literal: true

class BusinessUserAccountsController < ApplicationController
  include EnterpriseAccountStatsHelper
  include BusinessesHelper

  PAGE_SIZE = 30

  before_action :business_admin_required
  before_action :dotcom_required
  before_action :saml_enabled_required, only: %i(sso unlink_identity revoke_sso_session revoke_sso_token)
  before_action :validate_user, only: %i(unlink_identity revoke_sso_session revoke_sso_token)
  around_action :register_enterprise_account_controller_stats

  def organizations
    query_args = parse_query_string \
      query_param, filter_map: BusinessesHelper::USER_ACCOUNT_MEMBERSHIP_QUERY_FILTERS
    organizations = business_user_account.enterprise_organizations(
      current_user,
      query: query_args[:query],
      order_by_field: "login",
      order_by_direction: "ASC",
      org_member_type: ::BusinessUserAccount.org_member_type_from_role(query_args[:role]),
    ).paginate(page: current_page, per_page: PAGE_SIZE)
    outside_collaborator_repositories_count = \
      business_user_account&.user&.outside_collaborator_repositories(business: this_business)&.count || 0
    installation_count = business_user_account.user_enterprise_installations.count

    if request.xhr?
      headers["Cache-Control"] = "no-cache, no-store"
      render partial: "business_user_accounts/organizations_list", locals: {
        user_account: business_user_account,
        organizations: organizations,
        query: query_param,
      }
    else
      pending_invitations_count = this_business.pending_member_invitations(login: business_user_account.login).count
      render "business_user_accounts/organizations", locals: {
        user_account: business_user_account,
        organizations: organizations,
        outside_collaborator_repositories_count: outside_collaborator_repositories_count,
        installation_count: installation_count,
        pending_invitations_count: pending_invitations_count,
        query: query_param,
      }
    end
  end

  def enterprise_installations
    query_args = parse_query_string \
      query_param, filter_map: BusinessesHelper::USER_ACCOUNT_MEMBERSHIP_QUERY_FILTERS
    installations = business_user_account.user_enterprise_installations(
      query: query_args[:query],
      order_by_field: "host_name",
      order_by_direction: "ASC",
      role: query_args[:role],
    ).paginate(page: current_page, per_page: PAGE_SIZE)
    organization_count = business_user_account.enterprise_organizations(current_user).count
    outside_collaborator_repositories_count = \
      business_user_account&.user&.outside_collaborator_repositories(business: this_business)&.count || 0

    if request.xhr?
      headers["Cache-Control"] = "no-cache, no-store"
      render partial: "business_user_accounts/enterprise_installations_list", locals: {
        user_account: business_user_account,
        installations: installations,
        query: query_param,
      }
    else
      render "business_user_accounts/enterprise_installations", locals: {
        user_account: business_user_account,
        installations: installations,
        organization_count: organization_count,
        outside_collaborator_repositories_count: outside_collaborator_repositories_count,
        query: query_param,
      }
    end
  end

  def sso
    organization_count = business_user_account.enterprise_organizations(current_user).count
    installation_count = business_user_account.user_enterprise_installations.count
    outside_collaborator_repositories_count = \
      business_user_account&.user&.outside_collaborator_repositories(business: this_business)&.count || 0

    render_template_view "business_user_accounts/sso", Sso::ShowView,
      {
        target: business_user_account.business,
        member: business_user_account.user || business_user_account.login,
        business_user_account: business_user_account,
      },
      locals: {
        user_account: business_user_account,
        organization_count: organization_count,
        installation_count: installation_count,
        outside_collaborator_repositories_count: outside_collaborator_repositories_count,
      }
  end

  def unlink_identity
    ExternalIdentity.unlink(
      provider: this_business.saml_provider,
      user: person,
      perform_instrumentation: true,
      instrumentation_payload: {
        actor: current_user,
        user: person,
      },
    )

    flash[:notice] = "External identity for #{person} successfully revoked."
    redirect_to sso_enterprise_user_account_path(business_user_account.id)
  end

  # Action: Revokes an active SAML SSO session for the member.
  def revoke_sso_session
    external_identity_session = ExternalIdentitySession.by_saml_provider(this_business.saml_provider).
                                                        find_by(id: params[:session_id])
    if external_identity_session&.user_session&.user_id == person.id
      external_identity_session.destroy
      this_business.instrument_sso_session_revoked(actor: current_user, user: person)
    end

    if request.xhr?
      head :ok
    else
      flash[:notice] = "SAML session successfully revoked."
      redirect_to sso_enterprise_user_account_path(business_user_account.id)
    end
  end

  # Action: Revokes whitelisted personal access token for the intersection of
  # organizations which the token is assigned to, belong to this business,
  # and the current_user has admin rights over. Without the whitelist,
  # this token will not be able access private data via the API or Git for its
  # organization.
  def revoke_sso_token
    credential_type = params[:credential_type]
    authorizations = if credential_type == "OauthAccess"
      token = person.oauth_accesses.find_by_id(params[:token_id])
      token.credential_authorizations unless token.nil?
    elsif credential_type == "PublicKey"
      token = person.public_keys.find_by_id(params[:token_id])
      token.active_org_credential_authorizations unless token.nil?
    end
    return render_404 if authorizations.nil?

    (authorizations.map(&:organization) &
        current_user.owned_organizations &
        this_business.organizations).each do |organization|
      Organization::CredentialAuthorization.revoke \
        organization: organization,
        credential: token,
        actor: current_user
    end

    flash[:notice] = "API and Git access successfully revoked."
    redirect_to sso_enterprise_user_account_path(business_user_account.id)
  end

  private

  def target_for_conditional_access
    return :no_target_for_conditional_access unless this_business
    this_business
  end

  def this_business
    business_user_account.business
  end

  def business_admin_required
    render_404 unless business_user_account.business.owner?(current_user)
  end

  def business_user_account
    @business_user_account ||= BusinessUserAccount.find(params[:id])
  end

  def query_param
    return @query if defined?(@query)
    @query = params[:query]
  end

  def saml_enabled_required
    render_404 unless this_business.saml_sso_enabled?
  end

  def validate_user
    if person.nil?
      flash["error"] = "This action cannot be performed because there is no GitHub.com user associated with this person."
      redirect_to sso_enterprise_user_account_path(business_user_account.id)
    end
  end

  def person
    @person ||= business_user_account.user
  end
end
