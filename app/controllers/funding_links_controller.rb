# frozen_string_literal: true

class FundingLinksController < AbstractRepositoryController
  ShowQuery = parse_query <<~'GRAPHQL'
    query($sponsorableUsersIds: [ID!]!, $sponsorableOrgsIds: [ID!]!) {
      ...Views::FundingLinks::Links::Root
    }
  GRAPHQL

  def show
    respond_to do |format|
      format.html do
        if !request.xhr? && params[:fragment].to_i != 1
          render_404
          return
        end

        data = platform_execute(ShowQuery, variables: {
          sponsorableUsersIds:    funding_links.sponsorable_users_ids,
          sponsorableOrgsIds:     funding_links.sponsorable_orgs_ids,
        })

        render partial: "funding_links/links",
               layout: false,
               locals: {
                 funding_links: funding_links,
                 data: data,
               }
      end
    end
  end

  private

  def funding_links
    @funding_links ||= current_repository.funding_links
  end
end
