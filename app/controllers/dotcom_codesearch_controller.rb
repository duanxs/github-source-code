# frozen_string_literal: true

class DotcomCodesearchController < CodesearchController
  areas_of_responsibility :search

  before_action :disable_for_fedramp_private_instance
  before_action :ensure_dotcom_search_enabled

  def index
    respond_to do |format|
      format.html do

        return render_404 if page > 100

        # for the view
        @language = language
        @search   = sanitized_query
        @state    = params[:state]
        @package_type = params[:package_type]
        @sort     = get_sort
        @order    = get_order

        if @search.present?
          @type = params[:type] = queries.search_type

          if get_query.include? "environment:local"
            return redirect_to search_url(q: @search)
          end

          GitHub.dogstats.increment("search", tags: ["client:mobile"]) if mobile?
          render "dotcom_codesearch/results", locals: { scope: :global }
        else
          @type = type
          render "codesearch/index"
        end
      end
    end
  end

  def advanced_search
    @language = language
    @search   = sanitized_query
    render "dotcom_codesearch/advanced_search"
  end

  def queries
    @queries ||= Search::QueryHelper.new(sanitized_query, type,
        current_user: current_user,
        remote_ip: request.remote_ip,
        page: page,
        per_page: per_page,
        user_session: user_session
    )
  end

private
  def ensure_dotcom_search_enabled
    render_404 if !GitHub::Connect.unified_search_enabled?
  end

  def page
    current_page(:p)
  end
end
