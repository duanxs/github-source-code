# frozen_string_literal: true

class CodeNavController < AbstractRepositoryController
  map_to_service :code_navigation

  def definition
    language = params[:language] || params[:lang]
    blob_path = params[:blob_path] || params[:context]
    return head 204 unless aleph_code_navigation_available? || aleph_darkship_language?(language)

    commit_oid = current_repository.ref_to_sha(params[:ref])
    return head 400 unless commit_oid

    locations, backend = if aleph_use_lsp_proxy?
      response = GitHub::Aleph.text_document_definition(
        repo: current_repository,
        commit_oid: commit_oid,
        path: blob_path,
        row: params[:row].to_i,
        col: params[:col].to_i,
      )
      return head 204 if response.nil? || response.error || aleph_darkship_language?(language)

      locations = response.data.locations
      [locations, response.data.backend]
    else
      response = GitHub::Aleph.find_symbols(
        repo_id: current_repository.id,
        sha: commit_oid,
        query: params[:q],
        language: language,
        is_public: current_repository.public?
      )
      return head 204 if response.nil? || response.error || aleph_darkship_language?(language)

      locations = response.data.tags.map do |tag|
        Aleph::Proto::Location.new({
          first_line: tag.line,
          # NB: Convert to zero-based indexing
          range: {start: {line: tag.row - 1, character: tag.column - 1}, end: {line: 0, character: 0}},
          path: tag.path,
          kind: tag.kind,
          uri: "file:///#{tag.path}",
        })
      end
      [locations, :ALEPH_FUZZY]
    end

    repo_visibility = current_repository.public ? "public" : "private"
    GitHub.dogstats.increment("symbol_search", tags: ["visibility:#{repo_visibility}", "language:#{language}"])
    GitHub.dogstats.count("symbol_search.#{repo_visibility}.#{language}.tag_count", locations.length)

    return head 204 if aleph_darkship_language?(language)

    locations_by_path = locations.each_with_object({}) do |loc, hash|
      hash[loc.path] ||= []
      hash[loc.path] << loc
    end

    respond_to do |format|
      format.html do
        locals = {
          locations: locations,
          locations_by_path: locations_by_path,
          backend: backend,
          language: language,
          commit_oid: commit_oid,
          ref: params[:ref],
          blob_path: blob_path,
          query: params[:q],
          row: params[:row].to_i,
          col: params[:col].to_i,
        }
        render "code_navigation/show", layout: false, locals: locals
      end
    end
  end

  def references
    language = params[:language] || params[:lang]
    blob_path = params[:blob_path] || params[:context]
    return head 204 unless aleph_code_navigation_available? || aleph_darkship_language?(language)

    commit_oid = current_repository.ref_to_sha(params[:ref])
    return head 400 unless commit_oid

    locations, backend = if aleph_use_lsp_proxy?
      response = GitHub::Aleph.text_document_references(
        repo: current_repository,
        commit_oid: commit_oid,
        path: blob_path,
        row: params[:row].to_i,
        col: params[:col].to_i,
      )
      return head 204 if response.nil? || response.error || aleph_darkship_language?(language)

      locations = response.data.locations
      [locations, response.data.backend]
    else
      response = GitHub::Aleph.find_symbol_references(
        repo_id: current_repository.id,
        sha: commit_oid,
        query: params[:q],
        language: language,
        is_public: current_repository.public?
      )
      return head 204 if response.nil? || response.error || aleph_darkship_language?(language)

      locations = response.data.tags.map do |tag|
        Aleph::Proto::Location.new({
          first_line: tag.line,
          # NB: Convert to zero-based indexing
          range: {start: {line: tag.row - 1, character: tag.column - 1}, end: {line: 0, character: 0}},
          path: tag.path,
          kind: tag.kind,
          uri: "file:///#{tag.path}",
        })
      end
      [locations, :ALEPH_FUZZY]
    end

    locations_by_path = locations.each_with_object({}) do |loc, hash|
      hash[loc.path] ||= []
      hash[loc.path] << loc
    end

    respond_to do |format|
      format.html do
        render "code_navigation/references", layout: false, locals: {
          locations: locations,
          locations_by_path: locations_by_path,
          backend: backend,
          ref: params[:ref],
          language: language,
          commit_oid: commit_oid,
        }
      end
    end
  end
end
