# frozen_string_literal: true

class NotificationsV2Controller < ApplicationController
  layout "notifications_v2"

  before_action :login_required
  before_action :redirect_to_preferred_inbox_view, only: [:index]
  before_action :require_selected_summaries_or_mark_all, only: [:mark_as_read, :mark_as_unread, :mark_as_archived]
  before_action :require_selected_summaries, only: [:mark_as_unarchived, :mark_as_subscribed, :mark_as_unsubscribed, :mark_as_starred, :mark_as_unstarred]
  before_action :require_xhr, only: [:create_custom_inbox, :update_custom_inbox, :delete_custom_inbox, :custom_inboxes_dialog]
  skip_before_action :perform_conditional_access_checks, only: [:index, :mark_as_read, :mark_as_unread, :mark_as_archived, :mark_as_unarchived, :mark_as_subscribed, :mark_as_unsubscribed, :mark_as_starred, :mark_as_unstarred, :recent_notifications_alert, :notifications_exist]

  statsd_tag_actions only: [:index]

  REFERRER_PARAMS = [
    :notification_referrer_id,
    :notifications_after,
    :notifications_before,
    :notifications_query,
  ]

  PAGE_SIZE = 25
  RECENT_NOTIFICATIONS_MAX_COUNT = 10

  def index
    # Force page to be refreshed after pressing the back button
    headers["Cache-Control"] = "no-cache, no-store"

    variables = graphql_pagination_params(page_size: PAGE_SIZE)
    if Notifications::V2::IndexView.user_viewing_notifications_grouped_by_repo?(current_user, parsed_query)
      variables[:notificationListsWithThreadCountFirst] = 100
    else
      variables[:notificationListsWithThreadCountFirst] = 25
    end

    variables[:UnwatchSuggestionsEnabled] = GitHub.flipper[:unwatch_suggestions].enabled?(current_user)

    data = platform_execute(
      IndexQuery,
      variables: variables.merge(graphql_filter_variables)
    )

    render_params = {
      # Don't render any layout for XHR update requests
      layout: (request.xhr? && !pjax?) ? false : nil,

      locals: {
        viewer: data.viewer,
        view: Notifications::V2::IndexView.new(
          index_view_params.merge(
            viewer: data.viewer,
            requester: data.requester
          )
        ),
      },
    }.compact

    render("notifications/v2/index", render_params)
  end

  def set_preferred_inbox_query
    if parsed_query.is_only_an_inbox_query?
      current_user.set_preferred_notifications_query(parsed_query.query)
    end

    redirect_to notifications_beta_path(query: parsed_query.query)
  end

  def update_view_preference
    if params[:view_preference] == "group_by_repository"
      current_user.set_prefers_notifications_group_by_list_view
    elsif params[:view_preference] == "sort_by_date"
      current_user.unset_prefers_notifications_group_by_list_view
    end

    redirect_to notifications_beta_path(query: parsed_query.query)
  end

  def mark_as_read
    return mark_all(:read) if mark_all_request?

    unless GitHub.newsies.web.mark_summaries_as_read(current_user, selected_summary_ids).success?
      return head :service_unavailable
    end

    log_action_to_hydro(:read)
    head :ok
  end

  def mark_as_unread
    return mark_all(:unread) if mark_all_request?

    unless GitHub.newsies.web.mark_summaries_as_unread(current_user, selected_summary_ids).success?
      return head :service_unavailable
    end

    log_action_to_hydro(:unread)
    head :ok
  end

  def mark_as_archived
    return mark_all(:archived) if mark_all_request?

    unless GitHub.newsies.web.mark_summaries_as_archived(current_user, selected_summary_ids).success?
      return head :service_unavailable
    end

    log_action_to_hydro(:archive)
    head :ok
  end

  def mark_as_unarchived
    unless GitHub.newsies.web.mark_summaries_as_read(current_user, selected_summary_ids).success?
      return head :service_unavailable
    end

    log_action_to_hydro(:unarchive)
    head :ok
  end

  def mark_as_subscribed
    selected_notification_threads.each do |notification_thread|
      newsies_thread = to_newsies_thread(notification_thread)
      unless GitHub.newsies.subscribe_to_thread(current_user, newsies_thread.list, newsies_thread, "manual").success?
        return head :service_unavailable
      end
    end

    unless GitHub.newsies.web.mark_summaries_as_read(current_user, selected_summary_ids).success?
      return head :service_unavailable
    end

    head :ok
  end

  def mark_as_unsubscribed
    selected_notification_threads.each do |notification_thread|
      newsies_thread = to_newsies_thread(notification_thread)
      case notification_thread.subscription_status
      when "LIST_SUBSCRIBED"
        unless GitHub.newsies.ignore_thread(current_user, newsies_thread.list, newsies_thread).success?
          return head :service_unavailable
        end
      when "THREAD_SUBSCRIBED", "THREAD_TYPE_SUBSCRIBED"
        unless GitHub.newsies.unsubscribe(current_user, newsies_thread.list, newsies_thread).success?
          return head :service_unavailable
        end
      end
    end

    unless GitHub.newsies.web.mark_summaries_as_archived(current_user, selected_summary_ids).success?
      return head :service_unavailable
    end

    log_action_to_hydro(:unsubscribe)
    head :ok
  end

  def mark_as_starred
    selected_notification_threads.each do |notification_thread|
      next if notification_thread.is_starred

      newsies_thread = to_newsies_thread(notification_thread)
      unless GitHub.newsies.web.save_thread(current_user, newsies_thread).success?
        return head :service_unavailable
      end
    end

    log_action_to_hydro(:star)
    head :ok
  end

  def mark_as_unstarred
    selected_notification_threads.each do |notification_thread|
      next unless notification_thread.is_starred

      newsies_thread = to_newsies_thread(notification_thread)
      unless GitHub.newsies.web.unsave_thread(current_user, newsies_thread).success?
        return head :service_unavailable
      end
    end

    log_action_to_hydro(:unstar)
    head :ok
  end

  RecentNotificationsAlertQuery = parse_query <<-'GRAPHQL'
    query($maxCount: Int!, $filterBy: NotificationThreadFilters, $query: String) {
      viewer {
        ...Views::Notifications::V2::RecentNotificationsAlert
      }
    }
  GRAPHQL

  def recent_notifications_alert
    return head :not_found unless params[:since]

    data = platform_execute(RecentNotificationsAlertQuery, variables: {
      maxCount: RECENT_NOTIFICATIONS_MAX_COUNT + 1,
    }.merge(graphql_filter_variables))

    render partial: "notifications/v2/recent_notifications_alert", locals: {
      viewer: data.viewer,
      since: Time.at(params[:since].to_i),
      max_count: RECENT_NOTIFICATIONS_MAX_COUNT,
      view: Notifications::V2::IndexView.new(index_view_params),
    }
  end

  NotificationsExistQuery = parse_query <<-'GRAPHQL'
    query($filterBy: NotificationThreadFilters, $query: String) {
      viewer {
        notificationThreads(first: 1, filterBy: $filterBy, query: $query) {
          nodes {
            id
          }
        }
      }
    }
  GRAPHQL

  # Checks if any notifications exist for the current query
  def notifications_exist
    data = platform_execute(NotificationsExistQuery, variables: graphql_filter_variables)

    return head :service_unavailable if data.errors.any?

    respond_to do |format|
      format.json do
        render json: { notifications_exist: data.viewer.notification_threads.nodes.first.present? }
      end
    end
  end

  CustomInboxesDialogQuery = parse_query <<-'GRAPHQL'
    query {
      viewer {
        ...Views::Notifications::V2::CustomInboxesDialogContents
      }
    }
  GRAPHQL

  def custom_inboxes_dialog
    data = platform_execute(CustomInboxesDialogQuery)
    render partial: "notifications/v2/custom_inboxes_dialog_contents", locals: { viewer: data.viewer }
  end

  CreateCustomInboxMutation = parse_query <<-'GRAPHQL'
    mutation($input: CreateCustomInboxInput!) {
      createCustomInbox(input: $input) {
        customInbox {
          id
          name
          queryString
        }
      }
    }
  GRAPHQL

  def create_custom_inbox
    input = custom_inbox_params
    data = platform_execute(
      CreateCustomInboxMutation,
      variables: {
        input: {
          name: input["name"],
          queryString: input["query_string"],
        },
      },
    )

    if data.errors.any?
      error_type = data.errors.details[:createCustomInbox]&.first["type"]
      return head :bad_request if %w[UNPROCESSABLE].include?(error_type)
    end

    head :created
  end

  UpdateCustomInboxMutation = parse_query <<-'GRAPHQL'
    mutation($input: UpdateCustomInboxInput!) {
      updateCustomInbox(input: $input) {
        customInbox {
          id
          name
          queryString
        }
      }
    }
  GRAPHQL

  def update_custom_inbox
    input = custom_inbox_params
    data = platform_execute(
      UpdateCustomInboxMutation,
      variables: {
        input: {
          customInboxId: input["id"],
          name: input["name"],
          queryString: input["query_string"],
        },
      },
    )

    if data.errors.any?
      error_type = data.errors.details[:updateCustomInbox]&.first["type"]
      return head :bad_request if %w[UNPROCESSABLE].include?(error_type)
      return head :not_found if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)
    end

    head :ok
  end

  DeleteCustomInboxMutation = parse_query <<-'GRAPHQL'
    mutation($input: DeleteCustomInboxInput!) {
      deleteCustomInbox(input: $input) {
        success
      }
    }
  GRAPHQL

  def delete_custom_inbox
    input = custom_inbox_params
    data = platform_execute(
      DeleteCustomInboxMutation, variables: { input: { customInboxId: input["id"] } }
    )

    if data.errors.any?
      error_type = data.errors.details[:deleteCustomInbox]&.first["type"]
      return head :not_found if %w[NOT_FOUND FORBIDDEN SERVICE_UNAVAILABLE].include?(error_type)
    end

    head :ok
  end

  SUGGESTED_REPOSITORIES_QUERY = parse_query <<-'GRAPHQL'
    query {
      viewer {
        notificationListsWithThreadCount(first: 100, statuses: [UNREAD, READ, ARCHIVED], listTypes: REPOSITORY) {
          nodes {
            list {
              ... on Repository {
                nameWithOwner
              }
            }
          }
        }
      }
    }
  GRAPHQL

  def suggested_repositories
    data = platform_execute(SUGGESTED_REPOSITORIES_QUERY)

    return head :service_unavailable if data.errors.any?

    lists = data.viewer.notification_lists_with_thread_count.nodes.map do |node|
      { value: node.list.name_with_owner }
    end

    respond_to do |format|
      format.json do
        render json: lists
      end
    end
  end

  def shelf
    render partial: "notifications/v2/top_shelf"
  end

  def dismiss_unwatch_suggestion_alert
    return render_404 unless GitHub.flipper[:unwatch_suggestions].enabled?(current_user)

    current_user.set_dismissed_unwatch_suggestions
    head :ok
  end

  private

  def custom_inbox_params
    params.require(:custom_inbox).permit(:id, :name, :query_string)
  end

  def mark_all_request?
    params[:mark_all] == "1"
  end

  def mark_all(state)
    # query parameter must be set to something, even if it's empty string
    return head(:bad_request) if params[:query].nil?
    return head(:forbidden) if parsed_query.contains_unsupported_qualifiers?

    mark_at = Time.now.utc

    filter_options = {}
    filter_options[:before] = mark_at
    filter_options[:thread_types] = filter_by_thread_types
    filter_options[:lists] = filter_by_list_ids

    filter_options[:reasons] = if filter_by_reasons
      parsed_query.reasons.map(&:to_sym)
    end

    filter_options[:statuses] = if filter_by_statuses.empty?
      # If a user is in the `All` view, we want everything that is not archived
      [:unread, :read]
    else
      # Otherwise, use whatever statuses they have in their query
      parsed_query.statuses.map(&:to_sym)
    end

    filter_options.compact!

    # We only allow the user to run one mark_all_from_query job at a time.
    # If there is an existing job running, return with 409 and let the user know.
    status = Newsies::MarkAllNotificationsFromQueryJobStatus.status(current_user.id)
    return head :conflict if status && !status.finished?

    unless GitHub.newsies.web.mark_all_from_query(current_user, state, filter_options).success?
      return head :service_unavailable
    end

    # Given we are processing this request in a background job, we use the accepted status code here
    # instead of success. The consumer can then use the JobStatus generated by the job to check when it's complete.
    # i.e. Newsies::MarkAllNotificationsFromQueryJobStatus.status(current_user.id)
    #
    # We use this on the client to decide if we need to display the "processing" toast to the user.
    head :accepted
  end

  def log_action_to_hydro(action)
    summary_response = GitHub.newsies.web.find_rollup_summaries_by_ids(selected_summary_ids)

    if summary_response.success?
      summary_response.value.each do |summary|
        GlobalInstrumenter.instrument(
          "notifications.#{action}",
          {
            list_type: summary.newsies_list.type,
            list_id: summary.newsies_list.id,
            thread_type: summary.newsies_thread.type,
            thread_id: summary.newsies_thread.id,
            comment_type: summary.last_comment.type,
            comment_id: summary.last_comment.id,
            handler: :web,
            user: current_user,
            version: :v2,
          },
        )
      end
    end
  end

  def require_selected_summaries_or_mark_all
    # We don't need the selected summaries for marking all from a query,
    # but we do want them for individual and bulk actions
    head :not_found unless selected_summary_ids.present? || mark_all_request?
  rescue PlatformHelper::ConditionalAccessError
    # this is an xhr request, so follow the pattern in
    # https://github.com/github/github/blob/4bf41fdfcdc9b7b30615e5dc7df6ae55bf73c583/app/controllers/application_controller/external_sessions_dependency.rb#L186-L187
    head :unauthorized
  end

  def require_selected_summaries
    head :not_found unless selected_summary_ids.present?
  rescue PlatformHelper::ConditionalAccessError
    # this is an xhr request, so follow the pattern in
    # https://github.com/github/github/blob/4bf41fdfcdc9b7b30615e5dc7df6ae55bf73c583/app/controllers/application_controller/external_sessions_dependency.rb#L186-L187
    head :unauthorized
  end

  def redirect_to_preferred_inbox_view
    # don't redirect XHR only requests
    return if request.xhr? && !pjax?

    # don't redirect if a query is present, even if empty
    return unless params[:query].nil?

    if (preference = current_user.preferred_notifications_query).present?
      redirect_to notifications_beta_path(query: preference)
    end
  end

  IndexQuery = parse_query <<-'GRAPHQL'
    query($first: Int, $last: Int, $before: String, $after: String, $filterBy: NotificationThreadFilters, $query: String, $notificationListsWithThreadCountFirst: Int, $UnwatchSuggestionsEnabled: Boolean!) {
      viewer {
        ...Views::Notifications::V2::Index::Viewer
        ...Notifications::V2::IndexView::NonDefaultTabsFragment
      }

      requester {
        ...Notifications::V2::IndexView::RequesterWithUnwatchSuggestions@include (if: $UnwatchSuggestionsEnabled)
      }
    }
  GRAPHQL

  def index_view_params
    {
      before: params[:before].presence,
      after: params[:after].presence,
      query: parsed_query,
      unauthorized_saml_targets: unauthorized_saml_targets,
      unauthorized_ip_whitelisting_targets: unauthorized_ip_whitelisting_targets,
      user: current_user,
    }
  end

  def target_for_conditional_access
    :no_target_for_conditional_access
  end

  def parsed_query
    @parsed_query ||= Search::Queries::NotificationsQuery.new(query: params[:query], viewer: current_user)
  end

  def notification_search_enabled?
    GitHub.flipper[:notifications_search].enabled?(current_user)
  end

  def graphql_filter_variables
    if notification_search_enabled?
      return { query: params[:query] || "" }
    end

    if parsed_query.contains_unsupported_qualifiers?
      # Filter by empty list of statuses to makes sure we return zero results when the user attempts
      # an (unsupported) full-text search. This is inefficient, but it ensures that the GraphQL
      # response has the right shape.
      return { filterBy: { statuses: [] } }
    end

    {
      filterBy: {
        statuses: filter_by_statuses,
        reasons: filter_by_reasons,
        listIds: filter_by_list_ids,
        starredOnly: parsed_query.starred?,
        threadTypes: filter_by_thread_types,
      }.compact
    }
  end

  def filter_by_reasons
    parsed_query.qualifier_used?(:reason) ? parsed_query.reasons.map(&:upcase) : nil
  end

  def filter_by_list_ids
    parsed_query.qualifier_used?(:repo) ? parsed_query.repositories.map(&:global_relay_id) : nil
  end

  def filter_by_thread_types
    parsed_query.thread_type? ? parsed_query.thread_types : nil
  end

  def filter_by_statuses
    parsed_query.statuses.map(&:upcase).presence
  end

  SelectedNotificationsQuery = parse_query <<-'GRAPHQL'
    query($ids: [ID!]!) {
      nodes(ids: $ids) {
        ... on NotificationThread {
          summaryId
          listId
          listType
          threadType
          threadId
          subscriptionStatus
          isStarred
        }
      }
    }
  GRAPHQL

  # Retrieve selected notification threads for markAs* methods
  #
  # We set { enforce_conditional_access_via_graphql: true } which means the
  # platform_execute call will errors that inherit from
  # PlatformHelper::ConditionalAccessError if any of the listed notifications
  # are in orgs where the user does not meet conditional access requirements.
  def selected_notification_threads
    return @selected_notification_threads if defined? @selected_notification_threads

    variables = {
      ids: Array(params[:notification_ids]).first(PAGE_SIZE),
    }
    @selected_notification_threads = platform_execute(SelectedNotificationsQuery,
      variables: variables, context: { enforce_conditional_access_via_graphql: true }).nodes.compact
  end

  def selected_summary_ids
    @selected_summary_ids ||= selected_notification_threads.map(&:summary_id)
  end

  def to_newsies_thread(notification_thread)
    list = Newsies::List.new(notification_thread.list_type, notification_thread.list_id)
    Newsies::Thread.new(notification_thread.thread_type, notification_thread.thread_id, list: list)
  end
end
