# frozen_string_literal: true
require "github-launch"

class CheckSuitesController < AbstractRepositoryController
  areas_of_responsibility :checks
  before_action :require_push_access, except: [:show_partial, :download_logs, :download_artifact]

  def rerequest
    check_suite = find_check_suite
    return render_404 unless check_suite

    if check_suite.rerequestable
      # We only want to allow re-requestable check suites for XHR requests.
      begin
        # If the check_suite is for actions, only_failed_check_suites is false so that all jobs can be re-run (even successfull ones)
        only_failed_check_suites = !check_suite.actions_app?
        check_suite.rerequest(actor: current_user, only_failed_check_runs: params[:only_failed_check_runs] == "true", only_failed_check_suites: only_failed_check_suites)
      rescue CheckSuite::ExpiredWorkflowRunError
        head :bad_request and return if request.xhr?

        flash[:error] = "Unable to re-run this workflow because it was created over a month ago."
        redirect_to :back and return
      rescue CheckSuite::AlreadyRerunningError
        # If the check suite is already re-running, we want to return a Bad
        # Request.
        head :bad_request and return if request.xhr?

        flash[:error] = "This check suite is already re-running."
        redirect_to :back and return
      rescue CheckSuite::NotRerequestableError
        head :bad_request and return if request.xhr?

        flash[:error] = "Unable to re-run check suite."
        redirect_to :back and return
      end


      head :ok and return if request.xhr?

      checks_name = check_suite.actions_app? ? "jobs" : "checks"
      flash[:notice] = "You have successfully requested #{checks_name} from #{check_suite.github_app.name}."
      redirect_to :back
    else
      head :bad_request and return if request.xhr?

      flash[:error] = "Re-runs for this check suite are disabled."
      redirect_to :back and return
    end

  end

  def show_partial
    return render_404 unless request.xhr?

    check_suite = find_check_suite
    return render_404 unless check_suite

    selected_check_run = check_suite.check_runs.find_by_id(params[:selected_check_run_id].to_i)
    pull = current_repository.pull_requests.find_by_id(params[:pull_id].to_i)
    check_suite_selected = params[:check_suite_selected] == "true"
    check_suite_focus = params[:check_suite_focus] == "true"

    respond_to do |format|
      format.html do
        render partial: "checks/checks_sidebar_item", locals: {
          check_suite: check_suite,
          selected_check_run: selected_check_run,
          check_suite_selected: check_suite_selected,
          check_suite_focus: check_suite_focus,
          pull: pull,
        }
      end
    end
  end

  def cancel
    check_suite = find_check_suite
    return render_404 unless check_suite

    unless check_suite.actions_app?
      flash[:error] = "Cancelling check suites is not enabled for this GitHub app."
      redirect_to :back and return
    end

    return render_404 unless GitHub.actions_enabled?

    if check_suite.completed?
      flash[:error] = "Cannot cancel a check suite that is completed."
      redirect_to :back and return
    end

    result = check_suite.cancel

    if result.call_succeeded?
      flash[:notice] = "You have successfully requested the workflow to be canceled."
    else
      flash[:error] = "Failed to cancel workflow."
    end

    redirect_to :back
  end

  def download_artifact
    return render_404 unless logged_in?

    check_suite = find_check_suite
    return render_404 unless check_suite

    artifact = check_suite.artifacts.find(params[:artifact_id])
    return render_404 unless artifact

    result = GrpcHelper.rescue_from_grpc_errors("Actions") do
      grpc_request = GitHub::Launch::Services::Artifactsexchange::ExchangeURLRequest.new({
        unauthenticated_url: artifact.source_url,
        repository_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: current_repository.global_relay_id),
        resource_type: GitHub::Launch::Services::Artifactsexchange::ResourceType::TYPE_DOWNLOAD_ARTIFACT,
      })

      GitHub.launch_artifacts_exchange_for_check_suite(check_suite).exchange_url(grpc_request)
    end

    if result.call_succeeded?
      GitHub.dogstats.increment("actions.exchange_url_request.succeeded", tags: ["artifacts"])
      redirect_to result.value.authenticated_url, status: 307
    else
      GitHub.dogstats.increment("actions.exchange_url_request.failed", tags: ["artifacts"])
      flash[:error] = "Failed to generate URL to download artifact."
      redirect_back fallback_location: checks_path(ref: check_suite.head_sha)
    end
  end

  def delete_artifact
    check_suite = find_check_suite
    return render_404 unless check_suite

    artifact = check_suite.artifacts.find(params[:artifact_id])
    return render_404 unless artifact

    begin
      artifact.destroy
      flash[:notice] = "The artifact was successfully deleted."
    rescue RuntimeError
      flash[:error] = "Failed to delete the artifact."
    end

    redirect_back fallback_location: checks_path(ref: check_suite.head_sha)
  end

  def download_logs
    return render_404 unless logged_in?

    check_suite = find_check_suite
    return render_404 unless check_suite
    return render_404 unless check_suite.completed_log_url

    result = GrpcHelper.rescue_from_grpc_errors("Actions") do
      grpc_request = GitHub::Launch::Services::Artifactsexchange::ExchangeURLRequest.new({
        unauthenticated_url: check_suite.completed_log_url,
        repository_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: current_repository.global_relay_id),
        resource_type: GitHub::Launch::Services::Artifactsexchange::ResourceType::TYPE_COMPLETED_RUN_LOG,
      })

      GitHub.launch_artifacts_exchange_for_check_suite(check_suite).exchange_url(grpc_request)
    end

    if result.call_succeeded?
      redirect_to result.value.authenticated_url, status: 307
    else
      flash[:error] = "Failed to generate URL to download logs."
      redirect_back fallback_location: checks_path(ref: check_suite.head_sha)
    end
  end

  private
  def require_push_access
    render_404 unless current_user_can_push?
  end

  def find_check_suite
    check_suite = CheckSuite.where(id: params[:id]).first
    if !check_suite || check_suite.repository_id != current_repository.id
      return nil
    end
    check_suite
  end
end
