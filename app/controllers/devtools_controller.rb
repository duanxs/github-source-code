# frozen_string_literal: true

class DevtoolsController < ApplicationController
  areas_of_responsibility :stafftools

  before_action :push_failbot_metadata
  before_action :dotcom_required
  before_action :require_admin_frontend
  before_action :devtools_only
  skip_before_action :perform_conditional_access_checks

  layout "devtools"

  def index
    render "devtools/index"
  end

  def restricted
    flash[:notice] = "Redirected you to #{GitHub.admin_host_name} and… You made it through! Congrats! 🥳"
    render "devtools/index"
  end

  def bounce
    redirect_to "/devtools/#{params[:section]}/#{params[:args].join '/'}"
  end


  protected

  def push_failbot_metadata
    Failbot.push(devtools: true)
  end

  # before_action to restrict access to GitHub developers only
  #
  # Throws a 404 if the current user is not GitHub developer. Shows a special
  # error page for other GitHub employees.
  def devtools_only
    return if github_developer? || site_admin?
    render_403_for_employees
  end
end
