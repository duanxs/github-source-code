# frozen_string_literal: true

class NetworkController < GitContentController
  areas_of_responsibility :network_graph

  statsd_tag_actions only: :dependencies

  skip_before_action :try_to_expand_path
  before_action :redirect_away_from_trailing_slashes, only: [:show]
  before_action :dependency_graphs_enabled?, only: [:dependents, :dependencies]
  before_action :enforce_plan_supports_insights, only: :show
  layout :repository_layout

  # All of the controller actions in this controller are accessed at github.com/:user_id/:repository/network/foo, EXCEPT for the sub_dependencies action
  # That action queries the dependency graph to find out what sub dependencies a given dependency has, so that we can navigate through our dependencies as a tree
  # Because we use the NodesController to load Manifests, we don't always have a user_id and repository in the context where we are accessing the sub_dependnecies action,
  # and we actually don't need them for it to work since it's pulling the dependency we're querying from the params
  # The sub_dependencies action is routed at /repositories/sub_dependencies instead.
  # The above means that we don't have a user_id or a repository which means that we have to skip the authorization actions that rely on them.
  # The sub dependnecies query is only telling us about public information that has nothing to do with the user_id/repository we're routing through so it is fine to skip these checks.
  skip_before_action :ask_the_gitkeeper, only: [:sub_dependencies]
  skip_before_action :authorization_required, only: [:sub_dependencies]

  DependentsQuery = parse_query <<-'GRAPHQL'
    query(
      $repositoryId: ID!,
      $dependentType: DependencyGraphDependentType!,
      $packageId: String,
      $dependentsFirst: Int,
      $dependentsAfter: String,
      $dependentsLast: Int,
      $dependentsBefore: String,
    ) {
      node(id: $repositoryId) {
        ...Views::Network::Dependents::Packages
      }
    }
  GRAPHQL

  DependenciesQuery = parse_query <<-'GRAPHQL'
    query(
      $first: Int,
      $repositoryId: ID!,
      $dependenciesFirst: Int,
      $dependenciesAfter: String,
      $dependenciesPrefers: [String]
    ) {
      node(id: $repositoryId) {
        ...Views::Network::Dependencies::Manifests
      }
    }
  GRAPHQL

  SubDependenciesQuery = parse_query <<-'GRAPHQL'
    query(
      $packageName: String!,
      $packageManager: String!,
      $requirements: String,
      $dependenciesFirst: Int,
      $dependenciesAfter: String
    ) {
      releases: dependencyGraphPackageReleases(
        first: 1,
        packageName: $packageName,
        packageManager: $packageManager,
        requirements: $requirements,
        dependenciesFirst: $dependenciesFirst,
        dependenciesAfter: $dependenciesAfter,
        includeDependencies: true
      ) {
        nodes {
          ...Views::Network::SubDependencies::PackageRelease
        }
      }
    }
  GRAPHQL

  DEPENDENTS_PER_PAGE = 30
  MANIFESTS_PER_PAGE = 100 # Currently, manifest processing is capped at 20
  DEPENDENCIES_PER_PAGE = 100

  def members
    render_template_view "network/members", Networks::MembersView,
      network: current_repository.network
  end

  def dependents
    dependent_type = params[:dependent_type] || default_dependent_type

    variables = {
      repositoryId:     current_repository.global_relay_id,
      packageId:        params[:package_id],
      dependentType:    dependent_type,
    }

    if params[:dependents_before]
      variables[:dependentsBefore] = params[:dependents_before]
      variables[:dependentsLast] = DEPENDENTS_PER_PAGE
    else
      variables[:dependentsAfter] = params[:dependents_after]
      variables[:dependentsFirst] = DEPENDENTS_PER_PAGE
    end

    data = platform_execute(DependentsQuery, variables: variables)

    dependents_unavailable = data.errors.all.any?

    respond_to do |format|
      format.html do
        if repository = data.node
          render "network/dependents", locals: {
            repository:             repository,
            dependent_type:         dependent_type,
            dependents_unavailable: dependents_unavailable,
            package_id:             params[:package_id],
          }
        else
          render_404
        end
      end
    end
  end

  def dependencies
    # Enable fetchPoll for cached manifests
    if request.xhr? && params[:polling] == "true"
      head 200
      return
    end

    dependencies_first_page = 20

    # for repos with many manifests reduce number of dependencies to show per manifest
    if GitHub.flipper[:dependency_graph_max_manifests_high].enabled?(current_repository)
      dependencies_first_page = 5
    end

    data = platform_execute(DependenciesQuery, variables: {
      first:             MANIFESTS_PER_PAGE,
      repositoryId:      current_repository.global_relay_id,
      dependenciesFirst: dependencies_first_page,
      dependenciesafter: params[:after],
      dependenciesPrefers: current_repository.repository_vulnerability_alerts.affected_package_names,
    })

    loading = timedout = unavailable = false
    case data.errors.all.messages["node"]&.first
    when /loading/
      loading = true
    when /timedout/
      timedout = true
    when /unavailable/
      unavailable = true
    end

    respond_to do |format|
      format.html do
        if repository = data.node
          render "network/dependencies", locals: {
            dependencies_unavailable: unavailable,
            dependencies_loading:     loading,
            dependencies_timedout:    timedout,
            repository:               repository,
            commit:                   current_commit,
            user:                     current_user,
          }
        else
          render_404
        end
      end
    end
  end

  def sub_dependencies
    data = platform_execute(SubDependenciesQuery, variables: {
      packageName:       params[:package_name],
      packageManager:    params[:package_manager],
      requirements:      params[:requirements],
      dependenciesFirst: DEPENDENCIES_PER_PAGE,
      dependenciesAfter: params[:dependencies_after],
    })

    respond_to do |format|
      format.html do
        if release = data.releases.nodes.first
          render "network/sub_dependencies", layout: false, locals: {
            release: release,
            indent:  params[:indent],
            user:    current_user,
          }
        else
          render_404
        end
      end
    end
  end

  def show
    check_update_network_graph
    render "network/show"
  end

  def meta
    # Clients can poll every 10s.
    expires_in 10.seconds

    if network_graph.current?
      render json: network_graph.meta_json
    else
      check_update_network_graph
      head 202
    end
  end

  def chunk
    # Clients can poll every 10s.
    expires_in 10.seconds

    start_time = params[:start] ? params[:start].to_i : nil
    end_time   = params[:end] ? params[:end].to_i : nil
    if network_graph.current?
      render json: (network_graph.data_json(start_time, end_time))
    else
      check_update_network_graph
      render json: {}
    end
  end

  private

  def dependency_graphs_enabled?
    render_404 unless GitHub.dependency_graph_enabled?
    GitHub.context.push(is_public: current_repository.public?)
    Audit.context.push(is_public: current_repository.public?)
  end

  def default_dependent_type
    PlatformTypes::DependencyGraphDependentType::REPOSITORY
  end

  def network_graph
    @network_graph ||= current_repository.network_graph
  end
  helper_method :network_graph

  def check_update_network_graph
    graph = current_repository.network_graph
    graph.build
  end

  def enforce_plan_supports_insights
    return if current_repository.plan_supports?(:insights)
    redirect_to network_members_path(current_repository.owner, current_repository)
  end
end
