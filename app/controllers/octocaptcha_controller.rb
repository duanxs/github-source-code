# frozen_string_literal: true

class OctocaptchaController < ApplicationController
  before_action :set_octocaptcha_csp, except: [:test]
  before_action :clear_cookies
  layout false

  def index
    # Octocaptchas can be served to logged out users too. In order to
    # experiment with this, we will be "dark shipping" this to a
    # percentage of users. The only way to do this is to use the
    # ghost user to check if the feature is enabled for the request.
    # That way we can serve the captcha to a percentage of our logged out
    # users.
    user = current_user || User.ghost

    @target_origin = target_origin
    @origin_page = params[:origin_page] || "origin_page_unknown"
    @responsive = params[:responsive] == "true"
    @nojs = params[:nojs] == "true"
    if GitHub.flipper[:octocaptcha].enabled?(user)
      GitHub.dogstats.increment "octocaptcha.requested", tags: ["response_code:200"]
      render "octocaptcha/index"
    else
      GitHub.dogstats.increment "octocaptcha.requested", tags: ["response_code:404"]
      render plain: "404 Not Found", status: 404
    end
  end

  def test
    render "octocaptcha/test"
  end

  def not_found
    render plain: "404 Not Found", status: 404
  end

  private

  def target_origin
    # Allow origin to be from review-lab and staging environments
    origin = params[:origin].to_s
    return origin if Rails.env.development?
    return origin if origin.end_with?(".github.com")
    return origin if origin.end_with?(".githubapp.com")

    GitHub.url
  end

  # octocaptcha is always public
  def require_conditional_access_checks?
    false
  end

  def clear_cookies
    cookies.clear
  end
end
