# frozen_string_literal: true
require "asset_bundles"

class SiteController < ApplicationController
  areas_of_responsibility :unassigned

  # As of 2019-04-16, status_lolrails is averaging around 510 rq/sec, or a bit
  # under 3% of total requests
  set_statsd_sample_rate 0.01, only: :status_lolrails
  statsd_tag_actions only: :status_lolrails

  include ActionView::Helpers::NumberHelper
  include TextHelper
  include SiteHelper
  include Site::CustomerStoriesFeedHelper
  include EscapeHelper

  before_action :employee_only, only: [:contexttown, :sciencetown, :sleeptown, :custom_sleeptown, :roletown, :report_bug, :componenttown, :toggle_canary]
  before_action :dotcom_required, only: [:pricing, :features]
  before_action :spam_counter_measures, only: :send_contact
  before_action :disable_hydro_request_logging, only: [:sha, :status_lolrails]

  skip_before_action :employee_only_unicorn, only: [:sha, :status_lolrails]
  skip_before_action :perform_conditional_access_checks
  if GitHub.enterprise?
    skip_before_action :first_run_check, only: [:sha, :status_lolrails]
    skip_before_action :license_expiration_check, only: [:status_lolrails]
    skip_before_action :private_instance_bootstrap_check, only: [:sha, :status_lolrails]
  end
  skip_after_action :block_non_xhr_json_responses, only: [:microsoft_identity_association_json]

  layout "site"

  # Allow frames from youtube/ustream/vimeo for marketing pages
  # and allow for <video> elements to be embedded
  before_action :add_csp_exceptions, only: %w(
    business
    personal
    open_source
    features
    feature_code_review
    feature_integrations
    developer_stories_alicia
    developer_stories_amy
    developer_stories_lisa
    developer_stories_mario
    building_the_future
    building_the_future_tiffani
    building_the_future_conrad
    building_the_future_julius
    building_the_future_jamica
    index
    enterprise
    contact
    codespaces
    feature_packages
    learn_security
    learn_devops
  )
  CSP_EXCEPTIONS = {
    frame_src: ["https://www.youtube.com", "https://www.ustream.tv", "https://player.vimeo.com", GitHub.urls.octocaptcha_host_name],
    form_action: %w( https://github.us11.list-manage.com ),
    media_src: [GitHub.asset_host_url],
    img_src: [ExploreFeed::CustomerStory::CUSTOMER_STORIES_FEED_URL],
  }

  RATELIMITED_ERROR =
    "We've received too many requests from your IP address recently. Please wait a few minutes, then try again."


  IndexQuery = parse_query <<-'GRAPHQL'
    query($loggedIn: Boolean!) {
      ...Views::Site::Index::Root
    }
  GRAPHQL

  def index
    data = platform_execute(IndexQuery, variables: { loggedIn: logged_in? })
    render "site/index", locals: { data: data }
  end

  def pricing
    render "site/pricing"
  end

  def features
    render "site/features"
  end

  def feature_code_review
    render "site/feature_code_review"
  end

  def feature_project_management
    render "site/feature_project_management"
  end

  def feature_integrations
    render "site/feature_integrations"
  end

  def feature_insights
    if github_one_launched? && GitHub.flipper[:insights_landing_page].enabled?(current_user)
      render "site/feature_insights"
    else
      render_404
    end
  end

  def feature_security
    render "site/feature_security"
  end

  def feature_actions
   render "site/feature_actions"
  end

  def feature_package_registry
    redirect_to features_packages_path
  end

  def feature_packages
    render "site/feature_packages"
  end

  def learn_devops
    render "site/learn/devops"
  end

  def learn_security
    render "site/learn/security"
  end

  def mobile
    render "site/mobile"
  end

  def universe_2016
    redirect_to "/features"
  end

  def codespaces
    render "site/codespaces"
  end

  def coming_soon
    render "site/coming_soon"
  end

  def forrester
    render "site/forrester"
  end

  def personal
    render "site/personal"
  end

  def tenyears
    redirect_to "/"
  end

  def relaunch_styles
    if site_relaunch_enabled?
      render "site/relaunch_styles"
    else
      render_404
    end
  end

  def building_the_future
    render "site/campaigns/building_the_future"
  end

  def building_the_future_tiffani
    render "site/campaigns/building_the_future_tiffani"
  end

  def building_the_future_conrad
    render "site/campaigns/building_the_future_conrad"
  end

  def building_the_future_julius
    render "site/campaigns/building_the_future_julius"
  end

  def building_the_future_jamica
    render "site/campaigns/building_the_future_jamica"
  end

  def open_source
    render "site/open_source"
  end

  def business
    redirect_to("https://github.com/enterprise")
  end

  def business_security
    redirect_to("https://github.com/enterprise/security")
  end

  def enterprise
    render "site/enterprise"
  end

  def team
    render "site/team"
  end

  def enterprise_security
    render "site/enterprise_security"
  end

  def enterprise_cloud_trial
    render "site/enterprise_cloud_trial", layout: "site_chromeless"
  end

  def business_features
    redirect_to "/pricing#compare"
  end

  def security
    render "site/security"
  end

  def security_trust
    render "site/security_trust"
  end

  def security_team
    render "site/security_team"
  end

  def security_incident
    render "site/security_incident"
  end

  def security_txt
    render plain: <<~EOF
      Contact: https://hackerone.com/github
      Acknowledgments: https://bounty.github.com/bounty-hunters.html
      Preferred-Languages: en
      Canonical: https://github.com/.well-known/security.txt
      Policy: https://bounty.github.com
    EOF
  end

  def choose_team
    render "site/choose_team" , layout: "site_chromeless"
  end

  # This path is used to verify the github.com domain as the owner of the
  # GitHub Team Sync Azure application.
  def microsoft_identity_association_json
    render content_type: "application/json", json: <<~JSON
    {
      "associatedApplications": [
        {
          "applicationId": "c3629460-0f4b-4a5d-9da5-6be011f495f5"
        }
      ]
    }
    JSON
  end

  def logos
    render "site/logos"
  end

  def netneutrality
    render "site/netneutrality"
  end

  def premium_support
    render "site/premium_support"
  end

  def routing_error
    render_404
  end

  def business_try
    unless params[:code].present?
      return render_404
    end
    redirect_to redeem_coupon_url(params[:code])
  end

  def business_why_github
    redirect_to "/business"
  end

  def nonprofit
    render "site/nonprofit"
  end

  def site_map
    render "site/site_map"
  end

  def developer_stories
    render "site/developer_stories"
  end

  def developer_stories_alicia
    render "site/developer_stories_alicia"
  end

  def developer_stories_amy
    render "site/developer_stories_amy"
  end

  def developer_stories_lisa
    render "site/developer_stories_lisa"
  end

  def developer_stories_mario
    render "site/developer_stories_mario"
  end

  def git_guides_index
    render "site/git_guides/index", layout: "site_git_guide"
  end

  def git_guides_add
    render "site/git_guides/show/git_add", layout: "site_git_guide"
  end

  def git_guides_clone
    render "site/git_guides/show/git_clone", layout: "site_git_guide"
  end

  def git_guides_commit
    render "site/git_guides/show/git_commit", layout: "site_git_guide"
  end

  def git_guides_init
    render "site/git_guides/show/git_init", layout: "site_git_guide"
  end

  def git_guides_pull
    render "site/git_guides/show/git_pull", layout: "site_git_guide"
  end

  def git_guides_push
    render "site/git_guides/show/git_push", layout: "site_git_guide"
  end

  def git_guides_remote
    render "site/git_guides/show/git_remote", layout: "site_git_guide"
  end

  def git_guides_status
    render "site/git_guides/show/git_status", layout: "site_git_guide"
  end

  def git_guides_install
    render "site/git_guides/show/install_git", layout: "site_git_guide"
  end

  def keyboard_shortcuts
    respond_to do |format|
      format.html do
        if serving_gist3_standalone?
          render "site/gist_keyboard_shortcuts", layout: false
        else
          render "site/keyboard_shortcuts", layout: false
        end
      end
    end
  end

  def konami
    respond_to do |format|
      format.html do
        render "site/konami", layout: false, locals: { return_to: params[:return_to] }
      end
    end
  end

  def tilde
    safe_redirect_to "/#{path_string}", status: 301
  end

  def unverified_email
    return render_404 unless logged_in? && current_user.should_verify_email?

    # Don't show the flash notice that says
    # the exact same thing as this page.
    @hide_email_verification_warning = true

    # If we have bouncing emails, show details about that too
    if current_user.emails.user_entered_emails.all_bouncing.present?
      respond_to do |format|
        format.html do
          render "site/bouncing_email"
        end
      end
    else
      respond_to do |format|
        format.html do
          render "site/unverified_email"
        end
      end
    end
  end

  def report_bug
    if request.referrer.present?
      location = "<tr><th>Location</th><td><a href=\"#{request.referrer}\">#{request.referrer}</a></td></tr>"
    end

    browser = "<tr><th>Browser</th><td>#{request.user_agent}</td></tr>"

    body = <<~MD



      <table width=100%>
        #{location}
        #{browser}
      </table>

      *Brought to you by the `!` hotkey.*
    MD

    redirect_to new_issue_url("github", "github", body: body, "labels[]": "Bug")
  end

  def asset_redirect
    assets = AssetBundles.get
    bundle = "#{params[:name]}.#{params[:format]}"
    begin
      filename = assets.expand_bundle_name(bundle)
      redirect_to "#{GitHub.asset_host_url}/assets/#{filename}"
    rescue KeyError
      head :not_found, content_type: Mime[:text]
    end
  end

  def feature_gone
    render file: File.join(Rails.root, "public", "410-feature.html"), status: :gone, layout: false, formats: [:html]
  end

  def bounce
    safe_bounce_redirect_to params[:to], status: 301
  end

  # Bounces support users to the corresponding support thread in the archive
  # repo and normal users to the contact page.  Used for URLS like:
  # http://support.github.com/discussions/sales/658-interest-in-githubfi
  def support_bounce
    if employee? && !params[:thread].nil?
      thread_number = params[:thread].to_i
      thread_shard = "%02d" % (thread_number / 1000).ceil
      thread_path = "#{params[:category]}/#{thread_shard}/#{thread_number}.md"
      safe_bounce_redirect_to "/github/tender-archive/blob/master/archive/#{thread_path}", status: 301
    else
      safe_bounce_redirect_to "/contact", status: 301
    end
  end

  # Used as a health check by several load balancers. The first respond_to block
  # is rendered when the Accept header is empty, which is what happens when
  # using `curl` by default. There's currently not a test for this bevaior, so
  # please verify manually. See https://github.com/github/github/pull/35391 and
  # https://github.com/github/github/pull/35389 for context.
  def status_lolrails
    # An early hook to return a 503 instead of success when we're running on kube
    # but the last worker hasn't started and dropped the ready file. This allows
    # kube readiness checks to fail until the last worker comes up.
    if GitHub.kube? && !File.exist?(GitHub.kube_workers_ready_file)
      render plain: "Workers are not yet ready.", status: 503
      return
    end

    respond_to do |f|
      f.html do
        render "site/status", layout: false
      end
      f.json do
        data = { status: "ok" }
        data[:configuration_id] = GitHub.configuration_id if GitHub.configuration_id
        render json: data
      end
    end
  end

  def force502
    render plain: "I am a 502", status: 502
  end

  def sha
    render plain: GitHub.current_sha
  end

  # Used for the "Contact us" page.  Sets up variables the form will need like
  # user name, email, and last viewed repo.
  def contact
    params[:form] ||= {}
    params[:flavor] ||= "default"

    # Redirect to HelpHub, including the prefilled form subject if present.
    # Omit the report-abuse and report-content forms; those are not yet moving
    # to Helphub.
    if !GitHub.enterprise? && !%w( dmca report-abuse report-content ).include?(params[:flavor])
      # Send DMCA and privacy concerns directly to the HelpHub contact form,
      # rather than the landing page.
      helphub = case params[:flavor]
      when "dmca-notice"
        "https://support.github.com/contact/dmca-takedown"
      when "dmca-counter-notice"
        "https://support.github.com/contact/dmca-counter-notice"
      when "privacy"
        "https://support.github.com/contact/privacy"
      else
        "https://support.github.com"
      end

      # Include prefilled subject & comments when redirecting to HelpHub, if present.
      form_params = {
        subject: params.dig(:form, :subject),
        comments: params.dig(:form, :comments),
      }.compact
      if form_params.present?
        helphub += "/?#{{ form: form_params }.to_query}"
      end

      return redirect_to helphub
    end

    if params[:flavor] == "sales" && !GitHub.flipper[:contact_sales_form].enabled?(current_user)
      redirect_to contact_url and return
    end

    unless GitHub.contact_form_flavors.keys.include? params[:flavor]
      redirect_to contact_url and return
    end

    @from_desktop_app = params[:from_desktop_app] || params[:from_mac_app]

    params[:form][:subject] ||= if reported_content
      "Report abuse: #{reported_user.login} (content)"
    elsif reported_action
      "Report abuse: #{reported_action.name} (GitHub Action)"
    elsif reported_app
      "Report abuse: #{reported_app.name} (app)"
    elsif reported_user
      "Report abuse: #{reported_user.login} (user)"
    elsif params[:flavor] == "report-abuse" && params[:report].present?
      "Report abuse: #{params[:report]}"
    elsif @from_desktop_app
      @version = params[:app_version] || "???"
      "GitHub Desktop v#{@version}"
    elsif params[:flavor] == "sales"
      if params[:organization]
        "I have a question about my Legacy plan for #{params[:organization]}"
      else
        "I have a question about my Legacy plan"
      end
    elsif params[:flavor] == "default"
      "" # default form should have no subject to encourage users to enter one
    else
      GitHub.contact_form_flavors[params[:flavor]]
    end

    params[:form][:content_url] = safe_onsite_uri(reported_content) if reported_content
    params[:form][:report] = reported_user if reported_user

    render "site/contact"
  end

  # Dispatches an email to the supportocats
  def send_contact
    @contact_request = ContactRequest.new(contact_params.to_h)

    flavor = params.dig(:form, :flavor)
    if !logged_in? && (flavor == "report-abuse" || flavor == "report-content")
      octocaptcha = Octocaptcha.new(session, params["octocaptcha-token"], page: :report_abuse)
      octocaptcha.verify
      if !octocaptcha.solved?
        anonymous_flash[:error] = "Unable to verify your captcha answer. " \
          "Please try again or visit #{GitHub.help_url}/articles/troubleshooting-connectivity-problems/#troubleshooting-the-captcha for troubleshooting information."
        return redirect_back(fallback_location: "/contact/#{flavor}")
      end
    end

    if !@contact_request.valid?
      anonymous_flash[:error] = if GitHub.rails_6_0?
        @contact_request.errors.values.flatten.join('\n')
      else
        @contact_request.errors.map { |error| error.message }.flatten.join('\n')
      end
      params[:flavor] = @contact_request.flavor
      return redirect_back(fallback_location: "/contact/#{flavor}")

    elsif rate_limited?
      GitHub.instrument("contact_form.ratelimited")
      anonymous_flash[:error] = RATELIMITED_ERROR
      params[:flavor] = @contact_request.flavor
      return redirect_back(fallback_location: "/contact/#{flavor}")

    elsif @contact_request.spam?
      GitHub.instrument("contact_form.spam")

    else
      GitHub.instrument("contact_form.success", @contact_request.tags(logged_in: logged_in?))
      user = nil
      app = nil

      if @contact_request.flavor == "report-abuse"
        user = User.find_by_login(@contact_request.report)

        if @contact_request.report
          if user
            AbuseReport.create(reporting_user: current_user, reported_user: user)

            current_user.instrument(:report_abuse, user: user, actor: current_user, org_id: nil) if logged_in?
          end
        elsif @contact_request.app_report
          app = Integration.find_by_name(@contact_request.app_report)
          current_user.instrument(:report_abuse, app_id: app.id, app: app.name, actor: current_user, org_id: nil)
        elsif @contact_request.action_report
          action = RepositoryAction.find_by(name: @contact_request.action_report)
          current_user.instrument(:report_abuse, action_id: action.id, action: action.name, actor: current_user, org_id: nil)
        end
      elsif logged_in? && @contact_request.flavor == "report-content"
        if @contact_request.report
          user = User.find_by_login(@contact_request.report)
          content = @contact_request.content_object

          classifier = @contact_request.classifier
          possible_classifiers = Platform::Enums::AbuseReportReason.values.keys
          sanitized_classifier = possible_classifiers.include?(classifier) ? classifier : "UNSPECIFIED"

          if user && content&.is_a?(AbuseReportable)
            AbuseReport.create(
              reporting_user: current_user,
              reported_user: content.user,
              reported_content: content,
              repository_id: content.try(:repository_id),
              reason: sanitized_classifier.downcase)
          end

          current_user.instrument(:report_content, user: user, actor: current_user, org_id: nil, content_url: @contact_request.content_url, classifier: sanitized_classifier.downcase) if user
        end
      end

      GlobalInstrumenter.instrument "contact_form.submit", {
        contact_request: @contact_request,
        actor: current_user,
        reported_user: user,
        reported_integration: app,
      }
    end

    if @contact_request.subject == "Report abuse" && !GitHub.enterprise?
      @contact_request.subject = "Report abuse: #{@contact_request.report} (user)" if @contact_request.report
      @contact_request.subject = "Report abuse: #{@contact_request.app_report} (app)" if @contact_request.app_report
    end

    metadata =
      @contact_request.metadata(rate_limited: rate_limited?,
                                user: current_user,
                                has_session: (session != {}),
                                user_agent: request.env["HTTP_USER_AGENT"],
                                remote_ip: request.remote_ip)

    # When this feature flag is enabled, create tickets in Zendesk
    # instead of Halp.
    if GitHub.flipper[:zendesk_tickets].enabled?(current_user)
      tags = ["dotcom-contact-form", "dotcom-form-flavor-#{@contact_request.flavor}"]
      tags << "dotcom-detected-spam" if @contact_request.spam?
      tags << "dotcom-logged-in" if logged_in?

      formatted_metadata = metadata.map { |k, v| "#{k}: #{v}" }.join("\n")
      ua = user_agent

      custom_fields = {
        GitHub.zendesk_fields[:browser]            => ua[:browser],
        GitHub.zendesk_fields[:ip_address]         => request.remote_ip,
        GitHub.zendesk_fields[:javascript_enabled] => @contact_request.javascript_enabled,
        GitHub.zendesk_fields[:location]           => location,
        GitHub.zendesk_fields[:metadata]           => formatted_metadata,
        GitHub.zendesk_fields[:referring_article]  => @contact_request.referring_article_title,
        GitHub.zendesk_fields[:referring_url]      => @contact_request.referring_url,
      }.tap do |cf|
        cf[GitHub.zendesk_fields[:os]] = ua[:os] if ua[:os]

        if logged_in?
          cf[GitHub.zendesk_fields[:business_plus]] = current_user.business_plus?
          cf[GitHub.zendesk_fields[:login]] = current_user.login
          cf[GitHub.zendesk_fields[:plan]] = current_user.plan.name
          cf[GitHub.zendesk_fields[:user_spammy]] = current_user.spammy?
        else
          # If the user isn't signed in, see if the email address they entered is
          # linked to a suspended account. This isn't foolproof (they could've
          # entered a different email) but if true, is a pretty strong sign.
          #
          # The check is only done for non-logged-in users because suspending an
          # account also revokes all active browser sessions.
          if u = User.find_by_email(@contact_request.email)
            cf[GitHub.zendesk_fields[:user_suspended]] = u.suspended?
          end
        end
      end

      CreateZendeskTicket.perform_later(
        @contact_request.requester_name,
        @contact_request.email,
        @contact_request.subject,
        @contact_request.comments,
        brand_id: GitHub.zendesk_brand_id,
        tags: tags,
        custom_fields: custom_fields,
      )
    else
      SupportMailer.contact(@contact_request.subject,
        @contact_request.comments,
        metadata).deliver_later!
    end

    render "site/send_contact", layout: "application", locals: { view: send_contact_view }
  end

  def toggle_site_admin_performance_stats
    return render_404 unless can_toggle_site_admin_and_employee_status?

    if params[:enabled] == "true"
      enable_site_admin_performance_stats_mode
    else
      disable_site_admin_performance_stats_mode
    end

    redirect_to :back
  end

  def toggle_site_admin_and_employee_status
    if site_admin? || employee?
      disable_site_admin_and_employee_mode
    else
      enable_site_admin_and_employee_mode
    end

    if request.xhr?
      head :ok
    else
      # Don't use redirect_to :back, as toggle_site_admin_and_employee_status can be called as a GET
      # with no CSRF protection. Adding CSRF protection is problematic, as
      # toggle_site_admin_and_employee_status is called from our static 404 page. As a result,
      # we have no assurance that :back (Referer) doesn't point to somewhere
      # offsite.
      safe_redirect_to request.headers["Referer"]
    end
  end

  def mobile_preference
    case params[:mobile]
    when "true"
      session[:mobile] = true
    when "false"
      session[:mobile] = false
    end

    url = request.referrer.present? ? request.referrer : request.base_url

    if params[:anchor].present?
      safe_redirect_to "#{url}##{params[:anchor]}"
    else
      safe_redirect_to url
    end
  end

  # for failbot/haystack testing
  def boomtown
    # Enterprise CI currently relies on accessing boomtown to ensure Failbot
    # is setup correctly.
    return render_404 unless employee? || site_admin?
    if params[:job] == "true"
      BoomtownJob.perform_later(magic_number: 42, cause: params[:cause])
      render plain: "Job submitted"
    elsif params[:cause] == "true"
      # allows testing of exceptions with cause
      begin
        fail "the underlying cause"
      rescue
        fail "the outer wrapper (#{GitHub.host_name})"
      end
    elsif params[:redaction_test]
      # simulate an exception that needs redaction
      begin
        fail Zlib::Error, "failed to zip private private private"
      rescue
        raise "outer exception, no PII here! (#{GitHub.host_name})"
      end
    elsif params[:ruby_upgrade]
      call_kwargs_method({ say: "This method will throw a warning for kwargs. Find the logged exception in the github-ruby-warnings project in Sentry." })
    else
      fail "BOOM (#{GitHub.host_name})"
    end
  end

  # For debugging GitHub.context
  def contexttown
    render plain: JSON.pretty_generate(GitHub.context.to_hash)
  end

  # For science testing. Include a fail= parameter for customization.
  def sciencetown
    case params["fail"]
    when "internal" # internal errors
      science "sciencetown" do |e|
        e.context user: current_user.login
        e.use { true }
        e.try { true }
        e.clean { raise "kaboom" }
      end
    when "exception" # raising different internal exceptions
      science "sciencetown" do |e|
        e.context user: current_user.login
        e.use { raise "control" }
        e.try { raise "candidate" }
      end
    else # nothing goes wrong!
      science "sciencetown" do |e|
        e.context user: current_user.login
        e.use { true }
        e.try { true }
      end
    end

    render plain: "Sciencetown! Find the results in the `sciencetown` experiment."
  rescue => e
    render plain: "%s: %s\n\t%s" % [e.class.to_s, e.message, e.backtrace.join("\n\t")]
  end

  def archive_redirect
    redirect_to downloads_path(params[:user_id], params[:repository])
  end

  def toggle_labs
    if GitHub.employee_unicorn?
      GitHub::StaffOnlyCookie.delete!(cookies)
    elsif employee?
      set_employee_only_cookie(user: current_user, for_lab: true)
    end
    redirect_to :back
  end

  # POST to this to hit custom request timeout.
  # See: lib/github/config.rb#request_method.
  def custom_sleeptown
    sleeptown
  end

  def sleeptown
    duration = (params[:n] || 30).to_i
    sleep duration
    head :ok
  end

  def roletown
    render plain: GitHub.role
  end

  def componenttown
    render plain: GitHub.component
  end

  def toggle_canary
    if params[:toggle] == "true"
      cookies[:haproxy_backend] = "canary"
    else
      cookies.delete(:haproxy_backend)
    end
    redirect_to :back
  end

  # Allow access to manual testing endpoints without CSRF. These endpoints are
  # restricted to employee access.
  def verify_authenticity_token?
    if request.path == "/site/custom_sleeptown"
      return false
    end
    super
  end

  private

  def call_kwargs_method(say:)
    render plain: "#{say}"
  end

  def user_agent
    # The user-agent is already in the user's session, if they have one;
    # use that to avoid a second parse.
    ua = if user_session && user_session.ua
      user_session.ua
    else
      Browser.new(request.env["HTTP_USER_AGENT"])
    end

    {
       os: user_agent_platform(ua),
       browser: "#{ua.name} #{ua.full_version} #{"(mobile)" if ua.device.mobile?}".strip,
    }
  end

  def contact_params
    params.require(:form).permit %i[
      subject
      comments
      phone
      state
      email
      flavor
      report
      name
      app_report
      action_report
      javascript_enabled
      referring_url
      referring_article_url
      referring_article_title
      default_inbox
      content_url
      classifier
    ]
  end

  def location
    # The location is already in the user's session, if they have one;
    # use that to avoid a second lookup.
    location = if user_session
      user_session.location
    else
      GitHub::Location.look_up(request.remote_ip)
    end

    "#{location[:city]} #{location[:region]} #{location[:country_code]}".strip
  end

  def reported_user
    return @reported_user if @reported_user
    flavor = (params[:flavor] || params[:form][:flavor])
    return unless %w(report-abuse report-content).include?(flavor)
    report_string = params[:report] || params[:form][:report]
    if report_string =~ /([^ ]+)( \((user|comment)\))?\z/
      @reported_user = User.find_by_login($1)
    end
  end

  def reported_app
    return @reported_app if @reported_app
    return unless (params[:flavor] || params[:form][:flavor]) == "report-abuse"
    report_string = params[:report] || params[:form][:report]
    if report_string =~ /(.+) \(app\)/
      @reported_app = Integration.find_by_name($1)
    end
  end

  def reported_action
    return @reported_action if @reported_action
    return unless (params[:flavor] || params[:form][:flavor]) == "report-abuse"
    report_string = params[:report] || params[:form][:report]
    if report_string =~ /(.+) \(GitHub Action\)\z/
      @reported_action = RepositoryAction.find_by(name: $1)
    end
  end

  def reported_content
    return unless (params[:flavor] || params[:form][:flavor]) == "report-content"
    content_url = params[:content_url] || params[:form][:content_url]
    content_url.to_s if reported_user && content_url
  end

  def send_contact_view
    @send_contact_view ||=
      Site::ContactView.new(current_user: current_user,
                            flavor: params[:flavor],
                            content_url: params[:content_url],
                            classifer: params[:classifier],
                            reported_user: reported_user,
                            reported_app: reported_app,
                            reported_action: reported_action)
  end

  # We have a honeypot to catch automated attemts to spam our contact form. We
  # also filter out certain IP addresses based on a blacklist.
  #
  # To bypass the honeypot, valid requests must include a `phone` field that
  # contains the string "Phone" and a `state` field that is blank.
  def spam_counter_measures
    if params[:form][:phone] != "Phone" ||
      !params[:form][:state].blank?
      GitHub.instrument("contact_form.honeypot")
      render "site/send_contact", layout: "application", locals: { view: send_contact_view }
    elsif Spam.ip_is_blacklisted?(request.remote_ip)
      GitHub.instrument("contact_form.blacklisted")
      render "site/send_contact", layout: "application", locals: { view: send_contact_view }
    end
  end

  # Simple rate limit for the contact form.  Because spammers.
  #
  # The user's IP is always used, because spammers have made accounts for the
  # sole purpose of sending us a single spam in the past.
  #
  # Unauthenticated: 2 contacts every 60 minutes
  # Authenticated:   2 contacts every 10 minutes
  def rate_limited?
    return @rate_limited unless @rate_limited.nil?
    return @rate_limited = false if current_user&.employee?

    ttl = logged_in? ? 10.minutes.to_i : 60.minutes.to_i
    cache_key = "contact_form_limiter:#{request.remote_ip}"
    options = {
      max_tries: 2,
      ttl: ttl,
    }
    @rate_limited = RateLimiter.at_limit?(cache_key, options)
  end

  def stateless_request?
    action_name == "sha" || super
  end

  def full_raw_path
    params[:path]
  end

  # Allow SignedAuthToken authentication for archive_cmd and raw_cmd actions.
  def token_request_format?
    %w(archive_cmd raw_cmd).include?(action_name)
  end

  # All URLs that we should allow bounce redirection to.
  #
  # Returns an array of whitelisted domains
  ALLOWED_BOUNCE_HOSTS = [
    "github.com",
    "help.github.com",
    "windows.github.com",
    "mac.github.com",
    "shop.github.com",
    "pages.github.com",
    "gist.github.com",
    "jobs.github.com",
    "raw.github.com",
    "www.gitready.com",           # /guides/put-your-git-branch-name-in-your-shell-prompt
    "textile.thresholdstate.com", # /guides/textile-formatting
    "ozmm.org",                   # /guides/disaster-faq-what-to-do-when-github-goes-bad
    "wiki.eclipse.org",           # /guides/using-the-egit-eclipse-plugin-with-github
    "gitready.com",               # /guides/put-your-git-branch-name-in-your-shell-prompt
    "education.github.com",
    "developer.github.com",
    "training.github.com",
    "pages-auth.github.com",
    GitHub.gist_host_name,
    GitHub.host_name,
    GitHub.classroom_host,
  ].freeze

  # Used to redirect to a URL given in a param.  We only want to redirect to
  # trusted offsite locations.
  #
  # to      - String URL, usually from params[:to]
  # options - Optional Hash passed to #redirect_to.
  #           :status   - The HTTP status of the redirection (default: 302)
  #
  # Returns nothing.
  def safe_bounce_redirect_to(to, options = {})
    safe_redirect_to(to, options.merge(allow_hosts: ALLOWED_BOUNCE_HOSTS))
  end
end
