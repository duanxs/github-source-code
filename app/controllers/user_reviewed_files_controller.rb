# frozen_string_literal: true

class UserReviewedFilesController < AbstractRepositoryController

  before_action :login_required
  before_action :load_current_pull_request

  def create
    reviewed_file = current_user.reviewed_files.for(@pull).find_by(filepath: params[:path])

    if reviewed_file
      reviewed_file.update_attribute(:dismissed, false)
    else
      reviewed_file = GitHub::SchemaDomain.allowing_cross_domain_transactions do
        current_user.reviewed_files.create(filepath: params[:path], pull_request_id: @pull.id, head_sha: @pull.head_sha)
      end
    end

    GlobalInstrumenter.instrument("pull_request_file.viewed", {
      repository: @pull.repository,
      pull_request: @pull,
      actor: current_user,
      file_path: params[:path],
      action: "VIEWED",
    })

    view_model_params = {
      pull_request: @pull,
      path: params[:path],
      user: current_user,
    }

    # If this reviewed file record isn't valid, the user marked a file that no
    # longer exists as viewed and we're not persisting that. This will override
    # the checkbox so it still appears checked to them.
    unless reviewed_file.valid?
      view_model_params[:reviewed] = true
    end

    respond_to do |format|
      format.html do
        if request.xhr?
          render_partial_view "diff/file_review", Diff::FileReviewView, view_model_params
        else
          redirect_to :back
        end
      end
    end
  end

  def destroy
    reviewed_file = current_user.reviewed_files.for(@pull).find_by(filepath: params[:path])
    return render_404 unless reviewed_file
    reviewed_file.destroy

    GlobalInstrumenter.instrument("pull_request_file.unviewed", {
      repository: @pull.repository,
      pull_request: @pull,
      actor: current_user,
      file_path: params[:path],
      action: "UNVIEWED",
    })

    respond_to do |format|
      format.html do
        if request.xhr?
          render_partial_view "diff/file_review", Diff::FileReviewView,
            pull_request: @pull,
            path: params[:path],
            user: current_user
        else
          redirect_to :back
        end
      end
    end
  end

  private

  def load_current_pull_request
    @pull = current_repository.issues.find_by_number(params[:pull_id].to_i).try(:pull_request)
    return render_404 unless @pull
  end
end
