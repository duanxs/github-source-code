#frozen_string_literal: true

class OrganizationsController < ApplicationController
  areas_of_responsibility :orgs, :user_growth

  statsd_tag_actions only: [:access_list_members, :news_feed, :show]

  include BillingSettingsHelper
  include OrganizationsHelper
  include BusinessesHelper
  include DashboardAnalyticsHelper
  include SeatsHelper
  include SignupHelper
  include MarketingMethods
  include Actions::ForkPrWorkflowsPolicyHelper
  include CreationSurveyHelper

  before_action :login_required, except: %w( transforming enterprise_plan )

  before_action :check_ofac_sanctions, only: %w( new create ), if: :couponed_paid_upgrade?
  before_action only: :transform do
    check_ofac_sanctions(redirect_url: settings_user_organizations_url)
  end

  before_action :org_members_only,
    except: %w( show new create seats transform transforming
    access_list_members leave check_name check_company_name check_billing_email plan choose_plan enterprise_plan
    signup_billing collect_payment
    )
  before_action :org_admins_only, except: %w(
    access_list_members ajax_your_repos create destroy dismiss_notice leave new
    check_name check_company_name check_billing_email
    seats show transform transforming plan choose_plan enterprise_plan signup_billing collect_payment
  )
  before_action :validate_update_params, only: :update
  before_action :set_skip_payment, only: [:new]

  before_action :org_creators_only, only: %w(new create seats plan choose_plan)
  before_action :ldap_sync_only, only: %w(import)
  before_action :require_xhr, only: :access_list_members
  before_action :update_protected_branches_setting_flag_required, only: [:update_members_can_update_protected_branches]

  before_action :add_gh_classroom_csp_exceptions, only: %w( new )
  before_action :orgs_set_return_to, only: %w( new )

  before_action :require_signup_flow_redesign, only: %w( plan choose_plan signup_billing)
  before_action :require_plan_selection, only: %w( new )

  # The following actions do not require conditional access checks:
  # - leave: just removes a user from the organization, which doesn't expose any
  #          protected organization resources.
  skip_before_action :perform_conditional_access_checks, only: [:leave]

  YOUR_REPOS_LIMIT = 20

  def add_gh_classroom_csp_exceptions
    if GitHub.flipper[:github_classrooms_redirect].enabled?(current_user)
      SecureHeaders.append_content_security_policy_directives(request, {
        form_action: ["'self'"].concat([GitHub.classroom_host]),
      })
    end
  end

  def orgs_set_return_to
    if GitHub.flipper[:github_classrooms_redirect].enabled?(current_user)
      if params[:return_to] == "classroom"
        session[:return_to] = "classroom"
      end
    end
  end

  def invite
    render "organizations/signup/invite", locals: {
      plan: current_organization.plan,
      pricing_model: per_seat_pricing_model,
    }
  end

  ShowQuery = parse_query <<-'GRAPHQL'
    query(
      $yourReposLimit: Int!,
      $login: String!,
      $yourReposCursor: String
    ) {
      enabledFeatures
      organization(login: $login) {
        ...Views::Organizations::Show::Organization
      }
    }
  GRAPHQL

  def show
    # Redirect to dashboard if this org isn't found.
    # Useful after an org has been deleted and the user's context is
    # still set to that org.
    return redirect_to "/" if current_organization.nil?

    set_context
    variables = { yourReposLimit: YOUR_REPOS_LIMIT, login: current_organization.login,
                  yourReposCursor: params[:your_repos_cursor] }

    respond_to do |format|
      format.html do
        data = platform_execute(ShowQuery, variables: variables)
        render "organizations/show", layout: "dashboard_three_column", locals: {
          query: params[:q],
          org: data.organization,
        }
      end
    end
  end

  AjaxYourReposQuery = parse_query <<-'GRAPHQL'
    query($yourReposLimit: Int!, $login: String!, $yourReposCursor: String) {
      enabledFeatures
      organization(login: $login) {
        ...Views::Dashboard::OrgRepos::RepositoryOwner
      }
    }
  GRAPHQL

  def ajax_your_repos
    if request.xhr?
      next_page_url = ajax_your_repos_organization_path(current_organization, input_id: params[:input_id])
      data = platform_execute(AjaxYourReposQuery, variables: {
        yourReposLimit: 100, login: current_organization.login,
        yourReposCursor: params[:your_repos_cursor]
      })
      respond_to do |format|
        format.html do
          render partial: "dashboard/org_repos",
                 locals: { user: data.organization, next_page_url: next_page_url,
                           query: params[:q], input_id: params[:input_id] }
        end
      end
    else
      redirect_to user_path(current_organization)
    end
  end

  # Builds html for the org invite and user to org transform pages' js.
  def access_list_members
    user = User.find_by_login(params[:member])

    if user
      respond_to do |format|
        format.html do
          render partial: "organizations/transform/access_list_member",
                 locals: { user: user }
        end
      end
    else
      head :not_found
    end
  end

  def new
    if current_user.has_any_trade_restrictions? &&
        [GitHub::Plan.business.name, GitHub::Plan.business_plus.name].include?(params[:plan])
      return redirect_to org_plan_path
    end

    @coupon = Coupon.find_by_code(params[:coupon]) if params[:coupon]

    if @coupon && @coupon.plan
      @plan = @coupon.plan
    else
      @plan = GitHub::Plan.find(params[:plan]) || GitHub::Plan.default_plan
    end

    if @coupon && @coupon.user_only?
      return redirect_to redeem_coupon_url(@coupon.code)
    end

    # Prevent transform when sponsoring
    if org_transform? &&
       (current_user.has_a_plan_specific_coupon? ||
        current_user.sponsors_membership.present? ||
        current_user.sponsoring.any?)
      flash[:error] = if current_user.has_a_plan_specific_coupon?
        "You cannot transform this account into an organization, because you have an active coupon that is locked to a plan. Please contact support."
      elsif current_user.sponsors_membership.present?
        "You cannot transform this account into an organization, because you have signed up for GitHub Sponsors."
      elsif current_user.sponsoring.any?
        "You cannot transform this account into an organization, because active sponsorships must be cancelled first."
      end
      return redirect_to settings_user_organizations_url
    end

    @current_organization = Organization.new(
      plan: @plan.name,
      coupon: @coupon,
      login: params[:login],
      billing_email: params[:billing_email],
    )
    if org_transform?
      @current_organization.login = current_user.login
      @current_organization.billing_email = current_user.billing_email
    end

    if GitHub.billing_enabled?
      if @current_organization.coupon
        @current_organization.plan = @current_organization.best_plan_for_coupon
      end

      if org_transform?
        @current_organization.seats = current_user.default_seats

        if current_user.coupon.present? && !current_user.coupon.user_only?
          @current_organization.coupon = current_user.coupon
          @current_organization.plan = @current_organization.best_plan_for_coupon
        end

        if @current_organization.plan.repos < current_user.owned_private_repositories.count
          @current_organization.plan = GitHub::Plan.org_plans.detect do |available|
            available.repos > current_user.owned_private_repositories.count
          end
        end
      end

      @monthly_per_seat_pricing_model      = per_seat_pricing_model(coupon: @coupon, duration: "month")
      @annual_per_seat_pricing_model      = per_seat_pricing_model(coupon: @coupon, duration: "year")
      @per_seat_pricing_model      = per_seat_pricing_model(coupon: @coupon)
      @monthly_business_plus_pricing_model = per_seat_pricing_model(coupon: @coupon, new_plan: GitHub::Plan.business_plus, duration: "month")
      @annual_business_plus_pricing_model = per_seat_pricing_model(coupon: @coupon, new_plan: GitHub::Plan.business_plus, duration: "year")
      @business_plus_pricing_model = per_seat_pricing_model(coupon: @coupon, new_plan: GitHub::Plan.business_plus)

      # NB: If you are on a 100% off coupon the business plan could be
      # a default plan option. However, we also don't want to set it
      # if the coupon has a specific plan already associated.
      #
      # see https://github.com/github/github/commit/67d6809b7fd0219cccf8ddc646eb466da12bef28
      if @per_seat_pricing_model.final_price.zero? && !@coupon.try(:plan_specific?)
        @plan = GitHub::Plan.business
      end
    end

    if GitHub.billing_enabled? && params[:plan] == GitHub::Plan.business_plus.name
      view = Orgs::SetupView.new(plan: @plan, organization: @current_organization, collect_payment_for_trial: require_payment_info_for_trial?)
      render "organizations/signup/new", locals: { view: view }
    else
      if org_transform?
        render "organizations/transform/org_transform"
      else
        view = Orgs::SetupView.new(plan: @plan, organization: @current_organization, collect_payment_for_trial: require_payment_info_for_trial?)
        render "organizations/signup/new", locals: { view: view }
      end
    end
  end

  def plan
    if [GitHub::Plan.business.name, GitHub::Plan.business_plus.name].include?(params[:plan])
      redirect_args = { redirect_params: { plan: params[:plan], coupon: params[:coupon] } }
      redirect_to(new_organization_path(plan: params[:plan], coupon: params[:coupon]))
    else
      free_org = Organization.new(plan: GitHub::Plan.free)
      free_pricing_model = Billing::PlanChange::PerSeatPricingModel.new(free_org, seats: free_org.seats)

      business_org = Organization.new(plan: GitHub::Plan.business)
      business_pricing_model = Billing::PlanChange::PerSeatPricingModel.new(business_org, seats: business_org.seats)

      default_org = Organization.new(plan: GitHub::Plan.default_plan)
      business_plus_pricing_model = Billing::PlanChange::PerSeatPricingModel.new(default_org, seats: default_org.seats, new_plan: GitHub::Plan.business_plus)

      render "organizations/signup/plan", locals: {
        business_pricing_model: business_pricing_model,
        business_plus_pricing_model: business_plus_pricing_model,
        free_pricing_model: free_pricing_model,
      }
    end
  end

  def choose_plan
    redirect_to(new_organization_path(plan: params[:plan], coupon: params[:coupon]))
  end

  def enterprise_plan
    return render_404 if GitHub.enterprise?

    default_org = Organization.new(plan: GitHub::Plan.default_plan)
    business_plus_pricing_model = Billing::PlanChange::PerSeatPricingModel.new(
      default_org,
      seats: default_org.seats,
      new_plan: GitHub::Plan.business_plus,
    )

    render "organizations/signup/enterprise_plan", locals: {
      business_plus_pricing_model: business_plus_pricing_model,
    }
  end

  def collect_payment
    plan = GitHub::Plan.find(org_hash[:plan]) || GitHub::Plan.business_plus

    @current_organization = Organization.new(
      plan: plan.name,
      login: org_hash[:login],
      billing_email: org_hash[:billing_email],
      company_name: org_hash[:company_name],
    )

    view = Orgs::SetupView.new(plan: plan, organization: @current_organization)

    render "organizations/signup/collect_payment", locals: {
      view: view,
      company_info: trial_attributes.slice(
        :full_name,
        :industry,
        :other_industry,
        :employees_size,
      ),
    }
  end

  def signup_billing
    coupon = Coupon.find_by_code(params[:coupon]) if params[:coupon]

    if coupon && coupon.plan
      plan = coupon.plan
    else
      plan = GitHub::Plan.find(org_hash[:plan]) || GitHub::Plan.business
    end

    @current_organization = Organization.new(
      plan: plan.name,
      coupon: coupon,
      login: org_hash[:login],
      billing_email: org_hash[:billing_email],
      company_name: org_hash[:company_name],
    )

    if GitHub.billing_enabled?
      per_seat_pricing_model = per_seat_pricing_model(coupon: coupon)

      # NB: If you are on a 100% off coupon the business plan could be
      # a default plan option. However, we also don't want to set it
      # if the coupon has a specific plan already associated.
      #
      # see https://github.com/github/github/commit/67d6809b7fd0219cccf8ddc646eb466da12bef28
      if per_seat_pricing_model.final_price.zero? && !coupon.try(:plan_specific?)
        plan = GitHub::Plan.business
      end

    end

    view = Orgs::CreationView.new({
      per_seat_pricing_model: per_seat_pricing_model,
      coupon: coupon,
      organization: @current_organization,
      plan: plan,
    })
    render "organizations/signup/billing", locals: { view: view }
  end

  # Creates a fresh organization.
  def create
    GitHub.dogstats.increment("organization", tags: ["action:create"])
    GitHub.context.push(visitor_id: current_visitor.id)

    # Create as a free org plan if cloud trial
    is_enterprise_cloud_trial = params.dig("enterprise", "plan") == "cloud-trial" &&
      org_hash[:plan] == "business_plus"

    original_plan_name = org_hash[:plan] || params[:plan]
    GitHub.dogstats.increment("organization.plan_selection", tags: ["action:attempt", "plan:#{original_plan_name}"])

    plan_name = if is_enterprise_cloud_trial
      GitHub::Plan.free.name
    else
      original_plan_name
    end

    plan = GitHub::Plan.find(plan_name) || GitHub::Plan.default_plan
    old_seat_count = current_user.seats
    old_plan_name = current_user.plan.name

    org_hash[:plan_duration] = params[:plan_duration]

    if current_user.has_any_trade_restrictions? && plan.paid?
      flash[:error] = sanctioned_by_ofac_message
      GitHub.dogstats.increment("organization.plan_selection", tags: ["action:rejected", "plan:#{original_plan_name}"])
      return redirect_to(organizations_new_path)
    end

    if is_enterprise_cloud_trial && require_payment_info_for_trial? && no_payment_details?
      flash[:error] = "You must enter a payment method to begin your free trial."
      GitHub.dogstats.increment("organization.plan_selection", tags: ["action:rejected", "plan:#{original_plan_name}"])
      return collect_payment
    end

    result = Organization::Creator.perform(current_user,
                                           plan,
                                           org_hash,
                                           payment_details: payment_details,
                                           coupon_code: params[:coupon],
                                           business_owned: org_is_on_business_tos?,
                                           terms_of_service: params[:terms_of_service_type])
    @current_organization = result.organization
    @coupon = result.coupon

    if result.success?
      GitHub.dogstats.increment("organization.plan_selection", tags: ["action:success", "plan:#{original_plan_name}"])
      # Change plan to trial
      if is_enterprise_cloud_trial
        cloud_trial = Billing::EnterpriseCloudTrial.new(result.organization.reload)
        if cloud_trial.create
          send_welcome_email_for_enterprise_cloud_trial(current_user, result.organization)

          deliver_end_email_at = (cloud_trial.expires_on - 1.day).to_datetime
          send_delayed_end_enterprise_cloud_trial_email(current_user, result.organization, deliver_end_email_at)

          if params[:company_info]
            EnterpriseCloudTrialMarketingNotificationJob.perform_later(
              trial_details_for_marketing(
                organization: result.organization,
                cloud_trial: cloud_trial,
                trial_attributes: trial_attributes,
              ),
            )
          end
        end

        if require_payment_info_for_trial?
          GitHub::Billing.create_customer(@current_organization, payment_details)
        end

        session.delete(:skip_payment_collection_for_trial)
      end

      GitHub.dogstats.increment("organization", tags: ["action:create", "type:paid"]) if result.organization.plan.cost > 0
      ga_action = result.organization.plan.free? ? {} : { action: "upgrade"}
      flash[:analytics_location_params] = {
        target: "Organization",
        billing: false,
        plan: result.organization.plan.name,
      }.merge(ga_action)
      analytics_event organization_creation_ga_event_attributes(true, @current_organization, current_user, is_enterprise_cloud_trial)

      unless is_enterprise_cloud_trial
        analytics_ec_purchase(result.organization, @current_organization.subscription.discounted_price, "Signup")
        publish_billing_plan_changed_for(
          actor: current_user,
          user: @current_organization,
          old_plan_name: old_plan_name,
          new_plan_name: result.organization.plan.name,
          old_seat_count: old_seat_count,
          new_seat_count: @current_organization.seats,
          user_first_name: params[:user_first_name],
          user_last_name: params[:user_last_name],
        )

        if @current_organization.payment_method
          publish_payment_method_changed_for(
            actor: current_user,
            account: @current_organization,
            payment_method: @current_organization.payment_method,
          )
        end
      end

      if session[:return_to] == "classroom" && GitHub.flipper[:github_classrooms_redirect].enabled?(current_user)
        return redirect_to_return_to(fallback: "#{GitHub.classroom_host}/classrooms/new")
      else
        if @current_organization.plan.free? && remove_add_members_interstitial_enabled?
          return redirect_to user_path(@current_organization)
        else
          return redirect_to invite_organization_path(@current_organization)
        end
      end

    else
      flash.now[:error] = result.error_message
      GitHub.dogstats.increment("organization.plan_selection", tags: ["action:failed", "plan:#{original_plan_name}"])
      analytics_event organization_creation_ga_event_attributes(false, @current_organization, current_user, is_enterprise_cloud_trial, redirect: false)

      @current_organization.plan = GitHub::Plan.free
      if GitHub.billing_enabled?
        @monthly_per_seat_pricing_model      = per_seat_pricing_model(coupon: @coupon, duration: "month")
        @annual_per_seat_pricing_model       = per_seat_pricing_model(coupon: @coupon, duration: "year")
        @monthly_business_plus_pricing_model = per_seat_pricing_model(coupon: @coupon, new_plan: GitHub::Plan.business_plus, duration: "month")
        @annual_business_plus_pricing_model  = per_seat_pricing_model(coupon: @coupon, new_plan: GitHub::Plan.business_plus, duration: "year")
        @per_seat_pricing_model              = per_seat_pricing_model(coupon: @coupon)
        @business_plus_pricing_model         = per_seat_pricing_model(coupon: @coupon, new_plan: GitHub::Plan.business_plus)
        @plan = GitHub::Plan.business if @per_seat_pricing_model.final_price.zero?
      end

      view = Orgs::SetupView.new(plan: GitHub::Plan.find(original_plan_name), organization: @current_organization)
      render "organizations/signup/new", locals: { view: view }
    end
  end

  def seats
    @current_organization = Organization.new
    @coupon = Coupon.find_by_code(params[:coupon])
    new_org_url_options = {
      coupon: @coupon,
      plan_duration: params[:plan_duration],
      plan: params[:new_plan],
      organization: {
        profile_name: @current_organization.name,
        company_name: @current_organization.company_name,
      },
    }

    if params[:transform_user].present?
      new_org_url_options[:transform_user] = "1"
    end

    data = hash_for_pricing_model_change(
      per_seat_pricing_model(coupon: @coupon, new_plan: params[:new_plan]),
      org_signup_billing_url(new_org_url_options),
    )

    data[:is_enterprise_cloud_trial] = params[:new_plan] == GitHub::Plan.business_plus.name

    respond_to do |format|
      format.json do
        render json: data
      end
    end
  end

  def destroy
    DeleteToken.verify! current_user, current_organization, params[:dangerzone]

    if current_organization.has_any_trade_restrictions?
      flash[:trade_controls_organization_billing_error] = true
    end

    if current_organization.adminable_by? current_user
      if current_organization.trusted_oauth_apps_owner?
        flash[:error] = "#{current_organization} cannot be deleted. It’s the owner of some trusted applications."
      else
        current_organization.async_destroy(current_user)
        flash[:notice] = "#{current_organization} is being deleted."
      end
    end
  rescue DeleteToken::DangerZone => danger
    failbot StandardError.new("OrganizationsController.destroy failed - invalid CRSF token")
    GitHub::Logger.log_exception({method: "OrganizationsController.destroy"}, danger)
  ensure
    if request.xhr?
      head 200
    else
      redirect_to "/"
    end
  end

  def leave
    organization = Organization.find_by_login!(params[:organization_id])

    return render_404 unless current_user.affiliated_with_organization?(organization)

    begin
      organization.prevent_removal_of_last_admin!(current_user, "You can't remove the last admin")

      RemoveUserFromOrganizationJob.perform_later(organization.id, current_user.id)

      flash[:notice] = "You left #{organization}. It may take a few minutes to process."
    rescue Organization::NoAdminsError
      flash[:notice] = "Can’t leave #{organization}. If you’re the last owner, delete the organization."
    end

    redirect_to :back
  end

  # Transforms the currently logged in user into an
  # organization. Yikes.
  def transform
    GitHub.dogstats.increment("organization", tags: ["action:transform"])

    # Prevent transforms for plan specific coupons
    if current_user.has_a_plan_specific_coupon?
      flash[:error] = "You cannot transform this account into an organization, because you have an active coupon that is locked to a plan. Please contact support."
      return redirect_to settings_user_organizations_url
    end

    # Prevent transform when sponsoring
    if current_user.sponsoring.any?
      flash[:error] = "You cannot transform this account into an organization, because active sponsorships must be cancelled first."
      return redirect_to settings_user_organizations_url
    end

    if current_user.sponsors_membership.present?
      flash[:error] = "You cannot transform this account into an organization, because you have signed up for GitHub Sponsors."
      return redirect_to settings_user_organizations_url
    end

    # First convert the array of string user names into user objects.
    if !params[:organization]
      flash.now[:error] = "No owners or emails were submitted."
      return new
    end

    if current_user.organizations.any?
      flash[:error] = "You cannot transform this account into an organization before leaving all other organizations first."
      return redirect_to settings_user_organizations_url
    end

    if GitHub.billing_enabled?
      plan = org_hash[:plan] ||= params[:plan]
      unless GitHub::Plan.find(plan).try(:org_plan_or_per_seat?)
        flash.now[:error] = "Please choose a valid plan."
        return new
      end
    else
      plan = org_hash[:plan] = "enterprise"
    end

    if has_payment_details?
      if current_user.has_billing_record?
        result = GitHub::Billing.update_payment_method(current_user, payment_details)
      else
        result = GitHub::Billing.create_customer(current_user, payment_details)
      end
      unless result.success?
        flash[:error] = result.error_message.to_s
        return new
      end
    end

    org_hash.delete(:login)
    admin_logins = org_hash.delete(:admin_logins) || []
    owner        = User.find_by_login(admin_logins.first) if admin_logins.any?
    org          = Organization.new(org_hash)

    old_seats_count = owner&.seats
    old_plan_name = owner&.plan&.name

    if !current_user.needs_valid_payment_method_to_switch_to_plan?(plan)
      Organization.transform(current_user, owner, org_hash.to_hash)
      publish_billing_plan_changed_for(
        actor: current_user,
        user: owner,
        old_seat_count: old_seats_count,
        new_seat_count: org.seats,
        old_plan_name: old_plan_name,
        new_plan_name: plan,
      )
      reset_session
      cookies[:org_transform_notice] = { value: Base64.strict_encode64("Please sign in as #{owner.login}, the owner of your new #{current_user.safe_profile_name} organization."),
                                         expires: 1.hour.from_now, secure: request && request.ssl?, domain: cookie_domain }
      render "organizations/transform/transforming"
    else
      @current_organization = org
      flash[:error] = "Please provide a credit card to switch to the #{plan.capitalize} plan."
      return new
    end
  rescue Organization::TransformationFailed => e
    flash.now[:error] = e.to_s
    new
  end

  # Responds to the poll-include-fragment element ajax request, indicating
  # whether the user is currently transforming or not.
  def transforming
    user = User.find_by_login(params[:user])
    return head 404 if user.nil?

    if Organization.transforming?(user)
      head 202
    else
      head 200
    end
  end

  # Updates an organization's public profile.
  def update
    GitHub.context.push(spamurai_form_signals: spamurai_form_signals)

    notice = success = nil
    Organization.transaction do
      if success = current_organization.update(profile_params)
        notice = if org_hash.key?(:granular_repo_creation_permissions_changing)
          # Ensure we set a valid value, preventing private-only policies for free/team orgs
          # regardless of any client-side shenanigans.
          public_creation_allowed = current_organization.can_restrict_only_public_repo_creation? ?
            org_hash[:members_can_create_public_repositories] == "1":
            org_hash[:members_can_create_public_repositories] == "1" || org_hash[:members_can_create_private_repositories] == "1"
          enabled = current_organization.allow_members_can_create_repositories_with_visibilities(actor: current_user,
            public_visibility: public_creation_allowed,
            private_visibility: org_hash[:members_can_create_private_repositories] == "1",
            internal_visibility: org_hash[:members_can_create_internal_repositories] == "1")
          if enabled.length > 0
            "Members can now create #{to_sentence(enabled)} repositories."
          else
            "Members can no longer create #{available_repo_types_to_sentence("or")} repositories."
          end
        else
          "Profile updated."
        end
      end
    end

    if success
      redirect_to :back, notice: notice
    else
      redirect_to :back, flash: { error: current_organization.errors.full_messages.to_sentence }
    end
  end

  # Updates an organization's terms of service
  def tos
    on_corporate_terms = current_organization && current_organization.terms_of_service.corporate?
    new_terms_value = params[:organization][:terms_of_service_type]

    if on_corporate_terms && current_organization.company&.name.present? && new_terms_value == Organization::TermsOfService::CORPORATE
      redirect_to :back, flash: { error: "The terms of service is already up-to-date for this organization" }
    else
      if current_organization.terms_of_service.update(
        type: new_terms_value,
        actor: current_user,
        company_name: params[:organization][:company_name],
      )

        redirect_to :back, notice: "Terms of service updated!"
      else
        redirect_to :back, flash: { error: current_organization.errors.full_messages.to_sentence }
      end
    end
  end

  def update_members_can_create_teams
    notice = if params[:members_can_create_teams] == "1"
      current_organization.allow_members_to_create_teams(actor: current_user)
      "Members can now create teams."
    else
      current_organization.block_members_from_creating_teams(actor: current_user)
      "Members can no longer create teams."
    end

    redirect_to :back, notice: notice
  end

  def update_members_can_change_repo_visibility
    notice = if params[:members_can_change_repo_visibility] == "1"
      current_organization.allow_members_to_change_repo_visibility(actor: current_user)
      "Members can now change repository visibility."
    else
      current_organization.block_members_from_changing_repo_visibility(actor: current_user)
      "Members can no longer change repository visibility."
    end

    redirect_to :back, notice: notice
  end

  def update_action_execution_capability
    capability = params[:action_execution_capability]&.to_s
    notice = case capability
    when "DISABLED"
      current_organization.disable_all_action_executions(actor: current_user)
      "All Actions are now disabled for all repositories."
    when "ALL_ACTIONS"
      current_organization.enable_all_action_executions(actor: current_user)
      "All Actions are now enabled for all repositories."
    when "LOCAL_ACTIONS_ONLY"
      current_organization.enable_only_local_action_executions(actor: current_user)
      "Only Local/In-Repo Actions are now enabled for all repositories."
    end

    redirect_to :back, notice: notice
  end

  def update_fork_pr_workflows_policy
    return render_404 unless GitHub.flipper[:fork_pr_workflows_policy].enabled?(current_organization)
    form_policy = params[:fork_pr_workflows_policy] || {}
    set_fork_pr_workflows_policy(current_organization, form_policy)
  end

  def update_package_creation_permission
    if params[:publish_public_packages_enabled] == "1"
      current_organization.allow_members_to_publish_public_packages(actor: current_user)
    else
      current_organization.block_members_from_publishing_public_packages(actor: current_user)
    end

    if params[:publish_private_packages_enabled] == "1"
      current_organization.allow_members_to_publish_private_packages(actor: current_user)
    else
      current_organization.block_members_from_publishing_private_packages(actor: current_user)
    end

    redirect_to :back, notice: "Projects settings updated for this organization."
  end

  def update_members_can_delete_repositories
    if ["0", "1"].include?(params[:members_can_delete_repositories])
      notice = if params[:members_can_delete_repositories] == "1"
        current_organization.allow_members_can_delete_repositories(actor: current_user)
        "Members can now delete or transfer repositories."
      else
        current_organization.disallow_members_can_delete_repositories(actor: current_user)
        "Members can no longer delete or transfer repositories."
      end

      redirect_to :back, notice: notice
    else
      flash[:error] = "You specified an invalid value for the 'members can delete repositories' setting."
      redirect_to :back
    end
  end

  def update_members_can_delete_issues
    if ["0", "1"].include?(params[:members_can_delete_issues])
      notice = if params[:members_can_delete_issues] == "1"
        current_organization.allow_members_can_delete_issues(actor: current_user)
        "Members can now delete issues."
      else
        current_organization.disallow_members_can_delete_issues(actor: current_user)
        "Members can no longer delete issues."
      end

      redirect_to :back, notice: notice
    else
      flash[:error] = "You specified an invalid value for the 'members can delete issues' setting."
      redirect_to :back
    end
  end

  def update_members_can_update_protected_branches
    case params[:members_can_update_protected_branches]
    when "1"
      current_organization.allow_members_can_update_protected_branches(actor: current_user)
      redirect_to :back, notice: "Members can now update protected branches."
    when "0"
      current_organization.disallow_members_can_update_protected_branches(actor: current_user)
      redirect_to :back, notice: "Members can no longer update protected branches."
    else
      flash[:error] = "You specified an invalid value for the 'members can update protected branches' setting."
      redirect_to :back
    end
  end

  def update_members_can_invite_outside_collaborators
    if ["0", "1"].include?(params[:members_can_invite_outside_collaborators])
      notice =
        if params[:members_can_invite_outside_collaborators] == "1"
          current_organization.allow_members_can_invite_outside_collaborators(actor: current_user)
          "Members can now invite outside collaborators."
        else
          current_organization.disallow_members_can_invite_outside_collaborators(actor: current_user)
          "Members can no longer invite outside collaborators."
        end
      redirect_back(fallback_location: "/", notice: notice)
    else
      flash[:error] = "You specified an invalid value for the 'members can invite outside collaborators' setting."
      redirect_back(fallback_location: "/")
    end
  end

  def update_members_can_view_dependency_insights
    return render_404 unless current_organization.dependency_insights_enabled_for?(current_user)

    if ["0", "1"].include?(params[:members_can_view_dependency_insights])
      notice = if params[:members_can_view_dependency_insights] == "1"
        current_organization.allow_members_can_view_dependency_insights(actor: current_user)
        "Members can now view dependency insights."
      else
        current_organization.disallow_members_can_view_dependency_insights(actor: current_user)
        "Members can no longer view dependency insights."
      end

      redirect_to :back, notice: notice
    else
      flash[:error] = "You specified an invalid value for the 'members can view dependency insights' setting."
      redirect_to :back
    end
  end

  def update_settings
    case params[:allow_private_repository_forking]
    when "1" then current_organization.allow_private_repository_forking(actor: current_user)
    when "0" then current_organization.block_private_repository_forking(actor: current_user)
    end

    redirect_to :back, notice: "Repository forking setting updated!"
  end

  def update_default_repository_permission
    action = params.fetch(:default_repository_permission, "").strip.presence
    settings_context = params.fetch(:settings_context, "").strip.presence

    if current_organization.valid_default_repository_permission?(action)
      current_organization.update_default_repository_permission(action, actor: current_user)

      if GitHub.flipper[:custom_roles].enabled?
        custom_roles = Role.lower_custom_roles(action: action, org: current_organization)
        custom_roles.each { |role| Permissions::CustomRoles.update_users(role) }
      end

      flash[:notice] = if action != "none"
        "Default repository permission updated to '#{action}'."
      else
        "Default repository permission removed."
      end
    else
      flash[:error] = "You specified an invalid default repository permission."
    end

    if settings_context
      redirect_to("#{settings_org_member_privileges_path(current_organization)}##{settings_context}")
    else
      redirect_to :back
    end
  end

  def update_projects_enabled
    args = params.require(:organization).permit(:organization_projects_enabled, :repository_projects_enabled)

    if args[:organization_projects_enabled] == "1"
      current_organization.enable_organization_projects(actor: current_user)
    else
      current_organization.disable_organization_projects(actor: current_user)
    end

    if args[:repository_projects_enabled] == "1"
      current_organization.enable_repository_projects(actor: current_user)
    else
      current_organization.disable_repository_projects(actor: current_user)
    end

    flash[:notice] = "Projects settings updated for this organization."

    redirect_to :back
  end

  def update_team_discussions_enabled
    args = params.require(:organization).permit(:team_discussions_allowed)

    if args[:team_discussions_allowed] == "1"
      current_organization.allow_team_discussions(actor: current_user)
      flash[:notice] = "Team discussions enabled for this organization."
    else
      current_organization.disallow_team_discussions(actor: current_user)
      flash[:notice] = "Team discussions disabled for this organization."
    end

    redirect_to :back
  end

  def update_readers_can_create_discussions
    value = params[:readers_can_create_discussions]

    unless ["0", "1"].include?(value)
      flash[:error] = "You specified an invalid value for the setting for which users can create discussions."
      return redirect_to(settings_org_member_privileges_path(current_organization))
    end

    notice = if value == "1"
      current_organization.allow_readers_to_create_discussions(actor: current_user)
      "Users with read access to repositories can create new discussions."
    else
      current_organization.block_readers_from_creating_discussions(actor: current_user)
      "Only users with at least triage access to repositories can create new discussions."
    end

    redirect_to settings_org_member_privileges_path(current_organization), notice: notice
  end

  def update_display_commenter_full_name
    if !current_organization.plan_supports_display_commenter_full_name?(visibility: :private)
      supported_plans = GitHub::Plan.supported_org_plans_for_feature(
          feature: :display_commenter_full_name,
          visibilities: [:private],
        ).map(&:titleized_display_name).uniq
      plans_as_sentence = if supported_plans.count > 2
        supported_plans.to_sentence(last_word_connector: " or ")
      else
        supported_plans.to_sentence(two_words_connector: " or ")
      end
      flash[:error] = "Upgrade to #{plans_as_sentence} to enable this feature."
      redirect_to settings_org_member_privileges_path(current_organization)
    elsif ["0", "1"].include?(params[:display_commenter_full_name])
      notice = if params[:display_commenter_full_name] == "1"
        current_organization.enable_display_commenter_full_name(actor: current_user)
        "Private repositories will now show comment author's full name."
      else
        current_organization.disable_display_commenter_full_name(actor: current_user)
        "Private repositories will not show comment author's full name."
      end

      redirect_to settings_org_member_privileges_path(current_organization), notice: notice
    else
      flash[:error] = "You specified an invalid value for the \"display comment " \
        "author's full name in private repositories\" setting."
      redirect_to settings_org_member_privileges_path(current_organization)
    end
  end

  def ignore_upgrade
    current_organization.upgrade_ignore = current_organization.plan.name
    current_organization.save
    redirect_to org_dashboard_path(current_organization)
  end

  # Perform an org rename
  #
  # login - The new name for the org
  def rename
    GitHub.context.push({
      hydro: { actor: current_user }, # nested to avoid event_context serialization
      spamurai_form_signals: spamurai_form_signals,
    })

    @org = Organization.find_by_id(current_organization.id)
    old_login = @org.login

    if @org.rename(params[:login], actor: current_user)
      session[:org_renaming] = { "from" => old_login, "to" => params[:login] }
      render "organizations/rename"
    else
      flash[:error] = @org.errors.full_messages.to_sentence
      redirect_to :back
    end
  end

  def dismiss_notice
    input = params.require(:input).permit(:organizationId, :notice)
    org = Organization.find_by(id: input[:organizationId])
    return return_404 unless notice_dismissable?(org)

    current_user.dismiss_organization_notice(input[:notice], org)

    if request.xhr?
      head :ok
    else
      redirect_to :back
    end
  end

  # LDAP Group import
  ImportQuery = parse_query <<-'GRAPHQL'
    query($login: String!, $first: Int) {
      organization(login: $login) {
        ...Views::Organizations::Import::Organization
      }
    }
  GRAPHQL

  def import
    return render_404 unless GitHub.ldap_sync_enabled?
    data = platform_execute(ImportQuery,
      variables: {
        "login" => params[:organization_id],
        "first" => 30,
      },
    )

    render "organizations/import",
      locals: {organization: data.organization}
  end

  def check_billing_email
    billing_email = params[:value]
    return head 400 if billing_email.blank?

    if billing_email =~ User::EMAIL_REGEX
      respond_to do |format|
        format.html_fragment do
          head 200
        end
      end
    else
      respond_to do |format|
        format.html_fragment do
          render body: "Email is invalid", status: 422, content_type: "text/fragment+html"
        end
      end
    end
  end

  def check_name
    profile_name = params[:value]
    return head 400 if profile_name.blank?
    name = profile_name.parameterize(preserve_case: true)

    # Check if the org name is valid.
    if GitHub::DeniedLogins.include? name.downcase
      respond_to do |format|
        format.html_fragment do
          render partial: "organizations/signup/name_message", formats: :html, status: 422, locals: { exists: false,
            blacklisted: true,
            name: name,
            not_alphanumeric: false,
            over_max_length: false,
          }
        end
      end
    elsif User.find_by(name: name).present?
      respond_to do |format|
        format.html_fragment do
          render partial: "organizations/signup/name_message", formats: :html, status: 422, locals: { exists: true,
            blacklisted: false,
            name: name,
            not_alphanumeric: false,
            over_max_length: false,
          }
        end
      end
    elsif !name.match?(Organization::LOGIN_REGEX)
      respond_to do |format|
        format.html_fragment do
          render partial: "organizations/signup/name_message", formats: :html, status: 422, locals: { exists: false,
            blacklisted: false,
            name: name,
            not_alphanumeric: true,
            over_max_length: false,
          }
        end
      end
    elsif name.length > Organization::LOGIN_MAX_LENGTH
      respond_to do |format|
        format.html_fragment do
          render partial: "organizations/signup/name_message", formats: :html, status: 422, locals: { exists: false,
            blacklisted: false,
            name: name,
            not_alphanumeric: false,
            over_max_length: true,
          }
        end
      end
    else
      respond_to do |format|
        format.html_fragment do
          render partial: "organizations/signup/name_message", formats: :html, locals: {
            exists: false,
            blacklisted: false,
            name: name,
            not_alphanumeric: false,
            over_max_length: false,
          }
        end
      end
    end
  end

  def check_company_name
    company_name = params[:value]
    return head 400 if company_name.blank?

    if Company.valid_name?(company_name)
      respond_to do |format|
        format.html_fragment do
          head 200
        end
      end
    else
      respond_to do |format|
        format.html_fragment do
          render partial: "organizations/signup/company_name_message", formats: :html, status: 422, locals: { name: company_name }
        end
      end
    end
  end

  private

  def notice_dismissable?(org)
    org&.member?(current_user) || org&.adminable_by?(current_user)
  end

  def return_404
    request.xhr? ? head(404) : render_404
  end

  def org_is_on_business_tos?
    return false unless GitHub.terms_of_service_enabled?
    Organization::TermsOfService::BUSINESS_TERMS_OF_SERVICE_TYPES.include?(params[:terms_of_service_type]&.capitalize)
  end

  def couponed_paid_upgrade?
    params[:coupon].present?
  end

  def require_payment_info_for_trial?
    collect_payment_for_trials? && !skip_payment_collection_for_trial?
  end

  def skip_payment_collection_for_trial?
    session[:skip_payment_collection_for_trial] == true
  end

  def org_hash
    @org_hash ||= begin
      permitted_organization_params = [
        :name, :email, :login, :billing_email, :plan, :profile_name, :profile_email,
        :profile_blog, :profile_company, :profile_location, :profile_hireable,
        :profile_bio, :gravatar_email, :billing_extra, :plan_duration,
        :members_can_create_repositories, :granular_repo_creation_permissions_changing,
        :members_can_create_public_repositories, :members_can_create_private_repositories,
        :members_can_create_internal_repositories, :seats, :company_name, :profile_twitter_username,
        admin_logins: [],
      ]

      permitted_organization_profile_params = []

      if GitHub.sponsors_enabled? && GitHub.flipper[:corporate_sponsors_credit_card].enabled?(current_user)
        permitted_organization_profile_params << :sponsors_update_email
      end

      permitted_organization_params << { organization_profile_attributes: permitted_organization_profile_params }

      params.require(:organization).permit(permitted_organization_params)
    end
  end

  def profile_params
    org_hash.slice(
      :profile_name, :profile_email, :profile_bio, :profile_blog,
      :profile_company, :profile_location, :billing_email, :gravatar_email,
      :profile_twitter_username, :organization_profile_attributes,
    )
  end

  def per_seat_pricing_model(coupon: nil, new_plan: nil, duration: params[:plan_duration])
    Billing::PlanChange::PerSeatPricingModel.new \
      @current_organization,
      seats: params[:seats] || @current_organization.seats,
      plan_duration: duration,
      new_plan: new_plan,
      coupon: coupon
  end

  def org_creators_only
    if !user_can_create_organizations?
      if current_user.spammy? && !GitHub.enterprise?
        flash[:error] = "Something went wrong. Please contact support."
        redirect_to "/support"
      else
        return render_404
      end
    end
  end

  def ldap_sync_only
    render_404 unless GitHub.ldap_sync_enabled?
  end

  def update_protected_branches_setting_flag_required
    render_404 unless GitHub.update_protected_branches_setting_enabled? || GitHub.flipper[:update_protected_branches_setting].enabled?(current_organization)
  end

  def all_events
    @all_events ||= events_timeline.events(page: 1, per_page: Stratocaster::OrgAllTimeline::INDEX_LENGTH)
  end

  def events_timeline_key
    "org:#{current_organization.id}"
  end

  def validate_update_params
    if org_hash.key?(:members_can_create_repositories)
      unless ["1", "0", "all", "none", "private"].include?(org_hash[:members_can_create_repositories]&.to_s)
        redirect_to :back, flash: { error: "Invalid value given, unable to update the repository creation setting." }
      end
    end
  end

  def publish_payment_method_changed_for(actor:, account:, payment_method:)
    GlobalInstrumenter.instrument(
      "billing.payment_method.addition",
      actor_id: actor.id,
      account_id: account.id,
      payment_method_id: payment_method.id,
    )
  end

  def require_signup_flow_redesign
    render_404 unless GitHub.billing_enabled?
  end

  def require_plan_selection
    # Users coming from `https://classroom.github.com/` can only see the new form for free plan
    if GitHub.flipper[:github_classrooms_redirect].enabled?(current_user)
      return org_plan_path if params[:return_to] == "classroom"
    end

    GitHub::Logger.log({
      fn: "require_plan_selection",
      user: current_user,
      org_transform: org_transform?,
      billing_enabled: GitHub.billing_enabled?,
      trade_restrictions: current_user.has_any_trade_restrictions?,
      plan_param: params[:plan],
    })

    return if org_transform? || !GitHub.billing_enabled?
    # Have to choose plan first in redesigned flow
    # OFAC sanctioned users can only see the new form for free plan

    return redirect_to(org_plan_path) if !params[:plan] || (current_user.has_any_trade_restrictions? && [GitHub::Plan.free.name, GitHub::Plan::TEAM_FREE_PLAN_NAME].exclude?(params[:plan]))
  end

  def trial_attributes
    params.require(:company_info).permit(
      :full_name,
      :industry,
      :other_industry,
      :employees_size,
    ).merge(
      params.require(:user).permit(:ga_client_id, :ga_tracking_id, :ga_user_id),
    )
  end

  def trial_details_for_marketing(organization:, cloud_trial:, trial_attributes:)
    {
      organization_id: organization.id,
      organization_login: organization.login,
      company_name: organization.company_name,
      email: current_user.email,
      user_id: current_user.id,
      trial_start: cloud_trial.started_on.as_json,
      trial_expiration: cloud_trial.expires_on.as_json,
      user_agent: user_session.user_agent,
      remote_ip_address: user_session.ip,
      state: user_session.location[:region],
      city: user_session.location[:city],
      country: user_session.location[:country_code],
      postal_code: user_session.location[:postal_code],
      marketing_email_opt_in: NewsletterPreference.marketing_preference(user: current_user),
      trial_id: cloud_trial.plan_trial_id,
      event_type: "cloud_trial",
      agreed_to_terms: true,
      billing_email: organization.billing_email,
    }.merge(
      trial_attributes.slice(
        :full_name,
        :industry,
        :other_industry,
        :employees_size,
        :ga_client_id,
        :ga_tracking_id,
        :ga_user_id,
      ),
    ).merge(
      session.fetch(:utm_memo, Hash.new).slice("utm_medium", "utm_source", "utm_campaign"),
    ).stringify_keys
  end

  def available_repo_types_to_sentence(conjunction = "and")
    types = ["public", "private"]
    types.append("internal") if current_organization.supports_internal_repositories?
    to_sentence(types, conjunction)
  end

  def to_sentence(items, conjunction = "and")
    return items.first if items.length == 1
    list = items[0..-2].join(", ")
    list += "," if items.length > 2 # Oxford commas keep it classy
    list += " #{conjunction} "
    list += items.last
  end
end
