# frozen_string_literal: true

# Actions shared between the global dependencies controller and the org-scoped
# dependencies controller
module PackageDependencies::SharedActions
  include PlatformHelper
  include PackageDependencies::Licenses

  PAGE_SIZE = 25

  # Maps user-friendly strings (published, updated) to sort enums we're expecting in Enums::RepositoryPackageReleaseOrderField
  # Ensure that any additions are added to Platform::Objects::RequestingUser#repository_package_releases
  #
  # Configuration for Sort dropdown and query parameters.
  #
  # The key should be the upcased first section of the value for the sort query, e.g. published-desc would map to PUBLISHED
  # Each value should be a hash containing:
  #   - enum - Sort enum defined in Enums::RepositoryPackageReleaseOrderField
  #   - queries - Array of queries which will be available in the sort dropdown on Pacakges#index
  #
  SORT_QUERY_CONFIG = {
    PUBLISHED: {
      enum: "PUBLISHED_ON",
      queries: [
        { query: "published-desc", label: "Newest" },
        { query: "published-asc", label: "Oldest" },
      ],
    },
    UPDATED: {
      enum: "UPDATED_AT",
      queries: [
        { query: "updated-desc", label: "Recently updated" },
        { query: "updated-asc", label: "Least recently updated" },
      ],
    },
    DEPENDENTS: {
      enum: "DEPENDENTS",
      queries: [
        { query: "dependents-desc", label: "Most dependents", default: true }, # If you'd like to change the default sort, remember to apply it in lib/platform/objects/requesting_user.rb as well!
        { query: "dependents-asc", label: "Least dependents" },
      ],
    },
    VULNERABILITIES: {
      enum: "VULNERABILITIES",
      queries: [
        { query: "vulnerabilities-desc", label: "Most security advisories" },
        { query: "vulnerabilities-asc", label: "Least security advisories" },
      ],
    },
  }.freeze

  SEVERITIES = [:low, :moderate, :high, :critical].freeze

  IndexQuery = parse_query <<-'GRAPHQL'
    query($first: Int, $last: Int, $after: String, $before: String, $ownerIds: [Int!]!, $packageManager: String, $packageName: String, $packageVersion: String, $onlyVulnerablePackages: Boolean, $sortBy: RepositoryPackageReleaseOrder, $exactMatch: Boolean!, $severity: String, $licenses: [String!]) {
      requester {
        ...Views::PackageDependencies::Index::Requester
      }
    }
  GRAPHQL

  def index_data(owner_ids)
    variables = graphql_pagination_params(page_size: PAGE_SIZE)
    variables[:ownerIds] = owner_ids
    variables[:packageManager] = ecosystem_in_query if ecosystem_in_query.present?
    variables[:sortBy] = sort_query_to_options if sort_query_to_options.present?
    variables[:packageName] = package_name_in_query if package_name_in_query.present?
    variables[:exactMatch] = exact_package_name_match?
    variables[:packageVersion] = version_in_query if version_in_query.present?
    variables[:severity] = severity_in_query if severity_in_query.present?
    variables[:licenses] = license_enums_in_query if license_enums_in_query.present?
    variables[:onlyVulnerablePackages] = vulnerable_in_query

    platform_execute(IndexQuery, variables: variables)
  end

  def render_package_dependencies_index(
    owner_ids: nil,
    this_organization: nil,
    user_organizations: [],
    unauthorized_saml_targets: [],
    unauthorized_ip_whitelisting_targets: []
  )
    owner_ids = [this_organization.id] if this_organization.present?
    if owner_ids.present?
      data = index_data(owner_ids)
      case data.errors.all.messages["requester"]&.first
      when /timedout/
        dependency_graph_timed_out = true
      when /unavailable/
        dependency_graph_unavailable = true
      end
    end

    respond_to do |format|
      format.html do
        render_template_view "package_dependencies/index", PackageDependencies::IndexView, {
          parsed_query: parsed_query,
          raw_query: raw_query,
          enabled_orgs: user_organizations,
          this_organization: this_organization,
          queries_for_sort_dropdown: queries_for_sort_dropdown,
          unauthorized_saml_targets: unauthorized_saml_targets,
          unauthorized_ip_whitelisting_targets: unauthorized_ip_whitelisting_targets,
          dependency_graph_timed_out: dependency_graph_timed_out,
          dependency_graph_unavailable: dependency_graph_unavailable,
          licenses: LICENSES,
        }, locals: { data: data&.requester }
      end
    end
  end

  SecurityGraphQuery = parse_query <<-'GRAPHQL'
    query($ownerIds: [Int!]!, $packageManager: String, $packageName: String, $packageVersion: String, $exactMatch: Boolean, $licenses: [String!]) {
      requester {
        ... on RequestingUser {
          repositoryPackageReleaseVulnerabilities(ownerIds: $ownerIds, packageManager: $packageManager, packageName: $packageName, packageVersion: $packageVersion, exactMatch: $exactMatch, licenses: $licenses) {
            severity
            totalCount
            dependentsCount
          }
        }
      }
    }
  GRAPHQL

  def security_data(owner_ids)
    variables = {}
    variables[:ownerIds] = owner_ids
    variables[:packageManager] = ecosystem_in_query if ecosystem_in_query.present?
    variables[:packageName] = package_name_in_query if package_name_in_query.present?
    variables[:packageVersion] = version_in_query if version_in_query.present?
    variables[:licenses] = license_enums_in_query if license_enums_in_query.present?
    variables[:exactMatch] = exact_package_name_match?

    platform_execute(SecurityGraphQuery, variables: variables)
  rescue DependencyGraph::Client::ApiError => e
    Failbot.report(e, app: "github-dependency-graph", dependency_insights: true, owner_ids: owner_ids)
  end

  def render_package_dependencies_security_graph(owner_ids: nil, this_organization: nil)
    view = PackageDependencies::IndexView.new(raw_query: raw_query, this_organization: this_organization)
    severities = Hash[SEVERITIES.collect { |severity| [severity, create_severity(severity, view)] }]

    owner_ids = [this_organization.id] if this_organization.present?
    if owner_ids.present?
      selected_severity = severity_in_query.to_s.to_sym
      data = security_data(owner_ids)
      if data
        data.requester.repository_package_release_vulnerabilities.each do |vuln|
          severity = vuln.severity.to_s.to_sym
          next unless severities.key?(severity)

          severities[severity][:count] = vuln.total_count
          severities[severity][:dependents] = vuln.dependents_count
          severities[severity][:selected] = selected_severity == severity
        end
      end
    end

    respond_to do |format|
      format.json do
        render json: severities.values
      end
    end
  end

  LicenseGraphQuery = parse_query <<-'GRAPHQL'
    query($ownerIds: [Int!]!, $packageManager: String, $packageName: String, $packageVersion: String, $onlyVulnerablePackages: Boolean, $severity: String, $exactMatch: Boolean) {
      requester {
        ...Views::PackageDependencies::LicenseGraph::Requester
      }
    }
  GRAPHQL

  def license_data(owner_ids)
    variables = {}
    variables[:ownerIds] = owner_ids
    variables[:packageManager] = ecosystem_in_query if ecosystem_in_query.present?
    variables[:packageName] = package_name_in_query if package_name_in_query.present?
    variables[:packageVersion] = version_in_query if version_in_query.present?
    variables[:severity] = severity_in_query if severity_in_query.present?
    variables[:onlyVulnerablePackages] = vulnerable_in_query
    variables[:exactMatch] = exact_package_name_match?

    platform_execute(LicenseGraphQuery, variables: variables)
  rescue DependencyGraph::Client::ApiError => e
    Failbot.report(e, app: "github-dependency-graph", dependency_insights: true, owner_ids: owner_ids)
  end

  def render_package_dependencies_licenses_graph(owner_ids: nil, this_organization: nil)
    owner_ids = [this_organization.id] if this_organization.present?
    data = license_data(owner_ids) if owner_ids.present?

    respond_to do |format|
      format.html do
        render_partial_view "package_dependencies/license_graph", PackageDependencies::IndexView, {
          parsed_query: parsed_query,
          raw_query: raw_query,
          licenses: LICENSES,
          this_organization: this_organization,
          selected_licenses: licenses_in_query,
        }, locals: { data: data&.requester }
      end
    end
  end

  def render_package_dependencies_license_menu_content(this_organization: nil)
    render_partial_view "package_dependencies/filters/license_menu_content", PackageDependencies::IndexView, {
      this_organization: this_organization,
      parsed_query: parsed_query,
      licenses: LICENSES,
    }
  end

  private

  def package_manager
    params[:ecosystem]
  end

  def package_name
    Addressable::URI.unescape(params[:name])
  end

  def package_version
    params[:version]
  end

  def dependent_search
    params[:dependent_name]
  end

  def raw_query
    params[:query]
  end

  def parsed_query
    @parsed_query ||= Search::Queries::PackageDependenciesQuery.parse(params[:query])
  end

  def parsed_qualifiers
    @parsed_qualifiers ||= parsed_query.select do |component|
      # Select non-negated qualifiers such as qualifier:value
      component.is_a?(Array) && component.third != true
    end
  end

  def qualifier_values_in_query(qualifier)
    parsed_qualifiers.select do |component|
      component.first == qualifier
    end.map(&:second)
  end

  def first_qualifier_value_in_query(qualifier)
    qualifier_values_in_query(qualifier).first
  end

  def exact_package_name_match?
    first_qualifier_value_in_query(:name).present?
  end

  def package_name_in_query
    @package_name_in_query ||= first_qualifier_value_in_query(:name) || parsed_query.find { |component| component.is_a?(String) }
  end

  def sort_in_query
    return @sort_in_query if defined? @sort_in_query

    @sort_in_query = first_qualifier_value_in_query(:sort)
  end

  def ecosystem_in_query
    return @ecosystem_in_query if defined? @ecosystem_in_query

    ecosystem = first_qualifier_value_in_query(:ecosystem)
    @ecosystem_in_query = valid_ecosystem?(ecosystem) ? ecosystem : nil
  end

  def valid_ecosystem?(ecosystem)
    # checks if ecosystem passed for query is a valid one
    # Verifies this by checking package manager enum that is also used by AdvisoryDB
    ecosystem.present? && Platform::Enums::VulnerabilityPlatform.values.include?(ecosystem.upcase)
  end

  def version_in_query
    return @version_in_query if defined? @version_in_query

    @version_in_query = first_qualifier_value_in_query(:version)
  end

  def valid_severity?(severity)
    SEVERITIES.include?(severity.to_s.to_sym)
  end

  def severity_in_query
    return @severity_in_query if defined? @severity_in_query

    severity = first_qualifier_value_in_query(:severity)
    @severity_in_query = valid_severity?(severity) ? severity : nil
  end

  def licenses_in_query
    return @licenses_in_query if defined? @licenses_in_query

    @licenses_in_query = qualifier_values_in_query(:license).select do |license|
      valid_license?(license)
    end.presence
  end

  def license_enums_in_query
    return @license_enums_in_query if defined? @license_enums_in_query

    @license_enums_in_query = licenses_in_query&.map do |license|
      license
        .gsub("+", "-PLUS")
        .gsub(/\A0/, "ZERO-")
        .parameterize
        .underscore
        .upcase
    end
  end

  def vulnerable_in_query
    return @vulnerable_in_query if defined? @vulnerable_in_query

    @vulnerable_in_query = parsed_qualifiers.any? do |component|
      component.first == :is && "vulnerable".casecmp?(component.second)
    end
  end

  def sort_query_to_options
    return @sort_query_to_options if defined? @sort_query_to_options

    @sort_query_to_options = begin
      # If there's no sort specified, use the default argument value
      return if sort_in_query.blank?

      field, direction = sort_in_query.split("-").map(&:upcase)
      enum_field = SORT_QUERY_CONFIG[field.to_sym][:enum]

      # If the field doesn't map to an enum or the direction isn't ASC/DESC, use default sort
      return if enum_field.nil? || !direction.in?(%w(ASC DESC))

      { field: enum_field, direction: direction.upcase }
    end
  end

  def queries_for_sort_dropdown
    @queries_for_sort_dropdown ||= SORT_QUERY_CONFIG.collect { |_, config| config[:queries] }.flatten
  end

  def add_qualifiers(qualifiers)
    query = parsed_query.dup
    query += qualifiers
    Search::Queries::PackageDependenciesQuery.stringify(query)
  end

  def create_severity(severity, view)
    query = add_qualifiers([[:is, "vulnerable"], [:severity, severity]])
    {
      label: severity.capitalize,
      count: 0,
      dependents: 0,
      url: view.build_path(query: query),
    }
  end
end
