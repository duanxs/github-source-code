# frozen_string_literal: true

module ControllerMethods
  module Oauth
    def actor_id
      current_user.id if logged_in?
    end

    def check_eligibility
      return if @application.github_full_trust? # rubocop:disable GitHub/FullTrust
      return unless current_user.spammy? || current_user.must_verify_email?

      if current_user.spammy?
        flash[:error] = "You are marked as spam, and therefore cannot authorize a third party application."
        return redirect_to dashboard_path
      end

      if current_user.must_verify_email?
        flash[:error] = "Authorizing an application requires a verified email address."
        render_email_verification_required
      end
    end

    def cid
      if cookies[:_octo] =~ /\A[^.]+\.[^.]+\.\d+\.\d+\z/
        cookies[:_octo].split(".")[2..3].join(".")
      end
    end

    def conditional_sudo_filter
      return unless oauth_access_authorized?
      return unless (@scopes & Api::AccessControl.sudo_protected_scope_names).any?

      sudo_filter
    end

    def instrument_integration_listing
      if @application.integration_listing
        GitHub.instrument("integration.listing_authorized", {
          dimensions: {
            id: @application.integration_listing.id,
            cid: cid,
            actor_id: actor_id,
          },
        })
      end
    end

    def oauth_access_authorized?
      ["1", "Approve"].include? params[:authorize]
    end

    def reject_dangerous_requests
      # TODO As we change this code over time, move the business logic out of this
      # controller and into domain objects (like OauthAuthorizationRequest). Allow
      # the controller to function solely as a traffic cop.

      @authorization_request = OauthAuthorizationRequest.new(
        user: current_user,
        application: @application,
        scopes: @scopes
      )

      return unless @authorization_request.dangerous_to_github?
      render "oauth/dangerous_to_github", locals: { request: @authorization_request }
    end

    def reject_suspended_applications
      raise NotImplementedError
    end

    def secret_last_eight(secret)
      secret = secret.to_s
      return secret if secret.length < 8

      secret.to_s[-8..-1]
    end

    def show_sso_selection?(application)
      return false if params.fetch(:skip_sso, false)
      unauthorized_saml_organizations.any?
    end

    def unauthorized_saml_organizations
      return @unauthorized_saml_organizations if defined?(@unauthorized_saml_organizations)

      @unauthorized_saml_organizations = []
      return @unauthorized_saml_organizations if unauthorized_saml_organization_ids.empty?

      scope = Organization.includes(:business)

      @unauthorized_saml_organizations = if @application.is_a?(Integration)
        target_ids = @application.installations.where(target_id: unauthorized_saml_organization_ids, target_type: "User").pluck(:target_id)
        scope.with_ids(target_ids)
      else
        scope.with_ids(unauthorized_saml_organization_ids).oauth_app_policy_met_by(@application)
      end.to_a

      @unauthorized_saml_organizations
    end
  end
end
