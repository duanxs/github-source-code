# frozen_string_literal: true

module Settings
  class AuthRecoveryCodesController < ApplicationController
    # The following actions do not require conditional access checks because
    # they *don't* access protected organization resources.
    skip_before_action :perform_conditional_access_checks, only: %w(
      index
      download_recovery_codes
      print
      update
    )

    before_action :login_required
    before_action :ensure_two_factor_enabled
    before_action :ensure_user_has_two_factor_enabled
    before_action :set_cache_control_no_store
    before_action :sudo_filter
    before_action :track_views

    javascript_bundle :settings

    def index
      GitHub.dogstats.increment("two_factor.recovery_codes.viewed", tags: [
        "recent_security_checkup:#{current_user.recently_took_action_on_security_checkup?}",
      ])
      current_user.instrument_two_factor_recovery_codes_viewed
      render "settings/auth_recovery_codes/index"
    end

    def download_recovery_codes
      GitHub.dogstats.increment("two_factor.recovery_codes.downloaded", tags: [
        "recent_security_checkup:#{current_user.recently_took_action_on_security_checkup?}",
      ])
      current_user.instrument_two_factor_recovery_codes_downloaded
      # Windows notepad likes extra fancy newlines https://github.com/github/github/issues/29828
      codes = current_user.two_factor_credential.formatted_recovery_codes
      send_data(
        codes.join("\r\n"),
        filename: "github-recovery-codes.txt",
      )
    end

    def print
      GitHub.dogstats.increment("two_factor.recovery_codes.printed", tags: [
        "recent_security_checkup:#{current_user.recently_took_action_on_security_checkup?}",
      ])
      current_user.instrument_two_factor_recovery_codes_printed
      render "settings/auth_recovery_codes/print", layout: "popup", locals: { codes: current_user.two_factor_credential.formatted_recovery_codes }
    end

    def update
      credential = current_user.two_factor_credential
      credential.generate_recovery!
      if credential.save
        GitHub.dogstats.increment("two_factor.recovery_codes.regenerated", tags: [
          "recent_security_checkup:#{current_user.recently_took_action_on_security_checkup?}",
        ])
        flash[:notice] = "New two-factor recovery codes successfully generated."
      else
        flash[:error] = "Something went wrong. Please try again."
      end
      redirect_to :back
    end

    private

    def track_views
      select_write_database do
        current_user.two_factor_credential.recovery_codes_viewed!
      end
    end

    def ensure_two_factor_enabled
      unless GitHub.auth.two_factor_authentication_enabled?
        render_404
      end
    end

    def ensure_user_has_two_factor_enabled
      unless current_user.two_factor_authentication_enabled?
        if request.xhr?
          head 422
        else
          redirect_to settings_user_two_factor_authentication_intro_path
        end
      end
    end
  end
end
