# frozen_string_literal: true

class Settings::OauthApplicationTransfersController < ApplicationController
  areas_of_responsibility :platform

  include OauthApplicationTransfersControllerMethods

  javascript_bundle :settings

  # The following actions do not require conditional access checks because
  # they *don't* access any protected organization resources.
  skip_before_action :perform_conditional_access_checks, only: %w(
    show
    accept
    destroy
  )

  private

  def current_context
    current_user
  end
end
