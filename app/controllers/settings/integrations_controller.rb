# frozen_string_literal: true

class Settings::IntegrationsController < ApplicationController
  areas_of_responsibility :platform

  statsd_tag_actions only: [:create]

  include IntegrationsControllerMethods
  include IntegrationManagerHelper

  # Even though the receive_manifest action is a POST, it is idempotent and merely presents
  # the user with a view without making any changses to the datanase. Since the client side POST
  # is initiated from a host that is not GitHub, CSRF checks need to be disabled.
  skip_before_action :verify_authenticity_token, only: [:receive_manifest]

  # These actions are not at risk of allowing updates to full trust Apps and/or
  # they do not have a current_integration and so cannot update a full trust
  # App:
  skip_before_action :prevent_full_trust_apps_for_non_site_admins, only: [:authorizations, :new_from_manifest, :create_manifest]

  before_action :require_app_manifest_token_cookie, only: [:new_from_manifest]

  javascript_bundle :settings

  # IntegrationsControllerMethods then IntegrationsController in definition order
  ACTIONS_EXCLUDED_FROM_CAP_CHECKS = %w(
    index
    show
    permissions
    installations
    advanced
    beta_features
    beta_toggle
    new
    receive_manifest
    create
    update
    update_permissions
    preview_note
    generate_key
    remove_key
    keys
    make_public
    make_internal
    destroy
    transfer
    reset_secret
    revoke_all_tokens
    authorizations
    new_from_manifest
  )

  def require_whitelisted_ip?
    return false if ACTIONS_EXCLUDED_FROM_CAP_CHECKS.include?(action_name)
    true
  end

  def require_active_external_identity_session?
    return false if ACTIONS_EXCLUDED_FROM_CAP_CHECKS.include?(action_name)
    true
  end

  def two_factor_enforceable
    return :no if ACTIONS_EXCLUDED_FROM_CAP_CHECKS.include?(action_name)
    :yes
  end

  def target_for_conditional_access
    :no_target_for_conditional_access
  end

  def authorizations
    order = if params[:o] == "used-desc"
      "oauth_authorizations.accessed_at desc"
    elsif params[:o] == "used-asc"
      "oauth_authorizations.accessed_at asc"
    else
      "integrations.name asc"
    end

    @authorizations = current_user.oauth_authorizations.github_apps
      .joins(:integration)
      .includes(:integration)
      .order(order)
      .limit(15)
      .page(params[:page])

    render_template_view "integrations/authorizations", Integrations::AuthorizationsView,
      authorizations: @authorizations
  end

  def new_from_manifest
    kv_id = cookies[:app_manifest_token]

    # Due to replication delay we might not be successful reading from replicas
    # as this read happens very soon after our write
    kv_data = ActiveRecord::Base.connected_to(role: :writing, prevent_writes: true) do
      GitHub.kv.get(kv_id).value { nil }
    end

    cookies.delete(:app_manifest_token)

    unless kv_data
      manifest_not_found!(msg: "Unable to load App Manifest, please try again.")
      return
    end

    kv_data = JSON.parse(kv_data)

    owner, owner_validation = validate_owner(kv_data["owner"])
    return if owner_validation == :saml_session_required

    manifest = IntegrationManifest.new(
      data: kv_data["manifest"],
      owner: owner,
    )

    if manifest.valid?
      render_template_view "integrations/settings/new_from_manifest",
        Integrations::NewFromManifestView, manifest: manifest, owner: owner,
        manifest_token: kv_id
    else
      render_template_view "integrations/settings/invalid_manifest",
        Integrations::NewFromManifestView, manifest: manifest, owner: owner,
        manifest_token: kv_id
    end
  end

  def create_manifest
    owner_login = params[:integration_manifest].delete(:owner_login)
    GitHub.kv.del(params[:integration_manifest].delete(:manifest_token))

    owner, owner_validation = validate_owner(owner_login)
    return if owner_validation == :saml_session_required

    manifest = IntegrationManifest.new(
      owner: owner,
      creator: current_user,
      name: params[:integration_manifest][:name],
      data: params[:integration_manifest][:data],
    )

    if owner_validation == :success && manifest.save
      redirect_uri = Addressable::URI.parse(manifest.data["redirect_url"])
      query_values = { code: manifest.code }

      state = params[:state]
      query_values[:state] = state if state.present?

      redirect_uri.query_values = query_values

      render "integrations/settings/manifest_redirect",
        locals: { redirect_url: redirect_uri.to_s },
        layout: "redirect"
    else
      render_template_view "integrations/settings/new_from_manifest",
        Integrations::NewFromManifestView, manifest: manifest, owner: owner
    end
  end

  private

  def validate_owner(owner)
    return [current_user, :success] if owner == "current_user"
    return [current_user, :success] if current_user.login == owner

    requested_owner = current_user.owned_organizations.where(login: owner).first

    if (org = Organization.find_by(login: owner)) && manages_all_integrations?(user: current_user, owner: org)
      requested_owner = org
    end

    # check for SAML
    if !required_external_identity_session_present?(target: requested_owner)
      render_external_identity_session_required(target: requested_owner)
      return [nil, :saml_session_required] # abort manifest flow
    end

    return [requested_owner, :success] if requested_owner

    # default to not having permission
    flash[:notice] = "You don't have permission to create apps for #{owner}. You may still create this app for your own account."

    [current_user, :owner_login_mismatch]
  end

  def require_app_manifest_token_cookie
    return if cookies[:app_manifest_token]

    manifest_not_found!
  end

  def manifest_not_found!(msg: "We didn't find an App Manifest for your request.")
    flash[:error] = msg
    redirect_to new_settings_user_app_path
  end

  def current_context
    current_user
  end
end
