# frozen_string_literal: true

class Settings::DismissalsController < ApplicationController
  areas_of_responsibility :code_collab

  # The following actions do not require conditional access checks:
  # - create: doesn't access any protected organizations so
  # it doesn't require external session verification.
  skip_before_action :perform_conditional_access_checks, only: :create

  before_action :login_required

  # The browser performs a set to keyvalues when a POST request to /dismiss-notice/:notice
  #
  # Returns nothing.
  def create
    current_user.dismiss_notice(params[:notice])

    if request.xhr?
      head :ok
    else
      redirect_to :back
    end
  end
end
