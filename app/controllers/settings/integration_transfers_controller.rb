# frozen_string_literal: true

class Settings::IntegrationTransfersController < ApplicationController
  areas_of_responsibility :platform

  include IntegrationTransfersControllerMethods

  # The following actions do not require conditional access checks because
  # they *don't* access any protected organization resources.
  skip_before_action :perform_conditional_access_checks, only: %w(
    show
    accept
    destroy
  )

  javascript_bundle :settings

  private

  def current_context
    current_user
  end
end
