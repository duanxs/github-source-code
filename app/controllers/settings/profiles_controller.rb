# frozen_string_literal: true

class Settings::ProfilesController < ApplicationController
  areas_of_responsibility :user_profile

  before_action :login_required
  before_action :ensure_specified_user_is_current_user
  skip_before_action :perform_conditional_access_checks

  def update_activity_overview_enabled
    profile_settings = current_user.profile_settings
    profile_settings.activity_overview_enabled = params[:user][:activity_overview_enabled]
    current_user.dismiss_notice("org_scoped_activity_opt_in")

    if current_user.errors.any?
      flash[:error] = current_user.errors.full_messages.to_sentence
    else
      if profile_settings.activity_overview_enabled?
        flash[:contribution_graph_notice] = "Others will now see 'Activity overview' when they " \
                                            "view your profile."
      else
        flash[:contribution_graph_notice] = "The 'Activity overview' section will no longer " \
                                            "appear on your profile."
      end
    end

    redirect_to user_path(current_user)
  end

  def update_profile_badges_preference
    profile_settings = current_user.profile_settings

    unless params[:user][:pro_badge_enabled].nil?
      profile_settings.pro_badge_enabled = params[:user][:pro_badge_enabled]
    end

    if GitHub.flipper[:acv_badge].enabled?(current_user) && params[:user][:acv_badge_enabled].present?
      profile_settings.acv_badge_enabled = params[:user][:acv_badge_enabled]
    end

    if current_user.errors.any?
      flash[:error] = current_user.errors.full_messages.to_sentence
    else
      flash[:notice] = "Profile updated successfully"
    end

    redirect_to settings_user_profile_path
  end

  def update_mobile_opt_out_enabled
    profile_settings = current_user.profile_settings
    profile_settings.mobile_opt_out_enabled = params[:user][:mobile_opt_out_enabled]

    if current_user.errors.any?
      flash[:error] = current_user.errors.full_messages.to_sentence
    else
      flash[:mobile_opt_out] = true
    end

    if params[:redirect_back].present?
      redirect_to :back
    else
      redirect_to settings_user_admin_path
    end
  end

  def update_emoji_skin_tone_preference
    profile_settings = current_user.profile_settings
    profile_settings.set_preferred_emoji_skin_tone = params[:user][:emoji_skin_tone_preference]
    flash[:notice] = "Emoji skin tone preference successfully saved."
    redirect_to settings_user_profile_path
  end

  private

  def ensure_specified_user_is_current_user
    render_404 unless params[:id].downcase == current_user.login.downcase
  end
end
