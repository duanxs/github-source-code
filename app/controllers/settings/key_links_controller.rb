# frozen_string_literal: true

class Settings::KeyLinksController < AbstractRepositoryController
  areas_of_responsibility :repositories

  before_action :login_required
  before_action :writable_repository_required
  before_action :sudo_filter, only: [:new, :create]
  before_action :ensure_admin_access
  before_action :ensure_custom_key_links_enabled

  layout :repository_layout
  javascript_bundle :settings

  def index
    render "settings/key_links/index"
  end

  def new
    render "settings/key_links/new", locals: { key_link: KeyLink.new }
  end

  def check
    key_link = current_repository.key_links.build(key_link_params)

    if key_link.valid?
      render partial: "settings/key_links/preview", locals: { key_link: key_link }, layout: false
    else
      head :unprocessable_entity
    end
  end

  def create
    GitHub.context.push(spamurai_form_signals: spamurai_form_signals)
    key_link = current_repository.key_links.build(key_link_params)
    if key_link.save
      redirect_to key_links_path(current_repository.owner, current_repository)
    else
      flash.now[:error] = key_link.errors.full_messages.to_sentence
      GitHub.dogstats.increment("key_links", tags: ["action:create", "valid:false"])
      render "settings/key_links/new", locals: { key_link: key_link }
    end
  end

  def destroy
    current_repository.key_links.find(params[:id]).destroy
    redirect_to key_links_path(current_repository.owner, current_repository)
  end

  private

  def key_link_params
    params.require(:key_link).permit(:key_prefix, :url_template)
  end

  def ensure_custom_key_links_enabled
    render_404 unless current_repository.plan_supports?(:custom_key_links)
  end

end
