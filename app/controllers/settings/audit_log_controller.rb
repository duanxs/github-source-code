# frozen_string_literal: true

class Settings::AuditLogController < ApplicationController
  areas_of_responsibility :account_settings
  map_to_service :audit_logs

  statsd_tag_actions only: :index

  before_action :login_required
  skip_before_action :perform_conditional_access_checks

  def index
    render_template_view "settings/audit_log/index",
      Settings::AuditLog::IndexPageView,
      query: params[:q],
      page: page,
      after: params[:after],
      before: params[:before]
  end

  def suggestions
    headers["Cache-Control"] = "no-cache, no-store"

    respond_to do |format|
      format.html do
        render_partial_view "settings/audit_log/suggestions",
          Settings::AuditLog::SuggestionsView
      end
    end
  end

  protected

  def page
    params[:page].present? ? params[:page] : 1
  end
end
