# frozen_string_literal: true

class PaymentMethodController < ApplicationController
  include BillingSettingsHelper
  include OrganizationsHelper

  before_action :ensure_billing_enabled
  before_action :login_required
  before_action :ensure_target
  before_action :ensure_target_is_billable
  before_action :add_csp_exceptions, only: :show
  before_action only: [:show, :zuora_payment_show] do
    check_ofac_sanctions(target: target)
  end

  CSP_EXCEPTIONS = {
    img_src: [GitHub.paypal_checkout_url].freeze,
    connect_src: [GitHub.braintreegateway_url, GitHub.braintree_analytics_url].freeze,
  }.freeze

  def zuora_payment_show
    return render_404 if !request.xhr?
    # Ensure payment method ID is alphanumeric 32 characters
    if params[:payment_method_id].nil? || params[:payment_method_id] !~ /\A[0-9a-zA-Z]{32}\z/
      return render_404
    end

    begin
      payment_method = GitHub.zuorest_client.get_payment_method(params[:payment_method_id])
    rescue Zuorest::HttpError
      return render_404
    end

    return render_404 if payment_method["Type"] != "CreditCard"
    return render_404 if payment_method["CreditCardHolderName"] != current_user.id.to_s

    render json: {
      expiration_month: payment_method["CreditCardExpirationMonth"],
      expiration_year: payment_method["CreditCardExpirationYear"],
      masked_number: payment_method["CreditCardMaskNumber"],
      card_type: payment_method["CreditCardType"],
    }
  end

  def show
    @plan = GitHub::Plan.find(params[:plan]) if params[:plan]
    @plan = MunichPlan.wrap(@plan, target: target)

    @show_name_address_form = GitHub.flipper[:name_address_collection].enabled?(target) && target.user?
    @show_payment_form = if GitHub.flipper[:name_address_collection].enabled?(target) && target.user?
      target.has_personal_profile?
    else
      true
    end

    if @plan && !Billing::ChangeSubscription.can_perform?(target, plan: @plan, actor: target)
      flash[:error] = "#{@plan.display_name.capitalize} plan isn't available for #{target}"
    end

    if target.no_verified_emails?
      flash[:error] = "At least one email address must be verified to add or update your payment method"
      return redirect_to target_billing_path(target)
    end

    if target.spammy?
      flash[:error] = ::Billing::Zuora::HostedPaymentsPage::SPAMMY_MESSAGE
      return redirect_to target_billing_path(target)
    end

    render "payment_method/show"
  end

  def show_modal
    @modal_view = true
    @show_name_address_form = GitHub.flipper[:name_address_collection].enabled?(target) && target.user?
    @show_payment_form = if GitHub.flipper[:name_address_collection].enabled?(target) && target.user?
      target.has_personal_profile?
    else
      true
    end

    render partial: "payment_method/show_modal"
  end

  def destroy
    payment_method = target.friendly_payment_method_name # find out what it is before we clear it.

    if target.payment_method && target.payment_method.clear_payment_details(current_user)
      flash[:notice] = "Your #{payment_method} has been removed."
    else
      flash[:error] = "Failed to remove your #{payment_method}. #{GitHub.support_link_text} for help."
    end

    redirect_to target_billing_path(target)
  end

  def target_for_conditional_access
    return :no_target_for_conditional_access unless target
    target
  end

  def zuora_payment_page_signature
    page_id = GitHub.flipper[:name_address_collection].enabled?(target) && !target&.organization? ? GitHub.zuora_self_serve_compact_payment_page_id : GitHub.zuora_self_serve_payment_page_id
    page_params = ::Billing::Zuora::HostedPaymentsPage.params(
      page_id:  page_id,
      target: target,
    )

    render json: page_params
  end

  private

  def ensure_target
    render_404 if target.nil?
  end

  def target
    @target ||= target!
  end

  def target!
    if params[:organization_id]
      org = current_organization_for_member_or_billing
      if org && org.billing_manageable_by?(current_user)
        org
      end
    else
      current_user
    end
  end

  def ensure_target_is_billable
    render_404 unless target.billable?
  end
end
