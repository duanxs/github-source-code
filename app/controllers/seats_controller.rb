# frozen_string_literal: true
class SeatsController < ApplicationController
  include BillingSettingsHelper
  include OrganizationsHelper
  include ActionView::Helpers::TextHelper
  include SeatsHelper
  include MarketingMethods

  before_action :ensure_billing_enabled
  before_action :login_required
  before_action :ensure_target

  before_action :ensure_per_seat_plan, except: [:switch]
  before_action :ensure_downgradable, only: [:remove_seats]
  before_action :ensure_not_invoiced, only: [:show]
  before_action :use_available_pending_plan_change, only: [:remove_seats]
  before_action :load_seat_change, only: [:show, :remove_seats, :update]
  before_action :set_header_for_ajax, except: [:update, :switch]

  def show
    url = org_seats_path(target, seats: seat_delta, return_to: params[:return_to])

    respond_to do |format|
      format.json do
        render json: hash_for_seat_change(seat_delta, @seat_change, url)
      end

      format.html do
        render "seats/show"
      end
    end
  end

  def remove_seats
    url = remove_org_seats_path(target, seats: seat_delta, return_to: params[:return_to])

    respond_to do |format|
      format.json do
        render json: hash_for_seat_change(seat_delta, @seat_change, url)
      end

      format.html do
        render "seats/remove_seats"
      end
    end
  end

  def cancel
    unless satisfies_custom_role_reqirements?
      flash[:error] = custom_role_error_message
      redirect_to target_billing_path(target) and return
    end

    SurveyAnswer.save_as_group(current_user.id, params[:survey_id], params[:answers])

    result = GitHub::Billing.change_subscription(target, actor: current_user, plan: GitHub::Plan.free)

    if result.success?
      flash[:notice] = "You've successfully canceled your plan for this organization, sorry to see you go!"
    else
      flash[:error] = result.error_message
    end

    redirect_to target_billing_path(target)
  end

  def switch
    new_plan_name = params[:new_plan].presence || params[:plan].presence

    new_plan = GitHub::Plan.find(new_plan_name)
    unless satisfies_custom_role_reqirements?(new_plan)
      flash[:error] = custom_role_error_message(new_plan)
      redirect_to target_billing_path(target) and return
    end

    payment_method_id = params.dig(:billing, :zuora_payment_method_id)
    is_new_payment_method = target.payment_method&.payment_token != payment_method_id

    SurveyAnswer.save_as_group(current_user.id, params[:survey_id], params[:answers])

    old_plan_name = target.plan.name
    old_seat_count = target.seats

    per_seat_pricing_model = Billing::PlanChange::PerSeatPricingModel.new(
      target,
      seats: params[:seats],
      plan_duration: params[:plan_duration] || target.plan_duration,
      new_plan: new_plan_name,
    )

    success = per_seat_pricing_model.switch \
      actor: current_user,
      payment_details: payment_details,
      business_owned: !!params[:business_owned],
      organization_details: params[:organization]

    if success
      if per_seat_pricing_model.duration_change_only?
        flash[:notice] = "You have been successfully switched to #{target.pending_cycle_plan_duration}ly billing."
      else
        flash[:notice] = "Your plan was changed successfully."
        analytics_ec_purchase(target, per_seat_pricing_model.final_price, "Upgrade")
      end

      publish_billing_plan_changed_for(
        actor: current_user,
        user: target,
        old_plan_name: old_plan_name,
        new_plan_name: new_plan_name,
        old_seat_count: old_seat_count,
        new_seat_count: target.seats,
      )

      if is_new_payment_method
        publish_payment_method_changed_for(
          actor: current_user,
          account: target,
          payment_method: target.payment_method,
        )
      end

      notify_marketing_of_cloud_trial_upgrade(target, new_plan_name, :self_serve)
    else
      flash[:error] = per_seat_pricing_model.error_messages.join
    end

    analytics_event organization_plan_change_ga_event_attributes(success, target, current_user, old_plan_name, new_plan_name || target.plan.name, old_seat_count, target.seats)

    if params[:return_to].present?
      safe_redirect_to params[:return_to], fallback: target_billing_path(target)
    else
      redirect_to target_billing_path(target)
    end
  end

  def update
    old_seat_count = target.seats

    result = GitHub::Billing.change_seats(target, seats: @seat_change.seats, seat_delta: seat_delta, actor: current_user)

    if result.success?
      if target.save
        seat_change_ga_label = [:old_seats, :seats].map { |kind|
          seats_volume_bucket(@seat_change.send(kind))
        }.uniq.join(" -> ")

        if seat_delta > 0
          flash[:notice] = "Your purchase of #{pluralize(seat_delta, "more seat")} was successful. Thanks!"

          publish_billing_seat_count_change_for(
            actor: current_user,
            user: target,
            old_seat_count: old_seat_count,
            new_seat_count: @seat_change.seats,
          )
          analytics_event \
            category: "Orgs",
            action: "upgrade seats",
            label: seat_change_ga_label,
            value: seat_delta,
            redirect: true
        else
          flash[:notice] = "You have successfully downgraded to #{pluralize(target.pending_cycle_seats, "seat")}."
          analytics_event \
            category: "Orgs",
            action: "downgrade seats",
            label: seat_change_ga_label,
            redirect: true
        end

        if params[:return_to].present?
          safe_redirect_to params[:return_to]
        else
          redirect_to target_billing_path(target)
        end

        return
      else
        flash.now[:error] = target.errors.full_messages.join
      end
    else
      flash.now[:error] = result.error_message
    end

    render "seats/show"
  end

  private
  def seat_delta
    if billing_params = params[:billing]
      billing_params.fetch(:seats, 0)
    else
      params[:seats] || default_seat_delta
    end.to_i
  end

  def use_available_pending_plan_change
    next_pending_plan_change = target.pending_cycle_change
    if next_pending_plan_change&.changing_plan?
      target.plan = next_pending_plan_change.plan
    end
  end

  def load_seat_change
    @seat_change ||= Billing::PlanChange::SeatChange.new(target,
      seats: target.seats + seat_delta)
  end

  def set_header_for_ajax
    # Make sure the browser caches AJAX responses separately from regular HTML responses.
    response.headers["Vary"] = "X-Requested-With"
  end

  def default_seat_delta
    case action_name
    when "remove_seats"
      -1
    when "show"
      1
    end
  end

  def ensure_downgradable
    redirect_to target_billing_path(target) unless target.has_downgradable_seats?
  end

  def ensure_per_seat_plan
    redirect_to target_billing_path(target) unless target.plan.per_seat?
  end

  def ensure_not_invoiced
    if target.invoiced?
      redirect_to target_billing_path(target)
    end
  end

  def ensure_target
    render_404 if target.nil?
  end

  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def target
    @target ||= begin
      org = current_organization_for_member_or_billing
      if org && org.billing_manageable_by?(current_user)
        org
      end
    end
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  def publish_billing_plan_changed_for(actor:,
                                       user:,
                                       new_seat_count: 0,
                                       old_seat_count: 0,
                                       old_plan_name: "",
                                       new_plan_name:)
    GlobalInstrumenter.instrument(
      "billing.plan_change",
      actor_id: actor.id,
      user_id: user.id,
      old_plan_name: old_plan_name,
      old_seat_count: old_seat_count,
      new_plan_name: new_plan_name,
      new_seat_count: new_seat_count,
    )
  end

  def publish_payment_method_changed_for(actor:, account:, payment_method:)
    GlobalInstrumenter.instrument(
      "billing.payment_method.addition",
      actor_id: actor.id,
      account_id: account.id,
      payment_method_id: payment_method.id,
    )
  end

  # Internal: billing changes from business_plus to any other plan require the organization
  # to not have any custom roles.
  #
  # - new_plan: the new GitHub::Plan.
  #
  # Returns a Boolean.
  def satisfies_custom_role_reqirements?(new_plan = nil)
    return true if target.plan != GitHub::Plan.business_plus && new_plan != GitHub::Plan.business_plus && !GitHub.flipper[:custom_roles].enabled?(target)

    target.custom_roles.count == 0
  end

  # Internal: the error message to be rendered if custom role requirements are not met.
  #
  # - new_plan: the new GitHub::Plan.
  #
  # Returns a String.
  def custom_role_error_message(new_plan = GitHub::Plan.free)
    "The #{new_plan.titleized_display_name} plan does not support custom roles. Please delete all custom roles before downgrading billing plan."
  end
end
