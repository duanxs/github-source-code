# frozen_string_literal: true

class ComposableCommentsController < AbstractRepositoryController
  areas_of_responsibility :ce_extensibility

  statsd_tag_actions only: :show

  include ShowPartial

  before_action :login_required
  before_action :require_xhr

  ComposableCommentQuery = parse_query <<-'GRAPHQL'
    query($id: ID!) {
      node(id: $id) {
        ...Views::ComposableComments::Show::ComposableComment
      }
    }
  GRAPHQL

  def show_partial
    partial = params[:partial]
    return head :not_found unless valid_partial?(partial)

    composable_comment = ComposableComment.find(params[:composable_comment_id])

    composable_comment_node = platform_execute(
      ComposableCommentQuery,
      variables: { id: composable_comment.global_relay_id },
    ).node

    render partial: "composable_comments/show", formats: [:html], locals: { composable_comment: composable_comment_node }
  end
end
