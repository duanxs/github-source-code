# frozen_string_literal: true

class EditRepositories::ManageAccessController < AbstractRepositoryController
  areas_of_responsibility :repo_settings, :code_collab

  before_action :login_required
  before_action :repo_access_management_enabled, only: [:add_member, :members_toolbar_actions, :update_members, :remove_members]
  before_action do
    render "repositories/states/trade_controls_read_only" if current_repository.trade_controls_read_only?
  end
  before_action :sudo_filter, only: [:add_member, :members_toolbar_actions, :update_members]
  before_action :ensure_admin_access
  before_action :only_org_access, only: :role_details

  layout :repository_layout
  javascript_bundle :settings

  PLAN_PROTECTED_ROLES = ["triage", "maintain"]

  def add_member
    member_type, member = params[:member].to_s.split("/")
    begin
      if current_repository.in_organization?
        role = find_role_name!(params[:role])
      else
        # collaborators on user-owned repositories can only be given write permission
        role = :write
      end
    rescue ArgumentError, ActiveRecord::RecordNotFound
      flash[:error] = "Invalid role specified."
      redirect_to :back
      return
    end

    case member_type
    when "user"
      add_user(member, role: role)
    when "team"
      add_team(member.to_i, role: role)
    else
      render_404
    end
  end

  def update_members
    begin
      action = find_role_name!(params[:role])
    rescue ArgumentError, ActiveRecord::RecordNotFound
      flash[:error] = "Invalid permission specified."
      redirect_to :back and return
    end

    enqueued_jobs = false

    if params[:user_ids]&.any?
      members = fetch_members_by_ids(action_filter: action)
      GitHub.dogstats.gauge("edit_repositories.update_members.count", members.count, tags: ["member_type:member", "action:#{action}"])

      org_owner_ids =
        if current_repository.in_organization?
          current_repository.owner.admin_ids(actor_ids: members.pluck(:id))
        else
           []
        end

      user_roles = current_repository.direct_roles_for(members, actor_type: "User")
      user_roles.each do |member, role|
        next if org_owner_ids.include?(member.id)

        if role != action
          current_repository.enqueue_update_member(member, action: action)
          enqueued_jobs = true
        end
      end
    end

    if params[:invitation_ids]&.any?
      invitations = fetch_invitations(params[:invitation_ids])
      GitHub.dogstats.gauge("edit_repositories.update_members.count", invitations.count, tags: ["member_type:invitee", "action:#{action}"])

      invitations.each do |invitation|
        unless invitation.permissions == action.to_s
          invitation.enqueue_update_repo_permissions(setter: current_user, action: action)
          enqueued_jobs = true
        end
      end
    end

    if params[:team_ids]&.any?
      teams = fetch_teams_by_ids
      GitHub.dogstats.gauge("edit_repositories.update_members.count", teams.count, tags: ["member_type:team", "action:#{action}"])

      teams.each do |team|
        unless current_repository.direct_role_for(team) == action
          team.enqueue_update_repo_permissions(repo: current_repository, action: action)
          enqueued_jobs = true
        end
      end
    end

    if enqueued_jobs
      flash[:notice] = "Permissions will be updated for the selected members. Please wait a few minutes and then refresh the page to see your changes."
    else
      flash[:notice] = "No permissions required updating."
    end
    redirect_to :back and return
  end

  def remove_members
    if params[:invitation_ids]&.any?
      invitations = fetch_invitations(params[:invitation_ids])
      GitHub.dogstats.gauge("edit_repositories.remove_members.count", invitations.count, tags: ["member_type:invitee"])

      invitations.each do |invitation|
        invitation.enqueue_cancel_invitation(actor: current_user)
      end
    end

    if params[:user_ids]&.any?
      members = fetch_members_by_ids(action_filter: :none)
      GitHub.dogstats.gauge("edit_repositories.remove_members.count", members.count, tags: ["member_type:member"])

      members.each do |member|
        current_repository.enqueue_remove_member(member, remover: current_user)
      end
    end

    if params[:team_ids]&.any?
      teams = fetch_teams_by_ids
      GitHub.dogstats.gauge("edit_repositories.remove_members.count", teams.count, tags: ["member_type:team"])

      teams.each do |team|
        team.enqueue_remove_repository(current_repository)
      end
    end

    # If current user removes themself, we need to redirect away from the page
    if members&.include?(current_user)
      redirect_to "/", flash: { notice: "Removed yourself as a collaborator of #{current_repository.name_with_owner}" }
    else
      redirect_to :back, flash: { notice: "The selected members will be removed from #{current_repository.name_with_owner}. Please wait a few minutes and then refresh the page to see your changes." }
    end
  end

  def members_toolbar_actions
    selected = {}
    selected[:user_ids] = direct_member_ids + collaborator_ids if params[:user_ids].present?
    if params[:invitation_ids].present?
      selected[:invitation_ids] = current_repository.repository_invitations.where(id: params[:invitation_ids].map(&:to_i)).pluck(:id)
    end
    if params[:team_ids].present?
      selected[:team_ids] = current_repository.teams.where(id: params[:team_ids].map(&:to_i)).pluck(:id)
    end

    respond_to do |format|
      format.html do
        render_partial_view "edit_repositories/pages/members_toolbar_actions", EditRepositories::Pages::MemberToolbarActionsView,
          repository: current_repository,
          selected_ids: selected
      end
    end
  end

  def role_details
    render_template_view "edit_repositories/pages/role_details",
      ::EditRepositories::Pages::RoleDetailsView,
      repository: current_repository,
      organization: current_repository.organization
  end

  private

  def add_user(member, role:)
    begin
      if User.valid_email?(member)
        email = member
        result = RepositoryInvitation.invite_to_repo_by_email(
          email,
          current_user,
          current_repository,
          action: role,
        )
      else
        invitee = User.find(member)
        result = RepositoryInvitation.invite_to_repo(
          invitee,
          current_user,
          current_repository,
          action: role,
        )
      end
    rescue ::Permissions::Granters::RoleGranter::GrantFailure => e
      flash[:error] = e.message
      redirect_back fallback_location: repository_access_management_path(current_repository.owner, current_repository) and return
    end

    role_message = "with #{role} permissions"
    invitee_label = User.valid_email?(member) ? member : invitee.login

    if result[:success]
      flash[:notice] = "#{invitee_label} has been added as a collaborator #{role_message if current_repository.in_organization?} on the repository."
    else
      flash[:error] = result[:errors].messages.values.join(", ")
    end

    redirect_to repository_access_management_path(current_repository.owner, current_repository)
  end

  def add_team(team_id, role:)
    team = current_repository.organization.teams.find(team_id)

    if current_repository.teams.include?(team)
      return respond_to do |wants|
        wants.html { redirect_to repository_access_management_path(current_repository.owner, current_repository) }
        wants.json { render json: {error: error_message_for(Team::ModifyRepositoryStatus::DUPE)} }
      end
    end

    if current_repository.can_add_to_team?(team, adder: current_user)
      status = team.add_repository(current_repository, role)

      if status == Team::ModifyRepositoryStatus::SUCCESS
        respond_to do |wants|
          wants.html do
            flash[:notice] = "#{team.combined_slug} has been granted #{role} on the repository."
            redirect_to repository_access_management_path(current_repository.owner, current_repository)
          end
          wants.json do
            render json: {
              name: team.name,
               html: render_to_string(
                partial: "edit_repositories/admin_screen/team", locals: {
                  team: team,
                  organization: team.organization,
                  repository: current_repository,
                  action: team.async_most_capable_action_or_role_for(current_repository).sync,
                },
                formats: [:html]
              ),
            }
          end
        end
      else
        respond_to do |wants|
          wants.html { redirect_to repository_access_management_path(current_repository.owner, current_repository) }
          wants.json { render json: { error: error_message_for(status) } }
        end
      end
    else
      respond_to do |wants|
        wants.html { redirect_to repository_access_management_path(current_repository.owner, current_repository) }
        wants.json { render json: {error: "Team not found"} }
      end
    end
  end

  def direct_member_ids
    member_ids = params[:user_ids].map(&:to_i)

    member_ids & (current_repository.direct_member_ids.to_a - current_repository.outside_collaborators_ids.to_a)
  end

  def collaborator_ids
    member_ids = params[:user_ids].map(&:to_i)

    member_ids & current_repository.outside_collaborators_ids.to_a
  end

  # Internal: fetch members with access to the repo.
  # In the context of org-owned repos, it does not make sense to make the permission lower than the org defaults.
  # If the input action is lower than the default permission, we don't return any org members to be updated.
  #
  # action_filter  - the target permission
  #
  # Returns an ActiveRecord::Relation
  def fetch_members_by_ids(action_filter: :none)
    if current_repository.in_organization?
      member_ids =
        if role_assignable_to_all_members?(action_filter)
          direct_member_ids + collaborator_ids
        else
          collaborator_ids
        end

      User.where(id: member_ids)
    else
      User.where(id: collaborator_ids)
    end
  end

  def role_assignable_to_all_members?(action_filter)
    # action should only be :none when we're using this to fetch members for deletion
    action_filter == :none || role_greater_than_default?(action_filter)
  end

  # Internal: is the passed in role greater than the default role
  # If a custom role is passed in, we check if its base role is greater than
  # the org's default role (if there is one)
  #
  # role - the target permission name
  #
  # Returns Boolean
  def role_greater_than_default?(role)
    default_perm = current_repository.organization.default_repository_permission

    return true unless default_perm
    role =
      if !Role.valid_system_role?(role)
        Role.for_org(name: role, org: current_repository.organization).base_role.name
      else
        role
      end

    Ability::ACTION_RANKING[role.to_sym] >= Ability::ACTION_RANKING.fetch(default_perm, 0)
  end

  # Internal: fetch teams with access to the repo.
  #
  # Returns an ActiveRecord::Relation
  def fetch_teams_by_ids
    current_repository.organization.teams.where(id: params[:team_ids])
  end

  def fetch_invitations(invitation_ids)
    current_repository.repository_invitations.where(id: invitation_ids)
  end

  helper_method :current_repository

  # We allow access to the admin page in some cases where we don't allow access
  # to the code. To allow for this, we must override the
  # AbstractRepositoryController#ask_the_gatekeeper method for these cases
  #
  # See AbstractRepositoryController#ask_the_gatekeeper for more details
  def ask_the_gatekeeper
    repo = current_repository
    state = params[:fakestate] if real_user_site_admin?

    super if repo&.private? && (repo&.trade_restricted_by_owner? || current_user&.has_any_trade_restrictions?)

    # Allow disabled accounts to edit their repos
    unless (repository_specified? && repo && repo.disabled?) || state == "disabled"
      super
    end
  end

  # Override RepositoryControllerMethods#privacy_check.
  def privacy_check
    permission = current_repository.async_action_or_role_level_for(current_user).sync

    if ![:maintain, :admin].include?(permission)
      if logged_in? && current_user.site_admin?
        render "admin/locked_repo"
      else
        render_404
      end
    end
  end

  def at_least_maintain_required
    role = current_repository.async_action_or_role_level_for(current_user).sync
    render_404 if !role || Ability::ACTION_RANKING[role] < Ability::ACTION_RANKING[:maintain]
  end

  # returns a the provided role name if it exists as a valid role in the repository
  # it would raise if the role does not exists or is not supported by the current billing plan
  def find_role_name!(role_name)
    if Role.valid_system_role?(role_name)
      if PLAN_PROTECTED_ROLES.include?(role_name)
        raise ArgumentError, "Role: #{role_name}, is not supported for the current billing plan" unless current_repository.fine_grained_permissions_supported?
      end

      Repository.permission_to_action(role_name)
    else
      raise ArgumentError, "Role: #{role_name}, is not supported for the current billing plan" unless current_repository.custom_roles_supported?
      role = Role.find_by!(name: role_name, owner_id: current_repository.organization.id, owner_type: "Organization")
      role.name
    end
  end

  def repo_access_management_enabled
    render_404 unless repo_access_management_enabled?
  end

  def repo_access_management_enabled?
    GitHub.flipper[:repo_access_management].enabled?(current_repository.owner) || GitHub.flipper[:repo_access_management].enabled?(current_user)
  end

  def only_org_access
    render_404 unless current_repository.in_organization? && GitHub.flipper[:custom_roles].enabled?(current_repository.owner)
  end
end
