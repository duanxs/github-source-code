# frozen_string_literal: true

class WorksWithGitHubListingsController < ApplicationController
  areas_of_responsibility :marketplace

  before_action :marketplace_required
  before_action :works_with_enabled

  layout "site"

  IndexQuery = parse_query <<-'GRAPHQL'
    query($mktPerPage: Int!, $wwgPerPage: Int!, $mktAfter: String, $wwgAfter: String, $query: String!, $categorySlug: String, $selectedCategory: Boolean!, $queryPresent: Boolean!, $enterpriseCompatibleOnly: Boolean) {
      ...Views::WorksWithGitHubListings::Index::Root
    }
  GRAPHQL

  IndexPjaxQuery = parse_query <<-'GRAPHQL'
    query($mktPerPage: Int!, $wwgPerPage: Int!, $mktAfter: String, $wwgAfter: String, $query: String!, $categorySlug: String, $selectedCategory: Boolean!, $queryPresent: Boolean!, $enterpriseCompatibleOnly: Boolean) {
      ...Views::WorksWithGitHubListings::Listings::Root
    }
  GRAPHQL

  MARKETPLACE_LISTINGS_PER_PAGE = 100
  WWG_LISTINGS_PER_PAGE = 26

  def index
    search_query = params[:query] || ""

    graphql_query = pjax? ? IndexPjaxQuery : IndexQuery
    enterprise_compatible = if params[:enterprise_compatible].present?
      true
    end
    data = platform_execute(graphql_query, variables: {
      mktPerPage: MARKETPLACE_LISTINGS_PER_PAGE, mktAfter: params[:mkt_after],
      wwgPerPage: WWG_LISTINGS_PER_PAGE, wwgAfter: params[:wwg_after],
      query: search_query, categorySlug: params[:slug], selectedCategory: params[:slug].present?,
      queryPresent: search_query.present?, enterpriseCompatibleOnly: enterprise_compatible
    })

    respond_to do |format|
      format.html do
        if pjax?
          render partial: "works_with_github_listings/listings",
            locals: { data: data, query: search_query,
                      enterprise_compatible: enterprise_compatible }
        else
          render "works_with_github_listings/index",
            locals: { data: data, query: search_query,
                      enterprise_compatible: enterprise_compatible }
        end
      end
    end
  end

  def new
    respond_to do |format|
      format.html do
        render "works_with_github_listings/new"
      end
    end
  end

  EditQuery = parse_query <<-'GRAPHQL'
    query($id: ID!) {
      node(id: $id) {
        ... on NonMarketplaceListing {
          viewerCanEdit
          ...Views::WorksWithGitHubListings::Edit::NonMarketplaceListing
        }
      }
    }
  GRAPHQL

  def edit
    data = platform_execute(EditQuery, variables: { id: params[:listing_id] })
    listing = data.node

    return render_404 unless listing && listing.viewer_can_edit?

    respond_to do |format|
      format.html do
        render "works_with_github_listings/edit", locals: { listing: listing }
      end
    end
  end

  private

  def works_with_enabled
    if feature_enabled_globally_or_for_current_user?(:marketplace_works_with_deprecation)
      redirect_to marketplace_path
    end
  end

  def require_conditional_access_checks?
    false
  end
end
