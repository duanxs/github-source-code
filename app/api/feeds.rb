# frozen_string_literal: true

class Api::Feeds < Api::App
  areas_of_responsibility :news_feeds, :api

  # EXPERIMENTAL
  get "/feeds" do
    @route_owner = "@github/experience-engineering-work"
    @documentation_url = "/rest/reference/activity#get-feeds"

    control_access :public_site_information,
      resource: Platform::PublicResource.new,
      allow_integrations: true,
      allow_user_via_integration: true

    timeline_href            = html_url "/timeline"
    user_href                = html_url "/{user}"
    security_advisories_href = html_url "/security-advisories"

    feeds = {
      timeline_url: timeline_href,
      user_url: user_href,
    }
    links = {
      timeline: atom_link(timeline_href),
      user: atom_link(user_href),
    }

    # Only real User actors can have a feed
    if logged_in? && current_user.user?
      public_user_href = html_url "/#{current_user}"
      feeds[:current_user_public_url] = public_user_href
      links[:current_user_public] = atom_link(public_user_href)

      # Don't give out token urls to oauth clients or to integrations
      if !@oauth && !current_integration
        current_user_href = html_url "/#{current_user}.private.atom", auth: true
        actor_href = html_url "/#{current_user}.private.actor.atom", auth: true

        org_hrefs = current_user.organizations.includes(:saml_provider).reject(&:saml_sso_enabled?).map do |org|
          html_url "/organizations/#{org}/#{current_user}.private.atom", auth: true
        end

        links.update \
          current_user: atom_link(current_user_href),
          current_user_actor: atom_link(actor_href),
          current_user_organization: {href: "", type: ""},
          current_user_organizations: atom_link(org_hrefs)
        feeds.update \
          current_user_url: current_user_href,
          current_user_actor_url: actor_href,
          current_user_organization_url: "",
          current_user_organization_urls: org_hrefs
      end
    end

    if GitHub.security_advisories_enabled?
      feeds.update security_advisories_url: security_advisories_href
      links.update security_advisories: atom_link(security_advisories_href)
    end

    feeds[:_links] = links

    deliver_raw feeds
  end

  private
  ATOM = Mime[:atom].to_s

  # Private: Generate an Atom feed link.
  #
  # href - A String url or an Array of String urls.
  #
  # Returns a Hash link or an Array of Hash links.
  def atom_link(href)
    case href
    when Array
      href.map { |h| atom_link h }
    when String
      {href: href, type: ATOM}
    else
      raise ArgumentError
    end
  end
end
