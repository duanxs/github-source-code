# frozen_string_literal: true

class Api::RepositoryCodeScanning < Api::App
  areas_of_responsibility :api

  # Handle a status report
  put "/repositories/:repository_id/code-scanning/analysis/status" do
    @route_owner = "@github/dsp-code-scanning"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL

    repo = ActiveRecord::Base.connected_to(role: :reading) { find_repo! }
    # This endpoint is only exposed when reached from Actions
    return deliver_error!(404) unless current_integration&.launch_github_app? || current_integration&.launch_lab_github_app?
    # Report a user-friendly message if the repo is not opted in to code scanning
    return deliver_error(403, message: GitHub.enterprise? ? "Code scanning is not enabled. Please contact your local GitHub Enterprise site administrator for assistance." : "repository not enabled for code scanning") unless repo.code_scanning_enabled?

    if repo.code_scanning_pr_upload_enabled?
      # This repo allows uploads with only read access for refs/pull.
      control_access :upload_code_scanning_analysis_read,
        resource: repo,
        allow_integrations: true,
        allow_user_via_integration: false,
        forbid: repo.public?

      data = receive_with_schema("code-scanning-analysis", "status")

      unless data["ref"]&.start_with? "refs/pull/"
        control_access :write_code_scanning,
          resource: repo,
          allow_integrations: true,
          allow_user_via_integration: false,
          forbid: repo.public?
      end
    else
      control_access :write_code_scanning,
        resource: repo,
        allow_integrations: true,
        allow_user_via_integration: false,
        forbid: repo.public?

      data = receive_with_schema("code-scanning-analysis", "status")
    end

    data[:repository_id] = repo.id

    begin
      data["started_at"] = DateTime.parse(data["started_at"]) if data["started_at"].present?
    rescue ArgumentError
      return deliver_error!(400, message: "started_at is not a valid date")
    end
    begin
      data["action_started_at"] = DateTime.parse(data["action_started_at"]) if data["action_started_at"].present?
    rescue ArgumentError
      return deliver_error!(400, message: "action_started_at is not a valid date")
    end
    begin
      data["completed_at"] = DateTime.parse(data["completed_at"]) if data["completed_at"].present?
    rescue ArgumentError
      return deliver_error!(400, message: "completed_at is not a valid date")
    end

    GitHub::Turboscan.report_status(data)

    deliver_empty(status: 200)
  end

  # Handle a SARIF upload
  post "/repositories/:repository_id/code-scanning/sarifs" do
    @route_owner = "@github/dsp-code-scanning"
    @documentation_url = "/rest/reference/code-scanning#upload-a-sarif-analysis"

    repo = ActiveRecord::Base.connected_to(role: :reading) { find_repo! }
    return deliver_error!(404) unless repo.code_scanning_integration_api_enabled_for?(current_integration || current_user)

    control_access :write_code_scanning,
      resource: repo,
      allow_integrations: true,
      allow_user_via_integration: true,
      forbid: repo.public?

    data = receive_with_schema("code-scanning-analysis", "upload-sarif")

    # Normalize to commit_oid internally, and validate commit and ref for non pull requests
    data["commit_oid"] = data["commit_sha"]
    if !data["ref"].start_with?("refs/pull")
      return deliver_error!(404, message: "ref not found") unless repo.refs.find(data["ref"])
      begin
        repo.commits.find(data["commit_oid"])
      rescue GitRPC::InvalidObject, GitRPC::ObjectMissing
        return deliver_error!(404, message: "commit not found")
      end
    end

    data["tool_names"] = Array(data["tool_name"])

    resp = upload_analysis(repo, data, can_write: true)
    deliver_raw resp.body, status: resp.status
  end

  # Handle a SARIF upload (Actions Only)
  put "/repositories/:repository_id/code-scanning/analysis" do
    @route_owner = "@github/dsp-code-scanning"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL

    repo = ActiveRecord::Base.connected_to(role: :reading) { find_repo! }
    # This endpoint is only exposed when reached from Actions, because we allow the PR from forks flow in which
    # upload is enabled via a read token
    return deliver_error!(404) unless current_integration&.launch_github_app? || current_integration&.launch_lab_github_app?

    can_write = false
    if repo.code_scanning_pr_upload_enabled?
      # This repo allows uploads with only read access for refs/pull.
      control_access :upload_code_scanning_analysis_read,
        resource: repo,
        allow_integrations: true,
        allow_user_via_integration: false,
        forbid: repo.public?

      data = receive_with_schema("code-scanning-analysis", "upload-analysis")

      # Do we have write access?
      can_write = access_allowed? :write_code_scanning,
                  resource: repo,
                  allow_integrations: true,
                  allow_user_via_integration: false

      # User must have write access unless we are uploading PR data
      return deliver_error(403) unless can_write || data["ref"]&.start_with?("refs/pull/")

    else
      control_access :write_code_scanning,
        resource: repo,
        allow_integrations: true,
        allow_user_via_integration: false,
        forbid: repo.public?

      data = receive_with_schema("code-scanning-analysis", "upload-analysis")
      can_write = true
    end

    resp = upload_analysis(repo, data, can_write)
    deliver_raw resp.body, status: resp.status
  end

  patch "/repositories/:repository_id/code-scanning/alerts/:alert_number" do
    @route_owner = "@github/dsp-code-scanning"
    @documentation_url = "/rest/reference/code-scanning#update-a-code-scanning-alert"

    repo = find_repo!
    return deliver_error!(404) unless repo.code_scanning_integration_api_enabled_for?(current_integration || current_user)

    control_access :write_code_scanning,
      resource: repo,
      allow_integrations: true,
      allow_user_via_integration: true,
      forbid: repo.public?

    data = receive_with_schema("code-scanning-alert", "set-alert-status")

    if data["state"] == "dismissed"
      if data["dismissed_reason"].nil?
        return deliver_error!(400, message: "Setting an alert to \"dismissed\" requires a \"dismissed_reason\"")
      end
      dismissed_sym = GitHub::Turboscan.resolution_reason_sym(data["dismissed_reason"])
      resolution = GitHub::Turboscan.to_resolution(dismissed_sym)
    else
      if data["dismissed_reason"]&.present?
        return deliver_error!(400, message: "Can't set a \"dismissed_reason\" when setting an alert to \"open\"")
      end
      resolution = :NO_RESOLUTION
    end

    set_alert_status_options = {
      repository_id: repo.id,
      number: alert_number,
      resolver_id: current_user.id,
      resolution: resolution,
    }

    # turboscan will update the alert status successfully as long as the
    # alert exists and turboscan is running.
    # TODO: turboscan currently doesn't distinguish between if the alert was
    # modified or not. If we make it do so, then the check run job triggering
    # below should be conditional on if the status changed or not.
    response = GitHub::Turboscan.set_alert_status(set_alert_status_options)

    if response&.error&.code == :not_found
      return deliver_error! 404, message: "Couldn't find alert #{alert_number} to set status", documentation_url: @documentation_url
    elsif response.blank? || response.error.present?
      return deliver_error! 503, message: "Code scanning unavailable. Please try again later."
    end

    repo.refresh_code_scanning_status(alert_numbers: [alert_number])

    deliver :code_scanning_alert_hash, response.data.result, repo: repo
  end

  get "/repositories/:repository_id/code-scanning/alerts" do
    @route_owner = "@github/dsp-code-scanning"
    @documentation_url = "/rest/reference/code-scanning#list-code-scanning-alerts-for-a-repository"

    repo = find_repo!

    # NOTE: This is explicitely not checking the code_scanning_integration_api_enabled_for? predicate, as we
    #       have existing users that might not be enabled, and do not want to break those integrations for now.

    control_access :read_code_scanning,
      resource: repo,
      allow_integrations: true,
      allow_user_via_integration: true,
      forbid: repo.public?

    # NOTE: This is deprecated, and should use refs instead. Keeping for now for backwards compatibility.
    ref_names = if params[:ref].nil?
                  repo.default_code_scanning_ref_names
                else
                  if !params[:ref].start_with?("refs/")
                    Array("refs/#{ref_names}")
                  else
                    Array(params[:ref])
                  end
    end

    new_format = repo.code_scanning_integration_api_enabled_for?(current_integration || current_user)
    # fetch only for the default branch (rather than default + protected) if ref wasn't specified
    if new_format
      ref_names = Array(ref_names[0])
    end

    # Ref is deprecated. Should use refs instead, that is a comma,delimited,string
    if (refs = params[:refs]).present? && refs.respond_to?(:split)
        ref_names = refs.split(",")
    end

    if new_format
      state = GitHub::Turboscan.to_alert_state_filter(params[:state])
    else
      # fall back on the old filtering behaviour of open only unless params[:state] == "closed"
      state = params[:state] == "closed" ? :ALERT_STATE_FILTER_CLOSED : :ALERT_STATE_FILTER_OPEN
    end

    ts_request_params = {
      repository_id: repo.id,
      ref_names: ref_names,
      state: state,
      limit: per_page,
      numeric_page: pagination[:page],
    }

    if new_format
      ts_request_params[:sort_order] = :NUMBER_DESCENDING
    end

    response = GitHub::Turboscan.results(ts_request_params)

    if response.blank? || response.error.present?
      deliver_error!(503, message: "Code scanning unavailable. Please try again later.")
    elsif response.error.nil?
      if new_format
        deliver :code_scanning_alerts_hash, { alerts: response.data.results, total_count: response.data.open_count }, repo: repo
      else
        deliver :code_scanning_alerts_legacy_hash, { alerts: response.data.results, total_count: response.data.open_count }, repo: repo
      end
    end
  end

  get "/repositories/:repository_id/code-scanning/alerts/:alert_number" do
    @route_owner = "@github/dsp-code-scanning"
    @documentation_url = "/rest/reference/code-scanning#get-a-code-scanning-alert"

    repo = find_repo!

    # NOTE: This is explicitely not checking the code_scanning_integration_api_enabled_for? predicate, as we
    #       have existing users that might not be enabled, and do not want to break those integrations for now.

    control_access :read_code_scanning,
      resource: repo,
      allow_integrations: true,
      allow_user_via_integration: true,
      forbid: repo.public?

    response = GitHub::Turboscan.result(
      repository_id: repo.id,
      number: alert_number,
    )

    if response&.error&.code == :not_found
      deliver_error! 404, message: "No alert found for alert number #{alert_number}", documentation_url: @documentation_url
    elsif response.blank? || response.error.present?
      deliver_error!(503, message: "Code scanning unavailable. Please try again later.")
    else
      if repo.code_scanning_integration_api_enabled_for?(current_integration || current_user)
        deliver :code_scanning_alert_hash, response.data.result, repo: repo
      else
        deliver :code_scanning_alert_legacy_hash, response.data.result, repo: repo
      end
    end
  end

  get "/repositories/:repository_id/code-scanning/analyses" do
    @route_owner = "@github/dsp-code-scanning"
    @documentation_url = "/rest/reference/code-scanning#list-recent-code-scanning-analyses-for-a-repository"

    repo = find_repo!
    return deliver_error!(404) unless repo.code_scanning_integration_api_enabled_for?(current_integration || current_user)

    control_access :read_code_scanning,
      resource: repo,
      allow_integrations: true,
      allow_user_via_integration: true,
      forbid: repo.public?

    ref = params[:ref]&.present? ? Array(params[:ref]) : nil
    tool_name = params[:tool_name]

    request = {
      repository_id: repo.id,
      ref_names: ref,
      tool: tool_name
    }
    response = GitHub::Turboscan.analyses(request)

    if response.blank? || response.error.present?
      deliver_error!(503, message: "Code scanning unavailable. Please try again later.")
    elsif response.error.nil?
      deliver :code_scanning_analyses_hash, { analyses: response.data.analyses }, repo: repo
    end
  end

  private def alert_number(param_name: :alert_number)
    int_id_param!(key: param_name)
  end

  # Add required metadata to the request and forward it to turboscan
  def upload_analysis(repo, data, can_write)
    # Store the source repository in the data payload by looking it up in the PR object
    m = data["ref"]&.match(/\Arefs\/pull\/([0-9]*)\/head\Z/)
    pr_number = m[1] if m
    if pr_number
      source_repository_id = PullRequest.with_number_and_repo(pr_number, repo).head_repository_id

      # Must have write access if source_repository == repository
      if source_repository_id == repo.id && !can_write
        return deliver_error(403)
      end

      data[:source_repository_id] = source_repository_id
    else
      data[:source_repository_id] = repo.id
    end

    # Add some diagnostic data
    # Note: We use symbols (rather than strings) for data injected by the API
    data[:upload_started_at] = @request_start_time
    data[:upload_finished_at] = Time.now
    data[:request_id] = request.env["HTTP_X_GITHUB_REQUEST_ID"]

    data["analysis_key"] ||= "(default)"
    resp = GitHub::Turboscan.upload_analysis(repo, data)

    if resp.success?
      # We create check runs so that we can show that analysis is in progress, but
      # then wait for Turboscan to send the completion event
      CheckRun.create_for_code_scanning_analysis(
        repository: repo,
        commit_oid: data["commit_oid"],
        tool_names: data["tool_names"],
      )
    end

    # Return the response from turboscan directly
    resp
  end
end
