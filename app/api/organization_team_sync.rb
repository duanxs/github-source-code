# rubocop:disable Style/FrozenStringLiteralComment

class Api::OrganizationTeamSync < Api::App

  TEAM_SYNC_NOT_ENABLED_ERROR = [
    "This team is not externally managed. Learn more at",
    "#{GitHub.help_url}/articles/synchronizing-teams-between-your-identity-provider-and-github",
  ].join(" ")

  # Get list of available groups

  get "/organizations/:organization_id/team-sync/groups" do
    @route_owner = "@github/team-sync"
    @documentation_url = "/rest/reference/teams#list-idp-groups-for-an-organization"

    set_forbidden_message "You must be an organization owner or team maintainer to view team sync status."

    control_access :manage_org_users,
      resource: org = find_org!,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    deliver_error! 403, message: TEAM_SYNC_NOT_ENABLED_ERROR unless endpoint_available?(org)
    deliver_error! 404 unless public_api_enabled?(org)

    response = GroupSyncer.client.list_groups(org_id: org.global_relay_id,
                                              query: params[:q].presence,
                                              page_size: params[:per_page].to_i,
                                              page_token: params[:page].to_s,
                                             )

    if response.error
      deliver_error! 500, message: "An error occurred while fetching group data, please try again"
    end

    response_groups = response.data.groups
    groups = response_groups.map { |group| Api::Serializer.group_hash(group) }

    @links.add_current({page: response.data.next_page_token}, rel: "next") if response.data.next_page_token.present?
    deliver_raw({ groups: groups }, status: 200)
  end

  # Get list of group mappings

  get "/organizations/:org_id/team/:team_id/team-sync/group-mappings" do
    @route_owner = "@github/team-sync"
    @documentation_url = "/rest/reference/teams#list-idp-groups-for-a-team"

    team = find_team!
    @current_org = team.organization
    set_forbidden_message "You must be an organization owner or team maintainer to view team sync status."

    control_access :manage_team_sync_mappings,
      resource: team.organization,
      organization: team.organization,
      team: team,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    deliver_error! 403, message: TEAM_SYNC_NOT_ENABLED_ERROR unless endpoint_available?(team)
    deliver_error! 404 unless public_api_enabled?(team.organization)

    mappings = team.group_mappings.map { |mapping| Api::Serializer.mapping_hash(mapping) }

    deliver_raw({groups: mappings}, status: 200)
  end

  # Add remove mappings

  patch "/organizations/:org_id/team/:team_id/team-sync/group-mappings" do
    @route_owner = "@github/team-sync"
    @documentation_url = "/rest/reference/teams#create-or-update-idp-group-connections"

    team = find_team!
    @current_org = team.organization
    set_forbidden_message "You must be an organization owner or team maintainer to modify group mappings for this team."

    control_access :manage_team_sync_mappings,
      resource: team.organization,
      organization: team.organization,
      team: team,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    deliver_error! 403, message: TEAM_SYNC_NOT_ENABLED_ERROR unless endpoint_available?(team)

    groups_param = receive_with_schema("group-mapping", "update-group-mappings")["groups"]

    deliver_error! 422 unless groups_param

    groups = groups_param.map(&:symbolize_keys)

    Team::GroupMapping.batch_update_mappings(team, groups, actor: current_user)

    current_mappings = team.group_mappings.map { |mapping| Api::Serializer.mapping_hash(mapping) }

    deliver_raw({groups: current_mappings}, status: 200)
  end

  patch "/organizations/:org_id/team/:team_id/team-sync/group-mappings-state" do
    @route_owner = "@github/team-sync"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL

    team = find_team!

    control_access :update_group_mappings_state,
      resource: team.organization,
      organization: team.organization,
      team: team,
      allow_integrations: true,
      allow_user_via_integration: false

    deliver_error! 403, message: TEAM_SYNC_NOT_ENABLED_ERROR unless endpoint_available?(team)

    input = receive_with_schema("group-mapping", "group-mappings-state")
    team.group_mappings.update_all(synced_at: input["synced_at"], status: input["status"])

    deliver_empty(status: 204)
  end

  private

  def endpoint_available?(item)
    case item
      when Team
        item.can_be_externally_managed?
      when Organization
        item.team_sync_enabled?
    end
  end

  def public_api_enabled?(org)
    TeamSync::Provider.detect(issuer: org.external_identity_session_owner.saml_provider.issuer).azuread?
  end
end
