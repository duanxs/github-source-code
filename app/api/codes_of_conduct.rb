# rubocop:disable Style/FrozenStringLiteralComment

class Api::CodesOfConduct < Api::App
  areas_of_responsibility :api, :community_and_safety

  CodesOfConductQuery = PlatformClient.parse <<-'GRAPHQL'
    query {
      codesOfConduct {
        ...Api::Serializer::CodesOfConductDependency::PartialCodeOfConductFragment
      }
    }
  GRAPHQL

  get "/codes_of_conduct" do
    @route_owner = "@github/communities-heart-reviewers"
    @documentation_url = "/rest/reference/codes-of-conduct#get-all-codes-of-conduct"
    require_preview(:code_of_conduct)
    control_access :public_site_information,
      resource: Platform::PublicResource.new,
      allow_integrations: true,
      allow_user_via_integration: true

    results = platform_execute(CodesOfConductQuery)

    deliver :graphql_code_of_conduct_hash, results.data.codes_of_conduct
  end

  CodeOfConductQuery = PlatformClient.parse <<-'GRAPHQL'
    query($key: String!) {
      codeOfConduct(key: $key) {
        ...Api::Serializer::CodesOfConductDependency::FullCodeOfConductFragment
      }
    }
  GRAPHQL

  get "/codes_of_conduct/:key" do
    @route_owner = "@github/communities-heart-reviewers"
    @documentation_url = "/rest/reference/codes-of-conduct#get-a-code-of-conduct"
    require_preview(:code_of_conduct)
    control_access :public_site_information,
      resource: Platform::PublicResource.new,
      allow_integrations: true,
      allow_user_via_integration: true

    variables = {
      key: params[:key],
    }

    results = platform_execute(CodeOfConductQuery, variables: variables)
    deliver :graphql_code_of_conduct_hash, results.data.code_of_conduct, full: true
  end
end
