# frozen_string_literal: true

# Enterprise Installation API. This is for authenticating enterprise to dotcom
class Api::EnterpriseInstallation < Api::App
  TTL = 5.minutes

  RANDOM_BYTES = 20

  before do
    deliver_error! 404 if GitHub.enterprise?
  end

  post "/enterprise-installation" do
    @route_owner = "@github/admin-experience"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL

    control_access :enterprise_installation_create,
      allow_integrations: false,
      allow_user_via_integration: false

    data = receive_with_schema("enterprise-installation", "create-legacy")

    log_data.update({
      github_connect_request_source: data["host_name"],
      github_connect_request_version: data["version"],
    })

    validate(data)
    token = SecureRandom.hex(RANDOM_BYTES)
    token_hash = Digest::SHA256.base64digest(token)
    GitHub.kv.set("ghe-install-token-#{token_hash}", enterprise_installation_attributes(data).to_json, expires: TTL.from_now)
    deliver_raw({ token: token }, status: 201)
  end

  put "/enterprise-installation" do
    @route_owner = "@github/admin-experience"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL

    control_access :enterprise_installation,
      allow_integrations: true,
      allow_user_via_integration: false

    require_enterprise_installation!

    # The initial EAP release does not send this data, but it will be
    # required at a later point to enforce a minimal supported version.
    if request.body && request.body.size > 0
      data = receive_with_schema("enterprise-installation", "update-legacy")
      validate(data)
      current_enterprise_installation.update(enterprise_installation_attributes(data))
    end
    # The initial (OAuth-based) EAP release used this token,
    # but we switched to GitHub Apps authentication
    deliver_raw({ token: "unsupported" }, status: 200)
  end

  delete "/enterprise-installation" do
    @route_owner = "@github/admin-experience"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL

    control_access :enterprise_installation,
      allow_integrations: true,
      allow_user_via_integration: false

    require_enterprise_installation!
    current_enterprise_installation.destroy
    deliver_empty status: 204
  end

  get "/enterprise-installation/application" do
    @route_owner = "@github/admin-experience"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL

    control_access :enterprise_installation,
      allow_integrations: true,
      allow_user_via_integration: false

    require_enterprise_installation!
    app = current_enterprise_installation.github_app
    owner = current_enterprise_installation.owner

    payload = {
      client_id: app.key,
      client_secret: app.secret,
      id: app.id,
      owner_type: owner.event_prefix,
      owner_identifier: owner.to_param,
      login: owner.to_param, # Backwards-compatibility for Enterprise Server < 2.17
    }

    deliver_raw(payload, status: 200)
  end

  post "/enterprise-installation/contributions" do
    @route_owner = "@github/admin-experience"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL

    control_access :external_contributions,
      resource: Platform::PublicResource.new,
      allow_integrations: false,
      allow_user_via_integration: true

    require_enterprise_installation!
    require_user!

    data = receive_with_schema("enterprise-contribution", "create-legacy")
    require_and_store_valid_login!(data["login"])

    created = []
    data["contributions"].each do |contrib|
      created << EnterpriseContribution.insert_or_update_contribution(current_user, current_enterprise_installation, contrib["date"], contrib["count"])
    end
    Contribution.clear_caches_for_user(current_user)
    GitHub.dogstats.increment("github_connect.contributions.reported")

    deliver_raw({created: created.size}, status: 201)
  end

  delete "/enterprise-installation/user/contributions" do
    @route_owner = "@github/admin-experience"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL

    control_access :external_contributions,
      resource: Platform::PublicResource.new,
      allow_integrations: false,
      allow_user_via_integration: true

    require_enterprise_installation!
    require_user!
    EnterpriseContribution.clear_user_contributions(current_user, current_enterprise_installation)

    deliver_empty status: 204
  end

  delete "/enterprise-installation/contributions" do
    @route_owner = "@github/admin-experience"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL

    control_access :enterprise_installation,
      allow_integrations: true,
      allow_user_via_integration: false

    require_enterprise_installation!
    EnterpriseContribution.clear_installation_contributions(current_enterprise_installation)

    deliver_empty status: 204
  end

  post "/enterprise-installation/permissions" do
    @route_owner = "@github/admin-experience"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL

    control_access :enterprise_installation,
      allow_integrations: true,
      allow_user_via_integration: false

    require_enterprise_installation!

    data = receive_with_schema("enterprise-installation", "permissions-legacy")
    version = current_enterprise_installation.request_github_app_permissions_update(data["features"])

    return deliver_error! 422, message: version.errors.full_messages.to_sentence if version&.errors&.any?
    deliver_raw({url: "/enterprise_installations/#{current_enterprise_installation.id}/upgrade"}, status: 201)
  end

  VULNERABILITIES_PAGE_SIZE = 100

  get "/enterprise-installation/vulnerabilities" do
    @route_owner = "@github/admin-experience"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL

    control_access :enterprise_vulnerabilities,
      resource: Platform::PublicResource.new,
      allow_integrations: true,
      allow_user_via_integration: false

    require_enterprise_installation!

    maximum_id = params[:before]&.to_i || Vulnerability.maximum(:id).next
    count = [params.fetch("count", VULNERABILITIES_PAGE_SIZE).to_i, VULNERABILITIES_PAGE_SIZE].min
    GitHub.dogstats.increment("croquet.vulnerability_sync.reported")
    GitHub.dogstats.gauge("croquet.vulnerability_sync.count", count)

    vulns = GitHub.dogstats.time "croquet.vulnerability_sync.timing" do
      ::Vulnerability.includes(:vulnerable_version_ranges)
                     .where("vulnerabilities.id < ?", maximum_id)
                     .order(id: :desc)
                     .limit(count)
    end
    deliver :vulnerability_hash, vulns
  end

  class LicenseNotValid < RuntimeError
    def sub_errors
      []
    end
  end
  class HostNameNotValid < RuntimeError
    def sub_errors
      []
    end
  end
  class ServerIdNotValid < RuntimeError
    def sub_errors
      []
    end
  end

  private
  # check if all params are present
  def validate(data)
    errors = []
    license_data = decode_data(data["license"])
    unless github_connect_authenticator.valid_license?(license_data)
      errors << LicenseNotValid.new("\"license\" is not valid")
    end
    license_hash = Digest::SHA256.base64digest(license_data)
    if ::EnterpriseInstallation.blocked?(license_hash)
      errors << LicenseNotValid.new("\"license\" is not allowed to connect to GitHub.com")
    end
    unless UrlHelper.valid_host?(data["host_name"])
      errors << HostNameNotValid.new("\"host_name\" is not valid")
    end
    unless ::EnterpriseInstallation.valid_server_id?(data["version"], data["server_id"])
      if data["server_id"].nil?
        errors << ServerIdNotValid.new("\"server_id\" must be provided for versions >= 2.17")
      else
        errors << ServerIdNotValid.new("\"server_id\" is not valid")
      end
    end

    result = ApiSchema::ValidationResult.new(errors)
    return if result.valid?
    deliver_schema_validation_error!(result)
  end

  def enterprise_installation_attributes(data)
    license_data = decode_data(data.delete("license"))
    license = github_connect_authenticator.load_license(license_data)
    {
      server_id: data["server_id"],
      license_hash: Digest::SHA256.base64digest(license_data),
      license_public_key: decode_data(license.customer_public_key),
      customer_name: license.company,
      host_name: data["host_name"],
      http_only: data["http_only"],
      public_key: data["public_key"] && decode_data(data["public_key"]),
      version: data["version"],
    }
  end

  def decode_data(data)
    Base64.decode64(data)
  end

  def github_connect_authenticator
    @github_connect_authenticator ||= GitHub::Connect::Authenticator.new
  end

  def require_user!
    if !logged_in?
      deliver_error! 403, message: "User is required."
    elsif !current_enterprise_installation.user_has_access?(current_user)
      deliver_error! 403, message: "User must be connected to installation."
    end
  end

  def require_and_store_valid_login!(login)
    current_enterprise_installation.set_login_for(current_user, login)
  rescue ArgumentError
    deliver_error! 422, message: "Invalid login: #{login}"
  end
end
