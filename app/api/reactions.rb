# rubocop:disable Style/FrozenStringLiteralComment

class Api::Reactions < Api::App
  areas_of_responsibility :api, :issues, :pull_requests

  include Api::Issues::EnsureIssuesEnabled
  include ::Api::App::TeamDiscussionHelpers

  before "/organizations/:org_id/team/:team_id/discussions/:number/*" do
    organization = Organization.find_by(id: int_id_param!(key: :org_id))
    record_or_404(organization)
    @team = organization.teams.find_by(id: int_id_param!(key: :team_id))
    record_or_404(team)

    unless organization.team_discussions_allowed?
      deliver_error!(410,
        message: "Team discussions are disabled for this organization.",
        documentation_url: "/v3/teams/discussions")
    end

    # Something deep in the guts of GitHub Apps authz requires
    # this to be set.
    @current_org = organization
  end

  ListReactionsQuery = Api::App::PlatformClient.parse <<-'GRAPHQL'
    query($issueId: ID!, $content: ReactionContent, $limit: Int!, $numericPage: Int) {
      node(id: $issueId) {
        ... on Issue {
          reactions(
            content: $content,
            first: $limit,
            numericPage: $numericPage) {
            nodes {
              ...Api::Serializer::ReactionsDependency::ReactionFragment
            }
            totalCount
          }
        }
        ... on PullRequest {
          reactions(
            content: $content,
            first: $limit,
            numericPage: $numericPage) {
            nodes {
              ...Api::Serializer::ReactionsDependency::ReactionFragment
            }
            totalCount
          }
        }
      }
    }
  GRAPHQL

  # List Reactions for this Issue
  get "/repositories/:repository_id/issues/:issue_number/reactions" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/reactions#list-reactions-for-an-issue"
    require_preview(:reactions)

    repo  = find_repo!
    issue = repo.issues.find_by_number(int_id_param!(key: :issue_number))

    record_or_404(issue)

    control_access :show_issue,
      resource: issue,
      repo: repo,
      allow_integrations: true,
      allow_user_via_integration: true

    ensure_issues_enabled_or_pr!(repo, issue)

    variables = {
      issueId: issue.global_relay_id,
      limit: pagination[:per_page] || DEFAULT_PER_PAGE,
      numericPage: pagination[:page],
    }

    if (filter = params[:content].presence) && (emotion = Emotion.find_by_label(filter.downcase))
      variables[:content] = emotion.platform_enum
    end

    results = platform_execute(ListReactionsQuery, variables: variables)

    if results.errors.all.any?
      errors = results.errors.all.details["data"]
      error_types = errors.map { |err| err["type"] }
      if !(error_types & %w(NOT_FOUND UNAUTHENTICATED)).empty?
        deliver_error! 404, message: "Not Found", documentation_url: @documentation_url
      elsif error_types.any? { |err| err == "ISSUES_DISABLED" }
        deliver_error! 410, message: "Issues are disabled for this repo", documentation_url: @documentation_url
      else
        deliver_error! 422, errors: results.errors.values.flatten
      end
    end

    reactions = results.data.node.reactions

    paginator.collection_size = reactions.total_count
    deliver :graphql_reaction_hash, reactions.nodes
  end

  # Create Reaction for this Issue
  post "/repositories/:repository_id/issues/:issue_number/reactions" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/reactions#create-reaction-for-an-issue"
    require_preview(:reactions)

    repo  = find_repo!
    issue = repo.issues.find_by_number(int_id_param!(key: :issue_number))

    record_or_404(issue)

    control_access :create_issue_related_reaction,
      resource: issue,
      repo: repo,
      challenge: repo.public?,
      enforce_oauth_app_policy: repo.private?,
      # We only need to forbid in the case where a PAT or OAuth token does not have the right scopes.
      # Therefore, we could leave off the integration-related key/value pairs in this call.
      # However, that would count against our linter, so for completeness, we are adding them.
      forbid: access_allowed?(:get_repo, resource: repo, user: current_user, allow_integrations: true, allow_user_via_integration: true),
      allow_integrations: true,
      allow_user_via_integration: true

    authorize_content :issue, repo: repo
    ensure_content_visible issue

    data = receive_with_schema("reaction", "create-for-issue-legacy")
    reaction = Reaction.react user: current_user,
      subject_id: issue.id,
      subject_type: issue.class.name,
      content: Emotion.find_by_label(data["content"]).content

    GitHub::PrefillAssociations.prefill_belongs_to [reaction], User, :user_id

    deliver!(:reaction_hash, reaction, status: 200) if reaction.exists?
    deliver!(:reaction_hash, reaction, status: 201) if reaction.created?

    deliver_error!(422, errors: reaction.errors)
  end

  delete "/repositories/:repository_id/issues/:issue_number/reactions/:reaction_id" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/reactions#delete-an-issue-reaction"
    require_preview(:reactions)

    repo  = find_repo!
    issue = repo.issues.find_by_number(int_id_param!(key: :issue_number))
    record_or_404(issue)

    receive_with_schema("reaction", "delete")
    reaction = issue.reactions.find_by(id: int_id_param!(key: :reaction_id))
    record_or_404(reaction)

    control_access(
      :delete_reaction,
      reaction: reaction,
      resource: issue,
      repo: repo,
      challenge: true,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: repo.private?,
    )

    Reaction.unreact(
      user: current_user,
      subject_id: reaction.subject_id,
      subject_type: reaction.subject_type,
      content: reaction.content)

    deliver_empty status: 204
  end

  # List Reactions for this Issue Comment
  get "/repositories/:repository_id/issues/comments/:comment_id/reactions" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/reactions#list-reactions-for-an-issue-comment"
    require_preview(:reactions)

    repo    = find_repo!
    comment = repo.issue_comments.find_by_id(int_id_param!(key: :comment_id))

    record_or_404(comment)

    control_access :get_issue_comment,
      repo: repo,
      resource: comment,
      allow_integrations: true,
      allow_user_via_integration: true

    ensure_content_visible comment

    scope = comment.reactions.not_spammy.reorder("reactions.id ASC")
    if (filter = params[:content].presence) && (emotion = Emotion.find_by_label(filter.downcase))
      scope = scope.where(content: emotion.content)
    end

    reactions = paginate_rel(scope)
    GitHub::PrefillAssociations.prefill_belongs_to reactions, User, :user_id

    deliver :reaction_hash, reactions
  end

  # Create Reaction for this Issue Comment
  post "/repositories/:repository_id/issues/comments/:comment_id/reactions" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/reactions#create-reaction-for-an-issue-comment"
    require_preview(:reactions)

    repo    = find_repo!
    comment = repo.issue_comments.find_by_id(int_id_param!(key: :comment_id))

    record_or_404(comment)
    record_or_404(comment.issue)

    control_access :create_issue_related_reaction,
      resource: comment,
      repo: repo,
      challenge: repo.public?,
      # We only need to forbid in the case where a PAT or OAuth token does not have the right scopes.
      # Therefore, we could leave off the integration-related key/value pairs in this call.
      # However, that would count against our linter, so for completeness, we are adding them.
      forbid: access_allowed?(:get_repo, resource: repo, user: current_user, allow_integrations: true, allow_user_via_integration: true),
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: repo.private?

    authorize_content :issue_comment, issue: comment.issue, repo: repo
    ensure_content_visible comment

    data = receive_with_schema("reaction", "create-for-issue-comment-legacy")
    reaction = Reaction.react user: current_user,
      subject_id: comment.id,
      subject_type: comment.class.name,
      content: Emotion.find_by_label(data["content"]).content

    GitHub::PrefillAssociations.prefill_belongs_to [reaction], User, :user_id

    deliver!(:reaction_hash, reaction, status: 200) if reaction.exists?
    deliver!(:reaction_hash, reaction, status: 201) if reaction.created?

    deliver_error!(422, errors: reaction.errors)
  end

  delete "/repositories/:repository_id/issues/comments/:comment_id/reactions/:reaction_id" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/reactions#delete-an-issue-comment-reaction"
    require_preview(:reactions)

    repo    = find_repo!
    comment = repo.issue_comments.find_by_id(int_id_param!(key: :comment_id))

    record_or_404(comment)
    record_or_404(comment.issue)

    receive_with_schema("reaction", "delete")
    reaction = comment.reactions.find_by(id: int_id_param!(key: :reaction_id))
    record_or_404(reaction)

    control_access(
      :delete_reaction,
      reaction: reaction,
      resource: comment,
      repo: repo,
      challenge: true,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: repo.private?,
    )

    Reaction.unreact(
      user: current_user,
      subject_id: reaction.subject_id,
      subject_type: reaction.subject_type,
      content: reaction.content)

    deliver_empty status: 204
  end

  # List Reactions for this Pull Request Review Comment
  get "/repositories/:repository_id/pulls/comments/:comment_id/reactions" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/reactions#list-reactions-for-a-pull-request-review-comment"
    require_preview(:reactions)

    repo = find_repo!
    comment = repo.pull_request_review_comments.find_by_id(int_id_param!(key: :comment_id))

    control_access :get_pull_request_comment,
      repo: repo,
      resource: comment,
      allow_integrations: true,
      allow_user_via_integration: true

    ensure_content_visible comment

    scope = comment.reactions.not_spammy.reorder("reactions.id ASC")
    if (filter = params[:content].presence) && (emotion = Emotion.find_by_label(filter.downcase))
      scope = scope.where(content: emotion.content)
    end

    reactions = paginate_rel(scope)
    GitHub::PrefillAssociations.prefill_belongs_to reactions, User, :user_id

    deliver :reaction_hash, reactions
  end

  # Create Reaction for this Pull Request Review Comment
  post "/repositories/:repository_id/pulls/comments/:comment_id/reactions" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/reactions#create-reaction-for-a-pull-request-review-comment"
    require_preview(:reactions)

    repo = find_repo!
    comment = repo.pull_request_review_comments.find_by_id(int_id_param!(key: :comment_id))

    if repo.public?
      set_forbidden_message "Insufficient scopes for reacting to this Pull Request Review Comment."
    end

    control_access :create_pull_request_review_comment_reaction,
      resource: comment,
      repo: repo,
      challenge: repo.public?,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: repo.private?

    authorize_content :pull_request_comment, repo: repo
    ensure_content_visible comment

    data = receive_with_schema("reaction", "create-for-pr-review-comment-legacy")
    reaction = Reaction.react user: current_user,
      subject_id: comment.id,
      subject_type: comment.class.name,
      content: Emotion.find_by_label(data["content"]).content

    GitHub::PrefillAssociations.prefill_belongs_to [reaction], User, :user_id

    deliver!(:reaction_hash, reaction, status: 200) if reaction.exists?
    deliver!(:reaction_hash, reaction, status: 201) if reaction.created?

    deliver_error!(422, errors: reaction.errors)
  end

  delete "/repositories/:repository_id/pulls/comments/:comment_id/reactions/:reaction_id" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/reactions#delete-a-pull-request-comment-reaction"
    require_preview(:reactions)

    repo    = find_repo!
    comment = repo.pull_request_review_comments.find_by_id(int_id_param!(key: :comment_id))
    record_or_404(comment)

    receive_with_schema("reaction", "delete")
    reaction = comment.reactions.find_by(id: int_id_param!(key: :reaction_id))
    record_or_404(reaction)

    control_access(
      :delete_reaction,
      reaction: reaction,
      resource: comment,
      repo: repo,
      challenge: true,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: repo.private?,
    )

    Reaction.unreact(
      user: current_user,
      subject_id: reaction.subject_id,
      subject_type: reaction.subject_type,
      content: reaction.content)

    deliver_empty status: 204
  end

  # List Reactions for this Commit Comment
  get "/repositories/:repository_id/comments/:comment_id/reactions" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/reactions#list-reactions-for-a-commit-comment"
    require_preview(:reactions)

    repo = find_repo!
    comment = repo.commit_comments.find_by_id(int_id_param!(key: :comment_id))

    control_access :get_commit_comment,
      repo: repo,
      comment: comment,
      allow_integrations: true,
      allow_user_via_integration: true

    ensure_content_visible comment

    scope = comment.reactions.not_spammy.reorder("reactions.id ASC")
    if (filter = params[:content].presence) && (emotion = Emotion.find_by_label(filter.downcase))
      scope = scope.where(content: emotion.content)
    end

    reactions = paginate_rel(scope)
    GitHub::PrefillAssociations.prefill_belongs_to reactions, User, :user_id

    deliver :reaction_hash, reactions
  end

  # Create Reaction for this Commit Comment
  post "/repositories/:repository_id/comments/:comment_id/reactions" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/reactions#create-reaction-for-a-commit-comment"
    require_preview(:reactions)

    repo = find_repo!
    comment = repo.commit_comments.find_by_id(int_id_param!(key: :comment_id))

    if repo.public?
      set_forbidden_message "Insufficient scopes for reacting to this Commit Comment."
    end

    control_access :create_commit_comment_reaction,
      resource: comment,
      repo: repo,
      challenge: repo.public?,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: repo.private?

    authorize_content :commit_comment, repo: repo
    ensure_content_visible comment

    data = receive_with_schema("reaction", "create-for-commit-comment-legacy")
    reaction = Reaction.react user: current_user,
      subject_id: comment.id,
      subject_type: comment.class.name,
      content: Emotion.find_by_label(data["content"]).content

    GitHub::PrefillAssociations.prefill_belongs_to [reaction], User, :user_id

    deliver!(:reaction_hash, reaction, status: 200) if reaction.exists?
    deliver!(:reaction_hash, reaction, status: 201) if reaction.created?

    deliver_error!(422, errors: reaction.errors)
  end

  delete "/repositories/:repository_id/comments/:comment_id/reactions/:reaction_id" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/reactions#delete-a-commit-comment-reaction"
    require_preview(:reactions)

    repo    = find_repo!
    comment = repo.commit_comments.find_by_id(int_id_param!(key: :comment_id))
    record_or_404(comment)

    receive_with_schema("reaction", "delete")
    reaction = comment.reactions.find_by(id: int_id_param!(key: :reaction_id))
    record_or_404(reaction)

    control_access(
      :delete_reaction,
      reaction: reaction,
      resource: comment,
      repo: repo,
      challenge: true,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: repo.private?,
    )

    Reaction.unreact(
      user: current_user,
      subject_id: reaction.subject_id,
      subject_type: reaction.subject_type,
      content: reaction.content)

    deliver_empty status: 204
  end

  # List reactions for a Team Discussion
  get "/organizations/:org_id/team/:team_id/discussions/:discussion_number/reactions" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/reactions#list-reactions-for-a-team-discussion"
    require_preview(:reactions)

    discussion = find_discussion!

    control_access(
      :show_team_discussion,
      resource: discussion,
      organization: current_org,
      allow_integrations: true,
      allow_user_via_integration: true)

    scope = discussion.reactions.not_spammy.reorder("reactions.id ASC")
    if (filter = params[:content].presence) && (emotion = Emotion.find_by_label(filter.downcase))
      scope = scope.where(content: emotion.content)
    end

    reactions = paginate_rel(scope)
    GitHub::PrefillAssociations.prefill_belongs_to reactions, User, :user_id

    deliver :reaction_hash, reactions
  end

  post "/organizations/:org_id/team/:team_id/discussions/:discussion_number/reactions" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/reactions#create-reaction-for-a-team-discussion"
    require_preview(:reactions)
    discussion = find_discussion!

    # This code uses class methods like Reaction.user_can_react_to? and
    # Reaction.react, which all use ActiveRecord directly. Allow it as a one-off
    # direct access of Discussion{Post,PostReply} objects.
    control_access(
      :create_team_discussion_related_reaction,
      organization: current_org,
      resource: discussion,
      allow_integrations: true,
      allow_user_via_integration: true,
    )

    data = receive_with_schema("reaction", "create-for-discussion-legacy")
    reaction = Reaction.react user: current_user,
      subject_id: discussion.id,
      subject_type: discussion.class.name,
      content: Emotion.find_by_label(data["content"]).content

    GitHub::PrefillAssociations.prefill_belongs_to [reaction], User, :user_id

    deliver!(:reaction_hash, reaction, status: 200) if reaction.exists?
    deliver!(:reaction_hash, reaction, status: 201) if reaction.created?

    deliver_error!(422, errors: reaction.errors)
  end

  delete "/organizations/:org_id/team/:team_id/discussions/:discussion_number/reactions/:reaction_id" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/reactions#delete-team-discussion-reaction"
    require_preview(:reactions)

    discussion = find_discussion!

    receive_with_schema("reaction", "delete")
    reaction = discussion.reactions.find_by(id: int_id_param!(key: :reaction_id))
    record_or_404(reaction)

    control_access(
      :delete_team_discussion_related_reaction,
      reaction: reaction,
      resource: discussion,
      organization: current_org,
      challenge: true,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true)

    Reaction.unreact(
      user: current_user,
      subject_id: reaction.subject_id,
      subject_type: reaction.subject_type,
      content: reaction.content)

    deliver_empty status: 204
  end

  # List reactions for a Team Discussion Comment
  get "/organizations/:org_id/team/:team_id/discussions/:discussion_number/comments/:comment_number/reactions" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/reactions#list-reactions-for-a-team-discussion-comment"
    require_preview(:reactions)

    comment = find_discussion_comment!

    # Make an exception to the platform enforcement rule to allow loading of the
    # parent discussion object in the `comment.readable_by?(user)` call that
    # is issued due to passing `resource: comment` to `control_access`.
    control_access(
      :show_team_discussion_comment,
      resource: comment,
      organization: current_org,
      allow_integrations: true,
      allow_user_via_integration: true)

    scope = comment.reactions.not_spammy.reorder("reactions.id ASC")
    if (filter = params[:content].presence) && (emotion = Emotion.find_by_label(filter.downcase))
      scope = scope.where(content: emotion.content)
    end

    reactions = paginate_rel(scope)
    GitHub::PrefillAssociations.prefill_belongs_to reactions, User, :user_id

    deliver :reaction_hash, reactions
  end

  post "/organizations/:org_id/team/:team_id/discussions/:discussion_number/comments/:comment_number/reactions" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/reactions#create-reaction-for-a-team-discussion-comment"
    require_preview(:reactions)

    comment = find_discussion_comment!

    # This code uses class methods like Reaction.user_can_react_to? and
    # Reaction.react, which all use ActiveRecord directly. Allow it as a one-off
    # direct access of Discussion{Post,PostReply} objects.
    control_access(
      :create_team_discussion_related_reaction,
      organization: current_org,
      resource: comment,
      allow_integrations: true,
      allow_user_via_integration: true)

    data = receive_with_schema("reaction", "create-for-discussion-comment-legacy")
    reaction = Reaction.react user: current_user,
      subject_id: comment.id,
      subject_type: comment.class.name,
      content: Emotion.find_by_label(data["content"]).content

    GitHub::PrefillAssociations.prefill_belongs_to [reaction], User, :user_id

    deliver!(:reaction_hash, reaction, status: 200) if reaction.exists?
    deliver!(:reaction_hash, reaction, status: 201) if reaction.created?

    deliver_error!(422, errors: reaction.errors)
  end

  delete "/organizations/:org_id/team/:team_id/discussions/:discussion_number/comments/:comment_number/reactions/:reaction_id" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/reactions#delete-team-discussion-comment-reaction"
    require_preview(:reactions)

    comment = find_discussion_comment!

    receive_with_schema("reaction", "delete")
    reaction = comment.reactions.find_by(id: int_id_param!(key: :reaction_id))
    record_or_404(reaction)

    control_access(
      :delete_team_discussion_related_reaction,
      reaction: reaction,
      resource: comment,
      organization: current_org,
      challenge: true,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true)

    Reaction.unreact(
      user: current_user,
      subject_id: reaction.subject_id,
      subject_type: reaction.subject_type,
      content: reaction.content)

    deliver_empty status: 204
  end

  # Delete a Reaction
  delete "/reactions/:reaction_id" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/reactions#delete-a-reaction-legacy"
    require_preview(:reactions)

    receive_with_schema("reaction", "delete")

    reaction = find_reaction!(param_name: :reaction_id)

    deprecation_date = Date.new(2020, 02, 01)
    deprecated(
      deprecation_date: deprecation_date,
      sunset_date: deprecation_date + 1.year,
      info_url: "#{GitHub.developer_help_url}/changes/2020-02-26-new-delete-reactions-endpoints/",
      alternate_path_url: alternate_path_for_deprecation(reaction),
    )

    if reaction.user == current_user
      set_forbidden_message "Insufficient scopes for deleting this reaction."
    else
      set_forbidden_message "Only the user who created this reaction can delete it."
    end

    # This code uses class methods like Reaction#subject and Reaction.unreact
    # which use ActiveRecord directly. Allow it as a one-off direct access of
    # Discussion{Post,PostReply} objects.
    subject = reaction.subject

    if subject.respond_to?(:repository)
      @current_repo = subject.try(:repository)
      enforce_oauth_app_policy = @current_repo.present? ? @current_repo.private? : true

      control_access(
        :delete_reaction,
        reaction: reaction,
        resource: subject,
        repo: @current_repo,
        challenge: true,
        forbid: true,
        allow_integrations: true,
        allow_user_via_integration: true,
        enforce_oauth_app_policy: enforce_oauth_app_policy,
      )
    elsif subject.is_a?(DiscussionItem)
      @current_org = subject.organization
      require_team_discussions_enabled

      control_access(
        :delete_team_discussion_related_reaction,
        reaction: reaction,
        resource: subject,
        organization: current_org,
        challenge: true,
        forbid: true,
        allow_integrations: true,
        allow_user_via_integration: true)
    end

    Reaction.unreact(
      user: current_user,
      subject_id: reaction.subject_id,
      subject_type: reaction.subject_type,
      content: reaction.content)

    deliver_empty status: 204
  end

  private

  def find_reaction!(param_name: :id)
    reaction = Reaction.find_by_id(int_id_param!(key: param_name))
    record_or_404(reaction)
  end

  def authorize_content(kind, data = {})
    authorization = ContentAuthorizer.authorize(current_user, kind, "update", data)
    deliver_content_authorization_denied!(authorization) if authorization.failed?
  end

  def ensure_content_visible(content)
    deliver_error!(404) if content.nil? || content.hide_from_user?(current_user)
  end

  def alternate_path_for_deprecation(reaction)
    case reaction.subject_type
    when "CommitComment"
      commit_comment = reaction.subject
      "/repositories/#{commit_comment.repository_id}/comments/#{commit_comment.id}/reactions/#{reaction.id}"
    when "DiscussionPost"
      discussion = reaction.subject
      "/organizations/#{discussion.organization_id}/team/#{discussion.team_id}/discussions/#{discussion.number}/reactions/#{reaction.id}"
    when "DiscussionPostReply"
      discussion_comment = reaction.subject
      discussion_post = discussion_comment.discussion_post
      "/organizations/#{discussion_post.organization_id}/team/#{discussion_post.team_id}/discussions/#{discussion_post.number}/comments/#{discussion_comment.number}/reactions/#{reaction.id}"
    when "Issue"
      issue = reaction.subject
      "/repositories/#{issue.repository_id}/issues/#{issue.number}/reactions/#{reaction.id}"
    when "IssueComment"
      issue_comment = reaction.subject
      issue = issue_comment.issue
      "/repositories/#{issue.repository_id}/issues/comments/#{issue_comment.id}/reactions/#{reaction.id}"
    when "PullRequestReviewComment"
      pull_request_review_comment = reaction.subject
      pull_request = pull_request_review_comment.pull_request
      "/repositories/#{pull_request.repository_id}/pulls/comments/#{pull_request_review_comment.id}/reactions/#{reaction.id}"
    end
  end
end
