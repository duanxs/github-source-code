# rubocop:disable Style/FrozenStringLiteralComment

class Api::OrganizationTeamProjects < Api::App
  areas_of_responsibility :orgs, :api

  TeamProjectsQuery = PlatformClient.parse <<-'GRAPHQL'
    query($id: ID!, $affiliation: TeamProjectType, $limit: Int!, $numericPage: Int) {
      node(id: $id) {
        ... on Team {
          projects(affiliation: $affiliation, first: $limit, numericPage: $numericPage) {
            totalCount
            edges {
              ...Api::Serializer::ProjectsDependency::TeamProjectEdgeFragment
            }
          }
        }
      }
    }
  GRAPHQL

  # list projects in a team
  get "/organizations/:org_id/team/:team_id/projects" do
    @route_owner = "@github/pe-issues-projects"
    @documentation_url = "/rest/reference/teams#list-team-projects"
    require_preview(:projects)

    team = find_team!
    control_access :list_team_projects,
      resource: team,
      organization: team.organization,
      allow_integrations: true,
      allow_user_via_integration: true

    variables = {
      id: team.global_relay_id,
      limit: per_page,
      numericPage: pagination[:page],
      affiliation: "ALL",
    }

    results = platform_execute(TeamProjectsQuery, variables: variables)

    if results.errors.all.any?
      deprecated_deliver_graphql_error({
        errors: results.errors.all,
        resource: "Team",
        documentation_url: @documentation_url,
      })
    else
      projects = results.data.node.projects

      paginator.collection_size = projects.total_count
      deliver :graphql_team_project_hash, projects.edges, team: team
    end
  end

  ReviewTeamProjectQuery = PlatformClient.parse <<-'GRAPHQL'
    query($teamId: ID!, $projectId: ID!, $affiliation: TeamProjectType) {
      node(id: $teamId) {
        ... on Team {
          projects(id: $projectId, affiliation: $affiliation, first: 1) {
            edges {
              ...Api::Serializer::ProjectsDependency::TeamProjectEdgeFragment
            }
          }
        }
      }
    }
  GRAPHQL

  # review a project in a team
  get "/organizations/:org_id/team/:team_id/projects/:project_id" do
    @route_owner = "@github/pe-issues-projects"
    @documentation_url = "/rest/reference/teams#check-team-permissions-for-a-project"
    require_preview(:projects)

    team = find_team!
    project = find_project!

    control_access :list_team_projects,
      resource: team,
      organization: team.organization,
      allow_integrations: true,
      allow_user_via_integration: true

    variables = {
      teamId: team.global_relay_id,
      projectId: project.global_relay_id,
      affiliation: "ALL",
    }

    results = platform_execute(ReviewTeamProjectQuery, variables: variables)

    if results.errors.all.any?
      deprecated_deliver_graphql_error({
        errors: results.errors.all,
        resource: "Team",
        documentation_url: @documentation_url,
      })
    else
      deliver :graphql_team_project_hash, results.data.node.projects.edges.first, team: team
    end
  end

  AddTeamProjectQuery = PlatformClient.parse <<-'GRAPHQL'
    mutation($projectId: ID!, $teamId: ID!, $permission: ProjectPermission!) {
      addTeamProject(input: { projectId: $projectId, teamId: $teamId, permission: $permission }) {
        __typename # a selection is grammatically required here
      }
    }
  GRAPHQL

  UpdateTeamProjectQuery = PlatformClient.parse <<-'GRAPHQL'
    mutation($projectId: ID!, $teamId: ID!, $permission: ProjectPermission!) {
      updateTeamProject(input: { projectId: $projectId, teamId: $teamId, permission: $permission }) {
        __typename # a selection is grammatically required here
      }
    }
  GRAPHQL

  # add a project to a team
  put "/organizations/:org_id/team/:team_id/projects/:project_id" do
    @route_owner = "@github/pe-issues-projects"
    @documentation_url = "/rest/reference/teams#add-or-update-team-project-permissions"
    require_preview(:projects)

    team = find_team!
    project = find_project!

    control_access :add_team_project,
      resource: team,
      organization: team.organization,
      project: project,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true

    data = receive_with_schema("team-project", "update")

    variables = {
      teamId: team.global_relay_id,
      projectId: project.global_relay_id,
      permission: (data["permission"] || "write").upcase,
    }

    query = if team.projects.include?(project)
      UpdateTeamProjectQuery
    else
      AddTeamProjectQuery
    end

    results = platform_execute(query, variables: variables)

    if results.errors.all.any?
      deprecated_deliver_graphql_error({
        errors: results.errors.all,
        resource: "Team",
        documentation_url: @documentation_url,
      })
    else
      deliver_empty status: 204
    end
  end

  RemoveTeamProjectQuery = PlatformClient.parse <<-'GRAPHQL'
    mutation($projectId: ID!, $teamId: ID!) {
      removeTeamProject(input: { projectId: $projectId, teamId: $teamId }) {
        __typename # a selection is grammatically required here
      }
    }
  GRAPHQL

  # remove a project from a team
  delete "/organizations/:org_id/team/:team_id/projects/:project_id" do
    @route_owner = "@github/pe-issues-projects"
    @documentation_url = "/rest/reference/teams#remove-a-project-from-a-team"
    require_preview(:projects)

    receive_with_schema("team-project", "delete")

    team = find_team!
    project = find_project!

    control_access :remove_team_project,
      resource: team,
      organization: team.organization,
      project: project,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true

    variables = {
      teamId: team.global_relay_id,
      projectId: project.global_relay_id,
    }

    results = platform_execute(RemoveTeamProjectQuery, variables: variables)

    if results.errors.all.any?
      deprecated_deliver_graphql_error({
        errors: results.errors.all,
        resource: "Team",
        documentation_url: @documentation_url,
      })
    else
      deliver_empty status: 204
    end
  end

  private

  def find_project!
    return @find_project if defined?(@find_project)

    @find_project = Project.find_by_id(int_id_param!(key: :project_id))

    # Set current repo/org for OAP enforcement.
    case @find_project&.owner_type
    when "Repository"
      @current_repo ||= @find_project.owner
    when "Organization"
      @current_org ||= @find_project.owner
    end

    record_or_404(@find_project)
  end
end
