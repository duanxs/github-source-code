# frozen_string_literal: true

class Api::WorkflowRuns < Api::App
  map_to_service :actions_experience

  # Get all runs for a workflow
  get "/repositories/:repository_id/actions/workflows/:workflow_id/runs" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#list-workflow-runs"

    repo = find_repo!

    control_access :read_actions,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: repo.private?

    workflow = repo.workflows.find_from_id_or_filename(params[:workflow_id])
    deliver_error! 404 unless workflow

    query = es_query
    if !query.empty?
      result = Actions::WorkflowRun.search(
        query: query,
        repo: repo,
        workflow_id: workflow.id,
        current_user: current_user,
        remote_ip: remote_ip,
        page: current_page,
      )
      workflow_runs = result[:workflow_runs]
      total_count = result[:total_count]

      GitHub::PrefillAssociations.for_workflow_runs(workflow_runs, repo)
      deliver :workflow_runs_hash, { workflow_runs: workflow_runs, total_count: total_count }
    else
      workflow_runs = paginate_rel(workflow.workflow_runs.most_recent)

      GitHub::PrefillAssociations.for_workflow_runs(workflow_runs, repo)
      deliver :workflow_runs_hash, { workflow_runs: workflow_runs, total_count: workflow_runs.total_entries }
    end
  end

  # Get all workflow runs for a repository
  get "/repositories/:repository_id/actions/runs" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#list-workflow-runs-for-a-repository"

    repo = find_repo!

    control_access :read_actions,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: repo.private?

    query = es_query
    if !query.empty?
      result = Actions::WorkflowRun.search(
        query: query,
        repo: repo,
        current_user: current_user,
        remote_ip: remote_ip,
        page: current_page,
      )
      workflow_runs = result[:workflow_runs]
      total_count = result[:total_count]

      GitHub::PrefillAssociations.for_workflow_runs(workflow_runs, repo)
      deliver :workflow_runs_hash, { workflow_runs: workflow_runs, total_count: total_count }
    else
      workflow_runs = repo.workflow_runs
        .from("#{Actions::WorkflowRun.table_name} USE INDEX(index_workflow_runs_on_repository_id)")
        .includes(:workflow, :check_suite)
        .most_recent
      workflow_runs = paginate_rel(workflow_runs)

      GitHub::PrefillAssociations.for_workflow_runs(workflow_runs, repo)
      deliver :workflow_runs_hash, { workflow_runs: workflow_runs, total_count: workflow_runs.total_entries }
    end
  end

  # Get a workflow run by (database) id
  get "/repositories/:repository_id/actions/runs/:run_id" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#get-a-workflow-run"

    repo = find_repo!

    control_access :read_actions,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: repo.private?

    workflow_run = Actions::WorkflowRun.includes(:workflow, :check_suite).find_by(id: params[:run_id])
    record_or_404(workflow_run)

    deliver_error!(404) unless repo.id == workflow_run.check_suite.repository_id

    GitHub::PrefillAssociations.for_workflow_runs([workflow_run], repo)
    deliver :workflow_run_hash, workflow_run
  end

  # Get logs for a workflow run
  get "/repositories/:repository_id/actions/runs/:run_id/logs" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#download-workflow-run-logs"

    repo = find_repo!

    control_access :read_actions_downloads,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: repo.private?

    workflow_run = Actions::WorkflowRun.find_by(id: params[:run_id])
    record_or_404(workflow_run)

    deliver_error!(404) unless repo.id == workflow_run.check_suite.repository_id
    deliver_error!(404) unless workflow_run.check_suite.completed_log_url
    deliver_error!(410) if workflow_run.expired_logs?

    result = GrpcHelper.rescue_from_grpc_errors("Actions") do
      grpc_request = GitHub::Launch::Services::Artifactsexchange::ExchangeURLRequest.new({
        unauthenticated_url: workflow_run.check_suite.completed_log_url,
        repository_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: repo.global_relay_id),
        resource_type: GitHub::Launch::Services::Artifactsexchange::ResourceType::TYPE_COMPLETED_RUN_LOG,
      })

      GitHub.launch_artifacts_exchange_for_check_suite(workflow_run.check_suite).exchange_url(grpc_request)
    end

    if result.call_succeeded?
      redirect result.value.authenticated_url
    else
      deliver_error! 500, message: "Failed to generate URL to download logs"
    end
  end

  # Delete logs for a workflow run.
  delete "/repositories/:repository_id/actions/runs/:run_id/logs" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#delete-workflow-run-logs"

    repo = find_repo!

    control_access :write_actions,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    workflow_run = Actions::WorkflowRun.find_by(id: params[:run_id])
    record_or_404(workflow_run)

    check_suite = workflow_run.check_suite
    deliver_error!(404) unless repo.id == check_suite.repository_id

    receive_with_schema("workflow-run", "delete-logs")

    begin
      check_suite.delete_logs(actor: current_user)
      deliver_empty status: 204
    rescue RuntimeError
      deliver_error! 500, message: "Could not delete the logs from file storage"
    end
  end

  # Deletes a workflow run.
  delete "/repositories/:repository_id/actions/runs/:run_id" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED

    repo = find_repo!

    control_access :write_actions,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    workflow_run = Actions::WorkflowRun.find_by(id: params[:run_id])
    record_or_404(workflow_run)

    check_suite = workflow_run.check_suite
    deliver_error!(404) unless repo.id == check_suite.repository_id

    begin
      workflow_run.hard_delete(actor: current_user)
    rescue Actions::WorkflowRun::NotDeleteableError
      deliver_error! 403, message: "Could not delete the workflow run"
    end

    deliver_empty status: 204
  end

  # Create a re-run of a workflow run
  post "/repositories/:repository_id/actions/runs/:run_id/rerun" do
    @route_owner = "@github/pe-actions-experience"
    @documentation_url = "/rest/reference/actions#re-run-a-workflow"

    repo = find_repo!

    control_access :write_actions,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    workflow_run = Actions::WorkflowRun.find_by(id: params[:run_id])
    record_or_404(workflow_run)

    check_suite = workflow_run.check_suite
    deliver_error!(404) unless repo.id == check_suite.repository_id

    unless allowed_to_modify_app?(app_id: check_suite.github_app_id)
      deliver_error! 403, message: "Invalid run_id `#{params[:run_id]}`"
    end

    receive_with_schema("workflow-run", "rerun")

    begin
      check_suite.rerequest(actor: current_user, only_failed_check_suites: true)
      deliver_empty status: 201
    rescue CheckSuite::ExpiredWorkflowRunError
      deliver_error! 403, message: "Unable to re-run this workflow run because it was created over a month ago"
    rescue CheckSuite::AlreadyRerunningError
      deliver_error! 403, message: "This workflow is already re-running"
    rescue CheckSuite::NotRerequestableError
      deliver_error! 403, message: "This workflow run cannot be rerun"
    end
  end

  # Cancel a workflow run
  post "/repositories/:repository_id/actions/runs/:run_id/cancel" do
    @route_owner = "@github/pe-actions-experience"
    @documentation_url = "/rest/reference/actions#cancel-a-workflow-run"

    repo = find_repo!

    control_access :write_actions,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    workflow_run = Actions::WorkflowRun.find_by(id: params[:run_id])
    record_or_404(workflow_run)

    check_suite = workflow_run.check_suite
    deliver_error!(404) unless repo.id == check_suite.repository_id

    receive_with_schema("workflow-run", "cancel")

    if check_suite.completed?
      deliver_error! 409, message: "Cannot cancel a workflow run that is completed."
    end

    result = GrpcHelper.rescue_from_grpc_errors("Actions") do
      grpc_request = GitHub::Launch::Services::Deploy::WorkflowCancelRequest.new(
        check_suite_id: GitHub::Launch::Pbtypes::GitHub::Identity.new(global_id: check_suite.global_relay_id),
      )
      if check_suite.github_app.launch_lab_github_app?
        GitHub.launch_lab_deployer.workflow_cancel(grpc_request)
      else
        GitHub.launch_deployer.workflow_cancel(grpc_request)
      end
    end

    if result.call_succeeded?
      deliver_empty status: 202
    else
      deliver_error! 500, message: "Failed to cancel workflow run"
    end
  end

  # Get the billable information for a workflow run
  get "/repositories/:repository_id/actions/runs/:run_id/timing" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#get-workflow-run-usage"

    deliver_error!(404) if GitHub.enterprise?

    repo = find_repo!

    control_access :read_actions,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: repo.private?

    workflow_run = Actions::WorkflowRun.find_by(id: params[:run_id])
    record_or_404(workflow_run)

    deliver_error!(404) unless repo.id == workflow_run.check_suite.repository_id

    lines = workflow_run.billing_usage_line_items
    environments = lines.map { |line| line.job_runtime_environment }.uniq
    payload = { billable: {} }
    duration = workflow_run.duration
    payload[:run_duration_ms] = (duration * 1000).to_i if duration > 0

    environments.each do |environment|
      payload[:billable][environment] = billing_timing_for_environment(lines, environment)
    end

    deliver_raw(payload)
  end

  private

  def billing_timing_for_environment(lines, environment)
    env_lines = lines.select { |line| line.job_runtime_environment == environment }
    milliseconds = env_lines.sum do |line_item|
      line_item.duration_in_milliseconds
    end
    jobs = env_lines.map { |line| line.check_run_id }.uniq.size
    { total_ms: milliseconds, jobs: jobs }
  end

  def es_query
    query = ""
    keys = [:actor, :branch, :event, :status]
    keys.each do |key|
      field = key
      field = :is if key == :status
      query = "#{query} #{field}:\"#{params[key]}\"" if params[key]
    end
    query
  end
end
