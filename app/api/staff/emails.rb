# rubocop:disable Style/FrozenStringLiteralComment

# All endpoints added for Incident response
class Api::Staff::Emails < Api::Staff::App

  # Find UserEmail(s) (and its corresponding user)
  get "/staff/emails/:email" do
    @route_owner = "@github/stafftools"
    user_emails = paginate_rel(UserEmail.where("email = ?", params[:email]))

    record_or_404 user_emails

    GitHub::PrefillAssociations.fill_email_roles(user_emails, full: true)
    deliver :user_email_hash, user_emails, full: true
  end

  # get all emails associated with a user
  get "/staff/user/:user_id/emails" do
    @route_owner = "@github/stafftools"
    user = find_user!
    emails = paginate_rel(user.emails.order("id ASC"))
    GitHub::PrefillAssociations.fill_email_roles(emails)
    deliver :user_email_hash, emails
  end
end
