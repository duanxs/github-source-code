# frozen_string_literal: true

class Api::BusinessEnterpriseInstallationUserAccountsUploads < Api::App
  areas_of_responsibility :api, :admin_experience

  before do
    deliver_error! 404 if GitHub.enterprise?
  end

  # Called from alambic to deliver an upload hash after a user uploads directly
  # using the alambic endpoint:
  #
  # GET https://uploads.github.com/businesses/:business_id/user-accounts-uploads
  get "/businesses/:business_id/user-accounts-uploads/:upload_id" do
    # This endpoint is only designed to be called from alambic.
    bypass_allowed_ip_enforcement
    @route_owner = "@github/admin-experience"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL

    business = find_business!
    control_access :write_business_enterprise_installation_user_accounts,
      resource: business,
      allow_integrations: true, allow_user_via_integration: false

    upload = find_upload!(business)
    GitHub::PrefillAssociations.for_enterprise_installation_user_accounts_uploads([upload])
    deliver :enterprise_installation_user_accounts_upload_hash, upload
  end

  # Called from alambic to update the state field of an upload after a user
  # uploads directly using the alambic endpoint:
  #
  # POST https://uploads.github.com/businesses/:business_id/user-accounts-uploads
  patch "/businesses/:business_id/user-accounts-uploads/:upload_id" do
    # This endpoint is only designed to be called from alambic.
    bypass_allowed_ip_enforcement
    @route_owner = "@github/admin-experience"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL

    business = find_business!
    control_access :write_business_enterprise_installation_user_accounts,
      resource: business,
      allow_integrations: true, allow_user_via_integration: false

    upload = find_upload!(business)
    data = receive(Hash)

    if data["state"] == "uploaded"
      saved = upload.track_uploaded
      if saved
        GitHub::PrefillAssociations.for_enterprise_installation_user_accounts_uploads([upload])
        return deliver :enterprise_installation_user_accounts_upload_hash, upload, status: 201
      else
        return deliver_error 422,
          errors: upload.errors
      end
    end

    # Don't allow any other attributes of user accounts uploads to be changed.
    deliver_error 422,
      errors: [],
      documentation_url: "Only user accounts upload state can be changed."
  end

  # Called by GitHub Connect on successful upload, to initiate a sync of the
  # uploaded data with the data stored for an installation.
  patch "/businesses/:business_id/user-accounts-uploads/:upload_id/sync" do
    @route_owner = "@github/admin-experience"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL

    business = find_business!
    control_access :write_business_enterprise_installation_user_accounts,
      resource: business,
      allow_integrations: true, allow_user_via_integration: false
    require_enterprise_installation!
    upload = find_upload!(business)

    if !upload.uploaded?
      return deliver_error! 422, message: "This file wasn't successfully uploaded."
    end

    if !upload.sync_pending?
      return deliver_error! 422, message: "This file is not pending synchronization."
    end

    EnterpriseInstallation.synchronize_user_accounts_data \
      business: business,
      installation: current_enterprise_installation,
      upload_id: upload.id,
      actor: User.ghost

    GitHub::PrefillAssociations.for_enterprise_installation_user_accounts_uploads([upload])
    return deliver :enterprise_installation_user_accounts_upload_hash, upload, status: 202
  end

  def find_business!
    record_or_404 find_business
  end

  def find_business
    Business.where(slug: params[:business_id]).first
  end

  def find_upload!(business)
    record_or_404 find_upload(business)
  end

  def find_upload(business)
    EnterpriseInstallationUserAccountsUpload.find_by \
      id: params[:upload_id], business_id: business.id
  end
end
