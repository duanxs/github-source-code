# rubocop:disable Style/FrozenStringLiteralComment

class Api::RepositoryStats < Api::App
  areas_of_responsibility :traffic_graphs, :api

  # Get a list of contributors with weekly additions, deletions and commit counts
  get "/repositories/:repository_id/stats/contributors" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#get-all-contributor-commit-activity"
    repo  = find_non_empty_repo
    control_access :get_repo_stats, resource: repo, allow_integrations: true, allow_user_via_integration: true

    graph_data = begin
                   GitHub::RepoGraph.contributors_data(repo, viewer: current_user, force_eventer: params[:force_eventer])
                 rescue GitHub::RepoGraph::UnusableDataError, GitHub::RepoGraph::InvalidRepoError
                   []
                 end

    if graph_data.nil?
      deliver_empty status: 202
    else
      deliver :contributor_totals_hash, graph_data
    end
  end

  # Get the last year of commit activity data
  get "/repositories/:repository_id/stats/commit_activity" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#get-the-last-year-of-commit-activity"
    repo  = find_non_empty_repo
    control_access :get_repo_stats, resource: repo, allow_integrations: true, allow_user_via_integration: true

    graph_data = begin
                   GitHub::RepoGraph.commit_activity_data(repo, viewer: current_user, force_eventer: params[:force_eventer])
                 rescue GitHub::RepoGraph::UnusableDataError
                   []
                 end

    if graph_data.nil?
      deliver_empty status: 202
    else
      deliver_raw graph_data
    end
  end

  # Get the number of additions and deletions per week
  get "/repositories/:repository_id/stats/code_frequency" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#get-the-weekly-commit-activity"
    halt 403 if logged_in? && current_user.spammy?
    repo  = find_non_empty_repo
    control_access :get_repo_stats, resource: repo, allow_integrations: true, allow_user_via_integration: true

    graph_data = begin
                   GitHub::RepoGraph.code_frequency_data(repo, viewer: current_user, force_eventer: params[:force_eventer])
                 rescue GitHub::RepoGraph::UnusableDataError
                   []
                 end

    if graph_data.nil?
      deliver_empty status: 202
    else
      deliver_raw graph_data
    end
  end

  # Get the number of commits per hour in day
  get "/repositories/:repository_id/stats/punch_card" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#get-the-hourly-commit-count-for-each-day"
    repo  = find_non_empty_repo
    control_access :get_repo_stats, resource: repo, allow_integrations: true, allow_user_via_integration: true

    graph_data = begin
                   GitHub::RepoGraph.punch_card_data(repo)
                 rescue GitHub::RepoGraph::UnusableDataError
                   GitHub::RepoGraph::PunchCard.empty_graph
                 end
    if graph_data.nil?
      deliver_empty status: 202
    else
      deliver_raw graph_data
    end
  end

  # Get the weekly commit count for both the repo owner and everyone else
  get "/repositories/:repository_id/stats/participation" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#get-the-weekly-commit-count"
    repo = find_repo!
    control_access :get_repo_stats, resource: repo, allow_integrations: true, allow_user_via_integration: true

    if repo.empty?
      # Ugh.  Don't break the Windows app.
      deliver_raw all: [], owner: []
    else
      graph_data = begin
                     GitHub::RepoGraph.participation_data(repo)
                   rescue GitHub::RepoGraph::UnusableDataError
                     GitHub::RepoGraph::Participation.empty_graph
                   end

      if graph_data.nil?
        deliver_empty status: 202
      else
        deliver_raw graph_data
      end
    end
  end

  private

  def find_non_empty_repo
    if repo = find_repo!
      if repo.empty?
        halt 204
      else
        repo
      end
    end
  end
end
