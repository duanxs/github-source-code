# frozen_string_literal: true

class Api::RepositorySecretScanning < Api::App
  areas_of_responsibility :token_scanning, :api

  # check the status secret scanning feature on a repo
  get "/repositories/:repository_id/secret-scanning" do
    @route_owner = "@github/dsp-token-scanning"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED

    repo = find_repo!

    control_access :admin_secret_scanning,
      resource: repo,
      allow_integrations: true,
      allow_user_via_integration: false

    validate_repository_access(repo)

    if repo.token_scanning_enabled?
      deliver_empty(status: 204)
    else
      deliver_error!(404, message: "Secret scanning is disabled")
    end
  end

  # enable secret scanning feature for a repo
  put "/repositories/:repository_id/secret-scanning" do
    @route_owner = "@github/dsp-token-scanning"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED

    repo = ActiveRecord::Base.connected_to(role: :reading) { find_repo! }

    control_access :admin_secret_scanning,
      resource: repo,
      allow_integrations: true,
      allow_user_via_integration: false

    validate_repository_access(repo)

    repo.enable_token_scanning(actor: current_user)

    # if we opt into token scanning, there is implicit opt-in to content analysis.
    repo.enable_content_analysis(actor: current_user)

    # we need to ensure the repo is marked for backfill
    repo.ensure_current_token_scan_status_entry!

    if repo.token_scanning_enabled?
      deliver_empty(status: 204)
    else
      deliver_error!(422, message: "Failed to enable secret scanning")
    end
  end

  # disable secret scanning feature for a repo
  delete "/repositories/:repository_id/secret-scanning" do
    @route_owner = "@github/dsp-token-scanning"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED

    repo = ActiveRecord::Base.connected_to(role: :reading) { find_repo! }

    control_access :admin_secret_scanning,
      resource: repo,
      allow_integrations: true,
      allow_user_via_integration: false

    validate_repository_access(repo)

    repo.disable_token_scanning(actor: current_user)

    # we need to ensure the repo is marked for removal from backfill
    repo.ensure_current_token_scan_status_entry!

    if !repo.token_scanning_enabled?
      deliver_empty(status: 204)
    else
      deliver_error!(422, message: "Failed to disable secret scanning")
    end
  end

  def validate_repository_access(repo)

    # The org must be opted into secret scanning api
    return deliver_error!(404) unless repo.owner.organization? && repo.owner.secret_scanning_api_enabled?

    # This endpoint is only exposed when reached from apps that are opted into the secret scanning api
    return deliver_error!(403) unless current_integration&.secret_scanning_api_enabled?
  end
end
