# frozen_string_literal: true

class Api::CheckSuites < Api::App
  # Get a check suite.
  get "/repositories/:repository_id/check-suites/:check_suite_id" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = "/rest/reference/checks#get-a-check-suite"
    require_preview(:checks)

    repo = find_repo!

    control_access :read_check_suite,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true

    check_suite = CheckSuite.find_by(id: params[:check_suite_id])
    record_or_404(check_suite)
    deliver_error! 404 if repo.id != check_suite.repository_id

    GitHub::PrefillAssociations.for_check_suites([check_suite], repo)
    deliver :check_suite_hash, check_suite, compare_payload_to: :check_suite_candidate
  end

  CheckSuiteQuery = PlatformClient.parse <<~'GRAPHQL'
    query($checkSuiteId: ID!) {
      node(id: $checkSuiteId) {
        ... on CheckSuite {
          ...Api::Serializer::ChecksDependency::CheckSuiteFragment
        }
      }
    }
  GRAPHQL

  def check_suite_candidate(check_suite, _ = {})
    variables = {
      "checkSuiteId" => check_suite.global_relay_id,
    }

    results = platform_execute(CheckSuiteQuery, variables: variables)

    if has_graphql_system_errors?(results)
      deprecated_deliver_graphql_error!({
        errors: results.errors.all,
        resource: "CheckSuite",
        documentation_url: @documentation_url,
      })
    end

    graphql_check_suite = results.data.node

    Api::Serializer.serialize(:graphql_check_suite_hash, graphql_check_suite)
  end

  # List check suites for a ref.
  get "/repositories/:repository_id/commits/*/check-suites" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = "/rest/reference/checks#list-check-suites-for-a-git-reference"
    require_preview(:checks)

    repo = find_repo!

    control_access :read_check_suite,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true

    ref = params[:splat].first
    head_sha = repo.ref_to_sha(ref)
    return deliver_error 404 if head_sha.blank?
    find_commit!(repo, ref, @documentation_url)

    check_suites = CheckSuite.where(head_sha: head_sha, repository_id: repo.id)
    if id = params[:app_id].presence
      check_suites = check_suites.for_app_id(id)
    end
    if name = params[:check_name].presence
      check_suites = check_suites.with_check_run_named(name)
    end
    check_suites = paginate_rel(check_suites)
    GitHub::PrefillAssociations.for_check_suites(check_suites, repo)

    deliver :check_suites_hash, { check_suites: check_suites, total_count: check_suites.total_entries }, compare_payload_to: :check_suites_candidate
  end

  CheckSuitesQuery = PlatformClient.parse <<~'GRAPHQL'
    query($commitId: ID!, $limit: Int!, $numericPage: Int, $appId: Int, $checkName: String) {
      node(id: $commitId) {
        ... on Commit {
          checkSuites(first: $limit, numericPage: $numericPage, filterBy: { checkName: $checkName, appId: $appId }) {
            totalCount
            edges {
              node {
                ...Api::Serializer::ChecksDependency::CheckSuiteFragment
              }
            }
          }
        }
      }
    }
  GRAPHQL

  def check_suites_candidate(check_suites, _ = {})
    repo = find_repo!
    ref = params[:splat].first
    commit = find_commit!(repo, ref, @documentation_url)

    app_id = params[:app_id].present? ? params[:app_id].to_i : nil
    check_name = params[:check_name].present? ? params[:check_name] : nil

    variables = {
      "commitId" => commit.global_relay_id,
      "appId" => app_id,
      "checkName" => check_name,
      "limit" => per_page,
      "numericPage" => pagination[:page],
    }

    results = platform_execute(CheckSuitesQuery, variables: variables)

    if has_graphql_system_errors?(results)
      deprecated_deliver_graphql_error!({
        errors: results.errors.all,
        resource: "CheckSuites",
        documentation_url: @documentation_url,
      })
    end

    graphql_check_suites = results.data.node.check_suites
    paginator.collection_size = graphql_check_suites.total_count

    Api::Serializer.serialize(:graphql_check_suites_hash, { check_suites: graphql_check_suites.edges.map(&:node), total_count: graphql_check_suites.total_count })
  end

  # Set preferences for check suites on a repository
  patch "/repositories/:repository_id/check-suites/preferences" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = "/rest/reference/checks#update-repository-preferences-for-check-suites"
    @accepted_scopes = %w(repo)
    require_preview(:checks)

    repo = find_repo!
    # Return forbidden message about authentication, when the user could otherwise admin the repo
    if can_access_repo?(repo)
      set_forbidden_message "You must authenticate with a personal access token, or basic auth, or via a GitHub App in order to change check suite permissions.".freeze
    end

    control_access :set_check_suite_preferences,
      resource: repo,
      user: current_user,
      allow_integrations: true,
      allow_user_via_integration: true

    data = receive_with_schema("check-suite-preference", "update")

    auto_trigger_checks = data.fetch("auto_trigger_checks", [])

    auto_trigger_checks.each do |preference|
      # we don't allow setting check suite preferences for full trust apps
      app = Integration.third_party.find_by(id: preference["app_id"])
      if app && allowed_to_modify_app?(app_id: app.id)
        repo.set_auto_trigger_checks(actor: current_user, app: app, value: preference["setting"].to_s)
      else
        deliver_error!(403, message: "Invalid app_id `#{preference["app_id"]}` - check suite can only be modified by the GitHub App that created it.")
      end
    end

    deliver :check_suite_preferences_hash, repo
  end

  # Create a Check Suite
  post "/repositories/:repository_id/check-suites" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = "/rest/reference/checks#create-a-check-suite"
    require_preview(:checks)

    repo = find_repo!

    control_access :write_check_suite,
      resource: repo,
      allow_integrations: true,
      allow_user_via_integration: true

    data = receive_with_schema("check-suite", "create-legacy")

    # Temporarily cover all bases while old fields are transitioned out
    # see https://github.com/github/ecosystem-api/issues/1142
    data["sha"]         ||= data["head_sha"]
    data["head_sha"]    ||= data["sha"]

    data["head_sha"] = find_commit!(repo, data["head_sha"], @documentation_url).oid

    push = Push.where(after: data["head_sha"], repository_id: repo.id).first

    new_check_suite_attrs = {
      head_sha: data["head_sha"],
      github_app: current_integration,
      push: push,
      external_id: data["external_id"],
      head_branch: push&.branch_name,
      repository: repo,
    }

    result = CheckSuite.find_or_create_for_integrator(new_check_suite_attrs)

    # preserve historical special case for this kind of error
    if result.duplicate_for_sha?
      deliver_error! 422,
                   message: "A CheckSuite already exists for this sha",
                   documentation_url: @documentation_url
    elsif result.success?
      GitHub::PrefillAssociations.for_check_suites([result.record], result.record.repository)
      status = result.existing? ? 200 : 201
      deliver :check_suite_hash, result.record, status: status
    else
      deliver_error! 422,
        errors: result.errors,
        documentation_url: @documentation_url
    end
  end

  # Rerequest a check suite
  post "/repositories/:repository_id/check-suites/:check_suite_id/rerequest" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = "/rest/reference/checks#rerequest-a-check-suite"
    require_preview(:checks)

    # Introducing strict validation of the check-suite.rerequest
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("check-suite", "rerequest", skip_validation: true)

    repo = find_repo!

    control_access :request_check_suite,
      resource: repo,
      allow_integrations: true,
      allow_user_via_integration: true

    check_suite = CheckSuite.find_by(id: params[:check_suite_id])
    record_or_404(check_suite)
    deliver_error! 404 if repo.id != check_suite.repository_id

    unless allowed_to_modify_app?(app_id: check_suite.github_app_id)
      deliver_error!(403, message: "Invalid check_suite_id `#{params[:check_suite_id]}`")
    end

    begin
      check_suite.rerequest(actor: current_user, only_failed_check_suites: check_suite.actions_app?)
      deliver_empty status: 201
    rescue CheckSuite::ExpiredWorkflowRunError
      deliver_error!(403, message: "Unable to re-run this check suite because it was created over a month ago.")
    rescue CheckSuite::AlreadyRerunningError
      deliver_error!(403, message: "This check suite is already re-running.")
    rescue CheckSuite::NotRerequestableError
      deliver_error!(403, message: "This check suite is not rerequestable")
    end
  end

  private

  # Can they see that the repo exists?
  def can_access_repo?(repo)
    return true if repo.public?
    repo.pullable_by?(current_user) && scope?(current_user, "repo")
  end
end
