# rubocop:disable Style/FrozenStringLiteralComment

class Api::Git < Api::App
  areas_of_responsibility :git, :api

  # Private endpoint used by GitHub Desktop to determine (via the
  # X-Poll-Interval header) how often this repository should be `git fetch`ed.
  get "/repositories/:repository_id/git" do
    @route_owner = "@github/pe-repos"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL
    control_access :git_poll,
      resource: find_repo!,
      allow_integrations: true,
      allow_user_via_integration: true

    skip_rate_limit!
    set_poll_interval_header!

    deliver_empty status: 204
  end

private

  def poll_interval
    if desktop_high_fetch_frequency_enabled?
      GitHub.desktop_fetch_interval.minutes
    else
      1.hour
    end
  end

  def desktop_high_fetch_frequency_enabled?
    return false unless user_agent.github_desktop?

    if user_agent.github_windows?
      user_agent.github_app_version.major >= 3
    elsif user_agent.github_desktop_tng?
      true
    else
      case user_agent.github_app_version.major
      when 0..207
        false
      when 208
        user_agent.github_app_version.minor >= 1
      else
        true
      end
    end
  end
end
