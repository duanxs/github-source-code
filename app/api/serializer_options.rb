# rubocop:disable Style/FrozenStringLiteralComment

module Api
  class SerializerOptions < GitHub::Options.new \
    :accept_mime_types,
    :after_id,
    :assets,
    :comment_counts,
    :compare_payload_to,
    :compare_with,
    :current_user,
    :current_integration,
    :detail,
    :diff,
    :emails,
    :exclude, # for migrations
    :etag,
    :exclude_email,
    :files,
    :full,
    :highlight,
    :hook,
    :include_timestamps,
    :issues,
    :last_modified,
    :list,
    :max_age,
    :merged,
    :mime_params,
    :page,
    :path,
    :per_page,
    :plan,
    :private,
    :owner_private,
    :profile,
    :pusher,
    :ref,
    :release,
    :repo,
    :repositories,
    :route,
    :sha,
    :show_merge_settings,
    :generate_temp_clone_token,
    :simple,
    :summary,
    :team,
    :upload_hash,
    :url,
    :user,
    :version,
    :repo_path,
    :force, # for Media::Blobs
    :origin,
    :token,
    :license,
    :html_url,
    :business_plus, # remove after Business ship
    :code_of_conduct, # code_of_conduct preview
    :env, # Rack environment for request
    :include_values, # Repo config preview
    :pem, # For integration_hash after app creation
    :filter_project_events, # For project event details starfox preview
    :search,
    :permissions, # For token responses
    :single_file, # For token responses
    :repository_selection, # For token responses
    :installation, # For token responses
    :resolvers # For code scanning responses

    module MimeTypes
      # Check `Accept` headers against the media version of a preview.
      #
      # name - The name of the preview (symbol)
      #
      # Example:
      #
      #     options.accepts_preview?(:reactions)
      #
      # Returns true if the `Accept` headers explicitly reference the given
      #   version. Returns true if the given version is the default version, and
      #   the headers fail to reference some other (non-default) version.
      #   Returns false otherwise.
      def accepts_preview?(name)
        preview = REST::Previews.get(name)
        accepted_media_types.accepts_version?(preview.media_version)
      end

      # Check `Accept` headers against a given version.
      #
      # version - Version String (e.g., 'beta', 'v3').
      #
      # Example:
      #
      #     options.accepts_version?("squirrel-girl-preview")
      #
      # Returns true if the `Accept` headers explicitly reference the given
      #   version. Returns true if the given version is the default version, and
      #   the headers fail to reference some other (non-default) version.
      #   Returns false otherwise.
      def accepts_version?(version)
        accepted_media_types.accepts_version?(version)
      end

      def accepted_media_types
        Api::AcceptedMediaTypes.new(accept_mime_types)
      end

      # Check `Accept` headers for v3 support.
      #
      # Returns a Boolean.
      def accepts_v3?
        accepts_version?(:v3)
      end

      # Do the `Accept` headers indicate that the response should use the beta
      # media type?
      #
      # Only return true if beta is present and there are no newer versions
      # (including previews) available.
      #
      # Returns a Boolean.
      def wants_beta_media_type?
        # TODO Enhance this logic to respect the quality factor specified in the
        # `Accept` header. Only return true if the quality factors give
        # preference to the beta media type.
        # See http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html.
        accepts_version?(:beta) && !accepts_v3?
      end

      # Check Accept headers for presence of the given parameter.
      #
      # param - A String parameter name (e.g., 'raw', 'text-match', etc.).
      #
      # Returns a Boolean.
      def accepts_param?(param)
        Array(mime_params).include?(param)
      end
    end

    include MimeTypes
  end
end
