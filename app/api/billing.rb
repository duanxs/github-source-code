# frozen_string_literal: true

class Api::Billing < Api::App
  # Get Actions billing information for users
  get "/user/:user_id/settings/billing/actions" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/billing#get-github-actions-billing-for-a-user"

    deliver_error!(404) unless GitHub.flipper[:metered_billing_api].enabled?(current_integration || current_user)

    user = find_user!
    control_access :read_user_plan,
      resource: user,
      allow_integrations: false,
      allow_user_via_integration: false,
      enforce_oauth_app_policy: false

    deliver_raw(serialize_actions_usage(billable_owner: user))
  end

  # Get Actions billing information for organizations
  get "/organizations/:organization_id/settings/billing/actions" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/billing#get-github-actions-billing-for-an-organization"

    deliver_error!(404) unless GitHub.flipper[:metered_billing_api].enabled?(current_integration || current_user)

    org = find_org!
    control_access :view_org_settings,
      resource: org,
      allow_integrations: false,
      allow_user_via_integration: false,
      enforce_oauth_app_policy: false

    if org.delegate_billing_to_business?
      deliver_raw(serialize_actions_usage(billable_owner: org.business, owner: org))
    else
      deliver_raw(serialize_actions_usage(billable_owner: org))
    end
  end

  # Get Actions billing information for enterprises
  get "/enterprises/:enterprise_id/settings/billing/actions" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/billing#get-github-actions-billing-for-an-enterprise"

    deliver_error!(404) unless GitHub.flipper[:metered_billing_api].enabled?(current_integration || current_user)

    enterprise = find_enterprise!
    control_access :manage_business_billing,
      resource: enterprise,
      allow_integrations: false,
      allow_user_via_integration: false,
      enforce_oauth_app_policy: false

    deliver_raw(serialize_actions_usage(billable_owner: enterprise))
  end

  def serialize_actions_usage(billable_owner:, owner: nil)
    actions_usage = ::Billing::ActionsUsage.new(billable_owner, owner: owner)
    private_minutes_used = actions_usage.private_minutes_used.clone
    private_minutes_used.delete("TOTAL")
    private_minutes_used.each { |key, val| private_minutes_used[key] = val.to_i }

    {
      total_minutes_used: actions_usage.total_minutes_used.to_i,
      total_paid_minutes_used: actions_usage.total_paid_minutes_used,
      included_minutes: actions_usage.included_private_minutes,
      minutes_used_breakdown: private_minutes_used
    }
  end

  # Get Packages billing information for users
  get "/user/:user_id/settings/billing/packages" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/billing#get-github-packages-billing-for-a-user"

    deliver_error!(404) unless GitHub.flipper[:metered_billing_api].enabled?(current_integration || current_user)

    user = find_user!
    control_access :read_user_plan,
      resource: user,
      allow_integrations: false,
      allow_user_via_integration: false,
      enforce_oauth_app_policy: false

    deliver_raw(serialize_packages_usage(billable_owner: user))
  end

  # Get Packages billing information for organizations
  get "/organizations/:organization_id/settings/billing/packages" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/billing#get-github-packages-billing-for-an-organization"

    deliver_error!(404) unless GitHub.flipper[:metered_billing_api].enabled?(current_integration || current_user)

    org = find_org!
    control_access :view_org_settings,
      resource: org,
      allow_integrations: false,
      allow_user_via_integration: false,
      enforce_oauth_app_policy: false

    if org.delegate_billing_to_business?
      deliver_raw(serialize_packages_usage(billable_owner: org.business, owner: org))
    else
      deliver_raw(serialize_packages_usage(billable_owner: org))
    end
  end

  # Get Packages billing information for enterprises
  get "/enterprises/:enterprise_id/settings/billing/packages" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/billing#get-github-packages-billing-for-an-enterprise"

    deliver_error!(404) unless GitHub.flipper[:metered_billing_api].enabled?(current_integration || current_user)

    enterprise = find_enterprise!
    control_access :manage_business_billing,
      resource: enterprise,
      allow_integrations: false,
      allow_user_via_integration: false,
      enforce_oauth_app_policy: false

    deliver_raw(serialize_packages_usage(billable_owner: enterprise))
  end

  def serialize_packages_usage(billable_owner:, owner: nil)
    packages_usage = ::Billing::PackageRegistryUsage.new(billable_owner, owner: owner)
    {
      total_gigabytes_bandwidth_used: packages_usage.total_gigabytes_used,
      total_paid_gigabytes_bandwidth_used: packages_usage.billable_gigabytes,
      included_gigabytes_bandwidth: packages_usage.plan_included_bandwidth
    }
  end

  # Get shared storage billing information for shared users
  get "/user/:user_id/settings/billing/shared-storage" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/billing#get-shared-storage-billing-for-a-user"

    deliver_error!(404) unless GitHub.flipper[:metered_billing_api].enabled?(current_integration || current_user)

    user = find_user!
    control_access :read_user_plan,
      resource: user,
      allow_integrations: false,
      allow_user_via_integration: false,
      enforce_oauth_app_policy: false

    deliver_raw(serialize_shared_storage_usage(billable_owner: user))
  end

  # Get shared storage billing information for shared organizations
  get "/organizations/:organization_id/settings/billing/shared-storage" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/billing#get-shared-storage-billing-for-an-organization"

    deliver_error!(404) unless GitHub.flipper[:metered_billing_api].enabled?(current_integration || current_user)

    org = find_org!
    control_access :view_org_settings,
      resource: org,
      allow_integrations: false,
      allow_user_via_integration: false,
      enforce_oauth_app_policy: false

    if org.delegate_billing_to_business?
      deliver_raw(serialize_shared_storage_usage(billable_owner: org.business, owner: org))
    else
      deliver_raw(serialize_shared_storage_usage(billable_owner: org))
    end
  end

  # Get shared storage billing information for shared enterprises
  get "/enterprises/:enterprise_id/settings/billing/shared-storage" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/billing#get-shared-storage-billing-for-an-enterprise"

    deliver_error!(404) unless GitHub.flipper[:metered_billing_api].enabled?(current_integration || current_user)

    enterprise = find_enterprise!
    control_access :manage_business_billing,
      resource: enterprise,
      allow_integrations: false,
      allow_user_via_integration: false,
      enforce_oauth_app_policy: false

    deliver_raw(serialize_shared_storage_usage(billable_owner: enterprise))
  end

  def serialize_shared_storage_usage(billable_owner:, owner: nil)
    shared_storage_used = ::Billing::SharedStorageUsage.new(billable_owner, owner: owner)
    {
      days_left_in_billing_cycle: shared_storage_used.days_left_in_billing_cycle,
      estimated_paid_storage_for_month: shared_storage_used.estimated_monthly_paid_megabytes / 1024,
      estimated_storage_for_month: shared_storage_used.estimated_monthly_private_megabytes / 1024,
    }
  end

end
