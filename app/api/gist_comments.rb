# rubocop:disable Style/FrozenStringLiteralComment

class Api::GistComments < Api::App
  # list all comments on a gist
  get "/gists/:gist_id/comments" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/gists#list-gist-comments"
    control_access :list_gist_comments, resource: gist, allow_integrations: false, allow_user_via_integration: false

    comments = gist.comments
    comments = paginate_rel(comments)
    GitHub::PrefillAssociations.for_gist_comments(comments, gist)

    deliver :gist_comment_hash, comments
  end

  # get a gist comment
  get "/gists/:gist_id/comments/:comment_id" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/gists#get-a-gist-comment"
    comment = gist.comments.find_by_id(params[:comment_id])
    control_access :get_gist_comment, resource: comment, allow_integrations: false, allow_user_via_integration: false
    GitHub::PrefillAssociations.for_gist_comments([comment], gist)
    deliver :gist_comment_hash, comment, last_modified: calc_last_modified(comment)
  end

  # create a gist comment
  post "/gists/:gist_id/comments" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/gists#create-a-gist-comment"
    control_access :create_gist_comment, resource: gist, allow_integrations: false, allow_user_via_integration: false
    ensure_not_blocked! current_user, gist.user_id

    # Introducing strict validation of the gist-comment.create
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    data = receive_with_schema("gist-comment", "create", skip_validation: true)

    comment = gist.comments.build \
      body: data["body"]
    comment.user = current_user

    if comment.save
      GitHub.dogstats.increment("comment", tags: ["action:create", "subject:gist", "via:api", "visibility:#{gist.visibility}", "ownership:#{gist.ownership}"])

      deliver :gist_comment_hash, comment, status: 201
    else
      deliver_error 422,
        errors: comment.errors,
        documentation_url: @documentation_url
    end
  end

  # update a gist comment
  verbs :patch, :post, "/gists/:gist_id/comments/:comment_id" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/gists#update-a-gist-comment"
    comment = gist.comments.find_by_id(params[:comment_id])
    control_access :update_gist_comment, resource: comment, allow_integrations: false, allow_user_via_integration: false

    data = receive_with_schema("gist-comment", "update-legacy")
    comment.update_body(data["body"], current_user)

    deliver :gist_comment_hash, comment
  end

  # delete a gist comment
  delete "/gists/:gist_id/comments/:comment_id" do
    @route_owner = "@github/pe-repos"
    receive_with_schema("gist-comment", "delete")

    @documentation_url = "/rest/reference/gists#delete-a-gist-comment"
    comment = gist.comments.find_by_id(params[:comment_id])
    control_access :delete_gist_comment, resource: comment, allow_integrations: false, allow_user_via_integration: false

    comment.delete

    deliver_empty status: 204
  end

  private

  def gist
    @gist ||= find_gist!(param_name: :gist_id)
  end
end
