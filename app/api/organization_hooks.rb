# rubocop:disable Style/FrozenStringLiteralComment

class Api::OrganizationHooks < Api::App
  areas_of_responsibility :api, :webhook

  statsd_tag_actions "/organizations/:organization_id/hooks"
  statsd_tag_actions "/organizations/:organization_id/hooks/:hook_id"

  get "/organizations/:organization_id/hooks" do
    @route_owner = "@github/ecosystem-events"
    @documentation_url = "/rest/reference/orgs#list-organization-webhooks"
    org = find_org!
    control_access :list_org_hooks, resource: org, allow_integrations: true, allow_user_via_integration: true

    hooks = paginate_rel(filtered_hooks)
    GitHub::PrefillAssociations.for_hooks(hooks)
    deliver :org_hook_hash, hooks
  end

  get "/organizations/:organization_id/hooks/:hook_id" do
    @route_owner = "@github/ecosystem-events"
    @documentation_url = "/rest/reference/orgs#get-an-organization-webhook"
    org = find_org!
    control_access :read_org_hook, resource: org, allow_integrations: true, allow_user_via_integration: true

    hook = find_hook!(param_name: :hook_id)
    GitHub::PrefillAssociations.for_hooks([hook])
    deliver :org_hook_hash, hook, full: true, last_modified: calc_last_modified(hook)
  end

  post "/organizations/:organization_id/hooks/:hook_id/pings" do
    @route_owner = "@github/ecosystem-events"
    receive_with_schema("organization-hook", "ping")

    @documentation_url = "/rest/reference/orgs#ping-an-organization-webhook"
    org = find_org!
    control_access :test_org_hook, resource: org, allow_integrations: true, allow_user_via_integration: true

    hook = find_hook!(param_name: :hook_id)
    hook.ping
    deliver_empty(status: 204)
  end

  post "/organizations/:organization_id/hooks" do
    @route_owner = "@github/ecosystem-events"
    @documentation_url = "/rest/reference/orgs#create-an-organization-webhook"
    org = find_org!
    control_access :create_org_hook, resource: org, allow_integrations: true, allow_user_via_integration: true

    # Introducing strict validation of the organization-hook.create
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    data = receive_with_schema("organization-hook", "create", skip_validation: true)
    name = data["name"]

    hook = org.hooks.build(name: name)

    hook.track_creator(current_user)
    hook.events = Array(data["events"])
    hook.add_events("push") if hook.events.blank?
    hook.active = true
    hook.config = data["config"]

    if hook.save
      deliver :org_hook_hash, hook, status: 201,
        full: true
    else
      deliver_error 422,
        errors: hook.errors,
        documentation_url: @documentation_url
    end
  end

  patch "/organizations/:organization_id/hooks/:hook_id" do
    @route_owner = "@github/ecosystem-events"
    @documentation_url = "/rest/reference/orgs#update-an-organization-webhook"
    org = find_org!
    control_access :update_org_hook, resource: org, allow_integrations: true, allow_user_via_integration: true

    hook = find_hook!(param_name: :hook_id)

    # Introducing strict validation of the organization-hook.update
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    data = receive_with_schema("organization-hook", "update", skip_validation: true)
    attributes = attr(data, :active, :config)
    if (events = Array(data["events"])).present?
      attributes[:events] = events
    end

    if hook.update attributes
      deliver :org_hook_hash, hook
    else
      deliver_error 422,
        errors: hook.errors,
        documentation_url: @documentation_url
    end
  end

  delete "/organizations/:organization_id/hooks/:hook_id" do
    @route_owner = "@github/ecosystem-events"

    # Introducing strict validation of the organization-hook.delete
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("organization-hook", "delete", skip_validation: true)

    @documentation_url = "/rest/reference/orgs#delete-an-organization-webhook"
    org = find_org!
    control_access :delete_org_hook, resource: org, allow_integrations: true, allow_user_via_integration: true

    hook = find_hook!(param_name: :hook_id)
    if hook.destroy
      deliver_empty(status: 204)
    else
      deliver_error 404
    end
  end

  # Fetch the deliveries of a specific hook
  get "/organizations/:organization_id/hooks/:hook_id/deliveries" do
    @route_owner = "@github/ecosystem-events"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    require_preview(:hook_deliveries_api)
    org = find_org!
    control_access :list_org_hooks, resource: org, allow_integrations: true, allow_user_via_integration: true

    hook = find_hook!(param_name: :hook_id)

    safe_params = params.slice(:since, :until, :per_page, :status, :status_codes, :events, :redelivery, :guid, :cursor)
    status, body = Hookshot::Client.for_parent(hook.hookshot_parent_id).deliveries_for_hook(hook.id, safe_params.except(:per_page).merge(limit: per_page(safe_params["per_page"])))

    case status
    when 200
      build_cursor_based_links(body["page_info"], per_page(safe_params["per_page"]))
      deliver_raw body["deliveries"]
    when 400
      deliver_error! status, message: body["message"]
    else
      deliver_error! 500, message: "Something went wrong."
    end
  end

  # Fetch single delivery of a specific hook
  get "/organizations/:organization_id/hooks/:hook_id/deliveries/:delivery_id" do
    @route_owner = "@github/ecosystem-events"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    require_preview(:hook_deliveries_api)
    org = find_org!
    control_access :read_org_hook, resource: org, allow_integrations: true, allow_user_via_integration: true

    hook = find_hook!(param_name: :hook_id)

    safe_params = { include_payload: !!params[:full] }
    status, body = Hookshot::Client.for_parent(hook.hookshot_parent_id).delivery_for_hook(params[:delivery_id], hook.id, safe_params)

    case status
    when 200
      deliver_raw body
    when 400
      deliver_error! status, message: body["message"]
    else
      deliver_error! 500, message: "Something went wrong."
    end
  end

  # Trigger a new delivery attempt
  post "/organizations/:organization_id/hooks/:hook_id/deliveries/:delivery_id/attempts" do
    @route_owner = "@github/ecosystem-events"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    require_preview(:hook_deliveries_api)
    org = find_org!
    control_access :update_org_hook, resource: org, allow_integrations: true, allow_user_via_integration: true
    hook = find_hook!(param_name: :hook_id)
    deliver_error!(404) unless hook

    hook_client  = Hookshot::Client.for_parent(hook.hookshot_parent_id)
    status, body = hook_client.create_redelivery_attempt(int_id_param!(key: :delivery_id), hook.id, hook.config_attributes)

    case status
    when 202
      deliver_empty status: 202
    when 400
      deliver_error! status, message: body["message"]
    else
      deliver_error! 500, message: "Something went wrong."
    end
  end

  private

  def filtered_hooks
    find_org!.hooks.editable_by(current_user)
  end

  def find_hook!(param_name: :id)
    filtered_hooks.find_by_id(int_id_param!(key: param_name)) || deliver_error!(404)
  end

  def per_page(limit)
    return DEFAULT_PER_PAGE unless limit
    [limit.to_i, MAX_PER_PAGE].min
  end
end
