# rubocop:disable Style/FrozenStringLiteralComment

class Api::Deployments < Api::App
  areas_of_responsibility :api

  statsd_tag_actions "/repositories/:repository_id/deployments"

  DEFAULT_ATTRIBUTES = %i(description environment payload ref task sha creator transient_environment production_environment)

  # List the latest deployments for a repository across all shas
  get "/repositories/:repository_id/deployments" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = "/rest/reference/repos#list-deployments"
    control_access :list_deployments,
      resource: repo = find_repo!,
      allow_integrations: true,
      allow_user_via_integration: true

    filtered_options = params.slice("ref", "sha", "task", "environment").keep_if { |k, v| v.present? }
    deployments = paginate_rel(repo.deployments.where(filtered_options))

    GitHub::PrefillAssociations.for_deployments(deployments, repo)
    deliver :deployment_hash, deployments, repo: repo
  end

  # Create a deployment event for a repository at a specific ref.
  post "/repositories/:repository_id/deployments" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = "/rest/reference/repos#create-a-deployment"
    control_access :write_deployment,
      resource: repo = find_repo!,
      allow_integrations: true,
      allow_user_via_integration: true

    data = receive_with_schema("deployment", "create-legacy")
    attrs = [:auto_merge, :required_contexts, :description, :environment, :payload, :ref, :task]
    attrs.concat([:transient_environment, :production_environment]) if medias.api_preview?(:deployment_enhancements)
    data = attr(data, *attrs)

    commit  = find_ref(repo, data["ref"])
    payload = data["payload"]

    data["sha"]          = commit.oid
    data["creator"]      = current_user
    data["payload"]      = GitHub::JSON.encode(payload) if payload

    if data["environment"] == "production" && data["production_environment"].nil?
      data["production_environment"] = true
    end

    auto_merge        = data.delete("auto_merge")
    required_contexts = data.delete("required_contexts")

    deployment = repo.deployments.build(attr(data, *DEFAULT_ATTRIBUTES))

    deployment.auto_merge        = auto_merge
    deployment.required_contexts = required_contexts

    if integration_user_request?
      deployment.performed_via_integration = current_integration
    end

    if deployment.auto_merge?
      message = "Auto-merged #{repo.default_branch} into #{deployment.ref} on deployment."
      options = {
        reflog_data: request_reflog_data("auto-merge deployment api"),
        commit_message: message,
      }
      if deployment.merge_for(current_user, options)
        deliver_raw({message: message}, status: 202)
      else
        deliver_error(409,
          message: "Conflict merging #{repo.default_branch} into #{deployment.ref}.")
      end
    elsif deployment.failed_commit_statuses?
      validation_error = api_error(:Deployment, :required_contexts, :invalid,
                                   contexts: deployment.combined_status_contexts)

      deliver_error 409, errors: [validation_error],
        message: "Conflict: Commit status checks failed for #{deployment.ref}."
    else
      if deployment.save
        deliver :deployment_hash, deployment, repo: repo, status: 201,
          last_modified: calc_last_modified(deployment)
      else
        deliver_error 422, errors: deployment.errors
      end
    end
  end

  # Find a specific deployment for a repository
  get "/repositories/:repository_id/deployments/:deployment_id" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = "/rest/reference/repos#get-a-deployment"
    control_access :read_deployment, resource: repo = find_repo!, allow_integrations: true, allow_user_via_integration: true
    deployment = repo.deployments.find_by(id: int_id_param!(key: :deployment_id))
    record_or_404(deployment)

    GitHub::PrefillAssociations.for_deployments([deployment], repo)

    deliver :deployment_hash, deployment, repo: repo,
      last_modified: calc_last_modified(deployment)
  end

  # Delete a specific deployment for a repository
  delete "/repositories/:repository_id/deployments/:deployment_id" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = "/rest/reference/repos#delete-a-deployment"
    receive_with_schema("deployment", "delete")

    repo = find_repo!
    control_access :delete_deployment,
      resource: repo,
      allow_integrations: true,
      allow_user_via_integration: true
    deployment = repo.deployments.find_by(id: int_id_param!(key: :deployment_id))
    record_or_404(deployment)

    deployments_count = repo.deployments.where(environment: deployment.environment).count
    if deployment.active? && deployments_count > 1
      deliver_error 422, errors: ["We cannot delete an active deployment unless it is the only deployment in a given environment."]
    else
      deployment.destroy!
      deliver_empty(status: 204)
    end
  end

  def find_ref(repo, ref)
    if ref && commit_oid = repo.ref_to_sha(ref)
      repo.commits.find(commit_oid)
    else
      deliver_error! 422, message: "No ref found for: #{ref}"
    end
  rescue GitRPC::InvalidObject, GitRPC::ObjectMissing
    deliver_error! 422, message: "No ref found for: #{ref}"
  end
end
