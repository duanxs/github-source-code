# rubocop:disable Style/FrozenStringLiteralComment

module Api::Serializer::OrganizationsDependency
  # Creates a Hash to be serialized to JSON.
  #
  # org     - Organiztion instance
  # options - Hash
  #           :full    - Boolean specifying we want the extended output.
  #           :private - Boolean specifying we want the private output.
  #
  # Returns a Hash if the org exists, or nil.
  def organization_hash(org, options = {})
    return nil if !org
    options = Api::SerializerOptions.from(options)
    org_api_path = "/orgs/#{org}"
    hash = {
      login: org.login,
      id: org.id,
      node_id: org.global_relay_id,
      url: url(org_api_path, options),
      repos_url: url("#{org_api_path}/repos", options),
      events_url: url("#{org_api_path}/events", options),
      hooks_url: url("#{org_api_path}/hooks", options),
      issues_url: url("#{org_api_path}/issues", options),
      members_url: url("#{org_api_path}/members{/member}", options),
      public_members_url: url("#{org_api_path}/public_members{/member}", options),
      avatar_url: avatar(org),
      description: org.description,
    }

    include_details =  options[:full] || options[:private] || options[:owner_private]

    if include_details && (profile = org.profile)
      hash.update \
        name: profile.name,
        company: profile.company,
        blog: profile.blog,
        location: profile.location,
        email: profile.email,
        twitter_username: profile.twitter_username
    end

    if include_details && GitHub.domain_verification_enabled?
      hash[:is_verified] = org.is_verified?
    end

    if include_details
      hash.update \
        has_organization_projects: org.organization_projects_enabled?,
        has_repository_projects: org.repository_projects_enabled?,
        public_repos: org.repository_counts.public_repositories,
        public_gists: org.repository_counts.public_gists,
        followers: org.followers_count, # TODO: Remove this in v3
        following: org.following_count, # TODO: Remove this in v3
        html_url: org.permalink,
        created_at: time(org.created_at),
        updated_at: time(org.updated_at),
        type: org[:type]
    end

    if options[:private] || options[:owner_private]
      hash.update \
        total_private_repos: org.repository_counts.private_repositories,
        owned_private_repos: org.repository_counts.owned_private_repositories,
        private_gists: nil,
        disk_usage: nil,
        collaborators: nil,
        billing_email: nil,
        default_repository_permission: nil,
        members_can_create_repositories: org.members_can_create_repositories?,
        two_factor_requirement_enabled: nil

      if options.accepts_preview?(:repo_creation_permissions)
        hash[:members_allowed_repository_creation_type] = org.members_allowed_repository_creation_type
        hash[:members_can_create_public_repositories] = org.members_can_create_public_repositories?
        hash[:members_can_create_private_repositories] = org.members_can_create_private_repositories?
        hash[:members_can_create_internal_repositories] = org.members_can_create_internal_repositories?
      end
    end

    if options[:private] || options[:owner_private] || options[:plan]
      plan = {
        name: org.plan.display_name,
        space: org.plan.space / 1.kilobyte,
        private_repos: org.plan.org_repos(feature_flag: :plans_munich)
      }

      if options[:current_user]
        plan.update \
          filled_seats: org.filled_seats,
          seats: org.seats
      end

      hash.update \
        plan: plan
    end

    if options[:owner_private]
      hash.update \
        private_gists: org.repository_counts.private_gists,
        disk_usage: org.disk_usage,
        collaborators: org.collaborators_count,
        billing_email: org.billing_email,
        default_repository_permission: org.default_repository_permission_name,
        two_factor_requirement_enabled: org.two_factor_requirement_enabled?
    end

    hash
  end

  def invitation_hash(invitation, options = {})
    return nil if !invitation
    options = Api::SerializerOptions.from(options)

    {}.tap do |opts|
      opts[:id]         = invitation.id
      opts[:node_id]    = invitation.global_relay_id
      opts[:login]      = invitation.email.nil? ? invitation.invitee.login : nil
      opts[:email]      = invitation.email.nil? ? fetch_field(invitation.invitee.profile, :email) : invitation.email
      opts[:role]       = invitation.role.to_s
      opts[:created_at] = invitation.created_at
      opts[:inviter]    = user_hash(invitation.inviter, content_options(options))
      opts[:team_count] = invitation.teams.count
      opts[:invitation_teams_url] = url("/organizations/#{invitation.organization_id}/invitations/#{invitation.id}/teams", options)
    end
  end

  OrganizationInvitationFragment = Api::App::PlatformClient.parse <<-'GRAPHQL'
    fragment on OrganizationInvitation {
      id
      databaseId
      role
      email
      createdAt
      invitee {
        login
      }
      inviter {
        ...Api::Serializer::UserDependency::SimpleUserFragment
      }
      teams {
        totalCount
      }
      organization {
        databaseId
      }
    }
  GRAPHQL

  def graphql_invitation_hash(invitation, options = {})
    invitation = OrganizationInvitationFragment.new(invitation)
    options = Api::SerializerOptions.from(options)

    {
      id: invitation.database_id,
      node_id: invitation.id,
      login: invitation.invitee&.login,
      email: invitation.email,
      role: invitation.role&.downcase,
      created_at: time(invitation.created_at),
      inviter: graphql_simple_user_hash(invitation.inviter),
    }.tap do |invitation_hash|
      invitation_hash[:team_count] = invitation.teams.total_count
      invitation_hash[:invitation_teams_url] = url("/organizations/#{invitation.organization.database_id}/invitations/#{invitation.database_id}/teams", options)
    end
  end

  # Creates a Hash to be serialized to JSON.
  #
  # team    - Team instance
  # options - Hash
  #           :full    - Boolean specifying we want the extended output.
  #
  # Returns a Hash if the team exists, or nil.
  def team_hash(team, options = {})
    return unless team
    options = Api::SerializerOptions.from(options)
    hash = team_simple_hash(team, options)

    hash[:ldap_dn] = team.ldap_dn if team.ldap_mapped?

    if options[:full]
      hash.update \
        created_at: time(team.created_at),
        updated_at: time(team.updated_at),
        members_count: team.members_scope_count(membership: :all),
        repos_count: team.repositories_scope(affiliation: :all).count,
        organization: organization_hash(team.organization, full: true)
    end

    hash[:parent] = team_simple_hash(team.parent_team, options)

    hash
  end

  # Internal: a method to build a hash that matches the team-simple schema. The
  # idea here is that you could compose more complex team representations using
  # this method to give you a base to work from.
  #
  # Returns Hash
  private def team_simple_hash(team, options = {})
    return unless team

    team_api_path = "/organizations/#{team.organization_id}/team/#{team.id}"
    hash = {
      name: team.name,
      id: team.id,
      node_id: team.global_relay_id,
      slug: team.to_param,
      description: team.description,
      privacy: team.privacy.to_s,
      url: url(team_api_path),
      html_url: team.permalink,
      members_url: url("#{team_api_path}/members{/member}"),
      repositories_url: url("#{team_api_path}/repos"),
    }

    if options[:repo].present?
      hash[:permission] = team.permission_for(options[:repo])
    else
      hash[:permission] = team.permission.blank? ? "pull" : team.permission
    end

    hash
  end

  SimpleTeamFragment = Api::App::PlatformClient.parse <<-'GRAPHQL'
    fragment on Team {
      createdAt @include(if: $includeFullTeamDetails)
      updatedAt @include(if: $includeFullTeamDetails)
      name
      databaseId
      id
      slug
      description
      privacy
      permission
      ldapDn
      resourcePath
      parentTeam {
        name
        databaseId
        id
        slug
        description
        privacy
        permission
        resourcePath
        membersResourcePath
        repositoriesResourcePath
        organization {
          databaseId
        }
      }
      organization {
        databaseId
      }
      immediate_members: members(membership: IMMEDIATE) {
        totalCount
      }
      all_members: members(membership: ALL) @include(if: $includeFullTeamDetails){
        totalCount
      }
      immediate_repositories: repositories(affiliation: IMMEDIATE) {
        totalCount
      }
      all_repositories: repositories @include(if: $includeFullTeamDetails){
        totalCount
      }
      resourcePath
      membersResourcePath
      repositoriesResourcePath
    }
  GRAPHQL

  TeamFragment = Api::App::PlatformClient.parse <<-'GRAPHQL'
    fragment on Team {
      ...Api::Serializer::OrganizationsDependency::SimpleTeamFragment
      organization {
        hasProfile
        hasOrganizationProjectsEnabled
        hasRepositoryProjectsEnabled
        isVerified
        avatarUrl
        description
        login
        name
        company
        location
        blog: websiteUrl
        twitterUsername
        email
        databaseId
        id
        createdAt
        updatedAt
        resourcePath
        repositories(privacy: PUBLIC) {
          totalCount
        }
        gists(privacy: PUBLIC) {
          totalCount
        }
      }
    }
  GRAPHQL

  def graphql_team_hash(team, options = {})
    if options[:full]
      team_with_organization = TeamFragment.new(team)
      team = SimpleTeamFragment.new(team_with_organization)
    else
      team = SimpleTeamFragment.new(team)
    end

    hash = build_team_hash(team)
    options = Api::SerializerOptions.from(options)

    if team.parent_team.present?
      hash[:parent] = build_team_hash(team.parent_team)
    else
      hash[:parent] = nil
    end

    if options[:full]
      # We can't use team.ldap_mapped? here as this is a GraphQL TeamFragment
      # not a Team object
      hash[:ldap_dn] = team.ldap_dn if GitHub.ldap_sync_enabled? && team.ldap_dn?

      hash.update \
        created_at: time(team.created_at),
        updated_at: time(team.updated_at),
        members_count: team.all_members.total_count,
        repos_count: team.all_repositories.total_count,
        organization: build_organization_hash(team_with_organization.organization, full: true)
    end

    hash
  end

  # Creates a Hash to be serialized to JSON.
  #
  # org     - Organization instance
  # options - Hash
  #           :user - User instance to check membership of.
  #           :full = Whether or not to include the Organization.
  #
  # Returns a Hash if the organization and user exist, or nil.
  def org_membership_hash(org, options = {})
    options = Api::SerializerOptions.from(options)
    user = options[:user]
    wants_org = options[:full].nil? ? true : options[:full]
    wants_org_permissions = options.accepts_param?(:permissions)

    return nil unless org
    return nil unless user

    role = if !org.member?(user) && invitation = org.pending_invitation_for(user)
      invitation.role
    else
      org.role_of(user).type
    end

    # Sanitize the role
    case role.to_s
    when "direct_member"
      role = :member
    when "outside_collaborator"
      role = :unaffiliated
    end

    can_create_repository = false
    if wants_org_permissions
      can_create_repository = invitation.nil? && org.can_create_repository?(user, role: role)
    end

    hash = {
      url: url("/orgs/#{org.login}/memberships/#{user.login}"),
      state: org.membership_state_of(user).to_s,
      role: role.to_s,
      organization_url: url("/orgs/#{org.login}"),
      user: user_hash(user, content_options(options)),
    }

    if wants_org
      hash.update(organization: organization_hash(org))
    end

    if wants_org_permissions
      hash.update \
        permissions: {
          can_create_repository: can_create_repository,
        }
    end

    hash
  end

  # Creates a Hash to be serialized to JSON.
  #
  # data    - Hash
  #           :team - Team instance
  #           :user - User instance
  # options - Hash (unused)
  #
  # Returns a Hash if the team and user exist and there's a membership between
  # them, or nil.
  def team_membership_hash(data, options = {})
    team = data[:team]
    user = data[:user]

    return nil unless team.present?
    return nil unless user.present?

    role = if team.maintainer?(user) || team.owner?(user)
      "maintainer"
    elsif Team.member_of?(team.id, user.id, immediate_only: false)
      "member"
    elsif invitation = team.pending_invitation_for(user)
      # If the user is invited to the org, check if they're invited to this
      # team.
      if team_invitation = invitation.team_invitation_for(team)
        # If they are, use their role from that invitation
        team_invitation.role.to_s
      end
    end

    {
      state: team.membership_state_of(user, immediate_only: false).to_s,
      role: role || "unaffiliated",
      url: url("/organizations/#{team.organization_id}/team/#{team.id}/memberships/#{user.login}"),
    }
  end

  private def build_team_hash(team)
    team_api_path = "/organizations/#{team.organization.database_id}/team/#{team.database_id}"
    {
      name: team.name,
      id: team.database_id,
      node_id: team.id,
      slug: team.slug,
      description: team.description,
      privacy: team.privacy.downcase == "visible" ? "closed" : team.privacy.downcase,
      url: url(team_api_path),
      html_url: html_url(team.resource_path),
      members_url: url("#{team_api_path}/members{/member}"),
      repositories_url: url("#{team_api_path}/repos"),
      permission: team.permission.downcase,
    }
  end

  private def build_organization_hash(org, options = {})
    org_api_path = "/orgs#{org.resource_path}"
    hash = {
      login: org.login,
      id: org.database_id,
      node_id: org.id,
      url: url(org_api_path),
      repos_url: url("#{org_api_path}/repos"),
      events_url: url("#{org_api_path}/events"),
      hooks_url: url("#{org_api_path}/hooks"),
      issues_url: url("#{org_api_path}/issues"),
      members_url: url("#{org_api_path}/members{/member}"),
      public_members_url: url("#{org_api_path}/public_members{/member}"),
      avatar_url: org.avatar_url.to_s,
      description: org.description,
    }

    include_details = options[:full]
    if include_details && org.has_profile?
      hash.update \
        name: org.name,
        company: org.company,
        blog: org.blog.to_s,
        location: org.location,
        email: org.email,
        twitter_username: org.twitter_username
    end

    if include_details && GitHub.domain_verification_enabled?
      hash[:is_verified] = org.is_verified?
    end

    if include_details
      hash.update \
        has_organization_projects: org.has_organization_projects_enabled?,
        has_repository_projects: org.has_repository_projects_enabled?,
        public_repos: org.repositories.total_count,
        public_gists: org.gists.total_count,
        followers: 0,
        following: 0,
        html_url: "#{GitHub.url}#{org.resource_path}",
        created_at: time(org.created_at),
        updated_at: time(org.updated_at),
        type: "Organization"
    end

    hash
  end

  OrganizationInteractionAbilityFragment = Api::App::PlatformClient.parse <<-'GRAPHQL'
    fragment on Organization {
      adminInfo {
        interactionAbility {
          limit
          origin
          expiresAt
        }
      }
    }
  GRAPHQL

  def graphql_organization_interaction_ability_hash(org, options = {})
    org = OrganizationInteractionAbilityFragment.new(org)

    interaction = org.admin_info.interaction_ability

    return {} if interaction.limit == "NO_LIMIT"

    {
      limit: interaction.limit.downcase,
      origin: interaction.origin.downcase,
      expires_at: time(interaction.expires_at),
    }
  end
end
