# rubocop:disable Style/FrozenStringLiteralComment

module Api::Serializer::TeamRepositoryDependency
  # Creates a hash of the repo and which permissions the specified team has for
  # it, to be serialized to JSON.
  #
  # repo    - Team Repository instance.
  # options - Hash
  #           :team - a Team Instance
  #
  # Returns a Hash with the repository attributes and a :permissions Hash with
  # :admin, :push, and :pull keys and boolean values.
  # If the repo does not exist, it returns nil
  #
  def team_repo_hash(repo, options = {})
    return nil if !repo
    options = Api::SerializerOptions.from(options)

    # Fix for #37741. These attributes require a routed repo, but since
    # this serializer method is used in lists, we can't just bail on the
    # whole repo.
    default_branch = "master"

    begin
      default_branch = repo.default_branch
    rescue GitRPC::RepositoryOffline => boom
      Failbot.push app: "github-unrouted"
      Failbot.report boom
    end

    return nil unless hash = simple_repository_hash(repo, options)

    hash.update \
      created_at: time(repo.created_at),
      updated_at: time(repo.updated_at),
      pushed_at: time(repo.pushed_at),
      git_url: repo.gitweb_url,
      ssh_url: repo.ssh_url,
      clone_url: repo.clone_url,
      svn_url: repo.svn_url,
      homepage: repo.homepage,
      size: repo.disk_usage.to_i,
      stargazers_count: repo.stargazer_count,
      watchers_count: repo.stargazer_count,
      language: repo.primary_language_name,
      has_issues: repo.has_issues?,
      has_projects: repo.repository_projects_enabled?,
      has_downloads: repo.has_downloads?,
      has_wiki: repo.has_wiki?,
      has_pages: repo.page.present?,
      forks_count: repo.forks_count,
      mirror_url: repo.mirror ? repo.mirror.url : nil,
      archived: repo.archived?,
      disabled: repo.disabled?,
      open_issues_count: repo.open_issues_count,
      license: license_hash(repo.license)

    if options.accepts_version?(:"mercy-preview")
      hash[:topics] = repo.topic_names
    end

    if options.accepts_version?("extended-search-results") && options[:license].nil?
      hash[:license] = license_hash(repo.license)
    end

    # DEPRECATED: These attributes will be removed in API v4.
    hash.update \
      forks: hash[:forks_count],
      open_issues: hash[:open_issues_count],
      watchers: hash[:watchers_count]

    hash[:default_branch] = default_branch

    # DEPRECATED: Will be removed in API v4. Use default_branch instead.
    hash[:master_branch] = default_branch if options.wants_beta_media_type?

    if options[:current_user].present?
      hash[:permissions] = permissions_hash(repo, user: options[:current_user])
    end

    if options[:generate_temp_clone_token]
      hash[:temp_clone_token] = repo.temp_clone_token(options[:current_user])
    end

    if options[:show_merge_settings]
      hash[:allow_squash_merge] = repo.squash_merge_allowed?
      hash[:allow_merge_commit] = repo.merge_commit_allowed?
      hash[:allow_rebase_merge] = repo.rebase_merge_allowed?
      hash[:delete_branch_on_merge] = repo.delete_branch_on_merge?
    end

    if GitHub.anonymous_git_access_enabled?
      hash[:anonymous_access_enabled] = repo.anonymous_git_access_enabled?
    end

    hash[:permissions] = build_permissions(
      permission: options.team.async_most_capable_action_or_role_for(repo).sync&.upcase
    )
    hash
  end

  def graphql_team_repo_hash(repo, options = {})
    return nil unless repo

    team_repo_edge = TeamRepoEdgeFragment.new(repo)
    repo = TeamRepoFragment.new(team_repo_edge.node)

    options = Api::SerializerOptions.from(content_options(options))

    hash = graphql_simple_repository_hash(repo, options)
    return if hash.nil?

    hash.update \
      created_at: time(repo.created_at),
      updated_at: time(repo.updated_at),
      pushed_at: time(repo.pushed_at),
      git_url: repo.git_url.to_s,
      ssh_url: repo.ssh_url.to_s,
      clone_url: repo.clone_url.to_s,
      svn_url: repo.svn_url.to_s,
      homepage: repo.homepage_url&.to_s,
      size: repo.disk_usage,
      stargazers_count: repo.stargazers.total_count,
      watchers_count: repo.stargazers.total_count,
      language: repo.primary_language&.name,
      has_issues: repo.has_issues_enabled?,
      has_projects: repo.has_projects_enabled?,
      has_downloads: repo.has_downloads?,
      has_wiki: repo.has_wiki_enabled?,
      has_pages: repo.has_pages?,
      forks_count: repo.forks.total_count,
      mirror_url: repo.mirror_url&.to_s,
      archived: repo.is_archived?,
      disabled: repo.is_disabled?,
      open_issues_count: repo.issues.total_count + repo.pull_requests.total_count,
      license: graphql_simple_license_hash(repo.license_info)

    hash.update \
      forks: hash[:forks_count],
      open_issues: hash[:open_issues_count],
      watchers: hash[:watchers_count]

    hash[:default_branch] = repo.default_branch

    if options[:current_user]
      hash[:permissions] = graphql_permissions_hash(repo, options)
    end

    hash[:permissions] = build_permissions(
      permission: team_repo_edge.permission
    )

    hash
  end

  # fragment for Team repositories
  # excludes some fields in normal Repository
  # does not include:
  # - code_of_conduct
  # - tempCloneToken
  # - hasAnonymousAccessEnabled
  TeamRepoFragment = Api::App::PlatformClient.parse <<-'GRAPHQL'
    fragment on Repository {
      ...Api::Serializer::RepositoriesDependency::SimpleRepositoryFragment

      createdAt
      updatedAt
      pushedAt

      homepageUrl # homepage
      primaryLanguage {
        name
      } # language
      diskUsage # size
      defaultBranch

      hasIssuesEnabled # has_issues
      hasProjectsEnabled # has_projects
      hasWikiEnabled # has_wiki
      hasDownloads # has_downloads
      hasPages # has_pages

      isArchived
      isDisabled

      issues(states: OPEN) {
        totalCount
      } # open_issues_count (deprecated: open_issues)
      pullRequests(states: OPEN) {
        totalCount
      } # open_issues_count (deprecated: open_issues)
      stargazers {
        totalCount # stargazers_count (deprecated: watchers, watchers_count)
      }
      forks {
        totalCount # forks_count (deprecated: forks)
      }

      cloneUrl
      mirrorUrl
      gitUrl
      sshUrl
      svnUrl

      licenseInfo {
        ...Api::Serializer::LicensesDependency::SimpleLicenseFragment
      }

      ...Api::Serializer::RepositoriesDependency::RepositoryPermissionFragment
    }
  GRAPHQL

  TeamRepoEdgeFragment = Api::App::PlatformClient.parse <<-'GRAPHQL'
    fragment on TeamRepositoryEdge {
      permission
      node {
        ...Api::Serializer::TeamRepositoryDependency::TeamRepoFragment
      }
    }
  GRAPHQL

  # Internal: Given a team's permission level on a repo, return a hash of values enumerating which
  # permissions a team has for the repo in question.
  #
  # permission - The String representing the permission level of a team on a repo,
  # e.g. READ, WRITE, ADMIN
  #
  # Example
  #
  #   build_permissions(permissions: "ADMIN")
  #   # => { pull: true, push: true, admin: true }
  #
  # Returns a Hash of symbol keys and boolean values.
  private def build_permissions(permission:)
    {
      pull: %w(READ WRITE ADMIN).include?(permission),
      triage: %w(TRIAGE).include?(permission),
      push: %w(WRITE ADMIN).include?(permission),
      maintain: %w(MAINTAIN).include?(permission),
      admin: %w(ADMIN).include?(permission),
    }
  end

end
