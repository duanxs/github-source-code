# rubocop:disable Style/FrozenStringLiteralComment

module Api::Serializer::VerifiableDependency
  # Creates a hash of data about a commit or tag's signature.
  #
  # object  - A Commit or Tag instance.
  # options - Hash.
  #
  # Returns a Hash.
  def verification_hash(object, options = {})
    {
      verified: object.verified_signature?,
      reason: object.signature_verification_reason,
      signature: object.signature,
      payload: object.signing_payload,
    }
  end

  GitSignatureFragment = Api::App::PlatformClient.parse <<-'GRAPHQL'
    fragment on GitSignature {
      isValid
      payload
      signature
      state
    }
  GRAPHQL

  def graphql_verification_hash(object, options = {})
    signature = GitSignatureFragment.new(object)

    {
      verified: signature ? signature.is_valid? : false,
      reason:   signature ? signature.state.downcase : "unsigned",
      signature: signature&.signature,
      payload: signature&.payload,
    }
  end
end
