# frozen_string_literal: true

module Api::Serializer::CredentialAuthorizationsDependency
  def credential_authorizations_hash(credential_auth, options = {})
    credential_type_display_map = {
      "OauthAccess" => "personal access token",
      "PublicKey" => "SSH key",
    }

    hash = {
      login: credential_auth.actor.login,
      credential_id: credential_auth.id,
      credential_type: credential_type_display_map[credential_auth.credential_type],
      credential_authorized_at: time(credential_auth.created_at),
      credential_accessed_at: time(credential_auth.accessed_at),
    }

    if credential_auth.credential_type == "OauthAccess"
      # need these lonely operations here b/c these credential_auth's can become
      # orphaned so we cannot trust that the credential will be there.
      hash[:token_last_eight] = credential_auth&.credential&.token_last_eight
      hash[:scopes] = credential_auth&.credential&.scopes
    elsif credential_auth.credential_type == "PublicKey"
      hash[:fingerprint] = credential_auth.fingerprint
    end

    hash
  end
end
