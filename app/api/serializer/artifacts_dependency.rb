# frozen_string_literal: true

module Api::Serializer::ArtifactsDependency
  # Creates a hash to be serialized to JSON.
  #
  # artifact - Artifact instance.
  # options  - Hash
  #
  # Returns a Hash if the Artifact exists, or nil.
  def artifact_hash(artifact, options = {})
    options = Api::SerializerOptions.from(options)
    hash = simple_artifact_hash(artifact, options)
    return hash if hash.nil?

    # associations here

    hash
  end

  # Creates a hash from a Artifact to be serialized to JSON. A short representation
  # suitable for sub-resources.
  #
  # artifact - Artifact instance
  #
  # Returns a Hash if the Artifact exists, or nil
  def simple_artifact_hash(artifact, options = {})
    return nil unless artifact
    {
      id:            artifact.id,
      node_id:       artifact.global_relay_id,
      name:          artifact.name,
      size_in_bytes: artifact.size,
      url:           url(artifact_path(artifact)),
      archive_download_url:  url("#{artifact_path(artifact)}/zip"),
      expired:       artifact.expired?,
      created_at:    time(artifact.created_at),
      updated_at:    time(artifact.updated_at),
    }
  end

  def artifacts_hash(data, _ = {})
    artifacts = data.fetch(:artifacts, [])
    artifacts_hashes = artifacts.map do |artfifact|
      Api::Serializer.artifact_hash(artfifact)
    end
    {}.tap do |h|
      h[:total_count]  = data[:total_count]
      h[:artifacts] = artifacts_hashes
    end
  end

  private

  def artifact_path(artifact)
    "/repos/#{artifact.check_suite.repository.nwo}/actions/artifacts/#{artifact.id}"
  end
end
