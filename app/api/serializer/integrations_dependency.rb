# rubocop:disable Style/FrozenStringLiteralComment

module Api::Serializer::IntegrationsDependency
  def authentication_token_hash(authentication_token, options = {})
    return nil unless authentication_token

    options = Api::SerializerOptions.from(options)

    {}.tap do |hash|
      hash[:token]                = options[:token]
      hash[:expires_at]           = time(authentication_token.expires_at_timestamp)
      hash[:permissions]          = options[:permissions] unless options[:permissions].nil?
      hash[:repository_selection] = options[:repository_selection] unless options[:repository_selection].nil?
      hash[:single_file]          = options[:single_file] if single_file_permission?(options)

      repositories = Array(options.repositories)

      if repositories.any?
        repository_hashes = repositories.map do |repo|
          Api::Serializer.repository_hash(
            repo,
            { current_user: nil }.merge(content_options(options)),
          )
        end

        hash[:repositories] = repository_hashes
      end
    end
  end

  def lightweight_authentication_token_hash(authentication_token, options = {})
    return nil unless authentication_token

    options = Api::SerializerOptions.from(options)

    {
      token:      options[:token],
      expires_at: time(authentication_token.expires_at_timestamp),
    }
  end

  IntegrationFragment = Api::App::PlatformClient.parse <<-'GRAPHQL'
    fragment on App {
      id
      slug
      owner {
        ...Api::Serializer::UserDependency::SimpleUserFragment
      }
      name
      description
      url
      htmlUrl
      createdAt
      updatedAt
      defaultPermissions {
        resource
        access
      }
      defaultEvents
    }
  GRAPHQL

  def graphql_integration_hash(integration, options = {})
    integration = IntegrationFragment.new(integration)
    return nil unless integration

    # Take the array of AppPermission objects and translate them back into one JSON
    # object
    default_permissions = integration.default_permissions.each_with_object({}) do |permission, hash|
      hash[permission.resource] = permission.access.downcase
    end

    {
      id: model_id_for_global_id(integration.id),
      slug: integration.slug,
      node_id: integration.id,
      owner: graphql_simple_user_hash(integration.owner),
      name: integration.name,
      description: integration.description,
      external_url: integration.url.to_s,
      html_url: integration.html_url.to_s,
      created_at: time(integration.created_at),
      updated_at: time(integration.updated_at),
      permissions: default_permissions,
      events:      integration.default_events,
    }
  end

  def integration_hash(integration, options = {})
    return nil unless integration

    {}.tap do |h|
      h[:id] = integration.id
      h[:slug] = integration.slug
      h[:node_id] = integration.global_relay_id
      h[:owner] = simple_user_hash(integration.owner, content_options(options))
      h[:name] = integration.name
      h[:description] = integration.description
      h[:external_url] = integration.url
      h[:html_url] = "#{GitHub.url}/#{integration.bot.to_param}"
      h[:created_at] = time(integration.created_at)
      h[:updated_at] = time(integration.updated_at)

      # If we're providing the PEM, then we
      # want to see all of the important credentials.
      if options[:pem].present?
        h[:client_id]      = integration.key
        h[:client_secret]  = integration.secret
        h[:webhook_secret] = integration.hook.secret
        h[:pem]            = options[:pem]
      end

      h[:permissions] = integration.default_permissions
      h[:events]      = integration.default_events

      if options[:current_integration] == integration
        h[:installations_count] = integration.installations.count
      end
    end
  end

  # To be removed, in favor of installation_hash, around Nov 22 2017
  # See https://github.com/github/platform-integrations/issues/263 for more details.
  def integration_installation_hash(installation, options = {})
    options = Api::SerializerOptions.from(options)

    {}.tap do |h|
      h[:id] = installation.id

      h[:account]           = account(installation.target, content_options(options))
      h[:repository_selection] = installation.get_cached_repository_selection
      h[:access_tokens_url] = url("/app/installations/#{installation.id}/access_tokens", options)
      h[:repositories_url]  = url("/installation/repositories", options)

      h[:html_url] = installation_html_url(installation)
      h[:app_id] = installation.integration_id
      h[:target_id] = installation.target_id
      h[:target_type] = target_type(installation.target)
      h[:permissions] = installation.get_cached_permissions
      h[:events] = installation.events
      h[:created_at] = installation.created_at
      h[:updated_at] = installation.updated_at
      h[:single_file_name] = installation.single_file_name
    end
  end

  # The future replacement for integration_installation_hash, used
  # for the new InstallationRepositoriesPayload and InstallationPayload
  # which use *only* the new GitHub App naming.
  # See https://github.com/github/platform-integrations/issues/263 for more details.
  def installation_hash(installation, options = {})
    options = Api::SerializerOptions.from(options)

    {}.tap do |h|
      h[:id] = installation.id

      h[:account]           = account(installation.target, content_options(options))
      h[:repository_selection] = installation.get_cached_repository_selection
      h[:access_tokens_url] = url("/app/installations/#{installation.id}/access_tokens", options)
      h[:repositories_url]  = url("/installation/repositories", options)

      h[:html_url] = installation_html_url(installation)
      h[:app_id] = installation.integration_id
      h[:app_slug] = installation.integration.slug
      h[:target_id] = installation.target_id
      h[:target_type] = target_type(installation.target)
      h[:permissions] = installation.get_cached_permissions
      h[:events] = installation.events
      h[:created_at] = installation.created_at
      h[:updated_at] = installation.updated_at
      h[:single_file_name] = installation.single_file_name
      h[:suspended_by] = account(installation.suspended_by)
      h[:suspended_at] = time(installation.suspended_at)
    end
  end

  def scoped_installation_hash(installation, options = {})
    options = Api::SerializerOptions.from(options)

    {
      permissions:          installation.permissions,
      repository_selection: installation.repository_selection,
      single_file_name:     installation.single_file_name,
      repositories_url:     url("/user/repos", options),
      account:              account(installation.target, content_options(options)),
    }
  end

  def integration_installation_request_hash(integration_installation_request, options = {})
    options = Api::SerializerOptions.from(options)

    {
      id: integration_installation_request.id,
      node_id: integration_installation_request.global_relay_id,
      account: account(integration_installation_request.target, content_options(options)),
      requester: simple_user_hash(integration_installation_request.requester),
      created_at: time(integration_installation_request.created_at),
    }
  end

  private

  def account(target, options = {})
    case target
    when Business
      business_hash(target, options)
    when User # works for orgs and users
      user_hash(target, options)
    end
  end

  def installation_html_url(installation)
    case installation.target
    when Business
      "#{GitHub.url}/businesses/#{installation.target}/settings/installations/#{installation.id}"
    when Organization
      "#{GitHub.url}/organizations/#{installation.target}/settings/installations/#{installation.id}"
    when User
      "#{GitHub.url}/settings/installations/#{installation.id}"
    end
  end

  def target_type(target)
    return "Business" if target.is_a?(Business)
    target.organization? ? "Organization" : "User"
  end

  def single_file_permission?(options)
    options[:permissions].present? && options[:permissions].key?("single_file")
  end
end
