# frozen_string_literal: true

module Api::Serializer::CodespacesDependency
  def codespace_with_env_hash(data, options = {})
    codespace = data[:codespace]
    env = data[:environment]
    codespace_hash(codespace, options).merge({
      environment: env,
    })
  end

  def codespace_hash(codespace, options = {})
    {
      name: codespace.name,
      guid: codespace.guid,
      state: codespace.state,
      url: url("/vscs_internal/user/#{codespace.owner.login}/codespaces/#{codespace.name}"),
      token_url: url("/vscs_internal/user/#{codespace.owner.login}/codespaces/#{codespace.name}/token"),
    }
  end

  def codespaces_hash(data, options = {})
    codespaces = data[:codespaces]
    total_count = data[:total_count]
    hashed_codespaces = codespaces.map { |w| codespace_hash(w, options) }
    {
      # Return both workspaces and codespaces to maintain backwards compatibility with API clients
      # TODO: Remove 'workspaces' from response body once the calls in VSCS have been updated
      workspaces: hashed_codespaces,
      codespaces: hashed_codespaces,
      total_count: total_count,
    }
  end
end
