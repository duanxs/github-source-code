# frozen_string_literal: true

# These serializers are used only internally.
module Api::Serializer::GhvfsDependency
  def ghvfs_replicas_hash(replicas, options = {})
    {
      replicas: replicas.map { |r| ghvfs_replica_hash(r, options) },
    }
  end

  def ghvfs_replica_hash(replica, options = {})
    return nil unless replica
    {
      id: replica.id,
      fileserver: ghvfs_fileserver_hash(replica.ghvfs_fileserver, options),
      created_at: replica.created_at,
      updated_at: replica.updated_at,
    }
  end

  def ghvfs_fileserver_hash(fileserver, options = {})
    return nil unless fileserver
    {
      id: fileserver.id,
      host: fileserver.host,
      ip: fileserver.ip,
      online: fileserver.online,
      embargoed: fileserver.embargoed,
      evacuating: fileserver.evacuating,
      datacenter: fileserver.datacenter,
    }
  end
end
