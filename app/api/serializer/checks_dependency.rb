# frozen_string_literal: true

module Api::Serializer::ChecksDependency
  CheckSuiteFragment = Api::App::PlatformClient.parse <<-'GRAPHQL'
    fragment on CheckSuite {
      id
      databaseId
      branch {
        name
      }
      commit {
        sha: oid
        ...Api::Serializer::CommitsDependency::CommitFragment
      }
      status
      conclusion
      repository {
        nameWithOwner
        ...Api::Serializer::RepositoriesDependency::SimpleRepositoryFragment
      }
      push {
        previousSha
        nextSha
      }
      app {
        ...Api::Serializer::IntegrationsDependency::IntegrationFragment
      }
      matchingPullRequests(first: 100, apiSerializerRequest: true) {
        nodes {
          ...Api::Serializer::PullsDependency::MinimalPullRequestFragment
        }
      }
      latest_check_runs: checkRuns(filterBy: { checkType: LATEST }) {
        totalCount
      }
      createdAt
      updatedAt
    }
  GRAPHQL

  CheckRunFragment = Api::App::PlatformClient.parse <<-'GRAPHQL'
    fragment on CheckRun {
      id
      databaseId
      url
      repository {
        nameWithOwner
      }
      externalId
      permalink
      detailsUrl
      status
      conclusion
      startedAt
      completedAt
      name
      title
      summary
      text
      annotations {
        totalCount
      }
      checkSuite {
        databaseId
        commit {
          oid
        }
        app {
          ...Api::Serializer::IntegrationsDependency::IntegrationFragment
        }
        matchingPullRequests(first: 100, apiSerializerRequest: true) {
          nodes {
            ...Api::Serializer::PullsDependency::MinimalPullRequestFragment
          }
        }
      }
    }
  GRAPHQL

  CheckAnnotationFragment = Api::App::PlatformClient.parse <<-'GRAPHQL'
    fragment on CheckAnnotation {
      path
      blobUrl
      location {
        start {
          line
          column
        }
        end {
          line
          column
        }
      }
      annotationLevel
      title
      message
      rawDetails
    }
  GRAPHQL

  # Creates a Hash to be serialized to JSON.
  #
  # check_suite - CheckSuite instance.
  # options     -  Hash
  #
  # Returns a Hash if the CheckSuite exists, or nil.
  def check_suite_hash(check_suite, options = {})
    options = Api::SerializerOptions.from(options)
    hash = simple_check_suite_hash(check_suite, options)
    return hash if hash.nil?

    hash[:latest_check_runs_count] = check_suite.latest_check_runs_count
    hash[:check_runs_url]   = url("/repos/#{check_suite.repository.nwo}/check-suites/#{check_suite.id}/check-runs")
    hash[:head_commit]      = simple_commit_hash(check_suite.repository.commits.find(check_suite.head_sha))
    hash[:repository]       = simple_repository_hash(check_suite.repository)
    hash
  end

  def graphql_check_suite_hash(check_suite, options = {})
    options = Api::SerializerOptions.from(options)
    hash = graphql_simple_check_suite_hash(check_suite, options)
    return hash if hash.nil?

    check_suite = CheckSuiteFragment.new(check_suite)

    hash[:latest_check_runs_count] = check_suite.latest_check_runs.total_count
    hash[:check_runs_url]   = url("/repos/#{check_suite.repository.name_with_owner}/check-suites/#{check_suite.database_id}/check-runs")
    hash[:head_commit]      = graphql_simple_commit_hash(check_suite.commit)
    hash[:repository]       = graphql_simple_repository_hash(check_suite.repository)
    hash
  end

  # Creates a hash from a CheckSuite to be serialized to JSON. A short representation
  # suitable for sub-resources.
  #
  # check_suite - CheckSuite instance
  #
  # Returns a Hash if the CheckSuite exists, or nil
  def simple_check_suite_hash(check_suite, options = {})
    return nil unless check_suite
    {
      id:            check_suite.id,
      node_id:       check_suite.global_relay_id,
      head_branch:   check_suite.head_branch,
      head_sha:      check_suite.head_sha,
      status:        check_suite.status,
      conclusion:    check_suite.conclusion,
      url:           url("/repos/#{check_suite.repository.nwo}/check-suites/#{check_suite.id}"),
      before:        check_suite.push&.before,
      after:         check_suite.push&.after,
      pull_requests: related_pull_requests(check_suite, options),
      app:           integration_hash(check_suite.github_app),
      created_at:    time(check_suite.created_at),
      updated_at:    time(check_suite.updated_at),
    }
  end

  def graphql_simple_check_suite_hash(check_suite, options = {})
    return nil unless check_suite

    check_suite = CheckSuiteFragment.new(check_suite)

    {
      id:            check_suite.database_id,
      node_id:       check_suite.id,
      head_branch:   check_suite.branch&.name,
      head_sha:      check_suite.commit.sha,
      status:        check_suite.status.downcase,
      conclusion:    check_suite.conclusion&.downcase,
      url:           url("/repos/#{check_suite.repository.name_with_owner}/check-suites/#{check_suite.database_id}"),
      before:        check_suite.push&.previous_sha,
      after:         check_suite.push&.next_sha,
      pull_requests: graphql_related_pull_requests(check_suite, options),
      app:           graphql_integration_hash(check_suite.app),
      created_at:    time(check_suite.created_at),
      updated_at:    time(check_suite.updated_at),
    }
  end

  def check_suites_hash(data, _ = {})
    check_suites = data.fetch(:check_suites, [])
    check_suite_hashes = check_suites.map do |suite|
      Api::Serializer.check_suite_hash(suite)
    end

    {}.tap do |h|
      h[:total_count]  = data[:total_count]
      h[:check_suites] = check_suite_hashes
    end
  end

  def graphql_check_suites_hash(data, _ = {})
    check_suites = data.fetch(:check_suites, [])
    check_suite_hashes = check_suites.map do |suite|
      graphql_check_suite_hash(suite)
    end

    {}.tap do |h|
      h[:total_count]  = data[:total_count]
      h[:check_suites] = check_suite_hashes
    end
  end

  # Creates a Hash to be serialized to JSON.
  #
  # check_run - CheckRun instance.
  #
  # Returns a Hash if the CheckRun exists, or nil.
  def check_run_hash(run, options = {})
    hash = simple_check_run_hash(run)
    return hash if hash.nil?

    hash[:output]        = check_output_fields(run)
    hash[:name]          = run.visible_name
    hash[:check_suite]   = {id: run.check_suite_id}
    hash[:app]           = integration_hash(run.github_app)
    hash[:pull_requests] = related_pull_requests(run.check_suite, options)
    hash
  end

  # Creates a Hash to be serialized to JSON.
  #
  # check_run - CheckRun instance.
  #
  # Returns a Hash if the CheckRun exists, or nil.
  def simple_check_run_hash(run, _ = {})
    return nil unless run

    {}.tap do |h|
      h[:id]           = run.id
      h[:node_id]      = run.global_relay_id
      h[:head_sha]     = run.head_sha
      h[:external_id]  = run.external_id.to_s
      h[:url]          = url("/repos/#{run.repository.name_with_owner}/check-runs/#{run.id}")
      h[:html_url]     = html_url("#{run.permalink}")
      h[:details_url]  = run.details_url
      h[:status]       = run.status
      h[:conclusion]   = run.conclusion
      h[:started_at]   = time(run.started_at)
      h[:completed_at] = time(run.completed_at)
    end
  end

  # Creates a Hash to be serialized to JSON.
  #
  # check_runs - CheckRun active record relation or Array of CheckRun instances.
  #
  # Returns a Hash.
  def check_runs_hash(data, _ = {})
    check_runs = data.fetch(:check_runs, [])
    check_run_hashes = check_runs.map do |run|
      Api::Serializer.check_run_hash(run)
    end

    {}.tap do |h|
      h[:total_count] = data[:total_count]
      h[:check_runs]  = check_run_hashes
    end
  end

  def graphql_check_runs_hash(data, _ = {})
    check_runs = data[:check_runs]
    check_run_hashes = check_runs.map do |check_run|
      graphql_check_run_hash(check_run)
    end

    {}.tap do |h|
      h[:total_count] = data[:total_count]
      h[:check_runs]  = check_run_hashes
    end
  end

  def graphql_check_run_hash(check_run, options = {})
    check_run = CheckRunFragment.new(check_run)

    hash = {
      id: check_run.database_id,
      node_id: check_run.id,
      head_sha: check_run.check_suite.commit.oid,
      external_id: check_run.external_id.to_s,
      url:          url("/repos/#{check_run.repository.name_with_owner}/check-runs/#{check_run.database_id}"),
      html_url:     check_run.permalink.to_s,
      details_url:  check_run.details_url.to_s,
      status:       check_run.status.downcase,
      conclusion:   check_run.conclusion&.downcase,
      started_at:   time(check_run.started_at),
      completed_at: time(check_run.completed_at),
      output: {
        title:             check_run.title,
        summary:           check_run.summary,
        text:              check_run.text,
        annotations_count: check_run.annotations.total_count,
        annotations_url:   url("/repos/#{check_run.repository.name_with_owner}/check-runs/#{check_run.database_id}/annotations"),
      },
      name: check_run.name,
      check_suite: {
        id: check_run.check_suite.database_id,
      },
      app: graphql_integration_hash(check_run.check_suite.app),
      pull_requests: graphql_related_pull_requests(check_run.check_suite, options),
    }

    hash
  end

  # Creates a Hash to be serialized to JSON.
  #
  # repo - A Repository instance.
  #
  # Returns a Hash.
  def check_suite_preferences_hash(repo, _ = {})
    data = repo.check_suite_preferences

    hash = {}
    hash[:preferences]         = {}
    hash[:repository]          = simple_repository_hash(repo)

    hash[:preferences][:auto_trigger_checks] = []
    data[:auto_trigger_checks].each do |h|
      hash[:preferences][:auto_trigger_checks] << {
        app_id: h[:app][:id], setting: h[:setting]
      }
    end

    hash
  end

  # Creates a Hash to be serialized to JSON.
  #
  # check_run - CheckRun instance.
  #
  # Returns a Hash.
  def check_output_fields(check_run, _ = {})
    {}.tap do |h|
      h[:title]             = check_run.title
      h[:summary]           = check_run.summary
      h[:text]              = check_run.text
      h[:annotations_count] = check_run.annotations.count
      h[:annotations_url]   = url("/repos/#{check_run.repository.name_with_owner}/check-runs/#{check_run.id}/annotations")
    end
  end

  # Creates a Hash to be serialized to JSON.
  #
  # annotation - CheckAnnotation instance.
  #
  # Returns a Hash.
  def check_annotation_hash(annotation, _ = {})
    {}.tap do |h|
      h[:path] = annotation.path
      h[:blob_href] = annotation.blob_href
      h[:start_line] = annotation.start_line
      h[:start_column] = annotation.start_column
      h[:end_line] = annotation.end_line
      h[:end_column] = annotation.end_column
      h[:annotation_level] = annotation.annotation_level
      h[:title] = annotation.title
      h[:message] = annotation.message
      h[:raw_details] = annotation.raw_details
    end
  end

  def graphql_check_annotations_hash(data, _ = {})
    check_annotations = data.fetch(:check_annotations, [])
    check_annotation_hashes = check_annotations.map do |annotation|
      graphql_check_annotation_hash(annotation)
    end

    check_annotation_hashes
  end

  def graphql_check_annotation_hash(check_annotation, _ = {})
    check_annotation = CheckAnnotationFragment.new(check_annotation)

    {
      path: check_annotation.path,
      blob_href: check_annotation.blob_url.to_s,
      start_line: check_annotation.location.start.line,
      start_column: check_annotation.location.start.column,
      end_line: check_annotation.location.end.line,
      end_column: check_annotation.location.end.column,
      annotation_level: check_annotation.annotation_level.downcase,
      title: check_annotation.title,
      message: check_annotation.message,
      raw_details: check_annotation.raw_details,
    }
  end

  def related_pull_requests(check_suite, options)
    check_suite.matching_pull_requests(options[:current_user])&.map do |pull_request|
      minimal_pull_request_hash(pull_request)
    end
  end

  def graphql_related_pull_requests(check_suite, options = {})
    check_suite.matching_pull_requests.nodes.map do |pull|
      graphql_minimal_pull_request_hash(pull)
    end
  end
end
