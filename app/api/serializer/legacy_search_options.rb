# frozen_string_literal: true

module Api::Serializer
  class LegacySearchOptions < GitHub::Options.new(:search_hit, :score, :accept_mime_types)
    include Api::SerializerOptions::MimeTypes

    def search_hit
      self[:search_hit] ||= {}
    end

    def score
      @score ||= self[:score].to_f
    end
  end
end
