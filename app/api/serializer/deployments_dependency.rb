# rubocop:disable Style/FrozenStringLiteralComment

module Api::Serializer::DeploymentsDependency
  # Creates a Hash to be serialized to JSON.
  #
  # deployment - Deployment instance.
  # options    - Hash
  #
  # Returns a Hash if the Deployment exists, or nil.
  def deployment_hash(deployment, options = {})
    return nil unless deployment

    repository_url = "/repos/#{deployment.repository.name_with_owner}"
    deployment_url = "#{repository_url}/deployments/#{deployment.id}"

    hash = {
      url: url(deployment_url),
      id: deployment.id,
      node_id: deployment.global_relay_id,
      sha: deployment.sha,
      ref: deployment.ref,
      task: deployment.task,
      payload: deployment.json_payload,
      original_environment: deployment.environment,
      environment: deployment.latest_environment,
      description: deployment.description,
      creator: user_hash(deployment.creator, content_options(options)),
      created_at: time(deployment.created_at),
      updated_at: time(deployment.updated_at),
      statuses_url: url("#{deployment_url}/statuses"),
      repository_url: url(repository_url),
    }

    options = Api::SerializerOptions.from(options)
    if options.accepts_preview?(:deployment_enhancements)
      preview_attributes = {
        transient_environment: deployment.transient_environment?,
        production_environment: deployment.production_environment?,
      }

      hash.merge!(preview_attributes)
    end

    hash[:performed_via_github_app] = integration_hash(deployment.performed_via_integration)

    hash
  end
end
