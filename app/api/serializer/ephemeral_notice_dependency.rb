# frozen_string_literal: true

module Api::Serializer::EphemeralNoticeDependency
  def ephemeral_notice_hash(ephemeral_notice, options = {})
    parent = case ephemeral_notice.parent
    when InteractiveComponent
      interactive_component_hash(ephemeral_notice.parent)
    end

    {
      id: ephemeral_notice.id,
      notice_text: ephemeral_notice.notice_text,
      link_to: ephemeral_notice.link_to,
      link_title: ephemeral_notice.link_title,
      parent_type: ephemeral_notice.parent_type,
      parent: parent,
      created_at: ephemeral_notice.created_at,
      updated_at: ephemeral_notice.updated_at,
      user: simple_user_hash(ephemeral_notice.user),
    }
  end
end
