# frozen_string_literal: true

module Api::Serializer
  class UserTextMatch
    include TextMatch

    SUPPORTED_FIELD_NAMES = %w(login login.ngram email email.plain name).freeze

    def object_type
      "User"
    end

    def object_url_suffix
      "/users/#{user.login}"
    end

    def property
      case field_name
      when "login", "login.ngram" then "login"
      when "email", "email.plain" then "email"
      when "name"                 then "name"
      end
    end

    private

    def user
      search_result["_model"]
    end
  end
end
