# frozen_string_literal: true

module Api::Serializer::JobsDependency
  # Creates a hash to be serialized to JSON.
  #
  # job      - Job (CheckRun) instance.
  # options  - Hash
  #
  # Returns a Hash if the job exists, or nil.
  def job_hash(job, options = {})
    options = Api::SerializerOptions.from(options)
    hash = simple_job_hash(job, options)
    return hash if hash.nil?

    hash[:name]          = job.visible_name
    hash[:steps]         = job.steps.map do |step|
      {
        name:         step.name,
        status:       step.status,
        conclusion:   step.conclusion,
        number:       step.number,
        started_at:   step.started_at,
        completed_at: step.completed_at,
      }
    end
    hash[:check_run_url] = url("/repos/#{job.repository.name_with_owner}/check-runs/#{job.id}")

    hash
  end

  # Creates a hash from a job (CheckRun) to be serialized to JSON. A short representation
  # suitable for sub-resources.
  #
  # job - CheckRun instance
  #
  # Returns a Hash if the CheckRun exists, or nil
  def simple_job_hash(job, options = {})
    return nil unless job

    workflow_run = job.check_suite.workflow_run

    {}.tap do |h|
      h[:id]           = job.id
      h[:run_id]       = workflow_run.id
      h[:run_url]      = url(workflow_run_path(workflow_run))
      h[:node_id]      = job.global_relay_id
      h[:head_sha]     = job.head_sha
      h[:url]          = url("/repos/#{job.repository.name_with_owner}/actions/jobs/#{job.id}")
      h[:html_url]     = html_url("#{job.permalink}")
      h[:status]       = job.status
      h[:conclusion]   = job.conclusion
      h[:started_at]   = time(job.started_at)
      h[:completed_at] = time(job.completed_at)
    end
  end

  # Creates a Hash to be serialized to JSON.
  #
  # jobs - CheckRun active record relation or Array of CheckRun instances.
  #
  # Returns a Hash.
  def jobs_hash(data, _ = {})
    jobs = data.fetch(:jobs, [])
    job_hashes = jobs.map do |run|
      Api::Serializer.job_hash(run)
    end

    {}.tap do |h|
      h[:total_count] = data[:total_count]
      h[:jobs] = job_hashes
    end
  end

end
