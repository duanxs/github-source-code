# rubocop:disable Style/FrozenStringLiteralComment

module Api::Serializer::MigrationsDependency
  # Creates a Hash to be serialized to JSON.
  def migration_hash(migration, options = {})
    options = Api::SerializerOptions.from(options)
    repos   = migration.repositories
    path    = if migration.owner.organization?
                "/orgs/#{migration.owner}/migrations/#{migration.id}"
              else
                "/user/migrations/#{migration.id}"
    end

    hash = {
      id: migration.id,
      node_id: migration.global_relay_id,
      owner: user_hash(migration.owner, content_options(options)),
      guid: migration.guid,
      state: migration.current_state.name,
      lock_repositories: migration.lock_repositories,
      exclude_attachments: migration.exclude_attachments,
      repositories: [],
      url: url(path, options),
    }

    unless coerce_array(options[:exclude]).include?("repositories")
      hash[:repositories] = repos.map { |repo| repository_hash(repo, options) }
    end

    if migration.exported? && migration.file.present?
      hash[:archive_url] = url("#{path}/archive", options)
    end

    hash[:created_at] = migration.created_at
    hash[:updated_at] = migration.updated_at

    hash
  end

  private

  # Internal: coerces a comma delimited string, nil, or array into an array
  def coerce_array(obj)
    if obj.is_a?(String)
      obj.split(",")
    else
      Array.wrap(obj)
    end
  end
end
