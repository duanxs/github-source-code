# frozen_string_literal: true

module Api::Serializer::CodeScanningDependency
  def code_scanning_alerts_hash(data, options = {})
    alerts = data.fetch(:alerts, [])
    options[:resolvers] = User.where(id: alerts.map(&:resolver_id).select(&:nonzero?).uniq).index_by(&:id)

    alerts.map do |alert|
      Api::Serializer.code_scanning_alert_hash(alert, options, false)
    end
  end

  # TODO: delete this once the code_scanning_integration flag is removed
  def code_scanning_alerts_legacy_hash(data, options = {})
    alerts = data.fetch(:alerts, [])
    options[:resolvers] = User.where(id: alerts.map(&:resolver_id).select(&:nonzero?).uniq).index_by(&:id)

    alerts.map do |alert|
      Api::Serializer.code_scanning_alert_legacy_hash(alert, options, false)
    end
  end

  def code_scanning_alert_base_hash(data, options = {}, include_instances = true)
    return nil unless data

    repo = options[:repo]

    hash = {
      number: data.number,
      created_at: time(data.created_at),
      url: url("/repos/#{repo.name_with_owner}/code-scanning/alerts/#{data.number}"),
      html_url: html_url("/#{repo.name_with_owner}/security/code-scanning/#{data.number}"),
    }

    if include_instances
      hash[:instances] = data.instances.map do |i|
        ihash = {
          ref: i.ref_name,
          analysis_key: i.analysis_key&.analysis_key,
          environment: i.analysis_key&.environment,
        }

        if i.is_fixed
          ihash["state"] = "fixed"
        elsif data.resolution != :NO_RESOLUTION
          ihash["state"] = "dismissed"
        else
          ihash["state"] = "open"
        end
        ihash
      end
    end

    hash
  end

  def tool(data)
    return unless data.present?
    {
      name: data.name,
      version: nil, # TODO: This is currently not being sent by Turboscan
    }
  end

  def code_scanning_alert_hash(data, options = {}, include_instances = true)
    return nil unless data

    hash = code_scanning_alert_base_hash(data, options, include_instances)

    resolver = options[:resolvers]&.key?(data.resolver_id) ? options[:resolvers][data.resolver_id] : User.find_by_id(data.resolver_id)
    fixed = !data.instances.empty? && data.instances.all? { |i| i.is_fixed }

    hash.merge(
      {
        state: fixed ? "fixed" : (data.resolution != :NO_RESOLUTION ? "dismissed" : "open"),
        dismissed_by: user_hash(resolver, content_options(options)),
        dismissed_at: time(data.resolved_at),
        dismissed_reason: GitHub::Turboscan.api_resolution_reason(data.resolution),
        rule: {
          id: data.rule_id,
          severity: data.rule_severity.to_s.downcase,
          description: data.rule_short_description,
        },
        tool: tool(data.tool),
      }
    )
  end

  # TODO: delete this once the code_scanning_integration flag is removed
  def code_scanning_alert_legacy_hash(data, options = {}, include_instances = true)
    return nil unless data

    hash = code_scanning_alert_base_hash(data, options, include_instances)
    if hash[:instances].present?
      hash[:instances] = hash[:instances].map { |i| i.merge({ matrix_vars: i[:environment] }) }
    end

    resolver = options[:resolvers]&.key?(data.resolver_id) ? options[:resolvers][data.resolver_id] : User.find_by_id(data.resolver_id)
    tool = CodeScanning::Tool.from_name(data.tool.name)&.display_name if data.tool&.present?

    hash.merge(
      {
        closed_by: user_hash(resolver, content_options(options)),
        closed_at: time(data.resolved_at),
        closed_reason: GitHub::Turboscan.api_resolution_reason(data.resolution),
        open: data.resolution == :NO_RESOLUTION,
        rule_id: data.rule_id,
        rule_severity: data.rule_severity.to_s.downcase,
        rule_description: data.rule_short_description,
        tool: tool
      }
    )
  end

  def code_scanning_analyses_hash(data, options = {})
    analyses = data.fetch(:analyses, [])
    analyses.map do |a|
      Api::Serializer.code_scanning_analysis_hash(a, options)
    end
  end

  def code_scanning_analysis_hash(data, options = {})
    return nil unless data
    # Returns an analysis object.
    # Notes:
    # - We currently omit the AnalysisID, because no other endpoint can make use of this
    # - The Tool field should be an object with (at least) name and version. Ideally, we
    #   could encompass more of the SARIF specification (Driver Sec. 3.18.2)
    hash = {
      ref: data.ref_name,
      commit_sha: data.commit_oid,
      analysis_key: data.analysis_key,
      tool_name: data.tool,
      environment: data.environment,
      error: data.errors, # Using "error" as this is a single string.
      created_at: time(data.created_at),
    }
    hash
  end
end
