# rubocop:disable Style/FrozenStringLiteralComment

# Core Sinatra controller for all API requests.
class Api::Root < Api::App
  map_to_service :rest_api

  get "/" do
    @route_owner = "@github/ecosystem-api"
    @documentation_url = "/rest/overview/resources-in-the-rest-api#root-endpoint"
    control_access :public_site_information,
      resource: Platform::PublicResource.new,
      allow_integrations: true,
      allow_user_via_integration: true

    hash = {
      current_user_url: api_url("/user"),
      current_user_authorizations_html_url: html_url("/settings/connections/applications{/client_id}"),
      authorizations_url: api_url("/authorizations"),
      code_search_url: api_url("/search/code?q={query}{&page,per_page,sort,order}"),
      commit_search_url: api_url("/search/commits?q={query}{&page,per_page,sort,order}"),
      emails_url: api_url("/user/emails"),
      emojis_url: api_url("/emojis"),
      events_url: api_url("/events"),
      feeds_url: api_url("/feeds"),
      followers_url: api_url("/user/followers"),
      following_url: api_url("/user/following{/target}"),
      gists_url: api_url("/gists{/gist_id}"),
      hub_url: api_url("/hub"),
      issue_search_url: api_url("/search/issues?q={query}{&page,per_page,sort,order}"),
      issues_url: api_url("/issues"),
      keys_url: api_url("/user/keys"),
      label_search_url: api_url("/search/labels?q={query}&repository_id={repository_id}" \
                                "{&page,per_page}"),
    }

    hash = hash.merge(
      notifications_url: api_url("/notifications"),
      organization_url: api_url("/orgs/{org}"),
      organization_repositories_url: api_url("/orgs/{org}/repos{?type,page,per_page,sort}"),
      organization_teams_url: api_url("/orgs/{org}/teams"),
      public_gists_url: api_url("/gists/public"),
      rate_limit_url: api_url("/rate_limit"),
      repository_url: api_url("/repos/{owner}/{repo}"),
      repository_search_url: api_url("/search/repositories?q={query}{&page,per_page,sort,order}"),
      current_user_repositories_url: api_url("/user/repos{?type,page,per_page,sort}"),
      starred_url: api_url("/user/starred{/owner}{/repo}"),
      starred_gists_url: api_url("/gists/starred"),
    )

    if medias.api_preview?(:repository_topics)
      hash[:topic_search_url] = api_url("/search/topics?q={query}{&page,per_page}")
    end

    hash = hash.merge(
      user_url: api_url("/users/{user}"),
      user_organizations_url: api_url("/user/orgs"),
      user_repositories_url: api_url("/users/{user}/repos{?type,page,per_page,sort}"),
      user_search_url: api_url("/search/users?q={query}{&page,per_page,sort,order}"),
    )

    deliver_raw hash
  end

  get "/emojis" do
    @route_owner = "@github/ecosystem-api"
    @documentation_url = "/rest/reference/emojis#get-emojis"
    control_access :public_site_information,
      resource: Platform::PublicResource.new,
      allow_integrations: true,
      allow_user_via_integration: true

    url_prefix = GitHub.asset_host_url
    url_prefix = GitHub.url if url_prefix.blank?

    emojis = GitHub::Emoji.each_by_alias.each_with_object({}) do |(name, emoji), all|
      all[name] = "#{url_prefix}/images/icons/emoji/#{emoji.image_filename}?v8"
    end
    deliver_raw emojis
  end

  get "/rate_limit" do
    @route_owner = "@github/ecosystem-api"
    @documentation_url = "/rest/reference/rate-limit#get-rate-limit-status-for-the-authenticated-user"
    control_access :public_site_information,
      resource: Platform::PublicResource.new,
      allow_integrations: true,
      allow_user_via_integration: true

    skip_rate_limit!

    cache_control "no-cache"
    if GitHub.rate_limiting_enabled?
      deliver :rate_limit_statuses_hash, Api::RateLimitStatus.all(self)
    else
      deliver_error 404, message: "Rate limiting is not enabled."
    end
  end

  get "/boomtown" do
    @route_owner = "@github/ecosystem-api"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL
    control_access :staff_api, allow_integrations: false, allow_user_via_integration: false
    fail "BOOM (#{GitHub.host_name})"
  end

  get "/deprecated" do
    deprecation_date = Time.now

    deprecated(
      deprecation_date: deprecation_date,
      sunset_date: deprecation_date + 1.year,
      info_url: "#{GitHub.developer_help_url}/deprecation-1",
      alternate_path_url: "/not-deprecated",
    )

    @route_owner = "@github/ecosystem-api"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL
    control_access :staff_api, allow_integrations: false, allow_user_via_integration: false

    deliver_raw(string: "Test Deprecated Endpoint")
  end

  get "/sleeptown" do
    @route_owner = "@github/ecosystem-api"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL
    control_access :staff_api, allow_integrations: false, allow_user_via_integration: false
    duration = (params[:n] || 30).to_i
    sleep duration
    deliver_empty
  end
end
