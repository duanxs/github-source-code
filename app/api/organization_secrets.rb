# frozen_string_literal: true

require "grpc"
require "github-launch"
require "github/launch_client/credz"
require "github/launch_client"

class Api::OrganizationSecrets < Api::App
  include Api::App::EarthsmokeKeyHelper
  include Api::App::GrpcHelpers
  include GitHub::LaunchClient

  # For Local development, you need bin/server running and github/launch running (credz service)

  # Get public key for encrypting secrets
  get "/organizations/:organization_id/actions/secrets/public-key" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#get-an-organization-public-key"
    deliver_error! 404 unless org_secrets_enabled?

    org = find_org!
    deliver_error! 404 unless can_use_org_secrets?(org)

    control_access :read_org_actions_secrets,
      resource: org,
      user: current_user,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    public_key = if GitHub.enterprise?
      { key_identifier: 1, key: GitHub.actions_secrets_public_key }
    else
      generate_earthsmoke_key_payload(name: Platform::EarthsmokeKeys::CUSTOM_TASKS, scope: org.global_relay_id, count: 1).first
    end

    payload = { key_id: public_key[:key_identifier], key: public_key[:key] }

    deliver_raw(payload)
  end

  # Get the name and timestamps of all secrets set for the organization
  get "/organizations/:organization_id/actions/secrets" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#list-organization-secrets"
    deliver_error! 404 unless org_secrets_enabled?

    org = find_org!
    deliver_error! 404 unless can_use_org_secrets?(org)

    control_access :read_org_actions_secrets,
      resource: org,
      user: current_user,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    result = rescue_from_grpc_errors("Secrets") do
      Credz.list_credentials(app: GitHub.launch_github_app, owner: org, actor: current_user)
    end

    validate_listing!(result)

    # We map to an Array so pagination works
    secrets = paginate_rel(result.credentials.map { |cred| cred })

    deliver :actions_org_secrets_hash, { secrets: secrets, org: org, total_count: secrets.total_entries }
  end

  # Get a single secret
  get "/organizations/:organization_id/actions/secrets/:name" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#get-an-organization-secret"
    deliver_error! 404 unless org_secrets_enabled?

    org = find_org!
    deliver_error! 404 unless can_use_org_secrets?(org)

    control_access :read_org_actions_secrets,
      resource: org,
      user: current_user,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    result = rescue_from_grpc_errors("Secrets") do
      Credz.fetch_credential(app: GitHub.launch_github_app, owner: org, actor: current_user, key: params[:name])
    end

    deliver_error! 404 unless result

    deliver :actions_org_secret_hash, { secret: result.credential, org: org }
  end

  # Delete a secret
  delete "/organizations/:organization_id/actions/secrets/:name" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#delete-an-organization-secret"
    deliver_error! 404 unless org_secrets_enabled?

    org = find_org!
    deliver_error! 404 unless can_use_org_secrets?(org)

    control_access :write_org_actions_secrets,
      resource: org,
      user: current_user,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    receive_with_schema("organization-actions-secret", "delete-org-secret")

    result = rescue_from_grpc_errors("Secrets") do
      Credz.delete_credential(app: GitHub.launch_github_app, owner: org, actor: current_user, key: params[:name])
    end

    deliver_error! 404 unless result

    deliver_empty status: (result.success ? 204 : 404)
  end

   # Store a secret for an organization for a write user.
  put "/organizations/:organization_id/actions/secrets/:name" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#create-or-update-an-organization-secret"
    deliver_error! 404 unless org_secrets_enabled?

    org = find_org!
    deliver_error! 404 unless can_use_org_secrets?(org)

    control_access :write_org_actions_secrets,
      resource: org,
      user: current_user,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    data = receive_with_schema("organization-actions-secret", "set-org-secret")
    value = data["encrypted_value"]
    key_identifier = data["key_id"]
    visibility = GitHub::LaunchClient::Credz::FROM_VISIBILITY_MAP[data["visibility"]]
    selected_repository_ids = data["selected_repository_ids"]

    validation = Credz.validate_org_secret(params[:name], value, visibility)
    unless validation.succeeded?
      deliver_error! 422, message: validation.error, documentation_url: @documentation_url
    end

    value = if GitHub.enterprise?
      decrypt_enterprise_value(value)
    else
      pack_earthsmoke_value(key_identifier, value)
    end

    encoded_value = Base64.strict_encode64(value)

    # Filter repositories to org repositories
    org_repository_ids = org.repositories.where(id: selected_repository_ids).map(&:global_relay_id)

    # result is a GitHub::Launch::Services::Credz::StoreResponse
    result = rescue_from_grpc_errors("Secrets") do
      Credz.store_credential \
        app:   GitHub.launch_github_app, # we always store secrets in the prod launch env (even lab)
        owner: org,
        actor: current_user,
        key:   params[:name],
        value: encoded_value,
        visibility: visibility,
        selected_repositories: org_repository_ids
    end

    validate_result!(result)
    validate_storage!(result)

    status = 201
    if result.updated
      status = 204
    end

    deliver_empty(status: status)
  end

  # Get the repositories for a secret with `selected` visibility
  get "/organizations/:organization_id/actions/secrets/:name/repositories" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#list-selected-repositories-for-an-organization-secret"
    deliver_error! 404 unless org_secrets_enabled?

    org = find_org!
    deliver_error! 404 unless can_use_org_secrets?(org)

    control_access :read_org_actions_secrets,
      resource: org,
      user: current_user,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    # Fetch secret from credz
    result = rescue_from_grpc_errors("Secrets") do
      Credz.fetch_credential(app: GitHub.launch_github_app, owner: org, actor: current_user, key: params[:name])
    end
    deliver_error! 404 unless result

    total_count = result.credential.selected_repositories_count

    repository_ids = result.credential.selected_repositories.map { |repo| Platform::Helpers::NodeIdentification.from_global_id(repo.global_id)[1] }
    repositories = org.repositories.where(id: repository_ids)
    repositories = paginate_rel(repositories.sorted_by(:full_name, "asc"))

    deliver :actions_secret_repositories_hash, { repositories: repositories, total_count: total_count }
  end

  # Add a repository for a secret with `selected` visibility
  put "/organizations/:organization_id/actions/secrets/:name/repositories/:repository_id" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#add-selected-repository-to-an-organization-secret"
    deliver_error! 404 unless org_secrets_enabled?

    org = find_org!
    deliver_error! 404 unless can_use_org_secrets?(org)

    control_access :write_org_actions_secrets,
      resource: org,
      user: current_user,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    receive_with_schema("organization-actions-secret", "set-org-secret-repository")

    # Fetch secret from credz
    result = rescue_from_grpc_errors("Secrets") do
      Credz.fetch_credential(app: GitHub.launch_github_app, owner: org, actor: current_user, key: params[:name])
    end
    deliver_error! 404 unless result

    credential = result.credential
    unless credential.visibility == :VISIBILITY_SELECTED_REPOSITORIES
      deliver_error! 409, errors: "You cannot update selected repositories for a secret when the visibility is not set to 'selected'"
    end

    # Validate repository
    repo = find_repo!
    deliver_error! 422 unless repo.organization_id == org.id

    selected_repositories_ids = credential.selected_repositories.map(&:global_id).to_set
    selected_repositories_ids << repo.global_relay_id

    # result is a GitHub::Launch::Services::Credz::StoreResponse
    result = rescue_from_grpc_errors("Secrets") do
      Credz.update_credential \
        app:   GitHub.launch_github_app, # we always store secrets in the prod launch env (even lab)
        owner: org,
        actor: current_user,
        key:   credential.name,
        visibility: credential.visibility,
        selected_repositories: selected_repositories_ids.to_a
    end

    validate_result!(result)
    validate_storage!(result)

    deliver_empty(status: 204)
  end

  # Delete a repository for a secret with `selected` visibility
  delete "/organizations/:organization_id/actions/secrets/:name/repositories/:repository_id" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#remove-selected-repository-from-an-organization-secret"
    deliver_error! 404 unless org_secrets_enabled?

    org = find_org!
    deliver_error! 404 unless can_use_org_secrets?(org)

    control_access :write_org_actions_secrets,
      resource: org,
      user: current_user,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    receive_with_schema("organization-actions-secret", "delete-org-secret-repository")

    result = rescue_from_grpc_errors("Secrets") do
      Credz.fetch_credential(app: GitHub.launch_github_app, owner: org, actor: current_user, key: params[:name])
    end
    deliver_error! 404 unless result

    credential = result.credential
    unless credential.visibility == :VISIBILITY_SELECTED_REPOSITORIES
      deliver_error! 409, errors: "You cannot update selected repositories for a secret when the visibility is not set to 'selected'"
    end

    repo_id = params[:repository_id]
    selected_repositories_ids = credential.selected_repositories.map { |repo| Platform::Helpers::NodeIdentification.from_global_id(repo.global_id)[1] }.to_set
    deliver_error! 404 unless selected_repositories_ids.include?(repo_id)

    selected_repositories_ids.delete(repo_id)

    # Filter repositories to org repositories
    org_repository_ids = org.repositories.where(id: selected_repositories_ids).map(&:global_relay_id)

    # result is a GitHub::Launch::Services::Credz::StoreResponse
    result = rescue_from_grpc_errors("Secrets") do
      Credz.update_credential \
        app:   GitHub.launch_github_app, # we always store secrets in the prod launch env (even lab)
        owner: org,
        actor: current_user,
        key:   credential.name,
        visibility: credential.visibility,
        selected_repositories: org_repository_ids
    end

    validate_result!(result)
    validate_storage!(result)

    deliver_empty(status: 204)
  end

  # Replace the selected repositories for a secret with `selected` visibility
  put "/organizations/:organization_id/actions/secrets/:name/repositories" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#set-selected-repositories-for-an-organization-secret"
    deliver_error! 404 unless org_secrets_enabled?

    org = find_org!
    deliver_error! 404 unless can_use_org_secrets?(org)

    control_access :write_org_actions_secrets,
      resource: org,
      user: current_user,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    data = receive_with_schema("organization-actions-secret", "set-org-secret-repositories")
    result = rescue_from_grpc_errors("Secrets") do
      Credz.fetch_credential(app: GitHub.launch_github_app, owner: org, actor: current_user, key: params[:name])
    end
    deliver_error! 404 unless result

    credential = result.credential
    unless credential.visibility == :VISIBILITY_SELECTED_REPOSITORIES
      deliver_error! 409, errors: "You cannot update selected repositories for a secret when the visibility is not set to 'selected'"
    end

    selected_repository_ids = data["selected_repository_ids"]

    # Filter repositories to org repositories
    org_repository_ids = org.repositories.where(id: selected_repository_ids).map(&:global_relay_id)

    # result is a GitHub::Launch::Services::Credz::StoreResponse
    result = rescue_from_grpc_errors("Secrets") do
      Credz.update_credential \
        app:   GitHub.launch_github_app, # we always store secrets in the prod launch env (even lab)
        owner: org,
        actor: current_user,
        key:   credential.name,
        visibility: credential.visibility,
        selected_repositories: org_repository_ids
    end

    validate_result!(result)
    validate_storage!(result)

    deliver_empty(status: 204)
  end

  private

  def validate_listing!(result)
    unless result
      Failbot.report(StandardError.new("no response from list"), launch_credz: GitHub.launch_credz)
      deliver_error!(503, message: "Secrets unavailable. Please try again later.")
    end
  end

  def validate_result!(result)
    unless result
      Failbot.report(StandardError.new("no response from store_credential"), launch_credz: GitHub.launch_credz)
      deliver_error!(503, message: "Secrets unavailable. Please try again later.")
    end
  end

  def validate_storage!(result)
    unless result.stored
      Failbot.report(StandardError.new("store_credential did not store"), launch_credz: GitHub.launch_credz)
      deliver_error!(500, message: "Secret was not stored")
    end
  end

  def pack_earthsmoke_value(key_identifier, value)
    begin
      value = Base64.strict_decode64(value)
    rescue ArgumentError
      deliver_error! 422,
        message: "Provided value is not a valid base64 value.",
        documentation_url: @documentation_url
    end

    begin
      # validate that the public key provided is truly the latest
      key = GitHub.earthsmoke.low_level_key(Platform::EarthsmokeKeys::CUSTOM_TASKS)
      version = key.export.key_versions.first
      earthsmoke_latest_key_identifier = version.id

      unless key_identifier == earthsmoke_latest_key_identifier.to_s
        deliver_error! 422,
          message: "Provided key `#{key_identifier}` is not the latest version available. Call `GET /actions/secrets/public-key` and resign the data using the latest key.",
          documentation_url: @documentation_url
      end

      Earthsmoke::Embedding.embed(earthsmoke_latest_key_identifier, value)
    rescue ::Earthsmoke::UnavailableError, ::Earthsmoke::TimeoutError
      deliver_error! 503,
        message: "Signing and storage unavailable. Please try again in a few minutes.",
        documentation_url: @documentation_url
    end
  end

  def decrypt_enterprise_value(value)
    box = RbNaCl::Boxes::Sealed.from_private_key(Base64.decode64(GitHub.actions_secrets_private_key))
    box.decrypt(Base64.strict_decode64(value))
  end

  def can_use_org_secrets?(organization)
    # Exclude legacy plans
    Billing::ActionsPermission.new(organization).status[:error][:reason] != "PLAN_INELIGIBLE"
  end

  def org_secrets_enabled?
    GitHub.actions_enabled?
  end
end
