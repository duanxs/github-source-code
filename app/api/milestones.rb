# rubocop:disable Style/FrozenStringLiteralComment

class Api::Milestones < Api::App
  areas_of_responsibility :milestones, :api

  # List Milestones for an Issue
  get "/repositories/:repository_id/milestones" do
    @route_owner = "@github/pe-issues-projects"
    @documentation_url = "/rest/reference/issues#list-milestones"
    control_access :list_milestones,
      repo: repo = find_repo!,
      allow_integrations: true,
      allow_user_via_integration: true
    scope = repo.milestones

    sort = params[:sort] || "due_date"
    direction = params[:direction] || "asc"
    scope = scope.sorted_by(sort, direction)

    # filter by state
    scope = case params[:state]
    when "all"   then scope
    when /close/ then scope.closed_milestones
    else              scope.open_milestones
    end

    milestones = paginate_rel(scope)
    GitHub::PrefillAssociations.for_milestones(milestones, repo)

    deliver :milestone_hash, milestones, repo: repo
  end

  # Create a Milestone
  post "/repositories/:repository_id/milestones" do
    @route_owner = "@github/pe-issues-projects"
    @documentation_url = "/rest/reference/issues#create-a-milestone"

    control_access :create_milestone,
      repo: repo = find_repo!,
      allow_integrations: true,
      allow_user_via_integration: true
    ensure_repo_writable!(repo)

    data = receive_with_schema("milestone", "create-legacy")

    milestone = repo.milestones.build attr(data,
      :title, :state, :description, :due_on)
    milestone.created_by = current_user

    if milestone.save
      deliver :milestone_hash, milestone, status: 201, repo: repo
    else
      deliver_error 422,
        errors: milestone.errors,
        documentation_url: @documentation_url
    end
  end

  ShowMilestoneQuery = PlatformClient.parse <<-'GRAPHQL'
    query($id:ID!) {
      milestone: node(id:$id) {
        ...Api::Serializer::IssuesDependency::MilestoneFragment
      }
    }
  GRAPHQL

  def milestone_candidate(milestone, options)
    variables = {
      id: milestone.global_relay_id,
    }

    results   = platform_execute(ShowMilestoneQuery, variables: variables)
    milestone = results.data.milestone

    Api::Serializer.serialize(:graphql_milestone_hash, milestone, options)
  end

  # Get a single Milestone
  get "/repositories/:repository_id/milestones/:milestone_number" do
    @route_owner = "@github/pe-issues-projects"
    @documentation_url = "/rest/reference/issues#get-a-milestone"
    control_access :get_milestone,
      repo: repo = find_repo!,
      allow_integrations: true,
      allow_user_via_integration: true

    milestone = find_milestone(repo)

    if milestone
      GitHub::PrefillAssociations.for_milestones([milestone], repo)
      deliver :milestone_hash, milestone,
        repo: repo,
        last_modified: calc_last_modified(milestone),
        compare_payload_to: :milestone_candidate
    else
      deliver_error 404
    end
  end

  # Update a Milestone
  verbs :patch, :post, "/repositories/:repository_id/milestones/:milestone_number" do
    @route_owner = "@github/pe-issues-projects"
    @documentation_url = "/rest/reference/issues#update-a-milestone"
    control_access :update_milestone,
      repo: repo = find_repo!,
      allow_integrations: true,
      allow_user_via_integration: true
    ensure_repo_writable!(repo)

    data = receive(Hash)

    milestone = find_milestone(repo)
    deliver_error! 404 if milestone.nil?

    if milestone.update(attr(data, :title, :state, :description, :due_on))
      # Introducing strict validation of the milestone.update
      # JSON schema would cause breaking changes for integrators
      # skip_validation until a rollout strategy can be determined
      # TODO: replace `receive` with `receive_with_schema`
      # see: https://github.com/github/ecosystem-api/issues/1555
      _ = receive_with_schema("milestone", "update", skip_validation: true)

      GitHub::PrefillAssociations.for_milestones([milestone], repo)
      deliver :milestone_hash, milestone, repo: repo
    else
      deliver_error 422,
        errors: milestone.errors,
        documentation_url: @documentation_url
    end
  end

  # Delete a Milestone
  delete "/repositories/:repository_id/milestones/:milestone_number" do
    @route_owner = "@github/pe-issues-projects"

    # Introducing strict validation of the milestone.delete
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("milestone", "delete", skip_validation: true)

    @documentation_url = "/rest/reference/issues#delete-a-milestone"
    control_access :delete_milestone,
      repo: repo = find_repo!,
      allow_integrations: true,
      allow_user_via_integration: true
    ensure_repo_writable!(repo)

    if milestone = find_milestone(repo)
      milestone.destroy
      deliver_empty(status: 204)
    else
      deliver_error 404
    end
  end

  def find_milestone(repo)
    number = int_id_param!(key: :milestone_number).to_s
    return nil if number.blank?
    repo.milestones.find_by_number(number)
  end
end
