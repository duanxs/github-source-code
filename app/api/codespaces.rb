# frozen_string_literal: true

class Api::Codespaces < Api::App
  statsd_tag_actions

  # Set cascaded tokens to expire automatically. This value is currently the minimum to ensure that
  # Liveshare works with the codespace. Also to ensure that we never return a token that has less than
  # this minimum usable life left, we do not cache tokens generated from this API.
  CASCADE_TOKEN_EXPIRATION = 8.hours

  GONE_API_RESPONSE_MESSAGE = "This API endpoint has been removed. Please update the Visual Studio Codespaces extension."

  post "/vscs_internal/user/:user_id/codespaces/:name/token" do
    @route_owner = "@github/codespaces"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL

    deliver_error! 404 unless Codespaces::Policy.enabled_for_user?(current_user)

    owner = find_user!
    codespace = find_codespace(owner: owner, name: params[:name])

    control_access :write_codespace,
      resource: codespace,
      allow_integrations: false,
      allow_user_via_integration: true

    set_exception_context(codespace)

    # require a provisioned codespace so we can scope the token to a single environment
    # https://github.com/github/codespaces/issues/310
    deliver_error! 422, message: "codespace must be provisioned" unless codespace.provisioned?

    data = receive_with_schema("codespace", "mint-repository-token")

    client = ::Codespaces::ArmClient.new(resource_provider: codespace.plan.resource_provider)
    token = client.fetch_cascade_token \
      vscs_target: codespace.vscs_target,
      plan_id: codespace.plan.vscs_id,
      user: codespace.owner,
      cache: false,
      expires_in: CASCADE_TOKEN_EXPIRATION,
      environment_id: codespace.guid

    response = { token: token }

    if data["mint_repository_token"] && current_user.using_auth_via_oauth_application?
      repository_token = Codespaces.mint_github_token(current_user, codespace)
      response["repository_token"] = repository_token
    end

    codespace.mark_used!

    deliver_raw(response)
  end

  get "/vscs_internal/user/:user_id/codespaces" do
    @route_owner = "@github/codespaces"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL

    deliver_error! 404 unless Codespaces::Policy.enabled_for_user?(current_user)

    owner = find_user!

    control_access :list_codespaces,
      resource: owner,
      allow_integrations: false,
      allow_user_via_integration: true

    codespaces = paginate_rel(find_codespaces(owner: owner))
    deliver :codespaces_hash, { codespaces: codespaces, total_count: codespaces.total_entries }
  end

  get "/vscs_internal/user/:user_id/codespaces/:name" do
    @route_owner = "@github/codespaces"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL

    deliver_error! 404 unless Codespaces::Policy.enabled_for_user?(current_user)

    owner = find_user!
    codespace = find_codespace(owner: owner, name: params[:name])

    control_access :read_codespaces,
      resource: codespace,
      allow_integrations: false,
      allow_user_via_integration: true

    set_exception_context(codespace)

    env = if codespace.provisioned?
      client = ::Codespaces::VscsClient.for_codespace(codespace)
      client.fetch_environment(codespace.guid)
    end

    deliver :codespace_with_env_hash, { codespace: codespace, environment: env }
  end

  post "/vscs_internal/proxy/environments/:environment_id/start" do
    @route_owner = "@github/codespaces"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL

    deliver_error! 404 unless Codespaces::Policy.enabled_for_user?(current_user)

    codespace = find_codespace_by_guid(params[:environment_id])

    control_access :write_codespace,
      resource: codespace,
      allow_integrations: false,
      allow_user_via_integration: true

    deliver_error! 403 if codespace.owner.spammy? or codespace.repository.owner.spammy?

    set_exception_context(codespace)

    client = ::Codespaces::VscsClient.for_codespace(codespace)
    resp = client.start_environment(codespace.guid)

    deliver_raw resp.body, \
     status: resp.status,
     content_type: resp.headers["Content-Type"]
  end

  post "/workspaces/*" do
    @route_owner = "@github/codespaces"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL

    gone_response
  end

  post "/vso_internal/*" do
    @route_owner = "@github/codespaces"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL

    gone_response
  end

  get "/vso_internal/*" do
    @route_owner = "@github/codespaces"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL

    gone_response
  end

  private

  # Look up codespaces based on the given combination of owner, name, and guid,
  # filtering out codespaces that the current user cannot see.
  #
  # Note that `name` and `guid` are mutually exclusive.
  #
  # owner - the User owner of the codespaces. Defaults to `current_user`.
  # name  - the String name of the codespace. Defaults to nil.
  # guid  - the String guid of the codespace. Defaults to nil.
  def find_codespaces(owner: current_user, name: nil, guid: nil)
    raise ArgumentError, "`name` and `guid` are mutually exclusive" if name && guid

    scope = owner.codespaces
    scope = if name
      scope.where(name: name)
    elsif guid
      scope.where(guid: guid)
    else
      scope.order("id ASC")
    end

    codespaces = scope.visible_to(current_user).to_a

    GitHub::PrefillAssociations.for_codespaces(codespaces)

    unauthed_saml_targets = Set.new(protected_organization_ids)
    codespaces.reject do |codespace|
      unauthed_saml_targets.include?(codespace.plan&.owner_id)
    end
  end

  def find_codespace(owner:, name:)
    find_codespaces(owner: owner, name: name).first
  end

  def find_codespace_by_guid(guid)
    find_codespaces(guid: guid).first
  end

  def set_exception_context(codespace)
    Codespaces::ErrorReporter.push(codespace: codespace)
  end

  def gone_response
    GitHub.dogstats.increment("codespaces.deprecated_prefix_gone")
    deliver_error!(410, message: GONE_API_RESPONSE_MESSAGE)
  end
end
