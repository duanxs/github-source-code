# rubocop:disable Style/FrozenStringLiteralComment

class Api::Enterprise::Stats < Api::App
  areas_of_responsibility :enterprise_only, :api

  map_to_service :orgs, only: [
    "GET /enterprise/stats/orgs"
  ]
  map_to_service :admin_experience, only: [
    "GET /enterprise/stats/all",
    "GET /enterprise/stats/comments",
    "GET /enterprise/stats/gists",
    "GET /enterprise/stats/users"
  ]
  map_to_service :repo_info, only: [
    "GET /enterprise/stats/repos",
  ]
  map_to_service :pages, only: [
    "GET /enterprise/stats/pages",
  ]
  map_to_service :pull_requests, only: [
    "GET /enterprise/stats/pulls",
  ]
  map_to_service :issues, only: [
    "GET /enterprise/stats/issues",
    "GET /enterprise/stats/milestones",
  ]

  # These stats are primarily for use with Enterprise. Most of this data
  # is collected and used via statsd with .com, but we don't have any kind
  # of statsd infrastructure working with Enterprise yet. In the mean time,
  # we need to provide some metrics to make Enterprise admins happy. As
  # better methods become available we can replace this stuff with it.

  STATS = GitHub::Stats::Site

  get "/enterprise/stats/all" do
    @route_owner = "@github/admin-experience"
    @documentation_url = "/enterprise/v3/enterprise-admin/admin_stats/#get-statistics"
    unless trusted_port?
      control_access :enterprise, allow_integrations: false, allow_user_via_integration: false
    end

    deliver_raw(
      repos: STATS.repo_stats,
      hooks: STATS.hook_stats,
      pages: STATS.page_stats,
      orgs: STATS.org_stats,
      users: STATS.user_stats,
      pulls: STATS.pull_request_stats,
      issues: STATS.issue_stats,
      milestones: STATS.milestone_stats,
      gists: STATS.gist_stats,
      comments: STATS.comment_stats,
    )
  end

  get "/enterprise/stats/repos" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/enterprise/v3/enterprise-admin/admin_stats/#get-statistics"
    unless trusted_port?
      control_access :enterprise, allow_integrations: false, allow_user_via_integration: false
    end

    deliver_raw STATS.repo_stats
  end

  get "/enterprise/stats/hooks" do
    @route_owner = "@github/ecosystem-events"
    @documentation_url = "/enterprise/v3/enterprise-admin/admin_stats/#get-statistics"
    unless trusted_port?
      control_access :enterprise, allow_integrations: false, allow_user_via_integration: false
    end

    deliver_raw STATS.hook_stats
  end

  get "/enterprise/stats/pages" do
    @route_owner = "@github/pages"
    @documentation_url = "/enterprise/v3/enterprise-admin/admin_stats/#get-statistics"
    unless trusted_port?
      control_access :enterprise, allow_integrations: false, allow_user_via_integration: false
    end

    deliver_raw STATS.page_stats
  end

  get "/enterprise/stats/orgs" do
    @route_owner = "@github/teams-and-orgs"
    @documentation_url = "/enterprise/v3/enterprise-admin/admin_stats/#get-statistics"
    unless trusted_port?
      control_access :enterprise, allow_integrations: false, allow_user_via_integration: false
    end

    deliver_raw STATS.org_stats
  end

  get "/enterprise/stats/users" do
    @route_owner = "@github/admin-experience"
    @documentation_url = "/enterprise/v3/enterprise-admin/admin_stats/#get-statistics"
    unless trusted_port?
      control_access :enterprise, allow_integrations: false, allow_user_via_integration: false
    end

    deliver_raw STATS.user_stats
  end

  get "/enterprise/stats/pulls" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/enterprise/v3/enterprise-admin/admin_stats/#get-statistics"
    unless trusted_port?
      control_access :enterprise, allow_integrations: false, allow_user_via_integration: false
    end

    deliver_raw STATS.pull_request_stats
  end

  get "/enterprise/stats/issues" do
    @route_owner = "@github/pe-issues-projects"
    @documentation_url = "/enterprise/v3/enterprise-admin/admin_stats/#get-statistics"
    unless trusted_port?
      control_access :enterprise, allow_integrations: false, allow_user_via_integration: false
    end

    deliver_raw STATS.issue_stats
  end

  get "/enterprise/stats/milestones" do
    @route_owner = "@github/pe-issues-projects"
    @documentation_url = "/enterprise/v3/enterprise-admin/admin_stats/#get-statistics"
    unless trusted_port?
      control_access :enterprise, allow_integrations: false, allow_user_via_integration: false
    end

    deliver_raw STATS.milestone_stats
  end

  get "/enterprise/stats/gists" do
    @route_owner = "@github/admin-experience"
    @documentation_url = "/enterprise/v3/enterprise-admin/admin_stats/#get-statistics"
    unless trusted_port?
      control_access :enterprise, allow_integrations: false, allow_user_via_integration: false
    end

    deliver_raw STATS.gist_stats
  end

  get "/enterprise/stats/comments" do
    @route_owner = "@github/admin-experience"
    @documentation_url = "/enterprise/v3/enterprise-admin/admin_stats/#get-statistics"
    unless trusted_port?
      control_access :enterprise, allow_integrations: false, allow_user_via_integration: false
    end

    deliver_raw STATS.comment_stats
  end
end
