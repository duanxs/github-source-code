# frozen_string_literal: true

class Api::Enterprise::Actions < Api::App
  areas_of_responsibility :enterprise_only, :api
  map_to_service :actions_experience
  statsd_tag_actions "/enterprise/actions-token"

  before do
    deliver_error! 404 unless GitHub.enterprise? && GitHub.actions_enabled?
  end

  # This is used for authenticating dotcom to Enterprise using an Enterprise
  # installation.
  post "/enterprise/actions-token" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL

    require_authentication!
    deliver_error! 404 unless GitHub::Connect.download_dotcom_actions_enabled?

    # Only let the Actions scoped tokens access this endpoint.
    deliver_error! 404 unless actions_request?

    response = GitHub::Connect.create_dotcom_actions_download_token

    deliver_raw(GitHub::JSON.parse(response.body), status: response.status)
  end
end
