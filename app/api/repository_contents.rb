# frozen_string_literal: true

class Api::RepositoryContents < Api::App
  map_to_service :tree, only: [
    "GET /repositories/:repository_id/tarball/?*?",
    "GET /repositories/:repository_id/zipball/?*?"
  ]

  include ActionView::Helpers::TextHelper, ActionView::Helpers::TagHelper,
    TextHelper, BasicHelper, BlobMarkupHelper, Api::App::ContentHelpers,
    Api::App::GitActorHelpers

  MAX_BLOB_SIZE = 1.megabyte

  # Get a readme in a repo
  get %r{/repositories/(\d+)/readme(?:/(.*))?} do |repository_id, path|
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#get-a-repository-readme"
    begin
      @content_path = path

      control_access :get_readme,
        resource: current_repository,
        allow_integrations: true,
        allow_user_via_integration: true,
        approved_integration_required: false

      if current_repository.empty? || tree_name.blank?
        deliver_error!(404)
      end

      if readme
        if medias.api_param?(:raw)
          deliver_raw readme.data,
            content_type: "#{medias}; charset=utf-8",
            last_modified: calc_last_modified(current_commit)
        elsif medias.api_param?(:html)
          if readme.binary?
            deliver_error!(422, message: "We can’t display this README as HTML because it appears to contain binary data.")
          end

          if source = readme.symlink_source
            readme.info["path"] = source.path
          end

          deliver_raw render_file(readme, readme.path),
            content_type: "#{medias}; charset=utf-8",
            last_modified: calc_last_modified(current_commit)
        else
          if source = readme.symlink_source
            readme.info["sha"] = source.oid
            readme.info["path"] = source.path
          end

          deliver :content_hash, readme,
            repo: current_repository,
            full: true,
            last_modified: calc_last_modified(current_commit),
            ref: tree_name
        end
      else
        deliver_error(404)
      end
    rescue GitRPC::NoSuchPath
      deliver_error(404)
    end
  end

  # Get a repository's license
  get "/repositories/:repository_id/license" do |repository_id|
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/licenses#get-the-license-for-a-repository"
    control_access :get_license,
      resource: current_repository,
      allow_integrations: true,
      allow_user_via_integration: true,
      approved_integration_required: false

    deliver_error!(404) if current_repository.empty? || tree_name.blank?
    deliver_error!(404) unless current_repository.license && license

    if medias.api_param?(:raw)
      deliver_raw license.data,
        content_type: "#{medias}; charset=utf-8",
        last_modified: calc_last_modified(current_commit)
    elsif medias.api_param?(:html)
      deliver_raw render_file(license, license.path),
        content_type: "#{medias}; charset=utf-8",
        last_modified: calc_last_modified(current_commit)
    else
      deliver :license_content_hash, license,
        repo: current_repository,
        full: true,
        last_modified: calc_last_modified(current_commit),
        ref: tree_name
    end
  end

  # Get a specific file or directory in a repo
  get "/repositories/:repository_id/contents/?*" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#get-repository-content"
    control_access :get_contents,
      resource: current_repository,
      path: content_path,
      allow_integrations: true,
      allow_user_via_integration: true,
      approved_integration_required: false

    serve_contents do
      if tree
        content_options = {
          repo: current_repository,
          last_modified: calc_last_modified(current_repository),
          ref: tree_name,
          etag: tree_sha,
        }
        if medias.api_param?(:object)
          deliver :tree_object_content_hash, tree_object(tree), content_options
        else
          deliver :content_hash, tree, content_options
        end
      elsif submodule
        deliver :content_hash, submodule,
          repo: current_repository,
          full: true,
          last_modified: calc_last_modified(current_repository),
          ref: tree_name,
          etag: tree_sha
      else
        deliver_error!(404) if blob.nil?
        deliver_too_large_error!(blob.size) if blob.size > MAX_BLOB_SIZE && !blob.git_lfs?

        if medias.api_param?(:raw)
          data = (blob.symlink_target || blob).data
          deliver_raw data, content_type: "#{medias}; charset=utf-8",
            last_modified: calc_last_modified(current_commit),
            etag: blob.oid
        elsif medias.api_param?(:html)
          if link = blob.symlink_target
            link.info["path"] = blob.path
            deliver_raw render_file(link, path_string, "file"),
              content_type: "#{medias}; charset=utf-8",
              last_modified: calc_last_modified(current_commit),
              etag: blob.oid
          else
            deliver_raw render_file(blob, path_string, "file"),
              content_type: "#{medias}; charset=utf-8",
              last_modified: calc_last_modified(current_commit),
              etag: blob.oid
          end
        else
          highlight = %w(true 1).include?(params[:highlight])
          deliver :content_hash, blob,
            repo: current_repository,
            full: true,
            last_modified: calc_last_modified(current_commit),
            highlight: highlight,
            ref: tree_name,
            etag: blob.oid
        end
      end
    end
  end

  # Create or replace a file in a repo
  put %r{/repositories/(\d+)/contents/(.+)} do |repository_id, path|
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#create-or-update-file-contents"

    repo = current_repository

    control_access :update_file, resource: repo, path: path, allow_integrations: true, allow_user_via_integration: true

    ensure_repo_writable!(repo, path: path)

    if message = validate_path(path)
      deliver_error! 422,
        message: "path #{message}",
        errors: [api_error(:Commit, :path, :invalid)]
    end

    data = receive(Hash, required: true) || {}
    attributes = attr(data, :message, :content, :branch, :author, :committer, :sha)

    branch = (attributes["branch"] || repo.default_branch).to_s
    file_present = repo.includes_file?(path, branch)
    update = file_present
    create = !update
    action = create ? "create" : "update"

    ensure_data_satisfies_schema!(attributes, "file", action)

    # Determine whether the commit should be automatically signed by GitHub's
    # web committer.
    #
    # If the content creator is a bot and neither the committer nor the author
    # are specified, we can sign the commit with confidence. We're choosing
    # (for now) not to sign bot-created commits authored on behalf of a user.
    sign = current_user.bot? &&
      current_user.commit_signing_enabled? &&
      attributes["author"].nil? &&
      attributes["committer"].nil?

    metadata = set_commit_metadata(attributes)

    # Remove the specified committer if we are to sign the commit. This
    # allows CommitsCollection#create to assign GitHub's web committer instead
    # of the current user.
    metadata.delete(:committer) if sign

    begin
      ref = if create && repo.heads.empty?
        repo.heads.find_or_build(branch)
      else
        repo.heads.find(branch)
      end

      deliver_error!(404, message: "Branch #{branch} not found") unless ref

      if create && !repo.valid_file_path?(path, branch)
        deliver_error!(409, message: "Sorry, a file exists where you’re trying to create a subdirectory. Choose a new path and try again.")
      end

      if update
        blob = repo.tree_entry(ref.target_oid, path)
        if !blob.blob?
          deliver_error!(422, message: "#{path} is not a file")
        end
        blob_sha = attributes["sha"]
        if blob_sha != blob.sha
          deliver_error!(409, message: "#{path} does not match #{blob_sha}")
        end
      end

      ref.append_commit(metadata, current_user, sign: sign, reflog_data: request_reflog_data("git repo contents api")) do |files|
        files.add(path, decode_content(attributes["content"]))
      end
    rescue Git::Ref::HookFailed => e
      deliver_error!(409, message: "Could not #{action} file because a Git pre-receive hook failed.\n\n#{e.message}")
    rescue Git::Ref::ProtectedBranchUpdateError => e
      deliver_error!(409, {
        message: "Could not #{action} file: #{e.result.message}",
        documentation_url: "#{GitHub.help_url}/articles/about-protected-branches",
      })
    rescue Git::Ref::ComparisonMismatch => e
      deliver_error!(409, message: e.message)
    rescue GitRPC::BadGitmodules, GitRPC::SymlinkDisallowed => e
      deliver_error!(422, message: e.message)
    end

    content = content_at(repo, ref.target.tree_oid, path)

    GitHub.dogstats.increment("file", tags: ["via:api", "action:#{action}"])

    options = {
      content: content,
      commit: ref.target,
      ref: branch,
    }
    deliver :contents_crud_hash, options,
      repo: repo,
      status: create ? 201 : 200
  end

  # Delete a file in a repo
  delete %r{/repositories/(\d+)/contents/(.+)} do |repository_id, path|
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#delete-a-file"

    repo = current_repository

    control_access :delete_file, resource: repo, path: path, allow_integrations: true, allow_user_via_integration: true

    ensure_repo_writable!(repo, path: path)

    if message = validate_path(path)
      deliver_error! 422,
        message: "path #{message}",
        errors: [api_error(:Commit, :path, :invalid)]
    end

    data = receive(Hash, required: false) || {}
    attributes = attr(data, :message, :content, :branch, :author, :committer, :sha)

    # Most HTTP tooling doesn't support DELETE with body so we need to look for
    # required params in the query, too.
    attributes = attributes.merge(commit_params_from_query)

    branch = (attributes["branch"] || repo.default_branch).to_s
    file_present = repo.includes_file?(path, branch)

    ensure_data_satisfies_schema!(attributes, "file", "delete")
    if !repo.heads.exist?(branch) && !repo.heads.empty?
      deliver_error!(404, message: "Branch #{branch} not found")
    end
    deliver_error!(404) unless file_present

    metadata = set_commit_metadata(attributes)

    begin
      ref = repo.heads.find(branch)

      deliver_error!(404, message: "Branch #{branch} not found") unless ref

      blob = repo.tree_entry(ref.target_oid, path)
      if !blob.blob?
        deliver_error!(422, message: "#{path} is not a file")
      end
      blob_sha = attributes["sha"]
      if blob_sha != blob.sha
        deliver_error!(409, message: "#{path} does not match #{blob_sha}")
      end

      ref.append_commit(metadata, current_user, reflog_data: request_reflog_data("git repo contents api")) do |files|
        files.remove(path)
      end
    rescue Git::Ref::HookFailed => e
      deliver_error!(409, message: "Could not delete file because a Git pre-receive hook failed.\n\n#{e.message}")
    rescue Git::Ref::ProtectedBranchUpdateError => e
      deliver_error!(409, {
        message: "Could not delete file: #{e.result.message}",
        documentation_url: "#{GitHub.help_url}/articles/about-protected-branches",
      })
    rescue Git::Ref::ComparisonMismatch,
           Git::Ref::InvalidName,
           Git::Ref::UpdateFailed => e
      deliver_error!(409, message: e.message)
    rescue GitRPC::Failure,
           GitRPC::BadGitmodules,
           GitRPC::SymlinkDisallowed => e
      deliver_error!(422, message: e.message)
    rescue GitHub::DGit::UnroutedError,
           GitHub::DGit::InsufficientQuorumError,
           GitHub::DGit::ThreepcFailedToLock
      deliver_error!(503, message: "Could not delete file. Please try again later.")
    end

    GitHub.dogstats.increment("file", tags: ["via:api", "action:delete"])

    options = {
      content: nil,
      commit: ref.target,
      ref: branch,
    }
    deliver :contents_crud_hash, options,
      repo: repo,
      status: 200
  end

  # Get a tarball link
  get "/repositories/:repository_id/tarball/?*?" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#download-a-repository-archive"
    skip_rate_limit!
    control_access :get_archive_link,
      resource: current_repository,
      allow_integrations: true,
      allow_user_via_integration: true,
      approved_integration_required: false

    codeload "legacy.tar.gz"
  end

  # Get a zipball link
  get "/repositories/:repository_id/zipball/?*?" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#download-a-repository-archive"
    skip_rate_limit!
    control_access :get_archive_link,
      resource: current_repository,
      allow_integrations: true,
      allow_user_via_integration: true,
      approved_integration_required: false

    codeload "legacy.zip"
  end

private
  def commit_params_from_query
    params = request.params

    data = {}

    data.update("message" => params["message"]) if params.key?("message")
    data.update("sha" => params["sha"]) if params.key?("sha")
    data.update("branch" => params["branch"]) if params.key?("branch")

    author = {
      "name"  => params["author.name"],
      "email" => params["author.email"],
    }
    data.update("author" => author) if author.values.any?

    committer = {
      "name"  => params["committer.name"],
      "email" => params["committer.email"],
    }
    data.update("committer" => committer) if committer.values.any?

    data
  end

  def render_file(readme, path, id = "readme")
    content = format_readme(readme, relative_paths: true)
    ext = File.extname(readme.name).sub(".", "")
    content_tag :div, content.html_safe, # rubocop:disable Rails/OutputSafety
      :id => id,
      :class => ext,
      "data-path" => "#{h(path)}"
  end

  def base_url
    GitHub.url
  end

  def codeload(type)
    ac = current_repository.archive_command(branch_to_archive, type)
    url = ac.codeload_url(current_user)

    expires 0, :public, :must_revalidate
    redirect url
  end

  def branch_to_archive
    unless GitHub.flipper[:missing_archive_branch_redirect].enabled?(current_repository.owner)
      return splat_tree_name
    end

    if current_repository.valid_branch?(splat_tree_name)
      splat_tree_name
    else
      current_repository.default_branch
    end
  end

  # Internal: set the metadata for the generated commit
  #
  # see CommitsCollection#create for the necessary data
  #
  # Returns a Hash of commit metadata
  def set_commit_metadata(attributes)
    metadata = {
      message: attributes["message"],
      committer: current_user,
      author: current_user,
    }

    log_data[:commit_metadata_keys] = []

    if attributes["committer"]
      committer = git_actor!(attributes, key: "committer")
      metadata[:committer] = committer.to_gitrpc_hash.symbolize_keys
      metadata[:author] = committer.to_gitrpc_hash.symbolize_keys
      log_data[:commit_metadata_keys] << "committer"
    end

    if attributes["author"]
      author = git_actor!(attributes, key: "author")
      metadata[:author] = author.to_gitrpc_hash.symbolize_keys
      log_data[:commit_metadata_keys] << "author"
    end

    metadata
  end

  # Internal: decode the content from Base64
  #
  # Delivers an error (422) if the input is not valid Base64
  #
  # Returns a decoded String of content
  def decode_content(content)
    Base64.strict_decode64(content.gsub("\n", ""))
  rescue ArgumentError
    deliver_error!(422, message: "content is not valid Base64")
  end

  # Internal: deliver too large forbidden error.
  #
  # Delivers a 403 indicating the blob is too large to return. Provide alternative method for
  # obtaining the blob, depending on size.
  #
  # Delivers a 403 with explanation.
  def deliver_too_large_error!(blob_size)
    message = "This API returns blobs up to 1 MB in size. "\
      "The requested blob is too large to fetch via the API, "

    message += if blob_size <= Api::GitBlobs::MAX_BLOB_SIZE
      "but you can use the Git Data API to request blobs up to 100 MB in size."
    else
      "but you can always clone the repository via Git in order to obtain this blob."
    end

    deliver_error! 403,
      message: message,
      errors: [api_error(:Blob, :data, :too_large)],
      documentation_url: "/rest/reference/repos#get-repository-content"
  end

  def rate_limited_route?
    return false if request.path_info =~ %r{/(tar|zip)ball/}

    super
  end
end
