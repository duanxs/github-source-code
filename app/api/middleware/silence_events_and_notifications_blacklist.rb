# rubocop:disable Style/FrozenStringLiteralComment

class Api::Middleware::SilenceEventsAndNotificationsBlacklist

  OAUTH_APPLICATION_BLACKLIST_KEYS = [
    # https://github.com/github/github/issues/39050
    # GoogleCodeExporter
    "c564ab4618cc91889b39",
    # @kdaigle test app for silencing
    "c5dd1bde32564bcb7c36",
  ].freeze

  USER_AGENT_BLACKLIST_KEYS = [
    # Google Code manual importer has this unique UserAgent
    "GoogleCodeIssueExporter/1.0",
  ].freeze

  def initialize(app)
    @app = app
  end

  def call(env)
    if blacklisted_request?(env)
      GitHub.importing do
        @app.call(env)
      end
    else
      @app.call(env)
    end
  end

  def blacklisted_request?(env)
    user_agent_in_blacklist?(env) || oauth_application_in_blacklist?(env)
  end

  def user_agent_in_blacklist?(env)
    user_agent = env["HTTP_USER_AGENT"]
    return false unless user_agent.present?

    user_agent_blacklist_keys.include?(user_agent)
  end

  def oauth_application_in_blacklist?(env)
    creds = Api::RequestCredentials.from_env(env)
    return false unless creds.token_present?

    user = User.with_oauth_token(creds.token)
    return false unless user && oauth_application = user.oauth_application

    oauth_application_blacklist_keys.include?(oauth_application.key)
  end

  def user_agent_blacklist_keys
    USER_AGENT_BLACKLIST_KEYS
  end

  def oauth_application_blacklist_keys
    OAUTH_APPLICATION_BLACKLIST_KEYS
  end
end
