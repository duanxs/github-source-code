# rubocop:disable Style/FrozenStringLiteralComment

class Api::RepositoryReleases < Api::App
  before do
    @accepted_scopes = [:repo]
    bypass_allowed_ip_enforcement if hmac_authenticated_internal_service_request?
  end

  # List releases for a repository
  get "/repositories/:repository_id/releases" do
    @route_owner = "@github/ee-osn"
    @documentation_url = "/rest/reference/repos#list-releases"
    control_access :list_releases,
      repo: repo = find_repo!,
      allow_integrations: true,
      allow_user_via_integration: true,
      approved_integration_required: false

    releases = filter_releases(repo)
    assets = ReleaseAsset.for(repo, releases).group_by { |a| a.release_id }

    prefill_releases(repo, releases, assets.values.flatten)

    deliver :release_hash, releases, repo: repo, assets: assets
  end

  # Get a latest published release
  get "/repositories/:repository_id/releases/latest" do
    @route_owner = "@github/ee-osn"
    @documentation_url = "/rest/reference/repos#get-the-latest-release"
    repo = find_repo!
    release = Release.latest_for(repo)
    deliver_error! 404 if release.nil?

    control_access :get_release, repo: repo, resource: release, allow_integrations: true, allow_user_via_integration: true

    prefill_releases(repo, [release], release.uploaded_assets)

    deliver :release_hash, release, repo: repo, last_modified: calc_last_modified(release)
  end

  # Get a single release by tag
  get "/repositories/:repository_id/releases/tags/*" do
    @route_owner = "@github/ee-osn"
    @documentation_url = "/rest/reference/repos#get-a-release-by-tag-name"
    repo = find_repo!
    slug = params[:splat].join("/")
    release = Release.from_tag_or_slug(repo, slug, can_list_draft_releases?(repo))

    # If we fetch a release that hasn't been saved, we probably
    # fetched just a tag, and should not treat it as a release
    deliver_error! 404 if release.nil? || release.new_record?

    control_access :get_release, repo: repo, resource: release, allow_integrations: true, allow_user_via_integration: true

    prefill_releases(repo, [release], release.uploaded_assets)

    deliver :release_hash, release, repo: repo, last_modified: calc_last_modified(release)
  end

  # Get a single release
  get "/repositories/:repository_id/releases/:release_id" do
    @route_owner = "@github/ee-osn"
    @documentation_url = "/rest/reference/repos#get-a-release"
    repo, release = find_repo_and_release
    control_access :get_release, repo: repo, resource: release, allow_integrations: true, allow_user_via_integration: true

    prefill_releases(repo, [release], release.uploaded_assets)

    deliver :release_hash, release, repo: repo, last_modified: calc_last_modified(release)
  end

  # Delete a release
  delete "/repositories/:repository_id/releases/:release_id" do
    @route_owner = "@github/ee-osn"

    # Introducing strict validation of the release.delete
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("release", "delete", skip_validation: true)

    @documentation_url = "/rest/reference/repos#delete-a-release"
    repo, release = find_repo_and_release
    control_access :delete_release, repo: repo, resource: release, allow_integrations: true, allow_user_via_integration: true
    ensure_repo_writable!(repo)

    release.destroy

    deliver_empty(status: 204)
  end

  # Update a release
  verbs :patch, :post, "/repositories/:repository_id/releases/:release_id" do
    @route_owner = "@github/ee-osn"
    @documentation_url = "/rest/reference/repos#update-a-release"
    _repo, release = find_repo_and_release
    control_access :edit_release, repo: repo = find_repo!, resource: release, allow_integrations: true, allow_user_via_integration: true
    ensure_repo_writable!(repo)

    # Introducing strict validation of the release.update
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    data = receive_with_schema("release", "update", skip_validation: true)

    release.with_params \
      tag_name: data["tag_name"],
      name: data["name"],
      body: data["body"],
      draft: data["draft"],
      prerelease: data["prerelease"],
      target_commitish: data["target_commitish"]

    if GitHub::SchemaDomain.allowing_cross_domain_transactions { release.save }
      prefill_releases(repo, [release], release.uploaded_assets)
      deliver :release_hash, release, repo: repo
    else
      deliver_error 422,
        errors: release.errors,
        documentation_url: @documentation_url
    end
  end

  # Create a new release
  post "/repositories/:repository_id/releases" do
    @route_owner = "@github/ee-osn"
    @documentation_url = "/rest/reference/repos#create-a-release"
    control_access :create_release, repo: repo = find_repo!, allow_integrations: true, allow_user_via_integration: true
    ensure_repo_writable!(repo)

    data = receive_with_schema("release", "create-legacy")
    release = repo.releases.build author: current_user, reflog_data: request_reflog_data("releases create api")
    release.with_params \
      tag_name: data["tag_name"],
      name: data["name"],
      body: data["body"],
      draft: data["draft"],
      prerelease: data["prerelease"],
      target_commitish: data["target_commitish"]

    if release.save
      prefill_releases(repo, [release], release.uploaded_assets)
      deliver :release_hash, release, status: 201, repo: repo
    else
      deliver_error 422,
        errors: release.errors,
        documentation_url: @documentation_url
    end
  end

  # List a release's assets
  get "/repositories/:repository_id/releases/:release_id/assets" do
    @route_owner = "@github/ee-osn"
    @documentation_url = "/rest/reference/repos#list-release-assets"
    repo, release = find_repo_and_release
    control_access :get_release, repo: repo, resource: release, allow_integrations: true, allow_user_via_integration: true

    assets = release.release_assets
    assets = paginate_rel(assets)
    prefill_releases(repo, [release], assets)

    GitHub::PrefillAssociations.asset_release(release, assets) if assets.present?

    deliver :release_asset_hash, assets
  end

  # Get a single release asset
  get "/repositories/:repository_id/releases/assets/:asset_id" do
    @route_owner = "@github/ee-osn"
    @documentation_url = "/rest/reference/repos#get-a-release-asset"
    allow_media ReleaseAsset::DOWNLOAD_CONTENT_TYPE, GitHub::OCTOCAT_CONTENT_TYPE
    repo, asset = find_repo_and_release_asset
    control_access :get_release, repo: repo, resource: asset.release, allow_integrations: true, allow_user_via_integration: true
    if medias.api
      prefill_releases(repo, nil, [asset])
      deliver :release_asset_hash, asset, last_modified: calc_last_modified(asset)
    else
      asset.download
      status 302
      response["location"] = url = asset.url(actor: current_user)

      if medias.sub_type =~ /octocat/i
        content_type GitHub::OCTOCAT_CONTENT_TYPE
        GitHub.octocat(url)
      end
    end
  end

  # edit a release asset
  patch "/repositories/:repository_id/releases/assets/:asset_id" do
    @route_owner = "@github/ee-osn"
    @documentation_url = "/rest/reference/repos#update-a-release-asset"
    repo, asset = find_repo_and_release_asset
    control_access :edit_release, repo: repo, resource: asset.release, allow_integrations: true, allow_user_via_integration: true
    ensure_repo_writable!(repo)

    data = receive_with_schema("release-asset", "update")

    edited = false
    %w(name label).each do |key|
      next unless value = data[key]
      asset.send("#{key}=", value)
      edited = :save
    end

    if asset.starter? && data["state"] == "uploaded"
      edited = :track_uploaded
    end

    if !edited || asset.send(edited)
      prefill_releases(repo, nil, [asset])

      deliver :release_asset_hash, asset, last_modified: calc_last_modified(asset)
    else
      deliver_error 422,
        errors: asset.errors,
        documentation_url: @documentation_url
    end
  end

  # delete a single release asset
  delete "/repositories/:repository_id/releases/assets/:asset_id" do
    @route_owner = "@github/ee-osn"

    # Introducing strict validation of the release-asset.delete
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("release-asset", "delete", skip_validation: true)

    @documentation_url = "/rest/reference/repos#delete-a-release-asset"
    repo, asset = find_repo_and_release_asset
    control_access :delete_release, repo: repo, resource: asset.release, allow_integrations: true, allow_user_via_integration: true
    ensure_repo_writable!(repo)

    asset.destroy

    deliver_empty(status: 204)
  end

  private

  alias actually_ensure_acceptable_media_types! ensure_acceptable_media_types!

  # skip this check in a before filter, do it before #control_access instead
  # Needed so that an asset's content type can be added to the list of acceptable
  # media types
  def ensure_acceptable_media_types!
  end

  def control_access(*args)
    actually_ensure_acceptable_media_types!
    super(*args)
  end

  def filter_releases(repo)
    scope = can_list_draft_releases?(repo) ? repo.releases : repo.releases.published
    paginate_rel(scope.order("id desc"))
  end

  def find_repo_and_release
    repo = find_repo!
    release = repo.releases.find_by_id(int_id_param!(key: :release_id)) if repo
    deliver_error! 404 if release.nil?
    [repo, release]
  end

  def find_repo_and_release_asset
    repo = find_repo!
    asset = repo.release_assets.find_by_id(int_id_param!(key: :asset_id)) if repo
    deliver_error! 404 if asset.nil?
    [repo, asset]
  end

  def can_list_draft_releases?(repo)
    access_allowed? :list_draft_releases, repo: repo, allow_integrations: true, allow_user_via_integration: true
  end

  def prefill_releases(repository, releases, assets)
    GitHub::PrefillAssociations.for_releases(repository, releases, assets)
  end
end
