# rubocop:disable Style/FrozenStringLiteralComment

class Api::IntegrationInstallations < Api::App
  areas_of_responsibility :api
  statsd_tag_actions "/user/installations/:installation_id/repositories"

  class InstallationError < StandardError; end

  USER_OR_USER_VIA_GITHUB_APP_ACCESS_ONLY = "You must authenticate with an access token authorized to a GitHub App, a personal access token, or basic auth in order to ".freeze
  USER_ACCESS_ONLY = "You must authenticate with a personal access token or basic auth in order to ".freeze

  get "/installation/repositories" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = "/rest/reference/apps#list-repositories-accessible-to-the-app-installation"

    set_forbidden_message \
      "You must authenticate with an installation access token in order to " +
      "list repositories for an installation."

    control_access :list_installation_repositories,
      resource: current_integration,
      installation: current_integration_installation,
      challenge: true,
      allow_integrations: true,
      allow_user_via_integration: false

    repositories = current_integration_installation.repositories
    total_count  = repositories.count

    repositories = paginate_rel(repositories.sorted_by(:full_name, "asc"))
    GitHub::PrefillAssociations.for_repositories(repositories)
    GitHub::PrefillAssociations.fill_licenses(repositories)

    deliver :integration_installation_repositories_hash,
      { repositories: repositories, total_count: total_count,
        repository_selection: current_integration_installation.repository_selection }
  end

  delete "/installation/token" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = "/rest/reference/apps#revoke-an-installation-access-token"

    set_forbidden_message "You must authenticate with an installation access token in order to revoke it"

    control_access :revoke_installation_token,
      resource: current_integration,
      installation: current_integration_installation,
      challenge: true,
      allow_integrations: true,
      allow_user_via_integration: false

    receive_with_schema("installation", "revoke-token")

    authentication_token = AuthenticationToken.active.with_unhashed_token(request_credentials.token).first
    record_or_404(authentication_token)

    authentication_token.destroy

    deliver_empty(status: 204)
  end

  # List repositories for the authenticated user within an installation.
  get "/user/installations/:installation_id/repositories" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = "/rest/reference/apps#list-repositories-accessible-to-the-user-access-token"

    set_forbidden_message USER_OR_USER_VIA_GITHUB_APP_ACCESS_ONLY + "list repositories for an installation."

    @accepted_scopes = %w(user read:user)

    installation = find_installation!

    control_access :list_installation_repositories_for_user,
      resource: current_user,
      installation: installation,
      organization: installation.target.organization? && installation.target,
      challenge: true,
      allow_integrations: false,
      allow_user_via_integration: true

    repositories = Repository.with_ids(current_user.associated_repository_ids(repository_ids: installation.repository_ids))
    total_count = repositories.count

    repositories = paginate_rel(repositories.sorted_by(:full_name, "asc"))
    GitHub::PrefillAssociations.for_repositories(repositories)
    GitHub::PrefillAssociations.fill_licenses(repositories)

    deliver :integration_installation_repositories_hash,
      { repositories: repositories, total_count: total_count }
  end

  # Add a repository to an installation
  put "/user/installations/:installation_id/repositories/:repository_id" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = "/rest/reference/apps#add-a-repository-to-an-app-installation"
    @accepted_scopes = %w(repo)

    repo = find_repo!
    installation = find_installation!

    if can_access_repo_but_may_not_be_able_to_manage?(repo)
      forbidden_message = build_forbidden_action_on_repo_message(
        actor: current_user,
        repo: repo,
        installation: installation,
        action: :add_repositories,
      )
      set_forbidden_message forbidden_message
    end

    control_access :installation_repo_admin,
      installation: installation,
      resource: repository = repo,
      allow_integrations: false,
      allow_user_via_integration: false

    if installation.installed_on_all_repositories? || installation.repository_ids.include?(repository.id)
      # Introducing strict validation of the installation-repository.add
      # JSON schema would cause breaking changes for integrators
      # skip_validation until a rollout strategy can be determined
      # see: https://github.com/github/ecosystem-api/issues/1555
      receive_with_schema("installation-repository", "add", skip_validation: true)
      return deliver_empty(status: 204)
    end

    begin
      result = installation.edit(
        editor: current_user,
        repositories: installation.repositories.to_a << repository,
      )
    rescue InstallationError => e
      deliver_error!(422, message: e.to_s)
    end

    if result.success?
      # Introducing strict validation of the installation-repository.add
      # JSON schema would cause breaking changes for integrators
      # skip_validation until a rollout strategy can be determined
      # see: https://github.com/github/ecosystem-api/issues/1555
      receive_with_schema("installation-repository", "add", skip_validation: true)
      return deliver_empty(status: 204)
    end

    deliver_error!(422, message: result.error)
  end

  put "/installations/:installation_id/repositories/:repository_id" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = Platform::NotDocumentedBecause::DEPRECATED

    if GitHub.flipper[:github_apps_brown_out].enabled?
      # TODO: Update this with the brown out blog post
      # @documentation_url = Platform::NotDocumentedBecause::DEPRECATED
      deliver_error!(missing_repository_status_code)
    end

    @accepted_scopes = %w(repo)

    repo = find_repo!

    if can_access_repo_but_may_not_be_able_to_manage?(repo)
      set_forbidden_message USER_ACCESS_ONLY + "manage repositories for an installation."
    end

    control_access :installation_repo_admin,
      installation: installation = find_installation!,
      resource: repository = repo,
      allow_integrations: false,
      allow_user_via_integration: false

    return deliver_empty(status: 204) if installation.installed_on_all_repositories?
    return deliver_empty(status: 204) if installation.repository_ids.include?(repository.id)

    begin
      result = installation.edit(
        editor: current_user,
        repositories: installation.repositories.to_a << repository,
      )
    rescue InstallationError => e
      deliver_error!(422, message: e.to_s)
    end

    return deliver_empty(status: 204) if result.success?

    deliver_error!(422, message: result.error)
  end

  # Remove a repository from an installation
  delete "/user/installations/:installation_id/repositories/:repository_id" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = "/rest/reference/apps#remove-a-repository-from-an-app-installation"
    @accepted_scopes = %w(repo)

    repo = find_repo!
    installation = find_installation!

    if can_access_repo_but_may_not_be_able_to_manage?(repo)
      forbidden_message = build_forbidden_action_on_repo_message(
        actor: current_user,
        repo: repo,
        installation: installation,
        action: :remove_repositories,
      )
      set_forbidden_message forbidden_message
    end

    control_access :installation_repo_admin,
      installation: installation,
      resource: repository = repo,
      allow_integrations: false,
      allow_user_via_integration: false

    unless installation.repository_ids.include?(repository.id)
      # Introducing strict validation of the installation-repository.delete
      # JSON schema would cause breaking changes for integrators
      # skip_validation until a rollout strategy can be determined
      # see: https://github.com/github/ecosystem-api/issues/1555
      receive_with_schema("installation-repository", "delete", skip_validation: true)
      return deliver_empty(status: 204)
    end

    begin
      if installation.repository_ids.count == 1
        raise InstallationError, "Cannot remove the last repository from this installation."
      end

      result = installation.edit(
        editor: current_user,
        repositories: installation.repositories - [repository],
      )
    rescue InstallationError => e
      deliver_error!(422, message: e.to_s)
    end

    if result.success?
      # Introducing strict validation of the installation-repository.delete
      # JSON schema would cause breaking changes for integrators
      # skip_validation until a rollout strategy can be determined
      # see: https://github.com/github/ecosystem-api/issues/1555
      receive_with_schema("installation-repository", "delete", skip_validation: true)
      return deliver_empty(status: 204)
    end

    deliver_error!(422, message: result.error)
  end

  delete "/installations/:installation_id/repositories/:repository_id" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = Platform::NotDocumentedBecause::DEPRECATED

    if GitHub.flipper[:github_apps_brown_out].enabled?
      # TODO: Update this with the brown out blog post
      # @documentation_url = Platform::NotDocumentedBecause::DEPRECATED
      deliver_error!(missing_repository_status_code)
    end

    @accepted_scopes = %w(repo)

    repo = find_repo!

    if can_access_repo_but_may_not_be_able_to_manage?(repo)
      set_forbidden_message USER_ACCESS_ONLY + "manage repositories for an installation."
    end

    control_access :installation_repo_admin,
      installation: installation = find_installation!,
      resource: repository = repo,
      allow_integrations: false,
      allow_user_via_integration: false

    return deliver_empty(status: 204) unless installation.repository_ids.include?(repository.id)

    begin
      if installation.repository_ids.count == 1
        raise InstallationError, "Cannot remove the last repository from this installation."
      end

      result = installation.edit(
        editor: current_user,
        repositories: installation.repositories - [repository],
      )
    rescue InstallationError => e
      deliver_error!(422, message: e.to_s)
    end

    return deliver_empty(status: 204) if result.success?

    deliver_error!(422, message: result.error)
  end

  private

  def build_forbidden_action_on_repo_message(actor:, repo:, installation:, action:)
    check = IntegrationInstallation::Permissions.check(
      installation: installation,
      actor: actor,
      action: action,
      repositories: [repo],
    )

    # Regardless of the check state, error_message is always buildable
    check.error_message
  end

  def find_installation!(installation_id = params[:installation_id])
    installation = logged_in? && IntegrationInstallation.
      third_party.
      with_user(current_user).
      find_by_id(installation_id)
    record_or_404(installation)
  end

  # See https://github.com/github/github/pull/108438 for context and follow ups
  def can_access_repo_but_may_not_be_able_to_manage?(repo)
    return true if repo.public?
    repo.pullable_by?(current_user) && scope?(current_user, "repo")
  end
end
