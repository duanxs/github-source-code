# rubocop:disable Style/FrozenStringLiteralComment

class Api::DeploymentStatuses < Api::App
  areas_of_responsibility :commit_statuses, :api

  # Create a DeploymentStatus
  #
  # DeploymentsStatus are meant to be immutable.  If a Deployment has multiple
  # deployment statuses, the deployer should just create a new deployment status.
  #
  # To create a deployment status, you must have push rights to the repo. For
  # OAuth access, you must also have the appropriate scopes.
  post "/repositories/:repository_id/deployments/:deployment_id/statuses" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/repos#create-a-deployment-status"
    control_access :write_deployment_status,
      resource: deployment = find_repo_deployment!,
      allow_integrations: true,
      allow_user_via_integration: true

    GitHub.dogstats.time("deployment_status.time", tags: ["action:create"]) do
      attrs = [:state, :description, :environment_url, :target_url]
      attrs.concat([:environment_url, :log_url, :auto_inactive]) if medias.api_preview?(:deployment_enhancements)
      attrs.concat([:environment]) if medias.api_preview?(:deployment_status_enhancements)

      data = receive(Hash)
      data = attr(data, *attrs)

      # For compatibility we allow both `target_url` and `log_url` to be
      # passed in. But the DeploymentStatus model only accepts `log_url`.
      target_url = data.delete("target_url")
      data["log_url"] ||= target_url

      if medias.api_preview?(:deployment_enhancements) && data["auto_inactive"].nil?
        data["auto_inactive"] = true
      end
      auto_inactive = data.delete("auto_inactive")

      deployment_status = deployment.statuses.build(data)
      deployment_status.creator = current_user

      if integration_user_request?
        deployment_status.performed_via_integration = current_integration
      end

      invalid_state = (deployment_status.state == "inactive" && !medias.api_preview?(:deployment_enhancements))

      # Preview state used without preview header provided
      if !medias.api_preview?(:deployment_status_enhancements) &&
        DeploymentStatus::PREVIEW_STATES.include?(deployment_status.state)
        invalid_state = true
      end

      if !invalid_state && deployment_status.save
        GitHub.dogstats.increment("deployment_status", tags: ["action:create", "valid:true"])
        if medias.api_preview?(:deployment_enhancements) && auto_inactive
          include_production = medias.api_preview?(:deployment_status_enhancements)
          CreateAutoInactiveDeploymentStatuses.perform_later(deployment, include_production)
        end

        # Introducing strict validation of the deployment-status.create
        # JSON schema would cause breaking changes for integrators.
        # skip_validation until a rollout strategy can be determined
        # TODO: replace receive with receive_with_schema
        # see: https://github.com/github/ecosystem-api/issues/1555
        _ = receive_with_schema("deployment-status", "create", skip_validation: true)
        deliver :deployment_status_hash, deployment_status, status: 201
      else
        GitHub.dogstats.increment("deployment_status", tags: ["action:create", "valid:false"])
        error_message = if invalid_state
          # Copy the model validation error for the preview states so they
          # aren't exposed yet with a different error.
           [{
            resource: "DeploymentStatus",
            code: "custom",
            field: "state",
            message: "state is not included in the list",
          }]
        else
          deployment_status.errors
        end

        if !medias.api_preview?(:deployment_enhancements) &&
            error_message.respond_to?(:messages) &&
            error_message.messages[:log_url].present?

          log_url = error_message.delete(:log_url)
          if GitHub.rails_6_0?
            error_message.messages[:target_url] = log_url
          else
            error_message.add(:target_url, log_url.first)
          end
        end

        deliver_error 422, errors: error_message
      end
    end
  end

  # Get statuses for a Deployment.
  get "/repositories/:repository_id/deployments/:deployment_id/statuses" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/repos#list-deployment-statuses"
    control_access :read_deployment_status,
      resource: deployment = find_repo_deployment!,
      allow_integrations: true,
      allow_user_via_integration: true

    deployment_statuses = paginate_rel(deployment.statuses)
    GitHub::PrefillAssociations.for_deployment_statuses(deployment_statuses, deployment.repository)

    deliver :deployment_status_hash, deployment_statuses
  end

  # Get a status for a Deployment.
  get "/repositories/:repository_id/deployments/:deployment_id/statuses/:status_id" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/repos#get-a-deployment-status"

    deliver_preview_access_error!(feature: "Deployments API enhancement") unless medias.api_preview?(:deployment_enhancements)

    control_access :read_deployment_status,
      resource: deployment = find_repo_deployment!,
      allow_integrations: true,
      allow_user_via_integration: true

    deployment_status = find_repo_deployment_status!
    GitHub::PrefillAssociations.for_deployment_statuses([deployment_status], deployment.repository)

    deliver :deployment_status_hash, deployment_status
  end

  # Halts with a 404 if no Repository is found.
  # Returns a Repository instance, or nil.
  def find_repo_deployment!
    repo = find_repo!
    deployment = repo.deployments.where(id: params[:deployment_id]).first
    record_or_404(deployment)
  end

  def find_repo_deployment_status!
    deployment = find_repo_deployment!
    deployment_status = deployment.statuses.where(id: params[:status_id]).first
    record_or_404(deployment_status)
  end
end
