# rubocop:disable Style/FrozenStringLiteralComment

require "will_paginate/array"

class Api::Licenses < Api::App

  areas_of_responsibility :api

  def find_license!
    license = License.find_by_key(params[:key], hidden: true, psuedo: false)
    record_or_404(license)
  end

  get "/licenses" do
    @route_owner = Platform::NoOwnerBecause::UNAUDITED
    @documentation_url = "/rest/reference/licenses#get-all-commonly-used-licenses"
    control_access :public_site_information, resource: Platform::PublicResource.new, allow_integrations: true, allow_user_via_integration: true

    featured = params[:featured].present? ? parse_bool(params[:featured]) : nil
    licenses = License.all(featured: featured)
    deliver :license_hash, licenses.paginate(pagination), status: 200
  end

  LicenseQuery = PlatformClient.parse <<-'GRAPHQL'
    query($key: String!) {
      license(key: $key) {
        ...Api::Serializer::LicensesDependency::FullLicenseFragment
      }
    }
  GRAPHQL

  get "/licenses/:key" do
    @route_owner = Platform::NoOwnerBecause::UNAUDITED
    @documentation_url = "/rest/reference/licenses#get-a-license"
    control_access :public_site_information, resource: Platform::PublicResource.new, allow_integrations: true, allow_user_via_integration: true

    deliver :license_hash, find_license!,
      full: true,
      compare_payload_to: :license_candidate
  end

  def license_candidate(license, options)
    variables = {
      key: license.key,
    }

    results = platform_execute(LicenseQuery, variables: variables)
    license = results.data.license
    Api::Serializer.serialize(:graphql_full_license_hash, license, options)
  end
end
