# rubocop:disable Style/FrozenStringLiteralComment

# Provides a fingerprint identifying the actor originating a request based on
# authorization information (if any) and the remote IP.
class Api::RequestAuthenticationFingerprint

  class Actor
    attr_reader :fingerprint, :limit_multiplier

    def initialize(fingerprint:, limit_multiplier: 1)
      @fingerprint = fingerprint
      @limit_multiplier = limit_multiplier
    end
  end

  DELIMITER  = ":".freeze
  TOKEN_KEYS = %w(bearer_token access_token oauth_token)
  INVALID_USERNAME_PLACEHOLDER = "@invalid".freeze
  OAUTH_BASIC_PLACEHOLDER = "x-oauth-basic".freeze

  attr_reader :authorization
  attr_reader :request

  def initialize(env)
    @authorization = Rack::Auth::Basic::Request.new(env)
    @request       = Rack::Request.new(env)
  end

  def self.from(env)
    new(env)
  end

  def actor
    @actor ||= detect_actor
  end

  def limit_multiplier
    actor.limit_multiplier
  end

  def to_s
    actor.fingerprint
  end

  private

  # Internal: Determine an actor from an HTTP request, checking each
  # form of authorization we support, falling back to the remote IP if no
  # authorization info is available.
  #
  # Returns a String.
  def detect_actor
    token || app || user || integration || ip
  end

  # Internal: Return an actor for any request using an access token
  # with a fingerprint in the format of `token:<last 8 chars of token>`.
  #
  # Returns an Actor or nil if the request is not using a token.
  def token
    token = if authorization_valid?(authorization)
      auth_username = authorization.username.to_s
      if authorization.basic? && auth_username.length == 40
        auth_username
      elsif authorization.basic? &&
        auth_username == OAUTH_BASIC_PLACEHOLDER &&
        authorization.credentials[1].length == 40
        authorization.credentials[1]
      else
        authorization.params
      end
    else
      request.GET.slice(*TOKEN_KEYS).values.compact.first
    end

    token = token.to_s

    return unless token.valid_encoding?
    return unless token =~ OauthAccess::TOKEN_PATTERN || token =~ AuthenticationToken::TOKEN_PATTERN_V1

    Actor.new(fingerprint: build("token", token.last(8), request.ip))
  end

  # Internal: Return an actor for any request using an access token
  # with a fingerprint in the format of `app:<client_id>:<remote ip>`.
  #
  # Returns an Actor or nil if the request is not using application authentication.
  def app
    key = if authorization_valid?(authorization) && authorization.basic?
      if authorization.credentials.map(&:size) == [20, 40]
        authorization.username
      end
    else
      request.GET["client_id"]
    end

    return unless key
    return if !authorization_valid?(authorization) && (GitHub.flipper[:ncc_remediation_brown_out].enabled? && request.GET["oauth_credential_ratelimit_increase"].nil?)

    Actor.new(fingerprint: build("app", key, request.ip))
  end

  # Internal: Return an actor for any rany user Basic Auth request
  # with a fingerprint in the format of `user:<login>:<remote ip>`.
  #
  # Returns an Actor or nil if the request is not using application authentication.
  def user
    if authorization_valid?(authorization) && authorization.basic?
      # This is *probably* a user logging in with basic auth via their login and password.
      # But, it could actually be a user logging in via OAuth and a personal access token as their
      # password.
      username = if User::LOGIN_REGEX =~ authorization.username
        authorization.username
      end

      return unless username

      Actor.new(fingerprint: build("user", username, request.ip))
    end
  end

  # Internal: Return a unique key for any GitHub App (Integration) JWT Auth request
  # in the format of `integration:<id>:<remote ip>`.
  #
  # Returns a string or nil if the request Authorization header is not using
  # the "Bearer" scheme and if the JWT is invalid.
  def integration
    if authorization_valid?(authorization) && authorization.scheme == "bearer"
      assertion = Api::IntegrationAssertion.new(request.env)
      if assertion.valid?
        Actor.new(
          fingerprint: build("integration", assertion.integration.id, request.ip),
          limit_multiplier: assertion.integration.abuse_limits_multiplier,
        )
      end
    end
  end

  def ip
    Actor.new(fingerprint: request.ip)
  end

  def build(*parts)
    parts.join(DELIMITER)
  end

  # Internal: This method ensures that an auth header is valid before processing it.
  # It checks to see if a header exists, and it ensures that the value of that header
  # is non-nil.
  #
  # Returns a Boolean true/false.
  def authorization_valid?(authorization)
    # TODO: remove this if https://git.io/v2jQP ever merges and comes into GitHub
    authorization.provided? && authorization.params
  rescue NoMethodError
    false
  end
end
