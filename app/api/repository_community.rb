# frozen_string_literal: true

class Api::RepositoryCommunity < Api::App
  areas_of_responsibility :api, :community_and_safety

  before do
    deliver_error!(404) unless GitHub.community_profile_enabled?
  end

  # Access community health data for a repository
  get "/repositories/:repository_id/community/profile" do
    @route_owner = "@github/communities-heart-reviewers"
    @documentation_url = "/rest/reference/repos#get-community-profile-metrics"
    repo = find_repo!
    deliver_error!(404) unless repo.public? && !repo.fork?
    control_access :view_community_profile, resource: repo, allow_integrations: true, allow_user_via_integration: true
    community_profile = CommunityProfile.where(repository_id: repo.id).includes(:repository).first
    community_profile ||= CommunityProfile.new(repository: repo)
    deliver :community_profile_hash, community_profile
  end

  # Get a repository's CoC
  get "/repositories/:repository_id/community/code_of_conduct" do
    @route_owner = "@github/communities-heart-reviewers"
    @documentation_url = "/rest/reference/codes-of-conduct#get-the-code-of-conduct-for-a-repository"
    require_preview(:code_of_conduct)
    control_access :get_code_of_conduct, resource: repo = find_repo!, allow_integrations: true, allow_user_via_integration: true
    deliver :code_of_conduct_hash, repo.code_of_conduct, full: true
  end
end
