# rubocop:disable Style/FrozenStringLiteralComment

class Api::MonaLisaOctocat < Api::App
  areas_of_responsibility :api

  get "/octocat" do
    @route_owner = "@github/ecosystem-api"
    @documentation_url = Platform::NotDocumentedBecause::WE_FORGOT
    control_access :apps_audited,
      resource: Platform::PublicResource.new,
      allow_integrations: true,
      allow_user_via_integration: true

    content_type(user_agent.browser? ? "text/plain" : GitHub::OCTOCAT_CONTENT_TYPE)
    if params[:s] =~ /\A[\w\- ,\/]*\z/
      GitHub.octocat(params[:s])
    else
      GitHub.octocat
    end
  end

  get "/zen" do
    @route_owner = "@github/ecosystem-api"
    @documentation_url = "/rest/guides/getting-started-with-the-rest-api#hello-world"
    control_access :apps_audited,
      resource: Platform::PublicResource.new,
      allow_integrations: true,
      allow_user_via_integration: true

    content_type "text/plain"
    GitHub.random_zen
  end
end
