# rubocop:disable Style/FrozenStringLiteralComment

class Api::RepositoryCollaborators < Api::App

  # List collaborators on a repository.
  get "/repositories/:repository_id/collaborators" do
    @route_owner = "@github/iam"
    @documentation_url = "/rest/reference/repos#list-repository-collaborators"
    repo = find_repo!
    if repo.public?
      set_forbidden_message "Must have push access to view repository collaborators."
    end
    control_access :list_collaborators,
      resource: repo,
      challenge: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true

    # Since we're paginating the list of IDs rather than the scope itself, we
    # need to use WillPaginate::Collection#replace in order to ensure the
    # pagination link headers are correctly added to the response.
    if params[:affiliation] == "outside"
      members = repo.outside_collaborators_ids.sort.paginate(pagination)
    elsif params[:affiliation] == "direct"
      members = repo.direct_member_ids.sort.paginate(pagination)
    else
      members = repo.direct_or_team_member_ids(
        viewer: current_user,
        immediate_only: false,
      ).sort.paginate(pagination)
    end
    members.replace(User.where(id: members))

    GitHub::PrefillAssociations.for_profiles(members) if medias.api_param?("user-identity")

    deliver :collaborator_hash, members, repo: repo
  end

  # Get if user is a collaborator on a repository.
  get "/repositories/:repository_id/collaborators/:username" do
    @route_owner = "@github/iam"
    @documentation_url = "/rest/reference/repos#check-if-a-user-is-a-repository-collaborator"
    repo = find_repo!
    collab = User.find_by_login(params[:username])
    if !collab
      deliver_error! 404,
        message: "#{params[:username]} is not a user",
        documentation_url: "/v3/repos/collaborators/#get"
    end

    if repo.public?
      set_forbidden_message "Must have push access to view repository collaborators."
    end
    control_access :list_collaborators,
      resource: repo,
      collab: collab,
      challenge: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true

    collab_is_direct_or_team_member = repo.direct_or_team_member_ids(
      viewer: current_user,
      immediate_only: false,
    ).include?(collab.id)

    if collab_is_direct_or_team_member
      deliver_empty(status: 204)
    else
      deliver_error 404
    end
  end

  # Get the given user's highest permission level on the given repository
  get "/repositories/:repository_id/collaborators/:username/permission" do
    @route_owner = "@github/iam"
    @documentation_url = "/rest/reference/repos#get-repository-permissions-for-a-user"
    repo = find_repo!
    collab = User.find_by_login(params[:username])

    if repo.public?
      set_forbidden_message "Must have push access to view collaborator permission."
    end

    control_access :see_user_permission,
                   resource: repo,
                   collab: collab,
                   challenge: repo.public?,
                   allow_integrations: true,
                   allow_user_via_integration: true

    if collab.blank?
      deliver_error! 404,
                     message: "#{params[:username]} is not a user",
                     documentation_url: @documentation_url
    end

     permission =
      if GitHub.flipper[:fgp_api_v2].enabled?(current_repo.owner)
        repo.direct_role_for(collab) || :none
      else
        repo.access_level_for(collab) || :none
      end

    deliver(:repository_collaborator_permission, permission, user: collab)
  end

  # Add a user as a collaborator.
  put "/repositories/:repository_id/collaborators/:username" do
    @route_owner = "@github/iam"
    @documentation_url = "/rest/reference/repos#add-a-repository-collaborator"
    repo = find_repo!
    collaborator = User.find_by_login(params[:username])

    control_access :add_collaborator,
                   resource: repo,
                   collab: collaborator,
                   allow_integrations: true,
                   allow_user_via_integration: true

    ensure_repo_writable!(repo)

    # Introducing strict validation of the repositry-collaborator.add
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    data = receive_with_schema("repository-collaborator", "add", skip_validation: true)
    permission = data && (data["permission"] || data["permissions"])
    permission ||= "push"

    unless collaborator.user?
      deliver_error! 404,
        message: "#{params[:username]} is not a user",
        documentation_url: "/rest/reference/repos#add-a-repository-collaborator"
    end

    action = begin
                  Repository.permission_to_action(permission)
               rescue ArgumentError
                  nil
               end
    if repo.member?(collaborator)
      repo.update_member(collaborator, action: action)
      return deliver_empty(status: 204)
    end

    invitation = RepositoryInvitation.where(invitee_id: collaborator.id, repository_id: repo.id).first
    unless invitation
      result = RepositoryInvitation.invite_to_repo(collaborator, current_user, repo, action: action || :write)
      unless result[:success]
        deliver_error!(
          422,
          errors: result[:errors],
          documentation_url: "/rest/reference/repos#add-a-repository-collaborator",
        )
      end
      invitation = result[:invitation]
    end

    return deliver_empty(status: 204) unless invitation

    GitHub::PrefillAssociations.for_repository_invitations([invitation])
    deliver :repository_invitation_hash, invitation, status: 201
  end

  # Remove a user as a collaborator.
  delete "/repositories/:repository_id/collaborators/:username" do
    @route_owner = "@github/iam"

    # Introducing strict validation of the repository-collaborator.delete
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("repository-collaborator", "delete", skip_validation: true)

    @documentation_url = "/rest/reference/repos#remove-a-repository-collaborator"
    repo = find_repo!
    collab = User.find_by_login(params[:username])

    control_access :remove_collaborator,
                   resource: repo,
                   collab: collab,
                   allow_integrations: true,
                   allow_user_via_integration: true

    ensure_repo_writable!(repo)

    repo.remove_member collab

    deliver_empty(status: 204)
  end
end
