# rubocop:disable Style/FrozenStringLiteralComment

class Api::JavaScriptError < Api::Error
  def initialize(error)
    super error[:message] || error["message"]
    set_backtrace(error[:stack] || error["stack"])
  end
end
