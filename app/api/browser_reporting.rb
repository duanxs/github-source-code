# rubocop:disable Style/FrozenStringLiteralComment

class Api::BrowserReporting < Api::App
  areas_of_responsibility :browser_stats

  include GitHub::BrowserStatsHelper

  StatsQuery = PlatformClient.parse <<-'GRAPHQL'
    mutation($input: ReportBrowserMetricsInput!) {
      reportBrowserMetrics(input: $input)
    }
  GRAPHQL

  ErrorsQuery = PlatformClient.parse <<-'GRAPHQL'
    mutation($input: ReportBrowserErrorInput!) {
      reportBrowserError(input: $input)
    }
  GRAPHQL

  before do
    disable_caching!
    disable_hydro_request_logging
    # Still set headers even though we don't apply rate limiting
    custom_throttler = Api::ConfigThrottler.new(
      Api::RateLimitConfiguration.for(Api::RateLimitConfiguration::DEFAULT_FAMILY, self),
      { amount: 0 },
    )
    set_rate_limit!(custom_throttler.check)
  end

  # Do not apply rate limiting to these internal APIs
  rate_limit_as nil

  post "/_private/browser/stats" do
    @route_owner = "@github/web-systems"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL
    response = platform_execute(StatsQuery, variables: { input: receive }, force_readonly: true)
    GitHub.dogstats.increment("browser.reporting_errors") if response.errors.all.any?
    deliver_raw("", status: 200, content_type: "text/plain")
  end

  post "/_private/browser/errors" do
    @route_owner = "@github/web-systems"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL
    error = receive
    deliver_error!(400) unless error.is_a?(Hash)

    if error["csp-report"]
      report_csp_error(error["csp-report"])
    elsif error["expect-ct-report"]
      report_certificate_transparency_error(error["expect-ct-report"])
    elsif proxy_error = error[GitHub::ProxyDetection::PARAM]
      GitHub::ProxyDetection.report_proxy_site(proxy_error)
    else
      response = platform_execute(ErrorsQuery, variables: { input: error }, force_readonly: true)
      GitHub.dogstats.increment("browser.reporting_errors") if response.errors.all.any?
    end

    deliver_raw("", status: 200, content_type: "text/plain")
  end

  private
  def report_csp_error(report)
    # We don't care about CSP violations caused by extension scripts.
    if csp_report_noise?(report)
      GitHub.dogstats.increment("content_security_policy", tags: ["error:plugin_noise"])
      return
    end

    document_host = Addressable::URI.parse(report["document-uri"]).host

    # Ignore reports from dynamic labs
    return if GitHub.dynamic_lab_domain?(document_host)

    # Pull out just the directive name (img-src, script-src, etc.).
    directive_name = report["violated-directive"].split(" ", 2).first

    # blocked-uri might be a full URL, just an origin, or just a scheme (like
    # "data"). Turn it into an origin or scheme.
    blocked_origin_or_scheme =
      if report["blocked-uri"].include?(":")
        # Looks like a URL or origin.
        Addressable::URI.parse(report["blocked-uri"]).origin
      else
        # Probably just a scheme.
        report["blocked-uri"]
      end

    error = {
      "message" => "[%s] Directive \"%s\" blocked \"%s\"" % [document_host, directive_name, blocked_origin_or_scheme],
      "stack" => [[report["source-file"], report["line-number"], report["column-number"]].join(":")],
    }

    rollup = [
      Api::SecurityViolation,
      directive_name,
      blocked_origin_or_scheme,
      document_host,
      parsed_useragent.name,
    ].join("")

    context = {
      app: "github-csp",
      params: {},
      directive_name: directive_name,
      browser: parsed_useragent.name,
      document_host: document_host,
      rollup: Digest::MD5.hexdigest(rollup),
    }.reverse_merge(report)
    GitHub.dogstats.increment("browser_security_reports", tags: ["report_type:csp", "browser:#{parsed_useragent.name}"])
    Failbot.report!(Api::SecurityViolation.new(error), context)
  rescue Addressable::URI::InvalidURIError
    # nbd, could be garbage
    GitHub.dogstats.increment("content_security_policy", tags: ["error:invalid_report"])
  end

  # Attempt to detect reports from extensions and other unwanted sources.
  #
  # Returns true if we think the report comes from a plugin.
  def csp_report_noise?(report)
    source_file = report["source-file"]
    blocked_uri = report["blocked-uri"]
    script_sample = report["script-sample"]
    source_file.try(:start_with?, "chrome-extension://") ||
      blocked_uri.try(:start_with?, "safari-extension://") ||
      source_file.try(:start_with?, "safari-extension://") ||
      script_sample.try(:include?, "lastpass_iter")

  end

  def report_certificate_transparency_error(report)
    reported_hostname = report["hostname"]
    rollup = [
      Api::SecurityViolation,
      reported_hostname,
      parsed_useragent.name,
    ].join("")

    context = {
      "app" => "github-expect-ct",
      "params" => {},
      "browser" => parsed_useragent.name,
      "rollup" => Digest::MD5.hexdigest(rollup),
    }.reverse_merge(report)

    error = {
      "message" => "Expect-CT violation for #{reported_hostname} using #{parsed_useragent.name}:#{parsed_useragent.version}",
    }

    GitHub.dogstats.increment("browser_security_reports", tags: ["report_type:expect_ct", "browser:#{parsed_useragent.name}"])
    Failbot.report!(Api::SecurityViolation.new(error), context)
  end

  # Override this method from Api::App to not require authentication on garage.
  def protect_access_to_garage_hosts
    # noop
  end

  # Override this method from Api::App to not require authentication on enterprise.
  def protect_access_to_enterprise_hosts
    # noop
  end

  # Disable browser caching for this API.
  def disable_caching!
    cache_control "no-cache"
  end
end
