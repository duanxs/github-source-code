# rubocop:disable Style/FrozenStringLiteralComment

class Api::Pulls < Api::App
  map_to_service :pull_requests
  map_to_service :merge, only: [
    "GET /repositories/:repository_id/pulls/:pull_number/merge",
    "POST /repositories/:repository_id/pulls/:pull_number/merge"
  ]

  statsd_tag_actions "/repositories/:repository_id/pulls"
  statsd_tag_actions "/repositories/:repository_id/pulls/:pull_number"
  statsd_tag_actions "/repositories/:repository_id/pulls/:pull_number/merge"
  statsd_tag_actions "/repositories/:repository_id/pulls/:pull_number/update-branch"

  # List pull requests for this repository
  get "/repositories/:repository_id/pulls" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/pulls#list-pull-requests"
    control_access :list_pull_requests, repo: repo = find_repo!,
      allow_integrations: true,
      allow_user_via_integration: true,
      approved_integration_required: false

    indifferent_params = HashWithIndifferentAccess.new(params.merge(pagination))

    pulls = repo.pull_requests.filter_spam_for(current_user)

    if head_label = params[:head]
      head_ref, head_owner = head_label.split(":").reverse
      if head_owner
        head_repo = Repository.nwo("#{head_owner}/#{repo.name}")
        pulls = pulls.for_head_repo_and_head_ref(head_repo, head_ref)
      end
    end

    if base_ref = params[:base]
      pulls = pulls.for_base_ref(base_ref)
    end

    # Internal API only for now
    if medias.accepts_version?(:greenstone)
      if params[:since]
        pulls = since_filter(pulls)
      end
    end

    pulls = paginate_rel(pulls.filtered_and_ordered(indifferent_params))

    GitHub.dogstats.time "prefill", tags: ["via:api", "action:pulls_list"] do
      GitHub::PrefillAssociations.for_pulls(pulls, mirror: true)
      deliver :pull_request_hash, pulls, repo: repo
    end
  end

  # Create a pull request
  post "/repositories/:repository_id/pulls" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/pulls#create-a-pull-request"

    control_access :create_pull_request,
      repo: repo = find_repo!,
      enforce_oauth_app_policy: current_repo.private?,
      allow_integrations: true,
      allow_user_via_integration: true
    authorize_content(:create, repo: repo)

    # TODO: replace `legacy-create-legacy` with `create`
    # Introducing strict validation of the pull-request.create
    # JSON schema would cause breaking changes for integrators
    # Use pull-request.legacy-create-legacy until a rollout strategy
    # can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    data = receive_with_schema("pull-request", "legacy-create-legacy")

    attributes = attr(data, :title, :body, :base, :head, :maintainer_can_modify, :draft).
      update("user" => current_user)

    if attributes["draft"]
      # Gather metrics for values submitted for the draft parameter, as
      # a precursor for enforcing that the submitted data type be boolean.
      draft_value_type = :other
      case attributes["draft"]
      when TrueClass, FalseClass
        draft_value_type = :boolean
      when String
        if %w(true false).include?(attributes["draft"])
          draft_value_type = :string
        end
      end
      GitHub.dogstats.increment("api_create_pull.draft_param", tags: ["type:#{draft_value_type}"], sample_rate: 0.1)
    end

    if attributes["draft"] && !repo.plan_supports?(:draft_prs)
      deliver_error! 422,
        message: "Draft pull requests are not supported in this repository."
    end

    # TODO Move this validation to the JSON schema, once it can provide a better
    # error message. (http://git.io/J6aVVA)
    # Note: A patch was submitted to improve the error message (https://github.com/brandur/json_schema/pull/26),
    # but it's still not clear enough for our purposes.
    if data.key?("issue") && data.key?("title")
      deliver_error! 422,
        message: "You may only provide an issue number or a title string, but not both."
    end

    if (num = data["issue"].to_i) > 0
      log_data.update(alternative_input: true)

      if issue = repo.issues.find_by_number(num)

        # disallow removing existing issue -> pull request relationship.
        if issue.pull_request?
          deliver_error! 422,
            errors: [api_error(:PullRequest, :issue, :invalid, value: num)],
            message: "The specified issue is already attached to a pull request."
        end

        has_access = Api::AccessControl.access_allowed? \
            resource: issue,
            user: current_user,
            repo: repo,
            verb: :edit_issue
        if !has_access
          deliver_error! 422,
            errors: [api_error(:PullRequest, :issue, :unauthorized, value: num)],
            message: "You do not have permission to attach the specified issue."
        end

        attributes[:issue] = issue
      else
        deliver_error! 422,
          errors: [api_error(:PullRequest, :issue, :invalid, value: num)],
          message: "The specified issue does not exist."
      end
    end

    if attributes[:base].blank?
      deliver_error! 422,
      errors: [api_error(:PullRequest, :base, :missing_field)]
    end

    if attributes[:base].to_s.include?(":")
      deliver_error! 422,
        errors: [api_error(:PullRequest, :base, :invalid)]
    end

    if attributes[:maintainer_can_modify] == false
      attributes[:collab_privs] = attributes.delete(:maintainer_can_modify)
    else
      attributes[:maintainer_can_modify] = true
    end

    # TODO: Remove this special-case header, see: https://github.com/github/dependabot-api/issues/526
    if current_integration&.dependabot_github_app?
      attributes[:dependabot_update_id] = request.env["HTTP_X_DELTAFORCE_DEPENDENCY_UPDATE_ID"]
    end

    begin
      pull = PullRequest.create_for(repo, attributes)
    rescue ActiveRecord::RecordInvalid => e
      deliver_error! 422,
        errors: e.record.errors
    end

    if pull.persisted?
      if !pull.head_repository.heads.read(pull.head_ref).exist?
        log_data[:pull_request_non_branch_head] = pull.head
        err = PullRequest::NonBranchHeadError.new(pull.head)
        err.set_backtrace(caller)
        Failbot.report_user_error(err, fatal: "NO (just reporting)", repo_id: pull.head_repository_id)
      end

      pull.enqueue_mergeable_update

      GitHub::PrefillAssociations.for_pulls([pull])
      deliver :pull_request_hash, pull, { full: true, status: 201 }
    else
      branch_errors = []
      if pull.errors[:base_ref].any?
        branch_errors << api_error(:PullRequest, :base, :invalid)
      end
      if pull.errors[:head_ref].any?
        branch_errors << api_error(:PullRequest, :head, :invalid)
      end

      deliver_error 422,
        errors: branch_errors.any? ? branch_errors : pull.errors
    end
  end

  ShowPullRequestQuery = PlatformClient.parse <<-'GRAPHQL'
    query($id:ID!, $includeBody: Boolean!, $includeBodyHTML: Boolean!, $includeBodyText: Boolean!, $includeFullTeamDetails: Boolean!) {
      pullRequest: node(id:$id) {
        ...Api::Serializer::PullsDependency::PullRequestFragment
      }
    }
  GRAPHQL

  def pull_request_candidate(pull, options = {})
    variables = {
      id: pull.global_relay_id,
      includeFullTeamDetails: false,
    }.update(graphql_mime_body_variables(options))

    results       = platform_execute(ShowPullRequestQuery, variables: variables)
    pull_request  = results.data.pull_request

    Api::Serializer.serialize(:graphql_pull_request_hash, pull_request, options)
  end

  # Perform a comparison between REST and GraphQL-serialized pull-request hashes.
  #
  # Removes merge_commit_sha key from compared hashes as we're not porting
  # that REST functionlity to GraphQL.
  #
  # Removes 3 'mergeable' keys if mergeable_state is unknown because of how these
  # fields are calculated.
  # Explanation: https://github.com/github/github/pull/87808#issuecomment-379798326
  def compare_pull_request_hashes(control, candidate)
    exempt_keys = ["merge_commit_sha"]

    if control[:mergeable_state] == :unknown || candidate[:mergeable_state] == :unknown
      exempt_keys += ["mergeable_state", "mergeable", "rebaseable"]
    end

    control_shape = api_response_shape(control)
    candidate_shape = api_response_shape(candidate)

    control_shape.except(*exempt_keys) == candidate_shape.except(*exempt_keys)
  end

  # Get a single pull request
  get "/repositories/:repository_id/pulls/:pull_number" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/pulls#get-a-pull-request"
    repo, pull = find_repo_and_pull_request

    if medias.api_param?(:diff)
      control_access :get_contents,
        resource: repo,
        path: nil,
        allow_integrations: true,
        allow_user_via_integration: true,
        approved_integration_required: false
    else
      control_access :get_pull_request,
        repo: repo,
        resource: pull,
        allow_integrations: true,
        allow_user_via_integration: true,
        approved_integration_required: false
    end

    GitHub::PrefillAssociations.for_pulls([pull], mirror: true)

    delivering_raw_diff_content(medias) do
      if medias.api_param?(:diff) || medias.api_param?(:patch)
        comparison = pull.historical_comparison
        if !comparison.diffs.available? || comparison.diffs.truncated_for_timeout?
          deliver_undiffable_error!(:PullRequest, comparison.diffs)
        end

        raw_comparison = if medias.api_param?(:diff)
          comparison.to_diff
        else
          comparison.to_patch
        end

        deliver_raw raw_comparison,
          content_type: "#{medias}; charset=utf-8",
          last_modified: calc_last_modified(pull)
      else
        # we calculate a new merge commit, if necessary, on read.
        # this is done asynchronously so the current request is not held up
        # but so that there is a chance future requests will have
        # knowledge of the pull's mergeability.
        pull.enqueue_mergeable_update

        deliver :pull_request_hash, pull, {
          full: true,
          repo: repo,
          last_modified: calc_last_modified(pull),
          current_user: current_user,
          compare_payload_to: :pull_request_candidate,
          compare_with: :compare_pull_request_hashes,
        }
      end
    end
  end

  # Update a pull request
  verbs :patch, :post, "/repositories/:repository_id/pulls/:pull_number" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/pulls#update-a-pull-request"
    repo, pull = find_repo_and_pull_request
    control_access :update_pull_request,
      repo: repo,
      resource: pull,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: repo.private?
    authorize_content(:update, repo: repo)

    # Introducing strict validation of the pull-request.update
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    data = receive_with_schema("pull-request", "update", skip_validation: true)

    # Assert that only the pull author may change the fork collaboration grant setting.
    if data.key?("maintainer_can_modify") && current_user != pull.user
      deliver_error! 422,
        resource: "PullRequest",
        errors: [
          api_error(:PullRequest, :maintainer_can_modify, :invalid,
            message: "Only the pull request author may change the maintainer_can_modify value"),
        ]
    end

    attributes = attr(data, :title)
    issue = pull.issue
    saved = if data["state"].to_s =~ /\Aclose/i && issue.open?
      issue.close(current_user, attributes)
    elsif data["state"].to_s =~ /\Aopen/i && issue.closed?
      issue.open(current_user, attributes)
    else
      GitHub::SchemaDomain.allowing_cross_domain_transactions { issue.update(attributes) }
    end

    if data.key?("body")
      issue.update_body(data["body"], current_user)
    end

    if data.key?("maintainer_can_modify")
      pull.fork_collab_state = data["maintainer_can_modify"] ? :allowed : :denied
      pull.save if pull.changed?
    end

    if data.key?("base")
      begin
        pull.change_base_branch(current_user, data["base"])
      rescue ArgumentError => e
        deliver_error! 422,
          resource: "PullRequest",
          errors: [
            api_error(:PullRequest, :base, :invalid,
              message: e.message),
          ]
      rescue PullRequest::BaseNotChangeableError => e
        deliver_error! 422,
          resource: "PullRequest",
          errors: [
            api_error(:PullRequest, :base, :invalid,
              message: e.ui_message),
          ]
      rescue Git::Ref::NotFound
        deliver_error! 422,
          resource: "PullRequest",
          errors: [
            api_error(:PullRequest, :base, :invalid,
              message: "Proposed base branch '#{data["base"]}' was not found"),
          ]
      end
    end

    if saved
      pull.enqueue_mergeable_update

      GitHub::PrefillAssociations.for_pulls([pull], mirror: true)

      deliver :pull_request_hash, pull, {
        full: true,
        repo: repo,
        current_user: current_user,
      }
    else
      errors = pull.errors.any? ? pull.errors : issue.errors
      deliver_error 422,
        resource: "PullRequest",
        errors: errors
    end
  end

  # Check if a pull request is merged
  get "/repositories/:repository_id/pulls/:pull_number/merge" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/pulls#check-if-a-pull-request-has-been-merged"
    repo, pull = find_repo_and_pull_request
    control_access :get_pull_request_merge_status,
      repo: repo,
      resource: pull,
      allow_integrations: true,
      allow_user_via_integration: true

    if pull.merged?
      deliver_empty(status: 204)
    else
      deliver_error 404
    end
  end

  # Merge a pull request
  put "/repositories/:repository_id/pulls/:pull_number/merge" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/pulls#merge-a-pull-request"

    repo, pull = find_repo_and_pull_request
    control_access :merge_pull_request,
      repo: repo,
      resource: pull,
      allow_integrations: true,
      allow_user_via_integration: true
    authorize_content(:merge, repo: repo)
    ensure_repo_writable!(repo)
    ensure_changed_files_writable!(repo, pull.diffs)

    data = receive_with_schema("pull-request", "merge-legacy")
    sha_param!("sha", data) if data.key?("sha")

    begin
      merged, status, failure = pull.merge(current_user,
        message: data["commit_message"],
        message_title: data["commit_title"],
        method: data["merge_method"].try(:to_sym) || :merge,
        reflog_data: request_reflog_data("pull request merge api").merge({ pr_author_login: pull.safe_user.login }),
        expected_head: data["sha"],
      )

      if merged
        deliver_raw(
          sha: status,
          merged: merged,
          message: "Pull Request successfully merged")
      else
        error_code = 405
        error_code = 409 if failure == :head_mismatch

        response = {
          sha: nil,
          merged: merged,
          message: status,
        }

        if failure == :protected_branch
          response[:documentation_url] = "#{GitHub.help_url}/articles/about-protected-branches"
        end

        deliver_error error_code, response
      end
    rescue Git::Ref::HookFailed => e
      deliver_error 422,
        message: "Could not merge because a Git pre-receive hook failed.\n\n#{e.message}",
        documentation_url: "#{GitHub.help_url}/articles/about-protected-branches"
    end
  end

  # Merge HEAD from upstream branch into pull request branch
  put "/repositories/:repository_id/pulls/:pull_number/update-branch" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/pulls#update-a-pull-request-branch"
    require_preview(:update_branch)
    repo, pull = find_repo_and_pull_request

    control_access(:update_pull_request,
      repo: repo,
      resource: pull,
      allow_integrations: true,
      allow_user_via_integration: true)

    authorize_content(:update, repo: repo)
    ensure_repo_writable!(repo)

    data = receive_with_schema("pull-request", "update-branch")

    params = {
      user: current_user,
      author_email: current_user&.default_author_email(pull.repository, pull.head_sha),
    }

    params[:expected_head_oid] = data["expected_head_sha"] if data["expected_head_sha"]

    begin
      pull.merge_base_into_head(**params)
    rescue PullRequest::MergeConflictError => e
      deliver_error!(422, message: e.message)
    rescue PullRequest::RefMismatch
      deliver_error!(422, message: "expected head sha didn’t match current head ref.")
    rescue PullRequest::HeadMissing => e
      deliver_error!(422, message: e.message)
    rescue PullRequest::PermissionError => e
      deliver_error!(422, message: e.message)
    rescue Git::Ref::ProtectedBranchUpdateError => e
      deliver_error!(422, message: e.message)
    end

    url = api_url("/repos/#{repo.name_with_owner}/pulls/#{pull.number}")
    message = "Updating pull request branch."
    response["location"] = url
    deliver_raw({ message: message, url: url }, status: 202)
  end

  # List pull request commits
  get "/repositories/:repository_id/pulls/:pull_number/commits" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/pulls#list-commits-on-a-pull-request"
    repo, pull = find_repo_and_pull_request
    control_access :list_pull_request_commits,
      repo: repo,
      resource: pull,
      allow_integrations: true,
      allow_user_via_integration: true

    opts = { repo: repo }
    opts.update(pagination)

    if (mimes = request.accept).present?
      opts[:accept_mime_types] = mimes
    end

    deliver_raw Api::Serializer.commits_array(pull.changed_commits, opts),
      last_modified: calc_last_modified(pull)
  end

  GetPullRequestFilesQuery = PlatformClient.parse <<-'GRAPHQL'
    query($id: ID!, $perPage: Int!) {
      node(id: $id) {
        ... on PullRequest {
          id
          diff {
            patches(last: $perPage) {
              edges {
                node {
                  ...Api::Serializer::RepositoriesDependency::PatchFragment
                }
              }
            }
          }
        }
      }
    }
  GRAPHQL

  def pull_request_files_candidate(pull, options = {})
    variables = {
      id: pull.global_relay_id,
      perPage: paginator.per_page || paginator.max_per_page,
    }

    results = platform_execute(GetPullRequestFilesQuery, variables: variables)
    files   = results.data.node.diff.patches.edges.map(&:node)

    Api::Serializer.serialize(:graphql_condensed_diff_entry_hash, files, options)
  end

  # List pull request files (diffs)
  get "/repositories/:repository_id/pulls/:pull_number/files" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/pulls#list-pull-requests-files"
    repo, pull = find_repo_and_pull_request
    control_access :list_pull_request_files,
      repo: repo,
      resource: pull,
      allow_integrations: true,
      allow_user_via_integration: true

    diff = pull.historical_comparison.init_diffs

    delta_index_from = pagination[:per_page] * (pagination[:page] - 1) # Subtract 1 page num since it's 1-indexed
    delta_index_to = (pagination[:per_page] * pagination[:page]) - 1   # Subtract 1 from product since delta_indexes are inclusive. We want 0..29, not 0..30.
    diff.add_delta_indexes((delta_index_from..delta_index_to).to_a)

    if !diff.available? || diff.truncated_for_timeout?
      deliver_undiffable_error!(:PullRequest, diff)
    end

    files = diff.to_a

    paginator.collection_size = pull.diffs.changed_files

    deliver :condensed_diff_entry_hash, files,
      repo: repo,
      last_modified: calc_last_modified(pull),
      compare_payload_to: -> (_, opts) { pull_request_files_candidate(pull, opts) }
  end

private

  def find_repo_and_pull_request
    repo = find_repo!
    pull = repo.issues.find_by_number(int_id_param!(key: :pull_number)).try(:pull_request) if repo

    if pull.nil? || pull.hide_from_user?(current_user)
      deliver_error!(404)
    end

    [repo, pull]
  end

  def authorize_content(operation = :create, data = {})
    authorization = ContentAuthorizer.authorize(current_user, :pull_request, operation, data)
    deliver_content_authorization_denied!(authorization) if authorization.failed?
  end

end
