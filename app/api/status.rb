# frozen_string_literal: true

class Api::Status < Api::App
  areas_of_responsibility :high_availability

  get "/status" do
    @route_owner = "@github/ecosystem-api"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL
    control_access :public_site_information, resource: Platform::PublicResource.new, allow_integrations: true, allow_user_via_integration: true
    skip_rate_limit!
    cache_control "no-cache"

    payload = {
      message: "GitHub lives! (#{Time.now}) (1)",
    }
    deliver_raw payload
  end
end
