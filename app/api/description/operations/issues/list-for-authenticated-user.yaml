---
summary: List user account issues assigned to the authenticated user
description: |-
  List issues across owned and member repositories assigned to the authenticated user.

  **Note**: GitHub's REST API v3 considers every pull request an issue, but not every issue is a pull request. For this
  reason, "Issues" endpoints may return both issues and pull requests in the response. You can identify pull requests by
  the `pull_request` key. Be aware that the `id` of a pull request returned from "Issues" endpoints will be an _issue id_. To find out the pull
  request id, use the "[List pull requests](${externalDocsUrl}/v3/pulls/#list-pull-requests)" endpoint.
tags:
- issues
operationId: issues/list-for-authenticated-user
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/issues/#list-user-account-issues-assigned-to-the-authenticated-user"
x-github-resource-owner: "@github/pe-issues-projects"
x-github-backfill:
  id: get:user:issues
  path: "/user/issues"
  http-method: get
  openapi-operation-file: app/api/description/backfill/operations/issues/list-for-authenticated-user.yaml
  impl-file: app/api/issues.rb
  errors: []
  openapi-docs-file: definitions/operations/issues/list-for-authenticated-user.yml
parameters:
- name: filter
  description: "Indicates which sorts of issues to return. Can be one of:  \n\\* `assigned`:
    Issues assigned to you  \n\\* `created`: Issues created by you  \n\\* `mentioned`:
    Issues mentioning you  \n\\* `subscribed`: Issues you're subscribed to updates
    for  \n\\* `all`: All issues the authenticated user can see, regardless of participation
    or creation"
  in: query
  required: false
  schema:
    type: string
    enum:
    - assigned
    - created
    - mentioned
    - subscribed
    - all
    default: assigned
- name: state
  description: Indicates the state of the issues to return. Can be either `open`,
    `closed`, or `all`.
  in: query
  required: false
  schema:
    type: string
    enum:
    - open
    - closed
    - all
    default: open
- "$ref": "../../components/parameters/labels.yaml"
- name: sort
  description: What to sort results by. Can be either `created`, `updated`, `comments`.
  in: query
  required: false
  schema:
    type: string
    enum:
    - created
    - updated
    - comments
    default: created
- "$ref": "../../components/parameters/direction.yaml"
- "$ref": "../../components/parameters/since.yaml"
- "$ref": "../../components/parameters/per_page.yaml"
- "$ref": "../../components/parameters/page.yaml"
responses:
  '200':
    description: response
    content:
      application/json:
        schema:
          type: array
          items:
            "$ref": "../../components/schemas/issue.yaml"
        examples:
          default:
            "$ref": "../../components/examples/issue-with-repo-items.yaml"
    headers:
      Link:
        "$ref": "../../components/headers/link.yaml"
  '404':
    "$ref": "../../components/responses/not_found.yaml"
  '304':
    "$ref": "../../components/responses/not_modified.yaml"
x-github-releases:
- ghes: ">= 2.18"
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: false
  previews:
  - "$ref": "../../components/x-previews/machine-man.yaml"
    required: false
  - "$ref": "../../components/x-previews/squirrel-girl.yaml"
    required: false
