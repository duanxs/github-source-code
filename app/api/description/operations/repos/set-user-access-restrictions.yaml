---
summary: Set user access restrictions
description: |-
  Protected branches are available in public repositories with GitHub Free and GitHub Free for organizations, and in public and private repositories with GitHub Pro, GitHub Team, GitHub Enterprise Cloud, and GitHub Enterprise Server. For more information, see [GitHub's products](https://help.github.com/github/getting-started-with-github/githubs-products) in the GitHub Help documentation.

  Replaces the list of people that have push access to this branch. This removes all people that previously had push access and grants push access to the new list of people.

  | Type    | Description                                                                                                                   |
  | ------- | ----------------------------------------------------------------------------------------------------------------------------- |
  | `array` | Usernames for people who can have push access. **Note**: The list of users, apps, and teams in total is limited to 100 items. |
tags:
- repos
operationId: repos/set-user-access-restrictions
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/repos/branches/#set-user-access-restrictions"
x-github-resource-owner: no-owner:unaudited
x-github-alternate-operation-id: repos/set-user-access-restrictions-by-id
x-github-backfill:
  id: put:repos:%:%:branches:%:protection:restrictions:users
  path: "/repos/{owner}/{repo}/branches/{branch}/protection/restrictions/users"
  http-method: put
  openapi-operation-file: app/api/description/backfill/operations/repos/set-user-access-restrictions.yaml
  errors: []
  openapi-docs-file: definitions/operations/repos/set-user-access-restrictions.yml
parameters:
- "$ref": "../../components/parameters/owner.yaml"
- "$ref": "../../components/parameters/repo.yaml"
- "$ref": "../../components/parameters/branch.yaml"
requestBody:
  content:
    application/json:
      schema:
        type: array
        description: users parameter
        items:
          type: string
responses:
  '200':
    description: response
    content:
      application/json:
        schema:
          type: array
          items:
            "$ref": "../../components/schemas/simple-user.yaml"
        examples:
          default:
            "$ref": "../../components/examples/simple-user-items.yaml"
  '422':
    "$ref": "../../components/responses/validation_failed.yaml"
x-github-releases:
- ghes: ">= 2.18"
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: true
  previews: []
  requestBodyParameterName: users
