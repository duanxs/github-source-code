---
summary: Upload a release asset
description: "This endpoint makes use of [a Hypermedia relation](${externalDocsUrl}/v3/#hypermedia)
  to determine which URL to access. The endpoint you call to upload release assets
  is specific to your release. Use the `upload_url` returned in\nthe response of the
  [Create a release endpoint](${externalDocsUrl}/v3/repos/releases/#create-a-release)
  to upload a release asset.\n\nYou need to use an HTTP client which supports [SNI](http://en.wikipedia.org/wiki/Server_Name_Indication)
  to make calls to this endpoint.\n\nMost libraries will set the required `Content-Length`
  header automatically. Use the required `Content-Type` header to provide the media
  type of the asset. For a list of media types, see [Media Types](https://www.iana.org/assignments/media-types/media-types.xhtml).
  For example: \n\n`application/zip`\n\n${apiName} expects the asset data in its raw
  binary form, rather than JSON. You will send the raw binary content of the asset
  as the request body. Everything else about the endpoint is the same as the rest
  of the API. For example,\nyou'll still need to pass your authentication to be able
  to upload an asset.\n\nWhen an upstream failure occurs, you will receive a `502
  Bad Gateway` status. This may leave an empty asset with a state of `starter`. It
  can be safely deleted.\n\n**Notes:**\n*   ${apiName} renames asset filenames that
  have special characters, non-alphanumeric characters, and leading or trailing periods.
  The \"[List assets for a release](${externalDocsUrl}/v3/repos/releases/#list-assets-for-a-release)\"\nendpoint
  lists the renamed filenames. For more information and help, contact [${apiName}
  Support](https://github.com/contact).\n*   If you upload an asset with the same
  filename as another uploaded asset, you'll receive an error and must delete the
  old file before you can re-upload the new asset."
tags:
- repos
operationId: repos/upload-release-asset
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/repos/releases/#upload-a-release-asset"
servers:
- url: "{origin}"
  variables:
    origin:
      default: https://uploads.github.com
      description: The URL origin (protocol + host name + port) is included in `upload_url`
        returned in the response of the "Create a release" endpoint
x-github-resource-owner: no-owner:unaudited
x-github-alternate-operation-id: repos/upload-release-asset-by-id
x-github-backfill:
  id: post:repos:%:%:releases:%:assets
  path: "/repos/{owner}/{repo}/releases/{release_id}/assets"
  http-method: post
  openapi-operation-file: app/api/description/backfill/operations/repos/upload-release-asset.yaml
  errors: []
  openapi-docs-file: definitions/operations/repos/upload-release-asset.yml
parameters:
- "$ref": "../../components/parameters/owner.yaml"
- "$ref": "../../components/parameters/repo.yaml"
- "$ref": "../../components/parameters/release_id.yaml"
- name: name
  in: query
  schema:
    type: string
  description: name parameter
- name: label
  in: query
  schema:
    type: string
  description: label parameter
requestBody:
  content:
    "*/*":
      schema:
        type: string
        description: The raw file data
responses:
  '201':
    description: Response for successful upload
    content:
      application/json:
        schema:
          "$ref": "../../components/schemas/release-asset.yaml"
        examples:
          response-for-successful-upload:
            "$ref": "../../components/examples/release-asset-response-for-successful-upload.yaml"
x-github-releases:
- ghes: ">= 2.18"
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: true
  previews: []
