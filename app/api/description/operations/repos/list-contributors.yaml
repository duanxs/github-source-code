---
summary: List repository contributors
description: |-
  Lists contributors to the specified repository and sorts them by the number of commits per contributor in descending order. This endpoint may return information that is a few hours old because the GitHub REST API v3 caches contributor data to improve performance.

  GitHub identifies contributors by author email address. This endpoint groups contribution counts by GitHub user, which includes all associated email addresses. To improve performance, only the first 500 author email addresses in the repository link to GitHub users. The rest will appear as anonymous contributors without associated GitHub user information.
tags:
- repos
operationId: repos/list-contributors
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/repos/#list-repository-contributors"
x-github-resource-owner: no-owner:unaudited
x-github-alternate-operation-id: repos/list-contributors-by-id
x-github-backfill:
  id: get:repos:%:%:contributors
  path: "/repos/{owner}/{repo}/contributors"
  http-method: get
  openapi-operation-file: app/api/description/backfill/operations/repos/list-contributors.yaml
  errors: []
  openapi-docs-file: definitions/operations/repos/list-contributors.yml
parameters:
- "$ref": "../../components/parameters/owner.yaml"
- "$ref": "../../components/parameters/repo.yaml"
- name: anon
  description: Set to `1` or `true` to include anonymous contributors in results.
  in: query
  required: false
  schema:
    type: string
- "$ref": "../../components/parameters/per_page.yaml"
- "$ref": "../../components/parameters/page.yaml"
responses:
  '200':
    description: Response if repository contains content
    content:
      application/json:
        schema:
          type: array
          items:
            "$ref": "../../components/schemas/contributor.yaml"
        examples:
          response-if-repository-contains-content:
            "$ref": "../../components/examples/contributor-items-response-if-repository-contains-content.yaml"
    headers:
      Link:
        "$ref": "../../components/headers/link.yaml"
  '204':
    description: Response if repository is empty
  '403':
    "$ref": "../../components/responses/forbidden.yaml"
  '404':
    "$ref": "../../components/responses/not_found.yaml"
x-github-releases:
- ghes: ">= 2.18"
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: true
  previews: []
