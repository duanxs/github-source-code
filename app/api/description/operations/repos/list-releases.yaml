---
summary: List releases
description: |-
  This returns a list of releases, which does not include regular Git tags that have not been associated with a release. To get a list of Git tags, use the [Repository Tags API](${externalDocsUrl}/v3/repos/#list-repository-tags).

  Information about published releases are available to everyone. Only users with push access will receive listings for draft releases.
tags:
- repos
operationId: repos/list-releases
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/repos/releases/#list-releases"
x-github-resource-owner: no-owner:unaudited
x-github-alternate-operation-id: repos/list-releases-by-id
x-github-backfill:
  id: get:repos:%:%:releases
  path: "/repos/{owner}/{repo}/releases"
  http-method: get
  openapi-operation-file: app/api/description/backfill/operations/repos/list-releases.yaml
  errors: []
  openapi-docs-file: definitions/operations/repos/list-releases.yml
parameters:
- "$ref": "../../components/parameters/owner.yaml"
- "$ref": "../../components/parameters/repo.yaml"
- "$ref": "../../components/parameters/per_page.yaml"
- "$ref": "../../components/parameters/page.yaml"
responses:
  '200':
    description: response
    content:
      application/json:
        schema:
          type: array
          items:
            "$ref": "../../components/schemas/release.yaml"
        examples:
          default:
            "$ref": "../../components/examples/release-items.yaml"
    headers:
      Link:
        "$ref": "../../components/headers/link.yaml"
  '404':
    "$ref": "../../components/responses/not_found.yaml"
x-github-releases:
- ghes: ">= 2.18"
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: true
  previews: []
