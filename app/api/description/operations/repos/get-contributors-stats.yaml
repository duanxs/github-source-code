---
summary: Get all contributor commit activity
description: |2-

  Returns the `total` number of commits authored by the contributor. In addition, the response includes a Weekly Hash (`weeks` array) with the following information:

  *   `w` - Start of the week, given as a [Unix timestamp](http://en.wikipedia.org/wiki/Unix_time).
  *   `a` - Number of additions
  *   `d` - Number of deletions
  *   `c` - Number of commits
tags:
- repos
operationId: repos/get-contributors-stats
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/repos/statistics/#get-all-contributor-commit-activity"
x-github-resource-owner: no-owner:unaudited
x-github-alternate-operation-id: repos/get-contributors-stats-by-id
x-github-backfill:
  id: get:repos:%:%:stats:contributors
  path: "/repos/{owner}/{repo}/stats/contributors"
  http-method: get
  openapi-operation-file: app/api/description/backfill/operations/repos/get-contributors-stats.yaml
  errors: []
  openapi-docs-file: definitions/operations/repos/get-contributors-stats.yml
parameters:
- "$ref": "../../components/parameters/owner.yaml"
- "$ref": "../../components/parameters/repo.yaml"
responses:
  '200':
    description: |-
      *   `w` - Start of the week, given as a [Unix timestamp](http://en.wikipedia.org/wiki/Unix_time).
      *   `a` - Number of additions
      *   `d` - Number of deletions
      *   `c` - Number of commits
    content:
      application/json:
        schema:
          type: array
          items:
            "$ref": "../../components/schemas/contributor-activity.yaml"
        examples:
          default:
            "$ref": "../../components/examples/contributor-activity-items.yaml"
x-github-releases:
- ghes: ">= 2.18"
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: true
  previews: []
