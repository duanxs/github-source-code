---
x-github-resource-owner: no-owner:unaudited
x-github-alternate-operation-id: 
x-github-backfill:
  path: "/admin/hooks/{hook_id}"
  http-method: patch
  openapi-operation-file: app/api/description/backfill/operations/enterprise-admin/update-global-webhook.yaml
  errors: []
  openapi-docs-file: definitions/operations/enterprise-admin/update-global-webhook.yml
summary: Update a global webhook
description: Parameters that are not provided will be overwritten with the default
  value or removed if no default exists.
operationId: enterprise-admin/update-global-webhook
tags:
- enterprise-admin
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/enterprise-admin/global_webhooks/#update-a-global-webhook"
parameters:
- name: accept
  description: This API is under preview and subject to change.
  in: header
  schema:
    type: string
    default: application/vnd.github.superpro-preview+json
  required: true
- "$ref": "../../components/parameters/hook-id.yaml"
responses:
  '200':
    description: response
    content:
      application/json:
        schema:
          "$ref": "../../components/schemas/global-hook-2.yaml"
        examples:
          default:
            "$ref": "../../components/examples/global-hook-2.yaml"
requestBody:
  content:
    application/json:
      schema:
        type: object
        properties:
          config:
            type: object
            description: Key/value pairs to provide settings for this webhook.
            properties:
              url:
                type: string
                description: The URL to which the payloads will be delivered.
              content_type:
                type: string
                description: The media type used to serialize the payloads. Supported
                  values include `json` and `form`. The default is `form`.
              secret:
                type: string
                description: If provided, the `secret` will be used as the `key` to
                  generate the HMAC hex digest value in the [`X-Hub-Signature`](${externalDocsUrl}/webhooks/event-payloads/#delivery-headers)
                  header.
              insecure_ssl:
                type: string
                description: Determines whether the SSL certificate of the host for
                  `url` will be verified when delivering payloads. Supported values
                  include `0` (verification is performed) and `1` (verification is
                  not performed). The default is `0`. **We strongly recommend not
                  setting this to `1` as you are subject to man-in-the-middle and
                  other attacks.**
            required:
            - url
          events:
            type: array
            description: 'The [events](${externalDocsUrl}/webhooks/event-payloads)
              that trigger this webhook. A global webhook can be triggered by `user`
              and `organization` events. Default: `user` and `organization`.'
            items:
              type: string
          active:
            type: boolean
            description: Determines if notifications are sent when the webhook is
              triggered. Set to `true` to send notifications.
            default: true
      example:
        events:
        - organization
        config:
          url: https://example.com/webhook
x-github-releases:
- ghes: ">= 2.18"
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: false
  previews:
  - "$ref": "../../components/x-previews/superpro.yaml"
    required: true
