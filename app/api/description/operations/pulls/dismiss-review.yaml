---
summary: Dismiss a review for a pull request
description: "**Note:** To dismiss a pull request review on a [protected branch](${externalDocsUrl}/v3/repos/branches/),
  you must be a repository administrator or be included in the list of people or teams
  who can dismiss pull request reviews."
tags:
- pulls
operationId: pulls/dismiss-review
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/pulls/reviews/#dismiss-a-review-for-a-pull-request"
x-github-resource-owner: no-owner:unaudited
x-github-alternate-operation-id: pulls/dismiss-review-by-id
x-github-backfill:
  id: put:repos:%:%:pulls:%:reviews:%:dismissals
  path: "/repos/{owner}/{repo}/pulls/{pull_number}/reviews/{review_id}/dismissals"
  http-method: put
  openapi-operation-file: app/api/description/backfill/operations/pulls/dismiss-review.yaml
  errors: []
  openapi-docs-file: definitions/operations/pulls/dismiss-review.yml
parameters:
- "$ref": "../../components/parameters/owner.yaml"
- "$ref": "../../components/parameters/repo.yaml"
- "$ref": "../../components/parameters/pull-number.yaml"
- "$ref": "../../components/parameters/review_id.yaml"
requestBody:
  content:
    application/json:
      schema:
        type: object
        properties:
          message:
            type: string
            description: The message for the pull request review dismissal
          event:
            type: string
            example: '"APPROVE"'
        required:
        - message
responses:
  '200':
    description: response
    content:
      application/json:
        schema:
          "$ref": "../../components/schemas/pull-request-review.yaml"
        examples:
          default:
            "$ref": "../../components/examples/pull-request-review-3.yaml"
  '404':
    "$ref": "../../components/responses/not_found.yaml"
  '422':
    "$ref": "../../components/responses/validation_failed_simple.yaml"
x-github-releases:
- ghes: ">= 2.18"
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: true
  previews: []
