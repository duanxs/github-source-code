---
summary: List repositories starred by the authenticated user
description: |-
  Lists repositories the authenticated user has starred.

  You can also find out _when_ stars were created by passing the following custom [media type](${externalDocsUrl}/v3/media/) via the `Accept` header:
tags:
- activity
operationId: activity/list-repos-starred-by-authenticated-user
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/activity/starring/#list-repositories-starred-by-the-authenticated-user"
x-github-resource-owner: "@github/notifications"
x-github-backfill:
  id: get:user:starred
  path: "/user/starred"
  http-method: get
  openapi-operation-file: app/api/description/backfill/operations/activity/list-repos-starred-by-authenticated-user.yaml
  impl-file: app/api/repository_activity.rb
  errors: []
  openapi-docs-file: definitions/operations/activity/list-repos-starred-by-authenticated-user.yml
parameters:
- "$ref": "../../components/parameters/sort.yaml"
- "$ref": "../../components/parameters/direction.yaml"
- "$ref": "../../components/parameters/per_page.yaml"
- "$ref": "../../components/parameters/page.yaml"
responses:
  '200':
    description: response
    content:
      application/json:
        schema:
          type: array
          items:
            "$ref": "../../components/schemas/repository.yaml"
        examples:
          default-response:
            "$ref": "../../components/examples/repository-items-default-response.yaml"
      application/vnd.github.v3.star+json:
        schema:
          type: array
          items:
            "$ref": "../../components/schemas/starred-repository.yaml"
        examples:
          alternative-response-with-star-creation-timestamps:
            "$ref": "../../components/examples/starred-repository-items-alternative-response-with-star-creation-timestamps.yaml"
    headers:
      Link:
        "$ref": "../../components/headers/link.yaml"
  '304':
    "$ref": "../../components/responses/not_modified.yaml"
  '403':
    "$ref": "../../components/responses/forbidden.yaml"
  '401':
    "$ref": "../../components/responses/requires_authentication.yaml"
x-github-releases:
- ghes: ">= 2.18"
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: false
  previews: []
