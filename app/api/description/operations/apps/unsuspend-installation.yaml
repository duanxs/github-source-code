---
summary: Unsuspend an app installation
description: |-
  **Note:** Suspending a GitHub App installation is currently in beta and subject to change. Before you can suspend a GitHub App, the app owner must enable suspending installations for the app by opting-in to the beta. For more information, see "[Suspending a GitHub App installation](${externalDocsUrl}/apps/managing-github-apps/suspending-a-github-app-installation/)."

  Removes a GitHub App installation suspension.

  To unsuspend a GitHub App, you must be an account owner or have admin permissions in the repository or organization where the app is installed and suspended.

  You must use a [JWT](${externalDocsUrl}/apps/building-github-apps/authenticating-with-github-apps/#authenticating-as-a-github-app) to access this endpoint.
tags:
- apps
operationId: apps/unsuspend-installation
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/apps/#unsuspend-an-app-installation"
x-github-resource-owner: "@github/ecosystem-apps"
x-github-backfill:
  id: delete:app:installations:%:suspended
  path: "/app/installations/{installation_id}/suspended"
  http-method: delete
  openapi-operation-file: app/api/description/backfill/operations/apps/unsuspend-installation.yaml
  impl-file: app/api/integrations.rb
  errors: []
  openapi-docs-file: definitions/operations/apps/unsuspend-installation.yml
  json-schema-file: app/api/schemas/v3/schemas/installation.json
parameters:
- "$ref": "../../components/parameters/installation_id.yaml"
responses:
  '204':
    description: Empty response
  '404':
    "$ref": "../../components/responses/not_found.yaml"
x-github-releases:
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: false
  previews: []
