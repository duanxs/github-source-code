---
summary: Revoke a grant for an application
description: |-
  **Deprecation Notice:** ${apiName} will replace and discontinue OAuth endpoints containing `access_token` in the path parameter. We are introducing new endpoints that allow you to securely manage tokens for OAuth Apps by using `access_token` as an input parameter. The OAuth Application API will be removed on May 5, 2021. For more information, including scheduled brownouts, see the [blog post](${externalDocsUrl}/changes/2020-02-14-deprecating-oauth-app-endpoint/).

  OAuth application owners can revoke a grant for their OAuth application and a specific user. You must use [Basic Authentication](${externalDocsUrl}/v3/auth#basic-authentication) when accessing this endpoint, using the OAuth application's `client_id` and `client_secret` as the username and password. You must also provide a valid token as `:access_token` and the grant for the token's owner will be deleted.

  Deleting an OAuth application's grant will also delete all OAuth tokens associated with the application for the user. Once deleted, the application will have no access to the user's account and will no longer be listed on [the Applications settings page under "Authorized OAuth Apps" on ${apiName}](https://github.com/settings/applications#authorized).
tags:
- apps
operationId: apps/revoke-grant-for-application
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/apps/oauth_applications/#revoke-a-grant-for-an-application"
x-github-resource-owner: "@github/ecosystem-apps"
x-github-backfill:
  id: delete:applications:%:grants:%
  path: "/applications/{client_id}/grants/{access_token}"
  http-method: delete
  openapi-operation-file: app/api/description/backfill/operations/apps/revoke-grant-for-application.yaml
  impl-file: app/api/applications.rb
  errors: []
  openapi-docs-file: definitions/operations/apps/revoke-grant-for-application.yml
  json-schema-file: app/api/schemas/v3/schemas/application-grant.json
parameters:
- "$ref": "../../components/parameters/client-id.yaml"
- "$ref": "../../components/parameters/access-token.yaml"
responses:
  '204':
    description: Empty response
x-github-releases:
- ghes:
  - ">= 2.20"
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: false
  previews: []
  removalDate: '2021-05-05'
  deprecationDate: '2020-02-14'
deprecated: true
x-deprecation-details:
  date: '2020-02-14'
  sunsetDate: '2021-05-05'
