---
summary: Create a content attachment
description: |-
  Creates an attachment under a content reference URL in the body or comment of an issue or pull request. Use the `id` of the content reference from the [`content_reference` event](${externalDocsUrl}/webhooks/event-payloads/#content_reference) to create an attachment.

  The app must create a content attachment within six hours of the content reference URL being posted. See "[Using content attachments](${externalDocsUrl}/apps/using-content-attachments/)" for details about content attachments.

  You must use an [installation access token](${externalDocsUrl}/apps/building-github-apps/authenticating-with-github-apps/#authenticating-as-an-installation) to access this endpoint.
tags:
- apps
operationId: apps/create-content-attachment
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/apps/installations/#create-a-content-attachment"
x-github-resource-owner: "@github/ce-extensibility"
x-github-backfill:
  id: post:content_references:%:attachments
  path: "/content_references/{content_reference_id}/attachments"
  http-method: post
  openapi-operation-file: app/api/description/backfill/operations/apps/create-content-attachment.yaml
  impl-file: app/api/repository_content_references.rb
  errors: []
  not-documented-because: deprecated
  openapi-docs-file: definitions/operations/apps/create-content-attachment.yml
  json-schema-file: app/api/schemas/v3/schemas/content-reference-attachment.json
parameters:
- name: content_reference_id
  description: content_reference_id parameter
  in: path
  required: true
  schema:
    type: integer
requestBody:
  content:
    application/json:
      schema:
        properties:
          title:
            description: The title of the attachment
            example: Title of the attachment
            type: string
            maxLength: 1024
          body:
            description: The body of the attachment
            example: Body of the attachment
            type: string
            maxLength: 262144
        required:
        - title
        - body
        type: object
responses:
  '200':
    description: response
    content:
      application/json:
        schema:
          "$ref": "../../components/schemas/content-reference-attachment.yaml"
        examples:
          default:
            "$ref": "../../components/examples/content-reference-attachment.yaml"
  '422':
    "$ref": "../../components/responses/validation_failed.yaml"
  '404':
    "$ref": "../../components/responses/not_found.yaml"
  '410':
    "$ref": "../../components/responses/gone.yaml"
  '415':
    "$ref": "../../components/responses/preview_header_missing.yaml"
  '304':
    "$ref": "../../components/responses/not_modified.yaml"
  '403':
    "$ref": "../../components/responses/forbidden.yaml"
x-github-releases:
- ghes: ">= 2.18"
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: true
  previews:
  - "$ref": "../../components/x-previews/corsair.yaml"
    required: true
