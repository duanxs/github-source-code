---
summary: Search code
description: |-
  Searches for query terms inside of a file. This method returns up to 100 results [per page](${externalDocsUrl}/v3/#pagination).

  When searching for code, you can get text match metadata for the file **content** and file **path** fields when you pass the `text-match` media type. For more details about how to receive highlighted search results, see [Text match metadata](${externalDocsUrl}/v3/search/#text-match-metadata).

  For example, if you want to find the definition of the `addClass` function inside [jQuery](https://github.com/jquery/jquery) repository, your query would look something like this:

  `q=addClass+in:file+language:js+repo:jquery/jquery`

  This query searches for the keyword `addClass` within a file's contents. The query limits the search to files where the language is JavaScript in the `jquery/jquery` repository.

  #### Considerations for code search

  Due to the complexity of searching code, there are a few restrictions on how searches are performed:

  *   Only the _default branch_ is considered. In most cases, this will be the `master` branch.
  *   Only files smaller than 384 KB are searchable.
  *   You must always include at least one search term when searching source code. For example, searching for [`language:go`](https://github.com/search?utf8=%E2%9C%93&q=language%3Ago&type=Code) is not valid, while [`amazing
  language:go`](https://github.com/search?utf8=%E2%9C%93&q=amazing+language%3Ago&type=Code) is.
tags:
- search
operationId: search/code
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/search/#search-code"
x-github-resource-owner: "@github/ee-search"
x-github-backfill:
  id: get:search:code
  path: "/search/code"
  http-method: get
  openapi-operation-file: app/api/description/backfill/operations/search/code.yaml
  impl-file: app/api/search.rb
  errors: []
  openapi-docs-file: definitions/operations/search/code.yml
parameters:
- name: q
  description: The query contains one or more search keywords and qualifiers. Qualifiers
    allow you to limit your search to specific areas of GitHub. The REST API supports
    the same qualifiers as GitHub.com. To learn more about the format of the query,
    see [Constructing a search query](${externalDocsUrl}/v3/search/#constructing-a-search-query).
    See "[Searching code](https://help.github.com/articles/searching-code/)" for a
    detailed list of qualifiers.
  in: query
  required: true
  schema:
    type: string
- name: sort
  description: 'Sorts the results of your query. Can only be `indexed`, which indicates
    how recently a file has been indexed by the ${apiName} search infrastructure.
    Default: [best match](${externalDocsUrl}/v3/search/#ranking-search-results)'
  in: query
  required: false
  schema:
    type: string
    enum:
    - indexed
- "$ref": "../../components/parameters/order.yaml"
- "$ref": "../../components/parameters/per_page.yaml"
- "$ref": "../../components/parameters/page.yaml"
responses:
  '200':
    description: response
    content:
      application/json:
        schema:
          type: object
          properties:
            total_count:
              type: integer
            incomplete_results:
              type: boolean
            items:
              type: array
              items:
                "$ref": "../../components/schemas/code-search-result-item.yaml"
        examples:
          default:
            "$ref": "../../components/examples/code-search-result-item-paginated.yaml"
  '304':
    "$ref": "../../components/responses/not_modified.yaml"
  '503':
    "$ref": "../../components/responses/service_unavailable.yaml"
  '422':
    "$ref": "../../components/responses/validation_failed.yaml"
  '403':
    "$ref": "../../components/responses/forbidden.yaml"
x-github-releases:
- ghes: ">= 2.18"
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: true
  previews: []
