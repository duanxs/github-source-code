---
summary: Download a user migration archive
description: |-
  Fetches the URL to download the migration archive as a `tar.gz` file. Depending on the resources your repository uses, the migration archive can contain JSON files with data for these objects:

  *   attachments
  *   bases
  *   commit\_comments
  *   issue\_comments
  *   issue\_events
  *   issues
  *   milestones
  *   organizations
  *   projects
  *   protected\_branches
  *   pull\_request\_reviews
  *   pull\_requests
  *   releases
  *   repositories
  *   review\_comments
  *   schema
  *   users

  The archive will also contain an `attachments` directory that includes all attachment files uploaded to GitHub.com and a `repositories` directory that contains the repository's Git data.
tags:
- migrations
operationId: migrations/get-archive-for-authenticated-user
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/migrations/users/#download-a-user-migration-archive"
x-github-resource-owner: "@github/data-liberation"
x-github-backfill:
  id: get:user:migrations:%:archive
  path: "/user/migrations/{migration_id}/archive"
  http-method: get
  openapi-operation-file: app/api/description/backfill/operations/migrations/get-archive-for-authenticated-user.yaml
  impl-file: app/api/migrations.rb
  errors: []
  openapi-docs-file: definitions/operations/migrations/get-archive-for-authenticated-user.yml
parameters:
- "$ref": "../../components/parameters/migration_id.yaml"
responses:
  '302':
    description: response
  '304':
    "$ref": "../../components/responses/not_modified.yaml"
  '403':
    "$ref": "../../components/responses/forbidden.yaml"
  '401':
    "$ref": "../../components/responses/requires_authentication.yaml"
x-github-releases:
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: false
  previews:
  - "$ref": "../../components/x-previews/wyandotte.yaml"
    required: true
