---
summary: Create reaction for a team discussion (Legacy)
description: |-
  **Deprecation Notice:** This endpoint route is deprecated and will be removed from the Teams API. We recommend migrating your existing code to use the new [`Create reaction for a team discussion`](${externalDocsUrl}/v3/reactions/#create-reaction-for-a-team-discussion) endpoint.

  Create a reaction to a [team discussion](${externalDocsUrl}/v3/teams/discussions/). OAuth access tokens require the `write:discussion` [scope](${externalDocsUrl}/apps/building-oauth-apps/understanding-scopes-for-oauth-apps/). A response with a `Status: 200 OK` means that you already added the reaction type to this team discussion.
tags:
- reactions
operationId: reactions/create-for-team-discussion-legacy
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/reactions/#create-reaction-for-a-team-discussion-legacy"
x-github-resource-owner: no-owner:unaudited
x-github-backfill:
  id: post:teams:%:discussions:%:reactions
  path: "/teams/{team_id}/discussions/{discussion_number}/reactions"
  http-method: post
  openapi-operation-file: app/api/description/backfill/operations/reactions/create-for-team-discussion-legacy.yaml
  errors: []
  openapi-docs-file: definitions/operations/reactions/create-for-team-discussion-legacy.yml
parameters:
- "$ref": "../../components/parameters/team-id.yaml"
- "$ref": "../../components/parameters/discussion-number.yaml"
requestBody:
  content:
    application/json:
      schema:
        type: object
        properties:
          content:
            type: string
            description: The [reaction type](${externalDocsUrl}/v3/reactions/#reaction-types)
              to add to the team discussion.
            enum:
            - "+1"
            - "-1"
            - laugh
            - confused
            - heart
            - hooray
            - rocket
            - eyes
        required:
        - content
      example:
        content: heart
responses:
  '201':
    description: response
    content:
      application/json:
        schema:
          "$ref": "../../components/schemas/reaction.yaml"
        examples:
          default:
            "$ref": "../../components/examples/reaction.yaml"
x-github-releases:
- ghes:
  - ">= 2.21"
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: false
  previews:
  - "$ref": "../../components/x-previews/squirrel-girl.yaml"
    required: true
  removalDate: '2021-02-21'
  deprecationDate: '2020-02-26'
deprecated: true
x-deprecation-details:
  date: '2020-02-26'
  sunsetDate: '2021-02-21'
