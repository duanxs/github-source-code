---
summary: List SAML SSO authorizations for an organization
description: |-
  Listing and deleting credential authorizations is available to organizations with GitHub Enterprise Cloud. For more information, see [GitHub's products](https://help.github.com/github/getting-started-with-github/githubs-products).

  An authenticated organization owner with the `read:org` scope can list all credential authorizations for an organization that uses SAML single sign-on (SSO). The credentials are either personal access tokens or SSH keys that organization members have authorized for the organization. For more information, see [About authentication with SAML single sign-on](https://help.github.com/en/articles/about-authentication-with-saml-single-sign-on).
tags:
- orgs
operationId: orgs/list-saml-sso-authorizations
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/orgs/#list-saml-sso-authorizations-for-an-organization"
x-github-resource-owner: no-owner:unaudited
x-github-alternate-operation-id: orgs/list-saml-sso-authorizations-by-id
x-github-backfill:
  id: get:orgs:%:credential-authorizations
  path: "/orgs/{org}/credential-authorizations"
  http-method: get
  openapi-operation-file: app/api/description/backfill/operations/orgs/list-saml-sso-authorizations.yaml
  errors: []
  openapi-docs-file: definitions/operations/orgs/list-saml-sso-authorizations.yml
parameters:
- "$ref": "../../components/parameters/org.yaml"
responses:
  '200':
    description: response
    content:
      application/json:
        schema:
          type: array
          items:
            "$ref": "../../components/schemas/credential-authorization.yaml"
        examples:
          default:
            "$ref": "../../components/examples/credential-authorization-items.yaml"
x-github-releases:
- api.github.com
x-github:
  githubCloudOnly: true
  enabledForGitHubApps: true
  previews: []
