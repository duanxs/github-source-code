---
summary: Remove a SAML SSO authorization for an organization
description: |-
  Listing and deleting credential authorizations is available to organizations with GitHub Enterprise Cloud. For more information, see [GitHub's products](https://help.github.com/github/getting-started-with-github/githubs-products).

  An authenticated organization owner with the `admin:org` scope can remove a credential authorization for an organization that uses SAML SSO. Once you remove someone's credential authorization, they will need to create a new personal access token or SSH key and authorize it for the organization they want to access.
tags:
- orgs
operationId: orgs/remove-saml-sso-authorization
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/orgs/#remove-a-saml-sso-authorization-for-an-organization"
x-github-resource-owner: no-owner:unaudited
x-github-alternate-operation-id: orgs/remove-saml-sso-authorization-by-id
x-github-backfill:
  id: delete:orgs:%:credential-authorizations:%
  path: "/orgs/{org}/credential-authorizations/{credential_id}"
  http-method: delete
  openapi-operation-file: app/api/description/backfill/operations/orgs/remove-saml-sso-authorization.yaml
  errors: []
  openapi-docs-file: definitions/operations/orgs/remove-saml-sso-authorization.yml
parameters:
- "$ref": "../../components/parameters/org.yaml"
- name: credential_id
  description: credential_id parameter
  in: path
  required: true
  schema:
    type: integer
responses:
  '204':
    description: Empty response
  '404':
    "$ref": "../../components/responses/not_found.yaml"
x-github-releases:
- api.github.com
x-github:
  githubCloudOnly: true
  enabledForGitHubApps: true
  previews: []
