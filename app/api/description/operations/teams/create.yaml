---
summary: Create a team
description: |-
  To create a team, the authenticated user must be a member or owner of `{org}`. By default, organization members can create teams. Organization owners can limit team creation to organization owners. For more information, see "[Setting team creation permissions](https://help.github.com/en/articles/setting-team-creation-permissions-in-your-organization)."

  When you create a new team, you automatically become a team maintainer without explicitly adding yourself to the optional array of `maintainers`. For more information, see "[About teams](https://help.github.com/en/github/setting-up-and-managing-organizations-and-teams/about-teams)".
tags:
- teams
operationId: teams/create
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/teams/#create-a-team"
x-github-resource-owner: no-owner:unaudited
x-github-alternate-operation-id: teams/create-by-id
x-github-backfill:
  id: post:orgs:%:teams
  path: "/orgs/{org}/teams"
  http-method: post
  openapi-operation-file: app/api/description/backfill/operations/teams/create.yaml
  errors: []
  openapi-docs-file: definitions/operations/teams/create.yml
parameters:
- "$ref": "../../components/parameters/org.yaml"
requestBody:
  content:
    application/json:
      schema:
        type: object
        properties:
          name:
            type: string
            description: The name of the team.
          description:
            type: string
            description: The description of the team.
          maintainers:
            type: array
            description: List GitHub IDs for organization members who will become
              team maintainers.
            items:
              type: string
          repo_names:
            type: array
            description: The full name (e.g., "organization-name/repository-name")
              of repositories to add the team to.
            items:
              type: string
          privacy:
            type: string
            description: "The level of privacy this team should have. The options
              are:  \n**For a non-nested team:**  \n\\* `secret` - only visible to
              organization owners and members of this team.  \n\\* `closed` - visible
              to all members of this organization.  \nDefault: `secret`  \n**For a
              parent or child team:**  \n\\* `closed` - visible to all members of
              this organization.  \nDefault for child team: `closed`"
            enum:
            - secret
            - closed
          permission:
            type: string
            description: "**Deprecated**. The permission that new repositories will
              be added to the team with when none is specified. Can be one of:  \n\\*
              `pull` - team members can pull, but not push to or administer newly-added
              repositories.  \n\\* `push` - team members can pull and push, but not
              administer newly-added repositories.  \n\\* `admin` - team members can
              pull, push and administer newly-added repositories."
            enum:
            - pull
            - push
            - admin
            default: pull
          parent_team_id:
            type: integer
            description: The ID of a team to set as the parent team.
        required:
        - name
      example:
        name: Justice League
        description: A great team
        permission: admin
        privacy: closed
responses:
  '201':
    description: response
    content:
      application/json:
        schema:
          "$ref": "../../components/schemas/team-full.yaml"
        examples:
          default:
            "$ref": "../../components/examples/team-full.yaml"
  '422':
    "$ref": "../../components/responses/validation_failed.yaml"
  '403':
    "$ref": "../../components/responses/forbidden.yaml"
x-github-releases:
- ghes: ">= 2.18"
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: true
  previews: []
