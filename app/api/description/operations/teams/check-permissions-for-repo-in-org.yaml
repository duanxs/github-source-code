---
summary: Check team permissions for a repository
description: |-
  Checks whether a team has `admin`, `push`, `maintain`, `triage`, or `pull` permission for a repository. Repositories inherited through a parent team will also be checked.

  You can also get information about the specified repository, including what permissions the team grants on it, by passing the following custom [media type](${externalDocsUrl}/v3/media/) via the `application/vnd.github.v3.repository+json` accept header.

  If a team doesn't have permission for the repository, you will receive a `404 Not Found` response status.

  **Note:** You can also specify a team by `org_id` and `team_id` using the route `GET /organizations/{org_id}/team/{team_id}/repos/{owner}/{repo}`.
tags:
- teams
operationId: teams/check-permissions-for-repo-in-org
externalDocs:
  description: API method documentation
  url: "${externalDocsUrl}/v3/teams/#check-team-permissions-for-a-repository"
x-github-resource-owner: no-owner:unaudited
x-github-alternate-operation-id: teams/check-permissions-for-repo-in-org-by-id
x-github-backfill:
  id: get:orgs:%:teams:%:repos:%:%
  path: "/orgs/{org}/teams/{team_slug}/repos/{owner}/{repo}"
  http-method: get
  openapi-operation-file: app/api/description/backfill/operations/teams/check-permissions-for-repo-in-org.yaml
  errors: []
  openapi-docs-file: definitions/operations/teams/check-permissions-for-repo-in-org.yml
parameters:
- "$ref": "../../components/parameters/org.yaml"
- "$ref": "../../components/parameters/team_slug.yaml"
- "$ref": "../../components/parameters/owner.yaml"
- "$ref": "../../components/parameters/repo.yaml"
responses:
  '200':
    description: Alternative response with repository permissions
    content:
      application/vnd.github.v3.repository+json:
        schema:
          "$ref": "../../components/schemas/team-repository.yaml"
        examples:
          alternative-response-with-repository-permissions:
            "$ref": "../../components/examples/team-repository-alternative-response-with-repository-permissions.yaml"
  '204':
    description: Response if team has permission for the repository
  '404':
    description: Response if team does not have permission for the repository
x-github-releases:
- ghes:
  - ">= 2.21"
- api.github.com
x-github:
  githubCloudOnly: false
  enabledForGitHubApps: true
  previews: []
