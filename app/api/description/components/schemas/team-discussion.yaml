---
title: Team Discussion
description: A team discussion is a persistent record of a free-form conversation
  within a team.
type: object
properties:
  author:
    nullable: true
    allOf:
    - "$ref": "./simple-user.yaml"
  body:
    description: The main text of the discussion.
    example: Please suggest improvements to our workflow in comments.
    type: string
  body_html:
    type: string
    example: "<p>Hi! This is an area for us to collaborate as a team</p>"
  body_version:
    description: The current version of the body content. If provided, this update
      operation will be rejected if the given version does not match the latest version
      on the server.
    example: 0307116bbf7ced493b8d8a346c650b71
    type: string
  comments_count:
    type: integer
    example: 0
  comments_url:
    type: string
    format: uri
    example: https://api.github.com/organizations/1/team/2343027/discussions/1/comments
  created_at:
    type: string
    format: date-time
    example: '2018-01-25T18:56:31Z'
  last_edited_at:
    type: string
    format: date-time
    nullable: true
  html_url:
    type: string
    format: uri
    example: https://github.com/orgs/github/teams/justice-league/discussions/1
  node_id:
    type: string
    example: MDE0OlRlYW1EaXNjdXNzaW9uMQ==
  number:
    description: The unique sequence number of a team discussion.
    example: 42
    type: integer
  pinned:
    description: Whether or not this discussion should be pinned for easy retrieval.
    example: true
    type: boolean
  private:
    description: Whether or not this discussion should be restricted to team members
      and organization administrators.
    example: true
    type: boolean
  team_url:
    type: string
    format: uri
    example: https://api.github.com/organizations/1/team/2343027
  title:
    description: The title of the discussion.
    example: How can we improve our workflow?
    type: string
  updated_at:
    type: string
    format: date-time
    example: '2018-01-25T18:56:31Z'
  url:
    type: string
    format: uri
    example: https://api.github.com/organizations/1/team/2343027/discussions/1
  reactions:
    "$ref": "./reaction-rollup.yaml"
required:
- author
- body
- body_html
- body_version
- comments_count
- comments_url
- created_at
- last_edited_at
- html_url
- pinned
- private
- node_id
- number
- team_url
- title
- updated_at
- url
