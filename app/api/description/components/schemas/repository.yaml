---
title: Repository
description: A git repository
type: object
properties:
  id:
    description: Unique identifier of the repository
    example: 42
    type: integer
  node_id:
    type: string
    example: MDEwOlJlcG9zaXRvcnkxMjk2MjY5
  name:
    description: The name of the repository.
    type: string
    example: Team Environment
  full_name:
    type: string
    example: octocat/Hello-World
  license:
    nullable: true
    allOf:
    - "$ref": "./license-simple.yaml"
  forks:
    type: integer
  permissions:
    type: object
    properties:
      admin:
        type: boolean
      pull:
        type: boolean
      triage:
        type: boolean
      push:
        type: boolean
      maintain:
        type: boolean
    required:
    - admin
    - pull
    - push
  owner:
    nullable: true
    allOf:
    - "$ref": "./simple-user.yaml"
  private:
    description: Whether the repository is private or public.
    default: false
    type: boolean
  html_url:
    type: string
    format: uri
    example: https://github.com/octocat/Hello-World
  description:
    type: string
    example: This your first repo!
    nullable: true
  fork:
    type: boolean
  url:
    type: string
    format: uri
    example: https://api.github.com/repos/octocat/Hello-World
  archive_url:
    type: string
    example: http://api.github.com/repos/octocat/Hello-World/{archive_format}{/ref}
  assignees_url:
    type: string
    example: http://api.github.com/repos/octocat/Hello-World/assignees{/user}
  blobs_url:
    type: string
    example: http://api.github.com/repos/octocat/Hello-World/git/blobs{/sha}
  branches_url:
    type: string
    example: http://api.github.com/repos/octocat/Hello-World/branches{/branch}
  collaborators_url:
    type: string
    example: http://api.github.com/repos/octocat/Hello-World/collaborators{/collaborator}
  comments_url:
    type: string
    example: http://api.github.com/repos/octocat/Hello-World/comments{/number}
  commits_url:
    type: string
    example: http://api.github.com/repos/octocat/Hello-World/commits{/sha}
  compare_url:
    type: string
    example: http://api.github.com/repos/octocat/Hello-World/compare/{base}...{head}
  contents_url:
    type: string
    example: http://api.github.com/repos/octocat/Hello-World/contents/{+path}
  contributors_url:
    type: string
    format: uri
    example: http://api.github.com/repos/octocat/Hello-World/contributors
  deployments_url:
    type: string
    format: uri
    example: http://api.github.com/repos/octocat/Hello-World/deployments
  downloads_url:
    type: string
    format: uri
    example: http://api.github.com/repos/octocat/Hello-World/downloads
  events_url:
    type: string
    format: uri
    example: http://api.github.com/repos/octocat/Hello-World/events
  forks_url:
    type: string
    format: uri
    example: http://api.github.com/repos/octocat/Hello-World/forks
  git_commits_url:
    type: string
    example: http://api.github.com/repos/octocat/Hello-World/git/commits{/sha}
  git_refs_url:
    type: string
    example: http://api.github.com/repos/octocat/Hello-World/git/refs{/sha}
  git_tags_url:
    type: string
    example: http://api.github.com/repos/octocat/Hello-World/git/tags{/sha}
  git_url:
    type: string
    example: git:github.com/octocat/Hello-World.git
  issue_comment_url:
    type: string
    example: http://api.github.com/repos/octocat/Hello-World/issues/comments{/number}
  issue_events_url:
    type: string
    example: http://api.github.com/repos/octocat/Hello-World/issues/events{/number}
  issues_url:
    type: string
    example: http://api.github.com/repos/octocat/Hello-World/issues{/number}
  keys_url:
    type: string
    example: http://api.github.com/repos/octocat/Hello-World/keys{/key_id}
  labels_url:
    type: string
    example: http://api.github.com/repos/octocat/Hello-World/labels{/name}
  languages_url:
    type: string
    format: uri
    example: http://api.github.com/repos/octocat/Hello-World/languages
  merges_url:
    type: string
    format: uri
    example: http://api.github.com/repos/octocat/Hello-World/merges
  milestones_url:
    type: string
    example: http://api.github.com/repos/octocat/Hello-World/milestones{/number}
  notifications_url:
    type: string
    example: http://api.github.com/repos/octocat/Hello-World/notifications{?since,all,participating}
  pulls_url:
    type: string
    example: http://api.github.com/repos/octocat/Hello-World/pulls{/number}
  releases_url:
    type: string
    example: http://api.github.com/repos/octocat/Hello-World/releases{/id}
  ssh_url:
    type: string
    example: git@github.com:octocat/Hello-World.git
  stargazers_url:
    type: string
    format: uri
    example: http://api.github.com/repos/octocat/Hello-World/stargazers
  statuses_url:
    type: string
    example: http://api.github.com/repos/octocat/Hello-World/statuses/{sha}
  subscribers_url:
    type: string
    format: uri
    example: http://api.github.com/repos/octocat/Hello-World/subscribers
  subscription_url:
    type: string
    format: uri
    example: http://api.github.com/repos/octocat/Hello-World/subscription
  tags_url:
    type: string
    format: uri
    example: http://api.github.com/repos/octocat/Hello-World/tags
  teams_url:
    type: string
    format: uri
    example: http://api.github.com/repos/octocat/Hello-World/teams
  trees_url:
    type: string
    example: http://api.github.com/repos/octocat/Hello-World/git/trees{/sha}
  clone_url:
    type: string
    example: https://github.com/octocat/Hello-World.git
  mirror_url:
    type: string
    format: uri
    example: git:git.example.com/octocat/Hello-World
    nullable: true
  hooks_url:
    type: string
    format: uri
    example: http://api.github.com/repos/octocat/Hello-World/hooks
  svn_url:
    type: string
    format: uri
    example: https://svn.github.com/octocat/Hello-World
  homepage:
    type: string
    format: uri
    example: https://github.com
    nullable: true
  language:
    type: string
    nullable: true
  forks_count:
    type: integer
    example: 9
  stargazers_count:
    type: integer
    example: 80
  watchers_count:
    type: integer
    example: 80
  size:
    type: integer
    example: 108
  default_branch:
    description: The default branch of the repository.
    type: string
    example: master
  open_issues_count:
    type: integer
    example: 0
  is_template:
    description: Whether this repository acts as a template that can be used to generate
      new repositories.
    default: false
    type: boolean
    example: true
  topics:
    type: array
    items:
      type: string
  has_issues:
    description: Whether issues are enabled.
    default: true
    type: boolean
    example: true
  has_projects:
    description: Whether projects are enabled.
    default: true
    type: boolean
    example: true
  has_wiki:
    description: Whether the wiki is enabled.
    default: true
    type: boolean
    example: true
  has_pages:
    type: boolean
  has_downloads:
    description: Whether downloads are enabled.
    default: true
    type: boolean
    example: true
  archived:
    description: Whether the repository is archived.
    default: false
    type: boolean
  disabled:
    type: boolean
    description: Returns whether or not this repository disabled.
  visibility:
    description: 'The repository visibility: public, private, or internal.'
    default: public
    type: string
  pushed_at:
    type: string
    format: date-time
    example: '2011-01-26T19:06:43Z'
    nullable: true
  created_at:
    type: string
    format: date-time
    example: '2011-01-26T19:01:12Z'
    nullable: true
  updated_at:
    type: string
    format: date-time
    example: '2011-01-26T19:14:43Z'
    nullable: true
  allow_rebase_merge:
    description: Whether to allow rebase merges for pull requests.
    default: true
    type: boolean
    example: true
  template_repository:
    type: object
    nullable: true
    properties:
      id:
        type: integer
      node_id:
        type: string
      name:
        type: string
      full_name:
        type: string
      owner:
        type: object
        properties:
          login:
            type: string
          id:
            type: integer
          node_id:
            type: string
          avatar_url:
            type: string
          gravatar_id:
            type: string
          url:
            type: string
          html_url:
            type: string
          followers_url:
            type: string
          following_url:
            type: string
          gists_url:
            type: string
          starred_url:
            type: string
          subscriptions_url:
            type: string
          organizations_url:
            type: string
          repos_url:
            type: string
          events_url:
            type: string
          received_events_url:
            type: string
          type:
            type: string
          site_admin:
            type: boolean
      private:
        type: boolean
      html_url:
        type: string
      description:
        type: string
      fork:
        type: boolean
      url:
        type: string
      archive_url:
        type: string
      assignees_url:
        type: string
      blobs_url:
        type: string
      branches_url:
        type: string
      collaborators_url:
        type: string
      comments_url:
        type: string
      commits_url:
        type: string
      compare_url:
        type: string
      contents_url:
        type: string
      contributors_url:
        type: string
      deployments_url:
        type: string
      downloads_url:
        type: string
      events_url:
        type: string
      forks_url:
        type: string
      git_commits_url:
        type: string
      git_refs_url:
        type: string
      git_tags_url:
        type: string
      git_url:
        type: string
      issue_comment_url:
        type: string
      issue_events_url:
        type: string
      issues_url:
        type: string
      keys_url:
        type: string
      labels_url:
        type: string
      languages_url:
        type: string
      merges_url:
        type: string
      milestones_url:
        type: string
      notifications_url:
        type: string
      pulls_url:
        type: string
      releases_url:
        type: string
      ssh_url:
        type: string
      stargazers_url:
        type: string
      statuses_url:
        type: string
      subscribers_url:
        type: string
      subscription_url:
        type: string
      tags_url:
        type: string
      teams_url:
        type: string
      trees_url:
        type: string
      clone_url:
        type: string
      mirror_url:
        type: string
      hooks_url:
        type: string
      svn_url:
        type: string
      homepage:
        type: string
      language:
        type: string
      forks_count:
        type: integer
      stargazers_count:
        type: integer
      watchers_count:
        type: integer
      size:
        type: integer
      default_branch:
        type: string
      open_issues_count:
        type: integer
      is_template:
        type: boolean
      topics:
        type: array
        items:
          type: string
      has_issues:
        type: boolean
      has_projects:
        type: boolean
      has_wiki:
        type: boolean
      has_pages:
        type: boolean
      has_downloads:
        type: boolean
      archived:
        type: boolean
      disabled:
        type: boolean
      visibility:
        type: string
      pushed_at:
        type: string
      created_at:
        type: string
      updated_at:
        type: string
      permissions:
        type: object
        properties:
          admin:
            type: boolean
          push:
            type: boolean
          pull:
            type: boolean
      allow_rebase_merge:
        type: boolean
      template_repository:
        type: string
      temp_clone_token:
        type: string
      allow_squash_merge:
        type: boolean
      delete_branch_on_merge:
        type: boolean
      allow_merge_commit:
        type: boolean
      subscribers_count:
        type: integer
      network_count:
        type: integer
  temp_clone_token:
    type: string
  allow_squash_merge:
    description: Whether to allow squash merges for pull requests.
    default: true
    type: boolean
    example: true
  delete_branch_on_merge:
    description: Whether to delete head branches when pull requests are merged
    default: false
    type: boolean
    example: false
  allow_merge_commit:
    description: Whether to allow merge commits for pull requests.
    default: true
    type: boolean
    example: true
  subscribers_count:
    type: integer
  network_count:
    type: integer
  open_issues:
    type: integer
  watchers:
    type: integer
  master_branch:
    type: string
  starred_at:
    type: string
    example: '"2020-07-09T00:17:42Z"'
required:
- archive_url
- assignees_url
- blobs_url
- branches_url
- collaborators_url
- comments_url
- commits_url
- compare_url
- contents_url
- contributors_url
- deployments_url
- description
- downloads_url
- events_url
- fork
- forks_url
- full_name
- git_commits_url
- git_refs_url
- git_tags_url
- hooks_url
- html_url
- id
- node_id
- issue_comment_url
- issue_events_url
- issues_url
- keys_url
- labels_url
- languages_url
- merges_url
- milestones_url
- name
- notifications_url
- owner
- private
- pulls_url
- releases_url
- stargazers_url
- statuses_url
- subscribers_url
- subscription_url
- tags_url
- teams_url
- trees_url
- url
- clone_url
- default_branch
- forks
- forks_count
- git_url
- has_downloads
- has_issues
- has_projects
- has_wiki
- has_pages
- homepage
- language
- archived
- disabled
- mirror_url
- open_issues
- open_issues_count
- license
- pushed_at
- size
- ssh_url
- stargazers_count
- svn_url
- watchers
- watchers_count
- created_at
- updated_at
x-githubEnterpriseOverlays:
  2.18 || 2.19:
    properties:
      anonymous_access_enabled:
        type: boolean
      visibility:
      template_repository:
        properties:
          anonymous_access_enabled:
            type: boolean
          visibility:
          temp_clone_token:
          delete_branch_on_merge:
      temp_clone_token:
      delete_branch_on_merge:
  '2.20':
    properties:
      anonymous_access_enabled:
        type: boolean
      template_repository:
        properties:
          anonymous_access_enabled:
            type: boolean
          temp_clone_token:
          delete_branch_on_merge:
      temp_clone_token:
      delete_branch_on_merge:
  '2.21':
    properties:
      anonymous_access_enabled:
        type: boolean
      template_repository:
        properties:
          anonymous_access_enabled:
            type: boolean
          temp_clone_token:
      temp_clone_token:
