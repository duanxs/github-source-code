# frozen_string_literal: true

# Enterprise API. This is for admin users in Enterprise installs.
module Api
  module Enterprise
  end
end
