# rubocop:disable Style/FrozenStringLiteralComment

class Api::Authorizations < Api::App
  areas_of_responsibility :ecosystem_apps

  DEPRECATION_WARNING_INTERVAL = 1.month
  FORBIDDEN_MESSAGE = "This API can only be accessed with username and " +
                      "password Basic Auth"
  IGNORED_USER_AGENTS = [
    "git-credential-manager",
    "Intellij",
    "Hub",
    "GH/",
    "Xcode",
    "GitExtensions",
  ].map(&:downcase)

  DEPRECATION_DATE = DateTime.iso8601("2020-02-14T16:00:00+00:00")
  SUNSET_DATE = DateTime.iso8601("2020-11-13T16:00:00+00:00")
  DEPRECATION_BLOG_POST = "#{GitHub.developer_blog_url}/2020-02-14-deprecating-oauth-auth-endpoint/"

  # This action is whitelisted because it's needed for the @ecosystem-apps
  # oauth-token-exchange monitor: https://app.datadoghq.com/monitors/7443988
  statsd_tag_actions "/authorizations"

  # This API endpoint provides access to OAuth accesses. Due to backward
  # compatibility, the naming convention used in this API can be confusing.
  # Externally an OAuthAccess is called an "authorization" and an
  # OAuthAuthorization is called a "grant". The `/authorizations` endpoints
  # include the ability to list, get, create, update, and delete OAuth accesses
  # (authorizations) and the `/applications/grants` endpoints lets you list,
  # get, and delete their associated authorizations (grants).
  #
  # Because this endpoint deals with authentication credentials, it is only
  # accessible via basic authorization.
  before do
    if GitHub.flipper[:brownout_authorizations_api_removal].enabled?(current_user)
      deliver_error! 404, message: "You are receiving this error due to a service brownout. Please see #{DEPRECATION_BLOG_POST} for more information."
      return
    end

    @accepted_scopes = []
    set_forbidden_message(FORBIDDEN_MESSAGE, true)
    check_authorization { logged_in? && current_user.using_basic_auth? }
    populate_with_saml_context

    if GitHub.authorization_apis_and_password_authentication_deprecated?
      deprecated(
        deprecation_date: DEPRECATION_DATE,
        sunset_date: SUNSET_DATE,
        info_url: DEPRECATION_BLOG_POST,
        alternate_path_url: nil,
      )
    end
  end

  after do
    if logged_in? && current_user.using_basic_auth? && GitHub.authorization_apis_and_password_authentication_deprecated? && !GitHub.flipper[:brownout_authorizations_api_removal].enabled?(current_user)
      if should_notify_user_about_authorizations_api_deprecation?
        AccountMailer.authorizations_grants_api_basic_auth_deprecation(route: "Authorizations", user: current_user, user_agent: request.user_agent, blog_endpoint: "2020-02-14-deprecating-oauth-auth-endpoint/").deliver_later
      end

      if should_notify_app_owner_about_authorizations_api_deprecation?
        AccountMailer.app_owner_authorizations_grants_api_basic_auth_deprecation(route: "Authorizations", oauth_application: @authorization_application, user_agent: request.user_agent, blog_endpoint: "2020-02-14-deprecating-oauth-auth-endpoint/").deliver_later
      end
    end
  end

  # Private: Determine if we should notify a user the basic auth deprecation.
  def should_notify_user_about_authorizations_api_deprecation?
    return false if request.user_agent && IGNORED_USER_AGENTS.any? { |ua| request.user_agent.downcase.starts_with?(ua) }
    return false if @authorization_application && @authorization_application.id != OauthApplication::PERSONAL_TOKENS_APPLICATION_ID
    GitHub.flipper[:notify_about_authorizations_api_deprecation].enabled?(current_user) &&
    !GitHub.flipper[:opt_out_of_notify_about_authorizations_api_deprecation].enabled?(current_user) &&
      GitHub::ActionRestraint.perform?(
        "notify_about_authorizations_api_deprecation",
        interval: DEPRECATION_WARNING_INTERVAL,
        user_id: current_user.id,
      )
  end

  # Private: Determine if we should notify the owner of a given OAuth
  # application of the basic auth deprecation.
  def should_notify_app_owner_about_authorizations_api_deprecation?
    return false unless @authorization_application
    return false if @authorization_application.id == OauthApplication::PERSONAL_TOKENS_APPLICATION_ID
    GitHub.flipper[:notify_app_owner_about_authorizations_api_deprecation].enabled?(@authorization_application.owner) &&
    !GitHub.flipper[:opt_out_of_notify_app_owner_about_authorizations_api_deprecation].enabled?(@authorization_application.owner) &&
      GitHub::ActionRestraint.perform?(
        "notify_app_owner_about_authorizations_api_deprecation",
        interval: DEPRECATION_WARNING_INTERVAL,
        application_type: @authorization_application.class.name,
        application_id: @authorization_application.id,
      )
  end

  # While we are deprecating password auth, we should respond to them with more specificity
  # than the generic mailer.
  def password_auth_deprecated?
    false
  end

  # POST/PUT/PATCH requests should send an SMS for 2FA for the following routes:
  #
  # /authorizations
  # /authorizations/authorization_id
  # /authorizations/clients/client_id
  def route_sends_otp_sms?
    request.post? || request.put? || request.patch?
  end

  # List OAuth accesses
  #
  # Returns a list of OAuth accesses for the logged in user.
  get "/authorizations" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = "/rest/reference/oauth-authorizations#list-your-authorizations"
    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false

    scoped = current_user.oauth_accesses
    if key = params[:client_id].presence
      scoped = scoped.for_client_id(key)
      @authorization_application = OauthApplication.where(key: params[:client_id]).first || Integration.where(key: params[:client_id]).first
    end
    accesses = paginate_rel(scoped)

    GitHub::PrefillAssociations.for_oauth_accesses(accesses)

    deliver :oauth_access_hash, accesses
  end

  # Get a specific OAuth access
  #
  # Returns an OAuth access.
  get "/authorizations/:authorization_id" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = "/rest/reference/oauth-authorizations#get-a-single-authorization"
    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false

    if access = current_user.oauth_accesses.find_by_id(int_id_param!(key: :authorization_id))
      populate_with_app_context(access.application)
      @authorization_application = access.application

      GitHub::PrefillAssociations.for_oauth_accesses([access])
      deliver :oauth_access_hash, access,
        last_modified: calc_last_modified(access)
    else
      deliver_error 404
    end
  end

  # Create an OAuth access
  #
  # Creates a new OAuth access tied to the OAuth application specified by
  # client_id and client_secret OR creates a personal token tied to an 'API'
  # oauth application (id = 0).
  post "/authorizations" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = "/rest/reference/oauth-authorizations#create-a-new-authorization"
    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false

    # Introducing strict validation of the authorization.create
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    data = receive_with_schema("authorization", "create", skip_validation: true)
    client_id     = data["client_id"].to_s
    client_secret = data["client_secret"].to_s

    app = OauthApplication.pseudo if (client_id.empty? || client_secret.empty?)
    app ||= find_oauth_app(client_id, client_secret)

    if app.nil?
      deliver_error! 422, message: "Invalid Application client_id or secret."
    end

    @authorization_application = app
    populate_with_app_context(app)

    if app.key == OauthApplication::GITHUB_DESKTOP_CLIENT_ID
      if Platform::Authorization::SAML.new(user: current_user).saml_organizations.any?
        if GitHub.flipper[:authorizations_api_disabled].enabled?(current_user)
          deliver_error!(410, message: "This action is no longer available via the API")
        end
      end
    end

    token, hashed_token = OauthAccess.random_token_pair
    last_operations = DatabaseSelector::LastOperations.from_token(token)
    access = current_user.oauth_accesses.build(
      application: app,
      scopes: Array(data["scopes"]),
      note: data["note"],
      note_url: data["note_url"],
      fingerprint: data["fingerprint"],
      token_last_eight: token.last(8),
      hashed_token: hashed_token,
    )

    begin
      if !(access.valid? && access.save)
        deliver_error 422,
          errors: access.errors,
          documentation_url: @documentation_url
      else
        # After saving the new token we want to set the last write timestamp in
        # the cache, so the api DatabaseSelection can use the write DB for newly
        # created tokens and avoid issues due to replication lag.
        last_operations.update_last_write_timestamp
        deliver :oauth_access_hash, access, status: 201, token: token
      end
    rescue ActiveRecord::RecordNotUnique
      deliver_error! 422,
        message: "An authorization already exists with the given data." \
          " Please provide a unique fingerprint and note and try again."
    end
  end

  # Create an OAuth access for a given application (and fingerprint if
  # provided)
  #
  # Finds the application by its id (and fingerprint if provided) and creates an
  # OAuth access for it. Returns the Oauth access.
  put "/authorizations/clients/:client_id" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = "/rest/reference/oauth-authorizations#get-or-create-an-authorization-for-a-specific-app"
    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false

    data = receive_with_schema("authorization", "get-or-create-for-app")

    client_id     = params["client_id"].to_s
    client_secret = data["client_secret"].to_s

    app = find_oauth_app(client_id, client_secret)

    if app.nil?
      deliver_error! 422, message: "Invalid Application client_id or secret."
    end

    @authorization_application = app
    populate_with_app_context(app)

    fingerprint = data["fingerprint"]

    access = current_user.oauth_accesses.where(
      application: app,
      fingerprint: fingerprint.present? ? fingerprint : nil,
    ).first

    if access
      GitHub::PrefillAssociations.for_oauth_accesses([access])
      deliver :oauth_access_hash, access, status: 200
    else
      token, hashed_token = OauthAccess.random_token_pair
      last_operations = DatabaseSelector::LastOperations.from_token(token)
      access = current_user.oauth_accesses.build(
        application: app,
        scopes: Array(data["scopes"]),
        note: data["note"],
        note_url: data["note_url"],
        fingerprint: data["fingerprint"],
        token_last_eight: token.last(8),
        hashed_token: hashed_token,
      )

      begin
        if !(access.valid? && access.save)
          deliver_error 422,
            errors: access.errors,
            documentation_url: @documentation_url
        else
          # After saving the new token we want to set the last write timestamp in
          # the cache, so the api DatabaseSelection can use the write DB for newly
          # created tokens and avoid issues due to replication lag.
          last_operations.update_last_write_timestamp
          deliver :oauth_access_hash, access, status: 201, token: token
        end
      rescue ActiveRecord::RecordNotUnique
        deliver_error! 422,
          message: "An authorization already exists with the given data." \
          " Please provide a unique fingerprint and note and try again."
      end
    end
  end

  # Create an OAuth access for a given application and fingerprint
  #
  # Finds the application by its id and fingerprint and creates an OAuth
  # access for it. Returns the OAuth access.
  put "/authorizations/clients/:client_id/:fingerprint" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = "/rest/reference/oauth-authorizations#get-or-create-an-authorization-for-a-specific-app-and-fingerprint"
    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false

    # Introducing strict validation of the authorization.get-or-create-for-fingerprint
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    data = receive_with_schema("authorization", "get-or-create-for-fingerprint", skip_validation: true)
    client_id     = params["client_id"].to_s
    client_secret = data["client_secret"].to_s

    app = find_oauth_app(client_id, client_secret)

    if app.nil?
      deliver_error! 422, message: "Invalid Application client_id or secret."
    end

    @authorization_application = app
    populate_with_app_context(app)

    fingerprint = params["fingerprint"].to_s

    access = current_user.oauth_accesses.where(
      application: app,
      fingerprint: fingerprint,
    ).first

    if access
      GitHub::PrefillAssociations.for_oauth_accesses([access])
      deliver :oauth_access_hash, access, status: 200
    else
      data = data.merge("fingerprint" => fingerprint)
      token, hashed_token = OauthAccess.random_token_pair
      last_operations = DatabaseSelector::LastOperations.from_token(token)
      access = current_user.oauth_accesses.build(
        application: app,
        scopes: Array(data["scopes"]),
        note: data["note"],
        note_url: data["note_url"],
        fingerprint: data["fingerprint"],
        token_last_eight: token.last(8),
        hashed_token: hashed_token,
      )

      begin
        if !(access.valid? && access.save)
          deliver_error 422,
            errors: access.errors,
            documentation_url: @documentation_url
        else
          # After saving the new token we want to set the last write timestamp in
          # the cache, so the api DatabaseSelection can use the write DB for newly
          # created tokens and avoid issues due to replication lag.
          last_operations.update_last_write_timestamp
          deliver :oauth_access_hash, access, status: 201, token: token
        end
      rescue ActiveRecord::RecordNotUnique
        deliver_error! 422,
          message: "An authorization already exists with the given data." \
          " Please provide a unique fingerprint and note and try again."
      end
    end
  end

  # Update an OAuth access
  #
  # Updates and returns an OAuth access.
  verbs :patch, :post, "/authorizations/:authorization_id" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = "/rest/reference/oauth-authorizations#update-an-existing-authorization"
    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false

    if (access = current_user.oauth_accesses.find_by_id(int_id_param!(key: :authorization_id)))
      populate_with_app_context(access.application)
      @authorization_application = access.application

      # Introducing strict validation of the authorization.update
      # JSON schema would cause breaking changes for integrators
      # skip_validation until a rollout strategy can be determined
      # see: https://github.com/github/ecosystem-api/issues/1555
      data = receive_with_schema("authorization", "update", skip_validation: true)

      if (scopes = Array(data["scopes"])).present?
        access.scopes = scopes
      elsif (scopes = Array(data["add_scopes"])).present?
        access.scopes |= scopes
      elsif (scopes = Array(data["remove_scopes"])).present?
        access.scopes -= scopes
      end

      # Older tokens will have a code set that is not needed.
      access.code = nil
      access.note_url = data["note_url"] if data.key?("note_url")
      access.description = data["note"] if data.key?("note")
      access.fingerprint = data["fingerprint"] if data.key?("fingerprint")

      if access.save
        deliver :oauth_access_hash, access
      else
        deliver_error 422,
          errors: access.errors,
          documentation_url: "/rest/reference/oauth-authorizations#update-an-existing-authorization"
      end
    else
      deliver_error 404
    end
  end

  # Delete an OAuth access
  #
  # Destroys an OAuth access.
  delete "/authorizations/:authorization_id" do
    @route_owner = "@github/ecosystem-apps"

    # Introducing strict validation of the authorization.delete
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("authorization", "delete", skip_validation: true)

    @documentation_url = "/rest/reference/oauth-authorizations#delete-an-authorization"
    control_access :apps_audited, allow_integrations: false, allow_user_via_integration: false

    if access = current_user.oauth_accesses.find_by_id(int_id_param!(key: :authorization_id))
      populate_with_app_context(access.application)
      @authorization_application = access.application

      access.destroy_with_explanation(:api_user)
      deliver_empty(status: 204)
    else
      deliver_error 404
    end
  end

  private
  def find_oauth_app(client_id, client_secret)
    app = OauthApplication.where(key: client_id).find do |application|
      application.plaintext_secret == client_secret
    end

    app || Integration.where(key: client_id).find do |application|
      application.plaintext_secret == client_secret
    end
  end

  def populate_with_app_context(app)
    case app
    when OauthApplication
      log_data[:oauth_application_id] = app.id
      GitHub.context.push(oauth_application_id: app.id)
    when Integration
      log_data[:integration_id] = app.id
      GitHub.context.push(integration_id: app.id)
    end
  end

  def populate_with_saml_context
    log_data[:saml_org_member] = Platform::Authorization::SAML.new(user: current_user).saml_organizations.any?
  end
end
