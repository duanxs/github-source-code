# frozen_string_literal: true

class Api::PullRemindersMigration < Api::App
  post "/pull_reminders/migration" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    organization = current_integration_installation&.target

    deliver_error!(404) unless organization.is_a?(Organization)
    deliver_error!(404) unless GitHub.flipper[:scheduled_reminders_frontend].enabled?(organization)

    control_access(
      :pull_reminders_migration,
      resource: organization,
      allow_integrations: true,
      allow_user_via_integration: false,
    )

    data = receive_with_schema("pull-reminders-migration", "migrate")

    if data["channel_id"]
      Reminders::MigrateEligibleRemindersJob.perform_later(
        organization,
        workspace_id: data["workspace_id"],
        channel_id: data["channel_id"],
        ignore_team_warnings: data["ignore_team_warnings"]
      )
    elsif data["username"]
      user = User.find_by(login: data["username"])

      if organization.member?(user)
        Reminders::MigratePersonalReminderJob.perform_later(organization, workspace_id: data["workspace_id"], username: user.login)
      else
        deliver_error!(404)
      end
    else
      deliver_error!(422)
    end

    deliver_empty(status: 204)
  end
end
