# rubocop:disable Style/FrozenStringLiteralComment

module Api::Serializer
  extend self
  extend UrlHelper

  extend Api::Serializer::GraphqlHelperDependency

  # These depend only on the graphql dependencies, and nobody
  # depends on them in turn.
  extend Api::Serializer::MarketplaceListingDependency
  extend Api::Serializer::TopicsDependency
  extend Api::Serializer::VerifiableDependency

  # These depend only on the graphql dependencies,
  # and other dependencies depend on them in turn.
  # Order is important.
  extend Api::Serializer::CodesOfConductDependency
  extend Api::Serializer::CommitsDependency
  extend Api::Serializer::LicensesDependency
  extend Api::Serializer::UserDependency
  extend Api::Serializer::ProjectsDependency
  extend Api::Serializer::ReactionsDependency
  extend Api::Serializer::OrganizationsDependency
  extend Api::Serializer::IntegrationsDependency
  extend Api::Serializer::PullRequestReviewsDependency
  extend Api::Serializer::RepositoriesDependency
  extend Api::Serializer::TeamDiscussionsDependency
  extend Api::Serializer::TeamRepositoryDependency
  extend Api::Serializer::IssuesDependency
  extend Api::Serializer::PullsDependency
  extend Api::Serializer::ChecksDependency
  extend Api::Serializer::WorkflowsDependency
  extend Api::Serializer::JobsDependency

  # These don't rely on any other dependencies, and nothing
  # relies on them in return.
  # They can therefore be included at the end, in any order.
  extend Api::Serializer::ActionsRunnersDependency
  extend Api::Serializer::ActionsSecretsDependency
  extend Api::Serializer::CodeScanningDependency
  extend Api::Serializer::ArtifactsDependency
  extend Api::Serializer::ContentReferencesDependency
  extend Api::Serializer::CouponDependency
  extend Api::Serializer::CredentialAuthorizationsDependency
  extend Api::Serializer::DeploymentStatusesDependency
  extend Api::Serializer::DeploymentsDependency
  extend Api::Serializer::EnterprisesDependency
  extend Api::Serializer::EphemeralNoticeDependency
  extend Api::Serializer::EventsDependency
  extend Api::Serializer::GhvfsDependency
  extend Api::Serializer::GistsDependency
  extend Api::Serializer::GitDataDependency
  extend Api::Serializer::HooksDependency
  extend Api::Serializer::ImporterDependency
  extend Api::Serializer::IntegrationInstallationsDependency
  extend Api::Serializer::InteractiveComponentDependency
  extend Api::Serializer::InternalDependency
  extend Api::Serializer::LegacyDependency
  extend Api::Serializer::MigrationsDependency
  extend Api::Serializer::NotificationsDependency
  extend Api::Serializer::OrganizationTeamSyncDependency
  extend Api::Serializer::PorterDependency
  extend Api::Serializer::PreReceiveDependency
  extend Api::Serializer::RateLimitDependency
  extend Api::Serializer::RegistryPackagesDependency
  extend Api::Serializer::RegistryPackagesV2Dependency
  extend Api::Serializer::ReminderDependency
  extend Api::Serializer::RepositoryVulnerabilityAlertsDependency
  extend Api::Serializer::ScimDependency
  extend Api::Serializer::SearchDependency
  extend Api::Serializer::SecurityAdvisoriesDependency
  extend Api::Serializer::SponsorsTierDependency
  extend Api::Serializer::StafftoolsRoleDependency
  extend Api::Serializer::StarsDependency
  extend Api::Serializer::StatsDependency
  extend Api::Serializer::StatusesDependency
  extend Api::Serializer::TrafficDependency
  extend Api::Serializer::UploadableDependency
  extend Api::Serializer::VulnerabilitiesDependency
  extend Api::Serializer::CodespacesDependency

  def diff_url(repo, diff, type = "blob")
    return nil if diff.submodule?

    sha, path = diff_sha_and_path(diff)
    url = "#{GitHub.url}/#{repo}/#{type}/#{sha}/#{path}"
    encode(url)
  end

  def diff_contents_url(repo, diff)
    sha, path = diff_sha_and_path(diff)
    url = url_with_query("#{GitHub.api_url}/repos/#{repo}/contents/#{path}", ref: sha)
    encode(url)
  end

  def diff_sha_and_path(diff)
    diff.deleted? ? [diff.a_sha, diff.a_path] : [diff.b_sha, diff.b_path]
  end

  def avatar(user, size = nil)
    u = user.primary_avatar_url(size)
    u << "?" unless u["?"]
    u # lets people append "size=140" to the url
  end

  # TODO: avatar-urls feature flag.
  def event_avatar_url(user_hash, org = false)
    "#{GitHub.alambic_avatar_url}/u/#{user_hash["id"]}?"
  end

  def avatar_url(gravatar_id, org = false, size = nil)
    default_image = org ? "gravatar-org-420" : "gravatar-user-420"
    avatars = User::AvatarList.with_gravatar_id(gravatar_id)
    strip_gravatar_subdomain(avatars.gravatar_url(size, default_image))
  end

  # Public: The absolute URL to a preferred file (CONTRIBUTING, README, etc.)
  #
  # type - A Symbol type from PreferredFile::Types.
  # repository - A Repository to find the file in.
  #
  # See preferred_file_path in UrlHelper
  #
  # Returns a String or nil.
  def preferred_file_url(type:, repository:)
    path = preferred_file_path(type: type, repository: repository)
    return unless path
    "#{base_url}#{path}"
  end

  def serialize(method, obj, options = {})
    case obj
    when Array, WillPaginate::Collection, ActiveRecord::Relation, ActiveRecord::Associations::CollectionProxy
      obj.map { |resource| track_serialization(method, resource, options) }
    else # ActiveRecord::Base, Hash
      track_serialization(method, obj, options)
    end
  end

  def track_serialization(method, resource, options)
    initial_query_count = GitHub::MysqlInstrumenter.query_count
    tracked = resource.try(:track_loaded_associations)
    octotracer_span = GitHub.tracer.start_span("serialize.#{method}")
    GitHub.dogstats.time("serialization", tags: ["via:api", "method:#{method}"], sample_rate: 0.01) { send(method, resource, options) }
  ensure
    # We should have all the data before needing to serialize, so there should be
    # no database queries made by the time we get here
    final_query_count = GitHub::MysqlInstrumenter.query_count
    diff = final_query_count - initial_query_count
    octotracer_span.set_tag("mysql_queries", diff)
    GitHub.dogstats.count("serialization.queries", diff)
    # As above, we should not be loading any associations at this time
    if tracked
      loaded_associations_count = tracked.count
      octotracer_span.set_tag("lazy_loaded_associations", loaded_associations_count)
      GitHub.dogstats.count("serialization.lazy_loaded_associations", loaded_associations_count, tags: ["via:api", "method:#{method}"])
      if Rails.env.test? && loaded_associations_count > 0
        raise "Found #{loaded_associations_count} #{'record'.pluralize(loaded_associations_count)} excessively loaded at #{options[:route]}: #{tracked.loaded_association_names}"
      end
    end
    octotracer_span.finish
  end

  def validation_errors(validation_errors, resource = nil)
    errors = []

    resource = resource.to_s if resource.is_a?(Symbol)

    if resource && !resource.is_a?(String)
      Failbot.report(ArgumentError.new("Resource of type `#{resource.class.name}` is not a string."))
      resource = resource.class.name
    end

    resource ||= validation_errors.instance_variable_get(:@base).class.name

    opt = {resource: resource}
    if GitHub.rails_6_0?
      validation_errors.each do |key, message|
        if code = Api::App::ERROR_MAP[message]
          errors << opt.merge(code: code, field: key)
        else
          if key.to_s == "base"
            errors << opt.merge(code: :custom, message: message)
          else
            errors << opt.merge(code: :custom, field: key, message: "#{key} #{message}")
          end
        end
      end
    else
      validation_errors.each do |error|
        # Sinatra does not use the new error object API like rails does,
        # So if this error came from Sinatra it will still be an Array
        # instead of an AM:Error object.
        if error.is_a?(Array)
          key = error[0]
          message = error[1]
        else
          key = error.attribute
          message = error.message
        end

        if code = Api::App::ERROR_MAP[message]
          errors << opt.merge(code: code, field: key)
        else
          if key.to_s == "base"
            errors << opt.merge(code: :custom, message: message)
          else
            errors << opt.merge(code: :custom, field: key, message: "#{key} #{message}")
          end
        end
      end
    end
    errors
  end

  ValidationErrorFragment = Api::App::PlatformClient.parse <<-'GRAPHQL'
    fragment on ValidationError {
      shortMessage
      attribute
      path
    }
  GRAPHQL

  def url(suffix, options = nil)
    "#{GitHub.api_url}#{suffix}"
  end

  def html_url(suffix, options = nil)
    url_with_query GitHub.url + suffix.to_s, options
  end

  def encoded_html_url(prefix, path)
    encode(prefix) + u(path)
  end

  def encoded_content_url(prefix, path, query = {})
    url_with_query(encode(url(prefix)) + u(path), query)
  end

  def model_id_for_global_id(global_id)
    _, model_id = Platform::Helpers::NodeIdentification.from_global_id(global_id)

    model_id.to_i
  end

  def time(t)
    t ? t.to_time.utc.xmlschema : nil
  end

  private
  def url_with_query(path, options)
    return path if options.nil? || options.empty?
    parts = []
    options.each do |key, value|
      parts << "#{u(key)}=#{u(value)}"
    end

    "#{path}?#{parts * "&"}"
  end

  def content_options(options)
    { accept_mime_types: options[:accept_mime_types] }
  end

  # Escapes any characters not allowed in a URL into their hex equivalents,
  # including non-ascii unicode characters that may appear in user created
  # directory or file names.
  #
  # Examples
  #
  #   url = 'https://api.github.com/repos/github/hubot/contents/más?q=olé'
  #   url = encode(url)
  #   # => 'https://api.github.com/repos/github/hubot/contents/m%C3%A1s?q=ol%C3%A9'
  #
  # url - The String URL to escape.
  #
  # Returns an escaped URL String.
  def encode(url)
    Addressable::URI.encode(url)
  end

  def u(s)
    Api::LegacyEncode.encode(s.to_s, /[^-_.!~*'()a-zA-Z\d;\/:@&=$,\[\]]/n)
  end

  def repo_path(options, record)
    if path = options[:repo_path]
      return path
    end

    repo = options[:repo] || record.repository
    repo.respond_to?(:name_with_owner) ?
      repo.name_with_owner :
      repo.to_s
  end

  def time_from_string(iso8601_timestamp_string)
    return if iso8601_timestamp_string.nil?

    GitHub::TimezoneTimestamp.iso8601 iso8601_timestamp_string
  end

  def strip_gravatar_subdomain(url)
    url.sub %r(\Ahttps://\d.gravatar.com), "https://gravatar.com"
  end

  # Encode a Ruby object using Base64.encode and optionally
  # time the operation.
  #
  # data - Object data to be encoded
  # options - Hash
  #         - :stats_key String key for GitHub.dogstats.
  def encode_base64(data, options = {})
    if key = options[:stats_key]
      GitHub.dogstats.time("base64", tags: ["via:api", "key:#{key}"]) do
        Base64.encode64(data)
      end
    else
      Base64.encode64(data)
    end
  end

  # Handle various body formats for Accept mime types
  #
  # record - an object that responds to #body, #body_html, and #body_text
  # options - Hash
  def mime_body_hash(record, options)
    params   = options[:mime_params] || []
    full     = params.include? :full
    any_body = false
    hash     = {}
    if full || params.include?(:html)
      any_body = true
      hash.update body_html: record.body_html
    end
    if full || params.include?(:text)
      any_body = true
      hash.update body_text: record.body_text
    end
    if full || !any_body || params.include?(:raw)
      hash.update body: record.body
    end

    hash
  end
end
