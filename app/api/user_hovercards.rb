# frozen_string_literal: true

class Api::UserHovercards < Api::App
  areas_of_responsibility :api, :user_profile

  HovercardQuery = PlatformClient.parse <<-'GRAPHQL'
    query($userId: ID!, $primarySubjectId: ID) {
      node(id: $userId) {
        ... on User {
          hovercard(primarySubjectId: $primarySubjectId) {
            organization {
              databaseId
            }
            ...Api::Serializer::UserDependency::HovercardFragment
          }
        }
      }
    }
  GRAPHQL

  # Get the information contained in a user hovercard
  get "/user/:user_id/hovercard" do
    @route_owner = "@github/profile"
    @documentation_url = "/rest/reference/users#get-contextual-information-for-a-user"

    user = find_user!

    subject = nil
    if params[:subject_id]
      subject = Hovercard::SUBJECT_PREFIX_MAP[params[:subject_type]]&.find_by_id(params[:subject_id])
      record_or_404(subject)
    end

    results = platform_execute(HovercardQuery, variables: {
      userId: user.global_relay_id,
      primarySubjectId: subject&.global_relay_id,
    })

    # For OAuth application policy enforcement
    if results.errors.none? && org = results.data.node&.hovercard&.organization
      @current_org = Organization.find(org.database_id)
    end

    control_access :read_user_hovercard, {
      challenge: true,
      allow_integrations: false,
      allow_user_via_integration: false,
      enforce_oauth_app_policy: true,
    }

    if results.errors.all.any?
      deprecated_deliver_graphql_error(errors: results.errors.all, resource: "User", documentation_url: @documentation_url)
    else
      deliver :graphql_hovercard_hash, results.data.node.hovercard
    end
  end
end
