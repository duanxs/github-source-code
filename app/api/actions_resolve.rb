# frozen_string_literal: true

class Api::ActionsResolve < Api::App
  map_to_service :actions_experience
  statsd_tag_actions "/repositories/:repository_id/actions/resolve/:ref"

  get "/repositories/:repository_id/actions/resolve/:ref" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL
    require_authentication!

    repo = find_repo!

    control_access :resolve_actions,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true

    deliver_error!(404) unless GitHub.actions_enabled?
    deliver_error!(404) unless actions_connect_request?
    # Synced actions don't have a RepositoryAction on GHES.
    deliver_error!(422, message: resolve_action_error_message(repo, "repository is missing an action.yml file")) unless repo.actions.any? || GitHub.enterprise?

    ref = params[:ref]
    sha = repo.ref_to_sha(ref)
    deliver_error!(404, message: resolve_action_error_message(repo, "unable to find version `#{ref}`")) unless sha

    GitHub.dogstats.increment(
      "api.actions.resolve_action",
      tags: ["connect:#{connect_request?}"]
    )
    # If the original action is sent to the request, use that as the action
    # name.
    action_name = params["_action"] || repo.name_with_owner

    response = {
      name: action_name,
      resolved_name: repo.name_with_owner,
      resolved_sha: sha,
      tar_url: tarball_path(repo, sha),
      zip_url: zipball_path(repo, sha),
      version: ref,
    }

    deliver_raw response, status: 200
  end

private
  def zipball_path(repo, name)
    "#{GitHub.api_url}/repos/#{repo.name_with_owner}/zipball/#{name}"
  end

  def tarball_path(repo, name)
    "#{GitHub.api_url}/repos/#{repo.name_with_owner}/tarball/#{name}"
  end

  def resolve_action_error_message(repo, reason)
    base_message = "Unable to resolve action `#{repo.nwo}@#{params[:ref]}`"

    if reason
      return "#{base_message}, #{reason}"
    end

    return base_message
  end
end
