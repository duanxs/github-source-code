# rubocop:disable Style/FrozenStringLiteralComment

require "github/null_objects"

class Api::Users < Api::App
  areas_of_responsibility :enterprise_only, :user_profile, :api, :marketplace

  statsd_tag_actions "/user"
  # REF: https://github.com/github/github/issues/113443#issuecomment-488839528
  # Can be removed once the issue is closed
  statsd_tag_actions "/user/installations"

  map_to_service :apps, only: [
    "GET /user/installations"
  ]

  include Scientist

  USER_VIA_GITHUB_APP_ACCESS_ONLY = "You must authenticate with an access token authorized to a GitHub App in order to ".freeze

  # Get the authenticated User
  get "/user" do
    @route_owner = "@github/profile"
    @documentation_url = "/rest/reference/users#get-the-authenticated-user"
    control_access :read_user,
      resource: current_user,
      challenge: true,
      allow_integrations: false,
      allow_user_via_integration: true,
      installation_required: false

    enable_client_apps

    if GitHub.flipper[:prefill_profile].enabled?
      GitHub::PrefillAssociations.for_users([current_user])
    end
    GitHub::PrefillAssociations.for_user_repository_counts([current_user])

    options = {
      response_key        => true,
      :plan               => access_allowed?(:read_user_plan, resource: current_user, allow_integrations: false, allow_user_via_integration: true, installation_required: false),
      :last_modified      => calc_last_modified(current_user),
      :compare_payload_to => :user_candidate,
    }

    deliver :user_hash, current_user, options
  end

  ViewerQuery = PlatformClient.parse <<-'GRAPHQL'
    query($private: Boolean!) {
      viewer {
        ...Api::Serializer::UserDependency::PrivateUserFragment @include(if: $private)
        ...Api::Serializer::UserDependency::FullUserFragment @skip(if: $private)
      }
    }
  GRAPHQL

  MarketplacePurchasesQuery = PlatformClient.parse <<-'GRAPHQL'
    query($planBulletsLimit: Int!, $marketplaceListingId: ID!, $subscriptionItemLimit: Int!, $numericPage: Int) {
      viewer {
        marketplaceSubscriptions(marketplaceListingId: $marketplaceListingId, first: $subscriptionItemLimit, numericPage: $numericPage) {
          totalCount
          edges {
            node {
              ...Api::Serializer::MarketplaceListingDependency::SubscriptionItemFragment
            }
          }
        }
      }
    }
  GRAPHQL

  def user_candidate(_user, options)
    variables = {
      "private" => !!options[:private],
    }

    results = platform_execute(ViewerQuery, variables: variables)

    viewer = results.data.viewer

    if options[:private]
      fragment_type = Api::Serializer::UserDependency::PrivateUserFragment
      Api::Serializer.serialize(:graphql_private_user_hash, fragment_type.new(viewer), options)
    else
      fragment_type = Api::Serializer::UserDependency::FullUserFragment
      Api::Serializer.serialize(:graphql_full_user_hash, fragment_type.new(viewer), options)
    end
  end

  # Update the authenticated User
  verbs :patch, :post, "/user" do
    @route_owner = "@github/profile"
    @documentation_url = "/rest/reference/users#update-the-authenticated-user"
    control_access :update_user, resource: current_user, challenge: true, allow_integrations: false, allow_user_via_integration: false

    # Introducing strict validation of the user.update-authentication
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    data = receive_with_schema("user", "update-authenticated", skip_validation: true)

    keys = [
      :name,
      :email,
      :blog,
      :company,
      :location,
      :hireable,
      :bio,
      :twitter_username,
    ]

    attributes = attr(data,
      *keys,
      { prefix: "profile_" },
    )

    if current_user.update(attributes)
      deliver :user_hash, current_user, private: true
    else
      deliver_error 422,
        errors: current_user.errors,
        documentation_url: "/rest/reference/users#update-the-authenticated-user"
    end
  end

  get "/user/marketplace_purchases" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/apps#list-subscriptions-for-the-authenticated-user"
    control_access :read_user,
      resource: current_user,
      challenge: true,
      allow_integrations: false,
      allow_user_via_integration: true,
      installation_required: false

    return deliver_raw([]) unless current_user.oauth_access&.application

    marketplace_listing = current_user.oauth_access.application.marketplace_listing
    return deliver_raw([]) unless marketplace_listing

    variables = {
      "planBulletsLimit"     => Marketplace::ListingPlanBullet::BULLET_LIMIT_PER_LISTING_PLAN,
      "marketplaceListingId" => marketplace_listing.global_relay_id,
      "subscriptionItemLimit" => pagination[:per_page] || MAX_PER_PAGE,
      "numericPage" => pagination[:page],
    }

    results = platform_execute(MarketplacePurchasesQuery, variables: variables)

    error_type = results.errors.any? && results.errors.details["data"]&.first["type"]
    deliver_error! 404 if error_type == "NOT_FOUND"

    subscriptions = results.data.viewer.marketplace_subscriptions
    subscription_items = subscriptions.edges.map(&:node)
    paginator.collection_size = subscriptions.total_count

    deliver :graphql_marketplace_purchase_hash, subscription_items
  end

  # List a user's Marketplace purchases
  get "/user/marketplace_purchases/stubbed" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/apps#list-subscriptions-for-the-authenticated-user-stubbed"

    control_access :read_user,
      resource: current_user,
      challenge: true,
      allow_integrations: false,
      allow_user_via_integration: true

    deliver_raw Marketplace::StubbedResponse.purchases
  end

  # List the installations the authorized user has access to.
  get "/user/installations" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/apps#list-app-installations-accessible-to-the-user-access-token"

    set_forbidden_message USER_VIA_GITHUB_APP_ACCESS_ONLY + "list installations"

    control_access :read_user_installations,
      resource: current_user,
      challenge: true,
      forbid: true,
      allow_integrations: false,
      allow_user_via_integration: true,
      installation_required: false

    installations = current_integration.installations.with_user(current_user)
    installations = installations.where.not(target_id: protected_organization_ids)

    installations = paginate_rel(installations)

    GitHub::PrefillAssociations.for_integration_installations(
      installations,
      integration: current_integration,
    )

    deliver :integration_installations_hash, {
      integration_installations: installations,
      total_count: installations.count,
    }
  end

  # Browser Session Checks
  #
  #

  # Verify if a browser session is still valid for user and OAuth application
  get "/user/sessions/active" do
    @route_owner = "@github/identity"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL
    control_access :read_user, resource: current_user, challenge: true, allow_integrations: false, allow_user_via_integration: false

    message = "This API can only be accessed with OAuth tokens."
    set_forbidden_message(message, true)
    check_authorization { logged_in? && current_user.using_oauth? }

    if current_user.active_browser_session_for_application?(current_app,
                                                            params[:browser_session_id])
      deliver_empty(status: 204)
    else
      deliver_error 404
    end
  end

  get "/users" do
    @route_owner = "@github/profile"
    @documentation_url = "/rest/reference/users#list-users"
    control_access :read_user_public, resource: Platform::PublicResource.new, allow_integrations: true, allow_user_via_integration: true

    users = User.dump_page(cursor: params[:since].to_i, page_size: per_page)
    @links.add_dump_pagination(users.last)

    if GitHub.flipper[:prefill_profile].enabled?
      GitHub::PrefillAssociations.for_users([current_user])
    end
    GitHub::PrefillAssociations.for_user_repository_counts(users)
    GitHub::PrefillAssociations.for_bots(users)
    deliver(:user_hash, users)
  end

  # Get a single User
  get "/user/:user_id" do
    @route_owner = "@github/profile"
    @documentation_url = "/rest/reference/users#get-a-user"
    control_access :read_user_public,
      resource: Platform::PublicResource.new,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: false

    user = find_user!

    if GitHub.spamminess_check_enabled? && medias.api_param?(:spam)
      control_access :read_spammy_state, resource: user, allow_integrations: true, allow_user_via_integration: true

      if user.spammy?
        deliver_empty status: 202
      else
        deliver_error!(404)
      end
    else
      GitHub::PrefillAssociations.for_user_repository_counts([user])
      if GitHub.flipper[:prefill_profile].enabled?
        GitHub::PrefillAssociations.for_users([user])
      end
      GitHub::PrefillAssociations.for_bots([user])
      GitHub::PrefillAssociations.fill_ldap_mapping([user]) if GitHub.enterprise?

      deliver(
        :user_hash,
        user,
        response_key(user) => true,
        :plan => access_allowed?(:read_user_plan, resource: user, allow_integrations: false, allow_user_via_integration: true, installation_required: false),
        :last_modified => calc_last_modified(user),
        :exclude_email => anonymous_request?,
      )
    end
  end

  # Promote a user to site admin
  put "/user/:user_id/site_admin" do
    @route_owner = "@github/identity"
    @documentation_url = "/user/rest/reference/enterprise-admin#promote-a-user-to-be-a-site-administrator"
    deliver_error!(404) if GitHub.dotcom_request? || GitHub.private_instance?
    set_forbidden_message \
      "You must be an admin to promote another user to an admin."

    user = find_user!

    if current_user == user
      deliver_error! 403,
        message: "Whoa there. You can't revoke your own admin privileges."
    end

    control_access :promote_dotcom_user, allow_integrations: false, allow_user_via_integration: false
    user.grant_site_admin_access("Promoted via API by #{current_user.login}")

    deliver_empty status: 204
  end

  # Demote a user from site admin
  delete "/user/:user_id/site_admin" do
    @route_owner = "@github/identity"
    deliver_error!(404) if GitHub.dotcom_request? || GitHub.private_instance?

    @documentation_url = "/user/rest/reference/enterprise-admin#demote-a-site-administrator"

    set_forbidden_message \
      "You must be an admin to revoke a user's admin privileges."

    user = find_user!

    if current_user == user
      deliver_error! 403,
        message: "Whoa there. You can't revoke your own admin privileges."
    end

    control_access :demote_site_admin, allow_integrations: false, allow_user_via_integration: false
    user.revoke_privileged_access("Demoted via API by #{current_user.login}")

    deliver_empty status: 204
  end

  # Suspend a user
  put "/user/:user_id/suspended" do
    @route_owner = "@github/identity"
    deliver_error! 404 unless GitHub.enterprise?

    @documentation_url = "/user/rest/reference/enterprise-admin#suspend-a-user"

    set_forbidden_message "You must be an admin to suspend a user."
    control_access :suspend, allow_integrations: false, allow_user_via_integration: false

    user = find_user!
    if current_user == user
      deliver_error! 403,
        message: "You can't suspend yourself. " \
          "You'll have to convince another admin to do that for you."
    end

    if user.organization?
      deliver_error! 422,
        message: "Organizations cannot be suspended."
    end

    if user.external_account_suspension?
      deliver_error! 403,
        message: "Account suspension is managed by Active Directory. " \
          "Disable the user from your directory."
    end

    data = receive_with_schema("user", "update-legacy")

    if data && data.key?("reason")
      reason = data["reason"]
    else
      reason = "Suspended via API by #{current_user.login}"
    end

    if user.suspend(reason)
      deliver_empty status: 204
    else
      deliver_error! 403, message: user.errors[:base].to_sentence
    end
  end

  # Unsuspend a user
  delete "/user/:user_id/suspended" do
    @route_owner = "@github/identity"
    deliver_error! 404 unless GitHub.enterprise?

    @documentation_url = "/user/rest/reference/enterprise-admin#unsuspend-a-user"

    set_forbidden_message "You must be an admin to restore a suspended user."
    control_access :unsuspend, allow_integrations: false, allow_user_via_integration: false

    user = find_user!

    if user.external_account_suspension?
      deliver_error! 403,
        message: "Account suspension is managed by Active Directory. " \
          "Disable the user from your directory."
    end

    data = receive_with_schema("user", "delete-legacy")

    if data && data.key?("reason")
      reason = data["reason"]
    else
      reason = "Unsuspended via API by #{current_user.login}"
    end

    if user.unsuspend(reason)
      deliver_empty status: 204
    else
      deliver_error! 403, message: user.errors[:base].to_sentence
    end
  end

  private

  def response_key(user = nil)
    user ||= current_user

    if access_allowed?(:read_user_private, resource: user, allow_integrations: false, allow_user_via_integration: false)
      :private
    else
      :full
    end
  end

  # Internal: Instrument usage for known GitHub-owned client apps based on user
  # agent.
  #
  # Returns nothing.
  def enable_client_apps
    return unless logged_in?

    current_user.enable_mac_app if user_agent.github_mac?
    current_user.enable_windows_app if user_agent.github_windows?
    current_user.enable_visual_studio_app if user_agent.github_visual_studio?
    current_user.enable_xcode_app if user_agent.xcode?

    if user_agent.github_desktop_tng?
      platform = if user_agent.running_mac_platform?
        :mac
      elsif user_agent.running_windows_platform?
        :windows
      end

      current_user.enable_desktop_app(platform)
    end
  end
end
