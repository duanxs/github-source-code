# rubocop:disable Style/FrozenStringLiteralComment

class Api::IssueComments < Api::App
  include Api::Issues::EnsureIssuesEnabled

  # Get Comments for a Repo
  get "/repositories/:repository_id/issues/comments" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/issues#list-issue-comments-for-a-repository"
    repo = find_repo!
    control_access :list_all_issues_comments,
      repo: repo,
      allow_integrations: true,
      allow_user_via_integration: true,
      approved_integration_required: false

    cap_paginated_entries!

    issues_read = repo.resources.issues.readable_by?(current_user)
    pull_requests_read = repo.resources.pull_requests.readable_by?(current_user)

    scope = repo.issue_comments

    scope = if issues_read && pull_requests_read
      repo.has_issues? ? scope.with_issue : scope.with_pull_request
    elsif issues_read && !pull_requests_read
      repo.has_issues? ? scope.without_pull_request : deliver_error!(404)
    else
      scope.with_pull_request
    end

    scope = scope.filter_spam_for(current_user)
    comments = paginate_rel(filter_and_sort(scope))

    GitHub.dogstats.time "prefill", tags: ["via:api", "action:issues_comments_list"] do
      GitHub::PrefillAssociations.for_issue_comments(comments)
      Reaction::Summary.prefill(comments)
      deliver(:issue_comment_hash, comments, repo: repo)
    end
  end

  ListIssueCommentsQuery = PlatformClient.parse <<-'GRAPHQL'
    query(
      $issueId: ID!,
      $limit: Int!,
      $numericPage: Int,
      $since: DateTime,
      $includeBody: Boolean!,
      $includeBodyHTML: Boolean!,
      $includeBodyText: Boolean!,
      $includeReactions: Boolean!,
      $includePerformedViaGitHubApp: Boolean!
    ) {
      node(id: $issueId) {
        ... on Issue {
          comments(first: $limit, numericPage: $numericPage, since: $since) {
            nodes {
              ...Api::Serializer::IssuesDependency::IssueCommentFragment
            }
            totalCount
          }
        }
        ... on PullRequest {
          comments(first: $limit, numericPage: $numericPage, since: $since) {
            nodes {
              ...Api::Serializer::IssuesDependency::IssueCommentFragment
            }
            totalCount
          }
        }
      }
    }
  GRAPHQL

  def issue_comments_candidate(comments, options, issue)
    options = Api::SerializerOptions.from(options)

    variables = {
      issueId: issue.global_relay_id,
      limit:  pagination[:per_page] || DEFAULT_PER_PAGE,
      numericPage: pagination[:page],
      includeReactions: options.accepts_preview?(:reactions),
      includePerformedViaGitHubApp: true,
    }

    if (since = time_param!(:since)).present?
      variables[:since] = since.getlocal.iso8601
    end

    variables.update(graphql_mime_body_variables(options))

    results = platform_execute(ListIssueCommentsQuery, variables: variables)

    if has_graphql_system_errors?(results)
      deprecated_deliver_graphql_error!({
        errors: results.errors.all,
        resource: "IssueComment",
        documentation_url: @documentation_url,
      })
    end

    comments = results.data.node.comments

    paginator.collection_size = comments.total_count
    Api::Serializer.serialize(:graphql_issue_comment_hash, comments.nodes, options)
  end

  # Get Comments for an Issue
  get "/repositories/:repository_id/issues/:issue_number/comments" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/issues#list-issue-comments"
    repo = find_repo!
    issue = repo.issues.find_by_number int_id_param!(key: :issue_number)
    record_or_404(issue)

    control_access :list_issue_comments,
      repo: repo,
      resource: issue,
      allow_integrations: true,
      allow_user_via_integration: true,
      approved_integration_required: false

    ensure_issues_enabled_or_pr! repo, issue

    scope = filter_and_sort(issue.comments.filter_spam_for(current_user))
    comments = paginate_rel(scope)
    GitHub::PrefillAssociations.for_issue_comments(comments)
    Reaction::Summary.prefill(comments)

    deliver :issue_comment_hash, comments,
        repo: repo,
        compare_payload_to: -> (obj, opts) { issue_comments_candidate(obj, opts, issue) }
  end

  AddIssueCommentMutation = PlatformClient.parse <<-'GRAPHQL'
    mutation(
      $issueId: ID!,
      $body: String!,
      $clientMutationId: String!,
      $includeBody: Boolean!,
      $includeBodyHTML: Boolean!,
      $includeBodyText: Boolean!,
      $includeReactions: Boolean!,
      $includePerformedViaGitHubApp: Boolean!
    ) {
      addComment(input: {
        subjectId: $issueId,
        body: $body,
        clientMutationId: $clientMutationId
      }) {
        commentEdge {
          comment: node {
            ...Api::Serializer::IssuesDependency::IssueCommentFragment
          }
        }
      }
    }
  GRAPHQL

  # Create a Comment for an Issue
  post "/repositories/:repository_id/issues/:issue_number/comments" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/issues#create-an-issue-comment"
    repo  = find_repo!
    issue = repo.issues.find_by_number int_id_param!(key: :issue_number)
    record_or_404(issue)

    control_access :create_issue_comment,
      repo: repo,
      resource: issue,
      # We only need to forbid in the case where a PAT or OAuth token does not have the right scopes.
      # Therefore, we could leave off the integration-related key/value pairs in this call.
      # However, that would count against our linter, so for completeness, we are adding them.
      forbid: access_allowed?(:get_repo, resource: repo, user: current_user, allow_integrations: true, allow_user_via_integration: true),
      challenge: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: repo.private?

    ensure_not_blocked! current_user, repo.owner_id
    authorize_content(:create, issue: issue, repo: repo)

    data = receive_with_schema("issue-comment", "create-legacy")

    options = Api::SerializerOptions.fill(default_options)

    input_variables = {
      issueId: issue.global_relay_id,
      body: data["body"],
      clientMutationId: request_id,
      includeReactions: options.accepts_preview?(:reactions),
      includePerformedViaGitHubApp: true,
    }
    input_variables.update(graphql_mime_body_variables(default_options))

    results = platform_execute(AddIssueCommentMutation, variables: input_variables)

    if has_graphql_system_errors?(results)
      # The AddComment mutation doesn't support `UserErrors`, so we have to
      # manually map a 422 caused by hitting the rate limit to a 403
      if results.errors.all.first.include?(GitHub::RateLimitedCreation::ERROR_MESSAGE)
        deliver_error!(403, {
          message: ERROR_MESSAGE_RATE_LIMIT,
          documentation_url: DOC_URL_RATE_LIMIT,
        })
      else
        deprecated_deliver_graphql_error! errors: results.errors, resource: "IssueComment"
      end
    end

    comment = results.data.add_comment.comment_edge.comment
    deliver :graphql_issue_comment_hash, comment, status: 201
  end

  ShowIssueCommentQuery = PlatformClient.parse <<-'GRAPHQL'
    query(
      $id:ID!,
      $includeBody:Boolean!,
      $includeBodyHTML:Boolean!,
      $includeBodyText:Boolean!,
      $includeReactions:Boolean!,
      $includePerformedViaGitHubApp:Boolean!
    ) {
      comment: node(id:$id) {
        ...Api::Serializer::IssuesDependency::IssueCommentFragment
      }
    }
  GRAPHQL

  # View a single Issue Comment
  get "/repositories/:repository_id/issues/comments/:comment_id" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/issues#get-an-issue-comment"
    repo    = find_repo!
    comment = repo.issue_comments.find_by_id(int_id_param!(key: :comment_id))
    record_or_404(comment)
    record_or_404(comment.issue)

    control_access :get_issue_comment,
      repo: repo,
      resource: comment,
      allow_integrations: true,
      allow_user_via_integration: true,
      approved_integration_required: false

    if comment.hide_from_user?(current_user)
      deliver_error 404
    else
      options = Api::SerializerOptions.fill(default_options)

      variables = {
        id: comment.global_relay_id,
        includeReactions: options.accepts_preview?(:reactions),
        includePerformedViaGitHubApp: true,
      }
      variables.update(graphql_mime_body_variables(options))

      results = platform_execute(ShowIssueCommentQuery, variables: variables)
      comment = results.data.comment

      deliver :graphql_issue_comment_hash, comment
    end
  end

  # Edit an Issue Comment
  verbs :patch, :post, "/repositories/:repository_id/issues/comments/:comment_id" do
    @route_owner = "@github/pe-pull-requests"
    @documentation_url = "/rest/reference/issues#update-an-issue-comment"
    repo    = find_repo!
    comment = repo.issue_comments.find_by_id(int_id_param!(key: :comment_id))
    record_or_404(comment)
    record_or_404(comment.issue)

    control_access :update_issue_comment,
      repo: repo,
      resource: comment,
      challenge: repo.public?,
      # We only need to forbid in the case where a PAT or OAuth token does not have the right scopes.
      # Therefore, we could leave off the integration-related key/value pairs in this call.
      # However, that would count against our linter, so for completeness, we are adding them.
      forbid: access_allowed?(:get_repo, resource: repo, user: current_user, allow_integrations: true, allow_user_via_integration: true),
      enforce_oauth_app_policy: repo.private?,
      allow_integrations: true,
      allow_user_via_integration: true

    authorize_content(:update, issue: comment.try(:issue), repo: repo)

    data = receive_with_schema("issue-comment", "update-legacy")

    if comment.update_body(data["body"], current_user, performed_via_integration: current_integration)
      deliver :issue_comment_hash, comment, repo: repo
    else
      deliver_error 422,
        errors: comment.errors,
        documentation_url: @documentation_url
    end
  end

  # Delete an Issue Comment
  delete "/repositories/:repository_id/issues/comments/:comment_id" do
    @route_owner = "@github/pe-pull-requests"

    # Introducing strict validation of the issue-comment.delete
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("issue-comment", "delete", skip_validation: true)

    @documentation_url = "/rest/reference/issues#delete-an-issue-comment"
    repo    = find_repo!
    comment = repo.issue_comments.find_by_id(int_id_param!(key: :comment_id))
    record_or_404(comment)
    record_or_404(comment.issue)

    control_access :delete_issue_comment,
      repo: repo,
      resource: comment,
      challenge: repo.public?,
      # We only need to forbid in the case where a PAT or OAuth token does not have the right scopes.
      # Therefore, we could leave off the integration-related key/value pairs in this call.
      # However, that would count against our linter, so for completeness, we are adding them.
      forbid: access_allowed?(:get_repo, resource: repo, user: current_user, allow_integrations: true, allow_user_via_integration: true),
      enforce_oauth_app_policy: repo.private?,
      allow_integrations: true,
      allow_user_via_integration: true

    authorize_content(:delete, issue: comment.try(:issue), repo: repo)

    comment.destroy

    # Metric to see how many unverified users make this request over a period of time.
    # https://github.com/github/github/pull/94198#issuecomment-410690200
    tags = []
    tags << "type:deleting_comment_using_unverified_email" if current_user.must_verify_email?
    GitHub.dogstats.increment("api.routes.repositories_repository_id_issues_comments_comment_id", tags: tags)

    deliver_empty(status: 204)
  end

  private

  def filter_scope(scope)
    if (since = time_param!(:since)).present?
      scope.since(since.getlocal)
    else
      scope
    end
  end

  def sort_scope(scope)
    if (sort = params[:sort]).present?
      direction = params[:direction] || "asc"
      scope.sorted_by(sort, direction)
    else
      scope
    end
  end

  def filter_and_sort(scope)
    sort_scope(filter_scope(scope))
  end

  def authorize_content(operation = :create, data = {})
    authorization = ContentAuthorizer.authorize(current_user, :issue_comment, operation, data)
    deliver_content_authorization_denied!(authorization) if authorization.failed?
  end
end
