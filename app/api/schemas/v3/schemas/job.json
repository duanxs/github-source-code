{
  "$schema": "http://json-schema.org/draft-04/hyper-schema",
  "id": "https://schema.github.com/v3/job.json",
  "title": "Job",
  "description": "Information of a job execution in a workflow run",
  "type": "object",
  "definitions": {
    "id": {
      "description": "The id of the job.",
      "example": 21,
      "type": "integer"
    },
    "run_id": {
      "description": "The id of the associated workflow run.",
      "example": 5,
      "type": "integer"
    },
    "name": {
      "description": "The name of the job.",
      "example": "test-coverage",
      "type": "string"
    },
    "head_sha": {
      "description": "The SHA of the commit that is being run.",
      "example": "009b8a3a9ccbb128af87f9b1c0f4c62e8a304f6d",
      "type": "string"
    },
    "status": {
      "description": "The phase of the lifecycle that the job is currently in.",
      "example": "queued",
      "type": "string",
      "enum": [
        "queued",
        "in_progress",
        "completed"
      ]
    },
    "conclusion": {
      "description": "The outcome of the job.",
      "example": "success",
      "type": [
        "string",
        "null"
      ]
    },
    "started_at": {
      "description": "The time that the job started, in ISO 8601 format.",
      "example": "2019-08-08T08:00:00-07:00",
      "format": "date-time",
      "type": "string"
    },
    "step_started_at": {
      "description": "The time that the step started, in ISO 8601 format.",
      "example": "2019-08-08T08:00:00-07:00",
      "format": "date-time",
      "type": [
        "string",
        "null"
      ]
    },
    "completed_at": {
      "description": "The time that the job finished, in ISO 8601 format.",
      "example": "2019-08-08T08:00:00-07:00",
      "format": "date-time",
      "type": [
        "string",
        "null"
      ]
    },
    "repository_id": {
      "description": "The ID of the repository.",
      "example": 42,
      "type": "integer"
    },
    "node_id": {
      "type": "string",
      "example": "MDg6Q2hlY2tSdW40"
    },
    "url": {
      "type": "string",
      "example": "https://api.github.com/repos/github/hello-world/actions/jobs/21"
    },
    "run_url": {
      "type": "string",
      "example": "https://api.github.com/repos/github/hello-world/actions/runs/5"
    },
    "html_url": {
      "type": [
        "string",
        "null"
      ],
      "example": "https://github.com/github/hello-world/runs/4"
    },
    "number": {
      "type": "number",
      "example": 1
    },
    "check_run_url": {
      "type": "string",
      "example": "https://api.github.com/repos/github/hello-world/check-runs/4"
    },
    "steps": {
      "description": "Steps in this job.",
      "type": "array",
      "items": {
        "type": "object",
        "required": [
          "name",
          "status",
          "conclusion",
          "number"
        ],
        "properties": {
          "status": {
            "$ref": "#/definitions/status"
          },
          "conclusion": {
            "$ref": "#/definitions/conclusion"
          },
          "name": {
            "$ref": "#/definitions/name"
          },
          "number": {
            "$ref": "#/definitions/number"
          },
          "started_at": {
            "$ref": "#/definitions/step_started_at"
          },
          "completed_at": {
            "$ref": "#/definitions/completed_at"
          }
        }
      }
    }
  },
  "links": [

  ],
  "properties": {
    "id": {
      "$ref": "#/definitions/id"
    },
    "node_id": {
      "$ref": "#/definitions/node_id"
    },
    "run_id": {
      "$ref": "#/definitions/run_id"
    },
    "run_url": {
      "$ref": "#/definitions/run_url"
    },
    "head_sha": {
      "$ref": "#/definitions/head_sha"
    },
    "name": {
      "$ref": "#/definitions/name"
    },
    "url": {
      "$ref": "#/definitions/url"
    },
    "html_url": {
      "$ref": "#/definitions/html_url"
    },
    "status": {
      "$ref": "#/definitions/status"
    },
    "conclusion": {
      "$ref": "#/definitions/conclusion"
    },
    "started_at": {
      "$ref": "#/definitions/started_at"
    },
    "completed_at": {
      "$ref": "#/definitions/completed_at"
    },
    "check_run_url": {
      "$ref": "#/definitions/check_run_url"
    },
    "steps": {
      "$ref": "#/definitions/steps"
    }
  },
  "additionalProperties": false,
  "required": [
    "id",
    "node_id",
    "run_id",
    "run_url",
    "head_sha",
    "name",
    "url",
    "html_url",
    "status",
    "conclusion",
    "started_at",
    "completed_at",
    "check_run_url"
  ]
}
