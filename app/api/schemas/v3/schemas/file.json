{
  "$schema": "http://json-schema.org/draft-04/hyper-schema",
  "id": "https://schema.github.com/v3/file.json",
  "title": "File",
  "description": "Content and metadata for a file in a repository",
  "type": "object",
  "definitions": {
    "git-user": {
      "description": "Identifying information for the git user",
      "type": "object",
      "properties": {
        "email": {
          "$ref": "https://schema.github.com/v3/git-user.json#/definitions/email"
        },
        "name": {
          "$ref": "https://schema.github.com/v3/git-user.json#/definitions/name"
        }
      },
      "required": [
        "email",
        "name"
      ]
    },
    "branch": {
      "description": "Name of the branch; defaults to the repository’s default branch (which is usually master)",
      "example": "june-demo",
      "type": "string"
    },
    "content": {
      "description": "Content of the file (Base64 encoded)",
      "example": "bXkgbmV3IGZpbGUgY29udGVudHM=",
      "type": "string"
    },
    "message": {
      "description": "Message describing the purpose of the commit",
      "example": "Fix #42",
      "type": "string"
    },
    "name": {
      "description": "Name of the file",
      "example": "hello.js",
      "type": "string"
    },
    "path": {
      "description": "Path to the file from the root of the repository",
      "example": "src/hello.js",
      "type": "string"
    },
    "url": {
      "description": "URL for the file",
      "example": "https://api.github.com/repositories/42/contents/src/hello.js",
      "type": "string"
    },
    "repository_id": {
      "description": "The ID of the repository.",
      "example": 42,
      "type": "integer"
    }
  },
  "links": [
    {
      "href": "/repositories/{repository_id}/contents/{path}",
      "method": "PUT",
      "rel": "create",
      "schema": {
        "properties": {
          "author": {
            "$ref": "#/definitions/git-user"
          },
          "branch": {
            "$ref": "#/definitions/branch"
          },
          "committer": {
            "$ref": "#/definitions/git-user"
          },
          "content": {
            "$ref": "#/definitions/content"
          },
          "message": {
            "$ref": "#/definitions/message"
          }
        },
        "required": [
          "content",
          "message"
        ],
        "type": "object"
      },
      "title": "Create a new file in a repository"
    },
    {
      "href": "/repositories/{repository_id}/contents/{path}",
      "method": "PUT",
      "rel": "update",
      "schema": {
        "properties": {
          "author": {
            "$ref": "#/definitions/git-user"
          },
          "branch": {
            "$ref": "#/definitions/branch"
          },
          "committer": {
            "$ref": "#/definitions/git-user"
          },
          "content": {
            "$ref": "#/definitions/content"
          },
          "message": {
            "$ref": "#/definitions/message"
          },
          "sha": {
            "description": "Blob SHA of the file being updated",
            "example": "329688480d39049927147c162b9d2deaf885005f",
            "type": "string"
          }
        },
        "required": [
          "content",
          "message",
          "sha"
        ],
        "type": "object"
      },
      "title": "Update an existing file in a repository"
    },
    {
      "href": "/repositories/{repository_id}/contents/{path}",
      "method": "DELETE",
      "rel": "delete",
      "schema": {
        "properties": {
          "author": {
            "$ref": "#/definitions/git-user"
          },
          "branch": {
            "$ref": "#/definitions/branch"
          },
          "committer": {
            "$ref": "#/definitions/git-user"
          },
          "message": {
            "$ref": "#/definitions/message"
          },
          "sha": {
            "description": "Blob SHA of the file being deleted",
            "example": "329688480d39049927147c162b9d2deaf885005f",
            "type": "string"
          }
        },
        "required": [
          "message",
          "sha"
        ],
        "type": "object"
      },
      "title": "Delete an existing file in a repository"
    }
  ]
}
