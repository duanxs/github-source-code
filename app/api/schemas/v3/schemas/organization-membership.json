{
  "$schema": "http://json-schema.org/draft-04/hyper-schema",
  "id": "https://schema.github.com/v3/organization-membership.json",
  "title": "Organization Membership",
  "description": "A GitHub user's membership in an organization.",
  "type": "object",
  "definitions": {
    "username": {
      "description": "The username of the membership.",
      "type": "string",
      "example": "username1"
    },
    "role": {
      "description": "The role of the user in the organization.",
      "enum": [
        "admin",
        "member"
      ],
      "default": "member",
      "example": "member",
      "type": "string"
    },
    "state": {
      "description": "The state that the membership should be in.",
      "enum": [
        "active"
      ],
      "example": "active",
      "type": "string"
    },
    "async": {
      "description": "Whether to do this in a background job.",
      "type": "string",
      "enum": [
        "true",
        "false"
      ],
      "example": "true",
      "default": "false"
    },
    "organization_id": {
      "description": "The ID of the organization",
      "type": "integer"
    },
    "org": {
      "description": "The name of the organization",
      "type": "string"
    }
  },
  "links": [
    {
      "title": "Add or update organization membership",
      "method": "PUT",
      "rel": "replace",
      "href": "/organizations/{organization_id}/memberships/{username}",
      "schema": {
        "properties": {
          "role": {
            "$ref": "#/definitions/role"
          }
        },
        "type": [
          "object",
          "null"
        ],
        "additionalProperties": false
      }
    },
    {
      "title": "Edit your organization membership",
      "method": "PATCH",
      "rel": "update",
      "href": "/user/memberships/organizations/{organization_id}",
      "schema": {
        "properties": {
          "state": {
            "$ref": "#/definitions/state"
          }
        },
        "type": "object",
        "additionalProperties": false,
        "required": [
          "state"
        ]
      }
    },
    {
      "title": "Publicize a user's membership",
      "method": "PUT",
      "rel": "publicize",
      "href": "/organizations/{organization_id}/public_members/{username}",
      "schema": {
        "additionalProperties": false,
        "type": [
          "null",
          "object"
        ]
      }
    },
    {
      "title": "Edit your organization membership",
      "method": "POST",
      "rel": "update",
      "href": "/user/memberships/organizations/{organization_id}",
      "schema": {
        "properties": {
          "state": {
            "$ref": "#/definitions/state"
          }
        },
        "type": "object",
        "additionalProperties": false,
        "required": [
          "state"
        ]
      }
    },
    {
      "title": "Remove a member",
      "method": "DELETE",
      "rel": "delete-member",
      "href": "/organizations/{organization_id}/members/{username}",
      "schema": {
        "additionalProperties": false,
        "type": [
          "null",
          "object"
        ]
      }
    },
    {
      "title": "Conceal a user's membership",
      "method": "DELETE",
      "rel": "conceal",
      "href": "/organizations/{organization_id}/public_members/{username}",
      "schema": {
        "additionalProperties": false,
        "type": [
          "null",
          "object"
        ]
      }
    },
    {
      "title": "Remove organization membership",
      "method": "DELETE",
      "rel": "delete",
      "href": "/organizations/{organization_id}/memberships/{username}",
      "schema": {
        "additionalProperties": false,
        "type": [
          "null",
          "object"
        ]
      }
    },
    {
      "title": "Convert member to outside collaborator",
      "method": "PUT",
      "rel": "convert-to-outside-collaborator",
      "href": "/organizations/{organization_id}/outside_collaborators/{username}",
      "schema": {
        "properties": {
          "async": {
            "$ref": "#/definitions/async"
          }
        },
        "additionalProperties": false,
        "type": [
          "object",
          "null"
        ]
      }
    }
  ]
}
