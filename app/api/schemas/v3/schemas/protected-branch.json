{
  "$schema": "http://json-schema.org/draft-04/hyper-schema",
  "id": "https://schema.github.com/v3/protected-branch.json",
  "title": "Protected Branch",
  "description": "Branch protections protect branches",
  "type": "object",
  "definitions": {
    "policy-dismissal-restrictions": {
      "type": "object",
      "properties": {
        "url": {
          "type": "string",
          "format": "uri"
        },
        "users_url": {
          "type": "string",
          "format": "uri"
        },
        "teams_url": {
          "type": "string",
          "format": "uri"
        },
        "users": {
          "type": "array"
        },
        "teams": {
          "type": "array"
        }
      },
      "additionalProperties": false,
      "required": [
        "url",
        "users_url",
        "teams_url",
        "users",
        "teams"
      ]
    },
    "policy-required-pull-request-reviews": {
      "type": "object",
      "properties": {
        "url": {
          "type": "string",
          "format": "uri"
        },
        "dismiss_stale_reviews": {
          "type": "boolean"
        },
        "require_code_owner_reviews": {
          "type": "boolean"
        },
        "required_approving_review_count": {
          "type": "number"
        },
        "dismissal_restrictions": {
          "$ref": "#/definitions/policy-dismissal-restrictions"
        }
      },
      "additionalProperties": false,
      "required": [
        "url"
      ]
    },
    "policy-enforce-admins": {
      "type": "object",
      "properties": {
        "url": {
          "type": "string",
          "format": "uri"
        },
        "enabled": {
          "type": "boolean"
        }
      },
      "additionalProperties": false,
      "required": [
        "url",
        "enabled"
      ]
    },
    "required-linear-history": {
      "type": "object",
      "properties": {
        "enabled": {
          "type": "boolean"
        }
      },
      "additionalProperties": false,
      "required": [
        "enabled"
      ]
    },
    "allow-force-pushes": {
      "type": "object",
      "properties": {
        "enabled": {
          "type": "boolean"
        }
      },
      "additionalProperties": false,
      "required": [
        "enabled"
      ]
    },
    "allow-deletions": {
      "type": "object",
      "properties": {
        "enabled": {
          "type": "boolean"
        }
      },
      "additionalProperties": false,
      "required": [
        "enabled"
      ]
    },
    "name": {
      "description": "Unique name of the branch",
      "example": "master",
      "type": "string"
    },
    "required_linear_history": {
      "type": "boolean"
    },
    "allow_force_pushes": {
      "type": "boolean"
    },
    "allow_deletions": {
      "type": "boolean"
    },
    "owner": {
      "description": "The owner of the repository.",
      "type": "string"
    },
    "repo": {
      "description": "The name of the repository.",
      "type": "string"
    },
    "branch": {
      "description": "The name of the branch.",
      "type": "string",
      "example": "june-demo"
    },
    "repository_id": {
      "description": "The ID of the repository.",
      "type": "integer",
      "example": 42
    },
    "protection": {
      "type": "object",
      "properties": {
        "required_status_checks": {
          "anyOf": [
            {
              "allOf": [
                {
                  "$ref": "#/definitions/required_status_checks"
                },
                {
                  "required": [
                    "strict",
                    "contexts"
                  ]
                }
              ]
            },
            {
              "type": "null"
            }
          ]
        },
        "required_pull_request_reviews": {
          "anyOf": [
            {
              "allOf": [
                {
                  "$ref": "#/definitions/required_pull_request_reviews"
                }
              ]
            },
            {
              "type": "null"
            }
          ]
        },
        "required_signatures": {
          "anyOf": [
            {
              "allOf": [
                {
                  "$ref": "#/definitions/required_signatures"
                }
              ]
            },
            {
              "type": "null"
            }
          ]
        },
        "enforce_admins": {
          "anyOf": [
            {
              "allOf": [
                {
                  "$ref": "#/definitions/enforce_admins"
                }
              ]
            },
            {
              "type": "null"
            }
          ]
        },
        "required_linear_history": {
          "anyOf": [
            {
              "allOf": [
                {
                  "$ref": "#/definitions/required_linear_history"
                }
              ]
            },
            {
              "type": "null"
            }
          ]
        },
        "allow_force_pushes": {
          "anyOf": [
            {
              "allOf": [
                {
                  "$ref": "#/definitions/allow_force_pushes"
                }
              ]
            },
            {
              "type": "null"
            }
          ]
        },
        "allow_deletions": {
          "anyOf": [
            {
              "allOf": [
                {
                  "$ref": "#/definitions/allow_deletions"
                }
              ]
            },
            {
              "type": "null"
            }
          ]
        },
        "restrictions": {
          "anyOf": [
            {
              "allOf": [
                {
                  "$ref": "#/definitions/restrictions"
                },
                {
                  "required": [
                    "users",
                    "teams"
                  ]
                }
              ]
            },
            {
              "type": "null"
            }
          ]
        }
      },
      "required": [
        "required_status_checks",
        "restrictions",
        "enforce_admins",
        "required_pull_request_reviews"
      ]
    },
    "required_status_checks": {
      "type": "object",
      "properties": {
        "strict": {
          "type": "boolean"
        },
        "contexts": {
          "$ref": "#/definitions/contexts"
        }
      }
    },
    "required_pull_request_reviews": {
      "type": "object",
      "properties": {
        "dismiss_stale_reviews": {
          "type": "boolean"
        },
        "require_code_owner_reviews": {
          "type": "boolean"
        },
        "required_approving_review_count": {
          "type": "integer",
          "minimum": 1,
          "maximum": 6
        },
        "dismissal_restrictions": {
          "type": "object",
          "properties": {
            "users": {
              "$ref": "#/definitions/users"
            },
            "teams": {
              "$ref": "#/definitions/teams"
            }
          }
        }
      }
    },
    "required_signatures": {
      "type": "boolean"
    },
    "enforce_admins": {
      "type": "boolean"
    },
    "restrictions": {
      "type": "object",
      "properties": {
        "users": {
          "$ref": "#/definitions/users"
        },
        "teams": {
          "$ref": "#/definitions/teams"
        },
        "apps": {
          "$ref": "#/definitions/apps"
        }
      }
    },
    "contexts": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "teams": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "users": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "apps": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "url": {
      "type": "string",
      "format": "uri",
      "example": "https://api.github.com/repos/octocat/Hello-World/branches/master/protection"
    }
  },
  "links": [
    {
      "href": "/repositories/{repository_id}/branches/{branch}/protection",
      "method": "PUT",
      "rel": "enable-legacy",
      "title": "Enable branch protection",
      "schema": {
        "$ref": "#/definitions/protection"
      }
    },
    {
      "href": "/repositories/{repository_id}/branches/{branch}/protection/required_status_checks",
      "method": "PATCH",
      "rel": "update-required-status-checks-legacy",
      "title": "Update required status check settings of protected branch",
      "schema": {
        "$ref": "#/definitions/required_status_checks"
      }
    },
    {
      "href": "/repositories/{repository_id}/branches/{branch}/protection/required_status_checks/contexts",
      "method": "PUT",
      "rel": "replace-required-status-checks-contexts-legacy",
      "title": "Replace required status checks contexts of protected branch",
      "schema": {
        "$ref": "#/definitions/contexts"
      }
    },
    {
      "href": "/repositories/{repository_id}/branches/{branch}/protection/required_status_checks/contexts",
      "method": "POST",
      "rel": "add-required-status-checks-contexts-legacy",
      "title": "Add required status checks contexts of protected branch",
      "schema": {
        "$ref": "#/definitions/contexts"
      }
    },
    {
      "href": "/repositories/{repository_id}/branches/{branch}/protection/required_status_checks/contexts",
      "method": "DELETE",
      "rel": "remove-required-status-checks-contexts-legacy",
      "title": "Remove required status checks contexts of protected branch",
      "schema": {
        "$ref": "#/definitions/contexts"
      }
    },
    {
      "href": "/repositories/{repository_id}/branches/{branch}/protection/restrictions/users",
      "method": "PUT",
      "title": "Replace user restrictions of protected branch",
      "rel": "replace-user-restrictions-legacy",
      "schema": {
        "$ref": "#/definitions/users"
      }
    },
    {
      "href": "/repositories/{repository_id}/branches/{branch}/protection/restrictions/users",
      "method": "POST",
      "title": "Add user restrictions of protected branch",
      "rel": "add-user-restrictions-legacy",
      "schema": {
        "$ref": "#/definitions/users"
      }
    },
    {
      "href": "/repositories/{repository_id}/branches/{branch}/protection/restrictions/users",
      "method": "DELETE",
      "title": "Remove user restrictions of protected branch",
      "rel": "remove-user-restrictions-legacy",
      "schema": {
        "$ref": "#/definitions/users"
      }
    },
    {
      "href": "/repositories/{repository_id}/branches/{branch}/protection/restrictions/teams",
      "method": "PUT",
      "title": "Replace team restrictions of protected branch",
      "rel": "replace-team-restrictions-legacy",
      "schema": {
        "$ref": "#/definitions/teams"
      }
    },
    {
      "href": "/repositories/{repository_id}/branches/{branch}/protection/restrictions/teams",
      "method": "POST",
      "title": "Add team restrictions of protected branch",
      "rel": "add-team-restrictions-legacy",
      "schema": {
        "$ref": "#/definitions/teams"
      }
    },
    {
      "href": "/repositories/{repository_id}/branches/{branch}/protection/restrictions/teams",
      "method": "DELETE",
      "title": "Remove team restrictions of protected branch",
      "rel": "remove-team-restrictions-legacy",
      "schema": {
        "$ref": "#/definitions/teams"
      }
    },
    {
      "href": "/repositories/{repository_id}/branches/{branch}/protection/restrictions/apps",
      "method": "PUT",
      "title": "Replace app restrictions of protected branch",
      "rel": "replace-app-restrictions-legacy",
      "schema": {
        "$ref": "#/definitions/apps"
      }
    },
    {
      "href": "/repositories/{repository_id}/branches/{branch}/protection/restrictions/apps",
      "method": "POST",
      "title": "Add app restrictions of protected branch",
      "rel": "add-app-restrictions-legacy",
      "schema": {
        "$ref": "#/definitions/apps"
      }
    },
    {
      "href": "/repositories/{repository_id}/branches/{branch}/protection/restrictions/apps",
      "method": "DELETE",
      "title": "Remove app restrictions of protected branch",
      "rel": "remove-app-restrictions-legacy",
      "schema": {
        "$ref": "#/definitions/apps"
      }
    },
    {
      "href": "/repositories/{repository_id}/branches/{branch}/protection/required_pull_request_reviews",
      "method": "PATCH",
      "title": "Update pull request review enforcement of protected branch",
      "rel": "update-pull-request-review-enforcement-legacy",
      "schema": {
        "$ref": "#/definitions/required_pull_request_reviews"
      }
    },
    {
      "title": "Add admin enforcement of protected branch",
      "method": "POST",
      "rel": "enable-admin-enforcement",
      "href": "/repositories/{repository_id}/branches/{branch}/protection/enforce_admins",
      "schema": {
        "additionalProperties": false,
        "type": [
          "null",
          "object"
        ]
      }
    },
    {
      "title": "Add required signatures of protected branch",
      "method": "POST",
      "rel": "enable-required-signatures",
      "href": "/repositories/{repository_id}/branches/{branch}/protection/required_signatures",
      "schema": {
        "additionalProperties": false,
        "type": [
          "null",
          "object"
        ]
      }
    },
    {
      "title": "Remove branch protection",
      "method": "DELETE",
      "rel": "delete",
      "href": "/repositories/{repository_id}/branches/{branch}/protection",
      "schema": {
        "additionalProperties": false,
        "type": [
          "null",
          "object"
        ]
      }
    },
    {
      "title": "Enable branch protection",
      "method": "PUT",
      "href": "/repositories/{repository_id}/branches/{branch}/protection",
      "rel": "enable",
      "schema": {
        "$ref": "#/definitions/protection",
        "additionalProperties": false
      }
    },
    {
      "title": "Update required status check settings of protected branch",
      "method": "PATCH",
      "href": "/repositories/{repository_id}/branches/{branch}/protection/required_status_checks",
      "rel": "update-required-status-checks",
      "schema": {
        "$ref": "#/definitions/required_status_checks",
        "additionalProperties": false
      }
    },
    {
      "title": "Replace required status checks contexts of protected branch",
      "method": "PUT",
      "href": "/repositories/{repository_id}/branches/{branch}/protection/required_status_checks/contexts",
      "rel": "replace-required-status-checks-contexts",
      "schema": {
        "$ref": "#/definitions/contexts",
        "additionalProperties": false
      }
    },
    {
      "title": "Add required status checks contexts of protected branch",
      "method": "POST",
      "href": "/repositories/{repository_id}/branches/{branch}/protection/required_status_checks/contexts",
      "rel": "add-required-status-checks-contexts",
      "schema": {
        "$ref": "#/definitions/contexts",
        "additionalProperties": false
      }
    },
    {
      "title": "Remove required status checks contexts of protected branch",
      "method": "DELETE",
      "href": "/repositories/{repository_id}/branches/{branch}/protection/required_status_checks/contexts",
      "rel": "remove-required-status-checks-contexts",
      "schema": {
        "$ref": "#/definitions/contexts",
        "additionalProperties": false
      }
    },
    {
      "title": "Replace user restrictions of protected branch",
      "method": "PUT",
      "href": "/repositories/{repository_id}/branches/{branch}/protection/restrictions/users",
      "rel": "replace-user-restrictions",
      "schema": {
        "$ref": "#/definitions/users",
        "additionalProperties": false
      }
    },
    {
      "title": "Add user restrictions of protected branch",
      "method": "POST",
      "href": "/repositories/{repository_id}/branches/{branch}/protection/restrictions/users",
      "rel": "add-user-restrictions",
      "schema": {
        "$ref": "#/definitions/users",
        "additionalProperties": false
      }
    },
    {
      "title": "Remove user restrictions of protected branch",
      "method": "DELETE",
      "href": "/repositories/{repository_id}/branches/{branch}/protection/restrictions/users",
      "rel": "remove-user-restrictions",
      "schema": {
        "$ref": "#/definitions/users",
        "additionalProperties": false
      }
    },
    {
      "title": "Replace team restrictions of protected branch",
      "method": "PUT",
      "href": "/repositories/{repository_id}/branches/{branch}/protection/restrictions/teams",
      "rel": "replace-team-restrictions",
      "schema": {
        "$ref": "#/definitions/teams",
        "additionalProperties": false
      }
    },
    {
      "title": "Add team restrictions of protected branch",
      "method": "POST",
      "href": "/repositories/{repository_id}/branches/{branch}/protection/restrictions/teams",
      "rel": "add-team-restrictions",
      "schema": {
        "$ref": "#/definitions/teams",
        "additionalProperties": false
      }
    },
    {
      "title": "Remove team restrictions of protected branch",
      "method": "DELETE",
      "href": "/repositories/{repository_id}/branches/{branch}/protection/restrictions/teams",
      "rel": "remove-team-restrictions",
      "schema": {
        "$ref": "#/definitions/teams",
        "additionalProperties": false
      }
    },
    {
      "title": "Update pull request review enforcement of protected branch",
      "method": "PATCH",
      "href": "/repositories/{repository_id}/branches/{branch}/protection/required_pull_request_reviews",
      "rel": "update-pull-request-review-enforcement",
      "schema": {
        "$ref": "#/definitions/required_pull_request_reviews",
        "additionalProperties": false
      }
    },
    {
      "title": "Remove pull request review enforcement of protected branch",
      "method": "DELETE",
      "rel": "remove-pr-review-enforcement",
      "href": "/repositories/{repository_id}/branches/{branch}/protection/required_pull_request_reviews",
      "schema": {
        "additionalProperties": false,
        "type": [
          "object",
          "null"
        ]
      }
    },
    {
      "title": "Remove required status checks of protected branch",
      "method": "DELETE",
      "rel": "remove-required-status-checks",
      "href": "/repositories/{repository_id}/branches/{branch}/protection/required_status_checks",
      "schema": {
        "additionalProperties": false,
        "type": [
          "object",
          "null"
        ]
      }
    },
    {
      "title": "Remove required signatures of protected branch",
      "method": "DELETE",
      "rel": "remove-required-signatures",
      "href": "/repositories/{repository_id}/branches/{branch}/protection/required_signatures",
      "schema": {
        "additionalProperties": false,
        "type": [
          "object",
          "null"
        ]
      }
    },
    {
      "title": "Remove admin enforcement of protected branch",
      "method": "DELETE",
      "rel": "remove-admin-enforcement",
      "href": "/repositories/{repository_id}/branches/{branch}/protection/enforce_admins",
      "schema": {
        "additionalProperties": false,
        "type": [
          "object",
          "null"
        ]
      }
    },
    {
      "title": "Remove restrictions of protected branch",
      "method": "DELETE",
      "rel": "remove-restrictions",
      "href": "/repositories/{repository_id}/branches/{branch}/protection/restrictions",
      "schema": {
        "additionalProperties": false,
        "type": [
          "object",
          "null"
        ]
      }
    }
  ],
  "properties": {
    "url": {
      "$ref": "#/definitions/url"
    },
    "required_status_checks": {
      "$ref": "https://schema.github.com/v3/status-check-policy.json#"
    },
    "required_pull_request_reviews": {
      "$ref": "#/definitions/policy-required-pull-request-reviews"
    },
    "required_signatures": {
      "$ref": "https://schema.github.com/v3/required-signature-policy.json#"
    },
    "enforce_admins": {
      "$ref": "#/definitions/policy-enforce-admins"
    },
    "required_linear_history": {
      "$ref": "#/definitions/required-linear-history"
    },
    "allow_force_pushes": {
      "$ref": "#/definitions/allow-force-pushes"
    },
    "allow_deletions": {
      "$ref": "#/definitions/allow-deletions"
    },
    "restrictions": {
      "$ref": "https://schema.github.com/v3/branch-restriction-policy.json#"
    }
  },
  "additionalProperties": false,
  "required": [
    "url"
  ]
}
