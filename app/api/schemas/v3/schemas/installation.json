{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "id": "https://schema.github.com/v3/installation.json",
  "title": "Installation",
  "type": "object",
  "definitions": {
    "id": {
      "description": "The ID of the installation.",
      "type": "integer",
      "example": 1
    },
    "installation_id": {
      "description": "The ID of the installation.",
      "type": "integer",
      "example": 42
    },
    "app_id": {
      "type": "integer",
      "example": 1
    },
    "app_slug": {
      "type": "string",
      "example": "github-actions"
    },
    "target_id": {
      "description": "The ID of the user or organization this token is being scoped to.",
      "type": "integer"
    },
    "target_type": {
      "type": "string",
      "example": "Organization"
    },
    "single_file_name": {
      "type": [
        "string",
        "null"
      ],
      "example": "config.yml"
    },
    "repository_selection": {
      "description": "Describe whether all repositories have been selected or there's a selection involved",
      "type": "string",
      "enum": [
        "all",
        "selected"
      ]
    },
    "access_tokens_url": {
      "type": "string",
      "format": "uri",
      "example": "https://api.github.com/installations/1/access_tokens"
    },
    "html_url": {
      "type": "string",
      "format": "uri",
      "example": "https://github.com/organizations/github/settings/installations/1"
    },
    "repositories_url": {
      "type": "string",
      "format": "uri",
      "example": "https://api.github.com/installation/repositories"
    },
    "events": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "created_at": {
      "type": "string",
      "format": "date-time"
    },
    "updated_at": {
      "type": "string",
      "format": "date-time"
    },
    "permissions": {
      "type": "object",
      "example": {
        "issues": "read",
        "deployments": "write"
      }
    },
    "suspended_by": {
      "oneOf": [
        {
          "type": "null"
        },
        {
          "$ref": "https://schema.github.com/v3/simple-user.json#"
        }
      ]
    },
    "suspended_at": {
      "oneOf": [
        {
          "type": "null"
        },
        {
          "type": "string",
          "format": "date-time"
        }
      ]
    }
  },
  "links": [
    {
      "title": "Uninstall a GitHub App installation",
      "method": "DELETE",
      "rel": "delete",
      "href": "/app/installations/{installation_id}",
      "schema": {
        "additionalProperties": false,
        "type": [
          "object",
          "null"
        ]
      }
    },
    {
      "title": "Revoke an installation access token",
      "method": "DELETE",
      "rel": "delete",
      "href": "/installation/token",
      "schema": {
        "additionalProperties": false,
        "type": [
          "object",
          "null"
        ]
      }
    },
    {
      "title": "Suspend a GitHub App installation",
      "method": "PUT",
      "rel": "suspend",
      "href": "/app/installations/{installation_id}/suspended",
      "schema": {
        "additionalProperties": false,
        "type": [
          "object",
          "null"
        ]
      }
    },
    {
      "title": "Unsuspend a GitHub App installation",
      "method": "DELETE",
      "rel": "unsuspend",
      "href": "/app/installations/{installation_id}/suspended",
      "schema": {
        "additionalProperties": false,
        "type": [
          "object",
          "null"
        ]
      }
    }
  ],
  "properties": {
    "id": {
      "$ref": "#/definitions/id"
    },
    "app_id": {
      "$ref": "#/definitions/app_id"
    },
    "app_slug": {
      "$ref": "#/definitions/app_slug"
    },
    "target_id": {
      "$ref": "#/definitions/target_id"
    },
    "target_type": {
      "$ref": "#/definitions/target_type"
    },
    "single_file_name": {
      "$ref": "#/definitions/single_file_name"
    },
    "repository_selection": {
      "$ref": "#/definitions/repository_selection"
    },
    "access_tokens_url": {
      "$ref": "#/definitions/access_tokens_url"
    },
    "html_url": {
      "$ref": "#/definitions/html_url"
    },
    "repositories_url": {
      "$ref": "#/definitions/repositories_url"
    },
    "events": {
      "$ref": "#/definitions/events"
    },
    "account": {
      "oneOf": [
        {
          "type": "null"
        },
        {
          "$ref": "https://schema.github.com/v3/simple-user.json#"
        },
        {
          "$ref": "https://schema.github.com/v3/enterprise.json#"
        }
      ]
    },
    "permissions": {
      "$ref": "#/definitions/permissions"
    },
    "created_at": {
      "$ref": "#/definitions/created_at"
    },
    "updated_at": {
      "$ref": "#/definitions/updated_at"
    },
    "suspended_by": {
      "$ref": "#/definitions/suspended_by"
    },
    "suspended_at": {
      "$ref": "#/definitions/suspended_at"
    }
  },
  "additionalProperties": false,
  "required": [
    "id",
    "app_id",
    "app_slug",
    "target_id",
    "target_type",
    "single_file_name",
    "repository_selection",
    "access_tokens_url",
    "html_url",
    "repositories_url",
    "events",
    "account",
    "permissions",
    "created_at",
    "updated_at"
  ]
}
