{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "id": "https://schema.github.com/v3/check-output.json",
  "title": "Check Output",
  "type": "object",
  "definitions": {
    "lenient-title": {
      "type": [
        "string",
        "null"
      ],
      "description": "A title describing the check.",
      "example": "Code coverage report"
    },
    "title": {
      "type": "string",
      "description": "A title describing the check.",
      "example": "Code coverage report"
    },
    "lenient-summary": {
      "type": [
        "string",
        "null"
      ],
      "description": "Markdown-compatible summary of the result of a check.",
      "example": "Coverage decreased by 2%, bringing it below the configured threshold of 80%.",
      "maxLength": 65535
    },
    "summary": {
      "type": "string",
      "description": "Markdown-compatible summary of the result of a check.",
      "example": "Coverage decreased by 2%, bringing it below the configured threshold of 80%.",
      "maxLength": 65535
    },
    "lenient-text": {
      "type": [
        "string",
        "null"
      ],
      "description": "Markdown-compatible text containing information about the check.",
      "example": "Coverage by area:\n* lib: 97%\n*app/controllers: 73%,\n* app/models: 81%",
      "maxLength": 65535
    },
    "text": {
      "type": "string",
      "description": "Markdown-compatible text containing information about the check.",
      "example": "Coverage by area:\n* lib: 97%\n*app/controllers: 73%,\n* app/models: 81%",
      "maxLength": 65535
    },
    "annotations-count": {
      "type": "integer"
    },
    "annotations-url": {
      "type": "string",
      "format": "uri"
    },
    "annotation-path": {
      "description": "The path to the file being annotated.",
      "example": "hello.rb",
      "type": "string"
    },
    "annotation-start-line": {
      "description": "The first line relevant to the annotation.",
      "example": 17,
      "type": "integer"
    },
    "annotation-end-line": {
      "description": "The last line relevant to the annotation.",
      "example": 21,
      "type": "integer"
    },
    "annotation-start-column": {
      "description": "The first column relevant to the annotation.",
      "example": 1,
      "type": "integer"
    },
    "annotation-end-column": {
      "description": "The last column relevant to the annotation.",
      "example": 12,
      "type": "integer"
    },
    "annotation-level": {
      "description": "A level indicating the severity of the feedback.",
      "example": "notice",
      "type": "string",
      "enum": [
        "warning",
        "failure",
        "notice"
      ]
    },
    "annotation-title": {
      "description": "A short description of the annotation.",
      "example": "Hello#world",
      "type": "string"
    },
    "annotation-message": {
      "description": "A short description of the feedback for these lines of code.",
      "example": "Method has no test coverage.",
      "type": "string"
    },
    "annotation-raw-details": {
      "description": "Details about the annotation.",
      "type": "string"
    },
    "image-alt": {
      "description": "The alternative text for the image.",
      "example": "Changes in coverage over time.",
      "type": "string"
    },
    "image-url": {
      "description": "The full URL to the image.",
      "example": "http://example.com/builds/abc123/coverage-changes.png",
      "type": "string"
    },
    "image-caption": {
      "description": "The caption of the image.",
      "example": "Code coverage February-May 2018",
      "type": "string"
    },
    "images": {
      "description": "Images pertaining to the check.",
      "type": "array",
      "items": {
        "type": "object",
        "required": [
          "image_url",
          "alt"
        ],
        "properties": {
          "alt": {
            "$ref": "#/definitions/image-alt"
          },
          "image_url": {
            "$ref": "#/definitions/image-url"
          },
          "caption": {
            "$ref": "#/definitions/image-caption"
          }
        }
      }
    },
    "annotations": {
      "description": "Information about specific lines of code.",
      "type": "array",
      "maxItems": 50,
      "items": {
        "type": "object",
        "required": [
          "path",
          "start_line",
          "end_line",
          "annotation_level",
          "message"
        ],
        "properties": {
          "path": {
            "$ref": "#/definitions/annotation-path"
          },
          "start_line": {
            "$ref": "#/definitions/annotation-start-line"
          },
          "end_line": {
            "$ref": "#/definitions/annotation-end-line"
          },
          "start_column": {
            "$ref": "#/definitions/annotation-start-column"
          },
          "end_column": {
            "$ref": "#/definitions/annotation-end-column"
          },
          "annotation_level": {
            "$ref": "#/definitions/annotation-level"
          },
          "title": {
            "$ref": "#/definitions/annotation-title"
          },
          "message": {
            "$ref": "#/definitions/annotation-message"
          },
          "raw_details": {
            "$ref": "#/definitions/annotation-raw-details"
          }
        }
      }
    },
    "output-request": {
      "description": "JSON payload with information about the check.",
      "type": "object",
      "required": [
        "title",
        "summary"
      ],
      "properties": {
        "title": {
          "$ref": "#/definitions/title"
        },
        "summary": {
          "$ref": "#/definitions/summary"
        },
        "text": {
          "$ref": "#/definitions/text"
        },
        "images": {
          "$ref": "#/definitions/images"
        },
        "annotations": {
          "$ref": "#/definitions/annotations"
        }
      }
    },
    "legacy-output-request": {
      "description": "JSON payload with information about the check.",
      "type": [
        "object",
        "null"
      ],
      "required": [
        "title",
        "summary"
      ],
      "properties": {
        "title": {
          "$ref": "#/definitions/title"
        },
        "summary": {
          "$ref": "#/definitions/summary"
        },
        "text": {
          "$ref": "#/definitions/text"
        },
        "images": {
          "$ref": "#/definitions/images"
        },
        "annotations": {
          "$ref": "#/definitions/annotations"
        }
      }
    },
    "check-output-response": {
      "type": "object",
      "properties": {
        "title": {
          "$ref": "#/definitions/lenient-title"
        },
        "summary": {
          "$ref": "#/definitions/lenient-summary"
        },
        "text": {
          "$ref": "#/definitions/lenient-text"
        },
        "annotations_count": {
          "$ref": "#/definitions/annotations-count"
        },
        "annotations_url": {
          "$ref": "#/definitions/annotations-url"
        }
      },
      "additionalProperties": false,
      "required": [
        "title",
        "summary",
        "text",
        "annotations_count",
        "annotations_url"
      ]
    }
  },
  "properties": {
    "title": {
      "$ref": "#/definitions/lenient-title"
    },
    "summary": {
      "$ref": "#/definitions/lenient-summary"
    },
    "text": {
      "$ref": "#/definitions/lenient-text"
    },
    "annotations_count": {
      "$ref": "#/definitions/annotations-count"
    },
    "annotations_url": {
      "$ref": "#/definitions/annotations-url"
    }
  },
  "additionalProperties": false,
  "required": [
    "title",
    "summary",
    "text",
    "annotations_count",
    "annotations_url"
  ]
}
