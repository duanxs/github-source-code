# frozen_string_literal: true

module Api::App::JobEnqueueHelpers

  # Internal: Enqueues push job after user pushes data
  #
  # repo - The repository for which to enqueue a job
  # data -  Hash of data with pusher, refupdates and protocol information to enqueue job
  #
  def enqueue_push_job(repo, data)
    ref_updates = data["ref_updates"].map do |ref_update|
      Git::Ref::Update.new(repository: repo, refname: ref_update["refname"],
                           before_oid: ref_update["before_oid"], after_oid: ref_update["after_oid"])
    end

    RepositoryPushJobTrigger.new(repo, data["pusher"], ref_updates, data["protocol"]).enqueue
  end

end
