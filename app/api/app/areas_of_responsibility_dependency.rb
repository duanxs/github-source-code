# rubocop:disable Style/FrozenStringLiteralComment

module Api::App::AreasOfResponsibilityDependency
  extend ActiveSupport::Concern

  include GitHub::AreasOfResponsibility

  def initialize_areas_of_responsibility
    env["process.areas_of_responsibility"] = areas_of_responsibility

    GitHub.context.push(areas_of_responsibility: areas_of_responsibility)
    Audit.context.push(areas_of_responsibility: areas_of_responsibility)
    Failbot.push(areas_of_responsibility: areas_of_responsibility)
  end
end
