# frozen_string_literal: true

module Api::App::StatsDependency
  extend ActiveSupport::Concern

  class_methods do
    def statsd_tag_actions(path = /.*/, options = {})
      before(path, **options) do
        request.env[GitHub::TaggingHelper::STATSD_TAG_ACTIONS] = true
      end
    end
  end
end
