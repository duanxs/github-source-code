# rubocop:disable Style/FrozenStringLiteralComment

module Api::App::ErrorDependency
  ERROR_MAP = {
    "has already been taken" => "already_exists",
    "is invalid" => "invalid",
    "can't be blank" => "missing_field",
    GitHub::RateLimitedCreation::ERROR_MESSAGE => "abuse",
  }
  ERRORS = Set.new ERROR_MAP.values + %w(missing not_available too_large unauthorized unprocessable)

  ERROR_MESSAGE_RATE_LIMIT = "You have triggered an abuse detection mechanism and have been temporarily blocked from content creation. Please retry your request again later."
  DOC_URL_RATE_LIMIT = "/v3/#abuse-rate-limits"

  # Public: Creates an Error object for being serialized to JSON.
  #
  # resource - The String resource name (usually the AR model name)
  # field    - The String field name that the error is for.
  # code     - The String error code.  Possible values should be documented.
  # options  - Optional Hash of more error properties to add.
  #
  # Returns a Hash
  if Rails.test?
    # blow up early in tests
    def api_error(resource, field, code, options = {})
      raise "Invalid Error: #{code.inspect}" unless ERRORS.include?(code.to_s)
      options.update resource: resource, field: field, code: code
    end
  else
    def api_error(resource, field, code, options = {})
      options.update resource: resource, field: field, code: code
    end
  end

  # Public: Delivers an error response for this request.
  #
  # status  - Integer HTTP status code of the error.
  # options - Optional Hash to customize the error output.  Extra options are
  #           passed through to #deliver.
  #           :message           - String describing the error.
  #           :documentation_url - String URL for the documentation that will
  #                                help the user understand and resolve the
  #                                error (optional)
  #                                (default: 'https://developer.github.com').
  #           :errors            - Array of Strings with more detailed errors.
  #           :resource          - Optional String to override automatically-
  #                                detected resource.
  #
  # Returns a String body to be used as the response of this request.
  def deliver_error(status, options = {})
    doc_url = options.delete(:documentation_url)
    data = {}
    data[:message] = options.delete(:message)

    if err = options.delete(:errors)
      # Content creation rate limits are done as validations, so they'll get
      # here as a 422 (which is correct for browser flow). But they represent a
      # rate limit, so fix it to a more appropriate 403 for for API responses.
      if err.respond_to?(:full_messages)
        if err[:base].any? { |message| message.include?(GitHub::RateLimitedCreation::ERROR_MESSAGE) }
          status = 403
          doc_url = DOC_URL_RATE_LIMIT

          root_error_message = if root_of_error = err.instance_variable_get(:@base)
            root_of_error.class.to_s.underscore
          else
            nil
          end
          log_rate_limited_request(detailed_message: root_error_message)

          data[:message] = ERROR_MESSAGE_RATE_LIMIT
        else
          data[:errors] = convert_error(err, options[:resource])
        end
      else
        data[:errors] = err
      end
    end

    doc_url ||= @documentation_url || default_documentation_url
    doc_url = GitHub.developer_help_url + doc_url unless doc_url =~ /^http/

    data[:documentation_url] = doc_url

    data[:message] ||=
      case status
        when 404 then "Not Found"
        when 400 then "Bad Request"
        when 401 then "Requires authentication"
        when 403 then "Rate Limit Exceeded"
        when 409 then "Conflict"
        when 415 then "Unsupported Media Type"
        when 422 then "Validation Failed"
        when 301 then "Moved Permanently"
        when 302 then "Found"
        else          "Server Error"
      end

    deliver_raw data, options.update(status: status)
  end

  # Public: Delivers an error response based on a platform result error.
  #
  # options - Optional Hash to customize the error output.  Extra options are
  #           passed through to #deliver.
  #           :message           - String describing the error.
  #           :documentation_url - String URL for the documentation that will
  #                                help the user understand and resolve the
  #                                error (optional)
  #                                (default: 'https://developer.github.com').
  #           :errors            - A non-empty GraphQL::Client::Errors object
  #           :resource          - A String resource name that these errors
  #                                apply to
  #           :status            - Optional HTTP status. GraphQL errors can
  #                                usually figure out their own status code.
  #
  # Returns a String body to be used as the response of this request.
  def deprecated_deliver_graphql_error(options)
    options = options.dup
    status = options.delete(:status)

    raise ArgumentError, "A resource must be passed to #{__method__}" unless options[:resource].present?

    resource = options.delete(:resource)

    if !resource.is_a?(String)
      Failbot.report(ArgumentError.new("Resource of type `#{resource.class.name}` is not a string."))
      resource = resource.class.name
    end

    errors = options[:errors].try(:all)
    raise ArgumentError, "Empty error objects cannot be passed to #{__method__}" unless errors.try(:any?)

    if errors.details["data"].any? { |e| e["type"] == Platform::Errors::Unauthorized::Read.type }
      status ||= 404
      deliver_error(status)
    elsif (_error = errors.details["data"].find { |e| e["type"] == Platform::Errors::Unauthorized::Write.type })
      # We could, in theory, deliver a 403 here. But historically the REST API
      # has not distinguished between "can't see it" and "can see it, but can't
      # do that." We continue this tradition into the era of graphql.
      status ||= 404
      deliver_error(status)
    elsif (service_unavailable = errors.details["data"].find { |e| e["type"] == Platform::Errors::ServiceUnavailable.type })
      status ||= 503
      field_formatted_errors = [{ code: :service_unavailable, message: service_unavailable["message"] }]
      deliver_error(status, options.merge(errors: field_formatted_errors))
    else
      field_formatted_errors = []
      status ||= 422
      errors.details["data"].each do |error|
        if error["type"] == Platform::Errors::Validation::Field.type
          field_formatted_errors.push({
            resource: resource,
            code: :custom,
            field: error["field"],
            message: error["message"],
          })
        else
          field_formatted_errors.push({
            resource: resource,
            code: :unprocessable,
            field: "data",
            message: error["message"],
          })
        end
      end
      deliver_error(status, options.merge(errors: field_formatted_errors))
    end
  end

  # Halt the request with an error.
  #
  # See deliver_graphql_error for arguments and return values.
  def deprecated_deliver_graphql_error!(*args)
    halt deprecated_deliver_graphql_error(*args)
  end

  def convert_error(errors, resource = nil)
    Api::Serializer.validation_errors(errors, resource)
  end

  # Halts the request with an error.
  #
  # status  - Integer HTTP status code of the error.
  # options - Optional Hash to customize the error output.
  #           :message - String describing the error.
  #           :errors  - Array of Strings with more detailed errors.
  #
  # Halts with the given status and String response.
  # Returns nothing.
  def deliver_error!(status, options = {})
    halt deliver_error(status, options)
  end

  def deliver_content_authorization_denied!(authorization)
    instrument_content_authorization_failure(error: authorization.api_error)
    deliver_error! authorization.http_error_code, authorization.api_error_payload
  end

  def deliver_update_denied_using_ldap_sync!(message)
    url = "#{GitHub.enterprise_admin_help_url}/articles/ldap-sync"

    deliver_error! 403, message: message, documentation_url: url
  end

  def deliver_pagination_cap_exceeded!
    GitHub.dogstats.increment("pagination_cap", tags: ["via:api"])

    message =  "In order to keep the API fast for everyone, pagination is limited "
    message << "for this resource. Check the rel=last link relation in the Link "
    message << "response header to see how far back you can traverse."

    deliver_error! 422,
      message: message,
      documentation_url: "/v3/#pagination"
  end

  def deliver_disabled_repo_error!(repo)
    control_access :get_repo, resource: repo, allow_integrations: true, allow_user_via_integration: true

    halt deliver(:disabled_repository_hash, repo, status: 403)
  end

  def deliver_blocked_repo_error!(repo)
    control_access :get_repo, resource: repo, allow_integrations: true, allow_user_via_integration: true

    halt deliver(:disabled_repository_hash, repo, status: 451)
  end

  def deliver_disabled_gist_error!(gist)
    control_access :get_gist, resource: gist, allow_integrations: false, allow_user_via_integration: false

    gist.disabled_access_reason # preload the association

    halt deliver(:disabled_gist_hash, gist, status: 403)
  end

  # Halts the request because notifications are unavailable.
  def deliver_notifications_unavailable!
    deliver_error! 503
  end

  def default_documentation_url
    GitHub.developer_help_url + "/rest"
  end

  def deliver_schema_validation_error!(result)
    error_messages = result.error_messages.join("\n")
    deliver_error! 422, message: "Invalid request.\n\n#{error_messages}"
  end

  def has_graphql_mutation_errors?(results)
    results = results.to_h
    mutation_name = results["data"].keys.first
    results["data"][mutation_name]["errors"].present?
  end

  def deliver_graphql_mutation_errors(results, opts = {}, resource:, input_variables: nil)
    results = results.to_h
    mutation_name = results["data"].keys.first

    if resource && !resource.is_a?(String)
      Failbot.report(ArgumentError.new("Resource of type `#{resource.class.name}` is not a string."))
      resource = resource.class.name
    end

    errors = results["data"][mutation_name]["errors"].map do |error|
      path = path_with_integer_based_index(error["path"])

      if path
        api_error(resource, error["attribute"], :invalid, value: input_variables.dig(*path))
      else
        api_error(resource, error["attribute"], :invalid)
      end
    end

    deliver_error! 422, opts.merge(errors: errors)
  end

  def deliver_graphql_mutation_errors!(*args, **options)
    halt deliver_graphql_mutation_errors(*args, **options)
  end

  # Public
  # Returns true if there are top-level errors or errors on a mutation field
  def has_any_graphql_errors?(results)
    has_graphql_system_errors?(results) || has_graphql_mutation_errors?(results)
  end

  # Private
  # Returns true if the `results` have any top-level errors
  def has_graphql_system_errors?(results)
    results.errors.all.any?
  end


  # Public
  # Figure out what kind of errors are present in `results` and return
  # a response based on them, halting control flow.
  def deliver_graphql_error!(results, opts = {}, inputs: nil, resource: nil)
    if resource && !resource.is_a?(String)
      Failbot.report(ArgumentError.new("Resource of type `#{resource.class.name}` is not a string."))
      resource = resource.class.name
    end

    if has_graphql_system_errors?(results)
      halt deliver_graphql_system_error(results, opts, inputs: inputs, resource: resource)
    end

    if has_graphql_mutation_errors?(results)
      halt deliver_graphql_mutation_errors(results, opts, input_variables: inputs, resource: resource)
    end
  end

  # Private
  # Get root errors in `results` and return a REST response based on them.
  def deliver_graphql_system_error(results, opts = {}, inputs: nil, resource: nil)
    errors = results.errors.all.details["data"]
    error_types = errors.map { |err| err["type"] }

    if !(error_types & %w(NOT_FOUND UNAUTHENTICATED UNAUTHORIZED-READ UNAUTHORIZED-WRITE)).empty?
      status  = 404
      message = "Not Found"
    elsif error_types.any? { |err| err == "FORBIDDEN" }
      status  = 403
      message = "Forbidden"
    elsif error_types.any? { |err| err == "REPOSITORY_MIGRATION" }
      status  = 403
      message = "Repository has been locked for migration."
    elsif error_types.any? { |err| err == "REPOSITORY_ARCHIVED" }
      status  = 403
      message = "Repository was archived so is read-only."
    elsif error_types.any? { |err| err == "ISSUES_DISABLED" }
      status  = 410
      message = "Issues are disabled for this repository."
    elsif error_types.any? { |err| err == "SERVICE_UNAVAILABLE" }
      status  = 503
      message = "Service Unavailable"
    elsif errors.any? { |err| err["message"] == GitHub::RateLimitedCreation::ERROR_MESSAGE }
      status = 403
      message = ERROR_MESSAGE_RATE_LIMIT
      errors = nil
      documentation_url = DOC_URL_RATE_LIMIT
      log_rate_limited_request
    else
      status  = 422
      message = "Unprocessable Entity"
      errors  = results.errors.all.values.flatten
    end

    options = {}.tap do |hash|
      hash[:message]  = message if message
      hash[:errors]   = errors if errors
      hash[:documentation_url] = documentation_url if documentation_url
    end

    deliver_error status, options
  end

  # Private: Convert an array of string based path elements to integers if they
  # exist.
  #
  # path - An array of strings.
  #
  # Examples
  #
  # path = ["input", "labelNames", "1"]
  # path_with_integer_based_index(path)
  # #=> ["input", "labelNames", 1]
  #
  # Returns an array of strings and integers.
  def path_with_integer_based_index(path)
    return unless path

    path.map do |element|
      if element =~ /\A\d+\z/
        element.to_i
      else
        element
      end
    end
  end

  # Write to logs & Hydro context that a rate limiter was applied.
  # @return void
  def log_rate_limited_request(detailed_message: nil)
    logging_limit_message ||= "api/rate-limited-creation"

    if detailed_message
      logging_limit_message += "/#{detailed_message}"
    end

    if (log_data = env[Rack::RequestLogger::APPLICATION_LOG_DATA])
      log_data["limited"] = logging_limit_message
    end

    if (hydro_payload = env[GitHub::HydroMiddleware::PAYLOAD])
      hydro_payload["secondary_rate_limit_reason"] = logging_limit_message
    end
  end
end
