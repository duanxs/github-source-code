# rubocop:disable Style/FrozenStringLiteralComment

module Api::App::HelpersDependency
  # Public: Helper to coerce String params into Booleans
  #
  # value - The param value
  #
  # Returns a Boolean
  def parse_bool(value)
    ActiveRecord::Type::Boolean.new.deserialize(value)
  end

  # Applies the `since` scoping to a query.
  def since_filter(scope)
    time = Time.parse(time_param!(:since).to_s).getlocal
    scope.where(["#{scope.table_name}.updated_at >= ?", time])
  rescue ArgumentError
    docs_url = case scope.table_name
    when "pull_requests"
    "/v3/pulls/#list-pull-requests"
    else
    "/v3/issues/#list-issues"
    end
    deliver_error! 422,
      message: "Invalid datetime for since",
      documentation_url: docs_url
  end

  # Public: Helper to check if a String is present yet falsey
  #
  # value - The param value
  #
  # Returns a Boolean
  def explicitly_false(value)
    value.present? && !parse_bool(value)
  end

  # Ensures the Repository has a non-empty git repo.
  #
  # repo - Repository to check.
  #
  # Halts the request if the Repository is empty.
  def ensure_repo_content!(repo)
    deliver_error!(409, message: "Git Repository is empty.") if repo.empty?
  end

  # Ensures the repository is writable. Currently used to stop updates to
  # a repository and related models during a migration or when the repository
  # is archived.
  #
  # repo - Repository to check.
  # file - Single file to check access to.
  #
  # Halts the request if the repository is locked for migration or archived.
  def ensure_repo_writable!(repo, path: nil)
    if repo.private?
      verb = path.present? ? :write_file : :get_repo
      control_access verb, resource: repo, path: path, allow_integrations: true, allow_user_via_integration: true
    end

    deliver_error_if_locked_on_migration! repo
    deliver_error_if_archived! repo
    log_if_over_quota repo
  end

  # Ensures the file is writable. Currently used to stop writes to a
  # repository's file if it's a sensitive file such as workflows/a.yml or
  # workflow-labs/ci.yml
  #
  # repo - Repository to check.
  # path - File to check access to.
  #
  # If the file is a blacklisted file, halt the request.
  def ensure_file_writable!(repo, path, oid)
    unless RefUpdatesPolicy.path_updateable_for_app?(repo, path, oid)
      control_access :write_file, resource: repo, path: path, allow_integrations: true, allow_user_via_integration: true
    end
  end


  # Ensures the files changed between two shas are all writable. Currently used
  # to stop writes to a repository's file if it's a sensitive file such as
  # workflows/a.yml or workflows-lab/ci.yml
  #
  # repo - Repository to check.
  # diff - GitHub::Diff to check access of changed files.
  #
  # If a file which changes between base and head is blacklisted, halt the request.
  def ensure_changed_files_writable!(repo, diff)
    diff.summary.deltas.map do |delta|
      file = delta.new_file
      ensure_file_writable!(repo, file.path, file.oid)
    end
  rescue GitRPC::InvalidFullOid
    deliver_error!(404, message: "Head does not exist")
  end

  # Delivers an error if the repo is locked during a migration.
  #
  # repo - Repository to check.
  #
  # Halts the request if the repository is locked for migration.
  def deliver_error_if_locked_on_migration!(repo)
    if repo.locked_on_migration?
      deliver_error! 403, message: "Repository has been locked for migration."
    end
  end

  # Delivers an error if the repo is archived.
  #
  # repo - Repository to check.
  #
  # Halts the request if the repository is archived.
  def deliver_error_if_archived!(repo)
    if repo.archived?
      deliver_error! 403, message: "Repository was archived so is read-only."
    end
  end

  # Report to DD if the repository is above its size quota. This will eventually
  # turn into delivering an error.
  #
  # repo - Repository to check.
  def log_if_over_quota(repo)
    if repo.above_lock_quota?
      GitHub.dogstats.increment("git.api.quota", tags: ["type:lock"])
    elsif repo.above_warn_quota?
      GitHub.dogstats.increment("git.api.quota", tags: ["type:warn"])
    end
  end

  # Ensures the user actor is not blocked by another resource owner.
  #
  # user     - User actor to check.
  # owner_id - Integer ID of potentially blocking owner.
  #
  # Halts the request if the user is blocked by the given owner.
  def ensure_not_blocked!(user, owner_id)
    return false if !owner_id || !user || user.id == owner_id
    if user.blocked_by?(owner_id)
      deliver_error! 403, message: "Blocked"
    end
  end

  # Ensures the request supplies the required media types for a resource.
  # Usually used for preview periods.
  #
  # Halts if the proper media type is not supplied.
  def ensure_acceptable_media_types!
    if unacceptable_media_types?
      message = "Unsupported 'Accept' header: #{request.accept.inspect}. " +
        "Must accept 'application/json'."
      deliver_error! 415, message: message,
        documentation_url: "/v3/media"
    end
  end

  def deliver_undiffable_error!(resource_type, diff)
    valid_resource_types = [:PullRequest, :Commit, :Comparison]

    unless valid_resource_types.include?(resource_type)
      raise ArgumentError, "resource_type must be one of #{valid_resource_types}"
    end

    message =
      if diff.corrupt?
        "Server Error: Sorry, there was a problem generating this diff. The repository may be missing relevant data."
      elsif diff.too_busy?
        "Server Error: Sorry, this diff is temporarily unavailable due to heavy server load."
      elsif diff.missing_commits?
        "Sorry, there was a problem generating this diff. The repository may be missing relevant data."
      else
        "Server Error: Sorry, this diff is taking too long to generate."
      end

    response = {
      message: message,
      errors: [api_error(resource_type, :diff, :not_available)],
    }

    response[:documentation_url] = "/v3/pulls#diff-error" if diff.corrupt? || diff.missing_commits?

    # Don't report errors for missing commits. This is a valid state and would flood haystack if reported.
    # If there is no error, then the diff is either on a null git object or the unavailable reason
    # is cached and not worth reporting to Failbot.
    if !diff.missing_commits? && diff.unavailable_error
      Failbot.report(diff.unavailable_error)
    end

    # There are valid scenarios in which a pull request cannot be viewed due to missing commits.
    # For example the branch could have been force pushed so that it no longer shares a common
    # merge base with the base branch, or perhaps the head commit was removed due to a privacy
    # violation. These cases are not considered server errors.
    response_code = diff.missing_commits? ? 422 : 500

    deliver_error! response_code, response
  end

  # Provides basic GitRPC error handling when rendering raw diff content.
  def delivering_raw_diff_content(medias, &block)
    return unless block.respond_to?(:call)

    begin
      block.call
    rescue GitRPC::ObjectMissing, GitRPC::InvalidObject
      deliver_error! 404
    rescue GitRPC::Timeout
      reason = if medias.api_param?(:diff)
        "diff"
      elsif medias.api_param?(:patch)
        "patch"
      else
        "request"
      end
      deliver_error! 422, message: "The #{reason} could not be processed because too many files changed"
    end
  end

  def set_pagination_headers(collection_size: nil)
    paginator.collection_size ||= collection_size
    return if paginator.collection_size.nil? || paginator.collection_size.zero?

    if paginator.previous?
      @links.add_current({page: paginator.previous_page}, rel: "prev")
    end

    if paginator.next?
      @links.add_current({page: paginator.next_page}, rel: "next")
    end

    if paginator.page != paginator.last_page
      @links.add_current({page: paginator.last_page}, rel: "last")
    end

    if paginator.page != paginator.first_page
      @links.add_current({page: paginator.first_page}, rel: "first")
    end
  end

  def request_id
    request.env[Rack::RequestId::GITHUB_REQUEST_ID] || ""
  end

  # Given a user authenticating through an App -- can that App
  # modify the App given? In other words, this method checks
  # to ensure that App X is modifying values for App X,
  # and no other App
  #
  # **NOTE:** This method should be called AFTER
  # any relevant Egress role is checked!
  #
  def allowed_to_modify_app?(app_id:)
    # server-server: is it modifying itself?
    if current_user.respond_to?(:installation)
      current_integration.id == app_id.to_i
    # user-server: is it modifying itself?
    elsif current_user.using_auth_via_integration?
      current_user.oauth_access.application_id == app_id.to_i
    # user or OAuth app, go for it
    else
      true
    end
  end
end
