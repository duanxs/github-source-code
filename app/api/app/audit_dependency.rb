# frozen_string_literal: true

#
# NOTE: The data set here is written with every audit log event. Please ping
# the @github/audit-log team if adding/removing/updating any fields.
#

module Api::App::AuditDependency
  def initialize_audit_context
    # WARNING: Do not call any methods that can cause the request to
    # short-circuit  (e.g., don't call any methods that attempt to authenticate
    # the request). Doing so will prevent us from fully populating the context.
    context = {
      actor_ip: remote_ip,
      user_agent: request.user_agent.to_s,
      controller: self.class.to_s,
      from: "%s#%s" % [self.class, request.request_method],
      request_id: request.env["HTTP_X_GITHUB_REQUEST_ID"],
      request_method: request.env["REQUEST_METHOD"].downcase,
      request_category: request_category,
      server_id: Rack::ServerId.get(request.env),
      version: medias.to_api_version,
    }

    context[:robot] = robot_type if robot?

    unless request.query_string.empty?
      context[:query_string] = query_string_for_logging
    end

    Audit.context.push(context)
  end

  def populate_audit_context_with_authentication_details
    context = { auth: detect_auth }
    context[:current_user] = current_user.to_s if logged_in?
    context[:oauth_application_id] = current_app.id if current_app
    context[:integration_id] = current_integration.id if current_integration
    context[:installation_id] = current_integration_installation.id if current_integration_installation
    context[:parent_installation_id] = current_parent_integration_installation.id if current_parent_integration_installation
    context[:enterprise_installation_id] = current_enterprise_installation.id if current_enterprise_installation
    if logged_in? && current_user.oauth_access
      context[:oauth_scopes] = current_user.oauth_access.scopes_string
      context[:oauth_access_id] = current_user.oauth_access.id
      context[:oauth_application_id] = current_user.oauth_application_id
    end

    Audit.context.push(context)
  end
end
