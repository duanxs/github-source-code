# frozen_string_literal: true

# Rate limiting filters and requests helpers.

module Api::App::RateLimitDependency
  extend ActiveSupport::Concern

  ApiSkipRate = "github.skip_rate_limit"
  HTTP_X_GITHUB_DYNAMIC_CACHE = "HTTP_X_GITHUB_DYNAMIC_CACHE"

  class_methods do
    def rate_limit_as(family)
      # Provides the RateLimitConfiguration describing the rate limit rules for the
      # resource family associated with the current request and the API consumer
      # that initiated the request.
      define_method :rate_limit_configuration do
        if defined?(@rate_limit_configuration)
          @rate_limit_configuration
        else
          @rate_limit_configuration = if family.nil?
            nil
          else
            Api::RateLimitConfiguration.for(
              family,
              self,
            )
          end
        end
      end
    end
  end

  included do
    # Set the default configuration family
    # Concrete classes will likely want to set their own.
    rate_limit_as Api::RateLimitConfiguration::DEFAULT_FAMILY
  end

  # Determines whether access to the current route is forbidden once the
  # requestor uses up its rate limit.
  #
  # Returns a Boolean: true if access is forbidden once the rate limit is used
  # up; false otherwise.
  def rate_limited_route?
    case request.path_info
    when "/status"
      # It's a health check, don't rate-limit
      false
    when "/rate_limit"
      # Checking the limit is free
      false
    else
      true
    end
  end

  # Calling this in an API action prevents the rate limit from incrementing for
  # the current request.  This does not allow access to an API resource if the
  # rate limit is already exceeded.
  def skip_rate_limit!
    return if !rate_limiting_enabled?
    return if throttler.nil?

    env[ApiSkipRate] = true
    return unless rate = throttler.check
    set_rate_limit!(rate)
  end

  # This method is called twice during the life of a normal request:
  # - once in a `before` block, via `ensure_request_is_within_rate_limit!`
  # - once in an `after` block, via `increment_limit_and_set_headers!`
  def set_rate_limit!(rate)
    @rate = rate
    # We have to do this in two places because this mixin may be used with a JSONP
    # request which needs these headers to be serialized into the JSON response.
    rate.set_headers(@meta)
    rate.set_headers(response.headers)
  end

  def increment_rate_limit?(code = nil)
    code ||= response.status.to_i
    code != 304 && env[ApiSkipRate].nil?
  end

  def increment_rate_limit_amount
    1
  end

  def increment_rate_limit_and_set_headers!
    return if !rate_limiting_enabled?
    return if throttler.nil?
    return if !increment_rate_limit?

    GitHub.dogstats.increment("api_rate_limiter.rate", tags: ["store:redis"], sample_rate: 0.01)
    set_rate_limit!(throttler.rate!)
  end

  def ensure_request_is_within_rate_limit!
    return if !rate_limited_route?

    at_rate_limit = if !rate_limiting_enabled? || throttler.nil?
      false
    else
      GitHub.dogstats.increment("api_rate_limiter.check", tags: ["store:redis"], sample_rate: 0.01)
      rate = throttler.check
      set_rate_limit!(rate)
      rate.at_limit?
    end

    if at_rate_limit
      # Record the rate limited request
      auth_or_anon = rate_limit_configuration.authenticated_request? ? "auth" : "anon"
      family = rate_limit_configuration.family
      GitHub.dogstats.increment("rate_limited", {tags: ["via:api", "family:#{family}", "logged_in:#{auth_or_anon}", "store:redis"]})

      # Return an error response with a message
      type, id = rate_limit_configuration.key.split("-")
      message = if id
        "API rate limit exceeded for #{type} ID #{id}."
      else
        "API rate limit exceeded for #{type}."
      end

      if !rate_limit_configuration.authenticated_request?
        message += " (But here's the good news: " \
          "Authenticated requests get a higher rate limit. " \
          "Check out the documentation for more details.)"
      end

      deliver_error! rate_limit_status_code,
        message: message,
        documentation_url: "/v3/#rate-limiting"
    end
  end

  def rate_limit_status_code
    403
  end

  # Internal: Provide an Api::ConfigThrottler for the current request.
  #
  # Returns an Api::Throttler.
  def throttler
    return @throttler if defined?(@throttler)
    return if rate_limit_configuration.nil?

    options = { amount: increment_rate_limit_amount }
    @throttler = Api::ConfigThrottler.new(rate_limit_configuration, options)
  end

  def rate_limiting_enabled?
    # Don't track rate limits for varnished requests. This is handled
    # at the Varnish layer already.
    return false if GitHub.varnish_enabled? && request.env[HTTP_X_GITHUB_DYNAMIC_CACHE] == "api"
    GitHub.rate_limiting_enabled?
  end
end
