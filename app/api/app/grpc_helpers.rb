# frozen_string_literal: true

module Api::App::GrpcHelpers
  def rescue_from_grpc_errors(service_name)
    result = GrpcHelper.rescue_from_grpc_errors(service_name) do
      yield
    end

    return result.value if result.call_succeeded?
    if result.options.present?
      deliver_error!(result.status, result.options)
    else
      deliver_error!(result.status)
    end
  end
end
