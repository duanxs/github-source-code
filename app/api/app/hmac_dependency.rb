# frozen_string_literal: true

# Code for Request-HMAC verification. The Request-HMAC header can be sent by
# internal services with API requests to identify themselves as trusted.
module Api::App::HmacDependency
  REQUEST_HMAC_INTERVAL = 10.minutes
  REQUEST_HMAC_HEADER = "HTTP_REQUEST_HMAC"
  REQUEST_HMAC_INVALID = "Request-HMAC is not valid."
  HMAC_ALGORITHM = "sha256"

  class InvalidRequestHMAC < StandardError; end
  class MissingRequestHMACKeys < StandardError; end

  module InstanceMethods
    # Is this a request from an internal service with a valid Request-HMAC header?
    #
    # Sets `env[:request_hmac_key]` with the value of the matching HMAC key if
    # it returns true.
    #
    # Returns Boolean.
    def hmac_authenticated_internal_service_request?
      received_hmac = env[REQUEST_HMAC_HEADER]
      return false unless received_hmac.present?
      hmac_status, matching_key = self.class.verify_request_hmac(received_hmac)

      env[:request_hmac_key] = matching_key if hmac_status == :success
      hmac_status == :success
    end
  end

  module ClassMethods
    # Takes the HMAC of an integer epoch timestamp with key.
    def request_hmac(time, key)
      timestamp = time.to_i.to_s

      hmac = OpenSSL::HMAC.hexdigest(
        HMAC_ALGORITHM,
        key, timestamp
      )
      "#{timestamp}.#{hmac}"
    end

    # While we transition, use the same shared key as the content HMAC. Eventually
    # we will raise a NotImplementedError and require subclasses to define a key.
    def request_hmac_keys
      return @request_hmac_keys if defined?(@request_hmac_keys)
      # Rather than have to define this for every single subclass, we follow a
      # naming convention. Api::Internal::Pages results in a
      # config value of GitHub.api_internal_pages_hmac_key.
      config_value = "#{self.to_s.underscore.gsub("/", "_")}_hmac_keys"
      # During the transition we support either per-endpoint defined HMAC key(s)
      # OR the global key currently used for the content HMAC we are moving away
      # from.
      @request_hmac_keys =
        GitHub.try(config_value).presence || Array(GitHub.internal_api_hmac_key.to_s)
    end

    def verify_request_hmac(received_hmac)
      unless request_hmac_keys.present?
        err = MissingRequestHMACKeys.new "No Request-HMAC keys are set for #{self}"
        return status = :no_key
      end

      if received_hmac.blank?
        err = InvalidRequestHMAC.new "Request-HMAC header not present"
        return status = :empty_request_hmac
      end

      timestamp, hmac = received_hmac.split(".", 2)

      if timestamp.blank? || hmac.blank?
        err = InvalidRequestHMAC.new "Request-HMAC header missing either timestamp or hmac value"
        return status = :invalid_request_hmac
      end

      # We only need the integer value going forward.
      timestamp = timestamp.to_i

      # Allow for a bit of clock skew, latency, etc in either direction.
      valid_timestamp =
        timestamp > REQUEST_HMAC_INTERVAL.ago.to_i &&
        timestamp < REQUEST_HMAC_INTERVAL.from_now.to_i

      unless valid_timestamp
        err = InvalidRequestHMAC.new "Timestamp is outside the allowed range"
        return status = :invalid_timestamp
      end

      matching_key = request_hmac_keys.detect do |request_hmac_key|
        SecurityUtils.secure_compare(
          received_hmac,
          request_hmac(Time.at(timestamp), request_hmac_key),
        )
      end
      status = matching_key ? :success : :fail
      [status, matching_key]
    ensure
      Failbot.report_user_error(err) if err
      GitHub.dogstats.increment("api.internal.request_hmac", tags: ["status:#{status}"])
    end
  end

  def self.included(receiver)
    receiver.extend ClassMethods
    receiver.send :include, InstanceMethods
  end
end
