# frozen_string_literal: true

class Api::RepositoryActivity < Api::App
  areas_of_responsibility :notifications, :api
  register Api::App::MultiRoute
  include ::Api::App::ProtectedResourcesFilter

  # List repositories the authenticated user is watching
  get "/user/subscriptions" do
    @route_owner = "@github/notifications"
    @documentation_url = "/rest/reference/activity#list-repositories-watched-by-the-authenticated-user"
    require_authentication!
    control_access :list_watched,
      resource: Platform::PublicResource.new,
      allow_integrations: false,
      allow_user_via_integration: true,
      installation_required: false

    repos = watched_repos_for current_user

    if protected_organization_ids.any?
      set_sso_partial_results_header if protected_sso_organization_ids.any?
      repos = filter_protected_resources \
        type: :watched,
        resources: repos,
        protected_org_ids: protected_organization_ids
    end

    GitHub::PrefillAssociations.for_repositories(repos)
    GitHub::PrefillAssociations.fill_licenses(repos)

    deliver :repository_hash, repos
  end

  # List repositories a user is watching
  get "/user/:user_id/subscriptions" do
    @route_owner = "@github/notifications"
    @documentation_url = "/rest/reference/activity#list-repositories-watched-by-a-user"
    user = find_user!

    control_access :list_watched,
      resource: Platform::PublicResource.new,
      user: user,
      allow_integrations: true,
      allow_user_via_integration: true,
      installation_required: false

    repos = watched_repos_for user

    GitHub::PrefillAssociations.for_repositories(repos)
    GitHub::PrefillAssociations.fill_licenses(repos)

    deliver :repository_hash, repos
  end

  # Get if a repository is watched by the authenticated user
  get "/user/subscriptions/:owner/:repo" do
    @route_owner = "@github/notifications"
    @documentation_url = Platform::NotDocumentedBecause::DEPRECATED
    require_authentication!
    repo = this_repo

    @accepted_scopes = repo.public? ? %w(public_repo repo) : %w(repo)

    control_access :read_user_repo_subscription,
      resource: repo,
      allow_integrations: false,
      allow_user_via_integration: true,
      installation_required: false

    status_response = GitHub.newsies.subscription_status(current_user, repo)
    if status_response.failed?
      deliver_notifications_unavailable!
    end

    if status_response.subscribed?
      deliver_empty(status: 204)
    else
      deliver_error 404
    end
  end

  # Watch a repository
  put "/user/subscriptions/:owner/:repo" do
    @route_owner = "@github/notifications"
    @documentation_url = Platform::NotDocumentedBecause::DEPRECATED
    require_authentication!
    repo = this_repo

    @accepted_scopes = repo.public? ? %w(public_repo repo) : %w(repo)

    control_access :write_user_repo_subscription,
      resource: repo,
      enforce_oauth_app_policy: repo.private?,
      allow_integrations: false,
      allow_user_via_integration: true,
      installation_required: false

    current_user.watch_repo repo

    deliver_empty(status: 204)
  end

  # Unwatch a repository
  delete "/user/subscriptions/:owner/:repo" do
    @route_owner = "@github/notifications"
    @documentation_url = Platform::NotDocumentedBecause::DEPRECATED
    require_authentication!
    repo = this_repo

    @accepted_scopes = repo.public? ? %w(public_repo repo) : %w(repo)

    control_access :write_user_repo_subscription,
      resource: repo,
      enforce_oauth_app_policy: repo.private?,
      allow_integrations: false,
      allow_user_via_integration: true,
      installation_required: false

    current_user.unwatch_repo repo

    deliver_empty(status: 204)
  end

  # List users watching a repository
  get "/repositories/:repository_id/subscribers" do
    @route_owner = "@github/notifications"
    @documentation_url = "/rest/reference/activity#list-watchers"
    control_access :list_watchers, resource: repo = find_repo!,
      allow_integrations: true,
      allow_user_via_integration: true

    watchers_response = repo.watchers(pagination[:page], pagination[:per_page])
    if watchers_response.failed?
      deliver_notifications_unavailable!
    end
    watchers = watchers_response.value

    deliver :user_hash, watchers
  end

  # Starring
  #

  # List repositories the authenticated user has starred
  get "/user/starred" do
    @route_owner = "@github/notifications"
    @documentation_url = "/rest/reference/activity#list-repositories-starred-by-the-authenticated-user"
    require_authentication!
    control_access :list_starred,
      resource: Platform::PublicResource.new,
      allow_integrations: false,
      allow_user_via_integration: true,
      installation_required: false

    current_user_stars = stars_for current_user

    if protected_organization_ids.any?
      set_sso_partial_results_header if protected_sso_organization_ids.any?
      current_user_stars = filter_protected_resources \
        type: :starred,
        resources: current_user_stars,
        protected_org_ids: protected_organization_ids
    end

    GitHub::PrefillAssociations.for_stars(current_user_stars)

    if medias.api_param?(:star)
      deliver :star_hash_with_timestamps, current_user_stars
    else
      deliver :star_hash, current_user_stars
    end
  end

  get "/user/watched" do
    @route_owner = "@github/notifications"
    @documentation_url = Platform::NotDocumentedBecause::DEPRECATED
    require_authentication!
    control_access :list_starred,
      resource: Platform::PublicResource.new,
      allow_integrations: false,
      allow_user_via_integration: true,
      installation_required: false

    stars = stars_for current_user
    GitHub::PrefillAssociations.for_stars(stars)

    if medias.api_param?(:star)
      deliver :star_hash_with_timestamps, stars
    else
      deliver :star_hash, stars
    end
  end

  # List repositories a user has starred
  get "/user/:user_id/starred" do
    @route_owner = "@github/notifications"
    @documentation_url = "/rest/reference/activity#list-repositories-starred-by-a-user"
    user = find_user!

    control_access :list_starred,
      resource: Platform::PublicResource.new,
      user: user,
      allow_integrations: true,
      allow_user_via_integration: true,
      installation_required: false

    stars = stars_for user
    GitHub::PrefillAssociations.for_stars(stars)

    if medias.api_param?(:star)
      deliver :star_hash_with_timestamps, stars
    else
      deliver :star_hash, stars
    end
  end

  get "/user/:user_id/watched" do
    @route_owner = "@github/notifications"
    @documentation_url = Platform::NotDocumentedBecause::DEPRECATED
    user = find_user!

    control_access :list_starred,
      resource: Platform::PublicResource.new,
      user: user,
      allow_integrations: true,
      allow_user_via_integration: true,
      installation_required: false

    stars = stars_for user
    GitHub::PrefillAssociations.for_stars(stars)

    if medias.api_param?(:star)
      deliver :star_hash_with_timestamps, stars
    else
      deliver :star_hash, stars
    end
  end

  # Get if a repository is starred by the authenticated user
  get "/user/starred/:owner/:repo" do
    @route_owner = "@github/notifications"
    @documentation_url = "/rest/reference/activity#check-if-a-repository-is-starred-by-the-authenticated-user"
    require_authentication!

    repo = this_repo
    @accepted_scopes = repo.public? ? %w(public_repo repo) : %w(repo)

    control_access :get_star,
      resource: repo,
      allow_integrations: false,
      allow_user_via_integration: true,
      installation_required: false

    if current_user.starred?(repo)
      deliver_empty(status: 204)
    else
      deliver_error 404
    end
  end

  get "/user/watched/:owner/:repo" do
    @route_owner = "@github/notifications"
    @documentation_url = Platform::NotDocumentedBecause::DEPRECATED
    require_authentication!

    repo = this_repo
    @accepted_scopes = repo.public? ? %w(public_repo repo) : %w(repo)

    control_access :get_star,
      resource: repo,
      allow_integrations: false,
      allow_user_via_integration: true,
      installation_required: false

    if current_user.starred?(repo)
      deliver_empty(status: 204)
    else
      deliver_error 404
    end
  end

  # Star a repository
  put "/user/starred/:owner/:repo" do
    @route_owner = "@github/notifications"

    # Introducing strict validation of the repository.star
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("repository", "star", skip_validation: true)

    @documentation_url = "/rest/reference/activity#star-a-repository-for-the-authenticated-user"
    require_authentication!

    repo = this_repo
    @accepted_scopes = repo.public? ? %w(public_repo repo) : %w(repo)

    control_access :star,
      resource: repo,
      enforce_oauth_app_policy: repo.private?,
      allow_integrations: false,
      allow_user_via_integration: true,
      installation_required: false

    authorize_content(:star, :create, starrable: repo)
    current_user.star(repo, context: "api")

    deliver_empty(status: 204)
  end

  put "/user/watched/:owner/:repo" do
    @route_owner = "@github/notifications"
    @documentation_url = Platform::NotDocumentedBecause::DEPRECATED
    require_authentication!

    repo = this_repo
    @accepted_scopes = repo.public? ? %w(public_repo repo) : %w(repo)

    control_access :star,
      resource: repo,
      enforce_oauth_app_policy: repo.private?,
      allow_integrations: false,
      allow_user_via_integration: true,
      installation_required: false

    current_user.star(repo, context: "api")

    deliver_empty(status: 204)
  end

  # Unstar a repository
  delete "/user/starred/:owner/:repo" do
    @route_owner = "@github/notifications"

    # Introducing strict validation of the repository.unstar
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("repository", "unstar", skip_validation: true)

    @documentation_url = "/rest/reference/activity#unstar-a-repository-for-the-authenticated-user"
    require_authentication!

    repo = this_repo
    @accepted_scopes = repo.public? ? %w(public_repo repo) : %w(repo)

    control_access :unstar,
      resource: repo,
      enforce_oauth_app_policy: repo.private?,
      allow_integrations: false,
      allow_user_via_integration: true,
      installation_required: false

    current_user.unstar repo

    deliver_empty(status: 204)
  end

  delete "/user/watched/:owner/:repo" do
    @route_owner = "@github/notifications"
    @documentation_url = Platform::NotDocumentedBecause::DEPRECATED
    require_authentication!

    repo = this_repo
    @accepted_scopes = repo.public? ? %w(public_repo repo) : %w(repo)

    control_access :unstar,
      resource: repo,
      enforce_oauth_app_policy: repo.private?,
      allow_integrations: false,
      allow_user_via_integration: true,
      installation_required: false

    current_user.unstar repo

    deliver_empty(status: 204)
  end

  # List users starring a repository
  get "/repositories/:repository_id/stargazers" do
    @route_owner = "@github/notifications"
    @documentation_url = "/rest/reference/activity#list-stargazers"
    control_access :list_stargazers, resource: repo = find_repo!,
      allow_integrations: true,
      allow_user_via_integration: true

    cap_paginated_entries!

    scope = repo.stars.filter_spam_for(current_user)
    # If user is nil/false, force the MySQL optimizer to use the known best index
    if !logged_in?
      scope = scope.from("stars FORCE INDEX(index_stars_on_starrable_id_type_created_user_hidden)")
    end

    stargazers = paginate_rel(scope.order("stars.created_at ASC"))
    GitHub::PrefillAssociations.prefill_belongs_to stargazers, User, :user_id

    if medias.api_param?(:star)
      deliver :stargazer_hash_with_timestamps, stargazers
    else
      deliver :stargazer_hash, stargazers
    end
  end

  get "/repositories/:repository_id/watchers" do
    @route_owner = "@github/notifications"
    @documentation_url = Platform::NotDocumentedBecause::DEPRECATED
    control_access :list_stargazers, resource: repo = find_repo!,
      allow_integrations: true,
      allow_user_via_integration: true

    cap_paginated_entries!

    scope = repo.stars.filter_spam_for(current_user)
    # If user is nil/false, force the MySQL optimizer to use the known best index
    if !logged_in?
      scope = scope.from("stars FORCE INDEX(index_stars_on_starrable_id_type_created_user_hidden)")
    end

    stargazers = paginate_rel(scope.order("stars.created_at ASC"))
    GitHub::PrefillAssociations.prefill_belongs_to stargazers, User, :user_id

    if medias.api_param?(:star)
      deliver :stargazer_hash_with_timestamps, stargazers
    else
      deliver :stargazer_hash, stargazers
    end
  end

  private

  def stars_for(user)
    access_allowed = access_allowed?(:list_private_stars, resource: user, allow_integrations: false, allow_user_via_integration: true, installation_required: false)
    order = params[:sort]
    direction = params[:direction]
    direction = (direction == "asc" ? :asc : :desc)
    starrable_ids = user.stars.repositories.pluck(:starrable_id)

    starred_repo_scope = if access_allowed
      Platform::Security::RepositoryAccess.with_viewer(current_user) do
        permission = Platform::Authorization::Permission.new(viewer: current_user, integration: current_integration, origin: Platform::ORIGIN_API)
        permission.filtered_permissible_repository_scope(user, starrable_ids, resource: "metadata")
      end
    else
      Repository.public_scope.with_ids(starrable_ids)
    end

    case order
    when "updated"
      ordered_scope = starred_repo_scope.order(pushed_at: direction).order(:id)
      paginate_stars(user, ordered_scope)
    when  "stars"
      ordered_scope = starred_repo_scope.order("#{Repository.stargazer_count_column} #{direction}").order(:id)
      paginate_stars(user, ordered_scope)
    else
      starred_repository_ids = starred_repo_scope.ids
      scope = user.stars.repositories.with_ids(starred_repository_ids, field: :starrable_id).order(created_at: direction).order(:id)
      paginate_rel(scope)
    end
  end

  def watched_repos_for(user)
    watched_repositories = WatchedRepositories.new(user)

    response = if access_allowed?(:list_private_watched_repos, resource: user, allow_integrations: false, allow_user_via_integration: true, installation_required: false)
      if current_user.using_auth_via_integration?
        watched_repositories.integration_paginate(current_integration, current_user, pagination)
      elsif requestor_governed_by_oauth_application_policy?
        watched_repositories.oap_paginate(current_app, pagination)
      else
        watched_repositories.paginate(pagination)
      end
    else
      watched_repositories.public_paginate(pagination)
    end

    if response.failed?
      deliver_notifications_unavailable!
    end

    response.value
  end

  def authorize_content(kind, operation = :create, data = {})
    authorization = ContentAuthorizer.authorize(current_user, kind, operation, data)
    if authorization.failed?
      deliver_error! authorization.http_error_code, authorization.api_error_payload
    end
  end

  def paginate_stars(user, ordered_scope)
    paginated_scope = ordered_scope.paginate(per_page: pagination[:per_page], page: pagination[:page])
    ids_for_page = paginated_scope.ids

    WillPaginate::Collection.create(pagination[:page], pagination[:per_page], paginated_scope.total_entries) do |page|
      page.replace(user.stars.repositories.with_ids(ids_for_page, field: "stars.starrable_id").order(
          Arel.sql("FIELD(starrable_id, #{ids_for_page.join(",")})"), "id"
      ))
    end
  end
end
