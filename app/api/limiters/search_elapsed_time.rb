# rubocop:disable Style/FrozenStringLiteralComment

require "elastomer/query_stats"

module Api
  module Limiters
    class SearchElapsedTime < GitHub::Limiters::MemcachedWindow
      SEARCH_PATH = /\A(?:\/api\/v\d+)?\/search\/([^\/]+)\/?\z/i.freeze

      CODE_SEARCH_TYPE = "code".freeze
      CODE_SEARCH_COST_FACTOR = 3

      # Create a new SearchElaspedTime cost-based rate limit. The query time is
      # used as the cost factor. Each user can use up to `max` milliseconds of
      # query time every `ttl` period.
      #
      # max - The maximum number of milliseconds that can be used over the TTL
      #       period before rate limiting kicks in and 429 responses are returned
      # ttl - The "time to live" for rate limiting. The default is 60 seconds.
      #
      def initialize(max:, ttl: 60)
        super("search-elapsed-time", limit: max, ttl: ttl)
      end

      def start(request)
        return OK unless search_request?(request)
        super(request)
      end

      def record_finish(request)
        return OK unless search_request?(request)
        increment_counter(request)
      end

      # Internal: a request was canceled.
      #
      # This method is a no-op for this elapsed time rate limiter. Since cost is
      # calculated at the end of each request, there is no data to "cancel"
      # (i.e. remove from memcached).
      def cancel(request)
        return OK
      end

    protected

      def key(request)
        "#{search_type(request)}:#{fingerprint(request)}"
      end

      # The cost is the number of milliseconds this request has taken.
      def cost(request)
        millis = Elastomer::QueryStats.instance.time
        millis = CODE_SEARCH_COST_FACTOR * millis if code_search?(request)
        millis
      end

      def fingerprint(request)
        Api::Middleware::RequestAuthenticationFingerprint.get(request.env).to_s
      end

      # Returns the search type extracted from the `path_info`. Returns nil if
      # the current request is not a search request.
      def search_type(request)
        if match = SEARCH_PATH.match(request.path_info)
          match[1].downcase
        end
      end

      # Returns true if the current request is using the search API.
      def search_request?(request)
        !search_type(request).nil?
      end

      # Returns true if the current request is targeting code search.
      def code_search?(request)
        CODE_SEARCH_TYPE == search_type(request)
      end
    end
  end
end
