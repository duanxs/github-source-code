# rubocop:disable Style/FrozenStringLiteralComment

require "digest/md5"

module Api
  module Limiters
    class TwirpAuthenticationFingerprintByPath < GitHub::Limiters::MemcachedWindow
      include GitHub::AreasOfResponsibility
      areas_of_responsibility :ecosystem_api

      UNKNOWN_CLIENT_NAME = "unknown"

      # Public: Extract the Twirp path from the incoming request.
      #
      # request - Rack::Request instance.
      #
      # Returns String if Twirp path is present, false otherwise.
      def self.twirp_path(request)
        if request.path_info =~ ::Api::Internal::Twirp::PATH_REGEX
          request.path_info
        end
      end

      # Public: Determine if the incoming request is for a Twirp endpoint,
      # which always use POST requests.
      #
      # request - Rack::Request instance.
      #
      # Returns true if Twirp request, false otherwise.
      def self.twirp_request?(request)
        return false if request.request_method != "POST"
        !twirp_path(request).nil?
      end

      def initialize(max:)
        super("twirp-authentication-fingerprint-by-path", limit: max)
      end

      def start(request)
        return OK unless self.class.twirp_request?(request)
        super(request)
      end

      def record_finish(request)
        return OK unless self.class.twirp_request?(request)
        increment_counter(request)
      end

      # If the request was canceled we don't want to cost the request.
      def cancel(request)
        OK
      end

      protected

      # Protected: The key is composed of the client name, authentication fingerprint,
      # and the hashed path.
      def key(request)
        hashed_path = Digest::MD5.hexdigest(request.path_info)
        client_name = client_name_from_header(request) || UNKNOWN_CLIENT_NAME
        "#{client_name}:#{fingerprint(request)}:#{hashed_path}"
      end

      def fingerprint(request)
        Api::Middleware::RequestAuthenticationFingerprint.get(request.env).to_s
      end

      private

      # Private: Get the service name by verifying the Request-HMAC header.
      def client_name_from_header(request)
        received_hmac = request.env[Api::Internal::REQUEST_HMAC_HEADER]
        if received_hmac.present?
          hmac_status, client_key = Api::Internal::Twirp.verify_request_hmac(received_hmac)
          if hmac_status == :success
            GitHub.api_internal_twirp_hmac_settings.fetch(client_key, UNKNOWN_CLIENT_NAME)
          end
        end
      end
    end
  end
end
