# rubocop:disable Style/FrozenStringLiteralComment

require "digest/md5"

module Api
  module Limiters
    class AuthenticationFingerprintByPath < GitHub::Limiters::MemcachedWindow

      CUSTOM_COSTS = {
        # /app/installations/:installation_id/access_tokens
        # This endpoint is heavily itilized by GitHub Apps to generate tokens
        # hourly for installations.
        # See: https://github.com/github/partner-engineering/issues/289#issuecomment-503771913
        %r{\A/app/installations/\d+/access_tokens(/|\z)} => 2,

        # /applications/:client_id/token
        # This endpoint checks a token's validity, it does
        # incure more CPU time than any other GET.
        #
        # See https://github.com/github/ecosystem-apps/issues/946 for more details.
        %r{\A/applications/\w+/token\z} => 1,
      }

      include GitHub::Middleware::Constants

      def initialize(max:)
        super("authentication-fingerprint-by-path", limit: max)
      end

      def record_start(request)
        # If the path is for GraphQL or Twirp we want to not cost the request.
        graphql_path = Api::Limiters::GraphQLAuthenticationFingerprint.graphql_path(request)
        twirp_path = Api::Limiters::TwirpAuthenticationFingerprintByPath.twirp_path(request)
        if graphql_path.present? || twirp_path.present?
          OK
        else
          increment_counter(request)
        end
      end

      protected

      def key(request)
        authentication_fingerprint =
          Api::Middleware::RequestAuthenticationFingerprint.get(request.env).to_s

        hashed_path = Digest::MD5.hexdigest(request.path_info)
        "#{authentication_fingerprint}:#{hashed_path}"
      end

      def custom_cost_for(path)
        CUSTOM_COSTS.detect { |path_regex, _| path =~ path_regex }
      end

      # Mutating methods often incur more CPU time, so let's charge more for
      # those.
      def cost(request)
        if custom_cost = custom_cost_for(request.path_info)
          return custom_cost[1]
        end

        case request.request_method
        when "GET", "HEAD", "OPTIONS"
          1
        else # POST, PATCH, PUT, DELETE
          5
        end
      end
    end
  end
end
