# frozen_string_literal: true

class Api::GitignoreTemplates < Api::App
  areas_of_responsibility :api

  get "/gitignore/templates" do
    @route_owner = "@github/ecosystem-api"
    @documentation_url = "/rest/reference/gitignore#get-all-gitignore-templates"
    control_access :public_site_information, resource: Platform::PublicResource.new, allow_integrations: true, allow_user_via_integration: true

    deliver_raw(Gitignore.templates, status: 200)
  end

  get "/gitignore/templates/:key" do
    @route_owner = "@github/ecosystem-api"
    @documentation_url = "/rest/reference/gitignore#get-a-gitignore-template"
    control_access :public_site_information, resource: Platform::PublicResource.new, allow_integrations: true, allow_user_via_integration: true

    name = params[:key]
    template = Gitignore.template(name)
    deliver_error!(404, documentation_url: "/v3/gitignore") unless template.present?

    if medias.api_param?(:raw)
      deliver_raw template, content_type: "#{medias}; charset=utf-8"
    else
      deliver_raw(name: name, source: template)
    end
  end
end
