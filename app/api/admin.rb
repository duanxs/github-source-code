# frozen_string_literal: true

class Api::Admin < Api::App
  areas_of_responsibility :enterprise_only, :stafftools, :api

  before do
    @accepted_scopes = :site_admin if GitHub.require_site_admin_scope?
    control_access :admin_api, challenge: true, allow_integrations: false, allow_user_via_integration: false
  end
end
