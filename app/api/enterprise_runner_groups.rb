# frozen_string_literal: true

class Api::EnterpriseRunnerGroups < Api::App
  include Api::App::GrpcHelpers
  map_to_service :actions_runners

  # List runner groups
  get "/enterprises/:enterprise_id/actions/runner-groups" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    deliver_error! 404 unless enterprise_runners_enabled?

    enterprise = find_enterprise!

    control_access :read_enterprise_self_hosted_runners,
      resource: enterprise,
      user: current_user,
      forbid: false,
      allow_integrations: false,
      allow_user_via_integration: false,
      enforce_oauth_app_policy: false

    ensure_tenant!(enterprise)

    resp = rescue_from_grpc_errors("RunnerGroups") do
      GitHub::LaunchClient::RunnerGroups.list_groups(owner: enterprise)
    end

    validate_listing!(resp&.runner_groups)

    # We map to an Array so pagination works
    runner_groups = paginate_rel(resp&.runner_groups.map { |runner_group| runner_group })
    deliver :actions_enterprise_runner_groups_hash, {
      runner_groups: runner_groups,
      total_count: runner_groups.total_entries,
      enterprise: enterprise,
    }
  end

  # Get a single runner group
  get "/enterprises/:enterprise_id/actions/runner-groups/:runner_group_id" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    deliver_error! 404 unless enterprise_runners_enabled?

    enterprise = find_enterprise!

    control_access :read_enterprise_self_hosted_runners,
      resource: enterprise,
      user: current_user,
      forbid: false,
      allow_integrations: false,
      allow_user_via_integration: false,
      enforce_oauth_app_policy: false

    ensure_tenant!(enterprise)

    resp = rescue_from_grpc_errors("RunnerGroups") do
      GitHub::LaunchClient::RunnerGroups.get_group(owner: enterprise, group_id: params[:runner_group_id].to_i)
    end

    runner_group = resp&.runner_group
    deliver_error! 404 unless runner_group

    deliver :actions_enterprise_runner_group_hash, {
      runner_group: runner_group,
      enterprise: enterprise,
    }
  end

  # Create a runner group
  post "/enterprises/:enterprise_id/actions/runner-groups" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    deliver_error! 404 unless enterprise_runners_enabled?

    enterprise = find_enterprise!

    control_access :write_enterprise_self_hosted_runners,
      resource: enterprise,
      user: current_user,
      forbid: false,
      allow_integrations: false,
      allow_user_via_integration: false,
      enforce_oauth_app_policy: false

    ensure_tenant!(enterprise)

    data = receive_with_schema("enterprise-runner-group", "create-group")
    name = data["name"]
    runner_ids = data["runners"]
    visibility = GitHub::LaunchClient::RunnerGroups::FROM_VISIBILITY_MAP[data["visibility"]]
    selected_organization_ids = data["selected_organization_ids"]

    # Filter organizations within the enterprise
    enterprise_org_ids = enterprise.organizations.where(id: selected_organization_ids).map(&:global_relay_id)

    result = rescue_from_grpc_errors("RunnerGroups") do
      GitHub::LaunchClient::RunnerGroups.create_group(
        actor: current_user,
        owner: enterprise,
        name: name,
        runner_ids: runner_ids,
        visibility: visibility,
        selected_targets: enterprise_org_ids
      )
    end

    runner_group = result&.runner_group
    validate_result!(runner_group)

    deliver :actions_enterprise_runner_group_hash, {
      runner_group: runner_group,
      enterprise: enterprise,
    }, status: 201
  end

  # Delete a runner group
  delete "/enterprises/:enterprise_id/actions/runner-groups/:runner_group_id" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    deliver_error! 404 unless enterprise_runners_enabled?

    enterprise = find_enterprise!

    control_access :write_enterprise_self_hosted_runners,
      resource: enterprise,
      user: current_user,
      forbid: false,
      allow_integrations: false,
      allow_user_via_integration: false,
      enforce_oauth_app_policy: false

    ensure_tenant!(enterprise)

    receive_with_schema("enterprise-runner-group", "delete-group")

    result = rescue_from_grpc_errors("RunnerGroups") do
      GitHub::LaunchClient::RunnerGroups.delete_group(
        actor: current_user,
        owner: enterprise,
        group_id: params[:runner_group_id].to_i,
      )
    end

    deliver_error! 404 unless result
    deliver_empty status: 204
  end

  # Update a runner group
  patch "/enterprises/:enterprise_id/actions/runner-groups/:runner_group_id" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    deliver_error! 404 unless enterprise_runners_enabled?

    enterprise = find_enterprise!

    control_access :write_enterprise_self_hosted_runners,
      resource: enterprise,
      user: current_user,
      forbid: false,
      allow_integrations: false,
      allow_user_via_integration: false,
      enforce_oauth_app_policy: false

    ensure_tenant!(enterprise)

    data = receive_with_schema("enterprise-runner-group", "update-group")
    name = data["name"]
    visibility = GitHub::LaunchClient::RunnerGroups::FROM_VISIBILITY_MAP[data["visibility"]]

    resp = rescue_from_grpc_errors("RunnerGroups") do
      GitHub::LaunchClient::RunnerGroups.update_group(
        actor: current_user,
        owner: enterprise,
        group_id: params[:runner_group_id].to_i,
        name: name,
        visibility: visibility,
      )
    end

    runner_group = resp&.runner_group
    deliver_error! 404 unless runner_group

    deliver :actions_enterprise_runner_group_hash, {
      runner_group: runner_group,
      enterprise: enterprise,
    }
  end

  # Get runners in a runner group
  get "/enterprises/:enterprise_id/actions/runner-groups/:runner_group_id/runners" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    deliver_error! 404 unless enterprise_runners_enabled?

    enterprise = find_enterprise!

    control_access :read_enterprise_self_hosted_runners,
      resource: enterprise,
      user: current_user,
      forbid: false,
      allow_integrations: false,
      allow_user_via_integration: false,
      enforce_oauth_app_policy: false

    ensure_tenant!(enterprise)

    resp = rescue_from_grpc_errors("SelfHostedRunners") do
      GitHub::LaunchClient::RunnerGroups.get_group(
        owner: enterprise,
        group_id: params[:runner_group_id].to_i,
        include_runners: true,
      )
    end

    validate_listing!(resp&.runner_group&.runners)

    # We map to an Array so pagination works
    runners = paginate_rel(resp&.runner_group&.runners&.map { |runner| runner })
    deliver :actions_runners_hash, { runners: runners, total_count: runners.total_entries }
  end

  # Update runners in a runner group
  put "/enterprises/:enterprise_id/actions/runner-groups/:runner_group_id/runners" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    deliver_error! 404 unless enterprise_runners_enabled?

    enterprise = find_enterprise!

    control_access :write_enterprise_self_hosted_runners,
      resource: enterprise,
      user: current_user,
      forbid: false,
      allow_integrations: false,
      allow_user_via_integration: false,
      enforce_oauth_app_policy: false

    ensure_tenant!(enterprise)

    data = receive_with_schema("enterprise-runner-group", "update-runners")
    runner_ids = data["runners"]

    result = rescue_from_grpc_errors("SelfHostedRunners") do
      GitHub::LaunchClient::RunnerGroups.update_runners(
        actor: current_user,
        owner: enterprise,
        group_id: params[:runner_group_id].to_i,
        runner_ids: runner_ids,
      )
    end

    deliver_error! 404 unless result
    deliver_empty status: 204
  end

  # Add a runner to a runner group
  put "/enterprises/:enterprise_id/actions/runner-groups/:runner_group_id/runners/:runner_id" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    deliver_error! 404 unless enterprise_runners_enabled?

    enterprise = find_enterprise!

    control_access :write_enterprise_self_hosted_runners,
      resource: enterprise,
      user: current_user,
      forbid: false,
      allow_integrations: false,
      allow_user_via_integration: false,
      enforce_oauth_app_policy: false

    ensure_tenant!(enterprise)

    receive_with_schema("enterprise-runner-group", "add-runner")

    result = GrpcHelper.rescue_from_grpc_errors("SelfHostedRunners") do
      GitHub::LaunchClient::RunnerGroups.add_runners(
        actor: current_user,
        owner: enterprise,
        group_id: params[:runner_group_id].to_i,
        runner_ids: [params[:runner_id].to_i],
      )
    end

    deliver_error! 404 unless result
    deliver_empty status: 204
  end

  # Remove a runner from a runner group
  delete "/enterprises/:enterprise_id/actions/runner-groups/:runner_group_id/runners/:runner_id" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    deliver_error! 404 unless enterprise_runners_enabled?

    enterprise = find_enterprise!

    control_access :write_enterprise_self_hosted_runners,
      resource: enterprise,
      user: current_user,
      forbid: false,
      allow_integrations: false,
      allow_user_via_integration: false,
      enforce_oauth_app_policy: false

    ensure_tenant!(enterprise)

    receive_with_schema("enterprise-runner-group", "remove-runner")

    result = rescue_from_grpc_errors("SelfHostedRunners") do
      GitHub::LaunchClient::RunnerGroups.remove_runner(
        actor: current_user,
        owner: enterprise,
        group_id: params[:runner_group_id].to_i,
        runner_id: params[:runner_id].to_i,
      )
    end

    deliver_error! 404 unless result
    deliver_empty status: 204
  end

  # List the organizations that have access to a runner group
  get "/enterprises/:enterprise_id/actions/runner-groups/:runner_group_id/organizations" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    deliver_error! 404 unless enterprise_runners_enabled?

    enterprise = find_enterprise!

    control_access :read_enterprise_self_hosted_runners,
      resource: enterprise,
      user: current_user,
      forbid: false,
      allow_integrations: false,
      allow_user_via_integration: false,
      enforce_oauth_app_policy: false

    ensure_tenant!(enterprise)

    resp = rescue_from_grpc_errors("RunnerGroups") do
      GitHub::LaunchClient::RunnerGroups.get_group(owner: enterprise, group_id: params[:runner_group_id].to_i)
    end

    runner_group = resp&.runner_group
    deliver_error! 404 unless runner_group

    organization_targets = runner_group.selected_targets.map { |identity| identity.global_id }.to_set
    organization_ids = organization_targets.map { |global_id| Platform::Helpers::NodeIdentification.from_global_id(global_id)[1] }
    filtered_organizations = enterprise.organizations.where(id: organization_ids).order(:name)
    paginated_organizations = paginate_rel(filtered_organizations)

    deliver :actions_runner_group_organizations_hash, { organizations: paginated_organizations, total_count: filtered_organizations.size }
  end

  # Update organizations that have access to a runner group
  put "/enterprises/:enterprise_id/actions/runner-groups/:runner_group_id/organizations" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    deliver_error! 404 unless enterprise_runners_enabled?

    enterprise = find_enterprise!

    control_access :write_enterprise_self_hosted_runners,
      resource: enterprise,
      user: current_user,
      forbid: false,
      allow_integrations: false,
      allow_user_via_integration: false,
      enforce_oauth_app_policy: false

    ensure_tenant!(enterprise)

    data = receive_with_schema("enterprise-runner-group", "set-organizations")

    selected_organization_ids = data["selected_organization_ids"]

    # Filter organizations within the enterprise
    enterprise_org_ids = enterprise.organizations.where(id: selected_organization_ids).map(&:global_relay_id)

    result = rescue_from_grpc_errors("RunnerGroups") do
      GitHub::LaunchClient::RunnerGroups.update_targets(
        owner: enterprise,
        group_id: params[:runner_group_id].to_i,
        selected_targets: enterprise_org_ids)
    end

    deliver_error! 404 unless result
    deliver_empty status: 204
  end

  # Add an organization to a runner group
  put "/enterprises/:enterprise_id/actions/runner-groups/:runner_group_id/organizations/:organization_id" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    deliver_error! 404 unless enterprise_runners_enabled?

    enterprise = find_enterprise!

    control_access :write_enterprise_self_hosted_runners,
      resource: enterprise,
      user: current_user,
      forbid: false,
      allow_integrations: false,
      allow_user_via_integration: false,
      enforce_oauth_app_policy: false

    ensure_tenant!(enterprise)

    receive_with_schema("enterprise-runner-group", "add-organization")

    org = find_org_by_parameter
    deliver_error! 404 unless org
    deliver_error! 422 unless org.business&.id == enterprise.id

    result = rescue_from_grpc_errors("RunnerGroups") do
      GitHub::LaunchClient::RunnerGroups.add_target(
        owner: enterprise,
        group_id: params[:runner_group_id].to_i,
        selected_target: org.global_relay_id)
    end

    deliver_error! 404 unless result
    deliver_empty status: 204
  end

  # Remove an organization from a runner group
  delete "/enterprises/:enterprise_id/actions/runner-groups/:runner_group_id/organizations/:organization_id" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    deliver_error! 404 unless enterprise_runners_enabled?

    enterprise = find_enterprise!

    control_access :write_enterprise_self_hosted_runners,
      resource: enterprise,
      user: current_user,
      forbid: false,
      allow_integrations: false,
      allow_user_via_integration: false,
      enforce_oauth_app_policy: false

    ensure_tenant!(enterprise)

    receive_with_schema("enterprise-runner-group", "remove-organization")

    org = find_org_by_parameter
    deliver_error! 404 unless org
    deliver_error! 422 unless org.business&.id == enterprise.id

    result = rescue_from_grpc_errors("RunnerGroups") do
      GitHub::LaunchClient::RunnerGroups.remove_target(
        owner: enterprise,
        group_id: params[:runner_group_id].to_i,
        selected_target: org.global_relay_id)
    end

    deliver_error! 404 unless result
    deliver_empty status: 204
  end

  private

  def enterprise_runners_enabled?
    GitHub.actions_enabled? && (
      GitHub.flipper[:actions_enterprise_runners].enabled?(current_user) ||
      GitHub.enterprise?
    )
  end

  def validate_listing!(result)
    unless result
      Failbot.report(StandardError.new("no response from list"), launch_runnergroups: GitHub.launch_runnergroups)
      deliver_error!(503, message: "Runner Groups unavailable. Please try again later.")
    end
  end

  def validate_result!(result)
    unless result
      Failbot.report(StandardError.new("no response from create runner group"), launch_runnergroups: GitHub.launch_runnergroups)
      deliver_error!(503, message: "Runner Groups unavailable. Please try again later.")
    end
  end

  # The standard find_org! resolves the current user from the environment first
  # This will resolve to the enterprise, resulting in find_org_by_id returning nil
  def find_org_by_parameter
    if (id = params[:organization_id])
      Organization.find_by_id(id.to_i)
    else
      nil
    end
  end

  def ensure_tenant!(enterprise)
    result = rescue_from_grpc_errors("EnterpriseTenant") do
      GitHub::LaunchClient::Deployer.setup_tenant(enterprise)
    end
    deliver_error! 500 unless result
  end
end
