# frozen_string_literal: true

class Api::OrganizationRunners < Api::App
  map_to_service :actions_runners

  # Create a registration token for org-level runners
  post "/organizations/:organization_id/actions/runners/registration-token" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#create-a-registration-token-for-an-organization"
    deliver_error! 404 unless org_runners_enabled?

    org = find_org!
    deliver_error! 404 unless can_use_org_runners?(org)

    control_access :write_org_self_hosted_runners,
      resource: org,
      user: current_user,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    ensure_tenant!(org)

    receive_with_schema("runner", "create-org-runner-registration-token")

    expires_at = 1.hour.from_now
    scope = org.runner_creation_token_scope
    token = current_user.signed_auth_token(scope: scope, expires: expires_at)
    deliver_raw({ token: token, expires_at: expires_at }, status: 201)
  rescue Organization::ActionsSetupError => e
    deliver_error!(400, message: e.message)
  end

  # Delete an org-level runner
  delete "/organizations/:organization_id/actions/runners/:runner_id" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#delete-a-self-hosted-runner-from-an-organization"
    deliver_error! 404 unless org_runners_enabled?

    org = find_org!
    deliver_error! 404 unless can_use_org_runners?(org)

    control_access :write_org_self_hosted_runners,
      resource: org,
      user: current_user,
          forbid: true,
          allow_integrations: true,
          allow_user_via_integration: true,
          enforce_oauth_app_policy: true

    ensure_tenant!(org)

    receive_with_schema("runner", "delete-org-runner")

    resp = GrpcHelper.rescue_from_grpc_errors("SelfHostedRunners") do
      GitHub::LaunchClient::SelfHostedRunners.delete_runner(org, params[:runner_id].to_i, actor: current_user)
    end

    if resp.call_succeeded?
      deliver_empty status: 204
    else
      deliver_error! 500, message: "Failed to delete the specified runner, it may be actively running a job"
    end
  end

  # Create a remove token for org-level runners
  post "/organizations/:organization_id/actions/runners/remove-token" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#create-a-remove-token-for-an-organization"
    deliver_error! 404 unless org_runners_enabled?

    org = find_org!
    deliver_error! 404 unless can_use_org_runners?(org)

    control_access :write_org_self_hosted_runners,
      resource: org,
      user: current_user,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    ensure_tenant!(org)

    receive_with_schema("runner", "create-org-runner-remove-token")

    expires_at = 1.hour.from_now
    scope = org.runner_deletion_token_scope
    token = current_user.signed_auth_token(scope: scope, expires: expires_at)
    deliver_raw({ token: token, expires_at: expires_at }, status: 201)
  end

  # List runner downloads
  get "/organizations/:organization_id/actions/runners/downloads" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#list-runner-applications-for-an-organization"
    deliver_error! 404 unless org_runners_enabled?

    org = find_org!
    deliver_error! 404 unless can_use_org_runners?(org)

    control_access :read_org_self_hosted_runners,
      resource: org,
      user: current_user,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    ensure_tenant!(org)

    resp = GrpcHelper.rescue_from_grpc_errors("SelfHostedRunners") do
      GitHub::LaunchClient::SelfHostedRunners.list_downloads(org)
    end

    downloads = resp&.value&.downloads || []
    downloads_hashes = downloads.map do |download|
      download_hash(download)
    end
    deliver_raw(downloads_hashes)
  end

  # Get all runners
  get "/organizations/:organization_id/actions/runners" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#list-self-hosted-runners-for-an-organization"
    deliver_error! 404 unless org_runners_enabled?

    org = find_org!
    deliver_error! 404 unless can_use_org_runners?(org)

    control_access :read_org_self_hosted_runners,
      resource: org,
      user: current_user,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    ensure_tenant!(org)

    resp = GrpcHelper.rescue_from_grpc_errors("SelfHostedRunners") do
      GitHub::LaunchClient::SelfHostedRunners.list_runners(org)
    end

    validate_listing!(resp&.value&.runners)

    # We map to an Array so pagination works
    runners = paginate_rel(resp&.value&.runners.map { |runner| runner })
    deliver :actions_runners_hash, { runners: runners, total_count: runners.total_entries, current_user: current_user }
  end

  # Get a runner
  get "/organizations/:organization_id/actions/runners/:runner_id" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#get-a-self-hosted-runner-for-an-organization"
    deliver_error! 404 unless org_runners_enabled?

    org = find_org!
    deliver_error! 404 unless can_use_org_runners?(org)

    control_access :read_org_self_hosted_runners,
      resource: org,
      user: current_user,
      forbid: true,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    ensure_tenant!(org)

    resp = GrpcHelper.rescue_from_grpc_errors("SelfHostedRunners") do
      GitHub::LaunchClient::SelfHostedRunners.get_runner(org, params[:runner_id].to_i)
    end

    runner = resp&.value&.runner
    deliver_error! 404 unless runner

    deliver :actions_runner_hash, runner, { current_user: current_user }
  end

  private

  def download_hash(download)
    { os: download.os, architecture: download.architecture, download_url: download.download_url, filename: download.filename }
  end

  def org_runners_enabled?
    return GitHub.actions_enabled?
  end

  def can_use_org_runners?(organization)
    Billing::ActionsPermission.new(organization).status[:error][:reason] != "PLAN_INELIGIBLE"
  end

  def validate_listing!(result)
    unless result
      Failbot.report(StandardError.new("no response from list"), launch_selfhostedrunners: GitHub.launch_selfhostedrunners)
      deliver_error!(503, message: "Runners unavailable. Please try again later.")
    end
  end

  def ensure_tenant!(org)
    result = GrpcHelper.rescue_from_grpc_errors("OrgTenant") do
      GitHub::LaunchClient::Deployer.setup_tenant(org)
    end
    deliver_error! 500 unless result
  end
end
