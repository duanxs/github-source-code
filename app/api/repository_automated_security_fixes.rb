# frozen_string_literal: true

class Api::RepositoryAutomatedSecurityFixes < Api::App
  areas_of_responsibility :dependabot, :api

  # enable automated security fixes for a repo
  put "/repositories/:repository_id/automated-security-fixes" do
    @route_owner = "@github/dsp-dependabot"
    @documentation_url = "/rest/reference/repos#enable-automated-security-fixes"
    @accepted_scopes = %(repo)
    require_preview(:automated_security_fixes)
    deliver_error! 404 unless GitHub.dependabot_enabled?

    repo = find_repo!

    control_access(:admin_automated_security_fixes,
      user: current_user,
      repo: repo,
      allow_integrations: true,
      allow_user_via_integration: true)

    ensure_vulnerability_alerts_enabled(repo: repo)

    receive_with_schema("repository-automated-security-fixes-setting", "enable")

    repo.enable_vulnerability_updates(actor: current_user)

    if repo.vulnerability_updates_enabled?
      deliver_empty(status: 204)
    else
      deliver_error!(422, message: "Failed to enable automated security fixes.")
    end
  end

  # disable automated security fixes for a repo
  delete "/repositories/:repository_id/automated-security-fixes" do
    @route_owner = "@github/dsp-dependabot"
    @documentation_url = "/rest/reference/repos#disable-automated-security-fixes"
    @accepted_scopes = %(repo)
    require_preview(:automated_security_fixes)
    deliver_error! 404 unless GitHub.dependabot_enabled?

    repo = find_repo!

    control_access(:admin_automated_security_fixes,
      user: current_user,
      repo: repo,
      allow_integrations: true,
      allow_user_via_integration: true)

    ensure_vulnerability_alerts_enabled(repo: repo)

    receive_with_schema("repository-automated-security-fixes-setting", "disable")

    repo.disable_vulnerability_updates(actor: current_user)

    if repo.vulnerability_updates_disabled?
      deliver_empty(status: 204)
    else
      deliver_error!(422, message: "Failed to disable automated security fixes.")
    end
  end

  private

  def ensure_vulnerability_alerts_enabled(repo:)
    unless repo.vulnerability_alerts_enabled?
      deliver_error!(422, message: "Vulnerability alerts must be enabled to configure automated security fixes.")
    end
  end
end
