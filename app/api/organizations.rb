# rubocop:disable Style/FrozenStringLiteralComment

class Api::Organizations < Api::App
  areas_of_responsibility :orgs, :api

  # list the logged in user's organizations (public and private)
  get "/user/orgs" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/orgs#list-organizations-for-the-authenticated-user"
    @accepted_scopes = %w(admin:org read:org repo user write:org)

    set_forbidden_message "You need at least read:org scope or user scope to list your organizations."
    control_access :list_current_user_accessible_orgs,
      challenge: true,
      resource: current_user,
      allow_integrations: false,
      allow_user_via_integration: true,
      installation_required: false

    orgs = paginate_rel(accessible_orgs)
    GitHub::PrefillAssociations.for_profiles(orgs)

    deliver :organization_hash, orgs
  end

  # list the logged in user's organization memberships (public and private)
  get "/user/memberships/orgs" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/orgs#list-organization-memberships-for-the-authenticated-user"
    set_forbidden_message "You do not have access to organization memberships."
    control_access :get_current_user_org_membership,
      resource: current_user,
      member: current_user,
      challenge: true,
      allow_integrations: false,
      allow_user_via_integration: true,
      installation_required: false

    state = params[:state] && params[:state].strip

    if state.present? && %w(active pending).exclude?(state)
      deliver_error! 422,
        message: "If you pass a state to this endpoint, it must be either 'pending' or 'active'.",
        documentation_url: "/v3/orgs/#list-your-organization-memberships"
    end

    include_active  = state.blank? || state == "active"
    include_pending = state.blank? || state == "pending"

    org_ids = []
    org_ids |= current_user.organizations.pluck(:id)        if include_active
    org_ids |= current_user.invited_organizations.map(&:id) if include_pending

    if org_ids.any? && current_user.using_auth_via_integration?
      installation_ids = Authorization.service.actor_ids_with_granular_permissions_on(
        actor_type:   "IntegrationInstallation",
        subject_type: Organization,
        subject_ids:  org_ids,
        permissions:  [:members],
      )

      org_ids = current_integration.installations.where(id: installation_ids).pluck(:target_id)
    end

    scope = Organization.where(id: org_ids)

    if requestor_governed_by_oauth_application_policy?
      scope = scope.oauth_app_policy_met_by(current_app_via_oauth)
    end

    orgs = paginate_rel(scope)
    GitHub::PrefillAssociations.for_profiles(orgs)
    GitHub::PrefillAssociations.for_user_repository_counts(orgs)

    deliver :org_membership_hash, orgs, user: current_user
  end

  # get the logged in user's membership with an org
  get "/user/memberships/organizations/:organization_id" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/orgs#get-an-organization-membership-for-the-authenticated-user"
    org = find_org!

    set_forbidden_message "You do not have access to this organization membership."
    control_access :get_current_user_org_membership,
      member: current_user,
      resource: org,
      challenge: true,
      allow_integrations: false,
      allow_user_via_integration: true

    if org.membership_state_of(current_user) == :inactive
      deliver_error 404
    else
      deliver :org_membership_hash, org, user: current_user
    end
  end

  # update the logged in user's membership with an org
  patch "/user/memberships/organizations/:organization_id" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/orgs#update-an-organization-membership-for-the-authenticated-user"
    org = find_org!

    set_forbidden_message "You do not have access to this organization membership."
    control_access :update_current_user_org_membership,
      resource: current_user,
      organization: org,
      challenge: true,
      allow_integrations: false,
      allow_user_via_integration: true

    data = receive(Hash)

    if org.membership_state_of(current_user) == :inactive
      deliver_error!(404)
    end

    if data["state"] != "active"
      deliver_error!(
        422,
        message: "You can only update an organization membership's state to 'active'.",
        documentation_url: @documentation_url,
      )
    end

    # Introducing strict validation of the organization-membership.update
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # TODO: replace `receive` with `receive_with_schema`
    # see: https://github.com/github/ecosystem-api/issues/1555
    _ = receive_with_schema("organization-membership", "update", skip_validation: true)

    invitation = org.pending_invitation_for(current_user)

    if invitation.present? && !org.two_factor_requirement_met_by?(current_user)
      deliver_error!(
        403,
        message: "The #{org.safe_profile_name} organization requires all members to have two-factor authentication enabled.",
        documentation_url: @documentation_url,
      )
    end

    result = invitation.accept(acceptor: current_user) if invitation.present?
    if result && result.error?
      deliver_error!(
        422,
        message: result.error,
        documentation_url: @documentation_url,
      )
    end

    deliver :org_membership_hash, org, user: current_user
  end

  # list a user's public organizations
  get "/user/:user_id/orgs" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/orgs#list-organizations-for-a-user"

    control_access :public_site_information,
      resource: Platform::PublicResource.new,
      allow_integrations: true,
      allow_user_via_integration: true

    user = find_user!
    @accepted_scopes = []
    orgs = paginate_rel(user.public_organizations)
    GitHub::PrefillAssociations.for_profiles(orgs)

    deliver :organization_hash, orgs
  end

  # get an organization
  get "/organizations/:organization_id" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/orgs#get-an-organization"
    @accepted_scopes   = %w(admin:org read:org repo user write:org)

    control_access :apps_audited,
      resource: Platform::PublicResource.new,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: false

    org = find_org!

    key = if access_allowed?(:view_org_settings, resource: org, allow_integrations: true, allow_user_via_integration: true)
      :owner_private
    elsif access_allowed?(:get_org_private, resource: org, allow_integrations: true, allow_user_via_integration: true)
      :private
    else
      :full
    end

    include_plan = access_allowed?(:get_org_plan, resource: org, allow_integrations: true, allow_user_via_integration: true)

    GitHub::PrefillAssociations.for_enterprise_configuration(org)
    GitHub::PrefillAssociations.for_user_repository_counts([org])
    deliver :organization_hash, org, key => true, :last_modified => calc_last_modified(org), :plan => include_plan
  end

  # List the installations the authorized user has access to.
  get "/organizations/:organization_id/installations" do
    @route_owner = "@github/ecosystem-apps"
    @documentation_url = "/rest/reference/orgs#list-app-installations-for-an-organization"
    @accepted_scopes   = %w(admin:org read:org write:org)

    control_access :view_org_installations,
      resource: org = find_org!,
      allow_integrations: true,
      allow_user_via_integration: true

    installations = IntegrationInstallation.third_party.with_target(org)
    installations = paginate_rel(installations)

    GitHub::PrefillAssociations.for_integration_installations(installations)

    deliver :integration_installations_hash, {
      integration_installations: installations,
      total_count: installations.count,
    }
  end

  # update an organization
  verbs :post, :patch, "/organizations/:organization_id" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/orgs#update-an-organization"
    control_access :update_org,
      resource: org = find_org!,
      allow_integrations: true,
      allow_user_via_integration: true

    data = receive_with_schema("organization", "update-legacy")

    keys = [
      :name,
      :email,
      :blog,
      :company,
      :location,
      :twitter_username,
    ]

    # Attributes prefixed with `profile_`.
    attributes = attr(data,
      *keys,
      { prefix: "profile_" },
    )

    # Attributes with no prefixes.
    attributes.update(attr(data,
      :has_organization_projects,
      :has_repository_projects,
    ))

    attributes.update(org_permission_extras_attributes(data))
    attributes.update attr(data, :billing_email, :description)
    if update_org_attributes(org, attributes)
      GitHub::PrefillAssociations.for_user_repository_counts([org])
      deliver :organization_hash, org,
        owner_private: true
    else
      deliver_error!(422, errors: org.errors)
    end
  end

  # List all organizations
  get "/organizations" do
    @route_owner = "@github/identity"
    @documentation_url = "/rest/reference/orgs#list-organizations"
    control_access :public_site_information,
      resource: Platform::PublicResource.new,
      allow_integrations: true,
      allow_user_via_integration: true

    options = { cursor: params[:since].to_i, page_size: per_page }
    options[:conditions] = "login != 'github-enterprise'" if GitHub.enterprise?

    orgs = Organization.dump_page(options)
    GitHub::PrefillAssociations.for_profiles(orgs)
    @links.add_dump_pagination(orgs.last)

    deliver :organization_hash, orgs
  end

  private

  # Internal: Organizations that are visible based on the authorization of the
  # current request.
  #
  # Returns an ActiveRecord::Relation, suitable for chaining on additional
  # scopes.
  def visible_orgs
    if access_allowed?(:list_orgs_for_user, target: current_user, allow_integrations: false, allow_user_via_integration: false)
      current_user.organizations
    else
      current_user.public_organizations
    end
  end

  # Internal: Organizations whose resources the current requestor is authorized
  # to access. This is especially useful for OAuth applications that want to
  # discover which of the user's organizations the app is authorized to access.
  # While an OAuth app can *see* all of a user's public organizations, it can
  # only *access* an organization if the org's OAuth app policy allows access
  # for the app.
  #
  # Returns an ActiveRecord::Relation, suitable for chaining on additional
  # scopes.
  def accessible_orgs
    scope = if requestor_governed_by_oauth_application_policy?
      current_user.organizations.oauth_app_policy_met_by(current_app_via_oauth)
    else
      current_user.organizations
    end

    return scope unless scope.exists?

    if current_user.using_auth_via_integration?
      @organization_installation_ids = Authorization.service.actor_ids_with_granular_permissions_on(
        actor_type:   "IntegrationInstallation",
        subject_type: Organization,
        subject_ids:  scope.pluck(:id),
        permissions:  [:members],
      )

      target_ids = IntegrationInstallation.where(id: @organization_installation_ids, integration_id: current_integration.id).pluck(:target_id)
      scope.where(id: target_ids)
    else
      scope
    end
  end

  # Internal: Returns a hash of org settings that have been remapped from the request
  def org_permission_extras_attributes(data)
    extras_params = {}
    if data.has_key?("default_repository_permission")
      #possible values "none", "read", "write", "admin". v3/schemas/organization.json
      new_permission = data["default_repository_permission"]
      extras_params["default_repository_permission"] = new_permission
    end
    if data.has_key?("members_can_create_repositories")
      extras_params["members_can_create_repositories"] = data["members_can_create_repositories"]
    end
    if data.has_key?("members_allowed_repository_creation_type")
      extras_params["members_allowed_repository_creation_type"] = data["members_allowed_repository_creation_type"]
    end
    if data.has_key?("members_can_create_public_repositories")
      extras_params["members_can_create_public_repositories"] = data["members_can_create_public_repositories"]
    end
    if data.has_key?("members_can_create_private_repositories")
      extras_params["members_can_create_private_repositories"] = data["members_can_create_private_repositories"]
    end
    if data.has_key?("members_can_create_internal_repositories")
      extras_params["members_can_create_internal_repositories"] = data["members_can_create_internal_repositories"]
    end
    extras_params
  end

  # Internal: Updates an organizations attributes.
  # If the attribute default_repository_permission is included it will queue an async job
  # to update the permission settings for every org owned repo.
  #
  # Returns true or false
  def update_org_attributes(org, attributes)
    configurable_settings = {}
    if attributes.key?("default_repository_permission")
      deliver_updating_default_repo_permission_error! if org.updating_default_repository_permission?
      configurable_settings[:default_repository_permission] = attributes.delete("default_repository_permission")
    end
    if attributes.key?("members_can_create_repositories")
      configurable_settings[:can_create_repos] = attributes.delete("members_can_create_repositories")
    end
    if attributes.key?("members_allowed_repository_creation_type")
      require_preview(:repo_creation_permissions)

      if attributes["members_allowed_repository_creation_type"] == "private"
        unless org.can_restrict_only_public_repo_creation?
          deliver_error!(422, errors: '"private" is not a valid value for members_allowed_repository_creation_type in this organization.')
        end
      end
      configurable_settings[:members_allowed_repository_creation_type] = attributes.delete("members_allowed_repository_creation_type")
    end
    ["members_can_create_public_repositories",
      "members_can_create_private_repositories",
      "members_can_create_internal_repositories",
    ].each do |attribute|
      if attributes.key?(attribute)
        require_preview(:repo_creation_permissions)
        deliver_error!(422, errors: "Private-only repository creation policy is not allowed for this organization.") if setting_invalid_private_only_creation_policy?(org, attributes)
        if !org.supports_internal_repositories? && attribute == "members_can_create_internal_repositories"
          deliver_error!(422, errors: "This organization does not support internal repositories.")
        end
        configurable_settings[attribute.to_sym] = attributes.delete(attribute)
      end
    end

    update_org_attributes_and_configurables(org, attributes, configurable_settings)
  end

  private def setting_invalid_private_only_creation_policy?(org, attributes)
    return false if org.can_restrict_only_public_repo_creation?
    new_public_allowed = attributes.key?("members_can_create_public_repositories") ?
      attributes["members_can_create_public_repositories"] :
      org.members_can_create_public_repositories?
    new_private_allowed = attributes.key?("members_can_create_private_repositories") ?
      attributes["members_can_create_private_repositories"] :
      org.members_can_create_private_repositories?
    return true if !new_public_allowed && new_private_allowed
    false
  end

  # Internal: update the attributes and settings stored via Configurable in a single
  # transaction. Supported Configurable settings:
  #  - default_repository_permission
  #  - can_create_repos
  #  - members_can_create_public_repositories
  #  - members_can_create_private_repositories
  #  - members_can_create_internal_repositories
  #
  # Returns: true or false, based on attributes update also. Configurable updates do
  # not return a success/failure, so we assume they succeed (unless an exception is raised)
  def update_org_attributes_and_configurables(org, attributes, configurable_settings)
    success = false
    Organization.transaction do
      if success = org.update(attributes)
        if configurable_settings.key?(:default_repository_permission)
          org.update_default_repository_permission(configurable_settings[:default_repository_permission],
                                                   actor: current_user)
        end
        if configurable_settings.key?(:can_create_repos)
          org.update_members_can_create_repositories(configurable_settings[:can_create_repos].to_s,
                                                     actor: current_user)
        end
        if configurable_settings.key?(:members_allowed_repository_creation_type)
          org.update_members_can_create_repositories(
            configurable_settings[:members_allowed_repository_creation_type].to_s,
            actor: current_user,
          )
        end
        granular_permissions = [:members_can_create_public_repositories, :members_can_create_private_repositories, :members_can_create_internal_repositories]
        if granular_permissions.any? { |k| configurable_settings.key?(k) }
          org.allow_members_can_create_repositories_with_visibilities(
            public_visibility: configurable_settings[:members_can_create_public_repositories],
            private_visibility: configurable_settings[:members_can_create_private_repositories],
            internal_visibility: configurable_settings[:members_can_create_internal_repositories],
            actor: current_user,
          )
        end
      end
    end

    success
  end

  def deliver_updating_default_repo_permission_error!
    deliver_error!(409, errors: <<~STR.tr("\n", " ")
        Currently updating default_repository_permission. Further changes will be ignored until update is complete.
        If you wish to update other settings, please remove default_repository_permission from your request.
      STR
    )
  end
end
