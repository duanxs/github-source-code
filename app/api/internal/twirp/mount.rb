# frozen_string_literal: true

module Api::Internal::Twirp::Mount

  # Public: Mount a service for a Twirp handler.
  #
  # handler_class - The Api::Internal::Twirp::Handler subclass.
  # opts - the optional specific installer classes to run, UserAccess and ClientAccess are defaults
  #
  # Returns nothing.
  def mount(handler_class, opts = {})
    handler = handler_class.new

    installers = opts.fetch(:install) {
      [
        ::Api::Internal::Twirp::UserAccess,
        ::Api::Internal::Twirp::ClientAccess
      ]
    }

    installers.each { |i| i.install(handler.service, handler) }

    before "/internal/twirp/#{handler.service.full_name}/*" do
      halt handler.service.call(env)
    end
  end

end
