# frozen_string_literal: true

require "github-proto-insights"

class Api::Internal::Twirp
  module Insights
    module V1
      class IssueEventsAPIHandler < Api::Internal::Twirp::Handler
        # Limited to the Insights client, identified by the HMAC key used to sign requests.
        # See Api::Internal::Twirp::ClientAccess and Api::Internal::Twirp::Handler#allow_client?
        ALLOWED_CLIENTS = %w(insights).freeze

        LIST_ISSUE_EVENTS_DEFAULT_LIMIT = 10000

        # Internal: Implementation of the FindCreatedIssueEventsByTimeRequest RPC, retrieving a list
        # of issue events for a given repository id, which are created in given time frame.
        #
        # req - The Twirp request as a GitHub::Proto::Insights::V1::FindCreatedIssueEventsByTimeRequest.
        # env - The Twirp environment as a Hash.
        #
        # Returns the Twirp response data as a Hash suitable for use as a
        # GitHub::Proto::Insights::V1::FindCreatedIssueEventsByTimeRequest, or a Twirp::Error.
        def find_created_issue_events_by_time(req, env)

          if req.repository_id.zero?
            return Twirp::Error.invalid_argument("must be non-empty", argument: "repository_id")
          end

          if req.from_time.nil?
            return Twirp::Error.invalid_argument("must be non-empty", argument: "from_time")
          end

          limit = req.limit.zero? ? LIST_ISSUE_EVENTS_DEFAULT_LIMIT : req.limit
          sort_order = req.is_desc_order ? "created_at desc" : "created_at asc"

          if req.to_time.nil?
            issue_events = IssueEvent.where(repository_id: req.repository_id)
                               .where("created_at >= ?", protobuf_timestamp_to_time(req.from_time))
                               .order(sort_order).limit(limit)
          else
            issue_events = IssueEvent.where(repository_id: req.repository_id, created_at: protobuf_timestamp_to_time(req.from_time)..protobuf_timestamp_to_time(req.to_time))
                               .order(sort_order).limit(limit)
          end

          {
              issue_events: build_issue_events_list(issue_events),
          }

        end

        # Internal: See Api::Internal::Twirp::Handler#require_user_token?
        def require_user_token?(env)
          false
        end

        # See Api::Internal::Twirp::Handler#allow_client?
        #
        # Limits access to clients listed in ALLOWED_CLIENTS.
        def allow_client?(client_name, env)
          ALLOWED_CLIENTS.include?(client_name)
        end

        private

        # Private: Builds the service instance for this handler.
        # See Api::Internal::Twirp::Handler#build_service
        def build_service
          GitHub::Proto::Insights::V1::IssueEventsAPIService.new(self)
        end

        # Private: Convert various types of times to Google Protobuf Timestamp
        def to_protobuf_timestamp_if_present(time)
          if time && time.is_a?(ActiveSupport::TimeWithZone)
            return Google::Protobuf::Timestamp.new(seconds: time.to_i, nanos: time.nsec)
          end
          if time && time.is_a?(Integer)
            return Google::Protobuf::Timestamp.new(seconds: time)
          end
        end

        def protobuf_timestamp_to_time(timestamp)
          timestamp.present? ? Time.at(timestamp.nanos * 10 ** -9 + timestamp.seconds) : nil
        end

        # Private: Convert an array of issue event objects to the shape Twirp responses expect.
        #
        # issue_events - The array of issue event objects.
        #
        # Returns an array of Hash objects with issue event data that matches the
        # Twirp definition.
        def build_issue_events_list(issue_events)
          issue_events.map do |event|
            {
              id: event.id,
              issue_id: event.issue_id,
              actor_id: event.actor_id,
              repository_id: event.repository_id,
              event: event.event,
              commit_id: event.commit_id,
              created_at: to_protobuf_timestamp_if_present(event.created_at),
              commit_repository_id: event.commit_repository_id,
              referencing_issue_id: event.referencing_issue_id,
              performed_by_integration_id: event.performed_by_integration_id,
            }.compact
          end
        end
      end
    end
  end
end
