# frozen_string_literal: true

require "github-proto-insights"

class Api::Internal::Twirp
  module Insights
    module V1
      class IssuesAPIHandler < Api::Internal::Twirp::Handler
        # Limited to the Insights client, identified by the HMAC key used to sign requests.
        # See Api::Internal::Twirp::ClientAccess and Api::Internal::Twirp::Handler#allow_client?
        ALLOWED_CLIENTS = %w(insights).freeze

        LIST_ISSUES_DEFAULT_LIMIT = 10000

        # Internal: Implementation of the FindUpdatedIssuesByTimeRequest RPC, retrieving a list
        # of issues for a given repository id, which are updated in given time frame.
        #
        # req - The Twirp request as a GitHub::Proto::Insights::V1::FindUpdatedIssuesByTimeRequest.
        # env - The Twirp environment as a Hash.
        #
        # Returns the Twirp response data as a Hash suitable for use as a
        # GitHub::Proto::Insights::V1::FindUpdatedIssuesByTimeRequest, or a Twirp::Error.
        def find_updated_issues_by_time(req, env)

          if req.repository_id.zero?
            return Twirp::Error.invalid_argument("must be non-empty", argument: "repository_id")
          end

          if req.from_time.nil?
            return Twirp::Error.invalid_argument("must be non-empty", argument: "from_time")
          end

          limit = req.limit.zero? ? LIST_ISSUES_DEFAULT_LIMIT : req.limit
          sort_order = req.is_desc_order ? "updated_at desc" : "updated_at asc"

          if req.to_time.nil?
            issues = Issue.where(repository_id: req.repository_id)
                         .where("updated_at >= ?", protobuf_timestamp_to_time(req.from_time))
                         .order(sort_order).limit(limit)
          else
            issues = Issue.where(repository_id: req.repository_id, updated_at: protobuf_timestamp_to_time(req.from_time)..protobuf_timestamp_to_time(req.to_time))
                         .order(:updated_at).limit(limit)
          end

          {
              issues: build_issues_list(issues),
          }
        end

        # Internal: See Api::Internal::Twirp::Handler#require_user_token?
        def require_user_token?(env)
          false
        end

        # See Api::Internal::Twirp::Handler#allow_client?
        #
        # Limits access to clients listed in ALLOWED_CLIENTS.
        def allow_client?(client_name, env)
          ALLOWED_CLIENTS.include?(client_name)
        end

        private

        # Private: Builds the service instance for this handler.
        # See Api::Internal::Twirp::Handler#build_service
        def build_service
          GitHub::Proto::Insights::V1::IssuesAPIService.new(self)
        end

        def to_protobuf_timestamp_if_present(time)
          if time && time.is_a?(ActiveSupport::TimeWithZone)
            return Google::Protobuf::Timestamp.new(seconds: time.to_i, nanos: time.nsec)
          end
          if time && time.is_a?(Integer)
            return Google::Protobuf::Timestamp.new(seconds: time)
          end
        end

        def protobuf_timestamp_to_time(timestamp)
          timestamp.present? ? Time.at(timestamp.nanos * 10 ** -9 + timestamp.seconds) : nil
        end

        # Private: Convert an array of issue objects to the shape Twirp responses expect.
        #
        # issues - The array of issue objects.
        #
        # Returns an array of Hash objects with issue data that matches the
        # Twirp definition.
        def build_issues_list(issues)
          issues.map do |issue|
            {
              id: issue.id,
              repository_id: issue.repository_id,
              user_id: issue.user_id,
              issue_comments_count: issue.issue_comments_count,
              number: issue.number,
              title: issue.title,
              state: issue.state,
              body: issue.body,
              created_at: to_protobuf_timestamp_if_present(issue.created_at),
              updated_at: to_protobuf_timestamp_if_present(issue.updated_at),
              closed_at: to_protobuf_timestamp_if_present(issue.closed_at),
              pull_request_id: issue.pull_request_id,
              milestone_id: issue.milestone_id,
              assignee_id: issue.assignee_id,
              contributed_at: to_protobuf_timestamp_if_present(issue.contributed_at_timestamp),
              user_hidden: issue.user_hidden,
              performed_by_integration_id: issue.performed_by_integration_id
            }.compact
          end
        end
      end
    end
  end
end
