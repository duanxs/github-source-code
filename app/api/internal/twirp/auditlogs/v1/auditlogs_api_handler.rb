# frozen_string_literal: true

require "github-proto-auditlogs"

class Api::Internal::Twirp < ::Api::Internal
  module Auditlogs
    module V1
      # Provides access to audit log data.
      class AuditlogsAPIHandler < Api::Internal::Twirp::Handler

        # Currently limited to these clients, identified by the HMAC key
        # used to sign requests.
        #
        # See Api::Internal::Twirp::ClientAccess and Api::Internal::Twirp::Handler#allow_client?
        # for more information.
        ALLOWED_CLIENTS = %w().freeze

        # Public: Implementation of the Auditlogs Twirp RPC.
        #
        # req - The Twirp request as a GitHub::Proto::Auditlogs::V1::CreateAuditLogEntryRequest.
        # env - The Twirp environment as a Hash.
        #
        # Returns the Twirp response data, suitable for use in a
        # GitHub::Proto::Auditlogs::V1::CreateAuditLogEntryResponse.
        def create_audit_log_entry(req, env)
          actor_id = id_argument(req.actor_id, env[:user_id])
          unless actor_id
            return Twirp::Error.invalid_argument(
              "must be non-empty if a user token is not provided", argument: "actor_id"
            )
          end
          actor = User.find_by(id: actor_id)
          unless actor
            return Twirp::Error.invalid_argument("unknown actor", argument: "actor_id")
          end

          unless (prefix = req.prefix).present?
            return Twirp::Error.invalid_argument("must be non-empty", argument: "prefix")
          end

          unless (key = req.key).present?
            return Twirp::Error.invalid_argument("must be non-empty", argument: "key")
          end

          unless (payload_json = req.payload_json).present?
            return Twirp::Error.invalid_argument("must be non-empty", argument: "payload_json")
          end
          begin
            payload = JSON.parse(payload_json)
          rescue JSON::ParserError
            return Twirp::Error.invalid_argument("invalid JSON", argument: "payload_json")
          end

          instrumentation_service = GitHub.instrumentation_service
          payload = expand_payload(payload, actor: actor)
          instrumentation_service.instrument("#{prefix}.#{key}", payload)

          { actor_id: actor.id }
        end

        # See Api::Internal::Twirp::Handler#require_user_token?
        #
        # No handler methods require a user token
        def require_user_token?(env)
          false
        end

        # See Api::Internal::Twirp::Handler#allow_client?
        #
        # Limits access to clients listed in ALLOWED_CLIENTS.
        def allow_client?(client_name, env)
          ALLOWED_CLIENTS.include?(client_name)
        end

        private

        def expand_payload(payload, actor:)
          payload[:actor] = actor
          expand_user_id(payload, :user)
          expand_user_id(payload, :deleted_by_user)
          Context::Expander.expand(payload)
        end

        # Private: Modifies the given audit log event payload hash such that a User is looked up
        # for the ID in the given field. This will result in the full event context for a User
        # being included in the payload instead of just the ID.
        def expand_user_id(payload, field_prefix)
          id_field = "#{field_prefix}_id"
          if !payload.key?(field_prefix) && (user_id = payload[id_field])
            user = User.find_by(id: user_id)
            if user
              payload.delete(id_field)
              payload[field_prefix] = user
            end
          end
        end

        # Private: Builds the service instance for this handler.
        #
        # See Api::Internal::Twirp::Handler#build_service
        def build_service
          GitHub::Proto::Auditlogs::V1::AuditlogsAPIService.new(self)
        end
      end
    end
  end
end
