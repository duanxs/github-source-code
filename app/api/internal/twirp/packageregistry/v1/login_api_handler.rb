# frozen_string_literal: true

require "github-proto-packageregistry"

class Api::Internal::Twirp
  module Packageregistry
    module V1
      class LoginApiHandler < Api::Internal::Twirp::Handler
        # Limited to the Package Registry client, identified by the HMAC key used to sign requests.
        # See Api::Internal::Twirp::ClientAccess and Api::Internal::Twirp::Handler#allow_client?
        ALLOWED_CLIENTS = %w(package_registry).freeze

        ERR_UNAUTHENTICATED = "User cannot be authenticated with the token provided."
        ERR_FORBIDDEN = "The token provided does not match expected scopes."

        def validate_token_scopes(req, env)
          token = env[:authorization].sub(/\A(Bearer|bearer) /, "")

          # This stubs all of the instance variables needed to get through the
          # the authorization code.
          #
          # Cause we expect it to be coming from Sinatra.
          @remote_ip = env[:real_ip]
          @response  = Sinatra::Response.new
          @request   = req

          # Setting this instance variable overrides the one set
          # in the method `api_auth`.
          @api_auth = GitHub::Authentication::Attempt.new(
            allow_integrations:                 false,
            allow_user_via_integration:         false,
            from:                               :api,
            token:                              token,
            ip:                                 remote_ip,
            user_agent:                         env[:user_agent],
            request_id:                         env[:request_id],
            api_password_auth_deprecated:       true,
            password_auth_blocked:              true,
            url:                                env[:path],
          )

          result = api_auth.result

          unless result.success?
            return Twirp::Error.unauthenticated(ERR_UNAUTHENTICATED, {})
          end

          @current_user = result.user
          @oauth = @current_user.oauth_access

          unless req.expected_scopes.all? { |scope| scope?(current_user, scope) }
            return Twirp::Error.permission_denied(ERR_FORBIDDEN, {})
          end

          # OrgID 0 is the special case when this route is called for CLI login only (not for package operations)
          if req.org_id == 0
            return {
                user_id: @current_user.id,
                user_name: @current_user.login,
                scopes: @current_user.scopes
            }
          end

          # Get the org from the org ID
          @current_org = Organization.find_by(id: req.org_id)

          # Create a placeholder package because access_allowed wants an obj for resource
          pkg = Registry::Package.new(owner: @current_org)

          begin
            unless access_allowed?(:authenticate_for_registry, resource: pkg, organization: @current_org, allow_integrations: false, allow_user_via_integration: false)
              return Twirp::Error.permission_denied(ERR_FORBIDDEN, {})
            end
          rescue Platform::Errors::Forbidden => e
            return Twirp::Error.permission_denied(e.to_s, {})
          end

          {
            user_id: @current_user.id,
            user_name: @current_user.login,
            scopes: @current_user.scopes
          }
        end

        # Internal: See Api::Internal::Twirp::Handler#require_user_token?
        def require_user_token?(env)
          false
        end

        # Internal: See Api::Internal::Twirp::Handler#allow_client?
        def allow_client?(client_name, env)
          ALLOWED_CLIENTS.include?(client_name)
        end

        # These override methods in `Authorization` to short-circuit (or implement) data requirements.
        #
        # Actually, not all of them are overrides: some are just implicitly
        # required by the methods in `Authorization`.
        def current_resource_owner
          @current_org
        end

        alias :find_org :current_resource_owner

        attr_reader :current_repo
        alias :find_repo :current_repo

        def current_repo_loaded?
          @current_repo.present?
        end

        attr_reader :response

        def graphql_request?
          true
        end

        def repo_nwo_from_path
          current_repo_loaded? && current_repo.nwo
        end

        def remote_ip
          @remote_ip
        end

        def request
          @request
        end

        def params
          {}
        end

        # Require an allowed IP when IP allow list enforcement is enabled?
        # Returns true by default.
        #
        # Returns Boolean.
        def require_allowed_ip?
          true
        end

        private
        # Private: Builds the service instance for this handler.
        # See Api::Internal::Twirp::Handler#build_service
        def build_service
          GitHub::Proto::Packageregistry::V1::LoginAPIService.new(self)
        end
      end
    end
  end
end
