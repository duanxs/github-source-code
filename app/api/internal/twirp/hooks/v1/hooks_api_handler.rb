# frozen_string_literal: true

require "github-proto-hooks"

require_dependency "api/internal/twirp/hooks/v1"

class Api::Internal::Twirp < ::Api::Internal
  module Hooks
    module V1

      # Provides access to Hook data.
      class HooksAPIHandler < Api::Internal::Twirp::Handler

        SEARCH_HOOKS_LIMIT = 50

        # Currently limited to these clients, identified by the HMAC key
        # used to sign requests.
        #
        # See Api::Internal::Twirp::ClientAccess and Api::Internal::Twirp::Handler#allow_client?
        # for more information.
        ALLOWED_CLIENTS = %w(event_hydrator).freeze

        # Public: Implementation of the SearchHooks Twirp RPC.
        #
        # req - The Twirp request as a GitHub::Proto::Hooks::V1::SearchHooksRequest.
        # env - The Twirp environment as a Hash.
        #
        # Returns the Twirp response as a Hash suitable for use in a
        # GitHub::Proto::Hooks::V1::SearchHooksResponse.
        def search_hooks(req, env)
          if req.event_type.blank?
            return Twirp::Error.invalid_argument("must be non-empty", argument: "event_type")
          elsif req.entity_id.zero?
            return Twirp::Error.invalid_argument("must be non-empty", argument: "entity_id")
          end

          hooks = Hook::Directory.search_hooks_from_proto_request(req, limit: SEARCH_HOOKS_LIMIT)

          {
            hooks: build_hook_list(hooks),
          }
        end

        # See Api::Internal::Twirp::Handler#require_user_token?
        #
        # This handler does not access user-restricted information.
        def require_user_token?(env)
          false
        end

        # See Api::Internal::Twirp::Handler#allow_client?
        #
        # Limits access to clients listed in ALLOWED_CLIENTS.
        def allow_client?(client_name, env)
          ALLOWED_CLIENTS.include?(client_name)
        end


        private

        # Private: Builds the service instance for this handler.
        #
        # See Api::Internal::Twirp::Handler#build_service
        def build_service
          GitHub::Proto::Hooks::V1::HooksAPIService.new(self)
        end

        # Private: Convert an array of Hook objects to the shape Twirp responses expect.
        #
        # hooks - The array of Hook objects.
        #
        # Returns an array of Hash objects with hook data that matches the
        # Twirp definition.
        def build_hook_list(hooks)
          hooks.map do |hook|
            {
              id: hook.id,
              is_active: hook.active?,
              is_confirmed: hook.confirmed?,
              creator_id: hook.creator_id,
              config: V1.config(hook),
            }
          end
        end
      end
    end
  end
end
