# frozen_string_literal: true

require "github-proto-policy"

class Api::Internal::Twirp
  module Policy
    module V1
      class RepositoryAPIHandler < Api::Internal::Twirp::Handler
        # Limited to the Policy client, identified by the HMAC key used to sign requests.
        # See Api::Internal::Twirp::ClientAccess and Api::Internal::Twirp::Handler#allow_client?
        ALLOWED_CLIENTS = %w(policy).freeze

        LIST_REPOSITORIES_DEFAULT_LIMIT = 100

        # Public: Implementation of the ListRepositories RPC, retrieving a list
        # of repositories for a given organization name.
        #
        # req - The Twirp request as a GitHub::Proto::Policy::V1::ListRepositoriesRequest.
        # env - The Twirp environment as a Hash.
        #
        # Returns the Twirp response data as a Hash suitable for use as a
        # GitHub::Proto::Policy::V1::ListRepositoriesResponse, or a Twirp::Error.
        def list_repositories(req, env)
          if req.org_id.zero?
            return Twirp::Error.invalid_argument("must be non-empty", argument: "org_id")
          end

          org = Organization.find_by_id(req.org_id)
          unless org.present?
            return Twirp::Error.not_found("organization not found")
          end

          page = req.page
          if req.page == 0
            page = 1
          end
          limit = req.limit == 0 ? LIST_REPOSITORIES_DEFAULT_LIMIT : req.limit

          repos = org.org_repositories.paginate(page: page, per_page: limit)
          GitHub::PrefillAssociations.prefill_associations(repos, [:labels, :languages, :protected_branches])
          GitHub::PrefillAssociations.prefill_associations(repos.map(&:languages).flatten, [:language_name])
          GitHub::PrefillAssociations.topic_names_for_repositories(repos)

          { repositories: build_repository_list(repos) }
        end

        # Internal: See Api::Internal::Twirp::Handler#require_user_token?
        def require_user_token?(env)
          false
        end

        # Internal: See Api::Internal::Twirp::Handler#allow_client?
        def allow_client?(client_name, env)
          ALLOWED_CLIENTS.include?(client_name)
        end

        private

        # Private: Builds the service instance for this handler.
        # See Api::Internal::Twirp::Handler#build_service
        def build_service
          GitHub::Proto::Policy::V1::RepositoryAPIService.new(self)
        end

        def build_label_list(repository)
          repository.labels.map do |label|
            {
              name: label.name,
              description: label.description,
              color: label.color,
            }
          end
        end

        def build_topic_list(repository)
          repository.topic_names.map { |topic_name| { topic: { name: topic_name } } }
        end

        def build_language(language)
          if language
            {
              name: language.language_name.name,
              color: language.language_name.language&.color
            }
          end
        end

        def build_primary_language(language_name)
          if language_name
            {
              name: language_name.name,
              color: Linguist::Language.find_by_name(language_name.name)&.color,
            }
          end
        end

        def build_language_list(repository)
          repository.languages.map do |language|
              build_language(language)
          end
        end

        def build_default_branch_ref(default_branch_ref)
          if default_branch_ref
            {
              name: default_branch_ref.name,
              prefix: default_branch_ref.prefix,
            }
          end
        end

        def build_license_info(license)
          if license
            {
              key: license.key,
              spdxId: license.spdx_id,
            }
          end
        end


        def build_branch_protection_rules(repository)
          repository.protected_branches.map do |branch|
            {
              databaseId: branch.id,
              dismissesStaleReviews: branch.dismiss_stale_reviews_on_push?,
              isAdminEnforced: branch.admin_enforced?,
              pattern: branch.name.dup.force_encoding("utf-8"),
              requiredApprovingReviewCount: branch.pull_request_reviews_enabled? ? branch.required_approving_review_count : 0,
              requiredStatusCheckContexts: branch.required_status_checks.map(&:context),
              requiresStatusChecks: branch.required_status_checks_enabled?,
              requiresApprovingReviews: branch.pull_request_reviews_enabled?,
              requiresCodeOwnerReviews: branch.require_code_owner_review?,
              requiresCommitSignatures: branch.required_signatures_enabled?,
              requiresStrictStatusChecks: branch.strict_required_status_checks_policy,
              restrictsPushes: branch.authorized_actors_only,
              restrictsReviewDismissals: branch.authorized_dismissal_actors_only,
            }
          end
        end

        def build_funding_links(repository)
          repository.has_funding_file?.then do |has_funding|
            next [] unless has_funding
            results = []

            repository.funding_links.validated_config.each do |key, value|
              platform = ::FundingPlatforms::ALL[key.to_sym]

              Array(value).each do |url|
                funding_link = Platform::Models::FundingLink.new(
                  repository: repository,
                  platform: platform.key,
                  url: "#{platform.url}#{url}",
                )

                results << {
                  platform: funding_link.platform,
                  url: funding_link.url,
                }
              end
            end

            results
          end
        end

        def time_to_timestamp(time)
          if time
            Google::Protobuf::Timestamp.new(seconds: time.to_i, nanos: time.nsec)
          end
        end

        # Private: Convert an array of Repository objects to the Twirp response
        # ListRepositoriesResponse.
        #
        # repositories - The array of Repository objects.
        #
        # Returns an array of Hash objects with repository data that matches the
        # Twirp definition.
        def build_repository_list(repositories)
          repositories.map do |repository|
            labels = build_label_list(repository)
            topics = build_topic_list(repository)
            languages = build_language_list(repository)
            branch_protection_rules = build_branch_protection_rules(repository)

            {
              name: repository.name,
              databaseId: repository.id,
              description: repository.description,
              mirrorUrl: repository.mirror.try(:url),
              nameWithOwner: repository.name_with_owner,
              openGraphImageUrl: repository.open_graph_image_url(:viewer),
              url: repository.http_url,
              resourcePath: repository.owner ? repository.path : nil,
              projectsResourcePath: repository.owner ? repository.path + "/projects" : nil,
              homepageUrl: repository.homepage,
              createdAt: time_to_timestamp(repository.created_at),
              pushedAt: time_to_timestamp(repository.pushed_at),
              updatedAt: time_to_timestamp(repository.updated_at),
              diskUsage: repository.disk_usage,
              forkCount: repository.forks_count,
              deleteBranchOnMerge: repository.delete_branch_on_merge?,
              hasIssuesEnabled: repository.has_issues?,
              squashMergeAllowed: repository.squash_merge_allowed?,
              rebaseMergeAllowed: repository.rebase_merge_allowed?,
              isArchived: repository.archived?,
              isDisabled: repository.disabled?,
              isFork: repository.fork?,
              isLocked: repository.locked?,
              isMirror: repository.mirror ? true : false,
              isPrivate: repository.private?,
              isTemplate: repository.template?,
              hasWikiEnabled: repository.has_wiki?,
              usesCustomOpenGraphImage: repository.uses_custom_open_graph_image?,
              licenseInfo: build_license_info(repository.license),
              primaryLanguage: build_primary_language(repository.primary_language),
              hasProjectsEnabled: repository.projects_enabled?,
              defaultBranchRef: build_default_branch_ref(repository.default_branch_ref),
              fundingLinks: build_funding_links(repository),
              labels: { nodes: labels, totalCount: labels.size },
              repositoryTopics: { nodes: topics, totalCount: topics.size },
              languages: { nodes: languages, totalCount: languages.size, totalSize: languages.size },
              #TODO(fatih): check permissions, i.e: lib/platform/objects/repository.rb/branc_protection_rules()
              branchProtectionRules: { nodes: branch_protection_rules, totalCount: branch_protection_rules.size},
            }
          end
        end
      end
    end
  end
end
