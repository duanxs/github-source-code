# frozen_string_literal: true

# Abstract base class for Twirp handler classes.
class Api::Internal::Twirp::Handler
  include Platform::Authorization

  attr_reader :current_repo, :current_user, :env

  # Public: Determines whether requests to this handler will be checked by
  # Api::Internal::Twirp::Access for a user token first.
  #
  # env - The Twirp environment as a Hash.
  #
  # Raises a NotImplementedError if not defined in subclasses, which should
  # instead return a boolean.
  def require_user_token?(env)
    raise NotImplementedError, "#require_user_token? must be implemented by subclasses"
  end

  # Public: Determines whether the current request is allowed for the given client name,
  # identified by the HMAC key used to sign requests.
  #
  # By default requests are disallowed for all clients.
  #
  # See Api::Internal::Twirp::ClientAccess for more information.
  #
  # client_name - The client name as a String.
  # env - The Twirp environment as a Hash.
  #
  # Returns a boolean.
  def allow_client?(client_name, env)
    false
  end

  # Public: Check if a current repo has been set.
  #
  # Needed by Platform::Authorization.
  #
  # Returns a boolean.
  def current_repo_loaded?
    current_repo.present?
  end

  # Public: Instantiates a service for this handler and memoizes the result.
  #
  # This also extends the service to add conveniences for manipulating
  # request lifecycle hooks more flexibly.
  #
  # Returns a Twirp::Service subclass instance.
  def service
    @service ||= build_service.extend(Api::Internal::Twirp::ServiceHooks)
  end

  private

  # Private: Find the first non-empty (non-zero) ID argument.
  #
  # Go uses 0 for empty int values, so we need to discard them.
  #
  # Returns an Integer if a non-empty ID is found, nothing
  # otherwise.
  def id_argument(*possible_ids)
    provided_ids = possible_ids.compact
    provided_ids.detect { |id| id > 0 }
  end

  # Private: Builds the service instance for this handler.
  #
  # Raises a NotImplementedError if not defined in subclasses, which should
  # instead return a Twirp::Service subclass instance.
  def build_service
    raise NotImplementedError, "#build_service must be implemented by subclasses"
  end
end
