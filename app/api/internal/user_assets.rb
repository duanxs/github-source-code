# rubocop:disable Style/FrozenStringLiteralComment

class Api::Internal::UserAssets < Api::Internal
  areas_of_responsibility :community_and_safety, :api

  post "/internal/assets/quarantined_user_asset" do
    @route_owner = "@github/communities-heart-reviewers"
    deliver_error! 404, message: "Not enabled for Enterprise" if GitHub.enterprise?

    params = receive_json(request.body.read, type: Hash)

    asset = UserAsset.find_by_id(params["asset_id"].to_i)

    unless asset
      return deliver_error! 404, message: "User asset not found"
    end

    hit = PhotoDnaHit.new(content: asset, uploader: asset.uploader)
    unless hit.save
      Failbot.report("PhotoDnaHit record failed to save for UserAsset with ID #{asset.id}")
    end

    if asset.quarantine(reason: params["reason"])
      deliver_empty status: 200
    else
      deliver_error! 500, message: "quarantine not successful"
    end
  end

  delete "/internal/assets/quarantined_user_asset" do
    @route_owner = "@github/communities-heart-reviewers"
    deliver_error! 404, message: "Not enabled for Enterprise" if GitHub.enterprise?

    params = receive_json(request.body.read, type: Hash)

    asset = UserAsset.find_by_id(params["asset_id"].to_i)

    if !asset
      deliver_error! 404, message: "User asset not found"
    elsif asset.unquarantine(reason: params["reason"])
      deliver_empty status: 200
    else
      deliver_error! 500, message: "unquarantine not successful"
    end
  end
end
