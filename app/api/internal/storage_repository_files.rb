# rubocop:disable Style/FrozenStringLiteralComment

# Implements the API that Alambic uses for serving or accepting repository
# files through the Alambic storage cluster. Used on GitHub Enterprise only.
#
# https://github.com/github/alambic/tree/master/docs/assets
class Api::Internal::StorageRepositoryFiles < Api::Internal::StorageUploadable
  areas_of_responsibility :code_collab, :api
  require_api_version "smasher"

  get "/internal/storage/repositories/:repository_id/files/:id" do
    @route_owner = Platform::NoOwnerBecause::UNAUDITED
    file = ActiveRecord::Base.connected_to(role: :reading) { RepositoryFile.find_by_id(params[:id].to_i) }
    file = nil if file.repository_id != params[:repository_id].to_i
    repo = file && file.repository
    control_access :read_repo_file, repo: repo, file: file, allow_integrations: false, allow_user_via_integration: false
    GitHub::PrefillAssociations.for_repo_files([file])
    deliver :internal_storage_hash, file, env: request.env
  end

  post "/internal/storage/repositories/:repository_id/files" do
    @route_owner = Platform::NoOwnerBecause::UNAUDITED
    repo = ActiveRecord::Base.connected_to(role: :reading) { Repository.find_by_id(params[:repository_id].to_i) }
    control_access :pull_storage, resource: repo, user: current_user, allow_integrations: false, allow_user_via_integration: false
    validate_uploadable RepositoryFile,
      meta: {repository_id: params[:repository_id]},
      repo: repo
  end

  post "/internal/storage/repositories/:repository_id/files/verify" do
    @route_owner = Platform::NoOwnerBecause::UNAUDITED
    repo = ActiveRecord::Base.connected_to(role: :reading) { Repository.find_by_id(params[:repository_id].to_i) }
    control_access :pull_storage, resource: repo, user: current_user, allow_integrations: false, allow_user_via_integration: false
    create_uploadable RepositoryFile,
      meta: {repository_id: params[:repository_id]}
  end

  def verify_user(meta)
    RepositoryFile.storage_verify_token(@asset_token, meta)
  end
end
