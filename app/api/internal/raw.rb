# frozen_string_literal: true

class Api::Internal::Raw < Api::Internal::GitContent
  INTERNAL_RAW_COMMIT_OID_REGEX = /\A[a-f0-9]{40}\z/

  rate_limit_as Api::RateLimitConfiguration::RAW_FAMILY

  # Internal API endpoint for raw.githubusercontent.com.  Used to authenticate a raw
  # request.
  #
  #   https://raw.githubusercontent.com/{user}/{repo}/{commitish}/{path}?token={token}
  #
  get "/internal/raw/github" do
    # for new stuff, we want to pass all these params via params to avoid
    # path traversal
    @route_owner = "@github/pe-repos"
    populate_legacy_params_from_path_param

    # Bypass IP allow list enforcement to this endpoint called from codeload
    # for public repositories.
    bypass_allowed_ip_enforcement if current_repository&.public?

    verify_not_spammy

    branch, path = ref_sha_path_extractor.call(content_path)

    unless branch && path
      deliver_error!(404,
                     message: "invalid ref from #{GitHub::JSON.encode content_path}",
                     documentation_url: "/v3/repos/contents/#get-contents")
    end

    attempt_remote_token_login RawBlob.scope(current_repository, branch, path)

    control_access :get_contents, resource: current_repository, path: content_path, allow_integrations: true, allow_user_via_integration: true

    serve_contents do
      deliver_blob_info current_repository, branch, file: path, geo_block_list: current_repository.access.blocked_countries
    end
  end

  # Request comes from:
  #
  #   https://raw.githubusercontent.com/wiki/{user}/{repo}/{commitish}/{path}?token={token}
  #
  get "/internal/raw/wiki" do
    # for new stuff, we want to pass all these params via params to avoid
    # path traversal
    @route_owner = "@github/pe-repos"
    populate_legacy_params_from_path_param

    # Bypass IP allow list enforcement to this endpoint called from codeload
    # for public repositories.
    bypass_allowed_ip_enforcement if current_repository&.public?

    verify_wiki_access_allowed
    verify_not_spammy

    attempt_remote_token_login RawBlob.wiki_scope(current_repository, content_path)
    control_access :get_contents, resource: current_repository, allow_integrations: false, allow_user_via_integration: false

    wiki = current_repository.unsullied_wiki
    gitrpc { deliver_blob_info(wiki, wiki.default_oid, prefix: current_repository.id.to_s, nwo: "#{current_repository.name_with_owner}.wiki") }
  end

  # Request comes from:
  #
  #   https://gist.githubusercontent.com/{user}/{gist}/raw/{sha}/{filename}
  get "/internal/raw/gist" do
    # for new stuff, we want to pass all these params via params to avoid
    # path traversal
    @route_owner = "@github/pe-repos"
    populate_legacy_params_from_path_param

    verify_not_spammy

    parts = content_path.split("/")

    # Shift raw off of the parts list, this is from legacy
    # compat with the existing gist raw url structure
    parts.shift if parts.first == "raw"

    commit_oid = if parts.first =~ INTERNAL_RAW_COMMIT_OID_REGEX
      parts.shift
    end

    if parts.first =~ INTERNAL_RAW_COMMIT_OID_REGEX
      parts.shift
    end

    # "raw" isn't a valid username, don't try to use it to find a gist
    owner_param = :user unless params[:user] == "raw"
    gist = find_gist!(param_name: :gist, owner_param: owner_param)
    path = parts.join("/")
    attempt_remote_token_login RawBlob.gist_scope(gist, commit_oid || gist.sha, path)

    gitrpc do
      blob_oid = gist.retrieve_blob_oid_for_path(path, commit_oid)
      deliver_blob_info gist, nil, commit: blob_oid, file: path
    end
  end

  def deliver_blob_info(routable, commitish = nil, hash = {})
    hash[:file] ||= content_path
    hash[:commit] ||= begin
                        if commitish
                          # If we don't know our commit OID, we will look it up, but prefer to
                          # find a branch rather than a tag because the web UI is biased
                          # towards branches, rather than the ref lookup rules, that bias
                          # toward finding tags first.
                          # See:
                          #  https://www.kernel.org/pub/software/scm/git/docs/git-rev-parse.html
                          #  https://github.com/github/github/issues/40473
                          commit_oid ||= (
                            routable.rpc.rev_parse("refs/heads/#{commitish}") ||
                            routable.rpc.rev_parse("#{commitish}^{}")
                          )
                        end
                        if !commit_oid
                          return deliver_error 404, message: "bad branch: #{h commitish.inspect}"
                        end

                        routable.rpc.gist_blob_oid_by_path(hash[:file], commit_oid)
                      end

    preferred_dc = params[:dc] || GitHub.datacenter
    hash[:routes] ||= routable.dgit_read_routes(preferred_dc: preferred_dc).map do |route|
      { "route" => route.resolved_host, "path" => route.path }
    end
    hash[:route] ||= routable.route
    hash[:path] ||= routable.shard_path
    hash[:prefix] ||= "#{routable}"
    hash[:type] ||= "raw"
    hash[:mime_type] ||= begin
                           # routable should include TreeListable
                           if routable.respond_to?(:blob_by_oid)
                             blob = routable.blob_by_oid(hash[:commit], full_blob: false)
                             raw_mime_for(content_path, is_text: blob.text?)
                           else
                             raw_mime_for(content_path)
                           end
                         end
    hash[:can_sanitize] = (hash[:mime_type] == "image/svg+xml")
    hash[:nwo] ||= routable.name_with_owner
    hash[:allow_raw_svg] = GitHub.flipper[:codeload_allow_raw_svg_transition].enabled?(routable)

    deliver_raw hash
  end

  def populate_legacy_params_from_path_param
    deliver_error! 404, message: "Missing path parameter" unless params[:path]
    params[:path] = params[:path][1..-1] if params[:path][0] == "/"
    params[:user], params[:repo], params[:splat] = params[:path].scrub.split(/\//, 3)
    deliver_error! 404, message: "Bogus path" unless (params[:user] && params[:repo])
    params[:splat] = if params[:splat]
      [params[:splat].gsub(/\/+\Z/, "")]
    else
      []
    end
    params[:gist] = params[:repo]
  end

  def fingerprint_rate_limit_configuration
    {
      max_tries: GitHub.api_raw_fingerprint_rate_limit,
      ttl: rate_limit_configuration.duration,
      amount: increment_rate_limit_amount,
    }
  end
end
