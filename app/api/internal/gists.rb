# frozen_string_literal: true

class Api::Internal::Gists < Api::Internal
  include Api::App::JobEnqueueHelpers

  statsd_tag_actions "/internal/gists/:id/git/pushes"

  # Internal: Given a gist identifier, returns the gist database ID and repo name.
  get "/internal/gists/:id" do
    @route_owner = "@github/pe-repos"

    gist = if params[:include_hidden] == "true"
      find_gist_include_hidden!
    else
      find_gist!
    end

    deliver :internal_gist_hash, gist
  end

  # Internal: After a successful push to a Gist Git repository happened, this endpoint
  # is notified about who updated which refs over what protocol.
  post "/internal/gists/:id/git/pushes" do
    @route_owner = "@github/pe-repos"
    gist = find_gist!
    data = receive_with_schema("git-push", "create")
    decode_push_data!(data)

    enqueue_push_job(gist, data)

    deliver_empty status: 202
  end

  def externally_accessible?
    false
  end

  def require_request_hmac?
    true
  end

  def authenticated_for_private_mode?
    true
  end

  # Internal: Behaves like find_gist! but returns disabled gists.
  def find_gist_include_hidden!
    find_gist.tap do |gist|
      record_or_404(gist)
    end
  end

  # Relax user-agent requirements for internal API endpoints used by babeld
  def user_agent_allows_access_to_garage_hosts?
    request.user_agent =~ /#{GitHub.current_sha}|^babeld\/.*/
  end
end
