# frozen_string_literal: true

# Implements the API that Alambic uses for serving or accepting marketplace listing
# screenshot uploads through the Alambic storage cluster. Used in local development.
#
# https://github.com/github/alambic/tree/master/docs/assets
class Api::Internal::StorageMarketplaceListingScreenshots < Api::Internal::StorageUploadable
  areas_of_responsibility :marketplace, :api
  require_api_version "smasher"

  def self.enforce_private_mode?
    false
  end

  get "/internal/storage/marketplace-listing-screenshots/:id/files/:guid" do
    @route_owner = "@github/marketplace-eng"
    listing = ActiveRecord::Base.connected_to(role: :reading) { Marketplace::Listing.find(params[:id]) }
    control_access :read_marketplace_listing, marketplace_listing: listing, allow_integrations: false, allow_user_via_integration: false

    screenshot = listing.screenshots.find_by_guid(params[:guid])
    GitHub::PrefillAssociations.for_marketplace_listing_screenshots([screenshot])
    deliver :internal_storage_hash, screenshot, env: request.env
  end

  post "/internal/storage/marketplace-listing-screenshots/:id/files" do
    @route_owner = "@github/marketplace-eng"
    listing = ActiveRecord::Base.connected_to(role: :reading) { Marketplace::Listing.find(params[:id]) }
    control_access :write_marketplace_listing, marketplace_listing: listing, allow_integrations: false, allow_user_via_integration: false
    validate_uploadable Marketplace::ListingScreenshot,
      meta: { marketplace_listing_id: params[:id] }
  end

  post "/internal/storage/marketplace-listing-screenshots/:id/files/verify" do
    @route_owner = "@github/marketplace-eng"
    listing = ActiveRecord::Base.connected_to(role: :reading) { Marketplace::Listing.find(params[:id]) }
    control_access :write_marketplace_listing, marketplace_listing: listing, allow_integrations: false, allow_user_via_integration: false
    create_uploadable Marketplace::ListingScreenshot,
      meta: { marketplace_listing_id: params[:id] }
  end

  def verify_user(meta)
    Marketplace::ListingScreenshot.storage_verify_token(@asset_token, meta)
  end
end
