# rubocop:disable Style/FrozenStringLiteralComment

# Internal Auth endpoint for github/lfs-server
class Api::Internal::Lfs < Api::Internal
  areas_of_responsibility :pe_repos
  LFS_VERIFY_BATCH_SIZE         = 1000
  LFS_URL_GENERATION_BATCH_SIZE = 1000

  # LFS will be killed when password auth for git is deprecated
  def password_auth_blocked?
    return false if GitHub.git_password_auth_supported?

    logged_in? &&
      GitHub.flipper[:brownout_git_basic_auth_blocking].enabled?(current_user)
  end

  post "/internal/lfs/auth" do
    @route_owner = "@github/git-protocols"
    abilities = repo_abilities
    if abilities.blank?
      deliver_error!(@auth_error_status || 401, message: "Invalid repository")
    end

    output = {
      repository: {id: @repo.id, abilities: abilities, ability: abilities.first},
    }

    if authenticated?
      output[:viewer] = if logged_in?
        {id: current_user.id, login: current_user.login, type: "user"}
      else
        {id: @authenticated_key.id, login: @repo.nwo, type: "deploy-key"}
      end
    end

    deliver_raw output
  end

  def require_request_hmac?
    true
  end

  post "/internal/lfs/users" do
    @route_owner = "@github/git-protocols"
    deliver_error!(403, message: "Invalid token") unless login_from_lfs_server_token?

    user_ids = Array(@json_body["user_ids"])
    user_ids.map! { |i| i.to_i }
    user_ids.uniq!
    user_ids.delete_if { |i| i < 0 }

    if user_ids.size > 100
      deliver_error! 400, message: "Too many user ids: #{user_ids.size}"
    end

    output = {users: {}}
    User.where(id: user_ids).each do |u|
      next if u.disabled? || u.suspended?
      output[:users][u.id.to_s] = {login: u.login}
    end if user_ids.size > 0

    deliver_raw output
  end

  post "/internal/lfs/verify" do
    @route_owner = "@github/git-protocols"
    @current_repo = @repo = load_repository(@json_body)
    deliver_error!(400, message: "No valid repository") unless @repo

    # Accept only hashes with an exact length of 64 characters
    requested_oids = Array(@json_body["oids"])
    requested_oids.delete_if { |i| i.length != 64 }
    requested_oids.uniq!
    deliver_error!(400, message: "No valid LFS object IDs") unless requested_oids.size > 0

    unknown = []
    requested_oids.in_groups_of(LFS_VERIFY_BATCH_SIZE, false) do |oids|
      query_oids =  {
        oid: oids,
        repository_network_id: @repo.network_id,
        state: Media::Blob::states[:verified],
      }

      # The "/internal/lfs/verify" call is usually made right after new
      # Media::Blobs have been pushed. In order to reduce the risk of
      # races we perform the query explicitly not against the read-only
      # replica.
      verified_count = Media::Blob.where(query_oids).count

      if oids.length != verified_count
        # Query the actual object IDs only in the unhappy path
        verified_oids = Media::Blob.where(query_oids).pluck(:oid)
        unknown += oids - verified_oids
      end

      break if unknown.length > 0
    end

    log_data.update(
      "lfs_missing_objects_count".intern => unknown.length,
    )

    deliver_raw(unknown: unknown)
  end

  post "/internal/lfs/download-urls" do
    @route_owner = "@github/git-protocols"
    @current_repo = @repo = load_repository(@json_body)
    deliver_error!(400, message: "No valid repository") unless @repo

    # Accept only hashes with an exact length of 64 characters
    requested_oids = Array(@json_body["oids"])
    requested_oids.delete_if { |i| i.length != 64 }
    requested_oids.uniq!
    deliver_error!(400, message: "No valid LFS object IDs") unless requested_oids.size > 0

    control_access :get_media_blob,
        repo: @repo,
        allow_integrations: true,
        allow_user_via_integration: true

    result = {}
    requested_oids.in_groups_of(LFS_URL_GENERATION_BATCH_SIZE) do |oids|
      query_oids =  {
        oid: oids,
        repository_network_id: @repo.network_id,
        state: Media::Blob::states[:verified],
      }

      result.merge!(Media::Blob.where(query_oids).map { |blob| [blob.oid, blob.download_link(actor: current_user, repo: @repo)] }.to_h)
    end

    deliver_error!(404, message: "No LFS objects found") if result.empty?
    deliver_raw(links: result)
  end

  def repo_abilities
    return [] unless @repo
    if !authenticated?
      return @repo.public? ? [:pull] : []
    end

    if @authenticated_key
      if @lfs_operation == :upload && !@authenticated_key.read_only?
        return [:push, :pull]
      end
      return [:pull]
    end

    if @lfs_operation != :upload
      return @repo.pullable_by?(current_user) ? [:pull] : []
    end

    if @repo.adminable_by?(current_user)
      [:admin, :push, :pull]
    elsif @repo.pushable_by?(current_user)
      [:push, :pull]
    elsif @repo.pullable_by?(current_user)
      [:pull]
    end
  end

  # This is called before the actual request since Api::App#populate_context_with_authentication_details
  # calls #logged_in?. This parses the request body so it can load the repository,
  # which is needed to verify a remote auth token.
  def attempt_login
    # prevents this method being called multiple times when called anonymously
    return if @json_body
    @json_body = receive(Hash)
    @current_user = nil
    @authenticated = nil
    @deploy_key = nil

    # These are the only endpoints that need to auth as a user
    return unless request.path_info =~ %r[\A/internal/lfs/(auth|download-urls)\z]
    @endpoint = Regexp.last_match[1]

    if @repo = load_repository(@json_body)
      @accepted_scopes = [:repo]
      @accepted_scopes << :public_repo if @repo.public?
    end

    login_from_api_auth
    login_from_remote_auth unless @current_user

    if @repo && GitHub.flipper[:lfs_disable].enabled?(@repo)
      deliver_error! 404
    end

    @auth_error_status = 401

    if authenticated?
      @auth_error_status = 403
      if (@current_user && GitHub.flipper[:lfs_disable].enabled?(@current_user)) || (@repo && GitHub.flipper[:lfs_disable].enabled?(@repo))
        deliver_error! 404
      end

      if @oauth && !@oauth.scopes?(*@accepted_scopes)
        deliver_error! 403, message: "Invalid OAuth scopes"
      end

      @lfs_operation ||= :upload
      return unless @current_user && @current_user.suspended?
      @current_user = nil
    end

    return unless GitHub.private_mode_enabled?
    return unless @endpoint == "auth"

    deliver_error!(@auth_error_status, message: "Must authenticate to access this API.")
  end

  def login_from_remote_auth
    return unless @repo
    token = Api::RequestCredentials.token_from_scheme(env, "remoteauth")
    return if token.blank?

    operations = [:upload, :download]
    operations.each do |op|
      result = verify_token(token, op)

      log_data.update(
        "lfs_media_#{op}_scope".intern => result.reason,
      )

      if result.valid?
        @lfs_operation = op
        @remote_token_auth = true
        @current_user, @authenticated_key = case
        when user = Media::Token.user_for_token(result)
          # Signed auth token or GitAuth token for user.
          [user, nil]
        when key = Media::Token.deploy_key_for_token(result)
          # GitAuth token for deploy key.
          [nil, key]
        else
          # Something else.  The token was valid, but it grants no access.
          [nil, nil]
        end
        return
      end
    end
  end

  def verify_token(token, op)
    scope = Media.auth_scope(@repo.id, op.to_s, deploy_key_id: deploy_key_id)

    if GitHub::Authentication::GitAuth::SignedAuthToken.valid_format?(token)
      GitHub::Authentication::GitAuth::SignedAuthToken.verify(
        token: token,
        scope: scope,
        repo: @repo,
      )
    else
      User.verify_signed_auth_token(
        token: token,
        scope: scope,
      )
    end
  end

  def deploy_key_id
    Media.deploy_key_id_from_env(env)
  end

  def authenticated?
    !!@current_user || !!@authenticated_key
  end

  def deploy_key?
    @deploy_key
  end

  def login_from_lfs_server_token?
    token = Api::RequestCredentials.token_from_scheme(env, "token")
    GitHub.lfs_server_token.present? && token.present? &&
      SecurityUtils.secure_compare(token, GitHub.lfs_server_token)
  end

  def authenticated_for_private_mode?
    true
  end

  def rate_limit_status_code
    429
  end

  def load_repository(body)
    repo_name = body["repository"]
    repo_id = body["repository_id"]
    if repo_name.blank? && repo_id.blank?
      deliver_error! 400, message: "repository key is empty"
    end

    repo = Repository.with_name_with_owner(repo_name)
    repo ||= RepositoryRedirect.find_redirected_repository(repo_name)
    repo ||= Repository.find_by_id(repo_id.to_i)
    repo = nil if repo && repo.access.disabled?

    repo
  end

  def check_whitelist_for_request_limiting
    GitHub::Limiters::Middleware.skip_limit_checks(env)
  end
end
