# frozen_string_literal: true

# Shared base class for internal API endpoints deliverying info about git content.
class Api::Internal::GitContent < Api::Internal
  areas_of_responsibility :raw, :api

  include Api::App::ContentHelpers
  include RawBlob::ContentHelpers

  require_api_version "hyperion"

  HTTP_X_CLIENT_AS = "HTTP_X_CLIENT_AS"
  HTTP_X_CLIENT_UA = "HTTP_X_CLIENT_UA"
  HTTP_X_CLIENT_IP = "HTTP_X_CLIENT_IP"

  def repository_or_gist_or_404(repo_or_gist)
    if repo_or_gist.is_a?(Repository)
      repository_or_404(repo_or_gist)
    else
      gist_or_404(repo_or_gist)
    end
  end

  def find_repo
    @current_repo ||= ActiveRecord::Base.connected_to(role: :reading) do
      repo = Repository.nwo(params[:user].to_s, params[:repo].to_s)
      repo ||= RepositoryRedirect.find_redirected_repository("#{params[:user]}/#{params[:repo]}")
      repo = nil if repo && repo.access.disabled?

      repo
    end
  end

  def verify_not_spammy
    return unless (path = params[:user].to_s).present?
    return unless repo_owner = User.find_by_login(path)

    if repo_owner.spammy?
      return if repo_owner == @current_user # Spammy repo owner can download their own repo
      deliver_error! 403, message: "User #{repo_owner} flagged as spammy"
    end
  end

  # Check if the current repo is allowed to open the wiki feature
  def verify_wiki_access_allowed
    unless current_repository.plan_supports?(:wikis)
      deliver_error! 403, message: "Upgrade to GitHub Pro or make this repository public to enable this feature."
    end
  end

  def rate_limited_route?
    GitHub.flipper[:raw_throttle].enabled?
  end

  def rate_limit_status_code
    # For git content we use an explicit "too many requests" response.  This is
    # different than standard API rate limit response because our client is
    # codeload which needs to be able to distinguish  between rate limited and
    # legitimate authorization responses to support caching.
    # See https://github.com/github/github/issues/104509
    429
  end

  # Return a hash including keys such as max_tries and ttl to configure the rate limit
  # for fingerprint based throttler. Return nil to disable
  def fingerprint_rate_limit_configuration
    # sub-classes to define
  end

  def throttler
    return @throttler if defined?(@throttler)

    options = {
      amount: increment_rate_limit_amount,
    }

    throttlers = []

    # Requests that we consider "hotlinked" have a special per-resource rate limit
    unless request.referrer.blank? || request.referrer =~ /^https?:\/\/([^\/]+)?github.com(\/|$)/
      throttlers << Api::BlobThrottler.new(
       request.fullpath, request.referrer, options)
    end

    # If we have the real client's IP or this is an authenticated request, we
    # use our standard API rate limiting. Note that we DO NOT want to rate limit
    # by the internal api client's ip.
    if rate_limit_configuration.authenticated_request? || request.has_header?(HTTP_X_CLIENT_IP)
      throttlers << Api::ConfigThrottler.new(rate_limit_configuration, options)
    end

    # For extra protection against attacks that are spread across many IPs we
    # include a much higher limit throttler on "fingerprint".
    unless rate_limit_configuration.authenticated_request?
      if client_fingerprint && fingerprint_rate_limit_configuration
        # This could be made into a specific Throttler class if was to be re-used
        throttlers << Api::Throttler.new(
          "api_count:#{rate_limit_configuration.family}:fingerprint-#{client_fingerprint}",
          fingerprint_rate_limit_configuration)
      end
    end

    # Returning a nil throttler disables rate limiting
    @throttler = Api::MultiThrottler.new(throttlers) unless throttlers.empty?
  end

  def client_fingerprint
    if request.has_header? HTTP_X_CLIENT_AS
      "#{request.env[HTTP_X_CLIENT_AS]}-#{request.env[HTTP_X_CLIENT_UA]}".to_md5
    end
  end

  # We'll handle private mode by enforcing a ?token.  #current_user is not set
  # yet because we need to parse out the branch and path for the remote auth
  # scope.
  def protect_access_to_enterprise_hosts
  end
end
