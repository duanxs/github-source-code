# rubocop:disable Style/FrozenStringLiteralComment

# Implements the API that Alambic uses for serving or acceping cached
# render blobs through the Alambic storage cluster. Used on GitHub Enterprise
# only.
#
# https://github.com/github/alambic/tree/master/docs/assets
class Api::Internal::StorageRender < Api::Internal::StorageUploadable
  areas_of_responsibility :rendering, :api
  require_api_version "smasher"

  get "/internal/storage/render/:name" do
    @route_owner = "@github/render"
    blob = ActiveRecord::Base.connected_to(role: :reading) do
      RenderBlob.includes(:storage_blob).where(name: params[:name]).first
    end
    repo = blob && blob.repository

    control_access :view_render_blob, repo: repo, user: current_user, allow_integrations: false, allow_user_via_integration: false

    deliver :internal_storage_hash, blob, env: request.env
  end

  post "/internal/storage/render" do
    @route_owner = "@github/render"
    repo = ActiveRecord::Base.connected_to(role: :reading) do
      find_polymorphic_repository(@asset_meta[:repository_type],
                                  @asset_meta[:repository_id])
    end

    if GitHub.render_blob_storage_token.blank?
      control_access :view_render_blob, repo: repo, user: current_user, allow_integrations: false, allow_user_via_integration: false
    end

    validate_uploadable RenderBlob,
      meta: {path: env["HTTP_GITHUB_PATH"], ref: env["HTTP_GITHUB_REF"]},
      repo: repo
  end

  post "/internal/storage/render/verify" do
    @route_owner = "@github/render"
    repo = ActiveRecord::Base.connected_to(role: :reading) do
      find_polymorphic_repository(@asset_meta[:repository_type],
                                  @asset_meta[:repository_id])
    end

    if GitHub.render_blob_storage_token.blank?
      control_access :view_render_blob, repo: repo, user: current_user, allow_integrations: false, allow_user_via_integration: false
    end

    create_uploadable RenderBlob,
      meta: {path: env["HTTP_GITHUB_PATH"], ref: env["HTTP_GITHUB_REF"]}
  end

  def verify_user(meta)
    # Render creates RenderBlob records with a shared token
    #
    # TODO: Remove once enterprise2 is configuring the render_blob_storage_token
    # and all enterprise2 tests are passing.
    if request.post? && RenderBlob.render_token?(@asset_token)
      return GitHub::Authentication::SignedAuthToken.new(reason: "render token")
    end

    # this verifies remote auth tokens for _viewing_ render blobs
    blob_key = (meta[:name] || meta[:path_info] || "")[/([a-f0-9]+-[^\/]+$)/, 1]
    blob = blob_key && ActiveRecord::Base.connected_to(role: :reading) { RenderBlob.find_by_name(blob_key) }

    # These values can come from a blob, if we can find one, or from the request.
    # We have to pick the most reliable set to construct the scope to auth the token
    # The blob will exists on GET/HEAD requests for pre-created blobs, the request will
    # contain the data required otherwise.
    repository = ActiveRecord::Base.connected_to(role: :reading) do
      (blob && blob.repository) || find_polymorphic_repository(meta[:repository_type],
                                                               meta[:repository_id])
    end

    ref = (blob && blob.ref) ||
          env["HTTP_GITHUB_REF"]
    path = blob && blob.path ||
           env["HTTP_GITHUB_PATH"]

    scope = if repository && ref && path
      if repository.is_a?(Repository)
        RawBlob.scope(repository, ref, path)
      elsif repository.is_a?(Gist)
        RawBlob.gist_scope(repository, ref, path)
      end
    else
      "storage-render-no-scope"
    end

    User.verify_signed_auth_token(token: @asset_token, scope: scope.to_s)
  end

  def enforce_private_mode?
    return false unless super
    !(request.post? && RenderBlob.render_token?(@asset_token))
  end

  def authenticated_for_private_mode?
    return true if super
    request.post? && RenderBlob.render_token?(@asset_token)
  end

  def grab_metadata
    super

    # set default repository_type for back compatibility
    @asset_meta[:repository_type] ||= "Repository"
  end

  private

  def find_polymorphic_repository(repository_type, repository_id)
    return unless repository_id

    case repository_type.downcase.intern
    when :repository
      Repository.find_by_id(repository_id)
    when :gist
      Gist.find_by_id(repository_id)
    end
  end
end
