# frozen_string_literal: true

require "resolv"

class Api::Internal::Pages < Api::Internal
  # Internal: Given a page id and a signed user session token, this endpoint
  # checks if the token is valid and returns a status :ok
  post "/internal/pages/auth" do
    @route_owner = "@github/pe-issues-projects"
    data = receive_with_schema("internal_page", "validate-pages-token", skip_validation: true)

    token = data["token"].to_s
    page_id = data["page_id"].to_i
    ip_address = data["ip_address"].to_s

    if token.blank?
      deliver_error! 400, message: "property 'token' is required in request body"
    end

    if ip_address.blank?
      deliver_error! 400, message: "property 'ip_address' is required in request body"
    end

    unless page = Page.find_by_id(page_id)
      deliver_error! 404, message: "page does not exist"
    end

    deliver_error! 400, message: "property 'ip_address' is not a valid ip in request body" unless  !!(ip_address =~ Regexp.union([Resolv::IPv4::Regex, Resolv::IPv6::Regex]))

    auth_token = GitHub::Authentication::SignedAuthToken::Session.verify(
      token: token,
      scope: page.auth_token_scope,
    )

    unless auth_token.valid?
      if auth_token.bad_token?
        deliver_error! 401, message: "token format is invalid"
      elsif auth_token.bad_scope?
        deliver_error! 401, message: "token is not valid within the scope of this page"
      elsif auth_token.bad_login?
        deliver_error! 401, message: "token has an invalid user id"
      elsif auth_token.expired?
        deliver_error! 401, message: "token has expired"
      elsif auth_token.user_suspended?
        deliver_error! 401, message: "token is for a suspended user"
      elsif auth_token.session_expired?
        deliver_error! 401, message: "token's session has expired"
      elsif auth_token.session_revoked?
        deliver_error! 401, message: "token's session has been revoked"
      else
        deliver_error! 401, message: "token is malformed or has been tampered with"
      end
    end

    unless page.repository.pullable_by?(auth_token.user)
      deliver_error! 403, message: "user does not have read access to the repository"
    end

    satisfied = ip_address_allowed?(page.repository.owner, ip_address, auth_token.user)

    unless satisfied
      deliver_error! 403, message: <<~MSG.squish
        Although you appear to have the correct authorization credentials,
        the #{page.repository.owner.name} #{page.repository.owner.type} has an
        IP allow list enabled, and #{ip_address} is not permitted to access
        this resource.
      MSG
    end

    if saml_enforced?(page.repository.owner, auth_token.user)
      saml = Platform::Authorization::SAML.new(session: auth_token.session)
      if saml.authorized_organization_ids.exclude?(page.repository.owner.id)
        deliver_error! 403, message: "no active saml sessions found."
      end
    end

    deliver :internal_token_validation_output, auth_token.valid?, repo: page.repository, status: 200
  end

  def externally_accessible?
    false
  end

  def require_request_hmac?
    true
  end

  def saml_enforcement_policy_for(owner, current_user)
    provider_owner = owner.external_identity_session_owner
    case provider_owner
    when ::Organization
      Organization::SamlEnforcementPolicy.new(organization: provider_owner, user: current_user)
    when ::Business
      organization = owner.is_a?(::Organization) ? owner : nil
      Business::SamlEnforcementPolicy.new(business: provider_owner, organization: organization, user: current_user)
    end
  end
end
