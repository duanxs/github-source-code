# frozen_string_literal: true

# Implements the API that Alambic uses for serving or accepting repository
# image uploads through the Alambic storage cluster.
#
# https://github.com/github/alambic/tree/master/docs/assets
class Api::Internal::StorageRepositoryImages < Api::Internal::StorageUploadable
  areas_of_responsibility :repositories, :api
  require_api_version "smasher"

  def self.enforce_private_mode?
    false
  end

  get "/internal/storage/repository/:id/images/:guid" do
    @route_owner = "@github/pe-repos"
    repo = Repository.find_by_id(params[:id])
    return deliver_error!(404) unless repo
    if image = repo.repository_images.find_by_guid(params[:guid])
      control_access :read_repo_file, repo: repo, file: image, allow_integrations: false,
        allow_user_via_integration: false
      GitHub::PrefillAssociations.for_repository_images([image])
      deliver :internal_storage_hash, image, env: request.env
    else
      deliver_error!(404)
    end
  end

  post "/internal/storage/repository/:id/images" do
    @route_owner = "@github/pe-repos"
    repo = ActiveRecord::Base.connected_to(role: :reading) { Repository.find_by_id(params[:id]) }
    return deliver_error!(404) unless repo
    control_access :edit_repo, resource: repo, approved_integration_required: false,
      allow_user_via_integration: false, allow_integrations: false
    validate_uploadable RepositoryImage, repo: repo, meta: { repository_id: params[:id] }
  end

  post "/internal/storage/repository/:id/images/verify" do
    @route_owner = "@github/pe-repos"
    repo = ActiveRecord::Base.connected_to(role: :reading) { Repository.find_by_id(params[:id]) }
    return deliver_error!(404) unless repo
    control_access :edit_repo, resource: repo, approved_integration_required: false,
      allow_user_via_integration: false, allow_integrations: false
    create_uploadable RepositoryImage, meta: { repository_id: params[:id] }
  end

  def verify_user(meta)
    RepositoryImage.storage_verify_token(@asset_token, meta)
  end
end
