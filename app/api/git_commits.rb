# rubocop:disable Style/FrozenStringLiteralComment

class Api::GitCommits < Api::App
  areas_of_responsibility :git, :api

  include Api::App::GitActorHelpers

  # Get the contents of a commit object
  get "/repositories/:repository_id/git/commits/:commit_id" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/git#get-a-commit"
    control_access :get_commit,
      resource: repo = find_repo!,
      allow_integrations: true,
      allow_user_via_integration: true

    ensure_repo_content!(repo)

    begin
      commit = repo.commits.find(params[:commit_id])
    rescue GitRPC::InvalidObject,
           GitRPC::ObjectMissing,
           RepositoryObjectsCollection::InvalidObjectId
      deliver_error!(404)
    end
    deliver :commit_hash, commit, repo: repo, last_modified: calc_last_modified(commit)
  end

  # Create a new commit object
  post "/repositories/:repository_id/git/commits" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/git#create-a-commit"

    control_access :create_commit,
      resource: repo = find_repo!,
      allow_integrations: true,
      allow_user_via_integration: true
    ensure_repo_content!(repo)
    ensure_repo_writable!(repo)

    data = receive_with_schema("git-commit", "create-legacy")

    # message, tree are mandatory
    message = data["message"]
    tree    = sha_param!("tree", data)

    signature = data["signature"]

    # check tree
    begin
      if !(_tree_obj = repo.objects.read(tree, "tree"))
        deliver_error!(422, message: "Tree SHA is not an object")
      end

      rescue GitRPC::ObjectMissing
        deliver_error!(422, message: "Tree SHA does not exist")
      rescue GitRPC::InvalidObject
        deliver_error!(422, message: "Tree SHA is not a tree object")
    end

    # check parents
    parents = Array(data["parents"])
    if !repo.commits.exist?(parents)
      deliver_error!(422,
        message: "Parent SHA does not exist or is not a commit object")
    end

    info = {
      "tree"    => tree,
      "message" => message,
    }

    default_time = Time.zone.now.iso8601
    default_actor = GitActor.new(
      name: current_user.git_author_name,
      email: current_user.git_author_email,
      time: default_time,
    )
    web_committer = GitActor.new(
      name: GitHub.web_committer_name,
      email: GitHub.web_committer_email,
      time: default_time,
    )

    # Determine whether the commit should be automatically signed by GitHub's
    # web committer.
    #
    # If the content creator is a bot and neither the committer nor the author
    # are specified, we can sign the commit with confidence. We're choosing
    # (for now) not to sign bot-created commits authored on behalf of a user.
    sign_commit = current_user.bot? &&
      current_user.commit_signing_enabled? &&
      data["author"].nil? &&
      data["committer"].nil? &&
      signature.nil?

    author = if data["author"]
      git_actor!(data, key: "author", default_time: default_time)
    else
      default_actor
    end
    info["author"] = author.to_gitrpc_hash

    committer = if sign_commit
      web_committer
    elsif data["committer"]
      git_actor!(data, key: "committer", default_time: default_time)
    else
      author
    end
    info["committer"] = committer.to_gitrpc_hash

    begin
      sha = if sign_commit
        repo.rpc.create_tree_changes(parents, info, nil, false) do |body|
          repo.sign_commit(body)
        end
      else
        repo.rpc.create_tree_changes(parents, info, nil, false, signature)
      end

      commit = repo.commits.find(sha)
    rescue GitRPC::InvalidObject, GitRPC::ObjectMissing
      deliver_error!(404)
    rescue GitRPC::BadGitmodules, GitRPC::SymlinkDisallowed => e
      deliver_error!(422, message: e.to_s)
    end

    deliver :commit_hash, commit, repo: repo, status: 201
  end

end
