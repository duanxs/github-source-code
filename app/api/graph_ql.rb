# rubocop:disable Style/FrozenStringLiteralComment

class Api::GraphQL < Api::App
  PATH_REGEX = /\A(?:\/api)?\/graphql\/?\z/i.freeze

  # Don't rate limit the GraphQL API the same way we do for other
  # endpoints as it requires much different logic and is based on query
  # complexity instead of # of calls.
  rate_limit_as nil

  class InvalidInternalGraphQLToken < StandardError; end
  class ExternalIPRequest < StandardError; end
  class InternalIPAuthentication < StandardError; end

  before { ensure_user_logged_in! }

  after do
    update_logs_with_rate_limits
    update_hydro_with_rate_limits
    set_rate_limit_headers
  end

  post "/graphql" do
    @route_owner = "@github/ecosystem-api"
    @documentation_url = Platform::NotDocumentedBecause::IGNORED # GraphQL relies on the `default_documentation_url` not the ivar.
    cache_control "no-cache"
    response = Platform.execute(query, target: target, context: context, variables: variables, operation_name: operation_name, raise_exceptions: false, request_env: env)

    # Add graphql_errors to the `hydro_context` so they get instrumented in the Request event.
    hydro_context[:graphql_errors] = response.instrumenter.serialized_errors_for_instrumentation

    push_filtered_request_body_to_context(response.scrubbed_query_string) if response.success?
    deliver_raw(response)
  end

  get "/graphql" do
    @route_owner = "@github/ecosystem-api"
    @documentation_url = Platform::NotDocumentedBecause::IGNORED # GraphQL relies on the `default_documentation_url` not the ivar.
    if medias.api_param?(:idl)
      mask = Platform::SchemaRuntimeMask.new(target)
      result = Platform::Schema.to_definition(except: mask, context: context)
      deliver_raw({ data: result })
    else # Platform::Schema.to_json expects and invokes the instrumentation, which requires specific context keys to be set
      result = Platform.execute(GraphQL::Introspection::INTROSPECTION_QUERY, target: target, context: context, raise_exceptions: false, request_env: env)
      deliver_raw(result)
    end
  end

  def default_documentation_url
    GitHub.developer_help_url + "/graphql"
  end

  private

  def set_rate_limit_headers
    cost_limiter.rate_limit.set_headers(response.headers)
  end

  def update_logs_with_rate_limits
    cost_limiter.rate_limit.update_logs(log_data)
  end

  def update_hydro_with_rate_limits
    cost_limiter.rate_limit.update_hydro(hydro_context)
  end

  # Private: Determines if a specific route should be rate limited by the
  # built in rate limit dependency.
  #
  # In this case we return false and opt to handle it ourselves so clients can
  # query their rateLimit through GraphQL even when they may already be at their
  # rate limit.
  #
  # For that to work we need to go through the GraphQL query anaylzers and move
  # the rate limit enforcement to a GraphQL error instead.
  #
  # Returns Boolean.
  def rate_limited_route?
    false
  end

  def skip_rate_limit
    raise NotImplementedError, "Can't skip rate limits on GraphQL endpoint"
  end

  def query
    request_params["query"]
  end

  def variables
    request_params["variables"]
  end

  def operation_name
    request_params["operationName"]
  end

  def requested_schema
    request_params["schema"]
  end

  def cost_limiter
    @cost_limiter ||= Platform::CostLimiter.new(
      Api::RateLimitConfiguration.for(Api::RateLimitConfiguration::GRAPHQL_FAMILY, self)
    )
  end

  def service_requesting_internal_schema?
    if request.env["HTTP_GRAPHQL_SCHEMA"] == "internal"
      return false if current_integration && !current_integration.github_owned?

      request_token = request.env["HTTP_GITHUB_INTERNAL_GRAPHQL_TOKEN"]
      valid_token = request_token.present? &&
        GitHub.valid_graphql_service_token?(request_token)
      if valid_token
        return true
      else
        err = InvalidInternalGraphQLToken.new("Internal GraphQL schema service request made without a valid token")
        Failbot.report_trace(err)
      end

      if internal_ip_request?
        if GitHub.remote_ip_restrictions_enabled?
          err = InternalIPAuthentication.new("Internal GraphQL schema service request relying solely on internal IP authentication")
          extra = {
            current_user_id: current_user.try(:id),
            site_admin: current_user.try(:site_admin?),
            site_admin_ivar: current_user.try(:instance_variable_get, "@site_admin"),
            site_admin_without_two_factor_checK: current_user.try(:site_admin_without_two_factor_check?),
            require_employee_for_site_admin: GitHub.require_employee_for_site_admin?,
            employee: current_user.try(:employee?),
            gh_role: current_user.try(:gh_role),
            two_factor_authentication_enabled: current_user.try(:two_factor_authentication_enabled?),
            two_factor_credential_present: current_user.try(:two_factor_credential).present?,
            require_two_factor_for_site_admin: GitHub.require_two_factor_for_site_admin?,
            site_admin_pat: site_admin_pat?,
            github_oauth_app: github_oauth_app?,
            granted_oauth_scopes: context[:granted_oauth_scopes],
            access_id: @oauth.try(:id),
            access_scopes: @oauth.try(:scopes),
            personal_access_token: @oauth.try(:personal_access_token?),
            logged_in: logged_in?,
          }
          Failbot.report_trace(err, extra)
        end
        # Because of some bug with X-Forwarded-For in the kube proxy layers,
        # internal_ip_request? will return true for public requests to a lab.
        # As a simple fix, only allow internal_ip_requests if we are not on a lab.
        if !GitHub.dynamic_lab?
          return true
        end
      end
    end

    false
  end

  def site_admin_requesting_internal_schema?
    (request.env["HTTP_GRAPHQL_SCHEMA"] == "internal" || requested_schema == "internal") && site_admin?
  end

  def site_admin_pat?
    @oauth && @oauth.personal_access_token? && context[:granted_oauth_scopes]&.include?("site_admin")
  end

  def github_oauth_app?
    return false if current_integration
    return false unless current_app
    current_app.owner == Organization.find_by_login("github")
  end

  def site_admin?
    return false unless logged_in?

    if current_user.site_admin? && (site_admin_pat? || github_oauth_app?)
      return true
    end

    false
  end

  def target
    if site_admin_requesting_internal_schema? || service_requesting_internal_schema?
      # Collect data on external callers calling into the internal schema
      if GitHub.remote_ip_restrictions_enabled? && !internal_ip_request?
        err = ExternalIPRequest.new("Internal GraphQL schema request made from an external IP")
        Failbot.report_trace(err)
      end
      :internal
    else
      :public
    end
  end

  def context
    base_context = platform_context

    if (base_context[:viewer].employee? || base_context[:viewer].site_admin?) && request.env["HTTP_GRAPHQL_TRACE"]
      performance_trace = true
      # Add a detailed trace if requested
      if request.env["HTTP_GRAPHQL_TRACE_OBJECTS"]
        performance_trace_field_profile_type = :allocations
        performance_trace_field_profile_path = request.env["HTTP_GRAPHQL_TRACE_OBJECTS"]
      elsif request.env["HTTP_GRAPHQL_TRACE_LINES"]
        performance_trace_field_profile_type = :lines
        performance_trace_field_profile_path = request.env["HTTP_GRAPHQL_TRACE_LINES"]
      else
        performance_trace_field_profile_type = nil
        performance_trace_field_profile_path = nil
      end
    else
      performance_trace = false
      performance_trace_field_profile_type = nil
      performance_trace_field_profile_path = nil
    end

    base_context.merge(
      origin: origin,
      schema_previews: schema_previews,
      cost_limiter: cost_limiter,
      # Apply performance tracing if a site admin requests it.
      performance_trace: performance_trace,
      performance_trace_field_profile_type: performance_trace_field_profile_type,
      performance_trace_field_profile_path: performance_trace_field_profile_path,
    )
  end

  def request_params
    @request_params ||= receive(Hash, required: false) || {}
  end

  def origin
    :api
  end

  # Push the filtered request body as a Hash into
  # GitHub.context[:filtered_request_body].
  #
  # Returns nothing.
  def push_filtered_request_body_to_context(scrubbed_query)
    # Filter request body if needed and store for later use
    filtered_request_body = {}
    filtered_request_body[:query] = scrubbed_query if scrubbed_query
    filtered_request_body[:operationName] = operation_name if operation_name

    if filtered_request_body.keys.any?
      GitHub.context.push(filtered_request_body: filtered_request_body)
      Audit.context.push(filtered_request_body: filtered_request_body)
    end
  end
end
