# rubocop:disable Style/FrozenStringLiteralComment

require "octolytics/pond"

class Api::Traffic < Api::App
  areas_of_responsibility :analytics, :api, :traffic_graphs
  VALID_TIME_AGGREGATIONS = ["day", "week"]

  before do
    deliver_error!(404) unless GitHub.traffic_graphs_enabled?
  end

  error Octolytics::ServerError, Api::Serializer::InvalidTimestampError do
    Failbot.report(env["sinatra.error"])
    deliver_error! 503, message: "The Traffic API is temporarily unavailable"
  end

  get "/repositories/:repository_id/traffic/popular/referrers" do
    @route_owner = "@github/data-engineering"
    @documentation_url = "/rest/reference/repos#get-top-referral-sources"
    repo = find_repo!
    set_error_message(repo)
    control_access :repo_traffic, resource: repo, forbid: repo.public?, allow_integrations: true, allow_user_via_integration: true

    data = pond_client.referrers(repo.id).data["data"].first(10)
    deliver :traffic_referrers_hash, data, repo: repo
  end

  get "/repositories/:repository_id/traffic/popular/paths" do
    @route_owner = "@github/data-engineering"
    @documentation_url = "/rest/reference/repos#get-top-referral-paths"
    repo = find_repo!
    set_error_message(repo)
    control_access :repo_traffic, resource: repo, forbid: repo.public?, allow_integrations: true, allow_user_via_integration: true

    data = pond_client.content(repo.id).data["data"].first(10)
    deliver :traffic_contents_hash, data, repo: repo
  end

  get "/repositories/:repository_id/traffic/clones" do
    @route_owner = "@github/data-engineering"
    @documentation_url = "/rest/reference/repos#get-repository-clones"
    repo = find_repo!
    set_error_message(repo)
    control_access :repo_traffic, resource: repo, forbid: repo.public?, allow_integrations: true, allow_user_via_integration: true

    period = get_period(params, "get-repository-clones")
    data = pond_client.counts(repo.id, "clone", period).data["data"]
    deliver :traffic_clones_hash, data, repo: repo
  end

  get "/repositories/:repository_id/traffic/views" do
    @route_owner = "@github/data-engineering"
    @documentation_url = "/rest/reference/repos#get-page-views"
    repo = find_repo!
    set_error_message(repo)
    control_access :repo_traffic, resource: repo, forbid: repo.public?, allow_integrations: true, allow_user_via_integration: true

    period = get_period(params, "get-page-views")
    data = pond_client.counts(repo.id, "view", period).data["data"]
    deliver :traffic_views_hash, data, repo: repo
  end

  private

  def pond_client
    @pond_client ||= Octolytics::Pond.new(secret: GitHub.pond_shared_secret)
  end

 def set_error_message(repo)
   if access_allowed?(:get_repo, resource: repo, allow_integrations: true, allow_user_via_integration: true)
     set_forbidden_message "Must have push access to repository"
   end
 end

  def get_period(params, endpoint_name)
    period = params[:per].presence || "day"
    unless period.in?(VALID_TIME_AGGREGATIONS)
      deliver_error! 422,
      message: "Invalid time aggregation",
      documentation_url: "/rest/reference/repos##{endpoint_name}"
    end
    period
  end
end
