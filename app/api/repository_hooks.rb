# rubocop:disable Style/FrozenStringLiteralComment

class Api::RepositoryHooks < Api::App
  areas_of_responsibility :api, :webhook

  statsd_tag_actions "/repositories/:repository_id/hooks"
  statsd_tag_actions "/repositories/:repository_id/hooks/:hook_id"

  # List webhooks for a repo
  get "/repositories/:repository_id/hooks" do
    @route_owner = "@github/ecosystem-events"
    @documentation_url = "/rest/reference/repos#list-repository-webhooks"
    control_access :list_repo_hooks, repo: repo = find_repo!, allow_integrations: true, allow_user_via_integration: true
    hooks = Hook::StatusLoader.load_statuses(hook_records: paginate_rel(repo.hooks), parent: repo)
    GitHub::PrefillAssociations.for_hooks(hooks)
    # `hooks` is an Array, not a Relation, so set the collection size manually
    # (Usually `deliver(...)` would infer pagination info from the given collection.)
    paginator.collection_size = repo.hooks.count
    deliver :repo_hook_hash, hooks, repo: repo
  end

  # Get a single webhook
  get "/repositories/:repository_id/hooks/:hook_id" do
    @route_owner = "@github/ecosystem-events"
    @documentation_url = "/rest/reference/repos#get-a-repository-webhook"
    control_access :read_repo_hook, repo: repo = find_repo!, allow_integrations: true, allow_user_via_integration: true

    hook = Hook::StatusLoader.load_status(repo.hooks.find_by_id(int_id_param!(key: :hook_id)))
    GitHub::PrefillAssociations.for_hooks([hook])
    deliver :repo_hook_hash, hook, full: true, repo: repo, last_modified: calc_last_modified(hook)
  end

  # Get a single webhook's config
  get "/repositories/:repository_id/hooks/:hook_id/config" do
    @route_owner = "@github/ecosystem-events"
    @documentation_url = Platform::NotDocumentedBecause::DEPRECATED
    control_access :read_repo_hook, repo: repo = find_repo!, allow_integrations: true, allow_user_via_integration: true

    if hook = repo.hooks.find_by_id(int_id_param!(key: :hook_id))
      deliver_raw hook.config, last_modified: calc_last_modified(hook)
    else
      deliver_error! 404
    end
  end

  # trigger a test hook call (singular, deprecated)
  #
  # DEPRECATED: Will be removed in API v4.
  post "/repositories/:repository_id/hooks/:hook_id/test" do
    @route_owner = "@github/ecosystem-events"
    @documentation_url = Platform::NotDocumentedBecause::DEPRECATED
    control_access :test_repo_hook, repo: repo = find_repo!, allow_integrations: false, allow_user_via_integration: false

    if hook = repo.hooks.find_by_id(int_id_param!(key: :hook_id))
      check_services_delivery!(hook)
      repo.test_service(hook)
      deliver_empty(status: 204)
    else
      deliver_error! 404
    end
  end

  # trigger a test hook call
  post "/repositories/:repository_id/hooks/:hook_id/tests" do
    @route_owner = "@github/ecosystem-events"
    receive_with_schema("hook", "test")

    @documentation_url = "/rest/reference/repos#test-the-push-repository-webhook"
    control_access :test_repo_hook, repo: repo = find_repo!, allow_integrations: true, allow_user_via_integration: true

    if hook = repo.hooks.find_by_id(int_id_param!(key: :hook_id))
      check_services_delivery!(hook)
      repo.test_service(hook)
      deliver_empty(status: 204)
    else
      deliver_error! 404
    end
  end

  # Ping the hook
  post "/repositories/:repository_id/hooks/:hook_id/pings" do
    @route_owner = "@github/ecosystem-events"
    receive_with_schema("hook", "ping")

    @documentation_url = "/rest/reference/repos#ping-a-repository-webhook"
    repo = find_repo!
    @accepted_scopes << "read:repo_hook"
    control_access :test_repo_hook, repo: repo, allow_integrations: true, allow_user_via_integration: true

    if hook = repo.hooks.find_by_id(int_id_param!(key: :hook_id))
      check_services_delivery!(hook)
      hook.ping
      deliver_empty(status: 204)
    else
      deliver_error! 404
    end
  end

  # Create a new webhook
  post "/repositories/:repository_id/hooks" do
    @route_owner = "@github/ecosystem-events"
    @documentation_url = "/rest/reference/repos#create-a-repository-webhook"
    control_access :create_repo_hook, repo: repo = find_repo!, allow_integrations: true, allow_user_via_integration: true
    authorize_content(repo, :create)

    data = receive_with_schema("hook", "create-legacy")
    name = data["name"] || "web"

    unless name && find_service(name)
      deliver_error! 422,
        errors: [api_error(:Hook, :name, :invalid, value: name)],
        documentation_url: @documentation_url
    end

    is_webhook = (name =~ /\Aweb\z/i)
    unless is_webhook
      deliver_error! 422,
        message: <<~MSG
        As of #{services_date(:adding)}, GitHub Services can no longer be added. Please see the blog post for details: #{GitHub.developer_help_url(skip_enterprise: true)}/changes/2018-04-25-github-services-deprecation
        You can use the "Replacing GitHub Services" guide to help you update your services to webhooks: #{GitHub.developer_help_url}/v3/guides/replacing-github-services
        MSG
    end

    hook = is_webhook ?
      repo.hooks.build(name: name) :
      repo.hooks.where(name: name).first_or_initialize(name: name)

    hook.track_creator(current_user)
    hook.events = Array(data["events"])
    hook.add_events("push") if hook.events.blank?

    is_new_hook = hook.new_record?
    is_active = (data["active"] != false)
    if hook && hook.configure_with(is_active, data["config"])
      hook = hook.reload
      GitHub::PrefillAssociations.for_hooks([hook])
      deliver :repo_hook_hash, hook, status: is_new_hook ? 201 : 200,
        full: true, repo: repo
    else
      deliver_error 422,
        errors: hook.errors,
        documentation_url: @documentation_url
    end
  end

  # Update a webhook
  verbs :post, :patch, "/repositories/:repository_id/hooks/:hook_id" do
    @route_owner = "@github/ecosystem-events"
    @documentation_url = "/rest/reference/repos#update-a-repository-webhook"
    control_access :update_repo_hook, repo: repo = find_repo!, allow_integrations: true, allow_user_via_integration: true

    hook = Hook::StatusLoader.load_status(repo.hooks.find_by_id(int_id_param!(key: :hook_id)))
    deliver_error! 404 unless hook
    check_services_delivery!(hook)

    data = receive_with_schema("hook", "update-legacy")
    attributes = attr(data, :active, :config)
    unless (active = attributes["active"]).nil?
      hook.active = active
    end
    if (config = attributes["config"]).present?
      hook.config = config
    end
    if (events = Array(data["events"])).present?
      hook.events = events
    elsif (events = Array(data["add_events"])).present?
      hook.add_events(events)
    elsif (events = Array(data["remove_events"])).present?
      hook.remove_events(events)
    end

    if hook.save
      GitHub::PrefillAssociations.for_hooks([hook])
      deliver :repo_hook_hash, hook
    else
      deliver_error 422,
        errors: hook.errors,
        documentation_url: @documentation_url
    end
  end

  # Replace a webhook's config
  put "/repositories/:repository_id/hooks/:hook_id/config" do
    @route_owner = "@github/ecosystem-events"
    @documentation_url = Platform::NotDocumentedBecause::DEPRECATED
    control_access :update_repo_hook, repo: repo = find_repo!, allow_integrations: true, allow_user_via_integration: true

    hook = repo.hooks.find_by_id(int_id_param!(key: :hook_id))
    deliver_error! 404 unless hook
    check_services_delivery!(hook)

    data = receive(Hash)
    hook.config = data

    if hook.save
      deliver_raw hook.config
    else
      deliver_error errors: hook.errors
    end
  end

  # Update a service hook's config
  verbs :patch, :post, "/repositories/:repository_id/hooks/:hook_id/config" do
    @route_owner = "@github/ecosystem-events"
    @documentation_url = Platform::NotDocumentedBecause::DEPRECATED
    control_access :update_repo_hook, repo: repo = find_repo!, allow_integrations: true, allow_user_via_integration: true

    hook = repo.hooks.find_by_id(int_id_param!(key: :hook_id))
    deliver_error! 404 unless hook
    check_services_delivery!(hook)

    data = receive(Hash)
    hook.partial_config = data

    if hook.save
      deliver_raw hook.config
    else
      deliver_error errors: hook.errors
    end
  end

  # Delete a webhook
  delete "/repositories/:repository_id/hooks/:hook_id" do
    @route_owner = "@github/ecosystem-events"

    # Introducing strict validation of the hook.delete
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("hook", "delete", skip_validation: true)

    @documentation_url = "/rest/reference/repos#delete-a-repository-webhook"
    control_access :delete_repo_hook, repo: repo = find_repo!, allow_integrations: true, allow_user_via_integration: true

    hook = repo.hooks.find_by_id(int_id_param!(key: :hook_id))
    if hook && GitHub::SchemaDomain.allowing_cross_domain_transactions { hook.destroy }
      deliver_empty(status: 204)
    else
      deliver_error 404
    end
  end

  # Fetch the deliveries of a specific hook
  get "/repositories/:repository_id/hooks/:hook_id/deliveries" do
    @route_owner = "@github/ecosystem-events"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    require_preview(:hook_deliveries_api)
    control_access :list_repo_hooks, repo: repo = find_repo!, allow_integrations: true, allow_user_via_integration: true

    hook = repo.hooks.find_by_id(int_id_param!(key: :hook_id))

    deliver_error!(404) unless hook

    safe_params = params.slice(:since, :until, :per_page, :status, :status_codes, :events, :redelivery, :guid, :cursor)
    status, body = Hookshot::Client.for_parent(hook.hookshot_parent_id).deliveries_for_hook(hook.id, safe_params.except(:per_page).merge(limit: per_page(safe_params["per_page"])))

    case status
    when 200
      build_cursor_based_links(body["page_info"], per_page(safe_params["per_page"]))
      deliver_raw body["deliveries"]
    when 400
      deliver_error! status, message: body["message"]
    else
      deliver_error! 500, message: "Something went wrong."
    end
  end

  # Get a single delivery of a given webhook
  get "/repositories/:repository_id/hooks/:hook_id/deliveries/:delivery_id" do
    @route_owner = "@github/ecosystem-events"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    require_preview(:hook_deliveries_api)
    control_access :read_repo_hook, repo: repo = find_repo!, allow_integrations: true, allow_user_via_integration: true

    hook = repo.hooks.find_by_id(int_id_param!(key: :hook_id))

    deliver_error!(404) unless hook
    safe_params = { include_payload: !!params[:full] }
    status, body = Hookshot::Client.for_parent(hook.hookshot_parent_id).delivery_for_hook(params[:delivery_id], hook.id, safe_params)

    case status
    when 200
      deliver_raw body
    when 400
      deliver_error! status, message: body["message"]
    else
      deliver_error! 500, message: "Something went wrong."
    end
  end

  # Trigger a new delivery attempt
  post "/repositories/:repository_id/hooks/:hook_id/deliveries/:delivery_id/attempts" do
    @route_owner = "@github/ecosystem-events"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    require_preview(:hook_deliveries_api)
    repo = find_repo!
    control_access :update_repo_hook, repo: repo, allow_integrations: true, allow_user_via_integration: true
    hook = repo.hooks.find_by_id(int_id_param!(key: :hook_id))
    deliver_error!(404) unless hook

    hook_client  = Hookshot::Client.for_parent(hook.hookshot_parent_id)
    status, body = hook_client.create_redelivery_attempt(int_id_param!(key: :delivery_id), hook.id, hook.config_attributes)

    case status
    when 202
      deliver_empty status: 202
    when 400
      deliver_error! status, message: body["message"]
    else
      deliver_error! 500, message: "Something went wrong."
    end
  end

  private

  def per_page(limit)
    return DEFAULT_PER_PAGE unless limit
    [limit.to_i, MAX_PER_PAGE].min
  end

  def check_services_delivery!(hook = nil)
    return if hook&.webhook?

    deliver_error! 410,
      message: <<~MSG
      As of #{services_date(:delivery)}, GitHub Services can no longer be modified. Please see the blog post for details: #{GitHub.developer_help_url(skip_enterprise: true)}/changes/2018-04-25-github-services-deprecation
      You can use the "Replacing GitHub Services" guide to help you update your services to webhooks: #{GitHub.developer_help_url}/v3/guides/replacing-github-services
      MSG
  end

  def services_date(type)
    if GitHub.enterprise?
      type == :adding ? "2.17" : "2.20"
    else
      type == :adding ? "October 2018" : "January 2019"
    end
  end

  def find_service(name)
    Hook::Service.options_for(name.to_sym).presence
  end

  # TODO: Make Egress properly report accepted scopes and remove this overload.
  def find_repo!
    repo = super

    @accepted_scopes = %w(admin:repo_hook repo)
    @accepted_scopes << "read:repo_hook" if request.get? || request.head?
    @accepted_scopes << "write:repo_hook" unless request.delete?
    @accepted_scopes << "public_repo" if repo && repo.public?

    repo
  end

  def authorize_content(authorizable, operation = :create)
    authorization = ContentAuthorizer.authorize(current_user, :hook, operation, repo: authorizable)
    deliver_content_authorization_denied!(authorization) if authorization.failed?
  end
end
