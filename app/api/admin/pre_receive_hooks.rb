# rubocop:disable Style/FrozenStringLiteralComment

class Api::Admin::PreReceiveHooks < Api::Admin
  areas_of_responsibility :enterprise_only, :api

  before do
    deliver_error!(404) unless GitHub.enterprise_only_api_enabled?
  end

  get "/admin/pre-receive-hooks" do
    @route_owner = "@github/admin-experience"
    @documentation_url = "/user/rest/reference/enterprise-admin#list-pre-receive-hooks"

    targets = paginate_rel(sort(PreReceiveHookTarget.global))
    GitHub::PrefillAssociations.for_pre_receive_hook_targets targets
    deliver :pre_receive_hook_hash, targets
  end

  get "/admin/pre-receive-hooks/:pre_receive_hook_id" do
    @route_owner = "@github/admin-experience"
    @documentation_url = "/user/rest/reference/enterprise-admin#get-a-pre-receive-hook"

    target = find_global_pre_receive_hook_target!(param_name: :pre_receive_hook_id)
    GitHub::PrefillAssociations.for_pre_receive_hook_targets [target]
    deliver :pre_receive_hook_hash, target
  end

  post "/admin/pre-receive-hooks" do
    @route_owner = "@github/admin-experience"
    @documentation_url = "/user/rest/reference/enterprise-admin#create-a-pre-receive-hook"

    data = receive_with_schema("pre-receive-hook", "create")
    accepted_attributes = build_target_attributes(data)
    target = PreReceiveHookTarget.create accepted_attributes
    deliver_error! 422, errors: target.errors if target.errors.present?
    GitHub::PrefillAssociations.for_pre_receive_hook_targets [target]
    deliver :pre_receive_hook_hash, target, status: 201
  end

  verbs :patch, :post, "/admin/pre-receive-hooks/:pre_receive_hook_id" do
    @route_owner = "@github/admin-experience"
    @documentation_url = "/user/rest/reference/enterprise-admin#update-a-pre-receive-hook"

    data = receive_with_schema("pre-receive-hook", "update-for-ghe")
    target = find_global_pre_receive_hook_target!(param_name: :pre_receive_hook_id)
    accepted_attributes = build_target_attributes(data)
    target.update accepted_attributes
    deliver_error! 422, errors: target.errors if target.errors.present?
    GitHub::PrefillAssociations.for_pre_receive_hook_targets [target]
    deliver :pre_receive_hook_hash, target, status: 200
  end

  delete "/admin/pre-receive-hooks/:pre_receive_hook_id" do
    @route_owner = "@github/admin-experience"
    @documentation_url = "/user/rest/reference/enterprise-admin#delete-a-pre-receive-hook"

    find_pre_receive_hook!(param_name: :pre_receive_hook_id).destroy
    deliver_empty status: 204
  end

  private

  def build_target_attributes(data)
    data = {"enforcement" => "disabled",
            "allow_downstream_configuration" => false,
            "hookable" => GitHub.global_business}.merge(data)
    # If a script_repository is set, look it up by nwo and set the repository_id from that.
    if data["script_repository"].present?
      nwo = data["script_repository"]["full_name"]
      repository = Repository.nwo(nwo)
      if repository
        data["repository_id"] = repository.id
      else
        deliver_error! 422, message: "Cannot use specified script_repository.",
                       errors: [api_error("PreReceiveHook", :script_repository, :invalid, value: nwo)]
      end
    end
    # Remap environment_id if environment is present in the received data
    data["environment_id"] = data["environment"]["id"] if data["environment"].present?
    data["hook_attributes"] = attr(data, :name, :script, :repository_id, :environment_id)
    attr(data, :enforcement, :allow_downstream_configuration, :hookable, :hook_attributes)
  end

  def sort(scope)
    scope.sorted_by("hook.#{params[:sort] || "id"}", params[:direction] || "asc")
  end
end
