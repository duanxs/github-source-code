# rubocop:disable Style/FrozenStringLiteralComment

module Api
  # Represents the collection of media types that a request accepts, as
  # specified via the `Accept` headers.
  class AcceptedMediaTypes
    attr_reader :media_type_strings

    SINATRA_DEFAULT_MEDIA_TYPE = "*/*"

    # Public: Initialize an AcceptedMediaTypes.
    #
    # media_type_strings - String or array of media type Strings specified via the
    #                      `Accept` headers in a request.
    def initialize(media_type_strings, request_path = nil)
      @request_path = request_path
      @media_type_strings = media_type_strings.is_a?(String) ? media_type_strings.split(",") : Array(media_type_strings)
    end

    # Public: Determine whether the media types explicitly or implicitly accept
    # the given version.
    #
    # version - String API version name (e.g., 'beta', 'v3').
    #
    # Returns true if the media types explicitly reference the given version.
    #   Returns true if the given version is the default version, and the media
    #   types to reference some other (non-default) version. Returns false
    #   otherwise.
    def accepts_version?(version)
      return true if explicitly_accepts_version?(version)

      default_version?(version) && implicitly_accepts_default_version?
    end

    def contains_preview?
      media_type_strings.any? do |media_type_string|
        media_type = Api::MediaType.new(media_type_string)
        media_type.preview_version?
      end
    end

    # Public: Determine whether the media types explicitly reference the given
    # version.
    #
    # version - String API version name (e.g., 'beta', 'v3').
    #
    # Returns true if at least one of the media types explicitly references the
    #   given version. Returns false otherwise.
    def explicitly_accepts_version?(version)
      media_type_strings.any? do |media_type_string|
        media_type = Api::MediaType.new(media_type_string)
        media_type.explicit_api_version?(version)
      end
    end

    # Public: Determine whether the media types implicitly accept the default
    # version. If the media types fail to reference some other (non-default)
    # version, then the request implicitly gets the default version.
    #
    # Returns a Boolean.
    def implicitly_accepts_default_version?
      contains_preview? || Api::MediaType.non_default_versions.none? { |v| explicitly_accepts_version?(v) }
    end

    def api
      api_media_types.find { |m| m.api? }
    end

    def version(version)
      api_media_types.find { |m| m.api_version?(version) }
    end

    def versions
      api_media_types.map(&:version).compact
    end

    def api_versions
      api_media_types.select { |m| m.respond_to?(:api_version) }.map(&:api_version)
    end

    # application/json and Sinatra's defaults are not GitHub media types, and don't have `to_http_header`
    def to_http_header
      api_media_types.select { |m| m.respond_to?(:to_http_header) }.map(&:to_http_header).join(", ")
    end

    def to_api_version
      api_versions.join(", ")
    end

    def map(&block)
      api_media_types.map(&block)
    end

    def reject(&block)
      api_media_types.reject(&block)
    end

    def empty?
      api_media_types.empty?
    end

    def acceptable?
      !empty? && api
    end

    def version?(version)
      api_media_types.any? { |m| m.version == version }
    end

    def api_preview?(name)
      preview = REST::Previews.get(name)
      api_media_types.any? { |m| m.api_version?(preview.media_version) }
    end

    def api_version?(version)
      api_media_types.any? { |m| m.api_version?(version) }
    end

    def api_param?(param)
      api_media_types.any? { |m| m.api_param?(param) }
    end

    def preview_version?
      api_media_types.any? { |m| m.preview_version? }
    end

    def json?
      api_media_types.any? { |m| m.json? }
    end

    def v3?
      api_media_types.all? { |m|  api_version?(:v3) }
    end

    def api_params
      api_media_types.map { |m| m.api_params }
    end

    def sub_type
      v3&.sub_type
    end

    def to_s
      @media_type_strings.join(",")
    end

    private

    def v3
      api_media_types.find { |m| m.v3? }
    end

    # Internal: Determine whether the given version is the current default API
    # version.
    #
    # version - String or Symbol version name.
    #
    # Returns a Boolean.
    def default_version?(version)
      version.to_sym == Api::MediaType::DefaultVersion
    end

    # Internal: Get the GitHub-accepted media types from the list of accepted
    # media types.
    #
    # Returns an array of Api::MediaType.
    def api_media_types
      return @api_media_types if defined?(@api_media_types)
      @api_media_types = []
      api_version = Api::MediaType.identify_api_version(@request_path)
      media_type_strings.each do |media_type_string|
        parsed = Api::MediaType.new(media_type_string.strip, api_version)
        parsed.preview_version?
        if @api_media_types.empty? || parsed.preview_version? || (parsed.api? && !@api_media_types.any? { |m| m.api? })
          @api_media_types << parsed
        else
          # merge API params into last media type, per RFC
          @api_media_types.last.api_params.merge(parsed.api_params)
        end
      end

      if @api_media_types.empty? || @api_media_types.join(",") == SINATRA_DEFAULT_MEDIA_TYPE
        @api_media_types = [Api::MediaType.default(api_version)]
      else
        @api_media_types
      end
    end
  end
end
