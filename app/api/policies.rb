# rubocop:disable Style/FrozenStringLiteralComment

# Internal API endpoints for creating S3 Policies.
# Used exclusively by github/alambic_server, and not meant for public
# consumption.
class Api::Policies < Api::App
  areas_of_responsibility :data_infrastructure, :api

  post "/repositories/:repository_id/releases/:release_id/assets/policies" do
    @route_owner = "@github/data-infrastructure"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL

    deliver_error!(404) unless logged_in?
    if GitHub.storage_cluster_enabled?
      meta = {}
      attributes = params.merge(receive(Hash))
      ReleaseAsset.uploadable_policy_attributes.each do |key|
        meta[key] = attributes[key.to_s]
      end

      blob = ::Storage::Blob.new(size: meta[:size])
      uploadable = ReleaseAsset.storage_new(current_user, blob, meta)
      unless uploadable.valid?
        deliver_error! 422, errors: uploadable.errors
      end
      policy = uploadable.storage_policy(actor: current_user)
      deliver! :policy_hash, policy, status: 201
    end

    creator = ::Storage.policy_creator.for(:releases)
    deliver_error!(404) unless creator

    repo = Repository.find_by_id(params[:repository_id].to_i)
    rel = repo.releases.find_by_id(params[:release_id].to_i)
    deliver_error!(404) unless creator.access_allowed?(current_user, :edit_release, repo: repo, resource: rel)

    data = attr(receive(Hash), *creator.attributes)
    params.each do |key, value|
      data[key.to_s] = value
    end

    uploadable = creator.create(current_user, data)
    if uploadable.valid?
      policy = uploadable.storage_policy(actor: current_user)
      deliver :policy_hash, policy, status: 201
    else
      deliver_error 422, errors: uploadable.errors
    end
  end

  post "/migrations/:migration_id/archive/policies" do
    @route_owner = "@github/data-infrastructure"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL

    deliver_error!(404) unless logged_in?
    if GitHub.storage_cluster_enabled?
      meta = {}
      attributes = params.merge(receive(Hash))
      MigrationFile.uploadable_policy_attributes.each do |key|
        meta[key] = attributes[key.to_s]
      end

      blob = ::Storage::Blob.new(size: meta[:size])
      uploadable = MigrationFile.storage_new(current_user, blob, meta)
      unless uploadable.valid?
        deliver_error! 422, errors: uploadable.errors
      end
      policy = uploadable.storage_policy(actor: current_user)
      deliver! :policy_hash, policy, status: 201
    end

    creator = ::Storage.policy_creator.for(:migration_files)
    deliver_error!(404) unless creator

    migration = ActiveRecord::Base.connected_to(role: :reading) { Migration.find_by_id(params[:migration_id].to_i) }
    owner = migration.try(:owner)
    deliver_error!(404) unless creator.access_allowed?(current_user, :write_migration, migration: migration, resource: owner)

    data = attr(receive(Hash), *creator.attributes)
    params.each do |key, value|
      data[key.to_s] = value
    end

    uploadable = creator.create(current_user, data)
    if uploadable.valid?
      policy = uploadable.storage_policy(actor: current_user)
      deliver :policy_hash, policy, status: 201
    else
      deliver_error 422, errors: uploadable.errors
    end
  end

  post "/businesses/:slug/user-accounts-uploads/policies" do
    @route_owner = "@github/admin-experience"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL

    deliver_error!(404) unless logged_in?

    business = ActiveRecord::Base.connected_to(role: :reading) do
      Business.where(slug: params[:slug]).first
    end

    if GitHub.storage_cluster_enabled?
      meta = {}
      attributes = params.merge(receive(Hash))
      EnterpriseInstallationUserAccountsUpload.uploadable_policy_attributes.each do |key|
        meta[key] = attributes[key.to_s]
      end
      meta[:business_id] = business.id

      blob = ::Storage::Blob.new(size: meta[:size])
      uploadable = EnterpriseInstallationUserAccountsUpload.storage_new(current_user, blob, meta)
      unless uploadable.valid?
        deliver_error! 422, errors: uploadable.errors
      end
      policy = uploadable.storage_policy(actor: current_user)
      deliver! :policy_hash, policy, status: 201
    end

    creator = ::Storage.policy_creator.for(:enterprise_installation_user_accounts_uploads)
    deliver_error!(404) unless creator

    deliver_error!(404) unless creator.access_allowed?(
      current_user, :write_business_enterprise_installation_user_accounts, resource: business
    )

    data = attr(receive(Hash), *creator.attributes)
    params.each do |key, value|
      data[key.to_s] = value
    end
    data[:business_id] = business.id

    uploadable = creator.create(current_user, data)
    if uploadable.valid?
      policy = uploadable.storage_policy(actor: current_user)
      deliver :policy_hash, policy, status: 201
    else
      deliver_error 422, errors: uploadable.errors
    end
  end

  private

  def site_admin?
    logged_in? && current_user.site_admin?
  end
end
