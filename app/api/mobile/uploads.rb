# frozen_string_literal: true

# Endpoints to support uploads from the mobile clients. Makes use of Alambic
# very much like UploadPoliciesController and UploadsController.
#
#   1. POST to /mobile/upload/policies
#        to create the policy document validating the file upload.
#   2. POST the file to be uploaded to the endpoint specified in the policy
#   3. PUT to asset_upload_url (returned in the POST in step 1)
#        to signal that the file upload completed successfully.
#
class Api::Mobile::Uploads < Api::App
  areas_of_responsibility :mobile, :api

  post "/mobile/upload/policy" do
    @route_owner = "@github/pe-mobile"
    # This is intentionally not released to the general API population
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED

    deliver_error! 404, message: "Not enabled for Enterprise" if GitHub.enterprise?

    control_access :mobile_assets_write_asset,
      user: current_user,
      resource: Platform::PublicResource.new,
      allow_integrations: false,
      allow_user_via_integration: false

    data = receive_with_schema("mobile-upload", "policy")

    model_name = "assets"
    if data["repository_id"].present? && !image?(data["content-type"])
      model_name = "repository-files"
    end

    uploader = ::Storage.policy_creator.for(model_name)

    ActiveRecord::Base.connected_to(role: :writing) do
      policy = uploader.create(current_user, data)
      deliver_policy(policy, model_name)
    end
  end

  put "/mobile/upload/assets/:asset_id" do
    @route_owner = "@github/pe-mobile"
    # This is intentionally not released to the general API population
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED

    deliver_error! 404, message: "Not enabled for Enterprise" if GitHub.enterprise?

    control_access :mobile_assets_write_asset,
      user: current_user,
      resource: Platform::PublicResource.new,
      allow_integrations: false,
      allow_user_via_integration: false

    _ = receive_with_schema("mobile-upload", "complete-user-asset")

    current_asset = UserAsset.find_by(id: params["asset_id"], user_id: current_user.id)

    deliver_error! 404, message: "Invalid asset" unless current_asset.present?

    deliver_response current_asset
  end

  put "/mobile/upload/repository-files/:file_id" do
    @route_owner = "@github/pe-mobile"
    # This is intentionally not released to the general API population
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED

    deliver_error! 404, message: "Not enabled for Enterprise" if GitHub.enterprise?

    control_access :mobile_assets_write_asset,
      user: current_user,
      resource: Platform::PublicResource.new,
      allow_integrations: false,
      allow_user_via_integration: false

    _ = receive_with_schema("mobile-upload", "complete-repo-file")

    current_asset = RepositoryFile.where(id: params["file_id"], uploader_id: current_user.id).first

    deliver_error! 404, message: "Invalid repository file" unless current_asset.present?

    if !current_asset.upload_access_allowed?(current_user)
      deliver_error! 404, message: "Access not allowed"
    end

    deliver_response current_asset
  end

  private

  def image?(content_type)
    ::Storage::Uploadable::CONTENT_TYPES[:images].include?(content_type)
  end

  def deliver_policy(uploadable, model_name)
    if uploadable.valid?
      policy = uploadable.storage_policy(actor: current_user)
      deliver! :mobile_policy_hash, policy, url: "/mobile/upload/#{model_name}/#{uploadable.id}", status: 201
    end

    if !uploadable.upload_access_allowed?(current_user)
      deliver_error! 404, message: "Access not allowed"
    end

    extra = {
      model_name: uploadable.class.name,
    }

    # Removed the extra messsages,  as a few of
    # these errors looked like they had usernames.
    # See RepositoryImage#uploader_access for an example.

    err = ActiveRecord::ActiveRecordError.new("Failed to create #{uploadable.class.name}")
    Failbot.report_user_error(err, extra)

    deliver_error! 422, errors: uploadable.errors
  end

  def deliver_response(asset)
    if asset.track_uploaded
      deliver :mobile_asset_hash, asset.storage_policy, status: 200
    else
      deliver_error! 400, errors: asset.errors.full_messages
    end
  end
end
