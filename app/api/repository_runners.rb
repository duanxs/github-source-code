# frozen_string_literal: true

class Api::RepositoryRunners < Api::App
  map_to_service :actions_runners

  # Create a registration token for repo-level runners
  post "/repositories/:repository_id/actions/runners/registration-token" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#create-a-registration-token-for-a-repository"

    repo = find_repo!

    control_access :write_admin_actions_repo,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    ensure_tenant!(repo)

    receive_with_schema("runner", "create-registration-token")

    unless repo.actions_app_installed?
      GitHub.dogstats.increment("actions.self_hosted_tenant_setup", tags: ["runner_type:repo"])

      repo.enable_actions_app
      SetupRepositoryForActionsJob.perform_later(repository: repo)
    end

    expires_at = 1.hour.from_now
    scope = repo.runner_registration_token_scope
    token = current_user.signed_auth_token(scope: scope, expires: expires_at)
    deliver_raw({ token: token, expires_at: expires_at }, status: 201)
  end

  # Get all runners
  get "/repositories/:repository_id/actions/runners" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#list-self-hosted-runners-for-a-repository"

    repo = find_repo!

    control_access :read_admin_actions,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    resp = GrpcHelper.rescue_from_grpc_errors("SelfHostedRunners") do
      GitHub::LaunchClient::SelfHostedRunners.list_runners(repo)
    end

    validate_listing!(resp&.value&.runners)

    # We map to an Array so pagination works
    runners = paginate_rel(resp&.value&.runners.map { |runner| runner })
    deliver :actions_runners_hash, { runners: runners, total_count: runners.total_entries, current_user: current_user }
  end

  # Create a remove token
  post "/repositories/:repository_id/actions/runners/remove-token" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#create-a-remove-token-for-a-repository"

    repo = find_repo!

    control_access :write_admin_actions_repo,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    receive_with_schema("runner", "create-remove-token")

    expires_at = 1.hour.from_now
    scope = repo.runner_registration_token_scope
    token = current_user.signed_auth_token(scope: scope, expires: expires_at)
    deliver_raw({ token: token, expires_at: expires_at }, status: 201)
  end

  get "/repositories/:repository_id/actions/runners/downloads" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#list-runner-applications-for-a-repository"

    repo = find_repo!

    control_access :read_admin_actions,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    resp = GrpcHelper.rescue_from_grpc_errors("SelfHostedRunners") do
      GitHub::LaunchClient::SelfHostedRunners.list_downloads(repo)
    end

    downloads = resp&.value&.downloads || []
    downloads_hashes = downloads.map do |download|
      download_hash(download)
    end
    deliver_raw(downloads_hashes)
  end

  # Get a runner
  get "/repositories/:repository_id/actions/runners/:runner_id" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#get-a-self-hosted-runner-for-a-repository"

    repo = find_repo!

    control_access :read_admin_actions,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    resp = GrpcHelper.rescue_from_grpc_errors("SelfHostedRunners") do
      GitHub::LaunchClient::SelfHostedRunners.get_runner(repo, params[:runner_id].to_i)
    end

    runner = resp&.value&.runner
    deliver_error! 404 unless runner

    deliver :actions_runner_hash, runner, { current_user: current_user }
  end

  # Delete a runner
  delete "/repositories/:repository_id/actions/runners/:runner_id" do
    @route_owner = "@github/c2c-actions-experience"
    @documentation_url = "/rest/reference/actions#delete-a-self-hosted-runner-from-a-repository"

    repo = find_repo!

    control_access :write_admin_actions_repo,
      resource: repo,
      user: current_user,
      forbid: repo.public?,
      allow_integrations: true,
      allow_user_via_integration: true,
      enforce_oauth_app_policy: true

    receive_with_schema("runner", "delete-runner")

    resp = GrpcHelper.rescue_from_grpc_errors("SelfHostedRunners") do
      GitHub::LaunchClient::SelfHostedRunners.delete_runner(repo, params[:runner_id].to_i, actor: @current_user)
    end

    if resp.call_succeeded?
      deliver_empty status: 204
    else
      deliver_error! 500, message: "Failed to delete the specified runner, it may be actively running a job"
    end
  end

  private

  def download_hash(download)
    { os: download.os, architecture: download.architecture, download_url: download.download_url, filename: download.filename }
  end

  def validate_listing!(result)
    unless result
      Failbot.report(StandardError.new("no response from list"), launch_selfhostedrunners: GitHub.launch_selfhostedrunners)
      deliver_error!(503, message: "Runners unavailable. Please try again later.")
    end
  end

  def ensure_tenant!(enterprise)
    result = GrpcHelper.rescue_from_grpc_errors("EnterpriseTenant") do
      GitHub::LaunchClient::Deployer.setup_tenant(enterprise)
    end
    deliver_error! 500 unless result
  end
end
