# frozen_string_literal: true

class Api::InteractiveComponents < Api::App
  # Create an ephemeral notice for a component
  post "/interactive_components/interactions/:interaction_id/ephemeral_notices" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    require_preview(:interactions_api)

    interaction = InteractiveComponent::Interaction.find_by(id: int_id_param!(key: :interaction_id))
    record_or_404(interaction)

    interactive_component = interaction.interactive_component
    record_or_404(interactive_component)

    control_access :create_ephemeral_notice,
      resource: interactive_component,
      allow_integrations: true,
      allow_user_via_integration: false

    deliver_error!(404) unless GitHub.flipper[:interactions_api].enabled?(current_integration.owner)

    if interaction.expired?
      deliver_error! 422,
        errors: ["Interaction with the ephemeral notice expired at #{interaction.expiry_date.iso8601}"],
        documentation_url: @documentation_url
    end


    data = receive_with_schema("ephemeral-notice", "create")
    ephemeral_notice = interactive_component.ephemeral_notices.build(data)
    if ephemeral_notice.save
      GitHub::PrefillAssociations.for_ephemeral_notices([ephemeral_notice])
      deliver(:ephemeral_notice_hash, ephemeral_notice)
    else
      deliver_error 422, errors: ephemeral_notice.errors, documentation_url: @documentation_url
    end
  end

  # Delete an ephemeral notice for a component
  delete "/interactive_components/:interactive_component_id/ephemeral_notices/:ephemeral_notice_id" do
    @route_owner = "@github/ce-extensibility"
    @documentation_url = Platform::NotDocumentedBecause::UNRELEASED
    require_preview(:interactions_api)

    interactive_component = InteractiveComponent.find_by(id: int_id_param!(key: :interactive_component_id))
    record_or_404(interactive_component)

    control_access :delete_ephemeral_notice,
      resource: interactive_component,
      allow_integrations: true,
      allow_user_via_integration: false

    deliver_error!(404) unless GitHub.flipper[:interactions_api].enabled?(current_integration.owner)

    ephemeral_notice = interactive_component.ephemeral_notices.find_by(id: int_id_param!(key: :ephemeral_notice_id))
    record_or_404(ephemeral_notice)

    ephemeral_notice.destroy
    deliver_empty status: 204
  end
end
