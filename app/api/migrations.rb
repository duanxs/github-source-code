# rubocop:disable Style/FrozenStringLiteralComment

class Api::Migrations < Api::App
  areas_of_responsibility :api, :migration
  statsd_tag_actions "/user/migrations"
  statsd_tag_actions "/organizations/:organization_id/migrations"

  before do
    deliver_error! 404 unless GitHub.migration_api_available?
    require_authentication!
  end

  post "/organizations/:organization_id/migrations" do
    @route_owner = "@github/data-liberation"
    @documentation_url = "/rest/reference/migrations#start-an-organization-migration"
    require_preview(:migrations)

    owner = find_org!
    control_access :migration_export, resource: owner, forbid: true, challenge: true, allow_integrations: false, allow_user_via_integration: false

    data = receive_with_schema("migration", "create-for-org")
    lock = data.fetch("lock_repositories", false)
    exclude_attachments = data.fetch("exclude_attachments", false)
    exclude = data.fetch("exclude", [])

    repos, errors = validate_repositories_data_and_populate_repos(data["repositories"], owner)
    deliver_error! 422, errors: errors if errors.any?

    migration = GitHub.migrator.export_later \
      repos: repos,
      owner: owner,
      current_user: current_user,
      lock: lock,
      exclude_attachments: exclude_attachments

    deliver :migration_hash, migration, status: 201, exclude: exclude
  end

  post "/user/migrations" do
    @route_owner = "@github/data-liberation"
    @documentation_url = "/rest/reference/migrations#start-a-user-migration"
    require_preview(:migrations)

    owner = find_owner!
    control_access :migration_export, resource: owner, forbid: true, challenge: true, allow_integrations: false, allow_user_via_integration: false

    data = receive_with_schema("migration", "create-for-user")
    lock = data.fetch("lock_repositories", false)
    exclude_attachments = data.fetch("exclude_attachments", false)
    exclude = data.fetch("exclude", [])

    repos, errors = validate_repositories_data_and_populate_repos(data["repositories"], owner)
    deliver_error! 422, errors: errors if errors.any?

    migration = GitHub.migrator.export_later \
      repos: repos,
      owner: owner,
      current_user: current_user,
      lock: lock,
      exclude_attachments: exclude_attachments

    deliver :migration_hash, migration, status: 201, exclude: exclude
  end

  get "/organizations/:organization_id/migrations" do
    @route_owner = "@github/data-liberation"
    @documentation_url = "/rest/reference/migrations#list-organization-migrations"
    require_preview(:migrations)

    owner = find_org!
    control_access :migration_export, resource: owner, forbid: true, challenge: true, allow_integrations: false, allow_user_via_integration: false

    exclude = params.fetch(:exclude, [])

    migrations = paginate_rel(Migration.for_owner(owner).newest)
    GitHub::PrefillAssociations.for_migrations(migrations)

    deliver :migration_hash, migrations, status: 200, exclude: exclude
  end

  get "/user/migrations" do
    @route_owner = "@github/data-liberation"
    @documentation_url = "/rest/reference/migrations#list-user-migrations"
    require_preview(:migrations)

    owner = find_owner!
    control_access :migration_export, resource: owner, forbid: true, challenge: true, allow_integrations: false, allow_user_via_integration: false

    exclude = params.fetch(:exclude, [])

    migrations = paginate_rel(Migration.for_owner(owner).newest)
    GitHub::PrefillAssociations.for_migrations(migrations)

    deliver :migration_hash, migrations, status: 200, exclude: exclude
  end

  get "/organizations/:organization_id/migrations/:migration_id" do
    @route_owner = "@github/data-liberation"
    @documentation_url = "/rest/reference/migrations#get-an-organization-migration-status"
    require_preview(:migrations)

    owner = find_org!
    control_access :migration_export, resource: owner, forbid: true, challenge: true, allow_integrations: false, allow_user_via_integration: false

    exclude = params.fetch(:exclude, [])

    migration = find_migration!(owner)
    GitHub::PrefillAssociations.for_migrations([migration])

    deliver :migration_hash, migration, status: 200, exclude: exclude
  end

  get "/user/migrations/:migration_id" do
    @route_owner = "@github/data-liberation"
    @documentation_url = "/rest/reference/migrations#get-a-user-migration-status"
    require_preview(:migrations)

    owner = find_owner!
    control_access :migration_export, resource: owner, forbid: true, challenge: true, allow_integrations: false, allow_user_via_integration: false

    exclude = params.fetch(:exclude, [])

    migration = find_migration!(owner)
    GitHub::PrefillAssociations.for_migrations([migration])

    deliver :migration_hash, migration, status: 200, exclude: exclude
  end

  get "/organizations/:organization_id/migrations/:migration_id/repositories" do
    @route_owner = "@github/data-liberation"
    @documentation_url = "/rest/reference/migrations#list-repositories-in-an-organization-migration"
    require_preview(:migrations)

    owner = find_org!
    control_access :migration_export, resource: owner, forbid: true, challenge: true, allow_integrations: false, allow_user_via_integration: false

    migration = find_migration!(owner)
    GitHub::PrefillAssociations.for_migrations([migration])

    repos = migration.repositories

    deliver :repository_hash, repos, status: 200
  end

  get "/user/migrations/:migration_id/repositories" do
    @route_owner = "@github/data-liberation"
    @documentation_url = "/rest/reference/migrations#list-repositories-for-a-user-migration"
    require_preview(:migrations)

    owner = find_owner!
    control_access :migration_export, resource: owner, forbid: true, challenge: true, allow_integrations: false, allow_user_via_integration: false

    migration = find_migration!(owner)
    GitHub::PrefillAssociations.for_migrations([migration])

    repos = migration.repositories

    deliver :repository_hash, repos, status: 200
  end

  get "/organizations/:organization_id/migrations/:migration_id/archive" do
    @route_owner = "@github/data-liberation"
    @documentation_url = "/rest/reference/migrations#download-an-organization-migration-archive"
    require_preview(:migrations)

    owner = find_org!
    control_access :migration_export, resource: owner, forbid: true, challenge: true, allow_integrations: false, allow_user_via_integration: false

    migration = find_migration!(owner)
    file      = find_migration_file!(migration)

    file.download
    status 302
    response["location"] = file.download_url(actor: current_user)
  end

  get "/user/migrations/:migration_id/archive" do
    @route_owner = "@github/data-liberation"
    @documentation_url = "/rest/reference/migrations#download-a-user-migration-archive"
    require_preview(:migrations)

    owner = find_owner!
    control_access :migration_export, resource: owner, forbid: true, challenge: true, allow_integrations: false, allow_user_via_integration: false

    migration = find_migration!(owner)
    file      = find_migration_file!(migration)

    file.download
    status 302
    response["location"] = file.download_url(actor: current_user)
  end

  patch "/organizations/:organization_id/migrations/:migration_id/archive" do
    @route_owner = "@github/data-liberation"
    @documentation_url = Platform::NotDocumentedBecause::INTERNAL
    require_preview(:migrations)

    org       = find_org!
    control_access :write_migration, resource: org, forbid: true, challenge: true, allow_integrations: false, allow_user_via_integration: false

    migration = find_migration!(org)
    file      = find_migration_file!(migration)

    data = receive(Hash)

    if data["state"] == "uploaded"
      file.supports_multi_part_upload = !!data["is_multipart"]

      saved = file.track_uploaded
      if saved
        migration.upload_archive!
        GitHub::PrefillAssociations.for_migrations([migration])
        return deliver :migration_hash, migration, status: 201
      else
        return deliver_error 422,
          errors: file.errors
      end
    end

    # We don't allow changing other attributes of migration files right now.
    deliver_error 422,
      errors: [],
      documentation_url: "Currently, only migration file state can be changed."
  end

  delete "/organizations/:organization_id/migrations/:migration_id/archive" do
    @documentation_url = "/rest/reference/migrations#delete-an-organization-migration-archive"
    require_preview(:migrations)

    @route_owner = "@github/data-liberation"

    # Introducing strict validation of the migration.delete-org-archive
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("migration", "delete-org-archive", skip_validation: true)

    owner = find_org!
    control_access :migration_export, resource: owner, forbid: true, challenge: true, allow_integrations: false, allow_user_via_integration: false

    migration = find_migration!(owner)
    find_migration_file!(migration)

    MigrationDestroyFileJob.enqueue(migration)
    deliver_empty(status: 204)
  end

  delete "/user/migrations/:migration_id/archive" do
    @route_owner = "@github/data-liberation"
    @documentation_url = "/rest/reference/migrations#delete-a-user-migration-archive"
    require_preview(:migrations)

    receive_with_schema("migration", "delete-user-archive")

    owner = find_owner!

    control_access :migration_export, resource: owner, forbid: true, challenge: true, allow_integrations: false, allow_user_via_integration: false

    migration = find_migration!(owner)
    find_migration_file!(migration)

    MigrationDestroyFileJob.enqueue(migration)
    deliver_empty(status: 204)
  end

  delete "/organizations/:organization_id/migrations/:migration_id/repos/:repo/lock" do
    @documentation_url = "/rest/reference/migrations#unlock-an-organization-repository"
    require_preview(:migrations)

    @route_owner = "@github/data-liberation"

    # Introducing strict validation of the migration.unlock-repo
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("migration", "unlock-repo", skip_validation: true)

    org       = find_org!
    migration = find_migration!(org)
    repo      = record_or_404(migration.repositories.find_by_name(params[:repo]))
    set_current_repo_for_access_control(repo)
    control_access :migration_export, resource: org, forbid: current_repo.public?, challenge: true, allow_integrations: false, allow_user_via_integration: false

    current_repo.unlock_excluding_descendants! if current_repo.locked_on_migration?

    deliver_empty(status: 204)
  end

  delete "/user/migrations/:migration_id/repos/:repo/lock" do
    @route_owner = "@github/data-liberation"
    @documentation_url = "/rest/reference/migrations#unlock-a-user-repository"
    require_preview(:migrations)

    receive_with_schema("migration", "unlock")

    owner     = find_owner!
    migration = find_migration!(owner)
    repo      = record_or_404(migration.repositories.find_by_name(params[:repo]))
    set_current_repo_for_access_control(repo)
    control_access :migration_repo_unlock, repo: repo, forbid: current_repo.public?, challenge: true, allow_integrations: false, allow_user_via_integration: false

    if current_repo.locked_on_migration?
      current_repo.unlock_excluding_descendants!
      deliver_empty(status: 204)
    else
      deliver_error! 404
    end
  end

  # Look up repositories and ensure they're OK to export.
  #
  # * The repository must exist.
  # * The repository must be adminable_by the current_user.
  # * The repository must be in the org from the request URL.
  # * The repository must be public if the current_user has OFAC restrictions
  def validate_repositories_data_and_populate_repos(repositories_data, owner)
    repos  = []
    errors = []

    repositories_data.each_with_index do |name, i|
      repo = owner.repositories.find_by_name(name) || Repository.with_name_with_owner(name)

      if repo && repo.owner.to_s == owner.to_s && !ofac_restricted?(repo, owner)
        repos << repo
      else
        errors.push resource: "Migration", field: "repositories", index: i, code: "invalid_value"
      end
    end

    if repos.empty?
      errors.push resource: "Migration", field: "repositories", code: "missing_field"
    end

    [repos, errors]
  end

  # Private Is the migration for the given repo and owner ofac restricted?
  #
  # repo - Repository being migrated
  # owner - Organization or User that the Migration belongs to.
  #
  # Returns Boolean
  def ofac_restricted?(repo, owner)
    repo.private? && owner.has_any_trade_restrictions?
  end

  # Find Migration for request.
  #
  # owner - Organization or User that Migration belongs to.
  #
  # Returns a Migration or halts request early with a 404.
  def find_migration!(owner)
    record_or_404 find_migration(owner)
  end

  # Find Migration for request.
  #
  # owner - Organization or User that Migration belongs to.
  #
  # Returns a Migration or nil
  def find_migration(owner)
    Migration.for_owner(owner).find_by_id(params[:migration_id])
  end

  # Find the MigrationFile for this Migration.
  #
  # migration - Migration that MigrationFile belongs to.
  #
  # Returns a MigrationFile or halts request early with a 404.
  def find_migration_file!(migration)
    record_or_404 migration.file
  end

  def find_owner!
    record_or_404 find_owner
  end

  def find_owner
    return current_integration_installation.target if current_integration_installation
    current_actor
  end
end
