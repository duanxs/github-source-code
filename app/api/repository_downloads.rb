# rubocop:disable Style/FrozenStringLiteralComment

class Api::RepositoryDownloads < Api::App
  areas_of_responsibility :downloads, :api

  # List downloads for a repository (uploaded packages, not tag tarballs/zipballs)
  get "/repositories/:repository_id/downloads" do
    @route_owner = Platform::NoOwnerBecause::DEPRECATED
    @documentation_url = Platform::NotDocumentedBecause::DEPRECATED
    control_access :list_downloads, repo: repo = find_repo!, allow_integrations: false, allow_user_via_integration: false

    downloads = repo.downloads
    downloads = paginate_rel(downloads)
    SlottedCounterService.prefill downloads
    GitHub::PrefillAssociations.for_repo_downloads(downloads)
    deliver :download_hash, downloads, repo: repo
  end

  # Create a new download
  post "/repositories/:repository_id/downloads" do
    @route_owner = Platform::NoOwnerBecause::DEPRECATED
    @documentation_url = Platform::NotDocumentedBecause::DEPRECATED
    control_access :apps_audited, resource: Platform::PublicResource.new, allow_integrations: true, allow_user_via_integration: true
    deliver_error 406, message: "Cannot create downloads.  Use releases instead."
  end

  # Get a single download
  get "/repositories/:repository_id/downloads/:download_id" do
    @route_owner = Platform::NoOwnerBecause::DEPRECATED
    @documentation_url = Platform::NotDocumentedBecause::DEPRECATED
    repo, download = find_repo_and_download
    control_access :get_download, repo: repo, resource: download, allow_integrations: false, allow_user_via_integration: false
    GitHub::PrefillAssociations.for_repo_downloads([download])
    deliver :download_hash, download, repo: repo, last_modified: calc_last_modified(repo)
  end

  # Delete a download
  delete "/repositories/:repository_id/downloads/:download_id" do
    @route_owner = Platform::NoOwnerBecause::DEPRECATED
    @documentation_url = Platform::NotDocumentedBecause::DEPRECATED
    repo, download = find_repo_and_download
    control_access :delete_download, repo: repo, resource: download, allow_integrations: false, allow_user_via_integration: false

    download.destroy

    deliver_empty(status: 204)
  end

  private

  def find_repo_and_download
    repo = find_repo!
    download = repo.downloads.find_by_id(int_id_param!(key: :download_id)) if repo
    [repo, download]
  end
end
