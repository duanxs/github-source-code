# rubocop:disable Style/FrozenStringLiteralComment

class Api::GitBlobs < Api::App
  statsd_tag_actions "/repositories/:repository_id/git/blobs/:sha"

  MAX_BLOB_SIZE = 100.megabytes

  # Get the contents of a blob object
  get "/repositories/:repository_id/git/blobs/:sha" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/git#get-a-blob"
    control_access :get_blob,
      resource: repo = find_repo!,
      allow_integrations: true,
      allow_user_via_integration: true,
      approved_integration_required: false

    ensure_repo_content!(repo)

    begin
      blob = repo.blob_by_oid(sha_param!(:sha))
    rescue GitRPC::ObjectMissing, GitRPC::InvalidObject
      deliver_error!(404)
    end

    deliver_too_large_error! if blob.size > MAX_BLOB_SIZE

    if medias.api_param?(:raw)
      deliver_raw blob.raw_data, content_type: blob.content_type
    else
      deliver :grit_blob_hash, blob, repo: repo
    end
  end

  # Create a new blob object
  post "/repositories/:repository_id/git/blobs" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/git#create-a-blob"
    control_access :create_blob,
      resource: repo = find_repo!,
      allow_integrations: true,
      allow_user_via_integration: true

    authorize_content(:create, repo: repo)
    ensure_repo_content!(repo)
    ensure_repo_writable!(repo)

    # Introducing strict validation of the git-blob.create
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    data = receive_with_schema("git-blob", "create", skip_validation: true)
    content = data["content"]

    unless content
      deliver_error!(422,
        errors: [api_error(:Blob, :content, :missing_field)],
        documentation_url: @documentation_url)
    end

    unless content.is_a?(String)
      deliver_error!(422,
        message: "content must be a string",
        errors: [api_error(:Blob, :content, :invalid)],
        documentation_url: @documentation_url)
    end

    if data["encoding"] == "base64"
      content = Base64.decode64(content)
    end

    sha = repo.rpc.write_blob(content)

    blob = {
      sha: sha,
      url: api_url("/repos/#{repo.name_with_owner}/git/blobs/#{sha}"),
    }

    deliver_raw blob, repo: repo, status: 201
  end

  private

  def api_url(suffix)
    Api::Serializer.send :url, suffix
  end

  def deliver_too_large_error!
    deliver_error! 403,
      message: "This API returns blobs up to 100 MB in size. "\
        "The requested blob is too large to fetch via the API, "\
        "but you can always clone the repository via Git in order to obtain this blob.",
      errors: [api_error(:Blob, :data, :too_large)],
      documentation_url: "/v3/git/blobs/#get-a-blob"
  end

  def authorize_content(operation = :create, data = {})
    authorization = ContentAuthorizer.authorize(current_user, :blob, operation, data)
    deliver_content_authorization_denied!(authorization) if authorization.failed?
  end
end
