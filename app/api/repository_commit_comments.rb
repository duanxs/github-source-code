# rubocop:disable Style/FrozenStringLiteralComment

class Api::RepositoryCommitComments < Api::App
  # List commit comments for a repository
  get "/repositories/:repository_id/comments" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#list-commit-comments-for-a-repository"
    control_access :list_commit_comments, repo: repo = find_repo!, allow_integrations: true, allow_user_via_integration: true

    # We're going to be ordering by id, which makes MySQL try to use the
    # primary index for the entire lookup. This is *extremely* slow for
    # repositories with a lot of commit comments, so we tell MySQL to ignore
    # the primary index for the ORDER BY clause in order to give it a chance to
    # use a better index.
    comments = repo.commit_comments.from("`commit_comments` IGNORE INDEX FOR ORDER BY (PRIMARY)")
    comments = paginate_rel(comments.filter_spam_for(current_user).order("id asc"))

    GitHub.dogstats.time "prefill", tags: ["via:api", "action:repos_comments_list"] do
      GitHub::PrefillAssociations.for_comments(comments, repository: repo)
      Reaction::Summary.prefill(comments)

      deliver :commit_comment_hash, comments, repo: repo
    end
  end

  # List commit comments for a single commit
  get "/repositories/:repository_id/commits/*/comments" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#list-commit-comments"
    ref = params[:splat].first
    control_access :list_commit_comments, repo: repo = find_repo!, allow_integrations: true, allow_user_via_integration: true

    commit =
      if ref.size == 40 && ref =~ /^[A-Za-z0-9]{40}$/n
        ref
      else
        find_commit!(repo, ref, @documentation_url)
      end
    comments = paginate_rel(repo.commit_comments.filter_spam_for(current_user).order("id asc").where(commit_id: commit.to_s))
    GitHub::PrefillAssociations.for_comments(comments, repository: repo)
    Reaction::Summary.prefill(comments)
    deliver :commit_comment_hash, comments, repo: repo
  end

  # Create a commit comment
  post "/repositories/:repository_id/comments" do
    @route_owner = "@github/pe-repos"
    @documentation_url = Platform::NotDocumentedBecause::DEPRECATED
    control_access :create_commit_comment, repo: repo = find_repo!, allow_integrations: true, allow_user_via_integration: true
    authorize_content(repo: repo, operation: :create)

    data = receive_with_schema("comment", "deprecated-create")
    attributes = attr(data, :path, :position, :line, :commit_id, :body)
    attributes.update user: current_user, repository: repo

    comment = CommitComment.new(attributes)
    if comment.save
      deliver :commit_comment_hash, comment, status: 201, repo: repo
    else
      deliver_error 422, errors: comment.errors, documentation_url: @documentation_url
    end
  end

  # Create a commit comment
  post "/repositories/:repository_id/commits/*/comments" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#create-a-commit-comment"
    ref = params[:splat].first
    control_access :create_commit_comment, repo: repo = find_repo!, allow_integrations: true, allow_user_via_integration: true
    authorize_content(repo: repo, operation: :create)

    commit = find_commit!(repo, ref, @documentation_url)

    data = receive_with_schema("comment", "create-legacy")
    attributes = attr(data, :path, :position, :line, :commit_id, :body)
    attributes.update user: current_user, repository: repo

    attributes[:commit_id] = commit.oid if commit.oid

    comment = CommitComment.new(attributes)
    if comment.save
      deliver :commit_comment_hash, comment, status: 201, repo: repo
    else
      deliver_error 422, errors: comment.errors, documentation_url: @documentation_url
    end
  end

  # Get a commit comment
  get "/repositories/:repository_id/comments/:comment_id" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#get-a-commit-comment"
    repo = find_repo!
    comment = find_comment(repo)
    control_access :get_commit_comment, repo: repo, comment: comment, allow_integrations: true, allow_user_via_integration: true
    GitHub::PrefillAssociations.for_comments([comment], repository: repo)
    Reaction::Summary.prefill([comment])
    deliver :commit_comment_hash, comment, repo: repo, last_modified: calc_last_modified(comment)
  end

  # Update a commit comment
  verbs :patch, :post, "/repositories/:repository_id/comments/:comment_id" do
    @route_owner = "@github/pe-repos"
    @documentation_url = "/rest/reference/repos#update-a-commit-comment"

    repo = find_repo!
    comment = find_comment(repo)
    control_access :update_commit_comment, repo: repo, comment: comment, allow_integrations: true, allow_user_via_integration: true
    authorize_content(repo: repo, operation: :update)

    data = receive_with_schema("comment", "update-for-commit-legacy")

    if comment.update_body(data["body"], current_user)
      deliver :commit_comment_hash, comment, repo: repo
    else
      deliver_error 422,
        errors: comment.errors,
        documentation_url: @documentation_url
    end
  end

  # Delete a commit comment
  delete "/repositories/:repository_id/comments/:comment_id" do
    @route_owner = "@github/pe-repos"

    # Introducing strict validation of the comment.delete
    # JSON schema would cause breaking changes for integrators
    # skip_validation until a rollout strategy can be determined
    # see: https://github.com/github/ecosystem-api/issues/1555
    receive_with_schema("comment", "delete", skip_validation: true)

    @documentation_url = "/rest/reference/repos#delete-a-commit-comment"
    repo = find_repo!
    comment = find_comment(repo)
    control_access :delete_commit_comment, repo: repo, comment: comment, allow_integrations: true, allow_user_via_integration: true
    authorize_content(repo: repo, operation: :delete)

    comment.destroy
    deliver_empty(status: 204)
  end

private
  def find_comment(repo)
    return nil if !repo
    comment = repo.commit_comments.find_by_id(int_id_param!(key: :comment_id))

    if comment && comment.hide_from_user?(current_user)
      deliver_error!(404)
    else
      comment
    end
  end

  def authorize_content(repo:, operation: :create)
    authorization = ContentAuthorizer.authorize(current_user, :commit_comment, operation, repo: repo)
    deliver_content_authorization_denied!(authorization) if authorization.failed?
  end

end
