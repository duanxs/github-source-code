# frozen_string_literal: true

module GitHub
  # Custom scrubber for inlining link URLs directly into the text
  # for outputs that must be plaintext.
  # Leverages Rails' scrubber for removing HTML tags but keeping text content
  # see: https://github.com/rails/rails-html-sanitizer
  #
  # Example:
  # input:  "<p>See <a href='help.github.com'>help docs</a> for more info.</p>"
  # output: "See help docs at help.github.com for more info."
  class TextOnlyWithUrlsScrubber < ::Rails::Html::TextOnlyScrubber
    def scrub(node)
      node.after(" at #{node['href']}") if link? node
      super
    end

    private

    def link?(node)
      node.name == "a" && node["href"].yield_self { |href|
        href.present? && href != "#" && !href.start_with?("javascript:")
      }
    end
  end
end
