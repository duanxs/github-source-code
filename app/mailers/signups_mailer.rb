# frozen_string_literal: true

class SignupsMailer < AccountMailer
  self.mailer_name = "mailers/signups"
  self.delivery_job = SignupsDeliveryJob

  layout "html_email_layout", only: [:email_verification]

  def email_verification(email)
    user = email.user

    @user = user
    @email = email

    @cta_url = user_confirm_verification_email_url(
      @user,
      @email,
      @email.verification_token,
    )

    @cta_tracking_url = ga_campaign_url(
      @cta_url,
      source: "verification-email",
      medium: "email",
      campaign: "github-email-verification",
      content: "html",
    )

    premail(
      to: user_email(user, email.to_s),
      subject: "[GitHub] Please verify your email address.",
    )

    # This header is used by our mailservers to ensure that verification mails
    # do not go through SendGrid. This makes it possible for the verification
    # mails to bypass SendGrid bounce suppressions.
    headers["X-GitHub-Verify"] = user.login
  end
end
