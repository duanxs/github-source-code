# frozen_string_literal: true

class MarketplaceMailer < ApplicationMailer
  self.mailer_name = "mailers/marketplace"

  before_action :set_image_url

  def state_changed(action:, old_state:, new_state:, listing:, message:)
    return if listing.listable_is_sponsorable?

    subject = "#{listing.name} is now #{new_state}"

    @action = action
    @old_state = old_state
    @new_state = new_state
    @listing = listing
    @message = message

    mail(from: github_marketplace, to: "marketplace@github.com", subject: subject)
  end

  def listing_approved(user:, listing:, message: nil)
    @listing = listing
    @message = message

    mail(from: github_marketplace,
          to: user_email(user),
          subject: "#{listing.name} has been approved for the GitHub Marketplace")
  end

  def listing_redrafted(user:, new_state:, listing:, message: nil)
    subject = "#{listing.name} requires edits for the GitHub Marketplace"

    @listing = listing
    @new_state = new_state
    @message = message

    mail(from: github_marketplace, to: user_email(user), subject: subject)
  end

  def listing_delisted(user:, listing:, message: nil)
    subject = "#{listing.name} has been removed from the GitHub Marketplace"

    @listing = listing
    @message = message

    mail(from: github_marketplace, to: user_email(user), subject: subject)
  end

  def listing_rejected(user:, listing:, message: nil)
    subject = "#{listing.name} has been rejected for the GitHub Marketplace"

    @listing = listing
    @message = message

    mail(from: github_marketplace, to: user_email(user), subject: subject)
  end

  def retarget(user, previews, unsubscribe_token)
    @email_title = "Complete your GitHub Marketplace orders"
    @user = user
    @previews = previews
    @unsubscribe_token = unsubscribe_token

    mail(from: github_marketplace, to: user_email(user), subject: "Complete your GitHub Marketplace orders", categories: "marketplace, marketplace-retarget")
  end

  def featured_customers_need_review(listing:)
    @listing = listing
    @featured_org_logins = Organization.where(id: @listing.featured_organizations.where(approved: false).pluck(:organization_id)).pluck(:login)

    mail(from: github_marketplace, to: github_marketplace, subject: "Review: featured customers submitted for #{listing.name}", categories: "marketplace")
  end

  private

  def set_image_url
    @image_url = image_base_url
  end
end
