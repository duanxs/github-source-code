# frozen_string_literal: true

class OrganizationMailer < ApplicationMailer
  include ApplicationHelper
  include GitHub::RouteHelpers

  self.mailer_name = "mailers/organization"

  helper :application
  helper :email_link_tracking

  layout "organization/organization_email_layout",
         only: %i(removed_from_org remove_outside_collaborator domain_verification_notice notification_restrictions_enabled)

  # Notifies people when they're added to a team
  def team_added(user, team, adder)
    @org = team.organization
    @team = team
    @url = "#{GitHub.url}/orgs/#{@org}/teams/#{team.slug}"
    @signature = notification_signature(@url)

    subject = if adder
      "#{adder.safe_profile_name} added you to the #{@org.safe_profile_name} team #{team.name}"
    else
      "You've been added to the #{@org.safe_profile_name} team #{team.name}"
    end

    mail(
      from: github_noreply(team.organization),
      to: user_email(user, GitHub.newsies.email(user, @org).value),
      subject: subject,
      categories: "org,org-team-add-member",
    )
  end

  # Notifies everyone on a team when someone has been added to it.
  def team_member_added(requester, team, adder)
    @requester = requester
    @org = team.organization
    @team = team
    @adder = adder
    @url = "#{GitHub.url}/orgs/#{@org}/teams/#{team.slug}"
    @signature = notification_signature(@url)
    recipients = team_authoritative_emails(@team)

    subject = "#{@requester.login} was added to #{@org.safe_profile_name}'s #{@team.name} team"

    mail(
      from: github_noreply(@org),
      bcc: recipients,
      subject: subject,
    )
  end

  def removed_from_team(user, team_name, org, legacy_owner:, team_destroyed: false)
    @org = org
    @team_destroyed = team_destroyed
    @team_name = team_name
    @send_owners_team_email = legacy_owner && @org.adminable_by?(user)
    @signature = notification_signature(@url)

    subject = if @team_destroyed
      if @send_owners_team_email
        "Important: We've made changes to the Owners team on #{@org.safe_profile_name}"
      else
        "The #{@org.safe_profile_name} team #{@team_name} has been deleted"
      end
    else
      "You've been removed from the #{@org.safe_profile_name} team #{@team_name}"
    end

    mail(
      from: github_noreply(@org),
      to: user_email(user, GitHub.newsies.email(user, @org).value),
      subject: subject,
      categories: "org,org-team-remove-member",
    )
  end

  LOOKUP_ORG_OWNER_HELP_DOC_URL = \
    "#{GitHub.help_url}/articles/viewing-people-s-roles-in-an-organization/"

  # Send a notification email to User USER upon removal from Organization ORG.
  # Optionally, include a REASON (a Symbol).
  def removed_from_org(user, org, reason = nil)
    @user = user
    @org = org
    @signature = notification_signature(@url)
    @reason = reason&.to_sym
    @lookup_org_owner_help_doc_url = LOOKUP_ORG_OWNER_HELP_DOC_URL

    subject = "[GitHub] You've been removed from the #{org.safe_profile_name} organization"

    premail(
      from: github_noreply,
      to: user_email(user, GitHub.newsies.email(user, org).value),
      subject: subject,
      categories: "org,org-remove-member",
    )
  end

  # Send a notification email to User USER upon removal of outside collaborator
  # status from Organization ORG. Include the list of REPOSITORY_NAMES
  # (an Array of Strings) of repositories affected. Optionally include a REASON
  # (a String).
  def remove_outside_collaborator(user, org, repository_names, reason = nil)
    @user = user
    @org = org
    @repository_names = repository_names
    url = org_root_url(@org)
    @signature = notification_signature(url)
    @reason = reason
    @lookup_org_owner_help_doc_url = LOOKUP_ORG_OWNER_HELP_DOC_URL

    subject = "[GitHub] You've been removed from #{org.safe_profile_name}'s repositories"

    premail(
      from: github_noreply,
      to: user_email(user, GitHub.newsies.email(user, org).value),
      subject: subject,
      categories: "org,org-remove-outside-collaborator",
    )
  end

  def admin_added(user, organization, adder = nil)
    @org = organization
    @url = org_owners_url(@org)
    @signature = notification_signature(@url)

    subject = if adder
      "#{adder.safe_profile_name} made you an owner of the #{@org.safe_profile_name} organization"
    else
      "You've been made an owner of the #{@org.safe_profile_name} organization"
    end

    mail(
      from: github_noreply(@org),
      to: user_email(user, GitHub.newsies.email(user, @org).value),
      subject: subject,
      categories: "org,org-add-admin",
    )
  end

  def application_access_requested(requestor, org, oauth_app, recipient)
    @org_name  = org.safe_profile_name
    @requestor = requestor
    @app_name = oauth_app.name
    app_owner = oauth_app.owner
    @app_owner_name = app_owner.safe_profile_name
    @approvals_url = org_application_approval_url(org, oauth_app)

    mail(
      content_transfer_encoding: "quoted-printable",
      from: github_noreply(org),
      to: user_email(recipient, GitHub.newsies.email(recipient, org).value),
      subject: "[GitHub] Third-party application approval request for #{@org_name}",
    )
  end

  def application_access_approved(recipient:, organization:, application:)
    @org_name = organization.safe_profile_name
    @app_name = application.name
    @app_owner_name = application.owner.safe_profile_name

    mail(
      content_transfer_encoding: "quoted-printable",
      from: github_noreply(organization),
      to: user_email(recipient, GitHub.newsies.email(recipient, organization).value),
      subject: "[GitHub] #{@org_name} has approved a third-party application that you use",
    )
  end

  def team_membership_request(requester:, team:)
    @requester     = requester
    @team          = team
    @org_name      = @team.organization.safe_profile_name
    @approvals_url = team_members_url(@team.organization, @team)
    recipients  = team_authoritative_emails(@team)

    mail(
      bcc: recipients,
      subject: "#{@requester.login} would like to join #{@org_name}'s #{@team.name} team",
      from: github_noreply(@team.organization),
    )
  end

  def team_relationship_request(requester:, parent_team:, child_team:, initiated_by_parent:)
    @requester          = requester
    @parent_team        = parent_team
    @child_team         = child_team
    @target_team        = initiated_by_parent ? child_team : parent_team
    @org_name           = @child_team.organization.safe_profile_name
    @approvals_url      = if initiated_by_parent
      team_teams_url(@parent_team.organization, @child_team)
    else
      team_teams_url(@child_team.organization, @parent_team)
    end

    recipients = team_authoritative_emails(@target_team)

    subject = if initiated_by_parent
      "#{@requester.login} wants to make #{@child_team.name} team a child team of #{@org_name}'s #{@parent_team.name} team."
    else
      "#{@requester.login} wants to make #{@parent_team.name} team the parent of #{@org_name}'s #{@child_team.name} team."
    end

    mail(
      bcc:     recipients,
      subject: subject,
      from:    github_noreply(@parent_team.organization),
    )
  end

  def invited_to_billing_manager_role(invitation, invitation_token = nil)
    invitation.reset_token
    @invitation = invitation
    @user       = invitation.invitee
    @inviter    = invitation.inviter
    @org        = invitation.organization
    @url        = if invitation.email?
      org_show_pending_billing_manager_invitation_url(@org, invitation_token: invitation.token, via_email: "1")
    else
      org_show_pending_billing_manager_invitation_url(@org, via_email: "1")
    end

    # Use the inviter's email address as the reply-to email if the inviter
    # exists and has a valid email address. Fall back to the support address if
    # not.
    if invitation.show_inviter?
      reply_to = user_email(@inviter, allow_private: false)
    end
    reply_to ||= github

    subject = if invitation.show_inviter?
      "[GitHub] @#{@inviter.login} has invited you to be a billing manager for the @#{@org.login} organization"
    else
      "[GitHub] You're invited to be a billing manager for the @#{@org.login} organization"
    end

    mail(
      from: github,
      to: send_to(invitation),
      subject: subject,
      reply_to: reply_to,
      categories: "org,add-billing-manager",
    )
  end

  def failed_two_factor_enforcement(actor, organization)
    @actor = actor
    @organization = organization

    subject = "[GitHub] Failed to enable and enforce two-factor requirement for #{organization.safe_profile_name} organization"

    mail(
      from: github_noreply,
      to: user_email(actor, GitHub.newsies.email(actor, organization).value),
      subject: subject,
    )
  end

  # Disclose to an existing org. member (with verified-domain-emails) that admins can see their email
  def domain_verification_notice(user, organization, domain)
    @user = user
    @org = organization
    @domain = domain

    subject = "[GitHub] An admin of the #{@org.safe_profile_name} organization just verified the #{@domain} domain"

    mail(
      to: user_email(@user),
      from: github_noreply(organization),
      subject: subject,
    )
  end

  def notification_restrictions_enabled(user, organization)
    @user = user
    @org = organization
    @verified_domains = @org.async_verified_domains.sync.map(&:domain)

    subject = "[GitHub] #{@org.login} has restricted email notifications"
    mail(
     to: user_email(user, GitHub.newsies.email(user, @org).value),
     from: github_noreply(@org),
     subject: subject,
   )
  end

  def welcome_enterprise_cloud_trial(actor, organization)
    subject = "Welcome to your GitHub Enterprise trial"
    @organization = organization
    @user = actor
    @new_organization_repository_link = new_org_repository_path(organization)
    @email_source = "ghe_trial_welcome"
    cloud_trial = Billing::EnterpriseCloudTrial.new(@organization)
    @trail_expiry_date = cloud_trial.expires_on&.to_s(:long)

    premail(
      from: github_noreply,
      to: user_email(actor),
      subject: subject,
    )
  end

  def end_enterprise_cloud_trial(actor, organization)
    return unless Billing::PlanTrial.active_for?(organization, organization.plan.name)

    cloud_trial = Billing::EnterpriseCloudTrial.new(organization)
    if cloud_trial.expires_on > 25.hours.from_now
      delivery_date = (cloud_trial.expires_on - 1.day).to_datetime
      OrganizationMailer.end_enterprise_cloud_trial(actor, organization).deliver_later(wait_until: delivery_date)
      return
    end

    subject = "Your GitHub Enterprise trial ends today"
    @organization = organization
    @user = actor
    @new_organization_repository_link = new_org_repository_path(organization)
    @email_source = "ghe_trial_end"

    premail(
      from: github_noreply,
      to: user_email(actor),
      subject: subject,
    )
  end

  def api_oauth_access_via_query_params_deprecation(oauth_access, time:, url:, user_agent:)
    @application = oauth_access.application
    @org         = @application.owner

    @time       = time.to_s(:deprecation_mailer)
    @url        = url
    @user_agent = user_agent

    mail_opts = {
      from: github_noreply(@org),
      bcc: admin_emails(@org),
      subject: "[GitHub API] Deprecation notice for authentication via URL query parameters",
    }

    mail(mail_opts)
  end

  def api_oauth_credentials_via_params_deprecation(application, time:, url:, user_agent:)
    @application = application
    @org         = application.owner

    @time       = time.to_s(:deprecation_mailer)
    @url        = url
    @user_agent = user_agent

    mail_opts = {
      from: github_noreply(@org),
      bcc: admin_emails(@org),
      subject: "[GitHub API] Deprecation notice for authentication via URL query parameters",
    }

    mail(mail_opts)
  end

  def api_applications_endpoints_deprecation(application, time:)
    @application = application
    @org         = application.owner

    @time       = time.to_s(:deprecation_mailer)

    mail_opts = {
      from: github_noreply(@org),
      bcc: admin_emails(@org),
      subject: "[GitHub API] Deprecation notice for OAuth Application API",
    }

    mail(mail_opts)
  end

  def legacy_integration_event_deprecation(org, app_names)
    @org = org
    @app_names = app_names

    mail_opts = {
      from: github_noreply(@org),
      bcc: admin_emails(@org),
      template_name: :legacy_integration_event_deprecation,
      subject: "[GitHub] Deprecation notice for GitHub Apps webhook events",
    }

    mail(mail_opts)
  end

  def api_integrations_access_tokens_deprecation(application, time:)
    @application = application
    @org         = application.owner

    @time       = time.to_s(:deprecation_mailer)

    mail_opts = {
      from: github_noreply(@org),
      bcc: admin_emails(@org),
      subject: "[GitHub API] Deprecation notice for Installation Access Token creation API",
    }

    mail(mail_opts)
  end

  private
  # Private: An organization's admins' email addresses that are on a specific team.
  # Excluding suspended admins. Use this to set
  # From, To, CC, and Reply-To headers.
  #
  # team - A team.
  #
  # Returns an Array of String email addresses with
  # the admins' full names (if available).
  def org_admins_in_team_emails(team)
    org = team.organization
    admin_mails = org.admins.map do |admin|
      next if admin.suspended?
      next unless team.member? admin
      user_email(admin, GitHub.newsies.email(admin, org).value)
    end
    # Remove nil entries, as both 'next' and 'user_email' may return nil.
    admin_mails.compact
  end

  # Email addresses for a team's maintainers. Excluding suspended maintainers.
  # Use this to set From, To, CC, and Reply-To headers.
  #
  # team - A team.
  #
  # Returns the email string with the maintainers' full names if available.
  def team_maintainer_emails(team)
    maintainer_emails = team.maintainers.map do |maintainer|
      next if maintainer.suspended?
      user_email(maintainer, GitHub.newsies.email(maintainer, team.organization).value)
    end

    # Remove nil entries, as both 'next' and 'user_email' may return nil.
    maintainer_emails.compact
  end

  def team_authoritative_emails(team)
    if team_maintainer_emails(team).present?
      team_maintainer_emails(team)
    elsif org_admins_in_team_emails(team).present?
      org_admins_in_team_emails(team)
    else
      admin_emails(team.organization)
    end
  end

  def send_to(invitation)
    if invitation.email?
      invitation.email
    else
      user_email(@user, GitHub.newsies.email(@user, @org).value)
    end
  end
end
