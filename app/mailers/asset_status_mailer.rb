# frozen_string_literal: true

class AssetStatusMailer < ApplicationMailer
  self.mailer_name = "mailers/asset_status"

  def approaching_quota(asset_status)
    @asset_status = asset_status
    @user = asset_status.owner

    recipients = user_or_billing_recipients(@user)

    mail(recipients.merge(
      from: github,
      subject: "[GitHub] At 80% of Git LFS data quota for #{@user}",
    ))
  end

  def over_quota(asset_status)
    @asset_status = asset_status
    @user = asset_status.owner

    recipients = user_or_billing_recipients(@user)

    mail(recipients.merge(
      from: github,
      subject: "[GitHub] At 100% of Git LFS data quota for #{@user}",
    ))
  end

  def over_quota_disable(asset_status)
    @asset_status = asset_status
    @user = asset_status.owner

    if @asset_status.notified_state != "disabled_over_quota"
      Failbot.report(
        RuntimeError.new("Invalid Asset::Status state when sending over_quota_disable email: #{Asset::Status.notified_states[@asset_status.notified_state]}"),
        app: "github-user",
        owner_id: @user.id,
        asset_status_id: @asset_status.id,
      )
    end

    recipients = user_or_billing_recipients(@user)

    mail(recipients.merge(
      from: github,
      subject: "[GitHub] Git LFS disabled for #{@user}",
    ))
  end
end
