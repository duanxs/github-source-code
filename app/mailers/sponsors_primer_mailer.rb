# frozen_string_literal: true

class SponsorsPrimerMailer < ApplicationMailer
  include ApplicationHelper
  helper :bundle
  helper :mailer_bundle

  self.mailer_name = "mailers/sponsors_primer"
  layout "layouts/primer_layout"

  def listing_approved(sponsorable:, message: nil)
    @sponsorable = sponsorable
    recipients = listing_admin_recipients(sponsorable)

    premail(
      from: github_noreply,
      to: recipients[:to],
      bcc: recipients[:bcc],
      subject: "You've been approved for GitHub Sponsors",
    )
  end

  def milestone_reached(sponsorable:, milestone_title:)
    @sponsorable = sponsorable
    @milestone_title = milestone_title
    recipients = listing_admin_recipients(sponsorable)

    premail(
      from: github_noreply,
      to: recipients[:to],
      bcc: recipients[:bcc],
      subject: "You've reached a milestone!",
    )
  end

  private

  def listing_admin_recipients(sponsorable)
    sponsorable_contact = sponsorable
      .sponsors_membership
      .contact_email_address

    if sponsorable.organization?
      recipients = build_admin_recipients_list(sponsorable, sponsorable.admins)
      recipients[:bcc] << sponsorable_contact
      recipients
    else
      {
        to: sponsorable_contact,
        bcc: [],
      }
    end
  end
end
