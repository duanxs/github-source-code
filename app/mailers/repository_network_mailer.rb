# frozen_string_literal: true

class RepositoryNetworkMailer < ApplicationMailer
  self.mailer_name = "mailers/repository_network"

  def paid_org_private_fork_warning(repo, users = nil)
    @org = repo.owner.login
    @nwo = repo.name_with_owner
    @parent_nwo = repo.parent.name_with_owner

    emails = emails_for_org(users, repo)

    mail(
      bcc: emails.concat(bcc_log),
      from: github,
      subject: "Upcoming changes to your fork #{@nwo}",
    )
  end

  def free_org_private_fork_warning(repo, users = nil)
    @org = repo.owner.login
    @nwo = repo.name_with_owner
    @parent_nwo = repo.parent.name_with_owner

    emails = emails_for_org(users, repo)

    mail(
      bcc: emails.concat(bcc_log),
      from: github,
      subject: "Upcoming changes to your fork #{@nwo}",
    )
  end

  def paid_user_private_fork_warning(repo)
    @user = repo.owner.login
    @nwo = repo.name_with_owner
    @parent_nwo = repo.parent.name_with_owner

    mail(
      to: user_email(repo.owner),
      bcc: bcc_log,
      from: github,
      subject: "Upcoming changes to your fork #{@nwo}",
    )
  end

  def free_user_private_fork_warning(repo)
    @user = repo.owner.login
    @nwo = repo.name_with_owner
    @parent_nwo = repo.parent.name_with_owner

    mail(
      to: user_email(repo.owner),
      bcc: bcc_log,
      from: github,
      subject: "Upcoming changes to your fork #{@nwo}",
    )
  end

  private

  def emails_for_org(users, repo)
    emails = if users
      users.map { |user|
        user_email(user, GitHub.newsies.email(user, repo.owner).value)
      }
    else
      admin_emails(repo.owner)
    end
    emails.size > 10 ? emails.slice(-5, 5) : emails
  end
end
