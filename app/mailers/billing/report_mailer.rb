# frozen_string_literal: true

class Billing::ReportMailer < ApplicationMailer
  include ApplicationHelper

  self.mailer_name = "mailers/billing_report"

  layout "billing_email_layout"
  helper :application

  def metered_export_complete(metered_export)
    @export = metered_export
    @image_url = image_base_url

    mail(
      from: github,
      to: metered_export.requester.email,
      subject: "[GitHub] Your usage report is ready to download",
    )
  end

  def metered_export_error(billable_owner, requester)
    @billable_owner = billable_owner

    mail(
      from: github,
      to: requester.email,
      subject: "[GitHub] We were unable to process your usage export request",
    )
  end

  private

  def path_for_export
    if @export.billable_owner.is_a?(Business)
      metered_export_enterprise_path(@export.billable_owner, @export)
    elsif @export.billable_owner.organization?
      org_metered_export_path(@export.billable_owner, @export)
    else
      metered_export_path(@export)
    end
  end
  helper_method :path_for_export
end
