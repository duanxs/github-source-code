# frozen_string_literal: true

class AccountRecoveryMailer < ApplicationMailer

  self.mailer_name = "mailers/account_recovery"

  def send_otp(user, otp)
    return if GitHub.enterprise?

    @user = user
    @otp = otp

    mail_to_primary_bcc_remaining_account_related_emails(
      template_name: :send_otp,
      subject: "[GitHub] Two-factor lockout request",
      categories: "account-security",
    )
  end

  def confirm_request_completed(user, id, token)
    return if GitHub.enterprise?

    @user = user
    @url = two_factor_recovery_abort_request_url(id, token)

    mail_to_primary_bcc_remaining_account_related_emails(
      template_name: :request_completed,
      subject: "[GitHub] Two-factor lockout request completed",
      categories: "account-security",
    )
  end

  def successful_login_detected(user)
    return if GitHub.enterprise?

    @user = user
    @support_link = contact_url(form: { subject: "Account recovery cancelled - @#{user}" })

    mail_to_primary_bcc_remaining_account_related_emails(
      template_name: :successful_login,
      subject: "[GitHub] Two-factor lockout request cancelled",
      categories: "account-security",
    )
  end

  def request_approved_by_staff(user, id, abort_token, continue_token, emails = nil)
    return if GitHub.enterprise?

    @user = user
    @orgs_requiring_two_factor = user.organizations.select { |org| org.two_factor_requirement_enabled? }

    @abort_url = two_factor_recovery_abort_request_url(id, abort_token)
    @continue_url = two_factor_recovery_continue_url(id, continue_token)
    payload = { template_name: :request_approved_by_staff,
               subject: "[GitHub] Account recovery request approved",
               categories: "account-security" }
    if emails.blank?
      mail_to_primary_bcc_remaining_account_related_emails(**payload)
    else
      primary_email = emails.shift
      mail(payload.merge({to: primary_email, bcc: emails.present? ? emails : []}))
    end
  end

  def request_declined_by_staff(user)
    return if GitHub.enterprise?

    @user = user
    @support_link = contact_url(form: { subject: "Account recovery declined - @#{user}" })

    mail_to_primary_bcc_remaining_account_related_emails(
      template_name: :request_declined_by_staff,
      subject: "[GitHub] Account recovery request declined",
      categories: "account-security",
    )
  end
end
