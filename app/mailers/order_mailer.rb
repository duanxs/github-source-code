# frozen_string_literal: true

class OrderMailer < ApplicationMailer
  self.mailer_name = "mailers/order"

  # Public: Notify staff that an Order confirmation attempt has failed.
  #
  # confirmation_token - Confirmation token for the order
  # exception - Exception that was raised when trying to confirm the order
  #
  # Returns a Mail instance to deliver.
  def confirm_failed(confirmation_token, exception)
    @confirmation_token = confirmation_token
    @backtrace = [exception.message, *Array(exception.backtrace)].join("\n")

    mail(
      to: redalert,
      from: github,
      subject: "[GitHub] A customer was unable to confirm their order",
    )
  end
end
