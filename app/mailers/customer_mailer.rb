# frozen_string_literal: true

class CustomerMailer < ApplicationMailer
  self.mailer_name = "mailers/customer"

  def verify(customer_account)
    @customer_account = customer_account
    @customer         = customer_account.customer
    @target           = customer_account.user
    recipients        = user_or_admin_recipients(@target)

    mail(
      from: github,
      to: recipients[:to],
      bcc: recipients[:bcc],
      subject: "[GitHub] Please Verify Billing Change for @#{@target.login}",
    )
  end

  def verified(customer_account)
    @customer = customer_account.customer
    @target   = customer_account.user
    recipients = user_or_admin_recipients(@target)

    mail(
      from: github,
      to: recipients[:to],
      bcc: recipients[:bcc],
      subject: "[GitHub] Confirmed Billing Change for @#{@target.login}",
    )
  end

  def claim_new_organization(organization, admin, token)
    @organization      = organization
    @organization_name = @organization.profile_name || "@#{@organization.login}"
    @admin             = admin
    @token             = token

    mail(
      from: github,
      to: user_email(admin, GitHub.newsies.email(admin, organization).value),
      subject: "[GitHub] Setup your new organization: #{@organization_name}",
    )
  end
end
