# frozen_string_literal: true

class ExploreMailer < ApplicationMailer
  include GitHub::RecurringMailer

  self.mailer_name = "mailers/explore"

  helper :email_link_tracking

  # Build an explore overview email and send it to the supplied user.
  #
  # - user
  #    The person that the email is being sent to
  # - period (default 'weekly')
  #    The time period that should be used `'daily', 'weekly', monthly'`
  # - email
  #    Override user's email, used for testing purposes via devtools
  #
  # returns true if the mail was sent
  def overview(user:, period: "weekly", email: nil, auto_subscribed: false)
    return false unless user.present?

    @marketplace_listing = Marketplace::Listing.verified_and_shuffled(limit: 1).first
    @featured_collection = ExploreCollection.featured_and_shuffled(limit: 1).first
    @spotlights_in_feed = ExploreFeed::Spotlight.all.current.to_a.shuffle
    @repository_recommendations = RepositoryRecommendation
      .filtered_for(user, page: 1, per_page: 3) || []
    @recommended_topic = ExploreFeed::Recommendation::Topic.all(for_user: user).spotlight
    @trending_developers = ExploreFeed::Trending::Developer.all(period: period)
      .limit(Mailers::Explore::OverviewView::TRENDING_LIST_LENGTH)
    @trending_repositories = ExploreFeed::Trending::Repository.all(period: period)
      .limit(Mailers::Explore::OverviewView::TRENDING_LIST_LENGTH)
    @event = ExploreFeed::Event.all.upcoming_or_current.sample
    @view = Mailers::Explore::OverviewView.new(
      user: user,
      period: period,
      email: email,
      auto_subscribed: auto_subscribed,
    )
    @image_url = image_base_url

    premail(
      to: user_email(user, @view.email),
      subject: @view.title,
      categories: "explore,explore-period-#{period}",
    )
  end
end
