# frozen_string_literal: true

class BusinessMailer < ApplicationMailer
  include ApplicationHelper

  helper :avatar

  self.mailer_name = "mailers/business"

  layout "business/business_email_layout", only: [
    :invited_as_business_admin,
    :invited_as_business_admin_by_email,
    :invited_organization,
    :added_as_business_admin,
    :removed_as_business_admin,
    :removed_as_business_member,
    :removed_member_from_organizations,
    :removed_outside_collaborator_from_organizations,
    :admin_role_changed,
  ]

  # Public: Notify staff of a Partner Program request.
  #
  # form - Form params from partner program landing page.
  #
  # Returns a Mail instance to deliver.
  def partner_request(form)
    @form = form

    mail(
      to: "channels@github.com",
      from: github,
      subject: "Partner Program Request",
    )
  end

  def failed_two_factor_enforcement(actor, business)
    @actor = actor
    @business = business

    subject = "[GitHub] Failed to enable and enforce two-factor requirement for #{business.name} enterprise"

    mail(
      from: github_noreply,
      to: user_email(actor, GitHub.newsies.email(actor).value),
      subject: subject,
    )
  end

  # Public: Notify user that they have been invited to be a business owner.
  #
  # invitation - The BusinessMemberInvitation
  # stafftools_invite - A Boolean indicating whether the invitation was initiated
  #   by GitHub staff from /stafftools. Defaults to false.
  #
  def invited_as_business_admin(invitation, stafftools_invite = false)
    @invitation = invitation
    @user = invitation.invitee
    @inviter = invitation.inviter
    @business = invitation.business
    @show_inviter = !stafftools_invite && invitation.show_inviter?

    # Use the inviter's email address as the reply-to email if the inviter
    # exists and has a valid email address. Fall back to the support
    # address if not.
    reply_to = user_email(invitation.inviter, allow_private: false) if @show_inviter
    reply_to ||= github

    subject = if @show_inviter
      "[GitHub] @#{@inviter.login} has invited you to become an owner of the #{@business.name} enterprise"
    else
      "[GitHub] You’re invited to become an owner of the #{@business.name} enterprise"
    end

    premail(
      from: github,
      reply_to: reply_to,
      to: user_email(@user),
      subject: subject,
    )
  end

  # Public: Notify user that they have been invited to be a business owner via
  # only an email address.
  #
  # invitation - The BusinessMemberInvitation
  # invitation_token - The String token for the email invitation.
  # stafftools_invite - A Boolean indicating whether the invitation was initiated
  #   by GitHub staff from /stafftools. Defaults to false.
  #
  def invited_as_business_admin_by_email(invitation, invitation_token, stafftools_invite = false)
    @invitation = invitation
    @invitation_token = invitation_token
    @email = invitation.email
    @inviter = invitation.inviter
    @business = invitation.business
    @show_inviter = !stafftools_invite && invitation.show_inviter?

    # Use the inviter's email address as the reply-to email if the inviter
    # exists and has a valid email address. Fall back to the support
    # address if not.
    reply_to = user_email(invitation.inviter, allow_private: false) if @show_inviter
    reply_to ||= github

    subject = if @show_inviter
      "[GitHub] @#{@inviter.login} has invited you to become an owner of the #{@business.name} enterprise"
    else
      "[GitHub] You’re invited to become an owner of the #{@business.name} enterprise"
    end

    premail(
      from: github,
      reply_to: reply_to,
      to: @email,
      subject: subject,
    )
  end

  # Public: Notify user that their invitation to become a business owner has
  # been cancelled.
  #
  # invitation - The BusinessMemberInvitation
  #
  def business_admin_invitation_cancelled(invitation)
    @invitation = invitation
    @email = @invitation.email? ? @invitation.email : user_email(@invitation.invitee)
    @business = @invitation.business

    mail(
      from: github,
      to: @email,
      subject: "[GitHub] Your invitation to become an owner of #{@business.name} has been canceled",
    )
  end

  # Public: Notify a user that they have been added as an enterprise account
  # admin (owner or billing manager).
  #
  # Note: This is currently only used when GitHub staff directly add admins to
  # an enterprise account, and the regular invitation workflow is bypassed.
  #
  # business - The Business.
  # admin_role - A Symbol representing the role of the admin. Currently :owner
  #   or :billing_manager.
  # user - The User that was added.
  #
  def added_as_business_admin(business, admin_role, user)
    @business = business
    @user = user
    @role_for_email = role_for_business_email(admin_role)

    subject = "[GitHub] You’ve been added as #{@role_for_email} of the #{@business.name} enterprise"

    premail(
      from: github,
      to: user_email(@user),
      subject: subject,
    )
  end

  # Public: Notify a user that they have been removed as an enterprise account
  # admin (owner or billing manager).
  #
  # Called when they are removed via the UI, a GraphQL call, or because they had
  # become non-compliant with 2fa policy
  #
  # business - The Business.
  # admin_role - A Symbol representing the role of the admin. Currently :owner
  #   or :billing_manager.
  # user - The User that was removed.
  # reason - (optional) the reason the user was removed
  #
  def removed_as_business_admin(business, admin_role, user, reason = nil)
    @business = business
    @user = user
    @role_for_email = role_for_business_email(admin_role)
    @reason = reason&.to_sym

    subject = "[GitHub] You’ve been removed as #{@role_for_email} of the #{@business.name} enterprise"

    premail(
      from: github,
      to: user_email(@user),
      subject: subject,
    )
  end

  # Public: Notify a user that they have been removed from an enterprise account.
  #
  # Called when they are removed via a GraphQL call (or in the future via the UI)
  #
  # business - The Business.
  # user - The User that was removed.
  # user_roles - The roles that the user lost as a result of being removed. Could be any of :owner,
  #   :billing_manager, :member, and/or :outside_collaborator
  # organizations - (optional) the Organizations the user was removed from
  #
  def removed_as_business_member(business, user, user_roles, organizations)
    @business = business
    @user = user
    @organizations = organizations
    @org_count = @organizations.length
    @lookup_org_owner_help_doc_url = OrganizationMailer::LOOKUP_ORG_OWNER_HELP_DOC_URL
    @roles = Array(user_roles).map { |role| role_for_business_email(role) }

    # users might be admins and not belong to any organizations
    subject_parts = ["[GitHub] You’ve been removed from"]
    subject_parts << "#{@org_count} #{"organization".pluralize(@orgs_count)} in" if @org_count > 0
    subject_parts << "the #{@business.name} enterprise"

    premail(
      from: github,
      to: user_email(@user),
      subject: subject_parts.join(" "),
    )
  end

  # Public: Notify an enterprise admin when their role within the enterprise has
  # changed (owner <=> billing manager)
  #
  # business        - The Business
  # new_admin_role  - the admin's new role with the enterprise (:owner or :billing_manager)
  # user            - the admin whose role has changed
  #
  def admin_role_changed(business, new_admin_role, user)
    @business = business
    @user = user
    @role_for_email = role_for_business_email(new_admin_role)

    subject = "[GitHub] Your role with the #{@business.name} enterprise has changed, and you are now #{@role_for_email}."

    premail(
        from: github,
        to: user_email(@user),
        subject: subject,
    )
  end

  def invited_as_business_billing_manager(invitation, invitation_token)
    @invitation = invitation
    @user = invitation.invitee
    @inviter = invitation.inviter
    @business = invitation.business
    @url = if invitation.email?
      billing_manager_invitation_enterprise_url(@business, invitation_token: invitation_token)
    else
      billing_manager_invitation_enterprise_url(@business)
    end

    # Use the inviter's email address as the reply-to email if the inviter
    # exists and has a valid email address. Fall back to the support address if
    # not.
    if invitation.show_inviter?
      reply_to = user_email(@inviter, allow_private: false)
    end
    reply_to ||= github

    subject = if invitation.show_inviter?
      "[GitHub] @#{@inviter.login} has invited you to be a billing manager for the #{@business.name} enterprise"
    else
      "[GitHub] You're invited to be a billing manager for the #{@business.name} enterprise"
    end

    mail(
      from: github,
      to: invitation.email? ? invitation.email : user_email(@user),
      subject: subject,
      reply_to: reply_to,
    )
  end

  # Public: Notify organization that they have been invited to be a part of a business.
  #
  # invitation - The BusinessOrganizationInvitation
  #
  def invited_organization(invitation)
    @invitation = invitation
    @organization = invitation.invitee
    @inviter = invitation.inviter
    @business = invitation.business

    # Use the inviter's email address as the reply-to email if the inviter
    # exists and has a valid email address. Fall back to the support
    # address if not.
    reply_to = user_email(@inviter, allow_private: false) if invitation.show_inviter?
    reply_to ||= github

    # Use the organization email if it exists.
    # Fall back to organization billing email if it exists.
    # Finally fall back to organization owner email address.
    send_to = @organization.profile_email || @organization.billing_email || user_email(@organization.admins.first)

    subject = if invitation.show_inviter?
      "[GitHub] @#{@inviter.login} has invited #{@organization.name} to join #{@business.name} enterprise"
    else
      "[GitHub] #{@organization.name} has been invited to to join #{@business.name} enterprise"
    end

    premail(
      from: github,
      reply_to: reply_to,
      to: send_to,
      subject: subject,
    )
  end

  # Public - Notify a user that they've been removed from the Organizations in the Business
  # due to 2FA non-compliance
  #
  # user - the business member we're notifying
  # business - the Business that's enabling 2fa
  # organizations - list of oranizations in the Business that the user belongs to
  def removed_member_from_organizations(user, business, organizations)
    return if organizations.empty?

    @user = user
    @business = business
    @organizations = organizations
    @orgs_count = @organizations.length
    @lookup_org_owner_help_doc_url = OrganizationMailer::LOOKUP_ORG_OWNER_HELP_DOC_URL

    subject = "[GitHub] You've been removed from #{@orgs_count} #{"organization".pluralize(@orgs_count)} in the #{@business.name} enterprise"

    premail(
        from: github_noreply,
        to: user_email(@user),
        subject: subject,
        categories: "org,org-remove-member",
    )
  end

  # Public - Notify a user that their outside collaborator status has been removed from
  # repositories that belong to this Business due to 2FA non-compliance
  #
  # user - the outside collaborator we're notifying
  # business - the Business that's enabling 2fa
  # repository_names - array of repository names that the user is losing access to
  #                    (:name_with_owner)
  def removed_outside_collaborator_from_organizations(user, business, repository_names)
    return if repository_names.empty?

    @user = user
    @business = business
    @repo_count = repository_names.length
    @repository_names = repository_names
    @lookup_org_owner_help_doc_url = OrganizationMailer::LOOKUP_ORG_OWNER_HELP_DOC_URL

    subject = "[GitHub] You've been removed from #{@repo_count} #{"repository".pluralize(@repo_count)} in the #{@business.name} enterprise"

    premail(
        from: github_noreply,
        to: user_email(@user),
        subject: subject,
        categories: "org,org-remove-outside-collaborator",
    )
  end

  private

  # Private - translate a business admin role into a string to be used in an email
  #
  # member_role - string or symbol for the member's role. Currently :owner, :billing_manager,
  #   :member, or :outside_collaborator
  #
  # Returns: a string. Raises an ArgumentError exception if member_role is invalid
  #
  def role_for_business_email(member_role)
    case member_role.to_sym
    when :owner
      "an owner"
    when :billing_manager
      "a billing manager"
    when :member
      "a member"
    when :outside_collaborator
      "a collaborator"
    else
      raise ArgumentError, "admin_role argument value #{member_role} is invalid"
    end
  end
end
