# frozen_string_literal: true

class WorksWithGitHubMailer < ApplicationMailer
  self.mailer_name = "mailers/works_with_github"

  def listing_approved(listing:, message: nil)
    subject = "#{listing.name} has been approved for Works with GitHub"

    @listing = listing
    @message = message

    mail(from: github_partnerships, to: user_email(listing.creator), subject: subject)
  end

  def listing_delisted(listing:, message: nil)
    subject = "#{listing.name} has been removed from Works with GitHub"

    @listing = listing
    @message = message

    mail(from: github_partnerships, to: user_email(listing.creator), subject: subject)
  end

  def listing_rejected(listing:, message: nil)
    subject = "#{listing.name} has been rejected for Works with GitHub"

    @listing = listing
    @message = message

    mail(from: github_partnerships, to: user_email(listing.creator), subject: subject)
  end
end
