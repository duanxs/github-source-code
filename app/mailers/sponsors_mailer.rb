# frozen_string_literal: true

class SponsorsMailer < ApplicationMailer
  include ApplicationHelper
  helper :avatar

  self.mailer_name = "mailers/sponsors"

  def access_request(sponsorable_login:, sponsorable_name:, sponsorable_email:, sponsorable_country:, sponsorable_two_factor_enabled:)
    subject = "ACTION REQUIRED: @#{sponsorable_login} has requested to join GitHub Sponsors"

    @sponsorable_login = sponsorable_login
    @sponsorable_name = sponsorable_name
    @sponsorable_email = sponsorable_email
    @sponsorable_country = sponsorable_country
    @sponsorable_two_factor_enabled = sponsorable_two_factor_enabled

    mail(from: github_noreply, to: "invoices@github.com", subject: subject)
  end

  def waitlist_acceptance(sponsorable:)
    subject = "You're in! Welcome to GitHub Sponsors 💖"
    contact_email = sponsorable.sponsors_membership.contact_email_address

    @sponsorable = sponsorable
    @membership = sponsorable.sponsors_membership
    @is_user = sponsorable.user?

    mail(from: github_noreply, to: contact_email, subject: subject)
  end

  def approval_request_submitted(sponsorable:)
    subject = "Your GitHub Sponsors profile has been submitted for review"
    recipients = listing_admin_recipients(sponsorable)
    mail(
      from: github_noreply,
      to: recipients[:to],
      bcc: recipients[:bcc],
      subject: subject,
    )
  end

  def notify_org_admins(org:, actor:)
    @org = org
    @actor = actor
    admins = org.admins

    recipients = build_admin_recipients_list(org, admins)

    mail(
      from: github_noreply,
      to: recipients[:to],
      bcc: recipients[:bcc].concat(bcc_log),
      subject: "You can now access @#{org}’s sponsorship dashboard!",
    )
  end

  def new_sponsor(sponsorship)
    @sponsorship = sponsorship
    @sponsorable = sponsorship.sponsorable
    @sponsor = sponsorship.sponsor
    listing = @sponsorable.sponsors_listing
    membership = @sponsorable.sponsors_membership

    @subject = @sponsorable.user? ? "you" : "@#{@sponsorable.login}"
    billing_cycle = @sponsor.plan_duration
    amount = sponsorship.tier.base_price(duration: billing_cycle)
    @sponsorship_amount_cycle = "$#{amount}/#{billing_cycle}"
    @is_first_sponsorship = sponsorship.first_for?(sponsorable: @sponsorable)
    @needs_tax_form = @sponsorable.sponsors_docusign_enabled? && !listing.docusign_completed?
    @next_payout_date = listing.next_payout_date_formatted
    @on_payout_probation = listing.on_payout_probation?

    subject = "You have a new #{"private " if @sponsorship.privacy_private?}" \
              "#{@sponsorship_amount_cycle} sponsor on GitHub Sponsors " \
              "(@#{@sponsor.login})!"
    contact_email = membership.contact_email_address

    mail(from: github_noreply, to: contact_email, subject: subject)
  end

  def now_sponsoring(sponsorable_login:, sponsor:, sponsorship_amount:,
                     sponsor_billing_cycle:, sponsor_next_billing_date:, actor:)
    subject = "You're now sponsoring @#{sponsorable_login} on GitHub Sponsors!"
    recipient = sponsor.organization? ? actor : sponsor

    @sponsor = sponsor
    @sponsorable_login = sponsorable_login
    @sponsorship_amount = sponsorship_amount
    @sponsor_billing_cycle = sponsor_billing_cycle
    @sponsor_next_billing_date = sponsor_next_billing_date.strftime("%B %-d, %Y")

    mail(from: github_noreply, to: user_email(recipient), subject: subject)
  end

  def waitlist_confirmation(sponsorable:, waitlist_title:)
    contact_email = sponsorable.sponsors_membership.contact_email_address

    @waitlist_title = waitlist_title
    @types = if sponsorable.user?
      "developers"
    elsif sponsorable.organization?
      "organizations"
    end
    mail(from: github_opensource,
         to: contact_email,
         subject: "You're on the #{waitlist_title}")
  end

  def newsletter(sponsorable:, sponsors:, subject:, text_body:, html_body:)
    sponsorable_email = user_email(sponsorable, allow_private: false) if sponsorable.user?
    sponsorable_email ||= github_noreply(sponsorable)

    newsletter_bcc = sponsors.map { |sponsor| user_email(sponsor, sponsor.sponsors_update_email) }
    mail(from: github_noreply(sponsorable),
         reply_to: sponsorable_email,
         to: github_noreply,
         bcc: newsletter_bcc,
         subject: "[@#{sponsorable.login} GitHub Sponsors Update] #{subject}") do |format|
      format.text { text_body }
      format.html { html_body }
    end
  end

  def sponsorships_export(sponsorable:, filename:, mime_type:, export_content:, export_year:, export_month:)
    @sponsorable = sponsorable
    @export_year = export_year
    @export_month = export_month

    attachments[filename] = {
      mime_type: mime_type,
      content: export_content,
    }

    mail(
      from: github_noreply,
      to: sponsorable.sponsors_membership.contact_email_address,
      subject: "Your GitHub Sponsors #{@export_month} #{@export_year} sponsorships export is ready!",
    )
  end

  def reached_match_cap(sponsorable:)
    @sponsorable_login = sponsorable.login
    @match_limit = Billing::Money.new(Sponsors::MATCHING_LIMIT_AMOUNT_IN_CENTS)
                                 .format(no_cents: true)
    @sponsorships_plus_match = Billing::Money.new(Sponsors::MATCHING_LIMIT_AMOUNT_IN_CENTS*2)
                                             .format(no_cents: true)

    mail(
      from: github_noreply,
      to: sponsorable.sponsors_membership.contact_email_address,
      subject: "Wow, you've received #{@match_limit} in GitHub Sponsors sponsorships!",
    )
  end

  # This is an arbitrary limit used to make sure we don't
  # send an email with a long list of sponsors. It can be modified if needed.
  GOAL_CONTRIBUTORS_LIMIT = 40
  def goal_completed(goal:)
    @goal = goal
    @sponsorable = @goal.listing.sponsorable
    @contributions_count = @goal.contributions.count
    @sponsors = @goal
      .contributions
      .preload(:sponsor)
      .limit(GOAL_CONTRIBUTORS_LIMIT)
      .map(&:sponsor)

    recipients = listing_admin_recipients(@sponsorable)

    mail(
      from: github_noreply,
      to: recipients[:to],
      bcc: recipients[:bcc],
      subject: "You reached your goal!",
    )
  end

  private

  def listing_admin_recipients(sponsorable)
    sponsorable_contact = sponsorable
      .sponsors_membership
      .contact_email_address

    if sponsorable.organization?
      recipients = build_admin_recipients_list(sponsorable, sponsorable.admins)
      recipients[:bcc] << sponsorable_contact
      recipients
    else
      {
        to: sponsorable_contact,
        bcc: [],
      }
    end
  end
end
