# frozen_string_literal: true

class AdvancedSecurityBetaMailer < ApplicationMailer
  self.mailer_name = "mailers/advanced_security_beta"

  def waitlist_acceptance(membership)
    @membership = membership
    @repo_types = repo_types(membership.member)
    @secret_scanning = membership.member.advanced_security_private_beta_enabled?
    @products = @secret_scanning ? "code scanning and secret scanning" : "code scanning"
    @betas = "#{@products} " + (@secret_scanning ? "betas" : "beta")

    settings = GitHub.newsies.settings(membership.actor)
    email_address = if membership.member.organization?
      settings.email(membership.member).address
    else
      settings.email(:global).address
    end

    mail(
      from: github_noreply,
      to: user_email(membership.actor, email_address),
      subject: "You're in! Get started with #{@products} 💖",
    )
  end

  def repo_types(member)
    if member.advanced_security_public_beta_enabled? && member.advanced_security_private_beta_enabled?
      "public and private"
    elsif member.advanced_security_private_beta_enabled?
      "private"
    else
      "public"
    end
  end
end
