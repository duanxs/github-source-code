# frozen_string_literal: true

class CodespacesMailer < ApplicationMailer
  self.mailer_name = "mailers/codespaces"

  SUBJECT = "You're now in the Codespaces beta!"

  def waitlist_acceptance(membership)
    @membership = membership

    mail(
      from: github_noreply,
      to: user_email(membership.actor),
      subject: SUBJECT,
    )
  end
end
