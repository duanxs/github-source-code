# rubocop:disable Style/FrozenStringLiteralComment

# Tracks files uploaded and stored by users. This is used by the drag-and-drop
# image uploader on conversation forms.
class UserAsset < ApplicationRecord::Domain::Assets
  include ::Storage::Uploadable
  include Instrumentation::Model
  include UserAsset::Quarantine

  set_uploadable_policy_path :assets
  set_cache_age 1.year
  set_content_types :images

  belongs_to :storage_blob, class_name: "Storage::Blob"
  belongs_to :uploader, class_name: "User", foreign_key: :user_id
  has_many :attachments, foreign_key: :asset_id, dependent: :delete_all

  before_validation :set_guid, on: :create
  validate :storage_ensure_inner_asset
  validates_inclusion_of :content_type,
    in: lambda { |asset| asset.class.allowed_content_types }
  validates_presence_of :name
  validate :extension_matches_content_type
  validates_inclusion_of :size, in: 1..10.megabytes

  before_create :choose_storage_provider
  before_destroy :storage_delete_objects

  after_commit :instrument_uploaded, if: :saved_change_to_state?

  enum state: Storage::Uploadable::STATES

  # Public: Filter user assets to a subset of the given list by removing those assets that
  # have been determined to be illegal or of extreme violence.
  # See github.com/github/schaefer for more
  scope :subset_without_known_photo_dna_hits, ->(user_assets) do
    ids_of_known_hits = PhotoDnaHit.for_user_asset(user_assets).pluck(:content_id)
    where(id: user_assets).where.not(id: ids_of_known_hits)
  end

  def instrument_uploaded
    return true unless uploaded?

    if GitHub.flipper[:schaefer_instrument_upload].enabled?(uploader)
      GlobalInstrumenter.instrument "user_asset.uploaded", {
        user_asset: self,
        upload_ip: GitHub.context[:actor_ip],
      }
    end
  end

  # BEGIN storage settings

  def purge
    return destroy if GitHub.storage_cluster_enabled?

    urls = [storage_external_url]
    with_storage_provider(:default) do
      urls << storage_external_url
    end

    destroy

    urls.compact.each do |url|
      PurgeFastlyUrlJob.perform_later({"url" => url})
    end
  end

  def storage_policy(actor: nil, repository: nil)
    klass = if GitHub.storage_cluster_enabled?
      ::Storage::ClusterPolicy
    else
      ::Storage::S3Policy
    end
    klass.new(self, actor: actor, repository: repository)
  end

  # deletes from old and new aws accounts
  def storage_delete_objects
    if storage_provider == :s3_production_data
      with_storage_provider(:default) do
        storage_delete_object
      end
    end
    storage_delete_object
  end

  # Temporarily changes the storage provider to build URLs for the old S3
  # account. Necessary hack to purge cloud.githubusercontent.com until it
  # proxies from the new S3 account exclusively. Not needed in GHE.
  def with_storage_provider(prov)
    orig = read_attribute(:storage_provider)
    self.storage_provider = prov == :default ? nil : prov.to_s
    yield
  ensure
    self.storage_provider = orig
  end

  def self.storage_new(uploader, blob, meta)
    new(
      storage_blob: blob,
      uploader: uploader,
      content_type: meta[:content_type],
      name: meta[:name],
    )
  end

  def self.storage_create(uploader, blob, meta)
    storage_new(uploader, blob, meta).tap do |file|
      file.update(state: :uploaded)
    end
  end

  def storage_external_url(_ = nil)
    if GitHub.storage_cluster_enabled?
      return "#{GitHub.storage_cluster_url}/user/#{user_id}/files/#{guid}"
    end

    if storage_provider == :s3_production_data
      url = if cdn_url = GitHub.user_images_cdn_url
        cdn_url + storage_s3_key(policy = nil)
      else
        storage_policy.download_url
      end
      return url
    end

    "#{GitHub.asset_url_host}#{storage_s3_key(policy=nil)}"
  end

  def storage_blob_accessible?
    uploaded?
  end

  def creation_url
    "#{GitHub.storage_cluster_url}/user/#{user_id}/files"
  end

  def upload_access_grant(actor)
    Api::AccessControl.access_grant(
      verb: :write_user_files,
      user: actor,
      owner: uploader,
    )
  end

  # cluster settings

  def storage_cluster_url(policy)
    creation_url + "/#{guid}"
  end

  def storage_cluster_download_token(policy)
    # no token needed for downloads
  end

  def storage_upload_path_info(policy)
    "/internal/storage/user/#{user_id}/files"
  end

  # Public: Describes the filters that are run when images are uploaded.
  def alambic_upload_filters
    {
      original: [
        {
          command: :imagemagick,
          params: {
            max_width: "10000",
            max_height: "10000",
            strip: :exif,
            orient: :auto,
          },
        },
      ],
    }
  end

  # s3 storage settings

  def storage_s3_client
    if storage_provider == :s3_production_data
      GitHub.s3_production_data_client
    else
      GitHub.s3_primary_client
    end
  end

  def storage_s3_bucket
    if storage_provider == :s3_production_data
      self.class.storage_s3_new_bucket
    else
      self.class.storage_s3_bucket
    end
  end

  def storage_s3_key(policy)
    if storage_provider == :s3_production_data
      "#{user_id}/#{id}-#{guid}#{File.extname(name)}"
    else
      "#{GitHub.asset_base_path}/#{user_id}/#{id}/#{guid}#{File.extname(name)}"
    end
  end

  def storage_s3_access_key
    if storage_provider == :s3_production_data
      GitHub.s3_production_data_access_key
    else
      GitHub.s3_environment_config[:access_key_id]
    end
  end

  def storage_s3_secret_key
    if storage_provider == :s3_production_data
      GitHub.s3_production_data_secret_key
    else
      GitHub.s3_environment_config[:secret_access_key]
    end
  end

  def storage_s3_access
    :public
  end

  def storage_s3_upload_header
    {
      "Content-Type" => content_type,
      "Cache-Control"=>"max-age=2592000",
      "x-amz-meta-Surrogate-Control"=>"max-age=31557600",
    }
  end

  def storage_transition_ready?
    uploader && storage_blob_accessible?
  end

  # END storage settings

  def storage_policy_api_url
    "/assets/#{guid}"
  end

  # Needed for test/lib/github/transitions/20151211173128_enterprise_storage_cluster_upgrade_test.rb
  def alambic_absolute_local_path
    parts = [GitHub.file_asset_path, GitHub.asset_base_path]
    parts.push *("%08d" % user_id).scan(/..../)
    parts.push *("%08d" % id).scan(/..../)
    parts << (guid + asset_extension)
    parts * "/"
  end

  # Turn AssetScanner::Match objects into Assets to attach them to comments.
  # If the Asset ID is missing, load the Asset from its guid.
  #
  # matches - Array of AssetScanner::Match objects.
  #
  # Returns an Array of Asset objects.
  def self.from_matches(matches)
    asset_ids = []
    guids = []
    asset_id_guids = {} # int ID => string guid
    matches.each do |match|
      if id_string = match.asset_id
        id = id_string.to_i
        next unless id > 0
        asset_id_guids[id] = match.asset_guid
        asset_ids << id
      elsif guid = match.asset_guid
        next if guid.blank?
        guids << guid
      end
    end

    assets = []

    ActiveRecord::Base.connected_to(role: :reading) do
      asset_ids.each_slice(100) do |slice|
        found = UserAsset.where(id: slice).all.select do |user_asset|
          expected_guid = asset_id_guids[user_asset.id]
          # expected_guid is matched in the regex run on the img url
          # if the asset ID and guid are BOTH matched, then require the returned
          # UserAsset to have the same guid.
          expected_guid.blank? || user_asset.guid == expected_guid
        end
        assets.push(*found) if found.present?
      end

      guids.each_slice(100) do |slice|
        found = UserAsset.where(guid: slice).all
        assets.push(*found) if found.present?
      end
    end

    assets.uniq!
    assets
  end

  def self.storage_s3_bucket
    GitHub.s3_environment_config[:asset_bucket_name]
  end

  def self.storage_s3_new_bucket
    GitHub.s3_user_asset_new_bucket
  end

  def self.storage_s3_new_bucket_host
    GitHub.s3_user_asset_new_host
  end

  def uploadable_surrogate_key
    "user-#{user_id}"
  end

  def choose_storage_provider
    return if GitHub.storage_cluster_enabled?
    self.storage_provider = :s3_production_data
  end

  def event_prefix() :assets end

  def self.instrument_user_assets_batch_scan(payload)
    GitHub.instrument "user_asset.batch_scan", payload
  end

  def self.instrument_repository_assets_batch_scan(payload)
    GitHub.instrument "repository_asset.batch_scan", payload
  end
end
