# frozen_string_literal: true

class SecurityAdvisoryForceVisibility < SecurityAdvisory
  def visible_publicly?
    true
  end

  # There is no permalink for a whitesource advisory, but we have to override
  # this method because visible_publicly? is always true in this class, but we
  # still need to return nil for a permalink for non-publicly-visible advisories.
  def permalink(include_host: true)
    return nil unless (cve_id.present? || white_source_id.blank?)

    super(include_host: include_host)
  end
end
