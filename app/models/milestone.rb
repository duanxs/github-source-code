# rubocop:disable Style/FrozenStringLiteralComment

class Milestone < ApplicationRecord::Domain::Repositories
  areas_of_responsibility :projects
  include GitHub::UTF8
  include GitHub::UserContent
  include Instrumentation::Model
  include GitHub::Relay::GlobalIdentification
  include GitHub::Prioritizable::Context
  extend GitHub::BackgroundDependentDeletes
  include Spam::Spammable

  include Milestone::PrioritizationDependency

  States = %w(open closed)

  MAX_DESCRIPTION_LENGTH = 8 * 1024

  has_many :issues, inverse_of: :milestone
  nullify_dependents_in_background :issues

  has_many :issue_priorities, inverse_of: :milestone, validate: false
  destroy_dependents_in_background :issue_priorities

  prioritizes :issues, with: :issue_priorities, by: :priority,
    counter_cache: :open_issue_count,
    conditions: "issues.state = 'open'"

  has_many :pull_request_issues,
    -> { where("pull_request_id IS NOT NULL") },
    class_name: "Issue"

  belongs_to :repository, inverse_of: :milestones
  alias_method :entity, :repository
  alias_method :async_entity, :async_repository
  belongs_to :created_by, class_name: "User"

  setup_spammable(:created_by)

  validates_uniqueness_of :title, scope: :repository_id, case_sensitive: true
  validates_presence_of   :title
  validates_presence_of   :repository_id
  validates_presence_of   :created_by_id
  validates_inclusion_of  :state, in: States
  validates_length_of     :title, maximum: 255
  validate :ensure_creator_is_not_blocked, on: :create
  validates :description, :body, :title, bytesize: { maximum: MYSQL_UNICODE_BLOB_LIMIT },
    unicode: true, allow_blank: true, allow_nil: true

  before_validation :set_default_state, on: :create
  before_destroy :instrument_delete
  before_save  :set_closed_at
  after_create :set_number!
  after_commit :synchronize_search_index
  after_commit :instrument_create, on: :create
  after_commit :instrument_closed_or_opened, on: :update
  after_commit :instrument_update, on: :update
  after_commit :queue_webhook_delivery, on: :destroy
  after_save :update_issues

  scope :open_milestones,   -> { where(state: "open") }
  scope :closed_milestones, -> { where(state: "closed") }

  scope :sorted_by, -> (order_str, direction) {
    opts = {order: "milestones.updated_at DESC"}

    if !order_str.blank?
      case order_str
      when "due_date", "due_on"
        opts[:order] = "milestones.due_on"
      when "completeness"
        opts[:order] = "(milestones.closed_issue_count / (milestones.open_issue_count+milestones.closed_issue_count))"
      when "count"
        opts[:order] = "milestones.open_issue_count + milestones.closed_issue_count"
      when "created_at"
        opts[:order] = "milestones.created_at"
      when "number"
        opts[:order] = "milestones.number"
      when "title"
        opts[:order] = "milestones.title"
      else
        opts[:order] = "milestones.updated_at"
      end
      opts[:order] << (direction == "asc" ? " ASC" : " DESC")
    end

    order(Arel.sql(opts[:order]))
  }

  scope :with_issues,    -> { where("closed_issue_count > 0 OR open_issue_count > 0") }
  scope :without_issues, -> { where("closed_issue_count = 0 AND open_issue_count = 0") }

  def pull_requests
    pull_request_issues.all.map { |issue| issue.pull_request }
  end

  def open_pull_requests
    pull_request_issues.open_issues.map { |issue| issue.pull_request }
  end

  def closed_pull_requests
    pull_request_issues.closed_issues.map { |issue| issue.pull_request }
  end

  def open_issues
    issues.open_issues
  end

  def closed_issues
    issues.closed_issues
  end

  def open_issues_count
    issues.without_pull_requests.count
  end

  def open_pull_requests_count
    pull_request_issues.open_issues.count
  end

  def body
    description
  end

  extend GitHub::Encoding
  force_utf8_encoding :description, :title

  def description_before_type_cast
    utf8(super)
  end

  def title_before_type_cast
    utf8(super)
  end

  def user_id
    user.id
  end

  def user
    created_by
  end

  def last_modified_at
    @last_modified_at ||= last_modified_with :created_by
  end

  # we have to do this until Rails 3
  def labels
    Label.for_milestone(self.id)
  end

  def to_param
    number.to_s
  end

  ############################################################################
  ## Search

  # Public: Synchronize this milestone with it's representation in the search
  # index. If the milestone is newly created or modified in some fashion, then
  # it will be updated in the search index. If the milestone has been
  # destroyed, then it will be removed from the search index. This method
  # handles both cases.
  #
  def synchronize_search_index
    if self.destroyed?
      RemoveFromSearchIndexJob.perform_later("milestone", self.id, self.repository_id)
    else
      Search.add_to_search_index("milestone", self.id)
    end
    self
  end

  def progress_percentage
    total = (self.open_issue_count + self.closed_issue_count).to_f
    total_closed = self.closed_issue_count.to_f
    total == 0 ? 0 : (total_closed/total)*100
  end

  # Private: If the title of the milestone has changed, then we need to
  # update the search records for all the issues and pull requests associated
  # with this milestone.  Called via an after_save.
  def update_issues
    return self unless saved_change_to_title?

    issues.each do |issue|
      if issue.pull_request_id
        issue.pull_request.synchronize_search_index
      else
        issue.synchronize_search_index
      end
    end

    self
  end
  private :update_issues

  # Generate demilestoned events for all open issues that were assigned to this
  # milestone.
  def destroy
    unless repo_owner_destroyed?
      open_issues.each do |issue|
        Milestone.transaction do
          issue.events.create \
            event: "demilestoned",
            actor: actor,
            milestone_title: title
        end
      end
    end

    super
  end

  # Absolute permalink URL for this milestone.
  def url
    "%s/%s/milestones/%s" % [
      GitHub.url,
      repository.name_with_owner,
      UrlHelper.escape_path(title),
    ]
  end
  alias_method :permalink, :url

  # Determines whether the milestone is past due based on the viewer's
  # localized time. This may result in a milestone being past_due? in one
  # timezone (where it's the next calendar date) while it's not in another
  # timezone where it's still the due date.
  #
  # Returns a boolean
  def past_due?
    return false unless due_date

    Date.current > due_date
  end

  # The actual due_on timestamp is stored as midnight UTC. We ensure it's still
  # UTC before converting it to a date so that it is consistent for all viewers
  # regardless of timezone.
  #
  # Returns a Date or nil
  def due_date
    due_on.utc.to_date if due_on
  end

  def due_on=(val)
    date =
      if val.present? && valid_date?(val)
        val.to_date.to_time(:utc)
      else
        nil
      end
    super(date)
  end

  # Public: Preserve the actor performing the change, toggle this Milestone to
  # the opposite state, and immediately persist
  def toggle_state!
    self.update! state: next_state
  end

  # Public: What is the next valid state for this Milestone?
  def next_state
    next_state = closed? ? "open" : "closed"
  end

  # Public: Returns truthy if this Milestone is closed
  def closed?
    self.state == "closed"
  end

  # When was this milestone closed?
  # Returns a Date if it's closed, nil if not
  def closed_at
    return if open?

    read_attribute(:closed_at) || updated_at
  end

  def open?
    self.state == "open"
  end

  # Public: Whether the given user can see this milestone.
  def readable_by?(actor)
    repository && repository.readable_by?(actor)
  end

  def notify_subscribers
    channel = GitHub::WebSocket::Channels.milestone_prioritized(self)

    GitHub::WebSocket.notify_repository_channel(repository, channel, {
      timestamp: updated_at.to_i,
      wait: default_live_updates_wait,
      client_uid: GitHub.context[:client_uid],
   })
  end

  # Finds and sets the next number in the sequence scoped by
  # repository_id - this works exactly like Issue numbers
  def set_number!
    sql = github_sql.new(<<-SQL, repository_id: repository_id)
      SELECT MAX(number) + 1 AS num FROM milestones WHERE repository_id = :repository_id FOR UPDATE
    SQL
    update_column(:number, sql.value)
  end

  # The user who performed the action as set in the GitHub request context. If the context doesn't
  # contain an actor, fallback to the ghost user.
  def actor
    @actor ||= (User.find_by_id(GitHub.context[:actor_id]) || User.ghost)
  end

  def event_prefix
    :milestone
  end

  def event_payload(action)
    event_actor = (action == :created ? created_by : actor)
    {
      milestone_id: id,
      actor_id: event_actor.try(:id),
    }
  end

  def instrument_create
    GitHub.instrument "milestone.create", event_payload(:created)
  end

  def instrument_closed_or_opened
    return unless previous_changes["state"].present?

    if state == "closed"
      GitHub.instrument "milestone.close", event_payload(:closed)
    elsif state == "open"
      GitHub.instrument "milestone.open", event_payload(:opened)
    end
  end

  def instrument_update
    return if previous_changes.empty? || previous_changes["state"].present?

    changes_payload = {}.tap do |hash|
      hash[:old_description] = previous_changes["description"].first if previous_changes["description"].present?
      hash[:old_due_on] = previous_changes["due_on"].first if previous_changes["due_on"].present?
      hash[:old_title] = previous_changes["title"].first if previous_changes["title"].present?
    end

    return if changes_payload.empty?

    GitHub.instrument "milestone.update", event_payload(:edited).merge(changes: changes_payload)
  end

  # Public: Queues up the `deleted` milestone event if a milestone is deleted.
  #
  # We *do not* queue a `deleted` event if the repository owner is deleted. It
  # means, most likely, that the user has deleted itself, and this event is
  # firing as part of the callback destruction process. The webhook is eventually
  # fired by `DestroyUserCallbacks#prepare_demilestoned_events`; see also
  # https://git.io/v1L1o for more information.
  #
  # Returns nothing.
  def instrument_delete
    if actor&.spammy? || repo_owner_destroyed?
      @delivery_system = nil
      return
    end

    event = Hook::Event::MilestoneEvent.new(milestone_id: self.id, action: :deleted, actor_id: actor.try(:id), triggered_at: Time.now)
    @delivery_system = Hook::DeliverySystem.new(event)
    @delivery_system.generate_hookshot_payloads
  end

  def queue_webhook_delivery
    return if repo_owner_destroyed?
    raise "`generate_webhook_payload' must be called before `queue_webhook_delivery'" unless defined?(@delivery_system)
    @delivery_system&.deliver_later
  end

  def issues_to_render_for_viewer(viewer, state: :open, page: 1, per_page: GitHub::Prioritizable::MAXIMUM_PRIORITIZABLE_ITEM_COUNT)
    # Load the issues that match the specified state
    issues_to_render =
      if state == :open
        prioritized_issues
      else
        issues.closed_issues.reorder("closed_at DESC")
      end

    # Paginate the remaining issues.
    issues_to_render = issues_to_render.paginate(page: page, per_page: per_page)

    # Prefill the issues' associations.
    GitHub::PrefillAssociations.for_issues(issues_to_render, repository: repository)

    # Prefill status checks.
    pulls = issues_to_render.map(&:pull_request).compact
    if pulls.any?
      GitHub::PrefillAssociations.for_pulls_protected_branches(repository, pulls)
      PullRequest.attach_statuses(repository, pulls)
    end

    if viewer.present?
      # Prefill the issues' read statuses.
      read_issues = Conversation.read_for(viewer, issues_to_render)
      issues_to_render.each do |issue|
        issue.read_by_current_user = read_issues.include?(issue.id)
      end
    end

    issues_to_render
  end

  def memex_column_hash
    {
      id: id,
      number: number,
      state: state,
      title: title,
      url: url,
    }
  end

  def memex_suggestion_hash(selected:)
    memex_column_hash.merge(selected: selected)
  end

  protected
    def set_default_state
      self.state = "open" if self.state.blank?
    end

    # If this milestone was just closed set closed_at to the time.
    # before_save callback
    def set_closed_at
      return unless state_changed?

      if state == "closed"
        self.closed_at = Time.now
      elsif state == "open"
        self.closed_at = nil
      end

      true
    end

    # Implement GitHub::Prioritizable::Context#rebalance_job_class
    def rebalance_job_class
      RebalanceMilestoneJob
    end

  private

  def valid_date?(text)
    !!Date.parse(text.to_s)
  rescue ArgumentError
    false
  end

  def ensure_creator_is_not_blocked
    if user && repository && user.blocked_by?(repository.owner)
      errors.add :user, "is blocked"
    end
  end

  # Internal.
  def max_textile_id
    GitHub.max_textile_milestone_id
  end

  # If the owner is nil, the repo is being destroyed by a user that no longer
  # exists. We'll queue dependent milestone events in the destroy_user_callback
  # sequence
  def repo_owner_destroyed?
    entity.owner.nil?
  end
end
