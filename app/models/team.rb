# rubocop:disable Style/FrozenStringLiteralComment

class Team < ApplicationRecord::Domain::Users
  include Ability::Actor
  include Ability::Subject
  include Ability::Membership

  include Avatar::List::Model
  include GitHub::Relay::GlobalIdentification
  include Instrumentation::Model
  include LdapMapping::Subject
  include SubscribableList
  include PrimaryAvatar::Model
  include Referenceable
  include GitHub::FlipperActor

  # This is necessary under Ruby 2.4 to avoid
  # toplevel constant Permissions referenced by Team::Permissions
  require_dependency "team/permissions"

  include Team::LdapSync
  include Team::Permissions
  include Team::Roles
  include Team::Nested
  include Team::NewsiesAdapter

  class EmptyOwnersError < StandardError ; end
  class FeatureFlagError < StandardError ; end

  TEAM_UPDATE_BATCH_SIZE = 100
  AUTO_SUBSCRIBE_BATCH_SIZE = 100
  DEFAULT_PERMISSION = "pull"
  # Internal: Lookup table mapping team permissions to abilities
  PERMISSIONS_TO_ABILITIES = {
    "admin" => :admin,
    "push"  => :write,
    "pull"  => :read,
    "triage" => :triage,
    "maintain" => :maintain,
  }
  ABILITIES_TO_PERMISSIONS = PERMISSIONS_TO_ABILITIES.invert.with_indifferent_access
  PERMISSIONS = PERMISSIONS_TO_ABILITIES.keys
  ABILITIES = ABILITIES_TO_PERMISSIONS.keys
  # Internal: is a team eligible to become a child of another? "ELIGIBLE" means
  # yes and all others mean no for differenct reasons.
  ELIGIBLITY_STATUSES = {
    secret: "SECRET",
    ancestor: "ANCESTOR",
    child: "CHILD",
    eligible: "ELIGIBLE",
    permission: "PERMISSION",
  }

  enum privacy: { secret: 0, closed: 1 }

  include Team::ExternalIdentityDependency
  include Team::RoleBasedPermissionsDependency
  include Team::UserRankedDependency
  include Team::UserStatusDependency

  require_dependency "team/parent_change/error"
  require_dependency "team/parent_change/lock"
  require_dependency "team/parent_change/payload"

  attr_reader :old_team
  attr_accessor :eligibility_status

  scope :owned_by, -> (org) { where(organization_id: Array(org).map(&:id)) }
  scope :excluding_organization_ids, -> (ids) {
    ids.any? ? where("teams.organization_id NOT IN (?)", ids) : scoped
  }

  scope :with_minimum_privacy, lambda { |privacy|
    where(arel_table[:privacy].gteq(privacies[privacy]))
  }

  scope :legacy_admin, -> { where(permission: "admin").where("name <> 'Owners'") }

  belongs_to :organization, class_name: "User"
  belongs_to :creator, class_name: "User"

  has_many :team_invitations

  has_many :pending_invitations,
    -> { where(OrganizationInvitation.pending.where_values_hash) },
    through: :team_invitations,
    source: :organization_invitation

  has_many :team_membership_requests
  private :team_membership_requests

  has_many :pending_team_membership_requests,
    -> { where(approved_at: nil) },
    class_name: "TeamMembershipRequest"

  has_many :discussion_posts

  has_many :requests_to_parent, class_name: "TeamChangeParentRequest", foreign_key: :parent_team_id, dependent: :destroy
  has_many :requests_to_be_child, class_name: "TeamChangeParentRequest", foreign_key: :child_team_id, dependent: :destroy

  has_many :review_request_delegation_excluded_members, dependent: :destroy

  has_many :user_roles, as: :actor, dependent: :destroy

  has_many :group_mappings, class_name: "Team::GroupMapping"

  alias_attribute :to_s, :name

  validates_presence_of   :name, :organization_id
  validates_inclusion_of :permission, in: PERMISSIONS
  validate :name_and_slug_uniqueness_by_org
  validate :validate_parent_team_is_in_org, :validate_parent_team_does_not_cause_cycle, :validate_parent_not_secret, :validate_secret_or_nested, :validate_parent_has_no_group_mappings

  validates :review_request_delegation_member_count, numericality: [only_integer: true, greater_than: 0, less_than_or_equal_to: Team::ReviewRequestDelegation::DELEGATE_LIMIT]
  enum review_request_delegation_algorithm: { round_robin: 0, load_balance: 1 }, _scopes: false

  before_validation :strip_name
  before_validation :set_slug
  before_validation :restrict_changes_to_owners_team
  before_validation :set_default_permission

  after_commit :instrument_creation, on: :create
  after_create :add_team_as_org_dependent

  after_commit :instrument_update, on: :update

  after_update :update_repository_grants, if: :saved_change_to_permission?
  after_update_commit :instrument_change_privacy, if: :saved_change_to_privacy?
  after_update_commit :instrument_rename, if: :saved_change_to_name?

  after_initialize :reset_parent_team
  after_save :update_tree_path

  # The transaction around Platform::Mutations::UpdateTeam causes that the job is enqueued
  # and sometimes dispatched before the transaction finishes. When this happens, the job can't
  # see the data modified in the unicorn process, thus not being able to apply the team change.
  # For that reason, we moved enqueueing the job to an after commit hook, until a more general
  # solution, like the one documented at the end of https://github.com/github/github/issues/80642#issuecomment-353127221
  # is implemented. Please also read https://github.slack.com/archives/C103DQUPL/p1513866118000286
  # for more context
  after_commit -> { HandleTeamParentChangesJob.perform_later(organization_id) }, if: :enqueue_handle_team_parent_changes_after_commit

  # Public: The total number of team members in this installation (used for
  # enterprise stats).
  def self.total_team_members
    ActiveRecord::Base.connected_to(role: :reading) do
      Ability.github_sql.value <<-SQL, direct: Ability.priorities[:direct]
        SELECT COUNT(1)
        FROM abilities
        WHERE priority   = :direct
        AND actor_type   = 'User'
        AND subject_type = 'Team'
      SQL
    end
  end

  # Public: Given an Array of Team IDs, return all team members as a unique,
  # sorted Array of User IDs. This is a low level method, so try to use methods
  # like Team.members_of(*team_ids) instead.
  def self.user_ids_for(team_ids)
    return [] if team_ids.empty?

    PermissionCache.fetch ["user_ids_for", team_ids] do
      GitHub.instrument "ability.team.user-ids-for" do

        sql = Ability.github_sql.new \
          direct: Ability.priorities[:direct],
          team_ids: team_ids

        sql.add <<-SQL
          SELECT actor_id
          FROM   abilities ab
          WHERE  ab.actor_type   = 'User'
          AND    ab.subject_id  IN :team_ids
          AND    ab.subject_type = 'Team'
          AND    ab.priority     = :direct
        SQL

        sql.values.uniq.sort!
      end
    end
  end

  # Public: Returns teams that have no members.
  #
  # team_ids = teams to inpsect for members
  #
  # Returns an Team AR Relation.
  def self.without_members(team_ids)
    ActiveRecord::Base.connected_to(role: :reading) do
      sql = Ability.github_sql.new \
        team_ids: team_ids,
        priority: [Ability.priorities[:direct], Ability.priorities[:indirect]]

      sql.add <<-SQL
        SELECT DISTINCT
          subject_id
        FROM
          abilities
        WHERE
          subject_id IN :team_ids AND
          subject_type = 'Team' AND
          priority IN :priority AND
          actor_type = 'User'
      SQL

      ids_of_teams_with_members = sql.results
      team_ids = team_ids - ids_of_teams_with_members.flatten

      where(id: team_ids)
    end
  end

  # Public: Given a collection of teams, search them by name and slug using the
  # provided query.
  #
  # Returns: ActiveRecord::Relation of Teams.
  def self.search_name_and_slug(query:, scope: ::Team)
    sanitized_query = ActiveRecord::Base.sanitize_sql_like(query.to_s.strip.downcase).presence
    return scope unless sanitized_query

    scope.where(["name LIKE :query OR slug LIKE :query", query: "%#{sanitized_query}%"])
  end

  # By default abilities are cleared when any ActiveRecord class mixes-in Ability::Participant
  # However, we want to take control over the deletion of team hierarchies to do it
  # more performantly and in a sequential process.
  #
  # See #destroy, which is aliased to #destroy_with_descendants_support
  #
  def clear_abilities_per_destroyed_record?
    false
  end

  # Public: Select the attributes for updating a team
  #
  # In our team/creator and team/editor code we pass along attributes from the controller
  # that also includes ldap_dn (for the ldap sync code) and other attributes. Not all
  # attributes passed from the controller will be attributes that the Team Active Record
  # record can accept.
  #
  # To avoid an unknown attribute error, parse out the attributes just for a Team
  # before sending them to Active Record's `build`, `create`, or `update` methods.
  #
  # Returns a hash of attributes
  #
  def self.team_attributes_hash(attrs)
    team_attrs = Set.new self.column_names.map(&:to_s)
    attrs.select do |k, _|
      team_attrs.include?(k.to_s)
    end
  end

  # Public: Destroy this teams and their descendants by removing the descendants first
  #
  # This is aliased now, when you call `team.destroy` this method is called.
  # And the old destroy method from active record is aliased as
  # `ar_destroy` which is used by DestroyOperation
  #
  # The purpose of this method is to remove in bulk the dependants
  # when the descendants of a team that's being removed, need to be removed as well.
  #
  def destroy_with_descendants_support
    update_attribute(:deleted, true) unless frozen?
    Destruction::DestroyOperation.new(self).execute
    self.freeze
  end
  alias_method :ar_destroy, :destroy
  alias_method :destroy, :destroy_with_descendants_support

  # Defines an active record scope for use in the members connection.
  # A distinct scope is implicitly added to this method. This is because users
  # can be a part of descendant teams and appear multiple times.
  #
  # Returns an active record scope for the team members
  def members_scope(membership: :all, action: nil)
    case membership
    when :immediate
      members(action: action).distinct # distinct scope added here to maintain parity other methods
    when :child_team
      descendant_members(action: action)
    when :all
      descendant_or_self_members(action: action)
    end
  end

  def members_scope_count(membership: :all, action: nil)
    case membership
    when :immediate
      members_count(action: action)
    when :child_team
      descendant_members_count(action: action)
    when :all
      descendant_or_self_members_count(action: action)
    end
  end

  # Returns an active record scope for the repositories with the given affiliation
  # Be careful: like Team#repositories, if you are presenting a list of
  # repositories to someone, don't use this method.
  # Use the GraphQL team repository connection instead.
  #
  # affiliation:
  #  immediate - the repositories that the team has a direct ability for
  #  inherited - the repositories that the team has inherited abilities for
  #  all - the repositories that the team or its ancestors have a direct ability for
  def repositories_scope(affiliation: :all)
    repo_ids = direct_or_inherited_repo_ids(affiliation: affiliation)
    organization.repositories.where(id: repo_ids)
  end

  # Returns an active record scope for the repositories with the given affiliation across many teams
  # Teams must belong to the same organization
  # Be careful: like Team#repositories, if you are presenting a list of
  # repositories to someone, don't use this method.
  # Use the GraphQL team repository connection instead.
  #
  # teams: a list of Team objects. Must all belong to the same organization
  #
  # affiliation:
  #  immediate - the repositories that the team has a direct ability for
  #  inherited - the repositories that the team has inherited abilities for
  #  all - the repositories that the team or its ancestors have a direct ability for
  def self.repositories_scope(teams:, affiliation: :all)
    organization = teams.first.organization
    raise ArgumentError, "All teams must belong to the same organization" unless teams.all? { |t| t.organization_id == organization.id }
    repo_ids = direct_or_inherited_repo_ids(teams: teams, affiliation: affiliation)
    organization.repositories.where(id: repo_ids)
  end

  def repositories_scope_count(affiliation: :all)
    direct_or_inherited_repo_ids(affiliation: affiliation).size
  end

  # Returns a list of repo ids with the given affiliation
  # Be careful: like Team#repositories, if you are presenting a list of
  # repositories to someone, don't use this method.
  # Use the GraphQL team repository connection instead.
  #
  # affiliation:
  #  immediate - the repo ids that the team has a direct ability for
  #  inherited - the repo ids that the team has inherited abilities for
  #  all - the repo ids that the team or its ancestors have a direct ability for
  # sort: sort to apply. note: sort is on abilities table so only fields on
  #  the abilities table are available. ex { action: :desc }
  # action - the permission this team should have on the repositories.
  #          Valid values are :read, :write, or :admin. Default: [] which indicates any.
  #
  # Returns a [repo Id]
  def direct_or_inherited_repo_ids(affiliation: :all, sort: {}, action: [])
    Team.direct_or_inherited_repo_ids(teams: [self], affiliation: affiliation, sort: sort, action: action)
  end

  # Returns a list of repo ids with the given affiliation across many teams
  # All teams must belong to the same repositories
  # Be careful: like Team#repositories, if you are presenting a list of
  # repositories to someone, don't use this method.
  # Use the GraphQL team repository connection instead.
  #
  # teams: a list of Team objects. Must all belong to the same organization
  #
  # affiliation:
  #  immediate - the repo ids that the team has a direct ability for
  #  inherited - the repo ids that the team has inherited abilities for
  #  all - the repo ids that the team or its ancestors have a direct ability for
  # sort: sort to apply. note: sort is on abilities table so only fields on
  #  the abilities table are available. ex { action: :desc }
  # action - the permission this team should have on the repositories.
  #          Valid values are :read, :write, or :admin. Default: [] which indicates any.
  #
  # Returns a [repo Id]
  def self.direct_or_inherited_repo_ids(teams:, affiliation: :all, sort: {}, action: [])
    organization_id = teams.first.organization_id
    raise ArgumentError, "All teams must belong to the same organization" unless teams.all? { |t| t.organization_id == organization_id }

    team_ids = teams.flat_map do |team|
      case affiliation
      when :immediate then team.id
      when :inherited then team.ancestor_ids
      when :all then team.id_and_ancestor_ids
      end
    end

    qry = Ability.where(
      actor_type: "Team",
      actor_id: team_ids,
      subject_type: "Repository",
      priority: Ability.priorities[:direct],
    )

    # avoid unnecessarily expensive IN query if we are querying for all abilities.
    qry = qry.where(action: action) unless action.empty?
    qry = qry.order(sort) unless sort.empty?

    qry.pluck(:subject_id)
  end

  def self.valid_privacy?(privacy)
    privacies[privacy].present?
  end

  # Public: Override Ability::Participant#ability_description, building a
  # string that looks like a team mention.
  def ability_description
    "@#{organization.login}/#{name}"
  end

  # Public: Returns either the User who created the team, or the Organization that the team
  # belongs to.
  #
  # This method is mostly going to be used for front-end display purposes.
  def creator
    super || organization
  end

  # Public: Find a single team given owning Org name and a slug
  #
  # org_name - String name of the owning organization
  # slug - String slug that corresponds to the Team
  #
  # Returns the found Team or nil if none was found
  def self.with_org_name_and_slug(org_name, slug)
    joins(:organization).readonly(false).
      where("users.login" => org_name).
      where("teams.slug" => slug).
      first
  end

  # Find a team by its String combined slug, e.g. "github/web"
  #
  # combined_slug - String slug to search for
  #
  # Returns the found Team or nil if none was found
  def self.find_by_combined_slug(combined_slug) # rubocop:disable GitHub/FindByDef
    if combined_slug.include? "/"
      with_org_name_and_slug *combined_slug.split("/")
    else
      raise ArgumentError, "expected `#{combined_slug}' to include `/'"
    end
  end

  # Workaround for "incompatible character encodings: UTF-8 and ASCII-8BIT"
  # exception. See https://chat.githubapp.com/messages/819330033/context
  def description
    return unless description = super
    if description.respond_to?(:force_encoding)
      description = description.dup.force_encoding("UTF-8")
      description = GitHub::Encoding.transcode(description, "UTF-8", "UTF-8") unless description.valid_encoding?
    end
    description
  end

  # Public: Returns a uniquely identifiable slug for an Org / Team combination
  #
  # Examples:
  #
  #  team.combined_slug
  #  # => 'github/enterprise'
  #  team.combined_slug
  #  # => 'github/core-backend-devs'
  #
  # Returns the slug
  def combined_slug
    "#{organization.name}/#{slug}"
  end
  alias_method :to_s, :combined_slug

  # Public: Use the teams's slug for URL generation
  def to_param
    slug
  end

  # Absolute permalink URL for this team.
  #
  # include_host - Turn off the `GitHub.url` host in the url. (default true)
  #                team.permalink(include_host: false) => `/orgs/github/teams/employees`
  #
  def permalink(include_host: true)
    endpoint = "/orgs/#{organization}/teams/#{to_param}"

    if include_host
      "#{GitHub.url}#{endpoint}"
    else
      endpoint
    end
  end

  # Internal: Set a slug based on the Team's name.
  # Will set a unique slug scoped with the Organization, to avoid clashes
  # between two teams with names like "ruby team" and "ruby-team".
  # This will set the slug if the name of the Team changes
  # or if the slug is nil (to handle legacy data).
  #
  # Returns the slug that was set, or nil if slug was not set
  def set_slug
    return unless name_changed? || slug.nil?
    self.slug = generate_unique_slug
  end

  # Internal: generate unique slug for Org / Team combination
  def generate_unique_slug
    candidate = base = self.name.parameterize.to_s

    if candidate.blank?
      candidate = base = "team"
    end

    index = 0
    while any_conflicting_slugs?(candidate)
      index += 1
      candidate = "#{base}-#{index}"
    end
    candidate
  end

  # Internal: Find conflicting slugs
  # Ignoring the current slug if the current Team exists
  #
  # Returns the count of conflicting slugs found
  def any_conflicting_slugs?(candidate)
    scope = self.class.where(organization_id: organization_id, slug: candidate)
    scope = scope.where("id <> ?", id) unless new_record?
    scope.any?
  end

  # Public: the repositories associated with this team that are visible to the
  # specified user.
  #
  # Note: it's possible that the specified user isn't able to see this team, but
  # is able to see some of the repos on this team (because they're public, or
  # because they're on another team). This method will still return those repos,
  # so make sure to check separately that the user is able to see this team.
  #
  # user - The user to check repository visibility for.
  #
  # Returns an ActiveRecord scope.
  def visible_repositories_for(user, affiliation: :immediate)
    repo_ids = direct_or_inherited_repo_ids(affiliation: affiliation)

    return repositories.none if repo_ids.empty?

    if GitHub.enterprise? && user&.site_admin?
      return organization.repositories.where(id: repo_ids)
    end

    associated_repository_ids = user&.associated_repository_ids(repository_ids: repo_ids)
    organization.visible_repositories_for(user, associated_repository_ids: associated_repository_ids).where(id: repo_ids)
  end

  # Public: like visible_repositories_for but results can be sorted by the fields
  # defined in Platform::Enums::TeamRepositoryOrderField
  #
  # user - The user to check repository visibility for.
  # sort - A hash representing the order field and direction.
  #
  # Returns an ActiveRecord scope.
  def sorted_visible_repositories_for(user, affiliation: :immediate, sort: {})
    order_field, order_direction = validate_repositories_sorting_arguments(sort: sort)

    # Since "action" is a column on Abilities, the order must be applied when
    # querying for the repo ids through Authorization::Service#subject_ids_for_multiple_actors
    # ex: { action: :desc }
    repo_ids_sort = order_field == "action" ? { action: order_direction.downcase.to_sym, subject_id: order_direction.downcase.to_sym } : {}
    repo_ids = direct_or_inherited_repo_ids(affiliation: affiliation, sort: repo_ids_sort)

    return repositories.none if repo_ids.empty?

    if GitHub.enterprise? && user&.site_admin?
      scope = organization.repositories.where(id: repo_ids)
    elsif user&.using_auth_via_integration?
      integration  = user.oauth_access.application
      installation = user.oauth_access.installation

      accessible_repository_ids = integration.accessible_repository_ids(
        current_integration_installation: installation,
        repository_ids: repo_ids,
      )

      scope = Repository.where(id: accessible_repository_ids).or(Repository.public_scope.where(owner_id: organization_id, active: true))
    else
      associated_repository_ids = user&.associated_repository_ids(repository_ids: repo_ids)

      scope = organization.visible_repositories_for(user, associated_repository_ids: associated_repository_ids).where(id: repo_ids)
    end

    if order_field.present?
      if order_field == "action"
        # preserves the order of the repositories when the field is "action"
        scope = scope.order(Arel.sql("FIELD(repositories.id, #{repo_ids.join(",")})")).
          order("repositories.id")
      else
        # orders the repositories for all other fields
        scope = scope.order("repositories.#{order_field} #{order_direction}")
      end
    end

    scope
  end

  # Validates the sorting arguments for repositories.
  # order direction can be one of ascending ("ASC") or descending ("DESC")
  # order field can be any field defined by Platform::Enums::TeamRepositoryOrderField
  #
  # sort -- A hash representing the order field and direction.
  #
  # Returns an Array with valid field and direction or nil.
  private def validate_repositories_sorting_arguments(sort: {})
    order_field, order_direction = sort.values_at(:field, :direction)

    valid_order_field = Platform::Enums::TeamRepositoryOrderField.values.any? { |_, v| v.value == order_field }
    valid_order_direction = Platform::Enums::OrderDirection.values.any? { |_, v| v.value == order_direction }

    return order_field, order_direction if valid_order_field && valid_order_direction
  end

  # Public: the most capable inherited ability a team has for a repository.
  # Inherited ability means an ability inherited from a parent team.
  #
  # repo - The repo to check abilities for
  #
  # Returns an Ability or nil.
  def most_capable_inherited_ability_for_repo(repo)
    return nil unless self.ancestor_ids.present?
    Ability.where(
      actor_id: self.ancestor_ids,
      actor_type: "Team",
      subject_id: [repo.id],
      subject_type: "Repository",
      priority: Ability.priorities[:direct],
    ).max
  end

  # Public: the most capable ability a team has for the given subjects.
  # abilities w/ team as the actor are always :direct
  #
  # subject_ids - The ids of the subjects to check abilities for
  # subject_type - The type of the subject
  #
  # Returns a Hash{subject Id => Ability}.
  def most_capable_abilities_on_subjects(subject_ids, subject_type:)
    abilities = Ability.where(
      actor_id: id_and_ancestor_ids,
      actor_type: "Team",
      subject_type: subject_type.to_s,
      subject_id: subject_ids,
      priority: Ability.priorities[:direct],
    )

    select_most_capable_ability_for_subject(abilities)
  end

  # Internal: Selects the most capable abilities for each subject ignoring
  # which team the permission came from. However, we prefer the team's
  # own ability record over an ancestor team's record if there is a tie
  # in permission level.
  #
  # abilities - [Ability]
  #
  # Returns a Hash{subject Id => Ability}.
  private def select_most_capable_ability_for_subject(abilities)
    abilities.each_with_object({}) do |ability, result|
      subject_id = ability.subject_id
      actor_id = ability.actor_id

      if most_capable = result[subject_id]
        if ability > most_capable
          result[subject_id] = ability
        elsif ability == most_capable && actor_id == id
          result[subject_id] = ability
        end
      else
        result[subject_id] = ability
      end
    end
  end

  # Public: checks if the team has a direct ability on a repository as opposed
  # to inherited from an ancestor team
  #
  # repo - The repo to check
  #
  # Returns boolean
  def has_repository?(repo)
    !!Authorization.service.direct_ability_between(actor: self, subject: repo)
  end

  # Public: the repositories associated with this team.
  #
  # Be careful: If you are presenting a list of repositories to someone, don't
  # use this method. Use #visible_repositories_for instead.
  #
  # Returns an ActiveRecord scope.
  def repositories
    Repository.with_ids(repository_ids)
  end

  # Public: the Repository ids associated with this team.
  def repository_ids
    GitHub.instrument "ability.team.repository-ids" do
      Ability.where(
        subject_type: "Repository",
        actor_id: id,
        actor_type: "Team",
        priority: Ability.priorities[:direct],
      ).pluck(:subject_id)
    end
  end

  # Public: the projects associated with this team that are visible to the
  # specified user.
  #
  # Note: it's possible that the specified user isn't able to see this team, but
  # is able to see some of the projects on this team (because they're public,
  # or because they're on another team). This method will still return those
  # projects, so make sure to check separately that the user is able to see
  # this team.
  #
  # user - The user to check project visibility for.
  # affiliation:
  #  immediate - Get projects that the team has a direct ability for
  #  inherited - Get projects that the team has inherited abilities for
  #  all - Get projects that the team or its ancestors have a direct ability
  #
  # Returns an ActiveRecord scope.
  def visible_projects_for(user, affiliation: :immediate)
    project_ids = direct_or_inherited_project_ids(affiliation: affiliation)

    return Project.none if project_ids.empty?

    organization.visible_projects_for(user).where(id: project_ids)
  end

  # Public: the projects associated with this team.
  #
  # Be careful: If you are presenting a list of projects to someone, don't use
  # this method. Use visible_projects_for instead.
  #
  # Returns an ActiveRecord scope.
  def projects
    subject_ids = Ability.where(
      subject_type: "Project",
      actor_id: id,
      actor_type: self.ability_type,
      priority: Ability.priorities[:direct],
    ).pluck(:subject_id)
    Project.where(id: subject_ids)
  end

  # Returns a list of project ids with the given affiliation
  # Be careful: like Team#projects, if you are presenting a list of
  # projects to someone, don't use this method.
  # Use the GraphQL team projects connection instead.
  #
  # affiliation:
  #  immediate - the project ids that the team has a direct ability for
  #  inherited - the project ids that the team has inherited abilities for
  #  all - the project ids that the team or its ancestors have a direct ability
  # for
  #
  # Returns a [project Id]
  def direct_or_inherited_project_ids(affiliation: :all)
    team_ids = case affiliation
    when :immediate then id
    when :inherited then ancestor_ids
    when :all then id_and_ancestor_ids
    end

    Ability.where(
      actor_id: team_ids,
      actor_type: "Team",
      subject_type: "Project",
      priority: Ability.priorities[:direct],
    ).pluck(:subject_id)
  end

  # Public: Whether this team is updatable by a given actor
  #
  # Returns a boolean
  def updatable_by?(actor)
    (actor.can_have_granular_permissions? && organization.resources.members.writable_by?(actor)) || adminable_by?(actor)
  end

  # Does this team grant pull access? Because pull is the lowest
  # access level, the answer is always yes.
  def pull?
    true
  end

  # Does this team exist specifically to grant pull access? Any other
  # access level returns false.
  def pull_only?
    permission.blank? || permission == "pull"
  end

  # Does this team grant push access? Pull access is implied.
  def push?
    admin? || push_only?
  end

  # Does this team exist specifically to grant push access? Any other
  # access level returns false.
  def push_only?
    permission == "push"
  end

  # Does this team grant admin access? Push and pull access is
  # implied.
  def admin?
    permission == "admin"
  end

  # Check if this was an admin team in the legacy permission model.
  # This permission still applies for admin teams that were created under
  # the old model and have not been converted to the new model by an org admin.
  # See https://github.com/github/github/pull/45134 for more info
  def legacy_admin?
    permission == "admin" && !legacy_owners?
  end

  # Convert a legacy admin team to a "modern" team.
  #
  # Returns nothing.
  def migrate_legacy_admin
    if legacy_admin?
      self.permission = "pull"
      save!
    end
  end

  # Is this the team of owners?
  def owners?
    name == "Owners" || name_was == "Owners"
  end

  # Check if this was once the owners team when legacy org membership was around.
  def legacy_owners?
    (name == "Owners" || name_was == "Owners") && permission == "admin"
  end

  # Check if the adder can add a user to the team without needing to first send
  # an invite.
  def can_add_member_without_invite?(user)
    GitHub.bypass_org_invites_enabled? ||
      organization.direct_or_team_member?(user) ||
      organization.business&.direct_or_team_member?(user)
  end

  # Public: Check if the adder can add a user to this team
  #
  # Returns a boolean
  def user_can_add_member?(member, actor:)
    unless organization.direct_member?(member)
      return actor.can_add_members_for?(organization, teams: [self])
    end

    if actor.can_have_granular_permissions?
      organization.resources.members.writable_by?(actor)
    else
      adminable_by?(actor)
    end
  end

  # Public: Add a user to this team.
  #
  # Returns a Team::AddMemberStatus.
  def add_member(user, options = {})
    options.assert_valid_keys(:adder, :skip_organization_seat_checks)
    adder = options[:adder]
    skip_organization_seat_checks = !!options[:skip_organization_seat_checks]

    # Trade Restricted orgs cannot manage their teams
    return AddMemberStatus::TRADE_CONTROLS_RESTRICTED if organization.has_full_trade_restrictions?

    # Only traditional users can belong to teams.
    return AddMemberStatus::NOT_USER unless user.user?

    # Don't add a user to an org they have blocked.
    return AddMemberStatus::BLOCKED if user.blocking?(organization)

    # Don't add a user to a team if the actor is blocked by the user
    return AddMemberStatus::BLOCKED if adder&.blocked_by?(user)

    # Don't add a user twice
    return AddMemberStatus::DUPE if member_ids.include?(user.id)

    unless skip_organization_seat_checks
      # Don't add if per seat organization has no seat for the user.
      return AddMemberStatus::NO_SEAT unless organization.has_seat_for?(user)

      # Don't add if per seat organization has no seat for the user on pending
      # cycle.
      return AddMemberStatus::PENDING_CYCLE_NO_SEAT unless organization.has_seat_for?(user, pending_cycle: true)
    end

    # Don't add if user doesn't satisfy the 2FA requirements of the Organization
    return AddMemberStatus::NO_2FA unless organization.two_factor_requirement_met_by?(user)

    # Don't add if user doesn't satisfy the SAML SSO requirement of the Organization
    saml_session_owner = organization.external_identity_session_owner
    return AddMemberStatus::NO_SAML_SSO unless saml_session_owner.saml_sso_requirement_met_by?(user)

    if GitHub.bypass_org_invites_enabled? && adder.present?
      return AddMemberStatus::NO_PERMISSION if !user_can_add_member?(user, actor: adder)
    end

    transaction do
      Ability.transaction do
        # Revoke access for new GitHub hires.
        if GitHub.new_staff_precautions? && github_employees_team?
          user.revoke_new_hire_accesses
        end

        grant user, :read, grantor: adder
        organization.add_member(user)

        if (request = pending_team_membership_requests.where(requester_id: user.id).first)
          request.cancel(actor: adder)
        end
      end
    end

    reload # to refresh the team member associations

    instrument_options = {user: user}

    if adder.present?
      instrument_options[:actor] = adder
    end

    instrument :add_member, instrument_options
    GlobalInstrumenter.instrument("team.add_member", instrument_options.merge(
      team: self,
      action: :add,
    ))

    user.activate_notice :org_newbie
    auto_subscribe_user user

    if send_user_added_notifications? && adder != user
      adder_id = adder&.id

      OrganizationMailer.team_added(user, self, adder).deliver_later(queue: "team_member_added_emails")
      if GitHub.flipper[:deliver_team_add_email].enabled?(organization)
        OrganizationMailer.team_member_added(user, self, adder).deliver_later(queue: "team_member_added_emails")
      end
    end

    Contribution.clear_caches_for_user(user)

    AddMemberStatus::SUCCESS

  rescue ActiveRecord::RecordNotUnique
    # This will only happen due to race conditions (e.g. two identical,
    # simultaneous requests), but handle it anyway
    AddMemberStatus::SUCCESS
  ensure
    user.clear_employee_memo if self.name == "Employees"
  end

  def add_or_invite_member(user:, inviter:, email: nil)
    if can_add_member_without_invite?(user)
      result = add_member(user, adder: inviter)
    else
      # The user is not a member of the org, therefore invite them to org.
      if organization.at_seat_limit?
        result = AddMemberStatus::NO_SEAT
      elsif organization.at_seat_limit?(pending_cycle: true)
        result = AddMemberStatus::PENDING_CYCLE_NO_SEAT
      else
        role = :direct_member

        begin
          result = organization.invite(user, email: email, inviter: inviter, role: role, teams: [self])
        rescue OrganizationInvitation::InvalidError
          # We don't have to be friendly here, since you need to hack around
          # the UI to try to send an invalid invitation.
          result = false
        rescue OrganizationInvitation::TradeControlsError
          result = AddMemberStatus::TRADE_CONTROLS_RESTRICTED
        end
      end
    end

    result
  end

  # Internal: Used by Abilities to ensure that only Users can be granted
  # abilities on a Team.
  def grant?(actor, action)
    actor.is_a?(User) && actor.user? && super
  end

  # Is this the @github/employees team?
  #
  # Returns boolean.
  def github_employees_team?
    self == GitHub::FeatureFlag.employees_team
  end

  # Public: Add a Repository to this team directly, with admin permissions, and
  # without subscriptions or logging. Used internally as part of the owners team
  # to direct org membership migration.
  #
  # repo - the Repository to add.
  #
  # Returns nothing.
  def add_repository_directly(repo)
    repo.add_team self, action: :admin
  rescue ActiveRecord::RecordNotUnique
    # it's fine.
  end

  # Public: Add a Repository to this team.
  #
  # repo                  - the Repository to add.
  # perm                  - the permission this repo should be added with. Option is
  #                         ignored if the direct org membership flag is not enabled.
  #                         Valid values are :pull, :push, or :admin
  # allow_different_owner - true if explicitly assigning to a team of a new org
  #
  # Returns an ModifyRepositoryStatus.
  def add_repository(repo, perm, allow_different_owner: false)
    # Only permit org-owned repos
    org_id = organization_id
    if !allow_different_owner && repo.organization_id != org_id
      return ModifyRepositoryStatus::NOT_OWNED
    end

    # Owners team owns every repo so just ignore it.
    return ModifyRepositoryStatus::OWNERS if legacy_owners?

    # Don't add a repo twice to the same team.
    return ModifyRepositoryStatus::DUPE if repositories.exists?(repo.id)

    # input action can be in permission or ability format
    repo_permission = permission_for_repo(perm, repo)

    # Complain about missing perm when direct org membership is enabled
    if repo_permission.nil?
      return ModifyRepositoryStatus::NO_PERMISSION
    end

    # Do it.
    GitHub::SchemaDomain.allowing_cross_domain_transactions do
      transaction do
        Ability.transaction do
          repo.add_team self, action: repo_permission
        end
      end
    end

    instrument :add_repository, \
      repo: repo,
      permission: repo_permission

    #simple check to reduce useless jobs. Real check is `fork_inherits_teams?`.
    if repo.private?
      add_forks_of(repo)
    end

    if repo.pushable_by?(self)
      GitHub.newsies.async_subscribe_users_to_repository(repo.id, members.map(&:id))
    end

    ModifyRepositoryStatus::SUCCESS

  rescue ActiveRecord::RecordNotUnique
    # Race conditions only, but handle it anyway
    ModifyRepositoryStatus::SUCCESS
  end

  # Public: remove a user from the team.
  #
  # user  - the User to be removed.
  # force - skip the expensive membership check and cache refresh, used in batch
  #         operations when removing many users at once.
  # send_notification - whether to notify the user they've been removed
  # team_destroyed - whether or not the user is being removed as part of the
  #                  team being destroyed.
  #
  # Returns nothing.
  def remove_member(user, force: false, send_notification: true, team_destroyed: false, queue_delete_jobs: true)
    return unless force || member?(user)

    repo_ids = direct_or_inherited_repo_ids(affiliation: :all)
    membership = Team::Membership.new(self, user.id, repo_ids, send_notification: send_notification, team_destroyed: team_destroyed)
    membership.remove(queue_delete_jobs: queue_delete_jobs) do
      reload
      user.reload

      # Unsubscribe user from this and all parent teams they are not a member of anymore
      team_ids = ancestor_ids + [self.id]
      Team::Destruction::DestroySubscriptionsOperation.new([user.id], team_ids).execute

      instrument :remove_member, user: user
      GlobalInstrumenter.instrument("team.remove_member", {
        user: user,
        team: self,
        action: :remove,
      })
    end

    Contribution.clear_caches_for_user(user)
    user
  end

  # Public: remove a repository from a team directly. This skips any
  # validations, instrumentation, or cleanup, and is used by the owners team
  # migration code.
  #
  # Returns nothing.
  def remove_repository_directly(repo)
    repo.remove_team self
    repo.reload # clear cached associations, e.g. list of teams
  end

  # Public: remove a repository from the team.
  #
  # repository - the Repository to be removed.
  #
  def remove_repository(repo, inline_fork_cleanup: false)
    return unless repositories.include?(repo)

    repo.remove_team self

    instrument :remove_repository, repo: repo

    descendant_member_ids = Team.members_of(self.id, immediate_only: false).pluck(:id)
    if inline_fork_cleanup
      repo.remove_inaccessible_forks_for(descendant_member_ids)
    else
      if repo.private?
        Team.queue_fork_cleanup repo.id, descendant_member_ids
      end
    end

    Team.queue_clear_team_memberships(member_ids, organization_id, { repo: repo.id })

    repo.reload # clear cached associations, e.g. list of teams
  end

  # Public: enqueues a job to remove a repository from the team.
  #
  # repository - the Repository to be removed.
  #
  def enqueue_remove_repository(repo)
    return unless repositories.include?(repo)
    RemoveTeamFromRepoJob.perform_later(team: self, repo: repo)
  end

  # Public: Add a Project to this team.
  #
  # repo - the Project to add.
  # action - the permission this project should be added with.
  #          Valid values are :read, :write, or :admin
  #
  # Returns true if successful, false if not.
  def add_project(project, action)
    # Require a valid action.
    if Ability.valid_actions.exclude?(action)
      return ModifyProjectStatus::NO_PERMISSION
    end

    # Only permit organization-owned projects.
    if project.owner_type != "Organization"
      return ModifyProjectStatus::NON_ORGANIZATION_PROJECT
    end

    # Only permit projects owned by this team's organization.
    if project.owner_id != organization_id
      return ModifyProjectStatus::NOT_OWNED
    end

    # Don't add a project twice to the same team.
    if projects.exists?(project.id)
      return ModifyProjectStatus::DUPE
    end

    project.update_team_permission(self, action)

    ModifyProjectStatus::SUCCESS
  end

  # Public: Update project permission for this team.
  #
  # project - the Project to update.
  # action - the permission this project should be changed to.
  #          Valid values are :read, :write, or :admin
  #
  # Returns true if successful, false if not.
  def update_project_permission(project, action)
    ability = Authorization.service.direct_ability_between(actor: self, subject: project)

    return false if ability.nil?

    project.update_team_permission(self, action)

    true
  end

  # Public: remove a project from the team.
  #
  # project - the Project to be removed.
  #
  # Returns true if successful, false if not.
  def remove_project(project)
    return true if projects.exclude?(project)

    project.update_team_permission(self, nil)
    instrument :remove_project, project: project

    true
  end

  # Public: Request membership for a user
  #
  # user - The user requesting membership
  def request_membership(user)
    team_membership_requests.create(requester: user)
  end

  # Internal: subscribe a user to the team and every repository this team has
  # at least push on. Called when a user is added.
  def auto_subscribe_user(user)
    settings_response = GitHub.newsies.settings(user)

    # Only auto-subscribe the user to the team and its ancestors if they are auto-watching teams.
    if settings_response.success? && settings_response.auto_subscribe_teams?
      GitHub.newsies.subscribe_to_list(user, self)

      ancestors.each do |ancestor|
        GitHub.newsies.subscribe_to_list(user, ancestor)
      end
    end

    # Only auto-subscribe the user to the team's repositories if they are auto-watching repos.
    return if settings_response.success? && !settings_response.auto_subscribe_repositories?

    # We do this in batches as large orgs may have many repositories
    # https://github.com/github/github/issues/92298
    owned_repos = organization.repositories.owned_by(organization).pluck(:id)
    owned_repos.each_slice(AUTO_SUBSCRIBE_BATCH_SIZE) do |slice|
      repo_ids = Ability.where(
        actor_id: id_and_ancestor_ids,
        actor_type: "Team",
        subject_type: "Repository",
        subject_id: slice,
        action: [Ability.actions[:write], Ability.actions[:admin]],
        priority: Ability.priorities[:direct],
      ).pluck(:subject_id)

      unless repo_ids.empty?
        GitHub.newsies.async_auto_subscribe(user, repo_ids)
      end
    end
  end

  # Internal: queue up a job to add forks to team
  def add_forks_of(repo)
    TeamAddForksJob.perform_later(self, repo)
  end

  # Internal: add forks to team
  def add_forks_of!(repo)
    perm = permission_for(repo)
    if perm
      repo.forks.each do |fork|
        # don't add repo to team twice
        if repo.fork_inherits_teams?(fork) && permission_for(fork).blank?
          throttle do
            add_repository(fork, perm)
          end
        end
      end
      update_repo_permissions_for_forks(repo) unless perm == permission_for(repo)
    end
  end

  # Public: queue a job to clear team membership info for an organization in
  # slices. Delegates to GitHub::Jobs::ClearTeamMembership.
  def self.queue_clear_team_memberships(access_list, org_id, options)
    return if access_list.blank?

    Array(access_list).each_slice(500) do |slice|
      ClearTeamMembershipsJob.perform_later(org_id, slice, options)
    end
  end

  # Public: queue a job to remove private forks of all repositories the
  # given user ids no longer have access to.
  def self.queue_fork_cleanup(repo_ids, user_ids)
    RemoveForksForInaccessibleRepositoriesJob.perform_later(Array(repo_ids), Array(user_ids))
  end

  # An "owners" team can never have its name changed.
  def restrict_changes_to_owners_team
  end

  # Internal: before_validation hook: set explicit permission if not set.
  def set_default_permission
    self.permission = DEFAULT_PERMISSION if self.permission.blank?
  end

  # Public: a list of members sorted by login
  def members_sorted_by_login
    members.sort_by { |u| u.login.downcase }
  end

  def async_ranked_members_for(user, scope:, direction: :desc)
    user.async_following.then do
      User.ranked_for(user, scope: scope, direction: direction)
    end
  end

  # Public: Get team members that are ordered such that the users most relevant to the given
  # user are first.
  #
  # user - a User
  # scope - a User ActiveRecord relation to filter the members further, should be joined to
  #         abilities table as 'ab'
  # direction - Symbol for ordering, :desc or :asc for descending or ascending order.
  #
  # Returns a User scope.
  def ranked_members_for(user, scope:, direction: :desc)
    async_ranked_members_for(user, scope: scope, direction: direction).sync
  end

  # The user who performed the action as set in the GitHub request context. If the context doesn't
  # contain an actor, fallback to the ghost user.
  def actor
    @actor ||= (User.find_by_id(GitHub.context[:actor_id]) || User.ghost)
  end

  # Internal: event prefix for team instrumentation
  def event_prefix
    :team
  end

  # Internal: payload for team instrumentation
  def event_payload
    {
      event_prefix => self,
      :org         => organization,
      :ldap_mapped => ldap_mapped?,
      :note        => "Team #{self}",
    }
  end

  def event_context(prefix: event_prefix)
    {
      prefix => to_s,
      "#{prefix}_id".to_sym => id,
    }
  end

  # Internal: generates additional data to send for webhook payloads
  def webhook_payload(action)
    event_actor = (action == :created ? creator : actor)

    {
      actor_id: event_actor.try(:id),
      spammy: event_actor.try(:spammy?),
      team_id: self.id,
    }
  end

  # Internal: send email notifications when user added?
  def send_user_added_notifications?
    # Team membership doesn't get you a notification unless there's repos too.
    repositories.present? && send_notifications?
  end

  # Internal: Can we send notifications?
  #
  # Returns a boolean.
  def send_notifications?
    GitHub.send_notifications?
  end

  # Internal: after_update callback. Updates this team's permissions on each of
  # its repositories to match its "permission" attribute.
  #
  # On orgs with direct org membership enabled, this should never run, since
  # teams are no longer supposed to have a team-wide permission level.
  #
  # Returns nothing.
  def update_repository_grants
    true
  end

  def instrument_creation
    instrument :create, webhook_payload(:created)
  end

  def instrument_update
    return if previous_changes.empty?

    changes_payload = {}.tap do |hash|
      hash[:old_description] = previous_changes["description"].first if previous_changes["description"].present?
      hash[:old_name] = previous_changes["name"].first if previous_changes["name"].present?
      old_privacy = previous_changes["privacy"]
      if old_privacy.present?
        hash[:old_privacy] = old_privacy.first
      end
    end

    return if changes_payload.empty?

    GitHub.instrument "team.update", webhook_payload(:edited).merge(changes: changes_payload)
  end

  def instrument_rename
    instrument :rename,
      name: name,
      name_was: name_before_last_save
  end

  def instrument_change_privacy
    instrument :change_privacy,
      privacy: privacy,
      privacy_was: privacy_before_last_save
  end

  # Internal: an after_create hook. FIX: This should be done in Org#create_team
  # instead of a callback.
  def add_team_as_org_dependent
    organization.dependent_added(self)
  end

  def owning_organization_id
    organization_id
  end

  def owning_organization_type
    "Organization"
  end

  # Public: Get a recent pending invitation for the specified user that includes
  # this team.
  #
  # user - User to get the invitation for.
  #
  # Returns an OrganizationInvitation, or nil if there are no pending
  # invitations for the specified user.
  def pending_invitation_for(user)
    GitHub.dogstats.time("organization.invitations.time.team_pending_invitation_for") do
      org_invitation = organization.pending_invitation_for(user)

      if org_invitation && org_invitation.includes_team?(self)
        org_invitation
      end
    end
  end

  # Public: Get the membership state of the specified user for this team. This
  # is intended to be used by the API only.
  #
  # user - User to check the membership state for.
  #
  # Returns :pending, :active, or :inactive.
  def membership_state_of(user, immediate_only: true)
    if self.class.member_of?(id, user&.id, immediate_only: immediate_only)
      :active
    elsif pending_invitation_for(user).present?
      :pending
    else
      :inactive
    end
  end

  # Public: Add or Update repository permission for this team.
  #
  # repo                  - the Repository
  # perm                  - the permission this team should have for this repo
  #
  # Returns an ModifyRepositoryStatus.
  def add_or_update_repository(repo, perm)
    if repositories.exists?(repo.id)
      update_repository_permission(repo, perm)
    else
      add_repository(repo, perm)
    end
  end

  # Public: Update repository permission for this team.
  #
  # repo      - the Repository.
  # perm      - the String or Symbol name of the permission this team should have for this repo.
  #             It can be pull/push, read/triage/write/maintain/admin or a custom role name.
  # context   - Hash of Strings with the actor's previous Role {:old_permission, :old_base_role}
  #             A team might be given the same custom role permission, with a different base role.
  #             In these scenarios, the name is the same, but the Ability must be updated.
  #             For auditing purposes, we need the old Role and it's base role.
  #             A custom role is a user created role which inherits from any Role::VALID_REPO_BASE_ROLES
  #             with an additional set of fine grained permissions.
  #
  # Returns an ModifyRepositoryStatus.
  def update_repository_permission(repo, perm, context: {})
    # Only permit org-owned repos
    org_id = organization_id
    if repo.organization_id != org_id && repo.plan_owner.id != org_id
      return ModifyRepositoryStatus::NOT_OWNED
    end

    # input action can be in permission or ability format
    repo_permission = permission_for_repo(perm, repo)

    if repo_permission.nil?
      return ModifyRepositoryStatus::NO_PERMISSION
    end

    # in the case of custom roles, the old permission can't be obtained directly from Ability records
    # so we leverage the context
    old_permission = context.dig(:old_permission) || permission_for_repo(repo.direct_role_for(self), repo)
    old_base_role = context.dig(:old_base_role) || old_permission

    if duplicate_permission?(new_permission: repo_permission, old_permission: old_permission, old_base_role: old_base_role, repo: repo)
      return ModifyRepositoryStatus::DUPE
    end

    old_permissions = {
      pull: repo.pullable_by?(self),
      push: repo.pushable_by?(self),
      admin: repo.adminable_by?(self),
      triage: permission_greater_than_target?(old_base_role, target: "triage"),
      maintain: permission_greater_than_target?(old_base_role, target: "maintain")
    }

    # Do it.
    repo.add_team self, action: repo_permission.to_sym

    instrument :update_repository_permission, \
      { repo: repo,
        id: "#{event_prefix}.update_repository_permission.#{repo.id}",
        permission: repo_permission,
        old_permission: old_permission,
        old_permissions: old_permissions,
      }.merge(webhook_payload(:edited))

    GitHub.dogstats.increment("org.team.update_repository_permission")

    # apply to all forks of parent repos
    unless repo.fork?
      update_repo_permissions_for_forks(repo)
    end

    if old_permission == "read" && repo.pushable_by?(self)
      GitHub.newsies.async_subscribe_users_to_repository(repo.id, members.map(&:id))
    end

    ModifyRepositoryStatus::SUCCESS
  end

  # Public: Enqueue a jobs to update `batch_size` teams at a time.
  #
  # teams_repos   - The team and associated repos to be updated.
  # action        - The permission or role invitation is being updated to.
  # role          - the custom Role (default: nil)
  # context       - Hash of Strings with the teams' previous Role {:old_permission, :old_base_role}
  # actor_id      - The actor updating the permissions
  # batch_size    - Batch size to be passed processed.
  #
  def self.batch_enqueue_update_repo_permissions(teams_repos:, action:, role: nil, context: {}, actor_id: organization.id, batch_size: TEAM_UPDATE_BATCH_SIZE)
    teams_repos.each_slice(batch_size) do |teams_repos_slice|
      BatchUpdateTeamRepoPermissionsJob.perform_later(teams_repos_slice, actor_id, action: action, role: role, context: context)
    end
  end

  def enqueue_update_repo_permissions(repo:, action:)
    check_valid_action(action, repo)
    UpdateTeamRepoPermissionsJob.perform_later(self, action: action, repo: repo)
  end

  def check_valid_action(action, repo)
    valid_action = PERMISSIONS.include?(action.to_s) || ABILITIES.include?(action.to_s) || Role.org_repo_custom_role?(name: action, repo: repo)
    raise ArgumentError, "Invalid permission #{action}" unless valid_action
  end

  # Internal: apply permission to all forks of a repo in a background job
  def update_repo_permissions_for_forks(repo)
    TeamUpdateForkedRepositoryPermissionsJob.perform_later(id, repo.id, actor.id)
  end

  # Internal: Validates that a team's name and slug are unique by the team's
  # organization. Why not just use `validates_uniqueness_of`? We have added a
  # boolean attribute to the teams table called `deleted`. It exists so that
  # users do not see teams they have just deleted but are still queued to be
  # deleted by us.
  #
  # So this check, essentially does what `validate_uniqueness_of` does but
  # ensures we are only checking against teams that have not been marked as
  # deleted.
  def name_and_slug_uniqueness_by_org
    Team
      .owned_by(organization)
      .where(deleted: false)
      .where.not(id: id)
      .pluck(:name, :slug).each do |(existing_name, existing_slug)|
      errors.add :name, "must be unique for this org" if existing_name.casecmp?(self.name) # case_insensitve
      errors.add :slug, "must be unique for this org" if existing_slug == self.slug # case_sensitive
    end
  end

  # Internal: Validates that a team's parent is a member of the same org
  def validate_parent_team_is_in_org
    if (team = parent_team) && team.organization_id != organization_id
      errors.add :parent_team_id, "must be a member of the same org"
    end
  end

  # Internal: Validates that a cycle is not created
  def validate_parent_team_does_not_cause_cycle
    return unless parent_team

    path = Arvore::PathString.new(parent_team.tree_path)
    if path.include?(id)
      errors.add :parent_team_id,  "can't be a child of the current team"
      return
    end
  end

  # Internal: Validate that a parent is not secret
  def validate_parent_not_secret
    if (team = parent_team) && team.secret?
      errors.add :parent_team_id, "can't be a secret team"
    end
  end

  def validate_parent_has_no_group_mappings
    if parent_team&.externally_managed?
      errors.add :parent_team_id, "can't have team sync enabled"
    end
  end

  # Internal: Validate that the team is not secret and a parent or child at the same time.
  def validate_secret_or_nested
    if secret?
      errors.add :visibility, "can't be secret for a child team" if parent_team
      errors.add :visibility, "can't be secret for a parent team" if tree_path && child_teams.any?
    end
  end

  # Internal: Validate that requesting team is allowed to create parent requests for this team
  def allows_change_parent_requests_from?(requesting_team)
    requesting_team.organization_id == self.organization_id
  end

  def reload(*)
    super.tap do
      reset_parent_team
    end
  end

  # Public: Do any members have higher access to the specified repo than this
  # team provides?
  #
  # repo - Repo to check access level of.
  #
  # Returns a boolean.
  def members_with_higher_access_to?(repo)
    case Authorization.service.most_capable_ability_between(actor: self, subject: repo).action
    when "read"
      members.any? { |member| repo.pushable_by?(member) }
    when "write"
      members.any? { |member| repo.adminable_by?(member) }
    else
      false
    end
  end

  def primary_avatar_path
    "/t/#{id}"
  end

  def avatar_editable_by?(actor)
    adminable_by?(actor)
  end

  def cant_change_visibility?
    parent_team.present? || has_child_teams?
  end

  def strip_name
    self.name = self.name&.strip
  end

  # Public: Is this team externally managed via Team Sync?
  #
  # Returns a boolean
  def externally_managed?
    organization.team_sync_enabled? && group_mappings.any?
  end

  # Public: Is this team externally managed via Team Sync?
  #
  # Returns a Promise that resolves to a Boolean.
  def async_externally_managed?
    async_organization.then do |org|
      org.async_team_sync_enabled?.then do |enabled|
        next false unless enabled
        async_group_mappings.then do |mappings|
          mappings&.any?
        end
      end
    end
  end

  # Public: Is this team managed locally and not via Team Sync?
  #
  # Returns a boolean
  def locally_managed?
    !externally_managed?
  end

  # Public: Is this team locally managed and not via Team Sync?
  #
  # Returns a Promise that resolves to a Boolean.
  def async_locally_managed?
    async_organization.then do |org|
      org.async_team_sync_enabled?.then do |enabled|
        async_group_mappings.then do |mappings|
          !mappings&.any?
        end
      end
    end
  end

  # Public: Can this team be externally managed via Team Sync?
  #
  # Returns a boolean
  def can_be_externally_managed?
    async_can_be_externally_managed?.sync
  end

  # Public: Can this team be externally managed via Team Sync?
  #
  # Returns a Promise that resolves to a boolean
  def async_can_be_externally_managed?
    organization.async_team_sync_enabled?.then do |enabled|
      enabled && !has_child_teams?
    end
  end

  # Internal: Does the team have review request notifications disabled? True
  # if the team has enabled review request notifications and disabled the
  # `review_request_delegation_notify_team` attribute.
  #
  # Returns a Boolean.
  def review_request_notifications_disabled?
    review_request_delegation_enabled? && !review_request_delegation_notify_team?
  end

  # Public: Given a user and a query, search for teams that could potentially be
  # child teams. Filter by authorization and possibly by the query. Then
  # decorate the resulting teams with eligibility_status values as a form of
  # validation.
  #
  # Returns: Promise of Array of Teams
  def async_potential_child_teams(viewer:, query:)
    async_organization.then do |org|
      org
        .visible_teams_for(viewer)
        .where.not(id: id)
        .then { |teams| Team.search_name_and_slug(query: query, scope: teams) }
        .map { |team| team.determine_child_team_eligibility(viewer, self) }
    end
  end

  # Internal: Determine if a team is eligibly to be a child of the current team.
  # We use various criteria to decorate the team object with information about
  # whether a team is eligible to be a child team and why it is not eligible if
  # it is not.
  #
  # Returns: a team.
  protected def determine_child_team_eligibility(viewer, parent_team)
    tap do |team|
      team.eligibility_status =
        case
        when secret? then ELIGIBLITY_STATUSES[:secret]
        when ancestor_of?(parent_team) then ELIGIBLITY_STATUSES[:ancestor]
        when parent_team_id == parent_team.id then ELIGIBLITY_STATUSES[:child]
        when adminable_by?(viewer) then ELIGIBLITY_STATUSES[:eligible]
        else ELIGIBLITY_STATUSES[:permission]
        end
    end
  end

  # Public: Does the given team's ancestors include the team in question?
  #
  # Returns: Boolean.
  def ancestor_of?(possible_child)
    possible_child.ancestor_ids.include?(id)
  end

  private

  # Internal: when trying to update the permission on a repository, we must account for
  #           custom roles, which can have the same name, but different base roles.
  #           See #update_repository_permission
  def duplicate_permission?(new_permission:, old_permission:, old_base_role:, repo:)
    custom_role = Role.for_repo_owner(name: new_permission, repo: repo)

    return new_permission == old_permission unless custom_role

    custom_role.name == old_permission && custom_role.base_role.name == old_base_role
  end

  # Internal: Is the permission greater or equal in rank than the target permission.
  # Both inputs can be an Ability (e.g. read), a Role (e.g. triage) or a custom role.
  #
  # - permission: a String or Symbol.
  # - target: a String or Symbol.
  #
  # Returns a Boolean.
  def permission_greater_than_target?(permission, target:)
    return false if permission.nil?

    perm = permission.to_sym
    target = target.to_sym
    return true if perm == target

    permission =
      if Role.valid_system_role?(perm)
        Role.find_by(name: perm)
      else
        Role.for_org(name: perm, org: organization)
      end

    permission.target_greater_than_other_role?(other_role: target)
  end

  # Internal: find the auditable name for the permission over a given repo.
  #
  # - perm: the String or Symbol Ability (e.g. read), Role (e.g. triage),
  #         Custom Role or human readable action (e.g. pull).
  # - repo: the Repository object.
  #
  # Returns a String.
  def permission_for_repo(perm, repo)
    action = perm.to_s

    if PERMISSIONS.include?(action)
      PERMISSIONS_TO_ABILITIES[action].to_s
    elsif ABILITIES.include?(action)
      action.to_s
    elsif role = Role.for_repo_owner(name: action, repo: repo)
      role.name
    else
      nil
    end
  end
end
