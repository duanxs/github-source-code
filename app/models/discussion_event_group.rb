# frozen_string_literal: true

class DiscussionEventGroup
  attr_reader :events

  def initialize(events)
    @events = events.sort_by(&:created_at)
  end

  def hash
    events.hash
  end

  def ==(other)
    other.is_a?(DiscussionEventGroup) && events == other.events
  end
  alias_method :eql?, :==

  def earliest_event
    events.first
  end

  def latest_event
    events.last
  end

  # Pull event type and comment from the last event since it's the event that
  # represents the ending state
  delegate :event_type, :comment_author, :comment_id, :locked?,
    :unlocked?, :marked_or_unmarked_answer?, :created_at,
    :id, :safe_actor, to: :latest_event

  delegate :size, to: :events

  def event_ids
    @event_ids ||= Set.new(events.map(&:id))
  end
end
