# frozen_string_literal: true

class TwoFactorRecoveryRequestReview

  include Instrumentation::Model

  attr_reader :message, :reason, :type, :audit_entry_time

  AUTOMATED_REVIEW_PREFIX = "two-factor-recovery-request-review"

  # user_email.unverify is only logged when bounces occur.
  # This means the user could not take this action on their own accord,
  # so we are not tracking it.
  EMAIL_ACTIONS = %w(user.add_email user.remove_email user_email.confirm_verification email_role.create).freeze

  POTENTIAL_TAKEOVER_INDICATORS = %w(public_key.create user.correct_password_from_unrecognized_device_and_location user.correct_password_from_unrecognized_location user.rename).freeze

  POST_REQUEST_FLAGGED_ACTIONS = %w(user.login two_factor_authentication.disabled two_factor_authentication.enabled).freeze

  ACTIONS_LIST = [].concat(EMAIL_ACTIONS, POTENTIAL_TAKEOVER_INDICATORS, POST_REQUEST_FLAGGED_ACTIONS)

  SPONSOR_COUNT_THRESHOLD = 100

  SPONSOR_LISTING_THRESHOLD_IN_CENTS = 5000_00

  SPONSORSHIP_TOTAL_THRESHOLD_IN_CENTS = 1000_00

  def initialize(request)
    @request = request
  end

  def event_payload
    if @message.present?
      {
        user: @request.user,
        review_required: true,
        message: @message,
        type: @type,
        reason: @reason,
      }
    else
      {
        user: @request.user,
        review_required: false
      }
    end
  end

  def event_prefix
    :two_factor_account_recovery_review
  end

  def instrument_risk_assesment
    instrument :risk_assessment
  end

  def instrument_hydro
    GlobalInstrumenter.instrument("automated_two_factor_account_recovery_request_review",
      user: @request.user,
      review_required: review_required?,
      reason_type: hydro_reason_type,
      audit_log_event_type: hydro_audit_log_event_type,
      email_status_type: hydro_email_status_reason_type,
      org_membership_type: hydro_org_membership_type,
    )
  end

  # TwoFactorRecoveryRequestReview#perform makes decisions about whether to automate a recovery request
  # or open a support ticket based upon these criteria in sequence

  # User type - if a User changes from a 'User' to an 'Organization' a manual review is required
  # Staff - any member of GitHub staff that requests two-factor recovery should be manually reviewed by Support.
  # Deceased - Deceased users will require manual review

  # Unverified primary emails - if the users primary email is bouncing it will require manual review
  # Unverified backup email - the same here, except with backup email
  # Disposable email addresses - the request is flagged for manual review if the account requesting
  # account recovery has any email address belonging to a domain with very low reputation (<0.1).

  # Audit log actions after completing request - If any of the following actions happen after
  # the recovery request is initiated a manual review is required
  # ** user.login
  # ** two_factor_authentication.disabled
  # ** two_factor_authentication.enabled

  # Changes to email settings - Any recent email changes before (past 30 days) or during
  # the two factor recovery flow are considered suspicious these are found in the audit log as
  # ** user.add_email
  # ** user.remove_email
  # ** user_email.confirm_verification
  # ** email_role.create

  # Audit log actions before completing request - If any of the following actions happen after
  # the recovery request is initiated a manual review is required
  # ** user.correct_password_from_unrecognized_device_and_location
  # ** user.correct_password_from_unrecognized_location
  # ** public_key.create

  # GitHub Sponsors: A manual review is required for any user that is a part of GitHub Sponsors and satisfies any of the following criteria:
  # ** $5000+ tier
  # ** > $1000 monthly sponsorship
  # ** > 100 sponsors

  # Organization memberships -  a manual review is required for any user who is either the:
  # ** Last admin of organization requiring 2FA
  # ** Owner or billing manager of a business requiring 2FA

  def perform
    assess_risk
    review = event_payload.except(:user).merge({performed_at: Time.now.to_s})
    review[:audit_entry_time] = @audit_entry_time if @audit_entry_time.present?
    TwoFactorRecoveryRequestReview.store(@request.id, review)

    instrument_risk_assesment
    instrument_hydro
  end

  def review_required?
    @message.present?
  end

  def hydro_reason_type
    return :AUDIT_LOG if @type == "audit_log"
    return :EMAIL_STATUS if @type == "email_status"
    return :STAFF if @type == "staff"
    return :ORGANIZATION_MEMBERSHIP if @type == "org_membership"
    return :DECEASED if @type == "deceased"
    return :SPONSORS if @type == "sponsors"
    return :DEVICE if @type == "device"

    :REASON_TYPE_UNKNOWN
  end

  def hydro_audit_log_event_type
    return "" unless @type == "audit_log"

    @reason || ""
  end

  def hydro_email_status_reason_type
    return :EMAIL_STATUS_TYPE_UNKNOWN unless @type == "email_status"

    return :PRIMARY_EMAIL_BOUNCING if @reason == "primary_email_bouncing"
    return :BACKUP_EMAIL_UNVERIFIED if @reason == "backup_email_unverified"
    return :DISPOSABLE_EMAIL if @reason == "disposable_email_found"
    return :LOW_REPUTATION_DOMAIN if @reason == "disreputable_email"

    :EMAIL_STATUS_TYPE_UNKNOWN
  end

  def hydro_org_membership_type
    return :ORGANIZATION_MEMBERSHIP_TYPE_UNKNOWN unless @type == "org_membership"

    return :LAST_ADMIN if @reason == "last_admin"
    return :BUSINESS_AFFILIATION if @reason == "business_affiliation"

    :ORGANIZATION_MEMBERSHIP_TYPE_UNKNOWN
  end

  def self.mget(ids)
    keys = ids.map { |id| get_cache_key(id) }

    # using an empty array here as the fallback block as it'll be treated like
    # an array of `nil` values in the zip below
    cached_values = GitHub.kv.mget(keys).value { [] }

    result = {}

    # build up a hash of all non-null results found in cache
    ids.zip(cached_values) { |id, value| result[id] = JSON.parse(value) unless value.nil? }

    result
  end

  def self.store(id, review)
    key = get_cache_key(id)

    GitHub.kv.set(key, review.to_json, expires: 2.weeks.from_now)
  end

  def self.clear(id)
    key = get_cache_key(id)

    GitHub.kv.del(key)
  end

  def self.get_cache_key(id)
    "#{AUTOMATED_REVIEW_PREFIX}-#{id}"
  end

  private

  def assess_risk
    if @request.review_state == :evidence_missing
      @message = "Evidence used to complete the request is no longer present"
      @type = "evidence_missing"
      return
    end

    #  [ **** IMPORTANT **** ]

    # The ordering of our conditional checks is extremely important, as the review
    # process does not need to exhaust all checks. As soon as a problem is found,
    # the request is considered marked for manual review and no automation will
    # be performed.
    #
    # If a user is staff or deceased they need manual intervention - no exceptions.
    #
    # In any other case we render a staff reviewable list of emails in the approval
    # modal **ONLY** when their email settings are identified as a problem. We
    # need to ensure their email settings are checked first, as this method exits
    # as soon as a problem is identified.
    #
    # Since Organization memberships are the most resource intensive check
    # we have placed them last to lessen the burden on our MySQL clusters.
    #
    # Keep this in mind if adding any new checks to this flow
    user = @request.user
    begin
      return if check_user_state(user)
      return @type = "email_status" if check_emails(user)
      return @type = "audit_log" if check_audit_logs
      return @type = "sponsors" if check_sponsorships(user)
      return @type = "org_membership" if check_org_membership(user)
    rescue StandardError => e
      Failbot.report(e)
      @message = e.message
      return @type = "automated_review_process_failed"
    end
  end

  def timestamp_conversion(event)
    Time.at(event[:@timestamp] / 1000)
  end

  # Everything below this is mutable based on decisions we make
  # regarding information that can be used to automate decisions to approve /deny
  def recent_user_actions
    @recent_user_actions ||= Audit::Driftwood::Query.new_user_query({actor_id: @request.user.id, allowlist: ACTIONS_LIST, query_indexes: 1}).execute.results
  end

  # compare all actions and if the user has taken any actions since the request was initiated
  # support should review
  def flagged_actions?(actions)
    discover_flagged_action(actions, POST_REQUEST_FLAGGED_ACTIONS)
  end

  # Are there any indications that someone may be attempting to take over this account?
  def potential_takeover?(actions)
    discover_flagged_action(actions, POTENTIAL_TAKEOVER_INDICATORS)
  end

  # compare all actions and if an email was added, removed, upgraded or downgraded in the past 30 days
  # support should review
  def email_changes?(actions)
    discover_flagged_action(actions, EMAIL_ACTIONS)
  end

  # return an audit log entry result, or nil
  def discover_flagged_action(actions, set)
    actions.detect { |r| set.include?(r[:action]) }.presence
  end

  def check_user_state(user)
    @message, @type = if user.type != "User"
      ["User type has been changed", "wrong_user_type"]
    elsif user.staff?
      ["User is staff member", "staff"]
    elsif user.deceased?
      ["User has been marked as deceased by staff", "deceased"]
    end

    review_required?
  end

  def check_emails(user)
    user_emails = user.emails
    verified_emails = user.emails.verified
    primary_email = user.primary_user_email

    @message, @reason = if !primary_email.verified? && primary_email.bouncing?
      ["Primary email #{primary_email} is marked as bouncing. This email address should be verified again, and this account should be reviewed manually.", "primary_email_bouncing"]
    elsif user.has_backup_email? && !user.backup_user_email.verified?
      ["Backup email #{user.backup_user_email} is not verified. This request should be reviewed manually to ensure we do not send an email to an unverified address.", "backup_email_unverified"]
    elsif verified_emails.empty?
      disposable_email = user_emails.detect { |email| email.disposable? }
      ["Disposable email #{disposable_email} found on account without any verified email addresses", "disposable_email_found"] if disposable_email.present?
    else
      reputation = nil
      disreputable_email = user.account_related_emails.detect do |user_email|
        email = user_email.to_s
        reputation = EmailDomainReputationRecord.reputation(email).reputation
        reputation < 0.1
      end
      ["Email #{disreputable_email} comes from a domain with low reputation (#{reputation})", "disreputable_email"] if disreputable_email.present?
    end

    review_required?
  end

  def check_audit_logs
    # fetch audit logs for the past month, then split it into two sections
    # [[actions_after_request_initiated], [actions_before_request_initiated]]
    actions_after_request_initiated, actions_before_request_initiated = recent_user_actions.partition { |r| timestamp_conversion(r) > @request.created_at }

    # Rails lets you validate that a value is set when you set it
    # Following this flow allows us to check `action` to see if it is set, and then immediately use it
    # if it is set.
    @message = if action = flagged_actions?(actions_after_request_initiated)
      "A #{action[:action]} event was found for this user in the audit log after this recovery request was initiated."
    elsif action = email_changes?(recent_user_actions)
      "A #{action[:action]} event was found for this user in the audit log for the email address: #{action[:email]}."
    elsif action = potential_takeover?(actions_before_request_initiated)
      "A #{action[:action]} event was found for this user in the audit log before starting this recovery request."
    end

    if action.present?
      @reason = action[:action]
      @audit_entry_time = timestamp_conversion(action)
    end

    review_required?
  end

  def check_org_membership(user)
    last_org_admin = user.affiliated_organizations_with_two_factor_requirement.select do |org|
      org.last_admin?(user)
    end

    @message, @reason = if last_org_admin.present?
      ["User is last admin of at least one organization", "last_admin"]
    elsif user.affiliated_businesses_with_two_factor_requirement.present?
      ["User is affiliated with business that requires 2FA", "business_affiliation"]
    end

    review_required?
  end

  def check_sponsorships(user)

    listing_ids = SponsorsListing.where(sponsorable_id: user.id).pluck(:id)

    published_tiers_above_threshold = SponsorsTier
      .where(sponsors_listing_id: listing_ids)
      .where(state: 1) # published
      .where("monthly_price_in_cents > ?", SPONSOR_LISTING_THRESHOLD_IN_CENTS)
      .count > 0

    if published_tiers_above_threshold
      @message = "User has published sponsorship tier over $#{SPONSOR_LISTING_THRESHOLD_IN_CENTS / 100} per month"
      @reason = "high_sponsor_tier"
      return true
    end

    sponsorships = Sponsorship.all_active_as_sponsorable(sponsorable: user)

    if sponsorships.count > SPONSOR_COUNT_THRESHOLD
      @message = "User has #{sponsorships.count} active sponsors"
      @reason = "high_sponsor_count"
      return true
    end

    total_sponsorships_per_month_in_cents = sponsorships.map { |s| s.subscription_item.monthly_price_in_cents }.sum

    if total_sponsorships_per_month_in_cents >= SPONSORSHIP_TOTAL_THRESHOLD_IN_CENTS
      @message = "User requesting account recovery receives significant amount from sponsors: $#{total_sponsorships_per_month_in_cents / 100} per month"
      @reason = "high_monthly_sponsorship"
    end

    review_required?
  end
end
