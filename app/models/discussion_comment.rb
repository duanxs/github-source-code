# frozen_string_literal: true

class DiscussionComment < ApplicationRecord::Domain::Discussions
  extend GitHub::BackgroundDependentDeletes
  include GitHub::UTF8
  include GitHub::UserContent
  include GitHub::Validations
  include GitHub::RateLimitedCreation
  include UserContentEditable
  include Reaction::Subject::RepositoryContext
  include Spam::Spammable
  include InteractionBanValidation
  include AbuseReportable
  include Blockable
  include NotificationsContent::WithCallbacks
  include DiscussionComment::NewsiesAdapter
  include DiscussionComment::SearchAdapter
  include GitHub::MinimizeComment
  include Instrumentation::Model
  include EmailReceivable

  include Permissions::Attributes::Wrapper
  self.permissions_wrapper_class = Permissions::Attributes::DiscussionComment

  # Public: How many unique discussions must a user comment in within a repository
  # to be labelled a "Frequent Helper"?
  FREQUENT_HELPER_COUNT = 5

  URI_TEMPLATE = Addressable::Template.new("/{owner}/{name}/discussions/{number}#discussioncomment-{comment_id}").freeze

  extend GitHub::Encoding
  force_utf8_encoding :body

  alias_method :created_via_email?, :created_via_email

  belongs_to :discussion, required: true
  belongs_to :user
  belongs_to :repository, required: true
  belongs_to :parent_comment, class_name: "DiscussionComment"

  setup_spammable(:user)
  setup_attachments

  has_many :reactions, class_name: "DiscussionCommentReaction"
  destroy_dependents_in_background :reactions

  has_many :comments, class_name: "DiscussionComment", foreign_key: :parent_comment_id

  before_validation :set_repository, on: :create

  # Used to temporarily hold the current user, for use in creating events
  attr_accessor :actor

  validates :body, bytesize: { maximum: MYSQL_UNICODE_BLOB_LIMIT }, unicode: true,
    allow_nil: false, allow_blank: true
  validates :body, presence: true, on: :create
  validates :body, presence: true, on: :update, unless: :wiped?
  validate :user_exists_for_open_discussion, on: :create
  validate :user_can_interact?, on: :create
  validate :user_has_verified_email, on: :create
  validate :ensure_author_is_not_blocked
  validate :ensure_parent_comment_is_for_discussion
  validate :ensure_at_most_one_level_deep
  validate :parent_comment_exists
  validate :parent_comment_is_not_hidden, on: :create
  validate :parent_comment_is_not_self, on: :update

  before_destroy :ensure_not_a_parent
  before_destroy :unmark_as_answer
  after_destroy_commit :delete_wiped_parent_if_no_children

  # Hydro telemetry and audit logs
  after_commit :instrument_creation_event, on: :create
  after_commit :instrument_update_event, on: :update
  after_destroy_commit :instrument_deletion_event

  # Update discussion
  after_commit :update_discussion_comment_count, on: :create
  after_destroy :update_discussion_comment_count
  after_destroy :update_parent_comment_websocket

  # Live updates
  after_commit :notify_socket_subscribers, on: :update, if: :body_changed_after_commit?
  after_commit :notify_socket_subscribers_on_create, on: :create

  # Notifications
  after_commit :subscribe_and_notify, on: :create
  after_commit :update_subscriptions_and_notify, on: :update

  # Search
  after_commit :synchronize_search_index, on: :update, if: :body_changed_after_commit?

  # audit log for updates
  after_commit :audit_log_update_event, on: :update, if: :body_changed_after_commit?

  delegate :number, to: :discussion

  scope :for_user, ->(user) { where(user_id: user) }
  scope :for_discussion, ->(discussion) { where(discussion_id: discussion) }
  scope :after_comment, ->(comment) { where("discussion_comments.id > ?", comment) }
  scope :for_repository, ->(repo) { where(repository_id: repo) }
  scope :child_of, ->(comment) { where(parent_comment_id: comment) }
  scope :top_level, -> { where(parent_comment_id: nil) }
  scope :not_wiped, -> { where(deleted_at: nil) }

  scope :chosen_answers, -> do
    joins("INNER JOIN discussions ON " \
          "discussion_comments.discussion_id = discussions.id AND " \
          "discussion_comments.id = discussions.chosen_comment_id")
  end

  enum comment_hidden_by: GitHub::MinimizeComment::ROLES

  # Public: Is this comment in a public repository?
  def public?
    discussion&.public?
  end

  # Public: Deletes this comment if possible or wipes its contents and author if it cannot be deleted.
  #
  # actor - the currently authenticated User
  #
  # Returns a Boolean indicating success.
  def wipe_or_destroy(actor)
    self.actor = actor

    if comments.exists?
      wipe
    else
      destroy
    end
  end

  # Public: Has this comment been wiped? Wiping a comment is what we do when it's a parent to
  # other comments and the user wants to delete it, so we can avoid deleting the record and
  # leaving the child comments orphaned.
  #
  # Returns a Boolean.
  def wiped?
    deleted_at.present?
  end

  # Public: Get the HTML body for each comment in the specified discussion.
  #
  # discussion - a Discussion
  # actor - the current User
  #
  # Returns a Hash of DiscussionComment ID => String.
  def self.body_html_by_comment_id(discussion, actor:, comments: nil)
    result = {}
    comments ||= discussion.filter_spam_comments_for(actor)
    promises = comments.map(&:async_body_html)
    body_htmls = Promise.all(promises).sync

    comments.each_with_index do |comment, i|
      result[comment.id] = body_htmls[i]
    end

    result
  end

  def self.last_reported_at_by_comment_id(discussion, actor:, comments: nil)
    result = {}

    # Only staff can see abuse reports on discussion comments
    return result unless actor.site_admin?

    comments ||= discussion.filter_spam_comments_for(actor)
    promises = comments.map(&:async_last_reported_at)
    last_report_times = Promise.all(promises).sync

    comments.each_with_index do |comment, i|
      result[comment.id] = last_report_times[i]
    end

    result
  end

  def self.top_report_reason_by_comment_id(discussion, actor:, comments: nil)
    result = {}

    # Only staff can see abuse reports on discussion comments
    return result unless actor.site_admin?

    comments ||= discussion.filter_spam_comments_for(actor)
    promises = comments.map(&:async_top_report_reason)
    top_reasons = Promise.all(promises).sync

    comments.each_with_index do |comment, i|
      result[comment.id] = top_reasons[i]
    end

    result
  end

  def self.report_count_by_comment_id(discussion, actor:, comments: nil)
    result = Hash.new(0)

    # Only staff can see abuse reports on discussion comments
    return result unless actor.site_admin?

    comments ||= discussion.filter_spam_comments_for(actor)
    promises = comments.map(&:async_report_count)
    report_counts = Promise.all(promises).sync

    comments.each_with_index do |comment, i|
      result[comment.id] = report_counts[i]
    end

    result
  end

  # Public: Get the latest user content edit for each comment in a discussion.
  #
  # discussion - a Discussion
  # actor - the current User
  #
  # Returns a Hash of DiscussionComment ID => UserContentEdit.
  def self.latest_edit_by_comment_id(discussion, actor:, comments: nil)
    result = {}
    comments ||= discussion.filter_spam_comments_for(actor)
    promises = comments.map(&:async_latest_user_content_edit)
    latest_edits = Promise.all(promises).sync

    comments.each_with_index do |comment, i|
      result[comment.id] = latest_edits[i]
    end

    result
  end

  # Public: Get the most recent comment for the given discussions.
  #
  # discussion_ids - a list of Discussion IDs
  # repository - the Repository these discussions are in, or its ID
  # viewer - the current User or nil
  #
  # Returns a Hash of Discussion ID => DiscussionComment.
  def self.latest_by_discussion_id(discussion_ids, viewer:, repository: nil)
    inner_query = group(:discussion_id).
      select("discussion_id, MAX(created_at) AS created_at").to_sql
    comments = joins(
      "JOIN (#{inner_query}) latest_by_discussion_id " \
      "ON discussion_comments.created_at = latest_by_discussion_id.created_at " \
      "AND discussion_comments.discussion_id = latest_by_discussion_id.discussion_id",
    ).includes(:user).for_discussion(discussion_ids).filter_spam_for(viewer)
    comments = comments.for_repository(repository) if repository
    comments.index_by(&:discussion_id)
  end

  def author
    user || User.ghost
  end

  def authored_by_ghost?
    author.ghost?
  end

  # Public: Returns the ID of this comment.
  def discussion_comment_id
    id
  end

  def async_readable_by?(actor)
    async_discussion.then do |discussion|
      discussion.async_readable_by?(actor)
    end
  end

  def readable_by?(actor)
    async_readable_by?(actor).sync
  end

  # Public: Determine which comments for the specified discussion have authors who can be
  # blocked by the given user. Should be kept in sync with Blockable#async_viewer_can_block?.
  #
  # discussion - Discussion
  # actor - the current User
  # comments - optional list of DiscussionComments to use
  #
  # Returns a Hash of DiscussionComment ID => Boolean.
  def self.blockable_by_comment_id(discussion, actor:, comments: nil)
    result = Hash.new(false)

    return result unless actor
    return result unless discussion.in_organization?

    comments ||= discussion.filter_spam_comments_for(actor)

    promises = comments.map do |comment|
      comment.async_viewer_can_block?(actor)
    end

    blockable_statuses = Promise.all(promises).sync

    comments.each_with_index do |comment, i|
      result[comment.id] = blockable_statuses[i]
    end

    result
  end

  # Public: Determine which comments for the specified discussion have authors who can be
  # unblocked by the given user. Should be kept in sync with Blockable#async_viewer_can_unblock?.
  #
  # discussion - Discussion
  # actor - the current User
  # comments - optional list of DiscussionComments to use
  #
  # Returns a Hash of DiscussionComment ID => Boolean.
  def self.unblockable_by_comment_id(discussion, actor:, comments: nil)
    result = Hash.new(false)

    return result unless actor
    return result unless discussion.in_organization?

    comments ||= discussion.filter_spam_comments_for(actor)

    promises = comments.map do |comment|
      comment.async_viewer_can_unblock?(actor)
    end

    unblockable_statuses = Promise.all(promises).sync

    comments.each_with_index do |comment, i|
      result[comment.id] = unblockable_statuses[i]
    end

    result
  end

  # Public: Determine which comments for the specified discussion can be reported by the
  # given user to a repository maintainer. Should be kept in sync with
  # AbuseReportable#async_viewer_can_report_to_maintainer.
  #
  # discussion - Discussion
  # actor - the current User
  # comments - optional list of DiscussionComments to use
  #
  # Returns a Hash of DiscussionComment ID => Boolean.
  def self.reportable_to_maintainer_by_comment_id(discussion, actor:, comments: nil)
    result = Hash.new(false)

    return result unless GitHub.can_report?
    return result unless actor
    return result unless discussion.in_organization?
    return result if discussion.viewer_can_moderate_content?(actor)

    repo = discussion.repository
    return result if repo.private?

    comments ||= discussion.filter_spam_comments_for(actor)

    promises = comments.map do |comment|
      comment.async_viewer_can_report_to_maintainer(actor)
    end

    reportable_statuses = Promise.all(promises).sync

    comments.each_with_index do |comment, i|
      result[comment.id] = reportable_statuses[i]
    end

    result
  end

  # Public: Determine which comments for the specified discussion can be reported by the
  # given user. Should be kept in sync with AbuseReportable#async_viewer_can_report.
  #
  # discussion - Discussion
  # actor - the current User
  # comments - optional list of DiscussionComments to use
  #
  # Returns a Hash of DiscussionComment ID => Boolean.
  def self.reportable_by_comment_id(discussion, actor:, comments: nil)
    result = Hash.new(false)

    return result unless GitHub.can_report?
    return result unless actor

    repo = discussion.repository
    return result if repo.private?

    comments ||= discussion.filter_spam_comments_for(actor)

    promises = comments.map do |comment|
      comment.async_viewer_can_report(actor)
    end

    reportable_statuses = Promise.all(promises).sync

    comments.each_with_index do |comment, i|
      result[comment.id] = reportable_statuses[i]
    end

    result
  end

  def async_reactable_by?(actor)
    DiscussionCommentReaction.async_viewer_can_react?(actor, self)
  end

  def reactable_by?(actor)
    async_reactable_by?(actor).sync
  end

  # Public: Determine which comments can be reported by the given user.
  #
  # discussion - Discussion whose comments are being checked
  # actor - the current User
  # comments - optional list of DiscussionComments to use
  #
  # Returns a Hash of DiscussionComment ID => Boolean.
  def self.can_react_by_comment_id(discussion, actor:, comments: nil)
    result = Hash.new(false)
    return result unless actor
    return result if discussion.locked?
    return result if actor.should_verify_email?
    return result if actor.blocked_by?(discussion.user, discussion.repository_owner)

    comments ||= discussion.filter_spam_comments_for(actor)

    promises = comments.map do |comment|
      comment.async_reactable_by?(actor)
    end

    can_react_results = Promise.all(promises).sync

    comments.each_with_index do |comment, i|
      result[comment.id] = can_react_results[i]
    end

    result
  end

  # Public: Get the repository action/role level for users who have commented
  # in the given discussion.
  #
  # discussion - a Discussion
  # actor - the current User
  #
  # Returns a Hash of DiscussionComment ID => Symbol/nil.
  def self.action_or_role_level_by_comment_id(discussion, actor:, comments: nil)
    result = {}

    repo = discussion.repository
    comments ||= discussion.filter_spam_comments_for(actor).includes(:user)

    comments.map(&:user).compact.each_slice(1000) do |users|
      Repository.preload_repository_permissions(repositories: [repo], users: users)
    end

    promises = comments.map do |comment|
      if comment.user
        # We don't want to include employee granted permissions here because
        # this is specifically being used to show/hide badges per-user
        repo.async_action_or_role_level_for(comment.user, include_employee_granted_permissions: false)
      else
        Promise.resolve(nil)
      end
    end

    levels = Promise.all(promises).sync

    comments.each_with_index do |comment, i|
      result[comment.id] = levels[i]
    end

    result
  end

  # Public: Determine which authors in the given discussion thread should be
  # labelled 'Frequent Helpers' in a repository, based on how many unique
  # discussions they've commented on.
  #
  # discussion - a Discussion; only users who have commented in this discussion
  #              will be included in the returned hash
  # actor - the current User
  # comments - optional list of DiscussionComments
  #
  # Returns a Hash of User ID => Boolean.
  def self.frequent_helper_by_user_id(discussion, actor:, comments: nil)
    result = Hash.new(false)
    comments ||= discussion.filter_spam_comments_for(actor)
    user_ids = ([discussion.user_id] + comments.map(&:user_id)).uniq

    comment_counts_by_user_id = for_repository(discussion.repository).
      filter_spam_for(actor).for_user(user_ids).
      select(:discussion_id, :user_id).distinct.
      group(:user_id).count

    comment_counts_by_user_id.each do |user_id, comment_count|
      result[user_id] = comment_count >= FREQUENT_HELPER_COUNT
    end

    result
  end

  # Public: Determine which comments for the specified discussion can be deleted by the
  # given user. Should be kept in sync with #deletable_by?.
  #
  # discussion - Discussion
  # actor - the current User
  #
  # Returns a Hash of DiscussionComment ID => Boolean.
  def self.deletable_by_comment_id(discussion, actor:)
    result = Hash.new(false)

    # Anonymous users can't delete comments
    return result unless actor

    # Need a verified email address to delete discussions
    return result if actor.should_verify_email?

    repo = discussion.repository
    return result unless repo && !repo.archived?

    deletable_comments = discussion.filter_spam_comments_for(actor).not_wiped.
      select(:id)

    # If the discussion is locked and you don't have repo write access,
    # you cannot remove comments
    is_writable = repo.writable_by?(actor)
    return result if !is_writable && discussion.locked?

    if !is_writable && !repo.adminable_by?(actor)
      # Author of the comment can delete it
      deletable_comments = deletable_comments.for_user(actor)
    end

    deletable_comments.pluck(:id).each do |comment_id|
      result[comment_id] = true
    end

    result
  end

  # Public: Can the given actor delete the comment? Should be kept in sync with
  # #deletable_by_comment_id.
  def deletable_by?(actor)
    return false unless repository&.discussions_enabled?

    response = ::Permissions::Enforcer.authorize(
      action: :delete_discussion_comment,
      actor: actor,
      subject: self
    )
    response.allow?
  end

  def self.async_can_toggle_minimized_discussion_comment?(discussion, actor:)
    discussion.async_repository.then do |repo|
      repo.async_owner.then do |owner|
        response = ::Permissions::Enforcer.authorize(
          action: :toggle_discussion_comment_minimize,
          actor: actor,
          subject: discussion,
        )

        response.allow?
      end
    end
  end

  # Public: Can the given actor see the hide/unhide buttons on a discussion comment in the given discussion?
  def self.can_toggle_minimized_discussion_comment?(discussion, actor:)
    async_can_toggle_minimized_discussion_comment?(discussion, actor: actor).sync
  end

  # Public: Can the given actor hide the comment?
  def async_minimizable_by?(actor)
    # Site admins can always minimize comments via stafftools.
    return Promise.resolve(true) if actor.site_admin?

    async_discussion.then do |disc|
      DiscussionComment.can_toggle_minimized_discussion_comment?(disc, actor: actor)
    end
  end

  def async_unminimizable_by?(actor)
    return Promise.resolve(false) unless actor.present?
    return Promise.resolve(true) if actor.site_admin?

    async_repository.then do |repo|
      next false unless repo

      repo.async_pushable_by?(actor).then do |is_pushable|
        next is_pushable if minimized_by_maintainer?
        next false unless minimized_by_author?

        is_pushable || actor.id == user_id
      end
    end
  end

  # Public: Was the comment body changed in previous_changes? We use this to determine
  # if body changes _only_ after_commit.
  def body_changed_after_commit?
    previous_changes.key?(:body)
  end

  def notify_socket_subscribers
    channel = GitHub::WebSocket::Channels.discussion(discussion)
    return unless channel

    GitHub::WebSocket.notify_discussion_channel(discussion, channel,
      timestamp: Time.now.to_i,
      wait: default_live_updates_wait,
      reason: "discussion comment ##{id} updated",
      gid: global_relay_id,
    )
  end

  def notify_socket_subscribers_on_create
    # If it's a top-level comment, trigger an update for the timeline.
    # If this is a nested comment, trigger an update for the parent comment.

    if top_level_comment?
      channel = GitHub::WebSocket::Channels.discussion_timeline(discussion)
      return unless channel

      GitHub::WebSocket.notify_discussion_channel(
        discussion,
        channel,
        timestamp: Time.now.to_i,
        wait: default_live_updates_wait,
        reason: "discussion comment ##{id} updated",
      )
    else
      channel = GitHub::WebSocket::Channels.discussion(discussion)
      return unless channel

      GitHub::WebSocket.notify_discussion_channel(discussion, channel,
        timestamp: Time.now.to_i,
        wait: default_live_updates_wait,
        reason: "discussion comment ##{id} updated",
        gid: parent_comment.global_relay_id,
      )
    end
  end

  def async_path_uri
    return @async_path_uri if defined?(@async_path_uri)

    @async_path_uri = async_repository.then(&:async_owner).then do
      URI_TEMPLATE.expand(
        owner:  repository.owner.login,
        name:   repository.name,
        number: discussion.number,
        comment_id: id,
      )
    end
  end

  # Public: Is this comment a reply to the original discussion and not another comment?
  def top_level_comment?
    parent_comment_id.nil?
  end

  # Public: Is this comment a nested (or child) comment, meaning is it a reply to
  # another comment on the Discussion and not to the original Discussion itself?
  def is_a_child_comment?
    !top_level_comment?
  end

  # Public: Can a comment be made in reply to this comment, nested under it?
  def can_be_commented_on?
    top_level_comment? && !comment_hidden
  end

  # Public: How many comments are nested under this comment?
  def comment_count
    comments.count
  end

  # Public: Determine the author association for each comment for the comment's author to the
  # repository.
  #
  # discussion - Discussion
  # actor - the current User
  # comments - optional list of DiscussionComments to use
  #
  # Returns a Hash of DiscussionComment ID => Symbol.
  def self.repository_author_associations_by_comment_id(discussion, actor:, comments: nil)
    result = {}
    comments ||= discussion.filter_spam_comments_for(actor)

    promises = comments.map do |comment|
      association = CommentAuthorAssociation.new(comment: comment, viewer: actor)
      association.async_to_sym
    end

    associations = Promise.all(promises).sync

    comments.each_with_index do |comment, i|
      result[comment.id] = associations[i]
    end

    result
  end

  # Public: Determine which comments for the specified discussion can be modified by the
  # given user. Should be kept in sync with #modifiable_by?.
  #
  # discussion - Discussion
  # actor - the current User
  # comment_ids - optionally pass in an array of comment ids to narrow the scope
  #
  # Returns a Hash of DiscussionComment ID => Boolean.
  def self.modifiable_by_comment_id(discussion, actor:, comment_ids: nil)
    result = Hash.new(false)

    # Anonymous users can't modify comments
    return result unless actor

    # Require the user to have a verified email address before we let them edit any comment
    return result if actor.should_verify_email?

    # Can't modify comments in an archived repository
    repo = discussion.repository
    return result unless repo && !repo.archived?

    # If the author of the discussion has blocked this user, don't let them edit any comment
    return result if discussion.user && actor.blocked_by?(discussion.user)

    # If the conversation has been locked, only those with write access
    # to the repository can continue to comment
    return result if discussion.locked? && !repo.writable_by?(actor)

    modifiable_comments = discussion.filter_spam_comments_for(actor).not_wiped.
      select(:id)

    if comment_ids.present?
      modifiable_comments = modifiable_comments.where(id: comment_ids)
    end

    unless repo.writable_by?(actor) || repo.adminable_by?(actor)
      # Unless you have admin or write access to the repo, you are only allowed to edit
      # your own comments.
      modifiable_comments = modifiable_comments.for_user(actor)
    end

    modifiable_comments.pluck(:id).each do |comment_id|
      result[comment_id] = true
    end

    result
  end

  # Public: Can the given actor change the contents of this comment? Should be kept in sync
  # with #modifiable_by_comment_id.
  def modifiable_by?(actor)
    return false unless repository&.discussions_enabled?

    response = ::Permissions::Enforcer.authorize(
      action: :edit_discussion_comment,
      actor: actor,
      subject: self
    )
    response.allow?
  end

  def async_viewer_can_update?(viewer)
    async_repository.then do |repository|
      repository && viewer_cannot_update_reasons(viewer).empty?
    end
  end

  def viewer_can_update?(viewer)
    async_viewer_can_update?(viewer).sync
  end

  # Public: Determine if the specified user can mark and unmark the correct answer in the
  # given discussion.
  #
  # discussion - a Discussion
  # actor - the currently authenticated User
  #
  # Returns a Boolean.
  def self.can_toggle_answer_in_discussion?(discussion, actor:)
    # Ensure the feature flag is enabled
    return false unless discussion.repository&.discussions_enabled?

    response = ::Permissions::Enforcer.authorize(
      action: :toggle_discussion_answer,
      actor: actor,
      subject: discussion
    )
    response.allow?
  end

  # Public: Is this comment one that is eligible to be marked as the answer?
  def marking_as_answer_allowed?
    # If the discussion already has an answer, can't mark another one
    return false if discussion&.answered?

    # Wiped comments are soft-deleted and no longer have any content,
    # so they don't make a reasonable answer
    return false if wiped?

    # Hidden comments are not eligible to be marked as the answer
    return false if minimized?

    # Only top-level comments (not those nested in a thread) can be marked
    # as the answer
    top_level_comment?
  end

  def can_mark_as_answer?(actor)
    return false unless marking_as_answer_allowed?

    self.class.can_toggle_answer_in_discussion?(discussion, actor: actor)
  end

  # Public: Determine which comment can be unmarked as the answer for a given discussion.
  #
  # discussion - a Discussion
  # actor - the current User or nil
  #
  # Returns a Hash of DiscussionComment ID => Boolean.
  def self.can_unmark_as_answer_by_comment_id(discussion, actor:)
    result = Hash.new(false)

    return result unless can_toggle_answer_in_discussion?(discussion, actor: actor)

    if discussion.chosen_comment_id
      result[discussion.chosen_comment_id] = true
    end

    result
  end

  def can_unmark_as_answer?(actor)
    # If this comment isn't marked as the answer already, can't unmark it as such
    return false unless answer?

    self.class.can_toggle_answer_in_discussion?(discussion, actor: actor)
  end

  def async_viewer_cannot_update_reasons(viewer)
    Promise.resolve(viewer_cannot_update_reasons(viewer))
  end

  def viewer_cannot_update_reasons(viewer)
    return [:login_required] unless viewer
    context = { repo: repository, discussion: discussion }
    errors = ContentAuthorizer.authorize(viewer, :DiscussionComment, :edit, context).errors.
      map(&:symbolic_error_code)
    errors << :insufficient_access unless modifiable_by?(viewer)
    errors
  end

  def response_to_question?
    discussion.question?
  end

  def answer?
    persisted? && discussion && id == discussion.chosen_comment_id
  end

  def mark_as_answer
    return false unless discussion
    discussion.chosen_comment_id = id
    return false unless discussion.save

    event = DiscussionEvent.new(discussion: discussion, actor: actor,
      event_type: :answer_marked, comment: self)
    return false unless event.save

    GlobalInstrumenter.instrument "discussion_comment.mark_as_answer", discussion_comment: self,
      actor: actor

    true
  end

  def unmark_as_answer
    return false unless answer?
    discussion.chosen_comment_id = nil
    return false unless discussion.save

    event = DiscussionEvent.new(discussion: discussion, actor: actor,
      event_type: :answer_unmarked, comment: self)
    return false unless event.save

    GlobalInstrumenter.instrument "discussion_comment.unmark_as_answer", discussion_comment: self,
      actor: actor

    true
  end

  def delete_wiped_parent_if_no_children
    return unless parent_comment&.wiped?
    parent_comment.destroy unless parent_comment.comments.exists?
  end

  # Public: Remove the given discussion comments as the chosen answer in their discussions.
  #
  # chosen_comments - a list of DiscussionComments that are marked as the answer
  def self.unmark_as_answers(chosen_comments)
    discussion_ids = chosen_comments.map(&:discussion_id).compact.uniq
    discussions = Discussion.where(id: discussion_ids)
    discussions.each do |discussion|
      discussion.events.create(event_type: :answer_unmarked, comment_id: discussion.chosen_comment_id)
    end
    discussions.update_all(chosen_comment_id: nil)
  end

  # Public: Determine if this comment is locked for the given user.
  def async_locked_for?(viewer)
    async_discussion.then do |discussion|
      discussion.async_locked_for?(viewer)
    end
  end

  def reaction_groups
    async_reaction_groups.sync
  end

  def async_reaction_groups
    Platform::Loaders::DiscussionCommentReactionGroups.load(self)
  end

  # Public: Returns the list of reactions groups of each discussion comment
  #
  # discussion - a Discussion
  # actor - the current User or nil
  # comments - the list of comments to use
  #
  # Returns a Hash of DiscussionComment ID => Array of ReactionGroup.
  def self.reaction_groups_by_comment_id(discussion, actor:, comments: nil)
    result = {}
    comments ||= discussion.filter_spam_comments_for(actor)

    promises = comments.map do |comment|
      comment.async_reaction_groups
    end

    reaction_groups = Promise.all(promises).sync

    comments.each_with_index do |comment, i|
      result[comment.id] = reaction_groups[i]
    end

    result
  end

  # Public: Checks spamminess of associated discussion.
  #
  # Returns a Boolean.
  def belongs_to_spammy_content?
    discussion&.spammy?
  end

  def react(actor:, content:)
    DiscussionCommentReaction.react(user: actor, discussion_comment_id: id, content: content)
  end


  def threaded?
    parent_comment_id.present?
  end

  def unreact(actor:, content:)
    DiscussionCommentReaction.unreact(user: actor, discussion_comment_id: id, content: content)
  end

  private

  # Private: Wipes the body of this comment and sets its deleted_at to
  # the current time, thus soft-deleting it, as well as unmarking it as
  # the answer if it's currently the answer.
  #
  # Returns a Boolean indicating success.
  def wipe
    new_attrs = { body: "", deleted_at: Time.zone.now }
    success = if answer?
      transaction do
        success = unmark_as_answer && update(new_attrs)
        raise ActiveRecord::Rollback unless success
        success
      end
    else
      update(new_attrs)
    end
    instrument_deletion_event if success
    success
  end

  def user_exists_for_open_discussion
    if user.nil? && discussion&.open?
      errors.add(:user, "can't be blank")
    end
  end

  def user_has_verified_email
    return unless user

    # If we're converting an existing issue to a discussion, don't require the issue comment
    # to have been created by a user with a verified email address.
    return unless discussion && discussion.open?

    if user.should_verify_email?
      errors.add(:user, "must have a verified email address")
    end
  end

  def update_discussion_comment_count
    return unless discussion
    discussion.update_comment_count
  end

  def set_repository
    return unless discussion
    self.repository = discussion.repository
  end

  def ensure_author_is_not_blocked
    return unless user && discussion
    return if discussion && discussion.repository.pushable_by?(user)

    potential_blockers_to_check = [discussion.user]
    potential_blockers_to_check << repository.owner if repository.owner

    if user.blocked_by?(potential_blockers_to_check)
      errors.add(:user, "cannot comment at this time")
    end
  end

  def ensure_parent_comment_is_for_discussion
    return unless parent_comment && discussion_id

    if parent_comment.discussion_id != discussion_id
      errors.add(:parent_comment, "is in a different discussion")
    end
  end

  def ensure_at_most_one_level_deep
    return unless parent_comment

    if parent_comment.parent_comment
      errors.add(:parent_comment, "is already in a thread, cannot reply to it")
    end
  end

  def parent_comment_exists
    return unless parent_comment_id

    unless parent_comment
      errors.add(:parent_comment, "does not exist")
    end
  end

  def parent_comment_is_not_hidden
    return unless parent_comment

    if parent_comment.comment_hidden
      errors.add(:parent_comment, "has been hidden and cannot be replied to")
    end
  end

  def parent_comment_is_not_self
    return unless parent_comment_id

    if parent_comment_id == id
      errors.add(:parent_comment, "cannot be itself")
    end
  end

  def ensure_not_a_parent
    return if comments.empty?

    errors.add(:base, "Has nested comments and cannot be deleted.")
    throw(:abort)
  end

  def update_parent_comment_websocket
    return unless discussion && parent_comment
    channel = GitHub::WebSocket::Channels.discussion(discussion)
    GitHub::WebSocket.notify_discussion_channel(discussion, channel,
      timestamp: Time.now.to_i,
      wait: default_live_updates_wait,
      reason: "discussion comment ##{id} deleted",
      gid: parent_comment.global_relay_id,
    )
  end

  def instrument_creation_event
    GlobalInstrumenter.instrument "discussion_comment.create",
      discussion_comment: self
  end

  def instrument_update_event
    GlobalInstrumenter.instrument "discussion_comment.update",
      discussion_comment: self, actor: actor
  end

  def instrument_deletion_event
    # Audit log
    instrument :destroy

    # Hydro
    GlobalInstrumenter.instrument "discussion_comment.delete",
      discussion_comment: self, actor: actor
  end

  def audit_log_update_event
    previous_body = previous_changes[:body].try(:first)

    instrument :update, old_body: previous_body
  end

  def event_prefix() :discussion_comment end
  def event_payload
    payload = {
      :repo =>        repository,
      :discussion =>  discussion,
      event_prefix => self,
      :body =>        body,
      :user =>        user,
    }

    if repository&.organization
      payload[:org] = repository.organization
    end

    payload
  end
end
