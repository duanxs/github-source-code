# frozen_string_literal: true

class CodeSearchIndexedHead < ApplicationRecord::Collab

  extend GitHub::Encoding
  force_utf8_encoding :ref

  validates :commit_oid, presence: true
  validate :commit_oid_is_valid_full_sha1
  validates :ref, presence: true
  validates :repository, presence: true

  belongs_to :repository

  class InvalidRepositoryError < ArgumentError; end

  # Get latest indexed sha
  def self.for_repo(repo)
    indexed_head = find_by(repository: repo, ref: default_ref(repo))

    indexed_head&.commit_oid || GitHub::NULL_OID
  end

  # Update latest indexed sha
  def self.update_for_repo(repo, commit_oid)
    ref = default_ref(repo)

    # Using an UPSERT here to avoid running into race conditions
    github_sql.run(<<-SQL, repository_id: repo.id, ref: ref, commit_oid: commit_oid)
      INSERT INTO code_search_indexed_heads (repository_id, ref, commit_oid, created_at, updated_at)
      VALUES (:repository_id, :ref, :commit_oid, NOW(), NOW())
      ON DUPLICATE KEY UPDATE
        commit_oid = VALUES(commit_oid),
        updated_at = VALUES(updated_at)
    SQL

    self.find_by(repository: repo, ref: ref)
  end

  # Returns the ref for the default branch of the given repo
  def self.default_ref(repo)
    raise InvalidRepositoryError unless repo.is_a?(Repository)

    "refs/heads/#{repo.default_branch}"
  end

  # Public: Boolean is the index up-to-date with the latest sha
  def index_up_to_date?
    commit_oid == repository.rpc.read_refs[self.class.default_ref(repository)]
  end

  def commit_oid_is_valid_full_sha1
    if !GitRPC::Util.valid_full_sha1?(commit_oid)
      errors.add(:commit_oid, "must be a 40 character SHA1")
    end
  end
end
