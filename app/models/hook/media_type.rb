# rubocop:disable Style/FrozenStringLiteralComment

require "github/pi_media_type"

class Hook::MediaType < GitHub::PiMediaType
  DefaultVersion = :v3

  def payload_version
    @payload_version ||= DefaultVersion
  end
end
