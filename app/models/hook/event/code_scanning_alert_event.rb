# frozen_string_literal: true
class Hook::Event::CodeScanningAlertEvent < Hook::Event
  supports_targets(*DEFAULT_TARGETS)

  description "Code Scanning alert created, fixed in branch, or closed"

  event_attr :action, :repository_id, :alert_number, required: true
  event_attr :resolution, :commit_oid, :ref, :actor_id

  feature_flag :code_scanning_integration do |flag, actor|
    if GitHub.enterprise?
      true
    else
      GitHub.flipper[flag].enabled?(actor)
    end
  end

  def target_repository
    @repository ||= Repository.find_by(id: repository_id)
  end

  def target_organization
    owner = target_repository.owner
    owner.organization? ? owner : nil
  end

  def alert
    return @alert if defined? @alert
    @alert = alert_response_data&.result
  end

  def actor
    return nil unless actor_id.present?
    @actor ||= User.find_by_id(actor_id)
  end

  private
  def alert_response_data
    GitHub::Turboscan.result(
      repository_id: repository_id,
      number: alert_number,
    )&.data.tap do |alert|
      GitHub.dogstats.increment("turboscan_client.webhook.fetch_alert_nil")
      GitHub::Logger.log(
        fn: "Turboscan::alert_response_data",
        msg: "the response was nil",
        repository_id: repository_id,
        number: alert_number,
      )
    end
  end
end
