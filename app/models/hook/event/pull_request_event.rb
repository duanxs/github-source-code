# rubocop:disable Style/FrozenStringLiteralComment

class Hook::Event::PullRequestEvent < Hook::Event
  supports_targets *DEFAULT_TARGETS

  def self.description(desc = nil)
    actions = [
      :opened,
      :closed,
      :reopened,
      :edited,
      :assigned,
      :unassigned,
      :review_requested,
      :review_request_removed,
      :labeled,
      :unlabeled,
      :synchronized,
      :ready_for_review,
      :converted_to_draft,
      :locked,
      :unlocked,
    ]

    actions.map! { |action|  action.to_s.humanize(capitalize: false) }
    @description = "Pull request #{actions.to_sentence(last_word_connector: ", or ")}."
  end

  event_attr :action, :pull_request_id, required: true
  event_attr :actor_id, :label_id, :assignee_id, :subject_id, :subject_type, :changes, :before, :after

  def pull_request
    @pull_request ||= PullRequest.find_by_id(pull_request_id)
  end

  def target_repository
    pull_request.try(:repository)
  end

  def actor
    @actor ||= User.find_by_id(actor_id) || pull_request.try(:user)
  end

  # The label which was labeled/unlabeled
  def label
    return @label if defined?(@label)

    @label = Label.find_by_id(label_id)
  end

  # The user or team which was review_requested/review_request_removed
  def requested_reviewer
    return @requested_reviewer if defined?(@requested_reviewer)

    if subject_type == "Team"
      @requested_reviewer = Team.find_by_id(subject_id)
    else
      @requested_reviewer = User.find_by_id(subject_id)
    end
  end

  # The user which was assigned/unassigned
  def assignee
    return @assignee if defined?(@assignee)

    @assignee = User.find_by_id(assignee_id)
  end

  def changes
    return unless changes_attr

    {}.tap do |changes_hash|
      changes_hash[:body] = { from: changes_attr[:old_body] } if body_changes?
      changes_hash[:title] = { from: changes_attr[:old_title] } if title_changes?

      changes_hash[:base] = {} if base_ref_changes? || base_sha_changes?
      changes_hash[:base][:ref] = { from: changes_attr[:old_base_ref] } if base_ref_changes?
      changes_hash[:base][:sha] = { from: changes_attr[:old_base_sha] } if base_sha_changes?
    end
  end

  def deliverable?
    target_repository.present?
  end

  private

  def changes_attr
    attributes.with_indifferent_access[:changes]
  end

  def body_changes?
    changes_attr[:old_body] && changes_attr[:body]
  end

  def title_changes?
    changes_attr[:old_title] && changes_attr[:title]
  end

  def base_ref_changes?
    changes_attr[:old_base_ref] && changes_attr[:base_ref]
  end

  def base_sha_changes?
    changes_attr[:old_base_sha] && changes_attr[:base_sha]
  end
end
