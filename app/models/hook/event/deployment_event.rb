# rubocop:disable Style/FrozenStringLiteralComment

class Hook::Event::DeploymentEvent < Hook::Event
  supports_targets *DEFAULT_TARGETS
  description "Repository was deployed or a deployment was deleted."

  event_attr :deployment_id, required: true
  event_attr :action, required: false
  event_attr :specific_app_id

  def deployment
    @deployment ||= Deployment.find(deployment_id)
  end

  def target_repository
    deployment.repository
  end

  def actor
    deployment.creator
  end

end
