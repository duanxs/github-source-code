# frozen_string_literal: true

class Hook::Event::WorkflowRunEvent < Hook::Event
  supports_targets Integration

  feature_flag :actions_workflow_run_webhook

  description "Workflow run requested or completed on a repository."

  event_attr :run_id, :action, required: true

  def feature_flag_enabled?
    super || (target_repository && GitHub.flipper[:actions_workflow_run_webhook].enabled?(target_repository))
  end

  # Helper method to look up the workflow_run using the id we were passed.
  def workflow_run
    @workflow_run ||= Actions::WorkflowRun.includes(:workflow, :repository, check_suite: :creator).find_by(id: run_id)
  end

  # The repository that this event is associated with. The delivery
  # system will use this to find subscribed repository and organization
  # hooks.
  #
  # This will automatically be included in hook payloads.
  def target_repository
    workflow_run.try(:repository)
  end

  # The workflow associated with this workflow_run
  def workflow
    workflow_run.try(:workflow)
  end

  # The user who performed the action.
  #
  # This will automatically be included in hook payloads.
  def actor
    @actor ||= workflow_run.try(:check_suite).try(:creator)
  end

  def deliverable?
    workflow_run.present? && workflow.present? && workflow.active? && target_repository.present?
  end
end
