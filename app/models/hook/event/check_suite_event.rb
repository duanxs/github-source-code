# frozen_string_literal: true

class Hook::Event::CheckSuiteEvent < Hook::Event
  include Hook::Event::ChecksDependency

  supports_targets(*DEFAULT_TARGETS)
  description "Check suite is requested, rerequested, or completed."

  event_attr :check_suite_id, :action, required: true
  event_attr :specific_app_only, :actor_id

  def check_suite
    @check_suite ||= CheckSuite.find(check_suite_id)
  end

  def deliverable?
    check_suite && !target_repository.nil? && target_repository.commits.exist?(check_suite.head_sha)
  end
end
