# frozen_string_literal: true

class Hook::Event::PackageEvent < Hook::Event
    supports_targets *DEFAULT_TARGETS

    description "GitHub Packages published or updated in a repository."

    event_attr :registry_package_id, :package_version_id, :action, :actor_id, required: true

    # Helper method to look up the package using the id we were passed.
    def package
      @package ||= Registry::Package.find(registry_package_id)
    end

    # Helper method to look up the package_version using the id we were passed.
    def package_version
      @package_version ||= Registry::PackageVersion.find(package_version_id)
    end

    # The repository that this event is associated with. The delivery
    # system will use this to find subscribed repository and organization
    # hooks.
    #
    # This will automatically be included in hook payloads.
    def target_repository
      package.try(:repository)
    end

    # The user who performed the action.
    #
    # This will automatically be included in hook payloads.
    def actor
      @actor ||= User.find(actor_id)
    end

    def deliverable?
      target_repository.present? && package.present?
    end
end
