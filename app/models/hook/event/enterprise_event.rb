# rubocop:disable Style/FrozenStringLiteralComment

class Hook::Event::EnterpriseEvent < Hook::Event
  # this event is only available in GHE Server
  # adding the check for a single business environment here rather than
  # wrapping the entire class in a check to so that the rest of the codebase
  # doesn't need special handling for whether the class is defined or not
  supports_targets Business if GitHub.single_business_environment?

  display_name "Enterprise"
  description "Global anonymous access enabled, anonymous access disabled."

  event_attr :action, :actor_id, required: true

  def actor
    @actor ||= User.find(actor_id)
  end
end
