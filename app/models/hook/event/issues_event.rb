# rubocop:disable Style/FrozenStringLiteralComment

class Hook::Event::IssuesEvent < Hook::Event
  supports_targets *DEFAULT_TARGETS

  def self.description(desc = nil)
    actions = [
      :opened,
      :edited,
      :deleted,
      :transferred,
      :pinned,
      :unpinned,
      :closed,
      :reopened,
      :assigned,
      :unassigned,
      :labeled,
      :unlabeled,
      :milestoned,
      :demilestoned,
      :locked,
      :unlocked,
    ]

    @description = "Issue #{actions.to_sentence(last_word_connector: ", or ")}."
  end

  event_attr :action, :issue_id, :actor_id, required: true
  event_attr :label_id, :assignee_id, :changes, :milestone_id

  def issue
    @issue ||= Issue.find_by(id: issue_id)
  end

  def target_repository
    issue&.repository
  end

  def actor
    @actor ||= User.find(actor_id)
  end

  # The label which was labeled/unlabeled
  def label
    return @label if defined?(@label)

    @label = Label.find_by_id(label_id)
  end

  # The milestone which was milestoned/demilestoned
  def milestone
    return @milestone if defined?(@milestone)

    @milestone = Milestone.find_by_id(milestone_id)
  end

  # The user which was assigned/unassigned
  def assignee
    return @assignee if defined?(@assignee)

    @assignee = User.find_by_id(assignee_id)
  end

  def changes
    return unless changes_attr

    GitHub.dogstats.increment("hooks.stale", tags: ["hook_event:issues"]) if stale_changes?

    {}.tap do |changes_hash|
      changes_hash[:body] = { from: changes_attr[:old_body] } if body_changes?
      changes_hash[:title] = { from: changes_attr[:old_title] } if title_changes?
    end
  end

  def deliverable?
    issue.present? && target_repository.present?
  end

  def source_issue_transfer
    @source_issue_transfer ||= IssueTransfer.includes(:old_issue, :old_repository).find_by(old_issue_id: issue_id)
  end

  def target_issue_transfer
    @target_issue_transfer ||= IssueTransfer.includes(:new_issue, :new_repository).find_by(new_issue_id: issue_id)
  end

  private

  def stale_changes?
    return unless body_changes? || title_changes?

    (body_changes? && changes_attr[:old_body] == issue.body) || (title_changes? && changes_attr[:old_title] == issue.title)
  end

  def changes_attr
    attributes.with_indifferent_access[:changes]
  end

  def body_changes?
    changes_attr[:old_body] && changes_attr[:body]
  end

  def title_changes?
    changes_attr[:old_title] && changes_attr[:title]
  end
end
