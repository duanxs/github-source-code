# frozen_string_literal: true

class Hook::Event::ContentReferenceEvent < Hook::Event
  supports_targets Integration

  description "Content reference created when a link is posted in an issue or pull request."

  event_attr :content_reference_id, :action, required: true

  def content_reference
    @content_reference ||= ContentReference.find(content_reference_id)
  end

  def content
    @content ||= content_reference.content
  end

  def actor
    @actor ||= content_reference.user
  end

  # Right now all the models that unfurls support are within a repository
  # but this will not always be the case. We'll need to update this when
  # we're ready to support unfurls in org level content
  def target_repository
    content_reference.repository
  end

  def subscribed_hooks
    @target_hooks ||= find_integration_hooks
  end

  private

  def find_integration_hooks
    integrations = IntegrationContentReference.integrations_listening_for(
      repository: target_repository,
      url: content_reference.reference,
    )

    Hook.where(
      installation_target_type: Integration.to_s,
      installation_target_id:   integrations.pluck(:id),
    ).active
  end
end
