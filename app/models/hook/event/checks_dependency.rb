# frozen_string_literal: true

class Hook::Event
  module ChecksDependency
    def actor
      return @actor if defined?(@actor)

      @actor = if actor_id
        User.find_by_id(actor_id)
      else
        check_suite&.push&.pusher || User.ghost
      end
    end

    def app
      @app ||= check_suite.github_app
    end

    def app_is_installed?
      app.installations.not_suspended.with_repository(target_repository).any?
    end

    # Override subscribed_hooks to only include the specific app's hook.
    # Specified on a per action basis when the action is subscribed to.
    def subscribed_hooks
      if specific_app_only && app_is_installed?
        return @target_hook if defined?(@target_hook)
        @target_hook = []

        if app.subscribable_hook?
          @target_hook << app.hook
        end

        @target_hook
      else
        super
      end
    end

    # Override subscribed_installations_for so that the installation
    # information is included in event payloads for these specific app
    # action payloads, where the app may not be subscribed to the event.
    def subscribed_installations_for(integration_id)
      if specific_app_only && app.id == integration_id.to_i
        app.installations.not_suspended.with_repository(target_repository)
      else
        super
      end
    end

    def target_repository
      check_suite.repository
    end
  end
end
