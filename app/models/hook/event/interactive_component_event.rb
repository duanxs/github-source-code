# frozen_string_literal: true

class Hook::Event::InteractiveComponentEvent < Hook::Event
  supports_targets Integration
  auto_subscribed
  description "A user interacted with an interactive component."

  event_attr :action, :actor_id, :integration_id, :associations,
    :interactive_component_id, :element_id, :interaction_id, required: true


  def subscribed_hooks
    return [] unless integration.subscribable_hook?

    [integration.hook]
  end

  def actor
    @actor ||= User.find(actor_id)
  end

  def integration
    @integration ||= Integration.find(integration_id)
  end

  def associated_resources
    @associations ||= associations.each_with_object({}) do |(key, association), hash|
      case key
      when :repository_id
        hash[:repository] = Repository.find(association)
      else
        raise ArgumentError, "#{key} unhandled in associations"
      end
    end
  end

  def interactive_component
    @interactive_component ||= InteractiveComponent.find(interactive_component_id)
  end

  def container
    interactive_component.container
  end

  def container_type
    interactive_component.container_type.underscore
  end

  def interacted_element
    interactive_component.element_by_id(element_id)
  end

  def installations
    case interactive_component.container.associated_domain
    when InteractiveComponent::Container::ASSOCIATED_DOMAINS[:repo]
      integration.installations.with_repository(associated_resources[:repository])
    end
  end

  def subscribed_installations_for(integration_id)
    unless integration_id.to_i == integration.id
      raise ArgumentError("Invalid integration id")
    end

    installations
  end

  def deliverable?
    installations.present?
  end
end
