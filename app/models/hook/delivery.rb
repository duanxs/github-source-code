# frozen_string_literal: true

class Hook::Delivery
  attr_reader :hook_event, :parent, :version, :hooks, :muted_hooks
  attr_accessor :hookshot_payload

  def initialize(hook_event, parent, version, hooks)
    @hook_event = hook_event
    @parent = parent
    @version = version
    @hooks, @muted_hooks = hooks.partition { |h| delivery_allowed_for_hook?(h) }
  end

  def payload
    hook_event.to_payload_version(version)
  end

  def headers_for(hook)
    hook_event.headers_for(hook)
  end

  def guid
    hook_event.guid
  end

  def target_repository
    hook_event.target_repository
  end

  private

  def delivery_allowed_for_hook?(hook)
    OauthApplicationPolicy::Hook.new(hook, hook_event).satisfied?
  end
end
