# frozen_string_literal: true

# Hook Directory is a facade that the internal twirp hooks api is using
# Is trying to mimic an event instrumentation and from that is getting its
# subscribed hooks. It is not by all means a clean cut but we didn't want to re-write
# our hook event construction layer in order to validate our experiment of log first hook
# hydration
#
class Hook::Directory
  EVENT_TYPE_SEPARATOR = ".".freeze

  # Public: Provide search capabillities for hooks
  #
  # req - The Twirp request as a Github::Proto::Hooks::V1::SearchUsersRequest
  #
  # Returns a list of hooks based on the request query params
  def self.search_hooks_from_proto_request(req, limit: 50)
    event, action = req.event_type.split(EVENT_TYPE_SEPARATOR)

    case event
    when "issue"
      issue_event = Hook::Event::IssuesEvent.new(
        action: action,
        issue_id: req.entity_id,
        actor_id: User.ghost.id, # Issue have actor_id as required field but we do not need it for getting the subscriptions
      )
      issue_event.subscribed_hooks.first(limit)
    end
  end
end
