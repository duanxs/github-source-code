# rubocop:disable Style/FrozenStringLiteralComment

class Hook
  module ConfigAccessor
    extend ActiveSupport::Concern

    module ClassMethods
      def config_accessor(*attrs)
        config_reader *attrs
        config_writer *attrs
      end

      def config_reader(*attrs)
        attrs.each do |attr|
          define_method attr do
            config[attr.to_s]
          end
        end
      end

      def config_writer(*attrs)
        attrs.each do |attr|
          define_method :"#{attr}=" do |value|
            self.update_existing_config(attr.to_s => value)
          end
        end
      end
    end
  end
end
