# rubocop:disable Style/FrozenStringLiteralComment

class Hook::Payload::PushPayload < Hook::Payload
  # Helper methods that require the module to respond to
  # repository, before, after and ref
  include Push::CommitsHelper

  delegate :pusher, :repository, :before, :after, :ref, to: :hook_event

  version(:v3) do
    {
      ref: ref,
      before: before,
      after: after,
      repository: repo_hash,
      pusher: user_hash(pusher),
    }.tap { |payload| payload.merge!(git_only) }
  end

  def git_only
    return {} unless @include_git_data

    ActiveRecord::Base.connected_to(role: :reading) do
      {
        created: created?,
        deleted: deleted?,
        forced: forced?,
        base_ref: base_ref,
        compare: compare_url,
        commits: commits_hashes_array(commits_pushed),
        head_commit: commit_hash(head_commit),
      }
    end
  end

  def initialize(hook_event, include_git_data: true)
    @include_git_data = include_git_data
    super(hook_event)
  end

  # Override the #forced? method in Push::CommitsHelper
  # Only return true if the push isn't creating or deleting
  # and if the before and merge_base don't match
  def forced?
    !created? &&
    !deleted? &&
    before != merge_base_commit_sha
  end

  # Builds an array of commit hashes.
  #
  # commits - The commits to build hashes for.
  #
  # Returns an Array of Hashes.
  def commits_hashes_array(commits)
    commits.map do |commit|
      commit_hash(commit)
    end
  end

  # Builds the payload representation for the given commit.
  #
  # commit - A Commit instance.
  #
  # Returns a Hash.
  def commit_hash(commit)
    return unless commit

    diff = commit_file_stats_hash(commit)
    hash = {
      id: commit.oid,
      tree_id: commit.tree_oid,
      distinct: distinct?(commit),
      message: commit.message,
      timestamp: commit.committed_date.xmlschema,
      url: commit_url(commit),
      author: {
        name: commit.author_name,
        email: commit.author_email,
      },
      committer: {
        name: commit.committer_name,
        email: commit.committer_email,
      },
      added: diff["added"],
      removed: diff["removed"],
      modified: diff["modified"],
    }
    if user = commit.author
      hash[:author][:username] = user.login
    end
    if user = commit.committer
      hash[:committer][:username] = user.login
    end
    hash
  end

  # Builds the diff stats payload representation for the given commit.
  #
  # commit - A Commit instance.
  #
  # Returns a Hash.
  def commit_file_stats_hash(commit)
    hash = {
      "removed" => [],
      "added" => [],
      "modified" => [],
    }
    deltas = commit.init_diff.deltas

    deltas.each do |delta|
      case delta.status
      when "A"
        hash["added"] << delta.new_file.path
      when "D"
        hash["removed"] << delta.old_file.path
      when "R"
        hash["added"] << delta.new_file.path
        hash["removed"] << delta.old_file.path
      else
        hash["modified"] << delta.new_file.path
      end
    end

    hash["removed"].sort!
    hash["added"].sort!
    hash["modified"].sort!

    hash
  end

  # Builds the payload representation for the Repository.
  #
  # Returns a Hash.
  def repo_hash
    serialized_repo = Api::Serializer.repository_hash(repository)

    serialized_owner =
      user_hash(repository.owner).merge(serialized_repo[:owner].to_h)

    serialized_repo = serialized_repo.
      merge(legacy_repo_hash(repository)).
      merge(owner: serialized_owner)

    # NOTE: In order to ensure backwards compatibility, we need to make sure that the repo
    # pushed_at and created_at timestamps are sent at unix timestamps
    serialized_repo[:pushed_at] = serialized_repo[:pushed_at].to_i
    serialized_repo[:created_at] = serialized_repo[:created_at].to_i

    serialized_repo
  end

  # Builds the payload representation for the User
  #
  # Returns a Hash.
  def user_hash(user)
    if user
      {
        name: user.login,
        email: user.outbound_email,
      }
    else
      {
        name: "none",
      }
    end
  end

  # Builds the absolute URL for the given commit.
  #
  # commit - A Commit instance.
  #
  # Returns a String URL.
  def commit_url(commit)
    "#{repo_url}/commit/#{commit.oid}"
  end

  # Builds the absolute URL for the current Repository.
  #
  # Returns a String URL.
  def repo_url
    @repo_url ||= "%s/%s/%s" % [GitHub.url, repository.owner.to_s, repository.to_s]
  end

  def base_ref
    return @base_ref if defined?(@base_ref)
    return unless created? || (distinct_commits && distinct_commits.empty?)

    # hash of 'oid' => [Ref, ...] for all tags/branches
    target_map = repository.refs.group_by(&:target_oid)

    # first, try the new sha being pushed
    matches = target_map[after]

    # then search the pushed commits for one we recognize
    matches ||=
      if base_commit = commits_pushed.find { |commit| target_map.key?(commit.oid) }
        target_map[base_commit.oid]
      end

    # and fallback to the previous sha value
    matches ||= target_map[before] || []

    matches = matches.map(&:qualified_name)
    matches.delete(ref)
    @base_ref = matches.first
  end

  def head_commit
    return if deleted?

    @head_commit ||= begin
      commits_pushed.detect { |c| c.oid == after } || repository.commits.history(after, 1).first
    # It's valid for a tag to point to a blob though it happens rarely.
    # Capture the error and return nil.
    rescue GitRPC::BadRepositoryState
      nil
    end
  end

  # Checks if the given Commit is being pushed to the Repository for the
  # first time.
  #
  # commit - A Commit instance.
  #
  # Returns true if the commit is distinct, or false.
  def distinct?(commit)
    distinct_commits.nil? || distinct_commits.include?(commit.oid)
  end

  # A Set of all the distinct commits that from the current push.
  #
  # Returns a Set of String commit SHAs, or nil when all commits are distinct.
  def distinct_commits
    if created? || deleted? || forced?
      nil
    else
      @distinct_commits ||= Set.new(distinct_commits_pushed.map { |c| c.sha })
    end
  end

  def compare_url
    @compare_url ||= if !created?
      "#{repo_url}/compare/#{before[0, 12]}...#{after[0, 12]}"
    elsif commits_pushed.length > 1
      "#{repo_url}/compare/#{commits_pushed.first.oid[0, 12]}^...#{commits_pushed.last.oid[0, 12]}"
    elsif commits_pushed.length == 1
      "#{repo_url}/commit/#{commits_pushed.first.oid[0, 12]}"
    else
      "#{repo_url}/compare/#{branch_name}"
    end
  end

  def commits_pushed
    super # Push::CommitsHelper
  end

  def distinct_commits_pushed
    super # Push::CommitsHelper
  end

  private

  def legacy_repo_hash(repo)
    {
      id: repo.id,
      name: repo.name,
      url: repo.permalink,
      description: repo.description,
      homepage: repo.homepage,
      watchers: repo.stargazer_count,
      stargazers: repo.stargazer_count,
      forks: repo.forks_count,
      fork: repo.fork?,
      size: repo.disk_usage.to_i,
      owner: repo.owner.to_s,
      private: repo.private?,
      open_issues: repo.open_issues_count,
      has_issues: repo.has_issues?,
      has_downloads: repo.has_downloads?,
      has_wiki: repo.has_wiki?,
      language: repo.primary_language_name,
      created_at: repo.created_at,
      pushed_at: repo.pushed_at,
      master_branch: repo.default_branch,
      default_branch: repo.default_branch,
      organization: repo.organization&.to_s,
    }.compact
  end
end
