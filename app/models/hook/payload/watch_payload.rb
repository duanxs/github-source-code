# frozen_string_literal: true

class Hook::Payload::WatchPayload < Hook::Payload

  version(:v3) do
    {
      action: hook_event.action,
    }
  end

end
