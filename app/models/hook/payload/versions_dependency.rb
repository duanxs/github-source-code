# rubocop:disable Style/FrozenStringLiteralComment

class Hook::Payload
  module VersionsDependency
    extend ActiveSupport::Concern

    module ClassMethods
      def defaults(version_name, &block)
        @@defaults ||= {}

        if block_given?
          @@defaults[version_name] = Version.new(:"#{version_name}_defaults", &block)
        else
          @@defaults[version_name]
        end
      end

      def version(version_name, &block)
        @versions ||= {}

        if block_given?
          version = Version.new(version_name, &block)
          version.append defaults(version_name)
          @versions[version_name] = version
        else
          @versions[version_name]
        end
      end
    end # ClassMethods
  end
end
