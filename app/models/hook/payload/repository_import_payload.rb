# frozen_string_literal: true

class Hook::Payload::RepositoryImportPayload < Hook::Payload
  version(:v3) do
    {
      status: hook_event.status,
    }
  end
end
