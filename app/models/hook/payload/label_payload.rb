# frozen_string_literal: true

class Hook::Payload::LabelPayload < Hook::Payload
  version(:v3) do
    {}.tap do |hash|
      hash[:action]   = hook_event.action
      hash[:label]    = Api::Serializer.label_hash(hook_event.label)
      hash[:changes]  = hook_event.changes if hook_event.changes
    end
  end
end
