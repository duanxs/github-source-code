# frozen_string_literal: true

class Hook::Payload::RegistryPackagePayload < Hook::Payload

  version(:v3) do
    {
      action: hook_event.action,
      registry_package: Api::Serializer.registry_package_hash(hook_event.registry_package, hook_event.package_version),
    }
  end

end
