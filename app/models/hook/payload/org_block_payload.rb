# frozen_string_literal: true
class Hook::Payload::OrgBlockPayload < Hook::Payload
  version(:v3) do
    {
      action: hook_event.action,
      blocked_user: Api::Serializer.user_hash(hook_event.blocked_user),
    }
  end
end
