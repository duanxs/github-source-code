# frozen_string_literal: true

class Hook::Payload::RepositoryDispatchPayload < Hook::Payload

  version(:v3) do
    {
      action: hook_event.action,
      branch: hook_event.branch,
      client_payload: hook_event&.client_payload,
    }
  end

end
