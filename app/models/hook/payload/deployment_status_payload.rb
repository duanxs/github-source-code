# frozen_string_literal: true

class Hook::Payload::DeploymentStatusPayload < Hook::Payload

  delegate :id, :state, :deployment, :environment_url, :log_url, :description, to: :deployment_status

  version(:v3) do
    {
      deployment_status: Api::Serializer.deployment_status_hash(deployment_status),
      deployment: Api::Serializer.deployment_hash(deployment),
      action: hook_event.action,
    }
  end

  def deployment_status
    hook_event.deployment_status
  end

end
