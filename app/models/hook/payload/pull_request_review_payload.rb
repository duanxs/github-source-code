# frozen_string_literal: true

class Hook::Payload::PullRequestReviewPayload < Hook::Payload
  delegate :action, :changes, :pull_request_review, :pull_request, to: :hook_event

  version(:v3) do
    {}.tap do |payload|
      payload[:action]       = action
      payload[:review]       = Api::Serializer.pull_request_review_hash(pull_request_review)
      payload[:pull_request] = Api::Serializer.pull_request_hash(pull_request, hook: true, show_merge_settings: true)
      payload[:changes]      = changes if changes
    end
  end
end
