# frozen_string_literal: true

class Hook::Payload::DeletePayload < Hook::Payload

  version(:v3) do
    {
      ref: hook_event.branch_or_tag_name,
      ref_type: hook_event.ref_type,
      pusher_type: hook_event.pusher_type,
    }
  end

end
