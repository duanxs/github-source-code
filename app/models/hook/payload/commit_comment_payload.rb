# frozen_string_literal: true

class Hook::Payload::CommitCommentPayload < Hook::Payload

  version(:v3) do
    {
      action: hook_event.action,
      comment: Api::Serializer.commit_comment_hash(hook_event.commit_comment),
    }
  end
end
