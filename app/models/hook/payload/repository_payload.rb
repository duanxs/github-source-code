# frozen_string_literal: true

class Hook::Payload::RepositoryPayload < Hook::Payload
  version(:v3) do
    {
      action: hook_event.action,
    }.merge(changes_payload)
  end
end
