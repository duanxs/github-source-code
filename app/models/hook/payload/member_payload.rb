# frozen_string_literal: true

class Hook::Payload::MemberPayload < Hook::Payload

  version(:v3) do
    {
      action: hook_event.action,
      member: Api::Serializer.user_hash(hook_event.user),
    }.merge(changes_payload)
  end
end
