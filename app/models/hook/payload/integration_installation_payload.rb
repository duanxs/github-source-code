# frozen_string_literal: true

class Hook::Payload::IntegrationInstallationPayload < Hook::Payload

  version(:v3) do
    {}.tap do |opts|
      opts[:action]       = hook_event.action
      opts[:installation] = installation_hash

      if hook_event.action.to_s == "created"
        opts[:repositories] = repositories_array(hook_event.repositories)
      end
    end
  end

  private

  def installation_hash
    Api::Serializer.integration_installation_hash(hook_event.installation)
  end

  def repositories_array(repositories)
    return [] if repositories.blank?

    repositories.inject([]) do |collection, repo|
      collection << Api::Serializer.repository_identifier_hash(repo)
    end
  end
end
