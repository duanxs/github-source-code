# frozen_string_literal: true

class Hook::Payload::GitHubAppAuthorizationPayload < Hook::Payload
  version(:v3) do
    {}.tap do |opts|
      opts[:action] = hook_event.action
    end
  end
end
