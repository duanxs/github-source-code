# frozen_string_literal: true

class Hook::Payload::SecurityAdvisoryPayload < Hook::Payload
  version(:v3) do
    {
      action: hook_event.action,
      security_advisory: Api::Serializer.security_advisory_hash(hook_event.security_advisory),
    }
  end
end
