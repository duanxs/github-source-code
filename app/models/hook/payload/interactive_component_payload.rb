# frozen_string_literal: true

class Hook::Payload::InteractiveComponentPayload < Hook::Payload
  version(:v3) do
    {}.tap do |opts|
      opts[:action] = hook_event.action
      opts[:interaction_id] = hook_event.interaction_id
      opts[:external_id] = hook_event.interactive_component.external_id
      opts[:element] = hook_event.interacted_element
      opts[:container] = {
        type: hook_event.container_type,
        data: hook_event.container.serialized_hash,
      }

      opts[:associations] = {}
      if hook_event.associated_resources.key?(:repository)
        repo = Api::Serializer.simple_repository_hash(hook_event.associated_resources[:repository])
        opts[:associations][:repository] = repo
      end

      opts[:sender_association] = hook_event.container.author_association(hook_event.actor).to_s
    end
  end
end
