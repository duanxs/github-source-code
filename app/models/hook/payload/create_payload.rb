# frozen_string_literal: true

class Hook::Payload::CreatePayload < Hook::Payload

  version(:v3) do
    {
      ref: hook_event.branch_or_tag_name,
      ref_type: hook_event.ref_type,
      master_branch: hook_event.repository.default_branch,
      description: hook_event.repository.description,
      pusher_type: hook_event.pusher_type,
    }
  end

end
