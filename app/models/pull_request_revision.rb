# frozen_string_literal: true

class PullRequestRevision < ApplicationRecord::Domain::Repositories
  belongs_to :pull_request, inverse_of: :revisions
  belongs_to :actor, class_name: :User

  validates :pull_request_id, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :actor_id, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :number, presence: true, numericality: { only_integer: true, greater_than: 0 }, uniqueness: { scope: :pull_request_id }
  validates :ready, inclusion: { in: [true, false] }
  validates :base_oid,  presence: true, format: { with: /\A[a-f0-9]{40}\Z/, message: "should be a full commit SHA" }
  validates :head_oid,  presence: true, format: { with: /\A[a-f0-9]{40}\Z/, message: "should be a full commit SHA" }
  validates :revised_at, presence: true
  validates :force_pushed, inclusion: { in: [true, false] }
  validates :commits_count, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :push_id, numericality: { only_integer: true, greater_than: 0 }, allow_nil: true

  before_validation :set_revised_at_if_blank, on: :create

  def self.ready
    where(ready: true)
  end

  def self.number_desc
    order("`pull_request_revisions`.`number` DESC")
  end

  def draft?
    !ready?
  end

  # Absolute permalink URL for this pull request revision.
  #
  # include_host - Turn off the `GitHub.url` host in the url. (default true)
  #                pull_request.permalink(include_host: false) => `/github/github/pull/4`
  #
  def permalink(include_host: true)
    return nil unless pull_request&.repository.present?
    return @permalink if defined?(@permalink)

    range_oids = []
    range_oids << base_oid unless base_oid == pull_request.base_sha
    range_oids << head_oid
    @permalink = "#{pull_request.permalink(include_host: include_host)}/files/#{range_oids.join("..")}"
  end

  private

  def set_revised_at_if_blank
    self.revised_at ||= Time.now.utc
  end
end
