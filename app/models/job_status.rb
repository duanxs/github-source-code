# rubocop:disable Style/FrozenStringLiteralComment

class JobStatus
  class RecordNotFound < RuntimeError; end

  def self.throttle(&block)
    ApplicationRecord::Domain::KeyValues.throttle(&block)
  end

  DEFAULT_TTL = 7.days

  def self.find(id)
    json = GitHub.kv.get(cache_key(id)).value!
    return nil unless json
    self.new(JSON.parse(json))
  end

  def self.find!(id)
    found = find(id)
    if !found
      raise RecordNotFound, "job status id not found in memcache: #{id}"
    end
    found
  end

  def self.create(attributes = {})
    status = new(attributes)
    status.save
    status
  end

  STATES = %w[pending started success error].freeze

  attr_reader :id, :state

  def initialize(attributes = {})
    attributes = attributes.with_indifferent_access
    @id = attributes[:id] || SecureRandom.uuid
    @state = attributes[:state] || "pending"
    raise "bad state" if @state && !STATES.include?(@state)

    self.ttl = attributes[:ttl] if attributes.key?(:ttl)
  end

  def ttl=(value)
    # String#to_i has a habit of returning 0 for non-integers. We don't want to make any mistakes
    # so let's use the Integer method instead.
    @ttl = Integer(value)
  end

  def ttl
    @ttl || DEFAULT_TTL
  end

  def save
    expires_at = Time.now + ttl
    GitHub.kv.set(cache_key, to_json, expires: expires_at)
  end

  def destroy
    GitHub.kv.del(cache_key)
  end

  def expire
    GitHub.kv.set(cache_key, to_json, expires: 3.minutes.from_now)
  end

  def pending?
    state == "pending"
  end

  def started?
    state == "started"
  end

  def success?
    state == "success"
  end

  def error?
    state == "error"
  end

  def finished?
    %w[success error].include?(@state)
  end

  def started!
    @state = "started"
    save
  end

  def success!
    @state = "success"
    expire
  end

  def error!
    @state = "error"
    expire
  end

  def to_json
    as_json.to_json
  end

  def as_json
    {
      id: id,
      state: state,
      ttl: ttl,
    }
  end

  # Track the progress of the block. Ensures that the job status is updated
  # regardless of how the block finishes.
  #
  # Yields to the given block to complete that job.
  #
  # Returns nothing.
  def track
    started!
    yield
    success!
  rescue => boom
    error!
    raise boom
  end

  private

  def cache_key
    self.class.send(:cache_key, id)
  end

  def self.cache_key(id)
    "jobs:status:%s" % id
  end
  private_class_method :cache_key
end
