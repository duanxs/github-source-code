# rubocop:disable Style/FrozenStringLiteralComment

# RepositoryForker is responsible for creating Repository forks
class RepositoryForker
  include GitHub::CacheLock

  # Create the RepositoryForker instance
  def initialize(repo, options = {})
    @repo = repo
    @options = options
  end

  # Public: Fork this repository
  #
  # options - Hash options that determine fork behavior. Default: {}
  #   :forker - The User performing the fork.
  #   :owner  - Deprecated alias of `:forker`.
  #   :org    - Optional. The Organization who should own the fork.
  #
  # Returns [false, Symbol reason, errors] if the fork cannot be created.
  # Returns [Repository fork, Symbol reason] if it worked.
  def fork
    forker = options[:forker]
    new_owner = forker

    # actually, formally deprecate this argument
    if !forker && options[:owner]
      new_owner = forker = options[:owner]
    end

    if org = options[:org]
      if !org.fork_allowed?(repo: repo, user: forker)
        return [false, :org_private]
      elsif !repo.internal? && org.can_create_repository?(forker, visibility: repo.visibility)
        new_owner = org
      else
        return [false, :permission]
      end
    end

    return [false, :plan] if GitHub.billing_enabled? && repo.private? && new_owner.organization? && new_owner.free_plan?

    if repo.forking_disabled?
      return [false, :disabled]
    end

    if GitHub.flipper[:disallow_nested_private_forks].enabled?(forker) && repo.private_fork?
      return [false, :private_fork]
    end

    if existing_repo = repo.find_fork_in_network_for_user(new_owner)
      unless existing_repo.active?
        # XXX repo is marked as deleted in DB but not cleaned up yet. can't
        # fork. not much we can do about this.
        fail "can't refork deleted repo"
      end
      return [existing_repo, :exists]
    end

    if !forker.can_fork?(repo, new_owner)
      return [false, :account]
    end

    if repo.locked?
      return [false, :locked]
    end

    # what attributes are we copying?
    default_attrs = repo.attributes.slice(*Repository::FIELDS_COPIED_ON_FORK)

    # strip out control characters from description, which are invalid
    # See https://github.com/github/github/issues/42763
    default_attrs["description"] = default_attrs["description"].gsub(/[[:cntrl:]]/, "") if default_attrs["description"]

    # what attributes are unique to the fork
    new_attributes = {
      parent: repo,
      has_issues: false,
      name: Platform::Loaders::NewForkName.load(new_owner, repo.name).sync,
      created_by_user_id: forker.id,
      wiki_access_to_pushers: true,
    }

    # make the fork by merging default + new attrs. perform the actual
    # create with a cache lock to ensure the repo isn't forked twice at the same
    # time by the same user.
    #
    # XXX: This should be a unique constraint on repos (owner_id, source_id)
    # but we need to clean up all existing dupe forks in order to add it.
    new_repo =
      cache_lock("repo-fork-lock:#{new_owner.id}:#{repo.network_id}", 1.minute) do
        GitHub::SchemaDomain.allowing_cross_domain_transactions do
          new_owner.repositories.create(default_attrs.merge(new_attributes))
        end
      end
    return [false, :forking] if new_repo.nil?
    return [false, :invalid, new_repo.errors] if new_repo.new_record?

    if repo.fork_inherits_teams?(new_repo)
      # The parent's owner is an org and the new owner is a member of
      # that org - add the new repo to the same teams the parent is
      # on.
      new_repo.add_teams_of(repo)
    elsif org && forker
      # We're forking the new repo into an org
      if !org.adminable_by?(forker)
        new_repo.add_member(forker, action: :admin)
      end
    elsif !repo.in_organization? && repo.private?
      # All collaborators on a private repo should be added to private
      # forks.
      new_repo.copy_permissions_of(repo)
    end

    if (l = repo.repository_license)
      RepositoryLicense.create(repository: new_repo, license_id: l.license_id)
    end

    instrument(parent: repo)
    [new_repo, :created]
  end

  def self.message_from_reason(reason, errors)
    case reason
    when :forking
      "Being forked, check now"
    when :exists
      "Forked, check now"
    when :invalid
      errors.full_messages.to_sentence
    when :account
      "You can't fork this repository at this time."
    when :permission
      "Don’t have permission to fork this repository"
    when :org_private
      "You cannot fork a private repository into an organization outside the repository's enterprise."
    when :plan
      "You can't fork a private repository into an organization on a free plan. Sorry about that!"
    when :private_fork
      "Cannot fork a private fork."
    end
  end

  private

  attr_reader :repo, :options

  def instrument(parent:)
    GitHub.dogstats.increment("repository", tags: ["action:internal-repo-fork"]) if parent.internal?
    GitHub.dogstats.increment("repository", tags: ["action:private-fork-fork"]) if parent.private_fork?
  end
end
