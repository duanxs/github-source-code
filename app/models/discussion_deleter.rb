# frozen_string_literal: true

# Public: Delete discussions by creating a new DeletedDiscussion record before
# destroying the Discussion record.
class DiscussionDeleter
  attr_reader :discussion

  def initialize(discussion)
    @discussion = discussion
  end

  # Public: Mark the discussion as deleted by the specified user.
  #
  # actor - the User who is doing the deleting
  #
  # Returns a Boolean indicating success.
  def delete(actor)
    DeletedDiscussion.transaction do
      deleted_discussion = DeletedDiscussion.create(
        repository: discussion.repository,
        deleted_by: actor,
        number: discussion.number,
        old_discussion_id: discussion.id
      )

      # Set transient 'actor' on the Discussion so that when we destroy it,
      # Hydro and audit log events see the user who did the deletion:
      discussion.actor = actor

      success = deleted_discussion.persisted? && discussion.destroy
      raise ActiveRecord::Rollback unless success
      success
    end
  end
end
