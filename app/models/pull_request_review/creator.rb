# frozen_string_literal: true

class PullRequestReview
  class Creator

    class Result
      attr_accessor :errors
      attr_reader :review

      def initialize(success:, review:, errors: [])
        @success = success
        @review = review
        @errors = errors
      end

      def success?
        @success
      end
    end

    def self.execute(pull_request:, user:, body: nil, comments: [], event: nil, pull_comparison: nil)
      new(
        pull_request: pull_request,
        user: user,
        body: body,
        comments: comments,
        event: event,
        pull_comparison: pull_comparison || PullRequest::Comparison.find(
          pull: pull_request,
          start_commit_oid: pull_request.merge_base,
          end_commit_oid: pull_request.head_sha,
          base_commit_oid: pull_request.merge_base,
        ),
      ).execute
    end

    def initialize(pull_request:, user:, body:, comments:, event:, pull_comparison:)
      @pull_request = pull_request
      @user = user
      @body = body
      @comments = comments
      @event = event
      @pull_comparison = pull_comparison
    end

    attr_reader :pull_request, :user, :body, :comments, :event, :pull_comparison

    def execute
      review = pull_request.reviews.build(
        body: body,
        user: user,
        head_sha: pull_comparison.end_commit.oid,
      )
      errors = []

      PullRequestReview.transaction do
        unless review.save
          errors.concat(review.errors.full_messages)
          raise ActiveRecord::Rollback
        end

        comments.each do |draft_comment|
          comment = nil

          if parent_id = draft_comment[:in_reply_to]
            parent = pull_request.review_comments.find(parent_id)
            thread = parent.pull_request_review_thread

            comment = thread.build_reply(
              pull_request_review: review,
              user: user,
              body: draft_comment[:body],
            )
          else
            if draft_comment[:line].present?
              thread = review.build_thread
              comment = thread.build_first_comment(
                user: user,
                diff: pull_comparison.diffs.only_params,
                body: draft_comment[:body],
                path: draft_comment[:path],
                line: draft_comment[:line],
                side: draft_comment[:side]&.downcase&.to_sym || :right,
                start_line: draft_comment[:start_line],
                start_side: draft_comment[:start_side]&.downcase&.to_sym || :right,
              )
            else
              thread, comment = review.build_thread_with_comment(
                user: user,
                body: draft_comment[:body],
                diff: pull_comparison.diffs.only_params,
                position: draft_comment[:position],
                path: draft_comment[:path],
              )
            end
          end

          unless comment.save
            errors.concat(comment.errors.full_messages)
            raise ActiveRecord::Rollback
          end

          unless thread.save
            errors.concat(thread.errors.full_messages)
            raise ActiveRecord::Rollback
          end
        end

        if event
          unless review.trigger(event)
            errors << review.halted_because if review.halted?
            raise ActiveRecord::Rollback
          end
        end
      end

      if errors.empty?
        Result.new(success: true, review: review)
      else
        Result.new(success: false, review: review, errors: errors)
      end
    end
  end
end
