# frozen_string_literal: true

class PullRequestReview < ApplicationRecord::Domain::Repositories
  class Decision
    attr_reader :ref_update, :policy_fulfilled, :reason, :instrumentation_payload

    def initialize(ref_update, policy_fulfilled, reason:, instrumentation_payload: {})
      @ref_update = ref_update
      @policy_fulfilled = policy_fulfilled
      @reason = ProtectedBranchPolicy::Reason.new(**reason)
      @instrumentation_payload = instrumentation_payload
    end

    # Public: Create a successful Decision for PullRequestReview
    def self.success(ref_update, reason:, instrumentation_payload: nil)
      new(ref_update, true, reason: reason, instrumentation_payload: instrumentation_payload)
    end

    # Return all the enforced PullRequestReview involved in determining this Decision.
    #
    # Returns Array of PullRequestReview
    def reviews
      return @reviews if defined?(@reviews)
      @reviews = PullRequestReview.find(instrumentation_payload[:review_ids])
    end

    alias_method :policy_fulfilled?, :policy_fulfilled

    def more_reviews_required?
      !!instrumentation_payload[:approving_reviews_required] || code_owner_review_required? || soc2_approval_process_required?
    end

    def code_owner_review_required?
      !!instrumentation_payload[:code_owner_review_required]
    end

    def soc2_approval_process_required?
      !!instrumentation_payload[:soc2_approval_process_required]
    end

    def has_reviews?
      !!instrumentation_payload[:review_ids]&.any?
    end

    def has_approving_reviews?
      instrumentation_payload[:approving_reviews_count].to_i > 0
    end

    def approved?
      case reason.code
      when :review_policy_not_required
        has_approving_reviews?
      when :review_policy_not_satisfied
        false
      when :review_approved
        true
      end
    end

    def changes_requested?
      !!instrumentation_payload[:has_requested_changes]
    end
  end
end
