# rubocop:disable Style/FrozenStringLiteralComment

class PullRequestReview < ApplicationRecord::Domain::Repositories
  class Policy
    include MethodTiming

    SOC2_REPOS = %w(github/github).freeze
    SOC2_REVIEWERS_TEAM_PATTERN = /-reviewers\z/.freeze

    # Public: Check the pull request review policy for one or more ref updates
    # for protected branches
    #
    # repository                    - The Repository, Gist, or Unsullied::Wiki whose refs
    #                                 are being updated
    # ref_updates                   - Array of Git::Ref::Updates
    # protected_branches_by_refname - Hash with qualified ref names as keys and relevant `ProtectedBranch`es as values
    #                                 (Passed for performance reasons to avoid duplicate lookups)
    # actor                         - The User attempting to make an update
    #
    # Returns a list of Decisions
    def self.check(repository, ref_updates, protected_branches_by_refname, actor:)
      new(repository, ref_updates, protected_branches_by_refname, actor: actor).check
    end

    # Public: Check the pull request review policy for a single Pull Request.
    #
    # pull_request: PullRequest to check the policy for
    # actor: the User attempting to make an update
    #
    # Returns a single Decision.
    def self.check_pull_request(pull_request:, actor:, ref_update:)
      new(pull_request.repository, [ref_update], { ref_update.refname => pull_request.protected_base_branch }, actor: actor, pull_request: pull_request).check_pull_request
    end

    def initialize(repository, ref_updates, protected_branches_by_refname, actor:, pull_request: nil)
      @repository = repository
      @ref_updates = ref_updates
      @protected_branches_by_refname = protected_branches_by_refname
      @actor = actor
      @pull_request = pull_request
    end

    # Check the policy for a single Pull Request, and return a Decision.
    #
    # For looking up reviews, all pull requests are considered (open and closed ones).
    #
    # Returns a single Decision
    def check_pull_request
      check_commit(ref_updates.first,
                   commit_oid: pull_request.head_sha,
                   consider_open_pulls_only: pull_request.open?)
    end

    # Check the review Policy for all the ref updates.
    #
    # For looking up reviews, only open pull requests are considered.
    #
    # Returns an Array of Decision records
    def check
      ref_updates.map do |ref_update|
        policy_commit = policy_commit_for(ref_update)
        decision = check_commit(ref_update, commit_oid: policy_commit.oid)

        if policy_required_and_fulfilled?(decision)
          decision
        elsif relevant_merge_commit?(ref_update, policy_commit)
          decision_for_merge_commit(ref_update, policy_commit)
        else
          decision
        end
      end
    end

    time_method :check, key: "pull_request_review.policy.check"

    private

    attr_reader :repository, :ref_updates, :protected_branches_by_refname, :actor, :pull_request

    def decision_for_merge_commit(ref_update, policy_commit)
      relevant_parents = policy_commit.parent_oids - [ref_update.before_oid]
      parent_decisions = relevant_parents.map do |parent_oid|
        check_commit(ref_update, commit_oid: parent_oid)
      end

      # TODO create a combined Decision
      if parent_decisions.all?(&:policy_fulfilled?)
        parent_decisions.first
      else
        parent_decisions.find { |decision| !decision.policy_fulfilled? }
      end
    end

    def check_commit(ref_update, commit_oid:, consider_open_pulls_only: true)
      reviews = reviews_for(commit_oid, ref_update, consider_open_pulls_only)
      pull_authors_block = pull_request_authors_block_for(commit_oid, consider_open_pulls_only)

      # Because of a potential race-condition related to updating the PRs `head_sha`
      # in the database from a post receive job, we need to make sure that we include
      # the reviews of the PR at hand.
      if pull_request
        pull_request.latest_enforced_reviews(writers_only: true).each do |review|
          reviews << review unless reviews.include?(review)
        end
      end

      process_policy(ref_update, reviews, pull_authors_block)
    end

    def reviews_for(commit_oid, ref_update, consider_open_pulls_only)
      PullRequestReview::EnforcedLoader.new(
        repository: repository,
        pull_request_head_sha: commit_oid,
        pull_request: pull_request,
        open_pulls_only: consider_open_pulls_only,
        writers_only: true,
        base_ref_name: ref_update.refname,
      ).execute
    end

    def pull_request_authors_block_for(commit_oid, consider_open_pulls_only)
      -> do
        pr_scope = repository.pull_requests.where(head_sha: commit_oid)
        pr_scope = pr_scope.open_pulls if consider_open_pulls_only
        user_ids = pr_scope.group(:user_id).pluck(:user_id)

        User.where(id: user_ids)
      end
    end

    def process_policy(ref_update, reviews, pull_authors_block)
      protected_branch = find_protected_branch(ref_update)

      if protected_branch && protected_branch.pull_request_reviews_required?
        check_reviews(ref_update, reviews, pull_authors_block, protected_branch.required_approving_review_count)
      else
        check_reviews_non_protected_branch(ref_update, reviews)
      end
    end

    def policy_required_and_fulfilled?(decision)
      decision.policy_fulfilled? &&
        decision.reason.code != :review_policy_not_required
    end

    def relevant_merge_commit?(ref_update, policy_commit)
      policy_commit.merge_commit? &&
        policy_commit.parent_oids.include?(ref_update.before_oid)
    end

    # Private: Returns a Hash of the default data we want for the instrumentation payload
    # for a single Decision
    def default_instrumentation_payload(reviews)
      { review_ids: reviews.map(&:id) }
    end

    # Check the state of reviews for a ref_update that lives on a non protected branch.
    #
    # Returns a Decision
    def check_reviews_non_protected_branch(ref_update, reviews)
      summary, message = nil, nil
      payload = default_instrumentation_payload(reviews)

      if reviews.any?(&:changes_requested?)
        summary = "Changes requested"
        message = build_message(reviews)
        payload[:has_requested_changes] = true
      elsif reviews.any?(&:approved?)
        summary = "Changes approved"
        message = build_message(reviews)
      end

      payload[:approving_reviews_count] = reviews.count(&:approved?)

      Decision.success(ref_update,
        reason: {
          code: :review_policy_not_required,
          summary: summary,
          message: message,
        },
        instrumentation_payload: payload,
      )
    end

    # Private: Check policy for an Array of reviews for a ref_update
    #
    # Returns a Decision
    def check_reviews(ref_update, reviews, pull_authors_block, required_approving_review_count)
      reason = :review_policy_not_satisfied
      policy_fulfilled = false
      summary, message = nil, nil
      payload = default_instrumentation_payload(reviews)

      begin
        case
        when reviews.any?(&:changes_requested?)
          summary = "Changes requested"
          message = build_message(reviews) + " by reviewers with write access."
          payload[:has_requested_changes] = true
        when (waiting_on = code_owners_awaiting_review(ref_update, reviews, pull_authors_block)).any?
          summary = "Code owner review required"
          message = awaiting_code_owners_message(waiting_on)
          payload[:code_owner_review_required] = true
        when soc2_reason = awaiting_soc2_approval_process(reviews)
          summary = "Review from compliance team required"
          message = soc2_reason
          payload[:soc2_approval_process_required] = true
        when reviews.count(&:approved?) >= required_approving_review_count
          policy_fulfilled = true
          reason = :review_approved
          summary = "Changes approved"
          message = build_message(reviews) + " by reviewers with write access."
        else
          summary = "Review required"
          message = "At least #{required_approving_review_count} #{"approving review".pluralize(required_approving_review_count)} #{"is".pluralize(required_approving_review_count)} required by reviewers with write access."
          payload[:approving_reviews_required] = true
          payload[:approving_reviews_count] = reviews.count(&:approved?)
        end
      rescue PullRequest::DetermineCodeownersError
        summary = "Code owner review required"
        message = "Could not determine code owners from the current diff."
        payload[:code_owner_review_required] = true
      end

      Decision.new(ref_update, policy_fulfilled,
        reason: {
          code: reason,
          summary: summary,
          message: message,
        },
        instrumentation_payload: payload
      )
    end

    def soc2_approval_process_required?
      !GitHub.enterprise? && SOC2_REPOS.include?(repository.name_with_owner)
    end

    # Private: Check if this policy fails due to Soc 2 review policy compliance not being fulfilled.
    #
    # Soc 2 review process requires that an review is requested to a team ending with '-reviewers' and
    # that at least one of those requests has been fulfilled via an approval review.
    #
    # reviews - An Array of PullRequestReviews that are considered for this policy check.
    #
    # Returns a reason String why Soc 2 compliance is not fulfilled or nil if it is fulfilled.
    def awaiting_soc2_approval_process(reviews)
      return unless soc2_approval_process_required?

      if pull_request
        soc2_check_for_pull(pull_request, reviews)
      else
        soc2_check_for_ref_update(reviews)
      end
    end

    def soc2_check_for_pull(pull_request, reviews)
      requests = soc2_review_requests(pull_request)

      if requests.none?
        "Waiting on review request to a compliance team (i.e. '@github/*-reviewers')"
      elsif filter_requests_fulfilled_by(requests, reviews).none?
        compliance_teams = requests.map { |req| req.reviewer.to_s }
        compliance_teams = compliance_teams.uniq.to_sentence \
          two_words_connector: " or ",
          last_word_connector: ", or "
        "Waiting on approval from at least one compliance team: #{compliance_teams}"
      end
    end

    def soc2_check_for_ref_update(reviews)
      fulfilled_requests = reviews.flat_map do |review|
        review.review_requests.includes(:reviewer)
      end.uniq
      fulfilled_soc2_requests = filter_soc2_review_requests(fulfilled_requests)

      if fulfilled_soc2_requests.none?
        "Waiting on review request to and subsequent approval from a compliance team (i.e. '@github/*-reviewers')"
      end
    end

    # Private: Finds any review requests for this PR which are requested for review compliance teams.
    #
    # Returns an Array of ReviewRequests.
    def soc2_review_requests(pull_request)
      review_requests = pull_request.review_requests.not_dismissed.includes(:reviewer, :pull_request_reviews)
      filter_soc2_review_requests(review_requests)
    end

    def filter_soc2_review_requests(review_requests)
      review_requests.select do |request|
        request.reviewer.is_a?(Team) && request.reviewer.slug =~ SOC2_REVIEWERS_TEAM_PATTERN
      end
    end

    def filter_requests_fulfilled_by(requests, reviews)
      requests.select { |request| (request.pull_request_reviews & reviews).any? }
    end

    def code_owners_awaiting_review(ref_update, reviews, pull_authors_block)
      protected_branch = find_protected_branch(ref_update)
      return [] unless protected_branch&.require_code_owner_review?

      GitHub.dogstats.time("protected_branches.pull_request_review.policy.find_code_owners_awaiting_review") do
        find_code_owners_awaiting_review(ref_update, reviews, pull_authors_block)
      end
    end

    def find_code_owners_awaiting_review(ref_update, reviews, pull_authors_block)
      reviewer_ids = reviews.map(&:user_id)
      codeowners = if pull_request
        pull_request.codeowners!
      else
        Repository::Codeowners.new(repository, ref: ref_update.refname,  paths: ref_update.paths)
      end

      pull_request_authors = if pull_request
        [pull_request.user]
      else
        pull_authors_block.call
      end

      codeowners.owners_by_rule.each_with_object(Set.new) do |(rule, owners), awaiting|
        # Don't block if the PR author is a required code owner. They aren't
        # allowed to review their own PR and being the author fulfills the
        # spirit of enforced code owners.
        owners -= pull_request_authors

        awaiting.merge(owners) unless code_owner_reviewed?(owners, reviewer_ids)
      end
    end

    def code_owner_reviewed?(owners, reviewer_ids)
      teams, users = owners.partition { |o| o.is_a?(Team) }
      return true if (reviewer_ids & users.map(&:id)).any?

      team_member_ids = Team.members_of(teams.map(&:id), immediate_only: false).pluck(:id)
      (reviewer_ids & team_member_ids).any?
    end

    def awaiting_code_owners_message(waiting_on)
      phrase = waiting_on.to_a.to_sentence \
        two_words_connector: " and/or ",
        last_word_connector: ", and/or "

      "Waiting on code owner review from #{phrase}."
    end

    def build_message(reviews)
      messages = []
      rejected_reviews_count = reviews.count(&:changes_requested?)
      approving_reviews_count = reviews.count(&:approved?)

      if rejected_reviews_count > 0
        messages << "#{rejected_reviews_count} #{"review".pluralize(rejected_reviews_count)} requesting changes"
      end

      if approving_reviews_count > 0
        messages << "#{approving_reviews_count} #{"approving review".pluralize(approving_reviews_count)}"
      end

      messages.join(" and ")
    end

    def policy_commit_for(ref_update)
      if ref_update.respond_to?(:policy_commit)
        ref_update.policy_commit
      else
        ref_update.after_commit
      end
    end

    # Private: Find a protected branch for a ref_update
    def find_protected_branch(ref_update)
      protected_branches_by_refname[ref_update.refname]
    end

    def required_review_requests?
      pull_request&.review_requests&.pending&.any? { |request| request.required? }
    end
  end
end
