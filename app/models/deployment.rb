# rubocop:disable Style/FrozenStringLiteralComment

class Deployment < ApplicationRecord::Domain::Repositories
  include Instrumentation::Model
  include GitHub::Relay::GlobalIdentification
  include GitHub::UTF8
  include GitHub::Validations

  include Permissions::Attributes::Wrapper
  self.permissions_wrapper_class = Permissions::Attributes::Deployment

  ABANDONMENT_THRESHOLD = 30.minutes

  after_commit :trigger_creation_events, on: :create
  after_commit :trigger_issue_deployed_events, on: :create
  after_commit :notify_socket_subscribers, on: :create

  before_create :set_latest_environment

  validates_presence_of :sha, :creator_id, :repository_id
  validate :validate_sha

  validates_length_of :task, within: 1..128
  validates :description, bytesize: { maximum: MYSQL_TEXT_FIELD_LIMIT }, unicode: true
  validates :payload, bytesize: { maximum: MYSQL_TEXT_FIELD_LIMIT }, unicode: true

  belongs_to :creator, class_name: "User"
  belongs_to :performed_via_integration,
    foreign_key: :performed_by_integration_id,
    class_name: "Integration"

  belongs_to :repository

  # belongs_to :latest_status,
  #   foreign_key: :latest_deployment_status_id,
  #   class_name: "DeploymentStatus"

  scope :by_sha, lambda { |sha| where(sha: sha) }

  has_many :statuses, -> { order("id DESC") },
                      dependent: :destroy,
                      class_name: "DeploymentStatus"

  has_many :workflow_runs, as: :trigger

  def payload
    utf8(read_attribute(:payload))
  end

  def description
    utf8(read_attribute(:description))
  end

  # Deprecated in favor of #current_state
  # Returns the state for the most recently created DeploymentStatus object
  #
  # Returns a String or nil
  def latest_state
    return latest_status.state if latest_status
    return "abandoned" if abandoned?
    "pending"
  end

  # Public: The current state of this deployment, based on the latest status,
  #   the deployment's settings, and the time.
  def state
    status_state = latest_status&.state

    if status_state.nil?
      return "abandoned" if created_at < ABANDONMENT_THRESHOLD.ago
      return "pending"
    end

    return "active" if status_state == "success"
    return "destroyed" if transient_environment? && status_state == "inactive"
    status_state
  end

  def active?
    state == "active"
  end

  # The latest status for this deployment
  #
  # Returns a Status or nil
  def latest_status
    @latest_status ||= statuses.first
  end

  # The current combined status state for the repo@ref
  #
  # Returns a new combined status object for the Deployment
  def combined_status
    @combined_status ||= CombinedStatus.new(repository, sha, statuses: Statuses::Service.current_for_shas(repository_id: repository.id, shas: sha), check_runs: CheckRun.latest_for_sha_and_event_in_repository(sha, repository.id))
  end

  # Return context/state pairs to surface which services are failing
  #
  # Returns an Array of Hashes, one for each unique context for the commit
  def combined_status_contexts
    combined_status.status_checks.map do |status|
      {context: status.context, state: status.state}
    end
  end

  # Return a list of unique context names
  #
  # This list of contexts is used to verify commit statuses when no required
  # contexts are specified at creation time.
  #
  # Returns an Array of Strings, the context names
  def unique_context_names
    combined_status.status_checks.map { |status| status.context }.uniq
  end

  # Return a list of context names that are currently in a "success" state
  #
  # Returns an Array of Strings, the successful context names
  def successful_context_names
    result = combined_status_contexts.map do |status|
      status[:context] if status[:state] == DeploymentStatus::SUCCESS
    end
    result.compact
  end

  # Generate a shortened sha for easier displaying
  #
  # Returns the first 8 characters of a sha
  def short_sha
    sha[0..7]
  end

  # Generate the payload as a hash for dumping to JSON
  #
  # Returns the @payload as parsed json or an empty hash on parse error
  def json_payload
    GitHub::JSON.decode(self.payload)
  rescue StandardError
    { }
  end

  # Get the specified ref or default branch for the repo
  #
  # Returns the ref specified at creation time or falls back to repo defaults.
  def ref
    @ref || attributes["ref"] || repository.default_branch
  end

  # Return whether or not we should bring this ref up to date with default_branch
  #
  # Returns true if a merge should be attempted, false otherwise
  def auto_merge?
    @auto_merge && behind_default_branch?
  end

  # Set the auto_merge attribute based on form input. Defaults to true
  #
  # Returns nothing
  def auto_merge=(value)
    @auto_merge = value.nil? ? true : !!value
  end

  # Determine if any required contexts are in a non "success" state
  #
  # Return - true if deployment should be aborted due to required context conflicts
  def failed_commit_statuses?
    required_contexts.any? && !valid_commit_statuses?
  end

  # Validated if the required contexts for deployment are valid.
  #
  # Check the incoming array of required contexts against the successful contexts for the commit.
  #
  # Returns: true if all commit status contexts are 'success', false otherwise
  def valid_commit_statuses?
    return true if required_contexts.empty?

    contexts = successful_context_names
    return false if contexts.empty?
    required_contexts.each do |context|
      return false unless contexts.include?(context)
    end
    true
  end

  # Set the required commit status contexts to validate before creation.
  #
  # Returns nothing
  def required_contexts=(value)
    @required_contexts = value.nil? ? unique_context_names : value
  end

  # An array of named contexts that must be in a "success" state
  #
  # Returns an Array of Strings for named commit statuses
  def required_contexts
    @required_contexts ||= unique_context_names
  end

  # Check if the requested deployment sha is behind the default branch.
  #
  # In the certain cases we want to ensure that the requested deployment
  # is caught up with the default branch, this tells us if we need to.
  #
  # Returns true if the sha is behind the default branch, false otherwise.
  def behind_default_branch?
    repository.comparison(repository.default_branch, sha).behind?
  end

  # Merge the default branch into the requested Deployment ref
  #
  # Returns true if successful, false otherwise.
  def merge_for(actor, options = { message: "Auto-Merging for deployment"})
    return false unless behind_default_branch?

    base_ref = repository.heads.find(ref)
    return false unless base_ref

    merge_commit, error, error_message = base_ref.merge(actor, repository.default_branch, options)
    merge_commit.present?
  end

  def pull_requests
    return @pull_requests if defined?(@pull_requests)

    # Make sure that the pull requests for the repo are up to date
    # so that they are queryable by their `head_sha` on create
    if self.new_record?
      PullRequest.synchronize_requests_for_ref(repository, ref, creator)
    end

    @pull_requests = PullRequest.where(
      repository_id: repository_id,
      head_sha: sha,
    )
  end

  # Check if the requested deployment ever got picked up by a service.
  #
  # In certain cases deployments can be requested that weren't ever handled
  # by a 3rd party. We detect this by the absence of any deployment status
  # relations on a deployment that's older than 30 minutes.
  #
  # Returns true if nothing will likely pick this up, false otherwise.
  def abandoned?
    statuses.empty? && created_at < 30.minutes.ago
  end

  # Public: Whether the given user can see this deployment.
  def readable_by?(actor)
    repository && repository.readable_by?(actor)
  end

  # Set all the previous successful Deployments to the same environment as this
  # Deployment as inactive by creating a new, inactive DeploymentStatus.
  def set_previous_environment_deployments_inactive!(include_production: false)
    latest_status = self.reload.statuses.first
    return unless latest_status&.succeeded?

    status_ids = DeploymentStatus.github_sql.run(<<-SQL, environment: latest_environment, repository_id: repository_id, id: id, success: DeploymentStatus::SUCCESS, include_production: include_production).results.flatten
      SELECT deployment_statuses.id
        FROM deployments
        JOIN deployment_statuses ON deployments.latest_deployment_status_id = deployment_statuses.id
      WHERE
        deployments.latest_status_state = :success
        AND deployments.latest_environment = :environment
        AND deployments.repository_id  = :repository_id
        AND (deployments.production_environment = 0 OR deployments.production_environment = :include_production)
        AND deployments.transient_environment = 0
        AND (deployments.id != :id)
    SQL

    statuses_to_update = DeploymentStatus.where(id: status_ids)

    statuses_to_update.each do |status|
      DeploymentStatus.create(
        state: "inactive",
        deployment_id: status.deployment_id,
        creator_id: status.creator_id,
        environment: latest_environment,
      )
    end
  end

  def dashboard_channel
    repository.deployments_dashboard_environment_channel(environment)
  end

  def self.for_pull_request(pull)
    Deployment.where(id: pull.events.where(event: "deployed").pluck(:deployment_id))
        .or(Deployment.where(repository_id: pull.repository.id, sha: pull.head_sha))
  end

  private

  def set_latest_environment
    self.latest_environment = environment
  end

  def event_payload
    # TODO: Normalize repository prefix to repo
    {
      event_prefix => self,
      :repository  => repository,
      :creator     => creator,
    }
  end

  def validate_sha
    if !GitRPC::Util.valid_full_sha1?(sha)
      errors.add(:sha, "must be a 40 character SHA1")
    end
  end

  def trigger_creation_events
    instrument :create
    GitHub.dogstats.increment("deployment", tags: ["action:create"])
  end

  def trigger_issue_deployed_events
    pull_requests.each do |pull_request|
      pull_request.issue.create_deployed_event(self)
    end
  end

  def notify_socket_subscribers
    pull_requests.each do |pull_request|
      pull_request.notify_socket_subscribers
    end

    GitHub::WebSocket.notify_deployment_channel(self, dashboard_channel, {
      wait: default_live_updates_wait,
    })
  end
end
