# frozen_string_literal: true

class DocusignEnvelope < ApplicationRecord::Collab
  belongs_to :owner, polymorphic: true, required: true

  scope :active, -> { where(active: true) }

  VOIDABLE_STATUSES = [:declined, :voided]

  enum status: {
    unsent: 0,
    created: 1,
    deleted: 2,
    sent: 3,
    delivered: 4,
    signed: 5,
    completed: 6,
    declined: 7,
    timedout: 8,
    voided: 9,
  }

  enum document_type: {
    w9: 0,
    w_8ben: 1,
    w_8ben_e: 2,
  }

  before_save :deactivate_if_voidable
  validates_uniqueness_of :active, scope: [:owner_id, :owner_type],
    message: "envelope already exists",
    if: :envelope_will_become_active

  def status=(value)
    super(value.downcase)
  end

  def voidable?
    sent? || delivered?
  end

  private

  def deactivate_if_voidable
    self.active = false if VOIDABLE_STATUSES.include?(status.to_sym)
  end

  def envelope_will_become_active
    # self.active? is true for a new record because of default column value
    self.active? || (will_save_change_to_active? && (saved_change_to_active? == true))
  end
end
