# rubocop:disable Style/FrozenStringLiteralComment

module User::NoticesDependency
  ORGANIZATION_NOTICES = {
    saml_sso_banner: "saml_sso_banner",
    enterprise_trial_warning_banner: "enterprise_trial_warning_banner",
    enterprise_trial_expired_banner: "enterprise_trial_expired_banner",
    add_successor_prompt: "add_successor_prompt",
    new_organization_survey: "new_organization_survey",
  }.freeze

  REPOSITORY_NOTICES = %w(cta_marketplace_ci first_time_contributor_issues_banner
    first_time_contributor_pull_requests_banner marketplace_project_management_issues_banner popular_repo_successor_prompt).freeze

  # Figures out which notices to show on the user's dashboard.
  #
  # Returns an Array of notice names as Strings.
  def notices_for_dashboard
    return @notices_for_dashboard unless @notices_for_dashboard.nil?
    @notices_for_dashboard =
      DashboardNoticesStore.get_for_user_id(self.id)
        .rescue { |result| report_failure(result, :notices_for_dashboard, nil) }
        .value { [] }
        .map { |notice| notice.name }
  end

  # Activates a notice for the user. Safe to call multiple times with
  # the same value. If the user has dismissed the notice, it will not show up
  # again.
  #
  # name - String or symbol corresponding to the views/notices partial to show.
  #
  # Returns nothing.
  def activate_notice(name)
    return if name.blank?
    name = name.to_s
    @notices_for_dashboard = nil
    DashboardNoticesStore.add(self.id, name)
      .rescue { |result| report_failure(result, :activate_notice, name) }
  end

  # Forcefully activates a notice even if the user has dismissed it in the
  # past.
  #
  # name - String or symbol corresponding to the views/notices partial to hide.
  #
  # Returns nothing.
  def activate_notice!(name)
    delete_notice(name)
    activate_notice(name)
  end

  # Deactivates (hides) a notice for the user. This notice will never show up
  # again. If you want to re-use the notice, you should use delete_notice so
  # subsequent activate_notice calls work as expected.
  #
  # name - String or symbol corresponding to the views/notices partial to hide.
  #
  # Returns nothing.
  def deactivate_notice(name)
    return if name.blank?
    name = name.to_s
    @notices_for_dashboard = nil
    DashboardNoticesStore.deactivate(self.id, name)
      .rescue { |result| report_failure(result, :deactivate_notice, name) }
  end

  # Deletes a notice for the user. If activate_notice is called after this
  # notice, the notice will show up again even if the user has dismissed it.
  #
  # name - String or symbol corresponding to the views/notices partial to hide.
  #
  # Returns nothing.
  def delete_notice(name)
    return false if name.blank?
    name = name.to_s
    @notices_for_dashboard = nil
    DashboardNoticesStore.delete(self.id, name)
      .rescue { |result| report_failure(result, :delete_notice, name) }
  end

  # Deletes all notices for the user.
  #
  # Returns nothing.
  def delete_notices
    @notices_for_dashboard = nil
    DashboardNoticesStore.delete_all_for_user(self.id)
  end

  # Set User Key Value for Notice
  #
  # notice - key to be added
  def dismiss_notice(notice_name)
    return unless notice = get_notice(notice_name)

    if !dismissed_notice?(notice_name)
      GitHub.kv.set("user.dismissed_notice.#{notice.name}.#{id}", Time.now.utc.iso8601)
    end
  end

  # Delete User Key Value for Notice
  #
  # notice - key to be deleted
  def reset_notice(notice)
    GitHub.kv.del("user.dismissed_notice.#{notice}.#{id}")
  end

  # Get User Key Value for Notice
  #
  # notice - key to check for in key values
  #
  # Returns true or false
  def dismissed_notice?(notice_name)
    return unless notice = get_notice(notice_name)
    return true if notice.hide_from_new_user? && created_at > notice.effective_date
    ActiveRecord::Base.connected_to(role: :reading) do
      GitHub.kv.get("user.dismissed_notice.#{notice.name}.#{id}").value!.present?
    end
  end

  # The time in UTC that the notice was dismissed at. Nil if no time was set.
  #
  # Returns: Time or nil
  def notice_dismissed_at(notice_name)
    return nil unless notice = get_notice(notice_name)
    return nil if notice.hide_from_new_user? && created_at > notice.effective_date

    value = ActiveRecord::Base.connected_to(role: :reading) do
      GitHub.kv.get("user.dismissed_notice.#{notice.name}.#{id}").value!
    end
    return nil if value.nil?

    begin
      Time.iso8601(value)
    rescue ArgumentError
      nil
    end
  end

  # Delete User key value for a repository notice
  #
  # notice - key to be deleted
  # repository_id - the ID of the Repository for which the notice was made
  #
  # Returns nothing
  def reset_repository_notice(notice, repository_id:)
    GitHub.kv.del("user.dismissed_repository_notice.#{notice}.#{repository_id}.#{id}")
  end

  # Get User Key Value for Org Notice
  #
  # notice  - key to check for in key values
  # org     - an Organization object
  #
  # Returns a Boolean.
  def dismissed_organization_notice?(notice, org)
    ActiveRecord::Base.connected_to(role: :reading) do
      GitHub.kv.get("user.dismissed_organization_notice.#{notice}.#{org.id}.#{id}").value! == notice
    end
  end

  # Set User Key Value for Org Notice
  #
  # notice - key to be added
  # org - an Organization object
  def dismiss_organization_notice(notice, org)
    if ORGANIZATION_NOTICES.include?(notice.to_sym) && !dismissed_organization_notice?(notice, org)
      GitHub.kv.set("user.dismissed_organization_notice.#{notice}.#{org.id}.#{id}", notice)
    end
  end

  # Get User Key Value for a repository notice
  #
  # notice - key to check for in key values
  # repository_id - a Repository database ID
  #
  # Returns a Boolean.
  def dismissed_repository_notice?(notice, repository_id:)
    ActiveRecord::Base.connected_to(role: :reading) do
      key = "user.dismissed_repository_notice.#{notice}.#{repository_id}.#{id}"
      GitHub.kv.get(key).value! == notice
    end
  end

  # Set User Key Value for repository notice
  #
  # notice - key to be added
  # repository_id - a Repository database ID
  def dismiss_repository_notice(notice, repository_id:)
    if REPOSITORY_NOTICES.include?(notice)
      if !dismissed_repository_notice?(notice, repository_id: repository_id)
        GitHub.kv.set("user.dismissed_repository_notice.#{notice}.#{repository_id}.#{id}", notice)
      end
    end
  end

  private

  # Report the error and repackage the error back into a Result
  #
  # error - the error to report_failure
  #
  # Returns a new error result.
  def report_failure(error, operation, notice_name)
    Failbot.report(error, operation: operation, notice_name: notice_name, app: "github-user")
    GitHub::Result.error error
  end

  def get_notice(notice_name)
    if notice = UserNotice.find(notice_name)
      notice
    else
      report_failure(ArgumentError.new("unregistered notice"), caller_locations.first.label, notice_name)
      nil
    end
  end

end
