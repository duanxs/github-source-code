# rubocop:disable Style/FrozenStringLiteralComment

module User::DormancyDependency
  # This dependency implements various checks for account activity.
  # This "active" is used on github.com to help determine if an account name can
  # be released when a user requests one that has been taken.  On Enterprise it
  # is used to release paid seats for other users.
  #
  # There are different rules and timeframes used for dotcom and enterprise.
  # These are handled using the `GitHub.dormancy_threshold` setting, which is
  # passed to these functions.  You will find various `GitHub.enterprise?`logic
  # within these methods as well.
  #
  # The rules used for dotcom can be found at
  # https://githubber.com/article/crafts/support/accounts/releasing-dormant-usernames

  ###########################
  ### NOTE TO MAINTAINERS ###
  ###########################
  # Logic for Organizations is different in some spots, if you are updating this
  # file do not forget to also update the org-specific overrides in
  # app/models/organization/dormancy_dependency.rb

  # Public: Check whether user fulfills dormancy guidelines
  #
  # Returns Boolean
  def dormant?(threshold = GitHub.dormancy_threshold)
    return @dormant if defined?(@dormant)
    @dormant = !exempt_from_dormancy?(threshold) && !recently_active?(threshold)
  end

  # Public: Check if this account meets any conditions that would exempt it from
  # being flagged dormant regardless of activity.  For example, a user that has
  # no activity, but is paying us, would not be considered dormant
  #
  # threshold - a time frame (ex `6.months`) to check for activity within
  #
  # Returns a Boolean
  def exempt_from_dormancy?(threshold = GitHub.dormancy_threshold)
    # On Enterprise there are currently no rules for User exemption
    return false if GitHub.enterprise?

    # Giving us money is the simplest way to earn exemption
    return true if GitHub.billing_enabled? && plan.paid? && !disabled?

    # If a user has enabled 2FA, consider them exempt
    return true if two_factor_authentication_enabled?

    # On dotcom, being a member of any org earns an exemption
    return true if organizations.any?

    # If user has created any repositories (non-fork), consider them exempt
    return true if repositories.network_roots.count > 0

    # If any repos are active (have recent pushes), then the owner user is
    # active too.  We check this to ensure we don't disrupt an active repo that
    # has an otherwise dormant owner.
    return true if repositories.any? { |repo| !repo.dormant?(threshold) }

    # Wow, not paying and not part of an org?  OK well let's see what we have...
    return true if oauth_accesses.personal_tokens.any?
    return true if oauth_accesses.third_party.any?
    return true if public_keys.any?
    return true if member_repositories.any?

    false
  end

  def active_organizations(threshold = GitHub.dormancy_threshold)
    organizations.reject { |org| org.dormant?(threshold) }
  end

  def active_repositories(threshold = GitHub.dormancy_threshold)
    repositories.reject { |repo| repo.dormant?(threshold) }
  end

  # Public: Check if this account has any recent activity.  For example, a user
  # that has recently generated a dashboard event like a push or a star would be
  # considered "active"
  #
  # threshold - a time frame (ex `6.months`) to check for activity within
  #
  # Returns a Boolean
  def recently_active?(threshold = GitHub.dormancy_threshold)
    cutoff = Time.now - threshold

    # Creating an account is activity
    return true if created_at >= cutoff

    # Paying us (or attempting to) is activity
    last_bill = last_billing_transaction_time
    return true if last_bill && last_bill >= cutoff

    # Check for a recent session
    last_session = last_web_session_time
    return true if last_session && last_session >= cutoff

    # Starring a repo is activity
    last_star = last_repo_star_time
    return true if last_star && last_star >= cutoff

    # Watching a repo is activity if auto-watch is disabled
    unless GitHub.newsies.settings(self).auto_subscribe?
      last_watch = last_repo_watch_time
      return true if last_watch && last_watch >= cutoff
    end

    # Check for dashboard events, the most fundamental indicator of activity
    last_event = last_stratocaster_event_at
    return true if last_event && last_event >= cutoff

    # Finally, check the audit log for any non-failure events
    last_audit_event = last_audit_log_entry_time
    return true if last_audit_event && last_audit_event >= cutoff

    # On GitHub Enterprise Server we also want to check for the last access
    # of personal access tokens and public keys to avoid counting service accounts
    # or read-only API (business) users as dormant.
    # Note that we exempt all users with personal access tokens and public keys on dotcom,
    # but not on GHES, see the "exempt_from_dormancy?" method
    if GitHub.enterprise?
      # Check for the last time a personal access token was used
      last_personal_token_access = last_personal_token_access_time
      return true if last_personal_token_access && last_personal_token_access >= cutoff

      # Check for the last time a public key was used
      last_public_key_access = last_public_key_access_time
      return true if last_public_key_access && last_public_key_access >= cutoff
    end

    false
  end

  def last_billing_transaction_time
    if GitHub.billing_enabled? && (txn = billing_transactions.last)
      txn.created_at
    end
  end

  def last_web_session_time
    if session = most_recent_session
      session.accessed_at
    end
  end

  def last_repo_star_time
    if last_star = stars.last
      last_star.created_at
    end
  end

  def last_repo_watch_time
    response = GitHub.newsies.subscriptions(self, set: :all, page: 1, per_page: 1, sort: :desc, list_type: "Repository")
    if response.success?
      subscriptions = response.value
      if subscriptions.present?
        subscriptions.first.created_at
      end
    end
  end

  def last_audit_log_entry_time
    options = {
      actor_id: id,
      allowlist: AuditLogEntry.user_dormancy_action_names,
      raw: true,
    }
    query = Audit::Driftwood::Query.new_dormant_user_query(options)
    hits = begin
      query.execute
    rescue
      Search::Results.empty
    end

    if hits.any?
      hit = hits.first
      Time.at(hit["created_at"] / 1000)
    end
  end

  def signup_timeframe
    return "unknown" unless created_at

    if created_at > 1.day.ago
      "onboarding"
    elsif created_at > 30.days.ago
      "recent"
    else
      "established"
    end
  end

  def last_personal_token_access_time
    oauth_accesses.personal_tokens.order(accessed_at: :desc).pluck(:accessed_at).first
  end

  def last_public_key_access_time
    public_keys.order(accessed_at: :desc).pluck(:accessed_at).first
  end
end
