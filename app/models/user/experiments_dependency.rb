# frozen_string_literal: true

module User::ExperimentsDependency
  # Checks if a user has been previously assigned to an experiment.
  def experiment_variant(experiment_slug)
    GitHub::UserResearch.experiment_variant({
      experiment: experiment_slug,
      subject: self,
    })
  end

  # Checks if a user is enrolled in the experiment's test group,
  # also checking their qualification and enrolling them if qualified
  # if they haven't already been assigned a group.
  def in_experiment_test_group?(experiment_slug)
    GitHub::UserResearch.in_experiment_test_group?(
      experiment: experiment_slug,
      subject: self,
    )
  end

  def last_login_from_country?(country_code)
    location = GitHub::Location.look_up(self.last_ip)
    location[:country_code] == country_code if location
  end
end
