# frozen_string_literal: true

module User::SponsorsDependency
  extend ActiveSupport::Concern

  SPONSORS_ACTIVITY_BANNER = "sponsors_activity_banner"
  SPONSORS_GOALS_BANNER = "sponsors_goals_banner"
  PREVIEW_SPONSORS_PROFILE_BANNER = "preview_sponsors_profile"

  included do
    has_many :sponsors_activities, as: :sponsorable, dependent: :destroy
    has_many :sponsors_activity_metrics, as: :sponsorable
    has_many :sponsors_activities_as_sponsor, class_name: "SponsorsActivity", foreign_key: :sponsor_id, dependent: :nullify

    has_many :sponsorships_as_sponsor,
      class_name: "Sponsorship",
      foreign_key: :sponsor_id,
      dependent: :destroy do
        def ranked(for_user:)
          ranked_by_sponsorable(for_user: for_user)
        end
      end

    has_many :sponsorships_as_sponsorable,
      class_name: "Sponsorship",
      foreign_key: :sponsorable_id do
        def ranked(for_user:)
          ranked_by_sponsor(for_user: for_user)
        end
      end

    has_many :sponsorship_match_bans_as_sponsorable,
      class_name: "SponsorshipMatchBan",
      as: :sponsorable,
      dependent: :destroy

    has_many :sponsorship_newsletters,
       foreign_key: :sponsorable_id

    has_many :sponsors_listing_featured_items, as: :featureable, dependent: :destroy

    has_one :published_sponsorable_listing, -> { with_approved_state },
      class_name: "SponsorsListing",
      as: :sponsorable

    has_one :sponsors_listing, as: :sponsorable, dependent: :destroy

    has_one :sponsors_membership, as: :sponsorable, dependent: :destroy
  end

  class_methods do
    def sponsorable_users_from_logins(sponsors_logins)
      user_ids = User.where(login: sponsors_logins).pluck(:id)
      users_with_listings = SponsorsListing.with_approved_state.where(sponsorable_id: user_ids).pluck(:sponsorable_id)
      User.select(:id, :login).where(id: user_ids & users_with_listings)
    end
  end

  delegate :active_docusign_envelope, to: :sponsors_listing, allow_nil: true

  def next_billing_date_formatted
    if monthly_plan?
    else
    end
  end

  # Public: Is this sponsorable eligible for stripe connect?
  #
  # Returns a Boolean.
  def sponsors_stripe_eligible?
    sponsors_membership&.eligible_for_stripe_connect?
  end

  # Public: Does this sponsorable need to complete stripe connect verification
  #         to be considered for approval?
  #
  # Returns a Boolean.
  def sponsors_stripe_pending?
    sponsors_stripe_eligible? && !sponsors_listing&.stripe_connect_account&.verified?
  end

  # Public: Is docusign enabled for this sponsorable?
  #
  # Returns a Boolean.
  def sponsors_docusign_enabled?
    sponsors_membership&.unsupported_fiscal_host? &&
      !GitHub.flipper[:skip_sponsors_docusign].enabled?(self)
  end

  # Public: Does this sponsorable need to complete the docusign process to
  #         be considered for approval?
  #
  # Returns a Boolean.
  def sponsors_docusign_pending?
    sponsors_docusign_enabled? && !sponsors_listing&.docusign_completed?
  end

  def sponsors_program_member?
    async_sponsors_program_member?.sync
  end

  # Public: Has this account been accepted into the Sponsors program?
  #
  # Returns a Promise<Boolean>.
  def async_sponsors_program_member?
    return Promise.resolve(false) unless GitHub.sponsors_enabled?

    Platform::Loaders::SponsorsMembershipCheck.load(id)
  end

  def sponsorable_by?(actor)
    async_sponsorable_by?(actor).sync
  end

  # Public: Indicates if an actor is able to sponsor this account.
  #
  # actor - The User that would be the sponsor.
  #
  # Returns a Promise<Boolean>.
  def async_sponsorable_by?(actor)
    return Promise.resolve(false) unless GitHub.sponsors_enabled?
    return Promise.resolve(false) if self == actor

    async_sponsors_program_member?.then do |is_sponsors_member|
      next false unless is_sponsors_member

      Platform::Loaders::SponsorsListingCheck.load(id).then do |is_listed|
        next false unless is_listed
        next true if actor.nil?

        # For actor as sponsor, batch self as sponsorable,
        # invert to indicate that actor is NOT already sponsoring selfs
        Platform::Loaders::IsSponsoringCheck.load(
          sponsor_id: actor.id,
          sponsorable_id: id,
          include_private: true,
          invert: true
        )
      end
    end
  end

  def is_public_github_sponsor?
    async_is_public_github_sponsor?.sync
  end

  # Public: Indicates if a user has an active public sponsorship as a sponsor.
  #
  # actor - The User that would be the sponsor.
  #
  # Returns a Promise<Boolean>.
  def async_is_public_github_sponsor?
    Platform::Loaders::IsSponsoringCheck.load(
      sponsor_id: id,
      sponsorable_id: nil,
      include_private: false
    )
  end

  def already_sponsored_by?(sponsor)
    async_already_sponsored_by?(sponsor).sync
  end

  # Public: Indicates if a user is already sponsoring this account.
  #
  # sponsor - The User to check.
  #
  # Returns a Boolean.
  def async_already_sponsored_by?(sponsor)
    return Promise.resolve(false) unless sponsor.present?

    # For sponsor, batch self as sponsorable
    Platform::Loaders::IsSponsoringCheck.load(
      sponsor_id: sponsor.id,
      sponsorable_id: id,
      include_private: true
    )
  end

  # Public: If the user is a sponsor for the given maintainer, returns the
  # corresponding sponsorship record.
  def sponsorship_as_sponsor_for(sponsorable)
    return unless sponsorable
    Sponsorship.find_by(sponsor: self, sponsorable: sponsorable)
  end

  def sponsors_marketplace_listing
    async_sponsors_marketplace_listing.sync
  end

  def async_sponsors_marketplace_listing
    async_marketplace_listing
  end

  def async_has_sponsors_listing
    Platform::Loaders::SponsorsListingCheck.load(id)
  end

  # Public: The total monthly amount pledged in dollars.
  def total_monthly_pledged_in_dollars
    sponsorships = sponsorships_as_sponsorable.active.includes(:subscribable)

    sponsorships.reduce(0) do |sum, sponsorship|
      monthly_price = Billing::Money.new((
        sponsorship.subscribable.base_price(duration: User::MONTHLY_PLAN).cents
      ).to_i)
      sum + monthly_price
    end
  end

  def sponsors(include_private: false,
               exclude_sponsor_opted_out_from_email: false,
               tiers: nil,
               sponsor_id: nil)
    sponsorships = sponsorships_as_sponsorable.active
    sponsorships = sponsorships.where(sponsor: sponsor_id) if sponsor_id
    sponsorships = sponsorships.privacy_public unless include_private
    sponsorships = sponsorships.where(is_sponsor_opted_in_to_email: true) if exclude_sponsor_opted_out_from_email
    sponsorships = sponsorships.where(subscribable: Array(tiers)) if tiers.present?

    User.where(id: sponsorships.pluck(:sponsor_id))
  end

  # Public: List of sponsors sponsoring this user
  def async_sponsors(include_private: false,
                     exclude_sponsor_opted_out_from_email: false,
                     tiers: nil,
                     sponsor_id: nil)
    Promise.resolve(
      sponsors(
        include_private: include_private,
        exclude_sponsor_opted_out_from_email: exclude_sponsor_opted_out_from_email,
        tiers: tiers,
        sponsor_id: sponsor_id
      )
    )
  end

  # Public: List of maintainers this user is sponsoring
  def sponsoring(include_private: false)
    sponsorships = sponsorships_as_sponsor.active
    sponsorships = sponsorships.privacy_public unless include_private

    User.where(id: sponsorships.pluck(:sponsorable_id))
  end

  def async_sponsoring(include_private: false)
    Promise.resolve(
      sponsoring(include_private: include_private)
    )
  end

  def sponsors_listing_slug
    "sponsors-#{login}"
  end

  # Public: Whether this user is part of the limited release
  # and is actively sponsoring someone
  #
  # Returns Boolean
  def active_sponsor?
    sponsoring.any?
  end

  # Public: Is this user ineligible to have their sponsorships (as sponsor)
  #         matched due to being spammy or not being old enough?
  #
  # Returns a Boolean.
  def sponsorship_match_ineligible_from_age_or_spamminess?
    return true if spammy?
    created_at > 1.month.ago
  end

  # Public: Are the sponsorships created by this user (as sponsor) eligible to
  #         be matched by GitHub?
  #
  # Returns a Boolean.
  def eligible_for_sponsorship_match?(sponsorable:)
    return false if sponsorship_match_ineligible_from_age_or_spamminess?
    return false if SponsorshipMatchBan.where(sponsor: self, sponsorable: sponsorable).exists?

    published_sponsorable_listing.blank?
  end

  # Public: Queues a job to disable Sponsors Stripe Connect payouts for a user.
  #
  # Returns nothing.
  def disable_sponsors_payouts
    return unless listing = sponsors_listing
    listing.disable_payouts
  end

  # Public: Disable Sponsors Stripe Connect payouts for a user.
  #
  # Returns the updated Billing::StripeConnect::Account or `false` on failure.
  # Raises Stripe::APIConnectionError if connecting to the Stripe API fails.
  # Raises Stripe::StripeError if updating the Stripe account fails.
  def disable_sponsors_payouts!
    return false unless listing = sponsors_listing
    listing.disable_payouts!
  end

  # Public: Queues a job to enable Sponsors Stripe Connect payouts for a user.
  #
  # Returns nothing.
  def enable_sponsors_payouts
    return unless listing = sponsors_listing
    listing.enable_payouts
  end

  # Public: Enables Sponsors Stripe Connect payouts for a user.
  #
  # Returns the updated Billing::StripeConnect::Account or `false` on failure.
  # Raises Stripe::APIConnectionError if connecting to the Stripe API fails.
  # Raises Stripe::StripeError if updating the Stripe account fails.
  def enable_sponsors_payouts!
    return false unless listing = sponsors_listing
    listing.enable_payouts!
  end

  def sponsors_contact_email
    sponsors_membership.contact_email.email
  end

  def sponsors_update_email
    email
  end

  def sponsors_enabled_accounts
    @sponsors_enabled_accounts ||= begin
      User.where(id: potential_sponsor_ids)
          .includes(:sponsors_listing, :sponsors_membership)
          .to_a
    end
  end

  def any_sponsors_listing_accounts?
    return @any_sponsors_listing_accounts if defined?(@any_sponsors_listing_accounts)

    @any_sponsors_listing_accounts = SponsorsListing.where(
      sponsorable_type: :User,
      sponsorable_id: [id] + owned_organization_ids,
    ).exists?
  end

  def sponsors_listing_accounts
    @sponsors_listing_accounts ||=
      sponsors_enabled_accounts.select(&:sponsors_listing)
  end

  def potential_sponsor_logins
    @potential_sponsor_logins ||= potential_sponsor_accounts.pluck(:login)
  end

  def potential_sponsor_accounts
    @potential_sponsor_accounts ||= User.
      where(id: potential_sponsor_ids).
      # Order this user first, then other accounts alphabetically by login
      order(Arel.sql("CASE id WHEN #{id} THEN 0 ELSE 1 END")).
      order(:login)
  end

  def potential_sponsor_ids
    [id, *owned_organization_ids]
  end

  def tier_subscription_counts
    Hash.new(0).merge(sponsorships_as_sponsorable.active.group(:subscribable_id).count)
  end

  def corporate_sponsors_credit_card_enabled?
    GitHub.flipper[:corporate_sponsors_credit_card].enabled?(self)
  end

  def fiscal_host?
    false
  end
end
