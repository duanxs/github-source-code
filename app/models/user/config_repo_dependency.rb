# frozen_string_literal: true

module User::ConfigRepoDependency
  CONFIG_REPO_TOPIC_NAMES = %w(config github-config)

  def config_repo_name
    login
  end

  def configuration_repository
    async_configuration_repository.sync
  end

  def async_configuration_repository
    return Promise.resolve(nil) unless user?
    @config_repo_promise ||= Promise.resolve(repositories.active.find_by_name(config_repo_name))
  end

  def has_configuration_repository?
    configuration_repository.present?
  end

  def create_configuration_repository(is_public:, reflog_data: {})
    return unless user?

    repo_attrs = {
      name: config_repo_name,
      description: "Config files for my GitHub profile.",
      auto_init: true,
      has_wiki: false,
      has_issues: false,
      homepage: "#{GitHub.url}/#{login}", # URL to the user profile
      public: is_public,
    }
    reflog_data = reflog_data.merge(
      repo_name: "#{login}/#{config_repo_name}",
      repo_public: is_public,
    )
    result = Repository.handle_creation(self, login, false, repo_attrs, reflog_data)
    config_repo = result.repository

    if result.success?
      CONFIG_REPO_TOPIC_NAMES.each do |topic_name|
        Topic.apply_to_repository(topic_name, repository: config_repo, state: :created, user: self)
      end
      @configuration_repository = config_repo
    end

    config_repo
  end
end
