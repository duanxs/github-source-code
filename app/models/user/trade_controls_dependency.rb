# frozen_string_literal: true

module User::TradeControlsDependency
  extend ActiveSupport::Concern

  included do
    has_one :trade_controls_restriction, class_name: "TradeControls::Restriction", autosave: false
    has_many :pending_trade_controls_restrictions, class_name: "TradeControls::PendingRestriction", foreign_key: :target_id, autosave: false
  end

  # Public: check if the user has been restricted for trade
  # controls compliance as required by the Office of Foreign
  # Assets Control (OFAC)
  #
  # Returns Boolean
  def ofac_sanctioned?
    trade_controls_restriction.any?
  end

  # Public: whether this user has partial trade restrictions
  #
  # Returns Boolean
  def has_partial_trade_restrictions?
    async_trade_controls_restriction.then(&:partial?).sync
  end

  # Public: whether this user has tier_1 trade restrictions
  #
  # Returns Boolean
  def has_tier_1_trade_restrictions?
    trade_controls_restriction.tier_1?
  end

  # Public: whether this user has tier_0 trade restrictions
  #
  # Returns Boolean
  def has_tier_0_trade_restrictions?
    trade_controls_restriction.tier_0?
  end

  # Public: whether this user has any tiered restrictions
  #
  # Returns Boolean
  def has_any_tiered_trade_restrictions?
    has_tier_0_trade_restrictions? || has_tier_1_trade_restrictions?
  end

  # Public: whether this user has full trade restrictions
  #
  # Returns Boolean
  def has_full_trade_restrictions?
    async_trade_controls_restriction.then(&:full?).sync
  end

  # Public: whether this user has tiered trade restrictions
  #
  # Returns Boolean
  def has_tiered_trade_restrictions?
    trade_controls_restriction.tiered_restriction?
  end

  # Public: whether this user has any trade restrictions
  #
  # Returns Boolean
  def has_any_trade_restrictions?
    async_trade_controls_restriction.then(&:any?).sync
  end

  # Public: returns the has_one :trade_controls_restriction association.
  # If the associated record doesn't exist, it falls back to building a new
  # one. This ensures callers do not need the safe-navigation operator. It
  # also ensures a consistent API between Users with an `unrestricted` record
  # and those without an associated restriction at all; as those two scenarios
  # are semantically the same.
  #
  # Returns TradeControls::Restriction
  def trade_controls_restriction
    ActiveRecord::Base.connected_to(role: :reading) { super || build_trade_controls_restriction }
  end

  # Public: Returns this User/Organization's scheduled pending trade controls restriction
  # record with a particular reason
  #
  # reason - Reason for the pending enforcement.
  #
  # Returns TradeControls::PendingRestriction or nil
  def pending_trade_controls_restriction(reason:)
    pending_trade_controls_restrictions.find_by(reason: reason, enforcement_status: :pending)
  end

  # Public: Checks whether this User/Organization has a scheduled pending trade controls restriction
  # enforcement with a particular reason
  #
  # Returns Boolean
  def has_pending_trade_controls_restriction?(reason:)
    pending_trade_controls_restriction(reason: reason).present?
  end

  # Public: Checks whether this User/Organization has any scheduled pending trade controls restriction
  # enforcement
  #
  # Returns Boolean
  def has_any_pending_trade_restriction?
    scheduled_pending_trade_restriction.present?
  end

  # Public: Returns any scheduled pending trade controls restriction record for this
  # User/Organization
  #
  # Returns TradeControls::PendingRestriction
  def scheduled_pending_trade_restriction
    @pending_restriction ||= pending_trade_controls_restrictions.find_by(enforcement_status: :pending)
  end

  # Public: Schedule a new pending enforcement for this User/Organization
  # We only want to schedule an enforcement one time for the same reason.
  # As long as a User/Organization has an active (not cancelled) pending enforcement,
  # any subsequent flagging of minor threshold violations with the same reason are ignored.

  # reason - Reason to check for previous appeals.
  # threshold - Threshold for the proposed pending restriction.
  #
  def schedule_pending_enforcement(reason:, threshold:)
    if !has_pending_trade_controls_restriction?(reason: reason) &&
        !has_successful_trade_controls_restriction_appeal?(reason: reason, threshold: threshold)
      pending_restriction = pending_trade_controls_restrictions.build(
        reason: reason,
        enforcement_status: :pending,
        enforcement_on: GitHub::Billing.now + 14.days,
        last_restriction_threshold: threshold,
      )
      if pending_restriction.save
        send_trade_controls_pending_enforcement_email
        GitHub.dogstats.increment("pending_trade_controls_restriction", tags: ["action:create"])
      end
    end
  end

  # Public: returns the unique instance of TradeControls::PendingRestriction
  # associated record with enforcement_status cancelled(successfully appeal).
  #
  # Before scheduling a pending enforcement, we need to know if the actor
  # has already made an successful appeal for the same, or lower, threshold
  # and reason.
  #
  # reason - Reason to check for previous appeals.
  # threshold - Threshold for the proposed pending restriction.
  #
  # Return Boolean
  def has_successful_trade_controls_restriction_appeal?(reason:, threshold:)
    successfully_appealed_restriction = pending_trade_controls_restrictions
      .where(reason: reason, enforcement_status: :cancelled).last

    return false if successfully_appealed_restriction.nil?

    successfully_appealed_restriction.last_restriction_threshold >= threshold
  end

  # Override the built-in async_* association to ensure it is never nil.
  # Whenever the restriction record doesn't exist, we build a new one.
  # see overridden trade_controls_restriction association.
  def async_trade_controls_restriction
    ActiveRecord::Base.connected_to(role: :reading) {
      super.then do |restriction|
        restriction || trade_controls_restriction
      end
    }
  end

  # Public: Determine if this organization can access a feature based on their restriction tier.
  #
  # object: This is the feature we are checking against a tier for trade restriction. (default: nil)
  #
  # Returns Boolean
  def restriction_tier_allows_feature?(type: nil)
    return true unless has_any_trade_restrictions?
    return false if self.is_a?(Organization) && has_full_trade_restrictions?

    case type
    when :repository
      trade_controls_restriction.tier_0?
    else
      false
    end
  end

  def scheduled_ofac_downgrade
    @ofac_downgrade ||= OFACDowngrade.incomplete.where(user_id: id).last
  end

  # Public: Is this user's trade restriction finalized?
  #
  # Returns Boolean
  def trade_restriction_finalized?
    has_any_trade_restrictions? && complete_ofac_downgrade.present?
  end

  # Public: Date when the trade restriction downgrade was completed
  #
  # Returns Date or nil
  def trade_restriction_finalized_date
    complete_ofac_downgrade&.downgrade_on
  end

  # Private: Returns the last completed OFAC downgrade if it exists
  #
  # Returns OFACDowngrade or nil
  def complete_ofac_downgrade
    @complete_ofac_downgrade ||= OFACDowngrade.completed.where(user_id: id).last
  end

  # Internal: Sends an email for the individual actor being restricted
  #
  # Returns nil
  def send_trade_controls_enforcement_email
    TradeControlsMailer.individual_actor_restricted(self).deliver_later
  end

  # Internal: Sends an email for the individual actor being overriden
  #
  # Returns nil
  def send_trade_controls_override_email
    TradeControlsMailer.individual_actor_reactivated(self).deliver_later
  end

  def instrument_trade_controls_enforcement(compliance:)
    GlobalInstrumenter.instrument("trade_restriction.flag",
                                  user: self, **compliance.to_hydro)
  end

  def instrument_trade_controls_override(compliance:)
    GlobalInstrumenter.instrument("trade_restriction.unflag",
                                  user: self, **compliance.to_hydro)
  end
end
