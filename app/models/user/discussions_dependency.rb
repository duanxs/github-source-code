# frozen_string_literal: true

module User::DiscussionsDependency
  # Public: Can this user start a new discussion in the given repository?
  #
  # repo - a Repository
  #
  # Returns a Boolean.
  def can_create_discussion?(repo)
    # Ensure the feature flag is enabled
    return false unless repo.discussions_enabled?

    require_explicit_perm = repo.discussion_creation_requires_explicit_permission?

    response = ::Permissions::Enforcer.authorize(
      action: :create_discussion,
      actor: self,
      subject: repo,
      context: {
        "organization.configuration.discussions.requires_explicit_access_for_creation" => require_explicit_perm
      }
    )
    response.allow?
  end

  # Public: Can this user create discussion categories in the given repository?
  #
  # repo - a Repository
  #
  # Returns a Boolean.
  def can_create_discussion_category?(repo)
    # Ensure the feature flag is enabled
    return false unless repo.discussions_enabled?

    response = ::Permissions::Enforcer.authorize(
      action: :create_discussion_category,
      actor: self,
      subject: repo
    )
    response.allow?
  end

  # Public: Does this user have permission to add, remove, and edit
  # discussion spotlights for the given repository?
  #
  # repo - a Repository
  #
  # Returns a Boolean.
  def can_manage_discussion_spotlights?(repo)
    return false unless repo
    return false if should_verify_email?
    return false if spammy? || suspended?
    return false unless repo.active? && repo.writable?
    return false unless repo.discussion_spotlights_enabled?
    return false unless repo.show_discussions?

    action_or_role_level = repo.async_action_or_role_level_for(self).sync
    [:maintain, :admin].include?(action_or_role_level)
  end

  # Public: Does this user have permission to add discussion spotlights
  # for the given repository, and the repository isn't already at the limit of
  # how many discussion spotlights it can have?
  #
  # repo - a Repository
  #
  # Returns a Boolean.
  def can_create_discussion_spotlight?(repo)
    return false if DiscussionSpotlight.repository_at_limit?(repo)
    can_manage_discussion_spotlights?(repo)
  end

  # Public: Returns a count of how many replies by this user to discussions in this repository have been marked
  # as the right answer.
  def chosen_answer_count(repo)
    discussion_comments.for_repository(repo).chosen_answers.count
  end

  # Public: Is this user someone who frequently responds to discussions in the given repository?
  #
  # repo - a Repository or its ID
  # actor - the current User
  #
  # Returns a Boolean.
  def frequent_helper?(repo, actor:)
    unique_disc_comment_count = discussion_comments.filter_spam_for(actor).for_repository(repo).
      select(:discussion_id).distinct.count
    unique_disc_comment_count >= DiscussionComment::FREQUENT_HELPER_COUNT
  end
end
