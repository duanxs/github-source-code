# frozen_string_literal: true

class User::InteractionAbility

  include Instrumentation::Model

  attr_reader :user_id

  TTL = 1.day

  # Determine whether the user is allowed to make comments on public repos
  def self.interaction_allowed?(user_id, repo = nil)
    async_interaction_allowed?(user_id, repo).sync
  end

  def self.async_interaction_allowed?(user_id, repo = nil)
    return new(user_id).async_interactions_allowed? unless repo
    Platform::Loaders::ActiveRecord.load(User, user_id).then do |user|
      next false unless user
      repo.async_pushable_by?(user).then do |pushable|
        next true if pushable
        async_repo_interaction_allowed?(user, repo).then do |allowed|
          next false unless allowed
          new(user_id).async_interactions_allowed?
        end
      end
    end
  end

  # Allow the user to make interactions on public repos
  def self.allow_interactions(user_id)
    new(user_id).allow_interactions
  end

  # Prevent the user from interactioning on public repos
  def self.disallow_interactions(user_id)
    new(user_id).disallow_interactions
  end

  def self.toggle_interaction_ban(user_id)
    new(user_id).toggle_interaction_ban
  end

  # Get the expiration date of the ban
  def self.ban_expiry(user_id)
    new(user_id).ban_expiry
  end

  def initialize(user_id)
    @user_id = user_id
  end

  def allow_interactions
    instrument_update("allow", user_id)
    GitHub.kv.del(interaction_key)
  end

  def disallow_interactions
    instrument_update("disallow", user_id)
    GitHub.kv.set(interaction_key, TTL.from_now.to_s, expires: TTL.from_now) && true
  end

  def interactions_allowed?
    async_interactions_allowed?.sync
  end

  def async_interactions_allowed?
    return Promise.resolve(true) unless GitHub.interaction_limits_enabled?
    Platform::Loaders::KV.load(interaction_key).then { |value| !value }
  end

  def toggle_interaction_ban
    interactions_allowed? ? disallow_interactions : allow_interactions
  end

  def ban_expiry
    expiry = GitHub.kv.get(interaction_key).value!
    expiry && DateTime.parse(expiry)
  end

  private

  def interaction_key
    "user.interactions_disallowed.#{user_id}"
  end

  def event_prefix
    "stafftools_interaction_ability"
  end

  def instrument_update(toggle_type, user_id)
    instrument toggle_type, user_id: user_id
  end

  def self.async_repo_interaction_allowed?(user, repo)
    RepositoryInteractionAbility.async_restricted_by_limit?(:collaborators_only, repo, user).then do |restricted|
      next false if restricted
      RepositoryInteractionAbility.async_restricted_by_limit?(:contributors_only, repo, user).then do |restricted|
        next false if restricted
        RepositoryInteractionAbility.async_restricted_by_limit?(:sockpuppet_ban, repo, user).then { |restricted| !restricted }
      end
    end
  end
  private_class_method :async_repo_interaction_allowed?
end
