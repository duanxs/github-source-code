# frozen_string_literal: true

module User::RepositoryTemplateDependency
  # Public: Get a list of repo IDs for templates this user has recently cloned.
  #
  # limit - optional integer count of how many repo IDs at most to return
  #
  # Returns an Array. Will only contain repository IDs for a User, not an Organization or Bot.
  def recently_used_template_repository_ids(limit: 100)
    return [] unless user?
    RepositoryClone.for_user(self).order("repository_clones.id DESC").limit(limit).
      pluck(:template_repository_id)
  end

  # Public: Get a list of possible templates connected to this User or Organization that the
  # current user can access to generate a new repository.
  #
  # viewer - the currently authenticated User
  # scope - optional Repository ActiveRecord relation for filtering
  #
  # Returns an Array of Repositories.
  def repository_templates_for(viewer, scope: nil)
    template_scope = Repository.active.templates_relevant_to(self).filter_spam_and_disabled_for(viewer)
    template_scope = template_scope.merge(scope) if scope

    templates = if viewer
      template_ids = template_scope.pluck(:id)

      # Pass the template ids as a limiting parameter. This assumes there is almost always a relatively small number
      # of template repos whereas the number of associated repos is unbounded.
      associated_template_ids = viewer.associated_repository_ids(repository_ids: template_ids)
      if associated_template_ids.any?
        template_scope.where("`repositories`.`id` IN (?) OR `repositories`.`public` = ?", associated_template_ids, true)
      else
        template_scope.public_scope
      end
    else
      template_scope.public_scope
    end

    # Do sorting in Ruby to avoid a `CrossDomainQueryError` when sorting by user+repo in SQL
    templates.includes(:owner).sort_by do |template|
      is_mine = template.owner == viewer ? 0 : 1
      # Put viewer's own templates first, then group remaining templates by their owner
      "#{is_mine}#{template.name_with_owner.downcase}"
    end
  end
end
