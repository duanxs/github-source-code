# frozen_string_literal: true

module User::BusinessDependency
  # An Organization has_one :business, but a vanilla User never has a business.
  def business
    nil
  end

  def async_business
    Promise.resolve(nil)
  end

  # Returns true if this account is part of a business
  # which is managed by sales rather than being self-serve
  # Always false for individual users,
  # only true when an organization is a member of a business
  # and that business has can_self_serve disabled.
  #
  # Returns a Boolean.
  def in_a_sales_managed_business?
    business && !business.can_self_serve?
  end

  # Public: The IDs of businesses of which this user is a member.
  #
  # A "member" is defined as either:
  # - An admin of a business
  # - A billing manager of a business
  # - A member of an org that is owned by a business
  #
  # membership_type - A Symbol representing user membership type. Businesses
  #   matching the user membership type will be returned. Must be one of one of the following:
  #
  #   - :all - Returns all businesses in which the user is a member.
  #   - :admin - Returns all businesses in which the user is an admin.
  #   - :billing_manager - Returns all businesses in which the user is a billing manager.
  #   - :org_membership - Returns all businesses in which the user is a member of an org
  #   - :support_entitled - Returns all businesses in which the user is a member and is support entitled
  #     that is owned by the business.
  #
  #   Defaults to :all.
  #
  # Returns a Promise
  def async_business_ids(membership_type: :all)
    return Promise.resolve([]) if EnterpriseAttestation.contractor?(id)
    case membership_type
    when :all
      Promise.resolve(
        business_admin_ability_ids |
        business_billing_management_ability_ids |
        membership_via_org_ids,
      )
    when :admin
      Promise.resolve(business_admin_ability_ids)
    when :billing_manager
      Promise.resolve(business_billing_management_ability_ids)
    when :org_membership
      Promise.resolve(membership_via_org_ids)
    when :support_entitled
      Promise.resolve(business_support_entitled_ids)
    else
      raise ArgumentError, "membership_type must be one of :all, :admin, :billing_manager, :org_membership"
    end
  end

  # Public: The business IDs in which this user is a member.
  #
  # Synchronous version of async_business_ids
  #
  # Returns an array of IDs
  def business_ids(membership_type: :all)
    async_business_ids(membership_type: membership_type).sync
  end

  def async_businesses(membership_type: :all)
    async_business_ids(membership_type: membership_type).then do |ids|
      Business.where(id: ids.compact.uniq)
    end
  end

  # The businesses in which the user is a member.
  #
  # Returns an ActiveRecord relation
  def businesses(membership_type: :all)
    ids = business_ids(membership_type: membership_type)
    Business.where(id: ids.compact.uniq)
  end

  def is_business_member?(business_id)
    business_ids.any?(business_id)
  end

  # Public: obtain the internal repositories to which the user has access via business membership.
  # It does not take into consideration explicit access (direct/indirect) to the repo.
  #
  # Returns an ActiveRecord::Relation of Repositories
  def internal_repositories
    ids = business_ids
    return Repository.none if ids.empty?

    Repository.joins(:internal_repository).where("internal_repositories.business_id IN (?)", ids)
  end

  def internal_repo_ids
    internal_repositories.pluck(:id)
  end
  alias_method :internal_repository_ids, :internal_repo_ids

  private

  # IDs of businesses where the user has a direct admin ability on a business.
  def business_admin_ability_ids
    return [] if new_record?
    return [] if ability_delegate.nil?
    return @business_admin_ability_ids if defined?(@business_admin_ability_ids)
    @business_admin_ability_ids = Ability.where(
        subject_type: "Business",
        actor_id: self,
        actor_type: self.ability_type,
        priority: Ability.priorities[:direct],
        action: Ability.actions[:admin],
    ).pluck(:subject_id)
  end

  # IDs of businesses where the user has an ability on the business billing management.
  def business_billing_management_ability_ids
    return [] if new_record?
    return [] if ability_delegate.nil?
    return @business_billing_management_ability_ids if defined?(@business_billing_management_ability_ids)
    @business_billing_management_ability_ids = Ability.where(
        subject_type: "Business::BillingManagement",
        actor_id: self,
        actor_type: self.ability_type,
        priority: Ability.priorities[:direct],
    ).pluck(:subject_id)
  end

  # IDs of businesses where the user has a SupportEntitlee ability and is a member
  def business_support_entitled_ids
    return [] if new_record?
    return [] if ability_delegate.nil?
    return @business_support_entitled_ids if defined?(@business_support_entitled_ids)
    support_entitlee = Business::SupportEntitlee.new(self)
    @business_support_entitled_ids = support_entitlee.abilities.pluck(:subject_id) & membership_via_org_ids
  end

  # IDs of businesses that include an org in which the user is a member.
  def membership_via_org_ids
    return @membership_via_org_ids if defined?(@membership_via_org_ids)
    @membership_via_org_ids = ::Business::OrganizationMembership.where(organization_id: organization_ids).distinct.pluck(:business_id)
  end
end
