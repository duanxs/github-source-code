# rubocop:disable Style/FrozenStringLiteralComment

module User::InstrumentationDependency
  # Performs all tracking necessary for a plan change: creates a Transaction,
  # instruments and increments stats.  Take options to help provide more
  # context when instrumenting.
  #
  # actor     - User that initiated for the change
  # old_plan  - Plan before the change
  # opts      -
  #   :coupon                    - Coupon that was redeemed in conjunction with the change
  #   :repository                - Repository that was created in conjunction with the change
  #   :billing_transaction       - BillingTransaction if there was a charge
  #   :old_subscription_provider - The old subscription provider name
  #   :reason                    - (Optional) A String describing why the change was made.
  #
  # Returns nothing
  def track_plan_change(actor, old_plan, opts = {})
    old_plan = GitHub::Plan.default_plan if old_plan.nil?
    return if old_plan == plan

    opts.reverse_merge!({
      coupon: nil,
      repository: nil,
      billing_transaction: nil,
    })

    # transaction
    transactions.create(old_plan: old_plan.name,
      billing_transaction: opts[:billing_transaction])

    payload = plan_change_payload(actor)
    payload.update(old_plan: old_plan.name)
    payload.update(coupon: opts[:coupon].code) if opts[:coupon]
    payload.update(repository: opts[:repository].full_name) if opts[:repository]
    payload.update(reason: opts[:reason]) if opts[:reason]

    if opts[:old_subscription_provider]
      payload.update(old_subscription_provider: opts[:old_subscription_provider])
    end

    GitHub.instrument("account.plan_change", payload)

    # stats
    if old_plan.cost < plan.cost
      GitHub.dogstats.increment("user.plan", tags: ["action:upgrade"])
    else
      GitHub.dogstats.increment("user.plan", tags: ["action:downgrade"])
    end
  end

  # Create a Transaction to track billing cycle changes (plan_duration).
  #
  # actor              - User that initiated for the change.
  # old_plan_duration  - Plan duration before the change.
  #
  # Returns nothing.
  def track_plan_duration_change(actor, old_plan_duration)
    return if old_plan_duration == plan_duration

    transactions.create(old_plan_duration: old_plan_duration)

    payload = plan_change_payload(actor)
    payload.update(old_plan_duration: old_plan_duration)

    GitHub.instrument("account.plan_change", payload)

    GitHub.dogstats.increment("user.billing.cycle_change",
      tags: ["plan_duration:#{plan_duration}"])
  end

  # Create a Transaction to track organization seat count changes
  #
  # actor   - User that initiated the change
  # options - Hash of additional options.
  #           :old_seats           - Seat count before the change.
  #           :billing_transaction - Associated BillingTransaction.
  #           :reason              - (Optional) A String describing why the change was made.
  #
  # Returns nothing
  def track_seat_change(actor, options = nil)
    options ||= {}
    old_seats = options[:old_seats].to_i
    return if old_seats == seats

    transactions.create \
      old_seats: old_seats,
      billing_transaction: options[:billing_transaction]

    payload = plan_change_payload(actor)
    payload.update(old_seats: old_seats)
    payload.update(reason: options[:reason]) if options[:reason]

    GitHub.instrument("account.plan_change", payload)
  end

  # Create a Transaction to track data pack count changes
  #
  # actor           - User that initiated the change.
  # old_data_packs - Integer Data packs before the change.
  #
  # Returns nothing
  def track_data_pack_change(actor, old_data_packs:)
    old_data_packs    = old_data_packs.to_i
    asset_packs_total = data_packs
    asset_packs_delta = asset_packs_total - old_data_packs
    return if asset_packs_delta == 0

    transactions.create \
      asset_packs_delta: asset_packs_delta,
      asset_packs_total: asset_packs_total

    payload = plan_change_payload(actor)
    payload.update(old_data_packs: old_data_packs)

    GitHub.instrument("account.plan_change", payload)
  end

  def track_subscription_item_changes(actor, previous_plan_duration: nil)
    subscription_items.each do |item|
      prefix = item.listable_is_sponsorable? ? "sponsorship" : "marketplace_purchase"

      GitHub.instrument "#{prefix}.changed",
        subscription_item_id: item.id,
        sender_id: actor.id,
        previous_plan_duration: previous_plan_duration
    end
  end

  def track_lock_billing
    GitHub.instrument("billing.lock", user: self)
  end

  def track_unlock_billing
    GitHub.instrument("billing.unlock", user: self)
  end

  private

  def plan_change_payload(actor)
    payload = user? ? { user: self } : { org: self }
    payload.update \
      old_plan: plan.name,
      plan: plan.name,
      old_plan_duration: plan_duration,
      plan_duration: plan_duration,
      old_seats: seats,
      seats: seats,
      old_data_packs: data_packs,
      asset_packs: data_packs,
      tos_sha: TosAcceptance.current_sha

    if actor&.staff?
      payload.update GitHub.guarded_audit_log_staff_actor_entry(actor)
    else
      payload.update actor: actor
    end

    payload
  end
end
