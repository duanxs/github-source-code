# frozen_string_literal: true

class User
  class CardConverter
    def initialize(target)
      @target = target
    end

    # Public: Can target be converted to card based billing?
    #
    # Returns true if convertable, otherwise false
    def convertable?
      target.invoiced?
    end

    # Public: Switch to credit card billing
    #
    # actor - User taking this action.
    #
    # Returns truthy if the switch was successful.
    def convert(actor:)
      target.transaction do
        target.update!(billing_type: User::CARD_BILLING_TYPE)
        target.customer.update!(zuora_account_id: nil) if target.customer
        target.plan_subscription&.clear_zuora_association
      end

      instrument(actor: actor)

      true
    end

    private

    attr_reader :target

    def instrument(actor:)
      GlobalInstrumenter.instrument(
        "billing.change_billing_type",
        old_billing_type: target.attribute_before_last_save(:billing_type),
        billing_type: target.billing_type,
        user: target,
        actor: actor,
      )
    end
  end
end
