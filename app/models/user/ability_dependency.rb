# rubocop:disable Style/FrozenStringLiteralComment

module User::AbilityDependency
  extend ActiveSupport::Concern

  include Ability::Actor
  include Ability::Subject

  def ability_description
    "@#{login}"
  end

  def permit?(actor, action)
    async_permit?(actor, action).sync
  end

  def async_permit?(actor, action)
    actor = actor.ability_delegate

    return Promise.resolve(false) if !actor.is_a?(User) || actor.new_record?

    super
  end

  # Internal: Because we don't allow grants where a user is the subject, a user
  # cannot be used as an Abilities connector.
  def connector?
    false
  end

  # Internal: Disallow grants where a user is the subject.
  def grant?(actor, action)
    false
  end
end
