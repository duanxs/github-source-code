# rubocop:disable Style/FrozenStringLiteralComment

module User::EnterpriseArOverridesDependency
  if GitHub.enterprise?
    BLACKLISTED_ATTRIBUTES = [:bcrypt_auth_token, :crypted_password, :auth_token, :salt, :session_fingerprint, :token_secret]

    # Public: Overrides inspect to only return non-blacklisted keys for a User.
    # For use in enterprise environment to prevent sensitive attributes
    # from showing up in stack traces.
    def inspect
      cols = self.class.column_names.reject do |name|
        BLACKLISTED_ATTRIBUTES.include?(name.to_sym)
      end

      attributes_as_nice_string = cols.collect { |name|
        if has_attribute?(name) || new_record?
          "#{name}: #{attribute_for_inspect(name)}"
        end
      }.compact.join(", ")

      "#<#{self.class} #{attributes_as_nice_string}>"
    end
  end
end
