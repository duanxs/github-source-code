# rubocop:disable Style/FrozenStringLiteralComment

require "github/billing"

module User::BillingTransactionsDependency
  extend ActiveSupport::Concern

  included do
    has_many :billing_transactions, class_name: "Billing::BillingTransaction"
    has_many :line_items, through: :billing_transactions, class_name: "Billing::BillingTransaction::LineItem"
    has_many :billing_disputes, class_name: "Billing::Dispute"
  end
end
