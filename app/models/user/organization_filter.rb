# rubocop:disable Style/FrozenStringLiteralComment

class User

  # Public: Filter the set of orgs that the user is a member of.
  class OrganizationFilter
    attr_reader :user

    def initialize(user)
      @user = user
    end

    def scope
      Organization.where(id: scoped_ids)
    end

    def scoped_ids
      if user.governed_by_oauth_application_policy?
        org_ids = Organization.where(id: unscoped_ids).oauth_app_policy_met_by(user.oauth_application).pluck(:id)
        org_ids |= user.public_organizations.pluck(:id)
      else
        unscoped_ids
      end
    end

    def unscoped_ids
      GitHub.instrument "ability.user.organization-filter" do
        return [] if user.new_record?
        return [] if user.ability_delegate.nil?

        Ability.where(
          subject_type: "Organization",
          actor_id: user.id,
          actor_type: "User",
          priority: Ability.priorities[:direct],
        ).pluck(:subject_id).sort
      end
    end
  end
end
