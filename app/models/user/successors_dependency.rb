# rubocop:disable Style/FrozenStringLiteralComment

module User::SuccessorsDependency
  extend ActiveSupport::Concern
  included do
    has_one :deceased_user, dependent: :destroy
  end

  # Public: Mark this user as being deceased.
  #
  # Returns nil on error or a DeceasedUser on success.
  def mark_deceased
    if deceased?
      errors.add(:deceased, "has already been set for this user")
      return nil
    end
    create_deceased_user
  end

  def deceased?
    deceased_user.present?
  end
end
