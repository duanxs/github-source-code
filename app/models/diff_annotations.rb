# frozen_string_literal: true

class DiffAnnotations
  include Enumerable

  def initialize(annotations, diffs = nil)
    @annotations = annotations
    @diffs = diffs

    if @diffs.present?
      @diff_paths = encoded_diff_paths
    end
  end

  # Retrieve the, possibly empty, collection of annotations for this path name.
  #
  # Returns a DiffAnnotations collection.
  def path(path)
    DiffAnnotations.new(paths[path] || [])
  end

  # Retrieve the, possibly empty, collection of annotations with end_lines at this line number.
  #
  # Returns a DiffAnnotations collection.
  def end_line(line)
    DiffAnnotations.new(end_lines[line] || [])
  end

  # Iterate through each Annotation in the collection.
  #
  # Returns an Enumerator.
  def each(&block)
    @annotations.each(&block)
  end

  # Returns an Array of Strings representing filepaths
  def non_diff_paths
    non_diff.pluck(:path).uniq
  end

  # Returns a hash of (key) file_path and (value) Array of context line
  # ranges that we want to render in order to display annotations, even if
  # they are not actually part of the diff.
  #
  # {
  #   "file_path": [2..8, 28..38]
  # }
  def line_ranges
    paths.map do |path, annotations|
      line_ranges = annotations.map(&:line_range)

      [path, line_ranges]
    end.to_h
  end

  private

  def paths
    @paths ||= @annotations.group_by { |annotation| annotation.path }
  end

  def end_lines
    @end_lines ||= @annotations.group_by { |annotation| annotation.end_line }
  end

  def non_diff
    return [] unless @diff_paths.present?
    @non_diff ||= @annotations.reject { |annotation| @diff_paths.include?(annotation.path) }
  end


  def encoded_diff_paths
    @diffs.requested_paths.map { |path| GitHub::Encoding.try_guess_and_transcode(path) }
  end
end
