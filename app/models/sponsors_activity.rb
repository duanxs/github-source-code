# frozen_string_literal: true

class SponsorsActivity < ApplicationRecord::Collab
  # These are offsets relative to Date.today used to find the beginning
  # of a time period for a sponsors activity timeline:
  #
  # - :day goes back zero days (only today)
  # - :week goes back 6 days (7 days including today)
  # - :month goes back 29 days (30 days including today)
  PERIOD_OFFSET_MAPPING = { day: 0, week: 6, month: 29 }
  DEFAULT_PERIOD = :alltime
  VALID_PERIODS = [:day, :week, :month, DEFAULT_PERIOD]

  enum sponsorable_type: [User.name, Organization.name], _prefix: :sponsorable_is
  enum sponsor_type: [User.name, Organization.name], _prefix: :sponsor_is

  belongs_to :sponsorable, polymorphic: true
  belongs_to :sponsor, polymorphic: true

  belongs_to :sponsors_tier
  belongs_to :old_sponsors_tier, class_name: "SponsorsTier"

  enum action: [
    # sponsorship is created with a successful prorated transaction
    :new_sponsorship,
    # existing sponsorship is cancelled by a pending change
    :cancelled_sponsorship,
    # sponsorship tier changed directly by sponsor (upgrade) or by a
    # scheduled/pending change (downgrade)
    :tier_change,
    # funds from a previous transaction have been refunded to the
    # sponsor and/or GitHub (match). Currently sourced from
    # SponsorshipTransferReversal events.
    :refund,
    # a downgrade or cancellation is scheduled for the start of the
    # sponsor's next billing cycle
    :pending_change,
    # a previously matched sponsorship will no longer be matched going forward
    :sponsor_match_disabled
  ], _prefix: :is

  validates :sponsorable, :action, :timestamp, presence: true
  validates :sponsors_tier, presence: true, unless: :is_pending_change?
  validates :old_sponsors_tier, presence: true, if: -> { is_pending_change? || is_tier_change? }

  scope :by_timestamp, -> { order(timestamp: :desc) }

  scope :for_period, -> (period) {
    if period == :alltime
      scoped
    else
      recorded_during(period)
    end
  }

  scope :recorded_during, -> (period) {
    end_date = Date.today
    offset = PERIOD_OFFSET_MAPPING[period]
    start_date = end_date - offset.days

    start_ts = start_date.strftime("%Y-%m-%d 00:00:00")
    end_ts = end_date.strftime("%Y-%m-%d 23:59:59")

    where("timestamp >= ? AND timestamp <= ?", start_ts, end_ts)
  }

  def sponsor
    super || User.ghost
  end

  def is_increase?
    return true if is_new_sponsorship?

    if is_tier_change? || is_pending_tier_change?
      sponsors_tier.monthly_price_in_dollars > old_sponsors_tier.monthly_price_in_dollars
    else
      false
    end
  end

  def is_pending_cancellation?
    is_pending_change? && sponsors_tier_id.nil?
  end

  def is_pending_tier_change?
    is_pending_change? && sponsors_tier_id.present?
  end
end
