# frozen_string_literal: true

class MannequinAssociationRewriter
  TRANSFERABLE_ASSOCIATIONS = [
    :assignments,
    :attachments,
    :commit_comments,
    :created_projects,
    :cross_references,
    :issue_comments,
    :issue_events,
    :issues,
    :project_cards,
    :projects,
    :pull_request_review_comments,
    :pull_request_reviews,
    :pull_requests,
    :review_requests,
    :subjected_issue_event_details,
  ].freeze

  attr_reader :source, :target

  def initialize(source, target)
    @source = source
    @target = target
  end

  def rewrite!
    TRANSFERABLE_ASSOCIATIONS.each do |association|
      source.send(association).transfer_to(target)
    end
  end
end
