# rubocop:disable Style/FrozenStringLiteralComment

class AuditLogExport < ApplicationRecord::Domain::Users
  areas_of_responsibility :audit_log
  REMOTE_BUCKET = "github-audit-log-exports".freeze

  include Instrumentation::Model

  belongs_to :actor, class_name: "User"
  belongs_to :subject, polymorphic: true

  validates :subject_type, presence: true
  validates :subject_id,   presence: true
  validates :actor_id,     presence: true
  validates :token,        presence: true
  validates :format,       presence: true, inclusion: { in: ["json", "csv"] }
  validate  :subject_must_be_valid

  before_validation :set_default_format, on: :create
  before_validation :generate_token, on: :create
  after_create :increment_create_count
  after_commit :enqueue_process_export_results, on: :create
  before_destroy :delete_remote_file

  def human_filename
    "export-#{subject_label}-#{created_at.to_i}.#{format}"
  end

  # Public: The human readable name for the subject.
  #
  # Returns String.
  def subject_label
    case subject
    when User, Organization
      subject.login
    when Business
      subject.slug
    end
  end

  def filename
    prefix = Rails.env.production? ? "prod" : "dev"
    "#{prefix}/#{token}.#{format}"
  end

  # Public: The content type for storing and downloading the audit log export.
  #
  # Returns String.
  def content_type
    case format
    when "json"
      "application/json"
    when "csv"
      "text/csv"
    end
  end

  def process
    GitHub.dogstats.time("audit_log_export", tags: ["action:process"]) do
      # Start the export process
      total_entries, contents = Time.use_zone(actor.time_zone) do
        export = Audit::BulkExport.create(
          subject: subject,
          phrase: phrase,
          format: format
        )

        export.run
      end

      # The export is complete, store the results
      uploaded = store_results(contents)

      if uploaded
        instrument :audit_log_export, total_entries: total_entries
      else
        false
      end
    end
  end

  def remote_object
    @remote_object ||= Aws::S3::Object.new(
      bucket_name: REMOTE_BUCKET, key: filename, client: GitHub.s3_primary_client,
    )
  end

  def remote_object?
    Aws::S3::Object.new(
      bucket_name: REMOTE_BUCKET, key: filename, client: GitHub.s3_primary_client,
    ).exists?
  end

  # Public: The event prefix for the audit log export event. It is dependent on
  # the subject the export is performed against.
  #
  # Returns Symbol
  def event_prefix
    subject.event_prefix
  end

  # Public: The event context to be logged for the audit log export. Since
  # exports are temporary they don't include any lookup reference.
  def event_context
    {}
  end

  def to_param
    token
  end

  private

  # Private: Store the export results remotely.
  #
  # contents - The String body of audit log export to store.
  #
  # Returns true if stored, false if not.
  def store_results(contents)
    result = GitHub.s3_primary_client.put_object(
      acl: "private",
      content_type: content_type,
      body: contents,
      bucket: REMOTE_BUCKET,
      key: filename,
    )
    result.successful?
  rescue Aws::S3::Errors::ServiceError
    false
  end

  # Private: Process the export request in the background and report back to the
  # user when their export is ready for downloading.
  #
  # Returns nothing.
  def enqueue_process_export_results
    JobStatus.create(id: token)

    ProcessAuditLogExportJob.perform_later(id)
  end

  # Private: Determine if the subject is a user, organization, or business since
  # they are the only possible entities we can query for.
  #
  # Returns nothing.
  def subject_must_be_valid
    unless ["User", "Organization", "Business"].include?(subject_type)
      errors.add(:subject_type, "expected User, Organization, or Business")
    end
  end

  # Private: The unique token for the audit log used to download the export.
  #
  # Returns String.
  def generate_token
    self.token = SecureRandom.uuid
  end

  def set_default_format
    self.format ||= "json"
  end

  def event_payload
    payload = {
      actor: actor,
    }

    if phrase?
      payload[:phrase] = phrase
    end

    if subject.respond_to?(:event_prefix)
      payload[subject.event_prefix] = subject
    else
      raise ArgumentError, "#{subject} does not respond to #event_prefix"
    end

    payload
  end

  def increment_create_count
    GitHub.dogstats.increment("audit_log_export", tags: ["action:create", "format:#{format}"])
  end

  # Private: Ensures the remote file is deleted when the record is deleted. There
  # is a change it has already been deleted due to the bucket lifecycle having
  # deleted the file already.
  #
  # Returns nothing.
  def delete_remote_file
    remote_object.delete
  rescue Aws::S3::Errors::NoSuchKey
    # File doesn't exist on S3.
  end
end
