# frozen_string_literal: true

# Public: A response is created when a repo is added to a team, or permissions updated
class Team::ModifyRepositoryStatus

  # Internal: Successful statuses.
  SUCCESSFUL = [:dupe, :owners, :success]

  # Public: A Symbol. :success, :blocked, :dupe, or :org.
  attr_reader :status

  def initialize(status)
    @status = status
    freeze
  end

  private :initialize

  # Public: Did an error occur while adding a member?
  def error?
    !success?
  end

  # Public: Was a member successfully added?
  def success?
    SUCCESSFUL.include?(status)
  end

  # Public: Team already has access to repo.
  DUPE = new(:dupe)

  # Public: The team's org and the repo's org aren't the same.
  NOT_OWNED = new(:not_owned)

  # Public: Permission to give to repo was not specified.
  NO_PERMISSION = new(:no_permission)

  # Public: The Owners team has implicit access to all org repos.
  OWNERS = new(:owners)

  # Public: It's all good.
  SUCCESS = new(:success)

  # Direct Org Membership is not enabled
  NO_DIRECT_ORG = new(:no_direct_org)
end
