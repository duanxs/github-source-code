# rubocop:disable Style/FrozenStringLiteralComment

class Team
  # Modifies teams.
  class Editor
    def self.service
      GitHub.ldap_sync_enabled? ? LdapEditor : self
    end

    def self.update_team(team, group_mappings: nil, **attrs)
      service.new(team, attrs[:updater]).update(attrs.except!(:updater), group_mappings: group_mappings)
    end

    def initialize(team, updater)
      @team = team
      @updater = updater
    end

    def update(attributes, group_mappings:)
      updating_parent_team = @team.parent_team_id != attributes[:parent_team_id]
      @team.assign_attributes(Team.team_attributes_hash(attributes))
      @team.parent_team_id = attributes[:parent_team_id] if attributes.key?(:parent_team_id)

      if not_team_mappable?(team: @team, group_mappings: group_mappings)
        @team.errors.add(:base, "This team cannot be externally managed.")
        return false
      end

      return false unless created = @team.save

      if team_mappable?(team: @team, group_mappings: group_mappings)
        Team::GroupMapping.batch_update_mappings(@team, group_mappings, actor: @updater)
      end

      created
    rescue Team::ParentChange::CannotAcquireLockError
      GitHub.dogstats.increment "team.update.lock_already_acquired"
      @team.errors.add(:base, "Whoops! There was a problem updating the team. Please try again.")
    end

    private

    def not_team_mappable?(team:, group_mappings:)
      group_mappings.present? && !team.can_be_externally_managed?
    end

    def team_mappable?(team:, group_mappings:)
      !group_mappings.nil? && team.can_be_externally_managed?
    end
  end

  class LdapEditor < Editor
    def update(attributes, group_mappings: [])
      ldap_dn = attributes.delete(:ldap_dn)

      # Ajax updates don't send the ldap_dn
      # We ignore the group management to not remove the ldap mapping by mistake.
      return super if ldap_dn.nil?

      if ldap_dn.present?
        # Ignore LDAP DN if it's unchanged
        return super if @team.ldap_mapped? && @team.ldap_dn == ldap_dn

        if @team.legacy_owners?
          @team.errors.add :base, "Owners team cannot be mapped to an LDAP Group"
          return false
        end

        # If the org is not adminable by the person making the request
        # add the error statement and bail out before making an LDAP call
        #
        # They can still update other aspects but just not any LDAP mappings
         if !@team.organization.adminable_by?(@updater)
           if !@updater.site_admin?
             @team.add_non_owner_group_error
             return false
           end
         end

        group = GitHub::LDAP.search.find_group(ldap_dn)

        if group
          updated = super && @team.map_ldap_entry(group.dn)
          @team.enqueue_sync_job if updated
        else
          @team.add_invalid_group_error(ldap_dn)
        end
      else
        if updated = super
          updated = @team.ldap_mapping.destroy if @team.ldap_mapped?
        end

        @team.reload if updated # clear cached association
      end

      updated
    end
  end
end
