# rubocop:disable Style/FrozenStringLiteralComment

# Public: Check a user's ability to add, remove, change, or see a team.
module Team::Permissions

  # Public: Can a user become a member of this team? Teams mapped to an
  # external LDAP service can't be joined. Administrators can join any team.
  def joinable_by?(user)
    return false if ldap_mapped? || member?(user)
    adminable_by?(user)
  end

  # Public: Can a user's membership be removed? Users can't leave a team mapped
  # to an external LDAP service.
  def leavable_by?(user)
    return false if ldap_mapped?

    member?(user)
  end

  # Public: Can the given user see this team?
  #
  # Returns a Boolean.
  def visible_to?(user)
    async_visible_to?(user).sync
  end

  # Public: Can the given viewer see this team?
  #
  # This method resolves to true in the following situations:
  #   1. viewer is an IntegrationInstallation with read permissions on the
  #      organization's members or team discussions
  #   2. viewer is a Bot whose IntegrationInstallation has read permissions on
  #      the organization's members or team discussions
  #   4. viewer is a User and an org member and the team is not secret
  #   3. viewer is a User and a member of this team
  #   5. viewer is a User and an admin of the organization
  #
  # viewer - One of {IntegrationInstallation, Bot, (non-bot) User}
  #
  # Returns a Promise of a Boolean.
  def async_visible_to?(viewer)
    return Promise.resolve(false) unless viewer

    Platform::Loaders::IsTeamMemberCheck.load(viewer.id, id).then do |is_team_member|
      next true if is_team_member

      async_organization.then do |org|

        async_integration_permissons = Promise.all([
          Promise.resolve(false),
          Promise.resolve(false),
        ])
        if viewer.can_act_for_integration?
          async_integration_permissons = Promise.all([
            org.resources.members.async_readable_by?(viewer),
            org.resources.team_discussions.async_readable_by?(viewer),
          ])
        end

        async_integration_permissons.then do |can_read_members, can_read_team_discussions|
          next true if can_read_members || can_read_team_discussions

          # Is the viewer is an org member viewing a non-secret team, or is the viewer an org admin?
          # actor is a User and subject is an Organization
          ability = ::Ability.where(
            actor_id: viewer.id,
            actor_type: "User",
            subject_id: org.id,
            subject_type: "Organization",
          ).first

          ability && (ability.admin? || !secret?)
        end
      end
    end
  end

  def can_create_team_discussion?(actor)
    async_can_create_team_discussion?(actor).sync
  end

  # Can the given actor create a team discussion?
  #
  # actor - One of {IntegrationInstallation, Bot, (non-bot) User}
  #
  # Returns a Promise of a Boolean.
  def async_can_create_team_discussion?(actor)
    return Promise.resolve(false) unless actor

    async_organization.then do |org|
      org.async_team_discussions_allowed?.then do |discussions_allowed|
        next false unless discussions_allowed

        async_integration_can_create = \
          if actor.can_act_for_integration?
            org.resources.team_discussions.async_writable_by?(actor)
          else
            Promise.resolve(true)
          end

        Promise
          .all([async_visible_to?(actor), async_integration_can_create])
          .then(&:all?)
      end
    end
  end

  # Public: Can a user associate a repository with this team?
  def can_add_repositories?(user, allow_owners_team: false)
    return false if legacy_owners? && !allow_owners_team
    return true if adminable_by?(user)

    # Keep support for allowing members of legacy admin teams to add repos.
    admin? && member?(user)
  end

  # Public: get the permission this team has on the given repository.
  def permission_for(repo)
    return unless repo
    ability = Authorization.service.most_capable_ability_between(actor: self, subject: repo)
    Team::ABILITIES_TO_PERMISSIONS[ability.try(:action)]
  end

  # Internal: Overrides Ability::Subject#grant?, only allowing users.
  def grant?(actor, action)
    super && actor.is_a?(User) && actor.user?
  end

  # Internal: Overrides Ability::Subject#permit?, also allowing read access
  # to non-secret teams by actors who can read this team's organization.
  def permit?(actor, action)
    async_permit?(actor, action).sync
  end

  def async_permit?(actor, action)
    super.then do |result|
      if result
        true
      elsif !secret? && action == :read
        actor = actor.ability_delegate
        # TODO: Does this need to be async?
        organization.direct_member?(actor)
      else
        false
      end
    end
  end

  def can_have_granular_permissions?
    false
  end
end
