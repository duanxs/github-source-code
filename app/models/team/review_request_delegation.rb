# frozen_string_literal: true

class Team
  module ReviewRequestDelegation
    class DelegationError < StandardError; end

    class Result
      def self.success(delegates:)
        new(delegates: delegates)
      end

      def self.failure(error:)
        new(error: error)
      end

      attr_reader :delegates, :error

      def initialize(delegates: [], error: nil)
        @delegates = delegates
        @error = error
      end

      def success?
        error.nil?
      end
    end

    extend self

    DELEGATE_LIMIT = 10
    MEMBER_LIMIT = 1_000

    # Public: Delegate a team review request to one or more team members by a chosen algorithm.
    #
    # actor - The User-ish the delegation will be attributed to.
    # pull_request - The PullRequest to delegate a team review request on.
    # team - The Team that has had its review requested.
    # needed_reviewers - A Fixnum count of members to delegate to, between 1 and DELEGATE_LIMIT.
    #                    (Default: 1).
    # strategy - The Symbol algorithm for selecting delegates, either :round_robin or :load_balance.
    # count_existing_reviewers - A boolean, whether team members whose review has already been
    #                            requested should count against needed_reviewers. (Default: true).
    # remove_team_request - A boolean indicating whether the team review request should be removed.
    #                       (Default: true.)
    # window_in_days - For the :load_balance strategy, the window back in time to consider request load.
    #
    # Given a team review request on a pull request, will request one or more specific team
    # members to review that pull request, optionally removing the team request.
    #
    # Selection algorithms:
    #
    # - Round robin: Selected by when the members were last delegated to, least-recently first.
    # - Load balance: Selected by the number of review requests they have received in the past N days, least-first
    #
    # Elegible team members for selection:
    #
    #   - Aren't the PR author
    #   - Don't have an existing review request on the PR
    #   - That haven't left an approved and not-dismissed approval
    #   - Don't have a "busy" user status on the team's organization
    #
    # Returns a Result with delegated-to Users on success, or explantory error on failure.
    def delegate_to_members(actor:, pull_request:, team:, strategy:, needed_reviewers: 1, count_existing_reviewers: true, remove_team_request: true, window_in_days: 30)
      return if !pull_request.review_requested_for?(team)

      result = \
        case strategy
        when :round_robin
          round_robin_delegates(
            pull_request: pull_request,
            team: team,
            needed_reviewers: needed_reviewers,
            count_existing_reviewers: count_existing_reviewers,
          )
        when :load_balance
          load_balance_delegates(
            pull_request: pull_request,
            team: team,
            needed_reviewers: needed_reviewers,
            count_existing_reviewers: count_existing_reviewers,
            window_in_days: window_in_days,
          )
        else
          raise ArgumentError.new("Unknown delegation strategy: #{strategy}")
        end

      return result if !result.success?
      return result if result.delegates.empty?

      pending_review_requests = pull_request.review_requests.pending.preload(:reviewer)
      reviewers = pending_review_requests.map(&:reviewer)

      if remove_team_request
        team_review_request = pending_review_requests.find { |rr| rr.reviewer == team }
        if team_review_request && pull_request.review_request_removable?(team_review_request)
          reviewers -= [team]
        end
      end

      reviewers.concat(result.delegates).uniq!

      pull_request.request_review_from(
        actor: actor,
        reviewers: reviewers,
        limit: reviewers.count,
        should_save: true,
        via_delegation: true,
      )

      params = {
        organization: team.organization,
        team: team,
        repository: pull_request.repository,
        pull_request: pull_request,
        algorithm: strategy,
      }
      GlobalInstrumenter.instrument("pull_request.review_request_delegated", params)

      result
    end

    # Internal: Round robin selection of team members for review delegation.
    #
    # pull_request - The PullRequest to delegate a team review request on.
    # team - The Team that has had its review requested.
    # needed_reviewers - A Fixnum count of members to delegate to, between 1 and DELEGATE_LIMIT.
    #                    (Default: 1).
    # count_existing_reviewers - Whether team members whose review has already been requested
    #                            should count against needed_reviewers. (Default: true).
    #
    # Selection algorithm:
    #
    # - Pick team members that:
    #     - Aren't the PR author
    #     - Don't have an existing review request on the PR
    #     - That haven't left an approved and not-dismissed approval
    #     - Don't have a "busy" user status on the team's organization
    # - Ordered by the when they were last delegated to, least-recently first.
    # - Limited to the max needed number of reviewers, M
    #
    # Returns a Result with a User Relation scoped to the candidate delegates found on success, or explantory error on failure.
    def round_robin_delegates(pull_request:, team:, needed_reviewers: 1, count_existing_reviewers: true)
      find_candidate_delegates_using_sort(pull_request, team, needed_reviewers, count_existing_reviewers) do |candidate_ids|
        delegated_at_by_member_id = TeamMemberDelegatedReviewRequest.where(
          team_id: team.id,
          member_id: candidate_ids,
        ).pluck(:delegated_at, :member_id).index_by { |(_, id)| id }

        # Sort by least-recent delegation first. Nil means the user has
        # never been delegated to, so sort nil values earliest.
        candidate_ids.sort do |id1, id2|
          id1_delegated_at = delegated_at_by_member_id[id1]
          id2_delegated_at = delegated_at_by_member_id[id2]
          if id1_delegated_at.nil?
            next 0 if id2_delegated_at.nil?
            next -1
          end
          next 1 if id2_delegated_at.nil?
          id1_delegated_at <=> id2_delegated_at
        end
      end
    end

    # Internal: Load balanced selection of team members for review delegation.
    #
    # pull_request - The PullRequest to delegate a team review request on.
    # team - The Team that has had its review requested.
    # needed_reviewers - A Fixnum count of members to delegate to, between 1 and DELEGATE_LIMIT.
    #                    (Default: 1).
    # count_existing_reviewers - Whether team members whose review has already been requested
    #                            should count against needed_reviewers. (Default: true).
    # window_in_days - The number of days before now to consider request volume for load balancing.
    #
    # Selection algorithm:
    #
    # - Pick team members that:
    #     - Aren't the PR author
    #     - Don't have an existing review request on the PR
    #     - That haven't left an approved and not-dismissed approval
    #     - Don't have a "busy" user status on the team's organization
    # - Ordered by the number of review requests they have received in the past N days, least-first
    # - Limited to the max needed number of reviewers, M
    #
    # Returns a Result with a User Relation scoped to the candidate delegates found on success, or explantory error on failure.
    def load_balance_delegates(pull_request:, team:, needed_reviewers: 1, count_existing_reviewers: true, window_in_days: 30)
      find_candidate_delegates_using_sort(pull_request, team, needed_reviewers, count_existing_reviewers) do |candidate_ids|
        next [] if candidate_ids.empty?

        ordered_candidate_ids_with_requests = ReviewRequest.github_sql.values(<<~SQL, reviewer_ids: candidate_ids, org_id: team.organization_id, since: (Time.current - window_in_days.days), limit: DELEGATE_LIMIT)
          SELECT rr.reviewer_id
          FROM review_requests rr
            INNER JOIN pull_requests pr ON rr.pull_request_id = pr.id
            INNER JOIN repositories r ON pr.repository_id = r.id AND r.organization_id = :org_id
          WHERE rr.reviewer_id IN :reviewer_ids AND rr.created_at > :since
          GROUP by rr.reviewer_id
          ORDER BY COUNT(rr.reviewer_id), rr.reviewer_id
          LIMIT :limit
        SQL

        # The INNER JOIN above means we we'll lose the ids of any members that haven't yet
        # received review requests. Below we'll prepend any lost ids to the query results.

        candidate_ids_with_zero_requests = candidate_ids - ordered_candidate_ids_with_requests

        candidate_ids_with_zero_requests.sort!

        candidate_ids_with_zero_requests.concat(ordered_candidate_ids_with_requests)
      end
    end


    # Private: Finds a set of team members who could be delegated to from a given sorting routine.
    #
    # pull_request - The PullRequest to delegate a team review request on.
    # team - The Team that has had its review requested.
    # needed_reviewers - A Fixnum count of members to delegate to, between 1 and DELEGATE_LIMIT.
    # count_existing_reviewers - Whether team members whose review has already been requested
    #                            should count against needed_reviewers.
    # block - A Proc object taking an Array of member ids and returning a sorted Array from
    #         those ids, ordered by priority; the first needed_reviewers will be the delegates.
    #
    # Returns a Result with a User Relation scoped to the candidate delegates found on success, or explantory error on failure.
    private def find_candidate_delegates_using_sort(pull_request, team, needed_reviewers, count_existing_reviewers, &block)
      if needed_reviewers > DELEGATE_LIMIT || needed_reviewers < 1
        raise ArgumentError.new("Between 1 and #{DELEGATE_LIMIT} reviewers may be delegated to. Requested: #{needed_reviewers}.")
      end

      requested_reviewer_ids = pull_request.review_requests.not_dismissed.type_users.pluck(:reviewer_id)
      excluded_member_ids = ReviewRequestDelegationExcludedMember.where(team_id: team.id).pluck(:user_id)
      approver_ids = pull_request.reviews.where(state: PullRequestReview.state_value(:approved)).pluck(:user_id)

      member_ids = team.descendant_or_self_member_ids.take(MEMBER_LIMIT)

      candidate_member_ids = member_ids - ([pull_request.user_id] + requested_reviewer_ids + approver_ids + excluded_member_ids)

      if count_existing_reviewers
        requested_member_ids = requested_reviewer_ids & member_ids
        needed_reviewers -= requested_member_ids.length

        if needed_reviewers < 1
          return Result.success(delegates: [])
        end
      end

      if candidate_member_ids.empty?
        return Result.failure(error: DelegationError.new("No team members were available for review."))
      end

      active_candidate_member_ids = Team.github_sql.values(<<-SQL, team_id: team.id, candidate_member_ids: candidate_member_ids, now: Time.current)
        SELECT u.id
        FROM users u
        LEFT JOIN user_statuses us
          ON u.id = us.user_id AND us.limited_availability = 1
          AND (us.expires_at > :now OR us.expires_at IS NULL)
        WHERE u.id IN :candidate_member_ids
          AND us.id IS NULL
      SQL

      delegate_ids = yield(active_candidate_member_ids).first(needed_reviewers)

      if delegate_ids.empty?
        return Result.failure(error: DelegationError.new("No team members were available for review."))
      end

      delegated_at = Time.current
      delegate_ids.each do |id|
        TeamMemberDelegatedReviewRequest.upsert(
          team_id: team.id,
          member_id: id,
          delegated_at: delegated_at,
        )
      end

      return Result.success(delegates: User.where(id: delegate_ids))
    end
  end
end
