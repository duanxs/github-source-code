# frozen_string_literal: true
module Team::Destruction

  # When a set of teams is removed in bulk, we need to destroy their dependants
  # A common case in which we destroy teams in bulk is when we remove a team
  # in that has descendants. In that situation, all the dependants
  # for the team being destroyed and their descendants need to be destroyed.
  #
  # Removing team dependants imply:
  # - Destroying their ExternalGroup
  # - Destroying their TeamMembershipRequests
  # - Destroying their pending TeamInvitations
  # - Destroying their LDAP Mappings
  # - Destroying their discussion posts
  # - Destroying their subscriptions
  # - Revoking their users' permissions, which in turn includes:
  #  - clearing the team's abilities
  #  - sending webhook to inform integrators about the members being removed from the team
  #  - enqueing jobs to clear membership information, like assigned issues, watched
  #    repositories, etc. For those repos that were owned by the team. See `Jobs::ClearTeamMemberships`
  #  - sending emails to the users to inform them about being removed from the team
  #  - clearing fork information
  #
  class DestroyDependantsOperation

    TEAM_BATCH_SIZE   = 20
    MEMBER_BATCH_SIZE = 100

    attr_reader :org_id, :team_ids_to_info, :throttler, :org

    # Creates a new instance of this command object.
    #
    # Params:
    #
    #  - org_id: the organization owning the teams which dependants are going to be destroyed.
    #  - team_ids_to_info: is a hash where the keys are the team ids to be destroyed and the values
    #    is some of the team's information that's needed to perform the action. This information has to
    #    be provided, as the teams themselves were deleted already if this was invoked from
    #    Team::Destruction::DestroyOperation. As numeric and symbol values in hashes are serialized as
    #    strings, this constructor also converts the keys to integers and the values's keys to symbols.
    #    An example value for `team_ids_to_info` is:
    #
    #     {
    #      "9381" => {"name" => "Engineering", "legacy_owners" => true },
    #      "6173" => {"name" => "Platform", "legacy_owners" => false }
    #     }
    #  - throttler: optional the throttler to be used. Defaults to the default throttler.
    #  If this was invoked whithin the request lifecycle, the GitHub::Thottler.null should
    #  be provided
    #
    def initialize(org_id, team_ids_to_info)
      @org_id = org_id
      @org = Organization.find_by_id(org_id)
      @team_ids_to_info = Hash[team_ids_to_info.map { |k, v| [k.to_i, v.symbolize_keys] }]
    end

    def execute
      all_team_ids = team_ids_to_info.keys

      all_team_ids.each_slice(TEAM_BATCH_SIZE) do |team_ids|
        team_ids_to_info_slice = team_ids_to_info.slice(*team_ids)

        measuring(:instrument_destruction, org, team_ids_to_info_slice)
        measuring(:destroy_ldap_mappings, team_ids) if GitHub.enterprise?
        measuring(:destroy_group_mappings, team_ids)
        measuring(:destroy_membership_requests, team_ids)
        measuring(:destroy_requests_to_parent, team_ids)
        measuring(:destroy_requests_to_be_child, team_ids)
        measuring(:destroy_team_invitations, org_id, team_ids)
        measuring(:revoke_user_permissions, org, team_ids, team_ids_to_info_slice)
        measuring(:destroy_discussion_posts, team_ids)
        measuring(:destroy_subscriptions, team_ids)
        measuring(:destroy_review_requests, team_ids)
        measuring(:destroy_user_roles, team_ids)
      end
    end

    private

    def measuring(operation, *args, &block)
      GitHub.dogstats.time("team.destruction.destroy_dependants_operation.duration", tags: ["operation:#{operation}"]) do
        send(operation, *args, &block)
      end
    end

    def instrument_destruction(org, team_ids_to_info)
      return unless org
      team_ids_to_info.each do |team_id, info|
        combined_slug = "#{org.login}/#{info[:slug]}"
        GitHub.instrument("team.destroy", {
          team: combined_slug,
          team_id: team_id,
          org: org,
          ldap_mapped: info[:ldap_mapped],
          note: "Team #{combined_slug}",
        })
      end
    end

    def destroy_group_mappings(team_ids)
      Team::GroupMapping.throttle do
        Team::GroupMapping.where(team_id: team_ids).destroy_all
      end
    end

    def destroy_membership_requests(team_ids)
      TeamMembershipRequest.throttle do
        TeamMembershipRequest.where(team_id: team_ids).destroy_all
      end
    end

    def destroy_requests_to_parent(team_ids)
      TeamChangeParentRequest.throttle do
        TeamChangeParentRequest.where(parent_team_id: team_ids).destroy_all
      end
    end

    def destroy_requests_to_be_child(team_ids)
      TeamChangeParentRequest.throttle do
        TeamChangeParentRequest.where(child_team_id: team_ids).destroy_all
      end
    end

    def destroy_team_invitations(org_id, team_ids)
      TeamInvitation.throttle do
        TeamInvitation.joins(:organization_invitation)
          .where("organization_invitations.organization_id": org_id, team_id: team_ids)
          .where("organization_invitations.accepted_at": nil, "organization_invitations.cancelled_at": nil).destroy_all
      end
    end

    def destroy_ldap_mappings(team_ids)
      LdapMapping.throttle do
        LdapMapping.where(subject_id: team_ids, subject_type: "Team").destroy_all
      end
    end

    def destroy_user_roles(team_ids)
      UserRole.throttle do
        UserRole.where(actor_id: team_ids, actor_type: "Team").destroy_all
      end
    end

    def revoke_user_permissions(org, team_ids, team_ids_to_info)
      team_ids_to_user_abilities = ::Ability.where(
        actor_type: "User",
        subject_id: team_ids,
        subject_type: "Team",
        priority: ::Ability.priorities[:direct],
      ).group_by(&:subject_id)

      team_ids_to_repo_abilities = ::Ability.where(
        actor_type: "Team",
        actor_id: team_ids,
        subject_type: "Repository",
        priority: ::Ability.priorities[:direct],
      ).group_by(&:actor_id)

      measuring(:clear_abilities, team_ids)

      team_ids_to_info.each do |team_id, team_info|
        member_ids = (team_ids_to_user_abilities[team_id] || []).map(&:actor_id).uniq
        repo_ids = (team_ids_to_repo_abilities[team_id] || []).map(&:subject_id).uniq
        measuring(:queue_fork_cleanup, repo_ids, member_ids)
        next unless org

        team_name     = team_info[:name]
        legacy_owners = team_info[:legacy_owners]
        User.where(id: member_ids).find_each(batch_size: MEMBER_BATCH_SIZE) do |member|
          next unless member
          measuring(:send_hookshot_event, org, member.id, member.login, team_id, team_name)
          measuring(:queue_clear_team_memberships, org, member.id, repo_ids)
          measuring(:send_removal_notification, org, member, team_id, team_name, repo_ids, legacy_owners)
        end
      end
    end

    def destroy_discussion_posts(team_ids)
      DiscussionPost.throttle do
        DiscussionPost.where(team_id: team_ids).destroy_all
      end
    end

    def destroy_review_requests(team_ids)
      ReviewRequest.throttle do
        ReviewRequest.where(reviewer_id: team_ids, reviewer_type: "Team").destroy_all
      end
    end

    def destroy_subscriptions(team_ids)
      team_ids.each do |team_id|
        # Newsies needs teams with the correct id, but the team has been destroyed
        # so instantiate them temporarily to pass to newsies
        team = Team.new.tap { |t| t.id = team_id }
        GitHub.newsies.async_delete_all_for_list(team)
      end
    end

    def clear_abilities(team_ids)
      Authorization.service.clear_abilities_for_multiple_participants(participant_type: Team, participant_ids: team_ids)
    end

    def send_hookshot_event(org, member_id, member_login, team_id, team_name)
      Hook::Event::MembershipEvent.queue(action: :removed,
                                         member_id: member_id,
                                         member_login: member_login,
                                         team_id: team_id,
                                         team_name: team_name,
                                         organization_id: org.id,
                                         actor_id: GitHub.context[:actor_id])
    end

    def queue_clear_team_memberships(org, member_id, repo_ids)
      Team.queue_clear_team_memberships(repo_ids, org.id, { user: member_id })
    end

    def send_removal_notification(org, member, team_id, team_name, repo_ids, legacy_owners)
      return unless repo_ids.any?

      # Do not change to #deliver_later, this mailer is currently run in the
      # DestroyTeamDependants job
      OrganizationMailer.removed_from_team(
        member,
        team_name,
        org,
        legacy_owner: !!legacy_owners,
        team_destroyed: true,
      ).deliver_now
    end

    def queue_fork_cleanup(repo_ids, member_ids)
      Team.queue_fork_cleanup(repo_ids, member_ids)
    end
  end
end
