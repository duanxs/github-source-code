# rubocop:disable Style/FrozenStringLiteralComment

# This class handles Repository and Gist country blocking and is used by
# GitRepositoryAccess.
#
# See app/models/git_repository_access.rb for more information.
class GitRepositoryBlock

  COUNTRY_BLOCK_TTL = 10.minutes

  UNBLOCKED_REPO = {}.freeze

  COUNTRY_BLOCK_TYPES = {
    "chinese_internet_blacklist" => ["CN"],
    "russian_internet_blacklist" => ["RU"],
    "spanish_internet_blacklist" => ["ES"],
    "turkish_internet_blacklist" => ["TR"],
    "us_trade_restrictions"      => ["CU", "IR", "KP", "SD", "SY"],
  }

  COUNTRY_BLOCK_DESCRIPTIONS = {
    "chinese_internet_blacklist" => "Chinese Internet Blacklist",
    "russian_internet_blacklist" => "Russian Internet Blacklist",
    "spanish_internet_blacklist" => "Spanish Internet Blacklist",
    "turkish_internet_blacklist" => "Turkish Internet Blacklist",
    "us_trade_restrictions"      => "U.S. Trade Restrictions",
  }

  COUNTRY_DESCRIPTIONS = {
    "CN" => "China",
    "CU" => "Cuba",
    "ES" => "Spain",
    "IR" => "Iran",
    "KP" => "North Korea",
    "SD" => "Sudan",
    "SY" => "Syria",
    "RU" => "Russia",
    "TR" => "Turkey",
  }

  def initialize(git_repository, type)
    @git_repository = git_repository
    @type = type
  end

  # Check if country block is enabled
  # block - the named country block to check
  def country_block_setup?(block)
    country_blocks.has_key?(block)
  end

  def blocked_countries
    country_blocks.map do |block, url|
      COUNTRY_BLOCK_TYPES[block]
    end.flatten.join(" ")
  end

  def country_block?(country)
    country_blocks.keys.find do |block|
      COUNTRY_BLOCK_TYPES[block].include?(country)
    end
  end

  def country_block_url(country)
    country_blocks.each do |block, url|
      if COUNTRY_BLOCK_TYPES[block].include?(country)
        return url
      end
    end
    nil
  end

  # Disable access based on countries.
  #
  # user - staff user record
  # block - the block that needs to be applied
  # url - URL of the public block notice, current placed on github/nation-state-blocks
  def country_block(user, block, url)
    unless COUNTRY_BLOCK_DESCRIPTIONS.has_key?(block)
      @git_repository.errors.add(:base, "invalid country block")
      return false
    end

    if country_block_setup?(block)
      @git_repository.errors.add(:base, "block already exists for #{COUNTRY_BLOCK_DESCRIPTIONS[block]}")
      return false
    end

    if @git_repository.private?
      @git_repository.errors.add(:base, "can not country block a private #{git_repo_full_name}")
      return false
    end

    begin
      DisabledAccessReason.new do |reason|
        reason.flagged_item_id = @git_repository.id
        reason.flagged_item_type = @type
        reason.country_block = block
        reason.country_block_url = url
        reason.disabled_at = Time.now
        reason.disabled_by = user
      end.save!
    rescue ActiveRecord::RecordInvalid => e
      Failbot.report Error.new(e.message)
      return false
    end

    self.class.expire_country_block_cache

    if @git_repository.is_a?(Repository)
      RepositoryMailer.country_block_notice(@git_repository, block, url).deliver_later
    end

    auditing_actor = GitHub.guarded_audit_log_staff_actor_entry(user)

    GitHub.instrument "staff.country_block", auditing_actor.merge(
      git_repo_stat_key => @git_repository,
      :user => @git_repository.owner,
      :block => block)

    true
  end

  # Removes a country block
  def remove_country_block(user, block)
    records = DisabledAccessReason.where(country_block: block,
      flagged_item_id: @git_repository.id, flagged_item_type: @type)
    records.destroy_all

    self.class.expire_country_block_cache

    auditing_actor = GitHub.guarded_audit_log_staff_actor_entry(user)

    GitHub.instrument "staff.country_unblock", auditing_actor.merge(
      git_repo_stat_key => @git_repository,
      :user => @git_repository.owner,
      :block => block)

    true
  end

  def country_blocks
    key = self.class.country_block_key(@type, @git_repository.id)
    self.class.all_country_blocks[key] || UNBLOCKED_REPO
  end

  def self.expire_country_block_cache
    remove_instance_variable(:@all_country_blocks_expires_at) if defined?(@all_country_blocks_expires_at)
    remove_instance_variable(:@all_country_blocks) if defined?(@all_country_blocks)
  end

  def self.all_country_blocks
    if defined?(@all_country_blocks_expires_at) && Time.now.utc < @all_country_blocks_expires_at
      return @all_country_blocks
    end

    @all_country_blocks = DisabledAccessReason.where("country_block IS NOT NULL").each_with_object({}) do |record, memo|
      key = country_block_key(record.flagged_item_type, record.flagged_item_id)
      memo[key] ||= {}
      memo[key][record.country_block] = record.country_block_url
    end

    @all_country_blocks_expires_at = Time.now.utc + COUNTRY_BLOCK_TTL
    @all_country_blocks
  end

  def self.country_block_key(type, id)
    "#{type}_#{id}"
  end

  private

  # Returns the first four of the git repository's class
  # as a symbol.  For example:
  # Repository -> :repo
  # Gist -> :gist
  def git_repo_stat_key
    git_repo_full_name.first(4).to_sym
  end

  # Returns the git repository's class as a lower
  # case string.
  def git_repo_full_name
    @git_repository.class.name.downcase
  end
end
