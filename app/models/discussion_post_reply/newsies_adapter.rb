# frozen_string_literal: true

# This module implements methods expected (often without clear interface specification) by Newsies.
module DiscussionPostReply::NewsiesAdapter
  extend ActiveSupport::Concern

  # Newsies::Emails::Message assumes this exists.
  def message_id
    "<#{team.name_with_owner}/discussions/#{discussion_post.number}/comments/#{number}/" +
    "@#{GitHub.urls.host_name}>"
  end

  # Newsies::Emails::Message assumes this exists.
  def permalink(include_host: true)
    "#{discussion_post.permalink(include_host: include_host)}/comments/#{number}"
  end
end
