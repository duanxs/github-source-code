# frozen_string_literal: true

require "grpc"
require "github/launch_client"

class Actions::Runner
  ONLINE = "online"
  OFFLINE = "offline"
  IDLE = "idle"
  ACTIVE = "active"

  SYSTEM_LABEL_TYPE = "system"
  CUSTOM_LABEL_TYPE = "user"

  attr_reader :id, :name, :os

  def self.for_entity(entity)
    resp = GrpcHelper.rescue_from_grpc_errors("SelfHostedRunners") do
      GitHub::LaunchClient::SelfHostedRunners.list_runners(entity)
    end

    from_grpc_collection Array(resp&.value&.runners)
  end

  def self.with_ids(ids, entity:)
    ids.map!(&:to_i)

    for_entity(entity).filter do |runner|
      ids.include? runner.id
    end
  end

  def self.from_grpc_collection(entities)
    entities.map { |entity| from_grpc_object(entity) }
  end

  def self.from_grpc_object(entity)
    new(
      id: entity.id,
      name: entity.name,
      os: entity.os,
      status: entity.status,
      current_parallelism: entity.current_parallelism,
      labels: entity.labels,
    )
  end

  def initialize(id:, name:, os:, status:, current_parallelism: 0, labels: [])
    @id = id
    @name = name
    @os = os
    @status = status
    @current_parallelism = current_parallelism
    @labels = labels
  end

  def status
    if @status == ONLINE
      @current_parallelism.zero? ? IDLE : ACTIVE
    else
      OFFLINE
    end
  end

  def offline?
    status == OFFLINE
  end

  def system_labels
    @labels.select { |label| label.type == SYSTEM_LABEL_TYPE }
  end

  def custom_labels
    @labels.select { |label| label.type == CUSTOM_LABEL_TYPE }
  end

  def custom_label_ids
    custom_labels.map(&:id)
  end

  def label_ids
    @labels.map(&:id)
  end
end
