# frozen_string_literal: true

# Actions-specific functionality for check suites (AKA workflow runs)

module CheckSuite::ActionsDependency
  extend ActiveSupport::Concern

  include CheckSuite::NewsiesAdapter
  class ExpiredWorkflowRunError < CheckSuite::NotRerequestableError; end

  # Events that can trigger workflows, see also
  # https://github.com/github/launch/blob/07429378c3d40c18bef6e9216547191f26879905/flow/flowevents/eventtypes.go#L80-L110

  # Webhook events the Actions app reacts to
  ACTIONS_WEBHOOK_EVENTS = [
    "check_run",
    "check_suite",
    "create",
    "delete",
    "deployment_status",
    "deployment",
    "fork",
    "gollum",
    "issue_comment",
    "issues",
    "label",
    "milestone",
    "page_build",
    "project_card",
    "project_column",
    "project",
    "public",
    "pull_request_review_comment",
    "pull_request_review",
    "pull_request",
    "push",
    "registry_package",
    "release",
    "repository_dispatch",
    "status",
    "watch",
    "workflow_dispatch",
    "workflow_run",
  ].freeze

  # Overall list of events that can occur in relation to Actions, these are webhook events
  # plus some "virtual" events like `schedule`
  ACTIONS_EVENTS = ACTIONS_WEBHOOK_EVENTS | [
    "schedule",
    "pull_request_target",
  ].freeze

  VISIBLE_ACTIONS_EVENTS = ["push", "pull_request", "pull_request_review", "pull_request_target", "deployment"].freeze

  included do
    scope :for_workflow, ->(name) { where(name: name) }
  end

  def actions_app?
    return false unless github_app
    github_app.launch_github_app? || github_app.launch_lab_github_app?
  end

  def workflow_name
    name.present? ? name : Actions::Workflow::PLACEHOLDER_NAME
  end

  def notify_workflow_run(workflow_run_id: , creator: , action:)
    # These events are enqueued only if the creator is GitHub Actions.
    if actions_app?
      GitHub.instrument("workflow_run.status_changed", {
        run_id: workflow_run_id,
        action: action,
      })
    end
  end

  def expired_workflow_run?
    actions_app? && created_at < 1.month.ago
  end

  private

  def deliver_notifications
    if deliver_notifications?
      GitHub.newsies.trigger(
        CheckSuiteEventNotification.new(self),
        recipient_ids: notification_recipients.map(&:id),
        reason: :ci_activity,
        event_time: updated_at || created_at,
      )
      instrument :notification_triggered, conclusion: conclusion
    end
  end

  def deliver_notifications?
    workflow_run? &&
      notification_recipients.any? &&
      !merging_upstream_into_fork? &&
      completed_with_conclusion_change?
  end

  # Is the head repo actually the parent of the base repo?
  # If so, this means that we're dealing with changes coming from upstream into
  # a fork. In other words, the base repo is actually a fork of the head repo.
  def merging_upstream_into_fork?
    head_repository_id && (head_repository_id == repository.parent_id)
  end

  def notification_recipients
    [creator].select do |user|
      user && !user.ghost? && !user.bot?
    end
  end

  def workflow_run?
    actions_app? && GitHub.actions_enabled?
  end
end
