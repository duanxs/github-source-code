# frozen_string_literal: true

# This module implements methods expected by Newsies.
module CheckSuite::NewsiesAdapter
  # Associates check suite updates with the repo it belongs to so that
  # subsequent updates will refer to the same repo.
  def async_notifications_list
    async_repository
  end

  def notifications_list
    async_notifications_list.sync
  end

  # Refer to itself as the thread so that subsequent updates will refer to the
  # same check suite object.
  def notifications_thread
    self
  end
end
