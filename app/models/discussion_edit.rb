# frozen_string_literal: true

class DiscussionEdit < ApplicationRecord::Domain::Discussions
  include UserContentEdit::Core
  include FilterPipelineHelper
  include Instrumentation::Model

  belongs_to :discussion, required: true

  alias_attribute :user_content_id, :discussion_id
  alias_attribute :user_content, :discussion
  alias_attribute :async_user_content, :async_discussion

  def user_content_type
    "Discussion"
  end

  def global_relay_id
    Platform::Helpers::NodeIdentification.to_global_id(platform_type_name, "DiscussionEdit:#{id}")
  end
end
