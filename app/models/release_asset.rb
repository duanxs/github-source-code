# rubocop:disable Style/FrozenStringLiteralComment

class ReleaseAsset < ApplicationRecord::Domain::Assets
  areas_of_responsibility :releases
  include GitHub::Relay::GlobalIdentification
  include ::Storage::Uploadable
  include SlottedCounterService::Countable

  set_uploadable_policy_path :releases
  add_uploadable_policy_attributes :repository_id, :release_id, :label, :deletion_candidates

  belongs_to :repository
  belongs_to :release
  belongs_to :uploader, class_name: "User", foreign_key: :uploader_id
  belongs_to :storage_blob, class_name: "Storage::Blob"

  before_validation :denormalize_release, on: :create
  validates_presence_of :repository_id, :release_id
  validate :storage_ensure_inner_asset
  validate :uploader_access, on: :create
  validate :extension_is_allowed
  validate :content_type_is_allowed
  validate :name_is_unique

  # S3 limit (w/o multipart upload) is 5GB
  # mysql column limit is 2GB
  validates_inclusion_of :size, in: 1..2.gigabytes

  before_create :choose_storage_provider
  before_save :destroy_duplicate, if: :replacing_asset
  before_destroy :storage_delete_object
  before_validation :set_guid, on: :create

  delegate :tag_name, to: :release

  enum state: Storage::Uploadable::STATES

  # BEGIN storage settings

  def registry_package_version
    Platform::LoaderTracker.ignore_association_loads { super }
  end

  def upload_access_grant(uploader)
    Api::AccessControl.access_grant(
      verb: :edit_release,
      user: uploader,
      repo: repository,
      resource: release,
    )
  end

  def storage_policy(actor: nil, repository: nil)
    klass = if GitHub.storage_cluster_enabled?
      ::Storage::ClusterPolicy
    else
      ::Storage::S3Policy
    end
    klass.new(self, actor: actor, repository: self.repository)
  end

  def self.storage_new(uploader, blob, meta)
    new(
      storage_blob: blob,
      uploader: uploader,
      content_type: meta[:content_type],
      name: meta[:name],
      size: meta[:size],
      release_id: meta[:release_id],
      label: meta[:label],
    )
  end

  def self.storage_create(uploader, blob, meta)
    storage_new(uploader, blob, meta).tap do |file|
      file.update(state: :uploaded)
    end
  end

  def storage_blob_accessible?
    uploaded?
  end

  def storage_uploadable_attributes
    label.blank? ? {} : {label: label}
  end

  def creation_url
    "#{GitHub.storage_cluster_url}/releases/#{release_id}/files"
  end

  # cluster settings

  def storage_external_url(_ = nil)
    permalink
  end

  def storage_api_url
    "#{GitHub.api_url}/repositories/#{repository_id}/releases/assets/#{id}"
  end

  def storage_cluster_url(policy)
    creation_url + "/#{id}"
  end

  def storage_cluster_download_token(policy)
    super unless !GitHub.private_mode_enabled? && repository.public? && release.published?
  end

  def storage_download_path_info(policy)
    "#{storage_upload_path_info(policy)}/#{id}"
  end

  def storage_upload_path_info(policy)
    "/internal/storage/releases/#{release_id}/files"
  end

  def storage_download_content_type
    served_content_type || DOWNLOAD_CONTENT_TYPE
  end

  def alambic_download_headers(options = nil)
    {
      "Content-Disposition" => served_content_disposition,
    }
  end

  # s3 storage settings

  def self.storage_s3_bucket
    GitHub.s3_environment_config[:asset_bucket_name]
  end

  def self.storage_s3_new_bucket
    "github-#{Rails.env.downcase}-release-asset-2e65be"
  end

  def storage_s3_bucket
    if storage_provider == :s3_production_data
      self.class.storage_s3_new_bucket
    else
      self.class.storage_s3_bucket
    end
  end

  def storage_s3_key(policy)
    if storage_provider == :s3_production_data
      "#{repository_id}/#{guid}"
    else
      "#{GitHub.release_asset_base_path}/#{repository_id}/#{guid}#{self.class.asset_extension(self)}"
    end
  end

  def storage_s3_access_key
    if storage_provider == :s3_production_data
      GitHub.s3_production_data_access_key
    else
      GitHub.s3_environment_config[:access_key_id]
    end
  end

  def storage_s3_secret_key
    if storage_provider == :s3_production_data
      GitHub.s3_production_data_secret_key
    else
      GitHub.s3_environment_config[:secret_access_key]
    end
  end

  def storage_s3_download_query(query)
    query["response-content-disposition"] = served_content_disposition
    query["response-content-type"] = served_content_type || DOWNLOAD_CONTENT_TYPE
  end

  def storage_s3_upload_header
    {
      "Content-Type" => "application/octet-stream",
    }
  end

  def storage_upload_expiration
    12.hours
  end

  def storage_transition_ready?
    repository && release && storage_blob_accessible?
  end

  # END storage settings

  def storage_policy_api_url
    "/repositories/%d/releases/assets/%d" % [repository_id, id]
  end

  # Needed for test/lib/github/transitions/20151211173128_enterprise_storage_cluster_upgrade_test.rb
  def alambic_absolute_local_path
    parts = [GitHub.file_asset_path, GitHub.release_asset_base_path]
    parts.push *("%08d" % repository_id).scan(/..../)
    parts << (guid + asset_extension)
    parts * "/"
  end

  def self.for(repository, releases)
    return [] if !(repository && releases.present?)
    return [] if releases.any? { |rel| rel.repository_id != repository.id }

    uploaded.where(release_id: releases.map(&:id))
  end

  # Keeps the file extension.  We only care because S3 stores the file with the
  # extension.  When ReleaseAssets are moved to Alambic, we can remove this
  # code.
  def name=(value)
    value_s = value.to_s
    if (old_name = self[:name]).present?
      old_ext = File.extname(old_name)
      if !value_s.downcase.end_with?(old_ext.downcase)
        value_s += old_ext
      end
    end

    @original_name = value_s
    write_attribute :name, sanitize_file_name(value_s)
  end

  # Public: Shows either the asset's label or its filename.
  def display_name
    label.present? ? label : name
  end

  # exposed through the api
  def downloadable_content_type
    if ctype = served_content_type
      ctype
    elsif (ctype = content_type).present?
      ctype
    else
      DOWNLOAD_CONTENT_TYPE
    end
  end

  # controls content type for s3 downloads
  def served_content_type
    CONTENT_TYPE_OVERRIDES[File.extname(name.to_s)]
  end

  def served_content_disposition
    prefix = CONTENT_DISPOSITION_OVERRIDES[File.extname(name.to_s)]
    prefix ||= DEFAULT_CONTENT_DISPOSITION
    if prefix.present?
      "#{prefix} #{filename_content_disposition}"
    else
      filename_content_disposition
    end
  end

  # Public: Absolute permalink URL for this release.
  #
  # Returns a String
  def permalink
    return nil unless repository && release && tag_name

    pieces = tag_name.to_s.split("/")
    pieces.map! { |s| CGI.escape(s) }
    escaped_tag = pieces.join("/")

    "#{repository.permalink}/releases/download/#{escaped_tag}/#{CGI.escape name.to_s}"
  end

  def uploaded=(value)
    self.state = value ? :uploaded : :starter
    write_attribute :uploaded, value
  end

  def downloads
    slotted_count_with :downloads
  end

  def download
    instrument :download, size: size
    slotted_count!(:downloads)
  end

  include Instrumentation::Model
  def event_prefix() :release_assets end

  def denormalize_release
    self.repository_id = release.repository_id if release
  end

  def uploadable_surrogate_key
    "repository-#{repository_id} #{super}"
  end

  def deletion_candidates=(values)
    @deletion_candidates = values.to_s.split(",").map(&:to_i)
  end

  def scheduled_for_deletion?(asset)
    defined?(@deletion_candidates) &&
      @deletion_candidates.include?(asset.id)
  end

  attr_reader :replacing_asset
  attr_reader :replaced_asset

private
  def choose_storage_provider
    return if GitHub.storage_cluster_enabled?
    self.storage_provider = :s3_production_data
  end

  def uploader_access
    if !uploader
      errors.add :uploader_id, "is not a valid User"
    elsif !release
      errors.add :release_id, "is not a valid release"
    elsif !repository
      errors.add :repository_id, "is not a valid repository"
    elsif !repository.resources.contents.writable_by?(uploader)
      errors.add :uploader_id, "does not have push access to #{repository.name_with_owner}"
    end
  end

  # Guidelines for overrides:
  #
  # 1. The extension is used by the user agent.
  # 2. The extension is used to install something.
  # 3. The extension has no known security issues.
  #
  # BAD EXTENSIONS:
  #
  # * exe - No user agent should be running these.
  # * zip - Downloading won't impede the ability to install it.
  # * pdf - Not used to actually install something.
  # * html, css, js, swf, svg - NOOOOPE.  See guideline 3 :)
  CONTENT_TYPE_OVERRIDES = {
    ".apk" => "application/vnd.android.package-archive",
    ".xpi" => "application/x-xpinstall",
  }

  CONTENT_DISPOSITION_OVERRIDES = {
    ".xpi" => "inline;",
  }

  DEFAULT_CONTENT_DISPOSITION = "attachment;".freeze
  DOWNLOAD_CONTENT_TYPE = "application/octet-stream".freeze
  BAD_CONTENT_TYPE = "application/x-www-form-urlencoded".freeze

  def content_type_is_allowed
    self.content_type = DOWNLOAD_CONTENT_TYPE if content_type.blank?
    self.content_type = content_type.strip

    if content_type == BAD_CONTENT_TYPE
      errors.add(:content_type, "can't be #{BAD_CONTENT_TYPE}")
    end
  end

  BAD_EXTENSIONS = Set.new %w(.app .xcarchive)
  def extension_is_allowed
    if BAD_EXTENSIONS.include?(name_extension)
      errors.add(:name, "has a file extension that is not allowed")
    end
  end

  def name_is_unique
    same_named = ReleaseAsset.where(release_id: release_id, name: name)
    same_named = same_named.where("id != ?", self.id) unless new_record?

    if duplicate = same_named.first
      if scheduled_for_deletion? duplicate
        @replacing_asset = duplicate.id
      else
        errors.add(:name, :taken, value: name)
      end
    end
  end

  def destroy_duplicate
    ReleaseAsset.destroy(@replacing_asset)
    @replaced_asset = @replacing_asset
    @replacing_asset = nil
  end

  def filename_content_disposition
    "filename=#{name}"
  end
end
