# frozen_string_literal: true
class GHVFS::Replica < ApplicationRecord::Collab
  self.table_name = "ghvfs_replicas"

  areas_of_responsibility :git_protocols

  validates :created_at, presence: true
  validates :updated_at, presence: true

  belongs_to :repository
  belongs_to :ghvfs_fileserver, class_name: "GHVFS::Fileserver"

  # Public: Adds a node as a GHVFS replica of the given repository. The node is
  # a hash of parameters. If the node already exists and is part of a replica,
  # only the timestamps are updated.
  #
  # Returns the ID of this replica.
  def self.add_or_update(repo, node)
    GHVFS::Fileserver.add_or_update(node)

    transaction do
      fsid =  GHVFS::Fileserver.where(host: node[:host]).first.id
      query = <<-SQL
      INSERT INTO ghvfs_replicas (repository_id, ghvfs_fileserver_id, created_at, updated_at)
      VALUES (:repository_id, :ghvfs_fileserver_id, NOW(), NOW())
      ON DUPLICATE KEY UPDATE
        updated_at = NOW()
      SQL
      github_sql.run(query,
                     repository_id: repo.id,
                     ghvfs_fileserver_id: fsid)
      where(repository_id: repo.id, ghvfs_fileserver_id: fsid).pluck(:id).first
    end
  end

  # Public: Remove the node as a GHVFS replica, if it is one.
  #
  # Returns the IDs of the replicas destroyed, if any.
  def self.delete(repo, host)
    fserv = GHVFS::Fileserver.find_by(host: host)

    if fserv
      where(repository: repo, ghvfs_fileserver_id: fserv.id).map do |r|
        id = r.id
        r.destroy!
        id
      end
    else
      []
    end
  end
end
