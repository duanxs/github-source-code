# frozen_string_literal: true

class CloneTemplateRepository
  # Public: The Repository from which to create a new repo, must be marked as a template.
  attr_reader :template_repository

  # Public: The new Repository that's created. Will be nil on error.
  attr_reader :new_repository

  # Public: A String error message if somethings goes wrong or nil otherwise.
  attr_reader :error

  # Public: Whether or not branches in the template repository will be copied to the new repository; Boolean.
  attr_reader :copy_branches

  # tmpl_repo - a Repository that's marked as a template
  # actor - the User doing the cloning
  # owner - a User or Organization to own the new repository, cloned from the template repo;
  #         not required if `new_repository` is given
  # name - the name for the new repository; not required if `new_repository` is given
  # new_repository - required if `name` and `owner` are omitted; the new Repository that was
  #                  created that should be populated from the template; if nil, a new repo will be
  #                  created
  # copy_branches - determine if all the branches existing in the template repository should be
  #                 copied into the new repository; defaults to not copying the branches, only
  #                 copying the default branch of the template and its contents to the new repo
  # new_repo_attrs - other attributes for the new repository if `new_repository` wasn't given, can
  #                  include:
  #   public             - True means public, false means private.
  #   visibility         - Used in place of 'public'. Can be public, private, internal.
  #   description        - Optional description as a String.
  #   homepage           - Optional homepage URL as a String.
  #   reflog_data        - Reflog data needed to create initial commit
  #   has_wiki           - Set up wiki for repo
  #   has_downloads      - Set up downloads for repo
  #   has_issues         - Set up issues for repo
  #   team_id            - The team that's granted access to this repository; orgs only
  #   allow_merge_commit - Allow the creation of merge commits - defaults to true
  #   allow_squash_merge - Allow squash merges - defaults to true
  #   allow_rebase_merge - Allow rebase merges - defaults to true
  def initialize(tmpl_repo, actor:, owner: nil, name: nil, new_repository: nil, copy_branches: false, **new_repo_attrs)
    @template_repository = tmpl_repo
    @actor = actor
    @error = nil
    @new_repository = new_repository
    @copy_branches = copy_branches
    @repo_attributes = new_repo_attrs
    if owner && name
      @repo_attributes = @repo_attributes.merge(owner: owner, name: name)
    end
  end

  # Public: Copy the template repository to a new repository with the specified owner,
  # name, and other attributes.
  #
  # Returns true on success, false on failure.
  def clone
    unless template_repository&.resources&.contents&.readable_by?(@actor)
      @error = "template repository not found."
      return false
    end

    unless template_repository.template?
      @error = "#{template_repository.name_with_owner} is not a template repository."
      return false
    end

    unless template_repository.active?
      @error = "#{template_repository.name_with_owner} is no longer active."
      return false
    end

    if template_repository.disabled_at.present? || template_repository.disabled_access_reason
      @error = "#{template_repository.name_with_owner} has been disabled and cannot be used " \
               "as a template."
      return false
    end

    if new_repository
      unless new_repository.empty?
        @error = "#{new_repository.name_with_owner} already has content."
        return false
      end
    else
      result = Repository.handle_creation(@actor, owner.login, false, @repo_attributes, reflog_data)
      unless result.success?
        @error = error_message_for_repo_creation(result)
        return false
      end

      @new_repository = result.repository
    end

    repo_clone = RepositoryClone.new(clone_repository: new_repository,
                                     template_repository: template_repository,
                                     cloning_user: @actor)

    if repo_clone.save
      CloneTemplateRepositoryFilesJob.perform_later(repo_clone.id, copy_branches: copy_branches)
      true
    else
      @error = repo_clone.errors.full_messages.join(", ")
      false
    end
  end

  private

  def error_message_for_repo_creation(result)
    if !result.allowed
      "#{@actor} does not have permission to create a repository owned by #{owner}"
    else
      result.repository.errors.full_messages.join(", ").presence ||
        result.error_message
    end
  end

  def owner
    @repo_attributes[:owner] || new_repository.owner
  end

  def name
    @repo_attributes[:name] || new_repository.name
  end

  def name_with_owner
    "#{owner.login}/#{name}"
  end

  def public?
    if @repo_attributes.key?(:public)
      @repo_attributes[:public]
    else
      new_repository&.public?
    end
  end

  def reflog_data
    (@repo_attributes[:reflog_data] || {}).merge(repo_name: name_with_owner, repo_public: public?)
  end
end
