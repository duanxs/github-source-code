# frozen_string_literal: true

# A session for operations for a specific commit
class Epoch < ApplicationRecord::Collab
  self.table_name = :epochs

  include GitHub::Relay::GlobalIdentification

  areas_of_responsibility :epochs

  belongs_to :repository

  has_many :epoch_operations
  has_many :epoch_commits

  validates :repository, presence: true
  validates :ref, presence: true
  validates :base_commit_oid, presence: true

  def self.store_operations(author:, ref:, base_commit_oid:, operations:)
    fail Git::Ref::UpdateFailed, "No author." unless author
    fail Git::Ref::UpdateFailed, "No operations." unless operations.present?
    fail Git::Ref::NotFound, "Cannot find ref." unless ref
    fail Git::Ref::UpdateFailed, "Cannot write to repository." unless ref.repository.pushable_by?(author)
    fail Git::Ref::UpdateFailed, "Ref is not a branch." unless ref.branch?
    fail Git::Ref::UpdateFailed, "Ref is a protected branch." if ref.protected_branch
    fail Git::Ref::UpdateFailed, "Commit is not at the tip of the branch." unless ref.target_oid == base_commit_oid

    results = []

    GitHub.dogstats.time("epoch.store_operations.time", tags: ["action:batch"]) do
      ActiveRecord::Base.transaction do
        epoch = Epoch.where(
          repository: ref.repository,
          ref: ref.qualified_name,
          base_commit_oid: base_commit_oid).first

        epoch ||= Epoch.create(
          repository: ref.repository,
          ref: ref.qualified_name,
          base_commit_oid: base_commit_oid)

        operations.each do |op|
          results << epoch.epoch_operations.create(
            author: author,
            operation: op)
          GitHub.dogstats.increment("epoch.operation", tags: ["action:create"])
        end
      end
    end

    results
  end
end
