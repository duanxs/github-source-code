# frozen_string_literal: true

module SecurityAnalysisDependency
  extend ActiveSupport::Concern

  DEPENDENCY_GRAPH_NEW_REPOS_KEY = "dependency_graph.new_repos_enable".freeze
  SECURITY_ALERTS_NEW_REPOS_KEY = "security_alerts.new_repos_enable".freeze
  VULNERABILITY_UPDATES_NEW_REPOS_KEY = "vulnerability_updates.new_repos_enable".freeze
  SECRET_SCANNING_NEW_REPOS_KEY = "secret_scanning.new_repos_enable".freeze

  # Dependency graph
  # Note: For public repos, dependency graph is enabled by default.

  # Enable dependency graph for all new private repos in an organization.
  def enable_dependency_graph_for_new_repos(actor:)
    config.enable(DEPENDENCY_GRAPH_NEW_REPOS_KEY, actor)
  end

  # Disable dependency_graph for all new private repos in an organization.
  # This will also disable dependabot alerts and security updates for new repos in the organization.
  def disable_dependency_graph_for_new_repos(actor:)
    config.delete(DEPENDENCY_GRAPH_NEW_REPOS_KEY, actor)
    disable_security_alerts_for_new_repos(actor: actor)
  end

  # Indicates if dependency graph was enabled for all new private repos from the organization level Security & analysis page.
  def dependency_graph_enabled_for_new_repos?
    config.enabled?(DEPENDENCY_GRAPH_NEW_REPOS_KEY)
  end

  # Dependabot alerts

  # Enable dependabot alerts for all new repos in an organization.
  # This will also enable dependency graph for all new private repos in the organization.
  def enable_security_alerts_for_new_repos(actor:)
    config.enable(SECURITY_ALERTS_NEW_REPOS_KEY, actor)
    enable_dependency_graph_for_new_repos(actor: actor)
  end

  # Disable dependabot alerts for all new repos in an organization.
  # This will also disable dependabot security updates for all new repos in the organization.
  def disable_security_alerts_for_new_repos(actor:)
    config.delete(SECURITY_ALERTS_NEW_REPOS_KEY, actor)
    disable_vulnerability_updates_for_new_repos(actor: actor)
  end

  # Indicates if dependabot alerts was enabled for all new repos from the organization level Security & analysis page.
  def security_alerts_enabled_for_new_repos?
    config.enabled?(SECURITY_ALERTS_NEW_REPOS_KEY)
  end

  # Dependabot security updates

  # Enable dependabot security updates for all new repos in an organization.
  # This will also enable dependency graph and dependabot alerts for all new repos in the organization.
  def enable_vulnerability_updates_for_new_repos(actor:)
    config.enable(VULNERABILITY_UPDATES_NEW_REPOS_KEY, actor)
    enable_security_alerts_for_new_repos(actor: actor)
  end

  # Disable dependabot security updates for all new repos in an organization.
  def disable_vulnerability_updates_for_new_repos(actor:)
    config.delete(VULNERABILITY_UPDATES_NEW_REPOS_KEY, actor)
  end

  # Indicates if dependabot security updates was enabled for all new repos from the organization level Security & analysis page.
  def vulnerability_updates_enabled_for_new_repos?
    config.enabled?(VULNERABILITY_UPDATES_NEW_REPOS_KEY)
  end

  # Secret scanning
  # Note: For public repos, secret scanning is enabled by default.

  # Enable secret scanning for all new private repos in an organization.
  def enable_secret_scanning_for_new_repos(actor:)
    config.enable(SECRET_SCANNING_NEW_REPOS_KEY, actor)
  end

  # Disable secret scanning for all new private repos in an organization.
  def disable_secret_scanning_for_new_repos(actor:)
    config.delete(SECRET_SCANNING_NEW_REPOS_KEY, actor)
  end

  # Indicates if secret scanning was enabled for all new private repos from the organization level Security & analysis page.
  def secret_scanning_enabled_for_new_repos?
    config.enabled?(SECRET_SCANNING_NEW_REPOS_KEY)
  end
end
