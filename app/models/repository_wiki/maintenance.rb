# rubocop:disable Style/FrozenStringLiteralComment

class RepositoryWiki
  # Schedule maintenance tasks to run from the old and active generations. We
  # first find the wikis that have received the most pushes over the
  # maintenance threshold and then select the remaining number of records up to
  # the limit from the list of all wikis.
  #
  # limit          - Total number of wikis to schedule for maintenance.
  #
  # Returns an array of wikis that were scheduled.
  def self.schedule_maintenance(limit, min_age = 1.week)
    wikis  = find_most_active_since_last_maintenance(limit * 0.5, 50)
    wikis += find_longest_time_since_last_maintenance(limit - wikis.size, min_age)

    scheduled = wikis.uniq.each { |wiki| wiki.schedule_maintenance }

    RepositoryNetwork.move_stuck_networks_to_retry!(RepositoryWiki)
    scheduled
  end

  # Find wikis that are most in need of maintenance due to push/write activity.
  #
  # limit     - Maximum number of wikis to return.
  # threshold - Minimum number of pushes the wiki must have received to
  #             qualify for maintenance. Wikis that haven't received this
  #             many pushes since the last maintenance are not included.
  #
  # Returns an array of RepositoryWiki objects.
  def self.find_most_active_since_last_maintenance(limit, threshold = 50)
    ActiveRecord::Base.connected_to(role: :reading) do
      find_by_sql <<-SQL
        SELECT *
        FROM repository_wikis
        WHERE maintenance_status IN ('complete', 'retry')
        AND (pushed_count_since_maintenance > #{threshold.to_i})
        ORDER BY pushed_count_since_maintenance DESC
        LIMIT #{limit.to_i}
      SQL
    end
  end

  # Find wikis that require maintenance and have gone the longest without
  # receiving any. This includes wikis that have never had maintenance.
  # Wikis are selected ordered by last_maintenance_at, so
  # new and never visited wikis fall into the same time queue.
  #
  # limit      - Maximum number of wikis to return.
  # min_age    - Minimum length of time between maintenance runs for any single
  #              wiki in seconds. Wikis that have received maintenance
  #              within this limit will not be selected.
  #
  # Returns an array of RepositoryWiki objects.
  def self.find_longest_time_since_last_maintenance(limit, min_age = 1.week)
    time_cutoff = (Time.now - min_age)
    ActiveRecord::Base.connected_to(role: :reading) do
      find_by_sql <<-SQL
        SELECT *
        FROM repository_wikis
        WHERE maintenance_status IN ('complete', 'retry')
        AND pushed_count_since_maintenance > 0
        AND last_maintenance_at < '#{time_cutoff.to_s(:db)}'
        ORDER BY last_maintenance_at ASC
        LIMIT #{limit.to_i}
      SQL
    end
  end

  module Maintenance
    # Schedule maintenance for this wiki. This enqueues the job on
    # the fs maintenance queue and updates the maintenance status column.
    def schedule_maintenance
      return unless repository
      update_status :scheduled, last_maintenance_attempted_at: Time.now
      WikiMaintenanceJob.set(queue: maintenance_queue_name).perform_later(id)
    end

    def mark_as_broken
      update_status :broken
    end

    # Run maintenance on this wiki. This typically isn't called directly but
    # run from the WikiNetworkMaintenance job.
    def perform_maintenance
      started_at = Time.now
      pushed_count_at_start = pushed_count
      update_status :running

      unsullied_wiki.repack if unsullied_wiki.exist?

      # reload to get latest pushed_count and update status
      reload
      update_status :complete,
        last_maintenance_at: started_at,
        pushed_count_since_maintenance: (pushed_count - pushed_count_at_start)

    rescue GitRPC::RepositoryOffline, GitRPC::NetworkError
      # Repository offline; try again later
      update_status :retry, last_maintenance_at: started_at
    rescue => boom
      update_status :failed
      raise boom
    end

    # Find all repositories in the network that were modified since the last
    # time maintenance was performed. If maintenance has never been performed
    # for the network, all repositories are returned.
    def wikis_modified_since_last_maintenance
      conditions = nil
      conditions = ["pushed_at >= ?", last_maintenance_at] if last_maintenance_at
      where(conditions).order(:id).to_a
    end

    # Internal: Update the wiki record's maintenance_status in the database.
    #
    # value - One of the maintenance status values as a string.
    #
    # Returns nothing.
    def update_status(value, attributes = {})
      attributes = attributes.merge(maintenance_status: value.to_s)
      update!(attributes)
    end

    # Internal: The resque queue where maintenance jobs should be scheduled
    # for this wiki.
    def maintenance_queue_name
      repository.maintenance_queue_name
    end
  end
end
