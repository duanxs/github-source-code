# frozen_string_literal: true

class AutomaticAppInstallation
  module Handlers
    class EditorOpened < BaseHandler
      alias repository originator

      def install_integration
        return false unless actor && GitHub.flipper[:collaborative_editing].enabled?(actor)

        install_triggers.each do |install_trigger|
          # enqueue background job to install it
          target_id = repository.owner_id
          integration_id = install_trigger.integration.id
          InstallAutomaticIntegrationsJob.perform_later(
            target_id,
            integration_id,
            install_trigger.id,
            GitHub::Jobs::InstallAutomaticIntegrations::INSTALL_ON_ALL_REPOS,
            {"enqueued_timestamp" => Time.now.to_i},
          )
        end
      end
    end
  end
end
