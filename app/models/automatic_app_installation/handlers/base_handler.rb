# frozen_string_literal: true

class AutomaticAppInstallation
  module Handlers
    # All handlers should inherit from this class to get generic
    # instrumentation, logging and validation.
    class BaseHandler

      class Result
        def self.success
          new(result: :success)
        end

        def self.failure(reason:)
          new(result: :failure, reason: reason)
        end

        attr_reader :result, :reason

        def initialize(result:, reason: nil)
          @result = result
          @reason = reason
        end

        def success?
          result == :success
        end
      end

      attr_reader :install_triggers, :originator, :actor

      def initialize(install_triggers:, originator:, actor:)
        @install_triggers = install_triggers
        @originator = originator
        @actor = actor
      end

      def validate
        if actor.nil?
          return Result.failure(reason: :actor_not_supplied)
        end

        if originator.nil?
          return Result.failure(reason: :originator_not_supplied)
        end

        if install_triggers.empty?
          return Result.failure(reason: :install_triggers_not_supplied)
        end

        Result.success
      end

    end
  end
end
