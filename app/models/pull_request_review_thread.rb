# frozen_string_literal: true

class PullRequestReviewThread < ApplicationRecord::Domain::Repositories
  include GitHub::Relay::GlobalIdentification
  extend GitHub::BackgroundDependentDeletes

  GLOBAL_ID_V2_IDENTIFIER = "v2".freeze

  belongs_to :pull_request
  belongs_to :pull_request_review
  belongs_to :resolver, class_name: "User"
  has_one :repository, through: :pull_request

  has_many :review_comments,
    class_name: "PullRequestReviewComment",
    inverse_of: :pull_request_review_thread
  destroy_dependents_in_background :review_comments

  # Public: Scope to filter legacy review threads
  #
  # Legacy review threads are threads that do _not_ belong to a `PullRequestReview`
  scope :legacy, -> {
    where(pull_request_review_id: nil)
  }

  # Public: Scope to filter threads visible to the viewer
  #
  # viewer - User viewing the threads (nil means anonymous)
  #
  # Notice: This logic needs to be kept in sync with `#async_visible_to?`.
  scope :visible_to, -> (viewer) {
    comments_scope = PullRequestReviewComment.
      visible_to(viewer).
      where(<<~SQL)
        `pull_request_review_threads`.`id` = `pull_request_review_comments`.`pull_request_review_thread_id`
      SQL

    where("EXISTS (#{comments_scope.to_sql})", comments_scope.where_values_hash)
  }

  def build_reply(pull_request_review: nil, user: pull_request_review.user, body:)
    first_comment = review_comments.first
    first_comment.build_reply(review: pull_request_review, body: body, user: user)
  end

  # Public: Instantiates a new PullRequestReviewComment attached to this thread.
  #
  # user - the User who commented. Defaults to the writer of the review
  # body - the String body of the comment
  # path - the path of the right side of the diff entry on which the comment has been left
  # diff - the GitHub::Diff on which this comment has been left. Defaults to the latest diff for the pull request.
  # start line & side - The start line of a multi line comment and the side of the diff it is on.
  # line & side - The line and side of the comment. The end of the range for a multi-line comment.
  #
  # Returns a new PullRequestReviewComment which is part of this thread's review_comments collection
  def build_first_comment(user: pull_request_review.user, body:, path:, diff: nil,
    start_line: nil, start_side: :right, line: nil, side: :right)

    diff ||= pull_request.historical_comparison.diffs
    diff = diff.only_params

    start_position_data = diff_position_data(diff, path, start_side, start_line)
    position_data = diff_position_data(diff, path, side, line)

    review_comments.build(
      pull_request: pull_request,
      pull_request_review: pull_request_review,
      user: user,
      body: body,
      end_position_data: position_data,
      start_position_data: start_position_data,
    )
  end

  # Public: Instantiates a new PullRequestReviewComment attached to this thread. This is
  #   primarily a legacy method supporting the API endpoints which support specifying a comment's
  #   position by its offset into the diff text.
  #
  # user - the User who commented
  # body - the String body of the comment
  # pull_comparison - the PullRequest::Comparison on which this comment has been left
  # position - the integer offset into the diff text on which this comment is being left
  # path - the path of the right side of the diff entry on which the comment has been left
  #
  # Returns a new PullRequestReviewComment which is part of this thread's review_comments collection
  def build_first_diff_position_comment(user:, body:, diff: nil, position:, path:)
    diff ||= pull_request.async_diff.sync
    position_data = legacy_position_data(diff, path, position)
    review_comments.build(
      pull_request: pull_request,
      pull_request_review: pull_request_review,
      user: user,
      body: body,
      end_position_data: position_data,
    )
  end

  def self.id_for(thread_id: nil, first_comment_id: nil, version: 2)
    case version
    when 1
      raise ArgumentError, "first_comment_id cannot be nil for version 1" unless first_comment_id
      first_comment_id.to_s
    when 2
      raise ArgumentError, "thread_id cannot be nil for version 2" unless thread_id
      [thread_id, GLOBAL_ID_V2_IDENTIFIER].join(":")
    end
  end

  def global_id
    self.class.id_for(thread_id: id, version: 2)
  end

  # Public: Visiblity check for thread
  #
  # viewer - User viewing the threads (nil means anonymous)
  #
  # Notice: This logic needs to be kept in sync with the scope `visible_to`.
  def async_visible_to?(viewer)
    async_review_comments.then do |review_comments|
      review_comments.any? { |comment| comment.visible_to?(viewer) }
    end
  end

  def published?
    if pull_request_review
      !pull_request_review.pending?
    else
      false
    end
  end

  def legacy?
    pull_request_review_id.nil?
  end

  def async_review_comments_for(viewer)
    Platform::Loaders::PullRequestReviewCommentsForReviewThread.load(id, viewer)
  end

  def async_to_deprecated_thread
    Promise.all([
      async_pull_request,
      async_review_comments,
    ]).then do |pull_request, comments|
      next if comments.nil? || comments.empty?

      comments = comments.sort_by do |comment|
        [comment.pull_request_review_thread_id, comment.created_at, comment.id]
      end

      comments.each do |comment|
        comment.association(:pull_request).target = pull_request
      end

      first_comment = comments.first
      first_comment.async_original_pull_request_comparison.then do |pull_comparison|
        ::DeprecatedPullRequestReviewThread.new(
          pull: pull_request,
          pull_comparison: pull_comparison,
          path: first_comment.path,
          position: first_comment.original_position,
          comments: comments,
        )
      end
    end
  end

  def async_to_platform_thread
    async_review_comments.then do |comments|
      Platform::Models::PullRequestReviewThread.from_review_comments(comments)
    end
  end

  def platform_type_name
    "PullRequestReviewThread"
  end

  def resolve(resolver:)
    self.resolver = resolver
    self.resolved_at = Time.now
    self.save!
  end

  def unresolve
    self.resolver = nil
    self.resolved_at = nil
    self.save!
  end

  def async_to_activerecord
    Promise.resolve(self)
  end

  # Determine whether the given viewer can see this thread.
  #
  # viewer - A User or nil for a anonymous viewer.
  #
  # Returns a Promise<Boolean>.
  def async_hide_from_user?(viewer)
    async_review_comments.then do |review_comments|
      review_comments.none? { |comment| comment.visible_to?(viewer) }
    end
  end

  def async_start_line
    async_first_comment.then(&:async_start_line)
  end

  def start_side
    async_first_comment.then(&:start_side)
  end

  def async_end_line
    async_first_comment.then(&:async_end_line)
  end

  def async_line
    async_first_comment.then(&:async_line)
  end

  # Determines whether the given viewer can reply to this thread.
  #
  # viewer - A User or nil for a anonymous viewer.
  #
  # Returns a Promise<Boolean>.
  def async_viewer_can_reply?(viewer)
    async_viewer_cannot_reply_reasons(viewer).then(&:empty?)
  end

  def async_viewer_cannot_reply_reasons(viewer)
    return Promise.resolve([:login_required]) unless viewer

    errors = []
    errors << :verified_email_required if viewer.must_verify_email?

    Promise.all([
      async_first_comment,
      async_original_pull_request_comparison
    ]).then do |first_comment, pull_comparison|
      errors << :missing_objects unless pull_comparison
      errors << :reply if first_comment.reply?

      async_repository.then do |repository|
        if repository.archived?
          errors << :archived
          errors
        else
          async_locked_for?(viewer).then do |locked|
            errors << :locked if locked
            errors
          end
        end
      end
    end
  end

  def async_locked_for?(viewer)
    async_pull_request.then(&:async_issue).then do |issue|
      issue.async_locked_for?(viewer)
    end
  end

  # Determines whether the pull request review thread is outdated.
  #
  # Returns a Boolean.
  def outdated?
    async_first_comment.then(&:outdated?).sync
  end

  def async_collapsed?
    async_resolved?
  end

  def async_resolved?
    async_resolved_by_id.then do |resolver_id|
      resolver_id.present?
    end
  end

  def async_diff_entry
    async_first_comment.then(&:async_diff_entry)
  end

  def async_diff_lines(syntax_highlighted_diffs_enabled:, max_context_lines:)
    Promise.all([
      async_pull_request,
      async_pull_request.then(&:async_compare_repository),
      async_diff_entry,
      async_path,
      async_first_comment,
    ]).then do |pull_request, repository, diff_entry, path, first_comment|
      async_warm_syntax_highlighted_diff = if syntax_highlighted_diffs_enabled && diff_entry
        Platform::Loaders::WarmSyntaxHighlightedDiffCache.load(repository, pull_request, diff_entry)
      else
        Promise.resolve(nil)
      end

      async_warm_syntax_highlighted_diff.then do
        # TODO: Using a ViewModel in here is a bit icky, but is the simplest way forward. 😱
        # As soon as all references to `::Diff::ReviewCommentCodeLinesPartialView` are
        # removed from the view, we should revisit this and refactor it to be less of a hack.
        partial_view = ::Diff::ReviewCommentCodeLinesPartialView.new({
          current_user: nil, # This value is never used inside of `::Diff::ReviewCommentCodeLinesPartialView`
          repository: repository,

          excerpt_offset: first_comment.excerpt_starting_offset(max_context_lines),
          line_enumerator: first_comment.excerpt_line_enumerator(max_context_lines),
          anchor: "discussion-diff-#{id}",

          diff_entry: diff_entry,
          path: path,

          syntax_highlighted_diffs_enabled: syntax_highlighted_diffs_enabled,
          commentable: false,
          expandable: false,
          max_context_lines: max_context_lines,
        })

        lines = partial_view.line_enumerator.to_a

        lines.map do |line|
          {
            type: line.type,
            text: line.text,
            html: partial_view.highlighted_line(line),
            position: line.position,
            left: line.left == -1 ? nil : line.left,
            right: line.right == -1 ? nil : line.right,
            no_newline_at_end: line.nonewline?,
          }
        end
      end
    end
  end

  def diff_lines_truncated?
    async_first_comment.then(&:excerpt_truncated?).sync
  end

  def async_discussion_diff_path_uri
    return @async_discussion_diff_path_uri if defined?(@async_discussion_diff_path_uri)

    @async_discussion_diff_path_uri = async_pull_request.then do |pull_request|
      pull_request.async_path_uri.then do |path_uri|
        async_first_comment.then do |first_comment|
          path_uri = path_uri.dup
          path_uri.fragment = "discussion-diff-#{first_comment.id}"
          path_uri
        end
      end
    end
  end

  def async_original_diff_path_uri
    async_first_comment.then(&:async_original_diff_path_uri)
  end

  def async_current_diff_path_uri
    async_first_comment.then(&:async_current_diff_path_uri)
  end

  def async_original_diff_file_path_uri
    return @async_original_diff_file_path_uri if defined?(@async_original_diff_file_path_uri)

    @async_original_diff_file_path_uri = async_original_diff_path_uri.then do |path_uri|
      next unless path_uri
      async_diff_file_path_uri(path_uri)
    end
  end

  def async_current_diff_file_path_uri
    return @async_current_diff_file_path_uri if defined?(@async_current_diff_file_path_uri)

    @async_current_diff_file_path_uri = async_current_diff_path_uri.then do |path_uri|
      next unless path_uri
      async_diff_file_path_uri(path_uri)
    end
  end

  def async_path
    async_position_data.then(&:path).then do |path|
      if path.respond_to?(:force_encoding) && path.encoding != ::Encoding::UTF_8
        path = path.dup
        path.force_encoding(::Encoding::UTF_8)
      end

      path
    end
  end

  def async_pull_request_commit
    Promise.all([
      async_pull_request,
      async_position_data.then(&:commit_oid),
    ]).then do |pull_request, commit_oid|
      pull_request.async_load_pull_request_commit(commit_oid)
    end
  end

  # Public: Returns the _original_ end blob position.
  def async_original_line
    async_position_data.then(&:line)
  end

  # Public: Returns the _original_ start blob position (multi-line only).
  def async_original_start_line
    async_position_data.then(&:original_start_line)
  end

  def async_diff_side
    async_position_data.then(&:diff_side)
  end

  def async_resolved_by
    async_to_activerecord.then(&:async_resolver)
  end

  def async_resolved_by_id
    async_to_activerecord.then(&:resolver_id)
  end

  def async_can_resolve(viewer)
    can_change_resolve_state(viewer) do
      async_resolved_by_id.then do |resolver_id|
        resolver_id.blank?
      end
    end
  end

  def async_can_unresolve(viewer)
    can_change_resolve_state(viewer) do
      async_resolved_by_id.then do |resolver_id|
        resolver_id.present?
      end
    end
  end

  def position
    async_first_comment.then do |first_comment|
      first_comment.original_position
    end
  end

  def async_first_comment
    async_review_comments.then do |review_comments|
      review_comments.first
    end
  end

  def comments
    review_comments
  end

  private

  def can_change_resolve_state(viewer)
    return Promise.resolve(false) if viewer&.spammy?

    async_pull_request_review.then do |review|
      if published?
        async_pull_request.then do |pull|
          Promise.all([pull.async_user, pull.async_repository]).then do |pr_author, repo|
            repo.async_writable_by?(viewer).then do |repo_writable|
              if (pr_author && viewer == pr_author) || repo_writable
                yield
              else
                Promise.resolve(false)
              end
            end
          end
        end
      else
        Promise.resolve(false)
      end
    end
  end

  attr_reader :first_comment

  def async_original_pull_request_comparison
    async_first_comment.then(&:async_original_pull_request_comparison)
  end

  def async_position_data
    return @async_position_data if defined?(@async_position_data)
    @async_position_data = async_first_comment.then(&:async_position_data)
  end

  def async_diff_file_path_uri(diff_path_uri)
    async_path_digest.then do |path_digest|
      diff_path_uri.dup.tap do |uri|
        uri.fragment = "diff-#{path_digest}"
      end
    end
  end

  def async_path_digest
    async_path.then do |path|
      Digest::MD5.hexdigest(path)
    end
  end

  def diff_position_data(diff, path, side, line)
    return nil if line.nil?

    PullRequestReviewComment::DiffPositionData.new(
      diff: diff,
      line: line,
      right_path: path,
      side: side,
    )
  end

  def legacy_position_data(diff, path, position)
    PullRequestReviewComment::LegacyPositionData.new(
      diff: diff,
      comment_path: path,
      diff_position: position,
    )
  end
end
