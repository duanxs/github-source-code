# frozen_string_literal: true

class Stratocaster::GlobalIndex < ApplicationRecord::Mysql5
  self.table_name = "global_stratocaster_indexes"

  scope :by_key, -> (key) { where(index_key: key) }
  scope :by_key_value, -> (key, value) { where(index_key: key, value: value) }

  class << self
    def insert(keys, value)
      rows = keys.map { |key| [key, value, Time.now] }
      github_sql.run(<<-SQL, rows: GitHub::SQL::ROWS(rows))
        INSERT INTO global_stratocaster_indexes (`index_key`, value, modified_at)
        VALUES :rows
      SQL
      GitHub.dogstats.histogram("stratocaster.global_indexer", rows.count, tags: ["operation:insert"])
    end

    def purge(keys, value)
      by_key_value(keys, value).delete_all
    end

    def values(key, limit, min_age = nil, max_age = nil)
      record_scope = by_key(key)

      if min_age && max_age
        record_scope = record_scope.where("modified_at BETWEEN ? AND ?", max_age.ago, min_age.ago)
      end

      record_scope.
        order("modified_at DESC, id DESC").
        limit(limit).pluck(:value)
    end

    def clear_keys(keys)
      by_key(keys).delete_all
    end
  end
end
