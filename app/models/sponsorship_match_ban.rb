# frozen_string_literal: true

class SponsorshipMatchBan < ApplicationRecord::Collab
  # sponsor and sponsorable are polymorphic because we once intended to allow
  # sponsorships by Repository. If this is eliminated from our product roadmap,
  # we can make these monomorphic associtations with User.
  enum sponsor_type: {
    User: 0,
  }, _prefix: :sponsor

  enum sponsorable_type: {
    User: 0,
  }, _prefix: :sponsorable

  belongs_to :sponsor, polymorphic: true
  belongs_to :sponsorable, polymorphic: true

  validate :must_be_unique_for_sponsor_and_sponsorable

  private

  def must_be_unique_for_sponsor_and_sponsorable
    return unless SponsorshipMatchBan.find_by(sponsor: sponsor, sponsorable: sponsorable)
    errors.add(:sponsorable, "already does not receive matching for sponsorships from this sponsor")
  end
end
