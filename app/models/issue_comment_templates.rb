# frozen_string_literal: true

class IssueCommentTemplates
  class Template
    attr_reader :repository, :filename

    def initialize(repository, data, filename)
      @repository = repository
      @data = data
      @filename = filename
    end

    def async_repository
      Promise.resolve(repository)
    end

    def name
      parsed_data["name"]
    end

    def about
      parsed_data["about"]
    end

    def body
      parsed_data["body"]
    end

    def valid?
      errors.blank?
    end

    def errors
      # The library assumes a full schema as per AdaptiveCard spec, while the
      # user should only define elements. The wrapper used below should be
      # somewhere in our framework, but lives here for now.
      @errors ||= AdaptiveCards::Schema::Validation.validate({
          "type" => "AdaptiveCard",
          "version" => "1.2",
          "body" => body,
      })
    end

    def parsed_data
      @parsed_data ||= YAML.safe_load(@data) || {}
    rescue Psych::BadAlias, Psych::DisallowedClass, Psych::SyntaxError
      return {}
    end
  end

  def self.template_directory
    ".github/ISSUE_COMMENT_TEMPLATES"
  end

  def initialize(repository)
    @repository = repository
  end

  def any?
    valid_templates.any?
  end

  def valid_templates
    @valid_templates ||= templates.select(&:valid?)
  end

  def templates
    @templates ||= templates_by_filename.values
  end

  def [](filename)
    templates_by_filename[filename]
  end

  private

  def templates_by_filename
    @templates_by_filename ||= template_tree_entries.each_with_object({}) do |tree_entry, result|
      next unless File.extname(tree_entry.name) =~ /\A\.(yml|yaml)\z/i
      filename = File.basename(tree_entry.name)
      result[filename] = IssueCommentTemplates::Template.new(@repository, tree_entry.data, filename)
    end
  end

  def template_tree_entries
    if @repository.nil?
      return []
    end

    if @repository.access.broken?
      return []
    end

    begin
      directory = @repository.directory(@repository.default_oid, IssueCommentTemplates.template_directory)
      directory&.tree_entries || []
    rescue GitRPC::Error, Repository::CorruptionDetected
      []
    end
  end
end
