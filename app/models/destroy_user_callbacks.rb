# rubocop:disable Style/FrozenStringLiteralComment

# Orchestrates the pre-destroy callback behavior when deleting a user from
# the database. This controls the order of the destroy steps as well as error
# handling. Wrapping them up in one place lets us better control
# failure conditions. This object must be used as shared instance across the
# callback chain
#
# Warning:
# All destroy callbacks (except after_commit) will be run inside a transaction.
# Long running transactions should be avoided at all costs
#
# Examples
#
#   # rails callbacks involved in destroying users should use an instance variable

# Always reuse the same destroy user callback because we have state that
# is shared across the callback chain. This is bad, but can't be avoided without
# serious refactoring.
#
#   before_destroy :destroy_user_callbacks_before_destroy
#   after_commit   :destroy_user_callbacks_after_commit, :on => :destroy
#
#   def destroy_user_callbacks
#     @destroy_callback ||= DestroyUserCallbacks.new
#   end
#
#   def destroy_user_callbacks_before_destroy
#     destroy_user_callbacks.before_destroy(self)
#   end
#
#   def destroy_user_callbacks_after_commit
#     destroy_user_callbacks.after_commit(self)
#   end
#
class DestroyUserCallbacks

  BATCH_SIZE = 1000
  MAX_THROTTLE_RETRIES = 5

  # Run the pre-destroy cleanup steps for the user. Because this is registered
  # as a before_destroy callback, if it returns false or throws an exception,
  # the user record will not be destroyed.
  #
  # user - The User that's about to be deleted from the database.
  #
  # Returns false to stop the destroy callback chain.
  def before_destroy(user)
    GitHub.dogstats.increment("user", tags: ["action:destroy_attempted"])
    if user.created_at
      GitHub.dogstats.gauge "user.age", (Time.current - user.created_at).to_i, tags: ["action:destroy"]
    end

    delete_spam_content(user)
    hide_repository_and_dependents(user)
    delete_repository_transfers(user)
    hide_gists(user)
    delete_stars(user)
    delete_user_status(user)
    delete_notification_subscriptions(user)
    user.remove_ignore_list
    remove_followers(user)
    remove_followings(user)
    user.mark_login_as_used
    remove_from_mailchimp(user)
    remove_assignments(user) if user.user?
    delete_business_support_entitlements(user)
    delete_client_application_sets(user)
    delete_public_org_members_joins(user)
    prepare_collab_removed_events(user)
    prepare_org_memberships_removal(user)
    prepare_demilestoned_events(user)
    prepare_project_events(user)
    prepare_webhook_events(user)
    retire_namespaces(user)
    uninstall_integration_installations(user)
    instrument_billable_product_removal(user)
    notify_succession_agreement_terminations(user)

    UserNotice.all.each do |notice|
      user.reset_notice(notice.name)
    end

    # explicitly find the account admins before the transaction is committed
    # and store as an instance variable to be used in after_commit
    @account_admins = find_account_admins(user)
  end

  def after_commit(user)
    queue_delete_repositories(user)
    queue_delete_gists(user)
    send_destroy_confirmation(user, @account_admins)
    @account_admins = nil
    GitHub.dogstats.increment("user", tags: ["action:destroy"])
  end

  private

  def notify_succession_agreement_terminations(user)
    user.received_successor_invitations.accepted.each do |invite|
      invite.send_successor_removed_email
    end
  end

  def remove_assignments(user)
    user.clear_issue_assignments
  end

  def find_account_admins(user)
    user.admins.select { |admin| !admin.suspended? }
  end

  def remove_from_mailchimp(user)
    return unless GitHub.mailchimp_enabled?
    user.emails.each do |email|
      MailchimpDeleteJob.perform_later(email.email, user.id)
    end
  end

  def delete_spam_content(user)
    return if !user.spammy?
    # Temporarily send notifications to SpamNotifications room when spammy users
    # delete their accounts.
    GitHub::SpamChecker.notify("SPAM: User #{user.login} (#{user.id}) being destroyed; last_ip is #{user.last_ip}. https://github.com/stafftools/audit_log?query=user_id:#{user.id}")
  end

  def send_destroy_confirmation(user, admins)
    # Don't send the email in certain cases
    return if !user.receives_confirmation_when_destroyed?

    begin
      if user.user?
        AccountMailer.delete_user(user, admins).deliver_now
      elsif user.organization?
        AccountMailer.delete_org(user, admins).deliver_now
      else
        raise ArgumentError.new("can't send destroy confirmation email to '#{user.id}'")
      end
      AccountMailer.delete_private(user).deliver_now
    rescue ApplicationMailer::NoEmailFound => e
    end
  end

  def hide_repository_and_dependents(user)
    deleter = user.deleted_by ? User.find_by_login(user.deleted_by) : user
    user.repositories.each do |repo|
      repo.hide_repository_and_dependents(deleter)
    end
  end

  # Prepare 'deleted' notifications for UserEvent and OrganizationEvent
  def prepare_webhook_events(user)
    return if Rails.env.test? && !Hook.delivers_in_test?
    actor_id = self_destruction?(user) ? user.id : actor.id
    # organizations use a different class with a different key for the id
    if user.organization?
      # In dotcom we always send organization deletion hooks.
      # In enterprise, sending the organization deletion hook is configurable
      if GitHub.dotcom_request? || GitHub.extended_org_hooks_enabled?
        event_class = Hook::Event::OrganizationEvent
        id_key = :organization_id
      end
    else
      return unless GitHub.user_hooks_enabled?
      event_class = Hook::Event::UserEvent
      id_key = :user_id
    end

    user.construct_future_event do
      event_class.new(
        :action => :deleted,
        :triggered_at => Time.now,
        :actor_id => actor_id,
        id_key => user.id,
      )
    end
  end

  def prepare_demilestoned_events(user)
    return if Rails.env.test? && !Hook.delivers_in_test?

    actor_id = self_destruction?(user) ? user.id : actor.id
    user.repositories.each do |repo|
      repo.milestones.each do |milestone|

        milestone.open_issues.each do |issue|
          user.construct_future_event do
            Hook::Event::IssuesEvent.new(
              action: :demilestoned,
              user_id: user.id,
              repo_id: repo.id,
              actor_id: actor_id,
              issue_id: issue.id,
              triggered_at: Time.now,
            )
          end
        end

        user.construct_future_event do
          Hook::Event::MilestoneEvent.new(
            action: :deleted,
            milestone_id: milestone.id,
            actor_id: actor_id,
            triggered_at: Time.now,
          )
        end
      end
    end
  end

  def prepare_project_events(user)
    return if Rails.env.test? && !Hook.delivers_in_test?

    actor_id = self_destruction?(user) ? user.id : actor.id
    user.repositories.each do |repo|
      repo.projects.each do |project|
        user.construct_future_event do
          Hook::Event::ProjectEvent.new(
            action: :deleted,
            project_id: project.id,
            actor_id: actor_id,
            triggered_at: Time.now,
          )
        end
        project.columns.each do |project_column|
          user.construct_future_event do
            Hook::Event::ProjectColumnEvent.new(
              action: :deleted,
              project_column_id: project_column.id,
              actor_id: actor_id,
              triggered_at: Time.now,
            )
          end
        end
        project.cards.each do |project_card|
          user.construct_future_event do
            Hook::Event::ProjectCardEvent.new(
              action: :deleted,
              project_card_id: project_card.id,
              actor_id: actor_id,
              triggered_at: Time.now,
            )
          end
        end
      end
    end
  end

  # Queue the removal of all repositories for this user
  # Note: this requires these repositories to have been previously marked
  #       for deletion
  def queue_delete_repositories(user)
    user.repositories.each do |repo|
      repo.enqueue_delete_job
    end
  end

  def queue_delete_gists(user)
    user.gists.each(&:enqueue_archive_job)
  end

  def delete_repository_transfers(user)
    RepositoryTransfer.clear_target(user)
  end

  def hide_gists(user)
    user.gists.each(&:hide)
  end

  def remove_followers(user)
    user.followers.each { |f| f.unfollow(user) }

    GitHub.kv.del("user.followers_count.#{user.id}")
  end

  def remove_followings(user)
    user.following.each { |f| user.unfollow(f) }

    GitHub.kv.del("user.following_count.#{user.id}")
  end

  def delete_stars(user)
    GitHub.kv.del("user.starred_public_repositories_count.#{user.id}")
    GitHub.kv.del("user.starred_private_repositories_count.#{user.id}")
  end

  def delete_user_status(user)
    UserStatus.where(user: user).delete_all

    if user.organization?
      UserStatus.where(organization: user).delete_all
    end
  end

  def delete_notification_subscriptions(user)
    GitHub.newsies.async_delete_all_for_user(user.id)
  end

  def delete_business_support_entitlements(user)
    Business::SupportEntitlee.new(user).abilities.map(&:destroy)
  end

  def delete_client_application_sets(user)
    ClientApplicationSet.new(user.id).clear
  end

  def delete_public_org_members_joins(user)
    loop do
      deleted_count = Organization.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
        result = Organization.github_sql.run <<-SQL, id: user.id, batch_size: BATCH_SIZE
          DELETE FROM public_org_members
          WHERE user_id = :id
          LIMIT :batch_size
        SQL

        result.affected_rows
      end

      return if deleted_count < BATCH_SIZE
    end
  end

  def prepare_collab_removed_events(user)
    return if Rails.env.test? && !Hook.delivers_in_test?

    user.associated_repository_ids(including: :direct).each do |repo_id|
      user.construct_future_event do
        Hook::Event::MemberEvent.new(
          action: :removed,
          user_id: user.id,
          repo_id: repo_id,
          actor_id: actor.try(:id),
          triggered_at: Time.now,
        )
      end
    end
  end

  def prepare_org_memberships_removal(user)
    user.organizations.each do |organization|
      organization.remove_member_with_instrumentation(user, actor: actor, reason: Organization::RemovedMemberNotification::USER_ACCOUNT_DELETED)
    end
  end

  def retire_namespaces(user)
    retired_namespaces = user.retired_namespaces.pluck(:name)
    repos = user.repositories.reject { |repo| retired_namespaces.include?(repo.name.downcase) }
    repos = repos.select { |repo| RetiredNamespace.should_retire?(repo) }
    repos.each { |repo| RetiredNamespace.create_from_repository!(repo) }
  end

  def uninstall_integration_installations(user)
    IntegrationInstallation.where(target: user).each do |installation|
      installation.uninstall(actor: actor)
    end
  end

  def instrument_billable_product_removal(user)
    GlobalInstrumenter.instrument(
      "billing.plan_change",
      actor_id: actor&.id,
      user_id: user&.id,
      old_plan_name: user&.plan&.name,
      old_seat_count: user&.seats,
    )

    GlobalInstrumenter.instrument(
      "billing.lfs_change",
      actor_id: actor&.id,
      user_id: user&.id,
      old_lfs_count: user&.data_packs,
      new_lfs_count: 0,
    )

    # Marketplace and Sponsorship billable products are handled in
    # SubscriptionItem#instrument_billable_product_removal
  end

  # The user who performed the action as set in the GitHub request context. If
  # the context doesn't contain an actor, fallback to the ghost user.
  def actor
    @actor ||= (User.find_by_id(GitHub.context[:actor_id]) || User.ghost)
  end

  def self_destruction?(user)
    GitHub.context[:actor_id] == user.id
  end
end
