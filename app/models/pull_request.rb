# rubocop:disable Style/FrozenStringLiteralComment

# coding: utf-8
require "socket"

class PullRequest < ApplicationRecord::Domain::Repositories
  areas_of_responsibility :pull_requests

  class Error < StandardError; end

  class PermissionError < Error
    include GitHub::UIError

    def ui_message
      "You don't have permission write to the base repository."
    end
  end

  class MergeConflictError < Error
    include GitHub::UIError

    def ui_message
      "Merge conflict between head and base."
    end
  end

  class HeadMissing < Error
    include GitHub::UIError

    def ui_message
      "Head ref does not exist."
    end
  end

  class RefMismatch < Error
    include GitHub::UIError

    def ui_message
      "Head branch was modified. Review and try the merge again."
    end
  end

  class BaseNotChangeableError < Error
    include GitHub::UIError

    attr_reader :new_base_ref

    def self.error_type_key
      to_s.underscore
    end

    def error_type_key
      self.class.error_type_key
    end

    def initialize(pull, new_base_ref)
      @pull, @new_base_ref = pull, new_base_ref
    end

    def ui_message
      "Cannot change the base branch from '#{@pull.display_base_ref_name}' to '#{@new_base_ref}'."
    end
  end

  class BaseRefNotFoundError < BaseNotChangeableError
    def ui_message
      "Proposed base branch '#{@new_base_ref}' was not found"
    end
  end

  class RefPairingAlreadyExistsError < BaseNotChangeableError
    def ui_message
      "A pull request already exists for base branch '#{@new_base_ref}' and head branch '#{@pull.display_head_ref_name}'"
    end
  end

  class ComparisonWouldBeEmptyError < BaseNotChangeableError
    def ui_message
      "There are no new commits between base branch '#{@new_base_ref}' and head branch '#{@pull.display_head_ref_name}'"
    end
  end

  class ClosedError < BaseNotChangeableError
    def ui_message
      "Cannot change the base branch of a closed pull request."
    end
  end

  class RevertError < Error; end

  # Default limit of commits to be returned.
  COMMIT_LIMIT = 250
  AUTO_CHANGE_BASE_MAX_PULL_REQUESTS = 50
  REVISIONS_RANGE_MENU_LIMIT = 250

  REVERT_URI_TEMPLATE = Addressable::Template.new("/{owner}/{name}/pull/{number}/revert").freeze
  DISMISS_MERGE_TIP_URI_TEMPLATE = Addressable::Template.new("/{owner}/{name}/pull/{number}/dismiss_protip").freeze
  RESTORE_HEAD_REF_URI_TEMPLATE = Addressable::Template.new("/{owner}/{name}/pull/{number}/undo_cleanup").freeze

  include IssueTimeline
  include GitHub::Validations
  include Spam::Spammable
  include UserContentEditable
  include Blockable
  include AbuseReportable
  include MemexProjectItem::Content

  include GitHub::TimezoneTimestamp
  timezone_timestamp :contributed_at

  include PullRequest::ChecksDependency
  include PullRequest::CloseIssueReferencesDependency
  include PullRequest::FindersDependency
  include PullRequest::HovercardDependency
  include PullRequest::ReviewsDependency
  include PullRequest::ReviewRequestsDependency

  extend GitHub::BackgroundDependentDeletes
  include GitHub::Relay::GlobalIdentification
  include GitHub::RateLimitedCreation

  include Permissions::Attributes::Wrapper
  self.permissions_wrapper_class = Permissions::Attributes::Issue

  include MethodTiming
  include InteractionBanValidation
  include AuthorAssociable

  belongs_to :repository
  belongs_to :user
  has_one :issue, validate: true, inverse_of: :pull_request, autosave: true

  setup_spammable(:user)

  # Internal: an abstract collection, for the sub-resources of a Pull Request available for GitHub Apps
  def resources
    PullRequest::Resources.new(self)
  end

  # repository for base and head refs
  belongs_to :base_repository, class_name: "Repository"
  belongs_to :head_repository, class_name: "Repository"

  # users that own that base and head repositories
  belongs_to :base_user, class_name: :User
  belongs_to :head_user, class_name: :User

  # reviews
  has_many :reviews, class_name: "PullRequestReview"
  destroy_dependents_in_background :reviews

  # review requests
  has_many :review_requests, -> { where(dismissed_at: nil) },
    autosave: true,
    extend: ReviewRequest::AssociationExtension,
    inverse_of: :pull_request

  has_many :unscoped_review_requests, nil,
    class_name: "ReviewRequest",
    dependent: :delete_all

  has_many :revisions, class_name: :PullRequestRevision do
    def for_range_menu
      ready.number_desc.limit(REVISIONS_RANGE_MENU_LIMIT)
    end
  end
  destroy_dependents_in_background :revisions

  has_many :review_comments, nil,
    class_name: "PullRequestReviewComment",
    inverse_of: :pull_request
  destroy_dependents_in_background :review_comments

  has_many :user_reviewed_files

  has_many :codespaces

  has_many :review_threads, -> {
    order(:pull_request_id, :created_at, :id)
  },
  class_name: "PullRequestReviewThread",
  inverse_of: :pull_request

  has_many :legacy_review_threads, -> {
    where(pull_request_review_id: nil).order(:pull_request_id, :created_at, :id)
  },
    class_name: "PullRequestReviewThread",
    inverse_of: :pull_request
  destroy_dependents_in_background :legacy_review_threads

  has_many :last_seen_pull_request_revisions, dependent: :delete_all

  has_many :dependency_updates, -> {
    order(id: :desc)
  },
    class_name: :RepositoryDependencyUpdate,
    inverse_of: :pull_request,
    dependent: :destroy

  has_one :conflict,
    class_name: "PullRequestConflict",
    dependent: :destroy

  has_many :close_issue_references
  destroy_dependents_in_background :close_issue_references

  has_many :workflow_runs, as: :trigger

  has_many :memex_project_items, -> { where(content_type: "PullRequest") },
    class_name: "MemexProjectItem",
    foreign_key: :content_id
  destroy_dependents_in_background :memex_project_items

  has_one :pull_request_import, dependent: :destroy
  has_one :import, through: :pull_request_import

  has_many :merge_queue_entries, dependent: :destroy

  # This will create the following methods:
  #
  # - fork_collab_denied?
  # - fork_collab_denied!
  # - fork_collab_allowed?
  # - fork_collab_allowed!
  #
  enum fork_collab_state: { denied: 0, allowed: 1 }, _prefix: "fork_collab"

  before_validation :record_concrete_commit_points, on: :create, unless: :importing?
  before_validation :set_contributed_at, on: :create

  after_create :subscribe_author
  after_create :clear_contributions_cache
  after_create :detect_mobile_apps
  after_create :detect_docker_file

  after_save :sync_issue_updated_at
  after_touch :sync_issue_updated_at

  after_save :record_tracking_ref_maintenance_required
  after_commit :maintain_tracking_ref_later, on: [:create, :update]

  after_commit :notify_socket_subscribers, on: :update
  after_commit :synchronize_search_index

  validates_presence_of :head_ref, :base_ref
  validates_presence_of :head_sha, :base_sha
  validates_presence_of :contributed_at, on: :create
  validates_presence_of :head_user_id, :base_user_id
  validates_presence_of :head_repository_id, :repository_id, :base_repository_id

  validate :repository_and_base_repository_must_match,
    on: :create, unless: [:in_advisory_workspace?, :importing?]
  validate :head_repository_must_be_advisory_workspace,
    on: :create, if: :in_advisory_workspace?, unless: :importing?
  validate :base_repository_must_be_advisory_workspace_origin_repository,
    on: :create, if: :in_advisory_workspace?, unless: :importing?
  validate :must_have_commits, on: :create, unless: :importing?
  validate :must_have_common_ancestor, on: :create, unless: :importing?
  validate :duplicate_check, on: :create, unless: :importing?
  validate :base_ref_is_real_branch, on: :create, unless: :importing?
  validate :head_ref_is_real_branch, on: :create, unless: :importing?
  validate :head_ref_is_not_namespaced, on: :create, unless: :importing?
  validate :validate_authorized_to_create_content, on: :create, unless: :importing?
  validate :validate_refs_readable, on: :create
  validate :user_can_interact?, on: :create
  validate :valid_cross_repo_collab, on: :create
  validate :validate_under_duplicate_head_sha_limit, on: :create

  validates :fork_collab_state, presence: true

  # fetch pull requests issues by a Label id or Label name
  scope :labeled, lambda { |label_or_collection|
    case label_or_collection
      when Integer
        joins(:issue).joins(
          " INNER JOIN `issues_labels` ON `issues_labels`.`issue_id` = `issues`.`id`
            INNER JOIN `labels` ON `labels`.`id` = `issues_labels`.`label_id`",
        ).where("labels.id = ?", label_or_collection)
      when Array
        joins(:issue).joins(
          " INNER JOIN `issues_labels` ON `issues_labels`.`issue_id` = `issues`.`id`
            INNER JOIN `labels` ON `labels`.`id` = `issues_labels`.`label_id`",
        ).where("labels.name IN (?)", label_or_collection.map { |l| ActiveRecord::Type::Binary::Data.new(l) })
    end
  }

  scope :public_scope,       -> { joins(:repository).where("repositories.public = ?", true) }

  scope :filter_for_dmca,    -> { joins(:repository).where("repositories.disabled_at IS NULL") }

  scope :for_repository, lambda { |repository|
    where(repository_id: repository)
  }

  scope :from_repository, lambda { |head_repository|
    where(head_repository_id: head_repository)
  }

  scope :to_repository, lambda { |base_repository|
    where(base_repository_id: base_repository)
  }

  scope :for_user, lambda { |user|
    where(user_id: user)
  }

  scope :for_organization, lambda { |organization|
    joins(:repository).where("repositories.organization_id = ?", organization)
  }

  scope :excluding_organization_ids, -> (ids) do
    if ids.any?
      joins(:repository).
        where("repositories.organization_id IS NULL OR repositories.organization_id NOT IN (?)", ids)
    else
      scoped
    end
  end

  scope :open_based_on_ref, -> (repository, ref) {
    ids = find_open_ids_based_on_ref(repository, ref)
    includes(:issue).where(id: ids)
  }

  #
  # Basic Attributes
  #

  delegate :number, :title, :close, :closed_at, :closed_by, :safe_closed_by,
    :events, :subscribers, :body, :body_html, :body_text,
    :body_version, :mentioned_users, :mentioned_teams,
    :subscribe, :unsubscribe, :subscribed?, :ignore, :task_list, :task_list?,
    :task_list_summary, :notifications_list, :notifications_thread,
    :notifications_author, :entity, :issue_comments_count, :subscriptions,
    :get_event_participants, :organization, :owner, :assignees, :labels,
    :milestone,
    to: :issue

  def readable_by?(actor)
    async_readable_by?(actor).sync
  end

  def async_readable_by?(actor)
    async_repository.then do |repository|
      next false unless repository

      repository.resources.pull_requests.async_readable_by?(actor)
    end
  end

  def labelable_by?(actor:)
    issue&.labelable_by?(actor: actor)
  end

  def async_notifications_list
    async_issue.then(&:async_notifications_list)
  end

  def async_truncated_body_html(max)
    async_issue.then { |issue| issue.async_truncated_body_html(max) }
  end

  def async_body_html
    async_issue.then(&:async_body_html)
  end

  def async_body_text
    async_issue.then(&:async_body_text)
  end

  # If there is an associated issue, it needs to have the same timestamp.
  #
  # The issue may have already been updated, so verify that the timestamp
  # is out of date before updating to avoid a redundant query (see #57779).
  def sync_issue_updated_at
    return if issue.nil? || issue.frozen? || issue.new_record?

    if issue.updated_at.to_i < updated_at.to_i
      issue.update_column(:updated_at, updated_at)
    end
  end

  def to_param
    issue.number.to_s
  end

  def base
    return if base_user.nil?
    "#{base_user.login}:#{base_ref}"
  end

  def head
    "#{safe_head_user.login}:#{head_ref}"
  end

  def base_label(username_qualified: cross_repo?)
    if username_qualified
      return if base_user.nil?
      "#{base_user.login}:#{display_base_ref_name}"
    else
      display_base_ref_name
    end
  end

  def head_label(username_qualified: cross_repo?)
    if username_qualified
      return if head_user.nil?
      "#{safe_head_user.login}:#{display_head_ref_name}"
    else
      display_head_ref_name
    end
  end

  def base_ref_name
    safe_ref_name(base_ref)
  end

  def head_ref_name
    safe_ref_name(head_ref)
  end

  def display_base_ref_name
    base_ref_name.force_encoding("utf-8").scrub!
  end

  def display_head_ref_name
    head_ref_name.force_encoding("utf-8").scrub!
  end

  def raw_head_ref_name
    head_ref_name.b
  end

  def safe_head_user
    head_user || User.ghost
  end

  def safe_user
    user || User.ghost
  end

  def open(opener)
    issue = self.issue
    issue.pull_request = self
    issue.open(opener)
  end

  def open?
    issue.nil? || issue.open?
  end

  def after_close(actor)
    unless merged?
      GlobalInstrumenter.instrument("pull_request.close", {
        actor: actor,
        pull_request: self,
      })
    end

    async_destroy_merge_refs
    destroy_conflict_metadata
  end

  def closed?
    !open?
  end

  def state
    return :open     if open?
    return :merged   if merged?
    return :closed   if closed?
  end

  def async_subscription_status(user)
    async_issue.then do |issue|
      issue.async_subscription_status(user)
    end
  end

  # Called when a closed but unmerged pull request is reopened. Updates
  # the base and head SHA1s based on the current branch state. This method
  # is called by Issue#open to ensure any new commits fill into the newly
  # reopened pull request but only when the #reopenable? method returns true.
  def reopened(opener)
    synchronize!(user: opener, repo: base_repository)
    GlobalInstrumenter.instrument("pull_request.reopen", {
      pull_request: self,
      actor: opener,
    })
    true
  rescue DetermineCodeownersError
    false
  end

  # Determine if this pull request is reopenable. Pull Requests that have
  # already been merged cannot be reopened because merging causes a pull request
  # to automatically close. You also cannot reopen pull requests whose base or
  # head branches no longer exist. You also cannot reopen a pull request whose
  # head repository is private but base repository is public. They're
  # automatically closed when a repository goes private.
  #
  # Returns true when the pull request can be reopened.
  def reopenable?
    GitHub.dogstats.time("pull_request", tags: ["action:reopenable"]) do
      !merged? && !find_existing && refs_exist? &&
        !(head_repository.private? && repository.public?) &&
          !is_head_merged_into_base? &&
          old_head_connected_to_new? &&
          common_ancestor?
    end
  rescue GitRPC::ObjectMissing, Repository::CommandFailed
    false
  end

  # Give a reason why this PR is not reopenable
  # Returns a String reason, or nil if this state doesn't deserve a reason
  def not_reopenable_reason
    return if reopenable?  # reopenable, so no reason

    # no special messages for these cases
    return if merged?
    return if head_repository.try(:private?) && repository.public?

    if !head_repository?
      "The repository that submitted this pull request has been deleted."
    elsif find_existing
      "There is already an open pull request from #{head_label} to #{base_label}."
    elsif bad_refs = missing_refs
      if bad_refs.length == 1
        "The #{bad_refs.first} branch has been deleted."
      else
        "The #{bad_refs.join(" and ")} branches have been deleted."
      end
    elsif historical_comparison.zero_commits?
      "There are no new commits on the #{head_label} branch."
    elsif is_head_merged_into_base?
      "These commits are already merged."
    elsif !old_head_connected_to_new?
      "The #{display_head_ref_name} branch was force-pushed or recreated."
    elsif !common_ancestor?
      "The #{head_label} branch has no history in common with #{base_label}."
    end
  rescue GitRPC::ObjectMissing, Repository::CommandFailed
    "The repository may be missing relevant data. Please contact support for more information."
  end

  # Internal: set the error message for this PR not being reopenable
  # Currently called only from Issue#open

  # Adds an error to the :state field
  # Returns nothing
  def set_not_reopenable_error
    message  = "cannot be changed. "
    message += not_reopenable_reason || "The pull request cannot be reopened."
    errors.add(:state, message)
  end

  # Absolute permalink URL for this pull request.
  #
  # include_host - Turn off the `GitHub.url` host in the url. (default true)
  #                pull_request.permalink(include_host: false) => `/github/github/pull/4`
  #
  def permalink(include_host: true)
    return nil unless repository.present?
    "#{repository.permalink(include_host: include_host)}/pull/#{number}"
  end
  alias url permalink

  def async_path_uri
    return @async_path_uri if defined?(@async_path_uri)

    @async_path_uri = async_issue.then(&:async_path_uri)
  end

  def async_uri_dependencies
    return @async_uri_dependencies if defined?(@async_uri_dependencies)

    @async_uri_dependencies = Promise.all([
      async_issue,
      async_repository.then(:async_owner),
    ]).then do |issue, owner|
      [owner, repository, issue]
    end
  end

  def async_revert_path_uri
    async_uri_dependencies.then do |owner, repo, issue|
      REVERT_URI_TEMPLATE.expand(owner: owner.login, name: repository.name, number: issue.number)
    end
  end

  def async_restore_head_ref_path_uri
    async_uri_dependencies.then do |owner, repo, issue|
      RESTORE_HEAD_REF_URI_TEMPLATE.expand(owner: owner.login, name: repository.name, number: issue.number)
    end
  end

  def async_dismiss_merge_tip_path_uri
    async_uri_dependencies.then do |owner, repo, issue|
      DISMISS_MERGE_TIP_URI_TEMPLATE.expand(owner: owner.login, name: repository.name, number: issue.number)
    end
  end

  def last_modified_at
    @last_modified_at ||= last_modified_with :user
  end

  # Unique identifier for this pull request used in email notifications.
  def message_id
    return nil unless repository.present?
    "<#{repository.name_with_owner}/pull/#{number}@#{GitHub.urls.host_name}>"
  end

  # Generate a default title based on the head ref branch name.
  #
  #   "less_debris" => "Less debris"
  #   "rails-2.2.3" => "Rails 2.2.3"
  #   "various_fixes_and_stuff" => "Various fixes and stuff"
  #
  # Returns nil when the comparison head is "master", "gh-pages", or
  # a commit SHA1.
  def default_title
    return if comparison.head.blank? || comparison.head_ref =~ /^[0-9a-f]{7,40}(?:[~^@{].*)?$/

    if comparison.total_commits == 1
      commit = comparison.commits.first
      if commit.empty_message?
        ""
      else
        GitHub::CommitMessage.new(commit.message).subject
      end
    else
      return if head_ref == "gh-pages"
      return if head_ref == comparison.head_repo.default_branch
      comparison.display_head_ref.underscore.humanize
    end
  end

  # A file under .github/PULL_REQUEST_TEMPLATES to read body content from
  attr_accessor :body_template_name

  # Public: Find the user supplied template to use in the body
  # of new pull requests if one exists.
  #
  # Read from a file under .github/PULL_REQUEST_TEMPLATES or
  # PULL_REQUEST_TEMPLATE.md.
  #
  # Returns a String from the template if one is found.
  # Returns nil if no template is found.
  def body_template
    return if repository.blank?
    return @body_template if defined?(@body_template)
    return @body_template = repository.preferred_pull_request_template&.data unless body_template_name.present?

    local_file = PreferredFile.find(
      directory: repository.root_directory,
      type: :pull_request_template,
      nested_filename: body_template_name,
    )&.data

    if !local_file.present? && repository.owner.organization? && !repository.global_health_files_repository?
      global_repo = Platform::Loaders::GlobalHealthFilesRepository.load(repository.owner_id).sync
      return @body_template = nil unless global_repo.present?

      @body_template = PreferredFile.find(
        directory: global_repo.root_directory,
        type: :pull_request_template,
        nested_filename: body_template_name,
      )&.data
    else
      @body_template = local_file
    end
  end

  def has_body_template?
    !!body_template
  end

  def template
    @template ||= repository.preferred_issue_templates[body_template_name]
  end

  def default_body
    body_parts = []

    if comparison.total_commits == 1
      commit = comparison.commits.first
      if !commit.empty_message?
        message = GitHub::CommitMessage.new(commit.message)
        body_parts << message.body if message.body.present?
      end
    end

    if has_body_template?
      body_parts << body_template
    end

    body_parts.join("\n\n")
  end

  # Makes this class compatible with IssueComments for rendering the body.
  # See GitHub::UserContent#body_pipeline for more info.
  def formatter
    :markdown
  end

  def created_via_email
    false
  end

  alias_attribute :draft, :work_in_progress

  def can_change_draft_state?(user)
    self.user == user || repository.pushable_by?(user)
  end

  def can_mark_ready_for_review?(user)
    return unless open?
    return unless draft?
    return unless repository.plan_supports?(:draft_prs)
    return unless user.present?
    can_change_draft_state?(user)
  end

  def can_convert_to_draft?(user)
    return unless open?
    return if draft?
    return unless repository.plan_supports?(:draft_prs)
    return unless user.present?
    can_change_draft_state?(user)
  end

  def author_or_committer?(user)
    self.user == user || changed_commits.map(&:author).include?(user)
  end

  # Public: Mark this pull request as ready for review
  #
  # `user` — The user who marked it ready for review, so review requests come
  # from the correct person
  def ready_for_review!(user:)
    return unless draft?

    if repository.pull_request_revisions_enabled?
      revisions_count = revisions.count
      first_revision = revisions.first if revisions_count > 0
    end

    transaction do
      # Lock the first revision to prevent a race condition against another process
      # updating revisions. Lock it immediately so that the transaction snapshot reflects
      # the state after the lock is released by another contending process.
      first_revision.lock! if first_revision

      self.draft  = false
      self.save!

      events.create!(event: "ready_for_review", actor: user)

      next unless first_revision

      # Check if a race condition has beaten us to an update.
      next unless revisions.count == revisions_count

      latest_revision = revisions.last
      next unless latest_revision.draft?

      # If the PR head hasn't moved since the current draft revision was
      # created then we'll just revert to the previous ready revision by
      # deleting the current draft revision.
      if latest_revision.base_oid == head_sha
        latest_revision.destroy!
        next
      end

      commits_count = repository.comparison(
        latest_revision.base_oid,
        head_sha,
        COMMIT_LIMIT,
        self
      ).total_commits

      latest_revision.update!(
        ready: true,
        head_oid: head_sha,
        commits_count: commits_count,
        revised_at: Time.now.utc,
        actor: user,
      )
    end

    channel = GitHub::WebSocket::Channels.pull_request_review_state(self)
    GitHub::WebSocket.notify_pull_request_channel(
      self,
      channel,
      {
        wait: default_live_updates_wait,
        pull_request_id: id,
      },
    )

    RequestPullRequestReviewersJob.perform_later(self, user)

    GlobalInstrumenter.instrument("pull_request.ready_for_review", {
      pull_request: self,
      actor: user,
    })

    instrument(:ready_for_review, actor: user)
  end

  # Public: Is this pull request ready for review?
  #
  # Returns Boolean
  def ready_for_review?
    !draft?
  end

  def convert_to_draft(user:)
    return if draft?

    if repository.pull_request_revisions_enabled?
      revisions_count = revisions.count
      first_revision = revisions.first if revisions_count > 0
    end

    transaction do
      # Lock the first revision to prevent a race condition against another process
      # updating revisions. Lock it immediately so that the transaction snapshot reflects
      # the state after the lock is released by another contending process.
      first_revision.lock! if first_revision

      self.draft = true
      self.save!

      events.create!(event: "convert_to_draft", actor: user)

      next unless first_revision

      # Check if a race condition has beaten us to an update.
      next unless revisions.count == revisions_count

      latest_revision = revisions.last
      next unless latest_revision.ready?

      revisions.create!(
        number: latest_revision.number + 1,
        ready: false,
        force_pushed: false,
        base_oid: latest_revision.head_oid,
        head_oid: head_sha,
        commits_count: 0,
        revised_at: Time.now.utc,
        actor: user,
      )
    end

    channel = GitHub::WebSocket::Channels.pull_request_review_state(self)
    GitHub::WebSocket.notify_pull_request_channel(
      self,
      channel,
      {
        wait: default_live_updates_wait,
        pull_request_id: id,
      },
    )

    instrument(:converted_to_draft, actor: user)

    params = {
      actor: user,
      pull_request: self
    }
    GlobalInstrumenter.instrument("pull_request.converted_to_draft", params)
  end

  # Public: The latest ready revision for this pull request.
  #
  # Returns a PullRequestRevision or nil.
  def current_revision
    return @current_revision if defined?(@current_revision)
    if base_repository.pull_request_revisions_enabled?
      @current_revision = revisions.ready.number_desc.first
    else
      @current_revision = nil
    end
  end

  # Internal: Notify subscribers that this Pull Request has been updated.
  #
  # Returns Set of channel id Strings that were notified.
  def notify_socket_subscribers
    # there is currently no reason to notify for new Pull Requests
    return if previous_changes.include?("id")

    # it seems that `after_commit ... :on => :update` doesn't work correctly
    return if self.destroyed? || !repository

    timestamp = Time.now.to_i

    data = {
      timestamp: timestamp,
      wait: default_live_updates_wait,
      reason: "pull request ##{id} updated",
      gid: global_relay_id,
    }

    channel = GitHub::WebSocket::Channels.pull_request(self)
    GitHub::WebSocket.notify_pull_request_channel(self, channel, data)
  end

  #
  # Merging
  #

  # Determine if the head has been merged into the base.
  def merged?
    !merged_at.nil?
  end

  def common_ancestor?
    historical_comparison.common_ancestor?
  end

  # Determine whether the base ref exists.
  def base_ref_exist?(refresh_refs: false)
    return false unless base_repository
    base_repository.clear_ref_cache if refresh_refs
    base_repository.heads.exist?(base_ref)
  end

  # For haystack-tracking creation of PRs with non-branch heads
  # https://github.com/github/github/pull/33519
  class NonBranchHeadError < StandardError
    def initialize(head)
      @head = head
    end

    def message
      "#{@head} is not a valid branch"
    end
  end

  # Determine whether the head ref exists.
  def head_ref_exist?(refresh_refs: false)
    return false unless head_repository && head_ref
    head_repository.clear_ref_cache if refresh_refs
    head_repository.heads.exist?(head_ref)
  end

  # Get missing ref names
  # Returns Array (1 or 2 elements), or nil if no refs are missing
  def missing_refs
    result = []
    result.push display_base_ref_name if !base_ref_exist?
    result.push display_head_ref_name if !head_ref_exist?
    result.presence
  end

  # Determine if the head_ref can be safely deleted after a pull request is
  # merged. The head_ref must be deleteable by the user, and must not have
  # any commits ahead of the base.
  #
  # Returns true if we can safely delete the lingering branch, false otherwise
  def head_ref_safely_deleteable_by?(user)
    head_ref_deleteable_by?(user) && (merged? || is_head_merged_into_base?)
  end

  # Determine if the head_ref can be deleted in a way that could cause
  # commits to be lost. The pull request must be closed, the head_ref
  # must be deleteable by the user and contain commits that were not
  # merged into the base.
  #
  # Returns true if the head_ref can be deleted but the branch contains
  # unmerged commits, false otherwise
  def head_ref_unsafely_deleteable_by?(user)
    head_ref_deleteable_by?(user) && !(merged? || is_head_merged_into_base?)
  end

  # Determine if the head_ref can be deleted at all. The PR
  # must be closed, the head_repository pushable by the user,
  # the branch must exist and not be the repo's default branch,
  # and must point at the same commit that it did when the PR
  # was merged or closed. There cannot be any other open PRs
  # using the branch as its head or base.
  #
  # Returns true if the head_ref is deleteable, false otherwise.
  def head_ref_deleteable_by?(user)
    async_head_ref_deleteable_by?(user).sync
  end

  def async_head_ref_deleteable_by?(user)
    async_head_ref_deleteable_by_user_cache(user).then do |deleteable|
      deleteable && !other_open_pulls_using_head_ref?
    end
  end

  # Internal: Determine if deletion would be allowed so long
  #           as other pull requests in this repository that
  #           reference the head ref as their base ref will be
  #           first updated to use this pull's base instead.
  #
  # Returns a Boolean.
  def head_ref_deleteable_after_updating_dependents?(user)
    async_head_ref_deleteable_after_updating_dependents?(user).sync
  end

  def async_head_ref_deleteable_after_updating_dependents?(user)
    async_head_ref_deleteable_by_user_cache(user).then do |deleteable|
      deleteable &&
        same_repo? &&
        merged? &&
        !other_open_pulls_using_head_ref_as_head?
    end
  end

  private def async_head_ref_deleteable_by_user_cache(user)
    @async_head_ref_deleteable_by_user_cache ||= { nil => Promise.resolve(false) }

    if @async_head_ref_deleteable_by_user_cache.key?(user&.id)
      return @async_head_ref_deleteable_by_user_cache[user&.id]
    end

    @async_head_ref_deleteable_by_user_cache[user.id] =
      Promise.all([
        async_head_repository,
        async_issue,
        async_head_repository_pushable_by?(user)
      ]).then do |head_repo, issue, pushable_by_user|
        next false unless head_repo && closed?

        async_head_repository.then do |head_repo|
          head_repo.async_network.then do
            head_reference = head_repo.heads.find(head_ref.b)
            next false unless head_reference

            head_reference.deleteable? &&
              head_reference.target_oid == head_sha &&
              pushable_by_user
          end
        end
      end
  end

  private def falsify_head_ref_deleteable_by_user_cache
    promise = Promise.resolve(false)
    @async_head_ref_deleteable_by_user_cache&.transform_values! { |_| promise }
  end

  def other_open_pulls_using_head_ref_as_head?
    other_open_pulls_using_head_ref_as[:head]
  end

  def other_open_pulls_using_head_ref_as_base?
    other_open_pulls_using_head_ref_as[:base]
  end

  def other_open_pulls_using_head_ref?
    other_open_pulls_using_head_ref_as_base? || other_open_pulls_using_head_ref_as_head?
  end

  private def other_open_pulls_using_head_ref_as
    return @other_open_pulls_using_head_ref_as if defined?(@other_open_pulls_using_head_ref_as)

    values = github_sql.results <<-SQL, repository_id: head_repository.id, ref: GitHub::SQL::BINARY(head_ref)
      SELECT EXISTS (
        SELECT pr.id
        FROM pull_requests pr
        JOIN issues ON pr.id = issues.pull_request_id AND issues.repository_id = pr.repository_id
        WHERE
          pr.base_repository_id = :repository_id
          AND pr.base_ref = :ref
          AND issues.state = 'open'
      ) AS as_base,
      EXISTS (
        SELECT pr.id
        FROM pull_requests pr
        JOIN issues ON pr.id = issues.pull_request_id AND issues.repository_id = pr.repository_id
        WHERE
          pr.head_repository_id = :repository_id
          AND pr.head_ref = :ref
          AND issues.state = 'open'
      ) AS as_head
    SQL
    values.flatten!
    @other_open_pulls_using_head_ref_as = {
      base: values.first == 1,
      head: values.second == 1,
    }
  end

  # Public: Determine if this pull request or its repository are hidden from the given user.
  #
  # user - a User
  #
  # Returns a Promise that resolves to a Boolean.
  def async_hidden_from_or_repo_hidden_from?(user)
    async_repository.then do |repo|
      Promise.all([repo.async_hide_from_user?(user),
                   async_hide_from_user?(user)]).then do |repo_hidden, pr_hidden|
        repo_hidden || pr_hidden
      end
    end
  end

  # Public: Determine if this pull request's repository is readable by the given user.
  #
  # user - a User
  #
  # Returns a Promise that resolves to a Boolean.
  def async_repository_metadata_readable_by?(user)
    async_repository.then do |repo|
      repo.resources.metadata.readable_by?(user)
    end
  end

  def async_base_repository_pushable_by?(user)
    async_base_repository.then do |base_repo|
      (base_repo&.async_writable_by?(user) || Promise.resolve(false))
    end
  end

  # Public: Determine if this pull request's head repository is pushable by the
  #         given user, where "pushable" is defined as write access to the head
  #         repository's git contents
  #
  # user - a User
  #
  # Returns a Promise that resolves to a Boolean.
  def async_head_repository_pushable_by?(user)
    async_head_repository.then do |head_repo|
      next false unless head_repo
      head_repo.resources.contents.async_writable_by?(user)
    end
  end

  def head_ref_pushable_by?(user)
    return false unless head_repository
    head_repository.pushable_by?(user, ref: head_ref)
  end

  def head_repository_pullable_by?(user)
    return false unless head_repository
    head_repository.pullable_by?(user)
  end

  # Delete the head_ref for a pull request if the head_ref can be safely
  # deleted.
  #
  # Returns true if we deleted the branch, false otherwise
  def cleanup_head_ref(user, reflog_data: {}, allow_updating_dependents: false)
    unless head_ref_safely_deleteable_by?(user) ||
           head_ref_unsafely_deleteable_by?(user) ||
           (allow_updating_dependents && head_ref_deleteable_after_updating_dependents?(user))
      return false
    end

    if allow_updating_dependents && head_ref_deleteable_after_updating_dependents?(user)
      return false unless update_base_of_dependent_prs(actor: user)
    end

    ref = head_repository.heads.build(head_ref, head_sha)
    reflog = pr_reflog_data("cleanup head ref").merge(reflog_data)
    ref.delete(user, reflog_data: reflog)
    create_head_ref_event(:deleted, user)
    falsify_head_ref_deleteable_by_user_cache
    true
  rescue Git::Ref::ComparisonMismatch
    false
  end

  def head_ref_restorable_by?(user)
    async_head_ref_restorable_by?(user).sync
  end

  # Checks if the provided user has permission to restore the PR's head ref
  def async_head_ref_restorable_by?(user)
    return @async_head_ref_restorable_by[user] if defined?(@async_head_ref_restorable_by)

    @async_head_ref_restorable_by = {}
    @async_head_ref_restorable_by[user] = async_head_ref_restorable_by!(user)
  end

  def async_head_ref_restorable_by!(user)
    return Promise.resolve(false) unless head_ref

    async_repository.then do |repo|
      next false if repo.locked_on_migration?

      async_head_repository.then do |head_repo|
        next false unless head_repo
        head_repo.async_network.then do
          next false if head_repo.heads.exist?(head_ref)
          async_head_repository_pushable_by?(user)
        end
      end
    end
  end

  # Restores the head_ref for a pull request if it doesn't exist.
  #
  # Returns true if we restored the branch, or false.
  def restore_head_ref(user, reflog_data = {})
    return false unless head_ref_restorable_by?(user)

    ref = head_repository.heads.build(head_ref)
    reflog = pr_reflog_data("restore head ref").merge(reflog_data)

    ref.create(head_sha, user, reflog_data: reflog)
    create_head_ref_event(:restored, user)

    true
  rescue Git::Ref::ComparisonMismatch
    false
  end

  def has_required_objects?
    return true unless cross_repo?

    if cross_network?
      repository.rpc.object_exists?(mergeable_base_sha, "commit")
    else
      repository.rpc.object_exists?(head_sha, "commit")
    end
  end

  # Can this pull request be merged? If the mergeable state is unknown,
  # fire off a background job to update mergeable state.
  #
  # enqueue - Boolean that determines if the background job should be fired.
  #           Default: true.
  #
  # Returns true/false, or nil if unknown.
  def git_merges_cleanly?(enqueue: true)
    enqueue_mergeable_update if enqueue
    currently_mergeable?
  end

  # Public: Enqueues a background job to create a new merge commit if
  #         the current mergeability status is out of date.
  def enqueue_mergeable_update
    return unless open?

    # currently_mergeable? is not a true predicate method and in this
    # particular scenario we are specifically interested in the nil state.
    # Therefore we need to do this somewhat awkward looking nil check
    # on a predicate method's return value.
    return unless currently_mergeable?.nil?

    CreatePullRequestMergeCommitJob.perform_later(id)
  end

  # Public: Is this pull request mergeable?
  #
  # Returns true if so, false if not, or nil if outdated or uncomputed
  def currently_mergeable?
    return false if !base_ref_exist?

    GitHub.dogstats.increment("pull_request", tags: ["action:git_merges_cleanly"])

    if mergeable?
      merge_commit_up_to_date? ? true : nil
    else
      mergeable
    end
  end

  # Public: Get a merge state, dependant on the viewer. This will cache
  # intelligently on the viewer argument, so this method is preferable to merge_state.
  #
  # viewer: the viewing user for context
  #
  # Returns MergeState
  def cached_merge_state(viewer: nil)
    @merge_states ||= Hash.new do |h, key|
      h[key] = MergeState.new(self, viewer: key)
    end
    @merge_states[viewer]
  end

  # Get merge state for view.
  #
  # enqueue: a Boolean of whether or not a background job should be enqueued to find the mergeable state.
  #          Default: true.
  # viewer: the viewing user for context
  #
  # Returns a new MergeState
  def merge_state(viewer: nil)
    MergeState.new(self, viewer: viewer)
  end

  # Public: The user that merged the pull request.
  #
  # Returns User
  def merged_by
    return nil unless merged?

    events.merges.last.try(:actor)
  end

  # Public: Test whether the merge_commit_sha is unset or is set but references a
  # missing git object.
  #
  # Used to determine whether the merge commit should be rebuilt in PR#merge.
  #
  # Returns Boolean
  def merge_commit_missing?
    merge_commit_sha.blank? ||
      !repository.commits.exist?(merge_commit_sha)
  end

  # Public: Test whether the merge_commit_sha is set and up to date.
  #
  # Up to date means it's pointing to the current mergeable head
  # and base commits.
  #
  # Returns Boolean
  def merge_commit_up_to_date?
    return false if merge_commit_sha.blank? ||
                    mergeable_base_sha.blank? ||
                    mergeable_head_sha.blank?

    merge_commit = repository.commits.find(merge_commit_sha)
    merge_commit.parent_oids == [mergeable_base_sha, mergeable_head_sha]
  rescue GitRPC::ObjectMissing => e
    # see github/github#54955 - merge commit may have been GC'd
    false
  end

  # Sets the mergeable attribute like update_attribute :mergeable but does not
  # change the updated_at timestamp. Otherwise, the pull request is displayed as
  # updated every time the base ref changes and the mergeable flag is reset.
  def update_mergeable_attribute(value)
    write_attribute(:mergeable, value)
    PullRequest.where(id: id, merged_at: nil).update_all(mergeable: value) if id
    value
  end

  # Run RPC#create_merge_commit to generate a merge commit for this
  # pull request. Sets #mergeable to true/false based on the result
  #
  # Returns the merge commit sha on success,
  # false when there were merge conflicts,
  # or nil on other errors.
  def create_merge_commit(priority: :high, skip_rebase: false)
    return nil if merged_at?
    return nil if closed?
    return false if !base_ref_exist?

    if cross_network? && !has_required_objects?
      return false if !head_ref_exist?

      head_reference = head_repository.heads.find(head_ref)
      comparison = build_comparison(base_commit_oid: current_base_sha, head_commit_oid: head_reference.target_oid)
      comparison.prepare!(origin_for_stats: "create_merge_commit")
    end

    return false if !has_required_objects?

    # #mergeable is nullified any time the pull is synchronized so
    # #conflict must be used to determine the prior mergeable state
    previous_mergeable = conflict.nil?
    previously_unknown = mergeable_unknown?
    mergeable, merge_commit_sha = Prepare.new(pull: self, priority: priority, skip_rebase: skip_rebase).perform

    update_count = 0

    if mergeable
      update_count = PullRequest.where(id: id, head_sha: head_sha, merged_at: nil).update_all({
        mergeable: mergeable,
        merge_commit_sha: merge_commit_sha,
      })

      if update_count > 0
        write_attribute(:mergeable, mergeable)
        write_attribute(:merge_commit_sha, merge_commit_sha)
        clear_attribute_changes([:mergeable, :merge_commit_sha])

        destroy_conflict_metadata
      else
        write_attribute(:mergeable, nil)
        clear_attribute_changes([:mergeable])
      end
    else
      update_count = PullRequest.where(id: id, head_sha: head_sha).update_all(mergeable: mergeable)

      if update_count > 0
        write_attribute(:mergeable, mergeable)
        clear_attribute_changes([:mergeable])
      else
        write_attribute(:mergeable, nil)
        clear_attribute_changes([:mergeable])
      end
    end

    if update_count > 0
      synchronize_search_index

      if previously_unknown || mergeable != previous_mergeable
        instrument_mergeability
      end
    end

    mergeable && merge_commit_sha
  end

  # Returns true when we haven't successfully calculated git mergeability
  def mergeable_unknown?
    !mergeable && conflict.nil?
  end

  def instrument_mergeability
    return if mergeable_unknown?

    instrument(
      :mergeability,
      pull_request_id: id,
      mergeable: mergeable,
    )
  end

  # Public: Does this Pull Request contain any merge commits?
  def contains_merges?
    repository.rpc.rev_list(mergeable_head_sha, {
      merges: true,
      limit: 1,
      exclude_oids: [mergeable_base_sha],
    }).length > 0
  end

  def rebase_state
    return @rebase_state if defined? @rebase_state

    @rebase_state = RebaseState.new(self)
  end

  # Public: Was a rebase prepared successfully?
  def rebase_prepared?
    rebase_state.prepared?
  end

  # Public: Was a rebase prepared and is it "safe"?
  def rebase_safe?
    rebase_state.safe?
  end

  # Builds the full ref for Pull Request merge commits.
  #
  # Returns a String ref name.
  def merge_ref
    @merge_ref ||= "refs/pull/#{number}/merge"
  end

  # Builds the full ref for Pull Request rebase commits.
  #
  # Returns a String ref name.
  def rebase_ref
    @rebase_ref ||= "refs/__gh__/pull/#{number}/rebase"
  end

  # Perform the merge described by this pull request.
  # Requires a #git_merges_cleanly? pull prepared via #create_merge_commit.
  #
  # actor   - User object of the GitHub user initiating the merge
  #           (optional, defaults to PR creator)
  # author_email - Custom email the user has chosen for this merge commit
  #                 (optional, if user doesn't make a selection will default to git email)
  # message_title - String commit message title to prefix the merge message
  #                 (optional, defaults to the default
  #                 "Merge pull request #N from repo/branch" text)
  # message - String commit message to append to the merge message
  #           (optional, defaults to PR title)
  # reflog_data   - Hash of reflog data to go with the ref update.
  #                 (optional)
  # expected_head - String commit OID where PR head ref is expected to be
  #                 (optional)
  # method - Symbol that sets the merge method to perform. Can be one of
  #          :merge, :squash or :rebase. (optional, defaults to :merge)
  #
  # Returns two-or-three-element array
  #   on success: [true,  the merge commit SHA]
  #   on failure: [false, explanatory failure message, Symbol failure code]
  def merge(actor = user, author_email: nil, message_title: nil, message: nil, reflog_data: {}, expected_head: nil, method: :merge)
    merger = Merge.new(self, method, actor, author_email: author_email, message_title: message_title, message: message)
    validate_result = merger.prepare_and_validate(expected_head)
    return [validate_result.success, validate_result.fail_message, validate_result.fail_code] unless validate_result.success
    merge_commit = validate_result.merge_commit
    merge_base_ref = validate_result.merge_base_ref

    perform_result = merger.perform
    return [perform_result.success, perform_result.fail_message, perform_result.fail_code] unless perform_result.success
    new_sha = perform_result.new_sha

    if new_sha
      # now that the new commit object is there, update the pull request and branches
      merge_base_ref.update(new_sha, actor, policy_commit: merge_commit,
        reflog_data: pr_reflog_data("merge").merge(reflog_data),
        post_receive: false
      )

      post_merge(actor, merge_commit, merge_base_ref, new_sha, method)

      if head_repository&.delete_branch_on_merge?
        cleanup_head_ref(actor, allow_updating_dependents: true)
      end

      [true, new_sha]
    else
      [false, "Could not re-write the merge commit for some reason", :rewrite]
    end
  rescue Git::Ref::ProtectedBranchUpdateError => e
    [false, e.result.message, :protected_branch]
  rescue Git::Ref::ComparisonMismatch
    [false, "Base branch was modified. Review and try the merge again.", :parent_mismatch]
  end
  time_method :merge, key: "pullrequest.merge"

  def post_merge(actor, merge_commit, merge_base_ref, new_sha, method)
    merge_base_sha, _ = merge_commit.parent_oids

    # Ensure that we don't leave merges in a half finished state (e.g. closing referenced
    # issues when the pull fails to be marked as merged). Also makes sure side effects tied to
    # after_commit hooks are not fired until all relevant records are committed to database.
    PullRequest.transaction do
      repository.update_pushed_at Time.now
      reload

      issue.reference_from_commit(actor, new_sha, repository)
      mark_as_merged(actor, new_sha, merge_base_sha)
      save!
    end

    merge_base_ref.enqueue_push_job(merge_base_sha, new_sha, actor, "merge-button")

    # There have been a handful of cases of PRs being merged and not closed.
    # Sometimes it's due to a timeout and sometimes the request returns a 200
    # and there is no corresponding timeout needle. This will report to Failbot
    # if the bad state is not due to a timeout. See Issue::State#close for
    # additional reporting for the same bug.
    #
    # TODO: remove once we've determined the cause of the bug.
    #
    if reload && merged? && open?
      boom = PRMergedAndNotClosed.new("PR not closed in post-merge")
      boom.set_backtrace(caller)
      Failbot.report(boom, pull_request_id: id, app: "github-pull-requests")
    end

    GlobalInstrumenter.instrument("pull_request.merge", {
      pull_request: self,
      actor: actor,
    })

    GitHub.dogstats.increment("pull_request", tags: ["action:merged", "result:#{method}"])

    if !combined_status.any?
      GitHub.dogstats.increment("status", tags: ["action:merged", "result:none"])
    else
      GitHub.dogstats.increment("pull_request", tags: ["action:merged", "result:failure"]) if combined_status.state == "failure"
      GitHub.dogstats.increment("pull_request", tags: ["action:merged", "result:pending"]) if combined_status.state == "pending"
      GitHub.dogstats.increment("pull_request", tags: ["action:merged", "result:success"]) if combined_status.state == "success"
      if combined_status.count == 1
        GitHub.dogstats.increment("status", tags: ["action:merged", "result:single"])
      else
        GitHub.dogstats.histogram("status", combined_status.count, tags: ["action:merged", "result:more", "type:combined_count"])
      end
    end

    PullRequestReviewStatsJob.perform_later(id)
  end

  # raised in order to report to Failbot when a PR is merged and not
  # subsequently closed.
  class PRMergedAndNotClosed < StandardError
  end

  # Public: Return an object to check how a pull can be updated.
  #
  # Returns a Updateability instance.
  def updateability
    return @updateability if defined?(@updateability)
    @updateability = Updateability.new(self)
  end

  # Public: Attempt to merge latest base ref into head ref.
  #
  # user - A User performing the sync
  # expected_head_oid - String OID of expected head ref (default: head db value)
  #
  # Returns new Commit of head ref.
  # Raises PullRequest::PermissionError if user has insufficient permissions.
  # Raises PullRequest::RefMismatch if expected head oid doesn't match current
  #   ref value.
  # Raises PullRequest::MergeConflictError if there was a merge conflict
  #   between the base and head.
  def merge_base_into_head(user:, author_email: nil, expected_head_oid: self.head_sha, base_oid: nil, resolve_conflicts: nil)
    update = Update.new(pull: self, actor: user, author_email: author_email, expected_head_oid: expected_head_oid)
    update.merge(base_oid: base_oid, conflict_resolutions: resolve_conflicts)

    reload
  end

  # Create a new branch with this pull request reverted.
  #
  # user        - the User that is responsible for the revert
  # reflog_data - optional metadata to be written to the reflog
  #
  # Returns a tuple of [Ref, error], only one of which will be present.
  #
  # error can be :not_revertable if #revertable_by? is not fulfilled
  #
  # See CommitsCollection#create_revert_commit for additional possible errors.
  def revert(user, reflog_data = {}, timeout: nil)
    return [nil, :not_revertable] unless revertable_by?(user)

    revert_repository  = async_pushable_repo_for(user).sync
    revert_branch_name = "revert-#{number}-#{head_ref_name}"

    if existing = revert_repository.heads.find(revert_branch_name)
      return [existing, nil]
    end

    base_commit = base_repository.heads.find(base_ref).target

    if revert_repository != base_repository
      revert_repository.rpc.fetch_commits(base_repository.shard_path, base_commit.oid)
    end

    backfill_base_sha_on_merge if base_sha_on_merge.nil?

    # If `base_sha_on_merge` was set or could be backfilled,
    # and we're not reverting a merge commit, revert all commits
    # (only following the first parent) in the range between `base_sha_on_merge`
    # and `merge_commit_sha`.
    if base_sha_on_merge && merged_commit.nil?
      revert_commit, error = revert_repository.commits.create_revert_commits_for_range(
        user,
        base_commit.oid,
        base_sha_on_merge,
        merge_commit_sha,
        timeout: timeout,
      )
    else
      revert_commit, error = revert_repository.commits.create_revert_commit(
        user,
        base_commit.oid,
        merged_commit ? merged_commit.oid : squashed_commit.oid,
        commit_message: "Revert \"#{title}\"",
        mainline: merged_commit ? mainline_number_for_revert : 0,
      )
    end

    return [nil, error] if error

    revert_branch = revert_repository.heads.create(
      revert_branch_name,
      revert_commit,
      user,
      reflog_data: pr_reflog_data("revert").merge(reflog_data),
    )

    [revert_branch, nil]
  rescue Git::Ref::ExistsError
    [revert_repository.reload.heads.find(revert_branch_name), nil]
  end

  # Since  https://github.com/github/github/pull/135509 we can revert pull
  # requests merged with commits we didn't make, so we need to examine the
  # merge commit relative to the PR head to determine the correct mainline
  # number (i.e. the 1-based index of the parent that is *not* the PR's
  # head_sha.
  def mainline_number_for_revert
    fail(RevertError, "missing merged_commit (#{id})") unless merged_commit
    parent_oids = merged_commit.parent_oids
    fail(RevertError, "cannot handle octopus merges (#{id})") unless parent_oids.size == 2
    case head_sha
    when parent_oids.first then 2
    when parent_oids.last  then 1
    else
      fail RevertError, "invariant violated (#{id}): head #{head_sha} is not among merge's (#{merged_commit.oid}) parents"
    end
  end

  # Internal: Determine and store the value for `base_sha_on_merge`.
  #
  # The `base_sha_on_merge` column was added to Pull Requests in November 2016.
  # It is used to determine the range of commits to revert in the case of a
  # "rebase-merged" pull request. It is also used when indexing pull requests
  # to build backlinks from commits to pull requests.
  #
  # This method tries to determine the correct value for `base_sha_on_merge`
  # for pull requests that were merged before this column was added.
  def backfill_base_sha_on_merge
    value = determine_base_sha_on_merge
    return unless value

    write_attribute :base_sha_on_merge, value
    PullRequest.where(id: id).update_all base_sha_on_merge: value
  end

  # Internal: Determine and return the value for `base_sha_on_merge`.
  #
  # This determines the correct value for `base_sha_on_merge` for
  # all different merge methods we support.
  #
  # This will try to link the commit id stored in the merge event to the
  # earliest entry in the `pushes` table via the
  # `pushes.after` column. If we find such an entry, the `before` column
  # will contain the `base_sha_on_merge` value we're looking for.
  def determine_base_sha_on_merge
    merge_event = events.merges.last
    return unless merge_event && merge_event.actor_id && merge_event.commit_id

    binds = {
      issue_event_id: merge_event.id,
      base_repository_id: base_repository_id,
      base_ref: "refs/heads/#{base_ref}",
      actor_id: merge_event.actor_id,
      commit_id: merge_event.commit_id,
    }

    # We fetch the value for base_sha_on_merge
    # by finding the entry in the `pushes` table that correlates to the
    # merge recorded in the "merged" issue event.
    #
    # We assume the earliest push event that matches is the one that
    # corresponds to the merge.
    Push.github_sql.value <<-SQL, binds
      SELECT `pushes`.`before` as `base_sha_on_merge`
        FROM `pushes`
      WHERE `pushes`.`repository_id` = :base_repository_id
        AND `pushes`.`ref` = :base_ref
        AND `pushes`.`pusher_id` = :actor_id
        AND `pushes`.`after` = :commit_id
      ORDER BY `pushes`.`id` ASC LIMIT 1
    SQL
  end

  # Is the user able to revert this pull request?
  #
  # If yes, returns the Repository from which they can revert it.
  # If no, returns false.
  def revertable_by?(user)
    return false unless user

    GitHub.dogstats.time("pull_request", tags: ["action:revertable_by"]) do
      merged_event = events.merges.last
      return false unless merged_event

      merged_event.async_revertable_by?(user).sync
    end
  end

  # Find a repository that the user could potentially
  # submit a pull request from. Chooses a repo in this
  # order, provided they have push access:
  #
  # 1. The base repository
  # 2. The head repository
  # 3. A fork that the user owns
  # 4. A fork that the user has push access to
  def async_pushable_repo_for(user)
    @pushable_repo_by_user ||= {}
    return @pushable_repo_by_user[user] if @pushable_repo_by_user.has_key?(user)

    @pushable_repo_by_user[user] = async_pushable_repo_for!(user)
  end

  def async_pushable_repo_for!(user)
    return Promise.resolve(nil) if user.nil?

    async_base_repository_pushable_by?(user).then do |writable|
      next base_repository if writable

      async_head_repository_pushable_by?(user).then do |writable|
        next head_repository if writable
        next nil unless base_repository

        # TODO: This could use a dedicated AssociatedRepositories loader
        base_repository.find_pushable_forks_in_network_for_user(user).sort_by do |fork|
          user == fork.owner ? 0 : 1
        end.first
      end
    end
  end

  # The merge commit that was used to merge the PR
  # into the base branch. This may not be present
  # if the merge was a fast-forward, as it could
  # be if done on the command line.
  def merged_commit
    return nil unless merged?
    return @merged_commit if defined?(@merged_commit)

    @merged_commit = begin
      commit = events.merges.last.try(:commit)
      commit if commit.merged_from?(head_sha)
    end
  end

  # The commit that was created on the base branch that contains
  # all the changes of the PR squashed together.
  def squashed_commit
    return nil unless merged?
    return @squashed_commit if defined?(@squashed_commit)

    @squashed_commit = begin
      commit = events.merges.last.try(:commit)
      commit if commit && commit.parent_oids.length == 1 && commit.oid != head_sha
    end
  end

  #
  # Creation Helpers and Finders
  #

  # Creates a PullRequest with dependent Issue for the given repo.
  #
  # repo    - Repository that is receiving the Pull Request.
  # options - Hash with these keys:
  #           user  - The User that is creating the Pull Request.
  #           base  - String SHA marking the starting point on the root
  #                   repository.
  #                   ex: "user:branch-ref"
  #           head  - String SHA marking the latest commit.
  #                   ex: "user:branch-ref"
  #           issue - Optional Issue for the PullRequest.  If empty, a title
  #                   and body must be passed to create one.
  #           title - String title for the created Issue.
  #           body  - String body for the created Issue.
  #           draft  - Boolean indicating if this PullRequest is still a draft/work in progress
  #           reviewer_user_ids - Array of user.ids that are to be requested review
  #           reviewer_team_ids - Array of team.ids that are to be requested review
  #
  # Returns a PullRequest that is either saved or invalid.
  def self.create_for(repo, options)
    unless options[:base].present? && options[:head].present?
      pull_request = repo.pull_requests.build
      pull_request.errors.add(:base, "can't be blank") unless options[:base].present?
      pull_request.errors.add(:head, "can't be blank") unless options[:head].present?
      return pull_request
    end

    existing_issue = pull_request = issue = nil
    comparison = repo.comparison(options[:base], options[:head])

    pull_request = comparison.build_pull_request(
      user: options[:user],
      user_hidden: !!options[:user_hidden],
      draft: (options[:draft] && repo.plan_supports?(:draft_prs)) || false,
    )
    return pull_request if !pull_request.valid?

    issue = options[:issue] ||
      repo.issues.build(
        title: options[:title],
        body: options[:body],
        user: options[:user],
        user_hidden: !!options[:user_hidden],
      )

    issue.repository = repo
    issue.pull_request = pull_request

    if !issue.valid?
      raise ActiveRecord::RecordInvalid, issue
    end

    existing_issue = !issue.new_record?
    pull_request.issue = issue

    if options[:collab_privs] || (options[:maintainer_can_modify] && pull_request.cross_repo?)
      pull_request.fork_collab_state = :allowed
    end

    reviewers  = User.where(id: options[:reviewer_user_ids] || [])
    reviewers += Team.where(id: options[:reviewer_team_ids] || [])
    pull_request.request_review_from(reviewers: reviewers, actor: options[:user], should_save: false)

    pull_request.request_review_from_codeowners(options[:user], should_save: false) if pull_request.ready_for_review?

    if issue.created_by_dependabot? && options[:dependabot_update_id].present?
      dependency_update = repo.dependency_updates.find_by(id: options[:dependabot_update_id])

      if dependency_update.present? && !dependency_update.complete?
        dependency_update.state = "complete"
        pull_request.dependency_updates = [dependency_update]
      else
        pull_request.errors.add(:dependency_updates)
        raise ActiveRecord::RecordInvalid, pull_request
      end
    end

    revisions_enabled = repo.pull_request_revisions_enabled?

    GitHub::SchemaDomain.allowing_cross_domain_transactions do
      PullRequest.transaction do
        ProjectCard.transaction do
          pull_request.save!

          if revisions_enabled
            pull_request.revisions.create!(
              number: 1,
              base_oid: pull_request.base_sha,
              head_oid: pull_request.head_sha,
              revised_at: Time.now.utc,
              actor: options[:user],
              ready: !pull_request.draft?,
              force_pushed: false,
              commits_count: comparison.total_commits,
            )
          end
        end
      end
    end

    # We've observed a handful of issues being saved with `issues.pull_request_id`
    # set to 0, causing the associated pull request to become detached. We're not
    # sure how this is happening, so this a temporary guard to gather more information
    # and hopefully protect against more detached PRs from being created.
    if issue.pull_request_id&.zero?
      boom = Issue::PullRequestIdInvalid.new("pull_request_id is invalid")
      boom.set_backtrace(caller)
      Failbot.report(boom, repo_id: repository.id, pull_request_id: pull_request.id, issue_id: issue.id)

      issue.update_attribute(:pull_request_id, pull_request.id)
    end

    if pull_request.user.associated_repository_ids(repository_ids: [pull_request.base_repository_id, pull_request.head_repository_id]).none?
      GitHub.dogstats.increment("pull_request", tags: ["type:non_collaborator_pr"])

      boom = NonCollaboratorPR.new
      Failbot.report_trace(boom, pull_request_id: pull_request.id)
    end

    GlobalInstrumenter.instrument("pull_request.create", { pull_request: pull_request })

    pull_request
  rescue DetermineCodeownersError
    # It's very unlikely this will happen, but there was an error
    # checking the new pull's diff to determine codeowner rules to apply.
    pull_request.errors.add(:base, :determine_codeowners, message: "Could not determine codeowners for the diff")
    pull_request
  ensure
    # this needs to be outside the transaction
    if pull_request && !pull_request.new_record? && existing_issue
      issue.instrument_transform_to_pull
      RemoveFromSearchIndexJob.perform_later("issue", issue.id, issue.repository_id)
    end
  end

  class NonCollaboratorPR < StandardError
  end

  # Like PullRequest.create_for but raises an ActiveRecord::RecordInvalid
  # exception when validation fails.
  def self.create_for!(repo, options)
    res = GitHub::SchemaDomain.allowing_cross_domain_transactions { create_for(repo, options) }
    raise ActiveRecord::RecordInvalid, res if res.new_record?
    res
  end

  # Public: Find pull requests for the given branch name
  #
  # head - A String or Array of Strings with ref names.
  #
  # Returns an ActiveRecord scope.
  def self.for_branch(head)
    where(head_ref: Git::Ref.permutations(head)).order("id")
  end

  # Is there an existing pull request for this repository, with this ref name?
  # It should be smart and show only:
  #
  # - Open pull requests
  # - default -> default only for cross-repo from fork
  def self.existing_for(repo, ref_name)
    return nil if ref_name == repo.default_branch
    repo.pull_requests.open_pulls.where(base_repository_id: repo.id, head_ref: ref_name).first
  end

  # Finds an existing pull request with the same head and base refs as this pull
  # request. Useful in validations and deciding whether a PR is reopenable, to check for duplicates.
  def find_existing
    repository.pull_requests.
      includes(:issue).joins(:issue).
      where("`issues`.`repository_id` = `pull_requests`.`repository_id`").
      where(
        base_ref: base_ref,
        head_ref: head_ref,
        base_repository_id: base_repository_id,
        head_repository_id: head_repository_id,
        issues: { state: "open" },
      ).first
  end

  # Public: Checks whether this is PR is across repos
  # (head and base repositories are not the same)
  #
  # Returns Boolean
  def cross_repo?
    head_repository_id != base_repository_id
  end

  # Public: Checks whether this is PR is within a single repo
  # (head and base repositories are the same)
  #
  # Returns Boolean
  def same_repo?
    !cross_repo?
  end

  # Public: An cross-network pull request is cross-repository and
  #    the head and base repositories belong to different networks.
  #
  # Returns Boolean
  def cross_network?
    async_cross_network?.sync
  end

  def async_cross_network?
    return @async_cross_network if defined?(@async_cross_network)
    return @async_cross_network = Promise.resolve(false) unless cross_repo?

    @async_cross_network = Promise.all([
      async_head_repository,
      async_base_repository,
    ]).then do |head_repository, base_repository|
      next false unless head_repository
      head_repository.network_id != base_repository.network_id
    end
  end

  # Public: Return commit ids that are linked to this pull request.
  #
  # limit - maximum number of commit ids to return.
  #
  # This includes:
  #   * commits between the `base_sha` and `head_sha`
  #   * merge commit for "normal" and squash merges
  #   * rebased commits for rebase merges
  def linked_commit_ids(limit: COMMIT_LIMIT)
    include_oids = [head_sha]
    exclude_oids = [base_sha]

    if merged? && base_sha_on_merge
      include_oids << merge_commit_sha
      exclude_oids << base_sha_on_merge
    end

    commit_ids = repository.rpc.rev_list(include_oids, exclude_oids: exclude_oids, limit: limit)

    # include the merge commit sha for pull requests that got merged before
    # `base_sha_on_merge` was introduced.
    #
    # TODO: Remove once `base_sha_on_merge` was backfilled.
    if merged? && !base_sha_on_merge
      commit_ids << merge_commit_sha
    end

    commit_ids.freeze
  end

  #
  # Active Comparison
  #

  # Determine whether the head repository exists. Falsey when the head
  # repository is deleted.
  def head_repository?
    !head_repository.nil?
  end

  # Determine whether the base and head refs still exist.
  def refs_exist?
    head_repository? && comparison.base_sha && comparison.head_sha
  end

  # Determine if commits exist in the head branch that are not in the base branch.
  def commits_pending?
    refs_exist? && comparison.ahead?
  end

  # Public: Check if head branch is behind base branch.
  #
  # PR may need to be sync'd with the base if not.
  #
  # Returns true/false.
  def behind_base?
    refs_exist? && comparison.behind?
  end

  # WARNING: Are you sure you don't want historical_comparison?
  # This is the live / active Comparison object used to determine the current state of
  # the pull request branch. THIS OBJECT IS ONLY VALID WHILE THE BASE AND THE HEAD EXIST.
  # Once either the base or -- more likely -- the head is deleted, the comparison ceases to be valid.
  # THIS WILL NOT BE VALID FOR MERGED PRS WITH DELETED BRANCHES. Use historical_comparison instead.
  def comparison
    @comparison ||= repository.comparison(base, head, COMMIT_LIMIT, self)
  end

  #
  # Historical Comparison
  #

  # The historical comparison object provides a view of changes between the base
  # and head even after the head has been merged or deleted. This is necessary
  # because the active comparison ceases to provide useful information once the
  # head is merged or deleted.
  #
  # This is accomplished by storing and updating the SHA1s for the base and head
  # commits while the comparison is active and then using them once the pull
  # request is closed, or the branch is merged / deleted.
  def historical_comparison
    async_historical_comparison.sync
  end

  def async_historical_comparison
    async_build_comparison(head_commit_oid: head_sha)
  end

  # Public: builds a PullRequest::Comparison for this pull request. By default
  #   the full range of the pull request is used.
  #
  # start_oid - the oid of the earliest commit for the comparison,
  #             defaulting to the merge base of the PR.
  # end_oid   - the oid of the latest commit for the comparison. Defaults to
  #             the head_sha of the pull request.
  # base_oid  - the oid of the merge base relevant to the range being viewed.
  #             defaults to the merge base of the PR.
  def async_pull_comparison(start_oid: merge_base, end_oid: head_sha, base_oid: merge_base)
    PullRequest::Comparison.async_find(
      pull: self,
      start_commit_oid: start_oid,
      end_commit_oid: end_oid,
      base_commit_oid: base_oid,
    )
  end

  def pull_comparison(start_oid: merge_base, end_oid: head_sha, base_oid: merge_base)
    async_pull_comparison(start_oid: start_oid, end_oid: end_oid, base_oid: base_oid).sync
  end

  # Public: Use this method if you don't need the extra validation and methods of
  #   a full pull request comparison.
  def async_diff(start_commit_oid: merge_base, end_commit_oid: head_sha, base_commit_oid: merge_base)
    async_compare_repository.then do |repo|
      GitHub::Diff.new(repo, start_commit_oid, end_commit_oid, base_sha: base_commit_oid)
    end
  end

  def compare_repository
    async_compare_repository.sync
  end

  def async_compare_repository
    return @async_compare_repository if defined?(@async_compare_repository)

    async_repository_with_network = async_repository.then do |repository|
      repository.async_network.then { repository }
    end

    @async_compare_repository = Promise.all([
      async_repository_with_network,
      async_cross_network?,
    ]).then do |repository, is_cross_network|
      if is_cross_network
        repository
      else
        Promise.all([
          async_head_repository,
          async_base_repository,
        ]).then do |head_repository, base_repository|
          Platform::Loaders::CompareRepository.load(
            repository,
            head_repository: head_repository,
            base_repository: base_repository,
          )
        end
      end
    end
  end

  def merge_base
    async_merge_base.sync
  end

  def async_merge_base
    async_historical_comparison.then(&:async_merge_base)
  end

  def build_comparison(base_commit_oid: base_sha, head_commit_oid:)
    async_build_comparison(base_commit_oid: base_commit_oid, head_commit_oid: head_commit_oid).sync
  end

  # A Comparison spanning the PR's base commit and given head commit.
  #
  # Optionally provide an alternative base commit. This is useful for inspecting
  # the state of the rollup diff at a previous point in the pull request's history.
  #
  # Returns Promise<GitHub::Comparison>
  def async_build_comparison(base_commit_oid: base_sha, head_commit_oid:)
    return @async_build_comparison[[base_commit_oid, head_commit_oid]] if defined?(@async_build_comparison)

    @async_build_comparison = Hash.new do |hash, (base_commit_oid, head_commit_oid)|
      async_repositories_with_networks = Promise.all([
        async_base_repository,
        async_head_repository,
        async_repository,
      ]).then do |repositories|
        Promise.all(repositories.compact.map(&:async_network))
      end

      async_advisory_parent_repository = async_in_advisory_workspace?.then do |is_in_advisory|
        next unless is_in_advisory
        async_repository.then(&:async_parent_advisory_repository)
      end

      async_comparison = Promise.all([
        async_base_repository,
        async_head_repository,
        async_repository,
        async_advisory_parent_repository,

        # TODO: The comparison accesses `pull.head_user` and `pull.base_user`
        #   Ideally this is passed to the `Comparison` or loaded when needed.
        async_base_user,
        async_head_user,
      ]).then do |base_repository, head_repository, repository|
        # If the users that created the PR are gone, we don't have anything
        # to compare with, so return an empty comparison.
        next repository.comparison(nil, nil) unless base_repository && head_commit_oid && base_commit_oid

        repositories = [base_repository, head_repository, repository].compact
        Promise.all(repositories.map(&:async_network)).then do
          GitHub::Comparison.build(
            base_repo: base_repository,
            head_repo: head_repository,
            base_revision: base_commit_oid,
            head_revision: head_commit_oid,
            limit: COMMIT_LIMIT,
            pull: self,
          )
        end
      end

      hash[[base_commit_oid, head_commit_oid]] = async_comparison
    end

    @async_build_comparison[[base_commit_oid, head_commit_oid]]
  end

  # Public: The historical diff for this pull request, with single entry limits maximized
  # for comment positioning.
  def historical_comments_diff
    return @historical_comments_diff if defined?(@historical_comments_diff)

    @historical_comments_diff = historical_comparison.diffs.only_params.tap do |diff|
      diff.maximize_single_entry_limits!
    end
  end

  # List of commits between the last known good head and base SHA1s. This does
  # not rely on the head repository or the head branch existing.
  #
  # Returns an Array of Commit objects.
  def changed_commits
    async_changed_commits.sync
  end

  def async_changed_commits
    return @async_changed_commits if defined?(@async_changed_commits)

    @async_changed_commits =
      Promise.all([async_historical_comparison, async_base_repository]).then { |historical_comparison, base_repo|
        historical_comparison.async_commits.then do |commits|
          commits = commits.compact.tap do |commits|
            fetching_changed_commits(commits)
          end

          if base_repo.use_rev_list_ordering?
            StableSorter.new(commits).sort_by(&:timeline_sort_by)
          else
            commits.sort_by(&:timeline_sort_by)
          end
        end
      }.catch { |error|
        case error
        when GitRPC::ObjectMissing, Repository::CommandFailed, GitHub::DGit::UnroutedError, GitRPC::CommandBusy
          @corrupt = true
          []
        else
          raise error
        end
      }
  end

  def fetching_changed_commits(commits)
    Commit.prefill_users(commits)

    # preset these to avoid additional lookups
    commits.each do |commit|
      if head_repository&.deleted?
        commit.repository = base_repository || repository
      else
        commit.repository = head_repository || base_repository || repository
      end
    end

    short_keys = commits.map { |c| c.message_cache_key("short_message_html") }
    GitHub.cache.get_multi(short_keys) if short_keys.any?
  end

  def async_load_pull_request_commit(commit_oid)
    Promise.all([
      async_repository,
      async_compare_repository,
    ]).then do |repository, compare_repository|
      async_load_commit = Platform::Loaders::GitObject.load(compare_repository, commit_oid).then do |commit|
        commit.repository = repository if commit

        commit
      end

      async_load_commit.then do |commit|
        next if commit.nil?
        Platform::Models::PullRequestCommit.new(self, commit)
      end
    end
  end

  # List of commits oids between the last known good head and base SHA1s.
  #
  # Returns an Array of Strings.
  def changed_commit_oids
    historical_comparison.rev_list
  end

  # Pull requests whose last known head or base SHA1s no longer resolve to valid
  # objects are considered corrupt. The first month or so of pull requests did
  # not have special "heads/pull/<id>/{head,base}" branches created for them and
  # so some of them commits were gc'd after the head branch was deleted.
  #
  # Use this method to check if the pull request is corrupt before accessing
  # commit information. Use the diff_available? method to check for corruptions
  # before accessing diff information.
  def corrupt?
    async_corrupt?.sync
  end

  def async_corrupt?
    return Promise.resolve(@corrupt) if defined?(@corrupt)

    async_changed_commits.then { !!@corrupt }
  end

  # Public: Total number of commits in the revision range, ignoring any limit
  # imposed on changed_commits.
  #
  # Returns an Integer.
  def total_commits
    async_total_commits.sync
  end

  def async_total_commits
    async_corrupt?.then do |corrupt|
      next 0 if corrupt

      async_historical_comparison.then(&:async_total_commits)
    end
  end

  # Determine if the commit limit was exceeded (total_commits > changed_commits.size).
  def commit_limit_exceeded?
    return false if corrupt?
    historical_comparison.commit_limit_exceeded?
  end

  # The GitHub::Diff object for the set of changes between the base and head
  # commits. No actual processing occurs when accessing this method. You must
  # access an attribute on the diff object to fire a real git operation.
  #
  # Use #set_diff_options to establish diff formatting options before accessing
  # this attribute.
  #
  # Returns a GitHub::Diff object.
  def diffs
    historical_comparison.diffs
  end

  # Set GitHub::Diff options and reset the memoized diffs object.
  #
  # options - :max_diff_size, :max_total_size, :max_files, :ignore_whitespace.
  #           See GitHub::Diff attribute docs for info on possible values.
  #
  # Returns nothing.
  def set_diff_options(options = {})
    historical_comparison.set_diff_options(options)
  end

  # Pull requests whose last known head or base SHA1s no longer resolve to valid
  # objects are considered corrupt. The first month or so of pull requests did
  # not have special "heads/pull/<id>/{head,base}" branches created for them and
  # so some of them commits were gc'd after the head branch was deleted.
  #
  # Use this method to check if the pull request is corrupt before accessing
  # diff information.
  def diff_available?
    diffs.available?
  end

  def fork_collab_available_for_user?(current_user)
    current_user == user &&
    base_repository != head_repository &&
    head_repository &&
    head_repository.pushable_by?(current_user) &&
    !repository.advisory_workspace? &&
    !(head_repository.fork? && head_repository.in_organization?)
  end

  # The total number of changed files included in the diff.
  def changed_files
    historical_comparison.diffs.changed_files
  end

  # Is the diff large enough to be something that requires dealing with?
  def large_diff?
    return false if !diff_available?
    historical_comparison.diffs.changes > 1000
  end

  # Public: Number of lines added across all files.
  #
  # Returns an Integer.
  def additions
    historical_comparison.diffs.additions
  end

  # Public: Number of lines removed across all files.
  #
  # Returns an Integer.
  def deletions
    historical_comparison.diffs.deletions
  end

  def async_changed_files
    async_historical_comparison.then do |comparison|
      comparison.async_diff(summary: true).then do |diff|
        diff.changed_files
      end
    end
  end

  def async_additions
    async_historical_comparison.then do |comparison|
      comparison.async_diff(summary: true).then do |diff|
        diff.additions
      end
    end
  end

  def async_deletions
    async_historical_comparison.then do |comparison|
      comparison.async_diff(summary: true).then do |diff|
        diff.deletions
      end
    end
  end

  # Update the base_sha and head_sha attributes to the best current values on
  # the active base and head branches, while accounting for merges from head
  # into base or from base into head. These values are used to build the changes
  # object.
  #
  # Returns true if either the base_sha or head_sha were changed, falsey if no
  # changes were made.
  def record_concrete_commit_points(forced = false)
    if !merged? && open? && refs_exist?
      previous_values = [base_sha, head_sha]
      write_attribute :base_sha, nil if forced
      write_attribute :base_sha, concrete_base_sha
      write_attribute :head_sha, comparison.head_sha
      if [base_sha, head_sha] != previous_values
        remove_instance_variable(:@async_build_comparison) if defined?(@async_build_comparison)
        true
      end
    end
  end

  # Maintains a special tracking ref in the repository for the head revision. This
  # ensures that the objects referenced by the pull request are not gc'd when
  # branches are deleted and old commits fall out of the reflog.
  #
  # actor - The User who triggered creation of the tracking ref.
  #         For use in the reflog.
  #
  # Returns true if the tracking ref was created or updated, nil otherwise.
  def maintain_tracking_ref(actor, priority: :high)
    # If this pull's issue or repository was destroyed, there is no need
    # to maintain the tracking ref anymore. This can happen if the
    # `maintain-tracking-ref` job is racing with repository deletion.
    return if issue.nil? && reload_issue.nil?
    return if repository.nil? && reload_repository.nil?

    # do a quick check to see if maybe the ref is already in place and current
    current_head = repository.refs.read("refs/pull/#{number}/head")
    return if current_head.target_oid == head_sha

    # Fetch any commits from the head repository that are not already
    # present in the base repository. This is necessary even in
    # repository networks because someone might try to submit a pull
    # request from a fork before the objects involved have been synced
    # to the network repository.
    if head_repository && head_repository != repository
      GitHub.dogstats.time("pull_request", tags: ["action:fetch_commits"]) do
        repository.rpc.fetch_commits(head_repository.shard_path, head_sha)
      end
    end

    # create the tracking ref
    current_head.update(head_sha,
                        actor || safe_user,
                        priority: priority,
                        post_receive: false,
                        clear_ref_cache: true,
                        no_custom_hooks: false)

    # Clean up possible unshipped PR tracking ref
    if history_chain_ref = repository.refs.find("refs/__gh__/pull/#{number}/heads")
      history_chain_ref.delete(actor || safe_user)
    end

    true
  end

  # Same as #maintain_tracking_ref, but handles race conditions
  # around multiple processes trying to update the tracking ref.
  def maintain_tracking_ref_with_retries(actor, max_retries: 10, priority: :high)
    attempts = 0

    1.upto(max_retries) do
      begin
        attempts += 1

        if maintain_tracking_ref(actor, priority: priority)
          GitHub.dogstats.increment("pull_request", tags: ["action:maintain_tracking_ref", "result:success"])
        else
          GitHub.dogstats.increment("pull_request", tags: ["action:maintain_tracking_ref", "result:noop"])
        end

        break
      rescue Git::Ref::ComparisonMismatch => e
        if attempts == max_retries
          GitHub.dogstats.increment("pull_request", tags: ["action:maintain_tracking_ref", "result:failure"])
          raise e
        else
          reload
        end
      end
    end

    GitHub.dogstats.histogram("pull_request", attempts, tags: ["action:maintain_tracking_ref_attempts"])
  end

  def record_tracking_ref_maintenance_required
    @tracking_ref_maintenance_required = true if saved_change_to_head_sha?
  end

  def maintain_tracking_ref_later
    return if Rails.env.test? && disable_disk_access?
    return unless @tracking_ref_maintenance_required

    MaintainTrackingRefJob.perform_later(id, user.try(:id))
    remove_instance_variable :@tracking_ref_maintenance_required
  end

  # Queues deletion of internal merge and rebase ref.
  private def async_destroy_merge_refs
    DestroyMergeRefsJob.perform_later(self.id)
  end

  def destroy_merge_refs
    repository.batch_write_refs(safe_user, [
      [merge_ref, nil, GitHub::NULL_OID],
      [rebase_ref, nil, GitHub::NULL_OID]
    ], clear_ref_cache: false, no_custom_hooks: true)
  end

  def destroy_conflict_metadata
    conflict&.destroy
  end

  # Purge our tracking refs from disk.
  #
  # This code is intended for use by support when a user wants to remove a pull
  # request that has sensitive info.
  #
  # Returns nothing.
  def destroy_tracking_refs
    repository.batch_write_refs(safe_user, [
      ["refs/pull/#{number}/head", nil, GitHub::NULL_OID],
      ["refs/__gh__/pull/#{number}/heads", nil, GitHub::NULL_OID],
      [merge_ref, nil, GitHub::NULL_OID],
      [rebase_ref, nil, GitHub::NULL_OID]
    ], clear_ref_cache: false, no_custom_hooks: true)
  end

  # Detect if the head line has been merged into the base line
  def is_head_merged_into_base?
    merge_comparison.valid? && merge_comparison.merged?
  end

  def head_is_commit?
    return unless head_repository
    head_repository.commits.exist?(head_ref.b)
  rescue RepositoryObjectsCollection::InvalidObjectId
    false
  end

  # Detect if the saved head_sha can be reached from the new branch tip
  def old_head_connected_to_new?
    return true if head_is_commit?
    return unless head_ref_exist?

    old_sha = head_sha
    new_sha = head_repository.heads.find(head_ref.b).target_oid

    return true if new_sha == old_sha

    head_repository.rpc.descendant_of?(new_sha, old_sha)
  end

  # Mark the pull request as merged and close the issue.
  # Also close any issues fixed by this PR.
  #
  # actor           - The user who pushed to the base ref or performed some other
  #                   operation that changed the base lineage. This user is attributed
  #                   with the merge.
  # merge_sha       - The merge commit sha to use for IssueEvent#commit_id.
  # base_sha_on_merge - (Optional) The latest commit on the `base_ref` before
  #                   the merge was performed.
  def mark_as_merged(actor = nil, merge_sha = nil, base_sha_on_merge = nil)
    write_attribute :merged_at, Time.now

    sha = merge_sha || merge_comparison.head_sha
    write_attribute(:merge_commit_sha, sha)
    write_attribute(:base_sha_on_merge, base_sha_on_merge)
    write_attribute(:mergeable, nil)

    if actor
      create_issue_event(:merged, actor,
        commit_id: sha)
      close_issues_on_merge(actor)
    end

    close(actor)

    # once the pull request is merged, clean up the review markers to save space
    last_seen_pull_request_revisions.destroy_all
    # also cleanup the conflict metdata if there is any
    destroy_conflict_metadata
  end

  # Public: Create a new IssueEvent for this PullRequest.
  #
  # event      - The String event name.  See IssueEvent::VALID_EVENTS.
  # actor      - The User performing the event.
  # attributes - Optional Hash of more IssueEvent properties to set.
  #
  # Returns the created IssueEvent.
  def create_issue_event(event, actor, attributes = {})
    issue.events.create!(attributes.merge(event: event.to_s, actor: actor))
  end

  # Internal: Comparison object used for merge detection.
  #
  # This handles the cases where the head ref (or repository) are gone, but
  # the commit still exists or has been merged into the base.
  #
  # Returns a GitHub::Comparison object
  def merge_comparison
    return @merge_comparison if @merge_comparison

    comparison_head = head_sha

    # If the PR was closed because it contained 0 commits, check to see if
    # the head ref still exists. If so, use that for the comparison.
    # This lets the PR be reopenable if the ref exists and has had new commits added to it.
    if closed_with_zero_commits?
      head_reference  = head_repository && head_repository.heads.find(head_ref.b)
      comparison_head = head_reference  && head_reference.target_oid
    end

    @merge_comparison = if current_base_sha && comparison_head
      GitHub::Comparison.build(
        base_repo: base_repository,
        head_repo: base_repository,
        base_revision: current_base_sha,
        head_revision: comparison_head,
        limit: COMMIT_LIMIT,
      )
    else
      historical_comparison
    end
  end

  def closed_with_zero_commits?
    return false if corrupt?
    closed? && !merged? && historical_comparison.zero_commits?
  end

  # Public: overrides IssueTimeline#visible_events
  def visible_events(all_valid_events: false, viewer: nil)
    commits = Set.new(changed_commits.map(&:oid))
    events = super
    events.reject.with_index do |e, i|
      # don't show references to this PR from commits in the PR
      (e.commit? && e.reference? && commits.include?(e.commit_id)) ||
      # unless all valid events were requested (eg. timeline API),
      # don't show the close event that occurs along with the merge event
      # it seems that the order is always merge-then-close
      (!all_valid_events && e.close? && (last_event = events[i-1]).is_a?(IssueEvent) &&
        last_event.merge?)
    end
  end

  # Public: Returns an Array of timeline items
  # See also included behavior from IssueTimeline module
  #
  # viewer - the User that is viewing the timeline (current_user)
  #
  # Note: if you add new types into the items being returned here, please notify
  # @github/ecosystem-api in your PR so that we can help you implement the changes in
  # the GraphQL schema.
  def timeline_items_for(viewer, all_valid_events: false)
    threads = review_comment_threads_for(viewer)

    loader = PullRequestReview::Loader.new(reviews.for_viewer(viewer), threads)
    reviews = loader.execute
      .select { |review| review.show_in_timeline? }
      .reject { |review| review.hide_from_user?(viewer) }

    pull_request_commits = changed_commits.map do |commit|
      commit.repository = repository
      Platform::Models::PullRequestCommit.new(self, commit)
    end

    issue.comments.reject { |c| c.hide_from_user?(viewer) } +
      visible_events_for(viewer, all_valid_events: all_valid_events) +
      issue_references_for(viewer) +
      threads.select(&:legacy_thread?) +
      commit_comment_threads_for(viewer) +
      pull_request_commits +
      reviews +
      issue.composable_comments
  end

  # Public: Returns an Array of timeline items with any extra
  #         markers inserted.
  # See also included behavior from IssueTimeline module
  #
  # viewer - the User that is viewing the timeline (current_user)
  # items - list of timeline items before the markers are inserted
  def timeline_insert_markers(viewer, items)
    return items if viewer.nil?

    latest_marker = PullRequestRevisionMarker.latest_for(self, viewer)

    new_items = []
    marker_for_next_commit = nil

    items.each_with_index do |item, index|
      if item.class == Platform::Models::PullRequestCommit
        if marker_for_next_commit
          # keep the timestamp the same as the commit, so live updates
          # don't filter it out.
          marker_for_next_commit.created_at = item.commit.created_at

          # only show the last marker if the following commits
          # aren't entirely authored by you
          unless commits_are_yours(viewer, items[index..-1])
            new_items << marker_for_next_commit
          end
        end
        marker_for_next_commit = item.commit.oid == latest_marker&.last_seen_commit_oid ? latest_marker : nil
      end
      new_items << item
    end

    new_items
  end

  # Return if all the commits in the timeline are yours
  def commits_are_yours(viewer, commit_timeline_entries)
    commit_timeline_entries.each do |commit_timeline_entry|
      next if commit_timeline_entry.class != Platform::Models::PullRequestCommit
      return false if commit_timeline_entry.commit.author != viewer
    end

    true
  end

  # Public: Retrieve the timeline for the given viewer
  #
  # TODO: Rename to `timeline_for` once the IssueTimeline module is removed.
  #
  # viewer         - A User that is viewing the timeline
  # filter_options - Hash of options passed to the timeline instance
  #
  # Returns Timeline::PullRequestTimeline
  def timeline_model_for(viewer, filter_options = {})
    return @timeline_model_for[[viewer, filter_options]] if defined?(@timeline_model_for)

    @timeline_model_for = Hash.new do |hash, (viewer, filter_options)|
      hash[[viewer, filter_options]] = Timeline::PullRequestTimeline.new(
        self, viewer: viewer, filter_options: filter_options
      )
    end
    @timeline_model_for[[viewer, filter_options]]
  end

  # Total number of distinct author and committer email addresses on all
  # commits this pull request spans.
  #
  # Returns the integer number of distinct authors.
  def author_and_committer_count
    @author_and_committer_count ||=
      changed_commits.inject Set.new do |set, commit|
        set << commit.author_email
        set << commit.committer_email
      end.size
  end

  # Internal: For suggestions_url helper
  def suggestion_id
    id
  end

  # Public: Find all Users that have commented or committed to this pull request.
  # Exclude Bots and users who no longer have access to this repo
  # (unless parent org has so many members that query may timeout).
  #
  # Note that the returned users are not filtered for spam.
  #
  # optimize_repo_access_checks - by default, #participants will check to make
  #                               sure that each user returned still has access
  #                               to the repo. If optimize_repo_access_checks is
  #                               set to true, that check will not happen if the
  #                               parent org has too many members (which may
  #                               cause the access checks to time out)
  #
  # Returns an Array of Users.
  def participants(viewer: nil, optimize_repo_access_checks: false)
    @participants ||= begin
      review_user_ids = PullRequestReviewComment.github_sql.values(<<-SQL, pull_request_id: self.id, state: PullRequestReviewComment.states[:pending])
        SELECT DISTINCT(user_id)
          FROM pull_request_review_comments
         WHERE pull_request_id = :pull_request_id
         AND state != :state
      SQL
      review_user_ids += reviews.submitted.distinct.pluck(:user_id)
      users = [
        user,
        commenters,
        User.where(id: review_user_ids),
        (corrupt? ? [] : historical_comparison.comments.map(&:user)),

        # This is delegated to Issue
        get_event_participants,
        changed_commits.map(&:author).select { |user| base_repository.pullable_by?(user) },
      ].flatten.compact.uniq
      user_ids_to_hide = repository.user_ids_to_hide_from_mentions(users, viewer: viewer, optimize_repo_access_checks: optimize_repo_access_checks)
      users.reject! { |user| user.is_a?(Bot) || user_ids_to_hide.include?(user.id) }
      GitHub::PrefillAssociations.for_profiles(users)
      users
    end
  end

  def commenters
    async_commenters.sync
  end

  def async_commenters
    async_issue.then do |issue|
      issue.async_commenters.then do |commenters|
        commenters
      end
    end
  end

  # Users who should be considered participants in the pull request.
  #
  # viewer - the User who is viewing the participants (current_user)
  # optimize_repo_access_checks - if optimize_repo_access_checks is set
  #                               to true, do not perform access checks
  #                               on repos that are owned by org's with
  #                               a lot of members
  #
  # Returns an Array of Users.
  def participants_for(viewer, optimize_repo_access_checks: false)
    participants(viewer: viewer, optimize_repo_access_checks: optimize_repo_access_checks).reject { |u| u.hide_from_user?(viewer) }
  end

  # Total number of issue + review comments on this pull request. Commit
  # comments are not included for performance reasons.
  def total_comments
    @total_comments ||= GitHub.dogstats.time("pull_request", tags: ["action:total_comments"]) do
      (review_body_count + issue_comments_count + review_comments_count)
    end
  end
  attr_writer :total_comments

  def review_body_count
    reviews.where("state <> ? and body > ''", PullRequestReview.state_value(:pending)).count
  end

  def review_comments_count
    review_comments.where("state = ? and body is not null", PullRequestReviewComment.states[:submitted]).count
  end

  #
  # Review Comments
  #

  # Public: All review comments for this pull request visible to the viewer
  #
  # Returns an array of PullRequestReviewComment sorted by created_at, ascending.
  def review_comments_for(viewer)
    review_comments.filter_spam_for(viewer)
                   .in_viewable_state_for(viewer)
                   .includes(:user)
                   .order(:pull_request_id, :created_at, :id)
  end

  # All review comments for this pull request grouped by "thread". A thread
  # consists of one or more comments on the same path and line number.
  #
  # viewer - the User who is viewing the review comments (current_user)
  #
  # Returns an array of DeprecatedPullRequestReviewThread sorted by comment creation date.
  # Older threads come first.
  def review_comment_threads_for(viewer)
    async_deprecated_review_threads_for(viewer).sync
  end

  # All review comments for this pull request grouped by "thread". A thread
  # consists of one or more comments on the same path and line number.
  #
  # viewer - the User who is viewing the review comments (current_user)
  #
  # Returns an Promise<Array<DeprecatedPullRequestReviewThread>> sorted by comment creation date.
  # Older threads come first.
  def async_deprecated_review_threads_for(viewer)
    async_review_threads.then do |review_threads|
      Promise.all(review_threads.map(&:async_to_deprecated_thread)).then do |deprecated_review_threads|
        deprecated_review_threads.compact!
        deprecated_review_threads.select! { |thread| thread.visible_to?(viewer) }
        deprecated_review_threads
      end
    end
  end

  # Returns the issue references visible to the specified viewer for this pull's issue.
  def issue_references_for(viewer)
    issue.visible_references_for(viewer)
  end

  # All commit comments for this pull request grouped by "thread". A thread
  # consists of multiple comments on the same path and line number.
  #
  # viewer - the User who is viewing the commit comments (current_user)
  #
  # Returns an array of CommitCommentThread sorted by comment creation date.
  # Older threads come first.
  def commit_comment_threads_for(viewer)
    return [] if corrupt?
    comments = historical_comparison.comments_for(viewer)

    # filter out commit comments the viewer can't see
    comments = comments.select { |comment| comment.readable_by?(viewer) }

    # all issues in an archived repository are locked but, unless they were locked
    # prior to the archive, have no locked_at.
    if locked? && locked_at
      comments = comments.select do |comment|
        if locked_at > comment.created_at # fine if happened before the lock
          comment
        else
          repository.writable_by?(comment.user)
        end
      end
    end

    CommitCommentThread.group_comments(comments)
  end

  def change_base_branch(user, new_base_ref, automatic: false)
    raise ArgumentError, "Ref must be a string" unless new_base_ref.is_a?(String)
    ref = base_repository.refs.read(new_base_ref)
    raise PullRequest::BaseRefNotFoundError.new(self, new_base_ref) unless ref.exist?
    raise PullRequest::ClosedError.new(self, new_base_ref) unless open?

    synchronize!(user: user,
                  repo: base_repository,
                  ref: new_base_ref,
                  before: nil,
                  after: ref.target_oid,
                  changing_base: true,
                  changing_base_automatically: automatic)
  rescue DetermineCodeownersError
    raise BaseNotChangeableError.new(self, new_base_ref)
  end

  #
  # Updating on push
  #

  # Force the pull request to be synchronized with the underlying git
  # repositories's current ref state. This can be used to manually update the
  # commits logged on a pull request after a post-receive job fails.
  #
  # user - the user that should be attributed with the push or merge
  # repo - the Repository that received the push
  # forced - whether the ref was updated in a non-fast-forward manner; this
  #          should only be set when you know the push was forced; defaults
  #          to false.
  # before - The SHA before the ref was updated
  # after - The SHA after the ref was updated
  #
  # Returns true when the pull request was modified, falsey otherwise.
  def synchronize!(user:, repo:, forced: false, ref: nil, before: nil, after: nil, changing_base: false, changing_base_automatically: false)
    GitHub.dogstats.time("pull_request", tags: ["action:synchronize"]) do
      # Track before approval state if a ref update is happening and
      # code owner review is required. Used to move project cards
      # when code owner required reviews are added/removed.
      if ref && protected_base_branch&.require_code_owner_review? && issue&.project_workflow_review_triggers?
        approved_before = approved_with_no_changes_requested?
      end

      update_mergeable_attribute(nil)

      # These things can happen outside of the transaction because
      # they merely set attributes on the model or do git stuff,
      # and don't write to the database.

      saved_base_sha = base_sha
      saved_head_sha = head_sha

      old_base_ref = self.base_ref
      old_display_base_ref_name = self.display_base_ref_name
      if changing_base
        if ref != self.base_ref
          self.base_ref = ref

          @comparison = nil
          remove_instance_variable(:@async_build_comparison) if defined?(@async_build_comparison)
          remove_instance_variable(:@async_changed_commits) if defined?(@async_changed_commits)
          remove_instance_variable(:@protected_base_branch) if defined?(@protected_base_branch)
        else
          changing_base = false
        end
      end

      commit_points_changed = GitHub.dogstats.time("pull_request", tags: ["action:record_concrete_commit_points", "sync:true"]) do
        record_concrete_commit_points(forced || changing_base)
      end

      if commit_points_changed
        recalculate_all_positions unless GitHub.flipper[:skip_comment_repositioning].enabled?(repository)
      end

      pushed_to_base = repo == base_repository

      # if a ref was specified, meaning that this call to #synchronize! came from a push,
      # then verify that the push was to the base ref.
      if pushed_to_base && ref
        pushed_to_base = Git::Ref.permutations(base_ref).include?(ref)
      end

      pushed_to_head = Git::Ref.permutations(head_ref).include?(ref)

      merged = if user && pushed_to_base && merged_at.nil?
        GitHub.dogstats.time("pull_request", tags: ["action:is_head_merged_into_base", "sync:true"]) do
          is_head_merged_into_base?
        end
      end

      inferred_merge_sha = nil

      # warm this up to be used in the transaction
      if merged
        GitHub.dogstats.time("pull_request", tags: ["action:close_issues", "sync:true"]) do
          close_issues
        end
        # If a merge commit is available in the history, persist it for revertability.
        GitHub.dogstats.time("pull_request", tags: ["action:determine_merge_sha"]) do
          inferred_merge_sha = determine_merge_sha
        end
      end

      # XXX force pushes just break pr history so let's just truncate the review
      # history in that case for now
      LastSeenPullRequestRevision.clear(self) if forced || changing_base

      force_push_event =
        if forced && ref && before && after
          if pushed_to_base
            if !repo.rpc.descendant_of?(after, saved_base_sha)
              :base_ref_force_pushed
            end
          else
            :head_ref_force_pushed
          end
        end

      # stuff we need below, but don't want to cause slowing of the transaction.
      no_common_ancestor = !cross_network? && !common_ancestor?
      head_merged_into_base = is_head_merged_into_base?
      should_dismiss_stale_reviews = dismiss_stale_reviews?(
        before: saved_head_sha, after: head_sha, base_changed: changing_base)
      should_dismiss_file_reviews = dismiss_file_reviews?(
        before: saved_head_sha, after: head_sha, base_changed: changing_base)
      has_potential_codeowners = codeowners!.any?

      if (forced || changing_base || pushed_to_head) && base_repository.pull_request_revisions_enabled?
        # Fetch the first revision so it may be locked by id in the transaction.
        first_revision = revisions.first
        latest_revision = nil
      end

      GitHub.dogstats.time("pull_request", tags: ["action:synchronize_txn"]) do
        # DON'T PUT ANYTHING UNNECESSARY INSIDE OF THIS TRANSACTION
        #
        # This transaction has a history of being slow and causing problems
        # for the pull requests table. Any calculations, calls to git or external
        # services etc, should take place outside of the transaction. Ideally anything
        # in here will be limited strictly to database operations.
        transaction do
          # Lock the first revision to prevent a race condition against a new revision
          # being created. Lock it immediately so that the transaction snapshot reflects
          # the state after the lock is released by another contending process.
          first_revision.lock! if first_revision

          GitHub.dogstats.time("pull_request", tags: ["action:save_review_comment_positions", "sync:true"]) do
            save_review_comment_positions if commit_points_changed && !GitHub.flipper[:skip_comment_repositioning].enabled?(repository)
          end

          if changing_base_automatically
            raise RefPairingAlreadyExistsError.new(self, ref) if find_existing

            # If we're automatically changing base and there are no changed
            # commits, then the pull request will be marked as merged.
            #
            # Reset the base sha here so that the PR's comparison page will
            # show the diff at the time of merge before the base update.
            self.base_sha = saved_base_sha if changed_commits.none?

            create_issue_event(:automatic_base_change_succeeded,
                              user,
                              title_was: old_display_base_ref_name,
                              title_is: display_base_ref_name)
          elsif changing_base
            raise RefPairingAlreadyExistsError.new(self, ref) if find_existing
            raise ComparisonWouldBeEmptyError.new(self, ref) if changed_commits.none?
            create_issue_event(:base_ref_changed,
                              user,
                              title_was: old_display_base_ref_name,
                              title_is: display_base_ref_name)
          end

          if merged
            GitHub.dogstats.time("pull_request", tags: ["action:mark_as_merged", "sync:true"]) do
              mark_as_merged(user, inferred_merge_sha)
            end
          elsif !pushed_to_base && head_merged_into_base
            close(user)
          elsif no_common_ancestor
            GitHub.dogstats.increment("pull_request", tags: ["action:closed_no_common_ancestor"])
            close(user)
          end

          if should_dismiss_stale_reviews
            GitHub.dogstats.time("pullrequest.sync.dismiss_stale_reviews") do
              dismiss_stale_reviews(actor: user, base_changed: changing_base)
            end
          end

          if should_dismiss_file_reviews
            GitHub.dogstats.time("pullrequest.sync.dismiss_file_reviews") do
              dismiss_file_reviews(before: saved_head_sha, after: head_sha, actor: user)
            end
          end

          diff_may_have_changed = forced || changing_base || pushed_to_head

          should_request_codeowner_reviews =
            ready_for_review? && has_potential_codeowners && diff_may_have_changed

          if should_request_codeowner_reviews
            GitHub.dogstats.time("pullrequest.sync.request_code_owners") do
              request_review_from_codeowners(user)
            end
          end

          if force_push_event
            create_issue_event(force_push_event,
                               user,
                               commit_repository_id: repo.id,
                               before_commit_oid: before,
                               after_commit_oid: after,
                               ref: ref)
          end

          if commit_points_changed || merged || changing_base
            update_mergeable_attribute(nil)
            GitHub::SchemaDomain.allowing_cross_domain_transactions { save! }
          end

          if first_revision
            latest_revision = revisions.last

            if forced || changing_base
              if latest_revision.draft?
                latest_revision.update!(
                  force_pushed: latest_revision.force_pushed || forced,
                  base_oid: base_sha,
                  head_oid: head_sha,
                  commits_count: changed_commits.count,
                  revised_at: Time.now.utc,
                  actor: user,
                )
              else
                revisions.create!(
                  number: latest_revision.number + 1,
                  ready: true,
                  force_pushed: forced,
                  base_oid: base_sha,
                  head_oid: head_sha,
                  commits_count: changed_commits.count,
                  revised_at: Time.now.utc,
                  actor: user,
                )
              end
            elsif pushed_to_head
              # Wait to update the commits_count below, outside of the transaction.
              latest_revision.update!(
                head_oid: head_sha,
                revised_at: Time.now.utc,
              )
            end
          end
        end

        # If this was a normal push to the head ref we'll need to check
        # the numbers of commits between the latest revision's base and head.
        #
        # We do this outside of the above transaction to avoid a potentially slow
        # network call inside it.
        if latest_revision && pushed_to_head && !(forced || changing_base)
          commits_count = repository.comparison(
            latest_revision.base_oid,
            latest_revision.head_oid,
            COMMIT_LIMIT,
            self
          ).total_commits

          latest_revision.update(
            commits_count: commits_count,
          )
        end

        if changing_base
          instrument(:change_base, {
            actor: user,
            issue: issue,
            before_base_ref: old_base_ref,
            before_base_sha: saved_base_sha,
            after_base_ref: ref,
            after_base_sha: after,
            spammy: user.spammy?,
            allowed: allowed?,
          })
        end

        if merged && head_repository&.delete_branch_on_merge?
          cleanup_head_ref(user, allow_updating_dependents: true)
        end

        # we don't track events on spammy pull requests
        return true if user && user.spammy? && !allowed?
        if !merged && previous_changes.include?("head_sha")
          unless approved_before.nil?
            # Reset codeowners to get an accurate new review state
            @codeowners = nil
            approved_after = approved_with_no_changes_requested?
          end
          payload = {
            before: saved_head_sha,
            after: head_sha,
            issue: issue,
            approved_before: approved_before,
            approved_after: approved_after,
          }
          if user.present?
            payload[:actor]   = user
            payload[:spammy]  = user.spammy?
            payload[:allowed] = allowed?
          end

          instrument(:synchronize, payload)
        end

        true
      end
    end
  end

  def allowed?
    repository.permit?(user, :write)
  end

  # A Repository::Codeowners object for the current changed paths.
  #
  # If there's an error determining the changed paths on this pull, the
  # Repository::Codeowners instance will effectively act as a NullObject.
  #
  # Returns a Repository::Codeowners object.
  def codeowners
    @codeowners ||= Repository::Codeowners.new(base_repository, ref: base_ref_name).tap do |codeowners|
      codeowners.paths = changed_paths_for_codeowners if codeowners.file
    end
  end

  # A Repository::Codeowners object for the current changed paths.
  #
  # Raises DetermineCodeownersError.
  # Returns a Repository::Codeowners object.
  def codeowners!
    codeowners.tap do
      raise DetermineCodeownersError.new(@codeowners_paths_load_error) if codeowners_paths_load_error?
    end
  end

  # Find the paths changed by this pull request which may be owned by code owners.
  #
  # Will return an empty Array if the diff can't be loaded.
  #
  # codeowners_paths_load_error? can be used to check whether or not
  # the diff was loaded.
  #
  # Returns an array of String paths.
  def changed_paths_for_codeowners
    historical_comparison.init_diffs.deltas.flat_map(&:paths).uniq
  rescue GitRPC::ObjectMissing => err
    @codeowners_paths_load_error = err
    []
  end

  # Internal: Can we trust the Repository::Codeowners object for this pull?
  #
  # True if we were able to load the historical diff for codeowners, false
  # if that encountered an error.
  #
  # Returns a boolean.
  def codeowners_paths_load_error?
    !!@codeowners_paths_load_error
  end

  # Internal: May be raised when there was an error determining the changed
  # paths for this pull request.
  class DetermineCodeownersError < StandardError
    attr_reader :cause

    # Public: Initialize a new PullRequest::DetermineCodeownersError.
    #
    # cause - The root error encountered when finding the changed paths
    #         for the pull request.
    #
    # Returns the error object.
    def initialize(cause)
      @cause = cause
      super(cause&.message)
    end
  end

  # In the event that we missed a push (dropped job perhaps), try to
  # catch the PR up to the current state of the branches involved.
  # We might attribute some things to the wrong user or miss out on
  # some details about pushes we missed. This could get better if we
  # end up using the reflog someday.
  #
  # This is only ever used from stafftools and rarely so.
  def catch_up
    synchronize!(user: base_repository.owner, repo: base_repository)
  end

  # recalculate `position`s and `blob_position`s for all associated review comments.
  def recalculate_all_positions
    recalculate_review_comment_diff_positions
    recalculate_review_comment_blob_positions
  end

  def recalculate_review_comment_diff_positions
    comment_count = review_comments.count
    return if comment_count < 1

    outdated = GitHub.dogstats.time("pull_request.sync.diff_position_recalculation") do
      recalculate_diff_positions_with_blob_positions
    end

    GitHub.dogstats.histogram("pull_request.sync.repositioned_comments", comment_count)
    GitHub.dogstats.histogram("pull_request.sync.outdated_comments", outdated)
  end

  def recalculate_review_comment_blob_positions
    outdated = 0
    review_comments.each do |comment|
      comment.update_position_data if comment.needs_position_update?
      outdated += 1 if !comment.blob_position_was.nil? && comment.blob_position.nil?
    end

    outdated
  end

  def recalculate_diff_positions_with_blob_positions
    outdated = 0
    return outdated if review_comments.empty?

    grouped_comments = review_comments.group_by(&:pull_request_review_thread_id).values

    adjustment_promises = grouped_comments.map do |comment_group|
      comment = comment_group.first
      comment.async_reposition_from_blob_position.then do
        comment_group[1..-1].each { |c| c.set_position_and_current_commit_oid(comment.position, comment.commit_id) }
        outdated += comment_group.size if comment.position_was.present? && comment.position.nil?
      end
    end

    Promise.all(adjustment_promises).sync
    outdated
  end

  # Public: Returns the current full diff for the PR, limited to those paths that have comments
  #         on them with maximal limits on each entry.
  #
  # Returns the diff object, or nil if one can't be generated due to missing objects.
  def async_current_comments_diff
    return @async_current_comments_diff if defined?(@async_current_comments_diff)
    @async_current_comments_diff = async_review_comments.then do |review_comments|
      begin
        diff = historical_comparison.init_diffs.only_params
        diff.maximize_single_entry_limits!
        diff.add_paths(review_comments.map(&:path))
        diff
      rescue GitRPC::ObjectMissing
        # This can happen when commits are manually removed from a repo, such
        # as when Support scrubs the pull request, deletes the refs, and runs gc.
        nil
      end
    end
  end

  def current_comments_diff
    async_current_comments_diff.sync
  end

  # We save the review comments separately, so the recalculating
  # can be done outside of the transaction.
  def save_review_comment_positions
    review_comments.each(&:save_positions)
  end

  # Retrieve all open pull requests for the given repository and ref. Any pull
  # request whose head or base ref matches is returned.
  #
  # Returns an array of PullRequest objects.
  def self.find_open_based_on_ref(repository, ref)
    open_based_on_ref(repository, ref).to_a
  end

  # Retrieve all open pull request ids for the given repository and ref. Any
  # pull request whose head or base ref matches is returned.
  #
  # Returns an array of integer ids.
  def self.find_open_ids_based_on_ref(repository, ref)
    refs = Git::Ref.permutations(ref).map { |ref| GitHub::SQL::BINARY(ref) }

    # If the repository is not part of an advisory workspace we can assume that
    # pr.base_repository_id = pr.repository_id and since we join on issues.repository_id = pr.repository_id
    # then we can filter the issues by repository_id as well, potentially using much more efficient indexes
    # in the issues table. See https://github.com/github/github/pull/110041
    #
    # Ordering pull ids from newest to oldest speeds up marking PRs as merged
    # in large repos, see https://github.com/github/github/issues/99241 and
    # https://github.com/github/github/pull/99449.
    issues_filter = "AND issues.repository_id = :repository_id" unless repository.advisory_workspace? || GitHub.importing?

    github_sql.values <<-SQL, repository_id: repository.id, refs: refs
      (
        SELECT pr.id
        FROM pull_requests pr
        JOIN issues ON pr.id = issues.pull_request_id AND issues.repository_id = pr.repository_id
        WHERE pr.base_repository_id = :repository_id AND pr.base_ref IN :refs
        AND issues.state = 'open'
        #{issues_filter}
      )

      UNION

      (
        SELECT pr.id
        FROM pull_requests pr
        JOIN issues ON pr.id = issues.pull_request_id AND issues.repository_id = pr.repository_id
        WHERE pr.head_repository_id = :repository_id AND pr.head_ref IN :refs
        AND issues.state = 'open'
      )

      ORDER BY id DESC
    SQL
  end

  # Retrieve all open pull requests for the given head repository and head ref
  #
  # Returns an array of PullRequest objects.
  def self.find_open_based_on_head_ref(repository, ref)
    if (ids = find_open_ids_based_on_head_ref(repository, ref)).any?
      includes(:issue).find(ids)
    else
      []
    end
  end

  # Retrieve all open pull request ids for the given head repository and head ref.
  #
  # Returns an array of integer ids.
  def self.find_open_ids_based_on_head_ref(repository, ref)
    refs = Git::Ref.permutations(ref).map { |ref| GitHub::SQL::BINARY(ref) }
    github_sql.values <<-SQL, repository_id: repository.id, refs: refs
      SELECT pr.id
      FROM pull_requests pr
      JOIN issues ON pr.id = issues.pull_request_id AND issues.repository_id = pr.repository_id
      WHERE (
        ( pr.head_repository_id = :repository_id AND pr.head_ref IN :refs )
      ) AND issues.state = 'open'
    SQL
  end

  # Called when a branch is deleted. Closes any open pull requests that have the
  # branch as either the base or head.
  def self.after_branch_delete(repository, ref, pusher, before_sha)
    find_open_based_on_ref(repository, ref).each do |pull|
      pull.close(pusher)
    end

    mark_head_ref_as(:deleted, repository, ref, before_sha, pusher)
  end

  # Updates any PR records that match the given repository, ref,
  # and head_sha, to indicate that the head branch was either deleted
  # or restored.
  #
  # action     - :deleted or :restored
  # repository - The Repository that the ref was deleted or restored in
  # ref        - The ref name that was deleted or restored
  # sha        - The SHA of the ref at the time of deletion or restoration
  # user       - The User who deleted or restored the head_ref
  def self.mark_head_ref_as(action, repository, ref, sha, user)
    where(
      head_repository_id: repository.id,
      head_ref: Git::Ref.permutations(ref),
      head_sha: sha,
    ).each do |pull|
      pull.create_head_ref_event(action, user)
    end
  end

  # Creates a head_ref_deleted or head_ref_restored
  # event in a way that protects against duplicate
  # events that could arise from a race condition.
  #
  # head_ref_deleted and head_ref_restored should only
  # ever alternate, e.g. it should not be possible
  # to have two head_ref_deleted events in a row.
  #
  # It should also not be possible to have a head_ref_restored
  # event without having a head_ref_deleted event.
  #
  # This method keeps that from happening.
  #
  # action - :deleted or :restored
  # user   - The User that deleted or restored the branch
  #
  # Returns the new IssueEvent, or nil if one was not
  #         created due to the existence of a duplicate
  def create_head_ref_event(action, user)
    event = {
      deleted: :head_ref_deleted,
      restored: :head_ref_restored,
    }[action]

    raise "Unknown action" if event.nil?

    transaction do
      reload(lock: true)

      last_head_ref_event = events.head_ref.last

      # It must have been deleted to be restored
      return if last_head_ref_event.nil? && event == :head_ref_restored
      # It cannot be deleted or restored twice in a row
      return if last_head_ref_event && last_head_ref_event.event == event.to_s

      head_ref_event = create_issue_event(event, user)
      touch
      head_ref_event
    end
  end

  # Update pull requests on the given repository that are based on the given
  # ref. The pull request is closed when the head is merged into the base.
  #
  # NOTE Because moving through all pull requests can take some time and other
  # push events can trigger merges behind our back, each pull request is
  # loaded immediately before being synchronized to minimize the window where a
  # merge can be detected by two separate workers processing two separate pushes.
  # See the following issue for more info:
  #
  # https://github.com/github/github/issues/5827
  def self.synchronize_requests_for_ref(repository, ref, pusher, forced: false, before: nil, after: nil)
    if pusher && pusher.is_a?(Bot)
      pusher.installation = pusher.integration.installations.with_repository(repository).first
    end

    pull_ids = find_open_ids_based_on_ref(repository, ref)

    GitHub.dogstats.histogram("pull_request", pull_ids.size, tags: ["action:synchronize_number_of_pulls"])

    pull_ids.each do |pull_id|
      if GitHub.flipper[:parallelize_pull_synchronization].enabled?(repository)
        SynchronizePullRequestJob.perform_later(
          pull_request_id: pull_id,
          user: pusher,
          installation: pusher.try(:installation),
          repo: repository,
          forced: forced,
          ref: ref,
          before: before,
          after: after,
        )
      else
        begin
          pull = includes(:issue).find(pull_id)
          pull.synchronize!(user: pusher,
                            repo: repository,
                            forced: forced,
                            ref: ref,
                            before: before,
                            after: after)
        rescue => boom
          # don't let exceptions stop us from processing other open requests
          Failbot.report(boom,
                         repo_id: repository.id,
                         user_id: repository.owner.id,
                         pusher: pusher && pusher.to_s,
                         force_pushed: forced,
                         pull_request_id: pull_id,
                        )
        end
      end
    end
  end

  # Before a Repository is deleted, make sure to sync any commits involved in cross-repo pulls
  # to the base repo. This could be necessary if the head repo is deleted immediately after
  # opening a PR, causing it to race with the MaintainTrackingRef job, or if a MaintainTrackingRef
  # job failed for some reason previously.
  def self.sync_outstanding_commits(repository)
    where("head_repository_id = ? AND head_repository_id <> base_repository_id", repository.id).each do |pull|
      begin
        pull.maintain_tracking_ref_with_retries(pull.safe_user)
      rescue => boom
        Failbot.report(boom, pull_request_id: pull.id)
      end
    end
  end

  def async_performed_via_integration
    async_issue.then(&:async_performed_via_integration)
  end

  def async_viewer_can_update?(viewer)
    async_viewer_cannot_update_reasons(viewer).then(&:empty?)
  end

  def async_viewer_cannot_update_reasons(viewer)
    async_issue.then { |issue| issue.async_viewer_cannot_update_reasons(viewer) }
  end

  def locked?
    issue.locked?
  end

  def locked_at
    issue.locked_at
  end

  def locked_for?(user)
    issue.locked_for?(user)
  end

  def locked_reason
    issue.locked_reason
  end

  def active_lock_reason
    issue.active_lock_reason
  end

  # Builds up a bunch of User hashes in as few queries as possible.  First,
  # go through the PullRequest, Issue, and related discussions for any user IDs
  # or emails.  Then, pull the user records from memcache or the DB.  Then,
  # build a simple index of User#to_hash values for each user.  The result
  # should be used to quickly look up the User hash data for a given email
  # or ID.
  #
  # entries - Optional Array of PullRequest discussion objects.
  #
  # Returns a Hash of ID => User#to_hash.
  def build_index_of_user_hashes(entries = self.discussion)
    user_ids    = [user_id, issue.try(:user_id), base_user_id, head_user_id]
    user_emails = []
    entries.flatten!
    entries.each do |entry|
      if id = entry.try(:user_id)
        user_ids << id
      elsif entry.respond_to?(:author_email)
        user_emails << entry.author_email
      elsif entry.respond_to?(:author)
        fail "this case should no longer be used"
      end
    end
    [user_ids, user_emails].each do |arr|
      arr.uniq!
      arr.compact!
    end

    user_hashes = {}
    if user_ids.present?
      User.with_ids(user_ids.flatten.compact).each do |user|
        user_hashes[user.id] = user.to_hash
      end
    end
    user_emails.each do |email|
      if user = User.find_by_email(email)
        user_hashes[email] = user.to_hash
      end
    end
    user_hashes
  end

  def async_in_advisory_workspace?
    async_repository.then do |repository|
      repository&.async_advisory_workspace?
    end
  end

  def in_advisory_workspace?
    async_in_advisory_workspace?.sync
  end

  #
  # Validations
  #

  def repository_and_base_repository_must_match
    return unless repository
    return if base_repository_id == repository_id

    errors.add(:base_ref, "#{base.inspect} should be in the root repo #{repository.name_with_owner}")
  end

  def head_repository_must_be_advisory_workspace
    return unless repository
    return unless in_advisory_workspace?

    return if head_repository_id == repository_id
    errors.add(:head_ref, "#{base.inspect} should be in the advisory workspace repo #{repository.name_with_owner}")
  end

  def base_repository_must_be_advisory_workspace_origin_repository
    return unless repository
    return unless in_advisory_workspace?

    return if base_repository_id == repository&.parent_advisory_repository&.id
    errors.add(:base_ref, "#{base.inspect} should target the advisory workspace repo #{repository.name_with_owner}")
  end

  # Validate that commits exist between the base and head.
  def must_have_commits
    return if Rails.env.test? && disable_disk_access?

    if !commits_pending?
      errors.add(:base, "No commits between #{base_label} and #{head_label}")
    end
  end

  def must_have_common_ancestor
    return if Rails.env.test? && disable_disk_access?
    return if repository.nil? || base_user.nil?
    return if !historical_comparison.valid?

    if !common_ancestor?
      errors.add(:base, "The #{head_label} branch has no history in common with #{base_label}")
    end
  end

  # Validate that base_ref is a real branch
  def base_ref_is_real_branch
    return if Rails.env.test? && disable_disk_access?

    errors.add(:base_ref, "must be a branch") unless base_ref_exist? || base_ref_exist?(refresh_refs: true)
  end

  # Validate that head_ref is a real branch
  def head_ref_is_real_branch
    return if Rails.env.test? && disable_disk_access?

    errors.add(:head_ref, "must be a branch") unless head_ref_exist? || head_ref_exist?(refresh_refs: true)
  end

  # Validate that head_ref isn't a heads-namespaced branch
  def head_ref_is_not_namespaced
    return if Rails.env.test? && disable_disk_access?

    if head_ref && head_ref.starts_with?("heads/")
      errors.add(:head, "must not include 'heads/' namespace") unless head_ref_exist?
    end
  end

  # Boolean flag used to disable the must_have_commits, ref creation, cached
  # diffstat, and network repository sync on create behavior. Used in tests when
  # only the record data need be present. All commit, comparison, diff or other
  # file access features will fail when this is set true.
  attr_accessor :disable_disk_access
  alias disable_disk_access? disable_disk_access

  # Validate that a new pull request isn't a duplicate.
  def duplicate_check
    if repository.present? && find_existing
      errors.add(:base, "A pull request already exists for #{head}.")
    end
  end

  #
  # Misc Internal Logic
  #

  # The sha currently associated with base_ref. As opposed to #base_sha,
  # which is cached when the pull request is created, or #concrete_base_sha,
  # which is the most ideal base for comparison.
  #
  # Returns a sha, or nil if the base branch has been deleted.
  def current_base_sha
    comparison.base_sha
  end
  alias current_base_oid current_base_sha

  def current_head_oid
    comparison.head_sha
  end

  # Used to report a slow run of git-branch-base to Haystack.
  class SlowBranchBase < StandardError
  end

  # Use the stored base commit SHA1 to find the most recent valid base commit on
  # the base branch. This uses a custom git-branch-base program to find the best
  # base commit SHA1 for the comparison. See the lib/git-core/bin/git-branch-base
  # file for more information on how it works.
  def concrete_base_sha
    if base_sha
      calculate_branch_base
    else
      current_base_sha
    end
  end

  # Run git-branch-base and report a SlowBranchBase exception if the
  # script took more than 500ms to run.
  def calculate_branch_base
    start = Time.now

    result = comparison.compare_repository.rpc.branch_base(comparison.head_sha, comparison.base_sha, base_sha)

    duration = (Time.now - start) * 1000

    if duration >= (GitHub.enterprise? ? 5000 : 500)
      boom = SlowBranchBase.new("git-branch-base was slow")
      boom.set_backtrace(caller)

      Failbot.report(boom,
        app: "slow-pull-requests",
        repo_id: repository.id,
        pull_request_id: id,
        head_oid: comparison.head_sha,
        base_oid: comparison.base_sha,
        root_oid: base_sha,
      )
    end

    result
  end

  alias mergeable_base_sha current_base_sha

  def mergeable_head_sha
    head_sha
  end

  def set_contributed_at
    self.contributed_at ||= Time.zone.now
  end

  # after_create callback to ensure the pull request author is subscribed
  def subscribe_author
    return if user.nil? || issue.try(:user_id) == user_id
    issue.subscribe user, :author
  end

  def contribution_time
    @contribution_time ||= if contributed_at_timezone_aware?
      contributed_at
    else
      created_at.localtime
    end
  end

  def contributed_on
    contribution_time.to_date
  end

  def is_searchable?
    # If the repository is missing, then we have a rogue pull request
    return false if repository.nil?
    # If the repository is not searchable, its PRs shouldn't be either
    return false unless repository.repo_is_searchable?
    # Data quality issue where the issue is nil
    return false if issue.nil?
    # If the base user deleted their account
    return false if base_user.nil?
    # The read_attribute here is used for speed to bypass the potentially slow #spammy? method.
    return false if safe_user.read_attribute(:spammy)

    # If we got this far, then add to the search index
    return true
  end

  # Synchronize this pull request with its representation in the search
  # index. If the pull request is newly created or modified in some fashion,
  # then it will be updated in the search index. If the pull request has been
  # destroyed, then it will be removed from the search index. This method
  # handles both cases.
  def synchronize_search_index
    if self.destroyed?
      RemoveFromSearchIndexJob.perform_later("pull_request", self.id, self.repository_id)
    else
      Search.add_to_search_index("pull_request", self.id)
    end
    self
  end

  class OutOfSyncPullRequestIndex < Error; end

  # Called when a pull request gets out of sync with the search index via some
  # error or timeout. This method is called in exceptional situations, so we log
  # to Datadog and Haystack.
  def resynchronize_search_index
    GitHub.dogstats.increment("pull_request.reindexed")

    boom = OutOfSyncPullRequestIndex.new("Pull request search index is out of sync and is being reindexed")
    boom.set_backtrace(caller)
    Failbot.report(
      boom,
      repo_id: repository.id,
      pull_request_id: id,
      issue_id: issue.id,
      app: "github-pull-requests",
    )

    synchronize_search_index
  end

  include Instrumentation::Model

  def event_prefix() :pull_request end
  def event_payload
    event_context
  end

  def reset_memoized_attributes
    @merge_ref             = nil
    @rebase_ref            = nil
    @comparison            = nil
    @merge_comparison      = nil
    remove_instance_variable(:@async_build_comparison) if defined?(@async_build_comparison)
    remove_instance_variable(:@async_compare_repository) if defined?(@async_compare_repository)
    remove_instance_variable(:@async_changed_commits) if defined?(@async_changed_commits)
    @participants          = nil
    @total_comments        = nil
    @branch                = nil
    @author_and_committer_count = nil
    @close_issues          = nil
    @filter_result         = nil
    @filter_content        = nil
    @pushable_repo_by_user = nil
    @merge_states          = nil
    @blocked_from_reviewing = nil
    remove_instance_variable(:@async_current_comments_diff) if defined?(@async_current_comments_diff)
    remove_instance_variable(:@rebase_state) if defined?(@rebase_state)
    remove_instance_variable(:@status) if defined?(@status)
    remove_instance_variable(:@corrupt) if defined?(@corrupt)
    remove_instance_variable(:@latest_enforced_reviews) if defined?(@latest_enforced_reviews)
    remove_instance_variable(:@pending_review_requests_by_reviewer) if defined?(@pending_review_requests_by_reviewer)
  end

  def combined_status
    return @combined_status if defined?(@combined_status)
    @combined_status = build_combined_status_for_sha(head_sha)
  end

  def combined_status=(status)
    if status && !status.is_a?(PullRequest::MergeStatus)
      raise TypeError, "PullRequest#combined_status must be of type PullRequest::MergeStatus, but was #{status.class}"
    end
    @combined_status = status
  end

  def build_combined_status_for_sha(sha)
    PullRequest::MergeStatus.new(base_repository, sha, target_branch: base_ref_name)
  end

  def protected_base_branch
    return @protected_base_branch if defined?(@protected_base_branch)
    if base_repository
      @protected_base_branch = ProtectedBranch.for_repository_with_branch_name(base_repository, base_ref_name)
    end
  end

  def protected_base_branch=(protected_branch)
    if protected_branch && !protected_branch.is_a?(ProtectedBranch)
      raise TypeError, "PullRequest#protected_base_branch must be of type ProtectedBranch, but was #{protected_branch.class}"
    end
    @protected_base_branch = protected_branch
  end

  def protected_head_branch
    return @protected_head_branch if defined?(@protected_head_branch)
    @protected_head_branch = ProtectedBranch.for_repository_with_branch_name(head_repository, head_ref_name)
  end

  def default_merge_commit_title
    "Merge pull request ##{number} from #{safe_head_user.login}/#{display_head_ref_name}"
  end

  def merging_one_commit?
    first_real_commit = nil
    has_multiple_commits = false
    count = 0
    changed_commits.each do |commit|
      next if commit.merge_commit?
      count += 1
      if count > 1
        has_multiple_commits = true
        break
      end
      first_real_commit = commit
    end

    [!has_multiple_commits  && first_real_commit, first_real_commit]
  end

  def default_squash_commit_message(author: user)
    return @squash_commit_message if @squash_commit_message
    return "" if corrupt?

    message = ""
    merging_one_commit, first_real_commit = merging_one_commit?

    if merging_one_commit
      # If there's only one commit, the squash commit "message" is populated
      # with that commit's messages _except_ for the first line. That's because
      # the first line of the commit message is returned by the
      # default_squash_commit_title method instead.
      lines = first_real_commit.message.lines[1..-1]
      message << lines.join if lines
    else
      # If there are multiple commits, we include the commit message for each
      # one in a bulleted list.
      changed_commits.each do |commit|
        # Exclude messages for merge commits and blank commit "titles."
        next if commit.merge_commit? || commit.message.lines.first.nil?
        message << "* #{commit.message.strip}\n\n"
      end
    end

    message.strip!

    git_actors = squash_commit_co_author_actors(author: author)
    if git_actors.any?
      # Separate the trailers from the current message (if needed).
      message << "\n\n" if message.present?

      git_actors.each do |git_actor|
        message << "Co-authored-by: #{git_actor.display_name} <#{git_actor.display_email}>\n"
      end
    end

    message.rstrip!

    @squash_commit_message = message
  end

  def default_squash_commit_title
    return @squash_commit_title if @squash_commit_title

    merging_one_commit, first_real_commit = merging_one_commit?

    @squash_commit_title =
      if !merging_one_commit || !first_real_commit.message.lines.first
        title
      else
        first_real_commit.message.lines.first.rstrip
      end
    @squash_commit_title += " (##{number})"
  end

  def fork_collab_granted?
    open? && fork_collab_allowed?
  end

  def compute_base_commit_id(head_commit_id)
    comparison = build_comparison(head_commit_oid: head_commit_id)
    return unless comparison.valid?
    comparison.merge_base
  end

  def matches_diff_range?(diffs)
    base_sha == diffs.sha1 && head_sha == diffs.sha2
  end

  def conflicted_files
    return nil if conflict.nil?
    conflict.filenames
  end

  def conflicted_file_contents(filename, ancestor_oid, base_oid, head_oid)
    ancestor_blob, base_blob, head_blob =
      if ancestor_oid.nil?
        # handle the case where the file was added in both revisions, no ancestor
        bb, hb = repository.rpc.read_blobs([base_oid, head_oid])
        [{}, bb, hb]
      else
        repository.rpc.read_blobs([ancestor_oid, base_oid, head_oid])
      end

    conflicted_file = repository.rpc.merge_single_file(
      ancestor_oid ? { oid: ancestor_oid, path: filename, filemode: 0100644 } : nil,
      { oid: base_oid,     path: filename, filemode: 0100644 },
      { oid: head_oid,     path: filename, filemode: 0100644 },
      our_label: head_ref, their_label: base_ref,
    )[:data]

    base_blob["path"] = head_blob["path"] = ancestor_blob["path"] = filename

    head_entry = TreeEntry.new(repository, head_blob)
    base_entry = TreeEntry.new(repository, base_blob)
    ancestor_entry = TreeEntry.new(repository, ancestor_blob)

    conflicted_file_contents_entry = TreeEntry.new(repository, {
      "data" => conflicted_file,
      "oid" => "",
      "type" => "blob",
      "binary" => head_blob["binary"] || ancestor_blob["binary"] || base_blob["binary"],
      "encoding" => head_blob["encoding"] || ancestor_blob["encoding"] || base_blob["encoding"],
    })

    [conflicted_file_contents_entry, ancestor_entry, base_entry, head_entry]
  end

  def conflict_resolvable?
    # in some cases, the conflict is too weird for our ui to resolve right now
    # like if a file was deleted in one side of the merge, or if there are any
    # submodules.
    return false if conflict.nil?
    conflict.resolvable? && conflict.head_sha == mergeable_head_sha && conflict.base_sha == mergeable_base_sha
  end

  def store_conflicts(conflicts_hash)
    base = conflicts_hash.delete(:base)
    head = conflicts_hash.delete(:head)

    # we don't want non-utf8 filenames to be truncated, so we just encode them going into the database
    conflicted_files = {}
    conflicts_hash[:conflicted_files].each_key { |k| conflicted_files[CGI.escape(k)] = conflicts_hash[:conflicted_files][k] }
    conflicts_hash[:conflicted_files] = conflicted_files

    if conflict.nil?
      begin
        create_conflict!(
          base_sha: base,
          head_sha: head,
          info: conflicts_hash.to_json,
        )
        already_exists = false
      rescue ActiveRecord::RecordNotUnique
        already_exists = true
      end
    else
      already_exists = true
    end

    if already_exists
      conflict.base_sha = base
      conflict.head_sha = head
      conflict.info = conflicts_hash.to_json
      conflict.save!
    end
  end

  # This allows a PullRequest to implement the Reactable GraphQL interface.
  #
  # Typically you would achieve that result by mixing in Reaction::Subject::RepositoryContext, but
  # a PullRequest is not currently a valid subject of a reaction.
  # FIXME: Make a PullRequest a valid subject of a reaction. This will require migrating existing
  #        reactions that are currently attached to the pull request's issue.
  def async_reaction_admin
    async_issue.then(&:async_reaction_admin)
  end

  def async_reaction_path
    async_issue.then(&:async_reaction_path)
  end

  def user_unable_to_create_pr?
    !GitHub.enterprise? &&
    repository &&
    repository.public? &&
    user &&
    (body.blank? || body.squish == default_body.squish) &&
    !user.is_a?(Bot) &&
    !user.login.match(/bot/i) &&
    !(repository.owner.organization? && repository.owner.member?(user)) &&
    head_repository &&
    base_repository &&
    !head_repository.pushable_by?(user) &&
    !base_repository.pushable_by?(user)
  end

  def suggested_change_applicable_by?(committer)
    return false unless committer

    @suggested_change_applicable_by ||= {}
    return @suggested_change_applicable_by[committer.id] if @suggested_change_applicable_by.key?(committer.id)

    @suggested_change_applicable_by[committer.id] = async_head_repository.then do |head_repo|
      next false unless head_repo
      head_repo.pushable_by?(committer, ref: head_ref_name)
    end.sync
  end

  def head_is_default_branch?
    head_ref_name == head_repository.default_branch
  end

  def head_is_protected_branch?
    protected_head_branch.present?
  end

  def head_is_default_or_protected_branch?
    head_is_default_branch? || head_is_protected_branch?
  end

  # Public: Returns the Issue associated with this PullRequest. This helps to
  # avoid `is_a?` calls when you have a variable that can be an issue or PR.
  #
  # Examples
  #
  #   issue_or_pr.to_issue.my_favorite_issue_method
  #
  # Returns an Issue
  def to_issue
    issue
  end

  # Public: Is this github process in importing mode?
  #
  # Returns a boolean.
  def importing?
    GitHub.importing?
  end

  def most_recent_vulnerability_dependency_update
    dependency_updates.visible.vulnerability.complete.first
  end

  # Internal: Search the history of the base ref for the most topologically
  # recent merge commit with this PR's head_sha as a parent, i.e. the ususal
  # value of merge_commit_sha after a PR is merged.
  #
  # This is useful in cases where a PR was merged implicitly as part of another
  # PR (which eventually was merged to the base branch), or some other scheme
  # that involves merge commits but does not involve pressing the merge button.
  # In these cases we'd like to know the appropriate merge commit so that the
  # revert button continues to work.
  #
  # This is intended to be called as part of PullRequest#synchronize.  In this
  # context it should be cheap since the distance between
  # current_base_oid and head_sha should be small (since the PR will have just
  # recently been merged).  It should also be accurate for older PRs, but it
  # may have to search through quite a bit of history to come up with an
  # answer (~12s for the oldest github/github PRs)
  def determine_merge_sha(page_size: 10_000, max_pages: 5)
    current_base_oid = repository.refs.read(base_ref).target_oid
    range_start = current_base_oid
    range_end = head_sha
    current_page = 1
    loop do
      candidate_merge_commits = repository.rpc.read_commits(
        # git rev-list --merges -n page_size range_start..range_end
        repository.rpc.rev_list([range_start, range_end], symmetric: true, merges: true, limit: page_size),
      )

      merge_commit = candidate_merge_commits.find { |commit| commit["parents"].include?(head_sha) }
      if merge_commit
        break merge_commit["oid"]
      elsif candidate_merge_commits.size == page_size
        # Didn't find the merge commit on this page.  Try the next one unless
        # we hit our limit.
        current_page += 1
        break nil if current_page > max_pages
        range_start = candidate_merge_commits.last["oid"]
      else
        # nothing found, give up
        break nil
      end
    end
  rescue GitRPC::ObjectMissing
    nil
  end

  def memex_suggestion_hash(last_interaction_at: nil)
    issue
      .memex_suggestion_hash(last_interaction_at: last_interaction_at)
      .merge(
        id: id,
        is_draft: draft?,
        type: "PullRequest",
        updated_at: updated_at.utc.iso8601,
      )
  end

  # Implements MemexProjectItem::Content#memex_content_hash.
  def memex_content_hash
    issue.memex_content_hash.merge({ id: id })
  end

  # Implements MemexProjectItem::Content#memex_system_defined_column_value.
  def memex_system_defined_column_value(column, require_prefilled_associations: true)
    value = issue.memex_system_defined_column_value(
      column,
      require_prefilled_associations: require_prefilled_associations
    )

    if column.name == MemexProjectColumn::TITLE_COLUMN_NAME
      value[:state] = state.to_s
      value[:is_draft] = draft?
    end

    value
  end

  def blocked_from_reviewing?(reviewer)
    return unless reviewer
    @blocked_from_reviewing ||= {}
    return @blocked_from_reviewing[reviewer.id] if @blocked_from_reviewing.key?(reviewer.id)

    blocked_by_author_and_lacks_push_access = reviewer.blocked_by?(user) && !repository.pushable_by?(reviewer)

    @blocked_from_reviewing[reviewer.id] = blocked_by_author_and_lacks_push_access || reviewer.blocked_by?(repository.owner)
  end

  private

  def async_commit_authors(viewer: nil)
    actor_promises = changed_commits.map do |commit|
      commit.async_unique_visible_author_actors(viewer)
    end
    Promise.all(actor_promises).then do |actors|
      user_promises = actors.flatten.map(&:async_user)
      Promise.all(user_promises).then do |users|
        users.compact.uniq.select { |commit_user| base_repository.pullable_by?(commit_user) }
      end
    end
  end

  # If there exists a User or Bot responsible for opening the pull request, we
  # exclude that user from the list of co-authors on the squash commit because
  # that user will be the commit's primary author.
  #
  # A commit authored by _any_ of a user's email addresses (not just verified
  # or user-entered email addresses) will associate the commit with that user.
  # So we make sure to exclude _all_ of the pull request opener's email
  # addresses from the squash commit's co-author trailers.
  #
  # Returns an Array of GitActor objects.
  def squash_commit_co_author_actors(author: user)
    git_actors = []
    emails_to_skip = author ? author.emails.map { |ue| ue.email.downcase } : []
    changed_commits.each do |commit|
      commit.author_actors.each do |git_actor|
        email = git_actor.display_email.downcase
        next if emails_to_skip.include?(email)
        emails_to_skip << email
        git_actors << git_actor
      end
    end
    git_actors
  end

  # Validate that the actor is allowed to create pull requests.
  def validate_authorized_to_create_content(actor: user)
    content_authorization = ContentAuthorizer.authorize(actor, :pull_request, :create,
                                                        issue: issue,
                                                        repo: repository)
    return if content_authorization.authorized?
    content_authorization.errors.each do |error|
      errors.add(:base, error.message)
    end
  end

  def validate_refs_readable
    unless repository && comparison && comparison.viewable_by?(user)
      errors.add(:base, "not all refs are readable")
    end
  end

  # Impose a limit on the number of pull requests that can be created with the same head_sha.
  # See https://github.com/github/github/issues/123630 for additional context.
  def validate_under_duplicate_head_sha_limit
    unless repository && repository.pull_requests.where(head_sha: head_sha).count < GitHub.duplicate_head_sha_limit
      errors.add(:base, "cannot have more than #{GitHub.duplicate_head_sha_limit} pull requests with the same head_sha.")
    end
  end

  def valid_cross_repo_collab
    if fork_collab_allowed?
      if !cross_repo?
        errors.add(:base, "Fork collab can only be enabled on cross-repo pull requests")
      elsif !head_repository.pushable_by?(issue.user)
        errors.add(:fork_collab, "Fork collab can't be granted by someone without permission")
      end
    end
  end

  def pr_reflog_data(via = nil)
    reflog_data = {
      pull_request_id: id,
      pull_request_head: head.b,
      pull_request_base: base.b,
      frontend: Socket.gethostname,
    }
    reflog_data[:combined_status] = combined_status.state if combined_status.any?
    reflog_data[:via] = via if via.present?
    reflog_data
  end

  def safe_ref_name(ref)
    ref.to_s.b.sub(/\Arefs\/heads\//, "")
  end

  def contributed_at_timezone_aware?
    contributed_at && contributed_at > Contribution::Calendar::TIMEZONE_AWARE_CUTOVER
  end

  # Internal: Ensure this PR counts towards the user's contributions.
  def clear_contributions_cache
    return if issue.nil?
    Contribution.clear_caches_for_user(issue.user)
  end

  # Internal: check and cache whether the default branch contains a mobile app.
  #
  # This will only be used if the repo doesn't already have CI, so don't bother
  # with the potentially expensive detection if CI is already running.
  #
  # Also don't bother checking if a cached result already exists.
  def detect_mobile_apps
    return if repository.has_ci? || GitHub.kv.exists(repository.mobile_key).value { nil }

    repository.set_mobile_status
  end

  # Internal: check and cache whether the default branch contains a Dockerfile.
  #
  # This will only be used if the repo doesn't already have CI, so don't bother
  # with the potentially expensive detection if CI is already running.
  #
  # Also don't bother checking if a cached result already exists.
  def detect_docker_file
    return if repository.has_ci? || GitHub.kv.exists(repository.docker_file_key).value { nil }

    repository.set_docker_file_status
  end

  # Internal: Updates the base ref of any of this repo's other PRs
  #           whose base ref is this PR's head ref.
  #
  # Short-circuits unless this pull is same-repo, else the wrong base
  # branch would be used.
  #
  # Short-circuits unless this pull is merged, otherwise we don't have
  # enough confidence to infer what the user's desired behavior would be.
  #
  # Return a boolean indicating success or failure.
  def update_base_of_dependent_prs(actor:)
    return false unless same_repo?
    return false unless merged?
    return false if head_is_default_branch?
    return false if protected_head_branch&.block_deletions_enabled?

    dependent_prs = PullRequest.
      open_pulls.
      to_repository(head_repository).
      for_base_ref(head_ref).
      from_repository(head_repository)

    return false if dependent_prs.count > AUTO_CHANGE_BASE_MAX_PULL_REQUESTS

    succeeded = true
    dependent_prs.each do |pr|
      begin
        prior_base_display_name = pr.display_base_ref_name
        pr.change_base_branch(actor, base_ref, automatic: true)
      rescue ActiveRecord::ActiveRecordError => err
        succeeded = false
        Failbot.report(err, pull_request_id: pr.id)
      rescue PullRequest::BaseNotChangeableError => err
        succeeded = false
        begin
          pr.create_issue_event(:automatic_base_change_failed,
                              actor,
                              message: err.error_type_key,
                              title_was: prior_base_display_name,
                              title_is: base_ref)
        rescue ActiveRecord::ActiveRecordError => err
          Failbot.report(err, pull_request_id: pr.id)
        end
      end
    end

    return succeeded
  end
end
