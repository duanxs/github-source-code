# frozen_string_literal: true
class DeceasedUser < ApplicationRecord::Collab
  include Instrumentation::Model
  include ActionView::Helpers::DateHelper

  areas_of_responsibility :community_and_safety
  belongs_to :user, required: true
  validates :user_id, uniqueness: true

  after_create_commit :instrument_creation

  def instrument_creation
    instrument :create,
      user: user
  end
end
