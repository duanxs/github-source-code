# frozen_string_literal: true

# Like many things in life, everything here is only here temporarily.
# See https://github.com/github/sponsors/issues/1431.
module SponsorsListing::TransitionDuplicateStripeAccountDependency

  # Public: Does this listing need to go through the process to transition to
  #         a new Stripe account that isn't a duplicate?
  #
  # Returns a Boolean.
  def needs_duplicate_stripe_remediation?
    return false unless GitHub.flipper[:duplicate_stripe_remediation].enabled?(sponsorable)
    duplicate_stripe_account?
  end

  # Public: The error that occurred when previously attempting to link a Stripe
  #         Connect account to this listing.
  #
  # Returns a String auth code to use with Stripe, or nil.
  def duplicate_stripe_remediation_auth_code
    GitHub.kv.get(duplicate_stripe_remediation_auth_key).value { nil }
  end

  # Public: Stores the authorization code for the new Stripe account this
  #         listing is transitioning to.
  #
  # code - A String authorization code to use with the Stripe API.
  #
  # Returns nothing.
  def set_duplicate_stripe_remediation_auth_code(code:)
    GitHub.kv.set(duplicate_stripe_remediation_auth_key, code)
  end

  # Public: Clears the auth code that was previously stored for the new Stripe
  #         account this listing was transitioning to.
  #
  # Returns nothing.
  def clear_duplicate_stripe_remediation_auth_code
    return if duplicate_stripe_remediation_auth_code.blank?
    GitHub.kv.del(duplicate_stripe_remediation_auth_key)
  end

  private

  def duplicate_stripe_remediation_auth_key
    "SponsorsDuplicateStripeRemediation:#{id}"
  end
end
