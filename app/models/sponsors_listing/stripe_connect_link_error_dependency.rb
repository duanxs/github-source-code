# frozen_string_literal: true

module SponsorsListing::StripeConnectLinkErrorDependency
  VALID_STRIPE_CONNECT_LINK_ERRORS = [:duplicate_account_id]

  # Public: Did a previous attempt at linking a Stripe Connect account to
  #         this listing fail?
  #
  # Returns a Boolean.
  def stripe_connect_link_failed?
    stripe_connect_link_error.present?
  end

  # Public: The error that occurred when previously attempting to link a Stripe
  #         Connect account to this listing.
  #
  # Returns a Symbol value VALID_STRIPE_CONNECT_LINK_ERRORS, or nil.
  def stripe_connect_link_error
    value = GitHub.kv.get(stripe_connect_link_error_key).value { nil }
    value&.to_sym
  end

  # Public: Clears the authorization code that we were trying to use to link this
  #         listing to a Stripe Connect account, and sets the error code in KV.
  #
  # error - A Symbol error that is a value from VALID_STRIPE_CONNECT_LINK_ERRORS.
  #
  # Returns nothing.
  def set_stripe_connect_link_error!(error:)
    unless VALID_STRIPE_CONNECT_LINK_ERRORS.include?(error)
      raise ArgumentError.new("`error` must be a value from VALID_STRIPE_CONNECT_LINK_ERRORS")
    end

    # there can't be an error if there's already an account successfully connected
    return if stripe_connect_account.present?

    # remove authorization code so we stop trying to link an invalid account
    update!(stripe_authorization_code: nil)
    GitHub.kv.set(stripe_connect_link_error_key, error.to_s)
    GitHub.dogstats.increment("stripe_connect_link_error.set", tags: ["error:#{error}"])
  end

  # Public: Clears the error that was stored when previously attempting to link
  #         a Stripe Connect account to this listing.
  #
  # Returns nothing.
  def clear_stripe_connect_link_error
    return if stripe_connect_link_error.blank?
    GitHub.kv.del(stripe_connect_link_error_key)
  end

  private

  def stripe_connect_link_error_key
    "SponsorsStripeConnectLinkError:#{id}"
  end
end
