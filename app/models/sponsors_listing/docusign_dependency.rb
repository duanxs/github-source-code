# frozen_string_literal: true

module SponsorsListing::DocusignDependency
  extend ActiveSupport::Concern

  included do
    has_many :docusign_envelopes, as: :owner
  end

  def docusign_voidable?
    active_docusign_envelope && (docusign_sent? || docusign_delivered?)
  end

  def docusign_delivered?
    active_docusign_envelope&.delivered?
  end

  def docusign_sent?
    active_docusign_envelope&.sent?
  end

  def docusign_completed?
    active_docusign_envelope&.completed?
  end

  def docusign_envelope?
    active_docusign_envelope.present?
  end

  def active_docusign_envelope
    docusign_envelopes.active.first
  end
end
