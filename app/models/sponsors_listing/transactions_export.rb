# frozen_string_literal: true

class SponsorsListing::TransactionsExport
  include ActionView::Helpers::NumberHelper

  CSV_HEADERS = [
    "transaction id",
    "organization",
    "sponsor handle",
    "sponsor email",
    "monthly tier amount",
    "processed amount",
    "transaction date",
    "status",
  ].freeze

  attr_reader :fiscal_host, :timeframe

  def initialize(fiscal_host:, timeframe: :month)
    @fiscal_host = fiscal_host
    @timeframe = timeframe
  end

  def as_csv
    GitHub::CSV.generate(encoding: Encoding::UTF_8) do |csv|
      # csv headers
      csv << CSV_HEADERS

      line_items.each do |line_item|
        sponsor = line_item.billing_transaction.live_user
        next unless sponsor
        sponsorable = line_item.subscribable.sponsors_listing.sponsorable
        sponsorship = sponsor.sponsorship_as_sponsor_for(sponsorable)
        public_sponsorship = sponsorship.privacy_public?
        share_with_host = public_sponsorship && sponsorship.is_sponsor_opted_in_to_share_with_fiscal_host?

        csv << [
          line_item.billing_transaction.transaction_id,
          sponsorable.login,
          public_sponsorship ? sponsor.login : "PRIVATE",
          share_with_host ? sponsor.publicly_visible_email(logged_in: true) : "PRIVATE",
          number_to_currency(line_item.subscribable.monthly_price_in_dollars),
          number_to_currency(line_item.amount_in_cents / 100),
          line_item.created_at,
          line_item.billing_transaction.last_status,
        ]
      end
    end
  end

  def filename
    "sponsors-#{fiscal_host}-transactions-#{Date.current}.csv"
  end

  private

  def line_items
    scope = Billing::BillingTransaction::LineItem
      .includes(billing_transaction: :live_user)
      .preload(subscribable: :sponsors_listing)
      .where(subscribable_id: tier_ids, subscribable_type: SponsorsTier.name)
      .order(:created_at)

    limit_timeframe(scope)
  end

  def tier_ids
    SponsorsTier
      .joins_sponsors_membership
      .where(sponsors_memberships: { fiscal_host: SponsorsMembership.fiscal_hosts[fiscal_host] })
      .pluck(:id)
  end

  def limit_timeframe(scope)
    case timeframe.to_sym
    when :all
      scope
    else
      # default to last month
      end_date = Date.current + 1.day
      start_date = Date.current - 1.month

      scope.where(created_at: start_date..end_date)
    end
  end
end
