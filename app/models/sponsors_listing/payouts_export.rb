# frozen_string_literal: true

class SponsorsListing::PayoutsExport
  CSV_HEADERS = [
    "payout id",
    "organization",
    "payout bank",
    "payout description",
    "processed amount",
    "payout date",
  ].freeze

  attr_reader :fiscal_host, :timeframe

  def initialize(fiscal_host:, timeframe: :month)
    @fiscal_host = fiscal_host
    @timeframe = timeframe
  end

  def as_csv
    GitHub::CSV.generate(encoding: Encoding::UTF_8) do |csv|
      # csv headers
      csv << CSV_HEADERS

      organizations.each do |organization|
        payouts(organization).each do |payout|
          arrival = Time.at(payout.arrival_date)
          next unless in_timeframe?(arrival)

          bank_name = if payout.destination.respond_to?(:bank_name)
            payout.destination.bank_name
          elsif payout.destination.respond_to?(:deleted) && payout.destination.deleted
            "Deleted #{payout.destination.currency.upcase} #{payout.destination.object.humanize}"
          else
            "Unknown Payout Destination"
          end

          amount = Billing::Money.new(payout.amount, payout.currency)
                                 .format
          csv << [
            payout.id,
            organization.login,
            bank_name,
            payout.statement_descriptor,
            amount,
            arrival.to_date,
          ]
        end
      end
    end
  end

  def filename
    "sponsors-#{fiscal_host}-payouts-#{Date.current}.csv"
  end

  private

  def organizations
    ids = SponsorsMembership.where(fiscal_host: fiscal_host)
                            .with_states(:accepted)
                            .joins(:sponsors_listing)
                            .preload(sponsors_listing: :stripe_connect_account)
                            .select { |m| m&.sponsors_listing&.stripe_connect_account }
                            .map { |m| m.sponsorable_id }
    orgs = Organization.where(id: ids)
                       .preload(sponsors_listing: :stripe_connect_account)

    stripe_ids = orgs.map do |org|
      org.sponsors_listing
         .stripe_connect_account
         .stripe_account_id
    end.uniq

    payout_webhooks = Billing::StripeWebhook.where(
      account_id: stripe_ids,
      kind: :payout_created,
    ).distinct
    if min_date
      payout_webhooks = payout_webhooks.where("created_at > ?", min_date)
    end
    paid_stripe_accounts = payout_webhooks.pluck(:account_id)

    one_payout_orgs = []
    orgs.find_each do |org|
      stripe_account_id = org.sponsors_listing.stripe_connect_account.stripe_account_id
      next unless paid_stripe_accounts.include?(stripe_account_id)
      one_payout_orgs << org
    end
    one_payout_orgs
  end

  def payouts(organization)
    stripe = organization.sponsors_listing
                         .stripe_connect_account
    return [] unless stripe

    if timeframe.to_sym == :month
      stripe.stripe_payouts(limit: 2)
    else
      stripe.stripe_payouts
    end.result
  end

  def in_timeframe?(date)
    return true if timeframe.to_sym != :month

    date >= min_date
  end

  def min_date
    return if timeframe.to_sym != :month

    @min_date ||= Date.current - 1.month
  end
end
