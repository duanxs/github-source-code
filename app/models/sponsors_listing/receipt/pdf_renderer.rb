# frozen_string_literal: true

require "prawn/table"

class SponsorsListing::Receipt::PdfRenderer
  attr_reader :receipt

  def initialize(receipt)
    @receipt = receipt
  end

  def render
    document.render
  end

  private

  def document
    return @document if defined?(@document)

    header_data = sponsor_header_data
    payout_data = sponsor_payout_data
    total_payout = receipt.total_payout.format(no_cents_if_whole: false)

    svg_logo = File.open("#{Rails.root}/public/images/modules/pricing/pdf-logo.svg").read

    @document = Prawn::Document.new({ margin: [50, 60, 50, 60] }) do
      font_families["Arial-Unicode"] = {
        normal: "#{Rails.root}/lib/assets/fonts/Arial Unicode Bold.ttf",
        bold: "#{Rails.root}/lib/assets/fonts/Arial Unicode Bold.ttf",
      }

      font_families["Helvetica"] = {
        normal: "#{Rails.root}/lib/assets/fonts/Helvetica.dfont",
        bold:   "#{Rails.root}/lib/assets/fonts/Helvetica Bold.ttf",
      }

      self.line_width = 0.3
      self.fallback_fonts ["Arial-Unicode"]

      fill_rectangle [65, cursor], bounds.width, 25

      move_down 60
      svg svg_logo, at: [0, cursor], width: 150

      move_down 50
      header_y_start = cursor

      bounding_box([0, header_y_start], width: 150, height: 70) do
        text "GitHub, Inc.", leading: 5, style: :bold
        text "88 Colin P Kelly Jr St", leading: 5
        text "San Francisco, CA 94107", leading: 5
      end

      header_y_end = cursor
      move_cursor_to header_y_start

      table(header_data, position: 160, column_widths: [140, 192]) do
        cells.borders = []
        column(0).padding = [0, 10, 5, 0]
        column(0).font_style = :bold
        column(0).align = :right
        column(1).padding = [0, 0, 5, 15]
      end

      move_cursor_to header_y_end

      move_down 50
      bounding_box([0, cursor], width: bounds.width, height: 150) do
        stroke_bounds

        move_down 20
        table(payout_data, column_widths: [bounds.width/2, bounds.width/2]) do
          cells.borders = []
          column(0).padding = [5, 5, 5, 25]
          column(1).padding = [5, 25, 5, 5]
          column(1).align = :right
        end
      end

      bounding_box([0, cursor], width: bounds.width, height: 50) do
        stroke_bounds

        move_down 20
        table([["Total Amount Due from GitHub (USD)", total_payout]], column_widths: [bounds.width/2, bounds.width/2]) do
          cells.borders = []
          cells.font_style = :bold
          column(0).padding = [0, 5, 5, 25]
          column(1).padding = [0, 25, 5, 5]
          column(1).align = :right
        end
      end
    end
  end

  def sponsor_header_data
    @sponsor_header_data ||= begin
      tmp_header_data = [
        ["Maintainer:", "#{receipt.sponsorable_legal_name} (@#{receipt.sponsorable_login})"],
        ["Statement Date:", Date.current.strftime("%b %e, %Y")],
        ["Statement Period:", "#{receipt.start_date.strftime("%b %e, %Y")} - #{receipt.end_date.strftime("%b %e, %Y")}"],
      ]

      tmp_header_data << ["Tax ID:", receipt.tax_id] if receipt.tax_id.present?
      tmp_header_data
    end
  end

  def sponsor_payout_data
    @sponsor_payout_data ||= begin
      tmp_payout_data = [
        ["Sponsorship Amount", receipt.total_sponsorship.format(no_cents_if_whole: false)],
      ]

      unless receipt.total_sponsorship_reversed.zero?
        tmp_payout_data << ["Sponsorship Reversed", receipt.total_sponsorship_reversed.format(no_cents_if_whole: false)]
      end

      unless receipt.total_match.zero?
        tmp_payout_data << ["GitHub Match", receipt.total_match.format(no_cents_if_whole: false)]
      end

      unless receipt.total_match_reversed.zero?
        tmp_payout_data << ["Match Reversed", receipt.total_match_reversed.format(no_cents_if_whole: false)]
      end

      tmp_payout_data
    end
  end
end
