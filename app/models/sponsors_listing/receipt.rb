# frozen_string_literal: true

class SponsorsListing::Receipt
  attr_reader :stripe_account, :sponsorable_login, :sponsorable_legal_name, :start_date, :end_date, :tax_id

  def initialize(sponsorable:, start_date:, end_date:, tax_id: nil)
    @stripe_account = sponsorable.sponsors_listing&.stripe_connect_account
    @sponsorable_login = sponsorable.login
    @sponsorable_legal_name = sponsorable.sponsors_membership&.legal_name
    @start_date = DateTime.parse(start_date)
    @end_date = DateTime.parse(end_date)
    @tax_id = tax_id

    raise ArgumentError.new("Sponsorable must have a Stripe account") unless @stripe_account.present?
    raise ArgumentError.new("Start date must be smaller than end date") if @start_date > @end_date
  end

  def as_pdf
    PdfRenderer.new(self).render
  end

  def pdf_filename
    "sponsors-#{sponsorable_login}-receipt-#{Date.current}.pdf"
  end

  # The total transfers and match amount minus reversals
  def total_payout
    return @total_payout if defined?(@total_payout)
    @total_payout = total_transfer - total_transfer_reversed
  end

  # The total amount transfered to the sponsorable's Stripe account
  def total_transfer
    return @total_transfer if defined?(@total_transfer)

    @total_transfer = to_money \
      ledger_entries
        .where(transaction_type: :transfer)
        .sum(:amount_in_subunits)
        .abs
  end

  # The total amount of transfers reversed
  # happens, for example, because a refund was issued to the sponsor
  def total_transfer_reversed
    return @total_transfer_reversed if defined?(@total_transfer_reversed)

    @total_transfer_reversed = to_money \
      ledger_entries
        .where(transaction_type: :transfer_reversal)
        .sum(:amount_in_subunits)
        .abs
  end

  # The total amount of sponsorship (not including match)
  def total_sponsorship
    return @total_sponsorship if defined?(@total_sponsorship)
    @total_sponsorship = total_transfer - total_match
  end

  # The total amount of sponsorship reversed
  # happens, for example, because a refund was issued to the sponsor
  def total_sponsorship_reversed
    return @total_sponsorship_reversed if defined?(@total_sponsorship_reversed)
    @total_sponsorship_reversed = total_transfer_reversed - total_match_reversed
  end

  # The total amount that GitHub matched
  def total_match
    return @total_match if defined?(@total_match)

    @total_match = to_money \
      ledger_entries
        .where(transaction_type: :github_match)
        .sum(:amount_in_subunits)
        .abs
  end

  # The total amount of GitHub match reversed
  # happens, for example, because a refund was issued to the sponsor
  def total_match_reversed
    return @total_match_reversed if defined?(@total_match_reversed)

    @total_match_reversed = to_money \
      ledger_entries
        .where(transaction_type: :github_match_reversal)
        .sum(:amount_in_subunits)
        .abs
  end

  private

  # All the ledger entries for the given timeframe
  def ledger_entries
    @ledger_entries ||= stripe_account
      .ledger_entries
      .where(transaction_timestamp: start_date..end_date)
  end

  def currency
    @currency ||= ledger_entries.pluck(:currency_code).first
  end

  def to_money(value)
    ::Billing::Money.new(value, currency)
  end
end
