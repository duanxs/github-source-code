# frozen_string_literal: true

class UserHovercard
  class GlobalSubject
    include UserHovercard::SubjectDefinition

    define_user_hovercard_context :organizations, ->(user, viewer, descendant_subjects:) do
      # Get a list of all organizations the viewer can see that the user belongs
      # to and bail if there are none we can show
      visible_organizations = user.organizations_visible_to(viewer).filter_spam_for(viewer)
      next if visible_organizations.empty?

      # Figure out what organization we'll be highlighting, and bail if there
      # are none and we're displaying a non-global subject
      related_organizations = descendant_subjects.select { |s| s.is_a?(Organization) }
      related_organizations = visible_organizations.where(id: related_organizations.map(&:id))
      next if related_organizations.empty? && descendant_subjects.any?

      # Make sure the viewer can see these organizations and show the list
      UserHovercard::Contexts::Organizations.new(related: related_organizations, all: visible_organizations, user: user)
    end

    define_user_hovercard_context :new_user, ->(user, viewer, descendant_subjects:) do
      if user.joined_in_last_month?
        Hovercard::Contexts::Custom.new("Joined GitHub this month", "rocket")
      end
    end
  end
end
