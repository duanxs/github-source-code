# frozen_string_literal: true

module UserHovercard::Contexts
  class OrganizationTeams < Hovercard::Contexts::Base
    attr_reader :organization, :related, :user

    def initialize(user:, related:, all:, organization:)
      @user = user
      @related = related
      @all = all
      @organization = organization
    end

    def total_team_count
      all.count
    end

    def message
      team_text = hovercard_sentence(highlighted, max: 3, total: all.count) do |team|
        "@#{team.combined_slug}"
      end

      "Member of #{team_text}"
    end

    def highlighted
      if related.any?
        related
      else
        Team.ranked_for(user, scope: all)
      end
    end

    def octicon
      "jersey"
    end

    def platform_type_name
      "OrganizationTeamsHovercardContext"
    end

    def more_teams_query_value
      "@#{user}"
    end

    private

    attr_reader :all
  end
end
