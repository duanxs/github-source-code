# rubocop:disable Style/FrozenStringLiteralComment

class Issue < ApplicationRecord::Domain::Repositories
  include GitHub::UTF8
  include Issue::State
  include GitHub::UserContent
  include GitHub::Validations
  include GitHub::RateLimitedCreation
  include GitHub::CommentMetrics
  include GitHub::Relay::GlobalIdentification
  include NotificationsContent::WithCallbacks
  include PushNotifiable
  include IssueTimeline
  include Referenceable, Referrer
  include Reaction::Subject::RepositoryContext
  include Conversation::Subject
  include Spam::Spammable
  extend GitHub::BackgroundDependentDeletes
  include UserContentEditable
  include InteractionBanValidation
  include AuthorAssociable
  include ReferenceableContent
  include Blockable
  include AbuseReportable
  include MemexProjectItem::Content

  include Issue::AbilityDependency
  include Issue::PermissionsDependency
  include Issue::AssignmentDependency
  include Issue::ProjectsDependency
  include Issue::DirtyDependency
  include Issue::HovercardDependency
  include Issue::PinDependency
  include Issue::CloseIssueReferenceDependency
  include Issue::DiscussionsDependency
  include Issue::MemexesDependency

  include Permissions::Attributes::Wrapper
  self.permissions_wrapper_class = Permissions::Attributes::Issue

  # Remove this when we upgrade to Zeitwerk (necessary because ::Builder exists)
  require_dependency "issue/builder"

  # Use timezone aware handling for created_at / updated_at
  include GitHub::TimezoneTimestamp
  timezone_timestamp :contributed_at

  # Has this issue been read by the current user?
  # Gets prefilled by Issue::SearchResult
  attr_accessor :read_by_current_user

  # Set by remove_noncollab_assignees, which saves the record.
  # Prevents an infinite loop.
  attr_accessor :skip_noncollab_assignee_callback

  # Set by edit-based API endpoints when the request was initiated by an
  # Integration on behalf of the modifying_user.
  attr_accessor :modifying_integration

  # A file under .github/ISSUE_TEMPLATES to read body content from
  attr_accessor :body_template_name

  # Is this issue being created as part of a transfer?
  attr_accessor :transfer

  # Set this if the event destroying this issue is not due to a :deletion action
  attr_accessor :deletion_hook_action

  # How many participants does the issue have?
  attr_accessor :participant_count

  # Use VARBINARY limit from the database
  TITLE_BYTESIZE_LIMIT = 1024
  LABEL_LIMIT = 100

  URI_TEMPLATE = Addressable::Template.new("/{owner}/{name}/{type}/{number}{#anchor*}").freeze

  HELP_WANTED_LABEL_NAMES = [
    Label::HELP_WANTED_NAME,
    Label::GOOD_FIRST_ISSUE_NAME,
  ].freeze

  LOCK_REASONS = ["off-topic", "too heated", "resolved", "spam"].freeze

  validates_presence_of :user_id, on: :create
  validate :ensure_creator_is_not_blocked, on: :create
  validates_presence_of :repository_id, :title
  validates_presence_of :contributed_at, on: :create
  validates :body, bytesize: { maximum: MYSQL_UNICODE_BLOB_LIMIT },
    unicode: true, allow_blank: true, allow_nil: true
  validates :title, bytesize: { maximum: TITLE_BYTESIZE_LIMIT },
    unicode: true
  validate :user_can_interact?, on: :create
  validate :ensure_user_can_create_pr, on: :create, if: :pull_request?
  validates :labels, length: { maximum: LABEL_LIMIT, message: "can have a maximum of %{count} labels" }
  validate :pull_request_id_is_not_zero

  belongs_to :repository
  belongs_to :user

  belongs_to :performed_via_integration, foreign_key: :performed_by_integration_id,
                                         class_name: "Integration"

  setup_spammable(:user)

  alias_method :entity, :repository
  alias_method :async_entity, :async_repository

  has_one :pinned_issue, dependent: :destroy

  has_many :assignments, inverse_of: :issue do
    def excluding_ids(these_assignments)
      these_assignments.any? ? where("id NOT IN (?)", these_assignments) : scoped
    end
  end
  destroy_dependents_in_background :assignments
  has_many :assignees, through: :assignments, split: true, source: :assignee
  has_many :comments, -> { order("issue_comments.id ASC").limit(COMMENT_LIMIT) }, class_name: "IssueComment"
  destroy_dependents_in_background :comments
  has_many :commenters, through: :comments, source: :user, split: true
  has_many :events, -> { order("issue_events.id ASC") }, class_name: "IssueEvent"
  destroy_dependents_in_background :events
  has_many :issue_priorities
  destroy_dependents_in_background :issue_priorities
  has_and_belongs_to_many :labels, -> { order "name" }

  has_many :composable_comments, -> { order("composable_comments.id ASC") }
  destroy_dependents_in_background :composable_comments

  has_many :marked_duplicate_issues, dependent: :delete_all,
    class_name: "DuplicateIssue", foreign_key: "canonical_issue_id"
  has_many :marked_canonical_issues, dependent: :delete_all,
    class_name: "DuplicateIssue"

  has_many :reactions, as: :subject

  belongs_to :assignee, foreign_key: :assignee_id, class_name: "User"

  validate :ensure_assignee_is_a_collaborator, on: :create
  validate :ensure_valid_milestone
  validate :ensure_authorized_to_create_content, if: :title_or_body_changed?
  validate :ensure_not_converted_to_discussion

  belongs_to :pull_request, dependent: :destroy, inverse_of: :issue
  after_save :sync_pull_request_updated_at
  after_touch :sync_pull_request_updated_at
  has_many :issue_blob_references
  after_save :save_issue_blob_references
  belongs_to :milestone, inverse_of: :issues
  has_many :workflow_runs, as: :trigger

  has_many :memex_project_items, -> { where(content_type: "Issue") },
    class_name: "MemexProjectItem",
    foreign_key: :content_id
  destroy_dependents_in_background :memex_project_items

  after_commit :update_close_issue_references, on: [:create, :update], if: :pull_request?

  before_validation :set_state
  before_validation :set_contributed_at, on: :create
  after_create :create_initial_events
  after_create :subscribe_author
  after_create :clear_contributions_cache
  after_create :enqueue_community_health_check
  after_create :set_first_contribution_flag, if: :pull_request?
  after_save :prioritize_in_milestone, if: :saved_change_to_milestone_id?
  after_save :trigger_milestone_change_events, if: :saved_change_to_milestone_id?
  after_save :create_renamed_event,      if: :saved_change_to_title?
  after_save :update_milestone_counts!,  if: :milestone_or_state_changed?
  after_destroy :update_milestone_counts!
  after_commit :instrument_update_event, on: :update, if: :title_or_body_changed?

  after_commit :enqueue_remove_noncollab_assignees, if: proc {
    !skip_noncollab_assignee_callback && assigned?
  }

  after_commit :notify_socket_subscribers, on: :update
  after_commit :instrument_creation_event, on: :create

  # spam check on create handled by Newsies::DeliverNotificationsJob
  after_commit :enqueue_check_for_spam, on: :update, if: :check_for_spam?

  after_commit :synchronize_search_index

  after_commit :measure_comment_metrics, on: :create

  after_commit :process_content_references, on: [:create, :update], if: :has_content_references?

  after_commit :subscribe_and_notify,            on: :create
  after_commit :update_subscriptions_and_notify, on: :update

  before_destroy :generate_webhook_payload, unless: :spammy?
  after_commit :instrument_destruction, on: :destroy, unless: :spammy?

  # NOTE: This callback is not a transaction style callback due to the
  # repository.owner property being nil in that case. The cause is most likely
  # related to the user destruction callback chain.
  after_destroy :instrument_destroy # rubocop:disable GitHub/AfterCommitCallbackInstrumentation

  setup_attachments
  setup_referrer

  scope :grouped_by_repo,    -> { group("issues.repository_id") }
  scope :public_scope,       -> { joins(:repository).where("repositories.public = ?", true) }
  scope :open_issues,        -> { where(state: "open").order("issues.id ASC") }
  scope :closed_issues,      -> { where(state: "closed").order("issues.created_at DESC") }
  scope :fresh,              -> (threshold: 1.month.ago) {
    where("issues.updated_at >= ?", threshold)
  }
  scope :stale,              -> (threshold: 1.month.ago) {
    where("issues.updated_at < ?", threshold)
  }
  scope :with_no_milestone,  -> { where(milestone_id: nil) }
  scope :with_any_milestone, -> { where("issues.milestone_id IS NOT NULL") }
  scope :assigned_to, lambda { |login|
    assignee = case login
      when String, Symbol
        found = User.find_by_login(login.to_s)
        raise AssigneeInvalid.new(login.to_s, "Could not find assignee: %p" % login.to_s) unless found
        found
      when User then login
      else
        raise AssigneeInvalid.new(login, "Assignee must be a User or login handle; got %p" % login)
    end

    # We use raw SQL for this join instead of `joins(:assignments)` because
    # `joins(:assignments)` tries to grab fields from the assignments table. We
    # don't want it to do this, for two reasons:
    #
    # 1. We don't actually need any of these fields, so this avoids bloating
    #    the result.
    # 2. When we do try to grab these fields, MySQL uses the wrong index
    #    (`index_issues_on_pull_request_id`) and the query takes forever.
    #
    # See https://github.com/github/github/pull/54620 for more details.
    scope = joins("INNER JOIN `assignments` ON `assignments`.`issue_id` = `issues`.`id`")

    # We need this because Rails marks records as readonly if they come from a
    # query with a raw SQL join.
    scope = scope.readonly(false)

    scope = scope.where(assignments: {
      assignee_id: assignee.id,
    })

    scope
  }
  scope :unassigned, -> {
    joins("LEFT OUTER JOIN `assignments` ON `assignments`.`issue_id` = `issues`.`id`").
      where("assignments.issue_id IS NULL")
  }

  scope :assigned, -> { where("assignee_id IS NOT NULL") }
  scope :created_by, lambda { |login|
    creator = (user = User.find_by_login(login) and user.id) or nil

    for_user(creator)
  }
  scope :mentioning, lambda { |user|
    joins(:events).where(["issue_events.event = 'mentioned' AND issue_events.actor_id = ?", user.id]).distinct
  }
  scope :from_ids, lambda { |ids| where(id: ids) }
  scope :participating, lambda { |user|
    joins(:comments).where(["issue_comments.user_id = ? OR issues.user_id = ?", user.id, user.id]).distinct
  }

  # Scope issues by a Label id (or array of them), case-insensitive Label name (or array of them) or
  # a Label class instance.
  scope :labeled, ->(label_or_collection) do
    case label_or_collection
    when Integer
      joins(:labels).where("issues_labels.label_id = ?", label_or_collection)
    when Array
      scope = joins(:labels)

      scope = case label_or_collection.first
      when Integer, Label
        scope.where("issues_labels.label_id IN (?)", label_or_collection)
      when String
        scope.merge(Label.with_name(label_or_collection)).where("labels.repository_id = issues.repository_id")
      else
        scope
      end

      # we need to do this funky HAVING clause because we are approximating ANDing the labels together
      # and a simple `labels.name` IN (?) only handles an OR.
      scope.group("issues.id").having("COUNT(issues.id) = #{label_or_collection.size}")
    when String
      joins(:labels).merge(Label.with_name(label_or_collection)).where("labels.repository_id = issues.repository_id")
    when Label
      joins(:labels).where("issues_labels.label_id = ?", label_or_collection.id)
    end
  end

  scope :sorted_by, lambda { |order, direction = "desc"|
    scope = self

    order =
      case order.to_s
      when "created" then "issues.created_at"
      when "updated" then "issues.updated_at"
      when "comments" then "issues.issue_comments_count"
      when "repo"
        scope = scope.includes(repository: :owner)
        "CONCAT(users.login, repositories.name) ASC, issues.created_at"
      else "issues.created_at"
      end

    dir = (direction.to_s == "asc" ? "ASC" : "DESC")
    scope.order(Arel.sql("#{order} #{dir}"))
  }

  scope :excluding_states, lambda { |excluding|
    # By default, we show only open pull requests on non-ajax requests
    if excluding.to_s.split(",").any?
      exclude = excluding.to_s.split(",").compact
    else
      exclude = ["closed"]
    end

    scope = self.includes(:issue).references(:issue)
    scope = scope.where("issues.state <> 'open'") if exclude.include?("open")
    scope = scope.where("issues.state <> 'closed'") if exclude.include?("closed")
    scope
  }

  scope :for_milestone, lambda { |milestone|
    where(milestone_id: milestone)
  }

  scope :for_repository_ids, lambda { |ids|
    where("issues.repository_id IN (?)", ids)
  }

  scope :for_repositories, lambda { |repo_scope|
    joins(:repository).merge(repo_scope)
  }

  scope :excluding_repository_ids, lambda { |ids|
    where("issues.repository_id NOT IN (?)", ids)
  }

  scope :excluding_organization_ids, lambda { |ids|
    if ids.any?
      joins(:repository).
      where("repositories.organization_id NOT IN (?) OR repositories.organization_id IS NULL", ids)
    else
      scoped
    end
  }

  scope :for_repository, lambda { |repository|
    where("issues.repository_id = ?", repository)
  }

  scope :for_user, lambda { |user|
    where(user_id: user)
  }

  scope :for_organization, lambda { |organization|
    joins(:repository).where("repositories.organization_id = ?", organization)
  }

  scope :since, lambda { |time|
    where("issues.updated_at >= ?", time)
  }

  scope :without_pull_requests, -> { where(pull_request_id: nil) }
  scope :with_pull_requests,    -> { where.not(pull_request_id: nil) }

  # For repositories that have issues disabled, only include issues associated to
  # a Pull Request. (For repositories with issues enabled, include all issues.)
  scope :excluding_issues_in_repos_with_issues_disabled, -> {
    joins(:repository).where("repositories.has_issues = 1 OR issues.pull_request_id IS NOT NULL")
  }

  # hard limit on the number of comments that can be added to an issue
  # beyond this value IssueComments will fail validation when they are created
  COMMENT_LIMIT = 2500

  # maximum numbers of Users to load for display in the Participants section in the sidebar
  PARTICIPANT_LOAD_LIMIT = 50

  SUGGESTION_LIMIT = 1000
  SORT_FIELDS      = %w[created updated comments].freeze
  SORT_DIRECTIONS  = %w[desc asc].freeze

  scope :suggestions, -> do
    select("id, number, title, pull_request_id, updated_at").
      order("updated_at desc").
      limit(SUGGESTION_LIMIT)
  end

  def self.count_by_repo(issues)
    repo_counts = {}
    repo_count_issue_ids = issues.pluck(:id)
    issues.grouped_by_repo.count({
      group: "issues.repository_id",
      conditions: ["issues.id IN(?)", repo_count_issue_ids],
    }).each do |count|
      repo_counts[count[0].first] = count[1]
    end

    repo_counts
  end

  def title=(value)
    self[:title] = value.to_s if value
  end

  # If there is an associated pull request, it needs to have the same timestamp.
  #
  # The pull request may have already been updated, so verify that the timestamp
  # is out of date before updating to avoid a redundant query (see #57779).
  def sync_pull_request_updated_at
    return if pull_request.nil? || pull_request.frozen? || pull_request.new_record?

    if pull_request.updated_at.to_i < updated_at.to_i
      pull_request.update_column(:updated_at, updated_at)
    end
  end

  extend GitHub::Encoding
  force_utf8_encoding :title, :body

  def template
    @template ||= repository.preferred_issue_templates[body_template_name]
  end

  # Deprecated. Use Issue#template
  def raw_body_template
    return if repository.blank?
    return @body_template if defined?(@body_template)
    return @body_template = repository.preferred_issue_template&.data unless body_template_name.present?

    local_file = PreferredFile.find(
      directory: repository.root_directory,
      type: :issue_template,
      nested_filename: body_template_name,
    )&.data

    if !local_file.present? && repository.owner.organization? && !repository.global_health_files_repository?
      global_repo = Platform::Loaders::GlobalHealthFilesRepository.load(repository.owner_id).sync
      return @body_template = nil unless global_repo.present?

      @body_template = PreferredFile.find(
        directory: global_repo.root_directory,
        type: :issue_template,
        nested_filename: body_template_name,
      )&.data
    else
      @body_template = local_file
    end
  end

  # Public: Find the user supplied template to use in the body of
  # new issues if one exists.
  #
  # Read from a file under .github/ISSUE_TEMPLATES or ISSUE_TEMPLATE.md.
  #
  # Returns a String from the template if one is found.
  # Returns nil if no template is found.
  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def body_template
    @body_template ||= begin
      body = template&.body || raw_body_template
      body&.gsub(GitHub::Goomba::YamlFilter::FRONTMATTER_REGEX, "")
    end
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  # Deprecated. Use Issue#template
  def has_body_template?
    !!body_template
  end

  def async_body_text
    Promise.all([
      async_repository,
      async_latest_user_content_edit,
      async_user,
    ]).then do |repository, latest_user_content_edit, _user|
      Promise.all([
        (latest_user_content_edit ? latest_user_content_edit.async_editor : nil),
        repository.async_owner,
      ]).then do
        super
      end
    end
  end

  # Fallback on the ghost user when the original author's been deleted.
  # See User.ghost for more.
  def safe_user
    user || User.ghost
  end

  # The user who performed the action as set in the GitHub request context. If the context doesn't
  # contain an actor, fallback to the safe_user which we know exists.
  def modifying_user
    @modifying_user ||= (User.find_by_id(GitHub.context[:actor_id]) || safe_user)
  end

  # Internal: For suggestions_url helper
  def suggestion_id
    id
  end

  # Public: Find all Users that have commented on this issue.
  # Exclude Bots and users who no longer have access to this repo
  # (unless parent org has so many members that query may timeout).
  # Exclude Organizations from results as participants should be Users, but
  # Users can transform into Organizations.
  #
  # Note that the returned users are not filtered for spam.
  #
  # optimize_repo_access_checks - by default, #user_ids_to_hide_from_mentions will
  #                               check to make sure that each user returned still
  #                               has access to the repo. If optimize_repo_access_checks
  #                               is set to true, that check will not happen if the
  #                               parent org has too many members (which may
  #                               cause the access checks to time out)
  # user_limit - limits the number of Users to load when set. If participants has already been
  #               memo-ized, the limit will not be applied
  #
  # Returns an Array of Users.
  def participants(optimize_repo_access_checks: false, user_limit: nil)
    return pull_request.participants(optimize_repo_access_checks: optimize_repo_access_checks) if pull_request?

    @participants ||= begin
      user_ids = [self.user_id, comment_and_event_participant_ids].flatten.compact.uniq
      user_ids_to_hide = repository.user_ids_to_hide_from_mentions(user_ids, optimize_repo_access_checks: optimize_repo_access_checks)
      participant_ids = user_ids - user_ids_to_hide

      user_ids_to_query = user_limit.to_i > 0 ? participant_ids.take(user_limit.to_i) : participant_ids
      users = User.where(id: user_ids_to_query).to_a

      participants = users.reject { |user| user.is_a?(Bot) || user.organization? }
      rejected_user_count = users.size - participants.size
      self.participant_count = participant_ids.size - rejected_user_count

      GitHub::PrefillAssociations.for_profiles(participants)
      participants
    end
  end

  # Internal: Find all the actors that have participated in this Issue through
  # the events association
  #
  # Returns an array of actors
  def get_event_participants
    User.where(id: event_participant_ids)
  end

  # Internal: Find all the actors that have participated in this Issue through
  # the events association
  #
  # Returns an array of actor_ids
  def event_participant_ids
    # The `unscoped` call is being used on the IssueEvent model to remove the include from that model's default scope. Calling
    # `unscoped` on the 'events' association also removes the issue scoping, which is why the IssueEvent model is used directly here.
    IssueEvent.unscoped.where(issue: self).participatory.distinct.pluck(:actor_id)
  end

  # Internal: Compiles the comment and event participant IDs
  #
  # Returns an array of User objects
  def comment_and_event_participant_ids
    @comment_and_event_participant_ids ||= begin
      commenter_ids = comments.reorder(nil).pluck(:user_id)
      comment_and_event_participant_ids = commenter_ids + event_participant_ids
      comment_and_event_participant_ids
    end
  end

  # Users who should be considered issue participants.
  #
  # viewer - the User who is viewing the participants (current_user)
  # optimize_repo_access_checks - if optimize_repo_access_checks is set
  #                               to true, do not perform access checks
  #                               on repos that are owned by org's with
  #                               a lot of members
  # user_limit - limits the number of Users to load when set. If participants has already been
  #               memo-ized, the limit will not be applied
  #
  # Returns an Array of Users.
  def participants_for(viewer, optimize_repo_access_checks: false, user_limit: nil)
    participants(optimize_repo_access_checks: optimize_repo_access_checks, user_limit: user_limit).reject { |u| u.hide_from_user?(viewer) }
  end

  # Public: Returns an Array of timeline items
  # See also included behavior from IssueTimeline module
  #
  # viewer - the User that is viewing the timeline (current_user)
  #
  # Note: if you add new types into the items being returned here, please notify
  # @github/ecosystem-api in your PR so that we can help you implement the changes in
  # the GraphQL schema.
  def timeline_items_for(viewer, all_valid_events: false)
    GitHub.dogstats.time("issue.time", tags: ["action:timeline_items_for"]) do
      self.comments.reject { |c| c.hide_from_user?(viewer) } +
        visible_events_for(viewer, all_valid_events: all_valid_events) +
        visible_references_for(viewer) +
        self.composable_comments
    end
  end

  # Public: Returns an Array of timeline items with any extra
  #         markers inserted.
  # See also included behavior from IssueTimeline module
  #
  # viewer - the User that is viewing the timeline (current_user)
  # items - list of timeline items before the markers are inserted
  def timeline_insert_markers(viewer, items)
    items
  end

  # Public: Retrieve the timeline for the given viewer
  #
  # TODO: Rename to `timeline_for` once the IssueTimeline module is removed.
  #
  # viewer         - A User that is viewing the timeline
  # filter_options - Hash of options passed to the timeline instance
  #
  # Returns Timeline::IssueTimeline
  def timeline_model_for(viewer, filter_options = {})
    return @timeline_model_for[[viewer, filter_options]] if defined?(@timeline_model_for)

    @timeline_model_for = Hash.new do |hash, (viewer, filter_options)|
      hash[[viewer, filter_options]] = Timeline::IssueTimeline.new(
        self, viewer: viewer, filter_options: filter_options
      )
    end
    @timeline_model_for[[viewer, filter_options]]
  end

  # This is a pre-check to determine whether to load the scoped timeline
  # or query the full issue timeline.
  # NOTE: This does not perform visible_for checks on the events returned. Those
  # will be performed by the timeline loader.
  #
  # This should only be used behind the :scoped_issue_timeline feature flag
  def has_scoped_timeline_items?
    comments.any? || references.any? || events.connects.any?
  end

  def show_issue_deletion_opt_in_link?(user)
    return false unless repository.owner.organization?
    return false if repository.owner.members_can_delete_issues?
    return false unless repository.owner.adminable_by?(user)
    true
  end

  def transferrable_by?(user)
    !pull_request? && repository.writable? && repository.resources.contents.writable_by?(user) && !locked?
  end

  def async_possible_transfer_repositories(viewer:, query: nil)
    async_repository.then do |repository|
      target_repo_scope = Repository.where(owner_id: repository.owner_id).
        filter_spam_and_disabled_for(viewer).
        with_issues_enabled.
        not_archived_scope

      if repository.private?
        target_repo_scope = target_repo_scope.private_scope
      end

      target_repo_scope = target_repo_scope.search(query) if query.present?

      target_repo_scope_ids = target_repo_scope.ids - [repository_id]
      repository_ids = viewer.associated_repository_ids(min_action: :write, repository_ids: target_repo_scope_ids)

      Repository.where(id: repository_ids).order(updated_at: :desc)
    end
  end

  # Determine whether user has commented on this issue.
  #
  # user - A User object.
  #
  # Returns true when user has left at least one comment.
  def commented_on_by?(user)
    commenters.include?(user)
  end

  # Public returns a promise that is resolved with a boolean indicating if a user has commented
  # on this issue
  #
  # Note: This is different then the normal #commented_on_by? by method in that it uses a limit
  # query for efficiency instead of querying all commenters through the association:
  # `IssueComment.select(:user_id).where(issue: self, user_id: user_id).limit(1).exists?`
  #
  # user - User instance
  #
  # Returns a Promise
  def async_commented_on_by?(user)
    Platform::Loaders::HasCommented.load(self, user.id)
  end

  def generate_webhook_payload
    if modifying_user&.spammy?
      @delivery_system = nil
      return
    end

    action = deletion_hook_action || :deleted
    event = Hook::Event::IssuesEvent.new issue_id: id, actor_id: modifying_user.id, action: action
    @delivery_system = Hook::DeliverySystem.new(event)
    @delivery_system.generate_hookshot_payloads
  end

  def instrument_destruction
    unless defined?(@delivery_system)
      raise "`generate_webhook_payload` must be called before `instrument_destruction`"
    end

    @delivery_system&.deliver_later
  end

  # Handle IssueEvents for adding / removing labels for a block
  #
  # Returns the result of the block
  def trigger_change_labels
    old_labels = self.labels.to_a.dup

    result = yield

    labeled   = self.labels - old_labels
    unlabeled = old_labels - self.labels

    labeled.each { |l| trigger_label_event(l) }
    unlabeled.each { |l| trigger_unlabel_event(l) }

    self.touch if labeled.any? || unlabeled.any?

    update_repo_community_profile(changed_labels: labeled + unlabeled)

    result
  end

  # Add labels to an issue, updates the search index and updated_at timestamp
  #
  # labels - an Array of Label objects
  def add_labels(labels)
    labels = [labels] if !labels.respond_to?(:each)

    GitHub::SchemaDomain.allowing_cross_domain_transactions do
      Issue.transaction do
        labels.each do |label|
          begin
            next if self.labels.include?(label)

            trigger_label_event(label)
            self.labels << label
          rescue ActiveRecord::RecordNotUnique
            # long hair, don't care
          end
        end

        self.touch
      end
    end

    notify_socket_subscribers
    update_repo_community_profile(changed_labels: labels)
    synchronize_search_index
  end

  # Add labels from issues by id.
  #
  # ids - an Array of Label ids
  def add_label_ids(ids)
    labels = repository.labels.where("labels.id IN (?)", ids).to_a
    add_labels(labels)
  end

  # Remove labels from an issue, updates the search index
  # and updated_at timestamp
  #
  # labels - an Array of Label objects
  def delete_labels(labels)
    # only delete the label and trigger events if the label is associated with the issue
    labels = self.labels.where(id: Array(labels).map(&:id))
    return self if labels.empty?

    GitHub::SchemaDomain.allowing_cross_domain_transactions do
      Issue.transaction do
        labels.each do |label|
          trigger_unlabel_event(label)
        end
        self.labels.delete(labels)
        self.touch
      end
    end

    notify_socket_subscribers
    update_repo_community_profile(changed_labels: labels)
    synchronize_search_index
  end

  # Remove labels from issues by id.
  #
  # ids - an Array of Label ids
  def delete_label_ids(ids)
    labels = repository.labels.where("labels.id IN (?)", ids).to_a
    delete_labels(labels)
  end

  # Remove all labels from an issue, updates the search index
  # and updated_at timestamp
  def clear_labels
    labels_to_clear = self.labels

    GitHub::SchemaDomain.allowing_cross_domain_transactions do
      Issue.transaction do
        labels_to_clear.each { |label| trigger_unlabel_event(label) }
        labels_to_clear.clear
        self.touch
      end
    end

    notify_socket_subscribers
    update_repo_community_profile(changed_labels: labels_to_clear)
    synchronize_search_index
  end

  # Public: Replace all labels on an issue, updates the search index
  # and updated_at timestamp
  #
  # new_labels: an Array of Label objects
  #
  # Returns a Boolean.
  def replace_labels(new_labels)
    replacement_successful = true
    old_labels = self.labels
    new_labels = Array(new_labels)

    GitHub::SchemaDomain.allowing_cross_domain_transactions do
      Issue.transaction do
        (old_labels - new_labels).each do |label|
          trigger_unlabel_event label
        end

        (new_labels - old_labels).each do |label|
          trigger_label_event label
        end

        github_sql.run <<-SQL, issue_id: self.id
          DELETE FROM `issues_labels` WHERE `issue_id` = :issue_id
        SQL

        if new_labels.any?
          values = new_labels.map do |label|
            [self.id, label.id]
          end

          github_sql.run <<-SQL, values: GitHub::SQL::ROWS(values)
            INSERT INTO `issues_labels` (`issue_id`, `label_id`)
            VALUES :values
          SQL
        end

        association(:labels).target = new_labels
        self.touch

        # Because labels are inserted with raw SQL without validations,
        # we need to check to see if the Issue is still valid and
        # rollback if it isn't.
        unless self.valid?
          replacement_successful = false
          raise ActiveRecord::Rollback
        end
      end
    end

    return false unless replacement_successful

    notify_socket_subscribers
    synchronize_search_index
    update_repo_community_profile(changed_labels: new_labels + old_labels)

    true
  end

  def pull_request?
    !pull_request.nil?
  end

  # Public: Ensures API responses stay fresh. The last_modified_at values are
  # used to calculate the Last-Modifed and ETag values for API responses.
  #
  # It's important that it returns the most recent timestamp of all the
  # associated objects, because the JSON representation of an issue includes
  # those nested objects in the response. If any of them change, the entire
  # JSON representation of the issue changes, and its ETag and Last-Modified
  # values need to reflect that.
  def last_modified_at
    @last_modified_at ||= last_modified_with(:user, :assignees, :pull_request,
      :milestone)
  end

  # Check if this issue has already been closed by the specified commit.
  def already_closed_by_commit?(commit_oid)
    @close_commit_oids ||= Set.new(events.where(event: "closed").pluck(:commit_id).compact)
    @close_commit_oids.include?(commit_oid)
  end

  # Add an issue event to note that the issue was referenced from a commit.
  #
  #   user       - User who referenced the issue
  #   commit_id  - commit SHA1 that referenced this issue
  #   repository - Repository that owns the commit (for cross repository
  #                references only).
  #
  # Returns the new IssueEvent record when successful, nil otherwise.
  def reference_from_commit(user, commit_id, repository = nil)
    return if events.where(commit_id: commit_id).first

    ignore_duplicate_records do
      event = events.create! \
        event: "referenced",
        actor: user,
        commit_id: commit_id,
        commit_repository: repository

      # only subscribe if the commit came from the same repository
      if repository && repository.id == self.repository_id
        subscribe(user, :comment)
      end

      event
    end
  end

  def subscribe(user, action = nil)
    if ["team-mentioned", "mentioned", :team_mention, :mention, :manual].include?(action)
       subscribe_with_event(user)
    end

    super user, action
  end

  def subscribe_all(users, action = nil)
    if ["team-mentioned", "mentioned", :team_mention, :mention, :manual].include?(action)
      subscribe_all_with_event(users)
    end

    super users, action
  end

  # Public: Subscribes a User to receive notifications from this Issue.
  #
  # user   - The User that receives the notifications.
  # action - The optional String IssueEvent action.
  #
  # Returns true if the subscription is successful.
  def subscribe_with_event(user)
    return unless subscribable_by?(user)
    ignore_duplicate_records do
      events.create(actor: user, event: "subscribed")
      true
    end
  end

  # Public: Subscribes an array of users to receive notifications from this Issue.
  #
  # user   - The array of users that receive the notifications.
  #
  def subscribe_all_with_event(users)
    users.each_slice(GitHub.subscribed_users_batch_size).each do |batched_users|
      CreateSubscribedIssueEventsJob.perform_later(self.id, batched_users.map(&:id))
    end
  end

  # Public: Unsubscribes a User from receiving any more notifications for this
  # Issue.
  #
  # user - The User that won't receive any more notifications.
  #
  # Returns true if the unsubscribe event is successful.
  def unsubscribe(user)
    super user
    ignore_duplicate_records do
      events.create(actor: user, event: "unsubscribed")
      true
    end
  end

  # Public: Keeps a user from seeing any more notifications for this thread.
  #
  # user - The User that is ignoring this thread's future notifications.
  #
  # Returns true.
  def ignore(user)
    super user
    return unless subscribed?(user)
    events.create(actor: user, event: "unsubscribed")
    true
  end

  def notifications_author
    user
  end

  def notifications_thread
    self
  end

  def async_notifications_list
    async_repository
  end

  # Subscribe a list of mentioned users to this issue. This also creates
  # mentioned events.
  #
  # mentions - Array of users that were mentioned. This defaults to users
  #            mentioned in the issue body but a custom array may be passed
  #            from associated comment models and whatnot too.
  # author   - Author of the content being posted.  Defaults to #user.
  #
  # Returns nil.
  def subscribe_mentioned(mentions = mentioned_users, author = user)
    return if author && author.spammy?
    subscribable_user_mentions(mentions, author) do |mentionee, author|
      unless author && author.blocked_by?(mentionee)
        events.create!(event: "mentioned", actor: mentionee)
        subscribe(mentionee, :mention)
      end
    end
  end

  # Internal: Filters out users that should keep a subscription to this thread.
  # This should be called after a comment has been edited, with a mentioned
  # user removed due to a typo.  Remove anyone that hasn't commented already.
  #
  # users - Array of Users.
  #
  # Returns an Array of User that can be unsubscribed.
  def unsubscribable_users(users)
    commenters = Set.new
    collect_commented_users commenters, comments
    if pull = (pull_request? && pull_request)
      collect_commented_users commenters, pull.review_comments
    end
    commenters.add(user_id).merge(assignments.map(&:assignee_id))

    users.reject { |user| commenters.include?(user.id) }
  end

  def collect_commented_users(set, scope)
    comments.select("distinct user_id").each do |comment|
      set << comment.user_id
    end
  end

  # Internal: Notify subscribers that the issue has been updated.
  #
  # Returns Set of channel id Strings that were notified.
  def notify_socket_subscribers(notify_associated: true)
    # If we're being deleted, no reason to notify anyone
    return if !repository

    if pull_request?
      pull_request.notify_socket_subscribers
    else
      data = {
        timestamp: Time.now.to_i,
        wait: default_live_updates_wait,
        reason: "issue ##{id} updated",
        gid: global_relay_id,
      }

      channel = GitHub::WebSocket::Channels.issue(self)
      GitHub::WebSocket.notify_issue_channel(self, channel, data)
    end

    if prev_state_changes = previous_changes["state"]
      data = {
        timestamp: Time.now.to_i,
        wait: default_live_updates_wait,
        reason: "issue ##{id} state changed to #{state}",
        state_changes: prev_state_changes,
      }
      channel = GitHub::WebSocket::Channels.issue_state(self)
      GitHub::WebSocket.notify_issue_channel(self, channel, data)
    end

    if notify_associated
      if milestone
        milestone.notify_subscribers
      end

      cards.each(&:notify_subscribers)
    end
  end

  # Public: Syncs the thread's read state if the user has it open in a browser.
  #
  # user - The User that read the thread.
  #
  # Returns true if queued, false if not.
  def sync_thread_read_state(user)
    target = pull_request? ? pull_request : self
    channel = GitHub::WebSocket::Channels.marked_as_read(user)
    GitHub::WebSocket.notify_user_channel(user.id, channel, gid: target.global_relay_id, timestamp: Time.now.to_i, wait: default_live_updates_wait)
  end

  # Returns the timestamp of the latest change in the issue's thread,
  # usually either the issue's created_at or the created_at of the most
  # recently added comment.
  def latest_change
    if issue_comments_count > 0 && comments.last
      comments.last.created_at
    else
      created_at
    end
  end

  # open/close issues
  def modifiable_by?(user)
    user.is_a?(User) && repository.pushable_by?(user)
  end

  # Can the given actor lock the conversation on this Issue?
  def lockable_by?(actor)
    case actor
    when Bot, IntegrationInstallation
      if self.pull_request?
        repository.resources.pull_requests.writable_by?(actor)
      else
        repository.resources.issues.writable_by?(actor)
      end
    else
      repository.writable_by?(actor) || actor.site_admin?
    end
  end
  alias :unlockable_by? :lockable_by?

  def self.valid_lock_reason?(reason)
    LOCK_REASONS.include?(reason)
  end

  def active_lock_reason
    return unless locked?
    events.locks.last&.lock_reason
  end

  # This is a set of checks against the issue's parent repo, to determine
  # if it should be removed from the index. Since these are repo-level checks,
  # if the repo is found to be unsearchable, a delete of _all issues for that repo_
  # from the index will be triggered. Handle individual issue-shouldn't-be-searchable
  # events from inside prune_issue method directly, not here.
  #
  # Some reasons the parent repo might not be searchable:
  #   - repo not routed on the file servers
  #   - repo's user is a spammer
  #   - disabled by an admin
  #   - no associated issues
  #
  #   Note: some of the repository.repo_is_searchable? checks were not
  #   cargo culted into this check on issue's parent repo because GraphQL
  #   ISSUE type queries loading related models async are running into deadlock
  #   due to circular loading patterns. GraphQL folks said to punt like so for now.
  #
  # Return `true` if we should add the issue to the search index; return
  # `false` if we should not.
  #
  def parent_repo_is_searchable?
    # If the parent repo has no issues
    return false if !repository.has_issues?

    return false unless repository.active?

    # When the parent repo user is spammy
    return false if repository.spammy?

    # When the parent repo has been disabled for any reason
    return false if repository.disabled?

    # When the parent repo has been disabled for DMCA and similar reasons
    return false if repository.access.disabled?

    # The issue is safe to index
    true
  end

  def lock(user, reason = nil)
    return unless lockable_by?(user) && !locked?
    return if repository.locked_on_migration? || (!repository.has_issues? && !self.pull_request?)
    return if reason && !Issue.valid_lock_reason?(reason.downcase)

    GitHub.dogstats.increment("issue", tags: ["action:lock"])

    if !GitHub.context[:referrer].nil?
      if GitHub.context[:referrer].include?("stafftools") && GitHub.guard_audit_log_staff_actor?
        user = User.staff_user
        reason = nil
      end
    end

    transaction do
      conversation.lock
      events.create!(event: "locked", actor: user, lock_reason: reason.try(:downcase))
      touch
    end

    notify_socket_subscribers
    synchronize_search_index
  end

  def unlock(user)
    return unless lockable_by?(user) && locked?

    GitHub.dogstats.increment("issue", tags: ["action:unlock"])

    if !GitHub.context[:referrer].nil?
      user = User.staff_user if GitHub.context[:referrer].include?("stafftools") && GitHub.guard_audit_log_staff_actor?
    end

    transaction do
      conversation.unlock
      events.create!(event: "unlocked", actor: user)
      touch
    end

    notify_socket_subscribers
    synchronize_search_index
  end

  # Try to determine the time when the issue was *last* locked. If multiple lock
  # events happened during the course of the issue, take the last one.
  #
  # Returns a Time if locked directly (not archived), otherwise returns nil.
  def locked_at
    return @locked_at if @locked_at
    return nil unless locked?
    @locked_at = events.locks.last&.created_at
  end

  def locked_for?(user)
    async_locked_for?(user).sync
  end

  def async_locked_for?(viewer)
    return Promise.resolve(false) if importing?

    async_repository.then do |repository|
      next true if repository.archived?

      async_locked?.then do |locked|
        next false unless locked
        next true unless viewer

        repository.async_writable_by?(viewer).then do |writable|
          !writable
        end
      end
    end
  end

  def async_lockable_by?(viewer)
    async_repository.then do |repository|
      lockable_by?(viewer)
    end
  end

  def locked_reason
    return "This repository has been archived." if repository.archived?
    "This conversation has been locked and limited to collaborators."
  end

  # Returns `true` if this issue has reached the maximum allowable number of
  # comments. Returns `false` if this is not the case.
  def over_comment_limit?
    issue_comments_count && (issue_comments_count >= COMMENT_LIMIT)
  end

  # Determines if the given `user` can comment on this issue. We look at the
  # comment limit and at the locked state of the issue to determine if the user
  # can comment.
  #
  # Returns `true` if the user can comment on the issue; `false` if they cannot
  def can_comment?(user)
    async_can_comment?(user).sync
  end

  def async_can_comment?(user)
    return Promise.resolve(false) if !user.is_a?(User) || over_comment_limit?
    async_locked?.then do |locked|
      next true unless locked
      async_repository.then do |repo|
        repo.async_pushable_by?(user)
      end
    end
  end

  def to_param
    number.to_s
  end

  # Absolute permalink URL for this issue.
  #
  # include_host - Turn off the `GitHub.url` host in the url. (default true)
  #                issue_comment.permalink(include_host: false) => `/github/github/issue/1`
  #
  def permalink(include_host: true)
    type = pull_request? ? "pull" : "issues"
    "#{repository.permalink(include_host: include_host)}/#{type}/#{number}"
  end
  alias url permalink

  def async_path_uri
    return @async_path_uri if defined?(@async_path_uri)

    @async_path_uri = fetch_path_uri
  end

  def async_path_body_uri
    return @async_path_body_uri if defined?(@async_path_body_uri)

    @async_path_body_uri = fetch_path_uri(anchor: "issue-#{id}")
  end

  # Unique identifier for this issue used in email messages.
  def message_id
    "<#{repository.name_with_owner}/issues/#{number}@#{GitHub.urls.host_name}>"
  end

  def self.find_numbers(numbers)
    where(number: numbers).to_a
  end

  # Public: can the user edit this issue?
  #
  # Returns Boolean
  def editable_by?(user)
    return false unless user
    return true if self.user == user && !locked_for?(user)
    repository.pushable_by?(user)
  end

  def async_editable_by?(viewer)
    return Promise.resolve(false) unless viewer

    async_user.then do |user|
      next false unless user

      editable_by?(viewer)
    end
  end

  def async_viewer_can_update?(viewer)
    async_viewer_cannot_update_reasons(viewer).then(&:empty?)
  end

  def async_viewer_cannot_update_reasons(viewer)
    return Promise.resolve([:login_required]) unless viewer

    Promise.all([async_repository, async_user, async_conversation]).then do |repository, user, conversation|
      context = { repo: repository }
      errors = ContentAuthorizer.authorize(viewer, :Issue, :edit, context).errors.map(&:symbolic_error_code)
      if locked_for?(viewer)
        errors << :locked
      end
      if discussion && repository.discussions_enabled?
        errors << :locked
      end
      if user != viewer && !repository.pushable_by?(viewer)
        errors << :insufficient_access
      end
      errors
    end
  end

  def comments_and_reference_events
    (comments.to_a + events.reference_types.to_a).
      sort_by { |record| record.created_at }
  end

  # Makes this class compatible with IssueComments for rendering the body.
  # See GitHub::UserContent#body_pipeline for more info.
  def formatter
    :markdown
  end

  def created_via_email
    false
  end

  # Internal.
  def max_textile_id
    GitHub.max_textile_issue_id
  end

  # Public: Returns an array of User IDs that are mentioned either in this
  # Issue or any of it's comments. If no users are mentioned then the returned
  # Array is empty.
  #
  # Returns an Array of User IDs.
  def mentioned_user_ids
    IssueEvent.mentioned_users(self).map(&:actor_id)
  end
  alias :referenced_user_ids :mentioned_user_ids

  # Returns the list of mentioned Teams in this issue or its comments. We are
  # using the CrossReference table to lookup this information.
  #
  # Returns an Array of Team IDs
  def referenced_team_ids
    CrossReference.from(self).referencing("Team").map(&:target_id)
  end

  def subscriber_reasons_and_ids
    return unless automated_security_update?

    {
      security_alert: repository.vulnerability_manager.authorized_user_ids,
    }
  end

  def automated_security_update?
    return unless pull_request?
    return unless created_by_dependabot?

    # Dependency updates are only associated by the Automatic Security Update
    # feature, if they are absent Dependabot is performing a version update
    # and we do not need to notify security admins.
    pull_request.dependency_updates.any?
  end

  def created_by_dependabot?
    return false if user.nil?
    return false unless user.bot?

    user.integration&.dependabot_github_app?
  end

  ############################################################################
  ## Search

  # Public: Synchronize this issue with its representation in the search
  # index. If the issue is newly created or modified in some fashion, then it
  # will be updated in the search index. If the issue has been destroyed, then
  # it will be removed from the search index. This method handles both cases.
  #
  def synchronize_search_index
    if pull_request?
      pull_request.synchronize_search_index
    else
      if self.destroyed?
        RemoveFromSearchIndexJob.perform_later("issue", self.id, self.repository_id)
      else
        Search.add_to_search_index("issue", self.id)
      end
    end
    self
  end

  def milestone_or_state_changed?
    saved_change_to_milestone_id? || saved_change_to_state?
  end

  def update_milestone!(working_milestone)
    open_count = working_milestone.issues.open_issues.count
    closed_count = working_milestone.issues.closed_issues.count

    working_milestone.open_issue_count = open_count
    working_milestone.closed_issue_count = closed_count
    working_milestone.save! if working_milestone.changed?
  end

  def update_milestone_counts!
    # update current milestone
    update_milestone!(self.milestone) if self.milestone

    # update previous milestone
    if old_milestone = self.milestone_id_before_last_save
      working_milestone = repository&.milestones&.find_by_id(old_milestone)
      update_milestone!(working_milestone) if working_milestone
    end
  end

  def ensure_valid_milestone
    if milestone && !repository.milestones.include?(milestone)
      errors.add :milestone, "must be valid"
    end
  end

  def ensure_creator_is_not_blocked
    if !transfer && user && repository && user.blocked_by?(repository.owner)
      errors.add :user, "is blocked"
    end
  end

  def ensure_assignee_is_a_collaborator
    return if assignee && assignee.ghost?

    if assignee && !assignable_to?(assignee)
      errors.add :assignee, "must be a collaborator"
    end
  end

  def ensure_user_can_create_pr
    if pull_request.user_unable_to_create_pr?
      errors.add(:base, "You can't perform that action at this time.")
    end
  end

  # Internal: record a "renamed" event if the issue's title has been changed.
  def create_renamed_event
    events.create(event: "renamed", title_is: title, title_was: title_before_last_save, actor: modifying_user) if title_before_last_save
  end

  # Public: create a "deployed" event if the pull request was deployed.
  def create_deployed_event(deployment)
    events.create({
      event: "deployed",
      actor: deployment.creator,
      performed_by_integration_id: deployment.performed_by_integration_id,
      deployment_id: deployment.id,
    })
  end

  # Public: Create a "deployment_environment_changed" event if the deployment
  #         environment was updated.
  def create_deployment_environment_changed_event(deployment_status)
    events.create({
      event: "deployment_environment_changed",
      actor: deployment_status.creator,
      performed_by_integration_id: deployment_status.performed_by_integration_id,
      deployment_status_id: deployment_status.id,
    })
  end

  # Internal: subscribe the author of this Issue.  Fires after_create
  def subscribe_author
    subscribe(user, :author)
  end

  # Internal: Ensure this issue counts towards the user's contributions.
  def clear_contributions_cache
    Contribution.clear_caches_for_user(user) if user
  end

  def show_first_contribution_prompt?(viewer)
    (repository.owner == viewer) && first_contribution_prompt_active?
  end

  # The key is based on the repository owner's id because we want each user to only see this prompt once
  def first_contribution_prompt_key
    "user.show_first_contribution_prompt.#{repository.owner_id}"
  end

  # The value is based on the issue and repository ids so that we know which issue to display the prompt on
  def first_contribution_prompt_value
    "#{self.id}.#{repository_id}"
  end

  def set_first_contribution_flag
    return if GitHub.enterprise?
    return unless CommunityProfile.eligible_repository?(repository)
    return if first_contribution_prompt_active? || seen_first_contribution_prompt?
    collaborator_ids = repository.all_member_ids
    return if collaborator_ids.include?(user_id)
    all_prs = Issue.for_repository(repository_id).with_pull_requests
    collaborator_prs = all_prs.where(user_id: collaborator_ids)
    return unless ((all_prs.count - collaborator_prs.count) == 1)
    GitHub.kv.set(first_contribution_prompt_key, first_contribution_prompt_value)
  end

  def first_contribution_prompt_active?
    GitHub.kv.get(first_contribution_prompt_key).value! == first_contribution_prompt_value
  end

  def seen_first_contribution_prompt?
    GitHub.kv.get(first_contribution_prompt_key).value!.present?
  end

  def dismiss_first_contribution_prompt
    GitHub.kv.set(first_contribution_prompt_key, "false")
  end

  def enqueue_community_health_check
    return unless repository.community_profile && !repository.community_profile.has_issue_opened_by_non_collaborator
    CommunityProfile.enqueue_health_check_job(repository)
  end

  def self.sql_list(query)
    connection.select_all(query).map do |row|
      row.values[0].to_i
    end
  end
  delegate :sql_list, to: "self.class"

  def self.sql
    connection
  end
  delegate :sql, to: "self.class"

  def set_contributed_at
    self.contributed_at ||= Time.zone.now
  end

  def trigger_milestone_event
    events.create(
      event: "milestoned",
      actor: modifying_user,
      performed_via_integration: modifying_integration,
      milestone_id: milestone.id,
      milestone_title: milestone.title,
    )
  end

  def trigger_demilestone_event(old_milestone)
    return if old_milestone.blank?

    events.create(
      event: "demilestoned",
      actor: modifying_user,
      performed_via_integration: modifying_integration,
      milestone_id: old_milestone.id,
      milestone_title: old_milestone.title,
    )
  end

  def trigger_milestone_change_events
    old_milestone = Milestone.find_by_id(self.milestone_id_before_last_save)

    if old_milestone.present?
      trigger_demilestone_event(old_milestone)
      old_milestone.deprioritize_dependent(self)
    end
    trigger_milestone_event if milestone
  end

  # Project events
  def trigger_add_to_project_event(project:, column:, card_id:)
    return if column.blank?

    events.create! \
      event: "added_to_project",
      subject: project,
      column_name: column.name,
      actor: modifying_user,
      card_id: card_id,
      performed_by_project_workflow_action_id: project.current_workflow_action_id
  end

  def trigger_remove_from_project_event(project:, column:, card_id:)
    return if project.blank?
    return if column.blank?

    events.create! \
      event: "removed_from_project",
      subject: project,
      column_name: column.name,
      actor: modifying_user,
      card_id: card_id
  end

  def trigger_move_within_project_event(card:, project:, column:, previous_column_name:)
    return unless previous_column_name.present?

    events.create \
      event: "moved_columns_in_project",
      subject: project,
      column_name: column.name,
      previous_column_name: previous_column_name,
      actor: modifying_user,
      card_id: card.id,
      performed_by_project_workflow_action_id: project.current_workflow_action_id
  end

  # Internal: create corresponding IssueEvents when a label is added to this Issue
  def trigger_label_event(label)
    last_event = events.last
    is_duplicate = last_event.try(:event) == "labeled" && last_event.label_name == label.name

    unless is_duplicate
      events.create \
        event: "labeled",
        actor: modifying_user,
        performed_via_integration: modifying_integration,
        label: label
    end
  end

  def trigger_unlabel_event(label)
    events.create \
      event: "unlabeled",
      actor: modifying_user,
      performed_via_integration: modifying_integration,
      label: label
  end

  def trigger_user_blocked_event(ignored_user)
    block_duration_days = ignored_user.duration || 0
    events.create(
      event: "user_blocked",
      actor: ignored_user.ignored_by,
      subject: ignored_user.ignored,
      block_duration_days: block_duration_days,
    )
  end

  def create_initial_events
    labels.each do |label|
      trigger_label_event(label)
    end

    # Dependent on the update state of labels
    update_repo_community_profile(changed_labels: labels)
  end

  # Public: Triggers the job to deliver notifications of this Comment after
  # creation.  See Summarizable.
  #
  # Returns nil.
  def deliver_notifications(direct_mention_user_ids: nil)
    GitHub.newsies.trigger(
      pull_request? ? pull_request : self,
      direct_mention_user_ids: direct_mention_user_ids,
      event_time: created_at,
    )
  end

  # Public: Gets the RollupSummary for this Comment's thread.
  # See Summarizable.
  #
  # Returns Newsies::Response instance.
  def get_notification_summary
    list = Newsies::List.new("Repository", repository_id)
    GitHub.newsies.web.find_rollup_summary_by_thread(list, self)
  end

  def destroy_notification_summary
    # if we don't have the repository anymore, fake it.
    # this eventually needs to be fixed up to pass the ID directly.
    repo = repository || Repository.new.tap { |r| r.id = repository_id }
    GitHub.newsies.async_delete_all_for_thread(repo, self)
  end

  # Summarizable#update_notification_rollup
  def update_notification_rollup(summary)
    changed = summarizable_changed?(:body, :title, :pull_request_id) ? :changed : :unchanged
    GitHub.dogstats.increment("newsies.rollup", tags: ["changed:#{changed}"])

    summary.summarize_issues(self)
  end

  # Overrides PushNotifiable#push_notification_subtitle
  def push_notification_subtitle
    "#{repository.name_with_owner} ##{number}"
  end

  # Ignores duplicate record creation for the duration of the block. We use
  # unique indexes to prevent records in various places.
  def ignore_duplicate_records
    yield
  rescue ActiveRecord::RecordNotUnique
    # XXX ignore duplicate entries. We can probably also do an
    # INSERT IGNORE query above instead.
  end

  def instrument_creation_event
    instrument :create, task_list: task_list?

    GlobalInstrumenter.instrument "issue.create", {
      actor: safe_user,
      repository: repository,
      repository_owner: repository.owner,
      issue: self,
      issue_creator: safe_user,
      title: title,
      body: body,
      template: template,
      pull_request: pull_request,
    }
  end

  def instrument_transform_to_pull
    instrument :transform_to_pull, issue: self

    GlobalInstrumenter.instrument "issue.transform_to_pull", {
      repository: repository,
      issue: self,
      pull_request: pull_request,
    }
  end

  def instrument_destroy
    instrument(:destroy, { title: title, body: body })
  end

  # Private: Ensure that title or body were set before they were changed.
  #
  # Returns Boolean
  def title_or_body_changed?
    (self.title_changed? || self.body_changed?) ||
      (previous_changes.include?(:title) || previous_changes.include?(:body))
  end

  def title_changed?
    self.title.try(&:b) != self.title_was.try(&:b)
  end

  def instrument_update_event
    old_title, current_title = previous_changes[:title]
    old_body, current_body = previous_changes[:body]
    payload = {issue_id: id, actor: modifying_user}
    payload[:pull_request_id] = pull_request.id if pull_request?
    payload.merge!(old_title: old_title, title: current_title) if old_title
    payload.merge!(old_body: old_body, body: current_body) if old_body
    instrument :update, payload
  end

  def instrument_hydro_update_event(actor:, updater:, repo:, prev_title:, prev_body:)
    # only instrument if title or body change
    return if prev_title == title && prev_body == body

    GlobalInstrumenter.instrument "issue.update", {
      actor: actor,
      repository: repo,
      repository_owner: repo.owner,
      issue: self,
      issue_updater: updater,
      previous_title: prev_title,
      current_title: title,
      previous_body: prev_body,
      current_body: body,
    }
  end

  # Collect conditions under which we don't bother checking the content
  # of this Issue for spam.
  def skip_spam_check?
    repository.nil?          ||   # Orphaned record, repo has been deleted
    repository.private?      ||   # Leave private repos alone.
    repository.member?(user) ||   # Don't bother repo members.
    user.nil?                ||
    user.spammy?             ||   # No point in checking these
    (user.created_at < 7.days.ago &&   # Only give a pass to slightly older
     !user.can_be_flagged?)            # 'unflaggable' accounts
  end

  def check_for_spam?
    persisted? && !skip_spam_check? && !previous_changes.empty?
  end

  # Check the Issue for spam
  #
  # options - currently unused
  def check_for_spam(options = {})
    return if skip_spam_check?

    if reason = GitHub::SpamChecker.test_issue(self)
      GitHub.dogstats.increment "spam.flagged", tags: ["spam_target:issue"]

      user.safer_mark_as_spammy(reason: reason)
    end
  end

  include Instrumentation::Model

  def event_prefix() :issue end
  def event_payload
    payload = {
      event_prefix           => self,
      :repo                  => repository,
      :user                  => safe_user,
      :spammy                => modifying_user.spammy?,
      :allowed               => user_allowed?,
    }

    if pull_request?
      payload[:pull_request] = pull_request
    end

    payload.merge!(event_analytics_payload) if repository
    payload
  end

  def user_allowed?
    repository.permit? modifying_user, :write
  end

  def event_analytics_payload
    {
      # core dimensions (private/public and org- or user-owned)
      private: repository.private?,
      owner_id: repository.owner_id,
      owner_type: repository.owner.type,

      # ancillary dimensions
      issue_id: id,
      created_at: created_at,
      updated_at: updated_at,
      closed_at: closed_at,
      milestone_id: milestone_id,
      assignee_id: assignee_id,
      assignee_ids: assignments.map(&:assignee_id),
      number: number,
      state: state,
      labels: labels.map(&:name),
      issue_comments_count: issue_comments_count,

      # measures
      mentioned_issues: mentioned_issues.size,
      mentioned_users: mentioned_usernames.size,
      mentioned_teams: mentioned_teams.size,
      task_list_items: task_list_summary.item_count || 0,
    }
  end

  def contribution_time
    @contribution_time ||= if contributed_at_timezone_aware?
      contributed_at
    else
      created_at.localtime
    end
  end

  def contributed_on
    contribution_time.to_date
  end

  def update_issue_comments_count
    GitHub::SchemaDomain.allowing_cross_domain_transactions do
      update_attribute(:issue_comments_count, comments.not_spammy.count)
    end
  end

  def priority_for_milestone?
    issue_priorities.where(milestone_id: self.milestone_id).exists?
  end

  def prioritize_in_milestone
    return unless milestone.present? && milestone.prioritizable?
    return if milestone.needs_backfill?(issue: self)
    return if priority_for_milestone?

    milestone.prioritize_issue!(self, position: :bottom)
  end

  def global_relay_id
    if pull_request_id.present?
      Platform::Helpers::NodeIdentification.to_global_id("PullRequest", pull_request_id)
    else
      super
    end
  end

  def human_name
    if pull_request
      "pull request"
    else
      "issue"
    end
  end

  def owner
    repository.owner
  end

  # Internal: Enqueues a job if help wanted labels have been added or removed
  #
  # Returns nil
  def update_repo_community_profile(changed_labels: [])
    changed_names = changed_labels.map { |label| label.name.downcase }
    changed_help_wanted_names = changed_names.select do |name|
      name.start_with?(*HELP_WANTED_LABEL_NAMES)
    end
    return if changed_help_wanted_names.empty?

    update_repo_community_profile!
  end

  def update_repo_community_profile!
    CommunityProfile.enqueue_help_wanted_job(repository)
  end

  # Public: Returns this Issue. This helps to avoid `is_a?` calls when you have
  # a variable that can be an issue or PR.
  #
  # Examples
  #
  #   issue_or_pr.to_issue.my_favorite_issue_method
  #
  # Returns an Issue
  def to_issue
    self
  end

  def unique_label_ids
    @unique_label_ids ||= Set.new(label_ids)
  end

  private

  # Internal: looks for issue_blob_references in body_result and saves them to the DB
  #
  # Returns nil
  def save_issue_blob_references
    return unless body && body_result[:issue_blob_references]

    body_result[:issue_blob_references].each do |reference_data|
      attributes = {
        commit_oid: reference_data[:commit_oid],
        filepath: "/#{reference_data[:filepath]}", # Add a leading slash for backwards-compatibility
        blob_oid: reference_data[:blob_oid],
        range_start: reference_data[:range_start],
        range_end: reference_data[:range_end],
      }

      unless issue_blob_references.exists?(attributes)
        bref = issue_blob_references.new(attributes)
        bref.enqueue_report_creation(safe_user) if bref.save
      end
    end
  end

  def contributed_at_timezone_aware?
    contributed_at && contributed_at > Contribution::Calendar::TIMEZONE_AWARE_CUTOVER
  end

  def ensure_authorized_to_create_content
    operation = new_record? ? :create : :update
    authorization = ContentAuthorizer.authorize(modifying_user, :issue, operation,
                                                repo: repository)

    if authorization.failed?
      errors.add(:base, authorization.error_messages)
    end
  end

  def ensure_not_converted_to_discussion
    return unless repository&.discussions_enabled?

    if discussion
      errors.add(:base, "Cannot be modified since it has been converted to a discussion.")
    end
  end

  # Internal: Is this github process in importing mode?
  #
  # Returns a boolean.
  def importing?
    GitHub.importing?
  end

  # Was the body changed in previous_changes? We use this to determine
  # if body changes _only_ after_commit.
  def body_changed_after_commit?
    previous_changes.key?(:body)
  end

  # Used to report invalid pull_request_id when attaching an issue to a pull request.
  class PullRequestIdInvalid < StandardError
  end

  # Used to report invalid assignee when validating assigned to input.
  class AssigneeInvalid < StandardError
    attr_reader :assignee
    def initialize(assignee, msg)
      super(msg)
      @assignee = assignee
    end
  end

  def fetch_path_uri(anchor: nil)
    async_repository.then(&:async_owner).then do
      type = pull_request_id.present? ? "pull" : "issues"
      URI_TEMPLATE.expand(
        owner:  repository.owner.login,
        name:   repository.name,
        type:   type,
        number: number,
        anchor: anchor,
      )
    end
  end

  # Internal: Validation preventing against `pull_request_id` being set to 0.
  #
  # Returns nothing. Adds an error to :pull_request_id unless valid.
  def pull_request_id_is_not_zero
    if pull_request_id&.zero?
      boom = PullRequestIdInvalid.new("pull_request_id set to 0")
      boom.set_backtrace(caller)
      Failbot.report(boom, repo_id: repository&.id)

      errors.add(:pull_request_id, "is invalid")
    end
  end
end
