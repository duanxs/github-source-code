# frozen_string_literal: true

class SponsorsTier < ApplicationRecord::Collab
  class RetirementNotAllowed < StandardError; end

  extend GitHub::Encoding
  force_utf8_encoding :description, :name

  include Workflow
  include GitHub::Relay::GlobalIdentification
  include Billing::Subscribable
  include SponsorsTier::ZuoraDependency

  MAX_NAME_LENGTH = 255
  MAX_DESCRIPTION_LENGTH = 750
  MAX_MONTHLY_SPONSORSHIP_AMOUNT = 6_000 # The max dollar amount allowed per monthly sponsorship transaction.
  PUBLISHED_TIER_COUNT_LIMIT_PER_LISTING = 10

  belongs_to :sponsors_listing, required: true

  # This is temporary, to give us a reliable way to track plan => tier as we transition
  # associated models to use tiers as subscribables.
  #
  # See https://github.com/github/sponsors/issues/353
  belongs_to :listing_plan, class_name: "Marketplace::ListingPlan",
                            foreign_key: "marketplace_listing_plan_id"

  has_many :sponsorships, as: :subscribable

  has_many :subscription_items, class_name: "Billing::SubscriptionItem",
                                as: :subscribable,
                                dependent: :restrict_with_error
  has_many :pending_subscription_item_changes, class_name: "Billing::PendingSubscriptionItemChange",
                                               as: :subscribable,
                                               dependent: :destroy
  has_many :billing_transaction_line_items, class_name: "Billing::BillingTransaction::LineItem",
                                            as: :subscribable

  has_many :sponsorship_newsletter_tiers, dependent: :destroy

  validates :name, :description, :monthly_price_in_cents, :yearly_price_in_cents, presence: true
  validates :name, length: { maximum: MAX_NAME_LENGTH }
  validates :description, length: { maximum: MAX_DESCRIPTION_LENGTH }
  validates :monthly_price_in_cents, :yearly_price_in_cents, numericality: {
    only_integer: true,
    greater_than: 0,
  }

  validate :name_can_be_changed, if: :name_changed?
  validate :tier_amount_can_be_changed
  validate :published_tier_count_limit
  validate :tier_amount_within_sponsorship_limit
  validate :published_tier_amount_is_unique
  validate :published_tier_name_is_unique

  alias_attribute :listing, :sponsors_listing
  alias_attribute :async_listing, :async_sponsors_listing

  scope :with_states, ->(*states) do
    state_values = states.map { |state| state_value(state) }
    where(state: state_values)
  end

  scope :with_active_sponsorships, -> {
    joins(:sponsorships).merge(Sponsorship.active).distinct
  }

  scope :joins_sponsors_membership, -> {
    joins(:sponsors_listing)
    .joins <<-SQL
      INNER JOIN sponsors_memberships ON (
        sponsors_memberships.sponsorable_id = sponsors_listings.sponsorable_id AND
        sponsors_memberships.sponsorable_type = sponsors_listings.sponsorable_type
      )
    SQL
  }

  delegate :sponsorable_type,
           :sponsorable_id,
           to: :sponsors_listing

  workflow :state do
    state :draft, 0 do
      event :publish, transitions_to: :published, if: :can_be_published_for_listing?
    end

    state :published, 1 do
      event :retire, transitions_to: :retired
    end

    state :retired, 2

    on_transition do |from, to, event, *args, **kwargs|
      SponsorsTierZuoraSyncJob.perform_later(self) if to == :published
    end
  end

  # Public: Find the tier based on a Marketplace::ListingPlan
  # This method is only a temp method while we decouple Marketplace::ListingPlan
  # to SponsorsTier https://github.com/github/sponsors/issues/353
  def self.find_from_plan(plan)
    SponsorsTier.find_by(marketplace_listing_plan_id: plan.id)
  end

  # Public: Get the Integer value matching a certain state.
  def self.state_value(name)
    workflow_spec.states[name.to_sym]&.value
  end

  # First pending_subscription_item_change for an account and a tier
  def pending_subscription_item_change(account:)
    account.pending_subscription_item_changes.for_sponsors_tier(self).first
  end

  # Public: This is always false for Sponsors tiers.
  # We need this method to support Billing::Subscribable interface.
  def per_unit?
    false
  end

  # Public: This is always false for Sponsors tiers.
  # We need this method to support Billing::Subscribable interface.
  def has_free_trial?
    false
  end

  # Public: Returns false until we support Corporate Sponsors and Sponsored Orgs
  # Need this method to support Billing::Subscribable interface.
  def for_organizations_only?
    false
  end

  # Public: Returns true as user-to-user is the only type of Sponsorship we currently support
  # We need this method to support Billing::Subscribable interface.
  def for_users_only?
    false
  end

  # Public: Returns true if the pricing can still be changed for this tier.
  def can_change_pricing?
    draft?
  end

  # Public: Whether this tier can be published.
  def can_be_published_for_listing?
    return false if sponsors_listing.reached_maximum_tier_count?
    sponsors_listing.published_tiers_allowed?
  end

  # Public: Whether this tier can be retired.
  def can_be_retired?
    return false unless published?
    other_published_tiers.any?
  end

  # Public: Is an actor able to view this tier?
  #
  # actor - The User to check permissions for.
  #
  # Returns a Promise<Boolean>.
  def async_readable_by?(actor)
    async_sponsors_listing.then do |listing|
      next true if listing.approved? && published?
      next false unless actor.present?
      next true if actor.can_admin_sponsors_listings?

      adminable_ids = Array(actor.id) | actor.admin_or_manager_organizations.pluck(:id)
      plan_subscriptions = Billing::PlanSubscription.where(user_id: adminable_ids)
      next true if Billing::SubscriptionItem.
        with_ids(plan_subscriptions.pluck(:id), field: "plan_subscription_id").
        where(subscribable: self).
        exists?

      actor.async_plan_subscription.then do
        listing.async_adminable_by?(actor)
      end
    end
  end

  # Public: Is an actor able to view this tier?
  #
  # actor - The User to check permissions for.
  #
  # Returns a Boolean.
  def readable_by?(actor)
    async_readable_by?(actor).sync
  end

  # Public: Returns true if the given User has admin access to this plan
  def adminable_by?(actor)
    sponsors_listing&.adminable_by?(actor)
  end

  # Public: Returns true if the given User has permission to edit this tier.
  def editable_by?(actor)
    sponsors_listing&.editable_by?(actor) && !retired?
  end

  # Public: Returns true if the given User has permission to delete this tier.
  #   We only allow this while:
  #    - Tier is in Draft.
  #    - Listing is in Draft (published tiers may be deleted in this state).
  def deletable_by?(actor)
    return false if sponsors_listing&.approved? && (published? || retired?)

    editable_by?(actor)
  end

  def description_html(context = {})
    GitHub::Goomba::SponsorsListingPipeline.to_html(description, context)
  end

  private

  # This is called by workflow when transitioning to a :retired state.
  #
  # We could instead include :can_be_retired? when defining the state
  # transition, but that would raise a TransitionNotAllowed exception
  # without surfacing the reason that a tier can't be retired.
  def retire(*args, **kwargs)
    raise RetirementNotAllowed.new("Can't retire the only published tier.") unless can_be_retired?
  end

  # Private: Ensures name is only changed when allowed
  def name_can_be_changed
    return if draft? || new_record?

    errors.add(:name, "cannot be changed for a published tier")
  end

  # Private: Ensures tier amount is only changed when allowed
  def tier_amount_can_be_changed
    return if draft? || new_record?

    errors.add(:monthly_price_in_cents, "cannot be changed for a published tier") if monthly_price_in_cents_changed?
    errors.add(:yearly_price_in_cents, "cannot be changed for a published tier") if yearly_price_in_cents_changed?
  end

  def published_tier_count_limit
    return unless sponsors_listing && published?

    if other_published_tiers.count >= PUBLISHED_TIER_COUNT_LIMIT_PER_LISTING
      errors.add(:sponsors_listing, "has reached its limit for published tiers")
    end
  end

  # Private: Ensures tier amount is within sponsorship limit
  def tier_amount_within_sponsorship_limit
    return unless monthly_price_in_cents

    if monthly_price_in_cents > SponsorsTier::MAX_MONTHLY_SPONSORSHIP_AMOUNT * 100
      errors.add(
        :monthly_price_in_cents,
        "exceeds maximum monthly tier amount of $#{SponsorsTier::MAX_MONTHLY_SPONSORSHIP_AMOUNT}",
      )
    end
  end

  # Private: Ensures published tiers have unique amount
  def published_tier_amount_is_unique
    return unless sponsors_listing && !published?

    if other_published_tiers.where(monthly_price_in_cents: monthly_price_in_cents).count > 0
      errors.add(:monthly_price_in_cents, "is already in use by a published tier")
    end
  end

  # Private: Ensures each published tier for the sponsors listing has a unique name
  def published_tier_name_is_unique
    return unless sponsors_listing && published?

    # mysql collation is case-insensitive
    if other_published_tiers.where(name: name).count > 0
      errors.add(:name, "is already in use by a published tier")
    end
  end

  # Private: Get a scope for this listing's published tiers excluding this instance.
  def other_published_tiers
    tiers = sponsors_listing.sponsors_tiers.with_published_state
    tiers = tiers.where("id <> ?", id) if persisted?
    tiers
  end
end
