# frozen_string_literal: true

class SponsorsCriterion::Automated
  attr_reader :sponsors_membership

  # Checks that can apply to either users or organizations
  GLOBAL_CHECKS = [
    AccountAge,
    SpammyStatus,
    AbuseReports,
    OFACCompliance,
  ].freeze

  USER_CHECKS = ([
    SuspensionStatus,
    BlockedThreshold,
    PublicContributions,
    FollowerThreshold,
  ] + GLOBAL_CHECKS).freeze

  ORGANIZATION_CHECKS = ([
    # TODO: Add org-specific automated checks
  ] + GLOBAL_CHECKS).freeze

  CHECKS = (USER_CHECKS + ORGANIZATION_CHECKS).uniq.freeze

  def initialize(sponsors_membership:)
    @sponsors_membership = sponsors_membership
  end

  def self.organization_checks
    @organization_checks ||= ORGANIZATION_CHECKS
  end

  def self.organization_checks=(checks)
    @organization_checks = checks
  end

  def self.user_checks
    @user_checks ||= USER_CHECKS
  end

  def self.user_checks=(checks)
    @user_checks = checks
  end

  # Public: Returns results for all automated checks for multiple SponsorsMemberships.
  #
  # memberships – An Array of SponsorsMemberships.
  #
  # Returns an Array[SponsorsCriterion::Automated::BulkResult].
  def self.bulk_results_for(memberships:)
    promises = memberships.map do |membership|
      async_check_results_for(sponsors_membership: membership)
    end

    results = Promise.all(promises).sync.flatten.group_by(&:sponsors_membership)

    results.map do |membership, results|
      BulkResult.new(sponsors_membership: membership, results: results)
    end
  end

  # Public: Returns results for all automated checks for a SponsorsMembership.
  #
  # sponsors_membership – The SponsorsMembership to return check results for.
  #
  # Returns a Promise<Array[SponsorsCriterion::Automated::Result]>.
  def self.async_check_results_for(sponsors_membership:)
    sponsors_membership.async_sponsorable.then do |sponsorable|
      checks = if sponsorable.organization?
        organization_checks
      else
        user_checks
      end

      result_promises = checks.map do |check|
        check.async_result_for(sponsors_membership: sponsors_membership)
      end

      Promise.all(result_promises)
    end
  end

  # Public: Returns results for all automated checks for a SponsorsMembership.
  #
  # sponsors_membership – The SponsorsMembership to return check results for.
  #
  # Returns an Array[SponsorsCriterion::Automated::Result].
  def self.check_results_for(sponsors_membership:)
    async_check_results_for(sponsors_membership: sponsors_membership).sync
  end

  # Public: Returns a single check's result for a SponsorsMembership.
  #
  # sponsors_membership – The SponsorsMembership to return the check result for.
  #
  # Returns a Promise<SponsorsCriterion::Automated::Result>.
  def self.async_result_for(sponsors_membership:)
    new(sponsors_membership: sponsors_membership).async_result
  end

  # Public: Updates automated criteria records for a membership.
  #
  # sponsors_membership – The SponsorsMembership to update records for.
  #
  # Returns a Boolean.
  def self.refresh_for(sponsors_membership:)
    bulk_results = bulk_results_for(memberships: [sponsors_membership])
    result = SponsorsCriterion::Automated::BulkWriter.call(
      bulk_results: bulk_results,
    ).first

    result.success?
  end

  # Public: Returns the SponsorsCriterion associated with an automated check.
  #
  # Returns a Promise<SponsorsCriterion>.
  def self.async_criterion
    Platform::Loaders::SponsorsCriterionBySlug.load(slug: slug)
  end

  # Public: The slug of the SponsorsCriterion this automated check is for.
  #
  # Returns a String.
  def self.slug
    raise NotImplementedError.new(".slug should be implemented by subclasses")
  end

  # Public: The result of an automated check for this sponsors membership.
  #
  # Returns a Promise<SponsorsCriterion::Automated::Result>.
  def async_result
    Promise.all([
      self.class.async_criterion,
      async_met?,
      async_value,
    ]).then do |criterion, met, value|
      Result.new(
        sponsors_membership: sponsors_membership,
        sponsors_criterion: criterion,
        met: met,
        value: value,
      )
    end
  end

  # Public: Indicates if the criterion is met.
  #
  # Returns a Promise<Boolean>.
  def async_met?
    raise NotImplementedError.new("#async_met? should be implemented by subclasses")
  end

  # Public: The value to store for this criterion, if applicable.
  #         Because the value field is a text field, this should always return a
  #         String if we want to store a value.
  #
  #
  # Returns a Promise<String|nil>.
  def async_value
    raise NotImplementedError.new("#async_value should be implemented by subclasses")
  end

  class Result
    attr_reader :sponsors_membership, :sponsors_criterion, :met, :value
    alias_method :met?, :met

    def initialize(sponsors_membership:, sponsors_criterion:, met:, value:)
      @sponsors_membership = sponsors_membership
      @sponsors_criterion = sponsors_criterion
      @met = met
      @value = value
    end
  end

  class BulkResult
    attr_reader :sponsors_membership, :results

    # sponsors_membership - The SponsorsMembership the results are for.
    # results - An Array of SponsorsCriterion::Automated::Result objects.
    def initialize(sponsors_membership:, results:)
      @sponsors_membership = sponsors_membership
      @results = results
    end
  end
end
