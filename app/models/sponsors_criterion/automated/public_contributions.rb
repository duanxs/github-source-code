# frozen_string_literal: true

class SponsorsCriterion::Automated::PublicContributions < SponsorsCriterion::Automated

  MINIMUM_CONTRIBUTIONS = 100

  CONTRIBUTION_CLASSES = [
    Contribution::CreatedCommit,
    Contribution::CreatedIssue,
    Contribution::CreatedPullRequest,
    Contribution::CreatedPullRequestReview,
  ]

  # Public: The slug of the SponsorsCriterion this automated check is for.
  #
  # Returns a String.
  def self.slug
    "public_contributions"
  end

  # Public: Indicates if the criterion is met. For PublicContributions, we return
  #         true if the sponsorable's account has a public contribution count
  #         greater than or equal to MINIMUM_CONTRIBUTIONS.
  #
  # Returns a Promise<Boolean>.
  def async_met?
    async_contribution_count.then do |follower_count|
      follower_count >= MINIMUM_CONTRIBUTIONS
    end
  end

  # Public: The number of public contributions for this sponsorable.
  #
  # Returns a Promise<String>.
  def async_value
    async_contribution_count.then { |count| count.to_s }
  end

  private

  # Private: The number of public contributions for this sponsorable.
  #
  # Returns a Promise<Integer>.
  def async_contribution_count
    sponsors_membership.async_sponsorable.then do |sponsorable|
      accessor = Contribution::Accessor.new(
        user: sponsorable,
        viewer: nil,
        contribution_classes: CONTRIBUTION_CLASSES,
        date_range: 1.year.ago.to_date..Date.current,
        organization_id: nil,
        skip_restricted: true,
      )

      accessor.counts_by_class_name.values.sum
    end
  end
end
