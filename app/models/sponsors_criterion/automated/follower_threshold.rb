# frozen_string_literal: true

class SponsorsCriterion::Automated::FollowerThreshold < SponsorsCriterion::Automated

  MINIMUM_FOLLOWERS = 5

  # The old deprecated v1 slug
  V1_SLUG = "follower_threshold"

  # Public: The slug of the SponsorsCriterion this automated check is for.
  #
  # Returns a String.
  def self.slug
    "follower_threshold_v2"
  end

  # Public: Indicates if the criterion is met. For FollowerThreshold, we return
  #         true if the sponsorable's account is not followed by at least the
  #         number of users defined by MINIMUM_FOLLOWERS.
  #
  # Returns a Promise<Boolean>.
  def async_met?
    async_follower_count.then do |follower_count|
      follower_count >= MINIMUM_FOLLOWERS
    end
  end

  # Public: The number of users who are following this sponsorable.
  #
  # Returns a Promise<String>.
  def async_value
    async_follower_count.then { |count| count.to_s }
  end

  private

  # Private: The number of users following a sponsorable account.
  #
  # Returns a Promise<Integer>.
  def async_follower_count
    sponsors_membership.async_sponsorable.then do |sponsorable|
      sponsorable.followers_count
    end
  end
end
