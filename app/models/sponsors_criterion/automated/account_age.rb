# frozen_string_literal: true

class SponsorsCriterion::Automated::AccountAge < SponsorsCriterion::Automated

  # Public: The slug of the SponsorsCriterion this automated check is for.
  #
  # Returns a String.
  def self.slug
    "account_age"
  end

  # Public: Indicates if the criterion is met. For AccountAge, we return
  #         true if the sponsorable's account was created at least 6 months ago.
  #
  # Returns a Promise<Boolean>.
  def async_met?
    async_created_at.then do |created_at|
      created_at <= 6.months.ago
    end
  end

  # Public: When the sponsorable's account was created.
  #
  # Returns a Promise<String>.
  def async_value
    async_created_at.then do |created_at|
      created_at.to_time.utc.iso8601
    end
  end

  private

  # Private: When the sponsorable's account was created.
  #
  # Returns a Promise<ActiveSupport::TimeWithZone>.
  def async_created_at
    sponsors_membership.async_sponsorable.then do |sponsorable|
      sponsorable.created_at
    end
  end
end
