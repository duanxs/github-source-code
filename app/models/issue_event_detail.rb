# frozen_string_literal: true

# Replaces serialized attributes from IssueEvent records
# Because the issue_event_details table can represent several types of
# IssueEvent, many of its fields may be null.
class IssueEventDetail < ApplicationRecord::Domain::Repositories
  include GitHub::Validations
  extend GitHub::Encoding

  belongs_to :issue_event
  belongs_to :subject, polymorphic: true

  alias_method :raw_subject, :subject
  alias_method :raw_subject=, :subject=

  attr_reader :label

  force_utf8_encoding :milestone_title, :title_is, :title_was, :message, :column_name,
                      :previous_column_name, :label_name

  # VARBINARY limit from the database
  UTF8_BYTESIZE_LIMIT = 1024

  validates :milestone_title, unicode: true, bytesize: { maximum: UTF8_BYTESIZE_LIMIT }
  validates :title_is, unicode: true, bytesize: { maximum: UTF8_BYTESIZE_LIMIT }
  validates :title_was, unicode: true, bytesize: { maximum: UTF8_BYTESIZE_LIMIT }
  validates :message, unicode: true, length: {maximum: 1000}

  # Public: set the label attributes
  #
  # label: A Label
  def label=(label)
    self.label_id         = label.id
    self.label_name       = label.name
    self.label_color      = label.color
    self.label_text_color = label.text_color
    @label                = label
  end

  # Public: set the subject
  #
  # assigned_subject: a User, Issue, Bot, Project, or Team
  def subject=(assigned_subject)
    self.raw_subject = @subject = assigned_subject
  end

  # Public: Return the subject
  #
  # Returns a User, Issue, Bot, Project, or Team
  def subject
    @subject ||= if subject_type
      self.raw_subject
    elsif subject_id
      User.find_by id: subject_id
    end
  end

  def async_subject
    type = subject_type&.constantize || ::User
    Platform::Loaders::ActiveRecord.load(type, subject_id)
  end
end
