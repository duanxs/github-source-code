# rubocop:disable Style/FrozenStringLiteralComment

module Issue::AssignmentDependency
  extend ActiveSupport::Concern

  # this is used as a fallback when we have trouble finding the repo owner's plan info
  # which can happen in some background jobs such as `RemoveNoncollabAssigneesFromIssueJob`
  DEFAULT_ASSIGNEE_LIMIT = 10

  included do
    validate :assignment_length_under_limit
  end

  # Public: Set the full list of assignees for this issue. For
  # backwards-compatibility, sets the assignee_id field to the first assignee.
  #
  # new_assignees - The Array of Users that will replace the existing list of
  #                 assignees. WARNING: This should include ALL assignees you
  #                 wish to add or keep.
  #
  # Returns an Array of Users
  def assignees=(new_assignees)
    previous_assignees = assignees.reload
    new_assignees = new_assignees.compact.uniq

    assignees_changed = (previous_assignees.sort != new_assignees.sort)

    assignees_to_keep = if single_assignee?
      # when just a single assignee is allowed, replace the existing assignee with the new one
      if assignees_changed
        new_assignees - previous_assignees
      else
        new_assignees
      end
    else
      new_assignees & previous_assignees
    end

    assignments_to_keep = assignments.for_assignees(assignees_to_keep)

    unless new_record?
      removed_assignments = self.assignments.excluding_ids(assignments_to_keep.to_a)
      removed_assignments = removed_assignments.reject do |unassigned|
        users_being_destroyed.find { |id| unassigned.assignee_id == id }
      end
      removed_assignments.each(&:trigger_unassigned_event)
    end

    unassignable_users = (new_assignees - assignees_to_keep).select do |user|
      available_assignee_ids.exclude?(user.id)
    end

    if unassignable_users.present?
      # Remove invalid assignees - they shouldn't be autosaved
      errors.add :assignees, "unable to be set"
      new_assignees = new_assignees - unassignable_users
    end

    new_assignees = if single_assignee?
      # when only one assignee is allowed, we want an array with the sole assignee
      if new_assignees.size > 1
        # track whenever we are truncating assignees on single assignee issue/PRs
        # (this is expected to happen when single assignee selection isn't implemented in whatever client app the user is using)
        stats_subject = pull_request ? "pull_request" : "issue"
        GitHub.dogstats.increment("assignees_truncated", tags: ["subject:#{stats_subject}"])
      end

      assignees_to_keep.take(1)
    else
      # Don't allow more than X assignees
      new_assignees.first(assignee_limit)
    end

    GitHub::SchemaDomain.allowing_cross_domain_transactions { super(new_assignees) }

    # Keep the legacy assignee_id field synced with the assignees.
    self[:assignee_id] = new_assignees.first && new_assignees.first.id

    # Nuke the memoization in assigned_to?
    remove_instance_variable(:@_assigned_to) if instance_variable_defined?(:@_assigned_to)

    remove_instance_variable(:@users_being_destroyed) if instance_variable_defined?(:@users_being_destroyed)

    # Track multi-assignment usage if the list has changed
    if assignees_changed
      self.dirty = true
      stats_namespace = pull_request ? "pull_request" : "issue"
      GitHub.dogstats.histogram("new_assignees", new_assignees.size, tags: ["subject:#{stats_namespace}"])
    end

    new_assignees
  end

  # Public: Return the assignee limit from the repo's plan configuration
  #
  # Returns an integer
  def assignee_limit
    @assignee_limit ||= if plan_assignee_limit_enabled?
      repository&.plan_limit(:issue_pr_assignees) || DEFAULT_ASSIGNEE_LIMIT
    else
      DEFAULT_ASSIGNEE_LIMIT
    end
  end

  # Public: Are we only allowing users to select one assignee?
  #
  # Returns a Boolean
  def single_assignee?
    assignee_limit == 1
  end

  # Public: Set the assignee for this Issue. CAUTION: This method will clear
  # any other assignments on this issue.
  #
  # new_assignee - The User to set as the new assignee
  #
  # Returns the new_assignee User
  def assignee=(new_assignee)
    self.assignees = [new_assignee].compact
    super
  end

  # Public: Add the specified assignees.
  #
  # users - Array of Users to add as assignees.
  #
  # Returns self (the Issue)
  def add_assignees(*users)
    self.assignees = self.assignees.reload + users.flatten.uniq
    self
  end

  # Public: Remove the specified assignees.
  #
  # users - Array of Users to remove as assignees.
  #
  # Returns self (the Issue)
  def remove_assignees(*users)
    users = users.flatten.uniq
    users.each do |user|
      self.users_being_destroyed << user.id if user.being_destroyed?
    end
    self.assignees = self.assignees.reload - users
    self
  end

  # Public: Is this issue assigned to that user?
  #
  # possible_assignee - a User
  #
  # Returns a Boolean
  def assigned_to?(possible_assignee)
    return false unless possible_assignee

    @_assigned_to ||= Hash.new do |hash, key|
      hash[key] = assignees.include?(key) || assignee == key
    end
    @_assigned_to[possible_assignee]
  end

  # Public: Can this issue be assigned to that user?
  #
  # This is a permissions check, not a check against existing assignees. This
  # means the result will be true for already-assigned users.
  #
  # possible_assignee - a User
  #
  # Returns a Boolean.
  def assignable_to?(possible_assignee, after_save = false)
    return false unless possible_assignee
    available_assignee_ids(after_save).include?(possible_assignee.id)
  end

  # Public: Get the IDs of the users that can be assigned to this issue.
  #
  # This is a permissions check, not a check against existing assignees. This
  # means the result will include users that are already assigned.
  #
  # Returns an array of Integers.
  def available_assignee_ids(after_save = false)
    return @available_assignee_ids unless @available_assignee_ids.nil?

    # user ids for all commenters
    commenter_ids = comments.pluck(:user_id)

    # user ids for everyone with write access to the repo
    # this actually also loads the users, not just their ids - WOMP
    privileged_ids = GitHub.dogstats.time("assignees.fetch_privileged_ids") { user_ids_with_privileged_access.compact }

    # add 'em together and remove any abusive users
    potential_assignees = (privileged_ids + commenter_ids).uniq
    suspended_ids = GitHub.dogstats.time("assignees.fetch_suspended_ids") { suspended_and_blocking_user_ids(potential_assignees, after_save) }
    @available_assignee_ids = potential_assignees - suspended_ids
  end

  # Internal: Find the users that are blocking the modfiyng_user, and suspended users
  # Those users are considered unavailable for assignment
  #
  # Check saved status -- if the record has already been saved with the assignment,
  # we want to ensure that the assignment was added by a non-blocked user.
  #
  # Returns an array of user_ids
  def suspended_and_blocking_user_ids(privileged_ids, after_save = false)
    return [] if privileged_ids.empty?

    unavailable_blocking_user_ids(privileged_ids, after_save) +
    User.where(id: privileged_ids).suspended.pluck(:id)
  end

  # Internal: Find the users that cannot be assigned by the modifying user
  # due to blocked status
  #
  # If the record has already been saved, we want to allow a blocking user
  # to remain assigned, as long as the blocked user isn't the one doing the assigning.
  #
  # Returns an array of user_ids
  def unavailable_blocking_user_ids(assignee_ids, after_save = false)
    assignee_ids = user_ids_assigned_by_modifying_user(assignee_ids) if after_save
    IgnoredUser.where(ignored_id: modifying_user.id, user_id: assignee_ids).pluck(:user_id)
  end

  # Internal: Find the users that were previously assigned by the modifying user
  #
  # Returns an array of user_ids
  def user_ids_assigned_by_modifying_user(assignee_ids)
    self.events.assigns
      .where(issue_events: { actor: modifying_user })
      .where(issue_event_details: { subject_id: assignee_ids })
      .pluck(:subject_id)
  end

  # Public: Get the users that can be assigned to this issue.
  #
  # This is a permissions check, not a check against existing assignees. This
  # means the result will include users that are already assigned.
  #
  # CAUTION: This is a potentially expensive method.
  #
  # Returns an array of Users.
  def available_assignees
    @available_assignees ||= User.where(id: available_assignee_ids).includes(:profile)
  end

  # Internal: Remove all non-collaborator assignees. Sometimes people are
  # assigned who shouldn't be (e.g. after being removed from a team). This
  # makes sure we cover our tracks.
  #
  # Returns the new list of assignees if any noncollab assignees are detected
  def remove_noncollab_assignees
    if has_noncollab_assignee?
      # reset @available_assignee_ids so they can be recalculated
      @available_assignee_ids = nil
      self.skip_noncollab_assignee_callback = true
      self.assignees = self.assignees - self.noncollab_assignees(true)
      self.save
    end
  end

  def enqueue_remove_noncollab_assignees
    RemoveNoncollabAssigneesFromIssueJob.perform_later(id)
  end

  # Internal: Is this issue assigned to anyone?
  #
  # Returns a boolean.
  def assigned?
    ActiveRecord::Base.connected_to(role: :reading) do
      !!assignee_id ||
        assignments.exists? ||
        assignees.present?
    end
  end

  # Internal: Is this issue assigned to any users who are not collaborators on
  # the repo?
  #
  # Returns a boolean.
  def has_noncollab_assignee?
    noncollab_assignee? || noncollab_assignees(true).any?
  end

  # Internal: Gets all associated assignments that are for users who are not
  # collaborators on the repo.
  #
  # Returns an Array of Users
  def noncollab_assignees(after_save = false)
    self.assignees.select do |member|
      noncollab_assignee?(member, after_save)
    end
  end

  # Resort assignees list prioritizing the current user and current assignee
  # followed by an alpha sort.
  #
  # users - Array of possible assignee Users
  #
  # Returns Array of Users.
  def sorted_assignees_list(current_user:)
    @sorted_assignees_list ||=  begin
      list = self.assignees + non_assigned_potential_assignees(current_user: current_user)

      if current_user && user = list.delete(current_user)
        list.unshift user
      end

      list.uniq
    end
  end

  # Private: Get the list of users that have not been assigned to this issue.
  #
  # This performs a permission check and also checks against existing assignees.
  # This means the result will not include already assigned users.
  #
  #
  # Returns an array of Users.
  private def non_assigned_potential_assignees(current_user:)
    @non_assigned_potential_assignees ||= begin
      available_ids = GitHub.dogstats.time("assignees.get_available_assignees_ids") { available_assignee_ids }

      GitHub.dogstats.time("assignees.fetch_users") do
        User.includes(:profile, :user_status)
          .select("users.id, users.login, users.type")
          .where(id: available_ids - self.assignees.pluck(:id))
          .where.not(type: "Bot")
          .filter_spam_for(current_user)
          .order(:login)
      end
    end
  end

  # Internal: Is this a collaborator on the repo?
  #
  # member - Any User (defaults to the Issue's assignee to preserve existing
  #          previous method behavior)
  #
  # Returns a boolean.
  def noncollab_assignee?(member = assignee, after_save = false)
    assigned_to?(member) && !assignable_to?(member, after_save)
  end

  # Internal: Create an assignment event for a new assignee.
  #
  # assigned - The User who was assigned
  #
  # Returns nothing
  def trigger_assigned_event(assigned)
    return unless assigned

    ignore_duplicate_records do
      GitHub::SchemaDomain.allowing_cross_domain_transactions do
        events.create(event: "assigned", actor_id: assigned.id, subject: modifying_user, assignee: assigned)
      end
    end
  end

  # Internal: Create an unassignment event when an assignee is removed.
  #
  # unassigned - The User who was unassigned
  #
  # Returns nothing
  def trigger_unassigned_event(unassigned)
    return unless unassigned

    if GitHub.context[:hide_staff_user] && GitHub.guard_audit_log_staff_actor?
      subject = User.staff_user
    else
      subject = modifying_user
    end
    ignore_duplicate_records do
      GitHub::SchemaDomain.allowing_cross_domain_transactions do
        events.create(event: "unassigned", actor_id: unassigned.id, subject: subject, assignee: unassigned)
      end
    end
  end

  # DEPRECATED: create an "unassigned" event if the issue was just assigned to
  # somebody else.
  def create_unassigned_event
    return unless assignee_id_was

    removed_assignee = User.find_by_id assignee_id_was
    trigger_unassigned_event(removed_assignee)
  end

  # Internal: Keeps track of the users being destroyed. We won't call
  # `trigger_unassigned_event` to avoid a possible race condition.
  #
  # Returns an array.
  def users_being_destroyed
    @users_being_destroyed ||= []
  end

  private def plan_assignee_limit_enabled?
    # this feature flag is to assist with a dark-ship rollout & to provide a safe rollback
    # the `plans_munich` flag will be also be required to enable the new private repo assignee limits on a per-org basis
    GitHub.flipper[:plan_assignee_limit].enabled?
  end

  private def assignment_length_under_limit
    return if plan_assignee_limit_enabled? && repository.nil?

    limit = plan_assignee_limit_enabled? ? assignee_limit : DEFAULT_ASSIGNEE_LIMIT

    # issues with multiple assigness can be moved to plans
    # that support different limits. In that case we only run this validation
    # if the assignees are being changed.
    if assignees.any?(&:changed?) && assignments.length > limit
      errors.add(:assignees, "You may not add more than #{limit} assignee(s)")
    end
  end
end
