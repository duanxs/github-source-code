# rubocop:disable Style/FrozenStringLiteralComment

class Issue
  module MysqlSearch
    SUPPORTED_FIELDS = [
      :assignee, :author, :is, :label, :milestone, :sort
    ]

    def self.supported?(query)
      query = ::Search::Queries::IssueQuery.coerce(query)

      has_multiple_labels = query.count { |component| component.is_a?(Array) && component.first == :label } > 1
      return false if has_multiple_labels

      query.all? do |component|
        if !component.is_a?(Array)
          # fulltext search
          false
        elsif component[0] == :no && %w(milestone assignee).include?(component[1])
          # no:milestone, no:assignee
          true
        elsif component[0] == :label && component[2]
          # -label:bug goes to es
          false
        elsif component[0] == :sort && component[1].start_with?("interactions-")
          # sort:interactions-desc, sort:interactions-asc goes to es
          false
        elsif component[0] == :is && component[1] == "draft"
          # draft status is only indexed in es
          false
        else
          SUPPORTED_FIELDS.include?(component.first)
        end
      end
    end

    # Runs a search on the database given a `query`.
    #
    # Returns a Hash with the following values:
    #
    #   hash[:issues]       - The paginated issues results array.
    #   hash[:open_count]   - The number of open issues for this query.
    #   hash[:closed_count] - The total number of closed issues for this query.
    #
    # :force_pulls is deprecated, please use `:force_type` instead
    def self.search(repo:, query:, current_user: nil, remote_ip: nil, user_session: nil, page: 1, per_page: 25, force_pulls: false, force_type: nil, show_spam_to_staff: false, initial_scope: nil)
      force_type = :pull_requests if force_pulls && force_type.nil?

      query = ::Search::Queries::IssueQuery.coerce(query)
      scope = partial_query_scope(repo: repo, current_user: current_user, initial_scope: initial_scope, query: query, force_type: force_type, show_spam_to_staff: show_spam_to_staff)

      # This uses the unsorted scope -- when counting we don't care about the sorting
      open_count = count_by_state(scope, state: "open")
      closed_count = count_by_state(scope, state: "closed")

      scope = sort_scope(query, scope)

      filter, scope = state_scope(query, scope)

      pagination_options = { per_page: per_page, page: page }

      case filter
      when "open"
        pagination_options[:total_entries] = open_count.to_i
      when "closed"
        pagination_options[:total_entries] = closed_count.to_i
      when "all"
        pagination_options[:total_entries] = open_count.to_i + closed_count.to_i
      end

      if scope.is_a?(ActiveRecord::NullRelation)
        scope.paginate(pagination_options)
      else
        pagination_scope = scope.select(:id).paginate(pagination_options)

        scope = sort_scope(query, Issue.where("`issues`.`id` IN (SELECT * FROM (?) subquery_for_limit)", pagination_scope))

        # Copy over pagination attributes
        scope = scope.extending(WillPaginate::ActiveRecord::RelationMethods)
        scope = scope.per_page(per_page)
        scope.current_page = pagination_scope.current_page
        scope.total_entries = pagination_scope.total_entries
      end

      {
        open_count: open_count,
        closed_count: closed_count,
        issues: scope,
      }
    end

    def self.query_scope(repo:, current_user: nil, initial_scope: nil, query:, force_type: nil, show_spam_to_staff: false)
      query = ::Search::Queries::IssueQuery.coerce(query)
      scope = partial_query_scope(
        repo: repo,
        current_user: current_user,
        initial_scope: initial_scope,
        query: query,
        force_type: force_type,
        show_spam_to_staff: show_spam_to_staff,
      )

      scope = sort_scope(query, scope)

      state_scope(query, scope).last
    end

    def self.partial_query_scope(repo:, current_user: nil, initial_scope: nil, query:, force_type: nil, show_spam_to_staff: false)
      query = ::Search::Queries::IssueQuery.coerce(query)
      scope = initial_scope || repo.issues

      scope = type_scope(query, scope, repo, force_type: force_type)
      scope = merge_state_scope(query, scope)
      scope = author_scope(query, scope, current_user)
      scope = assignee_scope(query, scope, current_user)
      scope = label_scope(repo, query, scope)
      scope = mention_scope(repo, query, scope, current_user)
      scope = milestone_scope(repo, query, scope)
      scope = no_scope(query, scope)

      scope.filter_spam_for(current_user, show_spam_to_staff: show_spam_to_staff)
    end

    # Internal: Determine the number of issues in the given scope that have the
    # given state.
    #
    # scope - An ActiveRecord::Relation for a collection of Issues.
    # state - The String issue state ("open" or "closed") (default: "open").
    #
    # Returns a Integer.
    def self.count_by_state(scope, state: "open")
      # The scope may or may not include a GROUP BY clause. (If the scope
      # filters by label, then it will include a GROUP BY clause.) If the scope
      # includes a GROUP BY clause, then fetching the count will return a Hash.
      # Otherwise, it will just return a Integer.
      count_results = scope.where("issues.state" => state).count("DISTINCT issues.id")
      if count_results.kind_of?(Hash)
        count_results.values.sum
      else
        count_results
      end
    end

    def self.basic_listing?(components)
      components.all? do |c|
        c.is_a?(Array) && [:is, :state, :type].include?(c.first)
      end
    end

    def self.get_state(components)
      is = components.select do |c|
        c.is_a?(Array) && (c.first == :is || c.first == :state)
      end

      state = is.find do |c|
        c.second == "open" || c.second == "closed"
      end.try(:second)

      return state unless state.nil?
      "all"
    end

    # Internal: If feature is enabled and user input @me, return the currently logged
    # in user, otherwise use the input to find user.
    #
    # username_input - either the username to search for, or @me
    # current_user - currently logged in user
    #
    # Returns a User
    def self.derive_user_from_input(username_input, current_user)
      if Search::Query::MACRO_ME.casecmp?(username_input)
        current_user
      else
        User.find_by_login(username_input)
      end
    end

    # is:open, state:open, etc.
    def self.state_scope(components, scope)
      case get_state(components)
      when "open"
        ["open", scope.where(state: "open")]
      when "closed"
        ["closed", scope.where(state: "closed")]
      else
        ["all", scope]
      end
    end

    # is:merged, is:unmerged
    def self.merge_state_scope(components, scope)
      is = components.select do |c|
        c.is_a?(Array) && c.first == :is
      end

      merge_state = is.find do |c|
        c.second == "merged" || c.second == "unmerged"
      end.try(:second)

      case merge_state
      when "merged"
        scope.joins(:pull_request).where("pull_requests.merged_at IS NOT NULL")
      when "unmerged"
        scope.joins(:pull_request).where("pull_requests.merged_at IS NULL")
      else
        scope
      end
    end

    def self.get_type(components)
      is = components.select do |c|
        c.is_a?(Array) && (c.first == :is || c.first == :type)
      end
      is.find { |c| c.second == "issue" || c.second == "pr" }.try(:second)
    end

    # is:issue, type:issue, etc.
    def self.type_scope(components, scope, repo, force_type: nil)
      return scope.with_pull_requests if force_type == :pull_requests
      return scope.without_pull_requests if force_type == :issues

      # If issues are disabled, never show issues.
      return scope.with_pull_requests if !repo.has_issues

      case get_type(components)
      when "pr"
        scope.with_pull_requests
      when "issue"
        scope.without_pull_requests
      else
        scope
      end
    end

    # sort:created-desc, etc.
    def self.sort_scope(components, scope)
      sort = components.select { |c| c.is_a?(Array) && c.first == :sort }.flatten.try(:second)
      # github/github#34432
      return scope.order("issues.id DESC") if !sort

      if sort.starts_with?("reactions")
        sort_by_reactions(sort, scope)
      else
        field, direction = sort.split("-")

        scope.sorted_by(field, direction)
      end
    end

    def self.sort_by_reactions(sort, scope)
      # using ActiveRecord::Base.sanitize instead of parameters because AR
      # doesn't allow parameters .joins().
      join_statement = <<-SQL
        LEFT OUTER JOIN reactions
        ON reactions.subject_id = issues.id
        AND reactions.subject_type = #{Reaction.connection.quote(scope.klass.name)}
      SQL

      # e.g., reactions, reactions-desc
      if matches = /\Areactions(?:-(asc|desc|))?\z/i.match(sort)
        direction = matches[1] || "desc"
      else # e.g., reactions-smile, reactions-smile-asc
        matches = /\Areactions-(.+?)(?:-(asc|desc|))?\z/i.match(sort)
        reaction = matches[1].downcase
        direction = matches[2] || "desc"

        join_statement += <<-SQL
          AND reactions.content = #{Reaction.connection.quote(reaction)}
        SQL
      end

      sql_direction = direction.downcase == "asc" ? "ASC" : "DESC"
      order_statement = Arel.sql("COUNT(reactions.subject_id) #{sql_direction}")

      scope.joins(join_statement).group("issues.id").order(order_statement)
    end

    # author:holman, etc.
    def self.author_scope(components, scope, current_user)
      author_component = components.select { |c| c.is_a?(Array) && c.first == :author }.flatten
      author = author_component.try(:second)
      return scope if !author

      if m = author.match(%r{\Aapp/(.+)})
        user = Bot.find_by_slug(m[1])
      else
        user = derive_user_from_input(author, current_user)
      end

      return scope.none if !user

      # -author:holman
      return scope.where(["issues.user_id != ?", user.id]) if author_component[2]

      scope.where(user_id: user.id)
    end

    # assignee:login, etc.
    def self.assignee_scope(components, scope, current_user)
      # assignee_component can look like:
      #
      # [:assignee, "login"] when query is "assignee:login"
      # [:assignee, "login", true] when query is "-assignee:login"
      assignee_component = components.detect { |c| c.is_a?(Array) && c.first == :assignee }
      return scope unless assignee_component

      user_login     = assignee_component[1]
      wants_assigned = !assignee_component[2]

      return scope unless user_login

      # api-related assigned wildcard
      return scope.assigned if user_login == "*"

      user = derive_user_from_input(user_login, current_user)
      return scope.none unless user

      if wants_assigned
        # Find all issues assigned to the specified user.
        scope.assigned_to(user)
      else
        # Find all issues that are *not* assigned to the specified user.

        # Use ActiveRecord::Base.sanitize instead of parameters because
        # ActiveRecord doesn't allow parameters in .joins().
        sanitized_assignee_id = Assignment.connection.quote(user.id)

        join = <<-SQL
          LEFT OUTER JOIN `assignments` ON
          `assignments`.`issue_id`    = `issues`.`id` AND
          `assignments`.`assignee_id` = #{sanitized_assignee_id}
        SQL

        scope.joins(join).where(assignments: { assignee_id: nil })
      end
    end

    # label:bug, etc.
    def self.label_scope(repo, components, scope)
      label_components = components.select { |c| c.is_a?(Array) && c.first == :label }

      label_component = label_components.first
      label_name = label_component.try(:second)
      return scope if !label_name

      # If this is an array of labels, pass it through to Issue#labeled
      if label_name.kind_of?(Array) || label_name.kind_of?(String)
        return scope.labeled(label_name)
      end

      scope.labeled(label)
    end

    # mentions:holman, etc.
    def self.mention_scope(repo, components, scope, current_user)
      mention_components = components.select { |c| c.is_a?(Array) && c.first == :mentions }

      mention_component = mention_components.first
      mention_name = mention_component.try(:second)
      return scope if !mention_name

      user = derive_user_from_input(mention_name, current_user)
      return scope.none if !user

      return scope.mentioning(user)
    end

    # milestone:"issues three", etc.
    def self.milestone_scope(repo, components, scope)
      milestone_component = components.select { |c| c.is_a?(Array) && c.first == :milestone }.flatten
      milestone_name = milestone_component.try(:second)
      return scope if !milestone_name

      # api-related milestone wildcard
      return scope.with_any_milestone if milestone_name == "*"

      milestone = repo.milestones.find_by_title(milestone_name)
      return scope.none if !milestone

      # -milestone:v3
      return scope.where(["milestone_id != ? OR milestone_id IS NULL", milestone.id]) if milestone_component[2]

      scope.for_milestone(milestone)
    end

    # Specifically handle two use cases:
    #
    #   no:milestone
    #   no:assignee
    #
    # Because no:label involves a join table, it becomes unwieldy to support
    # that directly in MySQL.
    def self.no_scope(components, scope)
      no_component = components.select { |c| c.is_a?(Array) && c.first == :no }

      no_component.each do |component|
        filter = component.try(:second)
        return scope if !filter

        # no milestones
        scope = scope.with_no_milestone if filter == "milestone"

        # no assignee
        scope = scope.unassigned if filter == "assignee"
      end

      scope
    end
  end
end
