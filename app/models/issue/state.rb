# rubocop:disable Style/FrozenStringLiteralComment

class Issue
  module State
    include Scientist

    States = %w(open closed)

    def open?
      state == "open"
    end

    def closed?
      state == "closed"
    end

    def toggle(toggler = nil)
      open? ? close(toggler) : open(toggler)
    end

    # Reopen the issue.
    #
    #   opener     - The User who opened the issue. May be nil to disable
    #                creation of an open event
    #   attributes - Hash of optional attributes to update with `state`.
    #
    # Returns true when the issue is successfully opened
    # Returns false-y if the issue cannot be opened
    # Returns false-y if the issue is already open
    def open(opener = user, attributes = {})
      return if open?

      opener = opener.try(:bot) || opener

      if pull_request? && !pull_request.reopenable?
        pull_request.set_not_reopenable_error
        return false
      end

      return if !reopenable_by?(opener)
      return if !update(attributes.merge(state: "open"))

      events.create(event: "reopened", actor: opener) if opener
      pull_request.reopened opener if pull_request?

      if milestone && milestone.prioritizable?
        milestone.prioritize_issue!(self)
      end

      after_open(opener, attributes)
    end

    alias :reopen! :open

    # Mark the issue as closed, possibly by a given user and commit.
    #
    #   closer     - The User who closed the issue.
    #   attributes - Hash of optional attributes to update with `state`.
    #                May contain a :commit hash containing :id and :repository
    #                keys
    #                May contain a :pr key
    #
    # Returns true when the issue is successfully closed
    # Returns false-y if the issue cannot be closed
    # Returns false-y if the issue is already closed
    def close(closer = user, attributes = nil)
      if closed?
        if pull_request? && pull_request.merged?
          report_to_failbot("already closed")
        end
        return
      end

      if !closeable_by?(closer)
        if pull_request? && pull_request.merged?
          report_to_failbot("not closeable by actor")
        end
        return
      end

      attributes ||= {}
      commit = attributes.delete(:commit) || {}
      pr_id  = attributes.delete(:pr)

      return if commit[:id] && already_closed_by_commit?(commit[:id])

      updated = GitHub::SchemaDomain.allowing_cross_domain_transactions do
        update(attributes.merge(state: "closed"))
      end
      if !updated
        if pull_request? && pull_request.merged?
          report_to_failbot("failed to update state")
        end
        return
      end

      closing_data = {}

      if commit[:id]
        closing_data[:commit_id]         = commit[:id]
        closing_data[:commit_repository] = commit[:repository]
      elsif pr_id
        closing_data[:referencing_issue_id] = pr_id
      end

      after_close(closer, closing_data)
      pull_request.after_close(closer) if pull_request?

      true
    end

    alias :close! :close

    # Internal: Handle any processing for after an Issue closes
    #
    #   closer - User who closed the issue, or nil to
    #            not create a closed event
    #   attributes - Extra data used to trace the closing to a source
    #                (either commit or PR)
    #
    # Returns true
    #  (none of these 'after close' events are really considered a failure)
    def after_close(closer, attributes)
      if closer
        ignore_duplicate_records do
          GitHub::SchemaDomain.allowing_cross_domain_transactions do
            events.create!(
              attributes.merge(
                event: "closed",
                actor: closer,
              ),
            )
          end
        end
        remove_instance_variable(:@closed_by) if instance_variable_defined?(:@closed_by)
      end

      if milestone
        milestone.deprioritize_dependent(self)
      end

      update_repo_community_profile(changed_labels: labels)

      true
    end

    def after_open(opener, attributes)
      update_repo_community_profile(changed_labels: labels)

      true
    end

    # Add a new comment to this issue and create issue events if the comment
    # marks the issue as a duplicate and tracking "marked_as_duplicate" events is
    # enabled. Create records within a transaction so that if one part fails we
    # roll back to the original issue state.
    #
    # commenter    - The User who is commenting
    # comment_body - A String of text
    #
    # Returns the IssueComment object.
    def create_comment(commenter, comment_body)
      comment = comments.build(body: comment_body)
      comment.user = commenter
      comment.repository = repository
      duplicate_issue_events = build_duplicate_issue_events(commenter, comment.duplicate_issues)

      GitHub::SchemaDomain.allowing_cross_domain_transactions do
        transaction do
          Attachment.transaction do
            comment.save!
            duplicate_issue_events.each(&:save!)
          end
        end
      end

      comment

    rescue ActiveRecord::RecordInvalid, ActiveRecord::RecordNotUnique => exception
      comment
    end

    # Add a new comment to this issue, create issue events if the comment marks
    # the issue as a duplicate and tracking "marked_as_duplicate" events is
    # enabled, and change the state to closed. Create records within a
    # transaction so that if one part fails we roll back to the original issue state.
    #
    # closer       - The User who is closing this issue
    # comment_body - A String of text
    #
    # Returns the newly created IssueComment object or nil.
    def comment_and_close(closer, comment_body)
      valid = true
      duplicate_issue_events = []

      unless comment_body.blank?
        comment = comments.build(body: comment_body)
        comment.user       = closer
        comment.repository = repository
        duplicate_issue_events = build_duplicate_issue_events(closer, comment.duplicate_issues)
      end

      GitHub::SchemaDomain.allowing_cross_domain_transactions do
        transaction do
          Attachment.transaction do
            valid = comment && comment.save
            duplicate_issue_events.each(&:save)
            close(closer)
          end
        end
      end

      comment
    end

    # Add a new comment to this issue, create issue events if the comment marks
    # the issue as a duplicate if tracking "marked_as_duplicate" events is enabled,
    # and change the state to open. Create records within a transaction so that
    # if one part fails we roll back to the original issue state.
    #
    # opener       - The User who is opening this issue
    # comment_body - Comment body as a String
    #
    # Returns the newly created IssueComment object or nil.
    def comment_and_open(opener, comment_body)
      valid = true
      duplicate_issue_events = []

      unless comment_body.blank?
        comment = comments.build(body: comment_body)
        comment.user       = opener
        comment.repository = repository
        duplicate_issue_events = build_duplicate_issue_events(opener, comment.duplicate_issues)
      end

      GitHub::SchemaDomain.allowing_cross_domain_transactions do
        transaction do
          Attachment.transaction do
            valid &&= comment && comment.save
            duplicate_issue_events.each(&:save)
            open(opener)
          end
        end
      end

      comment
    end

    def async_closeable_by?(other_user)
      return Promise.resolve(false) unless other_user
      async_repository.then do |repo|
        repo.async_owner.then do |owner|
          async_user.then do |user|
            Promise.all([async_pull_request, async_repository]).then do |pull, repo|
              Platform::Loaders::Permissions::BatchAuthorize.load(
                action: pull ? :close_pull_request : :close_issue,
                actor: other_user,
                subject: pull ? pull : self,
              ).then do |decision|
                decision.allow?
              end
            end
          end

        end
      end
    end

    # Does user have permission to close this issue?
    #
    # Returns true when user opened the issue or has write access
    # to the repository
    def closeable_by?(other_user)
      async_closeable_by?(other_user).sync
    end

    def async_closed_by
      Platform::Loaders::ClosedIssueEvents.load(self.id).then do |issue_events|
        issue_events&.last&.async_actor
      end
    end

    # The user that closed the issue, or nil when that information is not
    # available.
    def closed_by
      return @closed_by if defined?(@closed_by)

      @closed_by = async_closed_by.sync
    end

    # Fallback on the ghost user when the closer has been deleted or the closing
    # event doesn't exist.
    # See User.ghost for more.
    #
    # Returns a User, or nil if the Issue is open.
    def safe_closed_by
      return nil if open?

      closed_event = events.reverse.find { |e| e.event == "closed" }

      closed_event.present? ? closed_event.safe_actor : User.ghost
    end

    # Did the given commit close this issue by way of a reference?
    #
    # commit - Commit
    #
    # Returns Boolean.
    def closed_by_commit?(commit)
      if commit && closing_event = events.closes.last
        closing_event.commit && closing_event.commit.oid == commit.oid
      end
    end

    # Does user have permissions to reopen this issue?
    #
    # Returns false if this issue is actually a PR and the PR isn't reopenable
    # (unless you pass a true issue_only value)
    #
    # Returns true when user last closed the issue or has write access
    # to the repository
    def reopenable_by?(other_user, issue_only: false)
      check_pull_request = !issue_only && pull_request?

      GitHub.dogstats.time("reopenable_by", tags: ["subject:#{check_pull_request ? 'pull_request' : 'issue'}"]) do
        return false unless other_user
        return false if check_pull_request && !pull_request.reopenable?
        ::Permissions::Enforcer.authorize(
          action: pull_request? ? :reopen_pull_request : :reopen_issue,
          actor: other_user,
          subject: pull_request? ? pull_request : self
        ).allow?
      end
    end

    # Storing times in PDT is _not_ ideal, but we need to keep things consistent.
    def set_state
      if self.state.to_s =~ /close/
        self.closed_at ||= Time.now
        self.state       = States[1]
      else
        self.closed_at = nil
        self.state     = States[0]
      end
    end

    # Public: Returns a list including IssueEvent and DuplicateIssue records that are potentially
    # not persisted.
    #
    # user - the current user, a User instance
    # referenced_issues - a list of Issue instances; the user should have indicated this issue was a
    #                     duplicate of these issues
    #
    # Returns an array.
    def build_duplicate_issue_events(user, referenced_issues)
      return [] unless self.can_mark_as_duplicate?(user)

      related_dupes = DuplicateIssue.marked_as_duplicate.with_duplicate_issue(id).
        with_canonical_issue(referenced_issues).to_a
      eligible_issues = referenced_issues.reject do |canonical_issue|
        marked_as_duplicate_of?(canonical_issue, duplicate_issues: related_dupes)
      end

      eligible_issues.flat_map do |canonical_issue|
        event = events.marked_as_duplicates.
          build(actor_id: user.id, repository_id: repository_id, subject_type: self.class.name,
                subject_id: canonical_issue.id)

        dupe_issue = DuplicateIssue.find_or_build_for(issue: self, canonical_issue: canonical_issue,
                                         user: user)

        [event, dupe_issue]
      end
    end

    # Public: Returns true if this Issue is marked as a duplicate of the given Issue.
    #
    # other_issue - an Issue instance
    # duplicate_issues - an optional list of DuplicateIssue records that can be passed for
    #                    performance reasons, to be checked instead of querying the database
    #
    # Returns a Boolean.
    def marked_as_duplicate_of?(other_issue, duplicate_issues: nil)
      if duplicate_issues
        dupe_issue = duplicate_issues.
          detect { |dupe| dupe.issue_id == id && dupe.canonical_issue == other_issue }
        dupe_issue.present?
      else
        DuplicateIssue.marked_as_duplicate.exists?(issue_id: id, canonical_issue_id: other_issue)
      end
    end

    private


    # Private: Report to Failbot when the #close method is called on a merged
    # pull request and returns without closing the issue.
    #
    # This was added to investigate cases in which a pull request is merged and
    # not subsequently closed.
    #
    # TODO: remove once we've determined the cause of the bug.
    #
    def report_to_failbot(message)
      boom = PullRequest::PRMergedAndNotClosed.new(message)
      boom.set_backtrace(caller)
      Failbot.report(boom, pull_request_id: pull_request&.id, app: "github-pull-requests")
    end
  end
end
