# frozen_string_literal: true

# Public: Used to fetch issues and pull requests a user has somehow interacted with since
# the specified time.
class Issue::RecentInteractions
  EVENT_ACTIONS = %w[reopened deployed assigned referenced].freeze

  COMMENT_INTERACTION = "commented"
  RECEIVED_COMMENT_INTERACTION = "received_comment"
  COMMENT_EDITED_INTERACTION = "comment_edited"
  RECEIVED_COMMENT_EDITED_INTERACTION = "received_comment_edited"
  OPENED_INTERACTION = "authored"
  ASSIGNED_INTERACTION = "assigned"
  REVIEW_REQUESTED_INTERACTION = "review_requested"
  REVIEW_RECEIVED_INTERACTION = "review_received"

  # Public: Construct a new instance of Issue::RecentInteractions.
  #
  #                      user - a User instance
  #                     since - a DateTime in the past
  #                     types - a list of what kinds of records should be returned; valid values:
  #                             :issue, :pull_request
  #           organization_id - optional Integer database ID for an Organization, if activity should
  #                             be filtered to only include that within the specified org
  # excluded_organization_ids - optional list of Integer database IDs for Organizations whose issues
  #                             and pull requests should be excluded from the results; if
  #                             `organization_id` is given but also included in this list, no
  #                             results will be returned
  def initialize(user, since:, types: nil, organization_id: nil, excluded_organization_ids: nil)
    @user = user
    @since = since
    @types = types || [:issue, :pull_request]
    @organization_id = organization_id
    @excluded_organization_ids = excluded_organization_ids || []
  end

  # Public: Get a list of issues and/or pull requests the user has interacted with.
  #
  # limit - how many results to return
  #
  # Returns an Array of RecentInteraction instances.
  def fetch(limit:)
    return [] if @types.empty?

    sql_results = run_sql_queries
    return [] if sql_results.empty?

    interactions_by_issue_id = get_interactions_by_issue_id(sql_results)
    issues = get_issues(ids: interactions_by_issue_id.keys).
      sort_by { |issue| interactions_by_issue_id[issue.id][:occurred_at] }.
      reverse[0...limit]

    user_by_issue_ids = get_users_by_issue_ids(issues: issues,
                                               interactions_by_issue_id: interactions_by_issue_id)

    issues.map do |issue|
      interactable = issue.pull_request || issue
      data = interactions_by_issue_id[issue.id]

      # exclude recent interactions with spammy commenters
      commenter = user_by_issue_ids[issue.id]
      next if commenter&.spammy?

      RecentInteraction.new(interactable, interaction: data[:interaction],
                            occurred_at: data[:occurred_at], commenter: commenter,
                            comment_id: data[:comment_id])
    end.compact
  end

  private

  def get_issues(ids:)
    issues = Issue.open_issues.includes(issue_query_includes).where(id: ids)

    # Filter to pull requests only unless issues should be included:
    issues = issues.with_pull_requests unless @types.include?(:issue)

    # Filter to issues only unless pull requests should be included:
    issues = issues.without_pull_requests unless @types.include?(:pull_request)

    issues.select { |issue| issue_readable_by_user?(issue) }
  end

  def get_users_by_issue_ids(issues:, interactions_by_issue_id:)
    user_ids_by_issue_id = {}
    issues.map do |issue|
      interactable = issue.pull_request || issue
      interaction = interactions_by_issue_id[issue.id]
      user_ids_by_issue_id[issue.id] = interaction[:user_id]
    end

    user_ids = user_ids_by_issue_id.values.compact.uniq
    users_by_id = User.where(id: user_ids).map { |user| [user.id, user] }.to_h
    users_by_issue_id = {}
    user_ids_by_issue_id.each do |issue_id, user_id|
      users_by_issue_id[issue_id] = users_by_id[user_id]
    end

    users_by_issue_id
  end

  def issue_readable_by_user?(issue)
    repo = issue.repository
    return false unless repo
    return false if repo.user_hidden?
    interactable = issue.pull_request || issue
    return false if interactable.hide_from_user?(@user)
    return false if repo.disabled_at.present?
    repo.readable_by?(@user)
  end

  def issue_query_includes
    includes = [:repository, { repository: :owner }]
    includes << :pull_request if @types.include?(:pull_request)
    includes
  end

  def run_sql_queries
    all_results = []

    issue_event_results = run_sql_query(issue_events_sql, query_key: "issue-events",
                                        query_args: issue_event_query_args)
    all_results.concat issue_event_results

    review_request_results = run_sql_query(review_requests_sql, query_key: "review-requests",
                                           query_args: review_request_query_args)
    all_results.concat review_request_results

    review_received_results = run_sql_query(reviews_received_sql, query_key: "reviews-received",
                                            query_args: review_received_query_args)

    all_results.concat review_received_results

    all_results.concat created_comment_results

    if (issue_ids = user_open_issue_ids).any?
      all_results.concat received_comment_results(issue_ids)
    end

    opened_issue_results = run_sql_query(opened_issues_sql, query_key: "opened-issues",
                                         query_args: opened_issue_query_args)
    all_results.concat opened_issue_results

    assigned_issue_results = run_sql_query(assigned_issues_sql, query_key: "assigned-issues",
                                           query_args: assigned_issue_query_args)
    all_results.concat assigned_issue_results

    all_results
  end

  def created_comment_results
    augment_issue_comments_with_user_content_edits(
      run_sql_query(created_issue_comments_sql,
                    query_key: "created-issue-comments",
                    query_args: created_issue_comment_query_args,
      ),
      edit_interaction_type: COMMENT_EDITED_INTERACTION,
    )
  end

  def received_comment_results(issue_ids)
    augment_issue_comments_with_user_content_edits(
      run_sql_query(
        received_issue_comments_sql,
        query_key: "received-issue-comments",
        query_args: received_issue_comment_query_args(issue_ids),
      ),
      edit_interaction_type: RECEIVED_COMMENT_EDITED_INTERACTION,
    )
  end


  # Simulate the LEFT OUTER JOIN/CASE logic that used to be possible between
  # issue_comments and user_content_edits.  See
  # https://github.com/github/github/blob/8bcb38ccc7fafa679c273100e2a097992f86d88f/app/models/issue/recent_interactions.rb#L250-L260
  # For every issue_comment row, if there is a matching result in
  # user_content_edits, overwrite selected fields with the uce data.
  def augment_issue_comments_with_user_content_edits(issue_comment_rows, edit_interaction_type:)
    issue_comment_ids = issue_comment_rows.map { |row| row[4] }
    edits_by_user_content_id = UserContentEdit.where(user_content_type: "IssueComment", user_content_id: issue_comment_ids)
      .order("edited_at ASC")
      .pluck(:edited_at, :editor_id, :user_content_id)
      .group_by { |edit| edit[2] }
    issue_comment_rows.each_with_object([]) do |row, result|
      if edits = edits_by_user_content_id[row[4]]
        edits.each do |edit|
          edited_row = row.dup
          edited_row[1] = edit_interaction_type # CASE WHEN user_content_edits.editor_id IS NOT NULL THEN :edit_interaction
          edited_row[2] = edit[0]               # CASE WHEN user_content_edits.editor_id IS NOT NULL THEN user_content_edits.edited_at
          edited_row[3] = edit[1]               # CASE WHEN user_content_edits.editor_id IS NOT NULL THEN user_content_edits.editor_id
          result << edited_row
        end
      else
        result << row
      end
    end.slice(0, 20)
  end

  def run_sql_query(sql_query, query_key:, query_args: {})
    sql = Issue.github_sql.new
    sql.add(sql_query, query_args)

    start = Time.now
    results = sql.run.results
    elapsed_ms = (Time.now - start) * 1_000

    timing_key = if @organization_id
      "recent-activity.#{query_key}.in-org"
    else
      "recent-activity.#{query_key}"
    end
    GitHub.dogstats.timing(timing_key, elapsed_ms)

    results
  end

  # The user was assigned an issue or pull request
  def assigned_issues_sql
    query = <<-SQL
      SELECT assignments.issue_id, :interaction, assignments.updated_at
      FROM assignments
    SQL

    if @organization_id || @excluded_organization_ids.any?
      query += <<-SQL
        INNER JOIN issues
        ON assignments.issue_id = issues.id
        INNER JOIN repositories
        ON issues.repository_id = repositories.id
      SQL
    end

    query += <<-SQL
      WHERE assignments.assignee_id = :user_id
      AND (assignments.assignee_type = 'User' OR assignments.assignee_type IS NULL)
      AND assignments.updated_at >= :since
    SQL

    if @organization_id
      query += <<-SQL
        AND repositories.owner_id = :organization_id
      SQL
    end

    if @excluded_organization_ids.any?
      query += <<-SQL
        AND repositories.owner_id NOT IN :excluded_organization_ids
      SQL
    end

    query += <<-SQL
      ORDER BY assignments.id DESC
      LIMIT 20
    SQL
    query
  end

  # The user opened an issue or pull request
  def opened_issues_sql
    query = <<-SQL
      SELECT issues.id, :interaction, issues.created_at
      FROM issues
    SQL

    if @organization_id || @excluded_organization_ids.any?
      query += <<-SQL
        INNER JOIN repositories
        ON repositories.id = issues.repository_id
      SQL
    end

    query += <<-SQL
      WHERE issues.user_id = :user_id
      AND issues.created_at >= :since
      AND issues.state = 'open'
    SQL

    if @organization_id
      query += <<-SQL
        AND repositories.owner_id = :organization_id
      SQL
    end

    if @excluded_organization_ids.any?
      query += <<-SQL
        AND repositories.owner_id NOT IN :excluded_organization_ids
      SQL
    end

    query += <<-SQL
      ORDER BY issues.created_at DESC
      LIMIT 20
    SQL
    query
  end

  # The user opened an issue or pull request that got a comment
  def received_issue_comments_sql
    query = <<-SQL
      SELECT issue_comments.issue_id,
             :interaction,
             issue_comments.updated_at,
             issue_comments.user_id,
             issue_comments.id
      FROM issue_comments
    SQL

    if @organization_id || @excluded_organization_ids.any?
      query += <<-SQL
        INNER JOIN repositories
        ON repositories.id = issue_comments.repository_id
      SQL
    end

    query += <<-SQL
      WHERE issue_comments.updated_at >= :since
      AND issue_comments.issue_id IN :issue_ids
    SQL

    if @organization_id
      query += <<-SQL
        AND repositories.owner_id = :organization_id
      SQL
    end

    if @excluded_organization_ids.any?
      query += <<-SQL
        AND repositories.owner_id NOT IN :excluded_organization_ids
      SQL
    end

    query += <<-SQL
      ORDER BY issue_comments.updated_at DESC
      LIMIT 20
    SQL
    query
  end

  # The user's pull request was reviewed
  def reviews_received_sql
    query = <<-SQL
      SELECT issues.id, :interaction, pull_request_reviews.submitted_at
      FROM pull_request_reviews
      INNER JOIN issues
      ON pull_request_reviews.pull_request_id = issues.pull_request_id
    SQL

    if @organization_id || @excluded_organization_ids.any?
      query += <<-SQL
        INNER JOIN pull_requests
        ON pull_request_reviews.pull_request_id = pull_requests.id
        INNER JOIN repositories
        ON pull_requests.repository_id = repositories.id
      SQL
    end

    query += <<-SQL
      WHERE issues.user_id = :user_id
      AND pull_request_reviews.submitted_at >= :since
    SQL

    # Add an extra condition in the case that we are joining with pull_requests
    # The idea is to get an extra restriction on issues, thus enabling a more
    # efficient query execution plan. Experiments indicate that this improves
    # response times significantly, especially at the higher percentiles.
    if @organization_id || @excluded_organization_ids.any?
      query += <<-SQL
        AND issues.repository_id = pull_requests.repository_id
      SQL
    end

    if @organization_id
      query += <<-SQL
        AND repositories.owner_id = :organization_id
      SQL
    end

    if @excluded_organization_ids.any?
      query += <<-SQL
        AND repositories.owner_id NOT IN :excluded_organization_ids
      SQL
    end

    query += <<-SQL
      ORDER BY pull_request_reviews.submitted_at DESC
      LIMIT 20
    SQL
    query
  end

  # The user was the author of an issue comment
  def created_issue_comments_sql
    query = <<-SQL
      SELECT issue_comments.issue_id,
             :interaction,
             issue_comments.updated_at,
             issue_comments.user_id,
             issue_comments.id
      FROM issue_comments
    SQL

    if @organization_id || @excluded_organization_ids.any?
      query += <<-SQL
        INNER JOIN repositories
        ON repositories.id = issue_comments.repository_id
      SQL
    end

    query += <<-SQL
      WHERE issue_comments.user_id = :user_id
      AND issue_comments.updated_at >= :since
    SQL

    if @organization_id
      query += <<-SQL
        AND repositories.owner_id = :organization_id
      SQL
    end

    if @excluded_organization_ids.any?
      query += <<-SQL
        AND repositories.owner_id NOT IN :excluded_organization_ids
      SQL
    end

    query += <<-SQL
      ORDER BY issue_comments.updated_at DESC
      LIMIT 20
    SQL
    query
  end

  # The user's review was requested on a pull request
  def review_requests_sql
    query = <<-SQL
      SELECT issues.id, :interaction, review_requests.created_at
      FROM review_requests
      INNER JOIN issues
      ON review_requests.pull_request_id = issues.pull_request_id
    SQL

    if @organization_id || @excluded_organization_ids.any?
      query += <<-SQL
        INNER JOIN pull_requests
        ON review_requests.pull_request_id = pull_requests.id
        INNER JOIN repositories
        ON pull_requests.repository_id = repositories.id
      SQL
    end

    query += <<-SQL
      WHERE review_requests.reviewer_id = :user_id
      AND review_requests.reviewer_type = 'User'
      AND review_requests.created_at >= :since
      AND review_requests.dismissed_at IS NULL
    SQL

    if @organization_id
      query += <<-SQL
        AND repositories.owner_id = :organization_id
      SQL
    end

    if @excluded_organization_ids.any?
      query += <<-SQL
        AND repositories.owner_id NOT IN :excluded_organization_ids
      SQL
    end

    query += <<-SQL
      ORDER BY review_requests.id DESC
      LIMIT 20
    SQL
    query
  end

  # The user was the actor in some issue event
  def issue_events_sql
    query = <<-SQL
      SELECT issue_events.issue_id, issue_events.event, issue_events.created_at
      FROM issue_events
    SQL

    if @organization_id || @excluded_organization_ids.any?
      query += <<-SQL
        INNER JOIN repositories
        ON repositories.id = issue_events.repository_id
      SQL
    end

    query += <<-SQL
      WHERE issue_events.actor_id = :user_id
      AND issue_events.created_at >= :since
      AND issue_events.event IN :event_actions
    SQL

    if @organization_id
      query += <<-SQL
        AND repositories.owner_id = :organization_id
      SQL
    end

    if @excluded_organization_ids.any?
      query += <<-SQL
        AND repositories.owner_id NOT IN :excluded_organization_ids
      SQL
    end

    query += <<-SQL
      ORDER BY issue_events.id DESC
      LIMIT 20
    SQL
    query
  end

  def assigned_issue_query_args
    query_args_with_org_ids(user_id: @user.id, since: @since, interaction: ASSIGNED_INTERACTION)
  end

  def opened_issue_query_args
    query_args_with_org_ids(user_id: @user.id, since: @since, interaction: OPENED_INTERACTION)
  end

  def received_issue_comment_query_args(issue_ids)
    query_args_with_org_ids(since: @since, interaction: RECEIVED_COMMENT_INTERACTION,
                            edit_interaction: RECEIVED_COMMENT_EDITED_INTERACTION,
                            issue_ids: issue_ids)
  end

  def created_issue_comment_query_args
    query_args_with_org_ids(user_id: @user.id, since: @since, interaction: COMMENT_INTERACTION,
                            edit_interaction: COMMENT_EDITED_INTERACTION)
  end

  def review_request_query_args
    query_args_with_org_ids(user_id: @user.id, since: @since,
                            interaction: REVIEW_REQUESTED_INTERACTION)
  end

  def review_received_query_args
    query_args_with_org_ids(user_id: @user.id, since: @since,
                            interaction: REVIEW_RECEIVED_INTERACTION)
  end

  def issue_event_query_args
    query_args_with_org_ids(user_id: @user.id, since: @since, event_actions: EVENT_ACTIONS)
  end

  # Private: Decorates the given hash with an :organization_id key if an organization ID was
  # passed to the Issue::RecentInteractions constructor, and an :excluded_organization_ids key
  # if any excluded organization IDs were given.
  #
  # args - a Hash
  #
  # Returns a Hash.
  def query_args_with_org_ids(args)
    args[:organization_id] = @organization_id if @organization_id
    args[:excluded_organization_ids] = @excluded_organization_ids if @excluded_organization_ids.any?
    args
  end

  def user_open_issue_ids
    # Limited to 100 so we can use this in an `IN` clause in another query
    # without having too many IDs in the clause.
    @user.issues.where(state: "open").order("id DESC").limit(100).pluck(:id)
  end

  def get_interactions_by_issue_id(sql_results)
    all_interactions_by_issue_id = sql_results.inject({}) do |hash, row|
      issue_id, interaction, occurred_at, user_id, comment_id = row
      hash[issue_id] ||= []
      hash[issue_id] << { interaction: interaction, occurred_at: occurred_at, user_id: user_id, comment_id: comment_id }
      hash
    end

    # Keep only the latest interaction for a given issue:
    most_recent_interaction_per_issue(all_interactions_by_issue_id)
  end

  def most_recent_interaction_per_issue(all_interactions_by_issue_id)
    all_interactions_by_issue_id.inject({}) do |new_hash, data|
      issue_id, old_hash = data
      new_hash[issue_id] = old_hash.max_by { |hash| hash[:occurred_at] }
      new_hash
    end
  end
end
