# rubocop:disable Style/FrozenStringLiteralComment

module Issue::ProjectsDependency
  extend ActiveSupport::Concern

  included do
    has_many :cards, -> { includes(:project) },
      as: :content,
      class_name: "ProjectCard",
      dependent: :destroy

    has_many :project_columns, -> { includes(:project) },
      through: :cards,
      source: :column

    has_many :projects, through: :cards

    destroy_dependents_in_background :cards
  end

  # ProjectCards related to this issue that are visible to the user
  # Also loads the related Projects and ProjectColumns
  def visible_cards_for(viewer)
    return ProjectCard.none unless readable_by?(viewer)
    scope = associated_cards(only_for_enabled_projects: true).includes(:column)

    if repository.owner.is_a?(User)
      # If the owner is an org, or the owner is a user and the viewer has
      # access to their projects, show the issue's repo's owner's projects.
      visible_account_projects = repository.owner.visible_projects_for(viewer)
      scope = scope.where(projects: { owner_type: "Repository" })
        .or(scope.where(project_id: visible_account_projects))
    end

    scope
  end

  # Public: Return ProjectCards related to this Issue.
  #
  # only_for_enabled_projects - restrict results to cards
  #   associated with projects that are not disabled in any of the three
  #   ways it's possible to disable a project (default false).
  #
  # Returns ProjectCard::ActiveRecord_Relation for the matching cards.
  def associated_cards(only_for_enabled_projects: false)
    is_issue_card = ProjectCard.arel_table[:content_type].eq("Issue")
    matches_issue = ProjectCard.arel_table[:content_id].eq(id)

    matching_issue_condition = is_issue_card.and(matches_issue)

    is_repo_project = Project.arel_table[:owner_type].eq("Repository")
    issue_project_share_owner = Project.arel_table[:owner_id].eq(repository_id)

    # It's a repo project belonging to the same repo as the issue.
    repo_project_condition = is_repo_project.and(issue_project_share_owner)

    matching_project_condition = repo_project_condition

    # Or, it's an org/user project belonging to the org/user that owns the repo
    # that the issue is in.
    is_account_project = Project.arel_table[:owner_type].in(%w(Organization User))
    account_owns_issue_repo = Project.arel_table[:owner_id].eq(repository.owner_id)
    account_project_condition = is_account_project.and(account_owns_issue_repo)

    matching_project_condition = repo_project_condition.or(account_project_condition)

    relation = ProjectCard.joins(:project).
      where(matching_issue_condition).
      where(matching_project_condition)
    if only_for_enabled_projects
      owning_repository_ids = relation.where(projects: {owner_type: "Repository"}).distinct.pluck("projects.owner_id")
      if owning_repository_ids.any?
        owning_repository_owner_ids = Repository.where(id: owning_repository_ids).distinct.pluck(:owner_id)
      else
        owning_repository_owner_ids = []
      end

      # There are 3 types of disabling that could hide a project:
      joins_sql = <<-SQL
        /* cross-schema-domain-query-exempted */
        LEFT OUTER JOIN configuration_entries AS org_level_org_project_configuration_entries ON (
          -- 1. It's an org project, and the org has organization_projects.disable set.
          org_level_org_project_configuration_entries.name = "#{Configurable::DisableOrganizationProjects::KEY}" AND
          projects.owner_type = "Organization" AND
          org_level_org_project_configuration_entries.target_type = "User" AND
          projects.owner_id = org_level_org_project_configuration_entries.target_id
        )
        LEFT OUTER JOIN configuration_entries AS repo_level_repo_project_configuration_entries ON (
          -- 2. It's a repo project, and the repo has projects.disable set.
          repo_level_repo_project_configuration_entries.name = "#{Configurable::DisableRepositoryProjects::KEY}" AND
          projects.owner_type = "Repository" AND
          repo_level_repo_project_configuration_entries.target_type = "Repository" AND
          projects.owner_id = repo_level_repo_project_configuration_entries.target_id
        )
      SQL

      where_sql = <<-SQL
        (org_level_org_project_configuration_entries.value IS NULL OR org_level_org_project_configuration_entries.value = :false) AND
        (repo_level_repo_project_configuration_entries.value IS NULL OR repo_level_repo_project_configuration_entries.value = :false)
      SQL

      if owning_repository_owner_ids.any?
        joins_sql += <<~SQL
          LEFT OUTER JOIN configuration_entries AS org_level_repo_project_configuration_entries ON (
            -- 3. It's a repo project, the repo is owned by an organization,
            --    and that organization has projects.disable set.
            org_level_repo_project_configuration_entries.name = "#{Configurable::DisableRepositoryProjects::KEY}" AND
            projects.owner_type = "Repository" AND
            org_level_repo_project_configuration_entries.target_type = "User" AND
            org_level_repo_project_configuration_entries.target_id IN (#{owning_repository_owner_ids.join(",")})
          )
        SQL
        where_sql += <<~SQL
          AND (org_level_repo_project_configuration_entries.value IS NULL OR org_level_repo_project_configuration_entries.value = :false)
        SQL
      end

      relation.joins(joins_sql).where(where_sql, false: Configuration::FALSE)
    else
      relation
    end
  end


  # Projects related to this issue that are visible to the user.
  def visible_projects_for(viewer)
    Project.where(id: visible_cards_for(viewer).select(:project_id))
  end

  # Public: Projects that this user could add this issue to. Includes projects
  # that this issue is already in.
  #
  # adder - User wanting to add this issue to projects.
  #
  # Returns an array of projects.
  def potential_projects_for(adder, ids: :all)
    scopes = []

    if ids == :all || ids.any?
      # If the user can write to the issue's repository, all of its projects
      # are valid.
      if repository.writable_by?(adder)
        scopes << repository.projects
      end

      # If the issue's repository is owned by a user or org, any of its projects
      # that the user can write to are valid.
      if repository.owner.is_a?(User)
        scopes << repository.owner.writable_projects_for(adder)
      end

      if ids.is_a?(Array)
        scopes = scopes.map { |scope| scope.where(id: ids) }
      end
    end

    scopes.map(&:to_a).flatten.uniq
  end

  def project_workflow_review_triggers?
    projects.joins(:project_workflows)
      .where(project_workflows: { trigger_type: ProjectWorkflow::REVIEW_TRIGGERS })
      .exists?
  end
end
