# frozen_string_literal: true

module Issue::HovercardDependency
  extend ActiveSupport::Concern

  include UserHovercard::SubjectDefinition

  def related_teams
    comments.flat_map(&:mentioned_teams)
  end

  def user_hovercard_parent
    repository
  end

  included do
    define_user_hovercard_context :creator, ->(user, viewer, descendant_subjects:) do
      next unless readable_by?(viewer)

      if user_id == user.id
        first_in = if user.issues.minimum(:id) == self.id
          "(their first ever)"
        elsif user.issues.for_organization(repository.organization_id).minimum(:id) == self.id
          "(their first in @#{repository.organization})"
        elsif user.issues.for_repository(repository_id).minimum(:id) == self.id
          "(their first in #{repository.nwo})"
        end

        message = ["Opened this issue", first_in].compact.join(" ")
        Hovercard::Contexts::Custom.new(message, "issue-opened")
      end
    end
  end
end
