# frozen_string_literal: true

module Issue::PermissionsDependency

  # Public: Can the user set the issue milestone?
  #
  # Returns Boolean
  def can_set_milestone?(user)
    async_can_set_milestone?(user).sync
  end

  # Public: Can the user set the issue milestone?
  #
  # actor - The User to check permissions for.
  #
  # Returns a Promise<Boolean>.
  def async_can_set_milestone?(user)
    return Promise.resolve(false) unless user

    async_pull_request.then do |pull|
      subject = pull.present? ? pull : self
      Platform::Loaders::Permissions::BatchAuthorize.load(
        action: :set_milestone,
        actor: user,
        subject: subject,
      ).then do |decision|
        decision.allow?
      end
    end
  end

  # Public: Can the user mark/unmark an issue as duplicated?
  #
  # Returns Boolean
  def can_mark_as_duplicate?(user)
    return false unless user

    ::Permissions::Enforcer.authorize(
      action: :mark_as_duplicate,
      actor: user,
      subject: self,
    ).allow?
  end

  # Public: Determines if a user can delete the issue
  #
  # Returns Promise(Boolean) - true if allowed, false otherwise
  # rubocop:disable GitHub/DontSyncInsideAsyncMethods
  def async_deleteable_by?(user)
    return Promise.resolve(false) unless user
    return Promise.resolve(false) if pull_request?

    if GitHub.flipper[:custom_roles].enabled?(repository.owner) && GitHub.flipper[:fgp_delete_issue].enabled?(repository.owner)
      async_fgp_deleteable_by?(user)
    else
      result = science "fgp_delete_issue" do |e|
        e.context user: user,
                  issue: self

        e.use { original_deleteable_by?(user) }
        e.try { async_fgp_deleteable_by_result(user).sync }
        e.compare { |control, candidate| control == candidate.allow? }

        # Reduce noise of this experiment by ignoring network failures
        e.ignore { |_control, candidate| Authzd.ignore_error_in_science?(candidate) }
      end
      Promise.resolve(result)
    end
  end

  # Public: Determines if a user can delete the issue
  #
  # Returns Boolean true if allowed, false otherwise
  def deleteable_by?(user)
    async_deleteable_by?(user).sync
  end

  def original_deleteable_by?(user)
    if user.can_have_granular_permissions?
      if repository.owner.organization?
        repository.owner.members_can_delete_issues? && repository.resources.administration.writable_by?(user) && repository.resources.issues.writable_by?(user)
      else
        repository.resources.administration.writable_by?(user) && repository.resources.issues.writable_by?(user)
      end
    else
      if repository.owner.organization?
        repository.owner.members_can_delete_issues? && repository.adminable_by?(user)
      else
        repository.adminable_by?(user)
      end
    end
  end

  def async_fgp_deleteable_by_result(user)
    Platform::Loaders::Permissions::BatchAuthorize.load(
      action: :delete_issue,
      actor: user,
      subject: self,
    )
  end

  def async_fgp_deleteable_by?(user)
    async_fgp_deleteable_by_result(user).then do |decision|
      decision.allow?
    end
  end


  # Public: Can a user label this issue?
  #
  # actor - The User to check permissions for.
  #
  # Returns a Boolean.
  def labelable_by?(actor:)
    async_labelable_by?(actor: actor).sync
  end

  # Public: Can a user label this issue?
  #
  # actor - The User to check permissions for.
  #
  # Returns a Promise<Boolean>.
  def async_labelable_by?(actor:)
    return Promise.resolve(false) unless actor

    async_pull_request.then do |pull|
      subject = pull.present? ? pull : self
      Platform::Loaders::Permissions::BatchAuthorize.load(
        action: :add_label,
        actor: actor,
        subject: subject,
      ).then do |decision|
        decision.allow?
      end
    end
  end

  # Public: Can a user assign this issue to a user?
  #
  # actor - The User to check permissions for.
  #
  # Returns a Boolean.
  def assignable_by?(actor:)
    async_assignable_by?(actor: actor).sync
  end

  # Public: Can a user assign this issue to a user?
  #
  # actor - The User to check permissions for.
  #
  # Returns a Promise<Boolean>.
  def async_assignable_by?(actor:)
    return Promise.resolve(false) unless actor

    async_pull_request.then do |pull|
      subject = pull.present? ? pull : self
      Platform::Loaders::Permissions::BatchAuthorize.load(
        action: :add_assignee,
        actor: actor,
        subject: subject,
      ).then do |decision|
        decision.allow?
      end
    end
  end
end
