# frozen_string_literal: true

class Issue
  class Builder

    def initialize(user, repository)
      @user = user
      @repository = repository
    end

    # Construct an instance of the Issue model for the builder's User and Respository
    # Expects parameters to be in the format supplied by the view, and
    # handles the various legacy param formats we support all over the site
    #
    # Returns a populated but unsaved Issue object
    def build(unsafe_params)
      unsafe_issue_params = unsafe_params.fetch :issue, {}
      unsafe_params[:title] = unsafe_issue_params[:title]
      unsafe_params[:body] = unsafe_issue_params[:body]

      user_can_push = @repository.pushable_by?(@user)

      if user_can_push
        if milestone = unsafe_params[:milestone]
          unsafe_params[:milestone] = @repository.milestones.find_by_id(milestone)
        end
      else
        unsafe_params.delete :milestone
      end

      allowed_keys = [
        :repository_id,
        :user_id,
        :title,
        :body,
        :milestone,
      ]

      safe_issue_params = unsafe_params.slice(*allowed_keys).
        reverse_merge(repository_id: @repository.id)

      issue = @repository.issues.build safe_issue_params
      issue.user = @user

      if issue.labelable_by?(actor: @user)
        if labels = unsafe_issue_params[:labels]
          issue.labels = @repository.load_labels(labels)
        end

        if label_ids = unsafe_issue_params[:label_ids]
          issue.labels = @repository.labels.where(id: label_ids)
        end
      end

      assignees = []
      if issue.assignable_by?(actor: @user)
        if assignee_login = unsafe_issue_params[:assignee]
          assignees = [User.find_by_login(assignee_login)]
        end

        if assignee_id = unsafe_issue_params[:assignee_id]
          assignees = [User.find_by_id(assignee_id)]
        end

        # This param is named `user_assignee_ids` to make things easier if
        # we ever want to do team assignees as well.
        if user_assignee_ids = unsafe_issue_params[:user_assignee_ids]
          assignees = User.where(id: user_assignee_ids)
        end

        issue.assignees = assignees
      end

      if unsafe_issue_params[:body_template_name].present?
        issue.body_template_name = unsafe_issue_params[:body_template_name]
      end

      if !user_can_push && issue.body_template_name.present?
        template = issue.template

        if template
          # setting this to template#labels and template#assignees because we do not
          # want read only users to create issues with labels or assignees passed through params,
          # we want to create the issue with labels or assignees from the issue template
          issue.labels = template.labels
          issue.assignees = template.assignees
        end
      end

      if issue_project_ids = unsafe_params[:issue_project_ids]
        projects = issue.potential_projects_for(@user, ids: issue_project_ids.keys)
        GitHub.dogstats.count("pending_card.created_during_issue_creation", projects.size)

        projects.each do |project|
          if issue_project_ids[project.id.to_s] == "on"
            # We can set content directly here because the user is creating the issue,
            # so we know they have access to it
            issue.cards.build(creator: @user, project: project, content: issue)
          end
        end
      end

      issue
    end

    def build_from_api(data)
      issue = @repository.issues.build
      issue.title = data["title"]
      issue.body = data["body"]
      issue.user = @user
      issue.labels = data["labels"] if data["labels"]

      issue
    end
  end
end
