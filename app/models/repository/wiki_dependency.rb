# rubocop:disable Style/FrozenStringLiteralComment

module Repository::WikiDependency
  extend ActiveSupport::Concern

  included do
    # association used mostly for cascading destroys
    has_one :repository_wiki, dependent: :destroy
  end

  def unsullied_wiki(force = false)
    @unsullied_wiki = nil if force
    @unsullied_wiki ||= begin
      u = GitHub::Unsullied::Wiki.new(self)
      u.on_update(&method(:bump_wiki_caches))
      u
    end
  end

  def wiki_exists_on_disk?
    repository_wiki && unsullied_wiki.exist?
  end

  def bump_wiki_caches(new_head_oid = nil)
    if repository_wiki
      repository_wiki.increment_cache_version!
      unsullied_wiki.rpc.clear_repository_reference_key!

      repository_wiki.update_column(:pushed_at, Time.now)
    end
  end

  # Determines if the given user is allowed to edit the Wiki from the web UI.
  # Wiki repos follow the repository permissions for git pulls/pushes.
  #
  # user - A valid User instance.
  #
  # Returns true if the user has access, or false.
  def wiki_writable_by?(user)
    case cfg_wiki_permissions
      when "push"
        pushable_by?(user)
      else
        public? ? user.is_a?(User) :  pullable_by?(user)
    end
  end

  def wiki_world_writable?
    public? && cfg_wiki_permissions != "push"
  end

  def wiki_access_to_pushers=(value)
    self.cfg_wiki_permissions = (value && value != "0") ? "push" : nil
  end

  def wiki_access_to_pushers
    cfg_wiki_permissions == "push"
  end

  alias wiki_access_to_pushers? wiki_access_to_pushers

  # Public: setup a wiki.
  def initialize_wiki(user, fork_parent_wiki: false)
    return if !user.can_edit_wikis?

    repo_wiki = repository_wiki || create_repository_wiki!

    return if unsullied_wiki.exist?

    begin
      unsullied_wiki.setup_git_repository(fork_parent_wiki: fork_parent_wiki)
    rescue ActiveRecord::ActiveRecordError, GitRPC::Error, GitHub::DGit::Error => e
      repo_wiki.destroy
      raise e
    end
  ensure
    # wiki_replicas was changed via an independent copy of this object, as
    # we only passed our id.  So reload here to pick up the change.
    reload
  end

  def wiki_path
    return "" if !owner

    name_with_owner.chomp(".wiki") + ".wiki.git"
  end

  def wiki_is_searchable?
    repo_is_searchable? && has_wiki? && wiki_exists_on_disk?
  end

  # Private: update world_writable_wiki column based on changes to `public` or `cfg_wiki_permissions`.
  private def update_world_writable_status
    return unless public_changed? || raw_data_changed?

    self.world_writable_wiki = wiki_world_writable?
  end
end
