# frozen_string_literal: true

class Repository::ContributionGraphStatus < ApplicationRecord::Collab
  include GitHub::UTF8

  EXPIRY_WINDOW = 28.days # Expire records not viewed in this timeframe
  METRICS_BATCH_SIZE = 300 # 100 deltas x 3 metrics (commits, additions, deletions)
  TRACK_LAST_VIEWED_AT_THRESHOLD = 1.hour
  TOP_CONTRIBUTOR_COUNT = 100 # contributors graph only considers the top 100 contributors
  FIRST_WEEK_POST_EPOCH = 259200 # 1970-01-04 00:00:00 UTC
  INDEXING_TOO_LONG_THRESHOLD = 2.minutes
  JOB_STATUS_TTL = 2.hours

  ContributionCountFailure = Class.new(StandardError)

  belongs_to :repository

  validates :repository, presence: true
  validates :repository_id, uniqueness: true
  validates :last_viewed_at, presence: true
  validates :last_indexed_at, presence: true, if: :last_indexed_oid?
  validates :job_enqueued_at, presence: true, if: :job_status_id?

  before_validation :set_initial_viewed_at, on: :create
  after_commit :delete_metrics_from_eventer, on: :destroy
  after_initialize :backfill_new_timestamps

  serialize :top_contributor_ids, Array

  scope :expired, -> { where("repository_contribution_graph_statuses.last_viewed_at < :cutoff", cutoff: EXPIRY_WINDOW.ago) }

  # Public: Finds or creates a graph status record for the given repo.
  #
  # Returns a Repository::ContributionGraphStatus.
  def self.for_repository(repo)
    already_retried ||= false

    ActiveRecord::Base.connected_to(role: :writing) do
      where(repository: repo).first || create!(repository: repo)
    end
  rescue ActiveRecord::RecordNotUnique, ActiveRecord::RecordInvalid
    raise if already_retried

    already_retried = true
    retry
  end

  # Public: Are we ready to fetch the graph data from Eventer?
  # We are ready if we are "current" and either:
  #   a) Eventer has consumed all pending messages for the repository in question
  #   b) Eventer has NOT consumed all pending messages but sufficient time has elapsed to
  #      permit returning partial data. This can happen in cases of excessive Eventer consumer lag.
  #
  # Returns a Boolean
  def ready?
    data_fully_indexed? || serve_stale_data?
  end

  # Public: Have we indexed current metrics for the repository?
  #
  # Note: Even though the metrics have been indexed, they may
  # still be queued for processing by Eventer.
  #
  # Returns a Boolean.
  def current?
    return false unless last_indexed_oid?

    repository.default_oid == last_indexed_oid
  end

  # Public: Have we indexed the latest commit data and is Eventer ready to serve that data?
  #
  # Checks for a custom event we send eventer after sending the actual metrics.
  # If this returns false, Eventer is still working through its queue of metrics
  # or the custom event has been dropped.
  #
  # Returns a Boolean.
  def data_fully_indexed?
    return @fully_indexed if defined?(@fully_indexed)

    @fully_indexed = current? && eventer.up_to_date?(last_indexed_oid)
  end

  # Public: Should we serve stale data instead of waiting for current data to be ready?
  #
  # We'll serve stale data if the data is not fully indexed but we have previously indexed
  # data in Eventer, when one of the following is true:
  #   a) The indexing job is finished but we are waiting an excessive time for Eventer
  #   to be ready to serve the latest data (consumers backed up).
  #   b) We've queued an indexing job and it is still queued or running after an excessive time.
  #
  # Returns a Boolean.
  def serve_stale_data?
    return false unless last_indexed_oid? && !data_fully_indexed?
    return @serve_stale_data if defined?(@serve_stale_data)

    @serve_stale_data = waiting_on_indexing_too_long? && eventer.has_indexed_data?
  end

  # Public: Returns the current JobStatus for the indexing job.
  #
  # Returns a JobStatus.
  def job_status
    return @job_status if defined?(@job_status)
    @job_status = JobStatus.find(job_status_id)

    set_ttl_for_legacy_job_status

    @job_status
  end

  # Public: Overrides the default setter to also set `job_enqueued_at` and
  # de-memoize `job_status`.
  def job_status_id=(id)
    self.job_enqueued_at = id ? Time.now : nil
    remove_instance_variable(:@job_status) if defined?(@job_status)

    super
  end

  # Public: Overrides the default setter to also set `last_indexed_at`.
  def last_indexed_oid=(oid)
    self.last_indexed_at = oid ? Time.now : nil
    super
  end

  # Public: Is a metrics indexing job running or about to run?
  #
  # Returns a Boolean.
  def running?
    job_status&.pending? || job_status&.started?
  end

  # Public: Enqueues a background job to index metrics.
  # Does not enqueue a job if the metrics are already indexed
  # or if a job is already queued.
  #
  # Returns the JobStatus ID tracking the job.
  def enqueue_metrics_job
    return if current? || running?

    create_job_status

    RepositoryContributionMetricsJob.perform_later(
      contribution_graph_status: self,
      from_oid: last_indexed_oid,
      to_oid: repository.default_oid,
    )

    return job_status.id
  end

  # Public: Enqueues a background job to celare and regenerate metrics.
  # Does not enqueue a job if one is already queued.
  #
  # Returns the JobStatus ID tracking the job.
  def enqueue_rebuild_metrics_job
    return if running?

    Repository::ContributionGraphStatus.transaction do
      update!(last_indexed_oid: nil)
      create_job_status
    end

    RepositoryContributionMetricsJob.perform_later(
      contribution_graph_status: self,
      from_oid: nil,
      to_oid: repository.default_oid,
    )

    return job_status.id
  end

  # Public: Generates and indexes metrics given a range of commits.
  #
  # from_oid: The commit OID String to use as starting point for metric generation. Usually,
  #           the last indexed commit OID. If nil, metrics will be generated for all time.
  # to_oid:   The commit OID String to use as stopping point for metric generation.
  #
  # Returns nothing.
  def generate_metrics(from_oid:, to_oid:)
    subset_tag = "subset:#{from_oid.present?}"

    deltas = GitHub.dogstats.time "repographs.generate_metrics.fetch_from_git", tags: [subset_tag] do
      repository.rpc.gh_graph_data_by_day_and_author(from_oid: from_oid, to_oid: to_oid)
    end

    delete_metrics_from_eventer if from_oid.nil?

    GitHub.dogstats.time "repographs.generate_metrics.publish_to_eventer", tags: [subset_tag] do
      deltas.each_slice(METRICS_BATCH_SIZE) do |batch|
        GitHub.sync_hydro_publisher.batch do
          batch.each { |delta| publish_delta_metrics_to_eventer(delta) }
        end
      end
    end

    last_indexed!(to_oid)
  rescue GitRPC::ObjectMissing
    if from_oid
      # 'from_oid' may have been garbage collected since last indexing. Generate full metrics
      update!(last_indexed_oid: nil)
      generate_metrics(from_oid: nil, to_oid: to_oid)
    else
      raise
    end
  end

  # Public: updates the list of user ids stored in the serialized top_contributor_ids column
  #  to match the top 100 user ids returned from `Contributors#all`.
  def update_top_contributors!
    # we pass in an empty set of uncomputable ids since this method is intended to be run
    # on a job with a much longer timeout than a regular request. We have time to compute here.
    contributors = Contributors.new(repository, not_computable_ids: Set.new, ignore_merge_commits: true, mailmap: false)

    # Contributors#all returns an array of (user, count) tuples. We don't care about the counts.
    all_response = contributors.all
    if !all_response.computed?
      Failbot.report(ContributionCountFailure.new)
      return
    end

    # we only need the ordered contributors; drop the contribution counts
    contributors = all_response.value.map(&:first)
    contributors = filter_blocked_and_spammy(contributors)

    top_contributors = contributors.first(TOP_CONTRIBUTOR_COUNT)

    self.top_contributor_ids = top_contributors.map(&:id)
    save!
  end

  # Public: Marks this repository's graphs as viewed. Only updates the last_viewed_at if it hasn't
  #   been updated in over an hour.
  def viewed!
    seconds_since_last_viewed = Time.current - last_viewed_at
    return unless seconds_since_last_viewed > TRACK_LAST_VIEWED_AT_THRESHOLD

    ActiveRecord::Base.connected_to(role: :writing) do
      touch(:last_viewed_at)
    end
  end

  # Public: Calculates the contributors graph data for this repository. If the data is not sent
  #   to eventer yet, queues a job to do so. Touches the `last_viewed_at` as needed.
  #
  # Returns the graph data, or nil if the data has been queued but eventer isn't quite ready.
  def code_frequency_data
    prepare_graph_data(:code_frequency) do
      data = eventer.fetch_code_frequency_data
      GitHub::RepoGraph::Eventer::CodeFrequency.new(data).to_graph
    end
  end

  # Public: Calculates the contributors graph data for this repository. If the data is not sent
  #   to eventer yet, queues a job to do so. Touches the `last_viewed_at` as needed.
  #
  # Returns the graph data, or nil if the data has been queued but eventer isn't quite ready.
  def contributors_data
    prepare_graph_data(:contributors) do
      data = eventer.fetch_contributors_data(top_emails)
      GitHub::RepoGraph::Eventer::Contributors.new(data).to_graph
    end
  end

  def commit_activity_data
    prepare_graph_data(:commit_activity) do
      data = eventer.fetch_commit_activity_data
      GitHub::RepoGraph::Eventer::CommitActivity.new(data).to_graph
    end
  end

  # Public: The list of top emails corresponding to the list of top_contributor_ids
  def top_emails
    user_emails = Platform::Loaders::UserEmails.load_all(top_contributor_ids).sync
    expanded_emails = user_emails.flatten.each_with_object([]) do |email, coll|
      coll << email

      # Include the normalized (account-rename safe) version of their no-reply
      # address. Doesn't replace the non-normalized version since data may have
      # been indexed with that non-account-rename safe version.
      coll << normalized_stealth_email(email) if stealthy_email?(email)
    end

    expanded_emails.uniq
  end

  private

  # Internal: Have we been waiting on current indexed data for too long?
  #
  # Can be true if it's been a long time since we've queued and indexing job
  # and the current data is not ready to be served (either waiting on Eventer
  # consumers or the metrics job itself).
  #
  # Returns a Boolean
  def waiting_on_indexing_too_long?
    return false unless job_enqueued_at?

    waiting_on_indexing = current? ? !data_fully_indexed? : running?
    long_time_since_enqueueing = Time.now - job_enqueued_at > INDEXING_TOO_LONG_THRESHOLD

    waiting_on_indexing && long_time_since_enqueueing
  end

  # Internal: JobStatuses created prior to this method being added had no ttl which means they
  # never expired if the job never ran. This resulted in graphs whose jobs were discarded from
  # ever having their data generated.
  #
  # This method can be safely deleted 30 days after the PR introducing it is merged as all
  # repositories should have had their data either touched or purged by then.
  def set_ttl_for_legacy_job_status
    return unless job_status

    # nothing to do if the ttl is already the expected value
    return if job_status.ttl == JOB_STATUS_TTL

    job_status.ttl = JOB_STATUS_TTL
    ActiveRecord::Base.connected_to(role: :writing) do
      job_status.save
    end
  end

  def prepare_graph_data(graph_name)
    enqueue_metrics_job unless current?
    return nil unless ready?

    tags = ["graph_name:#{graph_name}", "stale:#{serve_stale_data?}"]
    data = GitHub.cache.fetch(cache_key(graph_name)) do
      GitHub.dogstats.time "repographs.display", tags: tags do
        yield
      end
    end

    GitHub.dogstats.increment "repographs.view", tags: tags
    viewed!
    data
  rescue Eventer::Client::Error => e
    Failbot.report(e)
    return nil
  end

  def publish_delta_metrics_to_eventer(delta)
    timestamp = Time.find_zone("UTC").parse(delta[:date])

    # Disregard metrics that would be attributed to any week before the Linux Epoch.
    # This maintains behavior with legacy repo graphs and prevents commits with malformed
    # dates from being graphed.
    return if timestamp.to_i < FIRST_WEEK_POST_EPOCH

    author_tag = utf8(delta[:author]).downcase.strip
    author_tag = normalized_stealth_email(author_tag) if stealthy_email?(author_tag)

    metrics = {
      GitHub::RepoGraph::Eventer::COMMITS => delta[:commits],
      GitHub::RepoGraph::Eventer::ADDITIONS => delta[:additions],
      GitHub::RepoGraph::Eventer::DELETIONS => delta[:deletions],
    }

    metrics.each do |event, value|
      next if value.zero?

      Eventer::Event.track(
        event,
        owner: repository,
        delta: value,
        timestamp: timestamp,
        tag: author_tag,
        async: false,
      )
    end
  end

  def delete_metrics_from_eventer
    eventer.delete_counters
  end

  def last_indexed!(oid)
    update(last_indexed_oid: oid)

    Eventer::Event.track(GitHub::RepoGraph::Eventer::INDEXED, owner: repository, tag: oid, async: false)
  end

  def set_initial_viewed_at
    self.last_viewed_at ||= Time.now
  end

  def create_job_status
    ActiveRecord::Base.connected_to(role: :writing) do
      self.update! job_status_id: SecureRandom.uuid

      # create a new job status and update the cache ivar used in #job_status
      @job_status = JobStatus.create(id: job_status_id, ttl: JOB_STATUS_TTL)
    end
  end

  def filter_blocked_and_spammy(users)
    blocked_ids = Set.new(repository.owner.ignored_ids)
    users.reject { |user| user.spammy? || blocked_ids.include?(user.id) }
  end

  def cache_key(graph_name)
    parts = [
      "repo-graph",
      "eventer",
      graph_name,
      "v3",
      repository.id,
      last_indexed_oid,
      last_indexed_at.to_i,
    ]
    parts << "stale" if serve_stale_data?
    parts.join(".")
  end

  def eventer
    return @eventer if defined?(@eventer)
    @eventer = GitHub::RepoGraph::Eventer.new(repository_id)
  end

  # Temporary after_initialize hook to backfill `last_indexed_at` and `job_enqueued_at`.
  # This hook is safe to remove 28 days after this change is deployed.
  def backfill_new_timestamps
    return unless persisted?

    new_timetamps = {}
    new_timetamps[:last_indexed_at] = updated_at if last_indexed_oid? && last_indexed_at.nil?
    new_timetamps[:job_enqueued_at] = updated_at if job_status_id? && job_enqueued_at.nil?

    ActiveRecord::Base.connected_to(role: :writing) do
      update_columns(new_timetamps) unless new_timetamps.empty?
    end
  end

  # Private: Is the email address a stealthy GH noreply email?
  # E.g. 42+defunkt@users.noreply.github.com
  def stealthy_email?(email)
    StealthEmail::STEALTH_EMAIL_REGEX.match?(email)
  end

  # Private: Returns a normalized, account-rename safe version of a stealty email.
  # Allows us to index commits authored by stealth email addresses and still look
  # them up for a given user even if their account is renamed.
  #
  # For example, take the user @dr-jekyll who commited using their GH-provided email
  # of '999+dr-jekyll@users.noreply.github.com'. If we store this as is in Eventer,
  # and they later rename their account to @mr-hyde, we won't be able to query for these metrics
  # given their new known email addresses. This is because their stealth email is automattically
  # updated to '999+mr-hyde@users.noreply.github.com'.
  #
  # If instead we store the metrics in Eventer tagged with the normalized '999+username@users.noreply.github.com'
  # we can always use this to query on in the future regardless of what their current account login is.
  def normalized_stealth_email(email)
    return unless StealthEmail::STEALTH_EMAIL_REGEX.match(email)

    "#{$1}+username@#{GitHub.stealth_email_host_name}"
  end
end
