# rubocop:disable Style/FrozenStringLiteralComment

module Repository::PorterDependency

  # Porter is working on this.
  def porter_started!
    GitHub.kv.set(porter_cache_key, true.to_s)
  end

  # Porter is not working on this.
  def porter_stopped!
    GitHub.kv.del(porter_cache_key)
  end

  # Is porter working on this?
  def porter_importing?
    GitHub.kv.exists(porter_cache_key).value { false }
  end

  def porter_cache_key
    "repository:porter:importing:#{repository.id}"
  end
end
