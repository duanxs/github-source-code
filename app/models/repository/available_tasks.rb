# frozen_string_literal: true

class Repository::AvailableTasks
  include Enumerable

  attr_reader :repository
  attr_reader :sha

  def initialize(repository, ref)
    @repository = repository
    @sha = @repository.ref_to_sha(ref)
  end

  # Public: Iterates through found Tasks.
  def each(*args, &block)
    parsed_task_list.each *args, &block
  end

  # Public: Return a list of tasks that run automatically on the given event
  #
  # event :: A String event type, like `push` or `issue_comment`
  #
  # Returns an Array of Tasks
  def for_event(event)
    to_a.select { |t| t.events.include?(event) }
  end

  # Public: Returns an array of Tasks for the current Repository.
  #
  # Returns an Array of Tasks.
  def parsed_task_list
    @parsed_task_list ||= parse_task_list + parse_hubot_task_list
  end

  # Public: Return a Task with the given String name, or nil.
  #
  # Returns a Task or nil.
  def for_name(name)
    each.find { |t| t.name == name }
  end

  # Inernal: Parses Task YAML into real Task objects.
  #
  # Returns an Array of Tasks.
  def parse_task_list
    return [] unless task_list_file_content.present?

    yaml_data = YAML.safe_load(task_list_file_content)
    return [] unless yaml_data.is_a? Array

    yaml_data.map { |entry| Task.new(@repository, entry) }
  rescue YAML::SyntaxError
    []
  end

  # Inernal: Parses Hubot YAML into real Task objects.
  #
  # Returns an Array of Tasks.
  def parse_hubot_task_list
    return [] unless hubot_task_list_file_content.present?

    yaml_data = YAML.safe_load(hubot_task_list_file_content)
    return [] unless yaml_data.is_a? Array

    yaml_data.map do |entry|
      entry["runner_url"] = "https://github-huboter.herokuapp.com/hook"
      Task.new(@repository, entry)
    end

  rescue YAML::SyntaxError
    []
  end

  # Inernal: Returns the contents of tasks.yml for the repository.
  #
  # Returns String if content is found and nil if it was not.
  def task_list_file_content
    @task_list_content ||= begin
      if root = repository.directory(@sha || repository.default_branch)
        if task_list_tree_entry = PreferredFile.find(directory: root, type: :repository_task_list)
          task_list_tree_entry.data
        else
          nil
        end
      else
        nil
      end
    end
  rescue RepositoryObjectsCollection::InvalidObjectId, GitRPC::ObjectMissing
    nil
  end

  # Internal: Returns the contents of hubot.yml for the repository.
  #
  # Returns String if content is found and nil if it was not.
  def hubot_task_list_file_content
    @hubot_task_list_content ||= begin
      if root = repository.directory(@sha || repository.default_branch)
        if hubot_task_list_tree_entry = PreferredFile.find(directory: root, type: :repository_hubot_task_list)
          hubot_task_list_tree_entry.data
        else
          nil
        end
      else
        nil
      end
    end
  rescue RepositoryObjectsCollection::InvalidObjectId, GitRPC::ObjectMissing
    nil
  end

end
