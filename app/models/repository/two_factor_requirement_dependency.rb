# frozen_string_literal: true

module Repository::TwoFactorRequirementDependency

  # Public: Returns true if this repository's 2FA Authentication requirement
  #         (if any) can be met by the given PROSPECTIVE_MEMBER.
  #
  # prospective_member - a User
  #
  # Returns a Boolean.
  def two_factor_requirement_met_by?(prospective_member)
    return false if organization && !organization.two_factor_requirement_met_by?(prospective_member)
    return true
  end

  def async_two_factor_requirement_met_by?(prospective_member)
    self.async_organization.then do |organization|
      next Promise.resolve(true) unless organization
      next Promise.resolve(organization.async_two_factor_requirement_met_by?(prospective_member))
    end
  end
end
