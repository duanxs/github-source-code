# frozen_string_literal: true

module Repository::SponsorsDependency
  extend ActiveSupport::Concern

  included do
    has_one :sponsors_listing_featured_item, as: :featureable, dependent: :destroy
  end

  def unfeature_from_sponsors_profile
    sponsors_listing_featured_item&.destroy
  end
end
