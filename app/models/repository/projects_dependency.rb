# frozen_string_literal: true

module Repository::ProjectsDependency
  extend ActiveSupport::Concern

  include Configurable::DisableRepositoryProjects

  class CannotEnableProjectsError < StandardError; end

  included do
    alias_method :projects_enabled?, :repository_projects_enabled?
    alias_method :async_projects_enabled?, :async_repository_projects_enabled?

    alias_method :has_projects=, :has_repository_projects=
    alias_method :has_projects, :repository_projects_enabled?

    def self.filter_ids_with_projects_enabled(ids)
      repo_ids = ids.dup

      # Filter repos where projects disabled
      disabled_projects_repo_ids = Configuration::Entry.where(
        name: Configurable::DisableRepositoryProjects::KEY,
        value: Configuration::TRUE, target_type: "Repository"
      ).with_ids(repo_ids, field: :target_id).pluck(:target_id)

      relevant_repo_ids = repo_ids - disabled_projects_repo_ids
      return relevant_repo_ids if relevant_repo_ids.empty?

      # Filter repos where org owner has repository projects disabled
      org_owned_repos_by_owner_id = Hash.new { |h, k| h[k] = [] }
      org_owned_repos = Repository.with_ids(relevant_repo_ids).org_owned.pluck(:owner_id, :id)
      org_owned_repos.each do |owner_id, id|
        org_owned_repos_by_owner_id[owner_id] << id
      end

      disabled_projects_org_ids = Configuration::Entry.where(
        name: Configurable::DisableRepositoryProjects::KEY,
        value: Configuration::TRUE, target_type: "User"
      ).with_ids(org_owned_repos_by_owner_id.keys, field: :target_id).pluck(:target_id)

      relevant_repo_ids -= disabled_projects_org_ids.flat_map { |org_id| org_owned_repos_by_owner_id[org_id] }

      relevant_repo_ids
    end
  end

  # Public: Can projects be enabled for this repository?
  #
  # Returns a Boolean.
  def can_enable_projects?
    async_can_enable_projects?.sync
  end

  # Public: Can projects be enabled for this repository?
  #
  # Returns a Promise<Boolean>.
  def async_can_enable_projects?
    async_owner.then do |owner|
      next true unless owner.organization?
      owner.async_repository_projects_enabled?
    end
  end

  def enable_repository_projects(**arguments)
    ensure_projects_can_be_enabled!
    super
  end

  def has_repository_projects=(new_value)
    ensure_projects_can_be_enabled! if new_value
    @has_repository_projects = new_value
  end

  def visible_projects_for(user)
    GitHub.dogstats.time("project_permissions.visible_projects_for", tags: ["owner_type:repository"]) do
      # Since repository-owned projects inherit permissions from their owning
      # repository, we don't have to do any extra abilities checks here.
      if (user&.can_have_granular_permissions? && projects_readable_by?(user)) || readable_by?(user)
        projects
      else
        Project.none
      end
    end
  end

  def writable_projects_for(user)
    GitHub.dogstats.time("project_permissions.writable_projects_for", tags: ["owner_type:repository"]) do
      # Since repository-owned projects inherit permissions from their owning
      # repository, we don't have to do any extra abilities checks here.
      if (user&.can_have_granular_permissions? && projects_writable_by?(user)) || writable_by?(user)
        projects
      else
        Project.none
      end
    end
  end

  private

  def ensure_projects_can_be_enabled
    if @has_repository_projects && !can_enable_projects?
      errors.add(:has_projects, "can't be enabled because the owning organization has repository projects disabled.")
    end
  end

  def ensure_projects_can_be_enabled!
    raise CannotEnableProjectsError unless can_enable_projects?
  end
end
