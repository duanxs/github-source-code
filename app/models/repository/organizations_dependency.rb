# rubocop:disable Style/FrozenStringLiteralComment

# Adds organization-specific functionality to repositories, specifically
# permissions. A repository may be owned by a user or owned by an
# organization. In the latter case, the repository may belong to
# teams and accessed in various ways by an arbitrary number of
# users. This module handles that.
module Repository::OrganizationsDependency
  extend ActiveSupport::Concern

  included do
    belongs_to :organization
  end

  # Does this repository belong to an Organization?
  #
  # Returns a Boolean
  def in_organization?
    !!(organization_id && organization)
  end

  def async_in_organization?
    return Promise.resolve(false) unless organization_id?

    async_organization.then do |organization|
      organization.present?
    end
  end

  def set_organization
    if owner && owner.organization?
      self.organization = owner
    elsif private? && parent && parent.in_organization?
      self.organization = parent.organization
    else
      self.organization = nil
    end
  end

  # Internal: update this repository's organization and clean up
  # any collaboration permissions that no longer apply.
  #
  # inline_fork_cleanup: -  (optional, default false) whether inaccessible
  #                         fork cleanup should happen inline rather than via a
  #                         background job, if the organization has changed from
  #                         one to another.
  # remove_collaborators: - (optional, default true) if bringing a user-owned
  #                         repo into an organization, whether or not to remove
  #                         all direct repo collaborators. Set to false by
  #                         user->org transformation so members are preserved
  #                         temporarily. This option is only respected when
  #                         moving from user-owned to organization-owned.
  def update_organization(inline_fork_cleanup: false, remove_collaborators: true)
    old_org = organization
    set_organization
    save

    return if old_org == organization

    # Add this repo as a dependent of the new organization so adminable_by
    # checks &c succeed before the repo is removed from the former organization.
    organization.dependent_added(self) if organization

    if old_org
      old_org.dependent_removed(self)

      teams.owned_by(old_org).each do |team|
        team.remove_repository(self, inline_fork_cleanup: inline_fork_cleanup)
      end
    end

    remove_all_members if remove_collaborators && (old_org || organization)
  end

  # Can a user or team view or pull this repo?
  def pullable_by?(actor)
    readable_by?(actor)
  end

  # Can a user or team push to this repo?
  def pushable_by?(actor, ref: nil)
    return false if archived?

    return true if resources.contents.writable_by?(actor)

    if ref
      pulls = PullRequest.find_open_based_on_head_ref(self, ref)
      pull = pulls.find do |pr|
        pr.fork_collab_granted? && pr.base_repository.resources.contents.writable_by?(actor)
      end
      return pull != nil
    else
      return false
    end
  end

  def has_maintain_role?(actor)
    direct_role_for(actor) == :maintain
  end

  def has_triage_role?(actor)
    direct_role_for(actor) == :triage
  end

  # TODO: Add fork-collab check to sync behavior of `async_pushable_by` and `pushable_by`.
  #       Then make `pushable_by` call `async_pushable_by`
  def async_pushable_by?(user)
    return Promise.resolve(false) if archived?
    return Promise.resolve(false) if user.organization?
    return Promise.resolve(false) if user.mannequin?

    return async_writable_by?(user) if user.user?

    user.async_load_installation_for(self).then do
      next false unless user.ability_delegate

      resources.contents.async_permit?(user, :write)
    end
  end

  def async_user_relationship(user)
    return Promise.resolve(:none) unless user

    @async_user_relationships ||= {}
    @async_user_relationships[user.id] ||= async_adminable_by?(user).then do |adminable|
      next :owner if adminable

      async_contributor?(user).then do |contributor|
        next :contributor if contributor

        async_writable_by?(user).then do |writable|
          writable ? :collaborator : :none
        end
      end
    end
  end

  # Internal: Repository and Organization-level checks for whether a User can report
  #           an AbuseReportable record.
  #
  # This is used in AbuseReportable#async_viewer_can_report. It's implemented at the
  # Repository level so that multiple comments on a timeline can share the memoized
  # check results here.
  #
  # See https://github.com/github/ce-community-and-safety/issues/1338#issuecomment-548590108
  # for decision tree on who can report in what circumstances
  #
  # Returns a Boolean or nil if the result can't be determined at this level.
  def async_user_can_report(user)
    return Promise.resolve(false) unless GitHub.can_report?
    return Promise.resolve(false) unless user
    return Promise.resolve(false) if private?

    @async_user_can_report ||= {}
    @async_user_can_report[user.id] ||= async_writable_by?(user).then do |user_can_write|
      next true if user_can_write
      async_member?(user).then do |collaborator|
        next true if collaborator
        async_owner.then do |owner|
          next false unless owner
          (owner.organization? ? owner.async_member?(user) : Promise.resolve(false)).then do |org_member|
            next true if org_member
            owner.async_blocking?(user).then do |user_blocked_by_owner|
              next false if user_blocked_by_owner
              nil
            end
          end
        end
      end
    end
  end

  # Public: Add the specified organization as a direct member of this
  # repository. This grants access to all members of the organization.
  #
  # organization - Organization to add to the repository.
  # action       - Level of access to grant.
  #
  # Returns nothing.
  def add_organization(org, action:)
    raise ArgumentError if org.nil?
    raise ArgumentError unless org.organization?
    return unless receives_organization_default_repository_permission?

    grant(org, action)
  end

  # Public: Remove the specified organization from this repository's direct
  # members. This removes the default permission from all members of the
  # organization.
  #
  # organization - Organization to remove from the repository.
  #
  # Returns nothing.
  def remove_organization(org)
    raise ArgumentError if org.nil?
    raise ArgumentError unless org.organization?
    return unless receives_organization_default_repository_permission?

    revoke(org)
  end

  # A Team scope representing the teams this repository is associated with.
  #
  # immediate_only - returns just the parent teams or includes all nested teams aswell
  def teams(immediate_only: true)
    async_teams(immediate_only: immediate_only).sync
  end

  # Public: Returns a Promise that resolves as a Team relation for this
  # Repository's teams.
  def async_teams(immediate_only: true)
    Platform::Loaders::RepositoryTeams.load(self, immediate_only: immediate_only).then do |team_ids|
      next Team.none unless team_ids.any?
      Team.where(id: team_ids)
    end
  end

  # A Scope of Teams the user belongs to which have access to this repository.
  def teams_for(user)
    return Team.none if !in_organization?
    user.teams.where(id: actor_ids_for_team_on_repo)
  end

  # A Scope of Teams the user is allowed to see which have access to this repository.
  def visible_teams_for(user)
    return Team.none if !in_organization?

    organization.visible_teams_for(user).with_ids(actor_ids_for_team_on_repo)
  end

  # action:     An optional Ability.action symbol or Role to filter on
  # actor_ids:  An optional Array filter to limit the team_ids we check
  def actor_ids_for_team_on_repo(action: nil, actor_ids: nil)
    roles_to_filter_out = nil
    ability_action = nil
    role = nil

    # Figure out if action is specified, and if it's a legacy Ability action or a pre-defined
    # fine grained permission role
    if action.present?
      if ::Ability.actions[action]
        ability_action = action
      else
        role = Role.find_by_name(action)
      end

      # :read, :triage, :write & :maintain need to filter out any dependent custom roles
      base_role = role || Role.find_by_name(ability_action)
      if base_role
        roles_to_filter_out = Role.where(base_role: base_role)
      end

      # If neither ability_action and role are defined, a valid action or role wasn't provided
      unless (ability_action || role)
        raise ArgumentError, "Invalid action for Repository#actor_ids_for_team_on_repo"
      end
    end

    # A Role was specified (triage, maintain or custom role). This can be determined solely from the
    # UserRole table. In this case we can shortcut and return quickly.
    if role
      role_team_ids = UserRole.where(role: role, target: self, actor_type: "Team")
      # if actor_ids is specified, we only need the intersection of role_team_ids and actor_ids
      role_team_ids = role_team_ids.with_ids(Array.wrap(actor_ids), field: "actor_id") if actor_ids.present?
      return role_team_ids.pluck(:actor_id)
    end

    role_team_ids_to_remove = []
    team_scope = Ability.teams_direct_on_repos(repo_id: id)

    if ability_action
      # Action filtering has some complications. Every Role has a "action" granted
      # in Abilties. So, when filtering on :read, we need to filter out Teams that are
      # also granted Triage.
      role_team_ids_to_remove = UserRole.where(role: roles_to_filter_out, target: self, actor_type: "Team").pluck(:actor_id)
      team_scope = team_scope.where(action: Ability.actions[ability_action])
    end

    if actor_ids
      team_scope = team_scope.with_ids(Array.wrap(actor_ids), field: "actor_id")
    end

    team_ids = team_scope.pluck(:actor_id) - role_team_ids_to_remove
  end

  # An Array of teams that the user is allowed to add this repository to.
  def addable_teams_for(user)
    return Team.none unless in_organization?
    return Team.none unless organization.member?(user)
    return Team.none unless adminable_by?(user)

    organization.visible_teams_for(user)
  end

  # Can this repository be added to the specified team by the specified user?
  #
  # team  - The Team we're checking addability for.
  # adder - The User trying to add this repo to a team.
  #
  # Returns a boolean.
  def can_add_to_team?(team, adder:)
    (addable_teams_for(adder) - teams).include?(team)
  end

  # All ids that are present in any of the teams the repository belongs to.
  #
  # include_org_admins - Boolean that indicates whether you'd like to include
  #                      all the owning org's admins (which always have access
  #                      to all org repos).
  #
  # Returns an Array of ids.
  def all_team_member_ids(include_org_admins:, immediate_only: true)
    return [] unless in_organization?

    if include_org_admins
      Team.member_ids_of(actor_ids_for_team_on_repo, immediate_only: immediate_only) | organization.admin_ids
    else
      Team.member_ids_of(actor_ids_for_team_on_repo, immediate_only: immediate_only)
    end
  end

  # Public: The ids of any User with admin abilities on this repository
  def admin_ids
    PermissionCache.fetch ["admin_ids", id] do
      ids = actor_ids(type: "User", min_action: :admin)
      ids << owner_id if owner && !owner.organization?
      ids
    end
  end

  # All users that are present in any of the teams the repository belongs to.
  #
  # include_org_admins - Boolean that indicates whether you'd like to include
  #                      all the owning org's admins (which always have access
  #                      to all org repos).
  #
  # Returns an Array of Users.
  def all_team_members(include_org_admins:)
    return User.none unless in_organization?

    PermissionCache.fetch ["all_team_members", ability_type, ability_id, include_org_admins] do
      User.where(id: all_team_member_ids(include_org_admins: include_org_admins))
    end
  end

  # Only add new root repositories to the owners team, not forks.
  def add_to_owners_team
    nil
  end

  # Adds a repository to the team specified by `@team_for_after_create`
  # after creation.
  def add_repository_to_team
    if team = @team_for_after_create
      team.add_repository self, team.permission
    end
  end
  attr_accessor :team_for_after_create

  # Internal: Queue up a job to add teams of repository
  #
  # Used when a private organization repo is forked. In that case all teams
  # that have access to the parent will have access to the fork.
  #
  # repo - Repository to copy team permissions from
  #
  # Returns nothing
  def add_teams_of(repo)
    RepositoryAddTeamsJob.perform_later(id, repo.id)
  end

  # Internal: Add teams of repository
  #
  # repo      - Repository to copy team permissions from
  #
  # Returns nothing
  def add_teams_of!(repo)
    repo.teams.each do |team|
      Ability.throttle do
        team.add_repository(self, team.permission_for(repo))
      end
    end
  end

  # Public: Determine whether forking is enabled for this Repository or not.
  #
  # Returns true if forking this Repository is disabled.
  def forking_disabled?
    if in_organization? && private?
      return true unless allow_private_repository_forking?
    end

    return false unless owner && owner.organization?
    return false unless owner.saml_sso_enabled?

    GitHub.flipper[:forking_org_owned_repository_disabled].enabled?(owner)
  end

  # Find all of the child teams for the parent teams by combining infinite LIKE
  # statements. See full conversation around performance here: https://github.com/github/github/pull/74530
  #
  # teams - teams you want to find the children for
  #
  # Returns a list of child Teams (not including the parent teams)
  def child_teams(teams)
    return Team.none if teams.empty?

    Team.where(teams.map(&:tree_path).map { |path| "tree_path LIKE '#{path}/%'" }.join(" OR "))
  end
end
