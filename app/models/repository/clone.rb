# frozen_string_literal: true
class Repository::Clone
  def self.from_url(repository, remote_url)
    return unless remote_url

    repository.rpc.backend.delegate.get_write_routes.each do |dgit_route|
      repo_backend_rpc = dgit_route.build_maint_rpc
      repo_backend_rpc.bare_clone(remote_url, dgit_route.path)
    end

    repository.correct_hooks_symlink
    repository.write_nwo_file_unsafe
    repository.async_backup
    repository.reset_git_cache
    repository.refs
    repository.analyze_languages
  end
end
