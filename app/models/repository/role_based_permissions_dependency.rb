# frozen_string_literal: true

module Repository::RoleBasedPermissionsDependency
  # Public: Returns the most capable action level or role name that an actor has on this repository.
  #
  # actor - The User or Team to check access for.
  #
  # Returns a Promise<Symbol|nil>.
  def async_action_or_role_level_for(actor, include_employee_granted_permissions: true, include_custom_roles: true)
    async_access_level_for(actor, include_employee_granted_permissions: include_employee_granted_permissions).then do |access_level|
      next Promise.resolve(nil) unless access_level.present?
      next Promise.resolve(access_level) unless actor.present?

      async_role_for(actor, include_custom_roles).then do |most_capable_role|
        next access_level unless most_capable_role.present?

        if most_capable_role.action_rank > Ability::ACTION_RANKING[access_level]
          most_capable_role.name.to_sym
        else
          access_level
        end
      end
    end
  end

  # Public: Returns the most capable action or role that an actor has on this repository.
  #
  # actor - The User or Team to check access for.
  #
  # Returns a Promise<Symbol|nil>.
  def async_most_capable_action_or_role_for(actor, include_employee_granted_permissions: true)
    async_ability_for(actor).then do |access_level|
      next Promise.resolve(nil) unless access_level.present?
      next Promise.resolve(access_level) unless actor.present?

      async_role_for(actor).then do |most_capable_role|
        next access_level unless most_capable_role.present?

        if most_capable_role.action_rank >= Ability::ACTION_RANKING[access_level.action.to_sym]
          most_capable_role
        else
          access_level
        end
      end
    end
  end

  # Public: Returns the most capable Role that an actor has on this repository.
  #
  # actor - The User or Team to check access for.
  # include_custom_roles - If custom roles should be included, or if their base role should be used instead
  #
  # Returns a Promise<Role|nil>.
  def async_role_for(actor, include_custom_roles = true)
    async_owner.then do |org|
      next unless org.organization?

      Platform::Loaders::Permissions::MostCapableUserRoleOnRepositoryForActor.load(
        actor: actor,
        repo: self,
      ).then do |most_capable_role|
        next unless most_capable_role.present?
        most_capable_role.async_role.then do |role|
          role.async_base_role.then do |base_role|
            if role.custom? && !include_custom_roles
              base_role
            else
              role
            end
          end
        end
      end
    end
  end

  # Public: Returns the most capable Ability that an actor has on this repository.
  #
  # actor - The User or Team to check access for.
  #
  # Returns a Promise<Role|nil>.
  def async_ability_for(actor)
    return Promise.resolve(nil) if new_record? || destroyed? || deleted?
    return Promise.resolve(nil) if actor.is_a?(User) && !actor.user?

    ability = PermissionCache.fetch(self.class.repository_permission_cache_key(actor.id, self.id)) do
      Authorization.service.most_capable_ability_between(actor: actor, subject: self)
    end

    Promise.resolve(ability)
  end

  # The class name persisted as `target_type` when a UserRole is created
  # with this object as target.
  def user_role_target_type
    "Repository"
  end
end
