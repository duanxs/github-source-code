# frozen_string_literal: true
# actions-specific functionality for repositories
module Repository::WorkflowsDependency

  def actions_check_suites
    suites = check_suites.order(id: :desc)
    # Only use the lab app id if the flag is enabled in the repo for a way faster query
    if GitHub.flipper[:launch_lab].enabled? self
      suites.for_app_ids(GitHub.launch_github_app&.id, GitHub.launch_lab_github_app&.id)
    else
      suites.where(github_app_id: GitHub.launch_github_app&.id)
    end
  end

  def workflow_file(branch)
    @file ||= PreferredFile.find(directory: self.directory(branch), type: :github_workflow, subdirectories: [".github"])
  end

  def workflow_file_present?(branch)
    !workflow_file(branch).nil?
  end

  # Inside .github/workflows. Any *.yml file counts
  def workflow_v2_file_present?(branch)
    Actions::Workflow::WORKFLOW_PATHS.any? do |path|
      return false unless directory = self.directory(branch, path)
      yml_files_at_path(directory).any?
    end
  end

  def workflow_content(repository, branch, path)
    head = repository.ref_to_sha(branch)
    return nil if head.nil?

    repository.tree_entry(head, path)
  rescue GitRPC::Error
    nil
  end

  def workflow_contents(sha:, paths:)
    hash = {}
    Actions::Workflow::WORKFLOW_PATHS.each do |dir|
      directory = self.directory(sha, dir)
      next unless directory

      begin
        paths.each do |path|
          # Git RPC will return the information in ASCII, so we need it in ASCII here to do the comparison
          ascii_path = path&.b
          directory.each do |entry|
            if entry[:content].path == ascii_path
              hash[path] = entry[:content]
            end
          end
        end
      rescue GitRPC::NoSuchPath, GitRPC::ObjectMissing, GitRPC::InvalidObject
        []
      end
    end
    hash
  end

  # Stores existing workflows metadata in the database
  def persist_existing_workflows
    branch = self.default_branch
    Actions::Workflow::WORKFLOW_PATHS.each do |path|
      return unless directory = self.directory(branch, path)

      yml_files_at_path(directory).each do |file|
        workflow_file_path = file["path"]
        parsed_workflow = Actions::ParsedWorkflow.parse_from_yaml(self, workflow_file_path)
        name = parsed_workflow.name
        state = GitHub.flipper[:actions_disabled_workflows].enabled?(owner) && self.public && parsed_workflow.has_schedule_trigger? ? "disabled_fork" : "active"
        Actions::Workflow.create_or_update_workflow(workflow_file_path, name, self, state, nil)
      end
    end
  end

  def workflow_runs_channel
    GitHub::WebSocket::Channels::workflow_runs(self)
  end

  private

  def yml_files_at_path(directory)
    entries = directory.tree_entries || []

    entries.select do |entry|
      entry.blob? && entry.display_name.ends_with?(".yml", ".yaml")
    end
  rescue GitRPC::NoSuchPath, GitRPC::ObjectMissing, GitRPC::InvalidObject
    []
  end
end
