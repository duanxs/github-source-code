# rubocop:disable Style/FrozenStringLiteralComment

module Repository::SlumlordDependency
  def svn_in_use?
    if @svn_in_use.nil?
      @svn_in_use = all_refs.exist?("refs/__gh__/svn/v3") || all_refs.exist?("refs/__gh__/svn/v4") || !!rpc.fs_exist?("svn.history.msgpack")
    end
    @svn_in_use
  end

  def svn_blocked?
    svn_status == :disabled
  end

  def svn_status
    @svn_status ||=
        rescue_offline(result_if_offline: :offline) do
          if rpc.config_get("github.blocksvn") == "true"
            :disabled
          else
            :enabled
          end
        end
  end

  def block_svn
    rpc.config_store("github.blocksvn", true)
    GitHub::DGit::Maintenance.safely_recompute_checksums(self, :vote)
    instrument :disable_svn, prefix: :staff
    @svn_status = :disabled
  end

  def unblock_svn
    rpc.config_delete("github.blocksvn")
    GitHub::DGit::Maintenance.safely_recompute_checksums(self, :vote)
    instrument :enable_svn, prefix: :staff
    @svn_status = :enabled
  end

  def svn_toggle_blocked
    if svn_blocked?
      unblock_svn
    else
      block_svn
    end
  end

  def svn_debugging?
    if @svn_debugging.nil?
      @svn_debugging = false
      if expires = rpc.config_get("github.debug-svn-until")
        begin
          if expires > Time.now
            @svn_debugging = expires
          end
        rescue ArgumentError
          # not valid means no debugging
        end
      end
    end
    @svn_debugging
  end

  def debug_svn
    expires = 1.day.from_now
    rpc.config_store("github.debug-svn-until", expires.iso8601)
    GitHub::DGit::Maintenance.safely_recompute_checksums(self, :vote)
    @svn_debugging = expires
  end

  def undebug_svn
    rpc.config_delete("github.debug-svn-until")
    # Legacy debug flag
    rpc.config_delete("github.debugsvn")
    GitHub::DGit::Maintenance.safely_recompute_checksums(self, :vote)
    @svn_debugging = false
  end

  def svn_toggle_debugging
    if svn_debugging?
      undebug_svn
    else
      debug_svn
    end
  end
end
