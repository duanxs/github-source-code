# frozen_string_literal: true

module Repository::ConfigRepoDependency
  delegate :preferred_readme, to: :default_directory, allow_nil: true

  def user_configuration_repository?
    async_user_configuration_repository?.sync
  end

  def async_user_configuration_repository?
    async_owner.then do |owner|
      owner&.user? && name.casecmp?(owner.config_repo_name)
    end
  end

  def profile_configuration_file
    return @profile_configuration_file if defined?(@profile_configuration_file)
    @profile_configuration_file = if user_configuration_repository?
      tree_entry = preferred_file(:profile_configuration)
      ProfileConfigurationFile.new(tree_entry) if tree_entry
    end
  end

  def has_profile_configuration_file?
    profile_configuration_file.present?
  end

  def has_readme?
    return false unless default_directory
    default_directory.has_readme?
  end

  def default_directory
    return @default_directory if defined?(@default_directory)
    @default_directory = directory(default_branch)
  end

  def configuration_repository_readme_template
    <<~MARKDOWN
      ### Hi there 👋

      <!--
      **#{escape_generated_readme_html(nwo)}** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

      Here are some ideas to get you started:

      - 🔭 I’m currently working on ...
      - 🌱 I’m currently learning ...
      - 👯 I’m looking to collaborate on ...
      - 🤔 I’m looking for help with ...
      - 💬 Ask me about ...
      - 📫 How to reach me: ...
      - 😄 Pronouns: ...
      - ⚡ Fun fact: ...
      -->
    MARKDOWN
  end
end
