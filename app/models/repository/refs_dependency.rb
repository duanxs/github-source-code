# rubocop:disable Style/FrozenStringLiteralComment

module Repository::RefsDependency
  # Public: Retrieve a repository's branch and tag refs.
  #
  # Returns a Git::Ref::Collection object for accessing Ref objects.
  def refs
    return @refs if defined?(@refs)
    @refs = Git::Ref::Collection.new(loader: refs_loader("default"))
  end

  # Public: Retrieve all user-visible refs in the repository including those outside
  # of the heads and tags namespaces. This can be fairly expensive on
  # repositories with many pull requests since special refs are used to maintain
  # the pull request's head and merge info.
  #
  # Returns a Git::Ref::Collection object for accessing Ref objects.
  def extended_refs(prefix = nil)
    return @extended_refs[prefix] if defined?(@extended_refs)

    @extended_refs = Hash.new do |hash, key|
      hash[key] = Git::Ref::Collection.new(loader: refs_loader("extended"), prefix: key)
    end

    @extended_refs[prefix]
  end

  # Public: Retrieve all refs in the repository. This can be fairly expensive on
  # repositories with many pull requests since special refs are used to maintain
  # the pull request's head and merge info.
  #
  # Returns a Git::Ref::Collection object for accessing Ref objects.
  def all_refs
    return @all_refs if defined?(@all_refs)
    @all_refs = Git::Ref::Collection.new(loader: refs_loader)
  end

  # Public: Retrieve a ref collection for all branch refs in the repository.
  #
  # Returns a Git::Ref::Collection object with the prefix set to "refs/heads/".
  def heads
    return @heads if defined?(@heads)
    @heads = Git::Ref::Collection.new(loader: refs_loader("default"), prefix: "refs/heads/")
  end

  # Public: Retrieve a tag collection for all tag refs in the repository.
  #
  # Returns a Git::Ref::Collection object with the prefix set to "refs/tags/".
  def tags
    return @tags if defined?(@tags)
    @tags = Git::Ref::Collection.new(loader: refs_loader("default"), prefix: "refs/tags/", order: :desc)
  end

  # Public: Retrieve a tag collection sorted in chronological order for
  # all tag refs in the repository.
  #
  # Returns a Git::Ref::Collection object with the prefix set to "refs/tags/".
  def sorted_tags
    return @sorted_tags if defined?(@sorted_tags)

    @sorted_tags = Git::Ref::Collection.new(
      loader: Git::Tag::SortedLoader.new(self),
      prefix: "refs/tags/",
      order: :desc,
      branch_sort: false,
    )
  end

  private def refs_loader(read_refs_filter = nil)
    return @refs_loader[read_refs_filter] if defined?(@refs_loader)

    @refs_loader = Hash.new do |hash, key|
      hash[key] = Git::Ref::Loader.new(self, key)
    end

    @refs_loader[read_refs_filter]
  end

  # Public: Clear the refs caches. This forces a fetch of refs hash data from
  # the repository on disk the next time #refs or #extended_refs is accessed by
  # any process.
  #
  # This should be called any time a ref is modified to make the change visible
  # to other processes. You typically don't need to worry about this if you're
  # using Ref#update since the refs cache is automatically cleared.
  #
  # Returns nothing.
  def clear_ref_cache
    return if network.nil? # bail out if network no longer exists, can't get rpc

    rpc.clear_repository_reference_key!
    reset_refs
  end

  # Public: Reset the memoized refs data. This forces a fetch of the refs hash
  # from memcached the next time #refs or #extended_refs is accessed.
  #
  # Returns nothing.
  def reset_refs
    remove_instance_variable(:@refs) if defined?(@refs)
    remove_instance_variable(:@all_refs) if defined?(@all_refs)
    remove_instance_variable(:@extended_refs) if defined?(@extended_refs)
    remove_instance_variable(:@heads) if defined?(@heads)
    remove_instance_variable(:@tags) if defined?(@tags)
    remove_instance_variable(:@sorted_tags) if defined?(@sorted_tags)
    remove_instance_variable(:@refs_loader) if defined?(@refs_loader)
    remove_instance_variable(:@default_oid) if defined?(@default_oid)

    # this really shouldn't be necessary but the GitRPC::Client object in
    # development and test environments holds a reference to the
    # Rugged::Repository used to get refs which keeps a cache once read.
    @rpc = nil
  end

  def default_branch_ref
    async_default_branch_ref.sync
  end

  def async_default_branch_ref
    async_default_branch.then do |default_branch|
      heads.find(default_branch)
    end
  end

  # Public: The repository's default branch name. This is configured in the
  # repository's admin section. It's set initially based on the owner's
  # preferred default branch name. It's shown by default on the repository's tree
  # homepage. It's also used by the graphs and other subsystems.
  #
  # Returns the string default branch name for this repository.
  def default_branch
    @default_branch ||= begin
      refname = rpc.read_symbolic_ref("HEAD")
      if refname.starts_with?("refs/heads/")
        refname = refname["refs/heads/".length..-1]
      end
      refname.force_encoding("UTF-8")
    rescue GitRPC::InvalidRepository
      fallback_default_branch
    rescue GitRPC::RepositoryOffline, GitRPC::ConnectionError, Repository::UnroutedError, GitHub::DGit::UnroutedError => e
      Failbot.report!(e, app: "github-unrouted")
      fallback_default_branch
    rescue GitRPC::Error => e
      Failbot.report!(e)
      fallback_default_branch
    end
  end

  private def fallback_default_branch
    if GitHub.flipper[:configurable_repo_default_branch].enabled?(owner)
      owner_default_new_repo_branch
    else
      "master"
    end
  end

  def async_default_branch
    async_network.then do
      default_branch
    end
  end

  # Public: Change the repository's default branch.
  def default_branch=(val)
    update_default_branch(val)
  end

  # Public: Check whether the default branch exists.
  #
  # Returns true if a default branch is set and it exists.
  def default_branch_exists?
    default_branch && heads.include?(default_branch)
  end

  # Public: Repository supports protected branches (unlike Unsullied::Wiki and Gist)
  def supports_protected_branches?
    true
  end

  # Public: Update a symbolic ref, while holding the dgit lock if the
  # repo is in dgit.
  def update_symbolic_ref(ref, new_value)
    # dgit-update does not deal with symrefs, so we perform the lock
    # here manually
    GitHub::DGit.with_dgit_lock(self, :vote) do
      rpc.update_symbolic_ref(ref, new_value)
    end.tap { reset_memoized_attributes }
  end

  # Public: Change the repository's default branch.
  #
  # branch - Branch name string like "master", "topic", etc.
  #
  # Returns Boolean - true if the branch was changed, falsey otherwise
  def update_default_branch(branch)
    return false unless heads.exist?(branch)

    old_branch = default_branch
    return false if old_branch == branch

    new_ref = "refs/heads/#{branch}"
    update_symbolic_ref("HEAD", new_ref)

    instrument_search_default_branch_update new_ref
    Search.add_to_search_index("code", self.id, "purge" => true)
    Search.add_to_search_index("commit", self.id, "purge" => true)

    enqueue_set_license_job
    RepositoryDependencyManifestInitializationJob.perform_later(id)
    CommunityProfile.enqueue_health_check_job(repository)
    CommitContribution.backfill(self, true)

    instrument_default_branch_update old: old_branch, branch: default_branch

    # Update memoized value
    @default_branch = branch
    true
  end

  # Public: Determine the optimal base branch for a given branch in the current
  # repository. This is used primarily for comparisons when a head branch is given
  # without a base branch.
  #
  #   branch - The String branch name to find an optimal base for.
  #   user   - The user whose access controls the parent repository's visibility.
  #
  # Returns a String extended ref describing the optimal base commit. The ref
  # may include a ':' in which case the ref exists in a different user
  # repository.
  def base_branch(branch, user = nil)
    if base_branch_parent_check?(user)
      if parent.heads.include?(branch)
        "#{parent.owner}:#{branch}"
      elsif parent.default_branch
        "#{parent.owner}:#{parent.default_branch}"
      else
        default_branch
      end
    elsif branch == "gh-pages"
      "gh-pages"
    else
      default_branch
    end
  end

  # Internal: Whether the parent should be checked for the comparison base branch
  # See `base_branch`, above
  #
  # Handles caching for repeated calls
  def base_branch_parent_check?(user)
    @base_branch_parent_check ||= {}

    key = user && user.id
    return @base_branch_parent_check[key] if @base_branch_parent_check.include?(key)

    result = parent && ((visibility == parent.visibility) || internal_fork?) &&
      (user.nil? || parent.pullable_by?(user))

    @base_branch_parent_check[key] = result
  end

  # Public: Given a ref, short sha, or sha, returns the 40 character sha it
  # points to. No network operation is performed when a 40 char SHA1 is
  # provided. Otherwise, the local ref cache is consulted before falling back
  # on a call to git-rev-parse.
  #
  # >> @repo.ref_to_sha('master')
  # => "38e5f93b6b419f5ffff620572c19a0c35799f5fd"
  #
  # >> @repo.ref_to_sha('master2')
  # => nil
  #
  # Returns a string oid or nil.
  def async_ref_to_sha(name)
    return Promise.resolve(nil) if name.nil? || name.include?("..")

    # TODO: push this call further down
    async_network.then do
      refs.async_find(name).then do |ref|
        next ref.commit.oid if ref

        # check this after looking the ref up in case the ref just has a name that looks like an OID
        next name if GitRPC::Util.valid_full_sha1?(name)

        async_rpc.then do |rpc|
          begin
            oid = rpc.rev_parse(name)
            oid unless oid.blank?
          rescue GitRPC::BadRepositoryState, GitRPC::Failure, GitRPC::InvalidRepository => boom
            nil
          end
        end
      end
    end
  end

  def ref_to_sha(name)
    async_ref_to_sha(name).sync
  end

  # Deprecated: Array of branch and tag names, not qualified with their fully
  # qualified ref name prefix.
  #
  # Note: This is super inefficient but only used by RefShaPathExtractor which
  # is also really inefficient and will be fixed after refs-next lands.
  def branch_and_tag_names
    tags.names + heads.names
  end

  # Deprecated: The SHA1 oid of the default branch.
  def default_oid
    default_branch_ref&.target_oid
  end

  def master_sha
    default_oid
  end

  # Public: update the default branch if the current one does not exist but
  # others do.
  def adjust_default_branch
    default_exists = !heads.find(default_branch).nil?

    return if default_exists

    new_default_branch = heads.names.first
    update_default_branch(new_default_branch)
  end

  # should be called whenever a ref gets deleted,
  # probably always by the PostReceive job.
  def ref_deleted(ref, pusher_id = nil)
    if ref == "refs/heads/#{default_branch}"
      # they removed the default branch so we have to pick a new one
      # to not break everything. anything but the current default.
      new_default_branch = (heads.names - [default_branch]).first
      update_default_branch(new_default_branch)
    end

    handle_pages_branch_delete(ref)
    handle_pages_deployments_delete(ref)
  end

  def handle_pages_branch_delete(ref)
    if ref == "refs/heads/#{pages_branch}" && page
      # deleting a gh-pages branch means pages get
      # unpublished
      page.destroy
    end
  end

  def handle_pages_deployments_delete(ref)
    return unless page && ref.to_s.start_with?("refs/heads/")
    # deleting a ref deletes its pages deployment
    page.deployments_for(ref.to_s.sub("refs/heads/", "")).destroy_all
  end

  def check_custom_hooks(old_oid, new_oid, qualified_name, options = {})
    return unless has_pre_receive_hooks?
    if options[:no_custom_hooks].nil? && GitHub.pre_receive_hooks_enabled?
      # Try running pre-receive hooks
      refline = "#{old_oid} #{new_oid} #{qualified_name}"
      begin
        sockstat_data = (options[:reflog_data] || {}).merge(repo_pre_receive_hooks: pre_receive_hooks.to_json)
        result = rpc.pre_receive_hook(refline, GitHub::GitSockstat.new(sockstat_data).to_env)
        # XXX tests on linux are sometimes returning 127 (not found?)
        raise Git::Ref::HookFailed, result["out"] + result["err"] if (result["status"] != 0 && result["status"] != 127)
      rescue GitRPC::SpawnFailure => e
        # if the hooks don't exist, that's then we don't have to run them
        raise unless e.message =~ /Errno::ENOENT/
      end
    end
  end

  # Public: Can the given user commit to the named branch?
  #
  # Returns Boolean
  def can_commit_to_branch?(user, name)
    ProtectedBranch.user_can_commit_to_branch?(self, user, name)
  end

  # Public: Protect a branch.
  #
  # name     - String name of the branch to protect
  # creator  - Required User who is establishing branch protection
  # required_status_checks - Optional Hash specifying required status check settings.
  # required_pull_request_reviews - Optional Hash specifying required pull
  #                                   request review  settings.
  # required_signatures - Optional boolean specifying a commit signature requirement.
  # required_linear_history - Optional boolean specifying merge conflict blocking.
  # block_force_pushes - Optional boolean specifying force push blocking.
  # block_deletions - Optional boolean modifying branch deletion rules.
  # enforce_admins: - Optional
  # restrictions - Optional Hash specifying restriction settings.
  #
  # Returns the ProtectedBranch.
  def protect_branch(name, creator:,
                           required_status_checks: nil,
                           required_pull_request_reviews: nil,
                           required_signatures: nil,
                           required_linear_history: nil,
                           block_force_pushes: nil,
                           block_deletions: nil,
                           enforce_admins: nil,
                           restrictions: nil)
    transaction do
      retries = 0
      begin
        protected_branch = protected_branches.where(name: name).first
        if protected_branch.nil?
          protected_branch_from_rule = heads.find(name)&.protected_branch
          protected_branch = if protected_branch_from_rule
            protected_branch_from_rule.deep_copy_as!(name: name, creator: creator)
          else
            protected_branches.create!(name: name, creator: creator)
          end
        end
      rescue ActiveRecord::RecordNotUnique
        retries += 1
        if retries >= 3
          raise
        else
          retry
        end
      end

      if required_status_checks
        protected_branch.update_required_status_checks(**required_status_checks)
      else
        protected_branch.clear_required_status_checks
      end

      if required_pull_request_reviews
        protected_branch.enable_required_pull_request_reviews(**required_pull_request_reviews)
      else
        protected_branch.clear_required_pull_request_reviews
      end

      case required_signatures
      when true
        protected_branch.enable_required_signatures
      when false
        protected_branch.clear_required_signatures
      end

      case required_linear_history
      when true
        protected_branch.enable_required_linear_history
      when false
        protected_branch.clear_required_linear_history
      end

      case block_force_pushes
      when true
        protected_branch.enable_blocked_force_pushes
      when false
        protected_branch.clear_blocked_force_pushes
      end

      case block_deletions
      when true
        protected_branch.enable_blocked_deletions
      when false
        protected_branch.clear_blocked_deletions
      end

      case enforce_admins
      when true
        protected_branch.admin_enforced = true
      when false
        protected_branch.admin_enforced = false
      end

      if restrictions
        protected_branch.update_restrictions(**restrictions)
      else
        protected_branch.clear_restrictions
      end

      protected_branch.save!
      protected_branch
    end
  end

  # Public: Fetches commits from base_ref. Creates a new ref
  #         in the repository pointing to base_ref.target.
  #
  # base_ref     - The ref to fetch from in the source repository
  # new_ref_name - Optional: The name of the new created branch.
  #                Defaults to the name of base_ref
  # user         - The user to use for the ref update operation
  #
  # Returns the created Ref
  def fetch_commits_from(base_ref, new_ref_name: base_ref.name, user:, reflog_data: {})
    new_ref = heads.find_or_build(new_ref_name)

    rpc.fetch_commits(base_ref.repository.shard_path, base_ref.target_oid)

    new_ref.update(base_ref.target_oid, user, {
      priority: :high,
      post_receive: false,
      clear_ref_cache: true,
      no_custom_hooks: false,
      reflog_data: reflog_data,
    })

    new_ref
  end

  include GitRepository::RefsDependency
end
