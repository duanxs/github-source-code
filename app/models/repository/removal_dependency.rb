# rubocop:disable Style/FrozenStringLiteralComment

# Repository removal and destruction
#
# == How to delete a repository
#
# The preferred way to delete a repository in all cases is with the Repository#remove
# method.
#
#   deleter = repository.owner # or whoever
#   repository.remove(deleter)
#
# This method should be fully idempotent and may be called repeatedly in cases
# where a job fails. If removal fails reliably for a repository in any state
# it's a bug and the logic must be modified to handle removing repositories in
# that state.
#
# == Stages of removal
#
# Deleting repositories is complicated due to a variety of factors. The
# operation cascades into a large number of dependent tables. Parent/fork
# relationships must be maintained when intermediate repositories are removed.
# Private repository deletion may bring in financially dependent forks.
#
# Because deletion can be fairly time consuming and resource intense, the
# process happens over two stages. The methods defined in this file model
# these stages:
#
# - The #hide stage runs *within* a web/API request and is designed to be fast,
#   lightweight, and fully transactional. All dependent repositories table
#   records are marked deleted = 1 but no heavy moving of records into archive
#   tables is performed.
#
# - The #archive stage runs within the RepositoryDelete job. This purges git and
#   auxiliary data and then copies repositories table records along with a ton
#   of dependent table (issues, issue_comments, pull_requests, etc etc etc)
#   records into archive tables via SELECT INTO and then deletes the active table
#   records. The entire operation runs within a single transaction so records
#   are either fully moved or not moved at all.
#
# These methods should not typically be called outside of the main #remove
# method.
#
# == Dependent repositories and cascading deletes
#
# When a private repository is removed, it may be necessary to delete a number
# of other repositories in the same network that depend on the repository
# financially. This is determined based on the network root's owner. When a
# private network root is deleted, all forks that descend from that repository
# must also be deleted.
#
# Note that deleting public forks never cascades to child repositories, nor does
# deleting private repositories that are forks of a plan owning repository. Only
# the immediate repository is removed and any forks reparented in those cases.
#
# Since many of a repository's dependent relationships are deleted in the
# background rather than immediately during the initial #archive transaction,
# it's possible for a restore attempt to occur before a repository's related
# tables have been cleared of the associated data. If this occurs, since
# restores happen with transactions of their own, it's likely that an insert
# while restoring archived dependents will fail with a unique key violation. In
# that instance, the restore as a whole will fail.
#
# It's also possible that the dependent deletion jobs didn't succeed and have
# left orphaned data in the tables that is blocking a restore. If this occurs,
# the data-quality-scan script can help. Consult @github/data-quality if you
# have questions.
#
# == Transactions and network integrity
#
# The parent_id column is especially tricky to deal with when deleting
# repositories. A repository not marked as deleted must never refer to a
# repository marked as deleted, although the opposite is not true. There
# must be exactly one non deleted repository with a NULL parent_id in each
# network to act as the root. When deleting intermediate and root repositories,
# any remaining repositories must be reparented so that basic integrity is not
# broken.
#
# All of this is handled during the #hide stage, which marks all effected
# repositories as deleted and reparents remaining repositories within a single
# transaction.
module Repository::RemovalDependency
  extend ActiveSupport::Concern

  # Hide the repository and all dependent objects, and queue a job to perform
  #   the task of deletion.
  #
  # deleter - User that is initiating the deletion. Must be specified.
  #
  # When a fork is removed, its children have their parent set to the parent of the removed fork.
  # The array of these reparented repos is returned.
  def remove(deleter, instrument: true)
    reparented_repos = hide_repository_and_dependents(deleter)
    publish_removed_repository(deleter) if instrument
    publish_search_removed_repository
    enqueue_delete_job
    reparented_repos
  end

  def enqueue_delete_job
    RepositoryDeleteJob.perform_later(id, nil, path)
  end

  # Instruments a push to kafka topic when a repository is deleted
  #
  # deleter - The user responsible for deleting the repository.
  #
  # Returns nothing.
  def publish_removed_repository(deleter)
    GlobalInstrumenter.instrument("repository.deleted", {
      deleted_repository: self,
      actor: deleter,
    })
  end

  # Emits an event to the Geyser codesearch Hydro topic when a repository is deleted.
  #
  def publish_search_removed_repository
    # TODO: the repository_deleted subscriber skips repo checks; we need this until GA
    return unless GitHub.flipper[:geyser_index].enabled?(self) ||
      GitHub.flipper[:geyser_index].enabled?(self.owner)

    payload = {
      change: :DELETED,
      repository: self,
      owner_name: self.owner&.name,
      updated_at: Time.now.utc,
      ref: "refs/heads/#{self.default_branch}",
    }

    GlobalInstrumenter.instrument("search_indexing.repository_deleted", payload)
    GitHub.dogstats.increment("geyser.repo_changed_event.published", tags: ["change_type:deleted"])
  end

  # Checks if use can delete the repository.
  #
  # Returns a reason if the user cannot, otherwise nil
  #
  # user - user attempting to delete the repository
  #
  # Returns symbol or nil.
  def cannot_delete_repository_reason(user)
    return :cant_administer unless self.adminable_by?(user)
    return :not_ready_for_writes unless ready_for_writes?
    return if user.site_admin?
    if owner.organization?
      return :ofac_trade_restricted if owner.has_full_trade_restrictions?

      # "Repository deletion and transfer" global business setting description:
      #
      # If enabled, members with admin permissions for the repository will be
      # able to delete or transfer public and private repositories. If disabled,
      # only organization owners can delete or transfer repositories.
      if GitHub.global_business&.members_can_delete_repositories_policy? &&
        !GitHub.global_business&.members_can_delete_repositories? &&
        !owner.adminable_by?(user)

        return :cant_delete_repos_on_this_appliance
      end

      return :members_cant_delete_repositories unless owner.members_can_delete_repositories? || owner.adminable_by?(user)
    end

    return :ofac_trade_restricted if private? && deletion_restricted_by_trade_controls?(owner)
  end

  # Private: Helper method while we transition between current restrictions and tiered restrictions.
  #
  # Returns Boolean
  def deletion_restricted_by_trade_controls?(owner)
    # Only organizations have tiered restriction for now
    if owner.organization?
      !owner.restriction_tier_allows_feature?(type: :repository)
    else
      owner.has_any_trade_restrictions?
    end
  end

  # Perform the first phase of removing a repository. Sets the deleted flag,
  # recursively hides dependent private forks, and reparents remaining forks
  # under a new root. This runs within the app request (web or API) within a
  # single transaction.
  #
  # deleter - The user responsible for deleting the repository.
  #
  # When a fork is removed, its children have their parent set to the parent of the removed fork.
  # The array of these reparented repos is returned.
  def hide_repository_and_dependents(deleter)
    reload if persisted? # when nested forks are being hidden, we need to reload for correct parent
    reparented_repos = []

    GitHub::SchemaDomain.allowing_cross_domain_transactions do
      transaction do
        new_parent = parent

        # find all repositories that depend on this repository financially and
        # hide all of them.
        repositories = repository_and_dependents(active: true)
        repositories.each { |repo| repo.hide_record(deleter) }

        # Set owner_id nil for all the repo's advisories. This hides them from
        # repo counts but retains them in the Advisory Database.
        # https://github.com/github/github/pull/129699#issuecomment-562643203
        hide_repository_advisories_on_delete

        # find all the workspace repositories that depend on repository and perform
        # their removal process
        RepositoryAdvisory::WorkspaceRepositoryManager.hide_and_remove_all(deleter, repository: self)

        # its possible (though highly unlikely) that the parent depends on this
        # repository financially. make sure we don't reparent everything under a
        # repo we're about to delete in that case.
        new_parent = nil if repositories.include?(new_parent)

        # if we're deleting a repository with a parent, just link all children to
        # the parent instead of this repo. if we're deleting a root repository,
        # things are much more complicated because public and private repos need
        # to be handled differently.
        if new_parent
          reparented_repos = reparent_forks_of_deleted_repositories(repositories, new_parent)
        elsif network.repositories.reload.count > 0
          if root = find_or_elect_public_root_repository
            # We don't return reparented forks here because public forks
            # don't get removed if their parent is inaccessible.
            reparent_public_forks_of_deleted_repositories(repositories, root)
            if (repos = network.repositories.select(&:private?)).size > 0
              extract_private_forks_of_deleted_repositories(repos)
            end
          else
            # if there are repositories left in the network that haven't been marked
            # deleted which aren't public, then the network is either mixed or the
            # forks have an incorrect plan owner. Attempt to extract them in enterprise.
            extract_private_forks_of_deleted_repositories(network.repositories)
          end
        end

        update_owner_repo_locks
        enable_or_disable_owner! if private? && owner&.plan&.per_repository?
      end
      reparented_repos
    end
  end

  # All repositories in this network that "depend" on this repository financially.
  # Only private repositories whose owner is the network owner have dependents. The
  # repositories returned are all financed by this repository and thus must be
  # deleted when this repository is deleted.
  #
  # conditions - Hash of extra conditions to apply when querying for dependent
  #              repositories. This is typically used to select or filter out
  #              deleted repositories.
  #
  # Returns an Array of all Repository objects financed by the current
  # repository, including the current repository. This is always an array with
  # just this repository for public and private fork repositories.
  def repository_and_dependents(conditions = {})
    return [self] if public? || fork?

    conditions = {
      source_id: network_id,
      public: false,
    }.merge(conditions)

    repos = Repository.where("parent_id IS NOT NULL").where(conditions).to_a
    repos.unshift(self)
    repos
  end

  # Internal: Mark this repository for deletion, performing a minimal amount of work.
  # Called by the recursive repository hiding methods. Don't call this directly.
  #
  # deleter - The user responsible for deleting the repository.
  #
  # Returns nothing.
  def hide_record(deleter)
    self.deleted_by_user_id = deleter.id if deleter
    self.deleted_at = Time.now
    self.deleted = true
    self.active = nil
    save!

    storage_adapter.hide

    instrument :destroy, actor: deleter
  end

  # Find an existing non-deleted network root repository or elect a new one from
  # remaining non-deleted repositories. This approach is only used for public
  # repositories since all repositories in the network should extend from the
  # same root. This should only be called within a transaction after all repositories
  # have been marked as deleted.
  #
  # Returns the new root if one was found or elected, nil when no non-deleted
  # public repositories exist in the network.
  def find_or_elect_public_root_repository
    conditions = { public: true }
    if root = network_repositories.network_roots.where(conditions).first
      root
    elsif root = network_repositories.where(conditions).order("id ASC").first
      root.update_attribute :parent_id, nil
      root.update_root_repository(root)
      root
    end
  end

  # Find all repositories whose parent is set to one of the repositories
  # provided and reparent them under an existing or newly elected parent.
  #
  # repositories - Array of Repository objects that have just been marked deleted.
  # new_parent   - The not-deleted repository to reparent under.
  #
  # Returns an array of repositories whose parents were changed.
  def reparent_forks_of_deleted_repositories(repositories, new_parent)
    raise TypeError, "new_parent required" if new_parent.nil?

    sql = github_sql.new <<-SQL, ids: repositories.map(&:id), network_id: network_id
      SELECT * FROM repositories
      WHERE source_id = :network_id AND parent_id IN :ids
    SQL
    sql.models(Repository).each do |repo|
      repo.parent_id = new_parent.id
      repo.update_organization
    end
  end

  # Reparent only public forks of deleted repositories under a new parent. We
  # omit private repositories because reparenting under random public forks
  # doesn't make sense.
  def reparent_public_forks_of_deleted_repositories(repositories, new_parent)
    raise TypeError, "new_parent required" if new_parent.nil?
    Repository.where(source_id: network_id, public: 1, parent_id: repositories.map(&:id))
              .update_all(parent_id: new_parent.id)
  end

  # Extract only private forks of deleted repositories as separate roots. This handles
  # cases where a public root repository is deleted and one or more private
  # forks need to be split off as separate roots. This is only currently valid in
  # Enterprise and will raise if encountered in non-Enterprise mode.
  def extract_private_forks_of_deleted_repositories(repositories)
    if !GitHub.enterprise?
      if repositories.select { |r| r.visibility != visibility }
        raise "Mixed network detected"
      else
        raise "Inconsistent plan_owner detected"
      end
    else
      repositories.each &:extract!
    end
  end

  # Archive records for this repository and all dependent repositories. Moves all
  # related records into archive tables, deletes active records, cleans up
  # contributions data and purges all git storage. The repository and dependents
  # must have been marked for deletion (deleted = 1) by the #remove method prior
  # to #archive being called. This will update the `update_at` timestamp to avoid
  # being prematurely cleaned up by the RepositoryPurgeArchived job.
  #
  # Returns nothing.
  def archive
    network.enable_or_disable_shared_storage_for_private_repositories if network
    repositories = repository_and_dependents(active: nil).reverse

    repositories.each_slice(100) do |batch|
      batch.each do |repo|
        deleter = User.find_by_id(repo.deleted_by_user_id)

        repo.packages.find_each do |package|
          package.package_versions.not_deleted.find_each do |version|
            begin
              version.delete!(actor: deleter, force_delete: true)
            rescue ActiveRecord::ActiveRecordError => e
              Failbot.report(e)
            end
          end
        end

        next repo.archive_record unless deleter

        event = Hook::Event::RepositoryEvent.new repository_id: repo.id, actor_id: deleter.id, action: :deleted, triggered_at: Time.now
        delivery_system = Hook::DeliverySystem.new(event)
        delivery_system.generate_hookshot_payloads

        repo.archive_record

        delivery_system.deliver_later
      end
    end
  end

  # Internal: Removes the git repository from disk and then archives the record
  # and all associations.
  #
  # Returns an Archived::Repository object.
  def archive_record
    throttle do
      PullRequest.sync_outstanding_commits(self)
      storage_adapter.archive
      Archived::Repository.archive(self)
    end
  end

  # Destroy the RepositoryNetwork record when the last repository in the network
  # is deleted. This also deletes the shared storage area from disk when
  # enabled. Called via after_destroy callback.
  def destroy_repository_network
    return if network.nil?
    return if network.repositories.count > 0
    return if network.deleted_repositories.count > 0
    network.destroy
  end

  # Perform the common repository removal logic across the various jobs.
  # Note that this is called from the RepositoryDelete job so it must be idempotent/retryable.
  def remove_via_job
    # Info for debugging failed jobs
    Failbot.push(network_id: network_id, repo_id: id)

    # if the repository isn't marked as deleted, that's pretty troubling;
    # bail out instead of assuming this really should be deleted.
    raise RepositoryNotMarkedForDeletion.new(id) if active?

    # Find all of the installations before the Ability records are destroyed.
    installations = IntegrationInstallation.with_repository(self)

    GitHub::SchemaDomain.allowing_cross_domain_transactions do
      throttle { archive }
    end

    # Queue the background jobs once we know the repository has
    # actually be archived.
    installations.each do |installation|
      IntegrationInstallationRepositoryRemovalJob.perform_later(installation, id)
    end

    CalculateUserStarsCountJob.perform_later(id)

    GitHub.instrument "repo.disk_archive", {repo: self}
    GitHub.dogstats.increment("org.repo.archive_fork") if fork?
  end

  def send_private_fork_deleted_email
    parent_repo_name = self.parent ? self.parent.name_with_owner : self.name
    RepositoryMailer.private_fork_deleted(repo_nwo: self.name_with_owner, parent_nwo: parent_repo_name, repo_owner: self.owner).deliver_later
  end

  # Internal: Intended to be called only by repository removal/archival jobs. Does the data-sanity checks they require.
  #           We only want the repos that we can confirm this user does not have access to, so we return true for those
  #           that are pullable or that throw a known exception.
  def pullable_by_user_or_no_plan_owner?(user)
    Failbot.push(network_id: network_id, repo_id: id)
    ActiveRecord::Base.connected_to(role: :reading) { pullable_by?(user) }
  rescue NoMethodError => e     # Repository#pullable_by? will end up throwing a NoMethodError if we try calling it on a repo that is unable to find its plan_owner
    if plan_owner.nil?
      Failbot.report(e)
      return true
    else
      raise e
    end
  end

  class RepositoryNotMarkedForDeletion < StandardError
    def initialize(repository_id)
      super("Attempt to archive and purge repository (#{repository_id}) not marked for deletion.")
    end
  end

  class_methods do
    # If a RepositoryDelete job fails for any reason, the repositories record is
    # left intact marked deleted = 1. This method is meant to be run at regular
    # intervals from timerd to requeue any deleted records that have been hanging
    # around for a while.
    #
    # count - Maximum number of delete jobs to enqueue.
    #
    # Returns the list of repository objects that were queued up.
    def schedule_failed_delete_jobs(count = 50)
      ids = find_failed_delete_jobs(count)
      find(ids).each do |repo|
        repo.touch
        repo.enqueue_delete_job
      end
    end

    # Find stale deleted records using a readonly replica database. We run this on
    # the replica because the query is fairly expensive (~25s, table scan). Try not
    # to load AR objects since the behavior when switching connections is kind of
    # up in the air.
    #
    # count - Number of repositories to return max.
    # age   - Time when records are considered stale.
    #
    # Returns an array of repository ids as integers.
    def find_failed_delete_jobs(count, age = 2.hours.ago)
      ActiveRecord::Base.connected_to(role: :reading) do
        Repository.find_by_sql(["
        SELECT repositories.id FROM repositories
        LEFT JOIN archived_repositories
          ON repositories.id = archived_repositories.id
        WHERE  archived_repositories.id IS NULL
        AND repositories.active IS NULL
        AND repositories.updated_at < ?
        ORDER BY repositories.id DESC
        LIMIT ? ", age, count]).map(&:id)
      end
    end
  end
end
