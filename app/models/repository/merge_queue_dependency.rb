# frozen_string_literal: true

module Repository::MergeQueueDependency
  extend ActiveSupport::Concern

  included do
    has_many :merge_queues, dependent: :destroy, inverse_of: :repository
  end

  def default_merge_queue
    @default_merge_queue ||= merge_queue_for(branch: default_branch)
  end

  def async_default_merge_queue
    async_default_branch.then do |branch|
      merge_queue_for(branch: branch)
    end
  end

  def merge_queue_for(branch:)
    ActiveRecord::Base.connected_to(role: :writing) do
      MergeQueue.retry_on_find_or_create_error do
        merge_queues.find_by(branch: branch) || merge_queues.create!(branch: branch)
      end
    end
  end
end
