# frozen_string_literal: true

module Repository::CodeScanningDependency
  CODE_SCANNING_WORKFLOW_TEMPLATES = [
    "codeql",
    "rubocop",
    "shiftleft",
    "anchore",
    "ossar",
  ].to_set.freeze

  CODE_SCANNING_MARKETPLACE_WORKFLOW_TEMPLATES = [
    "rubocop",
    "shiftleft",
    "anchore",
    "ossar",
  ].to_set.freeze

  CODE_SCANNING_TILE_DATA = {
    "codeql": {
      tool_name: "CodeQL Analysis",
      by_line: "by GitHub",
      verified: true,
      description: "Security analysis from GitHub for C, C++, C#, Java, JavaScript, TypeScript, Python, and Go developers.",
      template_filepath: ".github/workflows/codeql-analysis.yml",
      enterprise_requirements: [],
    },
    "shiftleft": {
      tool_name: "ShiftLeft Scan",
      by_line: "by ShiftLeft",
      verified: false,
      description: "ShiftLeft Scan is a free open-source security tool for modern DevOps teams",
      template_filepath: ".github/workflows/shiftleft-analysis.yml",
      enterprise_requirements: ["docker"],
    },
    "rubocop": {
      tool_name: "RuboCop Linting",
      by_line: "by arthurnn",
      verified: false,
      description: "A Ruby static code analyzer and formatter, based on the community Ruby style guide",
      template_filepath: ".github/workflows/rubocop-analysis.yml",
      enterprise_requirements: [],
    },
    "anchore": {
      tool_name: "Anchore Container Scan",
      by_line: "by Anchore",
      verified: false,
      description: "Produce container image vulnerability and compliance reports based on the open-source Anchore container image scanner.",
      template_filepath: ".github/workflows/anchore-analysis.yml",
      enterprise_requirements: ["docker"],
    },
    "ossar": {
      tool_name: "OSSAR",
      by_line: "by GitHub",
      verified: true,
      description: "Run multiple open source security static analysis tools without the added complexity with OSSAR (Open Source Static Analysis Runner).",
      template_filepath: ".github/workflows/ossar-analysis.yml",
      enterprise_requirements: [],
    },
  }.freeze

  def code_scanning_disabled_by_feature_flag?
    return false if GitHub.enterprise?
    @code_scanning_disabled ||= GitHub.flipper[:disable_code_scanning].enabled?(self) || GitHub.flipper[:disable_code_scanning].enabled?(owner)
  end

  def code_scanning_enabled?
    return false unless GitHub.code_scanning_enabled? # config flag check
    return false unless owner.present?
    return false if code_scanning_disabled_by_feature_flag?
    return true if owner.advanced_security_enabled? # billing/license check
    return false if GitHub.enterprise? # license is the only way to enable on GHES

    if GitHub::flipper[:code_scanning_enable_on_forks].enabled?(self)
      if root != self && public? && root&.public? && root&.code_scanning_enabled?
        # Public forks of public repos are enabled also if the parent is enabled
        GitHub::Logger.log({fn: "Repository::CodeScanningDependency::code_scanning_enable?",
                            repo: self.id})
        return true
      end
    end

    if owner.organization?
      if private?
        owner.advanced_security_private_beta_enabled?
      else
        owner.advanced_security_public_beta_enabled? || owner.advanced_security_private_beta_enabled?
      end
    else
      public? && owner.advanced_security_public_beta_enabled?
    end
  end

  def show_code_scanning_annotations_ui?(user)
    code_scanning_enabled?
  end

  def code_scanning_async_fgp_by_result(action, user)
    return Promise.resolve(Authzd::DENY) unless user
    Platform::Loaders::Permissions::BatchAuthorize.load(
      action: action,
      actor: user,
      subject: self,
    )
  end

  def code_scanning_readable_by?(user)
    return false unless code_scanning_enabled?

    if GitHub.flipper[:code_scanning_fgp].enabled?(self) &&
       GitHub.flipper[:code_scanning_fgp_read].enabled?(self)
       code_scanning_async_fgp_by_result(:read_code_scanning, user).sync.allow?
    else
      science "repository_fgp_friendly_code_scanning_read" do |e|
        e.context user: user,
                  repo: self

        e.use { writable_by?(user) }
        e.try {
          code_scanning_async_fgp_by_result(:read_code_scanning, user).sync
        }
        e.compare { |control, candidate| control == candidate.allow? }

        # Reduce noise of this experiment by ignoring network failures
        e.ignore { |_control, candidate| Authzd.ignore_error_in_science?(candidate) }
      end
    end
  end

  # returns a boolean if user has write access
  # to code scanning feature on the repository
  def code_scanning_writable_by?(user)
    return false unless code_scanning_enabled?

    if GitHub.flipper[:code_scanning_fgp].enabled?(self) &&
       GitHub.flipper[:code_scanning_fgp_write].enabled?(self)
       code_scanning_async_fgp_by_result(:write_code_scanning, user).sync.allow?
    else
      science "repository_fgp_friendly_code_scanning_write" do |e|
        e.context user: user,
                  repo: self

        e.use { writable_by?(user) }
        e.try {
          code_scanning_async_fgp_by_result(:write_code_scanning, user).sync
        }
        e.compare { |control, candidate| control == candidate.allow? }

        # Reduce noise of this experiment by ignoring network failures
        e.ignore { |_control, candidate| Authzd.ignore_error_in_science?(candidate) }
      end
    end
  end

  def code_scanning_pr_upload_enabled?
    return false if code_scanning_disabled_by_feature_flag?

    GitHub.flipper[:code_scanning_pr_upload].enabled?(self) || (owner.organization? && GitHub.flipper[:code_scanning_pr_upload].enabled?(owner))
  end

  def code_scanning_integration_api_enabled_for?(actor)
    # Feature gated only by standard access controls on enterprise
    return true if GitHub.enterprise?

    return false if code_scanning_disabled_by_feature_flag?
    GitHub.flipper[:code_scanning_integration].enabled?(actor)
  end

  # Get the default qualified ref names to use for code scanning.
  #
  # This includes all protected branches as well as the default branch.
  def default_code_scanning_ref_names
    @default_code_scanning_ref_names ||= begin
      branches = heads.to_a
      GitHub::PrefillAssociations.for_branches(self, branches)
      protected_ref_names = branches.select(&:protected?).map(&:qualified_name)
      ref_names = [default_branch_ref&.qualified_name] + protected_ref_names
      ref_names = ref_names.compact.map { |ref| ref.dup.force_encoding("UTF-8") }
      ref_names.uniq.select(&:valid_encoding?)
    end
  end

  def code_scanning_analysis_exists?
    code_scanning_counts_hash_cached[:analysis_exists]
  end

  # Get the number of open code scanning alerts for this repository's default
  # refs.
  #
  # Returns the open alerts count or -1 if the request fails/errors.
  def code_scanning_open_alerts_count
    code_scanning_counts_hash_cached[:open_count]
  end

  def code_scanning_marketplace_integrations
    code_scanning_marketplace_integrations ||= CODE_SCANNING_MARKETPLACE_WORKFLOW_TEMPLATES.sort
  end

  def code_scanning_tile_data_for(id)
    CODE_SCANNING_TILE_DATA[id.to_sym]&.merge({ workflow_id: id })
  end

  def code_scanning_workflow_template(id)
    return "" unless id == "codeql" || code_scanning_marketplace_integrations.include?(id)

    @code_scanning_workflow_templates ||= {}

    @code_scanning_workflow_templates[id] ||= begin
      template_path = Rails.root.join("config", "code_scanning", "#{id}.yml")
      template_content = File.read(template_path)

      if id == "codeql"
        # Pick and random hour and day to schedule this for
        template_content = template_content.gsub("cron: '0 0 * * 0'") do |match|
          "cron: '0 #{SecureRandom.rand(24)} * * #{SecureRandom.rand(7)}'"
        end
      end

      # Replace the magic keywords `$default_branch` and `$protected_branches` with a list of default
      # branches.
      template_content = template_content.gsub("$default_branch", default_branch)
      branches = heads.to_a
      GitHub::PrefillAssociations.for_branches(self, branches)
      protected_branches_utf8 = branches.select(&:protected?).map { |ref| ref.name.dup.force_encoding("UTF-8") }
      protected_branches_utf8 = protected_branches_utf8.uniq.select(&:valid_encoding?)
      protected_branches_utf8.delete(default_branch)

      if protected_branches_utf8.present?
        template_content = template_content.gsub("$protected_branches", protected_branches_utf8.join(", "))
      else
        template_content = template_content.gsub("$protected_branches", "")
      end

      template_content = template_content.gsub("$runner_label_any", GitHub.enterprise? ? "self-hosted": "ubuntu-latest")
      template_content = template_content.gsub("$runner_label_windows", GitHub.enterprise? ? "[self-hosted, windows]": "windows-latest")

      template_content
    end
  end

  # Indicates if the code scanning upload endpoint is available to serve requests
  def code_scanning_upload_endpoint_available?
    !(code_scanning_upload_endpoint_globally_disabled? || code_scanning_upload_endpoint_disabled?)
  end

  def refresh_code_scanning_status(alert_numbers: nil, check_run_ids: nil)
    counts = code_scanning_counts_hash
    if counts.present?
      GitHub.kv.set(code_scanning_counts_data_cache_key, counts.to_json)
    end

    if alert_numbers.nil? && check_run_ids.nil?
      raise ArgumentError, "alert_numbers and check_run_ids can't be both nil"
    end

    check_run_ids ||= ActiveRecord::Base.connected_to(role: :reading) do
      CodeScanningAlert.check_run_ids(alert_numbers: alert_numbers, repository: self)
    end

    check_run_ids.each do |check_run_id|
      CreateCodeScanningAnnotationsJob.perform_later(check_run_id: check_run_id)
    end
  end

  private
  def code_scanning_counts_hash
    data = GitHub::Turboscan.counts(
      repository_id: id,
      ref_names: default_code_scanning_ref_names,
    )&.data&.to_h
  end

  def code_scanning_counts_hash_cached
    return @code_scanning_counts_hash if defined? @code_scanning_counts_hash

    v = GitHub.kv.get(code_scanning_counts_data_cache_key).value!
    @code_scanning_counts_hash =
      if v.present?
        GitHub.dogstats.increment("code_scanning.counts_cache.hit")
        JSON.parse(v).symbolize_keys
      else
        v = code_scanning_counts_hash
        empty = false
        if v.nil?
          v = {analysis_exists: false, open_count: 0}
          empty = true
        end
        GitHub.dogstats.increment("code_scanning.counts_cache.miss", tags: ["empty:#{empty}"])
        v
      end
  end


  def code_scanning_counts_data_cache_key
    branch_hash = Digest::MD5.hexdigest(default_code_scanning_ref_names.join)
    "turboscan_counts/#{id}/#{branch_hash}"
  end
end
