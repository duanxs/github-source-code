# rubocop:disable Style/FrozenStringLiteralComment

class Repository
  class Resources < Permissions::FineGrainedResource
    # Internal: Which repository resources are publicly available.
    PUBLIC_SUBJECT_TYPES = %w(
      actions
      administration
      checks
      contents
      content_references
      deployments
      issues
      metadata
      pages
      packages
      pull_requests
      repository_hooks
      repository_projects
      secrets
      security_events
      vulnerability_alerts
      single_file
      statuses
      workflows
    ).freeze

    # Internal: Repository resources which are under preview,
    # and their corresponding feature flag.
    #
    # Anything that should *not* be surfaced to the public
    # should be included here.
    #
    #  Examples
    #
    #   { "checks" => :dat_feature_flag_name }
    #
    # Returns a Hash.
    PREVIEW_SUBJECTS_AND_FEATURE_FLAGS = {
      "composable_comments" => :interactions_api,
      "discussions" => :discussions,
    }.freeze
    # Internal: Which repository resources are under a preview feature flag.
    PREVIEW_SUBJECT_TYPES = PREVIEW_SUBJECTS_AND_FEATURE_FLAGS.keys

    # Internal: Which repository resources are only available in Enterprise.
    ENTERPRISE_SUBJECT_TYPES = %w(repository_pre_receive_hooks).freeze

    # Internal: Which repository resources can an IntegrationInstallation be
    # granted permission on.
    SUBJECT_TYPES = if GitHub.enterprise?
      PUBLIC_SUBJECT_TYPES + PREVIEW_SUBJECT_TYPES + ENTERPRISE_SUBJECT_TYPES
    else
      PUBLIC_SUBJECT_TYPES + PREVIEW_SUBJECT_TYPES
    end

    # Internal: Which repository resources does an IntegrationInstallation get
    # access on, by default
    DEFAULT_PERMISSIONS = {}.freeze

    ABILITY_TYPE_PREFIX = "Repository"
    INDIVIDUAL_ABILITY_TYPE_PREFIX = ABILITY_TYPE_PREFIX
    ALL_ABILITY_TYPE_PREFIX = "User/repositories"

    READONLY_SUBJECT_TYPES = %w(
      metadata
      vulnerability_alerts
    ).freeze

    WRITEONLY_SUBJECT_TYPES = %w(
      composable_comments
      workflows
    )

    ADMINABLE_SUBJECT_TYPES = %w(
      repository_projects
    ).freeze

    # Public: interface for access control to a specific file
    #
    # path - The String of the content path to check access to, within a given repository.
    def file(path)
      IntegrationInstallation::SingleFile.new(repository: repository, path: path)
    end
  end
end
