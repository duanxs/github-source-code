# frozen_string_literal: true

module Repository::SidebarSectionVisibilityDependency
  # KV key for hidden repository sidebar sections
  def sidebar_sections_visibility_key
    "repo:sidebar_section_visibility:#{id}"
  end

  # Internal: Stores the sidebar section visibility settings
  # Expects a hash of section names that have a value of "1" for visible and "0" for hidden.
  # eg. update_sidebar_section_visibility({"releases"=>"1", "packages"=>"1", "environments"=>"0"})
  #
  # returns nil
  def update_sidebar_section_visibility(sections)
    old_settings = sidebar_sections_visibility

    ActiveRecord::Base.connected_to(role: :writing) do
      if sections.present?
        GitHub.kv.set(sidebar_sections_visibility_key, sections.to_json)
      else
        GitHub.kv.del(sidebar_sections_visibility_key)
      end
    end

    # Keep stats on sections that are hidden so we can determine
    # from a product standpoint what sections need work.
    sections.each do |name, value|
      # If old value was "1" or there was no old value (nil)
      if value == "0" && (old_settings[name].nil? || old_settings[name] == "1")
        GitHub.dogstats.increment("edit_repositories.hidden_sidebar_sections.count", sample_rate: 1, tags: ["section:#{name}"])
      elsif value == "1" && old_settings[name] == "0"
        GitHub.dogstats.decrement("edit_repositories.hidden_sidebar_sections.count", sample_rate: 1, tags: ["section:#{name}"])
      end
    end
  end

  def sidebar_sections_visibility
    if data = GitHub.kv.get(sidebar_sections_visibility_key).value { nil }
      JSON.parse(data)
    else
      {}
    end
  end

  # Internal: Check if a single section is visible
  #
  # returns Boolean
  def sidebar_section_enabled?(section)
    visibility = sidebar_sections_visibility

    visibility[section.to_s].nil? || visibility[section.to_s] == "1"
  end
end
