# rubocop:disable Style/FrozenStringLiteralComment

module Repository::PermissionsDependency
  # Public: Checks if a user is authorized to toggle page settings.
  #
  # user - The User to check
  #
  # Returns a Promise<Boolean>
  def async_can_toggle_page_settings?(user)
    return Promise.resolve(false) unless user

    Platform::Loaders::Permissions::BatchAuthorize.load(
      action: :manage_settings_pages,
      actor: user,
      subject: self,
    ).then do |result|
       result.allow?
    end
  end

  # Public: Checks if a user can toggle merge settings for this repo.
  #
  # user - The User to check.
  #
  # Returns a Promise<Boolean>.
  def async_can_toggle_merge_settings?(user)
    return Promise.resolve(false) unless user

    Platform::Loaders::Permissions::BatchAuthorize.load(
      action: :manage_settings_merge_types,
      actor: user,
      subject: self,
    ).then do |result|
        result.allow?
    end
  end

  # Public: Is a user able to set social preview for this repo?
  #
  # user - The User to check.
  # allow_site_admin: if user is site admin, the operation will be granted
  #                   used through stafftools - site admins can change social preview there
  #
  # Returns a Promise<Boolean>.
  def async_can_set_social_preview?(user, allow_site_admin: false)
    return Promise.resolve(false) unless user

    Platform::Loaders::Permissions::BatchAuthorize.load(
      action: :set_social_preview,
      actor: user,
      subject: self,
      context: {
        considers_site_admin: allow_site_admin
      }
    ).then do |result|
      result.allow?
    end
  end

  # Public: Can a user set interaction limits in this repository?
  #
  # actor - The User to check permissions for.
  #
  # Returns a Promise<Boolean>.
  def async_can_set_interaction_limits?(actor)
    return Promise.resolve(false) unless actor.present?

    Platform::Loaders::Permissions::BatchAuthorize.load(
      action: :set_interaction_limits,
      actor: actor,
      subject: self,
    ).then do |result|
        result.allow?
    end
  end

  # Public: Can a user manage webhooks in this repository?
  #
  # actor - The User to check permissions for.
  #
  # Returns a Promise<Boolean>.
  def async_can_manage_webhooks?(actor)
    async_can_manage_webhooks_result(actor).then do |result|
      result.allow?
    end
  end

  # To be removed after Hook.editable_by? experiment is promoted
  def async_can_manage_webhooks_result(actor)
    return Promise.resolve(Authzd::DENY) unless actor.present?

    Platform::Loaders::Permissions::BatchAuthorize.load(
      action: :manage_webhooks,
      actor: actor,
      subject: self,
    )
  end

  # Public: Can a user manage deploy keys this repository?
  #
  # actor - The User to check permissions for.
  #
  # Returns a Promise<Boolean>.
  def async_can_manage_deploy_keys?(actor)
    return Promise.resolve(false) unless actor.present?

    async_owner.then do |owner|
      if GitHub.flipper[:custom_roles].enabled?(owner) && GitHub.flipper[:fgp_manage_deploy_keys].enabled?(owner)
         async_can_manage_deploy_keys_candidate(actor).then do |result|
           result.allow?
         end
      else
        can_manage_deploy_keys_experiment(actor)
      end
    end
  end

  def can_manage_deploy_keys_experiment(actor)
    science "additional_fgps.deploy_keys" do |experiment|
      # Need to call sync here for science
      experiment.use { async_can_manage_deploy_keys_control(actor).sync }
      experiment.try { async_can_manage_deploy_keys_candidate(actor).sync }

      experiment.compare do |control, candidate|
        control == candidate.allow?
      end

      experiment.ignore { |_control, candidate| Authzd.ignore_error_in_science?(candidate) }
    end
  end

  def async_can_manage_deploy_keys_control(actor)
    async_adminable_by?(actor)
  end

  def async_can_manage_deploy_keys_candidate(actor)
    Platform::Loaders::Permissions::BatchAuthorize.load(
      action: :manage_deploy_keys,
      actor: actor,
      subject: self,
    )
  end

  # Public: Can the user edit the description for this repository
  #
  # Returns Boolean
  def can_edit_repo_metadata?(user)
    return false unless user
    ::Permissions::Enforcer.authorize(
      action: :edit_repo_metadata,
      actor: user,
      subject: self,
    ).allow?
  end

  def can_manage_topics?(user)
    async_can_manage_topics?(user).sync
  end

  def async_can_manage_topics?(user)
    return Promise.resolve(false) unless user
    Platform::Loaders::Permissions::BatchAuthorize.load(
      action: :manage_topics,
      actor: user,
      subject: self,
    ).then do |decision|
      decision.allow?
    end
  end

  def async_can_toggle_wiki?(user)
    return Promise.resolve(false) unless user

    Platform::Loaders::Permissions::BatchAuthorize.load(
      action: :manage_settings_wiki,
      actor: user,
      subject: self,
    ).then do |decision|
      decision.allow?
    end
  end

  def can_toggle_wiki?(user)
    async_can_toggle_wiki?(user).sync
  end

  # Public: Can a user enable and disable projects in this repository?
  #
  # actor - The User to check permissions for.
  #
  # Returns a Promise<Boolean>.
  def async_can_toggle_projects?(actor)
    return Promise.resolve(false) unless actor.present?
    Platform::Loaders::Permissions::BatchAuthorize.load(
      action: :manage_settings_projects,
      actor: actor,
      subject: self,
    ).then do |decision|
      decision.allow?
    end
  end

  # Those are the FGPs that grant access to bits of the repository settings page
  # These are used to decide whether to render the settings tab or not
  REPOSITORY_SETTINGS_FGPS = [
    :manage_settings_wiki,
    :manage_settings_projects,
    :manage_settings_merge_types,
    :manage_settings_pages,
    :manage_deploy_keys,
    :manage_webhooks,
    :set_interaction_limits,
    :set_social_preview,
  ]
  # Public: Can a user access one or more repository settings options?
  #
  # actor - The User to check permissions for.
  #
  # Returns a Promise<Boolean>.
  def async_can_view_repository_settings?(actor)
    return Promise.resolve(Authzd::Response.from_decision(Authzd::Proto::Decision.deny)) unless actor.present?

    Platform::Loaders::Permissions::BatchAuthorize.load(
      action: REPOSITORY_SETTINGS_FGPS,
      actor: actor,
      subject: self,
    )
  end
end
