# frozen_string_literal: true

module Repository::AdvisoryDependency
  extend ActiveSupport::Concern

  included do
    has_many :repository_advisories do
      def available_to(actor)
        repo = proxy_association.owner

        # If there is no logged in user and the parent repo is public, we can show
        # published advisories. But if the repo is private, we show nothing.
        unless actor
          # Using Repository#readable_by? rather than Repository#public? takes
          # into account the state of GitHub.private_mode_enabled?.
          return repo.readable_by?(actor) ? published : none
        end

        # If the given actor is a repo admin, they can see all of its advisories.
        return all if repo.adminable_by?(actor)

        # Otherwise, we're dealing with an actor that may or may not have directly
        # granted abilities on some of the repository's advisories. So we look up
        # which advisory are explicitly available to the actor and combine those
        # with all of the repository's published advisories.
        #
        # This returns *all* advisory IDs granted to this actor.
        all_granted_ids = Authorization.service.subject_ids(
          subject_type: RepositoryAdvisory,
          actor: actor,
          through: [Team],
        )

        if all_granted_ids.any?
          # We need to scope down *all* granted advisories to just those that also
          # belong to the repository.
          published.or(where(id: all_granted_ids))
        else
          # If the actor has not been granted any specific abilities on any
          # advisories, then we return just the published advisories for this
          # repository.
          published
        end
      end
    end

    has_one :parent_advisory,
      class_name: "RepositoryAdvisory",
      foreign_key: :workspace_repository_id,
      inverse_of: :workspace_repository
  end

  def advisory_workspace?
    async_advisory_workspace?.sync
  end

  def async_advisory_workspace?
    async_parent_advisory.then(&:present?)
  end

  def parent_advisory_repository
    async_parent_advisory_repository.sync
  end

  def async_parent_advisory_repository
    async_parent_advisory.then { |advisory| advisory&.async_repository }
  end

  # Determines whether the Repository Advisories should be publically
  # available for this repo
  def advisories_enabled?
    GitHub.repository_advisories_enabled?
  end

  # Determines whether an actor is allowed to create and edit Advisories for
  # this repo
  def advisory_management_authorized_for?(actor)
    adminable_by?(actor)
  end

  # Ensures that a new workspace has admin abilities that match the source repo
  def setup_workspace_abilities
    WorkspaceAbilitySetupJob.perform_later(self.id)
  end

  def hide_repository_advisories_on_delete
    repository_advisories.update_all(owner_id: nil)
  end

  def unhide_repository_advisories_on_restore
    repository_advisories.update_all(owner_id: owner_id)
  end
end
