# frozen_string_literal: true

module UserEntitySerializer
  EntityQuery = PlatformHelper::PlatformClient.parse <<-'GRAPHQL'
    fragment AnalyticsUser on Actor {
      __typename
      login
      ... on User  {
        databaseId
        plan {
          displayName
        }
        isSpammy
        isHammy
        suspendedAt
        id
        createdAt
        analyticsTrackingId
      }
      ... on Organization {
        databaseId
        plan {
          displayName
        }
        isSpammy
        id
        createdAt
      }
    }
  GRAPHQL
end
