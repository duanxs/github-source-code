# frozen_string_literal: true

# A commit that occurs as a result of the operations contained within an epoch
class EpochCommit < ApplicationRecord::Collab
  areas_of_responsibility :epochs

  belongs_to :repository
  belongs_to :epoch

  validates :repository, presence: true
  validates :epoch, presence: true
  validates :commit_oid, presence: true
  validates :version_vector, presence: true
end
