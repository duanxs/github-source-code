# frozen_string_literal: true

class RepositoryLabelFixer
  attr_reader :repository

  def initialize(repository)
    @repository = repository
  end

  def duplicate_labels
    duplicate_labels_with_first_id.map { |(_, name, __)| name }
  end

  def fix!
    ids = []
    duplicate_labels_with_first_id.each do |(first_id, name, _)|
      labels_to_delete = @repository.labels.order("ID ASC").where(lowercase_name: name).where("id <> ?", first_id)
      labels_to_delete.includes(:issues).each do |label|
        label.issues.each do |issue|
          issue.events.includes(:issue_event_detail).where(event: "labeled").each do |event|
            event.issue_event_detail.update_column(:label_id, first_id)
          end
        end
      end

      ids += labels_to_delete.pluck(:id)
    end

    @repository.labels.where(id: ids).delete_all
  end

  private

  def duplicate_labels_with_first_id
    @duplicate_labels_with_first_id ||= begin
      sql = Label.github_sql.new(<<-SQL, repository_id: repository.id)
        SELECT id, lowercase_name AS label, COUNT(1) as total
        FROM labels
        WHERE repository_id = :repository_id
        GROUP BY label
        HAVING total > 1
      SQL

      sql.results
    end
  end
end
