# rubocop:disable Style/FrozenStringLiteralComment

require "strscan"

module Search

  class ParsedQuery
    # Public: Parse search query syntax.
    #
    # Parsing preserves query text and term ordering so the original query
    # string can be reconstructed from the parsed structure.
    #
    # There are a few exceptions.
    #
    # 1. Whitespace is normalized. Extra and multiple whitespace characters will
    #    be stripped.
    # 2. Quoting is normalized. Term values with unneeded quotes will be
    #    stripped.
    # 3. Legacy @user and @user/repo syntax is normalized to user: and repo:
    #    terms.
    #
    # str   - The String query
    # terms - Array of qualifier term Symbols (default: [])
    #
    # Examples
    #
    #     parse("parser label:search author:TwP", [:label, :author])
    #     # => ["parser", [:label, "search"], [:author, "TwP"]]
    #
    # Returns an Array of components, where a component is a literal String
    # query or a Array of [Symbol key, String value, Boolean negative].
    def self.parse(str, terms = [], current_user = nil)
      str = str.to_s.strip.scrub!
      scanner = StringScanner.new(str)
      terms_rgxp = /(?:^|\s)(-?)(#{terms.join('|')}):(?:"((?:(?<=\\)"|[^"])*)"|(\S+))/ if terms.any?

      ary = []
      query = ""

      until scanner.eos?
        # look for qualifier terms in the search phrase
        if terms_rgxp && scanner.scan(terms_rgxp)
          ary << query.strip unless query.blank?
          query = ""

          key = scanner[2].to_sym
          if scanner[3]
            value = scanner[3].gsub('\\"', '"')
          else
            value = scanner[4]
          end

          if scanner[1].blank?
            ary << [key, value]
          else
            ary << [key, value, true]
          end

        # look for escaped characters
        elsif scanner.scan(/\\/)
          query << scanner[0]
          query << scanner.getch unless scanner.eos?

        # look for @user/repo
        elsif terms.include?(:repo) && scanner.scan(/(?:^|\s)(-?)@([\w\-]+\/[\w\-.]+)(?=\s|$)/)
          ary << query.strip unless query.blank?
          query = ""

          value = scanner[2]
          if scanner[1].blank?
            ary << [:repo, value]
          else
            ary << [:repo, value, true]
          end

        # look for @user
        elsif terms.include?(:user) && scanner.scan(/(?:^|\s)(-?)@([\w\-]+)(?=\s|$)/)
          ary << query.strip unless query.blank?
          query = ""

          value = scanner[2]
          if scanner[1].blank?
            ary << [:user, value]
          else
            ary << [:user, value, true]
          end

        # look for #topic
        elsif terms.include?(:topic) && scanner.scan(/(?:^|\s)(-?)#([\w\-]+)/)
          ary << query.strip unless query.blank?
          query = ""

          value = scanner[2]
          if scanner[1].blank?
            ary << [:topic, value] # e.g. topic:electron
          else
            ary << [:topic, value, true] # e.g. -topic:electron
          end

        # look for literal strings (inside double quotes)
        elsif scanner.scan(/"/)
          query << scanner[0]
          query << scanner.scan_until(/(^|[^\\])"/).to_s

        # otherwise this is part of the query
        else
          query << scanner.getch
        end
      end

      ary << query.strip unless query.blank?

      ary
    end

    # Public: Generate a search query String given a parsed query Array.
    #
    # ary - Parsed query Array
    #
    # Examples
    #
    #     stringify(["parser", [:label, "search"], [:author, "TwP"]])
    #     # => "parser label:search author:TwP"
    #
    # Returns a search query String.
    def self.stringify(ary)
      ary.map { |component|
        case component
        when String
          component
        when Array
          key, value, negative = component
          value = encode_value(value.to_s)
          "#{'-' if negative}#{key}:#{value}"
        else
          raise TypeError, "unknown query component: #{component.inspect}"
        end
      }.join(" ")
    end

    # Public: Escape the value part of a query String.
    #
    # value - String value fragment
    #
    # Examples
    #
    #     encode_value(%Q{1.0.0: "The Big One"})
    #     # => "1.0.0: \\\"The Big One\\\""
    #     puts _
    #     # => 1.0.0: \"The Big One\"
    #
    # Returns a search query value String
    def self.encode_value(value)
      if /\s|"/.match?(value)
        safe_value = value.gsub('"', '\"')
        %Q{"#{safe_value}"}
      else
        value
      end
    end

    # Public: Select alls terms matching the block in reverse.
    #
    # Iterates over items in reverse since the right most terms override
    # previous values by convention.
    #
    # ary - Parsed query Array
    #
    # Returns the filtered Array in original order.
    def self.filter_terms(ary, &block)
      ary.reverse.select { |component|
        if component.is_a?(Array)
          key, value, negative = component
          yield key, value, negative
        else
          true
        end
      }.reverse
    end

    def self.asciify_emoji(str)
      str.gsub(GitHub::HTML::EmojiFilter.unicodes_pattern) do |unicode|
        if emoji = Emoji.find_by_unicode(unicode)
          emoji.name
        else
          unicode
        end
      end
    end

    # Returns a new Hash of BoolCollection values that can be used safely as a
    # qualifers hash.
    def self.qualifiers
      Hash.new { |h, k| h[k] = BoolCollection.new(k) }
    end

    # The raw search phrase obtained from the user.
    attr_reader :phrase

    # The qualifier terms to parse out of the search phrase.
    attr_reader :terms

    # The query parsed from the search phrase; everything except the qualifier
    # terms.
    attr_reader :query

    # The qualifiers parsed the search phrase; this is a Hash of
    # BoolCollection objects keyed by qualifier name.
    attr_reader :qualifiers

    if GitHub.enterprise?
      # the environment is parsed via the search phrase; this is
      # a String either empty or equal to "local" or "github"
      attr_reader :environment
    end

    # Create a new ParsedQuery.
    #
    # phrase - The raw search phrase as a String
    # terms  - An Array of terms to extract from the search phrase
    # current_user - Currently logged in User to use for @me replacement
    #
    def initialize(phrase, terms = [], current_user = nil)
      @phrase = phrase.to_s
      @terms = terms.flatten.uniq
      @qualifiers = ::Search::ParsedQuery.qualifiers
      @query = parse(current_user)
    end

    # Internal: Parse the query terms and the qualifiers from the raw search
    # phrase. The query terms are used in the query portion of the search, and
    # the qualifiers will be converted into filters.
    #
    # Returns query String from phrase.
    def parse(current_user)
      query = []
      terms = (self.terms + [:repo, :user]).uniq

      self.class.parse(phrase, terms, current_user).each do |component|
        case component
        when String
          query << component
        when Array
          key, value, negative = component
          if key == :environment
            @environment = value
          elsif negative
            qualifiers[key].must_not(value)
          else
            qualifiers[key].must(value)
          end
        end
      end

      query.join(" ")
    end

    # As we are parsing a query phrase, we want to store the various
    # parts of the query based on their required presence in the search
    # results. This class adopts the ElasticSearch boolean notation of using
    # "must", "must_not", and "should" components for storing this data.
    #
    # Values that have to be present in the search results are stored in the
    # `must` array. Values that cannot be present are stored in the `must_not`
    # array. Optional values are stored in the `should` array.
    #
    # Eventually this collection needs to be transformed into a query or
    # filter.
    class BoolCollection

      # The name of the collection as a Symbol (can be nil).
      attr_reader :name

      # Create a new collection for boolean terms.
      #
      # name - An optional collection name
      def initialize(name = nil)
        @name = name
        clear
      end

      # Clear all the stored components: `must`, `must_not`, and `should`.
      #
      # Returns this BoolCollection instance.
      def clear
        @must = nil
        @must_not = nil
        @should = nil
        self
      end

      # Create a copy of this BoolCollection that is independent of this
      # instance. The `must`, `must_not`, and `should` Arrays are duplicated
      # so that the new collection can operate on them without affecting this
      # instance.
      #
      # Returns a new BoolCollection instance.
      def dup
        copy = BoolCollection.new @name

        copy.must     = @must.dup     unless @must.nil?
        copy.must_not = @must_not.dup unless @must_not.nil?
        copy.should   = @should.dup   unless @should.nil?

        copy
      end

      # Removes duplicate values from the `must`, `must_not`, and
      # `should_arrays`.
      #
      # Returns this BoolCollection instance.
      def uniq!
        must.uniq!     if must?
        must_not.uniq! if must_not?
        should.uniq!   if should?

        self
      end

      # Merge the contents of the other BoolCollection into this one. This
      # collection is modified, but the other will remain unchanged.
      #
      # other - The other BoolCollection to merge into this one.
      #
      # Returns this BoolCollection instance.
      # Raises a TypeError if `other` is not a BoolCollection.
      def merge!(other)
        unless other.is_a? BoolCollection
          raise TypeError, "no implicit conversion of #{other.class.name} into #{self.class.name}"
        end

        self.must     = merge_arrays(@must,     other.must)
        self.must_not = merge_arrays(@must_not, other.must_not)
        self.should   = merge_arrays(@should,   other.should)

        self
      end

      # Merge the contents of the other BoolCollection into this one, and
      # return a new collection. Neither collection is modified by this
      # method.
      #
      # other - The other BoolCollection to merge into this one.
      #
      # Returns a new BoolCollection instance.
      # Raises a TypeError if `other` is not a BoolCollection.
      def merge(other)
        unless other.is_a? BoolCollection
          raise TypeError, "no implicit conversion of #{other.class.name} into #{self.class.name}"
        end

        self.dup.merge! other
      end

      # Determine whether the other BoolCollection is equivalent to this one.
      # We require that all the attributes between the two collecitons be the
      # same: `name`, `must` values, `must_not` values, and `should` values.
      #
      # other - The other BoolCollection to test for equality
      #
      # Returns true if the two collections are equal.
      def eql?(other)
        return true if super(other)

        return false unless self.name == other.name
        return false unless self.must == other.must
        return false unless self.must_not == other.must_not
        return false unless self.should == other.should

        true
      end
      alias :== :eql?

      # When a `value` is given, it will be added to the Array of `must`
      # components. When nothing is given the current Array of values is
      # returned.
      #
      # value - The value to add to the Array.
      #
      # Returns nil or the Array of values.
      def must(value = nil)
        case value
        when nil
          @must
        when Array
          @must = Array(@must).concat(value)
        else
          @must = Array(@must) << value
        end
      end

      # Returns `true` if there are any `must` components in this boolean
      # collection.
      def must?
        !@must.nil? && !@must.empty?
      end

      # When a `value` is given, it will be added to the Array of `must_not`
      # components. When nothing is given the current Array of values is
      # returned.
      #
      # value - The value to add to the Array.
      #
      # Returns nil or the Array of values.
      def must_not(value = nil)
        case value
        when nil
          @must_not
        when Array
          @must_not = Array(@must_not).concat(value)
        else
          @must_not = Array(@must_not) << value
        end
      end

      # Returns `true` if there are any `must_not` components in this boolean
      # collection.
      def must_not?
        !@must_not.nil? && !@must_not.empty?
      end

      # When a `value` is given, it will be added to the Array of `should`
      # components. When nothing is given the current Array of values is
      # returned.
      #
      # value - The value to add to the Array.
      #
      # Returns nil or the Array of values.
      def should(value = nil)
        case value
        when nil
          @should
        when Array
          @should = Array(@should).concat(value)
        else
          @should = Array(@should) << value
        end
      end

      # Returns `true` if there are any `should` components in this boolean
      # collection.
      def should?
        !@should.nil? && !@should.empty?
      end

      # Returns `true` if all of the components (must, must_not, should) are
      # empty. Returns `false` if any one of the components contains data.
      def blank?
        return false if must? || must_not? || should?
        true
      end

      # This fun little method will return the values from _all_ the
      # components. This will be a single Array that might be empty.
      # Duplicates are not removed.
      #
      # Array of all values from the `must`, `must_not`, and `should`
      # components.
      def all
        ary = []
        ary.concat must     if must?
        ary.concat must_not if must_not?
        ary.concat should   if should?
        ary
      end

      # Mutate all the values stored in the `must`, `must_not`, and `should`
      # component arrays. The _block_ will be used to change values via the
      # the `Array#map!` method.
      #
      # Returns this BoolCollection.
      def map_all!(&block)
        must.map!(&block).compact!     if must?
        must_not.map!(&block).compact! if must_not?
        should.map!(&block).compact!   if should?
        self
      end

      # Remove the `must_not` values from the `must` and `should` componenets.
      #
      # Returns this BoolCollection.
      def intersect!
        return self unless must_not?
        @must   -= must_not if must?
        @should -= must_not if should?
        self
      end

      # Internal setter for the `must` attribute.
      attr_writer :must

      # Internal setting for the `must_not` attribute.
      attr_writer :must_not

      # Internal setting for the `should` attribute.
      attr_writer :should

    protected

      # Internal: Helper method that will merge two Arrays and return the
      # result. The second Array will be merged into the first and the first
      # will be returned. If the first Array is nil then the second Array is
      # duplicated.
      #
      # ary1 - The first Array (or nil)
      # ary2 - The second Array (or nil)
      #
      # Returns nil, the first Array, or a duplicate of the second Array.
      def merge_arrays(ary1, ary2)
        return if ary1.nil? && ary2.nil?
        return ary2.dup if ary1.nil?
        return ary1     if ary2.nil?

        ary1.concat ary2
      end
    end

  end
end
