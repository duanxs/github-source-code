# frozen_string_literal: true

module Search
  module Filters
    # An issue project filter is used to limit search results to issues that
    # are attached to cards in a particular project. The document must contain
    # a `projects` field in order for this filter to be used.
    class IssueProjectFilter < ::Search::Filter
      # Create a new ProjectFilter.
      #
      # opts - The options Hash
      #
      def initialize(opts = {})
        super(opts)

        @current_user = options.fetch(:current_user, nil)
        @field = :project_ids

        map_bool_collection
      end

      def invalid_reason
        "An invalid project was specified."
      end

      # Internal: Build a filter Hash from the given set of values.

      # values - The Array of values from which to build the filter

      # Returns a filter Hash or nil.
      def build(values)
        return if values.blank?

        if values.first == :missing
          { bool: { must_not: { exists: { field: @field } } } }
        else
          build_term_filter(@field, values)
        end
      end

      # Interal: Override the superclass method and make it a noop
      # implementation. Mapping the boolean collection is not applicable for
      # the issue project filter.
      #
      # Returns this filter's boolean collection.
      def map_bool_collection
        return bool_collection if defined? @mapped
        @mapped = true

        if bool_collection.must && bool_collection.must.include?(:missing)
          bool_collection.clear
          bool_collection.must(:missing)
        else
          pre_filtered_values = bool_collection.all

          bool_collection.map_all! do |val|
            project = project_from(val)
            project && project.id
          end

          # If we've lost any values after filtering, it means at least one of
          # them was invalid.
          @valid = false if pre_filtered_values.size > bool_collection.all.size
        end

        bool_collection
      end

      private

      def project_from(value)
        param = ProjectQueryParam.new(param: value)
        return unless param.valid?

        project_owner = if param.repository_project?
          Repository.with_name_with_owner(param.owner_login, param.repository_name)
        else
          User.find_by(login: param.owner_login)
        end

        return unless project_owner

        project = project_owner.projects.find_by(number: param.number)
        project if project&.readable_by?(@current_user)
      end
    end
  end
end
