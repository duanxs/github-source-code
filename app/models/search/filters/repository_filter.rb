# rubocop:disable Style/FrozenStringLiteralComment

module Search
  module Filters

    # The RepositoryFilter is critical for ensuring that users only see the
    # private repository data that they are allowed to see. Any documents in
    # the search index that contain private data have two fields associated
    # with them: a `public` field and a `repo_id` field.
    #
    # Anyone is allowed to see documents where `public` is true. For
    # everything else, the user needs to have permission to see the document.
    # This is determined by performing database queries and then returning a
    # list of private repository IDs the user is allowed to see. This list is
    # used as a terms filter against the `repo_id` field.
    class RepositoryFilter < ::Search::Filter

      # Maximum size of the generated private repository filter. TODO: Apply limits to other repo filters as well.
      MAX_REPO_FILTER_SIZE = 4_000

      MAX_CONSIDERED_REPOSITORY_IDS = 10_000

      attr_reader :current_user, :user_session, :ip

      attr_reader :repo_id

      # Create a new RepositoryFilter.
      #
      # opts - The options Hash
      #   :current_user - the currently logged in User
      #   :repo_id      - restrict the filter to a single repository
      #   :resource     - restrict the filter to repositories where the user has
      #                   access to the specified resource
      #
      def initialize(opts = {})
        super(opts)

        allowed_resources = Repository::Resources.subject_types
        options.delete(:resource) unless allowed_resources.include?(options[:resource])

        @field ||= :repo_id
        @keys = nil

        @repo_id      = options.fetch(:repo_id, nil)
        @current_user = options.fetch(:current_user, nil)
        @user_session = options.fetch(:user_session, nil)
        @resource     = options.fetch(:resource, "contents")
        @ip           = options.fetch(:ip, nil)

        bool_collection  # initialize our boolean collection
      end

      # This reason can be used when the repository filter is invalid.
      def invalid_reason
        "The listed users and repositories cannot be searched either because the resources do not exist or you do not have permission to view them."
      end

      # Returns `true` if the generated filter includes all public
      # repositories.
      def global?
        !bool_collection.must?
      end

      # Returns nil or a filter Hash.
      def must
        if bool_collection.must?
          build_term_filter(field, bool_collection.must)

        elsif bool_collection.should?
          {bool: {
            should: [
              {term: {public: true}},
              build_term_filter(field, bool_collection.should),
            ].compact,
          }}
        else
          {term: {public: true}}
        end
      end

      # Returns nil or a filter Hash.
      def must_not
        build_term_filter(field, bool_collection.must_not)
      end

      # A `should` filter is not applicable to the repository filter.
      #
      # Returns nil
      def should; end

      # This filter will never be blank.
      #
      # Returns false
      def blank?
        false
      end

      # Override the superclass method and make it into a noop.
      #
      # Returns nil
      def build(values); end

      # For validating search results, this filter provides the set of all
      # accessible repository IDs that were allowed by the search criteria.
      # Results can be validated against this set of repository IDs.
      #
      # Returns a Set containing the accessible repository IDs.
      def accessible_repository_ids
        set = Set.new(bool_collection.must)
        set.merge(bool_collection.should) if bool_collection.should?
        set
      end

      def accessible_business_ids
        return [] unless current_user
        current_user.business_ids - saml_authorization.protected_business_ids
      end

      # Internal: Create the boolean collection that will be used by the
      # repository filter. The :repo and :user/:org keys from the qualifiers
      # hash are used to construct this collection. If there are no :repo or
      # :user/:org entries, then we try to include the current user's private
      # repository IDs.
      #
      # If current_user has access to too many private repositories to
      # efficiently use as a query filter, the list will be truncated, favoring
      # sources over forks and then ordering by a proxy for repo activity.
      #
      # Returns this filter's boolean collection.
      def bool_collection
        return @bool_collection if defined? @bool_collection
        @bool_collection = ::Search::ParsedQuery::BoolCollection.new

        unless repo_id.nil?
          @bool_collection.must repo_id
          return @bool_collection
        end

        # construct the list of repository IDs to include
        @bool_collection.must(repository_ids_from_repo(qualifiers[:repo].must))   # q=repo:foo/bar
        @bool_collection.must(repository_ids_from_user(qualifiers[:user].must))   # q=user:foo
        @bool_collection.must(repository_ids_from_user(qualifiers[:org].must))    # q=org:organization

        # construct the list of repository IDs to exclude
        @bool_collection.must_not(repository_ids_from_repo(qualifiers[:repo].must_not))   # q=-repo:foo/bar
        @bool_collection.must_not(repository_ids_from_user(qualifiers[:user].must_not))   # q=-user:foo
        @bool_collection.must_not(repository_ids_from_user(qualifiers[:org].must_not))    # q=-org:organization
        @bool_collection.must_not.uniq! if @bool_collection.must_not?

        # include the user's accessible private and internal repositories
        private_repos_included = !qualifiers.key?(:is) || qualifiers[:is].must&.first != "public"
        if private_repos_included && !@bool_collection.must? && can_search_private_repositories_for_user?
          repository_ids = current_user.associated_repository_ids(resource: @resource)

          # Any internal repos that are behind SAML-protected orgs will get filtered out below
          # when `protected_repo_ids` are removed from `@bool_collection.should`.
          if accessible_business_ids.any?
            repository_ids.concat Repository.active.joins(:internal_repository).where(
              "internal_repositories.business_id IN (?)", accessible_business_ids).pluck(:id)
          end

          # If we are using an integration, limit the repository ids for the search space
          if current_user.using_auth_via_integration?
            integration  = current_user.oauth_access.application
            installation = current_user.oauth_access.installation

            repository_ids &= integration.accessible_repository_ids(
              current_integration_installation: installation,
              repository_ids: repository_ids,
              permissions: [@resource],
            )
          end

          # Get a (possibly-truncated) list of ids to filter on
          repository_ids = limit_repository_ids(repository_ids)

          @bool_collection.should(repository_ids)
        end

        # for both of the positive filters, remove any repo ids that are
        # protected based on external identities
        if @bool_collection.must?
          @bool_collection.must = @bool_collection.must.uniq - protected_repo_ids
        end
        if @bool_collection.should?
          @bool_collection.should = @bool_collection.should.uniq - protected_repo_ids
        end

        # determine if the user passed in a @user or @user/repo they do not
        # have permission to access
        if qualifiers[:repo].must? || qualifiers[:user].must? || qualifiers[:org].must?
          @valid = @bool_collection.must?
        end

        # prune `must_not` repository IDs from the `must` and `should` lists
        @bool_collection.intersect!

        GitHub.dogstats.histogram("search.query.repository_filter.must", @bool_collection.must.size) if @bool_collection.must?
        GitHub.dogstats.histogram("search.query.repository_filter.should", @bool_collection.should.size) if @bool_collection.should?
        GitHub.dogstats.histogram("search.query.repository_filter.must_not", @bool_collection.must_not.size) if @bool_collection.must_not?

        @bool_collection
      end

      # Internal: Override the superclass method and make it a noop
      # implementation. Mapping the boolean collection is not applicable for
      # the repository filter.
      #
      # Returns this filter's boolean collection.
      def map_bool_collection
        bool_collection
      end

      # Internal: Return all the repository IDs from the `repo` that the
      # `current_user` is allowed to access.
      #
      # repos - Full user/repo String or an Array of Strings
      #
      # Returns an Array of repository IDs.
      def repository_ids_from_repo(repos)
        ids = Array(repos).map do |name_with_owner|
          r = Repository.nwo(name_with_owner)
          next if r.nil?

          next r.id if r.public?

          next unless current_user
          next unless can_search_private_repositories_for_user?
          next unless r.resources.public_send(@resource).readable_by?(current_user)

          next r.id if !current_user.using_auth_via_integration? && Api::AccessControl.oauth_application_policy_satisfied?(current_user, r)

          integration  = current_user.oauth_access.application
          installation = IntegrationInstallation.with_repository(r).find_by(integration: integration)

          next unless r.resources.public_send(@resource).readable_by?(installation)

          r.id
        end

        ids.compact!
        ids
      end

      # Internal: Return all the repository IDs that belong to the `users` that
      # the `current_user` is allowed to access.
      #
      # users - User login as a String or an Array of Strings
      #
      # Returns an Array of Repository IDs.
      def repository_ids_from_user(users)
        ids = []
        return ids if users.blank?

        user_ids = User.where(login: Array(users), spammy: false).pluck(:id)
        ids.concat Repository.where(active: true, public: true, owner_id: user_ids).pluck(:id)

        if can_search_private_repositories_for_user?
          if accessible_business_ids.any?
            ids.concat Repository.active.where(owner_id: user_ids).joins(
              :internal_repository).where("internal_repositories.business_id IN (?)",
              accessible_business_ids).pluck(:id)
          end

          private_repository_ids = current_user.associated_repository_ids(resource: @resource)
          private_repository_ids &= Repository.private_scope.where(owner_id: user_ids).ids

          if current_user.using_auth_via_integration?
            if private_repository_ids.any?
              integration  = current_user.oauth_access.application
              installation = current_user.oauth_access.installation

              installation_repository_ids = integration.accessible_repository_ids(
                current_integration_installation: installation,
                repository_ids: private_repository_ids,
                permissions: [@resource],
              )

              ids.concat (private_repository_ids & installation_repository_ids)
            end
          else
            ids.concat private_repository_ids
          end
        end

        ids.compact.uniq
      end

      # Private: Return a smaller set of repository_ids if the search space
      # would be too large. Note: This set is sorted to increase the chance
      # of hits for users with access to many repositories.
      #
      # Also only consider the first MAX_CONSIDERED_REPOSITORY_IDS repository
      # IDs if the collection is too large
      def limit_repository_ids(repository_ids)
        scope = Repository.private_scope.where(id: repository_ids.first(MAX_CONSIDERED_REPOSITORY_IDS))

        scope.order(Arel.sql(
          "repositories.parent_id IS NULL DESC," \
          "GREATEST(repositories.updated_at, repositories.pushed_at) DESC," \
          "repositories.id",
        )).limit(MAX_REPO_FILTER_SIZE).ids
      end

      # Internal: Determine whether private repositories can be included in the
      # query. Private repositories can only be included if we have a currently-
      # logged-in user. If we're performing the query for an OAuth request,
      # then we also need 'repo' scope in order to search the user's accessible
      # private repositories.
      #
      # Returns true if the list of accessible repositories should be determined
      #   based on the permissions of +current_user+. Returns false if only
      #   public repositories are accessible.
      def can_search_private_repositories_for_user?
        return false unless current_user
        Api::AccessControl.scope?(current_user, "repo")
      end

      # Private: List of organization names to exclude from results based on
      # whether current user has any required external identities and request
      # is from an allowed IP address.
      #
      # Returns Array of String.
      def protected_org_names
        saml_orgs = saml_authorization.protected_organizations.pluck(:login)
        ip_whitelisting_orgs = ip_whitelisting_authorization.protected_organizations.pluck(:login)

        (saml_orgs + ip_whitelisting_orgs).compact.uniq
      end
      private :protected_org_names

      # Private: List of repo ids to exclude from results based on
      # current user's external identities.
      def protected_repo_ids
        @protected_repo_ids ||= repository_ids_from_user(protected_org_names)
      end
      private :protected_repo_ids

      # Private: Builds a SAML Authorization object for the user, preferring
      # the user's session when available.
      #
      # Returns a Platform::Authorization::SAML object.
      def saml_authorization
        if user_session.present?
          Platform::Authorization::SAML.new(session: user_session)
        else
          Platform::Authorization::SAML.new(user: current_user)
        end
      end
      private :saml_authorization

      # Private: Builds an IP allow list authorization object for the user and
      # IP address.
      #
      # Returns Platform::Authorization::IpWhitelisting.
      def ip_whitelisting_authorization
        Platform::Authorization::IpWhitelisting.new user: current_user, ip: ip
      end
      private :ip_whitelisting_authorization
    end  # RepositoryFilter
  end  # Filters
end  # Search
