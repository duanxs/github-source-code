# frozen_string_literal: true

module Search::Parsers::Geyser
  # A shallow string scanner that builds off of the legacy parser
  module V1
    extend self

    # We need to do some transformations on these fields. Other available
    # qualifiers will be sent as-is to Geyser for its parser to handle.
    FIELDS = %i[language org repo sort user]

    # Parse a query string into component parts
    #
    # query - The query string to parse, using GitHub Search Syntax.
    #
    # Returns an array of ParsedNodes.
    def parse(query)
      # We are using the lightest-touch version of the legacy parser to help
      # separate some things from one another to make our job a little easier.
      # Using the `.parse` class method avoids creating BoolCollection objects
      # from qualifiers and keeps the parsed tokens in the order they appeared
      # in which will help during reserialization.
      parsed_query = Search::ParsedQuery.parse(query, FIELDS)
      parsed_query.map do |leaf|
        case leaf
        when String
          ParsedNode.new(:default, leaf, false)
        when Array
          qualifier = leaf[0]
          value     = leaf[1]
          negated   = leaf[2]
          ParsedNode.new(qualifier, value, negated)
        end
      end
    end

    # A shallow representation of a parsed search node.
    class ParsedNode
      attr_reader   :field, :value
      attr_writer   :valid
      attr_accessor :invalid_reason

      def initialize(field, value, negated = false)
        @field          = field
        @value          = value
        @negated        = negated
        @valid          = true
        @invalid_reason = nil
      end

      def negated?
        @negated ? true : false
      end

      def valid?
        @valid ? true : false
      end

      # Serialize the parsed node back to string form.
      def to_s
        case field
        when :default
          # The legacy parser does not know about negation of anything other
          # than qualifiers, and it will leave quoted phrases as-is.
          value
        else
          qualifier = field
          qualifier = "-#{qualifier}" if negated?

          # The legacy parser strips surrounding quotes from quoted qualifier
          # values. This isn't a perfect way to detect the original intent but
          # it should be enough for the majority of valid qualifier values.
          term   = value
          quoted = term.is_a?(String) && term =~ /\s/
          term   = "\"#{term}\"" if quoted

          "#{qualifier}:#{term}"
        end
      end

      def field?(qualifier)
        field.to_s == qualifier.to_s
      end
    end
  end
end
