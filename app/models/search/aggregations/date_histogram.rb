# rubocop:disable Style/FrozenStringLiteralComment

module Search
  module Aggregations
    class DateHistogram < Aggregation
      def initialize(aggregation)
        @items = aggregation["buckets"]
      end

      alias entries items

      def ==(obj)
        obj.items == items
      end
    end
  end
end
