# rubocop:disable Style/FrozenStringLiteralComment

module Search
  # Normalizes search results from ElasticSearch. Instances of this class are
  # returned from the Query class' execute method.
  #
  class Results
    include Enumerable
    # Creates a new, empty search results. This is handy to use after an error
    # has occurred but you still want a results object.
    def self.empty(error_message: nil, error_details: [])
      new({"hits"=>{"hits"=>[]}}, page: 1, error_message: error_message, error_details: error_details)
    end

    attr_reader :total, :time, :page, :per_page, :max_offset, :timed_out, :max_score, :error_message, :error_details
    attr_accessor :results, :aggregations

    # Create a new Results collection from the ElasticSearch response Hash.
    # The response should be the raw JSON document converted to a Ruby Hash;
    # no other processing needs to be performed.
    #
    # response - The response Hash returned from ElasticSearch.
    # opts     - Optional arguments Hash for pagination.
    #   :page     - The current page number.
    #   :per_page - The number of results per page.
    #
    def initialize(response, opts = {})
      @aggregations = {}
      @time   = response["took"].to_i

      hits = response["hits"]
      if hits.present?
        @total   = hits["total"].to_i
        @results = hits["hits"]
        @max_score = hits.fetch("max_score", 0).to_f
      else
        @total = 0
        @results = []
        @max_score = 0
      end

      @aggregations = response["aggregations"] if response.key? "aggregations"

      @timed_out = response["timed_out"] || false

      @page          = opts.fetch(:page, nil)
      @per_page      = opts.fetch(:per_page, ::Search::Query::PER_PAGE)
      @max_offset    = opts.fetch(:max_offset, ::Search::Query::MAX_OFFSET)
      @error_message = opts.fetch(:error_message, nil)
      @error_details = opts.fetch(:error_details, [])
    end

    def each(&block)
      results.each(&block)
    end

    def empty?
      results.empty?
    end

    def timed_out?
      @timed_out
    end

    def error?
      error_message.present?
    end

    def size
      results.size
    end
    alias :length :size

    def [](index)
      results[index]
    end

    def models
      results.map { |result| result["_model"] }.compact
    end

    # Here be will_paginate compatibility
    def total_entries
      pagination_enabled!

      total < max_offset ? total : max_offset
    end

    def current_page
      pagination_enabled!
      page
    end

    # Assert that we have a page number
    def pagination_enabled!
      return unless page.nil?
      raise ArgumentError,
        "Pagination is not available when `page` is not passed to #{self.class.name}.new."
    end

    # Returns the total number of pages available for these search results.
    #
    # The total number of search results that will ever be returned is
    # capped. The farther you offset into a set of search results, the more
    # work the search index has to do. This adversely affects performance;
    # hence it is capped.
    def total_pages
      pagination_enabled!
      max_pages   = (max_offset / per_page.to_f).ceil
      total_pages = (total / per_page.to_f).ceil
      (total_pages < max_pages) ? total_pages : max_pages
    end

    # Returns the previous page number of nil if there is no previous page.
    def previous_page
      pagination_enabled!
      current_page > 1 ? (current_page - 1) : nil
    end

    # Returns the next page number of nil if there is no next page.
    def next_page
      pagination_enabled!
      current_page < total_pages ? (current_page + 1) : nil
    end

    # The current offset into the actual search results.
    def offset
      pagination_enabled!
      per_page * (current_page - 1)
    end

    # Returns true if the current page is greater than the total number of pages.
    def out_of_bounds?
      pagination_enabled!
      current_page > total_pages
    end

    # Remove entries in the results array for which the block returns true. This
    # means the search index is out of sync with the database so report it to
    # Failbot.
    #
    # block - A block that must accept a single Hit object and return true if
    #         it should be removed from the results array.
    #
    # Returns the Array of missing entries.
    #
    def reject!(&block)
      missing = results.select(&block)
      @results = results - missing
      missing
    end

    # Return the list of language aggregations for which we have a
    # Linguist::Language object.
    def languages
      aggregation = @aggregations["language_id"] || @aggregations["code.language_id"]
      aggregation = aggregation["terms"] if aggregation && aggregation["terms"]

      if aggregation
        aggregation["buckets"].each do |bucket|
          bucket["key"] = Search.language_name_from_id(bucket["key"])
        end
      end

      @languages ||= if languages = aggregation && Search::Aggregations::Terms.new(aggregation)
        Search::Aggregations::Percentages.build(languages).select { |t| t.language.present? }
      else
        []
      end
    end

    # Return the list of state aggregations.
    def states
      @states ||= if state_terms
        Search::Aggregations::Percentages.build(state_terms)
      else
        []
      end
    end

    # Return a hash of counts for any aggregated states that match the query.
    def state_counts
      counts = {}
      if state_terms.present? && state_terms.items.present?
        state_terms.items.each do |state_aggregation|
          counts[state_aggregation["key"]] = state_aggregation["doc_count"]
        end
      end
      counts
    end

    # Return the list of package_type aggregations.
    def package_types
      @package_types ||= if package_type_terms
        Search::Aggregations::Percentages.build(package_type_terms)
      else
        []
      end
    end

    # Return a hash of counts for any aggregated package types that match the query.
    def package_type_counts
      counts = {}
      if package_type_terms.present? && package_type_terms.items.present?
        package_type_terms.items.each do |package_type_aggregation|
          counts[package_type_aggregation["key"]] = package_type_aggregation["doc_count"]
        end
      end
      counts
    end

    # Internal: Grabs information from the results for the hydro instrumention.
    # Currently, pretty straight mapping but will eventually be grabbing
    # information about the models in the results.
    #
    # Returns an array of result information for hydro instrumentation.
    def results_for_hydro
      return unless @results.is_a?(Array)

      @results.map do |result|
        next unless result.is_a?(Hash)
        hydro_payload = {
          search_id: result["_id"],
          index: result["_index"],
          mapping_type: result["_type"],
          score: result["_score"],
        }
        if result_object = result["_model"]
          hydro_payload.merge!({
            id: result_object.id,
            model_name: result_object.class.name.demodulize,
            global_relay_id: result_object.try(:global_relay_id),
            url: build_url(result),
          })
        end
        hydro_payload
      end.compact
    end

    def includes_public_repositories?
      lazy.any? { |result| result.repository_count.positive? }
    end

    private

    def build_url(result)
      source = result["_source"]
      url = result["_model"].try(:permalink)

      case result["_type"]
      when "commit"
        unless source.blank? || url.blank?
          "#{url}/commit/#{source["hash"]}"
        end

      when "code"
        unless source.blank? || url.blank?
          path = source.values_at("commit_sha", "path", "filename").compact.join("/")
          "#{url}/blob/#{path}"
        end

      else
        url
      end
    end

    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def state_terms
      @state_terms ||= begin
        aggregation = @aggregations.dig("state", "terms") || @aggregations["state"]
        Search::Aggregations::Terms.new(aggregation) if aggregation
      end
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def package_type_terms
      @package_type_terms ||= begin
        aggregation = @aggregations.dig("package_type", "terms") || @aggregations["package_type"]
        Search::Aggregations::Terms.new(aggregation) if aggregation
      end
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator
  end
end
