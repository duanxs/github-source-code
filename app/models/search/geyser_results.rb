# frozen_string_literal: true

module Search
  class GeyserResults < Results
    include GitHub::AreasOfResponsibility
    areas_of_responsibility :code_search

    OFFLINE_ERROR_MESSAGE = "Code Search is currently offline for maintenance, please try again later."
    CONNECTION_ERROR_MESSAGE = "Code Search is experiencing a temporary issue, please try again."
    ASSIGNMENT_NOT_FOUND_MESSAGE = "This repository has not yet been indexed, please try again in a few moments."
    DEFAULT_ERROR_MESSAGE = "Code Search could not process your request, please try different search criteria."
    RESULT_TYPE_EXACT = :EXACT
    RESULT_TYPE_AT_LEAST = :AT_LEAST

    # Creates a new, empty search results. This is handy to use after an error has occurred but you still
    # want a results object.
    def self.empty(error_code = nil, error_message = nil)
      new(::Search::Geyser::Responses::Search.empty(error_code, error_message))
    end

    # response - a Search::Geyser::Responses::Search object
    # options  - Optional arguments Hash for pagination.
    #            :page - The page number for paginated search requests. (Default 1)
    #            :per_page - The page size for paginated search requests. (Defaul 10)
    #            :max_offset - Upper limit of the number of results we'll return. (Default 1000)
    #
    # response described in https://github.com/github/geyser/blob/68312f2bcc4aa9aaab3d91f070345f7c72ea67e0/ruby/rpc/search/service_pb.rb#L74-L78
    #
    # Note: The super class assumes response is an Elasticsearch document. We could convert from Twirp to ES.
    # This feels more clean and we get to the same place but be careful about what the super class expects to
    # be defined.
    def initialize(response, options = {})
      @response = response

      if response.results&.any?
        # meta results described in https://github.com/github/geyser/blob/68312f2bcc4aa9aaab3d91f070345f7c72ea67e0/ruby/rpc/search/service_pb.rb#L57-L61
        @total = meta&.results&.count&.to_i || 0

        # results described in https://github.com/github/geyser/blob/68312f2bcc4aa9aaab3d91f070345f7c72ea67e0/ruby/rpc/search/service_pb.rb#L44-L57
        @results = response.results
      else
        @total = 0
        @results = []
      end

      @aggregations = {} # For backwards compatibility w/ classic search results
      @max_offset   = options.fetch(:max_offset, ::Search::Query::MAX_OFFSET)
      @max_score    = 0 # Not applicable to Geyser results
      @page         = options.fetch(:page, nil)
      @per_page     = options.fetch(:per_page, ::Search::Query::PER_PAGE)
      @time         = meta&.es_took_in_millis&.to_i || 0
    end

    def offline?
      !GitHub.geyser_search_enabled?
    end

    # Public: Geyser response metadata.
    #
    # meta described in https://github.com/github/geyser/blob/68312f2bcc4aa9aaab3d91f070345f7c72ea67e0/ruby/rpc/search/service_pb.rb#L66-L74
    def meta
      response.meta
    end

    # Public: Error code of Twirp response from Geyser or nil.
    # Used in GeyserCodeQuery to detect certain errors; not exposed to the user
    #
    # See https://twitchtv.github.io/twirp/docs/errors.html#error-codes
    def error_code
      response.error_code
    end

    def timed_out?
      return false unless response.error.present?
      case response.error_code.to_sym
      when :canceled, :deadline_exceeded, :geyser_timeout_error
        true
      else
        false
      end
    end

    # For backwards compatibility
    alias :timed_out :timed_out?

    # Public: Returns true if there was an assignment not found error,
    # indicating that the repository has not be indexed in Geyser.
    def assignment_not_found?
      return false unless response.error.present?

      response.error_code.to_sym == :not_found
    end

    # Public: The error message to display to users.
    def error_message
      return @error_message if defined?(@error_message)

      @error_message ||= begin
        if offline?
          OFFLINE_ERROR_MESSAGE
        else
          error_translation(response)
        end
      end
    end

    # Public: An array of error details. Used by the standard code results view.
    def error_details
      [error_message]
    end

    # Public: Return an array of TermCount structs with Linguist::Language objects based on the languages
    # represented in the result set.
    #
    # In the `#languages` method definition for the default Results class, a set of Elasticsearch document
    # fragments are first used to create a new Search::Aggregations::Terms instance, which is then used to
    # create a new set of structs via Search::Aggregations::Percentages.build. We just have to make our
    # Geyser language results quack like the final Search::Aggregations::Percentages::TermCount struct.
    # Note that the percentage is not currently used anywhere in the UI so we're going to return 0 for now
    # which saves us a loop over the counts to get the total count.
    def languages
      return @languages if defined?(@languages)

      # languages described in https://github.com/github/geyser/blob/fc9fbf9b4f69b52f8f659fc44db5b9ee7d3851c6/ruby/rpc/search/service_pb.rb#L118-L125
      if response.languages.nil? || response.languages.empty?
        @languages = []
        return @languages
      end

      @languages ||= begin
        response.languages.map do |language_count|
          name = Search.language_name_from_id(language_count.language_id)
          count = language_count.count
          percentage = 0.0
          language = Linguist::Language[name || ""]
          TermCount.new(name, count, percentage, language)
        end
      end
    end

    # meta results type described in https://github.com/github/geyser/blob/68312f2bcc4aa9aaab3d91f070345f7c72ea67e0/ruby/rpc/search/service_pb.rb#L61-L66
    def total_text
      case total == 0 ? nil : meta&.results&.type
      when nil
        "No code results found"
      when RESULT_TYPE_EXACT
        "#{total_with_delimiter} #{"code results".pluralize(total)} found"
      when RESULT_TYPE_AT_LEAST
        "Top #{total_with_delimiter} #{"code results".pluralize(total)} found"
      else
        "Approximately #{total_with_delimiter} #{"code results".pluralize(total)} found"
      end
    end

    # Close-enough clone of Search::Aggregations::Percentages::TermCount struct for use with language filter sidebar
    class TermCount
      attr_reader :term, :count, :percentage, :language

      def initialize(term, count, percentage, language)
        @term = term
        @count = count
        @percentage = percentage
        @language = language
      end

      def platform_type_name
        "LanguageAggregate"
      end
    end

    private

    def response
      @response
    end

    def error_translation(response)
      return unless response.error.present?
      case response.error_code.to_sym
      when :geyser_request_error, :geyser_timeout_error
        CONNECTION_ERROR_MESSAGE
      when :not_found
        ASSIGNMENT_NOT_FOUND_MESSAGE
      else
        if Rails.development?
          response.error_message
        else
          DEFAULT_ERROR_MESSAGE
        end
      end
    end

    def total_with_delimiter
      ActiveSupport::NumberHelper.number_to_delimited(total)
    end
  end
end
