# frozen_string_literal: true

module Search
  module Queries
    # This search subclass builds of its super class to require that the
    # returned users from the search _must_ belong to the given org.
    class OrgMemberQuery < UserQuery

      class InvalidOrgMemberQueryError < StandardError; end
      NO_ORG_SUPPLIED = "no Org supplied to query"
      PER_PAGE_MAX = 100
      MAX_OFFSET = 100

      def initialize(opts = {}, &block)
        opts[:per_page] ||= PER_PAGE_MAX
        opts[:max_offset] ||= MAX_OFFSET

        super(opts, &block)

        @org = opts[:org] || raise(InvalidOrgMemberQueryError.new(NO_ORG_SUPPLIED))
      end

      # Internal: Set qualifiers and filters on search to require that the given
      # org's id appears in the list of :all_org_ids on user records.
      #
      # Returns: a Hash
      def filter_hash
        return @filter_hash if defined? @filter_hash
        super

        attr = :all_org_ids
        qualifiers[attr].must @org.id
        @filter_hash[attr] = builder.term_filter(attr)

        @filter_hash
      end

      # Internal: noop for this search
      #
      # Returns: NilClass
      def build_highlight
        nil
      end

      # Internal: noop for this search
      #
      # Returns: NilClass
      def build_sort
        nil
      end

      # Internal: noop for this search
      #
      # Returns: NilClass
      def build_filter
        nil
      end

      # Internal: noop for this search
      #
      # Returns: NilClass
      def build_aggregations
        nil
      end
    end
  end
end
