# frozen_string_literal: true

module Search
  module Queries
    class PackageDependenciesQuery < ::Search::Query
      FIELDS = [:ecosystem, :is, :license, :name, :org, :severity, :sort, :version].freeze
      UNIQ_FIELDS = [:ecosystem, :is, :name, :severity, :sort, :version].freeze
    end
  end
end
