# frozen_string_literal: true

module Search
  module Queries
    class PoliciesQuery < ::Search::Query
      FIELDS = [:is, :resource].freeze
      UNIQ_FIELDS = [:is, :resource].freeze

      def initialize(query = "")
        super({raw_phrase: query})
      end

      def resource
        last_qualifier_value(:resource)
      end

      def compliance_status
        last_qualifier_value(:is).to_s
      end

      def compliant?
        last_qualifier_value(:is).to_s.casecmp?("compliant")
      end

      def noncompliant?
        last_qualifier_value(:is).to_s.casecmp?("noncompliant")
      end

      # Remove the qualifier name/value if present in the query or
      # add it if it isn't.
      def toggle_qualifier(name:, value:)
        new_query = parsed_query.dup
        if qualifier_selected?(name: name, value: value)
          new_query.reject! { |component| matches_qualifier?(component, name, value) }
        else
          new_query << [name, value]
        end
        self.class.stringify(new_query).presence
      end

      # Replace all components with the given name and replace with a single
      # component with the given name/value.
      def replace_qualifier(name:, value:)
        new_query = parsed_query.dup
        new_query.reject! { |component| qualifier?(component, name) }
        new_query << [name, value] unless value.nil?
        self.class.stringify(new_query).presence
      end

      def qualifier_selected?(name:, value:)
        parsed_query.any? { |component| matches_qualifier?(component, name, value) }
      end

      def contains_qualifier?(name:)
        parsed_query.any? { |component| qualifier?(component, name) }
      end

      def query_string
        self.class.stringify(parsed_query)
      end

      # Adds a value for the given name if no qualifier with that name is already present
      # Unlike most query modification methods, this one updates the existing query rather
      # than returning an updated query string.
      def ensure_qualifier!(name:, value:)
        parsed_query << [name, value] unless contains_qualifier?(name: name)
      end

      def remove_qualifier!(name:, value:)
        parsed_query.delete [name, value]
      end

      private

      def parsed_query
        @parsed_query ||= self.class.parse(user_query)
      end

      def parsed_qualifiers
        @parsed_qualifiers ||= parsed_query.select do |component|
          # Select non-negated qualifiers such as qualifier:value
          component.is_a?(Array) && component.third != true
        end
      end

      def qualifier_values(qualifier)
        parsed_qualifiers.select do |component|
          component.first == qualifier
        end.map(&:second)
      end

      # When queries are normalised, the right-most term is preferred when
      # multiple are given for a unique value. Therefore, return the last
      # qualifier value given for a term.
      #
      # Otherwise, the value for that term shown in parsed_query.query_string
      # will be different to the one actually retrieved from the relevant
      # method in this class.
      def last_qualifier_value(qualifier)
        qualifier_values(qualifier).last
      end

      def negated?(component)
        component.is_a?(Array) && component.third == true
      end

      def qualifier?(component, name)
        component.is_a?(Array) && component.first == name
      end

      def matches_qualifier?(component, name, value)
        qualifier?(component, name) && !negated?(component) && value.to_s.casecmp?(component.second)
      end
    end
  end
end
