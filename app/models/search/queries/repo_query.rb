# rubocop:disable Style/FrozenStringLiteralComment

module Search
  module Queries

    class RepoQuery < ::Search::Query

      # The set of fields that can be queried when performing a repository search
      FIELDS = [:in, :sort, :size, :forks, :fork, :pushed, :user, :org, :repo, :language,
                :license, :created, :followers, :stars, :mirror, :is, :topic, :topics,
                :archived, :"help-wanted-issues", :"good-first-issues"].freeze

      # The mapping of user-facing sort values to the corresponding values used
      # in the search index.
      SORT_MAPPINGS = {
        "stars"   => "followers",
        "forks"   => "forks",
        "updated" => "pushed_at",
        "help-wanted-issues" => "help_wanted_issues_count",
      }.freeze

      IS_VALUES = {
        public: %w[public private internal],
      }.freeze

      # The default sort ordering to use in the absence of a query or any
      # other sort information
      DEFAULT_SORT = %w[stars desc].freeze

      # Construct a RepoQuery. The query can be restricted to a single
      # language by providing a :language option.
      #
      # opts - The options Hash.
      #   :language - The language name or Linguist::Language instance to filter by
      #
      def initialize(opts = {}, &block)
        # @sort = %w[stars desc]   # default sort ordering by stars
        super(opts, &block)

        self.star_search  = opts.fetch(:star_search, false)
        self.star_user    = opts.fetch(:star_user, self.current_user)

        @index = Elastomer::Indexes::Repos.new
        @language = opts.fetch(:language, nil)
        @include_forks = opts.fetch(:include_forks, false)
      end

      # Restrict searches to the current user's constellation of stars + Bool predicate
      attr_accessor :star_search
      def star_search?
        !!star_search
      end

      # The user who's stars would be searched if `star_search` is true
      attr_accessor :star_user

      def global?
        filter_hash[:repo_id].global?
      end

      # Sets the list of source fields that will be returned for each matching
      # search document.
      #
      # value - Array of field names
      #
      def source_fields=(value)
        case value
        when Array, String
          @source_fields = Array(value) << "public"
          @source_fields.uniq!
        when nil, false
          @source_fields = %w[public]
        else
          @source_fields = true
        end
      end

      # Internal: Returns the Array of advanced search qualifiers supported by
      # this query type.
      def qualifier_fields
        FIELDS
      end

      def qualifiers=(value)
        super(value)

        if qualifiers.key?(:is) && qualifiers[:is].must?
          qualifiers[:is].must.each do |value|
            case value.downcase
            when "private"
              qualifiers[:public].clear.must(false)
              qualifiers[:visibility].clear.must("private")
            when "public"
              qualifiers[:public].clear.must(true)
            when "internal"
              qualifiers[:public].clear.must(false)
              qualifiers[:visibility].clear.must("internal")
            end
          end
        end
      end

      # Internal: Returns the Array of fields that support aggregation queries.
      def aggregation_fields
        [:language_id]
      end

      # Internal: Returns a Hash that will be passed as URL params for the query.
      def query_params
        {type: "repository"}
      end

      # Internal: Returns the Array of field names that will be queried.
      def query_fields
        return @query_fields if defined? @query_fields
        @query_fields = []

        search_in.each { |field|
          case field
          when "name";        @query_fields.concat(%w[name^1.2 name.camel name.ngram^0.8 name_with_owner])
          when "description"; @query_fields << "description"
          when "readme";      @query_fields << "readme"
          end
        }

        if @query_fields.empty?
          @query_fields = %w[name^1.2 name.camel name.ngram^0.8 description applied_topics]
          @query_fields << "name_with_owner" if escaped_query.index("/")
        end
        @query_fields
      end

      # Internal: Constructs the actual `:query` portion of the query
      # document. This will later be wrapped in a filtered query if search
      # qualifiers were also used.
      #
      # Returns the search query Hash.
      def query_doc
        query = if escaped_query.present?
          {
            function_score: {
              query: {
                query_string: {
                  query:            escaped_query,
                  fields:           query_fields,
                  default_operator: "AND",
                  use_dis_max:      true,
                },
              },
              score_mode: "multiply",
              functions: [{field_value_factor: {field: "rank", missing: 1}}],
            },
          }
        end

        if filtering_by_topic? # Searching explicitly for an applied topic
          queries = [query, *topic_queries].compact
          {bool: {must: queries}}
        else
          query
        end
      end

      # Internal: Returns the highlight Hash.
      def build_highlight
        fields = {}

        if query_fields.any? { |str| str.starts_with?("name") }
          fields[:name] = { number_of_fragments: 0 }
          fields["name.camel"] = { number_of_fragments: 0 }
          fields["name.ngram"] = { number_of_fragments: 0 }
        end

        if query_fields.any? { |str| str.starts_with?("name_with_owner") }
          fields[:name_with_owner] = { number_of_fragments: 0 }
        end

        if query_fields.any? { |str| str.starts_with?("description") }
          fields[:description] = {
            number_of_fragments: 1,
            fragment_size: 256,
          }
        end

        unless fields.empty?
          { encoder: :html, fields: fields }
        end
      end

      # Internal: Returns the sorting options Array.
      def build_sort
        return if sort.blank?

        build_sort_section(sort, "_score", SORT_MAPPINGS)
      end

      # Returns the default sort ordering to use in the absence of a query or
      # any other sort information.
      def default_sort
        DEFAULT_SORT
      end

      # Internal: We need to use the same filters for the query and for the
      # aggregations with the exception of the language query. Since we are aggregationing
      # on language, applying a language filter would be most unhelpful.
      #
      # Returns the Hash of filter hashes.
      def filter_hash
        return @filter_hash if defined? @filter_hash

        # override the language if created with a language option
        unless @language.nil?
          qualifiers[:language].clear
          qualifiers[:language].must @language
        end

        # the fork filter is a special case
        # by default we will not show forks in the search results - you have
        # to explicitly add "fork:true" or "fork:only" to the search
        qualifiers[:fork].must(false) unless qualifiers[:fork].must? || @include_forks

        filters = {}

        filters[:public] = builder.term_filter(:public)

        filters[:language_id] = builder.language_filter(:language_id, :language_id, :language)

        filters[:license_id] = builder.license_filter(:license_id, :license)

        filters[:visibility] = builder.term_filter(:visibility)

        filters[:fork] = builder.term_filter(:fork, singular: true) { |value|
          case value
          when /\Atrue\z/i; nil
          when /\Aonly\z/i; true
          else false
          end
        }

        filters[:mirror] = builder.term_filter(:mirror, singular: true) { |value|
          case value
          when /\Atrue\z/i; true
          else false
          end
        }

        filters[:archived] = builder.term_filter(:archived, singular: true) { |value|
          case value
          when /\Atrue\z/i; true
          when true; true
          else false
          end
        }

        filters[:forks]      = builder.range_filter(:forks)
        filters[:size]       = builder.range_filter(:size)
        filters[:followers]  = builder.range_filter(:followers, :followers, :stars)

        filters[:created_at] = builder.date_range_filter(:created_at, :created)
        filters[:pushed_at]  = builder.date_range_filter(:pushed_at,  :pushed)
        filters[:repo_id]    = builder.repository_filter(current_user, nil, "metadata", user_session: user_session, ip: remote_ip)
        filters[:num_topics]  = builder.range_filter(:num_topics, :topics)

        filters[:help_wanted_issues_count]  = builder.range_filter(:help_wanted_issues_count, :"help-wanted-issues")
        filters[:good_first_issue_issues_count]  = builder.range_filter(:good_first_issue_issues_count, :"good-first-issues")

        if star_search?
          filters[:star_search] = builder.star_filter(current_user, star_user)
        end

        filters.delete_if { |name, filter| filter.nil? || (filter.valid? && filter.blank?) }

        @filter_hash = filters
      end

      def valid_query?
        return false unless super

        if global? && escaped_query.empty? && !filtering_by_topic?
          filters = filter_hash.keys - [:repo_id, :fork]
          if filters.empty?
            @invalid_reason = ::Search::Query::REASON_EMPTY_QUERY
            return false
          end
        end

        true
      end

      # Internal: Prune results and then run the `normalizer` on the results
      # if one was provided.
      #
      # results - An Array of hits from the search index.
      #
      # Returns the normalized Array of hits.
      def normalize(results)
        prune_results(results) unless @results_pruned
        super(results)
      end

      # Internal: Take the array of repository results and remove those that
      # no longer exist in the database or have been flagged as spammy. We
      # will only perform this pruning in test and production; development
      # mode is for testing out everything.
      #
      # results - An Array of hits from the search index.
      #
      # Returns the pruned Array of hits.
      def prune_results(results)
        repo_ids = results.map { |h| h["_id"] }
        repos = Repository.where(id: repo_ids).index_by(&:id)
        error_reported = false

        ::GitHub::PrefillAssociations.for_repo_search(repos.values)

        results.delete_if do |h|
          repo_id = h["_id"].to_i
          repo = repos[repo_id]
          pub = is_public h

          if h["_type"] != "repository"
            unless error_reported
              GitHub.dogstats.increment("search.query.errors.type", { tags: ["index:" + index.name.to_s] })
              error_reported = true
            end
            true

          elsif repo.nil?
            RemoveFromSearchIndexJob.perform_later("code", repo_id)
            RemoveFromSearchIndexJob.perform_later("repository", repo_id)
            true

          elsif !repo.repo_is_searchable?
            RemoveFromSearchIndexJob.perform_later("repository", repo_id)
            true

          # if the public visibility of the repository has changed
          elsif !pub.nil? && repo.public != pub
            Search.add_to_search_index("repository", repo_id)
            true

          else
            h["_model"] = repo
            !security_validation h
          end
        end
      end

      # Validate that the user is allowed to see the given search result
      # document.
      #
      # doc - The search result Hash
      #
      # Returns boolean indicating visibility.
      def security_validation(doc)
        @include_repos ||= filter_hash[:repo_id].accessible_repository_ids

        repo = doc["_model"]

        return true if repo.public?
        return true if @include_repos.include? repo.id

        business_ids = filter_hash[:repo_id].accessible_business_ids
        return true if business_ids.include? repo.internal_visibility_business_id

        GitHub.dogstats.increment("search.query.errors.security", { tags: ["index:" + index.name.to_s] })
        false
      end

      # Internal: Returns `true`, `false`, or `nil`. nil will only be returned
      # if a public attribute is not included as part of the search result
      # doc.
      #
      # doc - The document Hash returned from the search index.
      #
      def is_public(doc)
        if source = doc["_source"]
          source["public"]
        end
      end

      private

      # Private: Is the user making a query for topics that have been manually applied
      # to repositories, and they're allowed to do so?
      def filtering_by_topic?
        qualifiers[:topic] && qualifiers[:topic].must?
      end

      def topic_query_for(term)
        {
          nested: {
            path: "ranked_hashtags",
            query: {
              function_score: {
                query: {
                  match: { "ranked_hashtags.applied" => term },
                },
                score_mode: "multiply",
                functions: [
                  {
                    field_value_factor: { field: "ranked_hashtags.rank", missing: 1 },
                  },
                ],
              },
            },
          },
        }
      end

      def topic_queries
        qualifiers[:topic].must.map { |term| topic_query_for(term) }
      end
    end  # RepoQuery
  end  # Queries
end  # Search
