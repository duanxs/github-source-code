# frozen_string_literal: true

module Search
  module Queries
    class DiscussionQuery < ::Search::Query
      # The set of fields that can be queried when performing a discussion search
      FIELDS = [
        :in, :sort, :author, :commenter, :involves, :user, :org, :repo, :created,
        :updated, :comments, :is, :category
      ].freeze

      # A set of terms that have mutually exclusive values.
      UNIQ_FIELDS = [
        :author, :sort
      ].freeze

      IS_VALUES = {
        public: %w[public private],
        answered: %[answered unanswered]
      }.freeze

      # The mapping of user-facing sort values to the corresponding values used
      # in the search index.
      SORT_MAPPINGS = {
        "top" => "interaction_score",
        "updated" => "updated_at",
      }.freeze

      # The default sort ordering to use in the absence of a query or any
      # other sort information
      DEFAULT_SORT = %w[updated desc].freeze

      # The maximum number of characters returned in the highlighted fragments.
      FRAGMENT_SIZE = 200

      # The largest possible 4-byte Ruby int value (this is half the
      # largest 4-byte Java int value).
      MAX_DISCUSSION_NUMBER = 1073741824

      def self.normalize(array)
        components = super(array)

        # Use the rightmost 'is':
        is = {}
        components = ParsedQuery.filter_terms(components) do |key, value|
          if key == :is
            IS_VALUES.find do |term, values|
              if !is.key?(term) && values.include?(value)
                is[term] = value
                true
              else
                false
              end
            end
          else
            true
          end
        end

        components
      end

      # Construct a DiscussionQuery. The query can be restricted to a single
      # repository by providing the :repo_id option.
      #
      # opts - The options Hash.
      #   :repo_id  - The Repository ID to filter by
      def initialize(opts = {}, &block)
        super(opts, &block)

        @index    = Elastomer::Indexes::Discussions.new
        @repo_id  = opts.fetch(:repo_id, nil)
      end

      # Internal: Returns the Array of advanced search qualifiers supported by
      # this query type.
      def qualifier_fields
        FIELDS
      end

      def qualifiers=(value)
        super(value)

        if qualifiers.key?(:is) && qualifiers[:is].must?
          qualifiers[:is].must.each do |value|
            case value.downcase
            when "locked"
              qualifiers[:locked].clear.must(true)
            when "unlocked"
              qualifiers[:locked].clear.must(false)
            when "public"
              qualifiers[:public].clear.must(true)
            when "private"
              qualifiers[:public].clear.must(false)
            when "answered"
              qualifiers[:answered].clear.must(true)
            when "unanswered"
              qualifiers[:answered].clear.must(false)
            end
          end
        end
      end

      # Sets the list of source fields that will be returned for each matching
      # search document.
      #
      # value - Array of field names
      #
      def source_fields=(value)
        case value
        when Array, String
          @source_fields = Array(value) << "public"
          @source_fields.uniq!
        when nil, false
          @source_fields = %w[public]
        else
          @source_fields = true
        end
      end

      # Internal: Returns a Hash that will be passed as URL params for the query.
      def query_params
        return @query_params if defined? @query_params
        @query_params = {}
        @query_params[:routing] = routing if routing.present?
        @query_params
      end

      # Internal: Returns the Array of field names that will be queried.
      def query_fields
        return @query_fields if defined? @query_fields
        @query_fields = []

        search_in.each { |field|
          case field
          when "title";    @query_fields << "title^1.5"
          when "body";     @query_fields << "body"
          when "comments"; @query_fields << "comments.body^0.8"
          end
        }

        @query_fields = %w[title^1.5 body comments.body^0.8] if @query_fields.empty?
        @query_fields
      end

      # Internal: Constructs the actual `:query` portion of the query
      # document. This will later be wrapped in a filtered query if search
      # qualifiers were also used.
      #
      # Returns the search query Hash.
      def query_doc
        return if escaped_query.empty?

        q = { function_score: {
          query: { query_string: {
            query: escaped_query,
            fields: query_fields,
            phrase_slop: 10,
            default_operator: "AND",
            use_dis_max: true,
            analyzer: "texty_search",
          }},
          score_mode: "sum",
          functions: [
            { exp: { created_at: {
              scale: "42d",
              decay: 0.5,
            }}},
            { exp: { updated_at: {
              scale: "84d",
              decay: 0.5,
            }}},
          ],
        }}

        # when the user query doesn't specify an `in` qualifier we look for discussion numbers
        # if we find them then we add clauses to look for those explicitly
        if search_in.empty?
          more_clauses = discussion_number(escaped_query)

          if more_clauses.any?
            more_clauses.unshift q
            q = { bool: { should: more_clauses }}
          end
        end

        q
      end

      # Internal: Helper method that will scan the query string looking for
      # Discussion numbers. If any are found, then an Array is returned containing
      # term query hashes, one for each Discussion number found. If no Discussion
      # numbers are found, then an empty Array is returned.
      #
      # query - The full query String (not the phrase).
      #
      # Returns an array of Discussion number term queries.
      def discussion_number(query)
        discussion_number_clauses = query.scan(/(?<=\A|\s)[1-9]\d*(?=\Z|\s)/)

        discussion_number_clauses.map! do |number|
          number = number.to_i
          next unless searchable_discussion_number?(number)

          { term: { number: {
              value: number,
              boost: 100,
          }}}
        end
        discussion_number_clauses.compact!
        discussion_number_clauses
      end

      # Determine if the give number is a valid Discussion number. It must be
      # greater than 0 and must fit in a 4-byte int value to prevent
      # ElasticSearch (and Java) from exploding with a type error.
      #
      # number - The Integer to validate
      #
      # Returns true or false
      def searchable_discussion_number?(number)
        number > 0 && number < MAX_DISCUSSION_NUMBER
      end

      # Internal: Returns the highlight Hash.
      def build_highlight
        fields = {}

        if query_fields.any? { |str| str =~ /^title/ }
          fields[:title] = { number_of_fragments: 0 }  # force the whole title to be included in the fragment
        end

        if query_fields.any? { |str| str =~ /^body/ }
          fields[:body] = { number_of_fragments: 1, fragment_size: FRAGMENT_SIZE }
        end

        if query_fields.any? { |str| str =~ /^comments/ }
          fields["comments.body"] = { number_of_fragments: 1, fragment_size: FRAGMENT_SIZE }
        end

        unless fields.empty?
          { encoder: :html, fields: fields }
        end
      end

      # Internal: Returns the sorting options Array.
      def build_sort
        return if sort.blank?

        ary = build_sort_section(sort, "_score", SORT_MAPPINGS)
        ary.each do |item|
          next unless item.is_a?(Hash)

          key = item.keys.first
          if "updated_at" == key
            order = item[key]
            item[key] = {"order" => order, "unmapped_type" => "date"}
          end
        end

        ary
      end

      # Returns the default sort ordering to use in the absence of a query or
      # any other sort information.
      def default_sort
        DEFAULT_SORT
      end

      # Internal: We need to use the same filters for the query and for the
      # aggregations.
      #
      # Returns the Hash of filter hashes.
      def filter_hash
        return @filter_hash if defined? @filter_hash

        filters = {}

        filters[:public] = builder.term_filter(:public)
        filters[:answered] = builder.term_filter(:answered)
        filters[:locked] = builder.term_filter(:locked)
        filters[:author_id] = builder.user_filter(:user_id, :author)
        filters[:commenter] = builder.user_filter("comments.user_id", :commenter)
        filters[:created_at] = builder.date_range_filter(:created_at, :created)
        filters[:updated_at] = builder.date_range_filter(:updated_at, :updated)
        filters[:num_comments] = builder.range_filter(:num_comments, :comments)
        filters[:category_id] = builder.discussions_category_filter(
          :category_id,
          :category,
          repository_id: @repo_id,
        )

        resource = "issues"
        filters[:repository_id] = builder.repository_filter(current_user, @repo_id, resource,
          user_session: user_session, ip: remote_ip, field: :repository_id)

        filters[:involves] = builder.involves_filter([:user_id, "comments.user_id"], :involves)

        filters.delete_if { |name, filter| filter.nil? || (filter.valid? && filter.blank?) }

        @filter_hash = filters
      end

      # Internal: Take the repository IDs and generate a routing String. This
      # routing string is used to limit our search to the specific set of
      # shards where the discussions are stored.
      #
      # Returns a routing String.
      def routing
        return @routing if defined? @routing
        @routing = @repo_id

        if @repo_id.nil? && !repository_filter.global?
          ids = repository_filter.accessible_repository_ids
          @routing = ids.to_a.join(",") unless ids.length > 200
        end

        @routing
      end

      def global?
        repository_filter.global?
      end

      def repository_filter
        filter_hash[:repository_id]
      end

      def valid_query?
        return false unless super

        if global? && escaped_query.empty?
          filters = filter_hash.keys - [:repository_id]
          if filters.empty?
            @invalid_reason = ::Search::Query::REASON_EMPTY_QUERY
            return false
          end
        end

        true
      end

      # Internal: Prune results and then run the `normalizer` on the results
      # if one was provided.
      #
      # results - An Array of hits from the search index.
      #
      # Returns the normalized Array of hits.
      def normalize(results)
        prune_results(results) unless @results_pruned
        super(results)
      end

      # Internal: Take the array of discussion results and remove those for which
      # the discussion no longer exists or has been flagged as spammy. We will
      # only perform this pruning in test and production; development mode is
      # for testing out everything.
      #
      # results - An Array of hits from the search index.
      #
      # Returns the pruned Array of hits.
      def prune_results(results)
        GitHub.tracer.with_span("Search::Query::DiscussionQuery#prune_results") do |span|
          discussion_ids = []
          error_reported = false

          results.each do |h|
            doc_id = h["_id"].to_i
            case h["_type"]
            when "discussion"; discussion_ids << doc_id
            else
              unless error_reported
                GitHub.dogstats.increment("search.query.errors.type", { tags: ["index:" + index.name.to_s] })
                error_reported = true
              end
            end
          end

          discussions = GitHub.dogstats.time("search", tags: ["index:#{index.name}", "action:prune_results_discussions_lookup"]) do
            Discussion.includes(:user, :repository).where(id: discussion_ids).index_by(&:id)
          end

          results.delete_if do |h|
            doc_id = h["_id"].to_i

            case h["_type"]
            when "discussion"; prune_discussion(h, discussions[doc_id])
            end
          end
        end
      end

      # Internal: Determine if the search result doc should be pruned from the
      # result set based on the existence or spamminess of the corresponding
      # discussion, and the enabled/disabled status of the discussion's parent repository.
      #
      # doc   - The document Hash from ElasticSearch
      # discussion - The corresponding Discussion or nil
      #
      # Returns true or false.
      def prune_discussion(doc, discussion)
        @discussion_repairs ||= {}
        pub = is_public doc

        # The read_attribute here is used for speed to bypass the potentially slow #spammy? method.
        if (discussion.nil? || discussion.user.nil? || discussion.user.read_attribute(:spammy))
          unless Rails.development?
            RemoveFromSearchIndexJob.perform_later("discussion", doc["_id"], doc["_routing"])
          end
          true

        # if the repo has no discussions, or isn't searchable (including marked spammy) then remove from index
        elsif !discussion.parent_repo_is_searchable?
          repo_id = discussion.repository_id
          unless @discussion_repairs[repo_id]
            RemoveFromSearchIndexJob.perform_later("bulk_discussions", repo_id)
            @discussion_repairs[repo_id] = true
          end
          true

        # if the public visibility of the repository has changed, or the user
        # does not have access.
        elsif !pub.nil? && discussion.repository.public != pub
          repo_id = discussion.repository_id
          unless @discussion_repairs[repo_id]
            Search.add_to_search_index("bulk_discussions", repo_id, "purge" => true)
            @discussion_repairs[repo_id] = true
          end
          true

        else
          doc["_model"] = discussion
          !security_validation doc

        end
      end

      # Internal: validate that the user is allowed to see the given search
      # result document.
      #
      # doc - The search result Hash
      #
      # Returns boolean indicating visibility.
      def security_validation(doc)
        @include_repos ||= repository_filter.accessible_repository_ids

        repo = doc["_model"].repository

        return true if repo.public?
        return true if @include_repos.include? repo.id

        business_ids = repository_filter.accessible_business_ids
        return true if business_ids.include? repo.internal_visibility_business_id

        GitHub.dogstats.increment("search.query.errors.security", { tags: ["index:" + index.name.to_s] })
        false
      end

      # Internal: Returns `true`, `false`, or `nil`. nil will only be returned
      # if a public attribute is not included as part of the search result
      # doc.
      #
      # doc - The document Hash returned from the search index.
      #
      def is_public(doc)
        if source = doc["_source"]
          source["public"]
        end
      end
    end
  end
end
