# rubocop:disable Style/FrozenStringLiteralComment

module Search
  module Queries

    class UserQuery < ::Search::Query

      # The set of fields that can be queried when performing a user search
      FIELDS = [:in, :sort, :fullname, :repos, :location, :language, :user, :org, :created, :followers, :type].freeze

      # The mapping of user-facing sort values to the corresponding values used
      # in the search index.
      SORT_MAPPINGS = {
        "followers"    => "followers",
        "joined"       => "created_at",
        "repositories" => "repos",
      }.freeze

      # The default sort ordering to use in the absence of a query or any
      # other sort information
      DEFAULT_SORT = %w[followers desc].freeze

      # Construct a UserQuery. The query can be restricted to a single
      # language by providing a :language option.
      #
      # opts - The options Hash.
      #   :language - The language name or Linguist::Language instance to filter by
      #
      def initialize(opts = {}, &block)
        super(opts, &block)
        @index = Elastomer::Indexes::Users.new
        @language = opts.fetch(:language, nil)
      end

      # Internal: Returns the Array of advanced search qualifiers supported by
      # this query type.
      def qualifier_fields
        FIELDS
      end

      # Internal: Returns the Array of fields that support aggregation queries.
      def aggregation_fields
        [:language_id]
      end

      # Internal: Returns a Hash that will be passed as URL params for the query.
      def query_params
        {type: "user"}
      end

      # Internal: Returns the Array of field names that will be queried.
      def query_fields
        return @query_fields if defined? @query_fields
        @query_fields = []

        search_in.each { |field|
          case field
          when "login"; @query_fields << "login^10" << "login.ngram^0.8"
          when "email"; @query_fields << "email" << "email.plain"
          when "name";  @query_fields << "name^1.2"
          end
        }

        @query_fields = %w[login^10 login.ngram^0.8 email email.plain name^1.2 profile_bio] if @query_fields.empty?
        @query_fields
      end

      # Internal: Constructs the actual `:query` portion of the query
      # document. This will later be wrapped in a filtered query if search
      # qualifiers were also used.
      #
      # Returns the search query Hash.
      def query_doc
        return if escaped_query.empty?

        query = { query_string: {
          query: escaped_query,
          fields: query_fields,
          default_operator: "AND",
          use_dis_max: true,
          analyzer: "lowercase",
        }}

        query = { function_score: {
          query: query,
          score_mode: "multiply",
          functions: [{
            field_value_factor: {
              field: "rank",
              missing: 1,
            },
          }],
        }}

        query
      end

      # Internal: Returns the highlight Hash.
      def build_highlight
        fields = {}

        if query_fields.any? { |str| str =~ /^login/ }
          fields["login.ngram"] = { number_of_fragments: 0 }
          fields[:login]        = { number_of_fragments: 0 }
        end

        if query_fields.any? { |str| str =~ /^email/ }
          fields["email.plain"] = { number_of_fragments: 0 }
          fields[:email]        = { number_of_fragments: 0 }
        end

        if query_fields.any? { |str| str =~ /^name/ }
          fields[:name] = { number_of_fragments: 0 }
        end

        if query_fields.any? { |str| str =~ /^profile_bio/ }
          fields[:profile_bio] = { number_of_fragments: 0 }
        end

        unless fields.empty?
          { encoder: :html,
            require_field_match: true,
            fields: fields }
        end
      end

      # Internal: Returns the sorting options Array.
      def build_sort
        return if sort.blank?

        build_sort_section(sort, "_score", SORT_MAPPINGS)
      end

      # Returns the default sort ordering to use in the absence of a query or
      # any other sort information.
      def default_sort
        DEFAULT_SORT
      end

      # Internal: We need to use the same filters for the query and for the
      # aggregations with the exception of the language query. Since we are aggregationing
      # on language, applying a language filter would be most unhelpful.
      #
      # Returns the Hash of filter hashes.
      def filter_hash
        return @filter_hash if defined? @filter_hash

        # override the language if created with a language option
        unless @language.nil?
          qualifiers[:language].clear
          qualifiers[:language].must @language
        end

        filters = {}

        filters[:language_id] = builder.language_filter(:language_id, :language_id, :language)
        filters[:location]   = builder.query_filter(:location) { |str| str.downcase }
        filters[:name]       = builder.query_filter(:name, :fullname) { |str| str.downcase }
        filters[:user]       = builder.user_filter(:user_id, :user, :org)

        filters[:followers]  = builder.range_filter(:followers)
        filters[:repos]      = builder.range_filter(:repos)
        filters[:created_at] = builder.date_range_filter(:created_at, :created)

        filters[:type] = build_type_filter

        filters.delete_if { |name, filter| filter.nil? || (filter.valid? && filter.blank?) }

        @filter_hash = filters
      end

      def build_type_filter
        return unless qualifiers.key? :type
        return unless qualifiers[:type].must?

        builder.term_filter(:organization, :type, singular: true) { |value|
          case value.downcase
          when "user"; false
          when "org", "organization"; true
          end
        }
      end

      def valid_query?
        return false unless super

        if escaped_query.empty?
          filters = filter_hash.keys
          if filters.empty?
            @invalid_reason = ::Search::Query::REASON_EMPTY_QUERY
            return false
          end
        end

        true
      end

      # Internal: Prune results and then run the `normalizer` on the results
      # if one was provided.
      #
      # results - An Array of hits from the search index.
      #
      # Returns the normalized Array of hits.
      def normalize(results)
        prune_results(results) unless @results_pruned
        super(results)
      end

      # Internal: Take the array of user results and remove those that
      # no longer exist in the database or have been flagged as spammy. We
      # will only perform this pruning in test and production; development
      # mode is for testing out everything.
      #
      # results - An Array of hits from the search index.
      #
      # Returns the pruned Array of hits.
      def prune_results(results)
        user_ids = results.map { |h| h["_id"] }
        users = user_ids.any? ?
          User.includes(:profile).where(id: user_ids).index_by(&:id) :
          []
        error_reported = false

        results.delete_if do |h|
          user_id = h["_id"].to_i
          user = users[user_id]

          if h["_type"] != "user"
            unless error_reported
              GitHub.dogstats.increment("search.query.errors.type", { tags: ["index:" + index.name.to_s] })
              error_reported = true
            end
            true
          elsif (user.nil? || user.spammy || user.mannequin?) && !Rails.development?
            RemoveFromSearchIndexJob.perform_later("user", user_id)
            true
          else
            h["_model"] = user
            false
          end
        end
      end

    end  # UserQuery
  end  # Queries
end  # Search
