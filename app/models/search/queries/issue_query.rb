# rubocop:disable Style/FrozenStringLiteralComment

require "set"

module Search
  module Queries

    class IssueQuery < ::Search::Query

      # The set of fields that can be queried when performing a issue search
      FIELDS = [
        :in, :sort, :author, :assignee, :"team-review-requested", :mentions, :commenter, :involves,
        :milestone, :label, :language, :team, :user, :org, :repo, :created,
        :updated, :merged, :closed, :comments, :type, :state, :is, :no, :status,
        :head, :base, :reactions, :interactions, :project,
        :review, :"reviewed-by", :"review-requested", :archived, :draft, :linked
      ].freeze

      # A set of terms that have mutually exclusive values.
      UNIQ_FIELDS = [
        :assignee, :author, :linked, :milestone, :mentions, :review, :"team-review-requested", :"review-requested", :"reviewed-by", :sort, :state, :type, :status
      ]

      IS_VALUES = {
        type: %w[issue pr],
        state: %w[open closed unlocked locked],
        merged_state: %w[merged unmerged],
        public: %w[public private],
      }

      # The mapping of user-facing sort values to the corresponding values used
      # in the search index.
      SORT_MAPPINGS = {
        "comments" => "num_comments",
        "created" => "created_at",
        "updated" => "updated_at",
        "reactions" => "num_reactions",
        "reactions-+1" => "reactions.+1",
        "reactions--1" => "reactions.-1",
        "reactions-smile" => "reactions.smile",
        "reactions-thinking_face" => "reactions.thinking_face",
        "reactions-heart" => "reactions.heart",
        "reactions-tada" => "reactions.tada",
        "interactions" => "num_interactions",
      }.freeze

      # The default sort ordering to use in the absence of a query or any
      # other sort information
      DEFAULT_SORT = %w[created desc].freeze

      # The maximum number of characters returned in the highlighted fragments.
      FRAGMENT_SIZE = 200

      # The largest possible 4-byte Ruby int value (this is half the
      # largest 4-byte Java int value).
      MAX_ISSUE_NUMBER = 1073741824

      CLOSE_ISSUE_REFERENCE_FILTER_TYPES = %w[issue pull-request].freeze

      class InsecureUserToServerAppQuery < StandardError
        def initialize(*args)
          super("Query must include 'is:issue' or 'is:pull-request'")
        end
      end

      # Internal: Normalize terms in parsed query.
      #
      # Removes duplicate terms preferring the right-most term.
      #
      # ary - Parsed query Array
      #
      # Returns Array.
      def self.normalize(ary)
        components = super(ary)

        # Convert deprecated is:draft -> draft:true, -is:draft -> draft:false
        components.map! do |component|
          key, value, negated = component
          if key == :is && value == "draft"
            [:draft, negated.nil? ? true : false]
          else
            component
          end
        end

        # Convert all state: and type: terms to is:
        components = components.map do |component|
          if component.is_a?(Array)
            if component[0] == :state || component[0] == :type
              component = component.dup
              component[0] = :is
              component
            else
              component
            end
          else
            component
          end
        end

        # Run sort: values through emoji ascii filter
        components.map! do |component|
          key, value = component
          if key == :sort
            [key, ParsedQuery.asciify_emoji(value)]
          else
            component
          end
        end

        # Use the right most is:
        is = {}
        components = ParsedQuery.filter_terms(components) do |key, value|
          if key == :is
            IS_VALUES.find do |term, values|
              if !is.key?(term) && values.include?(value)
                is[term] = value
                true
              else
                false
              end
            end
          else
            true
          end
        end

        seen_label = nil
        components = ParsedQuery.filter_terms(components) do |key, value|
          if key == :no && value == "label"
            if seen_label
              false
            else
              seen_label = :no
              true
            end
          elsif key == :label
            seen_label ||= :yes
            seen_label == :yes
          else
            true
          end
        end

        # Normalize sort order of default terms
        if components == [[:is, "issue"], [:is, "open"]]
          [[:is, "open"], [:is, "issue"]]
        elsif components == [[:is, "pr"], [:is, "open"]]
          [[:is, "open"], [:is, "pr"]]
        else
          components
        end
      end

      # Construct an IssueQuery. The query can be restricted to a single
      # language by providing a :language option. The query can also be
      # restricted to a single repository by providing the :repo_id option.
      #
      # opts - The options Hash.
      #   :language - The language name or Linguist::Language
      #               instance to filter by
      #   :state    - The issue state to filter by ('open' or 'closed')
      #   :repo_id  - The Repository ID to filter by
      #
      def initialize(opts = {}, &block)
        super(opts, &block)

        @index    = Elastomer::Indexes::Issues.searcher
        @language = opts.fetch(:language, nil)
        @state    = opts.fetch(:state, nil)
        @repo_id  = opts.fetch(:repo_id, nil)
        @type     = opts.fetch(:type, nil)
        @allow_insecure_user_to_server_app_query = opts.fetch(:allow_insecure_user_to_server_app_query, true)
      end

      attr_reader :allow_insecure_user_to_server_app_query

      def global?
        repository_filter.global?
      end

      def qualifiers=(value)
        super(value)

        if qualifiers.key?(:is) && qualifiers[:is].must?
          qualifiers[:is].must.each do |value|
            case value.downcase
            when "issue"
              qualifiers[:type].clear.must("issue")
            when "pr", "pull-request"
              qualifiers[:type].clear.must("pull-request")
            when "open"
              qualifiers[:state].clear.must("open")
            when "closed"
              qualifiers[:state].clear.must("closed")
            when "locked"
              qualifiers[:locked].clear.must(true)
            when "unlocked"
              qualifiers[:locked].clear.must(false)
            when "merged"
              qualifiers[:merged_state].clear.must(true)
            when "unmerged"
              qualifiers[:merged_state].clear.must(false)
            when "public"
              qualifiers[:public].clear.must(true)
            when "private"
              qualifiers[:public].clear.must(false)
            when "draft"
              qualifiers[:draft].clear.must(true)
            else
              qualifiers[:label].must(value)
            end
          end
        end

        if qualifiers.key?(:is) && qualifiers[:is].must_not?
          qualifiers[:is].must_not.each do |value|
            if value.downcase == "draft"
              qualifiers[:draft].clear.must(false)
            end
          end
        end

        if qualifiers.key?(:no) && qualifiers[:no].must?
          qualifiers[:no].must.each do |value|
            case value.downcase
            when "label", "labels"
              qualifiers[:label].clear.must(:missing)
            when "milestone", "milestones"
              qualifiers[:milestone].clear.must(:missing)
            when "project"
              qualifiers[:project].clear.must(:missing)
            when "assignee"
              qualifiers[:assignee].clear.must(:missing)
            when "review-requested"
              qualifiers[:"review-requested"].clear.must(:missing)
            when "reviewed-by"
              qualifiers[:"reviewed-by"].clear.must(:missing)
            end
          end
        end

        if qualifiers.key?(:review) && qualifiers[:review].must?
          qualifiers[:review].must.each do |value|
            case value.downcase
            when "changes_requested", "changes-requested"
              qualifiers[:review].clear.must("changes_requested")
            when "approved", "approval"
              qualifiers[:review].clear.must("approved")
            when "rejected"
              qualifiers[:review].clear.must("rejected")
            when "none"
              qualifiers[:review].clear.must("none")
            end
          end
        end

        # NOTE: this should run after "type" qualifier is processed.
        # Parses the "linked" qualifiers
        # And adds "type" qualifier if none presents, as "linked" qualifier requires "type" constraints
        #
        parse_value = lambda do |linked_value, is_negative|
          case linked_value
          when "issue"
            is_negative ? qualifiers[:linked].clear.must_not("issue") : qualifiers[:linked].clear.must("issue")
            qualifiers[:type].clear.must("pull-request") unless qualifiers[:type]&.must?
          when "pr", "pull-request"
            is_negative ? qualifiers[:linked].clear.must_not("pull-request") : qualifiers[:linked].clear.must("pull-request")
            qualifiers[:type].clear.must("issue") unless qualifiers[:type]&.must?
          end
        end
        # parse value for regular linked qualifier
        if qualifiers.key?(:linked) && qualifiers[:linked].must?
          qualifiers[:linked].must.each do |value|
            parse_value.call value, false
          end
        end
        # parse value for negating linked qualifier
        if qualifiers.key?(:linked) && qualifiers[:linked].must_not?
          qualifiers[:linked].must_not.each do |value|
            parse_value.call value, true
          end
        end
      end

      # Sets the list of source fields that will be returned for each matching
      # search document.
      #
      # value - Array of field names
      #
      def source_fields=(value)
        case value
        when Array, String
          @source_fields = Array(value) << "public" << "state"
          @source_fields.uniq!
        when nil, false
          @source_fields = %w[public state]
        else
          @source_fields = true
        end
      end

      # Internal: Returns the Array of advanced search qualifiers supported by
      # this query type.
      def qualifier_fields
        FIELDS
      end

      # Internal: Returns the Array of fields that support aggregation queries.
      def aggregation_fields
        [:language_id, :state]
      end

      # Internal, overriding to increase the timeout for issues and prs
      def default_query_params
        defaults = {
          timeout: "500ms",
        }
        # Set shard preference if configured. See
        # http://www.elasticsearch.org/guide/en/elasticsearch/reference/current/search-request-preference.html#search-request-preference
        defaults[:preference] = preference if preference
        defaults
      end

      # Internal: Returns a Hash that will be passed as URL params for the query.
      def query_params
        return @query_params if defined? @query_params

        types = @type

        if types.nil? && qualifiers.key?(:type)
          types = qualifiers[:type].must.nil? ? nil : qualifiers[:type].must.first
          qualifiers[:type].clear
        end

        @query_params =
            case types
            when "issue", "issues"
              {type: "issue"}
            when "pr", "pull-request"
              {type: "pull_request"}
            else
              {type: %w[issue pull_request]}
            end

        @query_params[:routing] = routing if routing.present?
        @query_params
      end

      # Internal: Returns the Array of field names that will be queried.
      def query_fields
        return @query_fields if defined? @query_fields
        @query_fields = []

        search_in.each { |field|
          case field
          when "title";    @query_fields << "title^1.5"
          when "body";     @query_fields << "body"
          when "comments"; @query_fields << "comments.body^0.8"
          end
        }

        @query_fields = %w[title^1.5 body comments.body^0.8] if @query_fields.empty?
        @query_fields
      end

      # Internal: Constructs the actual `:query` portion of the query
      # document. This will later be wrapped in a filtered query if search
      # qualifiers were also used.
      #
      # Returns the search query Hash.
      def query_doc
        return if escaped_query.empty?

        q = { function_score: {
          query: { query_string: {
            query: escaped_query,
            fields: query_fields,
            phrase_slop: 10,
            default_operator: "AND",
            use_dis_max: true,
            analyzer: "texty_search",
          }},
          score_mode: "sum",
          functions: [
            { exp: { created_at: {
              scale: "42d",
              decay: 0.5,
            }}},
            { exp: { updated_at: {
              scale: "84d",
              decay: 0.5,
            }}},
            {
              filter: { term: { state: "open" }},
              weight: 2,
            },
          ],
        }}

        # when the user query doesn't specify an `in` qualifier we look for potential commit SHAs and issue numbers
        # if we find them then we add clauses to look for those explicitly
        if search_in.empty?
          more_clauses = commit_sha(escaped_query)
          more_clauses += issue_number(escaped_query)

          if more_clauses.any?
            more_clauses.unshift q
            q = { bool: { should: more_clauses }}
          end
        end

        q
      end

      # Internal: Helper method that will scan the query string looking for
      # Issue numbers. If any are found, then an Array is returned containing
      # term query hashes, one for each Issue number found. If no Issue
      # numbers are found, then an empty Array is returned.
      #
      # query - The full query String (not the phrase).
      #
      # Returns an array of Issue number term queries.
      def issue_number(query)
        issue_number_clauses = query.scan(/(?<=\A|\s)[1-9]\d*(?=\Z|\s)/)

        issue_number_clauses.map! do |number|
          number = number.to_i
          next unless searchable_issue_number?(number)

          { term: { number: {
              value: number,
              boost: 100,
          }}}
        end
        issue_number_clauses.compact!
        issue_number_clauses
      end

      # Determine if the give number is a valid Issue number. It must be
      # greater than 0 and must fit in a 4-byte int value to prevent
      # ElasticSearch (and Java) from exploding with a type error.
      #
      # number - The Integer to validate
      #
      # Returns true or false
      def searchable_issue_number?(number)
        number > 0 && number < MAX_ISSUE_NUMBER
      end

      # Internal: Helper method that will scan the query string looking for
      # commit SHAs. If any are found, then an Array is returned containing
      # prefix query hashes, one for each comit SHA found. If no commit SHAs
      # are found, then an empty Array is returned.
      #
      # query - The full query String (not the phrase).
      #
      # Returns an array of commit SHA prefix queries.
      def commit_sha(query)
        commit_sha_clauses = query.scan(/\b[0-9a-f]{7,40}\b/i)

        commit_sha_clauses.map! do |sha|
          { prefix: { commits: {
            value: sha,
            boost: 100,
          }}}
        end
        commit_sha_clauses.compact!
        commit_sha_clauses
      end

      # Builds a filter that restricts the repositories from which we return search results.
      #
      # The main complexity in this method comes from our handling of user-to-server integration
      # (GitHub App) requests. Those are special because they require us to consider integration
      # installations which might have access to either issues or pull requests but not necessarily
      # both. We don't have an efficient way to perform such complex filtering in the general case,
      # so instead we treat user-to-server requests differently depending on the content of the
      # search query.
      #
      # If the query asks for only issues or only pull requests, then we're in luck: we can resolve
      # that type of query easily and efficiently, so we do.
      #
      # On the other hand, if the query does not ask for just one type, then we raise an
      # InsecureUserToServerAppQuery error.
      #
      # Lastly, if this isn't a user-to-server integration request, then `builder.repository_filter`
      # will perform the correct authorizations and and we can again return results for both issues
      # and pull requests.
      #
      # Returns a Hash representing the repository filter.
      def build_repository_filter
        if !current_user&.using_auth_via_integration? || allow_insecure_user_to_server_app_query
          return builder.repository_filter(current_user, @repo_id, "issues", user_session: user_session, ip: remote_ip)
        end

        if qualifiers[:type].must == ["issue"]
          builder.repository_filter(current_user, @repo_id, "issues", user_session: user_session, ip: remote_ip)
        elsif qualifiers[:type].must == ["pull-request"]
          builder.repository_filter(current_user, @repo_id, "pull_requests", user_session: user_session, ip: remote_ip)
        else
          raise InsecureUserToServerAppQuery
        end
      end

      # Internal: Returns the highlight Hash.
      def build_highlight
        fields = {}

        if query_fields.any? { |str| str =~ /^title/ }
          fields[:title] = { number_of_fragments: 0 }  # force the whole title to be included in the fragment
        end

        if query_fields.any? { |str| str =~ /^body/ }
          fields[:body] = { number_of_fragments: 1, fragment_size: FRAGMENT_SIZE }
        end

        if query_fields.any? { |str| str =~ /^comments/ }
          fields["comments.body"] = { number_of_fragments: 1, fragment_size: FRAGMENT_SIZE }
        end

        unless fields.empty?
          { encoder: :html, fields: fields }
        end
      end

      # Internal: Returns the sorting options Array.
      def build_sort
        return if sort.blank?

        ary = build_sort_section(sort, "_score", SORT_MAPPINGS)
        ary.each do |item|
          next unless item.is_a?(Hash)

          key = item.keys.first
          if "created_at" == key || "updated_at" == key
            order = item[key]
            item[key] = {"order" => order, "unmapped_type" => "date"}
          end
        end

        ary
      end

      # Returns the default sort ordering to use in the absence of a query or
      # any other sort information.
      def default_sort
        DEFAULT_SORT
      end

      # Internal: We need to use the same filters for the query and for the
      # aggregations with the exception of the language query. Since we are aggregationing
      # on language, applying a language filter would be most unhelpful.
      #
      # Returns the Hash of filter hashes.
      def filter_hash
        return @filter_hash if defined? @filter_hash

        # override the language if created with a language option
        unless @language.nil?
          qualifiers[:language].clear
          qualifiers[:language].must @language
        end

        # override the state if created with a state option
        unless @state.nil?
          qualifiers[:state].clear
          qualifiers[:state].must @state
        end

        filters = {}

        filters[:state]  = builder.term_filter(:state)
        filters[:status] = builder.term_filter(:status)
        filters[:merged] = builder.term_filter(:merged, :merged_state)
        filters[:draft]  = builder.term_filter(:draft)
        filters[:public] = builder.term_filter(:public)
        filters[:locked] = builder.term_filter(:locked)
        filters[:labels] = builder.term_filter(
                             :labels, :label,
                             execution: :and,
                             missing: :none) { |label| label.downcase }

        filters[:archived] = builder.term_filter(:archived, singular: true) { |value|
          case value
          when /true/i; true
          when true; true
          else false
          end
        }

        filters[:has_closing_reference] = builder.term_filter(:has_closing_reference, :linked, singular: true) { |value|
          case value
          when /issue/i; true
          when /pull-request/i; true
          else nil
          end
        }

        filters[:head_ref] = builder.prefix_filter(:head_ref, :head) { |ref| ref.downcase }
        filters[:base_ref] = builder.prefix_filter(:base_ref, :base) { |ref| ref.downcase }

        filters[:author_id]    = builder.user_filter(:author_id, :author)
        filters[:assignee_id]  = builder.user_filter(:assignee_id, :assignee, missing: :none)
        filters[:requested_reviewer_ids] = builder.review_request_filter(:requested_reviewer_ids, :"review-requested", missing: :none)
        filters[:requested_reviewer_team_ids] = builder.team_filter(current_user, :requested_reviewer_team_ids, :"team-review-requested")
        filters[:reviewer_ids] = builder.user_filter(:reviewer_ids, :"reviewed-by", missing: :none)
        filters[:review_status] = builder.term_filter(:review_status, :review)
        filters[:commenter]    = builder.user_filter("comments.author_id", :commenter)

        filters[:mentioned_users] = builder.user_filter(:mentioned_user_ids, :mentions)
        filters[:mentioned_teams] = builder.team_filter(current_user, :mentioned_team_ids, :team)

        filters[:created_at] = builder.date_range_filter(:created_at, :created)
        filters[:updated_at] = builder.date_range_filter(:updated_at, :updated)
        filters[:merged_at]  = builder.date_range_filter(:merged_at,  :merged)
        filters[:closed_at]  = builder.date_range_filter(:closed_at,  :closed)

        filters[:num_comments]  = builder.range_filter(:num_comments, :comments)
        filters[:num_reactions] =
            builder.range_filter(:num_reactions, :reactions)
        filters[:num_interactions] =
            builder.range_filter(:num_interactions, :interactions)

        filters[:milestone] = builder.milestone_filter(:milestone)
        filters[:project_ids] = builder.issue_project_filter(:project, current_user: current_user)
        filters[:language_id] = builder.language_filter(:language_id, :language_id, :language) if @repo_id.nil?

        if current_installation ||= current_user.try(:installation)
          issue_permission = current_installation.permissions["issues"]
          pr_permission = current_installation.permissions["pull_requests"]

          case
          when issue_permission && pr_permission
            # pass through existing type qualifier
          when issue_permission
            qualifiers[:is].clear; qualifiers[:type].clear
            qualifiers[:is].must("issue"); qualifiers[:type].must("issue")
          when pr_permission
            qualifiers[:is].clear; qualifiers[:type].clear
            qualifiers[:is].must("pr"); qualifiers[:type].must("pr")
          else
            # TODO: Integration without either permissions are not allowed to query
          end
        end

        filters[:repo_id] = build_repository_filter

        filters[:involves] = builder.involves_filter(
            [:author_id, :assignee_id, :mentioned_user_ids, :requested_reviewer_ids, :reviewer_ids, "comments.author_id"], :involves)

        filters.delete_if { |name, filter| filter.nil? || (filter.valid? && filter.blank?) }

        @filter_hash = filters
      end

      # Internal: Take the repository IDs and generate a routing String. This
      # routing string is used to limit our search to the specific set of
      # shards where the issues / pull requests are stored.
      #
      # Returns a routing String.
      def routing
        return @routing if defined? @routing
        @routing = @repo_id

        if @repo_id.nil? && !repository_filter.global?
          ids = repository_filter.accessible_repository_ids
          @routing = ids.to_a.join(",") unless ids.length > 200
        end

        @routing
      end

      def repository_filter
        filter_hash[:repo_id]
      end

      def valid_query?
        return false unless super

        if global? && escaped_query.empty?
          filters = filter_hash.keys - [:repo_id]
          if filters.empty?
            @invalid_reason = ::Search::Query::REASON_EMPTY_QUERY
            return false
          end
        end

        if qualifiers.key?(:linked)
          if !(CLOSE_ISSUE_REFERENCE_FILTER_TYPES & qualifiers[:linked].all).any?
            @invalid_reason = "Unsupported type for the closing reference filter."
            log_issue_query("execute.reject", { reason: @invalid_reason, linked_filter: qualifiers[:linked].all.first })
            return false
          end

          if qualifiers.key?(:type) && qualifiers[:type].must? && (qualifiers[:linked].all == qualifiers[:type].must)
            @invalid_reason = "Cannot search for the closing reference for the same type as the type filter."
            return false
          end
        end

        true
      end

      # Internal: Prune results and then run the `normalizer` on the results
      # if one was provided.
      #
      # results - An Array of hits from the search index.
      #
      # Returns the normalized Array of hits.
      def normalize(results)
        prune_results(results) unless @results_pruned
        super(results)
      end

      # Internal: Take the array of issue results and remove those for which
      # the issue no longer exists or has been flagged as spammy. We will
      # only perform this pruning in test and production; development mode is
      # for testing out everything.
      #
      # results - An Array of hits from the search index.
      #
      # Returns the pruned Array of hits.
      def prune_results(results)
        GitHub.tracer.with_span("Search::Query::IssueQuery#prune_results") do |span|
          issue_ids = []
          pull_request_ids = []
          error_reported = false

          results.each do |h|
            doc_id = h["_id"].to_i
            case h["_type"]
            when "issue"; issue_ids << doc_id
            when "pull_request"; pull_request_ids << doc_id
            else
              unless error_reported
                GitHub.dogstats.increment("search.query.errors.type", { tags: ["index:" + index.name.to_s] })
                error_reported = true
              end
            end
          end

          issues = GitHub.dogstats.time("search", tags: ["index:#{index.name}", "action:prune_results_issues_lookup"]) do
            Issue.includes(:user, :repository).where(id: issue_ids).index_by(&:id)
          end

          pull_requests = PullRequest.includes(:user, :issue, :repository).where(id: pull_request_ids).index_by(&:id)

          results.delete_if do |h|
            doc_id = h["_id"].to_i

            case h["_type"]
            when "issue"; prune_issue(h, issues[doc_id])
            when "pull_request"; prune_pull_request(h, pull_requests[doc_id])
            end
          end
        end
      end

      # Internal: Determine if the search result doc should be pruned from the
      # result set based on the existence or spamminess of the corresponding
      # issue, and the enabled/disabled status of the issue's parent repository.
      #
      # doc   - The document Hash from ElasticSearch
      # issue - The corresponding Issue or nil
      #
      # Returns true or false.
      def prune_issue(doc, issue)
        @issue_repairs ||= {}
        pub = is_public doc

        # The read_attribute here is used for speed to bypass the potentially slow #spammy? method.
        if (issue.nil? || issue.user.nil? || issue.user.read_attribute(:spammy))
          unless Rails.development?
            RemoveFromSearchIndexJob.perform_later("issue", doc["_id"], doc["_routing"])
          end
          true

        # if the repo has no issues, or isn't searchable (including marked spammy) then remove from index
        elsif !issue.parent_repo_is_searchable?
          repo_id = issue.repository_id
          unless @issue_repairs[repo_id]
            RemoveFromSearchIndexJob.perform_later("bulk_issues", repo_id)
            @issue_repairs[repo_id] = true
          end
          true

        # if the public visibility of the repository has changed
        elsif !pub.nil? && issue.repository.public != pub
          repo_id = issue.repository_id
          unless @issue_repairs[repo_id]
            Search.add_to_search_index("bulk_issues", repo_id, "purge" => true)
            @issue_repairs[repo_id] = true
          end
          true

        else
          if doc.dig("_source", "updated_at") && doc["_source"]["updated_at"] != issue.updated_at.to_s
            GitHub.dogstats.increment("issue.repair_on_read")
            issue.synchronize_search_index if GitHub.flipper["issue_search_repair_on_read"].enabled?(issue.repository)
          end

          doc["_model"] = issue
          !security_validation doc
        end
      end

      # Internal: Determine if the serach result doc should be pruned from the
      # result set based on the existence or spamminess of the corresponding
      # pull request.
      #
      # doc          - The document Hash from ElasticSearch
      # pull_request - The corresponding PullRequest or nil
      #
      # Returns true or false.
      def prune_pull_request(doc, pull_request)
        @pull_request_repairs ||= {}
        pub = is_public doc

        # The read_attribute here is used for speed to bypass the potentially slow #spammy? method.
        Platform::LoaderTracker.ignore_association_loads do
          if (pull_request.nil? || !pull_request.is_searchable?)
            unless Rails.development?
              RemoveFromSearchIndexJob.perform_later("pull_request", doc["_id"], doc["_routing"])
            end
            true

          # if the public visibility of the repository has changed
          elsif !pub.nil? && pull_request.repository.public != pub
            repo_id = pull_request.repository_id
            unless @pull_request_repairs[repo_id]
              Search.add_to_search_index("bulk_pull_requests", repo_id, "purge" => true)
              @pull_request_repairs[repo_id] = true
            end
            true

          else
            # if state of PR doesn't seem to match keep match but reindex
            if doc.dig("_source", "state") && doc["_source"]["state"] != pull_request.state.to_s
              GitHub.dogstats.increment("pull_request.repair_on_read")
              pull_request.synchronize_search_index
            end

            doc["_model"] = pull_request.issue
            !security_validation doc
          end
        end
      end

      # Internal: validate that the user is allowed to see the given search
      # result document.
      #
      # doc - The search result Hash
      #
      # Returns a boolean indicating visibility.
      def security_validation(doc)
        @include_repos ||= repository_filter.accessible_repository_ids

        repo = doc["_model"].repository

        return true if repo.public?
        return true if @include_repos.include? repo.id

        business_ids = repository_filter.accessible_business_ids
        return true if business_ids.include? repo.internal_visibility_business_id

        GitHub.dogstats.increment("search.query.errors.security", { tags: ["index:" + index.name.to_s] })
        false
      end

      # Internal: Returns `true`, `false`, or `nil`. nil will only be returned
      # if a public attribute is not included as part of the search result
      # doc.
      #
      # doc - The document Hash returned from the search index.
      #
      def is_public(doc)
        if source = doc["_source"]
          source["public"]
        end
      end

      private

      def log_issue_query(at, data_hash = {})
        log_payload = {
          at: "#{self.class.name}.#{at}",
        }
        GitHub::Logger.log(log_payload.merge(data_hash))
      end

    end  # IssueQuery
  end  # Queries
end  # Search
