# frozen_string_literal: true

module Search
  module Queries
    # The MarketplaceQuery is used to search Marketplace and Works with GitHub listings.
    class MarketplaceQuery < ::Search::Query
      # The set of fields that can be queried when performing a Marketplace search
      FIELDS = [:is, :category, :name, :sort, :state].freeze

      # A set of terms that have mutually exclusive values.
      UNIQ_FIELDS = [:sort, :is]

      VERIFIED_BOOST = 3.0
      UNVERIFIED_BOOST = 1.0
      PARTNER_BOOST = 2.5

      # The mapping of user-facing sort values to the corresponding values used
      # in the search index.
      SORT_MAPPINGS = {
        "created" => "created_at",
        "updated" => "updated_at",
        "_score" => "_score",
      }.freeze

      DEFAULT_SORT = %w[_score desc].freeze

      # The maximum number of characters returned in the highlighted fragments.
      FRAGMENT_SIZE = 200

      # Construct a new MarketplaceListingQuery instance that will highlight search results
      # by default.
      def initialize(opts = {}, &block)
        super(opts, &block)
        @index = Elastomer::Indexes::MarketplaceListings.searcher
        @state = opts.fetch(:state, nil)
        @type = opts.fetch(:type, nil)
        @enterprise_compatible = opts.fetch(:enterprise_compatible, nil)
        @offers_free_trial = opts.fetch(:offers_free_trial, nil)
      end

      # Internal: Returns the Array of advanced search qualifiers supported by
      # this query type.
      def qualifier_fields
        FIELDS
      end

      # Internal: Returns a Hash that will be passed as URL params for the query.
      def query_params
        return @query_params if defined? @query_params

        types = @type

        if types.nil? && qualifiers.key?(:type)
          types = qualifiers[:type].must.nil? ? nil : qualifiers[:type].must.first
          qualifiers[:type].clear
        end

        @query_params =
            case types
            when /works-with-github/i
              { type: %w[non_marketplace_listing] }
            when /marketplace-tools/i
              tool_types = %w[marketplace_listing repository_action]
              tool_types << "github_app" if GitHub.flipper[:github_apps_in_marketplace_search].enabled?(current_user)
              { type: tool_types }
            when /marketplace/i
              tool_types = %w[marketplace_listing]
              tool_types << "github_app" if GitHub.flipper[:github_apps_in_marketplace_search].enabled?(current_user)
              { type: tool_types }
            when /repository-action/i
              { type: %w[repository_action] }
            else
              { type: %w[marketplace_listing non_marketplace_listing] }
            end
      end

      # Internal: Returns the Array of field names that will be queried.
      def query_fields
        @query_fields ||= ["name^1.5", "name.ngram", "owner_login^1.1", "owner_name^1.1", "description", "short_description", "full_description", "categories", "categories.raw^0.8"]
      end

      # Internal: build a query doc with a boosting portion to score verified => unverified => unlisted
      def build_query
        qd = query_doc
        bd = boost_doc
        filter = build_query_filter

        query = { should: bd }
        query[:must] = qd if qd
        query[:filter] = filter if filter

        return { bool: query } unless GitHub.flipper[:increase_actions_marketplace_visibility].enabled?(current_user)

        {
          function_score: {
            query: {
              bool: query
            },
            functions: [{
              random_score: {
                seed: Date.today.to_s
              }
            }],
            boost_mode: "sum"
          }
        }
      end

      # Internal: a query portion for boosting marketplace listings so they score higher and
      #           are shown first in search results. Verified listings should appear first,
      #           followed by unverified listings (including those pending verification),
      #           followed by other documents in the 'marketplace-search' alias (eg. actions).
      def boost_doc
        return deprecated_boost unless GitHub.flipper[:increase_actions_marketplace_visibility].enabled?(current_user)

        [
          *marketplace_listing_boost,
          *repository_action_boost,
          *github_app_boost,
        ]
      end

      def marketplace_listing_boost
        [
          {
            constant_score: {
              filter: {
                bool: {
                  must: [
                    { term: { state: "verified" } },
                    { type: { value: "marketplace_listing" } },
                  ]
                }
              },
              boost: VERIFIED_BOOST,
            },
          },
          {
            constant_score: {
              filter: {
                bool: {
                  must: [
                    { terms: { state: ["unverified", "verification_pending_from_unverified"] } },
                    { type: { value: "marketplace_listing" } },
                  ]
                }
              },
              boost: UNVERIFIED_BOOST,
            },
          }
        ]
      end

      def repository_action_boost
        verified_repo_action = [
          { type: { value: "repository_action" } },
          { term: { is_verified_owner: true } },
        ]

        boost = [
          {
            constant_score: {
              filter: {
                bool: {
                  must: [
                    { type: { value: "repository_action" } },
                    { term: { is_verified_owner: false } },
                  ]
                }
              },
              boost: UNVERIFIED_BOOST,
            },
          }
        ]

        # When there is a query, we dont need to change queries at all as the search will change the results anyways
        # and there's no risk of "favouring" anyone by mistake.
        # Also, maybe we _do_ want to search "Azure" or something.
        if escaped_query.present?
          boost << {
            constant_score: {
              filter: {
                bool: {
                  must: verified_repo_action
                }
              },
              boost: VERIFIED_BOOST,
            },
          }
        else
          # When the search query is blank, we don't want to boost GitHub and Microsoft entries as much.
          # These actions were showing up on the front page and dominating the top results.
          # We want to avoid the situation where it appears that we are favouring ourselves or our parent company,
          # which could be poorly received by the community
          boost += [
            {
              constant_score: {
                filter: {
                  bool: {
                    must: verified_repo_action,
                    must_not: [
                      { terms: { "owner_login.raw": ::RepositoryAction::GITHUB_MICROSOFT_CREATORS } },
                    ]
                  }
                },
                boost: VERIFIED_BOOST,
              },
            },
            {
              constant_score: {
                filter: {
                  bool: {
                    must: verified_repo_action + [{ terms: { "owner_login.raw": ::RepositoryAction::GITHUB_MICROSOFT_CREATORS } }]
                  }
                },
                boost: PARTNER_BOOST, # We still want to boost these partners, but not as much
              },
            },
          ]
        end

        boost
      end

      def github_app_boost
        [
          {
            constant_score: {
              filter: {
                bool: {
                  must: [
                    { type: { value: "github_app" } },
                  ]
                }
              },
              boost: UNVERIFIED_BOOST,
            },
          },
        ]
      end

      # TODO: remove after activating increase_actions_marketplace_visibility for all users
      def deprecated_boost
        boost = [
          {
            bool: {
              must: [
                { term: { state: "verified" } },
                { type: { value: "marketplace_listing" } },
              ],
              boost: 200.0,
            },
          },
          {
            bool: {
              must: [
                { terms: { state: ["unverified", "verification_pending_from_unverified"] } },
                { type: { value: "marketplace_listing" } },
              ],
              boost: 40.0,
            },
          },
          {
            bool: {
              must: [
                { type: { value: "repository_action" } },
              ],
              boost: 10.0,
            },
          },
          {
            bool: {
              must: [
                { type: { value: "github_app" } },
              ],
              boost: 5.0,
            },
          },
        ]

        verified_repo_action = [
          { type: { value: "repository_action" } },
          { term: { is_verified_owner: true } },
        ]

        # When there is a query, we dont need to change queries at all as the search will change the results anyways
        # and there's no risk of "favouring" anyone by mistake.
        # Also, maybe we _do_ want to search "Azure" or something.
        if escaped_query.present?
          boost << {
            bool: {
              must: verified_repo_action,
              boost: 15.0,
            },
          }
        else
          # When the search query is blank, we don't want to boost GitHub and Microsoft entries as much.
          # These actions were showing up on the front page and dominating the top results.
          # We want to avoid the situation where it appears that we are favouring ourselves or our parent company,
          # which could be poorly received by the community
          boost += [
            {
              bool: {
                must: verified_repo_action,
                must_not: [
                  { terms: { owner_login: ::RepositoryAction::GITHUB_MICROSOFT_CREATORS } },
                ],
                boost: 15.0,
              },
            },
            {
              bool: {
                must: verified_repo_action + [{ terms: { owner_login: ::RepositoryAction::GITHUB_MICROSOFT_CREATORS } }],
                boost: 5.0, # We still want to boost these partners, but not as much
              },
            },
          ]
        end

        boost
      end

      # Internal: Constructs the search query based on the `query` attribute.
      #
      # Returns the search query Hash.
      def query_doc
        return if escaped_query.empty?

        {
          query_string: {
            query: escaped_query,
            fields: query_fields,
            phrase_slop: 10,
            default_operator: "AND",
            use_dis_max: true,
            analyzer: "texty_search",
          },
        }
      end

      # Internal: Returns the sorting options Array.
      def build_sort
        return if sort.blank?

        ary = build_sort_section(sort, SORT_MAPPINGS)
        ary&.each do |item|
          next unless item.is_a?(Hash)

          key = item.keys.first
          if "created_at" == key || "updated_at" == key
            order = item[key]
            item[key] = { "order" => order, "unmapped_type" => "date" }
          end
        end

        ary
      end

      # Returns the default sort ordering to use in the absence of a query or
      # any other sort information.
      def default_sort
        DEFAULT_SORT
      end

      # Internal: Build the filters required for the query.
      #
      # Returns the Hash of filter hashes.
      def filter_hash
        return @filter_hash if defined? @filter_hash

        filters = {}

        filters[:name] = builder.query_filter(:"name.ngram")
        filters[:created_at] = builder.date_range_filter(:created_at, :created)
        filters[:updated_at] = builder.date_range_filter(:updated_at, :updated)
        filters[:marketplace_id] = builder.marketplace_listing_filter(current_user)

        if [true, false].include? @enterprise_compatible
          qualifiers[:enterprise_compatible].clear.must(@enterprise_compatible)
          filters[:enterprise_compatible] = builder.term_filter(:enterprise_compatible)
        end

        if [true, false].include? @offers_free_trial
          qualifiers[:offers_free_trial].clear.must(@offers_free_trial)
          filters[:offers_free_trial] = builder.term_filter(:offers_free_trial)
        end

        if qualifiers.key?(:is) && qualifiers[:is].must?
          case qualifiers[:is].must.first
          when "verified"
            qualifiers[:state].clear.must("verified")
          when "draft"
            qualifiers[:state].clear.must("draft")
          when "rejected"
            qualifiers[:state].clear.must("rejected")
          when "archived"
            qualifiers[:state].clear.must("archived")
          when "verification-pending-from-draft"
            qualifiers[:state].clear.must("verification_pending_from_draft")
          else
            qualifiers[:is].must.each do |value|
              qualifiers[:category].must(value.downcase)
            end
          end
          filters[:state] = builder.term_filter(:state)
        end

        # @state overrides state from "is:[state]" in query
        if @state.present?
          case @state
          when "verified"
            qualifiers[:state].clear.must("verified")
          when "draft"
            qualifiers[:state].clear.must("draft")
          when "rejected"
            qualifiers[:state].clear.must("rejected")
          when "archived"
            qualifiers[:state].clear.must("archived")
          when "verification-pending-from-draft"
            qualifiers[:state].clear.must("verification_pending_from_draft")
          end
          filters[:state] = builder.term_filter(:state)
        else
          filters[:state] = builder.term_filter(:state, execution: :or)
        end

        filters[:categories] = builder.term_filter(
          :"categories.raw",
          :category,
          execution: :or,
          missing: :none) { |category| category.downcase }

        filters.delete_if { |name, filter| filter.nil? || (filter.valid? && filter.blank?) }

        @filter_hash = filters
      end

      # Internal: Returns the highlight Hash.
      def build_highlight
        fields = {}

        if query_fields.any? { |str| str.start_with?("name") }
          # force the whole name to be included in the fragment
          fields[:"name.ngram"] = { number_of_fragments: 0 }
        end

        fields[:description] = { number_of_fragments: 1, fragment_size: FRAGMENT_SIZE }
        fields[:short_description] = { number_of_fragments: 1, fragment_size: FRAGMENT_SIZE }
        fields[:full_description] = { number_of_fragments: 1, fragment_size: FRAGMENT_SIZE }

        unless fields.empty?
          { encoder: :html, fields: fields }
        end
      end

      # Internal: Prune results and then run the `normalizer` on the results
      # if one was provided.
      #
      # results - An Array of hits from the search index.
      #
      # Returns the normalized Array of hits.
      def normalize(results)
        prune_results(results) unless @results_pruned
        super(results)
      end

      # Internal: Take the array of listing results and remove those for which
      # the listing no longer exists. We will only perform this pruning in test
      # and production; development mode is for testing out everything.
      #
      # results - An Array of hits from the search index.
      #
      # Returns the pruned Array of hits.
      def prune_results(results)
        non_marketplace_listing_ids = []
        marketplace_listing_ids = []
        repository_action_ids = []
        github_app_ids = []
        error_reported = false

        results.each do |result|
          doc_id = result["_id"].to_i

          case result["_type"]
          when "marketplace_listing"     then marketplace_listing_ids << doc_id
          when "non_marketplace_listing" then non_marketplace_listing_ids << doc_id
          when "repository_action"       then repository_action_ids << doc_id
          when "github_app"              then github_app_ids << doc_id
          else
            unless error_reported
              GitHub.dogstats.increment("search.query.errors.type",
                                        { tags: ["index:" + index.name.to_s] })
              error_reported = true
            end
          end
        end

        # Preload relations used in NonMarketplaceListingResultView:
        non_marketplace_listings = NonMarketplaceListing.
          includes(:creator, integration_listing: :integration).
          where(id: non_marketplace_listing_ids).index_by(&:id)

        # Preload relations used in MarketplaceListingResultView:
        marketplace_listings = Marketplace::Listing.
          where(id: marketplace_listing_ids).index_by(&:id)

        repository_actions = RepositoryAction.
          includes(:repository).
          where(id: repository_action_ids).index_by(&:id)

        github_apps = Integration.
          where(id: github_app_ids).index_by(&:id)

        results.delete_if do |result|
          doc_id = result["_id"].to_i

          case result["_type"]
          when "marketplace_listing"
            prune_marketplace_listing(result, marketplace_listings[doc_id])
          when "non_marketplace_listing"
            prune_non_marketplace_listing(result, non_marketplace_listings[doc_id])
          when "repository_action"
            prune_repository_action(result, repository_actions[doc_id])
          when "github_app"
            prune_github_app(result, github_apps[doc_id])
          end
        end
      end

      def prune_marketplace_listing(doc, listing)
        if listing.nil? || listing.archived? || listing.listable_is_sponsorable?
          RemoveFromSearchIndexJob.perform_later("marketplace_listing", doc["_id"])
          true
        else
          doc["_model"] = listing
          !security_validation doc
        end
      end

      def prune_non_marketplace_listing(doc, listing)
        if listing.nil?
          RemoveFromSearchIndexJob.perform_later("non_marketplace_listing", doc["_id"])
          true
        else
          doc["_model"] = listing
          !security_validation doc
        end
      end

      def prune_repository_action(doc, action)
        if action.nil? || action.delisted? || action.repository.private?
          RemoveFromSearchIndexJob.perform_later("repository_action", doc["_id"])
          true
        else
          doc["_model"] = action
          !security_validation doc
        end
      end

      def prune_github_app(doc, app)
        if app.nil? || !app.public?
          RemoveFromSearchIndexJob.perform_later("github_app", doc["_id"])
          true
        else
          doc["_model"] = app
          !security_validation doc
        end
      end

      # Internal: Validate that the user is allowed to see the given search result document.
      #
      # doc - The search result Hash
      #
      # Returns nil
      # Returns a boolean indicating visibility.
      def security_validation(doc)
        tool = doc["_model"]
        tool_type = doc["_type"]

        tool_type_description = case tool_type
        when "non_marketplace_listing"
          "Works with GitHub"
        when "marketplace_listing"
          "Marketplace"
        when "repository_action"
          "Action"
        when "github_app"
          "GitHub App"
        end

        case tool_type
        when "repository_action"
          return true
        when "github_app"
          return true if GitHub.flipper[:github_apps_in_marketplace_search].enabled?(current_user)
        else
          return true if tool.publicly_listed?
          return true if tool.adminable_by?(current_user)
        end

        GitHub.dogstats.increment("search.query.errors.security",
                                  { tags: ["index:" + index.name.to_s] })
        false
      end
    end
  end
end
