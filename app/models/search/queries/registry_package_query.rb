# frozen_string_literal: true

module Search
  module Queries
    # The RegistryPackageQuery is used to search Registry Packages.
    #
    class RegistryPackageQuery < ::Search::Query

      # The set of fields that can be queried when performing a registry package search
      FIELDS = [:name, :sort, :user, :org, :repo, :package_type, :public, :topic].freeze

      # A set of terms that have mutually exclusive values.
      UNIQ_FIELDS = [:sort]

      # The mapping of user-facing sort values to the corresponding values used
      # in the search index.
      SORT_MAPPINGS = {
        "downloads" => "downloads",
        "created" => "created_at",
        "updated" => "updated_at",
      }.freeze

      DEFAULT_SORT = %w[downloads desc].freeze

      # The maximum number of characters returned in the highlighted fragments.
      FRAGMENT_SIZE = 200

      # Construct a new RegistryPackageQuery instance that will highlight search results
      # by default.
      def initialize(opts = {}, &block)
        super(opts, &block)
        @index = Elastomer::Indexes::RegistryPackages.new
        @repo_id = opts.fetch(:repo_id, nil)
        @owner = opts.fetch(:owner, nil)
        @package_type = opts.fetch(:package_type, nil)

        visibility = opts.fetch(:visibility, nil)
        if visibility.present?
          @public = visibility == "public"
        end
      end

      # Internal: Returns the Array of advanced search qualifiers supported by
      # this query type.
      def qualifier_fields
        FIELDS
      end

      # Internal: Returns the Array of fields that support aggregation queries.
      def aggregation_fields
        [:package_type]
      end

      # Internal: Returns a Hash that will be passed as URL params for the query.
      def query_params
        {
          index: Elastomer.env.index_name("packages"),
          type: "registry_package",
        }
      end

      # Internal: Returns the Array of field names that will be queried.
      def query_fields
        @query_fields ||= ["name^1.2", "name.ngram", "summary", "body"]
      end

      # Internal: Constructs the search query based on the `query` attribute.
      #
      # Returns the search query Hash.
      def query_doc
        return unless escaped_query.present?

        {
          function_score: {
            query: {
              query_string: {
                query:            escaped_query,
                fields:           query_fields,
                phrase_slop:      10,
                default_operator: "AND",
                use_dis_max:      true,
                analyzer:         "texty_search",
              },
            },
            # This function reduces the score of documents that haven't been
            # updated recently. A document not modified in 24 weeks will have its
            # score reduced by half. Look at updated_at so that old registry packages
            # that get renewed aren't punished for having an old created_at.
            exp: {
              updated_at: {
                scale:  "168d",
                offset: "14d",
                decay:  0.5,
              },
            },
          },
        }
      end

      # Internal: Returns the sorting options Array.
      def build_sort
        return if sort.blank?

        ary = build_sort_section(sort, "_score", SORT_MAPPINGS)
        ary.each do |item|
          next unless item.is_a?(Hash)

          key = item.keys.first
          if "created_at" == key || "updated_at" == key
            order = item[key]
            item[key] = {"order" => order, "unmapped_type" => "date"}
          end
        end

        ary
      end

      # Returns the default sort ordering to use in the absence of a query or
      # any other sort information.
      def default_sort
        DEFAULT_SORT
      end

      # Internal: Build the filters required for the query.
      #
      # Returns the Hash of filter hashes.
      def filter_hash
        return @filter_hash if defined? @filter_hash

        filters = {}
        filters[:created_at] = builder.date_range_filter(:created_at, :created)
        filters[:updated_at] = builder.date_range_filter(:updated_at, :updated)

        qualifiers[:user].clear.must(@owner.login) if @owner && @owner.is_a?(User)

        filters.delete_if { |_, filter| filter.nil? || (filter.valid? && filter.blank?) }

        @filter_hash = filters
      end

      def build_query_filter
        filter = ::Search::Filters::BoolFilter.new(filter_hash, aggregations)
        query_filter = filter.build

        query_filter ||= { bool: {} }
        query_filter[:bool][:must] = build_must_filters

        if package_filter.must
          query_filter[:bool][:must] = [query_filter[:bool][:must], package_filter.must].flatten
        else
          query_filter[:bool][:should] = package_filter.should
          query_filter[:bool][:minimum_should_match] = 1
        end

        query_filter
      end

      def build_must_filters
        qualifiers[:package_type].clear.must(@package_type) if @package_type
        qualifiers[:public].clear.must(@public) unless @public.nil?

        must_filters = []
        must_filters << builder.term_filter(:package_type).must
        must_filters << builder.term_filter(:public).must
        must_filters << builder.term_filter(
          :applied_topics, :topic,
          execution: :and,
          missing: :none
        ) { |topic| topic.downcase }.must

        must_filters.compact
      end

      # Internal: Returns the highlight Hash.
      def build_highlight
        {
          encoder: :html,
          fields: {
            name: { number_of_fragments: 0 },
            "name.ngram": { number_of_fragments: 0 },
          },
        }
      end

      def package_filter
        @package_filter ||= builder.registry_packages_filter(current_user, @owner&.id, @repo_id, "registry_packages", user_session: user_session)
      end

      # Internal: Prune results and then run the `normalizer` on the results
      # if one was provided.
      #
      # results - An Array of hits from the search index.
      #
      # Returns the normalized Array of hits.
      def normalize(results)
        prune_results(results) unless @results_pruned
        super(results)
      end

      # Internal: Take the array of registry package results and remove those for which
      # the registry package no longer exists. We will only perform this pruning in test
      # and production; development mode is for testing out everything.
      #
      # results - An Array of hits from the search index.
      #
      # Returns the pruned Array of hits.
      def prune_results(results)
        package_ids = results.map { |h| h["_id"] }
        rms_ids, ar_ids = package_ids.partition { |id| id.start_with?("rms") }
        rms_ids = rms_ids.map { |str_id| str_id.sub(/rms-/, "").to_i }

        # Get all AR Packages based on results
        ar_packages = Registry::Package.includes(:owner, :repository).where(id: ar_ids).index_by(&:id)
        rms_packages_by_id = {}

        # Get all RMS Packages based on results
        if GitHub.flipper[:container_registry_ui].enabled?(current_user)
          begin
            unless rms_ids.empty?
              rms_packages_by_id = PackageRegistry::Twirp.metadata_client.get_packages_metadata(user_id: current_user.id, package_ids: rms_ids)
              .each_with_object({}) { |package, accumulator| accumulator[package.id] = package }
            end
          rescue PackageRegistry::Twirp::BaseError
            rms_packages_by_id = {}
          end
        end

        results.delete_if do |result|
          doc_id_string = result["_id"]
          if doc_id_string.start_with?("rms")
            doc_id = doc_id_string.sub(/rms-/, "").to_i
            result["_model"] = rms_packages_by_id[doc_id]
            false
          else
            doc_id = doc_id_string.to_i
            registry_package = ar_packages[doc_id]

            if registry_package.nil? && !Rails.development?
              RemoveFromSearchIndexJob.perform_later("registry_package", doc_id)
              true
            else
              result["_model"] = registry_package
              !security_validation result
            end
          end
        end
      end

      # Internal: validate that the user is allowed to see the given search
      # result document.
      #
      # doc - The search result Hash
      #
      # Returns a boolean indicating visibility.
      def security_validation(doc)
        return true unless registry_package = doc["_model"]

        repo = registry_package.repository
        return true if repo.public?

        @include_repos ||= package_filter.accessible_repository_ids
        return true if @include_repos.include? repo.id

        business_ids = package_filter.accessible_business_ids
        return true if business_ids.include? repo.internal_visibility_business_id

        GitHub.dogstats.increment("search.query.errors.security", { tags: ["index:" + index.name.to_s] })
        false
      end
    end
  end
end
