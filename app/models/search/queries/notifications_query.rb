# frozen_string_literal: true

module Search
  module Queries
    class NotificationsQuery < ::Search::Query
      FIELDS = [:is, :reason, :repo].freeze
      UNSUPPORTED_IS_VALUES = ["issue", "pr", "pull-request"].freeze

      VALID_INBOX_ONLY_QUERIES = [
        "",
        "is:unread is:read",
        "is:read is:unread",
        "is:unread",
      ]

      attr_reader :parsed, :query, :viewer

      def initialize(query:, viewer:)
        @query = (query || "").strip
        @parsed = self.class.parse(@query)
        @viewer = viewer
      end

      # Is the query string one which is considered an "inbox" query
      def is_only_an_inbox_query?
        VALID_INBOX_ONLY_QUERIES.include?(query)
      end

      def read?
        return @read if defined? @read

        @read = qualifier_values(:is).any?("read")
      end

      def unread?
        return @unread if defined? @unread

        @unread = qualifier_values(:is).any?("unread")
      end

      def starred?
        return @starred if defined? @starred

        @starred = qualifier_values(:is).any?("saved")
      end

      def archived?
        return @archived if defined? @archived

        @archived = qualifier_values(:is).any?("done")
      end

      def thread_type?
        return @thread_type if defined? @thread_type

        @thread_type = qualifier_values(:is).any? { |v| !["read", "unread", "done"].include?(v) }
      end

      def participating?
        return @participating if defined? @participating

        @participating = qualifier_values(:reason).any?("participating")
      end

      def qualifier_used?(qualifier)
        qualifier_values(qualifier).any?
      end

      def contains_unsupported_qualifiers?
        query.gsub(/(#{FIELDS.join("|")}):\S+/, "").strip.present?
      end

      # Is supported by mysql if it doesn't contain any unsupported qualifiers
      # and it doesn't contain is:issue, is:pr, etc
      def supported_by_mysql?
        !contains_unsupported_qualifiers? && !qualifier_values(:is).any? { |value| UNSUPPORTED_IS_VALUES.include?(value) }
      end

      def stringify(unread: unread?, read: read?, archived: archived?, starred: starred?, reasons: nil, repositories: nil, repository_names: nil)
        new_query = parsed.dup

        if read && unread && !archived
          # Here the user wants all their non-archived notifications (both read and unread) which is
          # the default so remove some unnecessary qualifiers.
          new_query = toggle_qualifier(new_query, :is, "read", false)
          new_query = toggle_qualifier(new_query, :is, "unread", false)
        else
          new_query = toggle_qualifier(new_query, :is, "read", read)
          new_query = toggle_qualifier(new_query, :is, "unread", unread)
        end

        new_query = toggle_qualifier(new_query, :is, "done", archived)

        new_query = toggle_qualifier(new_query, :is, "saved", starred)

        if reasons
          new_query = replace_qualifiers(new_query, :reason, reasons)
        end

        if repositories
          name_with_owners = repositories.map(&:name_with_owner)
          new_query = replace_qualifiers(new_query, :repo, name_with_owners)
        end

        if repository_names
          new_query = replace_qualifiers(new_query, :repo, repository_names)
        end

        new_query.uniq!

        self.class.stringify(new_query)
      end

      def statuses
        return @statuses if defined? @statuses

        @statuses = []
        @statuses << "read" if read?
        @statuses << "unread" if unread?
        @statuses << "archived" if archived?

        @statuses = ["read", "unread"] if @statuses.empty?
        @statuses
      end

      # Get an array of valid reasons for the specified reason: qualifiers.
      def reasons
        return @reasons if defined? @reasons

        return Newsies::NotificationEntry::PARTICIPATING_REASONS if participating?

        reasons = qualifier_values(:reason)
                    .map { |reason| reason.gsub(/-/, "_") }
                    .select { |reason| Platform::Enums::NotificationReason.values.include?(reason.upcase) }
        @reasons = reasons.map(&:downcase)
      end

      def thread_types
        return @thread_types if defined? @thread_types

        is_values = qualifier_values(:is) || []

        @thread_types = is_values.map do |key|
          key = "issue" if ["issue-or-pull-request", "issue-or-pr"].include?(key)
          thread_type = Search::Queries::NotificationSearchQuery::TYPE_QUALIFIER_MAPPINGS[key]
          next "Issue" if thread_type == "PullRequest"
          thread_type
        end.compact
      end

      # Get an array of Repository objects for the specified repo: qualifiers
      def repositories
        return @repositories if defined? @repositories

        repositories = qualifier_values(:repo).map do |name_with_owner|
          repository = Repository.with_name_with_owner(name_with_owner)
          next if repository.nil?
          next unless repository.readable_by?(viewer)
          repository
        end

        @repositories = repositories.compact
      end

      private

      def toggle_qualifier(query, name, value, state)
        new_query = query.dup

        if state
          new_query << [name, value] unless new_query.include?([name, value])
        else
          new_query.reject! do |component|
            matches_qualifier?(component, name, value)
          end
        end

        new_query
      end

      def qualifiers
        @qualifiers ||= parsed.select { |component| qualifier?(component) }
      end

      def qualifier_values(name)
        values = qualifiers.select do |component|
          component.first == name
        end
        values.map(&:second)
      end

      def qualifier?(component)
        # Select non-negated qualifiers such as qualifier:value
        component.is_a?(Array) && component.third != true
      end

      def matches_qualifier?(component, name, value)
        qualifier?(component) && component.first == name && value.casecmp?(component.second)
      end

      def replace_qualifiers(query, name, values)
        new_query = query.reject { |component| component.first == name }
        values.each { |value| new_query << [name, value] }
        new_query
      end
    end
  end
end
