# rubocop:disable Style/FrozenStringLiteralComment

module Search
  class ClusterStatus

    CODE_SEARCH_INDEXING_KEY = "enable_code_search_indexing".freeze # allow indexing source code
    CODE_SEARCH_ENABLED_KEY  = "enable_code_search".freeze          # allow searching source code

    # Returns `true` if indexing of source code is currently enabled. Returns
    # `false` if it has been disabled.
    #
    # Defaults to true if KV is unavailable.
    def self.code_search_indexing_enabled?
      1 == GitHub.kv.get(CODE_SEARCH_INDEXING_KEY).value { 1 }.to_i
    end

    # Returns `true` if searching of source code is currently enabled. Returns
    # `false` if it has been disabled.
    #
    # Defaults to true if KV is unavailable.
    def self.code_search_enabled?
      1 == GitHub.kv.get(CODE_SEARCH_ENABLED_KEY).value { 1 }.to_i
    end

    def health_for_each_cluster
      Elastomer.router.clusters.each do |name|
        begin
          client = Elastomer.router.client(name)
          next if !client.available?

          yield client.cluster.health
        rescue Errno::ECONNREFUSED
          # ignore
        end
      end
    end

    # Returns `true` if indexing of source code is currently enabled. Returns
    # `false` if it has been disabled.
    #
    def code_search_indexing_enabled?
      self.class.code_search_indexing_enabled?
    end

    # Set the key in Redis to enable source code indexing.
    def enable_code_search_indexing
      GitHub.kv.set(CODE_SEARCH_INDEXING_KEY, "1")
    end

    # Set the key in Redis to disable source code indexing.
    def disable_code_search_indexing
      GitHub.kv.set(CODE_SEARCH_INDEXING_KEY, "0")
    end

    # Returns `true` if searching of source code is currently enabled. Returns
    # `false` if it has been disabled.
    #
    def code_search_enabled?
      self.class.code_search_enabled?
    end

    # Set the key in Redis to enable source code searching.
    def enable_code_search
      GitHub.kv.set(CODE_SEARCH_ENABLED_KEY, "1")
    end

    # Set the key in Redis to disable source code searching.
    def disable_code_search
      GitHub.kv.set(CODE_SEARCH_ENABLED_KEY, "0")
    end

  end  #  ClusterStatus
end  #  Search
