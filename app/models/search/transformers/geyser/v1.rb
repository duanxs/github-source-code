# frozen_string_literal: true

module Search::Transformers::Geyser
  module V1

    # Mirrors the error set by Search::Filters::RepositoryFilter
    REPOSITORY_TRANFORM_ERROR = "The listed users, orgs, or repositories cannot be searched either because the resources do not exist or you do not have permission to view them."

    class TransformError < StandardError; end

    extend self

    # Perform name/value substitution according to the rules.
    #
    # parsed_query - An array of ParsedNodes
    #
    # Returns a new array of ParsedNodes with transformations
    # applied. Nodes may be marked invalid if certain qualifiers
    # cannot be transformed.
    def transform(parsed_query)
      parsed_query.map do |node|
        case node.field
        when :language
          transform_language(node)
        when :org, :user
          transform_owner(node)
        when :repo
          transform_repo(node)
        else
          node
        end
      end
    end

    private

    # Transform a parsed language qualifier node to a language_id qualifier
    def transform_language(parsed_node)
      # Language names can include spaces
      alias_name = parsed_node.value&.downcase&.gsub(/\s/, "-")
      language_id = Linguist::Language[alias_name].try(:language_id)
      if language_id
        field = :language_id
        value = language_id
        negated = parsed_node.negated?
        Search::Parsers::Geyser::V1::ParsedNode.new(field, value, negated)
      else
        # Legacy search ignores invalid language qualifiers, don't mark invalid
        parsed_node
      end
    end

    # Transform a parsed org or user qualifier node to a {org|user}_id qualifier
    def transform_owner(parsed_node)
      login = parsed_node.value
      user = User.select(:id).find_by(login: login, spammy: false)
      if user
        field = "#{parsed_node.field}_id".to_sym
        value = user.id
        negated = parsed_node.negated?
        Search::Parsers::Geyser::V1::ParsedNode.new(field, value, negated)
      else
        node = parsed_node.dup
        node.valid = false
        node.invalid_reason = REPOSITORY_TRANFORM_ERROR
        node
      end
    end

    # Transform a parsed repo qualifier node to a repo_id qualifier
    def transform_repo(parsed_node)
      name_with_owner = parsed_node.value
      repo = Repository.select(:id).nwo(name_with_owner)
      if repo
        field = :repo_id
        value = repo&.id
        negated = parsed_node.negated?
        Search::Parsers::Geyser::V1::ParsedNode.new(field, value, negated)
      else
        node = parsed_node.dup
        node.valid = false
        node.invalid_reason = REPOSITORY_TRANFORM_ERROR
        node
      end
    end
  end
end
