# frozen_string_literal: true

module Reminders
  class SlackApi
    EXPIRATION_LEEWAY = 60.seconds

    class << self
      def post_reminder_change_to_channel(reminder_url:, reminder:, user:, action:)
        raise ArgumentError, "Can't send message to Slack, reminder is not valid" unless reminder.valid?

        payload = {
          github_user_login: user&.login || User.ghost.login,
          action: action,
          reminder_link: reminder_url,
          workspace_id: reminder.slack_workspace.slack_id,
          channel_name: reminder.slack_channel, # Always use Slack Channel name as we want to use this to validate the channel's existence
        }
        post("/internal/api/slack/post_reminder_change", jwt: sign_payload(payload))
      end

      def channel_status(channel_id, workspace_id:)
        success, response_data = post("/internal/api/slack/validate_channel", body: { channel_id: channel_id, workspace_id: workspace_id })

        if success
          response_data["status"]
        else
          "unknown"
        end
      end

      private

      def post(path, body: {}, jwt: sign_payload({}))
        post = Net::HTTP::Post.new(path, "Content-Type" => "application/json")
        post.body = { state: jwt }.merge(body).to_json
        send_request(post)
      end

      def send_request(request)
        integration_uri = URI.parse(slack_integration.url)
        http = Net::HTTP.new(integration_uri.host, integration_uri.port)

        # These requests will happen in web requests, which have a max of 10s to complete.
        # To make sure we don't brush up against that, let's keep the entire request to ~5s
        # to allow for degradation in other parts of the system.
        # Setting to 3s leaves 2s for the remaining aspects of the request to complete.
        http.open_timeout = 3
        http.read_timeout = 3
        http.use_ssl = true

        resp = http.request(request)
        returnable_response = begin
          (resp.body.present? ? JSON.parse(resp.body) : "")
        rescue JSON::ParserError
          resp.body
        end

        [resp.is_a?(Net::HTTPSuccess), returnable_response]
      rescue Net::HTTPError, Net::OpenTimeout => e
        [false, e.to_s]
      end

      def sign_payload(payload)
        issued_at = Time.now
        expires_at = issued_at + EXPIRATION_LEEWAY
        secret = slack_integration.secret
        payload = payload.merge({
          iat: issued_at.to_i,
          exp: expires_at.to_i,
        })
        payload[:uuid] ||= SecureRandom.urlsafe_base64(20)

        JWT.encode(payload, secret, "HS256")
      end

      def slack_integration
        Apps::Internal.integration(:slack) || raise(ArgumentError, "Slack Integration did not exist")
      end
    end
  end
end
