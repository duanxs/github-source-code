# rubocop:disable Style/FrozenStringLiteralComment

# Object representing arbitrary entity-specific configuration values.
#
# Configuration respects a chain of priority from repository to repository
# owner, all the way up to a global entity. For example, if a repository has no
# configuration entry, we look for one on its owner. A "final" flag is
# available to force always reading a configuration entry from a lower-priority
# entity (such as an organization instead of a repository).
#
# Values are stored as strings, so users must encode/decode in their config
# implementation, if necessary.
class Configuration
  FALSE = "false".freeze
  TRUE  = "true".freeze

  # Internal: Minimal AR class to persist the configuration values
  # Consider this an implementation detail
  class Configuration::Entry < ApplicationRecord::Domain::ConfigurationEntries
    self.table_name = "configuration_entries"

    KEY_MAX_LENGTH = 80
    VALUE_MAX_LENGTH = 255

    # keys for attributes that don't constitute a functional change to the
    # configuration entry
    NON_VALUE_ATTRIBUTES = %w(updater_id).freeze

    belongs_to :target, polymorphic: true
    belongs_to :updater, class_name: "User"

    validates_length_of :name, maximum: KEY_MAX_LENGTH
    validates_length_of :value, maximum: VALUE_MAX_LENGTH

    scope :global, -> { where(target_id: 0, target_type: "global") }

    def target
      return GitHub if target_type == "global"
      super
    end

    include Instrumentation::Model

    def event_prefix() :config_entry end
    def event_payload
      payload = {
        target_type: target_type,
        target_id: target_id,
        name: name,
        value: value,
        final: final,
        actor: updater,
      }

      target_key = case target
      when Organization
        :org
      when User
        :user
      when Repository
        :repo
      when Business
        :business
      end

      payload[target_key] = target if target_key

      if ["User", "Organization", "Repository", "Business"].include?(target_type)
        payload.merge!(target.event_context)
      end

      payload
    end

    after_create_commit  :instrument_create
    after_update_commit  :instrument_update
    after_destroy_commit :instrument_destroy

    def instrument_create
      instrument :create
    end

    def instrument_update
      instrument :update, old_value: value_before_last_save
    end

    # Used for :destroy instrumentation, to keep track of who's removing the entry
    attr_accessor :destroyer
    def instrument_destroy
      instrument :destroy, actor: destroyer
    end
  end

  def initialize(target)
    @target = target
  end

  # Internal: Get a snapshot of the current configuration
  # Handles whatever cascading or overridding is appropriate
  #
  # Returns a Hash of name => value pairs,
  # where name is a String and value is a Configuration::Entry object (with partial data)
  def entries
    return @entries if @entries

    entries = {}

    targets = [*@target.configuration_owners, @target].compact.uniq
    where = targets.collect do |target|
      %Q{
        (target_id = #{target.configuration_entry_id.inspect} AND
          target_type = #{target.configuration_entry_type.inspect})}
    end.join(" OR ")

    query = %Q{
      SELECT name, value, final,
        target_type, target_id,
        CASE target_type
        WHEN 'global'     THEN 0
        WHEN 'Business'   THEN 1
        WHEN 'User'       THEN 2
        WHEN 'Repository' THEN 3
        END AS priority
      FROM configuration_entries
      WHERE (#{where})
      ORDER BY priority ASC
    }

    sql = Entry.github_sql.new
    sql.add query

    # Iterate over each configuration entry in reverse priority order, halting
    # early if a lower-priority entry has a "final" flag (indicating we should
    # ignore higher-priority entries).
    sql.models(Entry).each do |entry|
      current = entries[entry.name]
      next if current && current.final?
      entries[entry.name] = entry
    end

    # memoization is sketchy on the global object, so don't use it
    if @target != GitHub
      @entries = entries.freeze
    end

    entries
  end

  # Internal: Reset the snapshot of the current configuration
  # The next call to `#entries` will get an updated snapshot
  #
  # Returns nothing
  def reset
    @entries = nil
  end

  # Public: Get a hash representation of the current configuration snapshot
  #
  # Returns a Hash of name => value pairs, where both name and value are Strings
  def to_hash
    result = {}
    configs = entries  # memoization can only do so much
    configs.each_key { |k| result[k] = configs[k].value }
    result
  end

  # Public: Enable a configuration flag by setting the value to "true".
  #
  # name    - String entry name
  # updater - User setting the value
  #
  # Returns true if the configuration changed, false otherwise
  def enable(name, updater)
    set(name, TRUE, updater)
  end

  # Public: Enable a configuration flag by setting the value to "true",
  # but use set! to override lower values.
  #
  # name    - String entry name
  # updater - User setting the value
  # final   - Boolean determining if this overrides lower values (default: true)
  #
  # Returns true if the configuration changed, false otherwise
  def enable!(name, updater, final = true)
    set!(name, TRUE, updater, final)
  end

  # Public: Is this entry enabled?
  #
  # name - String entry name
  #
  # Returns true or false.
  def enabled?(name)
    get(name) == TRUE
  end

  # Public: Disable a configuration flag by setting the value to "false".
  # To remove the flag completely use delete.
  #
  # name    - String entry name
  # updater - User setting the value
  #
  # Returns true if the configuration changed, false otherwise
  def disable(name, updater)
    set(name, FALSE, updater)
  end

  # Public: Disable a configuration flag by setting the value to "false" using
  # set! to override lower values.  To remove the flag completely use delete.
  #
  # name    - String entry name
  # updater - User setting the value
  # final   - Boolean determining if this overrides lower values (default: true)
  #
  # Returns true if the configuration changed, false otherwise
  def disable!(name, updater, final = true)
    set!(name, FALSE, updater, final)
  end

  # Public: Get the configuration value for a given key
  #
  # name - String entry name
  #
  # Note: Translates a value of "false" to false
  #
  # Returns a String, nil, or false
  def get(name)
    # BUG: Linter false positive using different raw() helper.
    value = raw(name) # rubocop:disable Rails/OutputSafety
    return false if value == FALSE

    value
  end

  # Public: Get the configuration value for a given key as an integer
  #
  # name - String entry name
  #
  # Returns a Integer or nil
  def int(name)
    get(name).try(:to_i)
  end

  # Public: Get the raw configuration value for a given key
  #
  # name - String entry name
  #
  # Returns a String or nil
  def raw(name)
    entry = entries[name.to_s]
    entry && entry.value
  end

  # Public: Check if the configuration for the given key is final
  #
  # name - String entry name
  #
  # Returns a Boolean
  def final?(name)
    entry = entries[name.to_s]
    entry && entry.final?
  end

  # Public: Check if the configuration entry can be written to on this object
  #
  # name - String entry name
  #
  # Returns a Boolean
  def writable?(name)
    !inherited?(name) || !final?(name)
  end

  # Public: Check if the configuration for the given key is set on the current object
  #
  # name - String entry name
  #
  # Returns a Boolean
  def local?(name)
    entry = entries[name.to_s]
    # check using the non-association `target_*` data to
    # avoid n+1 and issues loading the target association in async contexts
    entry &&
      entry.target_id == @target.configuration_entry_id &&
      entry.target_type == @target.configuration_entry_type
  end

  # Public: Check if the configuration for the given key is set on another object
  #
  # name - String entry name
  #
  # Returns a Boolean
  def inherited?(name)
    entry = entries[name.to_s]
    # check using the non-association `target_*` data to
    # avoid n+1 and issues loading the target association in async contexts
    entry &&
      (entry.target_id != @target.configuration_entry_id ||
        entry.target_type != @target.configuration_entry_type)
  end

  # Public: The object a given key was set on
  #
  # name - String entry name
  #
  # Returns a Configurable
  def source(name)
    entry = entries[name.to_s]
    entry && entry.target
  end

  # Public: Set a configuration value for a given key
  #
  #   name    - String for the entry name
  #   value   - String for the entry value
  #   updater - User setting the value
  #
  # Returns true if the configuration changed, false otherwise
  def set(name, value, updater, &block)
    entry = @target.configuration_entries.where(name: name.to_s).first_or_initialize(name: name.to_s)

    entry.final   = false
    entry.value   = value
    entry.updater = updater
    yield entry if !block.nil?

    # if the only changes were non-functional, the configuration entry
    # wasn't really updated, reload the entry and move along
    if (entry.changed_attributes.keys - Configuration::Entry::NON_VALUE_ATTRIBUTES).empty?
      entry.reload
      return false
    end

    entry.save!
    reset
    true
  end

  # Public: Set a configuration value for a given key, which by default will
  # override lower values instead of being overridden.
  #
  # Uses the overly-clever block argument, which will not be discussed further.
  #
  #   name    - String for the entry name
  #   value   - String for the entry value
  #   updater - User setting the value
  #   final   - Boolean determining if this overrides lower values (default: true)
  #
  # Returns true if the configuration changed, false otherwise
  def set!(name, value, updater, final = true)
    set(name, value, updater) { |e| e.final = final }
  end

  # Public: Remove a configuration value for a given key
  #
  #   name    - String entry name
  #   deleter - User removing the value
  #             (optional, for now)
  #
  # Returns true if the entry was deleted, false otherwise
  def delete(name, deleter = nil)
    entry = @target.configuration_entries.find_by_name(name.to_s)
    return false unless entry

    entry.destroyer = deleter
    entry.destroy
    reset
    true
  end
end
