# frozen_string_literal: true

module PushNotifiable
  extend ActiveSupport::Concern
  include ActionView::Helpers::TextHelper
  include ActionView::Helpers::SanitizeHelper

  def push_notification_title
    "@#{notifications_author.login} mentioned you"
  end

  def push_notification_subtitle
    nil
  end

  def push_notification_body
    body = try(:body_html) || try(:body)
    return unless body.present?

    stripped_body = strip_tags(body)
    # Make sure HTML entities like &amp; and &lt; get decoded to & and <
    decoded_body = Nokogiri::HTML.fragment(stripped_body).text

    truncate(
      decoded_body,
      length: Newsies::PushNotification::MAX_BODY_LENGTH,
      separator: /s+/,
      omission: " …",
      escape: false,
    )
  end

  def push_notification_url
    try(:permalink)
  end

  def push_notification_subject_id
    global_relay_id
  end
end
