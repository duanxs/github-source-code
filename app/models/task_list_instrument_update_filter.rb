# rubocop:disable Style/FrozenStringLiteralComment

class TaskListInstrumentUpdateFilter
  def self.after(controller)
    return if (tracking = tracking_for(controller)).nil?
    return if (key = key_for(controller)).nil?
    return unless controller.response.status.to_i == 200

    # Provide context to event subscribers.
    begin
      actor = controller.send(:current_user)
      repository = controller.send(:current_repository)
    rescue NoMethodError
      # Extra context not available.
    end

    payload = tracking.merge(
      key: key,
      actor_id: actor ? actor.id : nil,
      repository_id: repository ? repository.id : nil)

    GitHub.instrument "task_list.update", payload
  end

  def self.tracking_for(controller)
    track = controller.params[:task_list_track]
    return nil if track.blank?

    if track.start_with?("checked:")
      value =
        case track.split(":")[1]
        when "0", "false", false
          false
        when "1", "true", true
          true
        end
      return value.nil? ? nil : { type: :checked, value: value }
    end

    if track == "reordered"
      return { type: :reordered }
    end
  end

  def self.key_for(controller)
    controller.params[:task_list_key]
  end
end
