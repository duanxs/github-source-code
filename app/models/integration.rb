# rubocop:disable Style/FrozenStringLiteralComment

class Integration < ApplicationRecord::Domain::Integrations
  LEGACY_EVENT_ID_CUTTOFF = 68_000

  areas_of_responsibility :ecosystem_apps
  include PrimaryAvatar::Model
  include Avatar::List::Model
  include GitHub::UserContent
  include GitHub::FlipperActor
  include OauthAccess::Provider
  include GitHub::Relay::GlobalIdentification
  include AppNameValidity
  include Marketplace::Listable

  include Permissions::Attributes::Wrapper
  self.permissions_wrapper_class = Permissions::Attributes::Integration

  extend GitHub::BackgroundDependentDeletes

  extend GitHub::Encoding
  force_utf8_encoding :description

  alias_attribute :to_s, :name
  alias_attribute :plaintext_secret, :secret

  # A multiplier applied to abuse/secondary rate limits
  # for github trusted applications.
  TRUSTED_ABUSE_LIMIT_MULTIPLIER = 4

  # default error message for an invalid URL
  INVALID_URL_MESSAGE = "must be a valid URL".freeze

  IDENTICON_TYPE = "app".freeze

  KEY_PREFIX_V1 = "Iv1".freeze

  # Public: The Regexp describing the format of an Integrations's string
  # key value.
  KEY_PATTERN_V1 = %r{
   \A               # start
   #{KEY_PREFIX_V1} # Letter I prefix  the token format version
   \.               # a period
   [a-f0-9]{16}     # the random portion of the token
   \z               # end
  }xi
  CLIENT_ID_LENGTH = 8

  RESERVED_SLUGS = %w(new feature).freeze

  ALLOWED_OWNER_TYPES = %w(User Business).freeze

  BROWN_OUT_SCHEDULES = {
    machine_man_brown_out: [
      # August Brownouts
      Time.iso8601("2020-08-01T07:00:00-00:00")...Time.iso8601("2020-08-01T10:00:00-00:00"),
      Time.iso8601("2020-08-01T16:00:00-00:00")...Time.iso8601("2020-08-01T19:00:00-00:00"),
      # September brownouts
      Time.iso8601("2020-09-01T07:00:00-00:00")...Time.iso8601("2020-09-01T10:00:00-00:00"),
      Time.iso8601("2020-09-01T16:00:00-00:00")...Time.iso8601("2020-09-01T19:00:00-00:00"),
      # Disable entirely # TODO: remove after this date
      Time.iso8601("2020-10-01T23:59:59-00:00")...Time.iso8601("2025-12-31T23:59:59-00:00"),
    ]
  }

  # Public: The User, Organization, or Business which owns this integration.
  belongs_to :owner, polymorphic: true
  alias user owner

  # Category to which the integration belongs in Marketplace.
  belongs_to :marketplace_category, class_name: "Marketplace::Category"

  # Public: Alias async_user to the async_owner method added by ApplicationRecord::Base based on the
  # belongs_to :owner association above so that the OauthApplication object in the GitHub Platform
  # can handle OauthApplication or Integration instances equally.
  alias_method :async_user, :async_owner

  # Public: A bot that the integration can use for making requests on its own
  # behalf.
  belongs_to :bot,
    inverse_of: :integration,
    dependent: :destroy,
    autosave: true

  # Public: Installations for this integration.
  has_many :installations,
    class_name: "IntegrationInstallation",
    dependent: :destroy

  # Public: Hook used to deliver events to an integration. This one Hook
  # receives events for all of the integration's installations.
  has_one :hook,
    class_name: "Hook",
    as: :installation_target,
    inverse_of: :installation_target,
    dependent: :destroy

  accepts_nested_attributes_for :hook, reject_if: :all_blank, update_only: true
  validates_associated :hook

  # Public: The keys used to sign and verify integration access tokens.
  # Supports multiple keys to allow for rotating keys.
  has_many :public_keys,
    class_name: "IntegrationKey",
    dependent: :destroy

  # Public: The listing information for the Integration Directory, if any.
  has_one :integration_listing,
    as: :integration,
    dependent: :destroy

  # Public: Association relationship for IntegrationTransfers.
  has_one :transfer,
    class_name: "IntegrationTransfer",
    dependent: :destroy

  has_many :versions,
    -> { order("number") },
    class_name: "IntegrationVersion",
    autosave: true,
    dependent: :destroy

  has_one :latest_version,
    -> { order("number DESC") },
    autosave: true,
    class_name: "IntegrationVersion",
    inverse_of: :integration

  has_one :alias,
    class_name: "IntegrationAlias",
    autosave:   true,
    dependent: :destroy

  has_one :enterprise_installation

  # DefaultIntegrationPermissions representing the default permissions
  # that this integration requests at installation time.
  delegate :default_permission_records, to: :latest_version

  # HookEventSubscriptions representing the default event types that this
  # integration requests at installation time.
  delegate :default_event_records, to: :latest_version

  # Public: OauthAccesses granted to this Integration.
  has_many :accesses,
    -> { where(application_type: "Integration") },
    class_name: "OauthAccess",
    foreign_key: :application_id,
    dependent: :destroy

  # Public: OauthAccesses granted to this Integration.
  has_many :authorizations,
    -> { where(application_type: "Integration") },
    class_name: "OauthAuthorization",
    foreign_key: :application_id,
    dependent: :destroy

  has_many :pending_installation_requests,
    class_name: "IntegrationInstallationRequest"
  destroy_dependents_in_background :pending_installation_requests

  # Public: IPWhitelistEntries for connections from this Integration
  has_many :ip_whitelist_entries, as: :owner, dependent: :destroy

  # Public: The owner of this integration.
  # column :owner_id, :owner_type
  validates_presence_of :owner
  validates :owner_type, presence: true, inclusion: ALLOWED_OWNER_TYPES

  # Public: The bot account for this integration.
  # column :bot_id
  validates_presence_of :bot

  # Public: String URL of this integration.
  # column :url
  validates_presence_of :url
  validate :validate_integration_url

  # Public: String callback URL of this integration which we'll
  # redirect to during the callback phase of the identity flow.
  # column :callback_url
  validates_presence_of :callback_url, if: :request_oauth_on_install
  validate :validate_callback_url

  # Public: String setup URL of this integration which we'll
  # redirect a user to after installation of the integration.
  # column :setup_url
  validate :validate_setup_url

  # Public: String name of this integration.
  # column :name
  validates_presence_of :name
  validate :restrict_names_with_github

  # Public: String client_id of this oauth application.
  # column :key
  validates_presence_of :key
  validates_uniqueness_of :key, case_sensitive: true

  # Public: String client_secret of this oauth application.
  # column :secret
  validates_presence_of :secret

  # Public: String slug of this integration.
  # column :slug
  validate :validate_integration_slug, if: :name_changed?
  validate :slug_is_not_reserved, if: :slug_changed?
  validate :slug_is_not_existing_owner, if: :slug_changed?
  alias_attribute :to_param, :slug

  validates_with CreatedApplicationsLimitValidator, on: :create

  before_validation :set_default_hook_attributes

  before_validation :normalize_bgcolor
  before_validation :generate_slug,          if: :name_changed?
  before_validation :generate_bot_slug,      if: :slug_changed?
  before_validation :generate_alias_slug,    if: :slug_changed?

  after_validation :format_version_error_messages
  after_validation :prevent_setup_on_update, if: :setup_url_missing?

  before_create :default_user_token_expiration_enabled
  attr_accessor :user_token_expiration_enabled

  after_create_commit :instrument_creation
  after_destroy_commit :instrument_deletion

  before_update :update_latest_version

  after_update :async_invalidate_content_attachment_cache, if: :saved_change_to_name?
  before_destroy :async_invalidate_content_attachment_cache

  after_commit :synchronize_search_index

  require_dependency "integration/configuration_dependency"
  require_dependency "integration/permissions"

  include Integration::RepositoriesDependency
  include Integration::TokenScanningDependency
  include Integration::Sequence

  setup_sequence

  scope :with_latest_version, -> { includes(:latest_version) }

  # Returns Integrations that the given User owns or can manage because they're owned by an
  # Organization for which the given User is an admin.
  scope :adminable_by, ->(user) {
    user_ids = [user.id]

    adminable_org_ids = Ability.user_admin_on_organizations(
      actor_id: user.id,
    ).pluck(:subject_id)
    user_ids = adminable_org_ids.concat(user_ids) if adminable_org_ids.present?

    where(owner_id: user_ids)
  }

  # Returns Integrations that do not have a Marketplace::Listing.
  scope :not_in_marketplace, -> {
    left_outer_joins(:marketplace_listing).where(marketplace_listings: { listable_id: nil })
  }

  # GitHub applications that are not considered first-party (i.e. not full-
  # trust). Not all GitHub Apps owned by GitHub are necessarily full-trust.
  # These applications are treated like any other third-party app.
  scope :third_party, -> {
    where(full_trust: false)
  }

  # Returns Integrations that are shown in Marketplace but need a category.
  scope :for_classification, -> {
    select("*, integrations.id as id, integrations.slug as slug, integrations.name as name, count(integration_installations.id) as installation_count").
        not_in_marketplace.
        public.
        where(marketplace_category_id: nil).
        where.not(description: [nil, ""]).
        joins(:installations).
        group(:id).
        having("installation_count >= 25")
  }

  # TODO: Get rid of `no_repo_permissions_allowed` as its no longer used
  attr_writer :skip_restrict_names_with_github_validation, :skip_generate_slug, :no_repo_permissions_allowed

  scope :not_marked_for_deletion, -> { where(deleted_at: nil) }

  scope :not_for_github_connect, -> {
    where.not(id: EnterpriseInstallation.pluck(:integration_id))
  }

  # Used when integrations have been scoped to a repository by installations.
  #
  # This will likely not work on the full integrations table as there is
  # currently no combined index on name and slug.
  scope :name_or_slug_like, -> (query_str) {
    query = "%#{ActiveRecord::Base.sanitize_sql_like(query_str)}%"
    where(["#{table_name}.name LIKE ? OR #{table_name}.slug LIKE ?", query, query])
  }

  # Exclude Integrations that are internal, returning only public integrations.
  def self.public
    where(public: true)
  end

  # Public: Determines the number of Integrations created (owned) by user
  #
  # Returns an Integer
  def self.created_by_count(user)
    user.integrations.count
  end

  def self.from_owner_and_slug!(viewer: nil, slug:, user_login: nil, business_slug: nil)
    finder = third_party

    if user_login || business_slug
      raise ActiveRecord::RecordNotFound unless GitHub.flipper[:owner_scoped_github_apps].enabled?(viewer)

      owner = if user_login
        User.find_by!(login: user_login)
      else
        Business.find_by!(slug: business_slug)
      end

      finder = finder.where(owner: owner)
    end

    finder.find_by!(slug: slug)
  end

  # Public: The latest version for the integration
  def latest_version
    super || build_latest_version
  end

  # Public: The collection of resources that this integration requests access to.
  #
  # Example
  #   permissions
  #   # => { "statuses" => :write, "issues" => :read }
  #
  # Returns a Hash
  delegate :default_permissions, to: :latest_version

  # Public: The collection of resources that this integration requests access to.
  #
  # Example
  #   async_default_permissions.sync
  #   # => { "statuses" => :write, "issues" => :read }
  #
  # Returns a Promise
  def async_default_permissions
    async_latest_version.then do |version|
      version.async_default_permissions
    end
  end

  # Public: Set the collection of resources this integration requests access to.
  #
  # assigned_permissions - A Hash of resource Strings to permission Symbols.
  #
  # Returns the Hash of assigned permissions.
  def default_permissions=(assigned_permissions)
    current_version.default_permissions = assigned_permissions
  end

  # Public: The collection of event names that this integration subscribes to.
  #
  # Example
  #   events
  #   # => ["issues", "pull_request"]
  #
  # Returns an Array
  delegate :default_events, to: :latest_version

  # Public: The collection of event names that this integration subscribes to.
  #
  # Example
  #   async_default_events.sync
  #   # => ["issues", "pull_request"]
  #
  # Returns a Promise
  def async_default_events
    async_latest_version.then do |version|
      version.async_default_events
    end
  end

  # Public: Set the collection of event names this integration should subscribe
  # to.
  #
  # event_names - An Array of event names.
  #
  # Returns an Array of subscribed event names.
  def default_events=(event_names)
    # Exclude any integrator events.
    event_names -= Integration::Events::INTEGRATOR_EVENTS

    # In the event that default events are set
    # on a new record, there won't be a hook even if there
    # are hook attributes.
    #
    # Build a hook so that if there are failures
    # they show up in the UI.
    if event_names.any? && new_record? && hook.nil?
      build_hook # The other default settings are set at before_validation
    end

    return [] if hook.nil?

    transaction do
      current_version.default_events = event_names

      # In order to support any all possible events we will add
      # to the list of events instead of replacing it everytime the
      # default events are updated.
      hook.add_events(event_names)
    end
  end

  delegate :default_content_references, to: :latest_version

  def default_content_references=(content_references)
    current_version.default_content_references = content_references
  end

  def content_reference=(content_reference)
    return if content_reference.empty?
    self.default_content_references = content_reference.reduce({}) { |h, v| h[v] = "domain"; h }
  end

  # Public: The collection of event names that this integration subscribes to,
  # but does not pass along to its integration versions nor its installations.
  #
  # Example
  #   integrator_events
  #   # => ["security_advisory"]
  #
  # Returns an Array
  def integrator_events
    return [] if hook.nil?
    hook.events & Integration::Events::INTEGRATOR_EVENTS
  end

  # Public: Set the collection of integrator event names this integration should
  # subscribe to. Unlike #default_events=, these events are not persisted to the
  # current integration version. They are only associated with the hook for the
  # integration itself.
  #
  # The #default_events= method is only additive when it comes to the
  # integration's hook's events. The #integrator_events= method must also remove
  # events that should no longer be subscribed, without clobbering any default
  # events.
  #
  # event_names - An Array of event names.
  #
  # Returns an Array of subscribed event names.
  def integrator_events=(event_names)
    # Include only integrator events.
    event_names &= Integration::Events::INTEGRATOR_EVENTS

    # In the event that integrator events are set
    # on a new record, there won't be a hook even if there
    # are hook attributes.
    #
    # Build a hook so that if there are failures
    # they show up in the UI.
    if event_names.any? && new_record? && hook.nil?
      build_hook # The other default settings are set at before_validation
    end

    return [] if hook.nil?

    subscribed = integrator_events
    to_add = event_names - subscribed
    to_remove = subscribed - event_names

    transaction do
      hook.add_events(to_add)
      hook.remove_events(to_remove)
    end
  end

  delegate :note, to: :latest_version

  def note=(note)
    current_version.note = note
  end

  # Public: The String single_file_name that this integration
  # has access too.
  #
  # Example
  #   single_file_name
  #   # => ".github"
  #
  # Returns a String.
  delegate :single_file_name, to: :latest_version

  # Public: Set the String file name that maps to a
  # single file in the repository.
  #
  # file_name - The String mapping to the file name.
  #
  # Returns a String.
  def single_file_name=(file_name)
    return if file_name.blank?
    transaction { current_version.single_file_name = file_name }
  end

  def user_token_expiration_enabled?
    user_token_expiration
  end

  # Public: Give this integration access to repositories
  #
  # target        - The User or Organization account that the application is
  #                 being installed on.
  # repositories: - An Array of Repositories to include in the installation.
  # installer:    - The User performing the installation.
  # version       - The IntegrationVersion used for permissions and events.
  #                 Defaults to the `latest_version`.
  # trigger_id    - id of the integration_install_trigger that led to this
  #                 installation
  #
  # reinstalling_during_repository_transfer - A boolean flag used to indicate
  #                                         whether or not we're reinstalling an app
  #                                         during repository transfers
  #
  # Returns an IntegrationInstallation::Creator::Result.
  def install_on(target, repositories:, installer:, version: self.latest_version, trigger_id: nil, reinstalling_during_repository_transfer: false)
    options = {
      repositories:      repositories,
      installer:         installer,
      version:           version,
      trigger_id:        trigger_id,
      reinstalling_during_repository_transfer: reinstalling_during_repository_transfer,
    }

    IntegrationInstallation::Creator.perform(self, target, **options)
  end

  # Public: Is this integration installed on the specified target?
  #
  # target - A User or Organization account.
  #
  # Returns a Boolean.
  def installed_on?(target)
    installations_on(target).exists?
  end

  # Public: The installations for the specified target.
  #
  # target - A User or Organization account.
  #
  # Returns a AR scope of the installations for the target
  def installations_on(target)
    installations.with_target(target)
  end

  # Public: Is this integration owned by a user?
  #
  # Returns a Boolean
  def user_owned?
    owner_type == "User" && owner.type == "User"
  end

  # Public: Is this integration owned by a organization?
  #
  # Returns a Boolean
  def organization_owned?
    owner_type == "User" && owner.type == "Organization"
  end

  def adminable_by?(actor)
    owner.adminable_by?(actor)
  end

  def avatar_editable_by?(actor)
    ::Permissions::Enforcer.authorize(
      action: :manage_app,
      actor: actor,
      subject: self,
    ).allow?
  end

  # Public: Generate and persist a new public key for the integration.
  def generate_key(creator:)
    self.public_keys.transaction do
      self.public_keys.create(creator: creator)
    end
  end

  # Public: Determine whether or not a given user can view details about
  # an integration including description.
  #
  # For public integrations this is always true. For internal integrations
  # this is only true for the owning user or a member of the owning organization.
  #
  # Returns a Boolean.
  def readable_by?(actor)
    async_readable_by?(actor).sync
  end

  def async_readable_by?(actor)
    return Promise.resolve(true) if public?
    if actor.respond_to?(:integration)
      # an App viewing itself
      return Promise.resolve(true) if actor.integration == self
    end

    async_owner.then do |owner|
      if owner.respond_to?(:async_member?)
        owner.async_member?(actor)
      else
        owner.async_adminable_by?(actor)
      end
    end
  end

  # Public: Determine whether or not a given user is allowed to install the
  # integration.
  #
  # For public integrations this is true for signed in users.
  # For internal integrations this is only true for the owning user, or an admin of the
  # owning organization, or a user who can admin any repositories of the owning organization.
  #
  # Returns a Boolean.
  def installable_by?(user)
    return false unless user
    return true if public?

    installable_on_by?(target: owner, actor: user)
  end

  # Public: Determine whether or not a given user is allowed to install the
  # integration on the given target.
  #
  # Returns an instance of Integration::Permissions::Result.
  def installable_on_by(target:, actor:, repositories: nil)
    options = {
      integration:  self,
      actor:        actor,
      action:       :install,
      target:       target,
      repositories: repositories,
      version:      latest_version,
    }

    if actor&.user? && options[:repositories].nil?
      options[:repositories] ||= begin
        target_owned_repo_ids = Repository.owned_by(target).ids
        repo_ids = actor.associated_repository_ids(min_action: :admin, repository_ids: target_owned_repo_ids)

        workspace_repo_ids = RepositoryAdvisory.where(workspace_repository_id: repo_ids).pluck(:workspace_repository_id)

        Repository.where(id: repo_ids - workspace_repo_ids)
      end
    end

    Integration::Permissions.check(**options)
  end

  # Public: Determine whether or not a given user is allowed to install the
  # integration on the given target.
  #
  # Returns a Boolean.
  def installable_on_by?(target:, actor:, repositories: nil)
    check = installable_on_by(target: target, actor: actor, repositories: repositories)
    check.permitted?
  end

  # Can the given user install thie GitHub App
  # on all repositories on the target account.
  #
  # Returns Boolean.
  def installable_on_all_repositories_by?(target:, actor:)
    Integration::Permissions.check(
      integration:      self,
      actor:            actor,
      action:           :install,
      target:           target,
      all_repositories: true,
      version:          latest_version,
    ).permitted?
  end

  # Public: Determin whether the given user can request installation
  # of this GitHub App on this target account?
  #
  # Returns an instance of Integration::Permissions::Result.
  def requestable_on_by(target:, actor:)
    Integration::Permissions.check(
      integration: self,
      actor:       actor,
      action:      :request_installation,
      target:      target,
    )
  end

  # Can the given user request installation
  # of this GitHub App on this target account?
  #
  # Returns a Boolean.
  def requestable_on_by?(target:, actor:)
    check = requestable_on_by(target: target, actor: actor)
    check.permitted?
  end

  # Public: Determine whether or not the integration is installable on a given
  # account. For public integrations this is true for any account. For internal
  # integrations, this is only true for the owning account.
  #
  # Returns a Boolean.
  def installable_on?(target)
    return true if public?
    return true if internal? && github_full_trust? # rubocop:disable GitHub/FullTrust

    target == owner
  end

  # Public: Can this integration request the installer to also authorize the
  # user's account via OAuth?
  #
  # Note: Based on this model's internal boolean attribute `request_oauth_on_install`.
  #
  # Returns a Boolean.
  def can_request_oauth_on_install?
    can_send_callback_requests? && request_oauth_on_install?
  end

  def internal?
    !public?
  end

  def can_make_internal?
    return false if integration_listing.present?
    installations.none? { |installation| installation.target != owner }
  end

  def make_public
    update_attribute :public, true
  end

  def make_public!
    update!(public: true)
  end

  def make_internal
    return false unless can_make_internal? || internal?

    update_attribute :public, false
  end

  # Public: Synchronize this listing with its representation in the search index.
  # If the listing is publicly listed and not sponsorable, it will be updated in the search index.
  # If the listing has been destroyed or archived, it will be removed from the search index.
  # This method handles both cases.
  def synchronize_search_index
    if destroyed? || !public?
      RemoveFromSearchIndexJob.perform_later("github_app", self.id)
    elsif public?
      Search.add_to_search_index("github_app", self.id)
    end

    self
  end

  # Used for GitHub::UserContent
  def body
    description
  end

  include Instrumentation::Model

  def event_prefix
    :integration
  end

  def event_payload
    {}.tap do |payload|
      payload[:integration]       = self
      payload[:app]               = self
      payload[:name]              = name
      payload[:slug]              = slug
      payload[owner.event_prefix] = owner
    end
  end

  def event_context(prefix: :app)
    {
      "#{prefix}".to_sym    => name,
      "#{prefix}_id".to_sym => id,
    }
  end

  def default_user_token_expiration_enabled
    bool = if defined?(@user_token_expiration_enabled)
      ActiveModel::Type::Boolean.new.cast(user_token_expiration_enabled)
    else
      true
    end

    self.user_token_expiration = bool
  end

  def instrument_creation
    GitHub.dogstats.increment("integration", tags: ["action:create"])
    instrument :create
  end

  def instrument_deletion
    instrument :destroy
  end

  def instrument_github_app_manager(member, action:)
    case action
    when :grant
      instrument :manager_added, manager: member
    when :revoke
      instrument :manager_removed, manager: member
    end
  end

  def pending_transfer?
    transfer.present?
  end

  # Public: Whether to include this app in marketplace searches; if the app has
  #         been publicly listed, then defer to the listing instead.
  def include_in_marketplace_searches?
    return false unless self.id.present?

    listing = Marketplace::Listing.find_by(listable: self)
    !listing.present? || listing.draft?
  end

  # Public: Get an image URL for the logo that best represents this app, ignoring the current
  # user and whether this app is listed in the Marketplace or not.
  def preferred_avatar_url(size: 80)
    return primary_avatar_url(size) if primary_avatar
    Rails.application.routes.url_helpers.app_identicon_path(IDENTICON_TYPE, slug)
  end

  def primary_avatar_path
    return @primary_avatar_path if defined?(@primary_avatar_path)
    return @primary_avatar_path = "/in/#{id}" if primary_avatar

    # Back up to owner's avatar
    user = owner
    user ||= User.ghost

    @primary_avatar_path = user.primary_avatar_path
  end

  def primary_avatar_url(size = nil)
    return super if size.nil? || primary_avatar

    # The User Avatar needs to be double the
    # given size
    size *= 2

    super
  end

  # Public: Transfers the integration to a target user/organization and deletes
  # any transfer requests for the integration.
  #
  # target     - The User or Organization to transfer the integration to.
  # requester  - The User requesting the transfer.
  # responder  - The User completing the transfer.
  #
  # Returns a Boolean.
  def transfer_ownership_to(target, requester:, responder:)
    instrument :transfer, \
      transfer_from: owner,
      transfer_to: target,
      requester: requester,
      responder: responder

    self.owner = target

    transaction do

      result = remove_app_manager
      return false if result.failure?

      save!
      regenerate_hook! if hook.present?
      transfer.destroy if transfer

      true
    end
  end

  def keep_old_avatar?
    false
  end

  # Internal: can we send requests to this integration's callback_url?
  def can_send_callback_requests?
    callback_url.present?
  end

  # Public: check if a url is a direct match of the callback_url.
  #
  # This is used in OauthUtil and is implemented to match OauthApplication logic.
  #
  # Returns a boolean.
  def callback_url_direct_match?(url)
    url == callback_url
  end

  # Public: URI version of callback_url.
  #
  # Used in OauthUtil, provided for parity between OauthApps and GitHub Apps.
  #
  # Returns nil or an Addressable::URI
  def callback_uri
    return nil unless callback_url

    Addressable::URI.parse(callback_url)
  end

  # Internal: does this integration have additional setup after installation?
  def additional_setup_required?
    setup_url.present?
  end

  def key
    read_attribute(:key) || write_attribute(:key, random_key)
  end

  def secret
    read_attribute(:secret) || write_attribute(:secret, SecureRandom.hex(20))
  end
  alias plaintext_secret secret

  def outdated_installations(version = latest_version)
    installations.where("integration_installations.integration_version_number < ?", version.number)
  end

  def installations_with_version(version_number)
    installations.where("integration_installations.integration_version_number = ?", version_number)
  end

  def async_installation_for(resource)
    case resource
    when Repository
      Platform::Loaders::IntegrationInstallation::Repository.load(resource, id)
    else
      Promise.resolve(nil)
    end
  end

  # Public: Boolean flag to tell if an application is a full trust OAuth app
  # and either owned by GitHub or used for GitHub Connect.
  #
  # Returns false for Integrations.
  def github_full_trust?
    full_trust? && (github_owned? || connect_app?)
  end

  # Public: Boolean flag to tell if an integration is owned by GitHub.
  #
  # Returns true if this integration is owned by GitHub, Inc.
  def github_owned?
    owner_id == GitHub.trusted_apps_owner_id
  end

  # Public: Boolean flag to tell if an integration is used for GitHub Connect
  #
  # Returns true if this integration has a corresponding GitHub Connect
  # EnterpriseInstallation.
  def connect_app?
    enterprise_installation.present?
  end

  # Public: Boolean flag to tell if an application uses Oauth scopes.
  #
  # Returns false for all Integrations.
  def uses_scopes?
    false
  end

  # Indicates if the Integration should be exempted from organization
  # OAuth Application policies.
  #
  # Returns a Boolean.
  def organization_policy_exempt?
    true
  end

  # Indicates if third party restrictions can be applied to this integration.
  # False for all Integrations.
  def third_party_restrictions_applicable?
    false
  end

  # Indicates if the Integration is suspended or not.
  # Default "false" present for all Integrations, until such times as a `state`
  # is implemented for Integrations, so that there is arity with OauthApplications,
  # to enable oauth_accesses to check for suspended applications before generation.
  def suspended?
    false
  end

  # Right now do not want to have strict validation during the URI
  # redirection.
  def strict_callback_url_validation?
    false
  end

  def repository_installation_required?(target, version = latest_version)
    return false unless version.any_permissions_of_type?(Repository)

    relevant_permissions = version.permissions_relevant_to(target)
    return false if relevant_permissions.empty?

    repository_permissions = version.permissions_of_type(Repository)

    relevant_permissions.any? do |permission, _action|
      repository_permissions.include?(permission)
    end
  end

  def repository_permissions_only?(version = latest_version)
    version.all_permissions_of_type?(Repository)
  end

  def abuse_limits_multiplier
    # https://github.com/github/ecosystem-api/issues/1860#issuecomment-588390136
    Apps::Internal.capable?(:abuse_limit_multiplier, app: self) ? TRUSTED_ABUSE_LIMIT_MULTIPLIER : 1
  end

  def can_set_loopback_webhook?
    Apps::Internal.capable?(:can_set_loopback_webhook, app: self)
  end

  def launch_github_app?
    GitHub.launch_github_app&.id == self.id
  end

  def launch_lab_github_app?
    GitHub.launch_lab_github_app&.id == self.id
  end

  def dependabot_github_app?
    GitHub.dependabot_github_app&.id == id
  end

  # Internal: Determine if the integration is the GroupSyncer service
  def group_syncer_github_app?
    return false if GitHub.group_syncer_github_app_id.to_i.zero?
    GitHub.group_syncer_github_app_id.to_i == self.id
  end

  def accepts_secrets_from_users?
    # Only the prod app has secrets. In lab, we read prod's secrets.
    launch_github_app?
  end

  # Returns true if we should allow this integration to create multiple
  # check suites at any given commit SHA.
  def multiple_check_suites_per_sha_enabled?
    # if this is changed, update the find_or_create_for_integrator test
    launch_github_app? || launch_lab_github_app?
  end

  def reset_secret
    update(secret: nil)
  end

  def async_revoke_tokens
    RemoveIntegrationTokensJob.perform_later(self)
  end

  def revoke_tokens
    authorizations_revoked = 0

    authorizations.find_each do |authorization|
      if authorization.destroy_with_explanation(:integration)
        authorizations_revoked += 1
      end
    end

    instrument :revoke_tokens, tokens_revoked: authorizations_revoked
  end

  def bot_commit_signing_enabled?
    GitHub.web_commit_signing_enabled? &&
      GitHub.flipper[:bot_commit_signing].enabled?(self)
  end

  def platform_type_name
    "App"
  end

  # TODO: Setting this to a hard-coded value to unblock progress of getting the
  # quota based rate limiter shipped.
  def rate_limit
    10_000
  end

  def self.ghost
    GhostGitHubApp.instance
  end

  def ghost?
    self == Integration.ghost
  end

  def subscribable_hook?
    hook && hook.active?
  end

  def fingerprint
    Digest::MD5.hexdigest("#{id}:#{updated_at}:#{latest_version.id}")
  end

  def send_deprecated_event?
    return true if GitHub.enterprise?
    return false unless legacy_event_supported?
    return true if opted_out_of_brown_out?

    !brown_out_in_effect?
  end

  def deprecated_endpoint_enabled?
    return true if GitHub.enterprise?
    return true if connect_app?
    return true if opted_out_of_brown_out?

    !brown_out_in_effect?
  end

  def brown_out_in_effect?
    brown_outs = BROWN_OUT_SCHEDULES[:machine_man_brown_out]
    brown_outs.any? { |range| range.cover?(Time.now.utc) }
  end

  def opted_out_of_brown_out?
    GitHub.flipper[:github_apps_brown_out_opt_out].enabled?(self)
  end

  def legacy_event_supported?
    id < LEGACY_EVENT_ID_CUTTOFF
  end

  private

  # Private: In the event a hook is being created,
  # set the hook name and content_type since these
  # are not available to the creator of the Integration.
  #
  # Returns true.
  def set_default_hook_attributes
    return true if hook.nil?
    return true unless hook.new_record?

    hook.name         = "web"
    hook.content_type = "json"

    true
  end

  def normalize_bgcolor
    return unless bgcolor

    self.bgcolor = bgcolor.sub(/\A#/, "")
  end

  def slug_is_not_reserved
    if RESERVED_SLUGS.include?(slug)
      errors.add(:name, "is a reserved word and cannot be used")
    end
  end

  # Private: validate slug does not match an existing owners's login/slug
  # - This protects against name squatting
  # - Owners can create apps that match their User/Organization login or Business slug
  def slug_is_not_existing_owner
    return unless owner.present?

    owner_slug = case owner
    when Business
      owner.slug
    when User
      owner.login.parameterize
    end

    return if owner_slug == slug

    if owner.is_a?(User)
      return if owner.owned_organizations.where(login: slug).exists?
      return if owner.businesses(membership_type: :admin).where(slug: slug).exists?
    end

    errors.add(:name, "is already in use") if User.where(login: slug).exists?
    errors.add(:name, "is already in use") if Business.where(slug: slug).exists?
  end

  # Private: remove the manage_app grant from the given App manager.
  #
  # Returns a Permissions::GrantResult.
  def remove_app_manager
    results = ::Permissions::Enumerator.actor_ids_with_permission(
      action: :manage_app,
      subject_id: id,
    ).map do |actor_id|
      ::Permissions::Granter.revoke(
        action: :manage_app,
        actor_id: actor_id,
        subject_id: id,
      )
    end

    return ::Permissions::GrantResult.failure! if results.any? { |result| result.failure? }
    ::Permissions::GrantResult.success!
  end

  def restrict_names_with_github
    return unless name && owner
    return if github_owned?
    return if owner.is_a?(User) && owner.employee? && owner.site_admin?
    return if @skip_restrict_names_with_github_validation
    return if !new_record? && name.start_with?("GitHub Enterprise ") && connect_app?

    if name_starts_with_github?
      errors.add(:name, "should not begin with 'GitHub' or 'Gist'")
    end

    if name_implies_github_affiliation?
      errors.add(:name, "should not imply the integration is from GitHub")
    end
  end

  # Private: validate slug is
  # - present (if an invalid name prevented slug generation)
  # - less than or equal to the Bot::MAX_SLUG_LENGTH limit
  # - not already in use
  def validate_integration_slug
    return unless name.present?

    unless slug.present?
      revert_slug
      return errors.add(:name, "must contain at least one alphanumeric character")
    end

    if slug.length > Bot::MAX_SLUG_LENGTH
      revert_slug
      return errors.add(:name, "cannot be longer than #{Bot::MAX_SLUG_LENGTH} characters")
    end

    if self.slug_changed? && Integration.exists?(slug: slug)
      revert_slug
      return errors.add(:name, "is already taken")
    end

    if slug.include? "_"
      revert_slug
      return errors.add(:name, "cannot contain underscores")
    end
  end

  # Private: validate url is a String and valid URL.
  def validate_integration_url
    unless url.is_a?(String)
      return errors.add(:url, INVALID_URL_MESSAGE)
    end

    unless IntegrationUrl.valid_application_url?(url)
      errors.add(:url, INVALID_URL_MESSAGE)
    end
  end

  # Private: validate callback_url is a valid URL and is safe to redirect to.
  def validate_callback_url
    return unless callback_url.present?

    strict_options = {
      blocked_query_keys: OauthUtil::RESERVED_REDIRECT_URI_QUERY_KEYS,
      allow_fragment: false,
    }

    unless IntegrationUrl.valid_callback_url?(callback_url, strict_options)
      errors.add(:callback_url, INVALID_URL_MESSAGE)
    end
  end

  # Private: validate setup_url is a valid URL and is safe to redirect to.
  def validate_setup_url
    return unless setup_url.present?

    unless setup_url.is_a?(String)
      return errors.add(:setup_url, INVALID_URL_MESSAGE)
    end

    unless IntegrationUrl.valid_application_url?(setup_url)
      errors.add(:setup_url, INVALID_URL_MESSAGE)
    end
  end

  def generate_bot_slug
    return unless slug.present?

    bot || build_bot
    bot.slug = slug
  end

  def generate_alias_slug
    return unless slug.present?

    self.alias || build_alias
    self.alias.slug = slug
  end

  def generate_slug
    return unless name.present?
    return if new_record? && @skip_generate_slug && slug.present?
    self.slug = name.parameterize
  end

  def revert_slug
    self.slug = self.slug_was
  end

  def random_key
    suffix = SecureRandom.hex(CLIENT_ID_LENGTH)

    "#{KEY_PREFIX_V1}.#{suffix}"
  end

  def update_latest_version
    latest_version.save
  end

  # Private: Get the current version either used when creating
  # or updating an integration's permissions.
  #
  # Returns an IntegrationVersion
  def current_version
    if new_record?
      @version = latest_version unless defined?(@version)
    else
      @version = versions.build if !defined?(@version)
      @version = versions.build if @version.persisted?
    end

    @version
  end

  def regenerate_hook!
    events = hook.events
    config = hook.config
    active = hook.active

    hook.destroy!
    self.hook = nil

    build_hook(name: "web", content_type: "json")

    hook.config = config
    hook.add_events(events)
    hook.active = active
    hook.save!
  end

  # Because a child version does all of the
  # validation for permissions, events, and
  # single_file_name. We need to format the errors
  # so that they look alright for the integration.
  def format_version_error_messages
    return if self.errors.empty?
    version_error_prefix = %r{\A(latest_version|versions)\.}

    if GitHub.rails_6_0?
      error_names = self.errors.keys
    else
      error_names = self.errors.attribute_names
    end

    error_names.each do |key|
      next unless key.to_s.match?(version_error_prefix)
      new_key = key.to_s.gsub(version_error_prefix, "").to_sym

      self.errors[key].each { |error| errors.add(new_key, error) }
      errors.delete(key.to_sym)
    end
  end

  def setup_url_missing?
    setup_on_update && setup_url.blank? && !can_request_oauth_on_install?
  end

  def prevent_setup_on_update
    self.setup_on_update = false
  end

  def async_invalidate_content_attachment_cache
    ContentReferenceAttachmentRefreshBodyHtmlJob.perform_later(self.id)
  end
end
