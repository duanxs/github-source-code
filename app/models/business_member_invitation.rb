# frozen_string_literal: true

# Represents an invitation to an enterprise account administrator (despite the
# slightly confusing name `BusinessMemberInvitation`), where an administrator
# currently is either an *owner* or *billing manager*.
# The corresponding end-user-facing platform object is named
# "EnterpriseAdministratorInvitation" (see #platform_type_name).
class BusinessMemberInvitation < ApplicationRecord::Domain::Users
  include GitHub::Relay::GlobalIdentification


  enum role: {
    owner: 1,
    billing_manager: 2,
  }

  # Raised when someone accepts an already-accepted invitation.
  class AlreadyAcceptedError < StandardError; end

  # Raised when someone accepts an expired invitation.
  class ExpiredError < StandardError; end

  # Raised when someone other than the invitee tries to accept an invitation.
  class InvalidAcceptorError < StandardError; end

  # Raised when someone attempts to create an invalid BusinessMemberInvitation.
  class InvalidError < StandardError; end

  belongs_to :business
  belongs_to :inviter, class_name: "User"
  belongs_to :invitee, class_name: "User"

  scope :pending, -> { where(accepted_at: nil, cancelled_at: nil, expired_at: nil) }

  # This scope uses a cutoff date to considerably reduces the query cost by
  # preventing an unbounded query on created_at. Designed for use within the
  # ExpireBusinessAdminInvitationsJob.
  scope :recently_expired, -> do
    pending.where \
      "created_at BETWEEN ? AND ?",
      GitHub.invitation_expiry_cutoff.days.ago,
      GitHub.invitation_expiry_period.days.ago
  end

  scope :with_business_role, -> (*roles) {
    where(role: BusinessMemberInvitation.roles.values_at(*roles))
  }

  validates_length_of :email, within: 3..100, allow_blank: true
  validates_format_of :email,
    with: User::EMAIL_REGEX,
    message: "does not look like an email address",
    allow_blank: true
  validates_presence_of :business, :inviter, :role
  validates :hashed_token, presence: true, length: { is: 44 }, if: :email?
  validate :invitation_must_be_allowed!, on: :create
  validate :email_must_be_exclusive

  before_validation :generate_token, on: :create, if: :email?

  after_create :instrument_invite_member
  after_commit :send_invitation_email, on: :create

  attr_accessor :stafftools_invite

  # Public: Returns all pending invitations belonging to an invitee, or any of
  # the invitee's verified UserEmail addresses or the email address supplied.
  #
  # invitee     - A User model. The owner of a pending invitation.
  # emails      - One or more email addresses to consider when searching for
  #               pending invitations.
  # include_private_emails - a Boolean indicating if we should return the private
  #                          email address for a user
  #
  # Returns an Array of pending business member invitations.
  def self.with_invitee_or_normalized_email(invitee: nil, emails: nil, include_private_emails: true)
    emails = UserEmail.safe_bulk_normalize(users: invitee, emails: emails, verified: true, include_private_emails: include_private_emails)
    return [] unless invitee || emails.any?

    if invitee.present?
      scope = where(invitee_id: invitee.id)
      scope = scope.or(where(normalized_email: emails)) if emails.any?
    else
      scope = where(normalized_email: emails)
    end

    scope.order(id: :desc)
  end

  def instrument_invite_member
    if owner?
      business.instrument(:invite_admin, event_payload)
    elsif billing_manager?
      business.instrument(:invite_billing_manager, event_payload)
    end
  end

  def send_invitation_email
    return if inviter.spammy?

    if owner?
      if email?
        BusinessMailer.invited_as_business_admin_by_email(self, token, !!stafftools_invite).deliver_later
      else
        BusinessMailer.invited_as_business_admin(self, !!stafftools_invite).deliver_later
      end
    elsif billing_manager?
      BusinessMailer.invited_as_business_billing_manager(self, token).deliver_later
    end
  end

  # Public: Accept the invitation. This adds the invitee to the specified
  # business.
  #
  # acceptor    - Optional User who is accepting the invitation. Used for email
  #               address invites. Defaults to the invitee.
  # via_email   - Optional flag to indicate the invitation was landed on from
  #               email and should be recorded as such. Defaults to false.
  # auto_accept_duplicates - Optional flag to indicate if we should try to accept
  #                          all duplicate invites when accepting the current invite.
  #
  # Returns nothing.
  def accept(acceptor: invitee, via_email: false, auto_accept_duplicates: true)
    raise ExpiredError, "invitation expired" if expired?
    raise AlreadyAcceptedError, "invitation already accepted" if accepted?
    raise InvalidAcceptorError, "invitee must accept invitation" unless email? || (acceptor == invitee)

    BusinessMemberInvitation.transaction do
      # We record the creator of the invitation as the actor, even though the
      # receiver of the invitation is the one logged-in and accepting it.
      # We need to nil out actor_ip, since we don't want to record the
      # logged-in user (the invitee) as the actor, and we don't know the
      # inviter's IP.
      GitHub.context.push(actor_ip: nil) do
        Audit.context.push(actor_ip: nil) do
          add_invited_member_to_business(acceptor)

          if auto_accept_duplicates
            accept_duplicate_pending_invitations \
              role: role, acceptor: acceptor, via_email: via_email
          end
        end
      end

      touch :accepted_at
    end
  end

  # Public: Has the invitation been accepted?
  #
  # Returns a boolean.
  def accepted?
    accepted_at?
  end

  # Public: Is the invitation pending?
  #
  # Returns a boolean.
  def pending?
    !accepted? && !cancelled? && !expired?
  end

  # Public: Is the invitation cancelled?
  #
  # Returns a boolean
  def cancelled?
    cancelled_at?
  end

  # Public: Is the invitation expired?
  #
  # Returns a boolean
  def expired?
    expired_at?
  end

  # Public: Cancel the invitation.
  #
  # actor - The user canceling the invitation.
  # notify - Should we send an email to the inviter upon cancel?
  #
  # Returns nothing.
  def cancel(actor:, notify: true)
    raise AlreadyAcceptedError, "Can't cancel an accepted invitation" if accepted?

    if owner?
      business.instrument(:cancel_admin_invitation, event_payload.merge(actor: actor))
    elsif billing_manager?
      business.instrument(:cancel_billing_manager_invitation, event_payload.merge(actor: actor))
    end

    touch(:cancelled_at) if persisted?

    if notify && !inviter.spammy?
      if owner?
        BusinessMailer.business_admin_invitation_cancelled(self).deliver_later
      end
    end
  end

  # Public: Expire the invitation.
  #
  # Only expected to be called on persisted invitations.
  #
  # Returns Boolean.
  def expire
    touch :expired_at
  end

  # Public: Can the specified user cancel the specified invitation?
  #
  # canceler - The user trying to cancel the invitation.
  #
  # Returns a boolean.
  def cancelable_by?(canceler)
    case role
    when "owner"
      business.owner?(canceler)
    when "billing_manager"
      business.owner?(canceler) || business.billing_manager?(canceler)
    end
  end

  # Public: Should the inviter be shown to users?
  #
  # This will be false if:
  #   - there's no inviter (which can happen if the inviter
  #     is deleted after sending the invitation).
  #
  # Returns a Boolean.
  def show_inviter?
    !!inviter
  end

  # Public: The String representation of the BusinessMemberInvitation. If the
  # invitation is for a user it will return the user's safe profile name. If the
  # invitation is for an email invitation it will return the email.
  #
  # Returns String.
  def email_or_invitee_name
    if email?
      email
    else
      invitee.safe_profile_name
    end
  end

  # Public: The String representation of the BusinessMemberInvitation. If the
  # invitation is for a user it will return the user's login. If the
  # invitation is for an email invitation it will return the email.
  #
  # Returns String.
  def email_or_invitee_login
    if email?
      email
    else
      invitee.login
    end
  end

  def email=(address)
    self.normalized_email = begin
        UserEmail.normalize(address)
      rescue ArgumentError
        nil # Bad email address
      end

    write_attribute(:email, address)
  end

  # Public: Fetch the hash to use for server-side persistence of the given
  # token.
  #
  # token - A string.
  #
  # Returns the hashed base64 String.
  def self.digest_token(token)
    return nil unless token.present?

    Digest::SHA256.base64digest(token.to_s)
  end

  # Public: Finds an invitation matching the supplied token. If called on an
  # ActiveRecord scope, the current scope is used to lookup the invitation.
  #
  # token - The token String generated by the BusinessMemberInvitation.
  #
  # Returns an BusinessMemberInvitation or nil if the invitation could not be
  # found.
  def self.with_token(token)
    return nil unless token.present?

    where(hashed_token: digest_token(token)).first
  end

  # Public: Stores the raw token in-memory so it's available for the duration of
  # the request. Stores a hash of the token in the database to be used to lookup
  # invitations by token.
  #
  # Returns the set token.
  def token=(token_value)
    self.hashed_token = BusinessMemberInvitation.digest_token(token_value.to_s)
    @token = token_value
  end
  attr_reader :token

  # Public: Resets the token and saves the invitation. Since token is only stored
  # in-memory, this is needed to regenerate links for an invitation (e.g. to resend
  # and invitation email)
  #
  # Returns a Boolean indicating if the reset was successful.
  def reset_token
    return false unless email?

    generate_token && save
  end

  # Public: Determines whether the invitation should be readable by a user
  #
  # user - user to check for access to the invitation
  #
  # Returns true if the invitation is readable by the user, false otherwise
  def readable_by?(user)
    return false if user.nil?
    return false if !user.is_a?(User) || !user.user?

    # Email-only invitations are not associated with a user
    return true if email.present?

    # Let the invitee see their own invitation
    return true if invitee_id == user.id

    # Let business admins see any invitation.
    return true if business.owner?(user)

    # Let business billing managers see billing manager invitations.
    return true if billing_manager? && business.billing_manager?(user)

    false
  end

  def platform_type_name
    "EnterpriseAdministratorInvitation"
  end

  # Public: Returns the role attribute of the invitation for use in a message.
  #
  # Example usage:
  #
  # "You're invited to become #{invitation.role_for_message} of the enterprise."
  #
  # Returns String.
  def role_for_message
    Business.admin_role_for_message role&.to_sym
  end

  private

  def event_payload
    payload = {
      actor: inviter,
      invitation_id: self.id,
    }

    if email?
      payload[:email] = email
    else
      payload[:user] = invitee
      payload[:spammy] = invitee&.spammy?
    end

    payload
  end

  def invitation_must_be_allowed!
    Business::MemberInviteStatus.new(
      business, invitee, inviter: inviter, email: email,
      role: role, stafftools_invite: stafftools_invite).validate!
  end

  def email_must_be_exclusive
    if !invitee_id? && !email?
      errors.add(:base, "invitee or email must be present")
    elsif invitee_id? && email?
      errors.add(:email, "invitee has already been set")
    end
  end

  # Private: Generates the token used to lookup email invitations.
  def generate_token
    self.token = SecureRandom.hex(20)
  end

  # Private: When accepting an email invitation, accepts pending invitations
  # for the acceptor if it exists.
  def accept_duplicate_pending_invitations(acceptor:, role:, via_email:)
    return unless email?

    acceptor_emails = acceptor.emails.pluck(:email)

    pending_invites = business.invitations.pending.with_business_role(role)
      .with_invitee_or_normalized_email(invitee: acceptor, emails: acceptor_emails)

    # Don't include the current invitation that is being accepted.
    duplicate_invites = pending_invites - [self]

    duplicate_invites.each { |invite| invite.accept(acceptor: acceptor, via_email: via_email, auto_accept_duplicates: false) }
  end

  # Private: Add an invited member to the business, based on the role attribute.
  def add_invited_member_to_business(member)
    case role
    when "owner"
      business.add_owner(member, actor: inviter)
    when "billing_manager"
      business.billing.add_manager(member, actor: inviter)
    end
  end
end
