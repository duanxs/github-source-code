# frozen_string_literal: true
module SAML
  class Session < ApplicationRecord::Domain::Users
    self.table_name = "saml_sessions"

    belongs_to :user, inverse_of: :saml_session

    validates_uniqueness_of :user_id

    before_save    :log_change
    before_destroy :revoke_on_destroy

    # Public: Returns BatchEnumerator of sessions that are expired
    def self.expired
      SAML::Session.where("expires_at IS NOT NULL AND expires_at < ?", Time.now).in_batches(of: 1000)
    end

    private

    def log_change
      GitHub.auth.log(
        method: "#{self.class.name}.#{__method__}",
        at: "Updating SAML session",
        user_id: user.id,
        login: user.login,
        expires: expires_at || "No expiration provided",
      )
    end

    # Private: Revoke user's active sessions
    def revoke_on_destroy
      GitHub.auth.log(
        method: "#{self.class.name}.#{__method__}",
        at: "Revoking user sessions",
        user_id: user.id,
        login: user.login,
      )
      user.revoke_active_sessions(:saml_expired)
    end
  end
end
