# frozen_string_literal: true

class ApplicationCallbackUrl < ApplicationRecord::Collab

  # Public: The OauthApplication or Integration that owns this URL
  belongs_to :application, polymorphic: true

  validates :application_type, inclusion: { in: %w(OauthApplication Integration) }
  validates :url, presence: true, uniqueness: { scope: [:application_type, :application_id], case_sensitive: false }

  validates_with IntegrationUrlValidator
end
