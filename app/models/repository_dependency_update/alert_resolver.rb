# frozen_string_literal: true

# Acts as a factory class to produce correctly configured RepositoryDependencyUpdate
# objects given a RepositoryVulnerabilityAlert and trigger type.
class RepositoryDependencyUpdate::AlertResolver
  include GitHub::AreasOfResponsibility
  areas_of_responsibility :dependabot

  attr_reader :alert, :trigger

  FORCE_CREATE_TRIGGERS = %w(
    manual
  )

  MAX_RETRIES = 3

  def initialize(alert, trigger:)
    @alert = alert
    @trigger = trigger.to_s
  end

  def create_update
    return false if repository_inaccessible? || alert_blocked? || update_blocked?

    create_dependency_update
  end

  private

  def create_dependency_update
    alert.repository.dependency_updates.create!(
      repository_vulnerability_alert: alert,
      manifest_path: alert.vulnerable_manifest_path,
      package_name: alert.vulnerable_version_range.affects,
      reason: :vulnerability,
      trigger_type: trigger,
      dry_run: false,
    )
  end

  # If a repository is not accessible via the API, we shouldn't create a
  # RepositoryDependencyUpdate as Dependabot will not be able to resolve it.
  def repository_inaccessible?
    alert.repository.access.disabled? ||
      alert.repository.owner.has_any_trade_restrictions?
  end

  def alert_blocked?
    # We have a very small number of alerts that have a blank vulnerable_manifest_path
    # which breaks our validation rules.
    #
    # Alerts are deliberately created without validation in a long-running batch job
    # so we cannot guarantee to eliminate this problem by adding a validation rule
    # on the alert itself.
    #
    # Given the miniscule number of lifetime occurences we should just block the update
    # and track the event.
    if alert.vulnerable_manifest_path.blank?
      GitHub::Logger.log(
        at: "repository_dependency_update.blocked",
        alert_id: alert.id
      )
      GitHub.dogstats.increment("repository_dependency_update.blocked",
                                tags: [
                                  "trigger:#{trigger}",
                                  "reason:manifest_path_missing",
                                ])
      return true
    end

    repository_alerts_disabled? ||
      alert_not_resolvable? ||
          manifest_path_unsupported?
  end

  def repository_alerts_disabled?
    !alert.repository.vulnerability_alerts_enabled?
  end

  def alert_not_resolvable?
    alert.dismissed? || alert.vulnerable_version_range.fixed_in.blank?
  end

  def manifest_path_unsupported?
    !RepositoryDependencyUpdate.manifest_path_supported?(alert.vulnerable_manifest_path)
  end

  def update_blocked?
    return true unless dependabot_enabled?
    return false if overwrite_allowed?

    work_in_progress? || retries_exceeded?
  end

  # Dependabot is always considered enabled for manual requests as they are
  # explicitly requested by the user, otherwise we defer to Repository config
  def dependabot_enabled?
    return true if trigger == "manual"

    alert.repository.vulnerability_updates_enabled?
  end

  def overwrite_allowed?
    FORCE_CREATE_TRIGGERS.include? trigger
  end

  def work_in_progress?
    previous_alert_updates.where.not(state: :error).any?
  end

  def retries_exceeded?
    previous_alert_updates.
      where("created_at > ?", 30.days.ago).count >= MAX_RETRIES
  end

  def previous_alert_updates
    alert.repository_dependency_updates.
          visible
  end
end
