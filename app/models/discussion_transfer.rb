# frozen_string_literal: true

class DiscussionTransfer < ApplicationRecord::Domain::Discussions
  include Instrumentation::Model

  REASONS = [
    "Misfiled",
    "Sensitive",
    "Reorganization",
    "Other",
  ].freeze

  enum state: {
    started: 0,
    done: 1,
    errored: 2,
  }

  belongs_to :old_repository, class_name: "Repository", required: true
  belongs_to :old_discussion, class_name: "Discussion", required: true

  belongs_to :new_repository, class_name: "Repository", required: true
  belongs_to :new_discussion, class_name: "Discussion", required: true
  belongs_to :new_discussion_event, class_name: "DiscussionEvent"

  belongs_to :actor, class_name: "User", required: true

  before_validation :set_old_discussion_number, on: :create
  before_validation :set_old_repository, on: :create

  validates :old_discussion_number, presence: true
  validates :reason, inclusion: { in: REASONS, allow_nil: true }
  validate :old_discussion_open
  validate :actor_permissions
  validate :repos_not_archived
  validate :repos_have_discussions
  validate :transferrable_by_actor
  validate :with_same_owner
  validate :not_transferring_private_to_public

  scope :for_new_repository, ->(repo) { where(new_repository_id: repo) }
  scope :for_old_repository, ->(repo) { where(old_repository_id: repo) }
  scope :for_old_discussion, ->(discussion) { where(old_discussion_id: discussion) }
  scope :by_actor, ->(user) { where(actor_id: user) }

  def self.find_from(repository:, number:, attempts: 10)
    first_transfer = find_by(old_repository_id: repository.id, old_discussion_number: number)

    if first_transfer
      attempts.times.inject(first_transfer) do |transfer|
        # Is this the final transfer? Return it!
        return transfer if transfer.new_discussion

        # Can we find a new discussion transfer? If not, bail out
        return unless next_transfer = find_by(old_discussion_id: transfer.new_discussion_id)

        next_transfer
      end
    end

    nil
  end

  def instrument_transfer_event(staff_user = nil)
    audit_log_user = if staff_user&.staff?
      staff_user
    else
      actor
    end
    old_discussion.instrument(:transfer, actor: audit_log_user, discussion_transfer: self)
  end

  private

  def set_old_discussion_number
    return unless old_discussion
    self.old_discussion_number = old_discussion.number
  end

  def set_old_repository
    return unless old_discussion
    self.old_repository = old_discussion.repository
  end

  def old_discussion_open
    return unless old_discussion

    unless old_discussion.open?
      errors.add(:old_discussion, "cannot be locked, being transferred, or in an error state")
    end
  end

  def actor_permissions
    return unless actor && old_repository && new_repository

    acting_user = if actor.bot?
      actor.installation
    else
      actor
    end

    unless old_repository.resources.contents.writable_by?(acting_user)
      errors.add(:actor, "must have write permission on current repository")
    end

    unless new_repository.resources.contents.writable_by?(acting_user)
      errors.add(:actor, "must have write permission on new repository")
    end
  end

  def repos_not_archived
    if new_repository&.archived?
      errors.add(:new_repository, "must not be archived")
    end

    if old_repository&.archived?
      errors.add(:old_repository, "must not be archived")
    end
  end

  def repos_have_discussions
    return unless new_repository && old_repository

    unless new_repository.has_discussions?
      errors.add(:new_repository, "must have discussions enabled")
    end

    unless old_repository.has_discussions?
      errors.add(:old_repository, "must have discussions enabled")
    end
  end

  def transferrable_by_actor
    return unless old_discussion

    unless old_discussion.transferrable_by?(actor)
      errors.add(:old_discussion, "is not transferrable")
    end
  end

  def with_same_owner
    return unless new_repository && old_repository

    unless new_repository.owner_id == old_repository.owner_id
      errors.add(:new_repository, "must have the same owner as the current repository")
    end
  end

  def not_transferring_private_to_public
    if old_repository&.private? && new_repository&.public?
      errors.add(:old_discussion,
        "cannot be transferred from a private repository to a public repository")
    end
  end
end
