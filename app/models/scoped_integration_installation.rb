# frozen_string_literal: true

class ScopedIntegrationInstallation < ApplicationRecord::Collab
  PER_REPO_RATE_LIMIT = 1_000 # TODO: Make this configurable, per app, via Stafftools

  self.ignored_columns = %w(parent_id parent_type)

  areas_of_responsibility :ecosystem_apps

  include Ability::Actor
  include AuthenticationTokenable
  include Instrumentation::Model
  include IntegrationInstallable

  after_commit :instrument_creation, on: :create

  belongs_to :parent,
    class_name: "IntegrationInstallation",
    foreign_key: :integration_installation_id,
    optional: false

  has_one :integration, through: :parent

  has_one :version,
    class_name: "IntegrationVersion",
    through: :parent

  delegate :integrator_suspended?,
           :integrator_suspended_at,
           :single_file_name,
           :suspended?,
           :target_id,
           :target_type,
           :user_suspended?,
           :user_suspended_at,
           to: :parent

  delegate :single_file_name, to: :version

  validates_presence_of :integration_installation_id

  attribute :expires_at, :utc_timestamp

  # Public: The target for the parent.
  #
  # Returns a Promise.
  def async_target
    async_parent.then do |parent|
      parent.async_target
    end
  end

  # Public: The parent Integration's id.
  #
  # Returns an Integer.
  def integration_id
    integration.id
  end

  # Public: The target for the parent.
  #
  # Returns a User/Organization/Business.
  def target
    async_target.sync
  end

  # Returns a human readable string for use in audit logs.
  def name
    "scoped_integration_installation-#{id}"
  end

  def event_context(prefix: event_prefix)
    {
      prefix => name,
      "#{prefix}_id".to_sym => id,
    }
  end

  def event_payload
    {}.tap do |payload|
      payload[event_prefix]                     = self
      payload[:parent_integration_installation] = parent
      payload[:integration]                     = integration
      payload[:repository_selection]            = repository_selection
      if !installed_on_all_repositories?
        payload[:repository_ids]                = repository_ids
      end
      payload[target.event_prefix]              = target
      payload[:permissions]                     = permissions
    end
  end

  # Public: returns the API rate limit for this scoped installation. Considers
  # whether the App has the capability to have per-repo rate limits, otherwise
  # delegates to the parent installation.
  #
  # Returns an Integer.
  def rate_limit
    return @rate_limit if defined?(@rate_limit)

    @rate_limit = per_repo_rate_limit? ? PER_REPO_RATE_LIMIT : parent.rate_limit
  end

  def per_repo_rate_limit?
    Apps::Internal.capable?(:per_repo_rate_limit, app: self.integration) &&
      repository_ids.count == 1
  end

  # Public: extend the expires_at timestamp of this record and its associated
  # permissions to prevent it being cleaned up by pt-archiver.
  def extend_expires_at(timestamp)
    self.class.transaction do
      update!(expires_at: timestamp)

      ::Permissions::Service.update_expires_at_for_permissions(
        permission_ids: abilities.pluck(:id),
        timestamp: timestamp
      )

      instrument_extend_expires_at
    end
  end

  private

  def instrument_creation
    unless Apps::Internal.capable?(:skip_scoped_installation_audit_log, app: integration)
      instrument :create
    end
  end

  def instrument_extend_expires_at
    options = {}.tap do |opts|
      opts[:expires_at] = expires_at
      opts[:token_last_eight] = tokens.first.token_last_eight if tokens.any? # ScopedIntegrationInstallations should only ever have one token
    end

    instrument :extend_expires_at, options
  end
end
