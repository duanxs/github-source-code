# frozen_string_literal: true

class PendingVulnerableVersionRange < ApplicationRecord::Notify
  include GitHub::Relay::GlobalIdentification

  areas_of_responsibility :security_advisories

  belongs_to :pending_vulnerability

  validates :pending_vulnerability, presence: true
  validates :affects, presence: true
  validates :requirements, presence: true

  validates :ecosystem, inclusion: ::AdvisoryDB::Ecosystems.database_enum, allow_nil: true

  # These validations should only only run when a pending vuln is being marked as reviewed
  with_options if: :full_validation_required? do |range|

    range.validates :affects, format: { with: /\A\S+\z/ , message: "affects field can not contain whitespace" }

    range.validates :requirements, format: {
      with: VulnerableVersionRange::REQUIREMENTS_FORMAT,
      message: "not a valid requirements string",
    }
  end

  # parameters carried forward to a VulnerableVersionRange object when submitted
  SUBMIT_PARAMS = %w{
    affects
    ecosystem
    requirements
    fixed_in
  }

  def to_submit_params
    attributes.slice(*SUBMIT_PARAMS).tap do |params|
      params["ecosystem"] ||= pending_vulnerability.platform
    end
  end

  def full_validation_required?
    return false if pending_vulnerability.nil?
    self.pending_vulnerability.full_validation_required?
  end

end
