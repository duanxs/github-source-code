# frozen_string_literal: true

class ReviewRequest < ApplicationRecord::Domain::Repositories
  module AssociationExtension

    # Public: Returns reviewers with pending requests from the existing
    # collection.
    #
    # Excludes any reviewers whose requests are marked for deletion when the
    # parent is saved.
    #
    # Returns an Array of reviewers (i.e. Users and Teams).
    def pending_reviewers
      pending_requests.map(&:reviewer)
    end

    # Public: Helper method for setting pending requests by passing in
    # reviewers. Changes will be persisted when the parent is saved.
    #
    # Any existing pending reviewers, if not passed, will be marked for removal.
    #
    # Ignores any reviewers who are not allowed to be requested for review.
    #
    # Returns the new Array of pending reviewers.
    def pending_reviewers=(mixed_reviewers)
      to_add = []
      to_delete = pending_reviewers

      mixed_reviewers = proxy_association.owner.filter_allowed_reviewers(mixed_reviewers)
      by_type = mixed_reviewers.group_by { |r| r.class.name }

      by_type.each do |type, reviewers|
        existing = pending_reviewers.select { |pending| pending.class.name == type }

        to_add    += reviewers - existing
        to_delete -= reviewers & existing
      end

      defer = false
      if to_add.any?
        defer = proxy_association.owner.draft? &&
          proxy_association.owner.repository.pull_request_revisions_enabled?
      end

      to_add.each do |reviewer|
        build(reviewer: reviewer, deferred: defer)
      end

      to_delete.each do |reviewer|
        matching = pending_requests.select do |request|
          (request.reviewer == reviewer) && proxy_association.owner.review_request_removable?(request)
        end

        matching.each do |request|
          request.persisted? ? request.dismiss : delete(request)
        end
      end

      pending_reviewers
    end

    # Public: Adds a new reviewer and records the supplied reasons. If the
    # reviewer is already requested, a new request will not be made and the
    # reasons will not be recorded.
    #
    # reviewer  - The User or Team to request review from.
    # reasons   - (Optional) A Hash of reasons grouped by their type.
    #
    # Example
    #
    #   add_pending_reviewer_with_reasons(team, reasons: {
    #     codeowners: [{tree_oid: "0123456", path: "CODEOWNERS", line: 42, pattern: "*"}]
    #   })
    #
    # Returns the built request
    def add_pending_reviewer_with_reasons(reviewer, reasons: {})
      return true if pending_reviewers.include?(reviewer)

      allowed = proxy_association.owner.filter_allowed_reviewers([reviewer])
      return false unless allowed.include?(reviewer)

      build(reviewer: reviewer, reasons_by_type: reasons)
    end

    # Internal: Returns requests that are still pending that were marked as
    #           deferred when they were added.
    #
    # Returns an Array of ReviewRequests.
    def deferred
      pending.select(&:deferred?)
    end

    private

    def pending_requests
      select do |request|
        request.pending? && !request.marked_for_destruction?
      end
    end

  end
end
