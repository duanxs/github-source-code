# frozen_string_literal: true

module TradeControls
  class ManualCompliance
    include Compliance

    attr_reader :actor

    def initialize(actor:, reason: :manual, **)
      @actor = actor
      @reason = reason
    end

    def violation?
      true
    end

    def to_hydro
      {
        actor: @actor,
        reason: reason,
      }
    end

    # Internal: invoked by Instrumentation::Model when expanding event_payload
    def event_context(**)
      Context::Expander.expand(actor: @actor, reason: reason)
    end
  end
end
