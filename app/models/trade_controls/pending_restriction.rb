# frozen_string_literal: true

module TradeControls
  class PendingRestriction < ApplicationRecord::Collab
    include Instrumentation::Model

    self.table_name = "pending_trade_controls_restrictions"
    belongs_to     :target, class_name: "User", foreign_key: "target_id"
    before_save    :target_must_have_a_paid_plan!
    after_update   :instrument_enforcement_status_update, if: :saved_change_to_enforcement_status?
    after_update   :instrument_enforcement_on_update, if: :saved_change_to_enforcement_on?
    after_commit   :instrument_creation, on: :create

    enum reason: {
      organization_admin: "organization_admin",
      organization_billing_manager: "organization_billing_manager",
      organization_member: "organization_member",
      organization_outside_collaborator: "organization_outside_collaborator",
    }

    enum enforcement_status: {
      completed: "completed",
      cancelled: "cancelled",
      pending: "pending",
    }

    scope :pending, -> { where(enforcement_status: "pending") }
    scope :completed, -> { where(enforcement_status: "completed") }
    scope :cancelled, -> { where(enforcement_status: "cancelled") }
    scope :scheduled_for, ->(date) { where(enforcement_on: date.all_day, enforcement_status: "pending") }
    scope :past_scheduled_date, -> { where("enforcement_on < ? AND enforcement_status = 'pending'", GitHub::Billing.today.at_beginning_of_day) }

    validates :enforcement_on, :last_restriction_threshold, :reason, presence: true
    validates_uniqueness_of(
      :target_id,
      scope: %i[reason enforcement_status],
      message: "can't have more than 1 pending restriction with the same reason and status as 'pending'",
      if: proc { |restriction| restriction.pending? },
    )
    validate :target_must_have_a_paid_plan

    def organization
      @organization ||= Organization.find_by(id: target_id)
    end

    # Public: extend the enforcement date for this pending restriction by 14 days
    def extend
      updated = update(enforcement_on: enforcement_on + 14.days)
      if updated
        instrument :extend
      end

      updated
    end

    # Public: cancel the pending enforcement
    def cancel
      updated = update(enforcement_status: "cancelled")
      if updated
        instrument :cancel
        TradeControls::OrganizationComplianceCheckJob.perform_now(organization.id, reason: reason.to_sym)
      end

      updated
    end

    def past_scheduled_run_date?
      (GitHub::Billing.today > enforcement_on.to_date) && pending?
    end

    def run
      if organization && compliance

        unless organization.trade_controls_restriction.any?
          organization.trade_controls_restriction.enforce!(compliance: compliance)
          update(enforcement_status: :completed)
        end
      end
    end

    # Public: Used to get the trade compliance which will be used for the
    # actual enforcement
    #
    # Returns the relevant compliance for this pending restriction
    def compliance
      case reason
      when "organization_admin"
        OrgAdminThresholdCompliance.new(organization: organization)
      when "organization_billing_manager"
        BillingManagersCompliance.new(organization: organization)
      when "organization_member"
        OrganizationMembersCompliance.new(organization: organization)
      when "organization_outside_collaborator"
        OutsideCollaboratorsCompliance.new(organization: organization)
      end
    end

    # Public: Default event prefix for GitHub instrumentation. We're overriding
    #         it here because we don't want to use the default of
    #         "trade_controls/pending_restriction.
    #
    # Returns Symbol event prefix
    def event_prefix
      :pending_trade_controls_restriction
    end

    # Public: Default event payload
    def event_payload
      {
        org: organization,
      }
    end

    # Public: Instrument enforcement_status update.
    def instrument_enforcement_status_update
      GitHub.dogstats.increment("pending_trade_controls_restriction.enforcement_status_changed", tags: ["current_status:#{enforcement_status_in_database}"])
    end

    # Public: Instrument enforcement_on update.
    def instrument_enforcement_on_update
      GitHub.dogstats.increment("pending_trade_controls_restriction.enforcement_on_changed", tags: ["current_date:#{enforcement_on_in_database}"])
    end

    private

    def instrument_creation
      instrument :create
      InstrumentOrganizationTradeRestrictionEnforceJob.perform_later(
      organization: organization,
      enforcement_date: enforcement_on,
       pending: true,
       subscriber: "trade_restriction.organization_pending",
       **compliance.to_hydro
    )
    end

    # Private: Used to check if the target has a paid plan
    #
    # Returns true if the target has a paid plan
    def target_has_paid_plan?
      User.find_by(id: target_id)&.charged_account?
    end

    # Private: Used to enforce that the target of the pending enforcement
    # has a paid plan if someone on the console attempts
    # to force the issue with update_attribute.
    #
    # Raises an ArgumentError if this target don't have a paid plan
    def target_must_have_a_paid_plan!
      unless target_has_paid_plan?
        raise ArgumentError, "Target must have a paid plan"
      end
    end

    # Private: Custom validation method to prevent creating
    # pending restrictions for targets without a paid plan
    def target_must_have_a_paid_plan
      unless target_has_paid_plan?
        errors.add(:target_id, "Target must have a paid plan")
      end
    end
  end
end
