# frozen_string_literal: true

module TradeControls
    module OrgEmailCompliance
        include Compliance
        include DomainFlagging

        def full_restriction_violation?
            @organization.charged_account? && sanctioned_country.present?
        end

        def tier_1_restriction_violation?
        @organization.uncharged_account? && sanctioned_country.present?
        end

        def to_hydro
        {
            reason: reason,
            country: sanctioned_country.name,
            email: @email,
        }
        end

        # Internal: invoked by Instrumentation::Model when expanding event_payload
        def event_context(**)
        Context::Expander.expand(email: @email, reason: reason, country: sanctioned_country)
        end

        private

        def domain_field
        @email
        end

        def domain_field_type
        :email
        end
    end
end
