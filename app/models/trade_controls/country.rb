# frozen_string_literal: true

module TradeControls
  # A Value Object class that represents a country/region.
  #
  # In the future, it may become a proper Active Record model.
  class Country
    include Comparable

    attr_reader :name, :alpha2, :alpha3, :domain, :region_name, :region_code

    def self.from_braintree(braintree_country)
      name, alpha2, alpha3, * = braintree_country
      new(
        name: name,
        alpha2: alpha2,
        alpha3: alpha3,
        domain: alpha2&.downcase)
    end

    def self.from_domain(domain)
      new(domain: domain)
    end

    def self.from_ip(ip)
      from_location GitHub::Location.look_up(ip)
    end

    def self.from_location(location)
      # skipping name and alpha3 b/c we haven't matched countries by them before
      # name: location[:country_name],
      # alpha3: location[:country_code3],
      new(
        alpha2: location[:country_code],
        region: {
          name: location[:region_name],
          code: location[:region],
        })
    end

    def initialize(name: nil, alpha2: nil, alpha3: nil, domain: nil, region: {})
      @name = name
      @alpha2 = alpha2&.upcase # ISO 3166-1 alpha-2
      @alpha3 = alpha3&.upcase # ISO 3166-1 alpha-3
      @domain = domain&.downcase
      @region_name = region[:name]
      @region_code = region[:code]&.to_s&.upcase # ISO 3166-2
    end

    # ISO 3166-2
    def subdivision_code
      "#{alpha2}-#{region_code}"
    end

    def region?
      region_code.present? || region_name.present?
    end

    # As this may become an ActiveRecord model, the `update` method name was
    # leveraged as the semantics are similar: the result of calling `update`
    # is a Country instance with the same prior attributes set,
    # plus the given attributes set.
    #
    # As an AR model, it mutates the record in the db, of course. But for
    # a Value Object, it returns a new instance with the attributes merged.
    def update(**attributes)
      self.class.new(
        name: name,
        alpha2: alpha2,
        alpha3: alpha3,
        domain: domain,
        region: { name: region_name, code: region_code },
        **attributes)
    end

    # Compares for sortability using country code + region code
    def <=>(other)
      subdivision_code <=> other.subdivision_code
    end

    # Compares loose value equality using any of name/alpha2/alpha3/domain.
    # (Since all of these properties are unique to a country.)
    def ==(other)
      (name.present? && name == other.name) ||
        (alpha2.present? && alpha2 == other.alpha2) ||
        (alpha3.present? && alpha3 == other.alpha3) ||
        (domain.present? && domain == other.domain)
    end
    alias_method :same_country?, :==

    # Compares slightly stricter value equality by (in addition to ==),
    # requiring that region name or region code also match.
    def eql?(other)
      # the same_country? check here is likely redundant since
      # region name/code should be unique across all countries.
      same_country?(other) &&
        (region_code == other.region_code || region_name == other.region_name)
    end
    alias_method :same_region?, :eql?

    # Internal: invoked by Instrumentation::Model when expanding event_payload
    def event_context(prefix: :country)
      region = region? ? { region: region_name, region_code: region_code } : {}

      {
        prefix => name,
        :"#{prefix}_code" => alpha2,
        **region,
      }
    end
  end
end
