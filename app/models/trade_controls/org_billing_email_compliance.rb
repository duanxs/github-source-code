# frozen_string_literal: true

module TradeControls
  class OrgBillingEmailCompliance
    include OrgEmailCompliance

    def initialize(organization:, **)
      @organization = organization
      @reason = :organization_billing_email
      @email = organization.billing_email
    end
  end
end
