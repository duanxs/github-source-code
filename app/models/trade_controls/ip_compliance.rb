# frozen_string_literal: true

module TradeControls
  class IpCompliance
    include Compliance

    def initialize(ip:, **)
      @ip = ip
      @reason = :ip
    end

    def violation?
      sanctioned_country.present?
    end

    def to_hydro
      {
        reason: reason,
        country: sanctioned_country&.name,
        region: inferred_country&.region_name,
        ip: @ip,
      }
    end

    # Internal: invoked by Instrumentation::Model when expanding event_payload
    def event_context(**)
      Context::Expander.expand(ip: @ip, reason: reason, country: sanctioned_country)
    end

    private

    def inferred_country
      @inferred_country = Country.from_location(location)
    end

    def location
      @location ||= GitHub::Location.look_up(@ip)
    end

    def sanctioned_country
      defined?(@sanctioned_country) ? @sanctioned_country : @sanctioned_country =
        Countries::SANCTIONED.find { |c| c == inferred_country } ||
        Countries::SANCTIONED_REGIONS.find { |c| c.eql? inferred_country }
    end
  end
end
