# frozen_string_literal: true

module TradeControls
  module Notices
    # --- READ THIS BEFORE DOING ANY CHANGE IN THIS FILE ---
    # All the user facing messages in this file need to go through extra approval if they need to be changed.
    # If you need to change any of these, please follow these steps:
    # - Ping `@trade-controls-reviewers`.
    # - Make sure the change(s) are approved by legal and comms teams.
    #
    # If you have any questions please jump into `#pe-trade-compliance` on slack.

    # Delegates all methods to Notices, passing the message string through
    # a scrubber to replace <a> links with the inlined URL.
    module Plaintext
      module_function

      def method_missing(method, *args, &blk)
        TextHelper.strip_tags_inlining_urls Notices.__send__(method, *args, &blk)
      end

      def respond_to_missing?(*args)
        Notices.respond_to?(*args) || super
      end
    end

    APPEALS_URL = "https://airtable.com/shrGBcceazKIoz6pY"

    APPEALS_URL_GENERIC = "#{GitHub.help_url}/en/github/site-policy/github-and-trade-controls#how-is-github-ensuring-that-folks-not-living-in-andor-having-professional-links-to-the-sanctioned-countries-and-territories-still-have-access-or-ability-to-appeal"

    DUE_TO_LAW = "Due to U.S. trade controls law restrictions,"

    US_SANCTIONED_REGION = "It appears your account may be based in a U.S.-sanctioned region."

    US_SANCTIONED_REGION_NON_ADMIN = "It appears this account may be based in a U.S.-sanctioned region."

    PAID_SERVICES_SUSPENDED = <<~TEXT.chomp
      This means we have suspended access to private repository services and paid services for your account. \
      For free individual accounts, you still have access to free GitHub public repository services \
      (such as public repositories for open source projects and associated GitHub Pages and Gists).
    TEXT

    FILE_APPEAL = <<~HTML.chomp
      If you believe your account has been flagged in error, \
      and you are not located in or resident in a sanctioned region, \
      please <a href=#{APPEALS_URL}>file an appeal</a>.
    HTML

    ORG_FILE_APPEAL = <<~TEXT.chomp
      If you believe your organization’s account has been flagged in error, \
      and is not affiliated with a U.S.-sanctioned jurisdiction, please file \
      an appeal.
    TEXT

    ORG_FILE_APPEAL_GENERIC = <<~HTML.chomp
      If your account has been flagged in error, \
      and you are not located in or resident in a sanctioned region, \
      please <a href=#{APPEALS_URL_GENERIC}>file an appeal</a>.
    HTML

    MORE_INFO = <<~HTML.chomp
      Please read about <a href=#{GitHub.trade_controls_help_url}>\
      GitHub and Trade Controls</a> for more information.
    HTML

    NON_ADMIN_MORE_INFO = <<~HTML.chomp
      Please contact the organization admin and read about \
      <a href=#{GitHub.trade_controls_help_url}>\
      GitHub and Trade Controls</a> for more information.
    HTML

    PAID_ORG_SERVICES_BASE = <<~TEXT.chomp
      For free organization accounts, you may have access to free GitHub public repository \
      services (such as access to GitHub Pages and public repositories used for open source projects) \
      for personal communications only, and not for commercial purposes.
    TEXT

    PAID_ORG_SERVICES_SUSPENDED = <<~TEXT.chomp
      #{PAID_ORG_SERVICES_BASE} The restriction also includes \
      suspended access to private repository services and paid services (such as availability of \
      private organizational accounts and GitHub Marketplace services).
    TEXT

    ARCHIVED = <<~TEXT.chomp
      This repository has been archived with read-only access.
    TEXT

    UNABLE_TO_PROVIDE_ACCESS = "We are unable to provide access to GitHub private repository services."

    # Templates
    GENERIC_RESTRICTION_TEMPLATE = <<~HTML.chomp
      #{US_SANCTIONED_REGION} As a result, due to U.S. trade controls law restrictions, we are unable to \
      provide private repository services and paid services for your account. \
      GitHub has preserved, however, your access to <a href=#{GitHub.trade_controls_services_available_url}>certain free services for public repositories</a>. \

      #{ORG_FILE_APPEAL_GENERIC} #{MORE_INFO}
    HTML

    PAID_ORG_ENFORCEMENT_MAIL_TEMPLATE = <<~HTML.chomp
      #{US_SANCTIONED_REGION} As a result, due to U.S. trade controls law restrictions, \
      we are unable to provide private repository services and paid services for your account. \

      The restriction suspends access to private repository services and paid services, \
      such as availability of free or paid private repositories, secret gists, paid Action minutes, \
      Sponsors, and GitHub Marketplace services. For paid organizational accounts associated with sanctioned regions, \
      users may have limited access to their public repositories, which have been downgraded to archived read-only repositories.

      #{ORG_FILE_APPEAL_GENERIC} #{MORE_INFO}
    HTML

    GENERIC_FREE_ORG_TEMPLATE = <<~HTML.chomp
      #{US_SANCTIONED_REGION} As a result, due to U.S. trade controls law restrictions, we are unable to \
      provide private repository services and paid services for your account. \
      GitHub has preserved, however, your access to <a href=#{GitHub.trade_controls_services_available_url}>certain free services for public repositories</a>. \

      #{ORG_FILE_APPEAL_GENERIC} #{MORE_INFO}
    HTML

    GENERIC_BILLING_TEMPLATE = <<~HTML.chomp
      #{US_SANCTIONED_REGION} As a result, due to U.S. trade controls law restrictions, we are unable to \
      provide private repository services and paid services for your account. \
      GitHub has preserved, however, your access to <a href=#{GitHub.trade_controls_services_available_url}>certain free services for public repositories</a>. \

      #{ORG_FILE_APPEAL_GENERIC} #{MORE_INFO}
    HTML

    RESTRICTION_TEMPLATE_FOR_NON_ADMINS = <<~HTML.chomp
      #{US_SANCTIONED_REGION_NON_ADMIN} As a result, due to U.S. trade controls law restrictions, we are unable to \
      provide private repository services and paid services. \
      GitHub has preserved, however, access to <a href=#{GitHub.trade_controls_services_available_url}>certain free services for public repositories</a>.
    HTML

    ARCHIVED_REPO_TEMPLATE_FOR_ADMINS = <<~HTML.chomp
      #{US_SANCTIONED_REGION} As a result, due to U.S. trade controls law restrictions, we are unable to \
      provide private repository services and paid services for your account. \
      GitHub has preserved, however, your access to <a href=#{GitHub.trade_controls_services_available_url}>certain free services for public repositories</a>.
    HTML

    FREE_PRIVATE_REPO_TEMPLATE = <<~HTML.chomp
      #{UNABLE_TO_PROVIDE_ACCESS}
      #{US_SANCTIONED_REGION} As a result, due to U.S. trade controls law restrictions, we are unable to \
      provide private repository services and paid services for your account. \
      GitHub has preserved, however, your access to <a href=#{GitHub.trade_controls_services_available_url}>certain free services for public repositories</a>. \

      #{ORG_FILE_APPEAL_GENERIC} #{MORE_INFO}
    HTML

    REPO_DISABLED_FOR_NON_ADMINS_TEMPLATE = <<~HTML.chomp
      #{RESTRICTION_TEMPLATE_FOR_NON_ADMINS}

      #{NON_ADMIN_MORE_INFO}
    HTML

    REPO_ARCHIVED_FOR_NON_ADMINS_GENERIC_TEMPLATE = <<~HTML.chomp
      #{ARCHIVED}

      #{RESTRICTION_TEMPLATE_FOR_NON_ADMINS}
    HTML

    REPO_ARCHIVED_FOR_NON_ADMINS_TEMPLATE = <<~HTML.chomp
      #{ARCHIVED} #{DUE_TO_LAW} paid GitHub organization services have been restricted.
      #{PAID_ORG_SERVICES_BASE}
    HTML

    REPO_ARCHIVED_FOR_ADMINS_TEMPLATE = <<~HTML.chomp
      #{ARCHIVED} #{DUE_TO_LAW} paid GitHub organization and private repo services have been restricted.
      #{PAID_ORG_SERVICES_BASE}
    HTML

    REPO_ARCHIVED_FOR_ADMINS_GENERIC_TEMPLATE = <<~HTML.chomp
      #{ARCHIVED}

      #{ARCHIVED_REPO_TEMPLATE_FOR_ADMINS}
    HTML

    ORGANIZATION_ACCOUNT_RESTRICTED = GitHub::HTMLSafeString.make(GENERIC_FREE_ORG_TEMPLATE)
    BILLING_ACCOUNT_RESTRICTED = GitHub::HTMLSafeString.make(GENERIC_BILLING_TEMPLATE)
    FREE_PRIVATE_REPO_WARNING = GitHub::HTMLSafeString.make(FREE_PRIVATE_REPO_TEMPLATE)
    PAID_ORG_ENFORCEMENT_MAIL = GitHub::HTMLSafeString.make(PAID_ORG_ENFORCEMENT_MAIL_TEMPLATE)
    ACCOUNT_RESTRICTED_GENERIC = GitHub::HTMLSafeString.make(GENERIC_RESTRICTION_TEMPLATE)
    ACCOUNT_RESTRICTED_GENERIC_NON_ADMIN = GitHub::HTMLSafeString.make(RESTRICTION_TEMPLATE_FOR_NON_ADMINS)
    REPO_DISABLED_FOR_NON_ADMINS = GitHub::HTMLSafeString.make(REPO_DISABLED_FOR_NON_ADMINS_TEMPLATE)
    REPO_ARCHIVED_FOR_NON_ADMINS_GENERIC = GitHub::HTMLSafeString.make(REPO_ARCHIVED_FOR_NON_ADMINS_GENERIC_TEMPLATE)
    REPO_ARCHIVED_FOR_NON_ADMINS = GitHub::HTMLSafeString.make(REPO_ARCHIVED_FOR_NON_ADMINS_TEMPLATE)
    REPO_ARCHIVED_FOR_ADMINS = GitHub::HTMLSafeString.make(REPO_ARCHIVED_FOR_ADMINS_TEMPLATE)
    REPO_ARCHIVED_FOR_ADMINS_GENERIC = GitHub::HTMLSafeString.make(REPO_ARCHIVED_FOR_ADMINS_GENERIC_TEMPLATE)

    module_function

    def free_organization_account_restricted
      ORGANIZATION_ACCOUNT_RESTRICTED
    end

    def free_private_repo_warning
      FREE_PRIVATE_REPO_WARNING
    end

    def billing_account_restricted
      BILLING_ACCOUNT_RESTRICTED
    end

    def secret_gist_restricted
      ACCOUNT_RESTRICTED_GENERIC
    end

    # Strict newline formatting is required for the API
    def organization_account_restricted
      <<~HTML
        #{DUE_TO_LAW} paid GitHub organization services have been restricted.

        #{PAID_ORG_SERVICES_SUSPENDED}

        #{MORE_INFO}
      HTML
    end

    def organization_account_enforcement_mail
      PAID_ORG_ENFORCEMENT_MAIL
    end

    def organization_owned_repo_disabled
      <<~HTML
        #{DUE_TO_LAW} this repository has been disabled.

        #{PAID_ORG_SERVICES_SUSPENDED}

        #{MORE_INFO}
      HTML
    end

    def organization_owned_repo_disabled_for_non_admins
      <<~HTML
        #{DUE_TO_LAW} this repository has been disabled.

        #{NON_ADMIN_MORE_INFO}
      HTML
    end

    def organization_owned_repo_disabled_generic
      ACCOUNT_RESTRICTED_GENERIC
    end

    def organization_owned_repo_disabled_for_non_admins_generic
      REPO_DISABLED_FOR_NON_ADMINS
    end

    def organization_owned_repo_archived
      REPO_ARCHIVED_FOR_ADMINS
    end

    def organization_owned_repo_archived_for_non_admins
      REPO_ARCHIVED_FOR_NON_ADMINS
    end

    def organization_owned_repo_archived_for_non_admins_generic
      REPO_ARCHIVED_FOR_NON_ADMINS_GENERIC
    end

    def organization_owned_repo_archived_generic
      REPO_ARCHIVED_FOR_ADMINS_GENERIC
    end

    def archived_cta_for_non_admins
      <<~HTML
        #{NON_ADMIN_MORE_INFO}
      HTML
    end

    def archived_cta_for_admins
      <<~HTML
        #{ORG_FILE_APPEAL_GENERIC} #{MORE_INFO}
      HTML
    end

    # Strict newline formatting is required for the API
    def api_access_restricted
      <<~TEXT
        #{DUE_TO_LAW} we are unable to provide this access to this API. \
        #{MORE_INFO}
      TEXT
    end

    def api_org_invite_restricted_user
      <<~TEXT
        #{DUE_TO_LAW} we are unable to provide this feature for the invited user. \
        #{MORE_INFO}
      TEXT
    end

    def billing_email_restriction_reason
      "Billing email TLD is sanctioned"
    end

    def profile_email_restriction_reason
      "Public profile email TLD is sanctioned"
    end

    def website_url_restriction_reason
      "Public profile website TLD is sanctioned"
    end

    def percentage_restriction_reason(percent:, type:)
      "#{percent}% of #{type} are trade restricted"
    end

    # Strict newline formatting is required for the mailer.
    def user_account_restricted
      <<~HTML
        #{DUE_TO_LAW} your GitHub account has been restricted.

        #{PAID_SERVICES_SUSPENDED}

        #{FILE_APPEAL}

        #{MORE_INFO}
      HTML
    end

    def user_account_restricted_generic
      ACCOUNT_RESTRICTED_GENERIC
    end

    def org_invite_restricted
      <<~TEXT
        #{DUE_TO_LAW} we are unable to provide this feature.

        #{MORE_INFO}
      TEXT
    end

    def org_restricted
      <<~TEXT
        #{DUE_TO_LAW} this GitHub organization has been restricted.

        #{NON_ADMIN_MORE_INFO}
      TEXT
    end

    def org_restricted_repo_for_admins
      <<~HTML
        #{DUE_TO_LAW} paid GitHub organization services have been restricted.

        #{PAID_ORG_SERVICES_SUSPENDED} \
        #{MORE_INFO}
      HTML
    end

    def org_restricted_repo_for_collaborators
      <<~HTML
        #{DUE_TO_LAW} this GitHub organization has been restricted. \
        #{MORE_INFO}
      HTML
    end

    def repo_disabled
      <<~HTML
        #{DUE_TO_LAW} this repository has been disabled.

        #{PAID_SERVICES_SUSPENDED}

        #{FILE_APPEAL}

        #{MORE_INFO}
      HTML
    end

    def repo_disabled_generic
      ACCOUNT_RESTRICTED_GENERIC
    end

    def generic_prevent_toggle_to_private(type:)
      "Due to restrictions on this #{type}, you cannot change the visibility."
    end

    def restricted_public_abilities_for(type:)
      "You may only use this #{type} for personal communications, and not for commercial purposes."
    end
  end
end
