# frozen_string_literal: true

module TradeControls
  # Provides shared behavior for flagging blacklisted domains
  module DomainFlagging
    def violation?
      if GitHub.flipper[:org_compliance_refactor].enabled?(@organization)
        full_restriction_violation? || tier_1_restriction_violation?
      else
        sanctioned_country.present?
      end
    end

    private

    def domain_field
      throw "Missing implementation of 'domain_field'"
    end

    def domain_field_type
      throw "Missing implementation of 'domain_field_type'"
    end

    def tld
      uri = URI.parse(domain_field)
      uri = uri.scheme.nil? ? "http://#{domain_field}" : domain_field
      Addressable::URI.parse(uri).tld
    rescue Addressable::URI::InvalidURIError
      nil
    end

    def email_domain
      domain_field.split(".").last&.strip.to_s
    end

    def domain
      @domain ||=
      case domain_field_type
      when :email
        email_domain
      when :website_url
        tld
      else
        nil
      end
    end

    def inferred_country
      @inferred_country ||= Country.from_braintree(Braintree::Address::CountryNames.find { |c| c[1] == domain&.upcase })
    end

    def sanctioned_country
      @sanctioned_country ||= Countries::SANCTIONED.find { |c| c == inferred_country }
    end
  end
end
