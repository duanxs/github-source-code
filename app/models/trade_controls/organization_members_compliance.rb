# frozen_string_literal: true

module TradeControls
  class OrganizationMembersCompliance
    BATCH_SIZE = 1000

    include Compliance

    def initialize(organization:, **)
      @organization = organization
      @member_ids = organization.member_ids
      @reason = :organization_member
    end

    # returns the number of direct members of the organization
    def number_of_members
      @number_of_members ||= @member_ids.size
    end

    def count_restricted_members
      TradeControls::Restriction.count_any_restricted_ids(@member_ids)
    end

    def number_of_trade_restricted_members
      @number_of_trade_restricted_members ||= count_restricted_members
    end

    def current_threshold
      return 0 if (number_of_members.zero? || number_of_trade_restricted_members.zero?)

      @current_threshold ||= ((number_of_trade_restricted_members.to_f / number_of_members) * 100).round(2)
    end

    def violation?
      if GitHub.flipper[:org_compliance_refactor].enabled?(@organization)
        return false if @organization.has_any_pending_trade_restriction?
        return false unless GitHub.flipper[:org_member_flagging].enabled?(@organization)
        full_restriction_violation? || tier_1_restriction_violation?
      else
        if GitHub.flipper[:org_member_flagging].enabled?(@organization)
          if @organization.charged_account?
            current_threshold >= 50
          else
            current_threshold >= 25
          end
        else
          false
        end
      end
    end

    def full_restriction_violation?
      @organization.charged_account? && current_threshold >= 50
    end

    def tier_1_restriction_violation?
      return false unless GitHub.flipper[:org_member_flagging].enabled?(@organization)
      @organization.uncharged_account? && current_threshold >= 25
    end

    def minor_threshold_violation?
      if GitHub.flipper[:org_member_flagging].enabled?(@organization)
        @organization.charged_account? && current_threshold.between?(25.00, 49.99)
      else
        false
      end
    end

    def to_hydro
      {
        reason: reason,
        percentage_of_trade_restricted_members: current_threshold,
      }
    end

    # Internal: invoked by Instrumentation::Model when expanding event_payload
    def event_context(**)
      Context::Expander.expand(percentage_of_trade_restricted_members: current_threshold, reason: reason)
    end
  end
end
