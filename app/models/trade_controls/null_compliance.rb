# frozen_string_literal: true

module TradeControls
  class NullCompliance
    include Compliance

    def initialize(*)
      @reason = ""
    end
  end
end
