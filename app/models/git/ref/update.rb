# rubocop:disable Style/FrozenStringLiteralComment

module Git
  class Ref
    class Update
      attr_reader :repository, :refname, :before_oid, :after_oid
      attr_accessor :fast_forward

      # Public: Representation of a single ref update
      #
      # repository   - Repository the ref update is related to
      # refname      - String fully qualified ref name
      # before_oid   - String OID of the current state of the ref
      # after_oid    - String OID of the attempted state to write
      # fast_forward - Boolean to indicate whether the ref update is a fast-forward update,
      #                or `nil` to indicate fast-forwardness was not checked yet.
      def initialize(repository:, refname:, before_oid:, after_oid:, fast_forward: nil)
        @repository = repository
        @refname = refname
        @before_oid = before_oid
        @after_oid = after_oid
        @fast_forward = fast_forward

        validate_attributes
      end

      def before_commit
        load_commits
        @before_commit
      end

      def after_commit
        load_commits
        @after_commit
      end

      alias_method :eql?, :==

      def ==(object)
        object.class == self.class && object.state == state
      end

      def hash
        state.hash
      end

      def to_a
        [refname, before_oid, after_oid]
      end

      def paths
        @paths ||= diff.deltas.flat_map(&:paths).uniq
      end

      def diff
        @diff ||= GitHub::Diff.new(repository, before_oid, after_oid)
      end

      def deletion?
        after_oid == GitHub::NULL_OID
      end

      def creation?
        before_oid == GitHub::NULL_OID
      end

      def changed?
        before_oid != after_oid
      end

      protected

      def validate_attributes
        unless refname.is_a?(String)
          raise TypeError, "expected refname to be a String, but was #{refname.class}"
        end

        GitRPC::Util.ensure_valid_full_sha1(before_oid)
        GitRPC::Util.ensure_valid_full_sha1(after_oid)
      end

      def load_commits
        return if @commits_loaded

        commit_oids = [before_oid, after_oid].reject { |oid| oid == GitHub::NULL_OID }
        commits = repository.commits.find(commit_oids)

        if (index = commit_oids.index(before_oid)).present?
          @before_commit = commits[index]
        end

        if (index = commit_oids.index(after_oid)).present?
          @after_commit = commits[index]
        end

        @commits_loaded = true
      end

      def state
        [repository, refname, before_oid, after_oid]
      end
    end
  end
end
