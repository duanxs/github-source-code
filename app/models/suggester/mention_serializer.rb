# frozen_string_literal: true

module Suggester
  # A composite serializer that converts User and Team objects into the
  # JSON format expected by the mention suggester protocol. The client-side
  # suggestion menu renders these JSON objects as menu items.
  #
  # Examples
  #
  #   format = Suggester::MentionSerializer.new(viewer: user)
  #
  #   format.dump(users, teams)
  #   # => [{id, login, …}]
  class MentionSerializer
    def initialize(viewer:)
      @viewer = viewer
    end

    def dump(users, teams)
      to_hash = UserSerializer.new(viewer: @viewer)
      mentions = users.map(&to_hash)

      to_hash = TeamSerializer.new
      mentions + teams.map(&to_hash)
    end
  end
end
