# frozen_string_literal: true

module Suggester
  class OrganizationMemberSuggester
    def initialize(viewer:, org:)
      @viewer = viewer
      @org = org
    end

    def mentions
      format = Suggester::MentionSerializer.new(viewer: @viewer)
      format.dump(users, teams)
    end

    private

    def users
      list = []
      list.concat(@org.members)
      list.concat(@viewer.following)
      list.concat(@viewer.organizations)
      list.concat(@viewer.billing_manager_organizations)

      filter = BlockedUserFilter.new(viewer: @viewer)
      list = list.reject(&filter)

      GitHub::PrefillAssociations.for_profiles(list)
      list
    end

    def teams
      @org.visible_teams_for(@viewer)
    end
  end
end
