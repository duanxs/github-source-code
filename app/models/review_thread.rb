# frozen_string_literal: true

# This is a placeholder for right now.
#
# The hope is that we can refactor away LineCommentThread, CommitCommentThread,
# and DeprecatedPullRequestReviewThread to be a single ReviewThread class, which
# will play nicely with ReviewThreads and ReviewThread::Key (those two objects
# really do exist).
#
# Come ask in @github/workflow-team-cactus if you have questions! 🌵
class ReviewThread
end
