# rubocop:disable Style/FrozenStringLiteralComment

# RepositoryNetwork - Models a network of repositories.
#
# All repositories belong to a "network". The network is the set of repositories
# created by forking a repository within the network. Repositories in the same
# network share a network_id, fs host and disk location.
#
# The RepositoryNetwork model is used to store information that's applicable to
# the network as a whole, such as the fs host and partition location of git
# repositories, the root repository, push and access times, etc.
#
# == Examples
#
# The most common way of accessing a RepositoryNetwork is via the
# Repository#network association:
#
#   repository = Repository.with_name_with_owner("defunkt/dotjs")
#   repository.network
#
# The network can be used to determine the root repository as well as all other
# repositories in the network.
#
# == Backfill status
#
# The repository_networks table was added way after the concept of repository
# networks existed on the site and is not yet used in all of the places that
# you'd expect. Network records have been backfilled for all existing networks
# but we're still moving various bits of logic to this model.
#
# The table includes a number of columns that are not yet being maintained. This
# includes the owner_id, repository_count, backup_host, accessed_at,
# disabled_at, disabled_by, and disabling_reason columns. These attributes will
# always be nil and should not be used.
#
# See the initial pull request for more information on the schema and plans for
# columns that are not yet being maintained:
#
# https://github.com/github/github/pull/12418
#
class RepositoryNetwork < ApplicationRecord::Domain::Repositories
  # fix clash with Storage module
  require_dependency "repository_network/storage"

  include RepositoryNetwork::Backfill
  include RepositoryNetwork::Storage
  include RepositoryNetwork::Maintenance

  include GitHub::FlipperActor
  include Instrumentation::Model

  self.ignored_columns = %w(partition backup_host host)

  # The root repository for this network. All other repositories with the same
  # network id should descend from this object.
  belongs_to  :root, class_name: "Repository"

  belongs_to :parent,
    foreign_key: "owner_id",
    class_name: "RepositoryNetwork"

  has_many :children,
    foreign_key: "owner_id",
    class_name: "RepositoryNetwork"

  # All repositories in the network.
  has_many :repositories,
    -> { where(active: true) },
    foreign_key: "source_id"

  # All repositories in the network that are marked as deleted.
  has_many :deleted_repositories,
    -> { where(active: nil) },
    class_name: "Repository",
    foreign_key: "source_id"

  # All repositories in the network except the root.
  scope :forks, -> (network) { network.repositories.where.not(parent_id: nil) }

  # The root repository of each network in the network family
  scope :family_root_repositories,
    -> (network) { Repository.active.network_roots.where(source_id: network.family_ids) }

  scope :family_networks,
    -> (network) { RepositoryNetwork.default_scoped.where(id: network.family_ids) }

  # Set default attribute values when creating new network records
  before_create :set_default_values
  after_create  :set_storage_attributes

  after_commit :owner_disable_check,
               on: :create

  after_commit :initialize_placeholder_network_replicas,
               if: :needs_dgit_initialization_after_commit?,
               on: :create

  after_commit :delete_media_blobs,
               on: :destroy

  after_commit :delete_replicas_after_commit_on_destroy,
               on: :destroy

  def delete_replicas_after_commit_on_destroy
    GitHub::DGit::Maintenance.delete_network_replicas(self.id)
  end

  before_validation :set_initial_maintenance_status, on: :create

  # Alternative for setting the primary key id value. Modeled as a separate
  # attribute to work around AR limitations setting the id attribute at
  # initialization time.
  def network_id=(value)
    self.id = value
  end

  def network_id
    id
  end

  def set_initial_maintenance_status
    self.maintenance_status  ||= "complete"
    self.last_maintenance_at ||= Time.now
    self.last_maintenance_attempted_at ||= Time.now
  end

  # Increment the pushed_count and pushed_count_since_maintenance columns by 1.
  # This increments the values within a single SQL query which handles multiple
  # concurrent increments properly. Using an AR load, app increment, and save is
  # prone to concurrent processes stomping on each other.
  # Note: The newly incremented values are not written into this instances
  # attributes as they're typically not used at increment time.
  def increment_pushed_counts
    self.class.where(id: id).update_all(
      "pushed_count = pushed_count + 1,
       pushed_count_since_maintenance = pushed_count_since_maintenance + 1",
    )
  end

  # The name of the repository network, taken from the name_with_owner of the
  # network's root repository.
  def name
    root&.name_with_owner
  end

  # dgit_spec is the canonical description for a repository entity within
  # dgit/spokes.
  def dgit_spec
    "network/#{id}"
  end

  # Internal: Before create callback used to set default attributes values for a
  # record that's about to be created.
  def set_default_values
    self.pushed_count ||= 0
    self.pushed_count_since_maintenance ||= 0
  end

  # Whether placeholder network replicas need to be instantiated during a
  # the first after_commit callback.  Set from `set_storage_attributes`
  # during creation.  Unset upon `initialize_placeholder_network_replicas`.
  def needs_dgit_initialization_after_commit?
    @needs_dgit_initialization_after_commit
  end
  attr_writer :needs_dgit_initialization_after_commit

  # Internal: After create callback used to set whether we need to initialize
  # dgit data.
  def set_storage_attributes(insert_replicas = true)
    @needs_dgit_initialization_after_commit = insert_replicas
    save! if !new_record?
  end

  # Called from after_commit callbacks upon creation from one of either
  # the Repository or RepositoryNetwork classes.  Inserts new placeholder
  # network replica records for this network.
  def initialize_placeholder_network_replicas
    actor = root.owner unless root.nil?

    fileservers = GitHub::Spokes.client.pick_fileservers(actor: actor)
    GitHub::DGit::Maintenance::insert_placeholder_network_replicas(self.id, fileservers)

    @needs_dgit_initialization_after_commit = false
  end

  # Exception raised when extracting is attempted and we refuse to do so for
  # availability or other reasons
  class UnsafeExtractionError < StandardError
    def areas_of_responsibility
      [:repo_networks]
    end
  end

  # See if an extraction could work, given the amount of free space on the machine.
  #
  # Given that an extract currently disables shared storage for every repository to
  # extract, it will (temporarily) take up num_repos * disk_use disk.
  #
  # Returns Boolean
  def enough_space_to_extract?(repository, single_repo = false)
    available = calculate_free_space
    usage     = disk_usage(true)
    num_repos = single_repo ? 1 : repository.repository_and_descendants.size

    usage * num_repos < available - GitHub.shard_slack_space
  end

  # See if we can run maintenance without running the disk out of space.
  # Maintenance copies objects in from forks and rewrites the main pack,
  # each of which can temporarily double the size of the objects being
  # copied or rewritten.  In the worst case, all objects from all forks
  # will get copied to network.git and then repacked.  So a decent upper
  # bound on space needed for maintenance is basically the size of all
  # .pack and .idx files in all forks and network.git.
  #
  # Fortunately, we have a script that calculates that.  Return true if
  # the estimated size is less than available space minus slack space.
  #
  # Returns Boolean
  def enough_space_to_run_maintenance?
    available = calculate_free_space   # in KB
    needed = rpc.get_maint_size << 10  # convert MB to KB
    needed < available - GitHub.shard_slack_space
  end

  # See if this machine has enough space to take on the repository and its decendants
  # if they were to be extracted into this network.
  #
  # This assumes we're using the new copy-fork method of extraction and that the free
  # space needed in this network is less than or equal to the estimated disk usage of
  # the given repo's network plus a little slack space.
  def enough_space_to_receive?(repository)
    forks = repository.repository_and_descendants
    free_space = calculate_free_space
    slack = GitHub.shard_slack_space

    # Use the network's disk usage as an estimate, but fall back to the slower
    # calculation of actual disk usage of forks if the network's disk usage
    # is very large and bigger than the free space available
    (repository.network.disk_usage(true) + slack < free_space) ||
      (repository.network.disk_usage_of_forks(forks) + slack < free_space)
  end

  # Grabs repo state before a network operation (e.g. attach, detach, extract)
  # needed to determine which search indexes need updating after the operation.
  #
  # You'd be correct to notice returning both `network_root?` and `fork? is
  # superfluous. They should always have inverse values and you should only need
  # one. But one is a value stored with the RepositoryNetwork and the other is
  # stored with the Repository, and we have some known bad data where they don't
  # always align. Sadly this is the most resilient way to handle it until/unless
  # that's identified and thoroughly fixed.
  private def get_initial_state_for_search_reindex(repo)
    [repo.network_root?, repo.fork?, repo.network]
  end

  # Extract the given repository and all forks into a new or existing network.
  #
  # This method is fragile. If it fails, repositories may be left in a weird state.
  # It should be used sparingly and only run via RepositoryNetworkOperationsJob.
  #
  # repo - The repository to extract.
  #
  # new_network_id - The integer id of the network to transfer all repositories to.
  #                  This defaults to nil for a new network.
  #
  # reindex - Should this method perform search reindexing of the provided repo?
  #           Defaults to true, which you probably want if it's being called
  #           alone to extract into a new network.
  #           It should be false if it's being used to extract into an existing
  #           network and you're doing the reindex yourself after additional
  #           steps. (See e.g. RepositoryNetwork#reattach!)
  #
  # Returns nothing.
  def extract!(repo, new_network_id: nil, reindex: true)
    return if new_network_id.nil? && repositories.count == 1

    raise "Cannot extract into same network" if new_network_id == id

    # If the repository doesn't exist on disk it might indicate a race with some
    # other operation. We should use locking to prevent this, and we do in some
    # cases.
    raise UnsafeExtractionError, "missing storage" unless repo.exists_on_disk? || Rails.test?

    unless enough_space_to_extract?(repo)
      raise UnsafeExtractionError, "not enough disk space available to extract"
    end

    was_root, was_fork, old_network = get_initial_state_for_search_reindex(repo)
    old_parent = repo.parent

    network = if new_network_id.nil?
      children.create!(root: repo)
    else
      RepositoryNetwork.find(new_network_id).tap do |network|
        if self.visibility != network.visibility
          raise "Cannot extract into a network with different visiblity"
        end
      end
    end

    # On GHE there are some codepaths below that
    # seem to depend on this initialization.
    if GitHub.enterprise? && network && network.needs_dgit_initialization_after_commit?
      network.initialize_placeholder_network_replicas
    end

    former_organization = repo.organization # may be nil

    repos_to_extract = repo.repository_and_descendants

    move_repositories_into_network(repos_to_extract, network)

    repo_ids = repos_to_extract.map(&:id)
    transaction do
      Repository.where(id: repo_ids).update_all(source_id: network.id)
      new_parent = new_network_id.nil? ? nil : network.root_id
      repo.update_column :parent_id, new_parent

      repos_to_extract = Repository.find(repo_ids)
      repos_to_extract.each do |r|
        r.unlock_including_descendants! unless r.locked_on_billing?
        r.update_organization
      end
      network.update_permissions_on(repos_to_extract, former_organization)
    end

    # Note: .sync_routes_from_network MUST not run
    #       from within the above transaction.
    repo.reload
    repos_to_extract.each do |r|
      r.reload
      r.sync_routes_from_network
    end

    Media::Transition.async_copy(old_network, network)

    repos_to_extract.each &:enable_or_disable_shared_storage
    repo.owner.owned_private_repositories.reload
    repo.dgit_reload_routes!
    repo.touch
    old_parent.reload.calculate_network_counts! if old_parent

    repo.reindex_after_network_operation(was_root, was_fork, old_network) if reindex

    if Audit.context[:from]&.start_with?("stafftools")
      GitHub.instrument "staff.repo_extract", repo: repo, old_network_id: old_network&.id
    else
      GitHub.instrument "repo.extract", repo: repo, old_network_id: old_network&.id
    end
  end

  # Extract the given repository and all forks into a new or existing network.
  #
  # This method is fragile. If it fails, repositories may be left in a weird state.
  # It should be used sparingly and only run via RepositoryNetworkOperationsJob.
  #
  # repo - The repository to extract.
  #
  # Returns nothing.
  def new_extract!(repo)
    return if repositories.count == 1

    # If the repository doesn't exist on disk it might indicate a race with some
    # other operation. We should use locking to prevent this, and we do in some
    # cases.
    raise UnsafeExtractionError, "missing storage" unless repo.exists_on_disk? || Rails.test?

    was_root, was_fork, old_network = get_initial_state_for_search_reindex(repo)
    old_parent = repo.parent
    network = children.create!(root: repo)
    if !network.enough_space_to_receive?(repo)
      network.destroy
      raise UnsafeExtractionError, "not enough disk space available to extract"
    end

    former_organization = repo.organization # may be nil

    repos_to_extract = repo.repository_and_descendants

    begin
      Extraction.new(self, network, repos_to_extract).extract!
    rescue => f
      network.destroy
      raise f
    end

    repo_ids = repos_to_extract.map(&:id)
    transaction do
      Repository.where(id: repo_ids).update_all(source_id: network.id)
      repo.update_column :parent_id, nil

      repos_to_extract = Repository.find(repo_ids)
      repos_to_extract.each do |r|
        r.update_organization
      end
      network.update_permissions_on(repos_to_extract, former_organization)
    end

    Media::Transition.async_copy(old_network, network)

    repo.owner.owned_private_repositories.reload
    repo.touch
    old_parent.reload.calculate_network_counts! if old_parent

    repo.reindex_after_network_operation(was_root, was_fork, old_network)

    GitHub.instrument "staff.repo_extract", repo: repo, old_network_id: old_network&.id
  end

  # Detach this repository from the network, creating a new network with this
  # repository as the only member. The repository is moved to a new location
  # on disk (possibly over the network).
  #
  # All forks are reparented under this repository's parent when available.
  # When this repository is the root of a network, one of the forks is elected
  # as the new root and all other forks are reparented under it.
  def detach!(repo)
    return if repositories.count == 1

    # If the repository doesn't exist on disk it might indicate a race with some
    # other operation. We should use locking to prevent this, and we do in some
    # cases.
    raise UnsafeExtractionError, "missing storage" unless repo.exists_on_disk? || Rails.test?

    unless enough_space_to_extract?(repo, 1)
      raise UnsafeExtractionError, "not enough disk space available to detach"
    end

    was_root, was_fork, old_network = get_initial_state_for_search_reindex(repo)

    old_shard_path = repo.shard_path
    old_network = repo.network
    old_parent = repo.parent

    new_network = if repo.network_root?
      create_parent(root: repo, parent: parent)
    else
      children.create(root: repo)
    end
    move_repositories_into_network([repo], new_network)

    transaction do
      reparent_forks!(repo)

      repo.update_column :parent_id, nil
      repo.update_column :source_id, new_network.id
      repo.reload

      repo.update_organization
      save!
    end

    # Note: .sync_routes_from_network MUST not run
    #       from within the above transaction.
    repo.sync_routes_from_network
    repo.reload

    Media::Transition.async_copy(old_network, new_network)

    repo.owner.owned_private_repositories.reload
    repo.touch
    repo.calculate_network_counts!
    old_parent.reload.calculate_network_counts! if old_parent

    repo.reindex_after_network_operation(was_root, was_fork, old_network)

    if Audit.context[:from]&.start_with?("stafftools")
      GitHub.instrument "staff.repo_detach", repo: repo, old_network_id: old_network&.id
    else
      GitHub.instrument "repo.detach", repo: repo, old_network_id: old_network&.id
    end
  end

  # Reattach this network to its parent network.
  #
  # Returns nothing.
  def reattach!
    raise "No parent network to re-attach to" if parent.nil?
    raise "Cannot reattach to a network with different visibility" unless matches_parent_visibility?
    perform_attach(parent)
    GitHub.instrument "staff.repo_reattach", repo: root
  end

  # Reattach this network to a related network.
  #
  # The intent is for this to be called from a controller with `async: true` to check for errors
  # that can be communicated to the user then schedule a job. The job should then call this with
  # `async: false` to actually perform the attach. We default `async: false` to make behavior
  # consistent with similar methods here, e.g. `detach!` and `reattach!`.
  #
  # Raises if the repository cannot be attached. Returns nothing.
  def attach_to!(dest_network, async: false)
    allowed, reason = can_attach_to?(dest_network)
    raise "Cannot attach to that repository: #{can_attach_to_failure_reason_description(reason)}." unless allowed
    if async
      RepositoryNetworkOperationsJob.perform_later(:attach_to, self, dest_network)
    else
      perform_attach(dest_network)
      GitHub.instrument "staff.repo_attach_to", repo: root, destination_root: dest_network.root
    end
  end

  private def perform_attach(dest_network)
    was_root, was_fork, old_network = get_initial_state_for_search_reindex(root)
    extract!(root, new_network_id: dest_network.id, reindex: false)
    reparent_child_networks(dest_network.id)
    self.destroy
    root.reload.calculate_network_counts!
    root.reindex_after_network_operation(was_root, was_fork, old_network)
  end

  private def reparent_child_networks(new_parent_network_id)
    # This network is about to be destroyed. So we're going to take all of our
    # children and set their parent to our _new_ parent.
    child_network_ids = children.map(&:id)

    # If the network we're attaching to was previously our child, we can't set
    # it to be its own parent. So we exclude that possibility here.
    child_network_ids_to_update = child_network_ids - [new_parent_network_id]
    RepositoryNetwork.where(id: child_network_ids_to_update).update_all(owner_id: new_parent_network_id)

    # If the network we're attaching to was previously our child, it was
    # excluded above so we handle it here. Its new parent should be our _old_
    # parent.
    new_parent_child_id = child_network_ids & [new_parent_network_id]
    RepositoryNetwork.where(id: new_parent_child_id).update(owner_id: owner_id) if new_parent_child_id.any?
  end

  # Makes the given repository the root of the network. This makes the existing
  # root repository a fork of the given repository and moves the old root's forks
  # onto the new root. The plan owner for the network is also adjusted to
  # the new root repository's owner.
  #
  # Returns true if the operation was completed.
  def make_root!(repo)
    old_root = root
    if repo == root
      raise "This repository is already the root"
    end

    if repo.private? && root.private? && repo.owner.at_private_repo_limit?
      raise "New owner is at private repository limit"
    end

    transaction do
      update_root!(repo)

      # update memo caches
      root.reload_parent
    end

    GitHub.instrument "staff.repo_make_root", repo: repo, root_repo_was: old_root
    true
  end

  # Reparent all forks of the given repository under another repository. When the
  # repository has a parent, forks are reparented under that guy. When the
  # repository is the root of a network, one of the forks is elected as the
  # new root and all other forks are reparented under it.
  #
  # Returns the array of forks that were reparented or nil when this repository
  # had no forks to reparent.
  def reparent_forks!(parent_repo)
    forks = parent_repo.forks.sort_by(&:created_at).select { |repo| repo.active? }
    if forks.any?
      # try to find a parent that is marked as active
      repo = parent_repo
      while new_parent = repo.parent
        break if new_parent.active?
        repo = new_parent
      end

      if new_parent
        Repository.where(id: forks.map(&:id)).update_all(parent_id: new_parent.id)
        new_parent.update_organization
        repositories.reload.each do |repo|
          repo.update_organization
        end
      else
        elected = forks.find(&:public?) || forks.first
        forks -= [elected]
        update_root!(elected)
      end
      forks
    end
  end

  # Public - The visibility of all repositories in this network.
  #
  # returns 'public' or 'private'
  def visibility
    root.visibility
  end

  def matches_parent_visibility?
    parent && parent.visibility == self.visibility
  end

  # Internal - set a new root for the network. Updates the
  # parent_id of any fork that points at the current root
  # to point at the new root. Also updates the network
  # counts for both the old and new roots.
  #
  # new_root - The repository to become the new root.
  #
  # Returns nothing.
  def update_root!(new_root)
    old_root = root

    update_column :root_id, new_root.id
    new_root.update_column :parent_id, nil
    new_root.reload_parent

    roots = repositories.reload.where("parent_id IS NULL").to_a
    forks = repositories.where("parent_id IN (?)", roots).to_a
    forks += roots
    forks.delete new_root
    Repository.where(id: forks.map(&:id)).update_all(parent_id: new_root.id)
    new_root.update_organization
    repositories.reload.each do |repo|
      repo.update_organization
    end

    old_root.reload.calculate_network_counts!
    new_root.reload.calculate_network_counts!
  end

  def network_owner
    root.owner
  end
  alias_method :owner, :network_owner

  def shard_path
    fail "no shard_path for network without an id" unless id

    "#{storage_path}/network.git"
  end

  # Update permissions on the given repositories after
  # they have been extracted into a new network. Currently
  # only removes old collaborators who should no longer
  # have access.
  #
  # repos - the repositories in this network to be updated
  # former_organization - nil, or the repos' previous organization
  #                       if their permissions were managed via teams.
  #
  # Returns nothing.
  def update_permissions_on(repos, former_organization)
    repos.each do |repo|
      # never was involved in an org, only need to manage collabs, not teams
      if !former_organization && !repo.in_organization?
        repo.remove_member parent.root.owner if parent
      end
    end
  end

  def billing_unlock(log_as_part_of_job: nil)
    if root.locked_on_billing? && (root.public? || !root.owner.reload.at_private_repo_limit?)
      if log_as_part_of_job
        GitHub.dogstats.increment "repository_network", tags: ["action:billing_unlock", "type:#{log_as_part_of_job.underscore}"]
        GitHub::Logger.log \
          job: log_as_part_of_job,
          at: "repository_network.billing_unlock.unlock",
          network_id: self.id,
          repo_id: root.id
      end

      root.unlock_including_descendants!(log_as_part_of_job: log_as_part_of_job)
    elsif log_as_part_of_job
      GitHub.dogstats.increment "repository_network", tags: ["action:billing_unlock", "type:#{log_as_part_of_job.underscore}", "result:noop"]
      GitHub::Logger.log \
        job: log_as_part_of_job,
        at: "repository_network.billing_unlock.noop",
        network_id: self.id,
        repo_id: root.id,
        root_public?: root.public?,
        root_locked_on_billing?: root.locked_on_billing?,
        owner_at_private_repo_limit?: root.owner.at_private_repo_limit?
    end
  end

  def billing_lock
    return if root.public? || root.locked_on_billing?

    root.lock_for_billing if root.owner.at_private_repo_limit?
  end

  # Public: Finds a repository in the network owned by the specified owner, if any.
  #
  # owner    - The User/Organization to look up a repository for. If a String identifier is given (i.e. login)
  #            the User/Organization will be looked up.
  #
  # Returns a Respository.
  def find_fork_for(owner)
    owner = User.reify(owner)

    if owner == network_owner
      root
    elsif owner
      # Force use of index on `source_id`. Prevents fallback to primary index.
      # See https://github.com/github/github/issues/87754 for history.
      forced_index = "index_repositories_on_source_id_and_organization_id"
      repositories.from("`#{Repository.table_name}` USE INDEX (`#{forced_index}`)").
        find_by(owner_id: owner.id)
    end
  end

  def read_only?
    moving?
  end

  # Returns a `full_network_tree` for each network in the family.
  def family_network_trees
    self.class.family_networks(self).map do |related_network|
      repo_map = related_network.full_network_tree
      network_roots = repo_map[nil]
      raise InvalidNetworkError("Network #{related_network.id} has #{network_roots.count} roots") if network_roots.count != 1
      [network_roots.first, repo_map]
    end
  end

  def full_network_tree
    repositories.includes(:owner).group_by(&:parent_id)
  end

  def broken?
    (maintenance_status =~ /\Abroken\Z/i) ? true : false
  end

  # Public: Does the network's root belong to another network?
  #
  # A failed detach or extract can cause a network's root to point to
  # a different network.
  def orphaned?
    root.source_id != id
  end

  def increment_cache_version!
    ActiveRecord::Base.connected_to(role: :writing) do
      RepositoryNetwork.increment_counter(:cache_version_number, id)
      reload
    end
  end


  def event_context(prefix: :repository_network)
    {
      "#{prefix}_id".to_sym => id,
      prefix => name,
    }
  end

  def event_payload
    { repository_network: self }
  end

  def restore
    return if repositories.count > 1
    Media::Transition.async_restore(self)
  end


  # Public: Removes network forks owned by individuals who do not have access to root repository.
  def remove_invalid_user_forks
    RepositoryNetwork.forks(self).find_in_batches do |batch|
      inaccessible = repos_with_inaccessible_root(batch)
      inaccessible.each do |user_fork|
        user_fork.throttle { user_fork.remove(user_fork.owner) }
      end
    end
  end

  # Prevent infinite recursion in the event of circular references in the network tree
  MAX_TREE_HEIGHT = 1000
  private_constant :MAX_TREE_HEIGHT

  def root_network
    @root_network ||= begin
      current_network = self
      (0..MAX_TREE_HEIGHT).each do |height|
        break if current_network.parent.nil?
        current_network = current_network.parent
        raise "maximum network tree height exceeded: likely circular reference" if height >= MAX_TREE_HEIGHT
      end
      current_network
    end
  end

  # Prevent infinite recursion in the event of circular references in the network tree
  MAX_QUEUE_DEPTH = 10000
  private_constant :MAX_QUEUE_DEPTH

  # Returns the array of network IDs for all networks in the same "family."
  #
  # A network family is all networks descended from the same root. This method traverses the entire
  # network tree to retrieve all related networks. Note that this is not traversing repositories
  # within a network, it's traversing the networks themselves.
  def family_ids
    return @network_family_ids if defined?(@network_family_ids)

    tree_walker = lambda do |networks|
      networks.flat_map do |network|
        tree_walker.call(network.children).unshift(network.id)
      end
    end

    GitHub.dogstats.time "repository_network.family_ids.time", tags: ["visibility:#{visibility}"] do
      ids = tree_walker.call([root_network])
      ids.uniq!
      @network_family_ids = ids
    end
    GitHub.dogstats.gauge "repository_network.family_ids.size", @network_family_ids.count, tags: ["visibility:#{visibility}"]
    @network_family_ids
  end

  # Determines whether attaching the current network to the provided network is valid.
  # Returns a 2-element array:
  # - Boolean whether the attach is allowed
  # - Symbol reason when not allowed, :valid when allowed. Valid symbols:
  #   - :visibility if the networks have differing visibility
  #   - :same_network if the networks are the same
  #   - :unrelated if the networks are not in the same family
  #   - :dangling_fork if attaching the network would create new dangling forks: forks whose owner
  #     has no permissions on the fork parent.
  #   - :oopf if attaching the network would create new org-owned private forks outside the business
  #   - :valid when the network can be safely attached
  def can_attach_to?(attach_to_network)
    GitHub.dogstats.time "repository_network.can_attach_to.time", tags: ["visibility:#{visibility}"] do
      return [false, :visibility] unless visibilities_match_for_attach?(attach_to_network)
      return [false, :same_network] if id == attach_to_network.id
      return [false, :unrelated] if root_network.id != attach_to_network.root_network.id

      # No permission or ownership checks are necessary in public networks.
      return [true, :valid] if root.public?

      all_repos_valid = true
      reason = :valid
      all_repos_valid = repositories.all? do |repo|
        # No new OOPFs: all repos in the source network must be user-owned or part of the same
        # business as the network root.
        if GitHub.flipper[:disallow_oopfs].enabled? && repo.owner.organization? && !attach_to_network.root.same_business?(repo)
          reason = :oopf
          next false
        end
        # The source network will be flattened and re-parented under the destination network root.
        # So to prevent new dangling forks, owners of all repos in the source network must have read
        # access to the destination network's root.
        if repo.owner.user? && !attach_to_network.root.permit?(repo.owner, :read)
          reason = :dangling_fork
          next false
        end
        true
      end
      [all_repos_valid, reason]
    end
  end

  private def can_attach_to_failure_reason_description(reason)
    case reason
    when :visibility
      "the networks have different visibilities"
    when :same_network
      "the repositories are already in the same network"
    when :unrelated
      "the networks are unrelated"
    when :dangling_fork
      "a new dangling fork (whose owner has no access to the parent) would be created"
    when :oopf
      "a new organization-owned private fork would be created"
    else
      reason
    end
  end

  private def visibilities_match_for_attach?(attach_to_network)
      # If the new parent network isn't internal, visibility must match.
      return visibility == attach_to_network.visibility unless attach_to_network.visibility == "internal"

      # An internal network is a network whose root is internal and all forks are private. So if
      # we're joining an internal network, this network must be private.
      visibility == "private"
  end

  private def repos_with_inaccessible_root(batch)
    batch.reject do |fork|
      # Reject it, preventing removal, unless it's user-owned.
      # See https://github.com/github/github/issues/112160#issuecomment-497386928 and related issues.
      next true unless fork.owner.user?

      root.pullable_by_user_or_no_plan_owner?(fork.owner)
    end
  end

  protected

  def delete_media_blobs
    Media::Transition.async_delete(self)
  end

  def owner_disable_check
    if network_owner
      if parent
        # just do a single-repo lock if this network was detached/extracted from another
        billing_lock
      else
        network_owner.enable_or_disable! if root&.private?
      end
    end
  end
end
