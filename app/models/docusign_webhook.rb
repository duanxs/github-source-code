# frozen_string_literal: true

class DocusignWebhook < ApplicationRecord::Ballast
  store :payload, coder: JSON

  # Public: Whether or not the webhook has been processed already
  #
  # Returns Boolean
  def processed?
    processed_at.present?
  end

  # Public: Helper method to extract EnvelopeID from payload
  #
  # returns String
  def envelope_id
    payload.dig \
      "DocuSignEnvelopeInformation",
      "EnvelopeStatus",
      "EnvelopeID"
  end

  # Public: Helper method to extract Status from payload
  #
  # returns String
  def envelope_status
    payload.dig \
      "DocuSignEnvelopeInformation",
      "EnvelopeStatus",
      "Status"
  end

  # Public: Helper method to extract TimeGenerated from payload
  #
  # returns String
  def time_generated
    payload.dig \
      "DocuSignEnvelopeInformation",
      "EnvelopeStatus",
      "TimeGenerated"
  end
end
