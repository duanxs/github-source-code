# frozen_string_literal: true

class AdvisoryCredit < ApplicationRecord::Collab
  UnauthorizedActorError = Class.new(StandardError)

  include AdvisoryCredit::NewsiesAdapter
  include GitHub::RateLimitedCreation
  include Instrumentation::Model

  belongs_to :repository_advisory, primary_key: :ghsa_id, foreign_key: :ghsa_id,
    optional: true
  belongs_to :vulnerability, primary_key: :ghsa_id, foreign_key: :ghsa_id,
    optional: true
  belongs_to :recipient, class_name: "User"
  belongs_to :creator, class_name: "User", optional: true
  alias_method :user, :creator

  validates :ghsa_id, :recipient_id, :creator_id, presence: true

  validate :ensure_creator_is_not_blocked_by_recipient, on: :create

  before_create :auto_accept, if: :given_to_self?
  after_create_commit :instrument_create
  after_create_commit :instrument_accept, if: :accepted?
  after_create_commit :deliver_notifications
  after_destroy_commit :instrument_destroy

  attr_readonly :ghsa_id, :recipient_id, :creator_id

  scope :pending, -> { where(accepted_at: nil, declined_at: nil) }
  scope :accepted, -> { where.not(accepted_at: nil) }

  scope :on_public_repository_advisories, -> {
    repository_ids = joins(:repository_advisory).merge(RepositoryAdvisory.published).pluck(:repository_id)
    public_repository_ids = Repository.active.public_scope.with_ids(repository_ids).ids

    joins(:repository_advisory).where(repository_advisories: { repository_id: public_repository_ids })
  }

  def async_readable_by?(actor)
    # A credit is always readable by the credited user (even if they are spammy).
    # Whether the credit can be _accepted_ yet is a different question and depends
    # on the credited user's ability to read the credit's repository advisory
    return Promise.resolve(true) if actor.is_a?(User) && actor.id == recipient_id

    # If the creator or recipient is spammy, only those users with write access to the
    # advisory credit are allowed to see the credit.
    Promise.all([async_recipient, async_creator]).then do |recipient, creator|
      next async_writable_by?(actor) if recipient&.spammy? || creator&.spammy?

      # Whether or not the credit has been accepted, we'll need to know more about
      # its repository advisory, so first we load that if it exists.
      async_repository_advisory.then do |repository_advisory|
        if accepted?
          # If the credit is accepted, the viewer only needs to be able to read
          # either the associated repository advisory or security advisory
          # (vulnerability) in order to read the credit.
          repository_advisory&.async_readable_by?(actor).then do |repository_advisory_is_readable|
            # The repository_advisory_is_readable value is:
            # - true if a repository advisory exists and is readable
            # - false if a repository advisory exists but is not readable
            # - nil if no associated repository advisory exists
            next true if repository_advisory_is_readable

            # If there is no repository advisory or there is and it isn't
            # readable by the viewer, we check the vulnerability. The
            # vulnerability must exist and be readable by the viewer.
            async_vulnerability.then do |vulnerability|
              vulnerability&.async_readable_by?(actor) || false
            end
          end
        else
          # If the credit is *not* accepted, only those users with write access to
          # the advisory credit are allowed to see the pending or declined credit.
          async_writable_by?(actor)
        end
      end
    end
  end

  def readable_by?(actor)
    async_readable_by?(actor).sync
  end

  def async_writable_by?(actor)
    # A credit is always writable by the credited user.
    return Promise.resolve(true) if actor.is_a?(User) && actor.id == recipient_id

    # A credit is writable by any user that has write access to the underlying
    # repository advisory, if present.
    async_repository_advisory.then do |repository_advisory|
      repository_advisory&.async_writable_by?(actor) || false
    end
  end

  def writable_by?(actor)
    async_writable_by?(actor).sync
  end

  def pending?
    !accepted? && !declined?
  end

  def notified?
    notified_at?
  end

  def accepted?
    accepted_at?
  end

  def declined?
    declined_at?
  end

  def accept(actor:)
    raise UnauthorizedActorError unless actor == recipient

    already_accepted = false

    with_lock do
      already_accepted = accepted?
      next if already_accepted

      update!(accepted_at: Time.current, declined_at: nil)

      if repository_advisory
        repository_advisory.events.create!(
          actor: recipient,
          event: "credit_accepted",
          changed_attribute: "credit",
          created_at: accepted_at,
        )
      end
    end

    instrument_accept unless already_accepted
  end

  def decline(actor:)
    raise UnauthorizedActorError unless actor == recipient

    already_declined = false

    with_lock do
      already_declined = declined?
      next if already_declined

      update!(accepted_at: nil, declined_at: Time.current)

      if repository_advisory
        repository_advisory.events.create!(
          actor: recipient,
          event: "credit_declined",
          changed_attribute: "credit",
          created_at: declined_at,
        )
      end
    end

    instrument_decline unless already_declined
  end

  private

  def ensure_creator_is_not_blocked_by_recipient
    if creator.blocked_by?(recipient)
      # In vast majority of cases creator will not see this message,
      # as they just don't get the recipient in the auto-complete text box.
      # We still need database level check to make sure they didn't craft
      # malicious payload or got blocked between adding row and saving.
      # We are trying to not disclose the reason similar to
      # discussion comment ensure_author_is_not_blocked
      errors.add :creator, "cannot give credit at this time"
    end
  end

  def instrument_create
    instrument_event(:create)
  end

  def instrument_destroy
    instrument_event(:destroy)
  end

  def auto_accept
    self.accepted_at = Time.current
  end

  def given_to_self?
    recipient_id.present? && (recipient_id == creator_id)
  end

  def instrument_accept
    instrument_event(:accept)
  end

  def instrument_decline
    instrument_event(:decline)
  end

  def event_payload
    repository = repository_advisory&.repository
    actor_id = GitHub.context[:actor_id]
    actor = actor_id && User.find_by(id: actor_id)

    payload = {
      advisory_credit: self,
      ghsa_id: ghsa_id,
      repository_advisory: repository_advisory,
      repo: repository,
      vulnerability: vulnerability,
      recipient: recipient,
      creator: creator,
      actor: actor,
    }

    if repository&.in_organization?
      payload[:org] = repository.organization
    end

    payload
  end

  def instrument_event(event)
    instrument(event)

    full_event = "advisory_credit.#{event}"
    GlobalInstrumenter.instrument(full_event, event_payload)
    GitHub.dogstats.increment(full_event)
  end
end
