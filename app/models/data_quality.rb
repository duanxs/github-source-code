# frozen_string_literal: true

# TO APPEASE THE DEPENDENCY GODS
#
# DO NOT REMOVE FOR FEAR OF REPRISAL
module DataQuality
end

require_dependency "data_quality/script"
require_dependency "data_quality/script_runner"
