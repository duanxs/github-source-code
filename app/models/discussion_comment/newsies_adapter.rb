# frozen_string_literal: true

module DiscussionComment::NewsiesAdapter
  def async_viewer_can_delete?(viewer)
    return Promise.resolve(false) unless viewer

    Promise.all([async_user, async_repository]).then do
      deletable_by?(viewer)
    end
  end

  def message_id
    "<#{repository.name_with_owner}/repo-discussions/#{discussion.number}/comments/" \
      "#{id}@#{GitHub.urls.host_name}>"
  end

  # Internal: Filters out users that should keep a subscription to this thread.
  # This should be called after a comment has been edited, with a mentioned
  # user removed due to a typo. Remove anyone that hasn't commented already.
  #
  # users - Array of Users.
  #
  # Returns an Array of User that can be unsubscribed.
  def unsubscribable_users(users)
    discussion.unsubscribable_users(users)
  end

  def deliver_notifications?
    return false unless discussion.open?

    return false if discussion.converted_at && created_at <= discussion.converted_at

    return false if repository&.disable_discussions_notifications_flag_enabled?

    true
  end

  def get_notification_summary
    return unless discussion
    list = Newsies::List.new("Repository", discussion.repository_id)
    GitHub.newsies.web.find_rollup_summary_by_thread(list, discussion)
  end

  def notifications_thread
    discussion
  end

  def notifications_author
    user
  end

  def async_notifications_list
    async_repository
  end

  def async_entity
    async_repository
  end

  def entity
    repository
  end

  def permalink(include_host: true)
    "#{discussion.permalink(include_host: include_host)}#discussioncomment-#{id}"
  end
  alias_method :url, :permalink

  def author_subscribe_reason
    "comment"
  end
end
