# frozen_string_literal: true

class Registry::PackageActivity < ApplicationRecord::Domain::Repositories
  self.table_name = :package_activities

  include GitHub::Relay::GlobalIdentification

  belongs_to :owner, class_name: "User"
  belongs_to :repository
  belongs_to :package, class_name: "Registry::Package"

  scope :public_packages,   -> { joins(:repository).where(repositories: { public: true }) }
  scope :private_packages,  -> { joins(:repository).where(repositories: { public: false }) }

  def self.between(activity_start, activity_end)
    where(<<~SQL, start: activity_start, end: activity_end)
      activity_started_at BETWEEN :start AND :end
    SQL
  end

  # Public: Records hourly bandwidth usage for a given package.
  #
  # If activity is already recorded for the given package/hour, the bandwidth
  # numbers are added and the new source files are tracked.
  #
  # package       - The Registry::Package to record the usage for.
  # started_at    - An hour-aligned DateTime when the activity began.
  # up            - A Float indicating the bandwidth up in gigabytes.
  # down          - A Float indicating the bandwidth down in gigabytes.
  # source_files  - An Array of S3 log filenames where the activity was retreived from.
  #
  # Returns nothing.
  def self.track(package, started_at, up: nil, down: nil, source_files: nil)
    up = up.to_f
    down = down.to_f
    return unless up > 0.0 || down > 0.0

    uniq_src_files = Array(source_files).delete_if(&:blank?).uniq
    return if uniq_src_files.blank?

    sql = github_sql.new(
      owner_id: package.owner_id,
      repository_id: package.repository_id,
      package_id: package.id,
      started_at: started_at,
      down: down,
      up: up,
      files: encode_source_lines(uniq_src_files, 65535),
    )

    q = <<-SQL
      INSERT INTO package_activities (
        owner_id, repository_id, package_id,
        activity_started_at,
        bandwidth_down, bandwidth_up, source_files,
        created_at, updated_at
      ) VALUES (
        :owner_id, :repository_id, :package_id,
        :started_at,
        :down, :up, :files,
        NOW(), NOW()
      ) ON DUPLICATE KEY UPDATE
        bandwidth_down = bandwidth_down+VALUES(bandwidth_down),
        bandwidth_up = bandwidth_up+VALUES(bandwidth_up),
        source_files = RIGHT(CONCAT(source_files, ',', VALUES(source_files)), 65535),
        updated_at = NOW()
    SQL

    ActiveRecord::Base.connected_to(role: :writing) do
      sql.run(q)
    end
  end

  def self.encode_source_lines(lines, col_limit)
    s = Array(lines).join(",")
    s.gsub!(/\,{2,}/, ",") # remove dup commas
    s.gsub!(/\A,/, "")
    s.gsub!(/,\z/, "")
    if s.size > col_limit
      s = s[s.size - col_limit..-1]
    end
    s
  end

  def self.seen_source_files(package_id, started_at)
    activity = self.where(package_id: package_id, activity_started_at: started_at).first

    return Set.new unless activity

    ids = activity.source_files.to_s.split(",")
    ids.uniq!
    Set.new(ids.delete_if { |id| id.blank? })
  end

  def self.bandwidth
    summary = select(<<~SQL).first
      SUM(package_activities.bandwidth_up) as up,
      SUM(package_activities.bandwidth_down) as down
    SQL

    {
      up:   summary.up.to_f,
      down: summary.down.to_f,
    }
  end
end
