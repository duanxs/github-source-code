# frozen_string_literal: true

module Registry
  module PackageDownloadStatsService
    def downloads_today
      downloads_in_range(Time.now.beginning_of_day, Time.now.end_of_day)
    end

    def downloads_last_thirty_days
      downloads_in_range(30.days.ago.beginning_of_day, Time.now.end_of_day)
    end

    def downloads_this_week
      downloads_in_range(Time.now.beginning_of_week, Time.now.end_of_week)
    end

    def downloads_this_month
      downloads_in_range(Time.now.beginning_of_month, Time.now.end_of_month)
    end

    def downloads_this_year
      downloads_in_range(Time.now.beginning_of_year, Time.now.end_of_year)
    end

    def downloads_total_count
      downloads_in_range(nil, nil)
    end
  end
end
