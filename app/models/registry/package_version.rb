# frozen_string_literal: true

class Registry::PackageVersion < ApplicationRecord::Domain::Repositories
  include GitHub::Relay::GlobalIdentification
  include GitHub::UserContent
  include GitHub::UTF8
  include OcticonsCachingHelper
  include Registry::PackageDownloadStatsService

  class << self
    # Public: Given a database id, returns the global_relay_id
    def to_global_relay_id(version_id)
      Platform::Helpers::NodeIdentification.to_global_id("PackageVersion", version_id)
    end

    # Gets the summary for each version of a set of package version ids.
    # Uses a batched query.
    #
    # Returns: Hash { int => string }
    def summaries_for_ids(version_ids)
      self.left_joins(:metadata)
        .where(id: version_ids)
        .where("`registry_package_metadata`.`name` = ?", Registry::Metadatum::KEYS[:SUMMARY])
        .select("package_versions.id,registry_package_metadata.value as prefetched_summary")
        .group("registry_package_metadata.package_version_id")
        .each_with_object({}) { |p, h| h[p.id] = GitHub::Encoding.try_guess_and_transcode(p.prefetched_summary) }
    end

    # Gets the number of dependencies for each version of a set of package
    # version ids. Uses a batched query.
    #
    # Returns: Hash { int => int }
    def dependency_counts_for_ids(version_ids)
      self.left_joins(:dependencies)
        .where(id: version_ids)
        .select("package_versions.id, COUNT(registry_package_dependencies.id) as dependency_count")
        .group("registry_package_dependencies.registry_package_version_id")
        .each_with_object({}) { |p, h| h[p.id] = p.dependency_count }
    end

    def total_download_counts_for_ids(version_ids)
      batched_downloads_in_range(version_ids)
    end

    def thirty_day_download_counts_for_ids(version_ids)
      batched_downloads_in_range(version_ids, 30.days.ago.beginning_of_day, Time.now.end_of_day)
    end

    def batched_downloads_in_range(version_ids, start_time = nil, end_time = nil)
      if start_time && end_time
        # If we put the time condition on the where clause, it'll filter out the
        # empty rows from the left join. So we need to put the time condition on
        # the JOINS ON clause.
        join_condition = "LEFT OUTER JOIN `package_download_activities` ON `package_download_activities`.`package_version_id` = `package_versions`.`id` AND (`package_download_activities`.`started_at` BETWEEN ? AND ?)"
        query = self.joins(sanitize_sql_array([join_condition, start_time, end_time]))
      else
        query = self.left_joins(:downloads)
      end

      query = query.where(id: version_ids)
        .select("package_versions.id, SUM(package_download_activities.package_download_count) as downloads_count")
        .group("package_download_activities.package_version_id")

      raw_sums_by_version_id = query.each_with_object({}) { |v, h| h[v.id] = v.downloads_count.to_i }
      file_counts_by_version_id = Registry::PackageVersion
        .not_deleted
        .where(id: raw_sums_by_version_id.keys)
        .pluck(:id, :files_count)
        .to_h

      raw_sums_by_version_id.map do |id, raw_sum|
        # Return download count of 0 if the package version has no files.
        file_count = file_counts_by_version_id[id]
        if file_count.nil? || file_count == 0
          downloads = 0
        end

        downloads ||= (raw_sum / file_count.to_f).ceil
        [id, downloads]
      end.to_h
    end
  end

  belongs_to :package, class_name: "Registry::Package", foreign_key: :registry_package_id
  belongs_to :release, class_name: "Release"
  belongs_to :author, class_name: "User"
  belongs_to :deleted_by, class_name: "User"

  has_many :dependencies,
    class_name: "Registry::Dependency",
    foreign_key: "registry_package_version_id",
    dependent: :destroy

  has_many :files,
    class_name: "Registry::File"

  has_many :manifest_entries,
    class_name: "Registry::ManifestEntry",
    dependent: :destroy
  has_many :package_files, through: :manifest_entries, source: :file

  has_many :tags, class_name: "Registry::Tag",
    foreign_key: "registry_package_version_id",
    dependent: :destroy

  has_many :metadata, class_name: "Registry::Metadatum", dependent: :destroy

  has_many :downloads, class_name: "Registry::PackageDownloadActivity", dependent: :delete_all

  has_many :data_transfer_line_items, class_name: "PackageResitry::DataTransferLineItem"

  validates_presence_of :version
  validates_presence_of :author, on: :create

  after_commit :synchronize_search_index

  after_destroy_commit :destroy_package_if_last_version
  after_commit :retag_latest_version, on: [:destroy, :create]

  after_destroy_commit :ensure_delete_event_instrumented
  before_destroy :destroy_orphaned_files

  # Public: Returns package versions, latest first. If a version is tagged 'latest'
  # it comes first, otherwise the versions are returned most recently updated first.
  #
  # Returns a scoped relation.
  scope :latest, -> {
    joins("LEFT JOIN registry_package_tags " \
          "ON registry_package_tags.registry_package_version_id = package_versions.id " \
          "AND registry_package_tags.name = 'latest'").
      order("registry_package_tags.id DESC, package_versions.updated_at DESC")
  }

  scope :not_deleted, -> { where(deleted_at: nil) }

  scope :on_repository, ->(repository_id) {
      joins(:package).where("`registry_packages`.`repository_id` = ?", repository_id)
  }

  def body
    metadata.readme
  end

  def shell_safe_version
    Shellwords.escape(utf8(version))
  end

  def body_pipeline
    GitHub::Goomba::PackageVersionPipeline
  end

  def async_body_context
    super.then do |context|
      context.merge(anchor_icon: octicon("link"))
    end
  end

  def summary
    metadata.summary
  end

  def installation_command
    metadata.installation_command
  end

  def package_manifest
    m = self.manifest
    m ||= metadata.docker_manifest unless metadata.docker_manifest.empty?
    GitHub::Encoding.try_guess_and_transcode(m)
  end

  def serializeable_metadata
    metadata.serializeable
  end

  def trigger_create_webhook(actor_id)
    enqueue_webhook(:published, actor_id)
  end

  def trigger_update_webhook(actor_id)
    enqueue_webhook(:updated, actor_id)
  end

  def platform_type_name
    "PackageVersion"
  end

  def deleted?
    deleted_at.present?
  end

  def is_latest_version?
    self == package.latest_version
  end

  class PublicVersionDeletionError < StandardError; end

  # Marks the package version as deleted, hiding it from search results and
  # making it ineligible for downloads. Side effect: instruments
  # PackageVersionDeleted hydro event so the version is no longer counted
  # towards storage quota.
  #
  # Args:
  #   actor - the user who initiated this delete
  #   via_actions - whether or not this deletion came from Actions
  #   force_delete - whether or not to skip validation checks prior to deletion.
  #     Only true when call originates from repo archival.
  def delete!(actor: nil, via_actions: false, user_agent: "", force_delete: false)
    if !force_delete && package.repository&.public?
      raise PublicVersionDeletionError.new("Cannot delete public PackageVersion")
    end

    self.deleted_at = Time.now.utc
    self.deleted_by = actor
    save!

    if package.package_type.to_sym == :docker
      delete_docker_base_layer_if_last_version(actor, force_delete)
    end

    file_sizes = files.pluck(:size)
    params = {
      actor: actor,
      package: package,
      version: self,
      size: file_sizes.sum,
      files_count: file_sizes.count,
      deleted_at: deleted_at,
      storage_service: "AWS_S3",
      user_agent: user_agent,
      via_actions: via_actions,
    }

    # only purge fastly cache in non-enterprise production environment
    if Rails.env.production? && !GitHub.enterprise?
      files.each do |file|
        url = file.storage_external_url(actor)
        PurgeFastlyUrlJob.perform_later({"url" => strip_uri_query(url)}) if url.present?
      end
    end

    GlobalInstrumenter.instrument("package_registry.package_version_deleted", params)
  end

  def restore!(actor: nil)
    self.deleted_at = nil
    save!

    file_sizes = files.pluck(:size)
    files.each do |file|
      params = {
        actor: actor,
        package: package,
        version: self,
        size: file_sizes.sum,
        files_count: file_sizes.count,
        published_at: updated_at,
        storage_service: "AWS_S3",
        via_actions: published_via_actions,
        file: file,
      }

      GlobalInstrumenter.instrument("package_registry.package_version_published", params)
    end
  end

  def downloads_in_range(start_time, end_time)
    self.class.batched_downloads_in_range(id, start_time, end_time)[id]
  end

  private

  def should_retag_version?
    package.present? && Registry::Package.exists?(package.id) && (package.rubygems?)
  end

  def destroy_package_if_last_version
    with_lock do
      package.destroy if package && package.package_versions.empty?
    end
  end

  def delete_docker_base_layer_if_last_version(actor, force_delete = false)
    if package.latest_version&.version == "docker-base-layer"
      package.latest_version.delete!(actor: actor, via_actions: false, user_agent: "", force_delete: force_delete)
    end
  end

  def destroy_orphaned_files
    files.each do |file|
      file.destroy_if_orphaned
    end
  end

  def retag_latest_version
    return unless should_retag_version?

    latest_tag = Registry::Tag.where(name: "latest", registry_package_id: registry_package_id).first

    # Lower bound values.
    highest_version = Gem::Version.new("0.0.0")
    highest_version_id = -10

    package.reload.package_versions.pluck(:id, :version).each do |result|
      # Move to the next version unless this value is a semantic version string
      next unless !!result[1].match(Semantic::Version::SemVerRegexp)
      current_version = Gem::Version.new(result[1])
      if current_version > highest_version
        highest_version_id = result[0]
        highest_version = current_version
      end
    end

    # if we found a higher version.
    if highest_version_id > -10
      # Create a tag to point to the latest version.
      latest_tag = Registry::Tag.new(registry_package_id: registry_package_id, name: "latest") unless latest_tag
      latest_tag.registry_package_version_id = highest_version_id
      latest_tag.save
    end
  end

  def ensure_delete_event_instrumented
    return if self.deleted?

    file_sizes = files.pluck(:size)
    params = {
      actor: nil,
      package: package,
      version: self,
      size: file_sizes.sum,
      files_count: file_sizes.count,
      deleted_at: Time.now,
      storage_service: "AWS_S3",
      user_agent: nil,
      via_actions: false,
    }

    GlobalInstrumenter.instrument("package_registry.package_version_deleted", params)
  end

  def enqueue_webhook(event_type, actor_id)
    action = event_type == :published ? "create" : "update"
    input = {
      actor_id: actor_id,
      action: event_type,
      registry_package_id: self.registry_package_id,
      package_version_id: self.id,
    }

    GitHub.instrument("registry_package.#{action}", input)
    GitHub.instrument("package.#{action}", input)
  end

  def synchronize_search_index
    package&.synchronize_search_index
  end

  def strip_uri_query(uri)
    parsed = URI.parse(uri)
    parsed.scheme+"://"+parsed.host+parsed.path
  end
end
