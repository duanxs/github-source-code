# frozen_string_literal: true

class Registry::Tag < ApplicationRecord::Domain::Repositories
  self.table_name = :registry_package_tags

  include GitHub::Relay::GlobalIdentification

  belongs_to :package, class_name: "Registry::Package", foreign_key: :registry_package_id
  belongs_to :package_version, class_name: "Registry::PackageVersion", foreign_key: :registry_package_version_id

  validates :name, presence: true, uniqueness: { scope: :registry_package_id, case_sensitive: true }

  def platform_type_name
    "PackageTag"
  end
end
