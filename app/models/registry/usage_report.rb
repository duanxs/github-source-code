# frozen_string_literal: true

module Registry
  class UsageReport
    GIGABYTE = 1024**3

    FOREIGN_KEYS = {
      User => :owner_id,
      Repository => :repository_id,
      Registry::Package => :package_id,
    }

    attr_writer :public_storage, :private_storage
    attr_accessor :target, :activity_start, :activity_end,
      :public_up, :public_down, :private_up, :private_down,

    def self.between(target, activity_start, activity_end)
      report = new(target, activity_start, activity_end)
      report.calculate
      report
    end

    def initialize(target, activity_start, activity_end)
      @target = target
      @activity_start = activity_start
      @activity_end = activity_end

      raise ArgumentError, "target is not valid" unless foreign_key
    end

    def calculate
      calculate_bandwidth
      calculate_storage
    end

    def public_bandwidth_total
      (public_up + public_down).round(2)
    end

    def public_bandwidth_percent
      0 # public bandwidth is free
    end

    def private_bandwidth_total
      (private_up + private_down).round(2)
    end

    def private_bandwidth_percent
      return nil unless allocated_bandwidth

      percent(private_bandwidth_total /  allocated_bandwidth)
    end

    def allocated_bandwidth
      return unless target.is_a?(User)

      5.0 # TODO: Figure out allocation
    end

    def public_storage_percent
      0 # public storage is free
    end

    def private_storage_percent
      return nil unless allocated_storage

      percent(private_storage /  allocated_storage)
    end

    def allocated_storage
      return unless target.is_a?(User)

      5.0 # TODO: Figure out allocation
    end

    def public_storage
      @public_storage.round(2)
    end

    def private_storage
      @private_storage.round(2)
    end

    private

    def foreign_key
      return @foreign_key if @foreign_key

      FOREIGN_KEYS.each do |klass, key|
        return @foreign_key = key if target.is_a?(klass)
      end

      return nil
    end

    def calculate_bandwidth
      scoped_activity = PackageActivity.where(foreign_key => target.id).between(activity_start, activity_end)

      public_bandwidth = scoped_activity.public_packages.bandwidth
      self.public_up = public_bandwidth[:up]
      self.public_down = public_bandwidth[:down]

      private_bandwidth = scoped_activity.private_packages.bandwidth
      self.private_up = private_bandwidth[:up]
      self.private_down = private_bandwidth[:down]
    end

    # TODO: This is really expensive to calculate right now. Suggest we
    # denormalize repository_id on package_files so we can do this in SQL.
    def calculate_storage
      self.public_storage, self.private_storage = 0.0, 0.0
      files = target.package_files.where("package_files.created_at < ?", activity_end)

      files.find_each do |file|
        if file.repository.public?
          self.public_storage += (file.size.to_f / GIGABYTE)
        else
          self.private_storage += (file.size.to_f / GIGABYTE)
        end
      end
    end

    def percent(fraction)
      (fraction * 100).to_i
    end
  end
end
