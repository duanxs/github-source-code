# frozen_string_literal: true

class Registry::ManifestEntry < ApplicationRecord::Domain::Repositories
  self.table_name = :package_version_package_files

  include GitHub::Relay::GlobalIdentification
  belongs_to :package_version, class_name: "Registry::PackageVersion"
  belongs_to :file, class_name: "Registry::File", foreign_key: :package_file_id

  def platform_type_name
    "PackageVersionPackageFile"
  end
end
