# frozen_string_literal: true

class Import < ApplicationRecord::Domain::Imports
  areas_of_responsibility :import_api

  belongs_to :creator, class_name: "User", foreign_key: "user_id"

  has_many :repository_imports, class_name: "ImportedRepository"
  has_many :repositories, through: :repository_imports, split: true

  has_many :pull_request_imports
  has_many :pull_requests, through: :pull_request_imports, split: true

  validates_presence_of :creator
end
