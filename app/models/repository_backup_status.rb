# frozen_string_literal: true

# Dummy so we can run import the seed
class RepositoryBackupStatus < ApplicationRecord::Domain::GitbackupsMysql1
end
