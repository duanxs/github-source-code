# frozen_string_literal: true

# When users mention `Referenceable` objects, the context is largely in long
# form Markdown in descriptions and comment bodies. We process that text as
# `GitHub::UserContent` which detects mentions.
module Referrer
  def self.included(base)
    base.send :extend, ClassMethods
  end

  # Internal: the object that the reference comes from.
  #
  # Returns a Referrer.
  def referrer
    self
  end

  # Internal: the user making the reference.
  #
  # Returns a User object.
  def referring_actor
    user
  end

  # Private: should this comment track references?
  # This is a hook method provided for comments that have
  # more complicated workflow, and may not want to track their references
  # on every single save.
  #
  # Returns Boolean
  def track_references?
    true
  end
  private :track_references?

  # Creates cross references records via Referenceable#record_reference_from.
  #
  # reference_time - Timestamp used to record when the reference was made.
  #
  # Returns nothing.
  def create_mentioned_references(reference_time)
    mentioned_referenceables.each do |refable|
      refable.record_reference_from referrer, referring_actor, reference_time
    end
  end

  # Should we process the references in a background job?
  #
  # Returns a Boolean.
  def track_references_in_background?
    false
  end

  # Internal: Gather all of the `Referenceable` objects that were actually
  # mentioned by this `Referrer`.
  #
  # Returns an Array of `Referenceable` objects.
  def mentioned_referenceables
    refs = []

    if respond_to?(:mentioned_teams)
      refs.concat(mentioned_teams.compact)
    end

    if respond_to?(:mentioned_issues)
      refs.concat(mentioned_issues.compact)
    end

    if respond_to?(:mentioned_discussions)
      refs.concat(mentioned_discussions.compact)
    end

    refs
  end

  module ClassMethods
    # Public: Sets up the callbacks for reference mentions.
    #
    # Returns nothing.
    def setup_referrer
      before_save   reference_mentions_callback, if: :track_references?
      after_save    reference_mentions_callback, if: -> { track_references? && !track_references_in_background? }
      after_commit  reference_mentions_callback, if: -> { track_references? && track_references_in_background? }
    end

    def reference_mentions_callback
      @reference_mentions_callback ||= ReferenceMentionsCallback.new
    end
  end

  class ReferenceMentionsCallback
    def after_save(referrer)
      track_references!(referrer)
    end

    def after_commit(referrer)
      track_references!(referrer)
    end

    def before_save(referrer)
      @previous_updated_at = referrer.updated_at
    end

    def track_references!(referrer)
      reference_mentions(referrer)

      @previous_updated_at = nil

      true
    end

    # Create references for mentioned referenceables.
    # This is run after save.
    #
    # Returns nothing.
    private def reference_mentions(referrer)
      return unless actor = referrer.referring_actor

      ref_time = reference_time(referrer)

      if referrer.track_references_in_background?
        ProcessMentionedReferencesJob.perform_later(referrer, ref_time)
      else
        referrer.create_mentioned_references(ref_time)
      end
    end

    # Internal
    #
    # The reference time is recorded as when the reference itself was made.
    # This means when the referrer was updated (or created) if the body was changed.
    # If the body was *not* changed, use the cached updated_at value from
    # before saving.
    #
    # Returns Time used for reference
    private def reference_time(referrer)
      if referrer.saved_change_to_body?
        referrer.updated_at
      else
        @previous_updated_at
      end
    end
  end
end
