# frozen_string_literal: true

module Coupon::ZuoraDependency
  # Public: Returns the ProductRatePlanId for this plan and the given cycle in Zuora
  #
  # cycle: "year" or "month" - the billing cycle we want the zuora id for
  #
  # Returns String
  def zuora_id(cycle:)
    product_uuid(cycle).zuora_product_rate_plan_id
  end

  # Public: the Billing::ProductUUID object for this plan
  #
  # This returns a Billing::ProductUUID record representing this plan's
  # Zuora Rate Plan ID and the Zuora plan's Product Rate Plan IDs
  #
  def product_uuid(billing_cycle)
    ::Billing::ProductUUID.find_by(product_type: "github.coupon", product_key: product_key, billing_cycle: billing_cycle)
  end

  def zuora_charge_ids(cycle:)
    product_uuid(cycle).zuora_product_rate_plan_charge_ids
  end

  # Public: Syncs the plan to Zuora if its ProductUUID records don't exist, and will generate a ProductUUID
  # for each type of billing cycle
  #
  # Returns Array
  def sync_to_zuora
    GitHub::Billing::ZuoraProduct.create \
      product_type: "github.coupon",
      product_key: product_key,
      product_name: zuora_product_name,
      charges: zuora_charges
  end

  # Public: The charge type of this coupon. Either :fixed_discount or
  # :percentage_discount
  #
  # Returns Symbol
  def zuora_charge_type
    zuora_charges.first[:type]
  end

  def zuora_product_name
    type = percentage? ? "Percentage" : "Fixed"
    "GitHub #{type} Discount"
  end

  private

  # Private: The product key for looking up the ProductUUID record
  #
  # Returns String
  def product_key
    percentage? ? "percentage" : "fixed_amount"
  end

  # Private: The charges tied to this plan that will be synced to Zuora
  # These charges differ based on a percentage-off coupon vs a fixed-amount
  # coupon
  #
  # Returns Array
  def zuora_charges
    percentage? ? [percentage_charge] : [fixed_amount_charge]
  end

  def percentage_charge
    {
      type: :percentage_discount,
      prices: { year: zero_dollars, month: zero_dollars },
    }
  end

  def fixed_amount_charge
    {
      type: :fixed_discount,
      prices: { year: zero_dollars, month: zero_dollars },
    }
  end

  def zero_dollars
    Billing::Money.new(0).dollars
  end
end
