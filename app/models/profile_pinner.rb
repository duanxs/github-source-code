# frozen_string_literal: true

class ProfilePinner

  def self.areas_of_responsibility
    [:user_profile]
  end

  REPO_LIMIT = 1000

  # Public: Removes the given items from user's pinned list on their profile.
  #
  # items_to_unpin - one or more Repositories or Gists to unpin from the user's profile
  # user - a User or Organization
  # viewer - the User who is doing the unpinning
  #
  # Returns nothing.
  def self.unpin(*items_to_unpin, user:, viewer:)
    items = ActiveRecord::Base.connected_to(role: :reading) { user.pinned_items(viewer: viewer) }
    pinner = ProfilePinner.new(user: user, viewer: viewer, items: items)
    pinner.async_unpin(items_to_unpin).sync
  end

  attr_reader :user, :viewer, :items

  def initialize(user:, viewer:, items: nil)
    @user = user
    @viewer = viewer
    @items = items || []
  end

  # Public: Returns a Promise that resolves to a list of repositories and gists the user is allowed
  # to pin.
  #
  # types - list of pinnable item types to include, defaults to including all possible types;
  #         valid values include "Repository" and "Gist"
  #
  # Returns a Promise resolving to an Array of Repositories and Gists.
  def async_pinnable_items(types: ProfilePin.pinned_item_types.keys, repo_limit: REPO_LIMIT)
    if user.organization?
      async_organization_pinnable_items(types: types, repo_limit: repo_limit)
    elsif user.user?
      Promise.resolve(user_pinnable_items(types: types, repo_limit: repo_limit))
    else # bots can't pin repositories
      Promise.resolve([])
    end
  end

  # Public: Get a Promise returning a list of repos and gists that can be pinned for this user.
  #
  # types - list of pinnable item types to include, defaults to including all possible types;
  #         valid values include "Repository" and "Gist"
  #
  # Returns a Promise resolving to an Array of Repositories and Gists.
  def async_sorted_pinnable_items(types: ProfilePin.pinned_item_types.keys)
    async_pinnable_items(types: types).then do |pinnable_items|
      max_star_count = pinnable_items.map(&:stargazer_count).max
      already_pinned_items = user.pinned_items(viewer: viewer)
      total_pins = already_pinned_items.count
      has_pins = total_pins > 0

      pinnable_items.sort_by do |item|
        sort_order = if item.is_a?(Repository)
          is_my_repo = item.owner_id == user.id
          [
            is_my_repo ? 0 : 1,
            max_star_count - item.stargazer_count,
            item.name_with_owner.downcase,
          ]
        else # Gist
          is_my_gist = item.user_id == user.id
          [
            is_my_gist ? 0 : 1,
            max_star_count - item.stargazer_count,
            item.name.downcase,
          ]
        end

        if has_pins
          position = already_pinned_items.index(item) || total_pins
          sort_order = [position] + sort_order
        end

        sort_order
      end
    end
  end

  def async_unpin(items_to_unpin)
    items_to_unpin.each do |item|
      items.delete(item)
    end

    async_pin # deletes pins from list, resorts, and saves.
  end

  # Public: Pin a list of repositories and gists to a user's profile.
  #
  # types - list of item types to allow being pinned, defaults to including all possible types;
  #         valid values include "Repository" and "Gist"
  #
  # Returns nothing.
  def async_pin(types: ProfilePin.pinned_item_types.keys)
    profile = user.find_or_create_profile

    async_items_to_pin(types: types).then do |items_to_pin|
      profile.pin_items(items_to_pin)
    end
  end

  private

  def async_organization_pinnable_items(types:, repo_limit: REPO_LIMIT)
    if types.include?("Repository")
      Promise.resolve(user.public_repositories.limit(repo_limit).filter_spam_and_disabled_for(viewer))
    else
      Promise.resolve([])
    end
  end

  def user_pinnable_items(types:, repo_limit: REPO_LIMIT)
    pins = user.pinned_items(viewer: viewer, types: types)
    my_repos = other_repos = []

    if types.include?("Repository")
      my_repos = public_repositories_owned_or_contributed_to(repo_limit: repo_limit)
      other_repos = other_repositories_with_commits
    end

    gists = if types.include?("Gist")
      user.gists.are_public.filter_spam_and_disabled_for(viewer)
    else
      []
    end

    (pins + my_repos + gists + other_repos).uniq
  end

  def async_items_to_pin(types:)
    async_pinnable_items(types: types).then do |pinnable_items|
      items & pinnable_items
    end
  end

  # Private: Returns a list of the user's owned, active, public repositories as
  # well as public repositories to which they have contributed recently.
  def public_repositories_owned_or_contributed_to(repo_limit: REPO_LIMIT)
    ActiveRecord::Base.connected_to(role: :reading) do
      # Should include repositories contributed to in the last year:
      contributed_repo_ids = user.contributions_collector.visible_repository_ids
      Repository.active.public_scope.
        where("repositories.owner_id = ? OR repositories.id IN (?)",
              user.id, contributed_repo_ids).
        filter_spam_and_disabled_for(viewer).
        order("pushed_at DESC").limit(repo_limit)
    end
  end

  # Private: Get other repositories the user has committed to that may not be included in
  # what the contributions collector finds because the commits were made too long ago.
  #
  # Returns an Array or a Repository relation.
  def other_repositories_with_commits
    return [] if user.large_bot_account? || user.large_scale_contributor?

    ActiveRecord::Base.connected_to(role: :reading) do
      response = ::Collab::Responses::Array.new do
        CommitContribution.for_user(user).where("committed_date <= ?", 1.year.ago).
          distinct.pluck(:repository_id)
      end
      repo_ids = response.value.take(REPO_LIMIT)
      validator = CommitContribution::RepositoryValidator.new(user, repository_ids: repo_ids)
      valid_repo_ids = validator.valid_repository_ids

      Repository.active.public_scope.where(id: valid_repo_ids).
        filter_spam_and_disabled_for(viewer)
    end
  end
end
