# rubocop:disable Style/FrozenStringLiteralComment

class GistStar < ApplicationRecord::Domain::Gists
  self.table_name = :starred_gists

  belongs_to :user
  belongs_to :gist

  scope :recently_starred_gists, -> { joins(:gist).merge(Gist.public_listing).order("starred_gists.created_at DESC") }
end
