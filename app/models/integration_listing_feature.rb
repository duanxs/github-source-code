# frozen_string_literal: true

class IntegrationListingFeature < ApplicationRecord::Domain::Integrations
  areas_of_responsibility :ce_extensibility

  belongs_to :integration_feature
  belongs_to :integration_listing

end
