# frozen_string_literal: true

class ImportedRepository < ApplicationRecord::Domain::Imports
  self.table_name = :repository_imports

  areas_of_responsibility :import_api

  belongs_to :import, class_name: "Import"
  belongs_to :repository, class_name: "Repository"

  validates_presence_of :import, :repository
end
