# frozen_string_literal: true

module Eventer
  module Messages
    COUNTER_UPDATE_SCHEMA = "github.eventer.v0.CounterUpdate"

    class << self
      def build_event_owners(owners)
        Array.wrap(owners).each_with_object([]) do |owner, event_owners|
          next if owner.nil?
          if owner.is_a?(Organization)
            event_owners << { owner_type: :USER, owner_id: owner.id }
            event_owners << { owner_type: :BUSINESS, owner_id: owner.business.id } if owner.business
          else
            owner_type = owner.class.name.underscore.upcase.to_sym
            event_owners << { owner_type: owner_type, owner_id: owner.id }
          end
        end
      end

      def hydro_options(owner)
        {
          schema: COUNTER_UPDATE_SCHEMA,
          partition_key: "#{owner[:owner_type]}-#{owner[:owner_id]}",
        }
      end
    end
  end
end
