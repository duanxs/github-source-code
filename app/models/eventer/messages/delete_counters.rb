# frozen_string_literal: true

module Eventer
  module Messages
    class DeleteCounters

      # DeleteCounters messages are used to mark *all* events for owner/event for deletion.

      def self.build(event_name, owner:)
        Eventer::Messages.build_event_owners(owner).map do |o|
          msg = {
            delete_counters: {
              event_owner: o,
              event_type: {
                name: event_name,
              },
            },
          }

          [
            msg,
            Eventer::Messages.hydro_options(o),
          ]
        end

      end
    end
  end
end
