# frozen_string_literal: true

module Eventer
  module Messages
    class IncrementCounter
      # IncrementCounter messages increment/decrement (if delta <0) the count for the event being tracked
      def self.build(event_name, owner:, delta: nil, timestamp: Time.now.utc, bucket_size: Eventer::TimeBucket::BUCKET_SIZE_DAY, tag: nil)
        Eventer::Messages.build_event_owners(owner).map do |event_owner|
          message = {
            event_owner: event_owner,
            event_type: {
              name: event_name,
            },
            time_bucket: Eventer::TimeBucket.build(timestamp: timestamp, size: bucket_size),
          }

          message[:count] = delta if delta
          message[:tag] = { name: tag } if tag

          [
            {increment_counter: message},
            Eventer::Messages.hydro_options(event_owner),
          ]
        end
      end
    end
  end
end
