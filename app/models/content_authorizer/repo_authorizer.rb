# frozen_string_literal: true

class ContentAuthorizer::RepoAuthorizer < ContentAuthorizer
  attr_accessor :repo, :owner

  def initialize(actor, operation, data)
    super
    @repo  = data[:repo]  || GitHub::NullRepository.new
    @owner = data[:owner] || @actor
  end

  def self.valid_operations
    super + [:fork, :fork_select]
  end

  # Public: All of the authorization errors blocking the given actor from
  # creating or modifying a repo. These should be assembled in priority order
  # (from most urgent/important to least) because the API will only return the
  # first error discovered.
  #
  # fail_fast - Bail out after the first failure.
  #
  # Returns an Array of ContentAuthorizationError objects.
  def errors(fail_fast: false)
    @errors ||= ([]).tap do |errors|
      if !actor
        errors << ContentAuthorizationError::AnonymousActor.new
      end

      if verified_email_required_to?(operation)
        if actor.must_verify_email?
          errors << ContentAuthorizationError::EmailVerificationRequired.new
          return errors if fail_fast
        end
      end

      if repo.archived? && operation != :fork
        errors << ContentAuthorizationError::RepoArchived.new
        return errors if fail_fast
      end

      if !in_automated_data_transformation? && repo.locked_on_migration?
        errors << ContentAuthorizationError::RepoLocked.new
      end

      if owner.user? && actor != owner
        errors << ContentAuthorizationError::ActorOwnerMismatch.new
      end

      if actor.user? && actor.ghost?
        errors << ContentAuthorizationError::GhostCantDoThat.new
      end
    end
  end
end
