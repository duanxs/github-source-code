# rubocop:disable Style/FrozenStringLiteralComment

class UserContentEdit < ApplicationRecord::Domain::UserContentEdits
  include GitHub::Relay::GlobalIdentification
  include FilterPipelineHelper
  include Instrumentation::Model

  module Core
    extend ActiveSupport::Concern

    included do |base|
      belongs_to :editor, class_name: "User"
      belongs_to :deleted_by, class_name: "User"
      belongs_to :performed_via_integration, foreign_key: "performed_by_integration_id", class_name: "Integration"
    end

    class_methods do
      def matching_user_content(user_content_edit)
        where(user_content_id: user_content_edit.user_content_id)
      end
    end

    def event_prefix
      "user_content_edit"
    end

    def platform_type_name
      "UserContentEdit"
    end

    def async_viewer_can_read?(viewer)
      async_user_content.then do |user_content|
        user_content.async_viewer_can_read_user_content_edits?(viewer)
      end
    end

    def viewer_can_read?(viewer)
      async_viewer_can_read?(viewer).sync
    end

    def async_viewer_can_delete?(viewer)
      return Promise.resolve(false) unless viewer
      return Promise.resolve(true) if viewer.id == editor_id

      async_user_content.then do |user_content|
        user_content.async_viewer_can_delete_user_content_edits?(viewer)
      end
    end

    def viewer_can_delete?(viewer)
      async_viewer_can_delete?(viewer).sync
    end

    def event_payload
      {
        user_content_type: user_content_type,
        user_content_id: user_content_id,
        editor: self.editor&.login,
        editor_id: self.editor&.id,
      }
    end

    # Removes the public diff and marks the user and when it was "deleted"
    def soft_delete!(user)
      content = safe_diff
      transaction do
        # need to purge the sensitive content
        touch(:deleted_at)
        update(diff: "deleted", deleted_by: user)
      end
      instrument :delete, deleted_by: user.login, deleted_by_id: user.id, deleted_at: self.deleted_at, deleted_content: content
    end

    # Finds the content for this user_content_edit to compare it to for diff view
    def diff_before
      edit = self.prev_edit
      edit.nil? ? "" : edit.diff
    end

    # Ensure that everything coming back is UTF-8 this ensures that emoji don't break things
    def safe_diff
      transcode_yaml_value(self.diff)
    end

    # Find the previous user content edit
    def prev_edit
      self.class.matching_user_content(self).where("id < ? AND deleted_at IS NULL", self.id).order(id: :desc).limit(1).first
    end

    def newest?
      self.class.matching_user_content(self).where("id > ?", self.id).empty?
    end
  end

  include Core

  # Overwrites UserContentEdit::Core.matching_user_content to include `user_content_type`
  def self.matching_user_content(user_content_edit)
    super(user_content_edit).where(user_content_type: user_content_edit.user_content_type)
  end

  belongs_to :user_content, polymorphic: true

  def global_relay_id
    if self.user_content_type == "GistComment"
      async_id = self.async_user_content.then do |user_content|
        user_content.async_gist.then do |gist|
          "#{gist.repo_name}:#{id}"
        end
      end
      Platform::Helpers::NodeIdentification.to_global_id("UserContentEdit", async_id.sync)
    else
      super
    end
  end
end
