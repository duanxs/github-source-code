# rubocop:disable Style/FrozenStringLiteralComment

module Stafftools
  # This module is used via the authorized? function, it returns a bool indicating whether or
  # not a user can access a portion of stafftools. Authorization is determined by comparing the roles
  # assigned to the user via stafftools_roles to the access roles set for the controller/action
  # in `config/stafftools_permissions.yml`.
  #
  # If you're seeing 403s in stafftools and require escalated privileges for local development,
  # try `bin/rake stafftools_auth:set_up_roles_for_dev`.
  #
  module AccessControl
    extend self

    class RuleValidationError < StandardError
    end

    # Main authorization checking function, declared at module level
    # params:
    # user - Instance of activerecord User
    # area_description - Object - Expected to contain keys
    #        describing the controller and action to access as strings
    def authorized?(user, area_description)
      return false unless user.site_admin?
      return true if GitHub.enterprise?

      controller_name = area_description.fetch(:controller)
      action = area_description.fetch(:action)

      controller_settings = find_controller_permissions(controller_name)
      allowed_roles = get_access_roles(controller_settings, action)
      user_access = allowed_roles & get_user_roles(user)

      return user_access.any?
    end

    # Api authorization check
    # params:
    # user - Instance of ActiveRecord user.
    # area_description - Object - Expects keys
    #   • route_pattern - a string representing an api endpoint. I.E. /staff/users
    #   • request_method - the method used in the request. GET, POST, PUT etc
    def api_authorized?(user, area_description)
      return false unless user&.user? && user.site_admin?
      return true if GitHub.enterprise?

      route_pattern = area_description.fetch(:route_pattern)
      request_method = area_description.fetch(:request_method)

      # Get route level permissions
      route_access_settings = find_rest_api_permissions(route_pattern)

      # Get the allowed permissions
      allowed_roles = get_access_roles(route_access_settings, request_method, action_namespace: "request_method")

      # Check for user access
      user_access = allowed_roles & get_user_roles(user)

      return user_access.any?
    end

    def permissions
      @@permissions ||= load_permissions
    end

    def load_permissions
      file_path = Rails.root.join("config", "stafftools_permissions.yml")
      Psych.safe_load(File.read(file_path))["permissions"]
    end

    private

    # Compile a list of roles with access to the desired content and action
    def get_access_roles(content_settings, action_name, action_namespace: "name")
      allowed_roles = roles_with_access_for(content_settings)
      roles_with_action_access_for(content_settings, action_name, allowed_roles, action_namespace)
    end

    def get_user_roles(user)
      # "*" signifies access to everyone so add * to the users roles
      user.stafftools_roles.pluck(:name) + ["*"]
    end

    # Return a list of roles permitted to access the content
    def roles_with_access_for(content_settings)
      allowed = []
      if content_settings.length == 0
        # default to open to all users with "*".
        allowed += ["*"]
      else
        allowed += content_settings.first["allowed_roles"]
      end

      allowed
    end

    # Returns a list of roles permitted to access actions within the content
    def roles_with_action_access_for(content_permissions, action_name, allowed_roles, action_namespace)
      # we don't need to check for permissions as there aren't any to check against
      return allowed_roles if content_permissions.empty?
      action_rules =  find_action_permissions(content_permissions, action_name, action_namespace)
      # No action roles found, return controller roles
      return allowed_roles if action_rules.empty?
      # This should really live in a linter so that it is done at time of checkin. This
      # is just a sanity check for the time being
      validate_action_rules(action_rules)
      if override_roles = action_rules.first.fetch("allowed_roles_override", false)
        # the action defines new roles, that override the controller level allowed roles
        return override_roles
      elsif excluded_roles = action_rules.first.fetch("excluded_roles", false)
        # we are removing some roles from the controller level allowed roles definitions
        return calculate_excluded_roles(allowed_roles, excluded_roles)
      end

      # No change to the controller setting found
      return allowed_roles
    end


    def calculate_excluded_roles(allowed_roles, excluded_roles)
      if allowed_roles.include?("*")
        StafftoolsRole.where("name NOT IN (?)", excluded_roles).pluck(:name)
      else
        allowed_roles - excluded_roles
      end
    end

    # Check rules for validity
    def validate_action_rules(rules)
      rule = rules.first
      if rule.has_key?("allowed_roles")
        raise Stafftools::AccessControl::RuleValidationError.
          new(msg="invalid action rule #{rule['name']}, cannot set allowed_roles please use allowed_roles_override")
      end

      if rule.has_key?("excluded_roles") && rule.has_key?("allowed_roles_override")
        msg = "Invalid action rule #{rule['name']} cannot set both allowed_roles and excluded_roles"
        raise Stafftools::AccessControl::RuleValidationError.new(msg: msg)
      end
    end

    def find_rest_api_permissions(pattern)
      permissions.fetch("rest_api", []).select do |el|
        el["route_pattern"] == pattern
      end
    end

    def find_controller_permissions(name)
      permissions.fetch("controllers", []).select do |el|
        el["name"] == name
      end
    end

    def find_action_permissions(content_permissions, action_name, action_namespace)
      content_permissions.first.fetch("actions", []).select do |el|
        el[action_namespace] == action_name
      end
    end
  end
end
