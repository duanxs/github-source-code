# frozen_string_literal: true
module Stafftools
  class Searcher
    attr_reader :query, :results

    GPG_KEY_PATTERN = /\A[A-F0-9]{16}\z/i
    Struct.new(
      "StafftoolsSearchResults", :users, :businesses, :repositories, :gists,
      :oauth_app_user_counts, :oauth_apps, :integrations,
      :integration_installations, :fuzzy_users, :deleted_users, :renamed_user, :teams,
      :old_name, :gpg_keys, :gpg_key_users, :public_key, :oauth_access, :oauth_application, :hook) do
        def initialize(*)
          super
          self.users ||= []
          self.businesses ||= []
          self.repositories ||= []
          self.gists ||= []
          self.oauth_app_user_counts ||= {}
          self.oauth_apps ||= []
          self.integrations ||= []
          self.integration_installations ||= []
          self.fuzzy_users ||= []
          self.deleted_users ||= []
          self.teams ||= []
        end
      end

    def initialize(query, current_user)
      @results = Struct::StafftoolsSearchResults.new
      @query = (query || "").strip
      @current_user = current_user
    end

    def search_for_gpg_key
      if @query =~ GPG_KEY_PATTERN
        key_id = [@query.downcase].pack("H*")
        @results.gpg_keys = ::GpgKey.where(key_id: key_id)

        @results.gpg_key_users = @results.gpg_keys.map { |k| k.user }
      end
      self
    end

    def search_for_users
      search_for_gpg_key if @results.gpg_key_users.nil?
      standard_user_search
      coupon_users_search
      spammy_users_search
      deleted_users_search
      @results.users.uniq!
      @results.users.compact!
      self
    end

    def search_for_businesses
      businesses_search
      fuzzy_businesses_search
      @results.businesses.uniq!
      @results.businesses.compact!
      self
    end

    def search_for_teams
      @results.teams << ::Team.find_by_id(@query) if numeric_query?
      begin
        type, id = ::Platform::Helpers::NodeIdentification.from_global_id(@query)
        @results.teams << ::Team.find_by(id: id) if type == "Team"
      rescue ::Platform::Errors::NotFound
      end
      if @query.count("/") == 1
        org, team = @query.split("/")
        @results.teams += ::Team.joins(:organization).where(users: { login: org }).where(slug: team)
      end
      @results.teams.uniq!
      @results.teams.compact!
      self
    end

    def search_for_renamed_users
      # We can't query for "action:user.rename" because it's indexed with the
      # new name only.
      phrase = "(user:#{@query} OR org:#{@query})"
      es_query = Audit::Driftwood::Query.new_stafftools_query(
        phrase: phrase,
        current_user: @current_user,
      )
      results = es_query.execute
      logs = ::AuditLogEntry.new_from_array(results)
      if (log = logs.find { |l| l.hit[:org] == @query || l.hit[:user] == @query })
        id = log.hit[:org] == @query ? log.org_id : log.user_id
        @results.renamed_user = ::User.find_by_id(id)
        @results.old_name = @query
      end
      self
    end

    def search_for_repositories
      fuzzy_repository_search
      repos_search
      @results.repositories.uniq!
      @results.repositories.compact!
      self
    end

    def search_for_gists
      @results.gists << ::Gist.with_name_with_owner(@query)
      @results.gists << ::Gist.find_by_repo_name(@query)
      @results.gists << ::Archived::Gist.with_name_with_owner(@query)
      @results.gists << ::Archived::Gist.find_by_repo_name(@query)
      @results.gists.uniq!
      @results.gists.compact!
      self
    end

    def search_for_oauth_application
      cond = if numeric_query?
        ["id = ? OR name = ?", @query, @query]
      else
        {name: @query}
      end
      app_ids = ::OauthApplication.where(cond).pluck(:id)
      unless app_ids.empty?
        sql = OauthAuthorization.github_sql.new(<<-SQL, app_ids: app_ids)
          SELECT application_id, COUNT(oauth_authorizations.id) AS authorization_count
          FROM     oauth_authorizations
          WHERE    application_id
          IN       :app_ids
          GROUP BY application_id
          ORDER BY authorization_count DESC
          LIMIT    50
        SQL
        results = sql.results
        @results.oauth_app_user_counts = Hash[*results.flatten]
        @results.oauth_apps.concat ::OauthApplication
          .where(id: @results.oauth_app_user_counts.keys).includes(:user).all
      end
      self
    end

    def search_for_integrations
      integrations_search
      integration_installations_search if numeric_query?
      @results.integration_installations.uniq!
      @results.integrations.uniq!
      @results.integration_installations.compact!
      @results.integrations.compact!
      self
    end

    def search_for_hook
      @results.hook = Hook.find_by_id(@query) if numeric_query?
      self
    end

    def search_for_public_key
      @results.public_key = ::PublicKey.find_by_fingerprint(@query)
      self
    end

    def search_for_oauth_access_token
      return self unless @query =~ OauthAccess::TOKEN_PATTERN
      hashed_token = OauthAccess.hash_token(@query)
      @results.oauth_access = OauthAccess.find_by_hashed_token(hashed_token)
      self
    end

    def search_for_oauth_application_by_key
      return self unless @query =~ OauthApplication::CLIENT_ID_PATTERN
      @results.oauth_application = ::OauthApplication.where(key: @query).first
      self
    end

    private

    def standard_user_search
      @results.users = if @query =~ /@/
        u = []
        u << ::User.find_by_email(@query)
        u += ::User.where(gravatar_email: @query).to_a
        u += ::User.where(organization_billing_email: @query).to_a
      else
        u = ::User.where(login: @query).to_a
        u << ::User.find_by_id(@query) if numeric_query?

        id = ::User.decode_id(@query)
        if ::User.encode_id(id) == @query.downcase # Ensure decode wasn't truncated
          u << ::User.find_by_id(id)
        end
        key_users = @results.gpg_key_users
        u += key_users if key_users
        u
      end
    end

    def coupon_users_search
      coupon = ::Coupon.find_by_code(@query)
      @results.users += coupon.users if coupon
    end

    # spammy users are _not_ in the search index, so we are doing a fuzzy
    # search in addition to the normal user searches above
    def spammy_users_search
      fuzzy_finder = ::Search::Queries::UserFuzzyFinder.new current_user: @current_user,
                                                          query: @query, highlight: true
      fuzzyresults = fuzzy_finder.execute
      @results.fuzzy_users = fuzzyresults.results
      @results.fuzzy_users.delete_if do |hit|
        if hit["_exact"]
          @results.users << hit["_model"]
          true
        end
      end
    end

    def deleted_users_search
      # Find deleted users
      phrase = if numeric_query?
        "(user:#{@query} OR org:#{@query} OR user_id:#{@query} OR org_id:#{@query}) AND (action:user.delete OR action:org.delete)"
      elsif @query =~ /@/
        "(data.email:#{@query}) AND (action:user.delete OR action:org.delete)"
      else
        "(user:#{@query} OR org:#{@query}) AND (action:user.delete OR action:org.delete)"
      end
      es_query = Audit::Driftwood::Query.new_stafftools_query(
        phrase: phrase,
        current_user: @current_user,
      )
      results = es_query.execute
      @results.deleted_users = ::AuditLogEntry.new_from_array(results).select do |log|
        log.data["id"] ||= log.user_id || log.org_id
        log.data["legal_hold"] = ::LegalHold.where(user_id: log.data["id"]).any?
        # Users might have been recreated already.
        !::User.exists?(log.data["id"])
      end
      @results.deleted_users.uniq! { |log| log.data["id"] }
    end

    def fuzzy_repository_search
      return if @query =~ /\// # We have a name-with-owner query, don't bother with hitting ES
      es_query = {query: {
        bool: {
          must: {
            multi_match: {
              query: @query,
              type: "most_fields",
              fields: %w[name^1.2 name.camel name.ngram^0.8],
              operator: "and",
            },
          },
        }},
        _source: false,
        size: 50,
      }
      query_params = {type: "repository"}
      index = ::Elastomer::Indexes::Repos.new
      response = index.search(es_query, query_params)

      hits = response["hits"]["hits"]
      unless hits.empty?
        ids = hits.map { |hit| hit["_id"] }
        id_sort = Arel.sql("field(id, #{ids.join(',')})")
        @results.repositories.concat ::Repository.where(id: ids).order(id_sort).includes(:owner, :mirror).all
      end
    end

    def repos_search
      @results.repositories << ::Repository.with_name_with_owner(@query)
      @results.repositories << ::Repository.find_by_id(@query) if numeric_query?
    end

    def integrations_search
      @results.integrations << ::Integration.find_by_id(@query) if numeric_query?
      @results.integrations << ::Integration.where(name: @query).first
    end

    def integration_installations_search
      @results.integration_installations << ::IntegrationInstallation.find_by_id(@query)
    end

    def businesses_search
      return if GitHub.single_business_environment?

      @results.businesses << ::Business.find_by_id(@query) if numeric_query?
      @results.businesses << ::Business.find_by(slug: @query)
    end

    def fuzzy_businesses_search
      return if GitHub.single_business_environment?

      query = ::Search::Queries::EnterpriseQuery.new query: @query
      results = query.execute
      unless results.empty?
        ids = results.map { |hit| hit["_id"] }
        @results.businesses.concat(Business.where(id: ids).all)
      end
    end

    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def numeric_query?
      @numeric_query ||= /\A\d+\z/.match?(@query)
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator
  end
end
