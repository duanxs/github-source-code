# rubocop:disable Style/FrozenStringLiteralComment

class Stafftools::RecentComments
  def self.for_user(user)
    comments = []

    comments.concat(Issue.public_scope.where(user: user).order(created_at: :desc).limit(25).
      includes(:repository, :conversation))
    comments.concat(IssueComment.public_scope.where(user: user).order(created_at: :desc).limit(25).
      includes(:issue, :repository))
    comments.concat(PullRequestReviewComment.public_scope.where(user: user).order(created_at: :desc).limit(25).
      includes(:repository))
    comments.concat(CommitComment.public_scope.where(user: user).order(created_at: :desc).limit(25).
      includes(:repository))
    comments.concat(GistComment.public_scope.where(user: user).order(created_at: :desc).limit(25))

    comments.sort_by(&:created_at).reverse.first(25)
  end

  def self.minimized_comment_count(user)
    phrase = "action:*.minimize_comment user:#{user.login}"
    query = Audit::Driftwood::Query.new_stafftools_query(
      phrase: phrase,
      current_user: user,
    )
    query.execute.total
  end
end
