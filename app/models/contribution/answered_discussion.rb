# frozen_string_literal: true

class Contribution::AnsweredDiscussion < Contribution
  areas_of_responsibility :user_profile, :discussions

  delegate :repository, :discussion, :comment, :hash, :async_repository,
    :repository_id, to: :discussion_event
  delegate :title, :number, :comment_count, to: :discussion

  def discussion_event
    subject
  end

  def comment_id
    comment.id
  end

  def associated_subject
    discussion_event.repository
  end

  def organization_id
    associated_subject&.organization_id
  end

  # Public: Returns the discussion event's creation time as a Time.
  def occurred_at
    discussion_event.created_at
  end

  def eql?(other_contribution)
    other_contribution.respond_to?(:discussion_event) &&
      discussion_event == other_contribution.discussion_event
  end

  def self.subjects_for(user, date_range:, organization_id: nil, excluded_organization_ids: [])
    contribs = DiscussionEvent.for_comment_authored_by(user).still_marked_as_answer.
      where(created_at: buffered_time_range(date_range))
    contribs = contribs.for_organization(organization_id) if organization_id
    contribs = contribs.includes(:repository).preload(:comment, :discussion).limit(Contribution::DEFAULT_COUNT_LIMIT).to_a
    if excluded_organization_ids.any?
      contribs = contribs.reject do |event|
        excluded_organization_ids.include?(event.repository.organization_id)
      end
    end

    contribs.select do |event|
      GitHub.flipper[:discussions_for_repo_users].enabled?(event.repository)
    end
  end
end
