# frozen_string_literal: true

class Contribution
  module TimezoneFiltering
    TIMEZONE_BUFFER = 1.day

    private

    # Private: Some contribution types are filtered and ordered by a computed,
    # timezone aware value. We fetch these by created_at and then filter the
    # returned records by the computed field.
    #
    # This adds a buffer to the beginning and end of the time range so we get
    # all records that may match the time range considering timezone.
    #
    # date_range - A Range of Dates
    #
    # Returns a Range of Time objects.
    def buffered_time_range(date_range)
      from = date_range.begin - TIMEZONE_BUFFER
      to = date_range.end + TIMEZONE_BUFFER

      from.beginning_of_day..to.end_of_day
    end

    def date_range_to_time_range(date_range)
      date_range.begin.beginning_of_day..date_range.end.end_of_day
    end
  end
end
