# rubocop:disable Style/FrozenStringLiteralComment

# The Contribution::Calendar deals with a user's contributions over time.
class Contribution::Calendar
  include GitHub::AreasOfResponsibility
  areas_of_responsibility :user_profile

  DEFAULT_ZERO_COLOR = "#ebedf0".freeze
  HALLOWEEN_COLORS = %w[#ffee4a #ffc501 #fe9600 #03001c].freeze
  DEFAULT_COLORS = %w[#9be9a8 #40c463 #30a14e #216e39].freeze
  CACHE_VERSION = 2

  attr_reader :user, :viewer, :from, :to, :organization_id

  # Moment we switched to timezone aware contribution graphs
  TIMEZONE_AWARE_CUTOVER = Time.utc(2014, 3, 10)

  # Public: Return a time range that represents the span shown by a Calendar.
  #
  # to - Date or Time representing the end of the range.
  #
  # Returns a Range of Times.
  def self.time_range_ending_on(to)
    ends_on_last_day_of_year = to.end_of_year.to_date == to.to_date

    from = if ends_on_last_day_of_year
      to.beginning_of_year.beginning_of_week(:sunday)
    else
      (to - 1.year).beginning_of_week(:sunday)
    end

    from.beginning_of_day..to.end_of_day
  end

  # collector - The Contribution::Collector that is responsible for fetching data
  #             for this calendar. It should be scoped to a 1-year time range.
  def initialize(collector:)
    @user = collector.user
    @viewer = collector.viewer
    @time_range = collector.time_range
    @to = @time_range.end.to_date
    @from = @time_range.begin.to_date
    @organization_id = collector.organization_id
    @collector = collector
  end

  # Public: The login of the user this calendar represents.
  #
  # Returns a String.
  def user_login
    user.login
  end

  # Public: The count of total contributions in the calendar.
  # Returns an Integer.
  def total_contributions
    days.sum { |day, count| count }
  end

  # Public: Determine if the current time (not the from-to range for this calendar) is Halloween.
  #
  # Returns a Boolean.
  def halloween?
    current_time = Time.zone.now
    start  = Time.zone.local(current_time.year, 10, 31) # 2014-10-31 00:00:00
    finish = Time.zone.local(current_time.year, 11, 1) # 2014-11-01 00:00:00

    current_time >= start && current_time < finish
  end

  # Public: Returns a Quantile for the days in this calendar using the range of colors that suit
  # the current time (Halloween versus not).
  def quantile
    outliers = OutlierFilter.new
    counts = days.map { |pair| pair[1] }
    counts = outliers.filter(counts)
    Quantile.new(domain: [0, counts.max], range: colors)
  end

  # Public: Collection of daily contributions for a specific user based on the
  # viewer. Includes entries for all days even when they have 0 as a value.
  #
  # Returns an Array of [[day, count],...]
  def days
    @days ||= days!
  end

  def platform_type_name
    "ContributionCalendar"
  end

  def self.weeks_from(days, quantile:)
    calendar_days = days.map do |date, count|
      color = count.zero? ? DEFAULT_ZERO_COLOR : quantile.scale(count)
      ::Contribution::CalendarDay.new(date: date, count: count, color: color)
    end
    lists_of_squares = calendar_days.slice_before(&:sunday?).to_a

    max_weeks = 53 # SVG only has room for 53 columns representing a week each
    if lists_of_squares.size > max_weeks
      # Keep the most recent weeks
      lists_of_squares = lists_of_squares.drop(lists_of_squares.size - max_weeks)
    end

    lists_of_squares.map do |contrib_squares|
      ::Contribution::CalendarWeek.new(contrib_squares)
    end
  end

  def weeks
    self.class.weeks_from(days, quantile: quantile)
  end

  def self.months_from(weeks)
    weeks_by_month = weeks.group_by do |calendar_week|
      days = calendar_week.contribution_days
      earliest_day = days.first
      date = earliest_day.date
      date.beginning_of_month
    end

    weeks_by_month.map do |first_day, weeks|
      ::Contribution::CalendarMonth.new(first_day: first_day, total_weeks: weeks.size)
    end
  end

  def months
    self.class.months_from(weeks)
  end

  def async_url
    path = "/users/{login}/contributions?to={to}"
    template_args = { login: user_login, to: to.iso8601 }

    if organization_id
      org_promise = Platform::Loaders::ActiveRecord.load(::Organization, organization_id)
      org_promise.then do |org|
        if org
          path += "&org={org}"
          template_args[:org] = org.login
        end
        Addressable::Template.new(path).expand(template_args)
      end
    else
      Addressable::Template.new(path).expand(template_args)
    end
  end

  # Public: Colors for each weighted contribution on the contribution calendar.
  #
  # Returns an Array of String hex color codes.
  def colors
    if use_custom_colors?
      user_configured_colors
    elsif halloween?
      HALLOWEEN_COLORS
    else
      DEFAULT_COLORS
    end
  end

  private

  def use_custom_colors?
    return false unless GitHub.flipper[:config_repo].enabled?(viewer)
    return false unless user.has_configuration_repository?

    config_repo = user.configuration_repository
    return false unless config_repo.has_profile_configuration_file?

    profile_config = config_repo.profile_configuration_file
    profile_config.specifies_color?
  end

  def user_configured_colors
    config_repo = user.configuration_repository
    profile_config = config_repo.profile_configuration_file
    profile_config.colors
  end

  # Private: Collection of daily contributions for a specific
  # user based on the viewer.
  #
  # Returns an Array of [[day, count],...]
  def days!
    data = {}
    contribution_days = @collector.contribution_count_by_day

    end_date = @to

    if contribution_days.present?
      latest_contribution_date = contribution_days.keys.max

      if (latest_contribution_date - end_date).to_i <= 1 # days between
        end_date = [latest_contribution_date, end_date].max
      end
    end

    @from.upto(end_date) do |day|
      data[day] = contribution_days[day] || 0
    end

    data.sort_by { |date, contribs| date }
  end
end
