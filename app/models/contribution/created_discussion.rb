# frozen_string_literal: true

class Contribution::CreatedDiscussion < Contribution
  areas_of_responsibility :user_profile, :discussions

  delegate :repository, :repository_id, :comment_count, :title, :number,
    :async_repository, :hash, to: :discussion

  def discussion
    subject
  end

  def discussion_id
    subject.id
  end

  def associated_subject
    discussion.repository
  end

  def organization_id
    associated_subject.try(:organization_id)
  end

  # Public: Returns the discussion's creation time as a Time.
  def occurred_at
    discussion.created_at
  end

  def eql?(other_contribution)
    other_contribution.respond_to?(:discussion) && discussion == other_contribution.discussion
  end

  def self.subjects_for(user, date_range:, organization_id: nil, excluded_organization_ids: [])
    contribs = user.discussions.where(created_at: buffered_time_range(date_range))
    contribs = contribs.for_organization(organization_id) if organization_id
    contribs = contribs.includes(:repository).limit(Contribution::DEFAULT_COUNT_LIMIT)

    if excluded_organization_ids.any?
      contribs = contribs.select do |discussion|
        !excluded_organization_ids.include?(discussion.repository.organization_id)
      end
    end

    contribs.select do |discussion|
      GitHub.flipper[:discussions_for_repo_users].enabled?(discussion.repository)
    end
  end
end
