# frozen_string_literal: true

class ContentReferenceAttachment < ApplicationRecord::Ballast
  areas_of_responsibility :ce_extensibility

  # Use VARBINARY limit from the database
  TITLE_BYTESIZE_LIMIT = 1024

  include Workflow
  include GitHub::Validations
  include GitHub::UserContent

  belongs_to :content_reference
  belongs_to :integration

  extend GitHub::Encoding
  force_utf8_encoding :title, :body

  workflow :state do
    state :pending, 0 do
      event :processed, transitions_to: :processed
    end

    state :processed, 10 do
      event :hidden, transitions_to: :hidden
    end

    state :hidden, 20
  end

  delegate :content, :repository, to: :content_reference

  validates_presence_of :title, message: "cannot be blank"
  validates_presence_of :body, message: "cannot be blank"
  validates_presence_of :integration
  validates_presence_of :content_reference
  validates_uniqueness_of :integration, scope: :content_reference, message: "can only have one attachment per content reference"
  validates :title, bytesize: { maximum: TITLE_BYTESIZE_LIMIT }, unicode: true
  validates :body, bytesize: { maximum: MYSQL_UNICODE_BLOB_LIMIT },
            allow_blank: true, allow_nil: true
  validate :content_reference_cannot_be_too_old

  after_save :refresh_body_html
  after_create :instrument_create_via_hydo

  def self.for_integration_id(integration_id)
    where(integration_id: integration_id)
  end

  def refresh_body_html
    content.refresh_body_html
  end

  def content_reference_cannot_be_too_old
    if content_reference.created_at < 6.hours.ago
      errors.add(:content_reference, "is older than 6 hours")
    end
  end

  # Defining this method manually, because we want
  # the external type name to be "ContentAttachment"
  # not "ContentReferenceAttachment"
  def global_relay_id
    Platform::Helpers::NodeIdentification.to_global_id("ContentAttachment", id)
  end

  private

  def instrument_create_via_hydo
    GlobalInstrumenter.instrument "content_attachment.create", {
      integration: integration,
      content: content,
      title: title,
      body: body,
      repository: repository,
      content_reference_url: content_reference.reference,
    }
  end
end
