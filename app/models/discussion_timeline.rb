# frozen_string_literal: true

# Public: Represents a single discussion page in a repository. Used to memoize and batch load
# necessary checks and relations for rendering the discussion page.
class DiscussionTimeline
  UNREAD_MARKER = :unread_marker

  attr_reader :repository, :updated_at

  delegate :discussion, :viewer, :renderables, to: :render_context

  def self.last_modified_at_for(discussion:)
    [
      discussion.created_at,
      discussion.comments.last&.created_at,
      discussion.events.last&.created_at
    ].compact.max
  end

  def initialize(render_context:)
    @render_context = render_context
    @repository = discussion.repository
    @updated_at = Time.zone.now
  end

  def last_modified_at
    @last_modified_at ||= self.class.last_modified_at_for(discussion: discussion)
  end

  def chosen_comment_selected_by_user
    return @chosen_comment_selected_by_user if defined?(@chosen_comment_selected_by_user)
    @chosen_comment_selected_by_user = discussion.chosen_comment_selected_by_user
  end

  # Public: Is the given comment ID the ID of the first comment on the page that
  # the viewer could mark as the answer?
  #
  # comment_id - DiscussionComment ID, integer
  #
  # Returns a Boolean.
  def is_first_comment_markable_as_answer_on_the_page?(comment_id)
    target_comment = first_comment_that_could_be_marked_as_answer
    return false unless target_comment
    comment_id == target_comment.id
  end

  def old_repository_for_transfer_event(discussion_event)
    @old_repositories_for_transfer_events ||= DiscussionEvent.
      old_repositories_for_transfer_events(events: all_transfer_events, viewer: viewer)
    @old_repositories_for_transfer_events[discussion_event.id]
  end

  def answered?
    discussion.answered?
  end

  def question?
    return @discussion_is_question if defined?(@discussion_is_question)
    @discussion_is_question = discussion.question?
  end

  def discussion_number
    discussion.number
  end

  def locked_discussion?
    return @is_locked_discussion if defined?(@is_locked_discussion)
    @is_locked_discussion = discussion.locked_for?(viewer)
  end

  def frequent_helper_for?(discussion_or_comment)
    @frequent_helper_by_user_id ||= DiscussionComment.frequent_helper_by_user_id(discussion,
      actor: viewer)
    @frequent_helper_by_user_id[discussion_or_comment.user_id]
  end

  def last_reported_at_for(discussion_or_comment)
    if discussion_or_comment.is_a?(Discussion)
      last_reported_at_for_discussion
    else
      last_reported_at_for_comment(discussion_or_comment)
    end
  end

  def top_report_reason_for(discussion_or_comment)
    if discussion_or_comment.is_a?(Discussion)
      top_report_reason_for_discussion
    else
      top_report_reason_for_comment(discussion_or_comment)
    end
  end

  def report_count_for(discussion_or_comment)
    if discussion_or_comment.is_a?(Discussion)
      report_count_for_discussion
    else
      report_count_for_comment(discussion_or_comment)
    end
  end

  def last_edited_at_for(discussion_or_comment)
    latest_edit_for(discussion_or_comment)&.edited_at
  end

  def latest_edit_for(discussion_or_comment)
    if discussion_or_comment.is_a?(Discussion)
      latest_edit_for_discussion
    else
      latest_edit_for_comment(discussion_or_comment)
    end
  end

  def body_html_for(discussion_or_comment)
    if discussion_or_comment.is_a?(Discussion)
      discussion.body_html
    else
      body_html_for_comment(discussion_or_comment)
    end
  end

  def can_mark_answer?
    return @can_mark_answer if defined?(@can_mark_answer)
    @can_mark_answer = question? &&
      DiscussionComment.can_toggle_answer_in_discussion?(discussion, actor: viewer)
  end

  def render_mark_as_answer?(comment)
    can_mark_answer? && !comment.wiped? && !comment.minimized? && comment.top_level_comment?
  end

  def can_unmark_as_answer?(comment)
    return false unless question?
    @can_unmark_as_answer_by_comment_id ||= DiscussionComment.
      can_unmark_as_answer_by_comment_id(discussion, actor: viewer)
    @can_unmark_as_answer_by_comment_id[comment.id]
  end

  # Public: Can the viewer open an issue quoting the text from a discussion or a comment
  # within the discussion, in the same repository as the discussion?
  def can_open_issue_from_discussion?
    return @can_open_issue_from_discussion if defined?(@can_open_issue_from_discussion)
    @can_open_issue_from_discussion = if viewer
      if repository.has_issues?
        if repository.issue_templates.any?
          permission = repository.access_level_for(viewer)
          viewer_permission = permission ? permission.to_s : nil
          repository.issue_templates.any? && ["admin", "write"].include?(viewer_permission)
        else
          true
        end
      else
        # Repos with issues turned off can't have new issues opened
        false
      end
    else
      # Anonymous users can't open issues
      false
    end
  end

  def show_edit_button_requiring_email_verification?(discussion_or_comment)
    return false unless viewer_did_author?(discussion_or_comment)

    if defined?(@show_edit_button_requiring_email_verification)
      return @show_edit_button_requiring_email_verification
    end
    @show_edit_button_requiring_email_verification = viewer.should_verify_email?
  end

  def can_toggle_minimize?
    return @can_toggle_minimize if defined?(@can_toggle_minimize)
    @can_toggle_minimize = DiscussionComment.
      can_toggle_minimized_discussion_comment?(discussion, actor: viewer)
  end

  def discussion_channel
    GitHub::WebSocket::Channels.discussion(discussion)
  end

  def discussion_timeline_channel
    GitHub::WebSocket::Channels.discussion_timeline(discussion)
  end

  def child_comments(parent_comment)
    # All the comments for the whole discussion are loaded, just filter in
    # memory to those that are nested under the given comment.
    @child_comments ||= all_comments.reject(&:top_level_comment?).group_by(&:parent_comment_id)
    @child_comments[parent_comment.id] || []
  end

  def discussion_graphql_id
    discussion.global_relay_id
  end

  def display_commenter_full_name?(discussion_or_comment)
    !viewer_did_author?(discussion_or_comment) && show_commenter_full_name?
  end

  def repo_pushable?
    return @repo_pushable if defined?(@repo_pushable)
    @repo_pushable = viewer && repository.pushable_by?(viewer)
  end

  def can_comment?
    return @can_comment if defined?(@can_comment)
    @can_comment = discussion.can_comment?(viewer)
  end

  def can_reply_to_comment?(comment)
    return false unless comment.is_a?(DiscussionComment)
    return false unless can_comment?
    return false if locked_discussion?
    return false if blocked_from_commenting?
    comment.can_be_commented_on?
  end

  def blocked_from_commenting?
    return @blocked_from_commenting if defined?(@blocked_from_commenting)
    @blocked_from_commenting = if viewer
      if private_repo?
        false
      else
        if repo_pushable?
          false
        else
          if repository.owner_id.nil? || repository.owner_id == viewer.id
            false
          else
            viewer.blocked_by?(repository.owner) || viewer.blocked_by?(discussion.user)
          end
        end
      end
    else
      false
    end
  end

  def archived_repo?
    repository.archived?
  end

  def private_repo?
    repository.private?
  end

  def repo_name
    repository.name
  end

  def repo_owner
    repository.owner
  end

  def viewer_relationship_to_discussion
    viewer_relationship_to_discussion ||= discussion.async_viewer_relationship(viewer).sync
  end

  def repo_author_association_for(discussion_or_comment)
    if discussion_or_comment.is_a?(Discussion)
      repo_author_association_for_discussion
    else
      repo_author_association_for_comment(discussion_or_comment)
    end
  end

  def can_unblock?(discussion_or_comment)
    if discussion_or_comment.is_a?(Discussion)
      can_unblock_discussion?
    else
      can_unblock_comment?(discussion_or_comment)
    end
  end

  def can_block?(discussion_or_comment)
    if discussion_or_comment.is_a?(Discussion)
      can_block_discussion?
    else
      can_block_comment?(discussion_or_comment)
    end
  end

  def can_report_to_maintainer?(discussion_or_comment)
    if discussion_or_comment.is_a?(Discussion)
      can_report_discussion_to_maintainer?
    else
      can_report_comment_to_maintainer?(discussion_or_comment)
    end
  end

  def can_report?(discussion_or_comment)
    if discussion_or_comment.is_a?(Discussion)
      can_report_discussion?
    else
      can_report_comment?(discussion_or_comment)
    end
  end

  def can_delete?(discussion_or_comment)
    if discussion_or_comment.is_a?(Discussion)
      can_delete_discussion?
    else
      can_delete_comment?(discussion_or_comment)
    end
  end

  def can_update?(discussion_or_comment)
    if discussion_or_comment.is_a?(Discussion)
      can_update_discussion?
    else
      can_update_comment?(discussion_or_comment)
    end
  end

  def can_manage_spotlights?
    return @can_manage_spotlights if defined?(@can_manage_spotlights)
    @can_manage_spotlights = viewer&.can_manage_discussion_spotlights?(repository)
  end

  def can_update_discussion?
    return @can_update_discussion if defined?(@can_update_discussion)
    @can_update_discussion = discussion.modifiable_by?(viewer)
  end

  def viewer_did_author?(discussion_or_comment)
    viewer && discussion_or_comment.user == viewer
  end

  def authored_by_subject_author?(discussion_or_comment)
    discussion_or_comment.is_a?(DiscussionComment) &&
      discussion.user_id == discussion_or_comment.user_id
  end

  # Public: Is the author of the given discussion or comment a contributor to the repository
  # containing that discussion or comment?
  def author_is_contributor?(discussion_or_comment)
    contributor_user_ids.include?(discussion_or_comment.user_id)
  end

  def can_react?(discussion_or_comment)
    if discussion_or_comment.is_a?(Discussion)
      can_react_to_discussion?
    else
      can_react_to_comment?(discussion_or_comment)
    end
  end

  def reaction_groups(discussion_or_comment)
    if discussion_or_comment.is_a?(Discussion)
      discussion_reaction_groups
    else
      comment_reaction_groups(discussion_or_comment)
    end
  end

  def reaction_groups_with_reactions(discussion_or_comment)
    reaction_groups(discussion_or_comment).select do |group|
      group.total_count > 0
    end
  end

  def reaction_group_user_logins(discussion_or_comment, reaction_group)
    if discussion_or_comment.is_a?(Discussion)
      discussion_reaction_group_user_logins(reaction_group)
    else
      comment_reaction_group_user_logins(discussion_or_comment, reaction_group)
    end
  end

  def all_timeline_items
    render_context.timeline_items
  end

  def action_or_role_level_for(discussion_or_comment)
    if discussion_or_comment.is_a?(Discussion)
      action_or_role_level_for_discussion
    else
      action_or_role_level_for_comment(discussion_or_comment)
    end
  end

  private

  attr_reader :render_context

  def contributor_user_ids
    @contributor_user_ids ||= DiscussionContributorFinder.contributor_user_ids_for(
      repository: discussion.repository,
      user_ids: [discussion.user_id] + all_comments.map(&:user_id),
      viewer: viewer,
    )
  end

  def can_update_comment?(comment)
    @modifiable_by_comment_id ||= DiscussionComment.modifiable_by_comment_id(
      discussion,
      actor: viewer,
      comment_ids: all_comments.map(&:id),
    )
    @modifiable_by_comment_id[comment.id]
  end

  def action_or_role_level_for_discussion
    return @action_or_role_level_for_discussion if defined?(@action_or_role_level_for_discussion)
    @action_or_role_level_for_discussion = if discussion.user
      repository.async_action_or_role_level_for(discussion.user).sync
    end
  end

  def action_or_role_level_for_comment(comment)
    @action_or_role_level_by_comment_id ||= DiscussionComment.
      action_or_role_level_by_comment_id(discussion, actor: viewer, comments: all_comments)
    @action_or_role_level_by_comment_id[comment.id]
  end

  def repo_author_association_for_discussion
    @repo_author_association_for_discussion ||= CommentAuthorAssociation.new(
      comment: discussion, viewer: viewer,
    ).to_sym
  end

  def repo_author_association_for_comment(comment)
    @repo_author_associations_by_comment_id ||=
      DiscussionComment.repository_author_associations_by_comment_id(discussion,
        actor: viewer, comments: all_comments)
    @repo_author_associations_by_comment_id[comment.id]
  end

  def can_unblock_discussion?
    return @can_unblock_discussion if defined?(@can_unblock_discussion)
    @can_unblock_discussion = discussion.viewer_can_unblock?(viewer)
  end

  def can_unblock_comment?(comment)
    @unblockable_by_comment_id ||= DiscussionComment.unblockable_by_comment_id(discussion,
      actor: viewer, comments: all_comments)
    @unblockable_by_comment_id[comment.id]
  end

  def can_block_discussion?
    return @can_block_discussion if defined?(@can_block_discussion)
    @can_block_discussion = discussion.viewer_can_block?(viewer)
  end

  def can_block_comment?(comment)
    @blockable_by_comment_id ||= DiscussionComment.blockable_by_comment_id(discussion,
      actor: viewer, comments: all_comments)
    @blockable_by_comment_id[comment.id]
  end

  def can_report_discussion_to_maintainer?
    return @can_report_discussion_to_maintainer if defined?(@can_report_discussion_to_maintainer)
    @can_report_discussion_to_maintainer = discussion.
      async_viewer_can_report_to_maintainer(viewer).sync && !discussion.viewer_can_moderate_content?(viewer)
  end

  def can_report_comment_to_maintainer?(comment)
    @reportable_to_maintainer_by_comment_id ||= DiscussionComment.
      reportable_to_maintainer_by_comment_id(discussion, actor: viewer, comments: all_comments)
    @reportable_to_maintainer_by_comment_id[comment.id]
  end

  def can_report_discussion?
    return @can_report_discussion if defined?(@can_report_discussion)
    @can_report_discussion = discussion.async_viewer_can_report(viewer).sync
  end

  def can_report_comment?(comment)
    @reportable_by_comment_id ||= DiscussionComment.reportable_by_comment_id(discussion,
      actor: viewer, comments: all_comments)
    @reportable_by_comment_id[comment.id]
  end

  def can_react_to_discussion?
    return @can_react_to_discussion if defined?(@can_react_to_discussion)
    @can_react_to_discussion = DiscussionReaction.async_viewer_can_react?(viewer, discussion).sync
  end

  def can_react_to_comment?(comment)
    @can_react_by_comment_id ||= DiscussionComment.can_react_by_comment_id(discussion,
      actor: viewer, comments: all_comments)
    @can_react_by_comment_id[comment.id]
  end

  def can_delete_comment?(comment)
    @deletable_by_comment_id ||= DiscussionComment.deletable_by_comment_id(discussion,
      actor: viewer)
    @deletable_by_comment_id[comment.id]
  end

  def show_commenter_full_name?
    return @show_commenter_full_name if defined?(@show_commenter_full_name)
    @show_commenter_full_name = if discussion.in_organization?
      repo_owner.display_commenter_full_name_for_repo?(
        visibility: repository.visibility.to_sym, viewer: viewer,
      )
    else
      false
    end
  end

  def body_html_for_comment(comment)
    @body_html_by_comment_id ||= DiscussionComment.body_html_by_comment_id(discussion,
      actor: viewer, comments: all_comments)
    @body_html_by_comment_id[comment.id]
  end

  def latest_edit_for_discussion
    return @latest_edit_for_discussion if defined?(@latest_edit_for_discussion)
    @latest_edit_for_discussion = discussion.async_latest_user_content_edit.sync
  end

  def latest_edit_for_comment(comment)
    @latest_edit_by_comment_id ||= DiscussionComment.latest_edit_by_comment_id(discussion,
      actor: viewer, comments: all_comments)
    @latest_edit_by_comment_id[comment.id]
  end

  def last_reported_at_for_discussion
    return @last_reported_at_for_discussion if defined?(@last_reported_at_for_discussion)
    @last_reported_at_for_discussion = discussion.last_reported_at
  end

  def last_reported_at_for_comment(comment)
    @last_reported_at_by_comment_id ||= DiscussionComment.last_reported_at_by_comment_id(discussion,
      actor: viewer)
    @last_reported_at_by_comment_id[comment.id]
  end

  def top_report_reason_for_discussion
    return @top_report_reason_for_discussion if defined?(@top_report_reason_for_discussion)
    @top_report_reason_for_discussion = discussion.top_report_reason
  end

  def top_report_reason_for_comment(comment)
    @top_report_reason_by_comment_id ||= DiscussionComment.top_report_reason_by_comment_id(discussion,
      actor: viewer)
    @top_report_reason_by_comment_id[comment.id]
  end

  def report_count_for_discussion
    @report_count_for_discussion ||= discussion.report_count || 0
  end

  def report_count_for_comment(comment)
    @report_count_by_comment_id ||= DiscussionComment.report_count_by_comment_id(discussion,
      actor: viewer, comments: all_comments)
    @report_count_by_comment_id[comment.id]
  end

  def discussion_reaction_groups
    @discussion_reaction_groups ||= discussion.reaction_groups
  end

  def comment_reaction_groups(comment)
    reaction_groups_by_comment_id[comment.id]
  end

  def reaction_groups_by_comment_id
    @reaction_groups_by_comment_id ||= DiscussionComment.reaction_groups_by_comment_id(
      discussion,
      actor: viewer,
      comments: all_comments,
    )
  end

  def discussion_reaction_group_user_logins(reaction_group)
    user_logins_by_reaction_group_content = Discussion::ReactionGroup
      .user_logins_by_comment_id_by_reaction_group_content(
        {discussion: discussion_reaction_groups},
      )[:discussion]

    user_logins_by_reaction_group_content[reaction_group.content] || []
  end

  def comment_reaction_group_user_logins(comment, reaction_group)
    @user_logins_by_comment_id_by_reaction_group_content ||= Discussion::ReactionGroup
      .user_logins_by_comment_id_by_reaction_group_content(
        reaction_groups_by_comment_id,
      )
    user_logins_by_reaction_group_content = @user_logins_by_comment_id_by_reaction_group_content[comment.id]

    return [] if user_logins_by_reaction_group_content.nil?

    user_logins_by_reaction_group_content[reaction_group.content] || []
  end

  def all_comments
    return @_all_comments if @_all_comments

    top_level_comments = render_context.timeline_items.select do |item|
      item.is_a?(DiscussionComment)
    end
    child_comments = Promise.all(top_level_comments.map(&:async_comments)).sync.flatten

    @_all_comments = top_level_comments + child_comments
  end

  def all_transfer_events
    @all_transfer_events ||= render_context.timeline_items.select do |item|
      item.is_a?(DiscussionEvent) && item.transferred?
    end
  end

  def comments_that_could_be_marked_as_answers
    all_timeline_items.select do |item|
      item.is_a?(DiscussionComment) && render_mark_as_answer?(item)
    end
  end

  def first_comment_that_could_be_marked_as_answer
    if defined?(@first_comment_that_could_be_marked_as_answer)
      return @first_comment_that_could_be_marked_as_answer
    end
    @first_comment_that_could_be_marked_as_answer =
      comments_that_could_be_marked_as_answers.first
  end
end
