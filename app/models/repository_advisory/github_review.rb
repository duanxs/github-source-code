# frozen_string_literal: true

# A small value class that summarizes the state of our internal Vulnerability
# objects for a given RepositoryAdvisory and provides an interface for our
# review code to update the Advisory
#
# This class will be deprecated as Advisory review moves to the standalone
# Advisory DB service
class RepositoryAdvisory::GitHubReview
  def self.for(advisory)
    new(advisory)
  end

  def self.for_id(advisory_id)
    new RepositoryAdvisory.find(advisory_id)
  end

  def initialize(advisory)
    @advisory = advisory
    @pending_vulnerability = PendingVulnerability.preload(:vulnerability)
                                                 .find_by(source: RepositoryAdvisory::SOURCE_IDENTITY,
                                                          source_identifier: advisory.id)
  end

  # Provides a simplified masking of our internal review states, limited to:
  # - published
  # - withdrawn
  # - rejected
  # - pending
  #
  # returns nil if the advisory has not been submitted for review
  def status
    return unless @pending_vulnerability
    return "rejected" if @pending_vulnerability.status == "rejected"

    vulnerability&.status || "pending"
  end

  def withdrawn_by
    return unless status == "withdrawn"

    vulnerability&.withdrawn_by
  end

  def set_published(actor:)
    @advisory.add_review_state_event(actor, "github_published")
  end

  def set_rejected(actor:)
    @advisory.add_review_state_event(actor, "github_rejected")
  end

  def set_withdrawn(actor:)
    @advisory.add_review_state_event(actor, "github_withdrawn")
  end

  private

  def vulnerability
    @pending_vulnerability&.vulnerability
  end
end
