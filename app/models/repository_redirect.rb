# rubocop:disable Style/FrozenStringLiteralComment

class RepositoryRedirect < ApplicationRecord::Domain::Repositories
  belongs_to :repository, -> { where("active = 1") }

  REPOSITORY_NAME_FORMAT = %r{(?:[\w\.\-])+/(?:[\w\.\-])+}i
  validates_format_of :repository_name, with: REPOSITORY_NAME_FORMAT

  # Public: Find the new repository for a given name.
  #
  # name - The repository name with owner like "defunkt/dotjs".
  #
  # Returns a Repository object or nil when no redirect exists.
  def self.find_redirected_repository(name)
    return if name.blank? # no need to query for a non-nullable column
    return unless name.valid_encoding?
    return if name !~ REPOSITORY_NAME_FORMAT # skip anything that's not valid anyway
    redirect = joins(:repository).
      where(repository_name: name).
      order("repository_redirects.created_at DESC").
      first
    redirect && redirect.repository
  end

  # Public: Find the new repository for a given old owner name and network
  #
  # name       - The old owner name
  # repository - The repository whose network to search
  #
  # Returns a Repository object or nil when no redirect exists.
  def self.find_networked_by_old_owner(name, repository)
    redirect = joins(:repository).
      with_prefix("repository_redirects.repository_name", "#{name}/").
      where("repositories.source_id = ?", repository.network_id).
      order("created_at DESC").
      first
    redirect && redirect.repository
  end
end
