# frozen_string_literal: true

class CodeScanningAlert < ApplicationRecord::Ballast
  belongs_to :check_annotation
  belongs_to :check_run
  belongs_to :repository

  scope :for_annotations, ->(annotations) { where(check_annotation: annotations) }

  # Get the distinct set of check run ids for the given alert numbers and
  # repository.
  #
  # Returns an array of check run ids.
  def self.check_run_ids(alert_numbers:, repository:)
    where(repository: repository).where(alert_number: alert_numbers).pluck(Arel.sql("DISTINCT check_run_id"))
  end

  # Load a set of Turboscan results for the alerts associated with given
  # annotation ids.
  #
  # Returns a hash where the keys are check annotation ids and the values are
  # alert results from Turboscan.
  def self.results_by_id(repository:, annotations:)
    # Build a hash of alert numbers to check annotation ids
    alerts = for_annotations(annotations).pluck(:alert_number, :check_annotation_id).to_h
    return {} if alerts.empty?

    # Fetch all the annotation results from turboscan for the alert numbers
    results = GitHub::Turboscan.annotations(
      repository_id: repository.id,
      numbers: alerts.keys,
    )&.data&.results || []
    return {} if results.empty?

    # Build a hash of check annotation ids to results using the alerts hash
    results_by_id = {}
    results.each do |result|
      annotation_id = alerts[result.result.number]
      results_by_id[annotation_id] = result if annotation_id.present?
    end
    results_by_id
  end
end
