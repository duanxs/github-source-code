# frozen_string_literal: true

class CustomRoleError < StandardError; end

class Role < ApplicationRecord::Iam
  include Instrumentation::Model

  extend GitHub::Encoding
  force_utf8_encoding :name, :description

  RESERVED_NAMES = %w(
    triage
    maintain
    read
    write
    admin
    )

  CODESPACES_SYSTEM_ROLES = %w(codespace_org_creator)

  PACKAGES_SYSTEM_ROLES = %w(
    package_reader
    package_writer
    package_admin
  )
  SYSTEM_ROLES = RESERVED_NAMES + PACKAGES_SYSTEM_ROLES + CODESPACES_SYSTEM_ROLES

  NONBILLABLE_ROLES = PACKAGES_SYSTEM_ROLES
  USER_OWNED_ROLES = PACKAGES_SYSTEM_ROLES

  BUSINESS_PLUS_CUSTOM_ROLE_LIMIT = 3

  BASE_ACTION_FOR_ROLE = { read: :triage, write: :maintain }
  VALID_REPO_BASE_ROLES = %w(read triage write maintain)
  PRIMARY_REPO_BASE_ROLES = %w(read write admin)
  BASE_ROLE_TO_ABILITY = {
    "read": :read,
    "triage": :read,
    "write": :write,
    "maintain": :write,
  }.with_indifferent_access

  validates :name, presence: true, uniqueness: { scope: [:owner_id, :owner_type], case_sensitive: true }, length: { maximum: 62,
  too_long: "cannot be longer than %{count} characters" }
  validates :owner, presence: true, unless: :preset?
  validate :disallow_reserved_names
  validate :org_limit_for_roles, on: :create
  validate :can_create_custom_roles
  validate :valid_base_role
  validate :description_length

  # owner can be an organization or repo
  belongs_to :owner, polymorphic: true
  has_many :user_roles, dependent: :destroy
  has_many :permissions, class_name: "RolePermission", dependent: :destroy
  belongs_to :base_role, class_name: "Role", foreign_key: "base_role_id"

  scope :presets, -> { where(owner_type: nil, owner_id: nil) }
  scope :custom_roles_for_org, ->(org) { where(owner_type: "Organization", owner_id: org&.id) }
  scope :system_package_roles, -> { where("name IN (?)", PACKAGES_SYSTEM_ROLES) }
  scope :system_repo_roles, -> { where("name IN (?)", RESERVED_NAMES) }
  scope :primary_system_repo_roles, -> { where("name IN (?)", PRIMARY_REPO_BASE_ROLES) }

  after_create_commit  :instrument_create
  after_destroy_commit :instrument_destroy
  after_update_commit :instrument_update
  before_update :old_role_permissions

  # Public: The preset read role.
  #
  # Returns a Role.
  def self.read_role
    async_read_role.sync
  end

  # Public: The preset triage role.
  #
  # Returns a Role.
  def self.triage_role
    async_triage_role.sync
  end

  # Public: The preset write role.
  #
  # Returns a Role.
  def self.write_role
    async_write_role.sync
  end

  # Public: The preset maintain role.
  #
  # Returns a Role.
  def self.maintain_role
    async_maintain_role.sync
  end

  # Public: The preset admin role.
  #
  # Returns a Role.
  def self.admin_role
    async_admin_role.sync
  end

  # Public: The preset read role.
  #
  # Returns a Promise<Role>.
  def self.async_read_role
    Platform::Loaders::Permissions::PresetRole.load(name: "read")
  end

  # Public: The preset triage role.
  #
  # Returns a Promise<Role>.
  def self.async_triage_role
    Platform::Loaders::Permissions::PresetRole.load(name: "triage")
  end

  # Public: The preset write role.
  #
  # Returns a Promise<Role>.
  def self.async_write_role
    Platform::Loaders::Permissions::PresetRole.load(name: "write")
  end

  # Public: The preset maintain role.
  #
  # Returns a Promise<Role>.
  def self.async_maintain_role
    Platform::Loaders::Permissions::PresetRole.load(name: "maintain")
  end

  # Public: The preset admin role.
  #
  # Returns a Promise<Role>.
  def self.async_admin_role
    Platform::Loaders::Permissions::PresetRole.load(name: "admin")
  end

  # Public: The number of custom roles a given organization can create.
  #
  # - org: the Organization object.
  #
  # Returns an Integer.
  def self.custom_role_limit_for_org(org)
    case org.plan
    when GitHub::Plan.business_plus
      BUSINESS_PLUS_CUSTOM_ROLE_LIMIT
    else
      0
    end
  end

  def self.for_repo_owner(name:, repo:)
    return unless repo && repo.owner.organization?
    Role.find_by(name: name, owner_id: repo.owner_id, owner_type: "Organization")
  end

  def self.for_org(name:, org:)
    return unless org.organization?
    Role.find_by(name: name, owner_id: org.id, owner_type: "Organization")
  end

  # Public: is the role a valid custom role for the repository
  def self.org_repo_custom_role?(name:, repo:)
    !!for_repo_owner(name: name, repo: repo)
  end

  # or

  def self.valid_system_role?(role_name)
    return false unless role_name
    (RESERVED_NAMES + ["push", "pull"]).include? role_name.to_s
  end

  # Public: is the first role greater than the other?
  #
  # Returns a Boolean
  def self.target_greater_than_other_role?(target:, other_role:)
    Ability::ACTION_RANKING.fetch(target&.to_sym, 0) >= Ability::ACTION_RANKING.fetch(other_role, 0)
  end

  # Get ids of custom roles whose base roles have a lower rank than param action.
  #
  # action - one of the primary system roles
  #
  # Returns array of custom role ids
  def self.lower_custom_role_ids(action:, org:)
    lower_custom_roles(action: action, org: org).pluck(:id)
  end

  # Get the custom roles whose base roles have a lower rank than param action.
  #
  # action       - a PRIMARY_REPO_BASE_ROLES
  # organization - the target Organization
  #
  # Returns array of custom role ids
  def self.lower_custom_roles(action:, org:)
    return Role.none if action == "none" || action.nil?

    default_action_ranking = Ability::ACTION_RANKING.fetch(action.to_sym, 0)
    lower_roles = Ability::ACTION_RANKING.select { |role, ranking| ranking < default_action_ranking }.keys

    lower_role_ids = Role.presets.where(name: lower_roles).pluck(:id)
    Role.where(owner_id: org.id, owner_type: "Organization", base_role_id: lower_role_ids)
  end

  # Public: is the first role greater than the other?
  #
  # Returns a Boolean
  def target_greater_than_other_role?(other_role:)
    target = custom? ? base_role.name : self.name
    Ability::ACTION_RANKING.fetch(target&.to_sym, 0) >= Ability::ACTION_RANKING.fetch(other_role, 0)
  end

  # Public: grant the legacy ability
  def self.grant_legacy_permissions(actor, ability, target)
    Ability.grant(actor, ability, target)
  end

  def self.target_less_than_org_default_role?(target:, org:)
    return unless org.organization?

    role = for_org(name: target, org: org)
    target_ability = role.custom? ? role.base_role.name : role.name
    default_perm = org.default_repository_permission
    Ability::ACTION_RANKING[target_ability.to_sym] < Ability::ACTION_RANKING.fetch(default_perm, 0)
  end

  def self.greater_or_equal_to_org_default_role?(target:, org:)
    !target_less_than_org_default_role?(target: target, org: org)
  end

  def self.base_role_for(name)
    Role.find_by(name: name)&.base_role
  end

  def valid_org_role?(org)
    Role.custom_roles_for_org(org).include? self
  end

  # Public: Is this role one of the preset roles that we provide?
  #
  # Returns a Boolean.
  def preset?
    return false unless owner_type.nil? && owner_id.nil?
    SYSTEM_ROLES.include?(name)
  end

  # Public: Is this role a custom role?
  #
  # Returns a Boolean.
  def custom?
    !preset? && base_role.present?
  end

  # Public: the fine grained permissions that the role explicitly has
  # which are available for custom roles.
  #
  # Returns an ActiveRecord::Relation of role_permissions.
  def custom_role_permissions
    permissions.custom_roles_enabled
  end

  # Public: Is this role the preset read role?
  #
  # Returns a Boolean.
  def read?
    return false unless preset?
    name == "read"
  end

  # Public: Is this role the preset triage role?
  #
  # Returns a Boolean.
  def triage?
    return false unless preset?
    name == "triage"
  end

  # Public: Is this role the preset write role?
  #
  # Returns a Boolean.
  def write?
    return false unless preset?
    name == "write"
  end

  # Public: Is this role the preset maintain role?
  #
  # Returns a Boolean.
  def maintain?
    return false unless preset?
    name == "maintain"
  end

  # Public: Is this role the preset admin role?
  #
  # Returns a Boolean.
  def admin?
    return false unless preset?
    name == "admin"
  end

  # Public: Does this role require a special plan type?
  #
  # Returns a Boolean.
  def billable?
    !preset? || !NONBILLABLE_ROLES.include?(name)
  end

  # Public: Does this role require a target owner of type Organization?
  #
  # Returns a Boolean.
  def owner_must_be_organization?
    !preset? || !USER_OWNED_ROLES.include?(name)
  end

  # Public: The ids of the users assigned the role.
  #
  # Returns an Array of ids.
  def user_ids
    return @user_ids if defined?(@user_ids)
    @user_ids = user_roles.where(actor_type: "User").pluck(:actor_id)
  end

  # Public: The ids of the org members assigned the role.
  #
  # Returns an Array of ids.
  def org_member_ids
    return @org_member_ids if defined?(@org_member_ids)
    return [] unless owner.organization?

    org_member_ids = owner.direct_member_ids
    @org_member_ids = user_ids & org_member_ids
  end

  def collaborator_ids
    return @collab_ids if defined?(@collab_ids)
    return [] unless owner.organization?

    @collab_ids = user_ids - org_member_ids
  end

  # Public: The number of users assigned the role.
  #
  # Returns an Int.
  def user_count
    user_ids.size
  end

  # Public: The number of org members assigned the role.
  #
  # Returns an Int.
  def org_member_count
    org_member_ids.size
  end

  # Public: The number of outside collaborators assigned the role.
  #
  # Returns an Int.
  def collaborator_count
    collaborator_ids.size
  end

  # Public: The number of teams assigned a specific role.
  #
  # Returns an Int.
  def team_count
    return @team_count if defined?(@team_count)
    @team_count = user_roles.where(actor_type: "Team").size
  end

  # Public: Returns the action rank for this role.
  #
  # Returns a Float.
  def action_rank
    Ability::ACTION_RANKING[name.to_sym] || Ability::ACTION_RANKING[base_role&.name&.to_sym]
  end

  # Public: Does the role have a non nil base_role_id
  #
  # Returns a Boolean
  def has_base_role?
    base_role_id?
  end

  def implicit_fgps
    return [] unless base_role
    @implicit_fgps ||= base_role.permissions.pluck(:action)
  end

  # Public: Can this custom role be assigned, based on if the
  # role belongs to the org, and if it's above the org's default role level
  #
  # Returns a Boolean
  def custom_role_assignable?(repo:)
    return false unless owner.organization? && repo.owner == owner
    default_perm = owner.default_repository_permission

    target_greater_than_other_role?(other_role: default_perm)
  end

  # Public: Checks if custom role dependencies are deleted
  #
  # Returns a Boolean.
  def all_dependencies_updated?
    raise CustomRoleError.new("not a custom role") unless custom?
    !(
      UserRole.where(actor_type: ["User", "Team"], target_type: "Repository", role_id: id).exists? ||
      RepositoryInvitation.where(role_id: id).exists?
    )
  end

  private

  def disallow_reserved_names
    return if preset?
    errors.add(:name, "is a reserved role name") if (RESERVED_NAMES.include?(name) || %w(push pull).include?(name))
  end

  def org_limit_for_roles
    return if preset?
    return unless owner

    custom_role_count = Role.custom_roles_for_org(owner).count
    custom_role_limit = Role.custom_role_limit_for_org(owner)

    if owner.plan_supports?(:custom_roles) && custom_role_count >= custom_role_limit
      errors.add(:owner, "is already at the maximum number of custom roles")
    end
  end

  def can_create_custom_roles
    return if preset?
    return if owner&.plan_supports?(:custom_roles)
    errors.add(:owner, "does not have the correct plan to create custom roles")
  end

  def valid_base_role
    return if preset?
    base_role = Role.find_by(id: base_role_id)
    return if base_role && VALID_REPO_BASE_ROLES.include?(base_role&.name)
    errors.add(:base_role_id, "selection is invalid")
  end

  def description_length
    return unless Role.has_attribute?(:description)

    field_length = 152
    if description && description.length > field_length
      errors.add(:description, "cannot be longer than #{field_length} characters")
    end
  end

  def instrument_create
    instrument :create
  end

  def instrument_destroy
    instrument :destroy
  end

  def instrument_update
    changes_payload = {}.tap do |hash|
      hash[:old_name] = previous_changes["name"].first if name_changed?
      hash[:old_role_permissions] = permissions_descriptions(old_role_permissions) if permissions_changed?
      hash[:old_base_role] = old_base_role if previous_changes["base_role_id"].present?
    end

    return if changes_payload.empty?

    instrument :update, changes: changes_payload
  end

  def event_payload
    payload = {
      name: name,
      owner: owner&.login,
      role_permissions: permissions_descriptions(permissions.pluck(:action)),
      base_role: base_role&.name,
    }

    if owner.is_a? Organization
      payload[:org] = owner
      payload[:business] = owner&.business
    end

    payload
  end

  # Internal: the explicit FGPs for this role prior to being updated.
  # Note that this method is called before updating the object.
  #
  # Returns an Array of strings
  def old_role_permissions
    return [] unless custom?
    @old_fgps ||= permissions.pluck(:action)
  end

  def permissions_changed?
    old_role_permissions != permissions.pluck(:action)
  end

  def name_changed?
    return false unless previous_changes["name"].present?
    old, new = previous_changes["name"]
    # we take the binary difference because emoji's not yet persisted will not be binary encoded
    old.b != new.b
  end

  # Internal: the name of the base role prior to being updated.
  #
  # Returns a string
  def old_base_role
    Role.find_by(id: previous_changes["base_role_id"].first)&.name
  end

  # Internal: the human readable sentence of a set of fine grained permissions.
  #
  # - fgps: Array of strings or symbols of the fine grained permission identifiers.
  #
  # Returns a string
  def permissions_descriptions(fgps)
    return "None" if fgps.empty?

    descriptions = fgps.map { |fgp| FgpMetadata.description_for(fgp.to_sym) }
    descriptions.to_sentence
  end
end
