# frozen_string_literal: true

# Internal: ActiveRecord class to persist the onboarding events.
class Onboarding::Event < ApplicationRecord::Domain::Users
  include Instrumentation::Model
  self.table_name = :onboarding_events

  VALID_EVENTS = %w[
    enrolled_in_welcome_series
    welcomed_via_email
    answered_user_identification_questions
    enrolled_in_team_admin_onboarding_series
  ]

  belongs_to :user
  validates_presence_of :user_id, :name
  validates :name, inclusion: { in: VALID_EVENTS }
  validates_uniqueness_of :name, scope: [:user_id], case_sensitive: true

  after_commit :instrument_name, on: :create

  private

  def event_prefix
    :onboarding
  end

  def event_payload
    {
      user: user.login,
      user_id: user.id,
      event: name,
    }
  end

  def instrument_name
    instrument name
  end
end
