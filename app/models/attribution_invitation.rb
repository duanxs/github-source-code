# frozen_string_literal: true

class AttributionInvitation < ApplicationRecord::Collab
  areas_of_responsibility :migration

  validate :source_and_target_both_in_owner_org

  include Workflow

  belongs_to :source, class_name: "Mannequin"
  belongs_to :target, class_name: "User"
  belongs_to :creator, class_name: "User"
  belongs_to :owner, class_name: "Organization"

  workflow :state do
    state :invited, 0 do
      event :accept, transitions_to: :accepted
      event :reject, transitions_to: :rejected
      event :cancel, transitions_to: :canceled
    end

    state :accepted, 1 do
      event :complete, transitions_to: :completed
    end
    state :rejected, 2

    state :completed, 3
    state :canceled, 4
  end

  after_create :send_invitation_email

  def accept(*args, **kwargs)
    RewriteMannequinAssociationsJob.perform_later(source, target, self)
  end

  def display_state
    current_state.to_s.titleize
  end

  def cannot_accept_reason
    case current_state.name
    when :accepted, :completed
      "That attribution invitation has already been accepted"
    when :rejected
      "That attribution invitation could not be accepted "\
        "because it was already rejected"
    when :canceled
      "That attribution invitation could not be accepted "\
        "because it has been canceled."
    end
  end

  def cannot_reject_reason
    case current_state.name
    when :rejected
      "That attribution invitation has already been rejected"
    when :accepted, :completed
      "That attribution invitation could not be rejected "\
        "because it was already accepted"
    when :canceled
      "That attribution invitation could not be rejected "\
        "because it has been canceled"
    end
  end

  private

  def send_invitation_email
    AccountMailer.attribution_invitation_notification(self).deliver_later
  end

  def source_and_target_both_in_owner_org
    %i(source target).each do |e|
      unless self.public_send(e).organizations.include?(owner)
        errors.add(e, "must be a member of the #{owner.login} organization")
      end
    end
  end
end
