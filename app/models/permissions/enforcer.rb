# frozen_string_literal: true

module Permissions
  class Enforcer
    def initialize
      @attrs_for_subject = Hash.new do |hash, subject|
        hash[subject] = attrs_for_subject(subject).freeze
      end
      @attrs_for_actor = Hash.new do |hash, actor|
        hash[actor] = attrs_for_actor(actor).freeze
      end
    end

    # Public: Enforce access for an actor to a subject based on the action and
    # context.
    #
    # Returns an Authzd::Response
    def self.authorize(action:, subject:, actor:, context: {}, options: {})
      start = Time.now
      attrs = attrs_for(action: action, subject: subject, actor: actor, context: context)

      result = Permissions::Authorizer.authorize(
        Authzd::Proto::Request.new(attributes: attrs),
        options,
      )

      # rubocop:disable GitHub/DoNotBranchOnRailsEnv
      raise_on_error(result, action: action, attributes: attrs) unless Rails.production?
      GitHub.dogstats.distribution("authzd.client.enforcer.authorize", (Time.now - start) * 1_000, tags: ["action:#{action}"])
      result
    end

    # requests - an array of hashes with attributes
    #
    # Returns an Authzd::Proto::BatchDecision, which holds Authorizer
    # requests is an array of hashes with keys action, subject, actor, and
    # context
    def self.batch_authorize(requests:)
      new.batch_authorize(requests: requests)
    end

    def batch_authorize(requests:)
      start = Time.now
      enforcer_request_to_authzd_request = {}
      authzd_reqs = requests.map do |request|
        attrs = attrs_for(action: request[:action],
                          subject: request[:subject],
                          actor: request[:actor],
                          context: request[:context] || {})

        authzd_request = Authzd::Proto::Request.new(attributes: attrs)
        enforcer_request_to_authzd_request[request] = authzd_request
        authzd_request
      end

      batch_request = Authzd::Proto::BatchRequest.new(requests: authzd_reqs)
      batch_result = Permissions::Authorizer.batch_authorize(batch_request)

      self.class.raise_on_error(batch_result) unless Rails.production?

      requests.each do |request|
        authzd_request = enforcer_request_to_authzd_request[request]
        # FIXME not sure if this is a good idea.
        # The enforcer abstracts the Authzd::Proto::Request away, so it'd make sense to
        # allow getting the Authzd::Proto::Decision for a given enforcer request.
        # This adds to the map those enforcer requests as key. As a consequence,
        # batch_result.map will ne filled with the same decision with 2 different keys.
        batch_result.map[request] = batch_result[authzd_request]
      end
      GitHub.dogstats.distribution("authzd.client.enforcer.batch_authorize", (Time.now - start) * 1_000)
      batch_result
    end

    def self.attrs_for(**args)
      new.attrs_for(**args)
    end

    def attrs_for(action:, subject:, actor:, context: {})
      start = Time.now
      # PublicKey doesn't have abilities directly, rather abilities are
      # delegated to the User that owns the PublicKey, so a user's public keys
      # get the permissions of the user.
      actor = actor.ability_delegate if actor.is_a?(PublicKey)

      attrs = [] # Google::Protobuf::RepeatedField.new(:message, [])

      # enables authzd verbose output in development / test
      attrs << coerce_attr("_debug_", true) unless Rails.production?

      attrs << coerce_attr("action", action)
      # allow version coming from in context to override configured versions.
      # This enables doing science experiments when modifying policies
      version = context.fetch(:version, Permissions::PolicyVersion.version_for(action: action))
      attrs << coerce_attr(Permissions::PolicyVersion::VERSION_ATTRIBUTE, version)

      if subject
        attrs.concat @attrs_for_subject[subject]

        if subject.is_a?(Repository)
          if context.fetch(:considers_site_admin, nil).nil?
            attrs << coerce_attr("considers_site_admin", false)
          end
          interaction_allowed =
            if actor&.is_a?(User)
              User::InteractionAbility.interaction_allowed?(actor.id, subject)
            end
          attrs << coerce_attr("subject.repository.interaction_allowed", interaction_allowed)
        elsif subject.is_a?(Marketplace::Listing)
          attrs << coerce_attr("user.biztools_user", actor.biztools_user?)
        end
      end

      attrs.concat @attrs_for_actor[actor]

      if actor.is_a?(User)
        if repo = subject.try(:repository)
          attrs << coerce_attr("user.staff_unlock", actor.has_unlocked_repository?(repo))
        end
      end

      context.except(:version).each_pair do |key, value|
        attrs << coerce_attr(key, value)
      end

      GitHub.dogstats.distribution("authzd.client.enforcer.attributes", (Time.now - start) * 1_000, tags: ["action:#{action}"])

      attrs
    end

    def attrs_for_subject(subject)
      subject.permissions_wrapper.serialized_subject_attributes
    end

    def attrs_for_actor(actor)
      attrs = []

      if actor
        attrs << coerce_attr("actor.type", actor.class.name) unless actor.try(:bot?)
        attrs << coerce_attr("actor.id", actor.id) unless actor.try(:bot?)
        case actor
        when Bot
          # This maps a Bot to the underlying IntegrationInstallation
          # so we avoid having to create policies for Bots as well
          case actor.installation
          when ScopedIntegrationInstallation
            attrs << coerce_attr("actor.type", "ScopedIntegrationInstallation")
            attrs << coerce_attr("installation.parent.id", actor.installation.integration_installation_id)
          else
            attrs << coerce_attr("actor.type", "IntegrationInstallation")
            attrs << coerce_attr("installation.parent.id", nil)
          end

          attrs << coerce_attr("actor.id", actor.installation&.id)
          attrs << coerce_attr("installation.integration.id", actor.integration&.id)
          attrs << coerce_attr("installation.target.id", actor.installation&.target_id)
          attrs << coerce_attr("installation.target.type", actor.installation&.target_type)
        when Integration
          attrs << coerce_attr("installation.integration.id", actor&.id)
        when User
          attrs << coerce_attr("github.email_verification.enabled", GitHub.email_verification_enabled?)
          attrs << coerce_attr("user.id", actor.id)
          attrs << coerce_attr("user.site_admin", actor.site_admin?)
          attrs << coerce_attr("actor.spammy", actor.spammy?)
          attrs << coerce_attr("actor.suspended", actor.suspended?)
        end
      else
        attrs << coerce_attr("actor.id", nil)
        attrs << coerce_attr("actor.type", nil)
      end

      attrs
    end

    def coerce_attr(key, value)
      Authzd::Proto::Attribute.wrap(key, value)
    end

    # Used to make authzd errors more obvious in dev/test by failing loud
    def self.raise_on_error(result, action: "", attributes: [])
      # super-hack: workaround to a NOT_APPLICABLE not yet fixed with push_protected_branch
      # see https://github.com/github/authzd/issues/717
      return if action == :push_protected_branch
      if result.batch?
        batch_result = result
        batch_result.map.values.each do |res|
          dump = dump_attributes(batch_result.map.key(res).attributes)
          action = batch_result.map.key(res).attributes.detect { |attr| attr.id == "action" }.unwrapped_value
          next if "push_protected_branch" == action
          raise "Authzd returned INDETERMINATE for action \"#{action}\": an error happened while performing the authorization request: #{res.decision.reason} - do not to rescue the exception and contact #iam for support\n#{dump}" if res.indeterminate?
          raise "Authzd returned NOT_APPLICABLE for action \"#{action}\": the request did not match any policy - do not to rescue the exception and contact #iam for support\n#{dump}" if res.not_applicable?
        end
      else
        dump = dump_attributes(attributes)
        raise "Authzd returned INDETERMINATE for action \"#{action}\": an error happened while performing the authorization request: #{result.decision.reason} - do not to rescue the exception and  contact #iam for support\n#{dump}" if result.indeterminate?
        raise "Authzd returned NOT_APPLICABLE for action \"#{action}\": the request did not match any policy - do not to rescue the exception and contact #iam for support\n#{dump}" if result.not_applicable?
      end
    end

    def self.dump_attributes(attributes)
      hash = Hash.new
      attributes.each do |attr|
        hash[attr.id] = attr.value.to_s
      end
      hash.to_yaml
    end
  end
end
