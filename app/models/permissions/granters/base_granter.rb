# frozen_string_literal: true

module Permissions
  module Granters
    class BaseGranter
      attr_reader :actor_type, :action_int, :priority, :subject_type, :context

      def initialize(actor_type:, action_int:, priority:, subject_type:, context: {})
        @actor_type = actor_type
        @action_int = action_int
        @priority = priority
        @subject_type = subject_type
        @context = context
      end

      def grant!(actor_id:, subject_id:)
        raise ArgumentError.new("actor_id and subject_id are required") unless actor_id && subject_id
        row = [
          actor_id,
          actor_type,
          action_int,
          subject_id,
          subject_type,
          priority,
          0,
          GitHub::SQL::NOW,
          GitHub::SQL::NOW,
          GitHub::SQL::NULL,
        ]

        if Permissions::Service.grant_permissions([row], stats_key: self.class.name)
          GrantResult.success!
        else
          return GrantResult.failure!(reason: "Could not grant #{self.class.name} permission")
        end
      end

      def revoke!(actor_id:, subject_id:)
        raise ArgumentError.new("actor_id and subject_id are required") unless actor_id && subject_id
        args = {
          actor_id: actor_id,
          actor_type: actor_type,
          subject_id: subject_id,
          subject_type: subject_type,
          action: action_int,
          priority: priority,
        }

        if Permissions::Service.revoke_permissions(**args)
          GrantResult.success! # TODO: check this and handle async here
        else
          GrantResult.failure!(reason: "Failed to revoke #{self.class.name} permission on #{subject_id}")
        end
      end
    end
  end
end
