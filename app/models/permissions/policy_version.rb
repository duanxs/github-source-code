# frozen_string_literal: true

module Permissions
  class PolicyVersion

    POLICY_VERSIONS = {
      # please keep alphabetic order
      add_assignee:                       1,
      add_label:                          1,
      close_pull_request:                 1,
      close_issue:                        1,
      edit_repo_metadata:                 2,
      grant_manage_organization_apps:     1,
      grant_manage_app:                   1,
      grantable_manage_organization_apps: 1,
      grantable_manage_app:               1,
      manage_all_apps:                    1,
      manage_app:                         1,
      manage_topics:                      1,
      manage_settings_wiki:               2,
      manage_settings_projects:           2,
      manage_settings_pages:              2,
      manage_settings_merge_types:        2,
      manage_deploy_keys:                 2,
      mark_as_duplicate:                  1,
      push_protected_branch:              2,
      reopen_issue:                       1,
      reopen_pull_request:                1,
      request_pr_review:                  2,
      set_interaction_limits:             2,
      set_milestone:                      1,
      set_social_preview:                 2,
      setup_issue_template:               1,
      star_repo:                          1,
      unmark_as_duplicate:                1,
      view_hook_deliveries:               2,
      whitelist_ip:                       2,
    }.freeze

    LATEST = -1
    VERSION_ATTRIBUTE = "version".freeze
    InvalidPolicyVersion = Class.new(Authzd::Error)

    def self.version_for(action:)
      if action.kind_of?(Array)
        result = self.versions.values_at(*action).uniq.compact
        raise InvalidPolicyVersion.new("action array with different versions found") if result.size > 1
        return LATEST if result.size == 0
        return result.first
      end
      self.versions.fetch(action, LATEST)
    end

    def self.versions
      POLICY_VERSIONS
    end
  end
end
