# frozen_string_literal: true

module Permissions
  module Enumerators
    class BelongToOrganization < Enumerator
      def actor_ids
        abilities = Ability.where(
          actor_type: Enumerators::OwnOrganization::ACTOR_TYPE,
          subject_type: Enumerators::OwnOrganization::SUBJECT_TYPE,
          subject_id: subject_id,
          priority: Ability.priorities[:direct],
        )

        abilities.map(&:actor_id)
      end
    end
  end
end
