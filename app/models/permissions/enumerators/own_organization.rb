# frozen_string_literal: true

module Permissions
  module Enumerators
    class OwnOrganization < Enumerator
      ACTOR_TYPE = "User"
      SUBJECT_TYPE = "Organization"

      def actor_ids
        admin_abilities = Ability.where(
          actor_type: ACTOR_TYPE,
          subject_type: SUBJECT_TYPE,
          subject_id: subject_id,
          action: Ability.actions[:admin],
          priority: Ability.priorities[:direct],
        )
        admin_abilities.map(&:actor_id)
      end
    end
  end
end
