# frozen_string_literal: true

module Permissions
  module Attributes
    class PreReceiveEnvironment < Default
    end
  end
end
