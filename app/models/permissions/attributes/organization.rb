# frozen_string_literal: true

module Permissions
  module Attributes
    class Organization < Default
      def subject_attributes
        super.merge(
          "subject.business.id" => participant.business&.id,
        )
      end
    end
  end
end
