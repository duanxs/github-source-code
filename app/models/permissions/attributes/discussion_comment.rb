# frozen_string_literal: true

module Permissions
  module Attributes
    class DiscussionComment < Default
      def subject_attributes
        super.merge(
          "subject.wiped"                 => participant.wiped?,
          "subject.answer"                => participant.answer?,
          "subject.discussion.locked"     => participant.discussion&.locked?,
          "subject.author.id"             => participant.user_id,
          "subject.discussion.author.id"  => participant.discussion&.user_id,
          "subject.repository.id"         => participant.repository_id,
          "subject.repository.writable"   => participant.repository&.writable?,
          "subject.repository.public"     => participant.repository&.public?,
          "subject.repository.owner.id"   => participant.repository&.owner_id,
          "subject.owner.id"              => participant.repository&.owner_id,
          "subject.repository.owner.type" => participant.repository&.owner&.class.name,
        )
      end
    end
  end
end
