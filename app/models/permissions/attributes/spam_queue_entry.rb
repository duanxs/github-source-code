# frozen_string_literal: true

module Permissions
  module Attributes
    class SpamQueueEntry < Default
    end
  end
end
