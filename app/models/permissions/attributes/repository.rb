# frozen_string_literal: true

module Permissions
  module Attributes
    class Repository < Default
      def subject_attributes
        super.merge(
          "repository.public"              => participant.public?,
          "repository.owner.id"            => participant.owner_id,
          "subject.repository.id"          => participant.id,
          "subject.repository.public"      => participant.public?,
          "subject.repository.internal"    => participant.internal?,
          "subject.owner.id"               => participant.owner_id,
          "subject.business.id"            => participant.owner.async_business&.sync&.id,
          "subject.repository.owner.id"    => participant.owner_id,
          "subject.owning_organization.id" => participant.owning_organization_id,
          "repository.owner.type"          => participant.owner.class.name,
          "subject.repository.writable"    => participant.writable?,
        )
      end
    end
  end
end
