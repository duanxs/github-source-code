# frozen_string_literal: true

module Permissions
  module Attributes
    module Wrapper
      extend ActiveSupport::Concern

      class_methods do
        attr_writer :permissions_wrapper_class
        def permissions_wrapper_class
          @permissions_wrapper_class ||= Permissions::Attributes::Default
        end
      end

      def permissions_wrapper
        self.class.permissions_wrapper_class.new(self)
      end
    end
  end
end
