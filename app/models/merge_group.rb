# frozen_string_literal: true

class MergeGroup < ApplicationRecord::Collab
  belongs_to :queue, class_name: :MergeQueue, foreign_key: :merge_queue_id
  has_many :group_entries, class_name: :MergeGroupEntry, counter_cache: true
  has_many :queue_entries, through: :group_entries, source: :queue_entry

  enum state: {
    pending: 0,
    conflicts: 1,
    check_errors: 2,
    mergeable: 3
  }
end
