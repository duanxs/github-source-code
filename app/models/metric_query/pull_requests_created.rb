# frozen_string_literal: true

class MetricQuery::PullRequestsCreated < MetricQuery
  include MetricQuery::SingleModelColumn

  self.model = PullRequest
  self.display_name = "Pull Requests"

  def all
    relation
      .for_owner(@params[:owner_id])
      .bucketed_by(fully_qualified_column, timespan)
      .count
  end
end
