# frozen_string_literal: true

class MetricQuery::PullRequestsMerged < MetricQuery
  include MetricQuery::SingleModelColumn

  self.model = PullRequest
  self.column = :merged_at

  def all
    relation
      .for_owner(@params[:owner_id])
      .bucketed_by(fully_qualified_column, timespan)
      .count
  end
end
