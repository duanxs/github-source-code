# frozen_string_literal: true

class MetricQuery::PullRequestReviewsCreated < MetricQuery
  include MetricQuery::SingleModelColumn

  self.model = PullRequestReview
  self.display_name = "Code Review"

  def all
    relation
      .for_owner(@params[:owner_id], pull_request: :repository)
      .bucketed_by(fully_qualified_column, timespan)
      .count
  end
end
