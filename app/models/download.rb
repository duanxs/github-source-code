# rubocop:disable Style/FrozenStringLiteralComment

class Download < ApplicationRecord::Domain::Repositories
  areas_of_responsibility :lfs
  class << self
    attr_writer :adapter_type
    def adapter_type
      @adapter_type ||= default_adapter
    end

    def default_adapter
      GitHub.downloads_enabled? ? Download::S3Adapter : Download::TestAdapter
    end

    def move(*args)
      adapter_type.move(*args)
    end
  end

  include SlottedCounterService::Countable

  belongs_to :repository
  belongs_to :user

  before_create  :set_timestamp
  after_create   :inc_repo_disk_usage, :check_for_spam
  before_destroy :delete_file
  after_destroy  :dec_repo_disk_usage

  def self.recent(limit)
    order("timestamp DESC").take(limit)
  end

  validates_presence_of :name
  validates_uniqueness_of :name, scope: [:user_id, :repository_id], case_sensitive: true

  def download_adapter
    @download_adapter ||= self.class.adapter_type.new(self)
  end

  # Public
  delegate :path, :url, :delete_file, :file_exists?,
    :update_permissions, to: :download_adapter

  def full_url
    GitHub.url + "/" + self.path
  end

  # Public: Whether the given user can see this deployment.
  def readable_by?(actor)
    repository && repository.readable_by?(actor)
  end

  def set_fields(params)
    self.name = params[:file_name].to_s
    self.size = params[:file_size].to_i / 1.kilobyte
    self.description  = params[:description]

    self.content_type = case self.name.split(".").last
    when "apk"
      "application/vnd.android.package-archive"
    when "crx"
      "application/x-chrome-extension"
    else
      # Sometimes the client-side code sends us extensions like ".zip" as the content_type,
      # which is wrong. In that case we want to fall back to looking up a content_type
      # based on the filename.
      guess_content_type(content_type: params[:content_type], filename: params[:file_name])
    end
  end

  def hits
    slotted_count_with :hits
  end

  def hit!
    GitHub.dogstats.increment("download", tags: ["action:hit"])
    slotted_count!(:hits)
  end

  def reset_memoized_attributes
    @download_adapter = nil
  end

  def to_s
    name.to_s
  end

  def set_timestamp
    self.timestamp = Time.now
  end

  def inc_repo_disk_usage
    repository.update_attribute(:disk_usage, repository.disk_usage + size)
  end

  def dec_repo_disk_usage
    usage = repository.disk_usage - size
    usage = 0 if usage < 0
    repository.update_attribute(:disk_usage, usage)
  end

  def check_for_spam
    return if repository.pushed_at.present?

    # If the user is uploading html files to an unpushed repo, he may be spammy.
    # Flag the user for staff to verify.
    if self.name =~ /\.html?$/i
      repository.owner.safer_mark_as_spammy(reason: "HTML upload to unpushed repo")
    end
  end

  private

  def guess_content_type(content_type:, filename:)
    sent  = content_type unless content_type.blank? || content_type =~ /^\.\w+$/
    guess = filename ? GitHub::MimeTypeHelpers.simplified(MiniMime.lookup_by_filename(filename)) : nil

    sent || guess || "application/octet-stream"
  end
end
