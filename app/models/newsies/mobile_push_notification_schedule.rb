# frozen_string_literal: true

module Newsies
  class MobilePushNotificationSchedule < ApplicationRecord::Domain::Notifications
    self.utc_datetime_columns = %i[created_at end_time start_time updated_at]
    self.record_timestamps = true

    belongs_to :user, required: true

    validates :day, :end_time, :start_time, presence: true
    validates :day, uniqueness: { scope: :user_id }

    # based off of Time#wday which uses a 0-6 range, with 0 being sunday
    WDAY = {
      0 => :sunday,
      1 => :monday,
      2 => :tuesday,
      3 => :wednesday,
      4 => :thursday,
      5 => :friday,
      6 => :saturday,
    }

    enum day: WDAY.invert

    def self.deliver_to_user?(user)
      return false unless user
      return true unless exists?(user: user)

      Time.use_zone(user.mobile_time_zone) do
        now = Time.zone.now
        time = now.strftime("%H:%M:%S")
        schedule = find_by(day: now.wday, user_id: user.id)

        return false unless schedule
        return time >= schedule.start_time.in_time_zone.strftime("%H:%M:%S") &&
          time <= schedule.end_time.in_time_zone.strftime("%H:%M:%S")
      end
    end
  end
end
