# rubocop:disable Style/FrozenStringLiteralComment

module Newsies
  # Responsible for managing the subscribers to a type of thread within a list
  # e.g. Release (thread type) threads for a Repository (list).
  class ThreadTypeSubscription < ApplicationRecord::Domain::Notifications
    self.table_name = :notification_thread_type_subscriptions
    self.utc_datetime_columns = [:created_at]

    DEFAULT_REASON = "thread_type"
    DEFAULT_PAGE_SIZE = 100
    DEFAULT_BATCH_SIZE = 1000

    scope :for_list, ->(newsies_list) {
      where(list_type: newsies_list.type, list_id: newsies_list.id)
    }

    scope :for_lists, ->(lists) {
      raise ArgumentError, "lists cannot be blank" unless lists.present?
      where(where_condition_for_lists(lists))
    }

    scope :excluding_lists, ->(lists) {
      raise ArgumentError, "lists cannot be blank" unless lists.present?
      where.not(where_condition_for_lists(lists))
    }

    scope :for_thread_type, ->(thread_type) {
      where(thread_type: thread_type)
    }

    scope :for_threads, ->(threads) {
      raise ArgumentError, "threads cannot be blank" unless threads.present?
      where(where_condition_for_threads(threads))
    }

    scope :for_user, ->(user_id) {
      where(user_id: user_id)
    }

    scope :for_users, ->(user_ids) {
      where(user_id: user_ids)
    }

    # Private: build a WHERE condition for multiple lists, with possibly different list_types
    # With multiple lists, we want to check `list_id IN (ids)`, but we must separate that
    # by list_type to ensure we get the right results
    #
    # Do not use directly, use via `for_lists`
    #
    # lists - An array of Newsies::List intances
    #
    # Returns ["(list_type = ? AND list_id IN (?)) OR ...", value_bindings]
    def self.where_condition_for_lists(lists)
      query_parts = []
      bindings = []
      lists.group_by(&:type).each do |(list_type, lists_of_type)|
        query_parts.push("(list_type = ? AND list_id IN (?))")
        bindings.push(list_type, lists_of_type.map(&:id))
      end
      [query_parts.join(" OR "), *bindings]
    end

    # Private: build a WHERE condition for multiple threads.
    #
    # Do not use directly, use via `for_threads` scope.
    #
    # threads - An array of Newsies::Thread instances
    #
    # Returns ["(list_type = ? AND list_id = ? AND thread_type IN (?)) OR ...", value_bindings]
    def self.where_condition_for_threads(threads)
      query_parts = []
      bindings = []
      threads.group_by(&:list).each do |(list, threads_in_list)|
        query_parts.push("(list_type = ? AND list_id = ? AND thread_type IN (?))")
        bindings.push(list.type, list.id, threads_in_list.map(&:type).uniq)
      end
      [query_parts.join(" OR "), *bindings]
    end

    # Public: Subscribes a User to a thread type.
    #
    # user_id       - An Integer user id.
    # newsiest_list - A Newsies::List instance
    # thread_type   - A String of the thread type to subscribe to (e.g. "Release")
    #
    # Returns nothing.
    def self.subscribe_to_thread_type(user_id, newsies_list, thread_type)
      new({
        user_id: user_id,
        list_type: newsies_list.type,
        list_id: newsies_list.id,
        thread_type: thread_type,
        created_at: Time.now,
      }).save

    rescue ActiveRecord::RecordNotUnique
      # Ignore unique key violations (if the row is already there then all is well).
    end

    # Public: Get all the thread types a user is subscribed to for a given list.
    #
    # user_id      - An Integer user id
    # newsies_list - A Newsies::List instance
    def self.subscribed_thread_types(user_id, newsies_list)
      for_user(user_id).for_list(newsies_list).pluck(:thread_type)
    end

    # Public: Unsubscribes a User from all thread types for the given list.
    #
    # user_id      - An Integer user id.
    # newsies_list - A Newsies::List instance
    #
    # Returns nothing.
    def self.unsubscribe_from_all_thread_types_for_list(user_id, newsies_list)
      unsubscribe_from_all_thread_types_for_multiple_lists(user_id, [newsies_list])
    end

    # Public: Unsubscribes a User from all thread types for each given list.
    #
    # user_id       - An Integer user id.
    # newsies_lists - An array of Newsies::List instance
    #
    # Returns nothing.
    def self.unsubscribe_from_all_thread_types_for_multiple_lists(user_id, newsies_lists)
      for_user(user_id).for_lists(newsies_lists).delete_all
    end

    def self.subscriptions(user_id, page: nil, per_page: nil, excluding: nil, sort: :desc, list_type: "Repository", thread_type: "Release")
      page ||= 1
      per_page ||= DEFAULT_PAGE_SIZE

      scope = ThreadTypeSubscription
        .for_user(user_id)
        .where(list_type: list_type)
        .for_thread_type(thread_type)
        .offset([page - 1, 0].max * per_page)
        .limit(per_page)

      scope = scope.excluding_lists(excluding) if excluding.present?
      scope = scope.order(id: sort.to_sym) if sort && [:asc, :desc].include?(sort.to_sym)

      scope.to_a
    end

    def to_subscriber
      Subscriber.new(user_id, true, false, DEFAULT_REASON, created_at)
    end
  end
end
