# frozen_string_literal: true

module Newsies
  class MobilePushNotificationDelivery < ApplicationRecord::Domain::NotificationsDeliveries
    self.utc_datetime_columns = [:created_at, :updated_at]
    self.record_timestamps = true

    # This is a transient attribute used to track HTTP responses from Google FCM
    # within a single invocation of `Newsies::DeliverMobilePushNotificationsJob#perform`.
    attr_accessor :fcm_response

    FCM_NOT_REGISTERED_ERROR = "NotRegistered"
    FCM_RETRYABLE_ERRORS = %w( Unavailable InternalServerError DeviceMessageRateExceeded )
    DEFAULT_DELIVERY_REASON = "mention"

    enum state: { delivered: 0, retrying: 1, failed: 2 }

    belongs_to :mobile_device_token, required: true
    belongs_to :user, required: true
    belongs_to :list, polymorphic: true, required: true
    belongs_to :thread, polymorphic: true, required: true
    belongs_to :comment, polymorphic: true, required: true

    validates(
      :list_type,
      :list_id,
      :thread_type,
      :thread_id,
      :comment_type,
      :comment_id,
      :user_id,
      :state,
      presence: true,
    )

    # Upserts a set of delivery rows in a single SQL request.
    #
    # deliveries - Array<MobilePushNotificationDelivery>
    #
    # Returns GitHub::SQL instance.
    def self.upsert(deliveries)
      now = Time.now.utc

      rows = deliveries.map do |d|
        [
          d.mobile_device_token_id,
          d.list_type,
          d.list_id,
          d.thread_type,
          d.thread_id,
          d.comment_type,
          d.comment_id,
          d.user_id,
          d.reason,
          states[d.state], # This converts, e.g., `:delivered` to `0`.
          d.state_explanation || GitHub::SQL::NULL,
          now,
          now,
        ]
      end

      github_sql.run <<-SQL, rows: GitHub::SQL::ROWS(rows)
        INSERT INTO mobile_push_notification_deliveries
         (
           mobile_device_token_id,
           list_type,
           list_id,
           thread_type,
           thread_id,
           comment_type,
           comment_id,
           user_id,
           reason,
           state,
           state_explanation,
           created_at,
           updated_at
         )
        VALUES :rows
        ON DUPLICATE KEY UPDATE
          state = VALUES(state),
          state_explanation = VALUES(state_explanation),
          updated_at = VALUES(updated_at)
      SQL
    end

    # comment - PushNotifiable
    # token - MobileDeviceToken
    # fcm_response - Hash response object for the response from Google FCM for the delivery attempt
    #   to the given token.
    # state - Symbol value to override default computation from the given `fcm_response`.
    # state_explanation - String value to override default computation from the given `fcm_response`.
    #
    # Returns a new (unpersisted) MobilePushNotificationDelivery.
    def self.from(comment, token, fcm_response: nil, state: nil, state_explanation: nil)
      fcm_response ||= {}

      state ||= if fcm_response[:message_id].present? && fcm_response[:error].blank?
        :delivered
      elsif retryable_error?(fcm_response[:error])
        :retrying
      else
        :failed
      end

      newsies_comment = Newsies::Comment.to_object(comment)
      newsies_thread = Newsies::Thread.to_object(comment.notifications_thread)
      newsies_list = Newsies::List.to_object(comment.notifications_list)

      new(
        mobile_device_token_id: token.id,
        list_type: newsies_list.type,
        list_id: newsies_list.id,
        thread_type: newsies_thread.type,
        thread_id: newsies_thread.id,
        comment_type: newsies_comment.type,
        comment_id: newsies_comment.id,
        user_id: token.user_id,
        reason: DEFAULT_DELIVERY_REASON,
        state: state,
        state_explanation: state_explanation || fcm_response[:error],
        fcm_response: fcm_response,
      )
    end

    def token_invalid?
      failed? && token_unregistered?
    end

    def token_unregistered?
      [FCM_NOT_REGISTERED_ERROR, Firebase::CloudMessagingClient::UNREGISTERED_ERROR].include?(state_explanation)
    end

    def thread_key
      @thread_key ||= Newsies::Thread.new(thread_type, thread_id).key
    end

    def comment_key
      @comment_key ||= Newsies::Comment.new(comment_type, comment_id).key
    end

    def handler
      "mobile-push"
    end

    def delivered_at
      delivered? ? updated_at : nil
    end

    def self.retryable_error?(error)
      FCM_RETRYABLE_ERRORS.include?(error) ||
        Firebase::CloudMessagingClient::RETRYABLE_ERRORS.include?(error)
    end
  end
end
