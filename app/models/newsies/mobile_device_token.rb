# frozen_string_literal: true

module Newsies
  class MobileDeviceToken < ApplicationRecord::Domain::Notifications
    self.utc_datetime_columns = [:created_at, :updated_at]
    self.record_timestamps = true

    enum service: { fcm: 0, apns: 1 }

    belongs_to :user, required: true

    validates :device_token, :service, presence: true
    validates :device_token, length: { maximum: 255 }
    validates :device_token, uniqueness: { scope: [:user_id, :service], case_sensitive: true }
    validate :under_max_tokens_limit, on: :create

    MAX_TOKENS_PER_USER = 50

    private

    def under_max_tokens_limit
      if self.class.unscoped.where(user_id: attributes["user_id"]).count >= MAX_TOKENS_PER_USER
        errors.add(:base, "User has reached maximum number of allowed device tokens")
      end
    end
  end
end
