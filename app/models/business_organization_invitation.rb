# frozen_string_literal: true

# An invitation from a business for an organization to become part of it.
#
# It is created by a business admin, and its status flows like this:
#
#          org admin              business admin
#           accepts                  confirms
# CREATED -----------> ACCEPTED -----------------> CONFIRMED
#    |                     |
#    | org admin           | business admin
#    | declines            | declines
#    |                     V
#    +---------------> CANCELED

class BusinessOrganizationInvitation < ApplicationRecord::Domain::Users
  include GitHub::Relay::GlobalIdentification

  belongs_to :business
  belongs_to :inviter, class_name: "User"
  belongs_to :invitee, class_name: "Organization"

  validates_presence_of :business, :inviter, :invitee
  validate :inviter_must_be_business_admin
  validate :business_must_be_self_served
  validate :organization_not_business_member
  validate :organization_not_already_invited_to_business
  validate :organization_not_invoiced
  validate :validate_business_has_sufficient_seats

  after_commit :send_invitation_email, on: :create
  after_create :instrument_invite_organization

  # Pending invitations are those not in the final states
  scope :pending, -> { where(confirmed_at: nil, canceled_at: nil) }

  scope :with_status, ->(status) {
    case status&.to_sym
    when :created
      where(confirmed_at: nil, accepted_at: nil, canceled_at: nil)
    when :accepted
      where.not(accepted_at: nil).where(confirmed_at: nil, canceled_at: nil)
    when :confirmed
      where.not(confirmed_at: nil)
    when :canceled
      where.not(canceled_at: nil)
    else
      none
    end
  }

  # Raised when someone accepts an already-accepted invitation.
  class AlreadyAcceptedError < StandardError; end

  # Raised when someone accepts, confirms or cancels an already-confirmed invitation.
  class AlreadyConfirmedError < StandardError; end

  # Raised when someone confirms an unaccepted invitation.
  class NotYetAcceptedError < StandardError; end

  # Raised when someone other than the invitee tries to accept or cancel an invitation.
  class InvalidActorError < StandardError; end

  # Raised when someone accepts or confirms a canceled invitation
  class CanceledError < StandardError; end

  # Raised when someone accepts or confirms an invitation from a business that the org already belongs to
  class AlreadyBusinessMemberError < StandardError; end

  # Raised if business can no longer extend this invitation
  class InvalidInviterError < StandardError; end

  # Raised if organization can no longer accept this invitation
  class InvalidInviteeError < StandardError; end

  # Raised when not enough seats are available in this business to add an Organization
  class InsufficientAvailableSeatsError < StandardError; end

  def inviter_must_be_business_admin
    return if inviter.blank? || business.blank?
    unless business.owner?(inviter)
      errors.add(:inviter, "must be an administrator of the enterprise.")
    end
  end

  def organization_not_business_member
    return unless invitee
    if invitee.business
      errors.add(:base, "Organization #{invitee.name} already belongs to an enterprise.")
    end
  end

  def organization_not_invoiced
    return unless invitee
    if invitee.invoiced?
      errors.add(:base, "Please contact your account representative to add #{invitee.name} to this enterprise.")
    end
  end

  def business_must_be_self_served
    return unless business
    unless business.can_self_serve?
      errors.add(:business, "must be able to self-serve.")
    end
  end

  def organization_not_already_invited_to_business
    return if invitee.blank? || business.blank?
    if business.pending_organization_invitation_for(invitee)
      errors.add(:base, "Organization #{invitee.name} has already been invited to this enterprise.")
    end
  end

  def validate_business_has_sufficient_seats
    return if invitee.blank?

    unless business_has_sufficient_seats?
      errors.add(:business, "doesn't have sufficient available seats to add #{invitee.name}.")
    end
  end

  def accept(acceptor)
    raise AlreadyConfirmedError, "invitation already confirmed" if confirmed?
    raise AlreadyAcceptedError, "invitation already accepted" if accepted?
    raise CanceledError, "invitation has been canceled" if canceled?
    raise AlreadyBusinessMemberError, "organization already part of an enterprise" if invitee.business
    raise InvalidActorError, "acceptor not allowed to accept this invitation" unless invitee.adminable_by?(acceptor)
    raise InvalidInviterError unless business.can_self_serve?
    raise InvalidInviteeError, "organization must contact sales to join an enterprise" if invitee.invoiced?
    raise InsufficientAvailableSeatsError, "enterprise has insufficient seats" unless business_has_sufficient_seats?

    touch :accepted_at
    invitee.instrument(:accept_business_invitation, { business: business, actor: acceptor })
  end

  def confirm(confirmer)
    raise AlreadyConfirmedError, "invitation already confirmed" if confirmed?
    raise CanceledError, "invitation has been canceled" if canceled?
    raise NotYetAcceptedError, "invitation must be accepted before confirmation" unless accepted?
    raise AlreadyBusinessMemberError, "organization already part of an enterprise" if invitee.business
    raise InvalidActorError, "confirmer not allowed to confirm this invitation" unless business.adminable_by?(confirmer)
    raise InvalidInviterError unless business.can_self_serve?
    raise InvalidInviteeError, "organization must contact sales to join an enterprise" if invitee.invoiced?
    raise InsufficientAvailableSeatsError, "enterprise has insufficient seats" unless business_has_sufficient_seats?

    BusinessOrganizationInvitation.transaction do
      business.add_organization(invitee)
      invitee.plan = :business_plus
      invitee.save
      touch :confirmed_at

      implicitly_refused_invitations = invitee.business_invitations.pending.where.not(business: business)
      implicitly_refused_invitations.each { |invitation| invitation.cancel(confirmer) }
    end

    invitee.instrument :confirm_business_invitation, { business: business, actor: confirmer }
  end

  def status
    if confirmed_at.present?
      :confirmed
    elsif canceled_at.present?
      :canceled
    elsif accepted_at.present?
      :accepted
    else
      :created
    end
  end

  def accepted?
    accepted_at.present?
  end

  def confirmed?
    confirmed_at.present?
  end

  def canceled?
    canceled_at.present?
  end

  # Public: Cancel a pending invitation.
  #
  # actor - User that is cancelling the invitation.
  #
  # Returns nothing.
  def cancel(actor)
    raise CanceledError, "invitation has been canceled" if canceled?
    raise AlreadyConfirmedError, "invitation has already been confirmed" if confirmed?
    raise InvalidActorError, "actor not allowed to cancel this invitation" if !invitee.adminable_by?(actor) && !business.owner?(actor)

    touch :canceled_at

    invitee.instrument(:cancel_business_invitation, { business: business, actor: actor })
  end

  def send_invitation_email
    return if inviter.spammy?

    BusinessMailer.invited_organization(self).deliver_later
  end

  # Public: Should the inviter be shown to organizations?
  #
  # This will be false if:
  #   - there's no inviter (which can happen if the inviter
  #     is deleted after sending the invitation).
  #
  # Returns a Boolean.
  def show_inviter?
    !!inviter
  end

  def instrument_invite_organization
    invitee.instrument(:invite_to_business, { business: business, actor: inviter })
  end

  # The number of unique users in the invited organization that are not already
  # members of the business.
  #
  # Returns an Integer.
  def needed_seat_count
    @needed_seat_count ||= business.additional_licenses_consumed_by_organization(invitee)
  end

  def platform_type_name
    "EnterpriseOrganizationInvitation"
  end

  private

  def business_has_sufficient_seats?
    return if invitee.blank? || business.blank?
    business.has_sufficient_licenses_for_organization?(invitee)
  end
end
