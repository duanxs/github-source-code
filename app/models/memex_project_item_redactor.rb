# frozen_string_literal: true

class MemexProjectItemRedactor

  def initialize(viewer:, items:)
    @viewer, @items = viewer, items
  end

  def items
    @items_with_redactions ||= @items.map do |item|
      readable_by_viewer?(item) ? item : item.redact!
    end
  end

  private

  def readable_by_viewer?(item)
    case item.content
    when Issue
      visible_issue_ids.include?(item.content_id)
    when PullRequest
      visible_issue_ids.include?(item.content.issue.id)
    when DraftIssue
      true
    else
      false
    end
  end

  def visible_issue_ids
    return @visible_issue_ids if defined?(@visible_issue_ids)

    issue_ids = @items.reduce([]) do |ids, item|
      case item.content
      when Issue
        ids << item.content_id
      when PullRequest
        ids << item.content.issue.id
      else
        ids
      end
    end

    @visible_issue_ids = Issue.visible_ids_for(issue_ids, viewer: @viewer)
  end
end
