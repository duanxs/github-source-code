# rubocop:disable Style/FrozenStringLiteralComment

# A Customer is a named container for an entity that pays for multiple
# organizations, users, and/or enterprise installs.
class Customer < ApplicationRecord::Domain::Users
  extend GitHub::Encoding

  self.ignored_columns += [:salesforce_account_id]

  areas_of_responsibility :gitcoin

  force_utf8_encoding :name

  # Public: String full legal name. (Example: Apple, Inc.)
  validates_presence_of :name

  # Public: String universally unique identifier for external systems. Not
  # intended for use as an identifier in this application.
  before_validation :set_external_uuid, on: :create
  validates_presence_of :external_uuid

  validates :billing_end_date, presence: { if: :invoiced?, message: "must be specified for invoiced customers" }

  # Public: Optional parent Customer record. (i.e. Yammer's parent might be
  # Microsoft, Inc.)
  belongs_to :parent_customer, class_name: "Customer"

  # Public: A verified email used to contact a human about billing.
  belongs_to :billing_email, class_name: "UserEmail"

  # Public: Optional children Customer records.
  has_many :child_customers, class_name: "Customer", foreign_key: "parent_customer_id"

  # Public: Association record between a Customer and a User or Organization
  # account. Also holds state and verification status information.
  has_many :customer_accounts, dependent: :delete_all

  # Public: Organization accounts this customer is actively paying for.
  has_many :organizations,
    -> { where(type: "Organization").distinct },
    through: :customer_accounts,
    source: :user

  # Public: Enterprise account this customer is actively paying for.
  has_one :business

  # Public: User accounts this customer is actively paying for.
  has_many :users,
    -> { where(type: "User").distinct },
    through: :customer_accounts

  # Public: Represents how this customer is paying for GitHub and the payment
  # processor used to collect $$.
  #
  # This User's PaymentMethod which represents how this user is
  # paying for GitHub and the payment processor used to collect $$. A nil
  # payment_method means this user is not paying for GitHub.
  has_one :payment_method,
    -> { where(primary: true) },
    dependent: :destroy

  has_one :sales_serve_plan_subscription, class_name: "Billing::SalesServePlanSubscription", dependent: :destroy

  delegate :bill_cycle_day, to: :zuora_object_account, allow_nil: true
  # Public: Integer seats this customer is paying for.
  # :seats

  # Public: String Braintree customer id (optional).
  # :braintree_customer_id

  # Public: Redis key used for tracking an update credit card transaction
  #
  # Returns a String
  def update_payment_method_key
    "user:updating_credit_card:#{id}"
  end

  # Public: Update the customer with the payment details.
  #
  # payment_details - A Hash of payment details:
  #                   :charge        - Boolean whether to attempt a recurring charge with this update. Default: true
  #                   :billing_extra - String extra billing information.
  #                   :vat_code      - String VAT Identification Number.
  #                   :paypal_nonce  - String nonce representing a paypal account (optional).
  #                   :credit_card   - A Hash of potentially encrypted CC info, including:
  #                     * :number           - CC number as a String.
  #                     * :expiration_month - Expiration month as a String of form MM
  #                     * :expiration_year  - Expiration year as a String of form YY
  #                     * :cvv              - CVV as a String (e.g. "420")
  #                   :billing_address  - The billing address as a Hash of optionally encrypted CC info
  #                     * :country_code_alpha3 - Country as a String
  #                     * :region              - Region as a String
  #                     * :postal_code         - Postal code as a String
  #
  # Returns a Customer
  def update_payment_method_details(payment_details)
    self.vat_code = payment_details[:vat_code] if payment_details.has_key?(:vat_code)
    self.billing_extra = payment_details[:billing_extra] if payment_details.has_key?(:billing_extra)

    billing_address = payment_details[:billing_address] || {}

    self.country_code_alpha2 = Customer.countries[billing_address[:country_code_alpha3]]
    self.region              = billing_address[:region]
    self.postal_code         = billing_address[:postal_code]

    if payment_method.using_braintree_processor? && zuora_account_id.present?
      payment_method.update(
        payment_processor_customer_id: zuora_account_id,
        payment_processor_type: PaymentMethod.zuora_processor_slug,
      )
    end
    result = self.payment_method.update_payment_details(payment_details)

    if payment_method.blacklisted?
      BlacklistedPaymentMethod.create_from_user_and_payment_method \
        payment_method.user, payment_method
      payment_method.user.suspend("Using blacklisted payment method")
    end

    save if result.success?
    result
  end

  # Public: Boolean does this customer have a record in Braintree? Customers
  # that keep credit card or other self serve payment information like PayPal on
  # file will have records in Braintree.
  def braintree?
    braintree_customer_id.present?
  end

  # Public: Boolean does this customer have a record in Zuora? Customers
  # that keep credit card or other self serve payment information like PayPal on
  # file will have records in Zuora.
  def zuora?
    zuora_account_id.present?
  end

  # Public: String comma separated email addresses to send invoices to.
  # :emails

  # Public: Array of string email addresses.
  def email_addresses
    emails.to_s.split(",")
  end

  # Public: Set the emails attributes with an array of email addresses or a
  # comma separated string of emails.
  def email_addresses=(new_email_addresses)
    self[:emails] = if new_email_addresses.is_a?(Array)
      new_email_addresses.join(",")
    else
      new_email_addresses
    end
  end

  # Public: String extra billing information to appear on every receipt.
  # :billing_extra

  # Public: String additional billing instructions.
  # :billing_instructions

  # Public: Billing address attributes.
  # :bill_to
  # :street_address
  # :country_code_alpha2
  # :region
  # :postal_code
  # :vat_code

  def to_param
    "#{id}-#{name.parameterize}"
  end

  # Public: Gets the Zuora Account object from the Zuora API
  #
  # Returns a Zuora::Model::Account
  def zuora_account
    return unless zuora?
    Zuorest::Model::Account.find(self.zuora_account_id)
  end

  # Public: checks if this account has an external account
  #
  # Returns a boolean
  def external_account?
    braintree? || zuora?
  end

  def update_external_account_name
    return unless external_account?

    account = customer_accounts.first
    if braintree?
      Braintree::Customer.update braintree_customer_id, first_name: account.user.login
    elsif zuora?
      zuora = zuora_account
      login = account.user.login
      zuora.update!(Name: login)
      account_response = GitHub.zuorest_client.get_account(zuora.id)
      new_login_params = { FirstName: login, LastName: login }
      GitHub.dogstats.time("zuora.timing.action_contact_update") do
        GitHub.zuorest_client.update_action({
          objects: [
            { Id: account_response["SoldToId"] }.merge!(new_login_params),
            { Id: account_response["BillToId"] }.merge!(new_login_params),
          ],
          type: "Contact",
        })
      end
    end
  end

  # Maps Alpha3(3 letter code) Country to Alpha2(2 letter code).
  #
  # Returns a Hash
  def self.countries
    @@countries ||= ::Braintree::Address::CountryNames.inject({}) do |memo, c|
      memo[c[2]] = c[1]
      memo
    end
  end

  # Public: returns whether or not this customer is invoiced. returns false if
  # billing_type is unset.
  def invoiced?
    billing_type == "invoice"
  end

  # Public: Updates attributes from Zuora into the Customer object
  #
  # Returns nothing
  def update_from_zuora
    if zuora_object_account
      update(
        bill_cycle_day: zuora_object_account.bill_cycle_day
      )
    end
  end

  private

  def zuora_object_account
    return @zuora_object_account if defined?(@zuora_object_account)

    @zuora_object_account = ::Billing::Zuora::Object::Account.find(zuora_account_id)
  end

  # Private: Sets a uuid for external systems to reference when the Customer
  # record is first created.
  def set_external_uuid
    self.external_uuid ||= SecureRandom.uuid
  end
end
