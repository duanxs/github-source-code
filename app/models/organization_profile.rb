# frozen_string_literal: true

class OrganizationProfile < ApplicationRecord::Domain::UsersCollab
  before_validation :normalize_sponsors_update_email,
    if: :sponsors_update_email_changed?

  validates :sponsors_update_email,
    format: {
      with: User::EMAIL_REGEX,
      message: "does not look like an email address",
    },
    allow_nil: true

  belongs_to :organization

  private

  def normalize_sponsors_update_email
    return if sponsors_update_email.present?
    self.sponsors_update_email = nil
  end
end
