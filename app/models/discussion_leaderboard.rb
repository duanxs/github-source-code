# frozen_string_literal: true

# Public: Queries useful for ranking users
# based on their recent discussion activity for a given repository.
class DiscussionLeaderboard
  PERFORMANCE_LIMIT = 1000
  RECENT_TIME_PERIOD = 1.week

  def initialize(repository:, viewer:)
    @repository = repository
    @viewer = viewer
    @admin_or_maintainer_cache = {}
    @cached_users_by_id = {}
  end

  # Public: Which users have recently contributed the most answers to this repo, and how many did they contribute?
  #
  # limit - The maximum number of users to return
  #
  # Returns a hash of User to Integer representing the count of recent answers by that User
  def top_answer_counts(limit:)
    measure_time "top_answer_counts" do
      counts = users_to_answer_counts

      measure_time "top_answer_counts.filtering" do
        filter_out_admins_and_maintainers(counts, limit: limit)
      end
    end
  end

  # Public: Which users have recently contributed the most questions to this repo, and how many did they contribute?
  #
  # limit - The maximum number of users to return
  #
  # Returns a hash of User to Integer representing the count of recent questions by that User
  def top_question_counts(limit:)
    measure_time "top_question_counts" do
      counts = users_to_question_counts

      measure_time "top_question_counts.filtering" do
        filter_out_admins_and_maintainers(counts, limit: limit)
      end
    end
  end

  private

  attr_reader :repository, :viewer

  def users_to_answer_counts
    user_ids_to_answer_counts = measure_time "top_answer_counts.aggregation" do
      repository
        .discussion_comments
        .chosen_answers
        .where("discussion_comments.created_at >= ?", RECENT_TIME_PERIOD.ago)
        .filter_spam_for(viewer)
        .group(:user_id)
        .order(count_all: :desc)
        .limit(PERFORMANCE_LIMIT)
        .count(:all)
    end

    transform_ids_to_users(user_ids_to_answer_counts)
  end

  def users_to_question_counts
    user_ids_to_question_counts = measure_time "top_question_counts.aggregation" do
      repository
        .discussions
        .question
        .where("discussions.created_at >= ?", RECENT_TIME_PERIOD.ago)
        .filter_spam_for(viewer)
        .group(:user_id)
        .order(count_all: :desc)
        .limit(PERFORMANCE_LIMIT)
        .count(:all)
    end

    transform_ids_to_users(user_ids_to_question_counts)
  end

  # Private: Transform a hash of user id keys to have User keys.
  #
  # user_ids_to_counts - Hash of User ID (Integer) to counts (Integer)
  #
  # Returns a new Hash of User to counts (Integer)
  def transform_ids_to_users(user_ids_to_counts)
    update_cached_users_by_id(user_ids_to_counts.keys)
    user_ids_to_counts.transform_keys { |user_id| @cached_users_by_id[user_id] }
  end

  # Private: Load users by id (if not in cache), update the existing cache.
  #
  # user_ids - Array of User ID (Integer)
  #
  # Returns nothing, sets or updates @cached_users_by_id
  def update_cached_users_by_id(user_ids)
    new_user_ids = user_ids - @cached_users_by_id.keys
    new_users_by_id = User.where(id: new_user_ids).index_by(&:id)
    @cached_users_by_id.merge!(new_users_by_id)
  end

  def filter_out_admins_and_maintainers(users_to_counts, limit:)
    # Use a lazy enumerator so we limit calls to async_action_or_role_level_for
    users_to_counts
      .lazy
      .reject { |user, _| is_admin_or_maintainer?(user) }
      .take(limit)
      .to_h
  end

  def is_admin_or_maintainer?(user)
    return @admin_or_maintainer_cache[user] if @admin_or_maintainer_cache.key?(user)

    permission = repository.async_action_or_role_level_for(user).sync
    @admin_or_maintainer_cache[user] = permission.in? [:maintain, :admin]
  end

  def measure_time(key)
    GitHub.dogstats.time "discussions.leaderboard.#{key}" do
      yield
    end
  end
end
