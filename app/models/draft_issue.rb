# frozen_string_literal: true

class DraftIssue < ApplicationRecord::Domain::Memexes
  include GitHub::Validations
  include MemexProjectItem::Content

  TITLE_BYTESIZE_LIMIT = 1024

  belongs_to :memex_project_item

  validates :memex_project_item, presence: true, on: :create
  validates :title, presence: true, allow_nil: false
  validates :title, bytesize: { maximum: TITLE_BYTESIZE_LIMIT }, unicode: true

  # Implements MemexProjectItem::Content#memex_content_hash.
  def memex_content_hash
    { id: id }
  end

  # Implements MemexProjectItem::Content#memex_system_defined_column_value.
  def memex_system_defined_column_value(column, require_prefilled_associations: true)
    # Since `title` is the only system-defined column that draft issues support
    # at the moment, this can simply return nil for all other columns.
    column.name == MemexProjectColumn::TITLE_COLUMN_NAME ? { title: title } : nil
  end
end
