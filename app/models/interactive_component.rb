# frozen_string_literal: true

class InteractiveComponent < ApplicationRecord::Collab
  class InvalidButtonParamError < StandardError; end

  VALID_ELEMENT_KEYS = {
    "type" => nil,
    "value" => nil,
    "text" => nil,
    "attributes" => { "confirm" => nil },
  }

  areas_of_responsibility :ce_extensibility

  # A container can be a composable_comment for now,
  # other types in the future
  belongs_to :container, polymorphic: true

  has_many :interactions, class_name: "InteractiveComponent::Interaction"
  has_many :ephemeral_notices, as: :parent

  delegate :readable_by?, to: :container
  delegate :notify_socket_subscribers, to: :container

  validates_presence_of :container
  validates_presence_of :elements
  validates :external_id, length: { maximum: 500 }, presence: true
  validates_presence_of :container_order

  validate :elements_are_valid_json
  validate :elements_are_valid_structure
  validate :elements_have_only_valid_keys

  validate :at_least_one_element
  validate :no_more_than_three_elements

  scope :without_outdated, -> { where(outdated_at: nil) }

  extend GitHub::Encoding
  force_utf8_encoding :elements

  # Required because external_id is otherwise serialized to external in validations
  def self.human_attribute_name(attr, options = {})
    attr.to_s == "external_id" ? "external_id" : super
  end

  def outdated?
    !outdated_at.nil?
  end

  def process_button_interaction(button_id, user)
    begin
      button_id = Integer(button_id)
    rescue ArgumentError => e
      raise InvalidButtonParamError.new(e.message)
    end
    raise InvalidButtonParamError unless valid_button_id?(button_id)

    interaction = interactions.create(user: user, interacted_at: Time.now.utc)
    deliver_button_click_event(button_id: button_id, user: user, interaction: interaction)

    set_interacted_at
  end

  def integration
    # For now assumes that `integration` is present on the `container
    # In the future it might not be the `integration` but the `container`
    # `owner` that we want
    container.integration
  end

  def parsed_elements
    JSON.parse(elements)
  end

  def element_by_id(element_id)
    parsed_elements[element_id]
  end

  # There shouldn't be a lot of interactions for a given user as these are generated from a human clicking a button
  # so the in memory operations should be ok. We cannot order by id as the interacted_at and id aren't guaranteed to align
  def latest_interaction(user)
    interactions.where(user: user).max_by(&:interacted_at)
  end

  def latest_ephemeral_notice(user)
    ephemeral_notices.where(user: user).order(id: :desc).first
  end

  private

  def valid_button_id?(button_id)
    return false if button_id < 0

    button_id <= parsed_elements.length - 1
  end

  def elements_are_valid_json
    begin
      parsed_elements
    rescue JSON::ParserError, TypeError
      errors.add(:elements, "are not valid JSON")
    end
  end

  def elements_are_valid_structure
    validations = {
      buttons: {
        max_text_length: 25,
        max_value_length: 200,
      },
    }

    elements = parsed_elements

    if elements.any? { |el| el["text"].present? && el["text"].length > validations[:buttons][:max_text_length] }
      errors.add(:elements, "cannot have buttons with text longer than #{validations[:buttons][:max_text_length]}")
    end

    if elements.any? { |el| el["text"].blank? }
      errors.add(:elements, "cannot have buttons with blank text")
    end

    if elements.any? { |el| el["value"].present? && el["value"].length > validations[:buttons][:max_value_length] }
      errors.add(:elements, "cannot have buttons with a value longer than #{validations[:buttons][:max_value_length]}")
    end

    if elements.any? { |el| el["value"].blank? }
      errors.add(:elements, "cannot have buttons with a blank value")
    end

    button_values = elements.group_by { |el| el["value"] }
    non_compliant_button_values = button_values.select { |value, group| value.present? && group.size > 1 }.keys
    unless non_compliant_button_values.empty?
      quoted_values = non_compliant_button_values.map { |n| "\"#{n}\"" }
      errors.add(:elements, "must have unique button values. #{quoted_values.to_sentence} "\
        "#{"was".pluralize(quoted_values.size)} used more than once.")
    end
  end

  def elements_have_only_valid_keys
    parsed_elements.each do |el|
      invalid_keys = el.keys - VALID_ELEMENT_KEYS.keys
      unless invalid_keys.empty?
        errors.add(:elements, "have entries for invalid keys: #{invalid_keys.to_sentence}")
      end

      if attrs = el["attributes"]
        invalid_attr_keys = attrs.keys - VALID_ELEMENT_KEYS["attributes"].keys
        unless invalid_attr_keys.empty?
          errors.add(:elements, "have entries under 'attributes' that are invalid: #{invalid_attr_keys.to_sentence}")
        end
      end
    end
  end

  def at_least_one_element
    if parsed_elements.length < 1
      errors.add(:elements, "at least 1 element required")
    end
  end

  def no_more_than_three_elements
    if parsed_elements.length > 3
      errors.add(:elements, "no more than 3 elements permitted")
    end
  end

  def set_interacted_at
    self.interacted_at = Time.now.utc
    self.save
  end

  def deliver_button_click_event(button_id:, user:, interaction:)
    event_params = {
      action:         :button_click,
      actor_id:       user.id,
      triggered_at:   Time.now.utc,
      integration_id: integration.id,
      interactive_component_id: self.id,
      interaction_id: interaction.id,
      element_id: button_id,
      associations: {},
    }

    case container.associated_domain
    when InteractiveComponent::Container::ASSOCIATED_DOMAINS[:repo]
      event_params[:associations][:repository_id] = container.repository.id
    end

    event = Hook::Event::InteractiveComponentEvent.new(event_params)
    delivery_system = Hook::DeliverySystem.new(event)
    delivery_system.generate_hookshot_payloads
    delivery_system.deliver_later
  end
end
