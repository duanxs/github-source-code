# frozen_string_literal: true

require "chroma"

class ProfileConfigurationFile
  DEFAULT_NAME = "profile.yml"

  def initialize(tree_entry)
    @tree_entry = tree_entry
  end

  def name
    @tree_entry.name
  end

  def specifies_color?
    base_color.present?
  end

  def colors
    chroma_color = Chroma.paint(base_color)
    list = [base_color]

    if chroma_color.dark?
      lighten_color = ->(step) { chroma_color.lighten(step).to_s }
      step = chroma_color.brightness.round / 10
      list.unshift(lighten_color.call(step))
      list.unshift(lighten_color.call(step * 2))
      list.unshift(lighten_color.call(step * 3))
    else
      darken_color = ->(step) { chroma_color.darken(step).to_s }
      step = chroma_color.brightness.round / 12
      list << darken_color.call(step)
      list << darken_color.call(step * 2)
      list << darken_color.call(step * 3)
    end

    list
  end

  def base_color
    yaml["color"] if yaml
  end

  private

  def yaml
    @yaml ||= YAML.safe_load(@tree_entry.data)
  end
end
