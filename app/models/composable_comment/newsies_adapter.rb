# frozen_string_literal: true

# This module implements methods expected by Newsies.
module ComposableComment::NewsiesAdapter

  def subscribe_and_notify
    return unless @notifications&.dig("notify")

    SubscribeAndNotifyJob.perform_later(
      self,
      deliver_notifications: ::GitHub.send_notifications? && deliver_notifications?,
      author: notifications_author,
    )
  end

  def notifications_author
    async_user.sync
  end

  def notifications_thread
    issue
  end

  def async_notifications_list
    async_repository
  end

  def permalink(include_host: true)
    "#{issue.permalink(include_host: include_host)}##{anchor}"
  end

  def entity
    repository
  end

  def async_entity
    async_repository
  end

  def body
    notification_body
  end

  def notification_body
    # Todo: Come up with a way to include
    # interactive components here
    markdown_components.map { |markdown_component| markdown_component.body_text }.join("\n")
  end

  # Public: Gets the RollupSummary for this ComposableComment's thread.
  # See Summarizable.
  #
  # Returns Newsies::Response instance.
  def get_notification_summary
    list = Newsies::List.new("Repository", repository.id)
    GitHub.newsies.web.find_rollup_summary_by_thread(list, issue)
  end

  def update_notification_rollup(summary)
    suffix = summarizable_changed?(:body) ? :changed : :unchanged
    GitHub.dogstats.increment("newsies.rollup", tags: ["type:#{suffix}"])

    summary.summarize_comment(self, user_id: notifications_author.id)
  end

  # Unique identifier for this composable comment. Used in email messages.
  def message_id
    "<#{repository.name_with_owner}/issues/#{issue.number}/#{anchor}@#{GitHub.urls.host_name}>"
  end
end
