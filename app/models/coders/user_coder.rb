# frozen_string_literal: true

module Coders
  class UserCoder < Coders::Base
    data_accessors \
      :deleted,
      :deleted_at,
      :deleted_by,
      :_billing_email,
      :_primary_email,
      :avatar_uuid,
      :gravatar_id,
      :has_used_anonymizing_proxy,
      :hide_jobs_until,
      :needs_ldap_memberships_sync,
      :protocols,
      :raw_login,
      :renamed_at,
      :renaming,
      :repository_navigation_v3_participant,
      :repository_next_participant

    def deleted_at
      time(data[:deleted_at])
    end

    def hide_jobs_until
      time(data[:hide_jobs_until])
    end

    def renamed_at
      time(data[:renamed_at])
    end

    def empty?
      to_h.empty?
    end

    def deleted?
      status = data[:deleted]
      status && status != 0
    end
  end
end
