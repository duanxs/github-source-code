# frozen_string_literal: true

module Coders
  class RepositoryCoder < Coders::Base
    data_accessors :cfg_wiki_permissions,
                   :code_search_enabled,
                   :created_by_user_id,
                   :deleted_at,
                   :deleted_by_user_id,
                   :lock_reason,
                   :primary_language_name

    def created_by_user_id
      object = data[:created_by_user_id]
      object.respond_to?(:id) ? object.id : object
    end

    def deleted_at
      time data[:deleted_at]
    end

    def primary_language_name
      object = data[:primary_language_name]
      object.respond_to?(:name) ? object.name : object
    end
  end
end
