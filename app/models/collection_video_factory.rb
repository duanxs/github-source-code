# frozen_string_literal: true

class CollectionVideoFactory
  def initialize(client)
    @client = client
  end

  def build_from_url(url)
    metadata = build_video_metadata(url)
    if !metadata[:title].blank?
      metadata[:title] = metadata[:title].truncate(40)
      collection_video = CollectionVideo.new(metadata)
    end
    collection_video
  end

  private def build_video_metadata(url)
    metadata = {}
    case url
    when /youtube\.com|youtu\.be/
      if url.include?("embed")
        metadata[:url] = url
        id = url.split("/").last.split("?").first
        metadata[:thumbnail_url] = build_youtube_thumbnail_url(id)
        url_with_metadata = "https://www.youtube.com/watch?v=#{id}"
        metadata[:title], metadata[:description] = CollectionItemMetadata.build_from_url(url_with_metadata)
      else
        id = url.split("v=").last
        metadata[:url] = "https://www.youtube.com/embed/#{id}"
        metadata[:thumbnail_url] = build_youtube_thumbnail_url(id)
        metadata[:title], metadata[:description] = CollectionItemMetadata.build_from_url(url)
      end
    else
      id = extract_vimeo_id(url)
      metadata = get_vimeo_meta_data(id)
      metadata[:url] = url.include?("player.vimeo.com") ? url : "https://player.vimeo.com/video/#{id}"
    end
    metadata
  end

  private def build_youtube_thumbnail_url(id)
    "https://img.youtube.com/vi/#{id}/0.jpg"
  end

  private def get_vimeo_meta_data(id)
    begin
      metadata = {}
      video_response = @client.get_video(id)
      thumbnails = video_response["pictures"]["sizes"]
      metadata[:thumbnail_url] = thumbnails.select { |thumbnail| thumbnail["width"].eql?(200) }.map { |thumbnail| thumbnail["link"] }[0]
      metadata[:title] = video_response["name"]
      metadata[:description] = video_response["description"]
    rescue VimeoClient::VimeoClientError => e
      metadata = {}
    end
    metadata
  end

  private def extract_vimeo_id(url)
    url.split("/").last.split("?").first
  end
end
