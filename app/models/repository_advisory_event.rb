# frozen_string_literal: true

# Records events that occur around a RepositoryAdvisory.
# Attributes
# integer   :repository_advisory_id
# integer   :actor_id
# string    :event
# string    :changed_attribute
# string    :value_was
# string    :value_is
class RepositoryAdvisoryEvent < ApplicationRecord::Collab
  include RepositoryAdvisory::AfterPublication
  include Instrumentation::Model

  extend GitHub::Encoding
  force_utf8_encoding :value_was, :value_is

  belongs_to :repository_advisory
  belongs_to :actor, class_name: "User", foreign_key: :actor_id

  VALID_EVENTS = %w{
    closed
    reopened
    published
    renamed
    github_rejected
    github_withdrawn
    github_published
    cve_requested
    cve_assigned
    cve_not_assigned
    credit_accepted
    credit_declined
  }.freeze

  VALID_CHANGED_ATTRIBUTES = %w{
    title
    state
    review_state
    cve_id
    credit
  }.freeze

  INSTRUMENTED_EVENTS = {
    github_rejected: :github_reject,
    github_withdrawn: :github_withdraw,
    github_published: :github_broadcast,
    cve_requested: :cve_request,
    cve_assigned: :cve_assignment,
    cve_not_assigned: :declined_cve_assignment,
  }

  validates :repository_advisory, presence: true
  validates :actor, presence: true
  validates :event, inclusion: VALID_EVENTS, presence: true
  validates :changed_attribute, inclusion: VALID_CHANGED_ATTRIBUTES, presence: true

  after_create :instrument_cve_decision_time
  after_commit :instrument_event, on: :create

  def name
    event
  end

  def closed?
    event == "closed"
  end

  def reopened?
    event == "reopened"
  end

  def published?
    event == "published"
  end

  def renamed?
    event == "renamed"
  end

  def github_published?
    event == "github_published"
  end

  def github_withdrawn?
    event == "github_withdrawn"
  end

  def cve_assigned?
    event == "cve_assigned"
  end

  def cve_not_assigned?
    event == "cve_not_assigned"
  end

  def credit_accepted?
    event == "credit_accepted"
  end

  def credit_declined?
    event == "credit_declined"
  end

  def credit_event?
    credit_accepted? || credit_declined?
  end

  def readable_by?(viewer)
    async_readable_by?(viewer).sync
  end

  def async_readable_by?(viewer)
    # Non-collaborators are allowed to see their own advisory credit events,
    # but no other events.
    if credit_event? && viewer.is_a?(User) && viewer.id == actor_id
      return Promise.resolve(true)
    end

    async_repository_advisory.then do |advisory|
      # Collaborators are allowed to see advisory events.
      advisory.async_writable_by?(viewer)
    end
  end

  def instrument_event
    instrument INSTRUMENTED_EVENTS[event.to_sym] if INSTRUMENTED_EVENTS.keys.include?(event.to_sym)
  end

  def event_payload
    repository_advisory.event_payload
  end

  def event_prefix
    :repository_advisory
  end

  private

  def instrument_cve_decision_time
    return unless cve_assigned? || cve_not_assigned?

    request_times = RepositoryAdvisoryEvent.
      where(repository_advisory_id: repository_advisory_id, event: "cve_requested").
      pluck(:created_at).
      compact

    return if request_times.empty?

    earliest_request_time = request_times.min
    latest_request_time = request_times.max
    tags = ["decision:#{event}"]

    GitHub.dogstats.timing_since(
      "repository_advisory_event.cve_decision.total_time",
      earliest_request_time,
      tags: tags,
    )

    GitHub.dogstats.timing_since(
      "repository_advisory_event.cve_decision.time",
      latest_request_time,
      tags: tags,
    )
  end
end
