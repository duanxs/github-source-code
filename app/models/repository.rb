# rubocop:disable Style/FrozenStringLiteralComment

# coding: utf-8
class Repository < ApplicationRecord::Domain::Repositories
  areas_of_responsibility :repositories
  include Entity

  include ActionView::Helpers::NumberHelper
  include Ability::Subject
  include Ability::Membership
  include GitHub::FlipperActor
  include GitHub::Validations
  include GitHub::UTF8
  include GitHub::Relay::GlobalIdentification

  include Dumpable
  include GitRepository::UrlMethods
  include Repository::ConsistencyCheck
  include Repository::Backup
  include Repository::DiskQuota
  include Repository::StaffTools
  include Repository::StorageAdapter
  include Repository::LegalHold
  include TreeListable

  include SubscribableList

  include Spam::Spammable

  include Configurable::RepositoryExportedToUrl
  include Configurable::AllowPrivateRepositoryForking
  include Configurable::AllowAutoDeletingBranches
  include Configurable::TokenScanning
  include Configurable::SecuritySettings
  include Configurable::CodeScanning
  include StaffAccessible

  include Permissions::Attributes::Wrapper
  self.permissions_wrapper_class = Permissions::Attributes::Repository

  extend GitHub::BackgroundDependentDeletes

  class CorruptionDetected < StandardError
    def areas_of_responsibility
      [:git]
    end
  end

  include Repository::AbilityDependency
  include Repository::ActionsDependency
  include Repository::AdvisoryDependency
  include Repository::ArchiveCommandDependency
  include Repository::CheckDependency
  include Repository::CodeScanningDependency
  include Repository::ConfigRepoDependency
  include Repository::ConfigurationDependency
  include Repository::DependabotDependency
  include Repository::DependenciesDependency
  include Repository::DiscussionsDependency
  include Repository::EditorConfigDependency
  include Repository::FeatureFlagsDependency
  include Repository::RepositoryFundingLinksDependency
  include Repository::HovercardDependency
  include Repository::ImageDependency
  include Repository::LanguageAnalysisDependency
  include Repository::MarketplaceDependency
  include Repository::MergeQueueDependency
  include Repository::MilestoneDependency
  include Repository::NetworkDependency
  include Repository::NetworkPrivilegeDependency
  include Repository::OauthApplicationPolicyDependency
  include Repository::OrganizationsDependency
  include Repository::PermissionExportDependency
  include Repository::PermissionsDependency
  include Repository::PinnedIssuesDependency
  include Repository::PlanDependency
  include Repository::PorterDependency
  include Repository::ProjectsDependency
  include Repository::ReflogDependency
  include Repository::RefsDependency
  include Repository::RemovalDependency
  include Repository::RestoreDependency
  include Repository::RoleBasedPermissionsDependency
  include Repository::RpcDependency
  include Repository::SidebarSectionVisibilityDependency
  include Repository::SlumlordDependency
  include Repository::SponsorsDependency
  include Repository::StatusesDependency
  include Repository::TemplateDependency
  include Repository::TieredReportingDependency
  include Repository::TokenScanningDependency
  include Repository::TopicsDependency
  include Repository::TwoFactorRequirementDependency
  include Repository::UserRankedDependency
  include Repository::VulnerabilityDependency
  include Repository::WikiDependency
  include Repository::WorkflowsDependency

  require_dependency "repository/creator"
  require_dependency "repository/codeowners"
  require_dependency "repository/sequence"

  include GitRepository::CommitsDependency
  include Repository::Sequence
  setup_sequence

  class << self
    attr_accessor :per_page
  end
  self.per_page = 30

  PUBLIC_VISIBILITY = "public"
  PRIVATE_VISIBILITY = "private"
  INTERNAL_VISIBILITY = "internal"

  VISIBILITIES = [
    PUBLIC_VISIBILITY,
    PRIVATE_VISIBILITY,
    INTERNAL_VISIBILITY
  ].freeze

  MENTIONABLES_LIMIT = 1_000

  MEMBER_UPDATE_BATCH_SIZE = 100

  # Used to determine an upper limit on how many users can be returned in a JSON response for
  # user suggestions.
  JSON_USER_MENTION_LIMIT = 50_000

  NAME_WITH_OWNER_PATTERN = /\A[^\/]+\/[^\/]+\z/

  URI_TEMPLATE = Addressable::Template.new("/{owner}/{name}").freeze

  COMMITS_URI_TEMPLATE = Addressable::Template.new("/{owner}/{name}/commits").freeze

  # Used as the default upper limit for org membership when checking if participants have
  # access to a repo. Checking repo access in orgs with extremely large memberships (AutoDesk)
  # can cause requests to timeout
  ORG_MEMBERSHIP_VERIFICATION_LIMIT = 10_000

  # The repository name for organization level health files
  GLOBAL_HEALTH_FILES_NAME = ".github"

  # If we want to be nice, we bump this up from the default of 20 in dotcom
  INCREASED_HOOK_LIMIT = 30

  ALLOWED_OWNER_TYPES = %w(User Organization).freeze

  REPO_STARGAZERS_BATCH_SIZE = 1000

  # This method returns the name of the column that stores the stargazer count.
  # The column is currently named `watcher_count` for legacy reasons, but we'll
  # probably rename it at some point in the future. Any code referencing this
  # column in SQL fragments should use this method instead of hard coding
  # "watcher_count"
  def self.stargazer_count_column
    "watcher_count"
  end

  extend GitHub::RenameColumn
  rename_column watcher_count: :stargazer_count

  # The stock #read_attribute_for_serialization method is an alias for #send
  # which blows up when it tries to read the value of the watcher_count column
  # (since rename_column makes the #watcher_count method raise).
  # Aliasing it to #[] instead bypasses this problem.
  alias_method :read_attribute_for_serialization, :[]

  scope :public_scope,               -> { where(public: true) }
  scope :private_scope,              -> { where(public: false) }
  scope :private_not_internal_scope, -> { where(public: false).where("repositories.id NOT IN (SELECT repository_id FROM internal_repositories)") }
  scope :internal_scope,             -> { where(public: false).where("repositories.id IN (SELECT repository_id FROM internal_repositories)") }
  scope :archived_scope,             -> { where(maintained: false) }
  scope :not_archived_scope,         -> { where(maintained: true) }
  scope :active,                     -> { where(active: true) }
  scope :not_deleted,                -> { where(deleted: false) }

  scope :public_or_accessible_by, -> (viewer) {
    if viewer
      accessible_repo_ids = Ability.where(
        subject_type: "Repository",
        actor_id: viewer.id,
        actor_type: viewer.ability_type,
        priority: Ability.priorities[:direct],
      ).pluck(:subject_id)

      active.where("repositories.public = ? OR repositories.id IN (?)", true, accessible_repo_ids)
    else
      active.where("repositories.public = ?", true)
    end
  }

  # Repositories that are the roots of their network.
  scope :network_roots, -> {
    where(parent_id: nil).order("repositories.id ASC")
  }
  scope :from_ids, -> (repo_ids) { where(id: repo_ids) }

  DEFAULT_ASC_SORT_FIELDS = [:full_name, :id, :name]

  # Warning:
  # When sorting by :full_name, this scope executes the relation.
  # Do not apply :full_name sort unless the scope is adequately scoped down.
  # All of a users repos is OK. Repository.all is not OK. :)
  scope :sorted_by, -> (order_str, direction) {
    opts = {}

    order_str = :full_name if order_str.blank?
    order_str = order_str.to_sym

    default_dir = "DESC"
    default_dir = "ASC" if DEFAULT_ASC_SORT_FIELDS.include?(order_str)

    dir = if direction.blank?
      default_dir
    else
      direction.to_s.downcase == "asc" ? "ASC" : "DESC"
    end

    case order_str
    when :created
      opts[:order] = "repositories.created_at #{dir}"
    when :updated
      opts[:order] = "repositories.updated_at #{dir}"
    when :pushed
      opts[:order] = "repositories.pushed_at #{dir}"
    when :full_name
      opts[:order] = "repositories.owner_login #{dir}, repositories.name #{dir}"
    when :name
      opts[:order] = "repositories.name #{dir}"
    when :id
      opts[:order] = "repositories.id #{dir}"
    else
      opts[:order] = "repositories.created_at #{dir}"
    end

    order(Arel.sql(opts[:order]))
  }

  scope :sorted_by_name, -> { order("repositories.name ASC") }
  scope :not_forks, -> { where(parent_id: nil) }
  scope :forks, -> { where("repositories.parent_id IS NOT NULL") }
  scope :forks_owned_by, -> (user) { where("repositories.owner_id = ? AND repositories.parent_id IS NOT NULL", user.id) }
  scope :forks_of, -> (repo) { where(parent_id: repo.id, active: true) }
  scope :organization_member_private_forks, -> (org, members) {
    member_ids = members.is_a?(ActiveRecord::Relation) ? members.pluck(:id) : members.map(&:id)

    private_scope.where(organization_id: org.id, owner_id: member_ids)
  }
  scope :with_owner, -> { active.not_deleted }

  def self.private_forks_for(organization:, belonging_to_user:)
    includes(:parent)
      .where(parents_repositories: {public: false, owner_id: organization.id})
      .where(owner_id: belonging_to_user.id)
  end

  # Returns repositories owned by `belonging_to_user` that are forks (or forks of forks) of internal
  # repositories in orgs belonging to `business`.
  def self.biz_internal_forks_for(business:, belonging_to_user:)
    org_ids = business.organizations.pluck(:id)
    joins(:network)
      .joins("INNER JOIN `repositories` AS `root_repository` ON `repository_networks`.`root_id` = `root_repository`.`id`")
      .joins("INNER JOIN `internal_repositories` ON `root_repository`.`id` = `internal_repositories`.`repository_id`")
      .where(owner_id: belonging_to_user.id)
      .where("`root_repository`.`owner_id` IN (?)", org_ids)
  end

  scope :owned_by, lambda { |user| where(owner_id: user.id) }
  scope :not_owned_by, lambda { |user| where("repositories.owner_id != ?", user.id) }
  scope :owned_by_org, lambda { |org| where(organization_id: org.id) }
  scope :with_language, lambda { |language| where(primary_language_name_id: language.id) }

  scope :user_owned, -> do
    where("repositories.organization_id IS NULL OR repositories.organization_id != repositories.owner_id")
  end

  scope :org_owned, -> do
    where("repositories.organization_id = repositories.owner_id")
  end

  scope :in_same_network_as, lambda { |repo| where(source_id: repo.source_id) }

  scope :with_issues_enabled,  -> { where(has_issues: true) }
  scope :with_issues_disabled, -> { where(has_issues: false) }

  scope :since, lambda { |time|
    where("repositories.updated_at >= ?", time).order("repositories.updated_at ASC")
  }
  scope :before, lambda { |time|
    where("repositories.updated_at <= ?", time).order("repositories.updated_at DESC")
  }

  scope :recently_updated,
    -> { order("repositories.pushed_at DESC, repositories.created_at DESC") }
  scope :locked_repos, -> { where(locked: true) }
  scope :unlocked_repos, -> { where(locked: false) }

  scope :search, lambda { |query|
    with_prefix(:name, query)
  }

  # Public: Repositories whose organization attribute is set
  scope :with_organization, lambda {
    where.not(organization_id: nil).preload(:organization)
  }

  scope :is_not_disabled, -> { where(disabled_at: nil) }

  scope :filter_spam_and_disabled_for, ->(viewer) {
    relation = filter_spam_for(viewer)

    # Hide DMCA takedown repos from non-site-admins:
    relation = relation.is_not_disabled unless viewer&.site_admin?

    relation
  }

  FIELDS_COPIED_ON_FORK = %w(
    name
    source_id
    updated_at
    pushed_at
    pushed_at_usec
    public
    description
    homepage
    disk_usage
    primary_language_name_id
    has_wiki
    has_downloads
    template
  ).freeze

  belongs_to :owner, class_name: "User"
  belongs_to :parent, class_name: "Repository"
  belongs_to :network, class_name: "RepositoryNetwork", foreign_key: "source_id"

  has_one :business, through: :organization

  setup_spammable(:owner)

  # Internal: an abstract collection, for the sub-resources of a Repository available for abilities
  def resources
    Repository::Resources.new(self)
  end

  has_many :attachments, as: :entity
  has_many :assets, through: :attachments, split: true

  has_many :children,
    -> { where(active: true) },
    class_name: "Repository",
    foreign_key: :parent_id
  alias_method :forks, :children

  has_many :private_network_repositories,
    -> { where(active: true, public: false) },
    class_name: "Repository",
    foreign_key: :source_id,
    primary_key: :source_id

  has_many :stars, as: :starrable

  has_many :close_issue_references, foreign_key: "issue_repository_id"
  destroy_dependents_in_background :close_issue_references

  has_many :repository_invitations
  destroy_dependents_in_background :repository_invitations

  has_one :repository_sequence, dependent: :destroy, autosave: false

  has_one :disabled_access_reason, as: :flagged_item

  has_one :community_profile, dependent: :destroy

  has_one :repository_import, class_name: "ImportedRepository", dependent: :destroy
  has_one :import, through: :repository_import, class_name: "Import"

  has_many :commit_mentions
  destroy_dependents_in_background :commit_mentions

  has_many :public_keys,     extend: PublicKey::CreationExtension
  destroy_dependents_in_background :public_keys

  # always includes all hooks for the repo
  # including web, email and legacy service hooks
  # see https://github.com/github/github/issues/106423 for more details
  #
  # should not be used other than for destroy_dependents_in_background
  # _technically_ the way that destroy_dependents_in_background is
  # currently implemented, this would work for `:hooks` to, but let's
  # be very explicit
  has_many :all_hooks,
    class_name: "Hook",
    as: :installation_target
  destroy_dependents_in_background :all_hooks

  # does not return any non-web hooks (aka service hooks)
  # since they have been deprecated for a while.
  # The only way to access service hooks is via .all_hooks
  has_many :hooks,
    -> { where("hooks.name" => "web") },
    class_name: "Hook",
    as: :installation_target

  has_many :webhooks,
    -> { where("hooks.name" => "web") },
    class_name: "Hook",
    as: :installation_target

  has_many :email_hooks,
    -> { where("hooks.name" => "email") },
    class_name: "Hook",
    as: :installation_target

  # never includes web hooks, and also does not include "email" hooks
  # when the flag is enabled, as they are no longer hooks but are just
  # configuration for repository notifications
  has_many :service_hooks,
    -> { where("hooks.name NOT IN ('email', 'web')") },
    class_name: "Hook",
    as: :installation_target

  has_many :languages, dependent: :delete_all, autosave: true

  has_many :tabs
  destroy_dependents_in_background :tabs

  has_many :key_links, as: :owner
  destroy_dependents_in_background :key_links

  has_many :codespaces

  has_many :commit_comments, class_name: "CommitComment", inverse_of: :repository
  destroy_dependents_in_background :commit_comments

  has_many :downloads,
    -> { order("downloads.id DESC") },
    dependent: :destroy

  has_many :discussions
  destroy_dependents_in_background :discussions

  # No need to destroy comments when repository is destroyed.
  # They are destroyed in background when discussion is destroyed.
  has_many :discussion_comments

  has_many :discussion_categories
  destroy_dependents_in_background :discussion_categories

  has_many :discussion_spotlights, -> { order(:position) }
  destroy_dependents_in_background :discussion_spotlights

  has_many :projects,
    foreign_key: :owner_id,
    as: :owner
  destroy_dependents_in_background :projects

  has_many :protected_branches
  destroy_dependents_in_background :protected_branches

  has_many :releases
  destroy_dependents_in_background :releases

  has_many :release_assets
  has_many :repository_files

  has_many :packages, class_name: "Registry::Package"
  destroy_dependents_in_background :packages
  has_many :package_files, through: :packages

  has_many :redirects,  class_name: "RepositoryRedirect"
  delete_dependents_in_background :redirects

  has_many :commit_contributions
  delete_dependents_in_background :commit_contributions

  # Join records to clones of this repository where this repository is a template
  has_many :repository_clones, foreign_key: "template_repository_id"
  delete_dependents_in_background :repository_clones

  # The join record for this repository to the template repository it was cloned from, if any
  has_one :template_repository_clone, foreign_key: "clone_repository_id",
    class_name: "RepositoryClone", dependent: :destroy

  has_many :labels, dependent: :delete_all

  has_many :profile_pins, as: :pinned_item, dependent: :destroy
  has_many :user_dashboard_pins, as: :pinned_item, dependent: :destroy

  has_many :repository_vulnerability_alerts, dependent: :destroy,
    extend: RepositoryVulnerabilityAlert::ForManifestAndPackage
  has_many :visible_vulnerability_alerts, -> { visible }, class_name: "RepositoryVulnerabilityAlert"

  has_many :check_suites
  destroy_dependents_in_background :check_suites

  has_many :workflows, class_name: "Actions::Workflow"
  destroy_dependents_in_background :workflows

  has_many :workflow_runs, -> { order(id: :desc) }, class_name: "Actions::WorkflowRun"
  # Workflow runs also belong to check suites. They are deleted in background from the check suite model

  has_many :project_repository_links, dependent: :destroy
  has_many :linked_projects, through: :project_repository_links, source: :project

  # FIXME Replace with has_many :saved_replies
  def saved_replies
    SavedReply.where(id: -1)
  end

  validates_presence_of :name, :owner_id, on: :create

  before_validation lambda { self[:description] = nil }, if: lambda { will_save_change_to_description? && description.blank? }
  validates :description,
    format: { without: /[[:cntrl:]]/, message: "control characters are not allowed" },
    allow_nil: true,
    if: :will_save_change_to_description?
  validates :description, unicode: true, bytesize: { maximum: MYSQL_UNICODE_BLOB_LIMIT }, if: :will_save_change_to_description?

  extend GitHub::Encoding
  force_utf8_encoding :description

  has_many :transfers,    class_name: "RepositoryTransfer"
  destroy_dependents_in_background :transfers

  has_many :user_roles, as: :target, dependent: :destroy
  has_many :roles, as: :owner, dependent: :destroy

  has_many :ghvfs_replicas, class_name: "GHVFS::Replica"

  has_many :abuse_reported_to_maintainer,
    -> { where(show_to_maintainer: true) },
    class_name: "AbuseReport",
    foreign_key: :repository_id

  def entity
    self
  end

  def change_owner_of!(project:, creator:, old_owner:)
    # Clear out all abilities, since repo projects just inherit their repo's
    # abilities.
    Ability.clear(project)

    # Remove any project links that exist
    project.unlink_repositories
  end

  # Public: Create a new commit in this repository.
  #
  # parent_oid - String containing the OID of the new commit's
  #              parent. If there is no parent,
  #              nil and "" both work, but nil is preferred.
  #              Strings that are not valid OIDs, or parents
  #              that not members of the repository, are
  #              prohibited.
  # message    - String message. Required.
  # author     - An instance of User. This is typically the
  #              currently logged-in user.
  # files      - A dictionary (typically a Hash) representing
  #              the collected changes to be made for this commit.
  #              Each key->value mapping represents one of:
  #              (a) A create or update, represented as
  #              String path => String new contents, or (b)
  #              A deletion, represented as String path => nil,
  #              or (c) A moved file, represented as String
  #              new path => {:from => String old path,
  #              :contents => String new or same contents}
  # sign:       - Boolean of whether to try to sign the commit.
  #
  # Returns: An instance of Commit, or false if Commit#create
  #          (which it ultimately calls) raises certain exceptions.
  #          For example, if the parent_oid is a valid oid string but
  #          does not represent a parent commit in this respoistory,
  #          this method will return false.
  #
  # Raises: Does not directly raise any exceptions, but will pass through
  # some exceptions. For example, if parent_oid is not a valid oid string,
  # an exception will bubble up to the caller.
  def create_commit(parent_oid, message:, author: nil, files:, author_email: nil, committer: nil, sign: false)
    metadata = { message: message, author: author, author_email: author_email, committer: committer }
    metadata[:authored_date] = Time.zone.now.iso8601

    self.commits.create(metadata, parent_oid.presence, sign: sign) do |commit_files|
      files.each do |filename, contents|
        if contents.is_a?(Hash)
          commit_files.move(contents[:from].b, filename.b, contents[:contents].b)
        elsif contents
          commit_files.add(filename.b, contents.b)
        else
          commit_files.remove(filename.b)
        end
      end
    end
  rescue Git::Ref::ComparisonMismatch, GitRPC::Failure, ArgumentError => e
    false
  end

  def network_repositories
    network.repositories
  end

  def privileged_ids
    user_ids_with_privileged_access(min_action: :read).compact
  end

  def available_assignee_ids
    user_ids_with_read = privileged_ids
    user_ids_with_read - suspended_user_ids(user_ids_with_read)
  end

  def suspended_user_ids(privileged_ids)
    return [] if privileged_ids.empty?
    User.where(id: privileged_ids).suspended.pluck(:id)
  end

  def available_assignees
    User.where(id: available_assignee_ids).includes(:profile)
  end

  def outside_collaborators_ids
    collection = Set.new(member_ids)

    if in_organization?
      collection = collection - organization.member_ids
    end

    collection
  end

  def direct_or_team_member_ids(viewer:, immediate_only: true)
    @direct_or_team_member_ids ||= begin
      collection = Set.new(member_ids)

      if in_organization?
        if organization.default_repository_permission != :none
          collection.merge(organization.visible_user_ids_for(viewer, type: :all))
        end

        collection.merge(all_team_member_ids(include_org_admins: true, immediate_only: immediate_only))
      else
        collection << owner_id
      end

      collection
    end
  end

  def direct_member_ids
    collection = Set.new(member_ids)
    collection << owner_id unless in_organization?
    collection
  end

  def owner_login
    super || owner.login
  end

  # Label association dirty tracking.
  alias_method :original_labels=, :labels=
  def labels=(labels)
    @labels_changed = true
    self.original_labels = labels
  end

  # Populate repo with some initial labels.
  #
  # Inspired by jashkenas/coffee-script's labels.
  def populate_initial_labels
    # Return if labels are explicitly set
    return if @labels_changed || labels.count > 0

    initial_labels = if owner.organization?
      owner.user_labels.map { |label| label.label_attributes }
    else
      Label.initial_labels
    end

    initial_labels.each do |hash|
      new_label = labels.create(hash)
      new_label.instrument_creation(context: "repository creation") if new_label.persisted?
    end
  end

  # Finds existing Labels for this Repository.
  #
  # label_names - Array of Integer label IDs.
  #
  # Returns an Array of Label objects.
  def find_labels(label_ids)
    labels.where(id: label_ids).to_a
  end

  # Public: Find Labels with the given names, case-insensitive, in this Repository.
  #
  # names - a single String or an Array of Strings
  #
  # Returns an ActiveRecord relation.
  def find_labels_by_name(names)
    labels.with_name(names)
  end

  # Finds existing Labels for this Repository.
  #
  # names - Array of Integer label IDs input by a user.
  #
  # Returns an Array of Label objects.
  def load_labels(ids)
    label_loader.fetch ids
  end

  # Helper method to find the 'help wanted' label
  #
  # returns Label or nil
  def help_wanted_label
    return @help_wanted_label if defined?(@help_wanted_label)
    @help_wanted_label = labels.help_wanted || labels.similar_to_help_wanted
  end

  # Helper method to find the 'good first issue' label
  #
  # returns Label or nil
  def good_first_issue_label
    return @good_first_issue_label if defined?(@good_first_issue_label)
    @good_first_issue_label = labels.good_first_issue || labels.similar_to_good_first_issue
  end

  def label_loader
    @label_loader ||= GitHub::LabelLoader.new(self)
  end

  # Public: Determine which icon to show for the repository.
  #
  # Returns a String.
  def repo_type_icon
    @repo_type_icon ||= begin
      if fork? && parent
        "repo-forked"
      elsif mirror
        public? ? "mirror" : "lock"
      else
        public? ? "repo" : "lock"
      end
    end
  end

  # Returns a sorted collection of Labels for this Repository.
  #
  # issue_or_pr - Optional Issue or PullRequest. When provided, the labels that
  #   have already been applied to this object are sorted to the top of the
  #   returned list.
  #
  # Returns an Array of Label objects.
  def sorted_labels(issue_or_pr: nil, cache_label_html: false)
    available_labels = Label.smart_sort(labels.order("name"), false)

    if issue_or_pr
      issue = issue_or_pr.is_a?(PullRequest) ? issue_or_pr.issue : issue_or_pr
      available_labels = available_labels
        .partition { |l| issue.unique_label_ids.include?(l.id) }
        .flatten
    end

    Promise.all(available_labels.map(&:async_name_html)).sync if cache_label_html

    available_labels
  end

  has_many :conversations, as: :context

  has_many :issue_events
  has_many :issue_comments
  has_many :deleted_issues
  has_many :issues
  destroy_dependents_in_background :issues

  has_many :milestones, dependent: :destroy, inverse_of: :repository

  has_many :pull_requests
  has_many :pull_request_review_comments

  has_many :pull_requests_as_head,
    class_name: "PullRequest",
    foreign_key: :head_repository_id

  has_many :deployments,
    -> { order("deployments.id desc") },
    inverse_of: :repository
  destroy_dependents_in_background :deployments

  has_one :page, dependent: :destroy, inverse_of: :repository

  has_one :mirror, dependent: :destroy

  has_one :repository_license, dependent: :destroy
  has_one :internal_repository, dependent: :destroy

  belongs_to :primary_language, class_name: "LanguageName", foreign_key: :primary_language_name_id

  validates_length_of       :name, in: 1..100
  validates_exclusion_of    :name, in: %w( . .. followers following repositories )
  validate :name_is_not_a_wiki_repo
  validates_numericality_of :owner_id

  validates_with PrivateForkValidator, on: :create

  before_validation :normalize_name
  before_validation :set_organization, on: :create

  validate :ensure_owner_is_not_trade_controls_restricted, on: :create
  validate :ensure_creator_is_not_trade_controls_restricted, on: :create
  validate :ensure_uniqueness_of_name
  validate :ensure_uniqueness_of_fork_in_network, on: :create
  validate :ensure_gitignore_template_exists, on: :create
  validate :ensure_license_template_exists, on: :create
  validate :ensure_owner_has_enough_repo_quota, on: :create
  validate :ensure_projects_can_be_enabled, on: :create
  validate :ensure_owner_is_a_user_or_organization

  after_create :initialize_repository_network,
               :setup_git_repository_if_exists,
               :calculate_network_counts!,
               :populate_initial_labels,
               :populate_initial_discussion_categories,
               :clear_contributions_cache

  after_commit :setup_git_repository,
               :instrument_creation,
               :add_to_owners_team,
               :add_repository_to_team,
               :instrument_repo_added_to_installations_across_all_repositories,
               on: :create

  after_commit :auto_subscribe_owners, on: :create

  after_commit :setup_workspace_abilities, on: :create, if: :advisory_workspace?

  after_commit  :synchronize_search_index

  after_destroy :synchronize_search_index,
                :clear_contributions_cache

  before_save :set_made_public_at, if: :will_save_change_to_public?
  before_save :set_owner_login, if: :owner_id_changed?
  before_save :update_world_writable_status

  before_create :set_made_public_at
  before_create :initialize_has_discussions

  after_save :enqueue_calculate_user_stars_count, if: :saved_change_to_public?
  after_save :enqueue_community_health_check, if: :community_health_factors_changed?
  after_save :track_community_health_when_made_public, if: :saved_change_to_public?
  after_save :populate_initial_discussion_categories, if: :saved_change_to_has_discussions?

  after_save :delist_actions, if: :private_or_deleted?

  if !Rails.test?
    after_commit :enqueue_check_for_spam,
                 on: :create
  else
    cattr_accessor :checking_for_spam
    after_commit :enqueue_check_for_spam,
                 on: :create,
                 if: :checking_for_spam
  end

  before_destroy :instrument_repo_removed_from_installations

  after_destroy :calculate_network_counts!,
                :destroy_repository_network

  after_commit :delete_all_newsies_data, on: :destroy

  alias_attribute     :to_s, :name
  alias_attribute :to_param, :name
  alias_attribute     :user, :owner

  ##
  # Caching!
  after_commit :synchronize_issues_search_index
  after_commit :synchronize_discussions_search_index, if: -> { previous_changes.key?("has_discussions") }

  after_validation :record_name_change
  after_save :unpin_from_profiles, if: :made_private?
  after_save :unfeature_from_sponsors_profile, if: :made_private?
  after_save :disable_interaction_limits, if: :made_private?

  after_commit :initialize_replicas_from_network,
               if: :needs_dgit_initialization_after_commit?,
               on: :create

  after_commit :update_repository_projects_setting, if: :repository_projects_setting_changed?

  after_commit :delete_replicas_and_checksums_after_commit_on_destroy,
               on: :destroy

  after_commit :instrument_update, on: :update
  after_commit :snapshot_license_state, if: :has_business_owner?

  # Destroying the user-page can change the URL for all project-pages, so we
  # rebuild everything.
  after_destroy :queue_propagate_https_redirect, if: :is_cname_user_pages_repo?

  serialize :raw_data, Coders::Handler.new(Coders::RepositoryCoder)
  delegate *Coders::RepositoryCoder.members, to: :raw_data
  delegate :has_any_trade_restrictions?, to: :plan_owner

  attr_accessor :license_template, :gitignore_template, :auto_init, :template_hook_failure, :reflog_data
  alias_method :auto_init?, :auto_init

  # Override Dumpable.dumpable_options
  def self.dumpable_options(options)
    options.to_activerecord_options.update(include: :owner)
  end

  def self.dump_public(options = nil)
    protected_org_ids = options&.delete(:protected_organization_ids)
    options = Dumpable::Options.from(options)
    if options.custom_conditions?
      raise ArgumentError, "Use #dump_page to pass custom conditions"
    end
    if protected_org_ids.present?
      options.conditions = ["public = 1 AND (organization_id NOT IN (?) OR organization_id IS NULL)", protected_org_ids]
    else
      options.conditions = "public = 1"
    end
    dump_page(options)
  end

  def self.dump_all(options = nil)
    options = Dumpable::Options.from(options)
    if options.custom_conditions?
      raise ArgumentError, "Use #dump_page to pass custom conditions"
    end
    dump_page(options)
  end

  # Public: Is the given branch name or commit SHA one that exists in this repository?
  #
  # maybe_branch - String branch name or commit SHA or ref
  #
  # Returns a Boolean.
  def valid_branch?(maybe_branch)
    extractor = GitHub::RefShaPathExtractor.new(self)
    branch, _ = extractor.call(maybe_branch)
    !branch.nil?
  end

  # Check if a repository is open source. github/github is never open source.
  def public?
    return false if GitHub.never_public_network_ids.include? network_id
    read_attribute :public
  end

  # Public: Check whether the repository is private.
  def private?
    !public?
  end

  # Public: Check whether the repository is internal.
  def internal?
    return false if public?
    visibility == INTERNAL_VISIBILITY
  end

  # Public: True if a repository is fully private (not internal) and the root of its network.
  def private_network_root?
    network_root? && visibility == PRIVATE_VISIBILITY
  end

  # Public: Set the repository access permission to private. This only modifies
  # the database state if the record already exists.
  #
  # value - Boolean specifying whether the repository should be considered
  # private.
  #
  # Returns opposite of value.
  def private=(value)
    self.public = !value
  end

  # Public: The repository's visibility.
  #
  # Returns 'public', 'private', or 'internal'.
  def visibility
    async_visibility.sync
  end

  def async_visibility
    async_internal_repository.then do |internal_repository|
      internal_repository ? INTERNAL_VISIBILITY : public_or_private_visibility
    end
  end

  # Public: The repository's public or private visibility.
  def public_or_private_visibility
    public? ? PUBLIC_VISIBILITY : PRIVATE_VISIBILITY
  end

  # Public: The repository's visibility once toggled.
  #
  # An enterprise-internal repository is considered private, so its toggled visibility is public.
  #
  # Returns 'public' or 'private'
  def toggled_visibility
    public? ? PRIVATE_VISIBILITY : PUBLIC_VISIBILITY
  end

  # Internal: Set the permission (public/private) bit to the given
  # value in the database. This should not be called directly because
  # there are no limit checks. Use the Repository#toggle_visibility
  # instead.
  #
  # visibility - :public, :private, or :internal
  #
  # Return true if the visibility was changed, false if the visibility was
  # already set appropriately.
  def set_permission(visibility)
    visibility = visibility.to_s
    unless VISIBILITIES.include?(visibility)
      raise ArgumentError, "expected one of the following: #{VISIBILITIES.join(', ')}"
    end

    return false if self.visibility == visibility

    success = false
    transaction do
      case visibility
      when PRIVATE_VISIBILITY
        self.public = false
        self.internal_repository&.destroy
      when INTERNAL_VISIBILITY
        raise ArgumentError, "Only organization-owned repositories can have #{INTERNAL_VISIBILITY} visibility" unless owner.organization?
        raise ArgumentError, "Only organizations associated with an enterprise can set visibility to #{INTERNAL_VISIBILITY}" unless owner.business
        self.internal_repository ||= InternalRepository.new(repository: self, business: owner.business)
        self.internal_repository.business = owner.business
        self.public = false
      when PUBLIC_VISIBILITY
        self.public = true
        self.internal_repository&.destroy
      end

      # save returns false in the event of validation failures.
      # Make sure we roll back the InternalRepository changes if that happens.
      success = save
      raise ActiveRecord::Rollback unless success
      reload_internal_repository # makes calls to Repository#visibility on this instance correct
    end

    success
  end

  # See link for abilities https://github.com/github/pe-workflows/issues/938#issuecomment-451534911
  def can_update_protected_branches?(user)
    if owner.organization?
      if business = owner.business
        return true unless GitHub.update_protected_branches_setting_enabled? || GitHub.flipper[:update_protected_branches_setting].enabled?(business)
        return true if business.adminable_by?(user) && adminable_by?(user)
        return false if business.members_can_update_protected_branches_policy? && !business.members_can_update_protected_branches?
      end

      return true unless GitHub.update_protected_branches_setting_enabled? || GitHub.flipper[:update_protected_branches_setting].enabled?(owner)
      return true if owner.adminable_by?(user)
      return false if !owner.members_can_update_protected_branches?

      adminable_by?(user) || repository.resources.administration.writable_by?(user)
    else
      true
    end
  end

  def can_change_repo_visibility?(user)
    return false unless user
    return true if user.site_admin? || !owner.organization?
    return false if GitHub.single_business_environment? && !GitHub.global_business.members_can_change_repo_visibility?
    owner.members_can_change_repo_visibility?(to_public: private?) || owner.adminable_by?(user)
  end

  def can_see_deployments?(user)
    return false unless user && writable_by?(user)
    return false if deployments.empty?
    true
  end

  # User that created this repository.
  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def created_by
    @created_by ||= created_by_user_id && User.find_by_id(created_by_user_id)
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator
  alias_method :created_by_user, :created_by

  # User that deleted this repository. This is stored as a serialized
  # attribute when the record is marked as deleted.
  # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
  def deleted_by
    @deleted_by ||= deleted_by_user_id && User.find_by_id(deleted_by_user_id)
  end
  # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

  # After validation callback that tracks if the name was changed so we know
  # whether to crack the owner cache once the record is saved. We don't update
  # the owner cache here because it runs on validation.
  def record_name_change
    @name_changed_during_validation = true if will_save_change_to_name?
  end

  # Override #reload to also reset various memoized attributes.
  def reset_memoized_attributes
    reset_git_cache
    @teams_for = nil
    @network_owner = nil
    @default_branch = nil
  end

  ##
  # Misc

  def <=>(other)
    return nil unless other.is_a?(Repository)

    name <=> other.name
  end

  # NOTE: Even though async_name_with_owner doesn't need anything async anymore,
  # other parts of our code will raise errors in tests (and might n+1 in prod
  # more often) if we don't load it here. We're essentially counting on the
  # user to request `name` so that we can use this method to preload `async_owner`
  # for other methods down the line. That's definitely not ideal! In order to
  # remove `async_owner` here, we'd need to track down every place we're
  # calling `owner` in a GraphQL-dependent method.
  def async_name_with_owner
    async_owner.then do
      name_with_owner
    end
  end

  def name_with_owner(separator = "/")
    "#{owner_login}#{separator}#{self}"
  end

  alias full_name name_with_owner
  alias nwo name_with_owner
  alias name_with_owner_for_archive name_with_owner

  def readonly_name_with_owner(separator = "/")
    ActiveRecord::Base.connected_to(role: :reading) { name_with_owner(separator) }
  end

  def async_owner_default_new_repo_branch
    async_owner.then do |owner|
      if owner
        default_branch = owner.custom_default_new_repo_branch
        next default_branch if default_branch
      end

      async_organization.then do |org|
        if org
          org.async_business.then do |business|
            if business
              default_branch = business.custom_default_new_repo_branch
              next default_branch if default_branch
            end

            Configurable::DefaultNewRepoBranch::RECOMMENDED_NAME
          end
        else
          if GitHub.single_business_environment?
            default_branch = GitHub.global_business.custom_default_new_repo_branch
            next default_branch if default_branch
          end

          Configurable::DefaultNewRepoBranch::RECOMMENDED_NAME
        end
      end
    end
  end

  # Public: Get the default branch name that the owner of this repository would prefer be
  # used for new repositories.
  def owner_default_new_repo_branch
    async_owner_default_new_repo_branch.sync
  end

  # Find the repository corresponding to the full shard path. This is
  # capable of locating repositories organized in a sharded layout like
  # "/data/repositories/e/nw/e4/78/53/<network_id>/<repo_id>.git"
  # (github.com) as well as repositories organized in a simple layout like
  # "/data/repositories/<user>/<repo>.git".
  def self.with_path(path)
    if path =~ /\/[0-9a-f]\/nw\//
      repo_id = File.basename(path)[/\d+/]
      Repository.find_by_id(repo_id)
    else
      parts = path.split("/")
      repo = parts[-1].chomp(".git").chomp(".wiki")
      owner = parts[-2]
      Repository.nwo("#{owner}/#{repo}")
    end
  end

  # Public: Find repositories with the given name and owner pairs.
  #
  # names_with_owners - Array of Strings like ["github/ce-stardust", "user/repo-name"]
  #
  # Returns an ActiveRecord Repository relation.
  def self.with_names_with_owners(names_with_owners)
    return Repository.none if names_with_owners.empty?

    logins_and_names = names_with_owners.map do |nwo|
      owner, name = nwo.split("/")
      [owner, name]
    end

    logins = logins_and_names.collect(&:first)
    owner_ids_by_login  = User.where(login: logins).pluck(:login, :id).to_h
    owner_ids_by_login.transform_keys!(&:downcase)

    conditions = logins_and_names.map do |login, name|
      arel_table[:owner_id].eq(owner_ids_by_login[login]).
        and(arel_table[:name].eq(name))
    end

    joined_conditions = conditions.reduce do |all_conditions, condition|
      all_conditions.or(condition)
    end

    where(joined_conditions)
  end

  # Find a repository by its qualified "owner/repo" name.
  #
  # name_with_owner - The full repository name including a "/", or just the
  #                   owner name when repo is provided.
  # repo            - The repository name.
  #
  # Options
  #   search_redirects - Whether to search for redirected repositories with the
  #                      owner and name. Defaults to false.
  #
  # Returns a Repository when a matching repository is found.
  def self.with_name_with_owner(name_with_owner, repo = nil, search_redirects: false)
    return nil unless name_with_owner

    owner, name =
      if repo
        [name_with_owner, repo]
      else
        name_with_owner.split("/")
      end
    return unless owner = User.find_by_login(owner)
    owner.find_repo_by_name(name) || (RepositoryRedirect.find_redirected_repository("#{owner.login}/#{name}") if search_redirects)
  end

  class << self
    alias_method :nwo, :with_name_with_owner
  end

  # The user who performed the action as set in the GitHub request context. If the context doesn't
  # contain an actor, fallback to the ghost user.
  def actor
    @actor ||= (User.find_by_id(GitHub.context[:actor_id]) || User.ghost)
  end

  # The plan owner is the account this repository is attached to. For public
  # repositories, it's the repository's owner, and for private repositories it's
  # whomever owns the repository network's root repository.
  #
  # In other words, if rtomayko/jobs is a fork of github/jobs,
  # rtomayko/job's `plan_owner` is the `github` Organization.
  def plan_owner
    async_plan_owner.sync
  end

  def async_plan_owner
    if public?
      async_owner
    else
      async_network_owner
    end
  end

  # Check to see if this repo's owner is also the plan's owner.
  #
  # Returns a boolean
  def plan_owned_by_owner?
    plan_owner == owner
  end

  def async_network_owner
    if association(:network).loaded? && network.root_id == id
      return async_owner
    end

    Platform::Loaders::NetworkOwner.load(source_id)
  end

  def network_owner
    @network_owner ||= async_network_owner.sync
  end

  def deployments_dashboard_environment_channel(environment)
    GitHub::WebSocket::Channels.deployments_dashboard_environment(self, environment)
  end

  # Public: Return the user or organization that sets the security policies for
  # the repository.
  #
  # Returns a User or Organization.
  def policymaker
    return owner if public?
    return owner if !fork? # private root
    return owner if owner&.organization? && network_owner&.user? # org-owned private fork of user root
    return owner if owner&.restricts_oauth_applications? && !network_owner.restricts_oauth_applications?

    network_owner
  end

  def visible?
    active? && !spammy?
  end

  # Compare base and head refs with a new GitHub::Comparison object.
  #
  # base  - The extended ref identifying the commit to act as the
  #         base of the comparison.
  # head  - The extended ref identifying the commit to act as the
  #         head of the comparison.
  # limit - Numeric limit for the total number of commits to retrieve.
  def comparison(base, head, limit = 1000, pull = nil)
    GitHub::Comparison.deprecated_build(self, base, head, limit: limit, pull: pull)
  end

  def open_issues_count
    @open_issues_count ||= issues.open_issues.not_spammy.count
  end

  def help_wanted_issues_count
    @help_wanted_issues_count ||= begin
      return 0 unless community_profile
      community_profile.help_wanted_issues_count
    end
  end

  def good_first_issue_issues_count
    @good_first_issue_issues_count ||= begin
      return 0 unless community_profile
      community_profile.good_first_issue_issues_count
    end
  end

  attr_writer :open_issues_count

  # Public: Synchronize this repository with its representation in the search
  # index. If the repository is newly created or modified in some fashion,
  # then it will be updated in the search index. If the repository has been
  # destroyed, then it will be removed from the search index. This method
  # handles both cases.
  #
  # *args - Anything. Accepted so method can be used as an association callback.
  #
  # Returns this Repository instance.
  #
  def synchronize_search_index(*args)
    return self unless GitHub.elasticsearch_access_allowed?

    if self.destroyed? or !self.active?
      RemoveFromSearchIndexJob.perform_later("repository", self.id)
      RemoveFromSearchIndexJob.perform_later("bulk_issues", self.id)
      RemoveFromSearchIndexJob.perform_later("bulk_discussions", self.id)
      RemoveFromSearchIndexJob.perform_later("bulk_pull_requests", self.id)
      RemoveFromSearchIndexJob.perform_later("code", self.id) if Search::ClusterStatus.code_search_indexing_enabled?
      RemoveFromSearchIndexJob.perform_later("commit", self.id)
    elsif locked?
      # do nothing
    else
      if repo_is_searchable?
        Search.add_to_search_index("repository", self.id)
      else
        RemoveFromSearchIndexJob.perform_later("repository", self.id)
      end

      unless code_is_searchable?
        RemoveFromSearchIndexJob.perform_later("code", self.id) if Search::ClusterStatus.code_search_indexing_enabled?
      end

      unless commits_are_searchable?
        RemoveFromSearchIndexJob.perform_later("commit", self.id)
      end
    end

    self
  end

  # Public: If the `has_discussions` flag has changed state, then we need to remove all
  # discussions for this repository from the search index or add them to the search
  # index.
  #
  # *args - Anything. Accepted so method can be used as an association callback.
  #
  # Returns nothing.
  def synchronize_discussions_search_index(*args)
    if has_discussions
      Search.add_to_search_index("bulk_discussions", id)
    else
      RemoveFromSearchIndexJob.perform_later("bulk_discussions", id)
    end
  end

  # If the `has_issues` flag has changed state, then we need to remove all
  # issues for this repository from the search index or add them to the search
  # index.
  #
  # *args - Anything. Accepted so method can be used as an association callback.
  #
  # Returns this Repository instance.
  #
  # TODO combine the two synchronize index methods into one and use Foca's
  # code from PR 8480
  #
  def synchronize_issues_search_index(*args)
    return self unless previous_changes.key?("has_issues")

    if self.has_issues
      Search.add_to_search_index("bulk_issues", self.id)
    else
      RemoveFromSearchIndexJob.perform_later("bulk_issues", self.id)
    end

    self
  end

  # Public: Render the repository's Markdown description as HTML.
  def description_html
    GitHub::HTML::DescriptionPipeline.to_html(description)
  end

  # Public: Render the repository's Markdown description as HTML without any
  # links, gracefully truncated.
  def short_description_html(limit: 200)
    formatted = GitHub::Goomba::SimpleDescriptionPipeline.to_html(description)
    HTMLTruncator.new(formatted, limit).to_html(wrap: false)
  end

  # Public: Asynchronous implementation of #short_description_html.
  def async_short_description_html(limit: 200)
    GitHub::Goomba::SimpleDescriptionPipeline.async_to_html(description).then do |formatted|
      HTMLTruncator.new(formatted, limit).to_html(wrap: false)
    end
  end

  ##
  # Access

  def repository
    self
  end

  # Public: Whether or not a fork's visibility matches its root repo.
  #
  # Returns true if its visibility matches its source, if it's a fork.
  def matches_root_visibility?
    public? == root.public?
  end

  # Public: If the repository is private and the network root owner has a billing
  # issue(thus is disabled), we will disable the repository.
  #
  # `has_any_trade_restrictions?` delegates to the user model, where we
  # check for ofac sanctioning. This is broken out from the other methods
  # as it doesn't matter what plan the user is on, nor is the user "disabled"
  # in the same sense as billing issues.
  #
  # Returns Boolean
  def disabled?
    async_disabled?.sync
  end

  def async_disabled?
    return Promise.resolve(false) unless private?

    Promise.all([async_internal_repository, async_owner]).then do
      non_repo_plan_disabled_user? ||
      trade_restricted_by_owner? ||
      actor.has_any_trade_restrictions?
    end
  end

  # Public: If the repository is private and the network root owner is disabled
  # due to billing issues.
  #
  # This method purposly ignores the condition where a repo is disabled because
  # of ofac sanctions. the `disabled?` method should be used when
  # you want to check both conditions.
  #
  # Returns Boolean
  def disabled_due_to_billing_issue?
    private? && non_repo_plan_disabled_user?
  end

  # Internal: plan_owner has been disabled and plan doesn't support repos
  #
  # Returns Boolean
  private def non_repo_plan_disabled_user?
    plan_owner_disabled? && !plan_supports?(:repos)
  end

  # Internal: plan_owner either doesn't exist or has been disabled
  #
  # Returns Boolean
  private def plan_owner_disabled?
    plan_owner.nil? || plan_owner.disabled?
  end

  # Public: the ids of all direct collaborators on this repository, including
  # the owner (unless the owner is an organization).
  def all_member_ids
    if owner.present? && !owner.organization?
      member_ids << owner_id
    else
      member_ids
    end
  end

  # Public: all direct collaborators on this repository, including the owner
  # (unless the owner is an organization).
  def all_members
    User.where(id: all_member_ids)
  end

  # Users who are most likely to be @mentioned in the context of this
  # repository.
  #
  # - For repositories owned by a User, this includes the owner and
  #   any collaborators.
  # - For repositories owned by an Organization, this includes all members of
  #   the organization with at least read access on the repository, and any
  #   collaborators.
  #
  # Returns a scope.
  def mentionable_users(fields: nil, limit: nil, include_child_teams: true)
    mentionable_users_for(nil, fields: fields, limit: limit,
                          include_child_teams: include_child_teams)
  end

  # Users who are most likely to be @mentioned in the context of this
  # repository with the user.
  #
  # - For repositories owned by a User, this includes the owner and
  #   any collaborators.
  # - For repositories owned by an Organization, this includes all members of
  #   the organization with at least read access on the repository, and any
  #   collaborators.
  # - for public repositories, also include all code contributors, even if they
  #   no longer have explicit access (Abilities) to the repo
  #
  # user - The User to scope to. May be nil.
  # fields - an optional array of fields to select from the users table
  # limit - how many users to return at most; defaults to 1,000
  #
  # Returns a scope.
  def mentionable_users_for(user, fields: nil, limit: nil, include_child_teams: true)
    limit ||= MENTIONABLES_LIMIT

    ids = all_user_ids(viewer: user, include_child_teams: include_child_teams)

    ids = if user
      User.from("users FORCE INDEX(PRIMARY)").where(id: ids).filter_spam_for(user).not_suspended.pluck(:id)
    else
      User.from("users FORCE INDEX(PRIMARY)").where(id: ids).not_suspended.pluck(:id)
    end

    ActiveRecord::Base.connected_to(role: :reading) do
      if ids.size < limit && public?
        contribution_user_ids = commit_contributions.
          no_ghost_users.
          order("committed_date DESC").
          limit(limit - ids.size).
          distinct.pluck(:user_id)
        ids |= User.from("users FORCE INDEX(PRIMARY)").where(id: contribution_user_ids).not_suspended.pluck(:id)
      end

      ids = ids[0, limit]
      query = User.where(id: ids, type: "User")
      query = query.select(fields) if fields
      query
    end
  end

  # Queues a job to clear leftover permissions/subscriptions for the given
  # user IDs.
  #
  # user_ids - Array of Integer User IDs.
  #
  # Returns nothing.
  def cleanup_old_users(user_ids)
    Team.queue_clear_team_memberships(user_ids, 0, { "repo" => id })
  end

  # Public: Check if the repo's owner is an org that has default
  # repository permission set
  #
  # Returns true/false
  def org_with_default_permission_owner?
    owner.present? && owner.organization? &&
      owner.default_repository_permission != :none
  end

  # The ids of all users that have access to the repository.
  # Includes the following users:
  # - repo owner (if owner is a user)
  # - repo collaborators (direct collaborators)
  # - organization members (if org default repository permission is set)
  # - organization team members
  # - organization child team members
  # - organization admins
  #
  # viewer - the User who is currently authenticated, if any
  # include_child_teams: - Boolean. Whether to include child teams or not.
  #                        Note: this will be ignored if there is an
  #                        org default repository permission set.
  def all_user_ids(viewer: nil, include_child_teams: true)
    # Ideally, we would use Abilities here to find all users with read access to
    # the repository. However, in some cases, this could involve checking
    # permissions on thousands of users, so we take some shortcuts to improve
    # performance.
    if org_with_default_permission_owner?
      owner.visible_user_ids_for(viewer) | all_member_ids
    else
      all_member_and_owner_ids(include_child_teams: include_child_teams)
    end
  end

  # key to use for PermissionCache for #all_member_and_owner_ids
  def all_member_and_owner_ids_key(include_child_teams)
    ["all_member_and_owner_ids", id, include_child_teams]
  end

  # The ids of most users that have access to the repository. Does not include
  # organization members that have repo access via organization
  # default repository permissions. The following users have access:
  # - repo owner (if owner is a user)
  # - repo collaborators (direct collaborators)
  # - organization team members
  # - organization child team members
  # - organization admins
  #
  # include_child_teams: - Boolean. Whether to include child teams or not.
  def all_member_and_owner_ids(include_child_teams: true)
    PermissionCache.fetch(all_member_and_owner_ids_key(include_child_teams)) do
      if include_child_teams
        # includes direct, team & child team membership, and organization admin permissions
        member_ids = Authorization.service.actor_ids(actor_type: User, subject: self, through: [Team])
        member_ids += [owner_id] if owner&.user?

        if owning_organization_id
          owning_org = Organization.where(id: owning_organization_id).first
          member_ids += owning_org.admin_ids
        end

        member_ids.uniq
      else
        all_team_member_ids(include_org_admins: true) | all_member_ids
      end
    end
  end

  # Includes all members and teams members that have access to the repository,
  # as well as organization admins.
  def all_members_and_owners
    User.where(id: all_member_and_owner_ids)
  end

  # Given a list of users, return an array of user_ids that do not have
  # access to this repo (and thus shouldn't be in the @mentions list)
  # Exception: if the repo owner is an org, optimize_repo_access_checks
  # is true, and the owning org has a lot of members, don't return any
  # id's, as getting the id's for all the members of a large org
  # (e.g. EpicGames) may time out
  #
  # users - list of users to check
  # include_child_teams - Boolean. Whether to include child teams or not.
  #                       Note: #all_user_ids will ignore this param if an
  #                       org default repository permission is set.
  # optimize_repo_access_checks - by default, check that all users returned
  #                               have access to the repo. If optimize_repo_access_checks
  #                               passed in is true, and the owner org has a
  #                               lot of members (> ORG_MEMBERSHIP_VERIFICATION_LIMIT),
  #                               (e.g. EpicGames) skip the access checks.
  #
  # Returns an array of user_ids that should be excluded. Return of [] either
  # means all users have access, or the org is too large to check.
  def user_ids_to_hide_from_mentions(users, viewer: nil, include_child_teams: true, optimize_repo_access_checks: false)
    return [] if public?

    # if this is an org with lots of members, and we don't already have
    # a result cached, don't hide anyone, as #all_user_ids might time out
    if optimize_repo_access_checks &&
        !org_with_default_permission_owner? &&
        large_membership_org_owner?
      unless PermissionCache.key?(all_member_and_owner_ids_key(include_child_teams))
        # ideally, we'd now populate the cache in the background.
        # Issue to track/discuss: https://github.com/github/github/issues/83848
        return []
      end
    end

    user_ids = if users.first&.respond_to?(:id)
      users.map(&:id).uniq
    else
      # Assume given an array of IDs already
      users
    end
    user_ids_with_repo_access = (all_user_ids(viewer: viewer, include_child_teams: include_child_teams) & user_ids)
    user_ids - user_ids_with_repo_access
  end

  private def large_membership_org_owner?
    owner.organization? && owner.members_count > ORG_MEMBERSHIP_VERIFICATION_LIMIT
  end

  # List of users watching this repository. If passed an Integer,
  # returns a paginated slice. Otherwise returns everyone.
  #
  # page - The optional Integer page number. Respects this class's
  #        per_page.
  #
  # Returns a Newsies::Responses::WillPaginateCollection of User objects.
  def watchers(page = nil, per_page = 100)
    Newsies::Responses::WillPaginateCollection.new {
      page = [page.to_i, 1].max

      count_subscribers_response = GitHub.newsies.count_subscribers(self, exclude_spammy_users: true)
      raise count_subscribers_response.error if count_subscribers_response.failed?

      subscribers_response = GitHub.newsies.subscribers(self, page: page, per_page: per_page, exclude_spammy_users: true)
      raise subscribers_response.error if subscribers_response.failed?

      size = count_subscribers_response.count
      WillPaginate::Collection.create(page, per_page, size) do |pager|
        pager.replace(subscribers_response.value)
      end
    }
  end

  # TODO: Rename watchers_count to stargazers_count then rename
  #       this to watchers_count.
  #
  # Returns the number of users watching this repo.
  def watchers_next_count
    GitHub.newsies.count_subscribers(self, exclude_spammy_users: true).count
  end

  ##
  # Repository visibility management

  # Internal - should this repository be detached from the network when
  # it attempts to change visibility?
  # This can be cleaned up once there are no longer mixed networks.
  def detach_on_visibility_change?
    (public? && (sole_repo_in_network? || matches_root_visibility?))
  end

  # Public: sets repository visibility.
  #
  # If visibility is not provided, visibility is toggled public/private. Repositories having
  # internal visibility are considered private and will have their visibility set to public.
  #
  # Will prevent making a public repository private if the owner paying for
  # the repository is at their private repo limit.
  #
  # Will also prevent changing visibility of forks.
  #
  # Pulls repository out into its own network. Any forks of this repo are
  # reparented according to the following scheme:
  #
  # public -> private/internal: the repo is detached from the network
  # private/internal -> public: the repo is detached from the network
  # and all forks are extracted into their own private networks
  # if repo was the root, or reparented in the network otherwise.
  #
  # actor - Required. the User attempting to change visibility of this repo.
  # Used for can_change_repo_visibility? check.
  # visibility - Optional: "public", "private", or "internal".
  # Returns nil if no change made, or true.
  def toggle_visibility(actor:, visibility: self.toggled_visibility)
    return unless visibility_change_valid?(actor, visibility)
    Contribution.clear_caches_for_user(user)
    set_visibility!(visibility, detach: detach_on_visibility_change?)
  end
  alias :set_visibility :toggle_visibility

  private def visibility_change_valid?(actor, new_visibility)
    unless %w[public private internal].include?(new_visibility)
      errors.add(:visibility, "can't be changed to unknown visibility #{new_visibility}.")
      return false
    end

    if new_visibility == self.visibility
      errors.add(:visibility, "is already #{self.visibility}.")
      return false
    end

    if %w[private internal].include?(new_visibility) && (matches_root_visibility? || sole_repo_in_network?)
      if owner.at_private_repo_limit?
        errors.add(:visibility, "can't be private. Please upgrade your subscription to make this repository private.")
        return false
      elsif !owner_has_seats_for_collaborators?
        errors.add(:visibility, "can't be private. Please add seats for collaborators to make this repository private.")
        return false
      elsif !owner_has_seats_for_collaborators?(pending_cycle: true)
        errors.add(:visibility, "can't be private. Please cancel the pending seat downgrade to ensure there are seats for collaborators to make this repository private.")
        return false
      end

      if visibility_change_restricted_by_trade_controls?(owner: owner, actor: actor)
        errors.add(:visibility, "can't be private. We are unable to provide this feature for one of the collaborators or invitees in this repository.")
        return false
      end
    end

    if !can_change_repo_visibility?(actor)
      errors.add(:visibility, "can't be changed by this user.")
      return false
    end

    if trade_controls_read_only?(new_visibility: PUBLIC_VISIBILITY)
      # Using the admin based messaging, as they are the only people with the permission to toggle the repo
      # visibility, anyway.
      errors.add(:visibility, ::TradeControls::Notices::Plaintext.organization_owned_repo_disabled)
      return false
    end

    true
  end

  def visibility_change_restricted_by_trade_controls?(owner:, actor:)
    # `restriction_tier_allows_feature?`is true for:
    # - the owner doesn't have any trade restrictions
    # - when the type of trade restriction allow free private repos (Tier 0)
    owner_has_restrictions = !owner.restriction_tier_allows_feature?(type: :repository)

    actor_is_restricted = actor&.has_any_trade_restrictions?
    paid_org_actor_is_restricted = actor_is_restricted && owner.organization? && owner.charged_account?
    non_org_actor_is_restricted = actor_is_restricted && owner.user?

    owner_has_restrictions || paid_org_actor_is_restricted || non_org_actor_is_restricted
  end

  # Private: Instrumentation for kafka topic that triggers when visibility changes
  private def publish_visibility_change
    GlobalInstrumenter.instrument("repository.visibility_changed", {
      name_with_owner: repository.name_with_owner,
      repository_id: repository.id,
      is_private: repository.private?,
      is_fork: repository.fork?,
      visibility: repository.visibility,
    })

    if user_configuration_repository? && public? && has_readme?
      GlobalInstrumenter.instrument("user.profile_readme_action", {
        repository: self,
        owner: owner,
        actor: actor,
        change_type: :REPO_VISIBILITY_CHANGED_TO_PUBLIC,
        readme_body: preferred_readme&.data
      })
    end
  end

  # Private: issue Geyser search ingest event to record repo visibility change
  private def publish_search_visibility_change
    payload = {
      change: :VISIBILITY_CHANGED,
      repository: repository,
      owner_name: repository.owner.name,
      updated_at: Time.now.utc,
      ref: "refs/heads/#{repository.default_branch}",
    }

    GlobalInstrumenter.instrument("search_indexing.repository_changed", payload)
    GitHub.dogstats.increment("geyser.repo_changed_event.published", tags: ["change_type:visibility_changed"])
  end

  # Private: Sets a repo's visibility to public, private, or internal without
  # testing that the owner has private repos available.
  #
  # WARNING: This method skips authz. Use Repository#set_visibility instead.
  #
  # detach - whether the repository should be detached into its own network
  # before toggling the visibility change. Defaults to false.
  private def set_visibility!(new_visibility, detach: false)
    GitHub.dogstats.increment "repository", tags: [
      "action:set_visibility",
      "from_visibility:#{visibility}",
      "to_visibility:#{new_visibility}"]

    old_visibility = visibility
    set_permission(new_visibility)

    publish_visibility_change
    publish_search_visibility_change

    async_toggle_repository_visibility(detach, old_visibility: old_visibility)

    enable_or_disable_owner!
    if private?
      disable_repository_settings
      unpublish_page unless plan_supports?(:pages)
      destroy_protected_branches unless plan_supports?(:protected_branches)
      disable_tiered_reporting(actor: actor)

      unless fork?
        # unlock anonymous git access setting before disabling
        unlock_anonymous_git_access(actor)
        disable_anonymous_git_access(actor)
      end
    end

    async_update_download_acls
    NewsiesPurgeSubscribersJob.perform_later(id)
    enqueue_set_license_job
    enqueue_community_health_check
    instrument :access, access: new_visibility.to_sym, actor_id: actor.try(:id)

    true
  end

  def unpublish_page
    return unless page

    if page.destroy
      GitHub::Logger.log \
        at: self.class.to_s,
        fn: "unpublish_page",
        message: "[repository_unpublish_page] repository #{self.id} page has been destroyed"
    else
      GitHub::Logger.log \
        at: self.class.to_s,
        fn: "unpublish_page",
        message: "[repository_unpublish_page] repository #{self.id} errored with #{page.errors&.full_messages.to_sentence}"
    end
  end

  def unsupported_pages?
    !plan_supports?(:pages) && page.present? && page.status == "built"
  end

  # Queues a ToggleRepoVisibility job, which performs heavy-ish git operations
  # on disk and ensures that only a single job will run at a given time. This
  # job also extracts forks from the network and scans for sensitive tokens.
  #
  # detach - whether the repository should be detached into its own network
  # before toggling the visibility change. Defaults to false.
  def async_toggle_repository_visibility(detach = false, old_visibility:)
    # Lock repo that is being toggled
    lock_excluding_descendants!("moving!")
    # lock forks when toggling from private to public
    if public? && network_root?
      forks.select(&:private?).each(&:lock_for_move)
    end
    ToggleRepoVisibilityJob.perform_later(id, detach, old_visibility: old_visibility)
  end

  def disable_repository_settings
    disable_content_analysis(actor: actor)
    disable_dependency_graph(actor: actor)
    disable_vulnerability_alerts(actor: actor)
  end

  # Run the logic to check if any of the owner's repos need to
  # be unlocked.
  #
  # Returns nothing.
  def update_owner_repo_locks
    owner.update_locked_repositories if owner.present?
  end

  # Public: Enables or disables the owner for billing issues
  #
  # Returns nothing.
  def enable_or_disable_owner!
    if owner.present?
      should_disable? ? owner.disable! : owner.enable!
    end
  end

  # Should this user be disabled if it's not already?
  #
  # Returns a Boolean
  def should_disable?
    return false if owner.never_disable?
    owner.should_disable? || owner.beneficiary?
  end

  # Public: Does the owner of this repository have the plan capacity to make it
  #         private? Returns true if this repository is already private.
  #
  # Returns Boolean
  def can_privatize?
    return true if private?
    # Only organization with Tier 0 restriction can make a repository private. If the
    # organization has any other restriction just return false.
    return false unless owner.restriction_tier_allows_feature?(type: :repository)

    if owner.plan.per_seat?
      owner_has_seats_for_collaborators? && owner_has_seats_for_collaborators?(pending_cycle: true)
    else
      !owner.at_private_repo_limit? && !over_collaborator_limit_for_private_repos?
    end
  end

  # Public: Does the the owner have enough seats to cover the collaborators on
  #         this repository if it's not already private.
  #
  # Returns Boolean
  def owner_has_seats_for_collaborators?(pending_cycle: false)
    return true if owner.plan.free? && GitHub.flipper[:always_unlimited_seats].enabled?
    return true if private? || owner.has_unlimited_seats? || owner.user?

    return true if !pending_cycle && owner.plan.per_repository?
    return true if pending_cycle && owner.pending_cycle_plan.per_repository?

    owner.seats_needed_for_collaborators_on(self, pending_cycle: pending_cycle).zero?
  end

  # Public: Would the repository have enough seats to cover collaborators when
  #         made private?
  #
  # Returns Boolean
  def over_collaborator_limit_for_private_repos?
    [available_private_seats, 0].min < 0
  end

  # Can we assign issues (or other things) to this user?
  #
  # Returns true if user can be assigned issues.
  def assignable_member?(user)
    return false unless user.is_a?(User)
    return false unless user.assignable_to_issues?
    return true if user.id == owner_id

    available_assignee_ids.include?(user.id)
  end

  # Can we add this user to the repo?
  #
  def can_add_user?(addee, adder, action: :write, already_invited: false)
    if private? && addee.has_any_trade_restrictions?
      errors.add(:base, "User could not be added")
      return false
    end

    if trade_restricted?
      errors.add(:base, "User could not be added")
      return false
    end

    adders = [adder]
    adders << owner if adder && adder.id != owner_id
    adders.compact!

    actor = adder unless adder.is_a?(Bot)
    actor ||= if (installation = adder.installation)
      installation
    else
      IntegrationInstallation.with_repository(self).where(integration_id: adder.integration.id).first
    end

    if actor != organization && !resources.administration.writable_by?(actor)
      errors.add(:base, "You cannot administer this repository")
      return false
    end

    if has_invitation_for?(addee) && !already_invited
      errors.add(:base, "User has already been invited")
      return false
    end

    blocked = addee.blocked_by?(owner_id)
    unless blocked
      blocked = addee.blocked_by?(organization.admins) if owner.organization?
    end

    if blocked
      errors.add(:base, "User is blocked")
      return false
    end

    if addee.ignore?(*adders)
      errors.add(:base, "User has blocked you")
      return false
    end

    if spammy?
      errors.add(:base, "User could not be added")
      return false
    end

    # Skip if we are in a gh-migrator import process
    if addee.suspended? && !GitHub.importing?
      errors.add(:base, "User is suspended")
      return false
    end

    if members.include?(addee)
      errors.add(:base, "User is already a collaborator")
      return false
    end

    if owner == addee
      errors.add(:base, "Repository owner cannot be a collaborator")
      return false
    end

    if !addee.user?
      errors.add(:base, "Only users can be collaborators")
      return false
    end

    if owner.user? && action != :write
      errors.add(:base, "You can only give write access to user-owned repositories")
      return false
    end

    if owner.user? && private? && !has_seat_for?(addee)
      errors.add(:base, "You must upgrade your account to add more collaborators.")
      return false
    end

    if owner.organization? && private?
      if !owner.has_seat_for?(addee)
        errors.add(:base, "You must purchase at least one more seat to add this user as a collaborator.")
        return false
      end

      if !owner.has_seat_for?(addee, pending_cycle: true)
        errors.add(:base, "You must cancel your pending seat downgrade to add this user as a collaborator.")
        return false
      end
    end

    true
  end

  # Returns a collection of users who have been invited to this repository but
  # have not yet accepted the invitation.
  def invitees
    repository_invitations.includes(:invitee).map(&:invitee)
  end

  # Determine if this repository has an invitation for a particular user.
  def has_invitation_for?(user)
    repository_invitations.where(invitee_id: user.id).any?
  end

  def cancel_all_invitations_from_user(user, actor)
    repository_invitations.where(inviter_id: user.id).each { |i| i.cancel!(actor: actor, force: true) }
  end

  # Internal: Directly add the specified user to the repository, bypassing
  # validation and subscription/notification stuff.
  #
  # ONLY CALL THIS IF YOU KNOW EXACTLY WHAT YOU'RE DOING.
  #
  # addee  - The user to add as a collaborator to this repository.
  # adder  - The user who is adding the addee as a collaborator.
  # action - The level of permission the addee should be given on the
  #          repository. Can be :read, :write, or :admin.
  #
  # Returns nothing.

  def add_member_without_validation_or_notifications(addee, adder = owner, event: false, action: :write)
    grant(addee, action, grantor: adder)
    RepositoryAdvisory::WorkspaceRepositoryManager.grant(addee, repository: self, action: action)

    if licensing_enabled?
      Billing::SnapshotLicensesJob.perform_later(owner.business)
    end

    instrument :add_member, user: addee, actor: adder
    GlobalInstrumenter.instrument("repo.add_member", {
      user: addee,
      actor: adder,
      repo: self,
      action: :add,
    })
  end

  def add_members(members, adder = owner, event = true)
    members.each do |member|
      add_member(member, adder)
    end
  end

  # After ensuring that a user is allowed, adds that user to the repository
  # and subscribes them to notifications.
  #
  # addee - The user to add as a collaborator to this repository
  # adder - The member who is adding the user. Defaults to the repo owner.
  # event - Not used.
  # action - The default permission for the addee.
  #
  # Returns true on succees or nil on failure.
  def add_member(addee, adder = owner, event = true, action: :write)
    return unless two_factor_requirement_met_by?(addee)
    if can_add_user?(addee, adder, action: action)
      add_member_without_validation_or_notifications(addee, adder, action: action)
      response = GitHub.newsies.auto_subscribe(addee, self)
      if response.success?
        addee.reload # clear cached associations
      else
        GitHub.newsies.async_auto_subscribe(addee, [id])
      end

      Contribution.clear_caches_for_user(addee)
      true
    end
  end

  # Public: Update a member's permission on this repository.
  #
  # member  - Member whose permission we want to update.
  # action  - The permission to update to. Can be read/write/triage/maintain/admin or a custom role
  # actor   - User doing the update.
  # context - Hash of Strings with the member's previous Role {:old_permission, :old_base_role}
  #           A user might be given the same custom role permission, with a different base role.
  #           In these scenarios, the name is the same, but the Ability must be updated.
  #           For auditing purposes, we need the old Role and it's base role.
  #           A custom role is a user created role which inherits from any Role::VALID_REPO_BASE_ROLES
  #           with an additional set of fine grained permissions.
  #
  # Returns a boolean (true if the update worked, false if it didn't).
  def update_member(member, action:, actor: owner, context: {})
    if in_organization?
      # action must either be a system role or a valid custom role for the given repository
      if !VALID_ORG_REPO_ACTIONS_AND_ROLES.include?(action) && !Role.org_repo_custom_role?(name: action, repo: self)
        errors.add(:base, "Invalid permission passed")
        return false
      end
    elsif !VALID_USER_REPO_ACTIONS.include?(action)
      errors.add(:base, "Invalid permission passed")
      return false
    end

    if members.exclude?(member)
      errors.add(:base, "User is not yet a collaborator")
      return false
    end

    # in the case of custom roles, the old permission can't be obtained directly from Ability records
    # so we leverage the context
    old_permission = context.dig(:old_permission) || self.direct_role_for(member)
    old_base_role = context.dig(:old_base_role) || old_permission

    old_permissions = {
      pull: self.pullable_by?(member),
      push: self.pushable_by?(member),
      admin: self.adminable_by?(member),
      triage: permission_greater_than_target?(old_base_role, target: "triage"),
      maintain: permission_greater_than_target?(old_base_role, target: "maintain")
    }

    grant member, action
    RepositoryAdvisory::WorkspaceRepositoryManager.grant(member, repository: self, action: action)

    instrument :update_member,
      user: member,
      actor: actor,
      old_permission: old_permission.to_sym,
      new_permission: action.to_sym,
      old_permissions: old_permissions

    member.reload

    if member != organization && !adminable_by?(member)
      cancel_all_invitations_from_user(member, actor)
      vulnerability_manager.update_user_or_team(member, action: action)
      # Ensure we remove access to Protected Branches when the member loses write access
      if action == :read
        ProtectedBranch::AbilityRepositoryManager.revoke(member, repository: self)
      end
    end

    true
  end

  # Public: Checks if a user can invite collaborators to an organisation repository
  #
  # user  - User to be checked
  #
  # Returns boolean
  def cannot_invite_outside_collaborators?(user)
    in_organization? &&
    organization.can_restrict_repo_invites? &&
    !organization.members_can_invite_outside_collaborators? &&
    !organization.adminable_by?(user)
  end

  # Public: Batch update `batch_size` member's permission on this repository via background job.
  #
  # user_roles        - Member/collaborator whose permission we want to update.
  # action            - The permission to update to. Can be :read, :write, :admin, :triage, :maintain, or a custom role
  # role              - Custom role (default: nil)
  # batch_size        - Batch size to be passed processed.
  # organization      - Org object whose members and/or collaborators to be updated
  # context           - Hash of Strings with the users' previous Role {:old_permission, :old_base_role}
  #
  def self.batch_enqueue_update_member(user_roles:, action:, role: nil, organization:, context: {}, batch_size: MEMBER_UPDATE_BATCH_SIZE)
    user_roles.each_slice(batch_size) do |user_roles_slice|
      BatchUpdateMemberRepoPermissionsJob.perform_later(user_roles_slice, action: action, organization: organization, role: role, context: context)
    end
  end

  # Public: Update a member's permission on this repository via background job.
  #
  # member  - Member whose permission we want to update.
  # action  - The permission to update to. Can be :read, :write, :admin, :triage, :maintain, or a custom role
  def enqueue_update_member(member, action:)
    action = evaluate_action(action)
    UpdateMemberRepoPermissionsJob.perform_later(member, action: action, repo: self)
  end

  def evaluate_action(action)
    if VALID_ORG_REPO_ACTIONS_AND_ROLES.include? action
      action
    elsif Role.valid_system_role?(action)
      Repository.permission_to_action(action)
    else
      Role.find_by!(name: action, owner_id: owner_id, owner_type: owner.class).name
    end
  end

  # Removes someone as a collaborator on a repository.
  #
  # Also iterates through any issues the person may have been assigned to and
  # unassigns them. Keep this logic in this method (and not as a callback
  # after a membership is deleted) so we don't inadvertedly remove assignees
  # when a repo goes from personal -> org.
  #
  # removee - The member being removed.
  # remover - The user removing the member. defaults to owner.
  #
  # Returns nothing.
  def remove_member(removee, remover = owner)
    disassociate_member removee, remover
    cancel_all_invitations_from_user(removee, remover)

    unless pullable_by?(removee)
      if remover == removee || private?
        GitHub.newsies.async_delete_all_for_user_and_lists(removee.id, [self])
      end

      removee.clear_issue_assignments(scope: issues)
      removee.unstar(self)
      remove_fork_for(removee, remover)

      if internal? && network_root?
        remove_from_forks(removee, remover)
      end
    end

    removee.reload # clear cached associations

    if licensing_enabled?
      Billing::SnapshotLicensesJob.perform_later(owner.business)
    end

    Contribution.clear_caches_for_user(removee)
    true
  end

  # Public: enqueues a job to remove a member from the repository
  #
  # removee - The member being removed.
  # remover - The user removing the member. defaults to owner.
  #
  # Returns nothing.
  def enqueue_remove_member(removee, remover:)
    return unless pullable_by?(removee)
    RemoveRepoMemberJob.perform_later(removee, remover: remover, repo: self)
  end

  # Internal: remove all collaborators from this repository.
  #
  # actor - The user clearing the members. Defaults to owner.
  def remove_all_members(actor = owner)
    # dup since members is being modified during the iteration.
    members.dup.each do |member|
      disassociate_member member, actor
      cancel_all_invitations_from_user(member, actor)
    end
  end

  # Internal: remove removee from forks of this repository
  #
  # removee - The user being removed from forks
  # remover - The user removing the user from forks
  def remove_from_forks(removee, remover)
    descendants.each do |repo|
      repo.remove_member(removee, remover)
    end
  end

  # Internal: disassociate a collaborator from this repository.
  #
  # user  - The User being removed as a collaborator.
  # actor - The User responsible for removing the user.
  #
  # Only breaks the direct link between the user and this repo and leaves stars,
  # watching, and issue assignments intact.
  def disassociate_member(user, actor)
    return unless member?(user)

    # Ensure we remove access to Vulnerability Alerts
    vulnerability_manager.revoke_user_or_team(user)
    revoke user
    RepositoryAdvisory::WorkspaceRepositoryManager.revoke(user, repository: self)
    # Ensure we remove access to Protected Branches
    ProtectedBranch::AbilityRepositoryManager.revoke(user, repository: self)

    instrument :remove_member, user: user, actor: actor
    GlobalInstrumenter.instrument("repo.remove_member", {
      user: user,
      actor: actor,
      repo: self,
      action: :remove,
    })
  end

  # Internal: remove all inaccessible forks of this repository for the given user ids.
  #
  # When a fork is removed, its children have their parent set to the parent of the removed fork.
  # The array of these reparented repos is returned.
  def remove_inaccessible_forks_for(user_ids)
    reparented_repos = []
    forks.where(owner_id: user_ids).includes(:owner).each do |user_fork|
      if !pullable_by? user_fork.owner
        reparented_repos += remove_user_fork(user_fork)
      end
    end
    reparented_repos.uniq
  end

  # Internal: remove the given user's fork of this repository including their children.
  def remove_fork_for(user, remover = owner)
    if forked_repo = find_fork_for_user(user)
      forked_repo.descendants.each do |child_repo|
        remove_user_fork(child_repo, remover)
      end

      remove_user_fork(forked_repo, remover)
    end
  end

  # Remove a user's fork when they've lost access to it.
  #
  # This includes sending a nice email explaining what happened.
  #
  # When a fork is removed, its children have their parent set to the parent of the removed fork.
  # The array of these reparented repos is returned.
  def remove_user_fork(forked_repo, remover = owner)
    reparented_repos = forked_repo.remove(remover)
    forked_repo.send_private_fork_deleted_email
    reparented_repos
  end

  # Ensures that people who starred the repository are people who are allowed
  # to star it. Useful for private repositories and transferring between
  # owners who may change who has access to the repository (collabs -> teams).
  #
  # Returns nothing.
  def correct_stargazers
    return if self.public?
    stargazer_ids = Star.where(starrable_id: id, starrable_type: "Repository").pluck(:user_id)

    stargazer_ids.each_slice(REPO_STARGAZERS_BATCH_SIZE) do |stargazer_ids_slice|
      stargazers = User.with_ids(stargazer_ids_slice)

      Promise.all(stargazers.map { |user|
        self.async_readable_by?(user).then do |readable|
          user.unstar(self) unless readable
        end
      }).sync
    end
  end

  # Internal: Remove newsies records for users who no longer have access.
  def correct_watchers
    return if public?

    NewsiesPurgeSubscribersJob.perform_later(id)
  end

  # Internal: Correct any issue cards associated with organization-owned
  # projects since cards can only be associated with issues in repositories that
  # are in the organization.
  #
  # For public repositories the issue cards are converted to issue note
  # references linking to the new issue URLs.
  #
  # For private repositories the issue cards are removed from the project.
  def correct_project_cards(old_owner:)
    return unless old_owner.respond_to?(:projects)

    issue_ids = issues.pluck(:id)
    card_action_sym = public? ? :convert_to_note_reference! : :destroy
    old_owner.projects.find_each do |project|
      project.cards.
        for_content_type("Issue").
        where(content_id: issue_ids).
        find_each(&card_action_sym)
    end
  end

  def copy_permissions_of(repo, adder = owner)
    return if repo == self || repo.nil?
    add_members(repo.all_members, adder, false)
  end

  def fork_inherits_teams?(fork)
    private? && in_organization? && organization.member?(fork.owner)
  end

  def issue_templates
    @issue_templates ||= IssueTemplates.new(self)
  end

  def issue_comment_templates
    @issue_comment_templates ||= IssueCommentTemplates.new(self)
  end

  # Public: Issue templates for this repository, either from the local repository
  # or a global health files repository for an organization.
  #
  # Returns IssueTemplates or nil.
  def preferred_issue_templates
    @preferred_issue_templates ||= async_preferred_issue_templates.sync
  end

  # Public: Issue templates for this repository, either from the local repository
  # or a global health files repository for an organization.
  #
  # Returns Promise<IssueTemplates|nil>.
  def async_preferred_issue_templates
    if issue_templates.any?            ||
       global_health_files_repository? ||
       issue_templates.issue_template_config.configured?

      Promise.resolve(issue_templates)
    else
      async_owner.then do |owner|
        next issue_templates unless owner.organization?

        Platform::Loaders::GlobalHealthFilesRepository.load(owner_id).then do |global_repository|
          next issue_templates unless global_repository.present?

          global_templates = global_repository.issue_templates

          if global_templates.any? || global_templates.issue_template_config.configured?
            global_templates
          else
            issue_templates
          end
        end
      end
    end
  end

  def template_tree_path
    File.join("/", name_with_owner, "tree", default_branch, IssueTemplates.template_directory)
  end

  def can_create_issue_templates?
    # We're trying to prevent cases where either .github or .github/ISSUE_TEMPLATE is a file
    valid_file_path?(".github/ISSUE_TEMPLATE/bug.md")
  end

  def update_stargazer_count!
    count = stars.not_spammy.count

    if count != stargazer_count
      update_attribute :stargazer_count, count
    end
  end

  def contributors(with_anon: false, email_limit: nil, viewer: nil)
    Contributors.new(self).all(with_anon: with_anon, email_limit: email_limit, viewer: viewer)
  end

  # Public: Return the total count of non-spammy users who have contributed to this repository.
  #
  # Returns a Contributors::CountResponse that must be checked with :computed? before use.
  def contributor_count
    Contributors.new(self).count
  end

  # Public: Return the total count of non-spammy users who have contributed to this repository,
  # falling back to a default value if it cannot be computed.
  #
  # Returns an Integer.
  def contributor_count_or(fallback)
    response = contributor_count
    response.computed? ? response.value : fallback
  end

  # Public: Get contributors who have made the most commits to this repository. Pulls data
  # from the commit_contributions table instead of the git repo itself.
  #
  # limit - how many users to return at most
  # viewer - the current user, used for spam-filtering purposes; a User or nil
  #
  # Returns an Array of Users.
  def top_contributors(limit:, viewer:)
    user_ids = ActiveRecord::Base.connected_to(role: :reading) do
      commit_contributions.group(:user_id).no_ghost_users.
        order(Arel.sql("SUM(commit_count) DESC")).limit(limit).pluck(:user_id)
    end

    users = ActiveRecord::Base.connected_to(role: :reading) do
      User.where(id: user_ids, type: "User").filter_spam_for(viewer).to_a
    end

    # Sort with top contributors first:
    users.sort_by { |user| user_ids.index(user.id) }
  end

  # Return an array of contributors blocked by a given user
  def blocked_contributors_for(user)
    return [] unless user
    user.ignored.where("ignored_users.ignored_id IN (?)", contributor_ids)
  end

  # Return an array of contributor IDs
  #
  # user_ids - an array of user_ids to check
  #
  # If no argument is given, this will be all contributor IDs
  # If an array of IDs are passed, a subset of contributor IDs will be returned
  def contributor_ids(user_ids = [])
    if user_ids.empty?
      @contributor_ids ||= ActiveRecord::Base.connected_to(role: :reading) { commit_contributions.no_ghost_users.group(:user_id).pluck(:user_id) }
    else
      ActiveRecord::Base.connected_to(role: :reading) { commit_contributions.where(user_id: user_ids.uniq.compact).group(:user_id).pluck(:user_id) }
    end
  end

  # Is the given user a contributor to this repository?
  #
  # user - the user or user id
  # type - an optional type of contribution to consider; defaults to commits; valid values:
  #        Issue, PullRequest, CommitContribution, Discussion
  #
  # Returns bool, true if contributor, otherwise false
  def contributor?(user, type: CommitContribution)
    ActiveRecord::Base.connected_to(role: :reading) do
      if type == PullRequest
        pull_requests.exists? user_id: user
      elsif type == Issue
        issues.without_pull_requests.exists? user_id: user
      elsif type == Discussion
        discussions.authored_by(user).exists?
      else
        commit_contributions.exists? user_id: user
      end
    end
  end

  ##
  # Paths, URLs, Routing
  def normalize_name
    return unless active?

    throw(:abort) unless owner

    self.name = EntityName.normalize(self[:name])
    true
  end

  # Internal: does this repo have an unique name?  Returns true if name
  # is unique, false otherwise.  Called via validation.
  def ensure_uniqueness_of_name
    return true unless active?
    return true unless will_save_change_to_name?
    return false unless owner

    existing_repo = Repository.where(
      name: name,
      owner_id: owner.id,
      active: true,
    ).first

    if RetiredNamespace.retired?(owner.login, name)
      errors.add("name", "has been retired and cannot be reused")
      false
    elsif existing_repo.nil?
      true
    elsif !new_record? && existing_repo.id == id
      true
    else
      errors.add("name", "already exists on this account")
      false
    end
  end

  # A user may not have more than one fork of a repository within the same
  # network. Verify that an existing fork doesn't exist for the user before
  # creating.
  def ensure_uniqueness_of_fork_in_network
    return if network.nil?

    if repo = network.repositories.where(owner_id: owner_id).first
      errors.add(:base, "Fork already exists: #{repo.name_with_owner}")
      false
    else
      true
    end
  end

  # Absolute permalink URL for this repository.
  #
  # include_host - Turn off the `GitHub.url` host in the url. (default true)
  #                repository.permalink(include_host: false) => `/github/github`
  #
  def permalink(include_host: true)
    if include_host
      "#{GitHub.url}/#{name_with_owner}"
    else
      "/#{name_with_owner}"
    end
  end

  # The business that this repo is in.
  #
  # Returns nil if not in a business
  def async_business
    async_owner.then do |owner|
      owner.async_business
    end
  end

  # Returns the id of the business that this repo is in if it has internal
  # visibility. Returns nil otherwise.
  def internal_visibility_business_id
    return internal_repository&.business_id if internal?
    nil
  end

  def async_path_uri
    return @async_path_uri if defined?(@async_path_uri)

    @async_path_uri = async_owner.then do |owner|
      URI_TEMPLATE.expand(owner: owner.login, name: repository.name)
    end
  end

  def async_commits_path_uri(author: nil)
    async_owner.then do |owner|
      uri = COMMITS_URI_TEMPLATE.expand(owner: owner.login, name: name)
      uri.query_values = { author: author } if author
      uri
    end
  end

  def path
    @path || (owner && "#{owner.path}/#{self}.git")
  end

  def path=(path)
    @path = path
  end

  alias short_git_path name_with_owner

  ##
  # Git Repository Access

  # Get the host on which the repo (network) is stored.
  # For DGit, get the best available host on which the repo is stored.
  # For unrouted repos (nil network or no healthy DGit replicas), throws
  # an exception.
  def host
    dgit_read_routes.first.host
  end
  alias route host

  # dgit_spec is the canonical description for a repository entity within
  # dgit/spokes. It avoids using any PII and encodes network membership.
  def dgit_spec(wiki: false)
    if wiki || dgit_repo_type == GitHub::DGit::RepoType::WIKI
      "#{network_id}/#{id}.wiki"
    else
      "#{network_id}/#{id}"
    end
  end

  def namespace
    dgit_repo_type == GitHub::DGit::RepoType::WIKI ? "wiki" : "repository"
  end

  def dgit_delegate
    if dgit_repo_type == GitHub::DGit::RepoType::WIKI
      GitHub::DGit::Delegate::Wiki.new(network.id, id, original_shard_path)
    else
      GitHub::DGit::Delegate::Repository.new(network.id, id, original_shard_path)
    end
  end

  def coalesce_dgit_updates?
    false
  end

  def repository_spec
    "#{network.id}/#{id}"
  end

  # Get the host on which the repo (network) is stored.
  # For DGit, get the best available host on which the repo is stored.
  # If the repo is unrouted in DGit, eat the error.  "unrouted" isn't a
  # real host, but if the text is going to be forwarded opaquely to a log
  # or a web pages (e.g., haystack or stafftools), this is better than an
  # exception.
  def safe_route
    route
  rescue GitHub::DGit::UnroutedError
    "unrouted"
  end

  # The absolute path on disk to the repository.
  def shard_path
    mapped_shard_path(map_dgit_dev: true)
  end

  def no_dgit_shard_path
    mapped_shard_path(map_dgit_dev: false)
  end

  def mapped_shard_path(map_dgit_dev:)
    if name.end_with?(".wiki") && base_repo = Repository.nwo(name_with_owner.chomp(".wiki"))
      if map_dgit_dev
        base_repo.unsullied_wiki.shard_path
      else
        base_repo.unsullied_wiki.original_shard_path
      end
    else
      fail "no shard_path for repo without a network" unless network_id
      fail "no shard_path for repo without an id" unless id

      if map_dgit_dev && (Rails.development? || Rails.test?)
        # map route to a dgit subdirectory in dev and test
        dgit_read_routes.first.path
      else
        original_shard_path
      end
    end
  end

  def original_shard_path
    "#{network.storage_path}/#{id}.git"
  end

  # Get all routes that can be used for read-only operations, in order of preference.
  def dgit_read_routes(preferred_dc: nil)
    #
    # Clear potentially memoized routes for `preferred_dc`.  There are
    # lots of call sites that implicitly populate this cache but do
    # not necessarily pass down a `preferred_dc`.
    #
    dgit_reload_routes! if (preferred_dc != @preferred_dc)
    @preferred_dc = preferred_dc

    return @dgit_read_routes if @dgit_read_routes

    delegate = GitHub::DGit::Delegate::Repository.new(network.id, id, original_shard_path)
    @dgit_read_routes = delegate.get_read_routes(preferred_dc: preferred_dc)
  end

  def dgit_wiki_read_routes(preferred_dc: nil)
    #
    # Clear potentially memoized routes for `preferred_dc`.  There are
    # lots of call sites that implicitly populate this cache but do
    # not necessarily pass down a `preferred_dc`.
    #
    dgit_reload_routes! if (preferred_dc != @preferred_dc)
    @preferred_dc = preferred_dc

    return @dgit_wiki_read_routes if @dgit_wiki_read_routes

    delegate = GitHub::DGit::Delegate::Wiki.new(network.id, id, wiki_shard_path)
    @dgit_wiki_read_routes = delegate.get_read_routes(preferred_dc: preferred_dc)
  end

  # Get all routes that can be used for read-write operations, in order of preference.
  def dgit_write_routes(preferred_dc: nil)
    #
    # Clear potentially memoized routes for `preferred_dc`.  There are
    # lots of call sites that implicitly populate this cache but do
    # not necessarily pass down a `preferred_dc`.
    #
    dgit_reload_routes! if (preferred_dc != @preferred_dc)
    @preferred_dc = preferred_dc

    return @dgit_write_routes if @dgit_write_routes

    delegate = GitHub::DGit::Delegate::Repository.new(network.id, id, original_shard_path)
    @dgit_write_routes = delegate.get_write_routes(preferred_dc: preferred_dc)
  end

  def dgit_wiki_write_routes(preferred_dc: nil)
    #
    # Clear potentially memoized routes for `preferred_dc`.  There are
    # lots of call sites that implicitly populate this cache but do
    # not necessarily pass down a `preferred_dc`.
    #
    dgit_reload_routes! if (preferred_dc != @preferred_dc)
    @preferred_dc = preferred_dc

    return @dgit_wiki_write_routes if @dgit_wiki_write_routes

    delegate = GitHub::DGit::Delegate::Wiki.new(network.id, id, wiki_shard_path)
    @dgit_wiki_write_routes = delegate.get_write_routes(preferred_dc: preferred_dc)
  end

  # Get all routes, even unhealthy ones, in an undefined order.
  # Do not use this unless you are writing debugging, logging, or maintenance code.
  def dgit_all_routes(preferred_dc: nil)
    #
    # Clear potentially memoized routes for `preferred_dc`.  There are
    # lots of call sites that implicitly populate this cache but do
    # not necessarily pass down a `preferred_dc`.
    #
    dgit_reload_routes! if (preferred_dc != @preferred_dc)
    @preferred_dc = preferred_dc

    return @dgit_all_routes if @dgit_all_routes

    delegate = GitHub::DGit::Delegate::Network.new(network.id, original_shard_path)
    @dgit_all_routes = delegate.get_all_routes(preferred_dc: preferred_dc)
  end

  # Clear all remembered DGit routes.
  def dgit_reload_routes!
    @dgit_read_routes = @dgit_write_routes = @dgit_all_routes = nil
    @dgit_wiki_read_routes = @dgit_wiki_write_routes = nil
    @preferred_dc = nil
  end

  def recompute_checksums(read_host, ignore_dissent: false) # XXX: FIXME: Move this logic to callers and out of this AR class.
    tpc = GitHub::DGit.update_refs_coordinator(self)
    tpc.recompute_checksums(read_host, ignore_dissent: ignore_dissent)
  end

  # The maintenance queue used to enqueue jobs that run on the fs machine.
  #
  # Returns the maintenance queue name.
  def maintenance_queue_name
    network.maintenance_queue_name
  end

  # Write the name to a file in the git repo.
  # This is done without the distributed dgit lock, so is racy
  # See GitHub::Spokes.client.write_nwo_file for a safer approach
  def write_nwo_file_unsafe(nwo = name_with_owner)
    return unless exists_on_disk?
    rpc.fs_write("info/nwo", nwo)
    GitHub::DGit::Maintenance.safely_recompute_checksums(self, :vote)
  rescue Object => e
    Failbot.report(e)
  end

  # Is the repository server online and serving requests?
  def online?
    rescue_offline { rpc.online? }
  end

  # Try a block, and rescue all the errors that #online? rescues.
  def rescue_offline(result_if_offline: false)
    yield
  rescue GitRPC::InvalidRepository, GitRPC::RepositoryOffline, GitRPC::ConnectionError, Repository::UnroutedError, GitHub::DGit::UnroutedError, GitRPC::Timeout => e
    # If the repo is unrouted, we don't want to proceed with
    # git access (which blows up). But we also want to record
    # the problem repo to a special bucket.
    Failbot.report(e, app: "github-unrouted", repo_id: id)
    result_if_offline
  end

  # Are there routes for this repository?
  #
  # This is quicker than checking #online? and is a reasonably
  # close approximation to checking if GitRPC calls will succeed.
  def routed?
    host.present?
  rescue GitHub::DGit::UnroutedError => e
    Failbot.report(e, app: "github-unrouted", repo_id: id)
    false
  end

  # Resets the git cache key so that it's up to date with the repositories
  # pushed_at timestamp. This is typically only used in tests when a ref is
  # updated and you need to retrieve the updated ref values.
  def reset_git_cache
    @rpc = nil
    remove_instance_variable :@empty if defined?(@empty)
    reset_refs
  end

  JUNK_DIRS = ["dot_git", ".git", ".hg", ".svn", ".sass-cache", "build", "log", "tmp", "vendor"]

  # Get list of all files in tree.
  #
  # Designed for finder search results, so it excludes certain junk files by default.
  #
  # tree_oid - the oid of the tree to list
  # skip_directories - an array of which directories to ignore, as strings
  #
  # Returns Array of String file paths.
  def tree_file_list(tree_oid, skip_directories: JUNK_DIRS)
    rpc.tree_file_list(tree_oid,
      list_directories: false,
      list_submodules: false,
      skip_directories: skip_directories)
  end

  # Public: check if a file exists in the repository
  #
  # path - String full path
  # ref  - String ref to check for path
  #        (optional, defaults to default branch)
  #
  # Note this uses a very UNIX-y definition of "file" that's more like "directory entry"
  #
  # Returns Boolean
  def includes_file?(path, committish = default_branch)
    return true if path.empty? # repos always have a folder at their root
    return false unless ref = refs[committish]
    rpc.read_tree_entry(ref.target_oid, path, limit: 0)
    true
  rescue GitRPC::NoSuchPath, GitRPC::ObjectMissing
    false
  end

  # Public: check if a directory exists in the repository
  #
  # path - String full path
  # ref  - String ref to check for path
  #        (optional, defaults to default branch)
  #
  # Unlike #includes_file?, this checks if the path actually points to a directory (tree)
  #
  # Returns Boolean
  def includes_directory?(path, committish = default_branch)
    return true if path.empty? # repos always have a folder at their root
    return false unless ref = refs[committish]
    rpc.read_tree_entry(ref.target_oid, path, limit: 0, type: "tree")
    true
  rescue GitRPC::NoSuchPath, GitRPC::ObjectMissing, GitRPC::InvalidObject
    false
  end

  # Determines if the repository contains Xcode project files, allowing it
  # to be opened directly within Xcode after cloning.
  #
  # Returns true for Xcode repositories.
  def xcode_project?
    return false unless ref = refs[default_branch]
    result = rpc.read_tree_entries(ref.target_oid)
    result["entries"].any? { |entry| entry["name"].end_with?(".xcodeproj", ".xcworkspace", ".playground") }
  rescue GitRPC::NoSuchPath, GitRPC::ObjectMissing, GitRPC::InvalidObject
    false
  end

  # Public: find the longest existing part of a given path
  #
  # path - String full path
  # ref  - String ref to check for path
  #        (optional, defaults to default branch)
  #
  # Returns String path
  def longest_existing_subpath(path, ref = default_branch)
    return "" unless path
    redirect_path = path
    unless redirect_path.nil?
      until includes_directory?(redirect_path, ref)
        redirect_path = redirect_path.split("/")[0...-1].join("/")
      end
    end
    redirect_path = "" if redirect_path == "/"
    redirect_path
  end

  # Public: check if a path is okay for a file (editing or creating)
  #
  # path - String full path
  # ref  - String ref to check for path
  #        (optional, defaults to default branch)
  #
  # Returns Boolean
  def valid_file_path?(path, ref = default_branch)
    dir_segments = File.dirname(path).split("/")
    dir_segments = [] if dir_segments == ["."]

    file_indexes = dir_segments.each_index.select do |i|
      check_path = dir_segments[0..i].join("/")
      file = includes_file?(check_path, ref)
      dir  = includes_directory?(check_path, ref)

      if file && !dir
        # definitely not valid if something somewhere in the dirname is a regular file
        return false
      end
    end

    file = includes_file?(path, ref)
    dir  = includes_directory?(path, ref)

    !(file && dir)
  end

  # Public: Find Pushes by the specified user to the named branch in the last
  # hour.
  #
  # user        - User who did the pushing
  # branch_name - String name of the branch being pushed to (e.g., "master")
  #
  # Returns an Array of Push models.
  def recent_branch_pushes(user, branch_name)
    RecentBranchPushes.find(self, user, branch_name)
  end

  # Internal: Get the repository's three branches whose heads were most recently
  # touched by the specified user.
  #
  # user    - User object to find branches for
  # bad_oid - String target (commit) oid to ignore (optional)
  #
  # Returns an Array of Hashes with keys:
  #   :name - string of the branch's name
  #   :date - date of the last push
  def recently_touched_branches_for(user, bad_oid = nil)
    earliest_allowed_date = 1.hour.ago

    # Bail out early if there are no recently touched branches
    return [] if pushed_at < earliest_allowed_date

    # ignore the default and pages branches
    # and any branch that points to the default branch target
    bad_branches = [default_branch, pages_branch]
    bad_targets  = [default_oid, bad_oid].compact

    # Create hashes of branches from the push log
    # only care about the ones pushed by the current user
    # and were pushed less than an hour ago
    pushes = Push.latest_for(self, user, earliest_allowed_date)
    user_branches = pushes.map { |push|
      name = push.ref.sub("refs/heads/", "")
      next if bad_branches.include?(name)
      next if bad_targets.include?(push.after)

      # ignore any branch that points to the same target as the parent branch
      # cf `#base_branch`, currently in refs_dependency.rb
      if (base = base_branch(name, user).split(":")[1])
        bad_target = parent.heads.find(base).target_oid
        next if bad_target == push.after
      end

      { name: name, date: push.created_at, commit_oid: push.after }
    }.compact

    # if the branch has been pushed multiple times in the last hour,
    # only show the most recent push
    user_branches.uniq! { |b| b[:name] }

    # only show branches that still exist
    user_branches.reject! { |b| b[:commit_oid] == GitHub::NULL_OID }

    # Cheap ahead-behind test: we only want to show branches that are
    # *ahead* of the main branch. Instead of doing a full ahead-behind
    # count, we see if our branch is reachable from the default branch.
    # If so, the branch is obviously *not* ahead.
    descendant_info = rpc.descendant_of(user_branches.collect { |b| [default_oid, b[:commit_oid]] })
    user_branches.reject! { |b| descendant_info[[default_oid, b[:commit_oid]]] }

    return [] if user_branches.empty?

    # Find the pull requests for the remaining ones
    head_refs = user_branches.map { |b| b[:name] }
    pull_request_repository_ids = [id]
    pull_request_repository_ids.push parent_id if fork?
    matching_pull_requests = PullRequest.joins(:issue).where(
      base_repository_id: pull_request_repository_ids,
      head_ref: Git::Ref.permutations(head_refs)).where(
      [
        "(issues.state = ? OR
           (issues.state = ? AND issues.closed_at > ?)
         )",
         "open",
         "closed", earliest_allowed_date
      ],
    )

    pr_branches = Set.new(matching_pull_requests.collect(&:display_head_ref_name))

    # Filter out branches that have pull requests
    user_branches.reject! { |b| pr_branches.include?(b[:name]) }

    # Only the three most recent
    user_branches.first(3)
  end

  # Check if the repository directly exists on disk on the storage server. This
  # always makes an RPC call and should be used sparingly. Just because a
  # repository exists does not necessarily mean it has any branches, tags, or
  # other objects. The #empty? method is often a much better check for whether a
  # repository is in a useful state.
  #
  # If the repository isn't routed, returns false since there's no repo to check.
  #
  # Returns true when the repository exists.
  def exists_on_disk?
    host && shard_path && rescue_offline { rpc.exist? }
  rescue GitHub::DGit::UnroutedError
    false
  end

  # Check if the repository is "empty". Empty repositories exist on disk but do
  # not have any branches, tags, or other refs and therefore no commits.
  #
  # NOTE Because this method is very frequently used in before filters and other
  # early-on code in controller actions, it has been highly optimized to avoid an
  # RPC call to the storage servers to determine emptyness. Do not change this
  # unless you know what you're doing / know how to measure these types of perf
  # changes.
  #
  # Returns true when the repository is empty, false otherwise.
  def empty?
    return @empty if defined?(@empty)
    @empty =
      begin
        if (pushed_at.blank? && !fork?) || owner.nil?
          true
        else
          refs.empty?
        end
      rescue Errno::ENOENT
        # If the repo doesn't exist on disk yet then yes, it's definitely
        # empty.
        true
      end
  end

  def async_empty?
    Promise.all([async_internal_repository, async_owner, async_network]).then do
      empty?
    end
  end

  # Is this repository being created?  For repositories in dgit, we examine
  # the voting replicas to see if all were created recently _and_ any
  # are current in the `creating` state.
  def creating?
    return @creating if defined?(@creating)
    @creating =
      begin
        voting_replicas = GitHub::DGit::Routing.all_repo_replicas(self.id).select(&:voting?).sort_by(&:created_at)
        ready = voting_replicas.select { |r| r.checksum != "creating" }

        if ready.length >= GitHub.dgit_quorum
          false
        else
          if voting_replicas.empty?
            raise GitHub::DGit::NoVotingReplicas, "id:#{id}, network:#{source_id}"
          end
          # If the oldest replica is newer than 5 minutes old, then this is
          # a new repository being created.
          (Time.now - voting_replicas.first.created_at) < 5.minutes
        end
      end
  end

  def ready_for_writes?
    dgit_write_routes.any? && !creating?
  rescue GitHub::DGit::UnroutedError, GitHub::DGit::InsufficientQuorumError
    false
  end

  # Rails's Object#present? delegates to blank?, which in turn delegates to
  # empty?. This method is overridden above, and can return true on an existing
  # repository, which makes Repository#present? return false.
  #
  # To avoid Repository#present? (and any other methods that delegate to blank?)
  # from unexpectedly returning false, we override blank? here.
  #
  # Returns a boolean.
  def blank?
    false
  end

  def availability
    @availability ||= Availability.new(self)
  end

  def availability_status
    availability.state
  end

  # Check to see if this repo has been pushed to. This means it fits into one of
  # three things:
  #
  #   * pushed_at is nil (the repo was never touched)
  #   * pushed_at is older than created_at (this is an untouched fork)
  #   * pushed_at == created_at, within 15 seconds
  #                  (the repo was only initialized on create and never touched)
  #
  # Returns a Boolean
  def never_pushed_to?
    pushed_at.nil? || pushed_at <= (created_at + 15.seconds)
  end

  # Update the pushed_at_usec with the usec portion of the pushed_at timestamp.
  #
  # This works around mysql only storing second-resolution for timestamps.
  #
  # Called in before_save
  def update_pushed_at_usec(time)
    if time && (usec = time.usec) != 0
      self.pushed_at_usec = usec
    end
    true
  end

  # Combine pushed_at and pushed_at_usec to get a timestamp with usec set.
  #
  # Returns a Time object.
  def pushed_at
    if time = read_attribute(:pushed_at)
      time.utc.change(usec: pushed_at_usec || 0)
    end
  end

  def pushed_at=(time)
    update_pushed_at_usec(time)
    super
  end

  # Updates the pushed_at and pushed_at_usec timestamps without
  # running model callbacks.
  def update_pushed_at(time = Time.now)
    self.pushed_at = time

    self.class.where(id: id).update_all(pushed_at: pushed_at, pushed_at_usec: pushed_at_usec)
  end

  # Determine if this repository is offline. This accounts for the storage
  # server being down and also for cases where the storage server is up but the
  # repository is not available on disk due to the partition not being mounted
  # or a move being in progress.
  #
  # Returns true if the repository's storage server is offline or if the
  # repository doesn't exist on disk more than five minutes after being created.
  def offline?
    !online? ||
      (created_at < 5.minutes.ago && !exists_on_disk?)
  end

  # Ensures the repository exists on disk. This forks the parent repository when
  # the parent_id is set or creates a new bare repository when there is no
  # parent repository.
  def setup_git_repository
    return if exists_on_disk?

    if fork?
      RepositoryForkJob.perform_later(owner.id, id)
    elsif advisory_workspace?
      RepositoryCloneJob.perform_later(self, parent_advisory_repository&.internal_remote_url)
    elsif development_repository?
      symlink_development_repository!
    else
      create_git_repository_on_disk
      initialize_git_repository_templates
      update_pushed_at Time.now
      async_backup
    end
    reset_memoized_attributes
  end

  # Write template README and gitignore files if those options were selected
  # when the repository was created.
  def initialize_git_repository_templates
    TemplateInitializer.new(self).perform
  rescue Git::Ref::HookFailed => e
    self.template_hook_failure = e.message.to_s
  end

  # Generates the initial contents of the README.md file using the repository
  # name and description.
  #
  # Returns a string with the contents of the new README file.
  def generate_readme
    if user_configuration_repository?
      configuration_repository_readme_template
    else
      template = "# #{name}"
      template << "\n#{description}\n" if description && description.size > 0
      escape_generated_readme_html(template)
    end
  end

  # Special minimal HTML escape method for repository descriptions that are
  # inserted into generated READMEs. Only < and & are escaped so as not to make
  # the plain text of the generated README too tokeny but also avoiding
  # triggering HTML syntax in most cases.
  private def escape_generated_readme_html(content)
    content.gsub(/[&<]/) do |match|
      case match
      when "&"; "&amp;"
      when "<"; "&lt;"
      end
    end
  end

  # After create filter used when the repository already exists on disk. Just
  # updates the pushed_at timestamp to ensure caches are cracked.
  def setup_git_repository_if_exists
    if exists_on_disk?
      update_attribute :pushed_at, Time.now
    end
  end

  # Is this a defunkt/github development repository backed by the repository
  # at RAILS_ROOT/.git?
  def development_repository?
    Rails.development? && !GitHub.enterprise? &&
    %w[github/github defunkt/github].include?(name_with_owner)
  end

  # The root code of conduct object for the default branch
  def code_of_conduct
    @code_of_conduct ||= RepositoryCodeOfConduct.new(self)
  end

  # Make this repository use the development work tree git repository. Don't
  # even create this method in non-development environments.
  if Rails.development?
    def symlink_development_repository!
      system "
        rm -rf '#{shard_path}' &&
        mkdir -p $(dirname '#{shard_path}') &&
        ln -s '#{Rails.root}'/.git '#{shard_path}'
      "
      clear_ref_cache
    end
  end

  # DGit repos in test and dev have an origin that points to a directory
  # with `dgitN` somewhere in it, like
  #   .../github/github/repositories/test/dgit1/...
  #                                       ^^^^^
  # Since the origin is stored in the config, and the config is hard
  # state, that means each replica will have a different checksum, which
  # makes the new repo unroutable under DGit.
  #
  # To make unit-testing forked repos under DGit possible, we just point
  # all replicas at the same origin.  In prod, this would be a no-op,
  # since they're all pointing at the same origin (on their respective
  # servers) anyway.  But leave it undefined on prod to be extra safe.
  if Rails.development? || Rails.test?
    def overwrite_origin_for_tests!
      rpc.config_store("remote.origin.url", "file://#{shard_path}")
    end
  end

  # Verify that the repo.git/hooks symlink is in place and symlinked
  # to the appropriate hooks directory. This is used primarily in
  # development environments since the RAILS_ROOT is variable.
  def correct_hooks_symlink
    return if %w[development test].include?(Rails.env) && name == "github"

    rpc.symlink_hooks_directory
  end

  # Returns an instance of the graph cache for this repo
  def graph_cache
    @graph_cache ||= GitHub::RepoGraph::Cache.new(self, network_id, default_oid)
  end

  # Is the git graph cache enabled for this repo?
  #
  # Returns true or false
  def graph_cache_enabled?
    return unless online?
    !graph_cache.disabled?
  end

  # Toggle git graphing, turning it on if it's off, and off
  # if it's on
  def toggle_allow_git_graph
    if graph_cache_enabled?
      graph_cache.disable
    else
      graph_cache.enable!
    end
  end

  ##
  # Disk

  # Manage access to this repository and its network. Disabling a repository
  # cuts of access to all protocols: git, ssh, web and api.
  #
  # Examples:
  #
  #   repository.access.disable("size", staff_user)
  #   repository.access.dmca_takedown(staff_user, "https://....")
  #
  # See GitRepositoryAccess for details.
  def access
    @access ||= GitRepositoryAccess.new(self)
  end

  def country_blocks
    access.country_blocks
  end

  # Synchronize objects from this repository into the shared network.git
  # repository, or update an isolated repository's incremental commit-graphs.
  # This must eventually be called any time a repository's ref space is
  # modified.
  #
  # This is a potentially long-running operation. Use the
  # synchronize_shared_storage method to queue up a job instead.
  #
  # Returns nothing.
  def synchronize_shared_storage!
    rpc.nw_sync
  end

  # Enqueue a background job to run synchronize_shared_storage!.
  def synchronize_shared_storage
    RepositorySyncJob.set(queue: maintenance_queue_name).perform_later(id)
    true
  end

  # Run `git nw-repack` in safe mode. This a faster, less agressive version
  # of git-gc used for repository maintenance. Safe mode (-k) implies that
  # no objects will get purged from the repository, even if they are not
  # reachable
  def repack
    res = rpc.nw_repack(window_byte_limit: repack_window_byte_limit,
                  changed_path_bloom_filters: changed_path_bloom_filters_enabled?)
    raise RepackLocked.new(res.slice("ok", "status")) if res && res["status"] == 2
    raise RepackFailed.new(res.slice("ok", "status")) if res && !res["ok"]
    res

  # Raise a RepackLocked error if 1 <= N < nreplicas servers had a lock
  # conflict and the rest succeeded.
  rescue GitRPC::Protocol::DGit::ResponseError => e
    if locked = e.answers.values.find { |ans| !ans["ok"] && ans["status"] == 2 }
      if e.answers.values.all? { |ans| ans["ok"] || ans["status"] == 2 }
        raise RepackLocked.new(locked.slice("ok", "status"))
      end
    end
    raise
  end

  # Retrieve last run git-fsck output for the repository. This will never cause
  # an actual fsck operation.
  def fsck
    res = rpc.last_fsck(never: true)
    res["out"]
  end

  # Run git-fsck on the repository and store the result at <GIT_DIR>/fsck.
  def fsck!
    res = rpc.last_fsck(force: true)
    res["out"]
  end

  # Queue a job to run git-fsck on the repository.
  def async_fsck
    RepositoryFsckJob.perform_later(id)
  end

  DU_IGNORE = ["refs/__gh__/*", "refs/pull/*"]

  # Update the disk_usage attribute in the database
  # Returns the newly recorded disk usage value in kilobytes.
  def update_disk_usage
    disk_usage = exists_on_disk? && rpc.repo_disk_usage(DU_IGNORE)
    disk_usage = (disk_usage || 0) / 1024
    update_column :disk_usage, disk_usage
    disk_usage
  end

  # Get a human-friendly representation of the repository disk usage.
  #
  # Returns a String
  def human_disk_usage
    # number_to_human_size expects bytes, but disk_usage is reported in
    # kilobytes, so convert it first.
    number_to_human_size(disk_usage * 1024)
  end

  def ensure_owner_has_enough_repo_quota
    return true if fork? || public? || advisory_workspace?

    if owner.disabled? && !owner.plan_supports?(:repos, visibility: :private, fallback_to_free: true)
      errors.add(:visibility, "can't be private. Please update your payment information before creating a new private repository.")
      false
    elsif owner.at_private_repo_limit?
      errors.add(:visibility, "can't be private. Please upgrade your subscription to create a new private repository.")
      false
    else
      true
    end
  end

  # Internal: Verify the chosen template exists, otherwise set to nil.
  def ensure_gitignore_template_exists
    if gitignore_template.present? && !Gitignore.template_exists?(gitignore_template)
      errors.add(:gitignore_template, "is an unknown gitignore template.")
      false
    end

    true
  end

  # Internal: Verify the chosen license exists, otherwise set to nil.
  def ensure_license_template_exists
    if license_template.present? && !License[license_template]
      errors.add(:license_template, "is an unknown license template.")
      false
    end

    true
  end

  ##
  # Pages

  # Determine if the repository is a user pages repository. User pages
  # repositories are named as "<user>.github.io", or "<user>.github.com" if
  # "<user>.github.io" doesn't exist.
  #
  # Returns true if the repository is a user pages repository, false otherwise.
  def is_user_pages_repo?
    return false if GitHub.enterprise? # GHES does not have the concept of "user repos", as in dotcom
    async_is_user_pages_repo?.sync
  end

  def async_is_user_pages_repo?
    async_owner.then do |owner|
      next false unless owner # can happen during deletion
      next true if name_matches_new_user_pages?

      if name_matches_old_user_pages?
        next async_owner_owns_new_user_pages_repo?.then { |owns| !owns }
      end

      false
    end
  end

  def async_owner_owns_new_user_pages_repo?
    async_owner.then do |owner|
      next false unless owner
      Platform::Loaders::RepositoryByName.load(owner.id, "#{owner}.#{GitHub.pages_host_name_v2}").then do |repo|
        # TODO: Platform::Loader loaders should be able to detect if a record exists, rather than loading the whole thing.
        !!repo
      end
    end
  end

  def owner_owns_new_user_pages_repo?
    async_owner_owns_new_user_pages_repo?.sync
  end

  def name_matches_new_user_pages?
    return false unless owner.present?
    name.downcase == "#{owner.to_s.downcase}.#{GitHub.pages_host_name_v2}"
  end

  def name_matches_old_user_pages?
    return false unless owner.present?
    name.downcase == "#{owner.to_s.downcase}.#{GitHub.pages_host_name_v1}"
  end

  # Determine if the repository is a user pages repository with a custom domain
  #
  # Returns true if user pages with page.cname, else false.
  def is_cname_user_pages_repo?
    return page && page.cname && GitHub.pages_custom_cnames? && is_user_pages_repo?
  end

  # Return the expected branch name of which Pages is built.
  #
  # This function should be able to handle the cases where page is nil (and not defined yet)
  # and it does not necessarily return a branch that exists.
  def pages_branch
    # New behavior: use source_branch if it was set already, default branch for a user repo and gh-pages
    # for a project repo.
    if GitHub.flipper[:pages_any_branch].enabled?(owner)
      if page
        return page.source_branch
      elsif is_user_pages_repo?
        return default_branch
      else
        return "gh-pages"
      end
    end

    # Old behavior: User repos must build off master, else if a source_branch was set it can be used, in all other
    # cases gh-pages should be used.
    if is_user_pages_repo?
      "master"
    elsif page
      page.source_branch
    else
      "gh-pages"
    end
  end

  def has_gh_pages_branch?
    heads.include?("gh-pages")
  end

  def has_master_branch?
    heads.include?("master")
  end

  # Determine if a valid pages branch exists for this repository.
  #
  # Returns true if the repository has a pages branch, false if not.
  def has_gh_pages?
    return false if !GitHub.enterprise? && name_matches_old_user_pages? && owner_owns_new_user_pages_repo?
    return unless online?  # can't check the heads if it's not online
    heads.include?(pages_branch)
  end

  # Determine if the pages branch was created with the Page Generator.
  #
  # Returns true if the pages branch was generated, false if not.
  def has_generated_page?
    if has_gh_pages?
      ref = heads.find(pages_branch)
      blob(ref.target_oid, "params.json").present?
    else
      false
    end
  end

  # Return the pages host name for the owner of this Repository
  #
  # Depends on the pages host_name feature flag right now
  def pages_host_name
    page_for_url.url.async_pages_host_name.sync
  end

  # The GitHub Pages URL for this repository.
  #
  # Returns the String URL of the pages site, regardless of whether the
  #   repository has a pages branch or not.
  def gh_pages_url
    page_for_url.url.to_s
  end

  def async_gh_pages_url
    async_page.then do |page|
      # page_for_url uses `page`, or creates a temporary Page instance if one doesn't already exist.
      # It should not be saved to the database if it's a new instance.
      page_for_url.url.async_to_s
    end
  end

  # Create a standard page for this repository and writes it to a newly created
  # gh-pages branch.
  #
  # user            - the User record creating the page.
  # params          - Hash passed to the default page template as locals.
  # allow_overwrite - Whether an existing gh-pages branch can be overwritten
  #
  # Returns nothing.
  def pages_create(user, params, allow_overwrite = false)
    fail "branch already exists" if has_gh_pages? && !allow_overwrite
    GitHub::Pages.create(self, user, params)
  end

  # Rebuilds the repository's pages.
  #
  # Checks if a pages branch exists and the page is valid:
  #   Then, if the Pages integration is installed on this repo:
  #     A PageBuild job is queued.
  #   Otherwise:
  #     An AutomaticAppInstalltion event is triggered.
  # When no pages branch exists and a page object exists:
  #   The page is deleted from disk.
  #
  # pusher - The user that caused the pages to be rebuilt. Defaults to the
  #          owner of the repository. This user receives page build
  #          notifications.
  #
  # force_propagate_https_redirect - Whether to propagate_https_redirect to
  #          all other pages belonging to owner.
  #          Defaults to true when this repo is the user-pages repo, else false.
  #
  # git_ref_name - The ref to build. Does *not* include refs/heads/.
  #          Defaults to nil, and is a named parameter.
  #
  # Returns nothing.
  # Raises StandardError when page CNAME validation fails.
  def rebuild_pages(publisher = self.owner, force_propagate_https_redirect = false, git_ref_name: nil)
    return false if !plan_supports?(:pages)

    # If we're trying to create the page for the first time but the
    # publisher isn't allowed to create the page, then don't
    # allow the page to be published. This is a security feature.
    return false if !page && !can_create_page?(publisher)

    cname_user_repo_before = is_cname_user_pages_repo?
    # `has_gh_pages` checks default branch or `gh-pages` if the page has been destroyed,
    # so it does not always work as expected.
    # This should only affect scenarios used by staff.
    # See test `rebuilds the page if no page & the publisher is staff` for example.
    # Tracking issue: https://github.com/github/pages/issues/2826
    if has_gh_pages?
      page = self.page || self.build_page
      rebuilder = gh_pages_rebuilder(publisher)

      page.toggle_subdomain

      page_saved = GitHub::SchemaDomain.allowing_cross_domain_transactions { page.save }
      if page_saved && rebuilder.present?
        if should_install_pages_integration?
          AutomaticAppInstallation.trigger(
            type: :page_build,
            originator: {
              page_id: page.id,
              pusher_id: rebuilder.id,
              git_ref_name: git_ref_name,
            },
            actor: self,
          )
        else
          page.publish(rebuilder, git_ref_name: git_ref_name)
        end
      else
        raise ::Page::PageBuildFailed, "Page failed to build for repo id #{repository.id}"
      end
    elsif page = self.page
      page.destroy
    end

    if force_propagate_https_redirect || cname_user_repo_before != is_cname_user_pages_repo?
      queue_propagate_https_redirect
    end
    # TODO: stop returning true for "/staff/repos/:user/:repo/pages/builds"
    true
  rescue GitHub::Pages::Builder::MissingBuildEnvironment
    # ignore missing build environment errors in dev environments. causes
    # script/setup to fail otherwise.
    raise if !Rails.development?
  end

  # Determine if the given user can create pages site for this repository.
  # Due to security concerns, we don't want users with just write access to
  # be able to create pages -- we want them to have admin rights to the
  # repository. This better matches the Pages section in the Settings tab
  # in the UI, which allows users to create a Page for this repo only if an
  # admin on the repo.
  # We also want to allow staff to fire off the initial build.
  #
  # Returns true if either the user is an admin of the repo, or is staff.
  def can_create_page?(user)
    return false unless user

    adminable_by?(user) || user.site_admin?
  end

  # Is the gh-pages build failing?
  #
  # Returns a boolean
  def gh_pages_error?
    has_gh_pages? && gh_pages_last_build && gh_pages_last_build.error?
  end

  # Was the last gh-pages build a success?
  #
  # Returns a boolean
  def gh_pages_success?
    has_gh_pages? && gh_pages_last_build && gh_pages_last_build.status == "built"
  end

  # The error message for the most recently failed gh-pages build.
  #
  # Returns a simple String if the most recent build failed.
  #   (Markdown -> HTML now in EditRepositories::AdminScreen::PagesStatusView)
  # Returns nil otherwise.
  def gh_pages_error
    return unless gh_pages_error?
    gh_pages_last_build.try(:error)
  end

  # Most recent Pages build.
  #
  # Returns a Page::Build if one exists.
  def gh_pages_last_build
    page && page.builds.first
  end

  # Can this repository's Pages site be rebuilt
  # Used in stafftools and in rename jobs
  #
  # user - a user to check
  #
  # Returns true if the site can be rebuilt, otherwise false
  def gh_pages_rebuildable?(user = nil)
    !gh_pages_rebuilder(user).nil?
  end

  # Attemps to find a valid pusher that can trigger a Pages build
  #
  # user - a user to to check
  #
  # Returns the proper pusher, otherwise nil if not possible
  def gh_pages_rebuilder(user = nil)
    return user if user && !user.organization? && pullable_by?(user)
    return unless page

    # The given user can't pull the repo. Find the last non-staff builder and use them
    last_build = page.builds.find do |build|
      next false if GitHub.guard_audit_log_staff_actor? && build.pusher_id == User.staff_user.id
      build.pusher && !build.pusher.organization? && pullable_by?(build.pusher)
    end

    last_build.pusher if last_build
  end

  # Determines if the Pages integration is installed on this repo.
  #
  # Returns true the Pages integration exists and is installed on this repo.
  # Returns false othwerise.
  def pages_integration_installation_exists?
    return false unless GitHub.pages_github_app.present?
    IntegrationInstallation
      .with_repository(self)
      .where(integration_id: GitHub.pages_github_app.id)
      .first
      .present?
  end

  # Determines if the  Pages integration should be installed.
  def should_install_pages_integration?
    GitHub.flipper[:pages_github_app].enabled?(self) && !pages_integration_installation_exists?
  end

  ##
  # Service Hooks
  def test_service(hook)
    return unless last_push = Push.where(repository_id: id).last

    payload = {
      target_hook: hook,
      repo: self,
      pusher: last_push.pusher,
      ref: last_push.ref,
      before: last_push.before,
      after: last_push.after,
      triggered_at: Time.now,
    }
    event = Hook::Event::PushEvent.new(payload)
    delivery_system = Hook::DeliverySystem.new(event)

    delivery_system.generate_push_event_hookshot_payloads
    delivery_system.deliver_push_event_later
  end

  def hook_limit
    INCREASED_HOOK_LIMIT if increased_hook_limit?
  end

  def increased_hook_limit?
    GitHub.flipper[:increased_webhook_limit].enabled?(self) || GitHub.flipper[:increased_webhook_limit].enabled?(owner)
  end

  ##
  # Network, Forks, and Reparenting

  # The id of the RepositoryNetwork this repository belongs to.
  #
  # For historical reasons, the network_id column is named "source_id" in the
  # database. This is a simple alias but should be used in favor of source_id
  # because it's a better description.
  #
  # Returns the integer network id.
  def network_id
    source_id
  end

  def network_id=(value)
    self.source_id = value
  end

  # Set the network on this Repository and all decendent forks.
  #
  # network - A RepositoryNetwork object.
  #
  # Returns self.
  def recursive_set_network(network = self.network)
    self.network = network
    forks.each { |repo| repo.recursive_set_network(network) }
    save!
  end

  # Private: whether placeholder replicas and checksums need to be
  # instantiated during the first after_commit callback.  Set from
  # `initialize_repository_network` to avoid inadvertently creating
  # new replicas and checksum rows when unnecessary.
  def needs_dgit_initialization_after_commit?
    @needs_dgit_initialization_after_commit
  end
  attr_writer :needs_dgit_initialization_after_commit

  # Callback that creates the RepositoryNetwork record when this is the
  # first repository in a network, or associates the new repository with an
  # existing network when this is a fork. This is run after_create within the
  # same transaction as creating the repositories record to maintain
  # integrity between application tables.
  #
  # Spokes tables are instantiated with `initialize_replicas_from_network`
  # via after_commit callbacks, because those rely on performing inserts
  # into the Spokes database only after the initial application queries
  # have been committed to the main database.
  #
  # Returns nothing.
  def initialize_repository_network
    raise "Cannot initialize network on unsaved record" if id.nil? && parent.nil?

    if fork?
      update_attribute :network_id, parent.network_id if network_id.nil?
      @needs_dgit_initialization_after_commit = true
    elsif network.nil?
      network = create_network(root: self)
      network.repositories << self
      @needs_dgit_initialization_after_commit = true
    end
  end

  def delete_replicas_and_checksums_after_commit_on_destroy
    GitHub::DGit::Maintenance.delete_repo_replicas_and_checksums(network.id, self.id)
  end

  # Must be called any time the root repository changes to set the RepositoryNetwork#root
  # attribute. The root repository may change when a repository is deleted or
  # when a network adjustment is made, like detaching or setting a new root.
  #
  # repository - The new root repository.
  #
  # Returns nothing.
  def update_root_repository(repository)
    network.root = repository
    network.save!
  end

  # Public: Check whether a particular repo is part of this repo's network
  #
  # Returns Boolean
  def in_network?(repo)
    network_id && repo && network_id == repo.network_id
  end

  # Calculates the public_fork_count counter column value. The name is a little
  # misleading. This value is set differently based on the type of repository.
  # When the repository is the root repository, the value is set to the
  # total number of repositories in the whole network. When the repository is a
  # fork, the value is set to the number of immediate child repository forks.
  # When the repository is private, the value is set to zero no matter what.
  #
  # Returns the calculated count value.
  def calculate_public_fork_count!
    if public?
      if network_root?
        update_column :public_fork_count,
          network_repositories.public_scope.count - 1
      else
        update_column :public_fork_count, forks.public_scope.count
      end
    else
      update_column :public_fork_count, 0
    end
    public_fork_count
  end

  # Recursively recalculate network repository counts for this repository and all
  # parents.
  #
  # Returns the value returned from calculate_public_fork_count!
  def calculate_network_counts!
    parent.calculate_network_counts! if parent
    calculate_public_fork_count! if !frozen?
  end

  # Public: Returns an Integer number of direct forks for this Repository.
  def forks_count
    if public?
      public_fork_count
    else
      all_forks_count
    end
  end

  def all_forks_count
    @all_forks_count ||= forks.count
  end

  attr_writer :all_forks_count

  # Public: Returns an Integer number of Repositories in this network.
  def network_count
    if network.root.public?
      network.root.public_fork_count
    else
      full_network_count
    end
  rescue Object => boom
    # This is helpful in reporting networks with bad roots. Report these to github-user.
    Failbot.push(app: "github-user", network_id: network_id, parent_id: parent_id)
    report_error(boom)
    full_network_count
  end

  # Private: Counts the number of repositories in this repository's network.
  # Doesn't include the root repository.
  #
  # Returns an Integer.
  def full_network_count
    network_repositories.count - 1
  end

  # Find a repository owned by the given user in this repository's network.
  #
  # user - a String login name or User object. If no user can be found, nil is
  #        returned.
  #
  # Returns a Repository instance or nil if the user does not have a fork in
  # this repository's network.
  def find_fork_in_network_for_user(user)
    return self if user == owner
    network.find_fork_for(user)
  end

  # Find repositories in the same network as this repository
  # that the user has push access to.
  def find_pushable_forks_in_network_for_user(user)
    repo_ids = user.associated_repository_ids(min_action: :write)

    # Embedding the list of ids into the query is faster, but
    # only if the list of ids is rather small. If the list of ids
    # gets too big, the time spent on serializing the ids is most likely
    # higher than just getting all repos in the network and
    # performing the filtering on the app side.
    if repo_ids.size < 100
      Repository.in_same_network_as(self).where(id: repo_ids)
    else
      Repository.where(id: repo_ids & Repository.in_same_network_as(self).ids)
    end
  end

  # Find a *direct* fork of this repository owned by the given user.
  # This does not include forks of forks. If you need to look through
  # all possible descendants, use repository_and_descendants.
  #
  # user - User object to find a fork for.
  #
  # Returns a Repository instance or nil if the user does not have a fork of
  # this repository.
  def find_fork_for_user(user)
    forks.owned_by(user).first
  end

  # Used to report failed fork attempts.
  class ForkFailure < StandardError; end

  # Public: Fork this repository
  # See RepositoryForker for details
  #
  # options - Hash options that determine fork behavior. Default: {}
  #   :forker - The User performing the fork.
  #   :owner  - Deprecated alias of `:forker`.
  #   :org    - Optional. The Organization who should own the fork.
  #
  # Returns [false, Symbol reason, errors] if the fork cannot be created.
  # Returns [Repository fork, Symbol reason] if it worked.
  def fork(options = {})
    GitHub.dogstats.time "github.repository.fork" do
      RepositoryForker.new(self, options).fork
    end
  end

  def network_has_fork_for?(user)
    find_fork_in_network_for_user(user).present?
  end

  def forked_by?(user)
    find_fork_for_user(user).present?
  end

  # The long running side of #fork, called from a background job. Uses the
  # git-nw-clone utility to create the new repository from the parent at the
  # new location on disk. If the parent repository is linked to a shared
  # network.git repository, the new clone will also linked.
  #
  # Returns nothing.
  def clone_fork
    parent.enable_or_disable_shared_storage

    GitHub.dogstats.time("repository.fork.nw_clone") do
      parent.rpc.nw_clone(parent.default_branch, GitHub.repository_template, id)
    end

    if Rails.development? || Rails.test?
      overwrite_origin_for_tests!
    end

    # Do this after all other hard-state and git-state updates updates,
    # because it recomputes the DGit checksum.
    write_nwo_file_unsafe

    async_backup
    reset_git_cache
    refs
    copy_language_stats_from_parent
  end

  # Does this fork have unique code and more stargazers than its root?
  def popular_fork?
    return false if !fork?
    return false if untouched_fork?
    return false if stargazer_count == 0
    return false if stargazer_count < root.stargazer_count
    true
  end

  # Has this fork ever been pushed to?
  def untouched_fork?
    fork? && created_at >= pushed_at
  rescue
    false
  end

  def fork?
    !parent_id.nil?
  end

  def private_fork?
    private? && fork?
  end

  # Public: Is this repository a fork of an internal repository?
  #
  # Returns a boolean
  def internal_fork?
    fork? && root.internal? && private?
  end

  # Public: Does this repository allow forking?
  #
  # Returns a Boolean.
  def allows_forking?
    public? || allow_private_repository_forking?
  end

  MAX_HIERARCHY_DEPTH = 20

  # Finds all the parents for a forked repository. First item is the
  # most recent parent, last item is the root. If there are no
  # parents, returns an empty list. The list will never include this
  # repo - always distinct repos.
  #
  # Returns an Array of Repository objects or an empty Array.
  def parents
    r = self
    list = []

    while r.parent_id && r.parent_id != r.id
      r = r.parent
      list << r

      raise "Too many parents for repo #{id}!" if list.size > MAX_HIERARCHY_DEPTH
    end

    list
  end

  def descendant_ids
    repository_and_child_pairs = Repository.
      in_same_network_as(self).
      joins(:children).
      pluck(:id, Arel.sql("children_repositories.id AS child_id"))

    children_ids_by_repository_id = Hash.new { |h, k| h[k] = [] }
    repository_and_child_pairs.each_with_object(children_ids_by_repository_id) do |(repository_id, child_id), result|
      result[repository_id] << child_id
    end

    descendant_ids = []
    forks_to_be_visited = [id]

    while next_fork_id = forks_to_be_visited.pop
      children_ids = children_ids_by_repository_id[next_fork_id]
      next if children_ids.empty?

      forks_to_be_visited.concat(children_ids)
      descendant_ids.concat(children_ids)
    end

    descendant_ids
  end

  # Finds all descendants, incl. children, grand children, great grand children, etc.
  def descendants
    Repository.with_ids(descendant_ids)
  end

  # Check if this repo can be transferred to a new owner.
  #
  # Returns a boolean indicating transferrability
  def can_transfer_ownership?
    return false if trade_controls_read_only? || internal?
    return !private_fork?
  end

  def pending_transfer
    transfers.first
  end

  def pending_transfer?
    !pending_transfer.nil?
  end

  # Public: The list of environments this repo has been deployed to.
  #
  # Returns: An array of strings
  def deployment_environments
    deployments.group(:environment).order(nil).pluck(:environment)
  end

  # Public: The most deployed to environments for the repository.
  #         Considers the last 1000 deployments to the repository.
  #
  # Returns: An array of arrays. The second element in each array is the environment. The
  #          first element is the number of deployments with that environment.
  #
  # E.g. [[31, 'production'], [17, 'canary']
  def ranked_deployment_environments
    Deployment.github_sql.new(<<-SQL, repository_id: self.id).results
      SELECT COUNT(*) AS count_all, latest_environment AS deployments_environment
      FROM (
        SELECT id, latest_environment FROM deployments FORCE INDEX (index_deployments_on_repository_id_and_created_at) WHERE deployments.repository_id = :repository_id ORDER BY deployments.created_at DESC LIMIT 1000
      ) AS first_thousand
      GROUP BY latest_environment
      ORDER BY count_all desc
    SQL
  end

  def frequent_deploy_environments(first:)
    ranked_deployment_environments.first(first).map(&:second)
  end

  # Public: Request staff access to a private repository.
  #
  # A User object of the staffer requesting access must be passed in. A newly created
  # StaffAccessRequest object will be returned if successful.
  #
  # This does not create an actual access grant, nor an unlock — it only generates
  # the StaffAccessRequest.
  #
  # Returns a StaffAccessRequest.
  def request_staff_access(staffer, reason)
    self.staff_access_requests.create(requested_by: staffer, reason: reason)
  end

  # Public: Transfer ownership of this repository to the given user.
  #
  # user          - the user or organization to transfer this repository to.
  # target_teams  - (optional) the teams to add this repository to during the
  #                 transfer when transfering into an organization.
  # actor         - the user who is initiating the transfer.
  # notify_target - passing true will send the user an email letting them know that the repo was transferred
  #                 to them.
  def async_transfer_ownership_to(user, target_teams: [], actor:, notify_target: false)
    return false unless can_transfer_ownership?

    # don't let people get free private repos via transfer
    return false if private? && user.at_private_repo_limit?

    # disallow transfer when user has blocked repo's owner
    return false if owner.blocked_by?(user)

    team_ids = target_teams.collect(&:id)

    TransferRepositoryJob.perform_later(self.id, actor.id, user.id, team_ids, notify_target: notify_target)
  end

  # Internal: Dangerous! Changes the owner of this repository to the passed in
  # user. Should only be called from the TransferRepository job. Please use
  # the public #async_transfer_ownership_to method for all other calls.
  #
  # Example:
  #   We're moving olduser/reponame to newuser/reponame
  #
  #   irb$ repo = Repository.nwo("olduser/reponame")
  #   irb$ user = User.find_by_login("newuser")
  #   irb$ repo.transfer_ownership_to(user)
  #
  # Returns true if the transfer works; false if the new repo already exists or the
  # new owner already owns a fork in the same network.
  def transfer_ownership_to(new_owner, actor:, target_teams: [])
    # disallow transfer when user owns a repo in the same network
    return false if network.find_fork_for(new_owner)

    # check if user already has repo with same name (but not in the same network)
    return false if new_owner.find_repo_by_name(name)

    # disallow transfer when new_owner has blocked repo's owner
    return false if owner.blocked_by?(new_owner)

    GitHub.dogstats.increment("repository.transfer", tags: ["status:started"])

    set_transfer_in_progress(to: new_owner)

    move_downloads(owner, new_owner)
    old_owner = User.find(owner_id)
    old_licensing_enabled = licensing_enabled?
    old_nwo = name_with_owner

    installations = IntegrationInstallation.with_repository(self)

    # Select Apps that should be reinstalled after the transfer
    integrations_to_reinstall = installations.preload(:integration).select do |i|
      i.should_follow_moved_repo?(new_owner: new_owner)
    end.map(&:integration)

    # Remove all of the IntegrationInstallations from the repository before transferring
    remove_from_integration_installations(editor: old_owner, installations: installations)

    # Remove all of the linked projects from the repository.
    # Do this before changing the owner of the repo.
    project_repository_links.destroy_all

    update!(owner: new_owner)
    network.reload # to make sure pullable_by checks pick up the change (via network.owner)

    reinstall_integrations(new_owner: new_owner, integrations: integrations_to_reinstall)

    redirect_from_previous_location(old_nwo)

    # Add new teams first, because updating owning org will remove
    # the repo from any teams it is on
    if new_owner.organization?
      # give access to the specified teams
      target_teams.each do |team|
        team.add_repository(self, :pull, allow_different_owner: true)
      end
    end

    update_organization(inline_fork_cleanup: true, remove_collaborators: false)

    # Reset all collaborators, we'll rebuild them.
    collaborator_abilities = Authorization.service.direct_abilities_on_subject(subject: self, actor_type: User)
    # Since a role maps to an Ability these actor_ids should already be present in collaborator_abilities
    # we're just being extra careful here by querying user_roles as well
    user_role_ids = UserRole.where(actor_type: "User", target_id: self.id, target_type: "Repository").pluck(:actor_id)
    actor_ids = (collaborator_abilities.map(&:actor_id) + user_role_ids).uniq

    actors = User.where(id: actor_ids)
    actors = Hash[actors.map(&:id).zip(actors)]

    remove_all_members new_owner

    # Do the above member clean up and organization_id updates across the
    # repository network
    remove_members = new_owner.organization? && private?
    descendants.each do |child_repo|
      child_repo.remove_all_members(new_owner) if remove_members
      child_repo.update_organization
    end

    reload # refresh the various associations

    # Restore collaborators
    collaborator_abilities.each do |ability|
      # New owner can't be a collaborator too
      next if actors[ability.actor_id] == new_owner

      # We can't restore read collaborators if the new owner is a user, since
      # user-owned repositories can only have write collaborators and that
      # would increase their access.
      next if new_owner.user? && ability.read?

      # For now, organization-owned repos can grant collaborators any level of
      # access, but user-owned repos can only grant them write access. So, if
      # the new owner is a user, we drop read collaborators and add back the
      # rest of the collaborators as write collaborators (no matter what their
      # original permission was).
      action = new_owner.organization? ? ability.action : :write

      add_member(actors[ability.actor_id], new_owner, action: action)
    end

    packages.each do |package|
      package.transfer(old_owner: old_owner, new_owner: new_owner, actor: actor)
    end

    if old_owner.user?
      # transferring user still has access, but the new owner can remove
      add_member(old_owner, new_owner)
    end

    correct_issue_assignees
    correct_stargazers
    correct_watchers
    correct_project_cards(old_owner: old_owner)
    public_keys.each(&:save)
    unpublish_page unless plan_supports?(:pages)
    destroy_protected_branches unless plan_supports?(:protected_branches)
    UserRole.where(target_id: self.id, target_type: "Repository").destroy_all

    # reassign pull request owners
    PullRequest.where(base_repository_id: id).update_all(base_user_id: new_owner.id)
    PullRequest.where(head_repository_id: id).update_all(head_user_id: new_owner.id)

    Media::Blob.update_status_for_repository_transfer(repository,
      new_owner: new_owner, old_owner: old_owner)

    # If the old owner is an org, remove the repo from the old owner's pinned repositories.
    # Orgs are only allowed to pin their own public repos.
    if old_owner.organization?
      ProfilePinner.unpin(self, user: old_owner, viewer: actor)
    end

    # If the repository is featured on a Sponsors profile, remove it
    unfeature_from_sponsors_profile

    # Only clear for the new owner, the old owner gets their cache cleared when being added
    # back as a normal user to the repository
    Contribution.clear_caches_for_user(new_owner)

    instrument :transfer, {
      actor: actor,
      old_user: old_owner,
      owner: new_owner,
      owner_is_org: new_owner.organization?,
      owner_was_org: old_owner.organization?,
      repo_was: old_nwo,
    }

    # update the nwo file with new owner
    GitHub::Spokes.client.write_nwo_file(self, name_with_owner)

    RepositoryAdvisory.where(repository_id: id).update_all(owner_id: new_owner.id)
    RepositoryAdvisory::WorkspaceRepositoryManager.transfer_ownership(repository: self)
    transfers.destroy_all

    if old_licensing_enabled
      Billing::SnapshotLicensesJob.perform_later(old_owner.business)
    end

    if licensing_enabled?
      Billing::SnapshotLicensesJob.perform_later(owner.business)
    end

    GitHub.dogstats.increment("repository.transfer", tags: ["status:succeeded"])

    true
  rescue StandardError
    GitHub.dogstats.increment("repository.transfer", tags: ["status:failed"])
    raise TransferFailedError
  ensure
    set_transfer_in_progress(to: new_owner, completed: true)
  end

  TransferFailedError = Class.new(StandardError)

  # Called from TransferRepositoryJob via self.async_transfer_ownership_to
  def instrument_search_transfer_ownership_to(old_owner)
    if GitHub.flipper[:geyser_index].enabled?(self) && !GitHub.flipper[:geyser_denylist].enabled?(self)
      if GitHub.flipper[:geyser_index].enabled?(self.owner) && !GitHub.flipper[:geyser_denylist].enabled?(self.owner)
        # new owner is eligible for Geyser search treatment, change ownership
        # (results in a "repair" job on backend, deleting old repo docs and reindexing in full)
        payload = {
          change: :OWNER_CHANGED,
          repository: self,
          owner_name: self.owner.name,
          updated_at: Time.now.utc,
          ref: "refs/heads/#{self.default_branch}",
          old_owner_id: old_owner.id,
          publisher: :low_latency,
        }
      else
        # the new owner isn't eligible for Geyser search - emit a delete
        # for the repo (should ignore "owner" and use "repository.id" on backend
        # TODO FIXME: this can be removed once we're in GA!
        payload = {
          change: :DELETED,
          repository: self,
          owner_name: self.owner.name,
          updated_at: Time.now.utc,
          ref: "refs/heads/#{self.default_branch}",
          old_owner_id: old_owner.id, # janky hint of why delete was emitted here
          publisher: :low_latency,
        }
      end
    else
      # Do nothing - neither the old or new owner for this repo are eligible for Geyser search
      # TODO: remove once we're in GA!
      return
    end

    GlobalInstrumenter.instrument("search_indexing.repository_changed", payload)
    GitHub.dogstats.increment("geyser.repo_changed_event.published", tags: ["change_type:deleted"])
  end

  # Internal: mark the repo transfer "in progress" or "completed" via the
  # GitHub KV store.
  #
  # Returns nil.
  def set_transfer_in_progress(to:, completed: false)
    if completed
      GitHub.kv.del(transfer_in_progress_kv_key)
    else
      GitHub.kv.set(transfer_in_progress_kv_key, to.id.to_s, expires: 10.minutes.from_now)
    end
    nil
  end

  def transfer_in_progress?
    GitHub.kv.get(transfer_in_progress_kv_key).value { nil }
  end

  def transfer_in_progress_kv_key
    "repositories:transfer_in_progress:#{id}"
  end

  def correct_issue_assignees
    issues_with_bad_assignees = issues.select { |i| i.has_noncollab_assignee? }
    issues_with_bad_assignees.each { |i| i.save } # Let the before_save on issue do the work
  end

  # Internal: Remove repository for all IntegrationInstallations.
  #
  # Returns nil.
  def remove_from_integration_installations(editor:, installations:)
    installations.each do |installation|
      # Since the installation is on all we don't have to do anything.
      next if installation.installed_on_all_repositories?

      if installation.repository_ids.count == 1
        installation.uninstall(actor: editor)
      else
        installation.edit(
          editor: editor,
          repositories: installation.repositories - [self],
        )
      end
    end
  end

  # Add a record of this repository previously existing at a different
  # "user/repo" location. This is used to redirect requests for repositories
  # when their location changes.
  #
  # Returns a RepositoryRedirect object.
  def redirect_from_previous_location(name_with_owner)
    redirects.create(repository_name: name_with_owner)
  end

  # Find all repositories that are forks of this repository recursively. This
  # isn't very efficient. It'd probably be better to grab all repositories in
  # the network in a single query and figure out the parentage instead.
  def repository_and_descendants(repositories = [])
    repositories << self
    forks.each { |repo| repo.repository_and_descendants(repositories) }
    repositories
  end

  # the max depth to which we will recurse when checking for ancestry loops
  MAX_PARENT_DEPTH = 1000

  class InvalidAncestryError < StandardError
    attr_reader :failbot_context

    def initialize(message, repo_id:, new_parent_id: nil)
      super(message)
      @failbot_context = { repo_id: repo_id, parent_id: new_parent_id }
    end

    def areas_of_responsibility
      [:repos]
    end
  end

  # Moves this repository and all forks to a different parent, within the same
  # network.
  #
  # parent - The new parent repository for this repository. The new parent must
  #          be in the same network the repository is currently in.
  #
  # Returns self
  def reparent!(new_parent)
    raise ArgumentError, "cannot find new parent repository" unless new_parent
    raise InvalidAncestryError.new("cannot reparent across networks", repo_id: id) if new_parent.network_id != network_id

    if network.root == self
      new_parent.make_network_root!
      return self
    end

    validate_ancestry!(new_parent)

    transaction do
      update!(parent: new_parent)

      # this should be recursive to the repo's descendants but since it will become obsolete
      # when disallow_oopfs ships I'll just retain the current (buggy) behaviour and test it.
      update_organization
    end

    calculate_network_counts!
    self
  end

  # internal: `self` cannot be its own parent or its own ancestor. Recurse through the ancestry
  # to ensure this is not the case.
  #
  # raises InvalidAncestryError
  # returns nothing
  def validate_ancestry!(new_parent)
    candidate = new_parent
    (0..MAX_PARENT_DEPTH).each do |level_counter|
      break if candidate.nil?
      if level_counter >= MAX_PARENT_DEPTH
        raise InvalidAncestryError.new("exceeded max depth when reparenting", repo_id: id)
      elsif candidate == self
        raise InvalidAncestryError.new("cannot set repo as its own descendant", repo_id: id, new_parent_id: new_parent.id)
      end
      candidate = candidate.parent
    end
  end

  # Destructive! Renames a repository. This only changes database state since
  # repositories are named after their id. Also adds a redirect entry for the
  # old name, regenerates public key options strings, and manages any pages
  # related tasks.
  #
  # new_name - String name.
  # actor: User object or nil (optional, defaults to actor from GitHub.context).
  #
  # Returns a Boolean indicating whether or not the rename worked.
  def rename(new_name, actor: self.actor)
    completed = false

    normalized_name = EntityName.normalize(new_name)
    return false if normalized_name == name

    old_name = name.dup
    old_nwo = name_with_owner.dup

    was_cname_user_pages_repo = is_cname_user_pages_repo?

    transaction do
      transfers.destroy_all

      if update(name: normalized_name)
        redirect_from_previous_location(old_nwo)
        public_keys.each(&:save)

        # Renaming the user-page can change the URL for all project-pages
        # so force propagate_https_redirect if was or is cname_user_pages repo.
        rebuild_pages(actor || owner, was_cname_user_pages_repo || is_cname_user_pages_repo?)
        completed = true
      else
        self.name = old_name
        completed = false
      end
    end

    # This has to happen after the transaction is committed or the job will get
    # inconsistent data.
    if completed
      instrument :rename, old_name: old_name, actor_id: actor.id

      GlobalInstrumenter.instrument("repository.rename", {
        actor: actor,
        repository: self,
        previous_name: old_name,
        current_name: new_name,
      })

      RepositoryRenameJob.perform_later(id, old_name, nil)
    end

    completed
  end

  def lock!(lock_reason = nil)
    lock_including_descendants!(lock_reason)
  end

  # TODO: move this method out of repository once we have repo specs of some sort
  # public: true if the repo is currently being repaired
  def repairing?
    dgit_write_routes.size < GitHub.dgit_copies &&
      GitHub::Jobs::DgitRepairRepoReplica.is_repairing?(self)
  end

  # Public: Lock this repository.
  #
  # lock_reason - an optional reason for locking. Can be 'moving',
  # 'rename, or 'billing'.
  def lock_excluding_descendants!(lock_reason = nil)
    update locked: true, lock_reason: lock_reason
    auditing_actor = GitHub.guarded_audit_log_staff_actor_entry(actor)
    GitHub.instrument "staff.repo_lock", auditing_actor.merge(reason: (lock_reason || "unknown"), repo: self)
    true
  end

  # Public: Lock this repository and it's descendents.
  #
  # lock_reason - an optional reason for locking. Can be 'moving',
  # 'rename, or 'billing'.
  def lock_including_descendants!(lock_reason = nil)
    lock_excluding_descendants!(lock_reason)
    forks.each { |fork| fork.lock_including_descendants!(lock_reason) }
  end

  def unlock!
    unlock_including_descendants!
  end

  # Public: Unlock this repository.
  def unlock_excluding_descendants!(log_as_part_of_job: nil)
    auditing_actor = GitHub.guarded_audit_log_staff_actor_entry(actor)
    GitHub.instrument "staff.repo_unlock", auditing_actor.merge(reason: (lock_reason || "unknown"), repo: self)
    if log_as_part_of_job
      GitHub.dogstats.increment "repository", tags: ["action:unlock_excluding_descendants", "log_as_part_of_job:#{log_as_part_of_job.underscore}"]
      GitHub::Logger.log \
        job: log_as_part_of_job,
        at: "repository.unlock_excluding_descendants",
        repo_id: self.id
    end
    update locked: false, lock_reason: nil
    true
  end

  # Public: Unlock this repository and it's descendents.
  def unlock_including_descendants!(log_as_part_of_job: nil)
    if log_as_part_of_job
      GitHub.dogstats.increment "repository", tags: ["action:unlock_including_descendants", "log_as_part_of_job:#{log_as_part_of_job.underscore}"]
      GitHub::Logger.log \
        job: log_as_part_of_job,
        at: "repository.unlock_including_descendants",
        repo_id: self.id
    end
    unlock_excluding_descendants!(log_as_part_of_job: log_as_part_of_job)
    forks.each do |fork|
      fork.unlock_including_descendants!(log_as_part_of_job: log_as_part_of_job)
    end
  end

  def lock_for_move
    lock_including_descendants!("moving")
  end

  def lock_for_billing
    lock_including_descendants!("billing")
  end

  def lock_for_migration
    lock_excluding_descendants!("migrating")
  end

  def locked_on_move?
    locked? && lock_reason == "moving"
  end

  def locked_on_disk?
    locked? && !exists_on_disk?
  end

  def locked_on_billing?
    locked? && lock_reason == "billing"
  end

  def locked_on_rename?
    locked? && lock_reason == "rename"
  end

  def locked_on_migration?
    locked? && lock_reason == "migrating"
  end

  def moving?
    locked_on_move? || network.moving?
  end

  # Fetch this repository into its network repository, but only when there
  # are other repositories in the network or the network repository already
  # exists.
  #
  # force - Causes the repository's pushed_at timestamp to be updated before
  #         updating the network repository, which invalidates the
  #         has-been-fetched and ref caches.
  #
  # Returns true when a job was queued to sync the repository, nil otherwise.
  def update_network_repository(force = false)
    update_attribute :pushed_at, Time.now if force
    synchronize_shared_storage
  end

  # A most-recently cached NetworkGraph for the repository. Use NetworkGraph.new
  # for historical nethash support.
  def network_graph
    @network_graph ||= Repository::NetworkGraph.for_repository(self)
  end

  ##
  # Deploy Keys
  def public_keys=(new_keys)
    new_keys = [new_keys].flatten.reject(&:blank?)

    public_keys.each do |key|
      public_keys.delete(key) unless new_keys.include? key.to_s
      new_keys.delete(key.to_s)
    end

    new_keys.each do |key|
      pk = PublicKey.new(repository: self, key: key.to_s)
      public_keys << pk
    end
  end

  ##
  # Downloads

  def download_count
    tags.size + downloads.count
  end

  def move_downloads(old_owner, new_owner)
    if Rails.env.development?
      warn "warn: skipping move_downloads for repository #{self}"
      return
    end

    MoveRepoDownloadsJob.perform_later(id, {repo_owner: old_owner.to_s}, {repo_owner: new_owner.to_s})
  end

  def rename_downloads(old_name, new_name)
    MoveRepoDownloadsJob.perform_later(id, {repo_name: old_name.to_s}, {repo_name: new_name.to_s})
  end

  def async_update_download_acls
    DelayedJob.perform_later("Repository", id, :update_download_acls)
  end

  def update_download_acls
    downloads.each do |d|
      begin
        d.update_permissions
      rescue Aws::S3::Errors::NoSuchKey
        # Sometimes files don't make it to S3, this shouldn't be fatal
      end
    end
  end

  ##
  # Pull Requests

  # A performance-aware method to count the number of open requests for a
  # repository.
  #
  # viewer - The User who is viewing the count (current_user)
  #
  # Returns the number of open requests as an Integer.
  def open_pull_request_count_for(viewer, limit: 5_000)
    @open_pull_request_count_for ||= {}
    @open_pull_request_count_for[[viewer, limit]] ||= begin
      # Hide spammy pull requests
      scope = issues.filter_spam_for(viewer, show_spam_to_staff: false)

      # Apply the specified limit.
      # We started adding 1 to the limit in
      # https://github.com/github/github/pull/44952 so that we would know if
      # the limit was crossed or not.
      scope = scope.limit(limit + 1)

      scope.where("state = 'open' and pull_request_id > 0").count
    end
  end

  def open_issue_count_for(viewer, limit: 5_000)
    @open_issue_count_for ||= {}
    @open_issue_count_for[[viewer, limit]] ||= begin
      # Hide spammy issues
      scope = issues.filter_spam_for(viewer, show_spam_to_staff: GitHub.flipper[:show_spammy_issues_to_staff].enabled?(viewer))

      # Apply the specified limit.
      # We started adding 1 to the limit in
      # https://github.com/github/github/pull/44952 so that we would know if
      # the limit was crossed or not.
      scope = scope.limit(limit + 1)

      scope.where("state = 'open' and pull_request_id is null").count
    end
  end

  def self.find_all_public(page = 1)
    rel = where(public: true, parent_id: nil).where("pushed_at < NOW()").order("id DESC").page(page)
    rel.total_entries = -1
    rel
  end

  ##
  # Google

  # Should we be hiding this repository from Google results?
  # There are a number of reasons we might want to do this:
  #   - The repository is empty
  #   - The repo is flagged as spammy
  #   - Fork with nothing in it
  #   - Fork with only pull requests / minor changes
  #   - Stafftools revokes privilege
  #
  # Returns a Boolean
  def hide_from_google?
    return true if route.nil? || empty? # empty
    return true if spammy?
    return true if self.noindex?

    if fork?
      # If this is a fork with a different name than its root,
      # include it in Google results. Probably a new network.
      return false if name != root.name
      return true if !popular_fork?
    end

    return false # let Google see it
  end
  alias :hide_from_search? :hide_from_google?

  # Should we be adding this repository to the search index? Reasons for
  # keeping it out of the search index are:
  #   - Not routed on the file servers
  #   - The user is a spammer or doesn't exist
  #   - Disabled by an admin
  #
  # Return `true` if we should add the repository to the search index; return
  # `false` if we should not.
  #
  def repo_is_searchable?
    return false unless active?
    # When the repo does not belong to a valid network (usually means in the
    # process of deleting or restoring).
    return false if network.nil?
    # When the .git repo is un-routed on the file servers
    return false unless routed?
    # When user is nil don't index it
    return false if user.nil?
    # When the repo or the user is spammy
    return false if spammy?
    # When the repo has been disabled for any reason
    return false if !disabled_at.nil?
    # When the repository network is broken
    return false if network.broken?
    # The repo is safe to index
    return true

  # Since rpc is memoized, rpc.online? can return true for just-deleted
  # repos, and then we get an UnroutedError when we call route.  Catch
  # that exception and don't log it.
  rescue GitHub::DGit::UnroutedError
    false
  rescue StandardError => boom
    report_error boom
    false
  end

  # Should we be adding the source code for this repository to the search
  # index? Reasons for keeping it out of the search index are:
  #   - The repository is not searchable
  #   - No content in the git repo
  #   - Fork with no unique commits
  #   - Fork with fewer watchers than the parent
  #
  # Return `true` if we should add the source code to the search index; return
  # `false` if we should not.
  #
  def code_is_searchable?
    return false unless repo_is_searchable?
    # Definitely hide if this repo is empty.
    return false if empty?
    # Code search has been specifically enabled for a fork
    return true if code_search_enabled
    # Also hide forks with no unique code or no significant popularity
    return false if fork? && !popular_fork?
    # If we got this far, then add to the search index
    return true

  # Code is not searchable if we don't have a route to the repo
  rescue GitHub::DGit::UnroutedError
    false
  end

  # Should we be adding the commits for this repository to the search
  # index?
  #
  # Return `true` if we should add the commits to the search index; return
  # `false` if we should not.
  #
  def commits_are_searchable?
    return false unless repo_is_searchable?
    # Definitely hide if this repo is empty.
    return false if empty?
    # Also hide forks with no unique code or no significant popularity
    return false if fork? && !popular_fork?
    # If we got this far, then add to the search index
    return true

  # Commits are not searchable if we don't have a route to the repo
  rescue GitHub::DGit::UnroutedError
    false
  end

  ##
  # Uncategorized

  # Check the repository for spam
  #
  # options - currently unused
  def check_for_spam(options = {})
    return if spammy?
    reason = GitHub::SpamChecker.test_repo(self)
    owner.safer_mark_as_spammy(reason: reason) if reason
  end

  # Internal: Update the `primary_language` association with a new language,
  # and populate the the primary_language_name cache from the
  # `primary_language` association.
  def update_primary_language!(language)
    self.primary_language_name_id = language.id # association
    self.primary_language_name = language.name # cached name value
    save!
  end

  # Internal: Unset the `primary_language`. For cases where no language is
  # now detected for a repository. `primary_language_name` cache needs
  # setting to nil also.
  def unset_primary_language!
    self.primary_language_name_id = nil # association
    self.primary_language_name = nil # cached name value
    save!
  end

  def name_is_not_a_wiki_repo
    return unless active?
    if name.end_with?(".wiki")
      errors.add(:name, "cannot end in .wiki")
    end
  end

  include Stratocaster::EventTarget

  # Public: Generates the Stratocaster event key for Repositories.
  #
  # :type - A Symbol identifying a sub type, or nil.
  #         :issues  - The Repository issues feed.
  #         :network - The feed of public actions for repositories in the same
  #                    network.
  #
  # Returns a String key.
  def events_key(options = {})
    case options[:type]
    when :issues then "repo:#{id}:issues"
    when :network then "network:#{network_id}:public"
    when nil then "repo:#{id}"
    else
      raise ArgumentError, "Invalid :type"
    end
  end

  def auto_subscribe_owners
    owners = owner.organization? ? owner.admins : [owner]
    GitHub.newsies.async_subscribe_users_to_repository(self.id, owners.map(&:id), created_by_user_id)
  end

  def instrument_repo_added_to_installations_across_all_repositories
    return true if advisory_workspace?
    if installations_on_all = owner.installations_on_all_repositories
      installations_on_all.each do |installation|
        installation.instrument :repositories_added, actor_id: created_by_user_id, repositories_added: [self.id], repository_selection: "all"
      end
    end
  end

  def instrument_repo_removed_from_installations
    return true unless owner.present?
    return true if advisory_workspace?
    if installations = IntegrationInstallation.with_repository(self)
      installations.each do |installation|
        selection = installation.installed_on_all_repositories? ? "all" : "selected"
        installation.instrument :repositories_removed, actor_id: deleted_by_user_id, repositories_removed: [self.id], repository_selection: selection
      end
    end
  end

  def report_error(exception, options = {})
    Failbot.report(exception, { repo_id: id.to_s }.update(options))
  end

  # After destroy callback to delete all newsfeed data for the repository.
  def delete_all_newsies_data
    GitHub.newsies.async_delete_all_for_list(self)
  end

  include Instrumentation::Model

  def event_prefix() :repo end
  def event_payload
    payload = {
      event_prefix => self,
      :visibility  => visibility.to_sym,
    }

    # Could be a user or org, determine event_prefix on instance
    payload[owner.event_prefix] = owner if owner.present?

    if parent_id && parent
      payload[:fork_parent] = parent
    end

    if network && root != parent
      payload[:fork_source] = root
    end

    payload
  end

  def event_context(prefix: :repo)
    {
      prefix => name_with_owner,
      "#{prefix}_id".to_sym => id,
    }
  end

  def instrument_creation
    payload = {}

    if created_by.present?
      payload[:actor] = created_by
    end

    instrument :create, payload
    GlobalInstrumenter.instrument "repository.create", {
      actor: created_by,
      repository: self,
      gitignore_template: gitignore_template,
      license_template: license_template,
      init_with_readme: auto_init,
    }
  end

  def instrument_default_branch_update(branch:, old:)
    instrument :update, actor: actor, changes: {
      default_branch: branch,
      old_default_branch: old,
    }
  end

  def instrument_search_default_branch_update(new_ref)
    payload = {
      change: :DEFAULT_BRANCH_CHANGED,
      repository: self,
      owner_name: self.owner.name,
      updated_at: Time.now.utc,
      ref: new_ref,
    }

    GlobalInstrumenter.instrument("search_indexing.repository_changed", payload)
    GitHub.dogstats.increment("geyser.repo_changed_event.published", tags: ["change_type:default_branch_changed"])
  end

  def instrument_update
    changes = {}.tap do |hash|
      if previous_changes.has_key?("description")
        hash[:old_description] = previous_changes["description"].first
        hash[:description] = description
      end
      if previous_changes.has_key?("homepage")
        hash[:old_homepage] = previous_changes["homepage"].first
        hash[:homepage] = homepage
      end
    end

    unless changes.empty?
      instrument :update, actor: actor, changes: changes
    end
  end

  # Public: Log an audit log event when the repository is opted out of or back into being available
  # to recommend to users via the "Discover repositories" page.
  def instrument_repository_recommendations_change(opt_out:, actor:)
    key = opt_out ? :opt_out_of_recommendations : :opt_into_recommendations
    instrument(key, actor: actor)
  end

  # Public: Check whether repository fulfills dormancy guidelines
  # (Not created or pushed to for at least 6 months)
  #
  # Returns Boolean
  def dormant?(threshold = GitHub.dormancy_threshold)
    dormancy_time = Time.now - threshold
    [:pushed_at, :created_at].all? do |attr|
      t = send(attr)
      t.nil? || t < dormancy_time
    end
  end

  # Public: Do we allow public push to this repo?
  #
  # Returns true if the repository allows pushes from anyone with an account,
  # false otherwise.
  def public_push?
    GitHub.public_push_enabled? && public? && !!public_push
  end

  # Public: summarizes recent activity for this repository.
  #
  #
  # viewer  - User viewing the summary.
  # since   - When to summarize from. Absolute int unixtime.
  # period  - Alternative to since, specify a time period name like daily,
  #           halfweekly, weekly, or monthly
  # branch  - Branch to look at for commit summary.
  #
  # Returns an ActivitySummary object for the repository's recent activity.
  def activity_summary(options = {})
    if defined?(@activity_summary) && (options == @activity_summary_options)
      return @activity_summary
    end

    # Keep the options used to build the ActivitySummary cached so we can
    # invalidate if someone asks for an ActivitySummary with different options.
    @activity_summary_options = options
    @activity_summary = ActivitySummary.new(self, **options)
  end

  # Repository ids differ between production and development and Analytics uses
  # the production ID to identify a repository.  Let's always use twbs/bootstrap
  # in development mode.
  def id_for_analytics
    Rails.production? ? id : 2126244
  end

  # Fetch the search result for this Repository.
  #
  # Returns a Hash of repo details.
  def search_entry
    index = Elastomer::Indexes::Repos.new
    result = index.docs.get type: "repository", id: self.id
    result["found"] ? result["_source"] : {}
  end

  def read_only?
    network.read_only?
  end

  # Public: Returns true if the repository has CI services (including the
  # GitHub Actions app) operating.
  def has_ci?
    # exclude first party (GitHub-owned/operated) GitHub Apps that have access
    # to write `checks` to prevent core product features from being counted as
    # CI apps. We consider GitHub Actions to count as CI so it is excluded from
    # this check.
    IntegrationInstallation.for_actions(self).any? ||
      has_third_party_ci?
  end

  # Public: Returns true if the repository has third-party CI services operating.
  def has_third_party_ci?
    Statuses::Service.statuses_for_repo_exist?(repository_id: id) ||
      third_party_ci_integration_installations.any?
  end

  def has_dockerfile?
    tree_entry(ref_to_sha(default_branch), "Dockerfile").present?
  rescue GitRPC::Error
    # GitRPC::InvalidFullOid will happen if default_branch doesn't exist.
    # GitRPC::NoSuchPath will happen if Dockerfile doesn't exist.
    false
  end

  # Detect under what license the repository is licensed
  #
  # Returns the License model if a license is known, otherwise nil
  def license
    repository_license.license unless repository_license.nil?
  end

  # Detects under what license the repository is licensed and stores the result
  # in the repository_licenses table.
  #
  # Returns the license string
  def set_license
    RepositoryLicense.set_license(self)
  end

  # Public: The preferred LICENSE file from the Repository root.
  #
  # Returns a TreeEntry or nil.
  def preferred_license
    preferred_file(:license)
  end

  def async_preferred_license
    async_preferred_file(:license)
  end

  # Public: check if the repo is private and shouldn't be
  #
  # Returns a boolean
  def disabled_private?
    private? && cant_be_private?
  end

  # Private: check if the root owners plan supports private repos
  #
  # Returns a Boolean
  private def cant_be_private?
    return false if advisory_workspace?
    user_to_check = network_root? ? owner : plan_owner
    !user_to_check.plan_supports?(:repos, visibility: :private, fallback_to_free: user_to_check.disabled?)
  end

  # Private: Find preferred files from PreferredFile::Types.
  #
  # Searched from the root for the default_branch.
  #
  # Returns a TreeEntry or nil.
  private def preferred_file(type)
    async_preferred_file(type).sync
  end

  private def async_preferred_file(type)
    @preferred_files ||= {}

    return Promise.resolve(@preferred_files[type]) if @preferred_files.key?(type)

    access.async_broken?.then do |broken|
      next nil if broken

      async_default_branch.then do |default_branch|
        local_file = PreferredFile.find(
          directory: directory(default_branch),
          type: type,
        )

        next @preferred_files[type] = local_file unless local_file.nil?

        async_load_global_preferred_file(type).then do |global_file|
          @preferred_files[type] = global_file
        end
      end
    end
  end

  private def async_load_global_preferred_file(type)
    return Promise.resolve(nil) unless PreferredFile::GLOBAL_TYPES.include?(type)
    return Promise.resolve(nil) if global_health_files_repository?

    Platform::Loaders::GlobalHealthFilesRepository.load(owner_id).then do |global_repository|
      next unless global_repository.present?

      global_repository.async_default_branch.then do |default_branch|
        PreferredFile.find(
          directory: global_repository.directory(default_branch),
          type: type,
        )
      end
    end
  end

  # Public: destroy all protected branches for this repo
  #
  # Returns array
  def destroy_protected_branches
    protected_branches.destroy_all
  end

  # Public: Is this repository the global health files repository for an organization?
  #
  # Returns a Boolean.
  def global_health_files_repository?
    return false unless public?

    name == GLOBAL_HEALTH_FILES_NAME
  end

  # Public: The preferred CODE_OF_CONDUCT file from the Repository root.
  #
  # Returns a TreeEntry or nil.
  def preferred_code_of_conduct
    preferred_file(:code_of_conduct)
  end

  def async_preferred_code_of_conduct
    async_preferred_file(:code_of_conduct)
  end

  # Public: The preferred CONTRIBUTING file from the Repository root.
  #
  # Returns a TreeEntry or nil.
  def preferred_contributing
    preferred_file(:contributing)
  end

  def async_preferred_contributing
    async_preferred_file(:contributing)
  end

  def preferred_funding
    preferred_file(:funding)
  end

  def global_preferred_funding
    @global_preferred_funding_memo ||= async_load_global_preferred_file(:funding).sync
  end

  def async_preferred_funding
    async_preferred_file(:funding)
  end

  # Public: The preferred README file from the Repository root.
  #
  # Returns a TreeEntry or nil.
  def preferred_readme
    preferred_file(:readme)
  end

  def async_preferred_readme(ref_name: nil)
    async_default_branch.then do
      branch = ref_name || default_branch
      tree_entry = directory(branch)&.preferred_readme
      next unless tree_entry

      tree_entry.ref_name = branch
      tree_entry
    end
  end

  # Public: The preferred SUPPORT file from the Repository root.
  #
  # Returns a TreeEntry or nil.
  def preferred_support
    preferred_file(:support)
  end

  # Public: The preferred ISSUE_TEMPLATE file from the Repository root.
  #
  # Returns a TreeEntry or nil.
  def preferred_issue_template
    preferred_file(:issue_template)
  end

  def async_preferred_issue_template
    async_preferred_file(:issue_template)
  end

  # Public: The preferred PULL_REQUEST_TEMPLATE file from the Repository root.
  #
  # Returns a TreeEntry or nil.
  def preferred_pull_request_template
    preferred_file(:pull_request_template)
  end

  def async_preferred_pull_request_template
    async_preferred_file(:pull_request_template)
  end

  def preferred_security_policy
    preferred_file(:security)
  end

  # Public: The preferred TOKEN_SCANNING file from the Repository root.
  #
  # Returns a TreeEntry or nil.
  def preferred_token_scanning
    preferred_file(:token_scanning_configuration)
  end

  def async_preferred_token_scanning
    async_preferred_file(:token_scanning_configuration)
  end

  def security_policy
    @security_policy ||= SecurityPolicy.new(self)
  end

  # Is code of conduct detection enabled for this repository?
  #
  # Returns true if a public repository or if Enterprise
  def detect_code_of_conduct?
    public?
  end

  # Look through the directory entries of the default branch for a
  # code of conduct file.
  #
  # Returns a boolean.
  def detect_code_of_conduct
    !!preferred_code_of_conduct
  end

  # Public: Returns an elasticsearch query string that will find all of this
  # repo's audit log events
  #
  # Returns a String which can be passed to elasticsearch as a `query_string`
  def audit_log_query
    "repo_id:#{id}"
  end

  def unverified_public_keys?
    public_keys.any? { |key| !key.verified? }
  end

  def pre_receive_hooks
    @pre_receive_hooks ||= begin
      hooks = {}
      targets = PreReceiveHookTarget.for_hookable_and_parents(self)
                                    .includes(hook: [:environment])
                                    .order(Arel.sql("CASE hookable_type
                                                     WHEN 'Business'   THEN 0
                                                     WHEN 'User'       THEN 1
                                                     WHEN 'Repository' THEN 2
                                                     END"),
                                           :id)
      targets.each do |target|
        hook_id = target.hook_id
        current = hooks[hook_id]
        next if current && current.final?
        # delete existing entry so new one is inserted at the right order
        hooks.delete(hook_id)
        hooks[hook_id] = GitHub::PreReceiveHookEntry.new(
          target.hook.environment.id,
          target.hook.environment.checksum,
          hook_id,
          target.hook.repository_id,
          target.hook.script,
          target.enforcement_before_type_cast, # expecting the raw value, e.g. 2
          target.final,
        )
      end

      hooks.values.select { |entry| entry.enforcement != GitHub::PreReceiveHookEntry::DISABLED }
    end
  end

  def has_pre_receive_hooks?
    GitHub.pre_receive_hooks_enabled? && pre_receive_hooks.any?
  end

  # Public: Returns the internal URL to notify of a git push.
  #
  # wiki - Should be true if you want to get the URL for the associated wiki.
  def post_receive_hook_url(wiki = false)
    if wiki
      "#{GitHub.githooks_api_url}/internal/repositories/#{name_with_owner}/wiki/git/pushes"
    else
      "#{GitHub.githooks_api_url}/internal/repositories/#{name_with_owner}/git/pushes"
    end
  end

  # Public: Return the default merge method for the given viewer.
  #
  # viewer - The User viewing a pull request in the repository.
  #
  # This is based on the last used merge method by the viewer and
  # the allowed/disallowed merge methods set on the repository.
  def default_merge_method_for(viewer)
    if sticky_merge_method(viewer) == "squash" && squash_merge_allowed?
      :squash
    elsif sticky_merge_method(viewer) == "rebase" && rebase_merge_allowed?
      :rebase
    elsif merge_commit_allowed?
      :merge
    elsif squash_merge_allowed?
      :squash
    elsif rebase_merge_allowed?
      :rebase
    else
      :merge
    end
  end

  def sticky_merge_method(user)
    return "merge_commit" if id.nil? || user.nil?
    result = GitHub.kv.get("repo.merge_method.#{id}.user.#{user.id}")
    result.value { nil } || "merge_commit"
  end

  def set_sticky_merge_method(user, val)
    GitHub.kv.set("repo.merge_method.#{id}.user.#{user.id}", val)
  end

  def merge_commit_allowed?
    id.nil? || GitHub.kv.get("repo.merge_commit_blocked.#{id}").value! != "true"
  end

  def squash_merge_allowed?
    id.nil? || GitHub.kv.get("repo.squash_merge_blocked.#{id}").value! != "true"
  end

  def rebase_merge_allowed?
    id.nil? || GitHub.kv.get("repo.rebase_merge_blocked.#{id}").value! != "true"
  end

  # MergeMethodError is raised either
  # 1. when attempting to disable all merge methods, or
  # 2. when there is a protected branch rule with linear history requirement,
  #    attempting to disable both squash and merge.
  class MergeMethodError < ArgumentError
    attr_reader :reason
    def initialize(reason, msg = nil)
      @reason = reason == :protected_branch_policy ? reason : :no_merge_method
      msg ||= @reason == :protected_branch_policy ?
        "Sorry, you need to allow either squash or rebase merge strategies, or both." :
        "Sorry, you need to allow at least one merge strategy."
      super(msg)
    end
  end

  # Public: Sets the type of merges that are allowed when using the pull request
  # merge button. For each merge method, a param of `true` indicates that
  # the method should be allowed, `false` that it should be blocked, and `nil`,
  # that it should keep its current value.
  def set_allowed_merge_types(user, merge_allowed: nil, squash_allowed: nil, rebase_allowed: nil, delete_branch_allowed: nil)
    GitHub::SchemaDomain.allowing_cross_domain_transactions do
      ApplicationRecord::Domain::ConfigurationEntries.transaction do
        transaction do
          merge_commit_currently_allowed = merge_commit_allowed?
          merge_allowed = merge_commit_currently_allowed if merge_allowed.nil?

          squash_commit_currently_allowed = squash_merge_allowed?
          squash_allowed = squash_commit_currently_allowed if squash_allowed.nil?

          rebase_commit_currently_allowed = rebase_merge_allowed?
          rebase_allowed = rebase_commit_currently_allowed if rebase_allowed.nil?

          any_linear_history = protected_branches.where.not(linear_history_requirement_enforcement_level: :off).any?

          if merge_allowed && !squash_allowed && !rebase_allowed && any_linear_history
            raise MergeMethodError.new(:protected_branch_policy)
          end

          if !merge_allowed && !squash_allowed && !rebase_allowed
            raise MergeMethodError.new(:no_merge_method)
          end

          if merge_allowed != merge_commit_currently_allowed
            GitHub.kv.set("repo.merge_commit_blocked.#{id}", (!merge_allowed).to_s)
            instrument :change_merge_setting, {
              actor: user,
              merge_type: "merge_commit",
              enabled: !!merge_allowed,
            }
          end

          if squash_allowed != squash_commit_currently_allowed
            GitHub.kv.set("repo.squash_merge_blocked.#{id}", (!squash_allowed).to_s)
            instrument :change_merge_setting, {
              actor: user,
              merge_type: "squash",
              enabled: !!squash_allowed,
            }
          end

          if rebase_allowed != rebase_commit_currently_allowed
            GitHub.kv.set("repo.rebase_merge_blocked.#{id}", (!rebase_allowed).to_s)
            instrument :change_merge_setting, {
              actor: user,
              merge_type: "rebase",
              enabled: !!rebase_allowed,
            }
          end

          permission = async_action_or_role_level_for(user).sync
          delete_branch_allowed = delete_branch_allowed && permission == :admin

          if delete_branch_allowed == false
            disallow_auto_deleting_branches(actor: user)
          elsif delete_branch_allowed == true
            allow_auto_deleting_branches(actor: user)
          end
        end
      end
    end
  end

  # Private: Pages operations like `gh_pages_url` require the Page to exist
  # This method returns the page, if it exists, or initializes a new page
  # for calculating URLs prior to the page existing (e.g., previews)
  def page_for_url
    page || Page.new(repository: self)
  end

  def made_private?
    saved_change_to_public? && private?
  end

  def private_or_deleted?
    made_private? || deleted?
  end

  def unpin_from_profiles
    profile_pins.repositories.destroy_all
  end

  def disable_interaction_limits
    return unless GitHub.interaction_limits_enabled?
    RepositoryInteractionAbility.disable_all_for(:repository, self)
  end

  def set_made_public_at
    self.made_public_at = public? ? Time.now : nil
  end

  # Queue a job to find all pages repos belonging to the same owner
  # and make sure they have valid https_redirect values.
  #
  # Returns nothing.
  def queue_propagate_https_redirect
    if GitHub.pages_https_redirect_enabled? && owner
      PagesPropagateHttpsRedirectJob.perform_later(owner.id)
    end
  end

  # Internal: Ensure this repo affects the owner's contributions.
  def clear_contributions_cache
    return if user.nil?
    Contribution.clear_caches_for_user(user)
  end

  # Public: Queue a job to recalculate the cached stars count
  # for each user that's starred this repository.
  def enqueue_calculate_user_stars_count
    CalculateUserStarsCountJob.perform_later(id)
  end

  def enqueue_set_license_job
    RepositorySetLicenseJob.perform_later(self)
  end

  def track_community_health_when_made_public
    return unless public?
    enqueue_community_health_check(true)
  end

  def community_health_factors_changed?
    saved_change_to_description? || saved_change_to_name?
  end

  # Public: Queue a job to verify the repository's community health metrics
  def enqueue_community_health_check(track_private_to_public = false)
    CommunityProfile.enqueue_health_check_job(self, track_private_to_public)
  end

  # Public: Can the specified actor view projects on this organization?
  #
  # actor - The User trying to view projects.
  #
  # Returns a boolean.
  def projects_readable_by?(actor)
    resources.repository_projects.readable_by?(actor)
  end

  # Public: Can the specified actor view projects on this organization?
  #
  # actor - The User trying to view projects.
  #
  # Returns Promise<bool>
  def async_projects_readable_by?(actor)
    resources.repository_projects.async_readable_by?(actor)
  end

  # Public: Can the specified actor create/edit projects on this repository?
  #
  # actor - The User trying to create/edit projects.
  #
  # Returns a boolean.
  def projects_writable_by?(actor)
    resources.repository_projects.writable_by?(actor)
  end

  # Public: Can the specified actor create/edit projects on this repository?
  #
  # actor - The User trying to create/edit projects.
  #
  # Returns Promise<bool>
  def async_projects_writable_by?(actor)
    resources.repository_projects.async_writable_by?(actor)
  end

  # Public: Can the specified actor administer projects on this repository?
  #
  # actor - The User trying to administer projects.
  #
  # Returns a boolean.
  def projects_adminable_by?(actor)
    # We intentionally use writable_by? here, since there's no concept of
    # admin on projects in the pre-Abilities permission system.
    projects_writable_by?(actor)
  end

  # Public: Can the specified actor administer projects on this repository?
  #
  # actor - The User trying to administer projects.
  #
  # Returns Promise<bool>
  def async_projects_adminable_by?(actor)
    # We intentionally use writable_by? here, since there's no concept of
    # admin on projects in the pre-Abilities permission system.
    async_projects_writable_by?(actor)
  end

  # Public: Has the specified user contributed to this repository?
  #
  # user - The User to check for contributions.
  #
  # Returns Promise<bool>
  def async_contributor?(user)
    return Promise.resolve(false) unless user

    Platform::Loaders::CommitContributorCheck.load(self, user.id)
  end

  # Public: Returns true unless locked_on_migration? or archived? are true.
  #
  # Returns a boolean
  def writable?
    !archived? && !locked_on_migration? && !access.disabled?
  end

  # Public: Determine the "best" merge base between two commits.
  #
  # Returns a String containing the best merge commit oid, or nil.
  def best_merge_base(base_sha, head_sha, timeout: nil)
    science "best_merge_base" do |e|
      e.context repository: name_with_owner, base_sha: base_sha, head_sha: head_sha

      e.use { rpc.best_merge_base(base_sha, head_sha, timeout: timeout) }
      e.try { rpc.rugged_best_merge_base(base_sha, head_sha) }

      e.ignore { private? }
    end
  end

  # Public: Query if a repository is archived (read-only).
  def archived?
    !maintained? || trade_controls_read_only?
  end

  # Public: Restrict write access to public repos owned by trade controls restricted
  # organizations
  #
  # Returns Boolean
  def trade_controls_read_only?(new_visibility: nil)
    (public? || new_visibility == PUBLIC_VISIBILITY)  && plan_owner_organization_is_fully_trade_restricted?
  end

  # Public: Is the plan owner an organization, and is that organization fully trade restricted
  #
  # Returns Boolean
  def plan_owner_organization_is_fully_trade_restricted?
    plan_owner&.organization? && plan_owner.has_full_trade_restrictions?
  end

  # Public: Mark a repository as archived so it is read-only.
  def set_archived
    return true if !maintained?
    return false unless update_attribute :maintained, false
    instrument :archived, actor_id: actor.id
    # Instrumentation to publish hydro event
    GlobalInstrumenter.instrument("repository.archived_status_changed", {
      repository_id: self.id,
      is_archived: true,
      actor_id: actor.id,
    })
    # Reindex issues, discussions, and PRs to update archived attributes
    Search.add_to_search_index("bulk_issues", self.id, "purge" => true)
    Search.add_to_search_index("bulk_discussions", self.id, "purge" => true)
    Search.add_to_search_index("bulk_pull_requests", self.id, "purge" => true)
    true
  end

  # Public: Mark a repository no longer archived so it is read-write again.
  def unset_archived
    return true if maintained?
    return false unless update_attribute :maintained, true
    instrument :unarchived, actor_id: actor.id
    # Instrumentation to publish hydro event
    GlobalInstrumenter.instrument("repository.archived_status_changed", {
      repository_id: self.id,
      is_archived: false,
      actor_id: actor.id,
    })
    # Start manifest redetection process for dependency graph
    RepositoryDependencyManifestInitializationJob.perform_later(self.id) if self.dependency_graph_enabled?
    # Reindex issues, discussions, and PRs to update archived attributes
    Search.add_to_search_index("bulk_issues", self.id, "purge" => true)
    Search.add_to_search_index("bulk_discussions", self.id, "purge" => true)
    Search.add_to_search_index("bulk_pull_requests", self.id, "purge" => true)
    true
  end

  # Sign a commit body.
  #
  # Returns a String signature or nil.
  def sign_commit(commit_body)
    if GitHub.web_commit_signing_enabled?
      GitHub.gpg.sign(commit_body)
    else
      nil
    end
  rescue GpgVerify::Unavailable
    nil
  rescue GpgVerify::Error => e
    Failbot.report(e, app: GitSigning::HAYSTACK_BUCKET)
    nil
  end

  # Returns a temporary clone url with signed auth token embedded
  #
  # user    - User instance to use for token generation
  # expires - How long the temporary token lasts before expiring
  # action  - repo action token will be valid for (read or write)
  #
  # Returns signed auth token string
  def temp_clone_token(user, expires: nil)
    return if user.nil?
    return "" unless private?

    expires ||= 5.minutes.from_now

    signed_auth_token_options = {
      scope: temp_clone_token_scope,
      expires: expires,
    }
    user.signed_auth_token(signed_auth_token_options)
  end

  # The clone token scope is defined in duplicate here
  # and in the GitAuth code.
  private def temp_clone_token_scope
    "TemporaryCloneURL:#{id}:read"
  end

  # Is the token scanning feature enabled on this repository?
  #
  # This method does not include checks for if token scanning has been
  # disabled on this repository, `scan_for_tokens?` should be called for the
  # full checks instead.
  def token_scanning_feature_enabled?
    return false unless GitHub.configuration_supports_token_scanning?
    return true if process_found_tokens_externally?
    return true if token_scanning_private_feature_flag_enabled?
    false
  end

  def token_scanning_private_feature_flag_enabled?
    GitHub.flipper[:token_scanning_private].enabled?(self) || GitHub.flipper[:token_scanning_private].enabled?(owner)
  end

  # Should changes to this repo be scanned for GitHub OAuth tokens and third
  # party credentials?
  #
  # Returns boolean.
  def scan_for_tokens?
    return false unless token_scanning_feature_enabled?
    return false if GitHub.enterprise?
    return false if token_scanning_service_enabled?
    !token_scanning_staff_disabled?
  end

  # Public: Is there an active email Hook for this repository?
  #
  # Returns: Boolean
  def push_notifications_active?
    email_hooks.active.exists?
  end

  # Public: Determines whether this repository advertises the tips of its
  # parent.
  #
  # True unless a direct parent of the receiving repository explicitly does not
  # advertise its alternates, as in by being enrolled in the
  # ':conceal_alternate_tips' feature flag.
  #
  # Returns: Boolean
  def advertise_alternate_tips?
    fork? && !GitHub.flipper[:conceal_alternate_tips].enabled?(parent)
  end

  # Public: Determines whether this repository runs its GitRPC commands with
  # trace2 instrumentation.
  #
  # Returns: Boolean
  def trace2_enabled?
    !GitHub.enterprise?
  end

  # Public: Determines whether this repository generates changed-path Bloom
  # filters in its commit graphs.
  #
  # Returns: Boolean
  def changed_path_bloom_filters_enabled?
    actor = fork? ? network.root : self
    GitHub.flipper[:changed_path_bloom_filters].enabled?(actor)
  end

  # Public: Determines whether this repository uses an experimental
  # implementation of 'git blame-tree' which is often faster.
  #
  # Returns: Boolean
  def blame_tree_fast_enabled?
    GitHub.flipper[:blame_tree_fast].enabled?(self)
  end

  # Public: Returns the number of bytes to limit in-window comparisons for
  # finding delta/base candidate pairs when repacking.
  #
  # Returns: Integer.
  def repack_window_byte_limit
    1048576
  end

  def licensing_enabled?
    private? && active? && has_business_owner?
  end

  def should_remove_for_inacessible_parent?
    private? && owner.user? && parent&.private? && !parent.pullable_by?(owner)
  end

  # Public: Determines whether if either the owner or network owner is trade restricted
  #
  # Is the owner, or the network owner trade restricted?  Network owner is the root
  # owner of a fork repository.
  #
  # Returns: Boolean
  def trade_restricted_by_owner?
    if network_id && network_owner
      return true unless network_owner.restriction_tier_allows_feature?(type: :repository)
    end

    if owner
      !owner.restriction_tier_allows_feature?(type: :repository)
    end
  end

  # Public: Determines if functionality is limited in this repo due to trade
  # controls restrictions
  #
  # Is this a private repo and the owner or network owner restricted, or
  # is it public and owned by a fully restricted organization
  #
  # Returns: Boolean
  def trade_restricted?
    (private? && trade_restricted_by_owner?) || trade_controls_read_only?
  end

  # Public: Determine which notification to show to API users.
  #
  # Returns: String
  def trade_restriction_api_error_message(viewer = nil)
    if owner.is_a?(Organization) && owner.has_any_trade_restrictions?
      if owner.adminable_by?(viewer)
        ::TradeControls::Notices::Plaintext.org_restricted_repo_for_admins
      elsif owner.direct_or_team_member?(viewer)
        ::TradeControls::Notices::Plaintext.org_restricted_repo_for_collaborators
      else
        ::TradeControls::Notices::Plaintext.org_restricted
      end
    elsif viewer == owner || owner.is_a?(Organization)
      ::TradeControls::Notices::Plaintext.user_account_restricted
    else
      "Please contact #{owner.login} to resolve the issue."
    end
  end

  # Public: Determines if a commit with the given object ID is reachable in
  # this repository (i.e. in a branch or tag, not a fork).
  #
  # This has some statting and logging that's a bit ugly due to the need to
  # carefully instrument this.
  #
  # Returns: Boolean
  def is_commit_in_branch_or_tag?(oid)
    timed_out = false
    found = false

    begin
      start = Process.clock_gettime(Process::CLOCK_MONOTONIC)
      commit = commits.find(oid, check_reachability: true)
      found = !!commit
    rescue GitRPC::ObjectMissing
      found = false
    rescue GitRPC::Timeout
      timed_out = true
      found = false
    ensure
      finish = Process.clock_gettime(Process::CLOCK_MONOTONIC)
      elapsed_ms = ((finish - start) * 1000).round
      GitHub.dogstats.timing "repository.commit_is_in_branch_or_tag", elapsed_ms, tags: ["found:#{found}", "timed_out:#{timed_out}"]

      GitHub::Logger.log(
        fn: "Repository#is_commit_in_branch_or_tag?",
        oid: oid,
        timed_out: timed_out,
        found: found,
        repository: name_with_owner,
        elapsed_ms: elapsed_ms,
      )
    end
  end

  # Public: Does this repo have a codeowners file?
  #
  # Returns a boolean
  def codeowners?
    return @codeowners if defined? @codeowners
    @codeowners = !!preferred_file(:codeowners)
  end

  def memex_suggestion_hash(last_interaction_at: nil)
    memex_column_hash.merge(
      last_interaction_at: last_interaction_at&.utc&.iso8601,
      pushed_at: pushed_at&.utc&.iso8601,
    )
  end

  def memex_column_hash
    {
      id: id,
      is_forked: fork?,
      is_public: public?,
      name: name,
      url: permalink,
    }
  end

  private

  def ensure_owner_is_not_trade_controls_restricted
    if trade_restricted?
      errors.add(:trade_controls_restricted_owner, "can't create repositories.")
      return false
    end

    true
  end

  def ensure_creator_is_not_trade_controls_restricted
    return true if !(private? && created_by&.has_any_trade_restrictions?)

    errors.add(:trade_controls_restricted_creator, "can't create repositories.")
    false
  end

  def ensure_owner_is_a_user_or_organization
    errors.add(:owner, "must be a User or Organization") unless owner_valid?
  end

  def owner_valid?
    if GitHub.flipper[:no_owner_repo_check].enabled?
      return true if ALLOWED_OWNER_TYPES.include?(owner.class.name)
      owner.nil? && !active?
    else
      ALLOWED_OWNER_TYPES.include?(owner.class.name)
    end
  end

  def snapshot_license_state
    return unless saved_changes.has_key?(:public) || saved_changes.has_key?(:active)
    Billing::SnapshotLicensesJob.perform_later(owner.business)
  end

  # Private: Reinstalls Apps that need to be transferred over to the new_owner
  def reinstall_integrations(new_owner:, integrations:)
    integrations.each do |integration|
      result = if (current_installation = integration.installations.with_target(new_owner).first)
        IntegrationInstallation::Editor.append(current_installation, repositories: [self], editor: new_owner)
      else
        installer = new_owner.organization? ? new_owner.admins.first : new_owner
        integration.install_on(
          new_owner, repositories: [self], installer: installer, reinstalling_during_repository_transfer: true
        )
      end
      status = result.success? ? "succeeded" : "failed"
      GitHub.dogstats.increment("repository.reinstall_integration", tags: ["result:#{status}"])
    end
  end

  def third_party_ci_integration_installations
    IntegrationInstallation
      .third_party
      .with_resources_on(subject: self, resources: "checks", min_action: :write)
  end

  def has_business_owner?
    !GitHub.single_business_environment? && owner&.organization? && owner.business.present?
  end

  def set_owner_login
    self.owner_login = owner&.login
  end

  # Internal: Is the permission greater or equal in rank than the target permission.
  # Both inputs can be an Ability (e.g. read), a Role (e.g. triage) or a custom role.
  #
  # - permission: a String or Symbol.
  # - target: a String or Symbol.
  #
  # Returns a Boolean.
  def permission_greater_than_target?(permission, target:)
    return false if permission.nil?

    perm = permission.to_sym
    target = target.to_sym
    return true if perm == target

    permission =
      if Role.valid_system_role?(perm)
        Role.find_by(name: perm)
      else
        Role.for_org(name: perm, org: organization)
      end

    permission.target_greater_than_other_role?(other_role: target)
  end
end
