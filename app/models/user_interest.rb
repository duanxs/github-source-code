# frozen_string_literal: true

class UserInterest < ApplicationRecord::Domain::Users
  belongs_to :user

  validates_presence_of :user, :interest
end
