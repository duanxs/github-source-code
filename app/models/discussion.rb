# frozen_string_literal: true

class Discussion < ApplicationRecord::Domain::Discussions
  include Spam::Spammable
  extend GitHub::BackgroundDependentDeletes
  include GitHub::Validations
  include GitHub::RateLimitedCreation
  include UserContentEditable
  include GitHub::UTF8
  include Blockable
  include AbuseReportable
  include Referenceable
  include InteractionBanValidation
  include GitHub::UserContent
  include NotificationsContent::WithCallbacks
  include GitHub::Relay::GlobalIdentification
  include Discussion::NewsiesAdapter
  include Reaction::Subject::RepositoryContext
  include Discussion::SearchAdapter
  include Instrumentation::Model
  include Discussion::TransferAdapter
  include Discussion::HovercardDependency

  include Permissions::Attributes::Wrapper
  self.permissions_wrapper_class = Permissions::Attributes::Discussion

  LOCK_REASONS = ["off-topic", "too heated", "resolved", "spam"].freeze
  DISCUSSION_TYPE_SHORT_DESCRIPTION_MAPPINGS = {
    answered: "Answered question",
    conversation: "Conversation",
    default: "Discussion",
    unanswered: "Unanswered question",
  }

  # The roles additionally permitted to moderate content for discussions
  PERMITTED_MODERATOR_ROLES = [:triage, :maintain, :write].freeze

  # Text used for onboarding new discussion users
  WELCOME_BODY = <<-WELCOME
<!--
    ✏️ Optional: Customize the content below to let your community know what you intend to use Discussions for.
-->
## 👋 Welcome!
  We’re using Discussions as a place to connect with other members of our community. We hope that you:
  * Ask questions you’re wondering about.
  * Share ideas.
  * Engage with other community members.
  * Welcome others and are open-minded. Remember that this is a community we
  build together 💪.

  To get started, comment below with an introduction of yourself and tell us about what you do with this community.

<!--
  For the maintainers, here are some tips 💡 for getting started with Discussions. We'll leave these in Markdown comments for now, but feel free to take out the comments for all maintainers to see.

  📢 **Announce to your community** that Discussions is available! Go ahead and send that tweet, post, or link it from the website to drive traffic here.

  🔗 If you use issue templates, **link any relevant issue templates** such as questions and community conversations to Discussions. Declutter your issues by driving community content to where they belong in Discussions. If you need help, here's a [link to the documentation](#{GitHub.help_url}/en/github/building-a-strong-community/configuring-issue-templates-for-your-repository#configuring-the-template-chooser).

  ➡️ You can **convert issues to discussions** either individually or bulk by labels. Looking at you, issues labeled “question” or “discussion”.
-->
  WELCOME

  extend GitHub::Encoding
  force_utf8_encoding :title, :body

  belongs_to :repository, required: true
  belongs_to :user
  belongs_to :chosen_comment, class_name: "DiscussionComment"
  belongs_to :issue

  belongs_to :category, foreign_key: :discussion_category_id, class_name: "DiscussionCategory"
  alias_attribute :category_id, :discussion_category_id

  # Indicates the current state of the Discussion with respect to whether it's being converted
  # from an Issue or not, if conversation has been locked, or if it's being transferred
  # to another Repository.
  enum state: {
    open: 0,
    converting: 1,
    error: 2,
    locked: 3,
    transferring: 4,
  }

  # Indicates what went wrong if this Discussion was originally an Issue and the conversion
  # process failed, or if transferring this Discussion to another Repository failed.
  enum error_reason: {
    no_error: 0,
    comment_copy_failure: 1,
    close_failure: 2,
    reset_conversion_failure: 3,
  }

  # Indicates the kind of discussion this is and what its functionality should be, such as
  # whether an answer is expected.
  enum discussion_type: {
    # Represents a question that the author would like answered
    question: 0,
    # Represents an open-ended conversation, not looking for any particular answer
    conversation: 1,
  }

  has_many :comments, -> { order("discussion_comments.created_at ASC") },
    class_name: "DiscussionComment"
  destroy_dependents_in_background :comments

  has_many :commenters, through: :comments, source: :user, split: true

  has_many :events, class_name: "DiscussionEvent"
  destroy_dependents_in_background :events

  has_many :reactions, class_name: "DiscussionReaction"
  destroy_dependents_in_background :reactions

  has_one :spotlight, class_name: "DiscussionSpotlight", dependent: :destroy

  has_one :last_transfer, class_name: "DiscussionTransfer", foreign_key: :new_discussion_id

  setup_spammable(:user)
  setup_attachments

  # Use VARBINARY limit from the database
  TITLE_BYTESIZE_LIMIT = 1024

  before_validation :set_number!, on: :create
  before_validation :set_discussion_type, on: :update, if: :chosen_comment_id_changed?
  before_validation :strip_title

  validates :title, :number, presence: true
  validate :user_exists_when_open_state, on: :create
  validate :chosen_comment_matches_discussion
  validate :discussion_is_question_if_chosen_comment_set
  validate :chosen_comment_is_not_nested
  validate :ensure_author_is_not_blocked, on: :create
  validates :body, bytesize: { maximum: MYSQL_UNICODE_BLOB_LIMIT },
    unicode: true, allow_blank: true, allow_nil: true
  validates :title, bytesize: { maximum: TITLE_BYTESIZE_LIMIT }, unicode: true
  validate :user_can_interact?, on: :create
  validate :issue_present_if_converting
  validate :error_reason_present_if_error
  validate :user_has_verified_email, on: :create

  # Live updates
  after_commit :notify_socket_subscribers, on: :update

  # Notifications
  after_commit :subscribe_and_notify, on: :create
  after_commit :update_subscriptions_and_notify, on: :update
  after_destroy :destroy_notification_summary

  # Used to temporarily hold the current user, for use in logging to Hydro
  attr_accessor :actor

  # Hydro telemetry and audit logs
  after_commit :instrument_creation_event, on: :create
  after_commit :instrument_update_event, on: :update, if: :title_or_body_changed_after_commit?
  after_destroy_commit :instrument_deletion_event

  # audit log for updates
  after_commit :audit_log_update_event, on: :update, if: :title_or_body_changed_after_commit?

  # Search
  after_commit :synchronize_search_index

  scope :for_repository, ->(repo) { where(repository_id: repo) }
  scope :for_organization, ->(org) do
    repo_ids = Repository.where(organization_id: org).pluck(:id)
    for_repository(repo_ids)
  end
  scope :with_number, ->(number) { where(number: number) }
  scope :converted_from_issue, ->(issue) { where(issue_id: issue) }
  scope :converted_from_issue_numbered, ->(number) do
    where(number: number).where.not(issue_id: nil)
  end
  scope :authored_by, ->(user) { where(user_id: user) }
  scope :answered, -> { where.not(chosen_comment_id: nil) }
  scope :unanswered, -> { where(chosen_comment_id: nil) }
  scope :newest_first, -> { order("discussions.created_at DESC") }
  scope :oldest_first, -> { order("discussions.created_at ASC") }
  scope :recently_updated_first, -> { order("discussions.updated_at DESC") }
  scope :least_recently_updated_first, -> { order("discussions.updated_at ASC") }
  scope :most_commented_first, -> { order("discussions.comment_count DESC") }
  scope :least_commented_first, -> { order("discussions.comment_count ASC") }
  scope :highest_score_first, -> { order("discussions.score DESC") }
  scope :lowest_score_first, -> { order("discussions.score ASC") }
  scope :updated_since, ->(time) { where("discussions.updated_at > ?", time) }

  scope :commented_on_by, ->(user) do
    discussion_ids = DiscussionComment.for_user(user).select(:discussion_id).distinct
    where(id: discussion_ids)
  end

  scope :authored_by_and_visible_to, ->(user) do
    visible_repo_ids = user.associated_repository_ids
    authored_discussion_repo_ids = authored_by(user).distinct.pluck(:repository_id)
    public_repo_ids = Repository.where(id: authored_discussion_repo_ids).public_scope.
      pluck(:id)
    authored_by(user).for_repository(visible_repo_ids | public_repo_ids)
  end

  scope :commented_on_and_visible_to, ->(user) do
    visible_repo_ids = user.associated_repository_ids
    commented_discussion_repo_ids = commented_on_by(user).distinct.pluck(:repository_id)
    public_repo_ids = Repository.where(id: commented_discussion_repo_ids).public_scope.
      pluck(:id)
    commented_on_by(user).for_repository(visible_repo_ids | public_repo_ids)
  end

  scope :sorted_by, ->(filter) do
    if filter == "oldest"
      oldest_first
    elsif filter == "most_commented"
      most_commented_first
    elsif filter == "least_commented"
      least_commented_first
    elsif filter == "highest_score"
      highest_score_first
    elsif filter == "lowest_score"
      lowest_score_first
    elsif filter == "newest"
      newest_first
    elsif filter == "least_recently_updated"
      least_recently_updated_first
    else
      recently_updated_first
    end
  end

  scope :filter_by_type, ->(type) do
    case type
    when "unanswered"
      question.unanswered
    when "answered"
      question.answered
    when "conversation"
      conversation
    else
      all
    end
  end

  SUGGESTION_LIMIT = 1000

  scope :suggestions, -> do
    select("id AS id, number, title, updated_at").
      order("updated_at desc").
      limit(SUGGESTION_LIMIT)
  end

  # Public: Get a subset of discussions that do not include those in repositories owned
  # by the specified list of organizations.
  #
  # discussions - a list of Discussion records
  # org_ids - a list of Organization IDs whose discussions should be omitted
  #
  # Returns an Array of Discussions.
  def self.filter_out_org_discussions(discussions:, org_ids:)
    discussions.reject do |discussion|
      org_ids.include?(discussion.organization_id)
    end
  end

  def self.discussion_type_allows_chosen_comment?(discussion_type)
    discussion_type.to_s.downcase.to_sym == :question
  end

  def unmark_answer_if_set(actor)
    # If it's not answered, don't need to unmark anything
    return true unless answered?

    # Set the actor so we can log a Hydro event about the comment being updated
    chosen_comment.actor = actor

    chosen_comment.unmark_as_answer
  end

  # Public: Is this discussion publicly visible?
  def public?
    repository&.public?
  end

  # Public: The ID of the organization that owns the repository this discussion belongs to,
  # if any.
  def organization_id
    repository&.organization_id
  end

  # Public: Returns the users who have authored a discussion in the given repository.
  #
  # repo - a Repository or its ID
  #
  # Returns a User ActiveRecord::Relation.
  def self.authors_in_repo(repo)
    user_ids = for_repository(repo).pluck(:user_id)
    User.where(id: user_ids).order(:login)
  end

  # Public: Returns a new, unsaved Discussion with content taken from the given Issue.
  def self.from_issue(issue)
    Discussion.new(title: issue.title, body: issue.body, repository: issue.repository,
                   user: issue.user, user_hidden: issue.user_hidden,
                   comment_count: issue.issue_comments_count, created_at: issue.created_at,
                   updated_at: issue.updated_at, number: issue.number, issue_id: issue.id,
                   state: :converting)
  end

  # Public: Has this discussion been given a spotlight position in the repository?
  def spotlit?
    spotlight.present?
  end

  def author
    user || User.ghost
  end

  def authored_by_ghost?
    author.ghost?
  end

  # Internal: For suggestions_url helper
  def suggestion_id
    id
  end

  # Public: Users who should be considered discussion participants.
  #
  # viewer - the User who is viewing the participants (current_user)
  # optimize_repo_access_checks - if optimize_repo_access_checks is set
  #                               to true, do not perform access checks
  #                               on repos that are owned by org's with
  #                               a lot of members
  #
  # Returns an Array of Users.
  def participants_for(viewer, optimize_repo_access_checks: false)
    participants(optimize_repo_access_checks: optimize_repo_access_checks).reject do |u|
      u.hide_from_user?(viewer)
    end
  end

  # Public: Find all Users that have commented on this discussion.
  # Exclude Bots and users who no longer have access to this repo
  # (unless parent org has so many members that query may timeout).
  # Exclude Organizations from results as participants should be Users, but
  # Users can transform into Organizations.
  #
  # Note that the returned users are not filtered for spam.
  #
  # optimize_repo_access_checks - by default, #user_ids_to_hide_from_mentions will
  #                               check to make sure that each user returned still
  #                               has access to the repo. If optimize_repo_access_checks
  #                               is set to true, that check will not happen if the
  #                               parent org has too many members (which may
  #                               cause the access checks to time out)
  # prefill_profiles - should the Profile relation on each User be preloaded
  #
  # Returns an Array of Users.
  def participants(optimize_repo_access_checks: false, prefill_profiles: true)
    @participants ||= self.class.participants_for([self],
      optimize_repo_access_checks: optimize_repo_access_checks, prefill_profiles: prefill_profiles)
  end

  # Public: Returns an Array of Users with their Profiles loaded for all the participants
  # in the given list of Discussions.
  def self.participants_for(discussions, optimize_repo_access_checks: false, prefill_profiles: true)
    user_ids = discussions.map(&:user_id) +
      DiscussionComment.for_discussion(discussions.map(&:id)).
      select(:user_id).distinct.
      map(&:user_id)

    users = User.where(id: user_ids).to_a
    users.reject! { |user| user.is_a?(Bot) || user.organization? }
    GitHub::PrefillAssociations.for_profiles(users) if prefill_profiles
    users.sort_by { |user| user_ids.index(user.id) }
  end

  # Public: Get user IDs who participated in the specified discussions.
  #
  # discussions - a list of Discussions
  # viewer - the current User or nil
  #
  # Returns a hash of discussion ID => user IDs for all the participants in the
  # specified discussions.
  def self.participant_ids_by_discussion_id(discussions, viewer:)
    discussion_comments = DiscussionComment.for_discussion(discussions.map(&:id)).
      select(:discussion_id, :user_id).distinct.filter_spam_for(viewer)

    commenter_ids_by_discussion_id = {}
    discussion_comments.each do |comment|
      commenter_ids_by_discussion_id[comment.discussion_id] ||= []
      commenter_ids_by_discussion_id[comment.discussion_id] << comment.user_id
    end

    results = Hash.new([])
    discussions.each do |discussion|
      commenter_ids = commenter_ids_by_discussion_id[discussion.id] || []
      results[discussion.id] = ([discussion.user_id] + commenter_ids).uniq
    end
    results
  end

  # Public: Get users who participated in the given discussions.
  #
  # discussions - a list of Discussions
  # viewer - the current User or nil
  #
  # Returns a Hash of Discussion ID => Array of Users.
  def self.participants_by_discussion_id(discussions, viewer:)
    user_ids_by_discussion_id = participant_ids_by_discussion_id(discussions, viewer: viewer)
    users_by_id = participants_for(discussions).index_by(&:id)
    results = Hash.new([])
    user_ids_by_discussion_id.each do |discussion_id, user_ids|
      results[discussion_id] = users_by_id.slice(*user_ids).values
    end
    results
  end

  def to_param
    number.to_s
  end

  # Public: Can the given actor delete the discussion?
  def deletable_by?(actor)
    # Ensure the feature flag is enabled
    return false unless repository&.discussions_enabled?

    response = ::Permissions::Enforcer.authorize(
      action: :delete_discussion,
      actor: actor,
      subject: self,
    )
    response.allow?
  end

  # Public: Can the given actor change the title and body of this discussion?
  def modifiable_by?(actor)
    # Ensure the feature flag is enabled
    return false unless repository&.discussions_enabled?

    response = ::Permissions::Enforcer.authorize(
      action: :edit_discussion,
      actor: actor,
      subject: self,
    )
    response.allow?
  end

  # Public: Returns the ID of this Discussion.
  def discussion_id
    id
  end

  # Public: Returns nil because this is a Discussion not a DiscussionComment.
  def discussion_comment_id
    nil
  end

  # Public: Returns true if this discussion is taking place in an organization-owned repository.
  def in_organization?
    repository_owner.is_a?(Organization)
  end

  def archived_repository?
    repository&.archived?
  end

  def title_or_body_changed_after_commit?
    previous_changes.key?(:title) || previous_changes.key?(:body)
  end

  def filter_spam_comments_for(actor)
    comments.preload(:user).filter_spam_for(actor)
  end

  def filter_spam_events_for(actor)
    filtered_events = []
    ordered_events = events.includes(:actor, comment: :user,
      discussion_transfer: :old_repository).order(:id)
    promises = ordered_events.map do |event|
      event.async_readable_by?(actor)
    end
    readable_checks = Promise.all(promises).sync
    ordered_events.each_with_index do |event, i|
      filtered_events << event if readable_checks[i]
    end
    filtered_events
  end

  # Public: Returns items to show in the timeline for this discussion.
  #
  # actor - the User that is viewing the timeline (current_user)
  #
  # Returns an Array that could include DiscussionComments, DiscussionEvents,
  # and DiscussionEventGroups.
  def timeline_items_for(actor)
    # user relation used for comment.author in app/views/discussions/_comment.html.erb
    # discussion relation used for comment.answer? in app/views/discussions/_comment.html.erb
    comments = filter_spam_comments_for(actor).includes(:user, :discussion)
    events_and_event_groups = unsorted_filtered_and_grouped_events_for(actor)
    timeline_items = comments + events_and_event_groups
    timeline_items.sort_by(&:created_at)
  end

  # Public: Can this user create a new discussion comment?
  #
  # actor - a User
  #
  # Returns a Boolean.
  def can_comment?(actor)
    return false unless repository.discussions_enabled?

    response = ::Permissions::Enforcer.authorize(
      action: :create_discussion_comment,
      actor: actor,
      subject: self
    )

    response.allow?
  end

  def viewer_can_update?(viewer)
    return false unless repository
    viewer_cannot_update_reasons(viewer).empty?
  end

  def async_viewer_can_update?(viewer)
    async_repository.then do |repo|
      next false unless repo
      viewer_cannot_update_reasons(viewer).empty?
    end
  end

  def viewer_cannot_update_reasons(viewer)
    return [:login_required] unless viewer
    context = { repo: repository, discussion: self }
    errors = ContentAuthorizer.authorize(viewer, :Discussion, :edit, context).errors.
      map(&:symbolic_error_code)
    errors << :insufficient_access unless modifiable_by?(viewer)
    errors
  end

  def viewer_can_moderate_content?(viewer)
    return false unless repository

    PERMITTED_MODERATOR_ROLES.include?(repository.direct_role_for(viewer))
  end

  def readable_by?(actor)
    async_readable_by?(actor).sync
  end

  def async_readable_by?(actor)
    async_repository.then do |repository|
      next false unless repository&.discussions_enabled?
      next true if repository&.public?

      repository.async_owner.then do
        Platform::Loaders::Permissions::BatchAuthorize.load(
          action: :read_discussion,
          actor: actor,
          subject: self,
          context: { considers_anonymous: true },
        ).then do |decision|
          decision.allow?
        end
      end
    end
  end

  def update_comment_count
    update_attribute(:comment_count, comments.not_spammy.count)
  end

  def answered?
    chosen_comment.present?
  end

  def unanswered?
    !answered?
  end

  # Public: Determine if this discussion is in a locked state for most users.
  def locked_for_general_public?
    return true unless repository&.writable?
    locked?
  end

  def latest_comment_for(viewer)
    result = DiscussionComment.latest_by_discussion_id([id], repository: repository, viewer: viewer)
    result[id]
  end

  # Public: Determine if this discussion is locked for the given user.
  def locked_for?(viewer)
    return false unless locked_for_general_public?
    return true unless viewer
    !repository.writable_by?(viewer)
  end

  def async_locked_for?(viewer)
    async_repository.then do
      locked_for?(viewer)
    end
  end

  # Public: Determine if this discussion can be locked by the given user.
  def lockable_by?(actor)
    # Already locked, can't lock it any harder
    return false if locked?

    # Ensure the feature flag is enabled
    return false unless repository&.discussions_enabled?

    response = ::Permissions::Enforcer.authorize(
      action: :toggle_discussion_lock,
      actor: actor,
      subject: self,
    )
    response.allow?
  end

  # Public: Determine if this discussion can be unlocked by the given user.
  def unlockable_by?(actor)
    # Can't unlock it if it's not locked
    return false unless locked?

    # Ensure the feature flag is enabled
    return false unless repository&.discussions_enabled?

    response = ::Permissions::Enforcer.authorize(
      action: :toggle_discussion_lock,
      actor: actor,
      subject: self,
    )
    response.allow?
  end

  def lock(actor)
    transaction do
      locked!
      events.create(actor: actor, event_type: :locked)
    end

    notify_socket_subscribers
    synchronize_search_index
  end

  def unlock(actor)
    transaction do
      open!
      events.create(actor: actor, event_type: :unlocked)
    end

    notify_socket_subscribers
    synchronize_search_index
  end

  # Internal: Notify subscribers that the discussion has been updated.
  #
  # Returns Set of channel id Strings that were notified.
  def notify_socket_subscribers
    # If we're being deleted, no reason to notify anyone
    return unless repository

    channel = GitHub::WebSocket::Channels.discussion(self)
    GitHub::WebSocket.notify_discussion_channel(self, channel,
      timestamp: Time.now.to_i,
      wait: default_live_updates_wait,
      reason: "discussion ##{id} updated",
      # This is a load-bearing gid: https://github.com/github/github/pull/146273/files#r438329053
      gid: global_relay_id,
    )
  end

  def converted_from_issue?
    issue_id.present?
  end

  def repository_owner
    repository&.owner
  end

  def reaction_groups
    async_reaction_groups.sync
  end

  def async_reaction_groups
    Platform::Loaders::DiscussionReactionGroups.load(self)
  end

  def short_description
    if conversation?
      DISCUSSION_TYPE_SHORT_DESCRIPTION_MAPPINGS[:conversation]
    elsif answered?
      DISCUSSION_TYPE_SHORT_DESCRIPTION_MAPPINGS[:answered]
    elsif unanswered?
      DISCUSSION_TYPE_SHORT_DESCRIPTION_MAPPINGS[:unanswered]
    else
      DISCUSSION_TYPE_SHORT_DESCRIPTION_MAPPINGS[:default]
    end
  end

  def react(actor:, content:)
    DiscussionReaction.react(user: actor, discussion_id: id, content: content)
  end

  def unreact(actor:, content:)
    DiscussionReaction.unreact(user: actor, discussion_id: id, content: content)
  end

  # Public: Returns false because Discussions can't be created via email
  def created_via_email?
    false
  end

  # Public: Returns the User who marked the answer, or nil if unknown/not answered.
  def chosen_comment_selected_by_user
    return unless answered?
    event = events.answer_marked.for_comment(chosen_comment_id).newest_first.first
    event&.actor
  end

  # Public: Provides a method needed when referencing a discussion from a
  # comment
  # Related issue: https://github.com/github/discussions/issues/503
  def reference_from_commit(user, commit_id, repository = nil)
  end

  private

  # Private: Returns an Array of DiscussionEventGroup and DiscussionEvent records.
  def unsorted_filtered_and_grouped_events_for(actor)
    events = filter_spam_events_for(actor)
    event_grouper = DiscussionEventGrouper.new(events)
    groups = event_grouper.group_events
    remaining_events = event_grouper.ungrouped_events
    groups + remaining_events
  end

  def user_has_verified_email
    return unless user

    # If we're converting an existing issue to a discussion, don't require the issue to have
    # been created by a user with a verified email address.
    return unless open?

    if user.should_verify_email?
      errors.add(:user, "must have a verified email address")
    end
  end

  # Private: Set the plan number to the next sequence for this listing
  def set_number!
    return unless repository
    Sequence.create(repository) unless Sequence.exists?(repository)
    self.number ||= Sequence.next(repository)
  end

  def set_discussion_type
    if chosen_comment_id
      self.discussion_type = :question
    end
  end

  def strip_title
    self.title = title&.strip
  end

  def ensure_author_is_not_blocked
    return unless user && repository

    # Allow transferring a discussion to another repository even if the author
    # is blocked by the new repo owner
    return if transferring?

    if user.blocked_by?(repository_owner)
      errors.add :user, "cannot post at this time"
    end
  end

  def discussion_is_question_if_chosen_comment_set
    return unless answered?

    unless question?
      errors.add(:chosen_comment, "cannot be specified for a non-question discussion")
    end
  end

  def user_exists_when_open_state
    if open? && user.nil?
      errors.add(:user, "can't be blank")
    end
  end

  def chosen_comment_matches_discussion
    return if chosen_comment.nil? || new_record?

    unless chosen_comment.discussion_id == id
      errors.add(:chosen_comment, "is not for this discussion")
    end
  end

  def issue_present_if_converting
    return unless converting?

    unless issue
      errors.add(:issue, "must be specified if state = converting")
    end
  end

  def error_reason_present_if_error
    return unless error?

    if error_reason.nil? || no_error?
      errors.add(:error_reason, "must be specified when state = error")
    end
  end

  def chosen_comment_is_not_nested
    return unless chosen_comment

    unless chosen_comment.top_level_comment?
      errors.add(:chosen_comment, "cannot be a reply to another comment")
    end
  end

  def instrument_creation_event
    GlobalInstrumenter.instrument "discussion.create", discussion: self
  end

  def instrument_update_event
    GlobalInstrumenter.instrument "discussion.update", discussion: self,
      actor: actor
  end

  def audit_log_update_event
    old_title, _ = previous_changes[:title]
    old_body, _ = previous_changes[:body]

    payload = {}
    payload[:old_title] = old_title if old_title
    payload[:old_body] = old_body if old_body

    instrument :update, payload
  end

  def instrument_deletion_event
    instrument :destroy

    GlobalInstrumenter.instrument "discussion.delete", discussion: self,
      actor: actor
  end

  def event_prefix() :discussion end
  def event_payload
    payload = {
      event_prefix   => self,
      :title         => title,
      :body          => body,
      :user          => user,
      :number        => number,
      :score         => score,
      :comment_count => comment_count,
      :issue_id      => issue_id,
      :converted_at  => converted_at,
    }

    if repository
      payload[repository.event_prefix] = repository

      if org = repository.organization
        payload[org.event_prefix] = org
      end
    end

    if category
      payload[category.event_prefix] = category
    end

    payload
  end
end
