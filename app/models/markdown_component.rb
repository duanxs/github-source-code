# frozen_string_literal: true

class MarkdownComponent < ApplicationRecord::Collab
  areas_of_responsibility :ce_extensibility

  include GitHub::UserContent

  # A container can be an composable_comment for now,
  # other types in the future
  belongs_to :container, polymorphic: true

  validates_presence_of :container
  validates_presence_of :body
  validates_presence_of :container_order

  validates :body, bytesize: { maximum: MYSQL_UNICODE_BLOB_LIMIT }, unicode: true
end
