# frozen_string_literal: true

module OrganizationDiscussionItem
  extend ActiveSupport::Concern

  include GitHub::Relay::GlobalIdentification
  include GitHub::UserContent
  include UserContentEditable

  included do
    extend GitHub::BackgroundDependentDeletes
    extend GitHub::Encoding

    force_utf8_encoding :body

    validate :body_is_present
  end

  def viewer_can_update?(viewer)
    async_viewer_can_update?(viewer).sync
  end

  def async_viewer_can_update?(viewer)
    async_viewer_cannot_update_reasons(viewer).then(&:empty?)
  end

  def async_viewer_cannot_update_reasons(viewer)
    return Promise.resolve([:login_required]) unless viewer

    async_integration_reasons = if viewer.can_act_for_integration?
      async_is_integration_with_write_access?(viewer).then do |has_access|
        has_access ? [] : [:insufficient_access]
      end
    else
      Promise.resolve([])
    end

    async_user_reasons = async_readable_by?(viewer).then do |readable|
      next [:insufficient_access] unless readable
      next [] if viewer.id == user_id

      async_organization.then do |org|
        org.async_adminable_by?(viewer).then do |is_adminable|
          is_adminable ? [] : [:insufficient_access]
        end
      end
    end

    Promise.all([async_integration_reasons, async_user_reasons]).
      then do |integration_reasons, user_reasons|
        (integration_reasons + user_reasons).uniq
      end
  end

  # Can the given viewer delete this org discussion?
  #
  # viewer - One of {IntegrationInstallation, Bot, (non-bot) User}.
  #
  # Returns a Promise of a Boolean.
  def async_viewer_can_delete?(viewer)
    return Promise.resolve(false) unless viewer

    async_integration_can_delete = if viewer.can_act_for_integration?
      async_is_integration_with_write_access?(viewer)
    else
      Promise.resolve(true)
    end

    async_user_can_delete = async_readable_by?(viewer).then do |readable|
      next false unless readable
      next true if viewer.id == user_id

      async_organization.then { |org| org.async_adminable_by?(viewer) }
    end

    Promise.all([async_integration_can_delete, async_user_can_delete]).then(&:all?)
  end

  def viewer_can_delete?(viewer)
    async_viewer_can_delete?(viewer).sync
  end

  private

  def body_is_present
    errors.add(:body, "cannot be blank") if body&.strip.blank?
  end
end
