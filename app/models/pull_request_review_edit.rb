# rubocop:disable Style/FrozenStringLiteralComment

class PullRequestReviewEdit < ApplicationRecord::Domain::Repositories
  include FilterPipelineHelper
  include Instrumentation::Model
  include UserContentEdit::Core

  belongs_to :pull_request_review

  alias_attribute :user_content_id, :pull_request_review_id
  alias_method :user_content, :pull_request_review
  alias_method :async_user_content, :async_pull_request_review

  def user_content_type
    "PullRequestReview"
  end

  def global_relay_id
    Platform::Helpers::NodeIdentification.to_global_id(platform_type_name, user_content_edit_id || "PullRequestReviewEdit:#{id}")
  end
end
