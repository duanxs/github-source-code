# frozen_string_literal: true

require "octolytics/pond"

class RetiredNamespace < ApplicationRecord::Ballast
  include GitHub::Relay::GlobalIdentification

  # Number of clones a repo must have in a week to be retired
  WEEKLY_CLONE_THRESHOLD = 100

  belongs_to :owner, class_name: "User", foreign_key: "owner_login", primary_key: "login"

  validates_presence_of :owner_login, :name
  validates_uniqueness_of :name, scope: :owner_login, case_sensitive: false
  validate :retired_namespaces_enabled, on: :create
  validates_format_of :name_with_owner, with: Repository::NAME_WITH_OWNER_PATTERN

  before_validation :downcase_name_and_owner_login
  after_create :instrument_retire_namespace
  after_destroy :instrument_unretire_namespace

  default_scope { order("owner_login ASC", "name ASC") }
  scope :for_owner, -> (owner) {
    owner = owner.login if owner.is_a?(User)
    where(owner_login: owner.downcase)
  }

  class << self
    # Retire a given repository's namespace
    #
    # repository - the Repository object
    #
    # Returns the RetiredNamespace or raises an error if invalid
    def create_from_repository!(repository)
      retire_redirects!(repository)
      create!(owner_login: repository.owner.try(:login), name: repository.name)
    end

    # Retire a given repository's redirected namespaces
    #
    # repository - the Repository object
    def retire_redirects!(repository)
      redirects = repository.redirects.map do |redirect|
        owner_login, name = redirect.repository_name.downcase.split("/")
        { owner_login: owner_login, name: name }
      end.uniq

      # To avoid an N+1 by calling retired? for each redirect, batch query
      # existing namespaces so that we can skip already-retired ones below
      retired_namespaces = RetiredNamespace.where(
        owner_login: redirects.map { |h| h[:owner_login] },
        name: redirects.map { |h| h[:name] },
      ).map { |rn| { owner_login: rn.owner_login, name: rn.name } }

      (redirects - retired_namespaces).each { |redirect| create!(redirect) }
    end

    # Should the given repository's namespace be retired?
    #
    # Repositories must be public and must have received more clones
    # than the WEEKLY_CLONE_THRESHOLD in the previous week.
    #
    # repository - the Repository object or a name with owner string
    #
    # Returns true if it should be retired, otherwise false
    def should_retire?(repository)
      return false unless GitHub.retired_namespaces_enabled?
      repository = Repository.with_name_with_owner(repository) unless repository.is_a?(Repository)
      return false unless repository.public?
      return false if repository.is_user_pages_repo?
      clones_in_the_past_week(repository.id) >= WEEKLY_CLONE_THRESHOLD
    rescue Octolytics::ServerError
      true # If the Pond request fails, default to true to avoid false negatives
    end

    # Does the given repository conflict with a retired namespace?
    #
    # owner_or_nwo - the repository owner, or a full name with owner string
    # name         - if owner is passed as the first argument, the repo name
    #
    # Returns true if the namespace is retired, otherwise false
    def retired?(owner_or_nwo, name = nil)
      return false unless GitHub.retired_namespaces_enabled?
      owner_or_nwo, name = owner_or_nwo.split("/") if name.nil?
      RetiredNamespace.exists?(owner_login: owner_or_nwo.downcase, name: name.downcase)
    end

    private

    def pond_client
      @pond_client ||= Octolytics::Pond.new(secret: GitHub.pond_shared_secret)
    end

    def clones_in_the_past_week(repository_id)
      GitHub.dogstats.increment "retired_namespace.count_recent_clones"
      pond_client.counts(repository_id, "clone", "week").data["data"]["count"].to_i
    end
  end

  # Returns the retired namespace's name with owner string
  def name_with_owner
    @name_with_owner ||= [owner_login, name].join("/")
  end
  alias_method :nwo, :name_with_owner

  private

  def downcase_name_and_owner_login
    self.name = self.name.dup.downcase if self.name
    self.owner_login = self.owner_login.dup.downcase if self.owner_login
  end

  def retired_namespaces_enabled
    errors.add(:base, "Retired namespaces not enabled") unless GitHub.retired_namespaces_enabled?
  end

  def instrument_retire_namespace
    GitHub.dogstats.increment "retired_namespace.retire"
  end

  def instrument_unretire_namespace
    GitHub.dogstats.increment "retired_namespace.unretire"
  end
end
