# rubocop:disable Style/FrozenStringLiteralComment

class RepositoryLicense < ApplicationRecord::Domain::Repositories
  areas_of_responsibility :community_and_safety

  belongs_to :repository

  validates_presence_of :repository_id
  validates_uniqueness_of :repository_id
  validates_presence_of :license_id
  validates_inclusion_of :license_id, in: License.valid_license_ids,
    message: "%{value} is not a valid license id."

  # Returns the string key of a detected license, otherwise "other" or "no-license"
  def self.detect_license(repo)
    repo.rpc.detect_license(repo.default_oid)
  rescue GitRPC::BadRepositoryState
    "other" # license file is too small for git-based detection
  rescue GitRPC::InvalidRepository, GitRPC::ObjectMissing, GitRPC::Failure
    "no-license"
  end

  # Detect a repository's license and write to the database
  # Returns the License object, or nil if no license or a private repo
  def self.set_license(repo)
    key = RepositoryLicense.detect_license(repo)

    # Temporary fix for inconsistency with GitRPC
    # See https://github.com/github/gitrpc/pull/353
    key = "other" if key == "unknown"

    license = License.find_by_key(key, hidden: true)
    return license if license == repo.license # value in the database is accurate

    GitHub.dogstats.increment "license.set", tags: [
      "license:#{license.nil? ? "no-license" : license.key}",
      "from_license:#{repo.license.nil? ? "no-license" : repo.license.key}",
    ]

    ActiveRecord::Base.connected_to(role: :writing) do
      if license.nil? || license.key == "no-license"
        repo.repository_license.destroy if repo.repository_license
        return nil
      elsif repo.repository_license
        repo.repository_license.update! license_id: license.id
      else
        RepositoryLicense.create! license_id: license.id, repository: repo
      end
    end
    license
  end

  def self.push_changed_license?(push)
    return false if push.branch_name != push.repository.default_branch
    return true  if push.initial_commit? # initial commit
    return false unless push.changed_files
    push.changed_files.any? { |file| Licensee::ProjectFiles::LicenseFile.name_score(file.path) > 0 }
  end

  # Since License is not an ActiveRecord object, stub expected helper methods
  def license
    License.find_by_id(license_id)
  end

  def license=(license)
    self.license_id = license.id
  end
end
