# rubocop:disable Style/FrozenStringLiteralComment

class ReviewThread
  # Identifies a thread of comments as a first-class object. A ReviewThread::Key
  # is a composite key of the diff location as well as the thread
  # (DeprecatedPullRequestReviewThread or CommitCommentThread) on that line.
  #
  # With pull request reviews enabled, several threads may exist
  # on the same diff line, and a comment may explicitly be a reply to a "parent"
  # comment.
  class Key
    attr_reader :path, :position, :parent_id

    def initialize(comment:, position:, path:)
      @path = path
      @position = position

      # FIXME Find better modeling for key components.
      # @parent_id = comment.legacy_comment? ? nil : (comment.reply_to_id || comment.id)
      @parent_id = nil
    end

    # A thread is valid if it can still be located by position within the diff.
    # Threads turn invalid when the diff changes and the conversation becomes
    # outdated. A thread is also invalid when the file is deleted from the diff.
    def valid?
      position && path
    end

    def ==(other)
      path == other.path &&
        position == other.position &&
        parent_id == other.parent_id
    end
    alias_method :eql?, :==

    def hash
      [path, position, parent_id].hash
    end
  end
end
