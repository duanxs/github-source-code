# rubocop:disable Style/FrozenStringLiteralComment

# Timeline behavior which applies to Issues and Pull Requests.
module IssueTimeline

  # Public: Grab all timeline items sorted by creation time
  #   Depends upon included classes defining `timeline_items_for` and
  #   `timeline_insert_markers` methods, specifying what methods make up
  #   a timeline.
  #
  # viewer - the User that is viewing the conversation (current_user)
  # since  - optional Time to restrict showing only more-recent items
  #
  # Returns an Array of items.
  def timeline_for(viewer, since: nil, all_valid_events: false)
    all_timeline_items_for(
      viewer,
      since: since,
      all_valid_events: all_valid_events,
    )
  end

  # Public: Get all child items in the timeline. Parent items (e.g.,
  # DeprecatedPullRequestReviewThread or CommitCommentThread)
  # are not included, only their children.
  #
  # viewer - the User that is viewing the conversation (current_user)
  #
  # Returns an Enumerator that yields child items.
  def timeline_children_for(viewer)
    child_enumerator(timeline_for(viewer))
  end

  # Public: Get the time this timeline was last updated
  #
  # viewer - the User that is viewing the timeline (current_user)
  #
  # Returns a Time
  def timeline_updated_at_for(viewer)
    if item = timeline_for(viewer).last
      item.created_at
    else
      updated_at
    end
  end

  # Issue events that should be displayed in the timeline view.
  #
  # Returns an Array of IssueEvents
  #
  # NOTE: PullRequest#visible_events overrides this for PRs
  def visible_events(all_valid_events: false, viewer: nil)
    # TODO: See if we can take out the issue-ref reject
    # The data in production should all be moved from IssueEvent to CrossReference
    # and Enterprise *should* be fine, I think
    async_repository.then do |repository|
      # This method builds up a large scope depending on certain flags, and then eventually
      # calls #pluck(:id) on the scope to grab all the IssueEvent IDs we'll need.
      # Drops the default includes from events scope.
      selected_events = events.unscope(:includes)
      selected_events = all_valid_events ? selected_events.valid : selected_events.visible

      selected_events = selected_events.without_orphaned_commit_refs

      unless viewer&.site_admin?
        selected_events = selected_events.where("`commit_repositories`.`disabled_at` IS NULL")
      end

      event_ids = filter_project_events(selected_events)

      events_visible = []
      event_ids.each_slice(IssueEvent::MAX_EVENT_CHUNK_SIZE) do |slice|
        events_visible.concat(IssueEvent.find(slice))
      end

      events_visible.reject(&:issue_reference?)
    end.sync
  end

  # Internal: Filter project events from given events scope
  #
  # Return Array<Integer> - List of event ids
  private def filter_project_events(events_scope)
    repo_projects_disabled = !repository.projects_enabled?
    org_projects_disabled = repository.owner.organization? && !repository.owner.organization_projects_enabled?

    return events_scope.pluck(:id) if !repo_projects_disabled && !org_projects_disabled

    event_ids_with_subject = events_scope.left_joins(:issue_event_detail).
      pluck("issue_events.id, issue_event_details.subject_id, issue_event_details.subject_type")

    project_ids = event_ids_with_subject.each_with_object(Set.new) do |(_, subject_id, subject_type), result|
      result << subject_id if subject_type == "Project"
    end
    project_owner_types_by_id = Project.where(id: project_ids).pluck(:id, :owner_type).to_h

    event_ids_with_subject.reject! do |_, subject_id, subject_type|
      next unless subject_type == "Project"

      project_owner_type = project_owner_types_by_id[subject_id]

      (repo_projects_disabled && project_owner_type == "Repository") ||
        (org_projects_disabled && project_owner_type == "Organization")
    end

    event_ids_with_subject.map! { |event_id, _, _| event_id }
  end

  # Public: Returns the visible events for the viewer User.
  #
  # Filters out any private commit mentions that the viewer should not see.
  # Also filters out any special event types the viewer should not see.
  #
  # Returns an Array of IssueEvents.
  def visible_events_for(viewer, all_valid_events: false)
    visible_events(all_valid_events: all_valid_events, viewer: viewer).reject do |event|
      (event.commit? && event.reference? &&
       (!event.repository_for_commit.pullable_by?(viewer)) ||
        !event.visible_to?(viewer)) ||
      (event.event == "user_blocked" && event.subject&.hide_from_user?(viewer)) ||
      hide_subject_issue_for(viewer, event)
    end
  end

  private def hide_subject_issue_for(viewer, event)
    return false unless IssueEvent::CROSS_ISSUE_SUBJECT_EVENTS.include?(event.event)

    !event.subject_issue_readable_by?(viewer) ||
      violated_oap?(viewer, event.subject&.repository)
  end

  # Returns the issue references for this issue.
  #
  # If the thread is locked, return only refs that happened before the lock,
  # regardless of who made the issue reference.
  def visible_references_for(viewer)
    owner = self.repository.owner

    issue_refs = references.filter_spam_for(viewer)

    # Include only xrefs where the source still exists (eg. vs issues in deleted repos)
    # and is visible to the viewer (eg. vs issue marked as spam for non-staff viewers)
    #
    # Scoped to issues to prevent an xref with a non-issue source from joining in an issue that
    # happens to have the same id as the source.
    issue_refs = issue_refs.issues
                   .joins("INNER JOIN issues ON cross_references.source_id = issues.id")
                   .merge(Issue.filter_spam_for(viewer))

    # hide refs from actors ignored by the owner and/or viewer
    ignored  = owner.ignored.pluck(:id)
    ignored |= viewer.ignored.pluck(:id) if viewer && owner != viewer
    issue_refs = issue_refs.where("actor_id NOT IN (?)", ignored) if ignored.any?

    # only return pre-lock
    issue_refs = issue_refs.where("cross_references.created_at < ?", locked_at) if locked? && locked_at

    # hide refs if viewer is an oauth app and org restricts oauth access
    violating_ids = oap_violating_repo_ids(viewer)
    if violating_ids.present?
      issue_refs = issue_refs
        .with_valid_source_issue_repository
        .where("`source_issue_repositories`.`id` NOT IN (?)", violating_ids)
    end

    # hide refs from actors who do not have access to the issue
    issue_refs_with_visibility = Promise.all(issue_refs.map { |ref|
      ref.async_visible_to?(viewer).then { |visible| [ref, visible] }
    }).sync

    issue_refs = issue_refs_with_visibility.each_with_object([]) do |(ref, visible), result|
      result << ref if visible
    end

    issue_refs
  end

  def prefill_timeline_associations(items, preload_diff_entries: false, show_mobile_view: false)
    items.group_by(&:class).each_value do |items|
      case items.first
      when Platform::Models::PullRequestCommit
        # only prefill for mobile views that are not yet transitioned to GQL
        next unless show_mobile_view

        commits = items.map(&:commit)
        Commit.prefill_comment_counts(commits)
        Commit.prefill_combined_statuses(commits, repository)
        Commit.prefill_verified_signature(commits, repository)
      when DeprecatedPullRequestReviewThread
        DeprecatedPullRequestReviewThread.preload_diff_entries(items) if preload_diff_entries
      when IssueEvent
        next unless show_mobile_view
        GitHub::PrefillAssociations.for_issue_events(items)
      when PullRequestReview
        GitHub::PrefillAssociations.prefill_belongs_to items, PullRequest, :pull_request_id
        GitHub::PrefillAssociations.prefill_associations items, [:repository]
      end
    end

    nil
  end

  # Internal: Clear the cached timeline
  #
  # Returns nothing
  def reset_timeline
    @timeline = nil
    @all_timeline_items_memoized = nil
  end

  # Reset timeline when reloading
  def reset_memoized_attributes
    reset_timeline
  end

  def violated_oap?(viewer, repository)
    return false unless viewer&.using_oauth_application? && GitHub.oauth_application_policies_enabled?
    return false unless repository

    !OauthApplicationPolicy::Application.new(repository, viewer.oauth_application).satisfied?
  end

  private

  def oap_violating_repo_ids(viewer)
    return [] unless viewer&.using_oauth_application?
    Repository.oauth_app_policy_violated_repository_ids(
      repository_ids: viewer.associated_repository_ids(include_oauth_restriction: false),
      repository_scope: Repository.private_scope,
      app: viewer.oauth_application,
    )
  end

  def all_timeline_items_for(viewer, since: nil, all_valid_events: false)
    @all_timeline_items_memoized ||= {}
    @all_timeline_items_memoized[viewer] ||= begin
      # This experiment will compare the two existing timeline implementations;
      # one is used for mobile, and one for web views and API endpoints
      # Until we drop the experiment and merge the two timelines,
      # we'll continue to run it as part of our all-features CI build
      if viewer && GitHub.flipper["fast-timeline-experiment-inline"].enabled?(viewer)
        run_fast_timeline_experiment(viewer, since, all_valid_events)
      else
        legacy_all_timeline_items_for(viewer, since: since, all_valid_events: all_valid_events)
      end
    end
  end

  def legacy_all_timeline_items_for(viewer, since: nil, all_valid_events: false)
    items = StableSorter.new(timeline_items_for(viewer, all_valid_events: all_valid_events)).sort_by(&:timeline_sort_by)
    items = timeline_insert_markers(viewer, items)

    if since
      items.select! do |item|
        item.created_at > since
      end
    end

    items.freeze
  end

  def new_all_timeline_items_for(viewer, since: nil, all_valid_events: false)
    filter_options = {
      since: since,
      visible_events_only: !all_valid_events,
      filter_closed_if_preceded_by_merged: !all_valid_events &&
                                           self.respond_to?(:merged?) && merged?,
    }

    timeline_items = timeline_model_for(viewer, filter_options).async_values.sync

    IssueTimeline::LegacyAdapter.new(timeline_items, viewer).call
  end

  def run_fast_timeline_experiment(viewer, since, all_valid_events)
    FastTimelineExperimentJob.perform_now(
      self.class.name.underscore, id, viewer, since, all_valid_events
    )
  end

  # Iterate over all child items in the timeline. Parent items (e.g.,
  # DeprecatedPullRequestReviewThread or CommitCommentThread)
  # are not included, only their children.
  #
  # items - Array of timeline items (e.g., as returned by #timeline_for).
  #
  # Returns an Enumerator.
  def child_enumerator(items)
    Enumerator.new do |y|
      items.each do |item|
        if item.respond_to?(:timeline_children)
          item.timeline_children.each do |child|
            y << child
          end
        else
          y << item
        end
      end
    end
  end

  # Internal: To use the new timeline in `app/models/timeline/` as a drop-in replacement for this
  #   old implementation, we need to do some additional steps which will not become part of the
  #   new implementation but move to other places (like the UI). This class collects all these
  #   steps to make them them easier to spot, understand and dispose.
  class LegacyAdapter
    def initialize(original_timeline_items, viewer)
      @original_timeline_items = original_timeline_items
      @viewer = viewer
    end

    def call
      map_commit_comment_threads(original_timeline_items, viewer)
    end

    private

    attr_reader :original_timeline_items, :viewer

    def map_commit_comment_threads(timeline_items, viewer)
      Promise.all(timeline_items.map { |item|
        if item.is_a?(Platform::Models::PullRequestCommitCommentThread)
          item.async_to_commit_comment_thread(viewer)
        else
          item
        end
      }).sync
    end
  end
end
