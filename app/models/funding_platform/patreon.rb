# frozen_string_literal: true

class FundingPlatform::Patreon
  def self.name
    "Patreon"
  end

  def self.url
    URI("https://patreon.com/")
  end

  def self.key
    :patreon
  end

  def self.template_placeholder
    "# Replace with a single #{name} username"
  end
end
