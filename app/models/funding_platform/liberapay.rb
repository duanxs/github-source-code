# frozen_string_literal: true

class FundingPlatform::Liberapay
  def self.name
    "Liberapay"
  end

  def self.url
    URI("https://liberapay.com/")
  end

  def self.key
    :liberapay
  end

  def self.template_placeholder
    "# Replace with a single #{name} username"
  end
end
