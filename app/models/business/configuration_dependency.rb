# frozen_string_literal: true

module Business::ConfigurationDependency
  extend ActiveSupport::Concern

  include Configurable
  include Configurable::ActionExecutionCapabilities
  include Configurable::ActionsAllowedEntities
  include Configurable::AllowPrivateRepositoryForking
  include Configurable::DefaultRepositoryPermission
  include Configurable::DisableOrganizationProjects
  include Configurable::DisableRepositoryProjects
  include Configurable::DisableTeamDiscussions
  include Configurable::ForkPrWorkflowsPolicy
  include Configurable::IpWhitelistingEnabled
  include Configurable::IpWhitelistingAppAccessEnabled
  include Configurable::MembersCanChangeRepoVisibility
  include Configurable::MembersCanCreateRepositories
  include Configurable::MembersCanDeleteIssues
  include Configurable::MembersCanDeleteRepositories
  include Configurable::MembersCanInviteOutsideCollaborators
  include Configurable::MembersCanMakePurchases
  include Configurable::MembersCanUpdateProtectedBranches
  include Configurable::MembersCanViewDependencyInsights
  include Configurable::PackageAvailability
  include Configurable::SshCertificateRequirement
  include Configurable::TwoFactorRequired
  include Configurable::ResellerCustomer
  include Configurable::AdvancedSecurity

  # Internal: Get the configuration owner.
  #
  # Values for a Business can be cascaded from (or overridden by) the global
  # GitHub object.
  def configuration_owner
    GitHub
  end
end
