# frozen_string_literal: true

class Business::LicenseAttributer
  attr_reader :business

  delegate :organization_member_ids, :outside_collaborator_ids,
    :pending_member_invitations, :pending_collaborator_invitations,
    :user_accounts, :user_accounts_with_only_emails,
    :enterprise_installation_user_ids,
    to: :business

  def initialize(business)
    @business = business
  end

  def user_ids
    @user_ids ||= (
      organization_member_ids +
      enterprise_installation_user_ids +
      outside_collaborator_ids(on_repositories_with_visibility: [:private], include_forks: false) +
      non_expired_pending_member_invitations.where(email: nil).distinct.pluck(:invitee_id) +
      pending_collaborator_invitation_user_ids
    ).to_set
  end

  def emails
    @emails ||= (
      primary_emails_of_user_accounts_with_only_emails +
      non_expired_pending_member_invitations.where.not(email: nil).distinct.pluck(:normalized_email) +
      pending_collaborator_invitation_emails
    ).to_set
  end

  def unidentified_business_user_account_ids
    # enterprise server users without a linked user or email address
    user_accounts.where(user_id: nil).pluck(:id) - user_accounts_with_only_emails.pluck(:id)
  end

  # A user/email will consume a license if:
  # They are an admin of a managed organization,
  # They are a member of a managed organization (except if they have the role of billing manager),
  # They are an outside collaborator on a private repository owned by a managed organization (except forks),
  # They have a pending invitation to a managed organization (except if the invitation is for a role of billing manager), and/or
  # They have a pending invitation to a private repository owned by a managed organization (except forks).
  # In addition to associations with managed organizations, users from connected Enterprise Server instances will consume a license.
  def unique_count
    user_ids.count + emails.count + unidentified_business_user_account_ids.count
  end

  private

  def primary_emails_of_user_accounts_with_only_emails
    user_accounts_with_only_emails.
      distinct(false). # Since we're grouping on the ID which will be unique we don't need this (and save ~100ms for some queries without it)
      group(:id).
      pluck("MIN(email)") # Grab the first primary email address if they have multiple
  end

  def pending_collaborator_invitation_user_ids
    @_pending_collaborator_invitation_user_ids ||= pending_collaborator_invitation_user_ids_and_emails.map(&:first).compact
  end

  def pending_collaborator_invitation_emails
    @_pending_collaborator_invitation_emails ||= pending_collaborator_invitation_user_ids_and_emails.map(&:last).compact
  end

  def pending_collaborator_invitation_user_ids_and_emails
    @_pending_collaborator_invitation_user_ids_and_emails ||= pending_collaborator_invitations(repository_visibility: :private).
      excluding_expired.
      pluck(:invitee_id, :email)
  end

  def non_expired_pending_member_invitations
    pending_member_invitations.excluding_expired
  end
end
