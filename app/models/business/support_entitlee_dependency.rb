# frozen_string_literal: true

module Business::SupportEntitleeDependency
  MAX_SUPPORT_ENTITLEES = 20

  # Public: Add a support entitlement for a user in the business
  #
  # A Business::SupportEntitlee is a User who is a member of a
  # Business and is entitled to view/open tickets for the Business
  # in the HelpHub support portal
  #
  # user        - The User to add the support entitlement for
  # actor       - The User adding the support entitlement
  #
  # Does nothing if the user is not a member of the business.
  #
  # Returns nothing.
  def add_support_entitlee(user, actor:)
    return unless member?(user)
    return if actor_ids(type: "SupportEntitlee").count >= MAX_SUPPORT_ENTITLEES

    grant Business::SupportEntitlee.new(user), :write
    instrument :add_support_entitlee, user: user, actor: actor
  end

  # Public: Remove a support entitlement for a user in the business
  #
  # user    -  The User to remove the support entitlment for
  # actor   - The User revoking the support entitlement
  #
  # Returns nothing.
  def remove_support_entitlee(user, actor:)
    revoke Business::SupportEntitlee.new(user)
    instrument :remove_support_entitlee, user: user, actor: actor
  end

  # Internal: remove all support entitlees on destroy
  def remove_support_entitlees
    User.where(id: actor_ids(type: "SupportEntitlee"), type: "User").each do |user|
      remove_support_entitlee(user, actor: nil)
    end
  end

  # Public: Does the given user have a support entitlement for this Business?
  #
  # user - The user to check.
  #
  # Returns a Boolean.
  def support_entitled?(user)
    member?(user) && permit?(Business::SupportEntitlee.new(user), :write)
  end

  # Public: The users with a support entitlement for this Business
  #
  # Returns [Users]
  def support_entitlees
    User.where(id: actor_ids(type: "SupportEntitlee"), type: "User").select { |u| member?(u) }
  end


end
