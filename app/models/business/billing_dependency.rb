# frozen_string_literal: true

module Business::BillingDependency
  extend ActiveSupport::Concern

  include Billing::MeteredBillable

  FIRST_DAY_OF_AZURE_BILLING_CYCLE = 27

  included do
    scope :with_azure_subscription, -> {
      where(
        id: Billing::EnterpriseAgreement.active.where.not(azure_subscription_id: nil).pluck(:business_id),
      )
    }
  end

  # Public: Is this business billed by invoice?
  #
  # Returns a Boolean.
  def invoiced?
    return false if GitHub.single_business_environment?
    customer.invoiced?
  end

  # Public: Are decisions about this business's plan and settings made by a
  #         business owner? If not, the business is managed by our sales team.
  #
  # Returns a Boolean.
  def can_self_serve?
    return false if GitHub.single_business_environment?
    can_self_serve
  end

  # Public: Which plan the business is on.
  #
  # - On GitHub Enterprise the global business is on the Enterprise plan.
  # - On GitHub.com all businesses are on the Business Plus plan.
  #
  # Returns the GitHub::Plan for the business.
  def plan
    @plan ||= if GitHub.enterprise?
      GitHub::Plan.default_plan
    else
      GitHub::Plan.business_plus
    end
  end

  # Public: Is this business billed by the "month" or "year".
  #
  # Returns a String.
  def plan_duration
    Organization::YEARLY_PLAN
  end

  # Public: When will this business be invoiced next.
  #
  # Returns a Date.
  def billed_on
    return unless billing_term_ends_on
    billing_term_ends_on + 1.day
  end

  def direct_or_team_member?(user)
    organizations.any? { |org| org.direct_or_team_member?(user) }
  end

  # Public: Returns the aggregated asset status across all owned organizations.
  #
  # The resulting Hash looks like this:
  #
  # {
  #   asset_packs: 1,
  #   bandwidth_usage: 15.2,
  #   bandwidth_quota: 50.0,
  #   storage_usage: 36.4,
  #   storage_quota: 50.0
  # }
  #
  # Returns a Hash.
  def aggregated_asset_status
    @aggregated_asset_status ||= begin
      status = {
        asset_packs: 0,
        bandwidth_usage: 0.0,
        bandwidth_quota: 0.0,
        storage_usage: 0.0,
        storage_quota: 0.0,
      }
      async_organizations.then do |organizations|
        organizations.to_a.each do |org|
          org_asset_status = org.async_asset_status.sync || org.build_asset_status
          if org_asset_status
            status[:asset_packs] += org_asset_status.asset_packs
            status[:bandwidth_usage] += org_asset_status.bandwidth_usage
            status[:bandwidth_quota] += org_asset_status.bandwidth_quota
            status[:storage_usage] += org_asset_status.storage_usage
            status[:storage_quota] += org_asset_status.storage_quota
          end
        end
        status
      end.sync
    end
  end

  # Public: Returns bandwidth usage as a percentage.
  #
  # Returns an Integer.
  def bandwidth_usage_percentage
    return 0 if aggregated_asset_status[:bandwidth_quota] == 0
    percent = (aggregated_asset_status[:bandwidth_usage] / aggregated_asset_status[:bandwidth_quota]).round(2)
    (percent * 100).to_i
  end

  # Public: Returns storage usage as a percentage.
  #
  # Returns an Integer.
  def storage_usage_percentage
    return 0 if aggregated_asset_status[:storage_quota] == 0
    percent = (aggregated_asset_status[:storage_usage] / aggregated_asset_status[:storage_quota]).round(2)
    (percent * 100).to_i
  end

  # Public: Sets up an organization to have it billing managed by a business.
  #
  # Returns nil.
  def migrate_organization_to_business_billing(organization)
    Organization.transaction do
      actor = (User.find_by_id(GitHub.context[:actor_id]) || User.ghost)

      old_billing_type = organization.billing_type
      organization.switch_billing_type_to_invoice(actor)
      organization.enable!
      sync_organization_billing_settings(organization)

      GitHub.instrument("billing.change_billing_type",
           old_billing_type: old_billing_type,
           billing_type: organization.billing_type,
           user: organization,
           actor: actor,
           actor_id: actor.id)
    end
  end

  def synced_billing_settings_changed?
    watched = %w(seats billing_term_ends_at terms_of_service_type terms_of_service_company_name)
    (saved_changes.keys & watched).any?
  end

  def sync_all_organization_billing_settings
    organizations.each { |o| sync_organization_billing_settings(o) }
  end

  def sync_organization_billing_settings(organization)
    return unless GitHub.billing_enabled?

    track_billing_changes(organization, reason: "Synced with parent business billing settings") do |org|
      org.update! \
        plan: self.plan.name,
        plan_duration: self.plan_duration,
        seats: self.seats,
        billed_on: self.billed_on
    end

    sync_organization_terms_of_service(organization)
  end

  def sync_organization_terms_of_service(organization)
    terms_of_service = organization.terms_of_service
    terms_of_service.update(type: terms_of_service_type, actor: organization, company_name: terms_of_service_company_name)
  end

  def track_billing_changes(organization, actor: nil, reason: nil, &block)
    actor ||= (User.find_by_id(GitHub.context[:actor_id]) || User.ghost)

    # This is necessary since organization attributes now delegate to the business
    plan_name_was = organization.read_attribute(:plan) || GitHub.default_plan_name
    plan_was = GitHub::Plan.find(plan_name_was, effective_at: organization.plan_effective_at)
    seats_were = organization.read_attribute(:seats)

    yield organization

    organization.track_plan_change(actor, plan_was, reason: reason)
    organization.track_seat_change(actor, old_seats: seats_were, reason: reason)
  end

  def enterprise_agreement?
    enterprise_agreements.active.any?
  end

  def track_seat_change(old_seats)
    return if old_seats == seats

    actor = User.find_by(id: GitHub.context[:actor_id]) || User.ghost

    instrument(
      "seat_change",
      old_seats: old_seats,
      seats: seats,
      actor: actor,
      actor_id: actor.id,
    )
  end

  def previous_billing_date
    billing_date = billed_on || GitHub::Billing.today
    billing_date - 1.year
  end

  def first_day_in_next_metered_cycle
    if enterprise_agreement?
      # This works because FIRST_DAY_OF_AZURE_BILLING_CYCLE is the 27th which
      # exists in every month
      first_day_in_metered_cycle + 1.month
    else
      estimated = first_day_in_metered_cycle + 1.month
      year = estimated.year
      month = estimated.month
      day = day_starting_metered_cycle(year, month)
      Date.new(year, month, day)
    end
  end

  def first_day_in_metered_cycle
    if enterprise_agreement?
      if GitHub::Billing.today.day < FIRST_DAY_OF_AZURE_BILLING_CYCLE
        (GitHub::Billing.today - 1.month).change(day: FIRST_DAY_OF_AZURE_BILLING_CYCLE)
      else
        GitHub::Billing.today.change(day: FIRST_DAY_OF_AZURE_BILLING_CYCLE)
      end
    else
      year = year_starting_metered_cycle
      month = month_starting_metered_cycle
      day = day_starting_metered_cycle(year, month)
      Date.new(year, month, day)
    end
  end

  def day_starting_metered_cycle(year, month)
    day = previous_billing_date.day
    end_of_month = Date.new(year, month).end_of_month
    if day > end_of_month.day
      end_of_month.day
    else
      day
    end
  end

  def month_starting_metered_cycle
    if previous_billing_date.day > GitHub::Billing.today.day
      (GitHub::Billing.today - 1.month).month
    else
      GitHub::Billing.today.month
    end
  end

  def year_starting_metered_cycle
    if previous_billing_date.day > GitHub::Billing.today.day
      (GitHub::Billing.today - 1.month).year
    else
      GitHub::Billing.today.year
    end
  end

  # Public: Checks whether or not they pay GitHub directly for their service. Businesses with
  # enterprise agreements pay Microsoft, not GitHub directly, so we handle some billing related
  # situations differently for them.
  #
  # Returns Boolean whether GitHub is paid directly by the customer
  def pays_github_directly?
    !enterprise_agreement?
  end
end
