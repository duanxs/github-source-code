# frozen_string_literal: true

class Business::SamlEnforcementPolicy
  attr_reader :business, :user
  alias_attribute :target, :business

  def initialize(business:, user:, organization: nil)
    @business = business
    @organization = organization
    @user = user
  end

  def enforced?
    return @enforced if defined?(@enforced)
    @enforced = calculate_enforced
  end

  private

  def calculate_enforced
    return false unless @business && @user
    return false unless @business.saml_sso_enabled?
    return false unless @business.member?(@user)
    return false if     @organization&.user_is_outside_collaborator?(@user.id)
    return false unless @business.saml_sso_enforced?

    true
  end
end
