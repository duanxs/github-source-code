# frozen_string_literal: true

# Actions-specific functionality for Businesses
module Business::ActionsDependency
  extend ActiveSupport::Concern

  # We want to split this token into create and delete in the future
  def runner_creation_token_scope
    runner_registration_token_scope
  end

  def runner_deletion_token_scope
    runner_registration_token_scope
  end

  private

  def runner_registration_token_scope
    "CreateBusinessActionsRunner:#{id}"
  end
end
