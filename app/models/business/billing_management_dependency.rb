# frozen_string_literal: true

module Business::BillingManagementDependency
  # Public: Retrieve an instance of the billing management for this business.
  def billing
    @billing ||= Business::BillingManagement.new(self)
  end

  # Public: Is the given user a billing manager?
  def billing_manager?(user)
    billing.manager?(user)
  end

  # Public: retrieve a list of billing managers for this business
  def billing_managers
    billing.members
  end

  # Public: retrieve a list of billing manager ids for this business
  def billing_manager_ids
    billing.member_ids
  end

  # Internal: before_destroy hook to remove all billing managers
  def remove_billing_managers
    billing.remove_all_managers
    true
  end

  # Internal: The Users that should receive billing email
  # for an business.
  #
  # Returns an Array.
  def billing_users
    owners + billing_managers
  end
end
