# frozen_string_literal: true

# Represents a User entitled to the HelpHub support portal for a Business
class Business::SupportEntitlee
  include Ability::Actor

  def initialize(user)
    @user = user
  end

  def ability_type
    "SupportEntitlee"
  end

  def ability_id
    @user.id
  end
end
