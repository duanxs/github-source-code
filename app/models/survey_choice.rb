# rubocop:disable Style/FrozenStringLiteralComment

class SurveyChoice < ApplicationRecord::Domain::Surveys
  include GitHub::Relay::GlobalIdentification

  belongs_to :question, class_name: "SurveyQuestion", required: true
  has_many :answers, class_name: "SurveyAnswer", foreign_key: "choice_id", dependent: :destroy

  validates :short_text, presence: true, allow_blank: true
  validates :text, presence: true, allow_blank: true

  scope :with_answers_count, lambda {
    select("survey_choices.*, COUNT(survey_answers.id) AS num_of_answers")
      .joins("LEFT JOIN survey_answers ON survey_answers.choice_id = survey_choices.id
        LEFT JOIN survey_questions ON survey_questions.id = survey_choices.question_id")
      .order("display_order")
      .group("survey_choices.id")
  }

  def other?
    text =~ /\Aother(\b|$)/i
  end
end
