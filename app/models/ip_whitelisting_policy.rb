# frozen_string_literal: true

# Represents an IP whitelisting policy for an owner of IP whitelist entries.
# Determines whether the IP whitelisting policy of the owner is satisfied based
# on input parameters.
class IpWhitelistingPolicy
  attr_reader :owner, :repository, :ip, :action, :actor

  class PolicyAuthorizationIndeterminateError < StandardError; end

  # Public: Initialise a new policy.
  #
  # owner - The Business or Organization that owns the IP whitelisting policy.
  # ip - A String representing the originating IP address of the request.
  # repository - An optional Repository, if access to a repository is being
  #   requested. If repository is provided, you also need to provide a valid
  #   action argument.
  # action - A Symbol representing the requested action on the repository.
  #   Either :read or :write.
  # actor - The User making the request.
  #
  # Returns a new IpWhitelistingPolicy.
  def initialize(owner: nil, ip: nil, repository: nil, action: nil, actor: nil)
    @owner = owner
    @ip = ip
    @repository = repository
    @action = action
    @actor = actor
  end

  # Public: Is the IP whitelisting policy satisfied?
  #
  # Returns a Boolean.
  def satisfied?
    result = async_satisfied?.sync
    raise PolicyAuthorizationIndeterminateError.new(result.decision) if result.indeterminate?
    result.allow?
  end

  private

  # Private: Is the IP whitelisting policy satisfied?
  #
  # The result can be #indeterminate? and those cases need to be handled by
  # the caller of this method.
  #
  # Returns a Authzd::Response Promise.
  def async_satisfied?
    allowed = Promise.resolve(Authzd::ALLOW)
    return allowed unless GitHub.ip_whitelisting_available?
    return allowed if actor.nil?
    return allowed if owner.nil?
    return allowed if !owner.is_a?(Business) && !owner.is_a?(Organization)

    # Skip an authzd request unless an IP allow list is enabled to avoid
    # unnecessary network calls and possible failures in authzd.
    return allowed unless owner.ip_whitelisting_enabled?

    # Public repositories are always accessible to read
    if repository&.public? && action == :read
      return allowed
    end

    return allowed if exempt_request_for_internal_app?

    return authzd_async_satisfied?
  end

  # Private: Perform IP allow list check in authzd
  #
  # The result can be #indeterminate? and those cases need to be handled by
  # the caller of this method.
  #
  # Returns a Authzd::Response Promise.
  def authzd_async_satisfied?
    Platform::Loaders::Permissions::BatchAuthorize.load(
      actor: actor,
      action: :whitelist_ip,
      subject: owner,
      context: {
        "actor.ip" => actor_ip,
      }.symbolize_keys
    ).then do |satisfied|
      instrument_unsatisfied unless satisfied.allow?
      satisfied
    end
  end

  # Apps on behalf of actors who don't take IP allow lists into consideration.
  #
  # These are internal apps that we have specifically designated with this
  # capability.
  def exempt_request_for_internal_app?
    application = if actor.is_a?(Integration)
      actor
    elsif actor.is_a?(Bot)
      actor&.integration
    elsif actor.is_a?(GitAuth::SSHKey)
      nil
    else
      actor&.oauth_access&.application
    end
    return false unless application.present?

    Apps::Internal.capable?(:ip_whitelist_exempt, app: application)
  end

  def instrument_unsatisfied
    payload = {
      owner: owner,
      active_entries_for_owner: active_entries.includes(:owner).to_a,
      actor_ip: ip,
    }
    if actor.is_a?(Integration)
      payload[:integration_actor] = actor
    elsif actor.is_a?(GitAuth::SSHKey)
      payload[:actor] = nil
    else
      payload[:actor] = actor
    end
    GlobalInstrumenter.instrument("ip_allow_list.policy_unsatisfied", payload)
  end

  def active_entries
    @active_entries ||= IpWhitelistEntry.usable_for(owner).active
  end

  def actor_ip
    self.ip || GitHub.context[:actor_ip]
  end
end
