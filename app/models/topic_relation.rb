# frozen_string_literal: true

class TopicRelation < ApplicationRecord::Domain::Topics
  belongs_to :topic

  before_validation :normalize_name
  after_commit :synchronize_search_index

  MAX_TOPIC_ALIASES = 120
  MAX_RELATED_TOPICS = 10

  validates :name, :topic, presence: true
  validates :relation_type, presence: true
  validates :name, uniqueness: { scope: :topic_id, case_sensitive: true }, length: { maximum: Topic::MAX_NAME_LENGTH },
    format: {
      with: Topic::NAME_REGEX,
      message: "can only include letters, numbers, and hyphens, e.g., node-js-6-3-1",
    }
  validate :name_is_not_already_an_alias_for_another_topic
  validate :under_topic_alias_limit
  validate :under_related_topic_limit

  enum relation_type: { alias: 0, related: 1 }

  scope :with_relation_type, ->(relation_type) {
    where(relation_type: TopicRelation.relation_types[relation_type] || relation_type)
  }

  scope :without_relation_type, ->(relation_type) {
    where("topic_relations.relation_type <> ?", TopicRelation.relation_types[relation_type] || relation_type)
  }

  # Public: Returns true if this relation is for a featured topic.
  def is_topic_featured?
    topic && topic.featured?
  end

  # Public: Reindex the topic that's marked as an alias in ElasticSearch.
  def synchronize_search_index
    return unless name && alias?

    other_topic = Topic.where(name: name).first
    other_topic.synchronize_search_index if other_topic
  end

  private

  def under_topic_alias_limit
    return unless topic

    existing_alias_count = topic.topic_aliases.count
    unless existing_alias_count < MAX_TOPIC_ALIASES
      errors.add(:topic, "cannot have more than #{MAX_TOPIC_ALIASES} aliases")
    end
  end

  def under_related_topic_limit
    return unless topic

    existing_related_count = topic.related_topics.count
    unless existing_related_count < MAX_RELATED_TOPICS
      errors.add(:topic, "cannot have more than #{MAX_RELATED_TOPICS} related topics")
    end
  end

  # Private: Normalizes the name on this RelatedTopic.
  def normalize_name
    return unless name
    self.name = Topic.normalize(name)
  end

  def name_is_not_already_an_alias_for_another_topic
    return unless name && topic_id && alias?

    existing_relations = self.class.alias.where(name: name).where("topic_id <> ?", topic_id)

    if existing_relations.exists?
      errors.add(:name, "is already an alias for another topic")
    end
  end
end
