# frozen_string_literal: true

# This model is only used for replication for local development
class Language < ApplicationRecord::Domain::Repositories
  areas_of_responsibility :languages
  belongs_to :language_name
  belongs_to :repository
end
