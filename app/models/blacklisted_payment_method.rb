# rubocop:disable Style/FrozenStringLiteralComment

# Represents a `flagged` payment method that could be related
# to spam-like/unacceptable behavior. This data be all like:
#
# * user_id                  - Last associated user
# * unique_number_identifier - Braintree's unique id for credit cards.
# * paypal_email             - Their paypal email on file.
#
class BlacklistedPaymentMethod < ApplicationRecord::Domain::Users
  areas_of_responsibility :gitcoin

  # Public:
  belongs_to :user
  validates_presence_of :user
  validate :presence_of_identifier

  # Public: Generate a Blacklisted PaymentMethod from a User and its Payment Method.
  #
  # user - A User with a PaymentMethod
  #
  # returns a BlacklistedPaymentMethod
  def self.create_from_user(user)
    payment_method = user.try(:payment_method)
    create_from_user_and_payment_method(user, payment_method)
  end

  # Public: Generate a Blacklisted PaymentMethod from a User and a Payment Method.
  #
  # user - A User with a PaymentMethod
  #
  # returns a BlacklistedPaymentMethod
  def self.create_from_user_and_payment_method(user, payment_method)
    create \
      user_id: user.try(:id),
      unique_number_identifier: payment_method.try(:unique_number_identifier),
      paypal_email: payment_method.try(:paypal_email)
  rescue ActiveRecord::RecordNotUnique
    find_by_payment_method(payment_method)
  end

  def self.find_by_payment_method(payment_method)  # rubocop:disable GitHub/FindByDef
    where(
      "unique_number_identifier = ? or paypal_email = ?",
      payment_method.unique_number_identifier,
      payment_method.paypal_email,
    ).first
  end

  # Public: Returns the non-null unique identifier
  #
  # Returns a String
  def payment_identifier
    unique_number_identifier || paypal_email
  end

  # Private: Checks to ensure we have either identifier field for the
  # Blacklisted PaymentMethod.
  #
  def presence_of_identifier
    unless unique_number_identifier.present? || paypal_email.present?
      errors.add(:base, "Must have either unique_number_identifier or paypal_email")
    end
  end

  # Public: Returns all users using this blacklist payment method
  def users
    return [] if unique_number_identifier.blank? && paypal_email.blank?

    records = if unique_number_identifier.present?
      PaymentMethod.where("unique_number_identifier = ?", unique_number_identifier).includes(:user)
    else
      PaymentMethod.where("paypal_email = ?", paypal_email).includes(:user)
    end
    records.map(&:user)
  end
end
