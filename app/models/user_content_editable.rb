# rubocop:disable Style/FrozenStringLiteralComment

module UserContentEditable
  extend ActiveSupport::Concern

  # Classes named here mix in UserContentEditable but use an edit class other
  # than UserContentEdit. By convention, these are ActiveRecord models named
  # "#{base}Edit" - for example, a CommitComment's edit class is
  # CommitCommentEdit.
  CUSTOM_EDIT_CLASS_NAMES = {
    "CommitComment" => "CommitCommentEdit",
    "Discussion" => "DiscussionEdit",
    "DiscussionComment" => "DiscussionCommentEdit",
    "Issue" => "IssueEdit",
    "IssueComment" => "IssueCommentEdit",
    "PullRequestReview" => "PullRequestReviewEdit",
    "PullRequestReviewComment" => "PullRequestReviewCommentEdit",
    "RepositoryAdvisory" => "RepositoryAdvisoryEdit",
    "RepositoryAdvisoryComment" => "RepositoryAdvisoryCommentEdit",
  }.freeze

  # Public: Return the name of the ActiveRecord model used to store content edits
  # for a class that stores user content.
  #
  # "classname" is expected to be a model that includes the UserContentEdit::Core
  # model, but this is not enforced.
  def self.edit_class_for(classname)
    CUSTOM_EDIT_CLASS_NAMES.fetch(classname, "UserContentEdit")
  end

  # Public: Return true if a String matches a known custom edit class' name.
  def self.is_custom_edit_class?(classname)
    CUSTOM_EDIT_CLASS_NAMES.has_value?(classname)
  end

  included do |base|
    if edit_class_name = CUSTOM_EDIT_CLASS_NAMES[base.name]
      has_many :user_content_edits, class_name: edit_class_name
      has_one :latest_user_content_edit, -> { order("id DESC") }, class_name: edit_class_name
    else
      has_many :user_content_edits, as: :user_content
      has_one :latest_user_content_edit, -> { order("id DESC") }, as: :user_content, class_name: "UserContentEdit"
    end

    def async_latest_user_content_edit
      (self.is_a?(PullRequest) ? async_issue : Promise.resolve(self)).then do |comment|
        Platform::Loaders::LatestUserContentEdit.load(comment.class, comment.id).then do |latest_edit|
          comment.association(:latest_user_content_edit).target = latest_edit
          latest_edit
        end
      end
    end

    # Public: Determines if the content has been edited and the content edits include the creation edit.
    #
    # This is considered true if there are multiple edits and the first two were created at the same time.
    #
    # Returns a Promise which resolves to a Boolean.
    def async_includes_created_edit?
      (self.is_a?(PullRequest) ? self.async_issue : Promise.resolve(self)).then do |editable|
        Platform::Loaders::IncludesCreatedEdit.load(editable.class, editable.id)
      end
    end

    # Public: Was the content edited by someone besides the original author?
    #
    # Returns a Promise which resolves to a Boolean.
    def async_edited_by_another_user?
      (self.is_a?(PullRequest) ? self.async_issue : Promise.resolve(self)).then do |editable|
        Platform::Loaders::EditedByAnotherUser.load(editable.class, editable.id)
      end
    end
  end

  # Public: Updates this content's `body` attribute. Creates or updates the
  # UserContentEdit record to track the change.
  #
  # Optionally updates the formatter field of this content at the same time.
  #
  # Returns a Boolean.
  def update_body(body, editor, formatter: nil, performed_via_integration: nil)
    GitHub::SchemaDomain.allowing_cross_domain_transactions do
      transaction do
        if track_edits? && body != body_was
          edit_params = { editor: editor,
                          edited_at: Time.current,
                          created_at: Time.current,
                          performed_via_integration: performed_via_integration }

          editor.enable_edit_history_onboarding(self)
          unless user_content_edits.any?
            # create the "initial commit" type edit
            user_content_edits.create!(edit_params.merge(
              editor: user || User.ghost,
              diff: body_was,
              edited_at: created_at || Time.current,
            ))
          end

          user_content_edits.create!(edit_params.merge({ diff: body }))
        end

        # update body attribute last to make sure correct editor is being used
        # https://github.com/github/github/issues/55468
        attributes = { body: body }
        attributes[:formatter] = formatter if formatter

        saved = update(attributes)

        # rollback the transaction to prevent edit history from being created
        # if updating the issue fails.
        raise ActiveRecord::Rollback unless saved

        saved
      end
    end
  end

  # Public: Creates or updates a UserContentEdit record to track a given change
  # in a given attribute.
  #
  # This method assumes that a) it is being run inside a transaction, and b) the
  # content has already been saved in the parent record.
  #
  # This allows us to keep track of content stored in an attribute other than
  # the "body" attribute.
  #
  # Returns a nil or a UserContentEdit record.
  def add_user_content_edit!(content_was, content_is, editor)
    if track_edits? && content_is != content_was
      editor.enable_edit_history_onboarding(self)

      edit_params = { editor: editor,
                      edited_at: Time.current,
                      created_at: Time.current }


      if user_content_edits.empty?
        # create the "initial commit" type edit
        user_content_edits.create!(edit_params.merge({ editor: self.user || User.ghost,
                                                       diff: content_was,
                                                       edited_at: (self.created_at || Time.current),
        }))
      end
      user_content_edits.create!(edit_params.merge({ diff: content_is }))
    end
  end

  # Public: The date this content was edited
  #
  # Returns a DateTime
  def edited_at
    return unless edited?
    latest_user_content_edit.edited_at
  end

  # Public: Has this content been edited?
  #
  # Returns a Boolean
  def edited?
    user_content_edits.any?
  end

  # Public: The user who performed the edit
  #
  # Returns a User
  def editor
    return unless edited?
    latest_user_content_edit.editor
  end

  # Public: The user who performed the edit
  #
  # Returns a User or nil
  def async_editor
    async_latest_user_content_edit.then do |latest_edit|
      next if latest_edit.nil?
      latest_edit.async_editor
    end
  end

  # Use to determine if the edit history for
  # this particular instance of an object
  # includes an edit that marks the creation
  # of the content
  def includes_created_edit?
    if user_content_edits.count >= 2
      return user_content_edits.first.created_at == user_content_edits[1].created_at
    end
    false
  end

  # Whether to show the edit history to the given viewer. This default behavior
  # can be overridden per UserContentEditable model.
  def viewer_can_read_user_content_edits?(viewer)
    async_viewer_can_read_user_content_edits?(viewer).sync
  end

  def async_viewer_can_read_user_content_edits?(viewer)
    if respond_to?(:async_readable_by?)
      async_readable_by?(viewer)
    elsif respond_to?(:readable_by?)
      Promise.resolve(readable_by?(viewer))
    else
      Promise.resolve(false)
    end
  end

  # Whether the given viewer may delete *any* item in the edit history. This
  # default behavior can be overridden per UserContentEditable model.
  #
  # For some users, this will return false meaning that they cannot delete _any_
  # item in the edit history. Still, that same user may be able to delete _some_
  # items in the edit history, i.e., their own.
  #
  # See: UserContentEdit#viewer_can_delete?
  def viewer_can_delete_user_content_edits?(viewer)
    async_viewer_can_delete_user_content_edits?(viewer).sync
  end

  def async_viewer_can_delete_user_content_edits?(viewer)
    if respond_to?(:async_adminable_by?)
      async_adminable_by?(viewer)
    elsif respond_to?(:adminable_by?)
      Promise.resolve(adminable_by?(viewer))
    elsif is_a?(DiscussionItem)
      async_viewer_can_delete?(viewer)
    elsif respond_to?(:async_repository)
      async_repository.then do |repository|
        next false unless repository
        next true if viewer.id == repository.owner_id

        repository.async_pushable_by?(viewer)
      end
    else
      Promise.resolve(false)
    end
  end

  private
  def track_edits?
    # We should only track edits if the comment is not pending.
    # Not all comment types have states, so we have `try` for the method.
    !try(:pending?) && persisted?
  end
end
