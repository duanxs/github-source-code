# frozen_string_literal: true

# Emoji-ish reactions that can be attached to issues, pull requests, and
# comments.
class Reaction < ApplicationRecord::Domain::Repositories
  include GitHub::Relay::GlobalIdentification
  include Spam::Spammable

  belongs_to :user
  belongs_to :subject, polymorphic: true

  setup_spammable(:user)

  validates_presence_of :user_id
  validates_inclusion_of :content, in: Emotion.all.map(&:content)

  # We use a lambda here so that we can stub the valid types our tests.
  validates_inclusion_of :subject_type, in: lambda { |_| valid_subject_types }

  VALID_SUBJECT_TYPES = %w(
      CommitComment
      Discussion
      DiscussionComment
      DiscussionPost
      DiscussionPostReply
      Issue
      IssueComment
      PullRequestReview
      PullRequestReviewComment
      RepositoryAdvisory
      RepositoryAdvisoryComment
  ).to_set.freeze

  # Valid types (classes in this list should include Reaction::Subject)
  def self.valid_subject_types
    VALID_SUBJECT_TYPES
  end

  # Public: Can the specified object be the subject of a reaction?
  #
  # object - Object to check subject validity for.
  #
  # Returns a boolean.
  def self.valid_subject?(object)
    object.respond_to?(:reactable?) && object.reactable?
  end

  # Public: Idempotent all-purpose interface for ensuring a reaction exists
  # between users and subjects.
  # Performs permissions checks and prevents duplicates.
  #
  # user          - User: The user having this reaction
  # subject_id    - Integer: The ID of the subject of this reaction
  # subject_type  - String: The class name of the subject
  # content       - String: One of the members of valid_content
  #
  # Returns: A Reaction in the appropriate state for the subject and user.
  def self.react(user:, subject_id:, subject_type:, content:)
    if subject = reactable_subject_for(user: user, subject_id: subject_id, subject_type: subject_type)
      if existing_reaction = user.reactions.where(subject_id: subject.id, subject_type: subject.class, content: content).first
        # Reaction exists. Return it.
        Reaction::RecordStatus.new([existing_reaction, :exists])
      else
        # Reaction does not already exist. Create one.
        new_reaction = create(user: user, subject: subject, content: content).tap do |reaction|
          subject.notify_socket_subscribers

          if subject.respond_to?(:synchronize_search_index)
            subject.synchronize_search_index
          end

          GitHub.dogstats.increment("reaction", tags: ["action:create", "type:#{content}"])
        end
        Reaction::RecordStatus.new([new_reaction, :created])
      end
    else
      # User can't react to this subject. Return a generic "invalid" reaction
      Reaction::RecordStatus.new([new, :invalid])
    end
  end

  # Public: Idempotent all-purpose interface for ensuring a reaction DOES NOT
  # exist between users and subjects.
  # Performs permissions checks.
  #
  # user          - User: The user having this reaction
  # subject_id    - Integer: The ID of the subject of this reaction
  # subject_type  - String: The class name of the subject
  # content       - String: One of the members of valid_content
  #
  # Returns: A Reaction in the appropriate state for the subject and user.
  def self.unreact(user:, subject_id:, subject_type:, content:)
    if subject = reactable_subject_for(user: user, subject_id: subject_id, subject_type: subject_type)
      if existing_reaction = user.reactions.where(subject_id: subject.id, subject_type: subject.class, content: content).first
        # Reaction exists. Destroy it and return the object.
        existing_reaction.destroy
        subject.notify_socket_subscribers

        if subject.respond_to?(:synchronize_search_index)
          subject.synchronize_search_index
        end

        GitHub.dogstats.increment("reaction", tags: ["action:destroy", "type:#{content}"])

        Reaction::RecordStatus.new([existing_reaction, :deleted])
      else
        # Destroying a reaction that doesn't exist.
        # We simply return a seemingly valid, but unpersisted reaction
        unpersisted_reaction = new(user: user, subject: subject, content: content)
        Reaction::RecordStatus.new([unpersisted_reaction, :deleted])
      end
    else
      # User can't react to this subject. Return a generic "invalid" reaction
      Reaction::RecordStatus.new([new, :invalid])
    end
  end

  # Public: determine whether a given subject is "unlocked" for a user
  def self.subject_unlocked?(user, subject)
    async_subject_unlocked?(user, subject).sync
  end

  def self.async_subject_unlocked?(user, subject)
    return Promise.resolve(false) unless user

    case subject
    when DiscussionPost, DiscussionPostReply
      Promise.resolve(true)
    when RepositoryAdvisory, RepositoryAdvisoryComment
      Promise.resolve(true)
    when Issue
      subject.async_locked?.then { |locked| !locked }
    when IssueComment, PullRequest
      subject.async_issue.then(&:async_locked?).then { |locked| !locked }
    when PullRequestReview, PullRequestReviewComment
      subject.async_pull_request.then(&:async_issue).then(&:async_locked?).then { |locked| !locked }
    when CommitComment
      Platform::Loaders::CommitCommentsLocked.load(subject.repository_id, subject.commit_id).then { |locked|
        !locked
      }
    when Discussion, DiscussionComment
      subject.async_locked_for?(user).then { |locked| !locked }
    else
      Promise.resolve(false)
    end
  end

  def emotion
    Emotion.find(content)
  end

  def self.async_viewer_can_react?(viewer, subject)
    return Promise.resolve(false) unless subject && viewer

    if subject.respond_to?(:async_repository)
      subject.async_repository.then do |repository|
        async_actor_can_react_to?(viewer, subject, repository: repository)
      end
    else
      async_actor_can_react_to?(viewer, subject)
    end
  end

  # Internal: Does the subject exist and does the user have permission to react
  # to it?
  # Returns: The Subject instance
  def self.reactable_subject_for(user:, subject_id:, subject_type:)
    return unless user

    if (subject = find_subject(subject_id, subject_type))
      if self.async_viewer_can_react?(user, subject).sync
        subject
      end
    end
  end
  private_class_method :reactable_subject_for

  # Internal: Finds a Reaction subject
  def self.find_subject(subject_id, subject_type)
    return unless subject_id && subject_type
    subject_type = subject_type.underscore.classify
    return unless valid_subject_type?(subject_type)

    subject_type.safe_constantize.find_by_id(subject_id)
  end
  private_class_method :find_subject

  # Internal: Is the subject_type a valid class for a reaction subject?
  def self.valid_subject_type?(subject_type)
    subject_type && valid_subject_types.include?(subject_type)
  end
  private_class_method :valid_subject_type?

  def self.async_user_can_react_to?(user, subject)
    return Promise.resolve(false) if user.must_verify_email?
    return Promise.resolve(false) unless subject

    subject.async_user.then do |subject_user|
      subject.async_reaction_admin.then do |admin|
        Promise.all([subject.async_readable_by?(user),
                     async_subject_unlocked?(user, subject),
                     user.async_blocked_by?(subject_user),
                     user.async_blocked_by?(admin),
        ]).then do |readable, unlocked, blocked_by_user, blocked_by_admin|
          readable && unlocked && !blocked_by_user && !blocked_by_admin
        end
      end
    end
  end
  private_class_method :async_user_can_react_to?

  # Internal: Can a user react to this subject and repository?
  def self.user_can_react_to?(user, subject)
    async_user_can_react_to?(user, subject).sync
  end
  private_class_method :user_can_react_to?

  # Internal: Can an actor react to this subject and repository?
  def self.actor_can_react_to?(actor, subject, repository: nil)
    async_actor_can_react_to?(actor, subject, repository: repository).sync
  end

  def self.async_actor_can_react_to?(actor, subject, repository: nil)
    return Promise.resolve(false) unless actor

    if actor.can_have_granular_permissions?
      async_installation_could_react_to?(actor, subject, repository: repository)
    else
      async_user_can_react_to?(actor, subject)
    end
  end

  # Internal: COULD a IntegrationInstallation react
  # to this subject and repository?
  #
  # An IntegrationInstallation cannot react on it's own
  # because it's not a User. However we need to see if it
  # could for when a User reacts via an Integration.
  def self.async_installation_could_react_to?(actor, subject, repository: nil)
    case subject
    when CommitComment
      repository.resources.contents.async_writable_by?(actor)
    when PullRequest
      repository.resources.pull_requests.async_writable_by?(actor)
    when Issue
      subject.async_pull_request.then do |pr|
        if pr
          repository.resources.pull_requests.async_writable_by?(actor)
        else
          repository.resources.issues.async_writable_by?(actor)
        end
      end
    when IssueComment
      subject.async_issue.then do |subject_parent|
        subject_parent.async_pull_request.then do |pr|
          if pr
            repository.resources.pull_requests.writable_by?(actor)
          else
            repository.resources.issues.writable_by?(actor)
          end
        end
      end
    when PullRequestReview, PullRequestReviewComment
      repository.resources.pull_requests.async_writable_by?(actor)
    when DiscussionPost, DiscussionPostReply
      subject.async_viewer_can_update?(actor)
    end
  end
  private_class_method :async_installation_could_react_to?

  def self.installation_could_react_to?(actor, subject, repository: nil)
    async_installation_could_react_to?(actor, subject, repository: repository).sync
  end
  private_class_method :installation_could_react_to?
end
