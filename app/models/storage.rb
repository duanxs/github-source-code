# rubocop:disable Style/FrozenStringLiteralComment

module Storage
  require_dependency "storage/uploadable"
  require_dependency "storage/policy_creator"
  require_dependency "storage/policy"
  require_dependency "storage/alambic_policy"
  require_dependency "storage/cluster_policy"
  require_dependency "storage/s3_policy"

  class NotImplementedError < ::NotImplementedError
  end

  class ReplicationError < StandardError
    def initialize(replicas = nil)
      replicas ||= GitHub.storage_replica_count
      super("could not find #{replicas} online replicas")
    end
  end

  def self.policy_creator
    @policy_creator ||= ::Storage::PolicyCreator.new(
      Avatar,
      Marketplace::ListingScreenshot,
      Marketplace::ListingImage,
      OauthApplicationLogo,
      ReleaseAsset,
      RepositoryFile,
      RepositoryImage,
      UploadManifestFile,
      UserAsset,
      MigrationFile,
      EnterpriseInstallationUserAccountsUpload,
    )
  end

  def self.not_implemented!(policy, uploadable, method)
    raise NotImplementedError, "#{uploadable.class} does not implement #{policy.class}##{method}"
  end
end
