# frozen_string_literal: true

class NonMarketplaceListing < ApplicationRecord::Domain::Integrations
  areas_of_responsibility :marketplace

  include GitHub::Relay::GlobalIdentification
  include Workflow
  include PrimaryAvatar::Model
  include Avatar::List::Model
  include Instrumentation::Model
  include GitHub::Relay::GlobalIdentification

  NAME_MAX_LENGTH = 255
  DESCRIPTION_MAX_LENGTH = 255

  # Necessary to upload avatars for a listing. See UploadPoliciesController#deliver_policy.
  belongs_to :category, class_name: "IntegrationFeature", foreign_key: "integration_feature_id"
  belongs_to :integration_listing
  belongs_to :creator, class_name: "User"

  # :state is never nil because it's managed by workflow
  validates :name, :description, :category, :creator, :url, presence: true
  validates :description, length: { maximum: DESCRIPTION_MAX_LENGTH }
  validates :name, length: { maximum: NAME_MAX_LENGTH }
  validates :url, format: %r{\Ahttps?://}
  validate :integration_listing_is_unique
  validate :unique_name_with_approved_listings
  validate :creator_email_is_not_a_support_email

  after_commit :synchronize_search_index
  after_commit :instrument_creation, on: :create

  workflow :state do
    state :draft, 0 do
      event :approve, transitions_to: :approved
      event :reject, transitions_to: :rejected
    end

    state :rejected, 1

    state :approved, 2 do
      event :delist, transitions_to: :delisted
    end

    state :delisted, 3 do
      event :approve, transitions_to: :approved
    end

    on_transition do |from, to, event, *args|
      if user = args[0]
        instrument_state_change(action: event, actor: user, state: to)
      end

      message = args[1]
      email_state_change(action: event, message: message)
    end
  end

  alias_method :publicly_listed?, :approved?

  # Public: Select NonMarketplaceListings with a specified name or description.
  scope :matches_name_or_description, ->(value) do
    query = "%#{ActiveRecord::Base.sanitize_sql_like(value)}%"
    where("#{table_name}.name LIKE ? OR #{table_name}.description LIKE ?", query, query)
  end

  # Public: Select listings in the given category.
  scope :with_category, ->(slug) do
    joins(:category).where("integration_features.slug = ?", slug)
  end

  scope :order_by_name, -> { order("#{table_name}.name ASC") }

  scope :order_by_most_recent, -> { order("#{table_name}.id DESC") }

  scope :created_by, ->(login) { joins(:creator).annotate("cross-schema-domain-query-exempted").where(users: { login: login }) }

  # Return the Works with GitHub listing that was originally created based on the given Integrations
  # listing.
  scope :for_integration_listing, ->(integration_listing) do
    where(integration_listing_id: integration_listing)
  end

  # Returns the Works with GitHub listings where the given User has admin permission on them.
  scope :adminable_by, ->(actor) { where(creator_id: actor) }

  # Returns the Works with GitHub listings that are compatible with GitHub Enterprise
  scope :enterprise_compatible, -> { where(enterprise_compatible: true) }

  # Public: Get the Integer value matching a certain state.
  def self.state_value(name)
    workflow_spec.states[name.to_sym].value
  end

  # Public: Returns an OauthApplication, Integration, or nil.
  def integratable
    return unless integration_listing

    integration_listing.integration
  end

  def primary_avatar_path
    @primary_avatar_path ||= if primary_avatar
      "/nml/#{id}"
    elsif integratable
      integratable.primary_avatar_path
    else
      creator.primary_avatar_path
    end
  end

  # Public: Synchronize this listing with its representation in the search index. If the listing is
  # newly created or modified in some fashion, then it will be updated in the search index. If the
  # listing has been destroyed, then it will be removed from the search index. This method handles
  # both cases.
  def synchronize_search_index
    if self.destroyed?
      RemoveFromSearchIndexJob.perform_later("non_marketplace_listing", self.id)
    else
      Search.add_to_search_index("non_marketplace_listing", self.id)
    end

    self
  end

  def adminable_by?(actor)
    return false unless actor
    actor == creator || actor.can_admin_non_marketplace_listings?
  end

  def editable_by?(actor)
    return true if actor && actor.can_admin_non_marketplace_listings?
    adminable_by?(actor) && draft?
  end

  def avatar_editable_by?(actor)
    editable_by?(actor)
  end

  def event_prefix
    :non_marketplace_listing
  end

  def event_context(prefix: event_prefix)
    {
      "#{prefix}_id".to_sym => id,
      prefix => name,
    }
  end

  def event_payload
    payload = {
      event_prefix => self,
      :state => current_state.name,
      :category => category.name,
      :actor => creator,
      :user => creator,
    }

    if integration_listing
      payload[:integration_listing] = {
        id: integration_listing.id,
        state: integration_listing.state,
        name: integration_listing.name,
      }
    end

    payload
  end

  # Public: Creates an audit log event for this listing changing states with the given User as the
  # actor.
  def instrument_state_change(actor:, action:, state:)
    case action
    when :reject, :approve, :delist
      payload = event_payload.merge(actor: actor, state: state)
      instrument(action, payload)
    end
  end

  # Public: Emails the creator of this listing about the specified state change.
  def email_state_change(action:, message: nil)
    return if GitHub.enterprise?

    mailer_method = case action
    when :reject
      :listing_rejected
    when :approve
      :listing_approved
    when :delist
      :listing_delisted
    end

    return unless mailer_method

    WorksWithGitHubMailer.send(mailer_method, listing: self, message: message).deliver_later
  end

  # Public: Returns the listing's description without a trailing period and with ampersands
  # replaced by the word "and".
  def normalized_description
    (description || "").gsub(/&/, "and").gsub(/\.\z/, "")
  end

  private

  def instrument_creation
    instrument :create
  end

  def integration_listing_is_unique
    return unless integration_listing

    listings = self.class.where(integration_listing_id: integration_listing_id)
    listings = listings.where("id <> ?", id) if persisted?

    if listings.count > 0
      errors.add(:integration_listing, "is already in the Works with GitHub directory")
    end
  end

  def unique_name_with_approved_listings
    return unless name.present?

    listings = self.class.where(name: name).with_approved_state
    listings = listings.where("id <> ?", id) if persisted?

    if listings.count > 0
      errors.add(:name, "is already in use")
    end
  end

  # Disallow submissions from users whose email address begins with 'support@', because our
  # emails about the status of their Works with GitHub listing will not be allowed via Mailchimp.
  def creator_email_is_not_a_support_email
    return unless creator

    if creator.email.downcase.start_with?("support@")
      errors.add(:base, "Submitter's email address cannot start with 'support@'.")
    end
  end
end
