# rubocop:disable Style/FrozenStringLiteralComment

# A Bot is a special type of User.
#
# A Bot only exists in conjunction with an Integration. A Bot performs actions
# on behalf of the Integration. Bots exist solely as a means for attributing
# these actions to an Integration.
#
# Would you like to know more? https://github.com/github/platform/blob/master/proposals/integrations-2/bots-vs-users.md
class Bot < User
  has_one :integration
  validates_presence_of :integration

  has_one :marketplace_listing, through: :integration

  alias_attribute :display_login, :slug

  # This require_dependency can be removed when we switch to Zeitwerk
  require_dependency "bot/remote_authentication_dependency"
  include Bot::RemoteAuthenticationDependency

  LOGIN_SUFFIX = "[bot]"
  LOGIN_REGEX = %r{
    \A                               # Beginning of String
    ([a-zA-Z0-9]+(?:-[a-zA-Z0-9]+)*) # [1] slug
    #{ Regexp.quote LOGIN_SUFFIX }   # string literal "[bot]"
    \z                               # End of String
  }x                                 # x: ignore whitepace
  MAX_SLUG_LENGTH = User::LOGIN_MAX_LENGTH - LOGIN_SUFFIX.length

  AUTHENTICATABLE_TYPES = [
    "IntegrationInstallation",
    "ScopedIntegrationInstallation",
    "SiteScopedIntegrationInstallation",
  ].freeze

  validate :validate_login

  after_create :preemptively_whitelist

  # Public: Returns the IntegrationInstallation representing the bot's
  # current installation context.
  #
  # This installation context determines the bot's permissions. To perform any
  # privileged action (e.g., read the contents of a private repository, create a
  # commit status in a public repository), the bot's current installation
  # context must include permission to perform that action.
  attr_accessor :installation

  alias_attribute :to_s, :slug
  alias_attribute :name, :slug

  delegate :github_owned?, to: :integration

  # This overrides User#instrument_user_signup so that Bots don't
  # publish user.signup hydro events.
  #
  # Returns nothing.
  def instrument_user_signup
    # no=op
  end

  # Internal: the slug for this Bot, based on the unsuffixed part of the login
  #
  # Returns a String
  def slug
    LOGIN_REGEX.match(login) && $1
  end

  # Internal: sets the Bot's login, as slug with suffix
  # This is to namespace Bot logins (so they don't use up available logins for humans).
  #
  # Returns the slug String
  def slug=(value)
    self.login = value + LOGIN_SUFFIX
    value
  end

  # Internal: Set the string representing the Bot's key suitable for use in URLs.
  #
  # Example:
  #
  #   GitHub.enterprise?
  #   # => false
  #
  #   bot.marketplace_listing&.approved?
  #   # => true
  #
  #   bot.to_param
  #   # => "apps/stale"
  #
  #   ##################
  #
  #   GitHub.enterprise?
  #   # => true
  #
  #   bot.to_param
  #   # => "github-apps/stale"
  #
  # Returns a String.
  def to_param
    return @to_param[slug] if defined?(@to_param)

    @to_param = Hash.new do |hash, key|
      hash[key] =  Rails.application.routes.url_helpers.alias_app_path(key)[1..-1]
    end

    @to_param[slug]
  end

  # Internal: Returns the Marketplace listing path for the bot/app if it exists.
  def marketplace_listing_or_app_path
    async_marketplace_listing_or_app_path.sync
  end

  # Internal: Participates in abilities on behalf of this model.
  def ability_delegate
    installation
  end

  def to_query_filter
    self.class.query_filter_from_login(slug)
  end

  # Internal: converts a bot's login or slug to a string that's usable in a
  # search filter (e.g. author:app/hubot)
  def self.query_filter_from_login(login_or_slug)
    "app/#{login_or_slug.chomp(LOGIN_SUFFIX)}"
  end

  # Public: finds a Bot with the given slug.
  #
  # slug - The String slug.
  #
  # Returns a Bot
  # Raises ActiveRecord::RecordNotFound if no Bot is found with the given slug
  def self.find_by_slug(slug)  # rubocop:disable GitHub/FindByDef
    find_by_login("#{slug}#{LOGIN_SUFFIX}")
  end

  # Public: Find the Bot associated with the given token. The returned Bot has
  # its installation context set to the installation associated with the given
  # token. The installation context determines the Bot's permissions (i.e.,
  # which actions it can take on which Repositories).
  #
  # value - The String token value.
  #
  # Returns a Bot or nil.
  def self.find_by_token(token) # rubocop:disable GitHub/FindByDef
    return if token.blank?

    token = AuthenticationToken
      .includes(:authenticatable)
      .where(authenticatable_type: AUTHENTICATABLE_TYPES)
      .active
      .with_unhashed_token(token)
      .first

    tags = []
    if (authenticatable = token&.authenticatable)
      tags << "result:success"

      authenticatable = load_parent_installation(authenticatable, tags)
      GitHub.dogstats.increment("bot.find_by_token", tags: tags)

      return authenticatable.bot
    elsif token && (bot = bot_read_from_collab_primary(token, tags))
      tags << "result:success"
      GitHub.dogstats.increment("bot.find_by_token", tags: tags)

      return bot
    end

    failure_type = token.nil? ? "missing:token" : "missing:authenticatable"
    tags.concat([failure_type, "result:failure"])
    GitHub.dogstats.increment("bot.find_by_token", tags: tags)

    nil
  end

  def self.load_parent_installation(authenticatable, tags)
    return authenticatable unless authenticatable.class.name == "ScopedIntegrationInstallation"
    return authenticatable if authenticatable.parent.present?

    tags << "missing_parent:true"

    # A ScopedIntegrationInstallation's parent should never be nil.
    #
    # If it is, it's due to replication lag. This sets the parent to what it
    # should be so that we can continue the request.
    #
    # See https://github.com/github/github/issues/142265 for further details.
    ActiveRecord::Base.connected_to(role: :writing, prevent_writes: true) do
      authenticatable.parent = IntegrationInstallation.find_by(id: authenticatable.integration_installation_id)
    end

    authenticatable
  end

  def self.bot_read_from_collab_primary(token, tags)
    return false unless token.authenticatable_type == "ScopedIntegrationInstallation"
    tags << "check_primary:true"

    installation = ActiveRecord::Base.connected_to(role: :writing, prevent_writes: true) do
      ScopedIntegrationInstallation.find_by(id: token.authenticatable_id)
    end

    if installation.nil?
      tags << "primary_lookup:failed"
      return
    end

    return installation.bot unless installation.parent.nil?
    load_parent_installation(installation, tags).bot
  end

  # Public: Find the Bot associated with the given auto-generated email.
  # Bots do not have an email, however the auto-generated email used for commits
  # may contain a Bot login.
  #
  # email - a String containing the email address to test
  #
  # Returns a Bot or nil.
  def self.find_by_email(email)  # rubocop:disable GitHub/FindByDef
    return if email.blank?
    return unless email.split("@").first.end_with? Bot::LOGIN_SUFFIX

    if matches = email.match(StealthEmail::STEALTH_EMAIL_REGEX)
      where(id: matches[1]).first
    elsif matches = email.match(StealthEmail::OLD_STEALTH_EMAIL_REGEX)
      find_by_login(matches[1])
    end
  end

  # Public: Find the Bots associated with a list of emails.
  # Bots do not have an email, however the auto-generated email used for commits
  # may contain a Bot login.
  #
  # emails - an array of Strings containing the email addresses to test
  #
  # Returns a hash mapping emails to Bot objects.
  def self.find_by_emails(emails) # rubocop:disable GitHub/FindByDef
    emails = Array(emails)
    return {} if emails.empty?

    ids = []
    logins = []
    emails.each do |email|
      next unless email.split("@").first.end_with? Bot::LOGIN_SUFFIX

      if matches = email.match(StealthEmail::STEALTH_EMAIL_REGEX)
        ids.push matches[1].to_i
      elsif matches = email.match(StealthEmail::OLD_STEALTH_EMAIL_REGEX)
        logins.push matches[1]
      end
    end

    bots_by_id = Bot.where(id: ids).index_by(&:id)
    bots_by_login = Bot.where(login: logins).index_by(&:login)

    emails.map do |email|
      if matches = email.match(StealthEmail::STEALTH_EMAIL_REGEX)
        [email, bots_by_id[matches[1].to_i]]
      elsif matches = email.match(StealthEmail::OLD_STEALTH_EMAIL_REGEX)
        [email, bots_by_login[matches[1]]]
      else
        [nil, nil]
      end
    end.to_h.compact
  end

  def password_required?
    false
  end

  def email_address_required?
    false
  end

  def user?
    false
  end

  def organization?
    false
  end

  def bot?
    true
  end

  def mannequin?
    false
  end

  def restricts_oauth_applications?
    false
  end

  def billable?
    false
  end

  def can_authenticate_via_oauth?
    false
  end

  def can_authenticate_via_basic_auth?
    true
  end

  def can_authenticate_via_username_password_basic_auth?
    false
  end

  def can_have_granular_permissions?
    installation&.can_have_granular_permissions?
  end

  # Public: A Bot can never authenticate via password.
  #
  # Returns false.
  def authenticated_by_password?(password = nil)
    false
  end

  def email_spamminess_checks_enabled?
    false
  end

  def assignable_to_issues?
    false
  end

  # Internal: can the bot be subscribed to notifications
  #
  # Returns false
  def newsies_enabled?
    false
  end

  def can_own_repositories?
    false
  end

  # Public: Does the bot receive an email when the bot's account is destroyed?
  #
  # Returns false.
  def receives_confirmation_when_destroyed?
    false
  end

  # Public: We do not add bots to the users search index.
  #
  # Returns false.
  def searchable?
    false
  end

  # Don't require email verification for bots.
  def require_email_verification?
    false
  end

  # Integrations can be installed on an org or repo, and that installation can
  # delegate this Bot to act for it.
  def can_act_for_integration?
    true
  end

  # Make the primary avatar path the same as its integration.
  def primary_avatar_path
    return @primary_avatar_path if defined?(@primary_avatar_path)
    @primary_avatar_path ||=
      if integration.present?
        integration.primary_avatar_path
      else
        Integration.new.primary_avatar_path
      end
  end

  # Public: Return the list of repository IDs for the repositories that this bot
  # has been granted access to via the bot's current installation context.
  #
  # Arguments:
  #
  # min_action:    - See docs for User#associated_repository_ids.
  # including:     - See docs for User#associated_repository_ids.
  # include_oauth_restriction: - See docs for User#associated_repository_ids.
  # include_indirect_forks: - See docs for User#associated_repository_ids.
  # include_oopfs: - See docs for User#associated_repository_ids.
  # resource:      - An optional String to represent a child association, to
  #                  limit results to repositories where the bot has permissions
  #                  on that specific resource.
  # repository_ids: - An optional list of candidate repository ids. Only
  #                   repositories that are in this list and are accessible by
  #                   the integration will be returned.
  #
  # Examples:
  #
  #   associated_repository_ids(resource: "statuses")
  #   associated_repository_ids(resource: "issues")
  #
  # Returns an Array of Integer repository IDs.
  def associated_repository_ids(min_action: nil, including: nil, include_oauth_restriction: true, include_indirect_forks: true, include_oopfs: true, resource: nil, repository_ids: nil)
    return [] unless installation.present?

    installation.repository_ids(min_action: min_action, resource: resource, repository_ids: repository_ids)
  end

  # Public: Loads integration installation for the given resource and assigns it to `#installation`.
  #
  # resource - Resource for integration installation lookup. Supported types: `Repository`
  #
  # Returns nothing.
  def async_load_installation_for(resource)
    async_integration.then do |integration|
      next unless integration

      integration.async_installation_for(resource).then do |installation|
        self.installation = installation
      end
    end
  end

  # Internal: Don't apply the standard user login formatting validation to Bots.
  def validates_login_format?
    false
  end

  # Internal: Preemptively whitelist bots on bot creation.
  #
  # See https://github.com/github/platform-integrations/issues/164 for more details.
  def preemptively_whitelist
    whitelist_from_spam_flag(actor: self) if GitHub.spamminess_check_enabled?
  end

  def commit_signing_enabled?
    integration.bot_commit_signing_enabled?
  end

  private

  def validate_login
    if will_save_change_to_login? && !slug
      errors.add(:login, "is not in the correct format")
    end
  end

  def async_marketplace_listing_or_app_path
    url_helpers = Rails.application.routes.url_helpers

    if GitHub.enterprise?
      Promise.resolve("/#{to_param}")
    else
      async_marketplace_listing.then do |listing|
        if listing&.publicly_listed?
          url_helpers.marketplace_listing_path(listing.slug)
        else
          "/#{to_param}"
        end
      end
    end
  end
end
