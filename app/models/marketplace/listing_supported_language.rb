# frozen_string_literal: true

class Marketplace::ListingSupportedLanguage < ApplicationRecord::Domain::Integrations
  self.table_name = "marketplace_listing_supported_languages"

  belongs_to :language, class_name: "LanguageName", foreign_key: "language_name_id"

  def platform_type_name
    "Language"
  end
end
