# frozen_string_literal: true

module Marketplace::ListingPlan::ZuoraDependency
  ZUORA_PRODUCT_CATEGORY = "marketplace"
  ZUORA_PRODUCT_TYPE = "marketplace.listing_plan"
  SPONSORSHIP_PRODUCT_CATEGORY = "sponsorships"

  def product_uuid(billing_cycle)
    ::Billing::ProductUUID.find_by(product_type: ZUORA_PRODUCT_TYPE, product_key: id, billing_cycle: billing_cycle)
  end

  # Public: Returns the ProductRatePlanId for this plan and the given cycle in Zuora
  #
  # cycle: "year" or "month" - the billing cycle we want the zuora id for
  #
  # Returns String
  def zuora_id(cycle:)
    product_uuid(cycle).zuora_product_rate_plan_id
  end

  def zuora_charge_ids(cycle:)
    product_uuid(cycle).zuora_product_rate_plan_charge_ids
  end

  def zuora_product_id(cycle:)
    product_uuid(cycle).zuora_product_id
  end

  def zuora_product_name
    "#{listing.name}: #{name}"
  end

  # Public: The charges tied to this plan that will be synced to Zuora
  # These charges differ based on a flat rate plan vs a per unit plan
  #
  # Returns Array
  def zuora_charges
    per_unit? ? [per_unit_charge] : [flat_charge]
  end

  # Public: Syncs the plan to Zuora if its ProductUUID records don't exist, and will generate a ProductUUID
  # for each type of billing cycle
  #
  # Returns Array
  def sync_to_zuora
    GitHub::Billing::ZuoraProduct.create \
      product_type: ZUORA_PRODUCT_TYPE,
      product_key: id.to_s,
      product_name: zuora_product_name,
      charges: zuora_charges,
      custom_product_params: custom_product_params
  end

  private

  def flat_charge
    {
      type: :flat,
      prices: { year: yearly_price_in_dollars, month: monthly_price_in_dollars },
    }
  end

  def per_unit_charge
    {
      type: :unit,
      prices: { year: yearly_price_in_dollars, month: monthly_price_in_dollars },
      unit: "Seats",
    }
  end

  def custom_product_params
    base_params = { ProductCategory__c: product_category }

    name_slug_params = if listing.listable_is_sponsorable?
      {
        MaintainerName__c: listing.name,
        MaintainerSlug__c: listing.zuora_slug,
      }
    else
      {
        IntegratorName__c: listing.name,
        IntegratorSlug__c: listing.zuora_slug,
      }
    end

    base_params.merge(name_slug_params)
  end

  def product_category
    listing.listable_is_sponsorable? ? SPONSORSHIP_PRODUCT_CATEGORY : ZUORA_PRODUCT_CATEGORY
  end
end
