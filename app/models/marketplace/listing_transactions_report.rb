# frozen_string_literal: true

class Marketplace::ListingTransactionsReport
  DEFAULT_PERIOD = :week
  VALID_PERIODS = %i(day week month alltime)
  PERIOD_OFFSET_MAPPING = { day: 0, week: 6, month: 29 }
  MAX_TRANSACTIONS_SIZE = 100
  DEFAULT_PERIOD_TITLE = "Past Week"
  VALID_PERIOD_TITLES = {
    day:     "Past Day",
    week:    "Past Week",
    month:   "Past Month",
    alltime: "All-time",
  }

  attr_reader :filename, :period, :period_title

  def initialize(listing_id:, listing_slug:, period: DEFAULT_PERIOD)
    @listing_id = listing_id
    @listing_slug = listing_slug
    @filename = generate_filename(@listing_slug)
    @period = validate_period(period.to_s)
    @period_title = validate_period_title(@period)
    @transactions_count = count_total_transactions
    @truncate = true
  end

  def as_csv
    GitHub::CSV.generate do |csv|
      data = fetch_transactions
      csv << data.first.map(&:name) # use the column names as headers
      data.second.each { |transaction| csv << transaction }
    end
  end

  def as_blob
    GitHub::CSV.parse(as_csv)
  end

  def truncate(val = true)
    @truncate = val
    self
  end

  def empty?
    @transactions_count.zero?
  end

  def too_large?
    @transactions_count > MAX_TRANSACTIONS_SIZE
  end

  def valid_periods
    VALID_PERIOD_TITLES
  end

  private

  def fetch_transactions
    # GitHub.presto.run returns a [columns, rows] pair
    GitHub.presto.run(query)
  end

  def query
    %Q(
      SELECT
        DATE(created_at) AS date,
        app_name,
        user_login,
        user_id,
        user_type,
        country,
        amount_in_cents,
        renewal_frequency,
        subscribable_id as marketplace_listing_plan_id
      FROM
        hive.service_strategic_finance.marketplace_billing_transactions
      WHERE
        marketplace_listing_id = #{@listing_id}
        #{query_period_filter}
      ORDER BY
        date DESC
      #{query_limit}
    )
  end

  def count_total_transactions
    _, rows = GitHub.presto.run(count_query)
    # grab the first row then the first column, which should be the count
    rows.first&.first&.to_i || 0
  end

  def count_query
    %Q(
      SELECT
        COUNT(*)
      FROM
        hive.service_strategic_finance.marketplace_billing_transactions
      WHERE
        marketplace_listing_id = #{@listing_id}
        #{query_period_filter}
    )
  end

  def query_period_filter
    return if @period == :alltime

    offset = PERIOD_OFFSET_MAPPING[@period]
    start_date = query_date_format(latest_available_date - offset)
    end_date = query_date_format(latest_available_date)

    "AND created_at BETWEEN date '#{start_date}' AND date '#{end_date}'"
  end

  def query_date_format(date)
    date.strftime("%Y-%m-%d")
  end

  def latest_available_date
    Date.current - 2.days
  end

  def query_limit
    return unless @truncate
    "LIMIT #{MAX_TRANSACTIONS_SIZE}"
  end

  def validate_period(raw_period)
    VALID_PERIODS.find { |period| period.to_s == raw_period } || DEFAULT_PERIOD
  end

  def validate_period_title(raw_period)
    VALID_PERIOD_TITLES.fetch(raw_period, DEFAULT_PERIOD_TITLE)
  end

  def generate_filename(slug)
    "#{slug}-transactions-#{Date.today.strftime "%Y-%m"}.csv"
  end
end
