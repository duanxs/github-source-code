# frozen_string_literal: true

class Marketplace::ListingImage < ApplicationRecord::Domain::Integrations
  self.table_name = "marketplace_listing_images"

  include GitHub::Relay::GlobalIdentification
  include ::Storage::Uploadable

  areas_of_responsibility :marketplace

  # Necessary to upload images. See UploadPoliciesController#create.
  add_uploadable_policy_attributes :marketplace_listing_id
  set_cache_age 1.year
  set_content_types :images

  belongs_to :listing, class_name: "Marketplace::Listing",
                       foreign_key: "marketplace_listing_id"
  belongs_to :storage_blob, class_name: "Storage::Blob"
  belongs_to :uploader, class_name: "User", foreign_key: :uploader_id

  validates :listing, :uploader, :size, presence: true
  validate :storage_ensure_inner_asset
  validate :uploader_access, on: :create
  validates_inclusion_of :content_type, in: allowed_content_types
  validates_presence_of :name
  validate :extension_matches_content_type
  validates_inclusion_of :size, in: 1..1.megabytes

  before_validation :set_guid, on: :create
  before_destroy :storage_delete_object_and_purge_cdn

  enum state: Storage::Uploadable::STATES

  # BEGIN storage settings
  def self.storage_s3_bucket
    "github-#{Rails.env.downcase}-marketplace-image-7cfaf6"
  end

  def storage_policy(actor: nil, repository: nil)
    klass = if GitHub.storage_cluster_enabled?
      ::Storage::ClusterPolicy
    else
      ::Storage::S3Policy
    end
    klass.new(self, actor: actor, repository: nil)
  end

  def self.storage_new(uploader, blob, meta)
    new(
      storage_blob: blob,
      uploader: uploader,
      content_type: meta[:content_type],
      name: meta[:name],
      size: meta[:size],
      marketplace_listing_id: meta[:marketplace_listing_id],
    )
  end

  def self.storage_create(uploader, blob, meta)
    storage_new(uploader, blob, meta).tap do |image|
      image.update(state: :uploaded)
    end
  end

  def upload_access_grant(actor)
    Api::AccessControl.access_grant(verb: :public_site_information)
  end

  def storage_external_url(actor = nil)
    if cdn_url = GitHub.marketplace_images_cdn_url
      cdn_url + storage_s3_key(policy = nil)
    else
      url = storage_policy(actor: actor).download_url
      if GitHub.storage_cluster_enabled? && GitHub.storage_private_mode_url
        url = url.sub(GitHub.storage_cluster_url, GitHub.storage_private_mode_url)
      end
      url
    end
  end

  def creation_url
    "#{GitHub.storage_cluster_url}/marketplace-listing-images/" +
      "#{listing.id}/files"
  end

  def storage_blob_accessible?
    uploaded?
  end

  def storage_upload_path_info(policy)
    "/internal/storage/marketplace-listing-images/#{listing.id}/files"
  end

  def storage_download_path_info(policy)
    "#{storage_upload_path_info(policy)}/#{guid}"
  end

  def storage_uploadable_attributes
    { marketplace_listing_id: listing.id }
  end

  # cluster settings

  def storage_cluster_url(policy)
    "#{GitHub.storage_cluster_url}/marketplace-listing-images/" +
      "#{listing.id}/files/#{guid}"
  end

  # s3 storage settings

  def storage_s3_bucket
    self.class.storage_s3_bucket
  end

  def storage_s3_key(policy)
    "#{marketplace_listing_id}/#{guid}"
  end

  def storage_s3_access_key
    GitHub.s3_production_data_access_key
  end

  def storage_s3_secret_key
    GitHub.s3_production_data_secret_key
  end

  def storage_s3_access
    :public
  end

  def storage_s3_upload_header
    {
      "Content-Type" => content_type,
      "Cache-Control" => "max-age=2592000",
      "x-amz-meta-Surrogate-Control" => "max-age=31557600",
    }
  end

  # END storage settings

  include Instrumentation::Model

  def event_prefix
    :marketplace_listing_image
  end

  # Public: Returns true if the given User has edit access to this image.
  def editable_by?(actor)
    listing.editable_by?(actor)
  end

  private

  def storage_delete_object_and_purge_cdn
    storage_delete_object

    if GitHub.marketplace_images_cdn_url
      PurgeFastlyUrlJob.perform_later({"url" => storage_external_url})
    end
  end

  def uploader_access
    return unless listing

    unless listing.allowed_to_edit?(uploader)
      user_desc = uploader ? uploader : "anonymous user"
      errors.add :uploader,
        "#{user_desc} does not have access to upload an image to the specified Marketplace listing"
    end
  end
end
