# rubocop:disable Style/FrozenStringLiteralComment

class RequiredStatusCheck < ApplicationRecord::Domain::Repositories
  class Policy
    include MethodTiming

    # An error raised on an unexpected data edge case.
    class TooManyCommits < StandardError; end

    # Public: Check the required status check policy for one or more ref updates
    #
    # repository                    - The Repository, Gist, or Unsullied::Wiki whose refs
    #                                 are being updated
    # ref_updates                   - Array of Git::Ref::Updates
    # protected_branches_by_refname - Hash with qualified ref names as keys and relevant `ProtectedBranch`es as values
    #                                 (Passed for performance reasons to avoid duplicate lookups)
    #
    # Returns a list of RequiredStatusCheck::Policy::Decision
    def self.check(repository, ref_updates, protected_branches_by_refname)
      new(repository, ref_updates, protected_branches_by_refname).check
    end

    def initialize(repository, ref_updates, protected_branches_by_refname)
      @repository = repository
      @ref_updates = ref_updates
      @protected_branches_by_refname = protected_branches_by_refname
    end

    def check
      ref_updates.map do |ref_update|
        verify_combined_status_check_policy(ref_update)
      end
    end

    time_method :check, key: "required_status_check.policy_check.duration"

    private

    attr_reader :repository, :ref_updates, :protected_branches_by_refname

    def verify_combined_status_check_policy(ref_update)
      required_status_checks = required_status_checks_for(ref_update)
      decisions = required_status_checks.map do |status_check|
        status_decision_for(ref_update, status_check)
      end

      build_decision(ref_update, decisions)
    end

    def status_decision_for(ref_update, status_check)
      strict = protected_branch(ref_update).strict_required_status_checks_policy?
      SingleContextPolicy.check_one(repository: repository, ref_update: ref_update, status_check: status_check, strict: strict)
    end

    def build_decision(ref_update, decisions)
      can_update_ref = decisions.all?(&:policy_fulfilled?)

      if can_update_ref
        Decision.new(ref_update, can_update_ref)
      else
        message = failure_message(decisions)
        payload = instrumentation_payload(decisions)
        basis_commit = determine_rejection_basis_commit(decisions)

        Decision.new(ref_update, can_update_ref, reason: { code: :required_status_checks, message: message, basis_commit: basis_commit }, instrumentation_payload: payload)
      end
    end

    def failure_message(decisions)
      state_counts = Hash.new(0)
      successful_decisions, unsuccessful_decisions = decisions.partition(&:policy_fulfilled?)
      unsuccessful_status_checks = unsuccessful_decisions.map(&:status_check)

      unsuccessful_status_checks.each { |s| state_counts[s.state] += 1 }

      total_count = decisions.size
      bad_count = unsuccessful_decisions.size

      checks_string = "#{bad_count} of #{total_count} required status check".pluralize(total_count)

      case
      when bad_count == 1
        status_check = unsuccessful_status_checks.first
        "Required status check \"#{status_check.context}\" is #{StatusCheckRollup.adjective_state(status_check.state)}."
      when state_counts.size == 1
        is_string = "is".pluralize(bad_count)
        "#{checks_string} #{is_string} #{StatusCheckRollup.adjective_state(unsuccessful_status_checks.first.state)}."
      else
        has_string = "has".pluralize(bad_count)
        state_strings = Status::States
          .map { |s| "#{state_counts[s]} #{StatusCheckRollup.adjective_state(s)}" if state_counts[s] > 0 }
          .compact
        "#{checks_string} #{has_string} not succeeded: #{state_strings.to_sentence}."
      end
    end

    def instrumentation_payload(decisions)
      unsuccessful_status_checks = decisions.reject(&:policy_fulfilled?).map(&:status_check)

      failures = unsuccessful_status_checks.sort_by(&:context).map do |status_check|
        { status_check.context => status_check.state }
      end

      { failures_json: GitHub::JSON.encode(failures) }
    end

    # Due to logic that checks several places for statuses (see
    # SingleContextPolicy), the commit used as a basis for the decision may not
    # be the RefUpdate's after_oid.  Extract the commit that was the actual
    # rejection basis so it can be passed up the chain and exposed in the UI.
    def determine_rejection_basis_commit(decisions)
      unsuccessful_status_decisions = decisions
        .reject(&:policy_fulfilled?)
        .grep(RequiredStatusCheck::Policy::SingleContextPolicy::Decision)
      commits = unsuccessful_status_decisions.map(&:commit).compact.uniq
      if commits.size > 1
        # This scenario is unexpected, send a warning so we can debug
        # and just choose an arbitrary commit to return as the basis.
        boom = TooManyCommits.new(commits.map(&:sha).join(" "))
        boom.set_backtrace(caller)
        Failbot.report_trace(boom, repo_id: @repository.id)
      end
      commits.first
    end

    def protected_branch(ref_update)
      protected_branches_by_refname[ref_update.refname]
    end

    def required_status_checks_for(ref_update)
      @required_status_checks ||= required_status_checks_by_refname
      @required_status_checks[ref_update.refname] || []
    end

    def required_status_checks_by_refname
      relevant_branches = ref_updates.each_with_object([]) do |ref_update, branches|
        branch = protected_branch(ref_update)
        next if branch.nil? || !branch.required_status_checks_enabled?
        branches << branch
      end

      status_checks_by_branch = RequiredStatusCheck.for_branches(relevant_branches).group_by(&:protected_branch)

      ref_updates.each_with_object({}) do |ref_update, hash|
        hash[ref_update.refname] = status_checks_by_branch[protected_branch(ref_update)]
      end
    end
  end
end
