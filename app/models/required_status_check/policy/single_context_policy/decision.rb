# frozen_string_literal: true

class RequiredStatusCheck < ApplicationRecord::Domain::Repositories
  class Policy
    class SingleContextPolicy
      class Decision
        # A Boolean indicating if the policy has passed or not
        attr_reader :policy_fulfilled

        # A Status, CheckRun or RequiredStatusCheck with an expected or failure
        # state that led to this Decision.
        # The value is nil in case the policy is fulfilled
        attr_reader :status_check

        def initialize(policy_fulfilled:, status_check: nil)
          @policy_fulfilled = policy_fulfilled
          @status_check = status_check
        end

        def commit
          if status_check.respond_to?(:commit)
            status_check.commit
          end
        end

        alias_method :policy_fulfilled?, :policy_fulfilled
      end
    end
  end
end
