# frozen_string_literal: true

class RepositoryAdvisoryComment < ApplicationRecord::Collab
  include GitHub::Relay::GlobalIdentification
  include GitHub::UserContent
  include EmailReceivable
  include Reaction::Subject::RepositoryContext
  include UserContentEditable
  include NotificationsContent::WithCallbacks
  include RepositoryAdvisory::AfterPublication

  after_commit :subscribe_and_notify, on: :create
  after_commit :update_subscriptions_and_notify, on: :update

  extend GitHub::Encoding
  force_utf8_encoding :body

  belongs_to :repository_advisory
  belongs_to :user

  has_many :reactions, as: :subject

  validates :repository_advisory, presence: true
  validates :user, presence: true
  validates :body, presence: true

  validate :author_must_have_write_permission
  validate :cannot_be_created_after_advisory_publication, on: :form_submission

  def repository
    async_repository.sync
  end

  def async_repository
    async_repository_advisory.then(&:async_repository)
  end

  def readable_by?(viewer)
    async_readable_by?(viewer).sync
  end

  def async_readable_by?(viewer)
    async_repository_advisory.then do |advisory|
      advisory.async_writable_by?(viewer).then do |advisory_writable|
        # If the viewer is an advisory collaborator, they can see any comment.
        next true if advisory_writable

        advisory.async_readable_by?(viewer).then do |advisory_readable|
          # If the viewer is not an advisory collaborator, they must at the very
          # least be able to read the advisory.
          next false unless advisory_readable

          # At this point, we know the viewer is not an advisory collaborator
          # but can read the advisory. Now we only need to check that the
          # comment is not considered internal.
          async_internal?.then { |internal| !internal }
        end
      end
    end
  end

  def async_viewer_can_delete?(viewer)
    async_viewer_can_update?(viewer)
  end

  def async_viewer_can_update?(viewer)
    async_viewer_cannot_update_reasons(viewer).then(&:empty?)
  end

  def async_viewer_cannot_update_reasons(viewer)
    return Promise.resolve([:login_required]) unless viewer

    async_user.then do |user|
      # Only allow staff to update or delete comments by the special staff user.
      next [:insufficient_access] if user&.staff_user? && !viewer.site_admin?

      async_repository_advisory.then do |advisory|
        advisory.async_writable_by?(viewer).then do |advisory_writable|
          advisory_writable ? [] : [:insufficient_access]
        end
      end
    end
  end

  def async_internal?
    async_after_publication?.then do |after_publication|
      # All pre-publication comments are considered internal.
      next true unless after_publication

      # Even some comments that are posted after publication are considered
      # internal, like those authored by the special staff user regarding the
      # maintainers' CVE request.
      #
      # See: https://github.com/github/dsp-security-workflows/issues/826
      async_user.then do |user|
        user ? user.staff_user? : false
      end
    end
  end

  def internal?
    async_internal?.sync
  end

  # Notifications

  def message_id
    "<#{repository.name_with_owner}/repository-advisories/#{repository_advisory.id}/comments/#{id}@#{GitHub.urls.host_name}>"
  end

  def get_notification_summary
    list = Newsies::List.new("Repository", repository_advisory.repository_id)
    GitHub.newsies.web.find_rollup_summary_by_thread(list, repository_advisory)
  end

  def notifications_thread
    repository_advisory
  end

  def notifications_author
    user
  end

  def async_notifications_list
    async_repository
  end

  def async_organization
    async_repository.then(&:async_organization)
  end

  def async_entity
    async_repository
  end

  def entity
    repository
  end

  def permalink(include_host: true)
    "#{repository_advisory.permalink(include_host: include_host)}#advisory-comment-#{id}"
  end

  def unsubscribable_users(users)
    repository_advisory.unsubscribable_users(users)
  end

  def author_subscribe_reason
    "comment"
  end

  private

  def author_must_have_write_permission
    # The special staff user is allowed to author comments on the advisory
    # timeline without have access to the advisory itself.
    return if user&.staff_user?

    if repository_advisory.present? && !repository_advisory.writable_by?(user)
      errors.add(:user, "must have write permission")
    end
  end

  def cannot_be_created_after_advisory_publication
    if repository_advisory&.published?
      errors.add(:base, "The advisory has been published, so comments are closed.")
    end
  end
end
