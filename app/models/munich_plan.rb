# frozen_string_literal: true

class MunichPlan < SimpleDelegator

  ACTIONS_CHANGE_DATETIME = GitHub::Billing.timezone.parse("2020-05-14")
  ACTIONS_CHANGE_DATE = ACTIONS_CHANGE_DATETIME.to_date
  RELEASE_DATE = GitHub::Billing.timezone.parse("2020-04-14").to_date

  def self.wrap(plan, target:)
    if plan.present? && target.respond_to?(:flipper_id)
      new(plan, target: target)
    else
      plan
    end
  end

  def initialize(plan, target:)
    @target = target
    super(plan)
  end

  def base_units
    if effective_plan["munich_changes"] && GitHub.flipper[:munich_pricing].enabled?(target)
      effective_plan["munich_changes"].fetch("base_units", super)
    else
      super
    end
  end

  def cost
    if effective_plan["munich_changes"] && GitHub.flipper[:munich_pricing].enabled?(target)
      effective_plan["munich_changes"].fetch("cost", super)
    else
      super
    end
  end

  def unit_cost
    if effective_plan["munich_changes"] && GitHub.flipper[:munich_pricing].enabled?(target)
      effective_plan["munich_changes"].fetch("unit_cost", super)
    else
      super
    end
  end

  def shared_storage_included_megabytes
    if effective_plan["munich_changes"] && GitHub.flipper[:munich_pricing_pro_metered_units].enabled?(target)
      shared_storage = effective_plan["munich_changes"].fetch("shared_storage", {})
      if shared_storage.present?
        shared_storage.fetch("included_megabytes", 0)
      else
        super
      end
    else
      super
    end
  end

  def package_registry_included_bandwidth
    if effective_plan["munich_changes"] && GitHub.flipper[:munich_pricing_pro_metered_units].enabled?(target)
      package_registry = effective_plan["munich_changes"].fetch("package_registry", {})
      if package_registry.present?
        package_registry.fetch("included_bandwidth_in_gigabytes", 0)
      else
        super
      end
    else
      super
    end
  end

  def actions_included_private_minutes
    return super if MunichPlan::ACTIONS_CHANGE_DATE > GitHub::Billing.today
    if target.first_day_in_metered_cycle < MunichPlan::ACTIONS_CHANGE_DATE
      return github_actions_without_munich.fetch("included_private_minutes", 0)
    end

    if effective_plan["munich_changes"] && GitHub.flipper[:munich_pricing].enabled?(target)
      github_actions = effective_plan["munich_changes"].fetch("github_actions", {})
      if github_actions.present?
        github_actions.fetch("included_private_minutes", 0)
      else
        super
      end
    else
      super
    end
  end

  # Reimplement other helpers since the methods don't delegate to the decorator when called
  def cost_in_cents
    cost * 100
  end

  def yearly_cost_in_cents
    yearly_cost * 100
  end

  def unit_cost_in_cents
    unit_cost * 100
  end

  def yearly_unit_cost_in_cents
    yearly_unit_cost * 100
  end

  private

  attr_reader :target
end
