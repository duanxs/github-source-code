# frozen_string_literal: true

# The runner for reporting on legacy GitHub Services installed on a GitHub
# Enterprise installation.
class LegacyGitHubServicesReporter

  class Error < StandardError
  end

  # Create a new legacy GitHub Services reporter.
  #
  # output_io:       - An open IO handle to print the CSV results to. Usually
  #                    either a file in /tmp, or STDOUT.
  # include_header:  - Whether or not to include the CSV header in the output.
  #                    Defaults to false.
  # active_only:     - Whether or not to only report on active GitHub Services
  #                    installed. Defaults to false.
  # include_config:  - Whether or not to include the masked configuration data
  #                    for the installed GitHub Service. Defaults to false.
  def initialize(output_io:, include_header: false, active_only: false, include_config: false)
    raise Error, "Reporting on legacy GitHub Services is only supported on GitHub Enterprise" unless GitHub.enterprise?
    @output = output_io
    @include_header = include_header
    @active_only = active_only
    @include_config = include_config
  end

  # Prints a summary in CSV format to @output. Each row contains:
  #
  # - Installed GitHub Service ID
  # - Service name
  # - Active?
  # - Creator
  # - Repository ID
  # - Repository full "name with owner"
  # - Repository last pushed at timestamp
  # - Masked config data (if @include_config)
  def run
    results = filter_and_sort
    rows = []
    results.each do |hook|
      row = [
        hook.id,
        hook.name,
        hook.active?,
        hook.creator_name,
        (hook.repo_hook? ? hook.installation_target.id : ""),
        (hook.repo_hook? ? hook.installation_target.full_name : ""),
        (hook.repo_hook? ? hook.installation_target.pushed_at || "" : ""),
      ]
      row << hook.masked_config&.to_json || "" if @include_config
      rows << row
    end

    csv = GitHub::CSV.new(@output, headers: header, write_headers: @include_header)
    rows.each { |row| csv << row }
  end

  private

  def header
    config = @include_config ? ",masked_config_data" : ""
    "id,name,active,creator,repository_id,repository_full_name,repository_pushed_at#{config}"
  end

  def filter_and_sort
    # Legacy GitHub Services hooks always have a name other than "web"
    # See Hook#legacy_service?
    scope = Hook.where("name <> 'web'")
    scope = scope.active if @active_only
    scope.ordered
  end
end
