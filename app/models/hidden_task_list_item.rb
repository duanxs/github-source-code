# frozen_string_literal: true

class HiddenTaskListItem < ApplicationRecord::Collab
  belongs_to :user, required: true
  belongs_to :removable, polymorphic: true, required: true
end
