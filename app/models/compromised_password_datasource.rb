# frozen_string_literal: true

class CompromisedPasswordDatasource < ApplicationRecord::Collab
  has_and_belongs_to_many :compromised_passwords, readonly: true

  scope :not_qintel, -> { where.not(name: "qintel") }

  validates :name,    presence: true
  validates :version, presence: true, uniqueness: { scope: :name, case_sensitive: false }

  LOAD_BATCH_SIZE = 100
  READ_BATCH_SIZE = 1000

  # Create a new datasource and load its credentials.
  #
  # name:           - The String name of the datasource.
  # version:        - The String version of the datasource.
  # sha1_passwords: - An Enumerator of String hex SHA1 password hashes.
  #
  # Returns the new CompromisedPasswordDatasource instance.
  def self.store_passwords(name:, version:, sha1_passwords:)
    datasource = retry_on_find_or_create_error do
      where(name: name, version: version).first || create!(name: name, version: version)
    end

    sha1_passwords.each_slice(LOAD_BATCH_SIZE) do |batch|
      rows = GitHub::SQL::ROWS(batch.map { |sha| [sha] })

      throttle_with_retry(max_retry_count: 5) do
        retry_on_deadlock do
          github_sql.new(<<-SQL, rows: rows).run
            INSERT IGNORE INTO compromised_passwords (sha1_password)
            VALUES :rows
          SQL
        end
      end

      stat_credentials_loaded(name, version, batch.count)

      throttle_with_retry(max_retry_count: 5) do
        retry_on_deadlock do
          github_sql.new(<<-SQL, id: datasource.id, shas: batch).run
            INSERT IGNORE INTO compromised_password_datasources_passwords
            (compromised_password_datasource_id, compromised_password_id)
            SELECT :id, cp.id
            FROM compromised_passwords AS cp
            WHERE cp.sha1_password IN :shas
          SQL
        end
      end

      stat_credentials_linked(name, version, batch.count)
    end

    datasource
  end

  # Create a new datasource and load its credentials.
  #
  # compromised_records: - An Enumerator of arrays ["username", "password"]
  #
  # Returns the new CompromisedPasswordDatasource instance.
  def self.check_for_compromise(compromised_records:, name:, version:)
    CompromisedPasswordDatasource.throttle_with_retry(max_retry_count: 8) do
      compromised_records.each_slice(READ_BATCH_SIZE) do |batch|
        stat_credentials_processed(name, version, batch.count)

        logins = []
        emails = []

        batch.each do |username, password|
          next if username.blank?
          if username.include?("@")
            emails << username.downcase
          else
            logins << username.downcase
          end
        end

        valid_emails = User.select("users.*, user_emails.email as user_email").
          joins(:emails).
          where(user_emails: {email: emails})

        valid_logins = User.select("users.*, NULL as user_email").
          where(login: logins)

        valid_users = User.find_by_sql("SELECT * FROM (#{valid_emails.to_sql} UNION #{valid_logins.to_sql}) AS users")

        valid_users.each do |user|
          stat_email_match(name, version, user)
          next if user.password_check_metadata.exact_match?
          batch.select do |username, password|
            next if username.blank? || password.blank?
            username.downcase == user.login&.downcase || username.downcase == user.user_email&.downcase
          end.each do |_, password|
            if user.authenticated_by_password?(password)
              stat_exact_match(name, version, user)
              user.mark_compromised_via_direct_match(password: password, name: name, version: version)
              AccountMailer.username_and_password_compromised(user).deliver_later
              break
            end
          end
        end
      end
    end
  end

  # Sets the time that the import of compromised
  # records finished at
  def mark_import_as_finished!
    self.update!(import_finished_at: Time.now)
    GitHub.dogstats.increment("auth.compromised_password.import_finished", tags: [
      "version:#{version}",
      "datasource:#{name}",
    ])
  end

  def self.stat_exact_match(name, version, user)
    GitHub.dogstats.increment("auth.compromised_password.exact_match", tags:  [
      "employee:#{user&.employee?}",
      "spammy:#{user&.spammy?}",
      "tfa_enabled:#{user&.two_factor_authentication_enabled?}",
      "version:#{version}",
      "datasource:#{name}",
    ])
  end

  def self.stat_email_match(name, version, user)
    GitHub.dogstats.increment("auth.compromised_password.email_match", tags: [
      "employee:#{user&.employee?}",
      "spammy:#{user&.spammy?}",
      "tfa_enabled:#{user&.two_factor_authentication_enabled?}",
      "version:#{version}",
      "datasource:#{name}",
    ])
  end

  def self.stat_credentials_loaded(name, version, count)
    GitHub.dogstats.count("auth.compromised_password.credentials_loaded", count, tags: [
      "version:#{version}",
      "datasource:#{name}",
    ])
  end

  def self.stat_credentials_linked(name, version, count)
    GitHub.dogstats.count("auth.compromised_password.credentials_linked", count, tags: [
      "version:#{version}",
      "datasource:#{name}",
    ])
  end

  def self.stat_credentials_processed(name, version, count)
    GitHub.dogstats.count("auth.compromised_password.credentials_processed", count, tags: [
      "version:#{version}",
      "datasource:#{name}",
    ])
  end
end
