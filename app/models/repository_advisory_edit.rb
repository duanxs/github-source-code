# rubocop:disable Style/FrozenStringLiteralComment

class RepositoryAdvisoryEdit < ApplicationRecord::Domain::Repositories
  include FilterPipelineHelper
  include Instrumentation::Model
  include UserContentEdit::Core

  belongs_to :repository_advisory

  alias_attribute :user_content_id, :repository_advisory_id
  alias_method :user_content, :repository_advisory
  alias_method :async_user_content, :async_repository_advisory

  def user_content_type
    "RepositoryAdvisory"
  end

  def global_relay_id
    Platform::Helpers::NodeIdentification.to_global_id(platform_type_name, user_content_edit_id || "RepositoryAdvisoryEdit:#{id}")
  end
end
