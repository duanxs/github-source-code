# frozen_string_literal: true

class SpamDatasourceEntry < ApplicationRecord::Domain::Spam
  belongs_to :spam_datasource

  validates_presence_of :spam_datasource
  validates_presence_of :value

  # `additional_context` varchar(255) DEFAULT NULL,
end
