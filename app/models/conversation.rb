# rubocop:disable Style/FrozenStringLiteralComment

# The Conversation subsystem is responsible for enabling conversations around
# any resource on GitHub.com with a URL.
#
# See also https://github.com/github/github/issues/14723.
#
# Definitions:
#
#  author:  A User who started the conversation
#  subject: The subject of conversation - i.e. an Issue, PullRequest, etc...
#           Note that not all of these are implemented into the Conversation system as of yet.
#  context: A context is any top level entity that can be owned by a User (or Organization) and
#           can be referred to within the name with owner URL structure.
class Conversation < ApplicationRecord::Domain::Repositories
  validates_presence_of :author_id, :subject_id, :subject_type
  validates_presence_of :context_id, :context_type

  belongs_to :author, class_name: "User"

  belongs_to :subject, polymorphic: true
  belongs_to :context, polymorphic: true
  alias_attribute :user, :author

  before_validation :assign_author_from_subject, on: :create
  before_validation :assign_context_from_subject, on: :create
  after_create :auto_number, if: :number_subject?
  after_create :assign_number_to_subject, if: :number_subject?

  # Returns a Conversation record for the context and number.
  def self.with_context_and_number(context, number)
    return unless context
    includes(:subject).where(
      context_type: context.class.name,
      context_id: context.id,
      number: number,
    ).first
  end

  # Public: Filters mixture of conversations to ones that have been read by
  # the User.
  #
  # user     - The User.
  # subjects - Collection of mixed Issues/Discussions instances to lookup read
  #            status for.
  #
  # Returns a Set of Integer IDs that have been read by the User.
  def self.read_for(user, subjects)
    raise TypeError if !user.kind_of?(User)
    subject_ids = subjects.collect(&:id)

    unread_response = GitHub.newsies.web.by_conversations(user, subjects) do |notification_entry|
      notification_entry.unread?
    end
    Set.new(subject_ids - unread_response.collect(&:id))
  end

  # Public: Finds all users that have been mentioned within the conversation.
  #
  # Returns Array of Users mentioned throughout conversation
  def mentioned_users
    @mentioned_users ||= User.where(id: mentioned_user_ids).to_a
  end

  # Public: Finds all of the user ids that have been mentioned within the conversation.
  #
  # Returns Array of Integers for mentioned users within the conversation.
  def mentioned_user_ids
    return @mentioned_user_ids if defined?(@mentioned_user_ids)

    mentioned_user_ids = Set.new

    list   = subject.notifications_list
    thread = subject.notifications_thread
    subscriber_set = GitHub.newsies.subscriber_set_for(list: list, thread: thread)

    subscriber_set.subscribers.each do |subscriber|
      if subscriber.reason == "mention"
        mentioned_user_ids << subscriber.user_id
      end
    end

    @mentioned_user_ids = mentioned_user_ids.to_a
  end

  def lock
    update!(state: "locked")
  end

  def unlock
    update!(state: "published")
  end

  private

  # Internal
  def assign_author_from_subject
    self.author_id = subject.user_id if subject
  end

  # Internal
  def assign_context_from_subject
    if subject && subject.respond_to?(:entity)
      self.context = subject.entity
    else
      self.context = subject.repository if subject
    end
  end

  # Internal: returns true if the subject is numbered.
  def number_subject?
    %w[Issue].include? subject_type
  end

  # Validates the number.
  #
  # The number is valid when it is nonzero and isn't already taken.
  #
  # Returns true if the number is valid.
  def valid_number?
    non_zero_number? && number_is_available?
  end

  def non_zero_number?
    number.to_i > 0
  end

  def number_is_available?
    !self.class.exists?(["context_type = ? and context_id = ? and number = ? and id != ?",
                          context_type, context_id, number, id])
  end

  # Internal: handles auto-numbering the conversation.
  #
  # Sets the number to the next valid, available number in the sequence for the
  # current context.
  #
  # If number is already set and valid, updates the sequence.
  # If it's set to a number that is already taken, it will fall back to the next
  # available number in the sequence.
  #
  # NOTE: Requires that the context's sequence already exist.
  #
  # Returns nothing.
  def auto_number
    self.number = subject.number if subject && subject.respond_to?(:number?) && subject.number?
    Failbot.push number: number,
      subject_type: subject.class.name,
      subject_id: subject.id,
      id: id do
        if number? && valid_number?
          Sequence.set(context, number) if number > Sequence.get(context)
        else
          self.number = Sequence.next(context)
        end
      end
    self.class.where(id: id).update_all(number: number)
  end

  # Internal: assign the Sequence generated number to the Subject
  #
  # NOTE: We *do* want to raise here if for some reason the Issue update fails,
  # so that the entire transaction is rolled back
  #
  # Returns nothing. Raises ActiveRecord::Rollback if unable to update subject.
  def assign_number_to_subject
    if subject.respond_to?(:number=)
      subject.number = number
      unless subject.class.where(id: subject_id).update_all(number: number) == 1
        raise ActiveRecord::Rollback
      end
    end
  end
end
