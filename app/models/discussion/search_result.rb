# frozen_string_literal: true

class Discussion
  class SearchResult
    # Public: Load discussions using MySQL or ElasticSearch (and MySQL)
    #
    # query - parsed search query, e.g. ["foo", [:author, "iancanderson"]]
    # page - page to load as a string or integer, e.g. "2"
    # per_page - discussions per page, e.g. "20"
    # repo - Repository to search within, if any
    #
    # Returns a paginated list of Discussions.
    def self.search(**args)
      new(**args).search
    end

    def initialize(query:, page:, per_page:, current_user:, repo: nil, remote_ip: nil, user_session: nil)
      @query = query
      @page = (page || 1).to_i
      @per_page = (per_page || 25).to_i
      @repo = repo
      @current_user = current_user
      @remote_ip = remote_ip
      @user_session = user_session
    end

    def search
      discussion_query = ::Search::Queries::DiscussionQuery.new(
        phrase: sanitized_query_string,
        repo_id: repo&.id,
        page: page,
        per_page: per_page,
        current_user: current_user,
        remote_ip: remote_ip,
        user_session: user_session
      )
      es = begin
        discussion_query.execute
      rescue Elastomer::Client::Error => boom
        Failbot.report(boom.with_redacting!)
        case discussion_query
        when Search::Queries::GeyserCodeQuery
          Search::GeyserResults.empty
        else
          Search::Results.empty
        end
      end

      WillPaginate::Collection.create(page, per_page, es.total) do |pager|
        pager.replace(es.models)
      end
    end

    private

    attr_reader :current_user, :page, :per_page, :query, :repo, :remote_ip,
      :user_session

    def sanitized_query_string
      @sanitized_query_string ||= Search::Queries::DiscussionQuery.stringify(query)
    end
  end
end
