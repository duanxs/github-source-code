# frozen_string_literal: true

class TreeEntry
  module RenderHelper
    def self.raw_blob_url(viewer, repository, ref, path, expires_key: nil, host: nil, query: {})
      expires_key ||= :render
      query ||= {}

      needs_a_token = (repository && repository.private?) ||
                      (GitHub.enterprise? && GitHub.private_mode_enabled?)
      if viewer && needs_a_token
        scope = RawBlob.scope(repository, ref, path)
        query[:token] = RawBlob.token(viewer, repository, scope, expires_key)
      end

      query[:lab] = true if GitHub.employee_unicorn?

      host ||= GitHub.urls.raw_host_name || GitHub.render_raw_host_name
      prefix = if host
        "#{GitHub.scheme}://#{host}"
      else
        "#{GitHub.scheme}://#{GitHub.host_name}/raw"
      end

      url = [
        prefix,
        repository.name_with_owner,
        *ref.split("/").map! { |p| UrlHelper.escape_path(p) },
        *path.split("/").map! { |p| UrlHelper.escape_path(p) },
      ].join("/")
      url << "?#{query.to_query}" unless query.empty?
      url
    end

    def self.raw_gist_url(viewer, gist, commit_oid, path)
      file = path.sub(/\A\//, "")

      if GitHub.private_mode_enabled?
        scope = "Gist:#{gist.user_param}/#{gist.repo_name}/#{commit_oid}/#{file}"
        token = viewer.signed_auth_token(scope: scope, expires: RawBlob::EXPIRES[:blob].from_now)
      end

      scheme = GitHub.scheme
      host = GitHub.subdomain_isolation ? GitHub.urls.raw_host_name : GitHub.host_name

      path = [
        GitHub.subdomain_isolation? ? nil : %w(raw),
        "gist",
        UrlHelper.escape_path(gist.user_param),
        UrlHelper.escape_path(gist.repo_name),
        "raw",
        UrlHelper.escape_path(commit_oid),
        UrlHelper.escape_path(file),
      ].compact.join("/")

      query_string = token ? "?#{{token: token}.to_query}" : ""

      "#{scheme}://#{host}/#{path}#{query_string}"
    end

    def self.lfs_blob_url(viewer, repository, ref, path)
      if !GitHub.storage_cluster_enabled?
        return media_blob_url(viewer, repository, ref, path)
      end

      path_info = [
        "raw_lfs",
        repository.name_with_owner,
        ref.split("/").map! { |p| UrlHelper.escape_path(p) }.join("/"),
        path.split("/").map! { |p| UrlHelper.escape_path(p) }.join("/"),
      ].join("/")

      url = "#{GitHub.storage_cluster_url}/#{path_info}".dup
      return url if repository.public? && !GitHub.private_mode_enabled?
      scope = RawBlob.scope(repository, ref, path)
      token = RawBlob.token(viewer, repository, scope, :render)
      url << "?#{{token: token}.to_query}"
    end

    def self.media_blob_url(viewer, repository, ref, path)
      url = "%s/media/%s/%s/%s" % [
        GitHub.alambic_assets_url,
        repository.name_with_owner,
        ref.split("/").map! { |p| UrlHelper.escape_path(p) }.join("/"),
        path.split("/").map! { |p| UrlHelper.escape_path(p) }.join("/"),
      ]
      return url if repository.public? && !GitHub.private_mode_enabled?

      scope = Media::Blob.auth_scope_options(repository, ref, path)
      token = Media::Blob.auth_token(viewer, scope)
      url << "?#{{token: token}.to_query}"
    end

    def self.solid?(blob)
      blob.extname.downcase == ".stl"
    end

    def self.geojson?(blob)
      extension = blob.extname.downcase
      if %w(.geojson .topojson).include?(extension)
        GitHub.dogstats.increment "blob", tags: ["action:geojson", "type:.geo#{extension}"]
        true
      elsif extension == ".json"
        begin
          # If the blob doesn't have a repository, we won't be able to load the
          # data to check if its a GeoJSON .json file
          if !(blob && blob.repository)
            false
          elsif blob && blob.data =~ %r{"type"\s*:\s*"(featurecollection|topology|geometrycollection)"}i
            GitHub.dogstats.increment "blob", tags: ["action:geojson", "type:geo.json"]
            true
          end
        end
      end
    end

    # Topojson is a subset of geojson that we can't diff, yet.
    def self.topojson?(blob)
      extension = blob.extname.downcase
      if extension == ".topojson"
        true
      elsif extension == ".json"
        begin
          # If the blob doesn't have a repository, we won't be able to load the
          # data to check if its a GeoJSON .json file
          if !(blob && blob.repository)
            false
          elsif blob && blob.data =~ %r{"type"\s*:\s*"topology"}i
            true
          end
        end
      end
    end

    def self.img?(blob)
      blob.image?
    end

    def self.psd?(blob)
      blob.extname.downcase == ".psd"
    end

    def self.svg?(blob)
      blob.extname.downcase == ".svg"
    end

    def self.pdf?(blob)
      blob.extname.downcase == ".pdf"
    end

    def self.ipynb?(blob)
      blob.extname.downcase == ".ipynb"
    end

    # Internal: Detect render file type for tree entry file.
    #
    # Returns symbol file type or nil.
    def render_type
      return nil unless GitHub.render_enabled?

      return @render_type if defined? @render_type

      if symlink?
        return @render_type = nil
      end

      @render_type = if RenderHelper.solid?(self)
        :solid
      elsif RenderHelper.pdf?(self)
        :pdf
      elsif RenderHelper.topojson?(self)
        :topojson
      elsif RenderHelper.geojson?(self)
        :geojson
      elsif RenderHelper.ipynb?(self)
        :ipynb
      elsif RenderHelper.psd?(self)
        :psd
      elsif RenderHelper.svg?(self)
        :svg
      elsif RenderHelper.img?(self)
        :img
      else
        nil
      end

      if @render_type && GitHub.render_type_filter.any?
        # If filters are set, require type to be whitelisted
        # Primarily used for Enterprise where some formats are disabled
        unless GitHub.render_type_filter.include?(@render_type.to_s)
          @render_type = nil
        end
      end

      @render_type
    end

    # From Render::App
    #   https://github.com/github/render/blob/master/app/render.rb
    RENDER_COMMON_TYPES = [:solid, :geojson, :psd, :svg]
    RENDER_VIEW_TYPES = RENDER_COMMON_TYPES + [:pdf, :ipynb, :topojson]
    RENDER_DIFF_TYPES = RENDER_COMMON_TYPES + [:img]
    RENDER_PREVIEW_TYPES = [:geojson, :topojson]
    RENDER_IMAGE_TYPES = [:psd, :svg, :img]

    # Public: Detect render file for a given display context.
    #
    # Not all file types support all display contexts. For an example, PDF can
    # be viewed readonly but not diffed.
    #
    # Returns symbol file type or nil.
    def render_file_type_for_display(display_type, viewer: nil)
      return nil unless GitHub.render_enabled?

      type = render_type

      case display_type
      when :view
        type if RENDER_VIEW_TYPES.include?(type)
      when :diff
        type if RENDER_DIFF_TYPES.include?(type)
      when :preview
        type if RENDER_PREVIEW_TYPES.include?(type)
      else
        type
      end
    end

    # Returns URI.
    def render_url_for_display(display_type, commit_oid:, viewer: nil)
      if type = render_file_type_for_display(display_type, viewer: viewer)
        if display_type == :preview
          content_url = "client://"
        elsif repository.is_a?(Gist)
          content_url = RenderHelper.raw_gist_url(viewer, repository, commit_oid, path)
        elsif git_lfs?
          content_url = RenderHelper.lfs_blob_url(viewer, repository, commit_oid, path)
        else
          content_url = RenderHelper.raw_blob_url(viewer, repository, commit_oid, path, expires_key: :render, host: GitHub.render_raw_host_name)
        end

        query = {
          enc_url: render_encode_url(content_url),
          path: path,
          commit: commit_oid,
          nwo: repository.nwo,
          repository_id: repository.id,
          repository_type: repository.class.name,
        }
        URI.parse("#{GitHub.render_host_url}/view/#{type}?#{query.to_param}")
      end
    end

    private

    # Encode the URL as a hex string to never have to worry about
    # any character encoding not matching what we can send in URL
    #
    # Returns a String
    def render_encode_url(url)
      url.to_s.unpack1("H*")
    end
  end
end
