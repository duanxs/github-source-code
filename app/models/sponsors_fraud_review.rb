# frozen_string_literal: true

class SponsorsFraudReview < ApplicationRecord::Collab
  belongs_to :sponsors_listing
  belongs_to :reviewer, class_name: "User", required: false
  has_many :fraud_flagged_sponsors, autosave: true, dependent: :destroy

  after_create_commit :disable_payouts_for_listing
  after_create_commit :instrument_creation
  after_commit :instrument_state_change, if: :saved_change_to_state?

  enum state: [:pending, :resolved, :flagged]

  # Public: Resolves this fraud review and reenables payouts, if they were
  #         disabled due to being fraud flagged.
  #
  # Returns a Boolean.
  def resolve(actor:)
    return true if resolved?

    if actor.blank?
      errors.add(:reviewer, "must be present")
      return false
    end

    unless pending?
      errors.add(:state, "must be pending")
      return false
    end

    success = update(
      state: :resolved,
      reviewer: actor,
      reviewed_at: Time.now,
    )

    if success && sponsors_listing.completed_payout_probation?
      sponsors_listing.enable_payouts
    end

    success
  end

  # Public: Flags this fraud review as fraudulent and keeps payouts disabled.
  #
  # Returns a Boolean.
  def flag(actor:)
    return true if flagged?

    if actor.blank?
      errors.add(:reviewer, "must be present")
      return false
    end

    unless pending?
      errors.add(:state, "must be pending")
      return false
    end

    update(
      state: :flagged,
      reviewer: actor,
      reviewed_at: Time.now,
    )
  end

  # Public: Reverts a review to the pending state, and disables payouts again
  #         until a new review is completed.
  #
  # Returns a Boolean.
  def revert_to_pending(actor:)
    return true if pending?

    if actor.blank?
      errors.add(:reviewer, "must be present")
      return false
    end

    success = update(
      state: :pending,
      reviewer: nil,
      reviewed_at: nil,
    )

    disable_payouts_for_listing if success

    success
  end

  private

  # Private: Enqueues a job to disable payouts for a listing until we can review
  #          it for fraud.
  def disable_payouts_for_listing
    return unless sponsors_listing.completed_payout_probation?
    sponsors_listing.disable_payouts
  end

  def instrument_creation
    GlobalInstrumenter.instrument("sponsors.sponsors_fraud_review_create", {
      fraud_review: self,
      listing: sponsors_listing,
      sponsorable: sponsors_listing.sponsorable,
    })
  end

  def instrument_state_change
    GlobalInstrumenter.instrument("sponsors.sponsors_fraud_review_state_change", {
      fraud_review: self,
      previous_state: state_before_last_save,
      reviewer: reviewer,
      sponsorable: sponsors_listing.sponsorable,
      reviewed_at: reviewed_at,
    })
  end
end
