# frozen_string_literal: true

class SponsorshipNewsletter < ApplicationRecord::Collab
  self.ignored_columns = %w(maintainer_type maintainer_id)
  extend GitHub::Encoding
  force_utf8_encoding :body, :subject

  include Rails.application.routes.url_helpers
  include GitHub::Relay::GlobalIdentification

  belongs_to :sponsorable, polymorphic: true
  belongs_to :author, foreign_key: :author_id, class_name: "User"
  has_many :sponsorship_newsletter_tiers, autosave: true, dependent: :destroy
  has_many :sponsors_tiers, through: :sponsorship_newsletter_tiers, dependent: :destroy

  validates :author_id, :sponsorable_type, :sponsorable_id, presence: true
  validate :published_newsletter_cannot_become_draft

  after_commit :after_publish, on: [:create, :update], if: proc { saved_change_to_state? && published? }

  enum sponsorable_type: { User: 0 }, _prefix: true

  enum state: {
    draft: 0,
    published: 1,
  }

  PER_PAGE = 10

  # Public: The sponsors who have access to this newsletter.
  #
  # emailable_only - A Boolean indicating if this should return sponsors who have
  #                  opted into receiving email newsletters.
  #
  # Returns a Promise<Array[User]>.
  def async_sponsors_with_access(emailable_only: false)
    Promise.all([async_sponsorable, async_sponsors_tiers]).then do |sponsorable, tiers|
      sponsorable.async_sponsors(include_private: true,
                                exclude_sponsor_opted_out_from_email: emailable_only,
                                tiers: tiers)
    end
  end

  # Public: Is this newsletter accessible to all tiers, instead of being restricted
  #         to certain tiers?
  #
  # Returns a Boolean.
  def for_all_tiers?
    !sponsorship_newsletter_tiers.exists?
  end

  def async_author_or_ghost
    async_author.then do |author|
      author || User.ghost
    end
  end

  private

  def published_newsletter_cannot_become_draft
    if state_changed?(from: "published", to: "draft")
      errors.add(:state, "can't save draft after newsletter has been published")
    end
  end

  def after_publish
    send_mailer
    instrument_events
  end

  def send_mailer
    login = sponsorable.login
    unsubscribe_url = sponsorable_url(sponsorable, host: GitHub.host_name)
    text_body = self.body.dup
    text_body += <<~MARKDOWN

       ---

       Unsubscribe by unchecking "Receive email updates from #{login}" at <#{unsubscribe_url}>.
    MARKDOWN
    html_body = GitHub::Goomba::MarkdownPipeline.to_html(text_body)

    sponsors = async_sponsors_with_access(emailable_only: true).sync.to_a

    SponsorsMailer.newsletter(
      sponsorable: sponsorable,
      sponsors: sponsors,
      subject: self.subject,
      text_body: text_body,
      html_body: html_body,
    ).deliver_later
  end

  def instrument_events
    GitHub.instrument("sponsors.sponsored_developer_update_newsletter_send", {
      user: sponsorable,
    })

    GlobalInstrumenter.instrument("sponsors.sponsored_developer_update_newsletter_send", {
      sponsored_developer: sponsorable,
      subject: self.subject,
      body: self.body,
    })
  end
end
