# frozen_string_literal: true

class MemexProjectItemSorter

  attr_reader :column, :column_id, :direction

  DESCENDING_SORT_VALUES = %w(desc descending)

  # Create a new MemexProjectItemSorter
  #
  # @param column [MemexProjectColumn, nil] A project column that belongs to a
  #        MemexProject. Indicates which column will be used for sorting.
  #        If no column is provided, items are returned according to their orignal sort.
  # @param items [Array<Hash>] An array of serialized MemexProjectItems. All
  #        items must include the `memex_project_column_values` key.
  # @param direction [String, nil] Indicates the direction of sorting. Sorting
  #        will default to `ascending`.
  def initialize(column:, items: [], direction: nil)
    @column = column
    @column_id = column&.synthetic_id
    @direction = direction.to_s
    @items = items
  end

  # Sorts the items according to the column and direction provided to the sorter.
  # When provided column represents a list of data, the data in the column will
  # first be sorted in ascending order, then the column sort will be applied.
  #
  # Returns an Array of item Hashes.
  def items
    return @items unless sortable?

    sorted_items = @items.sort_by { |i| sort_key(i) }

    sorted_items.reverse! if descending?
    sorted_items
  end

  def active_sort
    return nil unless sortable?
    direction = descending? ? "desc" : "asc"
    { column_id: column_id, direction: direction }
  end

  private

  def sortable?
    column && column.visible?
  end

  # Returns a sort key for the given item
  #
  # @param item [Hash] A serialized representation of a MemexProjectItem
  #
  # Returns a tuple, with the first element representing the group the item
  # will be sorted with and the second element determines the order if the item
  # within its group.
  def sort_key(item)
    redacted_item_sort_key, empty_value_sort_key, non_empty_value_sort_key = if descending?
      # descending sorts will be `reversed` after initial sort
      # so initial sort should sort by redacted, value, and then `nil` so `nil`
      # appears at the top when reversed
      [0, 2, 1]
    else
      [2, 1, 0]
    end

    return [redacted_item_sort_key, nil] if item_redacted?(item)

    value = fetch_item_column_value(item)

    case value
    when nil, []
      [empty_value_sort_key, nil]
    when Array
      [non_empty_value_sort_key, value.map { |i| fetch_downcased_column_value(i) }]
    else
      [non_empty_value_sort_key, fetch_downcased_column_value(value)]
    end
  end

  def fetch_downcased_column_value(value_hash)
    val = value_hash[column.content_sort_key]
    val.is_a?(String) ? val.downcase : val
  end

  def descending?
    return @descending if defined?(@descending)
    @descending = DESCENDING_SORT_VALUES.include?(direction)
  end

  def item_redacted?(item)
    item[:content_type] == MemexProjectItem::REDACTED_ITEM_TYPE
  end

  def fetch_item_column_value(item)
    column_value = item[:memex_project_column_values].find do |value|
      value[:memex_project_column_id] == column_id
    end

    column_value && column_value[:value]
  end

end
