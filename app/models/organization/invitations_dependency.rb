# frozen_string_literal: true

module Organization::InvitationsDependency
  # Public: Invite the specified user to join this org.
  #
  # user    - User to invite to join the organization.
  # email   - Email to invite to join the organization.
  # inviter - User sending the invitation.
  # teams   - Array of teams to invite the user to join.
  # role    - Symbol representing the role the invitee is being invited into.
  #           Can be :direct_member, :admin, :billing_manager or :reinstate.
  # external_identity - ExternalIdentity associated with the organization invitation,
  #           if invited as a result of an external access management change
  #
  # Returns an OrganizationInvitation.
  # Raises TradeControlsError when inviter or invitee have been trade controls restricted
  # Raises NoAvailableSeatsError when there are no seats available
  # Raises NoAvailableSeatsError when no available seats because of pending cycle
  def invite(user = nil, email: nil, inviter:, teams: [], role: nil, external_identity: nil)
    role ||= :direct_member
    email = email.presence # force "falsey" values to nil

    if role == :reinstate
      GitHub.dogstats.increment("organization", tags: ["action:invite", "reinstate:true", "by_email:#{email.present?}"])
    else
      GitHub.dogstats.increment("organization", tags: ["action:invite", "reinstate:false", "by_email:#{email.present?}"])
    end

    invitation = invitations.new(
      invitee: user,
      email: email,
      inviter: inviter,
      role: role,
      external_identity_id: external_identity&.id,
    )

    if has_full_trade_restrictions?
      invitation.update!(failed_reason: :trade_controls)
      raise ::OrganizationInvitation::TradeControlsError, TradeControls::Notices::Plaintext.organization_account_restricted
    end

    if role != :billing_manager
      if organization? && business&.has_sufficient_licenses_for?(user, email: email)
        true
      elsif !has_seat_for?(user)
        invitation.update!(failed_reason: :no_more_seats)
        raise OrganizationInvitation::NoAvailableSeatsError, "No seats are available to invite this user"
      elsif !has_seat_for?(user, pending_cycle: true)
        invitation.update!(failed_reason: :no_more_seats)
        raise OrganizationInvitation::NoAvailableSeatsError, "Your organization has a pending seat downgrade. Please cancel your pending change before inviting this user"
      end
    end

    ApplicationRecord::Domain::Users.transaction do
      GitHub.dogstats.time("organization.time", tags: ["action:invite"]) do
        Organization::InviteStatus.new(self, user, email: email, inviter: inviter, teams: teams, role: role).validate!

        previous_invitation = pending_invitation_for(user, email: email, include_private_emails: false)
        invitation.save! unless previous_invitation.present?
        invitation = previous_invitation || invitation
      end

      # Filter out duplicates and teams that the user is already invited to.
      teams_to_add = teams.to_a.uniq - invitation.teams.reload
      teams_to_add.each { |team| invitation.add_team(team, inviter: inviter) }

      invitation
    end
  end

  # Public: Returns a scope of invitations
  #
  # actor    - User attempting the action
  # invitation_ids - Invitations to check for.
  #
  # Returns an array of OrganizationInvites.
  def invitations_for(actor, invitation_ids: nil)
    return [] unless self.adminable_by?(actor)

    invitations.where(id: invitation_ids)
  end
end
