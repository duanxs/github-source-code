# rubocop:disable Style/FrozenStringLiteralComment

class Organization::BillingManagement
  include Ability::Subject
  include Ability::Membership

  def initialize(organization)
    @organization = organization
  end

  # Add a billing manager.
  #
  # user        - A User
  # invitation  - The optional OrganizationInvitation inviting the user
  #
  # Returns nothing.
  def add_manager(user, actor:, invitation: nil)
    grant user, :write

    instrument_options = { user: user, actor: actor }
    instrument_options[:invitation_email] = invitation.email if invitation.try(:email?)
    @organization.instrument :add_billing_manager, instrument_options

    GitHub.dogstats.increment("billing.managers.count", tags: ["action:add"])
  end

  # Remove a billing manager.
  #
  # user - A User
  # actor - user performing the action
  # reason - description of the reason for removal
  #
  # Returns nothing.
  def remove_manager(user, actor:, reason: nil)
    return unless manager?(user)

    revoke user

    if @organization.saml_sso_enabled?
      ExternalIdentity.unlink_saml_identities(provider: @organization.saml_provider,
                                              user_ids: [user.id])
    end
    if @organization.business&.saml_sso_enabled?
      @organization.business.remove_user_from_business(user)
    end

    @organization.instrument :remove_billing_manager,
      user: user,
      actor: actor,
      reason: reason
    GitHub.dogstats.increment("billing.managers.count", tags: ["action:remove"])
  end

  def ability_id
    @organization.id
  end

  # Returns true if the given user is a billing manager of this organization.
  def manager?(user)
    permit?(user, :write)
  end

  # Internal: remove all billing managers, used when destroying an organization
  def remove_all_managers
    members.each do |manager|
      remove_manager manager, actor: nil
    end
  end

  # Internal: See Ability::Subject#grant?
  def grant?(actor, action)
    @organization.grant?(actor, action)
  end
end
