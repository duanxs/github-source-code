# frozen_string_literal: true

module Organization::TokenScanningDependency
  extend ActiveSupport::Concern

  # Describes if organization-level token-scanning staff tool operations are supported in general.
  # Org must be opted into the advanced security private beta program.
  def token_scanning_stafftools_available?
    return false unless GitHub.configuration_supports_token_scanning?
    advanced_security_private_beta_enabled?
  end

  # describes if the secret scanning api support is enabled for the org
  def secret_scanning_api_enabled?
    return false unless GitHub.configuration_supports_token_scanning?
    return false unless advanced_security_private_beta_enabled?
    GitHub.flipper[:secret_scanning_api_enabled].enabled?(self)
  end

  def token_scanning_org_ui_enabled?
    return false unless GitHub.configuration_supports_token_scanning?
    return false unless GitHub.configuration_advanced_security_enabled?
    return true if advanced_security_private_beta_enabled?
    advanced_security_enabled?
  end
end
