# frozen_string_literal: true
#
# This model tracks whether a credential has been granted access to an
# organization. An credential can be a personal access token or an oauth access
# token (both modeled by OauthAccess). It can be expanded to include other
# models such as public keys.
#
# See https://github.com/github/iam/blob/master/docs/adrs/adr-002-organization-saml-access-control-for-non-web-clients.md
class Organization::CredentialAuthorization < ApplicationRecord::Domain::Users
  include Instrumentation::Model
  include GitHub::Relay::GlobalIdentification

  def event_prefix
    :org_credential_authorization
  end

  def event_payload
    {
      org: organization,
      credential_id: credential_id,
      credential_type: credential_type,
      actor: actor,
      actor_type: actor_type,
    }
  end

  self.table_name = :organization_credential_authorizations

  belongs_to :organization
  belongs_to :credential, polymorphic: true
  belongs_to :actor, polymorphic: true
  belongs_to :revoked_by, class_name: "User"

  validates :actor, presence: true
  validates :organization, presence: true
  validates :credential, presence: true

  validates :credential_id, uniqueness: { scope: [:credential_type, :organization_id, :actor_id, :actor_type] }

  validates :credential_type, inclusion: { in: %w[OauthAccess PublicKey], message: "must be an OauthAccess or PublicKey" }
  validate :validate_actor_is_member_of_organization, unless: [:revoked?, :using_application_token?]
  validate :validate_credential_is_owned_by_actor, unless: :using_application_token?
  validate :validate_credential_is_not_revoked, if: :using_public_key?

  before_validation :set_fingerprint
  after_create_commit :instrument_grant
  after_destroy_commit :instrument_deauthorization

  scope :by_credential, -> (credential:) {
    fail "`credential` must not be a relation (cross-domain risk)" if credential.is_a?(ActiveRecord::Relation)
    where(credential: credential)
  }

  scope :by_organization, -> (organization:) {
    if organization.is_a?(Array)
      where(organization_id: organization.map(&:id))
    else
      where(organization_id: organization.id)
    end
  }

  scope :by_actor, -> (actor:) {
    where(actor_id: actor.id, actor_type: actor.class.name)
  }

  scope :by_organization_credential, -> (organization:, credential:) {
    by_organization(organization: organization).
    by_credential(credential: credential)
  }

  scope :excluding_organizations, -> (organization_ids:) {
    where("organization_credential_authorizations.organization_id NOT IN (?)", organization_ids)
  }

  scope :active, -> {
    where("revoked_by_id IS NULL")
  }

  scope :revoked, -> {
    where("revoked_by_id IS NOT NULL")
  }

  scope :public_key_credentials_by_fingerprint, -> (fingerprint:) {
    where(credential_type: "PublicKey", fingerprint: fingerprint)
  }

  scope :excluding_applications, -> {
    # Because we can't join against the oauth_accesses table we have to filter by limiting the ids.
    oauth_access_authorizations_ids = where(credential_type: "OauthAccess").pluck(:credential_id)
    pat_authorization_ids = OauthAccess.with_ids(oauth_access_authorizations_ids).personal_tokens.ids

    where("(credential_type = 'OauthAccess' AND credential_id IN (?)) OR credential_type <> 'OauthAccess'", pat_authorization_ids)
  }

  def self.grant(organization:, credential:, actor:)
    authorization = create(organization: organization, credential: credential, actor: actor)
    return unless authorization.valid?

    authorization
  end

  def self.revoke(organization:, credential:, actor:)
    # Organization admins can revoke any authorized credentials on its org
    can_revoke ||= organization.resources.organization_administration.writable_by?(actor)

    # Personal access tokens can be revoked by its owner
    can_revoke ||= (credential.personal_access_token? && credential.user == actor)

    return unless can_revoke

    if authorization = authorization(organization: organization, credential: credential)
      authorization.update(revoked_by_id: actor.id, revoked_at: Time.now)
      authorization.instrument :revoke, actor: actor, owner: authorization.actor
      authorization
    end
  end

  def self.authorization(organization:, credential:)
    return unless credential.present?

    active.by_organization_credential(organization: organization, credential: credential).first
  end

  # Public: Creates a signed auth token for the actor that stores a reference
  # to a credential. Currently, this is given to the actor in a URL if they
  # attempt to access SAML protected resources with a credential that has not
  # been whitelisted yet. The aforementioned URL can be visited to automatically
  # whitelist the credential by verifying the auth token before it expires.
  #
  # organization - The organization that the credential will be whitelisted for.
  # target       - The organization or business used to create and validate the request scope.
  # credential   - The credential to be whitelisted.
  # actor        - The owner of the credential.
  #
  # Returns a String.
  def self.generate_request(organization:, target:, credential:, actor:)
    actor.signed_auth_token(
      scope: "saml:authorized_credential:#{target.class.name}:#{target.id}",
      expires: 1.hour.from_now,
      data: {
        organization_id: organization.id,
        credential_id: credential.id,
        credential_type: credential.class.name,
      },
    )
  end

  # Public: Verifies a signed auth token generated by `.sign_token`. The
  # verified token can be used to grant a credential access to an organization.
  #
  # target       - The organization or business that the token was generated for.
  # token        - The signed auth token string to be verified.
  # actor        - The credential owner to be verified.
  #
  # Returns a GitHub::Authentication::SignedAuthToken or nothing.
  def self.consume_request(target:, token:, actor:)
    parsed_token = User.verify_signed_auth_token(
      scope: "saml:authorized_credential:#{target.class.name}:#{target.id}",
      token: token,
    )

    return unless parsed_token.valid?
    return unless parsed_token.user == actor

    parsed_token
  end

  # Public: whether or not this credential's authorization was revoked by an
  # organization administrator.
  #
  # Returns true if the authorization was revoked, false if not.
  def revoked?
    revoked_by_id.present?
  end

  # Public: whether or not this credential's authorization is active.
  #
  # Returns true if the authorization is active, false if not.
  def active?
    !revoked?
  end

  # Public: whether or not the credential is a token generated for use by an
  # OAuth Application.
  #
  # Returns true if `credential` is an application token, or false if it is a
  # personal access token or other type of object.
  def using_application_token?
    credential.is_a?(OauthAccess) && !credential.personal_access_token?
  end

  # Public: whether or not the credential is a token generated for use by an
  # Personal Access Token.
  #
  # Returns true if `credential` is a personal access token, or false if it is a
  # application token or other type of object.
  def using_personal_access_token?
    credential.is_a?(OauthAccess) && credential.personal_access_token?
  end

  # Public: whether or not the credential is an token generated for use by an
  # SSH key.
  #
  # Returns true if `credential` is a token for SSH key, or false if it is a
  # personal access token or other type of object.
  def using_public_key?
    credential_type == "PublicKey"
  end

  # Public: returns the last time the underlying credential was accessed
  #
  # Returns a ActiveSupport::TimeWithZone corresponding to the last time the credential was accessed
  # may be nil if the credential was never accessed
  def accessed_at
    credential&.last_access_time
  end

  private

  def set_fingerprint
    if using_public_key?
      self.fingerprint = credential.fingerprint
    end
  end

  def instrument_grant
    GitHub.dogstats.increment("organization_credential_authorization", tags: ["action:grant"])
    instrument :grant
  end

  def instrument_deauthorization
    GitHub.dogstats.increment("organization_credential_authorization", tags: ["action:deauthorize"])
    instrument :deauthorize
  end

  def validate_actor_is_member_of_organization
    return unless actor && organization

    unless organization.member?(actor)
      errors.add(:actor, "must be a member of organization")
    end
  end

  def validate_credential_is_owned_by_actor
    return unless actor && credential

    if actor != credential.user
      errors.add(:credential, "must be owned by actor")
    end
  end

  def validate_credential_is_not_revoked
    if self.class.by_organization(organization: organization).revoked.where(fingerprint: credential.fingerprint).any?
      errors.add(:credential, "has been revoked")
    end
  end
end
