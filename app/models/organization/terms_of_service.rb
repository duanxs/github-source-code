# frozen_string_literal: true
class Organization
  class TermsOfService
    include Instrumentation::Model

    attr_reader :organization

    CORPORATE = "Corporate"
    CUSTOM = "Custom"

    TERMS_OF_SERVICE_TYPES = ["Standard", CUSTOM, CORPORATE, "Evaluation", "ESA+Education"]
    BUSINESS_TERMS_OF_SERVICE_TYPES = ["Corporate", "Evaluation"]
    TERMS_OF_SERVICE_KEY = "organization.terms_of_service_type."
    CORPORATE_PROMPT_ENABLED_AT_KEY = "organization.terms_of_service_prompt_enabled_time."
    ESA_EDUCATION_PROMPT_ENABLED_AT_KEY = "organization.education_terms_of_service_prompt_enabled_time."
    CORPORATE_UPGRADE_BANNER_NAME = "org_corporate_tos_banner"
    ESA_EDUCATION_UPGRADE_BANNER_NAME = "org_esa_education_tos_banner"

    def initialize(organization:)
      @organization = organization
    end

    # Public: The name of the terms of service that was accepted.
    #
    # Returns a String.
    def name
      async_name.sync
    end

    # Public: The name of the terms of service that was accepted.
    #
    # Returns a Promise<String>.
    def async_name
      Platform::Loaders::KV.load(tos_key).then do |value|
        value.present? ? value : "Standard"
      end
    end

    # Public: Is this organization under the Standard terms of service?
    #
    # Returns a Boolean.
    def standard?
      async_standard?.sync
    end

    # Public: Is this organization under the Standard terms of service?
    #
    # Returns a Promise<Boolean>.
    def async_standard?
      async_accepted?("Standard")
    end

    # Public: Is this organization under the Custom terms of service?
    #
    # Returns a Boolean.
    def custom?
      async_custom?.sync
    end

    # Public: Is this organization under the Custom terms of service?
    #
    # Returns a Promise<Boolean>.
    def async_custom?
      async_accepted?("Custom")
    end

    # Public: Is this organization under the Corporate terms of service?
    #
    # Returns Boolean.
    def corporate?
      async_corporate?.sync
    end

    # Public: Is this organization under the Corporate terms of service?
    #
    # Returns Promise<Boolean>.
    def async_corporate?
      async_accepted?("Corporate")
    end

    # Public: Is this organization under the Evaluation terms of service?
    #
    # Returns Boolean.
    def evaluation?
      async_evaluation?.sync
    end

    # Public: Is this organization under the Evaluation terms of service?
    #
    # Returns Promise<Boolean>.
    def async_evaluation?
      async_accepted?("Evaluation")
    end

    # Public: Is this organization under the Evaluation terms of service?
    #
    # Returns Boolean.
    def esa_education?
      async_esa_education?.sync
    end

    # Public: Is this organization under the Corporate+Education terms of service?
    #
    # Returns Promise<Boolean>.
    def async_esa_education?
      async_accepted?("ESA+Education")
    end

    # Public: Is this organization under the Evaluation or Corporate terms of service?
    #
    # Returns Boolean.
    def business_terms_of_service?
      corporate? || evaluation?
    end

    # Public: Has the corporate ToS upgrade prompt been enabled by a staff user?
    #
    # Returns a Boolean.
    def corporate_upgrade_prompt_enabled?
      async_corporate_upgrade_prompt_enabled?.sync
    end

    # Public: Has the corporate ToS upgrade prompt been enabled by a staff user?
    #
    # Returns a Promise<Boolean>.
    def async_corporate_upgrade_prompt_enabled?
      async_corporate_upgrade_prompt_enabled_at.then do |enabled_at|
        enabled_at.present?
      end
    end

    # Public: The time that the prompt to change to the Corporate ToS was enabled
    #         by staff, if it ever has been.
    #
    # Returns a String | nil.
    def corporate_upgrade_prompt_enabled_at
      async_corporate_upgrade_prompt_enabled_at.sync
    end

    # Public: The time that the prompt to change to the Corporate ToS was enabled
    #         by staff, if it ever has been.
    #
    # Returns a Promise<String | nil>.
    def async_corporate_upgrade_prompt_enabled_at
      Platform::Loaders::KV.load(corporate_prompt_enabled_key)
    end

    # Public: Enable the corporate ToS upgrade banner for all owners for this
    #         organization.
    #
    # Returns a Boolean.
    def enable_corporate_upgrade_prompt
      organization.admins.each do |admin|
        admin.reset_notice(CORPORATE_UPGRADE_BANNER_NAME)
      end

      GitHub.kv.set(corporate_prompt_enabled_key, Time.current.to_s)

      true
    end

    # Public: Has the ESA+Education ToS upgrade prompt been enabled by a staff user?
    #
    # Returns a Boolean.
    def esa_education_upgrade_prompt_enabled?
      async_esa_education_upgrade_prompt_enabled?.sync
    end

    # Public: Has the ESA+education ToS upgrade prompt been enabled by a staff user?
    #
    # Returns a Promise<Boolean>.
    def async_esa_education_upgrade_prompt_enabled?
      async_esa_education_upgrade_prompt_enabled_at.then do |enabled_at|
        enabled_at.present?
      end
    end

    # Public: The time that the prompt to change to the ESA+education ToS was enabled
    #         by staff, if it ever has been.
    #
    # Returns a String | nil.
    def esa_education_upgrade_prompt_enabled_at
      async_esa_education_upgrade_prompt_enabled_at.sync
    end

    # Public: The time that the prompt to change to the ESA+education ToS was enabled
    #         by staff, if it ever has been.
    #
    # Returns a Promise<String | nil>.
    def async_esa_education_upgrade_prompt_enabled_at
      Platform::Loaders::KV.load(esa_education_prompt_enabled_key)
    end

    # Public: Enable the ESA+education ToS upgrade banner for all owners for this
    #         organization.
    #
    # Returns true
    def enable_esa_education_upgrade_prompt
      organization.admins.each do |admin|
        admin.reset_notice(ESA_EDUCATION_UPGRADE_BANNER_NAME)
      end

      GitHub.kv.set(esa_education_prompt_enabled_key, Time.current.to_s)
    end

    # Public: Update the terms of service for an organization.
    #
    # type - The String terms of service type, from `TERMS_OF_SERVICE_TYPES`.
    # actor - The User updating the terms of service for this organization.
    # company_name - The name of the company that the terms of service is
    #                accepted on behalf of.
    # staff_actor - A Boolean indicating if a staff user is updating the terms
    #               of service for this account.
    # change_note - A String note explaining why the terms of service is being
    #               updated. Required if `staff_actor` is `true`.
    #
    # Returns a Boolean.
    def update(type:, actor:, company_name: nil, staff_actor: false, change_note: nil)
      return false unless TERMS_OF_SERVICE_TYPES.include?(type)
      # Changes made by staff are required to have a change note.
      return false if staff_actor && change_note.blank?

      audit_context = organization.event_context
      existing_company = organization.company.try(:name)
      existing_terms = name
      company_changed = !company_name.nil? && (company_name != existing_company)
      terms_changed = type != existing_terms

      # Update the terms of service value for this organization,
      # if it changed.
      if terms_changed
        set_terms!(type)

        terms_context = {
          terms_of_service_type: {
            old_value: existing_terms,
            new_value: type,
          },
        }

        if standard?
          company_name = nil
          company_changed = true
        end

        audit_context = audit_context.merge(terms_context)
      end

      # Return true if there are no actual changes.
      return true unless company_changed || terms_changed

      if company_changed
        return false unless organization.update(company_name: company_name)

        # If the company name is blank, remove all companies for this
        # organization.
        organization.companies.delete_all if company_name.blank?

        company_context = {
          company_name: {
            old_value: existing_company,
            new_value: company_name,
          },
        }
        audit_context = audit_context.merge(company_context)
      end

      if staff_actor
        guarded_actor = GitHub.guarded_audit_log_staff_actor_entry(actor)
        staff_context = {
          prefix: "staff",
          terms_of_service_change_reason: change_note,
        }.merge(guarded_actor)

        audit_context = audit_context.merge(staff_context)
      else
        audit_context = audit_context.merge({ prefix: "org", actor: actor })
      end

      instrument :update_terms_of_service, audit_context

      true
    end

    private

    # Private: The KV key that stores the terms of service type for this organization.
    #
    # Returns a String.
    def tos_key
      "#{TERMS_OF_SERVICE_KEY}#{organization.id}"
    end

    # Private: The KV key that stores the time the terms of service upgrade
    #          prompt was enabled for this organization.
    #
    # Returns a String.
    def corporate_prompt_enabled_key
      "#{CORPORATE_PROMPT_ENABLED_AT_KEY}#{organization.id}"
    end

    # Private: The KV key that stores the time the terms of service upgrade
    #          prompt was enabled for this organization.
    #
    # Returns a String.
    def esa_education_prompt_enabled_key
      "#{ESA_EDUCATION_PROMPT_ENABLED_AT_KEY}#{organization.id}"
    end

    # Private: Set the stored terms of service value for this organization.
    #
    # value – A String terms of service name, from `TERMS_OF_SERVICE_TYPES`.
    #
    # Returns nothing.
    def set_terms!(value)
      raise ArgumentError, "Invalid terms of service type" unless TERMS_OF_SERVICE_TYPES.include?(value)
      GitHub.kv.set(tos_key, value)
    end

    # Private: Checks if a terms of service type is the type that this
    #          organization has accepted.
    #
    # Returns a Promise<Boolean>.
    def async_accepted?(tos_name)
      async_name.then do |value|
        value == tos_name
      end
    end
  end
end
