# frozen_string_literal: true

module Organization::ConfigurationDependency
  extend ActiveSupport::Concern

  include Configurable
  include Configurable::ActionExecutionCapabilities
  include Configurable::ActionsAllowedByOwner
  include Configurable::ActionsAllowedEntities
  include Configurable::ForkPrWorkflowsPolicy
  include Configurable::MembersCanDeleteIssues
  include Configurable::MembersCanUpdateProtectedBranches
  include Configurable::MembersCanChangeRepoVisibility
  include Configurable::MembersCanCreateRepositories
  include Configurable::MembersCanDeleteRepositories
  include Configurable::MembersCanInviteOutsideCollaborators
  include Configurable::MembersCanMakePurchases
  include Configurable::MembersCanViewDependencyInsights
  include Configurable::AllowPrivateRepositoryForking
  include Configurable::AllowOrganizationCodespaces
  include Configurable::AcceptOrganizationCodespacesTerms
  include Configurable::DisableTeamDiscussions
  include Configurable::DefaultRepositoryPermission
  include Configurable::TwoFactorRequired
  include Configurable::SmsForTwoFactorAuth
  include Configurable::RestrictNotificationDelivery
  include Configurable::DisplayCommenterFullName
  include Configurable::MembersCanCreateTeams
  include Configurable::SshCertificateRequirement
  include Configurable::IpWhitelistingEnabled
  include Configurable::IpWhitelistingAppAccessEnabled
  include Configurable::ReadersCanCreateDiscussions
  include Configurable::MembersCanPublishPackages
  include Configurable::RepositoryActionVerifiedOrg

  # Internal: Get the configuration owner.
  #
  # Values for an Organization can be cascaded from (or overridden by)
  # a Business if the Organization is a member in a Business,
  # or the global GitHub object.
  def configuration_owner
    async_configuration_owner.sync
  end

  # Internal: Get the configuration owner asynchronously.
  #
  # Values for an Organization can be cascaded from (or overridden by)
  # a Business if the Organization is a member in a Business,
  # or the global GitHub object.
  def async_configuration_owner
    self.async_business.then { |business| business || GitHub }
  end
end
