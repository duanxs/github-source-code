# frozen_string_literal: true

module Organization::MemexProjectsDependency
  extend ActiveSupport::Concern

  class MemexProjectsSearchResult
    attr_reader :memex_projects, :next_page_cursor, :total_open_count, :total_closed_count

    def initialize(memex_projects:, next_page_cursor:, total_open_count:, total_closed_count:)
      @memex_projects = memex_projects
      @next_page_cursor = next_page_cursor
      @total_open_count = total_open_count
      @total_closed_count = total_closed_count
    end

    def has_next_page?
      !!next_page_cursor
    end
  end

  # Returns a page of memex projects matching the given search query.
  #
  # query - Search::Queries::MemexProjectQuery
  # cursor - Optional Integer memex number that the returned page should start on (inclusively).
  #   If omitted, the page will begin with the most recently created memex.
  # limit - Option Integer number of results to return. Defaults to 30.
  #
  # Returns MemexProjectsSearchResult.
  def search_memex_projects(query:, viewer:, cursor: nil, limit: 30)
    base_scope = memex_projects.includes(:creator).order(number: :desc)

    base_scope = base_scope.where("number <= ?", cursor.to_i) if cursor
    if (login = query.creator_filter)
      login = viewer&.login if login == Search::Query::MACRO_ME
      creator = User.find_by(login: login)
      base_scope = base_scope.where(creator_id: creator&.id)
    end

    # This intentionally uses just the first full-text query term. Empirically this seems to be good
    # enough, and it reduces the number of wildcards we need to insert into the query (which likely
    # helps performance a bit, even though it is the leading wildcard that breaks index usage).
    if (term = query.full_text_query_terms.first)
      base_scope = base_scope.where(
        # Since title is a VARBINARY column, we ordinarily cannot compare a value to it using LIKE.
        # To fix that, first cast it to a UTF-8 string (whose collation is case-insensitive).
        "CAST(COALESCE(title, ?) AS CHAR CHARACTER SET utf8mb4) LIKE ?",
        MemexProject::DEFAULT_TITLE,
        "%#{term}%"
      )
    end

    open_scope = base_scope.where(closed_at: nil)
    closed_scope = base_scope.where.not(closed_at: nil)

    memexes = (query.state_filter == "closed" ? closed_scope : open_scope).limit(limit + 1).all.to_a
    next_memex = memexes.length > limit ? memexes.pop : nil

    return MemexProjectsSearchResult.new(
      memex_projects: memexes,
      next_page_cursor: next_memex&.number,
      total_open_count: open_scope.count,
      total_closed_count: closed_scope.count,
    )
  end
end
