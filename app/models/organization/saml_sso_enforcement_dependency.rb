# frozen_string_literal: true

module Organization::SamlSsoEnforcementDependency
  extend ActiveSupport::Concern

  include SamlProviderMembers

  # Public: Is this organization configured to use SAML Single Sign-on to
  # provision its memberships?
  #
  # Returns a Boolean
  def saml_sso_enabled?
    return false unless business_plus?
    saml_provider.present? && saml_provider.persisted?
  end

  # Public: Does this organization enforce membership via a SAML identity provider?
  #
  # Returns a Boolean
  def saml_sso_enforced?
    saml_sso_enabled? && saml_provider.enforced?
  end

  # Public: Is the given user already linked to the organization via the
  # current SAML identity provider?
  #
  # Returns a Boolean
  def saml_sso_requirement_met_by?(user)
    return true if !saml_sso_enforced?
    user && ExternalIdentity.linked?(
      provider: saml_provider,
      user: user,
    )
  end

  # Public: Find the object that contains the relevant SAML provider for the current
  # external identity session.
  #
  # When a business organization with a business SAML provider, the business is returned
  # When a non-business organization, self is returned
  #
  # Returns the organization or it's business owner
  def external_identity_session_owner
    return self.business if saml_enabled_on_business?
    self
  end

  def saml_enabled_on_business?
    self.business && self.business.saml_provider.present?
  end

  # Public: is access to this Organization protected by SAML SSO?
  #
  # Returns Boolean (true if SAML is enabled for this Organization or for its parent Enterprise)
  def saml_sso_present?
    saml_sso_enabled? || saml_enabled_on_business?
  end

  # Public: is SAML SSO access enabled and enforced for this Organization?
  #   true if SAML is enabled and enforced for this Organization
  #   true if SAML is enforced for the owning Enterprise (Enterprise SAML is automatically
  #   enforced if it's enabled)
  #
  # Returns Boolean
  def saml_sso_present_enforced?
    saml_sso_enforced? || business&.saml_sso_enforced?
  end
end
