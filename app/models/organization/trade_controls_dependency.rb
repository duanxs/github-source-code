# frozen_string_literal: true

module Organization::TradeControlsDependency
  def send_trade_controls_enforcement_email
    TradeControlsMailer.organization_restricted(self).deliver_later
  end

  def send_trade_controls_override_email
    TradeControlsMailer.organization_reactivated(self).deliver_later
  end

  def instrument_trade_controls_enforcement(compliance:)
    InstrumentOrganizationTradeRestrictionEnforceJob.perform_later(
      organization: self, **compliance.to_hydro)
  end

  def trade_controls_restricted_members
    TradeControls::Restriction.not_unrestricted.where(user: member_ids)
  end

  def send_trade_controls_pending_enforcement_email
    TradeControlsMailer.organization_scheduled_pending_enforcement(self).deliver_later
  end
end
