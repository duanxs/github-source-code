# rubocop:disable Style/FrozenStringLiteralComment

class Organization
  # Creates an Organization for the current user,
  # optionally setting up billing for paid Organization.
  #
  # See also OrganizationController#create
  class Creator

    class Error < StandardError; end

    # Result from the Organization creation
    class Result
      attr_accessor :error_message, :coupon
      attr_reader :organization
      def initialize(success, organization, error_message: nil)
        @success = success
        @organization = organization
        @error_message = error_message
      end

      def success?
        @success
      end
    end

    def self.perform(current_user, plan, org_hash, payment_details: {}, coupon_code: nil, business_owned: false, business: nil, terms_of_service: nil)
      new(
        current_user,
        plan,
        org_hash,
        payment_details: payment_details,
        coupon_code: coupon_code,
        business_owned: business_owned,
        business: business,
        terms_of_service: terms_of_service,
      ).perform
    end

    # Create the Creator
    #
    def initialize(current_user, plan, org_hash, payment_details: {}, coupon_code: nil, business_owned: false, business: nil, terms_of_service: nil)
      @current_user = current_user
      @plan = plan
      @org_hash = org_hash.except(:profile_name).merge(plan: plan)
      @payment_details = payment_details
      @coupon_code = coupon_code
      @business_owned = business_owned
      @profile_name = org_hash[:profile_name]
      @business = business
      @terms_of_service = terms_of_service&.capitalize
    end

    # The current User creating the Organization - must not be nil
    attr_reader :current_user
    # Plan object for this Organization - should not be nil
    attr_reader :plan
    # Hash of attributes for the new Org
    attr_reader :org_hash
    # Optional String coupon code
    attr_reader :coupon_code
    # Optional Hash of parsed params for payment - see also BillingSettingsHelper
    attr_reader :payment_details
    # Optional Boolean business owned
    attr_reader :business_owned
    # Optional String representing the profile name
    attr_reader :profile_name
    # Optional Business to add this newly created Organization to
    attr_reader :business
    # Optional Terms of Service type to create the Organization under if business_owned
    attr_reader :terms_of_service

    # Do the actual Org creation
    #
    # Returns a Creator::Result object
    def perform
      # Push the current user as the actor early on in the creation process
      GitHub.context.push(actor: current_user)
      Audit.context.push(actor: current_user)

      organization = Organization.new(org_hash)

      unless current_user.present?
        return Result.new(false, organization, error_message: "Admin user could not be found")
      end

      organization.creator = current_user

      if business_owned && org_hash[:company_name].blank?
        return Result.new(false, organization, error_message: "#{terms_of_service} Terms of Service require a business name")
      end

      if business_owned && !Company.valid_name?(org_hash[:company_name])
        return Result.new(false, organization, error_message: "Company name is invalid")
      end

      if organization.admins.blank?
        organization.admins = [current_user]
      end

      if coupon_code.present?
        if !organization.validate_coupon(coupon_code)
          # NB - this is stupid but needs to happen because of the Transaction
          # creation side effect in .redeem_coupon
          organization.coupon = coupon_code
          error_message = organization.errors.full_messages.first
          return Result.new(false, organization, error_message: error_message)
        else
          coupon = Coupon.find_by_code(coupon_code)
        end
      end

      billing_result = nil

      organization.seats = \
        if plan.per_seat?
          organization.default_seats(new_plan: plan)
        else
          0
        end

      saved_successfully = begin
        Organization.transaction do
          Ability.transaction do
            organization.company_name = nil
            organization.save
            organization.terms_of_service.update(type: terms_of_service, actor: current_user) if business_owned
            organization.company_name = org_hash[:company_name]

            saved = organization.save

            if saved
              if GitHub.single_business_environment? && GitHub.global_business
                # Automatically add new orgs to the global business when in a single
                # global business environment.
                GitHub.global_business.add_organization organization
              end

              organization.redeem_coupon(coupon, instrument: false, actor: current_user) if coupon
              GitHub.dogstats.increment("organization", tags: ["action:create", "valid:true"])

              if organization.payment_amount.positive?
                billing_result = GitHub::Billing.signup(organization,
                                                        plan.name,
                                                        actor: current_user,
                                                        payment_details: payment_details)

                context = {
                  note: "Added #{organization.friendly_payment_method_name} during creation",
                  org: organization,
                }

                GitHub.context.push(context)
                Audit.context.push(context)

                unless billing_result.success?
                  error_context = {
                    error: billing_result.error_message.to_s,
                    metadata: billing_result.error_message.try(:metadata),
                  }

                  GitHub.dogstats.increment("organization", tags: ["action:create", "valid:false"])
                  GitHub.context.push(error_context)
                  Audit.context.push(error_context)
                  raise Error
                end
              end

              if business
                raise Error unless business.adminable_by?(current_user)

                membership = business.add_organization(organization)
                # Business#add_organization returns a truthy value that might not have been saved
                raise Error unless membership.persisted?

                organization.update!(plan: :business_plus)
              end
            end
            saved
          end
        end
      rescue Error
        false
      end

      if saved_successfully
        organization.reload
        organization.set_beta_features_for_plan
        OrgCreationJob.perform_later(current_user.id, organization.id)
      else
        # Need to rollback the Organization to a clean ActiveRecord state
        organization = Organization.new(org_hash)
        organization.plan = plan.name
        organization.valid?
      end

      result = Result.new(saved_successfully, organization)
      result.error_message = billing_result.error_message.to_s if billing_result
      result.coupon = coupon
      result
    end

  end
end
