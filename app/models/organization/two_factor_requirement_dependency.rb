# rubocop:disable Style/FrozenStringLiteralComment

# This is a collection of public and private methods related to the two factor
# requirement feature for organizations.
#
# The following public methods should be used:
#
# * Organization#two_factor_requirement_enabled?
# * Organization#enable_two_factor_requirement
# * Organization#disable_two_factor_requirement
#
# DisableTwoFactorRequirementJob is also performed via the
# enterprise:disable_two_factor_requirement rake task on a configuration run on
# Enterprise Server installations, which checks whether the current
# authentication mode supports 2FA and if not, disables 2FA on the global
# business as well as on any orgs that have 2FA enabled.

module Organization::TwoFactorRequirementDependency
  TWO_FA_USER_COUNT_LIMIT = 2001

  # Public: Enables 2fa requirement for the organization.
  def enable_two_factor_requirement(actor:, log_event: false)
    enable_two_factor_required(actor: actor, log_event: log_event)

    yield if block_given?
  end

  # Public: Disables the 2fa requirement for the organization.
  def disable_two_factor_requirement(actor:, log_event: false)
    disabled_2fa = disable_two_factor_required(actor: actor, log_event: log_event)

    # remove the configuration entry for the org that states that SMS is not an
    # option for 2FA for members
    disable_restriction_sms_for_2fa(actor: actor, log_event: log_event)

    disabled_2fa
  end

  # Public: Direct members with 2fa disabled.
  #
  # Returns an ActiveRecord::Relation for User.
  def members_with_two_factor_disabled
    members.two_factor_disabled
  end

  # Public: Outside collaborators with 2fa disabled.
  #
  # Returns an ActiveRecord::Relation for User.
  def outside_collaborators_with_two_factor_disabled
    outside_collaborators.two_factor_disabled
  end

  # Public: Hiring managers with 2fa disabled.
  #
  # Returns an ActiveRecord::Relation for User.
  def billing_managers_with_two_factor_disabled
    billing_managers.two_factor_disabled
  end

  # Public: An array of scopes that represent the users affiliated with the
  # organization and have 2fa disabled.
  #
  # Returns an Array of ActiveRecord::Relation objects.
  def affiliated_users_with_two_factor_disabled_scopes
    [
      members_with_two_factor_disabled,
      outside_collaborators_with_two_factor_disabled,
      billing_managers_with_two_factor_disabled,
    ]
  end

  # Public: Affiliated users with 2FA disabled
  #
  # Returns a Boolean.
  def affiliated_users_with_two_factor_disabled
    affiliated_users_with_two_factor_disabled_scopes.
      inject(Set.new) do |users, scope|
      users.merge(scope.all)
    end.to_a
  end

  # Public: The number of unique affiliated users with 2FA disabled
  #
  # Returns an Integer, up to TWO_FA_USER_COUNT_LIMIT
  def affiliated_users_with_two_factor_disabled_count
    subqueries = affiliated_users_with_two_factor_disabled_scopes.map do |query|
      query.select(:id).limit(TWO_FA_USER_COUNT_LIMIT).to_sql
    end
    subqueries = subqueries.reject(&:blank?).join(") UNION (")

    self.class.connection.execute("SELECT COUNT(*) FROM ((#{subqueries}) LIMIT #{TWO_FA_USER_COUNT_LIMIT}) AS non_2fa_users").to_a.flatten.first
  end

  # Public: Does this organization need enforcement to enable the two factor
  # requirement?
  #
  # Returns a Boolean.
  def affiliated_users_with_two_factor_disabled_exist?
    affiliated_users_with_two_factor_disabled_scopes.each do |scope|
      return true if scope.exists?
    end

    false
  end

  # Public: Returns true if this organization's 2FA Authentication requirement
  #         (if any) can be met by the given PROSPECTIVE_MEMBER.
  #
  # prospective_member - a User
  #
  # Returns a Boolean.
  def two_factor_requirement_met_by?(prospective_member)
      return true if !two_factor_requirement_enabled?
      return false if prospective_member.nil?
      return true if members_without_2fa_allowed?
      prospective_member.two_factor_authentication_enabled?
  end

  def async_two_factor_requirement_met_by?(prospective_member)
    self.async_two_factor_requirement_enabled?.then do |two_fa_enabled|
      next Promise.resolve(true) if !two_fa_enabled
      next Promise.resolve(false) if prospective_member.nil?
      next Promise.resolve(true) if members_without_2fa_allowed?
      next Promise.resolve(prospective_member.async_two_factor_authentication_enabled?)
    end
  end

  # Public: Can the two factor requirement be enabled for this organization?
  #
  # At least one admin in the organization must have two factor authentication
  # enabled for the two factor requirement be enabled for this organization.
  #
  # Returns a Boolean.
  def can_two_factor_requirement_be_enabled?
    admins.any?(&:two_factor_authentication_enabled?)
  end

  # Public: Is the two factor requirement currently being enforced?
  #
  # Returns a Boolean.
  def enforcing_two_factor_requirement?
    return false unless enforce_two_factor_requirement_job_status.present?

    !enforce_two_factor_requirement_job_status.finished?
  end

  # Public: The JobStatus for the two factor requirement enforcement job.
  #
  # Returns a JobStatus or nil.
  def enforce_two_factor_requirement_job_status
    @enforce_two_factor_requirement_job_status ||= begin
      status = EnforceTwoFactorRequirementOnOrganizationJob.status(self)
      return status if status && !status.finished?

      if self.business
        status = EnforceTwoFactorRequirementOnBusinessJob.status(self.business)
      end

      status
    end
  end

  # Public: Is two factor enforcement enabled for the org or the business that
  # owns it?
  #
  # Returns a Boolen
  def two_factor_cap_enforcement_enabled?
    GitHub.flipper[:two_factor_cap_enforcement].enabled?(self) ||
      GitHub.flipper[:two_factor_cap_enforcement].enabled?(self.business)
  end

  # Public: Are members without 2fa allowed to join the organization? This can
  # only be true if the org has 2fa enforcement enabled already
  #
  # Returns a Boolen
  def members_without_2fa_allowed?
    return false unless two_factor_cap_enforcement_enabled?
    GitHub.flipper[:members_without_2fa_allowed].enabled?(self) ||
      GitHub.flipper[:members_without_2fa_allowed].enabled?(self.business)
  end

end
