# rubocop:disable Style/FrozenStringLiteralComment

class Organization
  class AuditLog
    def initialize(organization)
      @organization = organization
    end

    def search(query = {})
      Audit::Driftwood::Query.new_org_business_query(query.merge(org_id: @organization.id)).execute
    end

    def summary(from: nil)
      from ||= 13.days.ago.in_time_zone.beginning_of_day
      to = Time.zone.now.end_of_day
      query_indexes = from.month != to.month ? 2 : 1
      query = {
        phrase: "created:\"#{from.iso8601}..#{to.iso8601}\"",
        aggregations: true,
        search_type: "count",          # this "count" is properly converted in the
        query_indexes: query_indexes,   # app/models/search/queries/audit_log_query.rb class
      }

      search(query)
    end
  end
end
