# frozen_string_literal: true

# A class to encapsulate logic around chaining scopes based on filters expressed
# in the passed-in query object.
#
# Note: this intentionally does not scope users to a given organization. That
# should be handled separately when working with the scope returned from this
# class.
class Organization::People::Filter
  def initialize(query:)
    @query = query
  end

  # Public: Build a scope of users based on the query object that gets passed
  # in. Apply clauses to the relation we build up based on the state of the
  # query object.
  #
  # Drawing a lot of inspiration from https://thoughtbot.com/blog/using-yieldself-for-composable-activerecord-relations
  #
  # Returns: ActiveRecord::Relation of Users
  def call
    User
      .order("users.login")
      .then(&method(:two_factor_disabled_clause))
      .then(&method(:two_factor_enabled_clause))
      .then(&method(:external_identity_linked_clause))
      .then(&method(:external_identity_unlinked_clause))
  end

  private

  def two_factor_disabled_clause(relation)
    if @query.two_factor_disabled_scope?
      relation
        .includes(:two_factor_credential)
        .where("two_factor_credentials.id IS NULL")
        .references(:two_factor_credential)
    else
      relation
    end
  end

  def two_factor_enabled_clause(relation)
    if @query.two_factor_enabled_scope?
      relation
        .includes(:two_factor_credential)
        .where("two_factor_credentials.id IS NOT NULL")
        .references(:two_factor_credential)
    else
      relation
    end
  end

  def external_identity_linked_clause(relation)
    if @query.external_identity_linked_scope?
      relation
        .includes(:external_identities)
        .where("external_identities.provider_id" => @query.organization.external_identity_session_owner.saml_provider.id,
               "external_identities.provider_type" => @query.organization.external_identity_session_owner.saml_provider.class.name)
        .references(:external_identities)
    else
      relation
    end
  end

  def external_identity_unlinked_clause(relation)
    if @query.external_identity_unlinked_scope?
      provider = @query.organization.external_identity_session_owner.saml_provider
      users_with_linked_identity = ExternalIdentity.by_provider(provider).distinct.pluck(:user_id)

      if users_with_linked_identity.empty?
        relation.includes(:external_identities)
      else
        relation
          .includes(:external_identities)
          .where("external_identities.provider_id IS NULL OR external_identities.user_id NOT IN (?)", users_with_linked_identity)
          .references(:external_identities)
      end
    else
      relation
    end
  end
end
