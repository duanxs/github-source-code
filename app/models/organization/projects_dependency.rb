# frozen_string_literal: true

module Organization::ProjectsDependency
  extend ActiveSupport::Concern

  include Configurable::DisableOrganizationProjects
  include Configurable::DisableRepositoryProjects

  attr_writer :has_repository_projects
  attr_writer :has_organization_projects

  alias_method :projects_enabled?, :organization_projects_enabled?
  alias_method :async_projects_enabled?, :async_organization_projects_enabled?

  included do
    scope :with_organization_projects_enabled, -> {
      joins(<<~SQL).
        LEFT OUTER JOIN `configuration_entries` `org_project_config` /* cross-schema-domain-query-exempted */
          ON `org_project_config`.`target_type` = 'User'
         AND `org_project_config`.`target_id` = `users`.`id`
         AND `org_project_config`.`name` = '#{::Configurable::DisableOrganizationProjects::KEY}'
         AND `org_project_config`.`value` = 'TRUE'
      SQL
      where(<<~SQL)
        `org_project_config`.`value` IS NULL
      SQL
    }
  end

  def visible_projects_for(user)
    GitHub.dogstats.time("project_permissions.visible_projects_for", tags: ["owner_type:organization"]) do
      # Logged-out users can only see public projects.
      return projects.where(public: true) if user.nil?

      # Bots with the correct permissions can see all of the organization's
      # projects.
      return projects.scoped if user.can_have_granular_permissions? && resources.organization_projects.readable_by?(user.installation)

      # Org owners can see all of the organization's projects
      return projects.scoped if adminable_by?(user)

      # All other users need to go through abilities
      ids = Authorization.service.subject_ids(
        actor: user.ability_delegate,
        subject_type: Project,
        through: [Team, Organization],
      )

      projects.where("public = 1 OR id IN (?)", ids)
    end
  end

  def writable_projects_for(user)
    GitHub.dogstats.time("project_permissions.writable_projects_for", tags: ["owner_type:organization"]) do
      # Logged out users can't write to any projects.
      return Project.none if user.nil?

      # Bots with the correct permissions can write to all of the organization's
      # projects.
      return projects.scoped if user.can_have_granular_permissions? && resources.organization_projects.writable_by?(user.installation)

      # Org owners can write to all of the organization's projects
      return projects.scoped if adminable_by?(user)

      # All other users need to go through abilities
      ids = Authorization.service.subject_ids(
        actor: user.ability_delegate,
        subject_type: Project,
        through: [Team, Organization],
        actions: [:write, :admin],
      )

      projects.where(id: ids)
    end
  end
end
