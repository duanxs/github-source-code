# frozen_string_literal: true

class CheckRun < ApplicationRecord::Ballast
  require_dependency "check_run_action"

  areas_of_responsibility :checks

  serialize :images, Array
  serialize :actions, Array

  include Instrumentation::Model
  include GitHub::Relay::GlobalIdentification
  include CheckRun::CodeScanningDependency

  extend GitHub::Encoding
  force_utf8_encoding :name, :display_name, :text, :summary, :title

  extend GitHub::BackgroundDependentDeletes

  belongs_to :check_suite
  belongs_to :creator, class_name: "User"
  # TODO: we currently don't set a value to this field in the db
  # and we do delegate `github_app` to the check_suite. Keeping here temporarily
  # until we decide what the longer term approach should be.
  # belongs_to :github_app, :foreign_key => :performed_via_app_id, :class_name => "Integration"
  has_many :annotations, class_name: "CheckAnnotation", inverse_of: :check_run
  destroy_dependents_in_background :annotations

  has_many :steps,
    -> { order(:number) },
    class_name: "CheckStep",
    inverse_of: :check_run,
    autosave: :true
  destroy_dependents_in_background :steps

  enum status: { requested: -1, queued: 0, in_progress: 1, completed: 2 }
  enum conclusion: {
    neutral: 0,
    success: 1,
    failure: 2,
    cancelled: 3,
    action_required: 4,
    timed_out: 5,
    skipped: 6,
    stale: 7,
  }

  IMAGES_BYTESIZE_LIMIT = 65_535
  TEXT_BYTESIZE_LIMIT = 65_535
  MAX_ACTIONS_QUANTITY = 3

  SUMMARY_BYTESIZE_LIMIT = 65_535

  # MAX_NUMBER_VALUE = 2147483647, the size of a INT(11) value in MySQL
  # Calculated by getting the biggest value for 32bits, divided by 2 to get the max signed value
  # and substracting one
  MAX_NUMBER_VALUE = (1 << 32) / 2 - 1

  # Retrieve Check names that have been posted to the repository
  # recently.
  #
  # Returns a Set of Strings.
  def self.recent_check_names(repo_id:, start:, limit:)
    # If you change the timeframe here, be sure to update the user-visible
    # copy on the Protected Branch settings page.

    # Querying the ids first as a separate subquery because we want to limit it
    # without the DISTINCT, and it's not possible to do a subquery with LIMIT
    ids = github_sql.values <<-SQL, repo_id: repo_id, limit: limit
      SELECT id
        FROM check_suites
        WHERE repository_id = :repo_id
        ORDER BY id DESC
        LIMIT :limit
        /*GitHub-CheckRun.recent_check_ids*/
    SQL

    return [] if ids.empty?

    names = github_sql.values <<-SQL, start: start, ids: ids
      SELECT DISTINCT COALESCE(check_runs.display_name, check_runs.name)
      FROM check_runs
      WHERE check_suite_id IN :ids AND check_runs.completed_at > :start
      /*GitHub-CheckRun.recent_check_names*/
    SQL
    names.map { |name| GitHub::Encoding.try_guess_and_transcode(name) }.to_set
  end

  validates_presence_of :status
  validates_presence_of :conclusion, if: -> { completed_at.present? }
  validates_presence_of :conclusion, if: -> { completed? }

  validate :images_format_and_size_is_valid, on: [:create, :update]
  validate :size_of_summary, on: [:create, :update]
  validate :details_url_is_valid, on: [:create, :update]

  validates_length_of :actions, maximum: MAX_ACTIONS_QUANTITY, message: "exceeds a maximum quantity of #{MAX_ACTIONS_QUANTITY}"

  delegate :head_sha, :github_app, :repository, :commit, to: :check_suite

  before_validation :convert_images_to_normal_hashes
  before_save :instrument_status_changed
  before_save :set_started_at
  before_save :set_status_completed, if: -> { concluded? && !completed? }
  before_save :assign_completed_at, if: -> { concluded? && completed_at.blank? }
  before_save :completed_at_not_in_future
  before_save :overwrite_name, if: -> { display_name.present? && external_id.present? }

  after_commit :instrument_creation, on: :create
  after_commit :instrument_completion, on: [:create, :update]
  after_commit :update_suite_rollups, on: [:create, :update]
  after_commit :notify_socket_subscribers, on: [:create, :update]

  scope :for_sha_and_repository_id, ->(shas, repo_id) {
    joins(:check_suite).
    where("check_suites.head_sha IN (?) AND check_suites.repository_id = ?", shas, repo_id)
  }
  scope :for_app_id, ->(id) {
    joins(:check_suite).
    where("check_suites.github_app_id = ?", id.to_i)
  }
  scope :concluded, -> { where.not(conclusion: nil) }

  # Fetch the ids of every check suite's most recent CheckRun per sha and repository id
  #
  # Returns array of CheckRun ids.
  def self.latest_ids_for_sha_and_repository_id(shas, repository_id)
    binds = {
      head_shas: Array(shas),
      repository_id: repository_id,
    }

    github_sql.values <<-SQL, binds
      SELECT MAX(check_runs.id)
        FROM check_runs
          JOIN check_suites
          ON check_runs.check_suite_id = check_suites.id
        WHERE check_suites.head_sha IN :head_shas
          AND check_suites.repository_id = :repository_id
        GROUP BY check_runs.name, check_runs.check_suite_id
    SQL
  end

  def self.latest_for_sha_and_event_in_repository(shas, repository_id)
    binds = {
      head_shas: Array(shas),
      repository_id: repository_id,
    }

    # For Actions we want only the last (not hidden) check suite for a workflow file and event.
    # For any other github app, we want all check suites
    check_suite_ids = github_sql.values <<-SQL, binds
      SELECT MAX(check_suites.id)
        FROM check_suites
        WHERE check_suites.head_sha IN :head_shas
          AND check_suites.repository_id = :repository_id
          AND check_suites.hidden = FALSE
          AND check_suites.workflow_file_path IS NOT NULL
        GROUP BY check_suites.github_app_id, check_suites.workflow_file_path, check_suites.event

      UNION

      SELECT check_suites.id
        FROM check_suites
        WHERE check_suites.head_sha IN :head_shas
          AND check_suites.repository_id = :repository_id
          AND check_suites.workflow_file_path IS NULL
    SQL

    return [] if check_suite_ids.empty?

    check_run_ids = github_sql.values <<-SQL, ids: check_suite_ids
      SELECT MAX(check_runs.id)
        FROM check_runs
        WHERE check_runs.check_suite_id IN :ids
        GROUP BY check_runs.name, check_runs.check_suite_id
    SQL

    where(id: check_run_ids)
  end

  # Fetch every check suite's most recent CheckRun per sha and repository id
  #
  # Returns ActiveRecord::Relation
  def self.latest_for_sha_and_repository_id(shas, repository_id)
    ids = latest_ids_for_sha_and_repository_id(shas, repository_id)

    where(id: ids)
  end

  # Returns an array of check_runs
  def self.check_runs_at_merge(repository_id:, sha:, merged_at:)
    ids = github_sql.values <<-SQL, repository_id: repository_id, sha: sha, merged_at: merged_at
      SELECT MAX(check_runs.id)
        FROM check_runs
          JOIN check_suites
          ON check_runs.check_suite_id = check_suites.id
        WHERE check_suites.head_sha = :sha
          AND check_suites.repository_id = :repository_id
          AND check_runs.created_at < :merged_at
        GROUP BY check_runs.name, check_runs.check_suite_id
    SQL
    where(id: ids)
  end

  # The GitHub web URL for details about this CheckRun.
  #
  # pull - A PullRequest instance. If present, returns the pull request permalink
  #
  # Examples:
  #
  #   check_run.permalink
  #   # => `/github/github/runs/4
  #
  #   check_run.permalink(pull: pull)
  #   # => /github/github/pulls/1/checks?check_run_id=2
  #
  def permalink(pull: nil, include_host: nil, check_suite_focus: false)
    if pull
      "#{pull.permalink(include_host: false)}/checks?check_run_id=#{id}"
    elsif check_suite_focus
      "#{repository.permalink(include_host: include_host)}/runs/#{id}?check_suite_focus=true"
    else
      "#{repository.permalink(include_host: include_host)}/runs/#{id}"
    end
  end

  def event_payload
    {
      event_prefix => self,
    }
  end

  def creator
    super || User.ghost
  end

  def duration
    return 0 unless completed_at

    (completed_at - (started_at || completed_at)).floor
  end

  def instrument_status_changed
    return unless status_changed? || conclusion_changed?
    return if new_record?

    GlobalInstrumenter.instrument "check_run.status_changed", {
      check_run_id: id,
      previous_status: status_was,
      current_status: status,
      previous_conclusion: conclusion_was,
      current_conclusion: conclusion,
    }
  end

  def instrument_creation
    instrument :create
  end

  def instrument_completion
    instrument :complete if concluded? && saved_change_to_attribute?(:conclusion)
  end

  def rerequest(actor:)
    instrument :rerequest, event_payload.merge(actor_id: actor.id)

    check_suite.reset
  end

  # Has this CheckRun concluded?
  #
  # Returns a boolean.
  def concluded?
    conclusion.present?
  end

  def required_for_pull_request?(pull)
    async_required_for_pull_request?(pull).sync
  end

  def async_required_for_pull_request?(pull)
    return Promise.resolve(false) unless protected_branch = pull.protected_base_branch
    return Promise.resolve(false) unless protected_branch.required_status_checks_enabled?

    protected_branch.async_has_required_status_check?(visible_name)
  end

  def async_text
    Platform::Loaders::CheckRunText.load(self.id, column: :text)
  end

  def async_summary
    Platform::Loaders::CheckRunText.load(self.id, column: :summary)
  end

  def self.latest_version_of(check_run)
    binds = {
      name: GitHub::SQL::BINARY(check_run.name),
      check_suite_id: check_run.check_suite_id,
    }

    id = github_sql.value <<-SQL, binds
      SELECT `check_runs`.id FROM `check_runs`
      LEFT JOIN check_runs cr2 ON (
        check_runs.check_suite_id = cr2.check_suite_id
        AND check_runs.name = cr2.name
        AND check_runs.id < cr2.id)
      WHERE `check_runs`.`name` = :name
      AND `check_runs`.`check_suite_id` = :check_suite_id
      AND cr2.id IS NULL
    SQL

    find(id)
  end

  def details_url
    return permalink(include_host: true) if check_suite&.actions_app? || check_suite&.code_scanning_app?
    self[:details_url] || check_suite&.github_app&.url
  end

  def request_action(actor:, requested_action:)
    instrument :request_action, event_payload.merge(
      actor_id: actor.id,
      requested_action: requested_action,
    )
  end

  def annotation_count
    @annotation_count ||= annotations.count
  end

  def sort_order
    state = (conclusion || status).to_s
    [number || MAX_NUMBER_VALUE, StatusCheckRollup::STATE_SORT_ORDER[state], contextual_name]
  end

  def failed?
    status == "completed" && StatusCheckRollup::FAILURE_AND_INCOMPLETE_STATES.include?(conclusion)
  end

  def async_path_uri
    return @async_path_uri if defined?(@async_path_uri)

    @async_path_uri = async_repository.then(&:async_path_uri).then do |path_uri|
      path_uri = path_uri.dup
      path_uri.path += "/check-runs/"
      path_uri.fragment = id.to_s
      path_uri
    end
  end

  def async_repository
    async_check_suite.then do |suite|
      suite.async_repository
    end
  end

  def commit_channel
    GitHub::WebSocket::Channels.commit(check_suite.repository, check_suite.head_sha)
  end

  def channel
    GitHub::WebSocket::Channels.check_run(self)
  end

  def async_readable_by?(actor)
    async_repository.then do |repository|
      repository.async_readable_by?(actor)
    end
  end

  # Internal: Since `actions` is serialized, we can't check if it is valid without deserializing.
  def valid?(*)
    # Added `action.is_a?(CheckRunAction)` because there are some outdated actions which were
    # serialized in an inconsistent form.
    super && actions.all? do |action|
      if action.is_a?(CheckRunAction)
        action.valid?
      else
        err = ArgumentError.new("CheckRunAction was a #{action.class} instead of a CheckRunAction")
        Failbot.report(err, check_run_id: id)
        false
      end
    end
  end

  def log_update_suite_rollups(at, more_attrs = {})
    payload = { job: "UpdateSuiteRollups", at: at }
    payload.merge!(log_object)
    payload.merge!(check_suite.log_object)
    payload.merge!(more_attrs)
    GitHub::Logger.log(payload)
  end

  def seconds_to_completion
    if completed_at && started_at && completed_at >= started_at
      (completed_at - started_at).to_i
    end
  end

  # Helper method for the UI. Only Actions v2 checks have the `number` field set.
  # Also, Actions v2 that are used as a dummy check for presenting information
  # don't have that field either
  def can_have_steps?
    number.present?
  end

  # check run name as shown in the merge box
  # and dropdowns that show the list of contexts (statuses and check runs)
  def contextual_name
    full_name = visible_name
    full_name = "#{check_suite.name} / #{full_name}" if check_suite.name
    full_name = "#{full_name} (#{check_suite.event})" if check_suite.event
    full_name
  end

  def visible_name
    display_name || name
  end

  def expired_logs?
    completed_log_url && created_at + 90.days < Time.zone.now
  end

  def completed_without_logs?
    completed? && completed_log_url.nil?
  end

  # HashWithIndifferentAccess is the default type that gets given to us by the API.
  # It also takes much more space than a normal Hash when serialized to a text column.
  # Due to this, we force all hashes to be normal hashes with symbolized keys to keep data size low in the DB
  def convert_images_to_normal_hashes
    self.images = self.images.map { |i| i.to_hash.deep_symbolize_keys }
  end

  def delete_logs
    update(completed_log_url: nil, completed_log_lines: nil, streaming_log_url: nil)

    steps.each do |step|
      step.update(completed_log_url: nil, completed_log_lines: nil)
    end
  end

  private
  # Internal: Do the actual WebSocket notification
  def notify_socket_subscribers
    data = if new_record?
      {
        timestamp: created_at,
        reason: "check_run ##{id} created: #{status}",
        wait: default_live_updates_wait,
        raw_logs: completed_log_url.present?,
      }
    else
      {
        timestamp: updated_at,
        reason: "check_run ##{id} updated: #{status}",
        wait: default_live_updates_wait,
        has_steps: steps.any?,
        raw_logs: completed_log_url.present?,
      }
    end

    GitHub::WebSocket.notify_repository_channel(check_suite.repository, commit_channel, data)
    GitHub::WebSocket.notify_repository_channel(check_suite.repository, channel, data)
  end

  def images_format_and_size_is_valid
    if images.any? { |image| image[:image_url].blank? }
      errors.add(:images, "must all have image URLs provided")
    end

    # Validate JSON and not YAML as the user provides us with JSON in the API
    if images.to_json.bytesize > IMAGES_BYTESIZE_LIMIT
      errors.add(:images, "array was larger than #{IMAGES_BYTESIZE_LIMIT} bytes")
    end
  end

  def details_url_is_valid
    return unless self[:details_url]

    parsed_url = URI.parse(self[:details_url])

    unless ["http", "https"].include?(parsed_url.scheme)
      errors.add(:details_url, "must use the http or https scheme")
    end
  rescue URI::InvalidURIError
    errors.add(:details_url, "is not a valid URL")
  end

  def completed_at_not_in_future
    if self.completed_at && self.completed_at > Time.now.utc
      self.completed_at = Time.now.utc
    end
  end

  # In GitHub Actions we are migrating to using an id in `name`. But for existing checks
  # we want to keep the same `name` to prevent generating duplicates in the UI if the check reruns.
  # This is only for Actions check runs. If we allow third parties to use `display_name`
  # we should check if this is from GitHub Actions first.
  def overwrite_name
    previous = check_suite.check_runs.where(external_id: external_id).first
    return if previous.nil?

    self.name = previous.name
  end

  def log_object
    {
      check_run_id: id,
      check_run_status: status,
      check_run_conclusion: conclusion,
      check_run_created_at: created_at,
      check_run_updated_at: updated_at,
      check_run_started_at: started_at,
    }
  end

  def update_suite_rollups
    pair_id = SecureRandom.uuid
    log_update_suite_rollups("start", { pair_id: pair_id })

    check_suite.set_rollup_values!

    log_update_suite_rollups("end", { pair_id: pair_id })
  end

  def size_of_summary
    if (size = summary&.bytesize).present? && size > SUMMARY_BYTESIZE_LIMIT
      errors.add(:summary, "exceeds a maximum bytesize of #{SUMMARY_BYTESIZE_LIMIT}")
    end
  end

  def assign_completed_at
    self.completed_at ||= Time.now.utc
  end

  def set_status_completed
    self.status = "completed"
  end

  def set_started_at
    self.started_at ||= Time.now.utc
  end
end
