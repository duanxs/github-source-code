# rubocop:disable Style/FrozenStringLiteralComment

class ProtectedBranchPolicy
  class Decision
    include Instrumentation::Model

    # A Boolean indicating if the policy has passed or not
    attr_reader :policy_fulfilled

    # A Boolean indicating if the status check policy can be overridden
    attr_reader :can_override_status_checks

    # A Boolean indicating if the review policy can be overidden
    attr_reader :can_override_review_policy

    # A Boolean indicating if the required signatures can be overidden
    attr_reader :can_override_required_signatures

    # A Boolean indicating if merge conflict blocking can be overridden
    attr_reader :can_override_required_linear_history

    # An Array of Reasons indicating why the Decision failed
    attr_reader :reasons

    # A Hash of data to send to instrumentation
    attr_reader :instrumentation_payload

    def initialize(ref_update, policy_fulfilled:,
                               can_override_status_checks:,
                               can_override_review_policy: false,
                               can_override_required_signatures: false,
                               can_override_required_linear_history: false,
                               reasons: nil,
                               instrumentation_payload: nil)
      @ref_update = ref_update

      @policy_fulfilled = policy_fulfilled
      @can_override_status_checks = can_override_status_checks
      @can_override_review_policy = can_override_review_policy
      @can_override_required_signatures = can_override_required_signatures
      @can_override_required_linear_history = can_override_required_linear_history

      @reasons = reasons.sort_by(&:code) if reasons
      @instrumentation_payload = instrumentation_payload
    end

    alias_method :policy_fulfilled?, :policy_fulfilled
    alias_method :can_override_status_checks?, :can_override_status_checks
    alias_method :can_override_review_policy?, :can_override_review_policy
    alias_method :can_override_required_signatures?, :can_override_required_signatures
    alias_method :can_override_required_linear_history?, :can_override_required_linear_history

    def summary
      reasons&.map(&:summary)&.join(" ")
    end

    def message
      reasons&.map(&:message)&.join(" ")
    end

    def can_update_ref?
      policy_fulfilled? || failures_overridden?
    end

    def self.success(ref_update, can_override_status_checks: false,
                                 can_override_review_policy: false,
                                 can_override_required_signatures: false,
                                 can_override_required_linear_history: false)
      new(ref_update,
        policy_fulfilled: true,
        can_override_status_checks: can_override_status_checks,
        can_override_review_policy: can_override_review_policy,
        can_override_required_signatures: can_override_required_signatures,
        can_override_required_linear_history: can_override_required_linear_history)
    end

    def instrument_decision
      increment_result_counter
      instrument_payload
    end

    # Private: Prefix used for instrumentation events
    def event_prefix
      :protected_branch
    end

    def reason_codes
      reasons&.map(&:code)
    end

    private

    def failures_overridden?
      !policy_fulfilled? && reason_codes.sort == overridden_codes.sort
    end

    def overridden_codes
      reason_codes & overridable_codes
    end

    def overridable_codes
      @overridable_codes ||= Array.new.tap do |codes|
        codes << :review_policy_not_satisfied if can_override_review_policy?
        codes << :required_status_checks      if can_override_status_checks?
        codes << :invalid_signature           if can_override_required_signatures?
        codes << :merge_commit                if can_override_required_linear_history?
      end
    end

    def increment_result_counter
      GitHub.dogstats.increment("protected_branch.policy_check", tags: ["allowed:#{policy_fulfilled?}"])
    end

    def instrument_payload
      return if instrumentation_payload.blank?
      return if policy_fulfilled?

      instrument(instrumentation_key, instrumentation_payload.merge(
        overridden_codes: overridden_codes,
      ))
    end

    def instrumentation_key
      failures_overridden? ? :policy_override : :rejected_ref_update
    end
  end
end
