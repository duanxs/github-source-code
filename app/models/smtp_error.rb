# frozen_string_literal: true

# Public: An SMTP error to be raised while trying to communicate for mail
# delivery.
#
# Example Usage:
#
# ```
# begin
#    MyMailer.test_mail().deliver!
# rescue Errno::ECONNREFUSED
#    raise SmtpError.new($!)
# end
# ```
# will catch your delivery error and maintain the backtrace pointing back to
# `deliver!`.
class SmtpError < StandardError
  # Initializes a new SmtpError.  If you want the `backtrace` to be more
  # useful during debugging, you can pass the originator with `$?!` which
  # will help you escape any rescue block used in constructing this error.
  def initialize(originating_error = nil)
    super

    # We are setting the backtrace here, instead of relying on `raise` to ensure
    # that the appropriate backtrace is presented to `FailBot`.
    set_backtrace(originating_error.backtrace) if originating_error
  end

  # Public: The error message to be yielded to Haystack or the local developer
  def message
    if Rails.env.production?
      return MESSAGE_PRODUCTION
    end
    return MESSAGE_DEVELOPMENT
  end

  def self.delivery_method
    ActionMailer::Base.delivery_method
  end

  def self.smtp_settings
    ActionMailer::Base.smtp_settings
  end

  MESSAGE_DEVELOPMENT = <<-EOF
An error occured while trying to communicate with this host's SMTP server.
- If you are trying to work with mail locally, pass the ENABLE_EMAIL_PREVIEWS=1
  environment variable to script/server which will cause LetterOpener to take
  over.
- Delivery method is #{delivery_method} and smtp_settings are
  #{smtp_settings}.
  EOF

  MESSAGE_PRODUCTION = <<-EOF
An error occured while trying to communicate with this host's SMTP server.
- Delivery method is #{delivery_method} and smtp_settings are
  #{smtp_settings}.
  EOF
end
