# frozen_string_literal: true

class Profile < ApplicationRecord::Domain::Users
  areas_of_responsibility :user_profile

  include Spam::Spammable
  include GitHub::Validations

  BIO_MAX_LENGTH = 160
  STRING_ATTRS = [:name, :email, :blog, :company, :location]

  # https://help.twitter.com/en/managing-your-account/twitter-username-rules
  TWITTER_USERNAME_MAX_LENGTH = 15
  TWITTER_USERNAME_FORMAT = /\A\w+\z/

  include Profile::PinsDependency

  belongs_to :user, touch: true
  has_one :user_status, through: :user

  setup_spammable(:user)

  after_commit :enqueue_check_for_spam, if: :persisted?
  after_save :synchronize_search_index
  after_save :instrument_update

  validates *STRING_ATTRS, unicode3: true, length: { maximum: 255 }
  validates :bio, length: { maximum: BIO_MAX_LENGTH }, allow_blank: true
  validates :twitter_username,
    length: { maximum: TWITTER_USERNAME_MAX_LENGTH },
    format: { with: TWITTER_USERNAME_FORMAT },
    allow_nil: true
  validates :mobile_time_zone_name, length: { maximum: 40 } # Longest known value is 28, giving some buffer
  validate :time_zone_has_mapping


  delegate :hide_from_user?, to: :user

  before_validation :normalize_string_attributes

  extend GitHub::Encoding
  force_utf8_encoding :bio

  # Check the profile for spam
  #
  # options - currently unused
  def check_for_spam(options = {})
    reason = GitHub::SpamChecker.test_profile(self)
    user.safer_mark_as_spammy(reason: reason) if reason
  end

  def synchronize_search_index
    return self unless GitHub.elasticsearch_access_allowed?

    user.synchronize_search_index
  end

  CHANGED_ATTRIBUTES_TO_IGNORE = ["id", "updated_at", "user_id", "user_hidden"]

  # Public: Instrument changes after save.
  def instrument_update
    changed_attribute_names = saved_changes.keys - CHANGED_ATTRIBUTES_TO_IGNORE
    return if changed_attribute_names.empty?

    GlobalInstrumenter.instrument "profile.update", {
      actor: user,
      previous_profile: previous_instance,
      current_profile: self,
      changed_attribute_names: changed_attribute_names,
    }
  end

  def normalize_string_attributes
    STRING_ATTRS.each do |field|
      next if self[field].present?

      self[field] = self.class.column_defaults[field.to_s]
    end
  end

  def twitter_url
    return if twitter_username.blank?
    "https://twitter.com/#{twitter_username}"
  end

  private

  def previous_instance
    safe_attributes = attributes.keys & self.class.column_names

    self.class.new(
      attributes.slice(*safe_attributes).merge(saved_changes.transform_values(&:first)),
    ).tap do |instance|
      instance.id = id
      instance.readonly!
    end
  end

  def time_zone_has_mapping
    return if mobile_time_zone_name.nil?

    unless ActiveSupport::TimeZone[mobile_time_zone_name]
      errors.add(:mobile_time_zone_name, "is invalid")
    end
  end
end
