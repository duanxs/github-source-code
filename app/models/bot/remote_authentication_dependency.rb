# frozen_string_literal: true

module Bot::RemoteAuthenticationDependency
  # Public: Generate a SignedAuthToken string.
  #
  # NOTE: The Why
  #
  # We'll need to do an IntegrationInstallation lookup to fill in what IntegrationInstallation
  # the Bot is trying to use for its permissions.
  #
  # On the off chance the Bot is not part connected to the IntegrationInstallation
  # we'll return the Bot as is.
  #
  # Returns a String.
  def signed_auth_token(options = {})
    options[:data] ||= Hash.new

    if installation.present?
      options[:data][:installation_id]   = installation.id
      options[:data][:installation_type] = installation.class.to_s
    end

    super(options)
  end
end
