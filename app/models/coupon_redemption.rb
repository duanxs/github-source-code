# rubocop:disable Style/FrozenStringLiteralComment

# When a user redeems a coupon (i.e., applies it to their account)
# we create a CouponRedeption to track which users redeemed it, how
# long the coupon is active, and to update the Coupon record's usage
# counter (if it's usage count bound).
class CouponRedemption < ApplicationRecord::Domain::Users
  areas_of_responsibility :gitcoin
  belongs_to :user
  belongs_to :coupon

  before_create :set_expiration
  after_create  :decrease_coupon_limit,
                :track_coupon_redemption,
                :change_plan_subscription

  scope :expired, -> { where(expired: true) }
  scope :active,  -> { where(expired: false) }

  # Collects all coupon redemptions that are two weeks from today.
  scope :expiring_in_two_weeks, lambda { |from: GitHub::Billing.now.at_midnight|
    where("expires_at >= ? AND expires_at < ? AND expired = ?",
      from + 14.days, from + 15.days, false
    )
  }

  scope :expiring_now, lambda {
    includes(:user).where(
      "expires_at < ? AND expired = ?", GitHub::Billing.now, false
    )
  }

  scope :expiring_before, lambda { |expiration_time|
    where("expires_at < ? AND expired = ?", expiration_time, false)
  }

  # Public: Expire coupons that became stale after the previous billing cycle.
  #
  # Returns nothing.
  def self.expire!
    should_be_expired_and_user_has_been_billed.each do |coupon_redemption|
      if coupon_redemption.user
        coupon_redemption.user.expire_active_coupon
      else
        coupon_redemption.expire!
      end
    end
  end

  # Public: All coupons that are safe to expire
  #
  # We need make sure that the user's billing date has been
  # advanced past the expiration date of their coupon
  # before we can safely expire that coupon
  # because sometimes the billing system
  # might lag behind real calendar time
  #
  def self.should_be_expired_and_user_has_been_billed
    expiring_before(GitHub::Billing.now.beginning_of_day).
      select(&:expires_before_billing_date?)
  end

  # Expires an active coupon redemption by setting its `expired`
  # attribute to true. Also changes the expires_at timestamp, if
  # we must. Doesn't save or anything.
  #
  # Returns nothing
  def expire
    self.expires_at = GitHub::Billing.now if expires_at > GitHub::Billing.now
    self.expired = true
  end

  # Runs `expire` but also saves the record.
  #
  # Returns true if it saved
  # Returns false if it didn't save
  def expire!
    GitHub.dogstats.increment("coupon.expired")
    expire
    save
    change_plan_subscription
  end

  def expires_this_billing_cycle?
    self.expires_at < user.billed_on if user&.billed_on
  end

  # Public: Checks to see if the coupon redemption expired
  # since the last time the account got billed
  #
  def expired_since_last_billing?
    self.expires_at > user.previous_billing_date if user
  end

  # Public: Checks to see if the coupon redemption will expire before
  # the user's next billing date
  #
  # Returns a Boolean
  def expires_before_billing_date?
    billing_date = user&.billed_on || GitHub::Billing.today
    self.expires_at < billing_date
  end

  # Check to see if this redemption is past its expiration date
  #
  # Returns a Boolean
  def stale?
    self.expires_at < GitHub::Billing.now.beginning_of_day
  end

  def expires_at
    self[:expires_at].in_billing_timezone
  end

  private

  def change_plan_subscription
    user&.update_external_subscription!(force: true)
  end

  def set_expiration
    self[:expires_at] ||= GitHub::Billing.now + coupon.duration.days
  end

  def decrease_coupon_limit
    return if coupon.blank?
    coupon.decrement!(:limit)
  end

  def track_coupon_redemption
    if (coupon.limit + coupon.coupon_redemptions.count) >= 25
      GitHub.dogstats.increment("coupon.redeemed", tags: [
        "group:#{coupon.group.present? ? coupon.group : 'no_group'}",
        "code:#{coupon.code}",
      ])
    else
      GitHub.dogstats.increment("coupon.redeemed", tags: [
        "group:#{coupon.group.present? ? coupon.group : 'no_group'}",
      ])
    end
  end
end
