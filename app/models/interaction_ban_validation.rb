# frozen_string_literal: true

module InteractionBanValidation
  extend ActiveSupport::Concern
  include ActionView::Helpers::DateHelper

  # Public: Determine if the author is allowed to make comments
  def user_can_interact?
    return unless user && repository
    return unless GitHub.interaction_limits_enabled?
    return if repository.pushable_by?(user)
    restricted_by_repository_checks unless repository.private?
    return if User::InteractionAbility.interaction_allowed?(user.id)
    return if repository.owner.organization? && repository.owner.member?(user)

    expiry = User::InteractionAbility.ban_expiry(user.id).in_time_zone
    errors.add(
      :base,
      "ability has been suspended for #{distance_of_time_in_words(DateTime.now, expiry)}. If you believe this is in error, please contact GitHub support",
    )
  end

  private

  def restricted_by_repository_checks
    if RepositoryInteractionAbility.restricted_by_limit?(:sockpuppet_ban, repository, user)
      errors.add(
        :base,
        "could not be created. Interactions on this repository have been restricted from new users.",
      )
    elsif RepositoryInteractionAbility.restricted_by_limit?(:contributors_only, repository, user)
      errors.add(
        :base,
        "could not be created. Interactions on this repository have been restricted to prior contributors only.",
      )
    elsif RepositoryInteractionAbility.restricted_by_limit?(:collaborators_only, repository, user)
      errors.add(
        :base,
        "could not be created. Interactions on this repository have been restricted to collaborators only.",
      )
    end
  end
end
