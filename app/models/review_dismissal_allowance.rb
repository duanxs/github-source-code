# frozen_string_literal: true

class ReviewDismissalAllowance < ApplicationRecord::Domain::Repositories
  include GitHub::Relay::GlobalIdentification

  areas_of_responsibility :protected_branches

  belongs_to :protected_branch
  belongs_to :actor, polymorphic: true
  validates :protected_branch, presence: true
  validates :actor, presence: true
end
