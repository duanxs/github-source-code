# rubocop:disable Style/FrozenStringLiteralComment

# A legacy model that predates the audit log and is primarily used to hold audit
# log style information about changes to billing plans. Also provides very
# inaccurate information about revenue.
class Transaction < ApplicationRecord::Domain::Users
  areas_of_responsibility :gitcoin

  enum current_subscribable_type: [Marketplace::ListingPlan.name, SponsorsTier.name], _suffix: true
  enum old_subscribable_type: [Marketplace::ListingPlan.name, SponsorsTier.name], _suffix: true

  belongs_to  :user
  belongs_to  :billing_transaction, class_name: "Billing::BillingTransaction"
  belongs_to  :current_subscribable, polymorphic: true
  belongs_to  :old_subscribable, polymorphic: true
  before_save :set_fields

  scope :paid, -> {
    conditions = [
      "action = 'upgraded'",
      "action = 'downgraded'",
      "(action = 'signed-up' and current_plan <> 'free')",
      "(action = 'deleted' and current_plan <> 'free')",
    ].join(" or ")

    where(conditions)
  }

  scope :lost, -> {
    conditions = [
      "action = 'downgraded'",
      "(action = 'deleted' and current_plan <> 'free')",
      "(action = 'disabled' and current_plan <> 'free')",
    ].join(" or ")

    where(conditions)
  }

  scope :upgrades, -> { where(action: "upgraded") }

  scope :for_subscribables, -> (subscribables) {
    where(current_subscribable: subscribables).or(where(old_subscribable: subscribables))
  }

  attr_accessor :old_plan_duration, :params

  def initialize(*args)
    @params = args.first
    super
  end

  def current_plan
    plan = GitHub::Plan.find(super, effective_at: user.plan_effective_at)
    MunichPlan.wrap(plan, target: user)
  end

  def old_plan
    plan = GitHub::Plan.find(super, effective_at: user.plan_effective_at)
    MunichPlan.wrap(plan, target: user)
  end

  def seat_delta
    current_seats - old_seats
  end

  def old_data_packs
    asset_packs_total - asset_packs_delta
  end

  # Human compatible output for current plan name
  # Safe to use with plans that are no longer in the system like 'ey'
  #
  # Returns a string containing the humanized current plan name or `nil`
  def human_current_plan
    current_plan.display_name.humanize if current_plan
  end

  def org?
    !!user.try(:organization?)
  end

  def username
    user.try(:login)
  end

  private

  def set_fields
    self.current_plan = user.plan.try(:name)
    self.current_seats = user.seats
    self.plan_duration = user.plan_duration
    self.next_billed_on = user.billed_on
    self.timestamp = Time.now
    self.action ||= determine_action
  end

  def determine_action
    if params[:action].present? # action specified
      params[:action]
    elsif params[:old_plan].present? # change in plan
      current_plan_cost = current_plan.try(:cost) || 0
      old_plan_cost = GitHub::Plan.find(params[:old_plan]).try(:cost) || 0
      current_plan_cost > old_plan_cost ? "upgraded" : "downgraded"
    elsif params[:old_plan_duration].present? # change in plan duration
      "switched-to-#{plan_duration}ly"
    elsif params[:old_seats].present? # change in seats
      current_seats > params[:old_seats] ? "added_seats" : "removed_seats"
    elsif params[:asset_packs_delta].present? # change in data packs
      asset_packs_total > old_data_packs ? "added_asset_packs" : "removed_asset_packs"
    end
  end
end
