# frozen_string_literal: true

module Sponsors
  # Public: A Plain Old Ruby Object (PORO) used for accepting a Sponsors membership.
  class AcceptSponsorsMembership
    # inputs - Hash containing attributes to accept a membership.
    # inputs[:sponsors_membership] - The SponsorsMembership to accept.
    # inputs[:actor] - The User that is accepting this membership.
    # inputs[:automated] - (optional) A Boolean indicating if this is an
    #                      automated acceptance, defaulting to false.
    def self.call(inputs)
      new(**inputs).call
    end

    def initialize(sponsors_membership:, actor:, automated: false)
      @sponsors_membership = sponsors_membership
      @actor = actor
      @automated = automated
    end

    def call
      if !automated && !actor&.can_admin_sponsors_listings?
        return Result.failure(
          sponsors_membership: sponsors_membership,
          errors: ["Actor is not authorized to accept this membership"],
        )
      end

      if sponsors_membership.accepted?
        return Result.success(sponsors_membership: sponsors_membership)
      end

      if automated && !sponsors_membership.auto_acceptable?
        return Result.failure(
          sponsors_membership: sponsors_membership,
          errors: ["Membership is not auto acceptable"],
        )
      end

      begin
        if sponsors_membership.accept!
          instrument_acceptance
          Result.success(sponsors_membership: sponsors_membership)
        else
          Result.failure(
            sponsors_membership: sponsors_membership,
            errors: [sponsors_membership.halted_because],
          )
        end
      rescue Workflow::NoTransitionAllowed => error
        Result.failure(
          sponsors_membership: sponsors_membership,
          errors: [error.message],
        )
      end
    end

    private

    attr_accessor :sponsors_membership, :actor, :automated

    def instrument_acceptance
      GitHub.dogstats.increment("sponsors_membership.accept", tags: ["automated:#{automated}"])

      actor_context = GitHub.guarded_audit_log_staff_actor_entry(actor)
      context = {
        automated: automated,
      }.merge(sponsors_membership.event_context).merge(sponsors_membership.sponsorable.event_context).merge(actor_context)

      GitHub.instrument("sponsors_membership.accept", context)
    end

    class Result
      attr_reader :sponsors_membership, :success, :errors
      alias_method :success?, :success

      # sponsors_membership - The SponsorsMembership that is being accepted
      # success - A Boolean indicating if accepting the membership was successful
      # errors - An Array of any errors that occured when accepting a membership
      def initialize(sponsors_membership:, success:, errors:)
        @sponsors_membership = sponsors_membership
        @success = success
        @errors = errors
      end

      def self.success(sponsors_membership:)
        new(
          sponsors_membership: sponsors_membership,
          success: true,
          errors: [],
        )
      end

      def self.failure(sponsors_membership:, errors:)
        new(
          sponsors_membership: sponsors_membership,
          success: false,
          errors: errors,
        )
      end
    end
  end
end
