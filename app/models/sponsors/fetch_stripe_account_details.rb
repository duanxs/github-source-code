# frozen_string_literal: true

module Sponsors
  class FetchStripeAccountDetails
    def self.call(account)
      new(account).call
    end

    def initialize(account)
      @account = account
    end

    def call
      account_details = Stripe::Account.retrieve(account.stripe_account_id)
      account.update!(stripe_account_details: account_details.to_hash)
      account
    end

    private

    attr_reader :account
  end
end
