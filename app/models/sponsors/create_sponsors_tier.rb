# frozen_string_literal: true

# Public: A Plain Old Ruby Object (PORO) used for creating a Sponsors tier
module Sponsors
  class CreateSponsorsTier
    class UnprocessableError < StandardError; end
    class ForbiddenError < StandardError; end

    include ActionView::Helpers::NumberHelper

    # inputs - Hash containing attributes to create a sponsors tier
    # inputs[:listing_slug] - The slug used to look up the Sponsors listing.
    # inputs[:description] - A short description of the tier.
    # inputs[:monthly_price_in_cents] - How much this tier should cost monthly in cents.
    # inputs[:yearly_price_in_cents] - How much this tier should cost annually in cents.
    # inputs[:viewer] - Current viewer from GraphQL context.
    def self.call(inputs)
      new(**inputs).call
    end

    def initialize(listing_slug:, description:, monthly_price_in_cents:,
                   yearly_price_in_cents:, viewer:)
      @listing_slug = listing_slug
      @description = description
      @monthly_price_in_cents = monthly_price_in_cents
      @yearly_price_in_cents = yearly_price_in_cents
      @viewer = viewer
    end

    def call
      sponsors_listing = SponsorsListing.find_by_slug(listing_slug)
      raise UnprocessableError.new("Could not find Sponsors listing with slug #{listing_slug}") unless sponsors_listing

      tier = SponsorsTier.new(sponsors_listing: sponsors_listing)
      unless tier.editable_by?(viewer)
        raise ForbiddenError.new("#{viewer} does not have permission to create a Sponsors tier for the listing #{listing_slug}.")
      end

      tier.name = name
      tier.description = description
      tier.monthly_price_in_cents = monthly_price_in_cents
      tier.yearly_price_in_cents = yearly_price_in_cents

      if tier.save
        tier
      else
        errors = tier.errors.full_messages.join(", ")
        raise UnprocessableError.new("Could not create new Sponsors tier: #{errors}")
      end
    end

    private

    attr_reader :listing_slug, :description, :monthly_price_in_cents,
                :yearly_price_in_cents, :viewer

    def name
      "#{number_to_currency(monthly_price_in_cents / 100, precision: 0)} a month"
    end
  end
end
