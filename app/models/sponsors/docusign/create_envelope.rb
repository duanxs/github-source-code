# frozen_string_literal: true

require "docusign_esign"

module Sponsors
  module Docusign
    class CreateEnvelope
      class InvalidMembershipError < StandardError; end

      def self.call(sponsorable:, country_code: nil, template_id: nil)
        new(
          sponsorable: sponsorable,
          country_code: country_code,
          template_id: template_id,
        ).call
      end

      def initialize(sponsorable:, country_code:, template_id: nil)
        @sponsorable = sponsorable
        @country_code = country_code
        @template_id = template_id
      end

      def call
        if existing_envelope = sponsorable.active_docusign_envelope
          return existing_envelope
        end

        if !country_code && !template_id
          raise ::Sponsors::Docusign::TemplateError.new("Must provide either a country code or template ID")
        end

        envelope = create_envelope!

        GitHub.dogstats.increment("docusign",
          tags: [
            "source:sponsors",
            "action:create_envelope",
            "country:#{country_code}",
          ])

        envelope
      end

      private

      attr_reader :sponsorable, :country_code, :template_id

      def create_envelope!
        legal_name = sponsors_membership.legal_name
        email = sponsorable.sponsors_contact_email

        if !legal_name || !email
          raise InvalidMembershipError.new("This member has not provided their legal name and email")
        end

        envelope = ::Docusign::CreateEnvelope.call \
          name: legal_name,
          email: email,
          subject: "Action required: GitHub Sponsors Tax ID forms",
          template_id: document.template_id

        DocusignEnvelope.create!(
          owner: sponsors_listing,
          envelope_id: envelope.envelope_id,
          document_type: document.type,
          status: ::Docusign::STATUS_SENT,
        )
      end

      def document
        @document ||= if template_id
          find_template_from_id
        else
          find_template_from_country_code
        end
      end

      def find_template_from_id
        [
          ::Sponsors::Docusign::Document::W9,
          ::Sponsors::Docusign::Document::W_8BEN,
          ::Sponsors::Docusign::Document::W_8BEN_E,
        ].find { |template| template.template_id == template_id }
      end

      def find_template_from_country_code
        if country_code == "US"
          ::Sponsors::Docusign::Document::W9
        else
          if sponsorable.user?
            ::Sponsors::Docusign::Document::W_8BEN
          else
            ::Sponsors::Docusign::Document::W_8BEN_E
          end
        end
      end

      def sponsors_listing
        @sponsors_listing ||= sponsorable.sponsors_listing
      end

      def sponsors_membership
        @sponsors_membership ||= sponsorable.sponsors_membership
      end
    end
  end
end
