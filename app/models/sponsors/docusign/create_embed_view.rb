# frozen_string_literal: true

require "docusign_esign"

module Sponsors
  module Docusign
    class CreateEmbedView
      def self.call(sponsorable:, envelope:, return_url:)
        sponsors_membership = sponsorable.sponsors_membership

        embed_view = ::Docusign::CreateEmbedView.call \
          name: sponsors_membership.legal_name,
          email: sponsorable.sponsors_contact_email,
          envelope_id: envelope.envelope_id,
          return_url: return_url

        GitHub.dogstats.increment("docusign",
          tags: [
            "source:sponsors",
            "action:create_embed_view",
          ])

        embed_view
      end
    end
  end
end
