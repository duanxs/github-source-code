# frozen_string_literal: true
# rubocop:disable Naming/ClassAndModuleCamelCase

module Sponsors::Docusign
  module Document
    class W9
      def self.type
        :w9
      end

      def self.template_id
        GitHub.docusign_w9_template_id
      end
    end

    class W_8BEN
      def self.type
        :w_8ben
      end

      def self.template_id
        GitHub.docusign_w8ben_template_id
      end
    end

    class W_8BEN_E
      def self.type
        :w_8ben_e
      end

      def self.template_id
        GitHub.docusign_w8ben_e_template_id
      end
    end
  end
end
