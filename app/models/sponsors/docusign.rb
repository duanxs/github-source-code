# frozen_string_literal: true

module Sponsors::Docusign
  class TemplateError < StandardError; end
end
