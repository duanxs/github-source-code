# frozen_string_literal: true

# Public: A Plain Old Ruby Object (PORO) used for creating a Sponsors listing
module Sponsors
  class CreateSponsorsListing
    class UnprocessableError < StandardError; end
    class ResourceMissingError < StandardError; end

    # inputs - Hash containing attributes to create a sponsors listing
    # inputs[:sponsorable] - The user for which the listing is being created
    # inputs[:name] - The name of the user
    # inputs[:email] - The UserEmail to use as a contact email for the user
    def self.call(inputs)
      new(**inputs).call
    end

    def initialize(sponsorable:, name:, country_code:, email:)
      @sponsorable = sponsorable
      @name = name
      @country_code = country_code
      @email = email
    end

    # Public: Creates a sponsors listing.
    #
    # Returns a SponsorsListing.
    # Raises UnprocessableError if the sponsorable is spammy, or if creation fails.
    # Raises ResourceMissing in development if required categories are missing.
    def call
      ActiveRecord::Base.transaction do
        if sponsorable.spammy?
          raise UnprocessableError.new(
            "Your account is flagged and unable to enroll in Sponsors. Please contact support to have your account reviewed.",
          )
        end

        membership = sponsorable.sponsors_membership
        membership.contact_email = email
        membership.billing_country = country_code
        membership.legal_name = name

        unless membership.save
          errors = membership.errors.full_messages.to_sentence
          raise UnprocessableError.new(errors)
        end

        slug = sponsorable.sponsors_listing_slug

        sponsors_listing = SponsorsListing.new(
          slug: slug,
          sponsorable: sponsorable,
          short_description: "Support #{sponsorable.login}’s open source work",
          sponsors_membership: membership,
        )

        if sponsors_listing.exempt_from_payout_probation?
          probation_timestamp = Time.now
          sponsors_listing.payout_probation_started_at = probation_timestamp
          sponsors_listing.payout_probation_ended_at = probation_timestamp
        end

        unless sponsors_listing.save
          errors = sponsors_listing.errors.full_messages.join(", ")
          raise UnprocessableError.new("Could not create new Sponsors listing for #{slug}: #{errors}")
        end

        sponsors_listing
      end
    end

    private

    attr_reader :sponsorable, :name, :country_code, :email
  end
end
