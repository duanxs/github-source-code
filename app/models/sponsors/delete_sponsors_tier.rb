# frozen_string_literal: true

module Sponsors

  # Public: A Plain Old Ruby Object (PORO) used for deleting an existing Sponsors tier.
  class DeleteSponsorsTier
    class UnprocessableError < StandardError; end
    class ForbiddenError < StandardError; end

    # inputs - Hash containing attributes to delete a sponsors tier
    # inputs[:tier] - The sponsorship tier to delete.
    # inputs[:viewer] - Current viewer from GraphQL context.
    def self.call(inputs)
      new(**inputs).call
    end

    def initialize(tier:, viewer:)
      @tier = tier
      @viewer = viewer
    end

    def call
      unless tier.deletable_by?(viewer)
        raise ForbiddenError.new("#{viewer} does not have permission to delete the tier.")
      end

      unless tier.destroy
        errors = tier.errors.full_messages.join(", ")
        raise UnprocessableError.new("Could not delete tier: #{errors}")
      end
    end

    private

    attr_accessor :tier, :viewer
  end
end
