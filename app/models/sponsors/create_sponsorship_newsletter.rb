# frozen_string_literal: true

# Public: A Plain Old Ruby Object (PORO) used for creating a sponsorship newsletter
module Sponsors
  class CreateSponsorshipNewsletter
    class UnprocessableError < StandardError; end

    class Result
      attr_reader :success, :newsletter, :error
      alias_method :success?, :success

      def initialize(newsletter:, success:, error:)
        @success = success
        @newsletter = newsletter
        @error = error
      end

      def self.success(newsletter)
        new(
          error: nil,
          success: true,
          newsletter: newsletter,
        )
      end

      def self.failure(error)
        new(
          error: error,
          success: false,
          newsletter: nil,
        )
      end
    end

    def self.call(inputs)
      new(**inputs).call
    end

    def initialize(sponsorable:, author:, draft: false, body:, subject:, tier_ids: [])
      @sponsorable = sponsorable
      @author = author
      @draft = draft
      @body = body
      @subject = subject
      @tier_ids = tier_ids
    end

    def call
      newsletter = SponsorshipNewsletter.new(
        sponsorable: sponsorable,
        author: author,
        state: draft ? :draft : :published,
        body: body,
        subject: subject,
      )

      if tiers = sponsors_listing.sponsors_tiers.where(id: tier_ids)
        newsletter.sponsors_tiers = tiers
      end

      unless newsletter.save
        errors = newsletter.errors.full_messages.join(", ")
        error = UnprocessableError.new("Could not create update: #{errors}")
        return Result.failure(error)
      end

      Result.success(newsletter)
    end

    private

    attr_reader :sponsorable, :author, :draft, :body, :subject, :tier_ids

    def sponsors_listing
      sponsorable.sponsors_listing
    end
  end
end
