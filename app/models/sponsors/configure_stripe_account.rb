# frozen_string_literal: true

# A Plain Old Ruby Object (PORO) used for updating a Stripe Connect account.
module Sponsors
  class ConfigureStripeAccount
    class UpdatePayoutsFailedError < StandardError; end;

    # inputs - Hash containing attributes for updating a Stripe Connect account.
    # inputs[:account] - The Billing::StripeConnect::Account to update the configuration for.
    # inputs[:freeze_payouts] - A Boolean indicating if payouts should be frozen.
    def self.call(inputs)
      new(**inputs).call
    end

    def initialize(account:, freeze_payouts: false)
      @account = account
      @freeze_payouts = freeze_payouts
    end

    # Public: Updates the configuration for a Stripe Connect account.
    #
    # Returns the Billing::StripeConnect::Account that was updated.
    # Raises Stripe::APIConnectionError if connecting to the Stripe API fails.
    # Raises Stripe::StripeError if updating the Stripe account fails.
    def call
      response = Stripe::Account.update(account.stripe_account_id, {
        settings: {
          payouts: {
            schedule: payout_schedule,
          },
        },
      })

      account.update!(stripe_account_details: response.as_json)

      if freeze_payouts && !account.automated_payouts_disabled?
        raise UpdatePayoutsFailedError
      end

      if !freeze_payouts && account.automated_payouts_disabled?
        raise UpdatePayoutsFailedError
      end

      account
    end

    private

    attr_accessor :account, :freeze_payouts

    # Private: The payout schedule to use for this account.
    #
    # Returns a Hash.
    def payout_schedule
      if freeze_payouts
        { interval: "manual" }
      else
        { interval: "monthly", monthly_anchor: 22 }
      end
    end
  end
end
