# frozen_string_literal: true

# Public: A Plain Old Ruby Object (PORO) used for unpublishing a Sponsors listing
module Sponsors
  class UnpublishSponsorsListing
    class UnprocessableError < StandardError; end
    class ForbiddenError < StandardError; end

    # inputs - Hash containing attributes to unpublish a sponsors listing
    # inputs[:listing] - The listing being unpublished
    # inputs[:viewer] - Current viewer from GraphQL context
    def self.call(inputs)
      new(**inputs).call
    end

    def initialize(listing:, message: nil, viewer:)
      @sponsors_listing = listing
      @message = message # TODO: Send this along via email
      @viewer = viewer
    end

    def call
      unpublished_listing = unpublish_sponsors_listing
      { sponsors_listing: unpublished_listing }
    end

    private

    attr_accessor :sponsors_listing, :message, :viewer

    def unpublish_sponsors_listing
      unless viewer.can_admin_sponsors_listings?
        raise ForbiddenError.new("#{viewer} does not have permission to unpublish the listing.")
      end

      if sponsors_listing.can_unpublish?
        sponsors_listing.unpublish!
        sponsors_listing
      else
        raise UnprocessableError.new("Listing cannot be unpublished.")
      end
    end
  end
end
