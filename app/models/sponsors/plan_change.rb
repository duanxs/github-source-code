# frozen_string_literal: true

# Note: This wraps a Billing::PlanChange, which
# is a transient object that is 100% dependent on `tier`.
# Does not apply to tier selection partial.
#
# Calculate a potential plan by adding `tier` to the current user's
# existing subscription items
#
# It's not considered a plan change when the selected tier is equal to the
# current tier

module Sponsors
  class PlanChange
    def initialize(sponsor:, sponsorship: nil, new_tier: nil, listing:)
      @sponsor = sponsor
      @new_tier = new_tier
      @sponsorship = sponsorship
      @listing = listing
    end

    # If there's a plan change, return the item that has changed between the
    # existing plan and the "new" plan.
    def item
      return unless plan_change?
      return @plan_change_item if defined?(@plan_change_item)

      @plan_change_item = plan_change
        .new_subscription
        .subscription_items
        .detect { |item| item.subscribable == new_tier }
    end

    def price
      plan_change.final_price
    end

    private

    # If the new tier is the same as the existing sponsorship tier the
    # underlying Billing::PlanChange object is useless. We only care when we're
    # calculating a plan change for a new tier
    def plan_change?
      return false if new_tier.nil?
      return false if new_tier == sponsorship&.tier
      true
    end

    def plan_change
      @plan_change ||= begin
        new_item = Billing::SubscriptionItem.new(plan_subscription: sponsor.plan_subscription)
        new_item.assign_attributes(quantity: 1, subscribable: new_tier)

        # We need to preload :sponsors_listing and :listing to prevent
        # n+1 for SponsorTier and Marketplace::Plan respectively. The associations
        # aren't consistently named on subsribable, so here we are.
        existing_subscription_items = sponsor
          .subscription_items
          .active
          .preload(subscribable: [:sponsors_listing, :listing])

        subscription_items = existing_subscription_items
          .reject do |item|
            item.listing == listing
        end

        new_items = subscription_items + [new_item]
        new_subscription = Billing::Subscription.for_account(
          sponsor,
          subscription_items: new_items
        )

        Billing::PlanChange.new(sponsor.subscription, new_subscription)
      end
    end

    attr_reader :sponsorship, :new_tier, :sponsor, :listing
  end
end
