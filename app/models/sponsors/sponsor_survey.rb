# frozen_string_literal: true

module Sponsors
  class SponsorSurvey
    include ActiveModel::Validations

    validate :must_not_be_completed_already

    def initialize(sponsor:, sponsorable:, submitted_answers: nil)
      @sponsor = sponsor
      @sponsorable = sponsorable
      @submitted_answers = submitted_answers
    end

    def save
      return false unless valid?

      survey_group = SurveyAnswer.save_as_group \
        sponsor.id, survey.id, answers

      if survey_group
        instrument_submitted(survey_group)
        complete!
        survey_group
      else
        errors.add(:base, "answers were invalid")
        false
      end
    end

    def survey
      @survey ||= Survey.find_by_slug(slug)
    end

    def completed?
      ActiveRecord::Base.connected_to(role: :reading) do
        GitHub.kv.get(key).value!.present?
      end
    end

    def complete!
      if !completed?
        GitHub.kv.set(key, Time.now.utc.iso8601)
      end
    end

    private

    attr_reader :sponsor, :sponsorable, :submitted_answers

    def key
      [
        "sponsors",
        "sponsor_survey",
        slug,
        [sponsor.class, sponsor.id].join,
        [sponsorable.class, sponsorable.id].join,
      ].join(".")
    end

    def must_not_be_completed_already
      return unless completed?
      errors.add(:base, "you already completed this survey")
    end

    def total_accounts_sponsoring
      @total_accounts_sponsoring ||= sponsor.sponsorships_as_sponsor.active.count
    end

    def slug
      raise NotImplementedError, "slug should be implemented by subclass"
    end

    def answers
      raise NotImplementedError, "answers should be implemented by subclass"
    end

    def instrument_submitted(survey_group)
      raise NotImplementedError, "instrument_submitted should be implemented by subclass"
    end
  end
end
