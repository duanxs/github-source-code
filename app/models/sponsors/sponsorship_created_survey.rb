# frozen_string_literal: true

module Sponsors
  class SponsorshipCreatedSurvey < SponsorSurvey
    validate :must_answer_discovery_question
    validate :must_answer_influence_question
    validate :must_answer_why_sponsor_question

    def influence_question
      @influence_question ||= survey_questions.
        find { |question| question.short_text == Sponsors::PostSponsorshipSurvey::INFLUENCE_QUESTION_SHORT_TEXT }
    end

    def discovery_question
      @discovery_question ||= survey_questions.
        find { |question| question.short_text == Sponsors::PostSponsorshipSurvey::DISCOVERY_QUESTION_SHORT_TEXT }
    end

    def why_sponsor_question
      @why_sponsor_question ||= survey_questions.
        find { |question| question.short_text == Sponsors::PostSponsorshipSurvey::WHY_SPONSOR_QUESTION_SHORT_TEXT }
    end

    private

    def slug
      Sponsors::PostSponsorshipSurvey::SLUG
    end

    def instrument_submitted(survey_group)
      GlobalInstrumenter.instrument("sponsors.post_sponsorship_survey_submitted", {
        actor: sponsor,
        sponsorable: sponsorable,
        survey_id: survey_group.survey_id,
        survey_group_id: survey_group.id,
        total_accounts_sponsoring: total_accounts_sponsoring,
        tier: sponsor.sponsorship_as_sponsor_for(sponsorable).tier,
      })
    end

    def answers
      submitted_answers + metadata_answers
    end

    def metadata_answers
      [
        {
          question_id: sponsorable_question.id,
          choice_id: sponsorable_question.choices.find_by!(short_text: "other").id,
          other_text: sponsorable.id,
        },
        {
          question_id: sponsored_tier_question.id,
          choice_id: sponsored_tier_question.choices.find_by!(short_text: "other").id,
          other_text: sponsor.sponsorship_as_sponsor_for(sponsorable).tier.monthly_price_in_cents,
        },
        {
          question_id: total_accounts_sponsoring_question.id,
          choice_id: total_accounts_sponsoring_question.choices.find_by!(short_text: "other").id,
          other_text: total_accounts_sponsoring,
        },
      ]
    end

    def survey_questions
      @survey_questions ||= survey.questions.select(:id, :short_text, :text).to_a
    end

    def sponsorable_question
      @sponsorable_question ||= survey_questions.
        find { |question| question.short_text == Sponsors::PostSponsorshipSurvey::SPONSORABLE_QUESTION_SHORT_TEXT }
    end

    def sponsored_tier_question
      @sponsored_tier_question ||= survey_questions.
        find { |question| question.short_text == Sponsors::PostSponsorshipSurvey::SPONSORED_TIER_QUESTION_SHORT_TEXT }
    end

    def total_accounts_sponsoring_question
      @total_accounts_sponsoring_question ||= survey_questions.
        find { |question| question.short_text == Sponsors::PostSponsorshipSurvey::TOTAL_ACCOUNTS_SPONSORING_QUESTION_SHORT_TEXT }
    end

    def influence_answers
      @influence_answers ||= submitted_answers.
        select { |answer| answer[:question_id].to_i == influence_question.id }
    end

    def discovery_answers
      @discovery_answers ||= submitted_answers.
        select { |answer| answer[:question_id].to_i == discovery_question.id }
    end

    def why_sponsor_answers
      @why_sponsor_answers ||= submitted_answers.
        select { |answer| answer[:question_id].to_i == why_sponsor_question.id }
    end

    def must_answer_influence_question
      return if influence_answers.any?
      errors.add(:base, "“#{influence_question.text}” was not answered")
    end

    def must_answer_discovery_question
      return if discovery_answers.any?
      errors.add(:base, "“#{discovery_question.text}” was not answered")
    end

    def must_answer_why_sponsor_question
      return if why_sponsor_answers.map { |answer| answer[:other_text] }.any?(&:present?)
      errors.add(:base, "“#{why_sponsor_question.text}” was not answered")
    end
  end
end
