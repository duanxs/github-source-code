# rubocop:disable Style/FrozenStringLiteralComment

class CommentEmail < ApplicationRecord::Domain::Emails
  belongs_to :comment, polymorphic: true

  def message_id=(str)
    write_attribute :message_id, str.blank? ?
      str : str.gsub(/^<|>$/, "")
  end
end
