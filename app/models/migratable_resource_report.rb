# frozen_string_literal: true

class MigratableResourceReport < ApplicationRecord::Domain::Migrations
  areas_of_responsibility :migration

  belongs_to :migration

  validates :migration, presence: true
  validates :model_type, presence: true
  validates :total_count, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :success_count, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :failure_count, presence: true, numericality: { greater_than_or_equal_to: 0 }

end
