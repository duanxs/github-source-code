# frozen_string_literal: true

# Used by Actions Runtime to determine max concurrency + abuse monitoring
# Accessed via GraphQL
class ActionsPlanOwner
  attr_reader :owner, :repository

  ENTERPRISE = "enterprise"
  ENTERPRISE_TRIAL = "enterprise_trial"
  FREE_ORGANIZATION = "free_organization"
  TEAM = "team"
  FREE = "free"
  PRO = "pro"

  def initialize(owner)
    @owner = owner
  end

  def id
    owner.global_relay_id
  end

  def type
    owner.class.name
  end

  # Valid values are:
  # - free
  # - free_organization
  # - pro
  # - team
  # - enterprise
  #
  # These are used by Actions Service/Compute to:
  #   - Determine build concurrency
  #   - Determine Abuse risk (is the account paid or not?)
  #
  # If you need to change these values, or add a new plan. Please coordinate with them.
  def async_plan_name
    if owner.plan.business_plus? || owner.plan.enterprise?
      trial = Billing::PlanTrial.find_by(
        user: owner,
        plan: GitHub::Plan::BUSINESS_PLUS,
      )

      return Promise.resolve(ENTERPRISE) unless trial

      # For free trials, we return "enterprise_trial", to prevent abuse
      return trial.async_pending_plan_change.then do |_plan_change|
        trial.active? ? ENTERPRISE_TRIAL : ENTERPRISE
      end
    end

    return Promise.resolve(PRO) if owner.plan.pro?
    return Promise.resolve(TEAM) if owner.plan.business?

    if owner.plan.free? || owner.plan.free_with_addons?
      return Promise.resolve(FREE_ORGANIZATION) if owner.organization?
      return Promise.resolve(FREE)
    end

    # ^ These are the official plans that support Actions.
    #
    # We still have some legacy plans that have access to Actions that need to be migrated.
    # So we return free for those because Free plans have the least abilities.

    Promise.resolve(FREE)
  end

  def plan_name
    async_plan_name.sync
  end

  def name
    return owner.slug if owner.is_a? Business
    owner.login
  end
end
