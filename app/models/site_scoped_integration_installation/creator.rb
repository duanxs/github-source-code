# frozen_string_literal: true

class SiteScopedIntegrationInstallation
  class Creator
    INSTALL_ON_ALL_REPOSITORIES = :all
    DEFAULT_BATCH_SIZE = 1_000
    EXPIRATION_WINDOW = 2.hours

    attr_reader :integration, :target, :repositories, :permissions, :version

    class Result
      class Error < StandardError; end

      def self.success(installation) new(:success, installation: installation) end
      def self.failed(error) new(:failed, error: error) end

      attr_reader :error, :installation

      def initialize(status, installation: nil, error: nil)
        @status       = status
        @installation = installation
        @error        = error
      end

      def success?
        @status == :success
      end

      def failed?
        @status == :failed
      end
    end

    def self.perform(integration, target, repositories: [], permissions: {}, expires: true)
      new(integration, target, repositories: repositories, permissions: permissions, expires: expires).perform
    end

    def initialize(integration, target, repositories: [], permissions: {}, expires: true)
      @integration = integration
      @target = target
      @repositories = repositories
      @expires = expires
      @expiry = EXPIRATION_WINDOW.from_now

      unless @repositories == INSTALL_ON_ALL_REPOSITORIES
        @repositories = Array(@repositories)
      end

      @permissions = permissions.present? ? permissions : integration.default_permissions
    end

    def perform
      validate_global_apps_permissions
      validate_permissions

      ApplicationRecord::Iam.transaction do
        SiteScopedIntegrationInstallation.transaction do
          create_record
          if installing_on_all_repositories?
            grant_permissions_to_all_repositories
          else
            grant_permissions_to_repositories
          end
        end

        if target.is_a?(Organization)
          grant_organization_permissions
        end
      end

      GitHub.dogstats.increment("site_scoped_integration_installation.create", tags: ["result:success"])
      Result.success(@site_scoped_installation)
    rescue Result::Error => e
      GitHub.dogstats.increment("site_scoped_integration_installation.create", tags: ["result:failed"])
      Result.failed e.message
    end

    private

    def app_attributes
      @app_attributes ||= {}.tap do |opts|
        opts[:actor] = @site_scoped_installation

        if expires?
          opts[:expires_at] = @expiry.to_i
        end
      end
    end

    def expires?
      @expires
    end

    def validate_global_apps_permissions
      unless Apps::Internal.capable?(:installed_globally, app: integration)
        raise(Result::Error, "Integration can't be globally installed")
      end

      unless GitHub.flipper[:global_apps].enabled?(integration)
        raise(Result::Error, "Global-Apps is disabled for this integration")
      end

      validate_access
    end

    # Private: Validates this Global App can actually access the requested target/repos.
    #
    # Global Apps are limited by default. During development, integrators can define a
    # set of targets/repos that can be accessible.
    #
    # https://github.com/github/github/pull/139090 describes how the accessible targets can
    # be configured.
    #
    # May raise an error if the App can't access the requested target/repos.
    def validate_access
      if Apps::Internal.capable?(:limited_access, app: integration)
        accessible_targets = Apps::Internal.property(:accessible_targets, app: integration) || {}
        accessible_targets.transform_keys!(&:downcase)

        target_key = target.login.downcase
        if accessible_targets.empty? || !accessible_targets.has_key?(target_key)
          raise(Result::Error, "This integration doesn't have access to the given target")
        end

        accessible_repositories = accessible_targets[target_key].map(&:downcase)
        return if accessible_repositories.empty?

        if (repositories.map { |r| r.name.downcase } - accessible_repositories).any?
          raise(Result::Error, "This integration doesn't have access to all of the requested repositories")
        end
      end
    end

    def validate_permissions
      check = SiteScopedIntegrationInstallation::Permissions.check(
        integration:  integration,
        target:       target,
        repositories: repositories,
        action:       :create,
        permissions:  permissions,
      )

      raise(Result::Error, check.error_message) unless check.permitted?
    end

    def create_record
      options = { integration: integration, target: target }
      options[:expires_at] = @expiry if expires?

      @site_scoped_installation = SiteScopedIntegrationInstallation.create!(options)
    end

    def grant_permissions_to_repositories
      GitHub.dogstats.histogram("site_scoped_integration_installation.repositories.size", repositories.size)
      transient_version.permissions_of_type(Repository).each_pair do |permission, action|
        repositories.each_slice(DEFAULT_BATCH_SIZE) do |batch_repositories|
          attributes = []
          batch_repositories.each do |repository|
            subject = repository.resources.public_send(permission)
            attributes << ::Permissions::Service.app_attributes(**app_attributes.merge(subject: subject, action: action))
          end
          ::Permissions::Service.grant_permissions(attributes)
        end
      end
    end

    def grant_permissions_to_all_repositories
      attributes = []
      transient_version.permissions_of_type(Repository).each_pair do |permission, action|
        subject = target.repository_resources.public_send(permission.to_s)
        attributes << ::Permissions::Service.app_attributes(**app_attributes.merge(subject: subject, action: action))
      end
      ::Permissions::Service.grant_permissions(attributes)
    end

    def grant_organization_permissions
      attributes = []
      transient_version.permissions_of_type(Organization).each do |resource, action|
        subject = target.resources.public_send(resource)
        attributes << ::Permissions::Service.app_attributes(**app_attributes.merge(subject: subject, action: action))
      end
      ::Permissions::Service.grant_permissions(attributes)
    end

    def installing_on_all_repositories?
      @repositories == INSTALL_ON_ALL_REPOSITORIES
    end

    def transient_version
      IntegrationVersion.new(
        integration: integration,
        default_permissions: permissions,
        associated_with_scoped_installation: true,
      )
    end
  end
end
