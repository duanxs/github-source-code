# rubocop:disable Style/FrozenStringLiteralComment
require_dependency "storage/uploadable"

module Storage
  class DeletionError < StandardError
    def initialize(status, uploadable)
      super("Error deleting %s #%d from S3: HTTP %d" % [
        uploadable.class, uploadable.id,
        status
      ])
    end
  end

  module Uploadable
    def storage_policy(actor: nil, repository: nil)
      raise NotImplementedError, "#{self.class} needs #storage_policy"
    end

    def storage_external_url(_ = nil)
      storage_policy.download_url
    end

    def storage_verify_path
      "#{self.class.uploadable_policy_path}/#{id}"
    end

    def storage_provider
      prov = read_attribute(:storage_provider)
      prov.blank? ? :default : prov.to_sym
    end

    def storage_transition_ready?
      storage_blob_accessible?
    end

    def storage_blob_accessible?
      raise NotImplementedError
    end

    # returns the absolute URL for the alambic uploader app to fetch after a
    # successful upload.
    def storage_api_url
    end

    module ClassMethods
      def act_on_storage_uploadable?
        GitHub.storage_cluster_enabled?
      end

      # Returns a Time instance that indicates when a `Storage::Purge` object
      # created for this Uploadable should be scheduled for final deletion.
      # Defaults to an hour later than the archived Repository retention interval,
      # computed as an offset of `Time.now`
      def purge_at
        Time.now + ::RepositoryPurgeArchivedJob.expiration_period + 1.hour
      end

      def storage_auth_token(user, meta, expires: nil)
        expires ||= 1.hour
        user.signed_auth_token(scope: storage_auth_scope(meta), expires: expires.from_now)
      end

      def storage_verify_token(token, meta)
        User.verify_signed_auth_token(token: token, scope: storage_auth_scope(meta))
      end

      def storage_disk_usage(conditions)
        blob_ids = where(conditions).pluck(:storage_blob_id)

        blob_ids.each_slice(10000).sum { |ids| Storage::Blob.where(id: ids).sum(:size) }
      end

      def storage_auth_scope(meta)
        path_info = meta[:path_info]
        if path_info.blank?
          GitHub::Logger.log(method: __method__,
                             meta: meta.inspect)
          raise "Empty auth scope"
        end

        ctype = meta[:original_type] || meta[:content_type]
        return path_info if ctype.blank?

        size = determine_auth_scope_size(ctype, meta)
        attrs = [path_info, ctype, size]
        auth_scope_allowlist = Set.new([:upload_url])

        meta.sort.each do |(k, v)|
          next unless auth_scope_allowlist.include?(k)
          attrs << v
        end

        attrs.compact!
        attrs.join(":")
      end

      def determine_auth_scope_size(ctype, meta)
        return 0 if ctype == "url"
        (meta[:original_size] || meta[:size]).to_i
      end
    end
  end

  class Policy
    class HttpError < StandardError
      attr_reader :status
      attr_reader :uploadable_class
      attr_reader :id

      def initialize(status, klass, id, msg)
        @status = status
        @uploadable_class = klass
        @id = id
        super(msg)
      end
    end

    @@faraday = nil

    def self.set_faraday_adapter(*adapter_args)
      @@faraday = Faraday.new do |f|
        f.request :multipart
        f.request :url_encoded
        f.adapter(*adapter_args)
      end
    end

    def self.faraday
      return @@faraday if @@faraday || Rails.test?
      set_faraday_adapter(Faraday.default_adapter)
    end

    def self.stub_faraday
      stub = Faraday::Adapter::Test::Stubs.new
      set_faraday_adapter(:test, stub)
      stub
    end

    def self.faraday=(f)
      @@faraday = f
    end

    attr_reader :actor
    attr_reader :repository
    attr_reader :remote_auth_params

    # Builds a new Policy.
    #
    # uploadable - Instance of the ActiveRecord model that is being uploaded.
    # actor      - The optional User accessing the uploadable.
    # repository - The optional Repository that the uploadable belongs to.
    def initialize(uploadable, actor: nil, repository: nil)
      @uploadable = uploadable
      @actor = actor
      @repository = repository
      @remote_auth_params = {}
    end

    def repository_full_name
      @repository.try(:name_with_owner)
    end

    def public_repository?
      @repository.try(:public?)
    end

    def repository_id
      @repository.try(:id).to_i
    end

    def policy_hash
      now = Time.now
      h = {
        upload_url: upload_url,
        header: upload_header,
        asset: asset_hash,
        form: upload_form,
        same_origin: same_origin_upload?,
      }

      if @uploadable.respond_to?(:supports_multi_part_upload) && @uploadable.supports_multi_part_upload
        url_headers = multi_part_upload_url_headers(@uploadable.state)
        h[:upload_url] = url_headers[:url]
        h[:header] = url_headers[:headers]
        h[:guid] = @uploadable.guid
        h.delete(:form)
      end

      if url = asset_upload_url
        h[:asset_upload_url] = url
      end

      h
    ensure
      stats_timing(:upload, start: now) if now
    end

    def asset_hash
      h = {
        id: @uploadable.id,
        name: @uploadable.name,
        size: @uploadable.size,
        content_type: @uploadable.content_type,
        href: @uploadable.storage_external_url,
      }

      if @uploadable.respond_to?(:storage_metadata_url)
        h[:metadata_url] = @uploadable.storage_metadata_url
      end

      if @uploadable.respond_to?(:original_name)
        h[:original_name] = @uploadable.original_name
      end

      if @uploadable.respond_to?(:replaced_asset)
        h[:replaced_asset] = @uploadable.replaced_asset
      end

      h
    end

    def storage_supports_multi_part_upload
      false
    end

    def upload_url
      raise NotImplementedError
    end

    def multi_part_upload_url_headers
      raise NotImplementedError
    end

    def upload_link
      {
        href: upload_url,
        header: upload_header,
      }
    end

    def upload_contents(io)
      info = policy_hash
      self.class.faraday.post(info[:upload_url]) do |req|
        req.headers["Content-Type"] = "multipart/form-data"
        req.body = {}
        info[:header].each do |key, value|
          req.headers[key] = value
        end
        info[:form].each do |key, value|
          req.body[key.to_s] = value
        end
        req.body["file"] = Faraday::UploadIO.new(io, @uploadable.content_type)
      end
    end

    def upload_contents!(io)
      res = upload_contents(io)
      if res.status < 200 || res.status > 299
        GitHub::Logger.log(method: __method__,
                           status: res.status,
                           uploadable: @uploadable.inspect,
                           body: res.body)
        raise HttpError.new(res.status, @uploadable.class, @uploadable.id,
          "Error uploading for #{@uploadable.class} ##{@uploadable.id}: HTTP #{res.status}")
      end
    end

    def storage_policy_api_url
      if @uploadable.respond_to?(:storage_policy_api_url)
        @uploadable.storage_policy_api_url unless @uploadable.new_record?
      end
    end

    def stats_timing(op, start:)
      ms = (Time.now - start) * 1000
      GitHub.dogstats.timing("storage_policy.url", ms, tags: [
        "policy:#{self.class.name.underscore.sub(/\Astorage\//, "").chomp("_policy")}",
        "model:#{@uploadable.class.name.underscore}",
        "provider:#{@uploadable.storage_provider}",
        "op:#{op}",
      ])
    end

    private

    def upload_form
      asset_hash.except(:href, :replaced_asset)
    end

    def same_origin_upload?
      true
    end

    def upload_header
      {}
    end

    def asset_upload_url
      "/upload/#{@uploadable.storage_verify_path}"
    end
  end
end
