# frozen_string_literal: true

class Archived::Milestone < ApplicationRecord::Domain::Repositories
  include Archived::Base
end
