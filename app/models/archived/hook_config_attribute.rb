# frozen_string_literal: true

class Archived::HookConfigAttribute < ApplicationRecord::Domain::Hooks
  include Archived::Base
end
