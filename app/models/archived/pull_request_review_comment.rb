# frozen_string_literal: true

class Archived::PullRequestReviewComment < ApplicationRecord::Domain::Repositories
  include Archived::Base
end
