# frozen_string_literal: true

class Archived::ProjectColumn < ApplicationRecord::Domain::Projects
  include Archived::Base
  include GitHub::Prioritizable::Context

  has_many :cards, class_name: "Archived::ProjectCard", foreign_key: :column_id, dependent: :delete_all
  belongs_to :project, class_name: "Archived::Project"

  prioritizes :cards, with: :cards, by: :priority
end
