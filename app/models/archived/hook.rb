# rubocop:disable Style/FrozenStringLiteralComment

class Archived::Hook < ApplicationRecord::Domain::Hooks
  include Archived::Base

  has_many :hook_config_attributes,  class_name: "Archived::HookConfigAttribute", dependent: :delete_all

  has_many :hook_event_types,
    -> { where(subscriber_type: "Hook") },
    class_name: "Archived::HookEventSubscription",
    foreign_key: :subscriber_id,
    dependent: :delete_all
end
