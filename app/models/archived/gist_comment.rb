# frozen_string_literal: true

class Archived::GistComment < ApplicationRecord::Domain::Gists
  include Archived::Base
end
