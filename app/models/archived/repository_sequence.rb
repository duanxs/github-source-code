# frozen_string_literal: true

class Archived::RepositorySequence < ApplicationRecord::Domain::Repositories
  include Archived::Base
end
