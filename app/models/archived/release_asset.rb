# frozen_string_literal: true

class Archived::ReleaseAsset < ApplicationRecord::Domain::Assets
  include Archived::Base
end
