# frozen_string_literal: true

class Archived::IssueComment < ApplicationRecord::Domain::Repositories
  include Archived::Base
end
