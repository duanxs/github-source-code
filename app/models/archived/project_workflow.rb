# frozen_string_literal: true

class Archived::ProjectWorkflow < ApplicationRecord::Domain::Projects
  include Archived::Base
  has_many :project_workflow_actions, class_name: "Archived::ProjectWorkflowAction", dependent: :delete_all
  belongs_to :project, class_name: "Archived::Project"

  def creator
    @creator ||= User.where(id: creator_id).first || User.ghost
  end
end
