# frozen_string_literal: true

class Archived::Assignment < ApplicationRecord::Domain::Repositories
  include Archived::Base
end
