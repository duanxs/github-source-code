# frozen_string_literal: true

class Archived::CommitComment < ApplicationRecord::Domain::Repositories
  include Archived::Base
end
