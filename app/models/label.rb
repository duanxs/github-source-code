# rubocop:disable Style/FrozenStringLiteralComment

require "branch_sorter"

class Label < ApplicationRecord::Domain::Repositories
  include GitHub::Validations
  include GitHub::Relay::GlobalIdentification
  include Instrumentation::Model
  include Labelable

  extend GitHub::Encoding
  force_utf8_encoding :name, :lowercase_name, :description

  include Label::DiscussionsDependency

  URI_TEMPLATE = Addressable::Template.new("/{owner}/{name}/labels/{label_name}").freeze

  VALID_LABEL_CREATION_CONTEXTS = [
    "pull request sidebar",
    "issue sidebar",
    "labels list",
    "issues import",
    "repository import",
    "repository creation",
    "api",
  ].freeze

  validates :name, length: { maximum: NAME_MAX_LENGTH }

  validates :description, length: { maximum: DESCRIPTION_MAX_LENGTH }, allow_blank: true,
    unicode3: true

  belongs_to :repository

  alias_attribute :to_s, :name

  scope :for_milestone, -> (milestone) {
    includes(:issues).where("issues.milestone_id = ?", milestone).references(:issues)
  }

  scope :for_issue_ids, -> (ids) {
    includes(:issues).where("issues.id IN(?)", ids).references(:issues)
  }

  scope :on_open_issues,   -> { includes(:issues).where("issues.state = ?", "open").references(:issues) }
  scope :on_closed_issues, -> { includes(:issues).where("issues.state = ?", "closed").references(:issues) }

  # Public: Find labels with any of the given names, case insensitive.
  scope :with_name, ->(names) do
    if names.is_a?(String)
      where(lowercase_name: names.downcase)
    else
      where(lowercase_name: names.map(&:downcase))
    end
  end

  # Public: Find labels whose name starts with the given string, case insensitive.
  scope :with_name_like, ->(name) {
    sanitized_name = ActiveRecord::Base.sanitize_sql_like(name.downcase)
    where(arel_table[:lowercase_name].matches("#{sanitized_name}%"))
  }

  # Do we need to do something here for default scope
  has_and_belongs_to_many :issues

  before_validation :normalize_name, :normalize_description, :expand_color_shorthand,
                    :set_lowercase_name

  validates_presence_of   :name
  validates_format_of     :name,  with: NAME_REGEXP
  validate :uniqueness_of_name
  validate :name_has_more_than_emoji
  validates_format_of     :color, with: COLOR_REGEXP

  after_commit :synchronize_search_index
  after_commit :update_issues, on: :update
  after_commit :instrument_update, on: :update

  before_destroy :generate_hook_payload
  before_destroy :unlabel_associated_issues
  after_commit :instrument_destruction, on: :destroy

  # Public: Look up a label by name, case insensitive.
  #
  # name - String name of a label
  #
  # Returns a Label or nil.
  def self.find_by_name(name) # rubocop:disable GitHub/FindByDef
    with_name(name).first
  end

  # Public: Find a label whose name is exactly 'help wanted', case insensitive.
  def self.help_wanted
    where(lowercase_name: HELP_WANTED_NAME).first
  end

  # Public: Find a label whose name begins with 'help wanted', case insensitive.
  def self.similar_to_help_wanted
    with_name_like(HELP_WANTED_NAME).first
  end

  # Public: Find a label whose name is exactly 'good first issue', case insensitive.
  def self.good_first_issue
    where(lowercase_name: GOOD_FIRST_ISSUE_NAME).first
  end

  # Public: Find a label whose name begins with 'good first issue', case insensitive.
  def self.similar_to_good_first_issue
    with_name_like(GOOD_FIRST_ISSUE_NAME).first
  end

  # The user who performed the action as set in the GitHub request context. If
  # the context doesn't contain an actor, fallback to the ghost user.
  def actor
    @actor ||= (User.find_by_id(GitHub.context[:actor_id]) || User.ghost)
  end

  # Public: The number of associated open issues that may or may not be tied to
  # pull requests.
  #
  # Returns the Integer issues count.
  def issues_count
    return @issues_count if defined?(@issues_count)
    @issues_count = issues.open_issues.count
  end
  attr_writer :issues_count

  # Public: The number of associated open issues that are not tied to pull requests.
  #
  # Returns an integer.
  def issues_without_pull_requests_count
    return @issues_without_pull_requests_count if defined?(@issues_without_pull_requests_count)
    @issues_without_pull_requests_count = issues.open_issues.without_pull_requests.count
  end
  attr_writer :issues_without_pull_requests_count

  # Internal: Require Issue#repository_id and Label#repository_id to match.
  # Returns an ActiveRecord Issue scope.
  def repository_restricted_issues
    issues.where(repository_id: repository_id)
  end

  # Sort labels and optionally group by issue count.
  #
  # labels          - an Array or association collection of labels
  # by_issues_count - Whether to put labels without issues at the bottom (default: false).
  #                   This relies on the `issues_count` attribute.
  #
  # returns - a one-dimension sorted (and/or grouped) array of labels
  def self.smart_sort(labels, by_issues_count = false)
    sorted_labels = BranchSorter.new(labels) { |label| label.name.downcase }

    if by_issues_count
      labels_without_issues, labels = sorted_labels.partition { |label| label.issues_count.zero? }
      labels.concat labels_without_issues
    else
      sorted_labels.to_a
    end
  end

  DEFAULT_LABEL_NAMES = Set.new(initial_labels.map { |l| l[:name] })

  def default?
    DEFAULT_LABEL_NAMES.include?(name)
  end

  def to_param
    name
  end

  # Public: Synchronize this label with its representation in the search index. If the label is
  # newly created or modified in some fashion, then it will be updated in the search index. If the
  # label has been destroyed, then it will be removed from the search index. This method handles
  # both cases.
  def synchronize_search_index
    if self.destroyed?
      RemoveFromSearchIndexJob.perform_later("label", id, repository_id)
    else
      Search.add_to_search_index("label", id)
    end

    self
  end

  # Returns the name of the label in a search-friendly format.
  #
  # For labels with no spaces (`bug`), it just returns the label: bug.
  # For labels with spaces (`big bug`), it quotes the value: "big bug".
  #
  # Returns a String.
  def to_search_slug
    name.match(" ") ? "\"#{name}\"" : name
  end

  # Internal: If the name of the label has changed, then we need to
  # update the search records for all the issues and pull requests associated
  # with this label.
  def update_issues
    return unless self.previous_changes["name"].present?

    repository_restricted_issues.each do |issue|
      if issue.pull_request_id
        issue.pull_request.synchronize_search_index
      else
        issue.update_repo_community_profile!
        issue.synchronize_search_index
      end
    end

    self
  end

  def unlabel_associated_issues
    issues.open_issues.each do |issue|
      Label.transaction do
        issue.events.create \
          event: "unlabeled",
          actor: actor,
          label: self

        issue.touch
      end
    end
  end

  def async_path_uri
    return @async_path_uri if defined?(@async_path_uri)

    @async_path_uri = async_repository.then(&:async_owner).then do
      URI_TEMPLATE.expand(
        owner:      repository.owner.login,
        name:       repository.name,
        label_name: name,
      )
    end
  end

  def url
    GitHub.url + async_path_uri.sync.to_s
  end

  # Internal: Returns a non-persisted instance if the label is found
  #
  # name: The name of the label
  #
  # Returns a Label or nil
  def self.default_for(name)
    return unless data = initial_labels.find { |hash| hash[:name] == name }
    Label.new(data)
  end

  def instrument_creation(context:, issue_id: nil, actor: self.actor)
    actor = nil if actor && actor.ghost?
    context = "unknown" unless VALID_LABEL_CREATION_CONTEXTS.include?(context)
    dimensions = {
      repository_id: repository.id,
      user_id: repository.owner_id,
      action: "create",
      issue_id: issue_id,
      label_id: id,
      label_name: name,
      actor_id: actor.try(:id),
      relationship_to_repo: actor ? actor.relationship_to(repository).to_s : nil,
      context: context,
      public: repository.public?,
    }
    instrument :create, event_payload.merge(dimensions: dimensions)

    GlobalInstrumenter.instrument("label.create",
      actor: actor,
      label: self,
      issue_id: issue_id,
    )
  end

  def async_name_html(skip_cache: false)
    LabelNameRenderer.new(name, skip_cache: skip_cache).async_to_html.then do |name_as_html|
      @name_html = name_as_html
    end
  end

  def name_html(skip_cache: false)
    return @name_html if defined?(@name_html)
    @name_html = async_name_html(skip_cache: skip_cache).sync
  end

  def memex_column_hash
    {
      color: color,
      id: id,
      name: name,
      name_html: name_html,
      url: url,
    }
  end

  def memex_suggestion_hash(selected:)
    memex_column_hash.merge(selected: selected)
  end

  private

  # Used in uniqueness_of_name validation
  def sibling_labels_with_same_name
    repository.labels.where(lowercase_name: lowercase_name)
  end

  # Used in uniqueness_of_name validation
  def owner
    repository
  end

  def event_payload
    {
      label_id: id,
      actor_id: actor.try(:id),
    }
  end

  def generate_hook_payload
    if actor&.spammy?
      @delivery_system = nil
      return
    end

    payload = event_payload.merge(action: :deleted, triggered_at: Time.now)
    event = Hook::Event::LabelEvent.new(payload)
    @delivery_system = Hook::DeliverySystem.new(event)
    @delivery_system.generate_hookshot_payloads
  end

  def instrument_update
    return if previous_changes.empty?

    changes_payload = {
      old_name: previous_changes["name"].try(:first),
      old_color: previous_changes["color"].try(:first),
      old_description: previous_changes["description"].try(:first),
    }

    GitHub.instrument "label.update", event_payload.merge(changes: changes_payload)

    actor = User.find_by(id: GitHub.context[:actor_id])
    GlobalInstrumenter.instrument("label.update",
      actor: actor,
      label: self,
    )
  end

  def instrument_destruction
    actor = User.find_by(id: GitHub.context[:actor_id])
    GlobalInstrumenter.instrument("label.delete",
      actor: actor,
      label: self,
    )

    unless defined?(@delivery_system)
      raise "`generate_hook_payload' must be called before the webhook can be delivered"
    end

    @delivery_system&.deliver_later
  end
end
