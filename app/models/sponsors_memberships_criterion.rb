# frozen_string_literal: true

class SponsorsMembershipsCriterion < ApplicationRecord::Collab
  include GitHub::Relay::GlobalIdentification
  include Instrumentation::Model

  self.table_name = "sponsors_memberships_criteria"

  belongs_to :sponsors_criterion, required: true
  belongs_to :sponsors_membership, required: true
  belongs_to :reviewer, class_name: "User", required: false

  validates :sponsors_criterion, uniqueness: { scope: :sponsors_membership }

  scope :automated, -> { joins(:sponsors_criterion).where(sponsors_criteria: { automated: true }) }
  scope :manual, -> { joins(:sponsors_criterion).where(sponsors_criteria: { automated: false }) }

  after_create_commit :instrument_creation
  after_update_commit :instrument_update

  private

  def event_payload
    payload = {
      sponsors_memberships_criterion: self,
      sponsors_criterion: sponsors_criterion,
      sponsors_membership: sponsors_membership,
      met: met,
      criterion_value: value,
      actor: reviewer,
    }

    changes = previous_changes
    payload[:old_met] = changes[:met].first if changes.key?(:met)
    payload[:old_criterion_value] = changes[:value].first if changes.key?(:value)

    payload
  end

  def instrument_creation
    instrument :create
  end

  def instrument_update
    instrument :update
  end
end
