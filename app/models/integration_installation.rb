# rubocop:disable Style/FrozenStringLiteralComment

class IntegrationInstallation < ApplicationRecord::Domain::Integrations
  VALID_TARGET_TYPES = %w(User Business)
  MAX_REPOS_TO_INSTRUMENT = 100

  include GitHub::Relay::GlobalIdentification

  areas_of_responsibility :ecosystem_apps
  include Ability::Actor
  include AuthenticationTokenable
  include GitHub::FlipperActor
  include IntegrationInstallable

  belongs_to :integration
  belongs_to :target, polymorphic: true

  belongs_to :subscription_item, class_name: "Billing::SubscriptionItem"

  belongs_to :integration_install_trigger

  belongs_to :version,
    class_name: "IntegrationVersion",
    foreign_key: :integration_version_id

  belongs_to :user_suspended_by, class_name: "User"

  # HookEventSubscriptions representing the event types that this installation
  # is subscribed to.
  has_many :event_records,
    as: :subscriber,
    class_name: "HookEventSubscription",
    dependent: :destroy,
    autosave: true,
    extend: HookEventSubscription::AssociationExtension

  has_many :children,
    class_name: "ScopedIntegrationInstallation",
    dependent:  :destroy

  has_many :content_references, through: :version

  # integration_id, target_id, target_type should be unique UNLESS this is an
  # github app integration that is installed per repo, in which case we want
  # separate installs for the same user/org for different repos
  validates_uniqueness_of :integration_id, scope: [:target_id, :target_type]

  validates_presence_of :integration

  validates_presence_of :target
  validates             :target_type, inclusion: VALID_TARGET_TYPES

  validates_presence_of :integration_version_id
  validates_presence_of :integration_version_number

  after_update_commit :instrument_contact_email_changed, if: :saved_change_to_contact_email_id?

  after_destroy :clear_subscription_item_installation

  after_create :update_integration_installation_count
  after_destroy :update_integration_installation_count

  attribute :integrator_suspended_at, :utc_timestamp
  attribute :user_suspended_at,       :utc_timestamp

  scope :most_recent, -> { order("integration_installations.id DESC") }

  scope :since, -> (time) { where("integration_installations.created_at >= ?", time) }

  scope :not_suspended, -> { where(integrator_suspended: false, user_suspended_by_id: nil) }

  delegate :single_file_name, to: :version

  # Public: which installations the given user has access to.
  #
  # user - a User or Bot
  #
  # Returns an ActiveRecord scope of IntegrationInstallations
  def self.with_user(user)
    start_time = Time.now
    target_ids = [user.id]

    # Find organizations where the user is an admin.
    target_ids.push(*Authorization.service.subject_ids(actor: user, subject_type: "Organization", actions: [:admin]))

    # Load repositories that the user has been granted access to either directly or indirectly.
    associated_repository_ids = user.associated_repository_ids(including: [:direct, :indirect], include_indirect_forks: false)

    # Rather than search for all repositories, only look for those that there have an installation.
    associated_repos = Repository.where(id: associated_repository_ids).where.not(owner_id: target_ids)

    repo_ids_by_owner_id = Hash.new { |h, k| h[k] = [] }
    associated_repos.pluck(:owner_id, :id).each do |owner_id, id|
      repo_ids_by_owner_id[owner_id] << id
    end

    installation_target_ids = where(target_type: "User", target_id: repo_ids_by_owner_id.keys).distinct.pluck(:target_id)
    related_repo_ids = installation_target_ids.flat_map { |o| repo_ids_by_owner_id[o] }

    repo_access_ids = if related_repo_ids.any?
      Authorization.service.actor_ids_with_granular_permissions_on(
        actor_type: "IntegrationInstallation",
        subject_type: Repository,
        subject_ids: related_repo_ids,
        owner_ids: installation_target_ids,
        permissions: :any,
      )
    else
      []
    end

    duration_in_milliseconds = (Time.now - start_time) * 1_000
    GitHub.dogstats.timing("integration_installation.with_user", duration_in_milliseconds, tags: ["accessible_repos:#{related_repo_ids.any?}"])

    # Installations on orgs are still of type "User"
    where(target_type: "User", target_id: target_ids).or(where(id: repo_access_ids))
  end

  def self.ids_with_resources_on(subject:, resources:, min_action: :read)
    Authorization.service.actor_ids_with_granular_permissions_on(
      actor_type:   IntegrationInstallation,
      subject_type: subject.class,
      subject_ids:  subject.id,
      owner_ids:    subject.is_a?(User) ? nil : subject.owner_id,
      permissions:  Array(resources),
      min_action:   min_action,
    )
  end

  def self.with_resources_on(subject:, resources:, min_action: :read)
    where(id: ids_with_resources_on(subject: subject, resources: resources, min_action: min_action))
  end

  # IntegrationInstallations with permissions on the given Repository.
  def self.with_repository(repository)
    raise ArgumentError, "Repository required" if repository.nil?
    with_resources_on(subject: repository, resources: Repository::Resources.subject_types)
  end

  # IntegrationInstallations with org-level permissions.
  def self.with_org_permissions(org)
    raise ArgumentError, "Organization required" if org.nil?
    with_resources_on(subject: org, resources: Organization::Resources.subject_types)
  end

  scope :with_target, lambda { |target|
    where(target_id: target.id, target_type: target.class.base_class)
  }

  scope :with_target_id_type, lambda { |id, type|
    where(target_id: id, target_type: type)
  }
  # GitHub application installations that are not considered first-party (i.e.
  # not full-trust). Not all GitHub Apps owned by GitHub are necessarily full-
  # trust. These applications are be treated like any other third-party app.
  scope :third_party, -> {
    joins(:integration).
    where(integrations: { full_trust: false })
  }

  def self.ability_type
    self.name
  end

  def adminable_by?(actor)
    IntegrationInstallation::Permissions.check(installation: self, actor: actor, action: :admin).permitted?
  end

  def viewable_by?(actor)
    IntegrationInstallation::Permissions.check(installation: self, actor: actor, action: :view).permitted?
  end

  def async_configuration_url(viewer)
    if adminable_by?(viewer)
      if target.organization?
        url = Rails.application.routes.url_helpers.settings_org_installations_url(target, host: GitHub.host_name)
      elsif target.user?
        url = Rails.application.routes.url_helpers.settings_user_installations_url(host: GitHub.host_name)
      end

      URI.parse(url)
    else
      async_integration.then do |integration|
        URI.parse(Rails.application.routes.url_helpers.edit_alias_app_installation_url(integration, self, host: GitHub.host_name))
      end
    end
  end

  def self.for_actions(repository)
    with_repository(repository).joins(:integration).where(integrations: { id: GitHub.launch_github_app&.id })
  end

  def outdated?
    return @result if defined?(@result)
    @result = !!(integration_version_number < integration.latest_version.number)
  end

  # Public: Get the organization that this is installed on.
  #
  # Returns an Organization, or nil if this is not installed on an organization.
  def organization
    target if target && target.organization?
  end

  # Public: The collection of event names that this installation subscribes to.
  #
  # Example
  #   events
  #   # => ["issues", "pull_request"]
  #
  # Returns an Array
  def events
    event_records.names
  end

  # Public: Set the collection of event names this installation should subscribe
  # to.
  #
  # event_names - An Array of event names.
  #
  # Returns an Array of subscribed event names.
  def events=(event_names)
    event_records.names = event_names
  end

  def target_type=(class_name)
    super(class_name.constantize.base_class.to_s)
  end

  # Public: Grant and remove access to repositories for this installation
  #
  # repositories: - An Array of Repositories to include in the installation.
  # editor:       - The User performing the installation edit.
  #
  # Returns an IntegrationInstallation::Editor::Result.
  def edit(repositories:, editor:)
    IntegrationInstallation::Editor.perform(
      self,
      repositories: repositories,
      editor: editor,
    )
  end

  # Public: Update permissions and events for this installation.
  #
  # editor:       - The User performing the installation edit.
  # version       - The IntegrationVersion that the installation
  #                 is being updated to.
  #
  # Returns an IntegrationInstallation::PermissionsEditor::Result.
  def update_version(editor:, version:)
    IntegrationInstallation::PermissionsEditor.perform(
      self,
      editor: editor,
      version: version,
    )
  end

  CANNOT_AUTO_UPGRADE_ERROR_MESSAGE = "This installation cannot be auto upgraded".freeze

  def auto_update_version(editor:, version:)
    if auto_upgradeable_to?(version)
      return update_version(editor: editor, version: version)
    end

    error = IntegrationInstallation::PermissionsEditor::Result::Error.new(CANNOT_AUTO_UPGRADE_ERROR_MESSAGE)
    IntegrationInstallation::PermissionsEditor::Result.failed(error)
  end

  # Public: Determine if the given installation be upgraded
  # to the new version without permission from
  # the target.
  #
  # Returns a Boolean.
  def auto_upgradeable_to?(version_to_upgrade_to)
    IntegrationInstallation::Permissions.check(
      installation: self,
      actor:        nil,
      action:       :auto_upgrade,
      version:      version_to_upgrade_to,
    ).permitted?
  end

  # Public: Recalculate the rate limit for this installation.
  #
  # Returns nil.
  def recalculate_rate_limit
    new_rate_limit = IntegrationInstallation::RateLimitCalculator.calculate(self)
    update(dynamic_rate_limit: new_rate_limit)
  end

  # Public: Remove the installation.
  #
  # actor - The User performing the uninstall (optional).
  #
  # Returns nothing.
  def uninstall(actor: nil)
    generate_webhook_payload(actor: actor)

    result = nil
    if (result = self.destroy)
      instrument_deletion(actor: actor)
      queue_webhook_delivery
    end

    return result
  end

  # Internal: Get the default level of permission for the specified resource
  #           across all repositories.
  #
  # resource:   - A String to represent a child association, from
  #               Repository::Resources.subject_types, to get the level of permission for.
  # group:      - A String for which type of repositories: currently only 'private'.
  #
  # Returns a Integer action value representing the ability action
  def default_repository_permission(resource:, group: "private")
    raise ArgumentError if Repository::Resources.subject_types.exclude?(resource)

    subject = target.repository_resources.public_send(resource.to_s)
    ability = Authorization.service.most_capable_ability_between(actor: self, subject: subject)

    if ability
      Ability.actions[ability.action]
    end
  end

  def repository_installation_required?
    # Use a temporary version instead of the actual `version` so that we
    # are only using the permissions granted to the installation.
    integration.repository_installation_required?(target, IntegrationVersion.new(default_permissions: permissions))
  end

  # IntegrationInstallations default to the global default rate limit, but an
  # installation can also be granted a higher rate limit to get more calls. Use
  # #set_temporary_rate_limit to set a 3-day rate limit increase for initial
  # imports and that kind of thing.
  #
  # Returns the currently active rate limit integer value for calls per hour
  def rate_limit
    temporary_rate_limit || override_rate_limit || dynamic_rate_limit || GitHub.api_default_rate_limit
  end

  def temporary_rate_limit
    if temporary_rate_limit_expires_at && temporary_rate_limit_expires_at.future?
      self[:temporary_rate_limit]
    end
  end

  def set_temporary_rate_limit(limit, duration = 3.days)
    update! temporary_rate_limit: limit, temporary_rate_limit_expires_at: duration.from_now
  end

  def using_temporary_rate_limit?
    temporary_rate_limit.present?
  end

  include Instrumentation::Model

  def event_prefix
    :integration_installation
  end

  def event_payload
    {}.tap do |payload|
      payload[:installation_id]      = id
      payload[:integration]          = integration
      payload[:app]                  = integration
      payload[:name]                 = integration.name
      payload[:slug]                 = integration.slug
      payload[:repository_selection] = repository_selection

      payload[target.event_prefix] = target
    end
  end

  def instrument_deletion(actor:)
    instrument :destroy, options = { actor_id: actor&.id }
  end

  # Internal: Send instrumentation details about the repositories added to this
  # installation. This method can be called either as part of a web request or
  # via a background job that it self-enqueues to move processing of large
  # numbers of repositories out of the request.
  #
  # repository_ids       - Array of integers representing the added repos
  # actor                - User responsible for making this change
  # repository_selection - "all" or "selected"
  # pending_request      - The IntegrationInstallationRequest that caused this update (optional).
  # async                - Should the instrumentation be done asynchronously?
  #
  # Returns nothing.
  def instrument_repositories_added(repository_ids, actor:, repository_selection:, requester_id: nil, async: true)
    return if repository_ids.blank?

    options = {
      actor_id:             actor.id,
      repositories_added:   repository_ids,
      repository_selection: repository_selection,
      requester_id:         requester_id,
    }

    if instrument_async?(repository_ids, async)
      IntegrationInstallationInstrumentationJob.perform_later(
        :repositories_added,
        self.id,
        options[:actor_id],
        options[:repositories_added],
        options[:repository_selection],
        requester_id: options[:requester_id],
      )
    else
      if Apps::Internal.capable?(:custom_instrumentation_integration_installation, app: integration)
        if integration.launch_github_app?
          # This is Actions-specific logic. If you need something similar for another integration, you
          # might want to think about generalizing this. See https://github.com/github/github/pull/138260
          # for discussion of a potential approach.
          if repository_selection == "selected"
            # Create a separate entry for each repository the Actions app is added to.
            Repository.where(id: repository_ids).each do |repo|
              payload = {}
              payload[:repo] = repo
              payload[repo.owner.event_prefix] = repo.owner if repo.owner.present?

              GitHub.instrument "repo.actions_enabled", payload
            end
          end
        end
      else
        # Default instrumentation
        instrument :repositories_added, options
      end
    end

    # This is not actually a timing, but we want the same kind of
    # information, i.e. average/percentile number of repositories per change.
    GitHub.dogstats.timing "integration_installation.repositories", repository_ids.size, tags: ["action:added"]
  end

  # Internal: Send instrumentation details about the repositories removed from
  # this installation. This method can be called either as part of a web
  # request or via a background job that it self-enqueues to move processing of
  # large numbers of repositories out of the request.
  #
  # repository_ids  - Array of integers representing the removed repos
  # actor           - User responsible for making this change
  # async           - Should the instrumentation be done asynchronously?
  #
  # Returns nothing.
  def instrument_repositories_removed(repository_ids, actor:, async: true)
    return if repository_ids.blank?

    options = {
      actor_id:              actor.id,
      repositories_removed:  repository_ids,
      repository_selection:  "selected",
    }

    if instrument_async?(repository_ids, async)
      IntegrationInstallationInstrumentationJob.perform_later(
        :repositories_removed,
        self.id,
        options[:actor_id],
        options[:repositories_removed],
        options[:repository_selection],
      )
    else
      instrument :repositories_removed, options
    end

    # This is not actually a timing, but we want the same kind of
    # information, i.e. average/percentile number of repositories per change.
    GitHub.dogstats.timing "integration_installation.repositories", repository_ids.size, tags: ["action:removed"]
  end

  CONTACT_ORGANIZATION_OWNER_MESSAGE = " Please contact an Organization Owner.".freeze

  # Public: Can the given user manage this installation?
  #
  #   user                - A User.
  #   for_permissions     - A Hash of permissions, for use for a new Installation object,
  #                         with no saved permissions yet.
  #   for_repositories    - An Array of Repository objects, for use for a new Installation object,
  #                         with no saved repositories yet.
  #   on_all_repositories - A Boolean signifying if the installation is on the target (defaults to false).
  #
  # Returns an Array with a result Boolean and an error message String.
  def manageable_by?(user, for_permissions: nil, for_repositories: nil, on_all_repositories: false)
    verb = new_record? ? "install" : "modify"

    default_error_message = "You do not have permission to #{verb} this app on #{target}."
    default_error_message << CONTACT_ORGANIZATION_OWNER_MESSAGE if target.organization?

    return [false, default_error_message] unless user.present?

    # Short circuit if we can admin the target.
    return [true, nil] if target.adminable_by?(user)

    # Unless the target is an Organization, short-circuit the rest of the logic.
    return [false, default_error_message] unless target.organization?

    ############################
    # Organization Permissions #
    ############################

    if requires_organization_installation?(for_permissions: for_permissions)
      message = "You cannot #{verb} apps with organization permissions on #{target}."
      message << CONTACT_ORGANIZATION_OWNER_MESSAGE

      return [false, message]
    end

    ###########################
    # Installing on all repos #
    ###########################

    if on_all_repositories || installed_on_all_repositories?
      message = "You do not have permission to #{verb} apps with all repositories on #{target}."
      message << CONTACT_ORGANIZATION_OWNER_MESSAGE

      return [false, message]
    end

    ###################################
    # Installing on a subset of repos #
    ###################################

    for_repositories ||= repositories
    can_admin_repositories = (for_repositories.present? && for_repositories.all? { |repo| repo.adminable_by? user })

    return [true, nil] if can_admin_repositories

    message = "You do not have permission to #{verb} this App on all repositories belonging to #{target}."
    message << CONTACT_ORGANIZATION_OWNER_MESSAGE

    return [false, message]
  end

  def requires_organization_installation?(for_permissions: nil)
    for_permissions ||= permissions
    Organization::Resources.subject_types.any? { |type| for_permissions.include?(type) }
  end

  def installed_automatically?
    !integration_install_trigger_id.nil?
  end

  def repository_permissions_only?
    # Use a temporary version instead of the actual `version` so that we
    # are only using the permissions granted to the installation.
    integration.repository_permissions_only?(IntegrationVersion.new(default_permissions: permissions))
  end

  def should_follow_moved_repo?(new_owner:)
    Apps::Internal.capable?(:follow_repository_transfers, app: integration)
  end

  # Public: Retrieve the set of permissions for this installation
  # via the KV store.
  #
  # This should not in _any_ way be used for authz. This is meant
  # for displaying the result of its parent method to API consumers.
  #
  # Returns a Hash.
  def get_cached_permissions
    GitHub.tracer.with_span("Integration::get_cached_permissions") do |span|
      cache_key = "integration_installation:#{self.id}.permissions"

      result, stats_key = if self.permissions_cache.present?
        GitHub.tracer.with_span("cache_column") do |span|
          string_or_hash_permissions = self.permissions_cache
          cached_permissions = if string_or_hash_permissions.is_a?(String)
            JSON.parse(string_or_hash_permissions)
          else
            string_or_hash_permissions
          end

          [cached_permissions, "integration_installation.permissions_cache"]
        end
      else
        GitHub.tracer.with_span("kv") do |span|
          string_permissions = GitHub.kv.get(cache_key).value { nil }
          cached_permissions = JSON.parse(string_permissions) if string_permissions
          [cached_permissions , "integration_installation.get_cached_permissions"]
        end
      end

      unless result.nil?
        GitHub.dogstats.increment(stats_key, tags: ["cache_result:hit"])
        return result.deep_transform_values(&:to_sym)
      end

      GitHub.dogstats.increment(stats_key, tags: ["cache_result:miss"])
      set_cached_permissions
    end
  end

  # Public: Store the permissions for this installation
  # via the KV store.
  #
  # This should not in _any_ way be used for authz. This is meant
  # for caching results to display later to API consumers.
  #
  # Returns a Hash.
  def set_cached_permissions
    cache_key = "integration_installation:#{self.id}.permissions"
    current_permissions = permissions

    ActiveRecord::Base.connected_to(role: :writing) do
      self.update(permissions_cache: current_permissions)
    end

    current_permissions
  end

  def get_cached_repository_selection
    GitHub.tracer.with_span("Integration::get_cached_repository_selection") do |span|
      result, stats_key = if self.repository_selection_cache.present?
        GitHub.tracer.with_span("cache_column") do |span|
          [self.repository_selection_cache, "integration_installation.repository_selection_cache"]
        end
      else
        cache_key = "integration_installation:#{self.id}.repository_selection"

        GitHub.tracer.with_span("kv") do |span|
          [
            GitHub.kv.get(cache_key).value { self.repository_selection },
            "integration_installation.get_cached_repository_selection",
          ]
        end
      end

      unless result.nil?
        GitHub.dogstats.increment(stats_key, tags: ["cache_result:hit"])
        return result
      end

      GitHub.dogstats.increment(stats_key, tags: ["cache_result:miss"])
      set_cached_repository_selection
    end
  end

  def set_cached_repository_selection
    current_repository_selection = self.repository_selection

    ActiveRecord::Base.connected_to(role: :writing) do
      self.update(repository_selection_cache: current_repository_selection)
    end

    current_repository_selection
  end

  # Public: Determine if the installation
  # has been suspended by either the installation
  # target or by the Integration.
  #
  # Returns a Boolean.
  def suspended?
    integrator_suspended? || user_suspended?
  end

  # Get the latest suspended_at timestamp.
  #
  # Returns a String or nil.
  def suspended_at
    return unless suspended?

    # If one or the other is `nil` don't bother comparing timestamps.
    return integrator_suspended_at if user_suspended_at.nil?
    return user_suspended_at       if integrator_suspended_at.nil?

    # If both are set then we return the latest timestamp.
    integrator_suspended_at > user_suspended_at ? integrator_suspended_at : user_suspended_at
  end

  # Returns the latest actor who suspended the installation.
  #
  # Returns a User or nil.
  def suspended_by
    return unless suspended?

    # If one or the other is `nil` don't bother comparing timestamps.
    return self.bot          if user_suspended_at.nil?
    return user_suspended_by if integrator_suspended_at.nil?

    # If both are set then we return the latest actor who suspended the installation.
    integrator_suspended_at > user_suspended_at ? self.bot : user_suspended_by
  end

  # Public: Determine if the Integration
  # suspended its own installation.
  #
  # Returns a Boolean.
  def integrator_suspended?
    !!(super && integrator_suspended_at)
  end

  def suspend!(user: nil)
    suspended_at = Time.zone.now

    options = {}.tap do |opt|
      if user
        opt[:user_suspended_by] = user
        opt[:user_suspended_at] = suspended_at
      else
        # Set the Bot so that the event always has an actor.
        user = self.bot

        opt[:integrator_suspended]    = true
        opt[:integrator_suspended_at] = suspended_at
      end
    end

    update!(options)
    instrument :suspend, { actor_id: user.id }

    true
  end

  def unsuspend!(user: nil)
    options = {}.tap do |opt|
      if user
        opt[:user_suspended_by] = nil
        opt[:user_suspended_at] = nil
      else
        # Set the Bot so that the event always has an actor.
        user = self.bot

        opt[:integrator_suspended] = false
        opt[:integrator_suspended_at] = nil
      end
    end

    update!(options)
    instrument :unsuspend, { actor_id: user.id }

    true
  end

  # Public: Determine if a User actor
  # has suspended the installation on
  # behalf of the target.
  #
  # Returns a Boolean.
  def user_suspended?
    !!(user_suspended_by_id && user_suspended_at)
  end

  private

  # Internal: should we do instrumentation asynchronously?
  #
  # repository_ids  - Array of Integers representing the repos to instrument
  # async           - Boolean, whether to even consider doing this work async
  #
  # Returns a Boolean.
  def instrument_async?(repository_ids, async)
    !!async &&
      repository_ids.count >= MAX_REPOS_TO_INSTRUMENT &&
      GitHub.flipper[:instrument_integration_installation_in_background].enabled?
  end

  # Private: Serializes this IntegrationInstallation as a webhook payload for any apps
  # that listen for Hook::Event::IntegrationInstallationEvents. Under normal circumstances
  # we deliver webhook events using instrumentation, but this must be called as
  # a before_destroy and uses Hook::Event#generate_payload_and_deliver_later to
  # serialize the webhook payload before the record becomes unavailable.
  #
  # actor - The User who is performing the event (optional).
  def generate_webhook_payload(actor: nil)
    if integration.send_deprecated_event?
      # To be removed on Nov 22 2017 (https://github.com/github/platform-integrations/issues/283)
      deprecated_event = Hook::Event::IntegrationInstallationEvent.new(
        action: :deleted,
        installation_id: id,
        integration_id: integration_id,
        actor_id: (actor.try(:id) || GitHub.context[:actor_id]),
        triggered_at: Time.now,
      )

      @deprecated_delivery_system = Hook::DeliverySystem.new(deprecated_event)
      @deprecated_delivery_system.generate_hookshot_payloads
    end

    event = Hook::Event::InstallationEvent.new(
      action: :deleted,
      installation_id: id,
      integration_id: integration_id,
      actor_id: (actor.try(:id) || GitHub.context[:actor_id]),
      triggered_at: Time.now,
    )

    @delivery_system = Hook::DeliverySystem.new(event)
    @delivery_system.generate_hookshot_payloads
  end

  # Private: Queue the payloads generated above for delivery to Hookshot.
  def queue_webhook_delivery
    if integration.send_deprecated_event?
      return unless defined?(@delivery_system && @deprecated_delivery_system)
      @deprecated_delivery_system.deliver_later
      @delivery_system.deliver_later
    else
      return unless defined?(@delivery_system)
      @delivery_system.deliver_later
    end
  end

  def override_rate_limit
    self[:rate_limit] if using_override_rate_limit?
  end

  def using_override_rate_limit?
    self[:rate_limit].to_i > GitHub.api_default_rate_limit
  end

  # Internal: An after_destroy callback which nullifies the related
  # subscription item's installed_at
  #
  # Returns nothing
  def clear_subscription_item_installation
    subscription_item&.clear_marketplace_installation
  end

  # Internal: A callback which updates the installation_count for the
  # associated integration in the search index.
  def update_integration_installation_count
    integration&.synchronize_search_index
  end
end
