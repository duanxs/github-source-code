# frozen_string_literal: true

class FgpMetadata
  attr_reader :label, :category, :description

  # Fine Grained Permission metadata
  def initialize(fgp)
    @label = fgp
    @category = FgpMetadata.category_for(fgp)
    @description = FgpMetadata.description_for(fgp)
  end

  # FGP contains the metadata for an individual fine grained permission
  def self.for(fgp)
    new(fgp.to_sym)
  end

  # Public: get all the categories and FGPs for a role
  #
  # - role: the Role object
  #
  # Returns a Hash of categories titles to FGP descriptions
  def self.for_role(role)
    perms = role.permissions.map(&:action)

    CATEGORIES.each_with_object(Hash.new { |h, k| h[k]=[] }) do |(category, permissions), result|
      permissions.each do |category_permission|
        if perms.include?(category_permission.to_s)
          result[title_for(category)] << description_for(category_permission)
        end
      end
    end
  end

  def self.categories
    CATEGORIES.keys
  end

  def self.category_for(fgp)
    CATEGORIES.each do |category, fgps|
      return category if fgps.include?(fgp)
    end

    :unknown
  end

  def self.description_for(fgp)
    DESCRIPTIONS[fgp] || "unknown"
  end

  # Public: the human readable title for every FGP category
  def self.title_for(category)
    case category
    when :issues
      "Issue"
    when :prs
      "Pull Request"
    when :issues_prs
      "Issue and Pull Request"
    when :repository
      "Repository"
    end
  end

  # Public: the octicon for every FGP category
  def self.icon_for(category)
    case category
    when :issues
      "issue-opened"
    when :prs
      "git-pull-request"
    when :issues_prs
      "request-changes"
    when :repository
      "repo"
    end
  end

  # Public: the octicon for every FGP category by title
  def self.icon_for_title(title)
    case title
    when "Issue"
      "issue-opened"
    when "Pull Request"
      "git-pull-request"
    when "Issue and Pull Request"
      "request-changes"
    when "Repository"
      "repo"
    end
  end

  DESCRIPTIONS = {
    add_assignee:                "Assign or remove a user",
    remove_assignee:             "Remove an assigned user",
    add_label:                   "Add or remove a label",
    remove_label:                "Remove a label",
    close_issue:                 "Close an issue",
    reopen_issue:                "Reopen a closed issue",
    delete_issue:                "Delete an issue",
    mark_as_duplicate:           "Mark an issue as a duplicate",
    close_pull_request:          "Close a pull request",
    reopen_pull_request:         "Reopen a closed pull request",
    request_pr_review:           "Request a pull request review",
    manage_settings_merge_types: "Manage pull request merging settings",
    manage_settings_pages:       "Manage GitHub Page settings",
    manage_settings_projects:    "Manage project settings",
    manage_settings_wiki:        "Manage wiki settings",
    manage_topics:               "Manage topics",
    manage_deploy_keys:          "Manage deploy keys",
    manage_webhooks:             "Manage webhooks",
    push_protected_branch:       "Push commits to protected branches",
    set_interaction_limits:      "Set interaction limits",
    set_milestone:               "Set milestones",
    set_social_preview:          "Set the social preview",
    edit_repo_metadata:          "Edit repository metadata",
  }

  CATEGORIES = {
    issues_prs: %i[
      add_assignee
      remove_assignee
      remove_label
      add_label
    ],
    issues: %i[
      close_issue
      reopen_issue
      delete_issue
      mark_as_duplicate
    ],
    prs: %i[
      close_pull_request
      reopen_pull_request
      request_pr_review
    ],
    repository: %i[
      manage_settings_merge_types
      manage_settings_pages
      manage_settings_projects
      manage_settings_wiki
      manage_topics
      push_protected_branch
      set_interaction_limits
      set_milestone
      set_social_preview
      edit_repo_metadata
      manage_deploy_keys
      manage_webhooks
    ],
  }
end
