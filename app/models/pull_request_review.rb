# rubocop:disable Style/FrozenStringLiteralComment

class PullRequestReview < ApplicationRecord::Domain::Repositories
  areas_of_responsibility :pull_requests
  include GitHub::UTF8
  include GitHub::UserContent
  include GitHub::Relay::GlobalIdentification
  include Spam::Spammable
  include MethodTiming
  include Instrumentation::Model
  include EmailReceivable
  include Workflow
  include NotificationsContent::WithCallbacks
  include PushNotifiable
  include Reaction::Subject::RepositoryContext
  include UserContentEditable
  include InteractionBanValidation
  include AuthorAssociable
  include Blockable
  include AbuseReportable
  include GitHub::RateLimitedCreation

  extend GitHub::BackgroundDependentDeletes

  NEEDS_COMMENTS_WHEN_REQUESTING_CHANGES = "You need to leave a comment indicating the requested changes."

  SUBMISSION_EVENTS = [:comment, :approve, :request_changes]
  ALL_EVENTS = SUBMISSION_EVENTS + [:dismiss]

  validates :body, bytesize: { maximum: MYSQL_UNICODE_BLOB_LIMIT }, unicode: true
  validate :user_can_interact?, on: :create

  workflow :state do
    state :pending, 0 do
      event :comment, transitions_to: :commented
      event :approve, transitions_to: :approved
      event :request_changes, transitions_to: :changes_requested
    end

    state :commented, 1 do
      on_entry { after_submission }
    end
    state :changes_requested, 30 do
      on_entry { after_submission }
      event :dismiss,    transitions_to: :dismissed
    end
    state :approved, 40 do
      on_entry { after_submission }
      event :dismiss,    transitions_to: :dismissed
    end
    state :dismissed, 50

    on_transition do |from, to, event, *event_args, **kwargs|
      Rails.logger.debug { "on_transition from #{from} to #{to} with #{event_args}" }
    end
  end

  # NOTE: All after_* callback logic should go into the below methods.
  # It is much easier to follow the sequence of callback logic by keeping them
  # to a single method each, rather than having scattered callbacks all over.
  after_create :after_create
  after_commit :after_commit, if: :call_after_commit?
  after_commit :instrument_update_event, on: :update, if: :body_changed_after_commit?
  after_commit :notify_socket_subscribers, on: :update, if: :body_changed_after_commit?
  after_commit :instrument_dismiss_event, on: :update
  after_commit :update_subscriptions_and_notify, on: :update, if: :update_subscriptions_and_notify?

  after_destroy :after_destroy

  belongs_to :pull_request, touch: true
  belongs_to :user

  has_many :review_comments, class_name: "PullRequestReviewComment", inverse_of: :pull_request_review
  has_many :review_threads, class_name: "PullRequestReviewThread", inverse_of: :pull_request_review
  destroy_dependents_in_background :review_threads

  has_one :repository, through: :pull_request
  has_and_belongs_to_many :review_requests

  has_many :reactions, as: :subject

  validates :head_sha, presence: true
  validates :pull_request_id, presence: true
  validates :state, presence: true
  validates :user_id, presence: true
  validate :one_pending_review_per_user_and_pull_request, if: :pending?
  validate :validate_pull_request_lock, on: :create
  validate :ensure_creator_is_not_blocked, unless: :comment_hidden_changed?
  validate :explanation_exists?, if: :explanation_required?

  setup_spammable(:user)

  # Public: Return non-pending reviews
  scope :submitted, -> { where("state <> ?", state_value(:pending)) }

  # Public: Return pending reviews
  scope :pending, -> { where("state = ?", state_value(:pending)) }

  # Limited to those on repositories in a given organization
  scope :for_organization, ->(organization) { joins(:repository).where(repositories: {organization_id: organization}) }

  scope :excluding_organization_ids, -> (ids) do
    if ids.any?
      joins(:repository).
        where("repositories.organization_id IS NULL OR repositories.organization_id NOT IN (?)", ids)
    else
      scoped
    end
  end

  # All visible, non-pending, reviews for the current viewer. Viewers can see
  # their own pending reviews.
  #
  # viewer - The User who is viewing the reviews (current_user)
  scope :for_viewer, -> (viewer) {
    if viewer
      where("`pull_request_reviews`.`user_id` = ? OR `pull_request_reviews`.`state` <> ?", viewer.id, PullRequestReview.state_value(:pending))
    else
      where("`pull_request_reviews`.`state` <> ?", PullRequestReview.state_value(:pending))
    end
  }

  # All reviews that are visible to a viewer on the timeline.
  #
  # We attempted to split this in smaller scopes (see 912a1e8, https://git.io/vQKe3), but faced
  # issues with ActiveRecord in Rails 3.2. Once we are on Rails 5, this can be split up
  # into the following three scopes:
  #  * without_replies_only
  #  * without_commented
  #  * without_empty_body
  #
  # viewer - User who is viewing the timeline
  scope :visible_in_timeline_for, -> (viewer) {
    joins(<<~SQL).where(<<~SQL, commented: PullRequestReview.state_value(:commented)).
      LEFT OUTER JOIN `pull_request_review_comments` `comments`
         ON `comments`.`pull_request_review_id` = `pull_request_reviews`.`id`
        AND `comments`.`reply_to_id` IS NULL
    SQL
      `comments`.`id` IS NOT NULL OR
      `pull_request_reviews`.`state` != :commented OR
      (`pull_request_reviews`.`body` IS NOT NULL AND `pull_request_reviews`.`body` <> '')
    SQL
    group("`pull_request_reviews`.`id`").
    order("NULL"). # Optimization: Avoid sorting when grouping
    for_viewer(viewer).
    filter_spam_for(viewer)
  }

  # Public: Return the most recent review for each user
  #
  # Returns Array of PullRequestReview's
  def self.recent_approved_per_user(pull_request)
    # Group-wise maximum against id column rather than updated_at because
    # http://bugs.mysql.com/bug.php?id=54784
    sql = <<-SQL
      SELECT r1.*
      FROM pull_request_reviews r1
      INNER JOIN
      (
        SELECT max(id) as id
        FROM pull_request_reviews
        WHERE pull_request_id = :pull_request_id
        AND   state = :state
        GROUP BY user_id
      ) r2
      ON r1.id = r2.id
      ORDER BY r1.updated_at desc
    SQL
    github_sql.new(sql, state: state_value(:approved), pull_request_id: pull_request.id).models(self)
  end

  # Get the Integer value matching a certain state
  def self.state_value(name)
    workflow_spec.states[name.to_sym].value
  end

  # Gets the name of the state from the integer value
  def self.state_name(int)
    result = workflow_spec.states.values.find { |state| state.value == int }
    result ? result.name : nil
  end

  # Fallback on the ghost user when the original author's been deleted.
  # See User.ghost for more.
  def safe_user
    user || User.ghost
  end

  # Internal: Determine if an event is valid to be called from the current state
  # for this pull request review.
  def can_trigger?(event)
    event = event.to_sym
    raise(ArgumentError.new("unknown event")) unless ALL_EVENTS.include?(event)
    method = "can_#{event}?"
    public_send(method)
  end

  # Attempts to transistion the PullRequestReview with the given event, if valid
  def trigger(event, actor: nil, message: nil)
    case event.to_sym
    when :approve
      self.approve!
    when :request_changes
      self.request_changes!
    when :comment
      self.comment!
    when :dismiss
      self.dismiss!(actor, message: message)
    end
  end

  def build_thread
    review_threads.build(pull_request: pull_request)
  end

  # Basically just a convenience method instead of building the thread and then the
  # comment explicitly.
  def build_thread_with_comment(user: nil, body:, position:, path:, diff: nil)
    thread = build_thread
    comment = thread.build_first_diff_position_comment(
      user: user,
      body: body,
      position: position,
      path: path,
      diff: diff,
    )
    [thread, comment]
  end

  def async_performed_via_integration
    Promise.resolve(nil)
  end

  # Override reading the body to guarantee returning valid utf8 encoded data.
  #
  # See also GitHub::UTF8
  def body
    utf8(read_attribute(:body))
  end

  # Instrumentation
  def instrument_update_event
    return if pending?
    # This could technically fail if there are two `.save`s
    # in the same transaction. In that case, the first `previous_changes[:body]`
    # would be discarded by the second save, then the transaction would finish
    # and call this method.
    #
    # This method is tested at controller-level so we can make sure it works
    # correctly from a user's point of view.
    old_body = previous_changes[:body].try(:first)
    instrument :update, review_id: id, changes: {old_body: old_body, body: body}
    GlobalInstrumenter.instrument("pull_request_review.update",
      review: self,
      old_body: old_body,
    )
  end

  def instrument_dismiss_event
    return if @dismisser.nil? || !dismissed? || !previous_changes.key?(:state)

    payload = {
      review_id: id,
      issue_id: pull_request.issue.id,
      actor_id: @dismisser.id,
    }
    instrument(:dismiss, payload)
    GlobalInstrumenter.instrument("pull_request_review.dismiss",
      review: self,
      actor: @dismisser,
    )
    @dismisser = nil
  end

  def notify_socket_subscribers
    data = {
        timestamp: Time.now.to_i,
        wait: default_live_updates_wait,
        reason: "pull request review ##{id} updated",
        gid: global_relay_id,
    }
    channel = GitHub::WebSocket::Channels.pull_request_review(self)
    GitHub::WebSocket.notify_pull_request_channel(pull_request, channel, data)
  end

  # Returns the children items for the purposes of rendering review(s) on a timeline -
  # i.e. on PullRequest#show
  #
  # See also IssueTimeline#child_enumerator
  #
  # Returns PullRequestReviewComments
  def timeline_children
    review_comments.inject([]) { |result, comment|
      (result << comment).concat(comment.replies)
    }.uniq
  end

  def async_notifications_list
    async_repository
  end

  def notifications_thread
    pull_request.issue
  end

  def notifications_author
    user
  end

  # Summary of the review used by notifications.
  def notifications_summary_title
    "@#{notifications_author.login} #{state_summary} this pull request."
  end

  class InvalidStateForSummary < RuntimeError; end

  # Internal: Returns a summary of the action taken by the reviewer.
  #
  # Returns a String or raises an InvalidStateForSummary error.
  def state_summary
    case current_state.name
    when :changes_requested
      "requested changes on"
    when :approved
      "approved"
    when :commented
      "commented on"
    when :dismissed
      "dismissed review on"
    else
      raise InvalidStateForSummary, "invalid state for summary: #{state}"
    end
  end

  def update_notification_summary?
    !pending? && super
  end

  # Overrides Summarizable#update_notification_rollup. Reviews are summarized
  # in a non-standard way, so we override this method to use the custom
  # summary method.
  #
  # summary - RollupSummary object for the thread.
  #
  # Returns nothing.
  def update_notification_rollup(summary)
    suffix = summarizable_changed?(:body) ? :changed : :unchanged
    GitHub.dogstats.increment("newsies.rollup", tags: ["type:#{suffix}"])

    summary.summarize_pull_request_review(self)
  end

  # Overrides GitHub::UserContent#mentioned_users
  #
  # Users mentioned in the body and all comments of the pull request review.
  #
  # Returns the Array of User objects.
  def mentioned_users
    users = super
    review_comments.each do |comment|
      users += comment.mentioned_users
    end

    users
  end

  # Overrides GitHub::UserContent#mentioned_usernames
  #
  # User names mentioned in the body and all comments of the pull request review.
  #
  # Returns the Array of user names (logins).
  def mentioned_usernames
    user_names = super
    review_comments.each do |comment|
      user_names += comment.mentioned_usernames
    end

    user_names
  end

  # Overrides GitHub::UserContent#mentioned_teams
  #
  # Teams mentioned in the body and all comments of the pull request review.
  #
  # Returns the Array of Team objects.
  def mentioned_teams
    teams = super
    review_comments.each do |comment|
      teams += comment.mentioned_teams
    end

    teams
  end

  # Overrides PushNotifiable#push_notification_title
  #
  # Returns a String to generate the title of a mobile push notification
  def push_notification_title
    "@#{notifications_author.login} mentioned you in their review"
  end

  def destroy_pending_comments
    review_comments.with_pending_state.destroy_all

    destroy_if_empty
  end

  # Public: destroy this PullRequestReview if there are no comments and it is not approved
  def destroy_if_empty
    destroy if no_body_and_no_comments? && !approved?
  end

  # See IssueTimeline
  def timeline_sort_by
    [submitted_at || created_at]
  end

  def editable_by?(user)
    return false unless user
    self.user == user || repository.pushable_by?(user)
  end

  def async_viewer_can_update?(viewer)
    async_viewer_cannot_update_reasons(viewer).then(&:empty?)
  end

  def async_viewer_cannot_update_reasons(viewer)
    return Promise.resolve([:login_required]) unless viewer

    Platform::Loaders::ActiveRecordAssociation.load_all(self, [:pull_request, :repository, :user]).then do
      Platform::Loaders::ActiveRecordAssociation.load_all(self.pull_request, [:issue]).then do
        Platform::Loaders::ActiveRecordAssociation.load_all(self.pull_request.issue, [:conversation, :repository]).then do
          errors = []
          errors << :locked if self.pull_request.issue.locked_for?(viewer)
          errors << :insufficient_access if !editable_by?(viewer)
          errors
        end
      end
    end
  end

  def async_viewer_can_delete?(viewer)
    return Promise.resolve(false) unless viewer
    return Promise.resolve(true) if viewer.site_admin?

    async_viewer_can_update?(viewer)
  end

  def get_notification_summary
    return unless pull_request && pull_request.issue
    list = Newsies::List.new("Repository", pull_request.repository_id)
    GitHub.newsies.web.find_rollup_summary_by_thread(list, pull_request.issue)
  end

  # Overrides PushNotifiable#push_notification_subtitle
  def push_notification_subtitle
    pull_request.issue.push_notification_subtitle
  end

  # Absolute permalink for this PullRequestReview
  #
  # include_host - Turn off the `GitHub.url` host in the url. (default true)
  #                review.permalink(include_host: false) => `/github/github/pull/4#pullrequestreview-4`
  #
  def permalink(include_host: true)
    "#{pull_request.permalink(include_host: include_host)}##{anchor}"
  end
  alias url permalink

  def async_path_uri
    return @async_path_uri if defined?(@async_path_uri)

    @async_path_uri = async_pull_request.then(&:async_path_uri).then do |path_uri|
      path_uri = path_uri.dup
      path_uri.fragment = anchor
      path_uri
    end
  end

  def anchor
    "pullrequestreview-#{id}"
  end

  def writer?
    async_author_can_push_to_repository?.sync
  end

  def async_author_can_push_to_repository?
    Promise.all([async_user, async_pull_request.then(&:async_repository)]).then do |user, repository|
      next false unless user && repository

      repository.async_pushable_by?(user)
    end
  end

  def author_can_push_to_repository?
    async_author_can_push_to_repository?.sync
  end

  # Public: Are all the comments on this Review a reply?
  #
  # Returns a Boolean
  def all_replies?
    review_comments.any? && review_comments.all?(&:reply?)
  end

  # Public: Should this review show in the timeline?
  #
  # Returns a Boolean
  def show_in_timeline?
    !(all_replies? && body.blank?) || !commented?
  end

  # Public: Should this review satisfy a request for review?
  # If a review is not a comment reply, pending, or the PR author then the request is satisfied
  # and should be deleted.
  #
  # Returns a Boolean
  def satisfies_request?
    show_in_timeline? && !pending? && pull_request.user != user
  end

  # Public: Does this review lack a body and also associated comments?
  #
  # Returns a Boolean
  def no_body_and_no_comments?
    body.blank? && review_comments.empty?
  end

  # Public: Return an Array of DeprecatedPullRequestReviewThread(s) for this review
  #
  # Populated with comments already loaded in PullRequest#timeline_items_for.
  def threads
    DeprecatedPullRequestReviewThread.group_comments(pull_request, timeline_children)
  end

  # Public: Retrieve list of pull request review threads for this review visible to the viewer
  #
  # Return Promise<Array<PullRequestReviewThread>>
  def async_review_threads_for(viewer)
    Platform::Loaders::PullRequestReview::ReviewThreads.load(id, viewer)
  end

  def async_review_comments_for(viewer)
    Platform::Loaders::PullRequestReviewCommentsForReview.load(id, viewer)
  end

  # Used by HTML::Pipeline to determine what to linkify
  alias_method :entity, :repository
  alias_method :async_entity, :async_repository

  def can_be_dismissed_by?(viewer)
    if pull_request.protected_base_branch && pull_request.protected_base_branch.restricted_dismissed_reviews?
      pull_request.protected_base_branch.review_dismissable_by?(viewer)
    else
      pull_request.repository.pushable_by?(viewer)
    end
  end

  # Public: Gets the ID of the repository.
  #
  # Used around the codebase to ensure that this review belongs to a specific
  # repository
  def repository_id
    repository.id
  end

  # Public: Checks spamminess of associated pull request.
  #
  # Returns a Boolean.
  def belongs_to_spammy_content?
    pull_request&.spammy?
  end

  DIFF_URI_TEMPLATE = Addressable::Template.new("/{owner}/{name}/pull/{number}/files/{oid}").freeze

  # Public: Get the URI to the diff that this review was created against.
  def async_diff_uri
    Promise.all([async_repository, async_pull_request]).then do |repository, pull_request|
      Promise.all([repository.async_owner, pull_request.async_issue]).then do |owner, issue|
        DIFF_URI_TEMPLATE.expand({
          owner: owner.login,
          name: repository.name,
          number: issue.number,
          oid: head_sha,
        })
      end
    end
  end

  # Find all the teams on behalf of whom this review was performed.
  #
  # Returns Promise<Array<Team>>.
  def async_on_behalf_of_teams
    async_review_requests.then do |review_requests|
      team_review_requests = review_requests.select do |request|
        request.reviewer_type == "Team"
      end

      on_behalf_of_team_ids = team_review_requests.map(&:reviewer_id)
      Platform::Loaders::ActiveRecord.load_all(::Team, on_behalf_of_team_ids).then(&:compact)
    end
  end

  # Find all the teams on behalf of whom this review was performed,
  # and which are visible to the given viewer.
  #
  # Returns Promise<Array<Team>>.
  def async_on_behalf_of_visible_teams_for(viewer)
    return Promise.resolve([]) unless viewer

    async_on_behalf_of_teams.then { |teams|
      async_team_visibilities = Promise.all(teams.map { |team| team.async_visible_to?(viewer) })

      async_team_visibilities.then do |visibilities|
        result = Set.new
        teams.zip(visibilities) { |team, visible| result.add(team) if visible }
        result.to_a
      end
    }
  end

  # Internal: Should we enqueue the SubscribeAndNotifyJob when the review is
  # updated?
  #
  # This method checks that we're updating an already submitted review.
  # Since reviews are updated on submission, and `subscribe_and_notify` is triggered
  # during submission, we want to avoid enqueueing a duplicate SubscribeAndNotifyJob
  # here.
  #
  # Returns a Boolean.
  def update_subscriptions_and_notify?
    body_previously_changed? &&
      !pending? &&
      !submitted_at_previously_changed?
  end

  # Internal: Implements the NotificationsContent#unsubscribable_users method.
  #
  # Filters out users that should keep a subscription to this thread.
  #
  # users - Array of Users.
  #
  # Returns an Array of Users that can be unsubscribed.
  def unsubscribable_users(users)
    pull_request.issue.unsubscribable_users(users)
  end

  # Private: handle a successfully submitted PullRequestReview. This fires "on_entry"
  # to the submitted state, which means that the state has been successfully
  # transitioned to and persisted.
  #
  # Returns true if completed successfully.
  private def after_submission
    Rails.logger.debug { "[submission] after_submission for #{self} in #{current_state}" }
    return false unless valid?

    review_comments.with_pending_state.each(&:submit!)

    fulfill_requested_review
    @call_after_commit = true
    Rails.logger.debug { "[submission] after_submission for #{self} completed" }
    true
  end

  time_method :after_submission, key: "pull_request_review.after_submission"

  # Private: Should after_commit callbacks be run? This is set after_submission
  # completes successfully.
  private def call_after_commit?
    @call_after_commit
  end

  # Private: Run after_commit logic.
  private def after_commit
    payload = {
      id: id,
      state: state,
      issue_id: pull_request.issue.id,
      actor_id: user_id,
    }
    instrument(:submit, payload)
    GlobalInstrumenter.instrument("pull_request_review.submit", review: self)
    review_comments.with_submitted_state.each(&:instrument_submission)
    subscribe_and_notify
    pull_request.notify_socket_subscribers
    pull_request.synchronize_search_index
    notify_state_changed if state_previously_changed? && current_state > :commented
    @call_after_commit = false
  end

  private def notify_state_changed
    channel = GitHub::WebSocket::Channels.pull_request_review_state(pull_request)
    GitHub::WebSocket.notify_pull_request_channel(
      pull_request,
      channel,
      { review_id: id, state: current_state.name,
        timestamp: Time.now.to_i,
        wait: default_live_updates_wait },
    )
  end

  # Called on approve! before persistence.
  protected def approve(*args, **kwargs)
    if pull_request.user == user
      return halt "Can not approve your own pull request"
    end

    set_submitted_at
    nil
  end

  # Called on comment! before persistence.
  protected def comment(*args, **kwargs)
    if no_body_and_no_comments?
      return halt(NEEDS_COMMENTS_WHEN_REQUESTING_CHANGES)
    end

    set_submitted_at
    nil
  end

  # Called on request_changes! before persistence.
  protected def request_changes(*args, **kwargs)
    if no_body_and_no_comments?
      return halt NEEDS_COMMENTS_WHEN_REQUESTING_CHANGES
    end

    if pull_request.user == user
      return halt "Can not request changes on your own pull request"
    end

    set_submitted_at
    nil
  end

  # Protected: Guards against invalid dismiss! events. This is automatically
  # invoked by the Workflow before the state transistion happens.
  #
  # message         - (Optional) A String describing why the review was dismissed.
  # via_commit_oid  - (Optional) May be provided instead of a message if the
  #                   review was automatically dismissed due to new changes
  #                   being pushed.
  #
  # Returns nothing.
  protected def dismiss(user, message: nil, via_commit_oid: nil)
    if !pull_request.open?
      return halt "Can only dismiss reviews on open pull requests"
    end

    if message.blank? && via_commit_oid.blank?
      return halt "A message or commit OID is required to dismiss a review"
    end

    pull_request.events.create(
      event: "review_dismissed",
      actor_id: user.id,
      pull_request_review_state_was: state,
      pull_request_review_id: id,
      after_commit_oid: via_commit_oid,
      message: message,
    )
    @dismisser = user
  end

  def readable_by?(actor)
    async_readable_by?(actor).sync
  end

  def async_readable_by?(actor)
    async_pull_request.then do |pull_request|
      next false unless pull_request
      pull_request.async_readable_by?(actor)
    end
  end

  # Overrides NotificationsContent#defer_loading_mentions? to defer loading
  # mentioned users and teams to background jobs to increase performance
  def defer_loading_mentions?
    true
  end

  # Public: Has the pull request this review was left on changed since
  #         the review was submitted?
  #
  #
  # Returns a Boolean.
  def pull_request_has_changed?
    head_sha != pull_request.head_sha
  end

  # Public: Does this review's head_sha still appear in the pull request's
  #         changed commits?
  #
  # Returns a Boolean.
  def applies_to_current_diff?
    return @applies_to_current_diff if defined?(@applies_to_current_diff)
    @applies_to_current_diff = pull_request.changed_commit_oids.include?(head_sha)
  end

  # Public: List of changed commit oids following the review's head_sha.
  #
  # Returns an Array of Strings.
  def pull_request_oids_since
    return @pull_request_oids_since if defined?(@pull_request_oids_since)

    index = pull_request.changed_commit_oids.index(head_sha)
    index ||= -1 # If head_sha isn't in changed_commits we want them all.
    @pull_request_oids_since = pull_request.changed_commit_oids.from(index + 1)
  end

  private

  # Private: set submitted_at
  def set_submitted_at
    self.submitted_at = Time.zone.now
  end

  # Private: Do any necessary after_create processing. All after_create
  # logic should be called from this method.
  def after_create
    GitHub.dogstats.increment("pull_request_review", tags: ["action:create"])
    Contribution.clear_caches_for_user(user)

    GlobalInstrumenter.instrument("pull_request_review.create", {
      repository: pull_request.repository,
      review: self,
    })
  end

  # Private: Do any necessary after_destroy processing. All after_destroy
  # logic should be called from this method.
  def after_destroy
    GitHub.dogstats.increment("pull_request_review", tags: ["action:destroy"])
    destroy_notification_summary if related_repo_exists_when_defined
  end

  # Private: Subscribe the author of the review
  def subscribe_author
    subscribe(user, :comment)
  end

  # Private: enforce one pending review per [User / Pull Request]
  def one_pending_review_per_user_and_pull_request
    dupes = self.class.default_scoped.where(state: 0, pull_request_id: pull_request_id, user_id: user_id)
    dupes = dupes.where("id <> ?", id) if persisted?
    if dupes.exists?
      errors.add(:user_id, "can only have one pending review per pull request")
    end
  end

  def event_payload
    {
      review: self,
      spammy: user.spammy?,
      pull_request_id: pull_request.id,
      body: body,
      allowed: allowed?,
    }
  end

  def allowed?
    pull_request.repository.permit?(user, :write)
  end

  # Private: Validation to see if review is allowed based on pull request lock
  # Only someone allowed to push to the repository can comment on a locked pull request
  # (unlocked pull requsts have no such restriction)
  #
  # Returns true if comment is allowed, adds an error otherwise
  def validate_pull_request_lock
    return true unless pull_request.issue.locked?
    return true if pull_request.repository.pushable_by?(user)

    errors.add(:base, "lock prevents review")
  end

  # Private: Is there an explanation needed along with this review? An explanation
  # means _either_ body text on the review or associated review_comments.
  def explanation_required?
    commented? || changes_requested?
  end

  # Private: Validation to see if the review has a necessary explanation.
  #
  # Returns nothing.
  def explanation_exists?
    # The explanation can either be a review body or comments.
    return true if body.present? || review_comments.present?

    errors.add(:body, "required for pull request reviews that are comments or requesting changes")
  end

  # Private: Validation to ensure the creator of this review is not blocked
  def ensure_creator_is_not_blocked
    return unless user && pull_request

    if pull_request.blocked_from_reviewing?(user)
      errors.add(:user, "is blocked")
    end
  end

  # Private: Satisfy a ReviewRequest via this PullRequestReview
  def fulfill_requested_review
    return unless satisfies_request?

    fullfilled_team_requests = pull_request.team_requests_on_behalf_of(user)
    return unless fullfilled_team_requests.any? || pull_request.review_requested_for?(user)

    pending_requests = pull_request.review_requests_for(user)
    self.review_requests = [fullfilled_team_requests, pending_requests].compact.reduce([], :|)
  end

  # Private: Override workflow-orchestrator gem to use update_attributes instead of
  # update_column - this ensures that standard Rails callbacks still fire.
  def persist_workflow_state(new_value)
    update self.class.workflow_column => new_value
  end

  # Private: Override process_event! to wrap everything in a transaction.  We need
  # to do this to ensure a deterministic order of callbacks when events are fired.
  #
  # This ensures the following reliable order for a call like review.approve!:
  #
  #  BEGIN (transaction)
  #    * before_transition
  #    * approve (i.e. action method)
  #    * on_transition
  #    * on_exit
  #    * persist_workflow_state
  #    * on_entry
  #    * after_transition
  #  COMMIT
  #  after_commit (active_record))
  #
  # If we didn't do this, you could end up with the following confusing chain of
  # events (note the difference in when after_commit vs on_entry happens):
  #
  #    * before_transition
  #    * approve (i.e. action method)
  #    * on_transition
  #    * on_exit
  #    BEGIN (transaction)
  #    * persist_workflow_state
  #    COMMIT
  #    * after_commit (active_record)
  #    * on_entry
  #    * after_transition
  def process_event!(name, *args, **kwargs)
    GitHub::SchemaDomain.allowing_cross_domain_transactions do
      self.transaction do
        super(name, *args, **kwargs)
      end
    end
  end

  # Was the comment body changed in previous_changes? We use this to determine
  # if body changes _only_ after_commit.
  def body_changed_after_commit?
    previous_changes.key?(:body)
  end

  # Override Summarizable#author_subscribe_reason.
  #
  # Returns String.
  def author_subscribe_reason
    "comment"
  end
end
