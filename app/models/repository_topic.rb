# frozen_string_literal: true

class RepositoryTopic < ApplicationRecord::Domain::Repositories
  include GitHub::Relay::GlobalIdentification
  include Instrumentation::Model

  belongs_to :topic
  belongs_to :repository
  belongs_to :user

  LIMIT_PER_REPOSITORY = 20

  enum state: {
    # A topic that was manually added by a user to a repository.
    created: 1,

    # A topic that was suggested via the Munger service
    # and accepted and applied by a user to a repository.
    suggested: 2,

    # A topic that was suggested via the Munger service
    # and declined by the user as not being relevant to the repository.
    declined_not_relevant: 3,

    # A topic that was suggested via the Munger service
    # and declined by the user due to it being too specific
    # (e.g. #ruby-on-rails-version-4-2-1).
    declined_too_specific: 4,

    # A topic that was suggested via the Munger service
    # and declined by the user due to personal preference.
    declined_personal_preference: 5,

    # A topic that was suggested via the Munger service
    # and declined by the user due to it being too general.
    declined_too_general: 6,
  }

  APPLIED_STATES = %w(created suggested).freeze
  APPLIED_STATE_VALUES = APPLIED_STATES.map { |state| self.states[state] }.freeze

  validates :topic, :repository, :state, :user, presence: true
  validates :state, presence: true
  validates :repository_id, uniqueness: {scope: [:topic_id]}
  validate :repo_topic_limit_not_reached, on: :create

  after_commit :synchronize_search_index
  after_commit :instrument_add_topic, on: :create, if: :applied?
  after_commit :instrument_remove_topic, on: :destroy

  # Includes only records for topics users manually applied or accepted from
  # suggestions.
  scope :applied, -> { where(state: applied_state_values) }

  # Includes only RepositoryTopics for public repositories.
  scope :on_public_repositories, -> { joins(:repository).merge(Repository.public_scope) }

  # Includes only records for topic suggestions that the user rejected.
  scope :declined_suggestions, -> { where("state NOT IN (?)", applied_state_values) }

  # Orders the repo topics by how recently they were added to a repository.
  scope :newest_first, -> { order("repository_topics.id DESC") }


  # Returns a scope for RepositoryTopics applied to a set of Repository records.
  def self.applied_to(repository_ids: [], limit: 1_000)
    return none if repository_ids.empty?

    applied.distinct.limit(limit).where(repository_id: repository_ids)
  end

  # Returns a scope to query for the Topic ids for each of a set of Repository ids.
  def self.applied_topic_ids_by_repository(repository_ids:, limit: 1_000)
    applied_to(repository_ids: repository_ids, limit: limit).
      group(:repository_id, :topic_id).
      select(:repository_id, :topic_id)
  end

  # Public: Returns an array of integers for the RepositoryTopic states that indicate the
  # Topic has been applied to the Repository and not rejected for it.
  def self.applied_state_values
    APPLIED_STATE_VALUES
  end

  # Reindex the Repository and Topic in ElasticSearch.
  def synchronize_search_index
    if repository
      repository.synchronize_search_index
      repository.packages.each(&:synchronize_search_index)
    end

    topic.synchronize_search_index if topic
  end

  # Public: Whether or not the topic was applied to the repository.
  # Returns a Boolean.
  def applied?
    APPLIED_STATES.include?(state)
  end

  private

  def instrument_add_topic
    instrument :add_topic
  end

  def instrument_remove_topic
    instrument :remove_topic
  end

  # Audit log event prefix.
  def event_prefix
    :repo
  end

  # Audit log event payload.
  def event_payload
    {
      topic:         topic.try(:name),
      topic_id:      topic.try(:id),
      state:         state,
      repo:          repository.try(:name_with_owner),
      repo_id:       repository.try(:id),
      user:          user.try(:login),
      user_id:       user.try(:id),
      org:           repository.try(:organization).try(:login),
      org_id:        repository.try(:organization).try(:id),
      repository_topic_id: id,
    }
  end

  def repo_topic_limit_not_reached
    return unless repository

    existing_topic_count = repository.applied_repository_topics.count

    unless existing_topic_count < LIMIT_PER_REPOSITORY
      errors.add(:repository, "cannot have more than #{LIMIT_PER_REPOSITORY} topics.")
    end
  end
end
