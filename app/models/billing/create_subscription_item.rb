# frozen_string_literal: true

module Billing

  # Public: A Plain Old Ruby Object (PORO) used for creating a subscription item
  # representing a purchase.
  class CreateSubscriptionItem
    class UnprocessableError < StandardError; end
    class ForbiddenError < StandardError; end

    # inputs - Hash containing attributes to create a subscription item
    # inputs[:subscribable] - The listing plan or sponsors tier that was purchased.
    # inputs[:quantity] - How many units of this listing were purchased.
    # inputs[:account] - The account that purchased this.
    # inputs[:grant_oap] (optional) - Whether to grant OAP access to the application.
    # inputs[:viewer] - Current viewer from GraphQL context.
    def self.call(**inputs)
      new(**inputs).call
    end

    def initialize(subscribable:, quantity:, account:, grant_oap: nil, viewer:)
      @subscribable = subscribable
      @quantity     = quantity.to_i
      @account      = account
      @grant_oap    = grant_oap
      @viewer       = viewer
    end

    def call
      if account.blank?
        raise UnprocessableError.new("Account not found")
      end

      unless account.subscription_items_adminable_by?(viewer)
        raise ForbiddenError.new("#{viewer} does not have permission to manage this account (#{account})")
      end

      if account.spammy?
        raise UnprocessableError.new("Your account is flagged and unable to make purchases. Please contact support to have your account reviewed.")
      end

      if account.disabled || account.dunning?
        raise UnprocessableError.new("Your account is currently locked from purchases. Please update your payment information.")
      end

      if account.has_any_trade_restrictions?
        raise ForbiddenError.new(TradeControls::Notices::Plaintext.user_account_restricted)
      end

      if account.invoiced? && subscribable.paid?
        error_message = "Invoiced customers cannot purchase paid Marketplace plans at this time. Please contact support if you have any questions."
        raise UnprocessableError.new(error_message)
      elsif subscribable.paid? && !account.has_valid_payment_method?
        raise UnprocessableError.new("Please add a payment method before checking out.")
      end

      unless quantity > 0
        raise UnprocessableError.new("Quantity must be greater than 0.")
      end

      customer = account.customer || Billing::CreateCustomer.perform(account).customer

      account.ensure_addons_billed

      unless plan_subscription = account.async_plan_subscription.sync
        plan_subscription = Billing::PlanSubscription.create(
          user: account, customer: customer,
        )
      end

      if subscription_item = plan_subscription.subscription_items.cancelled.where(subscribable: subscribable).first
        subscription_item.quantity = quantity
      else
        subscription_item = Billing::SubscriptionItem.new(
          subscribable: subscribable,
          quantity: quantity,
          plan_subscription: plan_subscription,
        )
      end

      subscription_item_saved = Billing::SubscriptionItem.transaction do
        if grant_oap
          listing = subscribable.listing
          if listing.listable_is_oauth_application?
            account.approve_oauth_application(listing.listable, approver: viewer)
          end
        end

        subscription_item.save
      end

      if subscription_item_saved
        if subscription_item.on_free_trial?
          Billing::SubscriptionItemUpdater.perform(
            account: account.reload,
            subscribable: subscribable,
            quantity: quantity,
            start_free_trial: true,
            sender: viewer,
          )
        end

        purchase_payload = {
          subscription_item_id: subscription_item.id,
          sender_id: viewer.id,
        }

        if subscription_item.listable_is_sponsorable?
          GitHub.instrument "sponsorship.added", purchase_payload
        else
          if order_preview = viewer.marketplace_order_previews.find_by(marketplace_listing_id: subscribable.marketplace_listing_id)
            GitHub.dogstats.increment("marketplace.order_previews.completed", tags: ["notified:#{order_preview.notification_sent?}"])
            purchase_payload.merge!(
              order_preview_viewed_at: order_preview.viewed_at,
              order_preview_email_notification_sent_at: order_preview.email_notification_sent_at,
            )
            order_preview.destroy
          end

          if subscription_item.listable_is_integration?
            if installation = IntegrationInstallation.find_by(target: account, integration: subscription_item.listable)
              subscription_item.record_marketplace_installation(installed_at: installation.created_at)
            end
          end

          if subscription_item.installed_at.nil?
            Marketplace::PendingInstallations::Notice.new(user_id: viewer.id).reset
          end

          GitHub.instrument "marketplace_purchase.purchased", purchase_payload
        end

        { subscription_item: subscription_item }
      else
        errors = subscription_item.errors.full_messages.join(", ")
        raise UnprocessableError.new("Could not purchase this item: #{errors}")
      end
    end

    private

    attr_reader :grant_oap, :account, :quantity, :subscribable, :viewer
  end
end
