# frozen_string_literal: true

module Billing
  class PendingPlanChange < ApplicationRecord::Domain::Integrations
    include Instrumentation::Model

    areas_of_responsibility :gitcoin

    belongs_to :user
    belongs_to :actor, class_name: "User"
    has_many :pending_subscription_item_changes, class_name: "Billing::PendingSubscriptionItemChange", dependent: :destroy
    has_one :plan_trial, class_name: "Billing::PlanTrial"

    validates :active_on, presence: true

    after_update_commit :instrument_updated, if: :incomplete?
    after_create_commit :instrument_creation

    scope :incomplete, -> { where(is_complete: false) }
    scope :not_past, -> { where("active_on >= ?", GitHub::Billing.today) }
    scope :scheduled_for, ->(date) { where(active_on: date) }

    # Perform the scheduled plan change
    #
    # Returns a Boolean
    def run
      unless user
        errors.add(:base, "No user on pending plan change")
        return false
      end

      increment!(:attempts)

      if enterprise_cloud_plan_trial_with_coupon?
        EnterpriseCloudTrial.new(user).deactivate!
        return true
      end

      user.seats = seats if seats.present?
      user.plan = plan if plan?
      user.plan_duration = plan_duration if plan_duration?
      user.asset_status
        .update_data_packs(actor: actor, quantity: data_packs, force: true) if data_packs.present?

      self.plan_changed = user.plan_changed?
      self.plan_was = user.plan_was
      self.seats_changed = user.seats_changed?
      self.seats_was = user.seats_was
      self.plan_duration_changed = user.plan_duration_changed?
      self.plan_duration_was = user.plan_duration_was

      run_pending_subscription_item_changes

      if user.save
        if plan_changed && user.organization? && plan_was == GitHub::Plan.business_plus
          # Disable features that are only available on the business_plus plan
          user.enterprise_installations.destroy_all
          user.disable_notification_restrictions(actor: user) if user.restrict_notifications_to_verified_domains?
          user.saml_provider.destroy if user.saml_provider
          user.disable_ip_whitelisting(actor: actor) if user.ip_whitelisting_enabled?
          user.restrict_public_repo_creation_plan_downgrade(actor: actor)
          DestroyPrivatePageJob.perform_later(user)
        end

        # remove any gated features if the user is on a free plan
        user.reload
        user.remove_gated_features

        update_attribute :is_complete, true
        log_success
        track_changes

        if plan_trial.present? && plan_trial.enterprise_cloud?
          EnterpriseCloudTrial.new(user).deactivate!
        end

        true
      else
        log_failure
        user.errors.full_messages.each do |message|
          errors.add(:base, message)
        end

        false
      end
    end

    def enterprise_cloud_plan_trial_with_coupon?
      user.organization? && user.plan.business_plus? &&
        user.coupon&.business_plus_only_coupon? && plan_trial&.enterprise_cloud?
    end

    def plan
      name = read_attribute(:plan)
      MunichPlan.wrap(GitHub::Plan.find(name), target: user) if name.present?
    end

    def plan=(new_plan)
      write_attribute(:plan, new_plan.to_s)
    end

    def changing_plan?
      plan? && user.plan != plan
    end

    def changing_seats?
      seats.present? && user.seats != seats
    end

    def changing_duration?
      plan_duration? && user.plan_duration != plan_duration
    end

    def changing_data_packs?
      data_packs.present? && user.data_packs != data_packs
    end

    def incomplete?
      !is_complete?
    end

    # Public: Cancel a plan change so that it won't run as scheduled
    #
    # Returns Boolean wheter the cancellation succeeded
    def cancel
      update(is_complete: true)
    end

    private

    attr_accessor :plan_changed, :plan_was, :seats_changed, :seats_was,
      :plan_duration_changed, :plan_duration_was

    def event_prefix
      "pending_plan_change"
    end

    def instrument_creation
      payload = {
        active_on: active_on,
        actor: actor || user,
        user: user,
      }

      if changing_plan?
        payload[:plan_was] = user.plan.display_name
        payload[:plan] = plan.display_name
      end

      if changing_duration?
        payload[:plan_duration_was] = user.plan_duration
        payload[:plan_duration] = plan_duration.to_s
      end

      if changing_seats?
        payload[:seats_was] = user.seats.to_i
        payload[:seats] = seats
      end

      instrument :create, payload
    end

    def instrument_updated
      payload = {
        active_on: active_on,
        user: user,
      }
      if previous_changes[:plan].present?
        payload[:plan_was] = GitHub::Plan.find(previous_changes[:plan].first).try(:display_name)
        payload[:plan] = plan.try(:display_name)
      end

      if previous_changes[:plan_duration].present?
        payload[:plan_duration_was] = previous_changes[:plan_duration].try(:first)
        payload[:plan_duration] = plan_duration
      end

      if previous_changes[:seats].present?
        payload[:seats_was] = previous_changes[:seats].try(:first)
        payload[:seats] = seats
      end

      instrument :update, payload
    end

    def instrument_run
      payload = {
        active_on: active_on,
        user: user,
      }

      payload[:plan] = plan.display_name if plan
      payload[:plan_duration] = plan_duration if plan_duration
      payload[:seats] = seats if seats

      instrument :run, payload
    end

    def track_changes
      user.track_plan_change(actor, plan_was) if plan_changed
      user.track_seat_change(actor, old_seats: seats_was) if seats_changed
      user.track_subscription_item_changes(actor, previous_plan_duration: plan_duration_was) if plan_duration_changed
      user.track_plan_duration_change(actor, plan_duration_was) if plan_duration_changed
      instrument_run

      if plan_changed
        GlobalInstrumenter.instrument(
          "billing.plan_change",
          actor_id: actor&.id,
          user_id: user&.id,
          old_plan_name: plan_was.name,
          old_seat_count: seats_was,
          new_plan_name: plan.name,
          new_seat_count: seats,
        )
      elsif seats_changed
        GlobalInstrumenter.instrument(
          "billing.seat_count_change",
          actor_id: actor&.id,
          user_id: user&.id,
          old_seat_count: seats_was,
          new_seat_count: seats,
        )
      end

      if plan_trial.present?
        plan_trial.log_plan_change(new_plan: plan.name, user_initiated: false)
      end
    end

    def run_pending_subscription_item_changes
      pending_subscription_item_changes.each &:run
    end

    def log_failure
      Failbot.report runtime_error,
        app: "github-user",
        user: user

      log_data_dog_event(success: false) if attempts == 3
    end

    def log_success
      log_data_dog_event(success: true)
    end

    def log_data_dog_event(success:)
      GitHub.dogstats.increment "billing.pending_plan_change_run",
        tags: ["success:#{success}"]
    end

    def runtime_error
      RuntimeError.new "Invalid Billing::PendingPlanChange state for user: #{error_message}"
    end

    def error_message
      "#{user} - #{user.errors.full_messages.to_sentence}"
    end
  end
end
