# frozen_string_literal: true

module Billing
  class PrepaidMeteredUsageRefillCreator
    def initialize(attributes)
      @attributes = attributes
    end

    def create
      record = Billing::PrepaidMeteredUsageRefill.new(attributes)

      result = record.save
      Billing::InvoicedMeteredBillingConfigurationUpdateJob.perform_later(record.owner) if result

      { success: result, record: record }
    end

    def create!
      record = Billing::PrepaidMeteredUsageRefill.new(attributes)

      record.save!
      Billing::InvoicedMeteredBillingConfigurationUpdateJob.perform_later(record.owner)

      { success: true, record: record }
    end

    private

    attr_reader :attributes
  end
end
