# frozen_string_literal: true

class Billing::Budget < ApplicationRecord::Collab
  include Instrumentation::Model

  self.table_name = "billing_budgets"

  belongs_to :owner, polymorphic: true

  validates :product, uniqueness: { scope: [:owner_type, :owner_id], case_sensitive: true }
  validates :owner, presence: true
  validates :enforce_spending_limit, inclusion: { in: [true, false] }
  validates :spending_limit_in_subunits, numericality: {
    greater_than_or_equal_to: 0,
    only_integer: true,
    message: "Spending limit must be a positive number",
  }

  validate :valid_payment_method_for_overages?, unless: -> { owner.invoiced? }
  after_create_commit :instrument_create
  after_update_commit :instrument_update

  scope :overage_allowed, -> { where.not("enforce_spending_limit = 1 AND spending_limit_in_subunits = 0") }

  enum product: {
    shared: "shared",
    codespaces: "codespaces"
  }

  def self.valid_product?(product)
    [
      :shared,
      :actions,
      :gpr,
      :packages,
      :storage,
      :codespaces,
    ].include?(product.to_sym)
  end

  def self.product_key_for(product:)
    case product.to_sym
    when :actions, :packages, :storage, :gpr
      :shared
    else
      product.to_sym
    end
  end

  def self.configurable?(owner)
    if owner.is_a?(Business)
      enterprise_agreement = owner.enterprise_agreements.active.first
      return false if enterprise_agreement.present? && enterprise_agreement.azure_subscription_id.nil?
    elsif !owner.plan_metered_billing_eligible?
      return false
    end

    !Billing::PrepaidMeteredUsageRefill.enabled_for?(owner)
  end

  # Public: The configured spending limit
  #
  # This provides the spending limit configured by the user, which IS NOT the
  # effective spending limit used for determining if a user can exceed their
  # included usage.
  #
  # Before using this method, consider if you actually want to use
  # `#effective_spending_limit_in_subunits`.
  #
  # Returns Numeric
  def user_configured_spending_limit_in_subunits
    spending_limit_in_subunits
  end

  # Public: The effective spending limit
  #
  # For most accounts, this is the same as the the configured spending limit.
  #
  # However, for accounts with prepaid refills, this will calculate an effective
  # limit based on their previous usage, as these accounts have an effective limit
  # of 150% of their prepaid amount.
  #
  # Returns Numeric
  def effective_spending_limit_in_subunits
    return spending_limit_in_subunits unless Billing::PrepaidMeteredUsageRefill.enabled_for?(owner)

    limit_in_subunits = spending_limit_in_subunits

    if GitHub.flipper[:enforce_metered_billing_refill_check].enabled?(owner)
      return limit_in_subunits if Billing::PrepaidMeteredUsageRefill.total_active_amount_in_cents_for(owner: owner).zero?

      limit_in_subunits -= Billing::RemainingRefillCalculator.new(owner).previous_metered_cycle_costs_in_cents
    end

    [limit_in_subunits, 0].max
  end

  def configure(enforce_spending_limit:, limit: nil)
    overage_allowed_previously = overage_allowed?

    configured = if enforce_spending_limit
      set_spending_limit(limit)
    else
      set_unlimited_spending
    end
    return unless configured

    if owner.is_a?(Business)
      if !overage_allowed_previously && overage_allowed?
        ::Billing::UpdateSkippedMeteredLineItemsJob.perform_later(billable_owner: owner)
      end
    else
      # Does not attempt synchronization if user already has subscription
      ::Billing::PlanSubscription::Transition.activate(owner, force: true)
      set_billed_on
    end
  end

  def overage_allowed?
    !(enforce_spending_limit? && effective_spending_limit_in_subunits.zero?)
  end

  def set_unlimited_spending
    update(enforce_spending_limit: false)
  end

  def set_spending_limit(limit)
    update(
      enforce_spending_limit: true,
      spending_limit_in_subunits: Integer(limit.to_f * 100),
    )
  end

  private

  # Hide database field so that we can have dynamic limits for invoiced customers via
  # `effective_spending_limit_in_subunits`
  def spending_limit_in_subunits
    super
  end

  def valid_payment_method_for_overages?
    return true unless overage_allowed?
    return true if owner.has_valid_payment_method?

    errors.add(:payment_method, "must be present and valid")
    false
  end

  def set_billed_on
    owner.update(billed_on: ::GitHub::Billing.today + 1.month) if owner.billed_on.nil?
  end

  def event_prefix
    # TODO: switch this to `billing_budget` once we've moved over completely
    :metered_billing_configuration
  end

  def event_payload
    payload = {
      enforce_spending_limit: enforce_spending_limit,
      spending_limit_in_subunits: spending_limit_in_subunits,
      spending_limit_currency_code: spending_limit_currency_code,
      spending_limit_description: spending_limit_description,
    }

    if owner.organization?
      payload[:org] = owner
    else
      payload[:user] = owner
    end

    payload
  end

  def instrument_create
    instrument :create
  end

  def instrument_update
    was_enforce_spending_limit, _ = previous_changes[:enforce_spending_limit]
    was_spending_limit_in_subunits, _ = previous_changes[:spending_limit_in_subunits]

    instrument :update, {
      was_spending_limit_description: spending_limit_description(
        enforce_limit: was_enforce_spending_limit,
        subunits: was_spending_limit_in_subunits,
      ),
      was_enforce_spending_limit: was_enforce_spending_limit,
      was_spending_limit_in_subunits: was_spending_limit_in_subunits,
    }
  end

  def spending_limit_description(enforce_limit: nil, subunits: nil)
    enforce_limit = enforce_spending_limit if enforce_limit.nil?
    subunits ||= effective_spending_limit_in_subunits

    if enforce_limit
      Billing::Money.new(subunits).format
    else
      "Unlimited"
    end
  end
end
