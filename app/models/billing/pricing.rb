# frozen_string_literal: true

module Billing
  # Public: Calculates pricing for a subscription, taking into account per-seat
  # pricing, data packs, marketplace subscriptions, and coupons.
  #
  # Examples
  #
  #   pricing = Billing::Pricing.new(account: user_object)
  #   pricing.undiscounted.to_f
  #   # => 9.0
  #   pricing.discount.to_f
  #   # => 0.0
  #   pricing.discounted.to_f
  #   # => 9.0
  class Pricing

    # Internal: Struct to hold a detailed breakdown of annual recurring revenue
    class AnnualRecurringRevenueDetail
      # Public: Returns the Billing::Pricing object used for calculations
      attr_reader :pricing

      # Public: Initializes the current AnnualRecurringRevenueDetail
      #
      # pricing - A Billing::Pricing object
      def initialize(pricing)
        @pricing = pricing
      end

      # Public: Calculates the annual recurring revenue from the user's plan
      # itself
      #
      # Returns the plan annual recurring revenue as a Billing::Money object
      def plan
        pricing.discounted_plan_cost * annual_recurring_revenue_multiplier
      end

      # Public: Calculates the annual recurring revenue from additional seats
      # on the user's plan
      #
      # Returns the seat annual recurring revenue as a Billing::Money object
      def seats
        pricing.discounted_seat_cost * annual_recurring_revenue_multiplier
      end

      # Public: Calculates the annual recurring revenue from data packs that the
      # user has or may purchase
      #
      # Returns the data pack annual recurring revenue as a Billing::Money object
      def data_packs
        pricing.discounted_data_pack_cost * annual_recurring_revenue_multiplier
      end

      # Public: Calculates the annual recurring revenue from marketplace
      # subscription items
      #
      # Returns the marketplace annual recurring revenue as a Billing::Money object
      def marketplace
        Billing::Money.new((
          pricing.marketplace_item_cost.cents *
          annual_recurring_revenue_multiplier *
          BillingTransaction::MARKETPLACE_REVENUE_CUT
        ).floor)
      end

      # Public: Calculates the total annual recurring revenue from GitHub items
      # (i.e. excluding marketplace)
      #
      # Returns the total GitHub annual recurring revenues as a Billing::Money
      # object
      def github_total
        plan + seats + data_packs
      end

      # Public: Calculates the total annual recurring revenue
      #
      # NOTE: sponsorships do not provide any annual recurring
      # revenue for GitHub and should not be added in
      #
      # Returns the total annual recurring revenue as a Billing::Money object
      def total
        github_total + marketplace
      end

      private

      # Internal: Returns a multiplier to use for calculating annual recurring
      # revenue
      #
      # Returns either the integer 1 (yearly plan) or 12 (monthly plan)
      def annual_recurring_revenue_multiplier
        if pricing.monthly?
          12
        else
          1
        end
      end
    end

    # Public: Returns the target User or Organization
    attr_reader :account

    # Public: Initializes the pricing calculation
    #
    # account            - A User or Organization model (optional if
    #                      plan_subscription or plan_duration is provided)
    # plan_subscription  - The user's current PlanSubscription model, if any
    #                      (optional if account or plan_duration is provided)
    # plan               - The GitHub::Plan to use for price calculations if
    #                      different from the user's current plan (optional)
    # plan_duration      - The string plan duration to use for price calculations
    #                      if different from the user's current plan duration
    #                      (optional if account or plan_subscription is provided)
    # seats              - The integer number of total seats to use for price
    #                      calculations if different from the user's current
    #                      number of seats (optional)
    # data_packs         - The integer number of data packs to use for price
    #                      calculations if different from the user's current
    #                      number of data packs (optional)
    # subscription_item  - A new SubscriptionItem or PendingSubscriptionItemChange,
    #                      or an existing item with a different quantity as one
    #                      of the user's existing subscription items (optional;
    #                      can be used with subscription_items)
    # subscription_items - An array of new SubscriptionItems to be added or
    #                      changed to a different quantity and/or
    #                      PendingSubscriptionItemChanges (optional; can be
    #                      with subscription_item)
    # use_trial_prices   - Whether or not to use free trial prices for
    #                      marketplace items that are on or eligible for free
    #                      trials (default true)
    # coupon             - A Coupon to apply in pricing calculations (optional;
    #                      if omitted, the coupon on the current PlanSubscription
    #                      will be used if possible; cannot be used in
    #                      combination with the discount argument)
    # discount           - A raw discount value to be applied as a Numeric; can
    #                      be used in place of a Coupon object (optional; cannot
    #                      be used in combination with the coupon argument)
    # service_remaining  - A Numeric representing how much service is left for
    #                      the purpose of proration (default is 1 or 100% of the
    #                      service)
    #
    # Raises ArgumentError if the required arguments are not provided
    def initialize(account: nil, plan_subscription: nil, plan: nil, seats: nil, plan_duration: nil, data_packs: nil, subscription_item: nil, subscription_items: [], use_trial_prices: true, coupon: nil, discount: nil, service_remaining: 1)
      @account = account
      @plan_subscription = plan_subscription
      @new_plan = effective_plan(plan, account)
      @new_seats = seats
      @new_plan_duration = plan_duration
      @new_data_packs = data_packs
      @new_subscription_items = (subscription_items + [subscription_item]).compact
      @use_trial_prices = use_trial_prices
      @new_coupon = coupon
      @discount = discount
      @service_remaining = service_remaining.to_r

      if account.nil? && plan_subscription.nil? && plan_duration.nil?
        raise ArgumentError, "Billing::Pricing requires :account, :plan_subscription, " \
          "or :plan_duration to be provided"
      end

      if coupon && discount
        raise ArgumentError, "Billing::Pricing expects :coupon or :discount, " \
          "but cannot use both"
      end
    end

    def effective_plan(new_plan, account)
      return new_plan unless account
      plan = if new_plan == account.plan
        GitHub::Plan.find(new_plan, effective_at: account.plan_effective_at)
      else
        new_plan
      end

      MunichPlan.wrap(plan, target: account)
    end

    # Public: Calculates the undiscounted price for the user's subscription
    #
    # Returns the undiscounted price as a Billing::Money object
    def undiscounted
      discountable + undiscountable
    end

    # Public: Calculates the discounted price for the user's subscription
    #
    # Returns the discounted price as a Billing::Money object
    def discounted
      undiscounted - discount
    end

    # Public: Calculates the total discount for a user's subscription
    #
    # Returns the total discount as a Billing::Money object
    def discount
      if @discount
        raw_discount
      else
        coupon_discount(coupon)
      end
    end

    # Public: Calculates the total undiscounted price for items which can
    # be discounted by a coupon
    #
    # Returns the discountable price as a Billing::Money object
    def discountable
      prorated_plan_cost +
        prorated_seat_cost +
        prorated_data_pack_cost
    end

    # Public: Calculates the total undiscounted price for items which cannot
    # be discounted by a coupon
    #
    # Returns the undiscountable price as a Billing::Money object
    def undiscountable
      prorated_marketplace_item_cost + prorated_sponsorable_item_cost
    end

    # Public: Calculates the annual recurring revenue for the user's
    # subscription
    #
    # Returns the annual recurring revenue as a Billing::Money object
    def annual_recurring_revenue
      annual_recurring_revenue_details.total
    end
    alias_method :arr, :annual_recurring_revenue

    # Public: Returns a breakdown of annual recurring revenue for a user's
    # subscription
    #
    # Returns an AnnualRecurringRevenueDetail object
    def annual_recurring_revenue_details
      @annual_recurring_revenue_details ||= AnnualRecurringRevenueDetail.new(self)
    end

    # Public: Calculates the price of the user's plan itself for the
    # applicable subscription duration
    #
    # Returns the plan price as a Billing::Money object
    def plan_cost
      return Billing::Money.zero if !plan || active_plan_trial?

      if monthly?
        Billing::Money.new(plan.cost_in_cents)
      else
        Billing::Money.new(plan.yearly_cost_in_cents)
      end
    end

    # Public: Calculates the price of the user's plan itself for the
    # applicable subscription duration, less any discounts
    #
    # Returns the discounted plan price as a Billing::Money object
    def discounted_plan_cost
      plan_cost - plan_discount
    end

    # Public: Calculates the price of the user's plan itself for the
    # applicable subscription duration, prorated to the amount of
    # service remaining
    #
    # Returns the prorated plan price as a Billing::Money object
    def prorated_plan_cost
      Billing::Money.new(prorate(plan_cost.cents))
    end

    # Public: Calculates the amount of discount applied to the user's plan
    #
    # Returns the plan discount as a Billing::Money object
    def plan_discount
      return Billing::Money.zero if discountable.zero?
      Billing::Money.new((discount.cents * plan_cost.to_f / discountable.to_f).round)
    end

    # Public: Calculates the price of additional seats on the user's
    # plan (seats beyond the base number of seats included in the plan)
    #
    # Returns the seat price as a Billing::Money object
    def seat_cost
      return Billing::Money.zero if !plan&.per_seat? || active_plan_trial?

      additional_seats = seats - plan.base_units
      additional_seats = 0 if additional_seats.negative?

      additional_seats * if monthly?
        Billing::Money.new(plan.unit_cost_in_cents)
      else
        Billing::Money.new(plan.yearly_unit_cost_in_cents)
      end
    end

    # Public: Estimates the price of shared storage the user will pay based on
    # current usage and time remaining in billing cycle
    #
    # Returns the shared storage price as a Billing::Money object
    def estimated_shared_storage_cost
      return Billing::Money.zero if !plan.shared_storage_eligible? || !account.metered_billing_overage_allowed?(product: :storage)

      shared_storage_cost_in_cents = BigDecimal(Billing::SharedStorage::ZuoraProduct::UNIT_COST) * 100 / (1.gigabyte / 1.megabyte)
      estimated_shared_storage_cost_cents = shared_storage_usage.estimated_monthly_paid_megabytes * shared_storage_cost_in_cents

      Billing::Money.new(estimated_shared_storage_cost_cents)
    end

    # Public: Calculates the price of additional seats on the user's
    # plan (seats beyond the base number of seats included in the plan), less
    # any discounts
    #
    # Returns the discounted seat price as a Billing::Money object
    def discounted_seat_cost
      seat_cost - seat_discount
    end

    # Public: Calculates the price of additional seats on the user's
    # plan (seats beyond the base number of seats included in the plan),
    # prorated to the amount of service remaining
    #
    # Returns the prorated seat price as a Billing::Money object
    def prorated_seat_cost
      Billing::Money.new(prorate(seat_cost.cents))
    end

    # Public: Calculates the amount of discount applied to additional seats on
    # the user's plan
    #
    # Returns the seat discount as a Billing::Money object
    def seat_discount
      return Billing::Money.zero if discountable.zero?
      Billing::Money.new((discount.cents * seat_cost.to_f / discountable.to_f).round)
    end

    # Public: Calculates the price of data packs that the user has or may
    # purchase
    #
    # Returns the data pack price as a Billing::Money object
    def data_pack_cost
      return Billing::Money.zero unless data_packs

      Billing::Money.new(
        data_packs *
        Asset::Status.data_pack_unit_price.cents *
        plan_duration_multiplier,
      )
    end

    # Public: Calculates the price of data packs that the user has or may
    # purchase, less any discounts
    #
    # Returns the discounted data pack price as a Billing::Money object
    def discounted_data_pack_cost
      data_pack_cost - data_pack_discount
    end

    # Public: Calculates the price of data packs that the user has or may
    # purchase, prorated to the amount of service remaining
    #
    # Returns the prorated data pack price as a Billing::Money object
    def prorated_data_pack_cost
      Billing::Money.new(prorate(data_pack_cost.cents))
    end

    # Public: Calculates the amount of discount applied to data packs that the
    # user has purchased
    #
    # Returns the data pack discount as a Billing::Money object
    def data_pack_discount
      return Billing::Money.zero if discountable.zero?
      Billing::Money.new((discount.cents * data_pack_cost.to_f / discountable.to_f).round)
    end

    # Public: Calculates the price of marketplace subscription items that
    # the user has purchased
    #
    # Returns the marketplace subscription item price as a Billing::Money object
    def marketplace_item_cost
      return Billing::Money.zero unless marketplace_items

      subscription_item_cost(marketplace_items)
    end

    # Public: Calculates the price of sponsorship subscription items that
    # the user has purchased
    #
    # Returns the sponsorship subscription item price as a Billing::Money object
    def sponsorable_item_cost
      return Billing::Money.zero unless sponsorable_items

      subscription_item_cost(sponsorable_items)
    end

    # Public: Gets the non-sponsorable subscription items
    #
    # Returns the subscription items that are not of listable types reflecting sponsorships
    def marketplace_items
      @marketplace_items ||= subscription_items.select { |item| item.async_listing.sync&.is_marketplace? }
    end

    # Public: Gets the non-sponsorable subscription items
    #
    # Returns the subscription items that are not of listable types reflecting sponsorships
    def sponsorable_items
      @sponsorable_items ||= subscription_items.select { |item| item.async_listing.sync&.listable_is_sponsorable? }
    end

    # Public: Calculates the price of marketplace subscription items that
    # the user has purchased, prorated to the amount of service remaining
    #
    # Returns the prorated marketplace item price as a Billing::Money object
    def prorated_marketplace_item_cost
      Billing::Money.new(prorate(marketplace_item_cost.cents))
    end

    # Public: Calculates the price of sponsorable subscription items that
    # the user has purchased, prorated to the amount of service remaining
    #
    # Returns the prorated sponsorable item price as a Billing::Money object
    def prorated_sponsorable_item_cost
      Billing::Money.new(prorate(sponsorable_item_cost.cents))
    end

    # Public: Calculates the price of package downloads the user has incurred
    #
    # Returns Billing::Money
    def package_downloads_cost
      return Billing::Money.zero if !plan.package_registry_eligible? || !account.metered_billing_overage_allowed?(product: :packages)

      unit_cost = BigDecimal(::Billing::PackageRegistry::ZuoraProduct::UNIT_COST)
      Billing::Money.new(data_transfer_usage.billable_gigabytes * unit_cost * 100)
    end

    # Public: Calculates the price of actions the user has used
    #
    # Returns the actions price as a Billing::Money object
    def actions_cost
      return Billing::Money.zero if !plan.actions_eligible? || !account.metered_billing_overage_allowed?(product: :actions)

      actions_cost_cents = actions_usage.total_paid_minutes_used * plan.actions_overage_unit_cost_cents
      Billing::Money.new(actions_cost_cents)
    end

    # Public: Whether or not this pricing calculation is for a monthly plan
    #
    # Returns a boolean
    def monthly?
      plan_duration.to_s == User::MONTHLY_PLAN
    end

    # Public: Allows prices to be prorated to a specific percentage without
    # initializing a new Pricing object
    #
    # service_remaining - A Numeric representing how much service is left for
    #                     the purpose of proration
    # block             - Block in which all pricing will be prorated to the
    #                     amount of service_remaining
    #
    # Examples
    #
    #   pricing.discounted.to_f
    #   # => 7.0
    #
    #   pricing.prorate_to(0.5) { pricing.discounted }.to_f
    #   # => 3.5
    #
    # Returns the result of the block
    def prorate_to(service_remaining)
      old_service_remaining = @service_remaining
      @service_remaining = service_remaining.to_r

      result = yield self

      @service_remaining = old_service_remaining
      result
    end

    # Public: Allows prices to be calculated on a monthly basis without
    # initializing a new Pricing object
    #
    # block - Block in which all pricing will be calculated on a monthly basis
    #
    # Examples
    #
    #   pricing = Pricing.new(plan: GitHub::Plan.pro, plan_duration: "year")
    #
    #   pricing.discounted.to_f
    #   # => 84.0
    #
    #   pricing.monthly { pricing.discounted }.to_f
    #   # => 7.0
    #
    # Returns the result of the block
    def monthly(&block)
      change_duration(User::MONTHLY_PLAN, &block)
    end

    # Public: Allows prices to be calculated on a yearly basis without
    # initializing a new Pricing object
    #
    # block - Block in which all pricing will be calculated on a yearly basis
    #
    # Examples
    #
    #   pricing = Pricing.new(plan: GitHub::Plan.pro, plan_duration: "month")
    #
    #   pricing.discounted.to_f
    #   # => 7.0
    #
    #   pricing.yearly { pricing.discounted }.to_f
    #   # => 84.0
    #
    # Returns the result of the block
    def yearly(&block)
      change_duration(User::YEARLY_PLAN, &block)
    end

    private

    def data_transfer_usage
      @data_transfer_usage ||= ::Billing::PackageRegistryUsage.new(account)
    end

    def actions_usage
      @actions_usage ||= ::Billing::ActionsUsage.new(account)
    end

    def shared_storage_usage
      @shared_storage_usage ||= Billing::SharedStorageUsage.new(account)
    end

    # Internal: Sums the price of each passed subscription item
    #
    # items - an array of ::Billing::SubscriptionItem
    #
    # Returns ::Billing::Money
    def subscription_item_cost(items)
      items.reduce(Billing::Money.zero) do |memo, item|
        memo + item.price(duration: plan_duration, trial_price: use_trial_prices?)
      end
    end

    # Internal: Prorate a given amount for the amount of service remaining
    #
    # Truncates fractional cents, similar to Braintree and Zuora
    #
    # amount - Numeric amount for full service period
    #
    # Returns a integer prorated amount
    def prorate(amount)
      (amount * @service_remaining).to_i
    end

    # Internal: The coupon that the user has currently redeemed or the
    # hypothetical coupon to use for pricing calculations
    #
    # Returns a Coupon object
    def coupon
      plan_subscription&.async_user&.sync
      @new_coupon || plan_subscription&.coupon || account&.coupon
    end

    # Internal: The discount from a hypothetical coupon to use for pricing
    # calculations
    #
    # potential_coupon        - The Coupon to use for pricing calculations; this
    #                           can be any Coupon object and needs not be
    #                           persisted or redeemed by the account
    # check_coupon_expiration - Whether or not to check the coupon's expiration
    #                           before applying the discount (default is true)
    #
    # Returns the coupon discount as a Billing::Money object
    def coupon_discount(potential_coupon, check_coupon_expiration: true)
      return Billing::Money.zero unless potential_coupon
      return Billing::Money.zero if coupon&.plan_specific? && coupon&.plan != plan
      return Billing::Money.zero if account&.will_be_expired? && check_coupon_expiration

      if potential_coupon.percentage?
        Billing::Money.new(potential_coupon.discount * discountable.cents)
      else
        Billing::Money.new([potential_coupon.discount_in_cents * plan_duration_multiplier, discountable.cents].min)
      end
    end

    # Internal: The discount from a raw discount value (rather than a coupon)
    # to use for pricing calculations
    #
    # Returns the discount as a Billing::Money object
    def raw_discount
      hypothetical_coupon = Coupon.new(discount: @discount)
      coupon_discount(hypothetical_coupon, check_coupon_expiration: false)
    end

    # Internal: The current or hypothetical plan to use for pricing calculations
    #
    # Returns a GitHub::Plan
    def plan
      MunichPlan.wrap(@new_plan || plan_subscription&.plan || account&.plan, target: account)
    end

    # Internal: The current or hypothetical plan duration to use for pricing
    # calculations
    #
    # Returns either the string 'month' or 'year'
    def plan_duration
      @new_plan_duration || plan_subscription&.plan_duration || account&.plan_duration
    end

    # Internal: The current or hypothetical plan subscription to use for
    # pricing calculations
    #
    # Returns a Billing::PlanSubscription object
    def plan_subscription
      @plan_subscription || account&.plan_subscription
    end

    # Internal: Returns a multiplier to use for calculations where we need to
    # compute an annual cost ourselves
    #
    # Returns either the integer 1 (monthly plan) or 12 (yearly plan)
    def plan_duration_multiplier
      if monthly?
        1
      else
        12
      end
    end

    # Internal: The current or hypothetical total number of seats (inclusive of
    # and seats included in a plan) to use for pricing calculations
    #
    # Returns an integer number of seats
    def seats
      @new_seats || plan_subscription&.seats || account&.seats || 0
    end

    # Internal: The current or hypothetical number of data packs to use for
    # pricing calculations
    #
    # Returns an integer number of data packs
    def data_packs
      @new_data_packs || plan_subscription&.data_packs || account&.data_packs
    end

    # Internal: The current or hypothetical marketplace subscription items to
    # use for pricing calculations
    #
    # When @new_subscription_items has the same Marketplace::Listing as an
    # existing subscription item, it is assumed that we are changing the
    # listing plan or quantity of that item, and so pricing calculations will
    # be done on the new listing plan and quantity.
    #
    # Returns an Array of Billing::SubscriptionItems
    def subscription_items
      subscription_items = plan_subscription&.subscription_items.to_a

      if @new_subscription_items.any?
        # @new_subscription_items may have a different quantity than an existing
        # subscription for the same marketplace listing plan
        subscription_items.reject! do |existing_item|
          @new_subscription_items.any? do |new_item|
            new_item.async_listing.sync == existing_item.async_listing.sync
          end
        end
        subscription_items += @new_subscription_items
      end

      subscription_items
    end

    # Internal: Whether or not to use free trial prices for marketplace items
    # that are on or eligible for free trials
    #
    # Returns a boolean
    def use_trial_prices?
      @use_trial_prices
    end

    # Internal: Allows prices to be calculated with a specific plan duration
    # without initializing a new Pricing object
    #
    # override_duration - The String plan duration to use for pricing
    #                     calculations ("month" or "year")
    # block             - Block in which all pricing will be calculated using
    #                     the specified plan duration
    #
    # Returns the result of the block
    def change_duration(override_duration, &block)
      if plan_duration == override_duration
        result = block.call(self)
      else
        old_duration = plan_duration
        @new_plan_duration = override_duration

        result = block.call(self)

        @new_plan_duration = old_duration
      end
      result
    end

    def active_plan_trial?
      return @_active_plan_trial if defined?(@_active_plan_trial)
      @_active_plan_trial = Billing::PlanTrial.active_for?(account, plan.name)
    end
  end
end
