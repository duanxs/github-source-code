# frozen_string_literal: true

module Billing
  class ActionsUsageLineItem < ApplicationRecord::Collab
    include ::Billing::MeteredBillingLineItemDependency

    belongs_to :check_run, optional: true
    belongs_to :workflow, class_name: "Actions::Workflow", optional: true

    validates :job_id, presence: true
    validates :job_runtime_environment, presence: true
    validates :duration_multiplier, presence: true
    validates :duration_in_milliseconds, presence: true
    delegate :account_number, :subscription_number, :charge_number,
      to: :zuora_usage_attributes, prefix: :zuora

    UNIT_OF_MEASURE = "Minutes"

    def self.product_name
      "actions"
    end

    # Public: The billable rate multiplier for the runtime the job was executed on.
    #
    # Returns Double
    def self.billable_rate_for(runtime:)
      case runtime
      when :MACOS
        10.0
      when :WINDOWS
        2.0
      when :UBUNTU
        1.0
      else
        1.0
      end
    end

    def self.usage_line_item_by_repository_and_runtime_environment(owner_id)
      owner = User.find_by(id: owner_id)
      return {} unless owner

      where(owner_id: owner_id)
        .where("end_time >= ?", owner.first_day_in_metered_cycle)
        .group(["repository_id", "job_runtime_environment"])
        .order("job_runtime_environment")
        .sum("CAST(CEIL(duration_in_milliseconds / 60000.0) * duration_multiplier AS SIGNED)")
    end

    # Public: The unit of measure for billing
    #
    # Returns String literal "Minutes"
    def unit_of_measure
      UNIT_OF_MEASURE
    end

    # Public: The billable quantity given in minutes rounded up to the nearest minute
    #
    # Returns BigDecimal
    def billable_quantity
      duration_in_minutes.ceil * duration_multiplier
    end

    def billable_milliseconds_without_multiplier
      duration_in_minutes.ceil * 60_000
    end

    def duration_in_minutes
      duration_in_milliseconds / 60_000.0
    end

    def duration_in_seconds
      duration_in_milliseconds / 1_000.0
    end

    # Public: Is this usage for a private repository
    #
    # For Actions, we only record line item records for private repositories,
    # so this is always true
    #
    # Returns Boolean
    def private_visibility?
      true
    end

    # Public: When the usage occurred
    #
    # Returns DateTime
    def usage_at
      end_time
    end

    # Public: Publish a MeteredLineItemUpdated Hydro message for this record
    #
    # This method uses Hydro::PublishRetrier since thousands of these messages
    # can be published in a short timeframe, possibly overflowing the Hydro
    # message buffer and dropping messages.
    #
    # Returns nothing
    def publish_metered_line_item_updated_message
      message = {
        metered_product: :ACTIONS,
        line_item_id: id,
      }
      Hydro::PublishRetrier.publish(message, schema: "github.billing.v0.MeteredLineItemUpdated")
    end

    private

    def zuora_usage_attributes
      @zuora_usage_attributes ||= Billing::ZuoraUsageAttributes.new(
        billable_owner: billable_owner,
        usage_type: :actions,
        usage_at: end_time,
      )
    end
  end
end
