# frozen_string_literal: true

class Billing::Zuora::UsageFile::LineItemSerializer
  CSV_HEADERS = %i[
    ACCOUNT_ID
    UOM
    QTY
    STARTDATE
    ENDDATE
    SUBSCRIPTION_ID
    CHARGE_ID
    DESCRIPTION
    GITHUB_PRODUCT
    GITHUB_OWNER_ID
    GITHUB_ITEM_ID
    UUID
  ].freeze

  # Public: Serialize a metered line item for a given product
  #
  # line_item - A serializable metered line item
  # product   - The name of the metered product
  #
  # Returns Hash
  def self.serialize(**kwargs)
    new(**kwargs).serialize
  end

  # Public: Initialize the Serializer
  #
  # line_item - A serializable metered line item
  # product   - The name of the metered product
  def initialize(line_item:, product:)
    @line_item = line_item
    @product = product
  end

  # Public: Serialize the metered line item as an Array
  #
  # Returns Array
  def to_a
    [
      line_item.zuora_account_number,
      line_item.unit_of_measure,
      format("%.9f", line_item.billable_quantity),
      line_item.usage_at.to_s(:zuora_usage_file),
      line_item.usage_at.to_s(:zuora_usage_file),
      line_item.zuora_subscription_number,
      line_item.zuora_charge_number,
      nil,
      product,
      line_item.owner_id.to_s,
      line_item.id.to_s,
      SecureRandom.uuid,
    ]
  end

  # Public: Serialize the metered line item as a Hash
  #
  # Returns Hash
  def to_h
    CSV_HEADERS.zip(to_a).to_h
  end
  alias_method :serialize, :to_h

  # Public: Serialize the metered line item as a CSV line
  #
  # Returns String
  def to_csv
    CSV.generate_line(to_a)
  end

  private

  attr_reader :line_item, :product
end
