# frozen_string_literal: true

class Billing::Zuora::Subscription::RatePlanCharge

  def self.class_for_charge_type
    @class_for_charge_type ||= {
      "OneTime" => Billing::Zuora::Subscription::OneTimeCharge
    }
  end

  def self.for(rate_plan_charge)
    class_for_charge_type.fetch(rate_plan_charge[:type], self).new(rate_plan_charge)
  end

  def initialize(zuora_subscription_rate_plan_charge)
    @rate_plan_charge = zuora_subscription_rate_plan_charge
  end

  def [](key)
    rate_plan_charge[key]
  end

  def active?
    effective_end_date.blank? ||
      Date.parse(effective_end_date) > GitHub::Billing.today
  end

  def product_rate_plan_charge_id
    rate_plan_charge[:productRatePlanChargeId]
  end

  def number
    rate_plan_charge[:number]
  end

  def billing_period
    rate_plan_charge[:billingPeriod]
  end

  def annual?
    "Annual" == billing_period
  end

  def charged_through_date
    rate_plan_charge[:chargedThroughDate]
  end

  def usage?
    "Usage" == rate_plan_charge[:type]
  end

  def effective_start_date
    rate_plan_charge[:effectiveStartDate]
  end

  def effective_end_date
    rate_plan_charge[:effectiveEndDate]
  end

  def price
    rate_plan_charge[:price]
  end

  private

  attr_reader :rate_plan_charge
end
