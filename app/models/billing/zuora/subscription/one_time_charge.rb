# frozen_string_literal: true

class Billing::Zuora::Subscription::OneTimeCharge < Billing::Zuora::Subscription::RatePlanCharge
  def active?
    effective_start_date != effective_end_date
  end
end
