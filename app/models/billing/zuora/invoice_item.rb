# frozen_string_literal: true

class Billing::Zuora::InvoiceItem
  def initialize(raw_zuora_response)
    @invoice_item_response = raw_zuora_response
  end

  def [](attr)
    invoice_item_response[attr]
  end

  def id
    invoice_item_response["Id"]
  end

  def charge_name
    invoice_item_response["ChargeName"]
  end

  def unit
    invoice_item_response["UOM"]
  end

  def quantity
    invoice_item_response["Quantity"]
  end

  # The charge amount for the invoice items
  #
  # Returns Billing::Money
  def charge_amount
    ::Billing::Money.new(invoice_item_response["ChargeAmount"] * 100)
  end

  private

  attr_reader :invoice_item_response
end
