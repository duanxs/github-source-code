# frozen_string_literal: true

class Billing::Zuora::Object::Account
  def self.find(zuora_account_id)
    return unless zuora_account_id.present?

    response = GitHub.zuorest_client.get_account(zuora_account_id)

    new(response)
  rescue Zuorest::HttpError => e
    return if e.message == "HTTP 404"

    raise e
  end

  def initialize(raw_zuora_response)
    @account_response = raw_zuora_response
  end

  def bill_cycle_day
    account_response["BillCycleDay"]
  end

  private

  attr_reader :account_response
end
