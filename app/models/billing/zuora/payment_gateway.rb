# frozen_string_literal: true

class Billing::Zuora::PaymentGateway
  # List of Gateways created in Zuora
  # - Stripe v2 (to be used for 3DS support)
  # - Paypal
  # The names of the gateways correspond to the assigned names in Zuora's settings
  STRIPE_V2 = "Stripe v2"
  PAYPAL = "Paypal"

  # Public: Returns the name of the gateway to use for the given user and type of payment
  #
  # user - User to check gateway eligibility
  # type - Symbol either `credit_card` or `paypal` used to determine gateway
  #
  # Returns String
  def self.for(user, type:)
    new(user, type: type).name
  end

  def initialize(user, type: nil)
    validate_type(type)

    @user = user
    @type = type
  end

  # Public: Validates and assigns the type for the gateway determination
  #
  # type - Symbol either `credit_card` or `paypal` used to determine gateway
  def type=(type)
    validate_type(type)

    @type = type
  end

  # Public: Returns the name of the gateway to use for the given user and type of payment
  #
  # Returns String
  def name
    raise RuntimeError, "type needs to be present to resolve gateway name" unless type.present?

    if credit_card?
      STRIPE_V2
    else
      PAYPAL
    end
  end

  def paypal?
    name == PAYPAL
  end

  def credit_card?
    type == :credit_card
  end

  private

  attr_reader :type, :user

  def validate_type(type)
    raise ArgumentError, "Invalid gateway type #{type}" if type && !type.in?([:credit_card, :paypal])
  end
end
