# rubocop:disable Style/FrozenStringLiteralComment

module Billing
  class UpdatePaymentMethod

    attr_accessor :response

    # Public: Updates payment method details of an existing customer.
    # This also tries to charge the new card (or paypal account) if the user
    # account is disabled or its billed_on is in the past.
    #
    # target          - A User object who's credit card data we're updating.
    # payment_details - A Hash of payment details:
    #                   :zuora_payment_method_id - String Zuora Hosted Payment Pages payment method ID
    #                   :charge        - Boolean whether to attempt a recurring charge with this update. Default: true
    #                   :billing_extra - String extra billing information.
    #                   :vat_code      - String VAT identification number
    #                   :paypal_nonce  - String nonce representing a paypal account (optional).
    #                   :credit_card   - A Hash of potentially encrypted CC info, including:
    #                     * :number           - CC number as a String.
    #                     * :expiration_month - Expiration month as a String of form MM
    #                     * :expiration_year  - Expiration year as a String of form YY
    #                     * :cvv              - CVV as a String (e.g. "420")
    #                   :billing_address  - The billing address as a Hash of optionally encrypted CC info
    #                     * :country_code_alpha3 - Country as a String
    #                     * :region              - Region as a String
    #                     * :postal_code         - Postal code as a String
    #
    # Returns a GitHub::Billing::Result object, indicating
    # success or failure, the user that was updated and an error message if the
    # customer's credit card could not be updated.
    def self.perform(*args)
      new(*args).perform
    end

    def initialize(target, payment_details)
      unless target.try(:customer).try(:payment_method).present?
        raise Error, "No payment processor record to update."
      end

      @target  = target
      @payment_details = payment_details.reverse_merge!(charge: true)
    end

    def perform
      Failbot.push(user: @target, areas_of_responsibility: [:gitcoin])

      @target.check_for_spam
      if @target.spammy?
        @response = GitHub::Billing::Result.failure("This account has been flagged. #{GitHub.support_link_text} for further information.")
      else
        @response = @target.customer.update_payment_method_details(@payment_details)

        if @response.success?
          if @payment_details.has_key?(:billing_extra)
            @target.update_attribute(:billing_extra, @payment_details[:billing_extra])
          end

          if @payment_details.has_key?(:vat_code)
            @target.customer.update_attribute(:vat_code, @payment_details[:vat_code])
          end

          # NB - Past due Braintree subscriptions are charged automatically when
          # the payment method token is updated.
          if @payment_details[:charge] && !@target.braintree_subscription?
            BillingChargeJob.perform_later(@target)
          end

          if @target.disabled? && !@target.should_disable? && @payment_details[:unlock_billing]
            @target.unlock_billing!
          end

          if @target.external_subscription?
            begin
              @target.update_external_subscription!(force: true)
            rescue Braintree::ValidationsFailed => error
              Failbot.report(error)
            end
          end
        end
      end

      self
    end
  end
end
