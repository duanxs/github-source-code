# frozen_string_literal: true

module Billing
  class BraintreeVoid
    def self.process(*args); new(*args).process; end

    attr_reader :transaction

    delegate :transaction_id, to: :transaction

    def initialize(transaction)
      @transaction = transaction
    end

    def process
      response = GitHub.dogstats.time("braintree.timing.transaction_void") do
        ::Braintree::Transaction.void(transaction_id)
      end

      if response.success?
        transaction.update(transaction: response.transaction)
        GitHub::Billing::Result.success(transaction)
      else
        error_message = response.errors.to_a.map { |e| e.message }.join(", ")
        GitHub::Billing::Result.failure(error_message)
      end
    rescue Braintree::NotFoundError
      GitHub::Billing::Result.failure("Transaction not found: #{transaction_id}")
    end
  end
end
