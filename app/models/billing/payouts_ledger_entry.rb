# frozen_string_literal: true

module Billing
  # Represents a single entry in the Payouts Ledger
  #
  # WARNING:
  # Avoid using this class directly for writing to the ledger. Instead, use the
  # Billing::PayoutsLedgerTransaction class to ensure data consistency.
  #
  # Example:
  #
  #   transaction = Billing::PayoutsLedgerTransaction.new(stripe_account)
  #   transaction.add_entry(
  #     transaction_type: :payment,
  #     amount_in_subunits: -10_00,
  #     currency_code: "USD",
  #   )
  #   transaction.add_entry(
  #     transaction_type: :transfer,
  #     amount_in_subunits: 10_00,
  #     currency_code: "USD",
  #   )
  #   transaction.save!
  #
  # WARNING:
  # Avoid unscoped queries of the ledger. All queries should be scoped by
  # Stripe Connect account.
  #
  # Example:
  #
  #   stripe_account = Billing::StripeConnect::Account.find(id)
  #   stripe_account.ledger_entries.where(*criteria)
  class PayoutsLedgerEntry < ApplicationRecord::Collab
    areas_of_responsibility :gitcoin

    self.table_name = "billing_payouts_ledger_entries"

    belongs_to :stripe_connect_account, class_name: "Billing::StripeConnect::Account"

    # Transaction types SHOULD correspond to the Stripe webhook kinds in the
    # `Billing::StripeWebhook` model to avoid confusion.
    #
    # 10..19 - Cash-flow transactions
    #            These are any transactions where GitHub collects funds from or
    #            returns funds to a cardholder
    # 20..29 - Adjustments to transfers
    #            These are adjustments made prior to transferring funds to a
    #            maintainer, such as matching and fee recovery
    # 30..39 - Transfers
    #            These are the transfer of funds from GitHub's balance to a
    #            maintainer's balance in Stripe
    # 40..49 - Payouts
    #            These are the disbursement of funds from a maintainer's
    #            Stripe balance to their external account
    enum transaction_type: {
      # Cash-flow transactions
      payment: 10,
      refund: 11,
      chargeback: 12,
      chargeback_reversal: 13,
      # Adjustments to transfers
      github_match: 20,
      github_match_reversal: 21,
      # Transfers
      transfer: 30,
      transfer_reversal: 31,
      transfers_paid: 32,
      # Payouts
      payout: 40,
      payout_failure: 41,
    }

    store :additional_reference_ids, coder: YAML

    scope :github_net_match_transactions_for, ->(stripe_connect_account_id) do
      where(
        transaction_type: [:github_match, :github_match_reversal],
        stripe_connect_account_id: stripe_connect_account_id,
      )
    end

  end
end
