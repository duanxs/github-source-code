# frozen_string_literal: true

module Billing
  module Codespaces
    class StorageUsageBillableQuery
      SECONDS_IN_AN_HOUR = 3600

      # Public: Create an instance of this class, used to calculate gigabyte hours
      # for a given billable owner over a given time frame
      # from the stored size_in_bytes and duration_in_seconds
      #
      # billable_owner - The User, Organization or Business paying for recorded usage
      # after - The earliest Date we want line items from
      # before - A Date we want all line items from before
      # owner - The User or Organization responsible for the usage
      #
      # Returns an instance of ::Billing::Codespaces::StorageUsageBillableQuery
      def initialize(billable_owner:, after:, before: nil, owner: nil)
        @root_scope = ::Billing::Codespaces::StorageUsageLineItem
          .where(billable_owner: billable_owner)
          .where("end_time >= ?", after)

        if owner.present?
          @root_scope = @root_scope.where(owner: owner)
        end

        if before.present?
          @root_scope = @root_scope.where("end_time < ?", before)
        end
      end

      # Public: Returns the gigabyte hours stored for the parameters given
      # during initialization
      #
      # Returns Integer
      def gigabyte_hours
        root_scope.sum(:computed_usage)
      end

      private

      attr_reader :root_scope
    end
  end
end
