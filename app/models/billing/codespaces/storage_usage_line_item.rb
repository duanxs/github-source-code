# frozen_string_literal: true

module Billing::Codespaces
  class StorageUsageLineItem < ApplicationRecord::Collab
    include ::Billing::MeteredBillingLineItemDependency

    self.table_name = "codespaces_storage_usage_line_items"

    validates :unique_billing_identifier, presence: true
    validates :duration_in_seconds, presence: true
    delegate :account_number, :subscription_number, :charge_number,
      to: :zuora_usage_attributes, prefix: :zuora

    UNIT_OF_MEASURE = "Gigabyte Hours"
    SECONDS_IN_AN_HOUR = 3600

    def self.product_name
      "codespaces"
    end

    # Public: Publish a MeteredLineItemUpdated Hydro message for this record
    #
    # This method uses Hydro::PublishRetrier since thousands of these messages
    # can be published in a short timeframe, possibly overflowing the Hydro
    # message buffer and dropping messages.
    #
    # Returns nothing
    def publish_metered_line_item_updated_message
      message = {
        metered_product: :CODESPACES_STORAGE,
        line_item_id: id,
      }
      Hydro::PublishRetrier.publish(message, schema: "github.billing.v0.MeteredLineItemUpdated")
    end

    # Public: The unit of measure for billing
    #
    # Returns String literal "Gigabyte Hours"
    def unit_of_measure
      UNIT_OF_MEASURE
    end

    # Public: The billable quantity in Gigabyte Hours
    #
    # Returns Numeric
    def billable_quantity
      computed_usage
    end

    # Public: When the usage occurred
    #
    # Returns DateTime
    def usage_at
      end_time
    end

    # Public: Should this usage be billed due to repository visibility?
    #
    # For Codespaces storage usage we're not responsible for this business logic and may also bill
    # for public usage.  Always returns true as this method is required in
    # Billing::Zuora::UsageFile::LineItemSubmissionPolicy
    #
    # Returns Boolean
    def private_visibility?
      true
    end

    private

    def zuora_usage_attributes
      @zuora_usage_attributes ||= Billing::ZuoraUsageAttributes.new(
        billable_owner: billable_owner,
        usage_type: :codespaces_storage,
        usage_at: end_time,
      )
    end
  end
end
