# frozen_string_literal: true

module Billing
  module Codespaces
    class ComputeUsageBillableQuery

      # Public: Create an instance of this class, used to calculate the
      # billable minutes for the given billable owner over a given time
      # frame from the duration_in_seconds and duration_multiplier
      #
      # billable_owner - The User, Organization, or Business paying for recorded usage
      # after - The earliest Date we want line items from
      # before - A date we want all line items from before
      # owner - The User or Organization responsible for the usage
      #
      # Returns an instance of ::Billing::Codespaces::ComputeUsageBillableQuery
      def initialize(billable_owner:, after:, before: nil, owner: nil)
        @root_scope = Billing::Codespaces::ComputeUsageLineItem
          .where(billable_owner: billable_owner)
          .where("end_time >= ?", after)

        @billable_owner = billable_owner
        @owner = owner

        if owner.present?
          @root_scope = @root_scope.where(owner: owner)
        end

        if before.present?
          @root_scope = @root_scope.where("end_time < ?", before)
        end
      end

      # Public: Returns the hours of compute usage executed for the billable owner,
      # both broken down by sku and as a total.
      #
      # Example response:
      # { "SKU1"=>20, "SKU2"=>1, "TOTAL"=>21 }
      #
      # Returns Hash
      def hours_of_usage
        sum_of_hours_used_for(root_scope)
      end

      private

      attr_reader :root_scope

      def sum_of_hours_used_for(relation)
        relation
          .group(:sku)
          .sum(:computed_usage)
          .tap { |usage| usage["TOTAL"] = usage.values.sum }
      end
    end
  end
end
