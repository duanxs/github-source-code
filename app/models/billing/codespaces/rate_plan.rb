# frozen_string_literal: true

# Represents the rate plan for Codespaces for interaction with Zuora subscriptions
class Billing::Codespaces::RatePlan
  def initialize(user:)
    @user = user
    @product_uuid = ::Billing::Codespaces::ZuoraProduct.uuid
    @calculator = ::Billing::MeteredBilling::HourlyRateCalculator.new
  end

  # Public: The rate plan charge ID for Codespaces Storage rate plan charge
  #
  # Returns String
  def storage_product_rate_plan_charge_id
    product_uuid.zuora_product_rate_plan_charge_ids[:storage]
  end

  # Public: The rate plan charge ID for Codespaces Compute usage rate plan charge
  #
  # Returns String
  def compute_product_rate_plan_charge_id
    product_uuid.zuora_product_rate_plan_charge_ids[:compute]
  end

  # Public: The included Compute rate plan charge units
  # For organizations, because the model around included units is around
  # individual users having their own separate included units, we can't rely on Zuora's included units
  # So we'll set the included units to 0 and manually calculate them so that we only submit overages to Zuora
  # The unit of measure is seconds
  #
  # Returns Integer
  def included_compute_rate_plan_charge_units
    if user.organization?
      0
    else
      user.plan.codespaces_included_compute_minutes / 60
    end
  end

  # Public: The included Storage rate plan charge units
  # For organizations, because the model around included units is around
  # individual users having their own separate included units, we can't rely on Zuora's included units
  # So we'll set the included units to 0 and manually calculate them so that we only submit overages to Zuora
  # The unit of measure in Zuora is Megabyte hours
  #
  # Returns Integer
  def included_storage_rate_plan_charge_units
    if user.organization?
      0
    else
      calculator.hourly_rate_for(units_per_month: user.plan.codespaces_included_storage_gigabytes)
    end
  end

  # Public: Hash of arguments needed for ZuoraSubscriptionParams
  # This method should only be used by ZuoraSubscriptionParams as the return may change
  #
  # Returns Hash
  def to_zuora_subscription_params
    {
      productRatePlanId: product_uuid.zuora_product_rate_plan_id,
      chargeOverrides: [
        codespaces_compute_usage_overrides,
        codespaces_storage_usage_overrides,
      ]
    }
  end

  private

  attr_reader :product_uuid, :user, :calculator

  def codespaces_compute_usage_overrides
    {
      productRatePlanChargeId: compute_product_rate_plan_charge_id,
      includedUnits: included_compute_rate_plan_charge_units
    }
  end

  def codespaces_storage_usage_overrides
    {
      productRatePlanChargeId: storage_product_rate_plan_charge_id,
      includedUnits: included_storage_rate_plan_charge_units
    }
  end
end
