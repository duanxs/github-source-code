# frozen_string_literal: true

class Billing::MeteredUsageReportGenerator
  BATCH_SIZE = 8
  HEADER = ["Date", "Product", "Repository Slug", "Quantity", "Unit Type", "Price Per Unit", "Actions Workflow"].freeze
  VALID_DURATIONS = [7, 30, 90, 180].freeze

  def self.csv_for(owner:, days:)
    new(owner, start_date: Date.current - days.days, end_date: Date.current).to_csv
  end

  def initialize(billable_owner, start_date:, end_date: Date.current)
    @billable_owner = billable_owner
    @start_date = start_date
    @end_date = end_date
  end

  def to_csv
    GitHub::CSV.generate do |csv|
      csv << HEADER

      ActiveRecord::Base.connected_to(role: :reading) do
        (start_date..end_date).each_slice(BATCH_SIZE) do |date_range|
          usages = usage_between(start_date: date_range.first, end_date: date_range.last)
          repositories = Repository.where(id: usages.map(&:repository_id)).includes(:owner).select(:id, :name, :owner_id, :owner_login).index_by(&:id)

          usages.each do |usage|
            csv << usage.to_row(repositories)
          end
        end
      end
    end
  end

  private

  attr_reader :billable_owner, :start_date, :end_date

  def usage_between(start_date:, end_date:)
    start_time = start_date.beginning_of_day
    end_time = end_date.end_of_day

    [
      actions_usages(start_time: start_time, end_time: end_time),
      packages_usage(start_time: start_time, end_time: end_time),
      shared_storage_usage(start_time: start_time, end_time: end_time),
    ].flatten.sort
  end

  def actions_usages(start_time:, end_time:)
    lines = Billing::ActionsUsageLineItem.
      where(billable_owner: billable_owner, end_time: start_time..end_time).
      group(:repository_id, "DATE(end_time)", :job_runtime_environment, :workflow_id).
      order("DATE(end_time)").
      sum("CAST(CEIL(duration_in_milliseconds / 60000.0) AS SIGNED)")

    workflow_ids = lines.map { |line| line[0][3] }.uniq.compact
    workflows = Actions::Workflow.where(id: workflow_ids).index_by(&:id)

    lines.map do |(repository_id, date, runtime, workflow_id), value|
      price = (Billing::ActionsUsageLineItem.billable_rate_for(runtime: runtime.to_sym) * BigDecimal(Billing::Actions::ZuoraProduct::UNIT_COST)).round(3, :truncate)

      workflow_file = workflows[workflow_id]&.path
      Usage.new(repository_id, date, "actions", value.round, runtime, "$#{price}", workflow_file: workflow_file)
    end
  end

  def packages_usage(start_time:, end_time:)
    line_items = Billing::PackageRegistry::DataTransferLineItem.
      where(
        billable_owner: billable_owner,
        downloaded_at: start_time..end_time,
      ).
      group("registry_package_id", "DATE(downloaded_at)").
      order("DATE(downloaded_at)").
      sum("size_in_bytes")

    # avoid n+1 in `.map` below by associating packages with repository_id's
    package_ids = line_items.keys.map(&:first)
    packages = Registry::Package.where(id: package_ids).pluck(:id, :repository_id).to_h
    price = Billing::Money.new(Billing::PackageRegistry::ZuoraProduct::UNIT_COST_IN_CENTS).format

    usages = line_items.map do |(registry_package_id, date), value|
      repository_id = packages[registry_package_id]
      Usage.new(repository_id, date, "packages", (value.to_f / 1.gigabyte).round(4), "gb", price)
    end

    usages.group_by { |usage| [usage.repository_id, usage.date] }.values.map do |usages|
      usages.reduce(&:+)
    end
  end

  def shared_storage_usage(start_time:, end_time:)
    usages = Billing::SharedStorage::ArtifactAggregation.
      where(
        billable_owner: billable_owner,
        aggregate_effective_at: start_time..end_time,
      ).
      group(:repository_id, "DATE(aggregate_effective_at)").
      pluck(:repository_id, "DATE(aggregate_effective_at)", "AVG(aggregate_size_in_bytes)")

    price = Billing::Money.new(Billing::SharedStorage::ZuoraProduct::UNIT_COST_PER_GB_IN_CENTS).format

    usages.map do |(repository_id, date, value)|
      Usage.new(repository_id, date, "shared storage", (value.to_f / 1.gigabyte).round(4), "gb", price)
    end
  end

  class Usage
    include Comparable

    attr_reader :repository_id, :date

    def initialize(repository_id, date, product, quantity, unit_type, price_per_unit, workflow_file: nil)
      @repository_id = repository_id
      @date = date
      @product = product
      @quantity = quantity
      @unit_type = unit_type
      @price_per_unit = price_per_unit
      @workflow_file = workflow_file
    end

    def +(other)
      @quantity = (@quantity + other.quantity).round(4)
      self
    end

    def to_row(repository_cache)
      repository_nwo = repository_cache[repository_id]&.nwo || "deleted repositories"
      [date.iso8601, product, repository_nwo, quantity, unit_type, price_per_unit, workflow_file]
    end

    def <=>(other)
      date <=> other.date
    end

    protected

    attr_reader :product, :quantity, :unit_type, :price_per_unit, :workflow_file
  end
  private_constant :Usage
end
