# frozen_string_literal: true

class Billing::Azure::JobRunGoverner
  def initialize(job_key:)
    @job_key = job_key
  end

  def run_now?
    !next_run_time.future?
  end

  def next_run_time
    GitHub::Billing.timezone.at(GitHub.kv.get(job_key).value!.to_i)
  end

  def record_next_run_time(date:, hour:)
    GitHub.kv.set(job_key, GitHub::Billing.date_in_timezone(date, hours: hour, minutes: 0, seconds: 0).to_i.to_s)
  end

  private

  attr_reader :job_key
end
