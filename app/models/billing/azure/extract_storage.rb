# frozen_string_literal: true

class Billing::Azure::ExtractStorage
  include Singleton

  BUCKET_NAME = "github-billing-azure-exports"

  class << self
    delegate :store, :list_objects, :generate_expiring_url, to: :instance
  end

  def store(data:, filename:, content_type:)
    bucket.
      object(filename).
      put(body: data, content_type: content_type)
  end

  def list_objects(prefix:)
    bucket.objects(prefix: prefix)
  end

  def generate_expiring_url(object_key:)
    presigner.presigned_url(:get_object, bucket: BUCKET_NAME, key: object_key, expires_in: 10.minutes.to_i)
  end

  private

  def bucket
    @bucket ||= Aws::S3::Resource.new(client: client).
      bucket(BUCKET_NAME)
  end

  def presigner
    @presigner ||= Aws::S3::Presigner.new(client: client)
  end

  def client
    GitHub.s3_billing_azure_exports_client
  end
end
