# frozen_string_literal: true

module Billing
  class Dispute < ApplicationRecord::Collab
    self.table_name = "billing_disputes"
    areas_of_responsibility :gitcoin

    belongs_to :user
    belongs_to :billing_transaction, class_name: "Billing::BillingTransaction"

    enum platform: {
      unknown: 0,
      stripe: 1,
    }

    # Public: Money amount of this dispute
    #
    # Returns Money
    def amount
      Billing::Money.new(amount_in_subunits, currency_code)
    end
  end
end
