# frozen_string_literal: true

module Billing
  module Actions
    class UsageLineItemCreator
      extend Forwardable

      def_delegators ActionsUsageLineItem, :billable_rate_for

      def self.create(**kwargs)
        new.create(**kwargs)
      end

      def create(job_id:, actor_id:, repository_id:, check_run_id:, duration_in_milliseconds:, start_time:, end_time:, owner_id:, billable_owner_id:, billable_owner_type:, directly_billed:, job_runtime_environment:)
        duration_multiplier = billable_rate_for(runtime: job_runtime_environment)

        workflow_id = CheckRun.find_by_id(check_run_id)&.check_suite&.workflow_run&.workflow&.id
        line_item = ::Billing::ActionsUsageLineItem.create!(
          job_id: job_id,
          actor_id: actor_id,
          repository_id: repository_id,
          duration_in_milliseconds: duration_in_milliseconds,
          check_run_id: check_run_id,
          workflow_id: workflow_id,
          start_time: start_time,
          end_time: end_time,
          owner_id: owner_id,
          billable_owner_type: billable_owner_type,
          billable_owner_id: billable_owner_id,
          directly_billed: directly_billed,
          job_runtime_environment: job_runtime_environment,
          duration_multiplier: duration_multiplier,
          submission_state: :unsubmitted
        )

        UsageAggregationJob.perform_later(line_item) if User.exists?(owner_id)

        line_item.publish_metered_line_item_updated_message
      rescue ActiveRecord::RecordNotUnique
        # Do nothing
      end
    end
  end
end
