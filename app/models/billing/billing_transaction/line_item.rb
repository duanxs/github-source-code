# frozen_string_literal: true

module Billing
  class BillingTransaction::LineItem < ApplicationRecord::Domain::Users
    self.ignored_columns = %w(marketplace_listing_plan_id)

    ACTIONS_PRIVATE_USAGE_DESCRIPTION = "GitHub Actions - Private Repos Usage"
    PACKAGES_DATA_TRANSFER_USAGE_DESCRIPTION = "GitHub Package Registry - Data Transfer"
    SHARED_STORAGE_USAGE_DESCRIPTION = "GitHub Shared Storage - GitHub.Shared Storage"

    USAGE_DESCRIPTIONS = [
      ACTIONS_PRIVATE_USAGE_DESCRIPTION,
      PACKAGES_DATA_TRANSFER_USAGE_DESCRIPTION,
      SHARED_STORAGE_USAGE_DESCRIPTION,
    ].freeze

    areas_of_responsibility :gitcoin

    enum subscribable_type: {
      Marketplace::ListingPlan.name => 0,
      SponsorsTier.name => 1,
    }

    belongs_to :billing_transaction
    belongs_to :listing,
      class_name: "Marketplace::Listing",
      foreign_key: "marketplace_listing_id"
    belongs_to :subscribable, polymorphic: true

    validates :billing_transaction, presence: true
    validates :description, presence: true
    validates :quantity, presence: true, numericality: true
    validates :amount_in_cents, presence: true, numericality: true

    # Returns LineItems tied to Marketplace Purchases
    scope :marketplace, -> { where(subscribable_type: Marketplace::ListingPlan.name) }
    scope :sponsorships, -> { where(subscribable_type: SponsorsTier.name) }
    scope :github, -> { where(subscribable_id: nil) }
    scope :paid, -> { where("amount_in_cents != 0") }
    scope :for_plan, -> (plan) { where(subscribable: plan) }

    scope :usage, -> { where(description: USAGE_DESCRIPTIONS) }

    scope :actions_usage, -> { where(description: ACTIONS_PRIVATE_USAGE_DESCRIPTION) }
    scope :packages_data_transfer_usage, -> { where(description: PACKAGES_DATA_TRANSFER_USAGE_DESCRIPTION) }
    scope :shared_storage_usage, -> { where(description: SHARED_STORAGE_USAGE_DESCRIPTION) }

    # See https://github.com/github/sponsors/issues/353
    # After sponsors are decoupled from the marketplace, all marketplace
    # line items will be non_sponsorships, so we can change the
    # sponsorships/non_sponsorships scopes to just use subscribable_type.
    scope :non_sponsorships, -> {
      marketplace.where(subscribable_id: Marketplace::ListingPlan.for_non_sponsorships.pluck(:id).to_a)
    }

    # `listing` needs to be a Marketplace::Listing, and sponsorships may be recorded with a SponsorsListing.
    # Use the corresponding Marketplace::Listing instead.
    #
    # We need to remove the listing association before we stop maintaining parallel listings.
    # See https://github.com/github/sponsors/issues/353
    def listing=(new_listing)
      new_listing = new_listing.find_corresponding_marketplace_listing if new_listing.is_a?(SponsorsListing)
      super(new_listing)
    end

    def subscribable_is_marketplace_listing_plan?
      subscribable_type == Marketplace::ListingPlan.name
    end
  end
end
