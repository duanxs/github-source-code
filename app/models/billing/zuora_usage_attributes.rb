# frozen_string_literal: true

class Billing::ZuoraUsageAttributes
  PRODUCT_RATE_PLAN_CHARGE_IDS = {
    actions: {
      self_serve: -> { [Billing::Actions::ZuoraProduct::PRIVATE_VISIBILITY_RATE_PLAN_CHARGE.zuora_charge_ids[:usage]] },
      sales_serve: -> { GitHub.zuora_sales_serve_actions_product_charge_ids },
    }.freeze,
    packages: {
      self_serve: -> { [Billing::PackageRegistry::ZuoraProduct.uuid.zuora_product_rate_plan_charge_ids[:bandwidth]] },
      sales_serve: -> { GitHub.zuora_sales_serve_packages_product_charge_ids },
    }.freeze,
    shared_storage: {
      self_serve: -> { [Billing::SharedStorage::ZuoraProduct.uuid.zuora_product_rate_plan_charge_ids[:usage]] },
      sales_serve: -> { GitHub.zuora_sales_serve_shared_storage_product_charge_ids },
    }.freeze,
    codespaces_compute: {
      self_serve: -> { [Billing::Codespaces::ZuoraProduct.uuid.zuora_product_rate_plan_charge_ids[:compute_usage_rpc]] },
      sales_serve: -> { [] }
    }.freeze,
    codespaces_storage: {
      self_serve: -> { [Billing::Codespaces::ZuoraProduct.uuid.zuora_product_rate_plan_charge_ids[:storage_usage_rpc]] },
      sales_serve: -> { [] }
    }.freeze,
  }.freeze

  def initialize(billable_owner:, usage_type:, usage_at:)
    if !PRODUCT_RATE_PLAN_CHARGE_IDS.include?(usage_type)
      raise ArgumentError.new("Unknown usage_type #{usage_type} - it must be one of #{PRODUCT_RATE_PLAN_CHARGE_IDS.keys}")
    end

    @billable_owner = billable_owner
    @usage_type = usage_type
    @usage_at = usage_at
  end

  # Public: The Zuora Account Number to bill
  #
  # Returns String
  def account_number
    billable_owner.customer.zuora_account_number
  end

  # Public: The Zuora Subscription ID to bill
  #
  # Returns String
  def subscription_number
    subscription&.zuora_subscription_number
  end

  # Public: The Zuora Charge Number for the corresponding rate plan charge
  #
  # Returns String
  def charge_number
    product_rate_plan_charge_ids.each do |product_rate_plan_charge_id|
      number = subscription.zuora_rate_plan_charge_number(product_rate_plan_charge_id: product_rate_plan_charge_id)

      return number if number.present?
    end

    nil
  end

  private

  attr_reader :billable_owner, :usage_type, :usage_at

  def subscription
    @subscription ||= billable_owner.is_a?(Business) ? billable_owner.customer.sales_serve_plan_subscription : billable_owner.plan_subscription
  end

  def product_rate_plan_charge_ids
    PRODUCT_RATE_PLAN_CHARGE_IDS.dig(usage_type, customer_type).call
  end

  def customer_type
    billable_owner.invoiced? ? :sales_serve : :self_serve
  end
end
