# frozen_string_literal: true

module Billing
  # Closes down a Zuora subscription by cancelling the subscription in Zuora
  # and making necessary financial adjustments
  class CloseZuoraSubscription

    # Public: Close down a Zuora subscription
    #
    # Returns GitHub::Billing::Result
    def self.perform(**kwargs)
      new(**kwargs).perform
    end

    # Initialize a new CloseZuoraSubscription
    #
    # zuora_subscription_number - The zuora subscription number of the subscription to be cancelled
    # plan_subscription - The plan subscription to be closed can be nil.
    #   If the plan subscription is present, the PlanSubscription::Synchronizer will take care of
    #   cancelling the subscription and nullifying the values in plan_subscription
    #
    #
    def initialize(zuora_subscription_number:, plan_subscription:)
      @zuora_subscription_number = zuora_subscription_number
      @plan_subscription         = plan_subscription
    end

    # Public: Close down a Zuora subscription
    #
    # Returns GitHub::Billing::Result
    def perform
      return GitHub::Billing::Result.success if zuora_subscription_number.blank? && plan_subscription.blank?

      begin
        cancel_result = cancel_subscription
        return cancel_result unless cancel_result.success?
      rescue ::Faraday::TimeoutError
        zero_out_subscription_balance_and_invoices
        raise
      end
      zero_out_subscription_balance_and_invoices
    end

    private

    attr_reader :plan_subscription, :zuora_subscription_number

    # Internal: Mark the Zuora subscription as cancelled
    #
    # Returns GitHub::Billing::Result
    def cancel_subscription
      if plan_subscription.present?
        Billing::PlanSubscription::Synchronizer.cancel(plan_subscription)
      else
        zuora_subscription = ::Billing::ZuoraSubscription.find(zuora_subscription_number)
        unless zuora_subscription
          Failbot.report(
            ::Billing::ZuoraSubscription::NotFoundError.new("Subscription not found for #{zuora_subscription_number} when cancelling subscription"),
          )
          return GitHub::Billing::Result.success
        end

        return GitHub::Billing::Result.success if zuora_subscription.cancelled?

        result = zuora_subscription.cancel
        return GitHub::Billing::Result.from_zuora(result)
      end
    end

    def zero_out_subscription_balance_and_invoices
      plan_subscription&.update(balance_in_cents: 0)
      ::Billing::Zuora::ZeroOutInvoices.for_subscription(zuora_subscription_number)
    end
  end
end
