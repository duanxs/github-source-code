# frozen_string_literal: true

class Billing::ActionsUsage
  def initialize(billable_owner, owner: nil)
    @billable_owner = billable_owner
    @plan = billable_owner.plan
    @owner = owner
  end

  def included_private_minutes
    plan.actions_included_private_minutes
  end

  def total_minutes_used
    total_private_minutes_used
  end

  def total_paid_minutes_used(additional_private_minutes: 0)
    paid_private_minutes_used(additional_private_minutes: additional_private_minutes)
  end

  def included_private_minutes_used
    [total_private_minutes_used, included_private_minutes].min
  end

  def paid_private_minutes_used(additional_private_minutes: 0)
    [0, total_private_minutes_used + additional_private_minutes - plan.actions_included_private_minutes].max
  end

  # Returns Hash
  # Public: Returns the rounded billable minutes executed for private repositories
  # for the user, both broken down by job_runtime_environment and as a total.
  #
  # Example response:
  # { "UBUNTU"=>20, "MACOS"=>1, "WINDOWS"=>3, "TOTAL"=>24 }
  #
  # Returns Hash
  def private_minutes_used
    @_private_minutes_used ||= billable_query.private_billable_minutes
  end

  def private_usage_percentage
    return 100 if plan.actions_included_private_minutes.zero?
    percent = (total_private_minutes_used / Float(plan.actions_included_private_minutes)).round(2)
    (percent * 100).to_i
  end

  def included_private_minutes_used_percentage
    return 100 if plan.actions_included_private_minutes.zero?
    percent = (included_private_minutes_used / Float(plan.actions_included_private_minutes)).round(2)
    (percent * 100).to_i
  end

  def included_minutes_usage_percentage
    # As of now, we'll only alert on the usage of minutes
    # in private repositories for GitHub Actions.
    included_private_minutes_used_percentage
  end

  def total_private_minutes_used
    private_minutes_used["TOTAL"]
  end

  private

  attr_reader :billable_owner, :plan, :owner

  def billable_query
    @_query ||= Billing::Actions::BillableQuery.new(billable_owner: billable_owner, owner: owner, after: billable_owner.first_day_in_metered_cycle)
  end
end
