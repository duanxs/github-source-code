# frozen_string_literal: true

class Billing::PackageRegistryUsage
  def initialize(billable_owner, owner: nil)
    @billable_owner = billable_owner
    @plan = billable_owner.plan
    @owner = owner
  end

  def total_gigabytes_used
    paid_gigabytes_used
  end

  def paid_gigabytes_used
    @_paid_gigabytes_used ||= billable_query.paid_gigabytes_used
  end

  def unrounded_paid_gigabytes_used
    @_unrounded_gigabytes_used ||= billable_query.unrounded_paid_gigabytes_used
  end

  def billable_gigabytes(additional_gigabytes: 0)
    [0, (unrounded_paid_gigabytes_used + additional_gigabytes).ceil - plan_included_bandwidth].max
  end

  def paid_usage_percentage
    return 100 if plan_included_bandwidth.zero?
    (billable_gigabytes.to_f / plan_included_bandwidth) * 100
  end

  def included_usage_percentage
    return 100 if plan_included_bandwidth.zero?
    percent = (unrounded_paid_gigabytes_used / Float(plan_included_bandwidth)).round(3)
    [(percent * 100).to_i, 100].min
  end

  def private_bandwidth_usage_percentage
    return 100 if plan.package_registry_included_bandwidth.zero?
    percent = (unrounded_paid_gigabytes_used / Float(plan.package_registry_included_bandwidth)).round(2)

    (percent * 100).to_i
  end

  def plan_included_bandwidth
    plan.package_registry_included_bandwidth
  end

  private

  attr_reader :billable_owner, :plan, :owner

  def billable_query
    @_query ||= Billing::PackageRegistry::BillableQuery.new(billable_owner: billable_owner, owner: owner, after: billable_owner.first_day_in_metered_cycle)
  end
end
