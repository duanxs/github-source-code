# rubocop:disable Style/FrozenStringLiteralComment

module Billing
  class PlanSubscription < ApplicationRecord::Domain::Users
    areas_of_responsibility :gitcoin

    class CancellationError < StandardError; end

    include Instrumentation::Model
    include GitHub::Relay::GlobalIdentification
    include ::Billing::PlanSubscription::Synchronization
    include ::Billing::PlanSubscription::SynchronizationStatus

    belongs_to :customer, required: true
    belongs_to :user, inverse_of: false, required: true

    has_many :subscription_items, dependent: :destroy

    has_many :active_subscription_items, -> { active }, class_name: "Billing::SubscriptionItem"

    validates_uniqueness_of :user_id

    validates :apple_transaction_id, uniqueness: { allow_nil: true, case_sensitive: false }

    attr_reader :error_result

    # plan, plan_duration, seats, and payment_method_token are all stored in
    # the Users table. These four methods can be removed once we've relocated
    # the columns to PlanSubscription.
    delegate :billed_on,
             :coupon,
             :data_packs,
             :payment_amount,
             :payment_method_token,
             :plan,
             :plan_duration,
             :plan_duration_in_months,
             :seats,
             :yearly_plan?,
      to: :user, allow_nil: true

    delegate :zuora_account,
      :zuora_account_id,
      to: :customer

    delegate :active?, to: :external_subscription, allow_nil: true

    after_commit :synchronize_later, on: :create
    before_destroy :queue_external_subscription_cancellation

    serialize :zuora_rate_plan_charges, Hash

    def zuora_rate_plan_charge_number(product_rate_plan_charge_id:)
      zuora_rate_plan_charges.dig(product_rate_plan_charge_id, :number)
    end

    # Find the remote BraintreeSubscription object.
    # NB: This makes a remote call to Braintree.
    #
    # braintree_id - String id of the braintree subscription
    #
    # Returns a BraintreeSubscription or nil
    def braintree_subscription
      BraintreeSubscription.find(braintree_id) if braintree_id
    end

    # Find the remote ZuoraSubscription object.
    #
    # Returns a Billing::ZuoraSubscription or nil
    def zuora_subscription
      Billing::ZuoraSubscription.find(zuora_subscription_number)
    end

    # Resumes the Zuora subscription and updates the subscription ID if successful
    #
    # Returns Boolean
    def resume
      response = zuora_subscription&.resume
      return false if response.nil?

      if response["success"]
        update_from_zuora_subscription
      end

      response["success"]
    end

    # Suspends the Zuora subscription and updates the subscription ID if successful
    #
    # Returns Boolean
    def suspend
      response = zuora_subscription&.suspend
      return false if response.nil?

      if response["success"]
        update_from_zuora_subscription
      end

      response["success"]
    end

    # Updates the plan subscription information from the Zuora subscription
    #
    # Returns Boolean
    def update_from_zuora_subscription
      zuora_sub = zuora_subscription
      rate_plan_charges = {}
      zuora_sub.active_rate_plan_charges.each do |rate_plan_charge|
        rate_plan_charges[rate_plan_charge.product_rate_plan_charge_id] = { number: rate_plan_charge.number }
      end

      update(
        zuora_subscription_id: zuora_sub.id,
        zuora_rate_plan_charges: rate_plan_charges,
      )
    end

    def clear_external_subscription_references
      update(
        zuora_subscription_number: nil,
        zuora_subscription_id: nil,
        zuora_rate_plan_charges: {},
      )
    end

    def attach_orphaned_zuora_subscription
      active_subscription = zuora_account&.subscriptions.to_a.detect { |sub| sub["status"] == "Active" }
      return false unless active_subscription

      GitHub.dogstats.increment("billing.orphaned_zuora_subscription.count")
      update \
        zuora_subscription_id: active_subscription["id"],
        zuora_subscription_number: active_subscription["subscriptionNumber"]
    end

    # Public: Default event prefix for GitHub instrumentation. We're overriding
    #         it here because we don't want to use the default of
    #         "billing/plan_subscription".
    #
    # Returns Symbol event prefix
    def event_prefix
      :plan_subscription
    end

    # Public: Default event payload for GitHub instrumentation
    #
    # Returns Hash event payload
    def event_payload
      payload = { plan_subscription_id: id }

      if user.organization?
        payload[:org] = user
      elsif user.billable?
        payload[:user] = user
      end

      payload
    end

    # Public: Retry a charge for a Past Due subscription
    #
    # Returns a GitHub::BillingResult
    def retry_charge
      return GitHub::Billing::Result.success unless user.dunning?

      if zuora_subscription_id?
        response = GitHub.zuorest_client.create_invoice_collect accountKey: zuora_account_id
        ::GitHub::Billing::Result.from_zuora(response)
      end
    end

    # Public: Returns the balance on the subscription in dollars. Negative
    # balance means we owe the customer.
    #
    # Returns a BigDecimal
    def balance
      balance_in_cents / BigDecimal(100)
    end

    # Public: Generates Braintree related payloads
    # to send to Braintree during sync.
    #
    # Returns a Billing::PlanSubscription::BraintreeParams
    def braintree_params
      Billing::PlanSubscription::BraintreeParams.new(plan_subscription: self)
    end

    # Public: Generates Zuora related payloads
    # to send to Zuora during sync.
    #
    # Billing::PlanSubscription::ZuoraSubscriptionParams
    def zuora_params
      Billing::PlanSubscription::ZuoraSubscriptionParams.new(plan_subscription: self)
    end

    # Public: Cancel the Braintree::Subscription or Zuora::Subscription
    #
    # Returns result of cancelation
    def cancel_external_subscription
      begin
        CloseOutZuoraSubscriptionJob.perform_now(
          zuora_subscription_number: zuora_subscription_number,
          plan_subscription: self,
        )
      rescue ::Faraday::TimeoutError, ::Billing::Zuora::ZeroOutError => e
        raise CancellationError.new(e)
      end
    end

    def subscription_item_for_listing(listing)
      subscription_item = if listing.is_a?(SponsorsListing)
        subscription_item_for_sponsors_listing(listing)
      else
        subscription_item_for_marketplace_listing(listing)
      end

      subscription_item
    end

    # Public: The external subscription from Braintree or Zuora
    #
    # Returns BraintreeSubscription or ZuoraSubscription
    def external_subscription
      if braintree_id?
        braintree_subscription
      elsif zuora_subscription_number?
        zuora_subscription
      end
    end

    def has_external_subscription?
      braintree_id || zuora_subscription_number?
    end

    def external_subscription_type
      if zuora_subscription_number?
        EXTERNAL_SUBSCRIPTION_TYPES[:zuora]
      elsif braintree_id?
        EXTERNAL_SUBSCRIPTION_TYPES[:braintree]
      end
    end

    def sync_platform_type
      external_subscription_type ||
        (EXTERNAL_SUBSCRIPTION_TYPES[:zuora] if zuora_user?)
    end

    def discount
      Billing::Pricing.new(account: user, plan: plan).discount
    end

    def additional_seats
      return 0 if plan.per_repository?

      [seats - plan.base_units, 0].max
    end

    def unit_price
      if yearly_plan?
        Billing::Money.new(plan.yearly_unit_cost_in_cents)
      else
        Billing::Money.new(plan.unit_cost_in_cents)
      end
    end

    def base_price
      if yearly_plan?
        Billing::Money.new(plan.yearly_cost_in_cents)
      else
        Billing::Money.new(plan.cost_in_cents)
      end
    end

    def data_packs_unit_price
      Billing::Money.new(Asset::Status.data_pack_unit_price * (yearly_plan? ? 12 : 1))
    end

    def clear_zuora_association
      update!(zuora_subscription_number: nil, zuora_subscription_id: nil)
    end

    def platform_type_name
      "Subscription"
    end

    def on_free_trial?
      Billing::PlanTrial.active_for?(user, plan.name).tap do
        # We reload the user to ensure we have the most up-to-date plan information after we check for a trial.
        # This is because there's a race condition when customers are upgrading to paid customers while in a
        # free trial which results in them being billed for the enterprise with 50 users (which is what they
        # get during the trial), rather than being billed for whatever plan/seat usage they are trying to
        # purchase. This is caused by us having a stale copy of the `user` which doens't have the new plan and
        # seat information yet and a fresh answer to whether they're on a trial or not. This does not fix
        # the race condition, but by reloading the user we'll be ensuring we are billing the most up to date
        # plan and seat count.
        user.reload
      end
    end

    # Public: Boolean if this user's subscription is an Apple IAP subscription
    def apple_iap_subscription?
      apple_receipt_id.present? && apple_transaction_id.present?
    end

    private

    def queue_external_subscription_cancellation
      CloseOutZuoraSubscriptionJob.perform_later(zuora_subscription_number: zuora_subscription_number)
    end

    # Internal: is the plan subscription found on zuora
    #
    # Returns boolean
    def zuora_subscription?
      external_subscription_type == EXTERNAL_SUBSCRIPTION_TYPES[:zuora]
    end

    def zuora_user?
      user.zuora_account?
    end

    def subscription_item_for_marketplace_listing(listing)
      listing_plans_ids = Marketplace::ListingPlan.where(marketplace_listing_id: listing.id).pluck(:id)
      active_subscription_items.for_marketplace_listing_plans(listing_plans_ids).last
    end

    def subscription_item_for_sponsors_listing(listing)
      listing.subscription_items.active.find_by(plan_subscription: self)
    end

    EXTERNAL_SUBSCRIPTION_TYPES = {
      zuora: "zuora".freeze,
      braintree: "braintree".freeze,
    }.freeze

    attr_writer :error_result
  end
end
