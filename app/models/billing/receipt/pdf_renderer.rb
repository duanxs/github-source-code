# rubocop:disable Style/FrozenStringLiteralComment
require "prawn/table"

module Billing
  class Receipt::PdfRenderer
    CELL_VERTICAL_PADDING = 10
    LINESPACING = 4

    def initialize(receipt, show_email_address: true)
      @receipt = receipt
      @show_email_address = show_email_address
    end

    # Public: Returns a PDF of the receipt.
    def render
      receipt    = @receipt
      show_email_address = @show_email_address
      user_login = receipt.user.try(:login)
      user_email = receipt.user.try(:billing_email)

      svg_logo = File.open("#{Rails.root}/public/images/modules/pricing/pdf-logo.svg").read
      svg_thankyou = File.open("#{Rails.root}/public/images/modules/pricing/thankyou.svg").read

      document = Prawn::Document.new({ margin: [50, 60, 50, 60] }) do
        # Page width: 492

        font_families["Arial-Unicode"] = {
          normal: "#{Rails.root}/lib/assets/fonts/Arial Unicode Bold.ttf",
          bold: "#{Rails.root}/lib/assets/fonts/Arial Unicode Bold.ttf",
        }
        font_families["Helvetica"] = {
          normal: "#{Rails.root}/lib/assets/fonts/Helvetica.dfont",
          bold: "#{Rails.root}/lib/assets/fonts/Helvetica Bold.ttf",
        }

        self.line_width = 0.3
        self.fallback_fonts ["Arial-Unicode"]

        image "#{Rails.root}/public/images/modules/pricing/github-mark.png", position: :center, width: 80

        move_down 15

        font_size 11
        stroke_color "aaaaaa"
        fill_color "777777"
        horizontal_line 216, 276

        move_down 20

        fill_color "333333"

        if receipt.sponsorship_only_transaction? && receipt.sponsorship_line_items_displayable?
          text(receipt.sponsorship_only_text, leading: 5, inline_format: true)
        elsif receipt.prorated_charge? || receipt.job_posting?
          text("Thanks for your purchase!", leading: 5, inline_format: true)
        else
          text "We received payment for your GitHub.com subscription. Thanks for your business!",
            leading: 5, inline_format: true
        end

        text "Questions? Visit <link href='https://github.com/contact'><color rgb='0088cc'>https://github.com/contact</color></link>.",
          leading: 5, inline_format: true

        move_down 15

        data = []
        data << ["Date", "<b>#{receipt.purchased_at.strftime("%Y-%m-%d %I:%M%p %Z")}</b>"]
        data << ["Account billed", "<b>#{user_login}</b>" + (show_email_address ? " (#{user_email})" : "")]
        data << ["VAT/GST Identification Number", "<b>#{receipt.vat_code}</b>"] if receipt.vat_code.present?
        data << ["GitHub Plan", "<b>#{receipt.plan_summary}</b>"]
        data << ["Data", "<b>#{receipt.data_packs_addon_text}</b>"] if receipt.plan_renewal_with_data_packs?
        if receipt.actions_line_items.any?
          data << ["GitHub Actions", "<b>#{receipt.actions_line_items_text}</b>"]
        end
        if receipt.package_registry_transfer_line_items.any?
          data << ["GitHub Packages", "<b>#{receipt.packages_transfer_line_items_text}</b>"]
        end
        if receipt.shared_storage_line_items.any?
          data << ["Actions/Packages Storage", "<b>#{receipt.shared_storage_line_items_text}</b>"]
        end
        if receipt.marketplace_line_items.any? || receipt.sponsorship_line_items_displayable?
          data << ["Plan Amount", "<b>#{receipt.plan_amount.format(with_currency: true)}</b>"]
        end
        if receipt.marketplace_line_items.any?
          data << ["Marketplace Apps", "<b>#{receipt.marketplace_line_items_text}</b>"]
          data << ["Marketplace Amount", "<b>#{receipt.marketplace_amount.format(with_currency: true)}</b>"]
        end
        first_sponsor_row = nil
        if receipt.sponsorship_line_items_displayable?
          first_sponsor_row = data.length
          receipt.sponsorship_line_items.each_with_index do |item, i|
            first_column = i.zero? ? "Sponsorships" : ""
            data << [first_column, "<b>#{item.subscribable.listing.name} - #{item.subscribable.name}</b>"]
          end
          data << ["Sponsorship Amount", "<b>#{receipt.sponsorship_amount.format(with_currency: true)}</b>"]
        end
        data << ["Total", "<b>#{receipt.amount.format(with_currency: true)}*</b>"]
        data << ["Charged to", "<b>#{receipt.payment_type}</b> (#{receipt.payment_identifier})"]
        data << ["Transaction ID", "<b>#{receipt.transaction_id}</b>"]
        data << ["For service through", "<b>#{receipt.service_ends_on}</b>"] if receipt.service_ends_on
        data << ["Extra billing information\n<font size='10'>(Added by @#{user_login})</font>", "<b>#{receipt.extra}</b>"] if receipt.extra.present?

        table data, column_widths: [140, 352], cell_style: {
          borders: [:top],
          border_width: 0.3,
          border_color: "dddddd",
          inline_format: true,
          padding: [CELL_VERTICAL_PADDING, 0, CELL_VERTICAL_PADDING, 0],
        } do
          # Override borders for the first row
          row(0).borders = []
          cells.leading = 4

          # Override some cell borders and padding to make sponsors line items look as though they're one row
          # but really they're many rows so that we can have more than one page of them
          if first_sponsor_row && receipt.sponsorship_line_items.count > 1
            last_sponsor_row = first_sponsor_row + receipt.sponsorship_line_items.count - 1

            row(first_sponsor_row).padding = [CELL_VERTICAL_PADDING, 0, 0, 0]
            rows((first_sponsor_row + 1)..last_sponsor_row).borders = []
            rows((first_sponsor_row + 1)..(last_sponsor_row - 1)).padding = [LINESPACING, 0, 0, 0]
            row(last_sponsor_row).padding = [LINESPACING, 0, CELL_VERTICAL_PADDING, 0]
          end
        end

        # Footer here
        svg svg_thankyou, at: [0, 160], width: 180 if data.length <= 10
        svg svg_logo, at: [0, 100], width: 40
        move_down 15

        font_size 10
        fill_color "999999"

        table [["GitHub, Inc.\n88 Colin P. Kelly Jr. Street\nSan Francisco, CA 94107\nUnited States",
          "https://github.com/contact"]], cell_style: {
            borders: [], padding: 0
          }, column_widths: [150, 150] do
          row(0).leading = 4
        end

        move_down 10

        vat_line  = "* VAT/GST paid directly by GitHub, where applicable"
        draw_text vat_line, at: [0, 0], size: 10
      end

      document.render
    end
  end
end
