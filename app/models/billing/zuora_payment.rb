# frozen_string_literal: true

module Billing
  # A wrapper for Zuora Payment objects that provides easy access to payment
  # gateway-specific transaction details
  class ZuoraPayment
    attr_reader :zuora_payment

    # Public: Find a payment by its ID
    #
    # zuora_id - The ID of the Zuora payment
    #
    # Returns Billing::ZuoraPayment
    def self.find(zuora_id)
      zuora_payment = Zuorest::Model::Payment.find(zuora_id)
      new(zuora_payment)
    end

    # Public: Initialize a new ZuoraPayment
    #
    # zuora_payment - A Zuorest::Model::Payment object from Zuora
    def initialize(zuora_payment)
      @zuora_payment = zuora_payment
    end

    # Public: The Zuora-assigned payment ID
    #
    # Returns String
    def id
      zuora_payment.id
    end

    # Public: The amount of the payment
    #
    # Returns BigDecimal
    def amount
      money_amount.dollars
    end

    # Public: The amount of the payment in cents
    #
    # Returns Integer
    def amount_in_cents
      money_amount.cents
    end

    # Public: The date when the payment was created in Zuora
    #
    # Returns Time
    def created_date
      Time.parse(zuora_payment["CreatedDate"])
    end

    # Public: The amount of the payment that has been refunded
    #
    # Returns Integer
    def refund_amount
      zuora_payment["RefundAmount"]
    end

    # Public: The status of the payment transaction
    #
    # Returns String
    def status
      zuora_payment["Status"]
    end

    # Public: Decorate a BillingTransaction object with payment details
    #
    # This method will mutate the fields of the provided billing_transaction
    # with details about the payment that are specific to the type of payment.
    #
    # billing_transaction - A Billing::BillingTransaction object to decorate
    #
    # Returns nothing
    def decorate_billing_transaction(billing_transaction)
      billing_transaction.transaction_id = reference_id

      gateway_transaction.decorate_billing_transaction(billing_transaction)
    end

    # Public: The payment gateway that processed this transaction
    #
    # Returns String (e.g. Braintree)
    def gateway
      zuora_payment["Gateway"]
    end

    # Public: Is Stripe the gateway that processed this transaction?
    #
    # Returns Boolean
    def stripe?
      gateway == "Stripe"
    end

    # Public: Is Paypal the gateway that processed this transaction?
    #
    # Returns Boolean
    def paypal?
      gateway == "Paypal"
    end

    # Public: The gateway response for this transaction
    #
    # Returns String
    def gateway_response
      zuora_payment["GatewayResponse"]
    end

    # Public: The gateway response code for this transaction
    #
    # Returns String
    def gateway_response_code
      zuora_payment["GatewayResponseCode"]
    end

    # Public: The state of the transaction in the upstream gateway, normalized
    # by Zuora
    #
    # Returns String
    def gateway_state
      zuora_payment["GatewayState"]
    end

    # Public: The gateway-specific details about the transaction
    #
    # Returns Billing::ZuoraPayment::Braintree or similar class
    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def gateway_transaction
      @gateway_transaction ||= \
        case gateway
        when "Stripe", "Stripe v2" then ::Billing::ZuoraPayment::StripePayment.new(self)
        when "Braintree" then ::Billing::ZuoraPayment::BraintreePayment.new(self)
        when "Paypal" then ::Billing::ZuoraPayment::PaypalPayment.new(self)
        end
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

    # Public: Is this payment a retry for a previous failed charge?
    #
    # Returns boolean
    def is_retry?
      num_consecutive_failures > 0
    end

    # Public: The number of consecutive failures for this payment method
    #
    # Returns integer
    def num_consecutive_failures
      payment_method_snapshot["NumConsecutiveFailures"]
    end

    # Public: The ID of the Zuora payment method
    #
    # Returns String
    def payment_method_id
      zuora_payment["PaymentMethodId"]
    end

    # Public: The ID of the Zuora payment method snapshot
    #
    # Returns String
    def payment_method_snapshot_id
      zuora_payment["PaymentMethodSnapshotId"]
    end

    # Public: The Zuora payment method snapshot for this transaction
    #
    # Returns Hash
    def payment_method_snapshot
      @payment_method_snapshot ||= GitHub.zuorest_client.get_payment_method_snapshot(payment_method_snapshot_id)
    end

    # Public: The processor response from the processing gateway
    #
    # Returns String
    def processor_response
      gateway_transaction.processor_response
    end

    # Public: The processor response code from the processing gateway
    #
    # Returns String
    def processor_response_code
      gateway_transaction.processor_response_code
    end

    # Public: The reference ID (or transaction ID) for the transaction
    #
    # Returns String
    def reference_id
      zuora_payment["ReferenceId"]
    end

    # Public: A zuora provided number to reference a payment
    #
    # Returns String
    def payment_number
      zuora_payment["PaymentNumber"]
    end

    # Public: Finds all the invoices the payment was applied to
    #
    # Returns Array[Billing::ZuoraInvoice]
    def invoices
      ::Billing::ZuoraInvoice.invoices_for_transaction(id)
    end

    # Public: A list of invoice items that the payment was applied to
    #
    # Returns Array[Hash]
    def invoice_items
      @invoice_items ||= invoices.flat_map(&:invoice_items)
    end

    # Public: The Bank Identification Number for the Payment Method used
    #
    # Returns String
    def bank_identification_number
      zuora_payment["BankIdentificationNumber"]
    end

    private

    def money_amount
      ::Billing::Money.new(zuora_payment["Amount"] * 100)
    end
  end
end
