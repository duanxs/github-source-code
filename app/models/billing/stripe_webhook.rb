# frozen_string_literal: true

module Billing
  class StripeWebhook < ApplicationRecord::Ballast
    UnsupportedKind = Class.new(StandardError)
    RECENCY_THRESHOLD = 5.minutes

    areas_of_responsibility :gitcoin

    store :payload, coder: JSON

    scope :ignoring_recent, -> { where("created_at < ?", RECENCY_THRESHOLD.ago) }

    before_save :update_account_id_from_payload

    # Webhook kinds SHOULD correspond to transaction types in the payouts ledger
    # to avoid confusion. See `Billing::PayoutsLedgerEntry` for transaction types.
    #
    # 10..11 - Unused (these ledger transactions do not come from Stripe webhooks)
    # 12..14 - Stripe Disputes (chargebacks)
    # 15..29 - Unused (these ledger transactions do not come from Stripe webhooks)
    # 30..39 - Stripe Transfers
    # 40..49 - Stripe Payouts
    # 100..109 - Stripe Connect accounts
    enum kind: {
      unknown: 0,
      # Disputes
      charge_dispute_created: 12,
      charge_dispute_updated: 13,
      charge_dispute_closed: 14,
      # Transfers
      transfer_created: 30,
      transfer_reversed: 31,
      transfer_failed: 32,
      # Payouts
      payout_created: 40,
      payout_failed: 41,
      # Account
      account_updated: 100,
    }

    enum status: {
      pending: "pending",
      processed: "processed",
      ignored: "ignored",
    }

    validates :status, presence: true, on: :create

    # Public: The class that is capable of handling the webhook payload
    #
    # Returns Class responding to #perform
    # Raises UnsupportedKind exception if the webhook kind isn't supported
    def handler
      case kind.to_sym
      when :payout_created then GitHub::Billing::StripeWebhook::PayoutCreated
      when :payout_failed then GitHub::Billing::StripeWebhook::PayoutFailed
      when :transfer_created then GitHub::Billing::StripeWebhook::TransferCreated
      when :transfer_failed then GitHub::Billing::StripeWebhook::TransferFailed
      when :transfer_reversed then GitHub::Billing::StripeWebhook::TransferReversed
      when :account_updated then GitHub::Billing::StripeWebhook::AccountUpdated
      when :charge_dispute_created, :charge_dispute_updated, :charge_dispute_closed
        GitHub::Billing::StripeWebhook::ChargeDispute
      else
        raise UnsupportedKind, "Could not handle webhook kind: #{kind}"
      end
    end

    # Public: Handle the webhook payload provided by Stripe
    #
    # Returns Boolean
    def perform
      return true if processed?

      handler.perform(self)
      update!(status: :processed, processed_at: Time.now)
    end

    # Public: Whether or not the webhook has been processed already
    #
    # Returns Boolean
    def processed?
      processed_at.present?
    end

    # Public: The URL for the Stripe Connect Account in the Stripe dashboard
    #
    # Returns String
    def connect_account_url
      "#{GitHub.stripe_connect_dashboard_base_url}/connect/accounts/#{account_id}"
    end

    def update_account_id_from_payload
      self.account_id = case kind.to_sym
      when :payout_created, :payout_failed, :account_updated
        payload["account"]
      when :transfer_created, :transfer_failed, :transfer_reversed
        payload["data"]["object"]["destination"]
      when :charge_dispute_created, :charge_dispute_updated, :charge_dispute_closed
        charge_id = payload.dig("data", "object", "charge")
        transaction = Billing::BillingTransaction.find_by(transaction_id: charge_id)
        return unless transaction
        user = User.preload(sponsors_listing: [:stripe_connect_account])
          .find(transaction.user_id)
        user.sponsors_listing&.stripe_account_id
      end
    end
  end
end
