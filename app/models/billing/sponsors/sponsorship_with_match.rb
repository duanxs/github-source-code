# frozen_string_literal: true

module Billing
  module Sponsors
    class SponsorshipWithMatch
      delegate_missing_to :@sponsorship

      # Public: Initialize SponsorshipWithMatch
      #
      # sponsorship - An instance of Billing::BillingTransaction::LineItem
      #
      # Returns SponsorshipWithMatch
      def initialize(sponsorship)
        @sponsorship = sponsorship
      end

      # Public: Calculate matching for current sponsorship
      #
      # Returns Integer
      def match_amount_in_cents
        return 0 unless sponsor.eligible_for_sponsorship_match?(sponsorable: sponsorable)
        ::Sponsors::CalculateMatch.for(listing, sponsorship_amount: amount_in_cents)
      end

      # Public: The sponsorship amount in cents
      #
      # Returns Integer
      def amount_in_cents
        sponsorship.amount_in_cents
      end

      # Public: The total amount of the sponsorship, including the match
      #
      # Returns Integer
      def total_in_cents
        amount_in_cents + match_amount_in_cents
      end

      private

      attr_reader :sponsorship

      def listing
        @listing ||= sponsorship.subscribable.listing
      end

      def sponsor
        @sponsor ||= sponsorship.billing_transaction.user
      end

      def sponsorable
        @sponsorable ||= listing.sponsorable
      end
    end
  end
end
