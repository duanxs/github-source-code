# rubocop:disable Style/FrozenStringLiteralComment

module Billing
  class MeteredUsageExport < ApplicationRecord::Collab
    BUCKET_NAME = "github-billing-report-exports"
    areas_of_responsibility :gitcoin

    belongs_to :requester, class_name: "User", optional: true
    belongs_to :billable_owner, polymorphic: true, optional: false

    validates :starts_on, presence: true
    validates :ends_on, presence: true
    validates :filename, presence: true, uniqueness: { case_sensitive: false }

    def generate_expiring_url
      presigner.presigned_url(
        :get_object,
        bucket: BUCKET_NAME,
        key: filename,
        expires_in: 10.minutes.to_i,
      )
    end

    private

    def presigner
      @presigner ||= Aws::S3::Presigner.new(
        client: GitHub.s3_metered_exports_client,
      )
    end
  end
end
