# frozen_string_literal: true

module Billing
  class ZuoraInvoice
    attr_reader :invoice_id
    alias_method :id, :invoice_id

    CANCELLED = "Canceled"
    POSTED = "Posted"

    def self.past_due(account_id:)
      response = GitHub.zuorest_client.query_action queryString: "select Id from Invoice where AccountId = '#{account_id}' and Status = 'Posted' and DueDate < #{GitHub::Billing.today} and Balance > 0"

      response["records"].map { |record| new(record["Id"]) }
    end

    # Public: Returns the invoices associated with a given subscription
    #
    # zuora_subscription_number: The Zuora subscription number
    #
    # Returns Array of Zuora::Invoice objects
    def self.invoices_for_subscription(zuora_subscription_number)
      return [] if zuora_subscription_number.blank?

      response = GitHub.zuorest_client.query_action queryString: "select InvoiceId from InvoiceItem where SubscriptionNumber = '#{zuora_subscription_number}'"

      create_raw_invoices_from_zuora(response)
    end

    # Public: Returns the invoices associated with a given payment
    #
    # zuora_transaction_id: The ID of the Zuora Payment
    #
    # Returns Array of Zuora::Invoice objects
    def self.invoices_for_transaction(zuora_transaction_id)
      response = GitHub.zuorest_client.query_action queryString: "select InvoiceID from InvoicePayment where PaymentId = '#{zuora_transaction_id}'"

      create_raw_invoices_from_zuora(response)
    end

    # Public: Returns the invoices associated with an account
    #
    # zuora_account_id: The ID of the Zuora Account
    #
    # Returns Array of Zuora::Invoice objects
    def self.invoices_for_account(zuora_account_id)
      invoices = Zuorest::Model::Account.new(id: zuora_account_id).invoices
      invoices.map { |record| invoice_for_data(record) }
    rescue Zuorest::Error
      []
    end

    # Public: Returns the invoice associated with a given invoice number
    #
    # zuora_transaction_id: The Number of the Zuora Invoice
    #
    # Returns Zuora::Invoice object
    def self.invoice_for_number(zuora_invoice_number)
      response = GitHub.zuorest_client.query_action queryString: "select Id from Invoice where InvoiceNumber = '#{zuora_invoice_number}'"

      response["records"].map { |record| new(record["Id"]) }.first
    end

    # Public: Creates Zuora::Invoice object from Zuorest::Model::Invoice object
    #
    # raw_invoice: The Zuorest::Model::Invoice
    #
    # Returns Zuora::Invoice object
    def self.invoice_for_data(raw_invoice)
      new(raw_invoice["id"], raw_invoice)
    end

    # Private: take the response from zuora and create invoices
    #
    # Returns an array of ::Billing::ZuoraInvoice records
    def self.create_raw_invoices_from_zuora(response)
      response
        .fetch("records", [])
        .uniq { |invoice| invoice["InvoiceId"] }
        .compact
        .map { |record| new(record["InvoiceId"]) }
    end
    private_class_method :create_raw_invoices_from_zuora

    def initialize(invoice_id, raw_invoice = nil)
      @invoice_id = invoice_id
      @_raw_invoice = raw_invoice
    end

    # Public: The invoice amount
    #
    # Returns Numeric
    def amount
      raw_invoice["amount"]
    end

    # Public: The invoice balance
    #
    # Returns Numeric
    def balance
      raw_invoice["balance"]
    end

    # Public: The invoice date
    #
    # Returns String
    def invoice_date
      raw_invoice["invoiceDate"]
    end

    # Public: The invoice date in %Y/%m/%d format
    #
    # This is used for display purposes
    #
    # Returns String
    def formatted_invoice_date
      DateTime.parse(invoice_date).strftime("%Y/%m/%d")
    rescue TypeError, ArgumentError
      "N/A"
    end

    # Public: The payment status of the invoice
    #
    # This is used for display purposes
    #
    # Returns String
    def payment_status
      if posted?
        if paid?
          "Paid"
        elsif past_due?
          "Payment Overdue"
        else
          "Invoiced"
        end
      else
        status.titleize
      end
    end

    # Public: The invoice status
    #
    # Returns String
    def status
      raw_invoice["status"]
    end

    # Public: The invoice number
    #
    # Returns String
    def number
      raw_invoice["invoiceNumber"]
    end

    # Public: The invoice body
    #
    # Returns String
    def body
      raw_invoice["body"]
    end

    # Public: The invoice due date
    #
    # Returns String
    def due_on
      raw_invoice["dueDate"]
    end

    # Public: The invoice account id
    #
    # Returns String
    def account_id
      raw_invoice["accountId"]
    end

    # Public: Whether the invoice is cancelled
    #
    # Returns Boolean
    def cancelled?
      status == CANCELLED
    end

    # Public: Whether the invoice is posted
    #
    # Returns Boolean
    def posted?
      status == POSTED
    end

    # Public: Whether the invoice is paid
    #
    # Returns Boolean
    def paid?
      balance <= 0
    end

    # Public: Whether to suppress this invoice from the customer's view
    #
    # Returns Boolean
    def suppress_from_customer_view?
      raw_invoice["SuppressFromCustomerView__c"] == "Yes"
    end

    # Public: Whether the invoice is past due
    #
    # Returns Boolean
    def past_due?(as_of: GitHub::Billing.today)
      Date.parse(due_on) < as_of
    end

    def invoice_items
      @_invoice_items ||= begin
        response = GitHub.zuorest_client.query_action queryString: "select ChargeAmount, ChargeName, Quantity, UOM from InvoiceItem where InvoiceId = '#{invoice_id}'"
        response["records"].map { |record| ::Billing::Zuora::InvoiceItem.new(record) }
      end
    end

    def raw_invoice
      @_raw_invoice ||= Zuorest::Model::Invoice.find(invoice_id)
    end
  end
end
