# frozen_string_literal: true

module Billing
  class ProductUUID < ApplicationRecord::Domain::Users
    enum billing_cycle: {
      User::MONTHLY_PLAN => 0,
      User::YEARLY_PLAN => 1,
      "one_time" => 2,
    }

    validates :zuora_product_id, presence: true
    validates :zuora_product_rate_plan_id, presence: true, uniqueness: { case_sensitive: true }
    validates :zuora_product_rate_plan_charge_ids, presence: true
    validates :billing_cycle, presence: true

    serialize :zuora_product_rate_plan_charge_ids, Hash

    scope :github_products, -> {
      where("product_type LIKE 'github.%'")
        .where.not(product_type: "github.coupon")
    }
    scope :discounts, -> { where(product_type: "github.coupon") }
    scope :marketplace, -> { where(product_type: "marketplace.listing_plan") }
  end
end
