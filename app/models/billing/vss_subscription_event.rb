# frozen_string_literal: true

class Billing::VssSubscriptionEvent < ApplicationRecord::Collab
  areas_of_responsibility :gitcoin

  enum status: {
    unprocessed: "unprocessed",
    processed: "processed",
    failed: "failed"
  }
end
