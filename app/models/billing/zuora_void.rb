# frozen_string_literal: true

module Billing
  class ZuoraVoid
    def self.process(*args); new(*args).process; end

    attr_reader :transaction

    delegate :platform_transaction_id, to: :transaction

    def initialize(transaction)
      @transaction = transaction
    end

    def process
      zuora_response = GitHub.dogstats.time("zuora.timing.transaction_void") do
        GitHub.zuorest_client.update_payment platform_transaction_id,
          Status: "Voided",
          Type: "Electronic"
      end

      response = GitHub::Billing::Result.from_zuora zuora_response
      zero_out_invoice_and_update_transaction if response.success?
      response
    rescue Zuorest::HttpError
      GitHub::Billing::Result.failure("Transaction not found: #{transaction.transaction_id}")
    end

    private

    def zero_out_invoice_and_update_transaction
      zero_out && transaction.update(last_status: :voided)
    end

    def zero_out
      ::Billing::Zuora::ZeroOutInvoices.for_transaction(platform_transaction_id)
    end

    def payment_amount
      Billing::Money.new(zuora_payment.amount_in_cents)
    end

    def zuora_payment
      @_zuora_payment ||= Billing::ZuoraPayment.find(platform_transaction_id)
    end
  end
end
