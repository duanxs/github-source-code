# frozen_string_literal: true

class Billing::RemainingRefillCalculator
  def initialize(billable_owner)
    @billable_owner = billable_owner
  end

  def remaining_balance_in_cents
    total_active_refills_in_cents - billing_cycle_costs_in_cents
  end

  def billing_cycle_costs_in_cents
    previous_metered_cycle_costs_in_cents + Billing::MeteredBillingPermission.new(billable_owner).total_usage
  end

  def previous_metered_cycle_costs_in_cents
    billing_cycle_months.sum do |start_date|
      end_date = start_date + 1.month
      shared_storage_usage_cost_in_cents(start_date, end_date) +
      package_registry_usage_cost_in_cents(start_date, end_date) +
      actions_usage_cost_in_cents(start_date, end_date)
    end
  end

  private

  attr_reader :billable_owner

  def total_active_refills_in_cents
    Billing::PrepaidMeteredUsageRefill.total_active_amount_in_cents_for(owner: billable_owner)
  end

  def included_shared_storage
    billable_owner.plan.shared_storage_included_megabytes
  end

  def included_package_bandwidth
    billable_owner.plan.package_registry_included_bandwidth
  end

  def included_actions_minutes
    billable_owner.plan.actions_included_private_minutes
  end

  def billing_cycle_months
    months_into_current_billing_cycle.times.map do |month_into_cycle|
      cycle_date = beginning_of_first_metered_cycle + month_into_cycle.month
      cycle_date.change(day: billing_cycle_day_for_month(cycle_date))
    end
  end

  def beginning_of_first_metered_cycle
    billable_owner.previous_billing_date - 1.month
  end

  def billing_cycle_day_for_month(date)
    day_of_metered_cycle = billable_owner.first_day_in_metered_cycle.day
    end_of_month = Date.new(date.year, date.month).end_of_month

    if day_of_metered_cycle > end_of_month.day
      end_of_month.day
    else
      day_of_metered_cycle
    end
  end

  def months_into_current_billing_cycle
    (GitHub::Billing.today.year * 12 + GitHub::Billing.today.month) -
      (billable_owner.previous_billing_date.year * 12 + billable_owner.previous_billing_date.month)
  end

  def shared_storage_usage_cost_in_cents(after_date, before_date)
    hours = Billing::SharedStorage::BillableQuery.new(
      billable_owner: billable_owner,
      after: after_date,
      before: before_date,
    ).private_billable_megabyte_hours / Billing::SharedStorageUsage::HOURS_IN_ASSUMED_MONTH

    [hours - included_shared_storage, 0].max * Billing::SharedStorage::ZuoraProduct::UNIT_COST_PER_MB_IN_CENTS
  end

  def package_registry_usage_cost_in_cents(after_date, before_date)
    gigabytes = Billing::PackageRegistry::BillableQuery.new(
      billable_owner: billable_owner,
      after: after_date,
      before: before_date,
    ).paid_gigabytes_used

    [gigabytes - included_package_bandwidth, 0].max * Billing::PackageRegistry::ZuoraProduct::UNIT_COST_IN_CENTS
  end

  def actions_usage_cost_in_cents(after_date, before_date)
    minutes = Billing::Actions::BillableQuery.new(
      billable_owner: billable_owner,
      after: after_date,
      before: before_date,
    ).private_billable_minutes["TOTAL"]

    [minutes - included_actions_minutes, 0].max * Billing::Actions::ZuoraProduct::UNIT_COST_IN_CENTS
  end
end
