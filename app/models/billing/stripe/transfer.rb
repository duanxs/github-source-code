# frozen_string_literal: true

module Billing
  module Stripe
    class Transfer
      class NotEnoughMatchError < StandardError; end

      attr_reader :transfer_id, :transfer_group, :platform_url, :match_amount,
        :match_amount_reversed, :sponsor_login, :amount, :destination_currency,
        :transferred_at

      LIMIT = 100

      # Public: Retrieve transfers from stripe and modify for our use
      #
      # destination - A String representing the stripe connect account id
      # destination_currency - The default currency of the destination account
      # starting_after - A String representing the transfer id, used for pagination.
      # transfer_group - A String representing the initial platform_transaction_id from the
      #                  transaction that triggered the transfer
      #
      # Returns an Array of Transfer instances
      def self.list(destination:, destination_currency:, limit: LIMIT, starting_after: nil, transfer_group: nil, ending_before: nil)
        return [] if ending_before && starting_after

        param_list = {
          limit: limit,
          destination: destination,
          starting_after: starting_after,
          transfer_group: transfer_group,
          ending_before: ending_before,
        }

        stripe_transfers     = ::Stripe::Transfer.list(param_list)
        billing_transactions = ::Billing::BillingTransaction
          .where(platform_transaction_id: stripe_transfers.data.map(&:transfer_group))

        from_transfer(stripe_transfers, billing_transactions, destination_currency)
      end

      def self.limit
        LIMIT
      end

      # Internal: Takes an array of transfers from Stripe and formats them for our usage
      # Pulls in user_login from billing_transactions, based on the transfer_group
      # information found in the Stripe transfer object.
      #
      # transfers - An Array of transfer objects from Stripe
      # billing_transactions - An Array of Billing::BillingTransaction objects
      # destination_currency - The currency of the destination account
      #
      # Returns: An Array of our internal Transfer instances
      def self.from_transfer(transfers, billing_transactions, destination_currency)
        return [] unless transfers.present?

        transfers.data.map do |transfer|
          reversal_data       = transfer.reversals.data
          billing_transaction = billing_transactions.detect do |bt|
            bt.platform_transaction_id == transfer.transfer_group
          end

          new(
            transfer_id: transfer.id,
            transfer_group: transfer.transfer_group,
            platform_url: billing_transaction&.platform_url,
            amount: ::Billing::Money.new(transfer.amount, transfer.currency),
            match_amount: ::Billing::Money.new(transfer.metadata[:match_amount].to_i, transfer.currency),
            match_amount_reversed: ::Billing::Money.new(reversal_data.sum { |data| data.metadata[:match_amount_reversed].to_i }, transfer.currency),
            number_of_reversals: reversal_data.count { |data| data.metadata[:match_amount_reversed] },
            sponsor_login: billing_transaction&.user_login,
            destination_currency: destination_currency,
            transferred_at: Time.at(transfer.created).utc.to_datetime,
          )
        end
      end
      private_class_method :from_transfer

      def initialize(args)
        @transfer_id           = args[:transfer_id]
        @transfer_group        = args[:transfer_group]
        @platform_url          = args[:platform_url]
        @amount                = args[:amount]
        @match_amount          = args[:match_amount]
        @match_amount_reversed = args[:match_amount_reversed]
        @number_of_reversals   = args[:number_of_reversals]
        @sponsor_login         = args[:sponsor_login]
        @destination_currency  = args[:destination_currency]
        @transferred_at        = args[:transferred_at]
      end

      # Public: Have we fully reversed all match on this transfer?
      #
      # Returns Boolean
      def match_fully_reversed?
        match_amount == match_amount_reversed
      end

      # Public: Have we reached the max default return of reversals?
      # Retrieving transfer records returns a default of 10 reversals. If we
      # have done 10 or more reversals for a given transfer we are at risk
      # of the match_amount_reversed being incorrect. Used to display a warning
      # in the UI.
      #
      # Returns Boolean
      def possibility_of_reversal_mismatch?
        number_of_reversals >= 10
      end

      # Public: Build the url to view this transfer in Stripe
      #
      # Returns String
      def transfer_url
        "#{GitHub.stripe_connect_dashboard_base_url}/transfers/#{transfer_id}"
      end

      private

      attr_reader :destination, :number_of_reversals
    end
  end
end
