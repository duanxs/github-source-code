# frozen_string_literal: true

module Billing
  module Stripe
    class TransferPayments
      InvalidChargeId = Class.new(StandardError)
      DEFAULT_CURRENCY = "usd"

      # Public: Transfer payments to their maintainers Stripe Connect accounts
      #
      # Returns nothing
      def self.perform(*args)
        new(*args).perform
      end

      # Public: Initialize a new TransferPayment object
      #
      # sponsor_line_items - The sponsorship line items we're transfering payment
      # for
      # zuora_payment - The original Zuora payment
      def initialize(sponsor_line_items, zuora_payment)
        @sponsor_line_items = sponsor_line_items
        @zuora_payment = zuora_payment
      end

      # Public: Transfer payments to their maintainers Stripe Connect accounts
      #
      # Returns nothing
      def perform
        update_transfer_group

        sponsorships_with_match.each do |sponsorship_with_match|
          create_stripe_transfer(sponsorship_with_match)
        end
      end

      private

      attr_reader :zuora_payment, :sponsor_line_items

      def create_stripe_transfer(sponsorship)
        ::Stripe::Transfer.create({
          amount: sponsorship.total_in_cents,
          currency: DEFAULT_CURRENCY,
          destination: destination_connect_account_id(sponsorship),
          transfer_group: transfer_group,
          metadata: {
            payment_amount: sponsorship.amount_in_cents,
            match_amount: sponsorship.match_amount_in_cents,
            stripe_charge_id: zuora_payment.reference_id,
          },
        })
      rescue ::Stripe::InvalidRequestError => e
        GitHub.dogstats.increment("stripe.transfer.failed")
        Failbot.report(e)
      rescue ::Stripe::PermissionError => e
        GitHub.dogstats.increment("stripe.permission_error", tags: ["action:transfer"])
        Failbot.report(e)
      end

      def update_transfer_group
        return unless zuora_payment.stripe?
        ::Stripe::Charge.update(zuora_payment.reference_id, {
          transfer_group: transfer_group,
        })
      rescue ::Stripe::InvalidRequestError => e
        GitHub.dogstats.increment("stripe.charge.not_found")
        raise InvalidChargeId.new(e.message)
      end

      def transfer_group
        zuora_payment.id
      end

      def sponsorships_with_match
        @_sponsorships_with_match ||= sponsor_line_items.map do |sponsorship|
          next unless destination_connect_account_id(sponsorship)
          Billing::Sponsors::SponsorshipWithMatch.new(sponsorship)
        end.compact
      end

      def destination_connect_account_id(sponsorship)
        listing = if sponsorship.subscribable.is_a?(SponsorsTier)
          sponsorship.subscribable.sponsors_listing
        elsif sponsorship.subscribable.is_a?(Marketplace::ListingPlan)
          sponsorship.subscribable.listing
        end
        listing&.stripe_connect_account&.stripe_account_id
      end
    end
  end
end
