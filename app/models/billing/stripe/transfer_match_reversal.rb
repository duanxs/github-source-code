# frozen_string_literal: true

module Billing
  module Stripe
    class TransferMatchReversal
      class DestinationMismatchError < StandardError; end
      class NotEnoughRemainingMatchError < StandardError; end
      class AlreadyPaidOutError < StandardError; end

      DESTINATION_MISMATCH_ERROR_MESSAGE = "This transfer's destination does not match the sponsorable's Stripe account."
      REMAINING_MATCH_ERROR_MESSAGE = "Entered value is greater than the remaining match amount."
      ALREADY_PAID_OUT_ERROR_MESSAGE = "This transfer has already been paid out, so matching cannot be reversed."

      # Public: Reverse all or part of a match from a previous stripe transfer
      #
      # Returns nothing
      def self.perform(**options)
        new(**options).perform
      end

      # Initialze ::Billing::Stripe::TransferMatchReversal
      #
      # stripe_account           - The Billing::StripeConnect::Account that this reversal is for
      # transfer_id              - The ID to a Stripe::Transfer object we wish to reverse match for
      # match_amount_to_reverse: - The amount of the match we will be reversing
      def initialize(stripe_account:, transfer_id:, match_amount_to_reverse:)
        @stripe_account          = stripe_account
        @transfer_id             = transfer_id
        @match_amount_to_reverse = match_amount_to_reverse
      end

      # Public: perform the match reversal
      #
      # Returns nothing
      # Raises NotEnoughRemainingMatchError if requested match reversal amount is more than the total match
      # left on the transfer
      def perform
        if destination_mismatch?
          raise DestinationMismatchError.new(DESTINATION_MISMATCH_ERROR_MESSAGE)
        elsif already_paid_out?
          raise AlreadyPaidOutError.new(ALREADY_PAID_OUT_ERROR_MESSAGE)
        elsif (transfer.match_amount_reversed + match_amount_to_reverse) > transfer.match_amount
          raise NotEnoughRemainingMatchError.new(REMAINING_MATCH_ERROR_MESSAGE)
        else
          create_transfer_reversal
        end
      end

      private

      attr_reader :stripe_account, :transfer_id, :match_amount_to_reverse

      # Internal: Retrieve the parent transfer object from Stripe
      #
      # Returns Stripe::Transfer object
      # Raises Stripe::APIError in several situations
      def stripe_transfer
        @_stripe_transfer ||= ::Stripe::Transfer.retrieve(transfer_id)
      end

      # Internal: Initialize our representation of a Stripe::Transfer
      #
      # Returns an instance of ::Billing::Stripe::Transfer
      def transfer
        return @transfer if defined?(@transfer)

        match_amount = ::Billing::Money.new(
          stripe_transfer.metadata[:match_amount].to_i,
          stripe_transfer.currency
        )

        reversal_data = stripe_transfer.reversals.data
        match_amount_reversed = ::Billing::Money.new(
          reversal_data.sum { |data| data.metadata[:match_amount_reversed].to_i },
          stripe_transfer.currency
        )

        ::Billing::Stripe::Transfer.new(
          transfer_id: stripe_transfer.id,
          match_amount: match_amount,
          match_amount_reversed: match_amount_reversed
        )
      end

      # Internal: Is this transfer already paid out to the user?
      #
      # Returns a Boolean.
      def already_paid_out?
        latest_payout = stripe_account.latest_payout
        return false if latest_payout.blank?
        latest_payout.created > stripe_transfer.created
      end

      # Internal: Does the destination of this transfer not match the account we
      #           want to reverse the match for?
      #
      # Returns a Boolean.
      def destination_mismatch?
        stripe_account.stripe_account_id != stripe_transfer.destination
      end

      # Internal: Create a transfer reversal in Stripe
      #
      # Returns nothing
      # Raises Stripe::APIError in several situations
      # Raises Stripe::InvalidRequestError if transfer is fully reversed or doesn't exist anymore
      def create_transfer_reversal
        ::Stripe::Transfer.create_reversal(
          transfer_id,
          amount: match_amount_to_reverse.fractional,
          metadata: {
            payment_amount_reversed: 0,
            match_amount_reversed: match_amount_to_reverse.fractional,
            # The two key/value pairs below do not explicitly need to be included.
            # They are listed here to be explicting about them having a `nil` value,
            # not that they were forgotten.
            stripe_refund_id: nil,
            zuora_refund_id: nil,
          },
        )
      end
    end
  end
end
