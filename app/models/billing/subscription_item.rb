# frozen_string_literal: true

module Billing
  class SubscriptionItem < ApplicationRecord::Domain::Users
    self.ignored_columns = %w(marketplace_listing_plan_id)

    areas_of_responsibility :gitcoin

    include GitHub::Relay::GlobalIdentification

    MARKETPLACE_SUBSCRIBABLE_TYPE = 0

    enum subscribable_type: {
      Marketplace::ListingPlan.name => MARKETPLACE_SUBSCRIBABLE_TYPE,
      SponsorsTier.name => 1,
    }

    belongs_to :plan_subscription
    belongs_to :subscribable, polymorphic: true
    has_one :account, through: :plan_subscription, source: :user
    has_one :integration_installation
    has_one :sponsorship, dependent: :destroy

    scope :active, -> { where("quantity > 0") }
    scope :cancelled, -> { where(quantity: 0) }
    scope :with_marketplace_listing_plans_type, -> { where(subscribable_type: Marketplace::ListingPlan.name) }

    scope :joins_marketplace_listing_plans, -> {
      with_marketplace_listing_plans_type.
      joins("JOIN marketplace_listing_plans ON marketplace_listing_plans.id = subscription_items.subscribable_id").
      annotate("cross-schema-domain-query-exempted")
    }
    scope :joins_marketplace_listings, -> {
      joins_marketplace_listing_plans.
        joins("JOIN marketplace_listings on marketplace_listings.id = marketplace_listing_plans.marketplace_listing_id")
    }

    scope :for_marketplace_listing_plans, ->(listing_plans_ids) {
      with_marketplace_listing_plans_type.with_ids(listing_plans_ids, field: "subscribable_id")
    }
    scope :for_sponsors_tiers, -> { where(subscribable_type: SponsorsTier.name) }
    scope :for_sponsors_listing, -> (listing_id) {
      for_sponsors_tiers.
        where(subscribable_id: SponsorsTier.where(sponsors_listing_id: listing_id).pluck(:id))
    }
    scope :free_trials, -> {
      where("free_trial_ends_on > ?", GitHub::Billing.today.to_s(:db))
    }

    # See https://github.com/github/sponsors/issues/353
    # After sponsors are decoupled from the marketplace, all marketplace
    # subscription items will be non_sponsorships, so we can change this to
    # use subscribable_type and remove a cross-domain join.
    scope :for_non_sponsorable_marketplace_listing, -> {
      joins_marketplace_listings.
        merge(Marketplace::Listing.non_sponsorable)
    }

    scope :not_installed, -> { where(installed_at: nil) }
    scope :purchased_before, -> (time) { where("subscription_items.created_at < ?", time) }
    scope :purchased_between, -> (start_time, end_time) { where("subscription_items.created_at BETWEEN ? AND ?", start_time, end_time) }

    validates_presence_of :plan_subscription
    validates :subscribable_id, :subscribable_type, presence: true
    validates :quantity, \
      presence: true,
      numericality: {
        only_integer: true,
        greater_than_or_equal_to: 0,
        less_than_or_equal_to: 100_000,
      }
    validate :valid_subscribable, on: :create, if: :subscribable
    validate :one_plan_per_listing
    validate :valid_subscribable_for_account_type, on: :create, if: :subscribable

    delegate \
      :monthly_price_in_cents,
      :yearly_price_in_cents,
      :base_price,
      :listing,
      to: :subscribable

    delegate \
      :slug,
      :braintree_addon_slug,
      :listable_is_sponsorable?,
      :listable_is_integration?,
      :listable,
      to: :listing

    delegate \
      :name,
      to: :listing,
      prefix: true

    delegate \
      :external_subscription,
      :plan_duration,
      :user,
      to: :plan_subscription,
      allow_nil: true

    delegate :next_billing_date, to: :external_subscription, allow_nil: true

    after_commit :synchronize_plan_subscription
    before_destroy :instrument_billable_product_removal
    after_destroy :delete_pending_changes
    after_commit :downgrade_to_free_if_nothing_billed_on_free_with_addons_plan
    before_create :set_free_trial_ends_on

    def async_listing
      async_subscribable.then do |subscribable|
        subscribable.async_listing
      end
    end

    # Public: Returns the total cost for this item.
    # trial_price - Boolean. Include discounts for free trials? Default true
    #
    # Returns a Billing::Money
    def price(duration: nil, service_remaining: 1, trial_price: true)
      return Billing::Money.new(0) if trial_price && use_free_trial_price?

      duration ||= begin
        async_plan_subscription.sync
        plan_subscription.async_user.sync if plan_subscription
        plan_duration || User::MONTHLY_PLAN
      end

      Billing::Money.new((
        base_price(duration: duration).cents *
        quantity *
        service_remaining
      ).to_i)
    end

    # Public: Returns whether the subscription item disqualifies the free trial on the listing plan
    #
    # Returns a Boolean
    def disqualifies_listing_plan_free_trial?
      paid? || on_free_trial?
    end

    # Public: Checks if the subscription item is currently on a free trial
    #
    # Returns a Boolean
    def on_free_trial?
      !!free_trial_ends_on&.future?
    end

    # Public: Checks if the given user can administer the subscription item, such as cancelling
    # or editing it.
    #
    # Returns a Boolean.
    def adminable_by?(user)
      async_adminable_by?(user).sync
    end

    def async_adminable_by?(user)
      async_account.then do |account|
        if account
          account.subscription_items_adminable_by?(user)
        else
          false
        end
      end
    end

    def cancelled?
      quantity.zero?
    end

    # Public: Checks if this item is in a state that should be billed.
    #
    # Returns a Boolean
    def billable?
      quantity > 0 && paid? && listing.billable?
    end

    def paid?
      async_subscribable.sync.paid? && !on_free_trial?
    end
    delegate :paid?, to: :subscribable, prefix: true
    alias :account_has_been_charged? :paid?

    def marketplace_line_item_description
      "#{listing_name} #{subscribable.name}"
    end

    def async_viewable_by?(viewer)
      async_plan_subscription.then do |plan_subscription|
        plan_subscription.async_user.then do |user|
          if user.id == viewer.try(:id)
            true
          elsif viewer.try(:site_admin?)
            true
          elsif user.organization? && user.billing_manager?(viewer)
            true
          elsif viewer && viewer.owned_organizations.include?(user)
            true
          else
            async_subscribable.then do |subscribable|
              subscribable.async_listing.then do |listing|
                subscribable.adminable_by?(viewer)
              end
            end
          end
        end
      end
    end

    # Public: Schedule a cancellation for the subscription item
    #
    # actor - the user requesting the cancellation
    # force - cancel the item immediately
    #
    # Returns Hash - success, subscription_item, errors
    def cancel!(actor: account, force: false)
      ::Billing::SubscriptionItemUpdater.perform \
        account: account,
        force: force,
        subscribable: subscribable,
        quantity: 0,
        sender: actor
    end

    def pending_subscription_item_change
      subscribable.pending_subscription_item_change(account: account)
    end

    def active?
      quantity > 0
    end

    def set_free_trial_ends_on
      if eligible_for_free_trial?(excluded_item: self)
        self.free_trial_ends_on = GitHub::Billing.today + ::Billing::Subscription::FREE_TRIAL_LENGTH
      end
    end

    def eligible_for_free_trial?(excluded_item: nil)
      subscribable = async_subscribable.sync
      return false unless subscribable.has_free_trial?

      account = async_account.sync
      listing = subscribable.async_listing.sync

      account_is_eligible_for_trial(account, listing, excluded_item)
    end

    def record_marketplace_installation(installed_at: nil)
      touch(:installed_at, time: installed_at)
    end

    def clear_marketplace_installation
      update(installed_at: nil)
    end

    def self.for_users_on_listable(user_ids:, listable_id:, listable_type:)
      joins_marketplace_listings
        .joins(:plan_subscription)
        .active
        .where(plan_subscriptions: { user_id: user_ids })
        .where(marketplace_listings: { listable_id: listable_id, listable_type: listable_type })
    end

    def self.for_users_on_listing_plans(user_ids:, listing_plans_ids:)
      with_marketplace_listing_plans_type
        .joins(:plan_subscription)
        .active
        .where(plan_subscriptions: { user_id: user_ids })
        .where(subscribable_id: listing_plans_ids)
    end

    private

    def use_free_trial_price?
      async_subscribable.sync.has_free_trial? && (eligible_for_free_trial? || on_free_trial?)
    end

    # Internal: Ensure a valid published subscribable is present
    def valid_subscribable
      return if subscribable.published?
      errors.add(:base, "can't have a retired listing plan")
    end

    # Internal: Ensure that we only allow
    # 1 `Billing::PlanSubscription` per `Marketplace::Listing`.
    #
    def one_plan_per_listing
      return unless plan_subscription
      return unless quantity > 0

      any_items = plan_subscription.active_subscription_items.any? do |item|
        item.id != id && item.listing == self.listing
      end

      if any_items
        errors.add(:base, "can only have 1 plan for this listing")
      end
    end

    # Internal: Queues up a SynchronizePlanSubscription job for the user that
    # owns the PlanSubscription this item belongs to.
    #
    # Returns nothing
    def synchronize_plan_subscription
      SynchronizePlanSubscriptionJob.perform_later({"user_id" => plan_subscription.user_id}) if plan_subscription
    end

    def delete_pending_changes
      user.pending_subscription_item_changes.for_marketplace_listing(listing).destroy_all
    end

    def valid_subscribable_for_account_type
      return unless account

      unless subscribable.can_subscribe_with_account?(account)
        errors.add(:base, "This plan is for #{subscribable.account_type_text} accounts only, please select a different billing account or plan.")
      end
    end

    def account_is_eligible_for_trial(account, listing, excluded_item)
      # if account is nil this is only a hypothetical item being checked on
      # order preview. This means it's automatically eligible, since the user
      # has never had an item on this listing.
      if account.nil?
        true
      else
        account.eligible_for_free_trial_on?(marketplace_listing: listing, subscription_item: excluded_item)
      end
    end

    def instrument_billable_product_removal
      GlobalInstrumenter.instrument(
        "billing.subscription_item_change",
        actor_id: GitHub.context[:actor_id],
        user_id: plan_subscription&.user_id,
        old_subscribable: subscribable,
        old_quantity: quantity,
      )
    end

    def downgrade_to_free_if_nothing_billed_on_free_with_addons_plan
      return if account.nil?
      return if !destroyed? && active?

      account.downgrade_to_free_if_nothing_billed_on_free_with_addons_plan
    end
  end
end
