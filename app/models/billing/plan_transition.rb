# rubocop:disable Style/FrozenStringLiteralComment

module Billing
  class PlanTransition
    attr_reader :old_plan, :new_plan

    def initialize(target, new_plan_name)
      @target = target
      @old_plan = target.plan
      @new_plan = GitHub::Plan.find(new_plan_name) || @old_plan
      @new_plan = MunichPlan.wrap(@new_plan, target: target)
    end

    def target_type
      @target.type
    end

    def organization?
      @target.organization?
    end

    def billing?
      @target.has_valid_payment_method?
    end

    def plan_changed?
      new_plan != old_plan
    end

    def cost_changed?
      new_plan.cost != old_plan.cost
    end

    def cost_change_label
      if new_plan.cost > old_plan.cost
        "upgrade"
      elsif new_plan.cost < old_plan.cost
        "downgrade"
      end
    end
  end
end
