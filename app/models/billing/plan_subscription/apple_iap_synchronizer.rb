# frozen_string_literal: true

require "venice"

module Billing
  class PlanSubscription::AppleIapSynchronizer
    def self.call(subscription)
      new(subscription).call
    end

    def initialize(subscription)
      @subscription = subscription
      @user = subscription.user
    end

    def call
      if receipt = Venice::Receipt.verify(@subscription.apple_receipt_id, { shared_secret: GitHub.apple_iap_shared_secret })
        # For receipts containing auto-renewable subscriptions, check the value of the
        # responseBody.Latest_receipt_info key of the response to get the status of the
        # most recent renewal
        # https://developer.apple.com/documentation/appstorereceipts/responsebody/receipt/in_app
        original_json = receipt.original_json_response
        expires_date_ms = original_json.dig("latest_receipt_info", 0, "expires_date_ms")
        cancellation_date_ms = original_json.dig("latest_receipt_info", 0, "cancellation_date_ms")
        cancellable = cancellation_date_ms || (Time.zone.at(expires_date_ms.to_i) < Time.zone.now)

        if cancellable
          result = ::Billing::ChangeSubscription.perform(@user, actor: @user, plan: GitHub::Plan.free.to_s)

          if result.success?
            @subscription.update!(apple_receipt_id: nil, apple_transaction_id: nil)
          else
            @error_message = result.error_message
          end
        end
      else
        @subscription.update!(apple_receipt_id: nil, apple_transaction_id: nil)
        @error_message = "Apple receipt is not valid."
      end

      self
    end

    def success?
      @error_message.blank?
    end

    def error_message
      @error_message
    end
  end
end
