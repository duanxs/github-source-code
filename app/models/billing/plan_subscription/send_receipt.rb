# frozen_string_literal: true

class Billing::PlanSubscription::SendReceipt
  def self.perform(*args, **options); new(*args, **options).perform; end

  def initialize(plan_subscription, billing_transaction:)
    @plan_subscription   = plan_subscription
    @billing_transaction = billing_transaction
  end

  def perform
    BillingNotificationsMailer.receipt(
      plan_subscription.user,
      billing_transaction,
    ).deliver_later

    BillingNotificationsMailer.receipt_bcc(
      plan_subscription.user,
      billing_transaction,
    ).deliver_later if BillingNotificationsMailer.send_receipt_bcc?
  end

  private

  attr_reader :plan_subscription, :billing_transaction
end
