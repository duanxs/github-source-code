# rubocop:disable Style/FrozenStringLiteralComment

module Billing
  class PlanSubscription::Synchronizer
    class ExternalSubscriptionCancellationError < StandardError; end

    def self.for_plan_subscription(plan_subscription, set_billing_date_today = true)
      Billing::PlanSubscription::ZuoraSynchronizer.new(plan_subscription, set_billing_date_today)
    end

    def self.update(*args); for_plan_subscription(*args).update; end
    def self.cancel(*args); for_plan_subscription(*args).cancel; end
    def self.create(*args); for_plan_subscription(*args).create; end

    # Public: Cancel for a downgrade to free. Generally for internal use, but
    # can be used for user deletes which fully cancel associated plans.
    #
    # Return GitHub::Billing::Result on external subscription cancellation
    def cancel_for_downgrade_to_free
      result = plan_subscription.cancel_external_subscription
      if result.success?
        user.remove_all_payment_methods(user)
        user.reset_data_packs
        user.reset_billing_attempts
        user.enable_or_disable!
        user.reset_billed_on
      else
        Failbot.report(
          ExternalSubscriptionCancellationError.new("external subscription failed while downgrading to free"),
          user: user,
          message: result.error_message,
        )
      end
      result
    end
    protected

    def build_plan_change(plan: GitHub::Plan.free, seats: 0, asset_packs: 0, subscription_items: [])
      old_subscription = Billing::Subscription.for_account user,
          plan: plan,
          seats: seats,
          asset_packs: asset_packs,
          subscription_items: subscription_items

      Billing::PlanChange.new old_subscription, user.subscription
    end
  end
end
