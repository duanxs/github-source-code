# frozen_string_literal: true

class Billing::PlanSubscription::ZuoraSubscriptionParams
  BUSINESS_PLAN_ASSUMED_MINIMUM = 5
  attr_reader :plan_subscription, :user

  delegate \
    :active_subscription_items,
    :customer,
    :coupon,
    :data_packs,
    :on_free_trial?,
    :plan,
    :plan_duration,
    :plan_duration_in_months,
    :zuora_subscription_number,
    to: :plan_subscription

  delegate \
    :charged_through_date,
    to: :zuora_subscription

  delegate \
    :zuora_account_id,
    :zuora_account_number,
    to: :customer

  def initialize(plan_subscription:)
    @plan_subscription = plan_subscription
    @user = plan_subscription.user
    @pricing = Billing::Pricing.new(plan_subscription: plan_subscription)
  end

  # Public: Generates the hash sent to Zuora to create the user's subscription, which includes the product
  # rate plans they're subscribing to, as well as the default processing options
  #
  # Returns Hash
  def create_params
    {
      accountKey: zuora_account_id,
      contractEffectiveDate: contract_effective_date,
      termType: "EVERGREEN",
      subscribeToRatePlans: rate_plans,
      runBilling: true,
      collect: collect_invoice?(rate_plans: rate_plans),
    }
  end


  # Public: Generates the hash sent to Zuora to update the user's subscription
  #
  # Handles prorated upgrades/downgrades by setting contractEffectiveDate on product changes
  # We're using invoiceCollect since we're on an older version of the API, but that flag has been replaced by
  # collect and runBilling on future versions
  #
  # Returns Hash
  def update_params
    {
      applyCreditBalance: true,
      runBilling: true,
      collect: collect_invoice?(rate_plans: addition_rate_plans + updated_rate_plans),
      update: updates,
      remove: removals,
      add: additions,
    }
  end

  # Public: Generates the hash sent to Zuora to cancel the user's subscription
  #
  # Returns Hash
  def cancel_params
    {
      cancellationPolicy: "SpecificDate",
      cancellationEffectiveDate: charged_through_date.to_s,
      invoiceCollect: false,
    }
  end

  # Public: Generates an array of product details for a given user, which includes their GitHub and
  # Marketplace purchases
  #
  # Returns Array
  def rate_plans
    (github_rate_plans + marketplace_rate_plans).compact
  end

  # Public: Is the user's plan duration different than their Zuora subscription?
  #
  # Returns Boolean
  def changing_duration?
    plan_duration != zuora_subscription.plan_duration
  end

  def github_codespaces_rate_plan
    return unless plan.codespaces_eligible?
    return unless user.billing_github_codespaces_enabled?

    rate_plan = Billing::Codespaces::RatePlan.new(user: user)

    rate_plan.to_zuora_subscription_params
  end

  def github_package_registry_rate_plan
    return unless plan.package_registry_eligible?
    uuid = Billing::PackageRegistry::ZuoraProduct.uuid

    {
      productRatePlanId: uuid.zuora_product_rate_plan_id,
      chargeOverrides: [{
        productRatePlanChargeId: uuid.zuora_product_rate_plan_charge_ids[:bandwidth],
        includedUnits: plan.package_registry_included_bandwidth,
      }],
    }
  end

  # Public: Generates the actions rate plan for zuora
  #
  # Returns Boolean
  def github_actions_rate_plan
    return unless plan.actions_eligible?

    private_rate_plan_charge = Billing::Actions::ZuoraProduct::PRIVATE_VISIBILITY_RATE_PLAN_CHARGE
    {
      productRatePlanId: private_rate_plan_charge.zuora_id,
      chargeOverrides: [{
        productRatePlanChargeId: private_rate_plan_charge.zuora_charge_ids[:usage],
        includedUnits: plan.actions_included_private_minutes,
      }],
    }
  end

  # Public: Generates the shared storage rate plan for Zuora
  def github_shared_storage_rate_plan
    return unless plan.shared_storage_eligible?
    uuid = ::Billing::SharedStorage::ZuoraProduct.uuid
    calculator = ::Billing::MeteredBilling::HourlyRateCalculator.new

    {
      productRatePlanId: uuid.zuora_product_rate_plan_id,
      chargeOverrides: [{
        productRatePlanChargeId: uuid.zuora_product_rate_plan_charge_ids[:usage],
        includedUnits: calculator.hourly_rate_for(units_per_month: plan.shared_storage_included_megabytes).round,
      }],
    }
  end

  private

  def convert_to_megabytes(bytes)
    bytes.to_f / Numeric::MEGABYTE
  end

  attr_reader :pricing

  def discount_amount
    if coupon.percentage?
      # A discountPercentage value should be represented as a percentage
      # rather than a decimal (e.g 50 instead of 0.5 or 50%)
      coupon.discount * 100
    else
      pricing.discount.to_f
    end
  end

  def github_rate_plans
    rate_plans = [
      github_rate_plan,
      github_lfs_plan,
      github_coupon,
      github_actions_rate_plan,
      github_package_registry_rate_plan,
      github_shared_storage_rate_plan,
      github_codespaces_rate_plan
    ]

    rate_plans
  end

  def github_rate_plan
    return if plan.cost.zero? || on_free_trial?
    {
      productRatePlanId: plan.zuora_id(cycle: plan_duration),
      chargeOverrides: github_charge_overrides,
    }
  end

  # Should Zuora collect payment for the invoice immediately.
  # Payments are collected immediately if there are any Marketplace items added or updated
  #
  # rate_plans - The Array of rate plans to check for non GitHub rate plans inclusion
  #
  # Returns Boolean
  def collect_invoice?(rate_plans:)
    (rate_plans - github_rate_plans).present?
  end

  def updates
    updated_rate_plans.map do |updated_plan|
      rate_plan = rate_plan_for_product_rate_plan_id(updated_plan[:productRatePlanId])
      charge_updates = updated_plan[:chargeOverrides].each do |override|
        charge = charge_for_charge_id(rate_plan, override.delete(:productRatePlanChargeId))
        override[:ratePlanChargeId] = charge[:id]
      end
      {
        contractEffectiveDate: trigger_date,
        ratePlanId: rate_plan[:id],
        chargeUpdateDetails: charge_updates,
      }
    end
  end

  def update_actions
    (additions + removals + updates).compact
  end

  def additions
    addition_rate_plans.map do |rate_plan|
      rate_plan.tap { |plan| plan[:contractEffectiveDate] = trigger_date }
    end
  end

  def removals
    removed_rate_plan_ids.map do |removed_plan_id|
      rate_plan = rate_plan_for_product_rate_plan_id(removed_plan_id)
      {
        contractEffectiveDate: trigger_date,
        ratePlanId: rate_plan[:id],
      }
    end
  end

  def updated_rate_plans
    plans = per_unit_rate_plans - discount_rate_plans

    plans.select do |rate_plan|
      existing_rate_plans.any? do |existing_rate_plan|
        existing_rate_plan[:productRatePlanId] == rate_plan[:productRatePlanId] &&
          has_updated_charge_details?(rate_plan, existing_rate_plan)
      end
    end
  end

  def upgrading_plan?
    return false if changing_duration?

    increasing_payment_amount? || switching_to_per_seat?
  end

  def removed_rate_plan_ids
    removals = existing_rate_plan_ids.reject { |id| current_rate_plan_ids.include?(id) }

    if coupon
      updated_discount_rate_plans.each do |plan|
        if current_rate_plan_ids.include?(plan[:productRatePlanId])
          removals << plan[:productRatePlanId]
        end
      end
    end

    removals.flatten
  end

  def addition_rate_plans
    additions = rate_plans.reject do |plan|
      existing_rate_plan_ids.include?(plan[:productRatePlanId])
    end

    if coupon && updated_discount_rate_plans
      additions << updated_discount_rate_plans
    end

    additions.flatten
  end

  def discount_rate_plans
    rate_plans.select do |rate_plan|
      @discount_ids ||= ::Billing::ProductUUID.discounts.pluck(:zuora_product_rate_plan_id)
      @discount_ids.include?(rate_plan[:productRatePlanId])
    end
  end

  def per_unit_rate_plans
    rate_plans.select { |rate_plan| rate_plan[:chargeOverrides].any? }
  end

  def has_updated_discount_charge_details?(rate_plan, existing_rate_plan)
    charge = rate_plan[:chargeOverrides].first
    existing_rate_plan[:ratePlanCharges].any? do |existing_charge|
      existing_charge[:productRatePlanChargeId] == charge[:productRatePlanChargeId] &&
      discount_changed?(existing_charge, charge)
    end
  end

  def switching_to_per_seat?
    plan.per_seat? && zuora_subscription.plan&.per_repository?
  end

  def increasing_payment_amount?
    payment_amount >= zuora_subscription.payment_amount(plan_duration_in_months: plan_duration_in_months)
  end

  def updated_discount_rate_plans
    discount_rate_plans.select do |rate_plan|
      existing_rate_plans.any? do |existing_rate_plan|
        existing_rate_plan[:productRatePlanId] == rate_plan[:productRatePlanId] &&
          has_updated_discount_charge_details?(rate_plan, existing_rate_plan)
      end
    end
  end

  def has_updated_charge_details?(rate_plan, existing_rate_plan)
    rate_plan[:chargeOverrides].any? do |charge|
      existing_rate_plan[:ratePlanCharges].any? do |existing_charge|
        existing_charge[:productRatePlanChargeId] == charge[:productRatePlanChargeId] &&
          (
            quantity_changed?(existing_charge, charge) ||
            included_units_changed?(existing_charge, charge)
        )
      end
    end
  end

  def rate_plan_for_product_rate_plan_id(product_rate_plan_id)
    existing_rate_plans.detect do |existing_rate_plan|
      existing_rate_plan[:productRatePlanId] == product_rate_plan_id
    end
  end

  def charge_for_charge_id(rate_plan, charge_id)
    rate_plan[:ratePlanCharges].detect do |charge|
      charge[:productRatePlanChargeId] == charge_id
    end
  end

  def trigger_dates
    ["ContractEffective", "ServiceActivation", "CustomerAcceptance"].map do |trigger_type|
      {
        name: trigger_type,
        triggerDate: trigger_date.to_s,
      }
    end
  end

  def trigger_date
    upgrading_plan? ? today : charged_through_date
  end

  def included_units_changed?(existing_charge, charge)
    if GitHub.flipper[:billing_skip_override_munich_included_minutes_on_team].enabled?(user)
      # TODO: Remove this after June 14th since all users should be past the munich eligibility date
      # Remove this along with the checks in MunichPlan actions_included_private_minutes
      actions_rate_plan_charge = Billing::Actions::ZuoraProduct::PRIVATE_VISIBILITY_RATE_PLAN_CHARGE
      if actions_rate_plan_charge.zuora_charge_ids[:usage] == existing_charge[:productRatePlanChargeId]
        if existing_charge[:includedUnits].to_i == 3000 && plan.business?
          return false
        end
      end
      # TODO END
    end

    existing_charge[:includedUnits].to_i != charge[:includedUnits].to_i
  end

  def quantity_changed?(existing_charge, charge)
    existing_charge[:quantity].to_i != charge[:quantity].to_i
  end

  def discount_changed?(existing_charge, charge)
    return false unless coupon
    existing_charge[discount_charge_override_type] != charge[discount_charge_override_type]
  end

  def marketplace_overrides_for(item)
    if item.subscribable.per_unit?
      [{
        productRatePlanChargeId: item.subscribable.zuora_charge_ids(cycle: plan_duration)[:unit],
        quantity: item.quantity,
      }]
    else
      []
    end
  end

  def github_charge_overrides
    overrides = if plan.per_seat?
      [{
        productRatePlanChargeId: plan.zuora_charge_ids(cycle: plan_duration)[:unit],
        quantity:  github_additional_seat_quantity,
      }]
    else
      []
    end

    if plan_subscription.apple_iap_subscription? && plan.pro?
      overrides.push({
        # pro does not support seats so use :flat
        productRatePlanChargeId: plan.zuora_charge_ids(cycle: plan_duration)[:flat],
        price: 0
      })
    end

    if plan.business? && plan_subscription.seats < BUSINESS_PLAN_ASSUMED_MINIMUM
      overrides.push({
        productRatePlanChargeId: plan.zuora_charge_ids(cycle: plan_duration)[:base_unit],
        quantity: plan_subscription.seats,
      })
    elsif plan.business? && plan_subscription.seats >= BUSINESS_PLAN_ASSUMED_MINIMUM
      overrides.push({
        productRatePlanChargeId: plan.zuora_charge_ids(cycle: plan_duration)[:base_unit],
        quantity: BUSINESS_PLAN_ASSUMED_MINIMUM,
      })
    end

    overrides
  end

  def discount_overrides
    [{
      :productRatePlanChargeId => coupon.zuora_charge_ids(cycle: plan_duration)[coupon.zuora_charge_type],
      discount_charge_override_type => discount_amount,
    }]
  end

  def github_additional_seat_quantity
    if plan.business?
      # Hack to assume organizations on team plan ALWAYS have 5 seats as the default quantity
      # see https://github.com/github/gitcoin/issues/4241
      return [plan_subscription.seats - BUSINESS_PLAN_ASSUMED_MINIMUM, 0].max
    end

    if plan.base_cost?
      [plan_subscription.seats - plan.base_units, 0].max
    else
      plan_subscription.seats
    end
  end

  def github_lfs_plan
    return unless data_packs > 0
    {
      productRatePlanId: Asset::Status.zuora_id(cycle: plan_duration),
      chargeOverrides: [{
        productRatePlanChargeId: Asset::Status.zuora_charge_ids(cycle: plan_duration)[:unit],
        quantity: data_packs,
      }],
    }
  end

  def github_coupon
    return unless coupon
    {
      productRatePlanId: coupon.zuora_id(cycle: plan_duration),
      chargeOverrides: discount_overrides,
    }
  end

  def discount_charge_override_type
    coupon.percentage? ? :discountPercentage: :discountAmount
  end

  def marketplace_rate_plans
    active_subscription_items.select(&:billable?).map do |item|
      {
        productRatePlanId: item.subscribable.zuora_id(cycle: plan_duration),
        chargeOverrides: marketplace_overrides_for(item),
      }
    end
  end

  # Internal: The Zuora subscription tied to this user's account
  #
  # Returns Billing::ZuoraSubscription
  def zuora_subscription
    @_zuora_subscription ||= plan_subscription.zuora_subscription
  end

  def payment_amount
    @_payment_amount ||= Billing::Money.new(plan_subscription.payment_amount * 100)
  end

  def current_rate_plan_ids
    @_current_rate_plan_ids = rate_plans.map { |plan| plan[:productRatePlanId] }
  end

  def existing_rate_plan_ids
    @_existing_rate_plan_ids ||= existing_rate_plans.map { |plan| plan[:productRatePlanId] }
  end

  def existing_rate_plans
    @_existing_rate_plans ||= zuora_subscription.active_rate_plans
  end

  def today
    @_today ||= GitHub::Billing.today.to_s
  end
  alias_method :order_date, :today
  alias_method :contract_effective_date, :today
  alias_method :subscription_start_date, :today
end
