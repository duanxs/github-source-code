# rubocop:disable Style/FrozenStringLiteralComment

class Billing::PlanSubscription::UpdateBillingDate
  def self.perform(*args); new(*args).perform; end

  attr_reader :user, :old_billing_date, :new_billing_date, :plan_subscription, :reason

  def initialize(user, billing_date, reason: nil)
    @user              = user
    @plan_subscription = user.plan_subscription
    @old_billing_date  = @user.billed_on
    @new_billing_date  = billing_date
    @reason            = reason
  end

  # Update the billing date for the user's plan subscription. At any point that it fails,
  # it will rollback the changes so we can still keep a plan subscription associated with it.
  #
  # Returns a Billing::PlanSubscription
  def perform
    return unless plan_subscription
    Billing::PlanSubscription.transaction do
      begin
        plan_subscription.cancel_external_subscription

        user.billed_on = new_billing_date
        user.save!

        if user.zuora_account?
          result = user.zuora_account.update!({
            BcdSettingOption: "ManualSet",
            BillCycleDay: new_billing_date.day,
          }).first
          unless result["Success"]
            user.errors.add(:billed_on, "unable to change bill cycle day #{result}")
            raise ActiveRecord::RecordNotSaved, "Unable to change bill cycle day #{result}"
          end
        end

        plan_subscription.reload.synchronize(set_billing_date_today: false)
        if plan_subscription.errors.any?
          raise ActiveRecord::RecordNotSaved.new("Failed to synchronize subscription")
        end

        log_billing_date_change
        plan_subscription

      # NB - We don't `create!` earlier because the actual Braintree/Zuora error
      # is being swallowed up and stored on the error hash. This allows us to keep the attempted
      # record that is created and re-raise with the error message and put the transaction into a
      # rollback.
      rescue ActiveRecord::RecordNotSaved
        if plan_subscription
          if GitHub.rails_6_0?
            error_names = plan_subscription.errors.keys
          else
            error_names = plan_subscription.errors.attribute_names
          end

          raise ActiveRecord::RecordNotSaved.new("Unable to save plan subscription with invalid #{error_names}")
        else
          if GitHub.rails_6_0?
            error_names = user.errors.keys
          else
            error_names = user.errors.attribute_names
          end

          raise ActiveRecord::RecordNotSaved.new("Unable to save user with invalid #{error_names}")
        end
      end
    end
  end

  private

  def log_billing_date_change
    user_type = user.organization? ? :org : :user

    GitHub.instrument "account.billing_date_change", \
      user_type => user,
      :reason   => reason,
      :old_date => old_billing_date,
      :date     => new_billing_date
  end
end
