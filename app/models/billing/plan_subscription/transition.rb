# rubocop:disable Style/FrozenStringLiteralComment

class Billing::PlanSubscription::Transition
  def self.activate(*args, force: false); new(*args).activate(force: force); end

  def initialize(user)
    @user = user
  end

  # Public: Transitions an account to an external subscription
  #
  # force - Whether or not to disregard all guards and force activaton
  #         (default: false)
  #
  # Returns a PlanSubscription if successful, nil otherwise.
  def activate(force: false)
    GitHub::Logger.log \
      at: "billing.subscription.transition",
      billed_on: user.billed_on,
      billing_type: user.billing_type,
      login: user.login,
      plan: user.plan,
      plan_duration: user.plan_duration,
      zuora_subscription_number: user.plan_subscription&.zuora_subscription_number

    # These are also checked during synchronization, so we shouldn't bother to
    # enqueue a synchronization job that will noop
    return if user.invoiced?
    return if user.payment_method.blank?

    # We normally want to apply these guards, but in some cases we may wish to
    # transition a user anyway
    unless force
      return if user.monthly_plan? && user.billed_on && user.billed_on > GitHub::Billing.today
      return user.plan_subscription if user.external_subscription?
    end

    if user.plan_subscription
      synchronize_plan_subscription
    else
      create_plan_subscription
    end
  end

  def error_message
    @error.present? ? GitHub::Billing::ErrorMessage.new(@error).to_s : ""
  end

  private

  attr_reader :user

  def create_plan_subscription
    # N.B This will kick off a synchronize job in PlanSubscription#after_create
    user.create_plan_subscription!(customer: user.customer)
  end

  def synchronize_plan_subscription
    SynchronizePlanSubscriptionJob.perform_later({"user_id" => user.id})
    user.plan_subscription
  end
end
