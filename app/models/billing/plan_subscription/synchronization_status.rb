# frozen_string_literal: true

module Billing
  module PlanSubscription::SynchronizationStatus
    def fetched_external_subscription
      @fetched_external_subscription ||= external_subscription
    end

    # Public: Is this PlanSubscription synchronized with its counterpart on
    #         Zuora?
    #
    # Returns Boolean synchronization status
    def synchronized?
      return false unless fetched_external_subscription

      plan_synchronized? &&
      plan_duration_synchronized? &&
      seats_synchronized? &&
      asset_packs_synchronized? &&
      discount_synchronized? &&
      balance_synchronized? &&
      next_billing_date_synchronized?
    end

    def plan_synchronized?(subscription: fetched_external_subscription)
      if zuora_subscription? && subscription.plan.nil?
        plan.free? || plan.free_with_addons?
      else
        plan == subscription.plan
      end
    end

    def plan_duration_synchronized?(subscription: fetched_external_subscription)
      plan_duration == subscription.plan_duration
    end

    def seats_synchronized?(subscription: fetched_external_subscription)
      return true unless plan.per_seat?

      seat_count == subscription.seats
    end

    def seat_count
      plan.business? ? additional_seats : seats
    end

    def asset_packs_synchronized?(subscription: fetched_external_subscription)
      data_packs == subscription.data_packs
    end

    def discount_synchronized?(subscription: fetched_external_subscription)
      discount.to_d == subscription.discount.to_d
    end

    def balance_synchronized?(subscription: fetched_external_subscription)
      #Ensure both sides are BigDecimal because normally balance will be BigDecimal
      balance&.to_d == subscription.balance&.to_d
    end

    def next_billing_date_synchronized?(subscription: fetched_external_subscription)
      billed_on == subscription.next_billing_date
    end
  end
end
