# frozen_string_literal: true

module Billing
  class ActionsPermission
    REASON_MESSAGES = {
      plan_ineligible: "Legacy billing plans can't use GitHub Actions",
      disabled: "Account must be enabled to use the GitHub Actions",
      trade_restricted_organization: TradeControls::Notices::Plaintext.org_restricted,
    }.freeze

    def initialize(owner)
      if owner.try(:delegate_billing_to_business?)
        @owner = owner.business
      else
        @owner = owner
      end
    end

    def allowed?(public: false)
      return true if public && !metered_billing_permission.fully_trade_restricted_organization_owner?
      status[:allowed]
    end

    def status
      @_status ||= build_status
    end

    def usage_allowed?(public:)
      return true unless GitHub.billing_enabled? # Skip billing checks if billing is disabled.
      return false if metered_billing_permission.trade_controls_apply?(public: public)

      return true if public
      return false if !public && !owner.plan.actions_eligible?
      return true unless used_all_included_private_minutes?
      return false if metered_billing_permission.paid_overages_restricted_by_owner_payment_issue?
      return true if owner.skip_metered_billing_permission_check_for?(product: :actions)

      metered_billing_permission.usage_allowed?
    end

    def storage_allowed?(public:)
      return true unless GitHub.billing_enabled? # Skip billing checks if billing is disabled.
      return false if metered_billing_permission.trade_controls_apply?(public: public)
      return true if public
      return true unless used_all_included_private_shared_storage?
      return false if metered_billing_permission.paid_overages_restricted_by_owner_payment_issue?
      return true if owner.skip_metered_billing_permission_check_for?(product: :storage)

      metered_billing_permission.usage_allowed?
    end

    private

    attr_reader :owner

    def used_all_included_private_minutes?
      actions_usage.included_private_minutes_used >= actions_usage.included_private_minutes
    end

    def build_status
      return allowed_status unless GitHub.billing_enabled? # Skip billing checks if billing is disabled.

      if metered_billing_permission.fully_trade_restricted_organization_owner?
        not_allowed_status(:trade_restricted_organization)
      elsif !owner.plan.actions_eligible?
        not_allowed_status(:plan_ineligible)
      elsif owner.disabled?
        not_allowed_status(:disabled)
      else
        allowed_status
      end
    end

    def not_allowed_status(reason)
      {
        allowed: false,
        error: {
          reason: reason.to_s.upcase,
          message: REASON_MESSAGES[reason],
        },
      }
    end

    def allowed_status
      { allowed: true, error: {} }
    end

    def used_all_included_private_shared_storage?
     shared_storage_usage.estimated_monthly_private_megabytes >= shared_storage_usage.plan_included_megabytes
    end

    def shared_storage_usage
      @_shared_storage_usage ||= Billing::SharedStorageUsage.new(owner)
    end

    def metered_billing_permission
      @_metered_billing_permission ||= Billing::MeteredBillingPermission.new(owner)
    end

    def actions_usage
      @_actions_usage ||= Billing::ActionsUsage.new(owner)
    end
  end
end
