# rubocop:disable Style/FrozenStringLiteralComment

module Billing
  # A denormalized record of monetary transactions (sales or refunds) kept for:
  # - Reporting purposes (reconcile against Braintree/Zuora data).
  # - To show payment history to customers and GitHub staff.
  # - To render customer receipts.
  #
  # BillingTransactions are created:
  # - When purchasing GitHub for the first time
  # - On the automated recurring charge for plan renewal
  # - When refunding another BillingTransaction (self referential)
  # - When making a mid billing cycle, prorated purchase:
  #   - Plan upgrade on yearly accounts
  #   - Seat purchase
  #   - Asset pack purchase
  #
  # NOTES:
  #
  # - transaction_id is the external alphanumeric id for a transaction.
  #
  # - We always record a BillingTransaction, even if the customer has a coupon
  # that fully covers the amount owed. These are called zero charge
  # transactions.
  class BillingTransaction < ApplicationRecord::Domain::Users
    areas_of_responsibility :gitcoin

    MARKETPLACE_REVENUE_CUT = 0.25

    include GitHub::Billing::CreditCard,
            GitHub::Relay::GlobalIdentification

    enum last_status: Billing::BillingTransactionStatuses::ALL

    # Public: String external alphanumeric transaction identifier.
    #
    # NB: Not used to associate Transactions.
    # column :transaction_id
    validates_presence_of :transaction_id, if: :transaction_id_required?
    validates_uniqueness_of :transaction_id, allow_nil: true, case_sensitive: true

    # Public: User/Org this transaction belongs to.
    # column :user_id
    belongs_to :live_user, class_name: "User", foreign_key: :user_id
    validates_presence_of :user_id

    # Public: String transaction type. One of the following values:
    #   signed-up                 - New user signs up for paid plan
    #   first-time-paid-upgrade   - Existing user pays for an upgrade
    #   recurring-charge          - Monthly/Yearly automated charge
    #   prorate-charge            - Prorated charge for yearly upgrade
    #   prorate-seat-charge       - Prorated charge for seat purchase
    #   prorate-asset-pack-charge - Prorated charge for asset pack purchase
    #   refund                    - Refund
    #
    # column :transaction_type
    validates_presence_of :transaction_type

    # Public: Integer total amount in cents of this transaction.
    # column :amount_in_cents
    validates_presence_of :amount_in_cents

    # Public: String email address where their receipt was sent. Useful for
    # looking up the receipt in logs@github.com even if the user has been
    # deleted. It may be possible to also use user_login for this purpose.
    # column :billing_email_address

    # Public: String country name (needed for reporting and tax reasons).
    # column :country

    # Public: String region name  (needed for reporting and tax reasons).
    # column :region

    # Public: String postal code (needed for reporting and tax reasons).
    # column :postal_code

    # Public: String whether this transaction is for an "organization" or a "user".
    # column :user_type

    # Public: String login of the user or organization.
    # column :user_login

    # Public: Datetime timestamp the user created their account.
    # column :user_created_at

    # Public: String name of the plan this transaction is for.
    # column :plan_name

    # Public: Integer plan price in cents.
    # column :plan_price_in_cents

    enum renewal_frequency: { monthly: 0, yearly: 1 }

    # Public: String name of the coupon if one was used.
    # column :coupon_name

    # Public: Integer amount of the discount in cents if a coupon was used.
    # column :discount_in_cents

    enum platform: { legacy_braintree: 0, braintree: 1, zuora: 2 }
    validates :platform, presence: true

    PRODUCTS = %w(dotcom job_posting).freeze

    # Public: string product. "dotcom" for most transactions, "job_posting" for
    # job posting credit purchases. Nil results in "dotcom".
    # column :product
    validates_inclusion_of :product, in: PRODUCTS, allow_nil: true

    # Public: Datetime the service this transaction pays for will end.
    # column :service_ends_at

    # Public: BillingTransaction that refunds this transaction, if any
    has_one :refund,
      -> { where("sale_transaction_id IS NOT NULL") },
      class_name: "Billing::BillingTransaction",
      primary_key: :transaction_id,
      foreign_key: :sale_transaction_id

    # Public: BillingTransaction that this transaction refunds, if any
    belongs_to :sale, class_name: "Billing::BillingTransaction",
      primary_key: :transaction_id,
      foreign_key: :sale_transaction_id

    has_many :line_items, dependent: :destroy

    has_many :disputes, class_name: "Billing::Dispute"

    # Public String country of issuance of the payment method.
    # column :country_of_issuance

    # Public: The bank identification number for the credit card used.
    # column :bank_identification_number

    # Public: String settlement batch id from external processor.
    # column :settlement_batch_id

    # Public: The last four digits of the credit card used.
    # column :last_four

    # Public: Email of the paypal account, if applicable.
    # column :paypal_email

    enum payment_type: {
      credit_card: 0,
      paypal: 1,
      no_charge: 2, # Log things like 100% coupons that have $0 charges.
    }

    # Public: Integer delta seats this org added/removed as part of this
    # transaction.
    # column :seats_delta

    # Public: Integer total seats this org owns as part of this transaction.
    # column :seats_total

    # Public: Integer delta asset packs this user/org added/removed as part of
    # this transaction.
    # column :asset_packs_delta

    # Public: Integer total asset packs this user/org owns as part of this
    # transaction.
    # column :asset_packs_total

    # Public: Integer unit price in cents of a single asset pack at the time of
    # this transaction.
    # column :asset_pack_unit_price_in_cents

    # Public: Integer number of days this transaction amount was prorated for.
    # column :prorated_days

    # Public: Billing transaction statuses.
    has_many :statuses, class_name: "Billing::BillingTransactionStatus"

    # Public: Billing transaction notes.
    has_many :notes, class_name: "Billing::BillingTransactionNote"

    # Public: Scope to find transactions needing status updates
    scope :pending_status_updates, -> {
      where(
        "last_status IS NULL OR last_status NOT IN (?) AND transaction_id IS NOT NULL",
        Billing::BillingTransactionStatuses::FINAL.values,
      )
    }

    # Billing transactions that apply to the current period
    scope :current, -> { where("service_ends_at >= ?", GitHub::Billing.today) }

    # Sales only scope
    scope :sales, -> {
      where.not(transaction_type: "refund").
        where.not(payment_type: Billing::BillingTransaction.payment_types[:no_charge])
    }

    # Return in descending order
    scope :descending, -> { order("created_at DESC") }

    # Returns a BillingTransaction or nil
    scope :first_time_charge, -> {
      where(
        transaction_type: "first-time-paid-upgrade",
        last_status: Billing::BillingTransactionStatuses::SUCCESS.values,
      )
    }

    # Public: Updates records that are not yet in a "final" status
    def self.update_transactions_with_pending_status
      GitHub.dogstats.count \
        "billing_transaction.pending_status_updates",
        pending_status_updates.count

      pending_status_updates.find_each do |transaction|
        UpdateBillingTransactionStatusJob.perform_later(transaction)
      end
    end

    # Public: All refundable sales for the current billing cycle
    #
    # Returns an array of BillingTransaction
    def self.refundable_sales
      current.sales.descending.select(&:refundable?)
    end

    # Public: Log a Zuora initiatied charge.
    #
    # user              - The User/Organization to log this transaction for.
    # invoiced_item     - Array of items that we charged for in this transaction
    # charge_type       - String transaction charge type (optional, default:
    #                     "recurring-charge"). Should be one of:
    #                       "recurring-charge"          - Monthly/Yearly automated charge
    #                       "prorate-seat-charge"       - Prorated charge for seat purchase
    #                       "prorate-asset-pack-charge" - Prorated charge for asset pack purchase
    def log_recurring_charge(user:, invoiced_items: [], charge_type: nil)
      copy_user_details(user, charge_type: charge_type)
      save!
      create_recurring_line_items(invoiced_items: invoiced_items)
      self
    end

    # Public: Log a $0 billing transaction
    #
    # user       - The User/Organization to log this transaction for.
    # created_at - DateTime when this transaction happened (defaults to now)
    # dry_run    - If true, does not save the transaction (defaults to false)
    #
    # Returns the billing transaction.
    def log_zero_charge(user:, created_at: nil, dry_run: false)
      copy_user_details(user)
      copy_historical_information(created_at) if created_at

      unless dry_run
        save!
        # NB - Save and then update b/c we need to generate transaction_id from id
        # for uniqueness (see #our_transaction_id).
        update!(
          transaction_id: our_transaction_id,
          last_status: :settled,
        )
      end

      self
    end

    def recalculate_historical_information(dry_run: false)
      raise ArgumentError, "must be a $0 transation" unless self.amount_in_cents.zero?

      self.copy_historical_information(self.created_at)

      unless dry_run
        save!
      end

      self
    end

    # Deprecated: Log a new BillingTransaction.
    #
    # attrs - Hash of attributes to initialize BillingTransaction with.
    #
    # Returns a new BillingTransaction
    def self.log(attrs)
      billing_transaction = new(attrs)

      if user = billing_transaction.user
        billing_transaction.plan_name = user.plan.try(:name)
        billing_transaction.plan_price_in_cents = user.plan.try(:cost_in_cents)
        billing_transaction.arr_in_cents = billing_transaction.calculated_arr_in_cents
        billing_transaction.seats_total = user.seats
        billing_transaction.renewal_frequency = user.yearly_plan? ? :yearly : :monthly

        if user.coupon
          billing_transaction.coupon_name = user.coupon.code
          billing_transaction.discount_in_cents ||= (user.discount * 100).to_i
        end

        billing_transaction.billing_email_address = user.billing_email
        billing_transaction.user_type = user.type.downcase
        billing_transaction.user_login = user.login
        billing_transaction.user_created_at = user.created_at || Time.current

        if payment_method = user.payment_method
          if billing_transaction.payment_type.blank?
            billing_transaction.payment_type = payment_method.paypal? ? :paypal : :credit_card
          end
          billing_transaction.region = payment_method.region
          billing_transaction.country = payment_method.country
          billing_transaction.postal_code = payment_method.postal_code
        end
      end

      billing_transaction.save!

      billing_transaction
    end

    # Public: Fetches chargebacks from Braintree that were filed in the last 2
    # weeks and have not yet been recorded in the GitHub database.
    #
    # Transactions without a user (the account has been deleted) are not
    # included in the results.
    #
    # Returns an array of disputes with transaction, user, date, url, and
    # reason.
    def self.unrecorded_disputes
      disputed_transactions = GitHub.dogstats.time("braintree.timing.dispute_search") do
        Braintree::Transaction.search { |s| s.dispute_date >= 2.weeks.ago }
      end

      transactions = Billing::BillingTransaction.includes(:live_user)
        .joins(:live_user)
        .where(transaction_id: disputed_transactions.ids)
        .where("last_status != ?", Billing::BillingTransactionStatuses::ALL[:charged_back])

      unrecorded_disputes = Braintree::Transaction.search do |s|
        s.ids.in transactions.map(&:transaction_id)
      end

      unrecorded_disputes.inject([]) do |results, disputed_transaction|
        if (dispute = disputed_transaction.disputes.last) &&
           dispute.reason != "retrieval"
          transaction = transactions.find { |t| t.transaction_id == disputed_transaction.id }

          results << OpenStruct.new(
            transaction: transaction,
            user: transaction.user,
            id: transaction.transaction_id,
            platform_url: transaction.platform_url,
            date: dispute.received_date,
            reason: dispute.reason,
          )
        end

        results
      end
    end

    # Public: Set transaction details into this billing transaction
    #
    # DEPRECATION WARNING:
    # This method is deprecated with the transition to Zuora. It should not be
    # used for new integrations and may be removed in the near future. See the
    # method Billing::ZuoraPayment#decorate_billing_transaction.
    #
    # braintree_transaction - A Braintree::Transaction (or
    #                         GitHub::Billing::PaymentProcessorTransaction)
    #
    # Returns nothing
    def transaction=(braintree_transaction)
      return if braintree_transaction.nil?

      unless braintree_transaction.is_a?(GitHub::Billing::PaymentProcessorTransaction)
        braintree_transaction = Billing::BraintreeTransaction.new(braintree_transaction)
      end

      self.transaction_id = braintree_transaction.id
      self.platform_transaction_id = braintree_transaction.id

      if braintree_transaction.refund?
        self.sale_transaction_id = braintree_transaction.original_transaction_id
      end

      if braintree_transaction.paypal?
        self.payment_type = :paypal
        self.paypal_email = braintree_transaction.paypal_email
      else
        self.payment_type = :credit_card
        self.bank_identification_number = braintree_transaction.bank_identification_number
        self.country_of_issuance = braintree_transaction.country_of_issuance
        self.last_four = braintree_transaction.last_four
      end

      if braintree_transaction.status_history.present?
        braintree_transaction.status_history.each do |status_detail|
          next if find_status(status_detail.status)
          self.statuses.build(status_detail: status_detail)
        end
        self.last_status = braintree_transaction.status
        self.settlement_batch_id ||= braintree_transaction.settlement_batch_id
      end
    end

    def incomplete?
      transaction_id.blank?
    end

    def job_posting?
      product == "job_posting"
    end

    # Public: Generate transaction_id based on our id. Primarily for use by
    #         zero-charge transactions that don't have a corresponding
    #         transaction on an external processor.
    #
    # Returns a String containing a base-36 encoded id preceeded by "0-"
    def our_transaction_id
      "0-#{id.to_s(36).rjust(6, "0")}" if id
    end

    # Internal: Finds the BillingTransactionStatus for the status
    #
    # Returns nil or a BillingTransactionStatus
    def find_status(status)
      statuses.all.find { |s| s.status == status }
    end

    # Public: The total discount of this transaction as a Money object,
    #         adjusted for billing frequency
    #
    # Returns a Money object
    def total_discount
      discount = discount_in_cents
      discount *= 12 if yearly?
      Billing::Money.new(discount)
    end

    # Public: Refund this transaction (or partial).
    #
    # refund_amount_in_cents - Integer amount in cents to refund if less than
    #                          transaction.amount (optional - defaults to
    #                          transaction.amount).
    #
    # Returns a GitHub::Billing:Result.
    def refund!(refund_amount_in_cents = nil)
      refund_amount_in_cents = if refund_amount_in_cents.nil?
        amount_in_cents
      else
        [refund_amount_in_cents, amount_in_cents].min
      end

      result = GitHub::Billing::Refund.new(self).process(refund_amount_in_cents)

      if result.success?
        refunded_at = result.billing_transaction.try(:created_at) || Time.current
        instrument = paypal? ? "PayPal account" : "credit card"
        BillingNotificationsMailer.refund(user, created_at.to_date,
          refund_amount_in_cents, instrument, refunded_at).deliver_now
      end

      return result
    end

    # Public: Void this transaction.
    #
    # Returns a GitHub::Billing:Result.
    def void
      if zuora?
        Billing::ZuoraVoid.process(self)
      else
        Billing::BraintreeVoid.process(self)
      end
    end

    # Public: Money sum total amount of this transaction.
    #
    # Returns a Money object.
    def amount
      Billing::Money.new(amount_in_cents)
    end

    # Public: Returns true if this billing transaction is not in a "final"
    # state.
    def pending_status_update?
      Billing::BillingTransactionStatuses::FINAL.exclude?(last_status)
    end

    # Internal: Retrieves transaction from processor and updates status
    def update_status_from_processor
      return if transaction_id.blank?

      if zuora?
        zuora_payment = ZuoraPayment.find(platform_transaction_id)
        zuora_payment.decorate_billing_transaction(self)
      else
        update_braintree_transaction_status
      end

      save!
    end

    # Public: Whether or not this transaction failed at the processor
    #
    # Returns true or false
    def failed?
      !success?
    end

    # Public: Whether or not this transaction is a refund
    #
    # Returns true or false
    def is_refund?
      sale_transaction_id.present?
    end

    # Public: Whether or not this transaction has been refunded
    #
    # Returns true or false
    def was_refunded?
      refund.present?
    end

    # Public: Whether or not this transaction is refundable
    #
    # Returns true or false
    def refundable?
      return false if failed?
      return false if amount_in_cents == 0
      return false if voided? || charged_back?
      return false if is_refund? || was_refunded?
      return false if legacy_braintree?
      true
    end

    # Public: Whether or not this transaction is "successful" - defined
    #         as having a status in the set [submitted_for_settlement, settled]
    #         Note that historically this also included "settling", but
    #         this status is no longer supported.
    #
    # Returns true or false
    def success?
      Billing::BillingTransactionStatuses::SUCCESS.include?(last_status)
    end

    # Public: Is this a prorated charge?
    #
    # Returns Boolean
    def prorated_charge?
      ["prorate-charge", "prorate-seat-charge", "prorate-asset-pack-charge"].include? transaction_type
    end

    # Public: Record a chargeback
    #
    # dispute - A Braintree::Dispute from the braintree API linked to this
    #           billing transaction
    def chargeback!(dispute)
      details = "Chargeback\n" \
                "Date received: #{dispute.received_date}\n" \
                "Reason: #{dispute.reason}"
      notes.create(note: details)
      statuses.create(
        amount_in_cents: (dispute.amount.to_i * 100),
        status: :charged_back,
      )
      self.last_status = :charged_back

      save!
    end

    # Public: The unmasked card number used in this transaction
    #
    # Returns a String
    def truncated_number
      "#{bank_identification_number}#{"*" * 6}#{last_four}"
    end

    # Public: The formatted, masked card number used in this transaction
    #
    # Returns a String
    def card_number
      format_card_number(truncated_number)
    end

    # Public: The card type used in this transaction
    #
    # Returns a String
    def card_type
      detect_card_type(truncated_number)
    end

    def date
      @date ||= created_at.to_billing_date
    end

    def in_progress?
      created_at > 10.minutes.ago
    end

    # Public: String user friendly physical address. Ex: "San Francisco, CA, 94107"
    #
    def location
      [region, country, postal_code].compact.join ", "
    end

    # Public: User, if user is still around, otherwise DeadUser
    def user
      @user ||= live_user || dead_user
    end

    # Public: Sets live_user (so it acts like a belongs_to)
    def user=(user)
      self.live_user = @user = user
    end

    # Internal: Quacks like a User, if the user isn't around anymore
    def dead_user
      @dead_user ||= GitHub::Billing::DeadUser.new do |user|
        user.id                    = user_id
        user.login                 = user_login
        user.billing_email         = billing_email_address
        user.created_at            = user_created_at
        user.user_type             = user_type
      end
    end

    # Internal: Returns true if transaction_id should be required
    def transaction_id_required?
      has_status? && last_status != "failed"
    end

    # Internal: Returns true if there are any statuses present
    def has_status?
      last_status.present?
    end

    def calculated_arr_in_cents
      pricing.annual_recurring_revenue.cents
    end

    # Public: Returns the base URL for our merchant account at Braintree
    #
    # Returns a String URL
    def self.braintree_merchant_url
      @braintree_base_url ||= begin
        "#{GitHub.braintree_host}#{Braintree::Configuration.instantiate.base_merchant_path}"
      end
    end

    # Public: Returns the base URL for our account at Zuora
    #
    # Returns a String URL
    def self.zuora_url
      GitHub.zuora_host
    end

    # Public: Returns a URL to view this transaction on the processor's website
    # (e.g. Braintree or Zuora)
    #
    # Returns a String URL
    def platform_url
      case platform
      when "braintree"
        "#{self.class.braintree_merchant_url}/transactions/#{platform_transaction_id}"
      when "zuora"
        "#{self.class.zuora_url}/apps/NewPayment.do?method=view&id=#{platform_transaction_id}"
      end
    end

    # Public: Returns the name of the processor's website
    # (e.g. Braintree or Zuora)
    #
    # Returns a String
    def platform_name
      platform.titlecase
    end

    private

    # Private: Copies user details and plan information to this billing
    # transaction.
    #
    # user        - A User/Organization to copy details from.
    # charge_type - String charge type (optional, defaults to
    #               User#recurring_charge_type).
    #
    # Returns nothing.
    def copy_user_details(user, charge_type: nil)
      self.user = user

      self.asset_packs_total              = user.data_packs
      self.asset_pack_unit_price_in_cents = Asset::Status.data_pack_unit_price.cents
      self.billing_email_address          = user.billing_email
      self.plan_name                      = user.plan.name
      self.plan_price_in_cents            = base_price_in_cents
      self.renewal_frequency              = user.yearly_plan? ? :yearly : :monthly
      self.seats_total                    = user.seats
      self.transaction_type               = charge_type || user.recurring_charge_type
      self.user_created_at                = user.created_at || Time.current
      self.user_login                     = user.login
      self.user_type                      = user.type.downcase
      self.arr_in_cents                   = calculated_arr_in_cents

      if payment_method = user.payment_method
        self.region      = payment_method.region
        self.country     = payment_method.country
        self.postal_code = payment_method.postal_code
      end

      if user.coupon
        self.coupon_name       = user.coupon.code
        self.discount_in_cents = user.discount * 100
      end
    end

    # Private: Copies historical plan information to this billing transaction.
    #
    # created_at - a DateTime representing when this transaction orginally took place
    #
    # Returns nothing.
    def copy_historical_information(created_at)
      transaction = user.transactions.
        where(["timestamp <= ?", created_at]).
        order(:timestamp).
        last

      coupon_redemption = user.coupon_redemptions.
        where(["created_at <= ?", created_at]).
        where(["expires_at > ?", created_at]).
        last
      coupon = coupon_redemption.try(:coupon)

      plan = transaction.current_plan
      self.plan_name = plan.name

      self.plan_price_in_cents = base_price_in_cents

      if transaction.plan_duration == User::YEARLY_PLAN
        self.renewal_frequency = :yearly
      else
        self.renewal_frequency = :monthly
      end

      self.asset_packs_total = transaction.asset_packs_total
      self.seats_total = transaction.current_seats

      # NB: Since this needs to allow old coupons, we manually pass in the `discount`
      # so we don't apply normal coupon expiration rules
      # See https://github.com/github/github/blob/9da19220517/test/models/billing/billing_transaction_test.rb#L379
      pricing = Billing::Pricing.new \
        account: user,
        plan: plan,
        seats: seats_total,
        data_packs: asset_packs_total,
        plan_duration: user.plan_duration,
        discount: coupon&.discount

      pricing_attributes = {
        discount_in_cents: pricing.discount.cents,
        arr_in_cents: pricing.annual_recurring_revenue.cents,
      }

      assign_attributes(pricing_attributes)
    end

    def create_attempted_line_items(plan_change:)
      if plan_change.subscription_items_delta.present?
        create_github_line_item \
          amount_in_cents: plan_change.final_price(github_only: true, use_balance: true).cents
        create_marketplace_line_items \
          plan_change: plan_change,
          subscription_items: plan_change.subscription_items_delta,
          service_percent_remaining: service_percent_remaining(plan_change)
      end
    end

    def create_recurring_line_items(invoiced_items: [])
      if user.subscription_items.present?
        create_marketplace_line_items \
          subscription_items: user.active_subscription_items,
          invoiced_items: invoiced_items
        create_github_line_item \
          amount_in_cents: recurring_github_amount_in_cents
      end

      create_usage_line_items(invoiced_items)
    end

    # Private: Creates line items on the transaction from usage products from the invoiced items.
    #
    # Returns Nil
    def create_usage_line_items(invoiced_items)
      invoiced_usage_items(invoiced_items).each do |item|
        line_items.create \
          description: item.charge_name,
          quantity: item.quantity,
          amount_in_cents: item.charge_amount.cents
      end
    end

    # Private: Returns the line items from usage products.
    #
    # Returns [Hash]
    def invoiced_usage_items(invoiced_items)
      invoiced_items.select do |item|
        BillingTransaction::LineItem::USAGE_DESCRIPTIONS.include?(item.charge_name)
      end
    end

    def create_marketplace_line_items(plan_change: nil, subscription_items:, service_percent_remaining: 1, invoiced_items: [])
      subscription_items.select(&:billable?).each do |subscription_item|
        charged_invoiced_items = invoiced_items.select do |item|
          item.charge_name.downcase.include?(subscription_item.listing_name.downcase)
        end

        if charged_invoiced_items.present?
          cost = charged_invoiced_items.sum(&:charge_amount)
        else
          cost = subscription_item_cost subscription_item,
            plan_change: plan_change,
            service_percent_remaining: service_percent_remaining
        end

        if invoiced_items.empty? || charged_invoiced_items.present?
          # If invoiced items is present, that means we're in a Zuora transaction
          # Only create line items if there's matching invoice item in the applied invoices
          line_items.create \
            description: subscription_item.marketplace_line_item_description,
            quantity: subscription_item.quantity,
            amount_in_cents: cost.cents,
            listing: subscription_item.listing,
            subscribable: subscription_item.subscribable,
            arr_in_cents: subscription_item_arr(subscription_item)
        end
      end
    end

    def subscription_item_arr(subscription_item)
      return 0 if subscription_item.listable_is_sponsorable?

      (BillingTransaction::MARKETPLACE_REVENUE_CUT *
        subscription_item.price(duration: user.plan_duration).cents *
        arr_multiplier).floor
    end

    def subscription_item_cost(subscription_item, plan_change: nil, service_percent_remaining:)
      if plan_change
        plan_change.prorated_mp_price_for \
          subscription_item: subscription_item,
          service_percent_remaining: service_percent_remaining
      else
        subscription_item.price(service_remaining: service_percent_remaining)
      end
    end

    def create_github_line_item(amount_in_cents:)
      return if amount_in_cents == 0
      line_items.create \
        description: "GitHub",
        quantity: 1,
        amount_in_cents: amount_in_cents,
        arr_in_cents: github_arr
    end

    def github_arr
      pricing.annual_recurring_revenue_details.github_total.cents
    end

    def base_price_in_cents
      self.plan_price_in_cents = Billing::Pricing.new(
        plan: user.plan,
        plan_duration: user.plan_duration,
      ).discounted.cents
    end

    def pricing
      Billing::Pricing.new \
        account: user,
        plan: user.plan,
        seats: user.seats,
        plan_duration: user.plan_duration
    end

    def arr_multiplier
      user.monthly_plan? ? 12 : 1
    end

    def service_percent_remaining(plan_change)
      if plan_change.upgrading_from_free?
        1
      else
        plan_change.service_percent_remaining
      end
    end

    #Private: Updates the transaction status of braintree transactions
    def update_braintree_transaction_status
      braintree_transaction = Braintree::Transaction.find(transaction_id)
      if braintree_transaction.status_history.present?
        braintree_transaction.status_history.each do |status_detail|
          next if find_status(status_detail.status)
          self.statuses.build(status_detail: status_detail)
        end
        self.last_status = braintree_transaction.status
        self.settlement_batch_id ||= braintree_transaction.settlement_batch_id
      end
    end

    # Private: Calculate the recurring transaction amount in cents applied to GitHub specific expenses
    #
    # Returns Integer
    def recurring_github_amount_in_cents
      return calculate_github_amount_with_credit if amount_in_cents
      pricing.discounted_cents + (user.plan_subscription&.balance_in_cents || 0)
    end

    # Private: Calculates the amount spent on GitHub line items by subtracting the sum of non-github line
    # items from the transaction amount - this allows us to account for any credit that would have been
    # applied when generating the transaction
    #
    # Returns Integer
    def calculate_github_amount_with_credit
      amount_in_cents - marketplace_line_item_amount
    end

    def marketplace_line_item_amount
      line_items.marketplace.sum(:amount_in_cents)
    end
  end
end
