# frozen_string_literal: true

class Billing::MeteredBillingBillableOwnerDesignator
  def self.attributes_for(owner)
    new(owner).to_h
  end

  def initialize(owner)
    @owner = owner
  end

  def billable_owner
    @_billable_owner ||= owner.delegate_billing_to_business? ? owner.business : owner
  end

  def directly_billed?
    !billable_owner.try(:enterprise_agreement?)
  end

  def to_h
    {
      billable_owner_id: billable_owner.id,
      billable_owner_type: billable_owner.class.polymorphic_name,
      directly_billed: directly_billed?,
    }
  end

  private

  attr_reader :owner
end
