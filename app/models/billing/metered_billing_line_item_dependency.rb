# frozen_string_literal: true

module Billing::MeteredBillingLineItemDependency
  extend ActiveSupport::Concern

  included do
    enum submission_state: {
      unsubmitted: "unsubmitted",
      submitted: "submitted",
      skipped: "skipped",
    }

    belongs_to :owner, class_name: "User", optional: true
    belongs_to :actor, class_name: "User", optional: true
    belongs_to :repository, optional: true
    belongs_to :synchronization_batch,
      class_name: "Billing::UsageSynchronizationBatch", optional: true
    belongs_to :billable_owner, polymorphic: true, optional: true

    validates :owner_id, presence: true
    validates :actor_id, presence: true
    validates :repository_id, presence: true
    validates :billable_owner_id, presence: true
    validates :billable_owner_type, presence: true
    validates :start_time, presence: true
    validates :end_time, presence: true
    validates :submission_state, presence: true

    scope :directly_billed, -> { where(directly_billed: true) }
    scope :within_dates, -> (start_on, end_on) {
      where("end_time >= ?", GitHub::Billing.date_in_timezone(start_on)).
        where("end_time < ?", GitHub::Billing.date_in_timezone(end_on) + 1.day)
    }
    scope :after, -> (start_on) {
      where("end_time >= ?", GitHub::Billing.date_in_timezone(start_on))
    }
  end
end
