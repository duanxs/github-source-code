# frozen_string_literal: true

module Billing::SharedStorage
  class RepositoryArtifactEventAggregator
    attr_reader :aggregate_fields, :aggregation, :repository_id, :cutoff

    def initialize(repository_id:, cutoff:, **aggregate_fields)
      @aggregate_fields = aggregate_fields
      @repository_id = repository_id

      # Truncate everything after the hour
      @cutoff = cutoff.beginning_of_hour
    end

    def perform
      if aggregate_size_in_bytes.positive? || events.any?
        ArtifactAggregation.transaction do
          @aggregation = ArtifactAggregation.new(
            repository_id: repository_id,
            aggregate_effective_at: cutoff,
            repository_visibility: aggregation_repository_visibility,
            aggregate_size_in_bytes: aggregate_size_in_bytes,
            previous_aggregation: previous_aggregation,
            submission_state: :unsubmitted,
            **aggregate_fields,
          )

          if @aggregation.billable_owner.nil?
            GitHub.dogstats.increment(
              "billing.shared_storage.aggregate_repositories_skipped",
              tags: ["reason:missing_billable_owner"],
            )
            return
          end

          @aggregation.save!
          events_updated = ArtifactEvent.where(id: events.map(&:id)).update_all(aggregation_id: @aggregation.id)
          GitHub.dogstats.increment("billing.shared_storage.repositories_aggregated")
          GitHub.dogstats.count("billing.shared_storage.events_aggregated", events_updated)

          @aggregation.publish_metered_line_item_updated_message
        end
      end
    end

    private

    def aggregate_size_in_bytes
      # Do not allow negative sizes. This could happen when artifacts are removed that
      # we didn't track the addition of (for example artifacts created before we started
      # storing events).
      [previous_aggregation&.aggregate_size_in_bytes.to_i + size_delta_in_bytes, 0].max
    end

    def events
      @events ||= ActiveRecord::Base.connected_to(role: :reading) do
        ArtifactEvent
          .where("effective_at <= ?", cutoff)
          .where(aggregate_fields)
          .where(repository_id: repository_id)
          .where(aggregation_id: nil)
          .all
      end
    end

    def previous_aggregation
      @previous_aggregation ||= ActiveRecord::Base.connected_to(role: :reading) do
        ArtifactAggregation
          .where(aggregate_fields)
          .where(repository_id: repository_id)
          .order(:aggregate_effective_at)
          .last
      end
    end

    def aggregation_repository_visibility
      current_repository_visibility ||
      previous_aggregation_visibility ||
      lastest_event_visibility
    end

    def current_repository_visibility
      @current_repository_visibility ||= ActiveRecord::Base.connected_to(role: :reading) do
        repo = Repository.where(id: repository_id).select(:public).last ||
          Archived::Repository.where(id: repository_id).select(:public).last

        if repo
          repo.public ? :public : :private
        end
      end
    end

    def previous_aggregation_visibility
      previous_aggregation&.repository_visibility
    end

    def lastest_event_visibility
      events.last&.repository_visibility
    end

    def size_delta_in_bytes
      @size_delta_in_bytes ||= events.sum do |event|
        case event.event_type
        when "add" then event.size_in_bytes
        when "remove" then -1 * event.size_in_bytes
        else 0
        end
      end
    end
  end
end
