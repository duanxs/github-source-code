# frozen_string_literal: true

module Billing::SharedStorage
  class ArtifactEvent < ApplicationRecord::Collab
    self.table_name = "shared_storage_artifact_events"

    enum source: {
      unknown: "unknown",
      actions: "actions",
      gpr: "gpr",
    }, _suffix: true

    enum repository_visibility: {
      unknown: "unknown",
      public: "public",
      private: "private",
    }, _suffix: :visibility

    enum event_type: {
      unknown: "unknown",
      add: "add",
      remove: "remove",
    }, _suffix: :event

    belongs_to :owner, class_name: "User", optional: true
    belongs_to :repository
    belongs_to :aggregation, class_name: "Billing::SharedStorage::ArtifactAggregation"
    belongs_to :billable_owner, polymorphic: true

    validates :owner_id, presence: true
    validates :size_in_bytes, presence: true, numericality: { greater_than_or_equal_to: 0 }

    def self.upcoming_actions_expirations_by_repo_and_effective_at(owner_id)
      actions_source
        .remove_event
        .where("effective_at > ?", ::GitHub::Billing.now)
        .where(owner_id: owner_id)
        .group(["repository_id", "CAST(effective_at AS DATE)"])
        .sum(:size_in_bytes)
    end

    def self.billable_events_by_repo_and_source(owner_id)
      where("effective_at <= ?", ::GitHub::Billing.now)
      .where(owner_id: owner_id)
      .group(["repository_id", "source", "aggregation_id IS NOT NULL"])
      .sum(<<~SQL)
        CASE event_type
        WHEN 'add' then size_in_bytes
        WHEN 'remove' then -1 * size_in_bytes
        ELSE 0
        END
      SQL
    end
  end
end
