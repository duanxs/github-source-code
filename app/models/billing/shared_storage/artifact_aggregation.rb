# frozen_string_literal: true

module Billing::SharedStorage
  class ArtifactAggregation < ApplicationRecord::Collab
    UNIT_OF_MEASURE = "Megabytes"

    self.table_name = "shared_storage_artifact_aggregations"
    enum repository_visibility: {
      unknown: "unknown",
      public: "public",
      private: "private",
    }, _suffix: :visibility

    enum submission_state: {
      unsubmitted: "unsubmitted",
      submitted: "submitted",
      skipped: "skipped",
    }

    belongs_to :owner, class_name: "User"
    belongs_to :repository
    belongs_to :previous_aggregation, class_name: "Billing::SharedStorage::ArtifactAggregation"
    belongs_to :synchronization_batch, class_name: "Billing::UsageSynchronizationBatch"
    belongs_to :billable_owner, polymorphic: true, required: true

    validates :owner_id, presence: true
    validates :aggregate_size_in_bytes, presence: true
    validates :submission_state, presence: true

    delegate :account_number, :subscription_number, :charge_number,
      to: :zuora_usage_attributes, prefix: :zuora

    scope :directly_billed, -> { where(directly_billed: true) }
    scope :within_dates, -> (start_on, end_on) {
      where("aggregate_effective_at >= ?", GitHub::Billing.date_in_timezone(start_on)).
        where("aggregate_effective_at < ?", GitHub::Billing.date_in_timezone(end_on) + 1.day)
    }
    scope :after, -> (start_on) {
      where("aggregate_effective_at >= ?", GitHub::Billing.date_in_timezone(start_on))
    }

    def self.product_name
      "storage"
    end

    # Public: The billable quantity given in megabytes
    #
    # Returns BigDecimal
    def billable_quantity
      BigDecimal(aggregate_size_in_bytes) / Numeric::MEGABYTE
    end

    # Public: The unit of measure for billing
    #
    # Returns String literal "Megabytes"
    def unit_of_measure
      UNIT_OF_MEASURE
    end

    # Public: When the usage occurred
    #
    # Returns DateTime
    def usage_at
      aggregate_effective_at
    end

    # Public: Publish a MeteredLineItemUpdated Hydro message for this record
    #
    # This method uses Hydro::PublishRetrier since thousands of these messages
    # can be published in a short timeframe, possibly overflowing the Hydro
    # message buffer and dropping messages.
    #
    # Returns nothing
    def publish_metered_line_item_updated_message
      message = {
        metered_product: :STORAGE,
        line_item_id: id,
      }
      Hydro::PublishRetrier.publish(message, schema: "github.billing.v0.MeteredLineItemUpdated")
    end

    private

    def zuora_usage_attributes
      @zuora_usage_attributes ||= Billing::ZuoraUsageAttributes.new(
        billable_owner: billable_owner,
        usage_type: :shared_storage,
        usage_at: usage_at,
      )
    end
  end
end
