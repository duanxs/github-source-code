# frozen_string_literal: true

class Billing::SharedStorage::ZuoraProduct
  include GitHub::Billing::ZuoraProduct::ZuoraSettings

  UNIT_OF_MEASURE = "Megabytes"

  # We are making the assumption that every month has 31 days instead of the average of ~30.5
  # days per year in order to never overcharge users.
  ASSUMED_MONTHLY_DAYS = 31
  MEGABYTES_IN_GIGABYTE = 1.gigabyte / Numeric::MEGABYTE

  # Price per monthly GB usage
  UNIT_COST = "0.25"
  UNIT_COST_PER_GB_IN_CENTS = BigDecimal(UNIT_COST) * 100
  UNIT_COST_PER_MB_IN_CENTS = BigDecimal(UNIT_COST) * 100 / MEGABYTES_IN_GIGABYTE

  def self.uuid
    ::Billing::ProductUUID.find_by(product_type: product_type, product_key: product_key)
  end

  def self.product_name
    "GitHub Shared Storage"
  end

  def self.product_type
    "github.shared_storage"
  end

  def self.product_key
    "v0"
  end

  def self.sync_to_zuora
    new.sync_to_zuora
  end

  def sync_to_zuora
    return unless GitHub.billing_enabled?

    create_product
    create_rate_plans
  end

  private

  attr_reader :product_id

  def create_product
    result = GitHub.zuorest_client.create_product(
      {
        Name: self.class.product_name,
        EffectiveStartDate: GitHub::Billing.today.to_s,
        EffectiveEndDate: EFFECTIVE_END_DATE,
      },
    )
    @product_id = result["Id"]
  end

  def create_rate_plans
    return if Billing::ProductUUID.exists?(product_type: self.class.product_type, product_key: self.class.product_key)

    product_rate_plan = create_product_rate_plan
    product_rate_charges = create_product_rate_plan_charges(product_rate_plan)

    storage_rpc = product_rate_charges[0]

    Billing::ProductUUID.create!(
      product_type: self.class.product_type,
      product_key: self.class.product_key,
      billing_cycle: :month,
      zuora_product_id: product_id,
      zuora_product_rate_plan_id: product_rate_plan["Id"],
      zuora_product_rate_plan_charge_ids: {
        usage: storage_rpc["Id"],
      },
    )
  end

  def create_product_rate_plan
    GitHub.zuorest_client.create_product_rate_plan(
      Name: "#{self.class.product_name} - #{self.class.product_key.titleize}",
      EffectiveStartDate: GitHub::Billing.today.to_s,
      EffectiveEndDate: EFFECTIVE_END_DATE,
      ProductId: product_id,
    )
  end

  def create_product_rate_plan_charges(product_rate_plan)
    GitHub.zuorest_client.create_action(
      type: "ProductRatePlanCharge",
      objects: [
        storage_rate_plan_charge(product_rate_plan_id: product_rate_plan["Id"]),
      ],
    )
  end

  def storage_rate_plan_charge(product_rate_plan_id:)
    calculator = ::Billing::MeteredBilling::HourlyRateCalculator.new
    {
      BillingPeriod: "Month",
      ChargeModel: "Overage Pricing",
      ChargeType: "Usage",
      DeferredRevenueAccount: DEFERRED_REVENUE_ACCOUNT,
      Name: "#{self.class.product_name} - #{self.class.product_type.titleize}",
      ProductRatePlanId: product_rate_plan_id,
      RecognizedRevenueAccount: RECOGNIZED_REVENUE_ACCOUNT,
      TaxCode: TAX_CODE,
      TaxMode: "TaxExclusive",
      Taxable: false,
      TriggerEvent: "ContractEffective",
      UOM: UNIT_OF_MEASURE,
      ListPrice: calculator.hourly_unit_cost(
        cost_per_month: UNIT_COST,
        unit_divisor: 1024,
      ).to_s,
      IncludedUnits: 0,
      ProductRatePlanChargeTierData: {
        ProductRatePlanChargeTier: [
          {
            Currency: "USD",
            Price: calculator.hourly_unit_cost(
              cost_per_month: UNIT_COST,
              unit_divisor: 1024,
            ).to_s,
            PriceFormat: "Per Unit",
          },
        ],
      },
    }
  end
end
