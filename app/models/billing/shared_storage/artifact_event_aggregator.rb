# frozen_string_literal: true

module Billing::SharedStorage
  class ArtifactEventAggregator
    attr_reader :aggregate_fields, :cutoff

    def initialize(cutoff:, **aggregate_fields)
      @aggregate_fields = aggregate_fields
      @cutoff = cutoff

      @restraint = GitHub::Restraint.new
    end

    def perform
      restraint.lock!(lock_key, _concurrency = 1, _ttl = 5.minutes) do
        perform_without_lock
      end
    end

    def perform_without_lock
      GitHub.dogstats.count("billing.shared_storage.repositories_selected", repositories_to_aggregate.count)

      aggregations = repositories_to_aggregate.flat_map do |repository_id|
        aggregator = RepositoryArtifactEventAggregator.new(
          repository_id: repository_id,
          cutoff: cutoff,
          **aggregate_fields,
        )
        aggregator.perform
        aggregator.aggregation
      end

      if owner
        notify_later_if_applicable
        record_billable_owner if aggregations.compact.any?(&:persisted?)
      end
    end

    private

    attr_reader :restraint

    def notify_later_if_applicable
      MeteredBillingThresholdNotifierJob.perform_later(
        owner: owner,
        product: ::Billing::UsageNotifications::SHARED_STORAGE_PRODUCT,
      )
    end

    def record_billable_owner
      ::Billing::SharedStorage::BillableOwner.create!(
        owner: owner,
        billable_owner_type: aggregate_fields[:billable_owner_type],
        billable_owner_id: aggregate_fields[:billable_owner_id],
      )
    rescue ActiveRecord::RecordNotUnique
      # Pass
    end

    def owner
      @owner ||= User.find_by(id: aggregate_fields[:owner_id])
    end

    def repositories_to_aggregate
      ActiveRecord::Base.connected_to(role: :reading) do
        ArtifactEvent.where(aggregate_fields).select(:repository_id).distinct.pluck(:repository_id)
      end
    end

    def lock_key
      "shared-storage/artifact-event-aggregator/#{aggregate_fields.values.join("/")}"
    end
  end
end
