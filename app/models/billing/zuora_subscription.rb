# frozen_string_literal: true

class Billing::ZuoraSubscription
  attr_reader :raw_subscription

  class NotFoundError < StandardError; end

  def self.find(zuora_id)
    return if zuora_id.nil?

    subscription = new(zuora_id)
    return nil unless subscription.found?

    subscription
  end

  # Cancels the zuora subscription
  # The cancellation will take place effect on the subscription's charged through date
  #
  # options - The Hash of options to modify the default cancellation arguments
  #
  # Returns Hash
  def cancel(**options)
    GitHub.zuorest_client.cancel_subscription(number, {
      cancellationPolicy: "SpecificDate",
      cancellationEffectiveDate: charged_through_date.to_s,
      invoiceCollect: false,
    }.merge!(options))
  end

  # Suspends the zuora subscription
  # The suspension will take place immediately unless specified in the options through
  # the suspendPolicy argument. For more options see https://www.zuora.com/developer/api-reference/#operation/PUT_SuspendSubscription
  # - After suspension, a subscription can only be resumed in the future. No same day resume
  # - After suspension, a subscription's ID changes
  # - A subscription cannot be amended if it is suspended.
  # - EffectiveEndDate gets set to the suspension date.
  #
  # options - The Hash of arguments to modify the request based on the Zuora documentation
  #
  # Returns Hash
  def suspend(**options)
    response = GitHub.zuorest_client.suspend_subscription(number, {
      extendsTerm: false,
      resume: false,
      suspendPolicy: "Today",
    }.merge!(options))

    if response["success"]
      raw_subscription[:id] = response["subscriptionId"]
    end

    response
  end

  # Resumes the zuora subscription
  # The resume will take place immediately unless specified in the options through
  # the resumePolicy argument. For more options see https://www.zuora.com/developer/api-reference/#operation/PUT_ResumeSubscription
  # - After resume, a subscription can only be suspended in the future. No same day suspension
  # - After resume, a subscription's ID changes
  #
  # options - The Hash of arguments to modify the request based on the Zuora documentation
  #
  # Returns Hash
  def resume(**options)
    response = GitHub.zuorest_client.resume_subscription(number, {
      collect: false,
      runBilling: true,
      resumePolicy: "Today",
    }.merge!(options), ::Billing::PlanSubscription::ZuoraSynchronizer::ZUORA_VERSION_HEADER)

    if response["success"]
      raw_subscription[:id] = response["subscriptionId"]
    end

    response
  end

  # Active rate plans tied to the subscription
  #
  # This excludes plans that have been scheduled for removal in Zuora due to a downgrade
  #
  # Returns Array
  def active_rate_plans
    @_active_rate_plans ||= rate_plans.to_a.select do |plan|
      plan[:ratePlanCharges].any? { |charge| Billing::Zuora::Subscription::RatePlanCharge.new(charge).active? }
    end
  end

  # Number of seats in the subscription in addition to the base seats
  #
  # Returns Integer
  def seats
    return 0 if plan.nil? || !plan.per_seat?

    github_rate_plan = active_github_rate_plan

    seats_rate_plan_charge_id = plan.zuora_charge_ids(cycle: plan_duration)[:unit]
    seats_rate_plan_charge = github_rate_plan[:ratePlanCharges].detect do |rate_plan_charge|
      rate_plan_charge[:productRatePlanChargeId] == seats_rate_plan_charge_id
    end

    if plan.business?
      default_seats_rate_plan_charge_id = plan.zuora_charge_ids(cycle: plan_duration)[:base_unit]
      default_seats_rate_plan_charge = github_rate_plan[:ratePlanCharges].detect do |rate_plan_charge|
        rate_plan_charge[:productRatePlanChargeId] == default_seats_rate_plan_charge_id
      end

      # workaround, we always expect a default of 1 seat but we need to add the
      # "default seat" rate plan's to the quantity due to the < 5 seats workaround
      (seats_rate_plan_charge[:quantity].to_i + default_seats_rate_plan_charge[:quantity].to_i) - 1
    else
      seats_rate_plan_charge[:quantity].to_i
    end
  end

  # Returns whether the subscription is dated in the future
  #
  # Returns Boolean
  def pending?
    return false if raw_subscription[:contractEffectiveDate].blank?

    GitHub::Billing.timezone.parse(raw_subscription[:contractEffectiveDate]).future?
  end

  # Whether the subscription has a past due invoice
  #
  # Returns Boolean
  def past_due?
    return @past_due if defined?(@past_due)
    @past_due = Billing::ZuoraInvoice.past_due(account_id: raw_subscription[:accountId]).any?
  end

  # Number of data packs in the subscription
  #
  # Returns Integer
  def data_packs
    return @data_packs if defined?(@data_packs)
    rate_plan_id = Asset::Status.zuora_id(cycle: plan_duration)
    data_pack_rate_plan = active_rate_plans.detect do |plan|
      plan[:productRatePlanId] == rate_plan_id
    end
    return @data_packs = 0 unless data_pack_rate_plan

    rate_plan_charge_id = Asset::Status.zuora_charge_ids(cycle: plan_duration)[:unit]
    rate_plan_charge = data_pack_rate_plan[:ratePlanCharges].detect do |charge|
      charge[:productRatePlanChargeId] == rate_plan_charge_id
    end
    return @data_packs = 0 unless rate_plan_charge

    @data_packs = rate_plan_charge[:quantity].to_i
  end

  # The outstanding balance for the account
  #
  # Returns Float
  def balance
    zuora_account[:metrics][:balance]
  end

  # Cancelled rate plans tied to the subscription
  #
  # This includes plans that have been scheduled for cancellation, but not yet
  # cancelled
  #
  # Returns Array
  def cancelled_rate_plans
    rate_plans.to_a.select do |plan|
      plan[:ratePlanCharges].any? { |charge| charge[:effectiveEndDate].present? }
    end
  end

  # The active payment amount of the subscription based on the duration in months,
  # minus the user's discounts
  # plan_duration_in_months: [1, 12] based on the user's plan duration
  #
  # Returns Billing::Money
  def payment_amount(plan_duration_in_months:)
    contracted_mrr * plan_duration_in_months - discount
  end

  # The date that the subscription has been charged through. This does not always
  # line up with the next bill date, as we bill for usage based products every month.
  #
  # Returns String
  def charged_through_date
    active_charged_through_date || GitHub::Billing.today
  end
  alias_method :next_billing_date, :charged_through_date

  # Public: The charge through date from the active rate plans
  #
  # Returns String representing date or nil
  def active_charged_through_date
    charges = if active_billing_period_varies?
      # Monthly Actions rate plan - but other rate plans are annual
      active_rate_plan_charges.select(&:annual?)
    else
      active_rate_plan_charges
    end

    # Select the first charge with charged through date
    charge = charges.detect(&:charged_through_date)
    return unless charge

    if charge.usage?
      Date.parse(charge.charged_through_date) + 1.month
    else
      Date.parse(charge.charged_through_date)
    end
  end


  # The current GitHub plan on the subscription
  #
  # Returns GitHub::Plan or nil
  def plan
    @plan ||= GitHub::Plan.find(github_plan_product&.product_key)
  end

  # The type of billing cycle the subscription is currently on based on its active rate plans
  #
  # Defaults to "month"
  #
  # Returns String - "month", "year"
  def plan_duration
    billing_period.to_s.downcase == "annual" ? User::YEARLY_PLAN : User::MONTHLY_PLAN
  end

  # Public: The total discount amount on the wrapped ZuoraSubscription
  #
  # Returns Billing::Money discount
  def discount
    return Billing::Money.new(0) unless discounts
    Billing::Money.new(sum_discounts * 100)
  end

  # The versioned 32 character ID for the Zuora subscription
  #
  # Returns String
  def id
    raw_subscription[:id]
  end

  # The short form ID for the Zuora subscription
  #
  # Returns String
  def number
    raw_subscription[:subscriptionNumber]
  end

  # Public: Whether or not the subscription is active
  #
  # Returns Boolean
  def active?
    raw_subscription[:status] == "Active"
  end

  # Public: Whether the subscription has been cancelled
  #
  # Returns Boolean
  def cancelled?
    raw_subscription[:status] == "Cancelled"
  end

  def status
    raw_subscription[:status]
  end

  def dashboard_url
    "#{GitHub.zuora_host}/apps/Subscription.do?method=view&id=%s" % [id]
  end

  # Public: Whether or not the subscription was found
  #
  # Returns Boolean
  def found?
    raw_subscription[:success]
  end

  # Public: Returns all active rate plan charges
  #
  # Returns Array
  def active_rate_plan_charges
    @_active_rate_plan_charges ||= active_rate_plans.flat_map do |plan|
      plan[:ratePlanCharges].map do |rate_plan_charge|
        ::Billing::Zuora::Subscription::RatePlanCharge.new(rate_plan_charge)
      end
    end.select(&:active?)
  end

  private

  # Private: Amount that an active percentage coupon is discounting over eligible charges.
  #
  # Returns Float
  def percentage_discounted_amount(percentage_discount_charge)
    percentage_discount_charge["discountApplyDetails"].reduce(0) do |eligible_charge_amount, apply_details|
      discount_eligible_rate_plan = active_rate_plans.detect do |rate_plan|
        rate_plan["productRatePlanId"] == apply_details["appliedProductRatePlanId"]
      end

      if discount_eligible_rate_plan
        discount_eligible_rate_plan["ratePlanCharges"].each do |rate_plan_charge|
          eligible_charge_amount += rate_plan_charge["price"] * rate_plan_charge["quantity"]
        end
      end

      eligible_charge_amount
    end * (percentage_discount_charge["discountPercentage"] / 100)
  end

  def zuora_account
    @zuora_account ||= Zuorest::Model::Account.find(raw_subscription[:accountId])
  end

  def rate_plans
    raw_subscription[:ratePlans]
  end

  # Private: All discounts on the ZuoraSubscription.
  #
  # Returns Array
  def discounts
    return [] unless active_rate_plans
    active_rate_plans.select do |rate_plan|
      rate_plan["productName"].include?("Discount")
    end
  end

  # Private: The sum of all discounts on the wrapped ZuoraSubscription
  #
  # Returns Float
  def sum_discounts
    discounts.sum do |discount|
      discount["ratePlanCharges"].sum do |rate_plan_charge|
        if rate_plan_charge["discountPercentage"]
          percentage_discounted_amount(rate_plan_charge)
        else
          rate_plan_charge["discountAmount"]
        end
      end
    end
  end

  # Private: Is the discount a percentage?
  #
  # Returns Boolean
  def percentage_discount?
    discounts.any? do |discount|
      discount["ratePlanCharges"].any? { |charge| charge["discountPercentage"].present? }
    end
  end

  # Private: The billing period of the current subscription
  #
  # Returns String - "Month", "Annual"
  def billing_period
    @_billing_period ||= if active_billing_period_varies?
      # Monthly Actions rate plan - but other rate plans are annual
      "Annual"
    else
      active_rate_plan_charges.map(&:billing_period).compact.first
    end
  end

  # Private: Does the billing period in active rate plan charges vary?
  #
  # Returns Boolean
  def active_billing_period_varies?
    active_rate_plan_charges.map(&:billing_period).compact.uniq.length > 1
  end

  # Private: The GitHub ProductUUID associated with subscription
  #
  # Returns Billing::ProductUUID
  def github_plan_product
    return @_github_plan_product if @_github_plan_product

    rate_plan_ids = active_rate_plans.map { |rate_plan| rate_plan[:productRatePlanId] }

    # There should only be one active product rate plan for a github plan
    @_github_plan_product = Billing::ProductUUID.where(zuora_product_rate_plan_id: rate_plan_ids, product_type: "github.plan").first
  end

  # Private: The GitHub Rate Plan that is active
  #
  # Returns Hash or nil
  def active_github_rate_plan
    return unless github_plan_product

    @_active_github_rate_plan ||= active_rate_plans.find do |rate_plan|
      rate_plan[:productRatePlanId] == github_plan_product.zuora_product_rate_plan_id
    end
  end

  def contracted_mrr
    Billing::Money.new(raw_subscription[:contractedMrr] * 100)
  end

  def initialize(zuora_id)
    @raw_subscription = GitHub.zuorest_client.get_subscription(zuora_id).with_indifferent_access
  end
end
