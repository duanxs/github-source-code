# frozen_string_literal: true

module AdvisoryDB
  # Strict case-sensitive validation used for persistence
  def self.valid_ghsa_id_pattern
    /GHSA(?:-[23456789cfghjmpqrvwx]{4}){3}/
  end

  # Case insensitive validation used for user-input values
  def self.valid_ghsa_id_input_pattern
    /GHSA(?:-[23456789cfghjmpqrvwx]{4}){3}/i
  end

  def self.canonical_case_for_ghsa_id(ghsa_id)
    return ghsa_id unless ghsa_id =~ valid_ghsa_id_input_pattern

    ghsa_id.partition("-").tap do |ghsa_id_components|
      ghsa_id_components.first.upcase!
      ghsa_id_components.last.downcase!
    end.join
  end
end
