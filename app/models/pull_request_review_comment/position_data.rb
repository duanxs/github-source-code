# frozen_string_literal: true

class PullRequestReviewComment

  # Public: A concrete PORO implementation of AbstractPositionData. Used for comments which
  # are already persisted to the DB using blob positioning.
  class PositionData < AbstractPositionData
    attr_reader :blob_position, :left_blob, :commit_oid, :path, :comment_path, :original_start_blob_position
    alias left_blob? left_blob

    def self.async_from_comment(comment)
      comment.async_pull_request.then(&:async_compare_repository).then(&:rpc).then do |rpc|
        new(
          comment_path:  comment.path,
          commit_oid:    comment.blob_commit_oid,
          left_blob:     comment.left_blob,
          path:          comment.blob_path,
          blob_position: comment.blob_position,
          original_start_blob_position: comment.original_start_blob_position,
          rpc:           rpc,
        )
      end
    end

    def initialize(comment_path:, commit_oid:, left_blob:, path:, blob_position:, rpc:, original_start_blob_position: nil)
      super(rpc: rpc)
      @comment_path  = comment_path
      @commit_oid    = commit_oid
      @left_blob     = left_blob
      @path          = path
      @blob_position = blob_position
      @original_start_blob_position = original_start_blob_position
    end

    def adjustment_source
      { commit_oid: commit_oid, path: path }
    end
  end
end
