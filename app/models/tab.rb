# rubocop:disable Style/FrozenStringLiteralComment

class Tab < ApplicationRecord::Domain::Repositories
  belongs_to :repository

  validates_presence_of :anchor, :url
  validate :url_protocol_allowed

  def url_protocol_allowed
    return if url.blank?
    uri = Addressable::URI.parse(url)
    return errors.add(:url, "must be absolute") unless uri.absolute?
    errors.add(:url, "uses an invalid protocol") unless ["http", "https"].include? uri.scheme
  rescue Addressable::URI::InvalidURIError
    errors.add(:url, "is invalid")
  end
end
