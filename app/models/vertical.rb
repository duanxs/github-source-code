# rubocop:disable Style/FrozenStringLiteralComment

class Vertical

  VERTICALS = {
    1 => :education,
    2 => :government,
    3 => :research,
    4 => :maintainer,
  }

  VERTICAL_TO_ID = VERTICALS.invert

  attr_accessor :id

  def initialize(id)
    raise ArgumentError, "Invalid vertical" unless Vertical.vertical?(id)
    @id = id
  end

  def name
    VERTICALS[id]
  end

  def to_s
    name.to_s
  end

  def inspect
    "#<Vertical id=#{id} name=\"#{name}\">"
  end

  def ==(vertical)
    vertical.id == id
  end

  class << self
    def find_by_name(name)  # rubocop:disable GitHub/FindByDef
      name = name.to_sym if VERTICALS.values.map(&:to_s).include?(name)
      id = VERTICAL_TO_ID[name]
      find_by_id(id)
    end

    def find_by_id(id)  # rubocop:disable GitHub/FindByDef
      all.find { |vertical| vertical.id == id }
    end
    alias_method :find, :find_by_id

    def ids
      VERTICALS.keys
    end

    def vertical?(id)
      ids.include?(id)
    end

    def all
      @all ||= ids.map { |id| self.new(id) }
    end
  end
end
