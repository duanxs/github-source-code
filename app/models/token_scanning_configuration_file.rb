# frozen_string_literal: true

class TokenScanningConfigurationFile
  DEFAULT_NAME = "secret_scanning.yml"
  MAX_FILE_SIZE = 1048576 # 1MB limit on file size
  MAX_NUMBER_OF_PATTERNS = 1000

  def initialize(tree_entry)
    @tree_entry = tree_entry unless tree_entry.size > MAX_FILE_SIZE
    record_instantiation_stats
  end

  def name
    @tree_entry ? @tree_entry.name : nil
  end

  def ignore_path?(path)
    begin
      ignore = false
      patterns = omitted_paths
      if patterns && (defined? patterns.each) && (defined? patterns.slice)
        patterns.first(MAX_NUMBER_OF_PATTERNS).each do |pattern| # read up to MAX_NUMBER_OF_PATTERNS patterns
          if File.fnmatch?(pattern, path, File::FNM_CASEFOLD)
            ignore = true
            break
          end
        end
      end
      ignore
    rescue Psych::SyntaxError => e
      GitHub.dogstats.increment("token_scan_config_syntax_error")
      return false
    end
  end

  def omitted_paths
    yaml ? yaml["paths-ignore"] : []
  end

  private

  def record_instantiation_stats
    GitHub.dogstats.increment("token_scan_config_instantiated")
  end

  def yaml
    @yaml ||= @tree_entry ? YAML.safe_load(@tree_entry.data) : nil
  end
end
