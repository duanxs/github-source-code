# frozen_string_literal: true

class PersonalReminder < ApplicationRecord::Collab
  include ReminderScheduling
  include ReminderEvents

  belongs_to :slack_workspace, -> (reminder) { for_remindable(reminder.remindable) },
    class_name: "ReminderSlackWorkspace", foreign_key: "reminder_slack_workspace_id"
  belongs_to :remindable, polymorphic: true
  belongs_to :user

  validates :slack_workspace, presence: true
  validates :remindable, presence: true
  validates :user, presence: true
  validate :check_slack_workspace

  scope :for_remindable, -> (remindable) { where(remindable: remindable) }

  def self.listener_ids_for(remindable, event_type:)
    for_remindable(remindable).for_event_type(event_type).pluck(:user_id)
  end

  def include_approved?
    ignore_after_approval_count.to_i < 1
  end

  def authorized_slack_workspaces
    slack_workspace_memberships = ReminderSlackWorkspaceMembership.where(user: user)
    slack_workspace_ids = slack_workspace_memberships.pluck(:reminder_slack_workspace_id)

    ReminderSlackWorkspace.where(remindable: remindable, id: slack_workspace_ids)
  end

  def check_slack_workspace
    return if slack_workspace.nil?

    if !authorized_slack_workspaces.include?(slack_workspace)
      self.slack_workspace = nil
      self.errors.add(:slack_workspace, "can't be blank")
    end
  end

  def filtered_pull_requests
    PersonalReminderPullRequestFilter.run(self)
  end

  def subscribed_to_events?
    event_types.present?
  end

  def reset_memoized_attributes
    @supports_private_repos = nil
  end

  def supports_private_repos?
    return false if remindable.nil?
    @supports_private_repos ||= remindable.plan_supports?(:reminders, visibility: "private", fallback_to_free: false)
  end

  def accessible_repository_ids
    org_repos =  if supports_private_repos?
      remindable.repository_ids
    else
      remindable.repositories.public_scope.pluck(:id)
    end
    user.associated_repository_ids & org_repos
  end

  def permitted_to_repository_id?(repository_id)
    accessible_repository_ids.include?(repository_id)
  end

  def has_valid_associations?
    remindable.present? && user.present?
  end
end
