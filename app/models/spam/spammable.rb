# rubocop:disable Style/FrozenStringLiteralComment

module Spam
  # Provides spam related scopes and methods to AR models
  # that are associated with a user who may or may not be
  # flagged as spammy.
  #
  # Used for things like PullRequest, IssueComment, etc.
  module Spammable
    extend ActiveSupport::Concern

    def self.add_class_including(klass)
      @tables_classes_including ||= {}
      @tables_classes_including[klass.table_name.to_sym] = klass
    end

    def self.tables_classes_including
      @tables_classes_including ||= {}
    end

    included do
      extend ClassMethods
      after_validation :set_user_hidden
    end

    module ClassMethods
      # Setup the scopes and methods for the name of the user
      # association (the potential spammer) used in the model.
      #
      # user_association - a Symbol, the name of the user association
      #                    (:user, :owner, :author, etc)
      def setup_spammable(user_association)
        spammable_has_simple_user = begin
          user_association = user_association.to_s
          reflection = reflections[user_association]
          unless reflection
            raise RuntimeError, "You must call 'setup_spammable' after the '#{user_association}' association is loaded"
          end
          is_belongs_to   = reflection.macro == :belongs_to
          unknown_options = reflection.options.keys - [:class_name, :foreign_key, :dependent, :as, :touch, :required, :optional]
          is_belongs_to && unknown_options.empty?
        end
        unless spammable_has_simple_user
          raise ArgumentError, "#{self}'s '#{user_association}' association is not a simple :belongs_to and cannot be used with spammable."
        end

        # a user can make an issue or PR on a repo, and then that repo can become
        # marked as spammy
        def requires_parent_repository_filtering?(table_name)
          true if table_name == "issues" || table_name == "pull_requests"
        end

        # Exclude records that are made by spammy users, unless the
        # viewer is staff or the spammer who created the record.
        #
        # Includes records that were created by users
        # who no longer exist (ghosts).
        #
        # viewer - The User we are rendering the records for.
        #          Usually should be current_user. Accomodates viewer
        #          being nil/false for anonymous users.
        # show_spam_to_staff - Override the assumption that staff should
        #                      see spam by setting this to `false`.
        #                      Defaults to `true`.
        scope :filter_spam_for, lambda { |viewer, show_spam_to_staff: true|
          return if !GitHub.spamminess_check_enabled?

          # We don't filter anything for staff
          return if viewer.try(:site_admin?) && show_spam_to_staff

          conditions = if viewer
            condition = "#{table_name}.user_hidden = false OR #{table_name}.#{spammable_user_foreign_key} = ?"
            [condition, viewer.id]
          else
            ["#{table_name}.user_hidden = false"]
          end

          if requires_parent_repository_filtering?(table_name)
            where(conditions).joins(:repository).where(repositories: { user_hidden: false })
          else
            where(conditions)
          end
        }

        # Records that are created by spammy users
        scope :spammy, lambda {
          where(user_hidden: true)
        }

        # Records that are not created by spammy users
        scope :not_spammy, lambda {
          return if !GitHub.spamminess_check_enabled?
          where(user_hidden: false)
        }

        define_method(:spammy?) do
          user_hidden?
        end

        define_method(:user_association_for_spammy) do
          user_association
        end

        define_method(:user_authored_content?) do |viewer|
          viewer.id != read_attribute(self.class.spammable_user_foreign_key)
        end

        define_method(:async_hide_from_user?) do |viewer|
          return Promise.resolve(false) unless GitHub.spamminess_check_enabled?
          return Promise.resolve(false) unless spammy?
          return Promise.resolve(true) if !viewer
          return Promise.resolve(false) if viewer.site_admin?

          return Promise.resolve(user_authored_content?(viewer)) unless self.is_a?(Repository)

          async_owner.then do |owner|
            # Don't hide organization-owned repositories from the org members
            if owner.organization? && !owner.hide_from_user?(viewer)
              false
            else
              user_authored_content?(viewer)
            end
          end
        end

        define_method(:hide_from_user?) do |viewer|
          async_hide_from_user?(viewer).sync
        end

        define_method(:set_user_hidden) do
          user = send(user_association)
          return unless user
          self.user_hidden = user.content_hidden?
        end

        define_singleton_method(:spammable_user_foreign_key) do
          user_association = user_association.to_s
          reflections[user_association].foreign_key
        end

        Spam::Spammable.add_class_including(self)
      end
    end

    # Enqueues a job that runs #check_for_spam on this record
    def enqueue_check_for_spam
      CheckForSpamJob.enqueue(self)
    end
  end
end
