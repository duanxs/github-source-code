# frozen_string_literal: true

class Timeline::IssueTimeline < Timeline::BaseTimeline
  # Internal: List of Platform type names which are not backed by the `IssueEvent` model
  NON_ISSUE_EVENT_TYPE_NAMES = %w(
    IssueComment
    CrossReferencedEvent
    ComposableComment
  ).freeze

  # Internal: Compiles the unsorted list of timeline placeholders for issues.
  def async_placeholders
    placeholders = []

    if requested_item_type_names.include?("IssueComment")
      placeholders.push async_issue_comment_placeholders
    end

    if requested_item_type_names.include?("CrossReferencedEvent")
      placeholders.push async_cross_reference_placeholders
    end

    if requested_item_type_names.include?("ComposableComment")
      placeholders.push async_composable_comment_placeholders
    end

    unless requested_issue_event_type_names.empty?
      placeholders.push async_issue_event_placeholders(requested_issue_event_type_names: requested_issue_event_type_names)
    end

    Promise.all(placeholders).then(&:flatten)
  end

  def async_issue_comment_placeholders
    Platform::Loaders::Timeline::Placeholders::IssueComment.load(subject.id, viewer)
  end

  def async_issue_event_placeholders(requested_issue_event_type_names: [])
    Platform::Loaders::Timeline::Placeholders::IssueEvent.load(subject.id, viewer, visible_events_only: filter_options[:visible_events_only], requested_issue_event_type_names: requested_issue_event_type_names)
  end

  def async_cross_reference_placeholders
    Platform::Loaders::Timeline::Placeholders::CrossReference.load(subject.id, viewer)
  end

  def async_composable_comment_placeholders
    Platform::Loaders::Timeline::Placeholders::ComposableComment.load(subject.id, viewer)
  end

  private

  def issue_event_type_names_for_issue
    Platform::Unions::IssueTimelineItems.possible_types.map(&:graphql_name)
  end

  # Filters out events that are not IssueEvent types
  # and events that are pull-request specific
  def requested_issue_event_type_names
    (requested_item_type_names - NON_ISSUE_EVENT_TYPE_NAMES) & issue_event_type_names_for_issue
  end
end
