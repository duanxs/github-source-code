# frozen_string_literal: true

class BraintreeMigration < ApplicationRecord::Domain::Users
  areas_of_responsibility :gitcoin
  belongs_to :user
end
