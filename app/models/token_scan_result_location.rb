# frozen_string_literal: true

class TokenScanResultLocation < ApplicationRecord::Notify
  areas_of_responsibility :token_scanning

  belongs_to :token_scan_result, touch: true
  alias_method :result, :token_scan_result

  validates_presence_of :path
  validates_length_of :blob_oid, :commit_oid, is: 40
  validates_numericality_of :start_line, :end_line, greater_than: -1
  validates_numericality_of :start_column, :end_column, greater_than: -1

  extend GitHub::Encoding
  force_utf8_encoding :path

  enum ignore_token: { include: 0, exclude_by_path: 1 }

  # Find or create a location for the given parent result and found token.
  #
  # Returns the location that was either created or already existed.
  def self.create_from_found_token!(result, found_token)
    attributes = {
      path: found_token.path,
      commit_oid: found_token.commit,
      start_line: found_token.start_line || 0,
      end_line: found_token.end_line || 0,
      start_column: found_token.start_column || 0,
      end_column: found_token.end_column || 0,
    }
    retry_on_find_or_create_error do
      token_location = result.locations.find_by(attributes) || result.locations.create!(attributes.merge({ blob_oid: found_token.blob }))
      if result.repository.token_in_config_excluded_path?(found_token.path)
        token_location.exclude_by_path!
        GitHub.dogstats.increment("token_scanning_location_excluded_on_creation")
      end
      GitHub.dogstats.increment("token_scanning_new_result_is_excluded") if result.locations.size == 1 && token_location.exclude_by_path?
      token_location
    end
  end

  def self.create_and_check_path(token_scan_result, location_data)
    token_location = token_scan_result.locations.create!(location_data)
    token_location.exclude_by_path! if token_scan_result.repository.token_in_config_excluded_path?(location_data[:path])
    token_location
  end

  def normalized_start_column
    start_column + 1
  end

  def normalized_end_column
    end_column + 1
  end

  # Retrieves the blob for this result location or nil if the blob could not be found.
  def blob
    return @blob if defined? @blob

    raw_blob = result.repository.rpc.read_blobs([blob_oid]).first
    @blob = TreeEntry.new(result.repository, raw_blob.merge("path" => path))
  rescue GitRPC::ObjectMissing
    @blob = nil
  end

  # Returns true if the location was found in an archive file. For example, zip, xlsx, pptx, docx etc.
  # These locations have start and end line of location set to 0.
  def found_in_archive?
    start_line.zero? && end_line.zero?
  end
end
