# frozen_string_literal: true

class ReviewRequestDelegationExcludedMember < ApplicationRecord::Collab
  belongs_to :team
  validates :team, presence: true

  belongs_to :user
  validates :user, presence: true
end
