# frozen_string_literal: true

class DiscussionCommentEdit < ApplicationRecord::Domain::Discussions
  include UserContentEdit::Core
  include FilterPipelineHelper
  include Instrumentation::Model

  belongs_to :discussion_comment, required: true

  alias_attribute :user_content_id, :discussion_comment_id
  alias_attribute :user_content, :discussion_comment
  alias_attribute :async_user_content, :async_discussion_comment

  def user_content_type
    "DiscussionComment"
  end

  def global_relay_id
    Platform::Helpers::NodeIdentification.to_global_id(platform_type_name, "DiscussionCommentEdit:#{id}")
  end
end
