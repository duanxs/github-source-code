# frozen_string_literal: true

class PullRequest
  class Update
    def initialize(pull:, actor:, author_email: nil, expected_head_oid: pull.head_sha)
      @pull = pull
      @actor = actor
      @author_email = author_email
      @expected_head_oid = expected_head_oid
    end

    def merge(base_oid: nil, conflict_resolutions: nil)
      check_push_permissions
      check_head_ref_position

      unless (merge_commit_sha = pull.create_merge_commit(skip_rebase: true))
        if !conflict_resolutions
          raise MergeConflictError, "merge conflict between base and head"
        end

        mergeable, merge_commit_sha = Prepare.new(pull: pull, base_oid: base_oid, skip_rebase: true).perform(conflict_resolutions: conflict_resolutions)
        if !mergeable
          raise MergeConflictError, "merge conflict not fully resolved"
        end
      end

      fetch_commit_from_base_repository_into_head_repository(merge_commit_sha)

      merge_commit = head_repository.commits.find(merge_commit_sha)
      _, merge_head_sha = merge_commit.parent_oids

      if merge_head_sha != expected_head_oid
        raise RefMismatch, "expected head oid value didn't match merge commit parent"
      end

      rewritten_merge_commit_sha = rewrite_merge_commit(merge_commit)
      update_head_ref_to(rewritten_merge_commit_sha)
      rewritten_merge_commit_sha
    end

    private
    attr_reader :pull, :actor, :author_email, :expected_head_oid

    def check_push_permissions
      unless pull.head_ref_pushable_by?(actor)
        if !pull.head_repository
          raise PermissionError, "head repository does not exist"
        else
          raise PermissionError, "user doesn't have permission to update head repository"
        end
      end
    end

    def check_head_ref_position
      unless head_ref
        raise HeadMissing, "head ref does not exist"
      end

      unless head_ref.target_oid == expected_head_oid
        raise RefMismatch, "expected head oid value didn't match current head ref"
      end
    end

    def fetch_commit_from_base_repository_into_head_repository(commit_id)
      return if base_repository == head_repository

      head_repository.rpc.fetch_commits(base_repository.shard_path, commit_id)
    end

    def rewrite_merge_commit(merge_commit)
      author = {
        "email" => author_email || actor.git_author_email,
        "name"  => actor.git_author_name,
        "time"  => actor.time_zone.now.iso8601,
      }

      committer = {
        "email" => GitHub.web_committer_email,
        "name"  => GitHub.web_committer_name,
        "time"  => author["time"],
      }

      merge_base_sha, merge_head_sha = merge_commit.parent_oids
      args = [[merge_head_sha, merge_base_sha], {
        "message"   => "Merge branch '#{pull.base_ref_name}' into #{head_ref.name}",
        "committer" => committer,
        "author"    => author,
        "tree"      => merge_commit.tree_oid,
      }]

      head_repository.rpc.create_tree_changes(*args, &base_repository.method(:sign_commit))
    end

    def update_head_ref_to(commit_id, forced: false)
      previous_commit_id = head_ref.target_oid

      head_ref.update(commit_id, actor, {
        # TODO: Make PullRequest#pr_reflog_data non-private
        reflog_data: pull.send(:pr_reflog_data, "merge base into head"),
      })

      pull.update_mergeable_attribute(nil)
    end

    def head_ref
      return @head_ref if defined?(@head_ref)
      @head_ref = head_repository.heads.find(pull.head_ref)
    end

    def head_repository
      pull.head_repository
    end

    def base_repository
      pull.base_repository
    end
  end
end
