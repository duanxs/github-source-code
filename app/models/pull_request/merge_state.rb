# rubocop:disable Style/FrozenStringLiteralComment

# Whether or not a pull request can be merged, and whether or not the tests are passing.
class PullRequest::MergeState
  class UnexpectedState < StandardError; end

  attr_reader :pull, :viewer

  def initialize(pull, viewer: nil)
    @pull   = pull
    @viewer = viewer
  end

  def mergeable
    pull.currently_mergeable?
  end

  def draft?
    pull.draft?
  end

  def clean_status?
    combined_status = pull.combined_status
    return true unless combined_status.any?
    combined_status.green?
  end

  def has_pre_receive_hooks?
    pull.repository.has_pre_receive_hooks?
  end

  def has_unsuccessful_required_status?
    false
  end

  def dirty_status?
    !clean_status?
  end

  def dirty?
    status == :dirty
  end

  def clean?
    status == :clean
  end

  def unstable?
    status == :unstable
  end

  def unknown?
    status == :unknown
  end

  def has_hooks?
    status == :has_hooks
  end

  def has_unverified_email?
    return false unless viewer
    repo = pull.repository
    authorization = ContentAuthorizer.authorize(viewer, :pull_request, :merge, repo: repo, owner: repo.owner)
    !authorization.authorized?
  end

  def admin_override_possible?
    if %i(blocked behind).include?(status) && can_update_ref?
      true
    elsif mergeable && viewer && protected_branch_policy_unfulfilled? &&
        protected_branch_policy_decision.reason_codes == [:merge_commit] && can_update_ref?
      true
    else
      false
    end
  end

  def status
    async_status.sync
  end

  def async_status
    @async_status ||=
      begin
        return Promise.resolve(:draft) if draft?
        return Promise.resolve(:dirty) if mergeable == false
        return Promise.resolve(:unknown) if mergeable.nil?
        return Promise.resolve(:blocked) if has_unverified_email?

        pull.async_historical_comparison.then do |comparison|
          if viewer && protected_branch_policy_unfulfilled?
            reason_codes = protected_branch_policy_decision.reason_codes || []
            case
            when reason_codes.include?(:unauthorized)
              :blocked
            when reason_codes.include?(:required_status_checks)
              if up_to_date? || loose_policy?
                :blocked
              else
                :behind
              end
            when reason_codes.include?(:review_policy_not_satisfied)
              :blocked
            when reason_codes.include?(:invalid_signature)
              :blocked
            when reason_codes.include?(:merge_commit)
              :clean
            when reason_codes.include?(:object_missing)
              :unknown
            else
              report_unexpected_state
              :blocked
            end
          elsif dirty_status?
            :unstable
          elsif has_pre_receive_hooks?
            :has_hooks
          elsif clean_status?
            :clean
          end
        end
      end
  end

  # Public: Is the merge of this PR blocked by the pull request review policy?
  def blocked_by_review_policy?
    viewer && review_policy_unfulfilled?
  end

  # Public: Is the merge of this PR blocked by the required signatures policy?
  def blocked_by_required_signatures?
    reason_codes = protected_branch_policy_decision.reason_codes || []
    reason_codes.include?(:invalid_signature)
  end

  # Public: Is the merge of this PR blocked by protected branch authorized users/teams?
  def blocked_by_unauthorized_protection?
    reason_codes = protected_branch_policy_decision.reason_codes || []
    reason_codes.include?(:unauthorized)
  end

  # Public: Is the merge of this PR blocked only by required linear history of the base branch?
  def blocked_only_by_required_linear_history?
    protected_branch_policy_decision.reason_codes == [:merge_commit]
  end

  # Public: Can a user merge this PullRequest into the head_ref?
  # This takes into account various policy decision around protected branches,
  # admin overrides, etc.  See ProtectedBranchPolicy for details.
  #
  # Returns a Boolean
  def can_update_ref?
    return unless decision = protected_branch_policy_decision
    decision.can_update_ref?
  end

  # Public: Return the ProtectedBranchPolicy for the current viewer for this Merge State
  #
  # This can also return False if there is not a valid merge commit
  #
  # Returns either the ProtectedBranchPolicy::Decision or False
  def protected_branch_policy_decision
    return @protected_branch_policy_decision if defined?(@protected_branch_policy_decision)

    @protected_branch_policy_decision = begin
      return false unless ref_update_for_merge
      ProtectedBranchPolicy.check_one(pull.repository, ref_update_for_merge, viewer, pull_request_review_policy_decision)
    end
  end

  # Public: The PullRequestReview::Policy for the current actor (ie viewer)
  # for this Pull Request
  #
  # Returns a PullRequestReview::Policy::Decision
  def pull_request_review_policy_decision
    return @pull_request_review_policy_decision if defined?(@pull_request_review_policy_decision)

    @pull_request_review_policy_decision = begin
      ref_update = ref_update_for_merge || Git::Ref::Update::Null.create_for_merge_conflict(pull)
      PullRequestReview::Policy.check_pull_request(pull_request: pull, actor: viewer, ref_update: ref_update)
    end
  end

  # Public: Return all the enforced PullRequestReview involved in determining the
  # protected branch decision.
  #
  # Returns a collection of PullRequestReview
  def reviews
    pull_request_review_policy_decision.reviews
  end

  def requested_changes?
    pull_request_review_policy_decision.instrumentation_payload[:has_requested_changes]
  end

  def required_approving_review_count
    pull.protected_base_branch&.required_approving_review_count
  end

  def review_policy_decision_reason_summary
    review_policy_decision_reason&.summary
  end

  def review_policy_decision_reason_message
    review_policy_decision_reason&.message
  end

  private

  def ref_update_for_merge
    return @ref_update_for_merge if defined?(@ref_update_for_merge)
    @ref_update_for_merge = init_ref_update_for_merge
  end

  # Setup the RefUpdate for the merge commit
  def init_ref_update_for_merge
    # Merge commit hasn't been built, so we can't merge yet
    return false if pull.merge_commit_sha.blank?
    # Branch has been deleted, so we can't merge
    return false if pull.current_base_sha.blank?

    refname    = "refs/heads/#{pull.base_ref_name}"
    before_oid = pull.current_base_sha
    after_oid  = pull.merge_commit_sha

    Git::Ref::Update.new(repository: pull.repository, refname: refname,
                         before_oid: before_oid, after_oid: after_oid)
  end

  def protected_branch_policy_unfulfilled?
    return false if pull.protected_base_branch.blank?

    decision = protected_branch_policy_decision
    return false if decision.blank?

    !decision.policy_fulfilled?
  end

  def up_to_date?
    !pull.behind_base?
  end

  def loose_policy?
    !pull.protected_base_branch.strict_required_status_checks_policy?
  end

  def review_policy_decision_reason
    pull_request_review_policy_decision.reason
  end

  def review_policy_not_satisfied_decision_reason
    decision_reason = review_policy_decision_reason
    decision_reason if decision_reason.code == :review_policy_not_satisfied
  end

  def review_policy_unfulfilled?
    review_policy_not_satisfied_decision_reason.present?
  end

  def report_unexpected_state
    boom = UnexpectedState.new("Protected branch policy failed for unexpected reasons #{protected_branch_policy_decision.reasons.map(&:code).inspect}")
    boom.set_backtrace(caller.join("\n"))
    Failbot.report_trace(boom, pull_request_id: pull.id)
  end
end
