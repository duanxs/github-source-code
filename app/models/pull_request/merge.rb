# rubocop:disable Style/FrozenStringLiteralComment

class PullRequest
  # Merge is a Service Object that handles all operations that are required to
  # prepare and perform the merge of a Pull Request.
  class Merge
    include MethodTiming

    class ValidateSuccessResult
      attr_reader :merge_commit
      attr_reader :merge_base_ref

      def initialize(merge_commit, merge_base_ref)
        @merge_commit = merge_commit
        @merge_base_ref = merge_base_ref
      end

      def success
        true
      end
    end

    class PerformSuccessResult
      attr_reader :new_sha

      def initialize(new_sha)
        @new_sha = new_sha
      end

      def success
        true
      end
    end

    class FailResult
      attr_reader :fail_message, :fail_code

      def initialize(fail_message, fail_code = nil)
        @fail_message = fail_message
        @fail_code = fail_code
      end

      def success
        false
      end
    end

    # pull - The PullRequest to merge.
    # method - Symbol that specifies the merge method to perform. Can be one of
    #          :merge, :squash or :rebase. (optional, defaults to :merge).
    # actor   - User object of the GitHub user initiating the merge
    #           (optional, defaults to PR creator).
    # author_email - Custom email the user has chosen for this merge commit
    #                 (optional, if user doesn't make a selection will default to git email)
    # message_title - String commit message title to prefix the merge message
    #                 (optional, defaults to the default
    #                 "Merge pull request #N from repo/branch" text).
    # message - String commit message to append to the merge message
    #           (optional, defaults to PR title).
    # merge_commit - A previously prepared merge commit.
    #                (optional, if not provided prepare_and_validate must be called prior to perform)
    def initialize(pull, method, actor, author_email: nil, message_title: nil, message: nil, merge_commit: nil)
      raise ArgumentError, "Unknown merge method" unless [:merge, :squash, :rebase].include? method
      raise ArgumentError, "Invalid email for web commit" if author_email && !actor&.author_emails.include?(author_email)

      @pull = pull
      @method = method
      @actor = actor
      @author_email = author_email
      @message_title = message_title
      @message = message
      @merge_commit = merge_commit

      if author_email
        email_type = actor.primary_user_email.email != author_email ? "custom" : "primary"
        GitHub.dogstats.increment("commit.custom_merge_email", tags: ["email:#{email_type}"])
      end
    end

    attr_reader :pull

    def merge_base_sha
      fail NO_MERGE_COMMIT_MSG if merge_commit.nil?

      merge_commit.parent_oids.first
    end

    # Public: Perform the merge described by a PullRequest.
    #
    # Requires that the merge was previously prepared by calling `#prepare`
    # (e.g. from a background job), or that a previously prepared
    # merge commit was passed to #initialize.
    #
    # Returns PerformSuccessResult or FailResult
    def perform
      fail NO_MERGE_COMMIT_MSG if merge_commit.nil?

      # FIXME: Use verification support of `dgit-update` to check
      # that `pull.base_ref` still points to
      # `merge_commit.parent_oids[0]` and `"refs/pull/:number/head"`
      # points to `merge_commit.parent_oids[1]`.
      #
      # If they do not, we can fail early.
      if method == :rebase
        perform_rebase(actor, merge_commit)
      elsif method == :squash
        perform_squash(actor, merge_commit, message_title, message)
      else
        perform_merge(actor, author_email, merge_commit, message_title, message)
      end
    end
    time_method :perform, key: "pullrequest.merge.perform"

    # Prepare and validate the PR's merge state prior to #perform.
    #
    # Creates merge commit when necessary.
    #
    # Returns ValidateSuccessResult or FailResult
    def prepare_and_validate(expected_head = nil)
      content_authorization = ContentAuthorizer.authorize(actor, :pull_request, :merge,
                                                          issue: pull.issue,
                                                          repo: repository)
      unless content_authorization.authorized?
        return FailResult.new(content_authorization.first_error.message, :denied)
      end

      if pull.draft?
        return FailResult.new("Pull Request is still a draft", :draft)
      end

      pull.maintain_tracking_ref(actor)

      # some old PRs got mergeable and merge_commit_sha out of sync.
      # we need to try creating the merge commit again. (#9675, github/enterprise-support#353)
      merge_commit_out_of_sync = pull.mergeable == true && pull.merge_commit_missing?
      pull.create_merge_commit if merge_commit_out_of_sync || pull.mergeable.nil?

      return FailResult.new("Pull Request is not mergeable", :not_mergeable) if pull.mergeable != true

      merge_base_ref = pull.base_repository.heads.find(pull.base_ref.b)
      return FailResult.new("Base branch no longer exists", :base_missing) if merge_base_ref.nil?

      if expected_head.present? && expected_head != pull.head_sha
        return FailResult.new("Head branch was modified. Review and try the merge again.", :head_mismatch)
      end

      if pull.base_repository
        if method == :merge && !pull.base_repository.merge_commit_allowed?
          return FailResult.new("Merge commits are not allowed on this repository.", :merge_commit_blocked)
        end

        if method == :squash && !pull.base_repository.squash_merge_allowed?
          return FailResult.new("Squash merges are not allowed on this repository.", :squash_merge_blocked)
        end

        if method == :rebase && !pull.base_repository.rebase_merge_allowed?
          return FailResult.new("Rebase merges are not allowed on this repository.", :rebase_merge_blocked)
        end
      end

      # grab the merge commit from PR repo
      merge_commit = repository.commits.find(pull.merge_commit_sha)
      merge_base_sha, merge_head_sha = merge_commit.parent_oids

      # make sure that the parents of the merge commit are up to date
      if merge_base_sha != merge_base_ref.target_oid || merge_head_sha != pull.mergeable_head_sha
        pull.update_mergeable_attribute(nil)
        raise Git::Ref::ComparisonMismatch
      end

      # TODO We should also check that the head SHA1 has not moved by reading
      # directly from the repository here. It's possible that the PR has not been
      # synchronized in which case we could potentially merge an old head commit.
      # The commit would never have been displayed on the PR though at least.

      @merge_commit = merge_commit

      ValidateSuccessResult.new(merge_commit, merge_base_ref)
    end

  private

    attr_reader :method, :actor, :author_email, :merge_commit, :message_title, :message

    NO_MERGE_COMMIT_MSG = "No merge commit. Either provide one in initialize or call validate_and_prepare before perform.".freeze

    def repository
      pull.repository
    end

    # Does the base branch have a required signatures that cannot be
    # overridden by the actor?
    #
    # Returns boolean.
    def signature_required?(actor:)
      pull.protected_base_branch&.required_signatures_enabled? &&
        !pull.protected_base_branch.can_override_required_signatures?(actor: actor)
    end

    def perform_rebase(actor, merge_commit)
      ref = repository.refs.read(pull.rebase_ref)
      unless ref.exist?
        return FailResult.new("This branch can't be rebased", :rebase)
      end

      unless ref.target.tree_oid == merge_commit.tree_oid
        return FailResult.new("Base branch was modified. Review and try the merge again.", :rebase)
      end

      if signature_required?(actor: actor)
        return FailResult.new("Base branch requires signed commits. Rebase merges cannot be automatically signed by #{GitHub.flavor}", :protected_branch)
      end

      new_sha = repository.rpc.update_committer_info(ref.target.oid, merge_commit.parent_oids[0], {
        name: actor.git_author_name,
        email: actor.git_author_email,
        time: actor.time_zone.now.iso8601,
      })

      begin
        repository.batch_write_refs(actor, [[pull.rebase_ref, nil, new_sha]])
      rescue Git::Ref::ComparisonMismatch
        return FailResult.new("Base branch was modified. Review and try the merge again.", :rebase)
      end

      PerformSuccessResult.new(new_sha)
    end
    time_method :perform_rebase, key: "pullrequest.merge.perform_rebase"

    def perform_merge(actor, author_email, merge_commit, message_title, message)
      if pull.draft?
        return FailResult.new("Pull Request is still a draft", :draft)
      end

      # generate a commit message
      commit_message = message_title.presence || pull.default_merge_commit_title
      commit_message += if message
        "\n\n" + message
      elsif pull.title
        "\n\n" + pull.title
      else
        ""
      end

      author = {
        "email" => author_email || actor.git_author_email,
        "name"  => actor.git_author_name,
        "time"  => actor.time_zone.now.iso8601,
      }

      committer = {
        "email" => GitHub.web_committer_email,
        "name"  => GitHub.web_committer_name,
        "time"  => author["time"],
      }

      info = {
        "committer" => committer,
        "author" => author,
        "message" => commit_message,
      }

      args = [merge_commit.oid, info, false]

      new_sha = repository.rpc.rewrite_merge_commit(*args) do |commit_body|
        if signature = repository.sign_commit(commit_body)
          signature
        elsif signature_required?(actor: actor)
          return FailResult.new("The base branch requires that commits be signed and we failed to sign your merge commit. Please try again.", :protected_branch)
        else
          nil # continue without signature
        end
      end

      if author_email
        ActiveRecord::Base.connected_to(role: :writing) do
          # Set this as the default web commit email
          GitHub.kv.set(actor.default_author_email_cache_key(repository), author_email)
        end
      end

      return PerformSuccessResult.new(new_sha)
    end
    time_method :perform_merge, key: "pullrequest.merge.perform_merge"

    def perform_squash(actor, merge_commit, message_title, message)
      author = pull.user || actor
      commit_message = message_title.presence || pull.default_squash_commit_title
      commit_message += "\n\n" + (message || pull.default_squash_commit_message(author: author))

      now = Time.current
      data = {
        "author" => {
          "email" => author.default_author_email(pull.repository, pull.head_sha) || author.git_author_email,
          "name"  => author.git_author_name,
          "time"  => now.in_time_zone(author.time_zone).iso8601,
        },
        "committer" => {
          "email" => GitHub.web_committer_email,
          "name"  => GitHub.web_committer_name,
          "time"  => now.iso8601,
        },
        "message" => commit_message,
      }

      args = [merge_commit.oid, data, true]

      new_sha =
        repository.rpc.rewrite_merge_commit(*args) do |commit_body|
          if signature = repository.sign_commit(commit_body)
            signature
          elsif signature_required?(actor: actor)
            return FailResult.new("The base branch requires that commits be signed and we failed to sign your squash commit. Please try again.", :protected_branch)
          else
            nil # continue without signature
          end
        end

      return PerformSuccessResult.new(new_sha)
    end
    time_method :perform_squash, key: "pullrequest.merge.perform_squash"
  end
end
