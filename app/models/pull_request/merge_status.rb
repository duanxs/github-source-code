# rubocop:disable Style/FrozenStringLiteralComment

class PullRequest::MergeStatus < CombinedStatus
  attr_reader :target_branch

  def initialize(repo, sha, target_branch:, protected_target_branch: nil, **kwargs)
    @target_branch = target_branch
    @protected_target_branch = protected_target_branch if protected_target_branch

    unless target_branch.is_a?(String)
      raise ArgumentError, "PR combined status missing target branch"
    end

    super(repo, sha, **kwargs)
  end

  def self.merge_statuses_for_pulls(repo, pulls)
    current_by_sha = Statuses::Service.current_by_sha(repository_id: repo.id, shas: pulls.map(&:head_sha).compact)
    protected_branches = pulls.map(&:protected_base_branch).compact.index_by(&:name)
    combined_statuses = {}

    pulls.each do |pull|
      sha = pull.head_sha
      next unless sha

      statuses_for_sha = current_by_sha.fetch(sha, [])
      protected_target_branch = protected_branches[pull.base_ref_name]

      combined_statuses[sha] = new(repo, sha,
        statuses: statuses_for_sha,
        target_branch: pull.base_ref_name,
        protected_target_branch: protected_target_branch
      )
    end
    combined_statuses
  end

  # Are there any pending/expected statuses?
  #
  # Returns Boolean
  def incomplete?
    statuses.any? { |status| %w[pending expected].include?(status.state) }
  end

  # Return the latest status for each context for the repository with expected
  # statuses. See CombinedStatus#status_checks.
  #
  # Returns an Array of Statuses
  def status_checks
    @pr_memoized_statuses ||= prepend_expected_statuses(super)
  end

  private

  # Internal: Prepend any branch status policy expected statuses to a list of
  # statuses. If an expected context already has a status created, we'll omit
  # the expected status.
  #
  # statuses - Array of Statuses
  #
  # Returns an Array of Statuses.
  def prepend_expected_statuses(statuses)
    if protected_branch = protected_target_branch
      protected_branch.current_statuses_with_expected(statuses)
    else
      statuses
    end
  end

  def protected_target_branch
    return @protected_target_branch if defined?(@protected_target_branch)

    @protected_target_branch = ProtectedBranch.for_repository_with_branch_name(repository, target_branch)
  end
end
