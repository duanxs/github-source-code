# rubocop:disable Style/FrozenStringLiteralComment

class PullRequest
  # A PR specific Comparison state.
  #
  # A PR will have many comparison states over its lifetime. This class can
  # represent the latest and all previous states of a PR as well as ranges of
  # commits within the PR.
  #
  # Diverged from ::Comparison to add PR specific features. Eventually it might
  # be useful to PullRequest::Comparison < ::Comparison or define some sort
  # of minimal shared Comparison interface.
  class Comparison
    include Scientist

    # Public: Parent PullRequest record.
    attr_reader :pull

    # Public: Head Commit of the start of a commit range.
    attr_reader :start_commit

    # Public: Head Commit of the end of the commit range.
    attr_reader :end_commit

    # Public: Merge base Commit.
    attr_reader :base_commit

    attr_reader :ignore_whitespace

    attr_accessor :diff_options

    # Public: Initialize new Comparison.
    #
    # pull         - The parent PullRequest
    # start_commit - Head start Commit
    # end_commit   - Head end Commit
    # base_commit  - Merge base Commit
    def initialize(pull:, start_commit:, end_commit:, base_commit:)
      unless pull.is_a?(PullRequest)
        raise TypeError, "expected pull to be a PullRequest, but was #{pull.class}"
      end
      @pull = pull

      unless start_commit.is_a?(Commit)
        raise TypeError, "expected start_commit to be a Commit, but was #{start_commit.class}"
      end
      @start_commit = start_commit

      unless end_commit.is_a?(Commit)
        raise TypeError, "expected end_commit to be a Commit, but was #{end_commit.class}"
      end
      @end_commit = end_commit

      unless base_commit.is_a?(Commit)
        raise TypeError, "expected base_commit to be a Commit, but was #{base_commit.class}"
      end
      @base_commit = base_commit

      @diff_options = {}
    end

    # Public: Find new Comparison.
    #
    # pull             - The parent PullRequest
    # start_commit_oid - Head start Commit
    # end_commit_oid   - Head end Commit
    # base_commit_oid  - Merge base Commit
    #
    # Returns Comparison if commit objects are available.
    # Returns nil if any of the requested commits does not exist.
    def self.find(pull:, start_commit_oid:, end_commit_oid:, base_commit_oid:)
      async_find(
        pull: pull,
        start_commit_oid: start_commit_oid,
        end_commit_oid: end_commit_oid,
        base_commit_oid: base_commit_oid,
      ).sync
    end

    def self.async_find(pull:, start_commit_oid:, end_commit_oid:, base_commit_oid:)
      return Promise.resolve(nil) unless pull

      commit_oids = [start_commit_oid, end_commit_oid, base_commit_oid]

      all_commit_oids_valid = commit_oids.all? { |oid| GitRPC::Util.valid_full_sha1?(oid) }
      return Promise.resolve(nil) unless all_commit_oids_valid

      pull.async_historical_comparison.then do |comparison|
        comparison.async_load_commits(commit_oids).then do |start_commit, end_commit, base_commit|
          next nil unless start_commit && end_commit && base_commit

          new(pull: pull, start_commit: start_commit, end_commit: end_commit, base_commit: base_commit)
        end
      end
    end

    def repository
      pull.compare_repository
    end

    # Public: Set the ignore whitespace setting for diff. Changing this invalidates the cached diff.
    def ignore_whitespace=(value)
      @diffs = nil
      @ignore_whitespace = value
    end

    # Public: Initialize Diff for commit range.
    #
    # Returns unloaded GitHub::Diff object.
    def diffs
      @diffs ||= build_diff
    end

    def diffs=(value)
      @diffs = value
    end

    # Public: Return commit oids included in the PR range.
    #
    # Returns Array of String commit OIDs.
    def commit_oids
      @commit_oids ||= repository.rpc.rev_list(end_commit.oid, exclude_oids: [start_commit.oid, base_commit.oid].compact, reverse: true, limit: 250)
    end

    # Public: Return commits included in the PR range.
    #
    # Returns Array of Commit objects.
    def commits
      @commits ||= repository.commits.find(commit_oids.to_a)
    end

    # Public: Find all diff/review threads for this comparison.
    #
    # viewer - A User that is viewing the review comments. Maybe nil for
    #          anonymous users.
    #
    # Returns a ReviewThreads collection.
    def review_threads(viewer:)
      diffs.load_diff

      thread_positioner(viewer: viewer).positioned_threads
    end

    # Public: Get a thread positioner scoped to the given viewer.
    #
    # viewer - The User that is viewing the review comments. May be nil for
    #          anonymous users.
    def thread_positioner(viewer:)
      @thread_positioner ||= {}
      @thread_positioner[viewer] ||= PullRequest::ThreadPositioner.new(viewer, self)
    end

    # Public: Initialize review thread for diff entry path and position.
    #
    # path     - String diff entry path
    # position - Integer offset into diff hunk
    #
    # Returns DeprecatedPullRequestReviewThread.
    def review_thread(path:, position:)
      DeprecatedPullRequestReviewThread.new(pull_comparison: self, path: path, position: position, comments: [])
    end

    def total_commits_size
      pull.changed_commit_oids.size
    end

    def range?
      start_commit.oid != base_commit.oid
    end

    def current?
      start_commit.oid == base_commit.oid && end_commit.oid == pull.head_sha
    end

    def commit_before(commit)
      commits = pull.changed_commits
      if idx = commits.index { |c| c.oid == commit.oid }
        commits[idx-1] if idx > 0
      end
    end

    def commit_after(commit)
      commits = pull.changed_commits
      if idx = commits.index { |c| c.oid == commit.oid }
        commits[idx+1]
      end
    end

    # Public: Whether a given PullRequestRevision is inclusively within a range of
    #         revisions covered by start_commit and end_commit.
    #
    # revision - A PullRequestRevision to check against the current range, if any.
    #
    # Returns false if start_commit and end_commit don't map to the beginning and
    # end of a contiguous series of PullRequestRevisions.
    #
    # Returns true otherwise if revision is within that series, inclusive.
    #
    # Returns a Boolean.
    def revision_in_range?(revision)
      return false unless revision
      return false if revision_range.empty?
      return true if revision_range.map(&:number).include?(revision.number)
      return false unless revision_range.many?
      (revision_range.first.number..revision_range.second.number).include?(revision.number)
    end

    # Public: The start and end of the range of revisions exactly covered by
    #          start_commit..end_commit, if any.
    #
    # Returns an Array of 0 to 2 PullRequestRevisions.
    def revision_range
      return @revision_range if defined?(@revision_range)

      start_revision = pull.revisions.ready.number_desc.where(base_oid: start_commit.oid).first
      end_revision = pull.revisions.ready.number_desc.where(head_oid: end_commit.oid).first

      if start_revision && end_revision
        @revision_range = [start_revision, end_revision].uniq
      else
        @revision_range = []
      end
    end

    # Public: Mark Pull Request Comparison as seen by user.
    #
    # user - The User viewing comparison
    #
    # Returns nothing.
    def mark_as_seen(user:)
      unless user.is_a?(User)
        raise TypeError, "expected user to be a User, but was #{user.class}"
      end

      ranges = LastSeenPullRequestRevision.where(pull_request_id: pull.id, user_id: user.id)
      if !ranges.find { |r| r.last_revision == self.end_commit.oid }
        # only mark ranges that are descendants of the previous marker. so if you look at an old
        # revision, it doesn't add a nonsense marker. force pushes won't work with this, though.
        if ranges.last
          has_ancestor = false
          is_descendant = false
          has_commits_authored_by_other_people = false
          ancestor = ranges.last.last_revision
          pull.changed_commits.each do |commit|
            if commit.oid == ancestor
              has_ancestor = true
              next
            end

            if has_ancestor && commit.oid == self.end_commit.oid
              is_descendant = true
              break
            end

            has_commits_authored_by_other_people = true if commit.author != user
          end

          if is_descendant && has_commits_authored_by_other_people
            LastSeenPullRequestRevision.add_seen_rev(pull, user, self.end_commit.oid)
          end
        else
          LastSeenPullRequestRevision.add_seen_rev(pull, user, self.end_commit.oid)
        end
      end
    end

    private

    def build_diff
      diff_options[:ignore_whitespace] = ignore_whitespace unless ignore_whitespace.nil?
      # annotations are unbound, so we need to add a limit
      diff_options[:context_lines] = end_commit.annotations(limit: CheckAnnotation::MAX_READ_LIMIT)&.line_ranges

      GitHub::Diff.new(repository, start_commit.oid, end_commit.oid,
                       diff_options.merge(base_sha: base_commit.oid))
    end
  end
end
