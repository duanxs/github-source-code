# rubocop:disable Style/FrozenStringLiteralComment

class PublicKey < ApplicationRecord::Domain::Users
  include MethodTiming

  COLON_SEPARATED_MD5_LENGTH = 47 # 32 hex chars + 15 separators (':')

  module CreationExtension
    # Public: Create a verified public key, belonging to the association owner
    # (e.g., the User or Repository).
    #
    # attributes - The Hash of attributes used to construct the key.
    #              :key                 - The String public key value.
    #              :verifier            - The User that verified the key.
    #              :title               - The String title for the key (optional).
    #              :oauth_authorization - The OauthAuthorization that initiated
    #                                     the request
    #              :read_only           - The Boolean value indicating whether
    #                                     this key can only be used for reading.
    #
    # Examples
    #
    #   # Reference the extension when setting up the association
    #   class Repository < ApplicationRecord::Domain::Repositories
    #     has_many :public_keys, :extend => PublicKey::CreationExtension
    #   end
    #
    #   # Put it to use
    #   repo_instance.public_keys.create_with_verification \
    #     :key => 'ssh-rsa AAAAB...',
    #     :verifier => current_user
    #
    # Returns the new PublicKey. Callers can invoke #new_record? on the
    #   PublicKey to determine if the key was successfully persisted.
    def create_with_verification(attributes = {})
      public_key = build(
        key: attributes[:key],
        title: attributes[:title],
        read_only: attributes[:read_only] == true,
      )

      if authorization = attributes[:oauth_authorization]
        public_key.oauth_authorization = authorization

        # set oauth app foreign key for more efficient database join queries
        if authorization.oauth_application_authorization?
          public_key.oauth_application_id = authorization.application_id
        end
      end

      public_key.verify(attributes[:verifier])
      public_key
    end
  end

  belongs_to :user
  belongs_to :repository
  belongs_to :creator,  class_name: "User"
  belongs_to :verifier, class_name: "User"
  belongs_to :oauth_authorization

  has_many :active_org_credential_authorizations,
    -> { active },
    class_name: "Organization::CredentialAuthorization",
    as: :credential,
    inverse_of: :credential,
    foreign_key: "credential_id",
    dependent: :destroy

  has_many :two_factor_recovery_requests

  before_save       :set_fingerprint, :set_defaults
  # NOTE: set_title must run before strip_comments since it extracts the title
  # from the comments.
  before_validation :strip_whitespace, :set_title, :strip_prefix, :strip_comments
  before_validation :set_created_by, on: :create

  after_create :link_revoked_organization_credential_authorization
  after_create_commit :instrument_creation
  after_update_commit :instrument_update, unless: :saved_change_to_unverification_reason?
  before_destroy :generate_webhook_payload
  after_commit :instrument_deletion, :queue_webhook_delivery, on: :destroy

  after_commit  :notify_creation, on: :create

  validate :key_type_validity
  validate :key_validity
  validate :uncompromised_key
  validate :key_length, on: :create
  validate :not_weak_key, on: :create
  validate :uniqueness_of_key
  validate :user_or_repository_assigned
  validate :user_has_email, on: :create
  validate :consistency_between_created_by_and_oauth_authorization_id
  validate :only_read_only_if_repo_key
  validates_inclusion_of :created_by, in: %w(user oauth_access unknown)

  after_destroy :destroy_oauth_authorization, if: :oauth_authorization

  alias_attribute :to_s, :key

  # Records the association that is being destroyed and destroying this record
  # in the process.
  attr_accessor :destroyed_by_association_parent

  VALID_UNVERIFY_REASONS = {
    # Whenever a GitHub Employee is hired, we revoke their OAuth applications
    # and SSH keys so that 3rd parties don't inadvertently get access to GitHub code.
    new_hire: "unverified upon becoming a GitHub employee",

    # Keys are automatically unverified if they have not been used since
    # GitHub::Transitions::UnverifyStaleSshKeys::FreshUntil.ago
    stale: "unverified due to lack of use",

    # Keys are automatically unverified if we find the corresponding unencrypted
    # private key in a public repository.
    token_scan: "unverified automatically (private key found in a public repository)",

    # Keys are automatically unverified if we find the corresponding unencrypted
    # private key in a public repository.
    site_admin: "unverified by #{GitHub.host_name} administrator",
  }.freeze

  VALID_DESTROY_REASONS = {
    # Keys are automatically unverified if they have not been used
    # in over a year
    stale: "unverified due to lack of use",

    # Keys which have been manually removed by the user
    removed_by_user: "Deleted by the user",
  }.freeze

  DEFAULT_UNVERIFICATION_REASON = "legacy key automatically unverified".freeze

  PUBLIC_KEY_RECOMMENDATION = "GitHub recommends using ssh-keygen to generate a RSA key of at least 2048 bits.".freeze
  KEY_TYPE_RSA = "ssh-rsa".freeze
  KEY_TYPE_DSS = "ssh-dss".freeze

  def self.unverification_explanation(reason)
    VALID_UNVERIFY_REASONS[reason.try(:to_sym)] || DEFAULT_UNVERIFICATION_REASON
  end

  def self.destroy_explanation(reason)
    VALID_DESTROY_REASONS[reason.try(:to_sym)]
  end

  # Public: Scopes a query to public keys in repositories for which the
  # given User or Organization sets the access policy.
  #
  # user_or_org - User or Organization instance, or Integer record ID
  #
  # Returns an array of `Repository` ids
  def self.repository_ids_for_policymaker(policymaker)
    Repository.github_sql.values(<<-SQL, policymaker_id: policymaker.id)
      SELECT repositories.id
      FROM repositories
      WHERE repositories.public = 1 AND repositories.owner_id = :policymaker_id

      UNION

      SELECT repositories.id
      FROM repositories
      INNER JOIN repository_networks
        ON repository_networks.id = repositories.source_id
      INNER JOIN repositories network_roots
        ON network_roots.id = repository_networks.root_id
      WHERE repositories.public = 0 AND network_roots.owner_id = :policymaker_id
    SQL
  end

  # Public: Scopes a query to public keys created by a particular
  # OAuth Application
  #
  # app_id - OauthApplication instance, or Integer record ID
  #
  # Returns an ActiveRecord::NamedScope::Scope
  def self.created_by_application(oauth_app)
    app_id = case oauth_app
    when Integer then oauth_app
    when OauthApplication then oauth_app.id
    end

    raise ArgumentError, "OAuthApplication is required" if app_id.nil?

    joins(:oauth_authorization).where("oauth_authorizations.application_id" => app_id)
  end

  # Public: Scopes a query to public keys that are verified
  #
  # Returns an ActiveRecord::Relation
  def self.verified
    where("verified_at IS NOT NULL")
  end

  # Public: Scopes a query to public keys that are NOT verified
  #
  # Returns an ActiveRecord::Relation
  def self.unverified
    where("verified_at IS NULL")
  end

  # Public: Scopes a query to public keys accessed since a certain time
  #
  # time = the Time to query keys accessed since
  #
  # Returns an ActiveRecord::Relation
  def self.accessed_since(time)
    where(["accessed_at >= (?)", time])
  end

  # Public: Scopes a query to public keys used since a certain time
  #
  # order     = 'accessed' optional, defaults to order by 'created_at'
  # direction = 'asc' or 'desc' optional, defaults to 'desc'
  #
  # Returns an ActiveRecord::Relation
  def self.sorted_by(order = "created", direction)
    case order
    when /accessed/
      order = "accessed_at"
    else
      order = "created_at"
    end

    order << (direction == "asc" ? " ASC" : " DESC")
    order(order)
  end

  def key_type_validity
    return if !errors[:key].empty?
    allowed_algos = [SSHData::PublicKey::ALGO_RSA, SSHData::PublicKey::ALGO_ECDSA256, SSHData::PublicKey::ALGO_ECDSA384, SSHData::PublicKey::ALGO_ECDSA521, SSHData::PublicKey::ALGO_ED25519]

    unless allowed_algos.include?(parsed_key.algo)
      errors.add :key, "is invalid. It must begin with #{allowed_algos.map { |k| "'#{k}'" }.join(", ")}. Check that you're copying the public half of the key"
    end

  rescue SSHData::Error
    errors.add(:key, "is invalid. You must supply a key in OpenSSH public key format")
  end

  def user_has_email
    return if GitHub.enterprise? || repository

    if user && AccountMailer::Helpers.user_email(user).blank?
      errors.add :base, "To be extra safe, please add an email address to your account before creating a public key."
    end
  end

  def ==(other)
    to_s == other.to_s
  end

  def title
    self[:title].blank? && key ? key[0..25] : self[:title]
  end

  # Public: Returns true if the key has been verified (and a timestamp has been
  # set). LDAP synced keys are verified during the sync process.
  def verified?
    !verified_at.nil?
  end

  # Public: Returns true if the key is verified, or if the actor owns the key.
  def readable_by?(actor)
    verified? || user == actor
  end

  def can_verify_account_ownership?
    return false unless verified?
    return false unless oauth_authorization.nil? || oauth_application.try(:github_desktop?)
    return true
  end

  def verify(verifier)
    self.created_by = "user" if created_by_unknown?

    unless self.verified?
      self.unverification_reason = nil
      self.creator ||= verifier
      self.verifier = verifier
      self.verified_at = Time.now
    end

    if save_result = self.save
      GitHub.dogstats.increment("public_key", tags: ["action:verify", "valid:true"])
      instrument :verify
    else
      GitHub.dogstats.increment("public_key", tags: ["action:verify", "valid:false"])
      instrument :verification_failure, reason: "key #{errors[:key].join(', ')}"
    end

    save_result
  end

  def unverification_explanation
    self.class.unverification_explanation(unverification_reason)
  end

  def unverify(explanation_key)
    unless VALID_UNVERIFY_REASONS.key?(explanation_key)
      fail ArgumentError, "invalid unverify reason: #{explanation_key}"
    end

    self.verifier = nil
    self.verified_at = nil
    self.unverification_reason = explanation_key
    if self.save
      GitHub.dogstats.increment("public_key", tags: ["action:unverify", "valid:true", "explanation:#{explanation_key}"])
      instrument :unverify, explanation: explanation_key
    else
      GitHub.dogstats.increment("public_key", tags: ["action:unverify", "valid:false", "explanation:#{explanation_key}"])
      instrument :unverification_failure, reason: "key #{errors[:key].join(', ')}"
    end
  end

  # Public: Returns true if we are sure that the key was created by the user
  # directly or via a personal access token. Returns false otherwise.
  #
  # Keys created prior to github/github##20015 are of "unknown origin." We don't
  # know whether those keys were created by the user or by an OAuth app that the
  # user authorized. This method returns false for those keys.
  #
  # As the name implies, personal access tokens are *personal*. Using one to
  # create a key implies that the user is directly responsible for the creation
  # of the key.
  #
  # Returns a Boolean.
  def created_by_user?
    created_by == "user" || created_by_personal_access_token?
  end

  def deploy_key?
    !!repository
  end

  # Public: Returns true if the key was created by an OAuth application. Returns
  # false if the key was created by a personal access token, or if the key was
  # not created via OAuth.
  #
  # Returns a Boolean.
  def created_by_oauth_application?
    oauth_authorization && !oauth_authorization.personal_access_authorization?
  end

  # Public: Returns true if the key was created by a personal access token.
  # Returns false otherwise.
  #
  # Returns a Boolean.
  def created_by_personal_access_token?
    oauth_authorization && oauth_authorization.personal_access_authorization?
  end

  # Public: Returns true if the key was created by an oauth application and has
  # access to some of the given scopes.
  # Returns false otherwise.
  #
  # Returns a Boolean.
  def oauth_access?(*scopes)
    oauth_authorization && scopes.any? { |scope| oauth_authorization.scopes.include?(scope.to_s) }
  end

  # Public: Returns true if the key is of unknown origin. (All keys created
  # prior to github/github##20015 are of unknown origin. We don't know whether
  # those keys were created by the user or by an OAuth app that the user
  # authorized.)
  #
  # Returns a Boolean.
  def created_by_unknown?
    created_by == "unknown"
  end

  # Public: Returns the OAuth application (if any) that created this key.
  #
  # Returns an OauthApplication or nil.
  def oauth_application
    return nil unless created_by_oauth_application?

    oauth_authorization.application
  end

  # Public: Determine whether the given user is authorized to administer this
  # key.
  #
  # Returns a Boolean.
  def adminable_by?(user)
    user == self.owner || user.deploy_keys.include?(self)
  end

  # Public: The user or organization that owns this key. For deploy keys, we
  # consider the repository's owner to be the key's owner.
  #
  # Returns a User or Organization.
  def owner
    if repository
      repository.owner
    else
      user
    end
  end

  # Is this a repo key?
  #
  # Returns boolean.
  def repository_key?
    self.repository.present?
  end

  def key_type
    @key_type ||= key.split[0]
  end

  # Internal: Participates in abilities on behalf of this model.
  def ability_delegate
    owner
  end

  protected

  def notify_user_key_action_via_email?
    self.user && AccountMailer::Helpers.user_email(self.user).present?
  end

  def strip_whitespace
    self[:key] = self.class.strip_whitespace(self[:key])
  end

  def set_title
    if self[:title].blank? && self[:key] !~ /[\n\r]/ && !self[:key].split(" ")[2].blank?
      self[:title] = self[:key].split(" ")[2]
    end
  end

  def set_created_by
    self.created_by = oauth_authorization ? "oauth_access" : "user"
  end

  def strip_prefix
    self[:key] = self.class.strip_prefix(self[:key])
  end

  def strip_comments
    self[:key] = self.class.strip_comments(self[:key]) if self[:key] !~ /[\n\r]/
  end

  def set_defaults
    self.username = "git"
  end

  # Prevents users deleting and adding their public keys to get around
  # organizations that have revoked their credentials.
  def link_revoked_organization_credential_authorization
    Organization::CredentialAuthorization\
      .public_key_credentials_by_fingerprint(fingerprint: fingerprint)\
      .revoked.update_all(credential_id: id)
  end

  # Internal: validate that this key is not compromised
  def uncompromised_key
    return if !errors[:key].empty?

    if fingerprint.present? && GitHub::SSH.blacklisted_fingerprint?(fingerprint)
      errors.add :key, "is blacklisted because its private key has been compromised."
    end
  end

  def set_fingerprint
    self.fingerprint = begin
      parsed_key.fingerprint(md5: true)
    rescue SSHData::Error
      "bogus"
    end
  end

  def key_bit_length
    case parsed_key.algo
    when SSHData::PublicKey::ALGO_RSA
      parsed_key.openssl.params["n"].num_bits
    when SSHData::PublicKey::ALGO_DSA
      parsed_key.openssl.params["p"].num_bits
    when SSHData::PublicKey::ALGO_ECDSA256
      256
    when SSHData::PublicKey::ALGO_ECDSA384
      384
    when SSHData::PublicKey::ALGO_ECDSA521
      521
    when SSHData::PublicKey::ALGO_ED25519
      256
    else 0
    end
  rescue SSHData::Error
    0
  end

  def parsed_key
    @parsed_key ||= SSHData::PublicKey.parse(key)
  end

  # Internal: validate that this key is structurally correct by using
  # `ssh-keygen` to decode the key and generate the key's fingerprint. If the
  # key is corrupt `ssh-keygen` will fail to generate a valid fingerprint.
  def key_validity
    return if !errors[:key].empty?
    set_fingerprint if fingerprint.nil?

    if fingerprint == "bogus" || fingerprint !~ /\A[0-9a-f:]+\Z/
      errors.add :key, "is invalid. Ensure you've copied the file correctly"
    end
  end

  def uniqueness_of_key
    return if !errors[:key].empty?

    cond = ["fingerprint = ?", fingerprint]
    unless new_record?
      cond.first << " and id <> ?"
      cond << id
    end

    if self.class.select("id, `key`").where(cond).first
      errors.add :key, "is already in use"
    end
  end

  MIN_KEY_BIT_LENGTH = 1023

  def key_length
    return if !errors[:key].empty?

    # this validation is only relevant for RSA and DSA
    if key_type == KEY_TYPE_RSA || key_type == KEY_TYPE_DSS
      # Some versions of PuTTY for Windows had a bug where they would generate keys
      # of length (2^n)-1. So we don't end up blocking these keys (since they're
      # essentially just as strong as a key of size (2^n)), we compare the key
      # bit length to MIN_KEY_BIT_LENGTH - 1.
      if key_bit_length < MIN_KEY_BIT_LENGTH
        errors.add :key, "is too short. #{PUBLIC_KEY_RECOMMENDATION}"
      end
    end
  end

  def user_or_repository_assigned
    if self.user.nil? && self.repository.nil?
      errors.add :base, "one of either user_id or repository_id must be assigned"
    elsif self.user_id && self.repository_id
      errors.add :base, "only one of user_id or repository_id can be assigned"
    elsif self.user && !self.user.user?
      errors.add :user, "invalid user"
    end
  end

  def consistency_between_created_by_and_oauth_authorization_id
    if oauth_authorization.nil? && created_by == "oauth_access"
      errors.add :oauth_authorization,
        "can't be blank when created_by is 'oauth_access'"
    elsif oauth_authorization.present? && created_by != "oauth_access"
      errors.add :created_by,
        "must be 'oauth_access' when oauth_access is present"
    end
  end

  # Validation checking that only repo keys are marked as read-only.
  #
  # Returns boolean.
  def only_read_only_if_repo_key
    if read_only? && !repository_key?
      errors.add :base, "only repository keys can be read-only"
    end
  end

  # Called after create to send the public key added email notification.
  def notify_creation
    if repository
      RepositoryMailer.deploy_key_added(self).deliver_later
    else
      AccountMailer.public_key_added(self).deliver_later if notify_user_key_action_via_email?
    end
  end
  time_method :notify_creation, key: "public_key.notify_creation.duration"

public

  # Public: Normalizes the provided key String, removing whitespace.
  def self.strip_whitespace(key)
    key.to_s.strip.gsub(/[\r\n]/, "")
  end

  # Public: Strips comments from the provided key String.
  def self.strip_comments(key)
    key.split(" ")[0, 2].join(" ")
  end

  # Public: Strip prefixes "SSH:" and "SSHKey:".
  def self.strip_prefix(key)
    key.sub(/\ASSH(Key)?:/, "")
  end

  # Public: Normalize key to the key data, removing whitespace, stripping
  # prefixes and comments.
  def self.normalize_key(key)
    strip_comments(strip_prefix(strip_whitespace(key)))
  end

  include Instrumentation::Model

  def event_payload
    event = {
      public_key_id: id,
      title: title,
      key: key,
      fingerprint: fingerprint,
      created_by: created_by,
      read_only: read_only?.to_s,
    }

    if GitHub.context[:actor_id]
      event[:actor_id] = GitHub.context[:actor_id]
    end

    if user_id?
      event[:user] = user
    elsif repository_id?
      event[:repo] = repository
    end

    event
  end

  # Public: Instrument creating new public keys.
  #
  # payload - event payload Hash.
  #           :actor - The User adding this public key.
  #                    Default: the user (most add their own).
  #
  # Returns nothing.
  def instrument_creation(payload = {})
    instrument :create, payload
  end

  # Public: Instrument public key updates.
  #
  # payload - event payload Hash.
  #           :actor - The User updating this public key.
  #                    Default: the user (most update their own).
  #
  # Returns nothing.
  def instrument_update(payload = {})
    instrument :update, payload
  end

  # Public: Instrument public key deletion.
  #
  # options - event payload Hash.
  #           :actor - The User deleting this public key.
  #           Default: the user (most remove their own).
  #
  # Returns nothing.
  def instrument_deletion(payload = {})
    instrument :delete, payload.merge(explanation: @destroy_explanation)
    GitHub.dogstats.increment("public_key", tags: ["action:destroy", "explanation:#{@destroy_explanation.to_s.gsub(/_/, "-")}"])
  end

  # Public: Destroy access and instrument the deletion using the provided
  # explanation
  #
  # explanation - The reason the record is being destroyed
  #
  # Returns the result of calling destroy() on the model instance.
  def destroy_with_explanation(explanation)
    unless self.class.destroy_explanation(explanation)
      raise ArgumentError, "invalid destroy explanation: #{explanation}"
    end
    @destroy_explanation = explanation
    destroy
  end

  # Public: Generate webhook payload before destroy.
  #
  #
  # Returns nothing.
  def generate_webhook_payload
    return unless repository_id?

    event = Hook::Event::DeployKeyEvent.new(
      action: :deleted,
      key_id: self.id,
      actor_id: (GitHub.context[:actor_id] || User.ghost.id),
      repository_id: self.repository_id,
      triggered_at: Time.now,
    )

    @delivery_system = Hook::DeliverySystem.new(event)
    @delivery_system.generate_hookshot_payloads
  end

  # Public: Enqueue the pre-generated payload into resque
  #
  #
  # Returns nothing.
  def queue_webhook_delivery
    return unless repository_id?

    unless defined?(@delivery_system)
      raise "'generate_webhook_payload' must be called before 'queue_webhook_delivery'"
    end

    @delivery_system&.deliver_later
  end

  # Window of time between access logs writes.
  #
  # Returns a duration.

  ACCESS_THROTTLING = 1.week
  ACCESS_CUTOFF_DATE = Time.utc(2014, 3, 5)
  RECENTNESS = 1.week

  def self.last_accessed_memcache_key(id)
    "public_keys:last_accessed:#{id}"
  end

  # Public: Has this key been accessed in the access throttling period.
  #
  # now - The time to use to determine the end of the throttling period.
  #
  # Returns true if accessed within period else false.
  def self.accessed_within_throttling_period?(accessed_at:)
    accessed_at && accessed_at > (Time.zone.now - ACCESS_THROTTLING)
  end

  # Public: Register this public key has been used
  #
  # This only updates periodically to prevent a lot of writes if used often in a
  # short period of time.
  #
  # Returns nothing
  def self.access(id:, last_accessed_at:)
    return if accessed_within_throttling_period?(accessed_at: last_accessed_at)

    now = Time.zone.now
    if GitHub.cache.add(last_accessed_memcache_key(id), now, ACCESS_THROTTLING.to_i)
      PublicKeyAccessJob.perform_later(id, now.to_i)
    end

    GitHub.instrument "public_key.access", public_key_id: id
  end

  def access
    self.class.access(id: id, last_accessed_at: accessed_at)
  end

  def access!(time)
    return if self.class.accessed_within_throttling_period?(accessed_at: accessed_at)

    ActiveRecord::Base.connected_to(role: :writing) do
      threshold = ACCESS_THROTTLING.ago.to_s(:db)
      github_sql.run(<<-SQL, id: id, accessed_at: time, threshold: threshold)
        UPDATE public_keys SET accessed_at = :accessed_at
        WHERE id = :id AND (accessed_at < :threshold OR accessed_at IS NULL)
      SQL
    end

    oauth_authorization.bump(time) if oauth_authorization

    nil
  end

  def last_access_date
    last_access_time.to_date
  end

  def last_access_time
    accessed_at&.in_time_zone
  end

  def recent?
    accessed_at && accessed_at > RECENTNESS.ago
  end

  def global_relay_id
    Platform::Helpers::NodeIdentification.to_global_id("PublicKey", id)
  end

  def weak_key?
    return false unless parsed_key.algo == SSHData::PublicKey::ALGO_RSA
    GitHub::SSH.weak_rsa_key?(parsed_key.openssl)
  rescue SSHData::Error
    return true
  end

  private

  # Verify that the key is not weak, where the definition of weak is dependent
  # on the type of key.
  def not_weak_key
    return if !errors[:key].empty?

    if weak_key?
      errors.add :key, "is weak. #{PUBLIC_KEY_RECOMMENDATION}"
    end
  end

  # Private: Deletes associated OauthAuthorization if the authorization has no
  # remaining authorized objects (public keys and accesses).
  #
  # Returns nothing.
  def destroy_oauth_authorization
    # If we are being destroyed due to an association :dependent => :destroy
    # then there is no need for us to destroy our parent OauthAuthorization.
    return if destroyed_by_association_parent.is_a?(OauthAuthorization)
    oauth_authorization.destroy if oauth_authorization.unused?
  end
end
