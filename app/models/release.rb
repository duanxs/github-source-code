# rubocop:disable Style/FrozenStringLiteralComment

# A flexible method for distributing a piece of software. A Release can be
# many things:
#
# - A mini blog post, describing new features, bug fixes, etc.
# - ... coupled with a Git Ref object representing the Tag.
# - ... optionally coupled with several downloadable files (.pkg, .jar, etc)
#
# - A way to highlight important Tags. Once they're promoted to a Release they
#   can be highlighted and broadcasted.
#
# - An alternative to PGP for identifying an author + tag as authentic.
#
class Release < ApplicationRecord::Domain::Repositories
  include GitHub::Relay::GlobalIdentification
  include GitHub::UserContent, GitHub::UTF8
  include Instrumentation::Model
  include NotificationsContent::WithoutCallbacks

  PER_PAGE = 10

  belongs_to :repository
  alias_method :entity, :repository
  alias_method :async_entity, :async_repository
  belongs_to :author, class_name: "User"
  has_one :repository_action_release, dependent: :destroy
  has_one :repository_action, through: :repository_action_release

  has_many :release_assets, dependent: :destroy
  has_many :package_versions, class_name: "Registry::PackageVersion"
  has_many :packages, through: :package_versions
  has_many :workflow_runs, as: :trigger

  accepts_nested_attributes_for :release_assets,
    reject_if: proc { |attributes| attributes[:id].blank? },
    allow_destroy: true

  accepts_nested_attributes_for :package_versions,
    reject_if: proc { |attributes| attributes[:id].blank? },
    allow_destroy: true

  accepts_nested_attributes_for :repository_action_release, update_only: true
  accepts_nested_attributes_for :repository_action, update_only: true

  validates_presence_of :tag_name
  validates_uniqueness_of :tag_name, scope: :repository_id, allow_nil: true, case_sensitive: true
  validate :tag_name_is_well_formed
  validate :author_access, on: :create
  validate :published_releases_are_tagged
  validate :target_is_valid
  validate :tag_is_well_formed

  before_save :backdate_if_tagged
  before_validation :create_tag_on_publish
  before_validation :fake_tag_if_draft

  before_save  :set_published_at
  after_commit :trigger_publish_events
  after_commit :instrument_creation, on: :create
  after_commit :instrument_update, on: :update
  after_commit :instrument_unpublish, on: :update
  after_commit :instrument_prerelease, on: [:create, :update]
  after_commit :instrument_release_create, on: :create
  after_commit :instrument_release_update, on: :update

  before_destroy :generate_webhook_payload
  after_commit :queue_webhook_delivery, on: :destroy

  after_destroy :destroy_notification_summary

  enum state: {
    published: 0,
    draft: 1,
  }

  alias draft draft?

  scope :drafted, -> { draft }
  scope :latest, -> { order("created_at DESC, updated_at DESC") }

  extend GitHub::Encoding
  force_utf8_encoding :name
  force_utf8_encoding :tag_name
  force_utf8_encoding :pending_tag

  attr_accessor :reflog_data
  attr_writer :actor

  def uploaded_assets
    @uploaded_assets ||= release_assets.uploaded
  end

  # Public: Find a Release, or wrap a tag Ref object in a Release
  # object if it exists.
  #
  # repository - The Repository in question.
  # slug - A tag name to find by.
  #
  # Returns a Release or nil.
  def self.from_tag_or_slug(repository, slug, include_drafts = false)
    release_scope = repository.releases
    if !include_drafts
      release_scope = release_scope.published
    end

    release = release_scope.order("created_at DESC").find_by_tag_name(slug)
    return release if release

    tag = repository.tags.find(slug)
    from_tag tag
  end

  def self.latest_for(repository)
    return unless repository
    repository.releases.published.where(prerelease: false).latest.first
  end

  # Public: Retrieve a page of releases.
  #
  # options - Hash of paging options:
  #   :after - Optional string tag name of the last tag on the previous page.
  #   :limit - Optional number of releases in a single page (default: 10).
  #
  # Returns an Array of Release objects for the given page in the order requested.
  def self.page(repository, options = {})
    options = { limit: PER_PAGE }.merge(options)
    tags = repository.sorted_tags.page(options)

    from_tags(repository, tags)
  end

  def self.pagination_for(repository, after, limit, num_releases)
    repository.sorted_tags.paginate(after, limit, num_releases)
  end

  # Internal: Convert a list of tags to Release objects for the given repository.
  #
  # repository - The Repository.
  # refs       - A Git::Ref::Collection of the Repository's tags.
  #
  # Returns an Array of Release objects in the order the refs are given.
  def self.from_tags(repository, refs)
    cache = {} # "tag name" => Release.new(....)

    prefill_from_releases(cache, repository, refs)
    prefill_from_tags(cache, repository, refs)

    # Preload signature verification for tags/commits.
    targets = refs.map(&:target).group_by(&:class)
    Tag.prefill_verified_signature(targets[Tag], repository) if targets[Tag]
    Commit.prefill_verified_signature(targets[Commit], repository) if targets[Commit]

    refs.map do |ref|
      release = cache[ref.name]
      release.set_tag(ref)
      release
    end
  end

  def self.prefill_from_releases(cache, repository, refs)
    filtered_refs = refs.map(&:name)
    releases = repository.releases.published.
      where(tag_name: filtered_refs)

    releases.each do |rel|
      cache[rel.tag_name.b] = rel
    end
  end

  def self.prefill_from_tags(cache, repository, refs)
    # get the refs that don't have releases already
    tags = refs.select { |ref| ref.exist? && !cache[ref.name] }
    return if tags.blank?

    # dereference the ref to its commit or tag object
    target_oids = tags.map(&:target_oid)
    targets = repository.objects.read(target_oids)

    # get the author emails for a bulk user query
    author_emails = targets.map { |t| t.try(:author_email).to_s.strip }
    author_emails.delete_if(&:blank?)
    users = User.find_by_emails(author_emails)

    # build the Release from the tag ref and target.
    tags.each_with_index do |tag, idx|
      target = targets[idx]
      email = target.try(:author_email).to_s.downcase
      cache[tag.name] = from_ref_and_target(tag, target, users[email])
    end
  end

  # Build a new, unsaved Release from a tag Ref object.
  #
  # ref - Ref object.
  #
  # Returns a Release.
  def self.from_tag(ref)
    return unless ref && ref.exist?

    target = ref.target
    from_ref_and_target(ref, target, target.try(:author))
  end

  def self.from_ref_and_target(ref, target, author)
    name, body = tag_title_and_body(ref, target: target)

    target_date = target.try(:date)
    Release.new(
      repository: ref.repository,
      name: prefix_with_tag_name(name, ref.name),
      body: body.to_s,
      author: author,
      tag_name: ref.name,
      created_at: target_date && target_date.localtime,
    )
  end

  def self.prefix_with_tag_name(title, tag_name)
    if !title.b[tag_name.b]
      "#{tag_name.b}: #{title.b}".dup
    else
      title
    end.force_encoding("utf-8").scrub!
  end

  def self.tag_title_and_body(ref, target: nil)
    return if ref.nil?
    target ||= ref.target
    name, body = if target.respond_to?(:message_body_text)
      if target.message_body_text?
        [target.short_message_text, target.message_body_text]
      else
        [ref.name, target.short_message_text]
      end
    else
      ref.name
    end
  end

  def created_at_for_display
    self.created_at || self.target_commit.created_at
  end

  # Public: Updates the model's attributes from a release params hash in a
  # Rails controller.
  #
  #     def update
  #       @release = Release.find params[:id]
  #       @release.with_params params[:release]
  #       @release.save!
  #     end
  #
  # params - A Hash of parameters.  Could be a StrongParams with nothing permitted
  #          since the keys are accessed by name.
  #
  # Returns nothing.
  def with_params(params)
    params.slice(:draft, :tag_name, :name, :body, :prerelease, :target_commitish).each do |key, value|
      send("#{key}=", value) unless value.nil?
    end

    if attribs = params[:release_assets_attributes]
      update_attribs = attribs.map do |release_asset|
        release_asset.slice("name", "id", "_destroy")
      end
      self.release_assets_attributes = update_attribs
    end

    if attribs = params[:package_versions_attributes]
      update_attribs = attribs.map do |registry_package_version|
        registry_package_version.slice("id", "_destroy")
      end
      self.package_versions_attributes = update_attribs
    end

    if attribs = params[:repository_action_release_attributes]
      if self.repository_action = repository.action_at_root
        attribs[:repository_action_id] = repository_action.id
      end

      self.repository_action_release_attributes = attribs.slice(:repository_action_id, :published_on_marketplace)

      if action_attribs = params[:repository_action_attributes]
        self.repository_action_attributes = action_attribs.slice(:security_email)
      end
    end
  end

  def async_readable_by?(actor)
    async_repository.then do |repo|
      if draft?
        repo&.async_pushable_by?(actor)
      else
        repo&.async_readable_by?(actor)
      end
    end
  end

  # Public: Whether the given user can see this release.
  def readable_by?(actor)
    async_readable_by?(actor).sync
  end

  # Public: Is this Release tagged as well?
  #
  # force - Force a check of the tag despite cache.
  #
  # Returns a boolean.
  def tagged?(force = false)
    !tag(force).nil?
  end

  # Public: The Git tag ref association.
  #
  # force - Force a check despite cache
  #
  # Returns a Ref object.
  def tag(force = false)
    return nil if tag_name.blank?
    return @tag if defined?(@tag) && @tag && !force

    set_tag repository.tags.find(tag_name.b)
  end

  # Public: Gets the tag's target if it exists.
  #
  # Returns a Commit/Tag, or nil.
  def tag_target
    tag.target if tag
  end

  def tag_name=(value)
    set_tag(nil)
    self.pending_tag = value if published?
    write_attribute(:tag_name, value)
  end

  def tag_name
    self[:tag_name].dup.force_encoding("UTF-8").scrub! if self[:tag_name]
  end

  # Internal: Set the tag Ref object for this release
  # This is used by Release.page as an optimization to prevent
  # potentially hitting rpc/cache yet again even though we already
  # have the tag Ref object loaded and ready to use.
  #
  # tag - The tag Ref object for this release
  #
  # Returns nothing.
  def set_tag(tag)
    @tag_title = @tag_body = nil
    @tag = tag
  end

  def tag_name_or_target_commitish
    tag_name || abbreviated_target_commitish
  end

  # Public: Gets the default title from the tag.
  def tag_title
    @tag_title ||= begin
      title, body = self.class.tag_title_and_body(tag)
      @tag_body = body.to_s
      title.to_s
    end
  end

  # Public: Gets the default body from the tag.
  def tag_body
    @tag_body ||= begin
      title, body = self.class.tag_title_and_body(tag)
      @tag_title = title.to_s
      body.to_s
    end
  end

  # Draft releases are not yet published to the world. They can be
  # viewed/edited by repo owners only.
  def draft=(value)
    value = (value.to_i > 0) if value.is_a?(String)
    self.state = value ? :draft : :published
  end

  # Public: Does this Release have release notes? This is a good way to know
  # that the Release is persisted in the DB.
  #
  # Returns true if Release if there are notes to view.
  def notes?
    !new_record? && !body.blank?
  end

  # Public: Should this release be displayed, or rendered as just a tag?
  def viewable?
    !new_record?
  end

  # Public: Absolute permalink URL for this release.
  #
  # include_host - Turn off the `GitHub.url` host in the url. (default true)
  #                release.permalink(include_host: false) => `/github/github/releases/tag/v0.0.1`
  #
  # Returns a String
  def permalink(include_host: true)
    return nil unless repository && tag_name
    "#{repository.permalink(include_host: include_host)}/releases/tag/#{UrlHelper.escape_branch(tag_name)}"
  end

  def target_commitish
    read_attribute(:target_commitish) || write_attribute(:target_commitish, repository.default_branch)
  end

  def abbreviated_target_commitish
    c = target_commitish.to_s
    GitRPC::Util.valid_full_sha1?(c) ? c[0, 7] : c
  end

  def target_commitish=(value)
    write_attribute(:target_commitish, value.present? ? value.to_s : nil)
  end

  def target_commit
    if @target_commit.nil?
      @target_commit = target_commit! || false
    end
    @target_commit || nil
  end

  def target_commit!
    if ref = repository.heads.find(target_commitish)
      ref.target
    else
      repository.commits.find(target_commitish)
    end
  rescue RepositoryObjectsCollection::InvalidObjectId, GitRPC::ObjectMissing
  end

  # Public: Gets the most recent commits from the target commit.
  #
  # limit - The Integer max number of commits to return.
  #
  # Returns an Array of Commit objects.
  def target_commits(limit = 10)
    return [] unless oid = repository && target_commit.try(:oid)
    repository.commits.history(oid, limit)
  end

  def comparison
    return unless tagged?
    return unless base_branch = target_commitish
    return unless base_oid = target_commit.try(:oid)
    return unless tag_oid = tag.target_oid

    ab = repository.rpc.ahead_behind(base_oid, tag_oid)
    return unless ab[tag_oid]

    ahead, behind = ab[tag_oid]
    return if ahead == 0 && behind == 0

    {
      "base_branch" => base_branch,
      "commit_range" => "#{tag_name}...#{base_branch}",
      "ahead" => ahead,
      "behind" => behind,
    }
  end

  # Public: The name to present people for this release. Tries name ->
  # annotated message -> tag name.
  #
  # Returns a String
  def display_name
    return name.dup.force_encoding("utf-8").scrub! unless name.blank?
    return "Draft" unless published?
    return if !tagged?

    self.class.prefix_with_tag_name(tag_title, tag_name)
  end

  def display_body
    return body unless body.blank?
    if name.blank? || name.b[tag_title.b]
      tag_body
    else
      "#{tag_title.b}\n\n#{tag_body.b}"
    end.dup.force_encoding("utf-8").scrub!
  end

  # The hand written description for the release.
  #
  # Returns a string body guaranteed to be valid utf8 encoded data.
  def body
    utf8(read_attribute(:body))
  end

  def body_before_type_cast
    utf8(super)
  end

  def repository_never_pushed_to?
    repository.try(:empty?)
  end

  def notifications_author
    author
  end

  def notifications_thread
    self
  end

  def async_notifications_list
    async_repository
  end

  # Public: Gets the RollupSummary for this Comment's thread.
  # See Summarizable.
  #
  # Returns Newsies::Response instance.
  def get_notification_summary
    list = Newsies::List.new("Repository", repository_id)
    GitHub.newsies.web.find_rollup_summary_by_thread(list, self)
  end

  def destroy_notification_summary
    # if we don't have the repository anymore, fake it.
    # this eventually needs to be fixed up to pass the ID directly.
    repo = repository || Repository.new.tap { |r| r.id = repository_id }
    GitHub.newsies.async_delete_all_for_thread(repo, self)
  end

  def deliver_notifications?
    !draft?
  end

  # Unique identifier for this issue used in email messages.
  def message_id
    "<#{repository.name_with_owner}/releases/#{id}@#{GitHub.urls.host_name}>"
  end

  alias user author

  # Public: Gets the author of a Release.  Falls back to the repository owner
  # for Tags commited by a user without matching GitHub account.
  def author_or_owner
    return author if author
    return nil unless repository
    owner = repository.owner
    if owner.organization?
      repository.created_by
    else
      owner
    end
  end

  # Generate a cache key used for storing rendered HTML bodies.
  def async_body_cache_key(key)
    super.then { |cache_key| "#{cache_key}:v1" }
  end

  # Internal: Backdate the Release to the Tag date if if it's been tagged.
  def backdate_if_tagged
    if tagged? && tag.target.respond_to?(:date) && tag.target.date
      self.created_at = tag.target.date.localtime
    end
  end

  # Internal: Store a pending tag name, or create a fake tag name if it's a
  # draft.
  #
  # In order to prevent collisions with real tags, we prefix fake tags with
  # untagged-. If you create a real tag starting with untagged, you deserve
  # bad things to happen to you.
  def fake_tag_if_draft
    return unless draft?

    if tag_name.present?
      self.pending_tag = self.tag_name
    end

    if !untagged_name?
      self.tag_name = "untagged-#{SecureRandom.hex(10)}"
    end
  end

  # Internal: Create a Git Tag if we're publishing this thing.
  def create_tag_on_publish
    return unless published?
    self.tag_name = pending_tag if pending_tag.present?
    return nil if repository_never_pushed_to? || tag_name.blank?
    begin
      errors.add(:tag_name, "is not a valid tag") unless ensure_published_tag
    rescue Git::Ref::HookFailed, Git::Ref::InvalidName => e
      errors.add(:pre_receive, e.message)
    end
  end

  def untagged_name?
    self.class.untagged_name?(tag_name)
  end

  def only_tag?
    new_record? && tagged?
  end

  def potential_github_action?
    repository.listable_action?
  end

  def deletable?
    return true unless new_record?
    tagged?
  end

  def delete_tag(commit_oid, deleter, reflog_data = {})
    return if !repository.tags.exist?(tag_name.b)
    ref = repository.tags.build(tag_name.b, commit_oid)
    ref.delete(deleter, reflog_data: reflog_data)
    true
  rescue Git::Ref::ComparisonMismatch, TypeError
    nil
  end

  def exposed_tag_name
    draft? ? pending_tag.to_s : tag_name
  end

  def self.untagged_name?(name)
    name && name.starts_with?(UNTAGGED_PREFIX)
  end

  UNTAGGED_PREFIX = "untagged-".freeze

  # The user who performed the action as set in the GitHub request context. If
  # the context doesn't contain an actor, fallback to the ghost user.
  def actor
    @actor ||= (User.find_by_id(GitHub.context[:actor_id]) || User.ghost)
  end

private
  # Validation: Ensure that the author has access to create this Release.
  def author_access
    if !author
      errors.add :author_id, "is not a valid User"
    elsif !repository
      errors.add :repository_id, "is not a valid repository"
    elsif !repository.resources.contents.writable_by?(user)
      errors.add :author_id, "does not have push access to #{repository.name_with_owner}"
    end
  end

  # Validation: Make sure we don't let people enter bad target_commitish values
  def target_is_valid
    return if published? && !target_commitish_changed?
    return if target_commit || (draft? && target_commitish == repository.try(:default_branch))

    errors.add :target_commitish, "is invalid"
  end

  # Validation: Make sure we don't let people create Releases with names that
  # Git doesn't like.
  def tag_name_is_well_formed
    return if published? && !tag_name_changed?
    return if repository.tags.build(tag_name).well_formed?

    errors.add :tag_name, "is not well-formed"
  end

  # Validation: Ensure that all published Releases correspond to an existing
  # tag.
  def published_releases_are_tagged
    return unless published?
    if repository_never_pushed_to?
      errors.add(:base, "Repository is empty.")
    elsif !tagged?
      errors.add :base, "Published releases must have a valid tag"
    end
  end

  # Validation: Ensures the tag name is a valid format
  def tag_is_well_formed
    if published? && (tag && !tag.well_formed?)
      errors.add :base, "Tag is not well formed"
    end
  end

  def ensure_published_tag
    return true if tagged?(force = true)
    tag = repository.tags.build(tag_name)
    return unless tag.well_formed? && target_commit
    tag.create(target_commit, author, reflog_data: reflog_data)
  rescue Git::Ref::ExistsError
    true
  end

  # Sets the published_at timestamp if this Release is published.  Trigger the
  # publish events if the release is published, but published_at is empty.
  def set_published_at
    @trigger_publish_event = false
    if published?
      @trigger_publish_event = published_at.nil?
      self.published_at ||= Time.now
    else
      self.published_at = nil
    end
  end

  # Triggers publish related events if the release is published.
  #
  # Returns nothing.
  def trigger_publish_events
    return unless repository && @trigger_publish_event

    deliver_notifications(event_time: published_at)
    instrument :published
  end

  def body_pipeline
    GitHub::Goomba::ReleasePipeline
  end

  def async_body_context
    super.then do |context|
      context.merge(path: "", committish: tag_name_or_target_commitish)
    end
  end

  def body_version
    super + ":#{tag_name_or_target_commitish.to_s.to_md5}"
  end

  def instrument_creation
    instrument :create, actor: author
  end

  def generate_webhook_payload
    if actor&.spammy?
      @delivery_system = nil
      return
    end

    event = Hook::Event::ReleaseEvent.new(
      action: :deleted,
      release_id: self.id,
      actor_id: actor.id,
      triggered_at: Time.now,
    )

    @delivery_system = Hook::DeliverySystem.new(event)
    @delivery_system.generate_hookshot_payloads
  end

  def queue_webhook_delivery
    unless defined?(@delivery_system)
      raise "`generate_webhook_payload` must be called before `queue_webhook_delivery`"
    end

    @delivery_system&.deliver_later
  end

  def instrument_update
    encode_body_changes!
    return if previous_changes.empty?
    return if being_prereleased?

    changes = {}.tap do |hash|
      hash[:old_name] = previous_changes["name"].first if previous_changes["name"].present?
      hash[:old_body] = previous_changes["body"].first if previous_changes["body"].present?
    end

    return if changes.empty?
    instrument :update, changes: changes
  end

  def instrument_unpublish
    return unless being_unpublished?
    instrument :unpublish
  end

  def being_unpublished?
    return false unless previous_changes && previous_changes["state"]
    previous_changes["state"].first == "published" && state == "draft"
  end

  def being_published?
    return false if state != "published"
    previous_changes.key?("state")
  end

  def instrument_prerelease
    return unless being_prereleased?
    instrument :prerelease
  end

  def being_released?
    return false if state == "draft"
    previous_changes.key?("prerelease")
  end

  def being_prereleased?
    return false unless previous_changes && previous_changes["prerelease"]
    previous_changes["prerelease"].first != true && self.prerelease == true
  end

  def instrument_release_update
    return if prerelease == true
    return unless being_published? || being_released?
    instrument :release
  end

  def instrument_release_create
    # This is separate from `instrument_release_update` because when a release is
    # created in a `finalized` state, the state field defaults to `published`
    # if unspecified and as a result is not in the `previous_changes` hash. As such
    # it's more convenient to have separate callbacks for the `release` event for
    # create and update
    return unless state == "published" && prerelease == false
    instrument :release
  end

  def event_payload
    {
      event_prefix => self,
      :author      => author,
      :repository  => repository,
      :actor       => actor,
      :spammy      => actor != User.ghost && actor.spammy?,
    }
  end

  #
  # This method has a side-effect based on active records changes hash.
  # When we call force_encoding and the two values(pre, next) are the same, will remove the body values under
  # the previous_changes hash
  #
  def encode_body_changes!
    Array(previous_changes["body"]).each { |attr| !attr.frozen? && attr.force_encoding("utf-8") }
  end
end
