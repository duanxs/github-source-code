# frozen_string_literal: true
module TeamSync
  class ServiceResponseError < StandardError; end
end
