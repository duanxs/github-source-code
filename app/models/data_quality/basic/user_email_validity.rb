# rubocop:disable Style/FrozenStringLiteralComment

# Usage:
#
#   script = DataQuality::Basic::UserEmailValidity.new
#   script.run
#
# To mark users with invalid email addresses as not needing to verify,
# run this script with clean: true and provide an actor.
#
#   script = DataQuality::Basic::UserEmailValidity.new(clean: true, actor: some_site_admin)
#   script.run

module DataQuality::Basic

  class InvalidUserEmailTracker
    include ActionView::Helpers::NumberHelper

    attr_reader   :max
    attr_accessor :reasons

    def initialize
      @max     = BigDecimal(UserEmail.last.id)
      @reasons = Hash.new { |hsh, key| hsh[key] = {count: 0} }
    end

    # Public: Track an invalid UserEmail
    #
    # user_email: A UserEmail instance where valid? has been called
    def track(user_email)
      track_reasons(user_email.errors.full_messages)
    end

    # Public: Log an invalid UserEmail and % complete
    #
    # user_email: A UserEmail instance where valid? has been called
    def log(user_email)
      percent_complete = user_email.id / max
      message_parts = ["Found invalid email: [#{user_email.id}]",
                       user_email.email.rjust(40),
                       number_to_percentage(percent_complete * 100)]
      message_parts << "*SPAMMY*" if user_email.user.spammy?
      puts "\t* #{message_parts.join("\t")}"
    end

    private

    def track_reasons(error_messages)
      error_messages.each do |message|
        reasons[message][:count] += 1
      end
    end
  end

  class InvalidUserEmailCleaner
    attr_reader :actor

    def initialize(actor:)
      @actor = actor
    end

    def clean(user_email, verbose: true)
      return unless user = user_email.user
      return unless should_clean?(user: user, duplicate: user_email.duplicate?)
      user.disable_mandatory_email_verification(actor: actor)
      log(user.login) if verbose
    end

    private

    def log(user)
      puts "\t* Disabled mandatory email verification for #{user}."
    end

    def should_clean?(user:, duplicate:)
      return false unless user.should_verify_email?
      return false if user.spammy?
      return false unless duplicate
      true
    end
  end

  class UserEmailValidity < ::DataQuality::Script
    attr_accessor :count
    attr_reader :tracker, :cleaner

    BATCH_SIZE = 50

    def initialize(clean: false, verbose: true, actor: nil)
      super(clean: clean, verbose: verbose)
      @count = 0
      @tracker = InvalidUserEmailTracker.new
      @cleaner = InvalidUserEmailCleaner.new(actor: actor) if clean?
    end

    def run
      old_log_level = ActiveRecord::Base.logger.level
      ActiveRecord::Base.logger.level = 1
      log "* Scanning for invalid UserEmails" if verbose?
      begin
        UserEmail.find_each(batch_size: BATCH_SIZE) do |record|
          record.throttle do
            unless record.valid?
              tracker.track(record)
              tracker.log(record) if verbose?
              cleaner.clean(record, verbose: verbose?) if clean?
              self.count += 1
            end
          end
        end
        tracker
      ensure
        log "* Found #{count} invalid UserEmails." if verbose?
        ActiveRecord::Base.logger.level = old_log_level
      end
    end
  end
end
