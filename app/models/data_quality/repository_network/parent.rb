# rubocop:disable Style/FrozenStringLiteralComment

module DataQuality::RepositoryNetwork
  # Finds all Repository records whose parent_id references a deleted or
  # nonexistent repository or a repository in a different network
  # (source_id is different) and sets their parent_id to nil, removing any
  # linkage. Finds any non-root repositories whose parent_id is nil and
  # sets their parent_id to the root of the network.
  class Parent < ::DataQuality::Script
    def run
      log "* Scanning for repositories with deleted or nonexistent parent references"
      correct_repositories_with_bad_parent_references("deleted or nonexistent", find_repositories_with_missing_parent_references)
      log "* Scanning for repositories with parent references pointing to a repo in a different network"
      correct_repositories_with_bad_parent_references("cross-network", find_repositories_with_parents_in_other_network)
      correct_non_root_repositories_with_null_parents
    end

    def correct_repositories_with_bad_parent_references(description, repos)
      log "  * Found #{repos.size} repositories with #{description} parent_id references"
      repos.each do |repo|
        debug "    * Repository #{repo.id} (#{repo.name_with_owner}) parent_id is #{repo.parent_id}, should be nil"
      end

      return unless clean? && repos.size > 0

      count = 0
      Repository.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
        repos.each do |repo|
          repo.update_attribute :parent_id, nil
          debug "    * Setting #{repo.id}'s parent_id to nil"
          count += 1
        end
      end

      log "  * Updated #{count} repository parents to nil"
    end

    def correct_non_root_repositories_with_null_parents
      log "* Scanning for non-root repositories with a null parent reference"
      repos_and_roots = find_non_root_repositories_with_null_parents
      log "  * Found #{repos_and_roots.size} non-root repositories with null parent_ids"
      repo_root_hashes = repos_and_roots.map do |repo_root|
        return {} unless repo_root.size == 2
        repo = Repository.find_by_id(repo_root.first)
        root = Repository.find_by_id(repo_root.last)
        { repo: repo, root: root }
      end

      repo_root_hashes.each do |hash|
        debug "    * Repository #{hash[:repo].id} (#{hash[:repo].name_with_owner}) parent nil -> #{hash[:root].id} (the network root)"
      end

      return unless clean? && repo_root_hashes.length > 0

      count = 0
      Repository.throttle_with_retry(max_retry_count: MAX_THROTTLE_RETRIES) do
        repo_root_hashes.each do |hash|
          next unless repo = hash[:repo]
          next unless root = hash[:root]
          debug "    * Setting #{repo.id}'s parent to #{root.id}"
          repo.update_attribute :parent_id, root.id
          count += 1
        end
      end

      log "  * Updated #{count} repository parents"
    end

    def find_repositories_with_missing_parent_references
      readonly do
        Repository.github_sql.new(<<-SQL).models(Repository)
          SELECT *
            FROM repositories r
           WHERE r.parent_id IS NOT NULL
             AND r.active = 1
             AND NOT EXISTS (
                   SELECT *
                     FROM repositories r2
                    WHERE r2.id = r.parent_id
                      AND r2.active = 1
                 )
        SQL
      end
    end

    def find_repositories_with_parents_in_other_network
      readonly do
        Repository.github_sql.new(<<-SQL).models(Repository)
          SELECT r.*
            FROM repositories r
            JOIN repositories p ON p.id = r.parent_id
           WHERE p.source_id != r.source_id
        SQL
      end
    end

    def find_non_root_repositories_with_null_parents
      readonly do
        Repository.github_sql.results <<-SQL
          SELECT repo.id as repo_id, root.id as root_id
            FROM repositories repo
            JOIN repository_networks network
              ON repo.source_id = network.id
            JOIN repositories root
              ON network.root_id = root.id
              AND root.parent_id IS NULL
              AND repo.id <> root.id
          WHERE repo.active = 1
            AND root.active = 1
            AND repo.parent_id IS NULL
        SQL
      end
    end
  end
end
