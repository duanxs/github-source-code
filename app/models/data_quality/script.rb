# rubocop:disable Style/FrozenStringLiteralComment

# This is a base class for a variety of data quality scripts.
#
# They're effectively long-lived transitions that need to be run periodically
# rather than just once, and are available more conveniently as part of the
# `data-quality-check` script.
#
# These data quality scripts have a few things in common:
#
# * They're initialized with a boolean flag as to whether or not they should
#   merely *scan* for invalid data, or actually clean it
# * They have a `run` method which performs the work.
#
# Descends from GitHub::Transition, which provides built-in throttling and
# other helper methods. The same guidelines for writing transitions apply to
# these data quality checks.
class DataQuality::Script < ::GitHub::Transition

  # Public: Initialize a data quality script
  #
  # :clean   - set to true if it should remove orphaned ability records
  # :verbose - set to true to enable verbose logging to STDOUT.
  #
  def initialize(clean: false, verbose: false)
    @clean   = clean
    @verbose = verbose

    log "* Initializing #{self.class.name}"
  end

  # Internal: Should we clean, or just report results?
  def clean?
    @clean
  end

  # Internal: Verbose logging to STDOUT?
  def verbose?
    @verbose
  end

  # Log a message to STDOUT and the data-quality-check logfile.
  def log(message)
    return if Rails.env.test?
    puts "[#{Time.now.iso8601.sub(/-\d+:\d+$/, '')}] #{message}"
    logger.info message
  end

  # Log a debug message output to STDOUT and the data-quality-check logfile.
  # Logs to the log file always, and only to STDOUT if verbose is enabled.
  def debug(message)
    return if Rails.env.test?
    puts "[#{Time.now.iso8601.sub(/-\d+:\d+$/, '')}] #{message}" if verbose?
    logger.debug message
  end

  def logger
    if !defined? @logger
      @logger = Logger.new(log_path)
      @logger.formatter = lambda do |severity, datetime, progname, message|
        "[#{datetime.iso8601.sub(/-\d+:\d+$/, '')}] #{severity} #{message}\n"
      end
    end
    @logger
  end

  # Internal: log path for the data quality scripts
  def log_path
    "#{Rails.root}/log/data-quality-check.log"
  end

end
