# frozen_string_literal: true

class CheckStep < ApplicationRecord::Ballast
  extend GitHub::Encoding
  force_utf8_encoding :name

  areas_of_responsibility :checks

  belongs_to :check_run, inverse_of: :steps, touch: true

  enum status: CheckRun.statuses
  enum conclusion: CheckRun.conclusions

  after_commit :notify_socket_subscribers, on: [:update]

  def self.created_after(time)
    where("created_at > ?", time)
  end

  def seconds_to_completion
    if completed_at && started_at && completed_at >= started_at
      (completed_at - started_at).to_i
    end
  end

  def channel
    GitHub::WebSocket::Channels.check_step(self)
  end

  def async_readable_by?(actor)
    async_check_run.then do |check_run|
      check_run.async_repository.then do |repository|
        repository.async_readable_by?(actor)
      end
    end
  end

  def to_channel_info
    slice("number", "conclusion", "authenticated_completed_log_url", "started_at", "completed_at")
  end

  def authenticated_completed_log_url
    return nil unless completed_log_url

    Rails.application.routes.url_helpers.check_step_logs_path(repository: check_run.repository,
                                                              user_id: check_run.repository.owner.login,
                                                              ref: check_run.head_sha,
                                                              id: check_run.id,
                                                              step: number)
  end

  def skipped?
    conclusion == "skipped"
  end

  def completed?
    conclusion.present?
  end

  def completed_without_logs?
    completed? && completed_log_url.nil?
  end

  private

  def notify_socket_subscribers
    data = {
      timestamp: updated_at,
      reason: "check_step ##{id} updated",
      wait: default_live_updates_wait,
      stepData: to_channel_info,
    }

    GitHub::WebSocket.notify_repository_channel(check_run.repository, channel, data)
  end
end
