# frozen_string_literal: true

class RepositoryAdvisory < ApplicationRecord::Collab
  areas_of_responsibility :security_advisories
  class MarkdownBody
    include GitHub::UserContent

    attr_reader :body

    def initialize(body)
      @body = body || ""
      @sha = Digest::SHA1.hexdigest(@body)
    end

    def new_record?
      true
    end
  end

  include GitHub::RateLimitedCreation
  include GitHub::Relay::GlobalIdentification
  include NotificationsContent::WithCallbacks
  include Reaction::Subject::RepositoryContext
  include UserContentEditable
  include HasCveUrl
  include Instrumentation::Model

  include Permissions::Attributes::Wrapper
  self.permissions_wrapper_class = Permissions::Attributes::RepositoryAdvisory

  CVE_ID_PATTERN = /\ACVE-\d{4}-\d{4,31}\z/
  MESSAGE_ID_TEMPLATE = "<%{repo}/repository-advisories/%{id}@%{host}>"
  URI_TEMPLATE = Addressable::Template.new("/{owner}/{name}/security/advisories/{ghsa_id}")
  SOURCE_IDENTITY = "github/repository_advisories"
  ATTRIBUTE_NOT_PROVIDED = "Not provided."

  enum severity: %i{
    low
    moderate
    high
    critical
  }

  # All user editable text fields must support UTF-8 and need encoding forced
  extend GitHub::Encoding
  force_utf8_encoding :title,
                      :description,
                      :body,
                      :affected_versions,
                      :patches,
                      :cve_id,
                      :package,
                      :ecosystem

  belongs_to :repository
  belongs_to :author, class_name: "User", foreign_key: :author_id
  belongs_to :publisher, class_name: "User", foreign_key: :publisher_id, optional: true
  belongs_to :vulnerability, primary_key: :ghsa_id, foreign_key: :ghsa_id, optional: true
  belongs_to :workspace_repository,
             class_name: "Repository",
             inverse_of: :parent_advisory

  has_many :comments, -> { order(id: :asc) }, class_name: "RepositoryAdvisoryComment"
  has_many :events, -> { order(id: :asc) }, class_name: "RepositoryAdvisoryEvent"
  has_many :credits, -> { order(id: :asc) }, class_name: "AdvisoryCredit",
    primary_key: :ghsa_id, foreign_key: :ghsa_id, dependent: :destroy

  has_many :reactions, as: :subject

  accepts_nested_attributes_for :credits, allow_destroy: true

  alias_attribute :user_id, :author_id
  alias_method :user, :author
  alias_method :async_user, :async_author

  scope :published, -> { where(state: "published") }
  scope :unpublished, -> { where.not(state: "published") }
  scope :private_to_org, -> (organization) { unpublished.where(workspace_repository_id: organization.org_repositories.pluck(:id)) }
  scope :newest_first, -> { order(id: :desc) }

  # VALIDATIONS

  validates :repository, presence: true
  validates :author, presence: true
  validates :title, presence: true, length: { maximum: 1024 }
  validates :description, presence: true, if: :published?
  validates :affected_versions, presence: true, if: :published?
  validates :affected_versions, length: { maximum: 1024 }
  validates :patches, length: { maximum: 1024 }
  validates :package, length: { maximum: 100 }
  validates :ecosystem, length: { maximum: 50 }
  validates :severity, presence: true, if: :published?
  validates :cve_id, format: { with: CVE_ID_PATTERN, allow_blank: true }

  # CALLBACKS

  before_validation :set_ghsa_id!, on: :create, unless: :ghsa_id?
  before_validation :set_owner_id!, on: :create
  after_commit :subscribe_repository_admins_and_notify, on: :create
  after_commit :instrument_open_event, on: :create
  after_commit :instrument_update_event, on: :update, if: :instrument_update_event?
  after_commit :update_subscriptions_and_notify, on: :update
  after_commit :instrument_reopen_event, if: [:open?, :state_previously_changed?]
  after_commit :instrument_close_event, if: [:closed?, :state_previously_changed?]
  after_destroy :destroy_notification_summary

  attr_readonly :ghsa_id, :owner_id

  # HANDLING USER CONTENT {

  include GitHub::UserContent
  # These two methods are injected into the async_body_context which informs
  # how the description is rendered and which mentions are authorized.
  #
  # See: GitHub::UserContent
  # See also: #async_organization
  alias_method :async_body_context_user, :async_author
  alias_method :async_entity, :async_repository
  # } HANDLING USER CONTENT


  # ABILITY INTERFACE {
  include Ability::Subject

  def adminable_by?(actor)
    async_adminable_by?(actor).sync
  end

  # An Advisory permits admin abilities by an actor who
  # can admin its parent Repository
  def async_adminable_by?(actor)
    async_repository.then do |repository|
      next false unless repository

      repository.async_adminable_by?(actor)
    end
  end

  def writable_by?(actor)
    async_writable_by?(actor).sync
  end

  # An actor may have a direct write ability -or-
  # be an admin of the parent repository
  def async_writable_by?(actor)
    async_permit?(actor, :write).then do |permitted|
      next true if permitted

      async_adminable_by?(actor)
    end
  end

  def async_viewer_can_update?(viewer)
    async_viewer_cannot_update_reasons(viewer).then(&:empty?)
  end

  def async_viewer_cannot_update_reasons(viewer)
    return Promise.resolve([:login_required]) unless viewer

    async_writable_by?(viewer).then do |writable|
      writable ? [] : [:insufficient_access]
    end
  end

  def async_readable_by?(actor)
    if published?
      async_repository.then do |repository|
        next false unless repository

        repository.async_readable_by?(actor)
      end
    else
      async_permit?(actor, :read).then do |readable|
        next true if readable

        async_writable_by?(actor)
      end
    end
  end

  # An actor may have a direct read ability -or-
  # be an admin of the parent repository
  def readable_by?(actor)
    async_readable_by?(actor).sync
  end

  # Grants a collaborator write permissions on an Advisory
  # if they can :read the originating Repository.
  #
  # It will also grant on the workspace if it exists.
  def add_collaborator(actor)
    return false unless repository.readable_by?(actor)

    grant actor, :write
    grant_on_workspace actor, :write
    subscribe_collaborator(actor)
  end

  # Revokes a collaborator write permissions on an Advisory and
  # its Workspace if it exists
  def remove_collaborator(actor)
    revoke actor
    revoke_on_workspace(actor)
    unsubscribe_collaborator(actor)
  end

  def collaborators(actor_type:)
    actor_id_list = Authorization.service.direct_abilities_on_subject(subject: self, actor_type: actor_type).pluck(:actor_id)
    case actor_type
    when "User"
      User.where(id: actor_id_list)
    else
      Team.where(id: actor_id_list)
    end
  end

  def collaborating_users
    collaborators(actor_type: "User")
  end

  def collaborating_teams
    collaborators(actor_type: "Team")
  end
  # } ABILITY INTERFACE

  delegate :nwo, :owner, to: :repository

  def global_relay_id
    Platform::Helpers::NodeIdentification.to_global_id("RepositoryAdvisory", ghsa_id)
  end

  def async_path_uri
    return @async_path_uri if defined?(@async_path_uri)

    @async_path_uri = async_repository.then(&:async_owner).then do
      URI_TEMPLATE.expand(
        owner:   repository.owner.login,
        name:    repository.name,
        ghsa_id: ghsa_id,
      )
    end
  end

  def to_param
    ghsa_id
  end

  # ADVISORY STATE HANDLING {
  enum state: %i{
    open
    closed
    published
  }

  def set_published(actor: author)
    return false unless open?

    # set attribute directly,
    # in order for validations to trigger
    self.state = "published"

    if update(publisher: actor,
        published_at: Time.current)
      instrument_publish_event
      deliver_credit_notifications
      true
    else
      self.state = "open"
      false
    end
  end

  def set_closed
    return false unless open?

    update(state: "closed",
           closed_at: Time.current)
  end

  def set_open
    return false unless closed?

    update(state: "open",
           closed_at: nil)
  end

  def submit_for_review
    return unless published?

    PendingVulnerability.pending.create!(as_vulnerability_params)
  end

  def github_review
    @github_review ||= GitHubReview.for(self)
  end

  def form_filled_out?
    affected_versions.present? && description.present?
  end

  def publishable?
    open? &&
      form_filled_out? &&
      workspace_clean?
  end

  # } ADVISORY STATE HANDLING

  def async_permalink(include_host: true)
    async_repository.then do |repo|
      if repo
        "#{repo.permalink(include_host: include_host)}/security/advisories/#{ghsa_id}"
      else
        nil
      end
    end
  end

  def permalink(include_host: true)
    async_permalink(include_host: include_host).sync
  end

  DESCRIPTION_TEMPLATE =
    <<~DEFAULT
    ### Impact
    _What kind of vulnerability is it? Who is impacted?_

    ### Patches
    _Has the problem been patched? What versions should users upgrade to?_

    ### Workarounds
    _Is there a way for users to fix or remediate the vulnerability without upgrading?_

    ### References
    _Are there any links users can visit to find out more?_

    ### For more information
    If you have any questions or comments about this advisory:
    * Open an issue in [example link to repo](http://example.com)
    * Email us at [example email address](mailto:example@example.com)
    DEFAULT


  def description_or_template
    if description.blank?
      DESCRIPTION_TEMPLATE
    else
      description
    end
  end

  # event handling {
  def add_rename_event(actor, value_was, value_is)
    events.create(event: "renamed",
                  actor: actor,
                  changed_attribute: "title",
                  value_was: value_was,
                  value_is: value_is)

  end

  def add_state_event(actor, event)
    events.create(event: event,
                  actor: actor,
                  changed_attribute: "state")
  end

  def add_close_event(actor)
    add_state_event(actor, "closed")
  end

  def add_reopen_event(actor)
    add_state_event(actor, "reopened")
  end

  def add_publish_event(actor)
    add_state_event(actor, "published")
  end

  # Creates an event on the pseudo-attribute `review_state` that records
  # a decision by the security review team to publish, reject or withdraw
  # this advisory in our public security data.
  def add_review_state_event(actor, event)
    events.create!(event: event,
                   actor: actor,
                   changed_attribute: "review_state")
  end

  def add_cve_requested_event(actor)
    events.create!(
      event: "cve_requested",
      actor: actor,
      changed_attribute: "cve_id",
    )
  end

  def add_cve_assigned_event(comment: nil)
    transaction do
      events.create!(
        event: "cve_assigned",
        actor: User.staff_user,
        changed_attribute: "cve_id",
        value_is: cve_id,
      )

      if comment.present?
        comments.create!(
          user: User.staff_user,
          body: comment,
        )
      end
    end
  end

  def add_cve_not_assigned_event(comment: nil)
    transaction do
      events.create!(
        event: "cve_not_assigned",
        actor: User.staff_user,
        changed_attribute: "cve_id",
      )

      if comment.present?
        comments.create!(
          user: User.staff_user,
          body: comment,
        )
      end
    end
  end

  # } event handling

  # WORKSPACE HELPERS {
  def recently_touched_branches(recent_threshold = 24.hours)
    return [] unless workspace_repository && !workspace_repository.creating?
    return @branches if defined?(@branches)

    pushes = Push.latest(workspace_repository, recent_threshold.ago).to_a
    pushes.uniq!(&:ref)
    pushes.select! { |push| push.ref_is_branch? && push.after != GitHub::NULL_OID }

    branches = pushes.map { |p| {name: p.branch_name, pushed_at: p.created_at} }
    possible_ref_names = Git::Ref.permutations(branches.map { |b| b[:name] })
    with_existing_pulls = pull_requests.where(head_ref: possible_ref_names).
      collect(&:display_head_ref_name)

    @branches = branches.reject { |b| with_existing_pulls.include?(b[:name]) }
  end

  def pull_requests
    return [] unless workspace_repository

    @pull_requests ||= workspace_repository.pull_requests.joins(:issue).where(<<~SQL)
        issues.state = 'open'
        OR pull_requests.merged_at IS NOT NULL
      SQL
  end

  def open_pull_requests
    return [] unless workspace_repository

    @open_pulls ||= workspace_repository.pull_requests.open_pulls
  end

  def build_batch_merge(actor:)
    PullRequest::BatchMerge.new(
      repository,
      open_pull_requests,
      actor,
      message_title: pull_merge_message,
      method: repository.default_merge_method_for(actor),
    )
  end

  def enqueue_mergeable_updates
    open_pull_requests.each(&:enqueue_mergeable_update)
  end

  def cleanup_workspace(actor)
    workspace_repository.remove(actor, instrument: false) if workspace_repository
  end

  def workspace_clean?
    open_pull_requests.empty?
  end

  def pull_merge_message
    "Merge pull request from #{ghsa_id}"
  end
  # } WORKSPACE HELPERS

  # NOTIFICATION INTERFACE {
  # Used by Newsies to generate the Message-ID email header, which gives email
  # clients hints for how to thread related emails.
  def message_id
    MESSAGE_ID_TEMPLATE % {
      repo: nwo,
      id: ghsa_id,
      host: GitHub.urls.host_name,
    }
  end

  # Used by Newsies to identify the RollupSummary that describes the thread
  # containing email notifications for this repository advisory.
  #
  # Required for: NotificationsContent
  def get_notification_summary
    list = Newsies::List.new("Repository", repository_id)
    GitHub.newsies.web.find_rollup_summary_by_thread(list, self)
  end

  # as defined in Issue, RepositoryVulnerabilityAlert
  def destroy_notification_summary
    repo = repository || Repository.new.tap { |r| r.id = repository_id }
    GitHub.newsies.async_delete_all_for_thread(repo, self)
  end


  # Used by Newsies to update the rollup summary when specific attributes change
  # on the repository advisory. Implementation is taken directly from the
  # Summarizable module, modified only to include this model's particular
  # attributes of interest.
  #
  # Overridden for: NotificationsContent
  def update_notification_rollup(summary)
    suffix = summarizable_changed?(:title, :body) ? :changed : :unchanged
    GitHub.dogstats.increment("newsies.rollup", tags: ["type:#{suffix}"])

    summary.summarize_repository_advisory(self)
  end

  # Used by Newsies to allow users to unsubscribe from notifications for a
  # particular repository advisory.
  #
  # Required for: NotificationsContent
  def notifications_thread
    self
  end

  # Used by Newsies to set the From address for outgoing email notifications and
  # to prevent delivery to the author. This is how Issue notifications behave.
  #
  # Required for: NotificationsContent
  def notifications_author
    author
  end

  # Used by Newsies as a secondary grouping mechanism for notifications.
  # Currently, a notifications list may either point to a repository or a team.
  #
  # Required for: NotificationsContent
  def async_notifications_list
    async_repository
  end

  # Rather than using NotificationsContent#subscribe_and_notify as an
  # after_commit on: :create callback, we call this method instead because we
  # want to explicitly subscribe the original repository's admins rather than
  # any users or teams that are mentioned in the body of the repository
  # advisory.
  def subscribe_repository_admins_and_notify
    SubscribeAndNotifyJob.perform_later(self, {
      subscriber_reasons_and_ids: {
        manual: admin_user_ids,
      },
      mentioned_user_ids: mentioned_admin_user_ids,
      mentioned_team_ids: mentioned_admin_team_ids,
      deliver_notifications: ::GitHub.send_notifications?,
    })
  end

  def subscribe_collaborator(actor)
    user_ids = nil

    case actor
    when Team
      user_ids = subscribable_team_members(actor).pluck(:id)
    when User
      user_ids = [actor.id]
    else
      return
    end

    SubscribeAndNotifyJob.perform_later(self, {
      subscriber_reasons_and_ids: {
        manual: user_ids,
      },
      deliver_notifications: ::GitHub.send_notifications?,
    })
  end

  def unsubscribe_collaborator(actor)
    # TODO: maybe move to bg job?
    case actor
    when Team
      subscribable_team_members(actor).find_each(batch_size: GitHub.subscribed_users_batch_size) do |user|
        unsubscribe(user)
      end
    when User
      unsubscribe(actor)
    end
  end


  # This is automatically added to the context by which we parse the repository
  # advisory's description to detect mentioned users and teams. Along with
  # async_entity and async_body_context_user, the async_organization is merged
  # into the context and informs which mentioned users and teams are allowed to
  # be recognized and linked.
  #
  # See: GitHub::UserContent
  def async_organization
    async_repository.then(&:async_organization)
  end

  # Overrides: NotificationsContent#unsubscribable_users
  def unsubscribable_users(users)
    user_ids_to_preserve = admin_user_ids

    users.reject { |user| user_ids_to_preserve.include?(user.id) }
  end

  # Although Newsies will ultimately prevent notification delivery to users who
  # don't have read access to the repository advisory, here we're preventing
  # subscription in the first place by checking for read access proactively.
  #
  # Extends: SubscribableThread#subscribable_by?
  def subscribable_by?(user, *args)
    readable_by?(user) && super
  end

  def markdown_description
    MarkdownBody.new(description)
  end

  def mentionable_users_for(actor)
    admins = User.where(id: admin_user_ids)
    filter = Suggester::BlockedUserFilter.new(viewer: actor)
    (admins + collaborating_users).uniq.reject(&filter)
  end
  # } NOTIFICATION INTERFACE

  def timeline_for(viewer)
    timeline = if writable_by?(viewer)
      events + comments
    else
      events.after_publication + comments.after_publication
    end

    timeline.sort_by(&:created_at)
  end

  def create_comment(actor, body)
    comment = comments.build(user: actor, body: body)
    # Trigger custom validation to check whether the comment may be created
    # after the advisory publication.
    comment.save(context: :form_submission)
    comment
  end

  def comment_and_close(actor, body)
    comment = nil
    instrument = false

    if body.present?
      comment = comments.build(user: actor, body: body)
    end

    transaction do
      if set_closed
        instrument = true
        comment.save(context: :form_submission) if comment
        add_close_event(actor)
      end
    end

    if instrument
      GlobalInstrumenter.instrument("repository_advisory.close", {
        repository_advisory: self,
        actor: actor,
      })
    end

    comment
  end

  def comment_and_reopen(actor, body)
    comment = nil
    instrument = false

    if body.present?
      comment = comments.build(user: actor, body: body)
    end

    transaction do
      if set_open
        instrument = true
        comment.save(context: :form_submission) if comment
        add_reopen_event(actor)
      end
    end

    if instrument
      GlobalInstrumenter.instrument("repository_advisory.reopen", {
        repository_advisory: self,
        actor: actor,
      })
    end

    comment
  end

  def handle_update(actor, params)
    update_successful = false

    transaction do
      title_was = self.title
      description_was = self.description

      saved = self.update(params)
      return false unless saved

      if self.title != title_was
        saved = self.add_rename_event(actor, title_was, self.title)

        # rollback transaction if event creation fails
        raise ActiveRecord::Rollback unless saved
      end

      add_user_content_edit!(description_was, self.description, actor)

      update_successful = true
    end

    update_successful
  end

  def deliver_credit_notifications
    credits.preload(:recipient).each(&:deliver_notifications)
  end

  def instrument_publish_event
    instrument :publish
  end

  def instrument_open_event
    instrument :open
  end

  def instrument_reopen_event
    instrument :reopen
  end

  def instrument_close_event
    instrument :close
  end

  EVENT_PAYLOAD_FIELDS = [:title, :body, :affected_versions, :severity, :description].freeze

  def instrument_update_event
    payload = event_payload

    EVENT_PAYLOAD_FIELDS.each do |field|
      old_value, current_value = previous_changes[field]
      payload.merge!("#{field}_was": old_value, "#{field}": current_value) if previous_changes.include?(field)
    end

    instrument :update, payload
  end

  def instrument_update_event?
    EVENT_PAYLOAD_FIELDS.any? do |field|
      changes.include?(field) || previous_changes.include?(field)
    end
  end

  def event_context(prefix: repository_advisory)
    {
      "#{prefix}".to_sym    => ghsa_id,
      "#{prefix}_id".to_sym => id,
    }
  end

  def event_payload
    payload = {
      repository_advisory: self,
      repo: repository,
    }

    if repository&.in_organization?
      payload[:org] = repository.organization
    end

    payload
  end

  def public_url
    # There's no public URL unless the advisory is published.
    return nil unless published?

    # Return nil if the repository isn't readable by an anonymous visitor (as a
    # public repo typically is). Using Repository#readable_by? instead of
    # Repository#public? accounts for GitHub.private_mode_enabled?.
    return nil unless repository.readable_by?(nil)

    "#{GitHub.url}#{async_path_uri.sync}"
  end

  # Typically, if a record is viewable by some user, that user also has access
  # to see that record's edit history. But repository advisories require some
  # special handling. While the repository advisory itself may be published and
  # viewable widely, the body attribute is treated as an initial, *internal*
  # comment. So the edit history of that internal comment should likewise be
  # kept internal, only viewable by collaborators on the repository advisory
  # itself.
  def async_viewer_can_read_user_content_edits?(viewer)
    async_writable_by?(viewer)
  end

  def lock_cve_request(temporary: true)
    if temporary
      GitHub.kv.setnx(cve_request_lock_key, "locked", expires: 72.hours.from_now)
    else
      GitHub.kv.set(cve_request_lock_key, "locked")
    end
  end

  def unlock_cve_request
    GitHub.kv.del(cve_request_lock_key)
  end

  def cve_request_locked?
    !!GitHub.kv.get(cve_request_lock_key).value { nil }
  end

  def cve_request_unlocks_at
    ttl = GitHub.kv.ttl(cve_request_lock_key).value { nil }
    ttl&.future? ? ttl : nil
  end

  def cve_request_pending?
    cve_request_locked? && cve_request_unlocks_at.present?
  end

  def notify_socket_subscribers
    data = {
      timestamp: Time.now.to_i,
      wait: default_live_updates_wait,
      reason: "RepositoryAdvisory ##{id} updated",
    }

    channel = GitHub::WebSocket::Channels.repository_advisory(self)
    GitHub::WebSocket.notify_repository_advisory_channel(self, channel, data)
  end

  private

  def set_ghsa_id!
    self.ghsa_id = AdvisoryDB::GhsaIdGenerator.generate_unique_ghsa_id
  end

  def set_owner_id!
    self.owner_id = repository.owner_id
  end

  def revoke_on_workspace(actor)
    return unless workspace_repository

    if actor.is_a?(Team)
      workspace_repository.remove_team(actor)
    else
      workspace_repository.remove_member(actor)
    end
  end

  def grant_on_workspace(actor, action)
    return unless workspace_repository

    if actor.is_a?(Team)
      workspace_repository.add_team(actor, action: action)
    else
      workspace_repository.add_member(actor, action: action)
    end
  end

  # CONVERTING TO PENDINGVULNERABILITY {
  def as_vulnerability_params
    {
      ghsa_id: ghsa_id,
      cve_id: cve_id,
      external_reference: public_url,
      description: maintainer_description,
      review_notes: review_notes,
      severity: severity,
      platform: nil,
      classification: "N/A",
      source: SOURCE_IDENTITY,
      source_identifier: id,
      created_by: publisher,
    }
  end

  def maintainer_description
    <<~MD
      ## #{title}

      #{description}
      MD
  end

  def review_notes
    <<~MD
      #### Package Ecosystem
      #{ecosystem.present? ? ecosystem : ATTRIBUTE_NOT_PROVIDED}

      #### Package Name
      #{package.present? ? package : ATTRIBUTE_NOT_PROVIDED}

      #### Affected Versions
      #{affected_versions}

      #### Patches
      #{patches.present? ? patches : ATTRIBUTE_NOT_PROVIDED}

      Source: #{permalink}
      MD
  end
  # } CONVERTING TO PENDINGVULNERABILITY

  # Get IDs of all users with admin access to the original repository. This
  # admin access may be granted directly on the repository or indirectly via a
  # team or the repository's organization. If the repository is personally
  # owned, the owner's ID is included as well.
  def admin_user_ids
    repository.user_ids_with_privileged_access(min_action: :admin)
  end

  # Get IDs of all users mentioned in the description of this repository
  # advisory, so long as those mentioned users are *also* authorized to see the
  # repository advisory. Currently, that means that the user must also be an
  # admin on the repository advisory's parent repository.
  #
  # In reality, Newsie's internals would _eventually_ block notifications for
  # mentioned users that that lack admin access to the repository advisory via
  # Newsies::DeliverNotificationsJob#reason_to_stop_delivery? returning
  # :thread_unreadable, but we want to be especially certain for sensitive
  # security information that no user is ever subscribed without having access.
  def mentioned_admin_user_ids
    repository.user_ids_with_privileged_access(
      min_action: :admin,
      actor_ids_filter: mentioned_users.map(&:id),
    )
  end

  # Get IDs of all teams mentioned in the description of this repository
  # advisory, so long as those mentioned teams are *also* authorized to see the
  # repository advisory. Currently, that means that the team must also have
  # admin access to the repository advisory's parent repository.
  def mentioned_admin_team_ids
    repository.actor_ids(
      type: Team,
      min_action: :admin,
      actor_ids_filter: mentioned_teams.map(&:id),
    )
  end

  def cve_request_lock_key
    "repository_advisory:#{ghsa_id}:cve_request_lock"
  end
end
