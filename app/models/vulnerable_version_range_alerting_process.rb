# frozen_string_literal: true

# The VulnerableVersionRangeAlertingProcess model belongs to a
# VulnerabilityAlertingEvent and may have siblings. Only a
# VulnerabilityAlertingEvent with a reason of "process_alerts" will have
# associated VulnerableVersionRangeAlertingProcess records.
#
# Tracking the alerting process across an entire VulnerableVersionRange is
# something we only do when we're processing alerts via Staff Tools.
class VulnerableVersionRangeAlertingProcess < ApplicationRecord::Notify
  areas_of_responsibility :dependabot_alerts

  belongs_to :vulnerability_alerting_event
  belongs_to :vulnerable_version_range

  scope :not_processed, -> { where(processed_at: nil) }

  def set_processed!
    transaction do
      update!(processed_at: Time.current)

      # Check to see if any of the alerting event's alerting processes are still
      # in flight. If not, mark the alerting event as processed too. This will
      # kick off a TriggerCombinedVulnerabilityAlertEmailsJob.
      #
      # See: VulnerabilityAlertingEvent#set_processed!
      if vulnerability_alerting_event.
          vulnerable_version_range_alerting_processes.
          not_processed.
          none?
        vulnerability_alerting_event.set_processed!
      end
    end
  end

  def processed?
    !!processed_at
  end

  def datadog_tags
    return @datadog_tags if @datadog_tags

    datadog_tags = vulnerability_alerting_event.datadog_tags.dup
    datadog_tags << "process_id:#{id}"

    if vulnerable_version_range
      datadog_tags.concat([
        "range_id:#{vulnerable_version_range.id}",
        "ecosystem:#{vulnerable_version_range.ecosystem}",
      ])
    end

    @datadog_tags = datadog_tags
  end
end
