# rubocop:disable Style/FrozenStringLiteralComment

# Public: A row in our permission system. An Ability represents the
# combination of a actor, a subject, and an action. An Ability may also have
# an ancestor, an Ability that's ultimately responsible for its existence.
#
#   https://githubber.com/article/technology/dotcom/abilities
#
# We're moving over to using Ability very carefully, so please consider
# talking with someone from @github/abilities before you dive in.
class Ability < ApplicationRecord::Domain::IamAbilities
  include Comparable
  include Instrumentation::Model

  BATCH_SIZE = 100 # how many records to insert or delete at a time

  FGP_ACTORS = [
    "IntegrationInstallation",
    "OauthAuthorization",
  ].freeze

  # This defines the how privileged an action is in relation to
  # other actions. This includes roles, which aren't ability actions,
  # but need to be compared against the privilege an ability action grants.
  ACTION_RANKING = {
    read: 0,
    triage: 0.5,
    write: 1,
    maintain: 1.5,
    admin: 2,
  }

  require_dependency "ability/actor"
  require_dependency "ability/subject"
  require_dependency "ability/participant"
  require_dependency "ability/grant"
  require_dependency "ability/graph"

  enum action: { read: 0, write: 1, admin: 2 }
  enum priority: { indirect: 0, direct: 1}

  attr_accessor :grantor_id

  # Public: The valid actions that an Ability can have.
  #
  # Returns an array of symbols.
  def self.valid_actions
    self.actions.keys.map(&:to_sym)
  end

  # Public: Is the specified action valid?
  #
  # action - The action to test validity of. Can be a string or a symbol.
  #
  # Returns a boolean.
  def self.valid_action?(action)
    self.actions.include?(action)
  end

  validates :actor_id, presence: true
  validates :actor_type, presence: true
  validates :action, presence: true
  validates :subject_id, presence: true
  validates :subject_type, presence: true

  # NOTE: these associations only work if the related participant is an
  # ActiveRecord model.
  belongs_to :actor,   polymorphic: true
  belongs_to :subject, polymorphic: true

  # The direct ability closest to a subject in an indirect ability
  belongs_to :parent, class_name: "Ability", foreign_key: "parent_id"

  # Returns a Relation to limit Ability records to those that grant
  # direct `:write` or `:admin` access.
  scope :direct_write, -> {
    direct.where(action: Ability.actions.values_at(:write, :admin))
  }

  scope :user_direct_read_on_organization, ->(actor_id:, subject_id:) {
    direct.where({
      actor_id: actor_id,
      actor_type: "User",
      subject_id: subject_id,
      subject_type: "Organization",
    })
  }

  scope :user_direct_read_on_repository, ->(actor_id:, subject_id:) {
    direct.where({
      actor_id: actor_id,
      actor_type: "User",
      subject_id: subject_id,
      subject_type: "Repository",
    })
  }

  scope :user_direct_write_on_repository, ->(actor_id:, subject_id:) {
    direct_write.where({
      actor_id: actor_id,
      actor_type: "User",
      subject_id: subject_id,
      subject_type: "Repository",
    })
  }

  scope :user_admin_on_organization, ->(actor_id:, subject_id:) {
    admin.where({
      actor_id: actor_id,
      actor_type: "User",
      subject_id: subject_id,
      subject_type: "Organization",
      priority: Ability.priorities[:direct],
    })
  }

  scope :user_admin_on_organizations, ->(actor_id:) {
    admin.where({
      actor_id: actor_id,
      actor_type: "User",
      subject_type: "Organization",
      priority: Ability.priorities[:direct],
    })
  }

  scope :teams_direct_on_repos, ->(repo_id:) {
    direct.where({
      actor_type: "Team",
      subject_type: "Repository",
      subject_id: repo_id,
    })
  }

  scope :teams_direct_on_projects, ->(project_id:) {
    direct.where({
      actor_type: "Team",
      subject_type: "Project",
      subject_id: project_id,
    })
  }

  # Returns a Relation to limit Ability records to those that grant
  # indirect `:read`, `:write` or `:admin` access via children abilities.
  scope :indirect_via_children, -> {
    joins(<<~SQL).
      LEFT OUTER JOIN `abilities` `children` ON
        `children`.`actor_type` = `abilities`.`subject_type` AND
        `children`.`actor_id` = `abilities`.`subject_id`
        /* abilities-join-audited */
    SQL
      where(priority: Ability.priorities.values_at(:direct, :indirect)).
      where(children: { priority: Ability.priorities[:direct] })
  }

  # Returns a Relation to limit Ability records to those that grant
  # indirect `:write` or `:admin` access via children abilities.
  scope :indirect_write_via_children, -> {
    indirect_via_children.where(children: { action: Ability.actions.values_at(:write, :admin) })
  }

  scope :user_indirect_read_via_children_on_repository, -> (actor_id:, subject_id:) {
    indirect_via_children.where({
      actor_id: actor_id,
      actor_type: "User",
      children: {
        subject_id: subject_id,
        subject_type: "Repository",
      },
    })
  }

  scope :user_indirect_write_via_children_on_repository, -> (actor_id:, subject_id:) {
    indirect_write_via_children.where({
      actor_id: actor_id,
      actor_type: "User",
      children: {
        subject_id: subject_id,
        subject_type: "Repository",
      },
    })
  }

  def self.grants(subject)
    directs = direct.where(
      subject_id: subject.ability_id,
      subject_type: subject.ability_type,
    )

    indirects = indirect_via_children.where(
      children: {
        subject_id: subject.ability_id,
        subject_type: subject.ability_type,
      },
    )

    directs + indirects
  end

  # The direct ability one step away from the subject in an indirect ability cascade
  def grandparent
    if grandparent_id && grandparent_id > 0
      @grandparent ||= Ability.find(grandparent_id)
    end
  end

  alias_method :original_subject, :subject

  def subject
    return self.original_subject unless FGP_ACTORS.include?(actor_type)

    ability_prefix = subject_type.split("/")[0..-2].join("/")
    resource       = subject_type.split("/").last

    case ability_prefix
    when Repository::Resources::INDIVIDUAL_ABILITY_TYPE_PREFIX
      Repository.find(subject_id).resources.public_send(resource)
    when Repository::Resources::ALL_ABILITY_TYPE_PREFIX
      # This will work for both users and orgs because of STI
      User.find(subject_id).repository_resources.public_send(resource)
    when Organization::Resources::ABILITY_TYPE_PREFIX, User::Resources::ABILITY_TYPE_PREFIX
      # This will work for both users and orgs because of STI
      User.find(subject_id).resources.public_send(resource)
    when ProtectedBranch::Resources::ABILITY_TYPE_PREFIX
      ProtectedBranch.find(subject_id).resources.public_send(resource)
    end
  end

  # Fake column representing a pointer to a grandparent
  def grandparent_id=(value)
    @grandparent_id = value
  end

  # Fake column representing a pointer to a grandparent.
  def grandparent_id
    # Use read_attribute because GitHub::SQL#models assigns attributes directly
    # instead of using attr_writers.
    @grandparent_id || read_attribute(:grandparent_id)
  end

  after_commit :instrument_grant, on: :create

  # Revoke role on ability destroy
  before_destroy :revoke_remaining_role!

  # NOTE: Moving instrument_revoke to commit callback causes test failures in
  # membership_hooks_test.rb and pull_request_serializer_test.rb, related to
  # the User being inaccessible once this callback is invoked.
  after_destroy :instrument_revoke # rubocop:disable GitHub/AfterCommitCallbackInstrumentation

  # Delete dependent abilities when an ability is destroyed.
  before_destroy do |a|
    a.delete_dependent_abilities
  end

  # Public: Can it be done?
  #
  # actor   - An Ability::Actor
  # action  - A :read, :write, or :admin Symbol
  # subject - An Ability::Subject
  #
  # Returns true or false.
  def self.can?(actor, action, subject)
    actor = actor.ability_delegate
    subject = subject.ability_delegate

    return false if actor.nil? || subject.nil?
    return false if actor.ability_id.nil? || subject.ability_id.nil?
    # A simpler `actor == subject` triggers `method_missing` when using `CollectionProxy` instances as actors or subjects,
    # causing the entire association to be queried and loaded
    return true  if actor.ability_type == subject.ability_type && actor.ability_id == subject.ability_id

    # Use ability_type/ability_id pairs instead of the actors or subjects
    # themselves, as using them as hash keys may have unintended side effects.
    # For example, Object#hash on an ActiveRecord::Associations::CollectionProxy
    # will load the contents of the association and delegate #hash to the
    # resulting array.
    key = ["can?",
           actor.ability_type, actor.ability_id,
           action,
           subject.ability_type, subject.ability_id]
    PermissionCache.fetch key do
      GitHub.dogstats.time "ability.can" do
        ActiveRecord::Base.connected_to(role: :reading) do
          can_sql_query(actor, action, subject)
        end
      end
    end
  end

  def self.can_sql_query(actor, action, subject)
    owning_org_id = subject.owning_organization_id

    sql = Ability.github_sql.new \
      actor_id: actor.ability_id,
      actor_type: actor.ability_type,
      action: Ability.actions[action],
      subject_id: subject.ability_id,
      subject_type: subject.ability_type,
      direct: Ability.priorities[:direct],
      admin_action: Ability.actions[:admin],
      owner_id: owning_org_id,
      owner_type: subject.owning_organization_type

    sql.add <<-SQL
      SELECT 1
      /* abilities-join-audited */
      FROM   abilities
      WHERE  actor_id     = :actor_id
      AND    actor_type   = :actor_type
      AND    action      >= :action
      AND    subject_id   = :subject_id
      AND    subject_type = :subject_type
      AND    (priority    <= :direct)

      UNION

      SELECT 1
      FROM   abilities parent
      JOIN   abilities grandparent
      ON     grandparent.subject_type = parent.actor_type
      AND    grandparent.subject_id   = parent.actor_id
      AND    parent.priority          <= :direct
      AND    grandparent.priority     <= :direct
      WHERE  grandparent.actor_id     = :actor_id
      AND    grandparent.actor_type   = :actor_type
      AND    parent.action           >= :action
      AND    parent.subject_id        = :subject_id
      AND    parent.subject_type      = :subject_type
    SQL

    if owning_org_id
      sql.add <<-SQL

        UNION

        SELECT 1
        FROM abilities
        WHERE actor_id   = :actor_id
        AND actor_type   = :actor_type
        AND subject_id   = :owner_id
        AND subject_type = :owner_type
        AND action       = :admin_action
      SQL
    end

    sql.add "LIMIT 1"

    sql.value?
  end

  def self.async_can?(actor, action, subject)
    Platform::Loaders::Ability.load(actor, subject).then { |max_action|
      if max_action
        max_action >= self.actions[action]
      else
        false
      end
    }
  end

  # Public: Queue a job to remove any abilities involving an actor and/or subject.
  #
  # participant - An Ability::Participant
  # async       - When true (default) will perform the clear! in a background job.
  #               NOTE: setting to false will perform inline without a throttler.
  #               If the entity you're clearing has an unknown amount of Abilities
  #               call clear! directly to get a throttled delete.
  #
  # Returns participant.
  def self.clear(participant, async: true)
    return participant if !participant.ability_delegate

    original_participant = participant
    participant          = participant.ability_delegate

    ability_id   = participant.ability_id
    ability_type = participant.ability_type

    if async
      ClearAbilitiesJob.perform_later(ability_id, ability_type)
    else
      begin
        clear!(ability_id, ability_type, throttle_writes: false)
      ensure
        GitHub.dogstats.increment("abilities.clear.sync")
      end
    end

    original_participant
  end

  # Public: Remove any abilities involving an actor and/or subject.
  #
  # ability_id   - The id of the Ability::Participant
  # ability_type - The type of the Ability::Participant
  #
  # Returns nothing.
  def self.clear!(ability_id, ability_type, throttle_writes: true)
    PermissionCache.clear

    GitHub.dogstats.time "ability", tags: ["action:clear"] do
      direct = Ability.github_sql.new \
        id:     ability_id,
        type:   ability_type,
        direct: Ability.priorities[:direct]

      direct.add <<-SQL
        SELECT id FROM abilities
        WHERE (actor_id = :id AND actor_type = :type)
        OR    (subject_id = :id AND subject_type = :type)
        AND priority = :direct
      SQL

      transaction do
        direct.values.each_slice(Ability::BATCH_SIZE) do |slice|
          if throttle_writes
            throttle { where(id: slice).delete_all }
          else
            where(id: slice).delete_all
          end

          delete_dependent_abilities_for!(slice)
        end
      end
    end
  end

  # Internal: Queue a job to delete abilities referencing any of the specified
  # abilities through the indirect grant ancestry information in
  # parent_id and/or grandparent_id
  #
  # ability_ids - IDs of the abilities whose dependent abilities we want to
  #               delete.
  #
  # Returns nothing.
  def self.delete_dependent_abilities_for(ability_ids)
    ids = [ability_ids].flatten.compact
    return if ids.empty?

    DeleteDependentAbilitiesJob.perform_later(ids)
  end

  # Internal: Delete abilities referencing any of the specified abilities through
  # the indirect grant ancestry information in parent_id and/or grandparent_id
  #
  # ability_ids - IDs of the abilities whose dependent abilities we want to
  #               delete.
  #
  # Returns nothing.
  def self.delete_dependent_abilities_for!(ability_ids)
    ids = [ability_ids].flatten.compact
    return if ids.empty?

    # abilities w/ parent_id's can be for forks or nested teams
    dependent_ids = ActiveRecord::Base.connected_to(role: :reading) do
      ids.each_with_object([]) do |id, arr|
        arr << Ability.github_sql.values(<<-SQL, id: id, direct: priorities[:direct])
          SELECT id FROM abilities
          WHERE parent_id = :id
          AND priority <= :direct
        SQL
      end.flatten
    end

    return if dependent_ids.empty?

    transaction do
      dependent_ids.each_slice(Ability::BATCH_SIZE) do |slice|
        throttle do
          Ability.github_sql.run <<-SQL, ids: slice
            DELETE FROM abilities WHERE id IN :ids
          SQL
        end
      end
      GitHub.dogstats.count "ability.revoked.dependent", dependent_ids.size
    end
  end

  # Public: Grant an actor the ability to perform an action on a subject.
  #
  # actor   - An Ability::Actor
  # action  - A :read, :triage, :write, :maintain, or :admin Symbol
  # subject - An Ability::Subject
  # grantor: The actor granting the ability
  #
  # Returns the granted Ability.
  def self.grant(actor, action, subject, grantor: nil)
    actor   = actor.ability_delegate
    subject = subject.ability_delegate

    PermissionCache.clear

    # Handle granting/revoking FGP roles on organization repositories
    if subject.is_a?(Repository) && subject.owner.organization?
      # clear out any old roles before granting a new one
      revoke_role!(actor, subject)
      if !Ability.valid_action?(action.to_sym)
        grant_role!(actor, subject, action.to_s)
        action = nil
      end
    end

    return unless action

    GitHub.dogstats.time "ability.grant" do
      Ability::Grant.new(actor, action, subject, grantor: grantor).apply
    end
  end

  # Public: Revoke an actor's direct ability to perform an action on a subject.
  #
  # actor      - An Ability::Actor
  # subject    - An Ability::Subject
  # background - Optional. Delete dependent abilities in a background job unless false.
  #
  # Returns nothing.
  def self.revoke(actor, subject, background: true)
    actor   = actor.ability_delegate
    subject = subject.ability_delegate

    PermissionCache.clear
    GitHub.dogstats.time "ability.revoke" do
      return unless ability = Ability.where(
        actor_id: actor.ability_id,
        actor_type: actor.ability_type,
        subject_id: subject.ability_id,
        subject_type: subject.ability_type,
        priority: Ability.priorities[:direct],
      ).first

      transaction do
        delete_dependent_abilities_for!([ability.id]) unless background
        ability.destroy
        GitHub.dogstats.increment "ability.revoked.direct"
      end
    end

    nil
  end

  # Public: Destroy the given abilities.
  #
  # abilities - An Ability relation
  #
  # Returns nothing.
  def self.revoke_abilities(abilities)
    return unless abilities.present?

    PermissionCache.clear

    GitHub.dogstats.time "ability.revoke" do

      transaction do
        abilities.destroy_all
        GitHub.dogstats.increment "ability.revoked.direct"
      end
    end

    nil
  end

  # Public: The String action name for this ability.
  #
  # Returns a String.
  def action_name
    action.to_s
  end

  # Internal: Compare two abilities' permissions and priority.
  # Mirrors the "prioritized" scope.
  #
  # other - Another Ability
  #
  # Returns the usual tri-state. See Comparable for details.
  def <=>(other)
    return nil unless other.is_a?(Ability)

    [Ability.actions[action], Ability.priorities[priority]] <=>
      [Ability.actions[other.action], Ability.priorities[other.priority]]
  end

  # Internal: Does this Ability have an action that's equal or better?
  #
  # other_action - A :read, :write, or :admin Symbol
  #
  # Returns true or false.
  def can?(other_action)
    Ability.actions[action] >= Ability.actions[other_action]
  end

  # Internal: Does this Ability have an action that's equal or better?
  #
  # action - A :read, :write, or :admin Symbol
  #
  # Returns true or false.
  def self.can_at_least?(action, permission)
    return false if permission.nil? || self.actions[permission].nil?
    self.actions[permission] >= self.actions[action]
  end

  # Internal: Delete any abilities referencing this ability as an indirect
  # parent or grandparent.
  #
  # This is used when revoking direct grants to revoke related indirect grants.
  #
  # Returns self.
  def delete_dependent_abilities
    self.class.delete_dependent_abilities_for self.id
    self
  end

  # Public:
  #
  # Returns a more readable representation of the Ability
  def to_s
    type = if !id && priority == :indirect
      "inferred indirect"
    elsif id && priority == :indirect
      "id=#{id}, materialized indirect"
    else
      "id=#{id} #{priority}"
    end

    actor   = "#{actor_type} #{self.actor}"   rescue "missing actor (#{actor_type}##{actor_id})"
    subject = "#{subject_type} #{self.subject}" rescue "missing subject (#{subject_type}##{subject_id})"

    str = "Ability #{type}: #{actor} has #{action} permission over #{subject}."
    str << " Parent is: ##{parent_id}" if parent_id != 0
    str
  end

  # Internal: Base payload for instrumentation events
  def event_payload
    {
      action:               action.to_sym,
      priority:             priority.to_sym,
      ability_actor_id:     actor_id,
      ability_actor_type:   actor_type,
      ability_subject_id:   subject_id,
      ability_subject_type: subject_type,
    }
  end

  # Internal: callback for instrumentation
  def instrument_grant
    instrument :grant, event_payload.merge(grantor_id: grantor_id)
  end

  # Internal: callback for instrumentation
  def instrument_revoke
    instrument :revoke
  end

  # Internal: Grants a role on a subject for an actor.
  #
  # Returns a RoleGrantResult.
  def self.grant_role!(actor, subject, role_name)
    grant_result = Permissions::Granters::RoleGranter.new(
      actor: actor, target: subject, role_name: role_name,
    ).grant!
  end

  # Internal: Revokes role on a subject for an actor.
  #
  # Returns a RoleGrantResult.
  def self.revoke_role!(actor, subject)
    if subject.is_a?(Repository) && subject.owner.organization?
      result = Permissions::Granters::RoleGranter.new(
        actor: actor, target: subject,
      ).revoke_if_exists!
    end
  end

  # Internal: Revokes role on a subject for an actor.
  #
  # Returns a RoleGrantResult.
  def revoke_role!
    if subject_type == "Repository" && subject.owner.organization?
      result = Permissions::Granters::RoleGranter.new(
        actor: actor, target: subject,
      ).revoke_if_exists!
    end
  end
  alias :revoke_remaining_role! :revoke_role!
end
