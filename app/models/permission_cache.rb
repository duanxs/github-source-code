# rubocop:disable Style/FrozenStringLiteralComment

# Internal: A thread-local, in memory cache and Rack middleware for permission data
#
# This cache is cleared when Ability data is granted, revoked, or cleared.
# See the Ability model for where that happens and also for where this cache is used.
class PermissionCache
  def initialize(app)
    @app = app
  end

  def call(env)
    PermissionCache.enable { @app.call(env) }
  end

  def self.clear
    data.clear
  end

  def self.fetch(key, &block)
    return yield if !enabled?

    if data.key?(key)
      GitHub.dogstats.increment("ability.cache", tags: ["result:hit"])
      return data[key]
    end

    GitHub.dogstats.increment("ability.cache", tags: ["result:miss"])
    data[key] = yield
  end

  def self.set(key, value)
    return if !enabled?
    data[key] = value
  end

  def self.get(key)
    return if !enabled?
    data[key]
  end

  def self.key?(key)
    return if !enabled?
    data.key?(key)
  end

  def self.enable(&block)
    clear
    self.enabled = true
    yield
  ensure
    self.enabled = false
    clear
  end

  def self.data
    Thread.current[:ability_cache_data] ||= {}
  end
  private_class_method :data

  def self.enabled?
    Thread.current[:ability_cache_enabled] || false
  end
  private_class_method :enabled?

  def self.enabled=(flag)
    Thread.current[:ability_cache_enabled] = flag
  end
  private_class_method :enabled=
end
