# frozen_string_literal: true

class ProjectCardPrefiller
  def initialize(project, cards, prefill_pull_requests: true, prefill_references: true, viewer: nil)
    @project = project
    @cards = cards
    @prefill_pull_requests = prefill_pull_requests
    @prefill_references = prefill_references
    @viewer = viewer
  end

  def cards
    @cards.each do |card|
      card.association(:project).target = @project unless card.redacted?
    end

    GitHub::PrefillAssociations.for_project_cards(@cards)
    GitHub::PrefillAssociations.for_projects(projects)
    prefill_issues
    prefill_pull_requests
    prefill_discussions

    @cards
  end

  private

  def notes
    @notes ||= @cards.select(&:is_note?)
  end

  def issues
    @issues ||= @cards.map(&:content).compact + referenced_issues
  end

  def pull_requests
    @pull_requests ||= issues.map(&:pull_request).compact
  end

  def projects
    [@project].concat(referenced_projects)
  end

  def referenced_issues
    return [] unless @prefill_references
    notes.each_with_object([]) do |card, issues|
      issues.concat(card.references(viewer: @viewer).issues)
    end
  end

  def referenced_projects
    return [] unless @prefill_references
    notes.each_with_object([]) do |card, projects|
      projects.concat(card.references(viewer: @viewer).projects)
    end
  end

  def referenced_discussions
    return [] unless @prefill_references
    notes.each_with_object([]) do |card, discussions|
      discussions.concat(card.references(viewer: @viewer).discussions)
    end
  end

  def prefill_discussions
    discussions = referenced_discussions
    GitHub::PrefillAssociations.prefill_belongs_to(discussions, User, :user_id)
    GitHub::PrefillAssociations.prefill_belongs_to(discussions, Team, :team_id)
  end

  def prefill_issues
    GitHub::PrefillAssociations.for_issues(issues,
      exclude_prefills: [:conversation, :milestones, :comment_counts])
    GitHub::PrefillAssociations.prefill_repository_owners(issues.map(&:repository))
  end

  def prefill_pull_requests
    return unless @prefill_pull_requests

    issues.each do |issue|
      if issue.pull_request?
        # Set issues/repositories on associated pull requests to prevent them
        # from being loaded a second time when accessed via the pull request.
        issue.pull_request.association(:issue).target = issue
        issue.pull_request.association(:repository).target = issue.repository
      end
    end

    pull_request_repos = []
    GitHub::PrefillAssociations.prefill_belongs_to(pull_requests, Repository, :base_repository_id, :head_repository_id) do |repos|
      pull_request_repos = repos
    end

    pull_requests.group_by(&:repository).each do |repo, repo_pull_requests|
      # Needed to render review state and build status on cards
      GitHub::PrefillAssociations.for_pulls_protected_branches(repo, repo_pull_requests)

      # Needed to render build status on cards
      PullRequest.attach_statuses(repo, repo_pull_requests)
    end

    # Needed to render review state on cards
    user_ids = GitHub::PrefillAssociations.prefillable_ids(pull_requests, :user_id, :base_user_id, :head_user_id)
    user_ids |= GitHub::PrefillAssociations.prefillable_ids(pull_request_repos, :owner_id, :organization_id)
    user_map = GitHub::PrefillAssociations.prefillable_ids_to_map(user_ids, User)
    GitHub::PrefillAssociations.prefill_with_map(pull_requests, user_map, :user_id, :base_user_id, :head_user_id)
    GitHub::PrefillAssociations.prefill_with_map(pull_request_repos, user_map, :owner_id, :organization_id)

    # Needed to render review state on cards
    GitHub::PrefillAssociations.prefill_associations pull_request_repos, [:network]
  end
end
