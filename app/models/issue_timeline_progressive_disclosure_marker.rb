# rubocop:disable Style/FrozenStringLiteralComment

# When only a portion of an IssueTimeline is rendered, use this marker to
# render a progressive disclosure UI (e.g., a "Load more" button).
class IssueTimelineProgressiveDisclosureMarker
  attr_reader :timeline, :first, :last, :limit, :focus_padding

  # Create a IssueTimelineProgressiveDisclosureMarker
  #
  # timeline - IssueTimeline that contains this marker
  # first    - Integer index into the timeline at which the next-loaded
  #            slice should start
  # end      - Integer index into the timeline at which the next-loaded
  #            slice should end
  # limit    - Integer count of items that should be loaded in the next slice
  # focus_padding - Integer count of items to include around the focused item
  def initialize(timeline, first:, last:, limit:, focus_padding:)
    @timeline = timeline
    @first = first
    @last = last
    @limit = limit
    @focus_padding = focus_padding
  end

  # How many items are hidden by this marker?
  def item_count
    last - first + 1
  end
end
