# frozen_string_literal: true

class MergeQueueEntry < ApplicationRecord::Collab
  include GitHub::Relay::GlobalIdentification

  # NOTE: These columns were originally meant to be used for paper trail purposes.
  #       Other models exist that might be better suited for that (e.g. IssueEvent, Deployment).
  #       As we learn more we can figure out how to approach it and either keep the columns or remove them.
  self.ignored_columns = %w(dequeuer_id dequeued_at dequeue_reason deploy_started_at repository_id)

  belongs_to :queue,
    required: true,
    class_name: :MergeQueue,
    foreign_key: :merge_queue_id,
    inverse_of: :entries

  belongs_to :pull_request, required: true
  belongs_to :enqueuer, class_name: :User, required: true
  has_many :group_entries, class_name: :MergeGroupEntry

  validates :pull_request, uniqueness: { scope: :queue, message: "is already in the queue" }
  validate :pull_request_belongs_to_repo
  validate :pull_request_mergeable, on: :create

  after_commit :create_timeline_event, on: :create

  enum state: {
    green: 0,
    merge_conflict: 1,
    ci_failing: 2,
  }

  def mergeable?
    return false if blocked_by_other_than_pending_required_status?
    return false if blocked_by_required_status?

    true
  end

  def blocked_by_other_than_pending_required_status?
    return true if blocked_by_merge_conflicts?
    return true if blocked_by_review_approval?
    return true if blocked_by_authorization?
    return true if required_status_failing?

    false
  end

  def blocked_by_merge_conflicts?
    return @blocked_by_merge_conflicts if defined?(@blocked_by_merge_conflicts)
    @blocked_by_merge_conflicts = !pull_request.git_merges_cleanly?
  end

  def blocked_by_required_status?
    !required_status_success?
  end

  def blocked_by_review_approval?
    return false unless pull_request.protected_base_branch

    review_policy_decision = pull_request
      .cached_merge_state(viewer: enqueuer)
      .pull_request_review_policy_decision

    return false unless review_policy_decision
    return false if review_policy_decision.policy_fulfilled?

    true
  end

  def blocked_by_authorization?
    return @blocked_by_authorization if defined?(@blocked_by_authorization)
    @blocked_by_authorization = !pull_request.base_repository.pushable_by?(enqueuer)
  end

  def required_status_success?
    status_check_rollup_state == StatusCheckRollup::SUCCESS
  end

  def required_status_pending?
    status_check_rollup_state == StatusCheckRollup::PENDING
  end

  def required_status_failing?
    status_check_rollup_state == StatusCheckRollup::FAILURE
  end

  def async_repository
    async_pull_request.then(&:async_repository)
  end

  def force_solo!
    update!(solo: true)
  end

  def adminable_by?(actor)
    enqueuer.present? && actor == enqueuer
  end

  def self.timestamp_attributes_for_create
    super << "enqueued_at"
  end
  private_class_method :timestamp_attributes_for_create

  private

  def pull_request_belongs_to_repo
    return if pull_request.blank?
    return if queue.blank?
    return if queue.repository_id == pull_request.repository_id

    errors.add(:pull_request, "must belong to the queue's repository")
  end

  def pull_request_mergeable
    return if mergeable?

    if blocked_by_merge_conflicts?
      errors.add(:pull_request, "has merge conflicts")
    end

    if blocked_by_review_approval?
      errors.add(:pull_request, "doesn't have required approvals")
    end

    if blocked_by_required_status? && required_status_failing?
      errors.add(:pull_request, "has failing required statuses")
    end

    if blocked_by_authorization?
      errors.add(:enqueuer, "is not authorized to merge")
    end
  end

  def create_timeline_event
    pull_request.events.create!(event: "added_to_merge_queue", actor: enqueuer)
  end

  def required_check_runs
    return [] unless pull_request.protected_base_branch
    return @required_check_runs if defined?(@required_check_runs)

    required_status_checks = pull_request
      .protected_base_branch
      .required_status_checks
      .pluck(:context)

    @required_check_runs = []
    return @required_check_runs if required_status_checks.empty?

    @required_check_runs = pull_request
      .combined_status
      .status_checks
      .filter { |s| required_status_checks.include?(s.context) }
  end

  def status_check_rollup_state
    return StatusCheckRollup::SUCCESS unless pull_request.protected_base_branch
    return StatusCheckRollup::SUCCESS if pull_request.combined_status.green?
    return StatusCheckRollup::SUCCESS if required_check_runs.empty?

    @status_check_rollup_state ||= StatusCheckRollup
      .new(status_checks: required_check_runs)
      .state
  end
end
