# rubocop:disable Style/FrozenStringLiteralComment

# Credit card specific methods to be mixed into the PaymentMethod model.
module PaymentMethod::CreditCardDependency
  extend ActiveSupport::Concern

  include GitHub::Billing::CreditCard
  # Public: The truncated credit card number, containing only the first six and last four
  # digits.
  # column :truncated_number
  # validates_presence_of :truncated_number

  # Public: Expiration date of the credit card
  # column :expiration_month, :expiration_year
  # validates_presence_of :expiration_month, :expiration_year

  # Public: Brand of the credit card. Can be "Visa", "MasterCard", "American Express",
  # "Diners Club", "JCB", or "Discover"
  # column :card_type
  # validates_presence_of :card_type

  # Public: Boolean if the payment method is a credit card.
  def credit_card?
    truncated_number.present? && valid_payment_token?
  end

  # Public: Last four digits of the credit card number.
  #
  # Returns a String.
  def last_four
    return unless credit_card?

    truncated_number[-4..-1]
  end

  # Public: Updates the last four digits of the credit card number.
  #
  # Returns nothing.
  def last_four=(new_last_4)
    self.truncated_number = "#{truncated_number[0...-4]}#{new_last_4}"
  end

  # Public: The first six digits of the credit card number, also known as the Bank Identification
  # Number.
  #
  # Returns a String.
  def bank_identification_number
    return unless credit_card?

    truncated_number[0..5]
  end

  # Public: More masked version of the truncated_number that only contains the first number and
  # the last four, with asterisks in the middle.
  #
  # Returns a String.
  def formatted_number
    return unless credit_card?
    format_card_number truncated_number, card_type
  end

  # Public: Expiration date as a DateTime.
  #
  # Returns a DateTime.
  def expiration_date
    return nil unless expiration_month? && expiration_year?
    DateTime.parse(formatted_expiration_date)
  end

  # Public: Expiration date in M/YYYY format.
  #
  # Returns a String.
  def formatted_expiration_date
    "#{expiration_month}/#{expiration_year}"
  end
end
