# frozen_string_literal: true
class ContactRequest
  include GitHub::AreasOfResponsibility
  areas_of_responsibility :community_and_safety

  include ActiveModel::Model
  include ActiveModel::Validations::Callbacks

  EMAIL_REGEX = /[\p{Alnum}\/*?|._%+!$#&'^`{}=~-]+@(?<domain>[\p{Alnum}.-]+\.[A-Z]{2,12})/ui

  PUBLIC_ATTRIBUTES = [:subject, :comments, :email, :flavor, :report,
    :name, :app_report, :action_report,
    :javascript_enabled, :referring_url,
    :referring_article_url, :referring_article_title, :default_inbox, :shop,
    :content_url, :classifier]

  METADATA_ATTRIBUTES = [:name, :email, :flavor,
    :ip_address, :browser, :spam, :spam_filtered,
    :ratelimited, :has_session, :javascript_enabled, :referring_article_title,
    :referring_article_url, :referring_url,
    :default_inbox, :login, :plan, :user_id, :anonymizing_proxy_user, :business_plus,
    :content_url, :report, :classifier, :spammy_user]

  before_validation :normalize_comments
  validates :flavor, inclusion: {
    in: GitHub.contact_form_flavors,
    message: "%{value} is not a valid contact form",
  }

  validate :ensure_valid_email

  validates :comments, length: { minimum: 15,
                                 too_short: "Please describe your issue with at least %{count} characters" }

  validate :template_modified

  after_validation :append_content_url

  def initialize(attributes = {})
    @attributes = attributes.symbolize_keys.slice(*PUBLIC_ATTRIBUTES)
    yield(self) if block_given?
  end

  PUBLIC_ATTRIBUTES.each do |key|
    define_method(key)       { @attributes[key] }
    define_method("#{key}?") { !!@attributes[key] }
    define_method("#{key}=") { |val| @attributes[key] = val }
  end

  def normalize_comments
    if self.comments.present?
      encoding = self.comments.encoding
      self.comments = self.comments.encode(encoding, universal_newline: true)
    end
  end

  def self.template(flavor)
    return "" unless GitHub.contact_form_flavors.has_key?(flavor)
    template_source_file = File.join(Rails.root,
                                     "app/views/site/contact/templates",
                                     "/#{flavor}.md")
    if File.exist?(template_source_file)
      template_source = File.read(template_source_file)
      Mustache.render(template_source,
                      { help_url: GitHub.help_url })
    else
      ""
    end
  end

  def comments_template
    ContactRequest.template(flavor)
  end

  def metadata(rate_limited:, user:, has_session:, user_agent:, remote_ip:)
    results = @attributes.slice(*METADATA_ATTRIBUTES)
    results[:browser]       = user_agent
    results[:ip_address]    = remote_ip
    results[:spam]          = self.spam? || rate_limited
    results[:spam_filtered] = self.spam?
    results[:ratelimited]   = rate_limited
    results[:has_session]   = has_session
    results[:default_inbox] = self.default_inbox

    if user
      results[:login]           = user.to_s
      results[:plan]            = user.plan.name
      results[:user_id]         = user.id
      results[:anonymizing_proxy_user]   = user.anonymizing_proxy_user?.to_s
      results[:business_plus]   = user.business_plus?.to_s
      results[:spammy_user]     = user.spammy?.to_s
    end

    results.compact!
    results
  end

  def content_object
    @content_object ||= GitHub::Resources.find_by_url(@attributes[:content_url])
  end

  def content_type
    content_object && content_object.class.name
  end

  def tags(logged_in:)
    tags = { flavor: self.flavor,
             logged_in: logged_in }

    if self.referring_article_title
      tags[:source] = "help"
    end

    tags
  end

  # Returns name if present, the username part of the email address
  # (e.g. `mona` in `mona@github.com`) if email is present, or
  # a placeholder if both name and email are blank.
  #
  # Use this when creating a ticket in Zendesk, which requires that
  # the requester's name be non-blank.
  def requester_name
    email_parts = email.present? ? email.split("@", 2) : nil

    case
    when name.present?
      name
    when email_parts && email_parts.first.length > 0
      email_parts.first
    when email.present?
      email
    else
      "(no name specified)"
    end
  end

  def spam?
    return true if self.comments.blank?

    # Some of our spammers have a very specific pattern
    return true if self.comments =~ /friendgate/mi
    return true if self.comments =~ /yourappreport\.com/mi
    return true if self.comments =~ /(\?>.*){3}/mi
    return true if self.comments =~ /(<a href|\[url=|\[link=)/mi
    return true if self.comments =~ /gist\.github\.com.*LİNK/mi
    return true if self.subject  =~ /gist\.github\.com.*LİNK/mi

    # Don't deliver the email if it contains >3 non-github links
    link_count = self.comments.scan(/http:\/\//).size
    github_link_count = self.comments.scan(/http:\/\/[a-zA-Z0-9.]*github.com\//).size
    return true if (link_count - github_link_count) > 3

    false
  end

  def template_modified
    if self.comments == comments_template
      errors.add(:comments,
                 "Your request was not submitted. " +
                 "Please complete the form by answering the questions.")
    end
  end

  private

  def ensure_valid_email
    m = EMAIL_REGEX.match(email)

    if m.nil? || !PublicSuffix.valid?(m[:domain], default_rule: nil, ignore_private: true)
      errors.add(:email, "Please enter a valid email address.")
    end
  end

  def append_content_url
    return unless content_url? && comments?
    text_to_append = "Report content: #{content_url}"
    return if comments.to_s.end_with?(text_to_append)
    self.comments = "#{comments}\n\n#{text_to_append}"
  end
end
