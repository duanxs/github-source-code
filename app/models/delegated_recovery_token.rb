# frozen_string_literal: true

class DelegatedRecoveryToken < ApplicationRecord::Domain::Users
  include Instrumentation::Model
  include GitHub::Relay::GlobalIdentification
  LEGACY_KEY_VERSION_ID = 0

  belongs_to :user

  scope :confirmed, -> { where("confirmed_at IS NOT NULL") }
  scope :unconfirmed, -> { where("confirmed_at IS NULL") }

  after_commit :instrument_creation, on: :create
  after_commit :instrument_update, on: :update
  after_commit :instrument_deletion, on: :destroy

  validates_uniqueness_of :token_id, case_sensitive: true
  validates_associated :user

  # Used to pass extra data to audit event
  attr_accessor :countersigned_token_id, :level_of_assurance

  STANDARD_FRICTION_PROOF = 0x00
  STANDARD_FRICTION_PROOF_MESSAGE = "The user either solved a 2FA challenge, social captcha, or provided a password from a vetted machine"

  # The `options` field of a recovery token is used to indicate what sort of
  # authentication occurred on Facebook's end. 0x02 has been designated to mean
  # "low friction proof"
  LOW_FRICTION_PROOF = 0x02
  LOW_FRICTION_PROOF_MESSAGE = "The user provided a valid password from an unknown device on Facebook.com"

  def event_prefix
    :account_recovery_token
  end

  def event_payload
    encryptor = if legacy_crypto?
      "legacy"
    else
      "earthsmoke:#{key_version_id}"
    end

    payload = {
      token_id: token_id,
      user: self.user,
      countersigned_token_id: countersigned_token_id,
      encryptor: encryptor,
    }

    payload[:level_of_assurance] = level_of_assurance if self.level_of_assurance

    payload
  end

  # Public: Instrument creating records.
  #
  # Returns nothing.
  def instrument_creation
    instrument :create
  end

  # Public: Instrument deleting records.
  #
  # Returns nothing.
  def instrument_deletion
    instrument :destroy
  end

  def mark_recovered(countersigned_token_options, countersigned_token_id)
    self.countersigned_token_id = countersigned_token_id
    self.recovered_at = Time.now.utc
    self.level_of_assurance = case countersigned_token_options
    when STANDARD_FRICTION_PROOF
      STANDARD_FRICTION_PROOF_MESSAGE
    when LOW_FRICTION_PROOF
      LOW_FRICTION_PROOF_MESSAGE
    else
      raise Darrrr::TokenFormatError, "value for options is unexpected: #{countersigned_token_options}"
    end
    self.save! # triggers instrument(:recover)
  end

  # Public: Instrument updating records.
  #
  # Returns nothing.
  def instrument_update
    if saved_change_to_confirmed_at? && confirmed_at
      instrument :confirm
    end

    if saved_change_to_recovered_at? && recovered_at
      instrument :recover
    end
  end

  def legacy_crypto?
    key_version_id == LEGACY_KEY_VERSION_ID
  end
end
