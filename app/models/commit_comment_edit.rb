# rubocop:disable Style/FrozenStringLiteralComment

class CommitCommentEdit < ApplicationRecord::Domain::Repositories
  include FilterPipelineHelper
  include Instrumentation::Model
  include UserContentEdit::Core

  belongs_to :commit_comment

  alias_attribute :user_content_id, :commit_comment_id
  alias_method :user_content, :commit_comment
  alias_method :async_user_content, :async_commit_comment

  def user_content_type
    "CommitComment"
  end

  def global_relay_id
    Platform::Helpers::NodeIdentification.to_global_id(platform_type_name, user_content_edit_id || "CommitCommentEdit:#{id}")
  end
end
