# frozen_string_literal: true

# Public: Represents a single discussions page in a repository with "hidden items"
# which are comments and events we haven't loaded yet. "hidden items" are not
# minimized comments, but may contain minimized comments.
class DiscussionTimeline::PaginatedRenderContext
  ITEMS_PER_PAGE_KEY = "discussion-timeline-items-per-page"
  DEFAULT_ITEMS_PER_PAGE = 30

  attr_reader :discussion, :viewer

  # Public: Access the number of timeline items that should be rendered before hiding
  # any that remain.
  def self.items_per_page
    GitHub.kv.get(ITEMS_PER_PAGE_KEY)
      .then { |value| GitHub::Result.new { Integer(value) } }
      .value { DEFAULT_ITEMS_PER_PAGE }
  end

  # Public: Set the number of timeline items that should be rendered before
  # hiding occurs.
  #
  # count - a positive integer
  def self.items_per_page=(count)
    GitHub.kv.set(ITEMS_PER_PAGE_KEY, count.to_s)
  end

  def initialize(
    discussion,
    viewer:,
    before_cursor: nil,
    after_cursor: nil,
    items_per_page: nil
  )
    @discussion = discussion
    @viewer = viewer
    @before_cursor = before_cursor
    @after_cursor = after_cursor
    @items_per_page = items_per_page || self.class.items_per_page
  end

  # Public: Returns an array of items to render on the discussions page.
  #
  # Each element in the returned array will be one of:
  # * Array - an array composed of DiscussionComment + DiscussionEventGroup + DiscussionEvent
  # * Symbol - :unread_marker representing where to render the unread div
  # * DiscussionHiddenItems - Representation of items we aren't showing to the
  #   user and allow to load async
  #
  # Returns an array of the above elements
  def renderables
    @renderables ||= begin
      [
        start_items,
        hidden_items,
        end_items,
        unread_marker,
      ].compact
    end
  end

  def timeline_items
    @timeline_items ||= start_items + end_items
  end

  attr_reader :items_per_page

  private

  def start_items
    @start_items ||= timeline_items_between_cursors.take(items_per_page / 2)
  end

  def end_items
    @end_items ||= timeline_items_between_cursors.drop(start_items.size).last(items_per_page - start_items.size)
  end

  def hidden_items
    if hidden_items?
      DiscussionTimelineHiddenItems.new(
        hidden_items_count,
        before: end_items.first || start_items.last,
        after: start_items.last,
      )
    end
  end

  def hidden_items_count
    @hidden_items_count ||= timeline_items_between_cursors.size - (start_items.size + end_items.size)
  end

  def hidden_items?
    hidden_items_count > 0
  end

  attr_reader :timeline, :before_cursor, :after_cursor

  def timeline_items_between_cursors
    @timeline_items_between_cursors ||= begin
      timeline_item_finder = DiscussionTimeline::ItemFinder
        .new(discussion, viewer: viewer)
        .select_top_level_items

      if before_cursor && after_cursor
        timeline_item_finder.between_cursors(before: before_cursor, after: after_cursor)
      end

      timeline_item_finder.to_a
    end
  end

  def unread_marker
    DiscussionTimeline::UNREAD_MARKER if before_cursor.nil? && after_cursor.nil?
  end
end
