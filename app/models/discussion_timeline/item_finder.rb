# frozen_string_literal: true

# Public: Used to find and filter DiscussionComment,
# DiscussionEvent, and DiscussionEventGroup objects.
class DiscussionTimeline::ItemFinder
  attr_reader :timeline_items

  def initialize(discussion, viewer:)
    @discussion = discussion
    @viewer = viewer
    @timeline_items = discussion.timeline_items_for(viewer)
  end

  def to_a
    timeline_items
  end

  def select_top_level_items
    @timeline_items.select! do |item|
      !item.respond_to?(:top_level_comment?) || item.top_level_comment?
    end

    self
  end

  def between_cursors(before: before_cursor, after: after_cursor)
    @timeline_items = timeline_items
      .drop_while { |event| !cursor_id_match?(event, cursor: after) }
      .drop(1) # drop_while drops until it matches, so we drop to avoid including the element represented by `after`
      .take_while { |event| !cursor_id_match?(event, cursor: before) }

    self
  end

  def since(time)
    @timeline_items.select! do |item|
      item.created_at > time
    end

    self
  end

  private

  def cursor_id_match?(record, cursor:)
    DiscussionTimelineHiddenItems.to_cursor(record) == cursor
  end
end
