# frozen_string_literal: true

class DiscussionTimeline::SingleCommentRenderContext
  attr_reader :discussion, :viewer, :timeline_since

  def initialize(discussion, comment, viewer: nil)
    @discussion = discussion
    @comment = comment
    @viewer = viewer
  end

  def renderables
    [[comment]]
  end

  def timeline_items
    [comment]
  end

  private

  attr_reader :comment
end
