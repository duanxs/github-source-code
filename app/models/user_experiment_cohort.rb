# frozen_string_literal: true

class UserExperimentCohort
  # PLEASE KEEP THE FOLLOWING EXPERIMENTS IN ALPHANUMERIC ORDER
  #
  # Active experiments
  AUTO_SUBSCRIBE_NEW_USERS_TO_EXPLORE_EMAIL = :auto_subscribe_new_users_to_explore_email
  DIRECT_ORGS                               = :direct_orgs
  ENTERPRISE_LINK_IN_USER_MENU              = :enterprise_link_in_user_menu
  FREE_PRIVATE_REPOSITORIES                 = :free_private_repositories
  FREE_TRIAL_LABEL                          = :free_trial_label
  GIT_LFS                                   = :git_lfs
  NEW_HEADER_NAV                            = :new_header_nav
  NEW_REPO_NAV                              = :new_repo_nav
  ORG_DOWNGRADE_EXIT_SURVEY                 = :org_downgrade_exit_survey
  PRICING_CONTACT_SALES                     = :pricing_contact_sales
  USER_DOWNGRADE_EXIT_SURVEY                = :user_downgrade_exit_survey

  # Inactive experiments
  ALTERNATE_CTA_LANGUAGE_ITERATION_1        = :alternate_cta_language_iteration_1
  CI_CTA_VERSUS_CI_MISSING_REVIEW_STATUS    = :ci_cta_versus_ci_missing_review_status
  CREATE_ONBOARDING_REPO_AFTER_SIGNUP       = :create_onboarding_repo_after_signup
  DASHBOARD_STUDENT_DEVELOPER_PACK_CTA      = :dashboard_student_developer_pack_cta
  DESKTOP_INTERSTITIAL                      = :desktop_interstitial
  DISPLAY_CURRENCIES_EXPERIMENT             = :display_currencies
  EMAIL_OPT_IN_PLACEMENT                    = :email_opt_in_placement
  EMAIL_VERIFICATION_INTERSTITIAL           = :email_verification_interstitial
  EMAIL_VERIFICATION_REDIRECT               = :email_verification_redirect
  ENTERPRISE_PAGE_CTA_BUTTONS               = :enterprise_page_cta_buttons
  ENTERPRISE_TRIAL_FLOW_WITH_LIMITED_FORMS  = :enterprise_trial_flow_with_limited_forms
  GHE_CLOUD_TRIAL                           = :ghe_cloud_trial
  GHE_TRIAL_UPGRADE                         = :ghe_trial_upgrade
  GUIDECAMP_EXPERIMENT                      = :guidecamp
  LEARNING_LAB_CTA                          = :learning_lab_cta
  MANDATORY_EMAIL_VERIFICATION              = :mandatory_email_verification
  MARKETPLACECICTA                          = :marketplacecicta
  MARKETPLACE_VS_APPS_NAV_BAR               = :marketplace_vs_apps_nav_bar
  MULTIPLE_INVITATION_INPUTS                = :multiple_invitation_inputs
  MULTI_INVITE_MEMBERS                      = :multi_invite_members
  MULTI_USER_INVITE                         = :multi_user_invite
  NEW_ORGANIZATION_EMAIL_NURTURE            = :new_organization_email_nurture
  ORGANIZATION_ONBOARDING_CHECKLIST         = :organization_onboarding_checklist
  PRICING_DISPLAY_EXPERIMENT_RELAUNCH       = :pricing_display_experiment_relaunch
  SIGNUP_AFTER_REPOSITORY_DOWNLOAD          = :signup_after_repository_download
  SIGNUP_CHOOSE_PLAN                        = :signup_choose_plan
  SIGNUP_INDIVIDUAL_PLAN_CARDS_MODAL        = :signup_individual_plan_cards_modal
  SIGNUP_LANDING                            = :signup_landing
  SIGNUP_REDESIGN                           = :signup_redesign
  SIGNUP_REDESIGN_PART_TWO                  = :signup_redesign_part_two
  SIGNUP_SUBSCRIPTION_CARDS_MODAL           = :signup_subscription_cards_modal
  SIGNUP_THREE_ARM                          = :signup_three_arm
  UNIFIED_ENTERPRISE_CTA                    = :unified_enterprise_cta
  WELCOME_EMAIL_SERIES                      = :welcome_email_series
  WELCOME_EXPERIENCE                        = :welcome_experience
  WELCOME_SERIES                            = :welcome_series
  WELCOME_SERIES_COMBO                      = :welcome_series_combo
end
