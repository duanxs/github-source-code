# rubocop:disable Style/FrozenStringLiteralComment

# Public: Protected Branches.
#
# The existence of a protected branch entry for a branch marks the branch as
# "protected". All other branches are considered "unprotected" by default.
#
# If a branch is protected:
# * By default, the branch may only be fast-forwarded
# * By default, the branch may not be deleted
#
# See
#   https://github.com/github/github/settings/branches/master
#
class ProtectedBranch < ApplicationRecord::Domain::Repositories
  extend GitHub::BackgroundDependentDeletes
  include GitHub::Validations
  include GitHub::Relay::GlobalIdentification

  include Ability::Subject
  include Ability::Membership

  include Instrumentation::Model

  include ProtectedBranch::PermissionsDependency

  include Permissions::Attributes::Wrapper
  self.permissions_wrapper_class = Permissions::Attributes::ProtectedBranch

  # Maximum number of restricted actors that can be specified for a protected branch.
  MAX_AUTHORIZED_ACTORS = 100

  # Maximum number of bytes in a protected branch title
  TITLE_BYTESIZE_LIMIT = 1024

  # Default value for the number of required approving reviews
  DEFAULT_REQUIRED_APPROVING_REVIEW_COUNT = 1

  # Maximum value for the number of required approving reviews
  MAX_REQUIRED_APPROVING_REVIEW_COUNT = 255

  # Internal: Raised when trying to add a restricted actor when MAX_AUTHORIZED_ACTORS
  # has already been reached.
  class TooManyPermittedActors < StandardError ; end

  # Internal: Authorized actors are only allowed on org owned repos.
  class OnlyOrgsHaveAuthorizedActors < StandardError ; end

  LEVELS = {
    off: 0,
    non_admins: 1,
    everyone: 2,
  }.with_indifferent_access.freeze

  enum required_status_checks_enforcement_level: LEVELS, _prefix: true
  enum pull_request_reviews_enforcement_level: LEVELS, _prefix: true
  enum signature_requirement_enforcement_level: LEVELS, _prefix: true
  enum block_force_pushes_enforcement_level: LEVELS, _prefix: true
  enum block_deletions_enforcement_level: LEVELS, _prefix: true

  # part of step 2 in https://github.com/github/ce-oss-happiness/issues/69
  # replacing block_force_pushes_enforcement_level
  enum allow_force_pushes_enforcement_level: LEVELS, _prefix: true
  # replacing block_deletions_enforcement_level
  enum allow_deletions_enforcement_level: LEVELS, _prefix: true

  enum linear_history_requirement_enforcement_level: LEVELS, _prefix: true

  validates :repository_id, presence: true
  validates :name, presence: true, uniqueness: { scope: :repository_id, message: "already protected: %{value}", case_sensitive: true }
  validates :creator_id, presence: true
  validates :name, bytesize: { maximum: TITLE_BYTESIZE_LIMIT }
  validate :creator_must_be_user
  validate :rule_is_valid
  validate :ensure_plan_support

  validates :required_status_checks_enforcement_level, presence: true
  validates :pull_request_reviews_enforcement_level, presence: true
  validates :signature_requirement_enforcement_level, presence: true
  validates :linear_history_requirement_enforcement_level, presence: true
  validates :block_force_pushes_enforcement_level, presence: true
  validates :block_deletions_enforcement_level, presence: true
  validate :ensure_consistent_merge_strategies

  validates_numericality_of :required_approving_review_count, {
    greater_than_or_equal_to: DEFAULT_REQUIRED_APPROVING_REVIEW_COUNT,
    less_than_or_equal_to: MAX_REQUIRED_APPROVING_REVIEW_COUNT,
    only_integer: true,
  }

  has_many :required_status_checks, -> { order("id") }
  destroy_dependents_in_background :required_status_checks

  belongs_to :repository
  belongs_to :creator, class_name: :User
  has_many :review_dismissal_allowances

  after_save :update_abilities_and_instrument_changes
  after_create_commit :instrument_create
  after_update_commit :instrument_update
  after_commit :synchronize_search_index
  after_destroy_commit :clear_app_permissions
  after_destroy_commit :instrument_destroy

  # derive values for allow_*_enforcement_level from block_*_enforcement_level to prepare for column renames
  before_validation :derive_force_pushes_enforcement_level, if: :will_save_change_to_block_force_pushes_enforcement_level?
  before_validation :derive_deletions_enforcement_level, if: :will_save_change_to_block_deletions_enforcement_level?

  extend Scientist

  def self.user_can_commit_to_branch?(repository, user, branch_name)
    return false unless user
    protected_branch = ProtectedBranch.for_repository_with_branch_name(repository, branch_name)
    return true unless protected_branch

    protected_branch.commit_authorized?(user)
  end

  # Returns the protection for the given branch name in the given repository.
  def self.for_repository_with_branch_name(repository, branch_name)
    Platform::Loaders::BranchProtectionRule::ByBranchName.load(repository, branch_name).sync
  end

  # Returns a mapping of branch names to protections for the given repository and branch names.
  def self.for_repository_with_branch_names(repository, branch_names)
    Promise.all(branch_names.map { |branch_name|
      Platform::Loaders::BranchProtectionRule::ByBranchName.load(repository, branch_name).then do |protected_branch|
        [branch_name, protected_branch]
      end
    }).sync.compact.to_h
  end

  def clear_app_permissions
    Permissions::QueryRouter.delete_app_permissions_on_subject(self)
  end

  def resources
    ProtectedBranch::Resources.new(self)
  end

  # Public: Replace protected status children with a list of new contexts.
  #
  # contexts - Array of String context names
  #
  # Returns nothing.
  def replace_status_contexts(contexts)
    contexts = Array(contexts)

    transaction do
      prefill_required_status_checks_associations

      diff = diff_status_contexts(contexts)
      destroy_required_status_checks(diff.destroy)
      create_required_status_checks(diff.create)
    end
  end

  CONTAINS_WILDCARD = /\*|\?|\\|\[/.freeze

  def wildcard_rule?
    name.match?(CONTAINS_WILDCARD)
  end

  def matches_qualified_ref_name?(qualified_ref_name)
    File.fnmatch?(qualified_name, qualified_ref_name, File::FNM_PATHNAME)
  end

  def matches?(branch_name)
    File.fnmatch?(name, branch_name, File::FNM_PATHNAME)
  end

  # Public: Create associated RequiredStatusChecks for the given contexts.
  #
  # contexts - Array of String Status context names
  #
  # Returns nothing.
  def create_required_status_checks(contexts)
    contexts.each { |c| self.required_status_checks.create!(context: c, protected_branch: self) }
  end

  # Public: Destroy associated RequiredStatusChecks matching the given
  # contexts.
  #
  # contexts - Array of String Status context names
  #
  # Returns nothing.
  def destroy_required_status_checks(contexts)
    checks_by_context = self.required_status_checks.index_by(&:context)
    self.required_status_checks.destroy(checks_by_context.values_at(*contexts).compact)
  end

  # Public: Update required status check settings.
  #
  # strict         - Optional Boolean specifying whether the branch must
  #                  be up to date with the base branch to pass
  #                  required status checks.
  # contexts       - Optional Array containing a list of context names
  #                  to be used for required status checks.
  # include_admins - Optional Boolean specifying whether admin users are
  #                  included in required status checks.
  def update_required_status_checks(include_admins: nil, strict: nil, contexts: nil)
    unless include_admins.nil?
      self.required_status_checks_enforcement_level = include_admins ? LEVELS[:everyone] : LEVELS[:non_admins]
      self.admin_enforced = include_admins ? true : false
    end
    self.required_status_checks_enforcement_level = LEVELS[:non_admins] unless required_status_checks_enabled?
    self.strict_required_status_checks_policy = strict unless strict.nil?
    self.replace_status_contexts(contexts) unless contexts.nil?
  end

  # Public: Clear required status check settings.
  def clear_required_status_checks
    self.strict_required_status_checks_policy = true
    self.required_status_checks_enforcement_level = disabled_enforcement_level
    self.required_status_checks.destroy_all
  end

  # Public: Enable pull request review enforcement.
  #
  # dismissal_restrictions - Optional hash specifying users and teams that have
  #                  dismissal power
  # dismiss_stale_reviews - Optional Boolean specifying weather to turn on dismiss
  #                  stale reviews or not
  # require_code_owner_reviews - Optional Boolean specifying weather to turn on requiring
  #                  code owners reviews or not
  def enable_required_pull_request_reviews(dismissal_restrictions: nil, dismiss_stale_reviews: nil, require_code_owner_reviews: nil, required_approving_review_count: nil)
    self.pull_request_reviews_enforcement_level = LEVELS[:non_admins] unless pull_request_reviews_enabled?

    unless dismiss_stale_reviews.nil?
      self.dismiss_stale_reviews_on_push = dismiss_stale_reviews
    end

    unless require_code_owner_reviews.nil?
      self.require_code_owner_review = require_code_owner_reviews
    end

    unless required_approving_review_count.nil?
      self.required_approving_review_count = required_approving_review_count
    end

    if dismissal_restrictions
      raise OnlyOrgsHaveAuthorizedActors unless repository.in_organization?

      if dismissal_restrictions.empty?
        clear_dismissal_restrictions
      else
        replace_dismissal_restricted_users_and_teams(
          user_ids: User.where(login: dismissal_restrictions["users"]).pluck(:id),
          team_ids: repository.organization.teams.where(slug: dismissal_restrictions["teams"]).pluck(:id))
      end
    end
    save
  end

  # Public: Does this protected branch enforce code owner reviews? Always returns
  # false if the feature is not supported by repo's billing plan.
  #
  # Returns a Boolean
  def require_code_owner_review
    return false unless repository.plan_supports?(:codeowners)

    super
  end
  alias_method :require_code_owner_review?, :require_code_owner_review

  # Public: Clear pull request review enforcement settings
  def clear_required_pull_request_reviews
    self.pull_request_reviews_enforcement_level = disabled_enforcement_level
    self.dismiss_stale_reviews_on_push = false
    self.require_code_owner_review = false
    self.required_approving_review_count = DEFAULT_REQUIRED_APPROVING_REVIEW_COUNT
    clear_dismissal_restrictions
  end

  def clear_dismissal_restrictions
    return unless review_dismissal_allowances
    change_made = review_dismissal_allowances.count > 0
    review_dismissal_allowances.destroy_all
    change_made ||= self.authorized_dismissal_actors_only
    self.authorized_dismissal_actors_only = false
    instrument_dismissal_restricted_users_teams if change_made
  end

  def enable_required_signatures
    self.signature_requirement_enforcement_level = enabled_enforcement_level(non_admins_possible: true)
  end

  def clear_required_signatures
    self.signature_requirement_enforcement_level = disabled_enforcement_level
  end

  def enable_required_linear_history
    self.linear_history_requirement_enforcement_level = enabled_enforcement_level(non_admins_possible: true)
  end

  def clear_required_linear_history
    self.linear_history_requirement_enforcement_level = disabled_enforcement_level
  end

  # Public: Prevent anyone from force-pushing to branches covered by this rule. This is the default for new
  # ProtectedBranches.
  #
  # Unlike other ProtectedBranch settings, blocked force pushes are everyone or no-one, regardless of the value of
  # `:admin_enforced?`.
  def enable_blocked_force_pushes
    self.block_force_pushes_enforcement_level = enabled_enforcement_level(non_admins_possible: false)
  end

  # Public: Allow force pushes to branches covered by this rule.
  def clear_blocked_force_pushes
    self.block_force_pushes_enforcement_level = disabled_enforcement_level
  end

  # Public: Prevent anyone from deleting branches covered by this rule. This is the default for new ProtectedBranches.
  #
  # Unlike other ProtectedBranch settings, blocked deletions are everyone or no-one, regardless of the value of
  # `:admin_enforced?`.
  def enable_blocked_deletions
    self.block_deletions_enforcement_level = enabled_enforcement_level(non_admins_possible: false)
  end

  # Public: Allow branches covered by this rule to be deleted.
  def clear_blocked_deletions
    self.block_deletions_enforcement_level = disabled_enforcement_level
  end

  # Public: Update push restriction settings.
  #
  # Allows setting or updating the list of users, teams or integrations that are
  # restricted to pushing to the protected branch.
  #
  # Note: Setting users, teams, integrations to empty lists does not disable
  #       push restrictions, but limits pushes to admins only. Use
  #       `clear_restrictions` to disable push restrictions.
  #
  # users        - Optional Array of User login names.
  # teams        - Optional Array of Team slug names.
  # integrations - Optional Array of Integration slug names.
  def update_restrictions(users: nil, teams: nil, integrations: nil)
    user_ids = if users
      User.where(login: users).pluck(:id)
    else
      authorized_user_ids
    end

    team_ids = if teams
      repository.teams(immediate_only: false).where(slug: teams).pluck(:id)
    else
      authorized_team_ids
    end

    integration_ids = if integrations
      Integration.where(slug: integrations).pluck(:id)
    else
      authorized_integration_ids
    end

    replace_authorized_actors(user_ids: user_ids, team_ids: team_ids, integration_ids: integration_ids)
  end

  # Public: Clear push restriction settings.
  def clear_restrictions
    self.authorized_actors_only = false
  end

  def add_users_to_restrictions(user_logins)
    present_user_logins = authorized_users.map(&:login)
    update_restrictions(users: user_logins + present_user_logins)
  end

  def remove_users_from_restrictions(user_logins)
    present_user_logins = authorized_users.map(&:login)
    update_restrictions(users: present_user_logins - user_logins)
  end

  def add_teams_to_restrictions(team_slugs)
    present_team_slugs = authorized_teams.map(&:slug)
    update_restrictions(teams: team_slugs + present_team_slugs)
  end

  def remove_teams_from_restrictions(team_slugs)
    present_team_slugs = authorized_teams.map(&:slug)
    update_restrictions(teams: present_team_slugs - team_slugs)
  end

  def add_integrations_to_restrictions(integration_slugs)
    update_restrictions(integrations: integration_slugs + authorized_integration_slugs)
  end

  def remove_integrations_from_restrictions(integration_slugs)
    update_restrictions(integrations: authorized_integration_slugs - integration_slugs)
  end

  # Public: Get qualified ref name for branch.
  #
  # Examples
  #   "refs/heads/master"
  #
  # Returns String.
  def qualified_name
    "refs/heads/#{name}"
  end

  def admin_enforced=(value)
    if value
      self.pull_request_reviews_enforcement_level = LEVELS[:everyone] if pull_request_reviews_enabled?
      self.required_status_checks_enforcement_level = LEVELS[:everyone] if required_status_checks_enabled?
      self.signature_requirement_enforcement_level = LEVELS[:everyone] if required_signatures_enabled?
      self.linear_history_requirement_enforcement_level = LEVELS[:everyone] if required_linear_history_enabled?
    else
      self.pull_request_reviews_enforcement_level = LEVELS[:non_admins] if pull_request_reviews_enabled?
      self.required_status_checks_enforcement_level = LEVELS[:non_admins] if required_status_checks_enabled?
      self.signature_requirement_enforcement_level = LEVELS[:non_admins] if required_signatures_enabled?
      self.linear_history_requirement_enforcement_level = LEVELS[:non_admins] if required_linear_history_enabled?
    end
    super
  end

  # Public: Can this actor override required status checks?
  #
  # actor - User/Bot or PublicKey to check for
  #
  # Returns Boolean
  def can_override_status_checks?(actor:)
    return false unless actor
    can_override_protection?(enforcement_level: required_status_checks_enforcement_level, actor: actor)
  end

  # Public: Can this actor override rejected pull request reviews?
  #
  # actor - User/Bot or PublicKey to check for
  #
  # Returns Boolean
  def can_override_review_policy?(actor:)
    return false unless actor
    can_override_protection?(enforcement_level: pull_request_reviews_enforcement_level, actor: actor)
  end

  # Public: Can this actor override required signatures?
  #
  # actor - User/Bot or PublicKey to check for.
  #
  # Returns boolean.
  def can_override_required_signatures?(actor:)
    return false unless actor
    can_override_protection?(enforcement_level: signature_requirement_enforcement_level, actor: actor)
  end

  # Public: Can this actor override blocked merge commits?
  #
  # actor = User/Bot or PublicKey to check for.
  #
  # Returns boolean.
  def can_override_required_linear_history?(actor:)
    return false unless actor
    can_override_protection?(enforcement_level: linear_history_requirement_enforcement_level, actor: actor)
  end

  def required_signatures_enforced_for?(actor:)
    return false unless required_signatures_enabled?
    return true if signature_requirement_enforcement_level == "everyone"

    !can_override_required_signatures?(actor: actor)
  end

  def required_status_checks_enforced_for?(actor:)
    return false unless required_status_checks_enabled?
    return true if required_status_checks_enforcement_level == "everyone"

    !can_override_status_checks?(actor: actor)
  end

  def required_review_policy_enforced_for?(actor:)
    return false unless pull_request_reviews_enabled?
    return true if pull_request_reviews_enforcement_level == "everyone"

    !can_override_review_policy?(actor: actor)
  end

  def required_linear_history_enforced_for?(actor:)
    return false unless required_linear_history_enabled?
    return true if linear_history_requirement_enforcement_level == "everyone"

    !can_override_required_linear_history?(actor: actor)
  end

  # Public: Default merge method for this protected branch
  #
  # Returns merge, rebase, or squash.
  def default_merge_method_for(actor)
    return @merge_method if defined? @merge_method
    @merge_method =
      if required_linear_history_enabled? && repository.default_merge_method_for(actor) == :merge
        if repository.squash_merge_allowed?
          :squash
        elsif repository.rebase_merge_allowed?
          :rebase
        else
          # ¯\_(ツ)_/¯
          # At least show a less confusing disabled button.
          :merge
        end
      else
        repository.default_merge_method_for(actor)
      end
  end

  def pull_request_reviews_enabled?
    !pull_request_reviews_enforcement_level_off?
  end

  def required_status_checks_enabled?
    !required_status_checks_enforcement_level_off?
  end

  def required_signatures_enabled?
    !signature_requirement_enforcement_level_off?
  end

  def required_linear_history_enabled?
    !linear_history_requirement_enforcement_level_off?
  end

  def block_force_pushes_enabled?
    !block_force_pushes_enforcement_level_off?
  end

  def block_deletions_enabled?
    !block_deletions_enforcement_level_off?
  end

  def admin_enforced?
    admin_enforced ||
      required_status_checks_enforcement_level_everyone? ||
      pull_request_reviews_enforcement_level_everyone? ||
      signature_requirement_enforcement_level_everyone? ||
      linear_history_requirement_enforcement_level_everyone?
  end

  # Internal: are PullRequestReviews required for this protected branch?
  def pull_request_reviews_required?
    !pull_request_reviews_enforcement_level_off?
  end

  # Public: Does this protected branch require this context?
  #
  # context - String name of a context
  #
  # Returns Boolean
  def has_required_status_check?(context)
    async_has_required_status_check?(context).sync
  end

  def async_has_required_status_check?(context)
    case context
    # Special casing for Travis. See https://github.com/github/repos/issues/143 for details
    when RequiredStatusCheck::TRAVIS_PR_CONTEXT, RequiredStatusCheck::TRAVIS_PUSH_CONTEXT
      Platform::Loaders::BranchProtectionRule::RequiredContextCheck.load_all(self.id, [
        RequiredStatusCheck::TRAVIS_SPECIAL_CASE_CONTEXT,
        context,
      ]).then(&:any?)
    else
      Platform::Loaders::BranchProtectionRule::RequiredContextCheck.load(self.id, context)
    end
  end

  # Public: Combine the current statuses passed with those that are expected for this branch. If
  # an expected status's context is found in the current statuses, remove the expected status from the list
  #
  # current_statuses - The list of statuses already reported for this branch
  #
  # Returns an Array of Statuses
  def current_statuses_with_expected(current_statuses)
    return current_statuses unless required_status_checks_enabled?

    contexts = Set.new(current_statuses.map(&:context))
    expected_statuses = required_status_checks.to_a

    # Remove continuous-integration/travis-ci special case when a Travis status has already been reported
    #
    # See https://github.com/github/repos/issues/143 for details
    if contexts.include?(RequiredStatusCheck::TRAVIS_PR_CONTEXT) || contexts.include?(RequiredStatusCheck::TRAVIS_PUSH_CONTEXT)
      expected_statuses.delete_if { |status| status.context == RequiredStatusCheck::TRAVIS_SPECIAL_CASE_CONTEXT }
    end

    expected_statuses.reject { |s| contexts.include?(s.context) } + current_statuses
  end

  # Public: Return the list of possible required status contexts for this
  # branch. This includes recent status contexts seen in the repository as well
  # as any existing required status checks.
  #
  # Returns a Set of Strings
  def possible_required_status_contexts
    @possible_required_status_contexts ||= begin
      statuses = Statuses::Service.recent_status_contexts(repo_id: repository.id, start: 1.week.ago, limit: RequiredStatusCheck::MAX_PER_BRANCH)
      checks = CheckRun.recent_check_names(repo_id: repository.id, start: 1.week.ago, limit: RequiredStatusCheck::MAX_PER_BRANCH)
      statuses.merge(checks).merge(required_status_checks.pluck(:context))
    end
  end

  # Public: Return the list of possible required status contexts for this branch, with special
  # casing for Travis enabled. See https://github.com/github/repos/issues/143 for details
  def possible_required_status_contexts_for_form
    @possible_required_status_contexts_for_form ||= begin
      contexts = possible_required_status_contexts.dup

      # Special case for Travis
      if contexts.include?(RequiredStatusCheck::TRAVIS_PUSH_CONTEXT) || contexts.include?(RequiredStatusCheck::TRAVIS_PR_CONTEXT)
        contexts.add RequiredStatusCheck::TRAVIS_SPECIAL_CASE_CONTEXT
        contexts.delete RequiredStatusCheck::TRAVIS_PUSH_CONTEXT
        contexts.delete RequiredStatusCheck::TRAVIS_PR_CONTEXT
      end

      contexts
    end
  end

  # Public: Does this protected branch have restricted users or teams specified for review dismissal?
  #
  # Returns Boolean
  def restricted_dismissed_reviews?
    authorized_dismissal_actors_only? && repository.in_organization?
  end

  # Public: Does this protected branch have restricted users or teams specified?
  # Personal accounts can never have authorized users or teams.
  #
  # Returns Boolean
  def has_authorized_actors?
    authorized_actors_only? && repository.in_organization?
  end

  def authorized_actors_only=(value)
    if value
      raise NotImplementedError, "Changing authorized_actors_only to true must be done in replace_authorized_actors."
    end

    super
  end

  def authorized_dismissal_actors_only=(value)
    if value
      raise NotImplementedError, "Changing authorized_dismissal_actors_only to true must be done in replace_authorized_actors."
    end

    super
  end

  # Public: Set the restricted users/teams/integrations for this protected branch.
  # Only works on org owned repositories.
  #
  # user_ids        - Optional Array of User ids
  # team_ids        - Optional Array of Team ids
  # integration_ids - Optional Array of Integration ids
  #
  # Returns nothing
  def replace_authorized_actors(user_ids:, team_ids:, integration_ids: nil)
    raise OnlyOrgsHaveAuthorizedActors unless repository.in_organization?

    user_ids ||= []
    team_ids ||= []
    integration_ids ||= []

    num_authorized_actors = user_ids.length + team_ids.length + integration_ids.length
    if num_authorized_actors > MAX_AUTHORIZED_ACTORS
      raise TooManyPermittedActors.new("Only #{MAX_AUTHORIZED_ACTORS} users, teams and apps can be specified.")
    end

    # Use update_column so `authorized_actors_only=` isn't called.
    update_column(:authorized_actors_only, true) unless has_authorized_actors?

    clear_authorized_actor_abilities_and_permissions

    if team_ids.any?
      repository.teams(immediate_only: false).where(id: team_ids).each do |team|
        add_authorized_actor(team)
      end
    end

    if user_ids.any?
      User.where(id: user_ids).each do |user|
        add_authorized_actor(user)
      end
    end

    # Find IntegrationInstallations by integration id scoped by repository
    if integration_ids.any?
      IntegrationInstallation.with_repository(repository).joins(:integration).
        where(integrations: { id: integration_ids }).each do |integration_installation|
          add_authorized_actor(integration_installation)
        end
    end

    instrument_authorized_actors
  end

  def replace_dismissal_restricted_users_and_teams(user_ids:, team_ids:)
    if (user_ids.length + team_ids.length) > MAX_AUTHORIZED_ACTORS
      raise TooManyPermittedActors.new("Only #{MAX_AUTHORIZED_ACTORS} users and teams can be specified.")
    end

    # Use update_column so `authorized_dismissal_actors_only=` isn't called.
    update_column(:authorized_dismissal_actors_only, true) unless restricted_dismissed_reviews?

    review_dismissal_allowances.destroy_all

    repository.teams(immediate_only: false).where(id: team_ids).each do |team|
      add_allowed_review_dismissal_actor(team)
    end

    User.where(id: user_ids).each do |user|
      add_allowed_review_dismissal_actor(user)
    end

    instrument_dismissal_restricted_users_teams
  end

  def dismissal_restricted_users
    review_dismissal_allowances.select { |restriction| restriction.actor.is_a?(User) }.map(&:actor)
  end

  def dismissal_restricted_teams
    review_dismissal_allowances.select { |restriction| restriction.actor.is_a?(Team) }.map(&:actor)
  end

  def instrument_authorized_actors
    authorized_actor_names = authorized_users.pluck(:login) + authorized_teams.pluck(:name) + authorized_integration_slugs

    instrument :authorized_users_teams, {
      name: name,
      authorized_actors: authorized_actor_names,
      authorized_actors_only: authorized_actors_only,
    }
  end

  def instrument_dismissal_restricted_users_teams
    authorized_dismissal_actor_names = dismissal_restricted_users.map(&:to_s) + dismissal_restricted_teams.map(&:to_s)

    instrument :dismissal_restricted_users_teams, {
      name: name,
      authorized_actors: authorized_dismissal_actor_names,
      authorized_actors_only: authorized_dismissal_actors_only,
    }
  end

  # Public: The list of users, teams or IntegrationInstallations that updating this branch is restricted to
  #
  # Returns an Array of Users, Teams and IntegrationInstallations
  def authorized_actors
    return [] unless has_authorized_actors?

    authorized_users.to_a + authorized_teams.to_a + authorized_integration_installations.to_a
  end

  # Public: Get the Users authorized to push to this protected branch
  def authorized_users
    return [] unless has_authorized_actors?

    User.where(id: authorized_user_ids)
  end

  # Public: Get User ids authorized to push to this protected branch
  def authorized_user_ids
    return [] unless has_authorized_actors?

    Ability.where(
      actor_type: "User",
      subject_id: id,
      subject_type: "ProtectedBranch",
      priority: Ability.priorities[:direct],
    ).pluck(:actor_id)
  end

  # Public: Get the Teams authorized to push to this protected branch
  def authorized_teams
    return [] unless has_authorized_actors?

    Team.where(id: authorized_team_ids)
  end

  # Public: Get the Teams authorized to push to this protected branch
  def authorized_teams_with_preloaded_org
    return [] unless has_authorized_actors?

    Team.includes(:organization).where(id: authorized_team_ids)
  end

  # Public: Get Team ids authorized to push to this protected branch
  def authorized_team_ids
    return [] unless has_authorized_actors?

    Ability.where(
      actor_type: "Team",
      subject_id: id,
      subject_type: "ProtectedBranch",
      priority: Ability.priorities[:direct],
    ).pluck(:actor_id)
  end

  # Public: Get the IntegrationInstallations authorized to push to this protected branch
  def authorized_integration_installations
    return [] unless has_authorized_actors?

    IntegrationInstallation.where(id: authorized_integration_installation_ids)
  end

  # Public: Get IntegrationInstallations ids authorized to push to this protected branch
  def authorized_integration_installation_ids
    return [] unless has_authorized_actors?

    Permissions::Service.actor_ids_granted_permission(
      actor_type: "IntegrationInstallation",
      subject_type: "ProtectedBranch/contents",
      subject_ids: [self.id],
      action: Ability.actions[:write],
    )
  end

  # Public: The integration ids for the authorized installations
  def authorized_integration_ids
    return [] unless has_authorized_actors?

    authorized_integration_installations.pluck(:integration_id)
  end

  # Public: The integrations for the authorized installations
  def authorized_integrations
    return [] unless has_authorized_actors?

    Integration.where(id: authorized_integration_ids)
  end

  # Public: Has the number of restricted users and teams been reached?
  #
  # Returns Boolean
  def authorized_actors_limit_reached?
    authorized_actors.size >= MAX_AUTHORIZED_ACTORS
  end

  # Public: Has the number of restricted users and teams been reached?
  #
  # Returns Boolean
  def dismissal_restricted_users_and_teams_limit_reached?
    review_dismissal_allowances.size >= MAX_AUTHORIZED_ACTORS
  end

  # ProtectedBranch#authorized? has moved to:
  # app/models/protected_branch/permissions_dependency.rb

  # Public: the name of this branch, but tagged as UTF-8 and scrubbed so that it
  # it suitable for display.  +name+ is binary and *should not* be displayed
  # as a raw value in HTML.  Use this method instead.
  #
  # When should I use this method:
  #
  # * When *displaying* the protected branch name (in HTML or an email)
  #
  # When should I not use this method:
  #
  # * Communicating with GitRPC
  # * Constructing URLs
  def name_for_display
    name.dup.force_encoding("UTF-8").scrub!
  end

  def requires_status_checks_or_reviews?(user)
    return true if !can_override_status_checks?(actor: user) && required_status_checks.any?
    return true if !can_override_review_policy?(actor: user) && pull_request_reviews_required?

    false
  end

  # Checks to see if the actor who is passed in is in the list of allowed dismissers
  #
  # actor - the current_user viewing the page
  #
  # returns false if the actor is not logged_in
  # returns true if the actor is an admin and admin restrictions are not turned on
  # returns true if actor is in the list of specified users
  # returns true if actor is on one of the specified teams or children of those teams
  def review_dismissable_by?(actor)
    return false unless actor
    return true if !admin_enforced? && repository.resources.administration.writable_by?(actor)
    return true if review_dismissal_allowances.where(actor_id: actor.id, actor_type: "User").any?

    teams = review_dismissal_allowances.select { |restriction| restriction.actor.is_a?(Team) }.map(&:actor)
    Team.member_of?(teams.map(&:id), actor.id, immediate_only: false)
  end

  def deep_copy_as!(name:, creator: nil)
    protected_branch = dup
    protected_branch.name = name
    protected_branch.creator = creator if creator

    protected_branch.save!

    review_dismissal_allowances.each do |review_dismissal_allowance|
      protected_branch.review_dismissal_allowances.create!(actor: review_dismissal_allowance.actor)
    end

    required_status_checks.each do |required_status_check|
      protected_branch.required_status_checks.create!(context: required_status_check.context)
    end

    if has_authorized_actors?
      protected_branch.replace_authorized_actors(user_ids: authorized_user_ids, team_ids: authorized_team_ids,
                                                          integration_ids: authorized_integration_ids)
    end

    protected_branch
  end

  BRANCH_PROTECTION_RULE = "BranchProtectionRule".freeze
  def platform_type_name
    BRANCH_PROTECTION_RULE
  end

  private

  # Private: derive allow_force_pushes_enforcement_level value from block_force_pushes_enforcement_level
  def derive_force_pushes_enforcement_level
    self.allow_force_pushes_enforcement_level = INVERTED_LEVELS[block_force_pushes_enforcement_level]
  end

  # Private: derive allow_deletions_enforcement_level value from block_deletions_enforcement_level
  def derive_deletions_enforcement_level
    self.allow_deletions_enforcement_level = INVERTED_LEVELS[block_deletions_enforcement_level]
  end

  # Branch names can be split into different components (the parts between `/`).
  # This is a list of characters that can not be part of such a component for a rule.
  RULE_COMPONENT_CHARS = /[^\x00-\x1F\s\t\\:\^~\/]/
  VALID_RULE = /\A#{RULE_COMPONENT_CHARS}+(\/#{RULE_COMPONENT_CHARS}+)*\z/

  def rule_is_valid
    return if name.nil? || name.b.match?(VALID_RULE)

    errors.add :rule, "is invalid"
  end

  # Does the repository fall under a plan that supports protected branches?
  def ensure_plan_support
    return if repository&.plan_supports?(:protected_branches)
    errors.add :repository, "plan does not support protected branches"
  end

  # Internal: if linear history is enforced, ensure that the associated repo permits a merging method that doesn't
  # create a merge commit.
  def ensure_consistent_merge_strategies
    return if !required_linear_history_enabled? ||
      repository.nil? ||
      repository.squash_merge_allowed? ||
      repository.rebase_merge_allowed?

    errors.add :linear_history_requirement_enforcement_level,
      "cannot enforce linear history without a non-merge strategy enabled"
  end

  # Private: If authorized_actors_only changes from true to false, remove all
  # abilities on this protected branch and instrument the changes. Changing from
  # false to true isn't handled here because it is done in
  # `replace_authorized_actors`
  def update_abilities_and_instrument_changes
    if authorized_actors_only_before_last_save == true && authorized_actors_only == false
      clear_authorized_actor_abilities_and_permissions
      instrument_authorized_actors
    end
  end

  def clear_authorized_actor_abilities_and_permissions
    Ability.transaction do
      # Delete abilities for Users and Teams
      Ability.clear(self, async: false)

      clear_app_permissions
    end
  end

  # Private: Return the enforcement level that should be used for newly enabled `_enforcement_level` enum fields that
  # should be kept consistent with the current value of `admin_enforced?`.
  def enabled_enforcement_level(non_admins_possible:)
    !non_admins_possible || admin_enforced? ? LEVELS[:everyone] : LEVELS[:non_admins]
  end

  # Private: Return the enforcement level that should be used for disabled `_enforcement_level` enum fields.
  def disabled_enforcement_level
    LEVELS[:off]
  end

  # Internal: The integration slugs for the authorized installations
  def authorized_integration_slugs
    return [] unless has_authorized_actors?

    authorized_integration_installations.joins(:integration).pluck(:"integrations.slug")
  end

  # Internal: Creates ReviewDismissalAllowance for user or team actor
  #
  # Returns the ReviewDismissalAllowance record.
  def add_allowed_review_dismissal_actor(actor)
    return unless repository_writable_by?(actor)

    review_dismissal_allowances.create(actor: actor)
  end

  # Internal: Adds an authorized user or team.
  #
  # Returns the Ability record.
  def add_authorized_actor(actor)
    return unless repository_writable_by?(actor)

    if actor.is_a?(IntegrationInstallation)
      ::Permissions::Service.grant_app_permission(actor: actor, subject: resources.contents, action: :write)
    else
      grant actor, :write
    end
  end

  # Internal: Used by abilities to ensure that only Users or Teams can be granted abilities
  #
  # See Ability::Subject#grant? for more info.
  def grant?(actor, action)
    (actor.is_a?(User) || actor.is_a?(Team)) && super
  end

  # Internal: Can this actor override protected branch settings
  #
  # actor - User/Bot, or PublicKey to check for
  #
  # Returns Boolean
  def can_override_protection?(enforcement_level:, actor:)
    case enforcement_level
    when "off"
      true
    when "non_admins"
      case actor
      when PublicKey
        write_deploy_key?(actor)
      when User
        repository.resources.administration.writable_by?(actor)
      else
        raise TypeError, "expected actor to be a User or PublicKey, but was #{actor.class}"
      end
    when "everyone"
      false
    end
  end

  # Private: Instrument creation of this ProtectedBranch
  def instrument_create
    instrument :create, user: creator

    GlobalInstrumenter.instrument("branch_protection_rule.create", {
      actor: creator,
      repository: repository,
      repository_owner: repository.owner,
      branch_protection_rule: self,
    })
  end

  # we need to invert the levels until the legacy block_* column names are renamed to allow_*
  # currently used for instrumenting update_allow_force_pushes_enforcement_level and update_allow_deletions_enforcement_level
  # tracked in https://github.com/github/ce-oss-happiness/issues/69
  INVERTED_LEVELS = {
    off: LEVELS[:everyone],
    non_admins: LEVELS[:off],
    everyone: LEVELS[:off],
  }.with_indifferent_access.freeze

  # Private: Instrument update of this record with the new values
  def instrument_update
    actor = (User.find_by(id: GitHub.context[:actor_id]) || creator)

    GlobalInstrumenter.instrument("branch_protection_rule.update", {
      actor: actor,
      repository: repository,
      repository_owner: repository.owner,
      branch_protection_rule: self,
    })

    if saved_change_to_required_status_checks_enforcement_level?
      instrument :update_required_status_checks_enforcement_level, {
          required_status_checks_enforcement_level: LEVELS[required_status_checks_enforcement_level],
        }
    end

    if saved_change_to_strict_required_status_checks_policy?
      instrument :update_strict_required_status_checks_policy, {
          strict_required_status_checks_policy:
            read_attribute(:strict_required_status_checks_policy),
        }
    end

    if saved_change_to_dismiss_stale_reviews_on_push?
      instrument :dismiss_stale_reviews, {
        dismiss_stale_reviews_on_push:
          read_attribute(:dismiss_stale_reviews_on_push),
      }
    end

    if saved_change_to_require_code_owner_review?
      instrument :update_require_code_owner_review, {
        require_code_owner_review:
          read_attribute(:require_code_owner_review),
      }
    end

    if saved_change_to_pull_request_reviews_enforcement_level?
      instrument :update_pull_request_reviews_enforcement_level, {
        pull_request_reviews_enforcement_level: LEVELS[pull_request_reviews_enforcement_level],
      }
    end

    if saved_change_to_required_approving_review_count?
      instrument :update_required_approving_review_count, {
        required_approving_review_count:
          read_attribute(:required_approving_review_count),
      }
    end

    if saved_change_to_signature_requirement_enforcement_level?
      instrument :update_signature_requirement_enforcement_level, {
        signature_requirement_enforcement_level: LEVELS[signature_requirement_enforcement_level],
      }
    end

    if saved_change_to_linear_history_requirement_enforcement_level?
      instrument :update_linear_history_requirement_enforcement_level, {
        linear_history_requirement_enforcement_level: LEVELS[linear_history_requirement_enforcement_level],
      }
    end

    if saved_change_to_admin_enforced?
      instrument :update_admin_enforced, {
        admin_enforced:
          read_attribute(:admin_enforced),
      }
    end

    if saved_change_to_block_force_pushes_enforcement_level?
      instrument :update_allow_force_pushes_enforcement_level, {
        allow_force_pushes_enforcement_level: INVERTED_LEVELS[block_force_pushes_enforcement_level],
      }
    end

    if saved_change_to_block_deletions_enforcement_level?
      instrument :update_allow_deletions_enforcement_level, {
        allow_deletions_enforcement_level: INVERTED_LEVELS[block_deletions_enforcement_level],
      }
    end
  end

  # Private: Instrument deletion of this record with the actor who did it
  def instrument_destroy
    instrument :destroy
    actor = (User.find_by(id: GitHub.context[:actor_id]) || creator)

    GlobalInstrumenter.instrument("branch_protection_rule.destroy", {
      actor: actor,
      repository: repository,
      repository_owner: repository&.owner,
      branch_protection_rule: self,
    })
  end

  def synchronize_search_index
    if repository
      repository.pull_requests.open_pulls.where(base_ref: Git::Ref.permutations(name)).each do |pr|
        Search.add_to_search_index("pull_request", pr.id)
      end
    end
  end

  def event_payload
    payload = {
      event_prefix => self,
      :repo => repository,
      :name => name,
    }

    if repository && repository.in_organization?
      payload[:org] = repository.organization
    end

    payload
  end

  # Internal: Verify that the creator is a User, not something else (like an
  # Organization).
  def creator_must_be_user
    # We only run the validation when creator_id_changed? because Users can be
    # transformed to Organizations behind the scenes. When that happens we
    # still want the protected branches created by the transformed user to be
    # able to be updated.
    if creator_id_changed? && creator && (!creator.user? && !creator.is_a?(Bot))
      errors.add :creator_id, "must be a User"
    end
  end

  # Internal: is the actor a writable deploy key? These are treated as admins
  # for the purposes of overriding protected branch requirements. See
  # https://github.com/github/github/issues/69229 for discussion.
  def write_deploy_key?(actor)
    repository.owner == actor.ability_delegate &&
      (actor.respond_to?(:read_only?) && !actor.read_only?)
  end

  # Internal: Prefill ProtectedBranchStatus#protected_branch on self.required_status_checks
  # to avoid redundant queries.
  def prefill_required_status_checks_associations
    self.required_status_checks.each { |s| s.association(:protected_branch).target = self }
    # Audit log instrumentation digs into
    # status.protected_branch.repository.owner, so preload those too.
    self.repository.owner
  end

  StatusContextDiff = Struct.new(:create, :destroy)

  # Internal: Compare the given list of contexts to the current set of required
  # contexts and return a StatusContextDiff describing the difference between
  # the two.
  #
  # contexts - Array of String Status context names
  #
  # Returns a StatusContextDiff.
  def diff_status_contexts(contexts)
    existing_contexts = self.required_status_checks.map(&:context)

    contexts_to_create  = contexts - existing_contexts
    contexts_to_destroy = existing_contexts - contexts

    StatusContextDiff.new(contexts_to_create, contexts_to_destroy)
  end
end
