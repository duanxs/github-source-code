# frozen_string_literal: true

# An Organization is a special type of User. You cannot log into the
# site as an Organization. Instead, an organization can be fully
# administrated by any number of admins.
#
# We subclass User because they play such similar roles on the site:
# they own repositories, they follow users, they have a news feed. The
# main difference is as noted above: you can't log in as an
# organization.
class Organization < User
  include Configurable::RemindersBeta
  include Instrumentation::Model

  include Organization::ActionsDependency
  include Organization::TokenScanningDependency
  include MailchimpTeamListHelper

  include Permissions::Attributes::Wrapper
  self.permissions_wrapper_class = Permissions::Attributes::Organization

  BATCH_SIZE = 100
  MEGA_ORG_MEMBER_THRESHOLD = 50_000
  MEGA_ORG_REPOS_THRESHOLD = MEGA_ORG_MEMBER_THRESHOLD

  areas_of_responsibility :orgs

  # Value for user-into-org transformation flag.
  TRANSFORM_FLAG = "1".freeze
  # Keep transform flags around this long if the cleanup didn't work to allow
  # time for debugging.
  TRANSFORM_FLAG_EXPIRY = 14.days

  # Hack for Rails pre-zeitwerk autoloader
  # Because Organization inherits from User, for any of these modules whose
  # name matches anoter module under User::, we will end up with that module
  # instead.
  # Zeitwerk will solve this through autoloading, but until we enable it by
  # default, we must require these files explicitly.
  Dir["organization/*.rb", base: __dir__].each do |filename|
    feature = filename[0..-4]
    require_dependency feature
  end

  include Organization::AbilityDependency
  include Organization::BetaFeaturesDependency
  include Organization::BillingManagementDependency
  include Organization::ConfigurationDependency
  include Organization::DormancyDependency
  include Organization::EnterpriseDependency
  include Organization::HovercardDependency
  include Organization::InvitationsDependency
  include Organization::OauthApplicationPolicyDependency
  include Organization::OnboardingDependency
  include Organization::PolicyDependency
  include Organization::ProjectsDependency
  include Organization::UserRankedDependency
  include Organization::UserStatusDependency
  include Organization::SamlSsoEnforcementDependency
  include Organization::SponsorsDependency
  include Organization::TeamSyncDependency
  include Organization::TradeControlsDependency
  include Organization::TwoFactorRequirementDependency
  include Organization::MemexProjectsDependency

  serialize :raw_data, Coders::Handler.new(Coders::OrganizationCoder)
  delegate *Coders::OrganizationCoder.members, to: :raw_data

  alias_attribute :billing_email, :organization_billing_email
  alias_attribute :description, :profile_bio

  # Thrown when someone attempts to demote the last admin to a direct member.
  class NoAdminsError < StandardError; end

  # Thrown when an attempt to transform a user into an organization
  # has failed.
  class TransformationFailed < RuntimeError; end

  # Thrown when an attempt to update the privacy of all teams is made while an
  # update is already in progress.
  class AlreadyUpdatingTeamPrivacy < StandardError
  end

  has_one :saml_provider,
    class_name: "Organization::SamlProvider",
    inverse_of: :target,
    dependent: :destroy

  has_one :team_sync_tenant,
    class_name: "TeamSync::Tenant",
    inverse_of: :organization,
    dependent: :destroy

  has_many :enterprise_installations, as: :owner, dependent: :delete_all

  has_many :discussion_posts, class_name: "OrganizationDiscussionPost"
  destroy_dependents_in_background :discussion_posts

  has_many :org_repositories,
    -> { where(active: true) },
    class_name: "Repository",
    foreign_key: :organization_id

  has_and_belongs_to_many :public_members,
    -> { distinct },
    class_name: "User",
    join_table: "public_org_members"

  has_many :migrations, foreign_key: :owner_id
  has_many :mannequins, -> { distinct }, through: :migrations, split: true
  has_many :attribution_invitations, foreign_key: :owner_id

  has_many :teams,
    -> { where(deleted: false).order(Arel.sql("CASE WHEN teams.name = 'Owners' THEN 1 ELSE 2 END")).order("name ASC") },
    foreign_key: :organization_id,
    dependent: :destroy

  # Association relationship for OauthApplicationApprovals.
  has_many :oauth_application_approvals,
    class_name: "OauthApplicationApproval",
    dependent: :delete_all

  has_many :projects, -> { where(owner_type: "Organization") },
    foreign_key: :owner_id
  destroy_dependents_in_background :projects

  has_many :memex_projects, -> { where(owner_type: "Organization") }, foreign_key: :owner_id
  destroy_dependents_in_background :memex_projects

  has_many :pending_team_membership_requests, through: :teams

  private :pending_team_membership_requests

  has_many :organization_domains, dependent: :destroy

  has_many :invitations, class_name: "OrganizationInvitation", dependent: :destroy
  has_many :invitation_opt_outs, class_name: "OrganizationInvitation::OptOut", dependent: :destroy

  private :invitations

  has_many :pending_invitations,
    -> { where(OrganizationInvitation.pending.where_values_hash) },
    class_name: "OrganizationInvitation"

  has_many :repository_invitations,
    through: :org_repositories,
    class_name: "RepositoryInvitation",
    source: :repository_invitations

  has_many :pending_collaborators, through: :repository_invitations, class_name: "User", source: :invitee, split: true

  has_many :hooks, as: :installation_target, dependent: :destroy

  has_one :business_membership, class_name: "Business::OrganizationMembership"
  has_one :business, through: :business_membership
  has_many :business_invitations,
    class_name: "BusinessOrganizationInvitation",
    foreign_key: :invitee_id,
    dependent: :destroy

  has_one :organization_profile, dependent: :destroy
  accepts_nested_attributes_for :organization_profile, update_only: true
  delegate :sponsors_update_email, to: :organization_profile, allow_nil: true

  has_many :marketplace_order_previews, class_name: "Marketplace::OrderPreview", dependent: :destroy, foreign_key: :account_id

  has_many :credential_authorizations, class_name: "Organization::CredentialAuthorization"

  has_many :roles, as: :owner, class_name: "::Role", dependent: :destroy

  has_many :ssh_certificate_authorities, as: :owner, dependent: :destroy

  has_many :ip_whitelist_entries, as: :owner, dependent: :destroy

  # Only organizations can have user_labels, so it's defined in this class
  # rather than being defined on `User`
  has_many :user_labels, foreign_key: :user_id
  destroy_dependents_in_background :user_labels

  # Internal: an abstract collection, for the sub-resources of an Organization available for abilities
  def resources
    Organization::Resources.new(self)
  end

  # Organizations whose OAuth application policy allows the given application
  # to access the organization's resources.
  def self.oauth_app_policy_met_by(app)
    raise ArgumentError, "OAuth app required" if app.nil?

    return scoped if app.organization_policy_exempt?

    join_sql = "LEFT OUTER JOIN oauth_application_approvals
                 ON oauth_application_approvals.organization_id = users.id
                 AND oauth_application_approvals.state = 1"

    conditions = "(users.restrict_oauth_applications IS NULL OR users.restrict_oauth_applications = 0)
                  OR (users.id = ?)
                  OR (oauth_application_approvals.application_id = ?)"

    joins(join_sql).where([conditions, app.user_id, app.id]).distinct
  end

  def change_owner_of!(project:, creator:, old_owner:)
    # Set up abilities as if this were a newly-created org project
    project.owner_dependent_added
    project.grant_write_to_owning_org_after_commit
    project.update_user_permission(creator, :admin) if member?(creator)
  end

  # Organizations whose OAuth application policy expressly denies the given
  # application access to the organization's resources.
  scope :oauth_app_policy_denies, lambda { |app|
    raise ArgumentError, "OAuth app required" if app.nil?

    # organization restricts OAuth applications
    # AND organization has denied the app

    joins = "INNER JOIN oauth_application_approvals
               ON oauth_application_approvals.organization_id = users.id
               AND oauth_application_approvals.state = 2"

    conditions = "(users.restrict_oauth_applications = 1)
                  AND (oauth_application_approvals.application_id = ?)"

    joins(joins).where(conditions, app.id)
  }

  scope :business_plus, -> {
    where(plan: GitHub::Plan::BUSINESS_PLUS)
  }

  before_validation :initialize_application_policy, on: :create
  before_validation :destroy_companies, if: :on_standard_terms_of_service?

  validates_presence_of :admins
  validates_presence_of :billing_email, if: :billing_email_required?
  validate :must_be_org_plan, :gravatar_email_must_be_an_email
  validates :billing_email, unicode3: true

  after_commit :add_initial_admins, on: :create

  after_update_commit :instrument_billing_email_change, if: :saved_change_to_organization_billing_email?

  after_update :enqueue_set_vertical, if: :saved_change_to_organization_billing_email?
  after_create :enqueue_set_vertical

  after_create :populate_initial_user_labels

  after_commit :update_repository_projects_setting, if: :repository_projects_setting_changed?
  after_commit :update_organization_projects_setting, if: :organization_projects_setting_changed?

  before_save :update_company

  before_destroy :check_trusted_oauth_apps_owner
  before_destroy :add_admins_to_intrumentation
  after_destroy :remove_business_user_accounts_for_members, unless: -> { GitHub.single_business_environment? }

  # Naturally.
  def organization?
    true
  end

  # Users are users, Orgs are orgs, Mannequins are mannequins
  def user?
    false
  end

  # Organizations on the legacy plan cannot use organization secrets.
  def can_use_org_secrets?
    Billing::ActionsPermission.new(self).status[:error][:reason] != "PLAN_INELIGIBLE" || GitHub.enterprise?
  end

  # Users are users, Orgs are orgs, Mannequins are mannequins
  def mannequin?
    false
  end

  # For routing and whatnot.
  def user
    self
  end

  def email_address_required?
    false
  end

  # Public: Determine if the billing email is required to create the Organization.
  #
  # Returns true if required, false otherwise.
  def billing_email_required?
    GitHub.billing_enabled?
  end

  def password_required?
    # Organizations don't have passwords - you can't log in as them.
    false
  end

  def avatar_editable_by?(user)
    adminable_by?(user)
  end

  # Public: An Organization can never be authenticated by a password.
  #
  # Returns false
  def authenticated_by_password?(password = nil)
    GitHub.dogstats.increment("user", tags: ["action:org_login_attempt"])
    false
  end

  # Public: can this Organization be orphaned (can last admin be removed)?
  #         We [currently] only allow Organizations to be orphaned via a SAML or SCIM operation,
  #         and only if the Organization belongs to an Enterprise account, where the Enterprise
  #         owners will have UI to reclaim ownership of any orpahned organizations.
  #
  # Return true if: - Organization is owned by a Business
  #                 - Business has the :enterprise_idp_provisioning feature flag turned on
  #                 - Business has SAML enabled
  def can_be_orphaned?
    return false if business.nil?
    return false unless GitHub.flipper[:enterprise_idp_provisioning].enabled?(business)
    # If SAML is enabled on the Enterprise, SCIM de-provisioning is automatically enabled
    # (SAML deprovisioning is a configurable option)
    business.saml_sso_enabled?
  end

  # Public: Is user the last remaining admin of this organization?
  def last_admin?(user)
    admins == [user]
  end

  # Public: If the given user is the last admin of the org, then raise an error
  # and log pertinent info
  #
  # Returns NoAdminsError or nil
  def prevent_removal_of_last_admin!(user, error_message)
    return unless last_admin?(user)

    GitHub::Logger.log(
      ns: "Organization#prevent_removal_of_last_admin!",
      msg: "prevent_removal_of_last_admin!",
      org: self,
      org_id: id,
      user: user,
      user_id: user.id,
    )
    raise NoAdminsError.new(error_message)
  end

  # Public: find a member of any of this organization's teams given their login.
  def find_team_member_by_login(login)
    all_team_members.find_by_login(login)
  end

  # Public: find a direct member or a member of any of this organization's teams given their login.
  def find_direct_or_team_member_by_login(login)
    members.find_by_login(login)
  end

  # Public: Add the specified user as an admin of this org.
  #
  # user        - The user to make an admin.
  # adder       - The user doing the adding.
  # invitation  - The optional OrganizationInvitation inviting the user
  #
  # Returns nothing.
  def add_admin(user, adder: nil, invitation: nil)
    add_member(user, action: :admin, adder: adder, invitation: invitation)
  end

  # Public: Add a member to this organization
  #
  # user        - the user to add as a member
  # action:     - action to update to. A symbol, :read, :write, or :admin. Defaults
  #               to :read
  # invitation  - The optional OrganizationInvitation inviting the user
  #
  # Returns nothing.
  def add_member(user, action: :read, adder: nil, perform_instrumentation: true, invitation: nil)
    return if direct_member?(user)
    return unless two_factor_requirement_met_by?(user)
    return unless external_identity_session_owner.saml_sso_requirement_met_by?(user)

    grant(user, action)

    if perform_instrumentation
      instrument_options = { user: user, permission: action }
      instrument_options[:actor] = adder if adder.present?
      instrument_options[:invitation_email] = invitation.email if invitation.try(:email?)
      if GitHub.context[:hide_staff_user] && GitHub.guard_audit_log_staff_actor?
        instrument_options.merge!(GitHub.guarded_audit_log_staff_actor_entry(adder))
        # GlobalInstrumenter wants a real user
        instrument_options[:actor] = User.staff_user
      end

      instrument(:add_member, instrument_options)

      options = instrument_options.merge(org: self, action: :add)
      GlobalInstrumenter.instrument "org.add_member", options
    end

    publicize_member(user) if GitHub.default_org_membership_visibility_public?
    add_user_to_business(user) if business.present?

    # If the organization is being created, the Organization::Creator will send the email after
    # billing has succeeded.
    OrganizationMailer.admin_added(user, self, adder).deliver_later if action == :admin && !being_created?

    if !being_created?
      IntegrationInstallation.where(target_id: self.id).each do |installation|
        UpdateIntegrationInstallationRateLimitJob.enqueue(installation.id)
      end
    end

    Contribution.clear_caches_for_user(user)
    user.synchronize_search_index

    # When a user is added to an org, check if we need to subscirbe them to the mailchimp team list
    MailchimpTeamListJob.perform_later(user: user, org: self) if GitHub.mailchimp_enabled?

    nil
  end

  def invitation_rate_limit_exceeded?
    # Don't rate limit org invites if they aren't enabled
    return false if GitHub.bypass_org_invites_enabled?

    limit_policy = OrganizationInvitation::RateLimitPolicy.new(self)
    limit_key = "orgs/invitations.new:org-#{id}"
    max_tries = limit_policy.limit
    RateLimiter.at_limit?(limit_key, max_tries: max_tries, ttl: limit_policy.ttl)
  end

  def invitation_rate_limit_error_message
    limit_policy = OrganizationInvitation::RateLimitPolicy.new(self)
    max_tries = limit_policy.limit
    "You have exceeded the organization invitation rate limit of #{max_tries} per 24 hours."
  end

  # Public: Update a member's permission on this organization.
  #
  # user    - the user whose membership is being modified
  # action: - action to update to. A symbol, :read, :write, or :admin.
  # updater: - this ends up as the actor if it's not nil
  #
  # Returns nothing.
  def update_member(user, action:, updater: nil)
    return unless direct_member?(user)

    unless action == :admin
      prevent_removal_of_last_admin!(user, "You can't demote the last admin")
    end

    previous_action = Authorization.service.most_capable_ability_between(actor: user, subject: self).action.to_sym

    if action != previous_action
      update_member_without_callbacks_and_notifications(user, action: action)

      instrument_options = { user: user, permission: action, old_permission: previous_action }
      instrument_options[:actor] = updater if updater.present?
      if GitHub.context[:hide_staff_user] && GitHub.guard_audit_log_staff_actor?
        instrument_options.merge!(GitHub.guarded_audit_log_staff_actor_entry(updater))
      end

      instrument(:update_member, instrument_options)

      if action == :admin
        CancelTeamMembershipRequestsJob.perform_later(id, user.id)
        OrganizationMailer.admin_added(user, self).deliver_later
      else
        # Clean up notifications and forks for repos that the user no longer has access to
        repository_ids = Array(repositories.pluck(:id))
        CleanupListNotificationsJob.perform_later(user.id, "Repository", repository_ids)
        RemoveForksForInaccessibleRepositoriesJob.perform_later(repository_ids, Array(user.id))
      end

      if (action == :admin || previous_action == :admin)
        # Enroll or unenroll team adming campaign when admin status changes
        MailchimpTeamListJob.perform_later(user: user, org: self) if GitHub.mailchimp_enabled?
      end
    end

    cancel_all_invitations_involving(user)

    nil
  end

  # Public: update a member directly skipping callbacks and other checks.
  def update_member_without_callbacks_and_notifications(user, action:)
    grant(user, action)
  end

  # Public: retrieve all failed invitations
  def failed_invitations
    invitations.failed
  end

  def active_failed_invitations
    failed_invitations.where(cancelled_at: nil)
  end

  # Public: Queues job to restore membership with user's previous settings.
  def restore_membership(restorable_organization_user, actor:)
    status = JobStatus.create(id: "restorable_#{restorable_organization_user.id}")
    RestoreOrganizationUserJob.enqueue(
      restorable_organization_user: restorable_organization_user,
      actor: actor,
    )
  end

  # Public: Asynchronous version of convert_to_outside_collaborator!
  #
  # see: Organization#convert_to_outside_collaborator!
  #
  # Returns nothing.
  def convert_to_outside_collaborator(member, save_settings: true)
    raise ArgumentError, "Can only be called on an org member" unless direct_or_team_member?(member)
    ConvertToOutsideCollaboratorJob.perform_later(id, member.id, { save_settings: save_settings })
  end

  # Public: Remove an org member from the org and all their teams, then use
  # direct abilities to replicate the permissions they once had from those
  # teams.
  #
  # member - org member to migrate.
  #
  # Returns nothing.
  def convert_to_outside_collaborator!(member, save_settings: true)
    raise ArgumentError, "Can only be called on an org member" unless direct_or_team_member?(member)

    if save_settings
      save_organization_settings_for_user(member)
    end

    convert_team_abilities_to_direct_abilities(member)

    remove_member(member,
      remove_direct_repo_access: false,
      send_notification:         false,
      save_settings:             false,
    )

    GitHub.dogstats.increment("organization", tags: ["action:convert_to_outside_collaborator"])
  end

  # Public: Asynchronous version of remove_outside_collaborator!
  #
  # see: Organization#remove_outside_collaborator!
  #
  # Returns nothing.
  def remove_outside_collaborator(user, save_settings: true, reason: nil, send_notification: true)
    RemoveOutsideCollaboratorFromOrganizationJob.perform_later(user.id, id, { save_settings: save_settings, reason: reason, send_notification: send_notification })
  end

  # Public: Remove an outside collaborator from the organization.
  #
  # Since outside collaborators are only associated with an organization by
  # being a member of some of its repositories, this method works by removing
  # the user as a collaborator from all of the organization's repositories.
  #
  # NOTE: It is STRONGLY recommended that you only call this from a background job.
  #
  # user - User to remove as an outside collaborator.
  #
  # save_settings - Boolean indicating whether a Restorable record should be saved
  #
  # reason - reason the collaborator was removed.
  #          (e.g. :two_factor_requirement_non_compliance)
  #
  # send_notification - Boolean indicating whether an email notification should
  #                     be delivered
  #
  # Returns nothing.
  def remove_outside_collaborator!(user, save_settings: true, reason: nil, send_notification: true)
    # we must find all the repos that a user might be collaboratoring on, so we cannot have a limit
    collab_repos = collaborating_repositories_for_user(user, limit: nil)

    return unless user_is_outside_collaborator?(user.id, collab_repos.pluck(:id))

    if save_settings
      save_organization_settings_for_user(user)
    end

    membership_types = role_of(user).types

    remove_direct_repo_access(user, collab_repos)
    remove_user_email_setting_for_org(user)
    if send_notification
      repo_names = collab_repos.map(&:name_with_owner)
      OrganizationMailer.remove_outside_collaborator(user, self, repo_names, reason.to_s).deliver_later
    end

    RemoveOrgMemberForksJob.enqueue(self, user)
    RemoveOrgMemberWatchedRepositoriesJob.enqueue(self, user)
    RemoveOrgMemberRepositoryStarsJob.enqueue(self, user)
    RemoveOrgMemberIssueAssignmentsJob.enqueue(self, user)
    Billing::SnapshotLicensesJob.perform_later(business) if delegate_billing_to_business?

    instrument :remove_outside_collaborator,
      user: user,
      reason: reason,
      membership_types: membership_types
  end

  # Public: remove a member from this organization.
  #
  # user - the User whose membership is being revoked.
  # remove_direct_repo_access - Whether or not the user should also be removed
  #                             from all the repos they have direct access to.
  # send_notification - Whether or not the user should be sent a notification
  #                     about being removed from the org.
  # reason - a Symbol
  # background_team_remove_member - a boolean indicating whether calls to team.remove_member should
  #                                 be queued for deletion in the background
  # allow_last_admin_removal - allow removing the last admin. Only applicable to organizations that
  #                            are owned by a Business and whose membership is managed by an
  #                            external Identity Provider
  #
  # Returns nothing.
  def remove_member(user,
                    remove_direct_repo_access: true,
                    send_notification: true,
                    save_settings: true,
                    reason: nil,
                    background_team_remove_member: false,
                    allow_last_admin_removal: false
  )
    return unless direct_or_team_member?(user) || pending_members.include?(user)

    unless allow_last_admin_removal && can_be_orphaned?
      prevent_removal_of_last_admin!(user, "You can't remove the last admin")
    end

    conceal_member user

    # Force cancel all invitations involving this user, including:
    # - Pending organization invitations involving this user
    # - Pending org-owned repository invitations involving this user
    cancel_all_invitations_involving(user, force: true)

    if save_settings
      save_organization_settings_for_user(user)
    end

    membership_types = role_of(user).types

    CancelTeamMembershipRequestsJob.perform_later(id, user.id)

    billing.remove_manager(user, actor: nil, reason: reason) if billing_manager?(user)
    remove_direct_repo_access(user) if remove_direct_repo_access
    remove_direct_project_access(user)
    remove_user_from_business(user) if business.present?

    # Revoking apps-management grants on organizations with many apps could lead
    # to request timeouts.
    RevokeOrgAppsManagementGrantsJob.perform_later(self, user)

    # Revoking abilities on organizations with many teams/repos can cause
    # request timeouts. Perform the work in the background:
    RevokeOrgMembershipAbilitiesJob.enqueue(self, user, background_team_remove_member)

    remove_user_email_setting_for_org(user)

    if saml_sso_enabled?
      ExternalIdentity.unlink_saml_identities(provider: saml_provider, user_ids: [user.id])
    end

    # Remove user from this org's Sponsors listing featured users
    if sponsors_listing.present?
      sponsors_listing_featured_item = sponsors_listing.featured_users.find_by(featureable_id: user.id)
      sponsors_listing_featured_item&.destroy
    end

    OrganizationMailer.removed_from_org(user, self, reason&.to_s).deliver_later if send_notification

    instrument_options = {
      user: user,
      reason: reason,
      membership_types: membership_types.map(&:to_s),
      actor: actor,
    }

    if GitHub.context[:hide_staff_user] && GitHub.guard_audit_log_staff_actor?
      instrument_options.merge!(GitHub.guarded_audit_log_staff_actor_entry(actor))
      # GlobalInstrumenter requires a real user
      instrument_options[:actor] = User.staff_user
    end

    instrument :remove_member, instrument_options
    options = instrument_options.merge(org: self, action: :remove)
    GlobalInstrumenter.instrument "org.remove_member", options
    Contribution.clear_caches_for_user(user)
    user.destroy_org_restricted_user_status(self)

    if admins.include?(user)
      # This is important! If the user is an admin, then we need to synchronously
      # make them not an admin, otherwise prevent_removal_of_last_admin! does not work correctly.
      # see https://github.com/github/teams_and_orgs/issues/64
      Ability.revoke(user, self, background: false)

      admins.reload

    end

    # Update user in mailchimp
    MailchimpTeamListJob.perform_later(user: user, org: self) if GitHub.mailchimp_enabled?

    nil
  end

  # Public: Remove a user from an organization, irrespective of how the user is
  #         affiliated with the organization
  #
  # user - the User whose links to the Organization are being severed
  # actor - the User triggering the removal.
  #
  # Returns nothing.
  def remove_any_affiliation(user, actor:)
    save_organization_settings_for_user(user)

    if GitHub.context[:from] == "two_factor_recovery_request#confirm_continue"
      reason = Organization::RemovedMemberNotification::TWO_FACTOR_ACCOUNT_RECOVERY
    else
      reason = nil
    end

    remove_member(user, save_settings: false, reason: reason)
    billing.remove_manager(user, actor: actor)
    remove_outside_collaborator(user, save_settings: false)
  end

  def save_organization_settings_for_user(user)
    restorable = Restorable::OrganizationUser.start(self, user)
    # Start with memberships before they are removed.
    memberships = user_direct_abilities_for_organization_teams_and_repositories(user)
    restorable.save_memberships(memberships)
    restorable.save_memberships_complete
  end

  # Public: remove a member directly, skipping callbacks and other checks.
  #
  # user       - The user to remove from this organization
  # background - Optional. Delete dependent abilities in a background job unless false.
  def remove_member_without_callbacks_and_notifications(user, background: true)
    revoke(user, background: background)
  end

  def remove_member_with_instrumentation(user, background: true, actor: nil, reason: nil)
    remove_member_without_callbacks_and_notifications(user, background: background)

    membership_types = role_of(user).types
    instrument_options = {
      user: user,
      reason: reason,
      membership_types: membership_types.map(&:to_s),
      actor: actor,
      spammy: user.spammy?,
    }
    instrument :remove_member, instrument_options
  end

  def remove_user_email_setting_for_org(user)
    restorable = Restorable::OrganizationUser.continue(self, user)
    if !direct_or_team_member?(user)
      GitHub.newsies.get_and_update_settings(user) do |settings|
        default_email = settings.default_email.address
        email = settings.email(self).address
        if email != default_email
          restorable.save_custom_email_routings(email)
        end
        settings.email(self, nil)
      end
    end
    restorable.save_custom_email_routings_complete
  end

  # Public: Remove a user's direct access to all of this organization's
  # repositories.
  #
  # user - User to remove direct access from.
  # repositories (optional) - a list of repositories from which to remove USER's
  #                           access
  # Returns nothing.
  def remove_direct_repo_access(user, repositories = nil)
    # we must find all the repos that a user might have access to, so we cannot have a limit
    # this will likely timeout for orgs with lots of repos.
    repositories ||= collaborating_repositories_for_user(user, limit: nil)

    repositories.each do |repo|
      repo.disassociate_member(user, repo.owner)
    end
  end

  # Public: Remove a user's direct access to all of this organization's
  # projects.
  #
  # user - User to remove direct access from.
  #
  # Returns nothing.
  def remove_direct_project_access(user)
    project_abilities = Ability.where(
      actor_type: "User",
      actor_id: user.id,
      subject_type: "Project",
      subject_id: projects.pluck(:id),
      priority: Ability.priorities[:direct],
    )

    Project.where(id: project_abilities.map(&:subject_id)).each do |project|
      project.update_user_permission(user, nil)
    end
  end

  # Public: All of this organization's repositories to which User USER has
  #         direct access.
  #
  # limit - limit the number of repos queried
  #
  # Returns an Array of Repositories.
  def collaborating_repositories_for_user(user, limit: MEGA_ORG_REPOS_THRESHOLD)
    id_and_repos = collaborating_repositories_for(user.id, limit: limit) || []
    id_and_repos.flat_map(&:last)
  end

  # Public: Scope of Users associated with this org, limited by user's ability
  # to see their association with the org.
  #
  # viewer - The User viewing the org.
  # type   - What types of users do we want to see?
  #          :all                  - Show all visible org users. Does not
  #                                  include direct org members if direct org
  #                                  membership isn't enabled.
  #          :admin                - Only show users who are admins of the org.
  #          :direct_member        - Only show users who are direct members of
  #                                  the org.
  #          :member_without_admin - Only show users who are direct members of
  #                                  the org but NOT admins.
  # actor_ids - A list of user ids to filter the possible visible users on.
  #
  # Returns an ActiveRecord::Relation
  def visible_users_for(viewer, type: :all, actor_ids: nil, limit: MEGA_ORG_MEMBER_THRESHOLD)
    scope = case type
    when :all, :direct_member
      members(actor_ids: actor_ids, limit: limit)
    when :admin
      admins(actor_ids: actor_ids, limit: limit)
    when :member_without_admin
      members(action: :read, actor_ids: actor_ids, limit: limit)
    else
      raise ArgumentError, "Organization#visible_users_for type must be valid"
    end

    if limit_to_public_members?(viewer)
      # Anonymous users, non-org-members, and users using oauth apps blocked by
      # the org can only see public members of the org.
      scope = scope.publicly_belongs_to(id)
    end

    scope
  end


  def visible_user_ids_for(viewer, type: :all, actor_ids: nil, limit: MEGA_ORG_MEMBER_THRESHOLD)
    user_ids = case type
    when :all, :direct_member
      member_ids(actor_ids: actor_ids, limit: limit)
    when :admin
      admin_ids(actor_ids: actor_ids, limit: limit)
    when :member_without_admin
      member_ids(action: :read, actor_ids: actor_ids, limit: limit)
    else
      raise ArgumentError, "Organization#visible_user_ids_for type must be valid"
    end

    return user_ids if user_ids.empty?

    if limit_to_public_members?(viewer)
      # Anonymous users, non-org-members, and users using oauth apps blocked by
      # the org can only see public members of the org.

      user_ids = Organization.github_sql.values <<-SQL, user_id: user_ids, org_id: self.id
        SELECT user_id FROM public_org_members
         WHERE organization_id = :org_id
           AND user_id IN :user_id
      SQL
    end

    user_ids
  end

  # Public: Should a viewer only see public members?
  #
  # Returns a Boolean
  def limit_to_public_members?(viewer)
    return true if viewer.nil? || viewer.new_record? || human_non_member?(viewer) || bot_without_access_to_members?(viewer) || mannequin_without_access_to_members?(viewer)
    return true if viewer.governed_by_oauth_application_policy? && !allows_oauth_application?(viewer.oauth_application)

    # To avoid leaking private information(org member information) for scopeless
    # tokens we want to make sure the viewer has read:org or repo scope.
    # Context: https://github.com/github/github/issues/93190
    # Repo scope context: https://github.com/github/github/pull/93604#issuecomment-410885909
    has_permission = Api::AccessControl.scope?(viewer, "read:org") || Api::AccessControl.scope?(viewer, "repo")
    return true if !has_permission

    # Viewer can see private members
    false
  end

  # Public: Scope of *all* Users associated with this org.
  #
  # Be careful: If you are presenting a list of org users to someone, don't use
  # this method. Use #visible_users_for instead.
  #
  # Returns an ActiveRecord scope.
  def people
    User.where(id: people_ids)
  end

  # Public: List of *all* user ids associated with this org.
  #
  # Returns an Array of user ids.
  def people_ids
    member_ids
  end

  # Public: get id's for all members and billing managers of this org
  #
  # Returns an Array of id's, de-duplicated
  def member_and_billing_manager_ids
    Set.new(member_ids + billing_manager_ids).to_a
  end

  # Returns members of the Organization, users who could authenticate via the SAML SSO IdP if configured
  #
  # Returns ActiveRecord::Relation of Users
  def saml_members
    members
  end

  # Public:
  def direct_or_team_member?(user)
    member?(user)
  end

  # Organizations can't forget their password - they have none!
  #
  # Returns nothing.
  def forgot_password(address = nil)
    nil
  end

  # This is a User callback we want to skip.
  def set_initial_primary_email
    true
  end

  # Orgs don't have UserEmails or primary emails.
  def email=(email)
    warn "Called Organization#email= which is a noop" if !Rails.env.test?
  end

  # Orgs have no verified emails.
  def should_verify_email?
    false
  end
  alias_method :no_verified_emails?, :should_verify_email?

  # Orgs have no verified emails.
  def enable_mandatory_email_verification
    # noop
  end

  # Public: Orgs don't have UserEmails, so the outbound_email defaults to their profile_email
  # This overrides behavior on User.
  # NOTE: This is not a required attribute for Organizations!
  #
  # Returns a String or nil
  def outbound_email
    profile_email
  end

  # Public: Returns an Array of String email addresses for support to use when
  # contacting this org via Halp about non-billing matters
  def email_for_halp
    ids = admin_contacts_for_halp.pluck(:id)
    UserEmail.where(user_id: ids).includes(:email_roles).where(email_roles: {role: "primary"}).map &:email
  end

  def admin_contacts_for_halp
    admins.reject { |u| u.spammy? || u.suspended? }
  end

  # Public: Returns an Array of String email addresses for support to use when
  # contacting this org via Halp about billing matters
  def billing_email_for_halp
    ids = billing_contacts_for_halp.pluck(:id)
    emails = UserEmail.where(user_id: ids).includes(:email_roles).where(email_roles: {role: "primary"}).map &:email
    emails << billing_email
    emails.uniq
  end

  private def billing_contacts_for_halp
    users = admin_contacts_for_halp
    users += billing_managers.reject { |u| u.spammy? || u.suspended? }
    users.uniq
  end

  # Clobbered by serialize_attributes. Need to make sure we call
  # User's `gravatar_id` because it has special behavior.
  def gravatar_id
    super
  end

  # Ensures gravatar email at least has an '@' sign in it. C'mon.
  #
  # Should be run as a validation callback.
  #
  # Returns nothing.
  def gravatar_email_must_be_an_email
    if gravatar_email.present? && !gravatar_email.to_s.match(EMAIL_REGEX)
      errors.add("gravatar_email", "isn't a valid email address.")
    end
  end

  # The org's repositories used when searching using `search_repos`,
  # typically in conjunction with an autocompleter.
  #
  # Returns an Array of Repository objects
  def searchable_repos
    org_repositories
  end

  def internal_repositories
    return Repository.none unless business
    repositories.joins(:internal_repository).where("internal_repositories.business_id = ?", business.id)
  end

  def remove_internal_repositories
    repo_ids = repositories.pluck(:id)
    InternalRepository.with_ids(repo_ids, field: :repository_id).delete_all
  end

  def supports_internal_repositories?
    !!business&.supports_internal_repositories?
  end

  # Find repos that belong to the organization by either a match on the repo
  # name, or on owner name + repo name.
  #
  # Params:
  #
  #      term - The string to search for
  #     limit - Optional - Max # of results
  #
  # Returns an Array of Repository objects.
  def search_repos(term, limit = 30)
    limit = limit.to_i

    query = /^#{Regexp.quote(term.to_s)}/i
    searchable_repos.
      select { |repo| ("#{repo.owner.name}/#{repo.name}" =~ query) || (repo.name =~ query) }.
      uniq.
      sort_by { |repo| repo.name.downcase }.
      first(limit)
  end

  # Public: is the organization permitted to own a fork of the given repo?
  #
  # Returns true if the fork will not become an OOPF or if the disallow_oopfs is not enabled
  def fork_allowed?(repo:, user:)
    return true unless GitHub.flipper[:disallow_oopfs].enabled?(user)
    return true if repo.public?
    return false if repo.owner.business.nil?
    business == repo.owner.business
  end

  # Deleting orgs in the background is a good idea because of all the
  # associated objects - such as repos - which could cause the process
  # to be slow. As such, we wanted to set the `deleted` flag before
  # queueing up a job to hide the org from parts of the web UI so it
  # feels like it was actually deleted.
  #
  # Returns nothing.
  def async_destroy(actor = nil)
    return unless permit_deletion?(actor)
    add_admins_to_intrumentation

    super(actor)

    GitHub.dogstats.increment("organization", tags: ["action:destroy"])
  end

  # Site admins can delete all the things
  # Spammy orgs can delete themselves only if stafftools override is set
  # GHE appliances with disallow_members_can_delete_repositories enabled
  #   can only be deleted by site admins or if it doesn't contain repos
  #
  # An organization with a legal hold cannot be deleted regardless of the above.
  def permit_deletion?(actor = nil)
    return false if self.legal_hold?
    return true if actor&.site_admin?

    return false if has_any_trade_restrictions?
    return false unless repo_deletion_allowed?
    return false unless super # run checks in user#permit_deletion?
    true
  end

  def repo_deletion_allowed?(actor = nil)
    return true if actor&.site_admin?
    return true if repositories.count == 0
    return true if !GitHub.single_business_environment? || GitHub.global_business.members_can_delete_repositories?
    false
  end

  def add_admins_to_intrumentation
    context = {admin_ids: admin_ids, admins: admins.map(&:login)}
    GitHub.context.push(context)
    Audit.context.push(context)
  end

  # Orgs don't want emails.
  #
  # Returns a Boolean.
  def notifications?
    false
  end

  # Public: Finds all Teams in this Organization a given User belongs to.
  #
  # user   - The User in question.
  # viewer - (Optional) The User viewing the teams. If left nil, we assume all
  #          teams are visible to the viewer.
  #
  # Returns a Team scope.
  def teams_for(user, viewer: nil)
    scope = user.teams.owned_by(self)
    scope = scope.where(id: visible_teams_for(viewer)) if viewer.present?

    scope
  end

  # Public: Cancel any pending requests from a user to join any teams
  # in this organization
  #
  # user - User to cancel pending join requests for
  #
  # Returns nothing.
  def cancel_team_membership_requests_for(user)
    requests = pending_team_membership_requests.where(requester_id: user.id)

    requests.each { |r| r.cancel(actor: actor) }
  end

  # Public: Retrieve a Team matching slug.
  #
  # slug       - The team's slug String
  # visible_to - Require the team to be visible to this User
  #
  # Returns a Team or nil.
  def find_team_by_slug(slug, visible_to: nil)
    return unless team = teams.find_by_slug(slug)
    team if visible_to.nil? || team.visible_to?(visible_to)
  end

  # Public: Finds all this organization's discussions that are visible to a user.
  def visible_discussions_for(user, scope: nil)
    repo_ids = visible_repositories_for(user).owned_by(self).pluck(:id)
    discussions = Discussion.for_repository(repo_ids).filter_spam_for(user).includes(:repository)
    discussions = discussions.merge(scope) if scope
    discussions.select do |discussion|
      discussion.repository.discussions_enabled?
    end
  end

  # Public: Finds all this organization's teams that are visible to a user.
  #
  # * Org owners can see all of the org's teams, regardless of privacy level.
  # * A Bot whose installation has `read` permission on the org's `members`, can see all
  #   of the org's teams, regardless of privacy level.
  # * Org members can see all of the org's closed teams, as well as all of the
  #   org's secret teams that they're on.
  # * Outside collaborators and other users cannot see any of the org's teams.
  #
  # NOTE: This logic is duplicated in Team#visible_to?(user) to avoid loading
  # all team IDs to check a single team's visibility. Please change both spots.
  #
  # user - The User in question.
  # fields - an optional array of fields to select from the teams table
  #
  # Returns a Team scope.
  def visible_teams_for(user, fields: nil)
    if all_teams_visible_for?(user)
      query = teams.includes(:organization)
      query = query.select(fields) if fields
      query
    elsif member?(user)
      member_team_ids = teams_for(user).pluck(:id)
      closed_team_ids = teams.with_minimum_privacy(:closed).pluck(:id)
      query = Team.includes(:organization).where(id: member_team_ids | closed_team_ids, deleted: false)
      query = query.select(fields) if fields
      query
    else
      Team.none
    end
  end

  # Internal: are all teams in this Organization visible to this user?
  #
  # * Org owners can see all of the org's teams, regardless of privacy level.
  # * A Bot of installation with permission on members, can see all
  #   of the org's teams
  def all_teams_visible_for?(user)
    installation_with_access = user.respond_to?(:installation) && resources.members.readable_by?(user)

    adminable_by?(user) || installation_with_access
  end

  # Creates a Team.
  #
  # name        - A name for the team
  # creator     - User who is creating this team
  # repos       - Array of Repositories to add as members
  # ldap_dn     - String for LDAP distinguished name
  # maintainers - Array of Users to add as team maintainers
  # group_mappings - Array of external group Hashes to map the Team to.
  # attrs       - Attributes to be set on creation
  #
  # Accepts an optional block for additional attributes
  #
  # Returns a Team, whether created or not.
  def create_team(creator:, repos: nil, ldap_dn: nil, maintainers: [], group_mappings: [], attrs: {})
    attributes = attrs.merge({ldap_dn: ldap_dn})
    team = Team::Creator.create_team(
      creator,
      self,
      attributes,
      maintainers: maintainers,
      group_mappings: group_mappings,
    )

    return team unless team.errors.empty?

    if repos
      authorize_repos_for_team(repos.compact, creator).each do |repo|
        team.add_repository(repo, team.permission) if repo.organization_id == id
      end
    end

    team
  end

  # Internal: given a list of repos and a user trying to create a team for this
  # org, ensure that the creator can admin each repo. For any that the creator
  # is not admin, silently drop the repo from the list so that we do not expose
  # information about whether a given repo exists or not.
  #
  # Returns an Array<Repository>
  private def authorize_repos_for_team(repos, creator)
    organization_owned_repos = repos.filter do |repo|
      repo.organization_id == id
    end

    subject_ids = ::Authorization
      .service
      .most_capable_abilities_between_multiple_actors_and_subjects(
        actor_type: User,
        actor_ids: [creator.id],
        subject_type: Repository,
        subjects: organization_owned_repos,
    )
      .filter { |ability| ability.action == "admin" }
      .map(&:subject_id)

    repos.filter { |repo| subject_ids.include?(repo.id) }
  end

  # Public: Updates the specified teams in this organization with the given
  # privacy level. Returns an array of ids representing those teams that were
  # updated.
  def bulk_update_privacy(team_ids, privacy)
    updates = []

    return updates unless Team.valid_privacy?(privacy)

    teams.where(id: team_ids).find_each do |team|
      next if team.cant_change_visibility?

      team.privacy = privacy

      if team.save
        updates << team.id
      end
    end

    updates
  end

  # Checks if the given user should use transfer requests when moving a
  # repository to this organization
  #
  # Returns a boolean
  def requires_transfer_requests_from?(user, repository_visibility = nil)
    !can_create_repository?(user, visibility: repository_visibility)
  end

  # Is the default visibility for repositories owned by orgs private?
  #
  # On Enterprise Server, honor the default repository visibility setting
  # for the installation. On GitHub .com enable private as the default for orgs
  # that can add private repositories because they are paid accounts.
  #
  # Returns a Boolean.
  def private_repo_by_default?
    return super if GitHub.enterprise?
    can_add_private_repo?
  end

  def default_repo_visibility
    return GitHub.default_repo_visibility if GitHub.enterprise?
    return "internal" if supports_internal_repositories?
    super
  end

  # Can a given user create a repository for this organization?
  #
  # user: The user whose ability to create a  repo is being checked.
  # role: :admin if the user should be treated as an admin
  # visibility: The type of repo to create: "public", "private", or "internal".
  #             A nil value will return true if _any_ repo visiblibilities are
  #             allowed to be created.
  def can_create_repository?(user, role: nil, visibility: nil)
    async_can_create_repository?(user, role: role, visibility: visibility).sync
  end

  # Public: Returns a promise that resolves to a Boolean and represents whether the given user
  # can create a repository in this organization.
  #
  # user: The user who's ability to create a  repo is being checked.
  # role: :admin if the user should be treated as an admin
  # visibility: The type of repo to create: "public", "private", or "internal".
  #             A nil value will return true if _any_ repo visiblibilities are
  #             allowed to be created.
  def async_can_create_repository?(user, role: nil, visibility: nil)
    return Promise.resolve(false) unless visibility.nil? || Repository::VISIBILITIES.include?(visibility)
    return Promise.resolve(false) unless user && (user.user? || user.is_a?(Bot))
    return Promise.resolve(true) if role == :admin

    async_adminable_by?(user).then do |adminable|
      next true if adminable

      # cast public to boolean because it may be the string "false"
      public = public.to_s == "true"

      if user.respond_to?(:installation)
        installation = user.installation

        next false if installation.target_id != self.id

        installation.permissions["administration"] == :write
      else
        teams = teams_for(user)
        Promise.all(teams.map(&:async_organization)).then do
          # Keep support for allowing members of legacy admin teams to add repos.
          result = if teams.any?(&:admin?)
            true
          elsif role == :direct_member || member?(user)
            case visibility
            when nil
              members_can_create_repositories?
            when "public"
              members_can_create_public_repositories?
            when "private"
              members_can_create_private_repositories?
            when "internal"
              members_can_create_internal_repositories?
            end
          else
            false
          end
          Promise.resolve(result)
        end
      end
    end
  end

  def can_own_repositories?
    true
  end

  def update_team_privacy(privacy)
    raise ArgumentError unless Team.valid_privacy?(privacy)
    raise AlreadyUpdatingTeamPrivacy if updating_team_privacy?

    self.updated_team_privacy_at = Time.now
    save

    UpdateOrganizationTeamPrivacyJob.perform_later(id, privacy)
  end

  def update_team_privacy!(privacy)
    teams.each do |team|
      team.throttle do
        team.privacy = privacy
        team.save
      end
    end

    self.updated_team_privacy_at = nil
    save
  end

  def updating_team_privacy?
    updated_team_privacy_at && updated_team_privacy_at > 10.minutes.ago
  end

  def can_create_team?(user)
    # This check should be for integrators only
    return true if resources.members.writable_by?(user) && user.can_have_granular_permissions?

    # Eventually this should probably be checked in Authzd
    if members_can_create_teams?
      return member?(user) || adminable_by?(user)
    else
      return adminable_by?(user)
    end
  end

  def can_publicize_memberships?(current_user, members: self.members)
    return false if GitHub.private_org_membership_visibility_enforced?
    return false unless current_user && current_user.user?

    members == [current_user] && member_publicizable?(current_user)
  end

  def can_conceal_memberships?(current_user, members: self.members)
    return false if GitHub.public_org_membership_visibility_enforced?
    return false unless current_user && current_user.user?
    return true if adminable_by?(current_user)

    members == [current_user] && member?(current_user)
  end

  # create the trusted oauth org
  def self.create_trusted_oauth_apps_owner
    return unless GitHub.enterprise? # restrict to enterprise for now
    Organization.where(login: GitHub.trusted_oauth_apps_org_name).first_or_create(
      login: GitHub.trusted_oauth_apps_org_name,
      plan: GitHub::Plan.find("free").name,
      billing_email: "ghost-org@github.com",
      admins: [User.ghost],
    )
  end

  # Every organization needs a team of users who can administrate the
  # organization. These are generally called Owners.
  def add_initial_admins
    return unless @admins.present?
    @admins.each { |user| add_member user, action: :admin }
  end

  # Shortcut for setting just a single admin.
  def admin=(admin)
    self.admins = [admin]
  end

  # Shortcut for grabbing the admin, if there's only one.
  # Please don't use this method in app code - it's mainly to appease
  # Machinist.
  def admin
    admins.size == 1 ? admins[0] : raise("More than one admin!")
  end

  # Given an array of admin login strings, sets the admins to be the users
  # identified by those logins.
  #
  # e.g.
  #   org.admin_logins = %w( pjhyett defunkt mojombo )
  def admin_logins=(logins)
    self.admins = User.with_logins(*logins)
  end

  # The admins as user objects. This is used during organization creation by the
  # `add_initial_admins` method.
  def admins=(admins)
    @admins = admins
  end

  def admins(actor_ids: nil, limit: nil)
    # The @admins ivar is for pre-create validation that checks that admins are
    # present, even if they haven't been granted yet by `add_initial_admins`.
    # Don't use this ivar for anything but creating new organizations.
    return @admins if @admins
    direct_admins(actor_ids: actor_ids, limit: limit)
  end

  def admin_ids(actor_ids: nil, limit: nil)
    direct_admin_ids(actor_ids: actor_ids, limit: limit)
  end

  # The team of administrators for this organization. They have all the power.
  def owners_team
    if !defined? @owners_team
      @owners_team = teams.where(name: "Owners").first
    end

    @owners_team
  end

  # The former team of administrators for this organization. They once had all
  # the power, but as of direct org membership, they are a mere relic of their
  # former glory. Always use this instead of `owners_team` or manually checking
  # the team's name/slug.
  #
  # Returns a Team or nil.
  def legacy_owners_team
    teams.where(name: "Owners", permission: "admin").first
  end

  # Can the given user leave an Organization? Only members and non-surviving
  # admins can leave. A "non-surviving admin" is basically the last person
  # standing: an org must have at least one admin. If the last admin wants to
  # leave they can just delete the damn org.
  #
  # Returns a Boolean
  def can_leave?(user)
    leave_status_for(user) == true
  end

  def leave_status_for(user)
    reload

    if last_admin?(user)
      :last
    else
      direct_or_team_member?(user)
    end
  end

  # Maintenance command to verify consistency of the public_members association
  # used to determine whether a user's org membership is concealed or not.
  # Sometimes this member list gets out of sync with the system-of-record team
  # tables. If things *do* get out of sync, it should be considered a bug:
  # notify @github/abilities for further research.
  #
  # NOTE This method is really unoptimized and should not be used for any
  # general action on the site. Staff use only.
  #
  # TODO reexamine this after orgs-next, as there won't be anything to get out
  # of sync.
  def reset_public_members!
    users = public_members.select { |u| !direct_or_team_member?(u) }
    ActiveRecord::Base.connected_to(role: :writing) do
      users.each { |user| conceal_member(user) }
    end
  end

  # Cleanup internal state on AR reload
  def reset_memoized_attributes
    remove_instance_variable :@admins if defined? @admins
    remove_instance_variable :@owners_team if defined? @owners_team
    super
  end

  def claimable?
    repositories.blank? && admins.size == 1 && admins.first.claimable?
  end

  # Extremely destructive and awesome method: transforms a normal
  # user account into an organization.
  #
  # Teams are created and users placed in them according to the
  # current collaborators. All teams initially are `pull` as that's
  # the implicit permission level of collaborators on normal
  # repositories as of writing.
  #
  # user          - The User object that is being transformed into an Organization.
  # owner         - The User object that will become the owner of the new Organization.
  # new_org_attrs - A hash of attributes to set on the new Organization.
  #
  # Returns nothing.
  # Raises TransformationFailed if there was validation error before the job
  # could be scheduled.
  def self.transform(user, owner, new_org_attrs = {})
    if user.email.nil?
      raise TransformationFailed.new("Transform requires a primary email on the user account")
    end

    if owner.nil?
      raise TransformationFailed.new("Transform requires an owner.")
    end

    if owner == user
      raise TransformationFailed.new("This user will become an organization and can't be an owner.")
    end

    unless user.is_a?(User) && user.user?
      raise TransformationFailed.new("Transform requires the user be a user.")
    end

    unless owner.is_a?(User) && owner.user?
      raise TransformationFailed.new("Transform requires the owner be a user.")
    end

    if owned_org = user.organizations.detect { |org| org.last_admin?(user) }
      raise TransformationFailed.new("Cannot transform user because user is the last owner of #{owned_org.login}")
    end

    if start_transform(user)
      TransformUserIntoOrgJob.perform_later(user.id, owner.id, new_org_attrs)
    end
  end

  # Synchronous version of `Organization.transform`
  def self.transform!(user, owner, new_org_attrs = {})
    if user.organization?
      end_transform(user)
      return
    end

    transaction do
      # Remove user as a collaborator from anything they're a member of
      # Note: This must be done *before* the User becomes an Organization, or the
      # `remove_member` ability revocation won't work.
      user.teams.each { |team| team.remove_member user }
      user.member_repositories.each { |repo| repo.remove_member user }
      RepositoryInvitation.where(invitee_id: user.id).find_each { |invitation| invitation.destroy }
      OrganizationInvitation.where(invitee_id: user.id, accepted_at: nil).find_each { |invitation| invitation.cancel(actor: user) }
      user.clear_issue_assignments
      user.reactions.destroy_all

      # Revoke all OAuth tokens
      user.oauth_accesses.destroy_all

      # Destroy all dashboard notices
      user.delete_notices

      # It's no longer possible to block this user since it's an org now
      user.ignored_by_users.destroy_all

      # All account succession agreements for that user are now nullified, now that they're an org.
      SuccessorInvitation.terminate_all(user)

      # The metamorphosis.
      previous_plan = user.plan.to_s
      user.type = "Organization"
      user.save!
      user.update!(new_org_attrs)

      if GitHub.billing_enabled?
        # Ensure we have enough seats to cover all collaborators
        user.seats = user.default_seats
        user.save!
      end

      # Fresh version of our new Organization.
      org = find(user.id)
      org.instrument :transform, owner: owner.to_s, tos_sha: TosAcceptance.current_sha

      # Setting the Organization#creator skips sending OrganizationMailer#admin_added emails
      # till the transformation process is commited to the database
      org.creator = owner

      # Set up the owners team / admins
      org.admins = [owner]
      org.add_initial_admins

      # Ensure all our repos and forks know they're part of an organization.
      org.associate_repositories

      # Ensure all projects are updated to the correct owner type post-transfer.
      # This has to be done with User because owner is a polymorphic association
      # and we can't simply call `org.projects` until after this is complete
      org.associate_user_projects(user, owner)

      # Add collaborating team members to outside collaborators
      user.repositories.each do |repository|
        repository.teams.each do |team|
          team.members.each do |member|
            # Guard against trying to add the original User, which has now become an
            # Organization, which we have to test against `id`.
            next if member.id == user.id
            org.repositories.find(repository.id).add_member(member, action: Authorization.service.most_capable_ability_between(actor: member, subject: repository).action)
          end
        end
      end

      # Set up the default third-party application policy used for new orgs.
      org.initialize_application_policy

      # Set up the default attributes.
      org.sync_default_repository_permission!(actor: org)

      # Clean up some of the user's data that is no longer necessary.
      org.clear_transformed_user_data

      org.track_plan_change(org, GitHub::Plan.find!(previous_plan))

      # Disable if over plan limits
      org.enable_or_disable!

      # So everyone knows we've been transformed.
      end_transform(user)
      org.save
    end

    org = find(user.id)
    # NB: Need to force the BT update since the update catches
    # to see if there are changes on the relevant fields.
    org.update_external_subscription!(force: true)

    # ensure org has initial set of default labels
    org.populate_initial_user_labels

    # Notify the org owner that they have been added as an admin
    org.admins.each do |admin|
      OrganizationMailer.admin_added(admin, org, nil).deliver_later
    end

    # Tada.
    org
  ensure
    end_transform(user)
  end

  # Sets organization_id on all repos owned by the organization and
  # private forks owned by members of the org.
  def associate_repositories
    repositories.each do |repo|
      # Preserve the (outside) collaborators.
      repo.update_organization remove_collaborators: false

      # Forks of private repos get associated with the org.
      next unless repo.private?

      repo.forks.each do |frk|
        frk.update_organization remove_collaborators: false
      end
    end
  end

  # Changes the owner type and sets up the project permissions
  # This will also re-sequence, so the project number may change if they have
  # deleted projects
  def associate_user_projects(user, owner)
    projects = user.projects.sort_by(&:number)
    projects.each do |project|
      project.transform_owner_type!(owner: self, new_creator: owner)
    end
  end

  # Returns a list of potential repositories grouped by collaborators. The
  # values are arrays of repositories which should belong to unique teams.
  #
  # That is, if two repositories have the same collaborators they
  # will be one team while another repository with different
  # collaborators will be part of another.
  #
  # Repositories with no collaborators are excluded.
  def repositories_grouped_by_collaborators
    teams = {}

    repos_and_forks = repositories.map do |repo|
      [repo, repo.forks.private]
    end

    repos_and_forks.flatten.each do |repository|
      collabs = repository.all_members.select(&:user?)
      collabs_hash = collabs.map(&:id).sort_by(&:to_s).join("-")

      next if collabs_hash.empty?

      teams[collabs_hash] ||= []
      teams[collabs_hash] << repository
    end

    teams.values
  end

  # Removes user data which is no longer necessary now that the user
  # has been transformed into an organization, such as public keys and
  # passwords.
  def clear_transformed_user_data
    self.password = "n0n3:#{Time.now.to_i}"
    self.gravatar_email = gravatar_email || email
    self.billing_email  = billing_email || email
    self.gh_role = nil
    save!

    sessions.clear
    public_keys.clear

    users_the_org_unfollowed = following.to_a.dup
    users_who_unfollowed_the_org = followers.to_a.dup

    following.clear
    followers.clear
    emails.clear
    email_roles.clear

    # Update respected follower/ing counts on users
    users_the_org_unfollowed.each { |u| u.followers_count! }
    users_who_unfollowed_the_org.each { |u| u.following_count! }

    # Unstar all repos and Gists.
    (starred_repositories + starred_gists).each do |starrable|
      unstar(starrable)
    end

    # Orgs don't have review requests
    clear_review_requests

    # Orgs don't have notifications.
    GitHub.newsies.async_delete_all_for_user(id)

    # Orgs can't login, so they can't see the interaction warning
    interaction_setting.destroy if interaction_setting

    # If they have a profile we want to remove everything except the
    # white listed profile fields. This way an organization does not
    # remain hireable, for instance.
    #
    # Also sets profile display staff badge to false, since orgs cannot
    # have staff badges
    if profile
      whitelist = %w( name blog location )
      blacklist = Profile.column_names - whitelist

      blacklist.each do |field|
        method = "profile_#{field}="
        send(method, nil) if respond_to?(method)
      end

      profile.display_staff_badge = false
      profile.readme_opt_in = false

      profile.save
    end
  end

  # The Redis key used for keeping track of a user => org transform.
  def self.transform_key(user)
    "org:transforming:#{user.id}"
  end

  # Begin transforming the passed user into an organization.
  def self.start_transform(user)
    GitHub.kv.setnx(transform_key(user), TRANSFORM_FLAG, expires: TRANSFORM_FLAG_EXPIRY.from_now)
  rescue GitHub::KV::UnavailableError
    false # if KV is unavailable, we can't set the flag. Don't continue.
  end

  # Are we still transforming?
  def self.transforming?(user)
    GitHub.kv.get(transform_key(user)).value { false }
  end

  # Complete transforming the passed user into an organization.
  #
  # Returns nothing.
  def self.end_transform(user)
    GitHub.kv.del(transform_key(user))
  rescue GitHub::KV::UnavailableError
    # Noop, ignore. Rely on expiration to clean up the flag.
  end

  # Find organizations with logins matching a specific term.
  #
  # Params:
  #
  #   term  - The string to search for.
  #   limit - Maximum number of results. Defaults to 30.
  #
  # Returns an Array of Organizations.
  def self.search(term, limit: 30)
    orgs = user_query("#{term} type:org", friends: [], limit: limit)
    orgs.uniq!
    orgs.compact!
    orgs
  end

  def must_be_org_plan
    # Ignore this validation if this looks like the trusted org
    return if login == GitHub.trusted_oauth_apps_org_name
    errors.add("plan", "must be an Organization plan.") unless plan && plan.orgs?
  end

  def events_key(options = nil)
    "org:#{id}:public"
  end

  def event_prefix() :org end

  def event_payload
    {
      event_prefix => self,
    }
  end

  def event_context(prefix: event_prefix)
    {
      prefix => login,
      "#{prefix}_id".to_sym => id,
    }
  end

  # Public: Instrument new user creation.
  #
  # Returns nothing.
  def instrument_creation
    payload = {
      email: billing_email,
      plan: plan.try(:name),
      actor: creator,
      tos_sha: TosAcceptance.current_sha,
    }

    instrument :create, payload

    number_of_organizations_adminable_by_actor = creator&.owned_organizations&.count
    first_organization_adminable_by_actor = creator&.owned_organizations&.first

    # If the organization just created is the first adminable by the actor it may not be immediately
    # available from read replicas so we manually set the number to 1 and the first org to self.
    if number_of_organizations_adminable_by_actor == 0
      number_of_organizations_adminable_by_actor = 1
      first_organization_adminable_by_actor = self
    end

    GlobalInstrumenter.instrument "organization.create", {
      organization: self,
      actor: creator,
      number_of_organizations_adminable_by_actor: number_of_organizations_adminable_by_actor,
      first_organization_adminable_by_actor: first_organization_adminable_by_actor,
      actor_email: creator&.primary_user_email,
      actor_profile: creator&.profile,
      visitor_id: GitHub.context[:visitor_id].to_s,
    }
  end
  # Public: The User who is creating the Organization. This is not persisted,
  #         and is set by Organization::Creator#perform during org creation.
  attr_accessor :creator

  def instrument_github_app_manager(member, action:)
    case action
    when :grant
      instrument :integration_manager_added, manager: member
    when :revoke
      instrument :integration_manager_removed, manager: member
    end
  end

  # Public: Instrument an external identity for a user within this Organization's
  # SAML Provider getting revoked
  #
  # payload - Hash of custom payload data.
  #
  # Returns nothing.
  def instrument_external_identity_revoked(payload = {})
    instrument :revoke_external_identity, payload
  end

  # Public: retrieve the ExternalIdentity for this Organization
  #
  # An Organization can only have one ExternalIdentity, so this method
  # is just a shortcut to retrieve the first entry in external_identities
  def external_identity
    external_identities.first
  end

  # Public: Instrument a user's SAML SSO session getting revoked.
  #
  # payload - Hash of custom payload data.
  #
  # Returns nothing.
  def instrument_sso_session_revoked(payload = {})
    instrument :revoke_sso_session, payload
  end

  def audit_log
    @audit_log ||= AuditLog.new(self)
  end

  # Public: Check whether the github trusted auths are attached to this organization.
  #
  # Returns true when the org is the same that we configured.
  def trusted_oauth_apps_owner?
    self.id == GitHub.trusted_apps_owner_id
  end

  # Public: Safely add a public member, even if they're already a public member
  def publicize_member(user)
    return false unless member_publicizable?(user)

    # Allow multiple concurrent inserts, e.g. simultaneous API calls
    sql = github_sql.new user_id: user.id, org_id: id
    sql.add <<-SQL
      INSERT INTO public_org_members (user_id, organization_id)
      VALUES (:user_id, :org_id) ON DUPLICATE KEY UPDATE user_id = :user_id
    SQL
    sql.run

    user.synchronize_search_index

    true
  end

  def conceal_member(user)
    return false if user.nil?

    public_members.delete(user)
    user.synchronize_search_index

    true
  end

  # Public: Is user a public member of this organization?
  def public_member?(user)
    user && public_members.exists?(user.id)
  end

  # Overriding some following stuff from User
  # Orgs don't follow or get followed
  def followers_count;  0; end
  def following_count;  0; end
  def followers_count!; 0; end
  def following_count!; 0; end

  def repository_counts
    @repository_counts ||= OrgRepositoryCounts.new(self)
  end
  attr_writer :repository_counts

  # Public: Repositories owned by this org and visible to a user.
  #
  # user - User who should be able to see all the returned repositories
  # associated_repository_ids - Optional. Array of repository IDs as Integers
  #                             representing the repositories associated with this user.
  #                             Use this kwarg to memoize the potentially expensive call to
  #                             user.associated_repository_ids
  #
  # Returns a Repository scope.
  def visible_repositories_for(user, associated_repository_ids: nil)
    return org_repositories.public_scope if user.nil?
    return org_repositories if adminable_by?(user)

    ids = associated_repository_ids || user.associated_repository_ids

    if supports_internal_repositories?
      org_repositories.left_joins(:internal_repository).where(["repositories.id IN (?) OR internal_repositories.business_id IN (?) OR repositories.public = ?", ids, user.business_ids, true])
    else
      org_repositories.where(["repositories.id IN (?) OR repositories.public = ?", ids, true])
    end
  end

  # Public: Repositories owned by this org and associated with a user.
  #
  # For a repository to be "associated" with a user, the user must have some
  # sort of official connection to it, either through a team or by being a
  # direct collaborator.
  #
  # user - User who should be associated with all the returned repositories
  #
  # Returns a Repository scope.
  def repositories_associated_with(user)
    return Repository.none if user.nil?
    return org_repositories if adminable_by?(user)

    org_repositories.where(id: user.associated_repository_ids(include_oopfs: false))
  end

  # Public: Find an unaccepted invitation to this organization for the specified
  # user.
  #
  # invitee     - User to find an invitation for.
  # email       - The String email address to find an invitation for.
  # role        - the role user is invited as, can be any role from OrganizationInvitation::ROLES.keys
  # include_private_emails - a Boolean indicating if we should return invitations
  #                          associated with an invitee's private email
  #
  # Returns an OrganizationInvitation, or nil if there is none.
  def pending_invitation_for(invitee = nil, email: nil, role: nil, include_private_emails: true)
    return unless User.valid_email?(email) || invitee.is_a?(User)

    GitHub.dogstats.time("organization.time", tags: ["action:pending_invitation_for"]) do
      scope = invitations.pending

      scope = scope.with_business_role(*Array.wrap(role)) if role
      scope.with_invitee_or_normalized_email(invitee: invitee, emails: email, include_private_emails: include_private_emails).last
    end
  end

  # Public: Finds invitations to join a business sent to this organization
  #         that the specified user can accept (and thus should be warned about).
  #
  # user - User that should be able to accept any returned invitations.
  #
  # Returns an ActiveRecord::Relation for BusinessOrganizationInvitation.
  def business_invitations_acceptable_by(user)
    return BusinessOrganizationInvitation.none unless self.adminable_by?(user)

    BusinessOrganizationInvitation.with_status(:created).where(invitee: self)
  end

  # Public: Get the membership state of the specified user for this
  # organization. This is intended to be used by the API only.
  #
  # user - User to check the membership state for.
  #
  # Returns :pending, :active, or :inactive.
  def membership_state_of(user)
    if direct_or_team_member?(user)
      :active
    elsif pending_invitation_for(user).present?
      :pending
    else
      :inactive
    end
  end

  # Public: Get all the legacy admin members of the organization. A legacy admin
  # member is a non-owner org member who is on at least one legacy admin team.
  #
  # Note: This is a fairly expensive method, and it's not memoized that so it
  # won't act weird after changing team memberships. If you need to access this
  # multiple times in one request, save it in a variable first.
  #
  # Returns an Array.
  def legacy_admin_members
    Team.members_of(teams.legacy_admin.map(&:id)) - admins
  end

  def migrate_legacy_admin_teams
    MigrateLegacyAdminTeamsJob.perform_later(id)
  end

  def migrate_legacy_admin_teams!
    teams.legacy_admin.each do |legacy_admin_team|
      legacy_admin_team.throttle do
        legacy_admin_team.migrate_legacy_admin
      end
    end
  end

  # Public: Get all of this organization's repositories that the specified users
  # are collaborating on.
  #
  # The result is a Hash with a user id keys and an array of Repository values.
  #
  # {
  #   8  => [<Repo 1>, <Repo 2>],
  #   13 => [<Repo 3>]
  # }
  #
  # Note: this will *only* return directly collaborating repositories. If a user
  # has access to an repository through some other means (such as being an org
  # admin or a team member), it will not be included in this list.
  #
  # users - The id or ids of the users to get collaborating repositories for.
  #
  # limit - a limit to apply when querying for repos
  #
  # Returns a Hash (described above in more detail).
  def collaborating_repositories_for(user_ids, limit: MEGA_ORG_REPOS_THRESHOLD)
    user_and_repo_ids = collaborating_repository_ids_for(user_ids, limit: limit)

    _, org_repo_ids = user_and_repo_ids.transpose

    org_repo_by_id = Repository.where(id: org_repo_ids).index_by(&:id)
    return {} if org_repo_by_id.empty?

    user_and_repo_ids.each_with_object(Hash.new { |h, k| h[k] = [] }) do |(user_id, repo_id), result|
      next unless org_repo_by_id.has_key?(repo_id)

      result[user_id] << org_repo_by_id[repo_id]
    end
  end

  # Internal: Get all of this organization's repository ids that the specified users
  # are collaborating on.
  #
  # The result is an Array with a User id and Repository id values.
  #
  # [
  #   [<User 1>, <Repo 2>],
  #   [<User 1>, <Repo 3>],
  #   [<User 2>, <Repo 3>]
  # ]
  #
  # Note: this will *only* return directly collaborating repositories. If a user
  # has access to an repository through some other means (such as being an org
  # admin or a team member), it will not be included in this list.
  #
  # users - The id or ids of the users to get collaborating ids for.
  #
  # limit - a limit to apply when querying for repos
  #
  # Returns an Array (described above in more detail).
  def collaborating_repository_ids_for(user_ids, limit: MEGA_ORG_REPOS_THRESHOLD)
    user_ids = Set.new(Array(user_ids).compact)
    return [] if user_ids.empty?

    org_repo_ids = Repository.where(organization: self).active.limit(limit).ids
    return [] if org_repo_ids.empty?

    user_ids_set = user_ids.to_set

    Ability.with_ids(org_repo_ids, field: "subject_id").where(
      subject_type: "Repository",
      actor_type: "User",
      priority: Ability.priorities[:direct],
    )
      .distinct
      .pluck(:actor_id, :subject_id)
      .filter { |user_id, _repo_id| user_ids_set.include?(user_id) }
  end

  # Public: Gets a count of this organization's repositories that the specified users
  # are collaborating on.
  #
  # The result is a Hash with a user id keys and a count value.
  #
  # {
  #   8  => 0,
  #   13 => 42
  # }
  #
  # Note: this will *only* return a count of directly collaborating repositories. If a user
  # has access to an repository through some other means (such as being an org
  # admin or a team member), it will not be included in the count.
  #
  # users - The id or ids of the users to get collaborating repositories counts for.
  #
  # limit - a limit to apply when querying for repos
  #
  # Returns a Hash (described above in more detail).
  def collaborating_repository_count_for(user_ids, limit: MEGA_ORG_REPOS_THRESHOLD)
    user_ids = Array(user_ids).compact
    return {} if user_ids.empty?

    user_and_repo_ids = collaborating_repository_ids_for(user_ids, limit: limit)

    results = {}

    user_ids.each { |user_id| results[user_id] = 0 }

    user_and_repo_ids.each do |user_and_repo|
      user_id = user_and_repo.first
      results[user_id] = results[user_id] + 1
    end

    results
  end

  # Public: Get all the pending invitations for direct members of this
  # organization
  #
  # Returns an AR relation of OrganizationInvitations
  def direct_member_pending_invitations
    pending_invitations.with_business_role(:direct_member).includes(:invitee)
  end

  # Public: Get users who have been invited to join this organization. Does not include users who
  # have already accepted the invitation or users from cancelled invitations.
  #
  # Returns an ActiveRecord User relation.
  def pending_members
    User.joins("INNER JOIN organization_invitations " \
               "ON organization_invitations.invitee_id = users.id").
         where(organization_invitations: { organization_id: id }).
         merge(OrganizationInvitation.pending)
  end

  # Public: Get all the users who have been invited to be admins of this org.
  #
  # Once direct org membership ships, we can kill this and just use the
  # OrganizationInvitation scope
  #
  # Returns an Array of Users.
  def invited_admins
    @invited_admins ||= pending_invitations.with_business_role(:admin).map(&:invitee)
  end

  # Public: Get all the invitees who have been invited to be admins or direct
  # members of this organization.
  #
  # Returns an Array of OrganizationInvitations.
  def pending_non_manager_invitations
    pending_invitations.except_with_role(:billing_manager).with_valid_role.order("id ASC")
  end

  # Public: Get the role of the specified user in this org.
  #
  # user - User to check the role of.
  #
  # Returns a Organization::Role model object
  def role_of(user)
    Organization::Role.new(self, user)
  end

  # Internal: Initialize the default OAuth application policy if one has not
  # already been configured for this organization.
  #
  # Returns nothing.
  def initialize_application_policy
    return unless restrict_oauth_applications.nil?

    self.restrict_oauth_applications = GitHub.oauth_application_policies_enabled?

    # Don't let this method return false. This method is invoked in a
    # before_validation callback, and returning false would inadvertently
    # prevent the record from being saved.
    nil
  end

  # Public: Has someone attempted to destroy the owners team recently?
  #
  # Returns a boolean.
  def tried_to_destroy_owners_team_recently?
    destroy_owners_team_attempted_at.present? && destroy_owners_team_attempted_at > 10.minutes.ago
  end

  # Public: The last time the organization was active.
  #
  # Returns a string containing a date or "No activity".
  def last_active
    admin = admins.first
    if admin && event = admin.events(type: :org, param: self).first
      event.created_at.in_time_zone
    else
      "No activity"
    end
  end

  # Public: Get a list of IDs for repositories in this organization that are accessible by the
  # given user.
  #
  # user - a User
  # scope - optional Relation for filtering
  #
  # Returns an Array of Integers.
  def all_org_repo_ids_for_user(user, scope: nil)
    all_org_repos_for_user(user, scope: scope).pluck(:id)
  end

  # Return a list of repositories for this org, accessible by the given
  # user, including:
  #
  # - public repositories which are owned by the org
  # - repositories owned by the org, when user is a member of org's Owners team
  # - repositories owned by the org, when user is a member of an org team which
  #   grants access to the repository
  #
  # user - a User
  # scope - optional Relation for filtering
  #
  # Returns an Array of Repositories.
  def all_org_repos_for_user(user, scope: nil)
    public_scope = self.org_repositories.public_scope
    return filter_scope(public_scope, filter: scope) if user.nil?

    repo_ids_with_team_membership = user.associated_repository_ids(including: [:direct, :indirect]) & Repository.owned_by(self).ids
    public_and_team_repos_scope = public_scope.or(Repository.where(id: repo_ids_with_team_membership))
    filter_scope(public_and_team_repos_scope, filter: scope)
  end

  # Return a list of ids of repositories owned by any of the supplied organizations, accessible by the given
  # user, including:
  #
  # - public repositories which are owned by any of the the orgs
  # - private repositories owned by any of the orgs, when user is a member of org's Owners team
  # - private repositories owned by any of the orgs, when user is a member of an org team which
  #   grants access to the repository
  #
  # org_ids - a list of Organization ids
  # user - a User
  #
  # Returns an Array of Repository ids
  def self.all_repo_ids_for_orgs_for_user(org_ids, user)
    public_repos_for_orgs = Repository.public_scope.active.where(organization_id: org_ids).ids
    private_repos_for_orgs = Repository.private_scope.active.where(organization_id: org_ids)
    org_private_repos_for_user = user.associated_repository_ids(including: [:direct, :indirect]) & private_repos_for_orgs.ids

    public_repos_for_orgs + org_private_repos_for_user
  end

  # Public - Which layouts should be used when viewing this in site admin
  #
  # Returns "user" for users, and "organization" for orgs
  def site_admin_context
    "organization"
  end

  # Internal: can this organization be billed?
  #
  # Returns a boolean
  def billable?
    true
  end

  def email_spamminess_checks_enabled?
    false
  end

  # Internal: can the org be subscribed to notifications
  #
  # Returns false
  def newsies_enabled?
    false
  end

  # Public: Returns an elasticsearch query string that will find all of this
  # org's audit log events
  #
  # Returns a String which can be passed to elasticsearch as a `query_string`
  def audit_log_query
    "((_exists_:org AND org_id:#{id}) OR user_id:#{id} OR actor_id:#{id} OR data.old_user_id:#{id})"
  end

  # Public: Can the specified actor view projects on this organization?
  #
  # actor - The User trying to view projects.
  #
  # Returns a boolean.
  def projects_readable_by?(actor)
    if actor.can_have_granular_permissions?
      # When the actor is an app, we need to check if they've been granted
      # access to the organization's projects.
      resources.organization_projects.readable_by?(actor)
    else
      # When the actor is a user, they can always try to read an organization's
      # projects, since there may be public projects that are visible to
      # everyone. Per-project checking is done elsewhere.
      true
    end
  end

  # Public: Can the specified actor view projects on this organization?
  #
  # actor - The User trying to view projects.
  #
  # Returns Promise<bool>
  def async_projects_readable_by?(actor)
    if actor.can_have_granular_permissions?
      resources.organization_projects.async_readable_by?(actor)
    else
      Promise.resolve(true)
    end
  end

  # Public: Can the specified actor create/edit projects on this organization?
  #
  # actor - The User trying to create/edit projects.
  #
  # Returns a boolean.
  def projects_writable_by?(actor)
    resources.organization_projects.writable_by?(actor)
  end

  # Public: Can the specified actor create/edit projects on this organization?
  #
  # actor - The User trying to create/edit projects.
  #
  # Returns Promise<bool>
  def async_projects_writable_by?(actor)
    resources.organization_projects.async_writable_by?(actor)
  end

  # Public: Can the specified actor administer projects on this organization?
  #
  # actor - The User trying to administer projects.
  #
  # Returns a boolean.
  def projects_adminable_by?(actor)
    # We intentionally use writable_by? here, since there's no concept of
    # admin on projects in the pre-Abilities permission system.
    projects_writable_by?(actor)
  end

  # Public: Can the specified actor administer projects on this organization?
  #
  # actor - The User trying to administer projects.
  #
  # Returns Promise<bool>
  def async_projects_adminable_by?(actor)
    # We intentionally use writable_by? here, since there's no concept of
    # admin on projects in the pre-Abilities permission system.
    async_projects_writable_by?(actor)
  end


  # Public: The verified domains associated with this organization.
  #
  # Returns a Promise<Array[String]>.
  def async_verified_domains
    Platform::Loaders::VerifiedOrganizationDomains.load(id)
  end

  # Public: The verified domains from this organization's profile.
  #
  # Returns an Array[String].
  def verified_profile_domains
    async_verified_profile_domains.sync
  end

  # Public: The verified domains from this organization's profile.
  #
  # Returns a Promise<Array[String]>
  def async_verified_profile_domains
    return Promise.resolve([]) unless GitHub.domain_verification_enabled?

    async_profile.then do
      profile_domains = [profile_blog, profile_email].compact.reject(&:blank?)
      normalized_domains = profile_domains.map { |domain| OrganizationDomain.normalize_domain(domain) }.to_set
      next [] unless normalized_domains.any?

      async_verified_domains.then do |domains|
        verified_domains = domains.map(&:domain).to_set
        if normalized_domains.subset?(verified_domains)
          (verified_domains & normalized_domains).to_a
        else
          []
        end
      end
    end
  end

  # Public: Does this organization have a verified domain on its profile?
  #
  # Returns a Boolean.
  def is_verified?
    async_is_verified?.sync
  end

  # Public: Does this organization have a verified domain on its profile?
  #
  # Returns a Promise<Boolean>.
  def async_is_verified?
    return Promise.resolve(false) unless GitHub.domain_verification_enabled?

    async_verified_profile_domains.then do |verified_profile_domains|
      verified_profile_domains.any?
    end
  end

  # Public: The members of this organization that do not have a verified email
  # that is from a verified organization domain.
  #
  # Returns a Promise<ActiveRecord::Relation<User>>
  def async_members_without_verified_domain_email
    return Promise.resolve(User.none) unless GitHub.domain_verification_enabled?

    async_verified_domains.then do |domains|
      next User.none unless domains.any?

      members.where("NOT EXISTS (
        SELECT 1
        FROM user_emails
        WHERE user_emails.user_id = users.id
          AND user_emails.state = 'verified'
          AND user_emails.normalized_domain IN (:domains))",
      domains: domains.map(&:domain)).order(:id)
    end
  end

  # Public: Does this member have a verified email from a verified domain
  # for this organization?
  #
  # user - The User to check.
  #
  # Returns a Promise<Boolean>.
  def async_user_has_verified_domain_notification_email?(user)
    return Promise.resolve(false) unless GitHub.domain_verification_enabled?
    return Promise.resolve(false) unless user.present?

    Platform::Loaders::VerifiedDomainEmailsByUser.load(self, user.id).then do |domain_emails|
      domain_emails.present?
    end
  end

  # Public: Is this user able to receive email notifications for this organization?
  #
  # This method exists to check if a user is restricted by the verified domain
  # notification restrictions feature. It does not check if the user is able to
  # receive notifications for a specific repository and does not do any kind of
  # authorization checking.
  #
  # See also Newsies::Settings#email_notification_eligible_organizations
  #
  # user - The User to check.
  #
  # Returns a Promise<Boolean>.
  def async_user_can_receive_email_notifications?(user)
    return Promise.resolve(false) unless user

    async_member?(user).then do |is_member|
      # Outside collaborators and non-members who are receiving notifications for
      # this organization's public repositories are exempt from notification
      # restrictions, so we shouldn't prevent delivery for these users.
      next true unless is_member

      async_restrict_notifications_to_verified_domains?.then do |restriction_enabled|
        next true unless restriction_enabled

        async_user_has_verified_domain_notification_email?(user).then do |has_domain_email|
          next false unless has_domain_email

          Platform::Loaders::NewsiesSettings.load(user.id).then do |settings|
            next false unless settings

            email = settings.email(self)&.address
            next false unless email

            async_verified_domains.then do |verified_domains|
              domains = verified_domains.map(&:domain)
              _, domain = email.split("@")

              domains.include?(domain)
            end
          end
        end
      end
    end
  end

  # Public: Emails that a user can use to receive notifications from this organization.
  #
  # user - The User to get emails for.
  #
  # Returns a Promise<[UserEmail]>.
  def async_notifiable_emails_for(user)
    return Promise.resolve([]) unless user.present?

    async_member?(user).then do |is_member|
      next user.emails.notifiable unless is_member

      async_restrict_notifications_to_verified_domains?.then do |restriction_enabled|
        next user.emails.notifiable unless restriction_enabled

        Platform::Loaders::VerifiedDomainEmailsByUser.load(self, user.id)
      end
    end
  end

  # Public: Should the notification restrictions banner in this organization
  # be displayed for a specified user?
  #
  # user - The User to check.
  #
  # Returns a Promise<Boolean>.
  def async_show_notification_restriction_banner?(user)
    return Promise.resolve(false) unless user.present?

    async_user_can_receive_email_notifications?(user).then do |can_receive_notifications|
      next false if can_receive_notifications

      Platform::Loaders::NewsiesSettings.load(user.id).then do |settings|
        next false unless settings

        settings.participating_email? || settings.subscribed_email?
      end
    end
  end

  # Public: get the SAML provider for the Business that owns this Organization.
  # Note: this method will never return the Organization SAML provider - it is specifically
  # designed to retrieve the parent Business' SAML provider, if there is one.
  #
  # Returns a Promise<Business::SamlProvider>
  # will return a Promise<nil> if the Organization doesn't belong to a Business, or if
  # the Business doesn't have SAML enabled.
  def async_business_saml_provider
    async_business.then do |business|
      business&.async_saml_provider
    end
  end

  # Public: Does this organization have the EU data transfer SCC flag set?
  # Returns a boolean.
  def standard_contractual_clauses?
    GitHub.kv.exists("organization.standard_contractual_clauses.#{self.id}").value { false }
  end

  # Public: Flags the organization as subject to EU data transfer SCC.
  # Returns nothing.
  def flag_for_standard_contractual_clauses!(actor: self)
    GitHub.kv.setnx("organization.standard_contractual_clauses.#{id}", "1")

    payload = { org: self, prefix: "staff" }

    if actor.staff?
      guarded_actor = GitHub.guarded_audit_log_staff_actor_entry(actor)
      instrument :flag_for_standard_contractual_clauses, payload.merge(guarded_actor)
    else
      instrument :flag_for_standard_contractual_clauses, payload.merge(actor: actor)
    end
  end

  # Public: Remove the flag marking the organization as subject to EU data transfer SCC.
  # Returns nothing.
  def remove_standard_contractual_clauses_flag!(actor: self)
    GitHub.kv.del("organization.standard_contractual_clauses.#{id}")

    payload = { org: self, prefix: "staff" }

    if actor.staff?
      guarded_actor = GitHub.guarded_audit_log_staff_actor_entry(actor)
      instrument :remove_standard_contractual_clauses_flag, payload.merge(guarded_actor)
    else
      instrument :remove_standard_contractual_clauses_flag, payload.merge(actor: actor)
    end
  end

  # Private: Sets the associated company record via the company name provided.
  # Returns a Company
  def update_company
    return if @company_name.blank?
    # Don't allow an org to have a company if it is on the Standard terms of service
    return if on_standard_terms_of_service?

    self.company = retry_on_find_or_create_error do
      Company.find_by(name: @company_name) || Company.create(name: @company_name)
    end
  end

  # Private
  private def destroy_companies
    self.companies.destroy_all
  end

  def company=(company)
    self.companies.delete_all
    self.companies << company
  end

  def company
    self.companies.last
  end

  def terms_of_service
    @terms_of_service ||= Organization::TermsOfService.new(organization: self)
  end

  def on_standard_terms_of_service?
    terms_of_service.standard?
  end

  def root_teams
    Team.where(organization_id: id).where("LOWER(HEX(id)) = tree_path")
  end

  # Public: The last IP address of the most recently updated org admin
  def last_ip
    super || recent_admin_last_ip
  end

  # Check whether the Insights feature is enabled.  The feature is only available
  # for orgs with a business_plus billing plan when eventer is also enabled
  #
  # Returns a Boolean value.
  def insights_enabled?
    GitHub.eventer_enabled? && business_plus?
  end

  # Check whether the Dependency Insights feature is enabled and visible
  # to the given user.
  #
  # Returns a Boolean value.
  def dependency_insights_enabled_for?(viewer)
    return false unless GitHub.dependency_graph_enabled?
    return false unless business_plus?

    if members_can_view_dependency_insights?
      member?(viewer) || adminable_by?(viewer)
    else
      adminable_by?(viewer)
    end
  end

  def async_notices_for(viewer:)
    Promise.all([
      async_saml_sso_banner(viewer: viewer),
    ]).then { |notices| notices.compact }
  end

  def async_saml_sso_banner(viewer:)
    Promise.all([
      async_saml_provider,
      async_business,
    ]).then do
      saml_sso_banner = User::ORGANIZATION_NOTICES[:saml_sso_banner]

      if saml_sso_enabled? &&
         member?(viewer) &&
         !ExternalIdentity.linked?(provider: external_identity_session_owner.saml_provider, user: viewer) &&
         !GitHub.flipper[:org_idm_banner_disabled].enabled?(self) &&
         !viewer.dismissed_organization_notice?(saml_sso_banner, self)
         saml_sso_banner
      end
    end
  end

  # Internal: can the viewer see members of this organization?
  def member_or_can_view_members?(viewer)
    direct_or_team_member?(viewer) || bot_with_access_to_members?(viewer)
  end

  # memoized shortcut to get the current member count
  def members_count
    @count ||= super
  end

  # Check whether this org has the plan support and either:
  #  settings enabled for display commenter full name on the organization
  #  or user level enabling of the flag
  #
  # at a per repo level by visibility#
  #
  # visibility  - The visibility scope. :public or :private.
  def display_commenter_full_name_for_repo?(visibility:, viewer:)
    self.plan_supports_display_commenter_full_name?(visibility: visibility) && self.display_commenter_full_name_setting_enabled?
  end

  # Check if this org has a plan supporting display commenter full name
  #
  # visibility  - The visibility scope. :public or :private.
  def plan_supports_display_commenter_full_name?(visibility:)
    self.plan.supports?(:display_commenter_full_name, visibility: visibility)
  end

  # Populate organization with initial default labels.
  def populate_initial_user_labels
    UserLabel.initial_labels.each do |label_hash|
      new_label = user.user_labels.create(label_hash)
      new_label.instrument_creation(context: "organization creation") if new_label.persisted?
    end
  end

  def mega_org?
    members_count >= MEGA_ORG_MEMBER_THRESHOLD
  end

  def same_business?(user)
    return false unless business
    return false unless user&.organization?
    business == user.business
  end

  private

  # Internal: cancel pending invitations related to the organization where the
  # given user is either the inviter or the invitee. Includes direct membership
  # to organization owned repositories.
  #
  # user  - User involved in the invitations to be cancelled.
  # force - Boolean indicating if abilities/permissions checks should be
  # performed before cancelling. Defaults to false.
  #
  # Returns nothing.
  def cancel_all_invitations_involving(user, force: false)
    cancel_all_invitations_from(user, force: force)
    cancel_all_invitations_to(user)
    cancel_all_repository_invitations_involving(user)
  end

  # Internal: cancel pending organization invitations where the
  # given user is the inviter.
  #
  # user - User involved in the invitations to be cancelled.
  # force - Boolean indicating if abilities/permissions checks should be
  # performed before cancelling. Defaults to false.
  #
  # Returns nothing.
  def cancel_all_invitations_from(user, force: false)
    pending_invitations.where(inviter_id: user.id).each do |invitation|
      # Only cancel pending invitations from the user when forced or when the
      # user is no longer able to send invitations:
      if force || cannot_send_invitations?(user: user, teams: invitation.teams, role: invitation.role)
        invitation.cancel(actor: user)
      end
    end
  end

  # Internal: can the given user send invitations on behalf of the given teams
  # for the given role?
  #
  # user  - User to check ability to send invitations.
  # teams - Teams to check abilities to send invitations to.
  # role  - The role on the given teams that the invitation is intended for.
  #
  # Returns a Boolean.
  def cannot_send_invitations?(user:, teams:, role:)
    !user.can_send_invitations_for?(self, teams: teams, role: role)
  end

  # Internal: cancel pending organization invitations where the
  # given user is the invitee.
  #
  # user - User involved in the invitations to be cancelled.
  #
  # Returns nothing.
  def cancel_all_invitations_to(user)
    pending_invitations.where(invitee_id: user.id).each { |i| i.cancel(actor: user) }
  end

  # Internal: cancel pending invitations to org-owned repositories where the
  # given user is either the inviter or invitee.
  #
  # user - User involved in the invitations to be cancelled.
  #
  # Returns nothing.
  def cancel_all_repository_invitations_involving(user)
    RepositoryInvitation.cancel_all_invitations_involving(
      repo_ids: org_repositories.pluck(:id),
      user: user,
    )
  end

  # Internal: Instrument billing email change.
  #
  # Returns nothing.
  def instrument_billing_email_change
    GitHub.instrument "billing.change_email", org_id: id, org: login,
      email: billing_email, old_email: organization_billing_email_before_last_save
  end

  # Internal: Check if we can delete this organization.
  #
  # Return true if it's not the trusted org.
  def check_trusted_oauth_apps_owner
    throw :abort if trusted_oauth_apps_owner?
  end

  def enqueue_set_vertical
    UserSetVerticalJob.perform_later(id) unless GitHub.enterprise?
  end

  # Internal: Check if the specified user can have their membership with this
  # org publicized.
  #
  # user - User we're trying to publicize.
  #
  # Returns a boolean.
  def member_publicizable?(user)
    member?(user)
  end

  def all_team_members
    return User.none if new_record?
    User.where(id: all_team_member_ids)
  end

  def all_team_member_ids
    return [] if new_record?
    Team.user_ids_for(team_ids)
  end

  # Internal: is a user a member of one of this organization's teams teams?
  def team_member?(user)
    user && user.user? && !user.new_record? && all_team_member_ids.include?(user.id)
  end

  # Internal: Get all repositories owned by this org that the user has access to
  # through team memberships.
  #
  # Returns an arary of tuples: [[Repository ID, Ability]]
  def repository_abilities_from_team_membership(user)
    all_team_ids = Set.new
    teams_for(user).each { |team| all_team_ids.merge(team.id_and_ancestor_ids) }

    return [] if all_team_ids.blank?

    repo_ids = Repository.where(owner_id: id).pluck(:id)

    # Retrieve the repository id and team-based action of any direct or
    # team-based abilities a user has on this org's repos where the direct
    # action either does not exist or the team-based action is better.
    # Explicitly does not consider default org permissions nor permissions
    # through org adminship.
    results = []

    repo_ids.each_slice(1000) do |ids|
      query = Ability.github_sql.new(
        user_id: user.id,
        team_ids: all_team_ids.to_a,
        repo_ids: ids,
        direct: Ability.priorities[:direct],
      )

      query.add <<-SQL
        SELECT subject_id, MAX(abilities.action) team_action
        FROM abilities
        LEFT OUTER JOIN (
          SELECT subject_id repository_id, action
          FROM abilities
          WHERE actor_id   = :user_id
          AND actor_type   = 'User'
          AND subject_type = 'Repository'
          AND priority     = :direct
        ) direct_actions
        ON direct_actions.repository_id = abilities.subject_id
        WHERE subject_type        = 'Repository'
        AND abilities.subject_id IN :repo_ids
        AND priority              = :direct
        AND actor_type            = 'Team'
        AND actor_id             IN :team_ids
        AND (direct_actions.action IS NULL OR abilities.action > direct_actions.action)
        GROUP BY subject_id
      SQL

      results.concat(query.results)
    end

    results
  end

  # Internal: Take all the repository abilities that the specified user has
  # through this organization's teams and convert them to direct abilities.
  #
  # user - The user whose abilities we're converting.
  #
  # Returns nothing.
  def convert_team_abilities_to_direct_abilities(user)
    winning_team_actions_by_repo_ids = Hash[repository_abilities_from_team_membership(user)]

    Repository.find(winning_team_actions_by_repo_ids.keys).each do |repo|
      action = Ability.actions.key(winning_team_actions_by_repo_ids[repo.id])

      # We use add_member_without_validation_or_notifications instead of
      # add_member here because we don't want to subscribe the user to the repo
      # or send them emails.
      repo.add_member_without_validation_or_notifications(user, action: action)
    end
  end

  # Internal: is the viewer a Mannequin that will never have any permissions
  def mannequin_without_access_to_members?(viewer)
    viewer.is_a?(Mannequin)
  end

  # Internal: is the viewer a Bot who does NOT have permission to read 'members'
  #
  # * A Bot of installation without permission on members, cannot see all
  #   of the org's members
  def bot_without_access_to_members?(viewer)
    viewer.is_a?(Bot) && !resources.members.readable_by?(viewer)
  end

  # Internal: is the viewer a Bot who DOES have permission to read 'members'
  def bot_with_access_to_members?(viewer)
    viewer.is_a?(Bot) && resources.members.readable_by?(viewer)
  end

  # Internal: is the viewer a regular user who is not a member of the org?
  def human_non_member?(viewer)
    (viewer.user? && !member?(viewer))
  end

  # Remove all review request data
  def clear_review_requests
    issue_event_details = IssueEventDetail.joins(issue_event: [{issue: [:pull_request]}])
      .joins("INNER JOIN `review_requests` ON `review_requests`.`pull_request_id` = `pull_requests`.`id`")
      .where("review_requests.reviewer_id" => id)
      .where("issue_events.event" => "review_requested")
      .where("issue_event_details.subject_id" => id)

    issue_events = issue_event_details.map(&:issue_event)

    issue_events.map(&:destroy)
    issue_event_details.map(&:destroy)

    ReviewRequest.where(reviewer_id: id).each do |request|
      request.dismiss && request.save
    end
  end

  def actor
    @actor ||= (User.find_by_id(GitHub.context[:actor_id]) || User.ghost)
  end

  # Internal: is the organization is in the process of being created? During
  #           creation, Organization::Creator#perform will set the :creator
  #           accessor; otherwise :creator should be nil.
  def being_created?
    !creator.nil?
  end

  # Internal: Deletes business user accounts for users who are in this organization
  # and not in any other organization in the business
  #
  # Returns nothing.
  def remove_business_user_accounts_for_members
    return if GitHub.single_business_environment?
    return unless business.present?
    business.user_accounts.remove_members(unique_business_member_ids)
  end

  # Internal: Deletes the business user account for a user who is in this organization
  # and not in any other organization in the business
  #
  # user - the user to be removed from the business
  #
  # Returns nothing.
  def remove_user_from_business(user)
    return if GitHub.single_business_environment?
    return unless unique_business_member_ids.include?(user.id)

    business.user_accounts.remove_members([user.id])
  end

  # Internal: Creates a business user account for a user who is in this organization
  # and not in any other organization in the business
  #
  # user - the user to be added to the business
  #
  # Returns nothing.
  def add_user_to_business(user)
    return if GitHub.single_business_environment?
    business.add_user_accounts([user.id])
  end

  # Internal: Filters original scope using another scope if it exists
  #
  # scope - the original scope to filter
  # filter - the scope to use for the merge
  #
  # Returns filtered scope
  def filter_scope(scope, filter: nil)
    filter ? scope.merge(filter) : scope
  end

  # Returns organizations that have a team sync tenant with the appropriate
  # status
  scope :with_team_sync_status, lambda { |status|
    org_ids = TeamSync::Tenant.
      where(organization_id: ids, status: status).
      pluck(:organization_id)

    where(id: org_ids)
  }

  # Find the most recently updated admin for this org and return their last_ip
  def recent_admin_last_ip
    recent_admin = admins.order(updated_at: :desc).limit(1).first
    # It is unlikely that there would be no admins for an Org. It is possible
    # that replication lag could cause this call to return no admins. That
    # could impact operations like the CheckForSpamJob. See also:
    # https://github.com/github/github/issues/127523
    raise NoAdminsError unless recent_admin

    recent_admin.last_ip
  end
end
