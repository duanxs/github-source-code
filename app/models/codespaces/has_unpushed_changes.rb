# frozen_string_literal: true
module Codespaces
  class HasUnpushedChanges
    def self.call(**kwargs)
      new(**kwargs).call
    end

    def initialize(
      codespace:,
      user: codespace.owner,
      client: Codespaces::VscsClient
    )
      @codespace = codespace
      @user = user
      @client = client
    end

    def call
      return false unless @codespace.provisioned?

      @client.
        for_codespace(@codespace, user: @user).
        fetch_environment(@codespace.guid)["hasUnpushedGitChanges"]
    end
  end
end
