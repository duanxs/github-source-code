# frozen_string_literal: true

module Codespaces
  class ProvisionEnvironment
    attr_reader :codespace

    delegate :provisioning!, :pending!, :failed!, to: :codespace, prefix: true, private: true

    def self.call(codespace, vscs_target: nil)
      new(codespace, vscs_target: vscs_target).call
    end

    def initialize(codespace, vscs_target: nil, error_reporter: Codespaces::ErrorReporter)
      @codespace = codespace
      @vscs_target = vscs_target
      @error_reporter = error_reporter
    end

    def call
      @error_reporter.push(codespace: codespace)

      return unless codespace.pending?

      if codespace.owner.spammy?
        codespace_failed!
        return
      end

      instrument_provisioning do
        codespace_provisioning!
        provision_plan!
        provision_environment!
      end
    rescue
      # Any failures should put us back into `pending`. The job itself may put
      # us in `failed` depending on the type of error. I don't think we want to
      # duplicate the retryable error stuff here.
      codespace_pending!
      raise
    end

    private

    def plan_owner_from_repo
      codespace.repository&.organization || codespace.owner
    end

    def provision_plan!
      # If the codespace has a plan already then this must be a retry. There is
      # no reason to retry the plan portion so we can just bail early.
      return if codespace.plan

      plan = Codespaces::ProvisionPlan.call(
        owner: plan_owner_from_repo,
        location: codespace.location,
        vscs_target: @vscs_target
      )
      # We must set the plan here before trying to FetchEnvironment because
      # the client requires a plan `vso_id`.
      codespace.update!(plan: plan)
      @error_reporter.push(codespace: codespace, plan: plan)
    end

    def provision_environment!
      environment = Codespaces::FetchEnvironment.call(codespace)
      codespace.update!(
        guid: environment.id,
        state: :provisioned
      )
    end

    def instrument_provisioning
      # TODO: Instrument provisioning events
      yield
      GlobalInstrumenter.instrument("codespaces.provisioned", codespace: codespace)
      codespace.instrument :provision_environment, guid: codespace.guid
    end
  end
end
