# frozen_string_literal: true

module Codespaces
  class ResourceGroup < ApplicationRecord::Domain::Codespaces
    MAX_NAME_LENGTH = 90
    MAX_PLANS_PER = 800

    self.table_name = "workspace_resource_groups"

    has_many :plans, class_name: "Codespaces::Plan", foreign_key: :resource_group_id

    validates :subscription, length: { is: 36 }
    validates :location, presence: true
    validates :name, length: { in: 1..MAX_NAME_LENGTH }

    before_validation :generate_name, on: :create

    scope :in_active_subscription, -> { where(subscription: Azure.active_subscription_pool) }
    scope :with_available_capacity, -> {
      where("workspace_resource_groups.plans_count < ?", MAX_PLANS_PER).order(:plans_count)
    }

    def self.with_capacity_in_location(location)
      in_active_subscription.
        with_available_capacity.
        where(location: location).
        first
    end

    def vscs_id
      return unless persisted?

      "/subscriptions/#{subscription}/resourceGroups/#{name}"
    end

    private

    def generate_name
      return unless location?

      self.name ||= "#{ location }-#{ SecureRandom.uuid }"
    end
  end
end
