# frozen_string_literal: true
module Codespaces
  class Policy
    DEFAULT_ROLE_GRANTER = Permissions::Granters::RoleGranter
    DEFAULT_AUTHORIZER = Permissions::Enforcer
    CODESPACE_ORG_CREATOR_ROLE = "codespace_org_creator"

    def self.should_see_codespaces?(repo, user)
      enabled_for_user?(user) && creatable_by_user?(repo, user)
    end

    def self.creatable_by_user?(repo, user, org: nil, authorizer: DEFAULT_AUTHORIZER)
      org ||= repo.owner if repo_is_org_owned?(repo)
      repo_has_app_integration?(repo, user) &&
      repo_is_not_archived?(repo) &&
      (
        repo_is_public?(repo) ||
        repo_is_owned?(repo, user) ||
        repo_is_org_accessible?(repo, user, org, authorizer) ||
        gh_gh_special_flag_set?(repo, user)
      )
    end

    # Whether or not a user can push to or fork a given repo
    def self.pushable_or_forkable_by_user?(repo, user)
      repo_is_pushable?(repo, user) || repo_is_forkable?(repo, user)
    end

    # Whether or not the codespaces feature is available to a given user
    def self.enabled_for_user?(user)
      codespaces_enabled? && user_feature_flagged?(user)
    end

    # Whether or not the codespaces feature is available for repos in the given organization
    def self.enabled_for_organization?(repo, org)
      repo_is_org_owned?(repo) &&
      org_feature_flagged?(org) &&

      org.organization_codespaces_allowed?
    end

    # Whether or not codespaces can be available in the given environment
    def self.codespaces_enabled?
      not_enterprise? && codespaces_service_configured?
    end

    # Whether or not a user is a codespaces developer
    def self.is_codespaces_developer?(user)
      developer_feature_flag?(user)
    end

    # Whether or not we should fork up front for a codespace
    def self.should_fork_up_front?(repo, user, ref: nil)
      repo_not_pushable?(repo, user, ref: ref) && repo_is_forkable?(repo, user)
    end

    def self.grant_codespace_org_creator!(user, org, role_granter: Permissions::Granters::RoleGranter)
      result = nil
      if org_feature_flagged?(org)
        result = role_granter.new(actor: user, target: org, role_name: CODESPACE_ORG_CREATOR_ROLE).grant!
        GitHub.flipper[:workspaces].enable(user) unless result.nil? || user_feature_flagged?(user)
      end
      result
    end

    def self.revoke_codespace_org_creator!(user, org, role_granter: Permissions::Granters::RoleGranter)
      if org_feature_flagged?(org)
        role_granter.new(actor: user, target: org, role_name: CODESPACE_ORG_CREATOR_ROLE).revoke_if_exists!
      end
    end

    class << self
      private

      def user_feature_flagged?(user)
        GitHub.flipper[:workspaces].enabled?(user)
      end

      def org_feature_flagged?(org)
        GitHub.flipper[:workspaces].enabled?(org)
      end

      def repo_is_not_archived?(repo)
        repo && !repo.archived?
      end

      def repo_is_public?(repo)
        repo.public?
      end

      def repo_is_owned?(repo, user)
        repo.owner_id == user.id
      end

      # Authorizer by default is set to `Permissions::Enforcer` which makes a call to authzd.
      # Eventually we can make this authzd policy more general and use this to check
      # if the user can create a codespace. See: https://github.com/github/codespaces/issues/855
      def repo_is_org_accessible?(repo, user, org, authorizer)
        org &&
        authorizer &&
        enabled_for_organization?(repo, org) &&
        authorizer.authorize(
          action: :write_org_codespace,
          actor: user,
          subject: repo
        ).allow?
      end

      def repo_is_org_owned?(repo)
        repo.owner&.organization?
      end

      def repo_is_pushable?(repo, user, ref: nil)
        repo.pushable_by?(user, ref: ref)
      end

      def repo_not_pushable?(repo, user, ref: nil)
        !repo_is_pushable?(repo, user, ref: ref)
      end

      def repo_is_forkable?(repo, user)
        repo && user.can_fork?(repo)
      end

      def gh_gh_special_flag_set?(repo, user)
        # XXX @orph: see https://github.com/github/codespaces/issues/264
        repo.name_with_owner == "github/github" &&
          GitHub.flipper[:codespaces_monolith].enabled?(user)
      end

      def repo_has_app_integration?(repo, user)
        # Checks to see if the Codespaces integration for the user's current
        # VSCS target allows installations on the repository. For Dev and PPE
        # the integration is only allowed on specific organizations.
        vscs_target_config = Codespaces.vscs_target_config(user)
        integration = Apps::Internal.integration(vscs_target_config[:integration_alias])
        Apps::Internal.repo_accessible_to_limited_app?(repo, app: integration)
      end

      def developer_feature_flag?(user)
        GitHub.flipper[:codespaces_developer].enabled?(user)
      end

      def not_enterprise?
        !GitHub.enterprise?
      end

      def codespaces_service_configured?
        GitHub.codespaces_service_principal[:client_secret].present?
      end
    end
  end
end
