# frozen_string_literal: true

require_dependency "codespaces"

module Codespaces
  class BillingMessage < ApplicationRecord::Domain::Codespaces
    self.table_name = "codespace_billing_messages"

    areas_of_responsibility :codespaces

    belongs_to :codespace_plan, class_name: "Codespaces::Plan"

    validates :azure_storage_account_name, presence: true, length: { maximum: 24 }
    validates :event_id, presence: true, length: { maximum: 36 }
    validates :codespace_plan, :payload, presence: true

    after_create :enqueue_process_billing_message_job

    def period_start
      @start_time ||= Time.parse(payload.dig("periodStart"))
    end

    def period_end
      @end_time ||= Time.parse(payload.dig("periodEnd"))
    end

    def environments
      @environments ||= payload.dig("usageDetail", "environments")
    end

    private

    def enqueue_process_billing_message_job
      CodespacesProcessBillingMessageJob.perform_later(billing_message_id: self.id)
    end
  end
end
