# frozen_string_literal: true

module Codespaces
  class ProvisionResourceGroup
    attr_reader :location

    def self.call(location:)
      new(location: location).call
    end

    def initialize(location:)
      @location = location
    end

    def call
      return existing_resource_group if existing_resource_group

      resource_group = Codespaces::ResourceGroup.create!(
        subscription: Azure.subscription_for_provision,
        location: location
      )
      client.create_resource_group(
        resource_group.subscription,
        resource_group.name,
        resource_group.location
      )

      resource_group
    rescue Codespaces::Error, Faraday::Error
      resource_group.destroy!
      raise
    end

    private

    def existing_resource_group
      Codespaces::ResourceGroup.with_capacity_in_location(location)
    end

    def client
      @client ||= Codespaces::ArmClient.new(resource_provider: nil)
    end
  end
end
