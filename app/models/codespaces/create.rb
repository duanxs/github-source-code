# frozen_string_literal: true
require "codespaces"

module Codespaces
  class Create
    class InvalidTarget < Codespaces::Error; end

    attr_reader :attributes, :vscs_target

    def self.call!(attributes, **kwargs)
      new(attributes, **kwargs).call!
    end

    # `attributes` - a hash or permitted ActionController::Parameters
    def initialize(attributes, vscs_target: nil)
      @attributes, @vscs_target = attributes, vscs_target
    end

    def call!
      validate!
      fetch_target_repository!
      Codespace.create!(corrected_attributes).tap do |codespace|
        CodespacesProvisionJob.perform_later(codespace: codespace, vscs_target: vscs_target)
      end
    end

    private

    def validate!
      if !(attributes[:ref].present? ^ attributes[:pull_request_id].present?)
        raise ArgumentError, "exactly one of :ref or :pull_request_id must be specified"
      end
      if vscs_target && !GitHub.flipper[:codespaces_developer].enabled?(attributes[:owner])
        raise InvalidTarget, "vscs_target specified but owner lacks :codespaces_developer flag"
      end
    end

    def pull_request
      return @pull_request if defined?(@pull_request)

      if pull_request_id = attributes[:pull_request_id].presence
        @pull_request = PullRequest.find_by(id: pull_request_id)
      else
        @pull_request = nil
      end
    end

    def base_ref
      return @base_ref if defined?(@base_ref)

      @base_ref = attributes[:ref].presence || pull_request&.head_ref
    end

    def base_repository
      return @base_repository if defined?(@base_repository)
      # If we have a PR we should just use its `head_repository` as our base repo
      # effectively ignoring any supplied `repository_id`. We could validate this
      # but that seems unnecessary if we can just ignore the `repository_id`.
      @base_repository = pull_request&.head_repository || Repository.find(attributes[:repository_id])
    end

    def fetch_target_repository!
      @repository, @ref, @oid = Codespaces::GetTargetRepository.call(
        user: attributes[:owner],
        repository: base_repository,
        ref_or_name: base_ref
      )
    end

    def corrected_attributes
      attributes.merge(
        repository_id: @repository.id,
        ref: @ref,
        oid: @oid
      )
    end
  end
end
