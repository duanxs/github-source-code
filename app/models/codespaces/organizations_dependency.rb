# frozen_string_literal: true

module Codespaces::OrganizationsDependency

  extend ActiveSupport::Concern
  include OrganizationsHelper

  MAX_USERS = 15

  def members_with_access
    current_organization.members.where(
      id: UserRole.includes(:role).where(
        roles: {name: Codespaces::Policy::CODESPACE_ORG_CREATOR_ROLE},
        target_type: "Organization",
        target_id: current_organization.id,
        actor_type: "User"
      ).pluck(:actor_id)
    )
  end
end
