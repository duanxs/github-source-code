# frozen_string_literal: true
module Codespaces
  class SuspendUserEnvironments
    def self.call(user)
      new(user).call
    end

    def initialize(
      user,
      scope: user.codespaces.provisioned,
      error_reporter: Codespaces::ErrorReporter,
      client: Codespaces::VscsClient
    )
      @user = user
      @scope = scope
      @error_reporter = error_reporter
      @client = client
    end

    def call
      @scope.find_each do |codespace|
        @error_reporter.push(codespace: codespace) do
          @client.for_codespace(codespace).shutdown_environment(codespace.guid)
        end
      end
    end
  end
end
