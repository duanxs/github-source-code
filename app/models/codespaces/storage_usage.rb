# frozen_string_literal: true

module Codespaces
  class StorageUsage < Codespaces::ResourceUsage
    attr_accessor :size_in_gb

    validates_presence_of :size_in_gb

    validates :size_in_gb, numericality: {
        only_integer: true,
        greater_than_or_equal_to: 0,
    }

    def size_in_bytes
      @size_in_gb * Numeric::MEGABYTE * Numeric::KILOBYTE
    end

    def computed_usage
      @size_in_gb * (@billable_duration_in_seconds / UNIT_OF_MEASUREMENT)
    end
  end
end
