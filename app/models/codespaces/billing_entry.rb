# frozen_string_literal: true

require_dependency "codespaces"

module Codespaces
  class BillingEntry < ApplicationRecord::Domain::Codespaces
    include Instrumentation::Model

    self.table_name = "codespace_billing_entries"

    belongs_to :codespace, foreign_key: :codespace_guid, primary_key: :guid
    belongs_to :billable_owner, polymorphic: true
    belongs_to :codespace_owner, polymorphic: true
    belongs_to :repository, class_name: "::Repository"

    validates :billable_owner, :codespace_owner, :codespace_guid, :codespace_plan_name, presence: true
  end
end
