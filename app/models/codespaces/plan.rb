# frozen_string_literal: true

require_dependency "codespaces"

module Codespaces
  class Plan < ApplicationRecord::Domain::Codespaces
    include Instrumentation::Model

    MAX_NAME_LENGTH = 90
    DEFAULT_RESOURCE_PROVIDER = "Microsoft.Codespaces"

    self.table_name = "workspace_plans"

    belongs_to :owner, polymorphic: true
    belongs_to :resource_group,
      class_name: "Codespaces::ResourceGroup",
      inverse_of: :plans,
      counter_cache: :plans_count
    has_many :codespaces,
             foreign_key: :plan_id,
             dependent: :destroy
    has_many :billing_messages, class_name: "Codespaces::BillingMessage"

    validates :owner, :resource_group, presence: true
    validates :name, length: { in: 1..MAX_NAME_LENGTH }
    validates :vscs_target, presence: true, on: :create # TODO run validation all the time once backfilled
    validates :vscs_target, inclusion: Codespaces.vscs_targets

    before_validation :generate_name, on: :create

    after_commit :enqueue_deprovision_plan_job, on: :destroy
    after_commit :report_utilization!, on: :destroy

    scope :in_location, -> (location) { joins(:resource_group).where("workspace_resource_groups.location = ?", location) }

    # Public: Returns the VSCS target for this plan as a Symbol. If no
    # VSCS target is set, looks up the default VSCS target for the plan owner if available.
    #
    # Returns a Symbol or nil if no VSCS target specified.
    def vscs_target
      self[:vscs_target] ||= Codespaces.vscs_target(owner) if owner

      super.presence.to_sym
    end

    # Public: Is this backed by a real VSO plan or is it just abstract?
    #
    # Returns a Boolean.
    def provisioned?
      resource_group_id?
    end

    def event_prefix
      "codespaces_plan"
    end

    def event_payload
      {
        plan_id: id,
        owner_id: owner.id,
        resource_group_id: resource_group.id,
        name: name,
        location: resource_group.location,
        subscription: resource_group.subscription,
      }
    end

    def vscs_id
      return unless resource_group && name

      "#{resource_group.vscs_id}/providers/#{resource_provider}/plans/#{name}"
    end

    private

    def enqueue_deprovision_plan_job
      CodespacesDeprovisionPlanJob.perform_later(vscs_plan_id: vscs_id, user_id: owner_id, vscs_target: vscs_target, resource_provider: resource_provider)
    end

    def report_utilization!
      Codespaces::Azure.report_utilization!
    end

    def generate_name
      self.name ||= "plan-#{SecureRandom.uuid}"
    end
  end
end
