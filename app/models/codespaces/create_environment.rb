# frozen_string_literal: true

module Codespaces
  class CreateEnvironment
    class TimeoutError < Codespaces::Client::TimeoutError; end

    attr_reader :codespace

    def self.call(codespace)
      new(codespace).call
    end

    def initialize(codespace)
      @codespace = codespace
    end

    def call
      client.create_environment(
        codespace.location,
        codespace.repository,
        name: codespace.name,
        moniker: codespace.moniker
      )
    rescue Codespaces::Client::TimeoutError => e
      raise TimeoutError.new(e)
    end

    private

    def client
      @client ||= Codespaces::VscsClient.for_codespace(
        codespace,
        # The timeout here is roughly 1.5x the P99.9 response time for creation
        timeouts: { open_timeout: 2, timeout: 30 }
      )
    end
  end
end
