# frozen_string_literal: true

module Codespaces
  class ComputeUsage < Codespaces::ResourceUsage
    #
    # These figures & computed_usage logic are obtained from this
    # issue: https://github.com/github/codespaces/issues/779
    #
    BASIC_LINUX_PRICE_PER_HOUR = 0.085
    STANDARD_LINUX_PRICE_PER_HOUR = 0.169
    PREMIUM_LINUX_PRICE_PER_HOUR = 0.338
    SKU_HOUR_MULTIPLIER = BASIC_LINUX_PRICE_PER_HOUR
    SKU_HOUR_MULTIPLIERS = {
        basicLinux: BASIC_LINUX_PRICE_PER_HOUR / SKU_HOUR_MULTIPLIER,
        standardLinux: STANDARD_LINUX_PRICE_PER_HOUR / SKU_HOUR_MULTIPLIER,
        premiumLinux: PREMIUM_LINUX_PRICE_PER_HOUR / SKU_HOUR_MULTIPLIER
    }

    #
    # computed_usage is returning the amount of hours of usage for a given sku. Due to a
    # billing subscription service limitation, we expand the computed_usage for non-basic SKUs
    # to match what it would be billed at per hour.
    #
    # Context from gitcoin: because of a limitation in our subscription service, we can't have different prices
    # per SKU for usage based charges. So we get around this by creating a charge with the "base price" and then
    # we multiply the usage to match the subsequent prices
    #
    def computed_usage
      (@billable_duration_in_seconds / UNIT_OF_MEASUREMENT) * SKU_HOUR_MULTIPLIERS[@sku.to_sym]
    end
  end
end
