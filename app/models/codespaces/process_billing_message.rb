# frozen_string_literal: true

module Codespaces
  class ProcessBillingMessage
    class MappingError < StandardError; end

    def self.call(**kwargs)
      new(**kwargs).call
    end

    def initialize(
      billing_message_id:,
      error_reporter: Codespaces::ErrorReporter
    )
      @billing_message = Codespaces::BillingMessage.find(billing_message_id)
      @error_reporter = error_reporter
    end

    def call
      compute_usage, storage_usage = process_message
      instrument("codespaces.compute_usage", compute_usage)
      instrument("codespaces.storage_usage", storage_usage)
    rescue MappingError => e
      @error_reporter.report(e, billing_message_id: @billing_message.id)
    end

    def process_message
      compute_usage = []
      storage_usage = []

      @billing_message.environments.each do |environment|
        codespace_compute_usage, codespace_storage_usage = process_environment(environment)
        compute_usage = compute_usage + codespace_compute_usage
        storage_usage = storage_usage + codespace_storage_usage
      end

      return compute_usage, storage_usage
    end

    def process_environment(environment)
      guid = environment.dig("id")
      computes = environment.dig("resourceUsage", "compute")
      storages = environment.dig("resourceUsage", "storage")

      billing_entry = Codespaces::BillingEntry.find_by(
        codespace_plan_name: @billing_message.codespace_plan.name,
        codespace_guid: guid,
      )

      compute_usage = computes.nil? ? [] : process_compute(billing_entry, computes)
      storage_usage = storages.nil? ? [] : process_storage(billing_entry, storages)
      return compute_usage, storage_usage
    end

    def process_compute(billing_entry, computes)
      computes.map do |compute|
        codespace_compute_usage = Codespaces::ComputeUsage.new(
          billing_entry: billing_entry,
          billing_message: @billing_message,
          billable_duration_in_seconds: compute.dig("usage"),
          sku: compute.dig("sku"),
        )

        unless codespace_compute_usage.valid?
          raise MappingError, codespace_compute_usage.errors.full_messages.to_sentence
        end

        codespace_compute_usage
      end
    end

    def process_storage(billing_entry, storages)
      storages.map do |storage|
        codespace_storage_usage = Codespaces::StorageUsage.new(
          billing_entry: billing_entry,
          billing_message: @billing_message,
          billable_duration_in_seconds: storage.dig("usage"),
          sku: storage.dig("sku"),
          size_in_gb: storage.dig("size")
        )

        unless codespace_storage_usage.valid?
          raise MappingError, codespace_storage_usage.errors.full_messages.to_sentence
        end

        codespace_storage_usage
      end
    end

    def instrument(name, usage)
      usage.each do |u|
        GlobalInstrumenter.instrument(name, usage: u)
      end
    end
  end
end
