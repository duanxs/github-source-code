# frozen_string_literal: true

module Codespaces
  module Azure
    RESOURCE_GROUP_LIMIT = 980

    module ClassMethods
      def vso_locations_url(user)
        vscs_target = Codespaces.vscs_target(user)
        Codespaces.vscs_locations_url_for_target(vscs_target)
      end

      def active_subscription_pool
        if GitHub.flipper[:codespaces_expanded_subscription_pool].enabled?
          ::GitHub.codespaces_subscriptions
        else
          # The first element in the array is the singular subscription we've
          # been using so this effectively truncates the pool to only that one
          # known working subscription.
          ::GitHub.codespaces_subscriptions.take(1)
        end
      end

      def subscription_for_provision
        count_by_subscription = active_subscription_pool.each_with_object({}) do |sub, coll|
          coll[sub] = 0
        end
        count_by_subscription.merge! Codespaces::ResourceGroup.in_active_subscription.group(:subscription).count
        lowest_utilization, count = count_by_subscription.sort_by { |sub, count| count }.first

        lowest_utilization unless count >= RESOURCE_GROUP_LIMIT
      end

      def report_utilization!
        GitHub.dogstats.batch do
          active_groups = Codespaces::ResourceGroup
            .in_active_subscription
            .group(:subscription, :location)

          report_aggregated_count(active_groups.count, "active_resource_groups")

          with_capacity = active_groups.with_available_capacity
          # We do our own aggregation instead of having Datadog to do it so that
          # we don't have to report counts for *every* resource group. This
          # doesn't give us percentiles but that's ok for our purposes.
          report_aggregated_count(with_capacity.minimum(:plans_count), "active_plans.min")
          report_aggregated_count(with_capacity.maximum(:plans_count), "active_plans.max")
          report_aggregated_count(with_capacity.average(:plans_count), "active_plans.avg")
        end
      end

      private

      def report_aggregated_count(rel, key)
        rel.each do |(subscription, location), count|
          tags = ["location:#{location}", "subscription:#{subscription}"]
          GitHub.dogstats.gauge("codespaces.#{key}", count, tags: tags)
        end
      end
    end

    extend ClassMethods
  end
end
