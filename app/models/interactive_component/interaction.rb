# frozen_string_literal: true

class InteractiveComponent
  class Interaction < ApplicationRecord::Collab
    belongs_to :interactive_component
    belongs_to :user

    def expiry_date
      interacted_at + 15.minutes
    end

    def expired?
      Time.now.utc >= expiry_date
    end
  end
end
