# frozen_string_literal: true

class InteractiveComponent
  class Container < ApplicationRecord::Collab
    self.abstract_class = true

    areas_of_responsibility :ce_extensibility

    ASSOCIATED_DOMAINS = {
      repo: Repository.to_s,
    }.freeze

    def associated_domain
      raise NotImplementedError
    end
  end
end
