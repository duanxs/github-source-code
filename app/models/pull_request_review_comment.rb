# rubocop:disable Style/FrozenStringLiteralComment

class PullRequestReviewComment < ApplicationRecord::Domain::Repositories
  areas_of_responsibility :pull_requests
  include GitHub::UTF8
  include GitHub::UserContent
  include GitHub::Validations
  include GitHub::RateLimitedCreation
  include GitHub::CommentMetrics
  include GitHub::Relay::GlobalIdentification
  include GitHub::MinimizeComment

  include Instrumentation::Model
  include EmailReceivable
  include NotificationsContent::WithCallbacks
  include PushNotifiable
  include Reaction::Subject::RepositoryContext
  include Referrer
  include Spam::Spammable
  include UserContentEditable
  include InteractionBanValidation
  include AuthorAssociable
  include ReferenceableContent
  include Blockable
  include AbuseReportable

  include Permissions::Attributes::Wrapper
  self.permissions_wrapper_class = Permissions::Attributes::IssueComment

  enum state: {
    pending: 0,
    submitted: 1,
  }

  # Callers should never set the state directly. See submit! for changing
  # a pending PullRequestReviewComment to a submitted state...or better yet,
  # call workflow methods on the associated PullRequestReview.
  def state=(val)
    super
  end
  private :state=

  belongs_to :pull_request, touch: true, inverse_of: :review_comments
  belongs_to :pull_request_review, inverse_of: :review_comments
  belongs_to :pull_request_review_thread, inverse_of: :review_comments
  belongs_to :repository
  belongs_to :user

  # Used to validate the end position value and set various position attributes.
  attr_accessor :end_position_data

  # Used to validate the start position value and set the :start_position_offset attribute.
  attr_accessor :start_position_data

  setup_spammable(:user)

  # NOTE: If you try to load this association dynamically, your query will probably
  # time out!
  #
  # This generates a query against the pull_request_review_comments table containing
  # `where reply_to_id=?`, which causes a full table scan over all comment
  # records. There is an index on `reply_to_id`; however, there are too few records
  # containing a value, all legacy comments have NULL, so the query planner does not
  # use it, preferring a slow table scan.
  #
  # Solutions:
  #
  #   - Remove this association so we don't accidentally query the column.
  #   - Include `where pull_request_id=?` in the query to trigger a better index.
  #   - Use `force index` to tell the planner we really do want to use reply_to_id's index.
  #   - Backfill legacy comments reply_to_id values based on their path and position data.
  has_many :replies, foreign_key: :reply_to_id, class_name: "PullRequestReviewComment", inverse_of: :in_reply_to
  belongs_to :in_reply_to, foreign_key: :reply_to_id, class_name: "PullRequestReviewComment", inverse_of: :replies

  # Scoped association for eager loading with `includes`.
  has_many :submitted_replies,
    -> { submitted },
    foreign_key: :reply_to_id,
    class_name: "PullRequestReviewComment"

  has_many :reactions, as: :subject

  before_validation :assign_repository_from_pull_request, on: :create

  before_validation :ensure_state_is_set

  before_validation :copy_position_data_for_reply, on: :create
  before_validation :initialize_position_data, on: :create
  before_validation :write_start_position_offset, on: :create
  before_validation :write_end_position_data, on: :create
  validate :validate_end_position_data, on: :create
  validate :validate_start_position_offset, on: :create
  validates_presence_of :diff_hunk, on: :update

  validates_presence_of :body
  validates_presence_of :original_position, on: :create, if: -> (comment) { comment.end_position_data.nil? }
  validates :state, presence: true
  validates :body, bytesize: { maximum: MYSQL_UNICODE_BLOB_LIMIT }, unicode: true
  validate :user_can_interact?, on: :create

  before_validation :initialize_original_attribute_values, on: :create

  before_create :truncate_diff_hunk
  before_validation :initialize_review_thread, on: :create

  after_validation :instrument_failed_validation, on: :create, if: lambda { errors.any? }

  after_create :subscribe_to_issue
  after_commit :instrument_creation, on: :create

  # spam check on create handled by Newsies::DeliverNotificationsJob
  after_commit :enqueue_check_for_spam, on: :update,  if: :check_for_spam?

  after_create :ensure_synced_with_pull

  after_commit :instrument_update, on: :update, if: :body_changed_after_commit?
  after_commit :notify_socket_subscribers, on: :update, if: :body_changed_after_commit?

  before_destroy :generate_webhook_payload, if: :send_deleted_webhook?
  before_destroy :generate_issue_event
  after_commit :queue_webhook_delivery, if: :send_deleted_webhook?, on: :destroy

  after_commit :instrument_deletion, on: :destroy
  after_destroy :clean_up_review
  after_destroy :reparent_replies, unless: :reply?

  after_commit :measure_comment_metrics, on: :create

  after_commit :process_content_references, on: [:create, :update], if: :has_content_references?

  after_commit :subscribe_and_notify,            on: :create
  after_commit :update_subscriptions_and_notify, on: :update

  validate :validate_pull_request_lock, on: :create
  validate :validate_authorized_to_create_content
  validate :ensure_creator_is_not_blocked, on: :create

  validate :validate_review_is_pending, on: :create, unless: :importing?
  validate :validate_reply_relations, on: :create, unless: :importing?

  scope :with_pending_state, -> { pending }
  scope :live, -> { where("position IS NOT NULL") }

  scope :dead, -> { where("position IS NULL") }
  scope :since, -> (time) {
    where("pull_request_review_comments.updated_at >= ?", time)
  }
  scope :sorted_by, -> (order_str, direction) {
    opts = { order: "pull_request_review_comments.created_at DESC"}

    if !order_str.blank?
      case order_str
      when /created/
        opts[:order] = "pull_request_review_comments.created_at"
      when /updated/
        opts[:order] = "pull_request_review_comments.updated_at"
      else
        opts[:order] = "pull_request_review_comments.created_at"
      end

      sort_direction = (direction == "asc" ? " ASC" : " DESC")
      opts[:order] << sort_direction
      opts[:order] << ", pull_request_review_comments.id"
      opts[:order] << sort_direction
    end

    order(opts[:order])
  }
  scope :public_scope, -> { joins(:repository).where("repositories.public = ?", true) }

  scope :cross_review_replies, -> {
    joins(:pull_request_review_thread).where(<<~SQL)
      `pull_request_review_threads`.`pull_request_review_id` != `pull_request_review_comments`.`pull_request_review_id`
    SQL
  }

  # Public: Scope to filter comments visible to the viewer
  #
  # viewer - User viewing the comments (nil means anonymous)
  #
  # Notice: This logic needs to be kept in sync with `#visible_to?`.
  scope :visible_to, -> (viewer) {
    in_viewable_state_for(viewer).
    filter_spam_for(viewer)
  }

  scope :in_viewable_state_for, lambda { |user|
    if user
      clause = <<-SQL
        pull_request_review_comments.user_id = ?
        OR pull_request_review_comments.state = ?
      SQL

      where(clause, user.id, states[:submitted])
    else
      clause = <<-SQL
        pull_request_review_comments.state = ?
      SQL

      where(clause, states[:submitted])
    end
  }

  enum comment_hidden_by: GitHub::MinimizeComment::ROLES

  def self.with_submitted_state
    where("pull_request_review_comments.state = ?", states[:submitted])
  end

  def safe_user
    user || User.ghost
  end

  # Override reading the body to guarantee returning valid utf8 encoded data.
  #
  # See also GitHub::UTF8
  def body
    utf8(read_attribute(:body))
  end

  # Override async_body_context to insert the comment's current line number.
  #
  # Only if the body may contain a suggestion
  def async_body_context
    return super unless body_may_contain_suggestion?

    super.then do |context|
      async_repository.then do |repo|
        async_current_line.then do |current_line|
          if start_line
            context.update(start_line_number: start_line)
          elsif current_line
            context.update(start_line_number: current_line + 1)
          end

          context
        end
      end
    end
  end

  extend GitHub::Encoding
  force_utf8_encoding :diff_hunk, :path

  alias_method :entity, :repository
  alias_method :async_entity, :async_repository

  def issue
    pull_request.try(:issue)
  end

  setup_attachments
  setup_referrer

  # Override NotificationsContent#subscribe_and_notify so that we only
  # trigger the SubscribeAndNotify job for submitted legacy comments.
  def subscribe_and_notify
    super if submitted? && legacy_comment?
  end

  # Override Summarizable#deliver_notifications? for special case
  # behavior for pull_request_reviews.  See https://github.com/github/workflow/issues/426
  # for more information.
  #
  # Returns Boolean indicating if a notifications should send an email.
  def deliver_notifications?
    submitted? && legacy_comment?
  end

  # Public: Should the update_notification_summary happen?
  #
  # Overridding default behavior from Summarizable so that notification
  # summaries only get updated for submitted comments.
  #
  # Returns Boolean-ish
  def update_notification_summary?
    if legacy_comment?
      related_repo_exists_when_defined
    else
      false
    end
  end

  # Override NotificationsContent#update_subscriptions_and_notify
  #
  # When review comments are edited, we want to trigger the
  # SubscribeAndNotifyJob with the comment's review.
  def update_subscriptions_and_notify
    if legacy_comment?
      super
    else
      return unless submitted? && body_previously_changed?

      SubscribeAndNotifyJob.perform_later(
        pull_request_review,
        previous_body: body_previous_change,
        deliver_notifications: ::GitHub.send_notifications?,
      )
    end
  end

  def submitted_at
    async_submitted_at.sync
  end

  def async_submitted_at
    async_pull_request_review.then do |pull_request_review|
      if pull_request_review
        pull_request_review.submitted_at
      else
        created_at
      end
    end
  end

  # Unique identifier for this comment used in email notifications.
  def message_id
    "<#{repository.name_with_owner}/pull/#{pull_request.number}/r#{id}@#{GitHub.urls.host_name}>"
  end

  # Public: Visiblity check for comment
  #
  # viewer - User viewing the threads (nil means anonymous)
  #
  # Notice: This logic needs to be kept in sync with the scope `visible_to`.
  def visible_to?(viewer)
    return true if viewer && viewer.id == user_id

    submitted? && !hide_from_user?(viewer)
  end

  def async_minimizable_by?(actor)
    return Promise.resolve(false) unless actor.present?
    return Promise.resolve(false) if pending?
    return Promise.resolve(true) if actor.site_admin?

    # Users can always minimize their own comments, unless they're restricted
    # by internal App policy...
    return Promise.resolve(true) if unrestricted_actor_minimizing_own_comment?(actor)

    async_repository.then do |repository|
      next false unless repository

      repository.resources.pull_requests.async_writable_by?(actor)
    end
  end

  def async_unminimizable_by?(actor)
    return Promise.resolve(false) if pending?
    return Promise.resolve(false) unless actor.present?
    return Promise.resolve(true) if actor.site_admin?

    async_repository.then do |repo|
      next false unless repo

      repo.async_pushable_by?(actor).then do |is_pushable|
        next is_pushable if minimized_by_maintainer?
        next false unless minimized_by_author?

        is_pushable || actor.id == user_id
      end
    end
  end

  def async_viewer_can_delete?(viewer)
    return Promise.resolve(false) unless viewer
    return Promise.resolve(true) if viewer.site_admin?

    async_viewer_cannot_delete_reasons(viewer).then(&:empty?)
  end

  def async_viewer_cannot_delete_reasons(viewer)
    return Promise.resolve([:login_required]) unless viewer

    Promise.all([
      async_pull_request.then(&:async_issue).then { |issue| issue.async_locked_for?(viewer) },
      async_deletable_by?(viewer),
    ]).then do |locked, deletable|
      errors = []
      errors << :locked if locked
      errors << :insufficient_access unless deletable
      errors
    end
  end

  def async_deletable_by?(viewer)
    return Promise.resolve(false) unless viewer

    async_repository.then do |repository|
      next false unless repository

      async_user.then do |user|
        user ||= User.ghost
        next true if user == viewer

        repository.async_writable_by?(viewer)
      end
    end
  end

  def async_viewer_can_update?(viewer)
    async_viewer_cannot_update_reasons(viewer).then(&:empty?)
  end

  def async_viewer_cannot_update_reasons(viewer)
    return Promise.resolve([:login_required]) unless viewer

    Promise.all([
      async_pull_request.then(&:async_issue).then { |issue| issue.async_locked_for?(viewer) },
      async_editable_by?(viewer),
    ]).then do |locked, editable|
      errors = []
      errors << :locked if locked
      errors << :insufficient_access unless editable
      errors
    end
  end

  # TODO: The logic in this method should probably be moved into the `ContentAuthorizer` framework,
  #       but that is not async-aware yet and does not support `PullRequestReviewComment` objects,
  #       so this is the best place for this right now.
  #
  #       See `IssueComment#async_editable_by?` for a similar check on `IssueComment`s.
  def async_editable_by?(viewer)
    return Promise.resolve(false) unless viewer

    async_repository.then do |repository|
      next false unless repository

      async_user.then do |user|
        next false unless user

        repository.async_owner.then do |owner|
          Promise.all([
            user.async_blocked_by?(owner),
            user.async_blocked_by?(viewer),
            viewer.async_blocked_by?(user),
          ]).then do |user_blocked_by_owner, user_blocked_by_viewer, viewer_blocked_by_user|
            next false if user_blocked_by_owner || user_blocked_by_viewer || viewer_blocked_by_user
            next true if user == viewer

            repository.async_writable_by?(viewer)
          end
        end
      end
    end
  end

  def async_performed_via_integration
    Promise.resolve(nil)
  end

  # Public: return true if the user can delete this comment, falsey otherwise
  #
  # This may be the same as `editable_by?`, but duplicating for clarity and explicitness
  def deleteable_by?(user)
    self.user == user || repository.pushable_by?(user)
  end

  # Determine whether the comment is "live" in the sense that the commented on
  # diff text still exists in the current diff.
  #
  # TODO: once we transitioned to the PRRC#outdated? attribute, use it here
  def live?
    !position.nil?
  end

  def outdated?
    !live?
  end

  # Possible DeprecatedPullRequestReviewThread parent association.
  # Caution: Will only be set if loaded from a DeprecatedPullRequestReviewThread finder.
  attr_accessor :thread

  # Identifier used to group comments in the same thread together. If only this
  # was a simple integer in the database.
  def thread_grouping_id
    # If all modern fields have been recorded,
    if original_base_commit_id && original_start_commit_id && original_end_commit_id
      # and if over a range of commits
      if original_base_commit_id != original_start_commit_id
        [original_start_commit_id, original_end_commit_id, path, original_position]
      else
        [original_end_commit_id, path, original_position]
      end
    # else if legacy comment, group from original head oid.
    else
      [original_commit_id, path, original_position]
    end
  end

  # The lines in the diff hunk text. Memoized because it's used in a number of
  # places, including the views that show excerpts.
  def diff_hunk_lines
    @diff_hunk_lines ||= diff_hunk.scrub!.split("\n")
  end

  # Determine whether the changed lines continue after the diff_hunk_lines. If so,
  # the check for related lines may be unreliable.
  #
  # Returns true if the diff hunk is truncated, false if not.
  def diff_hunk_is_truncated?
    if index = end_of_diff_hunk
      next_line_prefix = diff_entry.text[index + 1]
      next_line_type   = GitHub::Diff::Enumerator.line_type(next_line_prefix)

      next_line_type == :addition || next_line_type == :deletion
    else
      false
    end

  end

  # Iterate over lines in the diff hunk with line type, number, and position
  # tracking.
  #
  # Returns a GitHub::Diff::RelatedLinesEnumerator object whose each method
  # yields a [type, text, position, left, right, current, related] tuple. See
  # Diff::RelatedLinesEnumerator for details on the individual tuple elements.
  def excerpt_line_enumerator(max = 15)
    enumerator = if diff_hunk_is_truncated?
      GitHub::Diff::Enumerator.new(diff_hunk_lines)
    else
      GitHub::Diff::RelatedLinesEnumerator.new(diff_hunk_lines)
    end

    enumerator.lazy.drop(excerpt_starting_offset(max))
  end

  # Internal: The maximum number of lines to return for a multi-line comment excerpt.
  MAX_MULTI_LINE_EXCERPT_LINES = 100

  # The zero-based line offset into the diff hunk text where the condensed
  # excerpt starts. The excerpt includes the last pair of insertions/deletions
  # with surrounding context up to the maximum number of lines specified.
  #
  # max - The maximum number of adjacent insertion/deletion lines to
  #       include in the excerpt. The actual number of lines may be more
  #       because surrounding context is always included.
  #
  # Returns the integer offset into the diff_hunk_links array where the
  #   excerpt starts.
  def excerpt_starting_offset(max = 15)
    if start_position_offset
      return diff_hunk_lines.size - [MAX_MULTI_LINE_EXCERPT_LINES, start_position_offset + 1, diff_hunk_lines.size].min
    end

    offset = diff_hunk_lines.size - 1

    context = false
    modified = 0
    while offset > 0
      line = diff_hunk_lines[offset]
      case line[0]
      when "-", "+"
        if context
          offset += 1
          break
        end
        modified += 1
      when "@"
        break
      else
        context = true if modified > 0
      end
      break if modified > max
      offset -= 1
    end
    [offset, 0].max
  end

  # The condensed diff excerpt as a String.
  def excerpt(max = 15)
    offset = excerpt_starting_offset(max)
    diff_hunk_lines[offset..-1].join("\n")
  end

  # Like excerpt but html escaped.
  def excerpt_html(max = 15)
    EscapeUtils.escape_html_as_html_safe(excerpt(max).to_s)
  end

  # Internal: Is the diff excerpt truncated?
  def excerpt_truncated?
    return false unless start_position_offset

    start_position_offset + 1 > MAX_MULTI_LINE_EXCERPT_LINES
  end

  def set_position_and_current_commit_oid(position, commit_oid)
    write_attribute(:position, position)
    write_attribute(:commit_id, commit_oid)
    write_attribute(:outdated, position.nil?)
  end

  # Internal: The current diff we'll use to calculate positioning information.
  # Default's to the pull request's `current_comments_diff`.
  #
  # Returns a Promise resolving to a GitHub::Diff object.
  def async_current_diff(diff = nil)
    if diff.nil?
      async_pull_request.then(&:async_current_comments_diff)
    else
      Promise.resolve(diff)
    end
  end

  # Public: Given the current diff, recalculate the positioning information.
  # Note: This method does not persist the recalculated positioning information.
  def async_reposition_from_blob_position(diff = nil)
    async_current_diff(diff).then do |current_diff|
      Promise.all([
        async_pull_request,
        async_adjusted_diff_position(current_diff),
        async_adjusted_start_blob_position(current_diff),
      ]).then do |pull_request, adjusted_diff_position, adjusted_start_blob_position|
        # If this was a multiline comment and we can't calculate the start position
        # anymore, then set position to nil so the comment becomes outdated.
        if start_position_offset && adjusted_start_blob_position.nil?
          adjusted_diff_position = nil
        end

        set_position_and_current_commit_oid(adjusted_diff_position, pull_request.head_sha)
      end
    end
  end

  # Internal: Given the current diff, find the adjusted end position of the
  # comment in the diff.
  def async_adjusted_diff_position(diff = nil)
    # just return the "current" diff position for nil (default) diff
    return Promise.resolve(position) if diff.nil?

    Promise.all([
      async_adjusted_blob_position(diff),
      async_adjusted_path(diff),
    ]).then do |adjusted_blob_position, adjusted_path|
      diff.position_for(adjusted_blob_position, adjusted_path, left_blob)
    end
  end

  # Internal: Given the current diff, find the adjusted path for the comment.
  def async_adjusted_path(diff)
    async_current_diff(diff).then do |current_diff|
      async_position_data.then do |position_data|
        position_data.adjustment_parameters(current_diff)[:destination][:path].dup.force_encoding(Encoding::UTF_8)
      end
    end
  end

  # Public: Returns the comment's adjusted end position in the blob.
  #
  # Returns a Promise resolving to an Integer or nil.
  def async_adjusted_blob_position(diff = nil)
    async_current_diff(diff).then do |diff|
      next nil unless diff

      async_position_data.then do |position_data|
        position_data.async_adjusted_blob_position(diff).then do |adjusted_blob_position|

          # if one or both commits are corrupted or missing, etc we revert to just
          # matching the diff hunk text against the vanilla diff using the loader
          if adjusted_blob_position == :bad
            destination_path = blob_path || path
            Platform::Loaders::LegacyDiffPosition.load(diff, destination_path, diff_match_text).then do |adjusted_blob_position|
              adjusted_position_dogstats(adjusted_blob_position, algorithm: :excerpt_match)
              adjusted_blob_position
            end
          else
            adjusted_position_dogstats(adjusted_blob_position, algorithm: :blob_offsets)
            adjusted_blob_position
          end
        end
      end
    end
  end

  alias async_current_line async_adjusted_blob_position

  # Public: Returns the comment's adjusted start position in the blob.
  #
  # Returns a Promise resolving to an Integer or nil.
  def async_adjusted_start_blob_position(diff = nil)
    return Promise.resolve(nil) unless start_position_offset

    async_start_line(diff).then do |start_line|
      next nil unless start_line

      start_line.deletion? ? start_line.blob_left : start_line.blob_right
    end
  end

  # Public: Returns the diff side of the comment's starting line.
  #
  # Returns a Promise resolving to a Symbol :right or :left, or nil.
  def async_start_side(diff = nil)
    return Promise.resolve(nil) unless start_position_offset

    async_start_line(diff).then do |start_line|
      next nil unless start_line

      start_line.deletion? ? :left : :right
    end
  end

  # Internal: Finds the comment's start line in the diff.
  #
  # Returns a Promise resolving to a GitHub::Diff::Line or nil.
  def async_start_line(diff = nil)
    async_adjusted_selected_lines(diff).then do |lines|
      lines.first
    end
  end

  # Internal: Finds the comment's end line in the diff.
  #
  # Returns a Promise resolving to a GitHub::Diff::Line or nil.
  def async_end_line(diff = nil)
    async_adjusted_selected_lines(diff).then do |lines|
      lines.last
    end
  end

  def async_adjusted_selected_lines(diff = nil)
    return Promise.resolve([]) unless start_position_offset

    async_current_diff(diff).then do |current_diff|
      next [] unless current_diff

      async_adjusted_diff_position(current_diff).then do |diff_position|
        next [] unless diff_position

        async_adjusted_path(current_diff).then do |path|
          selected_lines = selected_lines_for(diff_position, path, current_diff)

          # Does the extracted diff hunk, from the end position to the start
          # position, match the content of the provided diff across the same line range?
          next [] unless selected_lines.map(&:text) == diff_hunk_lines.last(start_position_offset + 1)

          selected_lines
        end
      end
    end
  end

  def async_selection_contains_deletions
    return Promise.resolve(true) if left_blob?

    async_adjusted_selected_lines.then do |lines|
      lines.any? { |c| c.type == :deletion }
    end
  end

  def selection_contains_deletions?
    async_selection_contains_deletions.sync
  end

  # Public: The adjusted 1-indexed blob position of the first of the
  # commented-on lines of a mutli-line comment.
  #
  # Returns an integer, or `nil` for single-line or outdated comments.
  def start_line
    async_start_line.then do |line|
      line.current if line
    end.sync
  end

  # Public: the side of the diff to which the first of commented lines applies.
  #
  # Returns `:left` for deletions, `:right` for additions and context, and `nil`
  # for single-line comments.
  def start_side
    return nil unless start_position_offset
    diff_line = original_start_diff_line
    return nil unless diff_line # diff was likely truncated
    diff_line.type == :deletion ? :left : :right
  end

  # Internal: The original 1-indexed blob position of the first of the
  # commented-on lines of a mutli-line comment.
  #
  # Returns a promise resolving to an integer, or `nil` for single-line comments.
  def async_original_start_line
    async_position_data.then(&:original_start_line)
  end

  # Public: The original 1-indexed blob position of the first of the
  # commented-on lines of a mutli-line comment.
  #
  # Returns an integer, or `nil` for single-line comments.
  def original_start_line
    async_original_start_line.sync if start_position_offset.present?
  end

  # Internal: The adjusted 1-indexed blob position of a single-line comment, or
  # the last of the commented-on lines of a mutli-line comment.
  #
  # Returns a promise resolving to an integer, or nil for an outdated comment.
  def async_line
    async_adjusted_blob_position.then do |line|
      line + 1 if line && !outdated?
    end
  end

  # Public: The adjusted 1-indexed blob position of a single-line comment, or
  # the last of the commented-on lines of a mutli-line comment.
  #
  # Returns an integer.
  def line
    async_line.sync
  end

  # Internal: The original 1-indexed blob position of a single-line comment, or
  # the last of the commented-on lines of a mutli-line comment.
  #
  # Returns a promise resolving to an an integer.
  def async_original_line
    async_position_data.then(&:line)
  end

  # Public: The original 1-indexed blob position of a single-line comment, or
  # the last of the commented-on lines of a mutli-line comment.
  #
  # Returns an integer.
  def original_line
    async_original_line.sync
  end

  # Public: the side of the diff to which a single-line comment or the last of
  # the commented lines of a multi-line comment applies.
  #
  # Returns `:left` for deletions or `:right` for additions and context.
  def side
    left_blob? ? :left : :right
  end

  # Internal: The selected diff lines for the comment in the provided diff.
  #
  # Returns an Array of GitHub::Diff::Line objects, or an empty Array.
  def selected_lines_for(position, path, diff)
    return [] unless start_position_offset

    entry = diff[path]
    return [] unless entry

    range_start = position - start_position_offset
    return [] unless range_start > 0

    entry.enumerator.each_with_object([]) do |diff_line, lines|
      break lines if diff_line.position > position

      if (range_start..position).include?(diff_line.position)
        lines << diff_line
      end
    end
  end

  # Public: The 0-based offset into the blob for the comment's start position (multi-line only).
  #
  # Returns an Integer or nil.
  def original_start_blob_position
    diff_line = original_start_diff_line
    return nil unless diff_line

    diff_line.current - 1
  end

  # Internal: The original start diff line, extracted from the diff_hunk column.
  #
  # Returns a GitHub::Diff::Line object or nil.
  def original_start_diff_line
    return nil unless start_position_offset

    start_line_index = -(start_position_offset + 1)
    GitHub::Diff::Enumerator.new(diff_hunk_lines).to_a[start_line_index]
  end

  def original_selection
    if start_position_offset.present?
      start_line_index = -(start_position_offset + 1)
      diff_hunk_lines[start_line_index..-1] || []
    else
      Array.wrap(diff_hunk_lines.last)
    end
  end

  # Public: Comments usually need to have their blob positions updated after force pushes
  def needs_position_update?

    # we are only updating previously blob positioned comments for now.
    return false unless has_blob_positioning_data?

    # never reposition if the current position is still in the PR's list of commits
    return false if pull_request.changed_commit_oids.include?(blob_commit_oid)

    if left_blob
      # If this is a comment on the left blob, it's ok if we reference the merge_base, even though
      # it isn't part of the changes.
      return false if pull_request.merge_base == blob_commit_oid

      # if the branch's merge base is lost due to a weird rebase onto a branch with no common ancestors,
      # there is nothing we can do to reposition comments on removals. Comments continue to display as long
      # as the blob_commit_oid is not GC'd and the line is still present, but removed in the displayed diff.
      if pull_request.merge_base.nil?
        GitHub.dogstats.increment("pull_request_review_comments.positioning_error", tags: ["no_merge_base"])
        return false
      end
    end

    true
  end

  # Public: Updates the blob positioning columns to valid values for the PR.
  #   Only update positioning if we found a new position. Otherwise, continue
  #   pointing to the potentially orphaned commit. Maybe a future force push will
  #   allow us to reposition this comment.
  def update_position_data
    diff = pull_request.historical_comments_diff
    data = position_data.adjusted_data(diff)
    write_position_data(data)

  rescue PullRequestReviewComment::BadDiffHunkError

    # Not sure how this would ever happen. But if it does, let's log it and not try to
    # do this again by putting some default values in so that blob_position_lost? is true.
    Failbot.report(e, comment_id: id)
    write_attribute :blob_commit_oid, pull_request.head_sha
    write_attribute :blob_path, path
  end

  # Internal: Update the blob columns according to the provided position data
  #
  # position_data - An instance of PullRequestReviewComment::AbstractPositionData
  def write_position_data(position_data)
    return if position_data.nil?

    # write this first as the left_blob? method is affected by the presence of the other values
    write_attribute :left_blob,       position_data.left_blob?

    write_attribute :blob_position,   position_data.blob_position

    # Use the destination path, which will be the a path for deletions, the b path for all others
    write_attribute :blob_path,       position_data.path

    # The destination commit will be commit 1 for a deletion, commit 2 for an addition
    write_attribute :blob_commit_oid, position_data.commit_oid
  end

  def position_for_diff(diffs, current)
    return nil if diffs.ignore_whitespace?

    if current
      position
    else
      calculate_position_on_diff(diffs)
    end
  end

  def calculate_position_on_diff(diffs)
    new_position = position

    if diff = diffs[path]
      diff_text = diff.text.to_s.b + "\n"
      match_text = diff_match_text.b
      if offset = diff_text.index(match_text)
        lines = diff_text[0, offset + match_text.size].split("\n")
        new_position = lines.size - 1
      else
        # original diff text not found in new diff. comment is dead.
        new_position = nil
      end
    else
      # file was removed
      new_position = nil
    end

    new_position
  end

  def has_blob_positioning_data?
    !blob_position.nil? && !blob_path.nil? && !blob_commit_oid.nil?
  end

  # Updates the row with the recalculated position and commit_id,
  # without calling #save, to avoid touching the parent PullRequest record.
  #
  # See github/github#8729
  def save_positions(skip_blob_fields: false)
    if has_changes_to_save?
      # Some validations are expensive and this method is called in bulk.
      #
      # Additionally, validation flow in this model for standard create/update
      # paths is convoluted for uhh, reasons.
      #
      # Therefore, just do a quick manual validation of the attributes being changed here.
      unless position.nil? || position.is_a?(Integer)
        errors.add(:position, "must be nil or an Integer")
      end

      unless GitRPC::Util.valid_full_sha1?(commit_id)
        errors.add(:commit_id, "must be a 40 character SHA1")
      end

      if errors.any?
        Failbot.report(ActiveRecord::RecordInvalid.new(self))
        return
      end

      data = {
        position:   position,
        outdated:   outdated,
        commit_id:  commit_id,
        updated_at: current_time_from_proper_timezone,
      }

      unless skip_blob_fields
        data.merge!({
          blob_commit_oid:  blob_commit_oid,
          blob_path:        blob_path,
          blob_position:    blob_position,
          left_blob:        left_blob,
        })
      end

      update_columns(data)
    end
  end

  # Text to locate in the changed diff to reposition the comment. The current
  # implementation uses the last five lines of the original diff hunk.
  #
  # The ending newline is to keep from matching when the last line has
  # text added to it.
  def diff_match_text
    buf = []
    pos = diff_hunk_lines.size - 1
    while pos > 0
      line = diff_hunk_lines[pos]
      break if line[0] == "@"
      break if buf.size >= 5
      buf.unshift(line)
      pos -= 1
    end
    buf.join("\n") + "\n"
  end

  # Absolute permalink URL for this pull request.
  #
  # include_host - Turn off the `GitHub.url` host in the url. (default true)
  #                pull_request_review_comment.permalink(include_host: false) => `/github/github/pull/4#discussion_r123`
  #
  def permalink(include_host: true)
    "#{pull_request.permalink(include_host: include_host)}##{CommentsHelper::DISCUSSION_DOM_ID_PREFIX}#{id}"
  end
  alias url permalink

  def async_path_uri
    return @async_path_uri if defined?(@async_path_uri)

    @async_path_uri = async_pull_request.then(&:async_path_uri).then do |path_uri|
      path_uri = path_uri.dup
      path_uri.fragment = "#{CommentsHelper::DISCUSSION_DOM_ID_PREFIX}#{id}"
      path_uri
    end
  end

  def async_original_diff_path_uri
    return @async_original_diff_path_uri if defined?(@async_original_diff_path_uri)

    @async_original_diff_path_uri = async_original_pull_request_comparison.then do |pull_request_comparison|
      next unless pull_request_comparison

      async_pull_request.then(&:async_path_uri).then do |path_uri|
        path_uri = path_uri.dup

        path_uri.path += if pull_request_comparison.range?
          "/files/#{pull_request_comparison.start_commit.oid}..#{pull_request_comparison.end_commit.oid}"
        else
          "/files/#{pull_request_comparison.end_commit.oid}"
        end

        path_uri.fragment = "#{CommentsHelper::COMMIT_COMMENT_DOM_ID_PREFIX}#{id}"
        path_uri
      end
    end
  end

  def async_current_diff_path_uri
    return @async_current_diff_path_uri if defined?(@async_current_diff_path_uri)
    return @async_current_diff_path_uri = Promise.resolve(nil) if outdated?

    async_pull_request.then(&:async_path_uri).then do |path_uri|
      path_uri = path_uri.dup
      path_uri.path += "/files"
      path_uri.fragment = "#{CommentsHelper::COMMIT_COMMENT_DOM_ID_PREFIX}#{id}"
      path_uri
    end
  end

  def async_update_path_uri
    return @async_update_path_uri if defined?(@async_update_path_uri)

    @async_update_path_uri = async_pull_request.then(&:async_path_uri).then do |path_uri|
      path_uri = path_uri.dup
      path_uri.path += "/review_comment/#{id}"
      path_uri
    end
  end

  def last_modified_at
    @last_modified_at ||= last_modified_with :user
  end

  def subscribe_mentioned(mentions = mentioned_users, author = user)
    issue.subscribe_mentioned(mentions, author)
  end

  def unsubscribable_users(users)
    issue.unsubscribable_users(users)
  end

  def diff_entry
    return @diff_entry if defined?(@diff_entry)

    @diff_entry = load_diff_entry
  end

  def async_diff
    return @async_diff if defined?(@async_diff)

    @async_diff = async_original_pull_request_comparison.then do |pull_request_comparison|
      next unless pull_request_comparison

      Platform::Loaders::PullRequestDiff.load(pull_request_comparison.pull,
        start_commit_oid: pull_request_comparison.start_commit.oid,
        end_commit_oid: pull_request_comparison.end_commit.oid,
        base_commit_oid: pull_request_comparison.base_commit.oid,
      )
    end
  end

  def async_diff_entry
    return @async_diff_entry if defined?(@async_diff_entry)

    @async_diff_entry = async_diff.then do |diff|
      next unless diff
      @diff_entry = diff[path]
    end
  end

  def current_diff_entry
    return @current_diff_entry if defined?(@current_diff_entry)
    @current_diff_entry = load_current_diff_entry
  end

  def disable_diff_entry
    @diff_entry = nil
  end

  def compute_base_commit_id(head_commit_id)
    pull_request.compute_base_commit_id(head_commit_id)
  end

  # Public: Prepare the diff for this comment specifically. Sets the path so we support diffs
  #         beyond the max files limit, and the overrides the lines and byte limit in the case
  #         the user clicked "load diff" for a skipped file. Should ONLY be used for comment
  #         create methods, when it is ok for the diff to only be on one file.
  def preload_original_diff
    return if original_pull_request_comparison.nil?

    diff = original_pull_request_comparison.diffs
    diff.add_path(path)
    diff.maximize_single_entry_limits!
  rescue GitRPC::Error => e
    # preload just doesn't happen in this case. Optimizations are not completed.
  end

  def load_diff_entry
    return unless comparison = original_pull_request_comparison
    diffs = comparison.diffs
    return unless diffs.load_diff
    diffs[path]
  end

  def load_current_diff_entry
    diffs = pull_request.build_comparison(head_commit_oid: commit_id).diffs
    return if !diffs.available?
    diffs[path]
  end

  # Public: Get PR comparison for original comment location.
  #
  # Returns PullRequest::Comparison or nil if the comparison objects can't be found.
  def original_pull_request_comparison
    async_original_pull_request_comparison.sync
  end

  def async_original_pull_request_comparison
    return @async_original_pull_request_comparison if defined? @async_original_pull_request_comparison

    @async_original_pull_request_comparison = async_pull_request.then do |pull_request|
      pull_request.async_historical_comparison.then do
        # Use recorded original_base_commit_id if available, otherwise fallback to
        # guessing by recomputing the merge base from the current PR base.
        base_commit_oid  = original_base_commit_id  || compute_base_commit_id(original_commit_id)
        start_commit_oid = original_start_commit_id || base_commit_oid
        end_commit_oid   = original_end_commit_id   || original_commit_id

        pull_request.async_pull_comparison(
          start_oid: start_commit_oid,
          end_oid: end_commit_oid,
          base_oid: base_commit_oid,
        )
      end
    end
  end

  def async_original_diff
    return @async_original_diff if defined? @async_original_diff

    @async_original_diff = async_pull_request.then do |pull_request|
      original_commit_oid = original_commit_id       || commit_id
      base_commit_oid     = original_base_commit_id  || compute_base_commit_id(original_commit_oid)
      start_commit_oid    = original_start_commit_id || base_commit_oid
      end_commit_oid      = original_end_commit_id   || original_commit_oid

      pull_request.async_diff(
        start_commit_oid: start_commit_oid,
        end_commit_oid: end_commit_oid,
        base_commit_oid: base_commit_oid,
      )
    end
  end

  def async_commit
    async_load_commit(oid: commit_id)
  end

  def async_original_commit
    async_load_commit(oid: original_commit_id)
  end

  private def async_load_commit(oid:)
    return Promise.resolve(nil) unless [commit_id, original_commit_id].include?(oid)

    async_pull_request.then do |pull_request|
      Promise.all([
        pull_request.async_repository,
        pull_request.async_head_repository,
        pull_request.async_base_repository,
      ]).then do |repository, head_repository, base_repository|
        Platform::Loaders::GitObject.load(repository, oid, alternate_repositories: [head_repository, base_repository], expected_type: :commit)
      end
    end
  end

  def backfill_original_range_commit_ids!
    pull_comparison = original_pull_request_comparison
    self.original_base_commit_id  = pull_comparison.base_commit_oid
    self.original_start_commit_id = pull_comparison.start_commit_oid
    self.original_end_commit_id   = pull_comparison.end_commit_oid
    save!
  end

  # Collect conditions under which we don't bother checking the content
  # of this Issue for spam.
  def skip_spam_check?
    repository.nil?          ||   # If the repo is gone
    repository.private?      ||   # Leave private repos alone.
    user.nil?                ||
    !user.can_be_flagged?    ||   # No point in checking these,
    user.spammy?             ||   # since nothing will change.
    repository.member?(user)      # Don't bother repo members.
  end

  def check_for_spam?
    persisted? && !skip_spam_check?
  end

  # Check the comment for spam
  #
  # options - currently unused
  def check_for_spam(options = {})
    return if skip_spam_check?

    if reason = GitHub::SpamChecker.test_comment(self)
      GitHub.dogstats.increment "spam.flagged", tags: ["spam_target:pull_request_review_comment"]

      GitHub::SpamChecker.notify "Pushing #{user.login} for review: #{reason}"
      GlobalInstrumenter.instrument(
        "add_account_to_spamurai_queue",
        {
          account_global_relay_id: user.global_relay_id,
          additional_context: "RESQUE_CHECK_FOR_SPAM_PULL_REQUEST_REVIEW_COMMENT",
          origin: :RESQUE_CHECK_FOR_SPAM_PULL_REQUEST_REVIEW_COMMENT,
          queue_global_relay_id: SpamQueue::POSSIBLE_SPAMMER_QUEUE_GLOBAL_RELAY_ID,
        },
      )
    end
  end

  # Internal: associate a PullRequestReview with this comment, and make sure the
  # state is in sync.  NOTE: This does not save the comment - the caller is
  # responsible for that.
  #
  # Returns the PullRequestReviewComment
  def add_review_for(user:, pull_request:, head_sha:)
    review = pull_request.reviews.with_pending_state.where(user_id: user.id).first ||
             pull_request.reviews.with_pending_state.create!(user_id: user.id, head_sha: head_sha)
    review.review_comments << self
    self.pull_request_review = review
    self.state = :pending
    self
  end

  # Internal: save! and submit this comment.  This will raise if
  # the save on the comment fails for some reason.
  #
  # Returns the PullRequestReviewComment
  def submit!
    raise "Cannot submit a comment twice!" if submitted?
    self.state = :submitted
    self.save!
    self
  end

  # Public: Is this PullRequestReviewComment a reply? Note that we only
  # began tracking reply_to_id around August 2016.  Older comments
  # were "replies" only in the sense that they were created on the same
  # file / line / position on a PR.
  #
  # Returns a Boolean
  def reply?
    reply_to_id.present?
  end

  # Public: Is this PullRequestReviewComment a legacy comment?
  #
  # We classify a legacy comment as any comment that DOES NOT have
  # a PullRequestReview associated with it.
  def legacy_comment?
    pull_request_review_id.nil?
  end

  def readable_by?(actor)
    async_readable_by?(actor).sync
  end

  def async_readable_by?(actor)
    async_pull_request.then do |pull_request|
      next false unless pull_request
      pull_request.async_readable_by?(actor)
    end
  end

  # Public: The positioning data for this comment.
  #
  # Returns an instance of a subclass of PullRequestReviewComment::AbstractPositionData
  # representing the positioning data of this comment.
  def position_data
    async_position_data.sync
  end

  def async_position_data
    return @async_position_data if defined?(@async_position_data)

    @async_position_data = if has_blob_positioning_data?
      PositionData.async_from_comment(self)
    else
      LegacyPositionData.async_from_comment(self)
    end
  end

  def write_end_position_data
    return if reply?
    return unless end_position_data

    write_attribute(:commit_id, end_position_data.diff.sha2)
    write_attribute(:original_commit_id, end_position_data.diff.sha2)
    write_attribute(:original_base_commit_id, end_position_data.diff.base_sha || end_position_data.diff.sha1)
    write_attribute(:original_start_commit_id, end_position_data.diff.sha1)
    write_attribute(:original_end_commit_id, end_position_data.diff.sha2)
    begin
      write_attribute(:blob_commit_oid, end_position_data.commit_oid)
      write_attribute(:left_blob, end_position_data.left_blob?)
      write_attribute(:blob_path, end_position_data.path)
      write_attribute(:blob_position, end_position_data.blob_position)
    rescue PullRequestReviewComment::AbstractPositionData::InvalidDiffError,
      PullRequestReviewComment::AbstractPositionData::InvalidLineError,
      GitRPC::Error
      # Swallow error; we're just trying to set the attributes here, not triage errors
    end

    begin
      write_attribute(:path, end_position_data.comment_path)
      write_attribute(:diff_hunk, end_position_data.diff_hunk_text)
      write_attribute(:position, end_position_data.diff_position)
      write_attribute(:original_position, end_position_data.diff_position)
    rescue PullRequestReviewComment::AbstractPositionData::InvalidDiffError,
      PullRequestReviewComment::AbstractPositionData::InvalidLineError,
      GitRPC::Error
      # Swallow these errors separately; depending whether the end_position_data is blob
      # or diff based, one batch is likely to fail when the other succeeds but we don't know which.
    end
  end

  def current_line
    async_adjusted_blob_position.sync
  end

  def current_line_range
    first_line = current_line - start_position_offset.to_i
    (first_line..current_line)
  end

  def notifications_author
    user
  end

  alias_method :notifications_list, :repository

  # Tell notifications about our thread structure.
  alias_method :notifications_thread, :issue

  def notify_socket_subscribers
    data = {
        timestamp: Time.now.to_i,
        wait: default_live_updates_wait,
        reason: "pull request review comment ##{id} updated",
        gid: global_relay_id,
    }
    channel = GitHub::WebSocket::Channels.pull_request_timeline(pull_request)
    GitHub::WebSocket.notify_pull_request_channel(pull_request, channel, data)
  end


  def build_reply(review:, body:, user: review.user)
    self.class.new(
      body: body,
      user: user,
      pull_request: pull_request,
      pull_request_review: review,
      pull_request_review_thread: pull_request_review_thread,
      reply_to_id: id,
    )
  end

  # Overrides PushNotifiable#push_notification_subtitle
  def push_notification_subtitle
    pull_request.issue.push_notification_subtitle
  end

  # Override Referrer#track_references_in_background? so that references
  # are created in a background job.
  def track_references_in_background?
    true
  end

  # Public: Checks spamminess of associated pull request or pull request review.
  #
  # Returns a Boolean.
  def belongs_to_spammy_content?
    pull_request&.spammy? || pull_request_review&.spammy?
  end

  protected

  def ensure_state_is_set
    self.state ||= :pending
  end

  def validate_end_position_data
    return if reply?
    return unless end_position_data

    message = "is not part of the pull request"

    error_promise = async_pull_request.then(&:async_historical_comparison).then do |comparison|
      Promise.all([
        comparison.async_covers_commit?(end_position_data.diff.sha1),
        comparison.async_covers_commit?(end_position_data.diff.sha2),
      ]).then do |start_present, end_present|
        errors.add(:start_commit_oid, message) unless start_present
        errors.add(:end_commit_oid, message) unless end_present

        if base_commit_oid = end_position_data.diff.base_sha
          errors.add(:base_commit_oid, "could not be found") unless GitRPC::Util.valid_full_sha1?(base_commit_oid)

          comparison.async_load_commits([base_commit_oid]).then do |commits|
            errors.add(:base_commit_oid, "could not be found") if commits.first.nil?
          end
        end
      end
    end

    error_promise.sync

    return errors unless errors.empty?

    begin
      if end_position_data.diff_entry.too_big?
        errors.add(:path, "diff too large")
      end
    rescue AbstractPositionData::InvalidDiffError => e
      errors.add :path, e.message
    end

    return unless errors.empty?

    valid_position = begin
      end_position_data.adjustment_blob_position_valid?
    rescue AbstractPositionData::InvalidDiffError
      false
    end

    unless valid_position
      if end_position_data.is_a?(LegacyPositionData)
        errors.add(:position, "is invalid")
      else
        errors.add(:line, "required and an integer greater than zero")
      end
    end

    if valid_position && !end_position_data.diff_position
      if end_position_data.is_a?(LegacyPositionData)
        errors.add(:position, "is invalid")
      else
        errors.add(:line, "must be part of the diff")
      end
    end
  end

  def write_start_position_offset
    return unless start_position_data

    write_attribute(:start_position_offset, start_position_data.calculate_start_position_offset(end_position_data))
  end

  def validate_start_position_offset
    return unless start_position_data

    if start_position_offset.nil?
      errors.add(:start_line, "must be part of the same hunk as the line.")
    elsif start_position_offset >= diff_hunk_lines.length
      # We only extract the diff hunk up to the first hunk header, so if we've
      # computed an offset that's equal to or greater than the extracted lines,
      # it's because it crosses a hunk header boundary.
      errors.add(:start_line, "must be part of the same hunk as the line.")
    elsif start_position_offset < 1
      errors.add(:start_line, "must precede the end line.")
    end
  end

  def validate_review_is_pending
    return if pull_request_review.nil? || pull_request_review.pending?
    errors.add(:pull_request_review_id, "must be pending")
  end

  def validate_reply_relations
    return unless reply?
    return unless pull_request_review_thread.pull_request_review

    return if pull_request_review_thread.published?

    same_author = user_id == pull_request_review_thread.pull_request_review.user_id
    return if same_author

    messages = []
    messages << "must be published" unless pull_request_review_thread.published?
    messages << "must have review with same author as comment" unless same_author

    errors.add(:pull_request_review_thread_id, messages.join(" or "))
  end

  # Protected: The commit_id and position are updated when new commits are pushed to the
  # pull request. Maintain the original commit_id and position so we can locate
  # the original diff hunk if necessary.
  #
  # Fired before_validation on_create
  def initialize_original_attribute_values
    write_attribute :original_commit_id, commit_id if original_commit_id.nil?
    write_attribute :original_base_commit_id, compute_base_commit_id(original_commit_id) if original_base_commit_id.nil?
    write_attribute :original_start_commit_id, original_base_commit_id if original_start_commit_id.nil?
    write_attribute :original_end_commit_id, original_commit_id if original_end_commit_id.nil?
    write_attribute :original_position, position if original_position.nil?
  end

  def copy_position_data_for_reply
    return unless reply?

    self.blob_commit_oid = in_reply_to.blob_commit_oid
    self.blob_path = in_reply_to.blob_path
    self.blob_position = in_reply_to.blob_position
    self.commit_id = in_reply_to.commit_id
    self.diff_hunk = in_reply_to.diff_hunk
    self.left_blob = in_reply_to.left_blob
    self.original_base_commit_id = in_reply_to.original_base_commit_id
    self.original_commit_id = in_reply_to.original_commit_id
    self.original_end_commit_id = in_reply_to.original_end_commit_id
    self.original_position = in_reply_to.original_position
    self.original_start_commit_id = in_reply_to.original_start_commit_id
    self.path = in_reply_to.path
    self.position = in_reply_to.position
    self.start_position_offset = in_reply_to.start_position_offset
  end

  # Protected: initialize blob positioning attributes.  Fired before_create
  def initialize_position_data
    self.end_position_data = position_data unless end_position_data
  end

  def subscribe_to_issue
    subscribe(user, :comment)
  end

  # Protected: Tries to ensure that this comment has an associated thread.
  #
  # This method is responsible for making sure that new review comments
  # that are not replies to existing comments and don't have a thread set
  # will be created in their own, new thread.
  #
  # Replies will either copy the thread id from the replied to comment,
  # or will get a thread assigned once we perform the back-fill transition.
  #
  # After the transition to back-fill `pull_request_review_thread_id`
  # is executed, we'll want to revisit this method.
  def initialize_review_thread
    if reply_to_id.nil? && (pull_request_review_thread.nil? && pull_request_review_thread_id.nil?)
      self.pull_request_review_thread = PullRequestReviewThread.new({
        pull_request_id:        pull_request_id,
        pull_request_review_id: pull_request_review_id,
      })
    end
  end

  # Internal: overrides the Referrer to be the issue.
  alias_method :referrer, :issue

  # Public: Gets the RollupSummary for this Comment's thread.
  # See Summarizable.
  #
  # Returns Newsies::Response instance.
  def get_notification_summary
    return unless issue
    list = Newsies::List.new("Repository", repository_id)
    GitHub.newsies.web.find_rollup_summary_by_thread(list, issue)
  end

  # The user who performed the action as set in the GitHub request context. If the context doesn't
  # contain an actor, fallback to the safe_user which we know exists.
  def modifying_user
    @modifying_user ||= (User.find_by_id(GitHub.context[:actor_id]) || safe_user)
  end

  def event_payload
    {
      comment: self,
      spammy: spammy?,
      submitted: submitted?,
      pull_request: pull_request.id,
      allowed: pull_request.repository.permit?(user, :write),
      body: body,
    }
  end

  def instrument_failed_validation
    instrument :validation_failed
  end

  def instrument_creation
    instrument :create

    GlobalInstrumenter.instrument("pull_request_review_comment.create", {
      actor: user,
      pull_request: pull_request,
      issue: issue,
      pull_request_creator: pull_request.user,
      repository: repository,
      repository_owner: repository.owner,
      pull_request_review: pull_request_review,
      review_comment: self,
    })
  end

  public def instrument_submission
    instrument :submission
  end

  def instrument_update
    instrument :update, changes: {old_body: previous_changes[:body].try(:first), body: body}, actor: user_content_edits.last.try(:editor)

    GlobalInstrumenter.instrument("pull_request_review_comment.update", {
      actor: user,
      pull_request: pull_request,
      repository: repository,
      review_comment: self,
      pull_request_review: pull_request_review,
    })
  end

  def instrument_deletion
    # If the destruction is happening as part of repo archiving
    # then there will be a lot of nil references during the destruction
    # and there will be no useful instrumentation data to store.
    return if repository.nil? || pull_request.nil?

    instrument :delete, repo: repository, actor: modifying_user, author: user

    GlobalInstrumenter.instrument("pull_request_review_comment.delete", {
      actor: user,
      pull_request: pull_request,
      repository: repository,
      review_comment: self,
      pull_request_review: pull_request_review,
    })
  end

  def clean_up_review
    pull_request_review.destroy_if_empty if pull_request_review
  end
  private :clean_up_review

  # Private: Should we send deletion webhooks?
  private def send_deleted_webhook?
    if spammy? || pending?
      false
    else
      true
    end
  end

  # Private: Serializes this PullRequestReviewComment as a webhook payload for any apps
  # that listen for Hook::Event::PullRequestReviewCommentEvents. Under normal circumstances
  # we deliver webhook events using instrumentation, but this must be called as
  # a before_destroy and uses Hook::Event#generate_payload_and_deliver_later to
  # serialize the webhook payload before the record becomes unavailable.
  #
  # Returns nothing.
  def generate_webhook_payload
    if modifying_user&.spammy?
      @delivery_system = nil
      return
    end

    event = Hook::Event::PullRequestReviewCommentEvent.new(action: :deleted, pull_request_review_comment_id: id, actor_id: modifying_user.id, triggered_at: Time.now)
    @delivery_system = Hook::DeliverySystem.new(event)
    @delivery_system.generate_hookshot_payloads
  end

  # Private: Queue the payloads generated above for delivery to Hookshot.
  def queue_webhook_delivery
    unless defined?(@delivery_system)
      raise "`generate_webhook_payload' must be called before `queue_webhook_delivery'"
    end

    @delivery_system&.deliver_later
  end

  def generate_issue_event
    if issue && modifying_user != user
      issue.events.create \
        event: "comment_deleted",
        actor: modifying_user,
        subject: user
    end
  end

  # Private: new review comments that are not replies to another comment get created with
  # the commit_id, path and position of the diff hunk that the user was looking
  # at when they wrote the comment. that's great - we need that info to look up
  # the hunk that the user was talking about.
  #
  # the problem is that the head of the pull request might now be different
  # than what was in the users browser when they made the comment. we need to
  # check if the head_sha of the pull has changed and recalculate the comment
  # position if so, making sure that it doesn't change yet again underneath
  # us while we do that.
  #
  # LOCK IN SHARE MODE allows the row to be read by other connections but not
  # updated or deleted. This means that if the PR gets pushed to while we're in
  # here and tries to update the head_sha, it will wait for us to finish.
  def ensure_synced_with_pull
    outdated = false
    GitHub.dogstats.time("pull_request_review_comment", tags: ["action:ensure_synced_with_pull"]) do
      transaction do
        PullRequest.github_sql.new(<<-SQL, pull_request_id: pull_request_id).run
          SELECT * FROM pull_requests WHERE id = :pull_request_id LOCK IN SHARE MODE
        SQL
        self.pull_request = PullRequest.find(pull_request_id)
        position_was = position

        begin
          async_reposition_from_blob_position.sync
        rescue PullRequestReviewComment::BadDiffHunkError => e
          # This is only caused by bad legacy test setups so far, but let's log to
          # haystack just in case.
          Failbot.report(e, comment_id: id)
        end

        outdated = position_was.present? && position.nil?
        save_positions(skip_blob_fields: true)
      end
    end
    GitHub.dogstats.increment("pull_request.sync.position_outdated", tags: ["blob_position:false"]) if outdated
  end
  private :ensure_synced_with_pull

  def truncate_diff_hunk
    if self.diff_hunk.bytesize > MYSQL_UNICODE_BLOB_LIMIT
      self.diff_hunk.force_encoding("binary")
      self.diff_hunk = self.diff_hunk[0...MYSQL_UNICODE_BLOB_LIMIT]
      GitHub.dogstats.increment("pull_request_review_comments", tags: ["action:diff_hunk_truncated"])
    end
  end

  # Find the index in the diff entry text where this diff hunk ends. This may
  # not be available if the diff entry can't be loaded or if the base branch
  # has been force-pushed.
  #
  # Returns the Integer string index, or nil if the index can't be determined.
  def end_of_diff_hunk
    return nil unless diff_entry && diff_entry.text

    diff_entry_text = diff_entry.text
    index = -1
    (original_position + 1).times do
      index = diff_entry_text.index("\n", index + 1)
      return if index.nil?
    end
    index
  end

  # Validation to see if commenting is allowed based on pull request lock
  # Only someone allowed to push to the repository can comment on a locked pull request
  # (unlocked pull requsts have no such restriction)
  #
  # Returns true if comment is allowed, adds an error otherwise
  def validate_pull_request_lock
    return true unless pull_request.issue.locked?
    return true if pull_request.repository.pushable_by?(user)

    errors.add(:base, "lock prevents comment")
  end

  def ensure_creator_is_not_blocked
    return unless user && pull_request

    if pull_request.blocked_from_reviewing?(user)
      errors.add(:user, "is blocked")
    end
  end

  def assign_repository_from_pull_request
    self.repository = pull_request.try(:repository)
  end

  def validate_authorized_to_create_content
    operation = new_record? ? :create : :update
    authorization = ContentAuthorizer.authorize(modifying_user, :pull_request_comment, operation,
                                                issue: issue,
                                                repo: repository)

    if authorization.failed?
      errors.add(:base, authorization.error_messages)
    end
  end

  # Internal.
  def max_textile_id
    GitHub.max_textile_pull_request_review_comment_id
  end

  private

  def reparent_replies
    GitHub.dogstats.time("pull_request_review_comment", tags: ["action:reparent_replies"]) do
      new_parent_id = github_sql.value(<<-SQL, pull_request_id: pull_request_id, reply_to_id: id)
        SELECT id FROM pull_request_review_comments
        WHERE pull_request_id = :pull_request_id AND reply_to_id = :reply_to_id
        ORDER BY created_at
      LIMIT 1
      SQL

      return unless new_parent_id

      github_sql.run(<<-SQL, new_parent_id: new_parent_id, pull_request_id: pull_request_id, reply_to_id: id)
        UPDATE pull_request_review_comments
        SET reply_to_id = :new_parent_id
        WHERE pull_request_id = :pull_request_id AND reply_to_id = :reply_to_id AND id != :reply_to_id
      SQL

      github_sql.run(<<-SQL, id: new_parent_id)
        UPDATE pull_request_review_comments
        SET reply_to_id = NULL
        WHERE id = :id
      SQL
    end
  end

  # Override GitHub::UserContent#attach_matching_assets? so that we only
  # attempt to attach matching assets when the review comment's body changes.
  def attach_matching_assets?
    body_previously_changed?
  end

  # Only track references when the review is submitted
  def track_references?
    submitted?
  end

  def blob_failure_context
    {
      blob_commit_oid: blob_commit_oid,
      blob_position:   blob_position,
      blob_path:       blob_path,
      app:             "github-blob-positioning",
    }
  end

  # Was the comment body changed in previous_changes? We use this to determine
  # if body changes _only_ after_commit.
  def body_changed_after_commit?
    previous_changes.key?(:body)
  end

  def importing?
    GitHub.importing?
  end

  def adjusted_position_dogstats(adjusted_blob_position, algorithm:)
    outdated = adjusted_blob_position.nil? ? "true" : "false"
    tags = ["algorithm:#{algorithm}", "outdated:#{outdated}"]
    GitHub.dogstats.increment("diff.comments.adjusted_blob_position", tags: tags)
  end

end
