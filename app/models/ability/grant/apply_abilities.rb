# frozen_string_literal: true
#
module Ability::Grant::ApplyAbilities
  # Internal: update the abilities table.
  def apply_abilities(rows = [], stats_key: nil)
    if !rows.empty?
      rows.each_slice(Ability::BATCH_SIZE) do |slice|
        origin = "Called from Ability::Grant#apply_abilities"
        Ability.throttle_with_retry(max_retry_count: Ability::Grant::THROTTLE_RETRIES, err_msg: origin, noop_on_foreground: true) do
          Ability.github_sql.run <<-SQL, rows: GitHub::SQL::ROWS(slice)
            INSERT IGNORE INTO abilities
            (actor_id, actor_type, action, subject_id, subject_type,
             priority, parent_id, created_at, updated_at)
            VALUES :rows
          SQL
        end
      end

      if stats_key
        GitHub.dogstats.histogram "ability.granted.#{stats_key}", rows.size
      end
      true
    end
  end
end
