# frozen_string_literal: true
class Ability::Grant
  class ApplyDependentsPendingQueue < GitHub::DataStructures::PersistentAtomicQueue
    def initialize(ability_id:)
      super(queue_type: "ability/grant/apply_dependents_pending", queue_name: ability_id, payload_class: ApplyDependentsPayload)
    end
  end
end
