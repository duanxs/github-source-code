# frozen_string_literal: true

module Ability::Graph
  def self.query(&block)
    builder = Ability::Graph::Builder.new
    yield(builder)
    query = builder.build
    query.result
  end
end
