# frozen_string_literal: true

module Ability::Graph
  class Result
    attr_reader :source, :block

    def initialize(source, &block)
      @source = source
      @block  = block
    end

    def values
      @values ||= block.call
    end

    def value
      # Todo fix this depending on the result values
      values && values[0][0]
    end

    def exists?
      values.length > 0
    end
  end
end
