# rubocop:disable Style/FrozenStringLiteralComment

module Ability::Graph
  class Query
    attr_reader :actors, :subjects, :select_columns, :count_by, :actions, :through, :uniq, :limit, :count, :sort
    delegate :values, :source, :exists?, to: :result

    def initialize(actors:, subjects:, select_columns:, count_by:, actions:, through:, uniq:, limit:, sort:, count:)
      @actors = actors
      @subjects = subjects
      @select_columns = select_columns
      @count_by = count_by
      @actions = actions
      @through = through
      @uniq = uniq
      @limit = limit
      @count = count
      @sort = sort
    end

    def result
      @result ||= result_for(to_s)
    end

    def to_s
      queries = []

      actors.each do |actor_type, actor_ids|
        subjects.each do |subject_type, subject_ids|
          if will_return_results?(actor_ids, subject_ids)
            queries << direct_ability_query(actor_type, actor_ids, subject_type, subject_ids, actions)
            if through.present?
              queries << indirect_ability_query(actor_type, actor_ids, subject_type, subject_ids, actions, through)
            end
          end
        end
      end

      query = queries.join(" UNION ")
      if sort_criteria.present?
        query << " #{sort_criteria}"
      end
      query
    end

    private

    def will_return_results?(actor_ids, subject_ids)
      (actor_ids == :all || actor_ids.any?) && (subject_ids == :all || subject_ids.any?)
    end

    def direct_ability_query(actor_type, actor_ids, subject_type, subject_ids, actions)
      sql = Ability.github_sql.new direct: Ability.priorities[:direct]

      sql.add <<-SQL
        SELECT #{select_clause}
          FROM abilities
         WHERE priority = :direct
      SQL

      add_actors_to_sql(sql, actor_type, actor_ids)
      add_subjects_to_sql(sql, subject_type, subject_ids)
      add_actions_to_sql(sql, actions)
      add_limit_to_sql(sql, limit)

      if count_by
        sql.add <<-SQL
          GROUP BY #{@count_by}
        SQL
      end

      sql.query
    end

    def indirect_ability_query(actor_type, actor_ids, subject_type, subject_ids, actions, through)
      sql = Ability.github_sql.new direct: Ability.priorities[:direct],
                            indirect: Ability.priorities[:indirect],
                            through: through
      sql.add <<-SQL
        SELECT #{indirect_abilities_select_clause}
        /* abilities-join-audited */
        FROM   abilities AS self
        JOIN   abilities AS parent
        ON     self.subject_type = parent.actor_type
        AND    self.subject_id   = parent.actor_id
        WHERE  parent.priority   = :direct
        AND    self.priority    <= :direct
        AND    self.subject_type IN :through
      SQL


      add_actors_to_sql(sql, actor_type, actor_ids, table: "self")
      add_subjects_to_sql(sql, subject_type, subject_ids, table: "parent")
      add_actions_to_sql(sql, actions, table: "parent")
      add_limit_to_sql(sql, limit)

      sql.query
    end

    def add_actions_to_sql(sql, actions, table: "abilities")
      if actions.present? && actions != :all
        sql.bind actions: actions.map { |action| Ability.actions[action] }, action_table: GitHub::SQL::LITERAL(table)

        sql.add <<-SQL
          AND :action_table.action IN :actions
        SQL
      end
    end

    def add_actors_to_sql(sql, actor_type, actor_ids, table: "abilities")
      sql.bind actor_type: actor_type, actor_table: GitHub::SQL::LITERAL(table)
      sql.add <<-SQL
        AND :actor_table.actor_type = :actor_type
      SQL

      unless actor_ids == :all
        sql.bind actor_ids: actor_ids
        sql.add <<-SQL
          AND :actor_table.actor_id IN :actor_ids
        SQL
      end
    end

    def add_subjects_to_sql(sql, subject_type, subject_ids, table: "abilities")
      sql.bind subject_type: subject_type, subject_table: GitHub::SQL::LITERAL(table)
      sql.add <<-SQL
        AND :subject_table.subject_type = :subject_type
      SQL

      unless subject_ids == :all
        sql.bind subject_ids: subject_ids
        sql.add <<-SQL
          AND :subject_table.subject_id IN :subject_ids
        SQL
      end
    end

    def add_limit_to_sql(sql, limit)
      return unless limit
      sql.bind limit: limit
      sql.add <<-SQL
        LIMIT :limit
      SQL
    end

    def select_clause
      return "COUNT(*)"                          if count
      return "#{@count_by}, COUNT(#{@count_by})" if count_by
      return select_columns_clause               if select_columns.length > 0
      "*, NULL grandparent_id"
    end

    def select_columns_clause
      clause = select_columns.join(",")
      clause = "distinct(#{clause})" if uniq
      clause
    end

    def indirect_abilities_select_clause
      return indirect_abilities_select_columns_clause if select_columns.length > 0
      <<-SQL
        NULL as id, self.actor_id, self.actor_type, parent.action,
        parent.subject_id, parent.subject_type,
        :indirect as priority,
        NULL as created_at, NULL as updated_at,
        self.id as parent_id, parent.id AS grandparent_id
      SQL
    end

    DIRECT_TO_INDIRECT_MAP = {
      actor_id: "self.actor_id",
      subject_id: "parent.subject_id",
    }

    def indirect_abilities_select_columns_clause
      remapped_columns = select_columns.map { |column| DIRECT_TO_INDIRECT_MAP[column] }
      clause = remapped_columns.join(",")
      clause = "distinct(#{clause})" if uniq
      clause
    end

    def sort_criteria
      @sort_criteria ||=
        begin
          sql = Ability.github_sql.new
          sort.each_with_index do |(column, direction), index|
            sql_string = index == 0 ? "ORDER BY :column :direction" : ", :column :direction"
            sql.add sql_string, column: GitHub::SQL::LITERAL(column), direction: GitHub::SQL::LITERAL(direction)
          end
          sql.query
        end
    end

    def result_for(sql)
      query = Ability.github_sql.new(sql)

      if sql.empty?
        Ability::Graph::Result.new(query) { [] }
      elsif count || count_by || select_columns.size > 0
        Ability::Graph::Result.new(query) { query.results }
      else
        Ability::Graph::Result.new(query) { query.models(Ability) }
      end
    end
  end
end
