# rubocop:disable Style/FrozenStringLiteralComment

class FlipperFeature < ApplicationRecord::Domain::Features
  include Instrumentation::Model
  include GitHub::Relay::GlobalIdentification

  areas_of_responsibility :feature_lifecycle

  GITHUB_ORG_ID = 9919
  BIG_FEATURE_WARNING_THRESHOLD = 100

  validates :name, presence: true,
    uniqueness: { case_sensitive: false },
    format: { with: /\A[a-z0-9_-]+\z/i }

  validate :team_must_be_part_of_github_org
  validate :team_or_service_provided, on: :create

  extend GitHub::Encoding
  force_utf8_encoding :description

  has_one :feature
  has_many :flipper_gates, dependent: :delete_all
  belongs_to :github_org_team, class_name: "Team"

  after_commit :instrument_create, on: :create
  after_commit :instrument_deletion, on: :destroy
  after_destroy :cleanup_raw_feature

  PERCENTAGES = [0, 1, 5, 10, 25, 50, 75, 100].freeze
  DELEGATED_METHODS = %i[state on? off? conditional? enabled?
    enable disable enable_group disable_group groups_value
    enable_percentage_of_actors disable_percentage_of_actors
    enable_percentage_of_time disable_percentage_of_time
    percentage_of_time_value percentage_of_actors_value
    actors_value boolean_value]

  delegate *DELEGATED_METHODS, to: :raw_feature

  scope :by_name, -> { order("name ASC") }

  scope :conditional, -> {
    feature_names = @klass.send(:raw_features_in_state, state: :conditional)
    where(name: feature_names).by_name
  }

  scope :enabled, -> {
    feature_names = @klass.send(:raw_features_in_state, state: :on)
    where(name: feature_names).by_name
  }

  scope :disabled, -> {
    feature_names = @klass.send(:raw_features_in_state, state: :off)
    where(name: feature_names).by_name
  }

  scope :matches_name_or_description, -> (query) do
    if query.present?
      sanitized_query = "%#{ActiveRecord::Base.sanitize_sql_like(query)}%"
      where("#{table_name}.name LIKE ? OR #{table_name}.description LIKE ?", sanitized_query,
            sanitized_query)
    else
      scoped
    end
  end

  scope :fully_enabled, -> {
    query_parts = {
      "flipper_gates.name = :boolean_name AND flipper_gates.value = :boolean_value": { boolean_name: "boolean", boolean_value: "true" },
      "flipper_gates.name IN (:percentile_name) AND flipper_gates.value = :percentile_value": { percentile_name: FlipperGate::PERCENTAGE_TYPES, percentile_value: "100" },
    }

    joins(:flipper_gates).where(query_parts.keys.join(" OR "), query_parts.values.reduce({}, :merge))
  }

  # Public: Find all flipper features turned on for a given actor.
  #
  # actor - a User
  # limit - how many FlipperFeatures to evaluate before truncating results
  scope :fully_enabled_or_enabled_for_actor, -> (actor, limit: 30) {
    # This scope results in N+1 additional queries, so applying a limit reduces
    # the potential impact.
    possible_results = self.limit(limit)

    filtered_results = possible_results.select do |feature|
      feature.fully_enabled? || feature.always_enabled?(actor)
    end

    possible_results == filtered_results ? possible_results : where(id: filtered_results)
  }

  class << self
    private
    def find_or_create_by(*)
      super
    end

    def find_or_create_by!(*)
      super
    end
  end

  # Takes a payload of data from an audit log entry and determines the string
  # label to use for the subject.
  def self.get_subject_label(gate_name:, subject:, skip_actor_gates: false)
    return nil if gate_name == :actor && skip_actor_gates

    case gate_name
    when :boolean then "everyone"
    when :percentage_of_time then "#{subject}% of enabled? calls"
    when :percentage_of_actors then "#{subject}% of actors"
    when :group then "the #{subject} group"
    when nil then "everyone"
    else subject
    end
  end

  def global_relay_id
    Platform::Helpers::NodeIdentification.to_global_id("Feature", name)
  end

  # Public: the Set of Flipper::Types::Groups that are not enabled for this feature
  def available_groups
    raw_feature.disabled_groups
  end

  # Public: The loaded actor instances which have access to the feature.
  #
  # Returns Array of mixed types
  def actors
    @actors ||= actor_ids_by_class.flat_map do |actor_class, ids|
      if actor_class.is_a?(String)
        # Class could not be constantized
        []
      elsif actor_class < ActiveRecord::Base
        scope = if [User, Organization].include?(actor_class)
          actor_class.includes(:profile)
        elsif actor_class == Repository
          actor_class.includes(:owner)
        else
          actor_class
        end
        scope.where(id: ids)
      else
        ids.map { |id| actor_class.new(id) }
      end
    end
  end

  # Public: Use the name of the feature when constructing URLs
  def to_param
    name
  end

  # Public: Determines if a percentage of actors has been set outside of
  # expected percentages defined in PERCENTAGES.
  #
  # Returns true if custom percentage is used, false if not.
  def custom_actor_percentage?
    !PERCENTAGES.include?(percentage_of_actors_value)
  end

  # Public: Determines if a percentage of random has been set outside of
  # expected percentages defined in PERCENTAGES.
  #
  # Returns true if custom percentage is used, false if not.
  def custom_random_percentage?
    !PERCENTAGES.include?(percentage_of_time_value)
  end

  def code_usage
    feature_name_in_code = name.underscore
    @code_usage ||= GitHub::Grep.new.code_use(/:#{feature_name_in_code}/,
                                              /#{feature_name_in_code}_enabled\?/,
                                              /#{feature_name_in_code}_required/,
                                              dirs: %w[app config jobs lib])
  end

  # Public: Determines if a feature is always enabled for a given actor
  # regardless of the percentage-of-time setting.
  #
  # Returns true if the feature is always enabled for this actor (i.e., would
  # still be enabled even if percentage-of-time were set to 0), false
  # otherwise.
  def always_enabled?(actor)
    values = raw_feature.gate_values
    context = Flipper::FeatureCheckContext.new(
      feature_name: raw_feature.name,
      values: values,
      thing: actor,
    )
    raw_feature.gates
      .reject { |gate| gate.key == :percentage_of_time } # reject the only non-deterministic gate
      .any? { |gate| gate.open?(context) }
  end

  # Public: Determines if a feature is enabled for everyone. This can return
  # true even when #on? returns false if the feature is enabled 100% of the
  # time or for 100% of actors.
  def fully_enabled?
    case state
    when :on
      true
    when :off
      false
    when :conditional
      percentage_of_actors_value == 100 || percentage_of_time_value == 100
    end
  end

  # Public: Determines if a feature is disabled for everyone. This can return
  # true even when #off? returns false if the feature is enabled 0% of the time
  # and not enabled for any groups or actors.
  def fully_disabled?
    case state
    when :on
      false
    when :off
      true
    when :conditional
      percentage_of_actors_value == 0 &&
        percentage_of_time_value == 0 &&
        groups_value.empty? &&
        actors_value.empty?
    end
  end

  def self.viewer_can_read?(viewer)
    return false unless GitHub.flipper_graphql_enabled?
    return false unless viewer
    return true if viewer.can_have_granular_permissions? &&
      Apps::Internal.capable?(:read_flipper_features, app: viewer.integration)

    viewer.github_developer? || viewer.site_admin?
  end

  def self.async_viewer_can_read?(viewer)
    Promise.resolve(viewer_can_read?(viewer))
  end

  def async_viewer_can_read?(viewer)
    self.class.async_viewer_can_read?(viewer)
  end

  def async_viewer_can_delete?(viewer)
    self.class.async_viewer_can_read?(viewer)
  end

  def async_description_html_for(viewer)
    async_github_repo = Platform::Loaders::ActiveRecord.load(User, "github", column: :login, case_sensitive: false).then do |github_org|
      Platform::Loaders::RepositoryByName.load(github_org.id, "github") if github_org
    end

    async_github_repo.then do |github_repo|
      GitHub::Goomba::MarkdownPipeline.async_to_html(description, {
        current_user: viewer,
        entity:       github_repo,
      })
    end
  end

  def description_html_for(viewer)
    async_description_html_for(viewer).sync
  end

  def platform_type_name
    "Feature"
  end

  def number_of_actor_gates
    flipper_gates.actor_gates.size
  end

  # Groups actor IDs by the class through which the actor was granted access to the feature.
  #
  # Returns an Array of tuples, where each tuple is of the form [Class, [Integer]] e.g.
  # [[User, [123, 456]], [Organization, [789]]].
  def actor_ids_by_class
    actor_ids.group_by(&:first).map do |actor_class, actors|
      ids = actors.map(&:second)

      begin
        actor_class = actor_class.constantize
      rescue NameError
      end

      [actor_class, ids]
    end
  end

  def team_must_be_part_of_github_org
    if !Rails.development? && github_org_team_id.present? && github_org_team.organization_id != GITHUB_ORG_ID
      errors.add(:github_org_team_id, "team must be part of GitHub org")
    end
  end

  def team_or_service_provided
    if GitHub::ServiceCatalog.enabled? && service_name.blank? && github_org_team.blank?
      errors.add(:service_name, "a team or service must be selected")
      errors.add(:github_org_team_id, "a team or service must be selected")
    end
  end

  def show_big_feature_warning?
    !GitHub::Config::Flipper::big_features.include?(name) && flipper_gates.actor_gates.size > BIG_FEATURE_WARNING_THRESHOLD
  end

  private

  # Internal: Fetches all serialized resources that have been given access to the
  # feature.
  #
  # Returns Array of [String, Integer] elements
  def actor_ids
    raw_feature.actors_value.collect { |actor| GitHub::FlipperActor.flipper_id_to_parts(actor) }
  end

  # Internal: The Flipper::Feature
  def raw_feature
    @raw_feature ||= GitHub.flipper[name.to_sym]
  end

  # Internal: The Flipper::Feature records in a specific state
  #   state - a Symbol
  # Returns a Array of feature names as Strings.
  def self.raw_features_in_state(state:)
    features = GitHub.flipper.features
    features = features.select { |feature| feature.state == state }
    features.map { |feature| feature.name.to_s }
  end
  private_class_method :raw_features_in_state

  # Internal: instrument creation of a feature
  def instrument_create
    instrument :create, operation: :create
  end

  # Internal: Instrument deletion of a feature
  def instrument_deletion
    instrument :destroy, operation: :destroy
  end

  # Internal: Ensures Flipper cache is cleared
  def cleanup_raw_feature
    GitHub.flipper.adapter.remove(raw_feature)
  end

  # Internal: event payload for Instrumentation
  def event_payload
    {
      feature_name: name,
    }
  end

  def event_prefix
    :feature
  end
end
