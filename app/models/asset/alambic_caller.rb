# frozen_string_literal: true

module Asset::AlambicCaller
  def self.included(model)
    model.extend ClassMethods
  end

  def http
    self.class.http
  end

  module ClassMethods
    def stub_http(&block)
      @http = Faraday.new { |b| b.adapter(:test, &block) }
    end

    def reset_http!
      @http = nil
    end

    def http
      @http ||= Faraday.new(url: GitHub.alambic_url) do |faraday|
        faraday.adapter Faraday.default_adapter
      end
    end
  end
end
