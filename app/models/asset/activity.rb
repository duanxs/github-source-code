# rubocop:disable Style/FrozenStringLiteralComment

# Represents an owner's asset activity broken down by owner, resource, and hour.
# Note that hours with no access activity will not count any byte hours.
class Asset::Activity < ApplicationRecord::Domain::Assets
  include Asset::Types

  areas_of_responsibility :lfs

  self.table_name = :asset_activities

  belongs_to :owner, class_name: "User"

  # This may be nil (ID 0) for older data which was not stored on a
  # per-repository basis.
  belongs_to :repository

  # Public: Increase the bandwidth and byte hours for an owner's data for a
  # given hour (as defined by started_at).
  def self.track(asset_type, owner_id, repo_id, started_at, up: nil, down: nil, source_files: nil)
    up = up.to_f
    down = down.to_f
    return unless up > 0.0 || down > 0.0

    uniq_src_files = Array(source_files).delete_if(&:blank?).uniq
    return if uniq_src_files.blank?

    sql = github_sql.new(
      owner_id: owner_id,
      repository_id: repo_id,
      started_at: started_at,
      down: down,
      up: up,
      files: encode_source_lines(uniq_src_files, 65535),
      asset_type: self.asset_types[asset_type],
    )

    q = <<-SQL
      INSERT INTO asset_activities (
        owner_id, repository_id, activity_started_at,
        bandwidth_down, bandwidth_up, source_files, asset_type,
        created_at, updated_at
      ) VALUES (
        :owner_id, :repository_id, :started_at,
        :down, :up, :files, :asset_type,
        NOW(), NOW()
      ) ON DUPLICATE KEY UPDATE
        bandwidth_down = bandwidth_down+VALUES(bandwidth_down),
        bandwidth_up = bandwidth_up+VALUES(bandwidth_up),
        source_files = RIGHT(CONCAT(source_files, ',', VALUES(source_files)), 65535),
        updated_at = NOW()
    SQL

    ActiveRecord::Base.connected_to(role: :writing) do
      sql.run(q)
    end

    GitHub.dogstats.increment("s3_usage.activities",
      tags: ["type:#{asset_type}"],
    )
  end

  def self.encode_source_lines(lines, col_limit)
    s = Array(lines).join(",")
    s.gsub!(/\,{2,}/, ",") # remove dup commas
    s.gsub!(/\A,/, "")
    s.gsub!(/,\z/, "")
    if s.size > col_limit
      s = s[s.size - col_limit..-1]
    end
    s
  end

  # Finds all Asset::Activities from a single owner over a time period, bucketed
  # to the hour.
  def self.fetch_for_owner(asset_type, owner, from, to)
    Asset::Activity.
      where(owner_id: owner.id, asset_type: Asset::Activity.asset_types[asset_type]).
      where("activity_started_at > ? AND activity_started_at < ?", from, to)
  end

  # Computes all activity from a single owner over a time period, bucketed by
  # network ID.
  def self.fetch_for_owner_by_network(asset_type, owner_id, from, to)
    networks = {}
    repos = {}
    ActiveRecord::Base.connected_to(role: :reading) do
      Asset::Activity.
        where("activity_started_at > ? AND activity_started_at < ?", from, to).
        where(
          asset_type: Asset::Activity.asset_types[asset_type],
          owner_id: owner_id,
      ).in_batches.each do |batch|
        batch.each do |activity|
          repo = activity.repository
          net = repos[repo] ||= repo&.network&.id
          networks[net] ||= { bandwidth_up: 0.0, bandwidth_down: 0.0 }
          networks[net][:bandwidth_up] += activity.bandwidth_up
          networks[net][:bandwidth_down] += activity.bandwidth_down
        end
      end
    end
    networks
  end

  def self.seen_source_files(asset_type, owner_id, repo_id, started_at)
    activity = Asset::Activity.find_by(
      asset_type: Asset::Activity.asset_types[asset_type],
      owner_id: owner_id,
      repository_id: repo_id,
      activity_started_at: started_at,
    )

    return Set.new unless activity

    ids = activity.source_files.to_s.split(",")
    ids.uniq!
    Set.new(ids.delete_if { |id| id.blank? })
  end
end
