# frozen_string_literal: true

module Asset::Types
  extend ActiveSupport::Concern

  included do
    enum asset_type: { lfs: 0, registry: 1 }
  end
end
