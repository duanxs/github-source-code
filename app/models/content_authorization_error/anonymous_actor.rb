# rubocop:disable Style/FrozenStringLiteralComment

# Error indicating that this operation cannot be performed by an anonymous
# actor.
class ContentAuthorizationError::AnonymousActor < ContentAuthorizationError
  def message
    "You must be logged in to do that."
  end

  def http_error_code
    401
  end

  def documentation_url
    "/v3/#authentication"
  end
end
