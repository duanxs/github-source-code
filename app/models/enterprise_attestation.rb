# frozen_string_literal: true

class EnterpriseAttestation

  DEFAULT_PAGE_SIZE = 50
  VALID_PAGE_SIZE_RANGE = (1..100)
  VALID_PAGE_RANGE = (1..1000)

  # Public: Fetch list of contractor User IDs.
  def self.contractor_ids(limit: DEFAULT_PAGE_SIZE, page: nil)
    return [] unless GitHub.restrict_contractors_from_default_access_to_internal_repos?

    limit = DEFAULT_PAGE_SIZE unless VALID_PAGE_SIZE_RANGE.include?(limit)
    page = 1 unless VALID_PAGE_RANGE.include?(page)
    start = (page - 1) * limit

    ids = storage.fetch_ids

    ids[start, limit]
  end

  # Public: Check if given User ID has contractor attestation set.
  def self.contractor?(user_id)
    return false unless GitHub.restrict_contractors_from_default_access_to_internal_repos?
    storage.exists?(user_id)
  end

  # Public: Get attestation for given User ID.
  def self.get(user_id)
    return unless GitHub.restrict_contractors_from_default_access_to_internal_repos?
    if storage.exists?(user_id)
      new(user_id: user_id, contractor: true)
    end
  end

  # Public: Set attestation for given User ID.
  def self.set(user_id, contractor: false)
    return false unless GitHub.restrict_contractors_from_default_access_to_internal_repos?
    storage.set(user_id, contractor: contractor)
  end

  attr_reader :user_id, :contractor

  def initialize(user_id:, contractor:)
    @user_id = user_id
    @contractor = contractor
  end

  def self.storage
    @storage ||= Storage.new(GitHub.kv)
  end
  private_class_method :storage

  class Storage
    CONTRACTOR_KEY = "ent:attest:contractor:%d"
    CONTRACTORS_INDEX_KEY = "ent:attest:contractors"

    attr_reader :kv

    def initialize(kv)
      @kv = kv
    end

    def fetch_ids
      v = kv.get(CONTRACTORS_INDEX_KEY).value { nil }
      decode_value(v, default: [])
    end

    def exists?(user_id)
      kv.exists(key_for(user_id)).value!
    end

    def set(user_id, contractor:)
      kv.connection.transaction do
        ids = fetch_ids

        if contractor
          kv.set(key_for(user_id), encode_value(Time.now.to_i))
          kv.set(CONTRACTORS_INDEX_KEY, encode_value(ids | [user_id]))
        else
          kv.set(CONTRACTORS_INDEX_KEY, encode_value(ids - [user_id]))
          kv.del(key_for(user_id))
        end

        true
      end
    end

    private

    def key_for(user_id)
      CONTRACTOR_KEY % user_id
    end

    def encode_value(arr)
      arr.to_json
    end

    def decode_value(val, default: nil)
      return default unless val.present?
      JSON.parse(val)
    end

  end

end
