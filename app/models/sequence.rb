# rubocop:disable Style/FrozenStringLiteralComment

# Provides a mechanism for generating sequences of natural order numbers,
# uniquely scoped by a given context.
#
# The context should be responsible for coordinating the creation of resources
# sharing a number space.
#
# For example, a Repository is responsible for creating Issues and Pull Requests
# that share the same sequence such that no two distinct records will share the
# same number.
#
# Each number can be any type of record that the context auto-numbers. Each new
# record will be assigned the next available number. It's the context's
# responsibility to know how to find the record for a given number.
#
# It's assumed that once a number has been generated for the context, it should
# be considered taken and should not be reused or reassigned. These numbers
# represent stable reference points for users.
#
# This model is used exclusively for coordinating number generation within the
# given context and in parallel with all other contexts.
#
# NOTE: Resetting a sequence is a destructive action, deleting it. In practice,
# it should never be done on production.
module Sequence
  class Error < RuntimeError; end;

  EXTRACTED_CONTEXTS = {
    Repository.name => RepositorySequence,
  }

  # An object that represents a global sequence.
  #
  # Unlike a Repository which gets its own sequence, in some cases we just need
  # one sequence for a given global context. This object provides users of the
  # Sequence API to provide a context object that is compatible with these
  # needs.
  #
  #   context = Sequence::GlobalContext.new("AccountLink::SequenceContext")
  #   Sequence.next(context)
  class GlobalContext
    attr_reader :sequence_context_type, :id

    def initialize(sequence_context_type, id = 0)
      @sequence_context_type = sequence_context_type
      @id = id
    end
  end

  # Public: Creates a sequence for the given context.
  #
  # Returns nothing.
  def self.create(context, default = 0)
    if sequence_class = extracted_context_sequence_class(context)
      sequence_class.create!(
        context.model_name.singular => context,
        :number => default,
      )
    else
      sql = prepare_query(context, default: default)
      sql.add <<-SQL
        INSERT INTO sequences (context_type, context_id, number, created_at, updated_at)
        VALUES (:context_type, :context_id, :default, NOW(), NOW())
      SQL
      sql.run
    end

    nil
  rescue ActiveRecord::RecordNotUnique
  end

  # Public: Checks to see if a sequence exists for a given context.
  #
  # Returns true if it exists, false otherwise.
  def self.exists?(context)
    if sequence_class = extracted_context_sequence_class(context)
      sequence_class.where(context.model_name.singular => context).exists?
    else
      sql = prepare_query(context)
      sql.add <<-SQL
        SELECT count(id) FROM sequences
        WHERE context_type = :context_type AND context_id = :context_id
        LIMIT 1
      SQL

      sql.value.to_i == 1
    end
  end

  # Public: Returns the current sequence number Integer for the context.
  def self.get(context)
    if sequence_class = extracted_context_sequence_class(context)
      sequence_class.where(context.model_name.singular => context).pluck(:number).first
    else
      sql = prepare_query(context)
      sql.add <<-SQL
        SELECT number FROM sequences
        WHERE
          context_type = :context_type AND
          context_id = :context_id
        LIMIT 1
      SQL
      sql.value.to_i
    end
  end

  # Set the current sequence number for the given context.
  #
  # Returns nothing.
  def self.set(context, number)
    if sequence_class = extracted_context_sequence_class(context)
      update_extracted_context_sequence(sequence_class, context, number: number)
    else
      set_or_next_sequence_query(context, number: number).run
    end

    nil
  end

  # Resets the sequence for the given context by removing the record.
  #
  # Returns nothing.
  def self.reset(context, force = Rails.env.test?)
    return unless force

    if sequence_class = extracted_context_sequence_class(context)
      sequence_class.where(context.model_name.singular => context).delete_all
    else
      sql = prepare_query(context)
      sql.add <<-SQL
        DELETE FROM sequences
        WHERE context_type = :context_type AND context_id = :context_id
      SQL
      sql.run
    end

    nil
  end

  # Increments the sequence number for a given context.
  #
  # The sequence MUST be created before enumerating.
  #
  # Returns the next sequence number Integer, or raises if the sequence doesn't
  # exist.
  def self.next(context, increment = 1)
    number = nil
    GitHub.instrument "sequence.next", context: context do
      raise Error, "No sequence for context of type #{context.class}" unless exists?(context)

      if sequence_class = extracted_context_sequence_class(context)
        update_extracted_context_sequence(sequence_class, context, increment: increment)

        # TODO: Call public method once this pull request is merged:
        #   https://github.com/github/trilogy-adapter/pull/201
        number = sequence_class.connection.send(:last_insert_id)
      else
        result = set_or_next_sequence_query(context, increment: increment).run
        number = result.last_insert_id
      end
    end

    number
  end

  # Internal: Returns a GitHub::SQL query object with optional context binds.
  def self.prepare_query(context = nil, binds = {})
    sql = ApplicationRecord::Domain::Sequences.github_sql.new binds

    if context
      context_type =
        if context.respond_to?(:sequence_context_type)
          context.sequence_context_type
        else
          context.class.name
        end

      sql.bind context_type: context_type, context_id: context.id
    end

    sql
  end

  # Internal: Prepares the set/next query.
  #
  # Provide `increment` bind to increment the existing sequence.
  # Otherwise, provide `number` bind to set the sequence explicitly.
  #
  # Sequence must be created before running this.
  #
  # Returns a prepared GitHub::SQL object ready to be executed.
  def self.set_or_next_sequence_query(context, binds = {})
    unless binds[:increment] || binds[:number]
      raise ArgumentError, ":increment or :number must be specified"
    end

    sql = prepare_query context, binds
    sql.add <<-SQL
      UPDATE sequences SET
    SQL

    if binds[:increment]
      # increment the sequence by the provided amount
      sql.add <<-SQL
        number     = LAST_INSERT_ID(number + :increment),
      SQL
    else
      # otherwise set the sequence explicitly
      sql.add <<-SQL
        number     = LAST_INSERT_ID(:number),
      SQL
    end

    sql.add <<-SQL
      updated_at = NOW()
    SQL

    # for the context's sequence
    sql.add <<-SQL
      WHERE
        context_type = :context_type AND
        context_id   = :context_id
    SQL

    sql
  end

  def self.extracted_context_sequence_class(context)
    EXTRACTED_CONTEXTS[context.class.name]
  end

  def self.update_extracted_context_sequence(sequence_class, context, number: nil, increment: nil)
    raise ArgumentError(":increment or :number must be specified") if number.nil? && increment.nil?

    update_sql = ""
    binds = {}

    if number
      update_sql << "`number` = LAST_INSERT_ID(:number)"
      binds[:number] = number
    else
      update_sql << "`number` = LAST_INSERT_ID(`number` + :increment)"
      binds[:increment] = increment
    end

    update_sql << ", `updated_at` = NOW()"

    sequence_class.where(context.model_name.singular => context).update_all([update_sql, binds])
  end
end
