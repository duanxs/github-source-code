# frozen_string_literal: true

module Integration::RepositoriesDependency
  # Public: Given an array of repository ids return
  # the list of repositories we actually have access to as
  # an Integration.
  #
  #  current_integration_installation: An installation record that can be used to further limit the result.
  #  repository_ids:                   An array of repository ids.
  #  permissions:                      An array of granular repository permissions.
  #
  # Returns an Array of repository ids.
  def accessible_repository_ids(current_integration_installation:, repository_ids: [], permissions: Repository::Resources.subject_types)
    GitHub.dogstats.time("integration.accessible_repository_ids.time") do
      return [] unless repository_ids.present?

      if current_integration_installation
        return [] unless current_integration_installation.integration_id == self.id
        return [] if current_integration_installation.suspended?

        current_permissions = current_integration_installation.version.permissions_of_type(Repository).keys
        return [] unless (current_permissions & permissions).any?

        return repository_ids & current_integration_installation.repository_ids
      end

      # Global apps have access to all repositories if it requires
      # repository permissions.
      if Apps::Internal.capable?(:installed_globally, app: self)
        # Ensure that the global app has at least one
        # of the permissions that are requested.
        return [] unless (latest_version.permissions_of_type(Repository).keys & permissions).any?
        return Repository.with_ids(repository_ids).pluck(:id)
      end

      owner_ids = Repository.with_ids(repository_ids).pluck(:owner_id)

      installation_ids = Authorization.service.actor_ids_with_granular_permissions_on(
        actor_type:   "IntegrationInstallation",
        subject_type: "Repository",
        subject_ids:  repository_ids,
        owner_ids:    owner_ids,
        permissions:  permissions,
      )

      relation = IntegrationInstallation.not_suspended.with_ids(installation_ids).where(integration: self)

      promise = Platform::Loaders::ActiveRecord.load_relation(relation).then do |installations|
        Promise.all(
          installations.map do |installation|
            installation.async_target.then do
              installation.repository_ids(repository_ids: repository_ids)
            end
          end,
        )
      end

      (promise.then(&:flatten).sync).sort
    end
  end

  # Public: Repository ids owned by target account that the given actor can
  # install integrations on.
  #
  #   target: the target of the installation, must be an Organization
  #   actor: the User requesting installation.
  #   exclude_installed: exclude the repositories already on the installation (default true).
  #
  # Returns an Array.
  def installable_repository_ids_on_by(target:, actor:, exclude_installed: true)
    return [] unless actor&.user?
    return [] if target.is_a?(Business)
    return [] if target.user? && actor != target

    actor_can_install_on_all_repositories = installable_on_all_repositories_by?(target: target, actor: actor)

    repository_ids = if actor_can_install_on_all_repositories
      target.repository_ids
    else
      actor.associated_repository_ids(min_action: :admin, repository_ids: target.repository_ids)
    end

    if exclude_installed && (installation = installations.with_user(target).first)
      if installation.repository_selection == "selected"
        repository_ids -= installation.repository_ids
      end
    end

    repository_ids -= RepositoryAdvisory.where(workspace_repository_id: repository_ids).pluck(:workspace_repository_id)

    if actor_can_install_on_all_repositories || installable_on_by?(target: target, actor: actor, repositories: Repository.where(id: repository_ids))
      repository_ids
    else
      []
    end
  end

  # Public: Repository ids owned by target account that the given actor can
  # request integration installation on.
  #
  #   target: the target of the installation, must be an Organization
  #   actor: the User requesting installation.
  #   exclude_requested: exclude the repositories already on the installation (default true).
  #
  # Returns an Array.
  def requestable_repository_ids_on_by(target:, actor:, exclude_requested: true)
    return [] if target.is_a?(Business) || target.user?
    return [] unless actor&.user?
    return [] unless requestable_on_by?(target: target, actor: actor)

    repository_ids = actor.associated_repository_ids(repository_ids: target.repository_ids)
    repository_ids -= installable_repository_ids_on_by(target: target, actor: actor)
    repository_ids |= target.repositories.public_scope.ids if target.member?(actor)

    if exclude_requested && (installation = installations.with_user(target).first)
      repository_ids -= installation.repository_ids
    end

    repository_ids
  end
end
