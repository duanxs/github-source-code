# frozen_string_literal: true
class Integration
  include Configurable
  include Configurable::IpWhitelistingEnabled

  # Internal: Get the configuration owner.
  #
  # Values for a Business can be cascaded from (or overridden by) the global
  # GitHub object.
  def configuration_owner
    GitHub
  end
end
