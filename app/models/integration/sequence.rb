# frozen_string_literal: true

module Integration::Sequence
  # Internal: Ensures a sequence is created for the integration.
  #
  # Returns nothing.
  def create_sequence
    GitHub.dogstats.time("integration.create_sequence.time") do
      ::Sequence.create(self)
      true
    end
  end

  def self.included(base)
    base.send :extend, ClassMethods
  end

  module ClassMethods
    def setup_sequence
      after_create :create_sequence
    end
  end
end
