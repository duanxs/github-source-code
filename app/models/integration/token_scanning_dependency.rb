# frozen_string_literal: true

module Integration::TokenScanningDependency
  def secret_scanning_api_enabled?
    GitHub.flipper[:secret_scanning_api_enabled].enabled?(self)
  end
end
