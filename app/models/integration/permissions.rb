# frozen_string_literal: true

class Integration
  class Permissions
    attr_reader :integration, :actor, :action, :target, :options, :version

    PERMITTED_ACTIONS = %i[install request_installation].freeze

    REPO_ADMIN_PROHIBITED_INSTALLATION_REPOSITORY_RESOURCES = %w[administration].freeze

    ORGANIZATION_INSTALLATION_REQUIRED_RESOURCES =\
      Organization::Resources.subject_types + REPO_ADMIN_PROHIBITED_INSTALLATION_REPOSITORY_RESOURCES

    class Result
      attr_reader :actor, :target, :action, :result, :reason

      def self.for(check, result:, reason: :default)
        new(check.actor, check.target, check.action, result, reason: reason)
      end

      DEFAULT_REASON = "You do not have permission to %{action} this app on %{target}."

      HUMAN_READABLE_REASONS = {
        all_repositories: "You do not have permission to %{action} apps with all repositories on %{target}.",
        requires_org_permissions: "You cannot %{action} apps with organization permissions on %{target}.",
        not_admin_on_subset: "You do not have permission to %{action} this app on these repositories belonging to %{target}.",
        not_owned_by_target: "Repositories must be owned by %{target}.",
        spammy_target: "%{target} has been flagged as spam and therefore cannot install GitHub Apps.",
        spammy_actor: "Your account has been marked as spam and therefore cannot install GitHub Apps on any account.",
      }.freeze

      def initialize(actor, target, action, result, reason:)
        @actor, @target, @action = actor, target, action
        @result, @reason = result, reason
      end

      def permitted?
        @result
      end

      CONTACT_ORGANIZATION_OWNER_MESSAGE = " Please contact an Organization Owner."

      def error_message
        full_reason = HUMAN_READABLE_REASONS.fetch(reason, DEFAULT_REASON)
        full_reason = full_reason + CONTACT_ORGANIZATION_OWNER_MESSAGE if target.is_a?(Organization)

        I18n.interpolate(full_reason, { action: action, target: target })
      end
    end

    def self.check(integration:, actor:, action:, **options)
      new(integration: integration, actor: actor, action: action, options: options).check
    end

    def initialize(integration:, actor:, action:, options: {})
      @integration = integration
      @actor       = actor
      @action      = action
      @options     = options
      @target      = options[:target]
      @version     = options[:version]
    end

    def check
      raise(ArgumentError, "#{action} is not a valid action") unless PERMITTED_ACTIONS.include?(action)

      GitHub.dogstats.time("integrations.permission_check.#{action}") do
        case action
        when :install
          can_install?
        when :request_installation
          can_request_installation
        end
      end
    end

    private

    def can_install?
      return result(false, :missing_target) if target.nil?
      return result(false, :spammy_target)  if target.spammy?

      repositories                  = options.fetch(:repositories, [])
      installed_on_all_repositories = options.fetch(:all_repositories, false)

      return result(true, :self_install) if integration_installation_can_self_install?

      return result(false, :invalid_actor) unless IntegrationInstallation::VALID_TARGET_TYPES.include?(actor.class.to_s)
      return result(false, :spammy_actor) if actor.spammy?

      # Cannot install an Integration on another target if it is private.
      return result(false, :not_installable_on) unless integration.installable_on?(target)

      if repositories.any?
        return result(false, :not_owned_by_target) unless target_owns_all_repos?(repositories)
        return result(false, :advisory_workspace_repo_included) if repos_include_advisory_workspace?(repositories)
      end

      # Short circuit if the target is adminable by the actor.
      return result(true, :is_admin) if target.adminable_by?(actor)

      # Short circuit unless the target is Organization record.
      return result(false, :not_adminable) unless target.is_a?(Organization)

      # An IntegrationVersion owned by the @integration is required
      # for checking organization permissions
      return result(false, :missing_version) if version.nil?
      return result(false, :invalid_version) if version.integration_id != integration.id

      # If the organization permissions are required
      # short circuit since only target admins can install
      # with org permissions.
      return result(false, :requires_org_permissions) if requires_organization_installation?

      # Only target admins can install on all repositories
      return result(false, :all_repositories) if installed_on_all_repositories || repositories.empty?

      return result(false, :not_admin_on_subset) unless can_admin_all_repos?(repositories)

      result(true)
    end

    def can_request_installation
      # Must have a target
      return result(false, :missing_target) if target.nil?

      # Cannot request an installation unless the actor is a User.
      return result(false, :requester_not_a_user) unless actor&.user?

      # Cannot request an installation on a User target.
      return result(false, :not_an_organization) unless target.is_a?(Organization)

      # Cannot install an Integration on another target if it is private.
      return result(false, :not_installable_on_target) unless integration.installable_on?(target)

      # Admins don't need to request an installation.
      return result(false, :is_admin) if target.adminable_by?(actor)

      # Organization members and outside collaborators on org repos can
      # request an installation.
      return result(true, :organization_member)  if target.member?(actor)
      return result(true, :outside_collaborator) if target.user_is_outside_collaborator?(actor.id)

      result(false, :not_part_of_organization)
    end

    def requires_organization_installation?
      (ORGANIZATION_INSTALLATION_REQUIRED_RESOURCES & version.default_permissions.keys).any?
    end

    def target_owns_all_repos?(repositories)
      if repositories.respond_to?(:where)
        !repositories.where("owner_id != ?", target.id).exists?
      else
        repositories.all? { |repository| repository.owner_id == target.id }
      end
    end

    def integration_installation_can_self_install?
      return false unless actor.is_a?(IntegrationInstallation)
      return false unless actor.target == target

      args = {
        priority: Ability.priorities[:direct],
        action: Ability.actions[:write],
        actor_id: actor.ability_id,
        actor_type: actor.ability_type,
        subject_type: Repository.new.resources.administration.ability_type,
      }

      sql = Permission.github_sql.new(<<-SQL, args)
        SELECT 1
        FROM permissions
        WHERE priority = :priority
        AND actor_id = :actor_id
        AND actor_type = :actor_type
        AND subject_type = :subject_type
        AND action IN (:action)
        LIMIT 1
      SQL
      sql.value?
    end

    def can_admin_all_repos?(repositories)
      repo_ids = if repositories.respond_to?(:pluck)
        repositories.pluck(:id)
      else
        repositories.map(&:id)
      end

      adminable_repo_ids = actor.associated_repository_ids(min_action: :admin)

      (repo_ids - adminable_repo_ids).empty?
    end

    def repos_include_advisory_workspace?(repositories)
      repo_ids = if repositories.respond_to?(:pluck)
        repositories.pluck(:id)
      else
        repositories.map(&:id)
      end

      RepositoryAdvisory.where(workspace_repository_id: repo_ids).any?
    end

    def result(value, reason = :default)
      Result.for(self, result: value, reason: reason)
    end
  end
end
