# frozen_string_literal: true

class Integration::Events
  @@feature_flags = {}

  # Events not available to GHES.
  SUPPORTED_DOTCOM_ONLY_EVENTS = %w(
    org_block
  )

  SUPPORTED_EVENTS = %w(
    code_scanning_alert
    content_reference
    check_run
    check_suite
    commit_comment
    create
    delete
    deploy_key
    deployment
    deployment_status
    fork
    gollum
    issue_comment
    issues
    label
    milestone
    member
    membership
    meta
    organization
    page_build
    project
    project_card
    project_column
    public
    pull_request
    pull_request_review
    pull_request_review_comment
    push
    registry_package
    release
    reminder
    repository
    repository_dispatch
    repository_import
    star
    status
    team
    team_add
    watch
    workflow_dispatch
    workflow_run
  )

  # Make sure we don't show dotcom only
  # events to GHES instances.
  unless GitHub.enterprise?
    SUPPORTED_DOTCOM_ONLY_EVENTS.map do |event|
      SUPPORTED_EVENTS.append(event)
    end
  end

  RESOURCE_TO_EVENTS = {
    "actions"                     => %w(workflow_run),
    "issues"                      => %w(issue_comment issues milestone),
    "pull_requests"               => %w(milestone pull_request pull_request_review pull_request_review_comment reminder),
    "statuses"                    => %w(status),
    "checks"                      => %w(check_suite check_run),
    "deployments"                 => %w(deployment deployment_status deploy_key),
    "administration"              => %w(member),
    "contents"                    => %w(commit_comment create delete fork gollum push release repository_dispatch workflow_dispatch workflow_run),
    "metadata"                    => %w(label public repository repository_import watch star),
    "members"                     => %w(member membership organization team team_add),
    "repository_projects"         => %w(project_card project_column project),
    "organization_projects"       => %w(project_card project_column project),
    "pages"                       => %w(page_build),
    "organization_administration" => %w(org_block),
    "content_references"          => %w(content_reference),
    "packages"                    => %w(registry_package),
    "security_events"             => %w(code_scanning_alert)
  }.freeze

  # Integrator events are those that do not depend on available resources or
  # granted permissions. The information delivered via an integrator event is
  # not associated with an individual installation or target (repository,
  # organization, etc.). The event is triggered only once for each subscribed
  # integration. Integrator events are not inherited by integration versions
  # nor by integration installations.
  INTEGRATOR_EVENTS = %w(
    security_advisory
    meta
  )

  INTEGRATOR_EVENTS_AND_FEATURE_FLAGS = {
  }

  attr_reader :actor

  def initialize(actor:)
    @actor = actor
  end

  # Public: a mapping of event types to the resources applicable to that event.
  # E.g. { "milestone" => ["pull_requests", "issues"], ... }
  #
  # Returns a Hash. Memoizes the event type to resource mapping based on the
  # current actor.
  def event_types_to_resources
    @event_types_to_resources ||= self.class.event_types_to_resources(actor: actor)
  end

  def self.event_types_for_resource(resource, actor: nil, include_feature_flagged_events: false)
    Array.wrap(RESOURCE_TO_EVENTS[resource]).select do |event|
      next true if include_feature_flagged_events
      next true unless event_feature_flagged?(event)
      actor.present? && event_feature_enabled?(event, actor: actor)
    end
  end

  def self.hook_event_for_event_type(event_type)
    Hook::EventRegistry.for_event_type(event_type)
  end

  # Internal: a mapping of event types to the resources applicable to that event.
  # E.g. { "milestone" => ["pull_requests", "issues"], ... }
  #
  # Returns a Hash.
  def self.event_types_to_resources(actor: nil)
    Integration::Events::SUPPORTED_EVENTS.inject({}) do |events_to_resources, event|
      # Skip this event type if it is behind a feature flag and not enabled for this actor.
      next(events_to_resources) if event_feature_flagged?(event) && !(actor.present? && event_feature_enabled?(event, actor: actor))

      resources = Integration::Events::RESOURCE_TO_EVENTS.select do |resource, events|
        events.include?(event)
      end.map(&:first)
      events_to_resources[event] = resources
      events_to_resources
    end
  end

  def self.organization_event?(event_type)
    organization_events.include?(event_type)
  end

  def self.repository_event?(event_type)
    repository_events.include?(event_type)
  end

  def self.resources_for_event(event_type)
    RESOURCE_TO_EVENTS.find_all { |_, events| events.include?(event_type) }.map(&:first)
  end

  def self.organization_resources_for_event(event_type)
    resources_for_event(event_type) & Organization::Resources.subject_types
  end

  def self.repository_resources_for_event(event_type)
    resources_for_event(event_type) & Repository::Resources.subject_types
  end

  def self.organization_events
    RESOURCE_TO_EVENTS.select do |resource, _events|
      Organization::Resources.subject_types.include?(resource)
    end.values.flatten
  end
  private_class_method :organization_events

  def self.repository_events
    RESOURCE_TO_EVENTS.select do |resource, _events|
      Repository::Resources.subject_types.include?(resource)
    end.values.flatten
  end
  private_class_method :repository_events

  def self.feature_flag(event, flag)
    (@@feature_flags[flag] ||= []).push(event)
  end

  def self.remove_feature_flag(event, flag)
    @@feature_flags[flag].delete(event)
  end

  def self.feature_flagged_events
    @@feature_flags.values.flatten
  end
  private_class_method :feature_flagged_events

  def self.event_feature_flagged?(event)
    feature_flagged_events.include?(event.to_sym)
  end
  private_class_method :event_feature_flagged?

  def self.event_feature_enabled?(event, actor:)
    @@feature_flags.any? do |flag, events|
      next false unless events.include?(event.to_sym)
      GitHub.flipper[flag].enabled?(actor)
    end
  end
  private_class_method :event_feature_enabled?

  feature_flag :repository_import, :porter_webhooks_in_github_apps
  feature_flag :reminder, :scheduled_reminders_reminder_event
  feature_flag :workflow_run, :actions_workflow_run_webhook

  unless GitHub.enterprise?
    feature_flag :code_scanning_alert, :code_scanning_integration
  end
end
