# rubocop:disable Style/FrozenStringLiteralComment

class RenderBlob < ApplicationRecord::Domain::Assets
  include ::Storage::Uploadable

  VALID_REPOSITORY_TYPES = %w(Repository Gist)

  belongs_to :repository, polymorphic: true
  belongs_to :storage_blob, class_name: "Storage::Blob"

  validates_presence_of :name
  validates_uniqueness_of :name, case_sensitive: true

  validates_presence_of :storage_blob
  validates_numericality_of :size, only_integer: true, greater_than: 0
  validate :storage_ensure_inner_asset
  validates_inclusion_of :repository_type, in: VALID_REPOSITORY_TYPES

  after_destroy :storage_remove_from_cluster

  enum state: Storage::Uploadable::STATES

  def storage_policy(actor: nil, repository: nil)
    klass = if GitHub.storage_cluster_enabled?
      ::Storage::ClusterPolicy
    else
      raise ArgumentError, "Local Render blob store is only enabled in cluster mode"
    end
    klass.new(self, actor: actor, repository: nil)
  end

  def self.storage_new(uploader, blob, meta)
    new(
      storage_blob: blob,
      content_type: meta[:content_type],
      name: meta[:name],
      size: meta[:size],
      path: meta[:path],
      ref: meta[:ref],
      repository_id: meta[:repository_id],
      repository_type: meta[:repository_type],
    )
  end

  def self.storage_create(uploader, blob, meta)
    storage_new(uploader, blob, meta).tap do |file|
      file.update(state: :uploaded)
    end
  end

  # RenderBlobs should be purged as soon as they are
  # removed, they're cheaper to regenerate than to
  # archive.
  def self.purge_at
    Time.now
  end

  def storage_blob_accessible?
    uploaded?
  end

  def creation_url
    "#{GitHub.storage_cluster_url}/render"
  end

  def storage_cluster_url(policy)
    creation_url + "/#{name}"
  end

  def storage_cluster_download_token(policy)
    return unless policy.actor

    scope = if repository.is_a? Gist
      RawBlob.gist_scope(repository, ref, path)
    else
      RawBlob.scope(repository, ref, path)
    end
    RawBlob.token(policy.actor, repository, scope, :render)
  end

  def self.render_token?(token)
    return false if GitHub.render_blob_storage_token.blank?
    SecurityUtils.secure_compare(token, GitHub.render_blob_storage_token)
  end

  def storage_cluster_upload_token(policy, expires: nil)
    if GitHub.render_blob_storage_token.present?
      return GitHub.render_blob_storage_token
    end

    # TODO: Remove once enterprise2 is configuring the render_blob_storage_token
    # and all enterprise2 tests are passing.
    if !policy.actor
      raise ArgumentError, "This #{self.class} storage policy needs an actor to create a token for uploads."
    end

    scope = if repository.is_a? Gist
      RawBlob.gist_scope(repository, ref, path)
    else
      RawBlob.scope(repository, ref, path)
    end
    RawBlob.token(policy.actor, repository, scope, :blob)
  end

  def alambic_download_headers(options = nil)
    { "Access-Control-Allow-Origin" => GitHub.render_host_url }
  end
end
