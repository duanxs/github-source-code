# rubocop:disable Style/FrozenStringLiteralComment

require "digest/sha2"

class Media::Blob < ApplicationRecord::Domain::Assets
  areas_of_responsibility :lfs
  BATCH_LIMIT = 100

  include Asset::AlambicCaller

  class CopyError < StandardError
  end

  self.table_name = :media_blobs

  include ::Storage::Uploadable
  include AssetUploadable
  include RawBlob::ContentHelpers

  enum state: {
    starter: 0,
    saved: 1,
    deleted: 2,
    verified: 3,
    archived: 4,
  }

  class << self
    attr_writer :network_copy_batch_size

    def network_copy_batch_size
      @network_copy_batch_size ||= 500
    end
  end

  belongs_to :repository_network
  belongs_to :originating_repository, class_name: "Repository"
  belongs_to :pusher, class_name: "User"
  belongs_to :asset
  belongs_to :storage_blob, class_name: "Storage::Blob"

  validate :storage_ensure_inner_asset, if: :verified?
  validates_presence_of :repository_network_id
  validate :oid_is_non_empty_sha256
  validates_numericality_of :size, greater_than: -1, less_than_or_equal_to: :max_blob_size
  validate :has_asset_status, if: :verified?

  after_destroy :dereference_asset
  after_destroy :storage_remove_from_cluster

  scope :purgeable, -> { where.not(state: :archived) }
  scope :restorable, -> { where(state: :archived) }
  scope :for_repository_network, -> (repo_network) { where(repository_network_id: repo_network.id) }

  REQUIRED_METADATA = []

  # BEGIN storage settings

  def storage_policy(actor: nil, repository: nil)
    klass = if GitHub.storage_cluster_enabled?
      ::Storage::ClusterPolicy
    else
      ::Storage::S3Policy
    end
    klass.new(self, actor: actor, repository: repository)
  end

  # Overrides Storage::Policy::ClassMethods#storage_auth_scope because Git LFS
  # objects do not track the content type.
  def self.storage_auth_scope(meta)
    super(meta.merge(
      original_type: nil,
      content_type: nil,
    ))
  end

  # cluster storage settings

  def self.storage_new(uploader, blob, meta)
    new(
      storage_blob: blob,
      pusher: uploader,
      repository_network_id: meta[:network_id],
      oid: meta[:oid],
      size: meta[:size],
    )
  end

  def self.storage_create(uploader, blob, meta)
    storage_new(uploader, blob, meta).tap do |file|
      file.update(state: :verified)
    end
  end

  def storage_blob_accessible?
    verified?
  end

  def storage_cluster_url(policy)
    "#{GitHub.storage_cluster_url}/lfs/#{policy.repository_id}/objects/#{oid}"
  end

  def storage_upload_path_info(policy)
    "/internal/storage/lfs/#{policy.repository_id}/objects/#{oid}"
  end

  alias storage_download_path_info storage_upload_path_info

  def storage_cluster_download_token(policy)
    super unless !GitHub.private_mode_enabled? && policy.public_repository?
  end

  def storage_cluster_upload_token_params
    {size: size}
  end

  # unnecessary, only downloaded through the lfs api
  def storage_external_url(_ = nil)
  end

  # s3 storage settings

  def storage_s3_key(policy)
    if storage_provider == :s3_production_data
      "#{repository_network_id}/#{oid}"
    else
      ["alambic", alambic_path_prefix, oid[0...2], oid[2...4], oid].join("/")
    end
  end

  def self.storage_s3_bucket
    GitHub.s3_environment_config[:asset_bucket_name]
  end

  def self.storage_s3_new_bucket
    "github-#{Rails.env.downcase}-media-blob-ea23ea"
  end

  def storage_s3_bucket
    if storage_provider == :s3_production_data
      self.class.storage_s3_new_bucket
    else
      self.class.storage_s3_bucket
    end
  end

  def storage_s3_access_key
    if storage_provider == :s3_production_data
      GitHub.s3_production_data_access_key
    else
      GitHub.s3_alambic_access_key
    end
  end

  def storage_s3_secret_key
    if storage_provider == :s3_production_data
      GitHub.s3_production_data_secret_key
    else
      GitHub.s3_alambic_secret_key
    end
  end

  def storage_download_expiration
    1.hour
  end

  def storage_s3_download_query(query)
    query[:token] = 1
  end

  # alambic storage settings

  def storage_alambic_url(policy)
    "#{GitHub.alambic_assets_url}/media/#{policy.repository_full_name}/object/#{oid}"
  end

  # END storage settings

  def archive
    return false if archived?

    if !verified?
      destroy
      return false
    end

    GitHub::SchemaDomain.allowing_cross_domain_transactions do
      transaction do
        if asset.nil?
          GitHub::Storage::Destroyer.dereference(self)
        else
          asset.archive!(self)
        end
        update!(state: :archived)
      end
    end

    true
  end

  def unarchive
    if archived? && has_blob_or_asset?
      GitHub::SchemaDomain.allowing_cross_domain_transactions do
        transaction do
          if asset.nil?
            GitHub::Storage::Creator.create_uploadable_references([self])
          else
            asset.reference!(self)
          end
          update!(state: :verified)
        end
      end

      return true
    end

    destroy unless has_blob_or_asset?
    false
  end

  def viewable?
    verified? && (owner_asset_status ? owner_asset_status.active? : true)
  end

  def owner_asset_status
    @owner_asset_status ||= Asset::Status.lfs.find_by(
      owner_id: responsible_owner_id,
    )
  end

  def set_verified_state!
    Asset.store(self) do |asset|
      asset.unarchive!(self) if archived?
      self.asset = asset
      self.state = :verified
      raise ActiveRecord::RecordInvalid.new(self) unless valid?

      num = self.class.
        where(id: id, oid: oid, size: size).
        where("(asset_id is null or asset_id = ?)", asset_id).
        update_all(asset_id: asset_id, state: self.class.states[state])

      if num != 1
        errors.add(:base, "Size or File mismatch: #{num.inspect}")
        raise ActiveRecord::RecordInvalid.new(self)
      end
    end
  end

  def verify!
    if !verified?
      http = Faraday.new(url: verify_url) do |b|
        if test_block = self.class.verify_test_block
          b.adapter(:test, &test_block)
        else
          b.adapter Faraday.default_adapter
        end
      end

      res = http.head(verify_url)
      if res.status != 200
        return false
      end
      set_verified_state!
    end
    true
  end

  def has_blob_or_asset?
    storage = if GitHub.storage_cluster_enabled?
      storage_blob
    else
      asset
    end

    storage.present?
  end

  def self.verify_test_block
    @verify_test_block
  end

  def self.reset_verify_test!
    @verify_test_block = nil
  end

  def self.stub_verify_test(blob, &block)
    @verify_test_block = lambda do |stub|
      stub.head("/#{blob.storage_s3_key(nil)}", &block)
    end
  end

  def self.surrogate_key_for(branch, path)
    hash = Digest::SHA2.new
    hash << Digest::SHA2.digest(branch)
    hash << " "
    hash << Digest::SHA2.digest(path)
    "media/#{hash}"
  end

  # Public: Counts the used storage for an owner.  This is potentially a slow
  # query, so it should be called from jobs and not live requests.
  #
  # NOTE: Unusable for GHE Storage Cluster since unique OIDs across multiple
  # repository networks are counted.
  #
  # owner  - A User or Organization.
  # cutoff - Optional Time specifying the latest created_at a Media::Blob should
  #          have to get counted.
  #
  # Returns the Integer total storage in bytes.
  def self.storage_by_owner(owner, cutoff: nil, group_size: 10)
    all_ids = possible_lfs_network_ids(owner)
    total_size = 0
    cutoff ||= Time.now

    ActiveRecord::Base.connected_to(role: :reading) do
      all_ids.in_groups_of(group_size) do |ids|
        network_ids = ids.compact
        next if network_ids.blank?
        total_size += where(repository_network_id: network_ids, state: 3)
          .where("created_at <= ?", cutoff).sum(:size)
      end
    end

    total_size
  end

  # Public
  def self.lfs_repositories(owner)
    Platform::Loaders::LfsRepositories.load(owner.id).sync
      .tap { |repos| GitHub::PrefillAssociations.for_repositories(repos) }
  end

  # Internal
  def self.possible_lfs_network_ids(owner)
    owner.repositories
      .where(active: true, parent_id: nil, locked: false)
      .group(:source_id)
      .pluck(:source_id)
  end

  def self.update_status_for_repository_transfer(repository, new_owner:, old_owner:)
    return unless in_network?(repository.network)
    new_owner.build_asset_status!
    new_owner.asset_status.rebuild
    old_owner.build_asset_status!
    old_owner.asset_status.rebuild
  end

  def self.in_network?(repository_network)
    !find_by_repository_network_id(repository_network.id).nil?
  end

  # Public: Gets the Media::Blob.
  #
  # repository - The Repository that is getting the Git LFS blob.  Note: the
  #              blob is actually stored with the RepositoryNetwork so it is
  #              accessible by any forks.
  # oid        - String ID of the content.  Should be a SHA-256 signature of the
  #              raw content.
  #
  # Returns a Media::Blob or nil if not found.
  def self.fetch(repository, oid)
    find_by(repository_network_id: repository.network_id, oid: oid)
  end

  # Public: Gets all Media::Blob objects for the given oids.
  #
  # repository - The Repository that is getting the Git LFS blob.  Note: the
  #              blob is actually stored with the RepositoryNetwork so it is
  #              accessible by any forks.
  # oid        - Array of String ID of the objects.
  #
  # Returns an Array of Media::Blob objects.
  def self.fetch_all(repository, oids)
    return [] if oids.blank?

    if oids.size > BATCH_LIMIT
      raise ArgumentError, "Expected up to 100 objects, got #{oids.size}."
    end

    where(repository_network_id: repository.network_id, oid: oids.uniq)
  end

  def self.fetch_by_id(repository, id)
    blob = find_by_id(id.to_i)
    blob && blob.in?(repository) && blob
  end

  # Public: Gets a page of Media::Blob records based on the given OID.
  def self.page(repository, oid: nil)
    cond = ["repository_network_id = ?", repository.network_id]
    if oid
      cond[0] << " AND oid > ?"
      cond    << oid
    end

    where(cond).order("repository_network_id, oid").limit(50)
  end

  def self.over_quota?(repo)
    return false if GitHub.enterprise?
    net = repo.network || raise(GitHub::DataQualityError.new(repo, :network))
    owner = net.owner || raise(GitHub::DataQualityError.new(net, :owner))
    owner.build_asset_status!
    !owner.asset_status.try(:active?)
  end

  def self.init_all(repository, pusher:, objects:)
    blob = new(repository_network_id: repository.network_id)
    blob.build_asset_status!
    root_repository_id = blob.root_repository_id

    oids = objects.map { |o| o[:oid] }
    blobs_by_oid = fetch_all(repository, oids).index_by(&:oid)

    new_oids = []
    rows = []
    now = blob.send(:current_time_from_proper_timezone)
    objects.each do |obj|
      oid, size = obj.values_at :oid, :size
      blob = blobs_by_oid[oid] ||= new(
        storage_blob: ::Storage::Blob.new(size: size, oid: oid),
        oid: oid,
        pusher: pusher,
        state: :starter,
        repository_network_id: repository.network_id,
        originating_repository_id: repository.id,
      )
      blob.size = size

      if blob.verified? || blob.archived?
        blob.errors.add(:size, "cannot change") if blob.size_changed?
      elsif blob.valid?
        new_oids << blob.oid
        rows << [
          blob.oid, blob.size, states[:saved], blob.pusher_id,
          blob.repository_network_id, blob.originating_repository_id, now
        ]
      end
    end

    if rows.any?
      github_sql.run <<-SQL, rows: GitHub::SQL::ROWS(rows)
        INSERT INTO media_blobs (`oid`, `size`, `state`, `pusher_id`,
          `repository_network_id`, `originating_repository_id`, `created_at`)
        VALUES :rows
        ON DUPLICATE KEY UPDATE
          `size` = IF(`state` = #{states[:verified]}, `size`, VALUES(`size`))
      SQL

      fetch_all(repository, new_oids).each do |blob|
        blobs_by_oid[blob.oid] = blob
      end
    end

    oids.map { |o| blobs_by_oid[o] }
  end

  # Public: Links a Git LFS blob to a Repository.  Creates the related records
  # as needed.
  #
  # repository - The Repository that is getting the Git LFS blob.  Note: the
  #              blob is actually stored with the RepositoryNetwork so it is
  #              accessible by any forks.
  # oid        - String ID of the content.  Should be a SHA-256 signature of the
  #              raw content.
  # meta       - Hash of the asset's meta data.  See Asset.upload for expected
  #              Asset meta values.
  #              :pusher - The User record that is uploading the asset.
  #
  # Returns a Media::Blob.
  def self.upload(repository, oid, meta)
    meta ||= {}
    # these meta values are ignored by Asset, but used in Media::Blob#after_upload.
    meta[:uploadable] = {
      repository: repository,
      pusher: meta.delete(:pusher),
    }

    media_blob = fetch(repository, oid)

    # If an archived model exists, we know the asset was uploaded and verified
    # once. We don't need to set the Asset meta data.  But we should require the
    # user to re-upload the file.
    if media_blob.try(:archived?)
      asset = media_blob.asset

      GitHub::SchemaDomain.allowing_cross_domain_transactions do
        transaction do
          asset.unarchive!(media_blob)
          media_blob.update!(state: :saved)
        end
      end

      return media_blob
    end

    Asset.upload(media_blob, oid, meta) do |asset|
      blob = create!(asset: asset, oid: oid, size: asset.size,
        charset: asset.charset, state: :saved,
        repository_network_id: repository.network_id)
      blob.build_asset_status!
      blob
    end
  end

  def self.auth_token(user, meta)
    user.signed_auth_token(scope: auth_scope(meta), expires: 1.hour.from_now)
  end

  def self.auth_scope(meta)
    "%s:%d:%s:%s" % [auth_scope_prefix, meta[:repository_id].to_i, meta[:committish], meta[:path]]
  end

  def self.auth_scope_prefix
    "Asset:%s" % self.name
  end

  def self.auth_scope_options(repository, committish, path)
    {repository_id: repository.id, committish: committish, path: path}
  end

  def self.pointer(data)
    return unless data
    Media::Pointer.parse(data.to_s)
  end

  def self.pointers_from_diff(diff)
    Media::Pointer.parse_diff(diff)
  end

  def public?
    root_repository.public?
  end

  def alambic_download_headers(options = nil)
    options ||= {}
    head = {
      "Access-Control-Allow-Origin" => GitHub.render_url_for_origin(options[:origin].to_s),
    }

    if max_age = options[:max_age]
      head["Cache-Control"] = "max-age=#{max_age.to_i}"
    end

    if path = options[:path]
      head["Surrogate-Key"] = "media/#{path}"
    end

    head
  end

  def self.dup_for_network(blobs, new_network)
    Array(blobs).map do |blob|
      GitHub::SchemaDomain.allowing_cross_domain_transactions do
        Media::Blob.create!(
          asset_id: blob.asset_id,
          storage_blob_id: blob.storage_blob_id,
          repository_network_id: new_network.id,
          oid: blob.oid,
          size: blob.size,
          charset: blob.charset,
          state: blob.state,
        )
      end
    end
  end

  def self.copy_for_network(transition, blobs, network)
    blobs = blobs.reject { |b| b.repository_network_id == network.id }
    unverified_blobs = blobs.reject { |b| b.verified? }
    if unverified_blobs.any?
      GitHub.dogstats.increment("lfs.copy_for_network.unverified_blobs")
      GitHub::Logger.log(class:                 "Media::Blob",
                         method:                "copy_for_network",
                         repository_network_id: transition.repository_network.id,
                        )
      return
    end

    return if blobs.blank?

    if !GitHub.storage_cluster_enabled?
      blobs.each do |b|
        b.copy_for_network(transition, network)
      end
      return
    end

    GitHub::SchemaDomain.allowing_cross_domain_transactions do
      transaction do
        new_blobs = dup_for_network(blobs, network)
        GitHub::Storage::Creator.create_uploadable_references(new_blobs)
      end
    end
  end

  # Internal: Copies the given Media::Blob into a new RepositoryNetwork.
  def copy_for_network(transition, new_network)
    res = http.post("/media/transitions") do |req|
      json = {
        transition_id: transition.id,
        blob_id: id,
        operation: transition.operation,
      }.to_json

      req.headers.update(
        "Content-Type" => "application/json",
        "Content-HMAC" => Api::Internal.content_hmac(json),
      )
      req.body = json
    end

    if res.status != 200
      raise CopyError, "Unsuccessful response: #{res.status}"
    end

    new_blobs = self.class.dup_for_network([self], new_network)
    new_blobs.each do |b|
      GitHub::SchemaDomain.allowing_cross_domain_transactions do
        transaction do
          asset.reference!(b)
          b.update!(
            pusher_id: pusher_id,
            originating_repository_id: originating_repository_id,
          )
        end
      end
    end
  end

  def safe_repository_network
    repository_network || raise(GitHub::DataQualityError.new(self, :repository_network))
  end

  def root_repository
    safe_repository_network.root || raise(GitHub::DataQualityError.new(self, :root_repository))
  end

  def repository_network_owner
    root_repository.plan_owner || raise(GitHub::DataQualityError.new(self, :plan_owner))
  end

  def root_repository_id
    root_repository.id
  end

  def repository_network_owner_id
    repository_network_owner.id
  end

  def responsible_owner_id
    repository_network_owner_id
  end

  # Deprecated: Legacy location of the file on GHE before the storage cluster
  # GitHub.storage_legacy_path => "/data/user/alambic_assets/shared"
  # /data/user/alambic_assets/shared/media/{id}/ab/cd/abcdef1234567890
  def alambic_absolute_local_path
    File.join(GitHub.storage_legacy_path, alambic_path_prefix, oid[0...2], oid[2...4], oid)
  end

  def alambic_path_prefix
    @alambic_path_prefix ||= "media/#{repository_network_id}"
  end

  def after_upload(meta)
    uploadable = meta[:uploadable]
    repo = uploadable && uploadable[:repository]
    pusher = uploadable[:pusher]
    if repo.nil? || pusher.nil?
      raise ArgumentError, "Expected :repository and :pusher in #{uploadable.inspect}"
    end

    self.pusher_id ||= pusher.id
    self.originating_repository_id ||= repo.id

    asset.update_meta(meta)
    save!
  end

  attr_accessor :path

  def content_type
    @content_type ||= raw_mime_for(@path)
  end

  def etag
    oid
  end

  def updated_at
    nil
  end

  # Only 1 Media::Blob exists per repository network, each with its own distinct
  # path prefix.  Therefore, always archive after deleting a media blob.
  def archive?(asset)
    asset.id == asset_id
  end

  def in?(repo)
    repository_network_id.to_i == (repo.try(:network_id) || repo.id)
  end

  def self.each_network_batch(network, last_id: nil, batch_size: network_copy_batch_size, readonly: false)
    network_id = network.respond_to?(:id) ? network.id : network.to_i
    start = last_id.nil? ? nil : last_id.to_i + 1
    enum = from("`media_blobs` IGNORE INDEX FOR ORDER BY (PRIMARY)").
      where(repository_network_id: network_id).
      find_in_batches(start: start, batch_size: batch_size)
    enum = GitHub::SQL::Readonly.new(enum) if readonly
    enum.each { |b| yield b }
  end

  def download_url(actor: nil, repo:)
    storage_policy(actor: actor, repository: repo).download_url
  end

  def verify_url
    S3Sign.query(self.class.s3_credentials, "HEAD", storage_s3_key(nil), 60).location
  end

  def download_link(actor: nil, repo:)
    storage_policy(actor: actor, repository: repo).download_link
  end

  def upload_link(actor:, repo:)
    # TODO: replace with Storage::Policy#policy_hash
    policy = storage_policy(actor: actor, repository: repo)
    now = Time.now
    if use_storage_cluster?
      {
        href: storage_cluster_url(policy),
        header: {
          "Accept" => "application/vnd.github.smasher+json",
          "Authorization" => "RemoteAuth #{storage_cluster_upload_token(policy)}",
        },
        expires_in: 1.hour.to_i,
      }
    else
      sign = S3Sign.header(self.class.s3_credentials, "PUT", storage_s3_key(nil), oid)
      sign.query[:actor_id] = actor.id
      sign.query[:repo_id] = repo.id
      {
        href: sign.location,
        header: sign.to_hash,
        expires_at: sign.expires_at.xmlschema,
        expires_in: sign.expires.to_i,
      }
    end
  ensure
    policy.stats_timing(:upload, start: now) if now && policy
  end

  def verify_token(user)
    user.signed_auth_token(scope: verify_scope, expires: 1.day.from_now)
  end

  def self.verify_scope(repository_network_id, oid)
    "LFS:verify:%d:%s" % [repository_network_id, oid]
  end

  def verify_scope
    self.class.verify_scope(repository_network_id, oid)
  end

  def use_storage_cluster?
    GitHub.storage_cluster_enabled?
  end

  def build_asset_status!
    if user = User.find_by_id(responsible_owner_id.to_i)
      user.build_asset_status!
    else
      raise "invalid user: #{responsible_owner_id.inspect}"
    end
  end

  def self.s3_credentials
    {
      bucket: GitHub.s3_environment_config[:asset_bucket_name],
      key: GitHub.s3_alambic_access_key,
      secret: GitHub.s3_alambic_secret_key,
    }
  end

  def self.stub_copy(&block)
    stub_http do |stub|
      stub.post("/media/transitions", &block)
    end
  end

  def self.stub_copy!
    stub_copy do |env|
      verify_env = {
        Api::Internal::REQUEST_METHOD => "POST",
        Api::Internal::CONTENT_HMAC_KEY => env[:request_headers]["Content-HMAC"],
      }
      hmac_status = Api::Internal.verify_content_hmac(verify_env, env[:body])
      body = JSON.parse env[:body]
      transition = Media::Transition.find_by_id(body["transition_id"].to_i)
      blob = Media::Blob.find_by_id(body["blob_id"].to_i)

      if hmac_status != :success
        [403, {}, "bad hmac: #{hmac_status}"]
      elsif transition.nil?
        [422, {}, "no transition"]
      elsif blob.nil?
        [422, {}, "no blob"]
      elsif blob.repository_network_id != transition.old_repository_network_id
        [422, {}, "different networks"]
      elsif body["operation"] != "copying"
        [422, {}, "operation #{body["operation"]} failed"]
      else
        [200, {}, "ok"]
      end
    end
  end

  private

  EMPTY_OID = "0"*64

  def oid_is_non_empty_sha256
    if oid.blank?
      errors.add(:oid, "is required")
    elsif oid == EMPTY_OID || oid !~ /\A[0-9A-F]{64}\Z/i
      errors.add(:oid, "is invalid")
    end
  end

  def has_asset_status
    return if deleted?
    return if errors.any?
    if user = User.find_by_id(responsible_owner_id.to_i)
      user.build_asset_status!
    else
      errors.add :base, "invalid user: #{responsible_owner_id.inspect}"
    end
  end

  def verified_on_cluster?
    GitHub.storage_cluster_enabled? && verified?
  end

  def verified_on_s3?
    !GitHub.storage_cluster_enabled? && verified?
  end

  def max_blob_size
    # We will always have a repository root except when the network has been
    # destroyed and we're archiving blobs.  In such a case, we need to default
    # to the largest possible value so that we don't fail validation for large
    # blobs.
    repository_network&.root&.plan_limit(:media_blob_max_size) || 5.gigabytes
  end
end
