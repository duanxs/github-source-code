# rubocop:disable Style/FrozenStringLiteralComment

class Media::Transition < ApplicationRecord::Domain::Assets
  # raised when trying to copy a Media::Blob to a repository network that no
  # longer exists.
  class NoDestinationError < StandardError
  end

  self.table_name = :media_transitions

  areas_of_responsibility :lfs

  enum operation: { copying: 0, deleting: 1, restoring: 2 }

  belongs_to :repository_network
  belongs_to :repository_network_owner, class_name: "User"
  belongs_to :old_repository_network, class_name: "RepositoryNetwork"

  validates_presence_of :repository_network_id
  validates :operation, presence: true
  after_destroy :log_transition_results

  def self.by_network(network)
    find_by_repository_network_id(network)
  end

  def self.async_copy(old_network, new_network)
    return false unless Media::Blob.in_network?(old_network)
    create!(
      repository_network: new_network,
      repository_network_owner: new_network.owner,
      old_repository_network: old_network,
      operation: operations["copying"],
    ).async_perform
    true
  end

  def self.async_delete(network)
    return false unless Media::Blob.in_network?(network)
    create!(
      repository_network: network,
      repository_network_owner: network_owner_from_deleted_root(network),
      operation: operations["deleting"],
    )
    true
  end

  def self.async_restore(network)
    return false unless Media::Blob.in_network?(network)

    unqueue_task(network, "deleting")

    create!(
      repository_network: network,
      repository_network_owner: network.owner,
      operation: operations["restoring"],
    )
    true
  end

  def self.unqueue_task(network, type)
    tasks = self.where(
      "repository_network_id = :network_id OR old_repository_network_id = :network_id",
      network_id: network.id,
    ).
    where(operation: operations[type]).
    order(id: :asc)

    tasks.each &:destroy
  end

  def self.network_owner_from_deleted_root(network)
    root = network.root ||
      Archived::Repository.find_by_id(network.root_id) ||
      raise(GitHub::DataQualityError.new(network, :root))
    root.owner
  end

  # Public: Verifies that a given operation and blob match this transition.
  def verify_blob(op, blob)
    if op != operation
      return :operation
    end

    if !blob.is_a?(Media::Blob)
      return :not_blob
    end

    matching_network_id = case operation
    when "copying" then old_repository_network_id
    when "deleting" then repository_network_id
    else raise "Bad operation for transition #{id}: #{operation}"
    end

    if blob.repository_network_id != matching_network_id
      return :wrong_network
    end

    :ok
  end

  def earlier_transition?
    network_ids = [repository_network_id, old_repository_network_id]
    network_ids.compact!

    transition_id = self.class.where(
      "repository_network_id IN (?) OR old_repository_network_id IN (?)",
      network_ids, network_ids
    ).order("id ASC").pluck(:id).first

    if transition_id
      transition_id < id
    else
      false
    end
  end

  def async_perform
    options = {"id" => id}
    if Rails.test?
      TransitionMediaBlobsJob.perform_now(options)
    else
      TransitionMediaBlobsJob.perform_later(options)
    end
  end

  def perform
    return if earlier_transition?

    github_sql.new(
      id: id,
      run_at: current_time_from_proper_timezone,
    ).run %Q(
      UPDATE media_transitions SET
        runs = runs + 1,
        run_at = :run_at
      WHERE id = :id
    )

    send("perform_#{operation}")

    timing = ((Time.now.to_f - created_at.to_f) * 1000).round
    GitHub.dogstats.timing("lfs.transitions", timing, tags: datadog_tags)

    if owner = repository_network_owner
      owner.asset_status.try(:rebuild)
    end

    destroy
  end

  private

  def perform_copying
    if repository_network.nil?
      return if runs > 4 # we've given enough attempts for db repl lag
      raise NoDestinationError, "No destination repository network: #{repository_network_id}"
    end

    each_network_batch old_repository_network_id do |blobs|
      existing = Media::Blob.where(
        repository_network_id: repository_network_id,
        oid: blobs.map(&:oid),
      ).index_by(&:oid)

      Media::Blob.copy_for_network(
        self,
        blobs.reject { |b| existing[b.oid] || invalid_blob?(b) },
        repository_network,
      )
    end
  end

  def perform_deleting
    each_network_batch repository_network_id do |blobs|
      blobs.each &:archive
    end
  end

  def perform_restoring
    each_network_batch repository_network_id do |blobs|
      blobs.each &:unarchive
    end
  end

  def invalid_blob?(blob)
    if GitHub.storage_cluster_enabled?
      blob.storage_blob.nil?
    else
      blob.asset.nil?
    end
  end

  def log_transition_results
    log = {
      now: Time.now.iso8601,
      ns: "Media::Transition",
      fn: operation,
      repository_network: repository_network_id,
      runs: runs,
      created_at: created_at.iso8601,
    }

    if id = old_repository_network_id
      log[:old_repository_network] = id
    end

    if time = run_at
      log[:run_at] = time.iso8601
    end

    GitHub::Logger.log(log)
  end

  def each_network_batch(network_id, readonly: false)
    current_last_id = last_blob_id
    Media::Blob.each_network_batch(network_id, last_id: current_last_id, readonly: readonly) do |blobs|
      Media::Blob.throttle { yield blobs }
      current_last_id = blobs.map(&:id).max
      GitHub.dogstats.count("lfs.transitions.blobs", blobs.size, tags: datadog_tags)
    end
  ensure
    update!(last_blob_id: current_last_id.to_i)
  end

  def datadog_tags
    @datadog_tags ||= ["op:#{operation}"]
  end
end
