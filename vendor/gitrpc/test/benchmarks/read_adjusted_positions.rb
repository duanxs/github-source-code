# frozen_string_literal: true

# this file must be loaded explicitly via `script/testsuite test/benchmarks/read_adjusted_positions.rb`

require "gitrpc"
require "repository_fixture"
require "minitest/benchmark"
require "pry"

class BenchAdjustedPositions < Minitest::Benchmark
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    data = {}
    file_range.to_a.each do |i|
      content = line_range_text(line_range, i)
      data["file#{i}.txt"] = content
    end

    @c1 = @fixture.commit_files(data, "commit1")

    data = {}
    file_range.to_a.each do |i|
      content = "intro\nlol\nfoobar\n\n" +
                line_range_text(0..33, i) +
                line_range_text(45..88, i) +
                "awesome content!\n" +
                line_range_text(89..99, i)
      data["file#{i}.txt"] = content
    end

    @c2 = @fixture.commit_files(data, "commit2")

    @repo = Rugged::Repository.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
  end

  def bench_encoding
    warmup.times { BERT.encode(reduced_commit_data) }
    assert_performance_linear do |n|
      BERT.encode(reduced_commit_data(n))
    end
  end

  def bench_decoding
    data = self.class.bench_range.map { |n| BERT.encode(reduced_commit_data(n)) }
    encoded_map = Hash[self.class.bench_range.zip(data)]

    warmup.times { BERT.decode(encoded_map[self.class.bench_range.last]) }

    assert_performance_linear do |n|
      BERT.decode(encoded_map[n])
    end
  end

  # bench positioning for 1, 10, and 100 blob oid pairs (100, 1000, and 10000 comments respectively)
  def bench_blob_oids
    backend = GitRPC::Backend.new(@fixture.path)
    warmup.times { backend.read_adjusted_positions(reduced_blob_data) }

    assert_performance_linear do |n|
      backend.read_adjusted_positions(reduced_blob_data(n))
    end
  end

  def bench_commit_oids
    backend = GitRPC::Backend.new(@fixture.path)
    backend.read_adjusted_positions(reduced_commit_data)
    warmup.times { backend.read_adjusted_positions(reduced_commit_data) }

    assert_performance_linear do |n|
      backend.read_adjusted_positions(reduced_commit_data(n))
    end
  end

  def bench_commit_oids_direct_over_wire
    client = GitRPC.new("fakerpc:#{@fixture.path}", cache: GitRPC::Util::HashCache.new)

    warmup.times { client.send_message(:read_adjusted_positions, reduced_commit_data) }

    assert_performance_linear do |n|
      client.send_message(:read_adjusted_positions, reduced_commit_data(n))
    end
  end

  def bench_commit_oids_via_client
    client = GitRPC.new("fakerpc:#{@fixture.path}", cache: GitRPC::Util::HashCache.new)

    warmup.times { client.read_commit_adjusted_positions_with_base(expanded_commit_data) }

    assert_performance_linear do |n|
      client.read_commit_adjusted_positions_with_base(expanded_commit_data(n))
    end
  end

  def self.bench_range
    [1, 10, 100, 1000]
  end

  private

  def line_range_text(rng, n)
    rng.map { |i| "offset #{i}, file #{n}\n" }.join
  end

  def file_range
    1..1000
  end

  def line_range
    1..100
  end

  def warmup
    5
  end

  def reduced_commit_data(n = self.class.bench_range.last)
    return @reduced_commit_data[n] if defined?(@reduced_commit_data)

    @reduced_commit_data = {}

    self.class.bench_range.each do |n|
      positioning_data = []
      (1..n).each do |i|
        positioning_data << [line_range.to_a, {commit_oid: @c1, path: "file#{i}.txt"}, {commit_oid: @c2, path: "file#{i}.txt"}]
      end
      @reduced_commit_data[n] = positioning_data
    end

    @reduced_commit_data[n]
  end

  def expanded_commit_data(n = self.class.bench_range.last)
    return @expanded_commit_data[n] if defined?(@expanded_commit_data)

    @expanded_commit_data = {}

    self.class.bench_range.each do |m|
      positioning_data = []
      (1..m).each do |i|
        line_range.each do |j|
          positioning_data << { position: j, source:      { commit_oid: @c1, path: "file#{i}.txt" },
                                             destination: { commit_oid: @c2, path: "file#{i}.txt" } }
        end
      end
      @expanded_commit_data[m] = positioning_data
    end

    @expanded_commit_data[n]
  end

  def reduced_blob_data(n = self.class.bench_range.last)
    return @reduced_blob_data[n] if defined?(@reduced_blob_data)

    @reduced_blob_data = {}

    self.class.bench_range.each do |n|
      positioning_data = []
      (1..n).each do |i|
        b1 = @repo.blob_at(@c1, "file#{i}.txt")
        b2 = @repo.blob_at(@c2, "file#{i}.txt")
        positioning_data << [line_range.to_a, b1.oid, b2.oid]
      end
      @reduced_blob_data[n] = positioning_data
    end

    @reduced_blob_data[n]
  end
end
