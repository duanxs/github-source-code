# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/util/hashcache"

class HashCacheTest < Minitest::Test
  def setup
    @cache = GitRPC::Util::HashCache.new
  end

  def test_starts_with_empty_cache
    assert_equal Hash.new, @cache.hash
  end

  def test_set_a_key
    refute @cache.hash.key?("key")
    @cache.set("key", "value")
    assert @cache.hash.key?("key")
  end

  def test_set_and_get_a_key
    @cache.set("key", "value")
    assert_equal "value", @cache.get("key")
  end

  def test_get_a_missing_key
    assert_nil @cache.get("key")
  end

  def test_fetch_a_key_to_set_it
    refute @cache.hash.key?("key")

    assert_equal "value", @cache.fetch("key") { "value" }
    assert_equal "value", @cache.get("key")
  end

  def test_fetching_a_set_key_does_not_execute_block
    @cache.set("key", "value")
    assert_equal "value", @cache.fetch("key") { fail "this block shouldn't be run" }
  end

  def test_delete_a_key
    refute @cache.hash.key?("key")
    @cache.set("key", "value")
    assert @cache.hash.key?("key")
    @cache.delete("key")
    refute @cache.hash.key?("key")
  end

  def test_get_multi
    (1..5).each { |i|  @cache.set("key#{i}", "value#{i}") }

    lookup   = %w[key1 key2 key4 key6 key8]
    expected = {"key1" => "value1", "key2" => "value2", "key4" => "value4"}

    # the keys are flattened, so either one works
    assert_equal expected, @cache.get_multi(lookup)
    assert_equal expected, @cache.get_multi(*lookup)
  end
end
