# frozen_string_literal: true
require_relative "setup"

require "gitrpc"
require "repository_fixture"

class ProtocolTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
  end

  def teardown
    @fixture.teardown
  end

  def test_resolve
    proto = GitRPC::Protocol.resolve("file:#{@fixture.path}")
  end

  def test_resolve_with_options
    proto = GitRPC::Protocol.resolve("file:#{@fixture.path}", :max => 1000)
    assert_equal 1000, proto.options[:max]
  end

  def test_resolve_local
    proto = GitRPC::Protocol.resolve("local:/")
    assert_nil proto
  end

  class FakeProtocol < Struct.new(:url, :options)
  end

  def test_resolve_string
    GitRPC::Protocol.register("fake", ProtocolTest::FakeProtocol)
    proto = GitRPC::Protocol.resolve("fake://example.com:1234/some/repo.git")

    assert_equal FakeProtocol, GitRPC::Protocol.registry["fake"]

    assert proto.is_a?(FakeProtocol)
    assert_equal "example.com", proto.url.host
    assert_equal 1234, proto.url.port
    assert_equal "/some/repo.git", proto.url.path
  end

  def test_resolve_missing_protocol
    assert_raises GitRPC::NoSuchProtocol do
      GitRPC::Protocol.resolve("noooope://example.com/some/repo.git")
    end
  end

  class DGitDelegate
    attr_reader :namespace, :id, :shard_path

    def initialize(namespace, id, shard_path)
      @namespace, @id, @shard_path = namespace, id, shard_path
    end

    def setup; end
  end

  def test_disagreement_possible
    proto = GitRPC::Protocol.resolve("file:#{@fixture.path}")
    assert !proto.disagreement_possible?

    proto = GitRPC::Protocol.resolve("bertrpc://localhost:1234/foo")
    assert !proto.disagreement_possible?

    proto = GitRPC::Protocol.resolve(
      "dgit:/",
      :delegate => DGitDelegate.new("network", 1234, "/foo"),
    )
    assert proto.disagreement_possible?
  end
end
