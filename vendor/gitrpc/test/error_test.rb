# frozen_string_literal: true
require_relative "setup"

require "gitrpc/error"

class TestError < Minitest::Test
  class TestException < StandardError
  end

  def test_initialize_message_only
    error = exception { raise GitRPC::Error, "the message" }
    assert_equal "the message", error.message
    assert error.original.nil?
  end

  def test_initialize_original_only
    original = exception { raise TestException, "the message" }
    assert original.is_a?(TestException)

    error = exception { raise GitRPC::Error, original }
    assert error.is_a?(GitRPC::Error)

    assert_equal original.message, error.message
    assert_same original, error.original
  end

  def test_initialize_message_and_original
    original = exception { raise TestException, "the message" }
    assert original.is_a?(TestException)

    error = exception { raise GitRPC::Error.new("diff message", original) }
    raise error if !error.is_a?(GitRPC::Error)

    assert_equal "diff message", error.message
    assert_same original, error.original
  end

  def exception
    yield
  rescue => boom
    boom
  end
end
