# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"
require "gitrpc/util/hashcache"

require "minitest/mock"

class ClientReadRefsTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => @cache)

    setup_test_commits
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @fixture.add_empty_commit("special commit")
    @fixture.command "git update-ref refs/special/foo $(git rev-parse HEAD)"
    @special_oid = @fixture.rev_parse("refs/special/foo")

    # add a tag that could be easily confused as an existing branch with a path
    @fixture.add_empty_commit("release commit")
    @fixture.command "git update-ref refs/tags/master/v1.0 $(git rev-parse HEAD)"
    @tag_oid = @fixture.rev_parse("refs/tags/master/v1.0")

    @fixture.add_empty_commit("master commit")
    @master_oid = @fixture.rev_parse("HEAD")
  end

  def test_caching_filtered_refs_writes_to_cache
    hash = @client.read_refs
    expect = {
      "refs/heads/master" => @master_oid,
      "refs/tags/master/v1.0" => @tag_oid
    }
    assert_equal expect, hash

    assert 1, @client.cache.hash.size
    assert_equal expect, @client.cache.get(@client.refs_key)
  end

  def test_caching_filtered_refs_does_not_invoke_backend
    hash = @client.read_refs
    expect = {
      "refs/heads/master" => @master_oid,
      "refs/tags/master/v1.0" => @tag_oid

    }
    @client.backend.stub :send_message, lambda { |_message, *args| fail } do
      assert_equal expect, @client.read_refs
      assert_equal expect, @client.read_refs
      assert_equal expect, @client.read_refs("default")
    end
  end

  def test_caching_all_refs_does_not_invoke_backend
    hash = @client.read_refs("extended")
    expect = {
      "refs/heads/master" => @master_oid,
      "refs/special/foo" => @special_oid,
      "refs/tags/master/v1.0" => @tag_oid
    }
    @client.backend.stub :send_message, lambda { |_message, *args| fail } do
      assert_equal expect, @client.read_refs("extended")
      assert_equal expect, @client.read_refs("extended")
    end
  end

  def test_invalid_arg_to_read_refs
    assert_raises ArgumentError do
      @client.read_refs("efault")
    end
  end

  def test_read_qualified_refs_does_not_invoke_backend
    result = @client.read_qualified_refs(["refs/heads/master", "refs/special/foo"])
    assert_equal [["refs/heads/master", @master_oid], ["refs/special/foo", @special_oid]], result

    @client.backend.stub :send_message, lambda { |_message, *args| fail } do
      assert_equal result, @client.read_qualified_refs(["refs/heads/master", "refs/special/foo"])
      assert_equal [["refs/heads/master", @master_oid]], @client.read_qualified_refs(["refs/heads/master"])
      assert_equal [["refs/special/foo", @special_oid]], @client.read_qualified_refs(["refs/special/foo"])
    end
  end

  def test_ref_counts
    assert_equal({ branches: 1, tags: 1, references: 3 }, @client.ref_counts)
  end

  def test_ref_counts_are_cached
    assert_equal({ branches: 1, tags: 1, references: 3 }, @client.ref_counts)

    @fixture.command "git tag v1.0 HEAD"
    assert_equal({ branches: 1, tags: 1, references: 3 }, @client.ref_counts)

    @client.clear_repository_reference_key!
    assert_equal({ branches: 1, tags: 2, references: 4 }, @client.ref_counts)
  end

  def test_read_head_oid
    assert_equal @master_oid, @client.read_head_oid
  end

  def test_read_ref_from_path_with_existing_ref
    assert_equal ["refs/heads/master", @master_oid], @client.read_ref_from_path("master/foo/bar/baz")
  end

  def test_read_ref_from_tag_that_looks_like_a_path
    assert_equal ["refs/tags/master/v1.0", @tag_oid], @client.read_ref_from_path("master/v1.0")
  end

  def test_read_ref_from_path_with_limit
    assert_equal ["refs/heads/master", @master_oid], @client.read_ref_from_path("master/foo/bar/baz", limit: 10)
    assert_equal ["refs/heads/master", @master_oid], @client.read_ref_from_path("master/foo/bar/baz", limit: 7)
    assert_equal ["refs/heads/master", @master_oid], @client.read_ref_from_path("master/foo/bar/baz", limit: 6)
    assert_nil @client.read_ref_from_path("master/foo/bar/baz", limit: 5)
  end

  def test_read_ref_from_path_with_non_existing_ref
    assert_nil @client.read_ref_from_path("does-not-exist")
  end
end
