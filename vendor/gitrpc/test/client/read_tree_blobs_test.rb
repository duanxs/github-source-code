# rubocop:disable Style/FrozenStringLiteralComment

require_relative "../setup"

require "gitrpc"
require "repository_fixture"
require "gitrpc/util/hashcache"
require "rugged"

class ClientReadTreeBlobsTest < Minitest::Test
  include GitRPC::Util

  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    setup_test_files
    @rugged = Rugged::Repository.new(@fixture.path)
    @tree_id = @rugged.lookup(@fixture.rev_parse("HEAD")).tree.oid

    @cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => @cache)
  end

  def setup_test_files
    @png_data = "\211PNG\r\n\032\n\000\000\000\rIHDR\000\000\000@\000\000\000@\004\003\000\000\000XGl\355\000\000\000'PLTE\377\377\377555\n\n\n...***&&&!!!\034\034\034\030\030\030\024\024\024\021\021\021\r\r\r2222\352\267\375\000\000\000\003tRNS\000\231\231\323&\336m\000\000\000JIDATx^\355\3121\001\200 \000EA*X\301\nT\260\002\025\254@\005+P\201\nT0\224\333\333\336\342\302\362o\276\262MD\034\202\360\nB\025\204K\020\232 \334\202\320\005\341\021\204!\bS\020\226 \234\242\374\023\021\037\023\000|Q\332\035Y\253\000\000\000\000IEND\256B`\202"
    @png_data.force_encoding("BINARY") if @png_data.respond_to?(:force_encoding)
    @fixture.commit_files({
      "README.md" => "# Example Readme\nWith some test content",
      "User.rb" => "class User\nend\n",
      "Repository.rb" => "class Repository\nend\n",
      "js/testing.js" => "function test() {\n  console.log('testing')\n}",
      "lib/png.rb" => "module PNG\nend\n",
      "lib/png/first.rb" => "module PNG\n  class First\n  end\nend\n",
      "lib/png/second.rb" => "module PNG\n  class Second\n  end\nend\n",
      "lib/png/third.rb" => "module PNG\n  class Third\n  end\nend\n",
      "lib/png/fourth.rb" => "module PNG\n  class Fourth\n  end\nend\n",
      "lib/png/fifth.rb" => "module PNG\n  class Fifth\n  end\nend\n",
      "lib/png/sixth.rb" => "module PNG\n  class Sixth\n  end\nend\n",
      "lib/png/seventh.rb" => "module PNG\n  class Seventh\n  end\nend\n",
      "heavy_minus_sign.png" => @png_data,
      "models/concerns/Billable.rb" => "module Billable\nend\n"
    }, "add first files")

    @fixture.commit_files({
      "README.md" => "# Example Readme\nWith some test content\nEven more"
    }, "add second files")
  end

  def teardown
    @fixture.teardown
  end

  def test_read_tree_blobs
    files = @client.read_tree_blobs(@tree_id)

    assert_equal 4, files.size
    readme = files.find { |f| f["name"] == "README.md" }
    refute_nil readme
    assert_equal 49, readme["size"]
    assert_equal "5768c7f9f3eeae76626e978dd7a2f6076e5de0ee", readme["oid"]
    assert_equal 33188, readme["mode"]

    png = files.find { |f| f["name"] == "heavy_minus_sign.png" }
    assert_equal @png_data.size, png["size"]

    refute_nil files.find { |f| f["name"] == "User.rb" }
    refute_nil files.find { |f| f["name"] == "Repository.rb" }
  end

  def test_read_tree_blobs_from_specific_version
    versions = @fixture.rev_list("HEAD", 2)

    files = @client.read_tree_blobs(@rugged.lookup(versions[1]).tree.oid)
    readme = files.find { |f| f["name"] == "README.md" }
    assert_equal 39, readme["size"]

    files = @client.read_tree_blobs(@rugged.lookup(versions[0]).tree.oid)
    readme = files.find { |f| f["name"] == "README.md" }
    assert_equal 49, readme["size"]
  end

  def test_read_tree_blobs_from_subtree
    files = @client.read_tree_blobs(@tree_id, path = "js")

    assert_equal 1, files.size

    testing_js = files.first
    assert_equal "testing.js", testing_js["name"]
    assert_equal 44, testing_js["size"]
  end

  def test_read_tree_blobs_from_subtree_with_slash
    files = @client.read_tree_blobs(@tree_id, path = "/js")

    assert_equal 1, files.size

    testing_js = files.first
    assert_equal "testing.js", testing_js["name"]
    assert_equal 44, testing_js["size"]
  end

  def test_read_tree_blobs_recursive
    files = @client.read_tree_blobs_recursive(@tree_id)
    assert_equal 14, files.size

    assert_equal "README.md", files[0]["name"]
    assert_equal "Repository.rb", files[1]["name"]
    assert_equal "User.rb", files[2]["name"]
    assert_equal "heavy_minus_sign.png", files[3]["name"]
    assert_equal "js/testing.js", files[4]["name"]
    assert_equal "lib/png.rb", files[5]["name"]
    assert_equal "lib/png/fifth.rb", files[6]["name"]
    assert_equal "lib/png/first.rb", files[7]["name"]
    assert_equal "lib/png/fourth.rb", files[8]["name"]
    assert_equal "lib/png/second.rb", files[9]["name"]
    assert_equal "lib/png/seventh.rb", files[10]["name"]
    assert_equal "lib/png/sixth.rb", files[11]["name"]
    assert_equal "lib/png/third.rb", files[12]["name"]
    assert_equal "models/concerns/Billable.rb", files[13]["name"]

    testing_js = files[4]
    assert_equal 44, testing_js["size"]
  end

  def test_read_tree_blobs_by_page
    files = []
    set_page_size 5

    hash = @client.read_tree_blobs_by_page(@tree_id, 0)
    files.concat hash["blobs"]
    assert_equal 0, hash["current_page"]
    assert_equal 1, hash["next_page"]
    assert_equal 5, hash["blobs"].length

    hash = @client.read_tree_blobs_by_page(@tree_id, 1)
    files.concat hash["blobs"]
    assert_equal 1, hash["current_page"]
    assert_equal 2, hash["next_page"]
    assert_equal 5, hash["blobs"].length

    hash = @client.read_tree_blobs_by_page(@tree_id, 2)
    files.concat hash["blobs"]
    assert_equal 2, hash["current_page"]
    assert_nil hash["next_page"]
    assert_equal 4, hash["blobs"].length

    assert_equal "README.md", files[0]["name"]
    assert_equal "Repository.rb", files[1]["name"]
    assert_equal "User.rb", files[2]["name"]
    assert_equal "heavy_minus_sign.png", files[3]["name"]
    assert_equal "js/testing.js", files[4]["name"]
    assert_equal "lib/png.rb", files[5]["name"]
    assert_equal "lib/png/fifth.rb", files[6]["name"]
    assert_equal "lib/png/first.rb", files[7]["name"]
    assert_equal "lib/png/fourth.rb", files[8]["name"]
    assert_equal "lib/png/second.rb", files[9]["name"]
    assert_equal "lib/png/seventh.rb", files[10]["name"]
    assert_equal "lib/png/sixth.rb", files[11]["name"]
    assert_equal "lib/png/third.rb", files[12]["name"]
    assert_equal "models/concerns/Billable.rb", files[13]["name"]
  end

  def test_read_tree_blobs_by_page_fence_posting
    set_page_size 7
    hash = @client.read_tree_blobs_by_page(@tree_id, 0)

    assert_equal 0, hash["current_page"]
    assert_equal 1, hash["next_page"]
    assert_equal 7, hash["blobs"].length

    files = hash["blobs"]
    assert_equal "README.md", files[0]["name"]
    assert_equal "Repository.rb", files[1]["name"]
    assert_equal "User.rb", files[2]["name"]
    assert_equal "heavy_minus_sign.png", files[3]["name"]
    assert_equal "js/testing.js", files[4]["name"]
    assert_equal "lib/png.rb", files[5]["name"]
    assert_equal "lib/png/fifth.rb", files[6]["name"]

    hash = @client.read_tree_blobs_by_page(@tree_id, 1)

    assert_equal 1, hash["current_page"]
    assert_nil hash["next_page"]
    assert_equal 7, hash["blobs"].length

    files = hash["blobs"]
    assert_equal "lib/png/first.rb", files[0]["name"]
    assert_equal "lib/png/fourth.rb", files[1]["name"]
    assert_equal "lib/png/second.rb", files[2]["name"]
    assert_equal "lib/png/seventh.rb", files[3]["name"]
    assert_equal "lib/png/sixth.rb", files[4]["name"]
    assert_equal "lib/png/third.rb", files[5]["name"]
    assert_equal "models/concerns/Billable.rb", files[6]["name"]
  end

  def test_raise_invalid_full_oid
    assert_raises GitRPC::InvalidFullOid do
      @client.read_tree_blobs(nil)
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.read_tree_blobs("lol")
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.read_tree_blobs_recursive(nil)
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.read_tree_blobs_recursive("lol")
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.read_tree_blobs_by_page(nil, 0)
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.read_tree_blobs_by_page("lol", 0)
    end
  end

  def set_page_size(per_page)
    GitRPC::Backend.__send__(:remove_const, "BLOBS_PER_PAGE")
    GitRPC::Backend.const_set("BLOBS_PER_PAGE", per_page)
  end
end
