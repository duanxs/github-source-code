# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"
require "gitrpc/util/hashcache"

class ClientDiskTest < Minitest::Test
  def setup
    @network_path = File.expand_path("../12345")
    FileUtils.rm_rf(@network_path)
    Dir.mkdir(@network_path)
    @fixture = RepositoryFixture.new("#{@network_path}/6789.git")

    @cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => @cache)
    @network_client = GitRPC.new("fakerpc:#{@network_path}", :cache => @cache)
  end

  def teardown
    @fixture.teardown
    FileUtils.rm_rf(@network_path)
  end

  def test_init
    @client.init
  end

  def test_remove
    @client.remove
  end

  def test_exist
    assert !@client.exist?
    @fixture.setup
    assert @client.exist?
  end

  def test_ensure_dir
    @fixture.setup
    refute File.exist?("#{@fixture.path}/foo/bar")
    @client.ensure_dir("foo/bar")
    assert File.exist?("#{@fixture.path}/foo/bar")

    assert_raises GitRPC::IllegalPath do
      @client.ensure_dir("/tmp")
    end
    assert_raises GitRPC::IllegalPath do
      @client.ensure_dir("#{@fixture.path}/../foo")
    end
  end

  def test_network_exist
    @fixture.setup
    assert @client.exist?
    assert !@client.network_exist?

    assert !@network_client.exist?
    assert @network_client.network_exist?
  end

  def test_network_remove
    @fixture.setup
    assert File.exist?(@network_path)
    assert File.exist?(@fixture.path)

    # no op
    assert !@client.network_remove
    assert File.exist?(@network_path)
    assert File.exist?(@fixture.path)

    # actually remove it
    assert @network_client.network_remove
    assert !File.exist?(@network_path)
    assert !File.exist?(@fixture.path)
  end

  def test_exist_all_replicas
    expect = { nil => false }
    assert_equal expect, @client.all_replicas_exist?
    @fixture.setup
    expect = { nil => true }
    assert_equal expect, @client.all_replicas_exist?
  end

  def test_fs_read
    @fixture.setup
    File.open("#{@fixture.path}/a-file", "wb") { |f| f.write("your great data\n") }
    assert_equal "your great data\n", @client.fs_read("a-file")
  end

  def test_fs_read_no_exist
    @fixture.setup
    assert_raises(GitRPC::SystemError) { @client.fs_read("nope/sorry.txt") }
  end

  def test_fs_write
    @fixture.setup
    @client.fs_write("new-file.txt", "new great data\n")
    assert_equal "new great data\n", File.read("#{@fixture.path}/new-file.txt")
  end

  def test_fs_write_over_directory
    @fixture.setup
    Dir.mkdir("#{@fixture.path}/some-dir")
    assert_raises(GitRPC::SystemError) do
      @client.fs_write("some-dir", "cant write it")
    end
  end

  def test_corrupt
    assert @client.corrupt?, "Non-existant repo is corrupt"
    @fixture.setup
    assert @client.corrupt?, "Repo is not valid with no refs"
    @fixture.commit_files({"README" => "Something"}, "Commit msg")
    refute @client.corrupt?, "Repo is not corrupt with a commit"
  end

  def test_fs_delete
    @fixture.setup
    File.open("#{@fixture.path}/unneeded.txt", "wb") { |f| f.write("your deleted data\n") }
    @client.fs_delete("unneeded.txt")
    assert !File.exist?("#{@fixture.path}/unneeded.txt")
  end

  def test_fs_move
    @fixture.setup
    File.open("#{@fixture.path}/old_path.txt", "wb") { |f| f.write("data\n") }
    @client.fs_move("old_path.txt", "new_path.txt")
    assert !File.exist?("#{@fixture.path}/old_path.txt")
    assert File.exist?("#{@fixture.path}/new_path.txt")
  end

  def test_fs_exist
    @fixture.setup
    assert @client.fs_exist?("config")
    assert !@client.fs_exist?("some-path")
  end

  def test_fs_lock_uncontended
    @fixture.setup
    res = @client.fs_lock("the-lock", "its contents")
    assert res, "should've gotten the lock"
    assert_equal "its contents", @client.fs_read("the-lock")
    @client.fs_unlock("the-lock")
    assert !@client.fs_exist?("the-lock")
  end

  # Someone else has the lock, and it's fresh, so we don't get it.
  def test_fs_lock_contended
    @fixture.setup
    fn = "#{@fixture.path}/the-lock"
    File.write(fn, "blah")

    res = @client.fs_lock("the-lock", "its contents")
    assert !res, "should not have gotten the lock"

    File.unlink(fn)
  end

  # There's a lock, but it's stale, so we get it.
  def test_fs_lock_stale
    @fixture.setup
    fn = "#{@fixture.path}/the-lock"
    File.write(fn, "blah")
    FileUtils.touch(fn, :mtime => Time.now - 3601)

    res = @client.fs_lock("the-lock", "its contents")
    assert res, "should've gotten the lock"
    assert_equal "its contents", @client.fs_read("the-lock")
    @client.fs_unlock("the-lock")
  end

  # There's a stale lock, but someone else is already refreshing it, so we
  # don't get it.
  def test_fs_lock_stale_and_contended
    @fixture.setup
    fn = "#{@fixture.path}/the-lock"
    File.write(fn, "blah")
    FileUtils.touch(fn, :mtime => Time.now - 3601)
    fp = File.open(fn)
    fp.flock(File::LOCK_EX)

    res = @client.fs_lock("the-lock", "its contents")
    assert !res, "should not have gotten the lock"
    assert_equal "blah", @client.fs_read("the-lock")

    fp.flock(File::LOCK_UN)
    fp.close
    File.unlink(fn)
  end

  def test_fs_exist_outside_of_repository
    @fixture.setup
    assert_raises(GitRPC::IllegalPath) { @client.fs_exist?("/etc/passwd") }
  end

  def test_create_network_dir
    %w[456.git network.git 567.wiki.git].each do |suffix|
      path = File.expand_path("../nw/123", __FILE__)
      nw_repo = RepositoryFixture.new("#{path}/#{suffix}")
      nw_repo.setup

      nw_client = GitRPC.new("fakerpc:#{nw_repo.path}", :cache => @cache)

      assert File.directory?(path), "#{path} is missing"
      assert_match %r{/nw/123$}, path   # protect the rm_r
      FileUtils.rm_r(path)
      refute File.directory?(path), "#{path} is still here"
      nw_client.create_network_dir
      assert File.directory?(path), "#{path} was not created"
      files = Dir["#{path}/*"]
      assert files.empty?, "#{path} should be empty: #{files.inspect}"
    end
  end

  def test_push_to
    ["localhost", Socket.gethostname, Socket.gethostname.split(".", 2).first, nil].each do |dest_host|
      begin
        @fixture.setup
        FileUtils.touch("#{@fixture.path}/gitmon.model")
        dirs = (0..2).map { Dir.mktmpdir }
        urls = dirs.map do |d|
          dest_host ? "#{dest_host}:#{d}" : d
        end
        @client.push_to(urls)
        dirs.each do |d|
          %w[HEAD config description info/nwo gitmon.model].each do |f|
            p1 = "#{@fixture.path}/#{f}"
            p2 = "#{d}/#{File.basename(@fixture.path)}/#{f}"
            assert File.exist?(p2), "#{p2} wasn't copied"
            assert FileUtils.cmp(p1, p2), "#{p1} is different from #{p2}"
          end
        end
      ensure
        (dirs || []).each do |d|
          FileUtils.rm_r(d)
        end
      end
    end
  end
end
