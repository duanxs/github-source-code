# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"
require "gitrpc/util/hashcache"
require "securerandom"

require "minitest/mock"

class ClientHistoryTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => @cache)

    setup_test_commits
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @oid1 = @fixture.commit_files({"README" => "Hello."}, "commit 1", {:email => "guy@example.com"})
    @oid2 = @fixture.commit_files({"README" => "Goodbye."}, "commit 2", {:email => "Guy@example.com"})
    @oid3 = @fixture.commit_files({"README" => "Hi again."}, "commit 3", {:email => "someguy@example.com"})
    @oid4 = @fixture.commit_files({"AUTHORS" => "Brian"}, "commit 4", {:email => "SomeGuy@example.com"})
  end

  def test_list_commit_parent_revisions
    res = @client.list_revision_history(@oid1)
    assert_equal [@oid1], res
    res = @client.count_revision_history(@oid1)
    assert_equal 1, res

    res = @client.list_revision_history(@oid2)
    assert_equal [@oid2, @oid1], res
    res = @client.count_revision_history(@oid2)
    assert_equal 2, res

    res = @client.list_revision_history(@oid3)
    assert_equal [@oid3, @oid2, @oid1], res
    res = @client.count_revision_history(@oid3)
    assert_equal 3, res
    res = @client.count_revision_history("HEAD")
    assert_equal 4, res
  end

def test_list_revisions_by_author
    res = @client.list_revision_history_multiple([@oid4], authors: ["guy@example.com"])
    assert_equal [@oid1], res
    res = @client.list_revision_history_multiple([@oid4], authors: ["guy@example.com", "Guy@example.com"])
    assert_equal [@oid2, @oid1], res
    res = @client.list_revision_history_multiple([@oid4], authors: ["SomeGuy@example.com"])
    assert_equal [@oid4], res
    res = @client.list_revision_history_multiple([@oid4], authors: ["SomeGuy@example.com"], regexp_ignore_case: true)
    assert_equal [@oid4, @oid3], res
    res = @client.list_revision_history_multiple([@oid4], authors: [".*guy@example.com"])
    assert_equal [@oid3, @oid1], res
    res = @client.list_revision_history_multiple([@oid4], authors: [".*guy@example.com"], regexp_ignore_case: true)
    assert_equal [@oid4, @oid3, @oid2, @oid1], res
    res = @client.list_revision_history_multiple([@oid4], authors: [".*guy@example.com"], regexp_ignore_case: true, fixed_strings: true)
    assert_equal [], res
end

  def test_raises_obj_missing_on_invalid_obj
    assert_raises GitRPC::ObjectMissing do
      @client.list_revision_history(SecureRandom.hex(20))
    end

    assert_raises GitRPC::ObjectMissing do
      @client.list_revision_history(SecureRandom.hex(20), {"limit" => 10})
    end

    assert_raises GitRPC::ObjectMissing do
      @client.list_revision_history_multiple([SecureRandom.hex(20)])
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.list_revision_history_multiple(["HEAD"])
    end
  end

  def test_list_commit_parent_revisions_with_limit
    res = @client.list_revision_history(@oid3, {"limit" => 1})
    assert_equal [@oid3], res

    res = @client.list_revision_history(@oid3, {"limit" => 2})
    assert_equal [@oid3, @oid2], res

    res = @client.list_revision_history_multiple([@oid4], max: 3)
    assert_equal [@oid4, @oid3, @oid2], res
  end

  def test_list_commit_parent_revisions_with_offset
    res = @client.list_revision_history(@oid3, {"offset" => 1})
    assert_equal [@oid2, @oid1], res

    res = @client.list_revision_history(@oid3, {"offset" => 2})
    assert_equal [@oid1], res
  end

  def test_list_commit_parent_revisions_with_pathspec
    res = @client.list_revision_history(@oid4, {"path" => "AUTHORS"})
    assert_equal [@oid4], res

    res = @client.list_revision_history(@oid3, {"path" => "AUTHORS"})
    assert_equal [], res

    res = @client.list_revision_history_multiple([@oid4], path: "AUTHORS")
    assert_equal [@oid4], res
    res = @client.list_revision_history_multiple([@oid4], path: "README")
    assert_equal [@oid3, @oid2, @oid1], res
    res = @client.list_revision_history_multiple([@oid4], path: "nonexistent")
    assert_equal [], res
  end

  def test_list_commit_parent_revisions_with_invalid_pathspec
    res = @client.list_revision_history(@oid1, {"path" => ""})
    assert_equal [@oid1], res
  end

  def test_search_revision_history
    res = @client.search_revision_history("grep", "foo", "HEAD")
    assert_equal [], res
    res = @client.search_revision_history("grep", "commit 1", "HEAD")
    assert_equal [@oid1], res
    res = @client.search_revision_history("grep", "Commit 1", "HEAD")
    assert_equal [], res
    res = @client.search_revision_history("grep", "commit", "HEAD")
    assert_equal [@oid4, @oid3, @oid2, @oid1], res
    res = @client.search_revision_history("grep", "commit", "HEAD", max: 2)
    assert_equal [@oid4, @oid3], res
  end

  def test_raise_invalid_full_oid
    assert_raises GitRPC::InvalidFullOid do
      @client.list_revision_history(nil, {"path" => "AUTHORS"})
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.list_revision_history("lol", {"path" => "AUTHORS"})
    end
  end
end
