# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class ClientParseSignaturesTest < Minitest::Test
  def setup
    @client = RepositoryFixture.sample("signed_objects")
    @cache = @client.cache

    @signed_commit_oid = "58266c4ab0e73d2a94319fa6695ba75bdc73edc8"

    @signed_commit_payload = <<-HERE
tree 68aba62e560c0ebc3396e8ae9335232cd93a3f60
author Ben Toews <mastahyeti@users.noreply.github.com> 1453744339 -0700
committer Ben Toews <mastahyeti@users.noreply.github.com> 1453744339 -0700

signed commit
    HERE

    @signed_commit_signature = <<-HERE.rstrip
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQEcBAABAgAGBQJWpmDTAAoJEKAG779xTNk6ikMIANpbvFs/ywzm2rEH9KXm1JE2
wnziL7ghZj1pxSuockBC9tA+sACaC0mYVpQwEhFrE6x5Br1jkBsAbCiKexBXVBtj
3ay47QrJN2KXTwwEWioHLOz00nBzZd8tVglf1K6uUXgrMJdHSnwPnpuvSpVnIf0B
byXsLyyaaCrEObYQJpCT19+TObY3b8MTtl6NkN8IWhXbEmrrgANj44wumqogI5wQ
LrVnOUfV60+fKPtMrNpYiW2t0OcolfLCzY0SyGYyV/YpESRvDogZ1j9wPq4I0wzl
LtqgxoimCEIMoI79uKMilsAun5DpGwAL7ewq2L6s+j2IazGgC/c/35XiApVsjRM=
=yycO
-----END PGP SIGNATURE-----
    HERE

    @unsigned_commit_oid = "644bfda241e0ae7c487a6102d9db39798921464f"

    @signed_tag_oid = "2a9cb251b98d10386d29c8bd43f19f1922b87cc5"

    @signed_tag_payload = <<-HERE
object 644bfda241e0ae7c487a6102d9db39798921464f
type commit
tag signed
tagger Ben Toews <mastahyeti@users.noreply.github.com> 1455220502 -0700

my signed commit
    HERE

    @signed_tag_signature = <<-HERE
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQEcBAABAgAGBQJWvOcWAAoJEKAG779xTNk6g7kIAN7FtE8891iAO8verPxpgDdB
KmUbI5QKWJphEtTFiI6aBu3K2Nox6c6jn/tyC34sRkYapAjGK2nH9MTUEpUjJQML
i/y//1Z+mewxQnXm5wT4Q5O7RQyFXHh6YxMyXrJk0mNo0kOXGnL7Ni4uoXEDv8mq
+K9Fg7J/1bNzLUoavPtmwfe/ZSNfrXOLTwk/CueO0lQjwYieWdYOZtPuCfhMFfGK
xHW8sjGegty5vfBM6Z1TX8o2f79wYp6U6TA8QZUpE1m+wUm9gLKc+kIGWypafYJq
HLFkF3Gw4XDR6F9GT6IBbxaAhGYoxuH1qvoW/DC0ONYPVtF2tFoXaPlC7x7o2N4=
=qtws
-----END PGP SIGNATURE-----
    HERE

    @unsigned_tag_oid = "94a7f208672b6387c937a564e19ac96d778d3b98"
  end

  def test_parse_commit_signature_for_signed_commit
    result = @client.parse_commit_signatures([@signed_commit_oid])
    assert_equal [[@signed_commit_signature, @signed_commit_payload]], result
  end

  def test_parse_tag_signature_for_signed_tag
    result = @client.parse_tag_signatures([@signed_tag_oid])
    assert_equal [[@signed_tag_signature, @signed_tag_payload]], result
  end

  def test_parse_commit_signature_for_unsigned
    result = @client.parse_commit_signatures([@unsigned_commit_oid])
    assert_equal [false], result
  end

  def test_parse_tag_signature_for_unsigned_tag
    result = @client.parse_tag_signatures([@unsigned_tag_oid])
    assert_equal [false], result
  end

  def test_parse_commit_signature_for_wrong_type
    assert_raises GitRPC::InvalidObject do
      @client.parse_commit_signatures([@signed_tag_oid])
    end
  end

  def test_parse_tag_signature_for_wrong_type
    assert_raises GitRPC::InvalidObject do
      @client.parse_tag_signatures([@signed_commit_oid])
    end
  end

  def test_parse_commit_signature_for_missing_object
    assert_raises GitRPC::ObjectMissing do
      @client.parse_commit_signatures(["a"*40])
    end
  end

  def test_parse_tag_signature_for_missing_object
    assert_raises GitRPC::ObjectMissing do
      @client.parse_tag_signatures(["a"*40])
    end
  end

  def test_multiple_oids
    expected = [[@signed_commit_signature, @signed_commit_payload], false]
    actual = @client.parse_signatures([@signed_commit_oid, @unsigned_commit_oid], :commit)
    assert_equal expected, actual
  end

  def test_preserves_oid_order
    result  = @client.parse_signatures([@signed_commit_oid, @unsigned_commit_oid], :commit)
    reverse = @client.parse_signatures([@unsigned_commit_oid, @signed_commit_oid], :commit)
    assert_equal reverse, result.reverse
  end

  def test_bad_oid
    assert_raises GitRPC::InvalidFullOid do
      @client.parse_signatures(["lol"], :commit)
    end
  end

  def test_writes_cache
    assert_equal 0, @cache.hash.size
    @client.parse_signatures([@signed_commit_oid, @unsigned_commit_oid], :commit)
    assert_equal 2, @cache.hash.size
  end

  def test_reads_from_cache
    assert_equal 0, @cache.hash.size
    @client.parse_signatures([@signed_commit_oid, @unsigned_commit_oid], :commit)
    assert_equal 2, @cache.hash.size

    @client.backend.stub :send_message, lambda { |*args| fail } do
      @client.parse_signatures([@signed_commit_oid, @unsigned_commit_oid], :commit)
    end

    expected = [[@signed_commit_signature, @signed_commit_payload], false]
    actual = @client.parse_signatures([@signed_commit_oid, @unsigned_commit_oid], :commit)
    assert_equal expected, actual
  end

  def test_read_memcache_offline
    expected = [[@signed_commit_signature, @signed_commit_payload], false]

    # When memcache is offline, this returns a list of keys with nil values
    callback = lambda do |keys|
      result = {}
      keys.each do |key|
        result[key] = nil
      end
      result
    end
    @cache.stub(:get_multi, callback) do
      actual = @client.parse_signatures([@signed_commit_oid, @unsigned_commit_oid], :commit)
      assert_equal expected, actual
    end
  end

  def test_uses_partial_cache
    assert_equal 0, @cache.hash.size
    @client.parse_signatures([@signed_commit_oid], :commit)
    assert_equal 1, @cache.hash.size
    @client.parse_signatures([@unsigned_commit_oid], :commit)
    assert_equal 2, @cache.hash.size
  end
end
