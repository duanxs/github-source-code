# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class CreateTreeTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @client = GitRPC.new("fakerpc:#{@fixture.path}")
    @rugged = Rugged::Repository.new(@fixture.path)
    setup_test_tree
  end

  def setup_test_tree
    index = Rugged::Index.new
    @blob_oid = @rugged.write("new stuff", "blob")
    index.add({
      :path => "README",
      :oid  => @blob_oid,
      :mode => 0100644,
    })

    @tree_oid = index.write_tree(@rugged)
  end

  def teardown
    @fixture.teardown
  end

  def test_create_tree
    files = { "README" => "hiya" }
    oid = @client.create_tree(files)
    assert_equal "8ed6623eb88b4b5a6c43e0c346d65f8f4f40247f", oid

    tree = @rugged.lookup(oid)
    assert_equal 1, tree.count
    assert_equal "README", tree[0][:name]
    assert_equal "hiya",   @rugged.lookup(tree[0][:oid]).content
  end

  def test_update_existing_tree
    files = { "hello" => "hiya" }
    oid = @client.create_tree(files, @tree_oid)
    assert_equal "2cd7c29063bd6bc74416a3703d853686222134f4", oid

    tree = @rugged.lookup(oid)
    assert_equal 2, tree.count
    entries = tree.entries

    entry = entries.shift
    assert_equal "README", entry[:name]
    assert_equal "new stuff",   @rugged.lookup(entry[:oid]).content

    entry = entries.shift
    assert_equal "hello", entry[:name]
    assert_equal "hiya",   @rugged.lookup(entry[:oid]).content
  end

  def test_create_tree_with_blob_oid
    files = { "README" => {"mode" => 0100755, "oid" => @blob_oid} }
    oid = @client.create_tree(files)
    assert_equal "132320f4b3263bc84b7b1b89d601a92611cdf102", oid

    tree = @rugged.lookup(oid)
    assert_equal 1, tree.count
    assert_equal "README", tree[0][:name]
    assert_equal "new stuff",   @rugged.lookup(tree[0][:oid]).content
  end

  def test_create_tree_with_subtree
    files = { "README" => {"mode" => 0100755, "oid" => @blob_oid },
              "some_file" => {"mode" => 0100755, "oid" => @blob_oid} }
    subtree_oid = @client.create_tree(files)

    files = { "subtree" => {"mode" => 040000, "oid" => subtree_oid} }
    oid = @client.create_tree(files)

    tree = @rugged.lookup(oid)
    assert_equal 1, tree.count
    assert_equal "subtree", tree[0][:name]
    assert_equal subtree_oid, tree[0][:oid]
  end

  def test_create_tree_with_blob_using_tree_oid
    files = { "goober" => {"mode" => 0100755, "oid" => @tree_oid} }
    assert_raises GitRPC::BadObjectState do
        @client.create_tree(files)
    end
  end

  def test_create_tree_with_blob_using_commit_oid
    commit_oid = Rugged::Commit.create @rugged,
                   committer: { :email => "foo@bar.net", :name => "foo" },
                   message: "a test",
                   parents: [],
                   tree: @tree_oid
    files = { "goober" => {"mode" => 0100755, "oid" => commit_oid} }
    assert_raises GitRPC::BadObjectState do
        @client.create_tree(files)
    end
  end

  def test_updating_existing_tree_with_invalid_tree
    files = { "hello" => "hiya" }
    assert_raises GitRPC::InvalidObject do
      @client.create_tree(files, GitRPC::NULL_OID)
    end
  end

  def test_update_existing_tree_with_non_tree
    files = { "hello" => "hiya" }
    assert_raises GitRPC::InvalidObject do
      @client.create_tree(files, @blob_oid)
    end
  end

end
