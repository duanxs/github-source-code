# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"
require "gitrpc/util/hashcache"

class ClientRebaseTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => @cache)
    @rugged = Rugged::Repository.new(@fixture.path)

    @committer = {
      "name" => "Some Guy",
      "email" => "guy@example.com",
      "time" => Time.utc(2000, 1, 1)
    }

    setup_test_commits
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @fixture.add_empty_commit("Initial import")

    @fixture.create_branch("topic")
    @fixture.create_branch("conflict")

    @master_commit = @fixture.commit_files({"one" => "foo"}, "Master")
    @topic_commit = @fixture.commit_files({"two" => "bar"}, "Topic", { :branch => "topic" })
    @conflicting_commit = @fixture.commit_files({"one" => "baz", "two" => "bar"}, "Conflict", { :branch => "conflict" })
  end

  def test_rebase
    expected_commit_oid = "6cce1314f2053264287fac0f96d969fd0c1cb859"

    oid = @client.rebase(@topic_commit, @master_commit, @committer)
    assert_equal expected_commit_oid, oid
  end

  def test_rebase_and_merge_produce_same_tree
    commit_oid, error = @client.create_merge_commit("master", "topic", @committer, "Example merge", :committer => @committer)
    assert_nil error

    assert rebase_oid = @client.rebase(@topic_commit, @master_commit, @committer)

    assert rebase_commit = @rugged.lookup(rebase_oid)
    assert merge_commit = @rugged.lookup(commit_oid)

    assert_equal rebase_commit.tree, merge_commit.tree
  end

  def test_rebase_conflict
    oid = @client.rebase(@conflicting_commit, @master_commit, @committer)
    assert_nil oid
  end

  def test_rebase_with_timeout
    stub_values = [0, 10]
    Process.stub(:clock_gettime, lambda { |_| stub_values.shift }) do
      @client.rebase(@topic_commit, @master_commit, @committer, :timeout => 10)
    end

    stub_values = [0, 11]
    Process.stub(:clock_gettime, lambda { |_| stub_values.shift }) do
      assert_raises GitRPC::Backend::RebaseTimeout do
        @client.rebase(@topic_commit, @master_commit, @committer, :timeout => 10)
      end
    end
  end
end
