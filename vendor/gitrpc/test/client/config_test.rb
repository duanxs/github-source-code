# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class ClientConfigTest < Minitest::Test
  include GitRPC::Util

  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @fixture.config.store("github.test", "true")

    @cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => @cache)
  end

  def teardown
    @fixture.teardown
  end

  def test_config_get_with_existing_value
    res = @client.config_get("github.test")
    assert_equal "true", res
  end

  def test_config_get_with_missing_value
    res = @client.config_get("github.missing")
    assert_nil res
  end

  def test_config_store_with_valid_value
    res = @client.config_store("github.test", "false")
    assert_nil res
    assert_equal "false", @fixture.config.get("github.test")
  end

  def test_config_store_with_invalid_value
    assert_raises GitRPC::Failure do
      @client.config_store("github.test", ["array"])
    end
  end

  def test_config_delete_with_existing_value
    res = @client.config_delete("github.test")
    assert_equal true, res
    assert_nil @fixture.config.get("github.test")
  end

  def test_config_delete_with_missing_value
    res = @client.config_delete("github.missing")
    assert_equal false, res
    assert_nil @fixture.config.get("github.missing")
  end
end
