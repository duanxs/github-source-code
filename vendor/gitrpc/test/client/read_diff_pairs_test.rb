# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"
require "gitrpc/diff/delta"

class ClientReadDiffPairsTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @commit1 = @fixture.commit_files({"README" => "Hello. This is the majority of the content of the file.\n"}, "commit 1")
    @commit2 = @fixture.commit_files({"README" => "Hello. This is the majority of the content of the file.\nNew!\n"}, "commit 2")

    cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => cache)
  end

  # This is far from general purpose. It deletes the blob from disk and probably only
  # works for tests with programmatically created repos
  def delete_blob(oid)
    path = File.join(@fixture.path, "objects", oid[0, 2], oid[2, 1000])
    File.delete(path)
  end

  def test_raises_immediately_on_0_timeout
    deltas = @client.native_read_diff_toc(@commit1, @commit2)
    @client.backend.stub(:send_message, lambda { |*args| fail "should not be called" }) do
      assert_raises(GitRPC::Timeout) { @client.read_diff_pairs_with_base(@commit1, @commit2, @commit1, deltas, "timeout" => 0) }
    end
  end

  def test_raises_object_missing_for_unable_to_read
    deltas = @client.native_read_diff_toc(@commit1, @commit2)

    # whoops! the blob is gone
    delete_blob(deltas.first.old_file.oid)

    assert_raises(GitRPC::ObjectMissing) { @client.read_diff_pairs(deltas, "timeout" => 5) }
  end

  def test_ignore_whitespace
    commit3 = @fixture.commit_files({"whitespace.txt" => "Hello.\n      \n    This file has  \nWhitespace     \n"}, "commit 3")
    commit4 = @fixture.commit_files({"whitespace.txt" => "Hello.\n\nThis file has\nWhitespace\n"}, "commit 4")

    deltas = @client.native_read_diff_toc(commit3, commit4)
    result = @client.read_diff_pairs_with_base(commit3, commit4, commit3, deltas, "ignore_whitespace" => true)
    assert_empty result["data"]
  end
end
