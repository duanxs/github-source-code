# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"
require "gitrpc/util/hashcache"

require "minitest/mock"

class ClientReadCommitsTest < Minitest::Test
  include GitRPC::Util

  def setup
    @bert_version = BERT::Encode.version
    BERT::Encode.version = :v2
    @fixture = RepositoryFixture.new
    @fixture.setup

    @cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => @cache)

    setup_test_commits
  end

  def teardown
    BERT::Encode.version = @bert_version
    @fixture.teardown
  end

  def setup_test_commits
    @fixture.add_empty_commit("commit 1")
    @oid1 = @fixture.rev_parse("HEAD")

    @fixture.add_empty_commit("commit 2")
    @oid2 = @fixture.rev_parse("HEAD")
  end

  def test_read_commits_sanity
    res = @client.read_commits([@oid1])
    assert_equal 1, res.size
    assert_equal "commit", res[0]["type"]
    assert_equal @oid1, res[0]["oid"]
    assert_equal "commit 1\n", res[0]["message"]
    assert_equal "UTF-8", res[0]["encoding"]
    assert_equal Encoding::UTF_8, res[0]["message"].encoding
    assert_equal "Some Author", res[0]["author"][0]
    assert_equal "someone@example.org", res[0]["author"][1]
    assert !res[0]["committer"].nil?
  end

  def test_read_commits_multiple_sanity
    res = @client.read_commits([@oid2, @oid1])

    assert_equal 2, res.size
    assert_equal "commit", res[0]["type"]
    assert_equal @oid2, res[0]["oid"]

    assert_equal "commit", res[1]["type"]
    assert_equal @oid1, res[1]["oid"]
  end

  def test_read_commits_writes_cache
    res = @client.read_commits([@oid2, @oid1])
    assert_equal 2, @cache.hash.size

    commit_keys = [@oid2, @oid1].map { |oid| @client.object_key(oid) }
    assert_equal @cache.hash.keys.sort, commit_keys.sort
  end

  def test_read_commits_uses_cache
    res = @client.read_commits([@oid2, @oid1])
    assert_equal 2, @cache.hash.size

    @client.backend.stub :send_message, lambda { fail } do
      @client.read_commits([@oid2, @oid1])
    end

    res = @client.read_commits([@oid2, @oid1])
    assert_equal "commit", res[0]["type"]
    assert_equal @oid2, res[0]["oid"]
    assert_equal "commit 2\n", res[0]["message"]
    assert_equal "UTF-8", res[0]["encoding"]
    assert_equal Encoding::UTF_8, res[0]["message"].encoding
    assert_equal "Some Author", res[0]["author"][0]
    assert_equal "someone@example.org", res[0]["author"][1]
    assert !res[0]["committer"].nil?
  end

  def test_read_commits_uses_partial_cache
    res = @client.read_commits([@oid1])
    assert_equal 1, @cache.hash.size

    @client.read_commits([@oid2, @oid1])
    assert_equal 2, @cache.hash.size
  end
end
