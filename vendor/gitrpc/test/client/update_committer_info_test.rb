# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class ClientUpdateCommiterInfoTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    setup_test_commits

    @cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => @cache)
    @rugged = Rugged::Repository.new(@fixture.path)

    @committer = {
      name: "Some Guy",
      email: "guy@example.com",
      time: Time.parse("2000-01-01T00:00:00Z")
    }
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @fixture.add_empty_commit("Initial import")

    @fixture.create_branch("topic")
    @fixture.create_branch("conflict")

    @first_commit_id = @fixture.commit_files({"one" => "foo"}, "First commit")
    @second_commit_id = @fixture.commit_files({"two" => "bar"}, "Second commmit")
    @third_commit_id = @fixture.commit_files({"three" => "baz"}, "Third commit")
  end

  def test_update_committer_info
    oid = @client.update_committer_info(@third_commit_id, @first_commit_id, @committer)

    third_commit = Rugged::Commit.lookup(@rugged, @third_commit_id)
    second_commit = Rugged::Commit.lookup(@rugged, @second_commit_id)
    first_commit = Rugged::Commit.lookup(@rugged, @first_commit_id)

    commit = Rugged::Commit.lookup(@rugged, oid)

    assert_equal "390474447fe1f50eedea0acd783b3e586c5bd3a8", commit.oid
    assert_equal third_commit.author, commit.author
    assert_equal @committer[:name], commit.committer[:name]
    assert_equal @committer[:email], commit.committer[:email]
    assert_equal @committer[:time], commit.committer[:time]
    assert_equal third_commit.message, commit.message
    assert_equal third_commit.tree_id, commit.tree_id

    commit = commit.parents[0]
    assert_equal second_commit.author, commit.author
    assert_equal @committer[:name], commit.committer[:name]
    assert_equal @committer[:email], commit.committer[:email]
    assert_equal @committer[:time], commit.committer[:time]
    assert_equal second_commit.message, commit.message
    assert_equal second_commit.tree_id, commit.tree_id

    commit = commit.parents[0]
    assert_equal first_commit, commit
  end
end
