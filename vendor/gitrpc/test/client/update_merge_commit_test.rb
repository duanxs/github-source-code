# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class ClientUpdateMergeCommitTest < Minitest::Test
  def setup
    @repo = RepositoryFixture.new
    @repo.setup
    setup_test_files

    @client = GitRPC.new("fakerpc:#{@repo.path}")
    @rugged = Rugged::Repository.new(@repo.path)
    @committer = {
      "name"  => "Some Guy",
      "email" => "guy@example.com",
      "time"  => "2000-01-01T00:00:00Z"
    }
  end

  def setup_test_files
    @repo.add_empty_commit("Initial import")
    @repo.create_branch("topic")

    @master_commit = @repo.commit_files({"one" => "foo"}, "Master")
    @topic_commit = @repo.commit_files({"two" => "bar"}, "Topic", { :branch => "topic" })

    @merge_oid = @repo.merge "topic"
  end

  def test_rewrite_merge_commit
    oid = @client.rewrite_merge_commit(@merge_oid, "message" => "Test message",
                                                  "committer" => @committer)
    old_merge = @rugged.lookup(@merge_oid)
    commit = @rugged.lookup(oid)
    assert_equal "Some Guy", commit.committer[:name]
    assert_equal "guy@example.com", commit.committer[:email]
    assert_equal "Some Guy", commit.author[:name]
    assert_equal "guy@example.com", commit.author[:email]

    parent_oids = commit.parents.map(&:oid)
    assert_equal parent_oids, [@master_commit, @topic_commit]

    assert_equal "Test message", commit.message

    assert_equal old_merge.tree.oid, commit.tree.oid
  end

  def test_rewrite_squash_merge_commit
    oid = @client.rewrite_merge_commit(@merge_oid, {"message" => "Test message",
                                                   "committer" => @committer},
                                                   true)
    old_merge = @rugged.lookup(@merge_oid)
    commit = @rugged.lookup(oid)
    assert_equal "Some Guy", commit.committer[:name]
    assert_equal "guy@example.com", commit.committer[:email]
    assert_equal "Some Guy", commit.author[:name]
    assert_equal "guy@example.com", commit.author[:email]

    parent_oids = commit.parents.map(&:oid)
    assert_equal parent_oids, [@master_commit]

    assert_equal "Test message", commit.message

    assert_equal old_merge.tree.oid, commit.tree.oid
  end

  def test_rewrite_merge_commit_when_merge_doesnt_exist
    fake_oid = "0123456789" * 4
    assert_raises(GitRPC::ObjectMissing) do
      @client.rewrite_merge_commit(fake_oid, "message" => "Test message",
                                                    "committer" => @committer)
    end
  end

  def test_rewrite_merge_commit_with_no_committer
    assert_raises GitRPC::Failure do
      @client.rewrite_merge_commit(@merge_oid, "message" => "Test message")
    end
  end

  def test_rewrite_merge_commit_with_no_message
    assert_raises GitRPC::Failure do
      @client.rewrite_merge_commit(@merge_oid, "committer" => @committer)
    end
  end

  def test_raise_invalid_full_oid
    assert_raises GitRPC::InvalidFullOid do
      @client.rewrite_merge_commit(nil, "message" => "Test message",
                                  "committer" => @committer)
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.rewrite_merge_commit("lol", "message" => "Test message",
                                  "committer" => @committer)
    end
  end

  def test_rewrite_merge_commit_signed
    sig = "fake-signature"
    opts = {
      "message" => "Test message",
      "committer" => @committer
    }

    oid = @client.rewrite_merge_commit(@merge_oid, opts) do |raw|
      assert raw.is_a?(String)
      sig
    end

    old_merge = @rugged.lookup(@merge_oid)
    commit = @rugged.lookup(oid)
    assert_equal sig, commit.header_field("gpgsig")
    assert_equal "Some Guy", commit.committer[:name]
    assert_equal "guy@example.com", commit.committer[:email]
    assert_equal "Some Guy", commit.author[:name]
    assert_equal "guy@example.com", commit.author[:email]

    parent_oids = commit.parents.map(&:oid)
    assert_equal parent_oids, [@master_commit, @topic_commit]

    assert_equal "Test message", commit.message

    assert_equal old_merge.tree.oid, commit.tree.oid
  end

  def test_rewrite_merge_commit_nil_signature
    opts = {
      "message" => "Test message",
      "committer" => @committer
    }

    oid = @client.rewrite_merge_commit(@merge_oid, opts) { |r| nil }

    old_merge = @rugged.lookup(@merge_oid)
    commit = @rugged.lookup(oid)
    refute commit.header_field?("gpgsig")
    assert_equal "Some Guy", commit.committer[:name]
    assert_equal "guy@example.com", commit.committer[:email]
    assert_equal "Some Guy", commit.author[:name]
    assert_equal "guy@example.com", commit.author[:email]

    parent_oids = commit.parents.map(&:oid)
    assert_equal parent_oids, [@master_commit, @topic_commit]

    assert_equal "Test message", commit.message

    assert_equal old_merge.tree.oid, commit.tree.oid
  end
end
