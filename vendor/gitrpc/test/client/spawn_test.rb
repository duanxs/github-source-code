# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"
require "gitrpc/util/hashcache"

class ClientSpawnTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new

    @url = "fakerpc:#{@fixture.path}"
    @cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new(@url, :cache => @cache)
  end

  def teardown
    @fixture.teardown
  end

  def test_spawn_not_exposed_to_client
    [:spawn, :spawn_ro, :spawn_git, :spawn_git_ro].each do |msg|
      assert_raises GitRPC::Failure do
        @client.send_message(msg, ["true"])
      end
    end
  end
end
