# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"
require "securerandom"

require "minitest/mock"

class ClientDistinctCommitsTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => @cache)

    setup_test_commits
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @past_time = Time.local(2000).iso8601
    @oid1 = @fixture.commit_files({"README" => "Bonjour."}, "commit 1", :commit_time => @past_time)
    @oid2 = @fixture.commit_files({"README" => "Hello."}, "commit 2", :commit_time => @past_time)
  end

  def test_distinct_commits
    commits = @client.distinct_commits("refs/heads/master")
    assert_equal [@oid2, @oid1], commits

    commits = @client.distinct_commits("refs/heads/master", count: true)
    assert_equal 2, commits
  end

  def test_distinct_commits_ref_with_at
    @fixture.rugged.references.rename("refs/heads/master", "refs/heads/master@fancyname")

    commits = @client.distinct_commits("refs/heads/master@fancyname", new_syntax: true)
    assert_equal [@oid2, @oid1], commits
    commits = @client.distinct_commits("refs/heads/master@fancyname", count: true, new_syntax: true)
    assert_equal 2, commits

    commits = @client.distinct_commits("refs/heads/master@fancyname@{#{@oid2}}", new_syntax: true)
    assert_equal [@oid2, @oid1], commits
    commits = @client.distinct_commits("refs/heads/master@fancyname@{#{@oid2}}", count: true, new_syntax: true)
    assert_equal 2, commits

  ensure
    @fixture.rugged.references.rename("refs/heads/master@fancyname", "refs/heads/master")
  end
end
