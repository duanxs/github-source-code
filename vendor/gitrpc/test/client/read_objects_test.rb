# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"
require "gitrpc/util/hashcache"

require "minitest/mock"

class ClientReadObjectsTest < Minitest::Test
  include GitRPC::Util

  def setup
    @bert_version = BERT::Encode.version
    BERT::Encode.version = :v2
    @fixture = RepositoryFixture.new
    @fixture.setup

    @cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => @cache)
    @original_max_data_size = GitRPC::Backend.blob_maximum_data_size

    setup_test_commits
  end

  def teardown
    BERT::Encode.version = @bert_version
    @fixture.teardown
    GitRPC::Backend.blob_maximum_data_size = @original_max_data_size
  end

  def setup_test_commits
    @fixture.add_empty_commit("commit 1")
    @oid1 = @fixture.rev_parse("HEAD")

    @fixture.add_empty_commit("commit 2")
    @oid2 = @fixture.rev_parse("HEAD")

    @png_data = "\211PNG\r\n\032\n\000\000\000\rIHDR\000\000\000@\000\000\000@\004\003\000\000\000XGl\355\000\000\000'PLTE\377\377\377555\n\n\n...***&&&!!!\034\034\034\030\030\030\024\024\024\021\021\021\r\r\r2222\352\267\375\000\000\000\003tRNS\000\231\231\323&\336m\000\000\000JIDATx^\355\3121\001\200 \000EA*X\301\nT\260\002\025\254@\005+P\201\nT0\224\333\333\336\342\302\362o\276\262MD\034\202\360\nB\025\204K\020\232 \334\202\320\005\341\021\204!\bS\020\226 \234\242\374\023\021\037\023\000|Q\332\035Y\253\000\000\000\000IEND\256B`\202"
    @fixture.commit_files({
      "README"      => "hello",
      "subdir/test" => "somedata",
      "heavy_minus_sign.png" => @png_data
    }, "commit 3")
    @oid3 = @fixture.rev_parse("HEAD")

    @fixture.add_empty_commit(<<~MSG)
      commit with trailers

      Co-authored-by: Chester <chester@github.com>
      Created-with: atom🛠
    MSG

    @oid4 = @fixture.rev_parse("HEAD")
  end

  def test_read_objects_sanity
    res = @client.read_objects([@oid1])
    assert_equal 1, res.size
    assert_equal "commit", res[0]["type"]
    assert_equal @oid1, res[0]["oid"]
    assert_equal "commit 1\n", res[0]["message"]
    assert_equal "UTF-8", res[0]["encoding"]
    assert_equal Encoding::UTF_8, res[0]["message"].encoding
    assert_equal "Some Author", res[0]["author"][0]
    assert_equal "someone@example.org", res[0]["author"][1]
    assert_equal false, res[0]["has_signature"]
    assert !res[0]["committer"].nil?
  end

  def test_read_commit_signature
    client = RepositoryFixture.sample("signed_objects")
    res = client.read_objects(["58266c4ab0e73d2a94319fa6695ba75bdc73edc8"])

    assert_equal true, res[0]["has_signature"]
  end

  def test_read_objects_multiple_sanity
    res = @client.read_objects([@oid2, @oid1])

    assert_equal 2, res.size
    assert_equal "commit", res[0]["type"]
    assert_equal @oid2, res[0]["oid"]

    assert_equal "commit", res[1]["type"]
    assert_equal @oid1, res[1]["oid"]
  end

  def test_read_objects_single_tree
    commits = @client.read_objects([@oid3])
    assert commits[0]["tree"]

    res = @client.read_objects([commits[0]["tree"]])
    assert_equal 1, res.size
    assert_equal "tree", res[0]["type"]
    assert_equal commits[0]["tree"], res[0]["oid"]
    assert_equal 3, res[0]["entries"].size

    entry = res[0]["entries"]["README"]
    assert_equal "b6fc4c620b67d95f953a5c1c1230aaab5db5a1b0", entry["oid"]
    assert_equal "blob", entry["type"]
    assert_equal 0100644, entry["mode"]
    assert_equal "README", entry["name"]

    entry = res[0]["entries"]["subdir"]
    assert_equal "3a9d26d61e2efa7b4b882d83e8883933d49c1ce4", entry["oid"]
    assert_equal "tree", entry["type"]
    assert_equal 040000, entry["mode"]
    assert_equal "subdir", entry["name"]
  end

  def test_read_objects_single_blob
    res = @client.read_objects(["b6fc4c620b67d95f953a5c1c1230aaab5db5a1b0"])
    assert_equal "b6fc4c620b67d95f953a5c1c1230aaab5db5a1b0", res[0]["oid"]
    assert_equal "hello", res[0]["data"]
    assert_equal Encoding::UTF_8, res[0]["data"].encoding
    assert_equal "hello".bytesize, res[0]["size"]
    assert_equal "UTF-8", res[0]["encoding"]
    assert_equal false, res[0]["binary"]

    res = @client.read_objects(["b8d3d82f2cd5141182c304ad90a6992eb2c5d4c1"])
    assert_equal Encoding::ASCII_8BIT, res[0]["data"].encoding
    assert_equal @png_data.bytesize, res[0]["size"]
    assert_nil res[0]["encoding"]
    assert_equal true, res[0]["binary"]

    GitRPC::Backend.blob_maximum_data_size = 1
    res = @client.read_full_blob("b6fc4c620b67d95f953a5c1c1230aaab5db5a1b0")
    assert_equal "b6fc4c620b67d95f953a5c1c1230aaab5db5a1b0", res["oid"]
    assert_equal "hello", res["data"]
    assert_equal Encoding::UTF_8, res["data"].encoding
    assert_equal "hello".bytesize, res["size"]
    assert_equal "UTF-8", res["encoding"]
    assert_equal false, res["binary"]
  end

  def test_read_object_headers_blob
    res = @client.read_objects([
      "b6fc4c620b67d95f953a5c1c1230aaab5db5a1b0",
      "b8d3d82f2cd5141182c304ad90a6992eb2c5d4c1"
    ])
    assert_equal 2, res.size
    assert_equal "blob", res[0]["type"]
    assert_equal "hello".bytesize, res[0]["size"]
    assert_equal "blob", res[1]["type"]
    assert_equal @png_data.bytesize, res[1]["size"]
  end

  def test_read_objects_writes_cache
    res = @client.read_objects([@oid2, @oid1])
    assert_equal 2, @cache.hash.size

    commit_keys = [@oid2, @oid1].map { |oid| @client.object_key(oid) }
    assert_equal @cache.hash.keys.sort, commit_keys.sort
  end

  def test_read_missing_objects_clears_cache
    missing_oid = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
    @cache.set(@client.object_key(missing_oid), "some bogus data")
    # pretend it's a skipmc=1 by stubbing out get_multi
    @cache.stub(:get_multi, nil) do
      assert_raises GitRPC::ObjectMissing do
        @client.read_objects([missing_oid])
      end
    end
    assert_equal 0, @cache.hash.size
  end

  def test_read_memcache_offline
    # When memcache is offline, this returns a list of keys with nil values
    callback = lambda do |keys|
      result = {}
      keys.each do |key|
        result[key] = nil
      end
      result
    end
    @cache.stub(:get_multi, callback) do
      res = @client.read_objects(["b6fc4c620b67d95f953a5c1c1230aaab5db5a1b0"], "blob")
      assert_equal "b6fc4c620b67d95f953a5c1c1230aaab5db5a1b0", res[0]["oid"]
      assert_equal "hello", res[0]["data"]
      assert_equal Encoding::UTF_8, res[0]["data"].encoding
      assert_equal "hello".bytesize, res[0]["size"]
      assert_equal "UTF-8", res[0]["encoding"]
      assert_equal false, res[0]["binary"]
    end
  end

  def test_read_objects_uses_cache
    res = @client.read_objects([@oid2, @oid1])
    assert_equal 2, @cache.hash.size

    @client.backend.stub :send_message, lambda { fail } do
      @client.read_objects([@oid2, @oid1])
    end

    res = @client.read_objects([@oid2, @oid1])
    assert_equal "commit", res[0]["type"]
    assert_equal @oid2, res[0]["oid"]
    assert_equal "commit 2\n", res[0]["message"]
    assert_equal "UTF-8", res[0]["encoding"]
    assert_equal Encoding::UTF_8, res[0]["message"].encoding
    assert_equal "Some Author", res[0]["author"][0]
    assert_equal "someone@example.org", res[0]["author"][1]
    assert !res[0]["committer"].nil?
  end

  def test_read_objects_uses_partial_cache
    res = @client.read_objects([@oid1])
    assert_equal 1, @cache.hash.size

    @client.read_objects([@oid2, @oid1])
    assert_equal 2, @cache.hash.size
  end

  def test_read_objects_missing_commit
    assert_raises GitRPC::ObjectMissing do
      @client.read_objects([@oid1, "0" * 40, @oid2])
    end
  end

  def test_read_objects_missing_object_with_skip_bad
    assert_equal 2, @client.read_objects([@oid1, "0" * 40, @oid2], type=nil, skip_bad=true).length
  end

  def test_read_objects_with_invalid_type
    assert_raises GitRPC::InvalidObject do
      @client.read_objects([@oid1], "tree")
    end
  end

  def test_read_objects_with_invalid_type_with_skip_bad
    assert_empty @client.read_objects([@oid1], "tree", skip_bad=true)
  end

  def test_read_objects_with_invalid_type_in_cache
    @client.read_objects([@oid1])
    assert_raises GitRPC::InvalidObject do
      @client.read_objects([@oid1], "tree")
    end
  end

  def test_read_objects_with_invalid_type_in_cache_with_skip_bad
    @client.read_objects([@oid1])
    assert_empty @client.read_objects([@oid1], "tree", skip_bad=true)
  end

  def test_read_objects_with_valid_raw_commit
    # Necessary to make sure RepositoryFixture#add_empty_raw_commit isn't
    # creating garbage commits by default
    oid = @fixture.add_empty_raw_commit
    refute_empty @client.read_objects([oid], "commit")
  end

  def test_read_objects_with_bad_timestamp_commit
    oid = @fixture.add_empty_raw_commit(
      "commit with garbage timestamps",
      "author Some Author <someone@example.org> 18446744071713594723 +55443228",
      "committer Some Author <someone@example.org> 18446744071713594723 +55443228"
    )

    assert_raises(GitRPC::BadObjectState) do
      @client.read_objects([oid], "commit")
    end
  end

  def test_read_objects_with_bad_timestamp_commit_with_skip_bad
    oid = @fixture.add_empty_raw_commit(
      "commit with garbage timestamps",
      "author Some Author <someone@example.org> 18446744071713594723 +55443228",
      "committer Some Author <someone@example.org> 18446744071713594723 +55443228"
    )

    assert_empty @client.read_objects([oid], "commit", skip_bad=true)
  end

  def test_raise_invalid_full_oid
    assert_raises GitRPC::InvalidFullOid do
      @client.read_objects([nil])
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.read_objects([nil, "lol"])
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.read_objects([nil])
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.read_full_blob(nil)
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.read_full_blob("lols")
    end
  end

  def test_raise_invalid_full_oid_with_skip_bad
    assert_raises GitRPC::InvalidFullOid do
      @client.read_objects([nil], type=nil, skip_bad=true)
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.read_objects([nil, "lol"], type=nil, skip_bad=true)
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.read_objects([nil], type=nil, skip_bad=true)
    end
  end

  def test_trailer_parsing
    commit = @client.read_objects([@oid4]).first

    assert_equal ["atom🛠"], commit["trailers"]["created-with"]
    assert_equal ["Chester <chester@github.com>"], commit["trailers"]["co-authored-by"]
  end
end
