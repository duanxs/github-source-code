# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"
#require 'gitrpc/util/hashcache'

class ClientReadTreeEntryTest < Minitest::Test
  include GitRPC::Util

  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    setup_test_commits

    @cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => @cache)
  end

  def setup_test_commits
    @fixture.commit_files({
      "README"      => "hello",
      "subdir/test" => "somedata",
    }, "commit msg")
    @commit = @fixture.rev_parse("HEAD")
    @bad_commit = @commit.tr("a-z0-9", "a")
  end

  def teardown
    @fixture.teardown
  end

  def test_read_tree_entries_tree
    res = @client.read_tree_entries(@commit, path: "")
    entries = res["entries"]
    assert_equal 2, entries.size

    readme = entries.shift
    subdir = entries.shift

    assert_equal "subdir", subdir["name"]
    assert_equal "3a9d26d61e2efa7b4b882d83e8883933d49c1ce4", subdir["oid"]
    assert_equal "tree", subdir["type"]

    assert_equal "README", readme["name"]
    assert_equal "b6fc4c620b67d95f953a5c1c1230aaab5db5a1b0", readme["oid"]
    assert_equal "blob", readme["type"]
    assert_equal 5, readme["size"]
    assert_equal 0100644, readme["mode"]
  end

  def test_read_tree_entries_blob
    assert_raises GitRPC::InvalidObject do
      @client.read_tree_entries(@commit, path: "README")
    end
  end

  def test_read_tree_entries_nested
    res = @client.read_tree_entries(@commit, path: "subdir")
    entries = res["entries"]
    assert_equal 1, entries.size
    test = entries.first

    assert_equal "test", test["name"]
    assert_equal "16ac089539f23bae4bfbc861281e48dfaabd233d", test["oid"]
    assert_equal "blob", test["type"]
  end

  def test_read_tree_entries_recursive_filtered
    ##
    # FIRST SET COPY PASTED FROM `test_read_tree_entries_recursive`
    # Since the behavior should be identical. These can move if
    # this method holds, and can be used to implement the other
    res = @client.read_tree_entries_recursive_filtered(@commit)
    entries = res["entries"]
    assert_equal 3, entries.size

    readme = entries.shift
    subdir = entries.shift
    test = entries.shift

    assert_equal "test", test["name"]
    assert_equal "subdir/test", test["path"]
    assert_equal "16ac089539f23bae4bfbc861281e48dfaabd233d", test["oid"]
    assert_equal "blob", test["type"]

    assert_equal "README", readme["name"]
    assert_equal "README", readme["path"]
    assert_equal "b6fc4c620b67d95f953a5c1c1230aaab5db5a1b0", readme["oid"]
    assert_equal "blob", readme["type"]

    assert_equal "subdir", subdir["name"]
    assert_equal "subdir", subdir["path"]
    assert_equal "3a9d26d61e2efa7b4b882d83e8883933d49c1ce4", subdir["oid"]
    assert_equal "tree", subdir["type"]
    # COPIED TEST ENDS
    ##

    res = @client.read_tree_entries_recursive_filtered(@commit, conditions: { type: "tree" })
    entries = res["entries"]
    assert_equal 1, entries.size

    res = @client.read_tree_entries_recursive_filtered(@commit, conditions: { type: "blob" })
    entries = res["entries"]
    assert_equal 2, entries.size

    res = @client.read_tree_entries_recursive_filtered(@commit,
      conditions: {
        type: "blob",
        min_size: 100,
      })
    entries = res["entries"]
    assert_equal 0, entries.size

    res = @client.read_tree_entries_recursive_filtered(@commit,
      conditions: {
        type: "blob",
        max_size: 6,
      })
    entries = res["entries"]
    assert_equal 1, entries.size

    res = @client.read_tree_entries_recursive_filtered(@commit,
      conditions: {
        type: "blob",
        min_size: 1,
        max_size: 10,
      })
    entries = res["entries"]
    assert_equal 2, entries.size

    res = @client.read_tree_entries_recursive_filtered(@commit,
      limit: 1,
      conditions: {
        type: "blob",
        min_size: 1,
        max_size: 10,
      })
    entries = res["entries"]
    assert_equal 1, entries.size
  end

  def test_read_tree_entries_recursive
    res = @client.read_tree_entries_recursive(@commit)
    entries = res["entries"]
    assert_equal 3, entries.size

    readme = entries.shift
    subdir = entries.shift
    test = entries.shift

    assert_equal "test", test["name"]
    assert_equal "subdir/test", test["path"]
    assert_equal "16ac089539f23bae4bfbc861281e48dfaabd233d", test["oid"]
    assert_equal "blob", test["type"]

    assert_equal "README", readme["name"]
    assert_equal "README", readme["path"]
    assert_equal "b6fc4c620b67d95f953a5c1c1230aaab5db5a1b0", readme["oid"]
    assert_equal "blob", readme["type"]

    assert_equal "subdir", subdir["name"]
    assert_equal "subdir", subdir["path"]
    assert_equal "3a9d26d61e2efa7b4b882d83e8883933d49c1ce4", subdir["oid"]
    assert_equal "tree", subdir["type"]
  end

  def test_read_tree_entries_recursive_with_limit
    res = @client.read_tree_entries_recursive(@commit, nil, 2)
    entries = res["entries"]
    assert_equal 2, entries.size
    assert res["truncated_entries"]

    readme = entries.shift
    subdir = entries.shift

    assert_equal "README", readme["name"]
    assert_equal "README", readme["path"]
    assert_equal "b6fc4c620b67d95f953a5c1c1230aaab5db5a1b0", readme["oid"]
    assert_equal "blob", readme["type"]

    assert_equal "subdir", subdir["name"]
    assert_equal "subdir", subdir["path"]
    assert_equal "3a9d26d61e2efa7b4b882d83e8883933d49c1ce4", subdir["oid"]
    assert_equal "tree", subdir["type"]
  end

  def test_read_non_existing_path
    assert_raises GitRPC::NoSuchPath do
      @client.read_tree_entries(@commit, path: "nonexistant")
    end
  end

  def test_read_non_existing_ref
    assert_raises GitRPC::ObjectMissing do
      @client.read_tree_entries(@bad_commit, path: "README")
    end
  end

  def test_read_tree_entry
    response = @client.read_tree_entry(@commit, "README")

    assert_equal "README", response["name"]
    assert_equal "blob", response["type"]
    assert_equal "b6fc4c620b67d95f953a5c1c1230aaab5db5a1b0", response["oid"]
    assert_equal 0100644, response["mode"]
    assert_equal 5, response["size"]
    assert_equal "hello", response["data"]
    assert_equal "hello", response["content"]
    # Ensuring we aren't duplicating blob contents
    assert_equal response["data"].object_id, response["content"].object_id
    assert_equal "UTF-8", response["encoding"]
    assert_equal false, response["binary"]
    assert_equal false, response["truncated"]
  end

  def test_read_tree_entry_limit
    response = @client.read_tree_entry(@commit, "README", "truncate" => 3)

    assert_equal true, response["truncated"]
    assert_equal 5, response["size"]
    assert_equal "hel", response["data"]
  end

  def test_read_tree_entry_nil_path
    expected = {
      "type" => "tree",
      "oid"  => @client.rev_parse(@commit + "^{tree}"),
      "mode" => 040000,
      "name" => "",
      "path" => "",
      "size" => nil,
      "content" => nil,
    }

    actual = @client.read_tree_entry(@commit)
    assert_equal actual, expected

    actual = @client.read_tree_entry(@commit, "")
    assert_equal actual, expected
  end

  def test_read_tree_entry_encodings
    png_data = "\211PNG\r\n\032\n\000\000\000\rIHDR\000\000\000@\000\000\000@\004\003\000\000\000XGl\355\000\000\000'PLTE\377\377\377555\n\n\n...***&&&!!!\034\034\034\030\030\030\024\024\024\021\021\021\r\r\r2222\352\267\375\000\000\000\003tRNS\000\231\231\323&\336m\000\000\000JIDATx^\355\3121\001\200 \000EA*X\301\nT\260\002\025\254@\005+P\201\nT0\224\333\333\336\342\302\362o\276\262MD\034\202\360\nB\025\204K\020\232 \334\202\320\005\341\021\204!\bS\020\226 \234\242\374\023\021\037\023\000|Q\332\035Y\253\000\000\000\000IEND\256B`\202"
    oid = @fixture.commit_files({
      "README"      => "hello",
      "subdir/test" => "somedata",
      "heavy_minus_sign.png" => png_data,
      "big5.md" => File.read(File.expand_path("../../examples/big5.md", __FILE__)),
    }, "commit")

    assert_equal Encoding.find("UTF-8"), @client.read_tree_entry(oid, "README")["data"].encoding
    assert_equal Encoding.find("binary"), @client.read_tree_entry(oid, "heavy_minus_sign.png")["data"].encoding
    assert_equal Encoding.find("Big5"), @client.read_tree_entry(oid, "big5.md")["data"].encoding
  end

  def test_raise_invalid_full_oid
    assert_raises GitRPC::InvalidFullOid do
      @client.read_tree_entries(nil, path: "")
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.read_tree_entries("lol", path: "")
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.read_tree_entry(nil, path: "README")
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.read_tree_entry("lol", path: "README")
    end
  end

  def test_read_tree_entry_oids
    file1 = "README"
    file2 = "subdir/test"
    response = @client.read_tree_entry_oids(@commit, [file1, file2])

    assert_equal 2, response.size
    assert_equal "b6fc4c620b67d95f953a5c1c1230aaab5db5a1b0", response[file1]
    assert_equal "16ac089539f23bae4bfbc861281e48dfaabd233d", response[file2]
  end

  def test_read_tree_entry_oids_with_bad_tree
    assert_raises GitRPC::ObjectMissing do
      @client.read_tree_entry_oids("19102815663d23f8b75a47e7a01965dcdc96c864", ["README"])
    end
  end

  def test_read_tree_entry_oids_with_bad_path
    assert_raises GitRPC::NoSuchPath do
      @client.read_tree_entry_oids(@commit, ["no/such/path"])
    end
  end

  def test_read_tree_entry_oid_with_path
    expected = @client.rev_parse(@commit + ":subdir")
    result = @client.read_tree_entry_oid(@commit, "subdir", :type => "tree")

    assert_equal expected, result

    assert_raises GitRPC::InvalidObject do
      @client.read_tree_entry_oid(@commit, "subdir", :type => "blob")
    end

    assert_raises GitRPC::NoSuchPath do
      @client.read_tree_entry_oid(@commit, "no/such/path")
    end

  end

  def test_read_tree_entry_oid_without_path
    expected = @client.rev_parse(@commit + "^{tree}")
    result = @client.read_tree_entry_oid(@commit, "")

    assert_equal expected, result
  end
end
