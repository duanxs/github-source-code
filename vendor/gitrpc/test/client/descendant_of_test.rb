# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class ClientDescendantOfTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    setup_test_commits

    @cache  = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => @cache)
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @commit1 = @fixture.add_empty_commit("commit 1")
    @commit2 = @fixture.add_empty_commit("commit 2")
    @commit3 = @fixture.add_empty_commit("commit 3")

    @fixture.command "git symbolic-ref HEAD refs/heads/noparents"
    @disjoint_oid = @fixture.add_empty_commit("other-branch commit")

    @fixture.command "git symbolic-ref HEAD refs/heads/master"
    @master_oid = @fixture.rev_parse("master")

    @commit4 = @fixture.commit_files({"README" => "Hello."}, "commit 4")
  end

  def test_descendant_of_with_oids
    assert @client.descendant_of?(@master_oid, @commit2)
    refute @client.descendant_of?(@commit2, @master_oid)
  end

  def test_descendant_of_with_ref
    assert_raises(GitRPC::InvalidFullOid) do
      @client.descendant_of?("master", @master_oid)
    end

    assert_raises(GitRPC::InvalidFullOid) do
      @client.descendant_of?(@master_oid, "master")
    end
  end

  def test_descendant_of_with_short_sha
    assert_raises(GitRPC::InvalidFullOid) do
      @client.descendant_of?(@master_oid[0, 7], @master_oid)
    end

    assert_raises(GitRPC::InvalidFullOid) do
      @client.descendant_of?(@master_oid, @master_oid[0, 7])
    end
  end

  def test_descendant_of_with_missing_object
    assert_raises(GitRPC::ObjectMissing) do
      @client.descendant_of?("deadbeef" * 5, @master_oid)
    end
  end

  def test_descendant_of_with_blob_oid
    commit = @client.read_commits([@commit4]).first
    tree   = @client.read_trees([commit["tree"]]).first

    assert_raises(GitRPC::InvalidObject) do
      @client.descendant_of?(tree["entries"]["README"]["oid"], @master_oid)
    end
  end

  def test_descendant_of_without_common_ancestors
    refute @client.descendant_of?(@disjoint_oid, @master_oid)
    refute @client.descendant_of?(@master_oid, @disjoint_oid)
  end

  def test_descendant_of_offline_cache
    cache_key = @client.descendant_of_key(@master_oid, @commit2)

    # We get nil in case memcache servers are offline
    @cache.stub(:get_multi, {cache_key => nil}) do
      assert @client.descendant_of?(@master_oid, @commit2)
    end
  end

  def test_bulk_descendant_of_with_oids
    expected = {
      [@master_oid, @commit2] => true,
      [@commit2, @master_oid] => false
    }

    assert_equal expected, @client.descendant_of([
      [@master_oid, @commit2],
      [@commit2, @master_oid]
    ])
  end

  def test_bulk_descendant_of_without_common_ancestors
    expected = {
      [@disjoint_oid, @master_oid] => false,
      [@master_oid, @disjoint_oid] => false
    }

    assert_equal expected, @client.descendant_of([
      [@disjoint_oid, @master_oid],
      [@master_oid, @disjoint_oid]
    ])
  end
end
