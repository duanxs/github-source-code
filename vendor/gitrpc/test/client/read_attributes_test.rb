# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class ClientReadAttributesTest < Minitest::Test
  include GitRPC::Util

  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => cache)

    setup_test_commits
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @commit1 = @fixture.commit_files({
      ".gitattributes"        => "README.md linguist-documentation=false",
      "README.md"             => "hello",
      "foo.rb"                => "bar",
      "subdir/.gitattributes" => "*.rb linguist-language=Java",
      "subdir/test.rb"        => "somedata",
      "another-subdir/.gitattributes" => "*.rb linguist-language=Java\n❤.rb linguist-documentation=false",
      "another-subdir/❤.rb"   => "bar",
    }, "commit 1")

    @tree1 = @fixture.rugged.lookup(@commit1).tree
  end

  def test_read_attributes_with_subdirectories
    paths = ["README.md", "subdir/test.rb", "another-subdir/❤.rb"]
    result = @client.read_attributes(@commit1, paths,
                keys: %w(linguist-language linguist-documentation))

    expected_result = {
      "README.md" => {
        "linguist-language" => nil,
        "linguist-documentation" => "false"
      },
      "subdir/test.rb" => {
        "linguist-language" => "Java",
        "linguist-documentation" => nil
      },
      "another-subdir/❤.rb" => {
        "linguist-language" => "Java",
        "linguist-documentation" => "false"
      }
    }
    assert_equal expected_result, result
  end

  def assert_requested_paths(commit, requested_paths:, fresh_paths:)
    if ruby_2_6?
      @client.backend.stub(:send_message, -> (_, _, paths, _, _) {
        assert_equal paths, fresh_paths
        []
      }) do
        @client.read_attributes(commit, requested_paths,
          keys: %w(linguist-language linguist-documentation))
      end
    else
      @client.backend.stub(:send_message, -> (_, _, paths, _) {
        assert_equal paths, fresh_paths
        []
      }) do
        @client.read_attributes(commit, requested_paths,
          keys: %w(linguist-language linguist-documentation))
      end
    end
  end

  def test_read_attributes_with_empty_cache
    @client.stub(:read_objects, -> (*args) { [{ "tree" => @tree1.oid }] }) do
      assert_requested_paths(@commit1,
        requested_paths: ["README.md"],
        fresh_paths:     ["README.md"]
      )
    end
  end

  def test_read_attributes_with_partial_cache_hit
    # partially fill cache
    @client.read_attributes(@commit1, ["README.md"],
      keys: %w(linguist-language linguist-documentation))

    @client.stub(:read_objects, -> (*args) { [{ "tree" => @tree1.oid }] }) do
      assert_requested_paths(@commit1,
        requested_paths: ["README.md", "subdir/test.rb"],
        fresh_paths:     ["subdir/test.rb"]
      )
    end
  end

  def test_read_attributes_with_full_cache_hit
    # fill cache
    @client.read_attributes(@commit1, ["README.md", "subdir/test.rb"],
      keys: %w(linguist-language linguist-documentation))

    @client.stub(:read_objects, -> (*args) { [{ "tree" => @tree1.oid }] }) do
      @client.backend.stub(:send_message, -> (*args) { fail "unexpected call" }) do
        @client.read_attributes(@commit1, ["README.md", "subdir/test.rb"],
          keys: %w(linguist-language linguist-documentation))
      end
    end
  end

  def test_read_attributes_with_commit_and_same_tree_hits_cache
    # fill cache
    @client.read_attributes(@commit1, ["README.md", "subdir/test.rb"],
      keys: %w(linguist-language linguist-documentation))

    commit2 = @fixture.add_empty_commit(":wave:")
    tree2 = @fixture.rugged.lookup(commit2).tree
    assert_equal @tree1.oid, tree2.oid # sanity check

    @client.stub(:read_objects, -> (*args) { [{ "tree" => tree2.oid }] }) do
      @client.backend.stub(:send_message, -> (*args) { fail "unexpected call" }) do
        @client.read_attributes(commit2, ["README.md", "subdir/test.rb"],
          keys: %w(linguist-language linguist-documentation))
      end
    end
  end

  def test_read_attributes_with_commit_and_new_tree_does_not_hit_cache
    # fill cache
    @client.read_attributes(@commit1, ["README.md", "subdir/test.rb"],
      keys: %w(linguist-language linguist-documentation))

    commit2 = @fixture.commit_files({ "bar.rb" => "foo" }, ":wave:")
    tree2 = @fixture.rugged.lookup(commit2).tree
    refute_equal @tree1.oid, tree2.oid # sanity check

    @client.stub(:read_objects, -> (*args) { [{ "tree" => tree2.oid }] }) do
      assert_requested_paths(commit2,
        requested_paths: ["README.md", "subdir/test.rb"],
        fresh_paths:     ["README.md", "subdir/test.rb"]
      )
    end
  end

  def test_read_attributes_with_different_keys
    # fill cache with one key
    @client.read_attributes(@commit1, ["README.md"],
      keys: %w(linguist-language))

    @client.stub(:read_objects, -> (*args) { [{ "tree" => @tree1.oid }] }) do
      # request same path with two keys
      assert_requested_paths(@commit1,
        requested_paths: ["README.md"],
        fresh_paths:     ["README.md"]
      )
    end
  end

  def test_read_attributes_with_subset_of_present_keys
    paths = ["README.md", "subdir/test.rb", "another-subdir/❤.rb"]
    result = @client.read_attributes(@commit1, paths,
                keys: %w(linguist-language))

    expected_result = {
      "README.md" => {
        "linguist-language" => nil
      },
      "subdir/test.rb" => {
        "linguist-language" => "Java"
      },
      "another-subdir/❤.rb" => {
        "linguist-language" => "Java"
      }
    }
    assert_equal expected_result, result
  end

  def test_read_attributes_with_invalid_commit_oid
    assert_raises(GitRPC::InvalidFullOid) do
      @client.read_attributes("foobar", ["README.md"])
    end
  end

  def test_read_attributes_with_unknown_commit_oid
    assert_raises(GitRPC::ObjectMissing) do
      @client.read_attributes(SecureRandom.hex(20), ["README.md"])
    end
  end
end
