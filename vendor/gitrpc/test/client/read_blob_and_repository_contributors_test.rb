# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class ClientReadBlobContributorsTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    @expected = setup_test_commits
    @head = @fixture.rev_parse("HEAD")

    @cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => @cache)
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    [{ :files => { "README" => "some readme content" },
        :message => "first commit",
        :name => "First Commit Author",
        :email => "firstcommitauthor@blah.com" },
      { :files => { "README" => "modified readme content" },
        :message => "second commit\n\nCo-authored-by: Fifth Commit Author <fifthcommitauthor@blah.com>",
        :name  => "Second Commit Author",
        :email => "secondcommitauthor@blah.com" },
      { :files => { "other_file" => "some other stuff" },
        :message => "third commit",
        :name  => "Third Commit Author",
        :email => "thirdcommitauthor@blah.com" },
      { :files => { "other_file" => "modified the stuff" },
        :message => "fourth commit",
        :name  => "Fourth Commit Author",
        :email => "fourthcommitauthor@blah.com" }
    ].map do |commit|
      commit[:sha] = @fixture.commit_files(
        commit[:files],
        commit[:message],
        { :name => commit[:name], :email => commit[:email] })
      commit
    end
  end

  def test_read_blob_contributors
    res = @client.read_blob_contributors(@head, "README")

    assert !res["truncated"]
    assert_equal 2, res["data"].size
    assert_matching_author @expected[1], res["data"].first
    assert_matching_author @expected[0], res["data"].last

    res = @client.read_blob_contributors(@head, "other_file")

    assert !res["truncated"]
    assert_equal 2, res["data"].size
    assert_matching_author @expected[3], res["data"].first
    assert_matching_author @expected[2], res["data"].last
  end

  def test_read_blob_contributors_exceptions
    assert_raises(GitRPC::ObjectMissing, "") {
      @client.read_blob_contributors("0" * 40, "README")
    }

    assert_raises(GitRPC::ObjectMissing, "") {
      @client.read_blob_contributors(@head, "missing_file")
    }

    assert_raises(GitRPC::ObjectMissing, "") {
      @client.read_blob_contributors(@head, nil)
    }

    assert_raises(GitRPC::ObjectMissing, "") {
      @client.read_blob_contributors(@head, "")
    }
  end

  def test_read_blob_contributors_group_by_signature
    sha = @fixture.commit_files(
      { "README" => "moar changes" },
      "changes",
      { :name => "1st Commit Author", :email => "firstcommitauthor@blah.com" }
    )

    res_by_email = @client.read_blob_contributors(sha, "README", group_by_signature: false)
    res_by_sig   = @client.read_blob_contributors(sha, "README", group_by_signature: true)
    res_default  = @client.read_blob_contributors(sha, "README")

    assert_equal res_by_email, res_default
    assert_equal 2, res_by_email["data"].size
    assert_equal 3, res_by_sig["data"].size
  end

  def test_read_blob_contributors_writes_cache
    assert_equal 0, @cache.hash.size

    res = @client.read_blob_contributors(@head, "README")
    assert_equal 1, @cache.hash.size
    assert_includes @cache.hash.first.to_s, Digest::MD5.hexdigest("README")

    assert_equal 2, res["data"].size
    assert_matching_author @expected[1], res["data"].first

    res = @client.read_blob_contributors(@head, "other_file")
    assert_equal 2, @cache.hash.size

    @client.backend.stub :send_message, lambda { fail } do
      res = @client.read_blob_contributors(@head, "README")

      assert_equal 2, res["data"].size
      assert_matching_author @expected[1], res["data"].first
    end

    res = @client.read_blob_contributors(@head, "README", co_authors: true)
    assert_equal 3, @cache.hash.size
    res = @client.read_blob_contributors(@head, "README", co_authors: true)
    assert_equal 3, @cache.hash.size

    res = @client.read_blob_contributors(@head, "README", group_by_signature: true)
    assert_equal 4, @cache.hash.size
    res = @client.read_blob_contributors(@head, "README", group_by_signature: true)
    assert_equal 4, @cache.hash.size

    res = @client.read_blob_contributors(@head, "README", co_authors: true, group_by_signature: true)
    assert_equal 5, @cache.hash.size
    res = @client.read_blob_contributors(@head, "README", co_authors: true, group_by_signature: true)
    assert_equal 5, @cache.hash.size
  end

  def test_patch_text_raise_invalid_full_oid
    assert_raises GitRPC::InvalidFullOid do
      @client.read_blob_contributors(nil, "README")
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.read_blob_contributors("lol", "README")
    end
  end

  def test_read_blob_contributors_co_authors
    res = @client.read_blob_contributors(@head, "README", co_authors: true)
    assert_equal 3, res["data"].size
  end

  # Assertion helpers

  def assert_matching_author(expected_data, author_data)
    assert_equal expected_data[:email], author_data["author"]
    assert_equal expected_data[:sha], author_data["commit"]
    assert_kind_of Numeric, author_data["time"]
    assert_kind_of Numeric, author_data["offset"]
  end
end
