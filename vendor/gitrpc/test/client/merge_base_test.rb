# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class ClientMergeBaseTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    setup_test_commits

    @cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => @cache)
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @commit1 = @fixture.add_empty_commit("commit 1")
    @fixture.command "git tag v0.1 HEAD"
    @tag_oid = @fixture.rev_parse("v0.1")

    @commit2 = @fixture.add_empty_commit("commit 2")
    @fixture.command "git branch some-branch HEAD"
    @branch_oid = @fixture.rev_parse("some-branch")

    @fixture.command "git tag -a -m 'v0.2 release' v0.2 HEAD"
    @annotated_oid = @fixture.rev_parse("v0.2")

    @commit3 = @fixture.add_empty_commit("commit 3")

    @fixture.command "git symbolic-ref HEAD refs/heads/noparents"
    @commit4 = @fixture.add_empty_commit("commit 4")

    @fixture.command "git symbolic-ref HEAD refs/heads/master"
    @master_oid = @fixture.rev_parse("master")

    @commit5 = @fixture.commit_files({"README" => "Hello."}, "commit 5")
  end

  def test_merge_base_from_oid
    assert_equal @commit2, @client.merge_base(@branch_oid, @master_oid)
    assert_equal @commit2, @client.best_merge_base(@branch_oid, @master_oid)
  end

  def test_merge_base_short_sha
    assert_raises(GitRPC::InvalidFullOid) { @client.merge_base(@branch_oid[0, 7], @master_oid) }
    assert_raises(GitRPC::InvalidFullOid) { @client.best_merge_base(@branch_oid[0, 7], @master_oid) }
  end

  def test_merge_base_with_bad_object
    assert_raises GitRPC::InvalidObject do
      @client.merge_base("deadbeefcafe0000000000000000000000000000", @master_oid)
    end
  end

  def test_best_merge_base_with_bad_object
    assert_raises GitRPC::InvalidObject do
      @client.best_merge_base("deadbeefcafe0000000000000000000000000000", @master_oid)
    end
  end

  def test_merge_base_with_blob_oid
    commit = @client.read_commits([@commit5]).first
    tree = @client.read_trees([commit["tree"]]).first
    assert_raises GitRPC::InvalidObject do
      @client.merge_base(tree["entries"]["README"]["oid"], @master_oid)
    end
  end

  def test_best_merge_base_with_blob_oid
    commit = @client.read_commits([@commit5]).first
    tree = @client.read_trees([commit["tree"]]).first
    assert_raises GitRPC::InvalidObject do
      @client.merge_base(tree["entries"]["README"]["oid"], @master_oid)
    end
  end

  def test_merge_base_without_common_ancestors
    assert @noparents_oid = @fixture.rev_parse("noparents")
    assert_nil @client.merge_base(@noparents_oid, @master_oid)
    assert_nil @client.best_merge_base(@noparents_oid, @master_oid)
  end

  def test_merge_base_with_nil_head_or_base
    assert_raises(GitRPC::InvalidFullOid) { @client.merge_base(nil, @master_oid) }
    assert_raises(GitRPC::InvalidFullOid) { @client.best_merge_base(nil, @master_oid) }
    assert_raises(GitRPC::InvalidFullOid) { @client.merge_base(@master_oid, nil) }
    assert_raises(GitRPC::InvalidFullOid) { @client.best_merge_base(@master_oid, nil) }
  end

  def test_merge_base_with_null_head_or_base
    assert_raises(GitRPC::InvalidObject) { @client.merge_base(GitRPC::NULL_OID, @master_oid) }
    assert_raises(GitRPC::InvalidObject) { @client.best_merge_base(GitRPC::NULL_OID, @master_oid) }
    assert_raises(GitRPC::InvalidObject) { @client.merge_base(@master_oid, GitRPC::NULL_OID) }
    assert_raises(GitRPC::InvalidObject) { @client.best_merge_base(@master_oid, GitRPC::NULL_OID) }
  end

  def test_branch_base
    branch_base = @client.branch_base(@commit5, @commit4, @commit4)
    assert_equal @commit4, branch_base
  end
end
