# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class ClientNativeReadDiff < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @commit1 = @fixture.commit_files({"README" => "Hello."}, "commit 1")
    @commit2 = @fixture.commit_files({"README" => "Goodbye."}, "commit 2")
    @commit3 = @fixture.commit_files({"README" => "Hi again."}, "commit 3")

    work_tree = "#{@fixture.path}/work"
    @fixture.command "git --work-tree='#{work_tree}' mv README README.md"
    @fixture.commit_changes "rename"
    @rename = @fixture.rev_parse("HEAD")

    @big_commit = @fixture.commit_files({
        "README" =>
        "Hello. This is the README.\n" +
        "It's also a multi-line change.\n" +
        ("\n" * 80) +
        "Very nice."
    }, "big commit")

    @bigger_commit = @fixture.commit_files({
        "README" =>
        "Hello. This is the README!\n" +
        "It's also a multi-line change.\n" +
        ("\n" * 80) +
        "VERY nice!"
        }, "big commit")

    @fixture.command("git --work-tree='#{work_tree}' checkout -b merge-test")
    @commit6 = @fixture.commit_files({"README" => "Hello AGAIN!"}, "commit 6", {:branch => "merge-test"})
    @commit7 = @fixture.commit_files({"README" => "Hello AGAIN! AGAIN"}, "commit 7")
    @commit8 = @fixture.commit_files({"README" => "Once more", "new_file" => "Something new!"}, "multiple files")

    @cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => @cache)
  end

  def teardown
    @fixture.teardown
  end

  def test_diff_text_sets_cache_keys_correctly
    kwarg_md5 = Digest::MD5.hexdigest("{:full_index=>false}")
    assert_equal ["v2::#@commit1:#@commit2:diff-text:v2:#{kwarg_md5}"], @cache.track_sets {
      @client.native_diff_text(@commit1, @commit2)
    }

    md5 = Digest::MD5.hexdigest("new_file")
    assert_equal ["v2::#@commit7:#@commit8:diff-text:v2:#{md5}:#{kwarg_md5}"], @cache.track_sets {
      @client.native_diff_text(@commit7, @commit8, paths: "new_file")
    }
  end

  def test_diff_text
    expected = "diff --git a/README b/README\nindex 63af512..9407a03 100644\n--- a/README\n+++ b/README\n@@ -1 +1 @@\n-Hello.\n\\ No newline at end of file\n+Goodbye.\n\\ No newline at end of file\n"
    assert_equal expected, @client.native_diff_text(@commit1, @commit2)
  end

  def test_diff_text_with_full_index
    expected = "diff --git a/README b/README\nindex 63af512c383ca207450e4340019cf55737bb40a0..9407a031d6de7be6466c72c1d6d58c9c3d9d2866 100644\n--- a/README\n+++ b/README\n@@ -1 +1 @@\n-Hello.\n\\ No newline at end of file\n+Goodbye.\n\\ No newline at end of file\n"
    assert_equal expected, @client.native_diff_text(@commit1, @commit2, full_index: true)
  end

  def test_diff_text_with_root_commit
    expected = "diff --git a/README b/README\nnew file mode 100644\nindex 0000000..63af512\n--- /dev/null\n+++ b/README\n@@ -0,0 +1 @@\n+Hello.\n\\ No newline at end of file\n"
    assert_equal expected, @client.native_diff_text(@commit1)
  end

  def test_diff_text_with_root_commit_with_full_index
    expected = "diff --git a/README b/README\nnew file mode 100644\nindex 0000000000000000000000000000000000000000..63af512c383ca207450e4340019cf55737bb40a0\n--- /dev/null\n+++ b/README\n@@ -0,0 +1 @@\n+Hello.\n\\ No newline at end of file\n"
    assert_equal expected, @client.native_diff_text(@commit1, full_index: true)
  end

  def test_diff_text_with_path
    expected = "diff --git a/new_file b/new_file\nnew file mode 100644\nindex 0000000..ba5f5c1\n--- /dev/null\n+++ b/new_file\n@@ -0,0 +1 @@\n+Something new!\n\\ No newline at end of file\n"
    assert_equal expected, @client.native_diff_text(@commit7, @commit8, paths: "new_file")
  end

  def test_diff_text_with_path_with_full_index
    expected = "diff --git a/new_file b/new_file\nnew file mode 100644\nindex 0000000000000000000000000000000000000000..ba5f5c1c6f35fe2a3a4b1cffd78a7dff086ace76\n--- /dev/null\n+++ b/new_file\n@@ -0,0 +1 @@\n+Something new!\n\\ No newline at end of file\n"
    assert_equal expected, @client.native_diff_text(@commit7, @commit8, paths: "new_file", full_index: true)
  end

  def test_diff_text_with_multiple_paths
    expected = "diff --git a/README b/README\nindex 0802f10..579c911 100644\n--- a/README\n+++ b/README\n@@ -1 +1 @@\n-Hello AGAIN! AGAIN\n\\ No newline at end of file\n+Once more\n\\ No newline at end of file\ndiff --git a/new_file b/new_file\nnew file mode 100644\nindex 0000000..ba5f5c1\n--- /dev/null\n+++ b/new_file\n@@ -0,0 +1 @@\n+Something new!\n\\ No newline at end of file\n"
    assert_equal expected, @client.native_diff_text(@commit7, @commit8, paths: ["new_file", "README"])
  end

  def test_diff_text_with_multiple_paths_with_full_index
    expected = "diff --git a/README b/README\nindex 0802f10ba9394791f2292e4f30cdd521acdd68e8..579c9117654d3f461575db2d5c22795824070772 100644\n--- a/README\n+++ b/README\n@@ -1 +1 @@\n-Hello AGAIN! AGAIN\n\\ No newline at end of file\n+Once more\n\\ No newline at end of file\ndiff --git a/new_file b/new_file\nnew file mode 100644\nindex 0000000000000000000000000000000000000000..ba5f5c1c6f35fe2a3a4b1cffd78a7dff086ace76\n--- /dev/null\n+++ b/new_file\n@@ -0,0 +1 @@\n+Something new!\n\\ No newline at end of file\n"
    assert_equal expected, @client.native_diff_text(@commit7, @commit8, paths: ["new_file", "README"], full_index: true)
  end

  def test_diff_text_with_bogus_arg_with_root
    assert_raises(GitRPC::InvalidObject) do
      @client.native_diff_text("0123abcdef"*4)
    end
  end

  def test_patch_text_sets_cache_keys_correctly
    kwarg_md5 = Digest::MD5.hexdigest("{:full_index=>false}")
    assert_equal ["v2::#@commit1:#@commit2:patch-text:v2:#{kwarg_md5}"], @cache.track_sets {
      @client.native_patch_text(@commit1, @commit2)
    }
  end

  def test_patch_text
    expected = "From 3bb0efd2b78c305a621e9acde284ad561d252211 Mon Sep 17 00:00:00 2001\nFrom: Some Author <someone@example.org>\nDate: Tue, 17 Jul 2012 20:21:53 -0700\nSubject: [PATCH] commit 7\n\n---\n README | 84 +---------------------------------------------------------\n 1 file changed, 1 insertion(+), 83 deletions(-)\n\ndiff --git a/README b/README\nindex 51e8c26..0802f10 100644\n--- a/README\n+++ b/README\n@@ -1,83 +1 @@\n-Hello. This is the README!\n-It's also a multi-line change.\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-VERY nice!\n\\ No newline at end of file\n+Hello AGAIN! AGAIN\n\\ No newline at end of file\n"
    assert_equal expected, @client.native_patch_text(@commit6, @commit7)
  end

  def test_patch_text_with_full_index
    expected = "From 3bb0efd2b78c305a621e9acde284ad561d252211 Mon Sep 17 00:00:00 2001\nFrom: Some Author <someone@example.org>\nDate: Tue, 17 Jul 2012 20:21:53 -0700\nSubject: [PATCH] commit 7\n\n---\n README | 84 +---------------------------------------------------------\n 1 file changed, 1 insertion(+), 83 deletions(-)\n\ndiff --git a/README b/README\nindex 51e8c266dc5ae6683de58293da2333fad58f50fc..0802f10ba9394791f2292e4f30cdd521acdd68e8 100644\n--- a/README\n+++ b/README\n@@ -1,83 +1 @@\n-Hello. This is the README!\n-It's also a multi-line change.\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-\n-VERY nice!\n\\ No newline at end of file\n+Hello AGAIN! AGAIN\n\\ No newline at end of file\n"
    assert_equal expected, @client.native_patch_text(@commit6, @commit7, full_index: true)
  end

  def test_patch_text_with_root_commit
    expected = "From fd75a9c501ed274b215457d4b22185f93f248358 Mon Sep 17 00:00:00 2001\nFrom: Some Author <someone@example.org>\nDate: Tue, 17 Jul 2012 20:21:53 -0700\nSubject: [PATCH] commit 1\n\n---\n README | 1 +\n 1 file changed, 1 insertion(+)\n create mode 100644 README\n\ndiff --git a/README b/README\nnew file mode 100644\nindex 0000000..63af512\n--- /dev/null\n+++ b/README\n@@ -0,0 +1 @@\n+Hello.\n\\ No newline at end of file\n"
    assert_equal expected, @client.native_patch_text(@commit1)
  end

  def test_patch_text_with_root_commit_with_full_index
    expected = "From fd75a9c501ed274b215457d4b22185f93f248358 Mon Sep 17 00:00:00 2001\nFrom: Some Author <someone@example.org>\nDate: Tue, 17 Jul 2012 20:21:53 -0700\nSubject: [PATCH] commit 1\n\n---\n README | 1 +\n 1 file changed, 1 insertion(+)\n create mode 100644 README\n\ndiff --git a/README b/README\nnew file mode 100644\nindex 0000000000000000000000000000000000000000..63af512c383ca207450e4340019cf55737bb40a0\n--- /dev/null\n+++ b/README\n@@ -0,0 +1 @@\n+Hello.\n\\ No newline at end of file\n"
    assert_equal expected, @client.native_patch_text(@commit1, full_index: true)
  end

  def test_diff_text_raise_invalid_full_oid
    assert_raises GitRPC::InvalidFullOid do
      @client.native_diff_text(nil)
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.native_diff_text(nil, nil)
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.native_diff_text("lol")
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.native_diff_text("lol", nil)
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.native_diff_text("lol", "lol")
    end
  end

  def test_patch_text_raise_invalid_full_oid
    assert_raises GitRPC::InvalidFullOid do
      @client.native_patch_text(nil)
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.native_patch_text(nil, nil)
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.native_patch_text("lol")
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.native_patch_text("lol", nil)
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.native_patch_text("lol", "lol")
    end
  end
end
