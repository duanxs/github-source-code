# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class ClientCreateMergeCommitTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    @client = GitRPC.new("fakerpc:#{@fixture.path}")
    @rugged = Rugged::Repository.new(@fixture.path)

    @author = {
      "name"  => "Some Human",
      "email" => "human@example.com",
      "time"  => Time.iso8601("2000-01-01T00:00:00Z")
    }

    setup_test_commits
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @base  = @fixture.commit_files({"README" => "Hello."}, "commit 1")
    @fixture.create_branch("changes")
    @fixture.create_branch("deletes")

    @oid3  = @fixture.commit_files({"README" => nil }, "commit 2", :branch => "deletes")
    @oid2  = @fixture.commit_files({"README" => "Auf Wiedersehen."}, "commit 2", :branch => "changes")
    @oid   = @fixture.commit_files({"README" => "Goodbye."}, "commit 2")
  end

  def test_create_merge_commit
    message = "merge it\n"
    merge_oid, err = @client.create_merge_commit(@base, @oid, @author, message)
    assert_nil err

    merge_commit = @client.read_commits([merge_oid]).first

    assert_equal [@oid, @base].sort, merge_commit["parents"].sort
    assert_equal message, merge_commit["message"]

    expected_author = [
      @author["name"],
      @author["email"],
      [@author["time"].to_i, 0]
    ]

    assert_equal expected_author, merge_commit["author"]
    assert_equal expected_author, merge_commit["committer"]
  end

  def test_create_merge_commit_with_conflicts
    message = "merge it\n"
    merge_oid, err, details = @client.create_merge_commit(@oid, @oid2, @author, message, :record_conflicts => true, :more_conflict_info => true)
    assert_equal "merge_conflict", err

    h = {
      :base => "1f97a72adedcfaf540956f70fd71daad432225ab",
      :head => "a83c6a7ac010fab5cf57133363ce52243841c96a",
      :conflicted_files => {
        "README" => {
          :base => {
            :oid => "6b2df6d31eb6138f925416c89b70edc44740136b",
            :file_size=>16,
            :binary=>false
          },
          :head => {
            :oid => "9407a031d6de7be6466c72c1d6d58c9c3d9d2866",
            :file_size => 8,
            :binary => false
          },
          :ancestor => {
            :oid => "63af512c383ca207450e4340019cf55737bb40a0",
            :file_size => 6,
            :binary => false
          },
          :type => :regular_conflict
        }
      },
      :more_conflicted_files => false
    }
    assert_equal h, details

    g = @client.merge_single_file(
        { :filename => "README", :mode => 100644, :oid => h[:conflicted_files]["README"][:ancestor][:oid] },
        { :filename => "README", :mode => 100644, :oid => h[:conflicted_files]["README"][:base][:oid]     },
        { :filename => "README", :mode => 100644, :oid => h[:conflicted_files]["README"][:head][:oid]     },
      )
    assert_equal g, {
      :automergeable => false,
      :path => "file.txt",
      :filemode => 0100644,
      :data => "<<<<<<< ours\nAuf Wiedersehen.\n=======\nGoodbye.\n>>>>>>> theirs\n"
    }
  end

  def test_create_merge_commit_with_resolved_conflicts
    message = "merge it\n"
    merge_oid, err, details = @client.create_merge_commit(@oid, @oid2, @author, message, :resolve_conflicts => {
      "README" => "Hello.\n"
    })

    merge_commit = @client.read_commits([merge_oid]).first
    assert_equal [@oid, @oid2].sort, merge_commit["parents"].sort
    assert_equal message, merge_commit["message"]

    expected_author = [
      @author["name"],
      @author["email"],
      [@author["time"].to_i, 0]
    ]

    assert_equal expected_author, merge_commit["author"]
    assert_equal expected_author, merge_commit["committer"]

    tree_oid = merge_commit["tree"]
    tree = @client.read_trees([tree_oid]).first
    blob = @client.read_blobs([tree["entries"]["README"]["oid"]]).first
    assert_equal "Hello.\n", blob["data"]
  end

  def test_create_merge_commit_with_deletes
    message = "merge it\n"
    merge_oid, err, details = @client.create_merge_commit(@oid, @oid3, @author, message, :record_conflicts => true, :more_conflict_info => true)
    assert_equal "merge_conflict", err

    h = {
      :base => "1f97a72adedcfaf540956f70fd71daad432225ab",
      :head => "ceb2c8a2c26fa7c3552e380953eeb507f0e230fb",
      :conflicted_files => {
        "README" => {
          :base => nil,
          :head => {
            :oid => "9407a031d6de7be6466c72c1d6d58c9c3d9d2866",
            :file_size => 8,
            :binary => false,
          },
          :ancestor => {
            :oid => "63af512c383ca207450e4340019cf55737bb40a0",
            :file_size => 6,
            :binary => false,
          },
          :type => :not_in_theirs
        },
      },
      :more_conflicted_files => false
    }
    assert_equal h, details
  end

  def test_create_merge_commit_with_signature
    message = "merge it\n"
    sig = "fake-signature"

    merge_oid, err = @client.create_merge_commit(@base, @oid, @author, message) do |raw|
      assert raw.is_a?(String)
      sig
    end
    assert_nil err

    merge_commit = @client.read_commits([merge_oid]).first

    assert merge_commit["has_signature"]
    assert_equal sig, @rugged.lookup(merge_oid).header_field("gpgsig")
    assert_equal [@oid, @base].sort, merge_commit["parents"].sort
    assert_equal message, merge_commit["message"]

    expected_author = [
      @author["name"],
      @author["email"],
      [@author["time"].to_i, 0]
    ]

    assert_equal expected_author, merge_commit["author"]
    assert_equal expected_author, merge_commit["committer"]
  end

  def test_create_merge_commit_with_nil_signature
    message = "merge it\n"

    merge_oid, err = @client.create_merge_commit(@base, @oid, @author, message) { |r| nil }
    assert_nil err

    merge_commit = @client.read_commits([merge_oid]).first

    refute merge_commit["has_signature"]
    refute @rugged.lookup(merge_oid).header_field?("gpgsig")
    assert_equal [@oid, @base].sort, merge_commit["parents"].sort
    assert_equal message, merge_commit["message"]

    expected_author = [
      @author["name"],
      @author["email"],
      [@author["time"].to_i, 0]
    ]

    assert_equal expected_author, merge_commit["author"]
    assert_equal expected_author, merge_commit["committer"]
  end
end
