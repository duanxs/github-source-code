# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class ClientReadTreeDiff < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @commit1 = @fixture.commit_files({"README" => "Hello."}, "commit 1")
    @commit2 = @fixture.commit_files({"README" => "Goodbye."}, "commit 2")
    @commit3 = @fixture.commit_files({"README" => "Hi again."}, "commit 3")
    @commit4 = @fixture.commit_files({"foo/bar" => "test"}, "commit 4")
    @commit5 = @fixture.commit_files({"foo/nested/bar" => "test"}, "commit 5")

    work_tree = "#{@fixture.path}/work"
    @fixture.command "git --work-tree='#{work_tree}' mv README README.md"
    @fixture.commit_changes "rename"
    @rename = @fixture.rev_parse("HEAD")

    @big_commit = @fixture.commit_files({
        "README" =>
        "Hello. This is the README.\n" +
        "It's also a multi-line change.\n" +
        ("\n" * 80) +
        "Very nice."
    }, "big commit")

    @bigger_commit = @fixture.commit_files({
        "README" =>
        "Hello. This is the README!\n" +
        "It's also a multi-line change.\n" +
        ("\n" * 80) +
        "VERY nice!"
        }, "big commit")

    @cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => @cache)
  end

  def teardown
    @fixture.teardown
  end

  def test_read_tree_diffs
    diffs = @client.read_tree_diff(@commit1, @commit2)
    diff = diffs.first

    assert_equal "modified", diff["status"]
  end

  def test_assume_parent
    parent_less = @client.read_tree_diff(@commit2)
    parent_with = @client.read_tree_diff(@commit1, @commit2)
    assert_equal parent_less, parent_with
  end

  def test_read_tree_diff_writes_cache
    res = @client.read_tree_diff(@commit2)
    assert_equal 1, @cache.hash.size
  end

  def test_read_tree_diff_raise_invalid_full_oid
    assert_raises GitRPC::InvalidFullOid do
      @client.read_tree_diff(nil)
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.read_tree_diff(nil, nil)
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.read_tree_diff("lol")
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.read_tree_diff("lol", nil)
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.read_tree_diff("lol", "lol")
    end
  end

  def test_read_tree_diffs_paths
    # test commits without changes in the paths
    diffs = @client.read_tree_diff(@commit1, @commit2, paths: ["foo/"])
    assert_equal 0, diffs.size

    # test commits with a change in the paths
    diffs = @client.read_tree_diff(@commit1, @commit4, paths: ["foo/"])
    assert_equal 1, diffs.size

    # test commits with changes within nested folders within the paths
    diffs = @client.read_tree_diff(@commit1, @commit5, paths: ["foo/"])
    assert_equal 2, diffs.size
  end

  def test_read_tree_diffs_cache_paths
    # test commits with a change
    diffs = @client.read_tree_diff(@commit1, @commit4, paths: ["foo/"])
    assert_equal 1, diffs.size

    # test the same commits but with a path where there are no changes
    diffs = @client.read_tree_diff(@commit1, @commit4, paths: ["bar/"])
    assert_equal 0, diffs.size
  end

  def test_read_tree_diffs_diff_trees
    commit1_tree_oid = Rugged::Repository.bare(@fixture.path).lookup(@commit1).tree.oid

    # test does not allow trees
    assert_raises GitRPC::InvalidObject do
      @client.read_tree_diff(commit1_tree_oid, @commit4)
    end

    # test allows trees
    diffs = @client.read_tree_diff(commit1_tree_oid, @commit2, diff_trees: true)
    assert_equal 1, diffs.size
  end
end
