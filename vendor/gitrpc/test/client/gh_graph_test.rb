# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"
require "securerandom"

require "minitest/mock"

class ClientGhGraphTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", cache: @cache)
    @time = Time.local(2000)
  end

  def teardown
    @fixture.teardown
  end

  def zone_offset
    Time.zone_offset(@time.zone)
  end

  def setup_test_commits
    @oid1 = @fixture.rugged_commit_files({ "README" => "Bonjour." }, "commit 1", time: @time)

    message = "commit 2\n\nco-authored-by: Homer Simpson <hsimpson@github.com>"
    @oid2 = @fixture.rugged_commit_files({ "README" => "Hello.\nWorld.\n" }, message, time: @time, parents: [@oid1])
  end

  def test_clear_graph_cache
    expected_path = File.join(@fixture.path, GitRPC::Backend::ContributionCaches::V1::PATH)
    refute File.exist?(expected_path)

    # first clear without any cache to ensure this is safe
    @client.clear_graph_cache
    refute File.exist?(expected_path)

    cache_file = GitRPC::Backend::ContributionCaches.new(@client.backend.backend).new_instance(version: 1)
    cache_file.write("a" * 40, [[1, 2, 3, 4, "email", 5]])

    assert File.exist?(expected_path)

    @client.clear_graph_cache
    refute File.exist?(expected_path)
  end

  def test_happy_path
    setup_test_commits

    expected_entries = [
      [@time.to_i, 946166400, 2, 1, "someone@example.org", zone_offset],
      [@time.to_i, 946166400, 1, 0, "someone@example.org", zone_offset]
    ]

    data = @client.gh_graph_data(@oid2)
    assert_equal expected_entries, data

    assert File.exist?(File.join(@fixture.path, GitRPC::Backend::ContributionCaches::V1::PATH))
  end

  def test_empty_commit_message
    @oid1 = @fixture.rugged_commit_files({ "README" => "Bonjour." }, "", time: @time)
    @oid2 = @fixture.rugged_commit_files({ "README" => "Hello." }, "", time: @time, parents: [@oid1])

    expected_entries = [
      [@time.to_i, 946166400, 1, 1, "someone@example.org", zone_offset],
      [@time.to_i, 946166400, 1, 0, "someone@example.org", zone_offset]
    ]

    data = @client.gh_graph_data(@oid2)
    assert_equal expected_entries, data
  end

  def test_empty_commit
    message = "commit 1\n\nco-authored-by: Ned Flanders <nflanders@github.com>"
    @oid1 = @fixture.add_simple_empty_commit(message, time: @time)
    @oid2 = @fixture.rugged_commit_files({ "README" => "Hello." }, "commit 2", time: @time, parents: [@oid1])

    expected = [[@time.to_i, 946166400, 1, 0, "someone@example.org", zone_offset]]

    data = @client.gh_graph_data(@oid2)
    assert_equal expected, data
  end

  def test_concatenation_to_previous_data
    message = "commit 1\n\nco-authored-by: Ned Flanders <nflanders@github.com>"
    oid1 = @fixture.rugged_commit_files({ "README" => "Bonjour." }, message, time: @time)

    emails = "someone@example.org"
    emails2 = "someone@example.org"

    row1 = [@time.to_i, 946166400, 1, 0, emails, zone_offset]
    row2 = [@time.to_i, 946166400, 1, 1, emails2, zone_offset]

    data = @client.gh_graph_data(oid1)
    assert_equal [row1], data

    message = "commit 2\n\nco-authored-by: Lisa Simpson <lsimpson@github.com>"
    oid2 = @fixture.rugged_commit_files({ "README" => "Hello reader!" }, message, time: @time, parents: [oid1])

    data = @client.gh_graph_data(oid2)
    assert_equal [row2, row1], data
  end

  def test_destructive_push
    message = "commit 1\n\nco-authored-by: Ned Flanders <nflanders@github.com>"
    oid1 = @fixture.rugged_commit_files({ "README" => "Bonjour." }, message, time: @time,
      parents: [])

    row1 = [@time.to_i, 946166400, 1, 0, "someone@example.org", zone_offset]
    row2 = [@time.to_i, 946166400, 2, 0, "hsimpson@github.com", zone_offset]

    data = @client.gh_graph_data(oid1)
    assert_equal [row1], data

    message = "commit 2\n\nco-authored-by: Lisa Simpson <lsimpson@github.com>"
    oid2 = @fixture.rugged_commit_files({ "README" => "Hello\nreader!" },
      message, time: @time, email: "hsimpson@github.com", parents: [])

    # simulate deletion of unreachable commit
    File.delete(File.join(@fixture.path, "objects", oid1[0, 2], oid1[2, 38]))

    data = @client.gh_graph_data(oid2)
    assert_equal [row2], data
  end

  def test_corrupted_cache
    File.open("#{@fixture.path}/info/graphs", "w") do |f|
      f.write "# Not Zlib::Deflated graph data"
    end

    oid1 = @fixture.rugged_commit_files({ "README" => "Bonjour." }, "commit 1", time: @time)
    data = @client.gh_graph_data(oid1)

    assert_equal [[@time.to_i, 946166400, 1, 0, "someone@example.org", zone_offset]], data
  end

  def test_nil_commit
    assert_equal [], @client.gh_graph_data(nil, version: 2)
  end

  def test_gh_graph_data_by_day_and_author
    oid = @fixture.rugged_commit_files({ "A" => "AAA\nAA\nA" }, "c1\n\nco-authored-by: Cee Cee <c@github.com>", email: "a@github.com", time: Time.utc(2012, 11, 18))
    oid = @fixture.rugged_commit_files({ "Z" => "ZZZ\nZ\nZ\nZ" }, "c2", parents: [oid], email: "a@github.com", time: Time.utc(2012, 11, 18))
    oid = @fixture.rugged_commit_files({ "A" => "zzz" }, "c3\n\nco-authored-by: Bee Bee <b@github.com>", email: "d@github.com", parents: [oid], time: Time.utc(2012, 11, 22))
    oid = @fixture.rugged_commit_files({ "C" => "CCC", "E" => "EEEE" }, "c4", email: "a@github.com", parents: [oid], time: Time.utc(2013, 3, 30))

    # the following commit will be attributed to the 26th, with a -4 offset. It is important that
    # we put it in the correct bucket, which is the 25th as commits are attributed WRT the
    # author's timezone.
    oid = @fixture.rugged_commit_files({ "D" => "DDD", "C" => "CCCCC" }, "c5",
      email: "c@github.com", parents: [oid], time: Time.new(2013, 3, 25, 23, 0, 0, "-04:00"))

    # c@github.com is a world traveller and was in multiple timezones, but they all ended up being
    # March 25th. Also a time traveller as the parent commit was made after the child :)
    oid = @fixture.rugged_commit_files({ "F" => "FFF" }, "c6",
      email: "c@github.com", parents: [oid], time: Time.new(2013, 3, 25, 22, 0, 0, "+11:00"))

    data = @client.gh_graph_data_by_day_and_author(to_oid: oid)

    expected_metrics = [
      { additions: 4, deletions: 0, commits: 1, date: "2012-11-18", author: "a@github.com" },
      { additions: 3, deletions: 0, commits: 1, date: "2012-11-18", author: "a@github.com", co_authors: ["c@github.com"] },
      { additions: 1, deletions: 3, commits: 1, date: "2012-11-22", author: "d@github.com", co_authors: ["b@github.com"] },
      { additions: 3, deletions: 1, commits: 2, date: "2013-03-25", author: "c@github.com" },
      { additions: 2, deletions: 0, commits: 1, date: "2013-03-30", author: "a@github.com" }
    ]

    assert_equal expected_metrics, data
  end

  def test_gh_graph_data_by_day_and_author_with_from_oid
    from_oid = @fixture.rugged_commit_files({ "A" => "AAA\nAA\nA" }, "c1\n\nco-authored-by: Cee Cee <c@github.com>", email: "a@github.com", time: Time.utc(2012, 11, 18))
    oid = @fixture.rugged_commit_files({ "A" => "zzz" }, "c2\n\nco-authored-by: Bee Bee <b@github.com>", email: "d@github.com", parents: [from_oid], time: Time.utc(2012, 11, 22))
    oid = @fixture.rugged_commit_files({ "C" => "CCC", "E" => "EEEE" }, "c3", email: "a@github.com", parents: [oid], time: Time.utc(2013, 3, 30))

    expected_metrics = [
      { additions: 1, deletions: 3, commits: 1, date: "2012-11-22", author: "d@github.com", co_authors: ["b@github.com"] },
      { additions: 2, deletions: 0, commits: 1, date: "2013-03-30", author: "a@github.com" }
    ]

    data = @client.gh_graph_data_by_day_and_author(to_oid: oid, from_oid: from_oid)
    assert_equal expected_metrics, data
  end

  def test_gh_graph_data_by_day_and_author_with_co_authors
    time = Time.utc(2012, 11, 18)
    oid = @fixture.rugged_commit_files({ "A" => "AAA\nAA\nA\n" }, "c1\n\nco-authored-by: Eff Eff <f@github.com>\nco-authored-by: Gee Gee <g@github.com>", email: "a@github.com", time: time)
    oid = @fixture.rugged_commit_files({ "A" => "AAAx\nAA\nA\nAAAAA\n" }, "c2\n\nco-authored-by: Gee Gee <g@github.com>\nco-authored-by: Eff Eff <f@github.com>", parents: [oid], email: "a@github.com", time: time)
    oid = @fixture.rugged_commit_files({ "A" => "AAAx\nAA\nA\nAAAAA\nA\nA\n" }, "c3\n\nco-authored-by: Eh Eh <a@github.com>\nco-authored-by: Eff Eff <f@github.com>", parents: [oid], email: "g@github.com", time: time)

    expected_metrics = [
      { additions: 2, deletions: 0, commits: 1, date: "2012-11-18", author: "g@github.com", co_authors: ["a@github.com", "f@github.com"] },
      { additions: 5, deletions: 1, commits: 2, date: "2012-11-18", author: "a@github.com", co_authors: ["f@github.com", "g@github.com"] }
    ]

    data = @client.gh_graph_data_by_day_and_author(to_oid: oid)
    assert_equal expected_metrics, data
  end

  # tests the following scenario:
  #
  # b---c---from
  #  \--c---to
  def test_gh_graph_data_by_day_and_author_with_diverging_endpoints
    base_oid = @fixture.rugged_commit_files({ "A" => "AAA\nAA\nA" }, "c1", email: "a@github.com", time: Time.utc(2012, 11, 18))

    oid = @fixture.rugged_commit_files({ "A" => "zzz" }, "c2", email: "b@github.com", parents: [base_oid], time: Time.utc(2012, 11, 22))
    from_oid = @fixture.rugged_commit_files({ "C" => "CCC", "E" => "EEEE" }, "c3", email: "c@github.com", parents: [oid], time: Time.utc(2013, 3, 30))

    oid = @fixture.rugged_commit_files({ "F" => "FFF" }, "c4", email: "c@github.com", parents: [base_oid], time: Time.utc(2013, 3, 30))
    to_oid = @fixture.rugged_commit_files({ "F" => "FFFgee\nmore data" }, "c5", email: "c@github.com", parents: [oid], time: Time.utc(2013, 3, 30))

    expected_metrics = [
      { additions: -1, deletions: -3, commits: -1, date: "2012-11-22", author: "b@github.com" },
      { additions: 1, deletions: 1, commits: 1, date: "2013-03-30", author: "c@github.com" }
    ]

    data = @client.gh_graph_data_by_day_and_author(to_oid: to_oid, from_oid: from_oid)
    assert_equal expected_metrics, data
  end

  def test_gh_graph_data_by_day_and_author_with_missing_commits
    oid = @fixture.rugged_commit_files({ "A" => "AAA\nAA\nA" }, "c1", email: "a@github.com", time: Time.utc(2012, 11, 18))
    begin
      @client.gh_graph_data_by_day_and_author(to_oid: "f" * 40, from_oid: oid)
      fail
    rescue GitRPC::ObjectMissing => e
      assert_equal "f" * 40, e.oid
    end

    begin
      @client.gh_graph_data_by_day_and_author(to_oid: oid, from_oid: "a" * 40)
      fail
    rescue GitRPC::ObjectMissing => e
      assert_equal "a" * 40, e.oid
    end
  end

  def test_gh_graph_data_by_day_and_author_with_different_roots
    oid = @fixture.rugged_commit_files({ "A" => "Amazing" }, "c1", email: "a@github.com", time: Time.utc(2012, 11, 18))
    from_oid = @fixture.rugged_commit_files({ "B" => "Beautiful" }, "c2", email: "a@github.com", parents: [oid], time: Time.utc(2012, 11, 19))

    oid = @fixture.rugged_commit_files({ "C" => "Cheerful" }, "c3", parents: [], email: "a@github.com", time: Time.utc(2012, 11, 18))
    to_oid = @fixture.rugged_commit_files({ "D" => "Delightful\nDogs\nDancing" }, "c4", email: "a@github.com", parents: [oid], time: Time.utc(2012, 11, 19))

    data = @client.gh_graph_data_by_day_and_author(to_oid: to_oid, from_oid: from_oid)

    expected_metrics = [
      { additions: 0, deletions: 0, commits: 0, date: "2012-11-18", author: "a@github.com" },
      { additions: 2, deletions: 0, commits: 0, date: "2012-11-19", author: "a@github.com" }
    ]

    assert_equal expected_metrics, data
  end
end
