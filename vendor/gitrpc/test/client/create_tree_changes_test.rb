# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class ClientCommitTreeChangesTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    setup_test_files

    @client = GitRPC.new("fakerpc:#{@fixture.path}")
    @committer = {
      "name"  => "Some Guy",
      "email" => "guy@example.com",
      "time"  => "2000-01-01T00:00:00Z"
    }
    @rugged = Rugged::Repository.new(@fixture.path)
    setup_test_tree
  end

  def setup_test_files
    @fixture.commit_files({
      "README"          => "stuff",
      "user.rb"         => "def stuff; end",
      "subtree/file.js" => "function stuff() {}"
    }, "add first files")

    @HEAD = @fixture.rev_parse("HEAD")
  end

  def setup_test_tree
    index = Rugged::Index.new
    oid = @rugged.write("new stuff", "blob")
    index.add({
      :path => "README",
      :oid  => oid,
      :mode => 0100644,
    })

    @tree_oid = index.write_tree(@rugged)
  end

  def teardown
    @fixture.teardown
  end

  def test_create_tree_changes
    files = { "README" => "hiya" }
    info  = { "message" => "test commit\n", "committer" => @committer }
    oid = @client.create_tree_changes(parent=nil, info, files)
    assert_equal "c953f091f2e49200cd212c8fb5f40474e9ec62d4", oid

    commit = @rugged.lookup(oid)
    assert_equal "test commit\n", commit.message
    assert_equal "Some Guy", commit.author[:name]
    assert_equal "guy@example.com", commit.author[:email]
    assert_equal Time.iso8601("2000-01-01T00:00:00Z"), commit.author[:time]

    assert_equal 1, commit.tree.count
    assert_equal "README", commit.tree[0][:name]
    assert_equal "hiya",   @rugged.lookup(commit.tree[0][:oid]).content
  end

  def test_create_tree_changes_with_data_hash
    files = {
      "README" => "hiya",
      "test_one" => { "data" => "stuff", "mode" => 0100755 },
      "test_two.js" => { "source" => "subtree/file.js" }
    }
    info  = { "message" => "test commit\n", "committer" => @committer }
    oid = @client.create_tree_changes(parent=@HEAD, info, files)

    commit = @rugged.lookup(oid)
    assert_equal "test commit\n", commit.message
    assert_equal "Some Guy", commit.author[:name]
    assert_equal "guy@example.com", commit.author[:email]
    assert_equal Time.iso8601("2000-01-01T00:00:00Z"), commit.author[:time]

    assert_equal 4, commit.tree.count
    assert_equal "README", commit.tree[0][:name]
    assert_equal "hiya",   @rugged.lookup(commit.tree[0][:oid]).content
    assert_equal "test_one", commit.tree[1][:name]
    assert_equal "stuff",    @rugged.lookup(commit.tree[1][:oid]).content
    assert_equal 0100755,    commit.tree[1][:filemode]
    assert_equal "test_two.js",         commit.tree[2][:name]
    assert_equal "function stuff() {}", @rugged.lookup(commit.tree[2][:oid]).content
    assert_raises(Rugged::TreeError)  { commit.tree.path("subtree/file.js") }
  end

  def test_create_tree_changes_with_tree
    info  = {
      "message"   => "test commit\n",
      "committer" => @committer,
      "tree"      => @tree_oid,
    }

    oid = @client.create_tree_changes(parent=@HEAD, info)
    assert oid

    commit = @rugged.lookup(oid)
    assert_equal "test commit\n", commit.message
    assert_equal 1, commit.tree.count
    assert_equal "README", commit.tree[0][:name]
    assert_equal "new stuff", @rugged.lookup(commit.tree[0][:oid]).content
  end

  def test_create_tree_changes_with_signature
    files = { "README" => "hiya" }
    info  = { "message" => "test commit\n", "committer" => @committer }
    sig   = "fake-signature"

    oid = @client.create_tree_changes(parent=nil, info, files) do |raw|
      assert raw.is_a?(String)
      sig
    end

    assert_equal "ceff8cb8cfbe65c1c3d54b22606906b0ebf9fb6f", oid

    commit = @rugged.lookup(oid)
    assert_equal sig, commit.header_field("gpgsig")
    assert_equal "test commit\n", commit.message
    assert_equal "Some Guy", commit.author[:name]
    assert_equal "guy@example.com", commit.author[:email]
    assert_equal Time.iso8601("2000-01-01T00:00:00Z"), commit.author[:time]

    assert_equal 1, commit.tree.count
    assert_equal "README", commit.tree[0][:name]
    assert_equal "hiya",   @rugged.lookup(commit.tree[0][:oid]).content
  end

  def test_create_tree_changes_with_signature_arg
    files = { "README" => "hiya" }
    info  = { "message" => "test commit\n", "committer" => @committer }
    sig   = "fake-signature"

    oid = @client.create_tree_changes(parent=nil, info, files, false, sig)
    assert_equal "ceff8cb8cfbe65c1c3d54b22606906b0ebf9fb6f", oid

    commit = @rugged.lookup(oid)
    assert_equal sig, commit.header_field("gpgsig")
    assert_equal "test commit\n", commit.message
    assert_equal "Some Guy", commit.author[:name]
    assert_equal "guy@example.com", commit.author[:email]
    assert_equal Time.iso8601("2000-01-01T00:00:00Z"), commit.author[:time]

    assert_equal 1, commit.tree.count
    assert_equal "README", commit.tree[0][:name]
    assert_equal "hiya",   @rugged.lookup(commit.tree[0][:oid]).content
  end

  def test_create_tree_changes_with_nil_signature
    files = { "README" => "hiya" }
    info  = { "message" => "test commit\n", "committer" => @committer }

    oid = @client.create_tree_changes(parent=nil, info, files) { |r| nil }

    commit = @rugged.lookup(oid)
    refute commit.header_field?("gpgsig")
    assert_equal "test commit\n", commit.message
    assert_equal "Some Guy", commit.author[:name]
    assert_equal "guy@example.com", commit.author[:email]
    assert_equal Time.iso8601("2000-01-01T00:00:00Z"), commit.author[:time]

    assert_equal 1, commit.tree.count
    assert_equal "README", commit.tree[0][:name]
    assert_equal "hiya",   @rugged.lookup(commit.tree[0][:oid]).content
  end

  def test_create_tree_changes_with_signature_error_handling
    files = { "README" => "hiya" }
    info  = { "message" => "test commit\n", "committer" => @committer }
    sig   = "fake-signature"
    error = Class.new(Exception)

    assert_raises(error) do
      @client.create_tree_changes(@HEAD, info, files) do |raw|
        raise error
      end
    end

    # no error when retrying without signature.
    @client.create_tree_changes(@HEAD, info, files)
  end

  def test_create_tree_changes_evil_gitmodules
    evil_gitmodules = <<-GITMODULES
    [submodule "../../something/evil.git"]
      path = totally-innocent
      url = foo
    GITMODULES
    files = { ".gitmodules" => evil_gitmodules}
    info  = { "message" => "test commit\n", "committer" => @committer }

    assert_raises(GitRPC::BadGitmodules) do
      @client.create_tree_changes(@HEAD, info, files, false, nil)
    end
  end

  def test_create_tree_changes_injection_via_url
    evil_gitmodules = <<-GITMODULES
    [submodule "something"]
      path = totally-innocent
      URL = -u./payload
    GITMODULES
    files = { ".gitmodules" => evil_gitmodules}
    info  = { "message" => "test commit\n", "committer" => @committer }

    assert_raises(GitRPC::BadGitmodules) do
      @client.create_tree_changes(@HEAD, info, files, false, nil)
    end

    evil_gitmodules = <<-GITMODULES
    [submodule "something"] URL = -u./payload
    GITMODULES
    files = { ".gitmodules" => evil_gitmodules}
    info  = { "message" => "test commit\n", "committer" => @committer }

    assert_raises(GitRPC::BadGitmodules) do
      @client.create_tree_changes(@HEAD, info, files, false, nil)
    end
  end

  def test_create_tree_changes_injection_via_path
    evil_gitmodules = <<-GITMODULES
    [submodule "evil"]
      Path = -what
      url = https://example.com/innocent.git
    GITMODULES
    files = { ".gitmodules" => evil_gitmodules}
    info  = { "message" => "test commit\n", "committer" => @committer }

    assert_raises(GitRPC::BadGitmodules) do
      @client.create_tree_changes(@HEAD, info, files, false, nil)
    end

    evil_gitmodules = <<-GITMODULES
    [submodule "something"] PaTh = -what
    GITMODULES
    files = { ".gitmodules" => evil_gitmodules}
    info  = { "message" => "test commit\n", "committer" => @committer }

    assert_raises(GitRPC::BadGitmodules) do
      @client.create_tree_changes(@HEAD, info, files, false, nil)
    end
  end

  def test_create_tree_changes_injectio_via_url_credential_helper
    evil_gitmodules = <<-GITMODULES
    [submodule "something"]
      path = totally-innocent
      URL = https://example.com/evil?%0ahost=github.com
    GITMODULES
    files = { ".gitmodules" => evil_gitmodules}
    info  = { "message" => "test commit\n", "committer" => @committer }

    assert_raises(GitRPC::BadGitmodules) do
      @client.create_tree_changes(@HEAD, info, files, false, nil)
    end

    evil_gitmodules = <<-GITMODULES
    [submodule "something"] URL = https://example.com/evil?%0ahost=github.com
    GITMODULES
    files = { ".gitmodules" => evil_gitmodules}
    info  = { "message" => "test commit\n", "committer" => @committer }

    assert_raises(GitRPC::BadGitmodules) do
      @client.create_tree_changes(@HEAD, info, files, false, nil)
    end
  end

  def test_create_tree_changes_rejects_empty_host_in_gitmodules
    gitmodules = <<-GITMODULES
    [submodule "something"]
      path = example
      URL = "https:///one.example.com/foo.git"
    GITMODULES
    files = { ".gitmodules" => gitmodules}
    info  = { "message" => "test commit\n", "committer" => @committer }

    err = assert_raises(GitRPC::BadGitmodules) do
      @client.create_tree_changes(@HEAD, info, files, false, nil)
    end
    assert_equal ".gitmodules contains a URL without a host", err.message
  end

  def test_create_tree_changes_rejects_missing_url_scheme
    gitmodules = <<-GITMODULES
    [submodule "something"]
      path = example
      URL = http::one.example.com/foo.git
    GITMODULES
    files = { ".gitmodules" => gitmodules}
    info  = { "message" => "test commit\n", "committer" => @committer }

    err = assert_raises(GitRPC::BadGitmodules) do
      @client.create_tree_changes(@HEAD, info, files, false, nil)
    end
    assert_equal ".gitmodules contains a URL without a protocol", err.message
  end

  def test_create_tree_changes_rejects_proto_relative_urls_in_gitmodules
    gitmodules = <<-GITMODULES
    [submodule "something"]
      path = example
      URL = http::://one.example.com/foo.git
    GITMODULES
    files = { ".gitmodules" => gitmodules}
    info  = { "message" => "test commit\n", "committer" => @committer }

    err = assert_raises(GitRPC::BadGitmodules) do
      @client.create_tree_changes(@HEAD, info, files, false, nil)
    end
    assert_equal ".gitmodules contains a protocol-relative URL", err.message
  end

  def test_create_tree_changes_rejects_relative_url_with_blank_component_in_gitmodules
    gitmodules = <<-GITMODULES
    [submodule "something"]
      path = example
      URL = ../../..//other-host/repo.git
    GITMODULES
    files = { ".gitmodules" => gitmodules}
    info  = { "message" => "test commit\n", "committer" => @committer }

    err = assert_raises(GitRPC::BadGitmodules) do
      @client.create_tree_changes(@HEAD, info, files, false, nil)
    end
    assert_equal ".gitmodules contains potential empty hostname", err.message
  end

  def test_create_tree_changes_rejects_relative_url_with_scheme_rewrite_in_gitmodules
    gitmodules = <<-GITMODULES
    [submodule "something"]
      path = example
      URL =  ../../../../../:localhost:8088/foo.git
    GITMODULES
    files = { ".gitmodules" => gitmodules}
    info  = { "message" => "test commit\n", "committer" => @committer }

    err = assert_raises(GitRPC::BadGitmodules) do
      @client.create_tree_changes(@HEAD, info, files, false, nil)
    end
    assert_equal ".gitmodules contains potential scheme rewrite", err.message
  end

  def test_create_tree_changes_rejects_embedded_newline_relative_url_in_gitmodules
    gitmodules = <<-GITMODULES
    [submodule "foo"]
      path = example
      url = "./%0ahost=two.example.com/foo.git"
    GITMODULES
    files = { ".gitmodules" => gitmodules}
    info  = { "message" => "test commit\n", "committer" => @committer }

    err = assert_raises(GitRPC::BadGitmodules) do
      @client.create_tree_changes(@HEAD, info, files, false, nil)
    end
    assert_equal ".gitmodules contains a URL with newlines", err.message
  end

  def test_create_tree_changes_rejects_relative_url_missing_scheme
    gitmodules = <<-'GITMODULES'
    [submodule "foo"]
      path = example
      url = "..\\../.\\../:one.example.com/foo.git"
    GITMODULES
    files = { ".gitmodules" => gitmodules}
    info  = { "message" => "test commit\n", "committer" => @committer }

    err = assert_raises(GitRPC::BadGitmodules) do
      @client.create_tree_changes(@HEAD, info, files, false, nil)
    end
    assert_equal ".gitmodules contains potential scheme rewrite", err.message
  end
end
