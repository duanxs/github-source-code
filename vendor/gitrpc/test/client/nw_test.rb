# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"
require "gitrpc/util/hashcache"

require "minitest/mock"

class ClientNwTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => @cache)

    setup_test_commits
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @oid1 = @fixture.commit_files({"README" => "Hello."}, "commit 1")
  end

  def test_nw_rm
    ret = @client.nw_rm
    assert_equal true, ret

    assert_raises GitRPC::InvalidRepository do
      allrefs = @client.list_refs
    end
  end

  def test_nw_link
    refute @client.nw_linked?

    # 'git nw-sync' is a no-op when run in an isolated repository.
    assert_nil @client.nw_sync

    assert_nil @client.nw_link
    assert @client.nw_linked?

    assert_nil @client.nw_sync

    assert_nil @client.nw_unlink
    refute @client.nw_linked?

    @fixture.command("rm -rf ../network.git")
  end

  def test_nw_gc
    res = @client.nw_gc
    assert res["ok"]
    assert_equal "", res["out"]
    assert res["err"] =~ /Running git-repack/
    assert res["err"] =~ /Repository GC completed/
  end

  def test_nw_repack
    assert_nil @client.nw_repack
  end

  def test_last_fsck
    # last_fsck won't work with never: true if it's never been run before.
    res = @client.last_fsck(never: true)
    refute res["ok"]

    res = @client.last_fsck
    assert res["ok"]
    res2 = @client.last_fsck(never: true)
    assert res2["ok"]

    @fixture.commit_files({"README" => "Hello again."}, "commit 2")

    # New commit won't be reflected in last_fsck output until we force.
    res2 = @client.last_fsck
    assert res2["ok"]
    assert_equal res["out"], res2["out"]

    res3 = @client.last_fsck(force: true)
    assert res3["ok"]
    refute_equal res["out"], res3["out"]
  end

  def test_janitor
    res = @client.janitor
    refute res["ok"]
    assert res["out"] =~ /Cannot determine the repository type from its path/

    e = assert_raises GitRPC::Error do
      res = @client.janitor(type: "asdf")
    end
    assert e.to_s =~ /invalid type asdf/

    res = @client.janitor(type: "fork")
    refute res["ok"]
    assert res["out"] =~ /Should be a symlink but is a directory: hooks/
  end
end
