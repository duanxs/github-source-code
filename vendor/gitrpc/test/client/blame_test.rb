# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"
require "securerandom"

require "minitest/mock"

class ClientBlameTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => @cache)

    setup_test_commits
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @past_time = Time.local(2000).iso8601
    @oid1 = @fixture.commit_files({"README" => "Bonjour."}, "commit 1", :commit_time => @past_time)
    @oid2 = @fixture.commit_files({"README" => "Hello."}, "commit 2", :commit_time => @past_time)
  end

  def test_blame
    out = @client.blame(@oid2, "README")
    assert_match /#{@oid2}/, out

    out = @client.blame(@oid1, "README")
    assert_match /#{@oid1}/, out
    refute_match /#{@oid2}/, out

    out = @client.blame(@oid2, "README", annotate: [1])
    assert_match /#{@oid2}/, out

    e = assert_raises GitRPC::NoSuchPath do
      out = @client.blame(@oid2, "nonexistent")
    end
    assert_equal "nonexistent", e.to_s

    assert_raises GitRPC::BadLineRange do
      out = @client.blame(@oid2, "README", annotate: [99])
    end
  end
end
