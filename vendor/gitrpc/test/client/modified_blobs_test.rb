# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"
require "securerandom"

class ClientModifiedBlobsTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    @submodule_fixture = RepositoryFixture.new("submodule.git")
    @submodule_fixture.setup

    setup_test_commits

    @cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => @cache)
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @commit1 = @fixture.commit_files({"README" => "Hello."}, "commit 1")
    @commit2 = @fixture.commit_files({"README" => "Goodbye."}, "commit 2")
    @commit3 = @fixture.commit_files({"README" => "Hi again."}, "commit 3")

    work_tree = "#{@fixture.path}/work"
    @fixture.command "git --work-tree='#{work_tree}' mv README README.md"
    @fixture.commit_changes "rename"
    @rename = @fixture.rev_parse("HEAD")

    @submodule_fixture.commit_files({"README" => "This is a submodule."}, "submodule commit 1")
    @commit4 = @fixture.add_submodule_commit(@submodule_fixture.path, "submod", "Add a submodule")
  end

  def test_modified_blobs
    modifications = @client.modified_blobs(@commit2, @commit1)
    modified = modifications["modified"]

    assert_equal 1, modified.size
    assert_equal "README", modified.first["path"]
    assert_equal "Hello.", modified.first["data"]
    assert_equal 0, modifications["deleted"].size
  end

  def test_raises_obj_missing_on_invalid_obj
    assert_raises GitRPC::ObjectMissing do
      @client.modified_blobs(SecureRandom.hex(20), SecureRandom.hex(20))
    end
  end

  def test_raise_invalid_full_oid
    assert_raises GitRPC::InvalidFullOid do
      @client.modified_blobs(nil, nil)
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.modified_blobs(nil, "lol")
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.modified_blobs("lol", nil)
    end

    assert_raises GitRPC::InvalidFullOid do
      @client.modified_blobs("lol", "lol")
    end
  end

  def test_added_submodule
    modifications = @client.modified_blobs(@rename, @commit4)
    modified = modifications["modified"]

    assert_equal 2, modified.size
    assert_equal ".gitmodules", modified[0]["path"]
    assert modified[0].key?("oid")
    assert_equal "submod", modified[1]["path"]
    refute modified[1].key?("oid")
  end
end
