# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class ParticipationStatsTest < Minitest::Test
  def setup
    @client = RepositoryFixture.sample("defunkt_facebox")
  end

  def facebox_start_date
    # Facebox commits happen in the first half of 2008,
    # from Feb to July.
    Time.new(2008, 8, 1)
  end

  def defunkt_commit_count
    21
  end

  def facebox_commit_count
    29
  end

  def test_output_is_well_formed
    stats = @client.participation_stats("master", [], Time.now)

    assert stats.is_a?(Hash)
    assert stats.include?("all")
    assert stats.include?("owner")

    assert stats["all"].is_a?(Array)
    assert stats["owner"].is_a?(Array)

    assert_equal GitRPC::Client::CONTRIBUTION_PERIOD, stats["all"].size
    assert_equal GitRPC::Client::CONTRIBUTION_PERIOD, stats["owner"].size
  end

  def test_output_is_shorter_for_shorter_periods
    stats = @client.participation_stats("master", [], Time.now, 24)
    assert_equal 24, stats["all"].size
    assert_equal 24, stats["owner"].size
  end

  def test_output_exists_for_invalid_branches
    stats = @client.participation_stats("no-branch-lol", [], Time.now)
    assert stats["all"].is_a?(Array)
    assert stats["owner"].is_a?(Array)
  end

  def test_stats_without_owner
    stats = @client.participation_stats("master", [], facebox_start_date)

    assert_equal facebox_commit_count, stats["all"].inject(:+)
    assert_equal 0, stats["owner"].inject(:+)
  end

  def test_stats_with_owner
    stats = @client.participation_stats("master", ["chris@ozmm.org"], facebox_start_date)

    assert_equal facebox_commit_count, stats["all"].inject(:+)
    assert_equal defunkt_commit_count, stats["owner"].inject(:+)
  end

  def test_stats_with_owner_uppercase
    stats = @client.participation_stats("master", ["Chris@ozmm.org"], facebox_start_date)
    assert_equal defunkt_commit_count, stats["owner"].inject(:+)
  end
end
