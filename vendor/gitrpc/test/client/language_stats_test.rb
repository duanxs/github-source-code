# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class LanguageStatsTest < Minitest::Test
  def setup
    @client = RepositoryFixture.sample("defunkt_facebox")
    @head = @client.read_head_oid
    @cache_file = File.expand_path("../../examples/defunkt_facebox.git/language-stats.cache", __FILE__)
  end

  def teardown
    # Remove stats cache
    FileUtils.rm_f(@cache_file)
  end

  def test_language_stats
    stats = @client.language_stats(@head)
    assert_equal({"Shell"=>313, "CSS"=>3030, "JavaScript"=>9478, "HTML"=>9997},
                 stats)
  end

  def test_language_stats_caches
    refute File.exist?(@cache_file), "Language stats file doesn't exist before the call"
    @client.language_stats(@head)
    assert File.file?(@cache_file), "Language stats file exists"
  end

  def test_clear_language_cache
    refute File.exist?(@cache_file), "Language stats file doesn't exist before the call"
    @client.language_stats(@head)
    assert File.file?(@cache_file), "Language stats file exists"
    @client.clear_language_cache
    refute File.exist?(@cache_file), "clear_language_cache removes the cache file"
  end

  def test_language_breakdown_by_file
    stats = @client.language_breakdown_by_file(@head)
    assert_equal({"Shell"=>["build_tar.sh"],
                  "CSS"=>["facebox.css", "faceplant.css"],
                  "JavaScript"=>["facebox.js"],
                  "HTML"=>["index.html", "remote.html", "test.html"]},
                 stats)
  end

  def test_language_breakdown_by_file_caches
    refute File.exist?(@cache_file), "Language stats file doesn't exist before the call"
    @client.language_breakdown_by_file(@head)
    assert File.file?(@cache_file), "Language stats file exists"
  end
end
