# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"
require "gitrpc/diff/delta"

class ClientReadDiffTocTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @commit1 = @fixture.commit_files({"README" => "Hello. This is the majority of the content of the file.\n"}, "commit 1")
    @commit2 = @fixture.commit_files({"README2" => "Hello. This is the majority of the content of the file.\nNew!\n", "README" => nil}, "commit 2")
    @commit3 = @fixture.commit_files({"README2" => "Hi there.\n"}, "commit 3")

    cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => cache)

    @committer = {
      "name"  => "Some Guy",
      "email" => "guy@example.com",
      "time"  => "2000-01-01T00:00:00Z"
    }
    @count = 0
    @rugged = Rugged::Repository.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
  end

  OLD_README = {
    oid: "3516421fa2ed05057777f5003403109de819f5b9",
    mode: "100644",
    path: "README"
  }

  EXPECTED_C1_ENTRY = GitRPC::Diff::Delta.new(
    old_file: {
      oid:  "0000000000000000000000000000000000000000",
      mode: "000000",
      path: "README"
    },
    new_file: OLD_README,
    similarity: 0,
    status:     "A"
  )

  EXPECTED_C1_C2_ENTRY = GitRPC::Diff::Delta.new(
    old_file: OLD_README,
    new_file: {
      oid: "6e85a66311daf5c4e044bb50e245ec749edacade",
      mode: "100644",
      path: "README2"
    },
    similarity: 91,
    status:     "R"
  )

  EXPECTED_C2_C3_ENTRY = GitRPC::Diff::Delta.new(
    old_file: {
      oid: "6e85a66311daf5c4e044bb50e245ec749edacade",
      mode: "100644",
      path: "README2"
    },
    new_file: {
      oid:  "ee4efdbe3a22bd650b364e6fc047d79238ebc129",
      mode: "100644",
      path: "README2"
    },
    similarity: 0,
    status:     "M"
  )

  EXPECTED_C1_C3 = [
    GitRPC::Diff::Delta.new(
      old_file: OLD_README,
      new_file: {
        oid:  "0000000000000000000000000000000000000000",
        mode: "000000",
        path: "README"
      },
      similarity: 0,
      status:     "D"
    ),
    GitRPC::Diff::Delta.new(
      old_file: {
        oid:  "0000000000000000000000000000000000000000",
        mode: "000000",
        path: "README2"
      },
      new_file: {
        oid:  "ee4efdbe3a22bd650b364e6fc047d79238ebc129",
        mode: "100644",
        path: "README2"
      },
      similarity: 0,
      status:     "A"
    )
  ]

  def test_native_toc
    results = @client.native_read_diff_toc(@commit1, @commit2)

    assert_equal [EXPECTED_C1_C2_ENTRY], results

    results = @client.native_read_diff_toc(@commit1, @commit3)

    assert_equal EXPECTED_C1_C3, results
  end

  def test_tree_diff
    c = @rugged.lookup(@commit1)

    results = @client.native_read_diff_toc(c.tree.oid, @commit2)

    assert_equal [EXPECTED_C1_C2_ENTRY], results
  end

  def test_nested_file
    commit4 = @fixture.commit_files({"subdirectory/nested.txt" => "this file is nested\n"}, "commit 4")
    commit5 = @fixture.commit_files({"subdirectory/nested2.txt" => "this file is also nested\n"}, "commit 5")

    results = @client.native_read_diff_toc(commit4, commit5)

    expected = GitRPC::Diff::Delta.new(
      old_file: {
        oid:  "0000000000000000000000000000000000000000",
        mode: "000000",
        path: "subdirectory/nested2.txt"
      },
      new_file: {
        oid:  "88062f166a013935fdc95cd7b9bfc1213e0a184c",
        mode: "100644",
        path: "subdirectory/nested2.txt"
      },
      similarity: 0,
      status:     "A"
    )

    assert_equal [expected], results
  end

  def test_bad_commit_oids
    assert_raises(GitRPC::InvalidFullOid) { @client.native_read_diff_toc("DEADBEEF", "DEADBEEF2") }
  end

  def test_adjusted_base_sha
    c1 = create_commit parent: nil, files: {
      "README" => "Numbers\n\n" + (1..100).to_a.join("\n")
    }

    c2 = create_commit parent: c1, files: {
      "README" => nil,
      "README.md" => "# Numbers\n\n" + (1..100).to_a.join("\n")
    }

    c3 = create_commit parent: c1, files: {
      "README" => "Numbers\n\n" + (1..101).to_a.join("\n")
    }

    c4 = create_commit parent: [c3, c2], files: {
      "README" => nil,
      "README.md" => "# Numbers\n\n" + (1..101).to_a.join("\n")
    }

    c5 = create_commit parent: c4, files: {
      "README.md" => "# Numbers 1 to 101\n\n" + (1..101).to_a.join("\n")
    }

    expected = GitRPC::Diff::Delta.new(
      old_file: {
        oid:  "4ffe72797cde3d448c74e3d6f8fa32ed28686daf",
        mode: "100644",
        path: "README.md"
      },
      new_file: {
        oid:  "13ae0cc6c3ff363b190676cf6e1f76dab1d8e163",
        mode: "100644",
        path: "README.md"
      },
      similarity: 0,
      status:     "M"
    )

    # this will result in a tree object being created and that being used as the start_oid for the diff
    results = @client.native_read_diff_toc(c2, c5, c3)

    assert_equal [expected], results
  end

  def test_timeout
    assert_raises(GitRPC::Timeout) { @client.native_read_diff_toc(@commit1, @commit3, nil, "timeout" => 0.0000000001) }

    # still raises even with a large timeout since it is cached
    assert_raises(GitRPC::Timeout) { @client.native_read_diff_toc(@commit1, @commit3) }

    # should not have an error
    @client.native_read_diff_toc(@commit1, @commit2, nil, "timeout" => 60)
  end

  def test_raises_immediately_on_0_timeout
    @client.backend.stub(:send_message, lambda { |*args| fail "should not be called" }) do
      assert_raises(GitRPC::Timeout) { @client.native_read_diff_toc(@commit1, @commit3, nil, "timeout" => 0) }
    end
  end

  def test_newlines_in_path
    c1 = create_commit parent: nil, files: {
      "name\nwith\nnewlines" => "FooBar"
    }

    deltas = @client.native_read_diff_toc(nil, c1, nil)

    assert_equal 1, deltas.length

    assert_equal GitRPC::Diff::Delta.new(
      status: "A",
      similarity: 0,
      old_file: {
        oid: "0000000000000000000000000000000000000000",
        mode: "000000",
        path: "name\nwith\nnewlines"
      },
      new_file: {
        oid: "61e2c47c2586f58135995994fd793da0d4627776",
        mode: "100644",
        path: "name\nwith\nnewlines"
      }
    ), deltas[0]
  end

  def test_empty_tree
    deltas = @client.native_read_diff_toc(GitRPC::EMPTY_TREE_OID, @commit3, nil)
    assert deltas.count == 1
    delta = deltas.first
    assert_predicate delta, :added?
  end

  def test_typechange_split_into_deletion_and_addition
    c1 = create_commit parent: nil, files: {
      "foo.txt" => "FooBar"
    }

    c2 = create_commit parent: nil, files: {
      "foo.txt" => {
        "data" => "bar.txt",
        "mode" => 0120000
      }
    }

    deltas = @client.native_read_diff_toc(c1, c2, nil)

    assert_equal 2, deltas.length

    assert_equal GitRPC::Diff::Delta.new(
      status: "D",
      similarity: 0,
      old_file: {
        oid: "61e2c47c2586f58135995994fd793da0d4627776",
        mode: "100644",
        path: "foo.txt"
      },
      new_file: {
        oid: "0000000000000000000000000000000000000000",
        mode: "000000",
        path: "foo.txt"
      }
    ), deltas[0]

    assert_equal GitRPC::Diff::Delta.new(
      status: "A",
      similarity: 0,
      old_file: {
        oid: "0000000000000000000000000000000000000000",
        mode: "000000",
        path: "foo.txt"
      },
      new_file: {
        oid: "5f34b0af07646aa529b5b005cde3a9559e606210",
        mode: "120000",
        path: "foo.txt"
      }
    ), deltas[1]
  end

  def test_cached_values_frozen
    results = @client.native_read_diff_toc(@commit1, @commit2)
    assert results.all?(&:frozen?)
    results = @client.native_read_diff_toc(@commit1, @commit2)
    assert results.all?(&:frozen?)
  end

  def create_commit(parent:, files: {})
    @count += 1
    @client.create_tree_changes(parent, {
      "message"   => "C#{@count}",
      "committer" => @committer
    }, files)
  end

  def test_multiple_commits
    results = @client.native_read_diff_toc_multi([@commit1, @commit2, @commit3])

    expected = {}
    expected[@commit1] = [EXPECTED_C1_ENTRY]
    expected[@commit2] = [EXPECTED_C1_C2_ENTRY]
    expected[@commit3] = [EXPECTED_C2_C3_ENTRY]

    assert_equal expected, results
  end

  def test_multiple_commits_out_of_order
    results = @client.native_read_diff_toc_multi([@commit3, @commit1, @commit2])

    expected = {}
    expected[@commit1] = [EXPECTED_C1_ENTRY]
    expected[@commit2] = [EXPECTED_C1_C2_ENTRY]
    expected[@commit3] = [EXPECTED_C2_C3_ENTRY]

    assert_equal expected, results
  end

  def test_single_commit_with_multi_api
    results = @client.native_read_diff_toc_multi([@commit2])

    expected = {}
    expected[@commit2] = [EXPECTED_C1_C2_ENTRY]

    assert_equal expected, results
  end

  def test_single_commit_with_multi_api_from_cache
    @client.native_read_diff_toc(nil, @commit2, nil)
    @client.backend.stub(:send_message, lambda { |*args| fail "shouldnt be called, should've used cache" }) do
      results = @client.native_read_diff_toc_multi([@commit2])
      expected = {}
      expected[@commit2] = [EXPECTED_C1_C2_ENTRY]
      assert_equal expected, results
    end
  end

  def test_multi_api_throws_timeout_exception
    # Throws when timeout value is invalid
    assert_raises(GitRPC::Timeout) { @client.native_read_diff_toc_multi([@commit2], {"timeout" => 0}) }

    # Throws when impossibly low timeout value is passed
    assert_raises(GitRPC::Timeout) { @client.native_read_diff_toc(nil, @commit2, nil, "timeout" => 0.0000000001) }

    # Throws from cached value. The whole batch operation should fail if we already timed out on 1 of the specified commits
    assert_raises(GitRPC::Timeout) { @client.native_read_diff_toc_multi([@commit2]) }
  end

  def test_multi_api_bubbles_up_other_exceptions
    @client.backend.stub(:send_message, lambda { |*args| fail "shouldnt be called" }) do
      assert_raises(GitRPC::InvalidFullOid) { @client.native_read_diff_toc_multi(["DEADBEEF"]) }
    end

    @client.backend.stub(:send_message, lambda { |*args| raise GitRPC::ObjectMissing }) do
      assert_raises(GitRPC::ObjectMissing) { @client.native_read_diff_toc_multi([@commit2]) }
    end
  end
end
