# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"
require "gitrpc/util/hashcache"

class FilterableCommitCount < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @author_email = "otherperson@example.com"
    @past_time = Time.local(2000).iso8601
    @future_time = Time.local(Time.now.year + 2).iso8601

    setup_test_files

    @client = GitRPC.new("fakerpc:#{@fixture.path}")
  end

  def setup_test_files
    @fixture.add_empty_commit("commit from the past", :commit_time => @past_time)

    10.times do |i|
      time = Time.local(2001 + i).iso8601
      @fixture.add_empty_commit("commit #{i}", :commit_time => time)
    end

    @fixture.commit_files({"README" => "Hello."}, "commit for a path")
    @fixture.add_empty_commit("commit for author", :email => @author_email)

    @fixture.add_empty_commit("commit from the future", :commit_time => @future_time)

    @HEAD = @fixture.rev_parse("HEAD")
  end

  def teardown
    @fixture.teardown
  end

  def test_returns_count_with_no_opts
    assert_equal 14, @client.filterable_commit_count(starting_oid: @HEAD)
  end

  def test_returns_count_when_filtered_by_author
    assert_equal 1, @client.filterable_commit_count(starting_oid: @HEAD, author_emails: [@author_email])
  end

  def test_returns_count_when_filtered_by_multiple_author_emails
    emails = (0..3).map { |i| "#{i}@example.com" }
    oids = emails.map { |email| @fixture.add_empty_commit("commit for author #{email}", :email => email) }
    assert_equal 4, @client.filterable_commit_count(starting_oid: oids.last, author_emails: emails)
  end

  def test_raises_error_when_not_author_emails_not_an_array
    assert_raises(NoMethodError) { @client.filterable_commit_count(starting_oid: @HEAD, author_emails: "hi") }
  end

  def test_returns_count_when_filtered_by_path
    assert_equal 1, @client.filterable_commit_count(starting_oid: @HEAD, path: "README")
  end

  def test_returns_count_when_filtered_by_since
    assert_equal 1, @client.filterable_commit_count(starting_oid: @HEAD, since: Time.now.iso8601)
  end

  def test_returns_count_when_filtered_by_until
    assert_equal 1, @client.filterable_commit_count(starting_oid: @HEAD, until_date: @past_time)
    assert_equal 13, @client.filterable_commit_count(starting_oid: @HEAD, until_date: Time.now.iso8601)
  end

  def test_returns_count_when_filtered_by_multiple_options
    count = @client.filterable_commit_count(
      starting_oid: @HEAD,
      until_date: @future_time,
      since: Time.local(2006).iso8601,
      author_emails: [@author_email]
    )

    assert_equal 1, count
  end

  def test_errors_when_no_starting_id
    assert_raises(ArgumentError) { @client.filterable_commit_count(until_date: Time.now.iso8601) }
  end

  def test_errors_when_starting_id_is_not_sha1
    assert_raises(GitRPC::InvalidFullOid) { @client.filterable_commit_count(starting_oid: "something-not-sha1") }
  end

  def test_caches_with_the_correct_key
    cache = GitRPC::Util::HashCache.new
    client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => cache)

    emails = (0..3).map { |i| "#{i}@example.com" }
    oids = emails.map { |email| @fixture.add_empty_commit("commit for author #{email}", :email => email) }

    assert cache.hash.keys.empty?

    client.filterable_commit_count(starting_oid: @HEAD,
      since: @past_time,
      until_date: @future_time,
      path: "README",
      author_emails: emails.shuffle
    )

    refute cache.hash.keys.empty?

    cache_key = cache.hash.keys.first
    assert_match "filterable_commit_count", cache_key
    assert_match @HEAD, cache_key
    assert_match @past_time, cache_key
    assert_match @future_time, cache_key
    assert_match "README", cache_key
    assert_match "0@example.com,1@example.com,2@example.com,3@example.com", cache_key
  end

  def test_client_caches_the_backend_method_results
    cache = GitRPC::Util::HashCache.new
    client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => cache)

    cached_respoonse = client.filterable_commit_count(starting_oid: @HEAD)

    refute cache.hash.empty?

    client.stub(:send_message, :new_fake_result) do
      result = client.filterable_commit_count(starting_oid: @HEAD)
      refute_equal :new_fake_result, result
      assert_equal cached_respoonse, result
    end
  end
end
