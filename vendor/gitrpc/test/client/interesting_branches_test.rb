# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class InterestingBranchesTest < Minitest::Test
  def setup
    @client = RepositoryFixture.sample("pr_test")
    @head = @client.read_head_oid
  end

  def test_interesting_branches
    assert_equal [[1422470969, "59e856168195ad7c5aa85c8525721ff1ed35fc56", "branch2"],
                  [1422470948, "ea96b3469718077ba310e9663d45f6754fde06fc", "branch1"]],
                 @client.interesting_branches
  end

  def test_intersting_branches_different_default
    assert_equal [[1422470948, "ea96b3469718077ba310e9663d45f6754fde06fc", "branch1"],
                  [1422470896, "5a691b2892b8c506154bb0f8e6b2a3a1f1211bd4", "master"]],
                 @client.interesting_branches("branch2")
  end

  def test_interesting_branches_limit
    assert_equal [[1422470969, "59e856168195ad7c5aa85c8525721ff1ed35fc56", "branch2"]],
                 @client.interesting_branches("master", 1)
  end
end
