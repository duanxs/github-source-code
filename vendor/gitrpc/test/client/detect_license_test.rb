# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class DetectLicenseTest < Minitest::Test
  def test_detect_unlicensed_repository
    repo = RepositoryFixture.sample("defunkt_facebox")
    assert_equal "no-license", repo.detect_license("4bf7a39e8c4ec54f8b4cd594a3616d69004aba69")
  end

  def test_licensed_repository
    repo = RepositoryFixture.sample("defunkt_facebox")
    assert_equal "mit", repo.detect_license("4dddaba2ddd3d9dd88076a90d2d993458b7c3a07")
  end
end
