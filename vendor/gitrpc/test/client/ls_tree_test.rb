# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"
require "gitrpc/util/hashcache"

require "minitest/mock"

class ClientLsTreeTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @committer = {
      "name"  => "Some Guy",
      "email" => "guy@example.com",
      "time"  => "2000-01-01T00:00:00Z"
    }

    @cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => @cache)

    setup_test_commits
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @oid1 = @fixture.commit_files({"README" => "Hello."}, "commit 1")
    @oid2 = @fixture.commit_files({"README" => "Goodbye."}, "commit 2")
    @oid3 = @fixture.commit_files({"README" => "Hi again."}, "commit 3")
    @oid4 = @fixture.commit_files({"AUTHORS" => "Brian"}, "commit 4")
    @oid5 = @fixture.commit_files({"subdir/foo.txt" => "yay"}, "commit 5")
  end

  def test_ls_tree
    a = @client.ls_tree("master")
    e = a["entries"]
    assert_equal 3, e.size
    assert_equal "fcd2b920e56d77d6074fcf2ea46c6cf28e73d1ef", e[0][2]
    assert_equal "AUTHORS", e[0][3]
    assert_equal "efaaad39c1559c95ab7e149b0cea3b6e063444ac", e[1][2]
    assert_equal "README", e[1][3]

    a = @client.ls_tree("master", long: true)
    e = a["entries"]
    assert_equal 3, e.size
    assert_equal "fcd2b920e56d77d6074fcf2ea46c6cf28e73d1ef", e[0][2]
    assert_equal "5", e[0][3]
    assert_equal "AUTHORS", e[0][4]
    assert_equal "efaaad39c1559c95ab7e149b0cea3b6e063444ac", e[1][2]
    assert_equal "9", e[1][3]
    assert_equal "README", e[1][4]

    a = @client.ls_tree("master", recurse: true)
    e = a["entries"]
    assert_equal 3, e.size
    a = @client.ls_tree("master", recurse: true, show_trees: true)
    e = a["entries"]
    assert_equal 4, e.size

    a = @client.ls_tree("master", path: "subdir")
    e = a["entries"]
    assert_equal 1, e.size
    assert_equal "subdir", e[0][3]
    a = @client.ls_tree("master", path: "subdir/")
    e = a["entries"]
    assert_equal 1, e.size
    assert_equal "subdir/foo.txt", e[0][3]
    a = @client.ls_tree("master", path: "subdir/", recurse: true, show_trees: true)
    e = a["entries"]
    assert_equal 2, e.size
    assert_equal "subdir", e[0][3]
    assert_equal "subdir/foo.txt", e[1][3]

    # testing multiple paths
    a = @client.ls_tree("master", path: ["README", "AUTHORS", "subdir/foo.txt"])
    entries = a["entries"]
    assert_equal 3, entries.size
    assert_equal ["100644"]*3, entries.map { |entry| entry[0] }
    assert_equal ["blob"]*3, entries.map { |entry| entry[1] }
    assert_equal ["AUTHORS", "README", "subdir/foo.txt"], entries.map { |entry| entry[3] }

    a = @client.ls_tree("master", recurse: true, byte_limit: 1)
    assert_equal true, a["truncated"]
    a = @client.ls_tree("master", recurse: true, byte_limit: 1000)
    assert_equal false, a["truncated"]
    a = @client.ls_tree("master", recurse: true)
    assert_equal false, a["truncated"]

    assert_raises GitRPC::CommandFailed do
      a = @client.ls_tree("bogus")
    end
  end
end
