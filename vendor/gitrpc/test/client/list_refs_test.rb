# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"
require "gitrpc/util/hashcache"

require "minitest/mock"

class ClientListRefsTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => @cache)

    setup_test_commits
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @past_time = Time.utc(2000).iso8601
    @oid1 = @fixture.commit_files({"README" => "Bonjour."}, "commit 1", :commit_time => @past_time)
    @oid2 = @fixture.commit_files({"README" => "Hello."}, "commit 2", :commit_time => @past_time)
  end

  def test_list_refs
    allrefs = @client.list_refs
    assert_equal ["refs/heads/master"], allrefs

    @client.update_ref("refs/heads/français", @oid1)
    allrefs = @client.list_refs
    assert_equal ["refs/heads/français".b, "refs/heads/master"], allrefs
  end

  def test_raw_branch_names_and_dates
    @client.update_ref("refs/heads/français", @oid1)

    branches = @client.raw_branch_names_and_dates

    assert_equal [["Sat, 1 Jan 2000 00:00:00 +0000", "refs/heads/français".b], ["Sat, 1 Jan 2000 00:00:00 +0000", "refs/heads/master"]], branches
  end
end
