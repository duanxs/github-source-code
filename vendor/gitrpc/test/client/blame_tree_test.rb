# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class BlameTreeTest < Minitest::Test
  def setup
    @client = RepositoryFixture.sample("defunkt_facebox")
    @head = @client.read_head_oid
  end

  def test_blame_tree_works
    assert_equal({"facebox.js"=>"4bf7a39e8c4ec54f8b4cd594a3616d69004aba69",
                   "releases/.gitignore"=>"fc18e486123da27cbf82dbb4b6d1ce462f301655",
                   "facebox.css"=>"e4e7aac29434bf8e038b3e2a9fa3ceb9d9465609",
                   "index.html"=>"e55ccdbcefe5c897946805c77dcfefe212f19d14",
                   "README.txt"=>"2525b57620f58af55465ef049a189d021924c232",
                   ".gitignore"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "b.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "bl.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "br.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "build_tar.sh"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "closelabel.gif"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "faceplant.css"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "jquery.js"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "loading.gif"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "logo.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "remote.html"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "screenshots/.DS_Store"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "screenshots/error.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "screenshots/error_small.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "screenshots/photo.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "screenshots/photo_small.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "screenshots/preview.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "screenshots/preview_small.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "screenshots/success.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "screenshots/success_small.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "shadow.gif"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "stairs.jpg"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "test.html"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "tl.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "tr.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca"},
                 @client.blame_tree(@head))
  end

  def test_blame_tree_subdir
    assert_equal({"releases/.gitignore"=>"fc18e486123da27cbf82dbb4b6d1ce462f301655"},
                 @client.blame_tree(@head, "releases"))
  end

  def test_blame_tree_non_tree_entry
    assert_equal({"README.txt"=>"2525b57620f58af55465ef049a189d021924c232"},
                 @client.blame_tree(@head, "README.txt"))
  end

  def test_blame_tree_non_existant_path
    assert_equal({}, @client.blame_tree(@head, "DOES_NOT_EXIST"))
  end

  def test_blame_tree_not_a_commit
    assert_raises(GitRPC::Error) do
      @client.blame_tree("d4fc2d5e810d9b4bc1ce67702603080e3086a4ed")
    end
  end

  def test_blame_tree_bad_oid
    assert_raises(GitRPC::Error) do
      @client.blame_tree("deadbeefdeadbeefdeadbeefdeadbeefdeadbeef")
    end
  end
end
