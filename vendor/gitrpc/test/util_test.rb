# rubocop:disable Style/FrozenStringLiteralComment
require_relative "setup"

require "gitrpc/util"
require "gitrpc/error"

class UtilTest < Minitest::Test
  include GitRPC::Util

  def test_valid_full_sha1
    %w[
      0123456789abcdef0123456789abcdef01234567
      aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
    ].each do |input|
      assert valid_full_sha1?(input)
    end

    [
      "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
      "azaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
      "azaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa💣",
      "0123456789abcdef",
      "AA💣",
      :aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
      nil
    ].each do |input|
      refute valid_full_sha1?(input)
    end
  end

  def test_ensure_valid_full_sha1
    %w[
      0123456789abcdef0123456789abcdef01234567
      aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
    ].each do |input|
      ensure_valid_full_sha1(input)
    end

    [
      "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
      "azaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
      "0123456789abcdef",
      :aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
      nil
    ].each do |input|
      assert_raises(GitRPC::InvalidFullOid) do
        ensure_valid_full_sha1(input)
      end
    end
  end

  def test_valid_sha1
    %w[
      0123456789abcdef0123456789abcdef01234567
      aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
      0123456789abcdef
    ].each do |input|
      assert valid_sha1?(input)
    end

    [
      "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
      "azaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
      "012345",
      :aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
      :"012345",
      nil
    ].each do |input|
      refute valid_sha1?(input)
    end
  end

  def test_ensure_valid_sha1
    %w[
      0123456789abcdef0123456789abcdef01234567
      aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
    ].each do |input|
      ensure_valid_full_sha1(input)
    end

    [
      "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
      "azaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
      "012345",
      nil
    ].each do |input|
      assert_raises(GitRPC::InvalidOid) do
        ensure_valid_sha1(input)
      end
    end
  end

  def test_valid_refname
    ["HEAD2", "head", "config"].each do |input|
      refute valid_refname?(input), input
    end
  end

  def test_ensure_valid_refname
    ["HEAD2", "head", "config"].each do |input|
      assert_raises(GitRPC::InvalidReferenceName) do
        ensure_valid_refname(input)
      end
    end
  end

  def test_valid_sanitary_argument
    ["-", "-v2.0", "v2..0"].each do |input|
      refute valid_sanitary_argument?(input), input
    end
  end

  def test_ensure_sanitary_argument
    ["-", "-v2.0", "v2..0"].each do |input|
      assert_raises(GitRPC::UnsafeArgument) do
        ensure_sanitary_argument(input)
      end
    end
  end

  def test_iso8601
    full     = iso8601("2011-12-21T02:03:04-05:00").iso8601
    no_colon = iso8601("2011-12-21T02:03:05+0400").iso8601
    short    = iso8601("2011-12-21T02:03:06-03").iso8601
    utc      = iso8601("2011-12-21T02:03:07Z").iso8601

    # In Ruby 1.9+, the UTC offset should be preserved
    if RUBY_VERSION >= "1.9.0"
      assert_equal "2011-12-21T02:03:04-05:00", full
      assert_equal "2011-12-21T02:03:05+04:00", no_colon
      assert_equal "2011-12-21T02:03:06-03:00", short
    end

    # In Ruby 1.8, we lose the UTC offset :(
    if RUBY_VERSION < "1.9.0"
      assert_equal "2011-12-21T07:03:04Z", full
      assert_equal "2011-12-20T22:03:05Z", no_colon
      assert_equal "2011-12-21T05:03:06Z", short
    end

    assert_equal "2011-12-21T02:03:07Z", utc
    assert_raises(ArgumentError) { iso8601("bad") }
  end

  def test_time_to_unixtime_localtime
    time = Time.at(12345)
    value, offset = time_to_unixtime(time)
    assert_equal 12345, value
    assert_equal time.utc_offset, offset
  end

  def test_time_to_unixtime_utc
    time = Time.at(12345).utc
    value, offset = time_to_unixtime(time)
    assert_equal 12345, value
    assert_equal 0, offset
  end

  def test_unixtime_to_time_localtime
    expect = Time.at(12345)
    time = unixtime_to_time([12345, expect.utc_offset])
    assert_equal 12345, time.to_i
    assert_equal expect.utc_offset, time.utc_offset
    assert_equal expect.iso8601, time.iso8601
  end

  def test_unixtime_to_time_utc
    time = unixtime_to_time([12345, 0])
    assert_equal 12345, time.to_i
    assert_equal 0, time.utc_offset
    assert_equal "1970-01-01T03:25:45Z", time.utc.iso8601
  end

  def test_unixtime_to_time_range
    time = unixtime_to_time([-12345678900, 0])
    assert_equal -12345678900, time.to_i
    assert_equal "1578-10-13T04:45:00Z", time.utc.iso8601
  end

  def test_unixtime_to_time_offset_out_of_range
    skip "Zone offsets not supported under MRI < 1.9.2" if RUBY_VERSION < "1.9"

    assert_raises(ArgumentError) { unixtime_to_time([12345,  (24 * 60 * 60)]) }
    assert_raises(ArgumentError) { unixtime_to_time([12345, -(24 * 60 * 60)]) }

    unixtime_to_time([12345,  (24 * 60 * 60) - 1])
    unixtime_to_time([12345, -((24 * 60 * 60) - 1)])
  end

  def test_convert_empty_pathspec
    assert_nil convert_empty_pathspec(nil)
    assert_equal ({foo: "bar"}), convert_empty_pathspec(path:  "", foo: "bar")
  end
end
