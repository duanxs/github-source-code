# frozen_string_literal: true
# Simple ActiveSupport::Notifications#instrument compatible object. Assign an
# instance of this object to GitRPC.instrumenter to record instrument events and
# assert things about what happened.
class FakeInstrumenter
  # Array of [name, payload] tuples
  attr_reader :events

  # Create a new instrumenter.
  def initialize
    @events = []
  end

  # AS::Notifications.publish compatible interface.
  def publish(name, started, ended, id, payload)
    @events << [name, payload]
  end

  # AS::Notifications.instrument compatible interface.
  def instrument(name, payload)
    # This is largely cribed from ActiveSupport::Notifications::Instrumenter
    start = Time.now

    begin
      yield payload
    rescue StandardError => e
        payload[:exception] = [e.class.name, e.message]
        raise e
    ensure
      publish(name, start, Time.now, 0, payload)
    end
  end

  # Test whether a certain event was published at all.
  def published_event?(name)
    events.any? { |key, pay| key == name || key == "#{name}.gitrpc" }
  end

  # Find an event tuple with the given event name.
  def find_event(name)
    events.find { |key, pay| key == name || key == "#{name}.gitrpc" }
  end
end
