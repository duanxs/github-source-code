# frozen_string_literal: true

require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class BackendMirrorTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @commit = @fixture.rugged_commit_files({ "data" => "stuff" }, "commit message", qualified_ref: "refs/namespace/b1")
    @fixture.rugged.references.create("refs/heads/b2", @commit)
    @fixture.rugged.references.create("refs/more-heads/b3", @commit)

    @fixture2 = RepositoryFixture.new("test2.git")
    @fixture2.setup

    @backend = GitRPC::Backend.new(@fixture2.path)
  end

  def test_fetch_for_mirror_fetches_from_specified_namespaces
    refs = @backend.fetch_for_mirror(@fixture.path, ["refs/namespace/*:refs/netscape/*", "refs/heads/*:refs/heads/*"], "refs/foo/", nil)
    expected_refs = {
      "refs/foo/refs/netscape/b1" => @commit,
      "refs/foo/refs/heads/b2" => @commit
    }

    assert_equal expected_refs, refs
  end

  def test_fetch_for_mirror_fetches_all_refs_by_default
    refs = @backend.fetch_for_mirror(@fixture.path, "", "refs/foo/", nil)
    expected_refs = {
      "refs/foo/refs/namespace/b1" => @commit,
      "refs/foo/refs/heads/b2" => @commit,
      "refs/foo/refs/more-heads/b3" => @commit
    }

    assert_equal expected_refs, refs
  end
end
