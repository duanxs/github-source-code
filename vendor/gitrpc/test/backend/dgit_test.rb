# frozen_string_literal: true
require_relative "../setup"
require "gitrpc/backend"
require "repository_fixture"

# Tests GitRPC::Backend's support for multiple object stores. Rugged and spawn
# interfaces are supported.
class DGitBackendTest < Minitest::Test
  include GitRPC::Util

  def setup
    @repo = RepositoryFixture.new
    @repo.setup
    @backend = GitRPC::Backend.new(@repo.path)
  end

  def teardown
    @repo.teardown
  end

  def test_dgit_state_init_v3
    assert_match /\A3:[0-9a-f]{40}\z/, @backend.dgit_state_init(3)
  end

  def test_dgit_state_init_v4
    assert_match /\A4:[0-9a-f]{40}\z/, @backend.dgit_state_init(4)
  end

  def test_dgit_state_init_v5
    assert_match /\A5:[0-9a-f]{40}\z/, @backend.dgit_state_init(5)
  end

  def test_dgit_state_init_with_no_repo
    @repo.teardown
    assert_raises GitRPC::DGitStateInitError do
      @backend.dgit_state_init(5)
    end
  end
end
