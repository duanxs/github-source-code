# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class BackendNwTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    setup_test_commits

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @oid1 = @fixture.commit_files({"README" => "Hello."}, "commit 1")
  end

  def test_nw_rm
    ret = @backend.nw_rm
    assert_equal true, ret

    assert_raises GitRPC::CommandFailed do
      allrefs = @backend.list_refs
    end
  end

  def test_nw_link
    refute @backend.nw_linked?

    # 'git nw-sync' is a no-op when run in an isolated repository.
    assert_nil @backend.nw_sync

    assert_nil @backend.nw_link
    assert @backend.nw_linked?

    assert_nil @backend.nw_sync

    assert_nil @backend.nw_unlink
    refute @backend.nw_linked?

    @fixture.command("rm -rf ../network.git")
  end

  def test_nw_gc
    res = @backend.nw_gc(root_network: "test", changed_path_bloom_filters: true)
    assert res["ok"]
    assert_equal "", res["out"]
    assert res["err"] =~ /Running git-repack/
    assert res["err"] =~ /changed-paths:true/
    assert res["err"] =~ /Repository GC completed/
  end

  def test_nw_repack
    assert_nil @backend.nw_repack
  end

  def test_nw_repack_trace2
    backend = GitRPC::Backend.new(@fixture.path, trace2: [:nw_repack])
    assert_nil backend.nw_repack
  end

  def test_nw_fsck
    res = @backend.nw_fsck
    assert res["ok"]
  end

  def test_last_fsck
    # last_fsck won't work with never: true if it's never been run before.
    res = @backend.last_fsck(never: true)
    refute res["ok"]

    res = @backend.last_fsck
    assert res["ok"]
    res2 = @backend.last_fsck(never: true)
    assert res2["ok"]

    @fixture.commit_files({"README" => "Hello again."}, "commit 2")

    # New commit won't be reflected in last_fsck output until we force.
    res2 = @backend.last_fsck
    assert res2["ok"]
    assert_equal res["out"], res2["out"]

    res3 = @backend.last_fsck(force: true)
    assert res3["ok"]
    refute_equal res["out"], res3["out"]
  end

  def test_janitor
    res = @backend.janitor
    refute res["ok"]
    assert res["out"] =~ /Cannot determine the repository type from its path/

    e = assert_raises GitRPC::Error do
      res = @backend.janitor(type: "asdf")
    end
    assert e.to_s =~ /invalid type asdf/

    res = @backend.janitor(type: "fork")
    refute res["ok"]
    assert res["out"] =~ /Should be a symlink but is a directory: hooks/
  end
end
