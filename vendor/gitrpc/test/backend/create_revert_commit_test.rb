# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class BackendCreateRevertCommitTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    @backend = GitRPC::Backend.new(@fixture.path)
    @rugged = @backend.rugged

    @author = {
      "name"  => "Some Human",
      "email" => "human@example.com",
      "time"  => "2000-01-01T00:00:00Z"
    }

    @committer = {
      "name"  => "Some Other Human",
      "email" => "other_human@example.com",
      "time"  => "2000-01-02T00:00:00Z"
    }

    setup_test_commits
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @oid  = @fixture.commit_files({"README" => "Goodbye."}, "commit 2")
  end

  def test_creates_revert_commit
    message = "revert it\n"
    rev_oid, err = @backend.create_revert_commit(@oid, @oid, @author, message, 0)
    assert_nil err

    rev_commit = @backend.read_commits([rev_oid]).first

    assert_equal [@oid], rev_commit["parents"]
    assert_equal message, rev_commit["message"]

    expected_author = [
      @author["name"],
      @author["email"],
      [Time.iso8601(@author["time"]).to_i, 0]
    ]

    assert_equal expected_author, rev_commit["author"]
    assert_equal expected_author, rev_commit["committer"]
  end

  def test_can_specify_committer
    message = "revert it\n"
    rev_oid, err = @backend.create_revert_commit(@oid, @oid, @author, message, 0, @committer)
    assert_nil err

    rev_commit = @backend.read_commits([rev_oid]).first

    expected_author = [
      @author["name"],
      @author["email"],
      [Time.iso8601(@author["time"]).to_i, 0]
    ]

    assert_equal expected_author, rev_commit["author"]

    expected_committer = [
      @committer["name"],
      @committer["email"],
      [Time.iso8601(@committer["time"]).to_i, 0]
    ]

    assert_equal expected_committer, rev_commit["committer"]

    assert_equal [@oid], rev_commit["parents"]
    assert_equal message, rev_commit["message"]
  end

  def test_stage_signed_revert_commit
    message = "revert it\n"
    raw, err = @backend.stage_signed_revert_commit(@oid, @oid, @author, message, 0)
    assert_nil err
    oid, err = @backend.create_revert_commit(@oid, @oid, @author, message, 0)
    assert_nil err

    assert_equal @rugged.read(oid).data, raw
  end

  def test_creates_revert_commits_for_range
    commit1_id = @fixture.commit_files({
      "README" => "Goodbye.\n\nFirst"
    }, "1")
    commit2_id = @fixture.commit_files({
      "README" => "Goodbye.\n\nSecond"
    }, "2")
    commit3_id = @fixture.commit_files({
      "README" => "Goodbye.\n\nThird"
    }, "3")

    revert_commit_id, err = @backend.create_revert_commits_for_range(commit1_id, commit3_id, commit3_id, @author, @author, 10)
    assert_nil err

    revert_commit1 = Rugged::Commit.lookup(@rugged, revert_commit_id)
    commit1 = Rugged::Commit.lookup(@rugged, commit1_id)

    assert_equal commit1.tree_id, revert_commit1.tree_id
    assert_equal <<-EOS, revert_commit1.message
Revert "2"

This reverts commit #{commit2_id}.
EOS

    revert_commit2 = revert_commit1.parents[0]
    assert_equal <<-EOS, revert_commit2.message
Revert "3"

This reverts commit #{commit3_id}.
EOS

    assert_equal [commit3_id], revert_commit2.parent_ids
  end

  def test_creates_revert_commits_for_range_timeout_failure
    commit1_id = @fixture.commit_files({
      "README" => "Goodbye.\n\nFirst"
    }, "1")
    commit2_id = @fixture.commit_files({
      "README" => "Goodbye.\n\nSecond"
    }, "2")
    commit3_id = @fixture.commit_files({
      "README" => "Goodbye.\n\nThird"
    }, "3")

    assert_raises GitRPC::Backend::RevertTimeout do
      @backend.create_revert_commits_for_range(commit1_id, commit3_id, commit3_id, @author, @author, 0.0001)
    end
  end

  def test_creates_revert_commits_for_range_fails_on_merge_commit
    commit1_id = @fixture.commit_files({
      "README" => "Goodbye.\n\nFirst"
    }, "1", branch: "master")

    @fixture.create_branch("topic")

    commit2_id = @fixture.commit_files({
      "other.md" => "Goodbye.\n\nSecond"
    }, "2", branch: "topic")

    commit3_id = @fixture.commit_files({
      "README" => "Goodbye.\n\nThird"
    }, "3", branch: "master")

    merge_commit_id = @fixture.merge("topic")

    message, err = @backend.create_revert_commits_for_range(commit3_id, merge_commit_id, merge_commit_id, @author, @author, 10)

    assert_equal "error", err
    assert_equal "merge commit encountered in specified commit range", message
  end
end
