# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"
require "securerandom"

require "minitest/mock"

class BackendPruneBackupsTest < Minitest::Test
  def setup
    @subfixture = RepositoryFixture.new
    @fixture = RepositoryFixture.new("#{@subfixture.path}/foo.git")
    @fixture.setup

    setup_test_commits

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
    @subfixture.teardown
  end

  def setup_test_commits
    @past_time = Time.local(2000).iso8601
    @oid1 = @fixture.commit_files({"README" => "Bonjour."}, "commit 1", :commit_time => @past_time)
    @oid2 = @fixture.commit_files({"README" => "Hello."}, "commit 2", :commit_time => @past_time)
  end

  def test_prune_backups
    parent = @fixture.path
    @fixture.command("mkdir #{parent}.backup.1000000000")
    @fixture.command("mkdir #{parent}.backup.1000000001")
    @fixture.command("mkdir #{parent}.backup.1000000002")
    @fixture.command("mkdir #{parent}.backup.1000000003")
    @fixture.command("mkdir #{parent}.backup.1000000004")

    errors = @backend.prune_backups(parent, 3)
    assert_empty errors

    assert File.exist?("#{parent}.backup.1000000000")
    refute File.exist?("#{parent}.backup.1000000001")
    refute File.exist?("#{parent}.backup.1000000002")
    assert File.exist?("#{parent}.backup.1000000003")
    assert File.exist?("#{parent}.backup.1000000004")

    @fixture.command("rmdir #{parent}.backup.*")
  end
end
