# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"
require "securerandom"

require "minitest/mock"

class BackendDiffTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    setup_test_commits

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @past_time = Time.local(2000).iso8601
    @oid1 = @fixture.commit_files({"README" => "Bonjour."}, "commit 1", :commit_time => @past_time)
    @oid2 = @fixture.commit_files({"README" => "Hello."}, "commit 2", :commit_time => @past_time)
  end

  def test_file_changed?
    assert @backend.file_changed?("README", @oid1, @oid2), "file_changed?('README')"
    refute @backend.file_changed?("nonexistent", @oid1, @oid2), "file_changed?('nonexistent')"
  end

  def test_file_changed_rejects_invalid_before
    assert_raises(GitRPC::InvalidCommitish) do
      @backend.file_changed?("README", "--output=/usr/bin/git", @oid2)
    end
  end

  def test_file_changed_rejects_invalid_after
    assert_raises(GitRPC::InvalidCommitish) do
      @backend.file_changed?("README", @oid1, "--output=/usr/bin/git")
    end
  end

  def test_file_changed_rejects_invalid_before_and_after
    assert_raises(GitRPC::InvalidCommitish) do
      @backend.file_changed?("README",
        "--output=/usr/bin/git", "--output=/usr/bin/git")
    end
  end

  def test_raw_diff
    raw_diff = @backend.raw_diff(@oid1, @oid2)
    assert_equal [":100644 100644 51cd7e8ece63f4cfbee8911acbc674e235d07aae 63af512c383ca207450e4340019cf55737bb40a0 M\tREADME"], raw_diff

    raw_diff = @backend.raw_diff("dead", "beef")
    assert_equal [], raw_diff
  end

  def test_raw_diff_rejects_invalid_before
    assert_raises(GitRPC::InvalidCommitish) do
      @backend.raw_diff("--output=/usr/bin/git", @oid2)
    end
  end

  def test_raw_diff_rejects_invalid_after
    assert_raises(GitRPC::InvalidCommitish) do
      @backend.raw_diff(@oid1, "--output=/usr/bin/git")
    end
  end

  def test_raw_diff_rejects_invalid_before_and_after
    assert_raises(GitRPC::InvalidCommitish) do
      @backend.raw_diff("--output=/usr/bin/git", "--output=/usr/bin/git")
    end
  end

  def test_diff_shortstat
    shortstat = @backend.diff_shortstat("#{@oid1}..#{@oid2}")
    assert_equal "1 file changed, 1 insertion(+), 1 deletion(-)", shortstat
  end

  def test_diff_shortstat_rejects_invalid_ranges
    assert_raises(GitRPC::InvalidOid) do
      @backend.diff_shortstat("--output=/usr/bin/git")
    end
  end

  def test_diff_tree
    diff = @backend.diff_tree(@oid1, @oid2)
    assert_equal [[":100644 100644 51cd7e8ece63f4cfbee8911acbc674e235d07aae 63af512c383ca207450e4340019cf55737bb40a0 M", "README"]], diff
  end

  def test_diff_tree_rejects_invalid_before
    assert_raises(GitRPC::InvalidCommitish) do
      @backend.diff_tree("--output=/usr/bin/git", @oid2)
    end
  end

  def test_diff_tree_rejects_invalid_after
    assert_raises(GitRPC::InvalidCommitish) do
      @backend.diff_tree(@oid1, "--output=/usr/bin/git")
    end
  end

  def test_diff_tree_rejects_invalid_before_and_after
    assert_raises(GitRPC::InvalidCommitish) do
      @backend.diff_tree("--output=/usr/bin/git", "--output=/usr/bin/git")
    end
  end
end
