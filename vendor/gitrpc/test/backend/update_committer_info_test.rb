# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class BackendUpdateCommiterInfoTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    setup_test_commits

    @backend = GitRPC::Backend.new(@fixture.path)
    @rugged = @backend.rugged

    @committer = {
      "name"  => "Some Guy",
      "email" => "guy@example.com",
      "time"  => "2000-01-01T00:00:00Z"
    }
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @fixture.add_empty_commit("Initial import")
    @base_commit_oid = @fixture.rev_parse("HEAD")
    @fixture.create_branch("topic")

    # advance master with invalid author email commit
    @fixture.command("git --work-tree='.' commit --author 'Joe Guy < >' --allow-empty -q -m 'sadly, no author email'")

    @no_email_oid = @fixture.rev_parse("HEAD")

    # put these commits on the topic branch
    @first_commit_id = @fixture.commit_files({"one" => "foo"}, "First commit", branch: "topic")
    @second_commit_id = @fixture.commit_files({"two" => "bar"}, "Second commmit", branch: "topic")
    @third_commit_id = @fixture.commit_files({"three" => "baz"}, "Third commit", branch: "topic")
  end

  def test_update_committer_info
    oid = @backend.update_committer_info(@third_commit_id, @first_commit_id, @committer)

    third_commit = Rugged::Commit.lookup(@rugged, @third_commit_id)
    second_commit = Rugged::Commit.lookup(@rugged, @second_commit_id)
    first_commit = Rugged::Commit.lookup(@rugged, @first_commit_id)

    commit = Rugged::Commit.lookup(@rugged, oid)

    assert_equal "390474447fe1f50eedea0acd783b3e586c5bd3a8", commit.oid
    assert_equal third_commit.author, commit.author
    assert_equal @committer["name"], commit.committer[:name]
    assert_equal @committer["email"], commit.committer[:email]
    assert_equal Time.parse(@committer["time"]), commit.committer[:time]
    assert_equal third_commit.message, commit.message
    assert_equal third_commit.tree_id, commit.tree_id

    commit = commit.parents[0]
    assert_equal second_commit.author, commit.author
    assert_equal @committer["name"], commit.committer[:name]
    assert_equal @committer["email"], commit.committer[:email]
    assert_equal Time.parse(@committer["time"]), commit.committer[:time]
    assert_equal second_commit.message, commit.message
    assert_equal second_commit.tree_id, commit.tree_id

    commit = commit.parents[0]
    assert_equal first_commit, commit
  end

  def test_handles_empty_author_fields
    original = Rugged::Commit.lookup(@rugged, @no_email_oid)
    oid = @backend.update_committer_info(@no_email_oid, @base_commit_oid, @committer)
    commit = Rugged::Commit.lookup(@rugged, oid)
    assert_equal "unknown", commit.author[:email]
    assert_equal original.author[:name], commit.author[:name]

    assert_equal original.message, commit.message
    assert_equal original.tree_id, commit.tree_id
  end

  def test_does_not_segfault_when_start_and_end_commit_are_equal
    oid = @backend.update_committer_info(@third_commit_id, @third_commit_id, @committer)
    assert_equal @third_commit_id, oid
  end
end
