# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

# Tests the GitRPC::Backend::Objects module methods
class BackendReadObjectsTest < Minitest::Test
  include GitRPC::Util

  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    setup_test_commits

    @backend = GitRPC::Backend.new(@fixture.path)
    @original_max_data_size = GitRPC::Backend.blob_maximum_data_size
  end

  def teardown
    @fixture.teardown
    GitRPC::Backend.blob_maximum_data_size = @original_max_data_size
  end

  def setup_test_commits
    @fixture.add_empty_commit("commit 1")
    @commit1 = @fixture.rev_parse("HEAD")

    @png_data = "\211PNG\r\n\032\n\000\000\000\rIHDR\000\000\000@\000\000\000@\004\003\000\000\000XGl\355\000\000\000'PLTE\377\377\377555\n\n\n...***&&&!!!\034\034\034\030\030\030\024\024\024\021\021\021\r\r\r2222\352\267\375\000\000\000\003tRNS\000\231\231\323&\336m\000\000\000JIDATx^\355\3121\001\200 \000EA*X\301\nT\260\002\025\254@\005+P\201\nT0\224\333\333\336\342\302\362o\276\262MD\034\202\360\nB\025\204K\020\232 \334\202\320\005\341\021\204!\bS\020\226 \234\242\374\023\021\037\023\000|Q\332\035Y\253\000\000\000\000IEND\256B`\202"
    @fixture.commit_files({
      "README"      => "hello",
      "subdir/test" => "somedata",
      "heavy_minus_sign.png" => @png_data
    }, "commit 2")
    @commit2 = @fixture.rev_parse("HEAD")

    @fixture.add_empty_commit("commit 3")
    @commit3 = @fixture.rev_parse("HEAD")
    @commit3_short = @commit3[0, 7]

    @fixture.add_empty_commit("commit 4\n\nThis has some near mentions (01010101 01ab23cd) and one real one (#{@commit3_short})")
    @commit4 = @fixture.rev_parse("HEAD")
  end

  def test_read_objects_commit
    res = @backend.read_objects([@commit2])
    assert_equal 1, res.size
    assert_equal "commit", res[0]["type"]
    assert_equal @commit2, res[0]["oid"]
    assert_equal "UTF-8", res[0]["encoding"]
    assert_equal "commit 2\n", res[0]["message"]
    assert_equal false, res[0]["message_truncated"]
    assert_equal({}, res[0]["message_shas"])
    assert_equal [@commit1], res[0]["parents"]
    assert_equal false, res[0]["has_signature"]

    assert_equal "Some Author", res[0]["author"][0]
    assert_equal "someone@example.org", res[0]["author"][1]
    assert_equal [1342581713, -25200], res[0]["author"][2]

    assert_equal res[0]["author"], res[0]["committer"]
  end

  def test_read_objects_commit_refers_to_other_commits
    commit = @backend.read_objects([@commit4]).first
    assert_equal({@commit3_short => @commit3}, commit["message_shas"])
  end

  def test_extract_shas_with_non_ascii_encoding
    message = "commit 4\n\nThis has some near mentions (01010101 01ab23cd) and one real one (#{@commit3_short})".encode(::Encoding::ISO_2022_JP)
    shas = @backend.extract_and_expand_shas(message)
    assert_equal({@commit3_short => @commit3}, shas)
  end

  def test_read_objects_commit_signature
    path = File.expand_path("../../examples/signed_objects.git", __FILE__)
    backend = GitRPC::Backend.new(path)
    res = backend.read_objects(["58266c4ab0e73d2a94319fa6695ba75bdc73edc8"])

    assert_equal true, res[0]["has_signature"]
  end

  def test_allows_signature_with_no_message
    path = File.expand_path("../../examples/signed_objects.git", __FILE__)
    backend = GitRPC::Backend.new(path)
    res = backend.read_objects(["d9630e25a00aa22a1f7fa72ddde37666095a3889"])

    assert_equal true, res[0]["has_signature"]
    assert_equal "", res[0]["message"]
  end

  def test_read_commits_truncates_commit_message
    GitRPC::Backend.stub :commit_message_max_length, 5 do
      res = @backend.read_objects([@commit2])
      assert_equal 1, res.size
      assert_equal "commi", res[0]["message"]
      assert_equal true, res[0]["message_truncated"]
    end
  end

  def test_read_objects_multiple_commits
    res = @backend.read_objects([@commit1, @commit3, @commit2])
    assert_equal 3, res.size
    assert_equal @commit1, res[0]["oid"]
    assert_equal @commit3, res[1]["oid"]
    assert_equal @commit2, res[2]["oid"]
  end

  def test_read_objects_missing_commit
    assert_raises GitRPC::ObjectMissing do
      @backend.read_objects([@commit1, "0" * 40, @commit2])
    end
  end

  def test_read_objects_missing_commit_with_skip_bad
    assert_equal 2, @backend.read_objects([@commit1, "0" * 40, @commit2], nil, true).length
  end

  def test_read_objects_tag
    tag_oid = @fixture.create_tag_object("v1.0", "We made it #{@commit3_short}", @commit2, "commit")
    tag = @backend.read_objects([tag_oid]).first
    assert_equal "tag", tag["type"]
    assert_equal tag_oid, tag["oid"]
    assert_equal @commit2, tag["target"]
    assert_equal "v1.0", tag["name"]
    assert_equal "commit", tag["target_type"]
    assert_equal "We made it #{@commit3_short}\n", tag["message"]
    assert_equal({@commit3_short => @commit3}, tag["message_shas"])
    assert_equal "Some Author", tag["tagger"][0]
    assert_equal "someone@example.org", tag["tagger"][1]
    assert_equal false, tag["has_signature"]
  end

  def test_read_objects_tag_with_nil_message
    tag_oid = @fixture.create_tag_object("v1.0", nil, @commit2, "commit")
    tag = @backend.read_objects([tag_oid]).first
    assert_equal tag_oid, tag["oid"]
    assert_nil tag["message"]
  end

  def test_read_objects_tag_with_nil_tagger
    tag_oid = @fixture.create_tag_object("v1.0", "Yuuup", @commit2, "commit", nil)
    tag = @backend.read_objects([tag_oid]).first
    assert_equal tag_oid, tag["oid"]
    assert_nil tag["tagger"]
  end

  def test_read_objects_signed_tag
    tag_oid = @fixture.create_signed_tag_object("v1.0", "We made it", @commit2, "commit")
    tag = @backend.read_objects([tag_oid]).first
    assert_equal "tag", tag["type"]
    assert_equal tag_oid, tag["oid"]
    assert_equal @commit2, tag["target"]
    assert_equal "v1.0", tag["name"]
    assert_equal "commit", tag["target_type"]
    assert_equal "We made it\n", tag["message"]
    assert_equal "Some Author", tag["tagger"][0]
    assert_equal "someone@example.org", tag["tagger"][1]
    assert_equal true, tag["has_signature"]
  end

  def test_read_objects_tree
    commits = @backend.read_objects([@commit2])
    assert commits[0]["tree"]

    res = @backend.read_objects([commits[0]["tree"]])
    assert_equal 1, res.size
    assert_equal "tree", res[0]["type"]
    assert_equal commits[0]["tree"], res[0]["oid"]
    assert_equal 3, res[0]["entries"].size

    entry = res[0]["entries"]["README"]
    assert_equal "b6fc4c620b67d95f953a5c1c1230aaab5db5a1b0", entry["oid"]
    assert_equal "blob", entry["type"]
    assert_equal 0100644, entry["mode"]
    assert_equal "README", entry["name"]

    entry = res[0]["entries"]["subdir"]
    assert_equal "3a9d26d61e2efa7b4b882d83e8883933d49c1ce4", entry["oid"]
    assert_equal "tree", entry["type"]
    assert_equal 040000, entry["mode"]
    assert_equal "subdir", entry["name"]
  end

  def test_read_objects_blob
    res = @backend.read_objects(["b6fc4c620b67d95f953a5c1c1230aaab5db5a1b0"])
    assert_equal "b6fc4c620b67d95f953a5c1c1230aaab5db5a1b0", res[0]["oid"]
    assert_equal "hello", res[0]["data"]
    assert_equal "hello".bytesize, res[0]["size"]
    assert_equal "UTF-8", res[0]["encoding"]
    assert_equal Encoding::UTF_8, res[0]["data"].encoding
    assert_equal false, res[0]["binary"]

    res = @backend.read_objects(["b8d3d82f2cd5141182c304ad90a6992eb2c5d4c1"])
    assert_equal Encoding::ASCII_8BIT, res[0]["data"].encoding
    assert_equal @png_data.bytesize, res[0]["size"]
    assert_nil res[0]["encoding"]
    assert_equal true, res[0]["binary"]
  end

  def test_read_objects_truncate
    GitRPC::Backend.blob_maximum_data_size = 1
    res = @backend.read_objects(["b6fc4c620b67d95f953a5c1c1230aaab5db5a1b0"])
    assert_equal "b6fc4c620b67d95f953a5c1c1230aaab5db5a1b0", res[0]["oid"]
    assert_equal true, res[0]["truncated"]
    assert_equal "", res[0]["data"]

    GitRPC::Backend.blob_maximum_data_size = 16
    GitRPC::Backend.blob_truncate_data_size = 1
    res = @backend.read_objects(["b6fc4c620b67d95f953a5c1c1230aaab5db5a1b0"])
    assert_equal "b6fc4c620b67d95f953a5c1c1230aaab5db5a1b0", res[0]["oid"]
    assert_equal true, res[0]["truncated"]
    assert_equal "h", res[0]["data"]

    GitRPC::Backend.blob_truncate_data_size = 16
    res = @backend.read_full_blob("b6fc4c620b67d95f953a5c1c1230aaab5db5a1b0")
    assert_equal "b6fc4c620b67d95f953a5c1c1230aaab5db5a1b0", res["oid"]
    assert_equal false, res["truncated"]
    assert_equal "hello", res["data"]

    GitRPC::Backend.blob_maximum_data_size = 16
    res = @backend.read_objects(["b6fc4c620b67d95f953a5c1c1230aaab5db5a1b0"])
    assert_equal "b6fc4c620b67d95f953a5c1c1230aaab5db5a1b0", res[0]["oid"]
    assert_equal false, res[0]["truncated"]
    assert_equal "hello", res[0]["data"]
  end

  def test_read_object_headers_blob
    res = @backend.read_object_headers([
      "b6fc4c620b67d95f953a5c1c1230aaab5db5a1b0",
      "b8d3d82f2cd5141182c304ad90a6992eb2c5d4c1"
    ])
    assert_equal 2, res.size
    assert_equal "blob", res[0]["type"]
    assert_equal "hello".bytesize, res[0]["size"]
    assert_equal "blob", res[1]["type"]
    assert_equal @png_data.bytesize, res[1]["size"]
  end
end
