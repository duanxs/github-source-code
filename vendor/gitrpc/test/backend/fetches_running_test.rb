# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"

class BackendFetchesRunningTest < Minitest::Test
  def setup
    @backend = GitRPC::Backend.new("/tmp")
  end

  def test_fetches_running
    count = @backend.fetches_running
    assert_kind_of Integer, count
  end
end
