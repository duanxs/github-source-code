# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class BackendRevParseTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    setup_test_commits

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @fixture.add_empty_commit("commit 1")
    @fixture.command "git tag v0.1 HEAD"
    @tag_sha1 = @fixture.rev_parse("v0.1")

    @fixture.add_empty_commit("commit 2")
    @fixture.command "git branch some-branch HEAD"
    @branch_sha1 = @fixture.rev_parse("some-branch")

    @fixture.add_empty_commit("commit 3")
    @fixture.command "
      set -e
      mkdir refs/special
      git rev-parse HEAD > refs/special/foo
    "
    @special_sha1 = @fixture.rev_parse("refs/special/foo")
  end

  def test_rev_parse_branch
    assert_equal @branch_sha1, @backend.rev_parse("some-branch")
    assert_equal @branch_sha1, @backend.rev_parse("refs/heads/some-branch")
  end

  def test_rev_parse_tag
    assert_equal @tag_sha1, @backend.rev_parse("v0.1")
    assert_equal @tag_sha1, @backend.rev_parse("refs/tags/v0.1")
  end

  def test_rev_parse_sha1
    assert_equal @tag_sha1, @backend.rev_parse(@tag_sha1)
    assert_equal @branch_sha1, @backend.rev_parse(@branch_sha1)
  end

  def test_rev_parse_special
    assert_equal @special_sha1, @backend.rev_parse("refs/special/foo")
  end

  def test_rev_parse_special_syntax
    assert_equal @tag_sha1, @backend.rev_parse("some-branch^")
    assert_equal @tag_sha1, @backend.rev_parse("#{@branch_sha1}^")
  end

  def test_rev_parse_short_sha1
    assert_equal @branch_sha1, @backend.rev_parse(@branch_sha1[0, 7])
  end

  def test_rev_parse_bad_revision
    assert_nil @backend.rev_parse("doesnt-exist")
  end

  def test_rev_parse_with_special_dots
    assert_nil @backend.rev_parse("foo..bar")
  end

  def test_rev_parse_with_missing_numeric_branch_name
    assert_nil @backend.rev_parse("123")
  end
end
