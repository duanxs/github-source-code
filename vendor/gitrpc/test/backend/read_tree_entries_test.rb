# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class BackendReadTreeEntryTest < Minitest::Test
  include GitRPC::Util

  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    @submodule_fixture = RepositoryFixture.new("submodule.git")
    @submodule_fixture.setup

    setup_test_commits

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
    @submodule_fixture.teardown
  end

  def test_filename_encoding
    fname = "bad\x92enc".b
    repo = @backend.rugged
    oid = repo.write("This is a blob.", :blob)
    index = repo.index
    index.read_tree(repo.head.target.tree)
    index.add(:path => fname, :oid => oid, :mode => 0100644)

    options = {}
    options[:tree] = index.write_tree(repo)

    options[:author] = { :email => "testuser@github.com", :name => "Test Author", :time => Time.now }
    options[:committer] = { :email => "testuser@github.com", :name => "Test Author", :time => Time.now }
    options[:message] = "filename with bad encoding"
    options[:parents] = [repo.head.target]
    options[:update_ref] = "HEAD"

    commit = Rugged::Commit.create(repo, options)

    res = @backend.read_tree_entries(commit, "", nil, false, false)["entries"]
    assert res.map { |x| x["name"] }.find { |name| name.b == fname }, "we should find the bad name"
  end

  def setup_test_commits
    @commit = @fixture.commit_files({
      "README"       => "hello",
      ".gitignore"   => "asdf",
      "subdir/test"  => "somedata",
      "other/actual" => "Actual contents",
    }, "commit msg")

    @submodule_fixture.commit_files({
        "README" => "an submodule"
      }, "in sub")

    @submodule_commit = @fixture.add_submodule_commit(@submodule_fixture.path,
                                                      "submod",
                                                      "Add a submodule")

    @symlink = @fixture.commit_symlink("test",
                                       "subdir/link",
                                       "add symlink")

    @relative_symlink = @fixture.commit_symlink("../other/actual",
                                                "subdir/relative_link",
                                                "symlink up a dir")

    @submodule_symlink = @fixture.commit_symlink("../submod",
                                       "subdir/submod",
                                       "add symlink to a submodule")

    @readme_symlink = @fixture.commit_symlink("README",
                                              "README.md",
                                              "symlink README")

    @large_path_commit = @fixture.commit_files({
        "com/github/path/test/what/hello.java" => "foo",
        "com/github/path/test/what/world.java" => "foo",
        "com/github/path/test/wot/hello.java" => "foo",
        "org/github/path/test/very/long/path/dont/overflow/please.java" => "foo"
    }, "commit with large paths")

    @bad_commit = @commit.tr("a-z0-9", "a")
  end

  def test_read_tree_entries_tree
    [true, false].each do |skip_size|
      res = @backend.read_tree_entries(@commit, "", nil, false, skip_size)
      entries = res["entries"]
      assert_equal 4, entries.size
      assert_equal "41bb0eea179c9a4485b26ecc45dfa0be4cca28df", res["oid"]

      gitignore = entries.shift
      readme = entries.shift
      other  = entries.shift
      subdir = entries.shift

      assert_equal ".gitignore", gitignore["name"]
      assert_equal "blob", gitignore["type"]
      assert_equal "5e40c0877058c504203932e5136051cf3cd3519b", gitignore["oid"]
      assert_equal ".gitignore", gitignore["path"]

      assert_equal "subdir", subdir["name"]
      assert_equal "3a9d26d61e2efa7b4b882d83e8883933d49c1ce4", subdir["oid"]
      assert_equal "tree", subdir["type"]
      assert_equal "subdir", subdir["path"]

      assert_equal "other", other["name"]
      assert_equal "aa7edacb1b9500cd6dd49e5fe8efe75691030c21", other["oid"]
      assert_equal "tree", other["type"]
      assert_equal "other", other["path"]

      assert_equal "README", readme["name"]
      assert_equal "b6fc4c620b67d95f953a5c1c1230aaab5db5a1b0", readme["oid"]
      assert_equal "blob", readme["type"]
      if skip_size
        assert_nil readme["size"]
      else
        assert_equal 5, readme["size"]
      end
      assert_equal 0100644, readme["mode"]
      assert_equal "README", readme["path"]
    end
  end

  def test_read_tree_entries_blob
    assert_raises GitRPC::InvalidObject do
      @backend.read_tree_entries(@commit, "README", nil, false)
    end
  end

  def test_read_tree_entries_nested
    res = @backend.read_tree_entries(@commit, "subdir", nil, false)
    entries = res["entries"]
    assert_equal 1, entries.size
    assert_equal "3a9d26d61e2efa7b4b882d83e8883933d49c1ce4", res["oid"]
    test = entries.first

    assert_equal "test", test["name"]
    assert_equal "16ac089539f23bae4bfbc861281e48dfaabd233d", test["oid"]
    assert_equal "blob", test["type"]
    assert_equal "subdir/test", test["path"]
  end

  def test_read_non_existing_path
    assert_raises GitRPC::NoSuchPath do
      @backend.read_tree_entries(@commit, "nonexistant", nil, false)
    end
  end

  def test_read_non_existing_ref
    assert_raises GitRPC::ObjectMissing do
      @backend.read_tree_entries(@bad_commit, "README", nil, false)
    end
  end

  def test_read_with_symlink
    [true, false].each do |skip_size|
      res = @backend.read_tree_entries(@submodule_symlink, "subdir", nil, false, skip_size)
      entries = res["entries"]
      assert_equal 4, entries.size
      assert_equal "2c01b53e5a974f6b3556baad1519eb985f2d2013", res["oid"]
      link = entries.shift
      relative_link = entries.shift
      submod = entries.shift
      test = entries.shift

      assert_equal "test", test["name"]
      assert_equal "16ac089539f23bae4bfbc861281e48dfaabd233d", test["oid"]
      assert_equal "blob", test["type"]
      assert_nil test["symlink_target"]
      assert_nil test["symlink_target_object"]
      assert_equal "subdir/test", test["path"]

      assert_equal "link", link["name"]
      assert_equal "16ac089539f23bae4bfbc861281e48dfaabd233d", link["symlink_target"]
      assert_equal "blob", link["type"]
      assert_equal "30d74d258442c7c65512eafab474568dd706c430", link["oid"]
      assert_equal "subdir/link", link["path"]

      link_target = link["symlink_target_object"]
      assert_equal "16ac089539f23bae4bfbc861281e48dfaabd233d", link_target["oid"]
      assert_equal "blob", link_target["type"]
      assert_equal "test", link_target["name"]
      if skip_size
        assert_nil link_target["size"]
      else
        assert_equal 8, link_target["size"]
      end
      assert_equal 0100644, link_target["mode"]
      assert_equal "subdir/test", link_target["path"]

      assert_equal "relative_link", relative_link["name"]
      assert_equal "ed8724af6c745fd91fca37451a77f6b9cfa53185", relative_link["symlink_target"]
      assert_equal "blob", relative_link["type"]
      assert_equal "c1f427f13a39dd05233d9d0b0aa7364cd38db672", relative_link["oid"]
      assert_equal "subdir/relative_link", relative_link["path"]

      relative_link_target = relative_link["symlink_target_object"]
      assert_equal "ed8724af6c745fd91fca37451a77f6b9cfa53185", relative_link_target["oid"]
      assert_equal "blob", relative_link_target["type"]
      assert_equal "actual", relative_link_target["name"]
      if skip_size
        assert_nil relative_link_target["size"]
      else
        assert_equal 15, relative_link_target["size"]
      end
      assert_equal 0100644, relative_link_target["mode"]
      assert_equal "other/actual", relative_link_target["path"]

      assert_equal "submod", submod["name"]
      assert_nil submod["symlink_target"]
      assert_equal "blob", submod["type"]
      assert_equal "5e3541c87372373856fc578adfdd912336b5765a", submod["oid"]
      assert_equal "subdir/submod", submod["path"]
    end
  end

  def test_read_with_root_symlink
    [true, false].each do |skip_size|
      res = @backend.read_tree_entries(@readme_symlink, nil, nil, false, skip_size)
      entries = res["entries"]
      assert_equal 7, entries.size
      link = entries[3]

      assert_equal "README.md", link["name"]
      assert_equal "b6fc4c620b67d95f953a5c1c1230aaab5db5a1b0", link["symlink_target"]
      assert_equal "100b93820ade4c16225673b4ca62bb3ade63c313", link["oid"]
      assert_equal "blob", link["type"]
      assert_equal "README.md", link["path"]

      link_target = link["symlink_target_object"]
      assert_equal "b6fc4c620b67d95f953a5c1c1230aaab5db5a1b0", link_target["oid"]
      assert_equal "blob", link_target["type"]
      assert_equal "README", link_target["name"]
      if skip_size
        assert_nil link_target["size"]
      else
        assert_equal 5, link_target["size"]
      end
      assert_equal 0100644, link_target["mode"]
      assert_equal "README", link_target["path"]
    end
  end

  def test_limit_total_entries
    res = @backend.read_tree_entries(@readme_symlink, nil, 3, false)
    assert_equal 3, res["entries"].size
    assert_equal 4, res["truncated_entries"]
  end

  def test_without_limiting_entries
    res = @backend.read_tree_entries(@readme_symlink, nil, nil, false)
    assert_equal 7, res["entries"].size
    assert_nil res["truncated_entries"]
  end

  # read_tree_entry tests
  def test_read_tree_entry
    res = @backend.read_tree_entry(@commit, "README")
    assert_equal "README", res["name"]
    assert_equal "b6fc4c620b67d95f953a5c1c1230aaab5db5a1b0", res["oid"]
    assert_equal "blob", res["type"]
    assert_equal 5, res["size"]
    assert_equal 0100644, res["mode"]
    assert_equal "hello", res["data"]
    assert_equal "README", res["path"]
  end

  def test_read_nested_tree_entry
    res = @backend.read_tree_entry(@commit, "subdir/test")
    assert_equal "subdir/test", res["name"]
    assert_equal "16ac089539f23bae4bfbc861281e48dfaabd233d", res["oid"]
    assert_equal "blob", res["type"]
    assert_equal 8, res["size"]
    assert_equal 0100644, res["mode"]
    assert_equal "somedata", res["data"]
    assert_equal "subdir/test", res["path"]
  end

  def test_read_tree_entry_specific_type
    assert_raises GitRPC::InvalidObject do
      @backend.read_tree_entry(@commit, "subdir",
                               "truncate" => 1024,
                               "limit" => 1024,
                               "type" => "blob")
    end
  end

  def test_read_tree_entry_with_symlink
    link = @backend.read_tree_entry(@submodule_symlink, "subdir/link")

    assert_equal "subdir/link", link["name"]
    assert_equal "16ac089539f23bae4bfbc861281e48dfaabd233d", link["symlink_target"]
    assert_equal "blob", link["type"]
    assert_equal "30d74d258442c7c65512eafab474568dd706c430", link["oid"]
    assert_equal "subdir/link", link["path"]

    link_target = link["symlink_target_object"]
    assert_equal "16ac089539f23bae4bfbc861281e48dfaabd233d", link_target["oid"]
    assert_equal "blob", link_target["type"]
    assert_equal "test", link_target["name"]
    assert_equal 8, link_target["size"]
    assert_equal 0100644, link_target["mode"]
    assert_equal "subdir/test", link_target["path"]
  end

  def test_read_tree_entry_dotfile
    gitignore = @backend.read_tree_entry(@submodule_symlink, ".gitignore")

    assert_equal ".gitignore", gitignore["name"]
    assert_equal "blob", gitignore["type"]
    assert_equal "5e40c0877058c504203932e5136051cf3cd3519b", gitignore["oid"]
    assert_equal ".gitignore", gitignore["path"]
  end

  def test_read_simplified_paths
    result = @backend.read_tree_entries(@large_path_commit, nil, nil, true)

    org_entry = result["entries"].find { |e| e["name"] == "org" }
    com_entry = result["entries"].find { |e| e["name"] == "com" }

    assert org_entry
    assert com_entry

    assert_equal org_entry["simplified_path"], "org/github/path/test/very/long/path/dont/overflow"
    assert_equal com_entry["simplified_path"], "com/github/path/test"
  end

  def test_read_tree_entry_oid_with_path
    expected = @backend.rev_parse(@large_path_commit + ":com")
    result = @backend.read_tree_entry_oid(@large_path_commit, "com", "type" => "tree")

    assert_equal expected, result

    assert_raises GitRPC::InvalidObject do
      @backend.read_tree_entry_oid(@large_path_commit, "com", "type" => "blob")
    end

    assert_raises GitRPC::NoSuchPath do
      @backend.read_tree_entry_oid(@large_path_commit, "com2")
    end

  end

  def test_read_tree_entry_oid_without_path
    expected = @backend.rev_parse(@large_path_commit + "^{tree}")
    result = @backend.read_tree_entry_oid(@large_path_commit, "")

    assert_equal expected, result
  end
end
