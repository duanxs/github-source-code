# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class BackendAuditLogTest < Minitest::Test
  SIMPLE_AUDIT_LOG = File.expand_path("../../examples/simple_audit_log", __FILE__)

  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @audit_log = File.join(@fixture.path, "audit_log")
    FileUtils.cp(SIMPLE_AUDIT_LOG, @audit_log)

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
  end

  def test_read_audit_log_by_offsets
    lines = @backend.read_audit_log_by_offsets([0])
    expected = {
      "byteoffset" => 0,
      "bytesize" => 151,
      "ref" => "refs/heads/master",
      "old_oid" => "0000000000000000000000000000000000000000",
      "new_oid" => "d05a91386fa2ce466f9045089dab53896c7dd341",
      "committer_name" => "Some Author",
      "committer_email" => "someone@example.org",
      "committer_date_timestamp" => 1342581713,
      "committer_date_offset" => -25200,
      "sockstat" => {}
    }
    assert_equal expected, lines[0]
  end

  def test_sha1sum_audit_log
    sha1sum = @backend.sha1sum_audit_log
    assert_match /\A[0-9a-f]{7}\z/, sha1sum
  end

  def test_last_audit_log_time
    time = @backend.last_audit_log_time
    assert_kind_of Time, time
  end

  def test_return_empty_for_nonexistent_audit_log
    FileUtils.rm(@audit_log)
    refute File.exist?(@audit_log)

    lines, eof = @backend.read_audit_log_lines(0, 1)
    assert_equal [], lines
    assert eof
  end

  def test_read_entire_audit_log_from_top
    lines, eof = @backend.read_audit_log_lines(0, 100)
    assert_equal 10, lines.size
    assert eof

    assert_equal "refs/heads/master 0000000000000000000000000000000000000000 d05a91386fa2ce466f9045089dab53896c7dd341 Some Author <someone@example.org> 1342581713 -0700\n", lines[0]
    assert_equal "refs/heads/master 977c954a5d52a25bf658693817a8282daabe21f3 b43e94f468b4fe9a0b817d25eeb1b20c62b7a44e Some Author <someone@example.org> 1342581713 -0700\n", lines[-1]
  end

  def test_read_truncated_audit_log_from_top
    lines, eof = @backend.read_audit_log_lines(0, 3)
    assert_equal 3, lines.size
    refute eof

    assert_equal [
      "refs/heads/master 0000000000000000000000000000000000000000 d05a91386fa2ce466f9045089dab53896c7dd341 Some Author <someone@example.org> 1342581713 -0700\n",
      "refs/heads/master d05a91386fa2ce466f9045089dab53896c7dd341 ef64b4f1cb09900d4f79c017897273cbdcb7643e Some Author <someone@example.org> 1342581713 -0700\n",
      "refs/heads/master ef64b4f1cb09900d4f79c017897273cbdcb7643e a7d6711daf5a43b682d0a21cb7212109c74bcbdc Some Author <someone@example.org> 1342581713 -0700\n"
    ], lines
  end

  def test_read_audit_log_from_byte_offset
    lines, eof = @backend.read_audit_log_lines(151, 3)
    assert_equal 3, lines.size
    refute eof

    assert_equal [
      "refs/heads/master d05a91386fa2ce466f9045089dab53896c7dd341 ef64b4f1cb09900d4f79c017897273cbdcb7643e Some Author <someone@example.org> 1342581713 -0700\n",
      "refs/heads/master ef64b4f1cb09900d4f79c017897273cbdcb7643e a7d6711daf5a43b682d0a21cb7212109c74bcbdc Some Author <someone@example.org> 1342581713 -0700\n",
      "refs/heads/master a7d6711daf5a43b682d0a21cb7212109c74bcbdc 92533ebc0c16e412a355004934d22354d733118e Some Author <someone@example.org> 1342581713 -0700\n"
    ], lines
  end

  def test_read_audit_log_from_invalid_byte_offset
    lines, eof = @backend.read_audit_log_lines(175, 2)
    assert_equal 2, lines.size
    refute eof

    assert_equal [
      "386fa2ce466f9045089dab53896c7dd341 ef64b4f1cb09900d4f79c017897273cbdcb7643e Some Author <someone@example.org> 1342581713 -0700\n",
      "refs/heads/master ef64b4f1cb09900d4f79c017897273cbdcb7643e a7d6711daf5a43b682d0a21cb7212109c74bcbdc Some Author <someone@example.org> 1342581713 -0700\n",
    ], lines
  end
end
