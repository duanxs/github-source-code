# rubocop:disable Style/FrozenStringLiteralComment
require_relative "../setup"

require "gitrpc"
require "repository_fixture"

class BackendNativeReadDiff < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @commit1 = @fixture.commit_files({"README" => "Hello."}, "commit 1")
    @commit2 = @fixture.commit_files({"README" => "Goodbye."}, "commit 2")
    @commit3 = @fixture.commit_files({"README" => "Hi again."}, "commit 3")

    work_tree = "#{@fixture.path}/work"
    @fixture.command "git --work-tree='#{work_tree}' mv README README.md"
    @fixture.commit_changes "rename"
    @rename = @fixture.rev_parse("HEAD")

    @commit4 = @fixture.commit_files({"README" => File.read(File.expand_path("../../examples/README.md", __FILE__))}, "Bigger diff")
    @commit5 = @fixture.commit_files(Hash[10.times.map { |i| ["file#{i}", "Some file contents"] }], "Lots of files")

    @fixture.command("git --work-tree='#{work_tree}' checkout -b merge-test")
    @commit6 = @fixture.commit_files({"README" => "Hello AGAIN!"}, "commit 6", {:branch => "merge-test"})
    @commit7 = @fixture.commit_files({"README" => "Hello AGAIN! AGAIN"}, "commit 7")

    @fixture.command("git --work-tree='#{work_tree}' checkout --orphan gh-pages")
    @complete_separate = @fixture.commit_files({"index.html" => "<h1>It Lives!</h1>"}, "First commit on pages", {:branch => "gh-pages"})

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
  end

  def test_diff_text
    expected = "diff --git a/README b/README\nindex 63af512..9407a03 100644\n--- a/README\n+++ b/README\n@@ -1 +1 @@\n-Hello.\n\\ No newline at end of file\n+Goodbye.\n\\ No newline at end of file\n"
    assert_equal expected, @backend.native_diff_text(@commit1, @commit2)
  end

  def test_diff_text_with_rename
    expected = "diff --git a/README b/README.md\nsimilarity index 100%\nrename from README\nrename to README.md\n"
    assert_equal expected, @backend.native_diff_text(@commit3, @rename)
  end

  def test_diff_text_with_bogus_args
    assert_raises(GitRPC::InvalidObject) do
      @backend.native_diff_text("NOTASHA", @commit2)
    end

    assert_raises(GitRPC::InvalidObject) do
      @backend.native_diff_text(@commit1, "NOTASHA")
    end
  end

  def test_diff_text_with_no_merge_base
    expected = "diff --git a/README b/README\nindex 63af512..0802f10 100644\n--- a/README\n+++ b/README\n@@ -1 +1 @@\n-Hello.\n\\ No newline at end of file\n+Hello AGAIN! AGAIN\n\\ No newline at end of file\ndiff --git a/README.md b/README.md\nnew file mode 100644\nindex 0000000..efaaad3\n--- /dev/null\n+++ b/README.md\n@@ -0,0 +1 @@\n+Hi again.\n\\ No newline at end of file\ndiff --git a/file0 b/file0\nnew file mode 100644\nindex 0000000..0cf1d2b\n--- /dev/null\n+++ b/file0\n@@ -0,0 +1 @@\n+Some file contents\n\\ No newline at end of file\ndiff --git a/file1 b/file1\nnew file mode 100644\nindex 0000000..0cf1d2b\n--- /dev/null\n+++ b/file1\n@@ -0,0 +1 @@\n+Some file contents\n\\ No newline at end of file\ndiff --git a/file2 b/file2\nnew file mode 100644\nindex 0000000..0cf1d2b\n--- /dev/null\n+++ b/file2\n@@ -0,0 +1 @@\n+Some file contents\n\\ No newline at end of file\ndiff --git a/file3 b/file3\nnew file mode 100644\nindex 0000000..0cf1d2b\n--- /dev/null\n+++ b/file3\n@@ -0,0 +1 @@\n+Some file contents\n\\ No newline at end of file\ndiff --git a/file4 b/file4\nnew file mode 100644\nindex 0000000..0cf1d2b\n--- /dev/null\n+++ b/file4\n@@ -0,0 +1 @@\n+Some file contents\n\\ No newline at end of file\ndiff --git a/file5 b/file5\nnew file mode 100644\nindex 0000000..0cf1d2b\n--- /dev/null\n+++ b/file5\n@@ -0,0 +1 @@\n+Some file contents\n\\ No newline at end of file\ndiff --git a/file6 b/file6\nnew file mode 100644\nindex 0000000..0cf1d2b\n--- /dev/null\n+++ b/file6\n@@ -0,0 +1 @@\n+Some file contents\n\\ No newline at end of file\ndiff --git a/file7 b/file7\nnew file mode 100644\nindex 0000000..0cf1d2b\n--- /dev/null\n+++ b/file7\n@@ -0,0 +1 @@\n+Some file contents\n\\ No newline at end of file\ndiff --git a/file8 b/file8\nnew file mode 100644\nindex 0000000..0cf1d2b\n--- /dev/null\n+++ b/file8\n@@ -0,0 +1 @@\n+Some file contents\n\\ No newline at end of file\ndiff --git a/file9 b/file9\nnew file mode 100644\nindex 0000000..0cf1d2b\n--- /dev/null\n+++ b/file9\n@@ -0,0 +1 @@\n+Some file contents\n\\ No newline at end of file\ndiff --git a/index.html b/index.html\nnew file mode 100644\nindex 0000000..7bb9d96\n--- /dev/null\n+++ b/index.html\n@@ -0,0 +1 @@\n+<h1>It Lives!</h1>\n\\ No newline at end of file\n"
    assert_equal expected, @backend.native_diff_text(@commit1, @complete_separate)
  end

  def test_patch_text
    expected = "From f4a9faa616ec7a40ab37eb82801fe5c13e535f1c Mon Sep 17 00:00:00 2001\nFrom: Some Author <someone@example.org>\nDate: Tue, 17 Jul 2012 20:21:53 -0700\nSubject: [PATCH] commit 7\n\n---\n README | 202 +--------------------------------------------------------\n 1 file changed, 1 insertion(+), 201 deletions(-)\n\ndiff --git a/README b/README\nindex 994b6a2..0802f10 100644\n--- a/README\n+++ b/README\n@@ -1,201 +1 @@\n-GitRPC\n-======\n-\n-A network oriented library for accessing remote Git repositories reliably and\n-efficiently. GitRPC is a recasting of the many independent components that make\n-up GitHub's application git stack into a single cohesive library and set of\n-abstractions.\n-\n-  - Optimized for efficient access over a network.\n-  - Small core RPC interface designed for batch operation.\n-  - Simple objects only: hashes, arrays, and scalar types.\n-  - Layered design (Client \xE2\x86\x92 Cache \xE2\x86\x92 Protocol \xE2\x86\x92 Backend).\n-  - Aggressive client side caching (memcached).\n-  - [Rugged](https://github.com/libgit2/rugged) (libgit2) git access.\n-\n-See [RATIONALE][R] for more on the history behind this project and its goals.\n-\n-[R]: https://github.com/github/gitrpc/blob/master/RATIONALE.md\n-\n-Status\n-------\n-\n-GitRPC is currently under heavy pull request driven development. If you're\n-interested in coming up to speed, consider reviewing our progress so far:\n-\n- - [#2](https://github.com/github/gitrpc/pull/2) Basic development\n-   environment, `GitRPC::Backend`, and project roadmap discussion.\n- - [#3](https://github.com/github/gitrpc/pull/3) Ports `Backend#refs` from\n-   native commands to Rugged.\n- - [#4](https://github.com/github/gitrpc/pull/4) `GitRPC::Client` and basic\n-   caching strategy.\n- - [#5](https://github.com/github/gitrpc/pull/5) Ports `Backend#read_commits`\n-   from native commands to Rugged.\n- - [#9](https://github.com/github/gitrpc/pull/9) `GitRPC.new`, general library\n-   options, and usage documentation.\n- - [#8](https://github.com/github/gitrpc/pull/8) Kicks off the basic framework\n-   for URL protocol resolvers and implements `file:` URLs.\n- - [#10](https://github.com/github/gitrpc/pull/10) The `bertrpc:` protocol\n-   implementation and dealing with remote failures / exceptions.\n- - [#11](https://github.com/github/gitrpc/pull/11) The `chimney:` protocol\n-   implementation / looking up repo hosts in redis.\n- - [#19](https://github.com/github/gitrpc/pull/19) Adds the `list_revision_history`\n-   RPC call.\n- - [#20](https://github.com/github/gitrpc/pull/20) Adds the `init` and `exists?`\n-   checks for creating repos remotely.\n- - [#21](https://github.com/github/gitrpc/pull/21) Adds support for publishing\n-   `ActiveSupport::Notifications` style instrumentation events.\n-\n-At this point most of the core framework is stablizing and the focus is on\n-implementing RPC calls needed by Gist 2.0 and github.com.\n-\n-Basic Usage\n------------\n-\n-Get this party started:\n-\n-    >> require 'gitrpc'\n-    >> git = GitRPC.new('file:/path/to/repository.git')\n-    #<GitRPC::Client:0x10c... @backend=...>\n-\n-Optionally enable git object caching:\n-\n-    >> require 'dalli'\n-    >> GitRPC.cache = Dalli::Client.new('localhost:11211')\n-\n-Retrieve all refs for the repository in a single hash (cached: 1 netop):\n-\n-    >> git.read_refs\n-    { 'refs/heads/master' => 'deadbeee...', 'refs/head/some-branch' => ... }\n-\n-Resolve a complex revision down to a sha1 object id (not cached):\n-\n-    >> git.rev_parse('master@{2.days.ago}')\n-    'deadbeee...'\n-\n-Read commit metadata in batch and nothing else (cached: 1 netop):\n-\n-    >> git.read_commits(['deadbeee...', 'badfffff...'])\n-    [{\"type\"      => \"commit\",\n-      \"oid\"       => \"d3224d5...\",\n-      \"parents\"   => [\"fd1545cc...\"],\n-      \"tree\"      => \"6d1470d...\",\n-      \"author\"    => [\"Ryan Tomayko\", \"ryan@github.com\", \"2012-06-13T03:29:30Z\"],\n-      \"committer\" => [\"Ryan Tomayko\", \"ryan@github.com\", \"2012-06-13T03:29:30Z\"],\n-      \"message\"   => \":encoding docs\",\n-      \"encoding\"  => \"UTF-8\"},\n-     {\"type\"      => \"commit\",\n-      \"oid\"       => \"abcdef0...\",\n-      ...}\n-\n-Execute a native git command and return the output, exit status, and auxiliary\n-information:\n-\n-    >> git.spawn_git('diff', ['--stat', 'master~5', 'master'])\n-    {\n-      'ok'      => true,\n-      'status'  => 0,\n-      'pid'     => 4321,\n-      'out'     => \"...\",\n-      'err'     => \"...\",\n-      'argv'    => ['git', '--git-dir', '/path/to/repo.git', 'diff', ...],\n-      'path'    => '/path/to/repo.git'\n-    }\n-\n-Connect to a repository on a remote machine via BERTRPC:\n-\n-    >> git = GitRPC.new('bertrpc://example.com:8175/path/to/repository.git')\n-    >> git.rev_parse('HEAD')\n-    'deadbeee...'\n-    >> git.refs\n-    { ... }\n-\n-Call Reference Documentation\n-----------------------------\n-\n-All public calls are documented individually here:\n-\n-https://github.com/github/gitrpc/tree/master/doc/#gitrpc\n-\n-Architecture\n-------------\n-\n-GitRPC is designed to work in both large distributed environments with\n-multi-machine storage / cache clusters as well as entirely locally\n-using the same basic interfaces and with the same error modes.\n-\n-The architecture is enabled by separating operations into three distinct layers:\n-from bottom up they are the **Backend**, **Protocol**, and **Client** layers.\n-\n-### Backend\n-\n-**GitRPC::Backend** is where local git repository access is performed. Operations\n-are modeled as a simple flat namespace of RPC method invocations on this object.\n-\n-Most methods are implemented with Rugged (libgit2) for its rich API and to\n-minimize process creation overhead. Fast native git command spawning is also\n-available.\n-\n-See [GitRPC::Backend][b] for more information.\n-\n-[b]: https://github.com/github/gitrpc/tree/master/lib/gitrpc/backend.rb\n-\n-### Protocol\n-\n-Access to the **Backend** is mediated by one or more **GitRPC::Protocol**\n-implementations. Local and remote access to repositories is enabled through a\n-system of pluggable URL resolvers:\n-\n-    GitRPC.new(\"file:///data/repositories/example.git\")\n-    GitRPC.new(\"bertrpc://fs1.rs.github.com:8195/rtomayko/example.git\")\n-    GitRPC.new(\"chimney:rtomayko/example.git\")\n-    GitRPC.new(\"http://github.com/rtomayko/example.git\")\n-\n-See [GitRPC::Protocol][p] for more information on protocol responsibilities. In\n-addition to resolving URLs, protocol implementations also handle RPC\n-encoded/decoding, network access, and translating error modes.\n-\n-[p]: https://github.com/github/gitrpc/tree/master/lib/gitrpc/protocol.rb\n-\n-### Client\n-\n-The **GitRPC::Client** implements network frontend logic for all public\n-**Backend** methods. This part of the library is always available on the client\n-regardless of whether the **Backend** is being accessed remotely.\n-\n-`GitRPC.new` returns a **Client** object so this is also the main interface for\n-calling code.\n-\n-All public **Backend** methods must be explicitly implemented on the **Client**\n-interface. There is no `method_missing` style catchall that exposes all backend\n-calls by default. This is to encourage thinking through the caching and network\n-access patterns for all git access.\n-\n-Where possible, **Client** method implementations are designed to avoid\n-communication with the **Backend** fully or partially through aggressive caching\n-and short circuiting logic.\n-\n-See [GitRPC::Client][c] for more information on this layer.\n-\n-[c]: https://github.com/github/gitrpc/tree/master/lib/gitrpc/client.rb\n-\n-Contributing\n-------------\n-\n-### Setup\n-\n-To set up your environment bundle for development and running tests:\n-\n-    $ script/bootstrap --local\n-\n-### Pull Requests\n-\n-For simple bug fixes and small tweaks, submit a pull request.\n-\n-### New calls\n-\n-See the [CONTRIBUTING][T] file for details on adding new calls. Each call\n-requires a client and backend implementation, tests, and documentation. The\n-CONTRIBUTING file includes templates and examples.\n-\n-[T]: https://github.com/github/gitrpc/tree/master/CONTRIBUTING.md\n+Hello AGAIN! AGAIN\n\\ No newline at end of file\n"

    expected.force_encoding("BINARY")
    assert_equal expected, @backend.native_patch_text(@commit6, @commit7)
  end

  def test_patch_text_with_bogus_args
    assert_raises(GitRPC::InvalidObject) do
      @backend.native_patch_text("NOTASHA", @commit2)
    end

    assert_raises(GitRPC::InvalidObject) do
      @backend.native_patch_text(@commit1, "NOTASHA")
    end
  end

  def test_patch_text_with_root_commit
    expected = "From fd75a9c501ed274b215457d4b22185f93f248358 Mon Sep 17 00:00:00 2001\nFrom: Some Author <someone@example.org>\nDate: Tue, 17 Jul 2012 20:21:53 -0700\nSubject: [PATCH] commit 1\n\n---\n README | 1 +\n 1 file changed, 1 insertion(+)\n create mode 100644 README\n\ndiff --git a/README b/README\nnew file mode 100644\nindex 0000000..63af512\n--- /dev/null\n+++ b/README\n@@ -0,0 +1 @@\n+Hello.\n\\ No newline at end of file\n"
    assert_equal expected, @backend.native_patch_text(@commit1)
  end
end
