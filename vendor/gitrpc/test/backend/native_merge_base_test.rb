# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class BackendNativeMergeBaseTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    setup_test_commits

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @commit1 = @fixture.add_empty_commit("commit 1")
    @fixture.command "git tag v0.1 HEAD"
    @tag_oid = @fixture.rev_parse("v0.1")

    @commit2 = @fixture.add_empty_commit("commit 2")
    @fixture.command "git branch some-branch HEAD"
    @branch_oid = @fixture.rev_parse("some-branch")

    @fixture.command "git tag -a -m 'v0.2 release' v0.2 HEAD"
    @annotated_oid = @fixture.rev_parse("v0.2")

    @commit3 = @fixture.add_empty_commit("commit 3")

    @fixture.command "git symbolic-ref HEAD refs/heads/noparents"
    @commit4 = @fixture.add_empty_commit("commit 4")

    @fixture.command "git symbolic-ref HEAD refs/heads/master"
    @master_oid = @fixture.rev_parse("master")

    @commit5 = @fixture.commit_files({"README" => "Hello."}, "commit 5")
  end

  def test_merge_base_from_tag
    assert_equal @commit1, @backend.native_merge_base("v0.1", "master")
  end

  def test_merge_base_from_branch
    assert_equal @commit2, @backend.native_merge_base("some-branch", "master")
  end

  def test_merge_base_from_oid
    assert_equal @commit2, @backend.native_merge_base(@branch_oid, @master_oid)
  end

  def test_merge_base_from_annotated_tag
    assert_equal @commit2, @backend.native_merge_base("v0.2", @master_oid)
  end

  def test_merge_base_with_bad_object
    e = nil

    begin
      @backend.native_merge_base("deadbeefcafe", "master")
    rescue GitRPC::InvalidObject => e
    end

    assert !e.nil?
  end

  def test_merge_base_with_blob_oid
    commit = @backend.read_commits([@commit5]).first
    tree = @backend.read_trees([commit["tree"]]).first

    e = nil

    begin
      @backend.native_merge_base(tree["entries"]["README"]["oid"], "master")
    rescue GitRPC::InvalidObject => e
    end

    assert !e.nil?
  end

  def test_merge_base_without_common_ancestors
    assert_nil @backend.native_merge_base("noparents", @master_oid)
  end
end
