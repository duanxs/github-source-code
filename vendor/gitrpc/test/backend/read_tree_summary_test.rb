# rubocop:disable Style/FrozenStringLiteralComment

require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"
require "securerandom"

# Tests the GitRPC::Backend::Files module methods
class BackendReadTreeSummaryTest < Minitest::Test
  include GitRPC::Util

  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    setup_test_files
    @commit_id = @fixture.rev_parse("HEAD")

    @backend = GitRPC::Backend.new(@fixture.path)

    @original_max_data_size = GitRPC::Backend.tree_summary_maximum_data_size
  end

  def teardown
    GitRPC::Backend.tree_summary_maximum_data_size = @original_max_data_size
    @fixture.teardown
  end

  def setup_test_files
    @png_data = "\211PNG\r\n\032\n\000\000\000\rIHDR\000\000\000@\000\000\000@\004\003\000\000\000XGl\355\000\000\000'PLTE\377\377\377555\n\n\n...***&&&!!!\034\034\034\030\030\030\024\024\024\021\021\021\r\r\r2222\352\267\375\000\000\000\003tRNS\000\231\231\323&\336m\000\000\000JIDATx^\355\3121\001\200 \000EA*X\301\nT\260\002\025\254@\005+P\201\nT0\224\333\333\336\342\302\362o\276\262MD\034\202\360\nB\025\204K\020\232 \334\202\320\005\341\021\204!\bS\020\226 \234\242\374\023\021\037\023\000|Q\332\035Y\253\000\000\000\000IEND\256B`\202"
    @png_data.force_encoding("BINARY") if @png_data.respond_to?(:force_encoding)
    @fixture.commit_files({
      "README.md" => "# Example Readme\nWith some test content",
      "User.rb" => "class User\nend\n",
      "Repository.rb" => "class Repository\nend\n",
      "js/testing.js" => "function test() {\n  console.log('testing')\n}",
      "heavy_minus_sign.png" => @png_data,
      "models/concerns/Billable.rb" => "module Billable\nend\n"
    }, "add first files")

    @fixture.commit_files({
      "README.md" => "# Example Readme\nWith some test content\nEven more"
    }, "add second files")
  end

  def test_read_tree_summary
    files = @backend.read_tree_summary(@commit_id)

    assert_equal 4, files.size
    readme = files.find { |f| f["name"] == "README.md" }
    refute_nil readme
    assert_equal 49, readme["size"]
    assert !readme["truncated"]
    assert !readme["binary"]
    assert "UTF-8", readme["encoding"]
    assert_equal Encoding::UTF_8, readme["content"].encoding
    assert_equal "5768c7f9f3eeae76626e978dd7a2f6076e5de0ee", readme["oid"]

    png = files.find { |f| f["name"] == "heavy_minus_sign.png" }
    assert_equal @png_data.size, png["size"]
    assert !png["truncated"]
    assert png["binary"]
    assert_nil png["encoding"]
    assert_equal Encoding::ASCII_8BIT, png["content"].encoding

    refute_nil files.find { |f| f["name"] == "User.rb" }
    refute_nil files.find { |f| f["name"] == "Repository.rb" }
  end

  def test_read_tree_summary_with_maximum_data_size
    GitRPC::Backend.tree_summary_maximum_data_size = 10
    files = @backend.read_tree_summary(@commit_id)

    readme = files.find { |f| f["name"] == "README.md" }
    assert_equal "# Example ", readme["content"]
    assert readme["truncated"]
    assert !readme["binary"]
    assert "UTF-8", readme["encoding"]
    assert_equal Encoding::UTF_8, readme["content"].encoding
    assert_equal 49, readme["size"]
  end

  def test_read_tree_summary_from_specific_version
    versions = @fixture.rev_list("HEAD", 2)

    files = @backend.read_tree_summary(versions[1])
    readme = files.find { |f| f["name"] == "README.md" }
    assert_equal "# Example Readme\nWith some test content", readme["content"]
    assert !readme["binary"]
    assert "UTF-8", readme["encoding"]
    assert_equal Encoding::UTF_8, readme["content"].encoding

    files = @backend.read_tree_summary(versions[0])
    readme = files.find { |f| f["name"] == "README.md" }
    assert_equal "# Example Readme\nWith some test content\nEven more",
      readme["content"]
    assert !readme["binary"]
    assert "UTF-8", readme["encoding"]
    assert_equal Encoding::UTF_8, readme["content"].encoding
  end

  def test_read_tree_summary_from_subtree
    files = @backend.read_tree_summary(@commit_id, path = "js")

    assert_equal 1, files.size

    testing_js = files.first
    assert_equal "testing.js", testing_js["name"]
    assert_equal "function test() {\n  console.log('testing')\n}",
      testing_js["content"]
    assert !testing_js["binary"]
    assert "UTF-8", testing_js["encoding"]
    assert_equal Encoding::UTF_8, testing_js["content"].encoding
  end

  def test_read_tree_summary_from_subtree_with_slash
    files = @backend.read_tree_summary(@commit_id, path = "/js")

    assert_equal 1, files.size

    testing_js = files.first
    assert_equal "testing.js", testing_js["name"]
    assert_equal "function test() {\n  console.log('testing')\n}",
      testing_js["content"]
  end

  def test_read_tree_summary_recursive
    files = @backend.read_tree_summary_recursive(@commit_id)

    assert_equal 6, files.size

    assert_equal "README.md", files[0]["name"]
    assert_equal "Repository.rb", files[1]["name"]
    assert_equal "User.rb", files[2]["name"]
    assert_equal "heavy_minus_sign.png", files[3]["name"]
    assert_equal "js/testing.js", files[4]["name"]
    assert_equal "models/concerns/Billable.rb", files[5]["name"]

    testing_js = files[4]
    assert_equal "function test() {\n  console.log('testing')\n}",
      testing_js["content"]
  end

  def test_raises_obj_missing_on_invalid_obj
    assert_raises GitRPC::ObjectMissing do
      @backend.read_tree_summary(SecureRandom.hex(20))
    end

    assert_raises GitRPC::ObjectMissing do
      @backend.read_tree_summary_recursive(SecureRandom.hex(20))
    end
  end
end
