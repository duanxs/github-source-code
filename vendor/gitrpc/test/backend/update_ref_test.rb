# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class BackendUpdateRefTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    setup_test_commits

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @commit1 = @fixture.commit_files({"README" => "Hello."}, "commit 1")
    @commit2 = @fixture.commit_files({"README" => "Goodbye."}, "commit 2")
    @commit3 = @fixture.commit_files({"README" => "Hi again."}, "commit 3")
  end

  def test_create_new_ref
    refute @backend.read_refs["refs/heads/foo"]

    @backend.update_ref("refs/heads/foo", @commit1, nil)

    assert_equal @commit1, @backend.read_refs["refs/heads/foo"]
  end

  def test_update_ref_batch
    stuff  = "create refs/heads/foo #{@commit1}\n"
    stuff += "create refs/heads/bar #{@commit2}\n"
    stuff += "update refs/heads/bas #{@commit3}\n"
    ret = @backend.update_ref_batch(stuff, reason: "batch ftw")
    assert_nil ret
    refs = @backend.read_refs
    assert_equal @commit1, refs["refs/heads/foo"]
    assert_equal @commit2, refs["refs/heads/bar"]
    assert_equal @commit3, refs["refs/heads/bas"]
    out = @fixture.command("git reflog refs/heads/foo")
    assert out =~ /batch ftw/

    stuff  = "update refs/heads/foo #{@commit2} #{@commit1}\n"
    stuff += "update refs/heads/bar #{@commit1} #{@commit3}\n" # oldvalue doesn't match
    stuff += "create refs/heads/bas #{@commit1}\n"
    assert_raises GitRPC::RefUpdateError do
      ret = @backend.update_ref_batch(stuff)
    end
    refs = @backend.read_refs
    assert_equal @commit1, refs["refs/heads/foo"]
    assert_equal @commit2, refs["refs/heads/bar"]
    assert_equal @commit3, refs["refs/heads/bas"]

    stuff  = "update refs/heads/foo #{@commit2}\n"
    ret = @backend.update_ref_batch(stuff, reason: "different name", committer_name: "Somebody", committer_email: "somebody@example.com", tz: "UTC")
    assert_nil ret
    refs = @backend.read_refs
    assert_equal @commit2, refs["refs/heads/foo"]
    out = @fixture.command("grep 'different name' logs/refs/heads/foo")
    assert out =~ /Somebody <somebody@example.com>/
    assert out =~ /\+0000/

    stuff  = "update refs/heads/foo #{@commit3}\n"
    ret = @backend.update_ref_batch(stuff, reason: "different timezone", committer_name: "Ice Station Zebra", committer_email: "icestationzebra@example.com", tz: "Antarctica/McMurdo")
    assert_nil ret
    refs = @backend.read_refs
    assert_equal @commit3, refs["refs/heads/foo"]
    out = @fixture.command("grep 'different timezone' logs/refs/heads/foo")
    assert out =~ /Ice Station Zebra <icestationzebra@example.com>/
    assert out !~ /\+0000/
  end

  def test_create_new_ref_with_old_value
    refute @backend.read_refs["refs/heads/foo"]

    @backend.update_ref("refs/heads/foo", @commit1, "0"*40)

    assert_equal @commit1, @backend.read_refs["refs/heads/foo"]
  end

  def test_create_new_ref_with_bad_old_value
    refute @backend.read_refs["refs/heads/foo"]

    assert_raises GitRPC::RefUpdateError do
      @backend.update_ref("refs/heads/foo", @commit1, @commit2)
    end

    refute @backend.read_refs["refs/heads/foo"]
  end

  def test_create_new_ref_with_date
    refute @backend.read_refs["refs/heads/foo"]

    @backend.update_ref("refs/heads/foo", @commit1, nil, committer_date: "@1000000000 +0000")

    assert_equal @commit1, @backend.read_refs["refs/heads/foo"]
    out = @fixture.command("cat logs/refs/heads/foo")
    assert out =~ /1000000000 \+0000/
  end

  def test_update_existing_ref
    @backend.update_ref("refs/heads/foo", @commit1, nil)
    assert_equal @commit1, @backend.read_refs["refs/heads/foo"]

    @backend.update_ref("refs/heads/foo", @commit2, nil)

    assert_equal @commit2, @backend.read_refs["refs/heads/foo"]
  end

  def test_update_existing_ref_with_old_value
    @backend.update_ref("refs/heads/foo", @commit1, nil)
    assert_equal @commit1, @backend.read_refs["refs/heads/foo"]

    @backend.update_ref("refs/heads/foo", @commit2, @commit1)

    assert_equal @commit2, @backend.read_refs["refs/heads/foo"]
  end

  def test_update_existing_ref_with_bad_old_value
    @backend.update_ref("refs/heads/foo", @commit1, nil)
    assert_equal @commit1, @backend.read_refs["refs/heads/foo"]

    assert_raises GitRPC::RefUpdateError do
      @backend.update_ref("refs/heads/foo", @commit2, @commit3)
    end

    assert_equal @commit1, @backend.read_refs["refs/heads/foo"]
  end

  def test_delete_existing_ref
    @backend.update_ref("refs/heads/foo", @commit1, nil)
    assert_equal @commit1, @backend.read_refs["refs/heads/foo"]

    @backend.delete_ref("refs/heads/foo", nil)

    refute @backend.read_refs["refs/heads/foo"]
  end

  def test_delete_existing_ref_with_old_value
    @backend.update_ref("refs/heads/foo", @commit1, nil)
    assert_equal @commit1, @backend.read_refs["refs/heads/foo"]

    @backend.delete_ref("refs/heads/foo", @commit1)

    refute @backend.read_refs["refs/heads/foo"]
  end

  def test_delete_existing_ref_with_bad_old_value
    @backend.update_ref("refs/heads/foo", @commit1, nil)
    assert_equal @commit1, @backend.read_refs["refs/heads/foo"]

    assert_raises GitRPC::RefUpdateError do
      @backend.delete_ref("refs/heads/foo", @commit2)
    end

    assert_equal @commit1, @backend.read_refs["refs/heads/foo"]
  end

  def test_create_new_ref_with_invalid_new_value
    assert_raises(GitRPC::InvalidFullOid) do
      @backend.update_ref("refs/heads/foo", "refs/heads/master", nil)
    end
  end

  def test_create_new_ref_with_invalid_old_value
    assert_raises(GitRPC::InvalidFullOid) do
      @backend.update_ref("refs/heads/foo", @commit1, "refs/heads/master")
    end
  end

  def test_delete_ref_with_invalid_old_value
    @backend.update_ref("refs/heads/foo", @commit1, nil)
    assert_equal @commit1, @backend.read_refs["refs/heads/foo"]

    assert_raises(GitRPC::InvalidFullOid) do
      @backend.delete_ref("refs/heads/foo", "refs/heads/master")
    end
  end

  def test_update_symbolic_ref_invalid_values
    ["HEAD2", "head", "config"].each do |input|
      assert_raises(GitRPC::InvalidReferenceName) do
        @backend.update_symbolic_ref("HEAD", input)
      end
    end

    ["HEAD2", "head", "config"].each do |input|
      assert_raises(GitRPC::InvalidReferenceName) do
        @backend.update_symbolic_ref(input, "refs/heads/master")
      end
    end
  end

  def test_update_symbolic_ref_with_max_ref_length
    maxlen = 255 # try to sanity-check GitHub.maximum_ref_length
    refname = "refs/heads/" + ("abcd" * 61)
    assert_equal maxlen, refname.length

    refute @backend.read_refs[refname]
    @backend.update_ref(refname, @commit1, nil)
    assert_equal @commit1, @backend.read_refs[refname]

    @backend.update_symbolic_ref("HEAD", refname)
  end

  def test_update_ref_fails_on_dgit_repo
    @fixture.setup
    res = @backend.update_ref("refs/heads/master", "0"*40, nil)
    assert_nil res, "update-ref should be ok on non-DGit repos"

    @backend.config_store("core.dgit", true)
    e = assert_raises GitRPC::RefUpdateError do
      res = @backend.update_ref("refs/heads/master", "0"*40, nil)
    end
    assert_match /not allowed in read-only mode/, e.to_s

    res = @backend.update_ref("refs/heads/master", "0"*40, nil, dgit_disabled: true)
    assert_nil res, "update-ref should work on a DGit repo with DGit disabled"
  end
end
