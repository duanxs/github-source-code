# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"
require "securerandom"

require "minitest/mock"

class BackendLogTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    setup_test_commits

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @past_time = Time.utc(2000)
    oid1_files = {
      "README" => "Bonjour.",
      ".mailmap" => "Homer Simpson <hsimpson@github.com> Homey <hs@springfield.us>"
    }

    @oid1 = @fixture.rugged_commit_files(oid1_files, "commit 1", time: @past_time)
    @oid2 = @fixture.rugged_commit_files({ "README" => "Hello." }, "commit 2", time: @past_time, parents: [@oid1])
    @oid3 = @fixture.rugged_commit_files({ "README" => "Hallo." }, "commit 3\n\nCo-AuThoReD-By: Arthur Schreiber <arthurschreiber@github.com>", time: @past_time, parents: [@oid2])
    @oid4 = @fixture.rugged_commit_files({ "README" => "Hi." }, "commit 4\n\nCo-authored-by: invalid", time: @past_time, parents: [@oid3], branch: "master")

    feature_commit1 = @fixture.rugged_commit_files({ "something" => "Content" }, "feature commit 1", parents: [@oid1], time: @past_time)
    @merge = @fixture.rugged_commit_files({ "something" => "Content" }, "feature merge", parents: [@oid4, feature_commit1], time: @past_time)

    oid = @fixture.rugged_commit_files({ "things" => "Hello." }, "homer-test", email: "hs@springfield.us", name: "Homey", time: @past_time, parents: [@merge])
    @mailmap_oid = @fixture.rugged_commit_files({ "more-things" => "Hello." }, "homer-test2", email: "hsimpson@github.com", name: "Homer Simpson", time: @past_time, parents: [oid])
  end

  def test_activity_summary
    summary = @backend.activity_summary("1999")
    assert_equal ["     4\tSome Author <someone@example.org>"], summary

    summary = @backend.activity_summary("2525")
    assert_equal [], summary
  end

  def test_activity_summary_coauthors
    summary = @backend.activity_summary_coauthors("1999")
    assert_equal [
      [["Some Author", "someone@example.org"], { authored_count: 4, committed_count: 4 }],
      [["Arthur Schreiber", "arthurschreiber@github.com"], { authored_count: 1, committed_count: 0 }]
    ], summary

    summary = @backend.activity_summary_coauthors("2525")
    assert_equal [], summary
  end

  def test_contributor_shortlog
    contributors = @backend.contributor_shortlog(@oid4)
    assert_equal "     4\tSome Author <someone@example.org>\n", contributors
  end

  def test_contributor_shortlog_no_merges
    contributors = @backend.contributor_shortlog(@merge)
    assert_equal "     6\tSome Author <someone@example.org>\n", contributors

    contributors = @backend.contributor_shortlog(@merge, ignore_merge_commits: true)
    assert_equal "     5\tSome Author <someone@example.org>\n", contributors
  end

  def test_contributor_shortlog_raises_invalid_oid
    assert_raises(GitRPC::UnsafeArgument) do
      @backend.contributor_shortlog("--output=/usr/bin/git")
    end
  end

  def test_contributor_log
    contributors = @backend.contributor_log(@oid4)
    assert_equal({ { name: "Some Author", email: "someone@example.org" } => 4 }, contributors)
  end

  def test_contributor_log_no_merges
    contributors = @backend.contributor_log(@merge)
    assert_equal({ { name: "Some Author", email: "someone@example.org" } => 6 }, contributors)

    contributors = @backend.contributor_log(@merge, ignore_merge_commits: true)
    assert_equal({ { name: "Some Author", email: "someone@example.org" } => 5 }, contributors)
  end

  def test_contributor_log_bad_email
    output = <<~OUT
      Anonymous Person <>
      Flanders <flanders@github.com>
       <silly@silly.com>
      Lisa Simpson <lisa@github.com>
    OUT

    contributors = @backend.stub(:spawn_git, { "out" => output, "ok" => true }) do
      @backend.contributor_log("f" * 40)
    end

    expected = {
      { name: "Flanders", email: "flanders@github.com" } => 1,
      { name: "Lisa Simpson", email: "lisa@github.com"} => 1
    }
    assert_equal(expected, contributors)
  end

  def test_mailmap_respected
    contributors = @backend.contributor_log(@mailmap_oid, mailmap: false)
    expected = {
      { name: "Some Author", email: "someone@example.org" } => 6,
      { name: "Homer Simpson", email: "hsimpson@github.com" } => 1,
      { name: "Homey", email: "hs@springfield.us" } => 1
    }
    assert_equal expected, contributors

    contributors = @backend.contributor_log(@mailmap_oid, mailmap: true)
    expected = {
      { name: "Some Author", email: "someone@example.org" } => 6,
      { name: "Homer Simpson", email: "hsimpson@github.com" } => 2
    }
    assert_equal expected, contributors
  end

  def test_repo_graph_log_times
    times = @backend.repo_graph_log_times(@oid4)
    assert_equal [
      "Sat, 1 Jan 2000 00:00:00 +0000",
      "Sat, 1 Jan 2000 00:00:00 +0000",
      "Sat, 1 Jan 2000 00:00:00 +0000",
      "Sat, 1 Jan 2000 00:00:00 +0000"
    ], times
  end

  def test_repo_graph_log_times_raises_invalid_oid
    assert_raises(GitRPC::UnsafeArgument) do
      @backend.repo_graph_log_times("--output=/usr/bin/git")
    end
  end

  def test_commit_stats_log
    commit_stats_log = @backend.commit_stats_log(@oid4)
    expected = [
      "#{@oid4}\n 1 file changed, 1 insertion(+), 1 deletion(-)\n",
      "#{@oid3}\n 1 file changed, 1 insertion(+), 1 deletion(-)\n",
      "#{@oid2}\n 1 file changed, 1 insertion(+), 1 deletion(-)\n",
      "#{@oid1}\n 2 files changed, 2 insertions(+)\n"
    ]
    assert_equal expected, commit_stats_log
  end

  def test_commit_stats_log_raises_invalid_commitish
    assert_raises(GitRPC::InvalidCommitish) do
      @backend.commit_stats_log("--output=/usr/bin/git")
    end
  end

  def test_paged_commits_log
    commits = @backend.paged_commits_log(@oid4)

    assert_equal [@oid4, @oid3, @oid2, @oid1], commits

    commits = @backend.paged_commits_log(@oid1)
    assert_equal [@oid1], commits

    commits = @backend.paged_commits_log(@oid4, path: "nonexistent")
    assert_equal [], commits
    commits = @backend.paged_commits_log(@oid4, path: "README")
    assert_equal [@oid4, @oid3, @oid2, @oid1], commits

    commits = @backend.paged_commits_log(@oid4, author_emails: ["nobody@example.org"])
    assert_equal [], commits
    commits = @backend.paged_commits_log(@oid4, author_emails: ["someone@example.org"])
    assert_equal [@oid4, @oid3, @oid2, @oid1], commits

    commits = @backend.paged_commits_log(@oid4, since_time: "2525")
    assert_equal [], commits
    commits = @backend.paged_commits_log(@oid4, since_time: "1999")
    assert_equal [@oid4, @oid3, @oid2, @oid1], commits

    commits = @backend.paged_commits_log(@oid4, until_time: "1999")
    assert_equal [], commits
    commits = @backend.paged_commits_log(@oid4, until_time: "2525")
    assert_equal [@oid4, @oid3, @oid2, @oid1], commits

    commits = @backend.paged_commits_log(@oid4, skip: 1)
    assert_equal [@oid3, @oid2, @oid1], commits

    commits = @backend.paged_commits_log(@oid4, max_count: 1)
    assert_equal [@oid4], commits

    assert_raises(GitRPC::UnsafeArgument) do
      @backend.paged_commits_log("--output=/usr/bin/git")
    end
  end

  def test_log_last_commit_oneline
    oneline = @backend.log_last_commit_oneline
    assert_equal "#{@oid4[0, 7]} commit 4", oneline
  end
end
