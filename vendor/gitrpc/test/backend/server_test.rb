# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class BackendServerTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def test_host_encrypted?
    encrypted_lsblk_output = <<-END
MOUNTPOINT="" TYPE="disk"
MOUNTPOINT="/boot" TYPE="part"
MOUNTPOINT="" TYPE="part"
MOUNTPOINT="" TYPE="lvm"
MOUNTPOINT="/data/docker" TYPE="lvm"
MOUNTPOINT="" TYPE="lvm"
MOUNTPOINT="/data/lariatcache" TYPE="crypt"
MOUNTPOINT="/" TYPE="lvm"
MOUNTPOINT="" TYPE="lvm"
MOUNTPOINT="/data/repositories" TYPE="crypt"
MOUNTPOINT="" TYPE="part"
END

    unencrypted_lsblk_output = <<-END
MOUNTPOINT="" TYPE="disk"
MOUNTPOINT="/boot" TYPE="part"
MOUNTPOINT="" TYPE="part"
MOUNTPOINT="" TYPE="lvm"
MOUNTPOINT="/data/docker" TYPE="lvm"
MOUNTPOINT="/data/lariatcache" TYPE="lvm"
MOUNTPOINT="/" TYPE="lvm"
MOUNTPOINT="/data/repositories" TYPE="lvm"
MOUNTPOINT="" TYPE="part"
END

    non_dfs_lsblk_output = <<-END
MOUNTPOINT="" TYPE="disk"
MOUNTPOINT="/boot" TYPE="part"
MOUNTPOINT="" TYPE="part"
MOUNTPOINT="" TYPE="lvm"
MOUNTPOINT="/" TYPE="lvm"
MOUNTPOINT="" TYPE="part"
END

    fakeres = {"out" => encrypted_lsblk_output, "err" => "", "status" => 0, "ok" => true}
    assert_equal true, @backend.stub(:spawn, fakeres) { @backend.host_encrypted? }

    fakeres = {"out" => unencrypted_lsblk_output, "err" => "", "status" => 0, "ok" => true}
    assert_equal false, @backend.stub(:spawn, fakeres) { @backend.host_encrypted? }

    fakeres = {"out" => non_dfs_lsblk_output, "err" => "", "status" => 0, "ok" => true}
    assert_equal false, @backend.stub(:spawn, fakeres) { @backend.host_encrypted? }

    fakeres = {"out" => "", "err" => "barf", "status" => 1, "ok" => false, "argv" => []}
    assert_raises(GitRPC::CommandFailed) do
      @backend.stub(:spawn, fakeres) { @backend.host_encrypted? }
    end
  end
end
