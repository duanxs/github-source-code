# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

# Tests the GitRPC::Backend::Disk module methods
class BackendDiskTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
  end

  def test_init
    assert !File.exist?(@fixture.path)
    @backend.init
    assert File.exist?(@fixture.path)
    assert File.exist?("#{@fixture.path}/config")
  end

  def test_init_idempotent
    @fixture.setup
    assert File.exist?(@fixture.path)
    @backend.init
    @backend.init
    assert File.exist?(@fixture.path)
  end

  def test_init_non_destructive
    @fixture.setup
    @fixture.add_empty_commit("blah")
    head = @fixture.rev_parse("HEAD")
    @backend.init
    assert_equal head, @fixture.rev_parse("HEAD")
  end

  def test_remove
    @backend.init
    assert File.exist?(@fixture.path)
    @backend.remove
    assert !File.exist?(@fixture.path)
  end

  def test_remove_nonexistent
    assert !File.exist?(@fixture.path)
    @backend.remove
    assert !File.exist?(@fixture.path)
  end

  def test_exist
    assert !@backend.exist?
    @fixture.setup
    assert @backend.exist?
  end

  def test_ensure_dir
    @fixture.setup
    refute File.exist?("#{@fixture.path}/foo/bar")
    @backend.ensure_dir("foo/bar")
    assert File.exist?("#{@fixture.path}/foo/bar")

    assert_raises GitRPC::IllegalPath do
      @backend.ensure_dir("/tmp")
    end
    assert_raises GitRPC::IllegalPath do
      @backend.ensure_dir("#{@fixture.path}/../foo")
    end
  end

  def test_ensure_initialized
    refute File.exist?(@fixture.path)
    Dir.mktmpdir do |hooks_dir|
      @backend.ensure_initialized hooks_template: hooks_dir, files: {"info/nwo" => "example123"}

      assert_instance_of Rugged::Repository, Rugged::Repository.discover(@fixture_path), "rugged can init a repo object"
      assert_equal hooks_dir, File.readlink(File.join(@fixture.path, "hooks")), "hooks link target"
      assert_equal "example123", File.read(File.join(@fixture.path, "info/nwo")), "requested file"
    end
  end

  def test_ensure_initialized_is_not_destructive
    @fixture.setup
    @fixture.add_empty_commit("blah")
    head = @fixture.rev_parse("HEAD")

    Dir.mktmpdir do |hooks_dir|
      @backend.ensure_initialized hooks_template: hooks_dir, files: {"info/nwo" => "example123"}

      assert_instance_of Rugged::Repository, Rugged::Repository.discover(@fixture_path), "rugged can init a repo object"
      assert_equal hooks_dir, File.readlink(File.join(@fixture.path, "hooks")), "hooks link target"
      assert_equal "example123", File.read(File.join(@fixture.path, "info/nwo")), "requested file"

      assert_equal head, @fixture.rev_parse("HEAD")
    end
  end

  def test_ensure_initialized_raises_if_hooks_template_is_missing
    hooks_dir = Dir.mktmpdir
    Dir.delete hooks_dir
    refute Dir.exist?(hooks_dir)

    assert_raises(GitRPC::SystemError) do
      @backend.ensure_initialized hooks_template: hooks_dir
    end
  end

  def test_ensure_initialized_works_if_hooks_is_already_a_link
    @fixture.setup
    Dir.mktmpdir do |hooks_dir|
      fixture_hooks = File.join(@fixture.path, "hooks")
      FileUtils.remove_entry_secure fixture_hooks
      File.symlink File.dirname(hooks_dir), fixture_hooks

      @backend.ensure_initialized hooks_template: hooks_dir

      assert_equal hooks_dir, File.readlink(fixture_hooks), "hooks should now be a symlink"
    end
  end

  def test_ensure_initialized_works_if_hooks_were_missing
    @fixture.setup
    Dir.mktmpdir do |hooks_dir|
      fixture_hooks = File.join(@fixture.path, "hooks")
      FileUtils.remove_entry_secure fixture_hooks

      @backend.ensure_initialized hooks_template: hooks_dir

      assert_equal hooks_dir, File.readlink(fixture_hooks), "hooks should now be a symlink"
    end
  end

  def test_fs_read
    @fixture.setup
    File.open("#{@backend.path}/a-file", "wb") { |f| f.write("your great data\n") }
    assert_equal "your great data\n", @backend.fs_read("a-file")
  end

  def test_fs_read_no_exist
    @fixture.setup
    assert_raises(GitRPC::SystemError) { @backend.fs_read("nope/sorry.txt") }
  end

  def test_fs_read_invalid_repository
    assert_raises(GitRPC::InvalidRepository) { @backend.fs_read("nope/sorry.txt") }
  end

  def test_fs_move
    @fixture.setup
    @backend.fs_write("old-file.txt", "data\n")
    @backend.fs_move("old-file.txt", "new-file.txt")
    assert_equal "data\n", File.read("#{@fixture.path}/new-file.txt")
    assert !File.exist?("#{@fixture.path}/old-file.txt")
  end

  def test_fs_move_directory
    @fixture.setup
    @backend.fs_write("old-dir/file.txt", "data\n")
    @backend.fs_move("old-dir", "new-dir")
    assert_equal "data\n", File.read("#{@fixture.path}/new-dir/file.txt")
    assert !File.exist?("#{@fixture.path}/old-dir")
  end

  def test_fs_move_into_directory
    @fixture.setup
    @backend.fs_write("file.txt", "data\n")
    @backend.fs_move("file.txt", "new-dir/file.txt")
    assert_equal "data\n", File.read("#{@fixture.path}/new-dir/file.txt")
    assert !File.exist?("#{@fixture.path}/file.txt")
  end

  def test_fs_write
    @fixture.setup
    @backend.fs_write("new-file.txt", "new great data\n")
    assert_equal "new great data\n", File.read("#{@fixture.path}/new-file.txt")
  end

  def test_truncate_file
    @fixture.setup
    @backend.fs_write("truncated-file.txt")
    assert_equal "", File.read("#{@fixture.path}/truncated-file.txt")
  end

  def test_fs_write_under_missing_directory
    @fixture.setup
    @backend.fs_write("missing-dir/ok.txt", "new great data in subdir\n")
    assert_equal "new great data in subdir\n", File.read("#{@fixture.path}/missing-dir/ok.txt")
  end

  def test_write_over_existing_file
    @fixture.setup
    File.open("#{@backend.path}/existing.txt", "wb") { |f| f.write("existing great data\n") }
    @backend.fs_write("existing.txt", "new existing great data\n")
    assert_equal "new existing great data\n", @backend.fs_read("existing.txt")
  end

  def test_write_outside_of_repository
    @fixture.setup
    assert_raises(GitRPC::IllegalPath) do
      @backend.fs_write("../illegal-file.txt", "cant write it")
    end
  end

  def test_write_over_directory
    @fixture.setup
    Dir.mkdir("#{@backend.path}/some-dir")
    assert_raises(GitRPC::SystemError) do
      @backend.fs_write("some-dir", "cant write it")
    end
  end

  def test_fs_write_invalid_repository
    assert_raises(GitRPC::InvalidRepository) { @backend.fs_write("sorry.txt") }
  end

  def test_fs_delete
    @fixture.setup
    File.open("#{@fixture.path}/unneeded.txt", "wb") { |f| f.write("your deleted data\n") }
    @backend.fs_delete("unneeded.txt")
    assert !File.exist?("#{@fixture.path}/unneeded.txt")
  end

  def test_fs_delete_non_existing_file
    @fixture.setup
    assert !File.exist?("#{@fixture.path}/unneeded.txt")
    @backend.fs_delete("unneeded.txt")
  end

  def test_fs_delete_directory
    @fixture.setup
    Dir.mkdir("#{@fixture.path}/some-dir")
    @backend.fs_delete("some-dir")
    assert !File.exist?("#{@fixture.path}/some-dir")
  end

  def test_fs_delete_whole_repo
    @fixture.setup
    @backend.fs_delete(".")
    assert !File.exist?(@fixture.path)
  end

  def test_fs_delete_outside_of_repository
    @fixture.setup
    assert_raises(GitRPC::IllegalPath) do
      @backend.fs_delete("../illegal-file.txt")
    end
  end

  def test_fs_delete_invalid_repository
    assert_raises(GitRPC::InvalidRepository) { @backend.fs_delete("config") }
  end

  def test_fs_exist
    @fixture.setup
    assert @backend.fs_exist?("config")
    assert !@backend.fs_exist?("some-path")

    File.open("#{@backend.path}/some-path", "wb") {}
    assert @backend.fs_exist?("some-path")
  end

  def test_fs_exist_outside_of_repository
    @fixture.setup
    assert_raises(GitRPC::IllegalPath) { @backend.fs_exist?("../../stuff") }
    assert_raises(GitRPC::IllegalPath) { @backend.fs_exist?("/etc/passwd") }
    assert_raises(GitRPC::IllegalPath) { @backend.fs_exist?("www/../../etc") }
  end

  def test_fs_exist_invalid_repository
    assert_raises(GitRPC::InvalidRepository) { @backend.fs_exist?("config") }
  end

  def test_disk_free_space
    GitRPC.repository_root = "/fakepath"
    fake_df_output = <<-END
Filesystem                            1M-blocks    Used Available Use% Mounted on
/dev/mapper/github--dfs--6a865af-repo   7499901 5235044   1886863  74% /data/repositories
END
    fakeres = {"out" => fake_df_output, "err" => "", "status" => 0, "ok" => true}
    free_space_mb, total_space_mb = @backend.stub(:spawn, fakeres) do
      @backend.disk_free_space
    end
    assert_equal 1886863, free_space_mb
    assert_equal 7121907, total_space_mb
  end

  def test_ensure_nwo_sanitized
    @backend.ensure_initialized files: {"info/nwo" => "example123\n"}
    assert_equal "example123", @backend.nwo
  end
end
