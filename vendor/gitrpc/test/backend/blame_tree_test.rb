# frozen_string_literal: true
require_relative "../setup"
require "gitrpc"
require "repository_fixture"

class BackendBlameTreeTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    FileUtils.cp_r(Dir.glob("#{File.expand_path("../../examples/defunkt_facebox.git/", __FILE__)}/*"), @fixture.path)
    @backend = GitRPC::Backend.new(@fixture.path)
    @head = @backend.read_head_oid
    @rugged = Rugged::Repository.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
  end

  def test_blame_tree_recursive_works
    assert_equal({"facebox.js"=>"4bf7a39e8c4ec54f8b4cd594a3616d69004aba69",
                   "releases/.gitignore"=>"fc18e486123da27cbf82dbb4b6d1ce462f301655",
                   "facebox.css"=>"e4e7aac29434bf8e038b3e2a9fa3ceb9d9465609",
                   "index.html"=>"e55ccdbcefe5c897946805c77dcfefe212f19d14",
                   "README.txt"=>"2525b57620f58af55465ef049a189d021924c232",
                   ".gitignore"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "b.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "bl.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "br.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "build_tar.sh"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "closelabel.gif"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "faceplant.css"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "jquery.js"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "loading.gif"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "logo.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "remote.html"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "screenshots/.DS_Store"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "screenshots/error.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "screenshots/error_small.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "screenshots/photo.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "screenshots/photo_small.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "screenshots/preview.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "screenshots/preview_small.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "screenshots/success.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "screenshots/success_small.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "shadow.gif"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "stairs.jpg"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "test.html"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "tl.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "tr.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca"},
                  @backend.blame_tree(@head, nil, true))
  end

  def test_blame_tree_nonrecursive_works
    assert_equal({"facebox.js"=>"4bf7a39e8c4ec54f8b4cd594a3616d69004aba69",
                   "releases"=>"fc18e486123da27cbf82dbb4b6d1ce462f301655",
                   "facebox.css"=>"e4e7aac29434bf8e038b3e2a9fa3ceb9d9465609",
                   "index.html"=>"e55ccdbcefe5c897946805c77dcfefe212f19d14",
                   "README.txt"=>"2525b57620f58af55465ef049a189d021924c232",
                   ".gitignore"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "b.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "bl.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "br.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "build_tar.sh"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "closelabel.gif"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "faceplant.css"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "jquery.js"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "loading.gif"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "logo.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "remote.html"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "screenshots"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "shadow.gif"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "stairs.jpg"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "test.html"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "tl.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca",
                   "tr.png"=>"75822fc6473f2375d73a7ea5df7bcef6b7de03ca"},
                  @backend.blame_tree(@head, nil, false))
  end

  def test_blame_tree_subdir
    assert_equal({"releases/.gitignore"=>"fc18e486123da27cbf82dbb4b6d1ce462f301655"},
                 @backend.blame_tree(@head, "releases", false))
  end

  def test_blame_tree_raw
    # Pretend the client had cached this
    cached = "bf64466664441adcfdf8ef58ac525c418c93659c"

    assert_equal({"facebox.js"=>"4bf7a39e8c4ec54f8b4cd594a3616d69004aba69",
                   "releases/.gitignore"=>"fc18e486123da27cbf82dbb4b6d1ce462f301655",
                   "facebox.css"=>"e4e7aac29434bf8e038b3e2a9fa3ceb9d9465609",
                   "index.html"=>"e55ccdbcefe5c897946805c77dcfefe212f19d14",
                   "README.txt"=>"2525b57620f58af55465ef049a189d021924c232",
                   ".gitignore"=>"^98e0927d71a31ea2eb53cc505f8cfe639ff5d230",
                   "b.png"=>"^98e0927d71a31ea2eb53cc505f8cfe639ff5d230",
                   "bl.png"=>"^98e0927d71a31ea2eb53cc505f8cfe639ff5d230",
                   "br.png"=>"^98e0927d71a31ea2eb53cc505f8cfe639ff5d230",
                   "build_tar.sh"=>"^98e0927d71a31ea2eb53cc505f8cfe639ff5d230",
                   "closelabel.gif"=>"^98e0927d71a31ea2eb53cc505f8cfe639ff5d230",
                   "faceplant.css"=>"^98e0927d71a31ea2eb53cc505f8cfe639ff5d230",
                   "jquery.js"=>"^98e0927d71a31ea2eb53cc505f8cfe639ff5d230",
                   "loading.gif"=>"^98e0927d71a31ea2eb53cc505f8cfe639ff5d230",
                   "logo.png"=>"^98e0927d71a31ea2eb53cc505f8cfe639ff5d230",
                   "remote.html"=>"^98e0927d71a31ea2eb53cc505f8cfe639ff5d230",
                   "screenshots/.DS_Store"=>"^98e0927d71a31ea2eb53cc505f8cfe639ff5d230",
                   "screenshots/error.png"=>"^98e0927d71a31ea2eb53cc505f8cfe639ff5d230",
                   "screenshots/error_small.png"=>"^98e0927d71a31ea2eb53cc505f8cfe639ff5d230",
                   "screenshots/photo.png"=>"^98e0927d71a31ea2eb53cc505f8cfe639ff5d230",
                   "screenshots/photo_small.png"=>"^98e0927d71a31ea2eb53cc505f8cfe639ff5d230",
                   "screenshots/preview.png"=>"^98e0927d71a31ea2eb53cc505f8cfe639ff5d230",
                   "screenshots/preview_small.png"=>"^98e0927d71a31ea2eb53cc505f8cfe639ff5d230",
                   "screenshots/success.png"=>"^98e0927d71a31ea2eb53cc505f8cfe639ff5d230",
                   "screenshots/success_small.png"=>"^98e0927d71a31ea2eb53cc505f8cfe639ff5d230",
                   "shadow.gif"=>"^98e0927d71a31ea2eb53cc505f8cfe639ff5d230",
                   "stairs.jpg"=>"^98e0927d71a31ea2eb53cc505f8cfe639ff5d230",
                   "test.html"=>"^98e0927d71a31ea2eb53cc505f8cfe639ff5d230",
                   "tl.png"=>"^98e0927d71a31ea2eb53cc505f8cfe639ff5d230",
                   "tr.png"=>"^98e0927d71a31ea2eb53cc505f8cfe639ff5d230"},
                  @backend.blame_tree_raw(@head, nil, true, [cached]))
  end

  def test_blame_tree_raw_rejects_non_sha1s
    assert_raises(GitRPC::InvalidOid) do
      @backend.blame_tree_raw("notsha1")
    end
  end

  def test_blame_cache_detect_no_cache
    blame_cache = GitRPC::Backend::BlameTreeCache.new(@fixture.path)

    refute blame_cache.has_cache?
    assert_empty blame_cache.cached_commits
  end

  def test_blame_cache_fill
    blame_cache = GitRPC::Backend::BlameTreeCache.new(@fixture.path)

    refute blame_cache.has_cache?
    assert_empty blame_cache.cached_commits

    @backend.blame_tree_fill_cache(@head, 10)

    assert blame_cache.has_cache?
    assert_equal ["16c6542a0475179215a1667cd505960f1913dc3d"], blame_cache.cached_commits

    assert_raises(GitRPC::Error) { @backend.blame_tree_fill_cache(@head, 1000) }
  end

  def test_cache_per_tree
    expected = {"/" => {"facebox.js"=>["07bbcd71ab13bf3438826658d03cde9dfa298b0e",
                                       "2b862cbcbe8c8ead1549215240cb53ce272f73f9"],
                        "releases"=>["195f7a0188f5b48bd95ada6c117fbcd469c4a412",
                                     "75822fc6473f2375d73a7ea5df7bcef6b7de03ca"],
                        "facebox.css"=>["0ba0089c00762fd46e36446577388797b83fe676",
                                        "bf64466664441adcfdf8ef58ac525c418c93659c"],
                        "index.html"=>["aabe864514a0bb95dea0384f13d90b047bef807a",
                                       "d4f1c62e26be612f81508c8e0166c1e6b025879e"],
                        "README.txt"=>["128805ed2a997ad6f16f6c3acd742a9a088c1f96",
                                       "75822fc6473f2375d73a7ea5df7bcef6b7de03ca"],
                        ".gitignore"=>["e43b0f988953ae3a84b00331d0ccf5f7d51cb3cf",
                                       "75822fc6473f2375d73a7ea5df7bcef6b7de03ca"],
                        "b.png"=>["f184e6269b343014f58694093b55558dd5dde193",
                                  "75822fc6473f2375d73a7ea5df7bcef6b7de03ca"],
                        "bl.png"=>["f6271859d51654b6fb2719df5fe192c8398ecefc",
                                   "75822fc6473f2375d73a7ea5df7bcef6b7de03ca"],
                        "br.png"=>["31f204fc451cd9dd5cfdadfad2d86ed0e1104882",
                                   "75822fc6473f2375d73a7ea5df7bcef6b7de03ca"],
                        "build_tar.sh"=>["08f6f1fce2f6a02dcb15b6c66244470794587bb0",
                                         "75822fc6473f2375d73a7ea5df7bcef6b7de03ca"],
                        "closelabel.gif"=>["87b4f8bd699386e3a6fcc2e50d7c61bfc4aabb8d",
                                           "75822fc6473f2375d73a7ea5df7bcef6b7de03ca"],
                        "faceplant.css"=>["dc61a86c3f342b930f0a0447cae33fee812e27d3",
                                          "75822fc6473f2375d73a7ea5df7bcef6b7de03ca"],
                        "jquery.js"=>["ebe02bdd357c337e0e817fcbce2a034a54a13287",
                                      "75822fc6473f2375d73a7ea5df7bcef6b7de03ca"],
                        "loading.gif"=>["f864d5fd38b7466c76b5a36dc0e3e9455c0126e2",
                                        "75822fc6473f2375d73a7ea5df7bcef6b7de03ca"],
                        "logo.png"=>["e41cfe5c654e8e05ad46f15af1c462a1360e9764",
                                     "75822fc6473f2375d73a7ea5df7bcef6b7de03ca"],
                        "remote.html"=>["98d3e92373d1bc541e7f516e5e73b645a991ddc2",
                                        "75822fc6473f2375d73a7ea5df7bcef6b7de03ca"],
                        "screenshots"=>["bbf747873075ac28667d246491ffdefbd314fe4f",
                                        "75822fc6473f2375d73a7ea5df7bcef6b7de03ca"],
                        "shadow.gif"=>["e58b35b362ce5347bb5064e91a3bf8e4fed4f6ef",
                                       "75822fc6473f2375d73a7ea5df7bcef6b7de03ca"],
                        "stairs.jpg"=>["63459bb418f5f6d896a8eb925c01f45024933ed6",
                                       "75822fc6473f2375d73a7ea5df7bcef6b7de03ca"],
                        "test.html"=>["38756c5ecc3ce53b8da9555a497bd9cc20b1b1c9",
                                      "75822fc6473f2375d73a7ea5df7bcef6b7de03ca"],
                        "tl.png"=>["d99c8f6c6eaa12d7b49a20f41f08a5006f3ea8b7",
                                   "75822fc6473f2375d73a7ea5df7bcef6b7de03ca"],
                        "tr.png"=>["e99b6ec8310e859fd27519694f04e1babf2ab2c4",
                                   "75822fc6473f2375d73a7ea5df7bcef6b7de03ca"]},
                "releases" => { "facebox-1.0.tar.gz"=>["5de36bb59fdb5bce8749e72aeab0baace6658e7c",
                                                       "75822fc6473f2375d73a7ea5df7bcef6b7de03ca"]},
                "screenshots" => {".DS_Store"=>["7d8a1d70403b6701c37d9b97f235e912eef6234f",
                                                "75822fc6473f2375d73a7ea5df7bcef6b7de03ca"],
                                  "error.png"=>["c2165a01e6d1be1016965fe67cbe49fc32957541",
                                                "75822fc6473f2375d73a7ea5df7bcef6b7de03ca"],
                                  "error_small.png"=>["ce96ac453c19c66e3200ed0dbd75bd13a9fb4c72",
                                                      "75822fc6473f2375d73a7ea5df7bcef6b7de03ca"],
                                  "photo.png"=>["837cfbd897b1e06734c58081e827f46b22b1e765",
                                                "75822fc6473f2375d73a7ea5df7bcef6b7de03ca"],
                                  "photo_small.png"=>["933df8b638d4ada6737daf54e02e70ce37bd996f",
                                                      "75822fc6473f2375d73a7ea5df7bcef6b7de03ca"],
                                  "preview.png"=>["e04c5f64000c9cf382da03ba644b683ff30583da",
                                                  "75822fc6473f2375d73a7ea5df7bcef6b7de03ca"],
                                  "preview_small.png"=>["4deb6d708a105a3d28f52e80ee76549bdc70fa15",
                                                        "75822fc6473f2375d73a7ea5df7bcef6b7de03ca"],
                                  "success.png"=>["b547e7ad945666d70f6b73bd3eb171e14bbb8b9c",
                                                  "75822fc6473f2375d73a7ea5df7bcef6b7de03ca"],
                                  "success_small.png"=>["dfff54f6443f01a7b1fed54c8c05cc725880640e",
                                                        "75822fc6473f2375d73a7ea5df7bcef6b7de03ca"],
                                 },
               }

    to_cache = "bf64466664441adcfdf8ef58ac525c418c93659c"
    actual = GitRPC::Backend::BlameTreeCache.cache_per_tree(
      Rugged::Commit.lookup(@rugged, to_cache),
      @backend.blame_tree_raw(to_cache, nil, true, nil, true))

    assert_equal expected, actual
  end

  def test_blame_cache
    to_cache = "bf64466664441adcfdf8ef58ac525c418c93659c"

    pertree = GitRPC::Backend::BlameTreeCache.cache_per_tree(
      Rugged::Commit.lookup(@rugged, to_cache),
      @backend.blame_tree_raw(to_cache, nil, true, nil, true))

    @backend.blame_tree_fill_cache_at(to_cache)
    cache = GitRPC::Backend::BlameTreeCache.new(@fixture.path)

    assert cache.has_cache?
    assert_equal ["bf64466664441adcfdf8ef58ac525c418c93659c"], cache.cached_commits
    pertree.each do |dir, data|
      assert_equal data, cache.cache_for(to_cache, dir == "/" ? "" : dir)
    end

    assert_equal(@backend.blame_tree_raw(@head, nil, false, nil),
                 @backend.blame_tree(@head, nil, false, true))
    assert_equal(@backend.blame_tree_raw(@head, nil, true, nil),
                 @backend.blame_tree(@head, nil, true, false))
    assert_equal(@backend.blame_tree_raw(@head, "screenshots", false, nil),
                 @backend.blame_tree(@head, "screenshots", true, true))
    assert_equal(@backend.blame_tree(@head, "screenshots", false, false),
                 @backend.blame_tree(@head, "screenshots", false, true))
  end

  def test_blame_at_cache_point
    @backend.blame_tree_fill_cache_at(@head)
    assert_equal(@backend.blame_tree(@head, nil, false, false),
                 @backend.blame_tree(@head, nil, false, true))
    assert_equal(@backend.blame_tree(@head, "screenshots", false, false),
                 @backend.blame_tree(@head, "screenshots", false, true))
    assert_equal(@backend.blame_tree(@head, "releases", false, false),
                 @backend.blame_tree(@head, "releases", false, true))
  end

  def test_cache_point_is_descendant
    parent = Rugged::Commit.lookup(@rugged, @head).parent_ids[0]
    @backend.blame_tree_fill_cache_at(@head)
    assert_equal(@backend.blame_tree(parent, nil, false, false),
                 @backend.blame_tree(parent, nil, false, true))
  end

  def test_write_into_network
    files_to_copy = Dir.glob("#{File.expand_path("../../examples/defunkt_facebox.git/", __FILE__)}/*")
    Dir.mktmpdir do |dir|
      # Create the networked layout. We don't need to link them as we don't
      # check that thoroughly
      nwrepo = Rugged::Repository.init_at(File.join(dir, "network.git"), true)
      repo = Rugged::Repository.init_at(File.join(dir, "123.git"), true)

      # Copy some data so we have cache data to look at
      FileUtils.cp_r(files_to_copy, repo.path)
      backend = GitRPC::Backend.new(repo.path)
      backend.blame_tree_fill_cache_at(repo.head.target.oid)

      refute Dir.exist?(File.join(repo.path, "blame-tree-cache"))
      assert Dir.exist?(File.join(nwrepo.path, "blame-tree-cache"))
    end

  end
end
