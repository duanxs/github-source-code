# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class BackendNativeHistoryTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
    @backend = nil
  end

  def test_native_list_revision_history_raises_invalid_oid
    assert_raises(GitRPC::InvalidCommitish) do
      @backend.native_list_revision_history("--output=/usr/bin/git")
    end
  end
end
