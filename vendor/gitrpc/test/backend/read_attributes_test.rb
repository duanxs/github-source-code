# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class BackendReadAttributesTest < Minitest::Test
  include GitRPC::Util

  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @backend = GitRPC::Backend.new(@fixture.path)

    setup_test_commits
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @commit1 = @fixture.commit_files({
      ".gitattributes"        => "README.md linguist-documentation=false",
      "README.md"             => "hello",
      "foo.rb"                => "bar",
      "subdir/.gitattributes" => "*.rb linguist-language=Java",
      "subdir/test.rb"        => "somedata",
      "another-subdir/.gitattributes" => "*.rb linguist-language=Java\n❤.rb linguist-documentation=false",
      "another-subdir/❤.rb"   => "bar",
    }, "commit 1")
    @tree1 = @backend.rugged.lookup(@commit1).tree
  end

  def test_read_attributes_with_subdirectories
    paths = ["README.md", "subdir/test.rb", "another-subdir/❤.rb"]
    result = @backend.read_attributes(@tree1.oid, paths,
                %w(linguist-language linguist-documentation))

    expected_result = [
      {
        "linguist-documentation" => "false",
        "linguist-language" => nil
      },
      {
        "linguist-documentation" => nil,
        "linguist-language" => "Java"
      },
      {
        "linguist-documentation" => "false",
        "linguist-language" => "Java"
      }
    ]
    assert_equal expected_result, result
  end

  def test_read_attributes_without_attributes
    result = @backend.read_attributes(@tree1.oid, ["foo.rb"], %w(linguist-language))

    expected_result = [
      {
        "linguist-language" => nil
      }
    ]
    assert_equal expected_result, result
  end

  def test_read_attributes_with_subset_of_present_keys
    paths = ["README.md", "subdir/test.rb", "another-subdir/❤.rb"]
    result = @backend.read_attributes(@tree1.oid, paths, %w(linguist-language))

    expected_result = [
      {
        "linguist-language" => nil
      },
      {
        "linguist-language" => "Java"
      },
      {
        "linguist-language" => "Java"
      }
    ]
    assert_equal expected_result, result
  end

  def test_read_attributes_with_unknown_path
    result = @backend.read_attributes(@tree1.oid, ["unknown.rb"], %w(linguist-language))

    expected_result = [
      {
        "linguist-language" => nil
      }
    ]
    assert_equal expected_result, result
  end

  def test_read_attributes_with_invalid_tree_oid
    assert_raises(GitRPC::InvalidFullOid) do
      @backend.read_attributes("foobar", ["README.md"])
    end
  end

  def test_read_attributes_with_unknown_tree_oid
    assert_raises(GitRPC::InvalidObject) do
      @backend.read_attributes(SecureRandom.hex(20), ["README.md"])
    end
  end

  def test_read_attributes_with_nil_paths
    result = @backend.read_attributes(@tree1.oid, [nil])
    assert_equal([], result)
  end
end
