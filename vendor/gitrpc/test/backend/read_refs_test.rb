# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"

class BackendReadRefsTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    setup_test_commits

    @service = ::Spokes::Backend::GitService.new(GitRPC::GitHandler.new)
    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @fixture.add_empty_commit("commit 1")
    @fixture.command "git tag v0.1 HEAD"
    @tag_oid = @fixture.rev_parse("v0.1")

    @fixture.add_empty_commit("commit 2")
    @fixture.command "git branch some-branch HEAD"
    @branch_oid = @fixture.rev_parse("some-branch")

    @fixture.command "git tag -a -m 'v0.2 release' v0.2 HEAD"
    @annotated_oid = @fixture.rev_parse("v0.2")

    @fixture.add_empty_commit("commit 3")
    @fixture.command "git update-ref refs/special/foo $(git rev-parse HEAD)"
    @special_oid = @fixture.rev_parse("refs/special/foo")

    @fixture.add_empty_commit("hidden commit")
    @fixture.command "git update-ref refs/__gh__/hidden $(git rev-parse HEAD)"
    @hidden_oid = @fixture.rev_parse("refs/__gh__/hidden")

    @master_oid = @fixture.rev_parse("master")
  end

  # We get a list of objects which we need to convert to a hash in order to
  # match the backend output
  def refs_from_service(filter = nil)
    attrs =
      if filter
        {filter: filter}
      else
        {}
      end
    @service.call_rpc(:ReadRefs, attrs, backend: @backend).references.map do |ref|
      [ref.refname, ref.oid]
    end.to_h
  end

  def test_read_refs_filtered
    via_backend = @backend.read_refs
    via_service = refs_from_service

    [via_backend, via_service].each do |hash|
      assert_equal @tag_oid, hash["refs/tags/v0.1"]
      assert_equal @annotated_oid, hash["refs/tags/v0.2"]
      assert_equal @branch_oid, hash["refs/heads/some-branch"]
      assert_equal @master_oid, hash["refs/heads/master"]
      assert_equal 4, hash.size
      assert_equal hash, @backend.read_refs(true)
      assert_equal hash, @backend.read_refs("default")
    end
  end

  def test_read_refs_extended
    via_backend = @backend.read_refs("extended")
    via_service = refs_from_service(:EXTENDED)

    [via_backend, via_service].each do |hash|
      assert_equal @tag_oid, hash["refs/tags/v0.1"]
      assert_equal @annotated_oid, hash["refs/tags/v0.2"]
      assert_equal @branch_oid, hash["refs/heads/some-branch"]
      assert_equal @special_oid, hash["refs/special/foo"]
      assert_equal @master_oid, hash["refs/heads/master"]
      assert_equal 5, hash.size
    end
  end

  def test_read_refs_all
    via_backend = @backend.read_refs("all")
    via_service = refs_from_service(:ALL)

    [via_backend, via_service].each do |hash|
      assert_equal @tag_oid, hash["refs/tags/v0.1"]
      assert_equal @annotated_oid, hash["refs/tags/v0.2"]
      assert_equal @branch_oid, hash["refs/heads/some-branch"]
      assert_equal @special_oid, hash["refs/special/foo"]
      assert_equal @master_oid, hash["refs/heads/master"]
      assert_equal @hidden_oid, hash["refs/__gh__/hidden"]
      assert_equal 6, hash.size
      assert_equal hash, @backend.read_refs(false)
    end
  end

  def test_refs_empty_repo_empty_hash
    @fixture.teardown
    @fixture.setup

    assert_equal({}, @backend.read_refs)
    assert_equal({}, refs_from_service)
  end

  def test_read_qualified_refs
    result = @backend.read_qualified_refs(["refs/heads/master", "refs/tags/v0.2"])
    assert_equal [@master_oid, @annotated_oid], result
  end

  def test_read_qualified_refs_with_missing_ref
    assert_equal [nil], @backend.read_qualified_refs(["refs/heads/foobar"])
  end
end
