# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"
require "securerandom"

require "minitest/mock"

class BackendFetchTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    @fixture2 = RepositoryFixture.new("test2.git")
    @fixture2.setup

    setup_test_commits

    @backend = GitRPC::Backend.new(@fixture.path)
    @backend2 = GitRPC::Backend.new(@fixture2.path)
  end

  def teardown
    @fixture.teardown
    @fixture2.teardown
  end

  def setup_test_commits
    @past_time = Time.local(2000).iso8601
    @oid1 = @fixture2.commit_files({"README" => "Bonjour."}, "commit 1", :commit_time => @past_time)
    @oid2 = @fixture2.commit_files({"README" => "Hello."}, "commit 2", :commit_time => @past_time)
    @oid3 = @fixture2.commit_files({"README" => "Hola."}, "commit 3", :branch => "foo", :commit_time => @past_time)
    @fixture2.delete_branch("foo")
  end

  def test_fetch
    assert_raises GitRPC::CommandFailed do
      @backend.file_changed?("README", @oid1, @oid2)
    end

    res = @backend.fetch(@fixture2.path)

    assert_equal true, @backend.file_changed?("README", @oid1, @oid2)
  end

  def test_fetch_commits
    assert_raises GitRPC::CommandFailed do
      @backend.file_changed?("README", @oid2, @oid3)
    end

    assert_nil @backend.fetch_commits(@fixture2.path, @oid2) # has a ref
    assert_nil @backend.fetch_commits(@fixture2.path, @oid3) # doesn't have a ref

    assert_equal true, @backend.file_changed?("README", @oid2, @oid3)
  end
end
