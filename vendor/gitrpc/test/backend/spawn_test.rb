# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "gitrpc/backend"
require "repository_fixture"

# Tests the GitRPC::Backend::Native module methods
class BackendSpawnTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
  end

  def test_spawn_simple_command
    res = @backend.spawn(["echo hello"])
    assert_equal 0,  res["status"]
    assert_equal "hello\n", res["out"]
    refute res["truncated"]
  end

  def test_spawn_with_input
    res = @backend.spawn(["cat"], "some\ninput\n")
    assert_equal 0,  res["status"]
    assert_equal "some\ninput\n", res["out"]
  end

  def test_spawn_with_env
    res = @backend.spawn(["env"], nil, "foo" => "bar")
    assert_equal 0,  res["status"]
    assert_includes res["out"], "foo=bar"
  end

  def test_spawn_git_command
    res = @backend.spawn_git("help")
    assert res["ok"]
    assert_equal 0, res["status"]
    assert res["out"] =~ /^usage: git/
  end

  def test_spawn_git_command_failure
    res = @backend.spawn_git("noooope")
    assert !res["ok"]
    assert res["status"] != 0
    assert_equal "", res["out"]
  end

  def test_spawn_git_sets_cwd
    res = @backend.spawn_git("-c", ["alias.foo=!pwd", "foo"])
    assert_equal @fixture.path, res["out"].strip
  end

  def test_spawn_timeout_options
    @backend = GitRPC::Backend.new(@fixture.path, :timeout => 0.100)
    assert_raises(GitRPC::Timeout) { @backend.spawn(["sleep", 5.to_s]) }
  end

  def test_spawn_max_option
    @backend = GitRPC::Backend.new(@fixture.path, :max => 10)
    res = @backend.spawn(["yes"])
    refute res["ok"], "Truncated output results in a signal"
    assert res["out"].size >= 10, "Output should be more than or equal to 10, but finite"
    assert res["truncated"]
  end

  def test_spawn_max_param
    @backend = GitRPC::Backend.new(@fixture.path)
    res = @backend.spawn(["yes"], nil, {}, 10)
    refute res["ok"], "Truncated output results in a signal"
    assert res["out"].size >= 10, "Output should be more than or equal to 10, but finite"
    assert res["truncated"], "Output should be marked as truncated"
  end

  def test_spawn_env
    @backend = GitRPC::Backend.new(@fixture.path, :env => {"YO" => "DAWG"})
    res = @backend.spawn(["/bin/sh", "-c", "echo $YO"])
    assert res["ok"]
    assert_equal "DAWG\n", res["out"]
  end
end
