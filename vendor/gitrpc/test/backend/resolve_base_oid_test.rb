# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"
require "securerandom"

class BackendResolveBaseOidTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @committer = {
      "name"  => "Some Guy",
      "email" => "guy@example.com",
      "time"  => "2000-01-01T00:00:00Z"
    }
    @count = 0

    @backend = GitRPC::Backend.new(@fixture.path)
    @cache = GitRPC::Util::HashCache.new
    @client = GitRPC.new("fakerpc:#{@fixture.path}", :cache => @cache)
    @rugged = Rugged::Repository.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
  end

  def merged_tree(left_oid, right_oid, ancestor_oid = nil)
    left, right, ancestor = [left_oid, right_oid, ancestor_oid].map { |oid| oid ? @rugged.lookup(oid) : nil }
    index = left.tree.merge(right.tree, ancestor && ancestor.tree, GitRPC::Backend::BaseOidResolver::BASE_SHA_TREE_MERGE_OPTS)
    index.write_tree(@rugged)
  end

  def test_resolve_base_oid_requires_commit_oids
    assert c = create_commit(parent: nil, files: {})
    assert t = Rugged::Commit.lookup(@rugged, c).tree.oid

    assert_raises(GitRPC::InvalidObject) { base_rev_list(t, c, c) }
    assert_raises(GitRPC::InvalidObject) { base_rev_list(c, t, c) }
    assert_raises(GitRPC::InvalidObject) { base_rev_list(c, c, t) }
    assert_raises(GitRPC::InvalidObject) { @backend.resolve_base_oid(t, c, c) }
    assert_raises(GitRPC::InvalidObject) { @backend.resolve_base_oid(c, t, c) }
    assert_raises(GitRPC::InvalidObject) { @backend.resolve_base_oid(c, c, t) }
  end

  def test_resolve_simple
    c1 = create_commit parent: nil, files: {
      "README" => "README"
    }
    c2 = create_commit parent: c1, files: {
      "LICENSE" => "Copyright 2020"
    }

    assert_equal [c2], base_rev_list(c1, c2, c1)

    assert_equal c1, @backend.resolve_base_oid(c1, c2, c1)
  end

  def test_resolve_base_oid_with_adding_independent_file_changes_between_branches
    c1 = create_commit parent: nil, files: {
      "README" => "README"
    }
    c2 = create_commit parent: c1, files: {
      "LICENSE" => "Copyright 2015"
    }
    c3 = create_commit parent: c2, files: {
      "LICENSE" => "Copyright 2020"
    }
    c4 = create_commit parent: c1, files: {
      "README" => "# README"
    }
    c5 = create_commit parent: [c3, c4], files: {
      "README" => "# README",
      "LICENSE" => "Copyright 2020"
    }
    c6 = create_commit parent: c4, files: {
      "README" => "## README"
    }
    c7 = create_commit parent: c5, files: {
      "LICENSE" => "Copyright Forever"
    }
    c8 = create_commit parent: [c7, c6], files: {
      "README" => "## README",
      "LICENSE" => "Copyright Forever"
    }

    # c1
    # | \
    # |  \
    # c2 c4
    # |  |
    # |  |
    # c3 |
    # |  /
    # | /|
    # c5 c6
    # |  |
    # |  |
    # c7 |
    # |  /
    # | /
    # c8

    # From C1..

    assert_equal [c2], base_rev_list(c1, c2, c1)
    assert_equal c1, @backend.resolve_base_oid(c1, c2, c1)

    assert_equal [c2, c3], base_rev_list(c1, c3, c1)
    assert_equal c1, @backend.resolve_base_oid(c1, c3, c1)

    assert_equal [c2, c3, c5], base_rev_list(c1, c5, c4)
    assert_equal c4, @backend.resolve_base_oid(c1, c5, c4)

    assert_equal [c2, c3, c5, c7], base_rev_list(c1, c7, c4)
    assert_equal c4, @backend.resolve_base_oid(c1, c7, c4)

    assert_equal [c2, c3, c5, c7, c8], base_rev_list(c1, c8, c6)
    assert_equal c6, @backend.resolve_base_oid(c1, c8, c6)

    # From C2..

    assert_equal [c3], base_rev_list(c2, c3, c1)
    assert_equal c2, @backend.resolve_base_oid(c2, c3, c1)

    assert_equal [c3, c5], base_rev_list(c2, c5, c4)
    assert_equal merged_tree(c2, c4, c1), @backend.resolve_base_oid(c2, c5, c4)

    assert_equal [c3, c5, c7], base_rev_list(c2, c7, c4)
    assert_equal merged_tree(c2, c4, c1), @backend.resolve_base_oid(c2, c7, c4)

    assert_equal [c3, c5, c7, c8], base_rev_list(c2, c8, c6)
    assert_equal merged_tree(c2, c6, c1), @backend.resolve_base_oid(c2, c8, c6)

    # From C3..

    assert_equal [c5], base_rev_list(c3, c5, c4)
    assert_equal merged_tree(c3, c4, c1), @backend.resolve_base_oid(c3, c5, c4)

    assert_equal [c5, c7], base_rev_list(c3, c7, c4)
    assert_equal merged_tree(c3, c4, c1), @backend.resolve_base_oid(c3, c7, c4)

    assert_equal [c5, c7, c8], base_rev_list(c3, c8, c6)
    assert_equal merged_tree(c3, c6, c1), @backend.resolve_base_oid(c3, c8, c6)

    # From C5..

    assert_equal [c7], base_rev_list(c5, c7, c4)
    assert_equal c5, @backend.resolve_base_oid(c5, c7, c4)

    assert_equal [c7, c8], base_rev_list(c5, c8, c6)
    assert_equal merged_tree(c5, c6, c4), @backend.resolve_base_oid(c5, c8, c6)

    # From C7..

    assert_equal [c8], base_rev_list(c7, c8, c6)
    assert_equal merged_tree(c7, c6, c4), @backend.resolve_base_oid(c7, c8, c6)
  end

  def test_resolve_base_oid_with_changing_independent_file_changes_between_branches
    c1 = create_commit parent: nil, files: {
      "README" => "README"
    }
    c2 = create_commit parent: c1, files: {
      "LICENSE" => "Copyright 2015"
    }
    c3 = create_commit parent: c2, files: {
      "LICENSE" => "Copyright 2020"
    }
    c4 = create_commit parent: c2, files: {
      "README" => "# README"
    }
    c5 = create_commit parent: [c3, c4], files: {
      "README" => "# README",
      "LICENSE" => "Copyright 2020"
    }
    c6 = create_commit parent: c4, files: {
      "README" => "## README"
    }
    c7 = create_commit parent: c5, files: {
      "LICENSE" => "Copyright Forever"
    }
    c8 = create_commit parent: [c7, c6], files: {
      "README" => "## README",
      "LICENSE" => "Copyright Forever"
    }

    # c1
    # |
    # |
    # c2
    # | \
    # |  \
    # c3  c4
    # |  /|
    # | / |
    # c5  c6
    # |   |
    # |   |
    # c7  |
    # |  /
    # | /
    # c8

    # From C2..

    assert_equal [c3], base_rev_list(c2, c3, c2)
    assert_equal c2, @backend.resolve_base_oid(c2, c3, c2)

    assert_equal [c3, c5], base_rev_list(c2, c5, c4)
    assert_equal c4, @backend.resolve_base_oid(c2, c5, c4)

    assert_equal [c3, c5, c7], base_rev_list(c2, c7, c4)
    assert_equal c4, @backend.resolve_base_oid(c2, c7, c4)

    assert_equal [c3, c5, c7, c8], base_rev_list(c2, c8, c6)
    assert_equal c6, @backend.resolve_base_oid(c2, c8, c6)

    # From C3..

    assert_equal [c5], base_rev_list(c3, c5, c4)
    assert_equal merged_tree(c3, c4, c2), @backend.resolve_base_oid(c3, c5, c4)

    assert_equal [c5, c7], base_rev_list(c3, c7, c4)
    assert_equal merged_tree(c3, c4, c2), @backend.resolve_base_oid(c3, c7, c4)

    assert_equal [c5, c7, c8], base_rev_list(c3, c8, c6)
    assert_equal merged_tree(c3, c6, c2), @backend.resolve_base_oid(c3, c8, c6)

    # From C5..

    assert_equal [c7], base_rev_list(c5, c7, c4)
    assert_equal c5, @backend.resolve_base_oid(c5, c7, c4)

    assert_equal [c7, c8], base_rev_list(c5, c8, c6)
    assert_equal merged_tree(c5, c6, c4), @backend.resolve_base_oid(c5, c8, c6)

    # From C7..

    assert_equal [c8], base_rev_list(c7, c8, c6)
    assert_equal merged_tree(c7, c6, c4), @backend.resolve_base_oid(c7, c8, c6)
  end

  def test_resolve_base_oid_with_changing_seperate_hunks_in_same_file
    c1 = create_commit parent: nil, files: {
      "README" => "# Numbers\n\n" + (1..100).to_a.join("\n")
    }

    c2 = create_commit parent: c1, files: {
      "README" => "# Numbers 1 to 100\n\n" + (1..100).to_a.join("\n")
    }

    c3 = create_commit parent: c1, files: {
      "README" => "# Numbers\n\n" + (1..101).to_a.join("\n")
    }

    c4 = create_commit parent: [c3, c2], files: {
      "README" => "# Numbers 1 to 100\n\n" + (1..101).to_a.join("\n")
    }

    c5 = create_commit parent: c4, files: {
      "README" => "# Numbers 1 to 101\n\n" + (1..101).to_a.join("\n")
    }

    # c1
    # | \
    # |  \
    # c2  c3
    # |  /
    # | /
    # c4
    # |
    # |
    # c5

    # From C1..

    assert_equal [c2], base_rev_list(c1, c2, c1)
    assert_equal c1, @backend.resolve_base_oid(c1, c2, c1)

    assert_equal [c2, c4], base_rev_list(c1, c4, c3)
    assert_equal c3, @backend.resolve_base_oid(c1, c4, c3)

    assert_equal [c2, c4, c5], base_rev_list(c1, c5, c3)
    # TODO: This feels like it should really be c1, not c3 as c3 has only c1 as a parent
    assert_equal c3, @backend.resolve_base_oid(c1, c5, c3)

    # From C2..

    assert_equal [c4], base_rev_list(c2, c4, c3)
    assert_equal merged_tree(c2, c3, c1), @backend.resolve_base_oid(c2, c4, c3)

    assert_equal [c4, c5], base_rev_list(c2, c5, c3)
    assert_equal merged_tree(c2, c3, c1), @backend.resolve_base_oid(c2, c5, c3)

    # From C4..

    assert_equal [c5], base_rev_list(c4, c5, c3)
    assert_equal c4, @backend.resolve_base_oid(c4, c5, c3)
  end

  # TODO: These don't appear to actually be conflicting
  def test_resolve_base_oid_with_changing_conflicting_lines
    c1 = create_commit parent: nil, files: {
      "app.rb" => "class A\nend\n"
    }

    c2 = create_commit parent: c1, files: {
      "app.rb" => "class B\nend\n"
    }

    c3 = create_commit parent: c1, files: {
      "app.rb" => "class C\nend\n"
    }

    c4 = create_commit parent: [c2, c3], files: {
      "app.rb" => "class B\nend\n"
    }

    c5 = create_commit parent: c4, files: {
      "app.rb" => "class B < A\nend\n"
    }

    # c1
    # | \
    # |  \
    # c2  c3
    # |  /
    # | /
    # c4
    # |
    # |
    # c5

    # From C1..

    assert_equal [c2], base_rev_list(c1, c2, c1)
    assert_equal c1, @backend.resolve_base_oid(c1, c2, c1)

    assert_equal [c2, c4], base_rev_list(c1, c4, c3)
    assert_equal c3, @backend.resolve_base_oid(c1, c4, c3)

    assert_equal [c2, c4, c5], base_rev_list(c1, c5, c3)
    assert_equal c3, @backend.resolve_base_oid(c1, c5, c3)

    # From C2..

    # Case can't be rebased because of merge conflicts
    assert_equal [c4], base_rev_list(c2, c4, c3)
    assert_equal merged_tree(c2, c3, c1), @backend.resolve_base_oid(c2, c4, c3)

    # Case can't be rebased because of merge conflicts
    assert_equal [c4, c5], base_rev_list(c2, c5, c3)
    assert_equal merged_tree(c2, c3, c1), @backend.resolve_base_oid(c2, c5, c3)

    # From C4..

    # Case can't be rebased because of merge conflicts
    assert_equal [c5], base_rev_list(c4, c5, c3)
    assert_equal c4, @backend.resolve_base_oid(c4, c5, c3)
  end

  def test_resolve_base_oid_with_renamed_file
    c1 = create_commit parent: nil, files: {
      "README" => "Numbers\n\n" + (1..100).to_a.join("\n")
    }

    c2 = create_commit parent: c1, files: {
      "README" => nil,
      "README.md" => "# Numbers\n\n" + (1..100).to_a.join("\n")
    }

    c3 = create_commit parent: c1, files: {
      "README" => "Numbers\n\n" + (1..101).to_a.join("\n")
    }

    c4 = create_commit parent: [c3, c2], files: {
      "README" => nil,
      "README.md" => "# Numbers\n\n" + (1..101).to_a.join("\n")
    }

    c5 = create_commit parent: c4, files: {
      "README.md" => "# Numbers 1 to 101\n\n" + (1..101).to_a.join("\n")
    }

    # c1
    # | \
    # |  \
    # c2  c3
    # |  /
    # | /
    # c4
    # |
    # |
    # c5

    # From C1..

    assert_equal [c2], base_rev_list(c1, c2, c1)
    assert_equal c1, @backend.resolve_base_oid(c1, c2, c1)

    assert_equal [c2, c4], base_rev_list(c1, c4, c3)
    assert_equal c3, @backend.resolve_base_oid(c1, c4, c3)

    assert_equal [c2, c4, c5], base_rev_list(c1, c5, c3)
    assert_equal c3, @backend.resolve_base_oid(c1, c5, c3)

    # From C2..

    assert_equal [c4], base_rev_list(c2, c4, c3)
    assert_equal merged_tree(c2, c3, c1), @backend.resolve_base_oid(c2, c4, c3)

    assert_equal [c4, c5], base_rev_list(c2, c5, c3)
    assert_equal merged_tree(c2, c3, c1), @backend.resolve_base_oid(c2, c5, c3)

    # From C4..

    assert_equal [c5], base_rev_list(c4, c5, c3)
    assert_equal c4, @backend.resolve_base_oid(c4, c5, c3)
  end

  def test_resolve_base_oid_duplicate_hunks
    c1 = create_commit parent: nil, files: {
      "foo" => ("\n" * 20) + "(placeholder)" + ("\n" * 20) + "(placeholder)" + ("\n" * 20) + "(placeholder)" + ("\n" * 20)
    }

    c2 = create_commit parent: c1, files: {
      "foo" => ("\n" * 20) + "Josh" + ("\n" * 20) + "(placeholder)" + ("\n" * 20) + "(placeholder)" + ("\n" * 20)
    }

    c3 = create_commit parent: c1, files: {
      "foo" => ("\n" * 20) + "(placeholder)" + ("\n" * 20) + "(placeholder)" + ("\n" * 20) + "Josh" + ("\n" * 20)
    }

    c4 = create_commit parent: [c3, c2], files: {
      "foo" => ("\n" * 20) + "Josh" + ("\n" * 20) + "(placeholder)" + ("\n" * 20) + "Josh" + ("\n" * 20)
    }

    c5 = create_commit parent: c4, files: {
      "foo" => ("\n" * 20) + "Josh" + ("\n" * 20) + "Josh" + ("\n" * 20) + "Josh" + ("\n" * 20)
    }

    # c1
    # | \
    # |  \
    # c2  c3
    # |  /
    # | /
    # c4
    # |
    # |
    # c5

    # From C1..

    assert_equal [c2], base_rev_list(c1, c2, c1)
    assert_equal c1, @backend.resolve_base_oid(c1, c2, c1)

    assert_equal [c2, c4], base_rev_list(c1, c4, c3)
    assert_equal c3, @backend.resolve_base_oid(c1, c4, c3)

    assert_equal [c2, c4, c5], base_rev_list(c1, c5, c3)
    assert_equal c3, @backend.resolve_base_oid(c1, c5, c3)

    # From C2..

    assert_equal [c4], base_rev_list(c2, c4, c3)
    assert_equal merged_tree(c2, c3, c1), @backend.resolve_base_oid(c2, c4, c3)

    # From C4..

    assert_equal [c5], base_rev_list(c4, c5, c3)
    assert_equal c4, @backend.resolve_base_oid(c4, c5, c3)
  end

  def test_resolve_base_oid_shifted_duplicate_hunks
    c1 = create_commit parent: nil, files: {
      "foo" => ("\n" * 20) + "(placeholder)" + ("\n" * 20) + "(placeholder)" + ("\n" * 20) + "(placeholder)" + ("\n" * 20)
    }

    c2 = create_commit parent: c1, files: {
      "foo" => ("\n" * 20) + "(placeholder)" + ("\n" * 20) + "(placeholder)" + ("\n" * 20) + "Josh" + ("\n" * 20)
    }

    c3 = create_commit parent: c1, files: {
      "foo" => ("*\n" * 5) + ("\n" * 20) + "(placeholder)" + ("\n" * 20) + "(placeholder)" + ("\n" * 20) + "(placeholder)" + ("\n" * 20)
    }

    c4 = create_commit parent: c3, files: {
      "foo" => ("*\n" * 5) + ("\n" * 20) + "(placeholder)" + ("\n" * 20) + "Josh" + ("\n" * 20) + "(placeholder)" + ("\n" * 20)
    }

    c5 = create_commit parent: [c4, c2], files: {
      "foo" => ("*\n" * 5) + ("\n" * 20) + "(placeholder)" + ("\n" * 20) + "Josh" + ("\n" * 20) + "Josh" + ("\n" * 20)
    }

    c6 = create_commit parent: c5, files: {
      "foo" => ("*\n" * 5) + ("\n" * 20) + "Josh" + ("\n" * 20) + "Josh" + ("\n" * 20) + "Josh" + ("\n" * 20)
    }

    #  c1
    #  | \
    #  |  \
    #  c2 c3
    #  |  |
    #  |  |
    #  |  c4
    #  | /
    #  |/
    # c5
    #  |
    #  |
    # c6

    # From C1..

    assert_equal [c2], base_rev_list(c1, c2, c1)
    assert_equal c1, @backend.resolve_base_oid(c1, c2, c1)

    assert_equal [c2, c5], base_rev_list(c1, c5, c4)
    assert_equal c1, @backend.resolve_base_oid(c1, c2, c1)

    assert_equal [c2, c5, c6], base_rev_list(c1, c6, c4)
    assert_equal c1, @backend.resolve_base_oid(c1, c6, c1)

    # From C2..

    assert_equal [c5], base_rev_list(c2, c5, c4)
    assert_equal merged_tree(c2, c4, c1), @backend.resolve_base_oid(c2, c5, c4)

    assert_equal [c5, c6], base_rev_list(c2, c6, c4)
    assert_equal merged_tree(c2, c4, c1), @backend.resolve_base_oid(c2, c6, c4)

    # From C5..

    assert_equal [c6], base_rev_list(c5, c6, c4)
    assert_equal c5, @backend.resolve_base_oid(c5, c6, c4)
  end

  def test_resolve_between_binary_changes
    bin1 = "D\xF9\xEF\x93\xA0SK9\xA1\"\xABw\\\x81\xAC?\xD5k\x1C\xE3\xC6\xD3\xF8\xB3\xBB\x1E\xFC\xE7\xD4\xF8p\xD5\xD0\xA0"
    bin2 = "\xF2\x95\x9F\xCA(&\xFD]\x06\x82\x87\x9C\xCA\xB9\xFC|\xC3\xD2\x87\xB2\x00\xB9E\x8D}\xD9a\xD2R7^\x14\xE6,H6\xF0"
    bin3 = "?r\xC9\e\\\xFFa$\xBD\xB3<P\xC0\xC9\xAE\x8B\xEB\a\xAF\xC2T\xED}\x13\x80\xA5zp:\f\xF4\xBE\x1D,\x95.A\x04\xA8"

    c1 = create_commit parent: nil, files: {
      "txt" => "Hello",
      "bin1" => bin1
    }

    c2 = create_commit parent: c1, files: {
      "txt" => "Hello, World",
      "bin1" => bin1,
      "bin2" => bin2
    }

    c3 = create_commit parent: c1, files: {
      "bin3" => bin3
    }

    c4 = create_commit parent: [c3, c2], files: {
      "txt" => "Hello, World",
      "bin1" => bin1,
      "bin2" => bin2,
      "bin3" => bin3
    }

    c5 = create_commit parent: c4, files: {
      "txt" => "Hello, World!",
    }

    # c1
    # | \
    # |  \
    # c2  c3
    # |  /
    # | /
    # c4
    # |
    # |
    # c5

    # From C1..

    assert_equal [c2], base_rev_list(c1, c2, c1)
    assert_equal c1, @backend.resolve_base_oid(c1, c2, c1)

    assert_equal [c2, c4], base_rev_list(c1, c4, c3)
    assert_equal c3, @backend.resolve_base_oid(c1, c4, c3)

    assert_equal [c2, c4, c5], base_rev_list(c1, c5, c3)
    assert_equal c3, @backend.resolve_base_oid(c1, c5, c3)

    # From C2..

    assert_equal [c4], base_rev_list(c2, c4, c3)
    assert_equal merged_tree(c2, c3, c1), @backend.resolve_base_oid(c2, c4, c3)

    # From C4..

    assert_equal [c5], base_rev_list(c4, c5, c3)
    assert_equal c4, @backend.resolve_base_oid(c4, c5, c3)
  end

  def test_resolve_different_root_commits
    c1 = create_commit parent: nil, files: {
      "LICENSE" => "Copyright (c) 2015 GitHub, Inc.\n"
    }
    c2 = create_commit parent: c1, files: {
      "LICENSE" => "Copyright (c) 2016 GitHub, Inc.\n"
    }

    c3 = create_commit parent: nil, files: {
      "index.html" => "<h1>Hello, World</h1>\n"
    }

    c4 = create_commit parent: c2, files: {
      "LICENSE" => "Copyright (c) 2017 GitHub, Inc.\n"
    }
    c5 = create_commit parent: c4, files: {
      "LICENSE" => "Copyright (c) 2018 GitHub, Inc.\n"
    }

    c6 = create_commit parent: c3, files: {
      "index.html" => "<h1>Hello, World!</h1>\n"
    }

    c7 = create_commit parent: [c6, c5], files: {
      "LICENSE" => "Copyright (c) 2018 GitHub, Inc.\n",
      "index.html" => "<h1>Hello, World!</h1>\n"
    }

    c8 = create_commit parent: c7, files: {
      "index.html" => "<h2>Hello, World!</h2>\n"
    }

    # c1  c3
    # |   |
    # |   |
    # c2  c6
    # |   |
    # |   |
    # c4  |
    # |   |
    # |   |
    # c5  |
    #   \ |
    #    \|
    #     c7
    #     |
    #     |
    #     c8

    # From (root)...

    assert_equal [c3], base_rev_list(nil, c3, nil)
    assert_nil @backend.resolve_base_oid(nil, c3, nil)

    assert_equal [c3, c6], base_rev_list(nil, c6, nil)
    assert_nil @backend.resolve_base_oid(nil, c6, nil)

    assert_equal [c3, c6, c7], base_rev_list(nil, c7, c5)
    assert_equal c5, @backend.resolve_base_oid(nil, c7, c5)

    assert_equal [c3, c6, c7, c8], base_rev_list(nil, c8, c5)
    assert_equal c5, @backend.resolve_base_oid(nil, c8, c5)

    # From C3..

    assert_equal [c6], base_rev_list(c3, c6, c3)
    assert_equal c3, @backend.resolve_base_oid(c3, c6, c3)

    assert_equal [c6, c7], base_rev_list(c3, c7, c5)
    assert_equal merged_tree(c3, c5, nil), @backend.resolve_base_oid(c3, c7, c5)

    assert_equal [c6, c7, c8], base_rev_list(c3, c8, c5)
    assert_equal merged_tree(c3, c5, nil), @backend.resolve_base_oid(c3, c8, c5)
  end

  def test_resolve_added_removed_files
    c1 = create_commit parent: nil, files: {
      "file1" => "initial file 1\n",
      "file2" => "initial file 2\n",
      "file3" => "initial file 3\n",
      "file4" => "initial file 4\n",
      "file5" => "initial file 5\n"
    }

    c2 = create_commit parent: c1, files: {
      "file1" => nil,
      "file6" => "added in head 1\n"
    }

    c3 = create_commit parent: c1, files: {
      "file2" => nil,
      "file7" => "added in base 1\n"
    }

    c4 = create_commit parent: [c2, c3], files: {
      # 'file1' => nil,
      "file2" => nil,
      "file3" => "initial file 3\n",
      "file4" => "initial file 4\n",
      "file5" => "initial file 5\n",
      "file6" => "added in head 1\n",
      "file7" => "added in base 1\n"
    }

    c5 = create_commit parent: c4, files: {
      "file3" => nil,
      "file8" => "added in head 2\n"
    }

    # c1
    # | \
    # |  \
    # c2  c3
    # |  /
    # | /
    # c4
    # |
    # |
    # c5

    # From C1..

    assert_equal [c2], base_rev_list(c1, c2, c1)
    assert_equal c1, @backend.resolve_base_oid(c1, c2, c1)

    assert_equal [c3, c4], base_rev_list(c1, c4, c2)
    assert_equal c2, @backend.resolve_base_oid(c1, c4, c2)

    # From C2..

    assert_equal [c4], base_rev_list(c2, c4, c3)
    assert_equal merged_tree(c2, c3, c1), @backend.resolve_base_oid(c2, c4, c3)

    assert_equal [c4, c5], base_rev_list(c2, c5, c3)
    assert_equal merged_tree(c2, c3, c1), @backend.resolve_base_oid(c2, c5, c3)

    # From C4..

    assert_equal [c5], base_rev_list(c4, c5, c3)
    assert_equal c4, @backend.resolve_base_oid(c4, c5, c3)
  end

  def test_results_are_cached
    c1 = create_commit parent: nil, files: {
      "README" => "README"
    }
    c2 = create_commit parent: c1, files: {
      "LICENSE" => "Copyright 2020"
    }

    assert_equal c1, @backend.resolve_base_oid(c1, c2, c1)

    Rugged::Commit.stub(:lookup, -> (*args) { flunk "not using cached data" }) do
      assert_equal c1, @backend.resolve_base_oid(c1, c2, c1)
    end
  end

  def create_commit(parent:, files: {})
    @count += 1
    @backend.create_tree_changes(parent, {
      "message"   => "C#{@count}",
      "committer" => @committer
    }, files)
  end

  def base_rev_list(commit1_oid, commit2_oid, base_commit_oid)
    @client.rev_list([commit2_oid], exclude_oids: [commit1_oid, base_commit_oid].compact, reverse: true)
  end
end
