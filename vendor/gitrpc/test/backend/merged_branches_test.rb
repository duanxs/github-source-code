# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"

class MergedBranchesTest < Minitest::Test
  def setup
    path = File.expand_path("../../examples/merged_branches_test.git", __FILE__)
    @backend = GitRPC::Backend.new(path)
  end

  def test_shows_only_merged_branches
    merged = @backend.merged_branches("master")
    assert_equal ["branch1", "master"], merged
  end

  def test_merged_branches_raises_on_invalid_base_branch_oid
    assert_raises(GitRPC::InvalidCommitish) do
      @backend.merged_branches("--output=/usr/bin/git")
    end
  end
end
