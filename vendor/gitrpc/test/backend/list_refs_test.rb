# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"
require "securerandom"

require "minitest/mock"

class BackendListRefsTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    setup_test_commits

    @backend = GitRPC::Backend.new(@fixture.path)
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @past_time = Time.utc(2000).iso8601
    @oid1 = @fixture.commit_files({"README" => "Bonjour."}, "commit 1", :commit_time => @past_time)
    @oid2 = @fixture.commit_files({"README" => "Hello."}, "commit 2", :commit_time => @past_time)
  end

  def test_list_refs
    allrefs = @backend.list_refs
    assert_equal ["refs/heads/master"], allrefs

    @backend.update_ref("refs/heads/français", @oid1, nil)
    allrefs = @backend.list_refs
    assert_equal ["refs/heads/français".b, "refs/heads/master"], allrefs
  end

  def test_raw_branch_names_and_dates
    @backend.update_ref("refs/heads/français", @oid1, nil)

    branches = @backend.raw_branch_names_and_dates

    assert_equal [["Sat, 1 Jan 2000 00:00:00 +0000", "refs/heads/français".b], ["Sat, 1 Jan 2000 00:00:00 +0000", "refs/heads/master"]], branches
  end
end
