# frozen_string_literal: true
require_relative "../setup"

require "gitrpc/backend"
require "repository_fixture"
require "time"

class BackendCreateMergeCommitTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    @backend = GitRPC::Backend.new(@fixture.path)
    @rugged = @backend.rugged

    @author = {
      "name"  => "Some Human",
      "email" => "human@example.com",
      "time"  => "2000-01-01T00:00:00Z"
    }

    @committer = {
      "name"  => "Some Other Human",
      "email" => "other_human@example.com",
      "time"  => "2000-01-02T00:00:00Z"
    }

    setup_test_commits
  end

  def teardown
    @fixture.teardown
  end

  def setup_test_commits
    @base  = @fixture.commit_files({"README" => "Hello."}, "commit 1")
    @oid   = @fixture.commit_files({"README" => "Goodbye."}, "commit 2")
  end

  def test_creates_merge_commit
    message = "merge it\n"
    merge_oid, err = @backend.create_merge_commit(@base, @oid, @author, message)
    assert_nil err

    merge_commit = @backend.read_commits([merge_oid]).first

    assert_equal [@oid, @base].sort, merge_commit["parents"].sort
    assert_equal message, merge_commit["message"]

    expected_author = [
      @author["name"],
      @author["email"],
      [Time.iso8601(@author["time"]).to_i, 0]
    ]

    assert_equal expected_author, merge_commit["author"]
    assert_equal expected_author, merge_commit["committer"]
  end

  def test_raises_object_missing_when_given_a_missing_oid
    bad_oid = SecureRandom.hex(20)

    err = assert_raises GitRPC::ObjectMissing do
      @backend.create_merge_commit(@base, bad_oid, @author, "merge it\n")
    end

    assert_includes err.message, bad_oid
  end

  def test_raises_invalid_object_when_given_a_nonexistent_revision
    not_a_ref = "thisisnotavalidreference"
    err = assert_raises GitRPC::InvalidObject do
      @backend.create_merge_commit(@base, not_a_ref, @author, "merge it\n")
    end

    assert_includes err.message, not_a_ref
  end

  def test_raises_invalid_object_when_given_a_valid_non_commit_oid
    tree_oid = Rugged::Object.rev_parse(@rugged, @oid).tree.oid

    err = assert_raises GitRPC::InvalidObject do
      @backend.create_merge_commit(@base, tree_oid, @author, "merge it\n")
    end

    assert_includes err.message, "type does not match"
  end

  def test_raises_invalid_object_when_given_a_partial_valid_non_commit_oid
    tree_oid = Rugged::Object.rev_parse(@rugged, @oid).tree.oid

    err = assert_raises GitRPC::InvalidObject do
      @backend.create_merge_commit(@base, tree_oid[0..20], @author, "merge it\n")
    end

    assert_includes err.message, "tree"
    assert_includes err.message, tree_oid
  end

  def test_does_not_strip_message_comments
    message = "merge it\n# some comment\n"
    merge_oid, err = @backend.create_merge_commit(@base, @oid, @author, message)
    assert_nil err

    merge_commit = @backend.read_commits([merge_oid]).first
    assert_equal message, merge_commit["message"]
  end

  def test_can_specify_committer
    message = "merge it\n"
    merge_oid, err = @backend.create_merge_commit(@base, @oid, @author, message, @committer)
    assert_nil err

    merge_commit = @backend.read_commits([merge_oid]).first

    assert_equal [@oid, @base].sort, merge_commit["parents"].sort
    assert_equal message, merge_commit["message"]

    expected_author = [
      @author["name"],
      @author["email"],
      [Time.iso8601(@author["time"]).to_i, 0]
    ]

    assert_equal expected_author, merge_commit["author"]

    expected_committer = [
      @committer["name"],
      @committer["email"],
      [Time.iso8601(@committer["time"]).to_i, 0]
    ]

    assert_equal expected_committer, merge_commit["committer"]
  end

  def test_stage_signed_merge_commit
    message = "merge it\n"
    sig = "fake-signature"

    raw, err, details, tree_id = @backend.stage_signed_merge_commit(@base, @oid, @author, message, nil, {})
    assert_nil err
    assert_nil details
    oid, err = @backend.create_merge_commit(@base, @oid, @author, message)
    assert_nil err

    assert_equal tree_id, @rugged.lookup(oid).tree_oid
    assert_equal @rugged.read(oid).data, raw
  end
end
