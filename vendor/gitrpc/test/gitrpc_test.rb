# frozen_string_literal: true
require_relative "setup"

require "gitrpc"
require "repository_fixture"
require "fake_instrumenter"

class TestGitRPC < Minitest::Test
  def setup
    @instrumenter = GitRPC.instrumenter = FakeInstrumenter.new
    @fixture = RepositoryFixture.new
    @fixture.setup
  end

  def teardown
    GitRPC.instrumenter = nil
    @fixture.teardown
  end

  def test_sanity
    assert GitRPC
  end

  def test_new
    client = GitRPC.new("file:#{@fixture.path}")
    assert client.respond_to?(:send_message)
    assert_equal "holla back", client.send_message(:echo, "holla back")
  end

  # It should be possible to create instances for repositories that don't exist.
  # This will let us have a create method and also implement things like
  # clone or fork.
  def test_new_missing_directory
    GitRPC.new("file:/this/path/does/not/exist.git")
  end

  def test_instrument
    expect_payload = { :data => 42 }
    GitRPC.instrument :test, expect_payload do
      :boom
    end

    events = @instrumenter.events
    assert_equal 1, events.size

    name, payload = events[0]
    assert_equal "test.gitrpc", name
    assert_equal expect_payload, payload
  end

  def test_publish
    expect_payload = { :data => 42 }
    GitRPC.publish :test, Time.now, Time.now, 1, expect_payload

    events = @instrumenter.events
    assert_equal 1, events.size

    name, payload = events[0]
    assert_equal "test.gitrpc", name
    assert_equal expect_payload, payload
  end
end
