# frozen_string_literal: true
require_relative "setup"

require "gitrpc"
require "repository_fixture"

class GitmonClientTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup
    @backend = GitRPC::Backend.new(@fixture.path,
                                   :timeout => 1.0,
                                   :info => {:real_ip => "127.0.0.1",
                                             :repo_id => 123456})

    # Can be overridden by tests if they want to control the values
    @system_stats = GitRPC::GitmonClient::SystemStats
  end

  def teardown
    @fixture.teardown
  end

  def enable_gitmon
    # Do the ugly work of stubbing out the calls
    ret = nil
    GitRPC.gitmon_enabled?.tap do |orig|
      begin
        GitRPC.gitmon_enabled = true
        client, server = UNIXSocket.pair
        @gitmon_client = GitRPC::GitmonClient.new(client, system_stats=@system_stats)

        GitRPC::GitmonClient.stub(:connect, @gitmon_client) do
          ret = yield server
        end
      ensure
        @gitmon_client = nil
        GitRPC.gitmon_enabled = orig
      end
    end
    ret
  end

  def disable_gitmon
    ret = nil
    GitRPC.gitmon_enabled?.tap do |orig|
      begin
        GitRPC.gitmon_enabled = false
        ret = yield
      ensure
        GitRPC.gitmon_enabled = orig
      end
    end
    ret
  end

  def test_dont_call_gitmon_when_not_enabled
    disable_gitmon do
      GitRPC::GitmonClient.stub(:connect, proc { raise "Do Not Call" }) do
        assert_equal [], @backend.send_message(:echo, [])
      end
    end
  end

  def test_gitmon_functions_with_no_repo
    fixture = RepositoryFixture.new
    backend = GitRPC::Backend.new(fixture.path)

    enable_gitmon do
      assert_equal false, backend.send_message(:exist?)
    end
  end

  def test_dont_call_gitmon_for_non_tracked_calls
    GitRPC.gitmon_enabled?.tap do |orig|
      begin
        GitRPC.gitmon_enabled = true
        GitRPC::GitmonClient.stub(:connect, proc { raise "Do Not Call" }) do
          @backend.send_message(:loadavg)
          @backend.send_message(:online?)
        end
      ensure
        GitRPC.gitmon_enabled = orig
      end
    end
  end

  def test_gitmon_tracking_doesnt_affect_results
    enable_gitmon do |server|
      server.puts "continue"
      assert_equal [], @backend.send_message(:echo, [])
    end
  end

  def test_gitrpc_works_when_gitmon_isnt_around
    enable_gitmon do |server|
      server.close
      assert_equal [], @backend.send_message(:echo, [])
    end
  end

  def test_gitmon_fail_aborts_the_call
    assert_raises GitRPC::GitmonClient::AbortError do
      enable_gitmon do |server|
        server.puts "fail Too busy"
        assert_equal [], @backend.send_message(:echo, [])
      end
    end
  end

  def test_gitmon_receives_commands_in_order
    enable_gitmon do |server|
      server.puts "continue"
      assert_equal [], @backend.send_message(:echo, [])
      assert_equal "update", JSON.parse(server.gets)["command"]
      assert_equal "schedule", JSON.parse(server.gets)["command"]
      assert_equal "finish", JSON.parse(server.gets)["command"]
    end
  end

  def test_gitmon_receives_process_data
    resp = enable_gitmon do |server|
      server.puts "continue"
      assert_equal [], @backend.send_message(:echo, [])
      JSON.parse(server.readlines.first)
    end

    assert_equal "update", resp["command"]
    info = resp["data"]

    assert_equal "echo", info["program"]
    assert_equal @fixture.path, info["git_dir"]
    assert_equal "test/repo", info["repo_name"]
    assert_equal "gitrpc", info["via"]
    assert_equal "127.0.0.1", info["real_ip"]
    assert_equal 123456, info["repo_id"]
  end

  def test_gitmon_receives_stats_data
    @system_stats = MiniTest::Mock.new
      .expect(:disk_stats, [0, 0])
      .expect(:disk_stats, [100, 100])
      .expect(:cpu_time, 0)
      .expect(:cpu_time, 10)

    resp = enable_gitmon do |server|
      server.puts "continue"
      @backend.send_message(:fs_write, "new-file.txt", "data\n")
      JSON.parse(server.readlines.last)
    end

    assert_equal "finish", resp["command"]
    stats = resp["data"]

    # Non zero proc stats
    assert 0 < stats["disk_write_bytes"]
    assert 0 < stats["disk_read_bytes"]
    assert 0 < stats["cpu"]

    # 0 == success
    assert_equal 0, stats["result_code"]
    assert_nil stats["fatal"]
  end

  def test_gitmon_receives_data_on_error
    @system_stats = MiniTest::Mock.new
      .expect(:disk_stats, [0, 0])
      .expect(:disk_stats, [100, 100])
      .expect(:cpu_time, 0)
      .expect(:cpu_time, 10)

    resp = enable_gitmon do |server|
      server.puts "continue"
      assert_raises(RuntimeError) do
        @backend.send_message(:boomtown, RuntimeError)
      end
      JSON.parse(server.readlines.last)
    end

    assert_equal "finish", resp["command"]
    stats = resp["data"]

    # Non zero proc stats
    assert 0 < stats["disk_write_bytes"]
    assert 0 < stats["disk_read_bytes"]
    assert 0 < stats["cpu"]

    # 0 == success
    assert_equal 1, stats["result_code"]
    assert_equal "RuntimeError", stats["fatal"]
  end

  def test_gitmon_client_connection_refused
    client = GitRPC::GitmonClient.connect("/a/path/that/does/not/exist")
    assert_nil client
  end

  def test_gitmon_client_connection_successful
    sockname = "a-good-name-for-a-socket"
    response_str = "response string"
    request_str = "request string"
    File.unlink(sockname) if File.exist?(sockname)
    server = UNIXServer.new(sockname)
    client = GitRPC::GitmonClient.connect(sockname)
    assert client.is_a?(GitRPC::GitmonClient), "oops, client is a #{client.class}"

    ssock = server.accept
    ssock.puts response_str
    actual_response = client.request(request_str, response_expected: true)
    client.socket.close
    assert_equal response_str+"\n", actual_response

    str = ssock.read
    assert_equal %Q[{"command":"#{request_str}","data":{}}\n], str
  ensure
    ssock.close unless ssock.closed?
    server.close unless server.closed?
    client.socket.close unless client.socket.closed?
    File.unlink(sockname) if File.exist?(sockname)
  end
end
