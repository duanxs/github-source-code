# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "repository_fixture"
require "protocol/bertrpc_test_server"
require "gitrpc/protocol/dgit_lint"

# Stub out all non-blocking connections to IPs other than 127.0.0.1 so
# they raise EINPROGRESS forever and never succeed.
class WrapSocket
  def initialize(subject)
    @subject = subject
  end

  def skip_select?; @skip_select; end

  def connect_nonblock(addr)
    ip = Socket.unpack_sockaddr_in(addr)[1]
    unless ip == "127.0.0.1"
      @skip_select = true
      raise Errno::EINPROGRESS
    end
    super
  end

  private
  def method_missing(method, *args, &block)
    @subject.__send__(method, *args, &block)
  end
end

class ProtocolDGitTest < Minitest::Test
  class DGitDelegate
    attr_reader :id, :shard_path

    def initialize(namespace, id, shard_path, routes)
      @namespace, @id, @shard_path = namespace, id, shard_path
      @routes = routes
      @errors = []
      @warnings = []
    end

    def namespace
      "repository".freeze
    end

    def setup(*args); end

    def get_write_routes
      @routes.sort_by { |r| r.host }
    end

    def write_quorum
      @routes.length / 2 + 1
    end

    def get_read_routes
      get_write_routes
    end

    def on_error(route, err)
      @errors << {:host=>route.host, :err=>err}
    end

    def on_disagreement(answers, errors)
    end

    def on_warning(w)
      @warnings << w
    end

    def use_fakerpc?
      false
    end

    attr_reader :errors, :warnings
  end

  def setup
    @fixture = RepositoryFixture.new

    # We'll need multiple BERTRPC test servers if we want to test
    # concurrency, because the test-server implementation is single
    # threaded.
    @port = 10896
    @servers = (1..3).map do |i|
      BERTRPCTestServer.new(@port + i)
    end
    @url = "dgit:/"
    @routes = (1..3).map do |i|
      GitRPC::Protocol::DGit::Route.new("localhost", @fixture.path, port: @port+i)
    end
    @delegate = DGitDelegate.new("repository", 1234, "/shard/path", @routes)
    @cache = GitRPC::Util::HashCache.new
  end

  def teardown
    @fixture.teardown
    @servers.each { |s| s.stop }
  end

  # Test three basic dgit behaviors:
  #  - slow requests (1s) operate in parallel
  #  - echo operations return the expected string
  #  - reading a non-existent file raises an exception
  def test_dgit
    @servers.each { |s| s.start }
    proto = GitRPC::Protocol.resolve(@url, :delegate => @delegate)
    client = GitRPC::Client.new(proto, @cache)

    start = Time.now.to_f
    res = client.slow(1)
    assert_equal "OK", res
    finish = Time.now.to_f
    # It takes 1 seconds to do it concurrently and N=3 seconds to do it
    # serially.  Split the difference to allow for some slop -- a correct
    # run is < 2 seconds.
    assert finish-start >= 1, "requests did not run at all: (%f sec)" % (finish-start)
    assert finish-start < 2, "requests did not proceed in parallel: (%f sec)" % (finish-start)

    res = client.echo("lorem ipsum")
    assert_equal "lorem ipsum", res

    res = client.echo(true)
    assert_equal true, res

    res = client.echo(false)
    assert_equal false, res

    res = client.echo(nil)
    assert_nil res

    res = client.echo([])
    assert_equal [], res

    assert_raises(GitRPC::InvalidRepository) do
      res = client.fs_read("there is no such file")
    end
  end

  # Set the server timeout to 1s, and then schedule some 3s operations.
  # Assert that they raise timeouts and don't take more than 2s total.
  def test_dgit_timeout
    @servers.each { |s| s.start }

    proto = GitRPC::Protocol.resolve(@url, :timeout=>1, :delegate => @delegate)
    client = GitRPC::Client.new(proto, @cache)

    start = Time.now.to_f
    e = assert_raises(GitRPC::Timeout) do
      res = client.slow(3)
    end
    finish = Time.now.to_f

    assert_match /No response from/, e.message
    assert finish-start >= 1, "requests did not run at all: (%f sec)" % (finish-start)
    assert finish-start < 2, "requests did not time out: (%f sec)" % (finish-start)
  end

  # There are three servers.  If we try to connect to four, the fourth
  # will fail.  But since the others return answers, the answers dominate,
  # and the error is reported only via Delegate#on_error.  The call
  # actually succeeds.
  def test_dgit_one_connection_failure
    @servers.each { |s| s.start }
    routes = (1..4).map do |i|    # 4th will fail
      GitRPC::Protocol::DGit::Route.new("localhost", @fixture.path, port: @port+i)
    end
    my_delegate = DGitDelegate.new("repository", 1234, "/shard/path", routes)
    proto = GitRPC::Protocol.resolve(@url, :delegate => my_delegate)
    client = GitRPC::Client.new(proto, @cache)
    res = client.slow(1)
    assert_equal "OK", res
    assert_equal 1, my_delegate.errors.size
    assert_equal routes[3].original_host, my_delegate.errors.first[:host]
    assert_equal GitRPC::ConnectionError, my_delegate.errors.first[:err].class
  end

  # If we connect only to bogus servers, the #slow call does fail.  Since
  # the failures agree, we get that as the exception.
  def test_dgit_all_connection_failure
    @servers.each { |s| s.start }
    routes = (1..3).map do |i|
      GitRPC::Protocol::DGit::Route.new("localhost", @fixture.path, port: @port+5+i)
    end
    my_delegate = DGitDelegate.new("repository", 1234, "/shard/path", routes)
    proto = GitRPC::Protocol.resolve(@url, :delegate => my_delegate)
    client = GitRPC::Client.new(proto, @cache)
    e = assert_raises(GitRPC::ConnectionError) do
      res = client.slow(1)
    end
    assert_match /Unable to connect/, e.message
  end

  # If all hosts throw the same error, it gets bundled up as a standard
  # GitRPC::Failure.
  def test_dgit_uniform_failures
    @servers.each { |s| s.start }
    proto = GitRPC::Protocol.resolve(@url, :delegate => @delegate)
    client = GitRPC::Client.new(proto, @cache)
    e = assert_raises(GitRPC::Failure) do
      res = client.slow("one")
    end
    assert_match /TypeError/, e.message
  end

  # If two hosts are unreachable and the third returns an error, then we
  # get a ResponseError, and the caller has to sort it out.
  def test_dgit_mixed_failures
    @servers.first.start
    proto = GitRPC::Protocol.resolve(@url, :delegate => @delegate)
    client = GitRPC::Client.new(proto, @cache)
    e = assert_raises(GitRPC::Protocol::DGit::ResponseError) do
      res = client.slow("one")
    end
    assert e.answers.empty?, "Got #{e.answers.size} answer(s), expected 0"
    assert_equal 3, e.errors.values.size
    assert_equal TypeError, e.errors[@routes[0]].class
    assert_equal BERTRPC::ConnectionError, e.errors[@routes[1]].class
    assert_equal BERTRPC::ConnectionError, e.errors[@routes[2]].class
    assert e.failbot_context[:answers].empty?
    refute e.failbot_context[:errors].empty?
  end

  # If only the third server is started, a read call should quickly
  # attempt each server in turn, reporting that the first two are down,
  # and then return the result from the third.
  def test_dgit_read_failover_connection_refused
    @servers.last.start
    proto = GitRPC::Protocol.resolve(@url, :delegate => @delegate)
    client = GitRPC::Client.new(proto, @cache)
    start = Time.now
    res = client.online?
    finish = Time.now
    assert res
    assert_equal 2, @delegate.errors.size
    (0..1).each do |i|
      assert_equal @routes[i].original_host, @delegate.errors[i][:host]
      assert_equal GitRPC::ConnectionError, @delegate.errors[i][:err].class
    end
    assert finish-start < 1, "fail-over took too long: (%f sec)" % (finish-start)
  end

  # If the first two servers are unreachable, they should time out, GitRPC
  # should report that they are down, and then return the result from the
  # third.
  def test_dgit_read_failover_connection_timeouts
    @servers.last.start
    routes = [
      GitRPC::Protocol::DGit::Route.new("1.1.1.1",   @fixture.path, port: @port+1),
      GitRPC::Protocol::DGit::Route.new("1.1.1.1",   @fixture.path, port: @port+2),
      GitRPC::Protocol::DGit::Route.new("localhost", @fixture.path, port: @port+3),
    ]

    old_socket_new = Socket.method(:new)
    old_io_select = IO.method(:select)
    filter = -> (list) { list && list.reject { |x| x.respond_to?(:skip_select?) && x.skip_select? } }
    Socket.stub(:new, lambda { |a, b, c| WrapSocket.new(old_socket_new.call(a, b, c)) }) do
      IO.stub(:select, lambda { |r, w, e, t| old_io_select.call(filter.call(r), filter.call(w), filter.call(e), t) }) do
        my_delegate = DGitDelegate.new("repository", 1234, "/shard/path", routes)
        proto = GitRPC::Protocol.resolve(@url, :delegate => my_delegate, :connect_timeout => 1)
        client = GitRPC::Client.new(proto, @cache)
        start = Time.now
        res = client.online?
        finish = Time.now
        assert res
        assert_equal 2, my_delegate.errors.size
        (0..1).each do |i|
          assert_equal routes[i].original_host, my_delegate.errors[i][:host]
          assert_equal GitRPC::ConnectionError, my_delegate.errors[i][:err].class
        end
        assert finish-start > 2, "fail-over happened too fast: (%f sec)" % (finish-start)
        assert finish-start < 3, "fail-over took too long: (%f sec)" % (finish-start)
      end
    end
  end

  # A read operation that returns an exception other than a connection
  # failure does not fail over.
  def test_dgit_read_failure_without_failover
    @servers.each { |s| s.start }
    proto = GitRPC::Protocol.resolve(@url, :delegate => @delegate)
    client = GitRPC::Client.new(proto, @cache)
    e = assert_raises(GitRPC::InvalidRepository) do
      res = client.fs_read("a file that does not exist")
    end
  end

  def test_library_disagreement
    @servers.each { |s| s.start }
    proto = GitRPC::Protocol.resolve(@url, :delegate => @delegate)
    client = GitRPC::Client.new(proto, @cache)

    e = assert_raises(GitRPC::Protocol::DGit::ResponseError) do
      res = client.rand
    end
    assert_equal 3, e.answers.length
    assert_equal 3, e.answers.values.uniq.length
    e.answers.each do |key, a|
      assert_equal GitRPC::Protocol::DGit::Route, key.class
      assert_equal Float, a.class
    end
    refute e.failbot_context[:answers].empty?
    assert e.failbot_context[:errors].empty?
  end

  # Ensure `one_rand` only runs on one backend
  def test_one_rand_only_once
    @servers.each { |s| s.start }
    proto = GitRPC::Protocol.resolve(@url, :delegate => @delegate)
    client = GitRPC::Client.new(proto, @cache)

    randval = client.one_rand

    assert randval.is_a?(Float), "Read-only varying call only runs on one backend"
  end

  def test_explicit_demux
    @servers.each { |s| s.start }
    proto = GitRPC::Protocol.resolve(@url, :delegate => @delegate)
    client = GitRPC::Client.new(proto, @cache)

    res = client.send_demux(:rand)
    assert res.is_a?(Hash)
    assert_equal 3, res.length
    res.each do |key, a|
      assert_equal GitRPC::Protocol::DGit::Route, key.class
      assert_equal Float, a.class
    end
  end

  def test_varying_output_with_spawn
    @servers.each { |s| s.start }
    proto = GitRPC::Protocol.resolve(@url, :delegate => @delegate)
    client = GitRPC::Client.new(proto, @cache)

    res = client.send_message(:spawn_rand)
    assert res.is_a?(Hash), "Calls that spawn return a result hash even when output varies"
    assert_equal 0, res["status"]
    assert res["ok"], "Call should return successfully"
    assert_equal @servers.length+1, res["out"].split(/\[localhost\]/).size
    assert_equal "", res["err"]
  end

  def test_local_is_local
    proto = GitRPC::Protocol.resolve("local:/")
    client = GitRPC::Client.new(proto, @cache)
    assert_raises(NoMethodError) do
      client.slow(1)
    end
  end

  def test_bogus_dgit_urls
    cases = [
      "dgit",                     # no path
      "foo://network:123/path",   # bad scheme
    ]
    cases.each do |url|
      assert_raises(GitRPC::Error, GitRPC::NoSuchProtocol, URI::InvalidURIError) do
        proto = GitRPC::Protocol.resolve(url, :delegate => @delegate)
      end
    end
  end

  def test_valid_dgit_url
    proto = GitRPC::Protocol.resolve("dgit:/", :delegate => @delegate)
    assert proto.is_a?(GitRPC::Protocol::DGit)
  end

  def test_delegate_is_required
    e = assert_raises(GitRPC::Error) do
      proto = GitRPC::Protocol.resolve("dgit:/")
    end
    assert e.message =~ /delegate.*required/i
  end

  def test_dgit_repository_key
    proto = GitRPC::Protocol.resolve(@url, :delegate => @delegate)
    client = GitRPC::Client.new(proto, @cache)
    key = client.repository_key
    assert key.is_a?(String)
    assert key.length > 0
  end

  def test_compare_routes
    r1 = GitRPC::Protocol::DGit::Route.new("github-dfs111", "/foo/xyzzy", port: 8150)
    r2 = GitRPC::Protocol::DGit::Route.new("github-dfs13",  "/foo/baz",   port: 8149)
    r3 = GitRPC::Protocol::DGit::Route.new("github-dfs13",  "/foo/bar",   port: 8151)
    r4 = GitRPC::Protocol::DGit::Route.new("github-dfs100", "/foo/baa",   port: 123, voting: false)

    assert (r1 <=> r2) == -1
    assert (r2 <=> r3) == -1
    assert (r3 <=> r4) == -1
    assert (r1 <=> r4) == -1
  end

  def test_sorting_routes
    assert @routes.length > 1
    assert @routes.sort == @routes.reverse.sort
  end
end

class DGitRouteTest < Minitest::Test
  include ::GitRPC::Protocol::DGit::RouteLintTest
  def dgit_route
    ::GitRPC::Protocol::DGit::Route.new("originalhost", "/path/to/repo.git")
  end
end
