# frozen_string_literal: true
require_relative "../setup"

require "gitrpc"
require "bert"

class DGitCollatorTest < Minitest::Test
  def self.collator_test(prefix,
                         message, args,
                         voters, nonvoters,
                         expected_disagreement, expected_errors_reported,
                         &block)
    name = "test_#{prefix} with voting replicas #{voters}".dup
    name << " and nonvoting replicas #{nonvoters}" unless nonvoters.empty?
    name = name.gsub(/\W+/, "_").sub(/\A_*(.*?)_*\z/, "\\1")
    define_method(name.to_sym) do
      delegate = FakeCollationDelegate.new(message, args, voters, nonvoters)

      instance_exec(delegate.get_result_collator, &block)

      if expected_disagreement
        assert_equal 1, delegate.disagreements.length, "number of disagreements"
      else
        assert_equal [], delegate.disagreements, "disagreements"
      end

      hostnames = expected_errors_reported.map { |i| "dgit#{i + 1}" }
      assert_equal hostnames, delegate.errors_reported.sort, "hosts with errors"
    end
  end

  def simplify(message, answer, strict: false)
    collator = ::GitRPC::Protocol::DGit::ResultCollator.new(nil, message, nil, nil, nil)
    simplifier = collator.get_simplifier(strict)
    simplifier.call(answer)
  end

  # Results that end up with an answer of "ok":
  [
    # Success with various numbers of nonvoting replicas:
    [["ok"] * 3, [], false, []],
    [["ok"] * 3, ["ok"], false, []],
    [["ok"] * 3, ["ok"] * 2, false, []],
    [["ok"] * 3, ["ok"] * 3, false, []],

    # Two is also a sufficient quorum:
    [["ok"] * 2, [], false, []],
    [["ok"] * 2, ["ok"] * 3, false, []],

    # Results can outvote ConnectionErrors. ConnectionErrors *aren't*
    # considered disagreement, but are reported via `on_error`:
    [[:refuse, "ok", "ok"], ["ok"] * 3, false, [0]],
    [["ok", :refuse, "ok"], ["ok"] * 3, false, [1]],
    [["ok", "ok", :refuse], ["ok"] * 3, false, [2]],

    [[:refuse, "ok", "ok"], [:refuse] * 3, false, [0, 3, 4, 5]],
    [["ok", :refuse, "ok"], [:refuse] * 3, false, [1, 3, 4, 5]],
    [["ok", "ok", :refuse], [:refuse] * 3, false, [2, 3, 4, 5]],

    [[:refuse, "ok", "ok"], ["foo", :error, :refuse], true, [0, 4, 5]],
    [["ok", :refuse, "ok"], ["foo", :error, :refuse], true, [1, 4, 5]],
    [["ok", "ok", :refuse], ["foo", :error, :refuse], true, [2, 4, 5]],

    # With four replicas, we succeed if we have a quorum of 3:
    [["ok", "ok", "ok", :refuse], [], false, [3]],
    [["ok", "ok", "ok", :error], [], true, [3]],

    # Results can outvote errors. Errors *are* considered
    # disagreement, and are also reported via `on_error`:
    [[:error, "ok", "ok"], ["ok"] * 3, true, [0]],
    [["ok", :error, "ok"], ["ok"] * 3, true, [1]],
    [["ok", "ok", :error], ["ok"] * 3, true, [2]],

    [[:error, "ok", "ok"], [:refuse] * 3, true, [0, 3, 4, 5]],
    [["ok", :error, "ok"], [:refuse] * 3, true, [1, 3, 4, 5]],
    [["ok", "ok", :error], [:refuse] * 3, true, [2, 3, 4, 5]],

    [[:error, "ok", "ok"], ["foo", :error, :refuse], true, [0, 4, 5]],
    [["ok", :error, "ok"], ["foo", :error, :refuse], true, [1, 4, 5]],
    [["ok", "ok", :error], ["foo", :error, :refuse], true, [2, 4, 5]],

    # The voters determine the result regardless of what the nonvoters
    # do:
    [["ok"] * 3, [:refuse] * 3, false, [3, 4, 5]],
    [["ok"] * 3, [:error], true, [3]],
    [["ok"] * 3, ["foo", :error, :refuse], true, [4, 5]],
    [["ok"] * 3, ["burp"], true, []],

    # That's true even if we don't have a full complement of voters:
    [["ok"] * 2, [:error] * 3, true, [2, 3, 4]],
    [["ok"] * 2, ["burp", "burp", "burp"], true, []],
    [["ok"] * 2, ["foo", :error, :refuse], true, [3, 4]],
  ].each do |voters, nonvoters, expected_disagreement, expected_errors_reported|
    collator_test "collate success",
                  :echo, [],
                  voters, nonvoters,
                  expected_disagreement, expected_errors_reported do |result_collator|
      assert_equal "ok", result_collator.decide_consensus
    end
  end

  # Differences of opinion about binaryness of files from the create_merge_commit call.
  [
    [[:merge_ok] * 3, [:merge_ok] * 3, false, :merge_ok],
    [[:merge_bin] * 3, [:merge_bin] * 3, false, :merge_bin],
    [[:merge_bin] * 3, [:merge_txt] * 3, false, :merge_bin],
    [[:merge_bin, :merge_txt, :merge_txt], [:merge_txt] * 3, false, :merge_bin],
    [[:merge_txt, :merge_bin, :merge_bin], [:merge_txt] * 3, false, :merge_txt],
    [[:merge_bin_other_sha, :merge_bin, :merge_bin], [:merge_txt] * 3, true, :raise],
    [[:merge_txt_other_sha, :merge_txt, :merge_txt], [:merge_txt] * 3, true, :raise],
    [[:merge_bin] * 3, [:merge_bin, :merge_bin, :merge_bin_other_sha], true, :merge_bin],
  ].each do |voters, nonvoters, expected_disagreement, expected_result|
    collator_test "create merge commit",
                  :create_merge_commit, ["cacecacecacecacecacecacecacecacecacecace", "ceadceadceadceadceadceadceadceadceadcead", {"name" => "dontcare", "email" => "dontcare", "etc" => "etc"}, "this is a test merge", nil],
                  voters, nonvoters,
                  expected_disagreement, [] do |result_collator|
      if expected_result == :raise
        assert_raises(GitRPC::Protocol::DGit::ResponseError) do
          result_collator.decide_consensus
        end
      else
        assert_equal merge_results(expected_result), result_collator.decide_consensus
      end
    end
  end

  MergeResults = {
    :merge_ok            => lambda { ["0000111100001111000011110000111100001111", nil] },
    :merge_bin           => lambda { [nil, "merge_conflict", {:base => "bacebacebacebacebacebacebacebacebacebace", :head => "4ead4ead4ead4ead4ead4ead4ead4ead4ead4ead", :conflicted_files => {"any/file" => {:base => nil, :head => {:oid => "9ead9ead9ead9ead9ead9ead9ead9ead9ead9ead", :file_size => 123, :binary => true}, :type => :regular_conflict}}, :more_conflicted_files => false}] },
    :merge_txt           => lambda { [nil, "merge_conflict", {:base => "bacebacebacebacebacebacebacebacebacebace", :head => "4ead4ead4ead4ead4ead4ead4ead4ead4ead4ead", :conflicted_files => {"any/file" => {:base => nil, :head => {:oid => "9ead9ead9ead9ead9ead9ead9ead9ead9ead9ead", :file_size => 123, :binary => false}, :type => :regular_conflict}}, :more_conflicted_files => false}] },
    :merge_bin_other_sha => lambda { [nil, "merge_conflict", {:base => "bacebacebacebacebacebacebacebacebacebace", :head => "4ead4ead4ead4ead4ead4ead4ead4ead4ead4ead", :conflicted_files => {"any/file" => {:base => nil, :head => {:oid => "2ead9ead9ead9ead9ead9ead9ead9ead9ead9ead", :file_size => 123, :binary => true}, :type => :regular_conflict}}, :more_conflicted_files => false}] },
    :merge_txt_other_sha => lambda { [nil, "merge_conflict", {:base => "bacebacebacebacebacebacebacebacebacebace", :head => "4ead4ead4ead4ead4ead4ead4ead4ead4ead4ead", :conflicted_files => {"any/file" => {:base => nil, :head => {:oid => "2ead9ead9ead9ead9ead9ead9ead9ead9ead9ead", :file_size => 123, :binary => false}, :type => :regular_conflict}}, :more_conflicted_files => false}] },
  }

  def merge_results(k)
    MergeResults.fetch(k).call
  end

  def test_simplifier_removes_binaryness_from_create_merge_commit_output
    assert_equal simplify(:create_merge_commit, merge_results(:merge_bin)),
      simplify(:create_merge_commit, merge_results(:merge_txt))
  end

  # Results that end up raising ResponseError:
  [
    # Quorum not attained:
    [["ok", :refuse, :refuse], ["ok"] * 3, false, [1, 2]],
    [[:refuse, :refuse, "ok"], ["ok"] * 3, false, [0, 1]],
    [[:error, :refuse, :refuse], ["ok"] * 3, true, [0, 1, 2]],
    [[:refuse, :refuse, :error], ["ok"] * 3, true, [0, 1, 2]],
    [["ok", :refuse], ["ok"], false, [1]],
    [["ok"], ["ok"], false, []],

    # With four replicas, the quorum is 3:
    [["ok", "ok", :refuse, :refuse], [], false, [2, 3]],
    [["ok", "ok", :error, :refuse], [], true, [2, 3]],
    [["ok", "ok", "burp", "burp"], [], true, []],

    # Errors caused by disagreements:
    [["ok", "ok", "burp"], ["ok"], true, []],
    [["ok", :error], ["ok"], true, [1]],
    [["ok", "burp"], ["ok"], true, []],
    [["foo", "bar", "baz"], [], true, []],
    [["foo", "bar", :refuse], [], true, [2]],
    [["foo", "bar", :error], [], true, [2]],
    [["ok", :error, :refuse], [], true, [1, 2]],

    # One error can't outvote an answer or another error:
    [[:error, :error, :ok], [], true, [0, 1]],
    [[:error, :error, ZeroDivisionError.new("foo")], [], true, [0, 1, 2]],
  ].each do |voters, nonvoters, expected_disagreement, expected_errors_reported|
    collator_test "collate response errors",
                  :echo, [],
                  voters, nonvoters,
                  expected_disagreement, expected_errors_reported do |result_collator|
      assert_raises(::GitRPC::Protocol::DGit::ResponseError) do
        result_collator.decide_consensus
      end
    end
  end

  # Results that end up raising ConnectionError:
  [
    # Zero responses is a ConnectionError:
    [[:refuse] * 3, [], false, [0, 1, 2]],
    [[:refuse] * 3, ["ok"] * 3, false, [0, 1, 2]],
    [[:refuse] * 3, [:refuse] * 3, false, [0, 1, 2, 3, 4, 5]],

    # The same is true if we only have two replicas:
    [[:refuse] * 2, [], false, [0, 1]],
  ].each do |voters, nonvoters, expected_disagreement, expected_errors_reported|
    collator_test "collate connection errors",
                  :echo, [],
                  voters, nonvoters,
                  expected_disagreement, expected_errors_reported do |result_collator|
      assert_raises(::GitRPC::ConnectionError) do
        result_collator.decide_consensus
      end
    end
  end

  # Results that end up raising Failure:
  [
    # Unanimous errors are raised as GitRPC::Failure:
    [[:error] * 3, [], false, []],
    [[:error] * 3, ["ok"] * 3, true, []],
    [[:error] * 3, [:refuse] * 3, false, [3, 4, 5]],

    # Application errors can outvote connection errors:
    [[:refuse, :error, :error], [:refuse] * 3, false, [0, 3, 4, 5]],
    [[:error, :refuse, :error], ["foo", :error, :refuse], true, [1, 5]],
    [[:error, :error, :refuse], [], false, [2]],
  ].each do |voters, nonvoters, expected_disagreement, expected_errors_reported|
    collator_test "collate failures",
                  :echo, [],
                  voters, nonvoters,
                  expected_disagreement, expected_errors_reported do |result_collator|
      assert_raises(::GitRPC::Failure) do
        result_collator.decide_consensus
      end
    end
  end

  class FakeCollationDelegate
    attr_reader :write_quorum
    attr_reader :errors_reported, :warnings, :disagreements

    def initialize(message, args, voters, nonvoters)
      @message = message
      @args = args

      @write_quorum = [3, voters.length].max / 2 + 1

      routes = []
      @answers = {}
      @errors = {}

      [[voters, true], [nonvoters, false]].each do |behaviors, voting|
        behaviors.each do |behavior|
          route = ::GitRPC::Protocol::DGit::Route.new(
            "dgit#{routes.length + 1}", "/not/used", voting: voting,
          )
          routes << route

          success, result = result_for_behavior(behavior)
          (success ? @answers : @errors)[route] = result
        end
      end

      @errors_reported = []
      @warnings = []
      @disagreements = []
    end

    def get_result_collator
      ::GitRPC::Protocol::DGit::ResultCollator.new(
        self, @message, @args, @answers, @errors
      )
    end

    def on_disagreement(answers, errors)
      @disagreements << [answers, errors]
    end

    def on_error(route, err)
      @errors_reported << route.host
    end

    def on_warning(w)
      @warnings << w
    end

    # Return [success, value], where success is true if value is the
    # command's answer and false if value is the command's error.
    def result_for_behavior(behavior)
      case behavior
      when :ok
        [true, "Aye, capt'n"]
      when :error
        [false, RuntimeError.new("Ack! Thppt!")]
      when :refuse
        [false, ::BERTRPC::ConnectionError.new("dgit???", 1234)]
      when *MergeResults.keys
        [true, MergeResults.fetch(behavior).call]
      when String, Hash
        [true, behavior]
      when Exception
        [false, behavior]
      else
        raise "unknown behavior, #{behavior.inspect}"
      end
    end
  end
end
