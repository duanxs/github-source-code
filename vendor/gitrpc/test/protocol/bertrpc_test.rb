# frozen_string_literal: true
require_relative "../setup"

require "pp"
require "gitrpc"
require "repository_fixture"
require "fake_instrumenter"
require "protocol/bertrpc_test_server"

class ProtocolBERTRPCTest < Minitest::Test
  SOURCE_ROOT = File.expand_path("../../..", __FILE__)

  def setup
    @fixture = RepositoryFixture.new
    @port = 10896
    @server = BERTRPCTestServer.new(@port, source_root: SOURCE_ROOT)
    @url = "bertrpc://localhost:#{@port}#{@fixture.path}"
    @proto = GitRPC::Protocol.resolve(@url)
    @instrumenter = GitRPC.instrumenter = FakeInstrumenter.new
  end

  def teardown
    GitRPC.instrumenter = nil
    @fixture.teardown
    @server.stop
  end

  def test_resolve
    proto = GitRPC::Protocol.resolve(@url)
    assert proto.is_a?(GitRPC::Protocol::BERTRPC)
  end

  def test_url
    assert @proto.url.is_a?(URI)
    assert_equal "bertrpc", @proto.url.scheme
    assert_equal @fixture.path, @proto.url.path
  end

  def test_host
    assert_equal "localhost", @proto.host
  end

  def test_port
    assert_equal @port, @proto.port
  end

  def test_path
    assert_equal @fixture.path, @proto.path
  end

  def test_options
    opts = { :max => 1000 }
    proto = GitRPC::Protocol.resolve(@url, opts)
    assert_equal({ :max => 1000 }, proto.options)
  end

  def test_connection_error
    assert_raises GitRPC::ConnectionError do
      @proto.send_message(:echo)
    end
  end

  def test_send_message
    @server.start
    message = [123, "string", {"key" => "value"}]
    res = @proto.send_message(:echo, [123, "string", {"key" => "value"}])
    assert_equal message, res
  end

  def test_send_message_invalid_object
    assert_raises(GitRPC::EncodingError) do
      @proto.send_message(:echo, Object.new)
    end
  end

  def test_option_propagation
    @server.start
    opts = { :info => { :repo_name => "fizz/buzz" }, :trace2 => [] }
    proto = GitRPC::Protocol.resolve(@url, opts)
    res = proto.send_message(:echo_options)
    assert_equal opts, res
  end

  def test_option_propagation_localfilewrapper
    @server.start

    proto = GitRPC::Protocol.resolve(@url)
    proto.define_singleton_method(:resolved_host) do
      host
    end
    proto.instance_eval("undef :remote")

    opts = { :info => { :repo_name => "fizz/buzz" } }
    GitRPC.optimize_local_access = true
    results, errors = GitRPC::Protocol::BERTRPC.send_multiple([proto], :echo_options, [], opts)
    GitRPC.optimize_local_access = false

    assert_equal 0, errors.size
    assert_equal 1, results.size
    assert_equal opts[:info], results[proto][:info]
  end

  def test_error
    @server.start
    @fixture.teardown
    assert_raises(GitRPC::InvalidRepository) do
      @proto.send_message(:read_refs)
    end
  end

  def test_failure
    Dir.mkdir(@fixture.path)
    @fixture.init
    @server.start
    assert_raises(GitRPC::Failure) { @proto.send_message(:not_implemented_method) }
    failure = exception { @proto.send_message(:not_implemented_method) }

    original = failure.original
    assert original.kind_of?(NoMethodError)
  end

  def exception
    yield
  rescue => boom
    boom
  end

  def test_timeout_error
    @server.start
    proto = GitRPC::Protocol.resolve(@url, :timeout => 0.100)
    assert_raises GitRPC::Timeout do
      proto.send_message(:slow, 2)
    end
  end

  def test_default_timeout_error
    GitRPC.timeout = 0.100
    @server.start
    proto = GitRPC::Protocol.resolve(@url)
    assert_raises GitRPC::Timeout do
      proto.send_message(:slow, 2)
    end
  ensure
    GitRPC.timeout = nil
  end

  def test_timeout_fudge
    assert_nil @proto.client_timeout

    @proto.options[:timeout] = 30
    assert @proto.client_timeout > 30.0
  end

  # This checks that timeouts raise from the backend side.
  def test_timeout_raises_from_remote
    @server.start
    boom = nil
    proto = GitRPC::Protocol.resolve(@url, :timeout => 0.100)
    begin
      proto.send_message(:slow, 2)
    rescue GitRPC::Timeout => boom
    end
    assert boom, "didn't raise"
    bt = boom.backtrace
    i = bt.index("---- THE WIRE ----")
    assert i, "exception was not raised from remote side"
    assert i + 1 < bt.size,
           "exception didn't include remote-side backtrace"
    refute_match %r{^/}, bt[i - 1],
           "paths are not stripped from remote-side backtrace (SOURCE_ROOT: #{SOURCE_ROOT})"
  end

  # This checks that timeouts raise from the backend side.
  def test_timeout_raises_from_remote_global
    GitRPC.timeout = 0.100
    @server.start
    boom = nil
    proto = GitRPC::Protocol.resolve(@url)
    begin
      proto.send_message(:slow, 2)
    rescue GitRPC::Timeout => boom
    end
    assert boom, "didn't raise"
    bt = boom.backtrace
    i = bt.index("---- THE WIRE ----")
    assert i, "exception was not raised from remote side"
    assert i + 1 < bt.size,
           "exception didn't include remote-side backtrace"
    refute_match %r{^/}, bt[i - 1],
           "paths are not stripped from remote-side backtrace (SOURCE_ROOT: #{SOURCE_ROOT})"
  ensure
    GitRPC.timeout = nil
  end

  def test_send_message_instrumentation
    @server.start
    message = [123, "string", {"key" => "value"}]
    res = @proto.send_message(:echo, message)

    events = @instrumenter.events
    event = events.find { |e| e[0] == "bertrpc_send_message.gitrpc" }

    name, payload = event
    assert_equal :echo, payload[:name]
    assert_equal message, payload[:args].first
  end

  def test_send_message_timeout_instrumentation
    @server.start

    proto = GitRPC::Protocol.resolve(@url, :timeout => 0.100)

    assert_raises GitRPC::Timeout do
      proto.send_message(:slow, 2)
    end

    events = @instrumenter.events
    event = events.find { |e| e[0] == "bertrpc_send_message.gitrpc" }

    name, payload = event
    assert_equal :slow, payload[:name]
    assert_equal [2], payload[:args]
    assert_equal "GitRPC::Timeout", payload[:exception][0]
  end

  def test_send_single_with_instrumentation
    @server.start
    message = [123, "string", {"key" => "value"}]

    route = GitRPC::Protocol::DGit::Route.new("localhost", @fixture.path, port: @port)
    res = GitRPC::Protocol::BERTRPC.send_single(route, :echo, [message])

    events = @instrumenter.events
    event = events.find { |e| e[0] == "bertrpc_send_message.gitrpc" }

    name, payload = event
    assert_equal :echo, payload[:name]
    assert_equal message, payload[:args].first
  end
end

class ProtocolBERTRPCMultiTest < Minitest::Test
  SOURCE_ROOT = File.expand_path("../../..", __FILE__)

  def setup
    @fixture = RepositoryFixture.new

    @port_a = 10896
    @server_a = BERTRPCTestServer.new(@port_a, source_root: SOURCE_ROOT)

    @port_b = 10897
    @server_b = BERTRPCTestServer.new(@port_b, source_root: SOURCE_ROOT)

    @proto_a = GitRPC::Protocol::BERTRPC.new URI.parse("bertrpc://localhost:#{@port_a}#{@fixture.path}")
    @proto_b = GitRPC::Protocol::BERTRPC.new URI.parse("bertrpc://localhost:#{@port_b}#{@fixture.path}")

    @instrumenter = GitRPC.instrumenter = FakeInstrumenter.new
  end

  def teardown
    GitRPC.instrumenter = nil
    @fixture.teardown
    @server_a.stop
    @server_b.stop
  end

  def test_send_multiple_instrumentation
    @server_a.start
    @server_b.start

    args = [[123, "string", {"key" => "value"}]]
    results, errors = GitRPC::Protocol::BERTRPC.send_multiple([@proto_a, @proto_b], :echo, args)
    assert_equal 0, errors.length
    assert_equal 2, results.length

    events = @instrumenter.events
    event = events.find do |name, payload|
      name == "bertrpc_send_message.gitrpc" && payload[:url] == @proto_a.url
    end

    name, payload = event
    assert_equal :echo, payload[:name]
    assert_equal args, payload[:args]
  end

  def test_send_multiple_timeout_instrumentation
    GitRPC.timeout = 0.100

    @server_a.start
    @server_b.start

    args = [2]
    results, errors = GitRPC::Protocol::BERTRPC.send_multiple([@proto_a, @proto_b], :slow, args)
    assert_equal 2, errors.length
    assert_equal 0, results.length

    events = @instrumenter.events
    event = events.find do |name, payload|
      name == "bertrpc_send_message.gitrpc" && payload[:url] == @proto_a.url
    end

    name, payload = event
    assert_equal :slow, payload[:name]
    assert_equal args, payload[:args]

    # This exception class is different that the singular send_message case.
    # I don't know why that is, if it's on purpose, I just found it in testing.
    assert_equal "BERTRPC::ReadTimeoutError", payload[:exception][0]
  ensure
    GitRPC.timeout = nil
  end
end
