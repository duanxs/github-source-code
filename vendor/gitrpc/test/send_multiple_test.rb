# frozen_string_literal: true
require File.expand_path("../setup", __FILE__)

require "gitrpc"
require "repository_fixture"
require "protocol/bertrpc_test_server"

class TestGitRPCSendMultiple < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @fixture.setup

    @port1 = 10896
    @port2 = 10897
    @server1 = BERTRPCTestServer.new(@port1)
    @server2 = BERTRPCTestServer.new(@port2)
    @bertrpc_url1 = "bertrpc://localhost:#{@port1}/"
    @bertrpc_url2 = "bertrpc://localhost:#{@port2}/"
  end

  def teardown
    @fixture.teardown
    @server1.stop
    @server2.stop
  end

  def test_one_file
    url = "file:#{@fixture.path}"
    answers, errors = GitRPC.send_multiple([url], :echo, ["holla back"])
    assert_empty errors, "no errors"
    assert_equal "holla back", answers[url], "answer from #{url}"
    assert_equal 1, answers.size, "one answer"
  end

  def test_error_from_file
    url = "file:#{@fixture.path}"
    answers, errors = GitRPC.send_multiple([url], :boomtown, [ArgumentError])
    assert_empty answers, "no answers"
    assert_instance_of GitRPC::Failure, errors[url], "error from #{url}"
    assert_instance_of ArgumentError, errors[url].original, "wrapped error from #{url}"
    assert_equal 1, errors.size, "one error"
  end

  def test_multiple_bertrpc
    @server1.start
    @server2.start
    answers, errors = GitRPC.send_multiple([@bertrpc_url1, @bertrpc_url2], :echo, ["holla back"])
    assert_empty errors, "no errors"
    assert_equal "holla back", answers[@bertrpc_url1], "answer from #{@bertrpc_url1}"
    assert_equal "holla back", answers[@bertrpc_url2], "answer from #{@bertrpc_url2}"
    assert_equal 2, answers.size, "number of answers"
  end

  def test_multiple_bertrpc_in_parallel
    @server1.start
    @server2.start
    start = Time.now
    answers, errors = GitRPC.send_multiple([@bertrpc_url1, @bertrpc_url2], :slow, [1.0])
    duration = Time.now - start
    assert_in_delta duration, 1.0, 0.5, "two 'sleep 1' calls should go in parallel"
    assert_equal 2, answers.size, "number of answers"
    assert_equal 0, errors.size, "number of errors"
  end

  # This is a test of the BERTRPC interface that the main app
  # and that the DGit protocol use.
  def test_bertrpc_send_multiple_api
    @server1.start
    @server2.start
    route1 = FakeRoute.new(@port1)
    route2 = FakeRoute.new(@port2)
    answers, errors = GitRPC::Protocol::BERTRPC.send_multiple \
      [route1, route2], :echo, ["hi"],
      timeout: 1, connect_timeout: 1
    assert_empty errors, "errors"
    assert_includes answers.keys, route1, "route1 should provide an answer"
    assert_includes answers.keys, route2, "route2 should provide an answer"
    assert_equal 2, answers.size, "nubmer of answers"
  end
  class FakeRoute
    def initialize(port)
      @port = port
    end
    # Implement enough that BERTRPC.from_route will work.
    attr_reader :port
    def resolved_host; "127.0.0.1"; end
    def path; "/"; end
  end

  def test_multi_protocol_errors
    url1 = "file:/"
    url2 = "fakerpc://127.0.0.1/"
    url3 = @bertrpc_url1
    url4 = @bertrpc_url2
    @server1.start # only start this one

    answers, errors = GitRPC.send_multiple([url1, url2, url3, url4], :boomtown, ["boom message"])

    assert_empty answers, "no answers"
    assert_instance_of GitRPC::Failure, errors[url1], "error from #{url1} should be the boom error"
    assert_instance_of GitRPC::Failure, errors[url2], "error from #{url2} should be the boom error"
    assert_instance_of GitRPC::Failure, errors[url3], "error from #{url3} should be the boom error"
    assert_instance_of GitRPC::ConnectionError, errors[url4], "error from #{url4} should be a connect error"
  end
end
