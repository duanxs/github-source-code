# frozen_string_literal: true
require_relative "setup"

require "gitrpc/util/hashcache"

class CacheTest < Minitest::Test
  def setup
    @hash = {}
    @cache = GitRPC::Util::HashCache.new(@hash)
  end

  def test_set
    @cache.set("some:string", "some value")
    assert_equal "some value", Marshal.load(hashget("some:string"))

    @cache.set("some:array", [1, 2, 3])
    assert_equal [1, 2, 3], Marshal.load(hashget("some:array"))

    @cache.set("some:hash", {"k" => 123})
    assert_equal({"k" => 123}, Marshal.load(hashget("some:hash")))
  end

  def test_get
    hashset "some:string", Marshal.dump("some value")
    assert_equal "some value", @cache.get("some:string")

    hashset "some:array", Marshal.dump([1, 2, 3])
    assert_equal [1, 2, 3], @cache.get("some:array")

    hashset "some:hash", Marshal.dump({"k" => 123})
    assert_equal({"k" => 123}, @cache.get("some:hash"))
  end

  def test_hash_symbol_keys
    orig = { :key => "value" }
    @cache.set("some:symbol-hash", orig)
    assert_equal({:key => "value"}, @cache.get("some:symbol-hash"))
  end

  def test_fetch
    val = @cache.fetch("some:missing-value") { [1, 2, 3] }
    assert_equal [1, 2, 3], val

    val = @cache.fetch("some:missing-value") { fail }
    assert_equal [1, 2, 3], val
  end

  def test_fetch_and_get_use_same_keys
    val = @cache.fetch("some:value") { "set by fetch" }
    assert_equal "set by fetch", val

    val = @cache.get("some:value")
    assert_equal "set by fetch", val

    @cache.fetch("some:value") { fail "should hit and not enter here" }
  end

  def test_get_multi
    @cache.set("a", [1, 2, 3])
    @cache.set("b", nil)
    values = @cache.get_multi(["a", "b", "c"])
    assert_equal [1, 2, 3], values["a"]
    assert_nil values["b"]
    assert_nil values["c"]
  end

  def test_delete
    hashset "some:value", "blah"
    @cache.delete("some:value")
    assert_nil hashget "some:value"
  end

  def hashget(key)
    @hash[key]
  end

  def hashset(key, value)
    @hash[key] = value
  end
end
