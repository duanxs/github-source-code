# frozen_string_literal: true
module RepositoryFixtures
  module AdjustedPositions
    EXPECTED_README_MAPPINGS = {
      4     => 5,
      5     => nil,
      6     => 9,
      7     => nil,
      8     => nil,
      10000 => 10003,
      0     => 0,
      1     => 1,
      2     => 2,
      3     => 4
    }

    EXPECTED_NOTES_MAPPINGS = {
      0     => nil,
      1     => nil,
      2     => nil,
      3     => 0,
      4     => 1,
      5     => 4,
      6     => 5,
      7     => nil,
      8     => nil
    }

    # diff --git a/README b/README2
    # index 00b7376..b54e6e8 100644
    # --- a/README
    # +++ b/README
    # @@ -3,0 +4 @@ C
    # +$
    # @@ -6 +7,3 @@ E
    # -F
    # +E
    # +E
    # +E
    # @@ -8,2 +11,2 @@ G
    # -H
    # -I
    # +E
    # +E
    # diff --git a/notes b/notes
    # index 0719398..1c83124 100644
    # --- a/notes
    # +++ b/notes
    # @@ -1,3 +0,0 @@
    # -1
    # -2
    # -3
    # @@ -5,0 +3,2 @@
    # +5.5
    # +5.7
    # @@ -8,2 +7,3 @@
    # -8
    # -9
    # +10
    # +11
    # +12

    def setup_positioning_fixture
      unless @fixture
        @fixture = RepositoryFixture.new
        @fixture.setup
      end

      @fixture.commit_files({
        "README" => %w(A B C D E F G H I).join("\n"),
        "notes"  => %w(1 2 3 4 5 6 7 8 9).join("\n")
      }, "commit 1")
      @fixture.commit_files({
        "README"  => nil,
        "README2" => %w(A B C $ D E E E E G E E).join("\n"),
        "notes"   => %w(4 5 5.5 5.7 6 7 10 11 12).join("\n")
      }, "commit 2")

      @fixture
    end
  end
end
