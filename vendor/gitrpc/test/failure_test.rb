# frozen_string_literal: true
require_relative "setup"

require "gitrpc/failure"

class TestFailure < Minitest::Test
  def setup
    @runtime_error = exception { fail "boom" }
    @failure = GitRPC::Failure.new(@runtime_error)
  end

  def teardown
  end

  def exception
    yield
  rescue => boom
    boom
  end

  def test_original_attribute
    assert_same @runtime_error, @failure.original
  end

  def test_message_attribute
    assert_equal "RuntimeError: boom", @failure.message
  end

  def test_backtrace_inheritance
    assert_equal @runtime_error.backtrace, @failure.backtrace
  end

  def test_encode
    res = @failure.encode
    assert_equal "RuntimeError", res[0]
    assert_equal "boom", res[1]
    assert_equal @runtime_error.backtrace, res[2]
    assert_equal({}, res[3])
  end

  class TestException < StandardError
    def failbot_context
      { "something" => "value" }
    end
  end

  def test_encode_with_failbot_context
    error = exception { raise TestException, "boom" }
    failure = GitRPC::Failure.new(error)
    res = failure.encode
    assert_equal "TestFailure::TestException", res[0]
    assert_equal "boom", res[1]
    assert_equal error.backtrace, res[2]
    assert_equal({"something" => "value"}, res[3])
  end

  def test_decode
    error = ["RuntimeError", "boooom", ["blah.rb"], {"something" => "value"}]
    exception = GitRPC::Failure.decode(error)
    assert exception.kind_of?(RuntimeError)
    assert_equal "boooom", exception.message
    assert_equal ["blah.rb"], exception.backtrace
    assert_equal "value", exception.something
    assert_equal({"something" => "value"}, exception.info)
  end

  def test_decode_submodule_exception
    error = ["TestFailure::TestException", "boooom", [], {}]
    exception = GitRPC::Failure.decode(error)
    assert exception.kind_of?(TestException)
    assert_equal "boooom", exception.message
  end

  def test_decode_raise
    error = GitRPC::Failure.new(@runtime_error).encode
    assert_raises GitRPC::Failure do
      GitRPC::Failure.raise(error)
    end
  end

  def test_decode_raise_core_type
    boom  = exception { raise GitRPC::ConnectionError }
    error = GitRPC::Failure.new(boom).encode
    assert_raises GitRPC::ConnectionError do
      GitRPC::Failure.raise(error)
    end
  end
end
