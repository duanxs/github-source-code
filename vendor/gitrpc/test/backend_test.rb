# frozen_string_literal: true
require_relative "setup"

require "gitrpc"
require "repository_fixture"

class BackendTest < Minitest::Test
  def setup
    @fixture = RepositoryFixture.new
    @backend = GitRPC::Backend.new(@fixture.path, {:timeout => 1.0, info: {real_ip: "127.0.0.1", repo_id: 1, repo_name: "maddox/test", user_id: 1}})
  end

  def teardown
    @fixture.teardown
  end

  def test_path
    assert_equal @fixture.path, @backend.path
  end

  def test_repository_key
    assert_equal Digest::MD5.hexdigest(@fixture.path), @backend.repository_key
  end

  def test_rugged
    @fixture.setup
    assert @backend.rugged
  end

  def test_rugged_missing_repository
    assert_raises(GitRPC::InvalidRepository) { @backend.rugged }
  end

  def test_rugged_malformed_repository
    @fixture.create_directory
    assert_raises(GitRPC::InvalidRepository) { @backend.rugged }
  end

  def test_timeout_option
    assert_raises GitRPC::Timeout do
      @backend.send_message(:slow, 5)
    end
  end

  def test_message_timeout_attribute
    @backend.options[:timeout] = nil
    assert @backend.message_timeout.nil?
    assert_equal [], @backend.send_message(:echo, [])

    @backend.options[:timeout] = 0.0
    assert @backend.message_timeout.nil?
    assert_equal [], @backend.send_message(:echo, [])

    @backend.options[:timeout] = 10.0
    assert @backend.message_timeout > @backend.options[:timeout]
  end

  def test_rugged_invalid_error_wrapping
    assert_raises(GitRPC::BadRepositoryState) do
      @backend.send_message(:boomtown, Rugged::InvalidError.new)
    end
  end

  def test_sockstat_vars_in_native_env
   native_env = @backend.native_env
   expected = {
     "GIT_DIR" => @backend.path,
     "GIT_LITERAL_PATHSPECS" => "1",
     "GIT_SOCKSTAT_VAR_real_ip" => @backend.options[:info][:real_ip],
     "GIT_SOCKSTAT_VAR_repo_name" => @backend.options[:info][:repo_name],
     "GIT_SOCKSTAT_VAR_repo_id" => "uint:#{@backend.options[:info][:repo_id]}",
     "GIT_SOCKSTAT_VAR_user_id" => "uint:#{@backend.options[:info][:user_id]}"
   }
   assert_equal expected, native_env
  end

  def test_extra_native_env_in_native_env
    old_extra_native_env = GitRPC.extra_native_env

    key = "TEST_ENV_VAR"
    expected = "test value"

    GitRPC.extra_native_env = { key => expected }

    native_env = @backend.native_env
    assert_equal expected, native_env[key]
    # stolen from test_sockstat_vars_in_native_env to ensure we're not
    # totally clobbering the hash
    assert_equal "1", native_env["GIT_LITERAL_PATHSPECS"]
  ensure
    GitRPC.extra_native_env = old_extra_native_env
  end

  def test_missing_trace2_option_ok
    refute @backend.trace2_enabled?(:nw_repack)
  end
end
