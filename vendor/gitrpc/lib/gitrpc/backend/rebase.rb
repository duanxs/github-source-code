# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Backend
    class RebaseTimeout < Timeout
    end

    rpc_writer :rebase
    def rebase(commit_oid, upstream_commit_oid, committer, options = {})
      commit = Rugged::Commit.lookup(rugged, commit_oid)
      upstream_commit = Rugged::Commit.lookup(rugged, upstream_commit_oid)

      committer = symbolize_keys(committer)
      committer[:time] = iso8601(committer[:time])

      rebase = Rugged::Rebase.new(rugged, commit, upstream_commit,
        inmemory: true,
        fail_on_conflict: true,
        skip_reuc: true,
        no_recursive: true
      )

      # Start from the upstream commit
      final_commit_oid = upstream_commit_oid

      deadline = options[:timeout] ? Process.clock_gettime(Process::CLOCK_MONOTONIC) + options[:timeout] : nil

      while (step = rebase.next)
        fail RebaseTimeout if deadline && deadline < Process.clock_gettime(Process::CLOCK_MONOTONIC)

        index = rebase.inmemory_index

        if index.nil?
          rebase.abort
          return nil
        end

        commit_oid = rebase.commit(committer: committer)
        final_commit_oid = commit_oid if commit_oid
      end

      rebase.finish(committer)
      return final_commit_oid
    rescue Rugged::MergeError
      rebase.abort
      return nil
    end
  end
end
