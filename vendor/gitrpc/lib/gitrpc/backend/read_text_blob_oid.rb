# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Backend
    # Fetch the first non-binary blob's oid from a gist repo.
    # This is typically used to serve as a default blob
    # for URLs like https://gist.github.com/12345.txt
    #
    # commit_oid - 40 char oid string identifying the commit
    #              of which to find the first text blob
    #
    # Returns blob's 40 char oid or nil
    rpc_reader :read_text_blob_oid
    def read_text_blob_oid(commit_oid)
      rugged.lookup(commit_oid).tree.walk_blobs do |root, blob|
        blob_oid = blob[:oid]
        header = rugged.read_header(blob_oid)
        blob = get_blob_object(blob_oid, header, false)

        return blob["oid"] unless blob["binary"]
      end

      nil
    rescue Rugged::TreeError => boom
      # this is likely because tree.path failed to find an entry
      # at the path requested
      raise ::GitRPC::ObjectMissing, boom.message
    end
  end
end
