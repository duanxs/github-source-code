# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Backend
    # Public: resolves the ideal base commit for the given repository and commit "triplet".
    class BaseOidResolver
      BASE_SHA_TREE_MERGE_OPTS = {
        favor:            :ours,
        rename_threshold: 50,
        renames:          true,
        skip_reuc:        true
      }

      def initialize(backend, rugged)
        @results = {}
        @rugged  = rugged
        @backend = backend
      end

      # Public: Resolves the ideal base object for the given commits and caches the result.
      #   Note: This method writes to the git repository and therefore the result cannot be
      #         guaranteed to remain consistent across file servers.
      #
      # commit1_oid     - String oid of the start of the range.
      # commit2_oid     - String oid of the end of the range.
      # base_commit_oid - String oid of the base commit from the base branch.
      #
      # Returns a string oid of the best commit or proxy tree
      def resolve(commit1_oid, commit2_oid, base_commit_oid)
        args = [commit1_oid, commit2_oid, base_commit_oid]
        return results[args] if results.key?(args)

        results[args] = resolve!(commit1_oid, commit2_oid, base_commit_oid)
      end

      private

      attr_reader :rugged, :backend, :results

      # Internal: Find the rugged commit object or raise if it cannot be found.
      def lookup_commit(oid)
        return nil if oid.nil?

        commit = rugged.lookup(oid)
        if commit.type != :commit
          raise GitRPC::InvalidObject, "Invalid object type, expected commit but was #{commit.type}"
        end
        commit
      rescue Rugged::OdbError => e
        if e.message =~ /object not found - no match for (id)|(prefix) \((.+)\)/
          raise ObjectMissing.new(e.message, $1)
        else
          raise e
        end
      end

      def resolve!(commit1_oid, commit2_oid, base_commit_oid)
        commit1     = lookup_commit(commit1_oid)
        commit2     = lookup_commit(commit2_oid)
        base_commit = lookup_commit(base_commit_oid)

        if base_commit.nil? || commit1 == base_commit
          return commit1&.oid
        end

        commit1_base = best_merge_base(base_commit, commit1)
        commit2_base = best_merge_base(base_commit, commit2)

        # If head commits originate from the same merge base, just do a simple
        # diff. No hunks to filter out.
        return commit1.oid if commit1_base && (commit1_base.oid == commit2_base.oid)

        # Compare latest against new merge base if range starts at original merge base.
        # Nothing to filter.
        return commit2_base.oid if commit1_base && (commit1_base.oid == commit1.oid)

        return commit2_base.oid if commit1_base.nil? && commit1.nil?

        index = commit1.tree.merge(commit2_base.tree, commit1_base && commit1_base.tree,
          BASE_SHA_TREE_MERGE_OPTS)

        # If there was a merge conflict, fallback to a regular diff and flag it
        # as degraded.
        return commit1.oid if !index || index.conflicts?

        index.write_tree(rugged)
      end

      def best_merge_base(commit1, commit2)
        return nil if commit1.nil? || commit2.nil?

        if oid = backend.native_merge_base(commit1.oid, commit2.oid, "best-merge-base")
          Rugged::Commit.lookup(rugged, oid)
        end
      end
    end

    def resolve_base_oid(commit1_oid, commit2_oid, base_commit_oid)
      # cache the resolver for the lifetime of this Backend instance. This allows us to cache
      # the results of multiple invocations.
      @resolver ||= BaseOidResolver.new(self, rugged)
      @resolver.resolve(commit1_oid, commit2_oid, base_commit_oid)
    end

    # We can only accurately determine an ideal base oid (tree or commit) if all of
    # oid1, oid2, and base_oid are commits. But sometimes a diff might need to be
    # between a tree and a commit, or two blobs, etc for reasons. In these
    # cases, we safely fail the resolution process but just default to oid1 instead
    # of raising an error.
    def resolve_base_for_diff(oid1, oid2, base_oid)
      resolve_base_oid(oid1, oid2, base_oid)
    rescue GitRPC::InvalidObject => e
      oid1
    end
  end
end
