# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Backend
    # Public: Get the raw text for a diff
    #
    # commit1_oid - a commit sha1
    # commit2_oid - a commit sha1
    #
    # Returns the raw diff text as a String
    rpc_reader :native_diff_text
    def native_diff_text(commit1_oid, commit2_oid, paths = [], full_index: false)
      paths = Array(paths)
      commit1_oid, commit2_oid = select_commits(commit1_oid, commit2_oid)

      if (commit1_oid && !sha_valid?(commit1_oid)) || !sha_valid?(commit2_oid)
        raise GitRPC::InvalidObject
      end

      argv = DIFF_TREE_DETECT_RENAMES + %w[-p]
      argv << "--full-index" if full_index

      if commit1_oid.nil?
        argv += [EMPTY_TREE_OID, commit2_oid]
      else
        base_oid = merge_base(commit1_oid, commit2_oid)
        if base_oid
          argv += [base_oid, commit2_oid]
        else
          # No merge base found, just do a direct diff between the commits
          argv += [commit1_oid, commit2_oid]
        end
      end

      unless paths.empty?
        argv << "--"
        argv += paths
      end

      res = spawn_git("diff-tree", argv)
      if res["ok"]
        res["out"]
      else
        raise GitRPC::Error, res["err"]
      end
    end

    # Public: Get the raw text for a diff in patch format
    #
    # commit1_oid - a commit sha1
    # commit2_oid - a commit sha1
    #
    # Returns the diff text in patch format as a String
    rpc_reader :native_patch_text
    def native_patch_text(commit1_oid, commit2_oid = nil, full_index: false)
      commit1_oid, commit2_oid = select_commits(commit1_oid, commit2_oid)

      if (commit1_oid && !sha_valid?(commit1_oid)) || !sha_valid?(commit2_oid)
        raise GitRPC::InvalidObject
      end

      argv = ["--stdout", "--no-signature"]
      if commit1_oid.nil?
        argv += ["--root", commit2_oid]
      else
        argv << "#{commit1_oid}..#{commit2_oid}"
      end

      argv << "--full-index" if full_index

      res = spawn_git("format-patch", argv)
      if res["ok"]
        res["out"]
      else
        raise GitRPC::Error, res["err"]
      end
    end

    # Private: Determine the two oids necessary to perform a diff.
    #
    # This is a utility method that will fill in the parent commit if one
    # hasn't been specified by the caller.
    def select_commits(commit1_oid, commit2_oid = nil)
      if commit2_oid
        return commit1_oid, commit2_oid
      else
        raise GitRPC::InvalidObject unless sha_valid?(commit1_oid)
        commit = read_commits([commit1_oid]).first
        if commit["parents"].empty?
          return nil, commit1_oid
        else
          return commit["parents"][0], commit1_oid
        end
      end
    end

    ENCODING_DETECTION_LIMIT=50*1024

    def blob_encoding(oid)
      if oid && oid != GitRPC::NULL_OID
        blob = rugged.lookup(oid)
        content = blob.content(ENCODING_DETECTION_LIMIT)
        GitRPC::Encoding.guess_and_tag(content)
      end
    end

    TYPES = {
      :addition  => "+",
      :deletion  => "-",
      :context   => " ",
      :nonewline => '\\'
    }
  end
end
