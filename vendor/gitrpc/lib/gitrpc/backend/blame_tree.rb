# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Backend
    require "msgpack"
    require "lmdb"

    # Just a place to put the data together
    class BlameTreeCache
      include GitRPC::Util

      CACHE_VERSION = "v1"

      def initialize(path)
        @path = path

        nwdir = File.join(File.dirname(path), "network.git")
        base = Dir.exist?(nwdir) ? nwdir : @path
        @cache_path = File.join(base, "blame-tree-cache", CACHE_VERSION)
      end

      def has_cache?
        Dir.exist?(@cache_path)
      end

      # Returns the list of commits for which we have a cache
      def cached_commits
        return [] unless Dir.exist?(@cache_path)
        @cached ||= Dir.entries(@cache_path).select { |n| valid_full_sha1?(n) }
      end

      # Read and parse the cache for a particular cache point
      def cache_for(commit, dir)
        @caches ||= {}

        dir = "/" if !dir || dir.empty?
        return @caches[[commit, dir]] if @caches[[commit, dir]]

        env = nil
        begin
          env = LMDB.new(File.join(@cache_path, commit))
          @caches[[commit, dir]] = MessagePack.unpack(env.database[dir])
        ensure
          env.close unless env.nil?
        end
      end

      # Go through the trees reachable from +commit+'s root and
      # associate the data from +blame+
      #
      # Returns a map of tree paths to maps of entries to
      # [blamed_commit, current_id]. The root tree is returned as "/".
      #
      # In a different language one might write
      #
      #     Commit -> Map String TreeEntry-> Map String (Commitid, EntryId)
      #
      # The return value is the format we store in the database.
      def self.cache_per_tree(commit, blame)
        trees = {}
        commit.tree.walk(:postorder) do |root, entry|
          basepath = root.empty? ? "/" : root.chomp("/")
          trees[basepath] = {} unless trees[basepath]

          pathname = "#{root}#{entry[:name]}"
          blamed = blame[pathname]
          oid = entry[:oid]
          trees[basepath][entry[:name]] = [oid, blamed]
        end
        trees
      end

      # Write the data for +cache_point+ and +data+ into the cache
      def write(cache_point, blame_data)
        FileUtils.mkdir_p(@cache_path)
        cachedir = Dir.mktmpdir("blame_tree_cache", @cache_path)
        # In production map 1GB to make sure we don't run out of space
        # while we're writing. Disk space will be allocated as needed
        # and we're not short of address space. It should also stop
        # extreme cases from running away with disk usage.
        #
        # Anywhere else, especially for OSX where we don't have sparse
        # files, allocate 32MB which is enough for any data we want to
        # test with.
        map_size =
          if ENV["RAILS_ENV"] == "production"
            1024*1024*1024 # 1GB
          else
            32*1024*1024 # 32MB
          end

        env = nil
        begin
          env = LMDB.new(cachedir, :mapsize => map_size)
          db = env.database
          blame_data.each do |dir, data|
            env.transaction do
              db[dir] = MessagePack.pack(data)
            end
          end
        ensure
          env.close unless env.nil?
        end
        File.rename(cachedir, File.join(@cache_path, cache_point))
      rescue
        FileUtils.remove_entry(cachedir) if cachedir
        raise
      end

      # The blame-tree command may not give us any of the limits we
      # pass in, but whichever commit it was when it finished its
      # traversal. We thus cannot use the commit as the key for the
      # cache but must search for whichever chache point has the same
      # object id for the path as the tip.
      def fill_endpoints(at_tip, map, dir)
        map.map do |path, commit|
          if commit[0] == "^"
            needle = at_tip[path]
            found = nil
            cached_commits.each do |cp|
              cache = cache_for(cp, dir)
              # we store relative paths in the cache
              relpath = (dir.nil? || dir.empty?) ? path : path[dir.length+1..-1]
              oid, blamed = cache[relpath]
              if oid == needle
                found = blamed
                break
              end
            end
            raise GitRPC::Error.new("Blame point not found for '#{path}'") unless found
            [path, found]
          else
            [path, commit]
          end
        end.to_h
      end
    end

    def parse_blame(data)
      map = {}
      data.split("\0").map do |line|
        commit_oid, path = line.split("\t", 2)
        commit_oid.force_encoding "utf-8"
        map[path] = commit_oid
      end
      map
    end

    # Internal: spawn blame-tree with the given limits
    #
    # This is split out so we can test more easily and so we have
    # different bits for grabbing the list and refilling it from the
    # cache.
    def blame_tree_raw(commit_oid, path = nil, recursive = true, endpoints = nil, include_trees = false, fast: false)
      ensure_valid_sha1(commit_oid)

      opts = []
      argv = ["--"]

      opts << "--go-faster" if fast # must be the first argument
      opts << "-z"

      # blame-tree wants 0 for pathless non-recursive blames
      if !recursive
        if path && !path.empty?
          opts << "--max-depth=1"
        else
          opts << "--max-depth=0"
        end
      end

      if path && !path.empty?
        argv << path
      end

      opts << "-t" if include_trees

      limits = []
      if endpoints && !endpoints.empty?
        limits = endpoints.map { |limit| "^#{limit}" }
      end

      res = spawn_git("blame-tree", [opts, commit_oid, limits, argv].flatten)
      if res["ok"]
        parse_blame(res["out"])
      else
        raise ::GitRPC::Error, res["err"]
      end
    end

    # Converts a particular cache point into a blame so we can return
    # immediately if we're asked exactly for a cache point
    def cache_point_as_blame(cache, commit_oid, dirname)
      pfx = dirname.nil? ? "" : "#{dirname}/"
      cache.cache_for(commit_oid, dirname).map do |path, (_entry_oid, blame_oid)|
        ["#{pfx}#{path}", blame_oid]
      end.to_h
    end

    rpc_reader :blame_tree
    def blame_tree(commit_oid, path = nil, recursive = true, use_cache = true, fast: false)
      cachepoints = []
      blame_cache = nil

      if use_cache && !recursive
        blame_cache = BlameTreeCache.new(@path)
        cachepoints = blame_cache.cached_commits
        return cache_point_as_blame(blame_cache, commit_oid, path) if cachepoints.include?(commit_oid)
      end

      # We only want those entries which are ancestors of the commit
      # we're looking up so we don't end up hiding our commit
      cached = descendant_of(cachepoints.map { |c| [commit_oid, c] }).
        select { |_pair, is_ancestor| is_ancestor }.
        map { |(_commit_oid, ancestor), _is_ancestor| ancestor }

      map = blame_tree_raw(commit_oid, path, recursive, cached, fast: fast)

      # If there have been no changes to the path since the cache
      # point, blame-tree produces no output. In this case we can
      # treat it the same as if we'd been asked for the blame at that
      # point, since the results will be the same.
      return cache_point_as_blame(blame_cache, cached[0], path) if map.empty? && cached.length == 1

      return map unless use_cache && !recursive && blame_cache.has_cache?

      entries = read_tree_entries(commit_oid, path, nil, true)["entries"]
      at_tip = entries.map do |entry|
        [entry["path"], entry["oid"]]
      end.to_h
      blame_cache.fill_endpoints(at_tip, map, path)
    end

    # Private: fill in the cache for precisely +cache_point+
    #
    # This is what stores the data in the filesystem. It is called
    # from +blame_tree_fill_cache+ and it's split out for testing.
    def blame_tree_fill_cache_at(cache_point, fast: false)
      blame_cache = BlameTreeCache.new(@path)

      # We want to look up blames per-dir non-recursively, so the
      # quickest way to read that is to store precisely that into the
      # cache. We ask the blame for the directories as well. We then
      # have to put those trees as entries in their parent.
      pertree = GitRPC::Backend::BlameTreeCache.cache_per_tree(
        Rugged::Commit.lookup(rugged, cache_point),
        blame_tree_raw(cache_point, nil, true, nil, true, fast: fast))
      blame_cache.write(cache_point, pertree)
    end

    # Public: fill in the cache for a miss of +commit_oid+
    #
    # This is meant to be used in a background job triggered by a
    # blame-tree timeout.
    #
    # We go back +commits_back+ commits and raise Error if there isn't
    # enough history.
    rpc_writer :blame_tree_fill_cache
    def blame_tree_fill_cache(commit_oid, commits_back, fast: false)
      walker = Rugged::Walker.new(rugged)
      walker.push(commit_oid)
      walker.simplify_first_parent

      cache_point = walker.drop(commits_back).first
      raise GitRPC::Error.new("history does not go back far enough") unless cache_point
      blame_tree_fill_cache_at(cache_point.oid, fast: fast)
    end
  end
end
