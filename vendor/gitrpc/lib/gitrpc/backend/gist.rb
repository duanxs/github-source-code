# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Backend
    # Fetch the first non-auto generated filename from a gist
    # repository. This is typically used as the gist's title.
    #
    # Returns a String or nil
    rpc_reader :gist_title
    def gist_title(oid)
      rugged.lookup(oid).tree.walk_blobs do |root, blob|
        filename = blob[:name]

        return filename if filename !~ /^gistfile/
      end

      nil
    rescue Rugged::OdbError, Rugged::TreeError => boom
      # this is likely because tree.path failed to find an entry
      # at the path requested
      raise ::GitRPC::ObjectMissing, boom.message
    end

  end
end
