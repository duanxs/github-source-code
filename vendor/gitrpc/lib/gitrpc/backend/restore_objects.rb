# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Backend
    rpc_writer :restore_objects, output_varies: true
    def restore_objects
      argv = ["-r", "--fast"]
      spawn_git("restore-objects", argv)
    end
  end
end
