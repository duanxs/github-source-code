# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Backend
    GPG_SIGNATURE_PREFIX   = "-----BEGIN PGP SIGNATURE-----".freeze
    SMIME_SIGNATURE_PREFIX = "-----BEGIN SIGNED MESSAGE-----".freeze

    # Read the signature and signing payload from commit headers.
    #
    # oids - An Array of String OIDs. Must be 40 char sha1s.
    #
    # See Client#parse_commit_signature for usage documentation.
    rpc_reader :parse_commit_signatures
    def parse_commit_signatures(oids)
      oids.map do |oid|
        begin
          Rugged::Commit.extract_signature(rugged, oid) || false
        rescue Rugged::OdbError => e
          raise GitRPC::ObjectMissing.new(e.message, oid)
        rescue Rugged::InvalidError => e
          raise GitRPC::InvalidObject, "Invalid object type, expected commit"
        rescue Rugged::ObjectError => e
          raise GitRPC::InvalidObject, "Invalid object format, malformed commit"
        end
      end
    end

    # Read the signatures and signing payloads from tags.
    #
    # oids - An Array of String OIDs. Must be 40 char sha1s.
    #
    # See Client#parse_tag_signatures for usage documentation.
    rpc_reader :parse_tag_signatures
    def parse_tag_signatures(oids)
      oids.map do |oid|
        begin
          Rugged::Tag.extract_signature(rugged, oid, GPG_SIGNATURE_PREFIX) ||
          Rugged::Tag.extract_signature(rugged, oid, SMIME_SIGNATURE_PREFIX) ||
          false
        rescue Rugged::OdbError => e
          raise GitRPC::ObjectMissing.new(e.message, oid)
        rescue Rugged::InvalidError => e
          raise GitRPC::InvalidObject, "Invalid object type, expected tag"
        rescue Rugged::ObjectError => e
          raise GitRPC::InvalidObject, "Invalid object format, malformed tag"
        end
      end
    end
  end
end
