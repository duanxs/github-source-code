# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Backend
    rpc_reader :read_symbolic_ref
    def read_symbolic_ref(refname)
      ensure_valid_refname(refname)

      ref = rugged.references[refname]
      raise GitRPC::InvalidReferenceName, refname if ref.nil?
      raise GitRPC::InvalidReferenceName, refname if ref.type == :direct

      ref.target_id
    end
  end
end
