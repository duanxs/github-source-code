# frozen_string_literal: true

require_relative "commit_blob_data"
require_relative "simple_blob_data"
require_relative "offset_calculator"
require_relative "patch_resolver"

module GitRPC
  class Backend
    module ReadAdjustedPositions

      # Encapsulates one set of positioning data and provides an easy method for
      # retrieving the adjusted positions.
      class PositioningData

        # The original lookup data, useful for the hash returned to the GitRPC Client
        attr_reader :blob_lookup_data

        # A BlobData describing the destination blob of this mapping
        attr_reader :destination

        # An array of Integer positions in the source blob
        attr_reader :positions

        # A BlobData describing the source blob of this mapping
        attr_reader :source

        # Raise errors, or suppress and assign the value :bad?
        attr_reader :skip_bad
        alias skip_bad? skip_bad

        def initialize(positions, source, destination, skip_bad: false, backend:)
          @blob_lookup_data = [source, destination]
          @positions        = positions.dup
          @skip_bad         = skip_bad
          @backend          = backend

          @destination      = from_target(destination)
          @source           = from_target(source)
        end

        # Public: map the source positions to their corresponding destination positions.
        #   Mapping failures will either raise, or return :bad, depending on the value of
        #   :skip_bad.
        #
        # backend - GitRPC::Backend
        #
        # Returns a Hash of source positions to destination positions for the given
        #   source and destination blobs.
        def adjusted_positions_map
          Hash[positions.zip(adjusted_positions)]
        end

        private

        attr_reader :backend

        # Internal: Convert positions (0-based) to line numbers (1-based)
        def linenos
          return @linenos if defined?(@linenos)
          @linenos = positions.map { |p| p + 1 }
        end

        # Internal: loads the patch and calculates the corresponding line in the b blob
        # for each requested line in the a blob. Errors are handled according to the value
        # of `skip_bad`.
        #
        # backend - GitRPC::Backend
        #
        # Returns an array of Integer adjusted linenos matching those requested in linenos.
        def adjusted_positions
          patch = PatchResolver.load_patch(source, destination)
          return positions unless patch

          sort_map = adjusted_linenos_map(patch)

          linenos.map do |lineno|
            adjusted_lineno = sort_map[lineno]
            lineno_to_position(adjusted_lineno)
          end
        rescue GitRPC::InvalidObject, GitRPC::ObjectMissing, GitRPC::NoSuchPath => e
          return Array.new(positions.count, :bad) if skip_bad?
          fail e
        end

        # Internal: Returns a map of source linenos to destination linenos.
        #
        # patch - Rugged::Patch to use to calculate destination linenos.
        #
        # Returns a Hash of source to destination lineno integers.
        def adjusted_linenos_map(patch)
          sorted = linenos.sort
          adjusted = OffsetCalculator.adjusted_linenos_for_patch(patch, sorted)
          Hash[sorted.zip(adjusted)]
        end

        # Internal: Convert line numbers (1-based) to positions (0-based)
        def lineno_to_position(lineno)
          return nil if lineno.nil? || lineno == :bad
          lineno - 1
        end

        # Internal: Creates an instance of either SimpleBlobData or CommitBlobData, depending
        #   on the type of target.
        #
        # target - a string for a blob oid target, otherwise a hash as described in CommitblobData
        def from_target(target)
          if target.is_a?(String)
            SimpleBlobData.new(blob_oid: target, repository: backend.rugged)
          else
            CommitBlobData.new(target, backend: backend)
          end
        end
      end
    end
  end
end
