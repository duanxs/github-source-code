# frozen_string_literal: true

require_relative "blob_data"

module GitRPC
  class Backend
    module ReadAdjustedPositions

      # Public: A subclass of BlobData which allows for dynamically creating a blob
      #  as a result of a proxy tree merge.
      class CommitBlobData < BlobData
        attr_reader :commit_oid, :start_commit_oid, :end_commit_oid, :base_commit_oid, :path

        # Public: hash is a Hash which must contain :path and either:
        #   1. :commit_oid on which the path resides to load the blob
        #   2. :start_commit_oid, :end_commit_oid, and :base_commit_oid which can be used to
        #     resolve a commit or proxy tree, usually from a diff.
        def initialize(hash, backend:)
          super(repository: backend.rugged)

          if hash[:start_commit_oid] && hash[:end_commit_oid]
            @start_commit_oid = hash[:start_commit_oid]
            @end_commit_oid   = hash[:end_commit_oid]
            @base_commit_oid  = hash[:base_commit_oid]
          else
            @commit_oid = hash[:commit_oid]
          end

          @path             = hash[:path]
          @backend          = backend
          @hash             = hash
        end

        # Public: Overridden from BlobData to allow for generation of the proxy tree
        #
        # Returns a String blob oid
        def blob_oid
          return @blob_oid if defined?(@blob_oid)

          fail GitRPC::InvalidObject, self unless valid?

          oid = resolve_base_oid
          fail GitRPC::InvalidObject, self if oid.nil?

          object = Rugged::Object.lookup(backend.rugged, oid)
          tree = object.type == :commit ? object.tree : object

          fail GitRPC::InvalidObject, self unless tree.type == :tree

          blob_data = tree.path(path)
          blob_data[:oid]
        rescue Rugged::TreeError
          fail GitRPC::NoSuchPath, path
        end

        def to_s
          @hash.to_s
        end

        private

        attr_reader :backend

        # Internal: If the start and end commits are present, we call `backend.resolve_base_oid` to
        #   possibly compute a proxy tree from those values. Otherwise, we return commit_oid.
        #
        # Returns the OID of the base commit or tree.
        def resolve_base_oid
          if resolvable?
            backend.resolve_base_oid(start_commit_oid, end_commit_oid, base_commit_oid)
          else
            commit_oid
          end
        end

        # Internal: Predicate indicating if this object will resolve an OID or just return the
        #   stored commit_oid
        def resolvable?
          start_commit_oid && end_commit_oid
        end

        # Internal: Valid if either commit_oid is present or the object is resolvable.
        def valid?
          commit_oid || resolvable?
        end
      end
    end
  end
end
