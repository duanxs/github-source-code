# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Backend
    # Public: Retrieve a list of commit objects for the given commit oids.
    #
    # oids - Array of oid strings. All oids must be 40 char sha1s.
    #
    # See Client#read_objects for usage documentation.
    def read_commits(oids, read_trailers: nil)
      read_objects(oids, :commit, false, read_trailers: read_trailers)
    end

    # Public: Retrieve a list of blob objects for the given blob oids.
    #
    # oids - Array of oid strings. All oids must be 40 char sha1s.
    #
    # See Client#read_objects for usage documentation.
    def read_blobs(oids)
      read_objects(oids, :blob)
    end

    # Public: Retrieve a list of tree objects for the given tree oids.
    #
    # oids - Array of oid strings. All oids must be 40 char sha1s.
    #
    # See Client#read_objects for usage documentation.
    def read_trees(oids)
      read_objects(oids, :tree)
    end

    # Public: Retrieve a list of tag objects for the given tag oids.
    #
    # oids - Array of oid strings. All oids must be 40 char sha1s.
    #
    # See Client#read_objects for usage documentation.
    def read_tags(oids)
      read_objects(oids, :tag)
    end

    # Public: Git object reading workhorse method. This method can read any of
    # the core git objects: commit, tree, blob, and tag. Each object must be
    # located, inflated, and converted to the simple object types.
    #
    # oids          - Array of oid strings. All oids must be 40 char sha1s.
    # type          - String object type for validation.
    # skip_bad      - return nil for commits which would have raised errors
    # read_trailers - one of :git, :regexp, or nil to indicate how/if  trailers should
    #                 be parsed. nil means no trailer parsing.
    #
    # Returns an array of simple hashes representing the objects requested.
    # Raises a GitRPC::ObjectMissing exception when any of the requested objects
    # could not be found.
    rpc_reader :read_objects
    def read_objects(oids = [], type = nil, skip_bad = false, read_trailers: nil)
      type = type.to_sym if type

      commits = {}
      objects = oids.map do |oid|
        begin
          header = rugged.read_header(oid)
          if type && header[:type] != type
            if skip_bad
              next
            else
              raise GitRPC::InvalidObject, "Invalid object type #{header[:type]}, expected #{type}"
            end
          end

          # convert from rugged's internal format to gitrpc's simple objects
          object = case header[:type]
          when :commit; get_commit_object(oid, header, skip_bad: skip_bad, read_trailers: read_trailers)
          when :blob;   get_blob_object(oid, header)
          when :tree;   get_tree_object(oid, header, skip_bad: skip_bad)
          when :tag;    get_tag_object(oid, header, skip_bad: skip_bad)
          else
            { "oid" => oid, "type" => "unknown" }
          end

          commits[object["oid"]] = object if read_trailers == :git && header[:type] == :commit

          # sanity check the oid of the object that comes back from rugged just to
          # be sure we're not accidentally returning the wrong object.
          if object && oid != object["oid"]
            fail "object oid (#{object['oid']}) does match input oid (#{oid})"
          end

          object
        rescue Rugged::OdbError, Rugged::TreeError => boom
          if skip_bad
            nil
          else
            raise GitRPC::ObjectMissing.new(boom.message, oid)
          end
        rescue Rugged::InvalidError => boom
          if skip_bad
            nil
          else
            error = GitRPC::BadObjectState.new(boom.message)
            error.set_backtrace(boom.backtrace)
            raise error
          end
        end
      end.compact

      set_commit_trailers(commits) if read_trailers == :git

      objects
    end

    # Public: Read object headers without loading the full objects.
    #
    # oids - Array of oid strings. All oids must be 40 char sha1s.
    #
    # Returns an array of simple hashes representing the objects requested.
    #
    # Raises a GitRPC::ObjectMissing exception when any of the requested objects
    # could not be found.
    rpc_reader :read_object_headers
    def read_object_headers(oids)
      oids.map do |oid|
        begin
          header = rugged.read_header(oid)
          {
            "type" => header[:type].to_s,
            "size" => header[:len]
          }
        rescue Rugged::OdbError, Rugged::TreeError => boom
          raise GitRPC::ObjectMissing.new(boom.message, oid)
        rescue Rugged::InvalidError => boom
          error = GitRPC::BadObjectState.new(boom.message)
          error.set_backtrace(boom.backtrace)
          raise error
        end
      end
    end

    COMMIT_LINE_MATCHER = /commit\s([a-zA-Z0-9]{40})/
    GIT_TRAILER_MATCHER = /^(?<key>.+)\:\s(?<value>[^\n]+)\n?/

    # Internal: Sets the "trailers" key of each commit object contained in the commits
    #   hash with a unique hash of trailer keys to lists of their values.
    #
    # commits - a hash of string oids to commit hashes as returned by get_commit_object
    def set_commit_trailers(commits)
      return if commits.empty?

      res = spawn_git("rev-list", ["--stdin", "--no-walk", "--format=%(trailers:only,unfold)"], commits.keys.join("\n"))
      raise ::GitRPC::Error, res["err"] unless res["ok"]

      current_commit = nil
      res["out"].each_line do |line|
        if line =~ COMMIT_LINE_MATCHER
          current_commit = commits[$1]
          current_commit["trailers"] = {}
        elsif match = line.match(GIT_TRAILER_MATCHER)
          next if current_commit.nil?

          key = match["key"].downcase.force_encoding(current_commit["encoding"])
          value = match["value"].force_encoding(current_commit["encoding"])

          current_commit["trailers"][key] ||= []
          current_commit["trailers"][key] << value
        end
      end

      # This is far less than ideal but using a Set for this would mean
      # serialization would blow up later.
      commits.each_value do |commit_hash|
        commit_hash["trailers"].each_value(&:uniq!)
      end
    end

    # This finds the substring of the message containing the trailers but does not
    # identify separate keys and values in that section.
    # NOTE: This confused me, so probably someone else will be confused too at some point.
    # The initial ".*" is greedy so it takes as much of the input as possible, leaving
    # the last batch of trailers if there are multiple sections of trailers delimited by
    # double newlines.
    TRAILER_SECTION_MATCHER = /.*\r?\n\r?\n([\w0-9][^\s]+\:.+)/mn

    # Given the trailer section of a commit message, this identifies the set of keys and
    # values contained within.
    TRAILER_MATCHER = /^([\w0-9][^\s]+)\: (.+?(\r?\n[\t ]+.+?$)*$)/mn

    # Internal: Parse trailers out of the commit message. Trailers are represented as a hash
    #   of keys to arrays of values with matching keys, in order of occurrance in trailers
    #   of message.
    def parse_trailers(message, message_encoding)
      trailers = {}

      return trailers if message.nil?
      return trailers if !message.b.match(TRAILER_SECTION_MATCHER)

      trailer_text = $1
      matches = trailer_text.scan(TRAILER_MATCHER)

      matches.each do |key, value|
        key.force_encoding(message_encoding)
        value.force_encoding(message_encoding)

        key = key.downcase
        trailers[key] ||= []
        trailers[key] << value.split("\n").map(&:strip).join(" ")
      end

      # $' is a special variable containing the "post_match" portion of the string
      # last matched on. We want to make sure there is no text following the trailers
      # that is not itself a trailer. If that is present, then the trailers are not truly trailers
      # and therefore should be thrown away.
      trailers = {} unless $'&.strip&.empty?

      trailers
    end

    # Public: Retrieve a non-truncated blob object for the blob oid.
    #
    # oid - An oid string. Must be 40 char sha1s.
    #
    # See Client#read_large_object for usage documentation.
    rpc_reader :read_full_blob
    def read_full_blob(oid)
      header = rugged.read_header(oid)
      if header[:type] != :blob
        raise GitRPC::InvalidObject, "Invalid object type #{header[:type]}, expected blob"
      end

      object =  get_blob_object(oid, header, nil, nil)

      # sanity check the oid of the object that comes back from rugged just to
      # be sure we're not accidentally returning the wrong object.
      if oid != object["oid"]
        fail "object oid (#{object.oid}) does match input oid (#{oid})"
      end

      object
    rescue Rugged::OdbError => boom
      raise GitRPC::ObjectMissing.new(boom.message, oid)
    end

    # Internal: Maximum size we'll allow and truncate commit messages at.
    # If a message is longer than this value, we'll truncate it and return that.
    #
    # This value is kept here mostly so it can be stubbed by tests and
    # documented. It is not meant to be changed during normal usage of the
    # library.
    @commit_message_max_length = 65535
    (class <<self; attr_accessor :commit_message_max_length; end)

    # Lookup a Rugged::Commit and converts into GitRPC's simple object format.
    def get_commit_object(oid, header, skip_bad: false, read_trailers: false)
      commit = rugged.lookup(oid)
      encoding = message_encoding(commit)
      truncated_message = commit.message[0, Backend.commit_message_max_length]
      was_truncated = truncated_message.size < commit.message.size

      commit_hash = {
        "oid"               => commit.oid,
        "type"              => "commit",
        "tree"              => commit.tree_oid,
        "parents"           => commit.parent_oids,
        "author"            => convert_author(commit.author),
        "committer"         => convert_author(commit.committer),
        "message"           => truncated_message,
        "message_truncated" => was_truncated,
        "message_shas"      => extract_and_expand_shas(truncated_message),
        "encoding"          => encoding,
        "has_signature"     => commit.header_field?("gpgsig")
      }

      commit_hash["trailers"] = parse_trailers(commit.message, encoding) if read_trailers == :regexp
      commit_hash
    rescue Rugged::ObjectError => e
      if skip_bad
        nil
      else
        if e.message =~ /failed to parse/
          raise GitRPC::InvalidObject, "Invalid commit object #{oid}"
        else
          raise
        end
      end
    end

    def extract_and_expand_shas(message)
      if message && message.valid_encoding?
        if !message.encoding.ascii_compatible?
          message = message.encode(::Encoding::UTF_8, :undef => :replace)
        end
        expand_shas(message.scan(/\b[0-9a-f]{7,40}\b/), "commit")
      else
        {}
      end
    end

    # Convert a Rugged author hash into an author tuple.
    #
    # author - Rugged author hash or nil.
    #
    # Returns a [name, email, time] tuple or nil when author is nil. The name
    # and email values are always strings. The time value is a [unixtime, offset]
    # array where both values are integers.
    def convert_author(author)
      return if author.nil?
      [author[:name].scrub!, author[:email].scrub!, [author[:time].to_i, author[:time].utc_offset]]
    end

    # Lookup a Rugged tree object and convert to GitRPC's simple object format.
    def get_tree_object(oid, header, skip_bad: false)
      tree = rugged.lookup(oid)

      { "oid"       => tree.oid,
        "type"      => "tree",
        "entries"   => convert_tree_entries(tree) }
    rescue Rugged::TreeError => e
      if skip_bad
        nil
      else
        if e.message =~ /failed to parse/
          raise GitRPC::InvalidObject, "Invalid tree object #{oid}"
        else
          raise
        end
      end
    end

    # Convert a Rugged tree entry into GitRPC's simple object format.
    def convert_tree_entries(tree)
      entries = {}
      tree.each do |entry|
        entries[entry[:name]] = {
          "oid"  => entry[:oid],
          "type" => entry[:type].to_s,
          "mode" => entry[:filemode],
          "name" => entry[:name]
        }
      end
      entries
    end

    # Internal: Maximum amount of bytes of blob data to attempt to read from
    # disk into memory. If this limit is exceeded, the object won't be read
    # from disk, and the object will be returned with no data.
    #
    # This value is kept here mostly so it can be stubbed by tests and
    # documented. It is not meant to be changed during normal usage of the
    # library.
    @blob_maximum_data_size = 5 * 1024 * 1024
    (class <<self; attr_accessor :blob_maximum_data_size; end)

    # Internal: Maximum amount of bytes of blob data to return for one blob.
    # The default value is 500kb of blob data. This should allow blob data
    # to be cached.
    #
    # This value is kept here mostly so it can be stubbed by tests and
    # documented. It is not meant to be changed during normal usage of the
    # library.
    @blob_truncate_data_size = 500 * 1024
    (class <<self; attr_accessor :blob_truncate_data_size; end)

    # Lookup a Rugged blob and convert into GitRPC's simple object format.
    def get_blob_object(oid, header, truncate = Backend.blob_truncate_data_size, limit = Backend.blob_maximum_data_size)
      blob_hash = {
        "type" => "blob",
        "size" => header[:len],
        "data" => "",
        "truncated" => false,
      }

      if limit && header[:len] > limit
        blob_hash["oid"] = oid
        blob_hash["truncated"] = true
      else
        blob = rugged.lookup(oid)
        content = if truncate && blob.size > truncate
          blob_hash["truncated"] = true
          blob.content(truncate)
        else
          blob.content
        end
        blob_hash["data"] = content
        blob_hash["oid"] = blob.oid
        blob_hash["encoding"] = GitRPC::Encoding.guess_and_tag(content)
        blob_hash["binary"] = blob_hash["encoding"].nil?
      end

      blob_hash
    end

    # Get a Rugged tag object and convert into GitRPC's simple object format.
    def get_tag_object(oid, header, skip_bad: false)
      tag = rugged.lookup(oid)
      message, signed = extract_tag_signature(tag.message)

      { "oid"           => tag.oid,
        "type"          => "tag",
        "name"          => tag.name,
        "message"       => message,
        "message_shas"  => extract_and_expand_shas(message),
        "has_signature" => signed,
        "tagger"        => convert_author(tag.tagger),
        "target"        => tag.target_oid,
        "target_type"   => tag.target_type.to_s }
    rescue Rugged::TagError => e
      if skip_bad
        nil
      else
        if e.message =~ /failed to parse/
          raise GitRPC::InvalidObject, "Invalid tag object #{oid}"
        else
          raise
        end
      end
    end

    # Split the PGP signature part out of an annotated tag's message.
    #
    # message - String message or nil when no message has been set.
    #
    # Returns a [message, has_signature] array.
    def extract_tag_signature(message)
      if message.nil?
        [nil, false]
      elsif !message.valid_encoding?
        [message, false]
      elsif message.match(/^#{GPG_SIGNATURE_PREFIX}$/)
        message = message.split("#{GPG_SIGNATURE_PREFIX}\n", 2).first
        [message, true]
      elsif message.match(/^#{SMIME_SIGNATURE_PREFIX}$/)
        message = message.split("#{SMIME_SIGNATURE_PREFIX}\n", 2).first
        [message, true]
      else
        [message, false]
      end
    end

    # Internal: Returns the encoding name for a commit message and
    # tags the message with a Ruby-compatible encoding based on that.
    def message_encoding(commit)
      if GitRPC::Encoding.detectable?(commit.message)
        encoding = GitRPC::Encoding.guess_and_tag(commit.message)
      end

      encoding || GitRPC::Encoding::UTF8
    end

    # Internal: Set the detected encoding for the filename and return
    # what it was detected to be.
    def set_filename_encoding(filename)
      if GitRPC::Encoding.detectable?(filename)
        GitRPC::Encoding.guess_and_tag(filename)
      end
    end
  end
end
