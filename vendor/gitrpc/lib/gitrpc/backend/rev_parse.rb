# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Backend
    include GitRPC::Util

    # Public: Resolve an object name to a SHA1.
    #
    # Returns an oid or nil if the name could not be resolved.
    rpc_reader :rugged_rev_parse
    def rugged_rev_parse(object_name)
      if object_name.nil?
        raise TypeError, "object_name must be specified"
      elsif object_name.include?("..")
        return
      elsif valid_full_sha1?(object_name)
        object_name
      end

      rugged.rev_parse_oid(object_name)
    rescue Rugged::ReferenceError, Rugged::ObjectError
      # object not found
      nil
    end

    # Public: Resolve an object name to a SHA1.
    #
    # Returns an oid or nil if the name could not be resolved.
    rpc_reader :native_rev_parse
    def native_rev_parse(object_name)
      if object_name.nil?
        raise TypeError, "object_name must be specified"
      elsif object_name.include?("..")
        return
      elsif valid_full_sha1?(object_name)
        object_name
      end

      res = spawn_git("rev-parse", ["--verify", object_name])
      res["out"].strip if res["ok"]
    end

    # Make rugged implementation the default
    rpc_reader :rev_parse
    alias rev_parse rugged_rev_parse
  end
end
