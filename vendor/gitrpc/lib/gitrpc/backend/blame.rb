# frozen_string_literal: true

module GitRPC
  class Backend
    rpc_reader :blame
    def blame(commit_oid, path, annotate: [], timeout: nil, since: nil)
      ensure_valid_full_sha1(commit_oid)

      argv = ["--porcelain"]
      argv += ["--since", "#{since.to_i}"] if since
      annotate.each do |linenum|
        argv += ["-L", "#{linenum},#{linenum}"]
      end
      argv += [commit_oid, "--", path]

      res = spawn_git("blame", argv, nil, {}, nil, timeout)

      if !res["ok"]
        raise NoSuchPath, path if res["err"].start_with?("fatal: no such path")
        raise BadLineRange if /has only \d+ lines?\n?\z/.match?(res["err"])
        raise GitRPC::CommandFailed.new(res)
      end

      res["out"]
    end
  end
end
