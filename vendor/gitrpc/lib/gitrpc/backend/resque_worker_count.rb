# frozen_string_literal: true

module GitRPC
  class Backend
    rpc_reader :resque_worker_count
    def resque_worker_count
      count = begin
        File.read("/etc/github/resque-worker-count")
      rescue Errno::ENOENT
        0
      end
      count.to_i
    end
  end
end
