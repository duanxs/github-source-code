# rubocop:disable Style/FrozenStringLiteralComment
require "date"

module GitRPC
  class Backend
    # This creates a ref, do we need to 3PC?
    rpc_writer :create_tag_annotation
    def create_tag_annotation(tag_name, target_oid, annotation)
      annotation = rugged.tags.create_annotation(tag_name, target_oid, {
        :message => annotation["message"],
        :tagger  => {
          :name  => annotation["tagger"]["name"],
          :email => annotation["tagger"]["email"],
          :time  => iso8601(annotation["tagger"]["time"]),
        }
      })

      annotation.oid
    end
  end
end
