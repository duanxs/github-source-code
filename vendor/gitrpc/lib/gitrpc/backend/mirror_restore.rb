# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Backend
    rpc_writer :mirror_restore
    def mirror_restore(source, reponame)
      # First clone the repository as a mirror
      result = spawn_git("clone", ["--mirror", source, reponame])
      raise CommandFailed.new(result) if !result["ok"]

      # Now we need to get rid of the references we only want in the
      # backups and should not be replicated on the live site
      rugged = Rugged::Repository.new("#{@path}/#{reponame}")
      rugged.references.each("refs/__gh__/removed/*").
        map(&:name).
        each { |n| rugged.references.delete(n) }
    end
  end
end
