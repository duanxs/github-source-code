# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Backend
    rpc_reader :rev_list
    def rev_list(include_oids, exclude_oids, options)
      (include_oids + exclude_oids).each do |oid|
        begin
          commit = rugged.lookup(oid)
          if commit.type != :commit && commit.type != :tag
            raise GitRPC::InvalidObject, "Invalid object type, expected commit or tag but was #{commit.type}"
          end
        rescue Rugged::OdbError => e
          raise GitRPC::ObjectMissing.new(e.message, oid)
        end
      end

      git_opts = []
      git_opts << "--max-count" << options["limit"].to_s if options["limit"]
      git_opts << "--skip" << options["skip"].to_s if options["skip"]
      git_opts << "--reverse" if options["reverse"]
      git_opts << "--merges" if options["merges"]

      if options["symmetric"]
        git_opts << include_oids.join("...")
      else
        git_opts.concat(include_oids)
      end

      git_opts.concat(exclude_oids.map { |oid| "^#{oid}" })

      unless options["paths"].nil? || options["paths"].empty?
        git_opts << "--"
        git_opts.concat(options["paths"])
      end

      res = spawn_git("rev-list", git_opts)
      if res["ok"]
        res["out"].split("\n")
      else
        fail res["err"]
      end
    end
  end
end
