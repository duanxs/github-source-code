# rubocop:disable Style/FrozenStringLiteralComment
# Non-rugged implementation of the GitRPC::Backend::Commits interface.
#
# This module is here for benchmarking purposes only. It's the original
# read_commits implementation from Walker. Having both implementations
# available will let us do some basic tests of the two approaches.
module GitRPC
  class Backend
    # See GitRPC::Backend#read_commits
    rpc_reader :native_read_commits
    def native_read_commits(oids)
      return [] if oids.empty?
      input = oids.join("\n")
      res = spawn_git("cat-file", ["--batch"], input)
      if res["ok"]
        parse_commits(res["out"])
      else
        []
      end
    end

    # Internal: Parse output from `git-cat-file --batch'.
    #
    # data - String raw output from cat-file.
    #
    # Returns an Array of hashes describing commits.
    def parse_commits(data)
      io = StringIO.new(data)
      objects = []
      while line = io.gets
        oid, type, size = line.split(" ", 3)
        raise GitRPC::ObjectMissing.new(oid, oid) if type == "missing"

        if type != "commit"
          io.seek(size.to_i, IO::SEEK_CUR)
          objects << { "type" => type, "oid" => oid }
          next
        end

        data = io.read(size.to_i + 1)
        objects << parse_commit(oid, data)
      end
      objects
    end

    # Internal: Parse raw commit metadata for an individual commit.
    #
    # oid  - Commit sha1 as a 40 char string.
    # data - Git raw microformat data for the commit object.
    #
    # Returns a Hash describing the commit.
    def parse_commit(oid, data)
      info, message = data.split("\n\n", 2)
      lines = info.split("\n")

      tree = lines.shift.split(" ", 2).last

      parents = []
      parents << lines.shift[7..-1] while lines[0][0, 6] == "parent"

      author    = parse_actor(lines.shift)
      committer = parse_actor(lines.shift)

      {
        "type"      => "commit",
        "oid"       => oid,
        "tree"      => tree,
        "parents"   => parents,
        "author"    => author,
        "committer" => committer,
        "message"   => message,
        "encoding"  => GitRPC::Encoding::UTF8
      }
    end

    # Internal: Parse raw git microformat actor data.
    #
    # data - String like "Joe Blow <joe@blow.com> 123455555".
    #
    # Returns a [name, email, time] tuple.
    def parse_actor(data)
      m, actor, time = *data.match(/^.+? (.*) (\d+) .*$/)
      parse_person(actor) + [Time.at(time.to_i).iso8601]
    end

    # Internal: Parse raw git microformat describing a committer or author
    # into a name and email part.
    #
    # data - String like "Joe Blow <joe@blow.com>"
    #
    # Returns a [name, email] tuple. The email element may be nil when no
    # email was provided in the data.
    def parse_person(data)
      if data =~ /(.*) <(.+?)>/
        [$1, $2]
      else
        [data, nil]
      end
    end
  end
end
