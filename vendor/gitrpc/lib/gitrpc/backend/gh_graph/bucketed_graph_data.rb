# frozen_string_literal: true

require_relative "log_parser"

module GitRPC
  class Backend
    class BucketedGraphData

      # backend     - a reference to the GitRPC::Backend
      # from_oid    - the OID of the start of the commit range being considered
      # to_oid      - the OID of the end of the commit range being considered
      def initialize(backend:, from_oid:, to_oid:)
        @backend     = backend
        @from_oid    = from_oid
        @to_oid      = to_oid
      end

      # The GitRPC::Backend instance
      attr_reader :backend

      # The OIDs denoting the range to be calculated
      attr_reader :from_oid, :to_oid

      # Public: Returns the aggregated metrics for the range of commits spanning from to to. See
      #   below in #day_author_hash for more info.
      #
      # [
      #   {
      #     date: "2019-01-22",
      #     author: "author1@email.com",
      #     co_authors: ["author2@email.com"],
      #     additions: 5,
      #     deletions: 2,
      #     commits: 1
      #   },
      #   ...
      # ]
      def data
        verify_commits_exist
        hash = day_author_hash

        data = hash.values.flat_map(&:values)
        data.flatten.sort_by { |metrics| [metrics[:date], metrics[:authors]] }
      end

      private

      # Internal: Builds a nested hash of metrics representing the changes from the from_oid to the to_oid.
      #   If the from_oid is an ancestor of the to_oid, just includes all commits that are ancestors of the to_oid
      #   but not ancestors of the from_oid. If the from_oid is not present, includes all ancestors of the to_oid.
      #   If the from_oid is not an ancestor of the to_oid, the merge base is found and all metrics from it down
      #   to the from_oid are subtracted from all metrics to the to_oid from the merge base.
      #
      #  For example, in the following scenario, the additions, deletion, and commit totals for the days
      #  represented by c3, c4, and from will all be multiplied by -1. The metrics for c5, c6, and to will
      #  be aggregated as normal. The two groups of metrics will be reconciled and returned in a
      #  single list to the client.
      #
      #  c1---c2---b---c3---c4---from
      #             \--c5---c6---to
      #
      # Example format:
      # {
      #   "2019-01-22" => {
      #     ["author@email.com", "author2@email.com"] => {
      #       author: "author@email.com",
      #       co_authors: ["author2@email.com"],
      #       additions: 45,
      #       deletions: 3,
      #       commits: 5
      #     }
      #   }
      # }
      def day_author_hash
        range = from_oid.nil? ? to_oid : "#{from_oid}...#{to_oid}"
        log_parser = LogParser.new(backend: backend, revision_or_range: range)

        day_authors = build_default_day_author_hash

        log_parser.each do |commit_data|
          emails = sorted_emails(commit_data)
          time = Time.at(commit_data[:timestamp]).utc
          date = time.strftime("%Y-%m-%d")
          multiplier = commit_data[:removal] ? -1 : 1

          data = day_authors[date][emails]

          data[:additions] += commit_data[:stats][0] * multiplier
          data[:deletions] += commit_data[:stats][1] * multiplier
          data[:commits] += 1 * multiplier
        end

        day_authors
      end

      # Internal: build the default hash of the form returned by day_author_hash. Populates missing keys
      #   with "zeroed out" metrics.
      def build_default_day_author_hash
        Hash.new do |days, day|
          authors = Hash.new do |author_hash, emails|
            author, *co_authors = emails
            metrics = {
              date: day,
              author: author,
              additions: 0,
              deletions: 0,
              commits: 0
            }
            metrics[:co_authors] = co_authors unless co_authors.empty?
            authors[emails] = metrics
          end
          days[day] = authors
        end
      end

      def sorted_emails(commit_data)
        emails = commit_data[:emails]

        # The primary author is always first, and that leaves only one co-author to sort.
        return emails if emails.count <= 2

        author, *co_authors = emails
        co_authors.sort!
        co_authors.unshift(author)
      end

      def verify_commits_exist
        [to_oid, from_oid].compact.each do |oid|
          begin
            backend.rugged.rev_parse_oid(oid)
          rescue Rugged::ReferenceError, Rugged::ObjectError => e
            raise GitRPC::ObjectMissing.new(e.message, oid)
          end
        end
      end
    end
  end
end
