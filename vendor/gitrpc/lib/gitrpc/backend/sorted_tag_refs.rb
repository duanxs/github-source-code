# rubocop:disable Style/FrozenStringLiteralComment

require "version_sorter"

module GitRPC
  class Backend
    VALID_TAG_TARGETS = [Rugged::Commit, Rugged::Tag::Annotation]

    # Public: Retrieve tag ref mappings for a repository in chronological
    # order. The ordering is based on the creation date of the peeled commit
    # object the tag ref points to.
    #
    # Note that the collection returned only includes tag refs which resolve
    # to a commit object. Tag refs that resolve to any other type are
    # excluded.
    #
    # Returns an Array of:
    #   [ [tag-ref-name, immediate-target-oid, peeled-target-oid], ... ]
    rpc_reader :sorted_tag_refs
    def sorted_tag_refs
      refs = rugged.refs("refs/tags/*").map do |ref|
        begin
          if epoch = epoch_for(ref.target)
            peel = ref.peel || ref.target_id
            [ref.name, ref.target_id, peel, epoch]
          end
        rescue Rugged::TagError
          nil
        end
      end.compact

      refs.compact!

      refs.sort! do |a, b|
        # Sort tags to the nearest day of creation
        same_day = (a[3] / 86400) <=> (b[3] / 86400)
        if !same_day.zero?
          same_day
        # Then sort them by SemVer
        else
          semver = VersionSorter.compare(a[0], b[0]) <=> 0
          # Finally sort by second of creation
          semver.zero? ? a[3] <=> b[3] : semver
        end
      end

      refs.each do |ref|
        ref.pop
      end
    rescue Rugged::InvalidError => boom
      error = ::GitRPC::BadObjectState.new(boom.message)
      error.set_backtrace(boom.backtrace)
      raise error
    end

    def epoch_for(obj)
      case obj
      when Rugged::Commit
        obj.epoch_time
      when Rugged::Tag
        epoch_for(obj.annotation)
      when Rugged::Tag::Annotation
        return unless VALID_TAG_TARGETS.include?(obj.target.class)
        tagger = obj.tagger
        epoch = (tagger && tagger[:time]).to_i
        epoch.zero? ? epoch_for(obj.target) : epoch
      end
    end
  end
end
