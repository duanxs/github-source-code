# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Backend
    # Internal: Read audit log lines.
    #
    # offset - Integer byte offset to read from
    # limit  - Integer number of lines to read
    #
    # Returns Array of lines and a EOF boolean if the last line was read.
    def read_audit_log_lines(offset, limit)
      path = File.join(self.path, "audit_log")

      if !File.exist?(path)
        return [[], true]
      end

      with_exclusive_lock(path) do |f|
        f.seek(offset, IO::SEEK_SET)
        [f.take(limit), f.eof?]
      end
    end

    # Internal: Read audit log lines from a list of offsets.
    #
    # offsets - Array of Integer byte offsets to start reading from.
    #
    # Returns Array of lines.
    def read_audit_log_offsets(offsets)
      path = File.join(self.path, "audit_log")

      if !File.exist?(path)
        return []
      end

      with_exclusive_lock(path) do |f|
        offsets.map do |offset|
          f.seek(offset, IO::SEEK_SET)
          [offset, f.first]
        end
      end
    end

    # Internal: Read and parse audit log lines.
    #
    # offset - Integer byte offset to read from
    # limit  - Integer number of lines to read
    #
    # Returns an Array of entry Hashs and EOF boolean if the last line was read.
    rpc_reader :read_audit_log
    def read_audit_log(offset, limit)
      lines, eof = read_audit_log_lines(offset, limit)
      return parse_audit_log_lines(offset, lines), eof
    end

    # Internal: Read and parse audit log lines from a list of offsets.
    #
    # offsets - Array of Integer byte offsets to start reading from.
    #
    # Returns an Array of entry Hashs.
    rpc_reader :read_audit_log_by_offsets
    def read_audit_log_by_offsets(offsets)
      lines = read_audit_log_offsets(offsets)
      lines.map do |offset, line|
        parse_audit_log_line(offset, line)
      end
    end

    rpc_reader :sha1sum_audit_log
    def sha1sum_audit_log
      res = spawn(["sha1sum", "audit_log"])
      return res["out"][0..6] if res["ok"]
      return "missing" if res["err"] =~ /No such file or directory/
      "fail" + res["status"].to_s
    end

    rpc_reader :last_audit_log_time
    def last_audit_log_time
      res = spawn(["tail", "-1", "audit_log"])
      raise GitRPC::CommandFailed, res["err"] if !res["ok"]

      time_str = res["out"].strip.match(/\b\d{10}\b/)[0]
      Time.at(time_str.to_i)
    end

    # Internal: Parse audit log lines at a given start byte offset.
    #
    # byteoffset - Integer number of bytes to the first line
    # lines      - Array of String lines
    #
    # Returns Array of parsed Hashs or a corrupted Hash if parsing failed.
    def parse_audit_log_lines(byteoffset, lines)
      lines.map do |line|
        value = parse_audit_log_line(byteoffset, line)
        byteoffset += line.bytesize
        value
      end
    end

    AUDIT_LOG_LINE_RE = /\A(?<ref>.+) (?<old_oid>[0-9a-f]{40}) (?<new_oid>[0-9a-f]{40}) (?<name>.*) <(?<email>.*)> (?<timestamp>\d+) (?<offset>[0-9+-]+)(?:\t(?<json>.*))?\n\z/

    # Internal: Parse audit log line at a given byte offset.
    #
    # byteoffset - Integer number of bytes to the line
    # line       - String line including the line feed character
    #
    # Returns Hash of parsed components or a corrupted Hash if parsing failed.
    def parse_audit_log_line(byteoffset, line)
      match = line.match(AUDIT_LOG_LINE_RE)

      if match.nil?
        return {
          "byteoffset" => byteoffset,
          "bytesize" => line.bytesize,
          "corrupt" => true,
          "line" => line
        }
      end

      if match[:json].to_s =~ /\A\s*\{/
        sockstat = JSON.parse(match[:json])
      else
        sockstat = {}
      end

      {
        "byteoffset" => byteoffset,
        "bytesize" => line.bytesize,
        "ref" => match[:ref],
        "old_oid" => match[:old_oid],
        "new_oid" => match[:new_oid],
        "committer_name" => match[:name],
        "committer_email" => match[:email],
        "committer_date_timestamp" => match[:timestamp].to_i,
        "committer_date_offset" => match[:offset].to_i * 36,
        "sockstat" => sockstat
      }
    end

    def with_exclusive_lock(path)
      File.open(path, "r") do |f|
        f.flock(File::LOCK_EX)
        yield f
      end
    end
  end
end
