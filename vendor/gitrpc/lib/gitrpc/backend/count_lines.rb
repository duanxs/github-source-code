# frozen_string_literal: true

module GitRPC
  class Backend
    rpc_reader :count_lines
    def count_lines(oids)
      counts = {}

      oids.each do |oid|
        begin
          obj = rugged.lookup(oid)
          counts[obj.oid] = obj.loc if obj.is_a? Rugged::Blob
        rescue Rugged::OdbError
        end
      end

      counts
    end
  end
end
