# rubocop:disable Style/FrozenStringLiteralComment
require "uri"

module GitRPC
  class Backend

    rpc_writer :create_tree_changes
    def create_tree_changes(*args)
      options = create_tree_changes_options(*args)
      Rugged::Commit.create(rugged, options)
    end

    rpc_reader :stage_signed_tree_changes
    def stage_signed_tree_changes(*args)
      options = create_tree_changes_options(*args)
      [Rugged::Commit.create_to_s(rugged, options), options[:tree]]
    end

    rpc_writer :persist_signed_tree_changes
    def persist_signed_tree_changes(base_data, signature, *args)
      # ensure that any blobs/trees are created on all replicas.
      _ = create_tree_changes_options(*args)

      Rugged::Commit.create_with_signature(rugged, base_data, signature, "gpgsig")
    end

    def expand_subtrees(subtrees)
      result = {}

      subtrees.each do |subtree_path, data|
        tree = Rugged::Tree.lookup(rugged, data["oid"])

        tree.walk(:preorder) do |entry_root, entry|
          if entry[:type] != :tree
            full_path = File.join(subtree_path, entry_root, entry[:name])
            result[full_path] = {
              "oid" => entry[:oid],
              "mode" => entry[:filemode],
              "type" => entry[:type].to_s
            }
          end
          true
        end
      end

      result
    end

    rpc_writer :create_tree
    def create_tree(files, base_tree_oid = nil)
      if base_tree_oid
        base_tree = rugged.lookup(base_tree_oid)

        if base_tree.type != :tree
          raise GitRPC::InvalidObject, "Invalid object type, expected tree but was #{base_tree.type}"
        end
      end

      # Look for subtrees that the user wants to insert wholly into the
      # newly created tree.
      #
      # These subtrees are supposed to exist inside the repository already.
      # Since we cannot insert a subtree into the index (and since the user can
      # possibly mix and modify individual entries from this subtree with custom
      # entries from their data definition), we're going to blow up the subtree
      # and insert all the entries into the index
      subtrees = files.select { |path, data| data.is_a?(Hash) && data["oid"] && data["mode"] == 040000 }

      # Blow up the subtrees and merge the individual entries
      if !subtrees.empty?
        files.merge!(expand_subtrees(subtrees))
        files.delete_if { |path, _| subtrees.include? path }
      end

      process_files(files, base_tree, false)
    rescue Rugged::OdbError
      raise GitRPC::InvalidObject, "Invalid tree oid #{base_tree_oid}"
    end

    def process_files(files, base_tree, reset = false)
      index = Rugged::Index.new
      index.read_tree(base_tree) if base_tree && !reset

      files.each do |file, data|
        if data.nil?
          index.remove(file)
        else
          if data.is_a?(Hash)
            if data["source"]
              old_file = data["source"]
              index.remove(old_file)
            end
            oid = data["oid"]
            mode = data["mode"]
            data = data["data"]
          end

          old_file ||= file
          entry  = get_tree_entry(base_tree, old_file)
          mode ||= entry[:filemode] if entry
          mode ||= 0100644

          if !oid
            data ||= rugged.lookup(entry[:oid]).content
            validate_gitfile(file, mode, data)
            oid    = rugged.write(data, "blob")
          end

          index.add({
            :path => file,
            :oid  => oid,
            :mode => mode,
          })
        end
      end

      validate_gitfiles_in_index(index)
      index.write_tree(rugged)
    rescue Rugged::IndexError, Rugged::TreeError
      raise GitRPC::BadObjectState, "failed to write the requested tree"
    end

    def get_tree_entry(tree, path)
      return unless tree

      tree.path(path)
    rescue Rugged::TreeError
      nil
    end

    # Files we consider special and don't want to be a symlink
    def dotgit_file?(path)
      Rugged.dotgit_modules?(path)
    end

    def validate_gitfile_mode(name, mode)
      if mode.is_a?(String)
        mode = Integer(mode, 8)
      end
      if dotgit_file?(name) && mode == 0120000
        raise SymlinkDisallowed.new("#{name} is not allowed to be a symlink")
      end
    end

    def validate_submodule_names(path, content)
      return unless Rugged.dotgit_modules?(path)

      # FIXME: libgit2 does not currently support parsing config files from
      # memory, so we dump the contents to a temporary file and parse that
      Tempfile.open("gitmodules-candidate") do |file|
        begin
          file.write(content)
          file.flush
          config = Rugged::Config.new(file.path)
          # Reject bad submodule names
          config.each_key do |key|
            dot = key.index(".".freeze)
            return if dot.nil?
            return if key[0..dot-1] != "submodule".freeze
            key = key[dot+1..-1]
            dot = key.rindex(".".freeze)
            return if dot.nil?
            name = key[0..dot-1]

            # We've now reduced the key to just the submodule name, which we can validate
            if !Rugged::Submodule.valid_name?(name)
              raise GitRPC::BadGitmodules.new(".gitmodules contains an invalid submodule name")
            end
          end

          # Reject submodule URLs/paths that try to inject options into git-clone.
          config.each_pair  do |key, value|
            next unless /\Asubmodule\.(.*)\.(?:url|path)\z/ =~ key
            if value && value[0] == "-"
              raise GitRPC::BadGitmodules.new(".gitmodules contains an invalid submodule url")
            end
            if /\Asubmodule\.(.*)\.url\z/ =~ key
              # Always perform the newline check, since this isn't allowed in
              # either absolute or relative URLs.
              decoded = URI::DEFAULT_PARSER.unescape(value)
              if decoded.include?("\n")
                raise GitRPC::BadGitmodules.new(".gitmodules contains a URL with newlines")
              end

              # Split into whether we're in the relative or newline case.
              if /\A\.{1,2}[\\\/]/ =~ value
                # Check if the URL escapes past the root and has a
                # potentially-empty path component.
                #
                # Note: finding '../' in the middle of the string is not
                # required since Git's submodule relative resolver doesn't
                # bother.
                if /\A(?:\.[\\\/])*\.\.[\\\/]/ =~ value
                  if /[\\\/]{2}/ =~ value
                    raise GitRPC::BadGitmodules.new(".gitmodules contains potential empty hostname")
                  end
                  if /[\\\/]:/ =~ value
                    raise GitRPC::BadGitmodules.new(".gitmodules contains potential scheme rewrite")
                  end
                end
              else
                url = if /\A(?:http|ftp)s?::(.*)\z/ =~ value
                  $1
                elsif /\A(?:http|ftp)s?:\/\// =~ value
                  value
                else
                  # Do nothing, since Git's 'skip_remote_curl_protocol_prefix()'
                  # allows this URL to bypass further checks.
                  nil
                end

                if url
                  if url.start_with?("://")
                    raise GitRPC::BadGitmodules.new(".gitmodules contains a protocol-relative URL")
                  end

                  uri = URI(url)
                  if !uri.scheme || uri.scheme.empty?
                    raise GitRPC::BadGitmodules.new(".gitmodules contains a URL without a protocol")
                  end
                  if !uri.host || uri.host.empty?
                    raise GitRPC::BadGitmodules.new(".gitmodules contains a URL without a host")
                  end
                end
              end
            end
          end
        ensure
          file.unlink
        end
      end
    end

    def validate_gitfile(name, mode, content)
      validate_gitfile_mode(name, mode)
      validate_submodule_names(name, content)
    end

    def validate_unloaded_gitfile(name, mode, oid)
      return unless dotgit_file?(name)

      validate_gitfile_mode(name, mode)
      content = rugged.lookup(oid).content
      validate_submodule_names(name, content)
    end

    def validate_gitfiles_in_tree(tree)
      return if tree.nil?

      tree.each do |entry|
        validate_unloaded_gitfile(entry[:name], entry[:mode], entry[:oid])
      end
    end

    def validate_gitfiles_in_index(index)
      return if index.nil?

      index.each do |entry|
        basename = Pathname.new(entry[:path]).basename.to_s
        validate_unloaded_gitfile(basename, entry[:mode], entry[:oid])
      end
    end

    def process_files_updater(files, base_tree, reset = false)
      validate_gitfiles_in_tree(base_tree)

      # Convert the list of updates to what rugged expects

      updates = []
      files.each do |file, data|
        if data.nil?
          updates << {:action => :remove, :path => file}
          next
        end

        # We might be asked to move a file, which needs two updates
        old_removal = nil
        if data.is_a?(Hash)
          if data["source"]
            old_file = data["source"]
            # We sometimes get a source but then also contents for the file
            if !files.has_key?(old_file)
              updates << {:action => :remove, :path => old_file}
            end
          end
          oid = data["oid"]
          mode = data["mode"]
          data = data["data"]
        end
        old_file ||= file
        entry  = get_tree_entry(base_tree, old_file)
        mode ||= entry[:filemode] if entry
        mode ||= 0100644

        if !oid
          data ||= rugged.lookup(entry[:oid]).content
          oid    = rugged.write(data, "blob")
        end

        validate_gitfile(file, mode, data)
        updates << {:action => :upsert, :path => file, :oid => oid, :filemode => mode}
      end

      # Sort the inputs to work around a bug in libgit2 where it
      # itself does not sort them before using them. It should only be
      # a temporary workaround.
      updates.sort_by! { |e| e[:path] }

      baseline = (base_tree && !reset) ? base_tree : Rugged::Tree.empty(rugged)
      baseline.update(updates)
    rescue Rugged::TreeError
      raise GitRPC::BadObjectState, "failed to write the requested tree"
    end

    def create_tree_changes_options(parents, info, files = nil, reset = false, return_tree = false)
      message   = info["message"]
      committer = symbolize_keys(info["committer"])
      author    = symbolize_keys(info["author"] || committer.dup)
      tree      = info["tree"]

      parents = Array(parents)
      parent = parents.first

      [author, committer].each do |person|
        person[:time] =
          case person[:time]
          when nil
            raise ArgumentError, "Time field is required"
          when String
            iso8601(person[:time])
          when Array
            unixtime_to_time(person[:time])
          else
            raise ArgumentError, "Invalid time value: #{person[:time]}"
          end
      end

      options = {
        :message    => message,
        :committer  => committer,
        :author     => author,
        :parents    => [],
      }

      options[:parents] = parents

      reuse_tree = valid_full_sha1?(tree) && (rugged.exists?(tree) || tree == EMPTY_TREE_OID)
      options[:tree] = if reuse_tree
        validate_gitfiles_in_tree(rugged.lookup(tree))
        tree
      elsif files
        parent_tree = rugged.lookup(parent).tree if parent
        process_files_updater(files, parent_tree, reset)
      else
        raise ArgumentError, "You must specify either 'tree' in the info hash, or an array of files"
      end

      options
    end

  end
end
