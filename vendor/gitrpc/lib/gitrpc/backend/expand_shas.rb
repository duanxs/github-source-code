# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Backend
    rpc_reader :expand_shas
    def expand_shas(sha_list, expected_type)
      rugged.expand_oids(sha_list, expected_type)
    end

    rpc_reader :sha_valid?
    def sha_valid?(sha)
      # check whether the object exists in the ODB for this
      # repository.
      #
      # note that this ODB is shared between all the forks
      # in the network for the repository
      #
      # TODO: eventually, use bitmaps to verify that this SHA
      # can be reached from this repository's references.
      valid_sha1?(sha) && rugged.include?(sha)
    end
  end
end
