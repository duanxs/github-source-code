# frozen_string_literal: true

module GitRPC
  class Backend
    rpc_writer :update_committer_info

    UNKNOWN_EMAIL = "unknown"
    UNKNOWN_NAME = "unknown"

    def update_committer_info(start_commit_oid, end_commit_oid, committer)
      committer = symbolize_keys(committer)
      committer[:time] = iso8601(committer[:time])

      walker = Rugged::Walker.new(rugged)
      walker.sorting(Rugged::SORT_TOPO | Rugged::SORT_REVERSE)
      walker.push(start_commit_oid)
      walker.hide(end_commit_oid)

      walker.inject(end_commit_oid) do |parent_oid, commit|
        author = commit.author
        author[:email] = UNKNOWN_EMAIL if author[:email].strip.empty?
        author[:name] = UNKNOWN_NAME if author[:name].strip.empty?

        Rugged::Commit.create(rugged, {
          author: author,
          message: commit.message,
          committer: committer,
          parents: [parent_oid],
          tree: commit.tree
        })
      end
    end
  end
end
