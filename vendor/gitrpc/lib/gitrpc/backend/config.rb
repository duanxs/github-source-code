# frozen_string_literal: true

module GitRPC
  class Backend
    # Public: Get a git configuration value.
    #
    # name - the String config option name.
    #
    # Returns the String option value, or nil if the option is not set.
    rpc_reader :config_get
    def config_get(name)
      config.get(name)
    end

    # Public: Add or update a git configuration value.
    #
    # name - the String config option name.
    # value - the config option value, which must be a String, Integer,
    #         or true/false value.
    #
    # Returns the new option value.
    rpc_writer :config_store
    def config_store(name, value)
      config.store(name, value)
    end

    # Public: Delete a git configuration value.
    #
    # name - the String config option name.
    #
    # Returns true if the option was present, or false otherwise.
    rpc_writer :config_delete
    def config_delete(name)
      config.delete(name)
    end

    private

      # Private: Get the current configuration.
      #
      # Returns the Rugged::Config instance for the current repository.
      def config
        rugged.config
      end
  end
end
