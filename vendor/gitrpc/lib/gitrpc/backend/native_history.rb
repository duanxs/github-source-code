# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Backend
    # Public: Retrieve a revision list starting at the commit given.
    rpc_reader :native_list_revision_history
    def native_list_revision_history(oid, **options)
      ensure_valid_commitish(oid)

      oids = []

      git_opts = [oid]
      if options && options.is_a?(Hash)
        git_opts += ["-n", options[:limit].to_s] if options.key?(:limit)
        git_opts += ["--skip", options[:offset].to_s] if options.key?(:offset)

        path = options.fetch(:path, nil)
        git_opts += ["--", path.to_s] if path && !path.empty?
      end

      res = spawn_git("rev-list", git_opts)
      if res["ok"]
        oids += res["out"].split("\n")
      elsif res["err"].include? "bad object"
        raise GitRPC::ObjectMissing.new(res["err"], oid)
      end

      oids
    end
  end
end
