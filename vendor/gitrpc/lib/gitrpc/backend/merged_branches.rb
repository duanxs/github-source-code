# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Backend
    rpc_reader :merged_branches
    def merged_branches(base_branch_oid)
      ensure_valid_commitish(base_branch_oid)

      # TODO: this will eventually be `for-each-ref`; the API will not change
      res = spawn_git("branch", ["--merged", base_branch_oid])
      raise GitRPC::Error, res["err"] if !res["ok"]

      res["out"].each_line.map { |line| line.split.last }
    end
  end
end
