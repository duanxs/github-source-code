# rubocop:disable Style/FrozenStringLiteralComment
# Loaded by script/console. Land helpers here.

GITRPC_REPO = File.expand_path("../../../.git", __FILE__)

# Initialize a GitRPC::Backend for a repo at the path
# passed in; Which defaults to the gitrpc repo itself.
def backend(path = GITRPC_REPO)
  GitRPC::Backend.new(path)
end

# Initialize a GitRPC::Client for a repo at the path
# passed in; Which defaults to the gitrpc repo itself.
def client(path = GITRPC_REPO)
  GitRPC::Client.new(backend(path), GitRPC::Util::HashCache.new)
end

# Initialize a GitRPC::Client for a repo at the path passed in.
def rpc(path)
  client(path)
end

Pry.config.prompt = lambda do |context, nesting, pry|
  "[gitrpc] #{context}> "
end
