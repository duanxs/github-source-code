# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Client
    def create_tag_annotation(tag_name, target_oid, message:, tagger:)
      time = tagger.fetch(:time)
      time = time.respond_to?(:iso8601) ? time.iso8601 : time
      send_message(:create_tag_annotation, tag_name, target_oid, {
        "message" => message,
        "tagger" => {
          "name"  => tagger.fetch(:name),
          "email" => tagger.fetch(:email),
          "time"  => time,
        }
      })
    end
  end
end
