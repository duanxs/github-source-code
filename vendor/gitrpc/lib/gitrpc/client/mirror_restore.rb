# frozen_string_literal: true

module GitRPC
  class Client
    # Mirror a repository from +source+
    #
    # This will perform a mirror-clone of the +source+ repository into
    # the current network and store it as +reponame+.
    #
    # The references from git-removed (refs/__gh__/removed/) will be
    # deleted as we do not want them in the live site
    def mirror_restore(source, reponame)
      send_message(:mirror_restore, source, reponame)
    end
  end
end
