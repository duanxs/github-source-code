# frozen_string_literal: true

module GitRPC
  class Client
    # Public: Read a symbolic reference
    #
    # ref       - String Ref name
    #
    # Returns a string containing the path that the symbolic ref points to,
    # or raises GitRPC::CommandFailed.
    def read_symbolic_ref(ref)
      ensure_valid_refname(ref)
      @cache.fetch(repository_reference_cache_key("read_symbolic_ref", ref)) do
        send_message(:read_symbolic_ref, ref)
      end
    end
  end
end
