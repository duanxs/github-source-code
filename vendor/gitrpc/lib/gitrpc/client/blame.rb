# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Client
    # Public: Show what revision and author last modified each line of a file
    #
    # commit_oid - a commit
    # path       - a path
    # annotate   - array of line numbers to annotate
    #
    # Returns blame output in --porcelain format, or raises GitRPC::CommandFailed.
    def blame(commit_oid, path, options = {})
      ensure_valid_full_sha1(commit_oid)

      send_message(:blame, commit_oid, path, **options)
    end
  end
end
