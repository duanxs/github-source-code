# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Client
    def read_tree_blobs_count(oid, limit = nil)
      @cache.fetch(content_cache_key("read_tree_blobs_count", oid, limit, "v0")) do
        send_message(:read_tree_blobs_count, oid, limit)
      end
    end
  end
end
