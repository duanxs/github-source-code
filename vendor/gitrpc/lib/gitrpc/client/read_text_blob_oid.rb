# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Client
    def read_text_blob_oid(commit_oid)
      cache_key = repository_cache_key("read_text_blob_oid", commit_oid)

      @cache.fetch(cache_key) do
        send_message(:read_text_blob_oid, commit_oid)
      end
    end
  end
end
