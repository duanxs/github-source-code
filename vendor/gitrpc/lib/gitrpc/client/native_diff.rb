# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Client
    # Public: Get the raw text for a diff
    #
    # commit1_oid - a commit sha1
    # commit2_oid - a commit sha1
    #
    # Returns the raw diff text as a String
    def native_diff_text(commit1_oid, commit2_oid = nil, paths: [], full_index: false)
      paths = Array(paths)
      ensure_valid_full_sha1(commit1_oid)
      ensure_valid_full_sha1(commit2_oid) if commit2_oid

      @cache.fetch(diff_text_key("diff-text", commit1_oid, commit2_oid, "v2", paths, full_index: full_index)) do
        send_message(:native_diff_text, commit1_oid, commit2_oid, paths, full_index: full_index)
      end
    end

    # Public: Get the raw text for a diff in patch format
    #
    # commit1_oid - a commit sha1
    # commit2_oid - a commit sha1
    #
    # Returns the diff text in patch format as a String
    def native_patch_text(commit1_oid, commit2_oid = nil, full_index: false)
      ensure_valid_full_sha1(commit1_oid)
      ensure_valid_full_sha1(commit2_oid) if commit2_oid

      @cache.fetch(diff_text_key("patch-text", commit1_oid, commit2_oid, "v2", full_index: full_index)) do
        send_message(:native_patch_text, commit1_oid, commit2_oid, full_index: full_index)
      end
    end

    # Internal: Cache key used to store the diff.
    #
    # type    - The type of call to cache
    # commit1 - 40 char object id string.
    # commit2 - 40 char object id string.
    #
    # Returns a String key suitable for use with memcache.
    def diff_text_key(type, commit1, commit2, version, paths = [], **kwargs)
      args = [commit1, commit2, type, version]
      unless paths.empty?
        args << md5(paths)
      end
      unless kwargs.empty?
        args << md5(kwargs)
      end
      content_cache_key(*args)
    end
  end
end
