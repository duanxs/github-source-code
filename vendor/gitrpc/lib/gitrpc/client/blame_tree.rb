# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Client
    def blame_tree(commit_oid, path = nil, recursive = true, use_cache = true, fast: false)
      ensure_valid_full_sha1(commit_oid)

      key = content_cache_key("blame-tree", commit_oid, md5(path), recursive, "v4")
      @cache.fetch(key) do
        send_message(:blame_tree, commit_oid, path, recursive, use_cache, fast: fast)
      end
    end

    def blame_tree_fill_cache(commit_oid, commits_back = 1000, fast: false)
      ensure_valid_full_sha1(commit_oid)
      send_message(:blame_tree_fill_cache, commit_oid, commits_back, fast: false)
    end
  end
end
