# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Client
    def interesting_branches(default_branch = "master", limit = 50)
      cache_key = repository_cache_key(
        "interesting_branches", default_branch, limit.to_s)
      @cache.fetch(cache_key, 3600) do # 1 hour in secons
        send_message(:interesting_branches, default_branch, limit)
      end
    end
  end
end
