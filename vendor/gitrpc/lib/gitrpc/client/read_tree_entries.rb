# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Client
    # Public: Fetch a summary of a single tree in this Repository. The result
    # is an Array of blob Hashes containing the tree entry name, oid, size, etc.
    # The actual blob data is **not** returned by this method.
    #
    # tree_id - The 40 char sha1 oid of the tree (not a commit oid).
    # path    - String path to a subtree from the root of the tree. This
    #           must not start with a slash (/) and is always relative
    #           to the root of the repository.
    #
    # Returns an Array of Hashes:
    #
    #     { 'oid'   => 40 char sha1 oid of the entry,
    #       'name'  => filename (not including the path prefix),
    #       'size'  => the untruncated size of the blob, if the entry is a blob,
    #       'type'  => the type of the entry (usually tree or blob),
    #       'mode'  => the integer filemode of the entry }
    #
    def read_tree_entries(oid, path: nil, limit: 1000, simplify_paths: false, skip_size: false)
      ensure_valid_full_sha1(oid)

      path = normalize_path(path)
      cache_key = content_cache_key("read_tree_entries:v4", oid, md5(path.to_s), limit)
      cache_key << ":simplified" if simplify_paths
      cache_key << ":skip_size" if skip_size

      @cache.fetch(cache_key) do
        send_message(:read_tree_entries, oid, path, limit, simplify_paths, skip_size)
      end
    end

    # Public: Recursively fetch a summary of a single tree in this Repository.
    # The result is an Array of blob Hashes containing the tree entry name,
    # oid, size, etc. The actual blob data is **not** returned by this method.
    #
    # tree_id - The 40 char sha1 oid of the tree or a commit containing one
    # path    - String path to a subtree from the root of the tree. This
    #           must not start with a slash (/) and is always relative
    #           to the root of the repository.
    #
    # Returns an Array of Hashes:
    #
    #     { 'oid'   => 40 char sha1 oid of the entry,
    #       'name'  => filename (not including the path prefix),
    #       'size'  => the untruncated size of the blob, if the entry is a blob,
    #       'type'  => the type of the entry (usually tree or blob),
    #       'mode'  => the integer filemode of the entry }
    #
    def read_tree_entries_recursive(oid, path = nil, limit = 1000)
      path = normalize_path(path)
      cache_key = content_cache_key("read_tree_entries_recursive:v4", oid, md5(path.to_s), limit)

      @cache.fetch(cache_key) do
        send_message(:read_tree_entries_recursive, oid, path, limit)
      end
    end

    # Public: Similar to `read_tree_entries_recursive`, but allows the
    # specification of conditional filters that are applied by the backend
    # before the data is returned. The actual blob data is **not** returned
    # by this method.
    #
    # oid         - The 40 char oid of a tree or commit containing a tree
    # path        - String path to a subtree from the root of the tree. This
    #               must not start with a slash (/) and is always relative
    #               to the root of the repository.
    # :limit      - An upper bound on the number of objects returned, can be false
    # :conditions - A Hash of conditions that must be met by the returned objects
    #                 :max_size - The maximum size in bytes of an object
    #                 :min_size - The minimum size in bytes of an object
    #                 :type     - The required type of the object
    #                 :binary   - true or false, only useful on blobs
    #
    # Returns a Hash which includes an Array of Hashes in 'entries'. If the result is
    # truncated due to `limit`, `"truncated_entries"` will be true in the result.
    #
    #  { 'entries': [
    #     { 'oid'   => 40 char sha1 oid of the entry,
    #       'name'  => filename (not including the path prefix),
    #       'size'  => the untruncated size of the blob, if the entry is a blob,
    #       'type'  => the type of the entry (usually tree or blob),
    #       'mode'  => the integer filemode of the entry }
    #    ],
    #    'oid' => The oid of the tree that was walked
    #    'truncated_entries' => Present and true if the result was reduced due to limit
    #  }
    def read_tree_entries_recursive_filtered(oid, path = nil, conditions: {}, limit: 1000)
      # Stringify the keys of conditions, for transport
      conditions.keys.each do |key|
        conditions[key.to_s] = conditions.delete(key)
      end

      path = normalize_path(path)
      cache_key = content_cache_key("read_tree_entries_recursive_filtered:v4", oid, md5(path.to_s), md5(conditions.hash), limit)

      @cache.fetch(cache_key) do
        send_message(:read_tree_entries_recursive_filtered, oid, path, conditions, limit)
      end
    end

    # Public: read a single tree entry given a path and a sha.
    #
    # oid - the oid of the commit or tree to find the entry in.
    # path - The path of the blob or tree to look for.
    def read_tree_entry(oid, path = nil, options = {})
      ensure_valid_full_sha1(oid)

      defaults = {
        "truncate" => 1024 * 1024,
        "limit" => GitRPC::Backend::TREE_ENTRY_SIZE_LIMIT,
        "type" => nil
      }
      opts = defaults.merge(stringify_keys(options))

      path = normalize_path(path)
      key = "#{path}:#{opts["truncate"]}:#{opts["limit"]}:#{opts["type"]}"
      cache_key = content_cache_key("read_tree_entry:v4", oid, md5(key))

      @cache.fetch(cache_key) do
        send_message(:read_tree_entry, oid, path, opts).tap do |response|
          response["content"] = response["data"] # For legacy gist usage
          GitRPC::Encoding.tag_compatible(response["data"], response["encoding"])
        end
      end
    end

    # Public: read a tree entry's oid
    #
    # If no path is specified, the root tree id's is returned
    #
    # oid - the root tree's id
    # path - optional path to resolve, if nil or empty, the root tree oid is returned
    # options - include `:type` to assert the entry must have this type
    #
    # Returns the 40c oid of the specified entry
    def read_tree_entry_oid(oid, path = nil, options = {})
      ensure_valid_full_sha1(oid)

      defaults = {
        "type" => nil
      }
      opts = defaults.merge(stringify_keys(options))

      path = normalize_path(path)
      key = "#{path}:#{opts["type"]}"
      cache_key = content_cache_key("read_tree_entry:v4", oid, md5(key))

      @cache.fetch(cache_key) do
        send_message(:read_tree_entry_oid, oid, path, opts)
      end
    end

    # Public: read tree entries for multiple paths given a sha and path list
    #
    # oid - the oid of the commit or tree to find the entries in.
    # paths - Array of strings identifying paths to look up.
    #
    # Returns a hash of `{ path => oid }` for all the specified paths.
    def read_tree_entry_oids(oid, paths)
      ensure_valid_full_sha1(oid)

      key = paths.sort.join(":")
      cache_key = content_cache_key("read_tree_entry:v4", oid, md5(key))

      @cache.fetch(cache_key) do
        send_message(:read_tree_entry_oids, oid, paths)
      end
    end

    # Internal: Normalizes a path into a format acceptable to git. This will
    # remove leading slashes and convert empty or '.' paths to nil.
    #
    # path - The path String or nil.
    #
    # Return the normalized path String or nil.
    def normalize_path(path)
      return if path.nil?

      path = path.b.sub(/^\//, "")
      path = nil if path.empty? || path == "."
      path
    end
  end
end
