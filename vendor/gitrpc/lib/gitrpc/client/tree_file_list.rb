# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Client
    # Public: return a list of all the filenames inside a given tree and all its
    # subtrees. The result includes the full path to all the entries in the tree.
    #
    # oid - sha1 of the tree or commit to enumerate
    # list_directories - if false, actual directory paths will not be returned
    # in the filename list, but the files under the directories will
    # list_submodules - if false, submodule paths will not be returned in the
    # filename list
    # skip_directories - array of directory names that should not be descended into
    # when enumerating
    #
    # Returns an array of String
    def tree_file_list(oid, list_directories: false, list_submodules: false, skip_directories: [])
      ensure_valid_full_sha1(oid)

      hash_key = tree_file_list_key(
        oid,
        list_directories,
        list_submodules,
        skip_directories
      )

      @cache.fetch(hash_key) do
        send_message(:tree_file_list, oid, list_directories, list_submodules, skip_directories)
      end
    end

    protected
    def tree_file_list_key(oid, list_d, list_s, skip)
      list_d = list_d.to_s
      list_s = list_s.to_s
      skip = md5(skip)

      content_cache_key("tree_file_list", oid, list_d, list_s, skip)
    end
  end
end
