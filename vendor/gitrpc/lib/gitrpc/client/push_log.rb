# rubocop:disable Style/FrozenStringLiteralComment
module GitRPC
  class Client
    # Public: Show reflog information
    #
    # ref        - Limit results to a specific ref.
    # limit      - Show at most <num> log entries. Defaults to 50.
    # pagination - Show whether previous/next pages of log entries are available.
    # squash     - Squash adjacent similar entries
    # before_oid - Only show entries where the OID before the push was <oid>.
    # after_oid  - Only show entries where the OID after the push was <oid>.
    # newer_than - Only show entries newer than <timestamp>. This is
    # older_than - Only show entries older than <timestamp>. This is
    def push_log(options = {})
      send_message(:push_log, **options)
    end
  end
end
