# rubocop:disable Style/FrozenStringLiteralComment

require "gitrpc/middleware/instrumentation"
require "gitrpc/middleware/ensure_valid_call"

module GitRPC
  class Middleware

    def initialize(backend, &blk)
      @backend = backend
      @use = []
      instance_eval(&blk) if block_given?
    end

    def use(middleware)
      @use << middleware
    end

    def stack
      @use.reverse.inject(@backend) do |backend, middleware|
        middleware.new(backend)
      end
    end

    def method_missing(meth, *args, **kwargs, &blk)
      super unless @backend.respond_to?(meth)

      stack.call(meth, *args, **kwargs)
    end

    def respond_to?(meth)
      @backend.respond_to?(meth)
    end
  end
end
