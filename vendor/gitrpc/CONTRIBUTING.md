Contributing to GitRPC
======================

Simple bug fixes and small tweaks: submit a pull request. Write a test!

The most common type of change is adding a new call. This requires a backend and
client implementation of the call, tests for both, and call documentation. The
following sections give a rough outline of the process.

### Backend Implementation and Tests

Start by writing the backend implementation and tests. This is the piece that
runs on the same machine as the git repository and typically uses Rugged for git
access.

To start add a file at `lib/gitrpc/backend/<yours>.rb` with your call's
implementation. This is a basic template:

    module GitRPC
      class Backend
        def my_great_call(stuff)
          rugged.something(stuff)
          ...
        end
      end
    end

You don't need to add backend tests if the implementation is covered
in a client integration test. For code not covered by client tests, you
should add a backend test at `test/backend/<yours>_test.rb`. Check out the
other backend tests in that directory for examples. It's pretty
straightforward.

### Client Implementation and Tests

Once you have the backend part working, you have to expose it at the client
layer. The client layer may be as simple as passing the call directly through to
the backend or it may include complex caching logic depending on the type of
call.

The client implementation is similar to the backend. Add a file at
`lib/gitrpc/client/<yours>.rb`. If the client side of your call needs to just
pass through, it might look like this:

    module GitRPC
      class Client
        def my_great_call(stuff)
          send_message(:my_great_call, stuff)
        end
      end
    end

The `#send_message` method makes the remote call. Client code should implement
whatever caching and short-circuiting it can.

The client tests should live at `test/client/<yours>_test.rb`.

### Call Documentation

All public calls must be documented individually here:

https://github.com/github/gitrpc/tree/master/doc

These are simple markdown files but have a pretty well defined structure. Crib
formatting and structure from the other docs there.

Also remember to add a link to the docs INDEX:

https://github.com/github/gitrpc/tree/master/doc/INDEX.md

### Deploying to GitHub

After your PR has been merged, you must update the gitrpc gem on
github/github before you can use the gitrpc changes.

First, create a branch and update the gitrpc gem:

* `script/vendor-gem https://github.com/github/gitrpc`
* `git commit -m "Bump gitrpc for reasons"`

Next, push the new branch, start a PR, and be sure to mention @github/gitrpc.
After the PR has been reviewed and merged, update github/github code to use
the gitrpc changes.
