language_breakdown_by_file
==============

For each source code file in the repository, find what language that
file is in.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.language_breakdown_by_file("DEADBEEFDEADBEEFDEADBEEFDEADBEEF01234", incremental = true)

### Description

Use linguist to calculate language statistics for each file in the repository.
This looks at all blobs that are source code (as determined by linguist)
and then uses linguist to determine which language each of those blobs
are in. We then return a hash specifying what language each of the files
are in.

### Caching

The language stats calls are unique in that they have a on disk cache on
the fileservers, in the repository themselves. This is a language stats
file which is in Messagepack, and then compressed using zlib. The cache
is used automatically by default, and if you pass the incremental flag,
when calculating stats for a new commit, it will use the stats from a
previous commit to speed up detection.

### Arguments

* `commit_id` - The commit in the repository to calculate language stats
  from.
* `incremental` - Use the previous cached values to speed up language
  reconigtion time on new commits

### Return value

A hash of files with the following structure:

```ruby
{
  'Ruby' => ["foo.rb"],      # Language key, and a list of all files in
  'JavaScript' => ["bar.js"] # that lanaguage, as an array of filenames.
}
```

### Files

Defined in [`lib/gitrpc/client/language_stats.rb`](https://github.com/github/gitrpc/blob/master/lib/gitrpc/client/language_stats.rb#L7)
