config_delete
=============

Delete a git config option.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.config_delete('status.relativePaths')

### Description

Delete a git config option.

### Arguments

- `name`: The option name. The name is actually the section and the key separated by a dot.

### Operation

The option is removed from the git config file.

### Return Value

The call returns true if the option existed in the git config, or
false otherwise.

### Files

Defined in
[gitrpc/client/config.rb](https://github.com/github/gitrpc/blob/1dc93a54/lib/gitrpc/client/config.rb#L28)
