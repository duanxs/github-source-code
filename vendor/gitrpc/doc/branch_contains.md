branch_contains, tag_contains, commit_visible?
=========

Wrappers around `git branch --contains` and `git tag --contains`.
Return the list of refs of the correct type that have the specified commit oid
in their history.

Commit visibility checking involves seeing if a given commit is contained by
any branch or tag. This has both a predicate method, `commit_visible?`, and a
bulk interface, `commits_visible`.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.branch_contains("DEADBEEFDEADBEEFDEADBEEFDEADBEEF01234")
    rpc.tag_contains("DEADBEEFDEADBEEFDEADBEEFDEADBEEF01234")

    rpc.commit_visible?("DEADBEEFDEADBEEFDEADBEEFDEADBEEF01234")
    rpc.commits_visible(["DEADBEEF…", "DECAFBAD…"])


### Description

Look up the specified commit oid and return all branches or tags that
contain it.

### Arguments

oid - String 40c commit SHA1

For bulk-interface `commits_visible`:

oids - Array of String 40c commit SHA1s

### Return Value

For `branch_contains` and `tag_contains`:

An array of branches or tags that contain the specified revision.

For `commit_visible?`: Boolean
For `commits_visible`: Hash of Boolean results for OID keys

### Files

 - [gitrpc/client/branch_contains](/lib/gitrpc/client/branch_contains.rb)
 - [gitrpc/backend/branch_contains](/lib/gitrpc/backend/branch_contains.rb)
