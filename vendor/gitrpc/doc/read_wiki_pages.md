`read_wiki_pages`
=================

Get a list of all blobs in the repository that qualify as as "wiki
pages". These are the blobs recursively that have one of the extensions
that wikis can render. 


### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.read_wiki_pages("DEADBEEFDEADBEEFDEADBEEFDEADBEEF01234", "wiki/")

### Description

Recursively find all blobs in the repository under the given directory,
and for each one determine if it is a valid wiki page (see below). We
also check each blob to determine if it is a sidebar or a footer based
on whether it includes `/_sidebar/` or `/_footer/`. 

We only return a maximum of 5000 pages from the wiki (a hard limit on
the size of wikis on GitHub right now).

### Arguments

* `oid` - The commit in which to look for wiki pages. 
* `directory_prefix` - A subdirectory to look for wiki pages. This
  allows us to potentially use `doc/` or `wiki/` for wikis in the main
  repository in the future. Right now we always pass `nil` to fetch from
  the root.

### Return value

A hash with the following structure. 

```ruby
{
  "pages" => [entries],
  "sidebars" => {<directory> => <entry>}
  "footers" => {<directory> => <entry>}
}
```

Each entry has the same structure as returned by `read_tree_entries`:

```ruby
{
  "type" => Either tree, blob or commit (for submodules),
  "oid"  => The oid of the entry,
  "mode" => The mode of the entry,
  "name" => The name of the entry,
  "size" => The size of the blob. nil if this entry is not a blob.
  "symlink_target" => The target of the symlink if it exists. This
                      key is not present if the entry is not a symlink
                      or the symlink points outside of the repo. 
}
```

### Valid Wiki Pages

A blob is considered a valid wiki page if it has a file extension that
we can render in wikis. The list of extensions is currently:

* md
* mkd
* mkdn
* mdown
* markdown
* textile
* rdoc
* org
* creole
* rst
* rst.txt
* rest
* rest.txt
* asciidoc
* pod
* wiki
* mediawiki

### Files

Defined in [`lib/gitrpc/client/wiki.rb`](https://github.com/github/gitrpc/blob/master/lib/gitrpc/client/wiki.rb#L3)
