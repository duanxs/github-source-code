rev_parse
=========

Resolve an object name to a 40 char sha1 oid string.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.rev_parse(object_name)

### Description

Resolve an object name to a 40 char sha1 oid string.

The implementation tries hard to avoid hitting the backend by first
checking for some obviously malformed object names and also consults
the cached refs hash.

### Arguments

The `object_name` argument is a string revision specifier as described in
gitrevisions(7). Simple branch and tag names are fast-pathed at the
cache layer by interogating the list of cached refs. More complex revision
specifiers like `"master@{1.day.ago}"` always generate an RPC call.

### Return Value

The string oid of the object pointed to by `object_name` or nil when
`object_name` can not be resolved.

### Files

Defined in [gitrpc/client/refs.rb](https://github.com/github/gitrpc/blob/96529ab7/lib/gitrpc/client/refs.rb#L66)
