read_tree_summary_recursive
===========================

Fetch a summary of all trees in this Repository.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.read_tree_summary_recursive(commit_id)

### Description

Fetch a summary of all trees in this Repository. The result
includes the blob names with path prefixes, blob content, and blob oids.

### Arguments

- `commit_id`: The 40 char sha1 oid of the commit whose tree should be
  read. This must be a commit oid, not a tree oid.

### Return Value

The call returns an array of hashes with the following structure:

    { 'oid'       => 40 char sha1 oid of the blob,
      'name'      => filename (including the path prefix),
      'content'   => the (possibly truncated) content of the file as a string,
      'size'      => the untruncated size of the blob,
      'truncated' => true when the content string is truncated, false otherwise }

Note that only ~1MB of total blob content data will be returned across
all entries. Entries are returned for subsequent blobs but their data
will be truncated.

### Files

Defined in [gitrpc/client/tree_summary.rb](https://github.com/github/gitrpc/blob/96529ab7/lib/gitrpc/client/tree_summary.rb#L73)
