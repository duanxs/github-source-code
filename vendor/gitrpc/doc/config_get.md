config_get
==========

Get the value of a git config option.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.config_get('core.bare')

### Description

Get the value of a git config option.

### Arguments

- `name`: The option name. The name is actually the section and the key separated by a dot.

### Return Value

The call returns the string value of the specified option, or nil if the
option is not set.

### Files

Defined in
[gitrpc/client/config.rb](https://github.com/github/gitrpc/blob/1dc93a54/lib/gitrpc/client/config.rb#L8)
