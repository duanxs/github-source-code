echo
====

Perform a RPC roundtrip, returning whatever's provided as input.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.echo(obj=nil)

### Description

Perform a RPC roundtrip, returning whatever's provided as input.
This is primarily useful in tests and network analysis.

### Arguments

The call takes a single argument that may be any type that's serializeable
over the RPC protocol. This may include hashes, arrays, and simple
primitive types.

### Return Value

The call returns the argument provided without modification.

### Files

Defined in [gitrpc/client/auxiliary.rb](https://github.com/github/gitrpc/blob/96529ab7/lib/gitrpc/client/auxiliary.rb#L17)
