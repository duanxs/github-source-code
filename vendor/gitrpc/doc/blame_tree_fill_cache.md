blame_tree_fill_cache
=====================

Creates cache entries for later retrieval by `blame_tree`.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.blame_tree_fill_cache('deadbeff...')

### Description

From the given commit, look back 1000 commits (or however many are
specified) down the first-parent line and create a blame-tree cache
at that commit.

This call might take a while so it's expected to be run manually or
via a background job when some repository's blame-tree commands are getting
slow.


### Arguments

* `commit_oid` - The commit to start from. Generally the tip of the
  default branch or some popular commit
* `commits_back` - How many commits to go back in order to fill the
  cache. The default is 1000 which a problematic repo is bound to have
  and is far back enough to catch most requests.

### Return value

Nothing

### Files

Defined in [`lib/gitrpc/client/blame_tree.rb`](https://github.com/github/gitrpc/blob/master/lib/gitrpc/client/blame_tree.rb)
