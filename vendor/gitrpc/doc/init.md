init
====

Create and initialize the repository on disk.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.init()

### Description

Create and initialize the repository on disk. The repository is
guaranteed to be in a good state when this call returns. Repositories are
always created with the bare option set true.

This method is idempotent and non-destructive. Calling `init` on an
already initialized repository has no effect.

Protocol implementations may tap into this call and perform other types
of external initialization. For example, `GitRPC::Protocol::Chimney`
ensures that the repository has a host route in Redis.

### Return Value

The init call always returns nil.

### Exceptions

Raises GitRPC::Failure when the repository can not be created. The
repository may exist in a created but not initialized state in this case.

### Files

Defined in [gitrpc/client/disk.rb](https://github.com/github/gitrpc/blob/96529ab7/lib/gitrpc/client/disk.rb#L27)
