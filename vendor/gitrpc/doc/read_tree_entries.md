read_tree_entries
===============

Fetch all entries in a single tree by path.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.read_tree_entries(commit_oid, path = 'lib/great')

### Description

Retrieve the entire contents of a tree. This includes information about
each subtree, commit (submodule) and blob in the tree. The blob contents
are *not* returned with this call. 

This call also follows symlinks in the tree, and includes the oid of the
target if it exists in the tree.

### Arguments

The call takes one or two arguments. The first argument is the 40 char oid
string identifying the commit or tree object to read. If a commit is given,
it is peeled to find the tree. The second argument is the path to the
subtree to retrive the contents of. This argument is optional and
defaults to the root of the given tree in the first parameter.

### Return Value

The call returns a hash with the following structure:

    {
      "oid" => The oid of the tree we are listing,
      "entries" => Array of entries with the following structure. 
        {
          "type" => Either tree, blob or commit (for submodules),
          "oid"  => The oid of the entry,
          "mode" => The mode of the entry,
          "name" => The name of the entry,
          "size" => The size of the blob. nil if this entry is not a blob.
          "symlink_target" => The target of the symlink if it exists. This
                              key is not present if the entry is not a symlink
                              or the symlink points outside of the repo. 
        }
    }

### Exceptions

Raises `GitRPC::InvalidObject` when the provided oid does not resolve to a
tree object.

Raises `GitRPC::NoSuchPath` when the path does not exist in the given
tree.

Raises `GitRPC::ObjectMissing` when the oid provided does not point to
an object.

### Examples

Read the tree entries for the repository root

    rpc = GitRPC.new("file:#{Dir.pwd}/.git")
    rpc.read_tree_entries('deadbee...')

Read the tree entriesfor a repository subtree

    rpc = GitRPC.new("file:#{Dir.pwd}/.git")
    rpc.read_tree_entries('deadbee...', 'lib/foo')

### Files

Defined in
[gitrpc/client/read_tree_entries.rb](https://github.com/github/gitrpc/blob/master/lib/gitrpc/client/read_tree_entries.rb#L20)
