count_lines
===========

Count the LOC for several blobs in a single roundtrip

### Synopsis

    rpc.count_lines(['oids', ...])

### Description

Returns a hash with the line counts of several blobs, identified by their OID.
These line counts are performed efficiently by Rugged and are aware of e.g.
`\r\n` vs `\n`, non-terminated files and other corner cases.

### Arguments

An array of OIDs. If any of the OIDs doesn't identify a blob or is missing
from the repository, its line count will be missing from the output hash.

### Return value

The call returns a Hash like the following:

    {
      "c1b8e394b7a6592530ed629b464c1b3c88970b23" => 230,
      "9ca5454fa79f6218b6bc895cfc5c8b6ef928173d" => 43,
      "aa19bfecb544757e33f29c30804b735bda57fbb9" => 120,
      "de6dfdbcb964a63b90f084b2e01bbea6f770b622" => 23,
    }

Where the keys are the given OIDs (if the corresponding blob could be found on the
repository), and the values are the line counts.

### Exceptions

This method raises no exceptions

### Files

Defined in `client/count_lines.rb` and `backend/count_lines.rb`.
