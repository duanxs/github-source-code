parse_commit_signatures
======================

Read GPG signature headers and signing payloads from commits.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.parse_commit_signatures([oid1, oid2, ...])

### Description

Read GPG signature headers and signing payloads from commits.

### Arguments

The only argument is an Array of 40 char oid strings identifying the commit
objects to read signing data from.

### Return Value

Returns an Array of Arrays, each containing signing data for one of the commits
passed as arguments. The first element in each Array is the `gpgsig` header
String if present, or nil otherwise. The second element is the signing payload
String, or nil if the commit is not signed. The ordering of the signature data
Arrays matches the ordering of the oids passed as arguments.

### Exceptions

Raises `GitRPC::InvalidObject` if object isn't a commit.
Raises `GitRPC::ObjectMissing` if an object is missing.
Raises `GitRPC::InvalidFullOid` if an OID is invalid.

### Files

Defined in
[gitrpc/client/parse_signatures.rb](https://github.com/github/github/blob/master/vendor/gitrpc/lib/gitrpc/client/parse_signatures.rb#L14-L16)
