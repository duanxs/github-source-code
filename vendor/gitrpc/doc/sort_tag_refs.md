sorted_tag_refs
===============

Retrieve tag ref mappings for a repository in chronological order. The
ordering is based on the creation date of the peeled commit object the
tag points to.

### Synopsis

    >> rpc = GitRPC.new('file:/great/repo.git')
    >> rpc.sorted_tag_refs

### Description

This finds all of the refs in the `refs/tags/*` namespace, and sorts
them by the creation date of the target object. The tags are returned
with both their immediate target oids and peeled target oids. 

In the case of an annotated tag, the immediate target oid will be the
tag oid, and the peeled target oid will be the commit the annotated tag
points to. 

In the case of a normal tag ref pointing directly at a commit, the
immediate target and the peeled target will be identical: the oid of the
commit. 

### Arguments

None

### Return Value

An array of refs in chronological order. Each ref has the form:

    [tag-name, immediate target oid, peeled target oid]

If the tag points at a commit (not an annotated tag), the immediate
target and the peeled target will be the same. 

### Files

Defined in [gitrpc/client/sorted_tag_refs.rb](https://github.com/github/gitrpc/blob/master/lib/gitrpc/client/sorted_tag_refs.rb)
