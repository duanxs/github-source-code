read_tree_blobs
===============

Fetch a summary of a single tree in this Repository.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.read_tree_blobs(tree_id, path = 'lib/great')

### Description

Fetch a summary of a single tree in this Repository. The result is an array of
blob hashes containing the blob name, oid, size, etc. The actual blob data is
**not** returned by this method.

### Arguments

The call takes one or two arguments. The first argument is the 40 char oid
string identifying the tree object to read. The second argument is optional;
it is a path string identifying a subtree from the root of the tree. When
given, the resulting blob hashes are read from this subtree.

### Return Value

The call returns an array of hashes with the following structure:

    { 'oid'   => 40 char sha1 oid of the blob,
      'name'  => filename (not including the path prefix),
      'size'  => the untruncated size of the blob,
      'mode'  => the integer filemode of the blob }

### Exceptions

Raises `GitRPC::InvalidObject` when the provided oid does not resolve to a
tree object.

Raises `GitRPC::Error` when the path does not exist in the resolved tree
object.

### Examples

Read the blob summaries for the repository root

    rpc = GitRPC.new("file:#{Dir.pwd}/.git")
    rpc.read_tree_blobs('deadbee...')

Read the blob summaries for a repository subtree

    rpc = GitRPC.new("file:#{Dir.pwd}/.git")
    rpc.read_tree_blobs('deadbee...', 'lib/foo')

Read the full blob content for all blobs in the repository root less than 100K
in size

    rpc = GitRPC.new("file:#{Dir.pwd}/.git")
    blobs = rpc.read_tree_blobs('deadbee...')
    oids = blobs.map { |blob| blob['size'] > 100*1024 ? nil : blob['oid'] }
    oids.compact!
    rpc.read_blobs(oids)

### Files

Defined in
[gitrpc/client/read_tree_blobs.rb](https://github.com/github/gitrpc/blob/master/lib/gitrpc/client/read_tree_blobs.rb#L20)
