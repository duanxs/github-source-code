read_refs
=========

Retrieve ref name to object oid mappings from a repository.

### Synopsis

    >> rpc = GitRPC.new('file:/great/repo.git')
    >> rpc.read_refs
    >> rpc.read_refs("default")
    >> rpc.read_refs("extended")
    >> rpc.read_refs("all")

### Description

Retrieve ref information for all branches and tags in the repository; or, for
all refs when the filter option is set false.

Refs typically point to commit objects. However, refs outside of the
`refs/heads/*` namespace may actually point to any type of object. The
`refs/tags/*` hierarchy often includes refs that point to annotated tag objects.

Care must be taken when reading objects pointed to by refs using calls like
[`read_commits()`](read_commits.md) since a `GitRPC::InvalidObject` exception is
raised when a requested object is not actually a commit. Use the more generic
[`read_objects()`](read_objects.md) call instead.

### Arguments

 - `filter` - String specifying which set of refs should be returned. "default"
              means that only heads and tags will be returned. "extended" returns
              refs that would show up in `git ls-remote`. "all" returns all of the
              refs on the fileserver, including some that are only intended to be
              used inside of this app.

### Return Value

A hash of fully qualified ref names to oid mappings:

    { 'refs/...' => oid, ... }

All keys and values are strings. Branch keys look like `refs/heads/<branch>`,
tags look like `refs/tags/<tag>`. Refs from other hierarchies are included as
well when the `filter` argument is set `false`.

### Caching

The refs data is stored in cache and retrieved from the backend only when the
repository changes. When this isn't configured properly, refs data must be
retrieved from the backend on each invocation.

The caller may use `repository_reference_key_delegate` on the client object.
If this attribute is set, GitRPC will ask it for part of the refs key. This
value should change when the references change. If this attribute is not set,
GitRPC will ask the backend to define the key. The DGit backend delegates
refs key computation back to its delegate, which pulls the DGit checksum.

### Examples

Basic usage:

    >> irb(main):002:0> rpc = GitRPC.new('file:/great/repo.git')
    >> irb(main):002:0> rpc.read_refs
    { "refs/heads/bm"        => "111bc283000f6af7e52be8893855cbe23b8a033f",
      "refs/heads/files-api" => "a0660590fcb7397ec461aa0ff3704952fbadac6b",
      "refs/heads/gendocs"   => "e504fac6a03aca85310002c2f32654b72cee9aeb",
      "refs/heads/master"    => "cd55e2dbaf6c0e34b593f91312e1932839531273",
      ... }

To retrieve refs outside of the `refs/heads/*` and `refs/tags/*` namespaces:

    >> irb(main):002:0> rpc.read_refs("extended")
    { "refs/heads/bm"        => "111bc283000f6af7e52be8893855cbe23b8a033f",
      ...,
      "refs/special/hiya"    => "a0660590fcb7397ec461aa0ff3704952fbadac6b" }

To retrieve internal-to-github refs:

    >> irb(main):002:0> rpc.read_refs("extended")
    { "refs/heads/bm"        => "111bc283000f6af7e52be8893855cbe23b8a033f",
      ...,
      "refs/__gh__/svn/v3"   => "a0660590fcb7397ec461aa0ff3704952fbadac6b" }
