read_trees
==========

Retrieve tree information including a list of entries for a list of tree oids.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.read_trees(oids)

### Description

Retrieve tree information including a list of entries for a list
of tree oids.

This method is optimized for loading multiple trees using a single cache
lookup followed possibly by a single RPC call for trees that aren't in cache.

### Arguments

The call takes a single array of 40 char oid strings identifying trees
to load. Only full sha1 object ids are allowed. All oids must identify
tree objects.

### Return Value

The call returns an array guaranteed to have the same number of elements
as the list of oids given as input where the elements of the resulting
array match up with the `oids` provided.

Each element of the array is a hash with the following members:

    { 'type'    => 'tree',
      'oid'     => string sha1,
      'entries' => { tree entries hash } }

The tree entries hash includes one item for each tree entry. The key is the
name of the tree entry and values have the following members:

    { 'type'   => string entry type - 'tree' or 'blob',
      'oid'    => string entry oid,
      'mode'   => integer file mode,
      'name'   => string file name }

### Exceptions

Raises `GitRPC::ObjectMissing` when an object specified in `oids`
does not exist or can not be loaded from the object store.

Raises `GitRPC::InvalidObject` when the object is found but is not a
tree object.

### Examples

Read tree entry data for the root tree of the master branch:

    rpc = GitRPC.new("file:#{Dir.pwd}/.git")
    commit_sha = rpc.read_refs['refs/heads/master']
    commit = rpc.read_commits([commit_sha]).first
    tree = rpc.read_trees([commit.tree]).first

Read all subtrees for a given a tree:

    entries = tree['entries'].values.select { |entry| entry['type'] == 'tree' }
    tree_oids = entries.map { |entry| entry['oid'] }
    subtrees = rpc.read_trees(tree_oids)

### Files

Defined in [gitrpc/client/read_objects.rb](https://github.com/github/gitrpc/blob/master/lib/gitrpc/client/read_objects.rb#L192)
