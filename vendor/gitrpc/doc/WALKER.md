Walker Methods
==============

A large portion of github.com features are currently built on top of the [Walker
library][w]. This document gives an overview of those methods. The features
provided are driving a large portion of the basic GitRPC interface.

When porting features over from Walker, be sure to understand all of the cache
and backend logic for that method.

This file can be removed once things have been built out a bit. It's here for
reference only.

[w]: https://github.com/github/github/blob/master/lib/walker.rb

### Refs

Load all refs

```ruby
def refs(force_refresh = false)
```

Resolve the SHA1 for a ref

```ruby
def ref_to_sha(name)
```

Look for a branch or tag that points to the SHA1 provided.

```ruby
def sha_to_ref(sha)
```

List of ref names associated with each commit sha.

```ruby
def commit_names(match=pattern)
```

All branch heads as a Hash of `{ ref => commit }` elements

```ruby
def branch_commits
```

### Commits

Load commits

```ruby
def commit_multi(shas)
```

Retrieve a commit for the given sha

```ruby
def commit_for_ref(ref)
```

### Trees

Loads a Tree given a tree sha and path.

```ruby
def tree_path(tree_sha, path = '')
```

Load a tree given a tree sha1

```ruby
def tree(id)
```

Load a recursive list of tree entries for the entire tree:

```ruby
def tree_list(tree_sha)
```

Check that all tree entry commit data is loaded into cache.

```ruby
def cache_last_commits(commit_id, path = nil)
```

Loads commits from cache for each entry in a tree.

```ruby
def last_commits(sha, path=nil)
```

### Blobs

Load a blob given a commit sha1 and path

```ruby
def blob(tree_sha, path = '', limit=1024 * 1024)
```

Load a blob given an object id

```ruby
def blob_sha(blob_sha, limit=1024 * 1024)
```

Perform a git blame

```ruby
def blame(blob_sha, commit_sha, path)
```
### Submodules

Retrieve a list of submodules defined for the repository

```ruby
def submodules(id)
```

Retrieve information for a specific submodule

```ruby
def submodule(id, path)
```

Load blob data for a `.gitmodules` file

```ruby
def submodules_data(sha)
```

### Tags

Load a tag object given a tag name

```ruby
def tag(name)
```

Loads detailed tags information in a single roundtrip.

```ruby
def tag_multi(names)
```

### Histories

Retrieve an array of commit SHA1s starting from the given ref or SHA1

```ruby
def rev_list(start = 'master', max_count = 30, skip = 0, path = nil)
```

Load commits for a history

```ruby
def commits(start = 'master', max_count = 30, skip = 0, path = nil)
```

Load commits except for the commits given

```ruby
def commits_except(heads = [], except = [], max_count = 10, skip = 0)
```

### Notes

Find a note for a given note ref and commit sha.

```ruby
def get_note(note_ref, commit_sha)
```

### Misc

Retrieve the full plain-text diff for a commit or range of commits.

```ruby
def diff(commit1, commit2=nil)
```

Retrieve a full plain-text patch for a commit or range of commits.

```ruby
def patch(commit1, commit2=nil)
```

Find best README for a tree

```ruby
def preferred_readme(root_tree_sha, readme_path = '')
```

Batch load objects

```ruby
def batch(ids)
```
