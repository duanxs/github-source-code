participation_stats
===================

Get participation stats for a given repository

### Synopsis

    rpc.participation_stats(branch_oid, user_emails, end_date, weeks = 52)

### Description

Returns two arrays with the participation stats for a branch (usually the master branch)
in a repository. The 'all' array includes participation stats for all users that
participated on the repository; the 'owner' array returns stats for the user that owns
the repository.

Each array contains 52 entries (one for each week for which stats are tracked). The amount
of weeks for stat tracking can be changed, although the default value of 52 is the one
we usually show on GitHub's UI.

Each entry in the array is the amount of commits that were committed to the branch during
that week.

The contribution period goes from `end_date` to `end_date` minus X weeks (where X is the
contribution period). The end date is usually the current date, but it can be set to other
values to gather contributions for a period in the past.

To find which commits belong to the owner of the repository, the author email of each commit
is matched against the emails in `user_emails`. This matching is case insensitive.

### Arguments

- `branch_oid` is the OID or branch name to gather stats from. You're encouraged to pass an OID
instead of e.g. the current branch `master` to avoid races.

- `user_emails` in an array of all the emails owned by the owner of the repository. It will be used
to detect which commits belong to him/her.

- `end_date` is the date where the contribution stats end (usually the current time). It has to be
a `Time` object or a UNIX timestamp as an integer.

- `weeks` is the amount of weeks to track stats for.

### Return value

Returns a hash with two arrays, for 'owner' and 'all' contributions.

    {
        "all"=> [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ...], 
        "owner"=> [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... ],
    }

If the `user_emails` array is empty, the 'owner' array will be all zeros. If no
commits are found during the given period, both emails will be all zeros.

The call will always return two arrays of 52 elements, even if the GitRPC spawn
fails (again, on failure both arrays will be zeroes).

### Exceptions

This function raises no exceptions.

### Examples

Get the participation stats for the Facebox repository. Use @defunkt's email as owner
email.

    @client.participation_stats('master', ['chris@ozmm.org'], facebox_start_date)
    #=> {"all"=>[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 3, 8, 0, 0, 0, 4, 0, 0, 1, 4, 1, 0, 0, 1, 2, 3, 0, 0, 0, 0, 0, 0], "owner"=>[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 2, 6, 0, 0, 0, 4, 0, 0, 0, 3, 1, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0]}

### Files

Defined in `client/participation_stats.rb` and `backend/participation_stats.rb`.
