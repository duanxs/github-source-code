GitRPC instrumentation
======================

GitRPC publishes a number of instrumentation events. Use these to hook operation
timings and meta information into stats and auditing systems.

### Subscribing to instrumentation events

The `ActiveSupport::Notifications` library is used to publish instrumentation
events if available when gitrpc is loaded. Subscribing to receive events looks
like this:

``` ruby
ActiveSupport::Notifications.subscribe "remote_call.gitrpc" do |*args|
  event = ActiveSupport::Notifications::Event.new(*args)
  event.name      # => "remote_call.gitrpc"
  event.duration  # => 10 (ms)
  event.payload   # => { :extra => 'information' }
end
```

The arguments to the event handler are: `name`, `start`, `ending`,
`transaction_id`, `payload`.

The extra payload information for each event is documented below.

### remote_call.gitrpc

A remote call was made. Emitted only on the client, never on the server.

Payload:

 - `:name` The call name, like `read_refs`, `read_objects`, or `rev_parse`.
 - `:args` Array of arguments passed in the call.

### online_check.gitrpc

A check was made with Chimney (redis) to determine if a fs machine is currently
available.

Payload:

 - `:host` The fs machine host checked for upness.
 - `:path` The path to the repository on disk.

### charlock_detect.gitrpc

A string had it's encoding detected via charlock_holmes.

Payload:

 - `:bytesize` The size of the string to detect in bytes
 - `:encoding` The detected encoding of the string.

### charlock_transcode.gitrpc

A string was transcoded to a new encoding using charlock_holmes.

Payload:

 - `:source` The prior encoding of the string to transcode.
 - `:target` The encoding that we are transcoding the string to.
