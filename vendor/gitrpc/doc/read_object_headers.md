read_object_headers
==========

Retrieve object information, *without* contents, for a list of object oids.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.read_object_headers(oids)

### Description

Retrieve object information, *without* contents, for a list of object oids.

This method is optimized for loading multiple objects' metadata using a
single cache lookup followed possibly by a single RPC call for objects
that aren't in cache.

### Arguments

The call takes a single array of 40 char oid strings identifying object
headers to load. Only full sha1 object ids are allowed.

### Return Value

The call returns an array guaranteed to have the same number of elements
as the list of oids given as input where the elements of the resulting
array match up with the `oids` provided.

Each element of the array is a hash with the following members:

    { 'type' => 'blob',
      'size' => integer byte size of blob object,
    }

All keys are guaranteed to be present and non-nil. `size` is
a number representing the Git object's length, in bytes.

### Exceptions

Raises `GitRPC::ObjectMissing` when an object specified in `oids`
does not exist or can not be loaded from the object store.

### Examples

Read a single blob's size

    rpc = GitRPC.new("file:#{Dir.pwd}/.git")
    blob_headers = rpc.read_object_headers(['deadbee...'])
    blob_size = blob_headers.first["size"]

### Files

Defined in [gitrpc/client/read_objects.rb](https://github.com/github/gitrpc/master/lib/gitrpc/client/read_objects.rb#L243)
