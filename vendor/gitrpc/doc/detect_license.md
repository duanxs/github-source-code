detect_license
============

Attempt to detect the license in a Git repository for the given commit

### Synopsis

    rpc.detect_license(sha1) -> license_name

### Description

Returns a string with the most likely license for a Git repository. The
license is searched for in the tree of the given commit.

This RPC call is a simple wrapper for the [Licensee](https://github.com/benbalter/licensee)
gem. Check its documentation for details.

### Arguments

The only argument is the full OID of the commit whose tree will be used
to search for the license file.

### Return value

The call returns a string with the most likely license name, `"no-license"` if
the repository had no license or `"unknown"` if we found a license but we failed
to detect one.

### Exceptions

This function raises no exceptions.

### Examples

Detect the license of a repository, where `4dddaba` is the HEAD commit.

    rpc.detect_license('4dddaba2ddd3d9dd88076a90d2d993458b7c3a07')
    #=> "mit"

### Files

Defined in `client/detect_license.rb` and `backend/detect_license.rb`. The actual
implementation is written in the [Licensee](https://github.com/benbalter/licensee)
gem.

