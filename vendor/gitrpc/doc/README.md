GitRPC
======

This is the **gitrpc** API documentation.

### Introduction

 * **[README](..#readme)** - Basic usage, project status, and architectural overview.
 * **[CONTRIBUTING](../CONTRIBUTING.md)** - Tips for development, test, and submission.
 * **[Instrumentation](instrumentation.md)** - Events emitted via the instrumentation interface.

### Remote Call Interface

These sections document each of the remote calls available through GitRPC,
including basic argument and response data structures, cache behavior, and
computational complexity.

##### Repository

 * **[init()](init.md)**
   Create and initialize the repository on disk.
 * **[exist?()](exist?.md)**
   Check that the repository exists on disk and is valid, or that a specific
   file exists in the repository.
 * **[remove()](remove.md)**
   Remove repository from disk.

##### Refs and revision parsing

 * **[read_refs()](read_refs.md)**
   Retrieve all refs and their target oids.
 * **[rev_parse()](rev_parse.md)**
   Resolve an object name to a 40 char sha1 oid string.

##### Object database

  * **[read_objects()](read_objects.md)**
    The main git object reading workhorse call. Loads any type of object in batch.
  * **[read_commits()](read_commits.md)**
    Retrieve commit information for a list of commit oids.
  * **[read_trees()](read_trees.md)**
    Retrieve tree information including a list of entries for a list of tree oids.
  * **[read_blobs()](read_blobs.md)**
    Retrieve blob information and content for a list of blob oids.
  * **[read_tags()](read_tags.md)**
    Retrieve tag information for a list of tag oids.

##### History

 * **[list_revision_history()](list_revision_history.md)**
   Retrieve a revision list starting at the specified commit.
 * **[read_blob_contributors()](read_blob_contributors.md)**
   Retreive authors who have contributed to a blob at a given commit.

##### Configuration
  * **[config_get()](config_get.md)**
    Get a git config value.
  * **[config_store()](config_store.md)**
    Add or update a git config value.
  * **[config_delete()](config_delete.md)**
    Delete a git config value.

##### Repository filesystem

 * <strong><a href="fs_exist.md">fs_exist?()</a></strong>
   Check that a file exists within the repository on disk.
 * **[fs_read()](fs_read.md)**
   Read the entire contents of a file from disk.
 * **[fs_write()](fs_write.md)**
   Write the entire contents of a file on disk.
 * **[fs_delete()](fs_delete.md)**
   Remove a file or directory from disk.
 * **[free_space()](free_space.md)**
   Get free space on the partition holding a repo, in KB.
 * **[nw_usage()](nw_usage.md)**
   Get space used by a repo network, in KB, using `nw-usage`.

##### Auxiliary calls

  * **[echo()](echo.md)** Perform a RPC roundtrip, returning whatever's
    provided as input.
  * **[slow()](slow.md)** Emulate a slow response by sleeping on the server side.

##### Special case calls

These calls were added for special cases and should not be considered part of
the stable / general use API.

 * **[modified_blobs()](modified_blobs.md)**
   Finds all blobs which were changed between two commit oids.
 * **[read_tree_summary()](read_tree_summary.md)**
   Fetch a summary of a single tree in this Repository.
 * **[read_tree_summary_recursive()](read_tree_summary_recursive.md)**
   Fetch a summary of all trees in this Repository.
