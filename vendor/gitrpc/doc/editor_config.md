editor_config
=============

Retrieve tree entry EditorConfig information.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.fetch_editor_config(root_tree_oid, [path])

### Description

Retrieve tree entry EditorConfig information.

This method is optimized for finding multiple configs using a single cache
lookup followed possibly by a single RPC call for configs that aren't in cache.

### Arguments

The call takes a tree oid to start traversal from and an array of string paths.

### Return Value

The call returns an array guaranteed to have the same number of elements
as the list of paths given as input where the elements of the resulting
array match up with the `path` provided.

### Files

Defined in [gitrpc/client/editor_config.rb](https://github.com/github/gitrpc/blob/master/lib/gitrpc/client/editor_config.rb)
