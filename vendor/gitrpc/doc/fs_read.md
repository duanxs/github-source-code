fs_read
=======

Read a file from the git repository on disk.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.fs_read("info/refs")

### Description

Read the entire contents of the file given relative to the git repository
directory (`GIT_DIR`).

This does not operate on versioned git data but the repository structure itself.

### Return value

When successful, the entire content of the file is returned as a String.

### Exceptions

 - Raises `GitRPC::IllegalPath` when attempt is made to access a file outside the repository.
 - Raises `GitRPC::InvalidRepository` when the repository directory does not exist.
 - Raises `GitRPC::SystemError` when the file does not exist or another system
   level occurred while accessing the file.

### Files

 - [gitrpc/client/disk.rb](https://github.com/github/gitrpc/blob/master/lib/gitrpc/client/disk.rb)
 - [gitrpc/backend/disk.rb](https://github.com/github/gitrpc/blob/master/lib/gitrpc/backend/disk.rb)
