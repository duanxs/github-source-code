gist_title
==========

Find the filename of the first non-generated top level file (sorted
lexicographically). 

### Synopsis

    >> rpc = GitRPC.new('file:/great/repo.git')
    >> rpc.gist_title(oid)

### Description

This is only used for gist. We define the "title" of a gist as the
filename of the first file that isn't auto generated (i.e. doesn't match `/^gistfile/`).

This looks through each blob in the tree and returns the first one that
isn't generated. 

### Arguments

oid - The commit oid to look up the blobs in. 

### Return value

A string representation of the filename. 

### Files

Defined in
[gitrpc/client/gist.rb](https://github.com/github/gitrpc/blob/master/lib/gitrpc/client/gist.rb)
