modified_blobs
==========

Finds all blobs which were changed between two commit oids.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.modified_blobs(commit1_oid, commit2_oid)

### Description

Retrieve a list of blobs modified between two commits.

This

### Arguments

The call takes two parameters, both must be commit oids. The first would
normally be the older commit and the second being the newer.

### Return Value

Each hash returned will at least have a 'modified_status' key
denoting an addition, modification, rename, copy, deletion or type
change.

A 'path' key is also returned which is the full nested path to the blob.
For deletions only the 'modified_status' and 'path' keys are returned.
For all other types of changes the blob's oid is included as 'oid'.

All modifications that aren't deletions are stored under the 'modified'
key of the returned hash. For each of these hashes a full blob hash
(read via read_blobs) is merged in.

All deletions are stored under the 'deleted' key.

If an error occured while getting the diff used to find modified files,
an empty array is returned. Otherwise see below.

### Exceptions

Raises the same exceptions as **[read\_blobs()](read_blobs.md)**

### Examples

Fetch a list of modified blobs between 'deadbee...' and HEAD

    rpc = GitRPC.new("file:#{Dir.pwd}/.git")
    head_sha = rpc.read_refs['refs/heads/master']
    blobs = rpc.modified_blobs('deadbee...', head_sha)
    blobs['modified'].each do |blob|
      status = blob['modified_status']
      path = blob['path']
      encoding = blob['encoding']
      data = blob['data']
    end
    blobs['deleted'].each do |blob|
      status = blob['modified_status']
      path = blob['path']
    end

### Files

Defined in [gitrpc/client/modified_blobs.rb](https://github.com/github/gitrpc/blob/96529ab7/lib/gitrpc/client/modified_blobs.rb#L22)
