read_objects
============

The main git object reading workhorse method.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.read_objects(oids=[], type=nil, skip_bad=false)

### Description

The main git object reading workhorse method. Reads commit, tree,
blob, and tag objects from the object store and presents as simple hash
data structures. See the
[`read_commits()`](read_commits.md),
[`read_trees()`](read_trees.md),
[`read_blobs()`](read_blobs.md), and
[`read_tags()`](read_tags.md)
for detailed documentation on these data structures.

This method is optimized for loading multiple objects using a single cache
lookup followed possibly by a single RPC call for tags that aren't in cache.

### Arguments

 - `oids` - An array of 40 char oid strings identifying any type
   of git object to load. Only full sha1 object ids are allowed.
 - `type` - An optional expected type to validate against as objects are
   loaded. This must be 'commit', 'tree', 'blob', or 'tag' when given.
 - `skip_bad` - An optional flag that tells read object to either raise
   on any errors, or to keep going and ignore any objects that had
   issues.

### Return Value

The call returns an array guaranteed to have the same number of elements
as the list of oids given as input where the elements of the resulting
array match up with the `oids` provided.

Each element of the array is a hash with at least the following members

    { 'type'        => 'commit' | 'tree' | 'blob' | 'tag',
      'oid'         => string sha1 of the object,
      ... }

Additional fields are defined by each object type. See the appropriate
read method for documentation.

### Exceptions

Raises `GitRPC::ObjectMissing` when an object specified in `oids`
does not exist or can not be loaded from the object store.

Raises `GitRPC::InvalidObject` when the `type` argument was provided and
and object found is not of the given type.

### Files

[gitrpc/client/read_objects.rb](../lib/gitrpc/client/read_objects.rb),
[gitrpc/backend/read_objects.rb](../lib/gitrpc/backend/read_objects.rb)
