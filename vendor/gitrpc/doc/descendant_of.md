descendant_of
==========

Check whether one commit descends from another.

There are two interfaces:

* A predicate method descendant_of?, that returns true or false for a single commit and ancestor
* A bulk method descendant_of, that returns a hash of results for an array of [commit, ancestor] tuples.

### Synopsis

#### Predicate
    rpc = GitRPC.new("file:/great/repo.git")
    rpc.descendant_of?("deadbeef…", "decafbad…") => true/false

#### Bulk
    rpc = GitRPC.new("file:/great/repo.git")
    rpc.descendant_of([["deadbeef…", "decafbad…"]]) => { ["deadbeef…", "decafbad…"] => true/false }

### Description

Check whether one commit descends from another.

### Arguments

- `commit`:   The newer commit.
- `ancestor`: The older commit.

Both arguments must be full 40-character commit SHA1s.

### Return Value

Returns `true` if commit descends from ancestor, or `false` if it doesn't.

### Files

- [gitrpc/client/descendant_of](/lib/gitrpc/client/descendant_of.rb)
- [gitrpc/backend/descendant_of](/lib/gitrpc/backend/descendant_of.rb)
