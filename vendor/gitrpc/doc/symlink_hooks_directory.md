symlink_hooks_directory
=======================

Add a symlink to the hooks directory set in the configuration option
`GitRPC.hooks_template`.

### Synopsis

    rpc = GitRPC.new('file:/great/repo.git')
    rpc.symlink_hooks_directory

### Description

A very simple command that just symlinks the hooks directory in the
current repository to the pre-configured default hooks template
location. This is used to setup all of our GitHub hooks on new
repositories.

### Arguments

None

### Return Value

Always returns true

### Files

Defined in [`lib/gitrpc/client/symlink_hooks_directory.rb`](https://github.com/github/gitrpc/blob/master/lib/gitrpc/client/symlink_hooks_directory.rb)

