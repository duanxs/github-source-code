#!/bin/bash

set -e

cell=${CELL:?CELL variable required}
keyspace=${KEYSPACE:?KEYSPACE variable required}
shard=${SHARD:-'0'}
uid_base=$UUID_BASE
port_base=$((15000 + uid_base))
grpc_port_base=$((16000 + uid_base))
mysql_port_base=$((17000 + uid_base))

dbconfig_dba_flags="-db-config-dba-charset utf8mb4"

dbconfig_flags="$dbconfig_dba_flags \
  -db-config-app-charset utf8mb4 \
  -db-config-appdebug-charset utf8mb4 \
  -db-config-allprivs-charset utf8mb4 \
  -db-config-repl-charset utf8mb4 \
  -db-config-filtered-charset utf8mb4"

# Load structure file to boot schema
init_db_sql_file="$VTROOT/config/init_db.sql"
{
  echo "GRANT ALL ON *.* TO 'root'@'%';"
  echo "CREATE DATABASE vt_$keyspace;"
  echo "USE vt_$keyspace;"
  cat $STRUCTURE
  [ -n "$SQL_SETUP" ] && [ -f $SQL_SETUP ] && cat $SQL_SETUP
} >> $init_db_sql_file

export EXTRA_MY_CNF=$VTROOT/config/mycnf/master_mysql56.cnf:$VTROOT/config/mycnf/rbr.cnf

mkdir -p $VTDATAROOT/backups

# Start 2 vttablets by default.
# Pass TABLETS_UIDS indices as env variable to change
uids=${TABLETS_UIDS:-'0 1'}

# Start all mysqlds in background.
for uid_index in $uids; do
  uid=$((uid_base + uid_index))
  mysql_port=$((mysql_port_base + uid_index))
  printf -v alias '%s-%010d' $cell $uid
  printf -v tablet_dir 'vt_%010d' $uid

  echo "Starting MySQL for tablet $alias..."
  action="init -init_db_sql_file $init_db_sql_file"
  if [ -d $VTDATAROOT/$tablet_dir ]; then
    echo "Resuming from existing vttablet dir:"
    echo "    $VTDATAROOT/$tablet_dir"
    action='start'
  fi
  $VTROOT/bin/mysqlctl \
    -log_dir $VTDATAROOT/tmp \
    -tablet_uid $uid \
    $dbconfig_dba_flags \
    -mysql_port $mysql_port \
    $action &

done

# Wait for all mysqld to start up.
for uid_index in $uids; do
  mysql_port=$((mysql_port_base + uid_index))
  /script/wait-for-it localhost:$mysql_port
done

# Start all vttablets in background.
for uid_index in $uids; do
  uid=$((uid_base + uid_index))
  port=$((port_base + uid_index))
  grpc_port=$((grpc_port_base + uid_index))
  printf -v alias '%s-%010d' $cell $uid
  printf -v tablet_dir 'vt_%010d' $uid
  tablet_type=replica
  if [[ $uid_index -gt 2 ]]; then
    tablet_type=rdonly
  fi

  echo "Starting vttablet for $alias..."
  $VTROOT/bin/vttablet \
    $TOPOLOGY_FLAGS \
    -logtostderr=true \
    -log_dir $VTDATAROOT/tmp \
    -tablet-path $alias \
    -tablet_hostname "" \
    -init_keyspace $keyspace \
    -init_shard $shard \
    -init_tablet_type $tablet_type \
    -health_check_interval 5s \
    -enable_semi_sync \
    -enable_replication_reporter \
    -backup_storage_implementation file \
    -file_backup_storage_root $VTDATAROOT/backups \
    -restore_from_backup \
    -port $port \
    -grpc_port $grpc_port \
    -service_map 'grpc-queryservice,grpc-tabletmanager,grpc-updatestream' \
    -pid_file $VTDATAROOT/$tablet_dir/vttablet.pid \
    -vtctld_addr "http://$(hostname):15000/" \
    $dbconfig_flags &

done

# Wait for main tablet be up
/script/wait-for-it localhost:$port_base
if [ "start" != "$action" ]; then
  printf -v masteralias '%s-%010d' $cell $uid_base
  # If not restarting, make the first tablet master
  $VTROOT/bin/vtctl \
    $TOPOLOGY_FLAGS \
    InitShardMaster \
    -force \
    $keyspace/$shard $masteralias

fi

wait
