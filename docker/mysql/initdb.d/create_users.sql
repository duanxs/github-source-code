CREATE USER 'github_rw' IDENTIFIED BY 'github';
GRANT ALL ON github_production.* TO github_rw;

CREATE USER 'github_ro_2' IDENTIFIED BY 'github';
GRANT ALL ON github_production.* TO github_ro_2;
