const directory = process.argv[process.argv.indexOf('-d') + 1]
const fs = require('fs')
const {join} = require('path')

const mobileBrowsers = [
  '> 5%',
  'last 4 ios versions',
  'last 2 Android versions',
  'last 10 Chrome versions',
  'last 2 FirefoxAndroid versions'
]

const desktopBrowsers = [
  '> 5%',
  'last 2 firefox versions',
  'last 2 chrome versions',
  'last 2 safari versions',
  'last 2 edge versions',
  'ie 11'
]

module.exports = ({file}) => ({
  parser: 'postcss-scss',
  map: {
    sourcesContent: false,
    annotation: true
  },
  plugins: {
    'postcss-node-sass': {
      includePaths: ['node_modules', file.dirname],
      outputStyle: 'nested'
    },

    autoprefixer: {
      browsers: file.basename === 'mobile.scss' ? mobileBrowsers : desktopBrowsers
    },

    cssnano: process.env.CSS_MINIFY === '0' ? false : {},

    'postcss-hash': {
      manifest: `${directory}/manifest.json`,
      includeMap: true,
      name: ({dir, name, hash, ext}) => {
        try {
          fs.mkdirSync(dir)
        } catch (err) {
          if (err.code !== 'EEXIST') throw err
        }
        try {
          fs.unlinkSync(`${dir}/${name}${ext}`)
        } catch (err) {
          if (err.code !== 'ENOENT') throw err
        }
        fs.symlinkSync(`${name}-${hash}${ext}`, `${dir}/${name}${ext}`)
        return `${dir}/${name}-${hash}${ext}`
      },
      algorithm: 'sha512',
      trim: 32
    },

    'postcss-lockfile': {
      file: join(directory, `${file.basename}.lock`)
    }
  }
})
