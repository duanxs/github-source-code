# frozen_string_literal: true
require "github/console_auth.rb"

# Overrides the existing readline module and puts
# in place logging so we can track the actions being taken on the
# console. Concept adopted from http://blog.nicksieger.com/articles/2006/04/23/tweaking-irb/
module Readline
  module History
    def self.console_session
      ConsoleAuth::Session
    end

    def self.write_log(input)
      return if GitHub.enterprise?
      # Rescuing from inside of write log prevents
      # unwanted behaviors in console sessions
      begin
        GitHub::Logger.log({command: input, source: "gh_console", timestamp: Time.now, console_user: console_session.instance_variable_get(:@console_user) || "not authenticated", shell_user: `whoami`.strip, console_session: console_session.session_id})
      rescue Exception => e # rubocop:disable Lint/RescueException
        e.message
      end
    end

    def self.start_session_log
      write_log("# session started at #{console_session.hrt}")
      at_exit do
        write_log("# session stopped at #{console_session.hrt}")
      end
    end
  end

  alias :old_readline :readline
  # Override readline to log all the
  # commands that are input
  def readline(*args)
    ln = old_readline(*args)

    begin
      # only log if ENV["GH_CONSOLE"] is set (happens in gh-console command)
      # https://github.com/github/shell/blob/master/bin/gh-console#L12
      History.write_log(ln) if !ENV["GH_CONSOLE"].nil? && ENV["GH_CONSOLE"] != ""
    end
    # Return the original readline method
    # to ensure console behaves as normal
    ln
  end
end
