# frozen_string_literal: true

module Statuses
  CLUSTER_MYSQL1  = "mysql1".freeze
  CLUSTER_BALLAST = "ballast".freeze
  CLUSTER_MOVING  = "moving".freeze

  # this service provides an interface for all queries going to the statuses
  # table. As we are transitioning the statuses table out of mysql1 and into
  # the ballast cluster, we will use this service to encapsulate routing
  # logic and to trigger data transition jobs
  class Service
    # READS
    #
    # For those times when you want the status just prior to this one.
    # Returns a Status or nil.
    def self.previous_status(repo_id:, sha:, status_id:)
      sql = Status.github_sql.new
      sql.add <<-SQL, repo_id: repo_id, sha: sha, status_id: status_id
        SELECT *
        FROM statuses
        WHERE repository_id = :repo_id
        AND sha = :sha
        AND id < :status_id
        ORDER BY id DESC
        LIMIT 1
        /*GitHub-StatusesService.previous_status*/
      SQL
      sql.models(Status).first
    end

    def self.count_per_sha_and_context(repo_id:, sha:, context:)
      # context is a MySQL varchar so it only accepts UTF-8 characters
      # up to 3 bytes.
      return 0 unless GitHub::UTF8.valid_unicode3?(context)
      sql = Status.github_sql.new
      sql.add <<-SQL, repo_id: repo_id, sha: sha, context: context
        SELECT COUNT(*)
        FROM statuses
        WHERE repository_id = :repo_id
        AND sha = :sha
        AND context = :context
        /*GitHub-StatusesService.count_per_sha_and_context*/
      SQL

      sql.value
    end

    # Public: Return the most recent statuses for each context associated with the
    # given commit sha.
    #
    # repository - A Repository
    # sha        - A String or Array of one or more String SHAs to query.
    #              SHAs must be the full 40-character SHA.
    #
    # Example:
    #   Statuses::Service.current_for_shas(repository_id: repo.id, shas: "deadbeef...")
    #   #=> [#<Status context: 'blah' ...>, #<Status context: 'whatevs' ...>, #<Status context: default ... >]
    #
    # Returns an Array of Statuses.
    def self.current_for_shas(repository_id:, shas:)
      return [] if shas.empty?
      GitHub.dogstats.time("statuses_service", tags: ["action:current_for_shas"]) do
        sql = Status.github_sql.new
        sql.add <<-SQL, repository_id: repository_id, shas: Array(shas)
          SELECT statuses.* FROM statuses
          JOIN (
            SELECT MAX(id) current_id FROM statuses
            FORCE INDEX (index_statuses_on_repository_id_and_sha_and_context)
            WHERE repository_id = :repository_id AND sha IN :shas
            GROUP BY sha, context
          ) current_statuses ON statuses.id = current_statuses.current_id
          ORDER BY statuses.id
          /*GitHub-StatusesService.current_for_shas*/
        SQL
        sql.models(Status)
      end
    end

    # Return the current statuses by sha for the given repository and shas
    #
    # repository - a Repository instance
    # shas       - an Array of String commit shas
    #
    # Returns a Hash of {"sha" => [ <Status>,  ... ], ... }
    def self.current_by_sha(repository_id:, shas:)
      shas = Array.wrap(shas)
      if shas.empty?
        {}
      else
        GitHub.dogstats.time("statuses_service", tags: ["action:current_by_sha"]) do
          current_for_shas(repository_id: repository_id, shas: shas).group_by(&:sha)
        end
      end
    end

    # Return the most recent Statuses for a list of contexts and commit_oids
    #
    # repository  - A Repository
    # contexts    - Array of String context names
    # commit_oids - Array of commit_oids for the Status records
    #
    # Returns an Array of Statuses
    def self.current_for(repo_id:, contexts:, commit_oids:, connection: Status.connection)
      commit_oids = commit_oids.map do |commit_oid|
        GitHub::SQL.LITERAL "x#{connection.quote(commit_oid)}"
      end

      sql = Status.github_sql.new
      sql.add <<-SQL, repository_id: repo_id, commit_oids: commit_oids, contexts: contexts
        SELECT statuses.* FROM statuses
          JOIN (
            SELECT MAX(id) current_id FROM statuses
             WHERE repository_id = :repository_id
               AND commit_oid IN :commit_oids
               AND context IN :contexts
             GROUP BY commit_oid, context
          ) current_statuses ON statuses.id = current_statuses.current_id
          /*GitHub-StatusesService.current_for*/
      SQL
      sql.models(Status)
    end

    # Retrieve Status contexts that have been posted to the repository
    # recently.
    #
    # Returns a Set of Strings.
    def self.recent_status_contexts(repo_id:, start:, limit:)
      GitHub.dogstats.time("protected_branch", tags: ["action:recent_status_contexts"]) do
        # If you change the timeframe here, be sure to update the user-visible
        # copy on the Protected Branch settings page.
        sql = Status.github_sql.new
        sql.add <<-SQL, repo_id: repo_id, start: start, limit: limit
          SELECT DISTINCT context
          FROM statuses
          WHERE created_at > :start
          AND repository_id = :repo_id
          LIMIT :limit
          /*GitHub-StatusesService.recent_status_contexts*/
        SQL

        sql.values.to_set
      end
    end

    # Returns an array of statuses
    def self.statuses_at_merge(repo_id:, sha:, merged_at:)
      sql = Status.github_sql.new
      sql.add <<-SQL, repo_id: repo_id, sha: sha, merged_at: merged_at
        SELECT *
        FROM statuses
        WHERE sha = :sha
        AND repository_id = :repo_id
        AND created_at < :merged_at
        ORDER BY id DESC
        /*GitHub-StatusesService.statuses_at_merge*/
      SQL
      sql.models(Status).uniq(&:context)
    end

    def self.statuses_for_repo_exist?(repository_id:)
      sql = Status.github_sql.new
      sql.add <<-SQL, repo_id: repository_id
        SELECT id
        FROM statuses
        WHERE repository_id = :repo_id
        LIMIT 1
        /*GitHub-StatusesService.statuses_for_repo_exist*/
      SQL
      sql.value?
    end

    # WRITES
    #
    # creates a status
    # called from the API
    #
    # Returns a tuple [Status record, ActiveRecord::RecordInvalid error or nil]
    def self.create_status(repo_id:, data:, user_id:)
      context_pieces = [data["context"]]
      data[:repository_id] = repo_id
      data[:creator_id] = user_id
      data[:context] = context_pieces.compact.join(" ") if context_pieces.any?

      GitHub::SchemaDomain.allowing_cross_domain_transactions do
        Status.create!(data)
      end
    rescue ActiveRecord::RecordInvalid => error
      return error.record, error
    end

    PER_PAGE = 30

    # Look up statuses by repository ID and SHA, and paginate the results.
    #
    # Returns a WillPaginate::Collection.
    def self.paginated_statuses(repo_id:, sha:, per_page: PER_PAGE, page: 1)
      collection = WillPaginate::Collection.new(page, per_page)
      if repo_id.nil? || sha.nil?
        collection.total_entries = 0
        return collection
      end

      sql = Status.github_sql.new
      sql.add <<~SQL, repo_id: repo_id, sha: sha, limit: per_page, offset: ((page - 1) * per_page)
        SELECT `statuses`.* FROM `statuses`
        WHERE `statuses`.`repository_id` = :repo_id
          AND `statuses`.`sha` = :sha
        ORDER BY statuses.id DESC
        LIMIT :limit OFFSET :offset
      SQL

      collection.replace sql.models(::Status)

      if collection.total_entries.nil?
        count_sql = Status.github_sql.new
        count_sql.add <<~SQL, repo_id: repo_id, sha: sha
          SELECT count(*) FROM `statuses`
          WHERE `statuses`.`repository_id` = :repo_id
            AND `statuses`.`sha` = :sha
        SQL
        collection.total_entries = count_sql.value
      end

      collection
    end
  end
end
