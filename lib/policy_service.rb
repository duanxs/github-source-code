# frozen_string_literal: true

module PolicyService
  def self.client
    Client.new
  end
end
