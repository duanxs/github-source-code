# frozen_string_literal: true

require "yaml"
require "active_support/core_ext/string/inflections"
require "active_support/core_ext/hash/keys"

require_relative "preview"

module REST
  module Previews
    DEFAULT = {} unless const_defined?(:DEFAULT)

    def self.register(data)
      preview = REST::Preview.new(**data)
      DEFAULT[preview.name] = preview
    end

    def self.get(preview_name)
      DEFAULT.fetch(preview_name)
    end

    def self.media_versions
      DEFAULT.values.map(&:media_version)
    end

    def self.version(name)
      get(name).media_version
    end

    def self.owning_teams(name)
      get(name).owning_teams
    end
  end
end

Dir.glob("./app/api/app/previews/*").each do |file|
  REST::Previews.register YAML.safe_load(File.read(file)).symbolize_keys
end
