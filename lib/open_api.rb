# frozen_string_literal: true

module OpenApi
  def self.root
    @root ||= Rails.root.join("app/api/description")
  end

  def self.description
    @description ||= begin
      release = OpenApi::Description::Release.parse(GitHub.openapi_release)
      OpenApi::Description::Root.new(release.to_h)
    end
  end

  def self.meta_schema
    JSON.parse(root.join("config/meta_schema.json").read)
  end

  # Basic structure visitor pattern
  def self.visit(node, visitor)
    visitor.call(node)
    case node
    when Array
      node.each do |child|
        if child.is_a?(Hash)
          visit(child, visitor)
        end
      end
    when Hash
      node.each_value do |child|
        if child.is_a?(Hash)
          visit(child, visitor)
        end
      end
    end
  end
end
