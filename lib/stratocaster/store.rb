# rubocop:disable Style/FrozenStringLiteralComment

module Stratocaster
  class Mysql2Store
    def query_options
      GitHub.dogstats.increment("stratocaster.store.query_options")
      nil
    end

    # Public: Get an event by a key.
    #
    # key - String or Integer Event ID.
    #
    # Returns a Stratocaster::Event.
    def get(key)
      Model.get(key)
    end

    # Public: Get multiple events by a list of keys.
    #
    # *keys - One or more String/Integer Event IDs.
    #
    # Returns an Array of Stratocaster::Events, in the order the keys are provided
    def get_all(*keys)
      Model.get_all(*keys)
    end

    # Public: Saves the event.  Creates it if the event has no ID.
    #
    # event - A Stratocaster::Event, or something really similar.
    #
    # Returns the saved Stratocaster::Event.
    def save(event)
      if event.id
        set event.id, event
      else
        create event
      end
    end

    # Internal: Creates an Event without an existing ID.
    #
    # event - A Stratocaster::Event, or something really similar.
    #
    # Returns the saved Stratocaster::Event.
    def create(event)
      if event.id
        raise ArgumentError, "Cannot re-create an Event, use #set instead: #{event.class} ##{event.id}"
      end

      Model.create_from_event!(event).to_event
    end

    # Public: Saves an Event.
    #
    # key   - A String or Integer Event ID.
    # event - A Stratocaster::Event.
    #
    # Returns the saved Stratocaster::Event.
    def set(key, event)
      GitHub.dogstats.increment("stratocaster.store.set")
      if event.id.to_s != key.to_s
        raise ArgumentError, "Key #{key} does not match #{event.class} ##{event.id}"
      end

      Model.find(key).tap { |m|
        m.update_from_event!(event)
      }.to_event
    end

    # Public: Deletes an Event.
    #
    # key - A String or Integer Event ID.
    #
    # Returns nothing.
    def delete(key)
      GitHub.dogstats.increment("stratocaster.store.delete")
      delete_all(key)
    end

    # Public: Deletes Events.
    #
    # key - An Array of String/Integer Event IDs.
    #
    # Returns nothing.
    def delete_all(*keys)
      Model.delete(keys)
    end
  end

  class MemoryStore
    def initialize
      reset!
    end

    def save(event)
      if event.id
        set event.id, event
      else
        create event
      end
    end

    def create(event)
      event.id = @num+=1
      set event.id, event
    end

    def get(id)
      if hash = @client[id.to_s]
        Event.new hash
      end
    end

    def set(id, event)
      event.updated_at = Time.now.utc
      event.created_at ||= event.updated_at
      @client[id.to_s] = stringify_payload(event.to_hash)
      @last = id
      event
    end

    def delete(id)
      @client.delete id.to_s
    end

    def all
      @client.values.map { |v| Event.new(v) }
    end

    def last
      get @last
    end

    def get_all(*keys)
      keys.map { |k| get(k) }
    end

    def delete_all(*keys)
      keys.map { |k| delete(k) }
    end

    def reset!
      @num = 0
      @client = {}
    end

    private

    PAYLOAD_KEYS = [:payload, "payload"]

    def stringify_payload(event_hash)
      key = PAYLOAD_KEYS.detect { |k| event_hash.key?(k) }
      return event_hash unless key
      deep_stringify_hash(event_hash[key])
      event_hash
    end

    def deep_stringify_hash(hash)
      hash.keys.each do |k|
        hash[k.to_s] = stringify_value(hash.delete(k))
      end
      hash
    end

    def deep_stringify_array(array)
      array.map! { |v| stringify_value(v) }
    end

    def stringify_value(value)
      case value
      when Hash then deep_stringify_hash(value)
      when Array then deep_stringify_array(value)
      else value
      end
    end
  end
end
