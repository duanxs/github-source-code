# frozen_string_literal: true

module Stratocaster::Filters
  class IpWhitelistingFilter < BaseFilter
    # Filters a list of events to exclude events created in the context of an
    # IP allow list-protected organization for which the viewer is not
    # connecting from an allowed IP address.
    #
    # events - Array of Stratocaster::Event items to filter
    #
    # Returns Array of Stratocaster::Event items
    def self.apply(events, viewer)
      unauthorized_org_ids = unauthorized_organization_ids(viewer)
      return events if unauthorized_org_ids.none?

      events.reject do |event|
        unauthorized_org_ids.include?(event.org["id"].to_i)
      end
    end

    # Internal: A list of organization IDs for which the viewer does not
    # currently have an allowed IP address.
    #
    # viewer - The User viewing this organization's events
    #
    # Returns Array of Integer.
    def self.unauthorized_organization_ids(viewer)
      return [] unless GitHub.context[:actor_ip]

      Platform::Authorization::IpWhitelisting.new(
        user: viewer, ip: GitHub.context[:actor_ip],
      ).protected_organization_ids
    end
  end
end
