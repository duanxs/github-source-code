# frozen_string_literal: true
#
module Stratocaster::Filters
  class ExternalIdentityFilter < BaseFilter
    # Filters a list of events to exclude events created in the context of an
    # SAML-protected organization for which the viewer does not have a valid
    # external identity session.
    #
    # events - Array of Stratocaster::Event items to filter
    #
    # Returns an Array of Stratocaster::Event items
    def self.apply(events, viewer)
      org_ids_without_active_saml_sessions = unauthorized_organization_ids(viewer)
      return events if org_ids_without_active_saml_sessions.none?

      events.reject do |event|
        org_ids_without_active_saml_sessions.include?(event.org["id"].to_i)
      end
    end

    # Internal: A list of organization IDs for which the viewer does not
    # currently have a valid external identity session.
    # Only considers organization IDs to be unauthorized in the context of a
    # web request, meaning this filter does not impact the API.
    #
    # viewer - The User viewing this organization's events
    #
    # Returns an Array of Integers
    def self.unauthorized_organization_ids(viewer)
      return [] unless GitHub.context[:actor_session]
      user_session = UserSession.find_by_id(GitHub.context[:actor_session])
      return [] unless user_session

      protected_org_ids_for(viewer) - currently_authorized_org_ids_for(user_session)
    end

    # Internal: A list of organization IDs for which the viewer is a member and
    # that require a external identity session to access
    #
    # viewer - The User viewing potentially protected organization content
    #
    # Returns an Array of Integers
    def self.protected_org_ids_for(viewer)
      Platform::Authorization::SAML.new(user: viewer).protected_organization_ids
    end

    # Internal: A list of organization IDs for which the viewer holds a valid
    # external identity session
    #
    # viewer - The User with potentially valid external identity sessions
    #
    # Returns an Array of Integers
    def self.currently_authorized_org_ids_for(user_session)
      current_external_identity_sessions(user_session).select { |s| s.target.is_a?(::Organization) }
                                                      .map { |s| s.target.id }
    end

    # Internal: A list of active external identity sessions for the current
    # user session
    # external identity session
    #
    # user_session -  The UserSession to find associated external identity
    #                 sessions
    #
    # Returns an ActiveRecord::Relation of ExternalIdentitySession objects
    def self.current_external_identity_sessions(user_session)
      user_session.external_identity_sessions.active.
        includes(external_identity: {provider: :target})
    end
  end
end
