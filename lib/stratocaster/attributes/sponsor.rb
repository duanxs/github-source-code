# rubocop:disable Style/FrozenStringLiteralComment

module Stratocaster
  class Attributes::Sponsor < Attributes
    EVENT_TYPE = "SponsorEvent".freeze

    # Initializes the attributes for this event.
    #
    # sponsor_id    - The Integer ID of the Repository or Topic being watched.
    # maintainer_id - The Integer ID of the User watching the Repository.
    #
    # Returns nothing.
    def from(sponsor_id, maintainer_id)
      return unless @sponsor = User.find_by_id(sponsor_id)
      return unless @maintainer = User.find_by_id(maintainer_id)
    end

    # Converts a saved Event into the args necessary to get the other
    # attributes for this event type.
    #
    # event - A saved Stratocaster::Event instance.
    #
    # Returns nothing
    def from_event(event)
      @maintainer = User.find_by(id: event.target_id)
      @sponsor  = event.sender_record
    end

    # Builds the Stratocaster::Event attribute hash.
    #
    # Returns a Hash of attributes for Stratocaster::Event#dispatch.
    def to_hash
      return {} unless @sponsor && @maintainer
      {
        event_type: EVENT_TYPE,
        sender: @sponsor,
        targets: targets,
        payload: payload,
      }
    end

    # Builds the list of targets for an event.
    #
    # timeline_type - an optional String for the timeline that will be shown
    #
    # Returns an Array of User IDs.
    def targets(timeline_type = nil)
      targets_for @sponsor, @maintainer.id
    end

    # Builds the event's payload.
    #
    # Returns a Hash.
    def payload
      {
        target: Api::Serializer.user_hash(@maintainer, full: true),
        maintainer_id: @maintainer.id,
        sponsor_id: @sponsor.id,
      }
    end
  end
end
