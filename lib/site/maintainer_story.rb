# frozen_string_literal: true
# rubocop:disable GitHub/FindByDef

module Site
  class MaintainerStory
    ATTRIBUTES = %i(
      bio
      blockquote
      body
      blog
      description
      github_handle
      hero_header
      hero_subtitle
      interests
      location
      name
      parameterized_name
      project_name
      project_url
      twitter_handle
    ).freeze

    attr_reader(*ATTRIBUTES)

    STORIES = [
      {
        bio: "Dirk Lemstra is an open source maintainer and full-time developer who lives in The Netherlands with his wife. Dirk maintains the widely-used ImageMagick and is a member of GitHub Sponsors.",
        body: %Q(
I knew I wanted to be a developer when I was 12. Problem-solving and helping people are two things I gravitate towards—and with open source, I’ve found the best of both worlds. I like seeing that what I do makes people’s daily lives a little bit better. It’s good karma.

Open source in general is sort of like electronic volunteer work. I get a lot back that isn’t monetary. I meet a lot of people and get to hear how they’re using ImageMagick. It’s an opportunity to have fun, expand my knowledge, and encourage kindness in the community.

When I’m not maintaining, I work on an education platform for middle schools in Holland, which allows students and teachers to access the information they need on a daily basis, like educational materials, curriculums, and grades. I’m a developer by heart and am in the fortunate position that I really, really like my job(s). I always say that if the days were longer, I would do more work. I like to think I do a good job balancing a full-time job and my role as an open source maintainer (though my wife would surely disagree).

> Open source in general is sort of like electronic volunteer work. I get a lot back that isn’t monetary.

### Introducing your friendly ImageMagick maintainers

Over five years ago, I changed my full-time contract so that I work 36 hours a week instead of 40. This allows me to take a full day off every two weeks to focus on open source. If I can, I also spend a couple of hours on it every evening to catch up.

Finding the right balance of users and maintainers is the biggest challenge of open source. If you want to really maintain a project, you need to invest a lot of time and energy, and there aren’t a lot of people who can do that.

My journey as a maintainer began in 2013. The company I was with at the time had a .NET photography library that used ImageMagick, but I needed more functionality. So I contacted the owner and asked if he wanted help. After a month of silence, I just did it myself and created a .NET library-based graph called Magick.NET.

<img src="/images//modules/site/readme/wide-dlemstra.jpg" alt="The ReadME Project: Dirk Lemstra" name="img-landscape">

At some point, I discovered some bugs that needed to be addressed within ImageMagick. I started by fixing a memory leak, and sent a patch by email to Cristy, the principal ImageMagick Architect (and only contact I could find). I kept emailing him on a weekly basis until one day he asked, “Do you want to join the team? Because it’s just me. So if you want to help, please join me.”

Everything you can think of relating to an image is possible with ImageMagick: You can create, edit, compose, convert, resize, rotate, transform, adjust colors, apply special effects, draw texts, lines and shapes. It can read and write images in over 200 formats. People have told me that most of the world’s largest websites rely on it. And it’s maintained by three people: me, Cristy, and Fred Weinhaus, a mathematician who assists us with our algorithms and answers a lot of questions from the community.

Cristy created ImageMagick in 1987 and in 1990, it became open source (though, technically, the term wouldn’t be coined until 1998). Though he had help here and there, he was the primary maintainer until I came along. When I joined, I think it gave Cristy the space to focus. Most people don’t realize that when there’s only a few maintainers behind such a popular project, each question and comment takes them further and further from maintaining (and advancing) the project. Thankfully, we also have some really good contributors. Our primary contributors are very knowledgeable about the project. If you have a question, they’ll answer it.

Cristy attracts these kinds of people. He was the one who taught me that if you want people to join your cause, be friendly. If you want them to contribute, treat them how you want to be treated. He also helped me learn to look past any negativity in comments and find the relevant message that advances the issue or project (often buried in a rant).

Though he may have been delayed in his email response to me back in 2013, Cristy’s response was friendly. That really helped. You can just tell he’s working hard and really likes what he’s doing. And that kindness and passion really made me want to join.

> When I joined, I think it gave Cristy the space to focus. Most people don’t realize when there’s only one maintainer behind such a popular project, each question and comment takes them further and further from maintaining (and advancing) the project.

<img src="/images/modules/site/readme/portrait-dlemstra.jpg" alt="The ReadME Project: Dirk Lemstra" name="img-portrait">

### Migrating to GitHub shifts the team’s focus back to maintaining

We migrated to GitHub in 2015. Cristy created the project over 30 years ago, and it’s always been pretty widely used. But we were using Subversion until 2015, when someone urged us to move to Git.

At some point, I had a couple of days off and decided to see if I could complete the migration, which I did. My next question was, where will we host it? So we set up a Git server, but it was just too much work; it needed constant updates and would break down frequently. Maintaining the project is already a second job in and of itself, and we had a mirror in place for GitHub. So we moved everything over, and I’m really happy we did. GitHub saves us a lot of time and energy since we don’t need to maintain everything ourselves, and allows users to report issues.

### The next best thing to a dream scenario

I’d love to find a company that would hire me full time to maintain ImageMagick. That’s the dream scenario—to be a full-time maintainer. But I’m also quite realistic that it’s probably not going to happen. Because even though a lot of people use our project, it’s also free, so why would they pay?

To my surprise, there have been a lot of people who want to offer support. The introduction of GitHub Sponsors was a gamechanger for me. Having a funding solution that sits within the GitHub platform has removed a lot of the pain points I previously experienced around gaining monetary support for my contributions to open source. Sponsors are (in almost all cases) already using the software, so it’s no extra work to become a sponsor. I set it up so there’s a wide range, from $1 to $1,000. And it helps so much. That’s money that gives me the freedom to keep my day job and keep maintaining ImageMagick.

Open source is everywhere now, and sometimes, people don’t even realize it. Luckily, more enterprises, like Google, Microsoft, and GitHub, are recognizing the innovators behind open source projects. And they’re starting to hire directly from the community and balancing things out a bit more. By hiring maintainers and open source contributors, and encouraging employees to contribute back and volunteer, it becomes more symbiotic. What goes around comes around.
),
        blog: "henryzoo.com",
        blockquote: "To my surprise, there have been a lot of people who want to offer support. The introduction of GitHub Sponsors was a gamechanger for me.",
        description: "Dirk Lemstra maintains the widely-used ImageMagick and is a member of GitHub Sponsors.",
        github_handle: "dlemstra",
        hero_header: "A balancing act: puzzles and practicality",
        hero_subtitle: "How Dirk found the middle ground between his day job and open source",
        interests: "Reading, games, (tacit) knowledge, content/media, maintenance, sacredness, virtue, embodiment, and belonging",
        location: "The Netherlands",
        name: "Dirk Lemstra",
        parameterized_name: "dirk-lemstra",
        project_name: "ImageMagick",
        project_url: "https://github.com/imagemagick",
        twitter_handle: "MagickNET",
      }
    ]

    def initialize(attributes = {})
      ATTRIBUTES.each do |attribute|
        instance_variable_set("@#{attribute}", attributes[attribute])
      end
    end

    def self.all
      STORIES.map { |story| new(story) }
    end

    def self.find_by_parameterized_name(parameterized_name)
      all.find { |story| story.parameterized_name == parameterized_name }
    end

    def content
      GitHub::Goomba::MarkdownPipeline.to_html(@body)
    end
  end
end
