# frozen_string_literal: true

module CommonSubscriptionHelper
  DefaultTime = Time.utc(2008, 4, 10) # Launch Day!

  def self.included(struct)
    # Public: Returns a Boolean specifying whether this Subscriber is ignoring
    # the List or Thread.
    struct.send :alias_method, :ignored?, :is_ignored

    # Public: Returns a Boolean specifying whether the User has a Subscription.
    struct.send :alias_method, :valid?, :is_valid
  end

  # Public: Returns a Boolean specifying whether this Subscriber was
  # mentioned.
  def mentioned?
    reason =~ /mention/i
  end

  # Checks if the subscription was created for a specific reason.  A blank
  # reason, or one that matches the default doesn't count.  List
  # subscriptions have a default of "list", and thread subscriptions use
  # "thread".
  def reason?(default_reason = nil)
    return false unless reason && !reason.empty?
    default_reason ? reason != default_reason : true
  end

  # Public: Returns a Boolean specifying whether Subscriber is subscribed
  # and not ignored.
  def subscribed?
    valid? && !ignored?
  end

  # Public: Returns a Boolean specifying whether Subscriber should be notified
  # only if they have participated or were @mentioned in the thread.
  def participation_only?
    !valid? && !ignored?
  end

  def created_at
    if time = @created_at
      time = @created_at = time.utc unless time.utc?
    end
    time || DefaultTime
  end

  def build_inspect(value)
    %(#<%s %s valid=%s, ignored=%s, reason=%s, created=%s>) % [
      self.class, value, is_valid.inspect,
      is_ignored.inspect, reason.inspect, created_at.inspect]
  end
end
