# rubocop:disable Style/FrozenStringLiteralComment
require "forwardable"

module Newsies
  # Public: Simple Struct for tracking the status of a User Subscription to a
  # List. This object implements the Null Object pattern.  Any Subscriptions
  # that don't exist will return false for #valid?.
  class Subscription
    extend Forwardable

    def_delegator :@list, :id, :list_id
    def_delegator :@list, :type, :list_type

    def_delegator :@thread, :id, :thread_id
    def_delegator :@thread, :type, :thread_type
    def_delegator :@thread, :type, :thread_class # backwards compatibility

    attr_reader :list, :thread, :is_valid, :is_ignored, :reason
    attr_accessor :thread_types, :events

    # this include _has_ to be after the attr_accessor declaration because it
    # needs access to those accessors as it is being mixed in
    include CommonSubscriptionHelper

    # Returns a Subscription object for the list and/or thread, that represents being _not_ subscribed
    def self.not_subscribed(list = nil, thread = nil)
      new(list, thread, false, false, nil, nil)
    end

    def initialize(list = nil, thread = nil, is_valid = nil, is_ignored = nil, reason = nil, created_at = nil, events = [])
      @list         = list
      @thread       = thread
      @is_valid     = is_valid
      @is_ignored   = is_ignored
      @reason       = reason
      @created_at   = created_at
      @thread_types = []
      @events       = events
    end

    def thread_type_only?(thread_type_class)
      !ignored? && @thread_types.include?(Newsies::Object.type_from_class(thread_type_class))
    end

    def participation_only?
      super && @thread_types.empty?
    end

    def events_only?
      subscribed? && @events.present?
    end

    def inspect
      build_inspect "list=##{list_id}, thread_types=#{thread_types.inspect}, events=#{events.inspect}, "
    end
  end
end
