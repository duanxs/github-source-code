# frozen_string_literal: true

module Newsies
  class Response < Resiliency::Response

    def stats_tag
      Newsies::Responses::STATS_TAG
    end

    def failure_app_tag
      Newsies::Responses::FAILURE_APP_TAG
    end
  end

end
