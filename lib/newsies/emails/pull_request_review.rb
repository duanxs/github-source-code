# rubocop:disable Style/FrozenStringLiteralComment

module Newsies
  module Emails
    class PullRequestReview < Newsies::Emails::Message
      def self.matches?(comment)
        comment.is_a?(::PullRequestReview)
      end

      delegate :pull_request, to: :comment

      def deliverable?
        super && settings.notify_pull_request_review_email?
      end

      def in_reply_to
        pull_request.message_id
      end

      def message_id
        "<#{pull_request.repository.name_with_owner}/pull/#{pull_request.number}/review/#{comment.id}@#{GitHub.host_name}>"
      end

      def subject
        "Re: [#{pull_request.repository.name_with_owner}] #{pull_request.title} (##{pull_request.number})"
      end

      def content
        body = comment.notifications_summary_title + "\n\n" + comment.body.to_s + "\n\n"
        body << comment.review_comments.map { |pr_comment| [pr_comment.excerpt.to_s.gsub(/\A/, "> "), "", pr_comment.body].compact.join("\n") }.join("\n\n")
      end

      def content_html
        body = "<p><b>@" + comment.user.login + "</b> #{comment.state_summary} this pull request.</p>\n\n"
        body << comment.body_html_for_email if comment.body
        body << comment.review_comments.map { |pr_comment|
          ["<hr>\n\n<p>In #{link_to(pr_comment.path, pr_comment.permalink)}:</p>",
            "<pre style='color:#555'>#{pr_comment.excerpt_html.gsub(/\A/, '&gt; ')}\n</pre>",
            pr_comment.body_html_for_email].compact.join("\n")
        }.join("\n\n")
      end

      def url
        if comment.show_in_timeline?
          comment.permalink
        else
          comment.review_comments.first.permalink
        end
      end

      def entity
        pull_request.repository
      end
    end
  end
end
