# rubocop:disable Style/FrozenStringLiteralComment

module Newsies
  module Emails
    class PullRequestComposableComment < Newsies::Emails::ComposableComment
      def self.matches?(comment)
        comment.is_a?(::ComposableComment) && comment.issue.pull_request?
      end

      delegate :pull_request, to: :issue

      # Public: User-facing conversation type.
      #
      # This is overriden here beacuse comment.notifications_thread is the
      # PR's issue and not the PR itself.
      #
      # Retuns a String name.
      def conversation_type
        "Pull Request"
      end

      def message_id
        "<#{repository.name_with_owner}/pull/#{issue.number}/c#{comment.id}@#{GitHub.host_name}>"
      end

      def in_reply_to
        issue.pull_request.message_id
      end
    end
  end
end
