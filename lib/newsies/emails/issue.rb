# rubocop:disable Style/FrozenStringLiteralComment

module Newsies
  module Emails
    class Issue < Newsies::Emails::Message
      def self.matches?(comment)
        comment.is_a?(::Issue) && !comment.pull_request?
      end

      alias issue comment

      def subject
        "[#{repository.name_with_owner}] #{comment.title}#{issue_subject_suffix}"
      end
    end
  end
end
