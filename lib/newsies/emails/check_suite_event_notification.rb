# frozen_string_literal: true

module Newsies
  module Emails
    class CheckSuiteEventNotification < Newsies::Emails::Message
      include ActionView::Helpers::TextHelper

      def self.matches?(comment)
        comment.is_a?(::CheckSuiteEventNotification)
      end

      delegate :check_suite, to: :comment
      delegate :short_head_sha, :head_sha, :head_repository_id, to: :check_suite

      # Deliver if:
      # - user wants to be notified of all check suite results OR
      # - user only wants to know about failed check suites.
      def deliverable?
        settings.continuous_integration_email? && comment.deliver?(settings)
      end

      # Can't reply to a check suite update
      def tokenized_list_address
        GitHub.urls.noreply_address
      end

      # Can't unsubscribe from a check suite update
      def tokenized_unsubscribe_address
      end

      def subject
        if pull_request.present?
          "[#{repo_name}] PR run #{verb_conclusion}: #{workflow_name} - #{pull_request.title} (#{short_head_sha})"
        else
          "[#{repo_name}] Run #{verb_conclusion}: #{workflow_name} - #{branch_name} (#{short_head_sha})"
        end
      end

      def pull_request
        @_pull_request ||= repository.pull_requests.find_by(
          head_ref: branch_name,
          head_repository_id: head_repository_id,
          head_sha: head_sha,
        )
      end

      def entity
        repository
      end

      def inbox_snippet
        comment.body
      end

      def content
        [
          heading,
          basic_details.join("\n"),
          "View results: #{comment.permalink}",
          ["Jobs:", job_list_text].join("\n"),
        ].join("\n\n")
      end

      def content_html
        message = [
          tag.h2(heading),
          tag.p(safe_join(basic_details, tag.br)),
          tag.p(link_to("View results", comment.permalink)),
          tag.div(safe_join(["Jobs:", job_list_html])),
        ]

        safe_join(message)
      end

      def footer
        [
          "-- ",
          reason_in_words,
          "Manage your GitHub Actions notifications: #{notification_settings_url}",
        ].join("\n")
      end

      def footer_html
        msg = [
          MDASH,
          reason_in_words,
          safe_join([
            "Manage your GitHub Actions notifications ",
            link_to("here", notification_settings_url),
            ".",
          ]),
        ]

        msg = safe_join(msg, tag.br)
        msg += mark_read_image if tracking_image_enabled?

        tag.p(msg, style: "font-size:small;-webkit-text-size-adjust:none;color:#666;")
      end

      private

      def heading
        "Run #{verb_conclusion} for #{branch_name} (#{short_head_sha})"
      end

      def basic_details
        [
          "Repository: #{repo_name}",
          "Workflow: #{workflow_name}",
          "Duration: #{run_duration}",
          "Finished: #{comment.updated_at}",
        ]
      end

      # Uses `ActiveSupport::Duration` to convert the seconds into a more
      # human-friendly phrase, e.g. "2 minutes and 5.3 seconds".
      def run_duration
        duration_in_seconds = check_suite.duration
        ActiveSupport::Duration.build(duration_in_seconds).inspect
      end

      def job_list_text
        check_runs.map do |check_run|
          "  * #{job_info(check_run, as_link: false).join(" ")}"
        end
      end

      def job_list_html
        tag.ul(safe_join(
          check_runs.map do |check_run|
            tag.li(safe_join(job_info(check_run, as_link: true), " "))
          end,
        ))
      end

      def check_runs
        @_check_runs ||= check_suite.latest_check_runs
      end

      def job_info(check_run, as_link:)
        [
          check_run_name(check_run, as_link: as_link),
          StatusCheckRollup.verb_state(check_run.conclusion),
          "(#{pluralize(check_run.annotation_count, "annotation")})",
        ]
      end

      def check_run_name(check_run, as_link:)
        if as_link
          link_to(check_run.visible_name, check_run.permalink(include_host: true))
        else
          check_run.visible_name
        end
      end

      def repository
        @_repository ||= comment.repository
      end

      def repo_name
        repository.name_with_owner
      end

      def workflow_name
        check_suite.name
      end

      def branch_name
        comment.head_branch
      end

      def verb_conclusion
        StatusCheckRollup.verb_state(comment.conclusion)
      end

      def notification_settings_url
        Rails.application.routes.url_helpers.settings_user_notifications_url(host: GitHub.url)
      end

      def rerequest_url
        Rails.application.routes.url_helpers.rerequest_check_suite_url(
          check_suite,
          user_id: repository.owner,
          repository: repository,
          only_failed_check_runs: true,
          host: GitHub.url,
        )
      end
    end
  end
end
