# rubocop:disable Style/FrozenStringLiteralComment

module Newsies
  module Emails
    class Message
      include ::ApplicationMailer::Helpers
      include ::ActionView::Helpers::UrlHelper
      include ::ActionView::Helpers::TagHelper
      include UrlHelper
      include EscapeHelper

      # Public: Determine if this message can be used for the given comment.
      #
      # comment - An object that will be used to construct the message
      #
      # Returns a boolean.
      def self.matches?(comment)
        false
      end

      delegate :comment, to: :@delivery

      attr_accessor :settings

      # Initialize a new email message
      #
      # delivery - a Newsies::Delivery object
      # settings - An instance of Newsies::Settings
      # options  - A Hash with message-specific options
      #            :reason - Symbol reason for sending the email:
      #              :mention, :team_mention, :assign, :author, nil
      #
      # Returns an instance of Message
      def initialize(delivery, settings, options = nil)
        @delivery = delivery
        @settings = settings
        @options = options || {}
        @tokenized_list_address = @recipient_email = nil
      end

      def deliverable?
        @settings && @delivery && recipient_email.present?
      end

      def replyable?
        GitHub.email_replies_enabled? && tokenized_list_address != GitHub.urls.noreply_address
      end

      # Public: Gets the GitHub user that created the comment that triggered
      # this notification.
      def sender
        @sender ||= comment.notifications_author
      end

      # Public: Email address of the user that's sending the message.
      #
      # Returns a String email address in "full name <address>" format.
      def sender_address
        if GitHub.email_replies_enabled? && !GitHub.mail_use_noreply_addr
          user_email sender, GitHub.urls.notifications_sender_address
        else
          user_email sender, GitHub.urls.noreply_address
        end
      end

      # Public: Email address of the user that will receive this message. Note
      # that this isn't necessary the address used in the To header. It may be
      # placed on the Cc or the Bcc depending on whether the user was mentioned,
      # created, or is assigned to the thread.
      #
      # Returns a String
      def recipient_address
        user_email @settings.user, recipient_email
      end

      def recipient_login
        @settings.user.login
      end

      # Like recipient_address but only the email address portion, no name.
      def recipient_email
        @recipient_email ||=
          if entity.respond_to?(:organization)
            @settings.email(entity.organization || :global).address
          else
            @settings.email(:global).address
          end
      end

      # Public: A constructed email address that identifies the reason an email
      # was received. This is used as a cc address which can be useful for filtering.
      #
      # Returns a String.
      def reason_email_address
        if reason.present?
          reason_email_address = quote_email_address("#{reason}@noreply.#{GitHub.urls.smtp_domain}")
          %{"#{reason.capitalize.tr('_-', ' ')}" #{reason_email_address}}
        end
      end

      # Public: Email address of the project distribution list. This is
      # typically of the form: "reply+TOKEN@reply.github.com".
      #
      # The tokenized_list_address must be used as the Reply-To address in
      # order to properly route mail replies.
      #
      # Returns a String email address.
      def tokenized_list_address
        @tokenized_list_address ||=
          GitHub::Email.reply_email(comment, @settings.user)
      end

      # Public: Email address of the email addressed used to unsubscribe from
      # the list.
      #
      # Returns a String email address.
      def tokenized_unsubscribe_address
        GitHub::Email.unsub_email(comment, @settings.user)
      end

      def tokenized_unsubscribe_auth_url
        token = Newsies::Authentication.token :mute_auth, @settings.user, @delivery.summary[:id]
        "#{GitHub.url}/notifications/unsubscribe-auth/#{token}"
      end

      def unsubscribe_vuln_url
        token = Newsies::Authentication.token :mute_vuln, @settings.user, @delivery.summary[:id]
        "#{GitHub.url}/notifications/unsubscribe-vulnerability/#{token}"
      end

      def tokenized_unsubscribe_list_url
        token = Newsies::Authentication.token :mute_list, @settings.user, @delivery.summary[:id]
        "#{GitHub.url}/notifications/unsubscribe/#{token}"
      end

      # Public: Email address of the project distribution list used in
      # no-reply situations.
      #
      # Returns a String email address in "full name <address>" format.
      def noreply_list_address
        if GitHub.email_replies_enabled? && !GitHub.mail_use_noreply_addr
          notifications_list_email(notifications_list, "#{notifications_list.name}@noreply.#{GitHub.urls.smtp_domain}")
        else
          notifications_list_email(notifications_list, GitHub.urls.noreply_address)
        end
      end

      # Public: Email address used in the From field of the mail message. This
      # header is important because the "show images from this user" feature
      # depends on it not changing.
      #
      # Returns a String
      def from
        sender_address
      end

      # Public: Array of actual email addresses to deliver the message to. The
      # To, Cc, and Bcc have no bearing on where the message is actually
      # delivered when this returns a non-nil value. The addresses listed here
      # are not included in the mail message, rather they are provided to the
      # SMTP server on the RCPT TO line.
      def destinations
        [recipient_email]
      end

      # Public: Email address used in the To field of the message. This is
      # not the recipient as you might first suspect but is instead set to the
      # notifications_list's distribution list.
      #
      # Returns a String email address in "full name <address>" format.
      def to
        noreply_list_address
      end

      # Public: Email address used in the Reply-To field of the mail message.
      # This is set to the tokened list address by default to emulate a
      # mailing list.
      #
      # Returns a String email address in "full name <address>" format.
      def reply_to
        notifications_list_email(notifications_list, tokenized_list_address)
      end

      # Public: Email address used in the Bcc field of the mail message.
      # This is set to the recipient's configured email address. This field is
      # mostly worthless since #destinations controls where the message is
      # delivered.
      #
      # Returns a String email address in "full name <address>" format.
      def bcc
      end

      # Public: Email address used in the Cc field of the mail message.
      # This is set to the recipient's configured email address sometimes.
      #
      # Returns a String email address in "full name <address>" format.
      def cc
        if participating?
          recipient_address
        end
      end

      # Public: Email addresses used in the Cc field of the mail message.
      #
      # Returns an array of email addresses.
      def cc_list
        [cc, reason_email_address].compact
      end

      # Public: The subject of this message. Subclasses should override this
      # method with a subject appropriate for the message type.
      def subject
        raise ArgumentError, "No subject provided by #{self.class}"
      end

      # Public: The Message-Id to use in outgoing email notifications. This
      # should follow RFC 2822's definition of message-id fields:
      #
      # http://tools.ietf.org/html/rfc2822#section-3.6.4
      def message_id
        comment.message_id
      end

      # Public: The message ID that this message is replying to.
      def in_reply_to
      end

      def list_id
        address = "<#{notifications_list}.#{notifications_list.owner}.#{GitHub.urls.host_name}>"
        "#{notifications_list.name_with_owner} #{address}"
      end

      def list_post_address
        tokenized_list_address
      end

      def unsubscribe_list
        if email = tokenized_unsubscribe_address
          %(<mailto:#{email}>, <#{tokenized_unsubscribe_list_url}>)
        end
      end

      # Public: Should the 1x1 tracking image be inserted into the message?
      # Defaults to true. Subclasses may override this setting to disable the
      # tracking image.
      def tracking_image_enabled?
        true
      end

      # Public: Permalink to the item that this message is for.
      def url
        comment.permalink
      end

      # Public: Headers to include in outgoing email related to this
      # notification. Subclasses may override this to include additional
      # values.
      #
      # Returns a Hash of header names and values.
      def headers
        @headers ||= begin
          {
            "Message-Id" => message_id,
            "In-Reply-To" => in_reply_to,
            "References" => in_reply_to,
            "Precedence" => "list",
            "Return-Path" => "<#{GitHub.urls.noreply_address}>",
            "X-GitHub-Sender" => sender.login,
            "X-GitHub-Recipient" => @settings.user.login,
            "X-GitHub-Reason" => reason,
            "List-ID" => list_id,
            "List-Archive" => notifications_list.permalink,
            "List-Post" => "<mailto:#{list_post_address}>",
            "List-Unsubscribe" => unsubscribe_list,
          }.delete_if { |k, v| v.blank? }
        end
      end

      # The message's body parts.
      #
      # Returns an array of [type, body] tuples. The body can either be a
      # string of the whole body or a symbol matching one of the template
      # in the app/views/mailer/newsies directory.
      def parts
        res = []
        res << ["text/plain", body]
        res << ["text/html", body_html] if body_html?
        res
      end

      def body
        [content, footer].join("\n\n")
      end

      # Internal: The content for this notification.
      def content
        comment.body
      end

      # Internal: The reason this notification is being emailed as a sentence.
      def reason_in_words
        "You are receiving this because #{GitHub.newsies.reason_in_words(reason, email: true)}."
      end

      # Internal: The reason this notification is being emailed.
      def reason
        @options[:reason].to_s
      end

      # Internal: The content header for this notification if any.
      #
      # Useful to clarify user actions like "@user commented on" and prevent phishing attacks.
      def content_header
      end

      # Internal: The footer for this notification.
      #
      # Note that the space is intentional to match RFC expectations.
      def footer
        if replyable?
          "-- \n#{reason_in_words}\nReply to this email directly or view it on #{GitHub.flavor}:\n#{url}"
        else
          "-- \n#{reason_in_words}\nView it on #{GitHub.flavor}:\n#{url}"
        end
      end

      # Check that the Message supports HTML email bodies. By default, this is
      # enabled for comment objects that respond to :body_html (all user
      # content on github). Message subclasses may also override this and the
      # body_html method to provide a custom implementation.
      #
      # Returns true when this message supports generating an HTML email body.
      def body_html?
        comment.respond_to?(:body_html)
      end

      # The body HTML used in email notification messages. By default, these include
      # the #body_html and #url in a signature area.
      #
      # Returns the email body as a String of simple HTML markup.
      def body_html
        html = [content_header_html, content_html, "", footer_html]
        html << json_ld_html

        html.join "\n"
      end

      # The content header HTML
      def content_header_html
        content_tag(:p, content_header)
      end

      # The body HTML without any footer.
      def content_html
        if comment.respond_to?(:body_html_for_email)
          comment.body_html_for_email
        else
          comment.body_html
        end
      end

      # The signature portion of body_html.
      MDASH = GitHub::HTMLSafeString.make("&mdash;")
      def footer_html
        # Using a unicode `—` messes with the  encoding of the resulting message.
        msg = [MDASH, tag("br"), reason_in_words, tag("br")]

        if replyable?
          msg << "Reply to this email directly, "
          msg << link_to("view it on #{GitHub.flavor}", url) << ", or "
          msg << link_to("unsubscribe", tokenized_unsubscribe_auth_url) << "."
        else
          msg << link_to("View it on #{GitHub.flavor}", url) << " or "
          msg << link_to("unsubscribe", tokenized_unsubscribe_auth_url) << "."
        end

        msg = safe_join(msg)
        msg += mark_read_image if tracking_image_enabled?

        content_tag(:p, msg, style: "font-size:small;-webkit-text-size-adjust:none;color:#666;")
      end

      # A Gmail proprietary tag, which inserts a "View on GitHub" link on the
      # thread in the inbox.
      #
      # https://developers.google.com/gmail/actions/reference/go-to-action#view_action
      #
      # Returns a hash.
      def gmail_view_action
        {
          "@context": "http://schema.org",
          "@type": "EmailMessage",
          potentialAction: {
            "@type": "ViewAction",
            target: url,
            url: url,
            name: "View #{conversation_type}",
          },
          description: "View this #{conversation_type} on #{GitHub.flavor}",
          "publisher": {
            "@type": "Organization",
            name: "GitHub",
            url: "https://github.com",
          },
        }
      end

      # create the script for the JSON-LD data for Gmail
      def json_ld_html
        schemas = [gmail_view_action]
        content_tag(:script, json_escape(outlook_to_json(schemas)), { type: "application/ld+json" }, false)
      end

      def outlook_to_json(hash)
        JSON.generate(hash, {
          space: " ",
          object_nl: "\n",
          array_nl: "\n",
        })
      end

      # Public: User-facing conversation type. For example: "Pull Request",
      # or "Issue".
      #
      # Retuns a String name.
      def conversation_type
        if comment.notifications_thread.try(:pull_request?)
          "Pull Request"
        else
          comment.notifications_thread.class.name.titleize
        end
      end

      def mark_read_image
        tag(:img, src: mark_read_url, height: 1, width: 1, alt: "")
      end

      def mark_read_url
        token = Newsies::Authentication.token(
          :beacon,
          @settings.user,
          @delivery.summary[:id],
          { comment_type: @delivery.comment_type, comment_id: @delivery.comment_id },
        )

        "#{GitHub.url}/notifications/beacon/#{token}.gif"
      end

      # Tacked at the end of Issues-related notifications.
      def issue_subject_suffix
        if issue.present?
          " (##{issue.number})"
        else
          ""
        end
      end

      # Public: Controls whether to include categories as part of Sendgrid's
      # SMTP API and email headers. By default, this is disabled for notifications
      # emails since the presence of the "X-SMTPAPI" means we'll statically
      # route to sendgrid.
      #
      # Returns a Boolean.
      def category_tracking_enabled?
        false
      end

      # Public: Should this email be blocked from being delivered because
      # the user does not meet the notification restriction requirements for
      # an organization?
      #
      # Returns a Boolean.
      def blocked_by_organization_restriction?
        owner = notifications_list.owner

        owner.organization? &&
        !owner.async_user_can_receive_email_notifications?(settings.user).sync
      end

      # The after_delivery method is called after successful delivery of the
      # email and receives the delivered Mail::Message instance as an argument.
      #
      # Override this method in your Newsies::Emails::Message subclass if any
      # post-processing is needed.
      #
      # See: Newsies::EmailHandler#deliver
      def after_delivery(_mail)
      end

      private

      # Private:  Subclasses should override this method to return an issue
      # if one is needed to construct the email.
      def issue
      end

      # Internal method required in order to pass a block to content_tag
      #
      # &block - A block that returns a String.
      #
      # Returns the String that was returned by the block.
      def capture
        yield if block_given?
      end

      # Private: Checks if this message is being sent to someone who was participating
      # Depends on the specified reason in the message @options
      #
      # Returns a boolean
      def participating?
        ::Newsies::Reasons::Participating.include?(@options[:reason])
      end

      def notifications_list
        comment.notifications_list
      end
      alias_method :repository, :notifications_list

      def entity
        comment.entity
      end
    end
  end
end
