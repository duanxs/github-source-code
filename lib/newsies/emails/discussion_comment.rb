# frozen_string_literal: true

module Newsies
  module Emails
    class DiscussionComment < Newsies::Emails::Message
      def self.matches?(comment)
        comment.is_a?(::DiscussionComment)
      end

      delegate :discussion, to: :comment

      def in_reply_to
        discussion.message_id
      end

      def subject
        "Re: [#{repository.name_with_owner}] #{discussion.title} (##{discussion.number})"
      end
    end
  end
end
