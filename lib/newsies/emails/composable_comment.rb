# rubocop:disable Style/FrozenStringLiteralComment

module Newsies
  module Emails
    class ComposableComment < Newsies::Emails::Message
      def self.matches?(comment)
        comment.is_a?(::ComposableComment) && !comment.issue.pull_request?
      end

      delegate :issue, to: :comment

      def subject
        "Re: [#{issue.repository.name_with_owner}] #{issue.title}#{issue_subject_suffix}"
      end

      def in_reply_to
        issue.message_id
      end

      def deliverable?
        super && settings.notify_comment_email?
      end
    end
  end
end
