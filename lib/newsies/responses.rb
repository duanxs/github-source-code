# frozen_string_literal: true
#
module Newsies
  module Responses
    STATS_TAG = "newsies.unavailable_exception.count"
    FAILURE_APP_TAG = "github-notifications"

    class Boolean < Resiliency::Responses::Boolean
      def stats_tag
        Newsies::Responses::STATS_TAG
      end

      def failure_app_tag
        Newsies::Responses::FAILURE_APP_TAG
      end
    end

    class Array < Resiliency::Responses::Array
      def stats_tag
        Newsies::Responses::STATS_TAG
      end

      def failure_app_tag
        Newsies::Responses::FAILURE_APP_TAG
      end
    end

    class Count < Resiliency::Responses::Count
      def stats_tag
        Newsies::Responses::STATS_TAG
      end

      def failure_app_tag
        Newsies::Responses::FAILURE_APP_TAG
      end
    end

    class Set < Resiliency::Responses::Set
      def stats_tag
        Newsies::Responses::STATS_TAG
      end

      def failure_app_tag
        Newsies::Responses::FAILURE_APP_TAG
      end
    end

    class WillPaginateCollection < Resiliency::Responses::WillPaginateCollection
      def stats_tag
        Newsies::Responses::STATS_TAG
      end

      def failure_app_tag
        Newsies::Responses::FAILURE_APP_TAG
      end
    end

    class PageWithPreviousAndNextFlags < Resiliency::Response
      def initialize(&block)
        # Initialize with a plain object as the default return value.
        super(Newsies::PageWithPreviousAndNextFlags.new)
      end

      def stats_tag
        Newsies::Responses::STATS_TAG
      end

      def failure_app_tag
        Newsies::Responses::FAILURE_APP_TAG
      end
    end

  end
end
