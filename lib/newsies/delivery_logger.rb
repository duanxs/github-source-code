# rubocop:disable Style/FrozenStringLiteralComment

module Newsies
  class DeliveryLogger
    # Public: Logs a delivery to splunk and hydro.
    #
    # delivery               - An instance of either Newsies::Delivery or Newsies::MobilePushNotificationDelivery.
    # user                   - A User object.
    # reason                 - String specifying the reason the user received a notification.
    # handler_key            - Key specifying the handler.
    # root_job_enqueued_at   - Integer or Float epoch timestamp for when the first notification
    #                          delivery job for the content in question was enqueued. Note that since this job may be a
    #                          dependent of other delivery jobs, this might be the time at which an ancestor job was
    #                          enqueued.
    # additional_splunk_data - Additional key/value(s) to log to splunk.
    #
    # Returns nothing.
    def self.log_to_splunk_and_hydro(delivery, user:, reason:, handler_key:, root_job_enqueued_at:, **additional_splunk_data)
      log_to_splunk(
        delivery,
        user: user,
        reason: reason,
        handler_key: handler_key,
        root_job_enqueued_at: root_job_enqueued_at,
        **additional_splunk_data,
      )

      log_to_hydro(delivery, user: user, reason: reason, handler_key: handler_key)
    end

    def self.log_to_hydro(delivery, user:, reason:, handler_key:)
      GlobalInstrumenter.instrument("notifications.delivery",
        user: user,
        handler: handler_key,
        list_type: delivery.list_type,
        list_id: delivery.list_id,
        thread_type: delivery.thread_type,
        thread_id: delivery.thread_id,
        comment_type: delivery.comment_type,
        comment_id: delivery.comment_id,
        reason: reason.to_s,
      )
    end

    def self.log_to_splunk(delivery, user:, reason:, handler_key:, root_job_enqueued_at:, **additional)
      delivered_at_time = Time.now.to_f

      log_hash = {
        list_type: delivery.list_type,
        list_id: delivery.list_id,
        thread_type: delivery.thread_type,
        thread_id: delivery.thread_id,
        comment_type: delivery.comment_type,
        comment_id: delivery.comment_id,
        handler: handler_key,
        reason: reason.to_s,
        recipient: user.login,
        enqueued_at: root_job_enqueued_at,
        delivered_at: delivered_at_time,
        time_to_sent: root_job_enqueued_at.present? ? delivered_at_time - root_job_enqueued_at : nil,
      }.merge(additional)

      GitHub::Logger.log(log_hash)
    end
  end
end
