# frozen_string_literal: true

module Newsies
  class MobilePushHandler
    def handler_key
      :mobile_push
    end

    def deliver(delivery, settings, options)
      Responses::Boolean.new do
        DeliverMobilePushNotificationsJob.perform_later(
          delivery.comment,
          settings.id,
          root_job_enqueued_at: options[:root_job_enqueued_at],
        )
        true
      end
    end
  end
end
