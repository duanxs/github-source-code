# frozen_string_literal: true

require "newsies/object"

module Newsies
  class Thread < ::Newsies::Object
    # Public: Ensures that provided instance is a Newsies::Thread.
    #
    # object - The Newsies::Thread or object to convert to a Newsies::Thread.
    # list - An optional Newsies::List to assign to this thread.
    #
    # Returns a Newsies::Thread instance.
    def self.to_object(object, list: nil)
      return object if object.is_a?(self) && list.nil?
      new(to_type(object), to_id(object), list: list)
    end

    # Public: Converts a key into a Newsies::Thread instance.
    #
    # key - The String representation of type and id.
    # list - An optional Newsies::List to assign to this thread.
    #
    # Returns Newsies::Thread instance.
    def self.from_key(key, list: nil, separator: KEY_SEPARATOR)
      raise InvalidKey.new(key, separator) unless valid_key?(key, separator: separator)

      type, id = key.split(separator, 2)

      if list
        new(type, id, list: list)
      else
        new(type, id)
      end
    end

    attr_reader :list

    def initialize(type, id, list: nil)
      super(type, id)

      @list = list
    end

    def list_key(separator = KEY_SEPARATOR)
      require_list
      "#{@list.type}#{separator}#{@list.id}#{separator}#{key(separator)}"
    end

    # Public: Awkward but backward compatible thread_key with list id prefix.
    def list_id_key(separator = KEY_SEPARATOR)
      require_list
      "#{@list.id}#{separator}#{key(separator)}"
    end

    def list_id
      require_list
      @list.id
    end

    def list_type
      require_list
      @list.type
    end

    private

    def require_list
      raise ArgumentError, "@list is required" if @list.blank?
    end
  end
end
