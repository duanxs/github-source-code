# frozen_string_literal: true
module Newsies
  class Error < StandardError
    def areas_of_responsibility
      [:notifications]
    end
  end
end
