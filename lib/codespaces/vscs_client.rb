# frozen_string_literal: true

require "net/http"
require "uri"
require "json"
require "codespaces"

module Codespaces
  # Public: Interface to the VSCS backend.
  class VscsClient < Client
    DOTFILES_REPOSITORY_NAME = "dotfiles"

    SKU = "basicLinux"
    SKU_GITHUB_INTERNAL = "premiumLinux"

    AUTO_SHUTDOWN_MINUTES = 30
    ENVIRONMENT_TYPE = "cloudEnvironment"

    attr_reader :vscs_target_config, :plan_id, :user

    def self.for_codespace(codespace, plan: codespace.plan, user: codespace.owner, **kwargs)
      new(vscs_target: codespace.vscs_target,
          plan_id: plan.vscs_id,
          user: user,
          resource_provider: plan.resource_provider,
          **kwargs)
    end

    # Public: Constructor
    #
    # vscs_target - The Symbol VSCS target (e.g. :production, :ppe, etc) to direct API calls at.
    # plan_id     - The String Azure plan ID to make client requests against.
    #               E.g. "/subscriptions/819be122-a32e-469a-b97f-d5e5cb21f5ab/resourceGroups/WestUs2-2/providers/Microsoft.VSOnline/plans/plan-720a3363"
    # user        - The User to make client requests on behalf of.
    # timeouts    - (Optional) The timeouts to be used for API calls. Defaults to the 15 secs.
    #
    # Returns nothing.
    def initialize(vscs_target:, plan_id:, user:, resource_provider:, **kwargs)
      @vscs_target_config = fetch_vscs_target_config(vscs_target)
      @plan_id = plan_id
      @user = user
      @resource_provider = resource_provider
      super **kwargs
    end

    def create_environment(location, repo, name:, moniker:)
      git_username, git_email = User.git_author_info(user)
      git_email = user.default_author_email(repo) || git_email

      # XXX @orph: read SKU from devcontainer.json
      sku = repo.name_with_owner == "github/github" ? SKU_GITHUB_INTERNAL : SKU

      body = {
        id: "",
        type: ENVIRONMENT_TYPE,
        friendlyName: name,
        seed: {
          type: "git",
          moniker: moniker,
          gitConfig: {
            userName: git_username,
            userEmail: git_email,
          },
        },
        personalization: {
          dotfilesTargetPath: "~/dotfiles",
        },
        state: "Provisioning",
        connection: {
          sessionId: "",
          sessionPath: "",
        },
        created: Time.now.utc.iso8601(3),
        autoShutdownDelayMinutes: AUTO_SHUTDOWN_MINUTES,
        skuName: sku,
        experimentalFeatures: {
          customContainers: true,
        },
      }

      if dotfileRepo = user.find_repo_by_name(DOTFILES_REPOSITORY_NAME)
        body[:personalization][:dotfilesRepository] = dotfileRepo.permalink
      end

      log_body_on_error do
        env = vscs_api(:post, "/api/v1/environments", body: body.to_json, tags: ["location:#{location}"])
        return unless env

        ::Codespaces::Environment.from_json(env)
      end
    end

    def delete_environment(id)
      vscs_api(:delete, "/api/v1/environments/#{EscapeUtils.escape_uri_component(id)}", body: {}.to_json)
    end

    def list_environments(name: nil)
      url = Addressable::URI.new(path: "/api/v1/environments", query_values: { name: name }.compact).to_s
      vscs_api(:get, url).map do |env|
        ::Codespaces::Environment.from_json(env)
      end
    end

    def fetch_environment(id)
      raise ArgumentError, "empty environment id" if id.blank?

      vscs_api(:get, "/api/v1/environments/#{EscapeUtils.escape_uri_component(id)}")
    end

    def start_environment(id)
      raise ArgumentError, "empty environment id" if id.blank?

      _, resp = vscs_api_raw(:post, "/api/v1/environments/#{EscapeUtils.escape_uri_component(id)}/start", body: nil)
      resp
    end

    def shutdown_environment(id)
      raise ArgumentError, "empty environment id" if id.blank?

      vscs_api(:post, "/api/v1/environments/#{EscapeUtils.escape_uri_component(id)}/shutdown", body: nil)
    end

    def correlation_request_id(resp)
      resp.headers["vssaas-request-id"] if resp&.headers
    end

    def serialize_request_data
      vscs_request_data = request_data + arm_client.request_data
      vscs_request_data.sort_by { |req| req[:timestamp] }
    end

    private

    def arm_client
      @arm_client ||= Codespaces::ArmClient.new(resource_provider: @resource_provider)
    end

    def vscs_api_raw(method, path, body: {}, tags: [], retries: 0, force_refresh_token: false)
      token = arm_client.fetch_cascade_token(vscs_target: vscs_target_config[:name], plan_id: plan_id, user: user, force_refresh: force_refresh_token)

      url, resp = vscs_api_raw_authed(method, path, token, body: body, tags: tags)

      # If we get a 401, we'll try a few times to make the request again with a
      # refreshed token since it could be that the token has expired
      if resp.status == 401 && retries < 3
        GitHub.dogstats.increment("codespaces.client.unauthed_retry", tags: [method, "retry_count:#{retries}"])
        return vscs_api_raw(method, path, body: body, tags: tags, retries: retries += 1, force_refresh_token: true)
      end

      return url, resp
    end

    def vscs_api_raw_authed(method, path, token, body: {}, tags: [])
      headers = {
        "Content-Type" => "application/json",
        "Authorization" => "Bearer #{token}",
      }

      start_time = GitHub::Dogstats.monotonic_time

      resp = if method == :get
        vscs_connection.get(path, body, headers)
      else
        vscs_connection.run_request(method, path, body, headers)
      end

      request_url = vscs_target_config[:api_url] + path
      log_response(method, request_url, resp)

      caller_base_label = caller_locations(1, 1)[0].base_label
      all_tags = ["caller:#{caller_base_label}", "status:#{resp.status}"].concat(tags)
      GitHub.dogstats.timing_since("codespaces.client.vscs_api.response", start_time, tags: all_tags)

      return request_url, resp
    end

    def vscs_api(method, path, body: {}, tags: [])
      request_url, resp = vscs_api_raw(method, path, body: body, tags: tags)

      unless resp.success?
        raise BadResponseError, request_err_message("Bad response", method, request_url, resp)
      end

      GitHub::JSON.parse(resp.body)
    rescue Yajl::ParseError
      raise BadResponseError, request_err_message("Bad response", method, request_url, resp, "invalid JSON")
    rescue Faraday::TimeoutError
      raise TimeoutError, request_err_message("Timeout exceeded", method, request_url, resp)
    rescue Faraday::ConnectionFailed => e
      raise ConnectionFailed, request_err_message("Connection failed: #{e.message}", method, request_url, resp)
    end

    def vscs_connection
      @vscs_connection ||= connection_for(vscs_target_config[:api_url], retries: 3) do |f|
        f.use FollowVSORedirects
      end
    end

    def log_context
      @log_context ||= {
        user_id: user.id,
        vso_plan_id: plan_id,
      }
    end
  end

  class FollowVSORedirects < FaradayMiddleware::FollowRedirects
    ALLOWED_REDIRECT_ORIGIN = "visualstudio.com"

    # By default FollowRedirects only includes the Authorization header iff it's
    # being redirected to the same host as the original request. But in our case,
    # it's *also* OK to include if we're redirecting to a subdomain of the
    # original host or any subdomain of 'visualstudio.com' since that'd be a
    # specific Azure AZ.
    def redirect_to_same_host?(from_url, to_url)
      return true if super(from_url, to_url)

      to_uri = URI.parse(to_url)
      from_uri = URI.parse(from_url)
      to_uri.host.end_with?(from_uri.host, ALLOWED_REDIRECT_ORIGIN)
    end
  end
end
