# frozen_string_literal: true

require "net/http"
require "uri"
require "json"
require "codespaces"

module Codespaces
  # Public: Interface to the Azure Resource Management API
  class ArmClient < Client
    LOGIN_URL = "https://login.microsoftonline.com"
    ARM_URL = "https://management.azure.com"
    CASCADE_TOKEN_EXPIRATION = 24.hours
    STORAGE_ACCOUNT_TYPE = "Microsoft.Storage/storageAccounts"

    def initialize(resource_provider:, timeouts: DEFAULT_TIMEOUTS, logger: GitHub::Logger)
      @resource_provider = resource_provider
      super(timeouts: timeouts, logger: logger)
    end

    def create_resource_group(subscription, name, location)
      path = "/subscriptions/#{subscription}/resourceGroups/#{name}"
      arm_api(:put, path,
        api_version: "2019-10-01",
        body: {"location": "#{location}"}.to_json,
        tags: ["location:#{location}"]
      )
    end

    def delete_resource_group(subscription, name)
      path = "/subscriptions/#{subscription}/resourceGroups/#{name}"
      arm_api(:delete, path, api_version: "2019-10-01", body: {}.to_json)
    end

    def create_plan(vscs_target:, plan_id:, location:)
      with_log_context(plan_id: plan_id) do
        arm_api(:put, plan_id,
          api_version: vscs_api_version(vscs_target, resource_provider),
          body: {"location": "#{location}"}.to_json,
          tags: ["location:#{location}"]
        )
      end
    end

    def delete_plan(vscs_target:, plan_id:)
      with_log_context(plan_id: plan_id) do
        arm_api(:delete, plan_id,
          api_version: vscs_api_version(vscs_target, resource_provider),
          body: {}.to_json
        )
      end
    end

    def list_storage_account_names
      GitHub::JSON.parse(GitHub.codespaces_billing_queues_and_tokens)
    end

    # Public: Fetches a cascade token.
    #
    # vscs_target - The Symbol VSCS target to generate the token for. E.g. :production, :ppe, etc.
    # plan_id     - The String Azure plan ID to scope the token to.
    #               E.g. "/subscriptions/819be122-a32e-469a-b97f-d5e5cb21f5ab/resourceGroups/WestUs2-2/providers/Microsoft.VSOnline/plans/plan-720a3363"
    # user        - The User whose identity will be tied to the token.
    # cache       - Specifies whether or not the token should be cached
    #               locally in KV (optional, default: true).
    # expires_in  - The time in seconds before the cascade token should expire
    #               and no longer be valid for requests against VSO (optional, default: 24 hours).
    # environment_id - The environment that the cascade token should be scoped to (optional, default: nil)
    #
    # Returns a token String.
    def fetch_cascade_token(vscs_target:, plan_id:, user:, cache: true, expires_in: CASCADE_TOKEN_EXPIRATION, environment_id: nil, force_refresh: false)
      fetch = -> {
        fetch_cascade_token! \
          vscs_target: vscs_target,
          plan_id: plan_id,
          user: user,
          expires_in: expires_in,
          environment_id: environment_id
      }

      if cache
        cache_token(plan_id, user.id, type: "cascade", force: force_refresh) do
          [fetch.call, expires_in]
        end
      else
        fetch.call
      end
    end

    private

    attr_accessor :arm_token, :resource_provider

    def fetch_cascade_token!(vscs_target:, plan_id:, user:, expires_in:, environment_id: nil)
      post_body = {
        "identity": {
          "id": "#{user.id}",
          "username": "#{user.login}",
          "displayName": "#{user.name}",
        },
        "scope": "write:environments",
        "expiration": expires_in.from_now.to_i
      }
      post_body["environmentIds"] = Array(environment_id) unless environment_id.blank?

      resp = arm_api(:post, "#{plan_id}/writeDelegates",
        api_version: vscs_api_version(vscs_target, resource_provider),
        body: post_body.to_json
      )
      resp["accessToken"]
    end

    def fetch_arm_token
      return arm_token if arm_token

      service_principal = ::GitHub.codespaces_service_principal
      token_path = "/#{service_principal[:tenant_id]}/oauth2/token"

      self.arm_token = cache_token(token_path, type: "arm") do
        start_time = GitHub::Dogstats.monotonic_time
        resp = login_connection.post(token_path, {
          "grant_type" => "client_credentials",
          "resource" => "https://management.core.windows.net",
          "client_id" => service_principal[:client_id],
          "client_secret" => service_principal[:client_secret],
        })

        request_url = LOGIN_URL + token_path
        log_response(:post, request_url, resp)

        tags = ["caller:fetch_arm_token", "status:#{resp.status}"]
        GitHub.dogstats.timing_since("codespaces.client.fetch_arm_token.response", start_time, tags: tags)

        unless resp.success?
          raise Codespaces::Error, request_err_message("Bad response", :post, request_url, resp, "Could not get ARM token")
        end

        begin
          json = GitHub::JSON.parse(resp.body)
        rescue Yajl::ParseError => error
          raise Codespaces::Error, request_err_message("Bad response", :post, request_url, resp, "Could not get ARM token, invalid JSON")
        end

        token, expires_in = json.values_at("access_token", "expires_in")
        [token, expires_in.to_i]
      end
    end

    def arm_api(method, path, api_version:, query_params: {}, body: {}, tags: [])
      token = fetch_arm_token
      headers = {
        "Content-Type" => "application/json",
        "Authorization" => "Bearer #{token}",
      }
      path_with_query_params = "#{path}?#{CGI.unescape(query_params.merge("api-version": api_version).to_query)}"

      start_time = GitHub::Dogstats.monotonic_time

      resp = if method == :get
        arm_connection.get(path_with_query_params, body, headers)
      else
        arm_connection.run_request(method, path_with_query_params, body, headers)
      end

      request_url = ARM_URL + path_with_query_params
      log_response(method, request_url, resp)

      caller_base_label = caller_locations(1, 1)[0].base_label
      all_tags = ["caller:#{caller_base_label}", "status:#{resp.status}"].concat(tags)
      GitHub.dogstats.timing_since("codespaces.client.arm_api.response", start_time, tags: all_tags)

      unless resp.success?
        raise BadResponseError, request_err_message("Bad response", method, request_url, resp, resp.body)
      end

      GitHub::JSON.parse(resp.body)
    rescue Yajl::ParseError
      raise BadResponseError, request_err_message("Bad response", method, request_url, resp, "invalid JSON")
    rescue Faraday::TimeoutError
      raise TimeoutError, request_err_message("Timeout exceeded", method, request_url, resp)
    rescue Faraday::ConnectionFailed => e
      raise ConnectionFailed, request_err_message("Connection failed: #{e.message}", method, request_url, resp)
    end

    def login_connection
      @login_connection ||= connection_for(LOGIN_URL) do |f|
        f.request :url_encoded
      end
    end

    def arm_connection
      @api_connection ||= connection_for(ARM_URL, retries: 3)
    end

    def correlation_request_id(resp)
      resp.headers["x-ms-correlation-request-id"] if resp&.headers
    end
  end
end
