# frozen_string_literal: true

require "net/http"
require "uri"
require "json"
require "codespaces"

module Codespaces
  # Public: Interface to the VSCS backend.
  class Client
    include GitHub::AreasOfResponsibility

    class TimeoutError < Codespaces::Error; end

    class ConnectionFailed < Codespaces::Error; end
    class BadResponseError < Codespaces::Error; end

    DEFAULT_TIMEOUTS = { open_timeout: 2, timeout: 15 }.freeze

    def request_data
      @request_data ||= []
    end

    def fetch_vscs_target_config(vscs_target)
      GitHub::Config::VSCS_ENVIRONMENTS[vscs_target].tap do |config|
        raise ArgumentError, "invalid VSCS target" unless config
      end
    end

    def vscs_api_version(vscs_target, resource_provider) # resource_provider is a temporary arg until we move all plans to the new resource provider (Microsoft.Codespaces)
      fetch_vscs_target_config(vscs_target)[:api_version][resource_provider]
    end

    attr_reader :timeouts, :logger

    def initialize(timeouts: DEFAULT_TIMEOUTS, logger: GitHub::Logger)
      @timeouts = timeouts
      @logger = logger
    end

    protected

    # Internal: Fetches a token from KV if present or runs code to generate the token
    # and stores it in KV.
    #
    # key_segments - A list of identifying information for this token to generate the cache key.
    # type:        - The token type String used for the key and instrumentation.
    # block        - A block to run in order to generate the token. Should return a tuple of the
    #                token and the expiration time in seconds.
    #
    # Returns a token String.
    def cache_token(*key_segments, type:, force: false, &block)
      cache_key = "workspaces.token.#{type}.#{key_segments.join.to_md5}"

      if !force && cached_token = GitHub.kv.get(cache_key).value { nil }
        GitHub.dogstats.increment("codespaces.client.cache", tags: ["type:#{type}", "result:hit"])

        return cached_token
      else
        GitHub.dogstats.increment("codespaces.client.cache", tags: ["type:#{type}", "result:miss"])

        token, expires_in = yield

        # To avoid fetching tokens from the cache that are immediately about to expire, calculate
        # a buffer for the cache expiration. This is 10% of the life of the token or 5 minutes,
        # whichever is shorter.
        cache_expiry_buffer = [(expires_in * 0.10).floor, 5.minutes].min

        ActiveRecord::Base.connected_to(role: :writing) do
          GitHub.kv.set(cache_key, token, expires: (expires_in - cache_expiry_buffer).from_now)
        end

        token
      end
    end

    def log_response(method, request_url, resp)
      record_requests(method, request_url, resp, Time.now)
      logger.log_context(log_context) do
        log_data = {
          status: resp.status,
          method: method,
          url: request_url,
          vso_request_id: correlation_request_id(resp),
          request_data: serialize_request_data
        }
        log_data[:body] = resp.body if log_body_on_error? && !resp.success?
        logger.log(log_data)
      end
    end

    def log_body_on_error
      @log_body_on_error = true
      yield
    ensure
      @log_body_on_error = false
    end

    def log_body_on_error?
      @log_body_on_error
    end

    def log_context=(context)
      @log_context = context
    end

    def log_context
      @log_context ||= {}
    end

    def with_log_context(context, &block)
      original_context = log_context
      self.log_context = original_context.merge(context)

      yield
    ensure
      self.log_context = original_context
    end

    def request_err_message(message_title, method, url, resp, description = nil)
      request_id = resp.headers["vssaas-request-id"] if resp&.headers
      [
        "#{message_title} for #{method.to_s.upcase} #{url}",
        ("Status: #{resp&.status}" if resp),
        ("Request-ID: #{request_id}" if request_id),
        (description if description),
      ].compact.join(", ")
    end

    def record_requests(method, url, resp, timestamp)
     request_data.push({ method: method, url: url, status: resp&.status, request_id: correlation_request_id(resp), timestamp: timestamp})
    end

    def serialize_request_data
      request_data.sort_by { |req| req[:timestamp] }
    end

    private

    def connection_for(url, adapter: :excon, retries: nil)
      Faraday.new(url: url, request: timeouts) do |f|
        yield f if block_given?
        f.request(:retry, retry_options(retries)) if retries
        f.adapter adapter
      end
    end

    def retry_options(retries = 3)
      retry_proc = proc { |env, options, retries, exc|
        host = env.url.host
        exc_cls = exc.class
        GitHub.dogstats.increment(
          "codespaces.client.connection_retry",
          tags: ["host:#{host}", "exception:#{exc_cls}"],
        )
      }

      {
        max: retries,
        interval: 0.05,
        backoff_factor: 2,
        exceptions: [Faraday::ConnectionFailed, Faraday::TimeoutError],
        retry_block: retry_proc,
      }
    end
  end
end
