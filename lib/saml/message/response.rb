# rubocop:disable Style/FrozenStringLiteralComment

require "forwardable"
require "active_support/core_ext/module/delegation"
require "xmldsig"

module SAML
  class Message
    # This class represents a SAML Response. It currently wraps around
    # ruby-saml's Onelogin::SAML::Response class, but gives an interface
    # consistent with the rest of our SAML classes.
    #
    # [4.1.4.2 <Response> Usage](http://docs.oasis-open.org/security/saml/v2.0/saml-profiles-2.0-os.pdf)
    # [3.2 Requests and Responses](http://docs.oasis-open.org/security/saml/v2.0/saml-core-2.0-os.pdf)
    # [Schema](http://www.schemacentral.com/sc/saml2/e-samlp_Response.html)
    #
    class Response < Message
      include SAML::Shared::Issuer
      include SAML::Shared::Status
      include SAML::Shared::Response

      attr_accessor :name_id             # String principal of the assertion
      attr_accessor :name_id_format      # Format of NameID assertion
      attr_accessor :subject             # Subject element of IdP Response
      attr_accessor :session_expires_at  # UTC time of session expiration
      attr_accessor :session_index       # SessionIndex for single logout
      attr_accessor :status_message

      def self.parse(doc)
        d = doc.dup

        sig_nodes = d.xpath("//ds:Signature", namespaces)

        # remove Signature nodes to avoid pulling data from them
        # see: https://github.com/github/github/issues/67357#issuecomment-273664368
        # for details on how this can be exploited
        if sig_nodes && !sig_nodes.empty?
          sig_nodes.each do |sig_node|
            sig_node.remove
          end
        end

        destination = d.root.attr("Destination")

        # issuer can be either in the root response element or under the assertion itself.
        issuer = d.at_xpath("/saml2p:Response/saml2:Issuer", namespaces) && d.at_xpath("/saml2p:Response/saml2:Issuer", namespaces).text
        issuer ||= d.at_xpath("/saml2p:Response/saml2:Assertion/saml2:Issuer", namespaces) && d.at_xpath("/saml2p:Response/saml2:Assertion/saml2:Issuer", namespaces).text

        status_code = d.at_xpath("/saml2p:Response/saml2p:Status/saml2p:StatusCode", namespaces)
        status_code = status_code && status_code.attr("Value")

        # Second-level status codes are optional. These give more context in case of a failed AuthnRequest,
        # such as AuthnFailed or RequestDenied.
        #
        # See: https://docs.oasis-open.org/security/saml/v2.0/saml-core-2.0-os.pdf, sections 3.2.2.1 to 3.2.2.4
        #
        # Example of a second-level status code, nested in a top-level status code:
        #
        # <samlp:Status>
        #   <samlp:StatusCode Value="urn:oasis:names:tc:SAML:2.0:status:Responder">
        #     <samlp:StatusCode Value="urn:oasis:names:tc:SAML:2.0:status:RequestDenied"/>
        #   </samlp:StatusCode>
        # </samlp:Status>
        #
        second_level_status_code = d.at_xpath("/saml2p:Response/saml2p:Status/saml2p:StatusCode/saml2p:StatusCode", namespaces)
        second_level_status_code = second_level_status_code && second_level_status_code.attr("Value")

        status_message = d.at_xpath("/saml2p:Response/saml2p:Status/saml2p:StatusMessage", namespaces)
        status_message = status_message && status_message.text

        authn = d.at_xpath("/saml2p:Response/saml2:Assertion/saml2:AuthnStatement", namespaces)

        expiry = authn && authn["SessionNotOnOrAfter"]
        expiry = expiry && Time.parse(expiry + " UTC")

        session_index = authn && authn["SessionIndex"]

        # TODO: this belongs in Assertion
        conditions = d.at_xpath("/saml2p:Response/saml2:Assertion/saml2:Conditions", namespaces)
        not_before = conditions && conditions["NotBefore"]
        not_before = not_before && Time.parse(not_before + " UTC")
        not_on_or_after = conditions && conditions["NotOnOrAfter"]
        not_on_or_after = not_on_or_after && Time.parse(not_on_or_after + " UTC")
        audience_text = d.at_xpath("/saml2p:Response/saml2:Assertion/saml2:Conditions/saml2:AudienceRestriction", namespaces) && d.at_xpath("/saml2p:Response/saml2:Assertion/saml2:Conditions/saml2:AudienceRestriction/saml2:Audience", namespaces) && d.at_xpath("/saml2p:Response/saml2:Assertion/saml2:Conditions/saml2:AudienceRestriction/saml2:Audience", namespaces).text

        # TODO: this belongs in Assertion
        attribute_statements = d.at_xpath("/saml2p:Response/saml2:Assertion/saml2:AttributeStatement", namespaces)
        attributes = attribute_statements && attribute_statements.xpath("saml2:Attribute", namespaces).inject({}) do |attrs, attribute|
          # Name is required, FriendlyName is optional. We'll use either though.
          name = attribute["Name"]
          friendly_name = attribute["FriendlyName"]
          values = attribute.xpath("saml2:AttributeValue", namespaces).map do |attribute_value|
            attribute_value.text
          end
          attrs[name] = attrs[name.to_sym] = values
          if friendly_name
            attrs[friendly_name] = attrs[friendly_name.to_sym] = values
          end
          attrs
        end

        subject = d.at_xpath("/saml2p:Response/saml2:Assertion/saml2:Subject", namespaces) && d.at_xpath("/saml2p:Response/saml2:Assertion/saml2:Subject", namespaces).text
        name_id = d.at_xpath("/saml2p:Response/saml2:Assertion/saml2:Subject/saml2:NameID", namespaces) && d.at_xpath("/saml2p:Response/saml2:Assertion/saml2:Subject/saml2:NameID", namespaces).text
        name_id_format = d.at_xpath("/saml2p:Response/saml2:Assertion/saml2:Subject/saml2:NameID", namespaces) && d.at_xpath("/saml2p:Response/saml2:Assertion/saml2:Subject/saml2:NameID", namespaces)["Format"]

        subj_conf_data = d.at_xpath("/saml2p:Response/saml2:Assertion/saml2:Subject/saml2:SubjectConfirmation", namespaces) && d.at_xpath("/saml2p:Response/saml2:Assertion/saml2:Subject/saml2:SubjectConfirmation/saml2:SubjectConfirmationData", namespaces)
        recipient_attr = subj_conf_data && subj_conf_data["Recipient"]
        in_response_to_attr = subj_conf_data && subj_conf_data["InResponseTo"]

        # if we have both InResponseTo on the root element and on the SubjectConfirmationData element make sure they match.
        if in_response_to_attr && d.root.attr("InResponseTo") && d.root.attr("InResponseTo") != in_response_to_attr
          self.errors << "InResponseTo value on the Response element doesn't match the InResponseTo value in the SubjectConfirmationData element."
        end

        new({
          destination: destination,
          name_id: name_id,
          name_id_format: name_id_format || "urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified",
          subject: subject,
          issuer: issuer,
          in_response_to: in_response_to_attr || d.root.attr("InResponseTo"),
          recipient: recipient_attr,
          audience: audience_text,
          session_expires_at: expiry,
          session_index: session_index,
          status_code: status_code,
          second_level_status_code: second_level_status_code,
          status_message: status_message,
          not_before: not_before,
          not_on_or_after: not_on_or_after,
          attributes: attributes,
          document: doc,
        })
      end

      def build_document(include_sig_template: false)
        # This is to maintain compatibility with delegating parsing to ruby-saml
        # for the time being. When we decide to full implement .parse, we'll be
        # able to remove this.
        return @document if @document

        doc = Nokogiri::XML::Builder.new { |xml|
          root_attributes = {
            "xmlns:samlp"     => "urn:oasis:names:tc:SAML:2.0:protocol",
            "xmlns:saml"      => "urn:oasis:names:tc:SAML:2.0:assertion",
            "xmlns:ds"        =>  "http://www.w3.org/2000/09/xmldsig#",
            "ID"              => id,
            "IssueInstant"    => format_time(issue_instant),
            "Version"         => version,
          }
          root_attributes["Destination"]  = destination    if destination.present?
          root_attributes["InResponseTo"] = in_response_to if in_response_to.present?

          xml.Response(root_attributes) do
            # hack b/c nokogiri 1.5.6 doesn't evaluate ns definitions first
            xml.parent.namespace = xml.parent.namespace_definitions.first

            generate_issuer(xml)

            xml_signature_template(xml, id) if include_sig_template

            generate_status(xml)

            # Assertions match up with ruby-saml response functionality for now.
            if @attributes # ivar means they were set instead of read from docment
              assertion = Assertion.new({
                attributes: @attributes,
                issuer: issuer,
                name_id: name_id,
                name_id_format: name_id_format,
                recipient: recipient,
                audience: audience,
              })
              assertion.decorate(xml)
            end
          end
        }.doc

        @document = doc
        @document
      end

      # inserts a signature, usually after issuer, that looks like:
      # <ds:Signature>
      # <ds:SignedInfo>
      #   <ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/>
      #   <ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"/>
      #   <ds:Reference URI="#foo">
      #     <ds:Transforms>
      #       <ds:Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"/>
      #       <ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#">
      #       </ds:Transform>
      #     </ds:Transforms>
      #     <ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/>
      #     <ds:DigestValue></ds:DigestValue>
      #   </ds:Reference>
      # </ds:SignedInfo>
      # <ds:SignatureValue></ds:SignatureValue>
      # </ds:Signature>
      def xml_signature_template(builder, uri)
        builder["ds"].Signature do |builder|
          builder.SignedInfo do |builder|
            builder.CanonicalizationMethod(Algorithm: "http://www.w3.org/2001/10/xml-exc-c14n#")
            builder.SignatureMethod(Algorithm: "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256")
            builder.Reference(URI: "#" + String(uri)) do |builder|
              builder.Transforms do |builder|
                builder.Transform(Algorithm: "http://www.w3.org/2000/09/xmldsig#enveloped-signature")
                builder.Transform(Algorithm: "http://www.w3.org/2001/10/xml-exc-c14n#")
              end
              builder.DigestMethod(Algorithm: "http://www.w3.org/2001/04/xmlenc#sha256")
              builder.DigestValue
            end
          end
          builder.SignatureValue
        end
      end

      def validate(options)
        if !SAML.mocked[:skip_validate_signature]
          validate_has_signature
          validate_signatures(options[:idp_certificate])
        end
        validate_has_assertion unless SAML.mocked[:skip_validate_has_assertion]
        validate_issuer(options[:issuer])
        validate_destination(options[:sp_url])
        validate_recipient(options[:sp_url])
        validate_conditions
        validate_audience(options[:sp_url])
        validate_name_id_format(options[:name_id_format])
      end

      # Internal: Validate the issuer of the message if present in configuration.
      # We don't require one to be configured since the admin may not know the
      # appropriate value to use; nil and "" allow any issuer.
      def validate_issuer(expected)
        return if SAML.mocked[:skip_issuer] || !expected || expected.empty?
        if String(issuer) != expected
          self.errors << "Issuer is invalid."
        end
      end

      # Internal: Validate the audience of the message. Should always be
      # the dotcom or enterprise url (our EntityID)
      def validate_audience(sp_url)
        error = audience_validation(sp_url)
        if error
          self.errors << error
        end
      end

      # internal: for debugging purposes, this dumps the response's XML in a way
      # that we can securely log it
      def dump_xml_without_signature
        doc_copy = self.document.dup
        if sig = doc_copy.xpath("//ds:Signature", namespaces)
          sig.remove
        end
        doc_copy.to_xml(indent: 2)
      end

      # Error will be reported if audience is missing or distinct from sp_url
      # This is the new compliant logic to validate audience
      def audience_validation(sp_url)
        return if SAML.mocked[:skip_audience]
        if !audience || audience.downcase != sp_url&.downcase
          return "Audience is invalid. Audience attribute does not match #{sp_url}"
        end
      end

      # Internal: Validate the format of the NameId field
      def validate_name_id_format(specified_format)
        return unless specified_format
        return if SAML.mocked[:skip_name_id_format]

        if name_id_format && name_id_format != specified_format
          self.errors << "NameID format must be '#{ specified_format }'."
          return
        end
      end

      # Internal: Validate the recipient of the message. Should always be
      # the doctom or enterprise url followed by /saml/consume or /saml/validate.
      #
      # If no Subject element is present, no assertion is being made, no
      # SubjectConfirmationData element will be present, and therefore
      # no recipient. In these cases we do not require a recipient.
      #
      # For instance, this is the case when a 'RequestDenied' second-level
      # status code is sent. This sometimes indicates that the IdP has not
      # authorized the user for the SP (GitHub).
      #
      # See: https://github.com/github/github/pull/56870
      #
      def validate_recipient(sp_url)
        return if !subject
        return if SAML.mocked[:skip_destination]
        if !recipient
          self.errors << "Recipient in the SAML response must not be blank."
          return
        end
        if recipient.downcase != "#{sp_url&.downcase}/saml/consume" && recipient != "#{sp_url&.downcase}/saml/validate"
          self.errors << "Recipient in the SAML response was not valid."
          return
        end
      end

      # Internal: Validate the destination of the message.
      # MUST be present if SAML message itself is signed
      # If present MUST have
      # `_host_/saml/consume` as its value
      #
      # https://docs.oasis-open.org/security/saml/v2.0/saml-bindings-2.0-os.pdf
      #
      # 3.5.5.2
      #
      # If the message is signed, the Destination XML attribute in the root SAML
      # element of the protocol message MUST contain the URL to which the sender
      # has instructed the user agent to deliver the message. The recipient MUST
      # then verify that the value matches the location at which the message has
      # been received.
      #
      # From the above one infers destination is only required when the message
      # (the root document) is signed. If instead only the assertion was signed, it
      # wouldn't be necessary as per spec.
      #
      def validate_destination(sp_url)
        return if SAML.mocked[:skip_destination]
        # destination is only required when the message is signed, not the assertion
        return unless has_root_sig_and_matching_ref?
        unless destination && destination.downcase == "#{sp_url&.downcase}/saml/consume"
          self.errors << "Destination in the SAML response was not valid."
          return
        end
      end

      # Internal: Validate that the SAML message (root XML element of SAML response)
      # or all contained assertions are signed
      #
      # Verification of signatures is done in #validate_signatures
      def validate_has_signature
        # Return early if entire response is signed. This prevents individual
        # assertions from being tampered because any change in the response
        # would invalidate the entire response.
        return if has_root_sig_and_matching_ref?
        return if all_assertions_signed_with_matching_ref?

        self.errors << "SAML Response is not signed or has been modified."
      end

      # Internal: Validate all XML signatures against `certificate`. Returns
      # boolean.
      def validate_signatures(raw_cert)
        unless raw_cert
          self.errors << "No Certificate"
          return
        end
        certificate = OpenSSL::X509::Certificate.new(raw_cert)
        unless signatures.all? { |signature| signature.valid?(certificate) }
          self.errors << "Digest mismatch"
        end
      rescue Xmldsig::SchemaError => e
        self.errors << "Invalid signature"
      rescue OpenSSL::X509::CertificateError => e
        self.errors << "Certificate error: '#{e.message}'"
      end

      def validate_has_assertion
        return if !document.at("/saml2p:Response/saml2:Assertion", namespaces).nil?
        self.errors << "No assertion found"
      end

      def has_root_sig_and_matching_ref?
        return true if SAML.mocked[:mock_root_sig]
        root_ref = document.at("/saml2p:Response/ds:Signature/ds:SignedInfo/ds:Reference", namespaces)
        return false unless root_ref
        root_ref_uri = String(String(root_ref["URI"])[1..-1]) # chop off leading #
        return false unless root_ref_uri.length > 1
        root_rep = document.at("/saml2p:Response", namespaces)
        root_id = String(root_rep["ID"])

        # and finally does the root ref URI match the root ID?
        root_ref_uri == root_id
      end

      def all_assertions_signed_with_matching_ref?
        assertions = document.xpath("//saml2:Assertion", namespaces)
        return assertions.all? do |assertion|
          ref = assertion.at("./ds:Signature/ds:SignedInfo/ds:Reference", namespaces)
          return false unless ref
          assertion_id = String(assertion["ID"])
          ref_uri = String(String(ref["URI"])[1..-1]) # chop off leading #
          return false unless ref_uri.length > 1

          ref_uri == assertion_id
        end
      end

      def self.namespaces
        {
          "ds" => "http://www.w3.org/2000/09/xmldsig#",
          "saml2p" => "urn:oasis:names:tc:SAML:2.0:protocol",
          "saml2" => "urn:oasis:names:tc:SAML:2.0:assertion",
        }
      end

      def namespaces
        self.class.namespaces
      end

      # These interfaces are from ruby-saml. We want to move away from this
      # because it assumes only a single assertion per response.
      module RubySamlCompatibility
        extend ActiveSupport::Concern

        included do
          attr_accessor :not_before       # UTC time
          attr_accessor :not_on_or_after  # UTC time
        end

        # {
        #   "urn:oid:0.9.2342.19200300.100.1.1" => ["jch"],
        #   "uid" => ["jch"],
        #   :"urn:oid:0.9.2342.19200300.100.1.1" => ["jch"],
        #   :uid => ["jch"]
        # }
        # Public: Set released assertion attributes. `attrs` is a hash keyed
        # attribute name.
        #
        # response.attributes = {"uid" => ["jch"]}
        def attributes=(attrs)
          @attributes = attrs
        end

        # Public: Returns hash of attributes about the principal.
        #
        def attributes
          @attributes || {}
        end

        private

        # Private: Validate assertion conditions. This belongs in
        # SAML::Message::Assertion, but leaving here to follow ruby-saml
        # interface.
        def validate_conditions
          return if SAML.mocked[:skip_conditions]

          now = Time.now

          if not_before && (now.to_i < not_before.to_i)
            self.errors << "Current time is earlier than NotBefore condition"
          end

          if not_on_or_after && (now >= not_on_or_after)
            self.errors << "Current time is on or after NotOnOrAfter condition"
          end
        end
      end
      include RubySamlCompatibility

    end
  end
end
