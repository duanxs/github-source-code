# rubocop:disable Style/FrozenStringLiteralComment

module Hookshot
  class HookshotError < StandardError
    def failbot_context
      {
        "app" => "github-event-dispatch",
      }
    end
  end
end
