# rubocop:disable Style/FrozenStringLiteralComment

module Hookshot
  module DeliverJobLogger

    def log_payload_too_large_error(error:, target: nil, parent: nil, event: nil, guid: nil, hook_ids: nil)
      unless error.is_a?(PayloadTooLarge)
        raise TypeError, "error is a #{error.class.name}, but expected Hookshot::PayloadTooLarge"
      end

      base_info = {
        result: "payload_too_large",
        now: Time.now.iso8601,
        enterprise: GitHub.enterprise?,
        job: self.to_s,
        class: error.class.to_s,
        payload_size: error.size,
      }

      target_info = case target
      when Repository
        {
          target_repository_id: target.id,
          target_repository_nwo: target.nwo,
        }
      when Organization
        {
          target_organization_id: target.id,
          target_organization_name: target.name,
        }
      else
        {}
      end

      arg_info = {parent: parent, guid: guid, event: event, hook_ids: hook_ids}

      payload_info = error.payload.slice(:parent, :guid, :event)

      if hooks = error.payload[:hooks]
        payload_info[:hook_ids] = error.payload[:hooks].map { |hook| hook[:id] }.join(",")
      end

      payload_info = arg_info.inject({}) do |hash, item|
        key, value = item
        hash.merge! key => (value || payload_info[key])
      end

      info = {}
      arg_info.each { |k, v| info[k] = (v || payload_info[k]) }

      log_info = base_info.merge(target_info).merge(info)
      GitHub::Logger.log(log_info)
    end
  end
end
