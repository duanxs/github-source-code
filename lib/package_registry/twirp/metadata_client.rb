# frozen_string_literal: true

module PackageRegistry
  module Twirp
    class MetadataClient < PackageRegistry::Twirp::BaseClient
      def get_packages_metadata(user_id:, package_ids:)
        resp = rpc(:GetPackagesMetadata, user_id: user_id, package_ids: package_ids)
        resp.packages_metadata.map { |md| PackageRegistry::PackageMetadata.new(md) }
      end

      # get_package_metadata returns package metadata along with it's versions
      # 0 version_limit means ALL versions will be returned
      def get_package_metadata(ecosystem:, namespace:, name:, user_id:, version_offset: 0, version_limit: 0)
        e = ecosystem_value(ecosystem)
        resp = rpc(:GetPackageMetadata, ecosystem: e, namespace: namespace, name: name, user_id: user_id, version_offset: version_offset, version_limit: version_limit)
        PackageRegistry::PackageMetadata.new(resp.package_metadata) unless resp.package_metadata.nil?
      end

      def get_package_version(ecosystem:, user_id:, namespace:, name:, version_id:)
        e = ecosystem_value(ecosystem)
        resp = rpc(:GetPackageVersion, ecosystem: e, user_id: user_id, namespace: namespace, name: name, version_id: version_id)
        PackageRegistry::PackageVersion.new(resp.version) unless resp.version.nil?
      end

      def get_package_version_download_counts(package_id:, version_id:)
        resp = rpc(:GetPackageVersionDownloadCounts, package_id: package_id, version_id: version_id)
        resp ? PackageRegistry::DownloadCounts.new(resp) : PackageRegistry::DownloadCounts::NULL
      end

      def get_package_total_download_counts(package_id:)
        resp = rpc(:GetPackageTotalDownloadCounts, package_id: package_id)
        resp ? resp.total : 0
      end

      def update_package(ecosystem:, namespace:, name:, is_public:, user_id:)
        e = ecosystem_value(ecosystem)
        rpc(:UpdatePackage, ecosystem: e, namespace: namespace, name: name, is_public: is_public, user_id: user_id)
      end

      def delete_package(ecosystem:, namespace:, name:, user_id:, mode:)
        m = delete_mode_value(mode)
        e = ecosystem_value(ecosystem)
        rpc(:DeletePackage, ecosystem: e, namespace: namespace, name: name, user_id: user_id, mode: m)
      end

      def delete_package_version(ecosystem:, namespace:, name:, user_id:, mode:, version:)
        e = ecosystem_value(ecosystem)
        m = delete_mode_value(mode)
        rpc(:DeletePackageVersion, ecosystem: e, namespace: namespace, name: name, user_id: user_id, mode: m, version: version)
      end

      # get_all_packages is for use by internal jobs/etc as it does NOT perform and Authz checks on the RMS server side
      def get_all_packages(namespace:, limit: 100, offset: 0, exclude_deleted: false)
        resp = rpc(:GetAllPackages, namespace: namespace, limit: limit, offset: offset, exclude_deleted: exclude_deleted)
        resp.packages.map { |p| PackageRegistry::Package.new(p) }
      end

      private

      def twirp_class
        Proto::RegistryMetadata::V1::Package::MetadataClient
      end

      def ecosystem_value(ecosystem)
        case ecosystem.downcase.to_sym
        when :container
          return Proto::RegistryMetadata::V1::Package::Ecosystem::CONTAINER
        else
          return Proto::RegistryMetadata::V1::Package::Ecosystem::UNKNOWN
        end
      end


      def delete_mode_value(mode)
        case mode.downcase
        when :permanent
          return ::Proto::RegistryMetadata::V1::Package::DeleteMode::PERMANENT
        else
          return ::Proto::RegistryMetadata::V1::Package::DeleteMode::SOFT
        end
      end
    end
  end
end
