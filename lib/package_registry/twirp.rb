# frozen_string_literal: true

module PackageRegistry
  module Twirp
    class BaseError < StandardError; attr_accessor :msg; end
    class Error < BaseError; end
    class ServiceUnavailableError < BaseError; end
    class PermissionDeniedError < BaseError; end

    def self.metadata_client
      @metadata_client ||= MetadataClient.new
    end
  end
end
