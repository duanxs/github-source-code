# frozen_string_literal: true

module PackageRegistry
  class Package < SimpleDelegator
    include Permissions::Attributes::Wrapper

    self.permissions_wrapper_class = Permissions::Attributes::PackageRegistryPackage

    REPOSITORY_URL_REGEX = "(?<=github.com\/)(?<name_with_owner>[^/]+\/[^/]+)"
    NON_GITHUB_REPOSITORY_NAME_REGEX = /(\.\w+)\/(?<name_with_owner>[^\/]+\/[^\/]+)/

    attr_reader :latest_version, :package

    def initialize(package)
      @package = package
      super
    end

    def author
      User.find(package.author_id)
    end

    def dockerfile_url
      latest_version.metadata&.labels&.source
    end

    def ecosystem
      @package.ecosystem.downcase.to_sym
    end
    alias_method :package_type, :ecosystem

    def latest_version=(version)
      @latest_version = version
    end

    def owner
      User.find_by(login: @package.namespace)
    end

    def public?
      package.is_public
    end

    def repository
      return unless repository_url

      repository_url.downcase.match(REPOSITORY_URL_REGEX) do |m|
        Repository.with_name_with_owner(m[:name_with_owner])
      end
    end

    # Public: Get display name of the referenced repository. If a github repo,
    # this returns repo name with owner. If a non-github repo, attempt to parse
    # the url for a repo name.
    def repository_name_with_owner
      if repository
        repository.name
      elsif repository_url
        repository_url.downcase.match(NON_GITHUB_REPOSITORY_NAME_REGEX) { |m| m[:name_with_owner] }
      end
    end

    def repository_url
      latest_version&.metadata&.labels&.source
    end

    def user_role_target_type
      "Package"
    end

    def visibility
      @package.is_public ? "public" : "private"
    end

    def created_at
      epoch_micros = @package.created_at.nanos / 10 ** 6
      Time.at(@package.created_at.seconds, epoch_micros)
    end

    def updated_at
      epoch_micros = @package.updated_at.nanos / 10 ** 6
      Time.at(@package.updated_at.seconds, epoch_micros)
    end
  end
end
