# frozen_string_literal: true

require "graphql/client/view_module"

module Views
  extend GraphQL::Client::ViewModule

  self.path = File.join(Rails.root, "app/views").freeze
  self.client = PlatformHelper::PlatformClient
end
