# frozen_string_literal: true

module Primer
  module ProgressBar
    class Component < Primer::Component
      include ClassNameHelper
      include ViewComponent::Slotable

      with_slot :item, collection: true, class_name: "Item"

      SIZE_DEFAULT = :default

      SIZE_MAPPINGS = {
        SIZE_DEFAULT => "",
        :small => "Progress--small",
        :large => "Progress--large",
      }.freeze

      SIZE_OPTIONS = SIZE_MAPPINGS.keys

      def initialize(size: SIZE_DEFAULT, percentage: 0, **kwargs)
        @kwargs = kwargs
        @kwargs[:classes] = class_names(
          @kwargs[:classes],
          "Progress",
          SIZE_MAPPINGS[fetch_or_fallback(SIZE_OPTIONS, size, SIZE_DEFAULT)]
        )
        @kwargs[:tag] = :span

      end

      def render?
        items.any?
      end

      class Item < ViewComponent::Slot
        include ClassNameHelper
        attr_reader :kwargs

        def initialize(percentage: 0, bg: :green, **kwargs)
          @percentage = percentage
          @kwargs = kwargs

          @kwargs[:tag] = :span
          @kwargs[:bg] = bg
          @kwargs[:style] = "width: #{@percentage}%;"
          @kwargs[:classes] = class_names("Progress-item", @kwargs[:classes])
        end
      end
    end
  end
end
