# frozen_string_literal: true
module Primer
  class FlashComponent < Primer::Component
    DEFAULT_VARIANT = :default
    VARIANT_MAPPINGS = {
      DEFAULT_VARIANT => "",
      :warning => "flash-warn",
      :danger => "flash-error",
      :success => "flash-success"
    }.freeze

    def initialize(full: false, dismissible: false, icon: nil, variant: DEFAULT_VARIANT, **kwargs)
      @icon = icon
      @dismissible = dismissible
      @kwargs = kwargs
      @kwargs[:tag] = :div
      @kwargs[:classes] = class_names(
        @kwargs[:classes],
        "flash",
        VARIANT_MAPPINGS[fetch_or_fallback(VARIANT_MAPPINGS.keys, variant, DEFAULT_VARIANT)],
        "flash-full": full
      )
    end
  end
end
