# frozen_string_literal: true

# A utility class for measuring durations.
class Timer
  attr_reader :started_at

  # Public: Create and start a timer.
  #
  # Returns a Timer.
  def self.start
    new.tap(&:start)
  end

  # Public: Start the timer. Can be called repeatedly to reset the timer.
  #
  # Returns nothing.
  def start
    @started_at = Time.now
    @start_mono = now
    @start_cputime = cputime
  end

  # Public: Stop the timer.
  #
  # Returns nothing.
  def stop
    @finish_mono ||= now
    @finish_cputime ||= cputime
  end

  # Public: Measure the elapsed milliseconds since the timer started. Implicitly
  # stops the timer.
  #
  # Returns an Integer.
  def elapsed_ms
    stop

    ((@finish_mono - @start_mono) * 1000).round
  end

  # Public: Measure the elapsed milliseconds spent on CPU since the timer
  # started. Implicitly stops the timer.
  #
  # Returns an Integer.
  def elapsed_cpu_ms
    stop

    ((@finish_cputime - @start_cputime) * 1000).ceil
  end

  # Public: Measure the elapsed milliseconds *not* spent on CPU since the timer
  # started. Implicitly stops the timer.
  #
  # Returns an Integer.
  def elapsed_idle_ms
    stop

    [elapsed_ms - elapsed_cpu_ms, 0].max
  end

  private

  def cputime
    Process.clock_gettime(Process::CLOCK_PROCESS_CPUTIME_ID)
  end

  def now
    Process.clock_gettime(Process::CLOCK_MONOTONIC)
  end
end
