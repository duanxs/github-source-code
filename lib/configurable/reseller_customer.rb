# frozen_string_literal: true

# Whether a customer (organization or enterprise account) purchases GitHub products through a reseller
module Configurable
  module ResellerCustomer
    KEY = "reseller_customer".freeze

    def enable_reseller_customer(actor:)
      config.enable(KEY, actor)
    end

    def disable_reseller_customer(actor:)
      config.disable(KEY, actor)
    end

    def reseller_customer?
      return false if new_record?
      config.enabled?(KEY)
    end

    # Allow reseller_customer to be set like other active record attributes.
    # Reseller customer will only be able to be set by staff so try to get
    # current_user else default to backup_actor.
    def reseller_customer=(value)
      backup_actor = GitHub.enterprise? ? User.ghost : User.staff_user
      actor = User.find_by(id: GitHub.context[:actor_id]) || backup_actor
      entry = configuration_entries.find_by(name: KEY) || configuration_entries.new(name: KEY)
      cast_value = ActiveModel::Type::Boolean.new.cast(value) ? Configuration::TRUE : Configuration::FALSE
      entry.final   = false
      entry.value   = cast_value
      entry.updater = actor

      if persisted?
        entry.save!
        config.reset
      end

      cast_value
    end
  end
end
