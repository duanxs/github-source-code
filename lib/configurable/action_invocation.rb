# frozen_string_literal: true

module Configurable
  module ActionInvocation
    include Instrumentation::Model

    KEY = "action_invocation_blocked"

    # Public: Determine whether action invocation is blocked for the entity.
    #
    # Returns nothing.
    def action_invocation_blocked?
      config.enabled?(KEY)
    end

    # Public: Block action invocation for the entity.
    #
    # blocking_user - The user enabling the invocation block
    #
    # Returns nothing.
    def block_action_invocation(blocking_user)
      config.enable(KEY, blocking_user)
      instrument :block, prefix: :action_invocation, staff_actor: blocking_user
    end

    # Public: Unblock action invocation for the entity.
    #
    # unblocking_user - The user disabling the invocation block
    #
    # Returns nothing.
    def unblock_action_invocation(unblocking_user)
      config.disable(KEY, unblocking_user)
      instrument :unblock, prefix: :action_invocation, staff_actor: unblocking_user
    end
  end
end
