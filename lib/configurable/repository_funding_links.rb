# frozen_string_literal: true

# Configures whether to use a repo's `.github/FUNDING.md` file to display a
# Sponsor button on the repo page.
module Configurable
  module RepositoryFundingLinks
    KEY = "repository_funding_links".freeze

    # Enable repository funding links for the repository or organization
    def enable_repository_funding_links(actor:)
      return if repository_funding_links_explicitly_enabled?

      config.enable(KEY, actor)

      GlobalInstrumenter.instrument("sponsors.repo_funding_links_button_toggle", {
        repository: self,
        actor: actor,
        owner: owner,
        toggle_state: "ENABLED",
      })
    end

    # Disable repository funding links for the repository or organization
    def disable_repository_funding_links(actor:)
      return if repository_funding_links_explicitly_disabled?

      config.disable(KEY, actor)

      GlobalInstrumenter.instrument("sponsors.repo_funding_links_button_toggle", {
        repository: self,
        actor: actor,
        owner: owner,
        toggle_state: "DISABLED",
      })
    end

    # Determine whether repository funding links are explicitly enabled
    def repository_funding_links_explicitly_enabled?
      config.enabled?(KEY)
    end

    # Determine whether repository funding links are explicitly disabled
    def repository_funding_links_explicitly_disabled?
      config.get(KEY) == false
    end

    # Determine if repository funding links have neither been explicitly set
    # or unset
    def repository_funding_links_unset?
      config.get(KEY).nil?
    end
  end
end
