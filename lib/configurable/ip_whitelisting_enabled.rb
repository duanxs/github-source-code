# frozen_string_literal: true

# Manage whether IP whitelisting enabled is enabled on an object.
# Currently included by Organization and Business.
module Configurable
  module IpWhitelistingEnabled
    KEY = "ip_whitelisting_enabled".freeze

    # Raised when enabling the setting would lock the actor out of the
    # owning account.
    class ActorLockoutError < StandardError; end

    # Public: Enable IP whitelisting on the object.
    #
    # actor - The User enabling the setting.
    # actor_ip - A String representing the IP address of the user enabling the setting.
    #
    # Returns nothing.
    def enable_ip_whitelisting(actor: nil, actor_ip: nil)
      if enabling_will_lock_out_actor?(actor_ip)
        raise ActorLockoutError.new \
          "Enabling an IP allow list would prevent you from accessing the account from your current IP address."
      end

      return unless config.enable!(KEY, actor)

      instrument_enable(actor)
    end

    # Public: Disable IP whitelisting on the object.
    #
    # actor - The User disabling the setting.
    # reason - An optional String representing the reason for disabling.
    #
    # Returns nothing.
    def disable_ip_whitelisting(actor: nil, reason: nil)
      return unless config.delete(KEY, actor)

      instrument_disable(actor, reason)
    end

    # Public: Is IP whitelisting enabled on the object?
    #
    # Returns Boolean.
    def ip_whitelisting_enabled?
      GitHub.ip_whitelisting_available? && config.enabled?(KEY)
    end

    # Public: Is IP whitelisting enabled at on a higher level object?
    #
    # For example, currently returns true when called on an org that is a member
    # of a business that has IP whitelisting enabled.
    #
    # Returns Boolean.
    def ip_whitelisting_enabled_policy?
      ip_whitelisting_enabled? && config.inherited?(KEY)
    end

    private

    def enabling_will_lock_out_actor?(actor_ip)
      return false unless GitHub.ip_whitelisting_available?
      return false if actor_ip.blank?

      active_entries = IpWhitelistEntry.usable_for(self).active.to_a
      return false if IpWhitelistEntry.ip_included_in_entries? \
        ip: actor_ip, entries: active_entries

      true
    end

    def instrument_enable(actor)
      GitHub.instrument("ip_allow_list.enable", instrumentation_payload(actor))

      GlobalInstrumenter.instrument("ip_allow_list.enable", {
        owner: self,
        entries: self.ip_whitelist_entries,
        actor: actor,
      })
    end

    def instrument_disable(actor, reason)
      payload = instrumentation_payload(actor)
      payload[:reason] = reason unless reason.blank?
      GitHub.instrument("ip_allow_list.disable", payload)

      GlobalInstrumenter.instrument("ip_allow_list.disable", {
        owner: self,
        entries: self.ip_whitelist_entries,
        actor: actor,
      })
    end

    def instrumentation_payload(actor)
      payload = { user: actor }

      case self
      when ::Organization
        payload[:org] = self
        payload[:business] = self.business if self.business
      when ::Business
        payload[:business] = self
      end

      payload
    end
  end
end
