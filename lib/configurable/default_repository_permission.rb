# frozen_string_literal: true

module Configurable
  module DefaultRepositoryPermission
    KEY = "default_repository_permission".freeze

    VALUES = (Ability.valid_actions + [:none]).freeze

    # Thrown when an attempt to update the default repository permission is made
    # while an update is already in progress.
    class AlreadyUpdating < StandardError; end

    # Public: Updates the default repo permissions for an org
    #
    # permission - The permission level to update to. Can be any valid
    #              ability key (:read, etc), or :none.  Also accepts strings.
    # actor - The User performing this action
    #
    # Note: If you want to use this in tests, wrap it in an perform_enqueued_job { } block
    def update_default_repository_permission(permission, force: false, actor:)
      raise ArgumentError unless valid_default_repository_permission?(permission)
      raise AlreadyUpdating if updating_default_repository_permission?

      old_permission = default_repository_permission_name
      permission = permission&.to_sym

      # the default value is :read, remove the configuration entry
      changed = if permission == :read && !force
        config.delete(KEY, actor)
      else
        config.set!(KEY, permission, actor, force)
      end

      return unless changed
      instrument :update_default_repository_permission,
        actor:          actor,
        old_permission: old_permission,
        permission:     default_repository_permission_name
      sync_default_repository_permission(actor: actor)
    end

    # Public: Clear the default repository permission setting for this model.
    def clear_default_repository_permission(actor:)
      old_permission = default_repository_permission_name
      changed = config.delete(KEY, actor)

      return unless changed
      instrument :clear_default_repository_permission, actor: actor
      sync_default_repository_permission(actor: actor)
    end

    # Public: The default repository permission value
    # Returns an ability action symbol, or :none
    def default_repository_permission
      default_repository_permission_name.to_sym
    end

    # Public: A human-readable version of the organization's default repository
    # permission attribute.
    #
    # Returns a string "read", "write", "admin", "none".
    def default_repository_permission_name
      config.get(KEY) || "read"
    end

    # Public: Returns whether a default repository permission value is valid
    def valid_default_repository_permission?(permission)
      VALUES.include?(permission&.to_sym)
    end

    # Public: Returns whether a default repository permission policy is set
    def default_repository_permission_policy?
      !!config.final?(KEY)
    end

    # Internal: Queue a job to update default permissions for affected repositories
    def sync_default_repository_permission(actor:)
      return if updating_default_repository_permission?

      SyncOrganizationDefaultRepositoryPermissionJob.enqueue(self, actor: actor)
    end

    # Internal: Sync default repository permission changes to child repositories
    def sync_default_repository_permission!(actor:)
      organizations = if self.is_a?(Organization)
        [self]
      elsif self.is_a?(Business)
        self.organizations
      end

      organizations.each do |org|
        org.org_repositories.each do |repo|
          Ability.throttle do
            if org.default_repository_permission != :none
              # Create/update org -> repo abilities
              repo.add_organization(org, action: org.default_repository_permission)
            else
              # Revoke all org -> repo abilities
              repo.remove_organization(org)
            end
          end
        end

        if org.default_repository_permission == :none
          RemoveForksForInaccessibleRepositoriesJob.perform_later(org.org_repositories.map(&:id), org.member_ids)
        end
      end
    end

    def updating_default_repository_permission?
      return false unless sync_default_repository_permission_job_status.present?
      !sync_default_repository_permission_job_status.finished?
    end

    private

    def sync_default_repository_permission_job_status
      @sync_default_repository_permission_job_status ||= \
        GitHub::Jobs::SyncOrganizationDefaultRepositoryPermission.status(self)
    end
  end
end
