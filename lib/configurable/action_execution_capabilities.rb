# frozen_string_literal: true

module Configurable
  module ActionExecutionCapabilities
    Error = Class.new(StandardError)

    KEY = "action_execution_capabilities".freeze

    ALL_ACTIONS   = "all".freeze            # All actions are allowed.
    DISABLED      = "disabled".freeze       # No actions are allowed.
    LOCAL_ACTIONS = "local_actions".freeze  # Only in-repo actions are allowed.

    DEFAULT_POLICY = ALL_ACTIONS

    NO_POLICY         = "no_policy".freeze          # Not persisted. Implies ALL_ACTIONS.

    ALL_ACTIONS_FOR_SELECTED = "all_actions_for_selected".freeze
    LOCAL_ONLY_FOR_SELECTED = "local_only_for_selected".freeze

    OPTIONS_FOR_ALL_ENTITIES = [ALL_ACTIONS, LOCAL_ACTIONS].freeze
    OPTIONS_FOR_SELECTED_ENTITIES = [ALL_ACTIONS_FOR_SELECTED, LOCAL_ONLY_FOR_SELECTED].freeze
    OPTIONS_ALLOWED_BY_LOCAL = [LOCAL_ACTIONS, LOCAL_ONLY_FOR_SELECTED, DISABLED].freeze

    ALL_OPTIONS = OPTIONS_FOR_ALL_ENTITIES  + OPTIONS_FOR_SELECTED_ENTITIES + [DISABLED].freeze

    def update_action_execution_capabilities(capability, force: false, actor:)
      raise Error.new("Unknown option '#{capability}'") unless valid_action_execution_capabilities?(capability, actor: actor)
      raise Error.new("'#{capability}' is not allowed.") unless available_action_execution_options(user: actor).include?(capability)

      old_capability = action_execution_capabilities

      # When the policy isn't forced, we'll delete the configuration entry
      # if the new value is the same as the default value (currently
      # "ALL_ACTIONS").
      changed = if capability == DEFAULT_POLICY && !force
        config.delete(KEY, actor)
      else
        config.set!(KEY, capability, actor, force)
      end

      return unless changed
      instrument "update_actions_settings",
        actor:          actor,
        old_capability: old_capability,
        new_capability: action_execution_capabilities
    end

    # Public: Clear the action execution capabilities setting for this model.
    def clear_action_execution_capabilities(actor:)
      changed = config.delete(KEY, actor)

      return unless changed
      instrument "clear_actions_settings", actor: actor
    end

    def action_execution_capabilities
      if self.is_a?(Repository) && GitHub.flipper[:actions_enable_full_capabilities_for_repo].enabled?(self)
        return ALL_ACTIONS
      end

      # if config is nil or inherited from a parent, use default policy
      current = config.local?(KEY) ? config.get(KEY) : DEFAULT_POLICY

      if (configuration_owner&.respond_to?(:action_execution_capabilities))
        return most_restrictive_capability(entity_policy: current)
      end

      current
    end

    def action_execution_capabilities_policy?
      !!config.get(KEY)
    end

    def forced_actions_permission?
      !!config.final?(KEY)
    end

    def disable_all_action_executions(force: false, actor:)
      update_action_execution_capabilities(DISABLED, force: force, actor: actor)
    end

    def enable_all_action_executions(force: false, actor:)
      update_action_execution_capabilities(ALL_ACTIONS, force: force, actor: actor)
    end

    def enable_only_local_action_executions(force: false, actor:)
      update_action_execution_capabilities(LOCAL_ACTIONS, force: force, actor: actor)
    end

    def enable_local_actions_for_selected_entities(actor:)
      update_action_execution_capabilities(LOCAL_ONLY_FOR_SELECTED, actor: actor)
    end

    def enable_all_actions_for_selected_entities(actor:)
      update_action_execution_capabilities(ALL_ACTIONS_FOR_SELECTED, actor: actor)
    end

    def valid_action_execution_capabilities?(capability, actor:)
      ALL_OPTIONS.include? capability
    end

    def all_action_executions_enabled?
      ::Actions::PolicyChecker.all_action_types_allowed? action_execution_capabilities
    end

    def only_local_action_executions_enabled?
      ::Actions::PolicyChecker.local_actions_only? action_execution_capabilities
    end

    def all_action_executions_disabled?
      ::Actions::PolicyChecker.disabled? action_execution_capabilities
    end

    def available_action_execution_options(user: nil)
      return ALL_OPTIONS unless configuration_owner&.respond_to?(:action_execution_capabilities)
      return [owner_policy] if configuration_owner.forced_actions_permission?
      return [DISABLED] if entity_disallowed_by_owner?

      case owner_policy
      when ALL_ACTIONS, ALL_ACTIONS_FOR_SELECTED
        ALL_OPTIONS
      when LOCAL_ACTIONS, LOCAL_ONLY_FOR_SELECTED
        OPTIONS_ALLOWED_BY_LOCAL
      else
        [DISABLED]
      end
    end

    def most_restrictive_capability(entity_policy: nil)
      return DISABLED if entity_disallowed_by_owner?

      case owner_policy
      when ALL_ACTIONS, ALL_ACTIONS_FOR_SELECTED
        return entity_policy
      when LOCAL_ACTIONS, LOCAL_ONLY_FOR_SELECTED
        return entity_policy.in?(OPTIONS_ALLOWED_BY_LOCAL) ? entity_policy : owner_policy
      end

      DISABLED
    end

    private

    def owner_policy
      configuration_owner.action_execution_capabilities
    end

    # disallowed if owner policy is *_for_selected and entity is not selected
    def entity_disallowed_by_owner?
      owner_policy.in?(OPTIONS_FOR_SELECTED_ENTITIES) &&
        !actions_allowed_by_owner?
    end
  end
end
