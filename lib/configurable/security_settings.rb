# frozen_string_literal: true

module Configurable
  module SecuritySettings

    # Handles toggling of security settings - content analysis, dependency graph, Dependabot alerts, Dependabot security updates, and secret scanning.
    def manage_security_settings(params, actor:)

      # Content analysis
      if params[:consent_given_for_content_analysis] == "1"
        self.enable_content_analysis(actor: actor)
      elsif params[:consent_given_for_content_analysis] == "0"
        self.disable_content_analysis(actor: actor)
      end

      # Dependency graph
      if params[:dependency_graph_enabled] == "1"
        self.transaction do
          self.enable_content_analysis(actor: actor)
          self.enable_dependency_graph(actor: actor)
        end
      elsif params[:dependency_graph_enabled] == "0"
        self.transaction do
          self.disable_dependency_graph(actor: actor)
          self.disable_vulnerability_alerts(actor: actor)
          self.disable_vulnerability_updates(actor: actor)
        end
      end

      # Dependabot alerts
      if params[:vulnerability_alerts_enabled] == "1"
        self.transaction do
          self.enable_content_analysis(actor: actor)
          self.enable_dependency_graph(actor: actor)
          self.enable_vulnerability_alerts(actor: actor)
          self.ensure_manifest_is_initialized
        end
      elsif params[:vulnerability_alerts_enabled] == "0"
        self.transaction do
          self.disable_vulnerability_alerts(actor: actor)
          self.disable_vulnerability_updates(actor: actor)
        end
      end

      # Dependabot security updates
      if params[:vulnerability_updates_enabled] == "1"
        self.transaction do
          self.enable_content_analysis(actor: actor)
          self.enable_dependency_graph(actor: actor)
          self.enable_vulnerability_alerts(actor: actor)
          self.ensure_manifest_is_initialized
          self.enable_vulnerability_updates(actor: actor)
        end
      elsif params[:vulnerability_updates_enabled] == "0"
        self.disable_vulnerability_updates(actor: actor)
      end

      # Secret scanning
      if self.show_token_scanning_ui?(actor)
        if params[:token_scanning_enabled] == "1"
          self.transaction do
            self.enable_content_analysis(actor: actor)
            self.enable_token_scanning(actor: actor)
          end
        elsif params[:token_scanning_enabled] == "0"
          self.disable_token_scanning(actor: actor)
        end
      end

      self.ensure_current_token_scan_status_entry!
    end
  end
end
