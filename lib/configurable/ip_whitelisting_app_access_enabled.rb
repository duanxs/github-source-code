# frozen_string_literal: true

# Manage whether Integrations are allowed to access content that is protected
# with an IP allow list. If disabled, all Integration access will be denied,
# otherwise the org or business allow list, as well as the allow list for the
# Integration is checked to determine if the request is from a trusted source.
# Currently included by Organization and Business.
module Configurable
  module IpWhitelistingAppAccessEnabled
    KEY = "ip_whitelisting_app_access_enabled".freeze

    # Public: Enable IP whitelisting app access on the object.
    #
    # actor - The User enabling the setting.
    #
    # Returns nothing.
    def enable_ip_whitelisting_app_access(actor: nil)
      return unless config.enable!(KEY, actor)

      instrument_enable_app_access(actor)
    end

    # Public: Disable IP whitelisting app access on the object.
    #
    # actor - The User disabling the setting.
    # reason - An optional String representing the reason for disabling.
    #
    # Returns nothing.
    def disable_ip_whitelisting_app_access(actor: nil, reason: nil)
      return unless config.delete(KEY, actor)

      instrument_disable_app_access(actor, reason)
    end

    # Public: Is IP whitelisting app access enabled on the object?
    #
    # Returns Boolean.
    def ip_whitelisting_app_access_enabled?
      GitHub.ip_whitelisting_available? && config.enabled?(KEY)
    end

    # Public: Is IP whitelisting app access enabled at on a higher level object?
    #
    # For example, currently returns true when called on an org that is a member
    # of a business that has IP whitelisting enabled.
    #
    # Returns Boolean.
    def ip_whitelisting_app_access_enabled_policy?
      ip_whitelisting_app_access_enabled? && config.inherited?(KEY)
    end

    private

    def instrument_enable_app_access(actor)
      GitHub.instrument("ip_allow_list.app_access.enable", instrumentation_app_access_payload(actor))

      GlobalInstrumenter.instrument("ip_allow_list.app_access.enable", {
        owner: self,
        actor: actor,
      })
    end

    def instrument_disable_app_access(actor, reason)
      payload = instrumentation_app_access_payload(actor)
      payload[:reason] = reason unless reason.blank?
      GitHub.instrument("ip_allow_list.app_access.disable", payload)

      GlobalInstrumenter.instrument("ip_allow_list.app_access.disable", {
        owner: self,
        actor: actor,
      })
    end

    def instrumentation_app_access_payload(actor)
      payload = { user: actor }

      case self
      when ::Organization
        payload[:org] = self
        payload[:business] = self.business if self.business
      when ::Business
        payload[:business] = self
      end

      payload
    end

  end
end
