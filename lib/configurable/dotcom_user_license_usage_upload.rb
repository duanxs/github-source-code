# rubocop:disable Style/FrozenStringLiteralComment

# Whether automatic user license usage upload to dotcom is enabled.
module Configurable
  module DotcomUserLicenseUsageUpload
    KEY = "dotcom_user_license_usage_upload".freeze

    def enable_dotcom_user_license_usage_upload(actor)
      config.enable(KEY, actor)
    end

    def disable_dotcom_user_license_usage_upload(actor)
      config.disable(KEY, actor)
    end

    def dotcom_user_license_usage_upload_enabled?
      config.enabled?(KEY)
    end
  end
end
