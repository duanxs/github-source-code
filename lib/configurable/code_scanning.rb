# frozen_string_literal: true

module Configurable
  module CodeScanning
    UPLOAD_ENDPOINT_DISABLED_KEY = "code_scanning.upload_endpoint_disabled"

    def enable_code_scanning_upload_endpoint(actor:)
      config.delete(UPLOAD_ENDPOINT_DISABLED_KEY, actor)
    end

    def disable_code_scanning_upload_endpoint(actor:)
      config.enable(UPLOAD_ENDPOINT_DISABLED_KEY, actor)
    end

    def code_scanning_upload_endpoint_globally_disabled?
      GitHub.code_scanning_upload_endpoint_disabled?
    end

    def code_scanning_upload_endpoint_disabled?
      config.enabled?(UPLOAD_ENDPOINT_DISABLED_KEY)
    end
  end
end
