# frozen_string_literal: true

module Configurable
  module MembersCanInviteOutsideCollaborators
    KEY = "disable_members_can_invite_outside_collaborators".freeze

    # Enable the `members can invite collaborators` setting by making sure the
    # 'disable' key is either absent or set to false - need to set it to false
    # for cases where we want the setting to be inherited
    #
    # force - force the setting to override the settings on any child objects
    # actor - user doing the action
    #
    # Returns nothing
    def allow_members_can_invite_outside_collaborators(force: false, actor:)
      changed = if force
        # since the default is false, disabling the setting is equivalent to deleting,
        # but in order to enforce the value, the setting record needs to exist
        config.disable!(KEY, actor, force)
      else
        config.delete(KEY, actor)
      end
      return unless changed

      instrument(:update_member_repository_invitation_permission,
                 instrumentation_payload_for_invite_setting(actor))
    end

    # Disable the `members can invite collaborators` setting by making sure the 'disable'
    # key is present
    #
    # force - force the setting to override the settings on any child objects
    # actor - user doing the action
    #
    # Returns nothing
    def disallow_members_can_invite_outside_collaborators(force: false, actor:)
      changed = config.enable!(KEY, actor, force)
      return unless changed

      instrument(:update_member_repository_invitation_permission,
                 instrumentation_payload_for_invite_setting(actor))
    end

    # Clear the `members can invite collaborators` setting for this object,
    # thus removing the default value for inheritance
    #
    # actor - user doing the action
    #
    # Returns nothing
    def clear_members_can_invite_outside_collaborators(actor:)
      changed = config.delete(KEY, actor)
      return unless changed

      instrument(:clear_members_can_invite_outside_collaborators,
                 instrumentation_payload_for_invite_setting(actor).except(:permission))
    end

    # Retrieves the value for the `members can invite collaborators` setting
    # Automatically returns false if feature is disabled at the Instance level (GHE only)
    # Returns true if the setting isn't set for this object or
    # its parent. GitHub is the ultimate parent, but we never set the setting
    # at that level, only for Businesses and Organizations
    #
    # returns: Boolean
    def members_can_invite_outside_collaborators?
      return true unless can_restrict_repo_invites?

      # return true unless the key is set to true. Key not being set at all
      # defaults to true
      !config.enabled?(KEY)
    end

    # @return [Boolean] whether restricting repo invitations is allowed at all
    # true for all enterprise orgs
    # true for businesses and business orgs on the business_plus plan
    def can_restrict_repo_invites?
      GitHub.enterprise? ||
        is_a?(Business) ||
        (self.respond_to?(:business_plus?) && self.business_plus?)
    end

    # Checks if this setting is enforced for all of this object's Configurable
    # children
    #
    # Returns: Boolean
    def members_can_invite_outside_collaborators_policy?
      !!config.final?(KEY)
    end

    private def instrumentation_payload_for_invite_setting(actor)
      payload = {
        actor: actor,
        permission: members_can_invite_outside_collaborators?,
      }

      if self.is_a?(Organization)
        payload[:org] = self
        payload[:business] = self.business if self.business
      elsif self.is_a?(Business)
        payload[:business] = self
      end

      payload
    end
  end
end
