# frozen_string_literal: true

module Configurable
  module RestrictNotificationDelivery
    extend Configurable::Async

    KEY = "restrict_notification_delivery"

    async_configurable :restrict_notifications_to_verified_domains?
    def restrict_notifications_to_verified_domains?
      return false unless GitHub.domain_verification_enabled?

      config.enabled?(KEY)
    end

    # Public: Enables restricting notifications to emails that match a verified
    # domain for an organization.
    #
    # actor - The User enabling this setting.
    #
    # Returns a Boolean.
    def enable_notification_restrictions(actor:)
      return false unless GitHub.domain_verification_enabled?
      return false unless plan_supports?(:restrict_notification_delivery)
      return false unless organization_domains.verified.any?
      return true if restrict_notifications_to_verified_domains?

      changed = config.enable(KEY, actor)
      return false unless changed

      GitHub.dogstats.increment("restrict_notification_delivery.enable")
      instrument "restrict_notification_delivery.enable", event_context.merge({ actor: actor })

      # notify members who do not have a verified domain email and can no longer
      # receive notifications for this organization
      NotifyNotificationRestrictedMembersJob.perform_later(self)

      # automatically set eligible email address for newsies routing for
      # affected members
      UpdateOrganizationRoutingToEligibleEmailJob.perform_later(self)

      true
    end

    # Public: Disables restricting notifications to emails that match a verified
    # domain for an organization.
    #
    # actor - The User enabling this setting.
    #
    # Returns a Boolean.
    def disable_notification_restrictions(actor:)
      return false unless GitHub.domain_verification_enabled?
      return true unless restrict_notifications_to_verified_domains?

      changed = config.disable(KEY, actor)
      return false unless changed

      GitHub.dogstats.increment("restrict_notification_delivery.disable")
      instrument "restrict_notification_delivery.disable", event_context.merge({ actor: actor })
      true
    end
  end
end
