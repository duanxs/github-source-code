# frozen_string_literal: true

require "drb"
require "drb/unix" unless Gem.win_platform?

module ActiveSupport
  module Testing
    class Parallelization # :nodoc:
      class ServerInterface
        include DRb::DRbUndumped

        MAX_FAILING_TESTS = 15

        def initialize
          @queue = Queue.new
          @active_workers = Concurrent::Map.new
          @in_flight = Concurrent::Map.new
          @all_suites = []
          @first_run_failures = []
          @first_run = true
        end

        def record(worker_id, results)
          results.each do |reporter, result|
            raise DRb::DRbConnError if result.is_a?(DRb::DRbUnknown)
            @in_flight.delete([result.klass, result.name])

            if !result.passed? && !result.skipped?
              add_test_failure(result)
            end

            reporter.synchronize do
              reporter.record(result)
            end
          end
        end

        def <<(os)
          os.each do |o|
            o[2] = DRbObject.new(o[2])
          end if os
          @all_suites << os
          @queue << os
        end

        def pop(worker_id)
          if !max_failures_reached? && tests = @queue.pop
            tests.each { |x| @in_flight[[x[0].to_s, x[1]]] = x }
            tests
          end
        end

        def start_worker(worker_id, worker_number, worker_host)
          @active_workers[worker_id] = { name: "#{worker_number}@#{worker_host}", id: worker_id, start_time: Time.now  }
        end

        def stop_worker(worker_id)
          @active_workers.delete(worker_id)
        end

        def total_workers
          @active_workers.size
        end

        def active_workers?
          @active_workers.size > 0
        end

        def shutdown
          # Wait for initial queue to drain
          while still_testing?
            sleep 0.1
          end

          # To measure the time taken to rerun failed tests.
          retry_start_time = Time.current

          if max_failures_reached?
            STDERR.puts "=== WE'VE HIT THE MAXIMUM ALLOWED NUMBER OF TEST FAILURES! Shutting down early. ==="
          elsif run_flake_detection?
            perform_second_run
          else
            puts "===SKIPPED FLAKE DETECTION==="
          end

          @queue.close

          # Wait until all workers have finished
          while total_workers > 0
            sleep 1
          end

          report_tests_left_inflight

          ParallelCollector.descendants.each do |klass|
            klass.instance.process
          end

          retry_duration = (Time.current - retry_start_time) * 1000
          GitHubTest.dogstats.timing("flake_detection", retry_duration)
        end

        def report_tests_left_inflight
          @in_flight.values.each do |(klass, name, reporter)|
            result = Minitest::Result.from(klass.new(name))
            error = RuntimeError.new("result not reported")
            error.set_backtrace([""])
            result.failures << Minitest::UnexpectedError.new(error)
            reporter.synchronize do
              reporter.record(result)
            end
          end
        end

        def max_failures_reached?
          @first_run_failures.count >= MAX_FAILING_TESTS
        end

        def still_testing?
          return false if max_failures_reached?

          @queue.length != 0 || !@in_flight.empty?
        end


        def record_exception(worker_id, exception)
          warn "Exception from worker: #{@active_workers[worker_id][:name]}"
          warn exception
          warn exception.backtrace
        end

        def run_flake_detection?
          ENV["RUN_FLAKE_DETECTION"] &&
            !max_failures_reached? &&
            !@in_flight.empty?
        end

        def record_collector_data(data_by_class_name)
          data_by_class_name.each do |class_name, data|
            class_name.constantize.instance.append_data(data)
          end
        end

        def perform_second_run
          while !@in_flight.empty?
            sleep 0.1
          end
          @first_run = false

          refill_queue_for_flake_detection

          while @queue.length != 0
            sleep 0.1
          end
        end

        def add_test_failure(result)
          @first_run_failures << result if @first_run
        end

        def refill_queue_for_flake_detection
          return if @first_run_failures.empty?

          @first_run_failures.each do |failure|
            @all_suites.flatten(1).map do |test|
              if test[0].to_s == failure.klass && test[1] == failure.name
                @queue << [test]
                break
              end
            end
          end
        end
      end
    end
  end
end
