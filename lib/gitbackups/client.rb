# frozen_string_literal: true

require_relative "gitbackups_pb"
require_relative "gitbackups_services_pb"

module Gitbackups
  class Client
    DEFAULT_TIMEOUT = 5.0

    def initialize(host, creds)
      # This forces grpc to be ok with a certificate that doesn't match the hostname.
      channel_args = {
        "grpc.ssl_target_name_override" => "gitbackupsd",
      }

      @stub = GitHub::GitBackups::V1::Gitbackups::Stub.new(host, creds, channel_args: channel_args, timeout: DEFAULT_TIMEOUT)
    end

    # Public: delete a repository from backups
    #
    # Note that no checks are performed against the legal hold statuses. This must
    # only be called after checking there.
    #
    # Returns: nothing
    def delete(spec)
      req = GitHub::GitBackups::V1::DeleteRequest.new(spec: spec)
      @stub.delete(req)
      return :ok
    rescue GRPC::NotFound
      return :not_found
    rescue GRPC::BadStatus => e
      return [e.details.to_s, ""]
    end

    def schedule_maintenance(specs)
      req = GitHub::GitBackups::V1::MaintenanceRequest.new(specs: specs)
      @stub.schedule_maintenance(req)
    end

    def status(spec)
      req = GitHub::GitBackups::V1::StatusRequest.new(spec: spec)
      @stub.status(req)
    end
  end
end
