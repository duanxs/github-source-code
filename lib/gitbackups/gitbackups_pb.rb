# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: gitbackups.proto

require 'google/protobuf'

require 'google/protobuf/empty_pb'
require 'google/protobuf/timestamp_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_file("gitbackups.proto", syntax: :proto3) do
    add_message "gitbackups.DeleteRequest" do
      optional :spec, :string, 1
    end
    add_message "gitbackups.MaintenanceRequest" do
      repeated :specs, :string, 1
    end
    add_message "gitbackups.StatusRequest" do
      optional :spec, :string, 1
    end
    add_message "gitbackups.StatusResponse" do
      optional :backupEnabled, :bool, 1
      optional :lastBackup, :message, 2, "google.protobuf.Timestamp"
    end
  end
end

module GitHub
  module GitBackups
    module V1
      DeleteRequest = ::Google::Protobuf::DescriptorPool.generated_pool.lookup("gitbackups.DeleteRequest").msgclass
      MaintenanceRequest = ::Google::Protobuf::DescriptorPool.generated_pool.lookup("gitbackups.MaintenanceRequest").msgclass
      StatusRequest = ::Google::Protobuf::DescriptorPool.generated_pool.lookup("gitbackups.StatusRequest").msgclass
      StatusResponse = ::Google::Protobuf::DescriptorPool.generated_pool.lookup("gitbackups.StatusResponse").msgclass
    end
  end
end
