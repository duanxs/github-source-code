# rubocop:disable Style/FrozenStringLiteralComment

module GitSigning
  module GPG
    SIGNATURE_PREFIX = "-----BEGIN PGP SIGNATURE-----".freeze
    SIGNATURE_SUFFIX = "-----END PGP SIGNATURE-----".freeze

    # Is this a GPG signature?
    #
    # sig - A String that might be a GPG signature.
    #
    # Returns boolean.
    def signature?(sig)
      return false unless sig
      sig = sig.strip
      sig.start_with?(SIGNATURE_PREFIX) && sig.include?(SIGNATURE_SUFFIX)
    end

    # Verify a set of messages/signatures.
    #
    # requests - An Array of request Hashes.
    #            :message   - The String message.
    #            :signature - The String signature.
    #            :email     - The email of the User that allegedly signed the
    #                         message.
    #            :user      - The User associated with the email address.
    #            :id        - A unique String to identify this Hash.
    #
    # Returns request Hashes, updated to include :valid and :reason keys, plus
    # other metadata.
    def verify_signatures(requests)
      return requests if requests.empty?
      maybe_valid = requests.dup

      # Load key-ids
      signatures = maybe_valid.map { |r| r[:signature] }

      key_ids = begin
        GitHub.gpg.signature_issuer_key_ids(signatures)
      rescue GpgVerify::Error => e
        handle_gpgverify_error(maybe_valid, e)
        return requests
      end

      signature_to_key_id = signatures.zip(key_ids).to_h

      # Weed out requests whose signatures can't be parsed.
      maybe_valid.select! do |req|
        if key_id = signature_to_key_id[req[:signature]]
          req[:key_id] = key_id
        else
          req[:reason] = GitSigning::MALFORMED_SIG
          req[:valid] = false
        end
      end

      # Load keys and associated emails.
      user_ids = maybe_valid.map { |r| r[:user].id }
      key_ids = maybe_valid.map { |r| r[:key_id] }
      keys = GpgKey.with_emails.where(
        user_id: user_ids,
        key_id: key_ids,
      )

      # Because multiple users can have the same key, we index by user_id *and*
      # key_id. This ensures that we grab the correct key for the given request.
      key_by_uid_kid = keys.index_by do |key|
        [key.user_id, key.key_id]
      end

      # Weed out requests with missing key or mismatch between user/key emails.
      maybe_valid.select! do |req|
        req[:key] = key_by_uid_kid[[req[:user].id, req[:key_id]]]

        if req[:key].nil?
          req[:reason] = GitSigning::UNKNOWN_KEY
          req[:valid] = false
        elsif !req[:key].allowed_email?(req[:email])
          req[:reason] = GitSigning::BAD_EMAIL
          req[:valid] = false
        elsif !req[:key].can_sign?
          req[:reason] = GitSigning::NOT_SIGNING_KEY
          req[:valid] = false
        elsif req[:key].expired?
          req[:reason] = GitSigning::EXPIRED_KEY
          req[:valid] = false
        else
           true
        end
      end

      # Do actual verification of remaining requests.
      gpg_requests = maybe_valid.map do |req|
        {
          signature: req[:signature],
          message: req[:message],
          key_id: req[:key_id],
          public_key: req[:key].public_key,
          id: req[:id],
        }
      end

      gpg_results =  begin
        GitHub.gpg.batch_verify(gpg_requests)
      rescue GpgVerify::Error => e
        handle_gpgverify_error(maybe_valid, e)
        return requests
      end

      maybe_valid.select! do |req|
        if gpg_results[req[:id]] == GpgVerify::VALID
          req[:reason] = GitSigning::VALID
          req[:valid] = true
        else
          req[:reason] = GitSigning::INVALID
          req[:valid] = false
        end
      end

      requests
    end

    # Mark a set of requests invalid with the appropriate reason based on the
    # type of gpgverify error.
    #
    # maybe_valid - An Array of request Hashes.
    # e           - A GpgVerify::Error or subclass instnace.
    #
    # Returns nothing.
    def handle_gpgverify_error(maybe_valid, e)
      reason = if e.is_a?(GpgVerify::Unavailable)
        GitSigning::GPGVERIFY_UNAVAILABLE
      else
        Failbot.report(e,
          requests: maybe_valid.map { |r| r.merge(user: r[:user].try(:id)) },
          app: GitSigning::HAYSTACK_BUCKET,
        )

        GitSigning::GPGVERIFY_ERROR
      end

      maybe_valid.select! do |req|
        req[:reason] = reason
        req[:valid] = false
      end
    end

    extend self
  end
end
