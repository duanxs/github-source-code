# rubocop:disable Style/FrozenStringLiteralComment

module Elastomer::Indexes

  # The WorkflowRuns index contains documents for workflow runs.
  # These documents are searchable within this index.
  #
  class WorkflowRuns < ::Elastomer::Index

    # Defines the mappings for the 'workflow_run' document type.
    #
    # Returns the Hash containing the document type mappings.
    #
    def self.mappings
      {
        workflow_run: {
          _all: { enabled: false },
          properties: {
            name: {
              type: "multi_field",
              fields: {
                name: { type: "string", analyzer: "lowercase" },
                ngram: { type: "string", analyzer: "lowercase_ngram", search_analyzer: "lowercase" },
              },
            },
            workflow_run_id: { type: "integer" },
            check_suite_id: { type: "integer" },
            status: { type: "string" },
            conclusion: { type: "string" },
            head_branch: { type: "string" },
            head_sha: { type: "string" },
            event: { type: "string" },
            action: { type: "string" },
            created_at: { type: "date" },
            updated_at: { type: "date" },
            repo_id: { type: "integer" },
            workflow_name: { type: "string" },
            workflow_id: { type: "integer" },
            creator: { type: "string" },
            pusher: { type: "string" },
            is: { type: "string" },
            actor: { type: "string" },
            title: { type: "string" },
            lab: { type: "boolean" },
            rank: { type: "double" },
          },
        },
      }
    end

    # Settings for a WorkflowRuns search index.
    #
    # Returns the Hash containing the settings for this index.
    #
    def self.settings
      settings = {
        index: {
          number_of_shards: GitHub.es_shard_count_for_workflow_runs,
          number_of_replicas: GitHub.es_number_of_replicas,
          auto_expand_replicas: GitHub.es_auto_expand_replicas,
          "queries.cache.enabled": true,
        },
        analysis: {
          analyzer: {
            name: {
              tokenizer: "standard",
              filter: %w[lowercase p_asciifolding],
            },
            lowercase: {
              tokenizer: "whitespace",
              filter: %w[lowercase],
            },
            lowercase_ngram: {
              tokenizer: "whitespace",
              filter: %w[lowercase ngram_text],
            },
          },
          filter: {
            ngram_text: {
              type: "edgeNGram",
              min_gram: 2,
              max_gram: 20,
              side: "front",
            },
            p_asciifolding: {
              type: "asciifolding",
              preserve_original: true,
            },
          },
        },
      }

      ::Elastomer::Analyzers.configure_texty settings
      settings
    end

    # Purge the workflow run from the search index.
    #
    # workflow run - The WorkflowRun for whom all search records will be purged.
    #
    # Returns this search index.
    def purge_workflow_run(workflow_run)
      docs.delete type: "workflow_run", id: workflow_run.id
      self
    end

    # Restore the workflow run to the search index.
    #
    # workflow_run - The WorkflowRun for whom all search records will be restored.
    #
    # Returns this search index.
    def restore_workflow_run(workflow_run)
      store Elastomer::Adapters::WorkflowRun.create(workflow_run)
      self
    end

  end  # WorkflowRuns
end  # Elastomer::Indexes
