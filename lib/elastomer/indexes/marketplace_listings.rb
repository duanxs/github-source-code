# frozen_string_literal: true

module Elastomer::Indexes
  # The MarketplaceListings index contains documents for GitHub Marketplace listings.
  #
  # These documents are searchable within this index.
  class MarketplaceListings < ::Elastomer::Index
    # Defines the mappings for the 'marketplace_listing' document type.
    #
    # Returns the Hash containing the document type mappings.
    def self.mappings
      {
        marketplace_listing: {
          _all: { enabled: false },
          properties: {
            name: {
              type: "multi_field",
              fields: {
                name: { type:  "string", analyzer: "texty" },
                ngram: { type:  "string", analyzer: "index_ngram_text",
                         search_analyzer: "search_ngram_text" },
                raw: { type:  "string", index: "not_analyzed" },
              },
            },
            short_description: { type: "string", analyzer: "texty" },
            full_description: { type: "string", analyzer: "texty" },
            description: { type: "string", analyzer: "texty" },
            created_at: { type: "date" },
            updated_at: { type: "date" },
            primary_category: { type: "string", index: "not_analyzed" },
            secondary_category: { type: "string", index: "not_analyzed" },
            categories: {
              type: "string",
              fields: {
                raw: { type: "string", index: "not_analyzed" },
              },
            },
            free: { type: "boolean" },
            offers_free_trial: { type: "boolean" },
            state: { type: "string", index: "not_analyzed" },
            marketplace_id: { type: "string", index: "not_analyzed" },
            installation_count: { type: "integer" },
          },
        },
      }
    end

    # Settings for a MarketplaceListing search index.
    #
    # Returns the Hash containing the settings for this index.
    def self.settings
      settings = {
        index: {
          number_of_shards:   GitHub.es_shard_count_for_marketplace_listings,
          number_of_replicas: GitHub.es_number_of_replicas,
          auto_expand_replicas: GitHub.es_auto_expand_replicas,
          "queries.cache.enabled": true,
        },
        analysis: {
          analyzer: {
            index_ngram_text: {
              tokenizer: "standard",
              filter:    %w[lowercase standard ngram_text],
            },
            search_ngram_text: {
              tokenizer: "standard",
              filter:    %w[lowercase standard],
            },
          },
          filter: {
            ngram_text: {
              type:     "edgeNGram",
              min_gram: 1,
              max_gram: 20,
              side:     "front",
            },
          },
        },
      }

      ::Elastomer::Analyzers.configure_texty settings
      settings
    end

    # Returns the Array of valid aliases for this index type.
    def self.aliases
      [::Elastomer.env.logical_index_name(self), ::Elastomer.env.index_name("marketplace-search")]
    end

    # Public: Returns a MarketplaceListings index configured to search the
    # `marketplace-search` alias. This alias spans the `marketplace-listings` search index and
    # the `non-marketplace-listings` search index enabling us to search both with one
    # query.
    #
    # name    - The index name as a String or Symbol
    # cluster - The cluster name as a String or Symbol
    #
    # Returns an MarketplaceListings search index instance.
    def self.searcher(name = ::Elastomer.env.index_name("marketplace-search"), cluster = nil)
      if cluster.nil? && name.start_with?("marketplace-search")
        cluster = ::Elastomer.router.
          cluster_for_index(name.sub(/\Amarketplace-search/, "marketplace-listings"))
      end
      new(name, cluster)
    end
  end
end
