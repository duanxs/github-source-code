# frozen_string_literal: true

module Elastomer::Indexes
  # The NonMarketplaceListings index contains documents for Works with GitHub listings.
  #
  # These documents are searchable within this index.
  class NonMarketplaceListings < ::Elastomer::Index
    # Defines the mappings for the 'marketplace_listing' document type.
    #
    # Returns the Hash containing the document type mappings.
    def self.mappings
      {
        non_marketplace_listing: {
          _all: { enabled: false },
          properties: {
            name: {
              type: "multi_field",
              fields: {
                name: { type:  "string", analyzer: "texty" },
                ngram: { type:  "string", analyzer: "index_ngram_text",
                         search_analyzer: "search_ngram_text" },
                raw: { type:  "string", index: "not_analyzed" },
              },
            },
            description: { type: "string", analyzer: "texty" },
            url: { type: "string", index: "not_analyzed" },
            created_at: { type: "date" },
            updated_at: { type: "date" },
            categories: {
              type: "string",
              fields: {
                raw: { type: "string", index: "not_analyzed" },
              },
            },
            state: { type: "string", index: "not_analyzed" },
            marketplace_id: { type: "string", index: "not_analyzed" },
            enterprise_compatible: { type: "boolean" },
          },
        },
      }
    end

    # Settings for a NonMarketplaceListing search index.
    #
    # Returns the Hash containing the settings for this index.
    def self.settings
      settings = {
        index: {
          number_of_shards:   GitHub.es_shard_count_for_non_marketplace_listings,
          number_of_replicas: GitHub.es_number_of_replicas,
          auto_expand_replicas: GitHub.es_auto_expand_replicas,
          "queries.cache.enabled": true,
        },
        analysis: {
          analyzer: {
            index_ngram_text: {
              tokenizer: "standard",
              filter:    %w[lowercase standard ngram_text],
            },
            search_ngram_text: {
              tokenizer: "standard",
              filter:    %w[lowercase standard],
            },
          },
          filter: {
            ngram_text: {
              type:     "edgeNGram",
              min_gram: 1,
              max_gram: 20,
              side:     "front",
            },
          },
        },
      }

      ::Elastomer::Analyzers.configure_texty settings
      settings
    end

    # Returns the Array of valid aliases for this index type.
    def self.aliases
      [::Elastomer.env.logical_index_name(self), ::Elastomer.env.index_name("marketplace-search")]
    end
  end
end
