# frozen_string_literal: true

module Elastomer::Indexes
  class Vulnerabilities < ::Elastomer::Index
    def self.mappings
      {
        topic: {
          _all: { enabled: false },
          properties: {
            description: { type: "string", analyzer: "texty" },
            created_at: { type: "date" },
            updated_at: { type: "date" },
            published_at: { type: "date" },
            withdrawn_at: { type: "date" },
            withdrawn: { type: "boolean" },
            ghsa_id: { type: "string", index: "not_analyzed" },
            cve_id: { type: "string", index: "not_analyzed" },
            severity: { type: "string", index: "not_analyzed" },
            ecosystem: { type: "string", index: "not_analyzed" },
            affects: { type: "string", index: "not_analyzed" },
          },
        },
      }
    end

    def self.settings
      settings = {
        index: {
          number_of_shards: GitHub.es_shard_count_for_vulnerabilities,
          number_of_replicas: GitHub.es_number_of_replicas,
          auto_expand_replicas: GitHub.es_auto_expand_replicas,
          "queries.cache.enabled": true,
        },
        analysis: {
          analyzer: {
            index_ngram_text: {
              tokenizer: "standard",
              filter: %w[lowercase standard ngram_text],
            },
            search_ngram_text: {
              tokenizer: "standard",
              filter: %w[lowercase standard],
            },
          },
          filter: {
            ngram_text: {
              type: "edgeNGram",
              min_gram: 1,
              max_gram: 20,
              side: "front",
            },
          },
        },
      }

      ::Elastomer::Analyzers.configure_texty settings
      settings
    end
  end
end
