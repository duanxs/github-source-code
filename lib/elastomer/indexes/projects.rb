# rubocop:disable Style/FrozenStringLiteralComment

module Elastomer::Indexes

  # The Projects index contains documents for projects.
  # These documents are searchable within this index.
  #
  class Projects < ::Elastomer::Index

    # Defines the mappings for the 'project' document type.
    #
    # Returns the Hash containing the document type mappings.
    #
    def self.mappings
      {
        project: {
          _all: { enabled: false },
          properties: {
            name: {
              type: "multi_field",
              fields: {
                name: { type:  "string", analyzer: "texty" },
                ngram: { type:  "string", analyzer: "index_ngram_text", search_analyzer: "search_ngram_text" },
                raw: { type:  "string", index: "not_analyzed" },
              },
            },
            body: { type: "string", analyzer: "texty" },
            creator_id: { type: "integer" },
            created_at: { type: "date" },
            updated_at: { type: "date" },
            closed_at: { type: "date" },
            org_id: { type: "integer" },
            repo_id: { type: "integer" },
            business_id: { type: "integer" },
            user_id: { type: "integer" },
            public: { type: "boolean" },
            number: { type: "integer" },
            project_type: { type: "string", index: "not_analyzed" },
            state: { type: "string", index: "not_analyzed" },
            linked_repository_id: { type: "integer" },
          },
        },
        project_card: {
          _all: { enabled: false },
          properties: {
            project_id: { type: "integer" },
            type: { type: "string", index: "not_analyzed" },
            title: {
              type: "multi_field",
              fields: {
                title: { type: "string", analyzer: "texty" },
                ngram: { type: "string", analyzer: "index_ngram_text", search_analyzer: "search_ngram_text" },
                raw: { type: "string", index: "not_analyzed" },
              },
            },
            repo_id: { type: "integer" },
            business_id: { type: "integer" },
            public: { type: "boolean" },
            state: { type: "string", index: "not_analyzed" },
            author_id: { type: "integer" },
            labels: { type: "string", index: "not_analyzed" },
            assignee_ids: { type: "integer" },
            milestone_num: { type: "integer" },
            created_at: { type: "date" },
            updated_at: { type: "date" },
          },
        },
      }

    end

    # Settings for a Project search index.
    #
    # Returns the Hash containing the settings for this index.
    #
    def self.settings
      settings = {
        index: {
          number_of_shards:   GitHub.es_shard_count_for_projects,
          number_of_replicas: GitHub.es_number_of_replicas,
          auto_expand_replicas: GitHub.es_auto_expand_replicas,
          "queries.cache.enabled": true,
        },
        analysis: {
          analyzer: {
            index_ngram_text: {
              tokenizer: "standard",
              filter:    %w[lowercase standard ngram_text],
            },
            search_ngram_text: {
              tokenizer: "standard",
              filter:    %w[lowercase standard],
            },
          },
          filter: {
            ngram_text: {
              type:     "edgeNGram",
              min_gram: 1,
              max_gram: 20,
              side:     "front",
            },
          },
        },
      }

      ::Elastomer::Analyzers.configure_texty settings
      settings
    end
  end
end
