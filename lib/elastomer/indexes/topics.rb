# frozen_string_literal: true

module Elastomer::Indexes
  # The Topics index contains documents for the `topics` table.
  #
  # These documents are searchable within this index.
  class Topics < ::Elastomer::Index
    # Defines the mappings for the 'topic' document type.
    #
    # Returns the Hash containing the document type mappings.
    def self.mappings
      {
        topic: {
          _all: { enabled: false },
          properties: {
            name: {
              type: "multi_field",
              fields: {
                name: { type: "string", analyzer: "lowercase" },
                ngram: { type: "string", analyzer: "index_ngram_text",
                         search_analyzer: "search_ngram_text" },
              },
            },
            display_name: { type: "string", analyzer: "texty" },
            short_description: { type: "string", analyzer: "texty" },
            description: { type: "string", analyzer: "texty" },
            created_by: { type: "string", analyzer: "texty" },
            released: { type: "string", index: "not_analyzed" },
            created_at: { type: "date" },
            updated_at: { type: "date" },
            featured: { type: "boolean" },
            curated: { type: "boolean" },
            repository_count: { type: "integer" },
            aliases: { type: "string", index: "not_analyzed" },
            related: { type: "string", index: "not_analyzed" },
          },
        },
      }
    end

    # Settings for a MarketplaceListing search index.
    #
    # Returns the Hash containing the settings for this index.
    def self.settings
      settings = {
        index: {
          number_of_shards: GitHub.es_shard_count_for_topics,
          number_of_replicas: GitHub.es_number_of_replicas,
          auto_expand_replicas: GitHub.es_auto_expand_replicas,
          "queries.cache.enabled": true,
        },
        analysis: {
          analyzer: {
            index_ngram_text: {
              tokenizer: "standard",
              filter: %w[lowercase standard ngram_text],
            },
            search_ngram_text: {
              tokenizer: "standard",
              filter: %w[lowercase standard],
            },
            lowercase: {
              tokenizer: "whitespace",
              filter: %w[lowercase],
            },
          },
          filter: {
            ngram_text: {
              type: "edgeNGram",
              min_gram: 2,
              max_gram: 20,
              side: "front",
            },
          },
        },
      }

      ::Elastomer::Analyzers.configure_texty settings
      settings
    end
  end
end
