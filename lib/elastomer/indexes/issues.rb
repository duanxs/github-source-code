# rubocop:disable Style/FrozenStringLiteralComment

module Elastomer::Indexes

  # The Issues index contains documents for issues (and their comments) and
  # milestones. These documents are searchable within this index.
  #
  class Issues < ::Elastomer::Index

    # Defines the mappings for the 'issue' document type and the 'milestone'
    # document type.
    #
    # Returns the Hash containing the document type mappings.
    #
    def self.mappings
      {
        issue: {
          _all: { enabled: false },
          _routing: { required: true },
          properties: {
            title: {
              type: "multi_field",
              fields: {
                title: { type: "string", analyzer: "texty" },
                ngram: { type: "string", analyzer: "index_ngram_text", search_analyzer: "search_ngram_text" },
              },
            },
            body:               { type: "string", analyzer: "texty" },
            issue_id:           { type: "integer" },
            author_id:          { type: "integer" },
            num_comments:       { type: "integer" },
            num_reactions:      { type: "integer" },
            repo_id:            { type: "integer" },
            business_id:        { type: "integer" },
            network_id:         { type: "integer" },
            public:             { type: "boolean" },
            archived:           { type: "boolean" },
            state:              { type: "string", index: "not_analyzed" },
            number:             { type: "integer" },
            labels:             { type: "string", index: "not_analyzed" },
            language:           { type: "string", index: "not_analyzed" },
            language_id:        { type: "integer", doc_values: true },
            created_at:         { type: "date" },
            updated_at:         { type: "date" },
            closed_at:          { type: "date" },
            locked_at:          { type: "date" },
            locked:             { type: "boolean" },
            assignee_id:        { type: "integer" },
            milestone_num:      { type: "integer" },
            milestone_title:    { type: "string", index: "not_analyzed" },
            project_ids:        { type: "integer" },
            mentioned_user_ids: { type: "integer" },
            mentioned_team_ids: { type: "integer" },
            participating_user_ids: { type: "integer" },
            has_closing_reference: { type: "boolean" },
            reactions: {
              type: "object",
              properties: {
                "+1":          { type: "integer" },
                "-1":          { type: "integer" },
                smile:         { type: "integer" },
                thinking_face: { type: "integer" },
                heart:         { type: "integer" },
                tada:          { type: "integer" },
              },
            },

            comments: {
              type: "object",
              properties: {
                comment_id:   { type: "integer" },
                comment_type: { type: "string", index: "not_analyzed" },
                body:         { type: "string", analyzer: "texty" },
                author_id:    { type: "integer" },
                created_at:   { type: "date" },
                updated_at:   { type: "date" },

                reactions: {
                  type: "object",
                  properties: {
                    "+1":          { type: "integer" },
                    "-1":          { type: "integer" },
                    smile:         { type: "integer" },
                    thinking_face: { type: "integer" },
                    heart:         { type: "integer" },
                    tada:          { type: "integer" },
                  },
                },
              },
            },
          },
        },

        milestone: {
          _all: { enabled: false },
          _routing: { required: true },
          properties: {
            title: {
              type: "multi_field",
              fields: {
                title: { type: "string", analyzer: "texty" },
                ngram: { type: "string", analyzer: "index_ngram_text", search_analyzer: "search_ngram_text" },
              },
            },
            description: { type: "string", analyzer: "texty" },
            state:       { type: "string", index: "not_analyzed" },
            number:      { type: "integer" },
            author_id:   { type: "integer" },
            repo_id:     { type: "integer" },
            network_id:  { type: "integer" },
            public:      { type: "boolean" },
            due_on:      { type: "date" },
            created_at:  { type: "date" },
            updated_at:  { type: "date" },
          },
        },
      }
    end

    # Settings for an Issues search index.
    #
    # Returns the Hash containing the settings for this index.
    #
    def self.settings
      settings = {
        index: {
          number_of_shards: GitHub.es_shard_count_for_issues,
          number_of_replicas: GitHub.es_number_of_replicas,
          auto_expand_replicas: GitHub.es_auto_expand_replicas,
          "queries.cache.enabled": true,
        },
        analysis: {
          analyzer: {
            index_ngram_text: {
              tokenizer: "standard",
              filter: %w[lowercase standard ngram_text],
            },
            search_ngram_text: {
              tokenizer: "standard",
              filter: %w[lowercase standard],
            },
          },
          filter: {
            ngram_text: {
              type: "edgeNGram",
              min_gram: 2,
              max_gram: 20,
              side: "front",
            },
          },
        },
      }

      ::Elastomer::Analyzers.configure_texty settings
      settings
    end

    # Returns the Array of valid aliases for this index type.
    def self.aliases
      [::Elastomer.env.logical_index_name(self),
        ::Elastomer.env.index_name("issues-search")]
    end

    # Public: Returns an Issues index index configured to search the
    # `issues-search` alias. This alias spans the `issues` search index and
    # the `pull-requests` search index enabling us to search both with one
    # query.
    #
    # name    - The index name as a String or Symbol
    # cluster - The cluster name as a String or Symbol
    #
    # Returns an Issues search index instance.
    def self.searcher(name = ::Elastomer.env.index_name("issues-search"), cluster = nil)
      if cluster.nil? && name.start_with?("issues-search")
        cluster = ::Elastomer.router.cluster_for_index(name.sub(/\Aissues-search/, "issues"))
      end
      new(name, cluster)
    end

    # Purge all documents for the given user from this search index. This will
    # remove all issues and milestones where the user is (a) the author of the
    # document or (b) has commented on the document.
    #
    # user - The User for whom all search records will be purged.
    #
    # Returns this search index.
    def purge_user(user)
      begin
        delete_by_query(
            {term: {author_id: user.id}}, type: %w[issue milestone]
        )
      rescue StandardError => boom
        Failbot.report(boom.with_redacting!)
      end

      issue_ids = ActiveRecord::Base.connected_to(role: :reading) { user.interacted_issue_ids }
      issue_ids.each do |issue_id|
        begin
          store Elastomer::Adapters::Issue.create(issue_id)
        rescue StandardError => boom
          Failbot.report(boom.with_redacting!, issue_id: issue_id)
        end
      end

      self
    end

    # Restore all documents for given user in this search index. Any Issue or
    # Milestone will be re-indexed where the user is (a) the author or (b) has
    # commented on the item.
    #
    # user - The User for whom all search records will be restored.
    #
    # Returns this search index.
    def restore_user(user)
      issue_ids = ActiveRecord::Base.connected_to(role: :reading) { user.interacted_issue_ids }
      issue_ids.each do |issue_id|
        begin
          store Elastomer::Adapters::Issue.create(issue_id)
        rescue StandardError => boom
          Failbot.report(boom.with_redacting!, issue_id: issue_id)
        end
      end

      Milestone.where(created_by_id: user.id).each do |milestone|
        begin
          store Elastomer::Adapters::Milestone.create(milestone)
        rescue StandardError => boom
          Failbot.report(boom.with_redacting!, milestone_id: milestone.id)
        end
      end

      self
    end

  end  # Issues
end  # Elastomer::Indexes
