# rubocop:disable Style/FrozenStringLiteralComment

module Elastomer::Adapters

  # The Milestone adapter is used to transform a milestone ActiveRecord object
  # into a Hash document that can be indexed in ElasticSearch.
  #
  class Milestone < ::Elastomer::Adapter

    # Public: Returns the name of the Index class responsible for storing the
    # generated documents.
    def self.index_name
      "Issues"
    end

    # Public: Accessor for the data model instance. If the `document_id` does
    # not map to any row in the database, then `nil` is returned.
    #
    # Returns the data model instance.
    #
    def model
      @model ||= ::Milestone.includes(:repository, :created_by).find_by_id(document_id)
    end
    alias :milestone :model

    # Document routing information used to co-locate all milestones for a given
    # repository on a single shard in the search index.
    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def document_routing
      @document_routing ||= (milestone && milestone.repository_id)
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

    # Public: Construct a document suitable for indexing in ElasticSearch and
    # return it as a Hash.
    #
    # Returns the ElasticSearch document as a Hash.
    #
    def to_hash
      if model.nil?
        raise Elastomer::ModelMissing, "The data model has not been set, or the document ID does not exist in the database."
      end

      return @hash if defined? @hash
      @hash = nil

      # check the that a repository object exists and that issues are enabled
      return unless milestone.repository && milestone.repository.has_issues?
      return if milestone.spammy?

      @hash = {
        _id: document_id.to_s,
        _type: document_type,
        _routing: document_routing,
        number: milestone.number,
        author_id: milestone.created_by_id || milestone.user.id,
        repo_id: milestone.repository_id,
        network_id: milestone.repository.network_id,
        public: milestone.repository.public?,
        title: milestone.title,
        description: ::Search.sanitize(milestone.description),
        state: milestone.state,
        created_at: milestone.created_at,
        updated_at: milestone.updated_at,
      }
      @hash[:due_on] = milestone.due_on if milestone.due_on

      @hash
    end

  end  # Milestone
end  # Elastomer::Index
