# frozen_string_literal: true

module Elastomer::Adapters
  class NonMarketplaceListing < ::Elastomer::Adapter

    def self.areas_of_responsibility
      [:search, :marketplace]
    end

    # Public: Returns the name of the Index class responsible for storing the
    # generated documents.
    def self.index_name
      "NonMarketplaceListings"
    end

    # Public: Accessor for the data model instance. If the `document_id` does
    # not map to any row in the database, then `nil` is returned.
    #
    # Returns the data model instance.
    #
    def model
      @model ||= ::NonMarketplaceListing.find_by_id(document_id)
    end

    # Public: Construct a document suitable for indexing in ElasticSearch and
    # return it as a Hash.
    #
    # Returns the ElasticSearch document as a Hash.
    def to_hash
      if model.nil?
        raise Elastomer::ModelMissing,
          "The data model has not been set, or the document ID does not exist in the database."
      end

      return @hash if defined? @hash
      @hash = {
        _id: document_id.to_s,
        _type: document_type,
        name: model.name,
        description: model.description,
        url: model.url,
        created_at: model.created_at,
        updated_at: model.updated_at,
        categories: model.category.name,
        state: model.current_state.name.to_s,
        marketplace_id: "nmpl:#{document_id}",
        enterprise_compatible: model.enterprise_compatible?,
      }
    end
  end
end
