# rubocop:disable Style/FrozenStringLiteralComment

module Elastomer::Adapters

  # The PullRequest adapter is used to transform a pull request ActiveRecord
  # object into a Hash document that can be indexed in ElasticSearch.
  #
  class PullRequest < ::Elastomer::Adapter

    def self.areas_of_responsibility
      [:search, :pull_requests]
    end

    # Public: Returns the name of the Index class responsible for storing the
    # generated documents.
    def self.index_name
      "PullRequests"
    end

    # Public: Accessor for the data model instance. If the `document_id` does
    # not map to any row in the database, then `nil` is returned.
    #
    # Returns the data model instance.
    def model
      @model ||= ::PullRequest.includes(
        :repository, :user, :review_comments, :base_repository,
        :head_repository, :base_user, :head_user,
        { issue: [:repository, { comments: :user }, :labels] }
      ).find_by_id(document_id)
    end
    alias :pr :model

    # Document routing information used to co-locate all pull requests for a
    # given repository on a single shard in the search index.
    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def document_routing
      @document_routing ||= (pr && pr.repository_id)
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

    # Public: Construct a document suitable for indexing in ElasticSearch and
    # return it as a Hash.
    #
    # Returns the ElasticSearch document as a Hash.
    def to_hash
      if model.nil?
        raise Elastomer::ModelMissing, "The data model has not been set, or the document ID does not exist in the database."
      end

      return @hash if defined? @hash
      @hash = nil

      return unless pr.is_searchable?

      @hash = {
        _id: document_id.to_s,
        _type: document_type,
        _routing: document_routing,
        title: pr.title,
        body: ::Search.clean_and_sanitize(pr.body),
        issue_id: pr.issue.id,
        author_id: pr.user_id,
        repo_id: pr.repository_id,
        network_id: pr.repository.network_id,
        public: pr.repository.public?,
        archived: pr.repository.archived?,
        state: pr.issue.state,
        number: pr.number,
        labels: pr.issue.labels.map(&:name),
        head_ref: pr.head_ref_name.downcase,
        base_ref: pr.base_ref_name.downcase,
        created_at: pr.created_at,
        updated_at: pr.updated_at,
        closed_at: pr.closed_at,
        locked_at: pr.locked_at,
        locked: pr.locked?,
        assignee_id: pr.issue.assignees.map(&:id),
        project_ids: pr.issue.projects.pluck(:id),
        merged: pr.merged?,
        merged_at: pr.merged_at,
        mergeability: pr.merge_state.status,
        merge_commit: pr.merged? ? pr.merge_commit_sha : nil,
        status: pr.combined_status.state,
        draft: pr.draft?,
        review_status: pr.review_merge_states,
        mentioned_user_ids: pr.issue.referenced_user_ids,
        mentioned_team_ids: pr.issue.referenced_team_ids,
        participating_user_ids: pr.issue.participants.map(&:id),
        has_closing_reference: pr.close_issue_references.exists?,
        reactions: ActiveRecord::Base.connected_to(role: :reading) { pr.issue.reactions_count },
        requested_reviewer_ids: pr.review_requests.pending.users.map(&:id),
        requested_reviewer_team_ids: pr.review_requests.pending.teams.map(&:id),
        reviewer_ids: pr.reviews.map(&:user_id),
      }

      @hash[:labels].map! { |label| label.downcase }
      @hash.delete :labels if @hash[:labels].blank?

      @hash.delete :mentioned_user_ids if @hash[:mentioned_user_ids].blank?
      @hash.delete :mentioned_team_ids if @hash[:mentioned_team_ids].blank?
      @hash.delete :participating_user_ids if @hash[:participating_user_ids].blank?
      @hash.delete :project_ids if @hash[:project_ids].blank?

      @hash.merge! commit_info

      if language = pr.repository.primary_language
        @hash[:language]    = language.linguist_name
        @hash[:language_id] = language.linguist_id
      end

      if pr.issue.milestone
        @hash[:milestone_num]   = pr.issue.milestone.number
        @hash[:milestone_title] = pr.issue.milestone.title.downcase
      end

      @hash[:num_reactions] = ActiveRecord::Base.connected_to(role: :reading) { pr.issue.reactions_count }.values.sum
      @hash[:comments] = issue_comments + review_comments

      # the `total_comments` method is used here to keep the data in
      # Elasticsearch congruent with the results we present via the
      # app/view_models/issues/issue_list_item.rb `comment_count` method
      # see https://github.com/github/github/pull/58080
      @hash[:num_comments] = pr.total_comments

      @hash[:num_interactions] = @hash[:num_comments] + @hash[:num_reactions]

      @hash
    end

    # Internal: Generate information about all the commits in this pull
    # request.
    #
    # Returns a Hash containing summarized commit information.
    def commit_info
      hash = {
        additions: nil,
        deletions: nil,
        changed_files: nil,
        commits: nil,
        num_commits: nil,
      }

      return hash unless pr.repository.exists_on_disk?

      historical_comparison = pr.historical_comparison
      diffs = historical_comparison.diffs

      if diffs.available?
        hash[:additions] = diffs.additions
        hash[:deletions] = diffs.deletions
        hash[:changed_files] = diffs.size
      end

      hash[:commits] = pr.linked_commit_ids(limit: 1000)
      hash[:num_commits] = historical_comparison.total_commits

      hash

    rescue GitRPC::ObjectMissing, GitRPC::CommandFailed, ::Repository::CommandFailed, GitHub::DGit::UnroutedError
      hash
    end

    # Internal: Take all the issue comments and review comments for the pull
    # request and return them in a single array. Each comment is converted
    # into a Hash and added to the array.
    #
    # Returns an array of comments for the pull request.
    def issue_comments
      comments = []

      limit = ::Issue::COMMENT_LIMIT - comments.length
      normal_comments = pr.issue.comments.not_spammy.limit(limit).map do |comment|
        {
          comment_id: comment.id,
          comment_type: "issue",
          body: ::Search.clean_and_sanitize(comment.body),
          author_id: comment.user_id,
          created_at: comment.created_at,
          updated_at: comment.updated_at,
          reactions: ActiveRecord::Base.connected_to(role: :reading) { comment.reactions_count },
          dead: false,
        }
      end
      comments = comments + normal_comments
      comments.compact
    end

    # Internal: Take all the issue comments and review comments for the pull
    # request and return them in a single array. Each comment is converted
    # into a Hash and added to the array.
    #
    # Returns an array of comments for the pull request.
    def review_comments
      comments = pr.review_comments.not_spammy.reject(&:pending?).map do |comment|
        {
          comment_id: comment.id,
          comment_type: "review",
          body: ::Search.clean_and_sanitize(comment.body),
          author_id: comment.user_id,
          created_at: comment.created_at,
          updated_at: comment.updated_at,
          reactions: ActiveRecord::Base.connected_to(role: :reading) { comment.reactions_count },

          # TODO once we transitioned to the PRRC#outdated? attribute, use it here
          dead: !comment.live?,
        }
      end

      comments += pr.reviews.reject(&:pending?).map do |review|
        {
          comment_id: review.id,
          comment_type: "review_body",
          body: ::Search.clean_and_sanitize(review.body),
          author_id: review.user_id,
          created_at: review.created_at,
        }
      end
    end

  end  # PullRequest
end  # Elastomer::Index
