# frozen_string_literal: true

module Elastomer

  # The SearchIndexManager is responsible for creating search indices on the
  # clusters, promoting search indices to the primary role, and deleting search
  # indices. The manager class encapsulates the rules and logic for creating
  # sliced indices and promoting slices to the primary role or the entire sliced
  # index to the primary role.
  module SearchIndexManager
    extend self

    ES5_COMPATIBLE_VERSION = "5.0.0"

    # TODO gotta figure out naming and who is responsible for that ...

    # Compute a version SHA1 string for the :mappings and :settings of an
    # index. This version string will be stored in the index meta data. It is
    # useful for determining if the index settings are out of date and in need
    # of an update.
    #
    # opts - The options Hash
    #   :mappings   - index mappings Hash
    #   :settings   - index settings Hash
    #   :es_version - Elasticsearch version number as a String
    #   :v          - optional version number to force a new version string
    #
    # Returns a SHA1 version String.
    def version(mappings:, settings:, es_version: "0.0.0", v: 0)
      # COMPATIBILITY - remove after ES 5.x upgrade
      es_version = Semantic::Version.new(es_version.to_s)
      mappings = conform_to_es5(mappings, es_version)

      # add boilerplate ES5 settings if running in prod env
      settings = inject_es5_settings(settings, es_version, Rails.env.production?)

      version = Digest::SHA1.hexdigest \
        MultiJson.dump(v: v, mappings: mappings, settings: filter_settings(settings))

      version.freeze
    end

    # Filter out index settings based on GitHub.es_skip_settings_fields
    def filter_settings(settings)
      return if settings.nil?

      return settings if GitHub.es_skip_settings_fields.empty?

      return settings unless settings.key?(:index)

      settings = settings.dup
      settings[:index] = settings[:index].except(*GitHub.es_skip_settings_fields)

      settings
    end

    #
    #
    def create_index(name:, cluster: "default", mappings: nil, settings: nil, metadata: nil,
                     index_class: nil, range: nil, postfix: Elastomer::Environment.postfix)

      es_version  = Elastomer.router.client(cluster).version
      index_class = Elastomer.env.lookup_index(name) if index_class.nil?

      slicer = index_class.slicer
      slicer.postfix = postfix
      slicer.fullname = name

      metadata = {} unless metadata.is_a?(Hash)
      mappings = index_class.mappings || {} unless mappings.is_a?(Hash)
      settings = index_class.settings || {} unless settings.is_a?(Hash)

      # COMPATIBILITY - remove after ES 5.x upgrade
      mappings = conform_to_es5(mappings, es_version)

      # add boilerplate ES5 settings if running in prod env
      settings = inject_es5_settings(settings, es_version, Rails.env.production?)

      # application defined metadata
      index_metadata = index_class.metadata || {}
      index_metadata["version"] = metadata["version"] || version(mappings: mappings, settings: settings, es_version: es_version)

      # add the index metadata to the mappings
      mappings = mappings.merge({ Index::INDEX_META => { "_meta" => index_metadata }})

      # indexes that support slicing
      if index_class.sliced?
        if range
          each_slice(name, index_class, range) { |slice|
            _create_index(name: slice, cluster: cluster, mappings: mappings, settings: settings, metadata: metadata)
          }
        else
          _create_index(name: name, cluster: cluster, mappings: mappings, settings: settings, metadata: metadata)
        end

      # indexes that do not support slicing
      else
        _create_index(name: name, cluster: cluster, mappings: mappings, settings: settings, metadata: metadata)
      end
    end

    # Given a SearchIndexTemplateConfiguration instance and a slice name, create
    # a new search index for that particular slice. If an index slice already
    # exists a new one will still be created but with a higher slice version
    # number.
    #
    # template - a SearchIndexTemplateConfiguration instance
    # slice    - the String name of the slice (i.e. "2017-04" for audit logs)
    #
    # Returns the name of the newly created search index.
    def create_index_from_template(template:, slice:)
      slicer = template.slicer
      slicer.slice = slice
      slicer.slice_version = nil

      name = Elastomer.router.index_map.next_available_index_slice_name(slicer.fullname)

      metadata = {
        "read"    => true,
        "write"   => template.is_writable,
        "primary" => template.is_primary,
      }

      aliases = {}
      if template.is_primary
        aliases[slicer.index_alias] = {}

        slice_alias = slicer.slice_alias
        unless name == slice_alias
          aliases[slice_alias] = {}
        end
      end

      _create_index \
          name:     name,
          cluster:  template.cluster,
          metadata: metadata,
          aliases:  aliases,
          version:  template.version_sha

      name
    end

    # This methd will only create a new search index if a writable search index
    # created from the given `template` does not already exist. So if there is a
    # writable index for the template and slice, then that index name is
    # returned. But if there is not a writable index for the template and slice
    # then a new index is created.
    #
    # template - a SearchIndexTemplateConfiguration instance
    # slice    - the String name of the slice (i.e. "2017-04" for audit logs)
    #
    # Returns the name of the search index.
    def create_or_return_index_from_template(template:, slice:)
      name = writable_index_for(template: template, slice: slice)
      name = create_index_from_template(template: template, slice: slice) if name.nil?
      name
    end

    # Internal: Helper method that retrieves the first writable index for the
    # given index template and slice name. The search index must have the same
    # version number as the template and the given slice value.
    #
    # For audit logs, if the template is named "audit_log-3" and the slice value
    # is "2017-05" then the first writable index that matches "audit_log-3-2017-05-\d+"
    # will be returned.
    #
    # Returns a search index name as a String or `nil` if there are no writable
    # indices.
    def writable_index_for(template:, slice:)
      ary = Elastomer.router.index_map.writable(template.index_class, slice)
      return if ary.nil? || ary.empty?

      # find the index slice created from the template (the versions will match)
      index_config = ary.find { |ic| ic.index_version == template.version }
      return if index_config.nil?

      index_config.name
    end

    #
    #
    def open_index(name:, cluster: "default")
      client = Elastomer.router.client(cluster)
      index_class = Elastomer.env.lookup_index(name)

      if client.available?
        index = client.index(name)
        if index.exists?
          index.open
          GitHub.dogstats.event \
            "Search index opened: #{name.inspect}",
            "Search index #{name.inspect} opened on cluster #{cluster.inspect}",
            tags: %W[search:ops action:open]
        end
      end
    end

    #
    #
    def close_index(name:, cluster: "default", force: false)
      config = Elastomer.router.get_index_config(name)
      return false if !force && config && config.primary?

      client = Elastomer.router.client(cluster)
      index_class = Elastomer.env.lookup_index(name)

      unless config.nil?
        config.update!(read: false, write: false, primary: false)
        Elastomer.router.update_index_config(config)
      end

      if client.available?
        index = client.index(name)
        if index.exists?
          index.close
          GitHub.dogstats.event \
            "Search index closed: #{name.inspect}",
            "Search index #{name.inspect} closed on cluster #{cluster.inspect}",
            tags: %W[search:ops action:close]
        end
      end
    end

    #
    #
    def create_repair_job(name)
      job = Elastomer.env.lookup_repair_job(name)
      job.new(name) if job < Elastomer::Repair
    rescue Elastomer::Error
      nil
    end

    #
    #
    def delete_repair_job(name)
      repair_job = create_repair_job(name)
      repair_job.reset! if repair_job
    end

    #
    #
    def delete_index(name:, cluster: "default", index_class: nil, force: false, postfix: Elastomer::Environment.postfix)
      index_class = Elastomer.env.lookup_index(name) if index_class.nil?

      slicer = index_class.slicer
      slicer.postfix = postfix
      slicer.fullname = name

      if index_class.sliced? && slicer.slice.nil?
        all_index_slices(name) { |cfg| _delete_index(cfg.name, cfg.cluster, force) }
      else
        _delete_index(name, cluster, force)
      end
    end

    # Public: Make the given index the primary for the index class. This will
    # be the index that is used to serve queries.
    #
    # name - The index to make primary
    #
    # Returns `nil`
    def promote_index_to_primary(name:, index_class: nil, postfix: Elastomer::Environment.postfix)
      router      = Elastomer.router
      index_map   = router.index_map
      index_class = Elastomer.env.lookup_index(name) if index_class.nil?
      kind        = index_class.logical_index_name

      slicer          = index_class.slicer
      slicer.postfix  = postfix
      slicer.fullname = name

      # operate on all slices for the index
      cmds = if index_class.sliced? && slicer.slice.nil?
        primaries = index_map.primary(kind, "*")
        version   = primaries.map(&:index_version).uniq.min
        primary   = "#{slicer.index}-#{version}"
        return if primary == name

        candidates = index_map.all_index_slices(name)
        if candidates.nil? || candidates.empty?
          raise ArgumentError, "index #{name.inspect} does not exist in the search index router"
        else
          candidates.each do |cfg|
            next if cfg.exists?
            raise ArgumentError, "index #{cfg.name.inspect} does not exist on the search cluster"
          end
        end

        build_primary_promotion_cmds(primaries, candidates, index_class)
      else
        primaries = index_map.primaries(kind, slicer.slice)

        # Return if list of primary indexes consists of only the candidate
        return if primaries.size == 1 && primaries.first.name == name
        # Remove the candidate from the list of primaries to avoid it being demoted
        primaries.reject! { |index| index.name == name }

        candidate = router.get_index_config(name)
        if candidate.nil?
          raise ArgumentError, "index #{name.inspect} does not exist in the search index router"
        elsif !candidate.exists?
          raise ArgumentError, "index #{name.inspect} does not exist on the search cluster"
        end

        build_primary_promotion_cmds(primaries, candidate, index_class)
      end

      cmds.each { |method, args| self.send(method, *args) }

      GitHub.dogstats.event \
        "Search index promoted: #{name.inspect}",
        "Search index #{name.inspect} was promoted to the primary role and is now serving production queries for #{kind.inspect} search indices.",
        tags: %W[search:ops action:primary]

      nil
    end

    #
    #
    def create_template(name:, cluster: "default", mappings: nil, settings: nil, primary: false,
                        index_class: nil, postfix: Elastomer::Environment.postfix)

      client   = ::Elastomer.router.client(cluster)
      template = client.template(name)
      return if template.exists?

      es_version  = client.version
      name_adjust = name.sub(/_template\z/, "")
      index_class = ::Elastomer.env.lookup_index(name_adjust) if index_class.nil?

      slicer = index_class.slicer
      slicer.postfix = postfix
      slicer.fullname = name_adjust

      mappings = index_class.mappings || {} unless mappings.is_a?(Hash)
      settings = index_class.settings || {} unless settings.is_a?(Hash)

      # COMPATIBILITY - remove after ES 5.x upgrade
      mappings = conform_to_es5(mappings, es_version)

      # add boilerplate ES5 settings if running in prod env
      settings = inject_es5_settings(settings, es_version, Rails.env.production?)

      # application defined metadata
      index_metadata = index_class.metadata || {}
      index_metadata["version"] = version = version(mappings: mappings, settings: settings, es_version: es_version)

      # add the index metadata to the mappings
      mappings = mappings.merge({ Index::INDEX_META => { "_meta" => index_metadata }})
      template_glob = name_adjust =~ /test\z/i ? "#{name_adjust}*" : "#{name_adjust}-*"

      body = {
        order:    0,
        template: template_glob,
        mappings: mappings,
        settings: settings,
      }
      body[:aliases] = {slicer.index_alias => {}} if primary

      response = template.create(body)

      GitHub.dogstats.event \
        "Search index template created: #{name.inspect}",
        "Search index template #{name.inspect} created on cluster #{cluster.inspect} with version #{version.inspect}",
        tags: %W[search:ops action:create]

      response
    end

    #
    #
    def delete_template(name:, cluster: "default", force: false,
                        index_class: nil, postfix: Elastomer::Environment.postfix)

      client   = ::Elastomer.router.client(cluster)
      template = client.template(name)
      return unless template.exists?

      name_adjust = name.sub(/_template\z/, "")
      index_class = ::Elastomer.env.lookup_index(name_adjust) if index_class.nil?

      slicer = index_class.slicer
      slicer.postfix = postfix
      slicer.fullname = name_adjust

      hash = template.get
      aliases = hash.dig(name, "aliases")
      primary = aliases && aliases.keys.include?(slicer.index_alias)

      return if primary && !force

      begin
        response = template.delete
      rescue Elastomer::Client::TimeoutError => err
        Failbot.report(err)
      end

      GitHub.dogstats.event \
        "Search index template deleted: #{name.inspect}",
        "Search index template #{name.inspect} deleted from cluster #{cluster.inspect}",
        tags: %W[search:ops action:delete]

      response
    end

    # Internal: Build the list of commands needed to demote the current
    # `primaries` and promote the `candidates` to the primary role. The
    # `index_class` is included so this method can figure out all the required
    # aliases to transfer to the candidate index(es).
    #
    # primaries   - IndexConfig for the current primary (single or Array)
    # candidates  - IndexConfig for the candidate (single or Array)
    # index_class - the Index class for all the primaries and candidates
    #
    # Returns an array of [method_name, args] tuples.
    def build_primary_promotion_cmds(primaries, candidates, index_class)
      primaries  = Array(primaries)
      candidates = Array(candidates)
      alias_cmds = Hash.new { |h, k| h[k] = [] }
      cmds = []

      # 1 - add aliases to the candidate slices
      candidates.each do |cfg|
        cfg.aliases.each do |str|
          alias_cmds[cfg.cluster] << {add: {index: cfg.name, alias: str}}
        end
      end

      # 2 - remove aliases from the current primary slices
      primaries.dup.each do |cfg|
        next unless cfg.exists?
        aliases = cfg.aliases
        if aliases.include?(cfg.name)
          cmds << [:_delete_index, [cfg.name, cfg.cluster, true]]
          primaries.delete(cfg)
        else
          aliases.each do |str|
            alias_cmds[cfg.cluster] << {remove: {index: cfg.name, alias: str}}
          end
        end
      end

      alias_cmds = alias_cmds.to_a

      cmds << [:update_aliases, alias_cmds.first]
      cmds << [:update_index_config, [candidates, {read: true, write: true, primary: true}]]
      cmds << [:update_index_config, [primaries, {primary: false}]] unless primaries.empty?

      if alias_cmds.length == 2
        cmds << [:update_aliases, alias_cmds.last]

      elsif alias_cmds.length > 2
        raise ArgumentError, "too many search clusters: #{alias_cmds.keys.inspect}"
      end

      cmds
    end

    # Internal: Execute the various alias `actions` on the given Elasticsearch
    # `cluster`.
    #
    # cluster - name of the Elasticsearch cluster
    # actions - Array of alias actions
    #
    # see https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-aliases.html
    #
    # Returns the result from the Elastomer method call
    def update_aliases(cluster, actions)
      client = Elastomer.router.client(cluster)
      client.cluster.update_aliases(actions)
    end

    # Internal: Update the given `index_configs` using the `settings` hash.
    #
    # index_configs - IndexConfig to update (single or Array)
    # settings      - Hash of index config settings to update
    #
    def update_index_config(index_configs, settings)
      Array(index_configs).each do |cfg|
        cfg.update!(settings)
        Elastomer.router.update_index_config(cfg)
      end
    end

    # Internal: Given a base index name and a range of slices, this method will
    # generate full index names and yield them in turn to the given block.
    #
    # name        - base name for the index (audit_log-12)
    # index_class - an Elastomer::Index class
    # range       - a Range denoting the first and last slices
    #
    # Returns `nil`
    def each_slice(name, index_class, range)
      return unless index_class.sliced?

      slicer = index_class.slicer
      slicer.fullname = name
      slicer.slice_version = 1 if slicer.slice_version.nil?

      slicer.slice_range(range.first, range.last) do |slice|
        slicer.slice = slice
        yield slicer.fullname
      end

      nil
    end

    # Internal: Iterate the given block of code for the index configs of all
    # slices of an index. The index name and version are required to select all
    # the slices.
    #
    # name - The index name with version number
    #
    # Returns `nil` or an array of index settings.
    def all_index_slices(name, &block)
      index_map = Elastomer.router.index_map
      ary = index_map.all_index_slices(name)
      ary.each(&block) unless ary.nil?
    end

    # Internal: Creates an Elasticsearch index on a search cluster and adds an
    # entry to the MySQL index configuration table (see
    # Elastomer::Router:MysqlStore).
    #
    # name     - index name as a String
    # cluster  - cluster name as a String
    # mappings - Hash of index mappings
    # settings - Hash of index settings
    # metadata - Hash with metadata to store in the index config
    # aliases  - Hash of index aliases
    # version  - index version SHA
    #
    # Returns the Elasticsearch response Hash.
    def _create_index(name:, cluster:, metadata:, mappings: {}, settings: {}, aliases: {}, version: nil)
      client = Elastomer.router.client(cluster)
      index  = client.index(name)
      version ||= mappings.dig(Index::INDEX_META, "_meta", "version")

      return if index.exists?

      # create the index in Elasticsearch and wait for the cluster health to report green
      response = index.create(mappings: mappings, settings: settings, aliases: aliases)
      client.cluster.health \
          index: name,
          wait_for_status: "green",
          timeout: "7s",
          read_timeout: 9

      # now update our IndexConfig in the MySQL table so we know about this new index
      config = Elastomer::Router::IndexConfig.new \
          name:    name,
          cluster: cluster,
          version: version

      config.update!(metadata)
      Elastomer.router.update_index_config(config)

      GitHub.dogstats.event \
        "Search index created: #{name.inspect}",
        "Search index #{name.inspect} created on cluster #{cluster.inspect} with version #{version.inspect}",
        tags: %W[search:ops action:create]

      response
    end

    # Internal: Delete a single Elasticsearch index from a cluster and remove
    # the MySQL index configuration entry (see Elastomer::Router:MysqlStore).
    #
    # name     - index name as a String
    # cluster  - cluster name as a String
    # force    - boolean used to force index deletion
    #
    # Returns the Elasticsearch response Hash.
    def _delete_index(name, cluster, force)
      config = Elastomer.router.get_index_config(name)
      return false if !force && config && config.primary?

      client = Elastomer.router.client(cluster)
      Elastomer.router.remove_index_config(name)

      if client.available?
        index = client.index(name)
        if index.exists?
          delete_repair_job(name)
          begin
            index.delete
          rescue Elastomer::Client::TimeoutError => err
            Failbot.report(err)
          end
          GitHub.dogstats.event \
            "Search index deleted: #{name.inspect}",
            "Search index #{name.inspect} deleted from cluster #{cluster.inspect}",
            tags: %W[search:ops action:delete]
        end
      end
    end

    # Internal: [COMPATIBILITY] adjust index mappings to shim for breaking changes across
    # ES 2.x and 5.x line. Temporary, to be removed after ES clusters are fully migrated to 5.x
    #
    # mappings          - the mappings hash to be scanned (and possibly shimmed)
    # es_version        - the version of ES we'll be shimming mappings for
    #
    # Returns a possibly-mutated mappings hash
    def conform_to_es5(mappings, es_version)
      return mappings unless mappings.is_a?(Hash)
      return mappings if es_version && es_version < ES5_COMPATIBLE_VERSION

      # OK, so we're on ES5+, now let's recurse into the index mapping and shim stuff
      visitor = lambda do |(key, value)|
        next unless value.is_a?(Hash)

        # handle legacy "multi_field" type mapping
        # https://www.elastic.co/guide/en/elasticsearch/reference/1.4/_multi_fields.html
        if value.key?(:type) && value[:type].to_s == "multi_field"
            subval = value[:fields].delete(key)
            value.merge!(subval)
        end

        # lifts subfield with same name as this field's subvalues to top-level.
        # overwrites top-level "type" field. additional subfields remain under
        # "fields" subkey.

        # "string" type -> "text/keyword"
        # https://www.elastic.co/guide/en/elasticsearch/reference/5.5/breaking_50_mapping_changes.html#_literal_string_literal_fields_replaced_by_literal_text_literal_literal_keyword_literal_fields
        if value.key?(:type) && value[:type].to_s == "string"
          if value.key?(:index) && value.delete(:index).to_s == "not_analyzed"
            value[:type] = "keyword"
          else
            value[:type] = "text"
          end
        end

        # norms:{enabled:BOOLVAL} -> norms: BOOLVAL
        # https://www.elastic.co/guide/en/elasticsearch/reference/5.5/breaking_50_mapping_changes.html#_literal_norms_literal
        if value.key?(:norms) && value[:norms].is_a?(Hash)
          value[:norms] = value[:norms][:enabled]
        end

        value.each_pair(&visitor)
      end
      mappings.each_pair(&visitor)

      mappings
    end

    # Injects index-level settings into base settings hash only in prod env, for ES5+ clusters
    #
    # settings          -       base settings hash
    # es_version        -       string version of ES this client is communicating with
    # prod_env          -       boolean value, equal to "Rails.env.production?"
    def inject_es5_settings(settings, es_version, prod_env)
      return settings if es_version && es_version < ES5_COMPATIBLE_VERSION
      return settings if !prod_env

      settings[:index] = {} if !settings.key?(:index)
      if !settings.dig(:index, :translog)
        settings[:index][:translog] = {
          durability: "async",
          sync_interval: "5s",
        }
      end

      settings
    end

  end
end
