# Elastomer

Elastomer provides a uniform interface around ElasticSearch for creating and
maintaining search indexes, adding documents, and searching documents.

