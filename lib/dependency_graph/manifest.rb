# frozen_string_literal: true

# Public: Domain object to wrap dependency graph API `Manifest` type
module DependencyGraph
  class Manifest
    SUPERSEDED_BY = {
      "gemfile" => ["gemfile_lock"],
      "package_json" => ["package_lock_json", "yarn_lock"],
      "pipenv" => ["pipenv_lock"],
      "composer_json" => ["composer_lock"],
    }

    EXCEEDS_MAX_SIZE = :exceeds_max_size

    def self.wrap(manifests, dependencies_filter = {})
      manifests.map { |attrs| new(attrs["node"], dependencies_filter) }
    end

    def self.exceeds_max_size(attrs)
      new(attrs.merge("error" => EXCEEDS_MAX_SIZE))
    end

    def initialize(attrs, dependencies_filter = {})
      @attrs = attrs
      @dependencies_filter = dependencies_filter
    end

    def global_relay_id
      manifest_identifier = id
      Platform::Helpers::NodeIdentification.to_global_id("DependencyGraphManifest", "#{repository_id}:#{manifest_identifier}")
    end

    def repository_id
      attrs["repositoryId"]
    end

    def id
      attrs["id"]
    end

    def manifest_type
      attrs["manifestType"]
    end

    def basename
      attrs["filename"]
    end

    def path
      attrs["path"]
    end

    def vendored?
      !!attrs["isVendored"]
    end

    def filename
      pathname = Pathname.new(path).join(basename).to_s
      pathname.starts_with?("/") ? pathname[1..-1] : pathname
    end

    def depth
      filename.split("/").count
    end

    def dependencies_count
      attrs["dependenciesCount"] || attrs.dig("dependencies", "totalCount") || 0
    end

    def dependencies_page_info
      {
        start_cursor: attrs.dig("dependencies", "pageInfo", "startCursor"),
        end_cursor: attrs.dig("dependencies", "pageInfo", "endCursor"),
        has_next_page: attrs.dig("dependencies", "pageInfo", "hasNextPage"),
        has_previous_page: attrs.dig("dependencies", "pageInfo", "hasPreviousPage"),
      }
    end

    def dependencies_filter
      @dependencies_filter
    end

    def dependencies
      @dependencies ||= Dependency.wrap(attrs.dig("dependencies", "edges"), manifest: self)
    end

    def async_repository
      Platform::Loaders::ActiveRecord.load(::Repository, repository_id)
    end

    def to_json
      attrs.to_json
    end

    def ==(other)
      other.class == self.class && other.attrs == self.attrs
    end

    def supersedes?(other)
      other.is_a?(Manifest) && other.path == path && SUPERSEDED_BY[other.manifest_type]&.include?(manifest_type)
    end

    def exceeds_max_size?
      error == EXCEEDS_MAX_SIZE
    end

    def error
      attrs["error"]
    end

    def parseable?
      error.blank?
    end

    def platform_type_name
      "DependencyGraphManifest"
    end

    protected

    attr_reader :attrs
  end
end
