# frozen_string_literal: true

module DependencyGraph
  class ClearDependenciesMutation < Query
    def initialize(input:, backend: nil)
      @input = input
      super(backend: backend)
    end

    def execute
      backend.post <<~MUTATION, input: input
        mutation ClearDependencies($input: ClearDependenciesInput!) {
          clearDependencies(input: $input) {
            clientMutationId
          }
        }
      MUTATION
    end

    def execute!
      result = execute

      unless result.ok?
        raise DependencyGraph::Client::ApiError.new(result.error.message)
      end

      result
    end

    private

    attr_reader :input
  end
end
