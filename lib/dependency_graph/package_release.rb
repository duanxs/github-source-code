# frozen_string_literal: true

# Public: Domain object to wrap dependency graph API `PackageRelease` type
module DependencyGraph
  class PackageRelease
    def self.wrap(package_releases)
      package_releases.map { |attrs| new(attrs["node"]) }
    end

    def initialize(attrs)
      @attrs = attrs
    end

    def repository_id
      attrs["repositoryId"]
    end

    def package_name
      attrs["packageName"]
    end

    def package_manager
      attrs["packageManager"]
    end

    def published_on
      attrs["publishedOn"]
    end

    def version
      attrs["version"]
    end

    def license
      attrs["license"]
    end

    def dependencies_count
      attrs.dig("dependencies", "totalCount")
    end

    def dependencies_cursor
      dependencies.last.cursor
    end

    def dependencies
      @dependencies ||= Dependency.wrap(attrs.dig("dependencies", "edges"), manifest: nil)
    end

    def clearly_defined_score
      attrs["clearlyDefinedScore"]
    end

    def platform_type_name
      "DependencyGraphPackageRelease"
    end

    private

    attr_reader :attrs
  end
end
