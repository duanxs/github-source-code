# frozen_string_literal: true

require "docusign_esign"

module Docusign
  class CreateEmbedView
    def self.call(name:, email:, envelope_id:, return_url:)
      instance = new \
        name: name,
        email: email,
        envelope_id: envelope_id,
        return_url: return_url
      instance.call
    end

    def initialize(name:, email:, envelope_id:, return_url:)
      if envelope_id.nil?
        raise ::Docusign::MissingEnvelopeIdError
      end

      @name = name
      @email = email
      @envelope_id = envelope_id
      @return_url = return_url
    end

    def call
      envelopes_api.create_recipient_view \
        GitHub.docusign_account_id,
        envelope_id,
        embed_view_configuration
    end

    private

    attr_reader :name, :email, :envelope_id, :return_url

    def embed_view_configuration
      DocuSign_eSign::RecipientViewRequest.new \
        email: email,
        userName: name,
        returnUrl: return_url,
        authenticationMethod: "None",
        clientUserId: GitHub.docusign_user_id
    end

    def envelopes_api
      return @envelopes_api if defined?(@envelopes_api)
      @envelopes_api = DocuSign_eSign::EnvelopesApi.new(API.client)
    end
  end
end
