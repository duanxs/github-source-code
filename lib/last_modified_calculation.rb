# rubocop:disable Style/FrozenStringLiteralComment

# Extends models with the ability to calculate their own last_modified
# timestamp.
module LastModifiedCalculation
  # Public: Returns the Time that this object was last modified.
  def last_modified_at
    @last_modified_at ||= try(:updated_at) || try(:created_at) || Time.now.utc
  end

  # Calculates the last modified time for this object based on the most recent
  # timestamp based on this object's #updated_at, and any resources from the
  # attributes.
  #
  # *attrs - One or more Symbol attributes that mark dependent resources that
  #          affect the last modified timestamp for this object.
  #
  # Returns a Time.
  def last_modified_with(*attrs)
    tracked = track_loaded_associations
    resources = [updated_at, *attrs.map { |a| send(a) }]
    resources.flatten!
    resources.compact!
    last_modified_from(resources)
  ensure
    GitHub.dogstats.histogram("query.loaded_associations", tracked.count, tags: ["via:api", "resource:#{tracked.resource.class}"]) if tracked
  end

  # Gets the most recent Time for the given resources:
  #
  # *resources - One or more Resources that responds to #last_modified_at, or
  #              a Time object.
  #
  # Returns a Time.
  def last_modified_from(resources)
    resources.map { |r| r.respond_to?(:last_modified_at) ? r.last_modified_at : r }.max
  end

  # Tracks how many associations are loaded for a given model.
  #
  #   issue = Issue.find 1
  #   tracked = issue.track_loaded_associations
  #   puts tracked.count
  #   # => 0
  #
  #   puts issue.user
  #   puts issue.comments.to_a.size
  #
  #   puts tracked.count
  #   # => 2
  class LoadedAssociations
    attr_reader :resource, :reflections

    def initialize(resource)
      @reflection_methods = {}
      @resource = resource
      @reflections = resource.class.reflections.values
      reset_reflections
    end

    # Public: Count how many reflections are loaded.
    #
    # Returns an integer.
    def count
      loaded_reflections.size
    end

    # Public: Identifies whether or not any reflections were loaded.
    #
    # Returns a Boolean.
    def empty?
      count == 0
    end

    # Public: Pull any loaded reflections out of the list to check.  This is
    # called automatically during initialization so that the previously loaded
    # associations are not counted.
    #
    # Returns nothing.
    def reset_reflections
      @reflections.reject! { |r| reflection_loaded?(r) }
    end

    # Internal: Identifies the reflections that are truly loaded.
    #
    # Returns an Array.
    def loaded_reflections
      @reflections.select { |r| reflection_loaded?(r) }
    end

    # Internal: Identifies the names of the loaded reflections.
    #
    # Returns an Array.
    def loaded_association_names
      loaded_reflections.map(&:name)
    end

    def reflection_loaded?(refl)
      @resource.association(refl.name).loaded? && foreign_key_present?(refl)
    end

    # Internal: Rails treats `nil` foreign keys as loaded. This method determines
    # if there actually is a record associated with an FK.
    #
    # Returns a Boolean.
    def foreign_key_present?(refl)
      !@resource.send(refl.name).nil?
    end
  end

  def track_loaded_associations
    LoadedAssociations.new(self)
  end
end
