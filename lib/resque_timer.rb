# rubocop:disable Style/FrozenStringLiteralComment

require "github/config/stats"

module Resque
  module Plugins
    module Timer
      def around_perform_time(*args)
        GitHub.dogstats.time("resque", tags: ["name:#{name.split("::").last}"]) do
          yield
        end
      end
    end
  end
end
