# rubocop:disable Style/FrozenStringLiteralComment

module CoreDumpContext
  BUFFER = "\0" * 4096

  # the magic marker is written to the buffer in two parts to ensure that it
  # only appears in the process's memory once
  BUFFER[0, 12] = "\nGH_CONTEXT_"
  BUFFER[12, 16] = "UvqsLVzxRg4foMFY"

  def self.set(hash)
    json = hash.map { |k, v| [k, v.to_s] }.to_h.to_json

    # 4096 minus the magic marker length minus the newline terminator:
    max_length = 4096 - 28 - 1

    if json.length > max_length
      json = json[0, max_length]
    end

    BUFFER[28, json.length] = json
    BUFFER[28 + json.length] = "\n"
  end
end
