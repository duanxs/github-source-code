# frozen_string_literal: true

class ContentReference::URI
  def self.host(uri)
    Addressable::URI.parse(uri).host.downcase
  end
end
