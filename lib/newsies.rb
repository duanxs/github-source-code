# rubocop:disable Style/FrozenStringLiteralComment

# Public: This is the module namespace for the legacy GitHub Notifications system.
# See README.md for the big overview.
module Newsies
  NOTIFICATIONS_PER_PAGE = 50

  HANDLER_EMAIL = "email".freeze
  HANDLER_WEB = "web".freeze
  HANDLER_MOBILE_PUSH = "mobile_push".freeze
  HANDLERS = Set.new([HANDLER_WEB, HANDLER_MOBILE_PUSH, HANDLER_EMAIL]).freeze

  SETTINGS_GROUP_PARTICIPATING = "participating".freeze
  SETTINGS_GROUP_SUBSCRIBED = "subscribed".freeze
  SETTINGS_GROUPS = Set.new([SETTINGS_GROUP_PARTICIPATING, SETTINGS_GROUP_SUBSCRIBED]).freeze

  SETTINGS_EMAILS = "emails".freeze
  SETTINGS_EMAILS_GLOBAL = "global".freeze

  SETTINGS_AUTO_SUBSCRIBE = "auto_subscribe".freeze
  SETTINGS_NOTIFY_OWN_VIA_EMAIL = "notify_own_via_email".freeze
  SETTINGS_NOTIFY_COMMENT_EMAIL = "notify_comment_email".freeze
  SETTINGS_NOTIFY_PULL_REQUEST_REVIEW_EMAIL = "notify_pull_request_review_email".freeze
  SETTINGS_NOTIFY_PULL_REQUEST_PUSH_EMAIL = "notify_pull_request_push_email".freeze
  SETTINGS_VULNERABILITY_UI_ALERT = "vulnerability_ui_alert".freeze
  SETTINGS_VULNERABILITY_CLI = "vulnerability_cli".freeze
  SETTINGS_VULNERABILITY_WEB = "vulnerability_web".freeze
  SETTINGS_VULNERABILITY_EMAIL = "vulnerability_email".freeze
  SETTINGS_CONTINUOUS_INTEGRATION_WEB = "continuous_integration_web".freeze
  SETTINGS_CONTINUOUS_INTEGRATION_EMAIL = "continuous_integration_email".freeze
  SETTINGS_CONTINUOUS_INTEGRATION_FAILURES_ONLY = "continuous_integration_failures_only".freeze
  SETTINGS_DIRECT_MENTION_MOBILE_PUSH = "direct_mention_mobile_push".freeze
end

require "newsies/approximate_count"
require "newsies/error"
require "newsies/objects/list"
require "newsies/objects/thread"
require "newsies/objects/comment"
require "newsies/managers/web"
require "newsies/tracked_deliveries"
require "newsies/common_subscription_helper"
require "newsies/slo_helper"
require "newsies/subscriber_set"
require "newsies/subscription"
require "newsies/options"
