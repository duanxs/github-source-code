# frozen_string_literal: true
require "open3"

require "rails/command"
require "rails/test_unit/runner"
require "rails/test_unit/reporter"

module Rails
  module Command
    class TestChangesCommand < Base
      WARN_AFTER_NUMBER_OF_TESTS = 15

      long_desc <<~MESSAGE
        Run tests related to your changes.

        If there are uncommitted changes, find and run related tests.
        Otherwise, find and run tests related to changes made since master.
      MESSAGE

      class_option :copy, aliases: "-c", type: :boolean, desc: "Copy test command to clipboard"
      class_option :since, aliases: "-s", type: :string, desc: "Run tests related to changes since branch (default: master)", banner: "BRANCH_OR_REF"
      class_option :force, type: :boolean, desc: "Don't ask when there are more than #{WARN_AFTER_NUMBER_OF_TESTS} related tests"
      class_option :verbose, aliases: "-v", type: :boolean

      def perform(*)
        $LOAD_PATH << Rails::Command.root.join("test").to_s

        related_tests = find_related_tests
        if related_tests.empty?
          say "No related test files found.", :yellow
          return
        end

        test_arguments = build_test_arguments(related_tests)
        test_command = "bin/rails test #{test_arguments.join(" ")}"

        if options[:copy]
          copy_to_clipboard(test_command)
        else
          say test_command, :blue

          if okay_to_run_tests?(related_tests)
            prepare_argv_for_minitest
            Rails::TestUnit::Runner.parse_options(test_arguments)
            Rails::TestUnit::Runner.run(test_arguments)
          end
        end
      end

      def self.class_options_help(*)
        super
        Minitest.run(%w(--help))
      end

      # Include the description in the banner because later, in class_options_help,
      # printing Minitest options using Minitest.run causes the process to exit.
      def self.banner(command)
        super + indented_description(command)
      end

      def self.indented_description(command)
        indented_description = command.long_description.split("\n").map do |line|
          "  #{line}"
        end

        [
          "\n\nDescription:",
          indented_description
        ].join("\n")
      end

      private

      def build_test_arguments(test_file_paths)
        line_number_index = args.find_index { |arg| arg.match(/:\d+$/) }
        if line_number_index
          line_number = args.delete_at(line_number_index)

          test_file_paths = test_file_paths.map do |test_file_path|
            [test_file_path, line_number].join
          end
        end

        args + test_file_paths
      end

      # Mutate ARGV, removing arguments that this command expects but minitest will complain about if present.
      def prepare_argv_for_minitest
        # args includes everything that wasn't consumed by this command's option parser
        minitest_args = args
        minitest_args << "--verbose" if options[:verbose]
        minitest_args

        ARGV.reject! { |_| true }
        minitest_args.each do |arg|
          ARGV << arg
        end
      end

      def okay_to_run_tests?(related_tests)
        return true if options[:force]
        return true if related_tests.count < WARN_AFTER_NUMBER_OF_TESTS

        yes? "There are #{related_tests.count} related tests. Are you sure you want to run them all?", :yellow
      end

      def find_related_tests
        # Can only be required after `test/`` is added to $LOAD_PATH
        require "test_helpers/github_test/test_finder"

        GitHubTest::TestFinder.related_tests(
          merge_base_ref: options["since"] || "master",
          force_since_merge_base: !!options["since"],
          debug: options["verbose"]
        )
      end

      def copy_to_clipboard(command)
        begin
          _stdout, _stderr, status = Open3.capture3("pbcopy", stdin_data: command)
          say "Command copied to clipboard: ", nil, false # No newline
          say command, :blue
        rescue Errno::ENOENT => e
          say "#{e}"
          say "Failed to copy command to clipboard: ", :red, false # No newline
          say command
        end
      end
    end
  end
end
