# frozen_string_literal: true
#
# WaitForReplication encapsulates the algorithm for waiting until information
# written to the primary database has been replicated to all read replicas. This
# waits with sleeps based on the clock and knowledge of how our replication
# delay system works, and does *not* provide strict guarantees that data will be
# present on any given replica.
#
# There are several assumptions this makes:
#
# * The data has already been written to the primary. That is, this code is
#   being invoked after the data has been COMMITed to the database. Its initial
#   use is in a background job triggered by an after_commit hook.
# * You have the timestamp for when the data was written.
# * Replication delay as reported by freno is globally accurate, that is, any
#   specific replica code might be talking to with ActiveRecord::Base.connected_to(role: :reading) will
#   be delayed no further than reported by freno.
# * Clocks themselves are accurate. This is already an assumption built into the
#   replication delay system (it uses NOW() as reported by replica servers to
#   determine replication delay), and this library builds on top of that by
#   assuming application servers are also using accurate clocks.
#
# The replication delay system works by writing heartbeat timestamps every 100ms
# to the primary database, then using the current replicated value to determine
# if a replica is up to date. Freno uses this relicated heartbeat and `NOW()` to
# determine how far behind a replica is, but only with a maximum accuracy of
# 100ms.
#
# This library uses the replication delay values reported by Freno along with
# the time when write operation happened on the master. It asks Freno for the
# replication delay, and calculates if the time since the write operation
# occurred is higher than that replication delay. If that holds true, the data
# is assumed to be there. Otherwise it waits a reasonable amount of time (up to
# max_wait) and repeats the operation.
#
# Due to the way freno samples the replication delay metrics (each 100ms) the
# value reported by the server can be off by 100ms. As the replication delay
# doesn't grow faster then the clock, the value reported by the freno client
# adds a 100ms cushioning factor to ensure that the value we get can be safely
# compared to the time since the last write, and in turn, to determine that the
# data written is on the replicas. Importantly, this means the replication delay
# values reported by the Freno client will always be at least 100ms, and if this
# algorithm is triggered right after the write operation, it will wait at least
# that amount of time.
#
# Because this code introduces arbitrary sleep waits (up to a maximum value) it
# is not appropriate for scenarios where latency is constrained.
class WaitForReplication

  class DataUnavailable < StandardError
    def initialize(store_name:, wait_required:, max_wait:)
      super("Cannot wait #{wait_required} seconds for replication to catch up on #{store_name}. Maximum allowed is #{max_wait} seconds.")
    end
  end

  # How long we've waited for replication delay.
  attr_reader :waited

  def initialize(last_written_at_timestamp, store_name: :mysql1, max_wait_seconds: 5, default_wait_time: 1)
    @last_written_at = Timestamp.to_time(last_written_at_timestamp)
    @store_name = store_name
    @waited = 0
    @max_wait_seconds = max_wait_seconds
    @default_wait_time = default_wait_time
  end

  # Public: wait for replication delay to catch up.
  #
  # Sleep until the currently measured replication delay is less than the time
  # since the last write, or raise if it's waited too long or if the expected
  # wait is too long to even bother trying.
  #
  # Assuming that replication delay will only recover as fast as the clock,
  # the loop will either sleep the remaining difference, or exit early if
  # the required sleep to catch up exceeds the max_wait value.
  #
  # This may result in unnecessary sleeps in situations where replication delay
  # recovers quickly, but will exit early if replication delay is high and may
  # not recover quickly.
  #
  # Returns the time waited.
  # Raises a DataUnavailable error if the data is or will not be available
  # within the allowed max_wait_time.
  #
  # In case we ask freno for the replication delay and it returns an error
  # we act as if it was a replication delay, sleeping default_wait_time seconds,
  # and we report the error to failbot instead of bubbling up the exception.
  #
  def wait!
    @waited = 0

    time_since_last_write = Time.now - @last_written_at

    loop do
      begin
        replication_delay = Freno.client.replication_delay(store_name: @store_name)
        # first, check to see if the data is there:
        break if replication_delay <= time_since_last_write
        # replication hasn't caught up yet, so we need to wait.
        to_wait = (replication_delay - time_since_last_write)
      rescue Freno::Error => boom
        Failbot.report!(boom, app: "github-freno") if GitHub.environment.fetch("GH_FRENO_UNAVAILABLE", 0) == 0
        GitHub.dogstats.increment("wait_for_replication.freno_error", tags: dogstats_tags)
        to_wait = @default_wait_time
      end

      # but don't bother waiting if delay is too far behind
      if @waited + to_wait > @max_wait_seconds
        GitHub.dogstats.histogram("wait_for_replication.data_unavailable", @waited + to_wait, tags: dogstats_tags)
        raise DataUnavailable.new(store_name: @store_name, wait_required: @waited + to_wait, max_wait: @max_wait_seconds)
      end

      sleep to_wait

      # The actual time elapsed is longer due to processing and the freno check
      # above, but slight pessimism here is fine:
      time_since_last_write += to_wait
      @waited += to_wait
    end

    GitHub.dogstats.histogram("wait_for_replication.waited", @waited, tags: dogstats_tags)
    @waited
  rescue DataUnavailable
    # Record waits for optimistic algorithm even if the end result is an error
    if @waited > 0
      GitHub.dogstats.histogram("wait_for_replication.waited", @waited, tags: dogstats_tags)
    end
    raise
  end

  private

  def dogstats_tags
    @dogstats_tags ||= [
      "store_name:#{@store_name}"
    ]
  end
end
