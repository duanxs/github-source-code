# frozen_string_literal: true

require "forwardable"

module GitAuth
  class Gist
    TRUTHY_VALUES = [1, "1", true]
    extend Forwardable
    attr_reader :id, :user_id, :repo_name, :user_hidden, :is_public, :user_login

    def self.with_name(name)
      id, user_id, repo_name, user_hidden, is_public = ApplicationRecord::Domain::Gists.github_sql.results(<<~SQL, name: name).first
      SELECT id, user_id, repo_name, user_hidden, public
      FROM gists
      WHERE repo_name=:name
      SQL

      user_login = ApplicationRecord::Domain::Users.github_sql.value(<<-SQL, user_id: user_id) if user_id
        SELECT login
        FROM users
        WHERE id=:user_id
      SQL

      new id, user_id, repo_name, user_hidden, is_public, user_login, ::Gist.find_by_repo_name(name)
    end

    delegate [
      :flipper_id, #easy
      :route, # comes from Gist::DGit module
      :shard_path, # DGit
      :dgit_write_routes, # DGit
      :dgit_read_routes, # DGit
      :dgit_delegate, # DGit
      :post_receive_hook_url, #easy
      :above_warn_quota?, #easy, Gist::Quota module
      :above_lock_quota?, #easy, Gist::Quota module
      :available?, #gitrpc
      :access, #rails-y, big
      :human_name, # for password deprecation identification

      # Yeah, yeah.
      :nil?,
    ] => :@gist

    def initialize(id, user_id, repo_name, user_hidden, is_public, user_login, gist)
      @id = id
      @user_id = user_id
      @repo_name = repo_name
      @user_hidden = user_hidden
      @is_public = is_public
      @user_login = user_login
      @gist = gist
    end

    def public?
      TRUTHY_VALUES.include?(is_public)
    end

    def private?
      !public?
    end

    def user_hidden?
      TRUTHY_VALUES.include?(user_hidden)
    end
    alias_method :spammy?, :user_hidden?

    alias_method :gist_id, :repo_name

    def full_name
      "#{user_login}/#{repo_name}"
    end
    alias_method :name_with_owner, :full_name

    def coalesce_dgit_updates?
      false
    end

    def locked?
      false
    end

    def disabled_private?
      false
    end

    def read_only?
      false
    end

    def fork?
      false
    end

    def archived?
      false
    end
  end
end
