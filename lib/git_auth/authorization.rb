# frozen_string_literal: true

require "forwardable"

module GitAuth
  class Authorization
    extend Forwardable

    attr_reader :target, :authentication

    delegate [
      :member, :protocol, :action, :ip, :ssh_ca, :user, :public_key, :token
    ] => :authentication

    def initialize(target:, authentication:)
      @target         = target
      @authentication = authentication
    end

    def maintainer?
      !!@maintainer
    end

    def check
      if anonymous?(member) && GitHub.private_mode_enabled?
        return :anonymous_access_denied if repository.nil?
        return :anonymous_access_denied if target.gist? || target.wiki?
        return :ok if action == :read && repository.anonymous_git_access_enabled?
        return :anonymous_access_denied
      end

      return :missing if anonymous?(member) && repository.nil?

      if anonymous?(member) && action == :write
        # Private
        return :unauthorized_access_to_private_repository if repository.private?

        # Public
        return :no_access_to_spammy_repository if repository.spammy?
        return :no_anonymous_writes
      end

      # Reads
      if anonymous?(member)
        return :unauthorized_access_to_private_repository if !target.gist? && repository.private?
        return :unauthorized_access_to_private_repository if target.gist? && repository.private? && protocol == "git"
        return :no_access_to_spammy_repository if repository.spammy?
        return :tos_violation if repository_access.tos_violation?
        return :sensitive_data_violation if repository_access.sensitive_data_violation?
        return :abusive if repository_access.disabled?
        return :locked if repository.locked?
        return :ok
      end

      if member == :slumlord
        return :missing if repository.nil?

        return :no_access_to_spammy_repository if repository.spammy?

        return :tos_violation if repository_access.tos_violation?
        return :sensitive_data_violation if repository_access.sensitive_data_violation?
        return :abusive if repository_access.disabled?
        return :locked if repository.locked?
        return :disabled if repository.disabled_private?
      end

      if member == :slumlord && action == :write
        return :read_only if repository.read_only?
        return :ok
      end

      # The full-trust service is a special case.
      # It only has to take into account the repo-specific
      # errors. Once we're passed that, we're good to go.
      if member.to_s.start_with?("gitauth-full-trust:")
        return :missing if repository.nil?
        return :missing if repository.spammy?
        return :tos_violation if repository_access.tos_violation?
        return :sensitive_data_violation if repository_access.sensitive_data_violation?
        return :abusive if repository_access.disabled?
        return :locked if repository.locked?
        return :archived if repository.archived?
        return :disabled if repository.disabled_private?
        return :ok
      end

      unless member.is_a?(String)
        raise TypeError, "Invalid member: #{member.inspect}"
      end

      # If we're still here we should either have a user, or this is a valid deploy key.
      if user.nil? && public_key&.repository.nil?
        raise ArgumentError, "auth must be either a user or a deploy key"
      end

      # We've completed the authentication checks, so by this point we either
      # have a user, or this action is being taken by a deploy key.
      # If there is a user, then we can check for things that are
      # unrelated to the repository first.
      return :suspended if user&.suspended?
      return :ofac_sanctioned_user if ofac_applies? && user&.has_any_trade_restrictions?
      return :verified_email_required if action == :write && user&.must_verify_email?

      if member.start_with?("repo:") && GitHub.private_mode_enabled?
        return :invalid_deploy_key if repository.nil?
        return :invalid_deploy_key if repository.is_a?(Repository) && public_key.repository.id != repository.id
      end

      if target.gist?
        return :missing if repository.nil?

        # In private mode, you cannot access gists with deploy keys at all.
        if member.start_with?("repo:") && GitHub.private_mode_enabled?
          return :invalid_deploy_key
        end

        if action == :read
          # Private gists are readable except over unencrypted git://
          return :invalid_protocol_for_secret_gist if repository.private? && protocol == "git"
          return :ok
        end

        # All gists are writable when the user is an actual User and has the
        # `update_gist` Egress role on the Gist
        return :ok if user && AccessControl.new(viewer: user).can_update_gist?(repository)
        return :unauthorized_access_to_secret_gist if repository.private?
        return :unauthorized_write_to_gist
      end

      # run the check gamut
      case
      when repository.nil?
        # no repository found matching the given path
        :missing
      when repository.spammy? && !repository.resources.contents.writable_by?(user)
        # spammy repos should appear as missing for non-members
        :no_access_to_spammy_repository
      when user && !repository.resources.contents.readable_by?(user)
        :unauthorized_access_to_private_repository

      # The following group of error statuses are "no matter what" statuses.
      # However, the error message depends on whether or not you're allowed
      # to know about the repo or not, so it must come after the check for 'unpullable'.
      when repository_access.tos_violation?
        # TOS-violating are neither writable nor readable no matter what
        :tos_violation
      when repository_access.sensitive_data_violation?
        # Repos disabled for sensitive data policy violations are neither writable nor readable no matter what
        :sensitive_data_violation
      when repository_access.trademark_violation?
        # Repos disabled for trademark policy violations are neither writable nor readable no matter what
        :trademark_violation
      when repository_access.disabled?
        # abusive repos are neither writable nor readable no matter what
        :abusive
      when repository.locked?
        :locked
      when action == :write && repository.archived?
        # archived repositories aren't writable by anyone
        :archived
      when repository.disabled_private?
        :disabled
      when ofac_applies? && repository.has_any_trade_restrictions?
        if member.start_with?("repo:") # is this a deploy key?
          :ofac_sanctioned_repository
        elsif repository.network_owner.organization? && repository.network_owner.adminable_by?(user)
          :admin_of_ofac_sanctioned_organization
        elsif repository.network_owner.organization?
          :collaborator_in_ofac_sanctioned_organization
        else
          :collaborator_on_ofac_sanctioned_repository
        end
      when action == :read && repository.public?
        # public repositories are world readable
        :ok

      # This is a very long condition, but until I get the entire :check method
      # refactored I do not want to extract it into a helper method.
      # I promise it will be worth it. -kytrinyx
      when GitHub.oauth_application_policies_enabled? && public_key&.created_by_unknown? && repository.policymaker.restricts_oauth_applications?
        # The repository's OAuth application policy blocks access to some
        # apps, and we don't know who created the key that is being used to
        # access the repository (e.g., was it created by a blocked app?).
        :oap_denied_key_unknown_origin
      when public_key_app_blocked_by_oauth_app_policy?
        if repository.private?
          :unauthorized_access_to_private_repository
        else
          # The repository's OAuth application policy blocks access to the app
          # that created the key or OAuth token that is being used to access the
          # repository.
          :oap_denied_app
        end
      when member_app_blocked_by_oauth_app_policy?
        if repository.private?
          :unauthorized_access_to_private_repository
        else
          # The repository's OAuth application policy blocks access to the app
          # that created the key or OAuth token that is being used to access the
          # repository.
          :oap_denied_app
        end
      when member.start_with?("repo:")
        # If we reach this branch, we're either performing a read on a private repository, or a write.
        #
        # We have previously verified that the deploy key is internally consistent
        # with the 'member' value passed: the repo in the member matches the deploy
        # key's repo.
        #
        # Now we have to check that this deploy key is actually allowed to access
        # the target repository.
        if public_key.repository.id == repository.id
          if !ip_whitelisting_policy_satisfied?(repository)
            :not_ip_whitelisted
          else
            :ok
          end
        elsif repository.private?
          :unauthorized_access_to_private_repository
        else
          :invalid_deploy_key
        end
      when action == :write && repository.read_only?
        :read_only
      # writing to public repos and reading/writing from private repos
      # requires a real access check.
      when member_can?(repository)
        if !repository.in_organization?
          # If you're authenticating with an SSH certificate, then
          # you can't access user-owned repos.
          return :bad_ssh_ca if ssh_ca
          return :ok
        end

        return :not_ip_whitelisted if !ip_whitelisting_policy_satisfied?(repository)

        # If it's an installed GitHub App, then the abilities check
        # has handled the details.
        # Also, SSH cert requirements don't apply.
        # Also, SAML requirements don't apply.
        return :ok if user.bot?

        # If it's an OAuth app, then the OAP check will have
        # kicked it out by now.
        # Also, SSH cert requirements don't apply.
        # Also, SAML requirements don't apply.
        # If they authorized via OAuth via a GitHub App,
        # then presumably it's handled via abilities.
        return :ok if public_key&.created_by_oauth_application?

        # As of November 2019 we changed the rules about SAML
        # enforcement.
        #
        # Before this date all OAuth Apps and GitHub App
        # user to server authorizations ignored all SAML
        # requirements. They forcibly bypassed the SAML
        # protections on Organizations.
        #
        # Any OAuth App tokens and GitHub App user to
        # server authorizations created after that date
        # need to be whitelisted explicitly by the organization,
        # thereby respecting the SAML requirements.
        #
        # This makes this _way_ more complicated but is necessary
        # for security purposes and to not break existing credentials.
        if (oauth_access = user.oauth_access)
          return :ok unless oauth_access.saml_enforceable?
        end

        if repository.ssh_certificate_requirement_enabled? && !ssh_ca
          return :ssh_certificate_required
        end

        if ssh_ca && !ssh_ca.owned_by_repo_owner?(repository)
          return :bad_ssh_ca
        end
        return :ok if ssh_ca

        if saml_enforced?(repository) && !saml_credential_present?(repository)
          return :credential_authorization_missing
        end

        :ok
      when repository.parent && target.normal_repo? && repository.public? && !repository.in_organization? && member_can?(repository.parent)
        @maintainer = true

        if !repository.parent.in_organization?
          # If you're authenticating with an SSH certificate, then
          # you can't access user-owned repos.
          return :bad_ssh_ca if ssh_ca
          return :ok
        end

        return :not_ip_whitelisted if !ip_whitelisting_policy_satisfied?(repository.parent)

        # If it's an installed GitHub App, then the abilities check
        # has handled the details.
        # Also, SSH cert requirements don't apply.
        # Also, SAML requirements don't apply.
        if user.bot?
          # Log to Splunk for troubleshooting.
          payload = {
            index: "rails-gitauth",
            code_path: "maintainer_via_app",
            type: "server-to-server",
            app_name: user&.integration&.slug,
            repo: repository.name_with_owner,
          }
          ::GitHub::Logger.log(payload)
          return :ok
        end

        if GitHub.oauth_application_policies_enabled? && public_key&.created_by_unknown?
          GitHub.dogstats.increment("gitauth.maintainer_via_app", tags: ["type:pubkey_unknown_origin"])
          # The repository's OAuth application policy blocks access to some
          # apps, and we don't know who created the key that is being used to
          # access the repository (e.g., was it created by a blocked app?).
          return :oap_denied_key_unknown_origin if repository.parent.policymaker.restricts_oauth_applications?
        end

        if GitHub.oauth_application_policies_enabled? && oauth_application
          return :oap_denied_app if OauthApplicationPolicy::Application.new(repository.parent, oauth_application).violated?
        end

        # If it's an OAuth app, then SSH cert requirements don't apply.
        # Also, SAML requirements don't apply.
        if oauth_application
          # Log to Splunk for troubleshooting.
          payload = {
            index: "rails-gitauth",
            code_path: "maintainer_via_app",
            type: oauth_application.is_a?(Integration) ? "user-to-server" : "oauth",
            app_name: oauth_application.name,
            repo: repository.name_with_owner,
          }
          ::GitHub::Logger.log(payload)
          return :ok
        end

        if repository.parent.ssh_certificate_requirement_enabled? && !ssh_ca
          return :ssh_certificate_required
        end

        if ssh_ca && !ssh_ca.owned_by_repo_owner?(repository.parent)
          return :bad_ssh_ca
        end
        return :ok if ssh_ca

        if saml_enforced?(repository.parent) && !saml_credential_present?(repository.parent)
          return :credential_authorization_missing
        end

        :ok
      when repository.private?
        :unauthorized_access_to_private_repository
      else
        :access_denied_to_user
      end
    end

    private

    def repository
      target.repository
    end

    def anonymous?(member)
      member == :anonymous
    end

    def ofac_applies?
      GitHub.billing_enabled? && !repository.nil? && repository.private?
    end

    def repository_access
      @repository_access ||= repository.access
    end

    def oauth_application
      if public_key && public_key.created_by_oauth_application?
        public_key.oauth_application
      elsif (access = user.try(:oauth_access))
        access.application
      else
        nil
      end
    end

    def public_key_app_blocked_by_oauth_app_policy?
      return false unless GitHub.oauth_application_policies_enabled?
      return false unless public_key&.created_by_oauth_application?

      OauthApplicationPolicy::Application.new(repository, public_key.oauth_application).violated?
    end

    def member_app_blocked_by_oauth_app_policy?
      return false unless GitHub.oauth_application_policies_enabled?
      return false unless user&.oauth_access&.application

      OauthApplicationPolicy::Application.new(repository, user.oauth_access.application).violated?
    end

    def ip_whitelisting_policy_satisfied?(repository)
      IpWhitelistingPolicy.new(
        owner: repository.owner,
        ip: ip,
        repository: repository,
        action: action,
        actor: user || public_key,
      ).satisfied?
    end

    def saml_enforced?(repo)
      org = repo.organization

      provider_owner = org.external_identity_session_owner
      enforcement_policy = case provider_owner
      when ::Organization
        Organization::SamlEnforcementPolicy.new(organization: org, user: user)
      when ::Business
        Business::SamlEnforcementPolicy.new(business: provider_owner, organization: org, user: user)
      end

      enforcement_policy.enforced?
    end

    def saml_credential_present?(repo)
      # At this point, either SAML is enforced or the user has opted into SAML
      # before enforcement. They must have a whitelisted PAT or PublicKey now.
      org = repo.organization

      provider_owner = org.external_identity_session_owner

      orgs = if provider_owner.is_a?(::Business)
        provider_owner.organizations.to_a
      else
        [org]
      end

      # to access an internal repository, the token needs to be authorized for any org in the same business as the repo's org
      if token.present?
        credential = OauthAccess.find_by_token(token)
        !!Organization::CredentialAuthorization.authorization(organization: orgs, credential: credential)
      elsif public_key
        public_key.authorization_active_for_orgs?(orgs)
      else
        false
      end
    end

    class AccessControl < Platform::Authorization::Permission
      attr_reader :env
      def initialize(context)
        context[:origin] = Platform::ORIGIN_API
        @env = {}
        super
      end

      def graphql_request?
        false
      end

      def repository_accessible?(action, repository)
        action =
          case action
          when :read  then :pull
          when :write then :push
          else
            return false
          end
        access_allowed?(action,
          resource: repository,
          current_repo: repository,
          current_org: nil,
          allow_integrations: true,
          allow_user_via_integration: true,
          raise_on_error: false,
          enforce_oauth_app_policy: false,
        )
      end

      def can_update_gist?(gist)
        access_allowed?(:update_gist,
          resource: gist,
          current_repo: nil,
          current_org: nil,
          allow_integrations: false,
          allow_user_via_integration: false,
          raise_on_error: false,
          enforce_oauth_app_policy: false,
        )
      end
    end

    def member_can?(repo)
      Platform::Security::RepositoryAccess.with_viewer(user) do
        AccessControl.new(viewer: user).repository_accessible?(action, repo)
      end
    end
  end
end
