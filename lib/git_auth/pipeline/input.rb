# frozen_string_literal: true

module GitAuth
  class Pipeline
    class Input
      attr_reader :protocol, :action, :member, :password, :key, :repository, :owner_name, :target_path, :ip, :original_user_agent, :request_id
      def initialize(protocol:, action:, member:, password:, key:, target:, ip:, original_user_agent:, request_id:)
        @protocol   = protocol
        @action     = action
        @member     = member
        @password   = password
        @key        = key
        @repository = target.repository
        @owner_name = target.owner_name
        @ip         = ip

        # For logging purposes only
        @target_path = target.path
        @original_user_agent  = original_user_agent
        @request_id  = request_id
      end
    end
  end
end
