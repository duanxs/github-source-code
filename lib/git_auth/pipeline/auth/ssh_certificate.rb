# frozen_string_literal: true

module GitAuth
  class Pipeline
    module Auth
      class SSHCertificate < AuthenticationMethod
        def applicable?
          input.protocol == "ssh" && certificate?
        end

        def process
          status, user_login, ssh_ca = GitAuth::SSHCertificateAuthority.validate_certificate(
            input.key,
            ip: input.ip,
          )

          result.ssh_ca = ssh_ca # Used in error messages.

          return result.fail_with :ssh_error if status != :ok
          return result.fail_with :ssh_error if ssh_ca.nil?

          result.member = user_login

          result.success!
        end

        private

        def certificate?
          algo, _, _ = SSHData.key_parts(input.key)
          SSHData::Certificate::ALGOS.include?(algo)
        rescue SSHData::Error => e
          Failbot.report(e)
          false
        end
      end
    end
  end
end
