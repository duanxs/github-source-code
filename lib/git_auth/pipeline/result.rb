# frozen_string_literal: true

module GitAuth
  class Pipeline
    class Result
      attr_accessor :member, :ssh_ca, :public_key, :token
      attr_writer :user
      attr_reader :status, :input
      def initialize(input)
        @input  = input
        @member = input.member
        @status = :inconclusive
      end

      def user
        @user || look_up_user
      end

      def look_up_user
        return if member.is_a?(Symbol) # don't try looking up for slumlord or anonymous
        return if member.include?(":") # don't try looking up if member indicates custom auth types
        return unless GitHub::UTF8.valid_unicode3?(member)

        User.find_by(login: member)
      end

      def failed?
        ![:ok, :inconclusive].include?(status)
      end

      def succeeded?
        status == :ok
      end

      def inconclusive?
        status == :inconclusive
      end

      def fail_with(status)
        @status = status
      end

      def success!
        @status = :ok
      end
    end
  end
end
