# frozen_string_literal: true

module GitAuth
  # For now, return nil if no record is found,
  # to match the behavior of the existing Rails
  # associations.
  #
  # Note that this class uses ApplicationRecord::Domain in order to
  # avoid relying on ActiveRecord. This is the first step in decoupling
  # gitauth from the github/github, and the lowest level of raw SQL
  # that is currently acceptable in the github/github codebase.
  class Login
    def self.find(id)
      return if id.nil?

      login = ApplicationRecord::Domain::Users.github_sql.value(<<-SQL, user_id: id)
        SELECT login
        FROM users
        WHERE id=:user_id
      SQL

      new(id: id, login: login) if login
    end

    attr_reader :id, :login
    def initialize(id:, login:)
      @id = id
      @login = login
    end
  end
end
