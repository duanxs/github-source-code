# frozen_string_literal: true

require "forwardable"

module GitAuth
  # Figure out whether the actor is allowed to perform the given git operation
  # on the repository at `path`.
  class Access
    extend Forwardable

    attr_reader :action, :country, :key, :member, :password, :protocol, :ip, :target, :original_user_agent, :request_id, :status

    delegate [:public_key, :user] => :authentication

    # Verify member access to the repository to perform action. This includes
    # all the checks provided by the #check method.
    #
    # path       - the path to the repository.
    #              - "<user>/<repo>.git" for normal user repositories.
    #              - "<user>/<repo>.wiki.git" for wiki repositories.
    #              - "<gist-id>.git" or "gist/<gist-id>.git" for gists.
    # ip         - The IP address that permissions check is coming from.
    # member     - String username, "<user>/<repo>" deploy key,
    #              "user:<id>", or "repo:<id>" deploy key strings to test. May be
    #              nil or :anonymous to indicate anonymous access. May be
    #              :slumlord to indicate slumlord access.
    # action     - Either :read or :write.
    # protocol   - The protocol access is being requested from. Must be one of
    #              http, git, ssh, or svn.
    # country    - The country that the request is being made from.
    # key        - The SSH public key used to authenticate.
    # password   - The user's password.
    # original_user_agent - The requesting useragent (typically the client's git version)
    # request_id  - The request identifier, allowing us to log actions that stem
    #              from the same request.
    def initialize(path:, ip:, member:, action:, protocol:, country: nil, key: nil, password: nil, original_user_agent: nil, request_id: nil)
      @target     = GitAuth::Target.new(path)
      @ip         = ip
      @member     = member
      @action     = action.to_sym
      @protocol   = protocol
      @country    = country
      @original_user_agent = original_user_agent
      @request_id = request_id
      @key        = key
      @password   = password
    end

    # Verify member access to the repository to perform action. This includes
    # all the checks provided by the #check method.
    def verify
      validate_inputs

      @status = verify_instance
      @status ||= verify_target

      # First authenticate.
      authentication.process unless status
      @status = authentication.status if authentication.failed?

      # Then authorize.
      @status ||= authorization.check
      @status ||= :missing

      perform_bookkeeping

      if status == :ok
        # Return early for the happy path.
        return [status, "#{target.host}:#{target.full_path}"] if target.host
      end

      [status, failure.message]
    end

    def authentication
      return @authentication if @authentication

      inputs = {
        protocol: protocol,
        action: action,
        member: member,
        password: password,
        key: key,
        target: target,
        ip: ip,
        original_user_agent: original_user_agent,
        request_id: request_id,
      }
      @authentication = GitAuth::Pipeline.new(**inputs)
    end

    def authorization
      @authorization ||= GitAuth::Authorization.new(target: target, authentication: authentication)
    end

    # Hash of stats about the context of the permissions request. These are
    # relayed through the different git protocols stacks so they're available to
    # hooks and monitor programs.
    def stats
      stats = { }
      stats[:frontend] = Socket.gethostname
      stats[:frontend_pid] = Process.pid
      stats[:frontend_ppid] = Process.ppid
      stats[:committer_date] = Time.now.strftime("%s %z")

      if repository
        stats[:repo_id] = repository.id
        stats[:repo_public] = repository.public? if repository.respond_to?(:public?)
        stats[:repo_name] = "gist/#{repository.gist_id}" if repository.respond_to?(:gist_id)
        stats[:repo_name] = repository.name_with_owner if repository.respond_to?(:name_with_owner)
        stats[:repo_redirect] = target.name_with_owner if target.redirected?
        stats[:repo_config] = configuration.to_json

        if GitHub.anonymous_git_access_enabled? && repository.respond_to?(:anonymous_git_access_enabled?)
          stats[:repo_anonymous_access_enabled] = repository.anonymous_git_access_enabled?
        end

        # Set the following stats only for pushes as they require potentially
        # expensive database calls or memcache queries that we prefer to omit
        # when not strictly necessary.
        if action == :write
          if repository.respond_to?(:has_pre_receive_hooks?) && repository.has_pre_receive_hooks?
            data = repository.pre_receive_hooks.to_json
            data_limit = GitHub::PreReceiveHookEntry::HOOK_STATS_LIMIT

            if data.bytesize > data_limit
              stats[:repo_pre_receive_hooks] = GitHub::PreReceiveHookEntry::OVERFLOW
            else
              stats[:repo_pre_receive_hooks] = data
            end
          end

          unless target.gist?
            # We only check the LFS integrity in repos that have at least one
            # Git LFS object successfully pushed to GitHub. This way we avoid
            # the scanning overhead for the majority of repos that do not use
            # Git LFS.
            #
            # This heuristic has a drawback:
            # If a repo has never successfully received a single Git LFS
            # object, then we would never scan it and never report the
            # error. That also means we would never scan a repo that uses
            # external Git LFS storage (e.g. Artifactory) which is good.
            #
            # This DB call is usually made right after new Media::Blobs have
            # been pushed. In order to reduce the risk of races we perform the
            # query explicitly not against the read-only replica.
            stats[:check_lfs_integrity] = Media::Blob.exists?(repository_network_id: repository.network_id)
          end

          stats[:use_http_postrx] = true unless GitHub.enterprise?
        end

        # Advertise the tips of non-gist repositories part of a fork network.
        if repository.fork? && repository.advertise_alternate_tips?
          stats[:parent_repo_id] = repository.parent_id
        end
      end

      if user
        stats[:user_id] = user.id
        stats[:user_login] = user.login
        stats[:user_operator_mode] = (user.has_operator_mode?(configuration) || GitHub.global_operator_mode_enabled?)

        if user&.oauth_access.present?
          stats[:oauth_access_id] = user.oauth_access.id
        end

        if user.respond_to?(:installation)
          stats[:installation_id] = user.installation.id
          stats[:installation_type] = user.installation.class.to_s
        end

        if protocol == "svn"
          stats["user_author_name"] = user.git_author_name
          stats["user_author_email"] = user.git_author_email
          stats["user_time_zone"] = user.time_zone_name || "Etc/UTC"
        end
      end

      if public_key
        stats[:pubkey_id] = public_key.id
        stats[:pubkey_fingerprint] = public_key.fingerprint
        if (verifier = public_key.verifier) && verifier != user
          stats[:pubkey_verifier_id] = verifier.id
          stats[:pubkey_verifier_login] = verifier.login
        end
        if (creator = public_key.creator) && creator != verifier
          stats[:pubkey_creator_id] = creator.id
          stats[:pubkey_creator_login] = creator.login
        end
      end

      if authentication&.ssh_ca
        stats[:ssh_ca_id] = authentication.ssh_ca.id
        stats[:ssh_ca_owner_type] = authentication.ssh_ca.owner_type
        stats[:ssh_ca_owner_id] = authentication.ssh_ca.owner_id
      end

      stats["maintainer"] = true if authorization&.maintainer?

      stats["above_warn_quota"] = repository.above_warn_quota?
      stats["above_lock_quota"] = repository.above_lock_quota?
      stats["quotas_enabled"] = GitHub.repository_quotas_enabled?
      stats["gitauth_version"] = GitHub.current_sha[0..7]

      stats
    end

    private

    def failure
      @failure ||= GitAuth::Failure.new(status: status, authentication: authentication, authorization: authorization, ssh_enabled: ssh_enabled?, country: country)
    end

    def validate_inputs
      if ![:read, :write].include?(action)
        raise ArgumentError, "action must be :read or :write"
      end

      if protocol == "ssh" && !member.to_s.start_with?("user:", "repo:")
        raise ArgumentError, "invalid member #{member.inspect} with SSH protocol"
      end

      if protocol != "ssh" && member.to_s.start_with?("user:", "repo:")
        raise ArgumentError, "invalid member #{member.inspect} with #{protocol} protocol"
      end

      if protocol == "git" && action == :write
        raise ArgumentError, "cannot write with git protocol"
      end

      if member == :slumlord && action == :read
        raise ArgumentError, "slumlord should only perform writes"
      end

      if member.to_s.start_with?("gitauth-full-trust:")
        raise ArgumentError, "potential injection attack: member #{member.inspect}"
      end

      if protocol == "ssh" && key.nil?
        raise ArgumentError, "ssh protocol must be used with an SSH key or certificate"
      end
    end

    def perform_bookkeeping
      unless public_key.nil?
        # Do some book-keeping to track that the key was used.
        PublicKey.access(id: public_key.id, last_accessed_at: public_key.accessed_at)
      end

      GitHub.dogstats.increment("git", tags: ["type:#{protocol}", "action:#{action}", "status:#{status}"])
      if status == :verified_email_required
        GitHub.dogstats.increment("git_access_blocked", tags: ["type:verified_email_required"])
      end
      if status == :ok && authorization&.maintainer?
        GitHub.dogstats.increment("git", tags: ["action:maintainer_pushed"])
      end
      if status == :ok && action == :write && public_key&.deploy_key?
        GitHub.dogstats.increment("git", tags: ["action:deploy_key_pushed"])
      end
    end

    def verify_instance
      # check for maintenance mode first, in which case we want to avoid any kind of
      # mysql or external service access entirely
      return :maintenance if maintenance_mode?

      if GitHub.enterprise? && action == :write && GitHub::Enterprise.license.expired?
        return :license
      end

      if GitHub.private_instance? && !GitHub.private_instance_bootstrapper.bootstrapped?
        return :unbootstrapped
      end
    end

    def verify_target
      return :invalid unless target.valid?
      return :ssh_disabled if protocol == "ssh" && !ssh_enabled?
      return :dmca if !repository.nil? && repository.access&.dmca?
      return :country_block if !repository.nil? && repository.access&.country_block?(country)
    end

    def configuration
      @configuration ||= repo_config || default_config
    end

    def repo_config
      # Gists do not have a config object.
      repository.config.to_hash if repository.respond_to?(:config)
    end

    def default_config
      GitHub.enterprise? ? GitHub.config.to_hash : {}
    end

    def ssh_enabled?
      configuration["ssh_enabled"].to_s != "false"
    end

    def repository
      target.repository
    end

    def maintenance_mode?
      File.exist?("#{Rails.root}/public/system/maintenance-git.html")
    end
  end
end
