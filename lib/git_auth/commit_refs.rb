# frozen_string_literal: true

module GitAuth
  class CommitRefs
    PERMISSION_DENIED = "permission denied"
    HOOK_DECLINED = "pre-receive hook declined"
    attr_reader :body

    def initialize(body)
      @body = body
    end

    def git_sockstat
      @git_sockstat ||= GitHub::GitSockstat.parse(body.sockstat)
    end

    def actor
      @actor ||= commit_refs_actor_from_ctx(body.commit_ref_ctx)
    end

    def process
      return not_found_payload if target.repository.nil?
      return hook_declined_payload if body.pre_receive_hook_result != "ok"

      git_sockstat.register(actor)

      ## Update phase
      # Invoke custom update ref hook
      # update ref file system hooks are not supported

      if target.normal_repo?
        check_maintainer_ref_access
        check_protected_branches
      else
        refs.each(&:allow!)
      end

      if refs.any?(&:allow?)
        info = tpc.commit(allowed_refs.map(&:payload), git_sockstat.to_h)
        info[:refs_status].each do |refname, message|
          refs[refname].message = message
        end
        # set messages on refs if they didn't get set above
        if info[:err].nil?
          allowed_refs.each(&:ok!)
        else
          allowed_refs.each(&:failed!)
        end
      elsif target.normal_repo?
        # Update the target repository's pushed count in case all ref updates
        # were denied. If no ref updates happened, the post-receive hooks won't
        # fire and no network maintenance will be triggered.
        #
        # By updating the pushed count here, we can in turn trigger repository
        # network maintenance to run.
        target.repository.network.increment_pushed_counts
      end
      {
        "ok"   => true,
        "err"  => refs.error_message,
        "refs" => refs.payload,
      }
    end
    private

    def not_found_payload
      {
        "ok"   => false,
        "err"  =>"Repository not found.",
        "refs" => {},
      }
    end

    def hook_declined_payload
      {
        "ok"   => true, # babeld ignores per-ref failures when "ok" is false
        "err"  => "",
        "refs" => Hash[refs.map { |ref| [ref.encoded_name, HOOK_DECLINED] }],
      }
    end

    def check_maintainer_ref_access
      refs.each do |ref|
        if git_sockstat.value("maintainer") && actor
          # The pushable_by? query is pretty expensive, and
          # we are running it within a loop.
          # We could consider flattening the query.
          # However, this is the case where a maintainer is
          # pushing to a PR, so we do not expect them to push
          # more than one ref, so it seems fine.
          pushable_by = target.repository.pushable_by?(actor, ref: ref.decoded_name)

          if !pushable_by
            ref.disallow! PERMISSION_DENIED
            next
          elsif ref.after == GitHub::NULL_OID
            # maintainers can't delete the contributor's branch
            ref.disallow! PERMISSION_DENIED
            next
          end
        end
      end
    end

    def ref_update_objects
      refs.select(&:undecided?).map do |ref|
        Git::Ref::Update.new(
          repository: target.repository,
          refname: ref.decoded_name,
          before_oid: ref.before,
          after_oid: ref.after,
          fast_forward: ref.status == "ff" ? true : (ref.status == "nf" ? false : nil),
        )
      end
    end

    def allowed_refs
      refs.select(&:allow?)
    end

    def policy_decisions
      RefUpdatesPolicy.check(target.repository, ref_update_objects, actor, atomic: body.capabilities[:atomic])
    end

    def check_protected_branches
      policy_decisions.each do |decision|
        ref = refs[decision.ref_update.refname]

        if decision.allowed?
          ref.allow!
        else
          ref.disallow! decision.short_message
          ref.error = decision.long_message if decision.long_message
        end
      end
    end

    def target
      @target ||= GitAuth::Target.new(body.path)
    end

    def refs
      @refs ||= GitAuth::Refs.new(body.refs)
    end

    def tpc
      GitHub::DGit.update_refs_coordinator(target.wiki? ? target.repository.unsullied_wiki : target.repository)
    end

    # SAFELY DEPLOYING CHANGES:
    #
    # If making changes here and in #commit_refs_pack_ctx please consider
    # deploying separately. Requests to _git_auth and _commit_refs may happen
    # on different hosts, with different deploys. So changes may need to be
    # staged over two deploys.
    #
    # Returns an actor, or nil.
    def commit_refs_actor_from_ctx(ctx_json)
      return if ctx_json.nil?
      ctx = GitHub::JSON.parse(ctx_json)
      auth_type = ctx.key?("auth_type") && ctx["auth_type"].to_sym

      if auth_type == :bot && ctx.key?("installation_id") && ctx.key?("installation_type")
        # Fetching the Bot through the IntegrationInstallation or
        # ScopedIntegrationInstallation hydrates it with the repository
        # installation which is the abilities delegate.
        installation =
          case ctx["installation_type"]
          when "IntegrationInstallation"
            IntegrationInstallation.find(ctx["installation_id"].to_i)
          when "ScopedIntegrationInstallation"
            ScopedIntegrationInstallation.find(ctx["installation_id"].to_i)
          end
        return installation.bot
      elsif ctx.key?("user_id")
        user = User.find(ctx["user_id"].to_i)
        if ctx.key?("oauth_access_id")
          # Re-attach the oauth access that was used to authenticate so that we
          # can check the scopes used later in RefUpdatesPolicy.
          user.oauth_access = OauthAccess.find(ctx["oauth_access_id"].to_i)
        end
        return user
      elsif ctx.key?("pubkey_id")
        return PublicKey.find(ctx["pubkey_id"].to_i)
      elsif ctx.key?("slumlord") && ctx["slumlord"] == "true"
        return :slumlord
      else
        Failbot.push ctx_json: ctx_json.inspect
        raise NoActorError, "context missing installation_id, installation_type, user_id and pubkey_id"
      end
    rescue ActiveRecord::RecordNotFound
      Failbot.push ctx_json: ctx_json.inspect
      raise NoActorError, "record was deleted before lookup could complete"
    end
  end
end
