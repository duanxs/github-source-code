# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/advisory_db/v0/repository_advisory_curation_request.proto

require 'google/protobuf'

require 'hydro/schemas/github/v1/entities/user_pb'
require 'hydro/schemas/advisory_db/v0/entities/repository_advisory_content_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.advisory_db.v0.RepositoryAdvisoryCurationRequest" do
    optional :actor, :message, 1, "hydro.schemas.github.v1.entities.User"
    optional :repository_advisory_content, :message, 2, "hydro.schemas.advisory_db.v0.entities.RepositoryAdvisoryContent"
  end
end

module Hydro
  module Schemas
    module AdvisoryDb
      module V0
        RepositoryAdvisoryCurationRequest = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.advisory_db.v0.RepositoryAdvisoryCurationRequest").msgclass
      end
    end
  end
end
