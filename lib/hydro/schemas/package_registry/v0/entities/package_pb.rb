# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/package_registry/v0/entities/package.proto

require 'google/protobuf'

require 'google/protobuf/wrappers_pb'
require 'hydro/schemas/github/v1/entities/organization_pb'
require 'hydro/schemas/github/v1/entities/repository_pb'
require 'hydro/schemas/github/v1/entities/user_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.package_registry.v0.entities.Package" do
    optional :id, :uint64, 1
    optional :global_id, :message, 2, "google.protobuf.StringValue"
    optional :name, :string, 3
    optional :registry_type, :enum, 4, "hydro.schemas.package_registry.v0.entities.Package.RegistryType"
    optional :owner_user, :message, 5, "hydro.schemas.github.v1.entities.User"
    optional :owner_org, :message, 6, "hydro.schemas.github.v1.entities.Organization"
    optional :total_size, :message, 7, "google.protobuf.Int64Value"
    optional :version_count, :message, 8, "google.protobuf.Int64Value"
    optional :repository, :message, 9, "hydro.schemas.github.v1.entities.Repository"
    optional :visibility, :enum, 10, "hydro.schemas.package_registry.v0.entities.Package.Visibility"
    optional :owner_id, :uint64, 11
    optional :owner_global_id, :message, 12, "google.protobuf.StringValue"
  end
  add_enum "hydro.schemas.package_registry.v0.entities.Package.RegistryType" do
    value :UNKNOWN, 0
    value :NPM, 1
    value :RUBYGEMS, 2
    value :DOCKER, 3
    value :NUGET, 4
    value :MAVEN, 5
    value :PYPI, 6
  end
  add_enum "hydro.schemas.package_registry.v0.entities.Package.Visibility" do
    value :VISIBILITY_UNKNOWN, 0
    value :PUBLIC, 1
    value :PRIVATE, 2
    value :INTERNAL, 3
  end
end

module Hydro
  module Schemas
    module PackageRegistry
      module V0
        module Entities
          Package = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.package_registry.v0.entities.Package").msgclass
          Package::RegistryType = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.package_registry.v0.entities.Package.RegistryType").enummodule
          Package::Visibility = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.package_registry.v0.entities.Package.Visibility").enummodule
        end
      end
    end
  end
end
