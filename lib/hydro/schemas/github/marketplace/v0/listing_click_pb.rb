# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/marketplace/v0/listing_click.proto

require 'google/protobuf'

require 'hydro/schemas/github/v1/entities/request_context_pb'
require 'hydro/schemas/github/v1/entities/user_pb'
require 'hydro/schemas/github/marketplace/v0/entities/marketplace_listing_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.marketplace.v0.ListingClick" do
    optional :request_context, :message, 1, "hydro.schemas.github.v1.entities.RequestContext"
    optional :user, :message, 2, "hydro.schemas.github.v1.entities.User"
    optional :marketplace_listing, :message, 3, "hydro.schemas.github.marketplace.v0.entities.MarketplaceListing"
    optional :location, :string, 4
    optional :source_url, :string, 5
    optional :destination_url, :string, 6
  end
end

module Hydro
  module Schemas
    module Github
      module Marketplace
        module V0
          ListingClick = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.marketplace.v0.ListingClick").msgclass
        end
      end
    end
  end
end
