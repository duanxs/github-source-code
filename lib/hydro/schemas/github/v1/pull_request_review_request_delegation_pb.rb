# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/v1/pull_request_review_request_delegation.proto

require 'google/protobuf'

require 'hydro/schemas/github/v1/entities/organization_pb'
require 'hydro/schemas/github/v1/entities/team_pb'
require 'hydro/schemas/github/v1/entities/repository_pb'
require 'hydro/schemas/github/v1/entities/pull_request_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.v1.PullRequestReviewRequestDelegation" do
    optional :organization, :message, 1, "hydro.schemas.github.v1.entities.Organization"
    optional :team, :message, 2, "hydro.schemas.github.v1.entities.Team"
    optional :repository, :message, 3, "hydro.schemas.github.v1.entities.Repository"
    optional :pull_request, :message, 4, "hydro.schemas.github.v1.entities.PullRequest"
    optional :algorithm, :enum, 5, "hydro.schemas.github.v1.PullRequestReviewRequestDelegation.Algorithm"
  end
  add_enum "hydro.schemas.github.v1.PullRequestReviewRequestDelegation.Algorithm" do
    value :ROUND_ROBIN, 0
    value :LOAD_BALANCE, 1
  end
end

module Hydro
  module Schemas
    module Github
      module V1
        PullRequestReviewRequestDelegation = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.PullRequestReviewRequestDelegation").msgclass
        PullRequestReviewRequestDelegation::Algorithm = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.PullRequestReviewRequestDelegation.Algorithm").enummodule
      end
    end
  end
end
