# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/v1/similar_issues_show_hide_results_click.proto

require 'google/protobuf'

require 'hydro/schemas/github/v1/entities/request_context_pb'
require 'hydro/schemas/github/v1/entities/repository_pb'
require 'hydro/schemas/github/v1/entities/user_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.v1.SimilarIssuesShowHideResultsClick" do
    optional :request_context, :message, 1, "hydro.schemas.github.v1.entities.RequestContext"
    optional :actor, :message, 2, "hydro.schemas.github.v1.entities.User"
    optional :repository, :message, 3, "hydro.schemas.github.v1.entities.Repository"
    optional :action, :enum, 4, "hydro.schemas.github.v1.SimilarIssuesShowHideResultsClick.Action"
    optional :session_key, :string, 5
  end
  add_enum "hydro.schemas.github.v1.SimilarIssuesShowHideResultsClick.Action" do
    value :UNKNOWN, 0
    value :CLOSE, 1
    value :SHOW, 2
  end
end

module Hydro
  module Schemas
    module Github
      module V1
        SimilarIssuesShowHideResultsClick = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.SimilarIssuesShowHideResultsClick").msgclass
        SimilarIssuesShowHideResultsClick::Action = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.SimilarIssuesShowHideResultsClick.Action").enummodule
      end
    end
  end
end
