# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/v1/project_event.proto

require 'google/protobuf'

require 'hydro/schemas/github/v1/entities/request_context_pb'
require 'hydro/schemas/github/v1/entities/user_pb'
require 'hydro/schemas/github/v1/entities/project_pb'
require 'hydro/schemas/github/v1/entities/repository_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.v1.ProjectEvent" do
    optional :request_context, :message, 1, "hydro.schemas.github.v1.entities.RequestContext"
    optional :actor, :message, 2, "hydro.schemas.github.v1.entities.User"
    optional :project, :message, 3, "hydro.schemas.github.v1.entities.Project"
    optional :project_owner_user, :message, 4, "hydro.schemas.github.v1.entities.User"
    optional :project_owner_repo, :message, 5, "hydro.schemas.github.v1.entities.Repository"
    optional :action, :enum, 6, "hydro.schemas.github.v1.ProjectEvent.Action"
  end
  add_enum "hydro.schemas.github.v1.ProjectEvent.Action" do
    value :ACTION_UNKNOWN, 0
    value :CREATE, 1
    value :DELETE, 2
    value :OPEN, 3
    value :CLOSE, 4
  end
end

module Hydro
  module Schemas
    module Github
      module V1
        ProjectEvent = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.ProjectEvent").msgclass
        ProjectEvent::Action = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.ProjectEvent.Action").enummodule
      end
    end
  end
end
