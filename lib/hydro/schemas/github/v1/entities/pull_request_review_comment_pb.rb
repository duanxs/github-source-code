# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/v1/entities/pull_request_review_comment.proto

require 'google/protobuf'

require 'google/protobuf/timestamp_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.v1.entities.PullRequestReviewComment" do
    optional :id, :uint64, 1
    optional :global_relay_id, :string, 2
    optional :body, :string, 3
    optional :contains_suggestion, :bool, 4
    optional :author_id, :uint64, 5
    optional :created_at, :message, 6, "google.protobuf.Timestamp"
    optional :updated_at, :message, 7, "google.protobuf.Timestamp"
    optional :start_position_offset, :uint64, 8
  end
end

module Hydro
  module Schemas
    module Github
      module V1
        module Entities
          PullRequestReviewComment = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.entities.PullRequestReviewComment").msgclass
        end
      end
    end
  end
end
