# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/v1/entities/repository.proto

require 'google/protobuf'

require 'google/protobuf/timestamp_pb'
require 'google/protobuf/wrappers_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.v1.entities.Repository" do
    optional :id, :uint32, 1
    optional :global_relay_id, :string, 2
    optional :name, :string, 3
    optional :description, :string, 4
    optional :visibility, :enum, 5, "hydro.schemas.github.v1.entities.Repository.Visibility"
    optional :parent_id, :uint32, 6
    optional :stargazer_count, :uint32, 7
    optional :public_fork_count, :uint32, 8
    optional :pushed_at, :message, 9, "google.protobuf.Timestamp"
    optional :created_at, :message, 10, "google.protobuf.Timestamp"
    optional :updated_at, :message, 11, "google.protobuf.Timestamp"
    optional :template, :bool, 12
    optional :disk_usage, :uint32, 13
    optional :default_branch, :string, 14
    optional :primary_language_name, :string, 15
    optional :owner_id, :message, 16, "google.protobuf.UInt32Value"
    optional :organization_id, :message, 17, "google.protobuf.UInt32Value"
    optional :network_id, :uint32, 18
    optional :wiki_world_writable, :bool, 19
  end
  add_enum "hydro.schemas.github.v1.entities.Repository.Visibility" do
    value :VISIBILITY_UNKNOWN, 0
    value :PUBLIC, 1
    value :PRIVATE, 2
    value :INTERNAL, 3
  end
end

module Hydro
  module Schemas
    module Github
      module V1
        module Entities
          Repository = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.entities.Repository").msgclass
          Repository::Visibility = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.entities.Repository.Visibility").enummodule
        end
      end
    end
  end
end
