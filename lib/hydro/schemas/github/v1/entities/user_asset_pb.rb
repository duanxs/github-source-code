# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/v1/entities/user_asset.proto

require 'google/protobuf'

require 'google/protobuf/timestamp_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.v1.entities.UserAsset" do
    optional :id, :uint64, 1
    optional :user_id, :uint64, 2
    optional :name, :string, 3
    optional :content_type, :string, 4
    optional :size, :uint32, 5
    optional :guid, :string, 6
    optional :state, :enum, 7, "hydro.schemas.github.v1.entities.UserAsset.State"
    optional :storage_blob_id, :uint64, 8
    optional :oid, :string, 9
    optional :storage_provider, :string, 10
    optional :storage_external_url, :string, 11
    optional :created_at, :message, 12, "google.protobuf.Timestamp"
    optional :updated_at, :message, 13, "google.protobuf.Timestamp"
  end
  add_enum "hydro.schemas.github.v1.entities.UserAsset.State" do
    value :STATE_UNKNOWN, 0
    value :STARTER, 1
    value :UPLOADED, 2
    value :DELETED, 3
    value :MULTIPART_UPLOAD_STARTED, 4
    value :MULTIPART_UPLOAD_LIST_PARTS, 5
    value :MULTIPART_UPLOAD_COMPLETED, 6
  end
end

module Hydro
  module Schemas
    module Github
      module V1
        module Entities
          UserAsset = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.entities.UserAsset").msgclass
          UserAsset::State = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.entities.UserAsset.State").enummodule
        end
      end
    end
  end
end
