# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/v1/pull_request_review_dismiss.proto

require 'google/protobuf'

require 'hydro/schemas/github/v1/entities/pull_request_review_pb'
require 'hydro/schemas/github/v1/entities/repository_pb'
require 'hydro/schemas/github/v1/entities/pull_request_pb'
require 'hydro/schemas/github/v1/entities/user_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.v1.PullRequestReviewDismiss" do
    optional :actor, :message, 1, "hydro.schemas.github.v1.entities.User"
    optional :repository, :message, 2, "hydro.schemas.github.v1.entities.Repository"
    optional :pull_request, :message, 3, "hydro.schemas.github.v1.entities.PullRequest"
    optional :pull_request_review, :message, 4, "hydro.schemas.github.v1.entities.PullRequestReview"
  end
end

module Hydro
  module Schemas
    module Github
      module V1
        PullRequestReviewDismiss = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.PullRequestReviewDismiss").msgclass
      end
    end
  end
end
