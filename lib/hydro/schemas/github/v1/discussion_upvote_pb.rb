# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/v1/discussion_upvote.proto

require 'google/protobuf'

require 'hydro/schemas/github/v1/entities/discussion_pb'
require 'hydro/schemas/github/v1/entities/discussion_comment_pb'
require 'hydro/schemas/github/v1/entities/request_context_pb'
require 'hydro/schemas/github/v1/entities/spamurai_form_signals_pb'
require 'hydro/schemas/github/v1/entities/user_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.v1.DiscussionUpvote" do
    optional :request_context, :message, 1, "hydro.schemas.github.v1.entities.RequestContext"
    optional :actor, :message, 2, "hydro.schemas.github.v1.entities.User"
    optional :discussion, :message, 3, "hydro.schemas.github.v1.entities.Discussion"
    optional :discussion_comment, :message, 4, "hydro.schemas.github.v1.entities.DiscussionComment"
    optional :spamurai_form_signals, :message, 5, "hydro.schemas.github.v1.entities.SpamuraiFormSignals"
  end
end

module Hydro
  module Schemas
    module Github
      module V1
        DiscussionUpvote = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.DiscussionUpvote").msgclass
      end
    end
  end
end
