# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/v1/discussion_click.proto

require 'google/protobuf'

require 'hydro/schemas/github/v1/entities/request_context_pb'
require 'hydro/schemas/github/v1/entities/user_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.v1.DiscussionClick" do
    optional :request_context, :message, 1, "hydro.schemas.github.v1.entities.RequestContext"
    optional :actor, :message, 2, "hydro.schemas.github.v1.entities.User"
    optional :event_context, :enum, 3, "hydro.schemas.github.v1.DiscussionClick.EventContext"
    optional :target, :enum, 4, "hydro.schemas.github.v1.DiscussionClick.EventTarget"
    optional :discussion_repository_id, :uint64, 5
    optional :current_repository_id, :uint64, 6
    optional :discussion_id, :uint64, 7
    optional :discussion_comment_id, :uint64, 8
  end
  add_enum "hydro.schemas.github.v1.DiscussionClick.EventContext" do
    value :EVENT_CONTEXT_UNKNOWN, 0
    value :DISCUSSIONS_LIST, 1
    value :DISCUSSION_VIEW, 2
    value :NEW_DISCUSSION_VIEW, 3
  end
  add_enum "hydro.schemas.github.v1.DiscussionClick.EventTarget" do
    value :EVENT_TARGET_UNKNOWN, 0
    value :DISCUSSION_LINK, 1
    value :USER_PROFILE_LINK, 2
    value :NEW_DISCUSSION_LINK, 3
    value :COPY_LINK_MENU_ITEM, 4
    value :QUOTE_REPLY_MENU_ITEM, 5
    value :REFERENCE_IN_NEW_ISSUE_MENU_ITEM, 6
    value :REPORT_CONTENT_MENU_ITEM, 7
    value :COMMUNITY_LINK, 8
  end
end

module Hydro
  module Schemas
    module Github
      module V1
        DiscussionClick = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.DiscussionClick").msgclass
        DiscussionClick::EventContext = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.DiscussionClick.EventContext").enummodule
        DiscussionClick::EventTarget = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.DiscussionClick.EventTarget").enummodule
      end
    end
  end
end
