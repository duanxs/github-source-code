# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/v1/user_signup.proto

require 'google/protobuf'

require 'hydro/schemas/github/v1/entities/funcaptcha_response_pb'
require 'hydro/schemas/github/v1/entities/request_context_pb'
require 'hydro/schemas/github/v1/entities/spamurai_form_signals_pb'
require 'hydro/schemas/github/v1/entities/user_pb'
require 'hydro/schemas/github/v1/entities/user_email_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.v1.UserSignup" do
    optional :request_context, :message, 1, "hydro.schemas.github.v1.entities.RequestContext"
    optional :actor, :message, 2, "hydro.schemas.github.v1.entities.User"
    optional :email, :message, 3, "hydro.schemas.github.v1.entities.UserEmail"
    optional :spamurai_form_signals, :message, 4, "hydro.schemas.github.v1.entities.SpamuraiFormSignals"
    optional :funcaptcha_session_id, :string, 5
    optional :funcaptcha_solved, :bool, 6
    optional :funcaptcha_response, :message, 7, "hydro.schemas.github.v1.entities.FuncaptchaResponse"
    optional :elected_to_receive_marketing_email, :bool, 8
    optional :visitor_id, :string, 9
    optional :ga_id, :string, 10
  end
end

module Hydro
  module Schemas
    module Github
      module V1
        UserSignup = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.UserSignup").msgclass
      end
    end
  end
end
