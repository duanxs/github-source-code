# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/v1/repository_deleted.proto

require 'google/protobuf'

require 'hydro/schemas/github/v1/entities/repository_pb'
require 'hydro/schemas/github/v1/entities/user_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.v1.RepositoryDeleted" do
    optional :actor, :message, 1, "hydro.schemas.github.v1.entities.User"
    optional :deleted_repository, :message, 2, "hydro.schemas.github.v1.entities.Repository"
  end
end

module Hydro
  module Schemas
    module Github
      module V1
        RepositoryDeleted = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.RepositoryDeleted").msgclass
      end
    end
  end
end
