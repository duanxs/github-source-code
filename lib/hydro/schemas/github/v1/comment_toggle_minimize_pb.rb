# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/v1/comment_toggle_minimize.proto

require 'google/protobuf'

require 'hydro/schemas/github/v1/entities/user_pb'
require 'hydro/schemas/github/v1/entities/request_context_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.v1.CommentToggleMinimize" do
    optional :request_context, :message, 1, "hydro.schemas.github.v1.entities.RequestContext"
    optional :actor, :message, 2, "hydro.schemas.github.v1.entities.User"
    optional :actor_was_staff, :bool, 3
    optional :event_type, :enum, 4, "hydro.schemas.github.v1.CommentToggleMinimize.EventType"
    optional :reason, :string, 5
    optional :comment_id, :int64, 6
    optional :comment_type, :enum, 7, "hydro.schemas.github.v1.CommentToggleMinimize.CommentType"
    optional :classifier, :enum, 8, "hydro.schemas.github.v1.CommentToggleMinimize.Classifier"
    optional :user, :message, 9, "hydro.schemas.github.v1.entities.User"
  end
  add_enum "hydro.schemas.github.v1.CommentToggleMinimize.EventType" do
    value :EVENT_TYPE_UNKNOWN, 0
    value :MINIMIZE, 1
    value :UNMINIMIZE, 2
  end
  add_enum "hydro.schemas.github.v1.CommentToggleMinimize.CommentType" do
    value :COMMENT_TYPE_UNKNOWN, 0
    value :ISSUE_COMMENT, 1
    value :COMMIT_COMMENT, 2
    value :GIST_COMMENT, 3
    value :PULL_REQUEST_REVIEW, 4
    value :PULL_REQUEST_REVIEW_COMMENT, 5
    value :PULL_REQUEST_REVIEW_THREAD, 6
    value :DISCUSSION_COMMENT, 7
  end
  add_enum "hydro.schemas.github.v1.CommentToggleMinimize.Classifier" do
    value :CLASSIFER_UNKNOWN, 0
    value :SPAM, 1
    value :ABUSE, 2
    value :OFF_TOPIC, 3
    value :OUTDATED, 4
    value :RESOLVED, 5
    value :DUPLICATE, 6
  end
end

module Hydro
  module Schemas
    module Github
      module V1
        CommentToggleMinimize = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.CommentToggleMinimize").msgclass
        CommentToggleMinimize::EventType = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.CommentToggleMinimize.EventType").enummodule
        CommentToggleMinimize::CommentType = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.CommentToggleMinimize.CommentType").enummodule
        CommentToggleMinimize::Classifier = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.v1.CommentToggleMinimize.Classifier").enummodule
      end
    end
  end
end
