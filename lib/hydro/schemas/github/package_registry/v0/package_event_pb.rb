# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/package_registry/v0/package_event.proto

require 'google/protobuf'

require 'hydro/schemas/github/v1/entities/request_context_pb'
require 'hydro/schemas/github/v1/entities/user_pb'
require 'hydro/schemas/github/v1/entities/repository_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.package_registry.v0.PackageEvent" do
    optional :request_context, :message, 1, "hydro.schemas.github.v1.entities.RequestContext"
    optional :actor, :message, 2, "hydro.schemas.github.v1.entities.User"
    optional :repository, :message, 3, "hydro.schemas.github.v1.entities.Repository"
    optional :action, :enum, 4, "hydro.schemas.github.package_registry.v0.PackageEvent.Action"
    optional :registry_package_id, :uint64, 5
    optional :package_version, :string, 6
    optional :package_size, :uint64, 7
    optional :user_agent, :string, 8
  end
  add_enum "hydro.schemas.github.package_registry.v0.PackageEvent.Action" do
    value :UNKNOWN, 0
    value :PUBLISHED, 1
    value :DOWNLOADED, 2
  end
end

module Hydro
  module Schemas
    module Github
      module PackageRegistry
        module V0
          PackageEvent = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.package_registry.v0.PackageEvent").msgclass
          PackageEvent::Action = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.package_registry.v0.PackageEvent.Action").enummodule
        end
      end
    end
  end
end
