# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/sponsors/v1/tier_description_update.proto

require 'google/protobuf'

require 'hydro/schemas/github/v1/entities/request_context_pb'
require 'hydro/schemas/github/v1/entities/user_pb'
require 'hydro/schemas/github/sponsors/v1/entities/sponsors_listing_pb'
require 'hydro/schemas/github/sponsors/v1/entities/sponsors_tier_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.sponsors.v1.TierDescriptionUpdate" do
    optional :request_context, :message, 1, "hydro.schemas.github.v1.entities.RequestContext"
    optional :listing, :message, 2, "hydro.schemas.github.sponsors.v1.entities.SponsorsListing"
    optional :tier, :message, 3, "hydro.schemas.github.sponsors.v1.entities.SponsorsTier"
    optional :current_tier_description, :string, 4
    optional :previous_tier_description, :string, 5
    optional :sponsors_count_on_tier, :uint32, 6
    optional :actor, :message, 7, "hydro.schemas.github.v1.entities.User"
  end
end

module Hydro
  module Schemas
    module Github
      module Sponsors
        module V1
          TierDescriptionUpdate = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.sponsors.v1.TierDescriptionUpdate").msgclass
        end
      end
    end
  end
end
