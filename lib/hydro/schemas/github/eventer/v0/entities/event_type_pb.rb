# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/eventer/v0/entities/event_type.proto

require 'google/protobuf'

Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.eventer.v0.entities.EventType" do
    optional :name, :string, 1
  end
end

module Hydro
  module Schemas
    module Github
      module Eventer
        module V0
          module Entities
            EventType = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.eventer.v0.entities.EventType").msgclass
          end
        end
      end
    end
  end
end
