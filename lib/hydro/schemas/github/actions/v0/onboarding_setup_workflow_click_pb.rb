# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/actions/v0/onboarding_setup_workflow_click.proto

require 'google/protobuf'

require 'hydro/schemas/github/v1/entities/request_context_pb'
require 'hydro/schemas/github/v1/entities/repository_pb'
require 'hydro/schemas/github/v1/entities/user_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.actions.v0.OnboardingSetupWorkflowClick" do
    optional :request_context, :message, 1, "hydro.schemas.github.v1.entities.RequestContext"
    optional :user, :message, 2, "hydro.schemas.github.v1.entities.User"
    optional :repository, :message, 3, "hydro.schemas.github.v1.entities.Repository"
    optional :workflow_template, :string, 4
  end
end

module Hydro
  module Schemas
    module Github
      module Actions
        module V0
          OnboardingSetupWorkflowClick = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.actions.v0.OnboardingSetupWorkflowClick").msgclass
        end
      end
    end
  end
end
