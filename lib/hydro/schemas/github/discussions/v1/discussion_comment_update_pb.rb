# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/discussions/v1/discussion_comment_update.proto

require 'google/protobuf'

require 'hydro/schemas/github/v1/entities/repository_pb'
require 'hydro/schemas/github/v1/entities/user_pb'
require 'hydro/schemas/github/discussions/v1/entities/discussion_pb'
require 'hydro/schemas/github/discussions/v1/entities/discussion_comment_pb'
require 'hydro/schemas/github/v1/entities/request_context_pb'
require 'hydro/schemas/github/v1/entities/specimen_data_pb'
require 'hydro/schemas/github/v1/entities/spamurai_form_signals_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.discussions.v1.DiscussionCommentUpdate" do
    optional :request_context, :message, 1, "hydro.schemas.github.v1.entities.RequestContext"
    optional :discussion_comment, :message, 2, "hydro.schemas.github.discussions.v1.entities.DiscussionComment"
    optional :spamurai_form_signals, :message, 3, "hydro.schemas.github.v1.entities.SpamuraiFormSignals"
    optional :specimen_body, :message, 4, "hydro.schemas.github.v1.entities.SpecimenData"
    optional :actor, :message, 5, "hydro.schemas.github.v1.entities.User"
    optional :discussion, :message, 6, "hydro.schemas.github.discussions.v1.entities.Discussion"
    optional :repository, :message, 7, "hydro.schemas.github.v1.entities.Repository"
    optional :repository_owner, :message, 8, "hydro.schemas.github.v1.entities.User"
    optional :discussion_author, :message, 9, "hydro.schemas.github.v1.entities.User"
    optional :author, :message, 10, "hydro.schemas.github.v1.entities.User"
  end
end

module Hydro
  module Schemas
    module Github
      module Discussions
        module V1
          DiscussionCommentUpdate = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.discussions.v1.DiscussionCommentUpdate").msgclass
        end
      end
    end
  end
end
