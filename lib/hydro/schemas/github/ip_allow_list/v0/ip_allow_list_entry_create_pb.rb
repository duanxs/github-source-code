# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/ip_allow_list/v0/ip_allow_list_entry_create.proto

require 'google/protobuf'

require 'hydro/schemas/github/ip_allow_list/v0/entities/ip_allow_list_entry_pb'
require 'hydro/schemas/github/v1/entities/user_pb'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.ip_allow_list.v0.IpAllowListEntryCreate" do
    optional :entry, :message, 1, "hydro.schemas.github.ip_allow_list.v0.entities.IpAllowListEntry"
    optional :actor, :message, 2, "hydro.schemas.github.v1.entities.User"
  end
end

module Hydro
  module Schemas
    module Github
      module IpAllowList
        module V0
          IpAllowListEntryCreate = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.ip_allow_list.v0.IpAllowListEntryCreate").msgclass
        end
      end
    end
  end
end
