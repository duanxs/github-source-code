# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/github/billing/v0/serialized_metered_line_item.proto

require 'google/protobuf'

Google::Protobuf::DescriptorPool.generated_pool.build do
  add_message "hydro.schemas.github.billing.v0.SerializedMeteredLineItem" do
    optional :metered_product, :enum, 1, "hydro.schemas.github.billing.v0.SerializedMeteredLineItem.MeteredProduct"
    optional :line_item_id, :uint64, 2
    optional :csv_data, :string, 3
  end
  add_enum "hydro.schemas.github.billing.v0.SerializedMeteredLineItem.MeteredProduct" do
    value :UNKNOWN_METERED_PRODUCT, 0
    value :ACTIONS, 1
    value :PACKAGES, 2
    value :STORAGE, 3
    value :CODESPACES_COMPUTE, 4
    value :CODESPACES_STORAGE, 5
  end
end

module Hydro
  module Schemas
    module Github
      module Billing
        module V0
          SerializedMeteredLineItem = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.billing.v0.SerializedMeteredLineItem").msgclass
          SerializedMeteredLineItem::MeteredProduct = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.github.billing.v0.SerializedMeteredLineItem.MeteredProduct").enummodule
        end
      end
    end
  end
end
