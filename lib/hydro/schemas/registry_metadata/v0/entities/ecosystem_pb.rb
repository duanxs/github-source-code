# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: hydro/schemas/registry_metadata/v0/entities/ecosystem.proto

require 'google/protobuf'

Google::Protobuf::DescriptorPool.generated_pool.build do
  add_enum "hydro.schemas.registry_metadata.v0.entities.Ecosystem" do
    value :UNSPECIFIED, 0
    value :CONTAINER, 1
    value :NPM, 2
    value :RUBYGEMS, 3
    value :MAVEN, 4
    value :NUGET, 5
    value :PYPI, 6
  end
end

module Hydro
  module Schemas
    module RegistryMetadata
      module V0
        module Entities
          Ecosystem = Google::Protobuf::DescriptorPool.generated_pool.lookup("hydro.schemas.registry_metadata.v0.entities.Ecosystem").enummodule
        end
      end
    end
  end
end
