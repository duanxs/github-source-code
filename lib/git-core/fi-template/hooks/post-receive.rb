#!/usr/bin/env ruby
# frozen_string_literal: true
require "github/githooks/procline"
GitHub::GitHooks.setproctitle

require "github/githooks/push_context"

$stdin.set_encoding ::Encoding::ASCII_8BIT
context = GitHub::PushContext.new($stdin.read)

if Rails.env != "test"
  context.audit_msg ||= "got a push outside production in #{context.path}"
end

begin
  require "audit/syslog"
  Syslog.open Audit::Syslog::SYSLOG_IDENT, Audit::Syslog::SYSLOG_OPTIONS, Audit::Syslog::SYSLOG_FACILITY unless Syslog.opened?
  Syslog.log Syslog::LOG_INFO, "%s", context.audit_msg
  Syslog.close
rescue Object
  puts "Warning: R9133 Please contact GitHub Enterprise Support via #{GitHub.enterprise_support_url}" if !Rails.test?
end

path = "/internal/repositories/#{context.repo_name}/git/pushes"
if context.path.end_with?(".wiki.git")
  path = "/internal/repositories/#{context.repo_name}/wiki/git/pushes"
end
key = GitHub.api_internal_repositories_hmac_keys.first.to_s
resp = context.notify_app(path, key)

if resp
  begin
    h = GitHub::JSON.decode(resp.body)
  rescue Yajl::ParseError
    h = nil
  end
  $stdout.puts h["output"] if h && h.key?("output")
end
