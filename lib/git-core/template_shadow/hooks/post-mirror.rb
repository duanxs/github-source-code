#!/usr/bin/env ruby
# This hook is invoked by Mirror.perform! on the local repository
# immediately after updating refs. It executes once for the mirror
# operation. It takes no arguments, but for each ref to be updated it
# receives on standard input a line of the format:
#
#   <old-value> SP <new-value> SP <ref-name> LF
#
# where <old-value> is the old object name stored in the ref,
# <new-value> is the new object name to be stored in the ref,
# and <ref-name> is the full name of the ref. When creating a
# new ref, <old-value> is 40 0s. When deleting an existing ref,
# <new-value> is 40 0s.
#
# The hook's exit status is ignored.
require "github/githooks/procline"
GitHub::GitHooks.setproctitle

# Determine real file location and load basic config
require "pathname"
require Pathname.new(__FILE__).realpath + "../../../../../config/basic"
require "github"
require "github/config/stats"
require "github/config/resque"
require "github/config/active_job"
require "resque/buffered_resque"
require "timed_resque"
require "timed_resque/buffered_timed_resque"

path = ENV["REPO_PATH"]

begin
  $stdin.set_encoding ::Encoding::ASCII_8BIT
  refs = []
  $stdin.readlines.each do |line|
    before, after, ref = line.chomp.split
    next if ref.include?("refs/remotes") # apache mirror include refs twice
    refs << [ref, before, after]
  end

  PostMirrorHookJob.perform_later(path, ENV["GIT_PUSHER"], refs, "mirror")
rescue => e
  warn "Mirror worked, but post-mirror failed: #{e}"
  warn e.backtrace
  exit! 1
end
