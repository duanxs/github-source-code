#!/usr/bin/env ruby
# frozen_string_literal: true

require "github/githooks/procline"
GitHub::GitHooks.setproctitle

require "github/githooks/push_context"

$stdin.set_encoding ::Encoding::ASCII_8BIT
context = GitHub::PushContext.new($stdin.read)

GitHub.dogstats.time("git.hooks.post-receive", tags: ["repos:http"]) do
  path = "/internal/repositories/#{context.repo_name}/git/pushes"
  if context.path.end_with?(".wiki.git")
    path = "/internal/repositories/#{context.repo_name}/wiki/git/pushes"
  end
  key = GitHub.api_internal_repositories_hmac_keys.first.to_s
  resp = context.notify_app(path, key)

  if resp
    begin
      h = GitHub::JSON.decode(resp.body)
    rescue Yajl::ParseError
      h = nil
    end
    $stdout.puts h["output"] if h && h.key?("output")
  end
end
