#!/usr/bin/env ruby
require "github/githooks/procline"
GitHub::GitHooks.setproctitle

require "json"

require "github"
require "github/config/failbot"
require "github/config/stats"

require "github/githooks/push_context"

babeld_mode = (ENV["GIT_SOCKSTAT_VAR_babeld_hook"] == "bool:true")
quarantine_only = (ENV["GIT_SOCKSTAT_VAR_quarantine_only"] == "1")
custom_hooks_only = !!ENV["CUSTOM_HOOKS_ONLY"]

$stdin.set_encoding ::Encoding::ASCII_8BIT
context = GitHub::PushContext.new($stdin.read)

if ENV["DEBUG_GIT_HOOKS"] == "1"
  puts "DEBUG start"
  puts "DEBUG hook_name=pre-receive"
  puts "DEBUG babeld_mode=#{babeld_mode}"
  puts "DEBUG quarantine_only=#{quarantine_only}"
  puts "DEBUG custom_hooks_only=#{custom_hooks_only}"
  puts "DEBUG GIT_QUARANTINE_PATH=#{ENV["GIT_QUARANTINE_PATH"]}" unless ENV["GIT_QUARANTINE_PATH"].nil?
  puts "DEBUG stop"
end

case
when babeld_mode && !quarantine_only && !custom_hooks_only
  require "github/githooks/pre_receive_redirect"
  require "github/githooks/pre_receive_refs"
  require "github/githooks/pre_receive_ref_length"
  require "github/githooks/pre_receive_force_push"
  require "github/githooks/pre_receive_lfs_integrity"

  GitHub.dogstats.time("git.hooks.pre_receive.all_standard.time") do
    context.check(
      GitHub::PreReceiveRedirect,
      GitHub::PreReceiveRefs,
      GitHub::PreReceiveRefLength,
      GitHub::PreReceiveForcePush,
      GitHub::PreReceiveLFSIntegrity,
    )
  end
when !babeld_mode && quarantine_only && !custom_hooks_only
  require "github/githooks/pre_receive_quota"
  require "github/githooks/pre_receive_blob"

  GitHub.dogstats.time("git.hooks.pre_receive.all_quarantine.time") do
    context.check(
      GitHub::PreReceiveQuota,
      GitHub::PreReceiveBlob,
    )
  end
else
  # Only the above combinations of modes are allowed.
  context.mode_error(
    babeld_mode: babeld_mode,
    quarantine_only: quarantine_only,
    custom_hooks_only: custom_hooks_only,
  )
end
