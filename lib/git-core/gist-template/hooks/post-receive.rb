#!/usr/bin/env ruby
# frozen_string_literal: true

require "github/githooks/procline"
GitHub::GitHooks.setproctitle

require "github/githooks/push_context"

$stdin.set_encoding ::Encoding::ASCII_8BIT
context = GitHub::PushContext.new($stdin.read)

GitHub.dogstats.time("git.hooks.post-receive", tags: ["gists:http"]) do
  path = "/internal/gists/#{context.repo_name}/git/pushes"
  key = GitHub.api_internal_gists_hmac_keys.first.to_s
  context.notify_app(path, key)
end
