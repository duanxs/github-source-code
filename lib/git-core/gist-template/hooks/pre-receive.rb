#!/usr/bin/env ruby
require "github/githooks/procline"
GitHub::GitHooks.setproctitle

require "json"

require "github"
require "github/config/failbot"
require "github/config/stats"

require "github/githooks/push_context"

babeld_mode = (ENV["GIT_SOCKSTAT_VAR_babeld_hook"] == "bool:true")
quarantine_only = (ENV["GIT_SOCKSTAT_VAR_quarantine_only"] == "1")

$stdin.set_encoding ::Encoding::ASCII_8BIT
context = GitHub::PushContext.new($stdin.read)

if ENV["DEBUG_GIT_HOOKS"] == "1"
  puts "DEBUG start"
  puts "DEBUG hook_name=pre-receive"
  puts "DEBUG babeld_mode=#{babeld_mode}"
  puts "DEBUG quarantine_only=#{quarantine_only}"
  puts "DEBUG GIT_QUARANTINE_PATH=#{ENV["GIT_QUARANTINE_PATH"]}" unless ENV["GIT_QUARANTINE_PATH"].nil?
  puts "DEBUG stop"
end

case
when babeld_mode && !quarantine_only
  require "github/githooks/pre_receive_refs"

  context.check(GitHub::PreReceiveRefs)
when !babeld_mode && quarantine_only
  require "github/githooks/pre_receive_gist_directories"
  require "github/githooks/pre_receive_quota"
  require "github/githooks/pre_receive_blob"

  context.check(
    GitHub::PreReceiveGistDirectories,
    GitHub::PreReceiveQuota,
    GitHub::PreReceiveBlob,
  )
else
  # Only the above combinations of modes are allowed.
  context.mode_error(
    babeld_mode: babeld_mode,
    quarantine_only: quarantine_only,
  )
end
