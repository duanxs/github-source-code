# frozen_string_literal: true

module Sponsors
  class PostSponsorshipSurvey
    SLUG = "sponsorship_created"

    VISIBLE_QUESTIONS_SHORT_TEXTS = [
      DISCOVERY_QUESTION_SHORT_TEXT = "discovery",
      INFLUENCE_QUESTION_SHORT_TEXT = "influence",
      WHY_SPONSOR_QUESTION_SHORT_TEXT = "why_sponsor",
    ].freeze

    HIDDEN_QUESTIONS_SHORT_TEXTS = [
      SPONSORABLE_QUESTION_SHORT_TEXT = "sponsorable",
      SPONSORED_TIER_QUESTION_SHORT_TEXT = "sponsored_tier",
      TOTAL_ACCOUNTS_SPONSORING_QUESTION_SHORT_TEXT = "total_accounts_sponsoring",
    ].freeze

    class << self
      def find_or_create_survey!
        survey = Survey.find_by_slug(SLUG)
        return survey if survey

        survey = Survey.create!(
          title: SLUG.titleize,
          slug: SLUG,
        )

        create_discovery_question!(survey)
        create_influence_question!(survey)
        create_why_sponsor_question!(survey)
        create_sponsorable_question!(survey)
        create_tier_question!(survey)
        create_total_acccounts_sponsoring_question!(survey)

        survey
      end

      private

      def create_discovery_question!(survey)
        question = survey.questions.create!(
          display_order: 1,
          short_text: DISCOVERY_QUESTION_SHORT_TEXT,
          text: "How did you come across this sponsorship page? Select all that apply",
        )

        [
          {short_text: "sponsor_page", text: "GitHub Sponsors page (github.com/sponsors)"},
          {short_text: "user_profile", text: "From a GitHub user profile"},
          {short_text: "repository", text: "From a GitHub repository"},
          {short_text: "announcement", text: "GitHub announcement (conferences, blog post, etc)"},
          {short_text: "social_media", text: "Social media (Twitter, Facebook, etc)"},
          {short_text: "internal_channels", text: "Internal channels (Slack, email newsletter, etc)"},
          {short_text: "word_of_mouth", text: "Word of mouth"},
        ].each.with_index do |choice, index|
          SurveyChoice.create!(
            question: question,
            short_text: choice[:short_text],
            text: choice[:text],
            display_order: index + 1,
          )
        end
      end

      def create_influence_question!(survey)
        question = survey.questions.create!(
          display_order: 2,
          short_text: INFLUENCE_QUESTION_SHORT_TEXT,
          text: "How did you determine the amount you chose to sponsor?",
        )

        [
          {short_text: "already_knew", text: "I already knew how much I wanted to sponsor, and chose the closest tier"},
          {short_text: "reward", text: "I chose my sponsorship amount based on the reward"},
          {short_text: "financial", text: "I sponsored what I could, given my financial situation"},
        ].each.with_index do |choice, index|
          SurveyChoice.create!(
            question: question,
            short_text: choice[:short_text],
            text: choice[:text],
            display_order: index + 1,
          )
        end
      end

      def create_why_sponsor_question!(survey)
        question = survey.questions.create!(
          display_order: 3,
          short_text: WHY_SPONSOR_QUESTION_SHORT_TEXT,
          text: "Why do you sponsor?",
        )
        SurveyChoice.create!(
          question: question,
          text: "Other",
          short_text: "other",
        )
      end

      def create_sponsorable_question!(survey)
        question = survey.questions.create!(
          display_order: 4,
          short_text: SPONSORABLE_QUESTION_SHORT_TEXT,
          text: "Sponsorable ID",
          hidden: true,
        )
        SurveyChoice.create!(
          question: question,
          text: "Other",
          short_text: "other",
        )
      end

      def create_tier_question!(survey)
        question = survey.questions.create!(
          display_order: 5,
          short_text: SPONSORED_TIER_QUESTION_SHORT_TEXT,
          text: "Sponsored Tier Amount",
          hidden: true,
        )
        SurveyChoice.create!(
          question: question,
          text: "Other",
          short_text: "other",
        )
      end

      def create_total_acccounts_sponsoring_question!(survey)
        question = survey.questions.create!(
          display_order: 6,
          short_text: TOTAL_ACCOUNTS_SPONSORING_QUESTION_SHORT_TEXT,
          text: "Total number of accounts sponsoring",
          hidden: true,
        )
        SurveyChoice.create!(
          question: question,
          text: "Other",
          short_text: "other",
        )
      end
    end
  end
end
