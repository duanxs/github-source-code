# frozen_string_literal: true

module Sponsors
  class PostSponsorshipCancelationSurvey
    SLUG = "sponsorship_canceled"
    CANCELED_REASON_SHORT_TEXT = "why_canceled"

    class << self
      def find_or_create_survey!
        survey = Survey.find_by_slug(SLUG)
        return survey if survey

        survey = Survey.create!(
          title: SLUG.titleize,
          slug: SLUG,
        )

        question = survey.questions.create!(
          display_order: 1,
          short_text: CANCELED_REASON_SHORT_TEXT,
          text: "Why did you cancel your sponsorship?",
        )

        SurveyChoice.create!(
          question: question,
          short_text: "sponsored_intended_amount",
          text: "I sponsored the amount I intended to",
          display_order: 1,
        )

        SurveyChoice.create!(
          question: question,
          short_text: "save_money",
          text: "I need to save money",
          display_order: 2,
        )

        SurveyChoice.create!(
          question: question,
          short_text: "no_promised_reward",
          text: "I did not receive the reward promised",
          display_order: 3,
        )

        SurveyChoice.create!(
          question: question,
          short_text: "no_value",
          text: "I did not get value out of sponsoring",
          display_order: 4,
        )

        SurveyChoice.create!(
          question: question,
          short_text: "one_time",
          text: "I wanted to sponsor via a one-time payment",
          display_order: 5,
        )

        survey
      end
    end
  end
end
