# frozen_string_literal: true
require "csv"

class ApiRoutes::OwnershipReport < ApiRoutes::DefaultReport
  def print
    output = [%w(service_mapping method route codeowners aors)]

    collections.each do |collection|
      endpoints = collection.endpoints.map do |endpoint|
        output << [endpoint.service_mapping, endpoint.verb, endpoint.route, endpoint.codeowners, endpoint.aors]
      end
    end

    csv = CSV.new($stdout)
    output.map { |row| csv << row }
  end
end
