# rubocop:disable Style/FrozenStringLiteralComment

require "stratocaster/attributes"
require "stratocaster/event"
require "stratocaster/store"
require "stratocaster/service"
require "stratocaster/dispatcher"
require "stratocaster/response"

module Stratocaster
  EVENT = "Event".freeze unless const_defined?(:EVENT)
  DEFAULT_INDEX_SIZE = 300
  DEFAULT_PAGE_SIZE = 30

  TIMELINE_UPDATE_SCHEMA = "github.v1.StratocasterTimelineUpdate".freeze
  REVIEW_LAB_TIMELINE_TOPIC = "review-lab.v1.StratocasterTimelineUpdate".freeze

  # Public
  #
  # event_type - string (e.g. "WatchEvent")
  #
  # Return Stratocaster::Attributes class name
  def self.attributes_class_for(event_type)
    return if event_type.blank?
    "::Stratocaster::Attributes::#{event_type.to_s.chomp(EVENT)}".constantize
  end
end
