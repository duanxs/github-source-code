# frozen_string_literal: true

# Provides an abstract interface to searching via Github::Geyser::Rpc
module Search
  module Geyser
    extend self

    include GitHub::AreasOfResponsibility

    areas_of_responsibility :code_search

    # Public: List assignments in Geyser
    #
    # See Requests::GetAssignments.new for arguments
    #
    # Returns an instance of Responses::GetAssignments
    def get_assignments(repository_ids, lab_scoped: false, options: {})
      request = Requests::GetAssignments.new(repository_ids, options)

      response = Clients::Admin.new(geyser_client_env(lab_scoped)).get_assignments(request.query)
      Responses::GetAssignments.new(response)
    end

    # Public: List assignments in Geyser
    #
    # See Requests::KillSwitch.new for arguments
    #
    # Returns an instance of Responses::KillSwitch
    def toggle_kill_switch(desired_state, lab_scoped: false, options: {})
      request = Requests::KillSwitch.new(desired_state, options)

      response = Clients::Admin.new(geyser_client_env(lab_scoped)).toggle_kill_switch(request.request)
      Responses::KillSwitch.new(response)
    end

    # Public: Perform a Geyser search
    #
    # See Requests::Search.new for arguments
    #
    # Returns an instance of Responses::Search
    def search(search_term, repository_ids, user_id, lab_scoped: false, options: {})
      request = Requests::Search.new(search_term, repository_ids, user_id, options)

      response = Clients::Search.new(geyser_client_env(lab_scoped)).search(request.query)
      Responses::Search.new(response)
    end

    # Public: create a Geyser Collection
    def create_search_collection(lab_scoped: false, payload: {})
      request = Requests::CreateSearchCollection.new(payload)

      response = Clients::Admin.new(geyser_client_env(lab_scoped)).create_search_collection(request.query)
      Responses::CreateSearchCollection.new(response)
    end

    # Public: delete a Geyser Collection
    def delete_search_collection(lab_scoped: false, payload: {})
      request = Requests::DeleteSearchCollection.new(payload)

      response = Clients::Admin.new(geyser_client_env(lab_scoped)).delete_search_collection(request.query)
      Responses::DeleteSearchCollection.new(response)
    end

    # Public: update state of a Geyser search Collection
    def update_collection(lab_scoped: false, payload: {})
      request = Requests::UpdateCollection.new(payload)

      response = Clients::Admin.new(geyser_client_env(lab_scoped)).update_collection(request.query)
      Responses::UpdateCollection.new(response)
    end

    # Public: create a Geyser search Partition
    def create_search_partition(lab_scoped: false, payload: {})
      request = Requests::CreateSearchPartition.new(payload)

      response = Clients::Admin.new(geyser_client_env(lab_scoped)).create_search_partition(request.query)
      Responses::CreateSearchPartition.new(response)
    end

    # Public: delete a Geyser search Partition
    def delete_search_partition(lab_scoped: false, payload: {})
      request = Requests::DeleteSearchPartition.new(payload)

      response = Clients::Admin.new(geyser_client_env(lab_scoped)).delete_search_partition(request.query)
      Responses::DeleteSearchPartition.new(response)
    end

    # Public: update state of a Geyser search Partition
    def update_partitions(lab_scoped: false, payload: {})
      request = Requests::UpdatePartitions.new(payload)

      response = Clients::Admin.new(geyser_client_env(lab_scoped)).update_partitions(request.query)
      Responses::UpdatePartitions.new(response)
    end

    private

    def geyser_client_env(lab_scoped)
      if lab_scoped
        :lab
      else
       :production
      end
    end
  end
end
