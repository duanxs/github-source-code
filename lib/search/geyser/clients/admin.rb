# frozen_string_literal: true

require "geyser-client"

module Search
  module Geyser
    module Clients
      class Admin < Client
        attr_reader :geyser_env
        attr_reader :client

        # Public: Initializes the AdminClient
        def initialize(genv = :production)
          @geyser_env = genv
          @client = Github::Geyser::Rpc::AdminClient.new(connection)
        end

        # Public: returns the connection to the Geyser admin server, scoped to Geyser env
        def connection
          case geyser_env
          when :lab
            super(GitHub.geyser_admin_api_lab_url, GitHub.geyser_admin_api_lab_hmac_key)
          when :production
            super(GitHub.geyser_admin_api_url, GitHub.geyser_admin_api_hmac_key)
          else
            raise InvalidGeyserEnvError.new
          end
        end

        # Public: calls the #get_assignments method on the underlying admin client.
        #
        # request - a Github::Geyser::Rpc::GetAssignmentsRequest instance
        #
        # Returns a Twirp::ClientResp instance
        def get_assignments(request)
          client_request do
            client.get_assignments(request)
          end
        end

        # Public: calls the #create_search_collection method on the underlying admin client.
        #
        # request - a Github::Geyser::Rpc::CreateSearchCollectionRequest instance
        #
        # Returns a Twirp::ClientResp instance
        def create_search_collection(request)
          client_request do
            client.create_search_collection(request)
          end
        end

        # Public: calls the #create_search_partition method on the underlying admin client.
        #
        # request - a Github::Geyser::Rpc::CreateSearchPartitionRequest instance
        #
        # Returns a Twirp::ClientResp instance
        def create_search_partition(request)
          client_request do
            client.create_search_partition(request)
          end
        end

        # Public: calls the #update_collection method on the underlying admin client.
        #
        # request - a Github::Geyser::Rpc::UpdateCollectionRequest instance
        #
        # Returns a Twirp::ClientResp instance
        def update_collection(request)
          client_request do
            client.update_collection(request)
          end
        end

        # Public: calls the #update_partitions method on the underlying admin client.
        #
        # request - a Github::Geyser::Rpc::UpdatePartitionsRequest instance
        #
        # Returns a Twirp::ClientResp instance
        def update_partitions(request)
          client_request do
            client.update_partitions(request)
          end
        end

        # Public: calls the #delete_search_collection method on the underlying admin client.
        #
        # request - a Github::Geyser::Rpc::DeleteSearchCollectionRequest instance
        #
        # Returns a Twirp::ClientResp instance
        def delete_search_collection(request)
          client_request do
            client.delete_search_collection(request)
          end
        end

        # Public: calls the #delete_search_partition method on the underlying admin client.
        #
        # request - a Github::Geyser::Rpc::DeleteSearchPartitionRequest instance
        #
        # Returns a Twirp::ClientResp instance
        def delete_search_partition(request)
          client_request do
            client.delete_search_partition(request)
          end
        end

        # Public: calls the #toggle_kill_switch method on the underlying admin client.
        #
        # request - a Github::Geyser::Rpc::KillSwitchRequest instance
        #
        # Returns a Twirp::ClientResp instance
        def toggle_kill_switch(request)
          client_request do
            client.toggle_kill_switch(request)
          end
        end
      end
    end
  end
end
