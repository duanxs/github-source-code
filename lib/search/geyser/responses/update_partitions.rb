# frozen_string_literal: true

module Search
  module Geyser
    module Responses
      class UpdatePartitions < Response
        # Public: an array of 0..N Github::Search::Geyser::Rpc::SearchPartitions
        def search_partitions
          data&.search_partitions
        end
      end
    end
  end
end
