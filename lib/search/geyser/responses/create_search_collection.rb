# frozen_string_literal: true

module Search
  module Geyser
    module Responses
      class CreateSearchCollection < Response
        # Public: a Github::Search::Geyser::Rpc::SearchCollection
        def search_collection
          data&.search_collection
        end
      end
    end
  end
end
