# frozen_string_literal: true

module Search
  module Geyser
    module Responses
      class DeleteSearchPartition < Response
        # Public: a boolean value indicating if the request succeeded
        def success
          data&.success
        end

        # Public: a string value with the details of the success response message
        def details
          data&.details
        end
      end
    end
  end
end
