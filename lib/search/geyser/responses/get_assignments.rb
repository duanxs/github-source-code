# frozen_string_literal: true

module Search
  module Geyser
    module Responses
      class GetAssignments < Response
        # Public: an array of Github::Geyser::Rpc::Assignments from the response data, or nil
        def assignments
          data&.assignments
        end

        # Public: the count of assignments returned with the response data, or 0
        def count
          data&.count || 0
        end
      end
    end
  end
end
