# frozen_string_literal: true

require "geyser-client"

module Search
  module Geyser
    module Requests
      class DeleteSearchPartition

        attr_reader :query

        # options - A hash of options for Github::Geyser::Rpc::DeleteSearchPartitionRequest
        #           :partition_name  - A string representing the unique name of the Partition to delete
        def initialize(options = {})
          @query = Github::Geyser::Rpc::DeleteSearchPartitionRequest.new(
            partition_name: options.fetch(:partition_name, "ERR_NO_PARTITION_NAME_SUPPLIED")
          )
        end
      end
    end
  end
end
