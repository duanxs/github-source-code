# frozen_string_literal: true

require "geyser-client"

module Search
  module Geyser
    module Requests
      class Search
        SORT_TYPES = Github::Geyser::Rpc::SortType.constants
        SORT_DIRECTIONS = Github::Geyser::Rpc::SortDirection.constants

        attr_reader :query

        # search_term - The exact match query to perform
        # repository_ids - The list of repo IDs of the request, used for tracking.
        #           Used for filtering results to allowed or specified repos.
        # user_id - The ID of the user performing the search. Of important note
        #           is that we're not relying on Geyser to be the authoritative
        #           source of what a user can see, thus there's no
        #           authentication that would ensure the user_id being
        #           specified is the id of the user who's making the request.
        #           The system recieving the results should still check to
        #           ensure that any results displayed to an end user are for
        #           repositories that the user has access to.
        # options - A hash of options for Github::Geyser::Rpc::SearchRequest
        #   :dark_ship - A boolean value indicating whether this Geyser query
        #                originated as a dark ship request. (Default false)
        #   :page_number - The page number for paginated search requests.
        #                  (Default 1)
        #   :page_size - The page size for paginated search requests. (Default
        #                10)
        #   :request_id - The ID of the request, used for tracking. (Default nil)
        #   :search_scope - An enum identifying global vs. single repo/org
        #                   queries, one of :standard or :global (Default
        #                   :standard)
        #   :sort_direction - The sort direction, :ASC or :DESC (Default :DESC)
        #   :sort_type - The field to sort on, :BestMatch or :TimeIndexed
        #                (Default nil)
        #   :version - The transport version identifier to use (Default
        #              :GITHUB_SYNTAX_V0)
        def initialize(search_term, repository_ids, user_id, options = {})
          @query = Github::Geyser::Rpc::SearchRequest.new(
            dark_ship: options.fetch(:dark_ship, false),
            page_number: options.fetch(:page_number, 1),
            page_size: options.fetch(:page_size, ::Search::Query::PER_PAGE),
            query: query_field(search_term, options.fetch(:version, nil)),
            repository_ids: repository_ids,
            request_id: options.fetch(:request_id, nil),
            search_scope: search_scope_field(options.fetch(:search_scope, nil)),
            sorting: sorting_field(options.fetch(:sort_type, :nil),
                                   options.fetch(:sort_direction, :nil)),
            user_id: user_id,
          )
        end

        private

        def query_field(search_term, transport_version)
          transport_version ||= :GITHUB_SYNTAX_V0
          Github::Geyser::Rpc::Query.new(serialized: search_term,
                                         version: transport_version)
        end

        def search_scope_field(search_scope)
          case search_scope
          when :global
            :GLOBAL_SCOPE
          else
            :STANDARD_SCOPE
          end
        end

        def sorting_field(type, direction)
          return if type.nil? && direction.nil?
          type = nil unless SORT_TYPES.include?(type&.to_sym)
          direction = nil unless SORT_DIRECTIONS.include?(direction&.to_sym)
          if type && direction
            Github::Geyser::Rpc::Sorting.new(sort_by: type,
                                             direction: direction)
          else
            nil
          end
        end
      end
    end
  end
end
