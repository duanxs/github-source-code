# frozen_string_literal: true

require "geyser-client"

module Search
  module Geyser
    module Requests
      class UpdatePartitions

        attr_reader :query

        # options - A hash of options for Github::Geyser::Rpc::UpdatePartitionsRequest
        #           :name           - A string; the full name of the target Partition to update
        #           :search_status  - A string; if nonempty, the SearchStatus to update the Partition to
        #           :search_schema  - A string; if nonempty, the SearchSchema to update to (NOT typical to alter this!)
        #           :searchable     - A bool or nil; if non-nil, flag the Partition as searchable or not
        #           :writable       - A bool or nil; if non-nil, flag the Partition as writable or not
        #                                            (note: this only effects sync updates to repos in
        #                                             the Partition; SearchStatus effects new repo membership)
        #           :duplicate_of   - A string; if nonempty, full name of the Partition to set as original for target;
        #                                       supplying target Partition's name will promote _it_ to original
        def initialize(options = {})
          @query = Github::Geyser::Rpc::UpdatePartitionsRequest.new({
            partition_updates: [
              Github::Geyser::Rpc::PartitionUpdate.new(
                name:          options[:name],
                search_status: wrap_status(options),
                search_schema: options.fetch(:search_schema, ""),
                duplicate_of:  options.fetch(:duplicate_of, ""),
                searchable:    wrap_bool(options, :searchable),
                writable:      wrap_bool(options, :writable),
              ),
            ],
          })
        end

        private

        def wrap_status(opts)
          return "" unless opts[:search_status]

          status = opts[:search_status].to_s.downcase
          case status
          when "open", "closed", "initializing", "error"
            status
          else
            ""
          end
        end

        def wrap_bool(opts, key)
          return nil if opts[key].nil?

          case opts[key].to_s.downcase
          when "true"
            Google::Protobuf::BoolValue.new(value: true)
          else
            Google::Protobuf::BoolValue.new(value: false)
          end
        end

      end
    end
  end
end
