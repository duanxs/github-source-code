# frozen_string_literal: true

require "geyser-client"

module Search
  module Geyser
    module Requests
      class CreateSearchCollection

        attr_reader :query

        # options - A hash of options for Github::Geyser::Rpc::CreateSearchCollectionRequest
        #           :selector     - A string representing the Collection name suffix in: <search_scope>_<deploy_env>_<selector_suffix>
        #           :search_scope - A string, one of "standard" or "global" (default "standard")
        #           :primary      - A boolean value indicating the Collection should be the Primary in it's scope (default false)
        def initialize(options = {})
          @query = Github::Geyser::Rpc::CreateSearchCollectionRequest.new(
            selector: options.fetch(:selector, ""),
            search_scope: options.fetch(:search_scope, "standard").to_s.downcase,
            primary: format_primary(options),
          )
        end

        private

        def format_primary(opts)
          case opts.fetch(:primary, false).to_s.downcase
          when "true"
            true
          else
            false
          end
        end
      end
    end
  end
end
