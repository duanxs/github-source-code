# frozen_string_literal: true

require "geyser-client"

module Search
  module Geyser
    module Requests
      class CreateSearchPartition

        attr_reader :query

        # options - A hash of options for Github::Geyser::Rpc::CreateSearchPartitionRequest
        #           :collection_name         - A string representing the parent Collection to bind to
        #           :search_schema           - A string representing a valid SearchSchema to bind to
        #           :duplicate_of            - UNUSED. Future: used in DuplicatePartition API for migrations
        #           :search_cluster          - A string overriding Geyser's ES cluster placement logic
        def initialize(options = {})
          @query = Github::Geyser::Rpc::CreateSearchPartitionRequest.new(
            collection_name: options.fetch(:collection_name, ""),
            search_schema:   options.fetch(:search_schema, ""),
            duplicate_of:    options.fetch(:duplicate_of, ""),
            search_cluster:  options.fetch(:search_cluster, ""),
          )
        end
      end
    end
  end
end
