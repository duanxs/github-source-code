# frozen_string_literal: true

require "geyser-client"

module Search
  module Geyser
    module Requests
      class GetAssignments
        DEFAULT_STALENESS = 300
        PAGE_SIZE = 100

        attr_reader :query

        # repository_ids - The IDs of the repositories to return assignments for. And empty array means include all
        # options - A hash of options for Github::Geyser::Rpc::GetAssignmentsRequest
        #           :staleness_gte_seconds - Limits results to those that are more than n seconds stale. (Default 300)
        #           :page                  - The page number for paginated search requests. (Default 1)
        #           :size                  - The page size for paginated search requests. (Defaul 100)
        #           :collection_name       - Limits results to those that are part of named collection. (Default false)
        #           :partition_name        - Limits results to those that are part of named partition. (Default nil)
        #           :cluster_name          - Limits results to those that are part of named cluster. (Default nil)
        #           :search_schema         - Limits results to assignments bound to a partition with the named schema (Default nil)
        def initialize(repository_ids, options = {})
          @query = Github::Geyser::Rpc::GetAssignmentsRequest.new(
            repository_ids: repository_ids,
            staleness_gte_seconds: options.fetch(:staleness_gte_seconds, DEFAULT_STALENESS),
            page: options.fetch(:page, 1),
            size: options.fetch(:size, PAGE_SIZE),
            collection_name: options.fetch(:collection_name, nil),
            partition_name: options.fetch(:partition_name, nil),
            cluster_name: options.fetch(:cluster_name, nil),
            search_schema: options.fetch(:search_schema, nil),
          )
        end
      end
    end
  end
end
