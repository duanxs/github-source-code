# frozen_string_literal: true

require "geyser-client"

module Search
  module Geyser
    module Requests
      class DeleteSearchCollection

        attr_reader :query

        # options - A hash of options for Github::Geyser::Rpc::DeleteSearchCollectionRequest
        #           :collection_name  - A string representing the unique name of the Collection to delete
        def initialize(options = {})
          @query = Github::Geyser::Rpc::DeleteSearchCollectionRequest.new(
            collection_name: options.fetch(:collection_name, "ERR_NO_COLLECTION_NAME_SUPPLIED"),
          )
        end
      end
    end
  end
end
