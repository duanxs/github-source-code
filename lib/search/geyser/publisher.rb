# frozen_string_literal: true

module Search
  module Geyser
    module Publisher
      extend self

      include GitHub::AreasOfResponsibility

      areas_of_responsibility :code_search

      SMALL_LIST_SIZE = 10
      MEDIUM_LIST_SIZE = 5000

      # internal: allows selection of the event publisher appropriate for each call site
      # by applying an override field to the event payload. If the publisher field is
      # not supplied, the standard (analytics-oriented) publisher is applied. Example:
      #
      # payload = {
      #   change: :VISIBILITY_CHANGED,
      #   foo: "bar",
      #   ...
      #   publisher: :low_latency,
      # }
      #
      # GlobalInstrumenter.instrument("search_indexing.repository_changed", payload)
      def override(payload)
        selector = payload.fetch(:publisher, :standard)

        case selector.to_sym
        when :low_latency
          GitHub.low_latency_hydro_publisher
        when :sync
          GitHub.sync_hydro_publisher
        else
          GitHub.hydro_publisher
        end
      end

      # internal: obtain an appropriate publisher selector symbol (not the GitHub.*_hydro_publisher instance!)
      # in async job context, based on the size of the supplied list of Repository IDs
      def selector_by_list_size(list)
        return :standard if list.empty?

        case
        when list.size <= SMALL_LIST_SIZE
          :sync
        when list.size <= MEDIUM_LIST_SIZE
          :low_latency
        else
          :standard
        end
      end
    end
  end
end
