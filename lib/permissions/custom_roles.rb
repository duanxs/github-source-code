# frozen_string_literal: true

module Permissions
  module CustomRoles
    extend self

    # Creates a custom role and the corresponding role_permission records.
    #
    # role  - The Custom Role to persist.
    # fgps  - The FGP identifiers.
    #
    # Returns nothing.
    # Raises CustomRoleError if the role or role_permissions cannot be saved.
    def create!(role, fgps: [])
      fgps_to_add = additional_fgps(fgps, role.base_role)

      Role.transaction do
        RolePermission.transaction do
          raise CustomRoleError.new("could not create role #{role.name}") unless role.save

          fgps_to_add.each do |fgp|
            role_permission = RolePermission.new(
              role_id: role.id,
              fine_grained_permission_id: fgp.id,
              action: fgp.action)

            raise CustomRoleError.new("could not create role_permission for role: #{role.name}, fgp: #{fgp.action}") unless role_permission.save
          end
        end
      end
    end

    # Deletes a custom role and assigns its base role to the previously assigned users/teams/repo invitations
    #
    # role    - custom role to delete
    # actor   - user that deleting the custom role
    #
    # Returns nothing
    def destroy!(role, actor = nil)
      return unless role.custom?

      inherited_base_role = role.base_role.name.to_sym

      user_roles = UserRole.where(actor_type: "User", role_id: role.id)
      unless user_roles.empty?
        org = role.owner
        return unless role.owner.organization?
        Repository.batch_enqueue_update_member(user_roles: user_roles, action: inherited_base_role, organization: org, role: role)
        GitHub.dogstats.gauge("orgs_roles.delete.count", user_roles.length, tags: ["member_type:member"])
      end

      team_roles = UserRole.where(actor_type: "Team", role_id: role.id)
      unless team_roles.empty?
        Team.batch_enqueue_update_repo_permissions(teams_repos: team_roles, action: inherited_base_role, actor_id: actor&.id, role: role)
        GitHub.dogstats.gauge("orgs_roles.delete.count", team_roles.length, tags: ["member_type:team"])
      end

      repo_invitations = RepositoryInvitation.where(role_id: role.id)
      unless repo_invitations.empty?
        RepositoryInvitation.batch_enqueue_update_repo_permissions(invitations: repo_invitations, setter: actor, action: inherited_base_role, role: role)
        GitHub.dogstats.gauge("orgs_roles.delete.count", repo_invitations.length, tags: ["member_type:invitee"])
      end

      if user_roles.empty? && team_roles.empty? && repo_invitations.empty?
        role.destroy!
      else
        GitHub.dogstats.increment("orgs_roles.delete.queued_to_delete", tags: ["status:queued"])
      end
    end

    # Updates the abilities records of users/teams assigned to the custom role.
    #
    # role        - the (custom) Role object to update
    # role_params - a Hash of Role attributes to be updated
    # fgps        - an Array of Fine grained permission identifiers to be given to the role
    # actor       - User that is updating the custom role
    #
    # Returns nothing
    # Raises CustomRoleError if the role or role_permissions cannot be updated.
    def update!(role, role_params:, fgps:, actor: nil)
      return unless role.custom?

      old_role = role.name
      old_base = role.base_role.name
      update_role(role, role_params, fgps)

      unless old_base.eql?(role.base_role.name)
        # udpate underlying ability records
        context = {old_permission: old_role, old_base_role: old_base}
        update_users(role, context: context)
        update_teams(role, actor, context: context)
      end
    end

    # Batch update the abilities records of users assigned to the custom role.
    # Note that org-members can't be assigned an ability below the org default.
    # For those cases we will default to the org default.
    #
    # role    - the (custom) Role object
    # context - Hash of Strings with the users' previous Role {:old_permission, :old_base_role}
    #
    # Returns nothing
    def update_users(role, context: {})
      org = role.owner
      return unless org.organization?

      if Role.greater_or_equal_to_org_default_role?(target: role.name, org: org)
        update_all_users(role, org, context: context)
      else
        update_members_and_collabs_separately(role, org, context: context)
      end
    end

    private

    # update Role metadata, fgps and base role
    def update_role(role, role_params, fgps)
      Role.transaction do
        RolePermission.transaction do
          raise CustomRoleError.new("could not update role #{role.name}") unless role.update(role_params)

          prev_fgps = role.custom_role_permissions.map(&:action)
          fgps_to_remove = prev_fgps - fgps
          fgps_to_add = fgps - prev_fgps

          RolePermission.destroy_by(role: role, action: fgps_to_remove)

          FineGrainedPermission.where(action: fgps_to_add, custom_roles_enabled: true).each do |fgp|
            role_permission = RolePermission.new(role: role, fine_grained_permission: fgp, action: fgp.action)
            raise CustomRoleError.new("could not create role_permission for role: #{role.name}, fgp: #{fgp.action}") unless role_permission.save
          end
        end
      end
    end

    def update_teams(role, actor, context: {})
      team_roles = UserRole.where(actor_type: "Team", role_id: role.id)

      unless team_roles.empty?
        Team.batch_enqueue_update_repo_permissions(teams_repos: team_roles, action: role.name.to_sym, actor_id: actor&.id, context: context)
        GitHub.dogstats.gauge("orgs_roles.update.count", team_roles.length, tags: ["member_type:team"])
      end
    end

    def update_all_users(role, org, context: {})
      user_roles = UserRole.where(actor_type: "User", role_id: role.id)
      unless user_roles.empty?
        batch_update_users(user_roles: user_roles, new_role: role.name.to_sym, action: "update", org: org, context: context)
      end
    end

    def update_members_and_collabs_separately(role, org, context: {})
      user_roles_by_actor = role.user_roles.where(actor_type: "User").index_by(&:actor_id)

      unless user_roles_by_actor.empty?
        actor_ids = user_roles_by_actor.keys
        org_member_ids = org.direct_member_ids
        member_ids, collab_ids = [], []

        actor_ids.each do |id|
          if org_member_ids.include?(id)
            member_ids << id
          else
            collab_ids << id
          end
        end

        unless member_ids.empty?
          member_user_roles = user_roles_by_actor.values_at(*member_ids).compact
          batch_update_users(user_roles: member_user_roles, new_role: org.default_repository_permission.to_sym, action: "update", org: org, context: context)
        end

        unless collab_ids.empty?
          collab_user_roles = user_roles_by_actor.values_at(*collab_ids).compact
          batch_update_users(user_roles: collab_user_roles, new_role: role.name.to_sym, action: "update", org: org, context: context)
        end
      end
    end

    def batch_update_users(user_roles:, new_role:, action:, org:, role: nil, context: {})
      Repository.batch_enqueue_update_member(user_roles: user_roles, action: new_role, organization: org, role: role, context: context)
      GitHub.dogstats.gauge("orgs_roles.#{action}.count", user_roles.length, tags: ["member_type:member"])
    end

    def batch_update_teams(team_roles:, new_role:, actor:, action:, role: nil)
      Team.batch_enqueue_update_repo_permissions(teams_repos: team_roles, action: new_role, actor_id: actor&.id, role: role)
      GitHub.dogstats.gauge("orgs_roles.#{action}.count", team_roles.length, tags: ["member_type:team"])
    end

    def batch_update_repo_invitations(repo_invitations:, new_role:, actor:, action:, role: nil)
      RepositoryInvitation.batch_enqueue_update_repo_permissions(invitations: repo_invitations, setter: actor, action: new_role, role: role)
      GitHub.dogstats.gauge("orgs_roles.#{action}.count", repo_invitations.length, tags: ["member_type:invitee"])
    end

    # We do not need to add the FGPs inherited by the base role.
    # Return only the additional ones.
    #
    # submitted_fgps - Array of FGP identifiers
    # base_role      - The Role object
    #
    # Returns an ActiveRecord::Relation of FineGrainedPermissions
    def additional_fgps(submitted_fgps, base_role)
      return FineGrainedPermission.none if submitted_fgps.nil? || base_role.nil?
      return FineGrainedPermission.none if submitted_fgps.empty?

      inherited_fgps = base_role.permissions.map(&:action)
      fgps = FineGrainedPermission.only_enabled_fgps(submitted_fgps - inherited_fgps)

      FineGrainedPermission.where(action: fgps, custom_roles_enabled: true)
    end
  end
end
