# frozen_string_literal: true

module Permissions
  class CustomRoleFlagSubscriber

    def self.call(name, start, ending, transaction_id, payload)
      return unless valid_disabling_event?(payload)

      org = Organization.find_by(id: payload[:subject_id])
      return unless org

      org.custom_roles.each { |role| Permissions::CustomRoles.destroy!(role) }
    end

    # Public: only listen for custom_role flag disabling events.
    # Gated at the actor level, where the actor is an organization
    def self.valid_disabling_event?(e)
      e[:feature_name] == :custom_roles && e[:operation] == :disable &&
        e[:gate_name] == :actor && e[:subject_class] == "Organization" && e[:subject_id]
    end
  end
end
