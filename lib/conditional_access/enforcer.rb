# frozen_string_literal: true

# The Conditional Access Framework orchestrates the execution of
# policies that need to be met in order to grant access to an org/business
# owned resource.
#
# The enforcer handles point-in-time access to a resource. Policies
# are registered in the enforcer and executed in a predefined order.
# As a module, the enforcer is meant to be included into a class
# (i.e. ApplicationController, Model...), and invoked from an arbitrary choke point.
module ConditionalAccess
  module Enforcer

    Error = Class.new(StandardError)

    # Evaluates all registered policies until the first one that does not satisfy and returns it.
    #
    # This method sequences the execution of all registered policies. Execution will stop after the
    # first policy that fails.
    #
    # The context must implement the following methods to customize the evaluation behaviour:
    # - {policy_id}_enforceable: defines if the policy is enforceable. Used to have classes opt out from a given policy.
    #   If the behaviour is conditional, that must be handled in the applicability phase. It should
    #   be used solely to turn the policy on/off, and it's meant to ease auditability of this opting out.
    #
    # - {policy_id}_applicable: defines if the policy is applicable. This is where conditional logic to determine
    #   if a policy applies to the context or not. Here is were classes usually implement pre-condition checks.
    #
    # - {policy_id}_satisfied: once it has been determined the policy applies for the given context, satisfied determines
    #   if the actual rules of the policy are met.
    #
    # returns a symbol :ok if all policies were met, otherwise a symbol for the first policy that fails
    def evaluate_conditional_access_policies
      start_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
      conditional_access_policies.each do |policy|
        next if enforceable(policy) == :no
        next if applicable(policy) == :no
        next if satisfied(policy) == :yes

        return policy
      end

      :ok
    ensure
      end_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
      GitHub.dogstats.distribution("cap.enforcer.evaluation.dist", (end_time - start_time) * 1_000)
    end

    # Evaluates policies until the first one that does not satisfy, and then enforces it.
    #
    # This is a convenience method for callsites that only care about enforcement.
    # Example usage:
    #
    # return unless enforce_conditional_access_policies == :ok
    #
    # returns :ok if all policies were met, otherwise it returns the symbol representing the name of the
    #         failed policy that was enforced
    def enforce_conditional_access_policies
      unsatisfied_policy = evaluate_conditional_access_policies
      return :ok if unsatisfied_policy == :ok

      enforce(unsatisfied_policy)
      unsatisfied_policy
    end

    # Returns the of policies to be enforced. Classes including this method should define the policies to enforce.
    #
    # returns returns an array of symbols that identify the policies to enforce
    def conditional_access_policies
      raise Error.new("including clases should implement conditional_access_policies method")
    end

    private

    VALID_DECISIONS = [:yes, :no].freeze
    OP_ENFORCEABLE  = "enforceable"
    OP_APPLICABLE   = "applicable"
    OP_SATISFIED    = "satisfied"

    # Returns :yes if the provided policy is enforceable in the current context.
    # Enforceability is a means to make it possible for classes to opt-out from the enforcement.
    # Every policy is enforceable by default, unless specified otherwise explicitly.
    #
    # For example, for a SAML policy, this would allow SAML-related controllers to opt-out
    # from the enforcement altogether.
    #
    # policy - Symbol that identifies a specific policy. It must be registered
    #          in the policy registry
    #
    # returns :yes if the policy is enforceable, :no otherwise
    def enforceable(policy)
      run_operation(policy: policy, operation: OP_ENFORCEABLE, default_decision: :yes)
    end

    # Returns :yes if the provided policy is applicable in the current context.
    # A policy being applicable means the conditions provided by the context determine
    # the policy is elegible to be enforced.
    #
    # For example, in a SAML enforced Organization, applicable will determine if SAML
    # enforcement has been enabled for the owner of a resource.
    #
    # policy - Symbol that identifies a specific policy. It must be registered
    #          in the policy registry
    #
    # returns :yes if the policy is applicable, :no otherwise
    def applicable(policy)
      run_operation(policy: policy, operation: OP_APPLICABLE, default_decision: :yes)
    end

    # Returns :yes if the provided policy identifier is satisfied in the current context.
    # A policy being satisfied means that the policy rules or conditions are met within the
    # specified context.
    #
    # policy - Symbol that identifies a specific policy. It must be registered
    #          in the policy registry
    #
    # returns :yes if the policy is satisfied, :no otherwise
    def satisfied(policy)
      run_operation(policy: policy, operation: OP_SATISFIED, default_decision: :no)
    end

    # Performs the action needed when a policy is not met. This could be a status 4XX, a redirection,
    # or prompting the user to perform some action to satisfy the policy, like performing SSO.
    #
    # policy - Symbol that identifies a specific policy. It must be registered
    #          in the policy registry
    #
    # returns nil
    def enforce(policy)
      start_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
      raise_if_unknown_policy(policy)

      method = "#{policy}_enforce".to_sym
      raise Error.new("#{method} not implemented") unless respond_to?(method, true)

      self.send(method)
      nil
    ensure
      end_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
      tags = ["policy:#{policy}", "operation:enforce"]
      GitHub.dogstats.distribution("cap.enforcer.operation.dist", (end_time - start_time) * 1_000, tags: tags)
    end

    def run_operation(policy:, operation:, default_decision:)
      start_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
      begin
        raise_if_unknown_policy(policy)

        method = "#{policy}_#{operation}".to_sym
        return default_decision unless respond_to?(method, true)

        decision = self.send(method)
        return decision if VALID_DECISIONS.include?(decision)

        raise Error.new("invalid decision returned by #{method} - should be any of #{VALID_DECISIONS}")
      rescue StandardError => ex
        GitHub::Logger.log_exception({class: self.class.name, service: "cap"}, ex)
        raise ex
      ensure
        end_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
        tags = ["policy:#{policy}", "operation:#{operation}"]
        GitHub.dogstats.distribution("cap.enforcer.operation.dist", (end_time - start_time) * 1_000, tags: tags)
      end
    end

    def raise_if_unknown_policy(policy)
      raise Error.new("unknown policy #{policy}") unless conditional_access_policies.include?(policy)
    end
  end
end
