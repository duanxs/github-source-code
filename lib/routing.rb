# frozen_string_literal: true

module Routing
  extend ActiveSupport::Concern
  include Rails.application.routes.url_helpers

  included do
    def default_url_options
      # I prefer parsing the host and scheme from GitHub.url for clarity but
      # we could also use ActionMailer::Base.default_url_options here which
      # returns the hash we need.
      uri = URI.parse(GitHub.url)

      {host: uri.host, protocol: uri.scheme}
    end
  end
end
