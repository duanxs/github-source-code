# rubocop:disable Style/FrozenStringLiteralComment

module Flipper
  module Adapters
    class Memcacheable
      include ::Flipper::Adapter

      FEATURES_KEY = "flipper_features".freeze
      TTL = 30 # seconds

      def self.key_for(feature_key)
        "flipper_feature:#{feature_key}"
      end

      def self.key_for_actor(feature_key, actor_id)
        "flipper_feature_actor:#{feature_key}:#{actor_id}"
      end

      attr_reader :name

      # Public
      def initialize(adapter, cache)
        @name = :memcacheable
        @adapter = adapter
        @cache = cache
      end

      # Public
      def features
        cache.fetch(FEATURES_KEY, ttl: TTL) { @adapter.features }
      end

      # Public
      def add(feature)
        result = @adapter.add(feature)
        # This only works in the primary datacenter. Secondary datacenters must rely
        # on cache expiration for changes to apply.
        cache.delete(FEATURES_KEY)
        result
      end

      # Public
      def remove(feature)
        result = @adapter.remove(feature)
        # This only works in the primary datacenter. Secondary datacenters must rely
        # on cache expiration for changes to apply.
        cache.delete(FEATURES_KEY)
        cache.delete(feature_key(feature))
        result
      end

      # Public
      def clear(feature)
        result = @adapter.clear(feature)
        # This only works in the primary datacenter. Secondary datacenters must rely
        # on cache expiration for changes to apply.
        cache.delete(feature_key(feature))
        result
      end

      # Public
      def get(feature)
        cache.fetch(feature_key(feature), ttl: TTL) { @adapter.get(feature) }
      end

      def get_multi(features)
        keys = features.map { |feature| feature_key(feature) }
        result = cache.get_multi(keys)
        uncached_features = features.reject { |feature| result[feature_key(feature)] }

        if uncached_features.any?
          response = @adapter.get_multi(uncached_features)
          response.each do |key, value|
            cache.set(key_for(key), value, TTL)
            result[key] = value
          end
        end

        result
      end

      # Public
      def enable(feature, gate, thing)
        result = @adapter.enable(feature, gate, thing)
        # This only works in the primary datacenter. Secondary datacenters must rely
        # on cache expiration for changes to apply.
        cache.delete(feature_key(feature))
        if gate.name == :actor
          cache.delete(key_for_actor(feature.key, thing.value))
        end
        result
      end

      # Public
      def disable(feature, gate, thing)
        result = @adapter.disable(feature, gate, thing)
        # This only works in the primary datacenter. Secondary datacenters must rely
        # on cache expiration for changes to apply.
        cache.delete(feature_key(feature))
        if gate.name == :actor
          cache.delete(key_for_actor(feature.key, thing.value))
        end
        result
      end

      # Public. Look up a feature value for the given actor.
      def feature_enabled?(feature_key, actor_id)
        # wrap the cached value in an array to avoid any weirdness
        # related to checking if 'false' is cached.
        result = cache.fetch(key_for_actor(feature_key, actor_id), ttl: TTL) { [@adapter.feature_enabled?(feature_key, actor_id)] }
        result.first
      end

      # Public.
      def actors_value(feature_key)
        @adapter.actors_value(feature_key)
      end

      private

      attr_reader :cache

      def feature_key(feature)
        key_for(feature.key)
      end

      def key_for_actor(feature_key, actor_id)
        self.class.key_for_actor(feature_key, actor_id)
      end

      def key_for(feature_key)
        self.class.key_for(feature_key)
      end
    end
  end
end
