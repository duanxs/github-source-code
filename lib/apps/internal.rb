# frozen_string_literal: true

# Maps identity to configuration of GitHub Apps that are owned and operated
# by GitHub and its partners.
module Apps
  class Internal
    TRUTHY = ->(*) { true }
    FALSEY = ->(*) { false }

    # Eventually move this to a database
    def self.default_configuration
      {
        "Integration" => [
          {
            alias: :global, # Applies to all Integrations
            id: ->(*) { nil },
            capabilities: {
              abuse_limit_multiplier: false, # https://github.com/github/ecosystem-api/issues/1860
              auto_upgrade_permissions: ->(app) { app&.connect_app? },
              can_auto_approve_oauth_authorization: false, # https://github.com/github/github/issues/117804
              can_install_on_security_advisory_repos: false, # https://github.com/github/ecosystem-apps/issues/791
              can_receive_lightweight_access_token_response: false, # https://github.com/github/ecosystem-apps/issues/672
              can_set_loopback_webhook: false, # Used for Actions on GHES
              custom_instrumentation_integration_installation: false,
              enforce_internal_access_on_token_generation: false, # https://github.com/github/github/pull/141261
              extend_access_token_expiry: false, # https://github.com/github/ecosystem-apps/issues/792
              follow_repository_transfers: false,
              installed_globally: false,
              ip_whitelist_exempt: false, # Applies to user-to-server only, installations are already exempt.
              per_pull_request_permissions: false, # https://github.com/github/ce-engineering/issues/289
              per_repo_rate_limit: false,
              per_repo_user_to_server_tokens: false,
              per_repo_user_to_server_tokens_required: false, # Enforce that only scoped user-to-server tokens can be created
              restricted_modification_of_public_resources: false,
              saml_sso_required: true, # Applies to user-to-server only, installations are already exempt.
              skip_installation_creation_audit_log: false,
              skip_version_update_audit_log: false,
              read_flipper_features: false,
              user_installable: true,
            },
            properties: {},
            can_auto_install: {},
            owners: ["@github/ecosystem-apps"],
          },
          {
            alias: :internal, # Applies to all configured Internal Apps, overrides Global configuration
            id: ->(*) { nil },
            capabilities: {
              enforce_internal_access_on_token_generation: true, # https://github.com/github/github/pull/146162
              limited_access: ->(app) {
                Apps::Internal.capable?(:installed_globally, app: app)
              },
            },
            properties: {},
            can_auto_install: {},
            owners: ["@github/ecosystem-apps"],
          },
          Apps::Internal::Noodle::PRODUCTION,
          Apps::Internal::Actions::PRODUCTION,
          Apps::Internal::Actions::LAB,
          Apps::Internal::CodeScanning::PRODUCTION,
          Apps::Internal::Slack::PRODUCTION,
          Apps::Internal::StatusPage::PRODUCTION,
          {
            alias: :dependabot,
            id: ->() { GitHub.dependabot_github_app&.id },
            capabilities: {
              auto_upgrade_permissions: true,
              can_install_on_security_advisory_repos: true,
              can_receive_lightweight_access_token_response: true,
              enforce_internal_access_on_token_generation: true, # https://github.com/github/github/pull/147001
              follow_repository_transfers: true,
              installed_globally: true,
              ip_whitelist_exempt: true,
              skip_version_update_audit_log: true,
              user_installable: false,
            },
            properties: {},
            can_auto_install: {
              "AutomaticAppInstallation::Handlers::FileAdded" => ->(opts = {}) {
                return false unless opts[:repo]
                GitHub.dependabot_enabled?
              },
            },
            owners: [],
          },
          Apps::Internal::TeamSync::PRODUCTION,
          Apps::Internal::LearningLab::PRODUCTION,
          Apps::Internal::LearningLab::STAGING,
          Apps::Internal::Pages::INTEGRATION,
          Apps::Internal::PolicyService::DEV,
          Apps::Internal::PolicyService::PRODUCTION,
          Apps::Internal::PullPanda::PRODUCTION,
          *Apps::Internal::Codespaces::GITHUB_APPS,
        ],
        "OauthApplication" => [
          {
            alias: :global,
            id: ->(*) { nil },
            capabilities: {
              can_auto_approve_oauth_authorization: false, # https://github.com/github/github/issues/117804
              ip_whitelist_exempt: false,
              saml_sso_required: true,
              read_flipper_features: false,
              mobile_client: false,
            },
            properties: {},
            can_auto_install: {},
            owners: ["@github/ecosystem-apps"],
          },
          {
            alias: :internal, # Applies to all configured Internal Apps, overrides Global configuration
            id: ->(*) { nil },
            capabilities: {},
            properties: {},
            can_auto_install: {},
            owners: ["@github/ecosystem-apps"],
          },
          *Apps::Internal::Codespaces::OAUTH_APPS,
          *Apps::Internal::GitHubEducation::OAUTH_APPS,
          *Apps::Internal::Gist::OAUTH_APPS,
          Apps::Internal::GitHubImporter::PRODUCTION,
          Apps::Internal::GitHubSpamurai::PRODUCTION,
          Apps::Internal::HelpHub::PRODUCTION,
          Apps::Internal::HelpHub::STAGING,
          Apps::Internal::Mobile::ANDROID,
          Apps::Internal::Mobile::IOS,
          Apps::Internal::Pages::OAUTH,
        ],
      }
    end

    # Public: The Integration model for the configured 'alias'
    #
    # app_alias  - Symbol: the CONFIGURATION alias assigned to the Integration
    #
    # Returns a Integration or nil.
    def self.integration(app_alias)
      ActiveRecord::Base.connected_to(role: :reading) do
        id_finder = integration_configuration_for(app_alias).fetch(:id, ->() { nil })
        Integration.find_by_id(id_finder.call)
      end
    end

    # Public: The OauthApplication model for the configured 'alias'
    #
    # alias  - Symbol: the CONFIGURATION alias assigned to the OauthApplication
    #
    # Returns a OauthApplication or nil.
    def self.oauth_application(app_alias)
      ActiveRecord::Base.connected_to(role: :reading) do
        id_finder = oauth_configuration_for(app_alias).fetch(:id, ->() { nil })
        OauthApplication.find_by_id(id_finder.call)
      end
    end

    # Public: is the given app configured to be capable of performing the
    # action.
    #
    # action  - Symbol: represents a potentially configured capability.
    # app     - Integration or OauthApplication: the App to check capability
    #           for.
    #
    # Returns a Boolean.
    def self.capable?(action, app:)
      return false if app.nil?
      global_config = Apps::Internal::Registry.find_configuration(app_alias: :global, type: app.class.name)
      internal_config = Apps::Internal::Registry.find_configuration(app_alias: :internal, type: app.class.name)
      app_config = Apps::Internal::Registry.find_configuration(id: app.id, type: app.class.name)

      global_capabilities = global_config.fetch(:capabilities, {})

      all_capabilities =
        if app_config.any?
          # The priority of configuration is (highest to lowest):
          # 1. app_config
          # 2. internal_config
          # 3. global_config
          global_capabilities.merge(internal_config.fetch(:capabilities, {})).
            merge(app_config.fetch(:capabilities, {}))
        else
          global_capabilities
        end

      capability = all_capabilities.fetch(action, false)
      capability.respond_to?(:call) ? capability.call(app) : !!capability
    end

    def self.can_auto_install?(trigger_handler, app, opts = {})
      app_config = Apps::Internal::Registry.find_configuration(id: app.id, type: app.class.name)

      return true if app_config.empty? # Apps that have no explicit checks for this trigger handler can go ahead and install

      app_config.fetch(:can_auto_install, {}).fetch(trigger_handler.to_s, FALSEY).call(opts)
    end

    # Public: Checks if the internal app has access to the specified repository based on
    # the limited access configuration.
    #
    # repo  - The Repository to check.
    # app   - Integration or OauthApplication: the App to check access for.
    #
    # Returns a boolean.
    def self.repo_accessible_to_limited_app?(repo, app:)
      return true unless Apps::Internal.capable?(:limited_access, app: app)

      accessible_targets = Apps::Internal.property(:accessible_targets, app: app) || {}
      accessible_targets.transform_keys!(&:downcase)

      target_key = repo.owner_login.downcase
      return false if accessible_targets.empty? || !accessible_targets.has_key?(target_key)

      accessible_repositories = accessible_targets[target_key].map(&:downcase)
      return true if accessible_repositories.empty?

      accessible_repositories.include?(repo.name.downcase)
    end

    # Public: the value configured for the given property name for this App.
    #
    # name    - Symbol: the name of the configured property.
    # app     - Integration or OauthApplication: the App for which to retrieve
    #         a property.
    #
    # Returns the value configured for this property name and App or nil if the
    # App does not have the given property.
    def self.property(name, app:)
      app_config = Apps::Internal::Registry.find_configuration(id: app.id, type: app.class.name)

      return nil if app_config.empty?

      property = app_config.fetch(:properties).fetch(name, nil)
      property.respond_to?(:call) ? property.call : property
    end

    # Internal: The configuration Hash for the given alias and type of App.
    #
    # app_alias   - Symbol: the CONFIGURATION alias assigned to the App.
    # type    - Class: One of Integration or OauthApplication.
    #
    # Returns a Hash containing the configuration for the App.
    def self.configuration_for(app_alias, type)
      app_config = Apps::Internal::Registry.find_configuration(app_alias: app_alias, type: type.name)
    end

    # Internal: The configuration Hash for the named Integration.
    #
    # app_alias  - Symbol: the CONFIGURATION alias assigned to the Integration.
    #
    # Returns a Hash containing the configuration for the Integration.
    def self.integration_configuration_for(app_alias)
      configuration_for(app_alias, Integration)
    end

    # Internal: The configuration Hash for the named OauthApplication.
    #
    # app_alias  - Symbol: the CONFIGURATION alias assigned to the OauthApplication.
    #
    # Returns a Hash containing the configuration for the OauthApplication.
    def self.oauth_configuration_for(app_alias)
      configuration_for(app_alias, OauthApplication)
    end

    def self.integration_id(app_alias)
      configuration = integration_configuration_for(app_alias)

      ActiveRecord::Base.connected_to(role: :reading) do
        configuration.fetch(:id).call
      end
    rescue KeyError
      raise ArgumentError, "Configuration not found for #{app_alias} integration"
    end
  end
end
