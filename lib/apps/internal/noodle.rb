# frozen_string_literal: true

module Apps
  class Internal
    class Noodle
      APP_NAME = "noodle (production)"

      def self.id_finder
        ->() {
          Integration.find_by(
            owner_id: GitHub.trusted_apps_owner_id,
            name: APP_NAME,
          )&.id
        }
      end

      PRODUCTION = {
        alias: :noodle_production,
        id: id_finder,
        capabilities: {
          auto_upgrade_permissions: true,
          can_auto_approve_oauth_authorization: true, # https://github.com/github/github/issues/117804
          user_installable: false,
        },
        properties: {},
        can_auto_install: {},
        owners: [],
      }
    end
  end
end
