# frozen_string_literal: true

module Apps
  class Internal
    class Slack

      PRODUCTION = {
        alias: :slack,
        id: ->() { ENV.fetch("SLACK_INTEGRATION_ID", 7100) },
        capabilities: {
          enforce_internal_access_on_token_generation: false,
          ip_whitelist_exempt: true,
        },
        properties: {},
        can_auto_install: {},
        owners: [],
      }

    end
  end
end
