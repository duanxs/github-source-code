# frozen_string_literal: true

module Apps
  class Internal
    class Mobile
      ANDROID = {
        alias: :android_mobile,
        id: ->() {
          OauthApplication.where(
            user_id: GitHub.trusted_apps_owner_id,
            name: "GitHub Android",
          ).pluck(:id).first
        },
        capabilities: {
          can_auto_approve_oauth_authorization: false, # https://github.com/github/github/issues/117804
          mobile_client: true,
        },
        properties: {},
        can_auto_install: {},
        owners: [],
      }

      IOS = {
        alias: :ios_mobile,
        id: ->() {
          OauthApplication.where(
            user_id: GitHub.trusted_apps_owner_id,
            name: "GitHub iOS",
          ).pluck(:id).first
        },
        capabilities: {
          can_auto_approve_oauth_authorization: false, # https://github.com/github/github/issues/117804
          mobile_client: true,
        },
        properties: {},
        can_auto_install: {},
        owners: [],
      }
    end
  end
end
