# frozen_string_literal: true

module Apps
  class Internal
    class StatusPage
      APP_NAME = "GitHub Internal statuspage"

      def self.id_finder(name)
        -> () {
          Integration.find_by(
            name: name,
            owner: GitHub.trusted_oauth_apps_owner
          )&.id
        }
      end

      PRODUCTION = {
        alias: :github_internal_statuspage,
        id: id_finder(APP_NAME),
        capabilities: {
          enforce_internal_access_on_token_generation: false,
          read_flipper_features: true, # https://github.com/github/security-iam/issues/2244
        },
        properties: {},
        can_auto_install: {},
        owners: [],
      }
    end
  end
end
