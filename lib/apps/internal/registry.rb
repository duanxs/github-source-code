# frozen_string_literal: true

# Management and storage of configuration for internal GitHub and OAuth Apps.
module Apps
  class Internal
    class Registry
      NIL_ID = ->(*) { nil }
      VALID_APP_TYPES = %w(Integration OauthApplication)

      include Singleton

      class InvalidConfigurationError < ArgumentError; end

      # Public: add/modify a configuration entry in the internal Apps
      # registry.
      #
      # app               - Integration/OauthApplication (optional): The App
      #                     that is being configured. Either supply the `app` OR
      #                     `type` argument.
      # type              - String (optional): One of "Integration" or
      #                     "OauthApplication". Allows the "global" configuration
      #                     to be modified without passing an `app`.
      # app_alias         - Symbol (required): The alias to identify the
      #                     configuration entry. Note: the configuration entry
      #                     associated with this alias will be updated in the
      #                     case that `type` is supplied OR the `app` cannot be
      #                     found in the configuration. This allows a
      #                     configuration entry to be updated so that an alias
      #                     points to a different App.
      # id                - Proc (optional): A callable that should return the
      #                     database ID of the configured App. The Proc may
      #                     return `nil`. If not supplied, a default Proc that
      #                     returns `nil` will be used.
      # capabilities      - Hash (optional): The internal capabilities granted
      #                     to this App. See Apps::Internal::CONFIGURATION for
      #                     examples.
      # properties        - Hash (optional): The properties of this App.  See
      #                     Apps::Internal::CONFIGURATION for examples.
      # can_auto_install  - Hash (optional): A mapping of
      #                     AutomaticAppInstallation::TRIGGER_HANDLERS to
      #                     callables.
      #
      # Returns a Hash: The modifed configuration of the Registry.
      def self.configure(app: nil, type: nil, app_alias:, id: NIL_ID, capabilities: {}, can_auto_install: {}, properties: {}, owners: [])
        instance.configure(
          app: app,
          type: type,
          app_alias: app_alias,
          id: id,
          capabilities: capabilities,
          properties: properties,
          can_auto_install: can_auto_install,
          owners: [],
        )
      end

      # Public: the current configuration.
      #
      # Note: Always returns Apps::Internal::CONFIGURATION in Production.
      #
      # Returns a Hash: The current configuration of the Registry.
      def self.configuration
        instance.configuration
      end

      # Public: reset the configuration back to its original state.
      #
      # Note: The original configuration of the Registry will always be as it
      # appears in Production.
      #
      # Returns a Hash: The original configuration of the Registry.
      def self.reset_configuration!
        instance.reset_configuration!
      end

      # Public: Find an App's configuration.
      #
      # alias - Symbol: The configured alias of the Integration or
      #         OauthApplication
      # id    - Integer: The database ID of the Integration or OauthApplication
      # type  - String: The class of the App: One of 'Integration' or
      #         'OauthApplication'
      #
      # Returns a configuration Hash for the App or empty Hash if the App has
      # not been configured.
      def self.find_configuration(app_alias: nil, id: nil, type:)
        instance.find_configuration(app_alias: app_alias, id: id, type: type)
      end

      # Public: All App aliases configured, regardless of type. Does not
      # include the default (global or internal) aliases.
      #
      # Returns an Array of Symbols.
      def self.all_aliases
        instance.configuration.flat_map do |_, configs|
          configs.reject do |config|
            config[:alias] == :global || config[:alias] == :internal
          end.map { |c| c[:alias] }
        end.uniq
      end

      # Public: the number of configured internal Apps.
      #
      # type  - String (optional). Either "Integration" or "OauthApplication".
      #         When not supplied returns the count of both types of App.
      #
      # Returns an Integer.
      def self.count_apps(type: nil)
        configs =
          case type
          when "Integration"; instance.configuration["Integration"]
          when "OauthApplication"; instance.configuration["OauthApplication"]
          else; instance.configuration["Integration"] + instance.configuration["OauthApplication"]
          end

        configs.reduce(0) do |total, app|
          total += 1 unless app[:alias] == :global || app[:alias] == :internal
          total
        end
      end

      # Internal: Add or modify a configuration entry in the Registry.
      #
      # Note: Don't call this method directly. See Apps::Internal.configure for
      # the public documentation.
      #
      # Returns a Hash: The modified configuration of the Registry.
      def configure(app: nil, type: nil, app_alias:, id: NIL_ID, capabilities: {}, can_auto_install: {}, properties: {}, owners: [])
        return configuration unless registry_configurable?

        raise ArgumentError.new("Supply `app` or `type` keyword") unless (app || %w(Integration OauthApplication).include?(type))

        type = app.present? ? app.class.name : type
        config = nil

        config = defined?(@configuration) ? @configuration : default_configuration

        if app.nil?
          config[type].delete_if { |a| a[:alias] == app_alias }
        else
          config[type].delete_if { |a| a[:id].call == app.id || a[:alias] == app_alias }
        end

        config[type].push(
          {
            alias: app_alias,
            id: id,
            capabilities: capabilities,
            properties: properties,
            can_auto_install: can_auto_install,
            owners: owners,
          },
        )

        @configuration = validate_configuration!(config)
      end

      def configuration
        return @configuration if defined?(@configuration)

        @configuration = validate_configuration!(default_configuration)
      end

      def reset_configuration!
        return unless registry_configurable?
        @configuration = validate_configuration!(default_configuration)
      end

      # Internal: Find an App's configuration.
      #
      # alias - Symbol: The configured alias of the Integration or
      #         OauthApplication
      # id    - Integer: The database ID of the Integration or OauthApplication
      # type  - String: The class of the App: One of 'Integration' or
      #         'OauthApplication'
      #
      # Returns a configuration Hash for the App or empty Hash if the App has
      # not been configured.
      def find_configuration(app_alias: nil, id: nil, type:)
        empty_config = {}
        return empty_config unless id || app_alias

        unless VALID_APP_TYPES.include?(type)
          raise ArgumentError.new("supply a valid `type` argument. One of: #{VALID_APP_TYPES}")
        end

        # This check pre-loads the configuration the first time this method is called.
        return empty_config if configuration.empty?

        index =
          if app_alias.present?
            index_of_alias(app_alias, type)
          elsif id.present?
            index_of_id(id, type)
          end

        index && configuration.fetch(type)[index] || empty_config
      end

      private

      # Private: preloads all App IDs based on their configured callables and
      # creates indexes that are used to efficiently lookup a configured App
      # based on either its ID or alias.
      #
      # config  - Hash. A valid Apps::Internal configuration.
      #
      # Returns a Hash: the unmodified config.
      def preload_app_ids(config)
        start_time = Time.now
        @index_of_configurations_by_app_id = {}
        @index_of_configurations_by_alias = {}

        ActiveRecord::Base.connected_to(role: :reading) do
          VALID_APP_TYPES.each do |type|
            config[type].each_with_index do |config, index|
              @index_of_configurations_by_alias["#{type}:#{config[:alias]}"] = index

              app_id = config[:id].call
              if app_id
                @index_of_configurations_by_app_id["#{type}:#{app_id}"] = index
              end
            end
          end
        end

        duration_in_ms = (Time.now - start_time) * 1_000
        GitHub.dogstats.timing("apps_internal.registry.preload_app_ids", duration_in_ms)
        GitHub.dogstats.gauge("apps_internal.registry.configured_apps.count", @index_of_configurations_by_app_id.size)

        config
      end

      # Private: A safely modifiable copy of the default configuration values.
      #
      # Returns a Hash.
      def default_configuration
        # Use Rails' Object#deep_dup method instead of Marshal.dump here
        # because the configuration contains Procs, which can't be serialized
        # by Marshal.
        Apps::Internal.default_configuration.deep_dup
      end

      def registry_configurable?
        Rails.test? || Rails.development?
      end

      def validate_configuration!(config)
        validator = Apps::Internal::ConfigurationValidator.new(config)
        raise InvalidConfigurationError.new(validator.error_message) unless validator.valid?
        config = preload_app_ids(config)

        config
      end

      def index_of_alias(app_alias, type)
        return nil unless @index_of_configurations_by_alias
        @index_of_configurations_by_alias["#{type}:#{app_alias}"]
      end

      def index_of_id(id, type)
        return nil unless @index_of_configurations_by_app_id
        @index_of_configurations_by_app_id["#{type}:#{id}"]
      end
    end
  end
end
