# frozen_string_literal: true

module Apps
  class Internal
    class Actions

      def self.id_finder
        ->() {
          Integration.find_by(
            owner_id: GitHub.trusted_apps_owner_id,
            name: GitHub.launch_github_app_name,
          )&.id
        }
      end

      FILE_ADDED_DECISION = ->(opts = {}) {
        return false unless opts[:repo]
        GitHub.actions_enabled?
      }

      PRODUCTION = {
        alias: :actions,
        id: id_finder,
        capabilities: {
          abuse_limit_multiplier: true, # https://github.com/github/ecosystem-api/issues/1860
          auto_upgrade_permissions: true,
          can_install_on_security_advisory_repos: true, # https://github.com/github/c2c-actions-experience/issues/2474
          can_receive_lightweight_access_token_response: true,
          can_set_loopback_webhook: true,
          custom_instrumentation_integration_installation: true,
          # Do not enforce internal access for the local environment, but require it for production.
          enforce_internal_access_on_token_generation: !Rails.development?, # https://github.com/github/github/pull/141261
          extend_access_token_expiry: true, # https://github.com/github/ecosystem-apps/issues/792
          follow_repository_transfers: true,
          per_pull_request_permissions: true, # https://github.com/github/ce-engineering/issues/289
          per_repo_rate_limit: true, # https://github.com/github/c2c-actions-experience/issues/1968
          restricted_modification_of_public_resources: true, # https://github.com/github/ecosystem-apps/issues/581
          skip_installation_creation_audit_log: true,
          skip_scoped_installation_audit_log: true,
          skip_version_update_audit_log: true,
          user_installable: false,
        },
        properties: {},
        can_auto_install: {
          "AutomaticAppInstallation::Handlers::FileAdded" => FILE_ADDED_DECISION,
        },
        owners: [],
      }

      LAB = {
        alias: :actions_lab,
        id: ->() { GitHub.launch_lab_github_app&.id }, # TODO: Inline the logic from lib/github/config/launch.rb
        capabilities: {
          auto_upgrade_permissions: true,
          can_install_on_security_advisory_repos: true,
          can_receive_lightweight_access_token_response: true,
          enforce_internal_access_on_token_generation: true, # https://github.com/github/github/pull/141261
          extend_access_token_expiry: true, # https://github.com/github/ecosystem-apps/issues/792
          follow_repository_transfers: true,
          per_pull_request_permissions: true, # https://github.com/github/ce-engineering/issues/289
          per_repo_rate_limit: true, # https://github.com/github/c2c-actions-experience/issues/1968
          restricted_modification_of_public_resources: true, # https://github.com/github/ecosystem-apps/issues/581
          skip_version_update_audit_log: true,
          user_installable: false,
        },
        properties: {},
        can_auto_install: {
          "AutomaticAppInstallation::Handlers::FileAdded" => FILE_ADDED_DECISION,
        },
        owners: [],
      }

      # TODO: Use the resource registry
      PERMISSIONS = {
        "actions"              => :write,
        "checks"               => :write,
        "contents"             => :write,
        "deployments"          => :write,
        "issues"               => :write,
        "metadata"             => :read,
        "packages"             => :write,
        "pages"                => :write,
        "pull_requests"        => :write,
        "repository_hooks"     => :write,
        "repository_projects"  => :write,
        "security_events"      => :write,
        "statuses"             => :write,
        "vulnerability_alerts" => :read,
      }

      def self.seed_database!(app_url:, webhook_url:, webhook_secret:, insecure_ssl:, client_key:, client_secret:, public_key:)
        return if Apps::Internal::Actions.id_finder.call.present?

        integration_attributes = {
          owner: GitHub.trusted_oauth_apps_owner,
          name: GitHub.launch_github_app_name,
          url: app_url,
          key: client_key,
          secret: client_secret,
          public: true,
          full_trust: true,
          skip_restrict_names_with_github_validation: true,
          skip_generate_slug: true,
          no_repo_permissions_allowed: true,
        }
        app = Integration.create!(integration_attributes)

        app.update!({hook_attributes: {
          url: webhook_url, secret: webhook_secret, insecure_ssl: insecure_ssl, active: true
        }})

        app.update!({
          default_events: CheckSuite::ACTIONS_WEBHOOK_EVENTS - ["security_events", "workflow_run"],
          default_permissions: PERMISSIONS
        })

        if public_key.length > 0
          app.public_keys.create(creator: GitHub.trusted_oauth_apps_owner, skip_generate_key: true, public_pem: public_key)
        end

        create_integration_trigger(app)
        app
      end

      def self.create_integration_trigger(app)
        trigger_attributes = {
          install_type: "file_added",
          path: '\A\.github/(?:main\.workflow|workflows/[^/]+\.ya?ml)\z',
          reason: "",
          deactivated: false,
          integration_id: app.id,
        }
        IntegrationInstallTrigger.where(integration: app).delete_all
        IntegrationInstallTrigger.create!(trigger_attributes)
      end

    end
  end
end
