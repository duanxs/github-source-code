# frozen_string_literal: true

module Apps
  class Internal
    class Codespaces
      def self.integration_id_finder(key)
        ->() {
          Integration.find_by(key: key)&.id
        }
      end

      def self.oauth_app_id_finder(key)
        ->() {
          OauthApplication.find_by(key: key)&.id
        }
      end

      PRODUCTION = {
        alias: GitHub::Config::VSCS_ENVIRONMENTS[:production][:integration_alias],
        id: integration_id_finder(GitHub.codespaces_app_key),
        capabilities: {
          access_codespaces: true,
          can_auto_approve_oauth_authorization: true,
          enforce_internal_access_on_token_generation: false,
          installed_globally: true,
          limited_access: false,
          per_repo_user_to_server_tokens: true,
          per_repo_user_to_server_tokens_required: true, # Enforce that only scoped user-to-server tokens can be created
          user_installable: false,
        },
        properties: {},
        can_auto_install: {},
        owners: ["@github/codespaces"],
      }

      PPE = {
        alias: GitHub::Config::VSCS_ENVIRONMENTS[:ppe][:integration_alias],
        id: integration_id_finder(GitHub.codespaces_app_ppe_key),
        capabilities: {
          access_codespaces: true,
          enforce_internal_access_on_token_generation: false,
          can_auto_approve_oauth_authorization: true,
          installed_globally: true,
          per_repo_user_to_server_tokens: true,
          per_repo_user_to_server_tokens_required: true,
          user_installable: false,
        },
        properties: {
          accessible_targets: {
            "bookish-potato" => [],
          }
        },
        can_auto_install: {},
        owners: ["@github/codespaces"],
      }

      DEV = {
        alias: GitHub::Config::VSCS_ENVIRONMENTS[:development][:integration_alias],
        id: integration_id_finder(GitHub.codespaces_app_dev_key),
        capabilities: {
          access_codespaces: true,
          can_auto_approve_oauth_authorization: true,
          enforce_internal_access_on_token_generation: false,
          installed_globally: true,
          per_repo_user_to_server_tokens: true,
          per_repo_user_to_server_tokens_required: true,
          user_installable: false,
        },
        properties: {
          accessible_targets: {
            "bookish-potato" => [],
          }
        },
        can_auto_install: {},
        owners: ["@github/codespaces"],
      }

      VSCODE_AUTH_PROVIDER_KEY = "Iv1.ae51e546bef24ff1"

      VSCODE_AUTH_PROVIDER = {
        alias: :vscode_auth_provider,
        id: integration_id_finder(VSCODE_AUTH_PROVIDER_KEY),
        capabilities: {
          access_codespaces: true,
          enforce_internal_access_on_token_generation: false,
          per_repo_user_to_server_tokens: true,
        },
        properties: {},
        can_auto_install: {},
        owners: ["@github/codespaces"],
      }

      # These are all OAuth apps:

      VSCODE_OSS = {
        alias: :vscode_oss,
        id: oauth_app_id_finder("a5d3c261b032765a78de"),
        capabilities: {
          access_codespaces: true,
          enforce_internal_access_on_token_generation: false,
        },
        properties: {},
        can_auto_install: {},
        owners: ["@github/codespaces"],
      }

      VSCODE_EXPLORATION = {
        alias: :vscode_exploration,
        id: oauth_app_id_finder("94e8376d3a90429aeaea"),
        capabilities: {
          access_codespaces: true,
          enforce_internal_access_on_token_generation: false,
        },
        properties: {},
        can_auto_install: {},
        owners: ["@github/codespaces"],
      }

      VSCODE_VSO = {
        alias: :vscode_vso,
        id: oauth_app_id_finder("3d4be8f37a0325b5817d"),
        capabilities: {
          access_codespaces: true,
          enforce_internal_access_on_token_generation: false,
        },
        properties: {},
        can_auto_install: {},
        owners: ["@github/codespaces"],
      }

      VSCODE_VSO_PPE = {
        alias: :vscode_vso_ppe,
        id: oauth_app_id_finder("eabf35024dc2e891a492"),
        capabilities: {
          access_codespaces: true,
          enforce_internal_access_on_token_generation: false,
        },
        properties: {},
        can_auto_install: {},
        owners: ["@github/codespaces"],
      }

      VSCODE_KEY = "baa8a44b5e861d918709"
      VSCODE = {
        alias: :vscode,
        id: oauth_app_id_finder(VSCODE_KEY),
        capabilities: {
          access_codespaces: true,
          enforce_internal_access_on_token_generation: false,
        },
        properties: {},
        can_auto_install: {},
        owners: ["@github/codespaces"],
      }

      VSCODE_INSIDERS = {
        alias: :vscode_insiders,
        id: oauth_app_id_finder("31f02627809389d9f111"),
        capabilities: {
          access_codespaces: true,
          enforce_internal_access_on_token_generation: false,
        },
        properties: {},
        can_auto_install: {},
        owners: ["@github/codespaces"],
      }

      VSCODE_DEV = {
        alias: :vscode_dev,
        id: oauth_app_id_finder("84383ebd8a7c5f5efc5c"),
        capabilities: {
          access_codespaces: true,
          enforce_internal_access_on_token_generation: false,
        },
        properties: {},
        can_auto_install: {},
        owners: ["@github/codespaces"],
      }

      VSCODE_AUTH_SERVER = {
        alias: :vscode_auth_server,
        id: oauth_app_id_finder("01ab8ac9400c4e429b23"),
        capabilities: {
          access_codespaces: true,
          enforce_internal_access_on_token_generation: false,
        },
        properties: {},
        can_auto_install: {},
        owners: ["@github/codespaces"],
      }

      GITHUB_APPS = [
        PRODUCTION,
        PPE,
        DEV,
        VSCODE_AUTH_PROVIDER,
      ]

      OAUTH_APPS = [
        VSCODE_OSS,
        VSCODE_EXPLORATION,
        VSCODE_VSO,
        VSCODE_VSO_PPE,
        VSCODE,
        VSCODE_INSIDERS,
        VSCODE_DEV,
        VSCODE_AUTH_SERVER,
      ]
    end
  end
end
