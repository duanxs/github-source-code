# frozen_string_literal: true

module Apps
  class Internal
    class HelpHub
      APP_NAME = "GitHub Support"
      STAGING_APP_NAME = "GitHub Support (staging)"

      def self.id_finder(name)
        ->() {
          OauthApplication.find_by(
            user: GitHub.trusted_apps_owner_id,
            name: name
          )&.id
        }
      end

      # Please ping @github/support-operations when making changes to this
      # configuration.
      PRODUCTION = {
        alias: :help_hub,
        id: id_finder(APP_NAME),
        capabilities: {
          can_auto_approve_oauth_authorization: true
        },
        properties: {},
        can_auto_install: {},
        owners: [],
      }

      # Please ping @github/support-operations when making changes to this
      # configuration.
      STAGING = {
        alias: :help_hub_staging,
        id: id_finder(STAGING_APP_NAME),
        capabilities: {
          can_auto_approve_oauth_authorization: true
        },
        properties: {},
        can_auto_install: {},
        owners: [],
      }
    end
  end
end
