# rubocop:disable Style/FrozenStringLiteralComment

class RateLimiter
  API_TIME_LIMIT = 750 # seconds of tracked response time per minute

  DEFAULT_MAX_TRIES = 10

  DEFAULT_AMOUNT = 1

  DEFAULT_TTL = 60 * 5

  attr_accessor :unique_key, :tries, :limit, :max_tries, :remaining, :expires_at

  def initialize(unique_key = nil, tries = nil, limit = nil, max_tries = nil, remaining = nil, expires_at = nil)
    @unique_key = unique_key
    @tries      = tries
    @limit      = limit
    @max_tries  = max_tries
    @remaining  = remaining
    @expires_at = expires_at
  end

  alias at_limit? limit
  alias total tries

  # Public: Sets RateLimit HTTP headers.
  #
  # headers - hash to hold headers (or request.Env)
  #
  # Returns the same RateLimiter instance.
  def set_headers(headers)
    headers["X-RateLimit-Limit"]     = @max_tries.to_s
    headers["X-RateLimit-Remaining"] = @remaining.to_s
    headers["X-RateLimit-Reset"]     = @expires_at.to_i.to_s if @expires_at

    self
  end

  # Public: Sets RateLimit log information.
  #
  # log_data - hash to hold log data
  #
  # Returns the same RateLimiter instance.
  def update_logs(log_data)
    log_data.update(
      rate_limit: @max_tries,
      rate_limit_remaining: @remaining,
      rate_limit_key: @unique_key,
      rate_limit_used: @tries,
      rate_limit_reset: @expires_at.to_i.to_s,
    )

    self
  end

  # Public: Sets RateLimit Hydro information.
  #
  # hydro_context - the hash holding the current hydro context
  #
  # Returns the same RateLimiter instance.
  def update_hydro(hydro_context)
    hydro_context.merge!({
      rate_limit: @max_tries,
      rate_limit_remaining: @remaining,
      rate_limit_key: @unique_key,
    })

    self
  end

  def platform_type_name
    "RateLimit"
  end

  # Increments the rate limiting status of the given key.
  #
  # key - String rate limit key: "api_count:technoweenie"
  # ttl - Integer specifying the time to live in seconds.
  # amount - Integer specifying how the number to increment their key by.
  #
  # Returns the new value of the given key
  def self.incr(key, ttl = nil, amount = nil, internal = true)
    ttl ||= DEFAULT_TTL
    amount ||= DEFAULT_AMOUNT

    prefixed_key = "rate_limit:#{key}"
    tries = increment(prefixed_key, amount, internal)

    if tries.nil?
      tries = amount
      expiration = (Time.now.utc + ttl).to_i

      write(prefixed_key, tries.to_s, ttl, true, internal)
      write("#{prefixed_key}:expiration", expiration.to_s, ttl, true, internal)
    end

    tries
  end

  # Checks and increments the rate limiting status of the given key.
  #
  # key                      - String rate limit key: "api_count:technoweenie"
  # max_tries                - Integer specifying the highest allowed value.
  # ttl                      - Integer specifying the time to live in seconds.
  # ban_ttl                  - Integer specifying how long to ban the given key if rate limit is exceeded.
  # check_without_increment  - Creates a RateLimiter instance without incrementing the limiter.
  # internal                 - ??
  #
  # Returns a RateLimiter instance.
  def self.check(key, max_tries: DEFAULT_MAX_TRIES, ttl: DEFAULT_TTL, amount: nil, ban_ttl: nil, check_without_increment: false, internal: false)
    tries = check_without_increment ? attempts(key) : incr(key, ttl, amount, internal)
    remain   = [(max_tries - tries), 0].max
    at_limit = tries >= max_tries
    # Check conditions to avoiding banning more than once after a limiter
    # has already been exceeded.
    should_ban = tries == max_tries && !check_without_increment && ban_ttl
    ban(key, ban_ttl, tries) if should_ban

    if (seconds = memcache.get("rate_limit:#{key}:expiration", true))
      expiration = Time.at(seconds.to_i).utc
    else
      expiration = Time.now.utc + ttl
      # If the expiration was nil, it might have been evicted, let's set it again to avoid a never ending window.
      write("rate_limit:#{key}:expiration", expiration.to_i.to_s, ttl, true, internal)
    end

    new(key.split(":").last, tries, at_limit, max_tries, remain, expiration)
  end

  # Checks and increments the rate limiting status of the given key.
  #
  # key                      - String rate limit key: "api_count:technoweenie"
  # max_tries                - Integer specifying the highest allowed value.
  # ttl                      - Integer specifying the time to live in seconds.
  # ban_ttl                  - Integer specifying how long to ban the given key if
  #                            rate limit is exceeded.  Defaults to ttl if not provided.
  # check_without_increment  - Creates a RateLimiter instance without incrementing
  #                            the limiter.
  #
  # Returns a true if the limit has been met, or false.
  def self.at_limit?(key, options = {})
    check(key, **options).at_limit?
  end

  # Removes the rate limiting data associated with the given key.
  #
  # key - String rate limit key: "api_count:technoweenie"
  #
  # Returns nothing
  def self.remove_limit(key)
    prefixed_key = "rate_limit:#{key}"
    memcache.delete(prefixed_key)
    memcache.delete("#{prefixed_key}:expiration")
  end

  # Decrements the rate limit value of a given key.
  #
  # key - String rate limit key: "api_count:technoweenie"
  # amount - Integer amount by which to decrement
  #
  # Returns the new Integer rate limit.
  def self.decr(key, amount)
    memcache.decr("rate_limit:#{key}", amount)
  end

  # Checks the rate limit value of a given key without incrementing.
  #
  # key - String rate limit key: "api_count:technoweenie"
  #
  # Returns the current Integer rate limit.
  def self.attempts(key)
    memcache.get("rate_limit:#{key}", true).to_i
  end

  def self.memcache=(c)
    @memcache = c
  end

  def self.memcache
    @memcache || GitHub.cache
  end

  class << self
    private

    # Bans the given key.  During this time, this key is
    # restricted from accessing the resource defined by the key.
    #
    # key     - String rate limit key: "api_count:technoweenie"
    # ban_ttl - Integer specifying how long to ban the given key.
    # tries   - The number of tries that have previously been made for this key.
    #
    # Returns nothing.
    def ban(key, ban_ttl, tries)
      prefixed_key = "rate_limit:#{key}"
      expiration = (Time.now.utc + ban_ttl).to_i
      # Increase the expiration so the limiter's at_limit? will remain true
      # for the duration of the ban.
      write("#{prefixed_key}:expiration", expiration.to_s, ban_ttl, true)
      write("#{prefixed_key}", tries.to_s, ban_ttl, true)
    end

    def write(key, value, ttl, raw, internal = true)
      memcache.set(key, value, ttl, raw)
      unless internal
        GitHub.regional_caches.each do |name, region_cache|
          region_cache.set(key, value, ttl, raw)
        end
      end
    end

    def increment(key, amount, internal = true)
      new_value = memcache.incr(key, amount)
      if new_value && !internal
        GitHub.regional_caches.each do |name, region_cache|
          region_cache.incr(key, amount)
        end
      end
      new_value
    end
  end
end
