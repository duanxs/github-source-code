# frozen_string_literal: true

module Codespaces
  # The users that will always hit VSO's dev or PPE environments to allow for
  # testing.
  ENV_SPECIFIC_USERS = {
    # workspaces-dev
    62036984 => :development,

    # workspaces-ppe
    62037040 => :ppe,

    # vso-dev1
    62084956 => :development,

    # vso-dev2
    62085354 => :development,

    # vso-ppe1
    62085258 => :ppe,

    # vso-ppe2
    62085530 => :ppe,

    # legomushroom-dev
    62857957 => :development,

    # bcaleb-vsodev
    62307108 => :development,

    # fara-vso-dev
    62861641 => :development,

    # osvaldortega-dev
    62716080 => :development,

    # legomushroom-ppe
    62858056 => :ppe,

    # bcaleb-vsoppe
    62307266 => :ppe,

    # fara-vso-ppe
    62861669 => :ppe,

    # osvaldortega-ppe
    62716297 => :ppe,

    # klvnraju1
    62902932 => :development,

    # klvnraju2
    62903909 => :ppe,

    # pelisy-dev
    63374219 => :development,

    # pelisy-ppe
    63374491 => :ppe,

    # burkehollandmsft
    64420201 => :ppe,

    # alexandrudima
    58736272 => :ppe,

    # qnl2vwi8
    64407832 => :ppe,

    # aeschlisimple
    42860465 => :ppe,

    # pschmied71
    1502204 => :ppe,

    # sandysomavarapu
    64399812 => :ppe,

    # rmadira
    5083649 => :ppe,

    # roblourens2
    64314422 => :ppe,

    # lyup
    7369866 => :ppe,

    # Tyriar2
    64278615 => :ppe,

    # 10xdesigner
    64278273 => :ppe,

    # rssowlreader
    15859552 => :ppe,

    # rabbit-mac
    64436892 => :ppe,

    # thermocloud
    64658626 => :ppe,

    # gicherui-dev
    65628069 => :dev,

    # gicherui-ppe
    65628372 => :ppe,
  }.freeze

  # Public: Exception class for errors occurring when talking to the VSO backend.
  class Error < StandardError; end
  class TokenError < StandardError; end

  module ClassMethods
    def vscs_platform_auth_hosts
      GitHub::Config::VSCS_ENVIRONMENTS.map { |_, config| config[:platform_auth_host] }
    end

    def vscs_targets
      GitHub::Config::VSCS_ENVIRONMENTS.keys
    end

    def vscs_target(user)
      vscs_target_config(user)[:name]
    end

    def vscs_target_config(user)
      return GitHub.codespaces_vscs_environment if GitHub.enterprise?

      if env_name = ENV_SPECIFIC_USERS[user.id]
        GitHub::Config::VSCS_ENVIRONMENTS[env_name]
      else
        GitHub.codespaces_vscs_environment
      end
    end

    def vscs_locations_url_for_target(vscs_target)
      config = GitHub::Config::VSCS_ENVIRONMENTS[vscs_target]

      "#{config[:api_url]}/api/v1/locations"
    end

    def grant_repository_access(user, codespace, session: nil)
      existing_access = user.oauth_access if session.nil?

      raise ArgumentError, "session or user.oauth_access are required" if !session && !existing_access

      integration_alias = vscs_target_config(user)[:integration_alias]
      integration = ::Apps::Internal.integration(integration_alias)

      mint_method = session ? "via user session" : "via user oauth_access"
      GitHub::Logger.log(codespaces_mint_github_token: true, integration: integration.id, method: mint_method)

      new_access = integration.grant(user, { user_session: session })

      # If the oauth access's application is different from the Codespaces
      # integration that we're using to mint this new token (as will be the case
      # for the VS Code desktop app scenario) then we need to transfer the org
      # grants from the existing access to the new access.
      if session.nil? && existing_access && existing_access.application != integration
        saml = Platform::Authorization::SAML.new(user: user)
        authorized_orgs = Organization.where(id: saml.authorized_organization_ids)
      end

      ActiveRecord::Base.connected_to(role: :writing) do
        authorized_orgs&.each do |org|
          authorization = Organization::CredentialAuthorization.grant(organization: org, credential: new_access, actor: user)
          raise TokenError, "Could not create credential authorization grant. Org: #{org.login}, user: #{user.login}" if authorization.nil?
        end

        _, error = integration.grant_repository_scoped_installation_on(new_access, repository_id: codespace.repository_id)
        raise TokenError, "Could not create repository scoped grant (#{error[:error]}): #{error[:error_description]}" if error
      end

      new_access
    end

    def mint_github_token(user, codespace, session: nil)
      access = grant_repository_access(user, codespace, session: session)
      token, _ = access.redeem
      token
    end
  end

  extend ClassMethods
end
