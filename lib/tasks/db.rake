# frozen_string_literal: true
# rubocop:disable GitHub/DoNotCallMethodsOnActiveRecordBase
require "structure_cleaner"
require "github/database_structure"
require "tasks/database_tasks"

module DBStructureUtilities
  def self.dump(db:, output:)
    path = path_for(output)
    if GitHub.rails_6_0?
      config = ActiveRecord::Base.configurations.configs_for(env_name: Rails.env, spec_name: db).config
    else
      config = ActiveRecord::Base.configurations.configs_for(env_name: Rails.env, name: db).configuration_hash
    end

    ActiveRecord::Tasks::DatabaseTasks.structure_dump(config, path)

    if db == "mysql1_primary"
      File.open(path, "a") do |f|
        f.puts ActiveRecord::Base.connection.dump_schema_information
        f.print "\n"
      end
    end
  end

  def self.cleanup(input:, output: nil, select_tables: nil, reject_tables: nil)
    output ||= input

    in_path = path_for(input)
    out_path = path_for(output)

    contents = File.read(in_path)
    contents = StructureCleaner.clean_blank_lines(contents)
    contents = StructureCleaner.clean_auto_increment(contents)
    contents = StructureCleaner.clean_conditional_statements(contents)
    contents = StructureCleaner.remove_table_definitions(contents, ["_+[^`]*"])

    if select_tables
      contents = StructureCleaner.extract_table_definitions(contents, select_tables)
    end

    if reject_tables
      contents = StructureCleaner.remove_table_definitions(contents, reject_tables)
    end

    contents = StructureCleaner.clean_partitions_comment(contents)

    File.write(out_path, contents)
  end

  def self.path_for(file_name)
    File.join(Rails.root, "db", file_name)
  end
end

### GitHub db: customizations
namespace :db do
  # Clear check_protected_environments because it loads the env first and
  # the dbs might not exist yet. This is safe because the prod databases
  # are in their own database.yml.
  Rake::Task["db:check_protected_environments"].clear

  # Alias create/drop to *_all version, so we can work with mutiple
  # dbs on database.yml.
  Rake::Task["db:create"].clear
  task :create => ["db:create:all"]
  Rake::Task["db:drop"].clear
  task :drop => ["db:drop:all"]

  Rake::Task["db:migrate"].clear
  task :migrate => :load_config do
    ActiveRecord::Tasks::DatabaseTasks.migrate
    Rake::Task["db:structure:dump"].invoke
  end

  namespace :migrate do
    task down: :load_config do
      raise "VERSION is required - To go down one migration, use db:rollback" if !ENV["VERSION"] || ENV["VERSION"].empty?

      ActiveRecord::Tasks::DatabaseTasks.check_target_version
      migration_context = ActiveRecord::Base.connection.migration_context
      migration_context.run(:down, ActiveRecord::Tasks::DatabaseTasks.target_version)

      Rake::Task["db:structure:dump"].invoke
    end

    task up: :load_config do
      raise "VERSION is required" if !ENV["VERSION"] || ENV["VERSION"].empty?

      ActiveRecord::Tasks::DatabaseTasks.check_target_version
      migration_context = ActiveRecord::Base.connection.migration_context
      migration_context.run(:up, ActiveRecord::Tasks::DatabaseTasks.target_version)

      Rake::Task["db:structure:dump"].invoke
    end
  end

  Rake::Task["db:structure:dump"].clear
  namespace :structure do
    include GitHub::DatabaseStructure

    # Extension of db:structure:dump to normalize the output and reduce
    # conflicts for output generated on different machines.
    task :dump => :load_config do
      if !GitHub.enterprise?
        # github.com has tables spread across different schemas, here we need to dump
        # all of them in separate files.
        #
        # Once all are dumped, we clean them.
        DBStructureUtilities.dump(db: "abilities_primary", output: "abilities-structure.sql")
        DBStructureUtilities.dump(db: "mysql1_primary", output: "structure.sql")
        DBStructureUtilities.dump(db: "notifications_primary", output: "mysql2-structure.sql")
        DBStructureUtilities.dump(db: "notifications_deliveries_primary", output: "notifications-deliveries-structure.sql")
        DBStructureUtilities.dump(db: "notifications_entries_primary", output: "notifications-entries-structure.sql")
        DBStructureUtilities.dump(db: "kv_primary", output: "mysql5-structure.sql")
        DBStructureUtilities.dump(db: "repositories_primary", output: "repositories-structure.sql")
        DBStructureUtilities.dump(db: "spokes_write", output: "spokes-structure.sql")
        DBStructureUtilities.dump(db: "stratocaster_primary", output: "stratocaster-structure.sql")
        DBStructureUtilities.dump(db: "collab_primary", output: "collab-structure.sql")
        DBStructureUtilities.dump(db: "ballast_primary", output: "ballast-structure.sql")
        DBStructureUtilities.dump(db: "memex_primary", output: "memex-structure.sql")
        DBStructureUtilities.dump(db: "gitbackups_primary", output: "gitbackups-structure.sql")
        DBStructureUtilities.dump(db: "notify_primary", output: "notify-structure.sql")
        DBStructureUtilities.dump(db: "iam_primary", output: "iam-structure.sql")

        DBStructureUtilities.cleanup(input: "abilities-structure.sql")
        DBStructureUtilities.cleanup(input: "mysql2-structure.sql")
        DBStructureUtilities.cleanup(input: "notifications-deliveries-structure.sql")
        DBStructureUtilities.cleanup(input: "notifications-entries-structure.sql")
        DBStructureUtilities.cleanup(input: "mysql5-structure.sql")
        DBStructureUtilities.cleanup(input: "collab-structure.sql")
        DBStructureUtilities.cleanup(input: "memex-structure.sql")
        DBStructureUtilities.cleanup(input: "repositories-structure.sql")
        DBStructureUtilities.cleanup(input: "spokes-structure.sql")
        DBStructureUtilities.cleanup(input: "stratocaster-structure.sql")
        DBStructureUtilities.cleanup(input: "ballast-structure.sql")
        DBStructureUtilities.cleanup(input: "gitbackups-structure.sql")
        DBStructureUtilities.cleanup(input: "notify-structure.sql")
        DBStructureUtilities.cleanup(input: "iam-structure.sql")

        if Rails.env.production?
          DBStructureUtilities.cleanup(input: "structure.sql")
        else
          DBStructureUtilities.cleanup(input: "structure.sql",        output: "structure.sql",         reject_tables: OUTSIDE_MAIN_CLUSTER_TABLES - TABLES_BEING_MIGRATED)
        end
      elsif GitHub.enterprise?
        DBStructureUtilities.dump(db: "mysql1_primary", output: "structure.sql")
        DBStructureUtilities.cleanup(input: "structure.sql", output: "abilities-structure.sql",  select_tables: IAM_ABILITIES_TABLES)
        DBStructureUtilities.cleanup(input: "structure.sql", output: "mysql2-structure.sql",  select_tables: MYSQL2_TABLES)
        DBStructureUtilities.cleanup(input: "structure.sql", output: "notifications-deliveries-structure.sql",  select_tables: NOTIFICATIONS_DELIVERIES_TABLES)
        DBStructureUtilities.cleanup(input: "structure.sql", output: "notifications-entries-structure.sql",  select_tables: NOTIFICATIONS_ENTRIES_TABLES)
        DBStructureUtilities.cleanup(input: "structure.sql", output: "mysql5-structure.sql",  select_tables: MYSQL5_TABLES)
        DBStructureUtilities.cleanup(input: "structure.sql", output: "repositories-structure.sql",  select_tables: REPOSITORIES_TABLES)
        DBStructureUtilities.cleanup(input: "structure.sql", output: "spokes-structure.sql",  select_tables: SPOKES_TABLES)
        DBStructureUtilities.cleanup(input: "structure.sql", output: "stratocaster-structure.sql",  select_tables: STRATOCASTER_TABLES)
        DBStructureUtilities.cleanup(input: "structure.sql", output: "ballast-structure.sql", select_tables: BALLAST_TABLES)
        DBStructureUtilities.cleanup(input: "structure.sql", output: "collab-structure.sql",  select_tables: COLLAB_TABLES)
        DBStructureUtilities.cleanup(input: "structure.sql", output: "memex-structure.sql",  select_tables: MEMEX_TABLES)
        DBStructureUtilities.cleanup(input: "structure.sql", output: "notify-structure.sql",  select_tables: NOTIFY_TABLES)
        DBStructureUtilities.cleanup(input: "structure.sql", output: "iam-structure.sql",  select_tables: IAM_TABLES)
        DBStructureUtilities.cleanup(input: "structure.sql", output: "structure.sql",         reject_tables: OUTSIDE_MAIN_CLUSTER_TABLES - TABLES_BEING_MIGRATED)
      else
        raise RuntimeError.new("Cannot dump schemas in unkown environment")
      end
    end

    # replace db:abort_if_pending_migrations
    Rake::Task["db:abort_if_pending_migrations"].clear
    task :abort_if_pending_migrations => :load_config do
      ActiveRecord::Base.establish_connection
      pending_migrations = ActiveRecord::Base.connection.migration_context.open.pending_migrations

      if pending_migrations.any?
        puts "You have #{pending_migrations.size} pending #{pending_migrations.size > 1 ? 'migrations:' : 'migration:'}"
        pending_migrations.each do |pending_migration|
          puts "  %4d %s" % [pending_migration.version, pending_migration.name]
        end
        abort %{Run `rails db:migrate` to update your database then try again.}
      end
    end

    # replace db:structure:load to load all structure files
    Rake::Task["db:structure:load"].clear
    task :load => :load_config do
      ActiveRecord::Base.configurations.configs_for(env_name: "development").each do |db_config|
        if GitHub.rails_6_0?
          config = db_config.config
          structure_from_config = config["structure"]
        else
          config = db_config
          structure_from_config = db_config.configuration_hash[:structure]
        end

        Array(structure_from_config).each do |structure|
          structure_path = Rails.root.join("db", structure)
          ActiveRecord::Tasks::DatabaseTasks.structure_load(config, structure_path)
        end
      end
      ActiveRecord::Base.establish_connection(:mysql1_primary)
    end

    namespace :initial do
      task :dump => "db:structure:dump" do
        dest_root = Rails.root.join("db", "initial_structure")
        FileUtils.mkdir_p(dest_root)
        ActiveRecord::Base.configurations.configs_for(env_name: "development").each do |db_config|
          if GitHub.rails_6_0?
            structure_from_config = db_config.config["structure"]
          else
            structure_from_config = db_config.configuration_hash[:structure]
          end

          Array(structure_from_config).each do |structure|
            structure_path = Rails.root.join("db", structure)
            dest_path = dest_root.join(structure)
            FileUtils.cp(structure_path, dest_path)
          end
        end
      end

      task :load => :load_config do
        dest_root = Rails.root.join("db", "initial_structure")
        ActiveRecord::Base.configurations.configs_for(env_name: "development").each do |db_config|
          if GitHub.rails_6_0?
            structure_from_config = db_config.config["structure"]
          else
            config = db_config.configuration_hash
            structure_from_config = db_config.configuration_hash[:structure]
          end

          Array(structure_from_config).each do |structure|
            structure_path = Rails.root.join("db", structure)
            dest_path = dest_root.join(structure)
            FileUtils.cp(dest_path, structure_path)
          end
        end
        Rake::Task["db:structure:load"].invoke
      end
    end
  end

  namespace :test do
    # replace db:test:load_structure to load all structure files
    Rake::Task["db:test:load_structure"].clear
    task :load_structure => "db:load_config" do
      ActiveRecord::Base.configurations.configs_for(env_name: "test").each do |db_config|
        if GitHub.rails_6_0?
          config = db_config.config
          structure_from_config = config["structure"]
        else
          config = db_config
          structure_from_config = config.configuration_hash[:structure]
        end

        structure_paths = Array(structure_from_config).map do |structure|
          Rails.root.join("db", structure)
        end

        structure_paths.each do |structure_path|
          if ENV["GITHUB_CI"]
            ActiveRecord::Tasks::MySQLDatabaseTasks.new(config).structure_load_spawn(structure_path)
          else
            ActiveRecord::Tasks::DatabaseTasks.structure_load(config, structure_path)
          end
        end

        unless structure_paths.empty? || ENV["GITHUB_CI"]
          schema_sha1 = Digest::SHA1.hexdigest(structure_paths.map { |x| File.read(x) }.join)
          ActiveRecord::Base.establish_connection(config)
          ActiveRecord::InternalMetadata.create_table
          ActiveRecord::InternalMetadata[:schema_sha1] = schema_sha1
          ActiveRecord::Base.clear_all_connections!
        end
      end
    end

    task :load_initial_structure => "db:load_config" do
      db_configs = ActiveRecord::Base.configurations.configs_for(env_name: "test")

      dest_root = Rails.root.join("db", "initial_structure")

      db_configs.each do |db_config|
        if GitHub.rails_6_0?
          structure_from_config = db_config.config["structure"]
        else
          structure_from_config = db_config.configuration_hash[:structure]
        end

        Array(structure_from_config).each do |structure|
          structure_path = Rails.root.join("db", structure)
          dest_path = dest_root.join(structure)
          FileUtils.cp(dest_path, structure_path)
        end
      end

      Rake::Task["db:test:load_structure"].invoke
    end

    namespace :ci do
      task :reset => "db:load_config" do
        tasks = ActiveRecord::Tasks::DatabaseTasks
        concurrency = Integer(ENV["TEST_QUEUE_WORKERS"] || raise("Please provide TEST_QUEUE_WORKERS ENV"))

        1.upto(concurrency) do |i|
          ActiveRecord::Base.configurations.configs_for(env_name: "test").each do |db_config|
            if GitHub.rails_6_0?
              new_config = db_config.config.merge("database" => "#{db_config.config["database"]}#{i}")
              structure_from_config = new_config["structure"]
            else
              new_hash = db_config.configuration_hash.merge(database: "#{db_config.configuration_hash[:database]}#{i}")
              structure_from_config = new_hash[:structure]
              new_config = ActiveRecord::DatabaseConfigurations::HashConfig.new(db_config.env_name, db_config.name, new_hash)
            end

            tasks.drop(new_config)
            tasks.create(new_config)

            Array(structure_from_config).each do |structure|
              structure_path = Rails.root.join("db", structure)
              ActiveRecord::Tasks::MySQLDatabaseTasks.new(new_config).structure_load_spawn(structure_path)
            end
          end
        end
      end
    end
  end
end
