# rubocop:disable Style/FrozenStringLiteralComment

require "posix/spawn"

module ActiveRecord
  module Tasks
    module DatabaseTasks
      # Override drop method, so we ignore 'Couldn't drop..' errors
      # Rails 6.0 and 6.1 have different method signatures. It's much cleaner
      # to define two version-specific methods than try to wrangle arguments
      if GitHub.rails_6_0?
        def drop(*arguments)
          configuration = arguments.first
          config_adapter = configuration["adapter"]
          class_for_adapter(config_adapter).new(*arguments).drop
        rescue Exception => error # rubocop:disable Lint/RescueException
          unless error.message.include? "Unknown database"
            $stderr.puts error, *(error.backtrace)
            $stderr.puts "Couldn't drop #{configuration['database']}"
          end
        end
      else
        def drop(configuration, *arguments)
          db_config = resolve_configuration(configuration)
          database_adapter_for(db_config, *arguments).drop
        rescue Exception => error # rubocop:disable Lint/RescueException
          unless error.message.include? "Unknown database"
            $stderr.puts error, *(error.backtrace)
            $stderr.puts "Couldn't drop #{configuration['database']}"
          end
        end
      end
    end

    class MySQLDatabaseTasks
      def structure_load_spawn(filename)
        args = prepare_command_options
        args.concat(["--execute", %{SET FOREIGN_KEY_CHECKS = 0; SOURCE #{filename}; SET FOREIGN_KEY_CHECKS = 1}])
        if GitHub.rails_6_0?
          database_name = "#{configuration['database']}"
        else
          database_name = "#{configuration_hash[:database]}"
        end
        args.concat(["--database", database_name])
        process = POSIX::Spawn::Child.new("mysql", *args)
        unless process.status.success?
          raise "Importing MySQL structure #{filename} failed: #{process.err}"
        end
      end
    end
  end
end
