# frozen_string_literal: true

namespace :enterprise do
  namespace :packages do
    task :init_packages_ci, [:login] => [:environment] do |t, args|
        token, hashed_token = OauthAccess.random_token_pair
        @access = User.find_by_login(args[:login]).oauth_accesses.build do |access|
            access.application_id = OauthApplication::PERSONAL_TOKENS_APPLICATION_ID
            access.application_type = OauthApplication::PERSONAL_TOKENS_APPLICATION_TYPE
            access.description = "enterprise integration token"
            access.scopes = ["repo", "read:packages", "write:packages", "delete:packages"]
            access.token_last_eight = token.last(8)
            access.hashed_token = hashed_token
        end
        @access.save!
        puts token
    end
  end
end
