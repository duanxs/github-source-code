# frozen_string_literal: true
# resque-related worker code lives in config/resqued/github-environment.rb
namespace :resque do
  task :reset => :environment do
    abort "resque:reset is for development only!" unless Rails.env.development?
    Resque.queues.each do |queue|
      puts "removing #{Resque.size(queue)} items from #{queue} queue"
      while Resque.pop(queue)
      end
    end
  end
end
