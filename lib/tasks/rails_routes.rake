# frozen_string_literal: true

namespace :routes do
  desc "Get the ownership info for the rails routes in CSV format"
  task :ownership => [:environment] do

    csv = CSV.new($stdout)
    csv << %w(service_mapping endpoint codeowners AoR prefix verb path)

    Rails.application.routes.routes.each do |route|
      wrapped = ActionDispatch::Routing::RouteWrapper.new(route)

      # Some routes just redirect elsewhere. Redirects do not have owners.
      next if wrapped.endpoint.start_with?("redirect(")

      # In development, rails adds its own controllers. We can skip these.
      next if wrapped.controller.start_with?("rails/")

      aor = nil
      codeowners = nil
      service_mapping = nil

      begin
        controller_class = "#{wrapped.controller.camelize}Controller".constantize
        aor = controller_class.areas_of_responsibility || []
        aor = aor.join("|")

        codeowners = begin
          controller_class.codeowners
        rescue UnconfiguredAoRError
          []
        end
        codeowners = codeowners.to_a.map { |owner| owner.gsub(/\A@github\//, "") }.join("|")

        service_mapping = controller_class.service_mapping(wrapped.action)
      rescue NameError
        "no controller!"
      end

      csv << [service_mapping, wrapped.endpoint, codeowners, aor, wrapped.name, wrapped.verb, wrapped.path]
    end
  end
end
