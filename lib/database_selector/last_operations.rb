# frozen_string_literal: true
class DatabaseSelector
  class LastOperations
    def self.from_token(token)
      LastOperationsFromToken.new(token)
    end

    def self.from_session(session)
      LastOperationsFromSession.new(session)
    end

    def self.from_creds(login)
      LastOperationsFromBasicAuthCreds.new(login)
    end

    # Fetch LastOperations from an `Api::RequestCredentials` object.
    def self.from_request_creds(creds)
      if creds.token
        last_operations = from_token(creds.token)
      else
        last_operations = from_creds(creds.login)
      end
    end

    def update(key, value, ttl)
      GitHub.cache.set(key, value, ttl)
      GitHub.regional_caches.each do |_, region_cache|
        region_cache.set(key, value, ttl)
      end
    end

    def last_write_timestamp
      return @last_write_timestamp if defined?(@last_write_timestamp)
      load_from_store
      @last_write_timestamp = Timestamp.to_time(last_gtids.values.map { |g| g[:time] }.max.to_i)
    end

    def last_gtids
      return @last_gtids if defined?(@last_gtids)
      load_from_store
      @last_gtids
    end

    protected

    def current_gtids
      seen_connections = {}

      ApplicationRecord.gtid_tracking_clusters.each_with_object({}) do |cluster, gtids|
        connection = cluster.connection

        unless seen_connections[connection]
          gtids[cluster.cluster_name] = connection.last_gtid if connection.last_gtid
          seen_connections[connection] = true
        end
      end
    end

    def new_gtids
      new_gtids = {}
      now = Timestamp.from_time(Time.now)
      # Drop anything that's older than 30 seconds
      # since we don't care about it anymore.
      last_gtids.each do |cluster, gtid|
        if now - gtid[:time] < 30_000
          new_gtids[cluster] = gtid
        end
      end
      current_gtids.each do |cluster, gtid|
        new_gtids[cluster] = { gtid: gtid, time: now }
      end
      new_gtids
    end
  end

  class LastOperationsFromToken < LastOperations
    attr_reader :token

    def initialize(token)
      @token = token
    end

    def update_last_write_timestamp
      if token
        gtids = new_gtids
        update(memcache_key(token), gtids.to_json, 30.seconds)

        @last_gtids = gtids
      else
        @last_gtids = {}
      end
    end

    def load_from_store
      if token && val = GitHub.cache.get(memcache_key(token))
        @last_gtids = GitHub::JSON.parse(val).deep_symbolize_keys
      else
        @last_gtids = {}
      end
    end

    def token_hash(token)
      OpenSSL::Digest::SHA1.hexdigest(token)
    end

    def memcache_key(token)
      "api:replica-read:token-auth:v3:#{token_hash(token)}"
    end
  end

  class LastOperationsFromBasicAuthCreds < LastOperations
    attr_reader :login

    def initialize(login)
      @login = login
    end

    def credentials_hash(login)
      OpenSSL::Digest::SHA1.hexdigest(login)
    end

    def load_from_store
      if login && val = GitHub.cache.get(memcache_key(login))
        @last_gtids = GitHub::JSON.parse(val).deep_symbolize_keys
      else
        @last_gtids = {}
      end
    end

    def update_last_write_timestamp
      if login
        gtids = new_gtids
        update(memcache_key(login), gtids.to_json, 30.seconds)
        @last_gtids = gtids
      else
        @last_gtids = {}
      end
    end

    def memcache_key(login)
      "api:replica-read:basic-auth:v3:#{credentials_hash(login)}"
    end
  end

  class LastOperationsFromSession < LastOperations
    attr_reader :session

    def initialize(session)
      @session = session
    end

    def load_from_store
      if session[:last_gtids]
        @last_gtids = session[:last_gtids].deep_symbolize_keys
      else
        @last_gtids = {}
      end
    end

    def update_last_write_timestamp
      session[:last_gtids] = new_gtids
    end
  end
end
