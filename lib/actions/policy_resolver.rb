# frozen_string_literal: true

class Actions::PolicyResolver
  Error = Class.new(StandardError)

  ALL_ENTITIES      = "all_entities".freeze
  SELECTED_ENTITIES = "selected_entities".freeze
  VIRTUAL_OPTIONS = [ALL_ENTITIES, SELECTED_ENTITIES].freeze

  VALID_OPTIONS = Configurable::ActionExecutionCapabilities::ALL_OPTIONS +
    VIRTUAL_OPTIONS

  ALL_ACTIONS = Configurable::ActionExecutionCapabilities::ALL_ACTIONS
  ALL_ACTIONS_FOR_SELECTED = Configurable::ActionExecutionCapabilities::ALL_ACTIONS_FOR_SELECTED
  LOCAL_ACTIONS = Configurable::ActionExecutionCapabilities::LOCAL_ACTIONS
  LOCAL_FOR_SELECTED = Configurable::ActionExecutionCapabilities::LOCAL_ONLY_FOR_SELECTED

  SELECTED_TO_ALL_MAP = {
    ALL_ACTIONS_FOR_SELECTED => ALL_ACTIONS,
    LOCAL_FOR_SELECTED => LOCAL_ACTIONS,
  }

  ALL_TO_SELECTED_MAP = {
    ALL_ACTIONS => ALL_ACTIONS_FOR_SELECTED,
    LOCAL_ACTIONS => LOCAL_FOR_SELECTED,
  }

  ALL_ACTIONS_MAP = {
    SELECTED_ENTITIES => ALL_ACTIONS_FOR_SELECTED,
    ALL_ENTITIES => ALL_ACTIONS,
  }

  LOCAL_ACTIONS_MAP = {
    SELECTED_ENTITIES => LOCAL_FOR_SELECTED,
    ALL_ENTITIES => LOCAL_ACTIONS,
  }

  def self.perform(**options)
    new(**options).perform
  end

  def initialize(entity:, policy:)
    @entity = entity
    @policy = policy
  end

  def perform
    case @policy
    when SELECTED_ENTITIES
      find_equivalent_policy(map: ALL_TO_SELECTED_MAP, default: ALL_ACTIONS_FOR_SELECTED)
    when ALL_ENTITIES
      find_equivalent_policy(map: SELECTED_TO_ALL_MAP, default: ALL_ACTIONS)
    else
      @policy
    end
  end

  private

  # Preserve original policy if we're not changing the number entities we're
  # affecting.
  #
  # Examples:
  # - If the current policy is "local actions for selected entities",
  # selecting "selected entities" would be a no-op.
  # - If the current policy is "local actions for all entities", selecting
  # "selecting "all entities"/"Enable for all [entities]" is also a no-op.
  #
  # Otherwise, if we're going from "all entities" to "selected entities", we
  # should make sure that we set the equivalent local vs. all actions
  # option.
  #
  # In the case that we can't find an quivalent option, it's likely because
  # the current policy is "disabled". This means that we'll have to try applying
  # the chosen "all vs. selected entities" option to whatever Actions type
  # policy is found on the entity owner (all Actions vs. local only).
  def find_equivalent_policy(map:, default:)
    if current_policy.in?(map.values)     # the no-op case
      current_policy
    else
      map[current_policy] || map_by_owner_action_type || default
    end
  end

  def map_by_owner_action_type
    return unless owner_policy_exists?

    if entity_owner.all_action_executions_enabled?
      ALL_ACTIONS_MAP[@policy]
    else
      LOCAL_ACTIONS_MAP[@policy]
    end
  end

  def owner_policy_exists?
    entity_owner.respond_to?(:action_execution_capabilities)
  end

  def entity_owner
    @_owner ||= @entity.configuration_owner
  end

  def current_policy
    @_current_policy ||= @entity.action_execution_capabilities
  end
end
