# frozen_string_literal: true

class Actions::PolicyChecker
  ALL_ACTIONS = Configurable::ActionExecutionCapabilities::ALL_ACTIONS
  ALL_ACTIONS_FOR_SELECTED = Configurable::ActionExecutionCapabilities::ALL_ACTIONS_FOR_SELECTED
  LOCAL_ACTIONS = Configurable::ActionExecutionCapabilities::LOCAL_ACTIONS
  LOCAL_ONLY_FOR_SELECTED = Configurable::ActionExecutionCapabilities::LOCAL_ONLY_FOR_SELECTED
  DISABLED = Configurable::ActionExecutionCapabilities::DISABLED

  OPTIONS_FOR_ALL_ENTITIES = Configurable::ActionExecutionCapabilities::OPTIONS_FOR_ALL_ENTITIES
  OPTIONS_FOR_SELECTED_ENTITIES = Configurable::ActionExecutionCapabilities::OPTIONS_FOR_SELECTED_ENTITIES

  def self.enabled_for_all_entities?(policy)
    policy.in? OPTIONS_FOR_ALL_ENTITIES
  end

  def self.enabled_for_selected_entities_only?(policy)
    policy.in? OPTIONS_FOR_SELECTED_ENTITIES
  end

  def self.all_action_types_allowed?(policy)
    policy.in? [ALL_ACTIONS, ALL_ACTIONS_FOR_SELECTED]
  end

  def self.local_actions_only?(policy)
    policy.in? [LOCAL_ACTIONS, LOCAL_ONLY_FOR_SELECTED]
  end

  def self.disabled?(policy)
    policy == DISABLED
  end
end
