# rubocop:disable Style/FrozenStringLiteralComment

# Adds easy timing of methods in Graphite. Usage
#
# class MyClass
#   include MethodTiming
#
#   def expensive_method
#     do_work
#   end
#   time_method :expensive_method, key: "my_class.expensive_method.duration"
# end
module MethodTiming
  def self.included(base_class)
    # Add a module to hold the wrappers for all timed methods in this class.
    base_class.class_eval <<-RUBY
      module TimedMethods
      end

      prepend TimedMethods
    RUBY

    base_class.extend ClassMethods
  end

  module ClassMethods
    # Encapsulates the common pattern of:
    #
    # def some_method
    #   GitHub.dogstats.time(key) do
    #     # all method logic goes here
    #   end
    # end
    #
    # target - String or Symbol name of the method to be instrumented
    # key    - String Graphite key for storing the timing data
    #
    # Returns nothing.
    def time_method(target, key:)
      visibility = case
      when public_method_defined?(target)
        "public"
      when protected_method_defined?(target)
        "protected"
      when private_method_defined?(target)
        "private"
      end
      if GitHub::ruby_2_6?
        self::TimedMethods.class_eval <<-RUBY
        def #{target}(*args, &block)
          GitHub.dogstats.time(#{key.to_s.inspect}) do
            super
          end
        end
        #{visibility} :#{target}
        RUBY
      else
        self::TimedMethods.class_eval <<-RUBY
        def #{target}(*args, **kwargs, &block)
          GitHub.dogstats.time(#{key.to_s.inspect}) do
            super
          end
        end
        #{visibility} :#{target}
        RUBY
      end
    end
  end
end
