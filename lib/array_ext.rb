# frozen_string_literal: true

class Array
  def to_md5
    to_s.to_md5
  end
end
