# frozen_string_literal: true

module Checks
  # A class which consolidates CheckSuite and CheckRun operations into a reusable service.
  class Service

    def self.areas_of_responsibility
      [:checks]
    end

    # Finds or creates a check suite given a head sha, github app id, and a repository.
    def self.find_or_create_check_suite(github_app_id:, head_sha:, repo:)
      attrs = {
        head_sha: head_sha,
        github_app_id: github_app_id,
      }

      check_suite = repo.check_suites.find_by(attrs)
      return check_suite if check_suite.present?

      # When creating check suites elsewhere in the app, if the push is known we use it to set the
      # head_branch attribute. While we don't accept a head_branch param from integrators in the
      # api, it seems reasonable to use the information we know to better fill in the details.
      push = Push.find_by(after: head_sha, repository_id: repo.id)

      attrs.merge!(
        head_branch: push&.branch_name,
        push_id: push&.id,
      )

      # Attempts to find the check suite, otherwise tries to create the check suite.
      # If creation fails because the record is not unique, retry up to 3 times.
      retries = 3
      begin
        repo.check_suites.find_by(attrs) || repo.check_suites.create(attrs)
      rescue ActiveRecord::RecordNotUnique
        (retries -= 1) && retry if retries > 0
        raise # Re-raises the same error
      end
    end
  end
end
