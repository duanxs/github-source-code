# frozen_string_literal: true

module Authorization
  module Queries
    class BaseQuery < Authorization::Operation
    end
  end
end
