# frozen_string_literal: true
require_relative "base_query.rb"

module Authorization
  module Queries
    class ActorIds < BaseQuery

      attr_reader :actor_type, :subject, :actor_ids, :action, :through

      def initialize(actor_type:, subject:, actor_ids: :any, action: :any, through: [])
        @actor_type = actor_type
        @subject = subject
        @actor_ids = actor_ids
        @action = action
        @through = through
      end

      def execute_implementation
        Ability::Graph.query do |g|
          g.get  :actor_id
          g.from actor_type, actor_ids
          g.to subject.ability_type, subject.ability_id
          if action != :any
            g.action action
          end
          through.each do |connector|
            g.through connector
          end
          g.uniq
        end.values
      end

      def default_result
        []
      end

      def validation_errors
        errors = []

        if !actor_type
          errors << validation_error(:missing_actor_type)
        end

        unless valid_subject?(subject)
          errors << validation_error(:invalid_subject, subject: subject)
        end

        if action != :any && !Array.wrap(action).all? { |a| valid_action?(a) }
          errors << validation_error(:invalid_action, action: action)
        end

        errors
      end
    end
  end
end
