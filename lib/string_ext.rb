# frozen_string_literal: true

require "fast_blank"

class String
  def to_md5
    Digest::MD5.hexdigest(self)
  end

  alias blank? blank_as?
end
