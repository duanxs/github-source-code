# frozen_string_literal: true

module OpenApi
  class Validator
    def initialize(schema, request)
      @request = request
      @requested_operation = Router.new(schema, request).request_operation
    end

    def validate_request
      if !@requested_operation.exists?
        return errors(@requested_operation.errors, "not_found")
      end

      operation_description = @requested_operation.description
      validation_errors = operation_description.validate_request(@request, @requested_operation.parameters)
      errors(validation_errors, "request")
    end

    def validate_response(status, headers, body)
      return [] if !@requested_operation.exists?

      operation_description = @requested_operation.description
      validation_errors = operation_description.validate_response(status, headers, body)
      errors(validation_errors, "response")
    end

    private

    # Wrap operation errors in an error wrapper
    # that contains common request context fields
    def errors(validation_errors, category)
      validation_errors.map do |err|
        {
          path_info: @request.path_info,
          operation_id: @requested_operation.description ? @requested_operation.description.id : nil,
          category: category,
          operation_backfill_source: @requested_operation.description ? @requested_operation.description.backfill_source : nil,
        }.merge(err)
      end
    end
  end
end
