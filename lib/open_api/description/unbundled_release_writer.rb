# frozen_string_literal: true

module OpenApi
  module Description
    class UnbundledReleaseWriter < ReleaseWriter
      def path(format: :json)
        OpenApi.root.join(@release.filename)
      end

      delegate :content, to: :@release

      private

      def serialize(content, format:)
        YAML.dump(content)
      end
    end
  end
end
