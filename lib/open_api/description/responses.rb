# frozen_string_literal: true

module OpenApi
  module Description
    class Responses
      MISSING_RESPONSE = "MISSING_RESPONSE"

      def initialize(responses, operation_id)
        @operation_id = operation_id
        @responses = responses
      end

      def validate(status, headers, body)
        # TODO is there anytime where we might have a streamed body?
        body = body.first

        if !@responses.key?(status.to_s)
          return [{
            code: MISSING_RESPONSE,
            response: status.to_s,
            message: "No response with status #{status} was defined for operation #{@operation_id}",
            path: "/responses"
          }]
        end

        # TODO: validate headers

        response = @responses[status.to_s]
        content = response["content"]

        if content.nil?
          maybe_json_body = begin
            JSON.parse(body)
          rescue JSON::ParserError, TypeError
            nil
          end

          if body.nil? || body.empty?
            return []
          else
            return [{ message: "Response with status #{status} was on operation #{@operation_id} had no content. Verify if this was intended." }]
          end
        end

        if !headers["Content-Type"].blank? && !content.key?(headers["Content-Type"].split(";").first)
          return [{ message: "Response media type not handled by operation, only found these supported media types: #{content.keys}" }]
        end

        # TODO: Handle multiple media types
        media_type     = content.keys.first
        api_media_type = Api::MediaType.new(media_type)

        # application/json or any +json suffixes
        if !(media_type == "application/json" || api_media_type.json?)
          return [{
            message: "Only application/json response content are currently supported",
          }]
        else
          schema = OpenApi::Description::Schema.new(content[media_type]["schema"])
          json = GitHub::JSON.parse(body)

          json_pointer = "/responses/#{status}/content/#{json_pointer_escape(media_type)}/schema"

          schema.validate(json).to_a.map do |error|
            error.merge(path: [json_pointer, error[:path]].join("/"))
          end
        end
      end

      private

      def json_pointer_escape(segment)
        segment.gsub("/", "^/")
      end
    end
  end
end
