# frozen_string_literal: true

require "hana"

module OpenApi
  module Description
    class Release
      class Error < ::RuntimeError; end

      class OperationRef
        attr_reader :http_path, :http_method, :ref

        def initialize(http_path, http_method, ref)
          @http_path = http_path
          @http_method = http_method
          @ref = ref
        end
      end

      def self.writers
        @writers ||= {
          unbundled: OpenApi::Description::UnbundledReleaseWriter,
          bundled: OpenApi::Description::BundledReleaseWriter,
          dereferenced: OpenApi::Description::DereferencedReleaseWriter
        }
      end

      def self.parse(slug)
        name, version = slug.split("-", 2)
        new(name, version)
      end

      attr_reader :name, :version
      def initialize(name, version = nil)
        @name = name
        @version = version ? Gem::Version.new(version) : nil
      end

      def operations
        @operations ||= find_operations
      end

      def slug
        @slug ||= [@name, @version].compact.join("-")
      end

      def filename
        @filename ||= slug + ".yaml"
      end

      def meets_release_constraint?(release_constraint)
        case release_constraint
        when String
          release_constraint == @name
        when Hash
          if release_constraint.size == 1
            if @version
              constraint_name, raw_requirement = release_constraint.flatten
              requirement = Gem::Requirement.create(raw_requirement)
              constraint_name == @name && requirement.satisfied_by?(@version)
            end
          else
            raise Error, "Invalid release constraint: #{release_constraint.inspect}"
          end
        end
      end

      def content
        @content ||= build_content
      end

      def write(format:, serialize_as: :json)
        if (writer = self.class.writers[format])
          writer.write(self, format: serialize_as)
        else
          raise Error, "Unknown format `#{format.inspect}` - must be one of #{self.class.writers.keys.inspect}"
        end
      end

      # Returns a Hash of the fully dereferenced description
      def to_h
        writer = self.class.writers.fetch(:dereferenced)
        writer.content(self)
      end

      def variables
        @variables ||= config["variables"] || {}
      end

      private

      def build_content
        # Load the release boilerplate
        base = YAML.load_file(OpenApi.root.join("config/release.yaml"))
        # Apply any release-specific JSON patch files
        description = patch.apply(base)
        # Link operations
        description["paths"] = build_paths
        # Return description
        description
      end

      def build_paths
        operations.group_by(&:http_path).reduce({}) do |memo, (http_path, path_operations)|
          method_refs = path_operations.group_by(&:http_method).reduce({}) do |path_memo, (http_method, references)|
            if references.size > 1
              filenames = references.map(&:ref)
              raise Error, "Multiple operations for release #{slug} defined as [#{http_method} #{http_path}]: #{filenames}"
            end
            path_memo[http_method] = {"$ref" => references.first.ref.to_s}
            path_memo
          end
          memo[http_path] = method_refs
          memo
        end
      end

      def patch
        @patch ||= Hana::Patch.new(config["patch"] || [])
      end

      def config
        @config ||= begin
          patch_file = OpenApi.root.join("config/releases", filename)
          unless patch_file.exist?
            raise ArgumentError, "No release patch file found at #{patch_file}"
          end
          YAML.load_file(patch_file)
        end
      end

      def find_operations
        operation_filenames.each.with_object([]) do |operation_path, memo|
          operation_data = YAML.load_file(operation_path)
          release_constraints = operation_data.fetch("x-github-releases", [])
          if release_constraints.any?(&method(:meets_release_constraint?))
            http_method = operation_data["x-github-http-method"] || operation_data.dig("x-github-backfill", "http-method")
            http_path = operation_data["x-github-http-path"] || operation_data.dig("x-github-backfill", "path")
            if http_method && http_path
              ref = operation_path.to_s[%r{/(operations/.+?\.yaml)\Z}, 1]
              memo << OperationRef.new(http_path, http_method, ref)
            end
          end
        end
      end

      def operation_filenames
        OpenApi.root.glob("operations/**/*.yaml")
      end
    end
  end
end
