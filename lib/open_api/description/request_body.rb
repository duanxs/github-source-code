# frozen_string_literal: true

module OpenApi
  module Description
    class RequestBody
      def initialize(request_body)
        @raw = request_body
      end

      def validate(value, request_media_type)
        content = @raw["content"]

        if !content.key?(request_media_type) && !skip_media_type_check?(request_media_type)
          return [{
            code: "UNSUPPORTED_MEDIA_TYPE",
            message: "Request media type not handled by operation, only found these supported media types: #{content.keys}",
            possible: content.keys,
            actual: request_media_type
          }]
        end

        media_type     = content.keys.first
        api_media_type = Api::MediaType.new(media_type)

        # application/json or any +json suffixes
        if !(media_type == "application/json" || api_media_type.json?)
          return [{ message: "Only JSON content types are currently handled, got #{api_media_type}" }]
        end

        schema = OpenApi::Description::Schema.new(content[media_type]["schema"])

        json = GitHub::JSON.parse(value)
        json_path = "/requestBody/content/#{json_pointer_escape(media_type)}/schema"

        schema.validate(json).to_a.map do |error|
          error.merge(path: [json_path, error[:path]].join("/"))
        end
      end

      private

      def skip_media_type_check?(request_media_type)
        # Some tests dont set a request media type.
        # And application/x-www-form-urlencoded is the default POST media type for sinatra
        # Act as if this was OK during validation
        TestEnv.open_api_validation? && (request_media_type.nil? || request_media_type == "application/x-www-form-urlencoded")
      end

      def json_pointer_escape(segment)
        segment.gsub("/", "^/")
      end
    end
  end
end
