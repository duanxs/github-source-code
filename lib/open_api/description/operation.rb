# frozen_string_literal: true

module OpenApi
  module Description
    class Operation
      MISSING_PATH_PARAMETER = "MISSING_PATH_PARAMETER"
      MISSING_QUERY_PARAMETER = "MISSING_QUERY_PARAMETER"
      UNDESCRIBED_QUERY_PARAMETER = "UNDESCRIBED_QUERY_PARAMETER"

      attr_reader :raw, :path_parameters, :query_parameters

      def initialize(operation)
        @raw = operation

        @path_parameters = {}
        @query_parameters = {}

        parameters = (@raw["parameters"] || []).each_with_index do |param, index|
          if param["in"] == "path"
            @path_parameters[param["name"]] = Parameter.new(param, index)
          elsif param["in"] == "query"
            @query_parameters[param["name"]] = Parameter.new(param, index)
          end
        end

        @request_body = RequestBody.new(@raw["requestBody"]) if @raw["requestBody"]
      end

      def id
        @raw["operationId"]
      end

      def backfill_source
        json_schema_file = @raw.dig("x-github-backfill", "json-schema-file")

        if json_schema_file
          "json_schema"
        else
          "docs"
        end
      end

      def validate_request(request, path_parameters)
        validation_errors = []

        # First validate path parameters
        validation_errors += validate_path_parameters(path_parameters)

        # Then query params `?query=1`
        validation_errors += validate_query_parameters(request)

        # Then the request body
        validation_errors += validate_request_body(request)

        validation_errors
      end

      def validate_response(status, headers, body)
        if @raw["responses"]
          responses = Responses.new(@raw["responses"], @raw["operationId"])
        else
          return [{ message: "Operation `#{@raw["operationId"]}` has no defined responses" }]
        end

        responses.validate(status, headers, body)
      end

      private

      def validate_path_parameters(request_params)
        errors = []

        @path_parameters.each do |name, parameter|
          if request_params.key?(name)
            errors += parameter.validate(request_params[name])
          elsif parameter.required?
            # param was missing but is required
            # When can that actually happen?
            errors << {
              code: MISSING_PATH_PARAMETER,
              parameter: parameter.name,
              message: "Missing required path parameter #{parameter.name}",
              path: parameter.json_pointer,
            }
          end
        end

        errors
      end

      def validate_query_parameters(request)
        errors = []

        request_params = request.GET

        @query_parameters.each do |name, parameter|
          if request_params.key?(name)
            errors += parameter.validate(request_params[name])
          elsif parameter.required?
            errors << {
              code: MISSING_QUERY_PARAMETER,
              message: "Missing required query parameter #{parameter.name}",
              parameter: parameter.name,
              path: parameter.json_pointer
            }
          end
        end

        request_params.each do |key, value|
          if !@query_parameters[key]
            errors << {
              code: UNDESCRIBED_QUERY_PARAMETER,
              message: "Undescribed parameter #{key}",
              parameter: key,
              value: value
            }
          end
        end

        errors
      end

      def validate_request_body(request)
        return [] if @request_body.nil?

        request.body.rewind
        validation_result = @request_body.validate(request.body.read, request.media_type)
        request.body.rewind

        validation_result
      end
    end
  end
end
