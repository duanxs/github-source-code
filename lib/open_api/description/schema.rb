# frozen_string_literal: true

module OpenApi
  module Description
    KEYWORDS_WITH_SUBSCHEMAS = %w(
      allOf anyOf oneOf not if then else items
      additionalItems contains propertyNames
      properties patternProperties additionalProperties
    )

    class Schema
      attr_reader :raw
      def initialize(raw)
        @raw = raw
      end

      def validate(value, coerce: true)
        SchemaValidator.validate(schema: self, value: value, coerce: coerce)
      end

      def type
        raw["type"]
      end

      def nullable?
        !!@raw["nullable"]
      end

      def subschema_keyword
        subkeys = KEYWORDS_WITH_SUBSCHEMAS & raw.keys
        if subkeys
          subkeys.first
        end
      end

      def has_subschemas?
        !!subschema_keyword
      end

      def properties
        (raw["properties"] || []).each_with_object({}) do |(prop, schema), obj|
          obj[prop] = reflect(schema)
        end
      end

      def additional_properties
        return true if raw["additionalProperties"].nil?

        return raw["additionalProperties"] unless raw["additionalProperties"].respond_to?(:each)
        return raw["additionalProperties"] if raw["additionalProperties"].blank?

        reflect(raw["additionalProperties"])
      end

      def items
        reflect(raw["items"])
      end

      def required
        raw["required"]
      end

      def any_of?
        raw.key?("anyOf")
      end

      def any_of
        schemas_for("anyOf")
      end

      def one_of?
        raw.key?("oneOf")
      end

      def one_of
        schemas_for("oneOf")
      end

      def all_of?
        raw.key?("allOf")
      end

      def all_of
        schemas_for("allOf")
      end

      private

      def reflect(raw_schema)
        self.class.new(raw_schema)
      end

      def schemas_for(rule)
        return [] if raw[rule].nil?
        raw[rule].map { |sub_schema| reflect(sub_schema) }
      end
    end
  end
end
