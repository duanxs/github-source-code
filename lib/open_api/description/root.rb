# frozen_string_literal: true

module OpenApi
  module Description
    class Root
      attr_reader :raw

      def initialize(description)
        @raw = description
      end

      def paths
        @raw["paths"]
      end

      def operation(verb, path)
        verb = verb.downcase

        path = paths[path]
        return if path.nil?

        operation = path[verb]
        return if operation.nil?

        OpenApi::Description::Operation.new(operation)
      end
    end
  end
end
