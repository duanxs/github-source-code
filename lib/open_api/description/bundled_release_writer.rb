# frozen_string_literal: true

module OpenApi
  module Description
    class BundledReleaseWriter < ReleaseWriter

      def path(format: :json)
        OpenApi.root.join(
          "generated",
          File.basename(@release.filename, ".yaml"),
          File.basename(@release.filename, ".yaml") << ".#{format}"
        )
      end

      def content
        return @content if defined?(@content)
        @content = Marshal.load(Marshal.dump(@release.content))
        assigning_components do
          expand(@content, OpenApi.root.join(@release.filename))
        end
        replace_variables(@content)
        @content
      end

      def assigning_components(&block)
        @components = {}
        yield
        # Insert components inline
        @content["components"] ||= {}
        @content["components"].deep_merge!(@components)
      end

      def serialize(content, format:)
        if format == :json
          JSON.pretty_generate(content)
        elsif format == :yaml
          YAML.dump(content)
        else
          raise ArgumentError, "Unhandled format #{format}"
        end
      end

      private

      # Private: Recursively expand opertation $ref values in a datastructure, relative to a path,
      #          stripping any disallowed extended properties, and moving any component $refs to
      #          inline components.
      #
      # value - The data
      # source_path - a Pathname of the source file, used to resolve relative references
      #
      # Modifications are made in-place.
      def expand(value, source_path)
        # Expand
        case value
        when Hash
          OpenApi::Description::Overlay.apply(value, @release)
          strip_extended_properties!(value)

          if value.key?("$ref")
            if value.size != 1 && !value["$ref"].include?("components/x-previews")
              raise "$ref is only allowed as the _only_ key in an object (found: #{value.inspect})"
            end
            # Mark this file as expanded for any subsequent expansions
            ref_target = value.delete("$ref")
            if ref_target.start_with?("#")
              raise ArgumentError, "Inline components not supported: #{ref_target}"
            else
              ref_path = source_path.dirname.join(ref_target)
              if ref_path.exist?
                # Convert to absolute; remove .., etc
                ref_path = ref_path.realpath
                if ref_path.to_s.include?("/x-previews/")
                  ref_value = YAML.load_file(ref_path)
                elsif ref_path.to_s.include?("/components/")
                  ref_value = expand_component(ref_path)
                else
                  ref_value = expand_operation(ref_path)
                end
                value.merge!(ref_value)
              else
                raise ArgumentError, "Bad $ref #{ref_target.inspect} from #{source_path}"
              end
            end
          else
            value.each do |k, v|
              expand(v, source_path)
            end
          end
        when Array
          value.each { |v| expand(v, source_path) }
        else
          value
        end
      end

      # Private: Expand a component, assign it as an inline component, and
      #          change the reference accordingly.
      #
      # ref_path - the Pathname pointing to the component
      #
      # Returns a Hash with the updated (inline component) ref.
      def expand_component(ref_path)
        address = ref_path.to_s.
          sub(%r{\A.*?/components/}, "").
          sub(%r{\..*?\Z}, "").
          split("/")

        unless @components.dig(*address)
          # Haven't saved component, need to assign
          ref_value = YAML.load_file(ref_path)
          expand(ref_value, ref_path)
          assign_component(ref_value, address)
        end
        {"$ref" => File.join("#/components", *address)}
      end

      # Private: Expand a reference to an operation
      #
      # ref_path - the Pathname pointing to the operation
      #
      # Returns a Hash with the operation contents.
      def expand_operation(ref_path)
        ref_value = YAML.load_file(ref_path)
        expand(ref_value, ref_path)
        ref_value
      end

      # Private: Assign component contents to a component address.
      #
      # - content - a Hash
      # - address - an Array of String values
      def assign_component(content, address)
        target = @components
        address.each do |element|
          target = target[element] ||= {}
        end
        target.replace(content)
      end
    end
  end
end
