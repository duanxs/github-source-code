# frozen_string_literal: true

module Platform
  module Provisioning
    class SamlGroupData < UserData
      user_data_reader name_id: "NameID"

      def self.load(group_name)
        self.new.tap do |user_data|
          user_data.append "NameID", group_name,
                           "Format" => "urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified"
        end
      end
    end
  end
end
