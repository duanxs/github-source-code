# frozen_string_literal: true

# Mapper responsible for taking GroupData provided via SCIM
# protocol and mapping that to an ExternalIdentity.
module Platform::Provisioning
  module SCIMGroupMapper

    # Public: Builds a new or finds an existing SAML provisioned identity from SCIM user data
    #
    # SCIM doesn't doesn't find existing SCIM provisioned identities based on SCIM user data
    # since it uses explicit create or update actions. An error will still
    # occur if attempting to create an identity with an existing 'userName'
    # attribute. ('userName' will mirror 'displayName' for Group Identities)
    #
    # target        - The Business which the identity will be under.
    # user          - Organization that this ExternalIdentity is for
    # user_data     - group data from the identity provider
    #
    # Returns an ExternalIdentity
    def self.find_or_build_identity(target:, user:, user_data:)
      provider = target.external_identity_session_owner.saml_provider
      mapping = provider.identity_mapping

      # Only lookup pre-existing SAML provisioned identities.
      identity = ExternalIdentity.by_provider(provider).
        by_identifier(user_data, mapping: mapping, identifier_attributes: mapping.saml_attribute).
        first

      if identity
        # cleanup to make sure that the next time we won't treat this ExternalIdentity
        # as a SAML-provisioned one
        identity.saml_user_data = SamlGroupData.new
        identity.user = user
      else
        identity = ExternalIdentity.new(provider: provider, user: user)
      end

      identity
    end

    def self.set_user_data(identity:, user_data:)
      identity.scim_user_data = user_data
      identity
    end

  end
end
