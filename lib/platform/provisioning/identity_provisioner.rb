# frozen_string_literal: true
# rubocop:disable GitHub/UsePlatformErrors

module Platform
  module Provisioning
    class IdentityProvisioner

      # raised if IdentityProvisioner methods are invoked when a target Enterprise
      # that doesn't have the `enterprise_idp_provisioning` feature flag enabled.
      class UserProvisioningNotAllowed < StandardError
        def initialize
          super "User Provisioning is not allowed for this Enterprise."
        end
      end

      # Public: Updates or provisions an identity under the target.
      #
      # target_id       - The target the identity is under. Currently Business and Organization
      #                   objects can be targeted.
      # user_id         - (optional) The ID of the User the identity is linked to.
      # identity_guid   - (optional) The GUID of the external identity to
      #                   provision. If not specified, the mapper will be used
      #                   to find or build one.
      # user_data       - The Platform::Provisioning::UserData with all of the
      #                   IdP provided data about the user.
      # mapper          - The provisioning mapper responsible for mapping the
      #                   provided user_data to an external identity. This is
      #                   specific for the provisioning scheme (e.g. SAML vs SCIM).
      # identity        - (optional) The external identity to provision. If not specified
      #                   the mapper will be used to find or build one
      def self.provision(target:, user_id: nil, identity_guid: nil, user_data:, mapper:, identity: nil)
        return invalid_provisioning_target(target: target) unless target.is_a?(Business)
        return identity_not_found_error_status unless identity_guid.nil? || identity = target.saml_provider.external_identities.where(guid: identity_guid).first
        user = User.find_by id: user_id

        provision_identity(target: target, identity: identity, user: user, user_data: user_data, mapper: mapper) do |identity|
          success_status(identity)
        end
      end

      # Public: Provisions a new identity under the target for the given user and
      # adds the user to the organizations listed in the SAML `groups` attribute
      #
      # target               - The target the identity is under. Currently only Business
      #                        objects can be targeted.
      # user_id              - (optional) The ID of the User the identity is linked to.
      # identity_guid        - (optional) The GUID of the external identity to
     #                         provision. If not specified, the mapper will be used
      #                        to find or build one.
      # user_data            - The Platform::Provisioning::UserData with all of the
      #                        IdP provided data about the user.
      # mapper               - The provisioning mapper responsible for mapping the
      #                        provided user_data to an external identity. This is
      #                        specific for the provisioning scheme (e.g. SAML vs SCIM).
      # sso_invitation_token - The ID of the Organization::Invitation being accepted
      #                        on behalf of the user.
      def self.provision_and_add_member(target:, user_id: nil, identity_guid: nil, user_data:, mapper:, sso_invitation_token: nil)
        return invalid_provisioning_target(target: target) unless target.is_a?(Business)
        raise UserProvisioningNotAllowed.new unless enterprise_user_provisioning_enabled?(target: target)
        return identity_not_found_error_status unless identity_guid.nil? || identity = target.saml_provider.external_identities.where(guid: identity_guid).first
        return internal_provisioning_error_status unless user = User.find_by_id(user_id)

        provision_identity(target: target, identity: identity, user: user, user_data: user_data, mapper: mapper) do |identity|
          next success_status(identity) unless target.saml_provider.saml_provisioning_enabled?

          allowed_organizations_args = {
            target: target,
            allowed_organizations: get_allowed_organizations(business: target, user_data: user_data),
            identity: identity,
            allow_deprovisioning: target.saml_provider.saml_deprovisioning_enabled?,
            actor: user
          }
          results = with_allowed_organizations(**allowed_organizations_args) do |organization|
            OrganizationIdentityProvisioner.create_membership \
              identity, organization: organization, sso_invitation_token: sso_invitation_token
          end

          # TODO - need to devise a way to handle partial successes (able to join org A, but not org B)
          # or roll back the whole operation and fail altogether? For partial success, need to update
          # the error messages to include the name of the organization they were unable to join

          aggregate_results(results, identity)
        end
      end

      # Public: Provisions a new identity in the enterprise, inviting the identity to the list of organizations
      # via the given email address (when present).
      #
      # If the identity is already linked to a user, no invitation is delivered.
      #
      # target          - The Business to provision users into
      # user_data       - The Platform::Provisioning::UserData with all of the
      #                   IdP provided data about the user. Includes groups which will map to Organization names
      # inviter_id      - The ID of the actor sending the invitation.
      # inviter_type    - The type of the actor sending the invitation (defaults to User).
      # mapper          - The provisioning mapper responsible for mapping the
      #                   provided user_data to an external identity. This is
      #                   specific for the provisioning scheme (e.g. SAML vs SCIM).
      # identity        - In the case of Enterprise SCIM we might have an identity whose `groups` have just been updated
      #                   If so we need to send the identity through the provisioning process to make sure org memberships
      #                   get trued up.
      def self.provision_and_invite(target:, user_data:, inviter_id:, inviter_type: User, mapper:, identity: nil)
        return invalid_provisioning_target(target: target) unless target.is_a?(Business)
        raise UserProvisioningNotAllowed.new unless enterprise_user_provisioning_enabled?(target: target)

        provision_identity(target: target, user_data: user_data, mapper: mapper, identity: identity) do |identity|
          next success_status(identity) if identity.user.nil? && identity.scim_user_data.email.blank?

          organizations = get_allowed_organizations(business: target, user_data: user_data)

          allowed_organizations_args = {
            target: target,
            allowed_organizations: organizations,
            identity: identity,
            # this method is currently only called by SCIM, and SCIM de-provisioning is
            # automatically enabled, so just pass true.
            # If SAML ever calls here, will need to check saml_deprovisioning_enabled?
            allow_deprovisioning: true,
            actor: find_inviter(inviter_id, inviter_type)
          }

          organization_invitation_results = with_allowed_organizations(**allowed_organizations_args) do |organization|
            invite_identity(organization, identity, inviter_id, inviter_type)
          end

          aggregate_results(organization_invitation_results, identity)
        end
      end

      # Public: De-provisions an identity and removes user from the enterprise.
      #
      # target        - The enterprise that the identity is under.
      # user_data     - The Platform::Provisioning::UserData with all of the
      #                 IdP provided data about the user. Optional and will be
      #                 blank for DELETE requests.
      # identity_guid - The guid of the external_identity to be deprovisioned
      # actor_id      - Id of the actor making the deprovision request
      # hard_delete   - If the deprovision request is a "hard delete". This means
      #                 the External identity will not be used again.
      def self.deprovision(target:, user_data: nil, identity_guid:, actor_id:, hard_delete: false)
        return invalid_provisioning_target(target: target) unless target.is_a?(Business)
        raise UserProvisioningNotAllowed.new unless enterprise_user_provisioning_enabled?(target: target)
        return internal_provisioning_error_status unless target.present?
        return internal_provisioning_error_status unless actor = User.find_by_id(actor_id)

        identity = target.
          saml_provider.
          external_identities.
          with_attributes.
          where(guid: identity_guid).
          first

        return identity_not_found_error_status unless identity.present?
        return cannot_deprovision_self_error_status if identity.user_id == actor_id

        user = identity.user

        invitations = target.
          pending_invitations.
          with_invitee_or_normalized_email(invitee: user, emails: identity.scim_user_data.emails)

        credential_authorizations = Organization::CredentialAuthorization.where \
          actor_id: identity.user_id,
          organization: target.organizations

        context = {
          external_identity: identity,
          operation: "deprovision",
          identity_mapper: Platform::Provisioning::ScimMapper.to_s,
        }

        GitHub.context.push context
        Audit.context.push context

        ExternalIdentity.transaction do
          identity.destroy
          credential_authorizations.destroy_all

          invitations.each { |invitation| invitation.cancel(actor: actor) }

          target.remove_member(user, actor: actor) if user
        end

        GitHub.dogstats.increment("enterprises.scim.deprovision")

        success_status(identity)
      rescue Business::ForbiddenRemovalError, Business::InvalidRemovalError => err
        Platform::Provisioning::Result.new \
          errors: Platform::Provisioning::Error.forbidden_error(message: err.message)
      end

      # Private: processes UserData to ensure that the user/identity is associated with any corresponding
      #     target organizations. Yields a block for each organization that the user/identity should be associated
      #     with for further specific processing (invitations/membership).
      #
      # WARNING! the user's organization membership will be REMOVED if they currently belong to any organizations that
      # are not present in `allowed_organizations`. Each `allowed_organizations` is yielded to a block for specific
      # processing re: inviting a user to an organization or adding them.
      #
      # target: the Business that the identity will be associated with
      # allowed_organizations: the organizations that the identity could be associated with
      # identity: the external identity
      # allow_deprovisioning: flag for whether we can remove users from orgs or not (invitations
      #                       may be cancelled regardless)
      # actor: user performing the action - audit log actor for cancelling invitations and
      #        removing org members
      def self.with_allowed_organizations(target:, allowed_organizations:, identity:, allow_deprovisioning:, actor:)
        raise UserProvisioningNotAllowed.new unless enterprise_user_provisioning_enabled?(target: target)

        user = identity.user
        possibly_associated_organizations = OrganizationInvitation
          .where(external_identity: identity, organization: target.organizations)
          .includes(:organization)
          .map(&:organization).to_a.compact.uniq

        if user.present?
          user_organization_logins = (user.invited_organizations + user.organizations).compact.map(&:login)
          possibly_associated_organizations = possibly_associated_organizations | target.organizations.where(login: user_organization_logins).to_a
        end

        # we have the current target organizations that are associated with the identity/user
        # next we'll compare these to allowed_organizations and see if there are any differences
        # if a current_target_organization exists that's not allowed we'll remove the user/identity
        # we'll then yield all the allowed_organizations to the block
        results = []
        unpermitted_organizations = possibly_associated_organizations - allowed_organizations

        # cancel any unpermitted organization invitations
        invitations_scope = OrganizationInvitation.pending.where(
          external_identity: identity,
          organization: unpermitted_organizations,
        )
        if user.present?
          invitations_scope = invitations_scope.or(
            OrganizationInvitation.pending.where(
              invitee: user,
              organization: unpermitted_organizations
            )
          )
        end
        # REVIEW: for the SAML case, this ends up with the audit log recording that the user has
        # cancelled their own invitation... which is a little confusing (though better than not
        # having an actor for the action at all?)
        # Tracking issue: https://github.com/github/admin-experience/issues/484
        invitations_scope.each { |org_invitation| org_invitation.cancel(actor: actor) }

        if allow_deprovisioning
          unpermitted_organizations.each do |unpermitted_organization|
            GitHub.context.push(actor_id: actor.id) do
              unpermitted_organization.remove_member(user,
                background_team_remove_member: true, allow_last_admin_removal: true)
            end
            results << Platform::Provisioning::Result.new(
              external_identity: identity,
              errors: "User deprovisioned from #{unpermitted_organization}",
            )

            GitHub.dogstats.increment("enterprises.scim.remove_org_member")
          end
        end

        results = []
        if block_given?
          allowed_organizations.each do |allowed_organization|
            results << yield(allowed_organization)
          end
        end
        results
      end

      def self.invite_identity(organization, identity, inviter_id, inviter_type)
        return internal_provisioning_error_status unless organization && identity

        # Check if the user is already a member of this org, or if the ExternalIdentity has already
        # been invited via a different email address
        return success_status(identity) if organization.member?(identity.user)
        invitation = OrganizationInvitation.pending.
          find_by(organization: organization, external_identity: identity)
        unless inviter_id.present? || has_pending_invite_with_different_email?(identity, invitation)
          return success_status(identity)
        end

        # can't proceed if no inviter_id present. User is not logged in and coming from SAML.
        # they should be prompted to sign in at which point they'll pass through `provision_and_add_member` which
        # will add them to the target's organizations
        inviter = find_inviter(inviter_id, inviter_type)
        return internal_provisioning_error_status unless inviter

        if invitation
          # API/auth checks should prevent this?
          return internal_provisioning_error_status unless invitation.cancelable_by?(inviter)

          invitation.cancel(actor: inviter)

          # clean up the cancelled invitation's associations
          invitation.update!(external_identity: nil)
          identity.reload
        end

        invitee, email = if identity.user.present?
          [identity.user, nil]
        else
          [nil, identity.scim_user_data.email]
        end
        invite_status = organization.invite(invitee,
          email: email,
          inviter: inviter,
          role: "reinstate",
          external_identity: identity,
        )
        if invite_status.valid?
          success_status(identity)
        else
          Platform::Provisioning::Result.new \
            errors: Platform::Provisioning::Error.invite_identity
        end
      rescue OrganizationInvitation::AlreadyAcceptedError, OrganizationInvitation::InvalidError, ActiveRecord::RecordInvalid => e
        Platform::Provisioning::Result.new \
          errors: Platform::Provisioning::Error.invite_identity(message: e.message)
      rescue OrganizationInvitation::NoAvailableSeatsError => e
        target = identity.target

        message = "Unable to provision user. The #{target.name} enterprise has no available seats. Please purchase more seats and try again."

        Platform::Provisioning::Result.target_at_seat_limit_error(message: message)
      end

      # Internal: Builds a failed status when we weren't able to find the
      # user and/or organization to provision an identity for.
      #
      # This is not the result of user or IdP error. If this happens,
      # it is a problem on our end.
      def self.internal_provisioning_error_status
        Platform::Provisioning::Result.new \
          errors: Platform::Provisioning::Error.internal_error(
            message: "An internal error has occurred. Please try again. If the problem persists, contact support.",
          )
      end

      # Internal: Builds a failed status when we weren't able to find the identity
      # that the user is trying to operate on
      def self.identity_not_found_error_status
        Platform::Provisioning::Result.new \
          errors: Platform::Provisioning::Error.identity_not_found(
            message: "Unable to find the submitted identity. Please verify the identity information and submit again.",
          )
      end

      # Internal: Builds a failed status when we weren't able to provision an identity for the `target`
      # passed.
      #
      # This is not the result of user or IdP error. If this happens, it is a problem on our end.
      def self.invalid_provisioning_target(target:)
        Platform::Provisioning::Result.new \
          errors: Platform::Provisioning::Error.internal_error(
          message: "Unable to provision an identity, #{target} is not an Enterprise account.",
        )
      end

      # Internal: Builds a failed status when the admin that authenticated is the
      # same as the user that they are attempting to deprovision
      def self.cannot_deprovision_self_error_status
        Platform::Provisioning::Result.new \
          errors: Platform::Provisioning::Error.forbidden_error(
            message: "Unable to deprovision the admin authenticated to perform the SCIM actions. Authenticate your SCIM client with a different account and try again.",
          )
      end

      # Internal: Builds a success status
      def self.success_status(identity)
        Platform::Provisioning::Result.success(external_identity: identity)
      end

      # Internal: Builds a success status
      def self.pending_status(identity)
        Platform::Provisioning::Result.pending(external_identity: identity)
      end

      def self.save_identity(identity, target: nil, user: nil, mapper:)
        if identity.save
          success_status(identity)
        else
          messages = identity.errors.full_messages

          if ExternalIdentity.name_id_already_taken?(identity) &&
              existing_identity = mapper.find_or_build_identity(target: target, user: user, user_data: identity.saml_user_data)
            if existing_identity.persisted?
              messages = [
                "The identity '#{existing_identity.saml_user_data.name_id}' "\
                "is linked to your GitHub account @#{user.login}, "\
                "but you signed on as '#{identity.saml_user_data.name_id}'",
              ]
            end
          end

          Platform::Provisioning::Result.new \
            errors: messages.map { |m| Platform::Provisioning::Error.invalid_identity(message: m) }
        end
      end

      def self.rollback_unless_success(result)
        raise ActiveRecord::Rollback unless result.success?
      end

      # Private: Provisions a new identity and yields it to a block if given
      #
      # target          - The Business or Organization that the identity is under.
      # user            - (Optional) The User the identity is linked to.
      # identity        - (Optional) An existing identity to be updated instead of provisioning
      #                   a new one.
      # user_data       - The Platform::Provisioning::UserData with all of the
      #                   IdP provided data about the user.
      # mapper          - The provisioning mapper responsible for mapping the
      #                   provided user_data to an external identity. This is
      #                   specific for the provisioning scheme (e.g. SAML vs SCIM).
      def self.provision_identity(target:, identity: nil, user: nil, user_data:, mapper:)
        result = nil

        identity ||= mapper.find_or_build_identity \
          target: target,
          user: user,
          user_data: user_data
        mapper.set_user_data(identity: identity, user_data: user_data)

        ExternalIdentity.transaction do
          result = save_identity(identity, user: user, target: target, mapper: mapper)
          rollback_unless_success(result)

          context = {
            external_identity: identity,
            identity_mapper: mapper.to_s,
            operation: "provision",
            linked_user_id: user.try(:id),
            target: target,
          }

          # Audit log data
          GitHub.context.push context
          Audit.context.push context

          if block_given?
            result = yield(identity)
            rollback_unless_success(result)
          end
        end

        result
      end

      # Private: aggregates the results of several provisioning operations into a single
      # Platform::Provisioning::Result
      #
      # if all the operations had succeeded, returns success_status
      # if any results are pending, returns pending_status
      # if any had failed, returns an error result, which includes all the failures
      #
      # provisioning_results - results of individual provisioning operations
      # identity             - external identity that we were provisioning for
      def self.aggregate_results(provisioning_results, identity)
        failure = false
        pending = false
        provisioning_results.each do |result|
          failure ||= result.errors.any?
          break if failure

          pending ||= result.pending?
        end

        if failure
          failed_result_errors = Set.new
          provisioning_results.reject(&:success?).map do |result|
            result.errors.each do |result_error|
              failed_result_errors.add(result_error)
            end
          end

          Platform::Provisioning::Result.new(external_identity: identity,
                                             errors: failed_result_errors.to_a)
        elsif pending
          pending_status(identity)
        else
          success_status(identity)
        end
      end

      # Private: takes a Business and returns all Organizations that the user should be associated
      # with based on metadata passed in the `groups` attribute of `user_data`
      #
      # The `groups` attribute can reference Organizations either directly by their logins, or via
      # the associated Group External Identity by specifying the ExternalIdentity's guid. We
      # start by treating all the values as logins, then try to locate ExternalIdentity's for any
      # that didn't match an Organization login.
      #
      # For any Organization that's found via its login, we'll create a Group ExternalIdentity, if
      # it doesn't already have one.
      #
      # business           - Find Organizations that belong to this Business.
      # user_data          - The Platform::Provisioning::UserData containing the group attributes
      # allow_provisioning - can we provision an ExternalIdentity for an organization, if it
      #                      doesn't yet have one? Always true for SCIM operations; depends on the
      #                      setting for SAML.
      #
      # Returns: ActiveRelation of Organizations
      def self.get_allowed_organizations(business:, user_data:, allow_provisioning: true)
        org_logins = business.saml_provider.group_ids(user_data: user_data).keys.compact.uniq
        organizations = business.organizations.
          where(login: org_logins).
          includes(:external_identities)

        if allow_provisioning
          organizations.select { |org| org.external_identity.nil? }.each do |organization|
            group_data = Platform::Provisioning::SamlGroupData.load(organization.login)
            options = {
              target: business,
              user_id: organization.id,
              user_data: group_data,
              mapper: Platform::Provisioning::SamlGroupMapper,
            }

            provision(**options)
          end
        end

        organizations
      end

      # Private: guard method to ensure we cancel/re-send an invitation when provisioning an identity
      # if an existing OrganizationInvitation exists for a different e-mail address
      def self.has_pending_invite_with_different_email?(identity, invitation)
        identity.user.nil? &&
          invitation.present? &&
          invitation.pending? &&
          invitation.email? &&
          identity.scim_user_data.email != invitation.email
      end

      def self.find_inviter(inviter_id, inviter_type)
        case inviter_type.to_s
        when "IntegrationInstallation"
          if (installation = IntegrationInstallation.find_by(id: inviter_id))
            installation.bot
          end
        when "User"
          User.find_by(id: inviter_id)
        end
      end

      def self.enterprise_user_provisioning_enabled?(target:)
        GitHub.flipper[:enterprise_idp_provisioning].enabled?(target)
      end
    end
  end
end
