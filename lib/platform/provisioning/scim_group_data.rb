# frozen_string_literal: true

module Platform
  module Provisioning
    class SCIMGroupData < UserData
      user_data_reader \
        external_id: "externalId",
        display_name: "displayName",
        user_name: "userName",
        members: "members"

      # Public: Instantiates a new UserData instance from the provided SCIM input.
      #
      # scim_input - request data represented as a Hash.
      #
      # Returns a SCIMGroupData instance.
      def self.load(scim_input)
        scim_attrs = [
          # set the userName to displayName, so ExternalIdentity can do uniqueness validation
          { "name" => "userName", "value" => scim_input["displayName"] },
          { "name" => "displayName", "value" => scim_input["displayName"] },
          { "name" => "externalId", "value" => scim_input["externalId"] },
        ]

        if scim_input["members"].is_a?(Array)
          scim_attrs += scim_input["members"].map do |member|
            { "name" => "members", "value" => member["value"] }
          end
        end

        new(scim_attrs)
      end
    end
  end
end
