# frozen_string_literal: true

# Mapper responsible for taking UserData provided via SCIM
# protocol and mapping that to an ExternalIdentity.
module Platform
  module Provisioning
    module ScimMapper

      # Builds a new or finds existing SAML provisioned identity from SCIM user data
      #
      # SCIM doesn't doesn't find existing SCIM provisioned identities based on SCIM user data
      # since it uses explicit create or update actions. An error will still
      # occur if attempting to create an identity with an existing 'userName'
      # attribute.
      #
      # Additionally, SCIM doesn't care about the user since it only deals
      # with creating the identity and never linking the identity to a user.
      def self.find_or_build_identity(target:, user: nil, user_data:)
        provider = target.external_identity_session_owner.saml_provider
        mapping = provider.identity_mapping

        # Only lookup pre-existing SAML provisioned identities.
        identity = ExternalIdentity.by_provider(provider).
          by_identifier(user_data, mapping: mapping, identifier_attributes: mapping.saml_attribute).first

        identity ||= ExternalIdentity.new(provider: provider)
        identity
      end

      def self.set_user_data(identity:, user_data:)
        identity.scim_user_data = user_data

        return identity unless identity.provider.is_a?(Business::SamlProvider)

        saml_groups_wrapper = Platform::Provisioning::GroupsUserDataWrapper.new(identity.saml_user_data)
        return identity if saml_groups_wrapper.groups.empty?

        group_ids = identity.provider.group_ids(user_data: identity.scim_user_data)
        identity.saml_user_data = \
          saml_groups_wrapper.sync_group_attributes(valid_group_ids: group_ids)

        identity
      end
    end
  end
end
