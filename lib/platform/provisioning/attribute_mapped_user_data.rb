# frozen_string_literal: true

require "forwardable"

module Platform
  module Provisioning
    class AttributeMappedUserData
      SUPPORTED_ATTRIBUTES = {
        user_name: :single,
        external_id: :single,
        display_name: :single,
        emails: :multiple,
        given_name: :single,
        family_name: :single,
        active: :single,
        ssh_keys: :multiple,
        gpg_keys: :multiple,
        admin: :single,
        groups: :multiple,
      }

      extend Forwardable

      # delegate methods directly to the underlying UserData
      def_delegators :@user_data,
        :each,
        :append,
        :replace,
        :delete,
        :delete_all,
        :delete_if,
        :slice,
        :to_a,
        :map,
        :any?,
        :email,
        :active?,
        :name_id,
        :name_id_metadata,
        :default_attribute_mapping,
        :as_json,

      # Public: Defines a reader helper for attributes that return multiple values.
      #
      # method_name   - A name of a method to setup
      #
      # Returns   - An array of values, for example a list of emails
      def self.define_multiple_return_value_method(method_name)
        define_method(method_name) do
          return @user_data.send(method_name) if @user_data.respond_to?(method_name)

          fetch_all(method_name).map { |value| value["value"] }
        end
      end

      # Public: Defines a reader helper for attributes that return a single value.
      #
      # method_name   - A name of a method to setup
      #
      # Returns   - A value for a given attribute
      def self.define_single_return_value_method(name, field: "value", suffix: nil)
        method_name = [name, suffix].compact.join("_")
        define_method(method_name) do
          fetch(name, {})[field]
        end
      end

      private_class_method :define_multiple_return_value_method,
        :define_single_return_value_method

      # Public: Defines a reader helper for easily fetching an attributes
      # value and metadata.
      #
      # Examples
      #
      #   user_data.user_name
      #   #=> "mona.octocat"
      #   user_data.user_name_metadata
      #   #=> {"format" => "…"}
      SUPPORTED_ATTRIBUTES.each do |method_name, type|
        if type == :multiple
          define_multiple_return_value_method(method_name)
        elsif type == :single
          define_single_return_value_method(method_name)
          define_single_return_value_method(method_name, field: "metadata", suffix: "metadata")
        end
      end

      # Public: Initialize an AttributeMappedUserData.
      #
      # user_data  - User Data to wrap, can either be an instance of SCIM or SAML user data
      #              (ScimUserData or SamlUserData).
      # mapping    - mapping hash to setup readers for, also used in a fetch methods.
      #              It is an optional parameter and when not passed the user data will provide one
      def initialize(user_data, mapping: nil)
        @user_data = user_data
        @mapping = merge_with_defaults(mapping: mapping)
      end

      # Public: A fetch method to get the value out of the user data using a provided map
      #
      # name   - Name of the standardized accessor for all user data
      # default - Default value to return
      #
      # Returns - a hash of name/value pair
      def fetch(name, default = nil)
        real_name = @mapping.fetch(name, name)

        case real_name
        when String
          # real_name is a string no additional value checks are needed, just return it
          return @user_data.fetch(real_name, default)
        when Array
          # it's an array, so need to find first value that is not nil
          real_name.each do |lookup|
            value = @user_data.fetch(lookup)
            return value unless value.nil?
          end
        end

        default
      end

      # Public: A fetch all method to get the value out of the user data using a provided map
      #
      # name   - Name of the standardized accessor for all user data
      #
      # Returns - a array of hashes of name/value pairs
      def fetch_all(name)
        real_name = @mapping.fetch(name, name)

        case real_name
        when String
          # real_name is a string no additional value checks are needed, just return it
          return @user_data.fetch_all(real_name)
        when Array
          # it's an array, so need to find first value that is not nil
          real_name.each do |attr|
            value = @user_data.fetch_all(attr)
            return value unless value.nil? || value.count == 0
          end
        end

        []
      end

      # Public: Convenience helper for fetching 'primary email'.
      #
      # Returns the first email.
      def primary_email
        return @user_data.primary_email if @user_data.respond_to?(:primary_email)

        emails.first
      end

      # Public: Merge two hashes and insert mapping values as the first entry in the array
      #
      # Returns merged hash
      def self.merge_with_attribute_maps(default_map:, custom_map:)
        return default_map if custom_map.nil?

        # no need to check if the value is duplicate, just add it in
        default_map.merge(custom_map) do |key, default_value, custom_value|
          case default_value
          when String
            next custom_value if custom_value == default_value
            [custom_value, default_value]
          when Array
            [custom_value] + default_value
          end
        end
      end

      private

      # Private: Merge two hashes and insert mapping values as the first entry in the array
      #
      # Returns merged hash
      def merge_with_defaults(mapping:)
        self.class.merge_with_attribute_maps(
          default_map: @user_data.default_attribute_mapping,
          custom_map: mapping,
        )
      end
    end
  end
end
