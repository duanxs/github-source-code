# frozen_string_literal: true

module Platform
  class Preview
    INVALID_ARGUMENTS = <<~MESSAGE
    A list of strings must be passed to `owning_teams`.

    For example: `owning_teams "@github/ecosystem-api", "@github/ee-projects"`
    MESSAGE

    def self.owning_teams(*owning_teams)
      if owning_teams.empty?
        @owning_teams || []
      else
        if owning_teams.any? { |owning_team| !owning_team.is_a?(String) }
          raise ArgumentError.new(INVALID_ARGUMENTS) # rubocop:disable GitHub/UsePlatformErrors
        end

        @owning_teams = owning_teams
      end
    end

    def self.dotcom_only_preview
      environments :dotcom
    end

    def self.to_h
      {
        title: title,
        description: description,
        toggled_by: toggled_by,
        announcement: announcement,
        updates: updates,
        toggled_on: toggled_on,
        owning_teams: owning_teams.map(&:to_s),
      }
    end

    def self.environments(*new_environments)
      if new_environments.length > 0
        @environments = new_environments
      else
        @environments ||= [:enterprise, :dotcom]
      end
    end

    def self.title(new_title = nil)
      if new_title
        @title = new_title
      else
        @title
      end
    end

    def self.description(new_description = nil)
      if new_description
        @description = new_description
      else
        @description
      end
    end

    def self.toggled_by(new_toggled_by = nil)
      if new_toggled_by
        @toggled_by = new_toggled_by
      else
        @toggled_by
      end
    end

    def self.announced_on(date, url: nil)
      @announcement = {
        date: date,
        url: url,
      }
    end

    def self.announcement
      @announcement
    end

    def self.updated_on(date, url: nil)
      @updates ||= []

      @updates <<  {
        date: date,
        url: url,
      }
    end

    def self.updates
      @updates
    end

    def self.toggled_on(member_paths = nil)
      if member_paths
        member_paths.each do |path|
          unless path.is_a?(String)
            raise ArgumentError, "Provide string paths to `toggled_on`. For example, to toggle on a field called `name` on a type `Author`, use `\"Author.name\"``" # rubocop:disable GitHub/UsePlatformErrors
          end
        end

        @member_paths = member_paths
      else
        @member_paths
      end
    end

    def self.uninstall(schema, previews)
      previews.each do |prev|
        prev.set_preview_toggled_by_on_schema(schema, nil)
      end
    end

    def self.install(schema, previews)
      previews.each do |prev|
        toggled_environments = prev.environments
        value = Hash[toggled_environments.map { |env| [env, prev] }]
        prev.set_preview_toggled_by_on_schema(schema, value)
      end
    end

    # @api private
    def self.set_preview_toggled_by_on_schema(schema, value)
      members = @member_paths.map { |path| schema.find(path) }

      members.each do |member|
        # unless we're unsetting the preview, don't allow adding a new value
        if value && member.preview_toggled_by && member.preview_toggled_by != value
          raise ArgumentError, "Couldn't add `#{self.toggled_by}` to `#{member.path}`: it's already toggled with `#{member.preview_toggled_by}`" # rubocop:disable GitHub/UsePlatformErrors
        else
          member.preview_toggled_by = value
        end
      end
    end
  end
end
