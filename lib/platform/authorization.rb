# frozen_string_literal: true

module Platform
  module Authorization
    ApiChallenge = "github.challenge".freeze
    ApiForbid    = "github.forbid".freeze

    DEPRECATION_EMAIL_TTL = 1.month

    # Returns the authentication fingerprint from the environment, which is set
    # upstream by middleware.
    #
    # Returns a string.
    def auth_fingerprint
      Api::Middleware::RequestAuthenticationFingerprint.get(request.env).to_s
    end

    # Public: Ensure that the request is authenticated.
    #
    # Halts with a 401 if the request is not authenticated.
    # Returns nothing.
    def require_authentication!
      unless logged_in?
        deliver_error! 401, documentation_url: "/v3/#authentication"
      end
    end

    # Public: Checks to see if the current request is authenticated.  This will
    # check the HTTP Authorization header or the `?access_token` OAuth paramter
    # just once.
    #
    # Returns true if the user is logged in, or false.
    def logged_in?
      # We don't support anonymous requests with GraphQL
      # so skip the check entirely.
      unless graphql_request?
        # If an OAuth Application is using basic auth
        # return false even if the authentication was successful because
        # there isn't a `current_user`.
        return false if logged_in_as_oauth_application_via_basic_auth?
      end

      logged_in_as_user?
    end

    def logged_in_as_user?
      !!current_user
    end

    def logged_in_as_oauth_application_via_basic_auth?
      attempt_login_from_oauth_application
      @current_app_via_authorization_header.present?
    end

    # Public: Checks to see if the current request is truly anonymous or not
    def anonymous_request?
      !!(logged_in? || current_app.present?) ? false : true
    end

    # Public: Fetches the currently logged in user for this request.
    #
    # Returns either a User instance or nil.
    def current_user
      return @current_user if defined?(@current_user)

      attempt_login
      if @current_user
        actor_context = @current_user.event_context(prefix: :actor)
        GitHub.context.push(actor_context)
        Audit.context.push(actor_context)
      end

      Time.zone = timezone_for_request(@current_user)
      @current_user
    end

    def attempt_login
      @current_user = nil
      login_from_api_auth
    end

    def attempt_login_from_integration
      @current_user = nil
      login_from_integration_assertion
    end

    def attempt_login_from_oauth_application
      # Do not set current_user to be nil
      # until we have confirmed that it is
      # an OAuth App using basic auth.
      login_from_oauth_application_credentials
    end

    def attempt_remote_token_login(scope)
      if !logged_in?
        token = params[:token]
        if token && !token.to_s.dup.force_encoding("UTF-8").valid_encoding?
          deliver_error! 403, message: "Sorry, the provided token was unprocessable."
        end

        if token.present? && scope.present?
          @current_user = User.authenticate_with_signed_auth_token \
            token: token,
            scope: scope
          @remote_token_auth = @current_user.presence
        end
      end

      if GitHub.private_mode_enabled? && @current_user.nil?
        deliver_error! 403, message: "Must authenticate to access this API."
      end
    end

    # Public: Fetches the OauthApplication for the current request.
    #
    # API clients can pass ?client_id={key}&client_secret={secret} in an anonymous
    # (unauthenticated) request to receive a higer rate limit.
    #
    # May also return an `Integration` in `app/api/applications.rb`, see
    # https://github.com/github/github/blob/7830a583e23bbfa3ceb23251d4975639cbd018ca/app/api/applications.rb#L257-L268
    def current_app
      if @current_app.nil?
        @current_app = find_current_app || false
      end
      @current_app || nil
    end

    # Public: Fetches the Integration for the current request.
    def current_integration
      return @current_integration if defined?(@current_integration)

      @current_integration = if logged_in? && current_user.is_a?(Bot)
        current_user.integration
      elsif integration_user_request?
        @oauth.application
      else
        nil
      end
    end
    attr_writer :current_integration

    # Internal: is the request by a user logged in with Oauth,
    # via an Integration?
    #
    # Returns a boolean.
    def integration_user_request?
      logged_in? && @oauth && @oauth.integration_application_type?
    end

    # Internal: is the request by an Integration Bot?
    #
    # Returns a boolean.
    def integration_bot_request?
      current_integration && current_user.is_a?(Bot)
    end

    # Internal: is the request by a User, authenticated or not?
    #
    # Returns a boolean.
    def user_request?
      current_user.is_a?(User) || current_user.nil?
    end

    # Public: Fetches the integration installation for the current request.
    #
    # Returns a IntegrationInstallation, (SiteScoped|Scoped)IntegrationInstallation, or nil.
    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def current_integration_installation
      @current_integration_installation ||= case
      when integration_bot_request?
        current_user.installation
      when integration_user_request?
        if Apps::Internal.capable?(:per_repo_user_to_server_tokens, app: current_integration)
          @oauth.installation
        end
      end
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

    # Public: Fetches the parent integration installation for the current request.
    #
    # Returns an IntegrationInstallation or nil.
    # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
    def current_parent_integration_installation
      @current_parent_integration_installation ||= case current_integration_installation
      when ScopedIntegrationInstallation
        current_integration_installation.parent
      end
    end
    # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

    # Public: Fetches the actor for the current request.
    #
    # Returns either a IntegrationInstallation, Integration, User, or nil.
    def current_actor
      return current_integration_installation if current_integration_installation
      return current_user if current_user
      return current_app.owner if current_app
      return current_integration if current_integration
      nil # anonymous
    end

    def require_enterprise_installation!
      return if current_enterprise_installation
      if enterprise_installation_token
        deliver_error! 403, message: "Invalid Enterprise token."
      else
        deliver_error! 404
      end
    end

    # Public: Fetches the enterprise installation for the current request.
    #
    # Returns either a EnterpriseInstallation or nil.
    def current_enterprise_installation
      return nil if GitHub.enterprise?
      return @current_enterprise_installation if defined?(@current_enterprise_installation)

      @current_enterprise_installation = if current_integration
        EnterpriseInstallation.for_github_app(current_integration)
      else
        nil
      end
    end

    attr_writer :current_enterprise_installation

    def enterprise_installation_token
      request.env["HTTP_X_GITHUB_ENTERPRISE_TOKEN"]
    end

    # Public: The API should challenge anonymous requests with a 401 to
    # authenticate.  This is proper HTTP convention, but is disabled by default
    # so we don't leak private repositories.  Appropriate in other cases such as
    # "/user", or cases where a user has pull but not admin acccess and is trying
    # to access "/repos/:owner/:repo/hooks".
    #
    # Returns false.
    def challenge_api!
      env[ApiChallenge] = true
      false
    end

    # Internal: Indicates whether the API will respond with a 403 if authorization
    # fails. A 403 is proper HTTP convention for requests that fail authorization,
    # but we respond with 404 by default so that we don't leak private
    # repositories. A 403 is appopriate in other cases such as "/user", or cases
    # where a user has pull but not admin acccess and is trying to access
    # "/repos/:owner/:repo/hooks" (for example).
    #
    # Returns a String or nil. If the API will respond with a 403 for failed
    #   authorization, this method returns the message that will be included in
    #   the response. If the API will respond with a 404 for failed authorization,
    #   this method returns a falsely value.
    def forbidden_message
      env[ApiForbid]
    end

    # Public: Sets a 403 Forbidden message for requests that fail authorization.
    # This is proper HTTP convention, but is disabled by default so we don't leak
    # private repositories.  Appropriate in other cases such as "/user", or cases
    # where a user has pull but not admin acccess and is trying to access
    # "/repos/:owner/:repo/hooks".
    #
    # message   - String message to return in the API.
    # challenge - Boolean that tells the endpoint to challenge anonymous requests.
    #             Default: false.  See #challenge_api!
    # saml_error - Boolean that tells if the error came from a saml issue or not.
    #             Default: false
    #
    # Returns false.
    def set_forbidden_message(message, challenge = false, saml_error: false)
      if graphql_request?
        raise Platform::Errors::Forbidden.new(message, extensions: { saml_failure: saml_error }) if @raise_on_error
      else
        env[ApiForbid] = message
        challenge_api! if challenge
        false
      end
    end

    # For users who belong to SAML-protected organizations, we may sometimes
    # serve them partial results when they make API requests. A partial result
    # set would indicate the user has one or more organizations that they need
    # to whitelist their OAuth token to access.
    def set_sso_partial_results_header
      ids = protected_sso_organization_ids.join(",")
      response.headers["X-GitHub-SSO"] = "partial-results; organizations=#{ids}"
    end

  if !Rails.test?
    # Public: Checks the given block to see if the request is authorized.
    #
    # Yields nil in a block to check for authorization in a method.
    #
    # Halts with a 401 if the request is not authorized, there is not logged_in
    # user and the flag 'github.challenge' is set in the env Hash.
    #
    # Halts with a 403 if 'github.forbid' is set using the value as the message
    # returned to the user.
    #
    # Halts with a 404 if the request is not authorized.
    #
    # Returns nothing.
    def check_authorization(&block)
      return if auth_is_valid?(&block)

      if env[ApiChallenge] && !logged_in?
        deliver_error!(401)
      elsif message = forbidden_message
        deliver_error!(403, message: message)
      else
        deliver_error!(404)
      end
    end
  else
    # Public: Checks the given block to see if the request is authorized.  This
    # is a special method in test mode so that certain tests can check
    # authorization very quickly.
    #
    # Yields nil in a block to check for authorization in a method.
    #
    # Halts with a 304 if the request is authorized and a test is checking
    #   authorization.
    #
    # Halts with a 401 if the request is not authorized, there is not logged_in
    # user and the flag 'github.challenge' is set in the env Hash.
    #
    # Halts with a 403 if 'github.forbid' is set using the value as the message
    # returned to the user.
    #
    # Halts with a 404 if the request is not authorized.
    #
    # Returns nothing.
    def check_authorization(&block)
      return if graphql_request?
      if auth_is_valid?(&block)
        env["github.authcheck"] && deliver_error!(304, message: "Authorization successful in test. See #{__FILE__}:#{__LINE__} for explanation")
      elsif env[ApiChallenge] && !logged_in?
        deliver_error!(401)
      elsif message = forbidden_message
        deliver_error!(403, message: message)
      else
        deliver_error!(404)
      end
    end
  end

    # Public: Check this app's AccessControl class if the current user has
    # access to the given verb.  This basically wraps #access_allowed? (which
    # may have a unique implementation per app) inside a #check_authorization
    # block.
    #
    # *args - Array of arguments to pass to #access_allowed?.
    #
    # Returns nothing.
    def control_access(*args)
      check_authorization do
        science "readonly_access_allowed" do |e|
          e.use { access_allowed?(*args) }
          e.try do
            ActiveRecord::Base.connected_to(role: :reading) do
              access_allowed?(*args)
            end
          end
        end
      end
    end

    # Public: Scope checker shortcut for the protected resource.
    #
    # userish - An object that responds to #scopes.
    # scope   - The String scope to check against the scopes on userish.
    # options - The Hash of additional options for scope checking (optional).
    #           :target    - An object that responds to :method. Implies that your
    #                        userish object responds to the methods you've
    #                        defined on your whitelist.
    #           :method    - The method that returns the FixNum to be checked
    #                        against the whitelist. Defaults to :id.
    #           :whitelist - The Symbol name of a whitelist you've defined for
    #                        this scope.
    #
    # Returns truthy if this user has the specified scope.
    def scope?(userish, scope, options = nil)
      Api::AccessControl.scope?(userish, scope, options)
    end

    # Public: Set forbidden message unless the specified scope is given.
    #
    # scope   - String scope to check for.
    # options - Hash of options passed on to #scope? (optional).
    #           :message - String custom forbidden message.
    #
    # Returns nothing.
    def forbid_unless_scope(scope, options = nil)
      options ||= {}
      unless scope?(current_user, scope, options)
        set_forbidden_message(options[:message] || "#{scope} or greater scope required")
      end
    end

    # Public: Check if access to a resource is allowed.
    #
    # verb    - The verb Symbol specifying the action to take against a resource.
    # options - Options Hash
    #           :resource                 - Generally a Model instance like a User
    #                                       or Repository.
    #           :user                     - The User that is attempting to access
    #                                       the resource.
    #           :challenge                - Boolean flag. True if a 401 should be
    #                                       returned to the caller if !logged_in?.
    #           :forbid                   - Set a standard forbid message.
    #           :enforce_oauth_app_policy - Boolean indicating whether to enforce
    #                                       the organization's OAuth application
    #                                       policy, if applicable to this access
    #                                       check (optional) (default: true).
    #
    # Returns truthy if access is allowed.
    def access_allowed?(verb, options = {})
      enforce_oauth_app_policy =
        options.fetch(:enforce_oauth_app_policy, true)

      # Primarily for database filtering, we don't want to simply raise an error if a GitHub App
      # doesn't have access to a resource. In those cases, we'll just return with `false` and
      # let the resolver handle the next move.
      @raise_on_error = options.fetch(:raise_on_error, true)

      if current_repo_loaded? && current_repo&.advisory_workspace? && !verb_allowed_for_workspace_repos?(verb)
        return set_forbidden_message("This action is forbidden on workspace repositories.")
      end

      if integration_bot_request? || integration_user_request?
        authorizer = IntegrationAuthorizer.new(
          app: self,
          graphql_request: graphql_request?,
          repo_nwo_from_path: repo_nwo_from_path,
          current_resource_owner: current_resource_owner,
        )
      end

      allowed = if integration_bot_request? # server-to-server
        authorizer.integration_bot_request_allowed?(verb, options)
      elsif integration_user_request? # user-to-server
        authorizer.integration_user_request_allowed?(verb, options)
      elsif user_request?
        access_grant(verb, options).access_allowed?
      else
        # We should never end up here, but default to `false` just in case something unexpected occurs.
        false
      end

      # If we've been explicitly passed an organization, set @access_control_org
      # so we can check for possible SAML enforcement on it.
      @access_control_org = options[:organization] if options[:organization]

      return false unless allowed

      business = options[:resource] if options[:resource].is_a?(Business)
      org = current_resource_owner || @access_control_org
      owner = business || org
      actor = options[:user] || current_actor
      satisfied = ip_address_allowed?(owner, remote_ip, actor)

      unless satisfied
        owner_name_and_type = if owner.is_a?(Business)
          "`#{owner}` enterprise"
        else
          "`#{owner}` organization"
        end
        message = <<~MSG.squish
          Although you appear to have the correct authorization credentials,
          the #{owner_name_and_type} has an IP allow list enabled, and
          #{remote_ip} is not permitted to access this resource.
        MSG

        return set_forbidden_message(message)
      end

      if enforce_oauth_app_policy && !meets_oauth_application_policy?
        # This is irritating, but the REST API doesn't get to send a helpful message back. This is because
        # responses vary wildly between endpoints. `GET /orgs/:org/members`, for instance, responds
        # with a 200 and public members; but `GET /user/memberships/orgs/:org` responds with a 403
        #
        # The fix for this would be to go into individual app/api files and modify their behavior.
        return false unless graphql_request?

        org = current_resource_owner || @access_control_org

        message = <<~MSG.squish
        Although you appear to have the correct authorization credentials,
        the `#{org}` organization has enabled OAuth App access restrictions, meaning that data
        access to third-parties is limited. For more information on these restrictions, including
        how to whitelist this app, visit
        #{GitHub.help_url}/articles/restricting-access-to-your-organization-s-data/
        MSG

        return set_forbidden_message(message)
      end

      organization_credential_authorized?
    end

    # Allowed actions on a workspace repo are whitelisted to a small group of
    # required actions. cc https://github.com/github/pe-repos/issues/91
    #
    # New verbs should not be added to this list unless necessary for workspace workflows!
    # If you have questions, ask in #pe-repos or ping @github/pe-repos.
    ALLOWED_WORKSPACE_MUTATION_VERBS = [
      :create_commit_comment_reaction,
      :create_issue_related_reaction,
      :create_pull_request,
      :create_pull_request_comment,
      :create_pull_request_review_comment_reaction,
      :create_team_discussion_related_reaction,
      :delete_issue_comment,
      :delete_pull_request_review,
      :delete_pull_request_comment,
      :delete_reaction,
      :dismiss_pull_request_review,
      :edit_issue,
      :lock_issue,
      :mark_pull_request_ready_for_review,
      :minimize_repo_comment,
      :request_pull_request_review,
      :resolve_pull_request_review_thread,
      :submit_pull_request_review,
      :unlock_issue,
      :update_issue_comment,
      :update_pull_request,
      :update_pull_request_comment,
      :write_deployment,
      :write_deployment_status,
      :delete_deployment,
    ]

    def verb_allowed_for_workspace_repos?(verb)
      # All non mutation verbs are allowed
      return true unless @mutation

      ALLOWED_WORKSPACE_MUTATION_VERBS.include?(verb)
    end

    # This is extracted so that we can override it in GraphQL to use a cache,
    # that way we don't make these same DB calls over and over when we don't need to.
    def saml_enforced?(org, current_user)
      # this might load `Organization#business` to check SAML enforcement
      # there isn't a good way to make this async - ignore the association load
      Platform::LoaderTracker.ignore_association_loads do
        saml_enforcement_policy = Organization::SamlEnforcementPolicy.new(organization: org, user: current_user)
        saml_enforcement_policy.enforced?
      end
    end

    # This is extracted so that we can override it in GraphQL to use a cache,
    # that way we don't make these same DB calls over and over when we don't need to.
    def get_credential_authorization(org, oauth)
      Organization::CredentialAuthorization.by_organization_credential(
        organization: org,
        credential: oauth,
      ).first
    end

    def ip_address_allowed?(owner, ip, actor)
      # Platform requests from specific internal services to internal API
      # service hosts are exempt from enforcement.
      return true if ip_allow_list_exempt_internal_api_request?

      # Successful authentication using a signed auth token on internal endpoints
      # is exempt from IP allow list enforcement. See #attempt_remote_token_login.
      return true if @remote_token_auth && logged_in?

      # Individual API endpoints may opt out of IP allow list enforcement.
      return true unless require_allowed_ip?

      satisfied = IpWhitelistingPolicy.new(
        owner: owner,
        ip: ip,
        actor: actor,
      ).satisfied?

      unless satisfied
        # Set log fields to indicate IP allow list enforcement
        if self.respond_to?(:request) && request&.env[Rack::RequestLogger::APPLICATION_LOG_DATA]
          request.env[Rack::RequestLogger::APPLICATION_LOG_DATA].merge!({
            ip_allow_list_policy_unsatisfied: true,
            ip_allow_list_policy_owner_id: owner.id,
            ip_allow_list_policy_owner_type: owner.is_a?(::Business) ? :BUSINESS : :ORG,
            ip_allow_list_policy_actor: actor.to_s,
            ip_allow_list_policy_actor_type: actor.class.name,
            ip_allow_list_policy_actor_bot: actor.try(:bot?),
          })
        end
      end

      satisfied
    end

    def ip_allow_list_exempt_internal_api_request?
      return false unless GitHub.internal_api_role?

      # Check if actor is exempt from IP allow list enforcement on requests from
      # internal services to internal-api hosts.

      # Currently limited to group-syncer and actions requests.
      group_syncer_request? || actions_request?
    end

    def group_syncer_request?
      current_integration&.group_syncer_github_app?
    end

    def actions_request?
      current_integration&.launch_github_app? || current_integration&.launch_lab_github_app?
    end

    def actions_connect_request?
      actions_request? || connect_request?
    end

    def connect_request?
      current_integration&.connect_app?
    end

    def organization_credential_authorized?
      return true if @remote_token_auth && logged_in?
      return true if integration_bot_request?
      return true unless owner = current_resource_owner || @access_control_org
      return true unless owner.organization?
      return true unless saml_enforced?(owner, current_user)

      # If we fail, we'll start building out the X-GitHub-SSO header. It may
      # stay like this or it may contain a URL to authorize an OAuth token.
      sso_header = "required"
      message = "Resource protected by organization SAML enforcement."

      if @oauth
        return true unless @oauth.saml_enforceable?
        credential = get_credential_authorization(owner, @oauth)

        if credential
          # If we have a whitelisted OAuth token, we're good.
          return true if credential.active?

          # Otherwise, access has been revoked. Let the user know.
          message = "#{message} Your token's access was revoked. Please generate a new #{token_type_for(@oauth)} token and grant it access to this organization."
        else
          # An OAuth token is being used but has not ever been authorized, so
          # generate a new authorization request for the user in the SSO header.
          message = "#{message} You must grant your #{token_type_for(@oauth)} token access to this organization."
          token = Organization::CredentialAuthorization.generate_request(
            organization: owner,
            target: owner,
            credential: @oauth,
            actor: current_user,
          )

          url = Api::Serializer.html_url("/orgs/#{owner}/sso", authorization_request: token)
          sso_header = "#{sso_header}; url=#{url}"
        end
      else
        # No OAuth token was used.
        message = "#{message} Use a Personal Access Token (PAT) or OAuth Token that has been granted access to this organization."
      end

      @documentation_url = GitHub.sso_credential_authorization_help_url
      response.headers["X-GitHub-SSO"] = sso_header
      set_forbidden_message(message, saml_error: true)

      false
    end

    def protected_organization_ids
      return @protected_organization_ids if defined?(@protected_organization_ids)

      @protected_organization_ids = (
        protected_sso_organization_ids +
        protected_ip_whitelisting_organization_ids
      ).compact.uniq
    end

    def protected_sso_organization_ids
      return @protected_sso_organization_ids if defined?(@protected_sso_organization_ids)

      @protected_sso_organization_ids = Platform::Authorization::SAML.new(
        user: current_user,
        remote_token_auth: @remote_token_auth,
      ).protected_organization_ids
    end

    def protected_ip_whitelisting_organization_ids
      return @protected_ip_whitelisting_organization_ids if defined?(@protected_ip_whitelisting_organization_ids)

      @protected_ip_whitelisting_organization_ids = Platform::Authorization::IpWhitelisting.new(
        user: current_user,
        remote_token_auth: @remote_token_auth,
        ip: remote_ip,
      ).protected_organization_ids
    end

    # Public: Gets an AccessGrant for a resource.
    #
    # verb    - The verb Symbol specifying the action to take against a resource.
    # options - Options Hash
    #           :resource  - Generally a Model instance like a User or Repository.
    #           :user      - The User that is attempting to access the resource.
    #           :challenge - Boolean flag. True if a 401 should be returned to
    #                        the caller if !logged_in?.
    #           :forbid    - Set a standard forbid message.
    #           :forbid_message - A message to use if `forbid` is true and authorization fails
    #           ...
    #
    # Returns an Egress:AccessGrant object
    def access_grant(verb, options = nil)
      options ||= {}

      challenge_api! if options[:challenge]

      if options[:forbid]
        default_forbidden_message = "Must have admin rights to Repository."
        message = forbidden_message || options[:forbid_message] || default_forbidden_message
        set_forbidden_message(message)
      end

      options[:verb] = verb
      options[:user] ||= current_user
      options[:public_key] ||= @authenticated_key

      grant = Api::AccessControl.access_grant(options)
      @accepted_scopes ||= grant.accepted_scopes
      grant
    end

    # Public: Checks to see if the current User has the proper OAuth scope to
    # access the given Repository.
    #
    # repo - Repository instance.
    #
    # Returns true if the User has the right scopes, otherwise false.
    def oauth_allows_access?(repo)
      Api::AccessControl.oauth_allows_access?(current_user, repo)
    end

    private

    # Looks up the User for the current request using Basic Authentication.
    #
    # Halts with a 401 if the User is not authenticated.
    # Halts with a 403 if the User is not authenticated and over the auth limit.
    # Returns nothing.
    def login_from_api_auth
      return unless api_auth.credentials_present?
      result = api_auth.result

      # Is Authn via access token in a query parameter disabled?
      # https://github.com/github/ecosystem-apps/issues/404
      if query_param_auth_blocked?
        GitHub.dogstats.increment("api.authentication", tags: ["reason:access_token_via_params"])
        deliver_error!(400,
                       message: "Must specify access token via Authorization header",
                       documentation_url: "/v3/#oauth2-token-sent-in-a-header")
      end

      if result.success?
        @current_user = result.user
        @oauth = @current_user.oauth_access

        if @oauth && request_credentials.via_params?
          send_oauth_access_token_in_query_params_deprecation_email(@oauth)
        end

        if reject_for_password_auth_from_browser?
          reject_for_password_auth_from_browser!
        end

        @oauth
      elsif result.suspended_failure?
        reject_for_suspended_user!
      elsif result.suspended_oauth_application_failure?
        reject_for_suspended_oauth_application!
      elsif result.suspended_installation_failure?
        reject_for_suspended_installation!(result.message)
      elsif result.two_factor_partial_sign_in?
        ensure_user_has_otp
        ensure_two_factor_request_instrumented
        response.headers["X-GitHub-OTP"] = "required; #{result.two_factor_type}"
        deliver_error! 401,
          message: "Must specify two-factor authentication OTP code.",
          documentation_url: "/v3/auth#working-with-two-factor-authentication"
      elsif result.message
        deliver_error(400,
          message: result.message,
          documentation_url: "/v3/auth#basic-authentication")
      else
        reject_failed_login!
      end
    rescue GitHub::SMS::Error => e
      message =
        "We tried sending an SMS to your configured number, but #{e.message}." +
        " Please contact support at if you continue to have problems."
      deliver_error!(500, message: message)
    end

    def send_oauth_access_token_in_query_params_deprecation_email(oauth_access)
      return if GitHub.enterprise?

      # If the token is a personal access token
      # send an email for each token used.
      #
      # If the token belongs to an Integration or OauthApplication
      # use the id of the app so that we're not sending
      # and email for each token they make an API call with.

      actor      = oauth_access.personal_access_token? ? oauth_access : oauth_access.application
      actor_type = actor.class.to_s.underscore

      # Prevent sending emails for apps we're directly reaching out to
      if actor.is_a?(OauthApplication) || actor.is_a?(Integration)
        return if GitHub.flipper[:opt_out_of_oauth_access_token_in_query_params].enabled?(actor)
      end

      options = {
        interval:   DEPRECATION_EMAIL_TTL,
        actor_type: actor_type,
        actor_id:   actor.id,
      }

      # Add the User-Agent as an additional key if one is provided.
      if oauth_access.personal_access_token? && !user_agent.to_s.blank?
        options[:user_agent] = Digest::SHA256.base64digest(user_agent.to_s)
      end

      return unless GitHub::ActionRestraint.perform?("access_token_via_query_params", **options)

      flipper_actor = oauth_access.personal_access_token? ? oauth_access.user : actor
      return unless GitHub.flipper[:oauth_access_token_in_query_params].enabled?(flipper_actor)

      if oauth_access.personal_access_token? || oauth_access.application.owner.user?
        AccountMailer.api_oauth_access_via_query_params_deprecation(oauth_access, time: Time.zone.now, url: @current_url, user_agent: user_agent.to_s).deliver_later
      else
        OrganizationMailer.api_oauth_access_via_query_params_deprecation(oauth_access, time: Time.zone.now, url: @current_url, user_agent: user_agent.to_s).deliver_later
      end
    end

    ORIGINS_ALLOWED_TO_XHR_TO_API_WITH_PASSWORD = %w()

    RFC_1918_V4_SPACE = %w(
      10.0.0.0/8
      172.16.0.0/12
      192.168.0.0/16
      127.0.0.0/8
    ).map do |cidr|
      IPAddr.new(cidr)
    end

    def reject_for_password_auth_from_browser?
      # limit to users, not bots/integrations
      return false unless current_user.user?

      # limit blocking to passwords only
      return false unless api_auth.password_attempt?

      # gradual rollout
      return false unless GitHub.flipper[:block_password_auth_to_api_from_browsers].enabled?(current_user)

      # allow an opt out
      return false if GitHub.flipper[:block_password_auth_to_api_from_browsers_opt_out].enabled?(current_user)

      # require an origin
      stripped_origin = env["HTTP_ORIGIN"].to_s.strip
      return false if stripped_origin.blank?

      parsed_origin = begin
        Addressable::URI.parse(stripped_origin)
      rescue Addressable::URI::InvalidURIError
        # nbd, could be garbage, but not a browser
        return false
      end

      # we'll probably have to allow some origins...
      return false if ORIGINS_ALLOWED_TO_XHR_TO_API_WITH_PASSWORD.include?(stripped_origin)

      # only apply to http/https, not e.g. chrome-extension://
      return false unless %w(http https).include?(parsed_origin.scheme)

      host = parsed_origin.host

      # we call our own API sometimes
      subdomain = host.ends_with?(".github.com")
      github_host = host == "github.com"
      return false if subdomain || github_host

      # allow funky things running on ips
      if IPAddress.valid?(host) && RFC_1918_V4_SPACE.find { |range| range.include?(host) }
        return false
      end

      # Something microsofty is going on here :shrug:
      return false if host.ends_with?(".dynamics.com")

      # allow self
      return false if host == GitHub.host_name

      true
    end

    def reject_for_password_auth_from_browser!
      GitHub.dogstats.increment("authentication.browser_password")
      GitHub::Logger.log(
        browser_api_user: current_user.login,
        origin: env["HTTP_ORIGIN"].to_s,
        referer: env["HTTP_REFERER"].to_s,
        event_name: "rejected_api_auth_from_browser",
        request_id: env["HTTP_X_GITHUB_REQUEST_ID"],
      )
      # return the specific error code and message to tarpit activity
      deliver_error! 401,
        message: "Bad Credentials",
        documentation_url: "#{GitHub.developer_help_url}/rest"
    end

    # Internal: Attempts to authenticate the requesting
    # integration using a signed assertion token.
    #
    # Sets the current_user to the integration's bot if successful.
    # Otherwise responds with a 401 describing why the assertion
    # was invalid.
    def login_from_integration_assertion
      assertion = Api::IntegrationAssertion.new(env)

      if assertion.valid?
        self.current_integration = assertion.integration
      else
        response_code = assertion.error == :not_found ? 404 : 401
        deliver_error! response_code, message: assertion.error_message
      end
    end

    def login_from_oauth_application_credentials
      return @current_app_via_authorization_header if defined?(@current_app_via_authorization_header)

      @current_app_via_authorization_header = nil

      return unless request_credentials.login_password_present?

      if (app = OauthApplication.find_by(key: request_credentials.login))
        @current_user = nil

        if app.plaintext_secret != request_credentials.password
          reject_for_bad_credentials!
        elsif app.suspended?
          reject_for_suspended_oauth_application!
        else
          @current_app = @current_app_via_authorization_header = app
        end
      end
    end

    # Idempotent method of sending the user a 2FA OTP SMS. login_from_api_auth
    # can be called multiple times and we don't want to send multiple SMS.
    def ensure_user_has_otp
      return if defined? @sent_otp
      user = api_auth.attempted_user
      return unless user.two_factor_credential && user.two_factor_credential.sms_enabled?
      return unless request_sends_otp_sms?
      user.two_factor_credential.send_otp_sms
      @sent_otp = true
    end

    # Determine whether the request should result in a two-factor OTP SMS being sent.
    def request_sends_otp_sms?
      route_sends_otp_sms? && otp_header_unset?
    end

    # Determines whether the current route supports delivery of a one-time password
    # via SMS.
    #
    # This method should be overridden in classes that require SMS to be sent.
    #
    # Returns false.
    def route_sends_otp_sms?
      false
    end

    def otp_header_unset?
      !request.env.key?("HTTP_X_GITHUB_OTP")
    end

    # Internal: Finds the OauthApplication (if any) associated with the current
    # request.
    #
    # Returns an OauthApplication or nil.
    def find_current_app
      current_app_via_oauth || current_app_via_query_params
    end

    # Internal: Finds the OauthApplication associated with the OauthAccess (if
    # any) used to authenticated the current request.
    #
    # Returns an OauthApplication or nil.
    def current_app_via_oauth
      if @oauth && @oauth.oauth_application_type?
        @oauth.application
      end
    end

    # Internal: Finds the OauthApplication (if any) that matches the client ID
    # and client secret specified in the query parameters (if present).
    #
    # Returns an OauthApplication or nil.
    def current_app_via_query_params
      return @current_app_via_query_params if defined?(@current_app_via_query_params)

      @current_app_via_query_params = nil

      client_id     = params[:client_id].to_s
      client_secret = params[:client_secret].to_s

      return @current_app_via_query_params if client_id.blank? || client_secret.blank?
      return @current_app_via_query_params if GitHub.flipper[:ncc_remediation_brown_out].enabled? && params[:oauth_credential_ratelimit_increase].to_s != "true"

      @current_app_via_query_params = OauthApplication.where(key: client_id).find do |application|
        application.plaintext_secret == client_secret
      end
      instrument_app_found_via_query_params(@current_app_via_query_params)

      @current_app_via_query_params
    end

    # Internal: Finds the OauthApplication (if any) that matches the client ID
    # and client secret passed via an authorization header.
    #
    # Returns an OauthApplication or nil.
    def current_app_via_authorization_header
      OauthApplication.where(key: request_credentials.login).find do |application|
        application.plaintext_secret == request_credentials.password
      end
    end

    # Internal: Record stats reflecting the results of looking up the
    # OauthApplication from the query parameters.
    #
    # app - The OauthApplication found based on the query parameters, or nil if
    #       no OauthApplication matches the client ID and secret in the query
    #       parameters.
    #
    # Returns nothing.
    def instrument_app_found_via_query_params(app)
      app_lookup_result = if app.nil?
        "invalid"
      elsif app.suspended?
        "suspended"
      else
        "accepted"
      end

      GitHub.dogstats.increment "api.app_credentials_via_query_params", tags: ["result:#{app_lookup_result}"]
      send_oauth_credentials_in_query_params_deprecation_email(app)
    end

    def send_oauth_credentials_in_query_params_deprecation_email(app)
      return if GitHub.enterprise?
      return if app.nil?
      # Prevent sending emails for apps we're directly reaching out to
      return if GitHub.flipper[:opt_out_of_oauth_credentials_via_query_params].enabled?(app)

      options = {
        interval:   DEPRECATION_EMAIL_TTL,
        actor_type: "oauth_application",
        actor_id:   app.id,
      }

      return unless GitHub::ActionRestraint.perform?("oauth_credentials_via_query_params", **options)
      return unless GitHub.flipper[:oauth_credentials_via_query_params].enabled?(app)

      if app.owner.organization?
        OrganizationMailer.api_oauth_credentials_via_params_deprecation(app, time: Time.zone.now, url: @current_url, user_agent: user_agent.to_s).deliver_later
      else
        AccountMailer.api_oauth_credentials_via_params_deprecation(app, time: Time.zone.now, url: @current_url, user_agent: user_agent.to_s).deliver_later
      end
    end

    # Deliver appropriate response for the login failure.
    #
    # Renders 403 if the rate limit has been exceeded, 401 otherwise.
    def reject_failed_login!
      if api_auth.result.at_auth_limit_failure?
        reject_for_exceeding_auth_limit!
      elsif api_auth.result.weak_password_failure?
        reject_for_weak_password!
      elsif api_auth.result.api_with_password_failure?
        reject_for_api_with_password!
      else
        reject_for_bad_credentials!
      end
    end

    def reject_for_exceeding_auth_limit!
      GitHub.dogstats.increment("api.authentication", tags: ["reason:locked-out"])
      deliver_error! 403, message: "Maximum number of login attempts exceeded. Please try again later."
    end

    def reject_for_weak_password!
      GitHub.dogstats.increment("api.authentication", tags: ["reason:weak-password"])
      password_update_url = GitHub::Application.routes.url_helpers.settings_user_security_url(host: GitHub.host_name)
      deliver_error! 401, message: "Weak credentials. Update your password: #{password_update_url}", documentation_url: "#{GitHub.help_url}/articles/creating-a-strong-password"
    end

    def reject_for_api_with_password!
      GitHub.dogstats.increment("api.authentication", tags: ["reason:api-password-auth"])
      create_pat_url = GitHub::Application.routes.url_helpers.settings_user_tokens_url(host: GitHub.host_name)
      deliver_error! 401, message: "Bad credentials. The API can't be accessed using username/password authentication. Please create a personal access token to access this endpoint: #{create_pat_url}", documentation_url: "#{GitHub.help_url}/articles/creating-a-personal-access-token-for-the-command-line"
    end

    def reject_for_bad_credentials!
      GitHub.dogstats.increment("api.authentication", tags: ["reason:invalid"])
      deliver_error! 401, message: "Bad credentials"
    end

    def reject_for_suspended_user!
      GitHub.dogstats.increment("api.authentication", tags: ["reason:suspended"])
      deliver_error! 403, message: "Sorry. Your account was suspended."
    end

    def reject_for_suspended_oauth_application!
      GitHub.dogstats.increment("api.authentication", tags: ["reason:suspended-application"])
      deliver_error! 403, message: "Sorry. Your application was suspended. Please contact #{GitHub.support_link_text}"
    end

    def reject_for_suspended_installation!(message)
      GitHub.dogstats.increment("api.authentication", tags: ["reason:suspended-installation"])
      deliver_error! 403, message: message
    end

    def require_api_version(version)
      deliver_error! 404 unless medias.version?(version)
    end

    def api_auth
      return @api_auth if defined? @api_auth
      @api_auth = GitHub::Authentication::Attempt.new(
        allow_integrations:                 true,
        allow_user_via_integration:         true,
        from:                               :api,
        login:                              request_credentials.login,
        password:                           request_credentials.password,
        otp:                                request_credentials.otp,
        token:                              request_credentials.token,
        ip:                                 remote_ip,
        user_agent:                         user_agent,
        request_id:                         env["HTTP_X_GITHUB_REQUEST_ID"],
        api_password_auth_deprecated:       password_auth_deprecated?,
        password_auth_blocked:              password_auth_blocked?,
        url:                                request.url,
      )
    end

    def request_credentials
      @request_credentials ||= Api::RequestCredentials.from_env(env)
    end

    # Idempotent method of instrumenting that we requested a 2FA code.
    # login_from_api_auth can be called multiple times and we don't want to
    # instrument this twice.
    def ensure_two_factor_request_instrumented
      return if defined? @two_factor_request_instrumented
      user = api_auth.attempted_user
      user.instrument_two_factor_requested note: "From GitHub API", two_factor_type: "otp"
      @two_factor_request_instrumented = true
    end

    # Checks the block for authorization.
    # Yields nil in a block to check for authorization in a method.
    def auth_is_valid?(&block)
      !block || block.call
    end

    # Private: Determines whether the request satisfies the OAuth application
    # policy associated with the resources being accessed by the request.
    #
    # Returns true if the request satisfies the OAuth application policy or if no
    #   OAuth application policy applies to this request. Otherwise, returns
    #   false.
    def meets_oauth_application_policy?
      meets_oauth_application_policy_for_this_org? &&
        meets_oauth_application_policy_for_this_repo?
    end

    # Private: If the request involves a specific organization's resources,
    # determine whether the request satisfies the organization's OAuth application
    # policy.
    #
    # Returns false when *all* of the following conditions exist:
    #   - the request involves a single organization's resources, and
    #   - the request is from an OAuth application, and
    #   - the application violates the organization's OAuth application policy
    #
    #   Otherwise, returns true.
    def meets_oauth_application_policy_for_this_org?
      return true unless requestor_governed_by_oauth_application_policy?

      org = find_org
      return true unless org

      org.allows_oauth_application?(current_app_via_oauth)
    end

    # Private: If the request involves a specific repository, determine whether
    # the request satisfies the repository's OAuth application policy.
    #
    # Returns false when *all* of the following conditions exist:
    #   - the request involves a single repository, and
    #   - the request is from an OAuth application, and
    #   - the application violates the repository's OAuth application policy, and
    #   - the request is performing a non-read-only operation related to a public
    #     repository, or any kind of operation related to a private repository
    #
    #   Otherwise, returns true.
    def meets_oauth_application_policy_for_this_repo?
      req = graphql_request? ? nil : request
      OauthApplicationPolicy::HttpRequest.new(
        repository: find_repo,
        user: current_user,
        request: req,
      ).satisfied?
    end

    # Private: Determine whether OAuth application policies apply to the request.
    #
    # Returns a Boolean.
    def requestor_governed_by_oauth_application_policy?
      GitHub.oauth_application_policies_enabled? && current_app_via_oauth
    end

    def graphql_request?
      defined?(@graphql_request) && @graphql_request
    end

    # Is Authn via access token in a query parameter disabled?
    # https://github.com/github/ecosystem-apps/issues/404
    def query_param_auth_blocked?
      request_credentials.via_params? && GitHub.flipper[:access_token_via_params_disabled].enabled?
    end

    def token_type_for(oauth_access)
      oauth_access.personal_access_token? ? "Personal Access" : "OAuth"
    end
  end
end
