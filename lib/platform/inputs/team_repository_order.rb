# frozen_string_literal: true

module Platform
  module Inputs
    class TeamRepositoryOrder < Platform::Inputs::Base
      description "Ordering options for team repository connections"

      argument :field, Enums::TeamRepositoryOrderField, "The field to order repositories by.", required: true
      argument :direction, Enums::OrderDirection, "The ordering direction.", required: true
    end
  end
end
