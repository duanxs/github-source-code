# frozen_string_literal: true

module Platform
  module Inputs
    class FeatureOrder < Platform::Inputs::Base
      description "Ways in which lists of features can be ordered upon return."
      areas_of_responsibility :"feature-flags"
      visibility :internal

      argument :field, Enums::FeatureOrderField, "The field in which to order features by.", required: true
      argument :direction, Enums::OrderDirection, "The direction in which to order features by the specified field.", required: true
    end
  end
end
