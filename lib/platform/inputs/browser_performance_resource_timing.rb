# frozen_string_literal: true

module Platform
  module Inputs
    class BrowserPerformanceResourceTiming < Platform::Inputs::Base
      description <<~MD
        Models the browser's `PerformanceResourceTiming` performance timeline entry.

        Sample entries can be retrieved via `performance.getEntriesByType('resource')`.

        See the W3C Resource Timing Level specification for more information.

        https://www.w3.org/TR/resource-timing-2/#h-performanceresourcetiming
      MD

      visibility :internal

      argument :name, Scalars::URI, <<~MD, required: true
        This attribute must return the resolved URL of the requested resource.

        https://www.w3.org/TR/resource-timing-2/#widl-PerformanceResourceTiming-name
      MD

      argument :entry_type, String, <<~MD, required: true
        https://www.w3.org/TR/resource-timing-2/#widl-PerformanceResourceTiming-entryType
      MD

      argument :start_time, Float, <<~MD, required: true
        https://www.w3.org/TR/resource-timing-2/#widl-PerformanceResourceTiming-startTime
      MD

      argument :duration, Float, <<~MD, required: true
        https://www.w3.org/TR/resource-timing-2/#widl-PerformanceResourceTiming-duration
      MD

      argument :initiator_type, String, <<~MD, required: false
        https://www.w3.org/TR/resource-timing-2/#dom-performanceresourcetiming-initiatortype
      MD

      argument :next_hop_protocol, String, <<~MD, required: false
        https://www.w3.org/TR/resource-timing-2/#dom-performanceresourcetiming-nexthopprotocol
      MD

      argument :worker_start, Float, <<~MD, required: false
        https://www.w3.org/TR/resource-timing-2/#dom-performanceresourcetiming-workerstart
      MD

      argument :redirect_start, Float, <<~MD, required: false
        https://www.w3.org/TR/resource-timing-2/#dom-performanceresourcetiming-redirectstart
      MD

      argument :redirect_end, Float, <<~MD, required: false
        https://www.w3.org/TR/resource-timing-2/#dom-performanceresourcetiming-redirectend
      MD

      argument :fetch_start, Float, <<~MD, required: false
        https://www.w3.org/TR/resource-timing-2/#dom-performanceresourcetiming-fetchstart
      MD

      argument :domain_lookup_start, Float, <<~MD, required: false
        https://www.w3.org/TR/resource-timing-2/#dom-performanceresourcetiming-domainlookupstart
      MD

      argument :domain_lookup_end, Float, <<~MD, required: false
        https://www.w3.org/TR/resource-timing-2/#dom-performanceresourcetiming-domainlookupend
      MD

      argument :connect_start, Float, <<~MD, required: false
        https://www.w3.org/TR/resource-timing-2/#dom-performanceresourcetiming-connectstart
      MD

      argument :connect_end, Float, <<~MD, required: false
        https://www.w3.org/TR/resource-timing-2/#dom-performanceresourcetiming-connectend
      MD

      argument :secure_connection_start, Float, <<~MD, required: false
        https://www.w3.org/TR/resource-timing-2/#dom-performanceresourcetiming-secureconnectionstart
      MD

      argument :request_start, Float, <<~MD, required: false
        https://www.w3.org/TR/resource-timing-2/#dom-performanceresourcetiming-requeststart
      MD

      argument :response_start, Float, <<~MD, required: false
        https://www.w3.org/TR/resource-timing-2/#dom-performanceresourcetiming-responsestart
      MD

      argument :response_end, Float, <<~MD, required: false
        https://www.w3.org/TR/resource-timing-2/#dom-performanceresourcetiming-responseend
      MD

      argument :transfer_size, Integer, <<~MD, required: false
        https://www.w3.org/TR/resource-timing-2/#dom-performanceresourcetiming-transfersize
      MD

      argument :encoded_body_size, Integer, <<~MD, required: false
        https://www.w3.org/TR/resource-timing-2/#dom-performanceresourcetiming-encodedbodysize
      MD

      argument :decoded_body_size, Integer, <<~MD, required: false
        https://www.w3.org/TR/resource-timing-2/#dom-performanceresourcetiming-decodedbodysize
      MD
    end
  end
end
