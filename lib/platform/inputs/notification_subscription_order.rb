# frozen_string_literal: true

module Platform
  module Inputs
    class NotificationSubscriptionOrder < Platform::Inputs::Base
      areas_of_responsibility :notifications
      visibility :under_development
      description "Ways in which notification subscription connections can be ordered."

      argument :field, Enums::NotificationSubscriptionOrderField, "The field in which to order nodes by.", required: true
      argument :direction, Enums::OrderDirection, "The direction in which to order nodes.", required: true
    end
  end
end
