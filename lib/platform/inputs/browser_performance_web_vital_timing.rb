# frozen_string_literal: true

module Platform
  module Inputs
    class BrowserPerformanceWebVitalTiming < Platform::Inputs::Base
      description "https://web.dev/vitals/"
      visibility :internal
      argument :name, Scalars::URI, "The address of the current document.", required: true
      argument :cls, Float, "Cumulative Layout Shift", required: false
      argument :fcp, Float, "First Contentful Paint", required: false
      argument :fid, Float, "First Input Delay", required: false
      argument :lcp, Float, "Largest Contentful Paint", required: false
      argument :ttfb, Float, "Time to First Byte", required: false
    end
  end
end
