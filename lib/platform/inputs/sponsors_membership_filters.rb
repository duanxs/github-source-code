# frozen_string_literal: true

module Platform
  module Inputs
    class SponsorsMembershipFilters < Platform::Inputs::Base
      description "Filtering options for Sponsors memberships connections."
      visibility :internal

      argument :states, [Enums::SponsorsMembershipState], description: "The states to filter memberships by.", required: false
      argument :spammy, Boolean, "Whether the sponsorable has been marked as spammy.", required: false
      argument :suspended, Boolean, "Whether the sponsorable has been suspended.", required: false
      argument :type, [Enums::SponsorableType], description: "The sponsorable type to filter memberships by.", required: false
      argument :ignored, Boolean, "Whether the membership has been ignored in stafftools.", required: false
      argument :fiscal_host, [Enums::SponsorsMembershipFiscalHost], "The fiscal host to filter memberships by.", required: false
      argument :billing_country, [Enums::SponsorsMembershipBillingCountry], "The billing country to filter memberships by.", required: false
    end
  end
end
