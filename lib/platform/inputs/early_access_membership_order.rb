# frozen_string_literal: true

module Platform
  module Inputs
    class EarlyAccessMembershipOrder < Platform::Inputs::Base
      areas_of_responsibility :stafftools
      description "Ordering options for early access membership connections."
      visibility :internal

      argument :field, Enums::EarlyAccessMembershipOrderField, "The field to order memberships by.", required: true
      argument :direction, Enums::OrderDirection, "The ordering direction.", required: true
    end
  end
end
