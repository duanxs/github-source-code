# frozen_string_literal: true

module Platform
  module Loaders
    class VerifiedOrganizationDomains < Platform::Loader
      def self.load(organization_id)
        self.for.load(organization_id)
      end

      def fetch(organization_ids)
        results = ::OrganizationDomain.where(organization_id: organization_ids, verified: true)
                    .group_by(&:organization_id)

        organization_ids.each_with_object(results) do |organization_id, result|
          result[organization_id] ||= ::OrganizationDomain.none
        end
      end
    end
  end
end
