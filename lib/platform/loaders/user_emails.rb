# frozen_string_literal: true

module Platform
  module Loaders
    class UserEmails < Platform::Loader
      def self.load(user_id)
        self.for.load(user_id)
      end

      def self.load_all(user_ids)
        Promise.all(
          user_ids.map { |id| load(id) },
        )
      end

      def fetch(user_ids)
        results = UserEmail.github_sql.results(<<-SQL, user_ids: user_ids)
          SELECT user_id, email FROM user_emails WHERE user_id IN :user_ids
        SQL

        emails_by_user_id = {}

        user_ids.each do |user_id|
          emails_by_user_id[user_id] = []
        end

        results.each do |user_id, email|
          emails_by_user_id[user_id] << email
        end

        results = User.github_sql.results(<<-SQL, ids: user_ids)
          SELECT id, login FROM users WHERE id IN :ids
        SQL

        # old style stealth emails can still be used even after being overwritten
        results.each do |id, login|
          emails_by_user_id[id] << "#{login}@#{GitHub.stealth_email_host_name}"
        end

        emails_by_user_id
      end
    end
  end
end
