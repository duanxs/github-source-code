# frozen_string_literal: true

module Platform
  module Loaders
    class SponsorshipsAsSponsorable < Platform::Loader
      DEFAULT_ORDER_FIELD = "created_at"
      DEFAULT_ORDER_DIRECTION = "DESC"

      def self.load(sponsorable_id, viewer: nil, include_private: false, order_by: nil, tier: nil)
        self.for(viewer, include_private, order_by, tier).load(sponsorable_id)
      end

      def initialize(viewer, include_private, order_by, tier)
        @include_private = include_private
        @viewer = viewer
        @order_by = order_by
        @tier = tier
      end

      def fetch(sponsorable_ids)
        active_sponsorships = ::Sponsorship.active.where(sponsorable_id: sponsorable_ids)

        if tier.present?
          active_sponsorships = active_sponsorships.where(subscribable: tier)
        end

        return [] if active_sponsorships.empty?

        public_sponsorships = active_sponsorships.privacy_public

        if !order_by.nil?
          public_sponsorships = public_sponsorships
                                  .order("sponsorships.#{order_by[:field]} #{order_by[:direction]}")
        elsif viewer && !public_sponsorships.empty?
          public_sponsorships = public_sponsorships.ranked_by_sponsor(for_user: viewer)
        else
          public_sponsorships = public_sponsorships
                                  .order("sponsorships.#{DEFAULT_ORDER_FIELD} #{DEFAULT_ORDER_DIRECTION}")
        end

        # Append private sponsorships
        sponsorships = if include_private
          public_sponsorships + active_sponsorships.privacy_private
        else
          public_sponsorships
        end

        sponsorships.group_by(&:sponsorable_id)
      end

      private

      attr_reader :viewer, :order_by, :include_private, :tier
    end
  end
end
