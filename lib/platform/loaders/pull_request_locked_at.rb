# frozen_string_literal: true

module Platform
  module Loaders
    class PullRequestLockedAt < Platform::Loader
      def self.load(pull_request_id)
        self.for.load(pull_request_id)
      end

      def fetch(pull_request_ids)
        pull_request_id_to_last_locked_event_id = ::PullRequest.
          joins(issue: [:conversation, :events]).
          where(id: pull_request_ids, conversations: { state: "locked" }, issue_events: { event: "locked" }).
          group(:id).maximum(:"issue_events.id")

        last_locked_event_id_to_locked_at = ::IssueEvent.where(id: pull_request_id_to_last_locked_event_id.values).pluck(:id, :created_at).to_h

        pull_request_id_to_last_locked_event_id.map { |pull_request_id, last_locked_event_id|
          [pull_request_id, last_locked_event_id_to_locked_at[last_locked_event_id]]
        }.to_h
      end
    end
  end
end
