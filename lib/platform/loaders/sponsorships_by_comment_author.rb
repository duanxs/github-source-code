# frozen_string_literal: true

module Platform
  module Loaders
    class SponsorshipsByCommentAuthor < Platform::Loader
      def self.load(viewer, sponsorable, author_id)
        self.for(viewer, sponsorable).load(author_id)
      end

      def initialize(viewer, sponsorable)
        @viewer = viewer
        @sponsorable = sponsorable
      end

      def fetch(author_ids)
        return Hash.new unless @viewer
        return Hash.new unless @sponsorable

        # only the sponsorable user (org admins for sponsorable orgs) can see
        viewer_is_sponsorable = if @sponsorable.organization?
          @sponsorable.adminable_by?(@viewer)
        else
          @sponsorable.id == @viewer.id
        end

        return Hash.new unless viewer_is_sponsorable

        Sponsorship.where(
          sponsorable: @sponsorable,
          active: true,
          sponsor_id: author_ids
        ).index_by(&:sponsor_id)
      end
    end
  end
end
