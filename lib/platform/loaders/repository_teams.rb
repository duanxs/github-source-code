# frozen_string_literal: true

module Platform
  module Loaders
    class RepositoryTeams < Platform::Loader
      def self.load(repository, immediate_only:)
        return ::Promise.resolve([]) unless repository.present?

        promise = self.for.load(repository.id)

        return promise if immediate_only

        promise.then do |team_ids|
          # load the Team records because we need the `tree_path` for descendants
          Loaders::ActiveRecord.load_all(::Team, team_ids).then do |teams|
            teams.compact! # in case Teams cannot be found by ID
            next [] if teams.empty?

            Loaders::TeamDescendants.load_all(teams, immediate_only: false).then do |descendant_ids_by_parent_id|
              descendant_ids_by_parent_id.to_a.flatten.compact.uniq.sort
            end
          end
        end
      end

      # Internal: fetch the Team IDs for the list of Repository IDs.
      #
      # Returns a Hash{repository_id Integer => Array[team_id Integer...]}.
      def fetch(repo_ids)
        results = {}

        abilities = ::Ability.where(
          actor_type: "Team",
          subject_type: "Repository",
          subject_id: repo_ids,
          priority: ::Ability.priorities[:direct],
        )

        repo_ids.each do |repo_id|
          results[repo_id] = select_team_ids(repo_id, abilities)
        end

        results
      end

      private

      def select_team_ids(repo_id, abilities)
        team_ids = []

        abilities.each do |ability|
          team_ids << ability.actor_id if ability.subject_id == repo_id
        end

        team_ids
      end
    end
  end
end
