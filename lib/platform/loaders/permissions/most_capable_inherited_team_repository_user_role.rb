# frozen_string_literal: true

module Platform
  module Loaders
    module Permissions
      class MostCapableInheritedTeamRepositoryUserRole < Platform::Loader
        def self.load(team:, repo:)
          self.for(team).load(repo.id)
        end

        def initialize(team)
          @team = team
        end

        def fetch(repo_ids)
          user_roles = UserRole.where(
            actor_id: @team.ancestor_ids,
            actor_type: "Team",
            target_id: repo_ids,
            target_type: "Repository",
          )

          select_most_capable_role(user_roles)
        end

        private

        def select_most_capable_role(user_roles)
          promises = user_roles.map(&:async_role)
          results = Promise.all(promises).sync
          roles = results.index_by(&:id)

          user_roles.each_with_object({}) do |user_role, result|
            target_id = user_role.target_id
            role = roles[user_role.role_id]
            recorded_role = roles[result[target_id]&.role_id]

            if !result.key?(target_id) || role.action_rank > recorded_role.action_rank
              result[target_id] = user_role
            end
          end
        end
      end
    end
  end
end
