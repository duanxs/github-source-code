# frozen_string_literal: true

module Platform
  module Loaders
    class ReactionGroups < Platform::Loader
      def self.load(subject)
        self.for(subject.class.name).load(subject)
      end

      def initialize(subject_type)
        @subject_type = subject_type
      end

      def fetch(subjects)
        reactions = build_reactions_query(subjects: subjects).models(::Reaction)

        grouped_reactions = reactions.group_by { |reaction|
          [reaction.subject_id, reaction.content]
        }

        subjects.map { |subject|
          reaction_groups = ::Emotion.all.map { |emotion|
            ::ReactionGroup.new(
              subject: subject,
              emotion: emotion,
              reactions: grouped_reactions[[subject.id, emotion.content]] || [],
            )
          }

          [subject, reaction_groups]
        }.to_h
      end

      def build_reactions_query(subjects:)
        reactions_sql = Reaction.github_sql.new(<<~SQL, subject_type: @subject_type, subject_ids: subjects.map(&:id))
          SELECT id, subject_type, subject_id, content, user_id, created_at
          FROM reactions IGNORE INDEX (index_reactions_on_subject_content_created_at)
          WHERE subject_type = :subject_type AND subject_id IN :subject_ids
        SQL

        if GitHub.spamminess_check_enabled?
          reactions_sql.add <<~SQL
            AND user_hidden = false
          SQL
        end

        reactions_sql.add <<~SQL
          ORDER BY subject_id, subject_type, user_hidden, created_at, id
        SQL

        reactions_sql
      end
    end
  end
end
