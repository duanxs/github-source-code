# frozen_string_literal: true

module Platform
  module Loaders
    class BotByInstallation < Platform::Loader
      def self.load(installation_id)
        self.for.load(installation_id)
      end

      def fetch(installation_ids)
        results = ::IntegrationInstallation
          .includes(integration: [:bot])
          .where(id: installation_ids).index_by(&:id)

        bots_by_installation_id = {}

        results.each do |id, installation|
          bots_by_installation_id[id] = installation.bot
        end

        bots_by_installation_id
      end
    end
  end
end
