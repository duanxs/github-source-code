# frozen_string_literal: true

module Platform
  module Loaders
    class GlobalHealthFilesRepository < Platform::Loader
      def self.load(owner_id)
        self.for.load(owner_id)
      end

      def fetch(owner_ids)
        ::Repository.where(owner_id: owner_ids, public: true, name: Repository::GLOBAL_HEALTH_FILES_NAME).index_by(&:owner_id)
      end
    end
  end
end
