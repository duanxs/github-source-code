# frozen_string_literal: true

module Platform
  module Loaders
    class RepositoryVisibleCheck < Platform::Loader
      def self.load(viewer, repository_id, resource: nil)
        self.for(viewer, resource).load(repository_id)
      end

      def initialize(viewer, resource)
        @viewer = viewer
        @resource = resource
      end

      def fetch(repository_ids)
        accessible_repository_ids = []
        private_repository_ids = []

        scope = Repository.where(id: repository_ids)
        scope = scope.where(disabled_at: nil) unless @viewer&.site_admin?

        scope.pluck(:id, :public).each do |id, public|
          if public
            accessible_repository_ids << id
          else
            private_repository_ids << id
          end
        end

        # TODO: Allow `Bot` users to also access private repositories.
        if @viewer && !@viewer.bot? && private_repository_ids.any?
          accessible_repository_ids.concat(@viewer.associated_repository_ids(repository_ids: private_repository_ids, resource: @resource))
        end

        accessible_repository_ids.each_with_object(Hash.new(false)) do |repository_id, result|
          result[repository_id] = true
        end
      end
    end
  end
end
