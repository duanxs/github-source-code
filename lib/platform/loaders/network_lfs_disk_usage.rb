# frozen_string_literal: true

module Platform
  module Loaders
    class NetworkLfsDiskUsage < Platform::Loader
      def self.load(network_id)
        self.for.load(network_id)
      end

      def fetch(network_ids)
        result = ::Media::Blob.where(repository_network_id: network_ids, state: 3)
          .group(:repository_network_id)
          .sum(:size)

        network_ids.each_with_object(result) do |network_id, result|
          result[network_id] ||= 0
        end
      end
    end
  end
end
