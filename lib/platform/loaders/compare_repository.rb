# frozen_string_literal: true

module Platform
  module Loaders
    class CompareRepository < Platform::Loader
      def self.load(repository, head_repository: nil, base_repository: nil)
        self.for(repository).load([head_repository, base_repository])
      end

      def initialize(repository)
        @repository = repository
      end

      def fetch(head_and_base_repository_pairs)
        # TODO: Shouldn't all these repositories here belong to the same network?
        Promise.all(head_and_base_repository_pairs.flatten.compact.map(&:async_network)).sync

        head_and_base_repository_pairs.map { |head_and_base_repository_pair|
          compare_repo = Repository.find(@repository.id)
          compare_repo.association(:network).target = @repository.network

          alternates = head_and_base_repository_pair.compact
          compare_repo.extend_rpc_alternates(*alternates) unless alternates.empty?

          [head_and_base_repository_pair, compare_repo]
        }.to_h
      end
    end
  end
end
