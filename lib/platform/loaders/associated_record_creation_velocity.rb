# frozen_string_literal: true

module Platform
  module Loaders

    # Calculates the creation velocity of associated records.
    # Note: Useful for identifying spammers.
    class AssociatedRecordCreationVelocity < Platform::Loader
      DEFAULT_INTERVAL = 3600 # seconds

      def self.load(model, association, interval: DEFAULT_INTERVAL)
        reflection = model.class.reflections[association.to_s]

        if reflection.nil?
          raise Errors::Internal.new("#{association} association does not exist on #{model.class}")
        end

        self.for(reflection, interval).load(model.id)
      end

      def initialize(reflection, interval)
        @reflection = reflection
        @interval = interval
      end

      def fetch(model_ids)
        github_sql = @reflection.klass.github_sql
        rows = github_sql.results(<<-SQL, model_ids: model_ids)
          SELECT
            stats.foreign_key,
            stats.count,
            first_#{@reflection.table_name}.created_at AS first_timestamp,
            last_#{@reflection.table_name}.created_at AS last_timestamp
          FROM (
            SELECT
              #{@reflection.foreign_key} AS foreign_key,
              count(id) AS count,
              min(id) AS first_id,
              max(id) AS last_id
            FROM #{@reflection.table_name}
            WHERE #{@reflection.foreign_key} IN :model_ids
            GROUP BY #{@reflection.foreign_key}
          ) stats
          JOIN #{@reflection.table_name} first_#{@reflection.table_name} ON first_#{@reflection.table_name}.id = stats.first_id
          JOIN #{@reflection.table_name} last_#{@reflection.table_name} ON last_#{@reflection.table_name}.id = stats.last_id
        SQL

        model_ids.inject(Hash.new) do |result, model_id|
          _, count, first_timestamp, last_timestamp = rows.find { |row| row[0] == model_id }
          velocity = 0

          if count && first_timestamp && last_timestamp && count > 2
            active_interval = last_timestamp - first_timestamp

            if active_interval > 0
              velocity = count / (active_interval / @interval).to_f
            end
          end

          result[model_id] = velocity.round
          result
        end
      end
    end
  end
end
