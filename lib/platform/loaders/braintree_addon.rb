# frozen_string_literal: true

module Platform
  module Loaders
    class BraintreeAddon < Platform::Loader
      def self.load(slug)
        self.for.load(slug)
      end

      def fetch(slugs)
        Braintree::AddOn
          .all
          .select { |addon| slugs.include?(addon.id) }
          .index_by(&:id)
      end
    end
  end
end
