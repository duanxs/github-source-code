# frozen_string_literal: true

module Platform
  module Loaders
    module Authorization
      class MostCapableUserRoleOnTeam < Platform::Loader
        def self.load(user:, team:)
          user = user.ability_delegate
          team = team.ability_delegate
          team_owner_id = team.owning_organization_id

          return ::Promise.resolve(nil) unless user && team && team_owner_id
          return ::Promise.resolve(nil) unless user.ability_id && team.ability_id

          self.for.load([user.ability_id, team.ability_id, team_owner_id])
        end

        # Internal: Fetch the role the user has over the team
        #
        # Returns a Hash{[user_id Integer, team_id Integer, team_owner_id Integer] => Role}.
        def fetch(user_and_team_and_team_owner_ids)
          user_ids, team_ids, team_owner_ids = user_and_team_and_team_owner_ids.transpose

          user_ids.uniq!
          team_ids.uniq!
          team_owner_ids.uniq!

          return [] if user_ids.empty?
          return [] if team_ids.empty?
          return [] if team_owner_ids.empty?

          team_to_owner = user_and_team_and_team_owner_ids.map { |_, team, owner| [team, owner] }.to_h

          ## Determines users with explicit direct or indirect (materialized) ability on the team
          ## This could include org admins too with action: read, indicating membership
          abilities = ::Ability.where(
            actor_id: user_ids,
            actor_type: "User",
            subject_id: team_ids,
            subject_type: "Team",
          ).where(priority: [::Ability.priorities[:direct], ::Ability.priorities[:indirect]])

          ## Platform::Loader.fetch returns a hash were the key is each of the elements found in the input argument.
          ## The following will be used as the result to return and is a Hash{[user_id Integer, team_id Integer, team_owner_id Integer] => Role}.
          ## The team_owner_id is not really relevant to the invoker, but is a side effect of injecting it via the load method
          loader_key_to_role = abilities
            .map { |ability| [[ability.actor_id, ability.subject_id, team_to_owner[ability.subject_id]], ability.action == "admin" ? "maintainer" : "member"] }
            .to_h

          ## Determines users with explicit direct admin ability over the team owning Organization
          team_owner_admins = ::Ability
            .user_admin_on_organization(actor_id: user_ids, subject_id: team_owner_ids)
            .where(
              priority: ::Ability.priorities[:direct],
            )

          # Determine from the input keys (user, team, owner) for which the user is an admin of the owner
          user_is_admin_of_org = team_owner_admins
            .map { |ability| [[ability.actor_id, ability.subject_id], true] }.to_h
          user_and_team_and_team_owner_ids
            .select { |user, team, owner| user_is_admin_of_org[[user, owner]] == true }
            .each { |user, team, owner| loader_key_to_role[[user, team, owner]] = "maintainer" }

          loader_key_to_role
        end
      end
    end
  end
end
