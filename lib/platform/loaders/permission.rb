# frozen_string_literal: true

module Platform
  module Loaders
    class Permission < Platform::Loader

      def self.load(actor, subject)
        actor = actor.ability_delegate
        subject = subject.ability_delegate

        return ::Promise.resolve(nil) unless actor && subject
        return ::Promise.resolve(nil) unless actor.ability_id && subject.ability_id

        # A simpler `actor == subject` triggers `method_missing` when using `CollectionProxy` instances as actors or subjects,
        # causing the entire association to be queried and loaded
        if actor.ability_type == subject.ability_type && actor.ability_id == subject.ability_id
          # everyone has autonomy over themselves
          return ::Promise.resolve(::Ability.actions[:admin])
        end

        self.for(actor.ability_type, subject.ability_type).load([actor.ability_id, subject.ability_id])
      end

      def initialize(actor_type, subject_type)
        @actor_type = actor_type
        @subject_type = subject_type
      end

      def fetch(actor_and_subject_ids)
        fetch_results_for(actor_and_subject_ids).each_with_object({}) do |(actor_id, subject_id, action), result|
          key = [actor_id, subject_id]
          current_action = result[key]

          if current_action.nil? || current_action < action
            result[key] = action
          end
        end
      end

      private

      def all_same_actor?(actor_and_subject_ids)
        first_actor = actor_and_subject_ids.first.first
        actor_and_subject_ids.all? { |(actor_id, subject_id)| actor_id == first_actor }
      end

      def fetch_results_for(actor_and_subject_ids)
        args = {
          actor_type:   @actor_type,
          subject_type: @subject_type,
        }

        sql = ApplicationRecord::Iam.github_sql.new(<<-SQL, args)
          SELECT actor_id, subject_id, action
          FROM permissions
          WHERE actor_type = :actor_type
            AND subject_type = :subject_type
            AND (
        SQL

        actor_and_subject_ids.each_with_index do |(actor_id, subject_id), index|
          sql.add(") OR (") if index > 0
          sql.add <<~SQL, actor_id: actor_id, subject_id: subject_id
            actor_id = :actor_id AND subject_id = :subject_id
          SQL
        end
        sql.add ")"

        GitHub.dogstats.time("platform.loaders.permission.fetch_results_for") do
          sql.results
        end
      end
    end
  end
end
