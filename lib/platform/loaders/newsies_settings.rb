# frozen_string_literal: true

module Platform
  module Loaders
    class NewsiesSettings < Platform::Loader
      def self.load(user_id)
        self.for.load(user_id)
      end

      def fetch(user_ids)
        newsies_response = GitHub.newsies.load_user_settings(user_ids)

        if newsies_response.success?
          newsies_response.value.index_by { |response| response.user.id }
        else
          {}
        end
      end
    end
  end
end
