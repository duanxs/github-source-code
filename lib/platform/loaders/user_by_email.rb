# frozen_string_literal: true

module Platform
  module Loaders
    class UserByEmail < Platform::Loader
      def self.load(email)
        if email =~ UserEmail::GENERIC_DOMAIN_REGEXP || email.blank?
          return Promise.resolve(nil)
        end

        self.for.load(email)
      end

      def self.load_all(emails)
        Promise.all(emails.map(&method(:load)))
      end

      def fetch(emails)
        emails_to_users = User.find_by_emails(emails)

        emails.map { |email|
          [email, emails_to_users[email] || emails_to_users[email.downcase]]
        }.to_h
      end
    end
  end
end
