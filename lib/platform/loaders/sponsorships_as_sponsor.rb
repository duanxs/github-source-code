# frozen_string_literal: true

module Platform
  module Loaders
    class SponsorshipsAsSponsor < Platform::Loader
      DEFAULT_ORDER_FIELD = "created_at"
      DEFAULT_ORDER_DIRECTION = "DESC"

      def self.load(sponsor_id, viewer: nil, order_by: nil)
        self.for(viewer, order_by, **{}).load(sponsor_id)
      end

      def initialize(viewer, order_by)
        @viewer = viewer
        @order_by = order_by
      end

      def fetch(sponsor_ids)
        active_sponsorships = ::Sponsorship.active.where(sponsor_id: sponsor_ids)

        return [] if active_sponsorships.empty?

        public_sponsorships = active_sponsorships.privacy_public

        if !order_by.nil?
          public_sponsorships = active_sponsorships
                                  .privacy_public
                                  .order("sponsorships.#{order_by[:field]} #{order_by[:direction]}")
        elsif viewer && !public_sponsorships.empty?
          public_sponsorships = public_sponsorships.ranked_by_sponsorable(for_user: viewer)
        else
          public_sponsorships = active_sponsorships
                                  .privacy_public
                                  .order("sponsorships.#{DEFAULT_ORDER_FIELD} #{DEFAULT_ORDER_DIRECTION}")
        end

        public_sponsorships.group_by(&:sponsor_id)
      end

      private

      attr_reader :viewer, :order_by
    end
  end
end
