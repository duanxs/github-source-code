# frozen_string_literal: true

module Platform
  module Loaders
    class Ability < Platform::Loader

      def self.load(actor, subject)
        actor = actor.ability_delegate
        subject = subject.ability_delegate

        return ::Promise.resolve(nil) unless actor && subject
        return ::Promise.resolve(nil) unless actor.ability_id && subject.ability_id

        # A simpler `actor == subject` triggers `method_missing` when using `CollectionProxy` instances as actors or subjects,
        # causing the entire association to be queried and loaded
        if actor.ability_type == subject.ability_type && actor.ability_id == subject.ability_id
          # everyone has autonomy over themselves
          return ::Promise.resolve(::Ability.actions[:admin])
        end
        self.for(actor.ability_type, subject.ability_type).load([actor.ability_id, subject.ability_id])
      end

      def initialize(actor_type, subject_type)
        @actor_type = actor_type
        @subject_type = subject_type
      end

      def fetch(actor_and_subject_ids)
        fetch_results_for(actor_and_subject_ids).each_with_object({}) do |(actor_id, subject_id, action), result|
          key = [actor_id, subject_id]
          current_action = result[key]

          if current_action.nil? || current_action < action
            result[key] = action
          end
        end
      end

      private
      def all_same_actor?(actor_and_subject_ids)
        first_actor = actor_and_subject_ids.first.first
        actor_and_subject_ids.all? { |(actor_id, _subject_id)| actor_id == first_actor }
      end

      def fetch_results_for(actor_and_subject_ids)
        GitHub.dogstats.time("platform.loaders.ability.fetch_results_for") do
          owning_org_ids = owning_organization_ids(actor_and_subject_ids)
          actor_results = actor_results(actor_and_subject_ids)

          # The organization_owner_results query only needs to be run if there is a subject with an owning
          # organization.
          if !owning_org_ids.empty?
            # Create a Hash of actor/subject ids where the subject has an owning organization
            actor_and_subjects_with_owning_org_ids =
              actor_and_subject_ids.each_with_object({}) do |(actor_id, subject_id), as|
                as[[actor_id, subject_id]] = owning_org_ids[subject_id] if owning_org_ids.has_key? subject_id
              end

            actor_results = actor_results +
             organization_owner_results(actor_and_subjects_with_owning_org_ids)
          end

          actor_results + ancestor_results(actor_and_subject_ids)
        end
      end

      def actor_results(actor_and_subject_ids)
        router = ::Permissions::QueryRouter.for(subject_types: [@subject_type])
        options = {actor_type: @actor_type,
                   subject_type: @subject_type}

        if all_same_actor?(actor_and_subject_ids)
          options[:actor_id] = actor_and_subject_ids.first.first
        end

        actor_sql = router.sql(options)
        actor_sql.bind direct: ::Ability.priorities[:direct]
        actor_sql.add <<~SQL
          SELECT actor_id, subject_id, action
          FROM :abilities_or_permissions
          WHERE actor_type = :actor_type AND subject_type = :subject_type
          AND priority <= :direct AND ((
        SQL

        actor_and_subject_ids.each_with_index do |(actor_id, subject_id), index|
          actor_sql.add ") OR (" if index > 0
          actor_sql.add <<~SQL, actor_id: actor_id, subject_id: subject_id
            actor_id = :actor_id AND subject_id = :subject_id
          SQL
        end
        actor_sql.add "))"

        actor_sql.results
      end

      # Determine the owning organization of the subjects
      # Returns a hash indexed by subject_id
      def owning_organization_ids(actor_and_subject_ids)
        return {} if actor_and_subject_ids.empty?
        subject_ids = actor_and_subject_ids.map { |(_actor_id, subject_id)| subject_id }.uniq

        case @subject_type
        when "Team"
          ::Team.where({id: subject_ids}).pluck(:id, :organization_id).to_h
        when "Project"
          ::Project.where({id: subject_ids, owner_type: "Organization"}).pluck(:id, :owner_id).to_h
        when "Repository"
          repos = ::Repository.includes(:parent, :owner).where(id: subject_ids)
          repos.each_with_object({}) do |repo, owning_org_ids|
            owning_org_ids[repo.id] = repo.owning_organization_id if repo.owning_organization_id.present?
          end
        else
          {}
        end
      end

      def organization_owner_results(actor_and_subjects_with_owning_org_ids)
        return [] if actor_and_subjects_with_owning_org_ids.nil?
        actor_and_subject_ids = actor_and_subjects_with_owning_org_ids.keys

        router = ::Permissions::QueryRouter.for(subject_types: [@subject_type])
        options = {actor_type: @actor_type,
                   subject_type: @subject_type}

        if all_same_actor?(actor_and_subject_ids)
          options[:actor_id] = actor_and_subject_ids.first.first
        end

        organization_owner_sql = router.sql(options)
        organization_owner_sql.bind  admin: ::Ability.actions[:admin], direct: ::Ability.priorities[:direct]

        organization_owner_sql.add <<~SQL
          SELECT owning_org.actor_id, owning_org.subject_id
          FROM :abilities_or_permissions owning_org
          WHERE (
        SQL

        actor_and_subject_ids.each_with_index do |(actor_id, subject_id), index|
          owning_org_id = actor_and_subjects_with_owning_org_ids[[actor_id, subject_id]]
          organization_owner_sql.add ") OR (" if index > 0
          organization_owner_sql.add <<~SQL, actor_id: actor_id, owning_org_id: owning_org_id
            owning_org.actor_id = :actor_id AND
            owning_org.subject_id = :owning_org_id AND
            owning_org.actor_type = :actor_type AND
            owning_org.subject_type = 'Organization' AND
            owning_org.action = :admin AND
            owning_org.priority = :direct
          SQL
        end
        organization_owner_sql.add ")"
        actor_has_admin_on_owning_org = organization_owner_sql.results

        # If an org shows up in the results, then the actor has admin on it.
        # The calling results comparison expects an array of [actor_id, subject_id, action] results.
        # So, loop through actors_and_subjects, look up the org via owning_org_ids, and create the necessary array

        actor_and_subject_ids.each_with_object([]) do |(actor_id, subject_id, _action), results|
          org_id = actor_and_subjects_with_owning_org_ids[[actor_id, subject_id]]
          next unless actor_has_admin_on_owning_org.include?([actor_id, org_id])
          results << [actor_id, subject_id, ::Ability.actions[:admin]]
        end
      end

      def ancestor_results(actor_and_subject_ids)
        router = ::Permissions::QueryRouter.for(subject_types: [@subject_type])
        options = {actor_type: @actor_type,
                   subject_type: @subject_type}

        if all_same_actor?(actor_and_subject_ids)
          options[:actor_id] = actor_and_subject_ids.first.first
        end

        ancestor_sql = router.sql(options)
        ancestor_sql.bind direct: ::Ability.priorities[:direct]
        ancestor_sql.add <<~SQL
          SELECT grandparent.actor_id, parent.subject_id, parent.action
          FROM   :abilities_or_permissions parent
          /* abilities-join-audited */
          JOIN   :abilities_or_permissions grandparent
          ON     grandparent.subject_type = parent.actor_type
          AND    grandparent.subject_id   = parent.actor_id
          AND    parent.priority          <= :direct
          AND    grandparent.priority     <= :direct
          WHERE  grandparent.actor_type = :actor_type AND parent.subject_type = :subject_type AND (
        SQL

        actor_and_subject_ids.each_with_index do |(actor_id, subject_id), index|
          ancestor_sql.add ") OR (" if index > 0
          ancestor_sql.add <<~SQL, actor_id: actor_id, subject_id: subject_id
            grandparent.actor_id = :actor_id AND parent.subject_id = :subject_id
          SQL
        end

        ancestor_sql.add ")"

        ancestor_sql.results
      end
    end
  end
end
