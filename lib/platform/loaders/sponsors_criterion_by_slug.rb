# frozen_string_literal: true

module Platform
  module Loaders
    class SponsorsCriterionBySlug < Platform::Loader
      def self.load(slug:)
        self.for.load(slug)
      end

      def fetch(slugs)
        ::SponsorsCriterion.where(slug: slugs).index_by(&:slug)
      end
    end
  end
end
