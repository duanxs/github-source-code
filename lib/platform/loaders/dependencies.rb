# frozen_string_literal: true

module Platform
  module Loaders
    class Dependencies < Platform::Loader
      def self.load_manifests(repository, params)
        return stubbed_manifest_data(repository) if stub_data?

        if !repository.dependency_graph_enabled?
          return Promise.new.fulfill(GitHub::Result.new { [] })
        elsif !repository.dependency_manifests_detected?
          RepositoryDependencyManifestViewerInitializationJob.perform_later(repository.id)
          return Promise.new.fulfill(GitHub::Result.error(::Repository::ManifestsNotDetectedError.new))
        else
          self.for(::DependencyGraph::ManifestsQuery).load(params)
        end
      end

      def self.load_package_releases(params)
        return stubbed_package_releases_data(params) if stub_data?

        self.for(::DependencyGraph::PackageReleaseQuery).load(params)
      end

      def self.load_package_release_vulnerabilities(params)
        self.for(::DependencyGraph::PackageReleaseVulnerabilitiesQuery).load(params)
      end

      def self.load_repository_package_releases(params)
        self.for(::DependencyGraph::RepositoryPackageReleaseQuery).load(params)
      end

      def self.load_repository_dependencies(params)
        return stubbed_repository_dependencies(params) if stub_data?

        self.for(::DependencyGraph::RepositoryDependenciesQuery).load(params)
      end

      def self.load_repository_package_release_licenses(params)
        self.for(::DependencyGraph::RepositoryPackageReleaseLicensesQuery).load(params)
      end

      def self.load_repository_package_release_vulnerabilities(params)
        self.for(::DependencyGraph::RepositoryPackageReleaseVulnerabilitySeveritiesQuery).load(params)
      end

      def self.load_estimated_dependent_repository_count(params)
        return rand(100) if stub_data?

        ::DependencyGraph::AllRepositoriesWithVersionRangeQuery.new(**params.merge(timeout: 10))
          .estimated_repository_count
          .value!
      end

      def self.load_packages(params)
        return stubbed_packages_data(params) if stub_data?

        self.for(::DependencyGraph::PackagesQuery).load(params)
      end

      def self.load_unmapped_packages(params)
        self.for(::DependencyGraph::UnmappedPackagesQuery).load(params)
      end

      def self.stub_data?
        Rails.env.development? && !GitHub.dependency_graph_api_url
      end

      def self.stubbed_manifest_data(manifest_repo)
        stub_data ArrayWrapper.new(::DependencyGraph.stubbed_manifests(manifest_repo, random_repos))
      end

      def self.stubbed_package_releases_data(params)
        releases = ::DependencyGraph.stubbed_package_releases(params, random_repos)

        stub_data ArrayWrapper.new(releases)
      end

      def self.stubbed_packages_data(params)
        stub_data ArrayWrapper.new(::DependencyGraph.stubbed_packages(params, random_repos))
      end

      def self.stubbed_repository_dependencies(params)
        stub_data({ direct_dependencies: random_repos.pluck(:id), transitive_dependencies: random_repos.pluck(:id) })
      end

      def self.random_repos
        ::Repository.public_scope
          .where("repositories.id < ?", rand(1_000_000))
          .first(5)
      end

      def self.stub_data(data)
        Promise.new.fulfill GitHub::Result.new { data }
      end

      def initialize(query_class)
        @query_class = query_class
      end

      def fetch(keys)
        keys.map do |key|
          [key, query_class.new(**key).results]
        end.to_h
      end

      private

      attr_reader :query_class
    end
  end
end
