# frozen_string_literal: true

module Platform
  module Interfaces
    module RepositoryOwner
      include Platform::Interfaces::Base
      include Platform::Interfaces::FeatureFlaggable

      description "Represents an owner of a Repository."

      field :id, ID, method: :global_relay_id, null: false

      field :login, String, "The username used to login.", null: false

      field :url, Scalars::URI, description: "The HTTP URL for the owner.", null: false

      field :resource_path, Scalars::URI, "The HTTP URL for the owner.", null: false

      database_id_field(visibility: :internal)

      field :avatar_url, Scalars::URI, description: "A URL pointing to the owner's public avatar.", null: false do
        argument :size, Integer, "The size of the resulting square image.", required: false
      end

      field :repository, Objects::Repository, description: "Find Repository.", null: true do
        argument :name, String, "Name of Repository to find.", required: true
      end

      def repository(**arguments)
        repo = @object.find_repo_by_name(arguments[:name])
        repo || nil
      end

      field :viewer_can_administer, Boolean, visibility: :internal, description: "Owner of repo is adminable by the viewer.", null: false

      def viewer_can_administer
        @object.adminable_by?(@context[:viewer])
      end

      field :retired_namespaces, Connections.define(Platform::Objects::RetiredNamespace), visibility: :internal, description: "A list of retired namespaces for this owner", null: false, connection: true

      def retired_namespaces
        unless @context[:viewer].site_admin?
          raise Errors::Forbidden.new("#{@context[:viewer]} does not have permission to retrieve retired namespaces.")
        end

        ::RetiredNamespace.for_owner(@object)
      end

      field :repositories, resolver: Resolvers::Repositories, connection: true do
        description "A list of repositories that the user owns."
        argument :is_fork, Boolean, "If non-null, filters repositories according to whether they are forks of another repository", required: false
      end

      field :template_repositories, resolver: Resolvers::TemplateRepositories,
        visibility: :under_development, connection: true do
          description "A list of template repositories relevant to this user or organization."
        end

      field :is_actions_eligible, Boolean, visibility: :internal, description: "Owner's eligibility to use Actions", null: false

      def is_actions_eligible
        Billing::ActionsPermission.new(@object).allowed?
      end

      field :actions_status, Objects::ActionsStatus, visibility: :internal,
        null: true, description: "The image used to represent this repository in Open Graph data."

      def actions_status
        @object
      end

      field :is_usage_allowed, Boolean, visibility: :internal, description: "Returns whether or not an action can be run at this time", null: false do
        argument :public, Boolean, "Is usage for public use", required: true
      end

      def is_usage_allowed(**arguments)
        Billing::ActionsPermission.new(@object).usage_allowed?(public: arguments[:public])
      end

      field :is_storage_allowed, Boolean, visibility: :internal, description: "Returns whether or not the given amount of bytes would go over storage limits", null: false do
        argument :public, Boolean, "Is storage for public use", required: true
      end

      def is_storage_allowed(**arguments)
        Billing::ActionsPermission.new(@object).storage_allowed?(
          public: arguments[:public],
        )
      end

      field :is_spammy, Boolean, visibility: :internal, description: "Whether or not the user is spammy.", null: false, method: :spammy?
    end
  end
end
