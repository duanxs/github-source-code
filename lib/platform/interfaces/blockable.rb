# frozen_string_literal: true

module Platform
  module Interfaces
    module Blockable
      include Platform::Interfaces::Base
      description "Entities that have the action to block the creating user."
      visibility :internal
      areas_of_responsibility :community_and_safety

      field :viewer_can_block, Boolean, visibility: :internal, description: "Check if the current viewer can block the author of this content.", null: false

      field :viewer_can_unblock, Boolean, visibility: :internal, description: "Check if the current viewer can unblock the author of this content.", null: false

      def viewer_can_block
        @object.async_viewer_can_block?(@context[:viewer])
      end

      def viewer_can_unblock
        @object.async_viewer_can_unblock?(@context[:viewer])
      end
    end
  end
end
