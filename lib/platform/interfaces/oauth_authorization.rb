# frozen_string_literal: true

module Platform
  module Interfaces
    module OauthAuthorization
      include Platform::Interfaces::Base
      description "Represents an OAuth Authorization."
      visibility :internal

      global_id_field :id

      created_at_field
      field :accessed_at, Scalars::DateTime, "The last time this authorization was used to perform an action", null: true
    end
  end
end
