# frozen_string_literal: true

module Platform
  module Interfaces
    module ActionExecutionSettings
      include Platform::Interfaces::Base

      description "Represents a common set of settings for executing actions."
      visibility :internal
      areas_of_responsibility :actions

      field :action_execution_capability, Enums::ActionExecutionCapability, "Indicates action invocation capabilities for this object", method: :action_execution_capability, null: false

      def action_execution_capability
        @object.action_execution_capabilities && @object.action_execution_capabilities.downcase
      end
    end
  end
end
