# frozen_string_literal: true

module Platform
  module Interfaces
    module Comment
      include Platform::Interfaces::Base
      description "Represents a comment."

      field :id, ID, method: :global_relay_id, null: false

      field :authored_by_subject_author, Boolean, visibility: :internal, null: false, description: "Did the comment author also author the comment subject."
      def authored_by_subject_author
        case @object
        when ::CommitComment
          commit.then do |commit|
            if commit.present?
              commit.async_authors.then { |authors| authors.map(&:id).include?(@object.user_id) }
            else
              false
            end
          end
        when ::GistComment
          @object.async_gist.then { |gist| gist.user_id == @object.user_id }
        when ::IssueComment
          @object.async_issue.then { |issue| issue.user_id == @object.user_id }
        when ::PullRequestReview, ::PullRequestReviewComment
          @object.async_pull_request.then { |pull| pull.user_id == @object.user_id }
        when ::DiscussionPostReply
          @object.async_discussion_post.then { |post| post.user_id == @object.user_id }
        else
          false
        end
      end

      field :author_sponsorship_for_subject_owner, Objects::Sponsorship,
        visibility: :internal,
        null: true,
        description: "The sponsorship between the comment author and the associated repo owner."

      def author_sponsorship_for_subject_owner
        return unless GitHub.sponsors_enabled?
        return unless @context[:viewer]
        return unless @object.respond_to?(:async_user)
        return unless @object.respond_to?(:async_repository)

        @object.async_repository.then do |repo|
          next unless repo

          repo.async_owner.then do |sponsorable|
            next unless sponsorable

            Platform::Loaders::SponsorsListingCheck.load(sponsorable.id).then do |is_listed|
              next unless is_listed

              Platform::Loaders::SponsorshipsByCommentAuthor.load(
                @context[:viewer],
                sponsorable,
                @object.user_id,
              )
            end
          end
        end
      end

      field :subject_type, String, visibility: :internal, null: true, description: "The comment subject type."
      def subject_type
        case @object
        when ::IssueComment
          @object.async_issue.then { |issue| issue.pull_request? ? "pull request" : "issue" }
        when ::GistComment, ::CommitComment
          @object.class.name.downcase.gsub("comment", "")
        when ::PullRequestReview, ::PullRequestReviewComment
          "pull request"
        when ::DiscussionPostReply
          "discussion"
        else
          nil
        end
      end

      field :viewer_did_author, Boolean, description: "Did the viewer author this comment.", null: false

      def viewer_did_author
        if @context[:viewer]
          @object.user_id == @context[:viewer].id
        else
          false
        end
      end

      field :body, String, description: "The body as Markdown.", null: false

      def body
        case @object
        when ::PullRequest
          @object.async_issue.then { |issue| issue.body || "" }
        else
          @object.body || ""
        end
      end

      field :body_html, Scalars::HTML, description: "The body rendered to HTML.", null: false do
        argument :hide_code_blobs, Boolean, "Whether or not to include the HTML for code blobs", required: false, default_value: false, feature_flag: :pe_mobile
        argument :render_suggested_changes_as_text, Boolean, "Whether or not to include the HTML for suggested changes", required: false, default_value: false, feature_flag: :pe_mobile
      end

      def body_html(hide_code_blobs: false, render_suggested_changes_as_text: false)
        @object.async_body_html.then do |body_html|
          body_html || GitHub::HTMLSafeString::EMPTY
        end
      end

      field :short_body_html, Scalars::HTML, description: "Returns a truncated version of the body, rendered as HTML.", null: false, visibility: :under_development do
        argument :limit, Integer, "Limit the length of the returned HTML.", default_value: 150,
          required: false
      end

      def short_body_html(limit: nil)
        @object.async_truncated_body_html(limit)
      end

      field :body_text, String, description: "The body rendered to text.", null: false

      def body_text
        @object.async_body_text.then do |body_text|
          body_text || ""
        end
      end

      field :body_version, String, visibility: :internal, description: "The comment body hash.", null: false

      created_at_field
      updated_at_field

      field :published_at, Scalars::DateTime, "Identifies when the comment was published at.", method: :created_at, null: true

      field :created_via_email, Boolean, "Check if this comment was created via an email reply.", null: false

      field :viewer_can_read_user_content_edits, Boolean, visibility: :internal, description: "Check if this comment's edits may be shown to the viewer.", null: false

      def viewer_can_read_user_content_edits
        if @object.is_a?(UserContentEditable)
          @object.async_viewer_can_read_user_content_edits?(@context[:viewer])
        else
          Promise.resolve(false)
        end
      end

      field :user_content_edits, Connections::UserContentEdit, description: "A list of edits to this content.", null: true, connection: true

      def user_content_edits
        viewer_can_read_user_content_edits.then do |can_read|
          next ArrayWrapper.new unless can_read
          (@object.is_a?(PullRequest) ? @object.async_issue : Promise.resolve(@object)).then do |comment|
            comment.user_content_edits.order(id: :desc)
          end
        end
      end

      field :last_user_content_edit, Objects::UserContentEdit, description: "The last edit to this content.", null: true, visibility: :under_development

      def last_user_content_edit
        viewer_can_read_user_content_edits.then do |can_read|
          next unless can_read
          (@object.is_a?(PullRequest) ? @object.async_issue : Promise.resolve(@object)).then do |comment|
            comment.async_latest_user_content_edit
          end
        end
      end

      field :includes_created_edit, Boolean, "Check if this comment was edited and includes an edit with the creation data", null: false

      def includes_created_edit
        viewer_can_read_user_content_edits.then do |can_read|
          can_read ? @object.async_includes_created_edit? : false
        end
      end

      field :spammy, Boolean, visibility: :internal, description: "Check if this comment is spammy.", null: false

      def spammy
        if @context[:viewer].try(:site_admin?)
          @object.async_user.then do |user|
            user ? user.spammy? : false
          end
        else
          false
        end
      end

      field :author_association, Enums::CommentAuthorAssociation, description: "Author's association with the subject of the comment.", null: false

      def author_association
        association = CommentAuthorAssociation.new(comment: @object, viewer: @context[:viewer])
        association.async_to_sym
      end

      field :author, description: "The actor who authored the comment.", resolver: Resolvers::ActorUser

      field :editor, Interfaces::Actor, description: "The actor who edited the comment.", null: true

      def editor
        viewer_can_read_user_content_edits.then do |can_read|
          next unless can_read
          ::Platform::Helpers::Editor.for(editable: @object, context: @context)
        end
      end

      field :last_edited_at, Scalars::DateTime, description: "The moment the editor made the last edit", null: true

      def last_edited_at
        last_user_content_edit.then do |user_content_edit|
          user_content_edit&.edited_at
        end
      end

      field :show_edit_history_onboarding, Boolean, description: "Should the viewer see the edit history onboarding", null: false, visibility: :internal

      def show_edit_history_onboarding
        return false unless @context[:viewer]
        viewer_id = @context[:viewer].id

        Promise.all([
          viewer_can_read_user_content_edits,
          Platform::Loaders::KV.load("user.show_edit_history_onboarding.#{viewer_id}"),
          Platform::Loaders::KV.load("user.show_edit_history_onboarding.#{viewer_id}.#{@object.class.name}.#{@object.id}"),
        ]).then do |can_read, never_show_onboarding, show_onboarding_for_object|
          can_read && !never_show_onboarding && !!show_onboarding_for_object
        end
      end

      field :stafftools_url, Platform::Scalars::URI, visibility: :internal, description: "The URL for the content in stafftools for moderation purposes", null: true

      # Returns the stafftools URL for comment types viewable in stafftools
      def stafftools_url
        return nil unless @context[:viewer].try(:site_admin?)
        return nil unless @object.respond_to?(:async_repository)

        @object.async_repository.then do |repo|
          case @object
          when IssueComment
            @object.async_issue.then do |issue|
              "/stafftools/repositories/#{repo.nwo}/issues/#{issue.number}/comments/#{@object.id}"
            end
          when PullRequestReviewComment
            @object.async_pull_request.then do |pr|
              "/stafftools/repositories/#{repo.nwo}/pull_requests/#{pr.number}/review_comments/#{@object.id}"
            end
          when CommitComment
            Rails.application.routes.url_helpers.stafftools_commit_comment_path(@object)
          else
            nil
          end
        end
      end

      field :show_first_contribution_prompt, Boolean, visibility: :internal, description: "Should the viewer see the first contribution prompt", null: false

      def show_first_contribution_prompt
        if @object.is_a?(PullRequest) && @context[:viewer]
          @object.async_issue.then do |issue|
            issue.async_repository.then do |repository|
              repository.async_owner.then {
                issue.show_first_contribution_prompt?(@context[:viewer])
              }
            end
          end
        else
          false
        end
      end
    end
  end
end
