# frozen_string_literal: true

module Platform
  module Interfaces
    module ForkPrWorkflowsPolicy
      include Platform::Interfaces::Base

      description "Represents the policy applied to running workflows from fork pull requests."
      visibility :internal
      areas_of_responsibility :actions

      field :fork_pr_workflows_policy, Enums::ForkPrWorkflowsPolicyValue, "Indicates the fork PR workflow policy for this object", method: :fork_pr_workflows_policy, null: false

      def fork_pr_workflows_policy
        if GitHub.flipper[:fork_pr_workflows_policy].enabled?(@object.owner)
          @object.fork_pr_workflows_policy
        elsif @object.public?
          Configurable::ForkPrWorkflowsPolicy::PUBLIC_REPO_DEFAULT_POLICY
        else
          Configurable::ForkPrWorkflowsPolicy::PRIVATE_REPO_DEFAULT_POLICY
        end
      end
    end
  end
end
