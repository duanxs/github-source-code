# frozen_string_literal: true

module Platform
  module Interfaces
    module MarketplaceListingOwner
      include Platform::Interfaces::Base
      description "Represents an owner of a MarketplaceListing."
      visibility :internal
      areas_of_responsibility :marketplace

      database_id_field

      field :login, String, "The username used to login.", null: false

      field :name, String, description: "The owner's public profile name.", null: true

      def name
        @object.async_profile.then do |profile|
          profile.try(:name)
        end
      end

      field :avatar_url, Scalars::URI, description: "A URL pointing to the owner's public avatar.", null: false do
        argument :size, Integer, "The size of the resulting square image.", required: false
      end

      def avatar_url(**arguments)
        @object.primary_avatar_url(arguments[:size])
      end

      field :active_listing_plan, Objects::MarketplaceListingPlan, description: <<~DESCRIPTION, null: true do
          Returns the listing plan for which this owner has an active subscription, for the
          specified Marketplace listing.
        DESCRIPTION
        argument :listing_slug, String, "The short name of the listing used in its URL.", required: true
      end

      def active_listing_plan(**arguments)
        @object.async_plan_subscription.then do |plan_subscription|
          next unless plan_subscription

          plan_subscription.async_active_subscription_items.then do |items|
            plan_promises = items.map(&:async_subscribable)
            Promise.all(plan_promises).then do |listing_plans|
              listing_promises = listing_plans.map(&:async_listing)
              Promise.all(listing_promises).then do
                listing_plans.compact.detect do |listing_plan|
                  listing_plan.listing.slug == arguments[:listing_slug]
                end
              end
            end
          end
        end
      end
    end
  end
end
