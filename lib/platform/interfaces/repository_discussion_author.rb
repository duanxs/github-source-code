# frozen_string_literal: true

module Platform
  module Interfaces
    module RepositoryDiscussionAuthor
      include Platform::Interfaces::Base

      description "Represents an author of discussions in repositories."
      areas_of_responsibility :discussions
      visibility :under_development

      field :repository_discussions, Connections.define(Platform::Objects::Discussion),
          description: "Discussions this user has started.", null: false, connection: true do
        argument :order_by, Inputs::DiscussionOrder,
          "Ordering options for discussions returned from the connection.", required: false,
          default_value: { field: "created_at", direction: "DESC" }
        argument :repository_id, ID, "Filter discussions to only those in a specific repository.",
          required: false
        argument :answered, Boolean, "Filter discussions to only those that have been answered " \
          "or not. Defaults to including both answered and unanswered discussions.", required: false,
          default_value: nil
      end

      def repository_discussions(order_by: nil, repository_id: nil, answered: nil)
        scope = ::Discussion.authored_by(@object).filter_spam_for(@context[:viewer])

        if order_by
          field = order_by[:field]
          direction = order_by[:direction]
          scope = scope.order("discussions.#{field} #{direction}")
        end

        if answered == false
          scope = scope.unanswered
        elsif answered
          scope = scope.answered
        end

        if repository_id
          Helpers::NodeIdentification.async_typed_object_from_id(
            [Objects::Repository], repository_id, @context
          ).then do |repo|
            scope.for_repository(repo)
          end
        else
          visible_repository_ids = @context[:viewer].associated_repository_ids
          repository_ids_with_authored_discussions = Discussion.
            authored_by(@object).
            distinct.
            pluck(:repository_id)
          public_repo_ids = Repository.public_scope.where(id: repository_ids_with_authored_discussions).pluck(:id)

          scope.where(repository_id: visible_repository_ids | public_repo_ids)
        end
      end
    end
  end
end
