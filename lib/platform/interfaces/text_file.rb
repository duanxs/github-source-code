# frozen_string_literal: true

module Platform
  module Interfaces
    module TextFile
      include Platform::Interfaces::Base
      description "Entities that return contents of a text tree entry."

      areas_of_responsibility :repositories

      feature_flag :pe_mobile

      field :file_lines, [Objects::FileLine, null: true], feature_flag: :pe_mobile, description: "The lines for this file.", null: true

      def file_lines
        @object.async_file_lines
      end

      field :content_raw, String, description: "The raw content of the markdown.", feature_flag: :pe_mobile, method: :async_data, null: true
    end
  end
end
