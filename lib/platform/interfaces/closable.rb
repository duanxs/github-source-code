# frozen_string_literal: true

module Platform
  module Interfaces
    module Closable
      include Platform::Interfaces::Base
      description "An object that can be closed"

      field :closed, Boolean, "`true` if the object is closed (definition of closed may depend on type)", method: :closed?, null: false

      field :closed_at, Scalars::DateTime, description: "Identifies the date and time when the object was closed.", null: true

      def closed_at
        (@object.is_a?(PullRequest) ? @object.async_issue : Promise.resolve(@object)).then do |issue|
          @object.closed_at
        end
      end

      field :viewer_can_close, Boolean, visibility: :internal, null: false, description: "`true` if the current user can close the issue or PR."

      def viewer_can_close
        (@object.is_a?(PullRequest) ? @object.async_issue : Promise.resolve(@object)).then do |issue|
            issue.async_closeable_by?(@context[:viewer])
        end
      end

    end
  end
end
