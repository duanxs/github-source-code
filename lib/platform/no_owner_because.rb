# frozen_string_literal: true

module Platform
  module NoOwnerBecause
    DEPRECATED  = nil
    UNAUDITED   = nil
  end
end
