# frozen_string_literal: true

module Platform
  module Edges
    class PullRequestReviewThread < Edges::Base
    end
  end
end
