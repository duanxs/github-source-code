# frozen_string_literal: true

module Platform
  module Edges
    class EnterpriseMember < Edges::Base
      node_type Unions::EnterpriseMember
      description "A User who is a member of an enterprise through one or more organizations."

      field :is_unlicensed, Boolean, description: "Whether the user does not have a license for the enterprise.", null: false,
        deprecated: {
          start_date: Date.new(2020, 7, 10),
          reason: "All members consume a license",
          owner: "BrentWheeldon",
          superseded_by: nil,
        }
      def is_unlicensed
        false
      end

      field :user_license, Platform::Objects::UserLicense, description: "The user license for the enterprise.", null: true,
        deprecated: {
          start_date: Date.new(2020, 7, 13),
          reason: "Not all members will have a user license record",
          owner: "BrentWheeldon",
          superseded_by: nil,
        }
      def user_license
        nil
      end
    end
  end
end
