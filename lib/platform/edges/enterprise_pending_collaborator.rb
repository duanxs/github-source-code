# frozen_string_literal: true

module Platform
  module Edges
    class EnterprisePendingCollaborator < Edges::Base
      node_type Objects::User
      description "A user with an invitation to be a collaborator on a repository owned by an organization in an enterprise."

      field :repositories, Connections.define(Objects::EnterpriseRepositoryInfo), null: false,
        description: "The enterprise organization repositories this user is a member of." do
        argument :order_by, Inputs::RepositoryOrder,
          "Ordering options for repositories.",
          required: false, default_value: { field: "name", direction: "ASC" }
      end

      def repositories(order_by: nil)
        user = object.node
        business = object.parent

        # this maps to `Platform::Models::EnterpriseRepositoryInfo` to avoid
        # permissions checks on the `Repository` object.  Business admins
        # won't necessarily have read permissions on repos but we still want
        # to allow them to read repository names here
        # This should be looked at again before making business GraphQL public
        business_org_ids = business.organizations.pluck(:id)
        repos = user.invited_repositories.where(organization_id: business_org_ids)

        unless order_by.nil?
          repos = repos.order("repositories.#{order_by[:field]} #{order_by[:direction]}")
        end

        repos = repos.map { |repo| Platform::Models::EnterpriseRepositoryInfo.new(repo, business) }
        ArrayWrapper.new(repos)
      end

      field :is_unlicensed, Boolean, description: "Whether the invited collaborator does not have a license for the enterprise.", null: false,
        deprecated: {
        start_date: Date.new(2020, 7, 10),
        reason: "All pending collaborators consume a license",
        owner: "BrentWheeldon",
        superseded_by: nil,
      }

      def is_unlicensed
        false
      end
    end
  end
end
