# frozen_string_literal: true

module Platform
  module Edges
    class SponsorsListing < Edges::Base
      visibility :internal
    end
  end
end
