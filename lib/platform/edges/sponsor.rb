# frozen_string_literal: true

module Platform
  module Edges
    class Sponsor < Edges::Base
      visibility :internal
      description "Represents a sponsor."

      # TODO: We might have to adjust this based on public/anonymous sponsorships
      def self.authorized?(object, context)
        sponsor = object.node
        Objects::User.authorized?(sponsor, context)
      end

      node_type Objects::User
    end
  end
end
