# frozen_string_literal: true

module Platform
  module Edges
    class PullRequestReviewItem < Edges::Base
      feature_flag :pe_mobile

      node_type Unions::PullRequestReviewItem
    end
  end
end
