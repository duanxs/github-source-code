# frozen_string_literal: true
module Platform
  module Edges
    class GitActor < Edges::Base
      feature_flag :pe_mobile
    end
  end
end
