# frozen_string_literal: true

module Platform
  module Edges
    class EnterprisePendingMemberInvitation < Edges::Base
      node_type Objects::OrganizationInvitation
      description "An invitation to be a member in an enterprise organization."

      field :is_unlicensed, Boolean,
        description: "Whether the invitation has a license for the enterprise.", null: false,
        deprecated: {
          start_date: Date.new(2020, 1, 1),
          reason: "All pending members consume a license",
          owner: "BrentWheeldon",
          superseded_by: nil,
        }

      def is_unlicensed
        false
      end
    end
  end
end
