# frozen_string_literal: true

module Platform
  module Objects
    class JobPosition < Platform::Objects::Base
      description "An open job position to work at GitHub."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        true
      end

      minimum_accepted_scopes ["site_admin"]
      visibility :internal

      field :title, String, description: "The title of the job position.", hash_key: "text", null: false

      field :location, String, description: "The location of the job position.", null: false

      field :url, Scalars::URI, description: "The link to more information about the position.", hash_key: "hostedUrl", null: false
    end
  end
end
