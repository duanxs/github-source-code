# frozen_string_literal: true

module Platform
  module Objects

    class LineRange
      attr_accessor :lines
      attr_reader :commit
      attr_reader :repository

      def initialize(commit, lines, repository)
        @commit = commit
        @lines = []
        @now = Time.now
        @repository = repository

        domain = [0, @now - commit.repository.created_at]
        range = (1..10).map { |ix| "#{ix}" }
        @quantile = Quantile.new(domain: domain, range: range)
      end

      def scale
        @quantile.scale(@now - @commit.authored_date)
      end

      def platform_type_name
        # This class is actually a PORO defined in objects/blame.rb
        "BlameRange"
      end
    end

    class Blame < Platform::Objects::Base
      description "Represents a Git blame."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(_permission, _object)
        # No special API permissions
        true
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        permission.belongs_to_git_object(object)
      end

      scopeless_tokens_as_minimum

      field :ranges, [Objects::BlameRange], description: "The list of ranges from a Git blame.", null: false

      def ranges
        ranges = @object.empty? ? [] : [LineRange.new(@object.first[2], [], @object.repository)]
        @object.each do |lineno, old_lineno, commit, text|
          if commit.oid != ranges.last.commit.oid
            ranges << LineRange.new(commit, [], @object.repository)
          end
          line = { lineno: lineno, old_lineno: old_lineno, commit: commit, text: text }
          ranges.last.lines << line
        end
        ranges
      end
    end
  end
end
