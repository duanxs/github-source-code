# frozen_string_literal: true

module Platform
  module Objects
    class ExternalIdentity < Platform::Objects::Base
      description "An external identity provisioned by SAML SSO or SCIM."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, external_identity)
        external_identity.async_provider.then do |provider|
          provider.async_target.then do |target|
            case target
            when ::Organization
              permission.access_allowed?(:v4_manage_org_users, resource: target, organization: target, current_repo: nil, allow_integrations: true, allow_user_via_integration: true)
            when ::Business
              next true if permission.access_allowed?(:view_enterprise_external_identities, resource: target, organization: nil, current_repo: nil, allow_integrations: true, allow_user_via_integration: true, raise_on_error: false)
              external_identity.async_user.then do |user|
                target.async_organizations.then do |orgs|
                  Promise.all(orgs.map { |org|
                    org.async_member?(user).then do |member|
                      member && permission.access_allowed?(:v4_manage_org_users, resource: org, organization: org, current_repo: nil, allow_integrations: true, allow_user_via_integration: true, raise_on_error: false)
                    end
                  }).then { |org_checks| org_checks.any?(true) }
                end
              end
            else
              false
            end
          end
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        return true if permission.viewer.site_admin?

        object.async_provider.then do |provider|
          provider.async_target.then do |target|
            target.async_adminable_by?(permission.viewer).then do |adminable|
              next true if adminable

              case target
              when ::Organization
                next false unless permission.viewer.try(:installation)
                next true if target.resources.members.readable_by?(permission.viewer)
                false
              when ::Business
                next false unless permission.viewer.try(:installation)
                target.async_organizations.then do |orgs|
                  orgs.any? { |org| org.resources.members.readable_by?(permission.viewer) }
                end
              else
                false
              end
            end
          end
        end
      end

      minimum_accepted_scopes ["admin:org"]

      implements Platform::Interfaces::Node

      global_id_field :id

      field :guid, String, "The GUID for this identity", null: false

      field :saml_identity, Objects::ExternalIdentitySamlAttributes, description: "SAML Identity attributes", null: true

      def saml_identity
        Models::ExternalIdentityAttributes.new(@object, as: :saml)
      end

      field :scim_identity, Objects::ExternalIdentityScimAttributes, description: "SCIM Identity attributes", null: true

      def scim_identity
        Models::ExternalIdentityAttributes.new(@object, as: :scim)
      end

      field :user, Objects::User, "User linked to this external identity. Will be NULL if this identity has not been claimed by an organization member.", null: true

      class AdminableMember < SimpleDelegator
        # Viewer is an administrator in this organization context and should be
        # able to see spammy members with external identities.
        def hide_from_user?(viewer)
          false
        end

        def platform_type_name
          # This object is a proxy to a user, with some methods added.
          "User"
        end
      end

      def user
        @object.async_user.then do |user|
          AdminableMember.new(user) if user.present?
        end
      end

      field :organization_invitation, Objects::OrganizationInvitation, "Organization invitation for this SCIM-provisioned external identity", method: :async_organization_invitation, null: true

      field :saml_user_data, String, "SAML user data for the external identity formatted as JSON", null: true, visibility: :internal

      def saml_user_data
        @object.async_identity_attribute_records.then do
          @object.saml_user_data.to_json
        end
      end

      field :scim_user_data, String, "SCIM user data for the external identity formatted as JSON", null: true, visibility: :internal

      def scim_user_data
        @object.async_identity_attribute_records.then do
          @object.scim_user_data.to_json
        end
      end

      field :created_at, Scalars::DateTime, "The date and time that this external identity was created", null: false, visibility: :internal
      field :updated_at, Scalars::DateTime, "The date and time that this external identity was updated", null: false, visibility: :internal

      field :user_sessions, Connections.define(Objects::UserSession), description: "The user sessions for this external identity", null: false, connection: true, visibility: :internal

      def user_sessions
        @object.async_user_sessions.then do
          @object.user_sessions
        end
      end

      field :authorized_credentials, Connections.define(Objects::OrganizationCredentialAuthorization), description: "A list of AuthorizedCredentials for the user and SAML provider of this ExternalIdentity", null: false, connection: true

      def authorized_credentials
        Promise.all([
          @object.async_provider,
          @object.async_user,
        ]).then do
          @object.provider.async_target.then do
            @object.provider.target.credential_authorizations.
              where(actor: @object.user)
          end
        end
      end
    end
  end
end
