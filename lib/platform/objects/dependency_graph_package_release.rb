# frozen_string_literal: true

module Platform
  module Objects
    class DependencyGraphPackageRelease < Platform::Objects::Base
      description "A package release"

      areas_of_responsibility :dependency_graph

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        true
      end

      visibility :internal

      minimum_accepted_scopes ["public_repo"]

      field :package_name, String, "The package name", visibility: :under_development, null: true
      field :package_manager, String, "The package manager", visibility: :under_development, null: true
      field :published_on, Scalars::Date, "The date the package release was published", visibility: :under_development, null: true
      field :version, String, "The package release version", null: true
      field :license, String, description: "The license for the package release", null: true
      field :clearly_defined_score, Integer, description: "The ClearlyDefined score for the package release license, if available", null: true, visibility: :under_development

      field :repository, Objects::Repository, description: "The repository containing the package release", null: true

      def repository
        Loaders::ActiveRecord.load(::Repository, @object.repository_id, security_violation_behaviour: :nil)
      end

      field :dependencies_count, Integer, "The number of dependencies specified by the package release", null: true
      field :dependencies, [Platform::Objects::DependencyGraphDependency, null: true], description: "A list of package release dependencies", null: true
      field :external_package_manager_url, Scalars::URI, description: "The URL to a package release on its respective registry", null: true

      def external_package_manager_url
        return unless @object.package_name.present?
        return unless @object.package_manager.present?

        package_manager = @object.package_manager
        package_name = @object.package_name

        case package_manager
        when "NUGET"
          Addressable::Template.new("https://www.nuget.org/packages/{package}").expand(package: package_name)
        when "PIP"
          Addressable::Template.new("https://pypi.org/project/{package}").expand(package: package_name)
        when "NPM"
          Addressable::Template.new("https://www.npmjs.com/package/{package}").expand(package: package_name)
        when "RUBYGEMS"
          Addressable::Template.new("https://rubygems.org/gems/{package}").expand(package: package_name)
        when "MAVEN"
          # maven url for a package splits package name into group and artifact id
          org_id, artifact_id = package_name.split(":")
          Addressable::Template.new("https://search.maven.org/artifact/{org_id}/{artifact_id}").expand(org_id: org_id, artifact_id: artifact_id)
        when "COMPOSER"
          Addressable::Template.new("https://packagist.org/packages/{package}").expand(package: package_name)
        else
          # Package Manager for Package has not yet been defined
          nil
        end
      end
    end
  end
end
