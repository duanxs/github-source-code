# frozen_string_literal: true

module Platform
  module Objects
    class FeatureHistoryEntry < Platform::Objects::Base
      description "Represents a host that has a feature enabled"

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, *)
        return false unless permission.viewer
        return true if permission.viewer.can_have_granular_permissions? &&
          Apps::Internal.capable?(:read_flipper_features, app: permission.viewer.integration)

        permission.viewer.github_developer? || permission.viewer.site_admin?
      end

      visibility :internal

      scopeless_tokens_as_minimum

      implements Interfaces::UniformResourceLocatable

      field :feature_name, String, description: "The name of the feature flag", null: false
      field :feature, Feature, description: "The feature flag this entry is for", null: true

      def feature
        Loaders::ActiveRecord.load(::FlipperFeature, @object["feature_name"], column: :name)
      end

      field :actor_label, String, description: "The login of the actor", null: true

      def actor_label
        Loaders::ActiveRecord.load(::User, @object["actor_id"]).then do |user|
          user&.login
        end
      end

      field :action, Enums::FeatureHistoryEntryAction, description: "Whether the feature was enabled or disabled", null: false

      def action
        @object["action"].sub(/\Afeature\./, "").to_sym
      end

      field :gate_name, Enums::FeatureHistoryEntryGateName, description: "The conditions for which this entry applies", null: true

      def gate_name
        if gate_name = @object["data"]["gate_name"]
          gate_name.to_sym
        end
      end

      field :subject_label, String, description: "A label to help display the entry", null: true

      def subject_label
        case @object["data"]["gate_name"]
        when "boolean"
          "everyone"
        when "percentage_of_time"
          @object["data"]["subject"] + "%"
        when "percentage_of_actors"
          @object["data"]["subject"] + "%"
        when "actor"
          @object["data"]["subject"]
        when "group"
          @object["data"]["subject"]
        end
      end

      field :timestamp, Scalars::DateTime, description: "When this took place", null: true

      def timestamp
        Timestamp.to_time(@object["@timestamp"])
      end

      url_fields description: "The HTTP URL for this feature" do |object|
        template = Addressable::Template.new("/devtools/feature_flags/{name}")
        template.expand name: object["feature_name"]
      end
    end
  end
end
