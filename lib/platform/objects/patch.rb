# frozen_string_literal: true

module Platform
  module Objects
    class Patch < Platform::Objects::Base
      description "Represents the changes to an individual file in a diff."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, object)
        repo = object.diff.repo

        permission.async_owner_if_org(repo).then do |org|
          permission.access_allowed?(
            :get_diff,
            resource: repo,
            current_org: org,
            current_repo: repo,
            allow_integrations: true,
            allow_user_via_integration: true,
          )
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        permission.typed_can_see?("Diff", object.diff)
      end

      feature_flag :pe_mobile

      minimum_accepted_scopes ["repo"]

      field :lines_added, Integer, "The number of lines added in this patch.", method: :additions, null: false

      field :lines_deleted, Integer, "The number of lines removed in this patch.", method: :deletions, null: false

      field :lines_changed, Integer, "The total lines added or removed in this patch.", method: :changes, null: false

      field :new_tree_entry, Objects::TreeEntry, "The tree entry after the change.", null: true

      field :old_tree_entry, Objects::TreeEntry, "The tree entry before the change.", null: true

      field :similarity, Integer, "The percent similarity in the case of a rename.", visibility: :internal, null: true

      field :status, Enums::PatchStatus, "Indentifies the status of the patch.", null: false

      field :text, String, "The textual diff of this patch.", null: true

      field :unicode_text, String, "The textual diff of this patch in Unicode.", visibility: :internal, null: true

      field :diff_lines, [Objects::DiffLine, null: true], description: "The diff lines for this patch.", null: true do
        argument :syntax_highlighting_enabled, Boolean, default_value: true, description: "Indicates whether diff lines should be syntax highlighted.", required: false
      end

      def diff_lines(**arguments)
        @object.async_diff_lines \
          syntax_highlighted_diffs_enabled: arguments[:syntax_highlighting_enabled]
      end

      field :is_submodule, Boolean, null: false, method: :submodule?, description: "Whether or not the patch is in a submodule"
      field :is_binary, Boolean, null: false, method: :binary?, description: "Whether or not the patch is binary"
      field :is_large_diff, Boolean, null: false, method: :is_large_diff?, description: "Whether or not the patch is a large diff"

      field :blob_url, Scalars::URI, null: false, visibility: :internal, description: "The HTML blob patch URL."
      field :raw_url, Scalars::URI, null: false, visibility: :internal, description: "The HTML raw patch URL."
      field :contents_url, Scalars::URI, null: false, visibility: :internal, description: "The contents API URL."
    end
  end
end
