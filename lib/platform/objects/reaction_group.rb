# frozen_string_literal: true

module Platform
  module Objects
    class ReactionGroup < Platform::Objects::Base
      description "A group of emoji reactions to a particular piece of content."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, reaction_group)
        subject = reaction_group.subject

        permission.rewrite_reaction_subject(subject).then do |subject_type, subject|
          permission.typed_can_access?(subject_type, subject)
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        permission.rewrite_reaction_subject(object.subject).then do |subject_type, subject|
          permission.typed_can_see?(subject_type, subject)
        end
      end

      scopeless_tokens_as_minimum

      field :content, Enums::ReactionContent, "Identifies the emoji reaction.", null: false

      field :subject, Interfaces::Reactable, description: "The subject that was reacted to.", null: false

      def subject
        subject = @object.subject

        if subject.is_a?(::Issue)
          # It might be a PullRequest, let's check
          subject.async_pull_request.then { |pull| pull || subject }
        else
          subject
        end
      end

      field :created_at, Scalars::DateTime, "Identifies when the reaction was created.", null: true

      field :viewer_has_reacted, Boolean, description: "Whether or not the authenticated user has left a reaction on the subject.", null: false

      def viewer_has_reacted
        @object.user_reacted?(@context[:viewer])
      end

      field :users, Connections::ReactingUser, description: "Users who have reacted to the reaction subject with the emotion represented by this reaction group", null: false, connection: true

      def users
        ArrayWrapper.new(@object.reactions)
      end
    end
  end
end
