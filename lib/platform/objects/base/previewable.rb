# frozen_string_literal: true

module Platform
  module Objects
    class Base < GraphQL::Schema::Object
      module Previewable
        attr_accessor :preview_toggled_by
      end
    end
  end
end
