# frozen_string_literal: true

module Platform
  module Objects
    class Base < GraphQL::Schema::Object
      class Argument < GraphQL::Schema::Argument
        include Platform::Objects::Base::Deprecated
        include Platform::Objects::Base::MarkForUpcomingTypeChange
        include Platform::Objects::Base::Visibility
        include Platform::Objects::Base::Previewable

        def initialize(*args, visibility: nil, **kwargs, &block)
          @feature_flag = kwargs.delete(:feature_flag)
          if kwargs.key?(:default_value) && kwargs[:required]
            raise Platform::Errors::Internal, <<~ERR
              Use `required: true` _or_ a `default_value:`, but not both.

              (A required argument will never apply a default value, so using both doesn't make sense)
            ERR
          end
          visibility_from_config(visibility)

          super(*args, **kwargs, &block)
        end

        # @return [nil, Symbol] If present, an HTTP header which must be present to access this field
        attr_accessor :feature_flag
      end
    end
  end
end
