# frozen_string_literal: true

module Platform
  module Objects
    class Base < GraphQL::Schema::Object
      module AreasOfResponsibility
        def areas_of_responsibility(*areas)
          if areas.empty?
            @areas_of_responsibility || []
          else
            @areas_of_responsibility = areas.flatten
          end
        end

        def generate_input_type
          apply_areas_of_responsibility_to_class(super)
        end

        def generate_payload_type
          apply_areas_of_responsibility_to_class(super)
        end

        private

        def apply_areas_of_responsibility_to_class(defn_class)
          if @areas_of_responsibility
            defn_class.areas_of_responsibility(@areas_of_responsibility)
          end
          defn_class
        end
      end
    end
  end
end
