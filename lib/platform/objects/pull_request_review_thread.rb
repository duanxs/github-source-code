# frozen_string_literal: true

module Platform
  module Objects
    class PullRequestReviewThread < Platform::Objects::Base
      description "A threaded list of comments for a given pull request."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, prr_thread)
        permission.load_pull_and_issue(prr_thread).then do |pull|
          permission.async_repo_and_org_owner(pull).then do |repo, org|
            pull.repository = repo # avoid association load down the line
            permission.access_allowed?(:list_pull_request_comments, repo: repo, current_org: org, resource: pull, allow_integrations: true, allow_user_via_integration: true)
          end
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        object.async_pull_request.then do |pull_request|
          permission.typed_can_see?("PullRequest", pull_request).then do |can_read_pull_request|
            can_read_pull_request && object.async_hide_from_user?(permission.viewer).then do |hide_from_viewer|
              !hide_from_viewer
            end
          end
        end
      end

      scopeless_tokens_as_minimum

      implements Platform::Interfaces::Node

      global_id_field :id

      database_id_field(visibility: :internal)

      url_fields(
        prefix: :discussion_diff,
        visibility: :internal,
        description: "The HTTP URL permalink for this review thread's diff excerpt on the discussion page.",
      ) do |thread|
        thread.async_discussion_diff_path_uri
      end

      # TODO: define_url_field should be used here but doesn't yet support nullable values.
      #   When that method is updated, this definition should be migrated which will
      #   automatically add the corresponding definition for *Url.
      field :original_diff_resource_path, Scalars::URI, visibility: :internal, method: :async_original_diff_path_uri, description: "The HTTP URL permalink for this review thread positioned in the original diff.", null: true

      # TODO: define_url_field should be used here but doesn't yet support nullable values.
      #   When that method is updated, this definition should be migrated which will
      #   automatically add the corresponding definition for *Url.
      field :current_diff_resource_path, Scalars::URI, visibility: :internal, method: :async_current_diff_path_uri, description: "The HTTP URL permalink for this review thread positioned in the current diff.", null: true

      # TODO: define_url_field should be used here but doesn't yet support nullable values.
      #   When that method is updated, this definition should be migrated which will
      #   automatically add the corresponding definition for *Url.
      field :original_diff_file_resource_path, Scalars::URI, visibility: :internal, method: :async_original_diff_file_path_uri, description: "The HTTP URL permalink for this review thread's file in the original diff.", null: true

      # TODO: define_url_field should be used here but doesn't yet support nullable values.
      #   When that method is updated, this definition should be migrated which will
      #   automatically add the corresponding definition for *Url.
      field :current_diff_file_resource_path, Scalars::URI, visibility: :internal, method: :async_current_diff_file_path_uri, description: "The HTTP URL permalink for this review thread's file in the current diff.", null: true

      field :pull_request, Objects::PullRequest, "Identifies the pull request associated with this thread.", method: :async_pull_request, null: false
      field :repository, Repository, "Identifies the repository associated with this thread.", method: :async_repository, null: false

      field :position, Integer, visibility: :internal, description: "Identifies the position of this thread.", null: true

      field :path, String, feature_flag: :pe_mobile, description: "Identifies the file path of this thread.", method: :async_path, null: false

      field :start_line, Integer, description: "The start line in the file to which this thread refers (multi-line only)", null: true
      def start_line
        @object.async_start_line.then do |line|
          line.current if line
        end
      end

      field :original_start_line, Integer, description: "The original start line in the file to which this thread refers (multi-line only).", method: :async_original_start_line, null: true

      field :start_line_type, String, feature_flag: :pe_mobile, description: "The type of line the thread starts on (multi-line only)", null: true
      def start_line_type
        @object.async_start_line.then do |line|
          next nil unless line
          case line.type
          when :addition then "+"
          when :deletion then "-"
          else
            ""
          end
        end
      end

      field :start_diff_side, Enums::DiffSide, description: "The side of the diff that the first line of the thread starts on (multi-line only)", method: :start_side, null: true

      field :line, Integer, description: "The line in the file to which this thread refers", null: true, method: :async_line

      field :original_line, Integer, description: "The original line in the file to which this thread refers.", method: :async_original_line, null: true

      field :end_line_type, String, feature_flag: :pe_mobile, description: "The type of line the thread refers to (multi-line only)", null: true
      def end_line_type
        @object.async_end_line.then do |line|
          next nil unless line
          case line.type
          when :addition then "+"
          when :deletion then "-"
          else
            ""
          end
        end
      end

      field :diff_side, Enums::DiffSide, description: "The side of the diff on which this thread was placed.", method: :async_diff_side, null: false

      field :pull_request_commit, Objects::PullRequestCommit, visibility: :under_development, method: :async_pull_request_commit, description: "The commit to which this thread refers.", null: false

      field :is_outdated, Boolean, feature_flag: :pe_mobile, method: :outdated?, description: "Indicates whether this thread was outdated by newer changes.", null: false

      field :comments, resolver: Resolvers::PullRequestReviewComments, description: "A list of pull request comments associated with the thread.", connection: true do
        argument :skip, Integer, description: "Skips the first _n_ elements in the list.", required: false

        argument :focus, ID, description: "ID of element to focus on.", visibility: :internal, required: false

        argument :skip_if_collapsed, Boolean, default_value: false, description: "Skip loading of comments if thread is collapsed.", visibility: :internal, required: false
      end

      field :viewer_can_reply, Boolean, feature_flag: :pe_mobile, description: "Indicates whether the current viewer can reply to this thread.", null: false

      def viewer_can_reply
        @object.async_viewer_can_reply?(@context[:viewer])
      end

      field :viewer_cannot_reply_reasons, [Enums::ThreadCannotReplyReason], visibility: :internal, description: "Reasons why the current viewer can not reply to this thread.", null: false

      def viewer_cannot_reply_reasons
        @object.async_viewer_cannot_reply_reasons(@context[:viewer])
      end

      field :is_resolved, Boolean, null: false, description: "Whether this thread has been resolved"
      def is_resolved
        @object.async_resolved_by_id.then do |resolver_id|
          resolver_id.present?
        end
      end

      field :resolved_by, Objects::User, null: true, description: "The user who resolved this thread"
      def resolved_by
        @object.async_resolved_by.then do |resolver|
          next if resolver&.hide_from_user?(context[:viewer])

          resolver
        end
      end

      field :is_collapsed, Boolean, null: false, feature_flag: :pe_mobile, method: :async_collapsed?, description: "Whether or not the thread has been collapsed (outdated or resolved)"

      field :viewer_can_resolve, Boolean, null: false, description: "Whether or not the viewer can resolve this thread"
      def viewer_can_resolve
        @object.async_can_resolve(context[:viewer])
      end

      field :viewer_can_unresolve, Boolean, null: false, description: "Whether or not the viewer can unresolve this thread"
      def viewer_can_unresolve
        @object.async_can_unresolve(context[:viewer])
      end

      url_fields prefix: :resolve, visibility: :internal, description: "URL for resolve form" do |thread|
        thread.async_pull_request.then do |pull_request|
          pull_request.async_path_uri.then do |uri|
            uri = uri.dup
            uri.path = "#{uri.path}/threads/#{thread.global_relay_id}/resolve"
            uri
          end
        end
      end

      url_fields prefix: :unresolve, visibility: :internal, description: "URL for unresolve form" do |thread|
        thread.async_pull_request.then do |pull_request|
          pull_request.async_path_uri.then do |uri|
            uri = uri.dup
            uri.path = "#{uri.path}/threads/#{thread.global_relay_id}/unresolve"
            uri
          end
        end
      end

      field :diff_lines, [Objects::DiffLine, null: true], feature_flag: :pe_mobile, description: "The diff lines where this thread was created on.", null: true do
        argument :syntax_highlighting_enabled, Boolean, default_value: true, description: "Indicates whether diff lines should be syntax highlighted.", required: false

        argument :max_context_lines, Integer, default_value: 3, description: "Number of context lines to return.", required: false

        argument :skip_if_collapsed, Boolean, default_value: false, description: "Skip loading of diff lines if thread is collapsed.", required: false
      end

      def diff_lines(**arguments)
        @object.async_collapsed?.then do |is_collapsed|
          if is_collapsed && arguments[:skip_if_collapsed]
            Promise.resolve(nil)
          else
            @object.async_diff_lines \
              syntax_highlighted_diffs_enabled: arguments[:syntax_highlighting_enabled],
              max_context_lines: arguments[:max_context_lines]
          end
        end
      end

      field :are_diff_lines_truncated, Boolean, description: "Are the diff lines for this thread truncated? Only true for multi-line comments that reference more than 100 lines.", null: false, method: :diff_lines_truncated?, visibility: :under_development

      def self.load_from_global_id(id)
        id, version = id.split(":", 2)

        if version == ::PullRequestReviewThread::GLOBAL_ID_V2_IDENTIFIER
          # New global id format - id is based on the `id` column of `::PullRequestReviewThread`
          Loaders::ActiveRecord.load(::PullRequestReviewThread, id.to_i).then do |review_thread|
            next nil unless review_thread
            review_thread.async_repository.then do |repository|
              if GitHub.flipper[:use_review_thread_ar_model].enabled?(repository)
                review_thread
              else
                review_thread.async_review_comments.then do |comments|
                  next nil if comments.none?

                  Platform::Models::PullRequestReviewThread.from_review_comments(comments, id_version: 2)
                end
              end
            end
          end
        else
          # Old global id format - id is based on the `id` column of `::PullRequestReviewComment`

          # Support for old format can be dropped once this counter stays at 0
          GitHub.dogstats.increment("platform.deprecation.pull_request_review_thread_global_id")

          Loaders::ActiveRecord.load(::PullRequestReviewComment, id.to_i).then do |review_comment|
            next nil unless review_comment

            review_comment.async_repository.then do |repository|
              if GitHub.flipper[:use_review_thread_ar_model].enabled?(repository)
                review_comment.async_pull_request_review_thread
              else
                review_comment.async_pull_request_review_thread.then(&:async_review_comments).then do |review_comments|
                  if review_comments.first == review_comment
                    Platform::Models::PullRequestReviewThread.from_review_comments(review_comments, id_version: 1)
                  else
                    Platform::Models::PullRequestReviewThread.from_review_comments([review_comment], id_version: 1)
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
