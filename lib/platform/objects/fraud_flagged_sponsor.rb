# frozen_string_literal: true

module Platform
  module Objects
    class FraudFlaggedSponsor < Platform::Objects::Base
      description "An entry for a sponsor associated with a fraud review."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        permission.viewer.can_admin_sponsors_memberships?
      end

      visibility :internal

      minimum_accepted_scopes ["site_admin"]
      implements Platform::Interfaces::Node
      global_id_field :id

      created_at_field
      updated_at_field

      field :fraud_review,
        Objects::SponsorsFraudReview,
        method: :async_sponsors_fraud_review,
        description: "The fraud review this sponsor entry is for.",
        null: false

      field :sponsor,
        Interfaces::Sponsorable,
        method: :async_sponsor,
        description: "The sponsor that is being flagged for review.",
        null: true

      field :matched_current_client_id,
        String,
        description: "The matching client ID, if the sponsor's current client ID matched one of the sponsorable's historical client IDs.",
        null: true

      field :matched_current_ip,
        String,
        description: "The matching IP address, if the sponsor's current IP address matched one of the sponsorable's historical IP addresses.",
        null: true

      field :matched_historical_ip,
        String,
        description: "The matching IP address, if the sponsor's historical IP addresses matched one of the sponsorable's historical IP addresses.",
        null: true

      field :matched_historical_client_id,
        String,
        description: "The matching client ID, if the sponsor's historical client IDs matched one of the sponsorable's historical client IDs.",
        null: true

      field :matched_current_ip_region_and_user_agent,
        String,
        description: "The matching IP region and user agent, if the sponsor's current IP region and user agent matched the sponsorable's.",
        null: true
    end
  end
end
