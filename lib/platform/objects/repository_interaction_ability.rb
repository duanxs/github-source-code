# frozen_string_literal: true

module Platform
  module Objects
    class RepositoryInteractionAbility < Platform::Objects::Base
      description "Repository interaction limit that applies to this object."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, ability)
        return true if permission.viewer.site_admin?

        ability.interactable.async_can_set_interaction_limits?(permission.viewer)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        return true if permission.viewer.site_admin?

        object.interactable.async_can_set_interaction_limits?(permission.viewer)
      end

      visibility :under_development, environments: [:dotcom]
      visibility :internal, environments: [:enterprise]

      minimum_accepted_scopes ["repo"]

      field :limit, Enums::RepositoryInteractionLimit, description: "The current limit that is enabled on this object.", null: false

      def limit
        @object.interaction_ability.async_overall_active_limit
      end

      field :origin, Enums::RepositoryInteractionLimitOrigin, description: "The origin of the currently active interaction limit.", null: false

      def origin
        @object.interaction_ability.async_active_limit_origin
      end

      field :expires_at, Scalars::DateTime, description: "The time the currently active limit expires.", null: true

      def expires_at
        @object.interaction_ability.async_overall_active_limit_expiry
      end
    end
  end
end
