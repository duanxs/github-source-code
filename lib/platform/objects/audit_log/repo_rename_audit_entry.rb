# frozen_string_literal: true

module Platform
  module Objects
    module AuditLog
      class RepoRenameAuditEntry < Platform::Objects::Base
        description "Audit log entry for a repo.rename event."

        visibility :under_development
        minimum_accepted_scopes ["read:user", "admin:org", "admin:enterprise"]

        areas_of_responsibility :audit_log, :repositories

        # Determine whether the viewer can access this object via the API (called internally).
        # This is where Egress checks for OAuth scopes and GitHub Apps go.
        # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
        def self.async_api_can_access?(permission, audit_entry)
          permission.async_api_can_access_audit_entry?(audit_entry)
        end

        # Determine whether the viewer can see this object (called internally).
        # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
        def self.async_viewer_can_see?(permission, audit_entry)
          permission.async_viewer_can_see_audit_entry?(audit_entry)
        end

        def self.load_from_global_id(document_id)
          Audit.load_from_global_id(document_id)
        end

        implements Platform::Interfaces::Node
        implements Platform::Interfaces::AuditLog::AuditEntry
        implements Platform::Interfaces::AuditLog::RepositoryAuditEntryData

        global_id_field :id

        field :repository_name_was, String, null: true, description: "The former name of the repository"
        def repository_name_was
          old_name = @object.get(:old_name)

          if @object.get(:org).present?
            "#{@object.get(:org)}/#{old_name}"
          elsif @object.get(:user).present?
            "#{@object.get(:user)}/#{old_name}"
          else
            old_name
          end
        end
      end
    end
  end
end
