# frozen_string_literal: true

module Platform
  module Objects
    module AuditLog
      class OrgUpdateMemberRepositoryCreationPermissionAuditEntry < Platform::Objects::Base
        description "Audit log entry for a org.update_member_repository_creation_permission event."

        minimum_accepted_scopes ["read:user", "admin:org", "admin:enterprise"]

        areas_of_responsibility :audit_log, :orgs

        # Determine whether the viewer can access this object via the API (called internally).
        # This is where Egress checks for OAuth scopes and GitHub Apps go.
        # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
        def self.async_api_can_access?(permission, audit_entry)
          permission.async_api_can_access_audit_entry?(audit_entry)
        end

        # Determine whether the viewer can see this object (called internally).
        # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
        def self.async_viewer_can_see?(permission, audit_entry)
          permission.async_viewer_can_see_audit_entry?(audit_entry)
        end

        def self.load_from_global_id(document_id)
          Audit.load_from_global_id(document_id)
        end

        implements Platform::Interfaces::Node
        implements Platform::Interfaces::AuditLog::AuditEntry
        implements Platform::Interfaces::AuditLog::OrganizationAuditEntryData

        global_id_field :id

        field :visibility, Platform::Enums::AuditLog::OrgUpdateMemberRepositoryCreationPermissionAuditEntryVisibility, null: true, description: "The permission for visibility level of repositories for this organization."
        def visibility
          @object.get(:visibility)
        end

        field :can_create_repositories, Boolean, null: true, description: "Can members create repositories in the organization."
        def can_create_repositories
          @object.get(:permission) == "true"
        end
      end
    end
  end
end
