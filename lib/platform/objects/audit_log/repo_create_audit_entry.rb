# frozen_string_literal: true

module Platform
  module Objects
    module AuditLog
      class RepoCreateAuditEntry < Platform::Objects::Base
        description "Audit log entry for a repo.create event."

        minimum_accepted_scopes ["read:user", "admin:org", "admin:enterprise"]

        areas_of_responsibility :audit_log, :repositories

        # Determine whether the viewer can access this object via the API (called internally).
        # This is where Egress checks for OAuth scopes and GitHub Apps go.
        # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
        def self.async_api_can_access?(permission, audit_entry)
          permission.async_api_can_access_audit_entry?(audit_entry)
        end

        # Determine whether the viewer can see this object (called internally).
        # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
        def self.async_viewer_can_see?(permission, audit_entry)
          permission.async_viewer_can_see_audit_entry?(audit_entry)
        end

        def self.load_from_global_id(document_id)
          Audit.load_from_global_id(document_id)
        end

        implements Platform::Interfaces::Node
        implements Platform::Interfaces::AuditLog::AuditEntry
        implements Platform::Interfaces::AuditLog::RepositoryAuditEntryData
        implements Platform::Interfaces::AuditLog::OrganizationAuditEntryData

        global_id_field :id

        field :fork_parent_name, String, null: true, description: "The name of the parent repository for this forked repository."
        def fork_parent_name
          @object.get :fork_parent
        end

        field :fork_source_name, String, null: true, description: "The name of the root repository for this netork."
        def fork_source_name
          @object.get :fork_source
        end

        field :visibility, Platform::Enums::AuditLog::RepoCreateAuditEntryVisibility, null: true, description: "The visibility of the repository"
        def visibility
          @object.get(:visibility)
        end

        # internal only

        field :fork_parent_database_id, Integer, null: true, visibility: :internal, description: "The database ID of the parent repository for this forked repository."
        def fork_parent_database_id
          @object.get :fork_parent_id
        end

        field :fork_source_database_id, Integer, null: true, visibility: :internal, description: "The database ID of the root repository for this netork."
        def fork_source_database_id
          @object.get :fork_source_id
        end
      end
    end
  end
end
