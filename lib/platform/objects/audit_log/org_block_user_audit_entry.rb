# frozen_string_literal: true

module Platform
  module Objects
    module AuditLog
      class OrgBlockUserAuditEntry < Platform::Objects::Base
        description "Audit log entry for a org.block_user"

        minimum_accepted_scopes ["admin:org", "admin:enterprise"]

        areas_of_responsibility :audit_log, :orgs

        # Determine whether the viewer can access this object via the API (called internally).
        # This is where Egress checks for OAuth scopes and GitHub Apps go.
        # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
        def self.async_api_can_access?(permission, audit_entry)
          permission.async_api_can_access_audit_entry?(audit_entry)
        end

        # Determine whether the viewer can see this object (called internally).
        # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
        def self.async_viewer_can_see?(permission, audit_entry)
          permission.async_viewer_can_see_audit_entry?(audit_entry)
        end

        def self.load_from_global_id(document_id)
          Audit.load_from_global_id(document_id)
        end

        implements Platform::Interfaces::Node
        implements Platform::Interfaces::AuditLog::AuditEntry
        implements Platform::Interfaces::AuditLog::OrganizationAuditEntryData

        global_id_field :id

        field :blocked_user_name, String, null: true, description: "The username of the blocked user."
        def blocked_user_name
          @object.get(:blocked_user)
        end

        field :blocked_user_database_id, Integer, null: true, visibility: :internal, description: "The database ID of the blocked user."
        def blocked_user_database_id
          @object.get(:blocked_user_id)
        end

        field :blocked_user, Platform::Objects::User, null: true, description: "The blocked user."
        def blocked_user
          Loaders::ActiveRecord.load(::User, @object.get(:blocked_user_id))
        end

        url_fields prefix: :blocked_user, null: true, description: "The HTTP URL for the blocked user." do |audit_entry|
          Loaders::ActiveRecord.load(::User, audit_entry.get(:blocked_user_id)).then do |blocked_user|
            template_blocked_user = blocked_user&.login

            if template_blocked_user.present?
              template = Addressable::Template.new("/{blocked_user}")
              template.expand(blocked_user: template_blocked_user)
            end
          end
        end

        field :is_spammy, Boolean, null: true, visibility: :internal, description: "Whether the blocking User or Organization was marked spammy."
        def is_spammy
          @object.get(:spammy) == "true"
        end
      end
    end
  end
end
