# frozen_string_literal: true

module Platform
  module Objects
    class RepositoryTopicStafftoolsInfo < Platform::Objects::Base
      description "Repository topic information only visible to site admins"

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        Platform::Objects::Base::SiteAdminCheck.viewer_is_site_admin?(permission.viewer, self.class.name)
      end

      visibility :internal

      minimum_accepted_scopes ["site_admin"]

      created_at_field

      field :is_applied, Boolean, description: "Indicates whether the topic has been applied to the repository.", null: false

      def is_applied
        @object.repo_topic.applied?
      end

      field :state, Enums::RepositoryTopicState, visibility: :internal, description: "Type of the association between the topic and the repository.", null: false

      def state
        @object.repo_topic.state
      end
    end
  end
end
