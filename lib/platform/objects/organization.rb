# frozen_string_literal: true

module Platform
  module Objects
    class Organization < Platform::Objects::Base
      include Objects::Base::RecordObjectAccess

      description "An account on GitHub, with one or more owners, that has repositories, members and teams."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      # Because of the way OAP is set up, we want to intentionally skip the
      # `access_allowed?` logic here and just return true.
      def self.async_api_can_access?(permission, org)
        true
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        if permission.authenticating_as_app?
          true
        else
          # See if the org should be hidden for being spammy
          !object.hide_from_user?(permission.viewer)
        end
      end

      minimum_accepted_scopes ["read:org"]

      implements Platform::Interfaces::Node
      implements Interfaces::Actor
      implements Interfaces::PackageOwner
      implements Interfaces::PackageSearch
      implements Interfaces::ProjectOwner
      implements Interfaces::RepositoryDiscussionAuthor
      implements Interfaces::RepositoryDiscussionCommentAuthor
      implements Interfaces::RepositoryOwner
      implements Interfaces::MarketplaceListingOwner
      implements Interfaces::UniformResourceLocatable
      implements Interfaces::Billable
      implements Interfaces::PlanOwner
      implements Interfaces::FeatureFlaggable
      implements Interfaces::MemberStatusable
      implements Interfaces::ProfileOwner
      implements Interfaces::Sponsorable

      global_id_field :id
      database_id_field

      url_fields description: "The HTTP URL for this organization." do |org|
        template = Addressable::Template.new("/{login}")
        template.expand login: org.login
      end

      url_fields prefix: :billing_settings, visibility: :under_development, description: "The HTTP URL for this organization's billing settings." do |org|
        template = Addressable::Template.new("/organizations/{login}/settings/billing")
        template.expand login: org.login
      end

      url_fields prefix: "announcements", visibility: :under_development, description: "The HTTP URL for organization announcements" do |org|
        template = Addressable::Template.new("/orgs/{login}/announcements")
        template.expand login: org.login
      end

      def available_seats
        @object.async_business.then do
          @object.available_seats
        end
      end

      field :is_at_seat_limit, Boolean, visibility: :internal, description: "Does this organization not have any available seats?", null: false, method: :at_seat_limit?

      field :oap_whitelist_required, Boolean, visibility: :internal, description: <<~DESCRIPTION, null: false do
          Whether an OAuth application listed in the Marketplace
          must be whitelisted before authorization.
        DESCRIPTION

        argument :listing_slug, String, "The slug of the MarketplaceListing", required: true
      end

      def oap_whitelist_required(**arguments)
        return false unless @object.restricts_oauth_applications?

        Loaders::ActiveRecord.load(::Marketplace::Listing, arguments[:listing_slug], column: :slug).then do |listing|
          if listing.listable_is_oauth_application?
            oauth_application = listing.async_listable.then do |oauth_application|
              !@object.allows_oauth_application?(oauth_application)
            end
          else
            false
          end
        end
      end

      field :audit_log, Connections.define(Unions::OrganizationAuditEntry),
        description: "Audit log entries of the organization",
        null: false,
        connection: true,
        areas_of_responsibility: :audit_log do
          argument :query, String, "The query string to filter audit entries", required: false
          argument :order_by, Inputs::AuditLogOrder, "Ordering options for the returned audit log entries.", required: false,
            default_value: { field: "timestamp", direction: "DESC" }
        end

      def audit_log(**arguments)
        raise Errors::PlanNotSupported.new("Organization billing plan does not support access to auditLog information.") unless @object.plan_supports?(:audit_log_api)

        viewer_can_see = @object.adminable_by?(@context[:viewer]) && @context[:permission].can_access_audit_log_for_organization?(@object)

        raise Errors::Forbidden.new("#{@context[:viewer]} does not have permission to retrieve auditLog information.") unless viewer_can_see

        # We override the index_name in the test environment because audit
        # entries are logged to `audit_log-test` locally or
        # [`audit_log-test`..`audit_log-test-15`] in CI. They do not follow
        # the date slicing logic used in production.
        index_name = "audit_log#{Elastomer.env.postfix}" if Rails.env.test?

        public_platform = false

        allowlist =
          if Platform::GlobalScope.origin_api?
            public_platform = true
            AuditLogEntry.public_platform_organization_action_names
          else
            AuditLogEntry.all_platform_organization_action_names
          end

        search_params = {
          current_user: @context[:viewer],
          direction: arguments[:order_by] && arguments[:order_by][:direction],
          index_name: index_name,
          org_id: @object.id,
          phrase: arguments[:query],
          allowlist: allowlist,
          limit_history: true,
          public_platform: public_platform,
        }

        # This will either return an Search::Queries::AuditLogQuery if feature flag is off
        # or an Audit::Driftwood::Query
        # However the results, for now, will always be from Search::Queries::AuditLogQuery
        # during the course of the experiment
        ::Audit::Driftwood::Query.new_org_business_query(search_params)
      end

      field :login, String, description: "The organization's login name.", null: false

      field :name, String, description: "The organization's public profile name.", null: true

      def name
        @object.async_profile.then do |profile|
          profile.nil? || profile.name.blank? ? @object.login : profile.name
        end
      end

      field :has_profile, Boolean, visibility: :internal, description: "Whether the organization has a public profile.", null: false

      def has_profile
        @object.async_profile.then do |profile|
          !!profile
        end
      end

      field :requires_two_factor_authentication, Boolean, description: "When true the organization requires all members, billing managers, and outside collaborators to enable two-factor authentication.", minimum_accepted_scopes: ["admin:org"], null: true

      def requires_two_factor_authentication
        org = @object
        if @context[:permission].can_view_two_factor_enabled?(org)
          org.two_factor_requirement_enabled?
        else
          raise Errors::Forbidden.new("#{@context[:viewer]} does not have the right permission to retrieve requiresTwoFactorAuthentication information")
        end
      end

      field :description, String, description: "The organization's public profile description.", null: true

      def description
        @object.async_profile.then do |profile|
          @object.profile_bio
        end
      end

      field :description_html, String, description: "The organization's public profile description rendered to HTML.", null: true

      def description_html
        @object.async_profile.then do |profile|
          GitHub::HTML::DescriptionPipeline.to_html(@object.profile_bio)
        end
      end

      field :company, String, visibility: :internal, description: "The organization's public profile company.", null: true

      def company
        @object.async_profile.then do |profile|
          @object.profile_company
        end
      end

      field :website_url, Scalars::URI, description: "The organization's public profile URL.", null: true

      def website_url
        @object.async_profile.then do |profile|
          @object.profile_blog
        end
      end

      field :twitter_username, String, description: "The organization's Twitter username.", null: true

      def twitter_username
        @object.async_profile.then do |profile|
          profile.try(:twitter_username)
        end
      end

      field :location, String, description: "The organization's public profile location.", null: true

      def location
        @object.async_profile.then do |profile|
          @object.profile_location
        end
      end

      field :email, String, description: "The organization's public email.", null: true

      def email
        @object.async_profile.then do |profile|
          @object.profile_email
        end
      end

      field :has_organization_projects_enabled, Boolean, visibility: :internal, method: :organization_projects_enabled?, batch_load_configuration: true, description: "Whether the organization has projects enabled.", null: false

      field :has_repository_projects_enabled, Boolean, visibility: :internal, method: :repository_projects_enabled?, batch_load_configuration: true, description: "Whether the organization has repository projects enabled.", null: false

      field :has_insights_enabled, Boolean, visibility: :internal, method: :insights_enabled?, description: "Whether the organization has insights enabled.", null: false

      created_at_field

      updated_at_field

      field :viewer_is_billing_manager_only, Boolean, visibility: :under_development, null: true,
        description: <<~DESCRIPTION
          Check if the viewer's only relation to this organization is as its billing manager.
        DESCRIPTION

      def viewer_is_billing_manager_only
        @object.billing_manager_only?(@context[:viewer])
      end

      field :organization_billing_email, String, description: "The billing email for the organization.", minimum_accepted_scopes: ["admin:org"], null: true

      def organization_billing_email
        accessible = @context[:permission].can_view_org_billing_email?(@object)

        return "" unless accessible

        Loaders::ActiveRecord.load(::Profile, @object.id, column: :user_id).then do |profile|
          current_viewer = @context[:viewer]
          current_integratable = @context[:oauth_app] || @context[:integration]

          if current_viewer.present? && current_viewer.owned_organizations.include?(@object)
            @object.billing_email
          elsif current_integratable.present?
            @object.async_adminable_account_has_purchased_app?(current_integratable).then do |result|
              result ? @object.billing_email : nil
            end
          end
        end
      end

      field :avatar_url, Scalars::URI, description: "A URL pointing to the organization's public avatar.", null: false do
        argument :size, Integer, "The size of the resulting square image.", required: false
      end

      def avatar_url(**arguments)
        @object.primary_avatar_url(arguments[:size])
      end

      url_fields prefix: :sso, description: "The HTTP URL for the organization's Single Sign-on prompt.", visibility: :internal do |org|
        template = Addressable::Template.new("/orgs/{org}/sso")
        template.expand org: org.login
      end

      url_fields prefix: :dismiss_notice, description: "The HTTP URL for dismissing organization notices.", visibility: :internal do |org|
        template = Addressable::Template.new("/orgs/{org}/dismiss_notice")
        template.expand org: org.login
      end

      url_fields prefix: :members, description: "The HTTP URL for the organization's member list.", visibility: :under_development do |org|
        template = Addressable::Template.new("/orgs/{org}/people")
        template.expand org: org.login
      end

      field :sso_url, Scalars::URI, visibility: :internal, description: "The URL endpoint for the organization's SAML IdP SSO.", null: true

      def sso_url
        Loaders::ActiveRecord.load(::Organization::SamlProvider, @object.id, column: :organization_id).then do |provider|
          provider.try(:sso_url)
        end
      end

      field :issuer, String, visibility: :internal, description: "A unique identifier for the Organization's SAML IdP", null: true

      def issuer
        Loaders::ActiveRecord.load(::Organization::SamlProvider, @object.id, column: :organization_id).then do |provider|
          provider.try(:issuer)
        end
      end

      field :idp_certificate, Scalars::X509Certificate, visibility: :internal, description: "The organization's IdP x509 certificate.", null: true

      def idp_certificate
        Loaders::ActiveRecord.load(::Organization::SamlProvider, @object.id, column: :organization_id).then do |provider|
          provider.try(:idp_certificate)
        end
      end

      # http://www.datypic.com/sc/ds/e-ds_SignatureMethod.html
      field :signature_method, Scalars::URI, visibility: :internal, description: "The signature algorithm used to sign SAML requests for an Organization's identity provider.", null: true

      def signature_method
        Loaders::ActiveRecord.load(::Organization::SamlProvider, @object.id, column: :organization_id).then do |provider|
          provider.try(:signature_method)
        end
      end

      # http://www.datypic.com/sc/ds/e-ds_DigestMethod.html
      field :digest_method, Scalars::URI, visibility: :internal, description: "The digest algorithm used to sign SAML requests for an Organization's identity provider.", null: true

      def digest_method
        Loaders::ActiveRecord.load(::Organization::SamlProvider, @object.id, column: :organization_id).then do |provider|
          provider.try(:digest_method)
        end
      end

      field :saml_identity_provider, Objects::OrganizationIdentityProvider, description: "The Organization's SAML identity providers", null: true
      def saml_identity_provider
        Promise.all([
          object.async_business,
          object.async_saml_provider,
        ]).then do |business, saml_provider|
          next saml_provider if saml_provider.nil? || business.nil?

          business.async_saml_provider.then do |business_saml_provider|
            if business_saml_provider
              raise Errors::Forbidden.new("The Organization's SAML identity provider is disabled when an Enterprise SAML identity provider is available.")
            end

            saml_provider
          end
        end
      end

      field :external_identities, Connections.define(Objects::ExternalIdentity), visibility: :internal, description: "External Identities of organization members provisioned by the Identity Provider in effect for the organization (either its own, or its Enterprise's)", minimum_accepted_scopes: ["admin:org"], null: false

      def external_identities
        # Currently restricted to integrations only (e.g. group-syncer)
        unless @context[:viewer] && @context[:viewer].try(:installation)
          return ::ExternalIdentity.none
        end
        Promise.all([
          @object.async_saml_provider,
          @object.async_business_saml_provider,
        ]).then do |org_provider, business_provider|
          if business_provider
            # SAML configured at business level => get business SAML identities for members of the org
            # Business SAML takes precedence over org SAML, so ignore the Org SAML config, if it's present
            business_provider.external_identities_for_organization(@object)
          elsif org_provider
            # SAML configured at org level, and the org either doesn't belong to a business,
            # or the business doesn't have SAML configured => org member identities
            ::ExternalIdentity.by_provider(org_provider)
          else
            ::ExternalIdentity.none
          end
        end
      end

      field :ip_allow_list_enabled_setting,
        Enums::IpAllowListEnabledSettingValue,
        description: "The setting value for whether the organization has an IP allow list enabled.",
        null: false

      def ip_allow_list_enabled_setting
        accessible = object.adminable_by?(context[:viewer]) &&
          context[:permission].can_view_ip_allow_list_settings?(object)
        unless accessible
          raise Errors::Forbidden.new \
            "#{context[:viewer]} does not have the right permission to retrieve ipAllowListEnabledSetting."
        end

        if object.ip_whitelisting_enabled?
          Enums::IpAllowListEnabledSettingValue.values["ENABLED"].value
        else
          Enums::IpAllowListEnabledSettingValue.values["DISABLED"].value
        end
      end

      field :ip_allow_list_entries, Connections.define(Objects::IpAllowListEntry),
        description: "The IP addresses that are allowed to access resources owned by the organization.",
        connection: true, null: false do
        argument :order_by, Inputs::IpAllowListEntryOrder,
          "Ordering options for IP allow list entries returned.",
          required: false, default_value: { field: "whitelisted_value", direction: "ASC" }
      end

      def ip_allow_list_entries(order_by: nil)
        accessible = object.adminable_by?(context[:viewer]) &&
          context[:permission].can_view_ip_allow_list_settings?(object)
        unless accessible
          raise Errors::Forbidden.new \
            "#{context[:viewer]} does not have the right permission to retrieve ipAllowListEntries."
        end

        Promise.all([object.async_business]).then do
          entries = ::IpWhitelistEntry.usable_for object

          unless order_by.nil?
            entries = entries.order "ip_whitelist_entries.#{order_by[:field]} #{order_by[:direction]}"
          end

          entries
        end
      end

      field :admin_info, Objects::OrganizationAdminInfo, description: "Organization information only visible to admin", null: true

      def admin_info
        if @context[:viewer] && @object.adminable_by?(@context[:viewer])
          Models::OrganizationAdminInfo.new(@object)
        else
          nil
        end
      end

      field :viewer_can_create_repositories, Boolean, description: "Viewer can create repositories on this organization", null: false

      def viewer_can_create_repositories
        @object.async_can_create_repository?(@context[:viewer])
      end

      field :viewer_can_create_teams, Boolean, description: "Viewer can create teams on this organization.", null: false

      def viewer_can_create_teams
        @object.can_create_team?(@context[:viewer])
      end

      field :viewer_is_a_member, Boolean, description: "Viewer is an active member of this organization.", null: false

      def viewer_is_a_member
        @object.direct_member?(@context[:viewer])
      end

      field :viewer_can_administer, Boolean, description: "Organization is adminable by the viewer.", null: false

      def viewer_can_administer
        @object.adminable_by?(@context[:viewer])
      end

      field :discussions, Connections.define(Objects::OrganizationDiscussion),
        numeric_pagination_enabled: true, description: "A list of organization discussions.",
        null: false, visibility: :under_development, areas_of_responsibility: :orgs do
          argument :is_pinned, Boolean,
            "If provided, filters discussions according to whether or not they are pinned.",
            required: false
        end

      def discussions(**arguments)
        posts = @object.discussion_posts

        unless @object.direct_member?(@context[:viewer]) || @object.adminable_by?(@context[:viewer])
          posts = posts.where(private: false)
        end

        unless arguments[:is_pinned].nil?
          condition = arguments[:is_pinned] ? "NOT NULL" : "NULL"
          posts = posts.where("organization_discussion_posts.pinned_at IS #{condition}")
        end

        posts.order("organization_discussion_posts.id DESC")
      end

      field :discussion, Objects::OrganizationDiscussion, visibility: :under_development,
        description: "Find an organization discussion by its number.", null: true do
          argument :number, Integer, "The sequence number of the discussion to find.",
            required: true
        end

      def discussion(number:)
        Platform::Loaders::OrganizationDiscussionByNumber.load(@object.id, number).then do |post|
          if post
            post.async_readable_by?(@context[:viewer]).then { |visible| post if visible }
          end
        end
      end

      field :has_team_discussions_enabled, Boolean, method: :async_team_discussions_allowed?, visibility: :internal, description: "Whether or not team discussions are enabled for this organization.", null: false

      field :stafftools_info, Objects::OrganizationStafftoolsInfo, visibility: :internal, description: "User information only visible to site admin", null: true

      def stafftools_info
        if self.class.viewer_is_site_admin?(context[:viewer], self.class.name)
          Models::AccountStafftoolsInfo.new(@object)
        else
          nil
        end
      end

      field :team, Objects::Team, description: "Find an organization's team by its slug.", null: true do
        argument :slug, String, "The name or slug of the team to find.", required: true
      end

      def team(**arguments)
        Loaders::TeamBySlug.load(@object, arguments[:slug]).then do |candidate_team|
          next unless candidate_team
          candidate_team.async_visible_to?(@context[:viewer]).then do |visible|
            candidate_team if visible
          end
        end
      end

      field :migration, Objects::Migration, description: "Fetch a migration by GUID", feature_flag: :gh_migrator_import_to_dotcom, null: true do
        argument :guid, String, "The GUID of the migration to load", required: true
      end

      def migration(**arguments)
        if @object.adminable_by?(@context[:viewer])
          ::Migration.where(owner_id: @object.id, guid: arguments[:guid]).first
        end
      end

      field :repositories_with_topic_manage_access, Connections::Repository, visibility: :internal, description: "A list of repositories owned by the organization to which the viewer has contributed and also has permission to manage the topics on.", null: false, connection: true

      def repositories_with_topic_manage_access
        viewer = @context[:viewer]
        unless viewer
          return ::Repository.where("false")
        end

        contribution_classes = Contribution::Collector::CONTRIBUTION_CLASSES_ASSOCIATED_WITH_REPOS
        contributions_collector = Contribution::Collector.new(
          user: viewer,
          time_range: 1.year.ago..Time.zone.now,
          contribution_classes: contribution_classes,
          viewer: viewer,
          excluded_organization_ids: @context[:unauthorized_organization_ids],
        )
        contributed_repo_ids = contributions_collector.visible_repository_ids
        adminable_repo_ids = viewer.associated_repository_ids(min_action: :admin)
        repo_ids = contributed_repo_ids & adminable_repo_ids

        @object.async_business.then do
          @object.visible_repositories_for(viewer).where(id: repo_ids).order(:name).filter_spam_and_disabled_for(viewer)
        end
      end

      field :admins, Connections::User, visibility: :internal, description: "A list of all of the admins for this organization", null: false, connection: true, exempt_from_spam_filter_check: true

      field :mannequins, Connections.define(Objects::Mannequin),
        visibility: :internal,
        description: "A list of all mannequins for this organization.",
        null: false,
        connection: true do
          argument :order_by, Inputs::MannequinOrder,
            "Ordering options for mannequins returned from the connection.",
            required: false,
            default_value: { field: "created_at", direction: "ASC" }
        end

      def mannequins(**arguments)
        scope = @object.mannequins.scoped
        order_by = arguments[:order_by]

        scope.order("users.#{order_by[:field]} #{order_by[:direction]}")
      end


      field :membersWithRole, resolver: Resolvers::OrganizationMembers, description: "A list of users who are members of this organization.", connection: true do
        argument :max_members_limit, Integer, visibility: :internal, description: "The maximum number of organization members to retrieve", required: false, default_value: 50000
      end

      field :teams, resolver: Resolvers::OrganizationTeams, description: "A list of teams in this organization.", connection: true

      field :eligible_parent_teams, Connections.define(Objects::Team), visibility: :internal, description: "List of eligible parent teams", null: false, connection: true do
        argument :query, String, "The query string to filter teams by name with", required: false
        argument :for_team, String, "Find eligible parents for this team", required: false
      end

      def eligible_parent_teams(**arguments)
        @object.async_team_sync_tenant.then do |team_sync_tenant|
          @object.async_teams.then do
            scope = @object.visible_teams_for(@context[:viewer]).closed.reorder("teams.name ASC")

            if team_sync_tenant&.enabled?
              team_ids = team_sync_tenant.team_group_mappings.distinct.pluck(:team_id)
              scope = scope.where.not(id: team_ids)
            end

            if slug = arguments[:for_team].presence
              if team = @object.visible_teams_for(@context[:viewer]).where(slug: slug).includes(:organization).first
                scope = scope.where("teams.id NOT IN (?)", [team.id] + team.descendant_ids)
              end
            end

            query = ActiveRecord::Base.sanitize_sql_like(arguments[:query].to_s.strip.downcase)

            if query.present?
              scope = scope.where(["name LIKE ? OR slug LIKE ?", "%#{query}%", "%#{query}%"])
            end

            scope
          end
        end
      end

      field :externally_managed_teams, Connections.define(Objects::Team), visibility: :internal, method: :async_externally_managed_teams, description: "The teams that have one or more external mappings.", null: false

      field :gists, resolver: Resolvers::Gists, visibility: :internal, description: "List of gists belonging to the organization.", connection: true

      url_fields prefix: :projects, description: "The HTTP URL listing organization's projects" do |org|
        template = Addressable::Template.new("/orgs/{login}/projects")
        template.expand login: org.login
      end

      field :pending_members, resolver: Resolvers::PendingOrganizationMembers,
        description: "A list of users who have been invited to join this organization.",
        connection: true

      field :pending_collaborators, Connections::PendingCollaborator, "A list of users with pending collaborator invitations for this organization.", null: false, visibility: :internal do
        argument :is_occupying_seat, Boolean, "Optionally filter to show pending collaborators with invitations that occupy a seat.", required: false
        argument :query, String, "Optional query to search collaborators by login.", required: false
      end

      def pending_collaborators(**arguments)
        if @object.adminable_by?(@context[:viewer]) || @object.billing_manager?(@context[:viewer]) || @context[:viewer].site_admin?
          scope = @object.pending_collaborators

          if arguments[:is_occupying_seat] == true
            scope = scope.where(id: @object.private_repo_non_collaborator_invitee_ids)
          end

          if arguments[:query].present?
            query = ActiveRecord::Base.sanitize_sql_like(arguments[:query].strip)
            scope = scope.where(["users.login LIKE :query", { query: "%#{query}%" }])
          end

          scope.filter_spam_for(@context[:viewer])
        else
          ArrayWrapper.new([])
        end
      end

      field :pending_collaborator_invitations, Connections.define(Objects::RepositoryInvitation, visibility: :internal, edge_type: GraphQL::Relay::EdgeType.create_type(Objects::RepositoryInvitation) { visibility :internal }), null: false, visibility: :internal do
        description "The pending collaborator invitations for this organization."
        argument :is_occupying_seat, Boolean, "Optionally filter to show pending collaborators invitations that occupy a seat.", required: false, default_value: false
        argument :user_id, ID, "The ID of the user to optionally filter invitations for.", required: false
      end

      def pending_collaborator_invitations(**arguments)
        if @object.adminable_by?(@context[:viewer]) || @context[:viewer].site_admin?
          scope = @object.repository_invitations.scoped

          if arguments[:is_occupying_seat] == true
            scope = scope.where(invitee_id: @object.private_repo_non_collaborator_invitee_ids).or(
              scope.where(email: @object.private_repo_non_user_invited_emails - @object.pending_invited_non_user_emails)
            )
          end

          if arguments[:user_id]
            user = Helpers::NodeIdentification.typed_object_from_id([Objects::User], arguments[:user_id], @context)
            scope = scope.where(invitee_id: user.id)
          end
          scope
        else
          ArrayWrapper.new([])
        end
      end

      field :integration_installations, Connections.define(Objects::IntegrationInstallation), "A list of the org's installed GitHub Apps", visibility: :internal, null: false, connection: true do
        argument :include_subscriptions, Boolean, "Whether to include installations associated with a Marketplace subscription", required: false, default_value: true
        argument :include_marketplace_categories, [String], "Only return installations for apps that belong to a Marketplace listing that belongs to one of the specified categories.", required: false, default_value: []
        argument :exclude_marketplace_categories, [String], "Only return installations for apps that do not belong to a Marketplace listing that belongs to one of the specified categories.", required: false, default_value: []
      end

      def integration_installations(**arguments)
        if arguments[:include_marketplace_categories].any? && arguments[:exclude_marketplace_categories].any?
          raise Platform::Errors::Unprocessable.new("Arguments includeMarketplaceCategories and excludeMarketplaceCategories are incompatible.")
        end

        # exclude installations for full trust apps
        installations = @object.integration_installations.third_party

        if !arguments[:include_subscriptions]
          installations = installations.where(subscription_item_id: nil)
        end

        if arguments[:include_marketplace_categories].any?
          installations = installations.
            joins("JOIN marketplace_listings ON integration_installations.integration_id=marketplace_listings.listable_id").
            joins("JOIN marketplace_categories_listings ON marketplace_categories_listings.marketplace_listing_id=marketplace_listings.id").
            joins("JOIN marketplace_categories ON (marketplace_categories.id=marketplace_categories_listings.marketplace_category_id AND marketplace_listings.listable_type = 'Integration')").
            merge(Marketplace::Listing.for_primary_category_only).
            where("marketplace_categories.slug IN (?)", arguments[:include_marketplace_categories])
        end

        if arguments[:exclude_marketplace_categories].any?
          installations = installations.
            joins("LEFT JOIN marketplace_listings ON integration_installations.integration_id=marketplace_listings.listable_id").
            joins("LEFT JOIN marketplace_categories_listings ON marketplace_categories_listings.marketplace_listing_id=marketplace_listings.id").
            joins("LEFT JOIN marketplace_categories ON (marketplace_categories.id=marketplace_categories_listings.marketplace_category_id AND marketplace_listings.listable_type = 'Integration')").
            where("marketplace_listings.id IS NULL OR marketplace_categories.slug NOT IN (?)", arguments[:exclude_marketplace_categories])
        end

        installations
      end

      field :organization_domains, Connections.define(Objects::OrganizationDomain), "A list of domains owned by the organization", visibility: :internal, null: true, connection: true do
        argument :is_verified, Boolean, "Filter by if the domain is verified.", required: false
      end

      def organization_domains(**arguments)
        if @object.direct_or_team_member?(context[:viewer]) || self.class.viewer_is_site_admin?(context[:viewer], self.class.name)
          scope = @object.organization_domains

          if arguments[:is_verified] == true
            scope = scope.verified
          elsif arguments[:is_verified] == false
            scope = scope.unverified
          end

          scope
        else
          ArrayWrapper.new([])
        end
      end

      field :verified_profile_domains, [String], description: "The list of verified domains to show on the organization profile page", visibility: :internal, null: false, method: :async_verified_profile_domains

      field :is_verified, Boolean, description: "Whether the organization has verified its profile email and website.", null: false, method: :async_is_verified? do
        visibility :public, environments: [:dotcom]
        visibility :internal, environments: [:enterprise]
      end

      field :viewer_can_receive_email_notifications, Boolean, description: "Whether the viewer is able to receive email notifications for this organization.", visibility: :internal, null: false

      def viewer_can_receive_email_notifications
        @object.async_user_can_receive_email_notifications?(context[:viewer])
      end

      field :viewer_has_verified_domain_notification_email, Boolean, description: "Whether the viewer has a verified notification email from a verified domain for this organization.", visibility: :internal, null: false

      def viewer_has_verified_domain_notification_email
        @object.async_user_has_verified_domain_notification_email?(context[:viewer])
      end

      field :notifiable_emails_for_viewer, [String], description: "Email addresses that can be used to send notifications from this organization to the viewer.", visibility: :internal, null: false

      def notifiable_emails_for_viewer
        @object.async_notifiable_emails_for(@context[:viewer]).then do |notifiable_emails|
          notifiable_emails.map { |email| email.email.downcase }
        end
      end

      field :has_notification_restrictions_enabled, Boolean, description: "Indicates if this organization has notification restrictions enabled.", visibility: :internal, null: false, method: :async_restrict_notifications_to_verified_domains?

      field :action_invocation_blocked, Boolean, "Indicates if action invocation is blocked for this organization", method: :action_invocation_blocked?, batch_load_configuration: true, null: false, visibility: :internal

      field :enterprise, Objects::Enterprise, description: "The organization's enterprise account", visibility: :under_development, minimum_accepted_scopes: ["admin:org"], null: true

      def enterprise
        return unless object.adminable_by?(context[:viewer])
        object.async_business
      end

      field :pending_enterprise_invitations, Connections.define(Objects::EnterpriseOrganizationInvitation), description: "A list of pending invitations for this organization to join an enterprise", minimum_accepted_scopes: ["admin:org"], null: true, connection: true do
        visibility :under_development
        argument :enterprise_ids, [ID], "Enterprise IDs to filter invitations by", required: false
      end

      def pending_enterprise_invitations(enterprise_ids: nil)
        return unless object.adminable_by?(context[:viewer])

        invitations = object.business_invitations.pending

        if enterprise_ids&.any?
          businesses = enterprise_ids.map do |id|
            Helpers::NodeIdentification.typed_object_from_id(
              [Objects::Enterprise], id, @context
            )
          end

          invitations = invitations.where(business: businesses)
        end

        invitations
      end

      field :terms_of_service_type, Enums::OrganizationTermsOfServiceType, description: "The terms of service this organization has agreed to.", null: false, visibility: :internal

      field :plan, Objects::Plan, visibility: :internal, null: false, description: "The GitHub::Plan for this Organization"

      def terms_of_service_type
        @object.terms_of_service.async_name
      end

      field :active_enterprise_cloud_trial, Boolean, description: "Whether the organization has an active Enterprise Cloud Trial", null: false, visibility: :internal

      def active_enterprise_cloud_trial
        Billing::EnterpriseCloudTrial.new(@object).active?
      end

      url_fields prefix: :new_project, visibility: :internal, description: "The HTTP URL to create new projects" do |org|
        template = Addressable::Template.new("/orgs/{login}/projects/new")
        template.expand login: org.login
      end

      url_fields prefix: :teams, description: "The HTTP URL listing organization's teams" do |org|
        template = Addressable::Template.new("/orgs/{login}/teams")
        template.expand login: org.login
      end

      url_fields prefix: :new_team, description: "The HTTP URL creating a new team" do |org|
        template = Addressable::Template.new("/orgs/{login}/new-team")
        template.expand login: org.login
      end

      url_fields prefix: :teams_toolbar_actions, description: "The HTTP URL for team toolbar actions", visibility: :internal do |org|
        template = Addressable::Template.new("/orgs/{login}/teams/toolbar_actions")
        template.expand login: org.login
      end

      url_fields prefix: :ldap_group_suggestions, description: "The HTTP URL for fetching LDAP Group suggestions for Team Import", visibility: :internal do |org|
        template = Addressable::Template.new("/orgs/{login}/teams/group_suggestions")
        template.expand login: org.login
      end

      url_fields prefix: :import_organization, description: "The HTTP URL for importing LDAP Groups into an organization", visibility: :internal do |org|
        template = Addressable::Template.new("/organizations/{login}/import")
        template.expand login: org.login
      end

      url_fields prefix: :dismiss_teams_banner, description: "The HTTP URL for dismissing the teams banner", visibility: :internal do |org|
        template = Addressable::Template.new("/orgs/{login}/teams/dismiss_teams_banner")
        template.expand login: org.login
      end

      url_fields prefix: :insights, description: "The HTTP URL for insights", visibility: :internal do |org|
        template = Addressable::Template.new("/orgs/{login}/insights")
        template.expand login: org.login
      end
    end
  end
end
