# frozen_string_literal: true

module Platform
  module Objects
    class RepositoryAdvisoryComment < Platform::Objects::Base
      description "A comment on a repository security advisory"
      areas_of_responsibility :security_advisories
      feature_flag :pe_mobile
      minimum_accepted_scopes ["public_repo"]

      implements Platform::Interfaces::Node
      implements Interfaces::Comment
      implements Interfaces::Deletable
      implements Interfaces::MayBeInternal
      implements Interfaces::Reactable
      implements Interfaces::UniformResourceLocatable
      implements Interfaces::Updatable
      implements Interfaces::UpdatableComment

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, comment)
        # this check is only here for SAML enforcement (side
        # effect of `access_allowed` via `current_org`).
        permission.async_repo_and_org_owner(comment).then do |repo, org|
          permission.access_allowed?(
            :saml_via_graphql, # returns false
            resource: comment,
            current_org: org,
            current_repo: repo,
            allow_integrations: false,
            allow_user_via_integration: false)
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, comment)
        comment.async_repository_advisory.then do |advisory|
          permission.typed_can_see?("RepositoryAdvisory", advisory) &&
            comment.readable_by?(permission.viewer)
        end
      end

      global_id_field :id
      database_id_field

      # Interfaces::Comment

      def authored_by_subject_author
        @object.async_repository_advisory.then do |advisory|
          advisory.author_id == @object.user_id
        end
      end

      def subject_type
        "advisory"
      end

      # Interfaces::MayBeInternal

      def is_internal
        object.async_internal?
      end
    end
  end
end
