# frozen_string_literal: true

module Platform
  module Objects
    class ExperimentMismatchCandidateValue < Platform::Objects::Base
      description "Represents a candidate value for a code experiment mismatch."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        ::Experiment.viewer_can_read?(permission.viewer)
      end

      visibility :internal

      minimum_accepted_scopes ["devtools"]

      field :name, String, "The name for this experiment mismatch candidate value.", null: false
      field :value, String, "The value for this experiment mismatch candidate value.", null: false
      field :is_truncated, Boolean, "Indicates if the value has been truncated or not.", null: false
      field :exception, String, "The exception this experiment mismatch candidate value threw.", null: true
    end
  end
end
