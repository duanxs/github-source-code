# frozen_string_literal: true

module Platform
  module Objects
    class SponsorsListingAdminInfo < Platform::Objects::Base
      description "SponsorsListing information only visible to users that can administrate the listing."

      def self.async_api_can_access?(permission, _object)
        permission.hidden_from_public?(self)
      end

      def self.async_viewer_can_see?(permission, object)
        return Promise.resolve(false) unless permission.viewer.present?
        return Promise.resolve(true) if permission.viewer.can_admin_sponsors_listings?

        object.listing.async_adminable_by?(permission.viewer)
      end

      visibility :under_development
      minimum_accepted_scopes ["user"]

      field :tiers, Connections.define(Objects::SponsorsTier), connection: true, description: "A list of tiers for this GitHub Sponsors listing.", null: true do
        argument :states, [Enums::SponsorsTierState],
          "Select tiers based on states", required: false, visibility: :internal
        argument :order_by, Inputs::SponsorsTierOrder,
          "Ordering options for Sponsors tiers returned from the connection.", required: false,
          default_value: { field: "monthly_price_in_cents", direction: "ASC" }
      end

      def tiers(states: nil, order_by:)
        tiers = @object.listing.sponsors_tiers
        tiers = tiers.with_states(*states) if states

        tiers.order("sponsors_tiers.#{order_by[:field]} #{order_by[:direction]}")
      end
    end
  end
end
