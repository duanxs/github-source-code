# frozen_string_literal: true

module Platform
  module Objects
    class ComposableComment < Platform::Objects::Base
      areas_of_responsibility :ce_extensibility

      description "Represents a composable comment on an issue or pull request which contains components."

      visibility :under_development

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, composable_comment)
        composable_comment.async_issue.then do |issue|
          issue.async_pull_request.then do
            permission.async_repo_and_org_owner(composable_comment).then do |repo, org|
              # Permission to view composable comment is inherited
              # from permission to view issue comment
              access_type = issue.pull_request? ? :get_pull_request_comment : :get_issue_comment
              permission.access_allowed?(
                access_type,
                repo: repo,
                resource: composable_comment,
                current_org: org,
                # Todo: Ultimately we probably do want apps to be able to access this, but
                # it's likely that we want to show them a subset of the information
                allow_integrations: false,
                allow_user_via_integration: false,
              )
            end
          end
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        permission.belongs_to_issue(object)
      end

      scopeless_tokens_as_minimum

      implements Platform::Interfaces::Node
      implements Interfaces::RepositoryNode
      implements Interfaces::InteractiveComponentContainer

      global_id_field :id

      database_id_field

      field :issue_or_pull_request, Unions::IssueOrPullRequest, description: "Identifies the issue or pull request associated with the composable comment.", null: false

      def issue_or_pull_request
        @object.async_issue.then do |issue|
          if issue.pull_request?
            issue.async_pull_request
          else
            issue
          end
        end
      end

      field :repository, Repository, description: "The composable comment's parent repository.", method: :async_repository, null: false

      field :app, App, description: "The GitHub App that created this composable comment.", method: :async_integration, null: true

      field :author, description: "The GitHub App's bot user who authored the composable comment.", resolver: Resolvers::ActorUser, null: true, visibility: :internal

      # There is no explicit orderBy argument on :components, because there is only ever
      # one order in which we want to return these components: In the same order in which
      # they were specified by the GitHub App that created them
      field :components, Connections.define(Unions::Component), description: "The composable comment's components", null: true, connection: true

      def components
        object.async_interactive_components.then do |interactive_components|
          object.async_markdown_components.then do |markdown_components|
            ArrayWrapper.new(object.components)
          end
        end
      end

      field :websocket_channel, String, visibility: :internal, description: "The websocket channel on which composable comment updates are published", null: false

      created_at_field
      updated_at_field
    end
  end
end
