# frozen_string_literal: true

module Platform
  module Objects
    class NotificationSettings < Platform::Objects::Base
      description "The viewer's notification settings"

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _)
        permission.access_allowed?(
          :read_user_notification_settings,
          resource: permission.viewer,
          current_repo: nil,
          current_org: nil,
          allow_integrations: false,
          allow_user_via_integration: false,
        )
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        # `object` here is an instance of `Newsies::Settings`, so `object.id` is actually a User ID.
        permission.viewer&.id == object.id
      end

      minimum_accepted_scopes ["user"]
      feature_flag :pe_mobile
      areas_of_responsibility :notifications

      field :gets_participating_web,
        Boolean,
        description: "Does the viewer get web notifications for threads in which they are participating?",
        method: :participating_web?,
        null: false

      field :gets_participating_email,
        Boolean,
        description: "Does the viewer get email notifications for threads in which they are participating?",
        method: :participating_email?,
        null: false

      field :gets_watching_web,
        Boolean,
        description: "Does the viewer get web notifications for threads which they are watching or to which they are subscribed?",
        method: :subscribed_web?,
        null: false

      field :gets_watching_email,
        Boolean,
        description: "Does the viewer get email notifications for threads which they are watching or to which they are subscribed?",
        method: :subscribed_email?,
        null: false

      field :gets_direct_mention_mobile_push,
        Boolean,
        description: "Does the viewer get mobile push notifications for comments in which they are directly mentioned?",
        method: :direct_mention_mobile_push?,
        null: false

      field :gets_vulnerability_alerts_web,
        Boolean,
        description: "Does the viewer get web notifications for vulnerability alerts?",
        method: :vulnerability_web?,
        null: false

      field :enabled_ci_notifications,
        Boolean,
        description: "Did the view enable notifications for continuous integration activity?",
        method: :continuous_integration_enabled?,
        null: false

      field :organization_email_routes,
        Connections.define(Objects::EmailRoute),
        description: "The notification routing settings for this user's organizations.",
        connection: true,
        null: false

      def organization_email_routes
        @object.async_user.then do |user|
          settings = user.affiliated_organizations.sort_by(&:name).map do |organization|
            Models::EmailRoute.new(organization, @object)
          end

          ArrayWrapper.new(settings)
        end
      end

      field :notifiable_emails,
        [String],
        description: "The email addresses belonging to this user that can receive notifications.",
        null: false

      def notifiable_emails
        @object.async_user.then do |user|
          user.notifiable_emails.map(&:downcase)
        end
      end

      field :organizations_without_eligible_notification_email,
        Connections.define(Objects::Organization),
        description: "The organizations that this user is a member of that they do not have an eligible notification email for.",
        connection: true,
        null: false,
        visibility: :internal

      def organizations_without_eligible_notification_email
        return ArrayWrapper.new([]) unless GitHub.domain_verification_enabled?

        object.async_user.then do |user|
          org_promises = user.organizations.map { |org| async_load_org_notification_eligibility(user, org) }

          Promise.all(org_promises).then do |results|
            orgs = results.select(&:ineligible).map(&:organization)
            ArrayWrapper.new(orgs)
          end
        end
      end

      private

      def async_load_org_notification_eligibility(user, org)
        org.async_restrict_notifications_to_verified_domains?.then do |restriction_enabled|
          next OpenStruct.new(organization: org, ineligible: false) unless restriction_enabled

          org.async_user_has_verified_domain_notification_email?(user).then do |has_verified_email|
            OpenStruct.new(organization: org, ineligible: !has_verified_email)
          end
        end
      end
    end
  end
end
