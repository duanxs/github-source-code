# frozen_string_literal: true

module Platform
  module Objects
    class StripeAccountDetails < Platform::Objects::Base
      model_name "Stripe::Account"
      minimum_accepted_scopes ["user"]
      visibility :internal

      description "Account details associated with a Stripe Connect account"

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, sponsorship)
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        true
      end

      field :disabled_reason, String, "The reason why the account can’t create charges or receive payouts",
        visibility: :internal, null: true

      def disabled_reason
        if reason = @object.requirements.disabled_reason
          reason unless reason.empty?
        end
      end

      field :currently_due, [String], "The fields that need to be collected to keep the account enabled",
        visibility: :internal, null: false

      def currently_due
        @object.requirements.currently_due
      end

      field :current_deadline, Scalars::DateTime, "The date the fields in currently_due must be collected by to keep payouts enabled for the account",
        visibility: :internal, null: true

      def current_deadline
        return unless deadline = @object.requirements.current_deadline
        Time.at(deadline).iso8601
      end

      field :past_due, [String], "The fields that weren’t collected by the current_deadline. These fields need to be collected to re-enable the account",
        visibility: :internal, null: false

      def past_due
        @object.requirements.past_due
      end

      field :eventually_due, [String], "The fields that need to be collected. As they become required, these fields appear in currently_due",
        visibility: :internal, null: false

      def eventually_due
        @object.requirements.eventually_due
      end

      field :payouts_enabled, Boolean, "Whether Stripe can send payouts to this account.",
        visibility: :internal, null: false

      field :has_payout_option, Boolean, "Whether or not a payout option exists",
        visibility: :internal, null: true

      def has_payout_option
        @object.external_accounts.data.any?
      end

      field :billing_country, String, "The billing country where payouts are sent for this account",
        visibility: :internal, null: true

      def billing_country
        return unless country = @object.external_accounts.first&.country
        Braintree::Address::CountryNames.select { |(_, alpha2 , _, _)|
          alpha2.downcase == country.downcase
        }.flatten.first
      end

      field :has_automated_payouts_disabled, Boolean,
        description: "Indicates if automatic payouts are disabled for this account",
        visibility: :internal,
        null: false

      def has_automated_payouts_disabled
        @object.settings.payouts.schedule.interval == "manual"
      end
    end
  end
end
