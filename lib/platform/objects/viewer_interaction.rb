# frozen_string_literal: true

module Platform
  module Objects
    class ViewerInteraction < Platform::Objects::Base
      areas_of_responsibility :ce_extensibility

      scopeless_tokens_as_minimum

      def self.async_api_can_access?(permission, _object)
        # This is a simple struct to contain information for use in other objects
        permission.hidden_from_public?(self)
      end

      def self.async_viewer_can_see?(permission, _object)
        # This is a simple struct to contain information for use in other objects
        permission.hidden_from_public?(self)
      end

      description "A struct that denotes whether the object is viewable, otherwise contains an error explanaton"

      visibility :under_development

      field :can_interact, Boolean, "A boolean variable that denotes whether a view can interact with the object", null: false

      field :error, String, "An error explanation that explains why the viewer cannot interact, often shown in views", null: true
    end
  end
end
