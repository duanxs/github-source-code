# frozen_string_literal: true

module Platform
  module Objects
    class OrganizationAdminInfo < Platform::Objects::Base
      description "Organization information only visible to members"

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        # See if user is an admin of the org
        object.org.adminable_by?(permission.viewer)
      end

      visibility :internal

      minimum_accepted_scopes ["read:org"]

      field :members_can_create_repositories, Boolean, description: "ability for organization org members to create repositories", null: false

      def members_can_create_repositories
        @object.org.members_can_create_repositories?
      end

      field :members_can_create_public_repositories, Boolean, description: "ability for organization org members to create public repositories", null: false

      def members_can_create_public_repositories
        @object.org.members_can_create_public_repositories?
      end

      field :default_repository_permission, Enums::DefaultRepositoryPermissionField, description: "default repository permission for organization members", null: false

      def default_repository_permission
        Platform::Loaders::Configuration.load(@object.org, :default_repository_permission_name)
      end

      field :invitations, Connections.define(Objects::OrganizationInvitation), description: "A list of pending invitations for users to this organization", null: true, connection: true

      def invitations
        @object.org.pending_invitations.scoped
      end

      field :outside_collaborators, Connections.define(Objects::User), description: "A list of users who are outside collaborators of this organization.", null: true, connection: true

      def outside_collaborators
        @object.org.outside_collaborators.scoped.filter_spam_for(@context[:viewer])
      end

      field :interaction_ability, Objects::RepositoryInteractionAbility, "The interaction ability settings for this organization.", null: false

      def interaction_ability
        Platform::Models::RepositoryInteractionAbility.new(:organization, @object.org)
      end

      field :has_notification_delivery_restriction_enabled, Boolean, description: "Indicates if email notification delivery for this organization is restricted to verified domains.", null: false

      def has_notification_delivery_restriction_enabled
        @object.org.async_restrict_notifications_to_verified_domains?
      end

      field :members_without_verified_notification_email, Connections.define(Objects::User), description: "Members who do not have a notification email that matches a verified domain for this organization.", null: false, connection: true

      def members_without_verified_notification_email
        @object.org.async_members_without_verified_domain_email.then do |members|
          members.filter_spam_for(context[:viewer])
        end
      end

      field :enterprise_server_installations, Connections.define(Objects::EnterpriseServerInstallation),
        description: "Enterprise Server installations owned by the business.",
        visibility: {
          internal: { environments: [:enterprise] },
          under_development: { environments: [:dotcom] },
        },
        connection: true, null: false do

        argument :order_by, Inputs::EnterpriseServerInstallationOrder,
          "Ordering options for Enterprise Server installations returned.",
          required: false, default_value: { field: "host_name", direction: "ASC" }
      end

      def enterprise_server_installations(order_by: nil)
        installations = object.org.enterprise_installations
        unless order_by.nil?
          installations = installations.order "enterprise_installations.#{order_by[:field]} #{order_by[:direction]}"
        end
        installations
      end
    end
  end
end
