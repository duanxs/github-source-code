# frozen_string_literal: true

module Platform
  module Objects
    class CommunityContributorsCollection < Platform::Objects::Base
      description "A community contributor collection aggregates contributors based on a repositories direct or transitive dependencies."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # No special API permissions
        true
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        object.viewer == permission.viewer
      end

      areas_of_responsibility :dependency_graph

      scopeless_tokens_as_minimum

      # Users can specify the maximum number of contributors to fetch
      MAX_CONTRIBUTORS = 50
      DEFAULT_OFFSET = 0

      field :direct_contributors, [User], description: "List of users who have made contributions to a repository through its direct dependencies", null: false do
        argument :offset, Int, description: "The direct community contributors offset", required: false, default_value: DEFAULT_OFFSET
        argument :limit, Int, description: "The max number of direct community contributors to return", required: false, default_value: MAX_CONTRIBUTORS
      end

      field :transitive_contributors, [User], description: "List of users who have made contributions to a repository through its transitive dependencies", null: false do
        argument :offset, Int, description: "The transitive community contributors offset", required: false, default_value: DEFAULT_OFFSET
        argument :limit, Int, description: "The max number of transitive community contributors to return", required: false, default_value: MAX_CONTRIBUTORS
      end

      field :direct_contributors_count, Int, description: "Total unique count of a repositories direct community contributors", null: false
      field :transitive_contributors_count, Int, description: "Total unique count of a repositories transitive community contributors", null: false

      def direct_contributors(limit:, offset:)
        get_unique_contributors(ids: @object.direct_dependency_ids, limit: limit, offset: offset)
      end

      def direct_contributors_count
        get_unique_contributors_count(@object.direct_dependency_ids)
      end

      def transitive_contributors(limit:, offset:)
        get_unique_contributors(ids: @object.transitive_dependency_ids, limit: limit, offset: offset)
      end

      def transitive_contributors_count
        get_unique_contributors_count(@object.transitive_dependency_ids)
      end

      private

      def get_unique_contributors(ids:, limit:, offset:)
        return [] unless ids.any?

        result = CommitContribution.connection.exec_query(<<-EOQ)
          SELECT user_id, count(*) as contributions_count FROM commit_contributions
          WHERE repository_id IN  (#{ids.join(',')})
          GROUP BY user_id
          ORDER BY contributions_count DESC
          LIMIT #{limit} OFFSET #{offset}
        EOQ

        users = ActiveRecord::Base.connected_to(role: :reading) do
          ::User.where(id: result.pluck("user_id")).filter_spam_for(@context[:viewer])
        end

        users.reject { |user| user.blocked_by?(@context[:viewer]) }
      end

      def get_unique_contributors_count(ids)
        ::ActiveRecord::Base.connected_to(role: :reading) do
          ::CommitContribution.for_repository(ids).no_ghost_users.count("DISTINCT(user_id)")
        end
      end
    end
  end
end
