# frozen_string_literal: true

module Platform
  module Objects
    class MobilePushNotificationSchedule < Platform::Objects::Base
      model_name "Newsies::MobilePushNotificationSchedule"
      description "A push notifications schedule for a user."

      areas_of_responsibility :notifications
      minimum_accepted_scopes ["user"]
      feature_flag :pe_mobile

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      #
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _)
        permission.access_allowed?(
          :read_user_notification_settings,
          resource: permission.viewer,
          current_repo: nil,
          current_org: nil,
          allow_integrations: false,
          allow_user_via_integration: false,
        )
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        # we only want to allow users to view their own schedules for now
        object.user_id == permission.viewer&.id
      end

      implements Platform::Interfaces::Node

      global_id_field :id

      field :day, Enums::DayOfWeek, description: "The schedule day.", null: false
      field :starts_at, Scalars::DateTime, description: "The schedule start time.", null: false, method: :start_time
      field :ends_at, Scalars::DateTime, description: "The schedule end time.", null: false, method: :end_time
      field :time_zone, String, description: "The user's time zone name", null: false

      def time_zone
        @context[:viewer].async_mobile_time_zone.then(&:name)
      end
    end
  end
end
