# frozen_string_literal: true

module Platform
  module Objects
    class FeatureActorRepository < Platform::Objects::Base
      description <<~MD
        Represents a repository that is a feature actor. It provides a smaller
        number of fields than Repository in order to maintain privacy.
      MD

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, *)
        permission.viewer && (permission.viewer.site_admin? || permission.viewer.github_developer?)
      end

      visibility :internal

      minimum_accepted_scopes ["devtools"]

      implements Platform::Interfaces::Node
      implements Interfaces::FeatureActor

      global_id_field :id

      field :name_with_owner, String, description: "The name of the repository", null: false

      field :is_private, Boolean, "Identifies if the repository is private.", method: :private?, null: false

      url_fields description: "The HTTP URL for this repository's stafftools page", prefix: "stafftools" do |repository|
        Loaders::ActiveRecord.load(::User, repository.owner_id).then do |user|
          template = Addressable::Template.new("/stafftools/repositories/{user}/{repo}")
          template.expand user: user.login, repo: repository.name
        end
      end

      def self.load_from_global_id(id)
        Loaders::ActiveRecord.load(::Repository, id.to_i, security_violation_behaviour: :nil).then do |repository|
          Models::FeatureActorRepository.new(repository)
        end
      end
    end
  end
end
