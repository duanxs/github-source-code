# frozen_string_literal: true

module Platform
  module Objects
    class MobileDeviceToken < Platform::Objects::Base
      model_name "Newsies::MobileDeviceToken"
      description "A device token used to address push notifications to a user."
      minimum_accepted_scopes ["site_admin"]
      visibility :under_development
      areas_of_responsibility :notifications

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      #
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # Only allow access from queries against the internal schema.
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      #
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        viewer_is_site_admin?(permission.viewer, self.name)
      end

      field :service, Enums::PushNotificationService, description: "The push notification service that issued the device token.", null: false
      field :device_token, String, description: "The device token.", null: false
      field :device_name, String, description: "The name of the device with which this token is associated.", null: true
      created_at_field
      updated_at_field
    end
  end
end
