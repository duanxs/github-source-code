# frozen_string_literal: true

module Platform
  module Objects
    class DependencyGraphDependency < Platform::Objects::Base
      description "A dependency manifest entry"

      areas_of_responsibility :dependency_graph

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # This is public on dotcom, but internal on enterprise.
        true # rubocop:disable GitHub/GraphqlApiAuthorization
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        true
      end

      minimum_accepted_scopes ["public_repo"]

      visibility :public, environments: [:dotcom]
      visibility :internal, environments: [:enterprise]

      field :package_id, ID, "The dependency graph package ID", null: true, visibility: :internal

      field :package_name, String, "The required package name", null: false
      field :package_label, String, "The package label defined in the manifest", null: true, visibility: :internal

      field :package_manager, String, "The dependency package manager", null: true

      field :requirements, String, "The dependency version requirements", null: false

      field :human_requirements, String, "More readable dependency version requirements", null: false, visibility: :internal

      field :has_dependencies, Boolean, "Does the dependency itself have dependencies?", method: :has_dependencies?, null: false

      field :repository, Objects::Repository, description: "The repository containing the package", null: true

      def repository
        Loaders::ActiveRecord.load(::Repository, @object.repository_id, security_violation_behaviour: :nil)
      end

      field :vulnerability_alerts, [Platform::Objects::RepositoryVulnerabilityAlert],  "A list of vulnerability alerts for the manifest", null: false, visibility: :internal
      def vulnerability_alerts
        # Manifests have dependnecies, but so do PackageReleases (i.e. our dependencies also have sub dependencies)
        # We only show vulnerability alerts on manifest's dependencies, not on sub dependencies
        # So, we make sure that our parent is a manifest and then find vulnerability alerts that affect us through it
        # If/when we ever want to surface sub-dependency alerts this logic will have to change, but along with it so will our data model
        if object.manifest.blank?
          return []
        end

        # We can now get the repository and filename from our parent manifest to find if any alerts affect us.
        filename = object.manifest.filename
        object.manifest.async_repository.then do |repo|
          if repo.vulnerability_alerts_visible_to?(@context[:viewer])
            repo.applicable_vulnerability_alerts(manifest_path: filename, name: @object.package_name)
          else
            []
          end
        end
      end
    end
  end
end
