# frozen_string_literal: true

module Platform
  module Objects
    class RepositoryStafftoolsInfo < Platform::Objects::Base
      description "Repository information only visible to site admins"

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        Platform::Objects::Base::SiteAdminCheck.viewer_is_site_admin?(permission.viewer, self.class.name)
      end

      visibility :internal

      minimum_accepted_scopes ["site_admin"]

      field :repository_topics, Connections.define(Objects::RepositoryTopic), description: "A list of all (applied and declined) repository-topic associations for this repository.", null: false, connection: true

      def repository_topics
        @object.repo.repository_topics.scoped
      end

      field :can_be_recommended, Boolean, description: <<~DESCRIPTION, null: false do
          Returns true if this repository is currently allowed to be recommended to users.
        DESCRIPTION
      end

      def can_be_recommended
        recommendation = ::RepositoryRecommendation.new(repository: @object.repo)
        recommendation.can_be_recommended?
      end

      field :is_owner_hidden, Boolean, description: "Returns true if the owner of this repository is hidden.", null: false

      def is_owner_hidden
        @object.repo.user_hidden?
      end

      field :suggested_topics, [Topic, null: true], description: "A complete list of machine learning-suggested topics (including flagged topics) for this repository.", null: false

      def suggested_topics
        topics = GitHub.munger.topics_for_repository(@object.repo)
        if topics
          ::Promise.all(topics.sort_by(&:suggestion_score).reverse.map do |topic|
            Objects::Topic.load_from_global_id(topic.name)
          end)
        else
          []
        end
      end

      field :networkLfsDiskUsage, Integer, description: "The amount in bytes of LFS storage used by this repository's network.", null: false

      def network_lfs_disk_usage
        Loaders::NetworkLfsDiskUsage.load(@object.repo.network_id)
      end

      field :interaction_ability, Objects::RepositoryInteractionAbility, "The interaction ability settings for this repository.", null: false

      def interaction_ability
        Platform::Models::RepositoryInteractionAbility.new(:repository, @object.repo)
      end
    end
  end
end
