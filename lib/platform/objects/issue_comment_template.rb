# frozen_string_literal: true

module Platform
  module Objects
    class IssueCommentTemplate < Platform::Objects::Base
      description "A repository issue comment template."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, template)
        permission.async_repo_and_org_owner(template).then do |repo, org|
          permission.access_allowed?(
            :list_issue_comment_templates,
            resource: repo,
            current_repo: repo,
            current_org: org,
            allow_integrations: true,
            allow_user_via_integration: true,
          )
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        permission.belongs_to_repository(object)
      end

      areas_of_responsibility :ce_extensibility

      visibility :under_development

      feature_flag :structured_issue_comment_templates

      minimum_accepted_scopes ["repo"]

      field :filename, String, "The template filename.", null: false
      field :name, String, "The template name.", null: true
      field :about, String, "The template purpose.", null: true
      field :body, String, "The suggested issue body.", null: true
    end
  end
end
