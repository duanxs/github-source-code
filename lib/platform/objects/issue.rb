# frozen_string_literal: true

module Platform
  module Objects
    class Issue < Platform::Objects::Base
      description "An Issue is a place to discuss ideas, enhancements, tasks, and bugs for a project."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, issue)
        permission.async_repo_and_org_owner(issue).then do |repo, org|
          issue.async_pull_request.then do |pull|
            permission.access_allowed?(:show_issue,
              resource: issue,
              repo: repo,
              current_org: org,
              allow_integrations: true,
              allow_user_via_integration: true,
              # Allow issues on public repos to be returned even when the current GitHub app isn't installed on the repo
              approved_integration_required: false,
            )
          end
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        return false if object.hide_from_user?(permission.viewer)
        permission.load_repo_and_owner(object).then do
          object.readable_by?(permission.viewer)
        end
      end

      scopeless_tokens_as_minimum

      implements Platform::Interfaces::Node
      implements Interfaces::Assignable
      implements Interfaces::Closable
      implements Interfaces::Comment
      implements Interfaces::Commentable
      implements Interfaces::Updatable
      implements Interfaces::UpdatableComment
      implements Interfaces::Labelable
      implements Interfaces::Lockable
      implements Interfaces::PerformableViaApp
      implements Interfaces::Reactable
      implements Interfaces::Reportable
      implements Interfaces::RepositoryNode
      implements Interfaces::Subscribable
      implements Interfaces::UniformResourceLocatable
      implements Interfaces::Blockable
      implements Interfaces::AbuseReportable
      implements Interfaces::Trigger

      global_id_field :id

      url_fields description: "The HTTP URL for this issue" do |issue|
        issue.async_path_uri
      end

      database_id_field
      field :number, Integer, "Identifies the issue number.", null: false
      field :title, String, "Identifies the issue title.", null: false
      field :state, Enums::IssueState, "Identifies the state of the issue.", null: false
      field :milestone, Milestone, method: :async_milestone, description: "Identifies the milestone associated with the issue.", null: true
      field :body, String, description: "Identifies the body of the issue.", null: false

      def body
        @object.body || ""
      end

      field :body_version, String, visibility: :internal, description: "Identifies the comment body hash.", null: false

      field :body_text, String, description: "Identifies the body of the issue rendered to text.", null: false

      def body_text
        @object.async_body_text.then do |body_text|
          body_text || GitHub::HTMLSafeString::EMPTY
        end
      end

      field :body_html, Scalars::HTML, description: "The body rendered to HTML.", null: false do
        argument :hide_code_blobs, Boolean, "Whether or not to include the HTML for code blobs", required: false, default_value: false, feature_flag: :pe_mobile
        argument :render_suggested_changes_as_text, Boolean, "Whether or not to include the HTML for suggested changes", required: false, default_value: false, feature_flag: :pe_mobile
      end

      def body_html(hide_code_blobs: false, render_suggested_changes_as_text: false)
        @object.async_repository.then do |repo|
          repo.async_owner.then do
            repo.async_network.then do
              if hide_code_blobs
                @object.async_body_html!(context: {hide_code_blobs: hide_code_blobs}).then do |body_html|
                  body_html || GitHub::HTMLSafeString::EMPTY
                end
              else
                @object.async_body_html.then do |body_html|
                  body_html || GitHub::HTMLSafeString::EMPTY
                end
              end
            end
          end
        end
      end

      url_fields prefix: :body, description: "The http URL for this issue body", feature_flag: :pe_mobile do |issue|
        issue.async_path_body_uri
      end

      field :closed_by, Interfaces::Actor, visibility: :internal, method: :async_closed_by, description: "The actor who closed the issue.", null: true

      field :task_list_item_count, Integer, visibility: :internal, description: "Number of tasks in the issue's task list", null: false do
        argument :statuses, [Enums::TaskListItemStatus, null: true], "Limit the count to tasks in the specified statuses.", required: false
      end

      def task_list_item_count(**arguments)
        @object.async_task_list_item_count(*arguments[:statuses])
      end

      field :task_list_summary, Objects::TaskListSummary, null: true,
        description: "A summary of this issue's task list.", visibility: :under_development

      field :comment, Objects::IssueComment,
          description: "Find a particular comment on this issue.",
          visibility: :under_development, null: true do
        argument :database_id, Int, "Look up comment by its database ID.", required: true
      end

      def comment(database_id:)
        if context[:permission].typed_can_access?("Issue", @object)
          @object.comments.filter_spam_for(@context[:viewer]).where(id: database_id).first
        end
      end

      field :comments, resolver: Resolvers::IssueComments, description: "A list of comments associated with the Issue.", numeric_pagination_enabled: true

      field :total_comments_count, Integer, method: :issue_comments_count, null: true,
        description: "Returns a count of how many comments this issue has received.",
        visibility: :under_development

      field :timeline,
        resolver: Platform::Resolvers::IssueTimeline,
        description: "A list of events, comments, commits, etc. associated with the issue.",
        deprecated: {
          start_date: Date.new(2020, 4, 22),
          reason: "`timeline` will be removed",
          superseded_by: "Use Issue.timelineItems instead.",
          owner: "mikesea",
        }

      # New, fully batched timeline powered by `Timeline::IssueTimeline`. We do not
      # intend to publish it with this name!
      #
      # TODO: Make the timeline paginateable and publish it by merging it into `timeline`.
      field :timeline_items,
        resolver: Platform::Resolvers::IssueTimelineItems,
        max_page_size: 250,
        description: "A list of events, comments, commits, etc. associated with the issue."


      field :safe_user, Platform::Objects::User, null: true, description: "The user that created the issue - will fall back to ghost user if user was deleted", visibility: :under_development

      def safe_user
        Platform::Loaders::ActiveRecordAssociation.load(@object, :user).then do |user|
          @object.safe_user
        end
      end

      field :participants, resolver: Resolvers::Participants, description: "A list of Users that are participating in the Issue conversation."

      field :mentionable_items, resolver: Resolvers::MentionableItems, description: "A list of mentionable items that can be mentioned in the context of this issue.",
        connection: true, feature_flag: :pe_mobile, null: true

      field :websocket, String, visibility: :internal, description: "The websocket channel ID for live updates.", null: false do
        argument :channel, Enums::IssuePubSubTopic, "The channel to use.", required: true
      end

      def websocket(**arguments)
        case arguments[:channel]
        when "updated"
          GitHub::WebSocket::Channels.issue(@object)
        when "timeline"
          GitHub::WebSocket::Channels.issue_timeline(@object)
        when "state"
          GitHub::WebSocket::Channels.issue_state(@object)
        end
      end

      field :project_cards, resolver: Resolvers::ProjectCards, description: "List of project cards associated with this issue."

      def self.load_from_params(params)
        Objects::Repository.load_from_params(params).then do |repository|
          repository && Loaders::IssueByNumber.load(repository.id, params[:id].to_i)
        end
      end

      field :closed_by_pull_requests_references, resolver: Resolvers::ClosedByPullRequestsReferences, null: true, description: "List of open pull requests referenced from this issue",
        connection: true, visibility: :internal

      field :is_transfer_in_progress, Boolean, null: false, description: "Is this issue currently being transferred", visibility: :internal
      def is_transfer_in_progress
        Loaders::ActiveRecord.load(::IssueTransfer, @object.id, column: :new_issue_id).then do |transfer|
          transfer.present? && transfer.state != "done"
        end
      end

      field :is_read_by_viewer, Boolean, null: true, description: "Is this issue read by the viewer", feature_flag: :pe_mobile

      def is_read_by_viewer
        if viewer = @context[:viewer]
          !Conversation.read_for(viewer, [@object]).empty?
        else
          true
        end
      end

      field :possible_transfer_repositories_for_viewer, Connections::Repository, null: true, description: "Repositories that the viewer can transfer this issue to", visibility: :internal do
        argument :query, String, "A name to search by", required: false
      end
      def possible_transfer_repositories_for_viewer(query: nil)
        if viewer = @context[:viewer]
          @object.async_possible_transfer_repositories(viewer: viewer, query: query)
        else
          ArrayWrapper.new([])
        end
      end

      field :hovercard, Objects::Hovercard, description: "The hovercard information for this issue", null: false do
        argument :include_notification_contexts, Boolean, "Whether or not to include notification contexts", required: false, default_value: true
      end

      def hovercard(include_notification_contexts: true)
        ::IssueOrPullRequestHovercard.new(@object, viewer: @context[:viewer], include_notifications: include_notification_contexts)
      end
    end
  end
end
