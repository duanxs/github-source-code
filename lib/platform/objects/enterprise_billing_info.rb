# frozen_string_literal: true

module Platform
  module Objects
    class EnterpriseBillingInfo < Platform::Objects::Base
      description "Enterprise billing information visible to enterprise billing managers and owners."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, business)
        permission.access_allowed?(:manage_business_billing, resource: business, repo: nil, organization: nil, allow_integrations: false, allow_user_via_integration: false)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        object.billing_manager?(permission.viewer) || object.owner?(permission.viewer) || permission.viewer&.site_admin?
      end

      minimum_accepted_scopes ["manage_billing:enterprise"]


      # BILLING FIELDS

      field :seats,
        Integer,
        description: "The total seats across all organizations owned by the enterprise.",
        null: false,
        deprecated: {
          start_date: Date.new(2019, 9, 27),
          reason: "`seats` will be replaced with `totalLicenses` to provide more clarity on the value being returned",
          superseded_by: "Use EnterpriseBillingInfo.totalLicenses instead.",
          owner: "BlakeWilliams",
        }

      field :total_licenses, Integer, description: "The total number of licenses allocated.", null: false

      def total_licenses
        @object.async_organizations.then do
          @object.licenses
        end
      end
      alias_method :seats, :total_licenses # Remove this when the seats field is removed

      field :available_seats,
        Integer,
        description: "The number of available seats across all owned organizations based on the unique number of billable users.",
        null: false,
        deprecated: {
          start_date: Date.new(2019, 9, 27),
          reason: "`availableSeats` will be replaced with `totalAvailableLicenses` to provide more clarity on the value being returned",
          superseded_by: "Use EnterpriseBillingInfo.totalAvailableLicenses instead.",
          owner: "BlakeWilliams",
        }

      field :total_available_licenses, Integer, description: "The number of available licenses across all owned organizations based on the unique number of billable users.", null: false

      def total_available_licenses
        @object.async_organizations.then do
          @object.available_licenses
        end
      end
      alias_method :available_seats, :total_available_licenses # Remove this when the available_seats field is removed

      field :all_licensable_users_count, Integer, description: "The number of licenseable users/emails across the enterprise.", null: false

      def all_licensable_users_count
        @object.async_organizations.then do
          @object.consumed_licenses
        end
      end

      field :asset_packs, Integer, description: "The number of data packs used by all organizations owned by the enterprise.", null: false

      def asset_packs(**arguments)
        @object.aggregated_asset_status[:asset_packs]
      end

      field :bandwidth_usage, Float, description: "The bandwidth usage in GB for all organizations owned by the enterprise.", null: false

      def bandwidth_usage(**arguments)
        @object.aggregated_asset_status[:bandwidth_usage]
      end

      field :bandwidth_quota, Float, description: "The bandwidth quota in GB for all organizations owned by the enterprise.", null: false

      def bandwidth_quota(**arguments)
        @object.aggregated_asset_status[:bandwidth_quota]
      end

      field :bandwidth_usage_percentage, Integer, description: "The bandwidth usage as a percentage of the bandwidth quota.", null: false

      field :storage_usage, Float, description: "The storage usage in GB for all organizations owned by the enterprise.", null: false

      def storage_usage(**arguments)
        @object.aggregated_asset_status[:storage_usage]
      end

      field :storage_quota, Float, description: "The storage quota in GB for all organizations owned by the enterprise.", null: false

      def storage_quota(**arguments)
        @object.aggregated_asset_status[:storage_quota]
      end

      field :storage_usage_percentage, Integer, description: "The storage usage as a percentage of the storage quota.", null: false
    end
  end
end
