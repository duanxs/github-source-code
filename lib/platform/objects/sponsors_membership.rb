# frozen_string_literal: true

module Platform
  module Objects
    class SponsorsMembership < Platform::Objects::Base
      description "An application to join the GitHub Sponsors program."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        return true if permission.viewer.can_admin_sponsors_memberships?
        object.async_sponsorable.then do |sponsorable|
          permission.viewer == sponsorable || sponsorable.adminable_by?(permission.viewer)
        end
      end

      visibility :internal

      minimum_accepted_scopes ["user"]
      implements Platform::Interfaces::Node
      global_id_field :id

      created_at_field
      updated_at_field

      field :sponsorable, Interfaces::Sponsorable, description: "The account that is applying to the Sponsors program.", method: :async_sponsorable, null: false

      field :billing_country_code, String, description: "The alpha2 country code for the billing country", null: false

      def billing_country_code
        return "" unless @object.billing_country.present?
        @object.billing_country
      end

      field :full_billing_country, String, description: "The full name of the billing country", null: true

      field :is_eligible_for_stripe_connect, Boolean, "Whether or not this account is eligible for Stripe Connect", null: false

      def is_eligible_for_stripe_connect
        # NOTE: `eligible_for_stripe_connect?` compares against feature flags atm
        # so GraphQL raises an exception when trying to load `sponsorable`.
        # We should be able to remove this once the feature flags are no longer used.
        @object.async_sponsorable.then do
          @object.eligible_for_stripe_connect?
        end
      end

      field :is_ignored, Boolean, "Whether or not this membership has been ignored in stafftools", null: false, method: :ignored?

      field :contact_email, String, "The email to use for Sponsors notifications for this account", method: :async_contact_email_address, null: false

      field :admin_info, Objects::SponsorsMembershipAdminInfo, description: "SponsorsMembership information only visible to admin", null: true

      def admin_info
        if @context[:viewer] && @context[:viewer].can_admin_sponsors_memberships?
          Models::SponsorsMembershipAdminInfo.new(@object)
        else
          nil
        end
      end
    end
  end
end
