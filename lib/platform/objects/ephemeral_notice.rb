# frozen_string_literal: true

module Platform
  module Objects
    class EphemeralNotice < Platform::Objects::Base
      areas_of_responsibility :ce_extensibility

      description "Represents an app-created ephemeral notice as a result of an interaction with an interactive component"

      visibility :under_development

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, _object)
        permission.hidden_from_public?(self)
      end

      scopeless_tokens_as_minimum

      global_id_field :id

      database_id_field

      field :notice_text, String, "The text body of the ephemeral notice.", null: false

      field :link_to, String, "The link associated with the ephemeral notice.", null: true

      field :link_title, String, "The link title for the associated link.", null: true
    end
  end
end
