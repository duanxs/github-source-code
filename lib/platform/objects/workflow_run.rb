# frozen_string_literal: true

module Platform
  module Objects
    class WorkflowRun < Platform::Objects::Base
      description "A workflow run."
      areas_of_responsibility :actions
      visibility :internal

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, workflow_run)
        permission.async_repo_and_org_owner(workflow_run.check_suite).then do |repo, org|
          permission.access_allowed?(:read_actions, resource: repo, current_repo: repo, current_org: org, allow_integrations: true, allow_user_via_integration: true)
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, workflow_run)
        permission.load_repo_and_owner(workflow_run.check_suite).then do |repo|
          repo.resources.actions.async_readable_by?(permission.viewer)
        end
      end

      scopeless_tokens_as_minimum

      implements Platform::Interfaces::Node

      global_id_field :id

      database_id_field

      created_at_field

      updated_at_field

      field :run_number, Int, "A number that uniquely identifies this workflow run in its parent workflow.", null: false

    end
  end
end
