# frozen_string_literal: true

module Platform
  module Objects
    class OrganizationDiscussionComment < Platform::Objects::Base
      model_name "OrganizationDiscussionPostReply"
      description "A comment on an organization discussion."
      visibility :under_development
      minimum_accepted_scopes ["read:org"]
      areas_of_responsibility :orgs

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, comment)
        promises = [comment.async_organization, comment.async_discussion_post]
        Promise.all(promises).then do |org, post|
          access = if post.public?
            :list_public_members
          else
            :get_org_private
          end
          permission.access_allowed?(access, resource: org, allow_integrations: true,
                                     allow_user_via_integration: true, current_repo: nil,
                                     current_org: org)
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        object.async_discussion_post.then do |discussion|
          permission.typed_can_see?("OrganizationDiscussion", discussion)
        end
      end

      global_id_field :id
      implements Platform::Interfaces::Node
      implements Interfaces::Comment
      implements Interfaces::Deletable
      implements Interfaces::Updatable
      implements Interfaces::UpdatableComment

      url_fields description: "The HTTP URL for this comment." do |comment|
        comment.async_discussion_post.then do |post|
          post.async_organization.then do |org|
            template = Addressable::Template.new(
              "/orgs/{org}/discussions/{post}/comments/{comment}",
            )
            template.expand(org: org.login, post: post.number, comment: comment.number)
          end
        end
      end

      field :number, Integer, "Identifies the comment number.", null: false

      field :body_version, String, description: "The current version of the body content.", null: false

      field :discussion, OrganizationDiscussion, method: :async_discussion_post,
        description: "The discussion this comment is about.", null: false
    end
  end
end
