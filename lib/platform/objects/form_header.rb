# frozen_string_literal: true

module Platform
  module Objects
    class FormHeader < Platform::Objects::Base
      description "A form header to be used in a multipart/form-data request."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        true
      end

      visibility :internal
      minimum_accepted_scopes ["write:packages"]

      field :key, String, "Identifies the name of the header.", null: false
      field :value, String, "Identifies the value of the header.", null: false

    end
  end
end
