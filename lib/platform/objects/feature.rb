# frozen_string_literal: true

module Platform
  module Objects
    class Feature < Platform::Objects::Base
      model_name "FlipperFeature"
      description "An internal feature flag."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        object.async_viewer_can_read?(permission.viewer)
      end

      visibility :internal
      minimum_accepted_scopes ["devtools"]

      implements Platform::Interfaces::Node
      implements Interfaces::Deletable
      implements Interfaces::UniformResourceLocatable

      global_id_field :id

      database_id_field

      created_at_field

      field :name, String, "The feature's name.", null: false
      field :service_name, String, "The name of the service that owns this feature flag", null: true
      field :description, String, "The feature's description.", null: false

      field :description_html, Scalars::HTML, "The feature's formatted description.", null: false

      def description_html
        @object.async_description_html_for(@context[:viewer])
      end

      field :percentage_of_actors, Float, "The percentage of enabled actors expressed as a value 0-100.", method: :percentage_of_actors_value, null: false

      field :state, Enums::FeatureState, description: "The master enabled or disabled state of the feature.", null: false

      def state
        case @object.state
        when :on
          :enabled
        when :off
          :disabled
        else
          :conditional
        end
      end

      field :effective_state, Enums::FeatureState, null: false, description: <<~MD
        The effective enabled or disabled state of the feature. A feature is
        effectively enabled if it's master switch is on or if it's actor or
        percentage value is 100%.
      MD

      def effective_state
        if @object.fully_enabled?
          :enabled
        elsif @object.fully_disabled?
          :disabled
        else
          :conditional
        end
      end

      field :usage_state, Enums::FeatureUsageState, description: "Check if the feature is in used in the code base.", null: false

      def usage_state
        if @object.created_at > 1.day.ago
          :used
        elsif @object.code_usage.any?
          if @object.fully_enabled?
            :shipped
          elsif @object.fully_disabled?
            :disabled
          else
            :used
          end
        else
          :unused
        end
      end

      field :percentage_of_time, Float, "Dark-shipping percentage of time enabled.", method: :percentage_of_time_value, null: false

      field :haystack_url, Scalars::URI, description: "Server-side exceptions related to this feature.", null: false

      def haystack_url
        template = Addressable::Template.new("https://haystack.githubapp.com/github{?q}")
        template.expand({"q" => "enabled_features:#{@object.name}"})
      end

      field :actors, Connections::FeatureActor, description: "Feature flag actors", null: false, connection: true

      def actors
        wrapped_actors = @object.actors.map do |actor|
          case actor
          when ::Repository           then Models::FeatureActorRepository.new(actor)
          when ::Organization         then Models::FeatureActorOrganization.new(actor)
          when ::User                 then Models::FeatureActorUser.new(actor)
          when ::Business             then Models::FeatureActorEnterprise.new(actor)
          when ::OauthApplication     then actor
          when ::Integration          then Models::FeatureActorApp.new(actor)
          when ::GitHub::FlipperHost  then actor
          when ::GitHub::FlipperRole  then actor
          else
            Models::FeatureActorFlipperId.new(actor.flipper_id)
          end
        end
        ArrayWrapper.new(wrapped_actors)
      end

      field :history, Connections::FeatureHistoryEntry, description: "List of feature audit log entries", null: false, connection: true

      def history
        Platform::Helpers::FeatureHistoryQuery.new(@object, viewer: @context[:viewer])
      end

      field :available_groups, [Enums::FeatureGroup, null: true], description: "The groups that can have this feature enabled", null: false

      def available_groups
        @object.available_groups.map(&:name)
      end

      field :enabled_groups, [Enums::FeatureGroup, null: true], description: "The groups that have this feature enabled", null: false

      def enabled_groups
        @object.groups_value.map(&:to_sym)
      end

      field :code_ranges, Connections.define(Objects::FeatureCodeRange), description: "A list of places where this feature is checked in github/github", null: false, connection: true

      def code_ranges
        ArrayWrapper.new(@object.code_usage)
      end

      url_fields description: "The HTTP URL for this feature" do |feature|
        template = Addressable::Template.new("/devtools/feature_flags/{name}")
        template.expand name: feature.name
      end

      url_fields prefix: :edit, description: "The HTTP URL for editing this feature" do |feature|
        template = Addressable::Template.new("/devtools/feature_flags/{name}/edit")
        template.expand name: feature.name
      end

      url_fields prefix: :actors, description: "The HTTP URL for updating this feature's actors" do |feature|
        template = Addressable::Template.new("/devtools/feature_flags/{name}/actors")
        template.expand name: feature.name
      end

      def self.load_from_global_id(name)
        Loaders::ActiveRecord.load(::FlipperFeature, name, column: :name)
      end
    end
  end
end
