# frozen_string_literal: true

module Platform
  module Objects
    class NotificationThreadSubscription < Platform::Objects::Base
      model_name "Newsies::ThreadSubscription"
      description "A subscription to a thread (e.g. an Issue) that allows the user to receive notifications."
      visibility :under_development
      minimum_accepted_scopes ["notifications"]

      def self.async_api_can_access?(permission, object)
        # Only allow this access to queries against the internal schema.
        permission.hidden_from_public?(self)
      end

      def self.async_viewer_can_see?(permission, object)
        object&.async_readable_by?(permission.viewer)
      end

      field :list, Unions::NotificationsList, description: "The parent object of the thread.", null: false, method: :async_list
      field :thread, Unions::NotificationsSubject, description: "The thread object.", null: true, method: :async_thread
      created_at_field null: true
      database_id_field visibility: :internal
    end
  end
end
