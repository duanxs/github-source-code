# frozen_string_literal: true

module Platform
  module Objects
    class BiztoolsInfo < Platform::Objects::Base
      areas_of_responsibility :stafftools
      description "Biztools information for biztools users."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        permission.viewer.biztools_user? || permission.viewer.site_admin?
      end

      visibility :internal

      minimum_accepted_scopes ["biztools"]

      field :sponsors_memberships, Connections.define(Objects::SponsorsMembership), visibility: :internal, description: "Submitted GitHub Sponsors applications", connection: true, null: true do
        argument :query, String, description: "The login of a user to search by", required: false
        argument :filter_by, Inputs::SponsorsMembershipFilters,
          description: "Filtering options for memberships returned from the connection.",
          required: false
        argument :order_by, Inputs::SponsorsMembershipOrder,
          description: "Ordering options for memberships returned from the connection.",
          required: false,
          default_value: { field: "created_at", direction: "ASC" }
      end

      def sponsors_memberships(**arguments)
        scope = ::SponsorsMembership.all

        if arguments[:filter_by]
          if arguments[:filter_by][:states]
            states = arguments[:filter_by][:states].map do |state|
              ::SponsorsMembership.state_value(state)
            end

            scope = scope.where(state: states)
          end

          if arguments[:filter_by].key?(:spammy)
            scope = scope.for_automated_criterion \
              slug: ::SponsorsCriterion::Automated::SpammyStatus.slug,
              met:  !arguments[:filter_by][:spammy]
          end

          if arguments[:filter_by].key?(:suspended)
            scope = scope.for_automated_criterion \
              slug: ::SponsorsCriterion::Automated::SuspensionStatus.slug,
              met:  !arguments[:filter_by][:suspended]
          end

          if arguments[:filter_by].key?(:type)
            sponsorable_ids = scope.where(sponsorable_type: :User).pluck(:sponsorable_id)
            candidate_user_ids = ::User.where(id: sponsorable_ids, type: arguments[:filter_by][:type]).pluck(:id)

            scope = scope.where(sponsorable_type: :User, sponsorable_id: candidate_user_ids)
          end

          if arguments[:filter_by].key?(:ignored)
            scope = arguments[:filter_by][:ignored] ? scope.ignored : scope.not_ignored
          end

          if arguments[:filter_by].key?(:fiscal_host)
            scope = scope.where(fiscal_host: arguments[:filter_by][:fiscal_host])
          end

          if arguments[:filter_by].key?(:billing_country)
            scope = scope.where(billing_country: arguments[:filter_by][:billing_country].flatten)
          end
        end

        # NOTE: run this last so we can limit the number of users we search for
        if arguments[:query].present?
          query = "#{ActiveRecord::Base.sanitize_sql_like(arguments[:query].strip)}%"
          sponsorable_ids = scope.where(sponsorable_type: ::User.name).pluck(:sponsorable_id)
          candidate_user_ids = ::User.where(id: sponsorable_ids).where("users.login LIKE ?", query).pluck(:id)

          scope = scope.where(sponsorable_type: ::User.name, sponsorable_id: candidate_user_ids)
        end

        order_by = arguments[:order_by]
        order_by ||= { field: "created_at", direction: "ASC" }
        scope.order("sponsors_memberships.#{order_by[:field]} #{order_by[:direction]}")
      end

      field :sponsors_criteria, Connections.define(Objects::SponsorsCriterion), visibility: :internal, description: "Criteria used to evaluate Sponsors applications", connection: true, null: true do
        argument :filter, Enums::SponsorsCriterionFilter,
          description: "The condition on which to filter criteria.",
          required: false,
          default_value: :all
        argument :order_by, Inputs::SponsorsCriterionOrder,
          description: "Ordering options for criteria returned from the connection.",
          required: false,
          default_value: { field: "created_at", direction: "ASC" }
      end

      def sponsors_criteria(**arguments)
        scope = ::SponsorsCriterion.all

        case arguments[:filter]
        when :automated
          scope = scope.where(automated: true)
        when :manual
          scope = scope.where(automated: false)
        end

        order_by = arguments[:order_by]
        scope.order("sponsors_criteria.#{order_by[:field]} #{order_by[:direction]}")
      end

      field :sponsors_listings, Connections::SponsorsListing, description: "Gets all existing Sponsors listings.", visibility: :internal, null: false do
        argument :query, String,
          description: "Find Sponsors listings with slug or description that match the query.",
          required:    false
        argument :filter_by, Inputs::SponsorsListingFilters,
          description: "Filtering options for Sponsors listings returned from the connection.",
          required:    false
        argument :order_by, Inputs::SponsorsListingOrder,
          description:   "Ordering options for Sponsors listings returned from the connection.",
          required:      false,
          default_value: { field: "created_at", direction: "ASC" }
      end

      def sponsors_listings(query: nil, filter_by: nil, order_by: { field: "created_at", direction: "ASC" })
        scope = ::SponsorsListing.all

        if filter_by
          if filter_by[:states]
            scope = scope.where(state: filter_by[:states])
          end

          if filter_by[:type]
            sponsorable_ids = scope.where(sponsorable_type: :User).pluck(:sponsorable_id)
            candidate_user_ids = ::User.where(id: sponsorable_ids, type: filter_by[:type]).pluck(:id)

            scope = scope.where(sponsorable_type: :User, sponsorable_id: candidate_user_ids)
          end
        end

        if query.present?
          scope = scope.matches_slug_or_description(query)
        end

        scope.order("sponsors_listings.#{order_by[:field]} #{order_by[:direction]}")
      end
    end
  end
end
