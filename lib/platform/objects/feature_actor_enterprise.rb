# frozen_string_literal: true

module Platform
  module Objects
    class FeatureActorEnterprise < Platform::Objects::Base

      description <<~MD
        Represents an enterprise that is a feature actor. It provides a smaller
        number of fields than Enterprise in order to maintain privacy.
      MD

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, *)
        permission.viewer && (permission.viewer.site_admin? || permission.viewer.github_developer?)
      end

      visibility :internal

      minimum_accepted_scopes ["devtools"]

      implements Platform::Interfaces::Node
      implements Interfaces::FeatureActor

      global_id_field :id

      field :name, String, description: "The name of the enterprise", null: false
      field :slug, String, description: "The URL friendly identifier of the enterprise", null: false
      field :avatar_url, Scalars::URI, description: "A URL pointing to the enterprise's public avatar.", null: false do
        argument :size, Integer, "The size of the resulting square image.", required: false
      end

      def avatar_url(size: nil)
        @object.primary_avatar_url(size)
      end

      url_fields description: "The HTTP URL for this enterprise." do |enterprise|
        template = Addressable::Template.new("/enterprises/{slug}")
        template.expand slug: enterprise.slug
      end

      url_fields prefix: :stafftools, description: "The HTTP URL for this enterprise in stafftools." do |enterprise|
        template = Addressable::Template.new("/stafftools/enterprises/{slug}")
        template.expand slug: enterprise.slug
      end

      def self.load_from_global_id(id)
        Loaders::ActiveRecord.load(::Business, id.to_i, security_violation_behaviour: :nil).then do |business|
          Models::FeatureActorEnterprise.new(business)
        end
      end
    end
  end
end
