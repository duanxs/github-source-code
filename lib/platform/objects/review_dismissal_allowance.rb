# frozen_string_literal: true

module Platform
  module Objects
    class ReviewDismissalAllowance < Platform::Objects::Base
      description "A team or user who has the ability to dismiss a review on a protected branch."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, review_dismissal_allowance)
        review_dismissal_allowance.async_protected_branch.then do |protected_branch|
          permission.typed_can_access?("BranchProtectionRule", protected_branch)
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        object.async_protected_branch.then do |protected_branch|
          permission.typed_can_see?("BranchProtectionRule", protected_branch)
        end
      end

      minimum_accepted_scopes ["public_repo"]

      implements Platform::Interfaces::Node

      global_id_field :id

      field :actor, Unions::ReviewDismissalAllowanceActor, method: :async_actor, description: "The actor that can dismiss.", null: true

      field :branch_protection_rule, BranchProtectionRule, "Identifies the branch protection rule associated with the allowed user or team.", null: true

      def branch_protection_rule
        @object.async_protected_branch.then do |protected_branch|
          Platform::Models::BranchProtectionRule.new(protected_branch)
        end
      end
    end
  end
end
