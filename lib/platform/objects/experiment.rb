# frozen_string_literal: true

module Platform
  module Objects
    class Experiment < Platform::Objects::Base
      description "A Science experiment."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        ::Experiment.viewer_can_read?(permission.viewer)
      end

      visibility :internal
      minimum_accepted_scopes ["devtools"]

      implements Platform::Interfaces::Node
      implements Interfaces::Deletable
      implements Interfaces::UniformResourceLocatable

      global_id_field :id

      field :name, String, "The experiment's name.", null: false
      field :updated_at, Scalars::DateTime, "When the experiment was last updated.", null: false
      field :running_too_long, Boolean, "If a experiment has been running for too long.", method: :running_too_long?, null: false
      field :active, Boolean, "If the experiment is active.", method: :active?, null: false
      field :inactive, Boolean, "If the experiment is inactive.", method: :inactive?, null: false
      field :sample_threshold, Integer, "The sample threshold set on the experiment", null: true
      field :sample_ends_at, Integer, "When the sampling for this experiment ends in UNIX time.", null: true

      field :mismatches, [Objects::ExperimentMismatch], description: "The mismatches of the experiment", null: false do
        argument :page, Integer, "The page of results to return", default_value: 1, required: false
      end
      def mismatches(page:)
        mismatches = @object.mismatches(page: page)
        ArrayWrapper.new(mismatches.map { |mismatch|
          Devtools::Experiments::MismatchView.new experiment_data: mismatch
        })
      end

      field :percentage, Integer, description: "The percentage the experiment is set to.", null: false
      def percentage
        ::Experiment[@object.name].percent
      end

      field :activity_rate, Integer, description: "The activity rate of the experiment.", null: false
      field :mismatch_rate, Integer, description: "The mismatch rate of the experiment.", null: false

      field :total_mismatches, Integer, description: "The total number of mismatches collected for this experiment", null: false
      field :total_samples, Integer, description: "The total number of samples collected for this experiment", null: false

      field :samples, [Objects::ExperimentSample], description: "The experiments samples.", null: false do
        argument :page, Integer, "The page of results to return", default_value: 1, required: false
      end
      def samples(page:)
        samples = @object.samples(page: page)
        ArrayWrapper.new(samples.map { |sample|
          Devtools::Experiments::SampleView.new experiment_data: sample
        })
      end

      def self.load_from_global_id(name)
        Loaders::ActiveRecord.load(::Experiment, name, column: :name)
      end
    end
  end
end
