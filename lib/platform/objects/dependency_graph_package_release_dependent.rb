# frozen_string_literal: true

module Platform
  module Objects
    class DependencyGraphPackageReleaseDependent < Platform::Objects::Base
      description "A package release dependent"

      areas_of_responsibility :dependency_graph

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        true
      end

      visibility :internal

      minimum_accepted_scopes ["public_repo"]

      field :package_release, DependencyGraphPackageRelease, "Package Release object", null: false
      field :dependents_count, Integer, "Number of dependent repositories on a Package Release", null: false
      field :vulnerabilities_count, Integer, "Number of vulnerabilities on a Package Release", null: false
      field :lower_version_count, Integer, "Number of dependents on earlier package release versions", null: false
      field :upper_version_count, Integer, "Number of dependents on newer package release versions", null: false
      field :vulnerabilities_count, Integer, "Number of vulnerabilities on a Package Release", null: false
      field :dependents, [Platform::Objects::DependencyGraphDependent, null: true], description: "A list of package dependents", null: true
      field :dependents_page_info, GraphQL::Types::Relay::PageInfo, description: "Package dependents pagination information", null: true
    end
  end
end
