# frozen_string_literal: true

module Platform
  module Objects
    class FeatureActorApp < Platform::Objects::Base
      description <<~MD
        Represents a app that is a feature actor. It provides a smaller
        number of fields than App in order to maintain privacy.
      MD

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, *)
        permission.viewer && (permission.viewer.site_admin? || permission.viewer.github_developer?)
      end

      visibility :internal

      minimum_accepted_scopes ["devtools"]

      implements Platform::Interfaces::Node
      implements Interfaces::FeatureActor

      global_id_field :id

      field :name, String, "The name of the app.", null: false

      field :slug, String, "A slug based on the name of the app for use in URLs.", null: false

      url_fields description: "The HTTP URL for this app's stafftools page", prefix: "stafftools", null: true do |app|
        app.async_owner.then do |owner|
          if owner.is_a?(::User)
            # Business owned apps don't currently have a stafftools page
            template = Addressable::Template.new("/stafftools/users/{owner}/apps/{slug}")
            template.expand owner: owner.login, slug: app.slug
          end
        end
      end

      def self.load_from_global_id(id)
        Loaders::ActiveRecord.load(::Integration, id.to_i, security_violation_behaviour: :nil).then do |integration|
          Models::FeatureActorApp.new(integration)
        end
      end
    end
  end
end
