# frozen_string_literal: true

module Platform
  module Objects
    class OrganizationTeamsHovercardContext < Objects::Base
      description "An organization teams hovercard context"

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, ctx)
        true
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        true
      end

      scopeless_tokens_as_minimum

      implements Interfaces::HovercardContext

      url_fields prefix: :teams, description: "The URL for the full team list for this user" do |hovercard_context|
        template = Addressable::Template.new("/orgs/{org}/teams?query=@{user}")
        template.expand(user: hovercard_context.user.login, org: hovercard_context.organization.login)
      end

      field :totalTeamCount, Integer, null: false, method: :total_team_count do
        description "The total number of teams the user is on in the organization"
      end

      field :relevantTeams, Connections.define(Objects::Team), connection: true, null: false, method: :highlighted do
        description "Teams in this organization the user is a member of that are relevant"
      end
    end
  end
end
