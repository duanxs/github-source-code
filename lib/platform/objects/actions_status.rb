# frozen_string_literal: true

module Platform
  module Objects
    class ActionsStatus < Platform::Objects::Base
      description "The status of actions for a repository"
      areas_of_responsibility :gitcoin

      visibility :internal

      implements Platform::Interfaces::Node

      minimum_accepted_scopes ["site_admin"]

      def self.async_viewer_can_see?(permission, _object)
        # TODO this object will never be public
        permission.hidden_from_public?(self)
      end

      def self.async_api_can_access?(permission, _object)
        # TODO this object will never be public
        permission.hidden_from_public?(self)
      end

      field :is_allowed, Boolean, description: "Returns whether or not actions are allowed to run", null: false
      def is_allowed
        @object.async_customer.then do |customer|
          if customer.present?
            customer.async_payment_method.then do
              Billing::ActionsPermission.new(@object).allowed?
            end
          else
            false
          end
        end
      end

      field :error, Platform::Objects::ActionsStatusError, description: "The error representing why actions are disabled", null: true

      def error
        @object
      end
    end
  end
end
