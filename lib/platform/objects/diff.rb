# frozen_string_literal: true

module Platform
  module Objects
    class Diff < Platform::Objects::Base
      description "Represents a diff between two commits objects."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, object)
        permission.async_owner_if_org(object.base_repo).then do |org|
          permission.access_allowed?(
            :get_diff,
            resource: object.base_repo,
            current_org: org,
            current_repo: object.base_repo,
            allow_integrations: true,
            allow_user_via_integration: true,
          )
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        permission.typed_can_see?("Repository", object.base_repo)
      end

      feature_flag :pe_mobile

      minimum_accepted_scopes ["repo"]

      field :files_changed, Integer, "The number of files changed in this diff.", method: :changed_files, null: false

      field :lines_added, Integer, "The number of lines added in this diff.", method: :additions, null: false

      field :lines_deleted, Integer, "The number of lines removed in this diff.", method: :deletions, null: false

      field :lines_changed, Integer, "The total lines added or removed in this diff.", method: :changes, null: false

      field :patches, Connections.define(Objects::Patch), description: "The set of patches constituting this diff.", null: false, connection: true, extras: [:lookahead]

      def patches(lookahead:)
        patches = if GitHub.flipper[:pe_mobile].enabled?(@context[:oauth_app]) || GitHub.flipper[:pe_mobile].enabled?(@context[:viewer]) || Helpers::Diff.use_entries?(lookahead)
          @object.entries.map { |entry| Models::Patch.new(@object, entry) }
        else
          @object.deltas.map { |delta| Models::Patch.new(@object, delta) }
        end
        ArrayWrapper.new(patches)
      end
    end
  end
end
