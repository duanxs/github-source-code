# frozen_string_literal: true

module Platform
  module Objects
    class PullRequestReview < Platform::Objects::Base
      description "A review object for a given pull request."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, prr)
        permission.load_pull_and_issue(prr).then do |pull|
          permission.async_repo_and_org_owner(pull).then do |repo, org|
            permission.access_allowed?(:list_pull_request_comments, repo: repo, current_org: org, resource: pull, allow_integrations: true, allow_user_via_integration: true)
          end
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        return false if object.state == ::PullRequestReview.state_value(:pending) && object.user_id != permission.viewer.try(:id)
        object.async_user.then { |user|
          if user && user.hide_from_user?(permission.viewer)
            false
          else
            object.async_pull_request.then { |pull_request|
              Promise.all([
                permission.typed_can_see?("PullRequest", pull_request),
                object.async_readable_by?(permission.viewer),
              ]).then(&:all?)
            }
          end
        }
      end

      scopeless_tokens_as_minimum

      implements Platform::Interfaces::Node
      implements Interfaces::Comment
      implements Interfaces::Deletable
      implements Interfaces::Updatable
      implements Interfaces::UpdatableComment
      implements Interfaces::Reactable
      implements Interfaces::Reportable
      implements Interfaces::Blockable
      implements Interfaces::AbuseReportable
      implements Interfaces::RepositoryNode
      global_id_field :id
      database_id_field

      url_fields description: "The HTTP URL permalink for this PullRequestReview." do |review|
        review.async_path_uri
      end

      url_fields prefix: :diff, visibility: :internal, description: "The HTTP URL permalink for the pull request diff of the review." do |review|
        review.async_diff_uri
      end

      field :author_can_push_to_repository, Boolean, feature_flag: :pe_mobile, method: :async_author_can_push_to_repository?, description: "Indicates whether the author of this review has push access to the repository.", null: false

      field :body, String, description: "Identifies the pull request review body.", null: false

      def body
        @object.body || ""
      end

      field :body_version, String, visibility: :internal, description: "Identifies the pull request review body hash.", null: false

      field :body_text, String, description: "The body of this review rendered as plain text.", null: false

      def body_text
        Promise.all([
          @object.async_repository.then { |repository|
            repository.async_owner
          },
          @object.async_latest_user_content_edit.then { |user_content_edit|
            user_content_edit&.async_editor
          },
        ]).then do
          @object.async_body_text.then do |body_text|
            body_text || GitHub::HTMLSafeString::EMPTY
          end
        end
      end

      field :body_html, Scalars::HTML, description: "The body rendered to HTML.", null: false do
        argument :hide_code_blobs, Boolean, "Whether or not to include the HTML for code blobs", required: false, default_value: false, feature_flag: :pe_mobile
        argument :render_suggested_changes_as_text, Boolean, "Whether or not to include the HTML for suggested changes", required: false, default_value: false, feature_flag: :pe_mobile
      end

      def body_html(hide_code_blobs: false, render_suggested_changes_as_text: false)
        @object.async_repository.then do |repo|
          Promise.all([repo.async_owner, repo.async_network]).then do |owner, network|
            if hide_code_blobs
              @object.async_body_html!(context: {hide_code_blobs: hide_code_blobs}).then do |body_html|
                body_html || GitHub::HTMLSafeString::EMPTY
              end
            else
              @object.async_body_html.then do |body_html|
                body_html || GitHub::HTMLSafeString::EMPTY
              end
            end
          end
        end
      end

      field :pull_request, Objects::PullRequest, method: :async_pull_request, description: "Identifies the pull request associated with this pull request review.", null: false

      field :commit, Objects::Commit, description: "Identifies the commit associated with this pull request review.", null: true

      def commit
        @object.async_pull_request.then do |pull_request|
          pull_request.async_load_pull_request_commit(@object.head_sha).then do |pull_request_commit|
            pull_request_commit&.commit
          end
        end
      end

      field :state, Enums::PullRequestReviewState, description: "Identifies the current state of the pull request review.", null: false

      def state
        ::PullRequestReview.state_name(@object.state).to_s
      end

      field :comments, resolver: Resolvers::PullRequestReviewComments, numeric_pagination_enabled: true, description: "A list of review comments for the current pull request review.", connection: true

      field :on_behalf_of, Connections.define(Objects::Team), description: "A list of teams that this review was made on behalf of.", null: false, connection: true

      def on_behalf_of
        @object.async_on_behalf_of_visible_teams_for(@context[:viewer]).then do |teams|
          StableArrayWrapper.new(teams)
        end
      end

      field :threads_and_replies, Connections::PullRequestReviewItem, feature_flag: :pe_mobile, description: "A union of review threads and comments that are replies to other review threads.", null: true, connection: true do
        # Note: This is not intended to be published.

        argument :skip, Integer, description: "Skips the first _n_ elements in the list.", required: false

        argument :focus, ID, description: "ID of element to focus on.", required: false
      end


      def threads_and_replies(**arguments)
        async_review_thread_models =
          @object.async_repository.then do |repository|
            if GitHub.flipper[:use_review_thread_ar_model].enabled?(repository)
              @object.async_review_threads_for(@context[:viewer])
            else
              @object.async_review_threads_for(@context[:viewer]).then do |review_threads|
                Promise.all(review_threads.map { |thread|
                  thread.async_review_comments.then do |review_comments|
                    Platform::Models::PullRequestReviewThread.from_review_comments(review_comments)
                  end
                })
              end
            end
          end

        Promise.all([
          async_review_thread_models,
          Platform::Loaders::PullRequestReviewCrossReviewReplies.load(@object.id, @context[:viewer]),
        ]).then do |review_thread_models, cross_review_replies|
          StableArrayWrapper.new(review_thread_models + cross_review_replies).tap do |wrapper|
            wrapper.sort_by_proc = -> (item) {
              is_thread = item.is_a?(Platform::Models::PullRequestReviewThread) || item.is_a?(::PullRequestReviewThread)
              [item.created_at, is_thread ? 0 : 1, item.id]
            }
          end
        end
      end

      # NOTE: This is not intended to be published as it is. The current implementation
      #   exposes threads that only contain the answer to a thread of another review.
      #   We plan to address this with future work on first-class review thread objects.
      field :threads, Connections::PullRequestReviewThread, feature_flag: :pe_mobile, description: "A list of review comment threads for this pull request review.", null: true do
        argument :skip, Integer, description: "Skips the first _n_ elements in the list.", required: false
      end

      def threads(skip: nil)
        @object.async_review_threads_for(@context[:viewer]).then do |review_threads|
          @object.async_repository.then do |repository|
            if GitHub.flipper[:use_review_thread_ar_model].enabled?(repository)
              StableArrayWrapper.new(review_threads)
            else
              async_review_thread_models = review_threads.map do |thread|
                thread.async_review_comments.then do |review_comments|
                  Platform::Models::PullRequestReviewThread.from_review_comments(review_comments)
                end
              end

              Promise.all(async_review_thread_models).then do |review_thread_models|
                StableArrayWrapper.new(review_thread_models)
              end
            end
          end
        end
      end

      field :submitted_at, Scalars::DateTime, "Identifies when the Pull Request Review was submitted", null: true

      field :websocket, String, visibility: :internal, description: "The websocket channel ID for live updates.", null: false do
        argument :channel, Enums::PullRequestReviewPubSubTopic, "The channel to use.", required: true
      end

      def websocket(**arguments)
        case arguments[:channel]
        when "updated"
          GitHub::WebSocket::Channels.pull_request_review(@object)
        end
      end
    end
  end
end
