# frozen_string_literal: true

module Platform
  module Objects
    class PushAllowance < Platform::Objects::Base
      description "A team, user or app who has the ability to push to a protected branch."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, push_allowance)
        permission.typed_can_access?("BranchProtectionRule", push_allowance.branch_protection_rule)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        permission.typed_can_see?("BranchProtectionRule", object.branch_protection_rule)
      end

      minimum_accepted_scopes ["public_repo"]

      implements Platform::Interfaces::Node

      global_id_field :id

      field :actor, Unions::PushAllowanceActor, "The actor that can push.", null: true

      def actor
        actor = object.actor
        if actor.respond_to?(:spammy?) && actor.spammy?
          nil
        else
          actor
        end
      end

      field :branch_protection_rule, BranchProtectionRule, "Identifies the branch protection rule associated with the allowed user or team.", null: true

      def branch_protection_rule
        Platform::Models::BranchProtectionRule.new(@object.branch_protection_rule)
      end

      def self.load_from_global_id(id)
        Models::PushAllowance.load_from_global_id(id)
      end
    end
  end
end
