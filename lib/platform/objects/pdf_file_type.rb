# frozen_string_literal: true

module Platform
  module Objects
    class PdfFileType < Platform::Objects::Base
      graphql_name "PdfFileType"
      description "Represents a pdf file."

      areas_of_responsibility :repositories

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        true
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        true
      end

      feature_flag :pe_mobile

      minimum_accepted_scopes ["repo"]

      implements Interfaces::RawBlobUrl
    end
  end
end
