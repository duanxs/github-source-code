# frozen_string_literal: true
module Platform
  module Objects
    class Base < GraphQL::Schema::Object
      field_class Objects::Base::Field
      extend Platform::Helpers::Url
      extend Platform::Objects::Base::AreasOfResponsibility
      extend Platform::Objects::Base::FeatureFlag
      extend Platform::Objects::Base::MobileOnly
      extend Platform::Objects::Base::Scopes
      extend Platform::Objects::Base::SiteAdminCheck
      extend Platform::Objects::Base::Visibility
      extend Platform::Objects::Base::LimitActorsTo
      extend Platform::Objects::Base::ClassBasedEdgeType
      extend Platform::Objects::Base::FieldShortcuts
      extend Platform::Objects::Base::Previewable

      def self.inherited(child_cls)
        # Setup a type-specific error so that needles can be properly identified
        child_cls.const_set(:ViewerMayNotSee, Class.new(Platform::Errors::ViewerMayNotSee))
        super
      end

      # Figure out, for this GraphQL type, is this Ruby `object` allowed in this `context`?
      def self.authorized?(object, context)
        Platform::Helpers::ActorLimiter.check(self, context)
        # since the checks will occur inside promises, grab the current GraphQL path now
        current_path = context.namespace(:interpreter)[:current_path]
        context[:permission].typed_can_access?(self, object).then do |accessible|
          if accessible
            context[:permission].typed_can_see?(self, object).then do |can_read_result|
              if can_read_result
                true
              else
                raise self::ViewerMayNotSee.new(object, self, context, current_path) # rubocop:disable GitHub/UsePlatformErrors
              end
            end
          else
            false
          end
        end
      end

      def self.async_viewer_can_see?(permission, object)
        # rubocop:disable GitHub/UsePlatformErrors
        raise NotImplementedError, <<~MSG
          `#{self}.async_viewer_can_see?` should be implemented to check if
          the current viewer (#{permission.viewer.inspect}) may see
          this object (#{object.inspect}).

          (GraphQL type: #{graphql_name})
        MSG
        # rubocop:enable GitHub/UsePlatformErrors
      end

      def self.field(*args, **kwargs, &block)
        if mutation = kwargs[:mutation]
          kwargs[:feature_flag] = mutation.feature_flag
          kwargs[:areas_of_responsibility] = mutation.areas_of_responsibility
        end

        super
      end

      class << self
        def model_name(model_class_name = nil)
          if model_class_name
            @model_name = model_class_name
          elsif defined?(@model_name)
            @model_name
          else
            @model_name = self.name.split("::").last
          end
        end

        def load_from_global_id(id)
          model_class = model_name.constantize
          Platform::Objects.async_find_record_by_id(model_class, id)
        end
      end
    end
  end
end
