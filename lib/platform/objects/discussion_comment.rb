# frozen_string_literal: true

module Platform
  module Objects
    class DiscussionComment < Platform::Objects::Base
      description "A comment on a discussion."
      areas_of_responsibility :discussions
      visibility :under_development

      scopeless_tokens_as_minimum
      feature_flag :discussions

      implements Platform::Interfaces::Node
      implements Interfaces::Comment
      implements Interfaces::Deletable
      implements Interfaces::Minimizable
      implements Interfaces::Updatable
      implements Interfaces::UpdatableComment
      implements Interfaces::AbuseReportable
      implements Interfaces::Blockable
      implements Interfaces::Reactable

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, comment)
        permission.async_repo_and_org_owner(comment).then do |repo, org|
          permission.access_allowed?(
            :show_discussion_comment,
            resource: comment,
            current_org: org,
            repo: repo,
            allow_integrations: true,
            allow_user_via_integration: false)
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, comment)
        comment.async_readable_by?(permission.viewer)
      end

      global_id_field :id
      database_id_field

      field :user, Platform::Objects::User, description: "The user that authored the comment.",
        null: true, method: :async_user

      field :discussion, Platform::Objects::Discussion, description: "The discussion this comment was created in",
        null: true, method: :async_discussion

      # Interfaces::Comment

      def authored_by_subject_author
        @object.async_discussion.then do |discussion|
          discussion.user_id == @object.user_id
        end
      end

      def subject_type
        "discussion"
      end
    end
  end
end
