# frozen_string_literal: true

module Platform
  module Objects
    class ViewerHovercardContext < Objects::Base
      implements Interfaces::HovercardContext
      description "A hovercard context with a message describing how the viewer is related."
      areas_of_responsibility :user_profile

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, ctx)
        true
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        true
      end

      scopeless_tokens_as_minimum

      field :viewer, Objects::User, "Identifies the user who is related to this context.",
        null: false
    end
  end
end
