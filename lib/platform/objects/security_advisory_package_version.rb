# frozen_string_literal: true

module Platform
  module Objects
    class SecurityAdvisoryPackageVersion < Platform::Objects::Base
      visibility :public

      description "An individual package version"

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        permission.public_api_on_dotcom?
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        true
      end

      scopeless_tokens_as_minimum

      areas_of_responsibility :security_advisories

      field :identifier, String, "The package name or version", null: false
    end
  end
end
