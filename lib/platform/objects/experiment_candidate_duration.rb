# frozen_string_literal: true

module Platform
  module Objects
    class ExperimentCandidateDuration < Platform::Objects::Base
      description "Represents a candidate duration for a Scientist experiment sample."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        ::Experiment.viewer_can_read?(permission.viewer)
      end

      visibility :internal

      minimum_accepted_scopes ["devtools"]

      field :name, String, "The name of this duration.", null: false
      field :duration, Float, "The duration itself.", null: false
      field :difference, Float, "The difference.", null: false
    end
  end
end
