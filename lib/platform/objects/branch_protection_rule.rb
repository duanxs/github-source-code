# frozen_string_literal: true

module Platform
  module Objects
    class BranchProtectionRule < Platform::Objects::Base
      minimum_accepted_scopes ["public_repo"]

      description "A branch protection rule."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, protection_rule)
        permission.async_repo_and_org_owner(protection_rule).then do |repo, org|
          permission.access_allowed?(:read_branch_protection, resource: repo, current_repo: repo, current_org: org, allow_integrations: true, allow_user_via_integration: true)
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        permission.belongs_to_repository(object)
      end

      implements Platform::Interfaces::Node

      def self.load_from_global_id(id)
        Platform::Objects.async_find_record_by_id(::ProtectedBranch, id).then do |obj|
          Platform::Models::BranchProtectionRule.new(obj) if obj
        end
      end

      global_id_field :id

      database_id_field

      field :pattern, String,
        description: "Identifies the protection rule pattern.",
        null: false

      def pattern
        @object.name.dup.force_encoding("utf-8")
      end

      field :repository, Repository,
        description: "The repository associated with this branch protection rule.",
        method: :async_repository,
        null: true

      field :creator, resolver: Resolvers::ActorCreator,
        description: "The actor who created this branch protection rule."

      field :is_admin_enforced, Boolean,
        description: "Can admins overwrite branch protection.",
        method: :admin_enforced?,
        null: false

      field :requires_approving_reviews, Boolean,
        description: "Are approving reviews required to update matching branches.",
        method: :pull_request_reviews_enabled?,
        null: false

      field :required_approving_review_count, Integer,
        description: "Number of approving reviews required to update matching branches.",
        null: true

      def required_approving_review_count
        return unless @object.pull_request_reviews_enabled?
        @object.required_approving_review_count
      end

      field :dismisses_stale_reviews, Boolean,
        description: "Will new commits pushed to matching branches dismiss pull request review approvals.",
        method: :dismiss_stale_reviews_on_push?,
        null: false

      field :restricts_review_dismissals, Boolean,
        description: "Is dismissal of pull request reviews restricted.",
        method: :authorized_dismissal_actors_only,
        null: false

      field :review_dismissal_allowances,
        type: Connections::ReviewDismissalAllowance,
        resolver: Resolvers::ReviewDismissalAllowances,
        description: "A list review dismissal allowances for this branch protection rule.",
        null: false,
        connection: true

      field :requires_status_checks, Boolean,
        description: "Are status checks required to update matching branches.",
        method: :required_status_checks_enabled?,
        null: false

      field :requires_code_owner_reviews, Boolean,
        description: "Are reviews from code owners required to update matching branches.",
        null: false

      def requires_code_owner_reviews
        @object.async_repository.then do |repository|
          Promise.all([repository.async_internal_repository, repository.async_business]).then do
            @object.require_code_owner_review?
          end
        end
      end

      field :requires_strict_status_checks, Boolean,
        description: "Are branches required to be up to date before merging.",
        method: :strict_required_status_checks_policy,
        null: false

      field :required_status_check_contexts, [String, null: true],
        description: "List of required status check contexts that must pass for commits to be accepted to matching branches.",
        null: true

      def required_status_check_contexts
        @object.async_required_status_checks.then do |required_status_checks|
          required_status_checks.map(&:context)
        end
      end

      field :restricts_pushes, Boolean,
        description: "Is pushing to matching branches restricted.",
        method: :authorized_actors_only,
        null: false

      field :push_allowances,
        type: Connections::PushAllowance,
        resolver: Resolvers::PushAllowances,
        description: "A list push allowances for this branch protection rule.",
        null: false,
        connection: true

      field :requires_commit_signatures, Boolean,
        description: "Are commits required to be signed.",
        method: :required_signatures_enabled?,
        null: false

      field :requires_linear_history, Boolean,
        description: "Are merge commits prohibited from being pushed to this branch.",
        method: :required_linear_history_enabled?,
        null: false,
        visibility: :under_development

      field :allows_force_pushes, Boolean,
        description: "Are force pushes allowed on this branch.",
        null: false,
        visibility: :under_development

      def allows_force_pushes
        !@object.block_force_pushes_enabled?
      end

      field :allows_deletions, Boolean,
        description: "Can this branch be deleted.",
        null: false,
        visibility: :under_development

      def allows_deletions
        !@object.block_deletions_enabled?
      end

      field :matching_refs, Connections::Ref,
        resolver: Resolvers::Refs,
        description: "Repository refs that are protected by this rule",
        null: false,
        connection: true

      field :branch_protection_rule_conflicts, Connections.define(Objects::BranchProtectionRuleConflict),
        description: "A list of conflicts matching branches protection rule and other branch protection rules",
        connection: true,
        null: false

      def branch_protection_rule_conflicts
        @object.async_repository.then do |repository|
          Platform::Loaders::BranchProtectionRule::MatchesAndConflicts.load(repository, @object).then do |information|
            conflict_information = information[:conflicts].map do |data|
              Platform::Models::BranchProtectionRuleConflict.new(@object, data[:ref], data[:conflicting_protected_branch])
            end

            ArrayWrapper.new(conflict_information)
          end
        end
      end
    end
  end
end
