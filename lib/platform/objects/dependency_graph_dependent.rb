# frozen_string_literal: true

module Platform
  module Objects
    class DependencyGraphDependent < Platform::Objects::Base
      description "A package dependent"

      areas_of_responsibility :dependency_graph

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        true
      end

      visibility :internal

      minimum_accepted_scopes ["public_repo"]

      field :cursor, String, null: true

      field :manifest_filename, String, description: "Filename of the manifest containing this dependent", null: true
      field :manifest_path, String, description: "Path of the manifest containing this dependent", null: true
      field :name, String, description: "The dependent name if applicable", null: true
      field :repository, Objects::Repository, description: "The repository containing the dependent", null: true
      def repository
        Loaders::ActiveRecord.load(::Repository, @object.repository_id, security_violation_behaviour: :nil)
      end

      url_fields prefix: :manifest_blob, description: "URL to the blob for this manifest path", null: true do |dependent|
        Loaders::ActiveRecord.load(::Repository, dependent.repository_id, security_violation_behaviour: :nil).then do |repository|
          next nil if repository.nil?

          # Load the owner and network so we can construct our path based off of the owner name and default branch.
          Promise.all([repository.async_owner, repository.async_default_branch]).then do |owner, default_branch|
            repo_blob_path = Addressable::Template.new("/{owner}/{name}/blob/{branch}/").expand(
              branch: default_branch,
              name: repository.name,
              owner: owner.to_s,
            )
            # Use URI joining here to append the manifest path, given that it's user-generated and we don't know what'll be there
            repo_blob_path.join(dependent.manifest_path)
          end
        end
      end
    end
  end
end
