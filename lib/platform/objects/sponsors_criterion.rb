# frozen_string_literal: true

module Platform
  module Objects
    class SponsorsCriterion < Platform::Objects::Base
      description "An criterion used when evaluating Sponsors applications."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        permission.viewer.can_admin_sponsors_listings?
      end

      visibility :internal

      minimum_accepted_scopes ["biztools"]
      implements Platform::Interfaces::Node
      global_id_field :id

      created_at_field
      updated_at_field

      field :slug, String, description: "The slug of the criterion.", null: false
      field :description, String, description: "The description of the criterion.", null: false
      field :criterion_type, Enums::SponsorsCriterionType, description: "The input type of this criterion.", null: false
      field :is_automated, Boolean, description: "Indicates if determining if an application meets this criterion is an automated process.", method: :automated?, null: false
    end
  end
end
