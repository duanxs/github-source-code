# frozen_string_literal: true

module Platform
  module Objects
    class SponsorsMembershipAdminInfo < Platform::Objects::Base
      description "Sponsors membership information only visible to members"

      def self.async_api_can_access?(permission, _object)
        permission.hidden_from_public?(self)
      end

      def self.async_viewer_can_see?(permission, object)
        permission.viewer.biztools_user? || permission.viewer.site_admin?
      end

      visibility :internal

      minimum_accepted_scopes ["biztools"]

      field :reviewer, Objects::User, description: "The user that reviewed this application.", null: true

      def reviewer
        @object.membership.async_reviewer
      end

      field :reviewed_at, Scalars::DateTime, description: "When this application was last reviewed", null: true

      def reviewed_at
        @object.membership.reviewed_at
      end

      field :is_accepted, Boolean, description: "Whether this user's Sponsors membership has been accepted.", null: false

      def is_accepted
        @object.membership.accepted?
      end

      field :state, Enums::SponsorsMembershipState, description: "The current state of this application.", null: false

      def state
        @object.membership.current_state.name
      end

      field :has_unmet_automated_criteria, Boolean, description: "Indicates if this application is not meeting at least one criterion.", null: false

      def has_unmet_automated_criteria
        @object.membership.has_unmet_automated_criteria?
      end

      field :survey_answers, Connections.define(Objects::SurveyAnswer), description: "The answers this user provided for the associated survey.", null: true

      def survey_answers
        @object.membership.survey_answers
      end

      field :criteria, Connections.define(Objects::SponsorsMembershipsCriterion), description: "The criteria values for this membership.", null: false do
        argument :filter, Enums::SponsorsCriterionFilter,
          description: "The condition on which to filter criteria.",
          required: false,
          default_value: :all
      end

      def criteria(filter:)
        scope = @object.membership.sponsors_memberships_criteria

        case filter
        when :automated
          scope = scope.automated
        when :manual
          scope = scope.manual
        end

        scope
      end

      field :is_featured, Boolean, description: "Whether this membership is featured on the Sponsors landing page.", null: false

      def is_featured
        @object.membership.featured?
      end

      field :featured_description, String, description: "The description used on the Sponsors landing page.", null: true

      def featured_description
        @object.membership.featured_description
      end
    end
  end
end
