# frozen_string_literal: true

module Platform
  module Objects
    class JobDepartment < Platform::Objects::Base
      description "A GitHub organizational department that has open job positions."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        true
      end

      minimum_accepted_scopes ["site_admin"]
      visibility :internal

      field :title, String, description: "The title of the department: Engineering, Marketing, etc.", null: false

      field :positions, [Objects::JobPosition], description: "A list of open positions for this department.", method: "postings", null: false
    end
  end
end
