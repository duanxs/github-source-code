# frozen_string_literal: true

module Platform
  module Objects
    class DependencyGraphRepositoryPackageReleaseLicense < Platform::Objects::Base
      description "Repository package release license stats"

      areas_of_responsibility :dependency_graph

      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      def self.async_viewer_can_see?(permission, object)
        true
      end

      visibility :internal

      minimum_accepted_scopes ["public_repo"]

      field :license_id, String, "License spdx id", null: false
      field :total_count, Integer, "Count of repository package releases with this license", null: false
    end
  end
end
