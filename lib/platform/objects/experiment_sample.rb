# frozen_string_literal: true

module Platform
  module Objects
    class ExperimentSample < Platform::Objects::Base
      description "Represents a sample for a Scientist experiment."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        ::Experiment.viewer_can_read?(permission.viewer)
      end

      visibility :internal
      minimum_accepted_scopes ["devtools"]

      field :sample_threshold, Float, "The sample threshold for this sample.", null: false
      field :control_duration, Float, "The control duration for this sample.", null: false
      field :control_difference, Float, "The control difference for this sample.", null: false
      field :backtrace, [Objects::ExperimentBacktrace], "The backtrace for this experiment score", null: false
      field :raw, String, "The raw sample as JSON", null: false
      def raw
        @object.raw.to_json
      end

      field :candidate_durations, [Objects::ExperimentCandidateDuration], "The candidate durations for this sample", null: false
      def candidate_durations
        @object.candidate_durations.map do |name, duration, difference|
          Platform::Models::ExperimentSampleCandidateDuration.new(
            name: name,
            duration: duration,
            difference: difference,
          )
        end
      end
    end
  end
end
