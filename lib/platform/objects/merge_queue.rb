# frozen_string_literal: true

module Platform
  module Objects
    class MergeQueue < Platform::Objects::Base
      description "The (internal for now) queue of entries to be merged for this repository"

      implements Platform::Interfaces::Node
      global_id_field :id
      visibility :internal
      areas_of_responsibility :pull_requests

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, deployment)
        permission.async_repo_and_org_owner(deployment).then do |repo, org|
          permission.access_allowed?(:read_deployment, resource: repo, current_repo: repo, current_org: org, allow_integrations: true, allow_user_via_integration: true)
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, deployment)
        permission.belongs_to_repository(deployment)
      end

      scopeless_tokens_as_minimum

      field :repository, Objects::Repository, description: "The repository this merge queue belongs to", null: true, method: :async_repository

      field :entries, Connections.define(Objects::MergeQueueEntry), description: "The entries in the queue", null: true, visibility: :internal
    end
  end
end
