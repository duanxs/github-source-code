# frozen_string_literal: true

module Platform
  module Objects
    class SubscriptionItem < Platform::Objects::Base
      model_name "Billing::SubscriptionItem"
      description "An account's subscription item, representing something they pay for"

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        object.async_subscribable.then do |subscribable|
          subscribable.async_listing.then do |listing|
            if listing
              listing.async_owner.then do
                if permission.current_integratable_owner.present?
                  object.async_viewable_by?(permission.current_integratable_owner).then do |viewable|
                    if viewable
                      true
                    else
                      permission.viewer.present? && object.async_viewable_by?(permission.viewer)
                    end
                  end
                else
                  permission.viewer.present? && object.async_viewable_by?(permission.viewer)
                end
              end
            else
              permission.viewer.present? && object.async_viewable_by?(permission.viewer)
            end
          end
        end
      end

      visibility :internal
      minimum_accepted_scopes ["user"]

      implements Platform::Interfaces::Node

      global_id_field :id

      database_id_field(visibility: :internal)

      field :created_at, Scalars::DateTime, "Identifies the date and time when the subscription item was first created.", null: true

      field :updated_at, Scalars::DateTime, "Identifies the date and time when the subscription item was last updated.", null: true

      def pending_cycle_changes
        @object.async_account.then do |acct|
          next nil unless acct
          acct.async_pending_cycle_change
        end
      end

      def pending_subscription_item_changes(actor)
        actor.then do |result|
          next nil unless result
          result.async_pending_subscription_item_changes
        end
      end

      field :has_pending_cycle_change, Boolean, description: "Returns whether the subscription item has a pending plan change", null: false

      def has_pending_cycle_change
        pending_subscription_item_changes(pending_cycle_changes).then do |change_list|
          next false unless change_list
          Promise.all(change_list.map(&:async_listing)).then do |change_listings|
            next false unless change_listings
            @object.async_subscribable.then do |subscribable|
              subscribable.async_listing.then do |listing|
                change_listings.include?(listing)
              end
            end
          end
        end
      end

      field :pending_change_id, Integer, description: "Returns the id of a pending plan change", null: true

      def pending_change_id
        subscribable_pending_change.then do |pending_subscription_item_change|
          next nil unless pending_subscription_item_change
          pending_subscription_item_change.async_pending_plan_change.then do |plan_change|
            plan_change.id
          end
        end
      end

      field :marketplace_pending_change, PendingMarketplaceChange, description: "Pending change related to this item", null: true

      def marketplace_pending_change
        pending_subscription_item_changes(@object.async_account).then do |changes|
          next nil unless changes
          @object.async_subscribable.then do |subscribable|
            subscribable.async_listing.then do |listing|
              changes.find do |change|
                # Is the listing_plan associated with this change a member of the same parent listing
                # as the subscription item @object?
                change.subscribable_is_marketplace_listing_plan? &&
                  listing.listing_plans.where(id: change.subscribable_id).any?
              end
            end
          end
        end
      end

      field :subscribable_pending_change, PendingSubscribableChange, description: "Pending change related to this item", null: true

      def subscribable_pending_change
        pending_subscription_item_changes(@object.async_account).then do |changes|
          next nil unless changes
          @object.async_subscribable.then do |subscribable|
            Promise.all([async_sponsors_tier_ids, async_marketplace_listing_plan_ids]).then do |sponsors_tier_ids, marketplace_listing_plan_ids|
              changes.find do |change|
                # Is the subscribable associated with this change a member of the same parent listing
                # as the subscription item @object?
                (change.subscribable_type == Marketplace::ListingPlan.name && change.subscribable_id.in?(marketplace_listing_plan_ids)) ||
                  (change.subscribable_type == SponsorsTier.name && change.subscribable_id.in?(sponsors_tier_ids))
              end
            end
          end
        end
      end

      def async_sponsors_tier_ids
        @object.async_subscribable.then do |subscribable|
          next [] unless subscribable.is_a?(SponsorsTier)
          SponsorsTier.where(sponsors_listing_id: subscribable.sponsors_listing_id).pluck(:id)
        end
      end

      def async_marketplace_listing_plan_ids
        @object.async_subscribable.then do |subscribable|
          next [] unless subscribable.is_a?(Marketplace::ListingPlan)
          Marketplace::ListingPlan.where(marketplace_listing_id: subscribable.marketplace_listing_id).pluck(:id)
        end
      end

      field :quantity, Integer, "The number of units for this item.", null: false

      field :paid, Boolean, description: "Returns true if the subscription item is not free or on free trial", null: false

      def paid
        @object.async_listing_plan.then { @object.paid? }
      end

      field :on_free_trial, Boolean, description: "Is the subscription item currently on free trial", null: false, method: :on_free_trial?

      field :free_trial_ends_on, Scalars::DateTime, description: "Date the free trial ends", null: true

      def free_trial_ends_on
        @object.free_trial_ends_on.try(:to_datetime)
      end

      field :billing_cycle, String, description: "The billing cycle for the plan associated with this item", null: false

      def billing_cycle
        @object.async_plan_subscription.then do |plan_subscription|
          plan_subscription.async_user.then do
            plan_subscription.plan_duration # delegated through the user
          end
        end
      end

      field :next_billing_date, Scalars::DateTime, description: "The next billing date for the plan associated with this item", null: true

      def next_billing_date
        @object.async_plan_subscription.then do |plan_subscription|
          plan_subscription.async_user.then do |user|
            user.next_billing_date.try(:to_datetime) # delegated through the user
          end
        end
      end

      field :account_has_been_charged, Boolean, description: "Whether an account has been charged for this subscription, during signup or at the end of a free trial", null: false

      def account_has_been_charged
        @object.async_subscribable.then do |subscribable|
          @object.account_has_been_charged?
        end
      end

      # N.B Need to figure out what a Currency Scalar looks like
      field :price, Scalars::Money, description: "The total price for the item.", null: false do
        argument :free_trial, Boolean, "Should the price check for an eligible free trial?", default_value: true, required: false
        argument :duration, Platform::Enums::Billing::Duration, "The duration for which the price should be computed. Defaults to the current plan duration.", required: false
      end

      def price(**arguments)
        @object.async_subscribable.then { @object.price(trial_price: arguments[:free_trial], duration: arguments[:duration]) }
      end

      field :post_trial_prorated_price, Scalars::Money, description: "The price for this item after the free trial ends.", null: true
      def post_trial_prorated_price
        subscribable_pending_change.then do |pending_subscription_item_change|
          pending_subscription_item_change&.price
        end
      end

      field :post_trial_bill_date, Scalars::DateTime, description: "The next_billing_date after the free trial ends.", null: false
      def post_trial_bill_date
        pricing_promise do |user|
          post_trial_bill_date = user.subscription.next_bill_date_after \
            date: user.subscription.free_trial_end_date
        end
      end

      # Load some pricing-related values,
      # then yield to the block.
      # This is useful for fields that need pricing info.
      def pricing_promise
        @object.async_account.then do |user|
          relations = [
            user.async_coupons,
            user.async_asset_status,
            user.async_pending_cycle_change,
            user.async_pending_subscription_item_changes,
            user.async_plan_subscription,
          ]

          Promise.all(relations).then do |_, _, _, _, plan_subscription|
            if plan_subscription
              items_plans_promise = plan_subscription.async_subscription_items.then do |items|
                Promise.all(items.map(&:async_subscribable))
              end
              items_plans_promise.then { yield(user) }
            else
              yield(user)
            end
          end
        end
      end


      field :authorization_required, Boolean, description: "Has the purchased product been authorized for the account?", null: false

      def authorization_required
        @object.async_subscribable.then do |subscribable|
          subscribable.async_listing.then do |listing|
            @object.async_account.then do |account|
              listing.async_listable.then do |listable|
                if listing.listable_is_oauth_application?
                  @context[:viewer] && @context[:viewer].oauth_authorizations.
                    where(application_id: listing.listable_id).empty?
                elsif listing.listable_is_integration?
                  !listable.installed_on?(account)
                elsif listing.listable_is_sponsorable?
                  true # TODO: what should the logic be here?
                end
              end
            end
          end
        end
      end

      field :is_installed, Boolean, description: "Whether the item has been installed. Only relevant for GitHub or OAuth apps purchases.", null: false

      def is_installed
        @object.installed_at.present?
      end

      field :subscribable, Unions::BillingSubscribable, description: "The object that this is a subscription to.", null: false

      field :marketplace_listing, MarketplaceListing, method: :async_listing, description: "The marketplace listing that this is a purchase of", null: true

      field :account, Platform::Unions::Account, visibility: :internal, description: "The account associated with this subscription item", null: true

      def account
        @object.async_plan_subscription.then do |plan_subscription|
          plan_subscription.async_user
        end
      end

      field :formatted_total_price, String, description: "The total price for this subscription item, based on the owner's plan duration, formatted as a money string.", null: false

      def formatted_total_price
        @object.async_subscribable.then do |subscribable|
          @object.async_plan_subscription.then do |plan_subscription|
            plan_subscription.async_user.then do |owner|
              item = Billing::SubscriptionItem.new \
                subscribable: subscribable,
                quantity: @object.quantity

              Billing::Pricing.new(
                plan_duration: owner.plan_duration,
                subscription_item: item,
              ).marketplace_item_cost.format
            end
          end
        end
      end

      field :prorated_total_price_in_cents, Integer, description: "The total price for this subscription item, prorated to end of the owner's current billing cycle, in cents.", null: false

      def prorated_total_price_in_cents
        @object.async_subscribable.then do |subscribable|
          @object.async_plan_subscription.then do |plan_subscription|
            plan_subscription.async_user.then do |owner|
              subscribable.prorated_total_price(account: owner, quantity: @object.quantity).cents
            end
          end
        end
      end

      field :formatted_prorated_total_price, String, description: "The total price for this subscription item, prorated to end of the owner's current billing cycle, formatted as a money string.", null: false

      def formatted_prorated_total_price
        @object.async_subscribable.then do |subscribable|
          @object.async_plan_subscription.then do |plan_subscription|
            plan_subscription.async_user.then do |owner|
              subscribable.prorated_total_price(account: owner, quantity: @object.quantity).format
            end
          end
        end
      end

      field :viewer_can_admin, Boolean, description: "Can the current user cancel or edit this subscription item.", null: false

      def viewer_can_admin
        @object.async_adminable_by?(@context[:viewer])
      end

      field :listable_is_sponsorable, Boolean, description: "Whether the subscription item is tied to a sponsorable type marketplace listing.", null: false

      def listable_is_sponsorable
        @object.async_listing.then do
          @object.listable_is_sponsorable?
        end
      end
    end
  end
end
