# frozen_string_literal: true

module Platform
  module Objects
    class SponsorshipNewsletter < Platform::Objects::Base
      description "A sponsorship newsletter"
      visibility :internal

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, object)
        return false if permission.viewer.nil?
        return true if permission.viewer.can_admin_sponsors_newsletters?
        object.async_sponsorable.then do |sponsorable|
          permission.viewer == sponsorable || sponsorable.async_adminable_by?(permission.viewer)
        end
      end

      # Determine whether the viewer can see this object (called internally).
      #
      # TODO: Should this take into consideration whether or not the viewer is
      #       a sponsor?
      #
      # Returns true
      def self.async_viewer_can_see?(permission, object)
        return false if permission.viewer.nil?
        return true if permission.viewer.can_admin_sponsors_newsletters?
        object.async_sponsorable.then do |sponsorable|
          permission.viewer == sponsorable || sponsorable.async_adminable_by?(permission.viewer)
        end
      end

      minimum_accepted_scopes ["user"]

      implements Platform::Interfaces::Node
      global_id_field :id

      field :subject, String, "The newsletter subject", null: false
      field :body, String, "The newsletter body", null: false
      field :author, Objects::User, "The author of the newsletter", null: false, method: :async_author_or_ghost
      field :sponsorable, Interfaces::Sponsorable, "The sponsorable this newsletter is for", null: false, method: :async_sponsorable
      field :is_draft, Boolean, "True if the update is in a draft state", method: :draft?, null: false
      field :is_published, Boolean, "True if the update is in a published state", method: :published?, null: false
      field :is_for_all_tiers, Boolean, "Indicates if this newsletter is available to all sponsorship tiers", method: :for_all_tiers?, null: false
      field :sponsors_tiers, Connections.define(Objects::SponsorsTier),
        description: "The tiers that have been granted direct access to this newletter",
        connection: true,
        null: true

      created_at_field
      updated_at_field
    end
  end
end
