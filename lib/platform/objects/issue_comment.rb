# frozen_string_literal: true

module Platform
  module Objects
    class IssueComment < Platform::Objects::Base
      description "Represents a comment on an Issue."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, issue_comment)
        issue_comment.async_issue.then do |issue|
          issue.async_pull_request.then do
            permission.async_repo_and_org_owner(issue_comment).then do |repo, org|
              access_type = issue.pull_request? ? :get_pull_request_comment : :get_issue_comment
              permission.access_allowed?(access_type, repo: repo, resource: issue_comment, current_org: org, allow_integrations: true, allow_user_via_integration: true)
            end
          end
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        permission.belongs_to_issue(object)
      end

      scopeless_tokens_as_minimum

      implements Platform::Interfaces::Node
      implements Interfaces::Comment
      implements Interfaces::Deletable
      implements Interfaces::Minimizable
      implements Interfaces::Updatable
      implements Interfaces::UpdatableComment
      implements Interfaces::PerformableViaApp
      implements Interfaces::Reactable
      implements Interfaces::Reportable
      implements Interfaces::Blockable
      implements Interfaces::AbuseReportable
      implements Interfaces::RepositoryNode
      implements Interfaces::Trigger

      global_id_field :id

      url_fields description: "The HTTP URL for this issue comment" do |issue_comment|
        issue_comment.async_path_uri
      end

      field :body_version, String, visibility: :internal, description: "Identifies the comment body hash.", null: false

      field :body_html, Scalars::HTML, description: "The body rendered to HTML.", null: false do
        argument :hide_code_blobs, Boolean, "Whether or not to include the HTML for code blocks", required: false, default_value: false, feature_flag: :pe_mobile
        argument :render_suggested_changes_as_text, Boolean, "Whether or not to include the HTML for suggested changes", required: false, default_value: false, feature_flag: :pe_mobile
      end

      def body_html(hide_code_blobs: false, render_suggested_changes_as_text: false)
        @object.async_repository.then do |repo|
          repo.async_owner.then do
            repo.async_network.then do
              if hide_code_blobs
                @object.async_body_html!(context: {hide_code_blobs: hide_code_blobs}).then do |body_html|
                  body_html || GitHub::HTMLSafeString::EMPTY
                end
              else
                @object.async_body_html.then do |body_html|
                  body_html || GitHub::HTMLSafeString::EMPTY
                end
              end
            end
          end
        end
      end

      field :issue, Objects::Issue, method: :async_issue, description: "Identifies the issue associated with the comment.", null: false

      field :pull_request, Objects::PullRequest, description: <<~DESCRIPTION, null: true do
          Returns the pull request associated with the comment, if this comment was made on a
          pull request.
        DESCRIPTION
      end

      def pull_request
        @object.async_issue.then(&:async_pull_request)
      end

      field :websocket, String, visibility: :internal, description: "The websocket channel ID for live updates.", null: false do
        argument :channel, Enums::IssuePubSubTopic, "The channel to use.", required: true
      end

      def websocket(channel:)
        case channel
        when "updated"
          GitHub::WebSocket::Channels.issue(@object)
        end
      end
    end
  end
end
