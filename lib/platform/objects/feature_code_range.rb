# frozen_string_literal: true

module Platform
  module Objects
    class FeatureCodeRange < Platform::Objects::Base
      description "A piece of code that checks a feature flag"

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, *)
        permission.viewer && (permission.viewer.site_admin? || permission.viewer.github_developer?)
      end

      visibility :internal

      minimum_accepted_scopes ["devtools"]

      field :filename, String, description: "The file of this code range", null: true
      field :first_line, Integer, description: "The first line of this code range", null: true
      field :last_line, Integer, description: "The last line of this code range", null: true
      field :code, String, description: "The code of the range", null: true
      field :context_line, String, description: "Additional context", null: true

      def context_line
        @object[:context] && @object[:context][:line]
      end

      field :context_line_number, Integer, description: "Line number of context", null: true

      def context_line_number
        @object[:context] && @object[:context][:lineno]
      end

      url_fields description: "The HTTP URL for this code range" do |range|
        template = Addressable::Template.new("/github/github/blob/{oid}/{+filename}#L{first_line}-L{last_line}")
        template.expand oid: GitHub.current_sha, filename: range[:filename], first_line: range[:first_line], last_line: range[:last_line]
      end
    end
  end
end
