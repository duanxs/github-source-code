# frozen_string_literal: true

module Platform
  module Objects
    class Sponsorship < Platform::Objects::Base
      description "A sponsorship relationship between a sponsor and a maintainer"

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, sponsorship)
        permission.access_allowed?(:read_sponsorship,
          resource: sponsorship,
          current_org: nil,
          current_repo: nil,
          allow_integrations: true,
          allow_user_via_integration: true,
        )
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        true
      end

      minimum_accepted_scopes ["user"]
      global_id_field :id
      implements Platform::Interfaces::Node

      database_id_field visibility: :internal
      created_at_field

      field :is_sponsor_opted_in_to_email, Boolean, visibility: :internal, description: "True if the sponsor has opted in to sponsorship updates.", null: false
      field :privacy_level, Enums::SponsorshipPrivacy, description: "The privacy level for this sponsorship.", null: false
      field :maintainer, Objects::User, "The entity that is being sponsored", null: false do
        deprecated(
          start_date: Date.new(2019, 12, 17),
          reason: "`Sponsorship.maintainer` will be removed.",
          superseded_by: "Use `Sponsorship.sponsorable` instead.",
          owner: "antn",
        )
      end

      def maintainer
        if Rails.env.development?
          raise Platform::Errors::Unprocessable, "Sponsorship.maintainer is deprecated, please use Sponsorship.sponsorable"
        end

        @object.async_sponsorable
      end

      field :sponsorable, Interfaces::Sponsorable, "The entity that is being sponsored", null: false, method: :async_sponsorable
      field :sponsor, Objects::User, "The user that is sponsoring. Returns null if the sponsorship is private or if sponsor is not a user.", null: true do
        deprecated(
          start_date: Date.new(2020, 04, 27),
          reason: "`Sponsorship.sponsor` will be removed.",
          superseded_by: "Use `Sponsorship.sponsorEntity` instead.",
          owner: "nholden",
        )
      end

      def sponsor
        sponsor_entity.then do |entity|
          # We only want the `sponsor` field to return `User` sponsors. We must
          # inspect `entity.class` here because `entity.is_a?(::User)` will
          # return `true` for subclasses of `User`, including `Organization`.
          entity if entity.class == ::User
        end
      end

      field :sponsor_entity, Unions::Sponsor, "The user or organization that is sponsoring. Returns null if the sponsorship is private.", null: true

      def sponsor_entity
        Promise.all([@object.async_sponsorable, @object.async_sponsor]).then do |sponsorable, sponsor|
          next sponsor if @object.privacy_public?
          next sponsor if @context[:viewer]&.can_admin_sponsors_listings?

          sponsorable.async_sponsors_listing.then do |sponsors_listing|
            sponsors_listing.async_adminable_by?(@context[:viewer]).then do |adminable_by|
              adminable_by ? sponsor : nil
            end
          end
        end
      end

      field :tier, Objects::SponsorsTier, description: "The associated sponsorship tier", null: true

      def tier
        @object.async_subscription_item.then do |subscription_item|
          subscription_item.async_viewable_by?(context[:viewer]).then do |viewable_by|
            next unless viewable_by

            subscription_item.async_subscribable.then do |subscribable|
              subscribable
            end
          end
        end
      end

      field :has_pending_cancellation, Boolean, visibility: :internal, description: "Is this sponsorship scheduled to be canceled?", method: :async_has_pending_cancellation?, null: false
    end
  end
end
