# frozen_string_literal: true

module Platform
  module Objects
    class SponsorsTier < Platform::Objects::Base
      description "A GitHub Sponsors tier associated with a GitHub Sponsors listing."

      visibility :public, environments: [:dotcom]
      visibility :internal, environments: [:enterprise]

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, object)
        object.async_sponsors_listing.then do |sponsors_listing|
          sponsors_listing.async_sponsorable.then do |sponsorable|
            org = sponsorable.organization? ? sponsorable : nil
            permission.access_allowed?(:read_sponsors_listing,
              resource: object,
              sponsors_listing: sponsors_listing,
              current_org: org,
              current_repo: nil,
              allow_integrations: false,
              allow_user_via_integration: false,
            )
          end
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      # Ability check for a SponsorsTier object
      #
      # Adapted from MarketplaceListingPlan GraphQL object.
      def self.async_viewer_can_see?(permission, object)
        object.async_readable_by?(permission.viewer)
      end

      # We currently only support user to user sponsorships.
      # Add "admin:org" for org and "repo" for repo sponsorables in future.
      minimum_accepted_scopes ["user"]

      implements Platform::Interfaces::Node

      created_at_field
      updated_at_field
      global_id_field :id
      database_id_field(visibility: :internal)

      field :sponsors_listing, Objects::SponsorsListing, method: :async_sponsors_listing, description: "The sponsors listing that this tier belongs to.", null: false
      field :name, String, "The name of the tier.", null: false
      field :description, String, "The description of the tier.", null: false
      field :monthly_price_in_cents, Integer, "How much this tier costs per month in cents.", null: false
      field :monthly_price_in_dollars, Integer, "How much this tier costs per month in dollars.", null: false
      field :description_html, Scalars::HTML, description: "The tier description rendered to HTML", null: false
      def description_html
        @object.description_html(current_user: @context[:viewer])
      end

      field :yearly_price_in_cents, Integer, "How much this tier costs annually in cents.", null: false, visibility: :internal
      field :yearly_price_in_dollars, Integer, "How much this tier costs annually in dollars.", null: false, visibility: :internal
      field :is_per_unit, Boolean, "Is this tier charged per unit?", method: :per_unit, visibility: :internal, null: false
      field :can_publish, Boolean, "Can this tier be published?", method: :can_publish?, visibility: :internal, null: false
      field :can_be_retired, Boolean, "Can this tier be retired?", method: :can_be_retired?, visibility: :internal, null: false
      field :can_change_pricing, Boolean, "Can pricing on this tier be changed?", method: :can_change_pricing?, visibility: :internal, null: false
      field :state, Enums::SponsorsTierState, "The current state of the tier", visibility: :internal, null: false

      def state
        @object.current_state.name
      end

      field :subscription_items, Connections.define(Objects::SubscriptionItem),
        description: "A list of the subscription items associated with this Sponsors tier.",
        visibility:  :internal,
        null:        true

      def subscription_items
        if @context[:viewer].blank?
          ::Billing::SubscriptionItem.none
        elsif @context[:viewer].can_admin_sponsors_listings?
          Promise.resolve(@object.async_listing_plan).then do
            @object.subscription_items.active
          end
        else
          Promise.all([@object.async_sponsors_listing, @object.async_listing_plan]).then do |sponsors_listing, listing_plan|
            sponsors_listing.async_adminable_by?(@context[:viewer]).then do |can_admin|
              if can_admin
                @object.subscription_items.active
              else
                @object
                  .subscription_items
                  .joins(:plan_subscription)
                  .where(plan_subscriptions: { user_id: @context[:viewer].id })
                  .active
              end
            end
          end
        end
      end

      field :admin_info, Objects::SponsorsTierAdminInfo,
        description: "SponsorsTier information only visible to users that can administer the associated Sponsors listing.",
        null: true

      def admin_info
        @object.async_sponsors_listing.then do |listing|
          listing.async_adminable_by?(@context[:viewer]).then do |adminable_by|
            next if !adminable_by && !@context[:viewer]&.can_admin_sponsors_listings?
            Models::SponsorsTierAdminInfo.new(@object)
          end
        end
      end
    end
  end
end
