# frozen_string_literal: true

module Platform
  module Objects
    class Epoch < Objects::Base
      description "A collection of change operations made to a commit"

      scopeless_tokens_as_minimum

      global_id_field :id

      feature_flag :epochs
      areas_of_responsibility :epochs

      def self.async_api_can_access?(permission, epoch)
        permission.async_repo_and_org_owner(epoch).then do |repo, org|
          permission.access_allowed?(:get_ref, resource: repo, current_repo: repo, current_org: org, allow_integrations: true, allow_user_via_integration: true)
        end
      end

      def self.async_viewer_can_see?(permission, object)
        permission.belongs_to_git_object(object)
      end

      field :created_at, Scalars::DateTime, "When the epoch was created", null: false
      field :repository, Repository, "The repository the epoch belongs to", null: true
      field :base_commit_oid, String, "The base commit oid for the changes.", null: false
      field :operations, Connections.define(Objects::EpochOperation), max_page_size: 250, null: true, connection: true, method: :epoch_operations,
        description: "The operations which are being stored including buffer deletions, additions and changes."
    end
  end
end
