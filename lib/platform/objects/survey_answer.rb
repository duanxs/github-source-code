# frozen_string_literal: true

module Platform
  module Objects
    class SurveyAnswer < Platform::Objects::Base
      description "A single user answer to a question on a survey"

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        permission.viewer && permission.viewer.site_admin?
      end

      visibility :internal
      minimum_accepted_scopes ["devtools"]

      implements Platform::Interfaces::Node

      global_id_field :id

      field :text, String, description: "The text answer, if the question allows for text answers", method: :other_text, null: true
      field :question, Objects::SurveyQuestion, description: "The question for this answer", method: :async_question, null: true
      field :choice, Objects::SurveyChoice, description: "The associated choice for this answer", method: :async_choice, null: true
    end
  end
end
