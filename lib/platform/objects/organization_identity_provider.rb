# frozen_string_literal: true

module Platform
  module Objects
    class OrganizationIdentityProvider < Platform::Objects::Base
      model_name "Organization::SamlProvider"
      description "An Identity Provider configured to provision SAML and SCIM identities for Organizations"

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, object)
        object.async_target.then do |org|
          permission.access_allowed?(:v4_manage_org_users, resource: org, organization: org, current_repo: nil, allow_integrations: true, allow_user_via_integration: true)
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        object.async_target.then { |organization|
          organization.async_adminable_by?(permission.viewer).then { |adminable|
            next true if adminable
            next false unless permission.viewer.try(:installation)
            next true if organization.resources.members.readable_by?(permission.viewer)
            false
          }
        }
      end

      minimum_accepted_scopes ["admin:org"]

      implements Platform::Interfaces::Node

      global_id_field :id

      field :organization, Objects::Organization, method: :async_target, description: "Organization this Identity Provider belongs to", null: true

      field :external_identities, resolver: Platform::Resolvers::ExternalIdentities, description: "External Identities provisioned by this Identity Provider", connection: true

      field :sso_url, Scalars::URI, description: "The URL endpoint for the Identity Provider's SAML SSO.", null: true

      field :issuer, String, description: "The Issuer Entity ID for the SAML Identity Provider", null: true

      field :idp_certificate, Scalars::X509Certificate, description: "The x509 certificate used by the Identity Provder to sign assertions and responses.", null: true

      # http://www.datypic.com/sc/ds/e-ds_SignatureMethod.html
      field :signature_method, Scalars::URI, description: "The signature algorithm used to sign SAML requests for the Identity Provider.", null: true

      # http://www.datypic.com/sc/ds/e-ds_DigestMethod.html
      field :digest_method, Scalars::URI, description: "The digest algorithm used to sign SAML requests for the Identity Provider.", null: true
    end
  end
end
