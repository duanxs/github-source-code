# frozen_string_literal: true

module Platform
  module Objects
    class EnterpriseServerUserAccountEmail < Platform::Objects::Base
      implements Platform::Interfaces::Node

      model_name "EnterpriseInstallationUserAccountEmail"
      description "An email belonging to a user account on an Enterprise Server installation."

      scopeless_tokens_as_minimum

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, object)
        permission.access_allowed?(:enterprise_installation, resource: object,
          current_repo: nil, current_org: nil,
          allow_integrations: true, allow_user_via_integration: false)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        true # TODO: Implement permissions.
      end

      global_id_field :id
      created_at_field
      updated_at_field

      field :user_account, Objects::EnterpriseServerUserAccount,
        description: "The user account to which the email belongs.",
        null: false, method: :async_enterprise_installation_user_account
      field :email, String, description: "The email address.", null: false
      field :is_primary, Boolean,
        description: "Indicates whether this is the primary email of the associated user account.",
        method: :primary?, null: false
    end
  end
end
