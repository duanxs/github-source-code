# frozen_string_literal: true

module Platform
  module Objects
    class SponsorsMembershipsCriterion < Platform::Objects::Base
      description "An criterion value for a Sponsors application."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        permission.viewer.can_admin_sponsors_listings?
      end

      visibility :internal

      minimum_accepted_scopes ["biztools"]
      implements Platform::Interfaces::Node
      global_id_field :id

      created_at_field
      updated_at_field

      field :criterion, Objects::SponsorsCriterion,
        description: "The criterion that is being evaluated",
        method: :async_sponsors_criterion,
        null: false

      field :membership, Objects::SponsorsMembership,
        description: "The Sponsors membership associated with this criterion",
        method: :async_sponsors_membership,
        null: false

      field :reviewer, Objects::User,
        description: "The user that reviewed this criterion, if applicable",
        method: :async_reviewer,
        null: true

      field :is_met, Boolean,
        description: "Indicates if the criterion is met for this membership",
        method: :met?,
        null: false

      field :value, String,
        description: "The value associated with this criterion for this membership",
        null: true
    end
  end
end
