# frozen_string_literal: true

module Platform
  module Objects
    class SponsorsListing < Platform::Objects::Base
      description "A GitHub Sponsors listing."

      visibility :public, environments: [:dotcom]
      visibility :internal, environments: [:enterprise]

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, sponsors_listing)
        sponsors_listing.async_sponsorable.then do |sponsorable|
          org = sponsorable.organization? ? sponsorable : nil
          permission.access_allowed?(:read_sponsors_listing,
            resource: sponsors_listing,
            sponsors_listing: sponsors_listing,
            current_org: org,
            current_repo: nil,
            allow_integrations: false,
            allow_user_via_integration: false,
          )
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      # Ability check for a SponsorsListing object
      def self.async_viewer_can_see?(permission, object)
        if object.approved?
          true
        elsif permission.viewer.nil?
          false
        elsif permission.viewer.can_admin_sponsors_listings?
          true
        else
          permission.viewer.async_plan_subscription.then do
            if permission.viewer.subscription_items.for_sponsors_listing(object.id).exists?
              true
            else
              object.async_adminable_by?(permission.viewer)
            end
          end
        end
      end

      # We currently only support user to user sponsorships.
      # Add "admin:org" for org and "repo" for repo sponsorables in future.
      minimum_accepted_scopes ["user"]

      implements Platform::Interfaces::Node

      global_id_field :id

      created_at_field

      field :published_at, Scalars::DateTime, description: "The date and time this listing was published.", null: true, visibility: :under_development

      database_id_field(visibility: :internal)
      field :slug, String, "The short name of the listing.", null: false
      field :name, String, "The listing's full name.", null: false
      field :short_description, String, "The short description of the listing.", null: false

      def short_description
        @object.short_description || ""
      end

      field :full_description, String, "The full description of the listing.", null: false

      def full_description
        @object.full_description || ""
      end

      field :full_description_html, Scalars::HTML, description: "The full description of the listing rendered to HTML.", null: false

      def full_description_html
        Platform::Helpers::MarketplaceListingContent.html_for(
          @object, :full_description, { current_user: @context[:viewer] }
        )
      end

      field :stripe_authorization_code, String, "The Stripe authorization code used to fetch a user", null: true, visibility: :internal

      field :state, String, "The current state of the Sponsors Listing", null: false, visibility: :internal

      def state
        @object.current_state.name.to_s.humanize
      end

      field :viewer_can_approve, Boolean, description: "Can the current viewer approve this Sponsors listing.", null: false, visibility: :internal

      def viewer_can_approve
        @object.async_approvable_by?(@context[:viewer])
      end

      field :viewer_can_unpublish, Boolean, description: "Can the current viewer delist this Sponsors listing.", null: false, visibility: :internal

      def viewer_can_unpublish
        @object.unpublishable_by?(@context[:viewer])
      end

      field :remaining_tier_count, Integer, "How many more tiers can be added to this listing.", visibility: :internal, null: false
      field :is_public, Boolean, "Whether this listing has been approved.", visibility: :internal, null: false, method: :approved?
      field :is_verified_and_pending_approval, Boolean, "Whether this listing has been verified and pending approval", visibility: :internal, null: false, method: :verified_and_pending_approval?
      field :is_unverified_and_pending_approval, Boolean, "Whether this listing is unverified and pending approval", visibility: :internal, null: false, method: :unverified_and_pending_approval?
      field :is_draft, Boolean, "Whether this listing is in draft", visibility: :internal, null: false, method: :draft?
      field :has_failed_verification, Boolean, "Whether this listing has failed verification", visibility: :internal, null: false, method: :failed_verification?
      field :is_verified_state, Boolean, "Whether this listing is in the verified state.", visibility: :internal, null: false, method: :verified?
      field :is_approved, Boolean, "Whether this listing has been approved", visibility: :internal, null: false, method: :approved?
      field :is_disabled, Boolean, "Whether this listing has been disabled", visibility: :internal, null: false, method: :disabled?
      field :listable_is_sponsorable, Boolean, "Whether this listing is sponsorable", visibility: :internal, method: :listable_is_sponsorable?, null: false

      field :stripe_connect_account, Objects::StripeConnectAccount,
        "The Stripe Connect Account associated with this listing",
        visibility: :internal, null: true

      def stripe_connect_account
        @object.async_stripe_connect_account.then do |account|
          @context[:permission].typed_can_see?("StripeConnectAccount", account).then do |account_readable|
            account if account_readable
          end
        end
      end

      field :sponsorable, Interfaces::Sponsorable, method: :async_sponsorable, description: "The sponsorable entity for this GitHub Sponsors listing.", null: false, visibility: :under_development

      field :tiers, Connections.define(Objects::SponsorsTier),
        description: "The published tiers for this GitHub Sponsors listing.",
        connection: true,
        null: true do
        argument :order_by, Inputs::SponsorsTierOrder,
          "Ordering options for Sponsors tiers returned from the connection.", required: false,
          default_value: { field: "monthly_price_in_cents", direction: "ASC" }
      end

      def tiers(order_by:)
        scope = @object.sponsors_tiers.where(state: ::SponsorsTier.state_value(:published))
        scope.order("sponsors_tiers.#{order_by[:field]} #{order_by[:direction]}")
      end

      field :admin_info, Objects::SponsorsListingAdminInfo,
        description: "SponsorsListing information only visible to users that can administrate the listing.",
        null: true,
        visibility: :under_development

      def admin_info
        @object.async_adminable_by?(@context[:viewer]).then do |adminable_by|
          next if !adminable_by && !@context[:viewer]&.can_admin_sponsors_listings?
          Models::SponsorsListingAdminInfo.new(@object)
        end
      end

      field :subscription_items, Connections.define(Objects::SubscriptionItem),
        description: "A list of the subscription items associated with this Sponsors listing.",
        visibility:  :internal,
        null:        false

      def subscription_items
        if @context[:viewer].blank?
          ::Billing::SubscriptionItem.none
        elsif @context[:viewer].can_admin_sponsors_listings?
          @object.subscription_items.active
        else
          @object.async_adminable_by?(@context[:viewer]).then do |can_admin|
            if can_admin
              @object.subscription_items.active
            else
              @object
                .subscription_items
                .joins(:plan_subscription)
                .where(plan_subscriptions: { user_id: @context[:viewer].id })
                .active
            end
          end
        end
      end

      field :subscription_value, Integer, description: "The combined monthly recurring value in cents of active sponsorships for this listing.",
        null: true,
        visibility: :under_development

      def subscription_value
        if @context[:viewer]&.can_admin_sponsors_listings?
          @object.subscription_value
        else
          @object.async_adminable_by?(@context[:viewer]).then do |can_admin|
            @object.subscription_value if can_admin
          end
        end
      end

      field :is_on_payout_probation, Boolean,
        description: "Indicates if this listing is within the payout probation period",
        visibility: :internal,
        method: :on_payout_probation?,
        null: false

      field :has_completed_payout_probation, Boolean,
        description: "Indicates if this listing has completed the payout probation period",
        visibility: :internal,
        method: :completed_payout_probation?,
        null: false

      field :payout_probation_started_at, Scalars::DateTime,
        description: "The date that the payout probation period started for this listing",
        visibility: :internal,
        null: true

      field :payout_probation_ended_at, Scalars::DateTime,
        description: "The date that the payout probation period ended for this listing",
        visibility: :internal,
        null: true

      field :match_disabled, Boolean,
        description: "Whether or not match is enabled for this listing",
        visibility: :internal,
        null: false,
        method: :match_disabled?

      field :next_payout_date, Scalars::Date,
        description: "The date that this listing is eligible to receive a payout",
        visibility: :internal,
        null: true
    end
  end
end
