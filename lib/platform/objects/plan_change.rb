# frozen_string_literal: true

module Platform
  module Objects
    class PlanChange < Platform::Objects::Base
      description "A plan change for a given subscription that provides pricing details"

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        true
      end

      visibility :internal

      scopeless_tokens_as_minimum

      field :final_price, Scalars::Money, description: "prorated final price to apply the plan change", null: false do
        argument :github_only, Boolean, "Should the final price only include GitHub items?", required: false
        argument :use_balance, Boolean, "Should the final price include the account balance?", required: false
      end

      def final_price(**arguments)
        @object.final_price github_only: arguments[:github_only], use_balance: arguments[:use_balance]
      end

      field :subscription_item, Objects::SubscriptionItem, description: "the subscription item tied to a plan change for a given listing plan", null: false do
        argument :subscribable_id, ID, "The ID for the listing plan or sponsors tier tied to the item", required: false
        argument :quantity, Integer, "An optional quantity to override the planChange amount.", required: false
      end

      def subscription_item(**arguments)
        Platform::Helpers::NodeIdentification.async_typed_object_from_id(
          Platform::Unions::SubscribableType.possible_types, arguments[:subscribable_id], @context
        ).then do |subscribable|
          item = @object.new_subscription.subscription_items
            .detect { |item| item.subscribable == subscribable }
          item.quantity = arguments[:quantity] if arguments[:quantity]
          item
        end
      end
    end
  end
end
