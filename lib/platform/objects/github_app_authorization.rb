# frozen_string_literal: true

module Platform
  module Objects
    class GitHubAppAuthorization < Platform::Objects::Base
      description "Represents an authorization to GitHub Application"

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        permission.belongs_to_viewer(object)
      end

      visibility :internal

      minimum_accepted_scopes ["site_admin"]

      implements Interfaces::OauthAuthorization
      implements Platform::Interfaces::Node
      global_id_field :id

      implements Interfaces::FeatureActor
      implements Interfaces::FeatureFlaggable

      field :application, Objects::OauthApplication, method: :async_application, description: "The associated OAuth application.", null: false
    end
  end
end
