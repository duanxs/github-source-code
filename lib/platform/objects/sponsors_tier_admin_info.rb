# frozen_string_literal: true

module Platform
  module Objects
    class SponsorsTierAdminInfo < Platform::Objects::Base
      description "SponsorsTier information only visible to users that can administer the associated Sponsors listing."
      visibility :public, environments: [:dotcom]
      visibility :internal, environments: [:enterprise]

      def self.async_api_can_access?(permission, object)
        object.tier.async_sponsors_listing.then do |sponsors_listing|
          permission.access_allowed?(:admin_sponsors_listing,
            resource: object.tier,
            sponsors_listing: sponsors_listing,
            current_org: nil,
            current_repo: nil,
            allow_integrations: false,
            allow_user_via_integration: false,
          )
        end
      end

      def self.async_viewer_can_see?(permission, object)
        return Promise.resolve(false) unless permission.viewer.present?
        return Promise.resolve(true) if permission.viewer.can_admin_sponsors_listings?

        object.tier.async_sponsors_listing.then do |sponsors_listing|
          sponsors_listing.async_adminable_by?(permission.viewer)
        end
      end

      minimum_accepted_scopes ["user"]

      field :sponsorships, Connections.define(Objects::Sponsorship), connection: true, description: "The sponsorships associated with this tier.", null: false do
        argument :include_private, Boolean,
          "Whether or not to include private sponsorships in the result set", required: false,
          default_value: false
        argument :order_by, Inputs::SponsorshipOrder,
          "Ordering options for sponsorships returned from this connection. If left blank, the sponsorships will be ordered based on relevancy to the viewer.", required: false
      end

      def sponsorships(include_private:, order_by: nil)
        @object.tier.async_sponsors_listing.then do |sponsors_listing|
          Platform::Loaders::SponsorshipsAsSponsorable.load(sponsors_listing.sponsorable_id, viewer: @context[:viewer], include_private: include_private, order_by: order_by, tier: @object.tier).then do |sponsorships|
            sponsorships ||= ::Sponsorship.none
            ArrayWrapper.new(sponsorships)
          end
        end
      end
    end
  end
end
