# frozen_string_literal: true

module Platform
  module Objects
    class Survey < Platform::Objects::Base
      description "A user facing survey"

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, _object)
        # TODO write proper permissions before making this object public
        permission.hidden_from_public?(self)
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        permission.viewer && permission.viewer.site_admin?
      end

      visibility :internal
      minimum_accepted_scopes ["devtools"]

      implements Platform::Interfaces::Node

      global_id_field :id

      field :title, String, "Survey title", null: true
      field :slug, String, "Survey title as a slug", null: false
      field :csv, String, method: :csv_dump, description: "A csv representation of the replies to this survey", null: false
      field :answers, Connections.define(Platform::Objects::SurveyAnswer), description: "Answers to all questions on this survey", null: true, connection: true

      field :questions, Connections.define(Platform::Objects::SurveyQuestion), description: "Questions asked on this survey", null: true, connection: true do
        argument :visible, Boolean, "Visibility of question, defaults to true", required: false
      end

      def questions(**arguments)
        @object.async_questions.then do |questions|
          if arguments[:visible] == false
            ArrayWrapper.new(questions.select(&:hidden))
          else
            ArrayWrapper.new(questions.reject(&:hidden))
          end
        end
      end

      field :respondents_count, Integer, description: "Total number of survey respondents", extras: [:execution_errors], null: false

      def respondents_count(execution_errors:)
        begin
          @object.respondents_count
        rescue ActiveRecord::QueryInterruption
          execution_errors.add("Respondents_count query timed out! A best-guess estimate has been returned. If you need an exact current count, please run the query via `gh-dbconsole analytics`.")

          q = @object.questions.count
          q.zero? ? 0 : answers.count / q
        end
      end
    end
  end
end
