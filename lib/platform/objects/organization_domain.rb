# frozen_string_literal: true

module Platform
  module Objects
    class OrganizationDomain < Platform::Objects::Base
      description "A domain that can be verified for an organization."

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, org_domain)
        return true if permission.viewer && permission.viewer.site_admin?
        org_domain.async_organization.then do |org|
          permission.access_allowed?(:v4_manage_org_users, resource: org, organization: org, current_repo: nil, allow_integrations: false, allow_user_via_integration: false)
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        if !GitHub.domain_verification_enabled?
          false
        elsif permission.viewer && permission.viewer.site_admin?
          true
        else
          object.async_organization.then { |organization|
            organization.direct_or_team_member?(permission.viewer)
          }
        end
      end

      minimum_accepted_scopes ["admin:org"]

      visibility :internal

      global_id_field :id
      implements Platform::Interfaces::Node

      database_id_field
      field :domain, Scalars::URI, "The unicode encoded domain.", null: false
      field :punycode_encoded_domain, Scalars::URI, description: "The punycode encoded domain.", null: false, method: :punycode_encoded_domain
      field :is_verified, Boolean, "Whether or not the domain is verified.", null: false, method: :verified?
      field :organization, Objects::Organization, method: :async_organization, description: "The organization the domain belongs to", null: false
      field :verification_token, String, description: "The current verification token for the domain.", null: true, method: :async_verification_token
      field :dns_host_name, Scalars::URI, description: "The DNS host name that should be used for verification.", null: true, method: :async_dns_host_name
      field :token_expiration_time, Scalars::DateTime, description: "The time that the current verification token will expire.", null: true, method: :async_token_expiration_time
      field :has_found_host_name, Boolean, description: "Whether a TXT record for verification with the expected host name was found.", null: false, method: :async_host_name_found?
      field :has_found_verification_token, Boolean, description: "Whether a TXT record for verification with the expected verification token was found.", null: false, method: :async_verification_token_found?
      field :is_required_for_policy_enforcement, Boolean, description: "Whether this domain is required to exist for an organization policy to be enforced.", null: false, method: :async_required_for_policy_enforcement?
    end
  end
end
