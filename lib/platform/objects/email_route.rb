# frozen_string_literal: true

module Platform
  module Objects
    class EmailRoute < Platform::Objects::Base
      description "Notification routing settings for an organization."
      areas_of_responsibility :notifications

      # Determine whether the viewer can access this object via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_access?(permission, object)
        object.settings.async_user.then do |user|
          permission.access_allowed?(:read_user_notification_settings, resource: user, current_repo: nil, current_org: nil, allow_integrations: false, allow_user_via_integration: false)
        end
      end

      # Determine whether the viewer can see this object (called internally).
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_viewer_can_see?(permission, object)
        return false unless permission.viewer

        object.settings.async_user.then do |user|
          user.readable_by?(permission.viewer)
        end
      end

      visibility :internal

      minimum_accepted_scopes ["user"]

      field :organization, Objects::Organization, description: "The organization the notification routing settings are for.", null: false

      field :user, Objects::User, description: "The user this route is configured for.", null: false

      def user
        @object.settings.async_user
      end

      field :email, String, description: "The email address notifications will be routed to.", null: true

      def email
        @object.settings.email(@object.organization)&.address
      end

      field :is_default_email, Boolean, description: "Is the configured email the default global notification email?", null: false

      def is_default_email
        email == @object.settings.email(:global)&.address
      end
    end
  end
end
