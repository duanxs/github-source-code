# frozen_string_literal: true

module Platform
  module Authorization
    # A short-lived object that calls through to our API authorization code.
    # Isolate the parts of code that coordinate with the `Authorization` module.
    class PermissionCheck
      include Authorization

      # Setup the object with some state from the parent `Permission` instance.
      # This object can't be `freeze`d because `Authorization` caches some data in ivars.
      def initialize(current_user:, installation:, integration:, operation:, response:, oauth:, current_repo:, current_org:, mutation:, saml_enforced_cache:, credential_authorization_cache:, ip_address_allowed_cache:, remote_ip: nil)
        @current_user = current_user
        @installation = installation
        @integration = integration
        @operation = operation
        @response = response
        @graphql_request = true
        @oauth = oauth
        @current_repo = current_repo
        @current_org = current_org
        @mutation = mutation
        @saml_enforced_cache = saml_enforced_cache
        @credential_authorization_cache = credential_authorization_cache
        @ip_address_allowed_cache = ip_address_allowed_cache
        @remote_ip = remote_ip
      end

      # This overrides Authorization#access_allowed?;
      # set it up with some GraphQL-specific stuff.
      def access_allowed?(action, **options)
        # OAP is always enforced for mutations; unauthorized Oauth Apps can
        # query public resources but cannot mutate them
        if @operation == :mutation
          options[:enforce_oauth_app_policy] = true
        else
          if options[:enforce_oauth_app_policy].nil?
            options[:enforce_oauth_app_policy] = @current_repo.nil? ? true : @current_repo.private?
          end
        end

        if @current_org.present?
          @current_org.async_saml_provider.sync
        end

        if @installation.present?
          options[:installation] = @installation
        elsif @integration.present?
          options[:integration] = @integration
        end

        super
      end

      # These override methods in `Authorization` to short-circuit (or implement) data requirements.
      #
      # Actually, not all of them are overrides: some are just implicitly
      # required by the methods in `Authorization`.
      def current_resource_owner
        @current_org
      end

      alias :find_org :current_resource_owner

      attr_reader :current_repo
      alias :find_repo :current_repo

      def current_repo_loaded?
        @current_repo.present?
      end

      attr_reader :response

      def graphql_request?
        true
      end

      def repo_nwo_from_path
        current_repo_loaded? && current_repo.nwo
      end

      def remote_ip
        @remote_ip
      end

      def params
        {}
      end

      # Internal: is the request by a user logged in with Oauth,
      # via an Integration?
      #
      # Returns a boolean.
      def integration_user_request?
        @current_user&.oauth_access&.integration_application_type?
      end

      # Override this to use the passed-in cache, which lasts for a whole GraphQL query
      def saml_enforced?(org, user)
        org_cache = @saml_enforced_cache[org]
        if org_cache.key?(user)
          org_cache[user]
        else
          org_cache[user] = super(org, user)
        end
      end

      # Override this to use the passed-in cache, which lasts for a whole GraphQL query
      def ip_address_allowed?(org, ip, actor)
        if @ip_address_allowed_cache.key?(org)
          @ip_address_allowed_cache[org]
        else
          @ip_address_allowed_cache[org] = super(org, ip, actor)
        end
      end

      # Override this to use the passed-in cache, which lasts for a whole GraphQL query
      def get_credential_authorization(org, oauth)
        org_cache = @credential_authorization_cache[org]
        if org_cache.key?(oauth)
          org_cache[oauth]
        else
          org_cache[oauth] = super(org, oauth)
        end
      end

      # Require an allowed IP when IP allow list enforcement is enabled?
      # Returns true by default.
      #
      # Returns Boolean.
      def require_allowed_ip?
        return @require_allowed_ip if defined?(@require_allowed_ip)

        @require_allowed_ip = true
      end
    end
  end
end
