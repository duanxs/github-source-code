# frozen_string_literal: true

module Platform
  module Mutations
    class FollowUser < Platform::Mutations::Base
      description "Follow a user."

      def self.async_api_can_modify?(permission, **inputs)
        permission.access_allowed?(:follow, resource: permission.viewer,
                                   current_repo: nil, current_org: nil,
                                   allow_integrations: false, allow_user_via_integration: true)
      end

      minimum_accepted_scopes ["user:follow"]
      areas_of_responsibility :user_profile

      argument :user_id, ID, "ID of the user to follow.", required: true, loads: Objects::User

      field :user, Objects::User, "The user that was followed.", null: true

      def resolve(user:)
        if context[:viewer].following?(user) || context[:viewer].follow(user, context: "api")
          { user: user }
        else
          raise Errors::Unprocessable.new("User failed to follow.")
        end
      end
    end
  end
end
