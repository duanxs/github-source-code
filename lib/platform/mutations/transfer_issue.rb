# frozen_string_literal: true

module Platform
  module Mutations
    class TransferIssue < Platform::Mutations::Base
      description "Transfer an issue to a different repository"

      minimum_accepted_scopes ["repo"]

      argument :issue_id, ID, "The Node ID of the issue to be transferred", required: true, loads: Objects::Issue
      argument :repository_id, ID, "The Node ID of the repository the issue should be transferred to", required: true, loads: Objects::Repository

      field :issue, Objects::Issue, "The issue that was transferred", null: true
      error_fields

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, repository:, issue:, **inputs)
        permission.async_owner_if_org(repository).then do |org|
          permission.access_allowed? :transfer_issue, repo: repository, issue: issue, current_org: org, allow_integrations: true, allow_user_via_integration: true
        end
      end

      def resolve(issue:, repository:)
        viewer = context[:viewer]

        transfer = IssueTransfer.new(
          old_issue: issue,
          old_repository: issue.repository,
          new_repository: repository,
          actor: viewer,
        )

        ensure_transfer_valid!(transfer)

        transfer.async_transfer!
        {
          issue: transfer.new_issue,
          errors: {},
        }
      end

      private

      def ensure_transfer_valid!(transfer)
        transfer.valid?
        validation_errors = transfer.errors
        raise Errors::Forbidden.new("Actor must be admin of repository") if validation_errors.include?(:actor)

        # detect specific validation error types
        data_errors = validation_errors
          .to_hash(true)
          .values_at(:old_issue, :old_repository, :new_repository)
          .compact.join(", ")

        if data_errors.present?
          raise Errors::Unprocessable.new(data_errors)
        end
      end
    end
  end
end
