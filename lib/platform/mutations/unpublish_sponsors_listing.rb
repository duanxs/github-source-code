# frozen_string_literal: true

module Platform
  module Mutations
    class UnpublishSponsorsListing < Platform::Mutations::Base
      description "Unpublish a Sponsors listing."

      visibility :internal
      minimum_accepted_scopes ["user"]

      argument :id, ID, "The ID of the Sponors listing to unpublish.", required: true, loads: Objects::SponsorsListing, as: :listing
      argument :message, String, "A custom message for the sponsor.", required: false

      field :sponsors_listing, Objects::SponsorsListing, "The updated Sponsors listing.", null: true

      def resolve(listing:, **inputs)
        Sponsors::UnpublishSponsorsListing.call(
          listing: listing,
          message: inputs[:message],
          viewer: context[:viewer],
        )
      rescue Sponsors::UnpublishSponsorsListing::ForbiddenError => e
        raise Errors::Forbidden.new(e.message)
      rescue Sponsors::UnpublishSponsorsListing::UnprocessableError => e
        raise Errors::Unprocessable.new(e.message)
      end
    end
  end
end
