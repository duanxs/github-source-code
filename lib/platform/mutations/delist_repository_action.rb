# frozen_string_literal: true

module Platform
  module Mutations
    class DelistRepositoryAction < Platform::Mutations::Base
      description "Remove a GitHub Action from the Marketplace."
      visibility :internal
      areas_of_responsibility :marketplace

      minimum_accepted_scopes ["repo"]

      argument :id, ID, "The ID of the RepositoryAction to delist.", required: false
      argument :slug, String, "The slug of the RepositoryAction to delist.", required: false

      field :repository_action, Objects::RepositoryAction, "The updated RepositoryAction.", null: true

      def resolve(**inputs)
        action = if inputs[:id].present?
          _, repository_action_id = Platform::Helpers::NodeIdentification.from_global_id(inputs[:id])
          ::RepositoryAction.find_by(id: repository_action_id)
        elsif inputs[:slug].present?
          ::RepositoryAction.find_by(slug: inputs[:slug])
        end

        unless action&.listed?
          raise Errors::NotFound.new("Listed RepositoryAction not found")
        end

        unless action.adminable_by?(context[:viewer]) || context[:viewer].can_admin_repository_actions?
          error_message = "#{context[:viewer]} does not have permission to delist the RepositoryAction"
          raise Errors::Forbidden.new(error_message)
        end

        action.delisted!

        { repository_action: action }
      end
    end
  end
end
