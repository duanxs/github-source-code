# frozen_string_literal: true

module Platform
  module Mutations
    class UpdateSpamLastSeenUserCursor < Platform::Mutations::Base
      description "Updates the Spamurai New User Queue last seen user cursor"
      visibility :internal
      minimum_accepted_scopes ["site_admin"]

      argument :cursor, String, "The cursor of the last seen user.", required: true

      field :cursor, String, "The updated cursor.", null: true

      def resolve(**inputs)
        if self.class.viewer_is_site_admin?(context[:viewer], self.class.name)
          GitHub.kv.set(Spam::LAST_SEEN_USER_CURSOR_KEY, inputs[:cursor])

          {cursor: inputs[:cursor]}
        else
          {cursor: nil}
        end
      end
    end
  end
end
