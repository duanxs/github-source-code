# frozen_string_literal: true

module Platform
  module Mutations
    class CreateMarketplaceListingPlanBullet < Platform::Mutations::Base
      description "Creates a bullet point on a Marketplace listing payment plan."
      visibility :internal
      areas_of_responsibility :marketplace

      minimum_accepted_scopes ["repo"]

      argument :value, String, "The contents of the bullet point.", required: true
      argument :plan_id, ID, "The ID of the payment plan to add the bullet to.", required: true, loads: Objects::MarketplaceListingPlan

      field :marketplace_listing_plan_bullet, Objects::MarketplaceListingPlanBullet, "The new bullet point.", null: true

      def resolve(plan:, **inputs)
        bullet = Marketplace::ListingPlanBullet.new(listing_plan: plan)

        unless bullet.allowed_to_edit?(context[:viewer])
          raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to create " +
                                         "a bullet for the Marketplace listing plan.")
        end

        bullet.value = inputs[:value]

        if bullet.save
          { marketplace_listing_plan_bullet: bullet }
        else
          errors = bullet.errors.full_messages.join(", ")
          raise Errors::Unprocessable.new("Could not create a bullet on the Marketplace " +
                                          "listing plan: #{errors}")
        end
      end
    end
  end
end
