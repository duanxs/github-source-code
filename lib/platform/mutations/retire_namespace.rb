# frozen_string_literal: true

module Platform
  module Mutations
    class RetireNamespace < Platform::Mutations::Base
      description "Retires a namespace."

      visibility :internal
      minimum_accepted_scopes ["site_admin"]

      argument :owner_login, String, "The owning user or organization's login", required: true
      argument :name, String, "The retired repository name", required: true

      field :retired_namespace, Objects::RetiredNamespace, "The retired namespace.", null: true

      def resolve(**inputs)
        unless context[:viewer].site_admin?
          raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to retire a namespace.")
        end

        retired = RetiredNamespace.retired? inputs[:owner_login], inputs[:name]
        raise Errors::Unprocessable.new("Namespace '#{inputs[:owner_login]}/#{inputs[:name]}' is already retired") if retired

        retired_namespace = RetiredNamespace.create! owner_login: inputs[:owner_login], name: inputs[:name]
        { retired_namespace: retired_namespace }
      end
    end
  end
end
