# frozen_string_literal: true

module Platform
  module Mutations
    class CreateSponsorsListing < Platform::Mutations::Base
      description "Create a user listing for GitHub Sponsors"
      visibility :internal

      minimum_accepted_scopes ["user"]

      argument :sponsorable_id, ID, "ID of the sponsorable enrolling", loads: Interfaces::Sponsorable, required: true
      argument :name, String, "The name of the user", required: true
      argument :country_code, String, "The country code for the country where payouts will be sent", required: true
      argument :email, String, "The email address to contact the sponsorable", required: false

      field :listing, Objects::SponsorsListing, description: "The created sponsors listing.", null: true

      def resolve(sponsorable:, **inputs)
        email = unless sponsorable.is_a?(PlatformTypes::Organization)
          UserEmail.find_by(email: inputs[:email]) || sponsorable.email
        end

        listing = Sponsors::CreateSponsorsListing.call(
          sponsorable: sponsorable,
          name: inputs[:name],
          country_code: inputs[:country_code],
          email: email,
        )

        { listing: listing }
      rescue Sponsors::CreateSponsorsListing::UnprocessableError => error
        raise Errors::Unprocessable.new(error.message)
      rescue Sponsors::CreateSponsorsListing::ResourceMissingError => error
        raise Errors::ResourceMissing.new(error.message)
      end
    end
  end
end
