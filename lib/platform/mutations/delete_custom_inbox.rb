# frozen_string_literal: true

module Platform
  module Mutations
    class DeleteCustomInbox < Platform::Mutations::Base
      include Helpers::Newsies

      description "Delete a custom inbox."
      minimum_accepted_scopes ["notifications"]
      visibility :under_development
      areas_of_responsibility :notifications

      argument :custom_inbox_id, ID, "The ID of the custom inbox to delete.", required: true, loads: Objects::NotificationFilter
      field :success, Boolean, "Did the operation succeed?", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      #
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, custom_inbox:, **inputs)
        permission.access_allowed?(
          :delete_custom_inbox,
          resource: permission.viewer,
          current_repo: nil,
          current_org: nil,
          allow_integrations: false,
          allow_user_via_integration: false,
        )
      end

      def resolve(custom_inbox:)
        destroyed_inbox = unpack_newsies_response!(
          GitHub.newsies.web.destroy_custom_inbox(custom_inbox.id),
        )

        unless destroyed_inbox.destroyed?
          raise Errors::Unprocessable.new(destroyed_inbox.errors.full_messages.to_sentence)
        end

        { success: true }
      end
    end
  end
end
