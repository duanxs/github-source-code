# frozen_string_literal: true

module Platform
  module Mutations
    class AddOrganizationDomain < Platform::Mutations::Base
      description "Adds an organization domain to an organization."

      visibility :internal, environments: [:dotcom]
      minimum_accepted_scopes ["admin:org"]

      argument :organization_id, ID, "The ID of the organization to add the domain to", required: true, loads: Objects::Organization
      argument :domain, Scalars::URI, "The URL of the domain", required: true

      field :organization_domain, Objects::OrganizationDomain, "The organization domain that was added.", null: true

      def resolve(organization:, **inputs)
        unless context[:viewer] && GitHub.domain_verification_enabled?
          raise Errors::Forbidden.new("Viewer not authorized to add organization domains")
        end

        unless organization.adminable_by?(context[:viewer])
          raise Errors::Forbidden.new("User must be an organization admin")
        end

        organization_domain = organization.organization_domains.build(domain: inputs[:domain])
        if organization_domain.save
          organization_domain.instrument_create(context[:viewer])
          { organization_domain: organization_domain }
        else
          raise Errors::Unprocessable.new(organization_domain.errors.full_messages.join(", "))
        end
      end
    end
  end
end
