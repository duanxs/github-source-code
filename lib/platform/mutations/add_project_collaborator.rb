# frozen_string_literal: true

module Platform
  module Mutations
    class AddProjectCollaborator < Platform::Mutations::Base
      description "Adds user collaborator to project."
      visibility :internal

      scopeless_tokens_as_minimum

      argument :project_id, ID, "The ID of the project to add the collaborator to.", required: true, loads: Objects::Project
      argument :user_id, ID, "The ID of the user collaborator to add to the project.", required: true, loads: Objects::User
      argument :permission, Enums::ProjectPermission, "The permission that the collaborator is being granted on the project.", required: false

      field :project_user_edge, Edges::ProjectUser, "The edge between the project and the user.", null: true

      def resolve(user:, project:, **inputs)

        if project.owner_type == "Repository"
          raise Errors::Unprocessable.new("Repository projects cannot have collaborators.")
        end

        unless project.adminable_by?(context[:viewer])
          raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to change collaboration settings for this project.")
        end

        if project.members.include?(user)
          raise Errors::Unprocessable.new("#{user.login} is already a collaborator on this project.")
        end

        if inputs[:permission] == "none"
          raise Errors::Unprocessable.new("A permission level of 'none' cannot be specified.")
        end

        permission = inputs[:permission] || :read

        # String#to_sym is safe to call here, since we know it's going to be
        # one of the few values in Enum::ProjectPermission.
        project.update_user_permission(user, permission.to_sym) if !context[:viewer].can_act_for_integration?

        scope = project.direct_collaborators.order("login ASC")
        connection_class = GraphQL::Relay::BaseConnection.connection_for_nodes(scope)
        connection = connection_class.new(scope, {}, parent: project)

        { project_user_edge: Platform::Models::ProjectUserEdge.new(user, connection) }
      end
    end
  end
end
