# frozen_string_literal: true

module Platform
  module Mutations
    class RevertSponsorsMembershipToSubmitted < Platform::Mutations::Base
      description "Reverts a Sponsors membership back to the submitted state"

      visibility :internal
      minimum_accepted_scopes ["biztools"]

      argument :sponsors_membership_id, ID, "The ID of the Sponsors membership to revert back to submitted", required: true, loads: Objects::SponsorsMembership

      field :sponsors_membership, Objects::SponsorsMembership, "The Sponsors membership that was reverted back to submitted", null: true
      error_fields

      def resolve(sponsors_membership:)
        unless context[:viewer].can_admin_sponsors_listings?
          message = "#{context[:viewer]} does not have permission to revert the Sponsors membership to submitted"
          error = {
            message: message,
            short_message: message,
            attribute: "sponsorsMembershipId",
          }

          return {
            sponsors_membership: sponsors_membership,
            errors: [error],
          }
        end

        begin
          if sponsors_membership.submitted? || sponsors_membership.revert_to_submitted!
            {
              sponsors_membership: sponsors_membership.reload,
              errors: [],
            }
          else
            message = "An error occurred when reverting the Sponsors membership for #{sponsors_membership.sponsorable.login} to submitted"
            error = {
              message: message,
              short_message: message,
              attribute: "sponsorsMembershipId",
            }

            {
              sponsors_membership: nil,
              errors: [error],
            }
          end
        rescue Workflow::NoTransitionAllowed
          message = "This Sponsors membership's state cannot be reverted back to submitted"
          error = {
            message: message,
            short_message: message,
            attribute: "sponsorsMembershipId",
          }

          {
            sponsors_membership: nil,
            errors: [error],
          }
        end
      end
    end
  end
end
