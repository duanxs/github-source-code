# frozen_string_literal: true

module Platform
  module Mutations
    class DismissRepositoryRecommendation < Platform::Mutations::Base
      description "Declines a recommended repository for the current viewer."
      visibility :internal

      minimum_accepted_scopes ["public_repo"]

      argument :repository_id, ID, "The Node ID of the repository.", required: true, loads: Objects::Repository

      field :repository, Objects::Repository, "The dismissed repository.", null: true

      def resolve(repository:, **inputs)

        recommendation = RepositoryRecommendation.new(repository: repository)
        recommendation.dismiss_for(context[:viewer])

        { repository: repository }
      end
    end
  end
end
