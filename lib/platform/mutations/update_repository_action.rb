# frozen_string_literal: true

module Platform
  module Mutations
    class UpdateRepositoryAction < Platform::Mutations::Base
      description "Updates an existing Repository Action."
      areas_of_responsibility :actions

      visibility :internal

      minimum_accepted_scopes ["repo"]

      argument :repository_action_id, ID, "The Repository Action to update.", required: true, loads: Objects::RepositoryAction
      argument :featured, Boolean, "Whether or not the Repository Action is featured.", visibility: :internal, required: false
      argument :rank_multiplier, Float, "Ranking of this Action, used for ordering on Marketplace and Workflow editor.", visibility: :internal, required: false
      argument :categories, [String], "A list of categories that describe the action.", required: false, visibility: :internal
      argument :filter_categories, [String], "A list of filter categories that describe the action.", required: false, visibility: :internal

      field :repository_action, Objects::RepositoryAction, "The updated Repository Action.", null: true

      def resolve(repository_action:, **inputs)
        viewer_is_admin = context[:viewer].can_admin_repository_actions?

        unless viewer_is_admin
          raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to update " +
                                         "the Action.")
        end

        repository_action.featured = inputs[:featured] unless inputs[:featured].nil?
        repository_action.rank_multiplier = inputs[:rank_multiplier] if inputs[:rank_multiplier]
        resolve_categories(repository_action: repository_action, inputs: inputs)

        if repository_action.save
          { repository_action: repository_action }
        else
          raise Errors::Unprocessable.new(repository_action.errors.full_messages.join(", "))
        end
      end

      def load_categories(category_names)
        Marketplace::Category.where(name: category_names.reject(&:blank?))
      end

      def load_filter_categories(category_names)
        load_categories(category_names)
      end

      private

      def resolve_categories(repository_action:, inputs:)
        return unless inputs[:categories] || inputs[:filter_categories]

        categories = []
        if inputs[:categories]
          categories += inputs[:categories]
        else
          categories += repository_action.regular_categories
        end

        if inputs[:filter_categories]
          categories += inputs[:filter_categories]
        else
          categories += repository_action.filter_categories
        end

        repository_action.categories = categories
      end
    end
  end
end
