# frozen_string_literal: true

module Platform
  module Mutations
    class RegenerateOrganizationDomainToken < Platform::Mutations::Base
      description "Regenerates an organization domain's verification token."

      visibility :internal, environments: [:dotcom]

      minimum_accepted_scopes ["admin:org"]

      argument :id, ID, "The organization domain to regenerate the verification token of.", required: true, loads: Objects::OrganizationDomain, as: :domain

      field :verification_token, String, "The verification token that was generated.", null: true

      def resolve(domain:, **inputs)

        unless domain.adminable_by?(context[:viewer])
          raise Errors::Forbidden.new("Viewer not authorized to regenerate organization domain tokens")
        end

        if domain && domain.generate_verification_token
          { verification_token: domain.verification_token }
        else
          errors = domain.errors.full_messages.join(", ")
          raise Errors::Unprocessable.new("Could not regenerate organization domain token: #{errors}")
        end
      end
    end
  end
end
