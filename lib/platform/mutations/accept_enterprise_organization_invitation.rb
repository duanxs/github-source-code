# frozen_string_literal: true

module Platform
  module Mutations
    class AcceptEnterpriseOrganizationInvitation < Platform::Mutations::Base
      description "Accepts a pending invitation for an organization to join an enterprise."

      # This mutation can only be run by end-users in the Enterprise Cloud environment.
      visibility :under_development, environments: [:dotcom]
      visibility :internal, environments: [:enterprise]

      minimum_accepted_scopes ["admin:org"]

      argument :invitation_id, ID, "The Node ID of the pending enterprise organization invitation.", required: true, loads: Objects::EnterpriseOrganizationInvitation

      field :invitation, Objects::EnterpriseOrganizationInvitation, "The invitation that was accepted.", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, invitation:, **inputs)
        invitation.async_invitee.then do |org|
          permission.access_allowed?(:accept_business_organization_invitation,
            resource: org, repo: nil, organization: org,
            allow_integrations: false, allow_user_via_integration: false)
        end
      end

      def resolve(invitation:, **inputs)
        viewer = context[:viewer]
        business = invitation.business
        organization = invitation.invitee

        invitation.accept(viewer)

        {
          invitation: invitation,
        }
      rescue BusinessOrganizationInvitation::InvalidActorError
        raise Errors::Unprocessable.new("#{viewer.name} cannot accept invitations on behalf of #{organization.name}.")
      rescue BusinessOrganizationInvitation::InvalidInviterError
        raise Errors::Unprocessable.new("Please contact #{business.name}'s account representative to invite organizations.")
      rescue BusinessOrganizationInvitation::AlreadyAcceptedError
        raise Errors::Unprocessable.new("This invitation has already been accepted.")
      rescue BusinessOrganizationInvitation::CanceledError
        raise Errors::Unprocessable.new("This invitation has been canceled.")
      rescue BusinessOrganizationInvitation::AlreadyBusinessMemberError
        raise Errors::Unprocessable.new("#{organization.name} is already part of an enterprise.")
      rescue BusinessOrganizationInvitation::InvalidInviteeError
        raise Errors::Unprocessable.new("Please contact #{organization.name}'s account representative to join an enterprise.")
      rescue BusinessOrganizationInvitation::InsufficientAvailableSeatsError
        raise Errors::Unprocessable.new("#{business.name} does not have sufficient seats to add #{organization.name}.")
      end
    end
  end
end
