# frozen_string_literal: true

module Platform
  module Mutations
    class RemoveEnterpriseMember < Platform::Mutations::Base
      description "Removes a user from all organizations within the enterprise"

      # This mutation can only be run by end-users in the Enterprise Cloud environment.
      visibility :under_development, environments: [:dotcom]
      visibility :internal, environments: [:enterprise]

      minimum_accepted_scopes ["admin:enterprise"]

      argument :enterprise_id, ID, "The ID of the enterprise from which the user should be removed.", required: true, loads: Objects::Enterprise
      argument :user_id, ID, "The ID of the user to remove from the enterprise.", required: true, loads: Objects::User

      field :enterprise, Objects::Enterprise, "The updated enterprise.", null: true
      field :user, Objects::User, "The user that was removed from the enterprise.", null: true
      field :viewer, Objects::User, "The viewer performing the mutation.", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, enterprise:, **inputs)
        permission.access_allowed?(:administer_business, resource: enterprise, repo: nil, organization: nil, allow_integrations: false, allow_user_via_integration: false)
      end

      def resolve(enterprise:, user:, **inputs)
        viewer = context[:viewer]

        begin
          enterprise.remove_member(user, actor: viewer, reason: "removed_via_api")
        rescue Business::ForbiddenRemovalError => err
          raise Errors::Forbidden.new(err.message)
        rescue Business::InvalidRemovalError => err
          raise Errors::Unprocessable.new(err.message)
        end

        {
          enterprise: enterprise,
          user: user,
          viewer: viewer,
        }
      end
    end
  end
end
