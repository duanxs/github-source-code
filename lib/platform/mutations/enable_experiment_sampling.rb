# frozen_string_literal: true

module Platform
  module Mutations
    class EnableExperimentSampling < Platform::Mutations::Base
      description "Enables sampling on a experiment."

      visibility :internal
      minimum_accepted_scopes ["devtools"]

      argument :id, ID, "The id of the experiment we want to enable sampling for.", required: true
      argument :threshold, Float, "The threshold of the experiment sampling in milliseconds.", required: true
      argument :duration, Integer, "The duration of the experiment sampling in seconds.", required: true

      field :experiment, Objects::Experiment, "The experiment that was just updated.", null: true

      def resolve(**inputs)
        raise Errors::Unprocessable.new("You don't have permissions to perform that action.") if !Experiment.viewer_can_read?(context[:viewer])
        experiment = Platform::Helpers::NodeIdentification.typed_object_from_id(
          [Objects::Experiment], inputs[:id], context
        )
        if inputs[:threshold] > 0
          experiment.set_sample_threshold inputs[:threshold], inputs[:duration]
          { experiment: experiment }
        elsif inputs[:threshold] <= 0
          raise Errors::Unprocessable.new("Sample threshold must be greater than 0")
        else
          raise Errors::Unprocessable.new("Could not enable sampling.")
        end
      end
    end
  end
end
