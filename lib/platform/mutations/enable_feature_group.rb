# frozen_string_literal: true

module Platform
  module Mutations
    class EnableFeatureGroup < Platform::Mutations::Base
      description "Adds a feature flag for a group."

      visibility :internal
      scopeless_tokens_as_minimum

      argument :feature_id, ID, "The id of the feature flag.", required: true, loads: Objects::Feature
      argument :group, Enums::FeatureGroup, "The feature group to enable", required: true

      field :feature, Objects::Feature, "The feature", null: true

      def resolve(feature:, **inputs)

        if feature
          feature.enable_group(inputs[:group])
          { feature: feature }
        else
          raise Errors::Unprocessable.new("Could not enable group for feature.")
        end
      end
    end
  end
end
