# frozen_string_literal: true

module Platform
  module Mutations
    class CreateOrganizationDiscussionComment < Platform::Mutations::Base
      description "Creates a new organization discussion comment."
      minimum_accepted_scopes ["write:discussion"]
      visibility :under_development

      argument :discussion_id, ID, "The ID of the discussion to which the comment belongs.", required: true, loads: Objects::OrganizationDiscussion, as: :discussion
      argument :body, String, "The content of the comment.", required: true

      field :comment, Objects::OrganizationDiscussionComment, "The new comment.", null: true

      def resolve(discussion:, **inputs)
        discussion.async_organization.then do |organization|
          unless organization.direct_or_team_member?(context[:viewer])
            raise Errors::Forbidden.new(
              "#{context[:viewer]} can't create a discussion comment without being a member of " +
              "#{organization.login}.")
          end

          comment = discussion.replies.build(body: inputs[:body], user: context[:viewer])

          if comment.save
            { comment: comment }
          else
            raise Errors::Validation.new(comment.errors.full_messages.to_sentence)
          end
        end
      end
    end
  end
end
