# frozen_string_literal: true

module Platform
  module Mutations
    class AddRecoveryToken < Platform::Mutations::Base
      description "Adds a recovery token to a user."

      visibility :internal

      minimum_accepted_scopes ["repo"]

      argument :provider, Enums::RecoveryProvider, "The recovery provider origin.", required: true

      field :recovery_token, Platform::Interfaces::Node, "The subject", null: true
      field :token_state_url, Scalars::URI, "The canonical URL for the token at the recovery provider (also used for CSRF protection)", null: true
      field :sealed_token, String, "The base64 encoded value of the signed recovery token", null: true

      def resolve(**inputs)
        user_id = context[:viewer].id
        recovery_provider = Darrrr.recovery_provider(inputs[:provider])

        dar_context = {
          user: context[:viewer],
          key_version_id: nil, # this is populated during generate_recovery_token
        }

        token, sealed_token = Darrrr.this_account_provider.generate_recovery_token(data: user_id, audience: recovery_provider, context: dar_context)

        persisted_token = ::DelegatedRecoveryToken.create!({
          user_id: user_id,
          provider: inputs[:provider],
          token_id: token.token_id.to_hex,
          key_version_id: dar_context[:key_version_id],
        })

        if token && sealed_token
          GitHub.dogstats.increment("delegated_account_recovery_token", tags: ["create:success"])
          {
            recovery_token: persisted_token,
            token_state_url: token.state_url,
            sealed_token: sealed_token,
          }
        else
          GitHub.dogstats.increment("delegated_account_recovery_token", tags: ["create:error"])
          raise Errors::Unprocessable.new("Could not create token.")
        end
      end
    end
  end
end
