# frozen_string_literal: true

module Platform
  module Mutations
    class DisableExperimentSampling < Platform::Mutations::Base
      description "Disables sampling on a experiment."

      visibility :internal
      minimum_accepted_scopes ["devtools"]

      argument :id, ID, "The id of the experiment we want to disable sampling for.", required: true

      field :experiment, Objects::Experiment, "The experiment that was just updated.", null: true

      def resolve(**inputs)
        raise Errors::Unprocessable.new("You don't have permissions to perform that action.") if !Experiment.viewer_can_read?(context[:viewer])
        experiment = Platform::Helpers::NodeIdentification.typed_object_from_id(
          [Objects::Experiment], inputs[:id], context
        )
        if experiment && experiment.disable_sampling
          { experiment: experiment }
        else
          raise Errors::Unprocessable.new("Could not disable sampling for experiment.")
        end
      end
    end
  end
end
