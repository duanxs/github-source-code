# frozen_string_literal: true

module Platform
  module Mutations
    class DisableOrganizationNotificationRestriction < Platform::Mutations::Base
      description "Disables the setting to restrict notifications to only domains verified by an organization."
      areas_of_responsibility :community_and_safety

      visibility :under_development, environments: [:dotcom]

      minimum_accepted_scopes ["admin:org"]

      argument :organization_id, ID, "The ID of the organization to enable the restriction for.", required: true, loads: Objects::Organization

      field :organization, Objects::Organization, "The organization that the restriction was enabled for.", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, organization:, **inputs)
        permission.access_allowed?(:write_organization_settings, resource: organization, current_org: organization, current_repo: nil, allow_integrations: true, allow_user_via_integration: true)
      end

      def resolve(organization:, **inputs)
        unless organization.plan_supports?(:restrict_notification_delivery)
          raise Errors::Unprocessable.new "Notification restrictions are not supported on this organization's plan."
        end

        unless organization.organization_domains.verified.any?
          raise Errors::Unprocessable.new "You must have at least one verified domain to configure notification restrictions."
        end

        if organization.disable_notification_restrictions(actor: context[:viewer])
          { organization: organization }
        else
          raise Errors::Unprocessable.new "An error occured when disabling notification restriction for #{organization.login}"
        end
      end
    end
  end
end
