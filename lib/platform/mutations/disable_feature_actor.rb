# frozen_string_literal: true

module Platform
  module Mutations
    class DisableFeatureActor < Platform::Mutations::Base
      description "Disables a feature flag for an actor."

      visibility :internal
      minimum_accepted_scopes ["devtools"]

      argument :feature_id, ID, "The id of the feature flag.", required: true, loads: Objects::Feature
      argument :type, Enums::FeatureActorType, "The type of actor", required: true
      argument :identifier, String, <<~MD, required: true
        An identifier for the given type. These map as follows:

        USER - the user's login
        REPOSITORY - the repo's name_with_owner
        ENTERPRISE - the enterprise slug
        OAUTH_APPLICATION - the oauth app's key (client id)
        APP - the integration's database id
        HOST - the host
        ROLE - the role
        FLIPPER_ID - an arbitrary flipper ID
      MD

      field :feature, Objects::Feature, "The feature", null: true

      def resolve(feature:, **inputs)

        actor = Models::FeatureActorFinder.new(inputs[:type], inputs[:identifier]).actor

        if feature && actor
          feature.disable(actor)
          { feature: feature }
        else
          raise Errors::Unprocessable.new("Could not disable actor for feature.")
        end
      end
    end
  end
end
