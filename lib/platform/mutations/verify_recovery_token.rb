# frozen_string_literal: true

module Platform
  module Mutations
    class VerifyRecoveryToken < Platform::Mutations::Base
      description "Marks a given token as confirmed"

      # Typically (but not necessarily), users will be unauthenticated in this flow
      allow_anonymous_viewer true

      visibility :internal

      minimum_accepted_scopes ["repo"]

      argument :countersigned_token, String, "The countersigned delegated recovery token.", required: true

      field :validation_result, Enums::RecoveryTokenValidationResult, "The result of this validation.", null: true
      field :error_message, String, "A specific error message displayed when token verification fails.", null: true

      def resolve(**inputs)
        response = {}
        countersigned_token = inputs[:countersigned_token]
        recovery_provider = Darrrr::RecoveryToken.recovery_provider_issuer(Base64.decode64(countersigned_token))

        begin
          unverified_token_id = Darrrr.this_account_provider.dangerous_unverified_recovery_token(countersigned_token).token_id.to_hex
          persisted_token = DelegatedRecoveryToken.confirmed.find_by_token_id(unverified_token_id)

          response[:validation_result] = if persisted_token
            parsed_token = Darrrr.this_account_provider.validate_countersigned_recovery_token!(countersigned_token, key_version_id: persisted_token.key_version_id)
            countersigned_token = Darrrr::RecoveryToken.parse(Base64.decode64(countersigned_token))
            Helpers::DelegatedAccountRecovery.verify_secret(parsed_token, countersigned_token, persisted_token)
          else
            :no_confirmed_tokens_matched
          end
        rescue Darrrr::DelegatedRecoveryError => e
          GitHub.dogstats.increment("delegated_account_recovery.account_provider.countersigned_token", tags: ["error:recovery_token_token_parse_error"])
          response[:validation_result] = :unverifiable_token
          response[:error_message] = e.message
        end

        if token_owner = persisted_token&.user
          if DelegatedAccountRecoveryController::RECOVERY_SUCCESS_STATES.include?(response[:validation_result])
            DelegatedAccountRecoveryMailer.recovery_token_used(token_owner, recovery_provider.origin).deliver_later
          else
            DelegatedAccountRecoveryMailer.recovery_token_failed_attempt(token_owner, recovery_provider.origin).deliver_later
          end
        end

        Helpers::DelegatedAccountRecovery.record_recovery_stats(response)

        response
      end
    end
  end
end
