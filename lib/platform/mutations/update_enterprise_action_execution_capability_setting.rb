# frozen_string_literal: true

module Platform
  module Mutations
    class UpdateEnterpriseActionExecutionCapabilitySetting < Platform::Mutations::Base
      description "Sets the action execution capability setting for an enterprise."
      areas_of_responsibility :actions

      minimum_accepted_scopes ["admin:enterprise"]

      argument :enterprise_id, ID, "The ID of the enterprise on which to set the members can create repositories setting.", required: true, loads: Objects::Enterprise
      argument :capability, Platform::Enums::ActionExecutionCapabilitySetting, "The value for the action execution capability setting on the enterprise.", required: true

      field :enterprise, Objects::Enterprise, "The enterprise with the updated action execution capability setting.", null: true
      field :message, String, "A message confirming the result of updating the action execution capability setting.", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, enterprise:, **inputs)
        permission.access_allowed?(:administer_business, resource: enterprise, repo: nil, organization: nil, allow_integrations: false, allow_user_via_integration: false)
      end

      def resolve(enterprise:, **inputs)
        viewer = context[:viewer]

        unless enterprise.owner?(viewer)
          raise Errors::Forbidden.new("#{viewer} does not have permission to set the action execution capability setting on this enterprise.")
        end

        message = ""
        case inputs[:capability]
        when Configurable::ActionExecutionCapabilities::DISABLED
          enterprise.disable_all_action_executions(force: true, actor: viewer)
          message = "All Actions are now disabled for all repositories."
        when Configurable::ActionExecutionCapabilities::ALL_ACTIONS
          enterprise.enable_all_action_executions(force: true, actor: viewer)
          message = "All Actions are now enabled for all repositories."
        when Configurable::ActionExecutionCapabilities::LOCAL_ACTIONS
          enterprise.enable_only_local_action_executions(force: true, actor: viewer)
          message = "Only Local/In-Repo Actions are now enabled for all repositories."
        when Configurable::ActionExecutionCapabilities::NO_POLICY
          enterprise.clear_action_execution_capabilities(actor: viewer)
          message = "Organization administrators can now change this setting for individual organizations."
        end

        {
          enterprise: enterprise,
          message: message,
        }
      end
    end
  end
end
