# frozen_string_literal: true

module Platform
  module Mutations
    class CreateMarketplaceListing < Platform::Mutations::Base
      description "Creates a new marketplace listing."
      visibility :internal
      areas_of_responsibility :marketplace

      minimum_accepted_scopes ["repo"]

      argument :name, String, "The listing's full name.", required: true
      argument :short_description, String, "The listing's short description.", required: false
      argument :full_description, String, "The listing's full description.", required: false
      argument :primary_category_name, String, "The name of the category that best describes the listing.", required: true
      argument :privacy_policy_url, Scalars::URI, "A link to the listing's privacy policy.", required: false
      argument :support_url, Scalars::URI, "A link to the listing's support site.", required: false
      argument :oauthApplicationID, Integer, "The ID of the OAuth application to list.", required: false,
        as: :oauth_application_id
      argument :appID, Integer, "The ID of the GitHub App to list.", required: false,
        as: :app_id
      argument :installation_url, String, "A link to install the listing's application.", required: false
      argument :supported_language_names, [String, null: true], "The names of supported programming languages.", required: false

      field :marketplace_listing, Objects::MarketplaceListing, "The new marketplace listing.", null: true

      def resolve(**inputs)
        listable = if inputs[:oauth_application_id]
          Loaders::ActiveRecord.load(::OauthApplication, inputs[:oauth_application_id]).sync
        elsif inputs[:app_id]
          Loaders::ActiveRecord.load(::Integration, inputs[:app_id]).sync
        end

        unless listable
          raise Errors::Validation.new("An oauthApplicationID or appID is required " +
                                            "to create a new Marketplace listing.")
        end

        listing = Marketplace::Listing.new(listable: listable)

        unless listing.allowed_to_edit?(context[:viewer])
          raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to list the " +
                                         "application in the Marketplace.")
        end

        listing.name = inputs[:name]
        listing.short_description = inputs[:short_description]
        listing.full_description = inputs[:full_description]
        listing.privacy_policy_url = inputs[:privacy_policy_url].to_s
        listing.support_url = inputs[:support_url].to_s
        listing.installation_url = inputs[:installation_url]
        listing.bgcolor = listable.bgcolor

        category = Loaders::ActiveRecord.load(::Marketplace::Category,
                                              inputs[:primary_category_name], column: :name).sync
        unless category
          raise Errors::Validation.new("No such Marketplace category exists: " +
                                            inputs[:primary_category_name])
        end

        listing.categories = [category]
        listing.primary_category_id = category.id

        if inputs[:supported_language_names].present?
          supported_language_names = inputs[:supported_language_names]

          supported_languages = LanguageName.lookup_by_names(supported_language_names).compact

          if supported_languages.any?
            listing.languages = supported_languages
          end
        end

        if listing.save
          listing.instrument_creation(actor: context[:viewer])

          { marketplace_listing: listing }
        else
          if listing.errors[:categories].present? && category.errors.full_messages.present?
            listing.errors.delete(:categories)
            category.errors.full_messages.each do |err|
              # NOTE: using primary_category here since that's what the API
              # currently accepts. This should change once we accept multiple
              # categories instead.
              listing.errors.add(:primary_category, err.downcase)
            end
          end

          errors = listing.errors.full_messages.join(", ")
          raise Errors::Unprocessable.new("Could not create a Marketplace listing: #{errors}")
        end
      end
    end
  end
end
