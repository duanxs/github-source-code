# frozen_string_literal: true

module Platform
  module Mutations
    class MarkNotificationAsUnread < Platform::Mutations::Base
      extend Helpers::Newsies

      description "Marks a notification as unread"
      feature_flag :pe_mobile
      minimum_accepted_scopes ["notifications"]
      areas_of_responsibility :notifications

      argument :id, ID, "The NotificationThread id.", required: true, loads: Objects::NotificationThread, as: :notification_thread

      field :success, Boolean, "Did the operation succeed?", null: true
      field :viewer, Objects::User, "The user that the notification belongs to.", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, notification_thread:, **inputs)
        mark_notification_api_check(permission, notification_thread, inputs)
      end

      def resolve(notification_thread:, **inputs)
        self.class.unpack_newsies_response!(
          GitHub.newsies.web.mark_summaries_as_unread(context[:viewer], [notification_thread.summary_id]),
        )

        {
          success: true,
          viewer: context[:viewer],
        }
      end
    end
  end
end
