# frozen_string_literal: true

module Platform
  module Mutations
    class RequestUnverifiedMarketplaceListingApproval < Platform::Mutations::Base
      description "Request a draft listing be reviewed by GitHub for display in the Marketplace as an unverified listing."
      visibility :internal
      areas_of_responsibility :marketplace

      minimum_accepted_scopes ["repo"]

      argument :slug, String, <<~DESCRIPTION, required: true
        Select the listing that matches this slug. It's the short name of the listing used in
        its URL.
      DESCRIPTION

      field :marketplace_listing, Objects::MarketplaceListing, "The updated marketplace listing.", null: true

      def resolve(**inputs)
        listing = ::Marketplace::Listing.find_by(slug: inputs[:slug])

        unless listing.present?
          raise Errors::NotFound.new("Could not find listing with slug '#{inputs[:slug]}'.")
        end

        unless listing.adminable_by?(context[:viewer])
          raise Errors::Forbidden.new(
            "#{context[:viewer]} does not have permission to submit the listing for review by GitHub.",
          )
        end

        unless listing.can_request_unverified_approval?
          raise Errors::Validation.new("Marketplace listing cannot be submitted for review.")
        end

        listing.request_unverified_approval!(context[:viewer])

        { marketplace_listing: listing }
      end
    end
  end
end
