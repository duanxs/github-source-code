# frozen_string_literal: true

module Platform
  module Mutations
    class PublishMarketplaceListingPlan < Platform::Mutations::Base
      description "Publish a Marketplace listing plan."
      visibility :internal
      areas_of_responsibility :marketplace

      minimum_accepted_scopes ["repo"]

      argument :id, ID, "The Marketplace listing plan ID to publish.", required: true, loads: Objects::MarketplaceListingPlan, as: :plan

      field :marketplace_listing_plan, Objects::MarketplaceListingPlan, "The published Marketplace listing plan.", null: true

      def resolve(plan:, **inputs)
        unless plan.adminable_by?(context[:viewer])
          raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to change the " +
                                         "Marketplace listing plan.")
        end

        listing = plan.listing

        if plan.draft?
          if plan.can_publish?
            plan.publish!
          else
            error_messages = [].tap do |messages|
              if listing.reached_maximum_plan_count?
                messages << "the listing has reached the limit of published plans"
              end

              if plan.paid? && !listing.published_paid_plans_allowed?
                messages << "the listing must be verified to have published paid plans"
              end
            end

            raise Errors::Unprocessable.new(
              "This plan cannot be published: #{error_messages.join(", ")}",
            )
          end
        elsif plan.published?
          raise Errors::Unprocessable.new("This plan has already been published.")
        elsif plan.retired?
          raise Errors::Unprocessable.new("This plan has been retired.")
        end

        { marketplace_listing_plan: plan }
      end
    end
  end
end
