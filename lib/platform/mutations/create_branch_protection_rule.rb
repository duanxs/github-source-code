# frozen_string_literal: true

module Platform
  module Mutations
    class CreateBranchProtectionRule < Platform::Mutations::Base
      description "Create a new branch protection rule"

      minimum_accepted_scopes ["public_repo"]

      argument :repository_id, ID, "The global relay id of the repository in which a new branch protection rule should be created in.", required: true, loads: Objects::Repository
      argument :pattern, String, "The glob-like pattern used to determine matching branches.", required: true

      argument :requires_approving_reviews, Boolean, "Are approving reviews required to update matching branches.", required: false
      argument :required_approving_review_count, Integer, "Number of approving reviews required to update matching branches.", required: false
      argument :requires_commit_signatures, Boolean, "Are commits required to be signed.", required: false
      argument :requires_linear_history, Boolean, "Are merge commits prohibited from being pushed to this branch.", required: false, visibility: :under_development
      argument :allows_force_pushes, Boolean, "Are force pushes allowed on this branch.", required: false, visibility: :under_development
      argument :allows_deletions, Boolean, "Can this branch be deleted.", required: false, visibility: :under_development
      argument :is_admin_enforced, Boolean, "Can admins overwrite branch protection.", required: false
      argument :requires_status_checks, Boolean, "Are status checks required to update matching branches.", required: false
      argument :requires_strict_status_checks, Boolean, "Are branches required to be up to date before merging.", required: false
      argument :requires_code_owner_reviews, Boolean, "Are reviews from code owners required to update matching branches.", required: false
      argument :dismisses_stale_reviews, Boolean, "Will new commits pushed to matching branches dismiss pull request review approvals.", required: false
      argument :restricts_review_dismissals, Boolean, "Is dismissal of pull request reviews restricted.", required: false
      argument :review_dismissal_actor_ids, [ID], "A list of User or Team IDs allowed to dismiss reviews on pull requests targeting matching branches.", required: false
      argument :restricts_pushes, Boolean, "Is pushing to matching branches restricted.", required: false
      argument :push_actor_ids, [ID], "A list of User, Team or App IDs allowed to push to matching branches.", required: false
      argument :required_status_check_contexts, [String], "List of required status check contexts that must pass for commits to be accepted to matching branches.", required: false

      field :branch_protection_rule, Objects::BranchProtectionRule, "The newly created BranchProtectionRule.", null: true

      include Shared::ModifyBranchProtectionRule

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, repository:, **inputs)
        permission.async_owner_if_org(repository).then do |org|
          permission.access_allowed? :update_branch_protection, resource: repository, current_repo: repository, current_org: org, allow_integrations: true, allow_user_via_integration: true
        end
      end

      def resolve(repository:, **inputs)
        raise Errors::Forbidden.new("Upgrade to GitHub Pro or make this repository public to enable this feature.") unless repository.plan_supports?(:protected_branches)
        raise Errors::Forbidden.new("Branch protection creating is disabled on this repository.") unless repository.can_update_protected_branches?(context[:viewer])

        protected_branch = repository.protected_branches.build(name: inputs[:pattern], creator: context[:viewer])

        update_branch_protection_rule(protected_branch, inputs)
      end
    end
  end
end
