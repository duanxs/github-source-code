# frozen_string_literal: true

module Platform
  module Mutations
    class CreateNonMarketplaceListing < Platform::Mutations::Base
      description "Creates a new Works with GitHub listing."

      visibility :internal

      minimum_accepted_scopes ["repo"]

      argument :name, String, "The listing's full name.", required: true
      argument :description, String, "The listing's description.", required: true
      argument :category_slug, String, "The slug for the category that best describes the listing.", required: true
      argument :url, String, "A link for the listing.", required: true
      argument :enterprise_compatible, Boolean, "Whether the listing is compatible with GitHub Enterprise.", required: true

      field :non_marketplace_listing, Objects::NonMarketplaceListing, "The new Works with GitHub listing.", null: true

      def resolve(**inputs)
        category = Loaders::ActiveRecord.load(::IntegrationFeature,
                                              inputs[:category_slug], column: :slug).sync
        listing = NonMarketplaceListing.new(category: category, creator: context[:viewer],
                                            name: inputs[:name], description: inputs[:description],
                                            url: inputs[:url],
                                            enterprise_compatible: inputs[:enterprise_compatible])

        if listing.save
          { non_marketplace_listing: listing }
        else
          errors = listing.errors.full_messages.join(", ")
          raise Errors::Unprocessable.new("Could not submit Works with GitHub listing: #{errors}")
        end
      end
    end
  end
end
