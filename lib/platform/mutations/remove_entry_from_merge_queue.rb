# frozen_string_literal: true

module Platform
  module Mutations
    class RemoveEntryFromMergeQueue < Platform::Mutations::Base
      description "Adds a pull request to a merge queue"
      areas_of_responsibility :pull_requests

      minimum_accepted_scopes ["repo_deployment"]
      visibility :internal

      argument :entry_id, ID, "The Node ID of the merge queue entry to remove.", required: true, loads: Objects::MergeQueueEntry, as: :entry

      field :merge_queue, Objects::MergeQueue, "The updated merge queue", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, entry:, **inputs)
        repository = entry.queue.repository
        permission.async_owner_if_org(repository).then do |org|
          permission.access_allowed? :write_deployment,
            resource: repository,
            current_repo: repository,
            current_org: org,
            allow_integrations: true,
            allow_user_via_integration: true
        end
      end

      def resolve(entry:, **inputs)
        repository = entry.queue.repository
        context[:permission].authorize_content(:deployment, :create, repository: repository)

        merge_queue = entry.queue
        merge_queue.dequeue(pull_request: entry.pull_request, dequeuer: context[:viewer])

        {
          merge_queue: merge_queue
        }
      end
    end
  end
end
