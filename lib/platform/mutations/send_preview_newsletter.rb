# frozen_string_literal: true

module Platform
  module Mutations
    class SendPreviewNewsletter < Platform::Mutations::Base
      description "Sends a preview of a newsletter."

      visibility :internal
      minimum_accepted_scopes ["devtools"]

      argument :recipient, String, "The username or email of the user that should receive the email.", required: true
      argument :period, Enums::NewsletterPeriod, "How often the recipient should receive the newsletter.", required: true
      argument :type, Enums::NewsletterType, "The type of newsletter.", required: true

      error_fields
      field :success, Boolean, "If the action was successful.", null: true

      def resolve(**inputs)
        model = NewsletterPreview.new(inputs[:recipient], inputs[:period], inputs[:type])

        if model.valid?
          model.deliver
          {success: true, errors: []}
        else
          {success: false, errors: Platform::UserErrors.mutation_errors_for_model(model)}
        end
      end
    end
  end
end
