# frozen_string_literal: true

module Platform
  module Mutations
    class CreateIssue < Platform::Mutations::Base
      description "Creates a new issue."

      minimum_accepted_scopes ["public_repo"]

      argument :repository_id, ID, "The Node ID of the repository.", required: true, loads: Objects::Repository
      argument :title, String, "The title for the issue.", required: true
      argument :body, String, "The body for the issue description.", required: false
      argument :assignee_ids, [ID], "The Node ID for the user assignee for this issue.", required: false, loads: Objects::User
      argument :milestone_id, ID, "The Node ID of the milestone for this issue.", required: false, loads: Objects::Milestone
      argument :label_ids, [ID], "An array of Node IDs of labels for this issue.", required: false, loads: Objects::Label
      argument :project_ids, [ID], "An array of Node IDs for projects associated with this issue.", required: false, loads: Objects::Project
      argument :issue_template, String, "The name of an issue template in the repository, assigns labels and assignees from the template to the issue",
        required: false, feature_flag: :pe_mobile

      error_fields
      field :issue, Objects::Issue, "The new issue.", null: true

      extras [:execution_errors]

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, repository:, **inputs)
        permission.async_owner_if_org(repository).then do |org|
          permission.access_allowed? :open_issue, repo: repository, current_org: org, allow_integrations: true, allow_user_via_integration: true
        end
      end

      def resolve(execution_errors:, repository:, **inputs)
        attributes = { title: inputs[:title], body: inputs[:body] }

        context[:permission].authorize_content(:issue, :create, repo: repository)

        repo_writable = repository.writable_by?(context[:viewer]) || context[:actor].can_have_granular_permissions?

        if !repository.has_issues?
          raise Errors::Forbidden.new("Issues has been disabled in this repository.")
        end

        if inputs[:assignees]
          if repo_writable
            attributes[:assignees] = inputs[:assignees]
          else
            raise Errors::Forbidden.new("You don't have permission to assign issues in this repository.")
          end
        end

        if inputs[:milestone]
          if repo_writable
            attributes[:milestone] = inputs[:milestone]
          else
            raise Errors::Forbidden.new("You don't have permission to add to milestones in this repository.")
          end
        end

        if inputs[:labels]
          if repo_writable
            attributes[:labels] = inputs[:labels]
          else
            raise Errors::Forbidden.new("You don't have permission to add labels in this repository.")
          end
        end

        if inputs[:issue_template]
          if attributes[:labels] || attributes[:assignees]
            raise Errors::Unprocessable.new("You can not specify an issue_template with either label_ids or assignee_ids.")
          end

          if issue_template = repository.preferred_issue_templates.find_by_name(inputs[:issue_template])
            # we only want to assign labels and assignees from an issue_template if they
            # have not already been set above
            attributes[:assignees] = issue_template.assignees
            attributes[:labels] = issue_template.labels
          else
            raise Errors::NotFound.new("Could not find an issue template with name #{inputs[:issue_template]}")
          end
        end

        issue = repository.issues.build(attributes.reverse_merge(repository_id: repository.id))
        issue.user = context[:viewer]

        if inputs[:projects]
          inputs[:projects].each do |project|
            if project.writable_by?(context[:viewer])
              add_project_card_helper = Platform::Helpers::AddProjectCard.new(project, context)
              add_project_card_helper.check_permissions
              add_project_card_helper.check_issue_project_owner(issue)
              issue.cards.build(creator: context[:viewer], project: project, content: issue)
            else
              raise Errors::Forbidden.new("You don't have permission to add to project with id '#{project.global_relay_id}'.")
            end
          end
        end

        if @context[:permission].integration_user_request?
          issue.modifying_integration = @context[:integration]
        end

        begin
          if issue.save
            return { issue: issue, errors: [] }
          else
            Platform::UserErrors.append_legacy_mutation_model_errors_to_context(issue, execution_errors)

            return { issue: nil, errors: Platform::UserErrors.mutation_errors_for_model(issue) }
          end
        rescue GitHub::Prioritizable::Context::LockedForRebalance
          raise Errors::Unprocessable.new("This milestone is temporarily locked for maintenace. Please try again.")
        end
      end
    end
  end
end
