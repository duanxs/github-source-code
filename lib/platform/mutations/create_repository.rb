# frozen_string_literal: true

module Platform
  module Mutations
    class CreateRepository < Platform::Mutations::Base
      description "Create a new repository."

      include Settings::SecurityAnalysisSettings

      def self.async_api_can_modify?(permission, owner: nil, **inputs)
        owner ||= permission.viewer

        if owner.organization?
          permission.access_allowed?(:create_repo_for_org,
            resource: owner,
            allow_integrations: true,
            allow_user_via_integration: true,
            current_repo: nil,
            current_org: owner,
          )
        else
          permission.access_allowed?(:create_repo,
            resource: owner,
            allow_integrations: false,
            allow_user_via_integration: true,
            current_repo: nil,
            current_org: nil,
          )
        end
      end

      minimum_accepted_scopes ["public_repo"]
      areas_of_responsibility :repositories

      argument :name, String, "The name of the new repository.", required: true
      argument :owner_id, ID, "The ID of the owner for the new repository.", required: false,
        loads: Interfaces::RepositoryOwner
      argument :description, String, "A short description of the new repository.", required: false
      argument :visibility, Enums::RepositoryVisibility,
        "Indicates the repository's visibility level.", required: true
      argument :template, Boolean, "Whether this repository should be marked as a template " \
                                   "such that anyone who can access it can create new " \
                                   "repositories with the same files and directory structure.", required: false, default_value: false
      argument :homepage_url, Scalars::URI, "The URL for a web page about this repository.",
        required: false
      argument :has_wiki_enabled, Boolean,
        "Indicates if the repository should have the wiki feature enabled.",
        required: false, default_value: false
      argument :has_issues_enabled, Boolean,
        "Indicates if the repository should have the issues feature enabled.",
        required: false, default_value: true
      argument :team_id, ID, "When an organization is specified as the owner, this ID identifies the team that should be granted access to the new repository.", required: false, loads: Objects::Team

      field :repository, Objects::Repository, "The new repository.", null: true

      def resolve(name:, owner: nil, visibility:, description: nil, template: false,
                  homepage_url: nil, has_wiki_enabled: false, has_issues_enabled: true, team: nil)
        owner ||= context[:viewer]
        if owner.user? && owner != context[:viewer]
          raise Errors::Forbidden.new("#{context[:viewer]} cannot create a repository for " \
                                      "#{owner}.")
        end

        upgrade_to_bigger_plan = false
        repo_attributes = {
          owner: owner,
          name: name,
          description: description,
          visibility: visibility,
          template: template,
          homepage: homepage_url,
          has_wiki: has_wiki_enabled,
          has_issues: has_issues_enabled,
        }
        repo_attributes[:team_id] = team.id if team
        reflog_data = {
          repo_name: "#{owner}/#{name}",
          repo_public: visibility == "public",
        }

        result = Repository.handle_creation(context[:viewer], owner.login, upgrade_to_bigger_plan,
                                            repo_attributes, reflog_data)

        if result.success?
          install_current_integration_on_repository!(
            target: owner,
            repository: result.repository,
          )

          initialize_security_settings(owner, result.repository, context[:viewer]) unless GitHub.enterprise?

          { repository: result.repository }
        else
          if !result.allowed
            raise Errors::Forbidden.new("#{context[:viewer]} cannot create a repository for " \
                                        "#{owner}.")
          else
            error = result.repository.errors.full_messages.join(", ").presence ||
              result.error_message
            raise Errors::Unprocessable.new(error)
          end
        end
      end

      # Internal: installs the GitHub App making this API request onto the given
      # target and repository.
      #
      # Returns nothing.
      def install_current_integration_on_repository!(target:, repository:)
        permission = context[:permission]
        return unless permission.integration_bot_or_user_request?
        installation, installer =
          if permission.integration_bot_request?
            [permission.installation, permission.installation]
          elsif permission.integration_user_request?
            [permission.integration.installations_on(target).first, context[:viewer]]
          end

        if installation.installed_on_selected_repositories?
          permission.integration.install_on(
            target,
            repositories: [repository],
            installer: installer,
            version: installation.version,
          )
        end
      end
    end
  end
end
