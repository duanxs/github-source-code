# frozen_string_literal: true

module Platform
  module Mutations
    class UpdateMarketplaceListingPlanBullet < Platform::Mutations::Base
      description "Updates a bullet point on a Marketplace listing payment plan."
      visibility :internal
      areas_of_responsibility :marketplace

      minimum_accepted_scopes ["repo"]

      argument :value, String, "The contents of the bullet point.", required: true
      argument :id, ID, "The ID of the bullet point to update.", required: true, loads: Objects::MarketplaceListingPlanBullet, as: :bullet

      field :marketplace_listing_plan_bullet, Objects::MarketplaceListingPlanBullet, "The updated bullet point.", null: true

      def resolve(bullet:, **inputs)

        unless bullet.allowed_to_edit?(context[:viewer])
          raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to " +
                                         "update the bullet on the Marketplace listing plan.")
        end

        bullet.value = inputs[:value]

        if bullet.save
          { marketplace_listing_plan_bullet: bullet }
        else
          errors = bullet.errors.full_messages.join(", ")
          raise Errors::Unprocessable.new("Could not update bullet point on the Marketplace " +
                                          "listing plan: #{errors}")
        end
      end
    end
  end
end
