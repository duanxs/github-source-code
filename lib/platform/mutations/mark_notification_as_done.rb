# frozen_string_literal: true

module Platform
  module Mutations
    class MarkNotificationAsDone < Platform::Mutations::Base
      extend Helpers::Newsies

      description "Marks a notification as done"
      feature_flag :pe_mobile
      minimum_accepted_scopes ["notifications"]
      areas_of_responsibility :notifications

      argument :id, ID, "The NotificationThread id.", required: true, loads: Objects::NotificationThread, as: :notification_thread

      field :success, Boolean, "Did the operation succeed?", null: true
      field :viewer, Objects::User, "The user that the notification belongs to.", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, notification_thread:, **inputs)
        notification_thread.async_thread.then do |thread|
          raise Errors::NotFound.new("Could not resolve to a node with the global id of '#{inputs[:id]}'.") unless thread

          permission.access_allowed?(
            :mark_notification_as_done,
            resource: thread,
            current_repo: nil,
            current_org: nil,
            allow_integrations: false,
            allow_user_via_integration: false,
          )
        end
      end

      def resolve(notification_thread:, **inputs)
        self.class.unpack_newsies_response!(
          GitHub.newsies.web.mark_summaries_as_archived(context[:viewer], [notification_thread.summary_id]),
        )

        {
          success: true,
          viewer: context[:viewer],
        }
      end
    end
  end
end
