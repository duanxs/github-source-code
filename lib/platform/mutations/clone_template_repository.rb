# frozen_string_literal: true

module Platform
  module Mutations
    class CloneTemplateRepository < Platform::Mutations::Base
      description "Create a new repository with the same files and directory structure as a " \
                  "template repository."

      def self.async_api_can_modify?(permission, repository:, **inputs)
        permission.async_owner_if_org(repository).then do |org|
          permission.access_allowed?(:create_repo, resource: repository, repo: repository,
                                     current_org: org, allow_integrations: true,
                                     allow_user_via_integration: true)
        end
      end

      minimum_accepted_scopes ["public_repo"]
      areas_of_responsibility :repositories

      argument :repository_id, ID, "The Node ID of the template repository.", required: true,
        loads: Objects::Repository
      argument :name, String, "The name of the new repository.", required: true
      argument :owner_id, ID, "The ID of the owner for the new repository.", required: true,
        loads: Interfaces::RepositoryOwner
      argument :description, String, "A short description of the new repository.", required: false
      argument :visibility, Enums::RepositoryVisibility,
        "Indicates the repository's visibility level.", required: true
      argument :include_all_branches, Boolean,
        "Whether to copy all branches from the template to the new repository. " \
        "Defaults to copying only the default branch of the template.",
        required: false, default_value: false

      field :repository, Objects::Repository, "The new repository.", null: true

      def resolve(repository:, name:, owner:, visibility:, description: nil, include_all_branches: false)
        is_mine = owner == context[:viewer]
        can_write_to_org = owner.organization? &&
          owner.can_create_repository?(context[:viewer], visibility: visibility)

        if !is_mine && !can_write_to_org
          raise Errors::Forbidden,
            "#{context[:viewer]} cannot create a repository for #{owner}."
        end

        reflog_data = {
          real_ip: context[:rails_request]&.remote_ip,
          user_login: context[:viewer].login,
          user_agent: context[:user_agent],
          from: GitHub.context[:from],
          via: "template repository clone",
        }

        cloner = ::CloneTemplateRepository.new(repository,
          owner: owner,
          actor: context[:viewer],
          name: name,
          copy_branches: include_all_branches,
          description: description,
          visibility: visibility,
          reflog_data: reflog_data,
        )

        if cloner.clone
          { repository: cloner.new_repository }
        else
          raise Errors::Unprocessable.new("Could not clone: #{cloner.error}")
        end
      end
    end
  end
end
