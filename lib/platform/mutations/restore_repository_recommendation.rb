# frozen_string_literal: true

module Platform
  module Mutations
    class RestoreRepositoryRecommendation < Platform::Mutations::Base
      description "Restores a dismissed a recommended repository for the current viewer."
      visibility :internal

      minimum_accepted_scopes ["public_repo"]

      argument :repository_id, ID, "The Node ID of the repository.", required: true, loads: Objects::Repository

      field :repository, Objects::Repository, "The restored repository.", null: true

      def resolve(repository:, **inputs)

        recommendation = RepositoryRecommendation.new(repository: repository)
        if recommendation.restore_for(context[:viewer])
          { repository: repository }
        else
          raise Errors::Validation.new("Could not restore dismissed repository recommendation " +
                                       "at this time.")
        end
      end
    end
  end
end
