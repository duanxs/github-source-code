# frozen_string_literal: true

module Platform
  module Mutations
    class UpdateEmailRoute < Platform::Mutations::Base
      description "Updates the user's notification routing settings for an organization."
      areas_of_responsibility :notifications
      visibility :internal
      minimum_accepted_scopes ["user"]

      argument :organization_id, ID, "The ID of the organization to update routing settings for.", required: true, loads: Objects::Organization
      argument :email, String, "The email address to send notifications to for this organization.", required: true

      field :email_route, Objects::EmailRoute, "The notification email route that was updated.", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, **inputs)
        user = permission.viewer
        permission.access_allowed?(:update_user_notification_settings, resource: user, current_org: nil, current_repo: nil, allow_integrations: false, allow_user_via_integration: false)
      end

      def resolve(organization:, **inputs)
        viewer = context[:viewer]
        unless viewer.affiliated_organizations.include?(organization)
          raise Errors::Unprocessable.new("You can only configure email routing for organizations you are affiliated with.")
        end

        email = inputs[:email].strip.downcase
        notifiable_emails = viewer.notifiable_emails.map(&:downcase)

        if organization.restrict_notifications_to_verified_domains?
          org_notifiable_emails = organization.async_notifiable_emails_for(viewer).sync.map { |email| email.email.downcase }
          error_message = "Couldn't save #{email} because it is not eligible to receive notifications for this organization."
          raise Errors::Unprocessable.new(error_message) unless org_notifiable_emails.include?(email)
        end

        unless notifiable_emails.include?(email)
          # Clear unverified emails if somehow an unverified email is set to
          # receive notifications
          response = GitHub.newsies.get_and_update_settings(viewer) do |settings|
            settings.clear_unverified_emails(notifiable_emails)
          end

          if response.success?
            raise Errors::Unprocessable.new("Couldn't save #{email} because it is not verified.")
          else
            raise Errors::Unprocessable.new("Unable to update notification settings.")
          end
        end

        response = GitHub.newsies.get_and_update_settings(viewer) do |settings|
          settings.email organization, email
          settings.clear_unverified_emails(notifiable_emails)
          @settings = settings
        end

        raise Errors::Unprocessable.new("Unable to update notification settings.") unless response.success?

        {
          email_route: Platform::Models::EmailRoute.new(organization, @settings),
        }
      end
    end
  end
end
