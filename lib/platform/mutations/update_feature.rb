# frozen_string_literal: true

module Platform
  module Mutations
    class UpdateFeature < Platform::Mutations::Base
      description "Updates a feature flag."

      visibility :internal
      minimum_accepted_scopes ["devtools"]

      argument :feature_id, ID, "The ID of the feature to be updated", required: true, loads: Objects::Feature
      argument :name, String, "The name of the feature to be used in code", required: true
      argument :description, String, "A brief description of the feature.", required: false

      error_fields
      field :feature, Objects::Feature, "The updated feature.", null: true

      def resolve(feature:, **inputs)

        if feature.update(name: inputs[:name], description: inputs[:description])
          { feature: feature, errors: [] }
        else
          {
            feature: nil,
            errors: Platform::UserErrors.mutation_errors_for_model(feature),
          }
        end
      end
    end
  end
end
