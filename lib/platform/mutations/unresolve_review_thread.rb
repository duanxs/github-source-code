# frozen_string_literal: true

module Platform
  module Mutations
    class UnresolveReviewThread < Platform::Mutations::Base
      description "Marks a review thread as unresolved."

      minimum_accepted_scopes ["public_repo"]

      argument :thread_id, ID, required: true, description: "The ID of the thread to unresolve", loads: Objects::PullRequestReviewThread

      field :thread, Objects::PullRequestReviewThread, "The thread to resolve.", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, thread:, **inputs)
        thread.async_pull_request.then do |pull|
          permission.async_repo_and_org_owner(pull).then do |repo, org|
            permission.access_allowed?(:resolve_pull_request_review_thread, repo: repo, current_org: org, resource: pull, allow_integrations: true, allow_user_via_integration: true)
          end
        end
      end

      def resolve(thread:)
        thread.async_to_activerecord.then do |review_thread|
          review_thread.unresolve
        end
        {
          thread: thread,
        }
      end
    end
  end
end
