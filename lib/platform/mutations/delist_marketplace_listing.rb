# frozen_string_literal: true

module Platform
  module Mutations
    class DelistMarketplaceListing < Platform::Mutations::Base
      description "Remove an approved Marketplace listing from the public Marketplace."
      visibility :internal
      areas_of_responsibility :marketplace

      minimum_accepted_scopes ["repo"]

      argument :id, ID, "The ID of the Marketplace listing to delist.", required: true, loads: Objects::MarketplaceListing, as: :listing
      argument :message, String, "A custom message for the integrator.", required: false

      field :marketplace_listing, Objects::MarketplaceListing, "The updated Marketplace listing.", null: true

      def resolve(listing:, **inputs)

        unless context[:viewer].can_admin_marketplace_listings?
          raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to " +
                                         "delist the Marketplace listing.")
        end

        unless listing.can_delist?
          raise Errors::Validation.new("Marketplace listing cannot be delisted.")
        end

        listing.delist!(context[:viewer], inputs[:message])

        { marketplace_listing: listing }
      end
    end
  end
end
