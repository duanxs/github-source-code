# frozen_string_literal: true

module Platform
  module Mutations
    class UpdateOrganizationDiscussionComment < Platform::Mutations::Base
      description "Updates a discussion comment."
      minimum_accepted_scopes ["write:discussion"]
      visibility :under_development
      areas_of_responsibility :orgs

      argument :id, ID, "The ID of the comment to modify.", required: true, loads: Objects::OrganizationDiscussionComment, as: :comment
      argument :body, String, "The updated text of the comment.", required: true
      argument :body_version, String, "The current version of the body content.", required: false

      field :comment, Objects::OrganizationDiscussionComment, "The updated comment.", null: true

      def resolve(comment:, **inputs)
        unless comment.async_viewer_can_update?(context[:viewer]).sync
          message = "#{context[:viewer]} does not have permission to update the comment #{comment.global_relay_id}."
          raise Errors::Forbidden.new(message)
        end

        if (version = inputs[:body_version]) && version != comment.body_version
          raise Errors::StaleData.new({
            stale: true,
            updated_markdown: comment.body,
            updated_html: comment.body_html,
            version: comment.body_version,
          }.to_json)
        end

        comment.update_body(inputs[:body], context[:viewer])

        if comment.valid?
          { comment: comment }
        else
          raise Errors::Validation.new(comment.errors.full_messages.to_sentence)
        end
      end
    end
  end
end
