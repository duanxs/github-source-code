# frozen_string_literal: true

module Platform
  module Mutations
    class DeleteRecoveryToken < Platform::Mutations::Base
      description "Deletes a recovery token to a user."

      visibility :internal

      minimum_accepted_scopes ["repo"]

      argument :id, ID, "The DelegatedRecoveryToken ID to delete.", required: true

      field :is_destroyed, Boolean, "Whether or not the token was successfully destroyed.", null: true

      def resolve(**inputs)
        begin
          token = Helpers::NodeIdentification.typed_object_from_id([Objects::DelegatedRecoveryToken], inputs[:id], context)
        rescue Platform::Errors::NotFound
          # token not found, authorization hacking, or just straight up bug
        end

        # type of record returned and authorization are checked in #typed_object_from_id
        if token && token.destroy.destroyed?
          GitHub.dogstats.increment("delegated_account_recovery_token", tags: ["destroy:success"])
          { is_destroyed: true }
        else
          GitHub.dogstats.increment("delegated_account_recovery_token", tags: ["destroy:failure"])
          raise Errors::Unprocessable.new("Could not revoke token.")
        end
      end
    end
  end
end
