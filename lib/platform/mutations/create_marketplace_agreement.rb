# frozen_string_literal: true

module Platform
  module Mutations
    class CreateMarketplaceAgreement < Platform::Mutations::Base
      description "Creates a new Marketplace legal agreement."
      visibility :internal
      areas_of_responsibility :marketplace

      minimum_accepted_scopes ["site_admin"]

      argument :version, String, "The version for the new agreement.", required: true
      argument :body, String, "The Markdown text of the agreement.", required: true
      argument :signatory_type, Enums::MarketplaceAgreementSignatoryType, "Who is the intended audience for this agreement?", required: true

      field :marketplace_agreement, Objects::MarketplaceAgreement, "The new Marketplace agreement.", null: true

      def resolve(**inputs)
        unless context[:viewer].can_admin_marketplace_listings?
          raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to create a " +
                                         "new Marketplace agreement.")
        end

        agreement = Marketplace::Agreement.new(body: inputs[:body],
                                               version: inputs[:version],
                                               signatory_type: inputs[:signatory_type])
        if agreement.save
          { marketplace_agreement: agreement }
        else
          errors = agreement.errors.full_messages.join(", ")
          raise Errors::Unprocessable.new("Could not create a Marketplace agreement: #{errors}")
        end
      end
    end
  end
end
