# frozen_string_literal: true

module Platform
  module Mutations
    class AddComment < Platform::Mutations::Base
      description "Adds a comment to an Issue or Pull Request."

      minimum_accepted_scopes ["public_repo"]

      argument :subject_id, ID, "The Node ID of the subject to modify.", required: true, loads: Unions::IssueOrPullRequest, as: :object
      argument :body, String, "The contents of the comment.", required: true

      field :comment_edge, Objects::IssueComment.edge_type, "The edge from the subject's comment connection.", null: true

      field :timeline_edge, Unions::IssueTimelineItem.edge_type, "The edge from the subject's timeline connection.", null: true

      field :subject, Platform::Interfaces::Node, "The subject", null: true

      extras [:lookahead]

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, object:, **inputs)
        issueish = if object.is_a?(::PullRequest)
          object.issue
        else
          object
        end
        permission.async_repo_and_org_owner(issueish).then do |repo, org|
          permission.access_allowed?(:create_issue_comment, repo: repo, resource: issueish, current_org: org, allow_integrations: true, allow_user_via_integration: true)
        end
      end

      def resolve(lookahead:, object:, **inputs)
        commentable_object = if object.is_a?(::PullRequest)
          object.issue
        else
          object
        end

        comment = commentable_object.comments.build(body: inputs[:body])
        comment.user = context[:viewer]
        repo = object.repository
        comment.repository = repo

        if context[:permission].integration_user_request?
          comment.performed_via_integration = context[:integration]
        end

        context[:permission].authorize_content(:issue_comment, :create, issue: commentable_object, repo: repo)

        if GitHub::SchemaDomain.allowing_cross_domain_transactions { comment.save }
          comment_relation = commentable_object.comments.filter_spam_for(context[:viewer])
          comment_connection_class = GraphQL::Relay::BaseConnection.connection_for_nodes(comment_relation)
          comment_connection = comment_connection_class.new(comment_relation, {})
          comment_edge = GraphQL::Relay::Edge.new(comment, comment_connection)

          result = { subject: object, comment_edge: comment_edge }

          if lookahead.selects?(:timeline_edge)
            timeline = ArrayWrapper.new(object.timeline_for(context[:viewer]))
            timeline_connection_class = GraphQL::Relay::BaseConnection.connection_for_nodes(timeline)
            timeline_connection = timeline_connection_class.new(timeline, {})
            result[:timeline_edge] = GraphQL::Relay::Edge.new(comment, timeline_connection)
          end

          result
        else
          raise Errors::Unprocessable.new(comment.errors.full_messages.join(", "))
        end
      end
    end
  end
end
