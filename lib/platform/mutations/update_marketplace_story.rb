# frozen_string_literal: true

module Platform
  module Mutations
    class UpdateMarketplaceStory < Platform::Mutations::Base
      areas_of_responsibility :marketplace

      description "Allows updating a Marketplace story."
      visibility :internal

      scopeless_tokens_as_minimum

      argument :id, ID, "Select Marketplace story that matches this ID.", required: true, loads: Objects::MarketplaceStory, as: :story
      argument :featured, Boolean, "Whether or not the story should be marked as featured.", required: true

      field :marketplace_story, Objects::MarketplaceStory, "The updated Marketplace story.", null: true

      def resolve(story:, **inputs)
        unless story.present?
          raise Errors::NotFound.new("Could not find story with id '#{id}'.")
        end

        story.update!(featured: inputs[:featured])

        { marketplace_story: story }
      end
    end
  end
end
