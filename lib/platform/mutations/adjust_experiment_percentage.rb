# frozen_string_literal: true

module Platform
  module Mutations
    class AdjustExperimentPercentage < Platform::Mutations::Base
      description "Adjust the percentage on a experiment."

      visibility :internal
      minimum_accepted_scopes ["devtools"]

      argument :id, ID, "The id of the experiment we want to update the percentage for.", required: true
      argument :percent, Integer, "The percentage the experiment should be set to.", required: true

      field :experiment, Objects::Experiment, "The experiment that was just updated.", null: true

      def resolve(**inputs)
        raise Errors::Unprocessable.new("You don't have permissions to perform that action.") if !Experiment.viewer_can_read?(context[:viewer])
        experiment = Platform::Helpers::NodeIdentification.typed_object_from_id(
          [Objects::Experiment], inputs[:id], context
        )
        experiment.adjust inputs[:percent]
        { experiment: experiment }
      end
    end
  end
end
