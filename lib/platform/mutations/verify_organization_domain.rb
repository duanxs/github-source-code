# frozen_string_literal: true

module Platform
  module Mutations
    class VerifyOrganizationDomain < Platform::Mutations::Base
      description "Verify that an organization domain has the expected DNS record."

      visibility :internal, environments: [:dotcom]

      minimum_accepted_scopes ["admin:org"]

      argument :id, ID, "The organization domain to verify.", required: true, loads: Objects::OrganizationDomain, as: :domain
      argument :is_staff_actor, Boolean, "Whether or not the domain was verified through stafftools.", required: false, default_value: false

      field :organization_domain, Objects::OrganizationDomain, "The organization domain that was verified.", null: true

      def resolve(domain:, **inputs)
        viewer = context[:viewer]

        unless domain.adminable_by?(viewer)
          raise Errors::Forbidden.new("Viewer not authorized to verify this organization domain")
        end

        if domain && domain.verify
          domain.instrument_verify(viewer, inputs[:is_staff_actor])
          { organization_domain: domain }
        else
          raise Errors::Unprocessable.new(domain.errors.full_messages.join(", "))
        end
      end
    end
  end
end
