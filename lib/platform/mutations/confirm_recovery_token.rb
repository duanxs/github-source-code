# frozen_string_literal: true

module Platform
  module Mutations
    class ConfirmRecoveryToken < Platform::Mutations::Base
      description "Marks a given token as confirmed"

      visibility :internal

      minimum_accepted_scopes ["repo"]

      argument :token_id, String, "The DelegatedRecoveryToken ID to delete.", required: true

      field :token_owner, Objects::User, "The subject", null: true

      def resolve(**inputs)
        user = context[:viewer]
        token = user.delegated_recovery_tokens.find_by_token_id(inputs[:token_id])

        if token && token.update(confirmed_at: Time.now.utc)
          recovery_provider = Darrrr.recovery_provider(token.provider)
          DelegatedAccountRecoveryMailer.recovery_token_stored(user, recovery_provider.origin).deliver_later
          { token_owner: user }
        else
          # TODO: This error could be more specific about which attributes failed to save.
          raise Errors::Unprocessable.new("Could not update Recovery Token.")
        end
      end
    end
  end
end
