# frozen_string_literal: true

module Platform
  module Mutations
    class UpdateSponsorsMembershipsCriterion < Platform::Mutations::Base
      description "Updates a criterion associated with a membership"

      visibility :internal
      minimum_accepted_scopes ["biztools"]

      argument :sponsors_memberships_criterion_id, ID,
        description: "The ID of the Sponsors membership criterion to update",
        required: true,
        loads: Objects::SponsorsMembershipsCriterion,
        as: :criterion

      argument :is_met, Boolean,
        description: "Indicates if the criterion should be marked as met",
        required: false

      argument :value, String,
        description: "The value associated with this criterion for this membership",
        required: false

      field :criterion, Objects::SponsorsMembershipsCriterion,
        description: "The updated criterion",
        null: true

      error_fields

      def resolve(criterion:, **inputs)
        unless context[:viewer].can_admin_sponsors_listings?
          raise Errors::Forbidden.new \
            "#{context[:viewer]} does not have permission update criterion"
        end

        if criterion.sponsors_criterion.automated?
          raise Errors::Unprocessable.new "Automated criteria cannot be updated manually"
        end

        criterion.met = inputs[:is_met] if inputs.key?(:is_met)
        criterion.value = inputs[:value] if inputs.key?(:value)

        if criterion.changed?
          criterion.reviewer = context[:viewer]
        end

        if criterion.save
          {
            criterion: criterion,
            errors: [],
          }
        else
          {
            criterion: nil,
            errors: Platform::UserErrors.mutation_errors_for_model(criterion),
          }
        end
      end
    end
  end
end
