# frozen_string_literal: true

module Platform
  module Mutations
    class DelistNonMarketplaceListing < Platform::Mutations::Base
      description "Delist an approved listing so it is removed from Works with GitHub."

      visibility :internal

      minimum_accepted_scopes ["repo"]

      argument :id, ID, "The ID of the Works with GitHub listing to update.", required: true, loads: Objects::NonMarketplaceListing, as: :listing
      argument :message, String, "A custom message for the user who submitted the listing.", required: false

      field :non_marketplace_listing, Objects::NonMarketplaceListing, "The updated Works with GitHub listing.", null: true

      def resolve(listing:, **inputs)

        unless context[:viewer].can_admin_non_marketplace_listings?
          raise Errors::Forbidden.new("#{context[:viewer]} does not have permission " +
                                         "to delist the Works with GitHub listing.")
        end

        unless listing.can_delist?
          raise Errors::Validation.new("Works with GitHub listing cannot be delisted.")
        end

        listing.delist!(context[:viewer], inputs[:message])

        { non_marketplace_listing: listing }
      end
    end
  end
end
