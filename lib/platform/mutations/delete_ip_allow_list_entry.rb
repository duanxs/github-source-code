# frozen_string_literal: true

module Platform
  module Mutations
    class DeleteIpAllowListEntry < Platform::Mutations::Base
      description "Deletes an IP allow list entry."

      minimum_accepted_scopes ["admin:org", "admin:enterprise"]

      argument :ip_allow_list_entry_id, ID, "The ID of the IP allow list entry to delete.", required: true, loads: Objects::IpAllowListEntry

      field :ip_allow_list_entry, Objects::IpAllowListEntry, "The IP allow list entry that was deleted.", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, ip_allow_list_entry:, **inputs)
        owner = ip_allow_list_entry.owner
        if owner.is_a?(::Business)
          permission.access_allowed?(:administer_business,
            resource: owner, repo: nil, organization: nil,
            allow_integrations: false, allow_user_via_integration: false)
        elsif owner.is_a?(::Organization)
          permission.access_allowed?(:v4_manage_org_users,
            resource: owner, organization: owner, current_repo: nil,
            allow_integrations: false, allow_user_via_integration: false)
        end
      end

      def resolve(ip_allow_list_entry:, **inputs)
        ip_allow_list_entry.actor_ip = context[:ip]
        if ip_allow_list_entry.destroy
          { ip_allow_list_entry: ip_allow_list_entry }
        else
          raise Errors::Unprocessable.new(ip_allow_list_entry.errors.full_messages.to_sentence)
        end
      end
    end
  end
end
