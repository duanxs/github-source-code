# frozen_string_literal: true

module Platform
  module Mutations
    class CreateContentAttachment < Platform::Mutations::Base
      visibility :public
      areas_of_responsibility :ce_extensibility

      description "Create a content attachment."

      scopeless_tokens_as_minimum

      limit_actors_to [:github_app]

      argument :content_reference_id, ID, "The node ID of the content_reference.", required: true, loads: Objects::ContentReference
      argument :title, String, "The title of the content attachment.", required: true
      argument :body, String, "The body of the content attachment, which may contain markdown.", required: true

      error_fields

      field :content_attachment, Objects::ContentAttachment, "The newly created content attachment.", null: true

      extras [:execution_errors]

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, content_reference:, **inputs)
        content_reference.async_repository.then do |repo|
          permission.access_allowed?(
            :write_content_reference,
            resource: content_reference,
            current_repo: repo,
            current_org: nil,
            allow_integrations: true,
            allow_user_via_integration: false,
          )
        end
      end

      def resolve(execution_errors:, content_reference:, **inputs)
        content_attachment = ContentReferenceAttachment.new(
          content_reference: content_reference,
          integration: context[:integration],
          title: inputs[:title],
          body: inputs[:body],
          state: :processed,
        )

        if content_attachment.save
          { content_attachment: content_attachment, errors: [] }
        else
          raise Errors::Unprocessable.new(content_attachment.errors.full_messages.join(", "))
        end
      end
    end
  end
end
