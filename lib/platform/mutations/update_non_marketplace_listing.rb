# frozen_string_literal: true

module Platform
  module Mutations
    class UpdateNonMarketplaceListing < Platform::Mutations::Base
      description "Update a Works with GitHub listing."

      visibility :internal

      minimum_accepted_scopes ["repo"]

      argument :id, ID, "The ID of the Works with GitHub listing to update.", required: true, loads: Objects::NonMarketplaceListing, as: :listing
      argument :category_slug, String, "The slug of the category for this listing.", required: false
      argument :description, String, "The listing's description.", required: false
      argument :url, String, "A link to the product.", required: false
      argument :name, String, "The listing's full name.", required: false
      argument :enterprise_compatible, Boolean, "Whether the listing is compatible with GitHub Enterprise", required: false

      field :non_marketplace_listing, Objects::NonMarketplaceListing, "The updated Works with GitHub listing.", null: true

      def resolve(listing:, **inputs)

        if (name = inputs[:name]).present?
          listing.name = name
        end

        if (url = inputs[:url]).present?
          listing.url = url
        end

        if (description = inputs[:description]).present?
          listing.description = description
        end

        if (slug = inputs[:category_slug]).present?
          category = Loaders::ActiveRecord.load(::IntegrationFeature, slug, column: :slug).sync

          unless category
            raise Errors::Validation.new("No such Works with GitHub category exists: #{slug}")
          end

          listing.category = category
        end

        if inputs.key?(:enterprise_compatible)
          listing.enterprise_compatible = inputs[:enterprise_compatible]
        end

        if listing.save
          { non_marketplace_listing: listing }
        else
          errors = listing.errors.full_messages.join(", ")
          raise Errors::Unprocessable.new("Could not update the Works with GitHub " +
                                          "listing: #{errors}")
        end
      end
    end
  end
end
