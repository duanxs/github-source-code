# frozen_string_literal: true

module Platform
  module Mutations
    class GrantOapForMarketplaceListing < Platform::Mutations::Base
      description "Grants a Marketplace listing's OAuth application access to an organization's private data."
      visibility :internal
      areas_of_responsibility :marketplace

      minimum_accepted_scopes ["write:org"]

      argument :organization_id, ID, "The Organization ID to grant the OAuth application policy.", required: true, loads: Objects::Organization
      argument :listing_slug, String, "Grant access to the OAuth app for the listing that matches this slug. It's the short name of the listing used in its URL.", required: true

      def resolve(organization:, **inputs)

        unless organization.adminable_by?(context[:viewer])
          raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to " +
                                         "approve applications.")
        end

        listing = Loaders::ActiveRecord.load(::Marketplace::Listing, inputs[:listing_slug], column: :slug).sync

        if listing&.listable_is_oauth_application?
          organization.approve_oauth_application(listing.listable, approver: context[:viewer])
        else
          raise Errors::Unprocessable.new("Invalid application")
        end

        nil
      end
    end
  end
end
