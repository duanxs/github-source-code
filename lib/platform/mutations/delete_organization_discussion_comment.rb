# frozen_string_literal: true

module Platform
  module Mutations
    class DeleteOrganizationDiscussionComment < Platform::Mutations::Base
      description "Deletes an organization discussion comment."
      minimum_accepted_scopes ["write:discussion"]
      visibility :under_development
      areas_of_responsibility :orgs

      argument :id, ID, "The ID of the comment to delete.", required: true, loads: Objects::OrganizationDiscussionComment, as: :comment

      def resolve(comment:, **inputs)
        unless comment.async_viewer_can_delete?(context[:viewer]).sync
          message = "#{context[:viewer]} does not have permission to delete the comment '#{comment.global_relay_id}'."
          raise Errors::Forbidden.new(message)
        end

        comment.destroy

        {}
      end
    end
  end
end
