# frozen_string_literal: true

module Platform
  module Mutations
    class CreateSubscriptionItem < Platform::Mutations::Base
      description "Creates a subscription item representing a purchase"

      visibility :internal
      minimum_accepted_scopes ["user"]

      argument :quantity, Integer, "How many units of this listing were purchased", required: true
      argument :plan_id, ID, "The ID of the listing plan that was purchased", required: true, loads: Objects::MarketplaceListingPlan
      argument :account, String, "The login of the account that purchased this", required: true
      argument :grant_oap, Boolean, "Whether to grant OAP access to the application", required: false

      field :subscription_item, Objects::SubscriptionItem, "The new subscription item.", null: true

      def resolve(plan:, **inputs)
        account = User.find_by_login(inputs.delete(:account))
        Billing::CreateSubscriptionItem.call(
          subscribable: plan,
          quantity: inputs[:quantity],
          account: account,
          grant_oap: inputs[:grant_oap],
          viewer: context[:viewer],
        )
      rescue Billing::CreateSubscriptionItem::ForbiddenError => e
        raise Errors::Forbidden.new(e.message)
      rescue Billing::CreateSubscriptionItem::UnprocessableError => e
        raise Errors::Unprocessable.new(e.message)
      end
    end
  end
end
