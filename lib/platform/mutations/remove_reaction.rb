# frozen_string_literal: true

module Platform
  module Mutations
    class RemoveReaction < Platform::Mutations::Base
      description "Removes a reaction from a subject."

      minimum_accepted_scopes ["public_repo", "write:discussion"]

      argument :subject_id, ID, "The Node ID of the subject to modify.", required: true, loads: Interfaces::Reactable, as: :reactable
      argument :content, Enums::ReactionContent, "The name of the emoji reaction to remove.", required: true

      field :subject, Interfaces::Reactable, "The reactable subject.", null: true

      field :reaction, Objects::Reaction, "The reaction object.", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, reactable:, **inputs)
        reactable = reactable.is_a?(::PullRequest) ? reactable.issue : reactable
        reaction = permission.viewer.reactions.find_by_subject_id_and_content(reactable.id, inputs[:content])
        if reactable.is_a?(::DiscussionPost) || reactable.is_a?(::DiscussionPostReply)
          reactable.async_organization.then do |org|
            permission.access_allowed?(:delete_team_discussion_related_reaction, organization: org, reaction: reaction, resource: reactable, current_repo: nil, allow_integrations: true, allow_user_via_integration: true)
          end
        else
          permission.async_repo_and_org_owner(reactable).then do |repo, org|
            permission.access_allowed?(:delete_reaction, repo: repo, reaction: reaction, current_org: org, resource: reactable, allow_integrations: true, allow_user_via_integration: true)
          end
        end
      end

      def resolve(reactable:, **inputs)
        if reactable.is_a?(::PullRequest)
          pull = reactable
          reactable = reactable.issue
        end

        if context[:viewer].blocked_by?(reactable.reaction_admin)
          raise Errors::Forbidden, "User is blocked from reacting to this subject."
        end

        if reactable.async_locked_for?(context[:viewer]).sync
          raise Errors::Forbidden, "User can not react to locked subject."
        end

        reaction = reactable.unreact(actor: context[:viewer], content: inputs[:content])

        # reactable.unreact returns a new record if the reaction does not exist
        # In this scenario, we want to return a NOT_FOUND because calling *any*
        # attribute on that reaction will cause the request to error
        if reaction&.new_record?
          raise Errors::NotFound.new("Could not find a reaction of #{inputs[:content]} on the subject id #{inputs[:subject_id]}")
        end

        unless reaction.status == :deleted
          raise Errors::Unprocessable.new("Could not remove reaction from #{reactable.class.name} with ID #{reactable.id}.")
        end

        { subject: pull || reactable, reaction: reaction }
      end
    end
  end
end
