# frozen_string_literal: true

module Platform
  module Mutations
    class UpdateProject < Platform::Mutations::Base
      description "Updates an existing project."

      minimum_accepted_scopes ["public_repo", "write:org"]

      argument :project_id, ID, "The Project ID to update.", required: true, loads: Objects::Project
      argument :name, String, "The name of project.", required: false
      argument :body, String, "The description of project.", required: false
      argument :state, Enums::ProjectState, "Whether the project is open or closed.", required: false
      argument :public, Boolean, "Whether the project is public or not.", required: false
      argument :organization_permission, Enums::ProjectPermission, "The permission that all members of the owning organization have on this project.", required: false

      field :project, Objects::Project, "The updated project.", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, project:, **inputs)
        auth_options = {
          resource: project,
          current_org: nil,
          allow_integrations: true,
          allow_user_via_integration: true,
        }

        project.async_owner.then do |project_owner|
          auth_options[:owner] = project_owner

          if project_owner.is_a?(::Organization)
            auth_options[:organization] = project_owner
            auth_options[:current_org] = project_owner
            auth_options[:current_repo] = nil
          elsif project_owner.is_a?(::Repository)
            auth_options[:current_repo] = project_owner
          elsif project_owner.is_a?(::User)
            auth_options[:current_repo] = nil
          end

          permission.access_allowed?(:update_project, auth_options)
        end
      end

      def resolve(project:, **inputs)
        context[:permission].authorize_content(:project, :update, project: project)

        unless project.writable_by?(context[:viewer])
          raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to update this project.")
        end

        collaboration_fields = %w(public organization_permission)

        if (inputs.keys.map(&:to_s) & collaboration_fields).present?
          if project.owner_type == "Repository"
            # Don't touch collaboration fields on repository projects.
            raise Errors::Unprocessable.new("Collaboration-related fields cannot be updated on repository projects.")
          elsif !project.adminable_by?(context[:viewer])
            # Require project admin for changing collaboration fields.
            raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to update this project's collaboration settings.")
          end
        end

        if inputs.key?(:organization_permission)
          if project.owner_type == "User"
            # Don't touch collaboration fields on user projects.
            raise Errors::Unprocessable.new("Collaboration-related fields cannot be updated on user projects.")
          end

          unless project.owner.member?(context[:viewer])
            # Require org membership for changing organizationPermission.
            raise Errors::Forbidden.new("#{context[:viewer]} must be a member of the #{project.owner.login} organization to modify its projects' organizationPermission.}")
          end

          permission = if inputs[:organization_permission] == "none"
            nil
          else
            inputs[:organization_permission].downcase.to_sym
          end

          project.update_org_permission(permission)
        end

        case inputs[:state]
        when "open"
          project.open if project.closed?
        when "closed"
          project.close if project.open?
        end

        project.name = inputs[:name] if inputs.key?(:name)
        project.body = inputs[:body] if inputs.key?(:body)
        project.public = inputs[:public] if inputs.key?(:public)

        if project.save
          { project: project }
        else
          raise Errors::Unprocessable.new(project.errors.full_messages.join(", "))
        end
      end
    end
  end
end
