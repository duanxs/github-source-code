# frozen_string_literal: true

module Platform
  module Mutations
    class DeleteMarketplaceListingPlanBullet < Platform::Mutations::Base
      description "Delete a bullet point from a Marketplace listing payment plan."
      visibility :internal
      areas_of_responsibility :marketplace

      minimum_accepted_scopes ["repo"]

      argument :id, ID, "The ID of the bullet point to delete.", required: true, loads: Objects::MarketplaceListingPlanBullet, as: :bullet

      field :marketplace_listing_plan, Objects::MarketplaceListingPlan, "The Marketplace listing plan from which the bullet point was deleted.", null: true

      def resolve(bullet:, **inputs)

        unless bullet.allowed_to_edit?(context[:viewer])
          raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to delete " +
                                         "the bullet on the Marketplace listing plan.")
        end

        if bullet.destroy
          { marketplace_listing_plan: bullet.listing_plan }
        else
          errors = bullet.errors.full_messages.join(", ")
          raise Errors::Unprocessable.new("Could not delete bullet point on the Marketplace " +
                                          "listing plan: #{errors}")
        end
      end
    end
  end
end
