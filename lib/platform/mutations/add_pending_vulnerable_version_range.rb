# frozen_string_literal: true

module Platform
  module Mutations
    class AddPendingVulnerableVersionRange < Platform::Mutations::Base
      description "Creates or updates a pending vulnerable version range"
      visibility :internal
      minimum_accepted_scopes ["site_admin"]

      argument :external_identifier, String, "The external identifier of the pendingVulnerability", required: true
      argument :affects, String, "Name of package affected", required: true
      argument :requirements, String, "requirement string describing affected version range", required: true
      argument :fixed_in, String, "A version that specifically fixes the vulnerability for this affected range", required: true

      field :pending_vulnerability, Objects::PendingVulnerability, "The pendingVulnerability to which the vulnerable version range was added", null: true
      field :pending_vulnerable_version_range, Objects::PendingVulnerableVersionRange, "The new pending vulnerabile version range", null: true

      def resolve(**inputs)
        if !context[:viewer].site_admin?
          raise Errors::Unprocessable.new("This is only allowed for site admins")
        end

        vulns = PendingVulnerability.where external_identifier: inputs[:external_identifier]
        # there needs to be exactly one vuln with the non-unique externalIdentifier in order to proceed safely
        if vulns.empty?
          raise Errors::Unprocessable.new("No PendingVulnerability with externalIdentifier of: #{inputs[:external_identifier]}")
        elsif vulns.length > 1
          raise Errors::Unprocessable.new("More than one PendingVulnerability with externalIdentifier of: #{inputs[:external_identifier]}. Unable to proceed adding pending vulnerable version range.")
        end
        vuln = vulns.first

        new_pvvr = vuln.pending_vulnerable_version_ranges.new affects:      inputs[:affects],
                                                              requirements: inputs[:requirements],
                                                              fixed_in:     inputs[:fixed_in]
        if !new_pvvr.save
          raise Errors::Unprocessable.new(new_pvvr.errors.full_messages.join(", "))
        end
        { pending_vulnerability: vuln, pending_vulnerable_version_range: new_pvvr }
      end
    end
  end
end
