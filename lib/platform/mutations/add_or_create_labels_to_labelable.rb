# frozen_string_literal: true

module Platform
  module Mutations
    class AddOrCreateLabelsToLabelable < Platform::Mutations::Base
      description "Adds labels to a labelable object."

      visibility :internal
      minimum_accepted_scopes ["public_repo"]
      areas_of_responsibility :issues

      argument :labelable_id, ID, "The id of the labelable object to add labels to.", required: true, loads: Interfaces::Labelable
      argument :labels, [Inputs::AddOrCreateLabelsLabelInput], "The label attributes to add.", required: true

      error_fields
      field :labelable_record, Interfaces::Labelable, "The item that was labeled.", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, labelable:, **inputs)
        permission.async_repo_and_org_owner(labelable).then do |repo, org|
          permission.access_allowed?(:add_label, repo: repo, resource: labelable, current_org: org, allow_integrations: true, allow_user_via_integration: true)
        end
      end

      def resolve(labelable:, **inputs)
        record = labelable
        issue = record.is_a?(PullRequest) ? record.issue : record

        unless issue.labelable_by?(actor: context[:viewer])
          raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to add labels in this repository.")
        end

        issue.async_repository.then do |repository|
          if record.is_a?(Issue) && !repository.has_issues?
            raise Errors::Unprocessable::IssuesDisabled.new
          end

          context[:permission].authorize_content(:issue, :update, repo: repository)

          label_names = inputs[:labels].map { |l| l[:name] }
          labels_to_add = repository.find_labels_by_name(label_names).to_a
          existing_label_names = labels_to_add.map { |l| l.name.downcase }

          inputs[:labels].each do |label|
            unless existing_label_names.include?(label[:name].downcase)
              new_label = repository.labels.new(name: label[:name], description: label[:description], color: label[:color])
              labels_to_add.push(new_label)
            end
          end

          if @context[:permission].integration_user_request?
            issue.modifying_integration = @context[:integration]
          end

          if labels_to_add.any?(&:invalid?)
            next {
              labelable_record: nil,
              errors: Platform::UserErrors.mutation_errors_for_models(labels_to_add, path_prefix: ["input", "labels"]),
            }
          end

          labels_to_add.each do |label|
            if label.new_record?
              label.save
              label.instrument_creation(context: "api", issue_id: issue.id)
            end
          end
          issue.add_labels(labels_to_add)

          if issue.valid?
            {
              labelable_record: issue,
              errors: [],
            }
          else
            {
              labelable_record: nil,
              errors: Platform::UserErrors.mutation_errors_for_model(issue),
            }
          end
        end
      end
    end
  end
end
