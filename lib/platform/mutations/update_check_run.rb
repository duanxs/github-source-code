# frozen_string_literal: true

module Platform
  module Mutations
    class UpdateCheckRun < Platform::Mutations::Base
      include Platform::Helpers::GitHubAppValidation

      description "Update a check run"
      areas_of_responsibility :checks

      minimum_accepted_scopes ["public_repo"]

      limit_actors_to [:github_app]

      argument :repository_id, ID, "The node ID of the repository.", required: true, loads: Objects::Repository, as: :repo
      argument :check_run_id, ID, "The node of the check.", required: true, loads: Objects::CheckRun

      argument :name, String, "The name of the check.", required: false
      argument :display_name, String, "Optional name that, if defined, will be used in the UI instead of the name.", required: false, visibility: :internal
      argument :details_url, Scalars::URI, "The URL of the integrator's site that has the full details of the check.", required: false
      argument :external_id, String, "A reference for the run on the integrator's system.", required: false
      argument :status, Enums::RequestableCheckStatusState, "The current status.", required: false
      argument :started_at, Scalars::DateTime, "The time that the check run began.", required: false
      argument :conclusion, Enums::CheckConclusionState, "The final conclusion of the check.", required: false
      argument :completed_at, Scalars::DateTime, "The time that the check run finished.", required: false
      argument :output, Inputs::CheckRunOutput, "Descriptive details about the run.", required: false
      argument :actions, [Inputs::CheckRunAction], "Possible further actions the integrator can perform, which a user may trigger.", required: false
      argument :steps, [Inputs::CheckStepData], "Steps information to be updated", visibility: :internal, required: false
      argument :streaming_log, Inputs::StreamingLogData, "Streaming log information", visibility: :internal, required: false
      argument :completed_log, Inputs::CompletedLogData, "The completed log information", visibility: :internal, required: false
      argument :number, Int, "The topological order of the check run within the check suite.", visibility: :internal, required: false

      error_fields

      field :check_run, Objects::CheckRun, "The updated check run.", null: true

      extras [:execution_errors]

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, repo:, **inputs)
        permission.async_owner_if_org(repo).then do |org|
          permission.access_allowed?(:write_check_run, resource: repo, current_repo: repo, current_org: org, allow_integrations: true, allow_user_via_integration: true)
        end
      end

      def resolve(check_run:, repo:, execution_errors:, **inputs)

        if inputs[:output] && inputs[:output][:annotations] && inputs[:output][:annotations].size > Inputs::CheckAnnotationData::MAXIMUM_PER_REQUEST
          return {
            check_run: nil,
            errors: [{
              "path"=>["input", "output", "annotations"],
              "message"=>"Annotations exceeds a maximum quantity of #{Inputs::CheckAnnotationData::MAXIMUM_PER_REQUEST}",
            }],
          }
        end

        check_run_data = {}

        if repo.id != check_run.check_suite.repository_id # effectively, a 404
          raise Platform::Errors::NotFound.new("Could not resolve check run `#{check_run.global_relay_id}` to repository `#{repo.global_relay_id}`")
        end

        unless allowed_to_modify_app?(app_id: check_run.check_suite.github_app_id) # effectively, a 403
          raise Platform::Errors::Forbidden.new("GitHub App `#{context[:integration].id}` can't manage check suite `#{check_run.check_suite.global_relay_id}`")
        end

        if inputs[:conclusion] == "stale"
          raise Platform::Errors::Validation.new("You cannot set a check run with `conclusion: stale`. `stale` is for internal use only.")
        end

        check_run.name         = inputs[:name] if inputs[:name].present?
        check_run.details_url  = inputs[:details_url].to_s if inputs[:details_url].present?
        check_run.conclusion   = inputs[:conclusion] if inputs[:conclusion].present?
        check_run.started_at   = inputs[:started_at] if inputs[:started_at].present?
        check_run.completed_at = inputs[:completed_at] if inputs[:completed_at].present?
        check_run.external_id  = inputs[:external_id] if inputs[:external_id].present?
        check_run.display_name = inputs[:display_name] if inputs[:display_name].present?

        if inputs[:conclusion].present?
          check_run.status = "completed"
        elsif inputs[:status].present?
          check_run.status = inputs[:status]
        end

        image_data = []
        annotation_data = []
        if inputs[:output].present?
          inputs[:output].each do |output_key, output_value|
            if output_key == :images
              inputs[:output][:images].each do |image_hash|
                image_hash = image_hash.to_h
                # need to force conversion from `Addressable::URI`
                image_hash[:image_url] = image_hash[:image_url].to_s
                image_data << image_hash.to_h
              end
            elsif output_key == :annotations
              annotation_data = inputs[:output][:annotations].map do |annotation|
                Inputs::CheckAnnotationData.hash_from_input(annotation)
              end
            else
              check_run_data[output_key] = output_value
            end
          end
        end

        check_run.summary = check_run_data[:summary] if check_run_data[:summary].present?
        check_run.title   = check_run_data[:title] if check_run_data[:title].present?

        if check_run_data[:text].present?
          truncator = CheckRun::Truncator.new(check_run_data[:text])
          check_run.text = truncator.truncate
          if truncator.truncated?
            truncator.report_silent_truncation!(
              check_run: check_run,
              repo: repo,
              integration: context[:integration],
              type: "an existing",
            )
          end
        end

        check_run.images = image_data

        check_run.annotations.build(annotation_data)

        actions = inputs[:actions]&.map do |action|
          CheckRunAction.new(action[:label], action[:identifier], action[:description])
        end

        check_run.actions = (actions || [])

        if inputs[:steps]
          steps = check_run.steps
          inputs[:steps].each do |input_step|
            check_step = steps.find { |check_step| check_step.number == input_step[:number] }
            check_step ||= check_run.steps.build(number: input_step[:number])

            check_step.name                = input_step[:name] if input_step[:name].present?
            if input_step[:completed_log].present?
              check_step.completed_log_url   = input_step[:completed_log][:url]
              check_step.completed_log_lines = input_step[:completed_log][:lines]
            end
            check_step.completed_at        = input_step[:completed_at] if input_step[:completed_at].present?
            check_step.started_at          = input_step[:started_at] if input_step[:started_at].present?
            check_step.status              = input_step[:status] if input_step[:status].present?
            check_step.conclusion          = input_step[:conclusion] if input_step[:conclusion].present?
            check_step.external_id         = input_step[:external_id] if input_step[:external_id].present?
          end
        end

        if inputs[:completed_log].present?
          check_run.completed_log_url = inputs[:completed_log][:url]
          check_run.completed_log_lines = inputs[:completed_log][:lines]
        end
        check_run.number = inputs[:number] if inputs[:number].present?
        check_run.streaming_log_url = inputs[:streaming_log][:url] if inputs[:streaming_log]

        begin
          if check_run.save
            { check_run: check_run, errors: [] }
          else
            Platform::UserErrors.append_legacy_mutation_model_errors_to_context(check_run, execution_errors)
            check_run_translate_hash = {
                                        summary: ["output", "summary"],
                                      }
            check_run_action_path_prefix = ["input", "actions"]
            {
              check_run: nil,
              errors: (
                Platform::UserErrors.mutation_errors_for_model(check_run, translate: check_run_translate_hash) +
                Platform::UserErrors.mutation_errors_for_models(check_run.actions, path_prefix: check_run_action_path_prefix)
              )
            }
          end
        rescue ActiveRecord::RecordNotUnique => e
          message = "Record not unique."
          # Extract the specific case that we know of, where a race condition means a check step already exists with the same number
          if match = e.message.match(/Duplicate entry '\d*-(\d*)' for key 'index_check_steps_on_check_run_id_and_number'/)
            message = "Duplicate check step `#{match[1]}` already exists."
          end
          raise Platform::Errors::Unprocessable.new(message)
        end
      end
    end
  end
end
