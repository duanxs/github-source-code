# frozen_string_literal: true

module Platform
  module Mutations
    class CreateExperiment < Platform::Mutations::Base
      description "Creates a experiment."

      visibility :internal
      minimum_accepted_scopes ["devtools"]

      argument :name, String, "The name of the experiment.", required: true

      field :experiment, Objects::Experiment, "The experiment that was just created.", null: true

      def resolve(**inputs)
        raise Errors::Unprocessable.new("You don't have permissions to perform that action.") if !Experiment.viewer_can_read?(context[:viewer])

        experiment = ::Experiment.create do |e|
          e.name = inputs[:name]
        end

        if experiment.valid?
          { experiment: experiment }
        else
          raise Errors::Unprocessable.new("Can’t save! #{experiment.errors.full_messages.to_sentence}.")
        end
      end
    end
  end
end
