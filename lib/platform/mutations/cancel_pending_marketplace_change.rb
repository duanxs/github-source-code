# frozen_string_literal: true

module Platform
  module Mutations
    class CancelPendingMarketplaceChange < Platform::Mutations::Base
      description "Cancel a pending change for a marketplace purchase."
      visibility :internal
      areas_of_responsibility :gitcoin

      minimum_accepted_scopes ["site_admin"]

      argument :id, ID, "The ID of the pending marketplace change you wish to cancel.", required: true

      field :pending_marketplace_change, Objects::PendingMarketplaceChange, "The pending change for the marketplace purchase.", null: true

      def resolve(**inputs)
        change = Loaders::ActiveRecord.load(::Billing::PendingSubscriptionItemChange, inputs[:id].to_i, column: :id).sync

        unless context[:viewer]&.site_admin? || change.user.adminable_by?(context[:viewer])
          raise Errors::Forbidden.new("#{context[:viewer]} does not have permission to manage this account.")
        end

        event = generate_hook_event(change)
        delivery_system = Hook::DeliverySystem.new(event)
        delivery_system.generate_hookshot_payloads

        if change.destroy
          delivery_system.deliver_later
        end

        { pending_marketplace_change: change }
      end

      private

      def generate_hook_event(change)
        Hook::Event::MarketplacePurchaseEvent.new({
          action: "pending_change_cancelled",
          sender_id: change.user.id,
          pending_subscription_item_change_id: change.id,
          subscription_item_id: change.subscription_item.id,
        })
      end
    end
  end
end
