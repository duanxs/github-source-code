# frozen_string_literal: true

module Platform
  module Mutations
    class UpdateSponsorsMembership < Platform::Mutations::Base
      description "Updates a Sponsors membership"

      visibility :internal
      minimum_accepted_scopes ["biztools"]

      argument :sponsors_membership_id, ID, "The ID of the Sponsors membership to update", required: true, loads: Objects::SponsorsMembership

      argument :billing_country, String,
        description: "The alpha2 country code for the billing country for this membership",
        required: false

      field :sponsors_membership, Objects::SponsorsMembership, "The updated Sponsors membership", null: true

      def resolve(sponsors_membership:, **inputs)
        unless context[:viewer].can_admin_sponsors_listings?
          raise Errors::Forbidden.new \
            "#{context[:viewer]} does not have permission to update the Sponsors membership"
        end

        sponsors_membership.billing_country = inputs[:billing_country] if inputs.key?(:billing_country)

        if sponsors_membership.save
          { sponsors_membership: sponsors_membership.reload }
        else
          raise Errors::Unprocessable.new \
            "An error occurred when updating the Sponsors membership for #{sponsors_membership.sponsorable.login}: #{sponsors_membership.errors.full_messages.to_sentence}"
        end
      end
    end
  end
end
