# frozen_string_literal: true

module Platform
  module Mutations
    class ReportContent < Platform::Mutations::Base
      areas_of_responsibility :community_and_safety

      description "Reports content to repository maintainers"

      minimum_accepted_scopes ["public_repo"]

      visibility :under_development

      extras [:execution_errors]

      argument :reported_content_id, ID, "The AbuseReportable ID to report.", required: true, loads: Interfaces::AbuseReportable
      argument :reason, Enums::AbuseReportReason, description: "The reason this content is being reported.", required: false

      field :reported_content, Interfaces::AbuseReportable, "The reported content.", null: true
      error_fields

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, reported_content:, **inputs)
        if reported_content.respond_to?(:repository)
          permission.async_repo_and_org_owner(reported_content).then do |repo, org|
            permission.access_allowed?(:report_content, resource: reported_content, current_repo: repo, current_org: org, allow_integrations: false, allow_user_via_integration: false)
          end
        else
          false
        end
      end

      def resolve(execution_errors:, reported_content:, **inputs)
        context[:permission].authorize_content(:report_content, :create, reported_content: reported_content)

        reason = inputs[:reason] || "UNSPECIFIED"

        abuse_report = AbuseReport.new(
          reporting_user: @context[:viewer],
          reported_user: reported_content.user,
          reported_content: reported_content,
          repository_id: reported_content.repository,
          reason: reason.downcase,
          show_to_maintainer: true,
        )

        if abuse_report.save
          {
            reported_content: reported_content,
            errors: [],
          }
        else
          Platform::UserErrors.append_legacy_mutation_model_errors_to_context(abuse_report, execution_errors)
          {
            reported_content: nil,
            errors: Platform::UserErrors.mutation_errors_for_model(abuse_report),
          }
        end
      end
    end
  end
end
