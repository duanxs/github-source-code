# frozen_string_literal: true

module Platform
  module Mutations
    class DeleteOrganizationDomain < Platform::Mutations::Base
      description "Deletes an organization domain."

      visibility :internal, environments: [:dotcom]

      minimum_accepted_scopes ["admin:org"]

      argument :id, ID, "The organization domain ID to delete.", required: true, loads: Objects::OrganizationDomain, as: :domain

      field :organization, Objects::Organization, "The organization from which the domain was deleted.", null: true

      def resolve(domain:, **inputs)

        unless domain.adminable_by?(context[:viewer])
          raise Errors::Forbidden.new("Viewer not authorized to delete organization domains")
        end

        if domain.async_required_for_policy_enforcement?.sync
          raise Errors::Unprocessable.new("Could not delete organization domain: failed to disable notification restrictions.") unless domain.disable_dependent_policies(actor: context[:viewer])
        end

        if domain.destroy
          domain.instrument_destroy(context[:viewer])
          { organization: domain.organization }
        else
          errors = domain.errors.full_messages.join(", ")
          raise Errors::Unprocessable.new("Could not delete organization domain: #{errors}")
        end
      end
    end
  end
end
