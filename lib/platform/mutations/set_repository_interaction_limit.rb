# frozen_string_literal: true

module Platform
  module Mutations
    class SetRepositoryInteractionLimit < Platform::Mutations::Base
      description "Sets an interaction limit setting for a repository."

      visibility :under_development, environments: [:dotcom]
      visibility :internal, environments: [:enterprise]

      minimum_accepted_scopes ["repo"]

      argument :repository_id, ID, "The ID of the repository to set a limit for.", required: true, loads: Objects::Repository, as: :repo
      argument :limit, Enums::RepositoryInteractionLimit, "The limit to set.", required: true
      argument :expiry, Enums::RepositoryInteractionLimitExpiry, "When this limit should expire.", visibility: :internal, required: false
      argument :is_staff_actor, Boolean, "Indicates if the actor should be guarded.", visibility: :internal, default_value: false, required: false

      field :repository, Objects::Repository, "The repository that the interaction limit was set for.", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, repo:, **inputs)
        permission.async_owner_if_org(repo).then do |org|
          permission.access_allowed?(:set_repository_interaction_limits,
            resource: repo, current_repo: repo, current_org: org,
            allow_integrations: true, allow_user_via_integration: true)
        end
      end

      def viewer_can_resolve?(repo, context)
        return true if context[:actor].can_have_granular_permissions?
        repo.async_can_set_interaction_limits?(context[:viewer]).sync
      end

      def resolve(repo:, **inputs)
        unless viewer_can_resolve?(repo, context)
          if context[:viewer].site_admin?
            # Require the site_admin scope for a site admin if this is being called from the API.
            if Platform.requires_scope?(context[:origin]) && !context[:permission].has_scope?("site_admin")
              raise Errors::Forbidden.new "You must use a token with the `site_admin` scope to perform site admin actions."
            end
          else
            raise Errors::Forbidden.new "You are not authorized to set interaction limits for this repository."
          end
        end

        if repo.private?
          raise Errors::Unprocessable.new "Interaction limits cannot be set for private repositories."
        end

        if repo.owner.organization? && ::RepositoryInteractionAbility.has_active_limits?(:organization, repo.owner)
          raise Errors::Unprocessable.new "You cannot set repository level interaction limits when an organization level limit is enabled."
        end

        ability = RepositoryInteractionAbility.new(:repository, repo)

        if inputs[:expiry].present?
          raise Errors::Forbidden.new "Only site admins can specify an expiry." unless context[:viewer].site_admin?

          expiry = inputs[:expiry]
        else
          expiry = :one_day
        end

        ability.set_ability(inputs[:limit], context[:viewer], expiry, staff_actor: inputs[:is_staff_actor])

        { repository: repo }
      end
    end
  end
end
