# frozen_string_literal: true

module Platform
  module Mutations
    module Shared
      module ModifyBranchProtectionRule
        def update_branch_protection_rule(protected_branch, inputs)
          push_actors, dismissal_actors = Promise.all([
            Promise.all((inputs[:push_actor_ids] || []).map { |actor_id|
              Platform::Helpers::NodeIdentification.async_typed_object_from_id([Objects::User, Objects::Team, Objects::App], actor_id, context)
            }),
            Promise.all((inputs[:review_dismissal_actor_ids] || []).map { |actor_id|
              Platform::Helpers::NodeIdentification.async_typed_object_from_id([Objects::User, Objects::Team], actor_id, context)
            }),
          ]).sync

          ProtectedBranch.transaction do
            Ability.transaction do
              # Save the record because some methods called below expect it to exist.
              protected_branch.save!

              # Enforce code review restrictions.
              if inputs[:requires_approving_reviews] == false
                protected_branch.clear_required_pull_request_reviews
              elsif inputs[:requires_approving_reviews] == true || protected_branch.pull_request_reviews_enabled?
                protected_branch.pull_request_reviews_enforcement_level = :non_admins

                unless inputs[:dismisses_stale_reviews].nil?
                  protected_branch.dismiss_stale_reviews_on_push = inputs[:dismisses_stale_reviews]
                end

                unless inputs[:requires_code_owner_reviews].nil?
                  protected_branch.require_code_owner_review = inputs[:requires_code_owner_reviews]
                end

                unless inputs[:required_approving_review_count].nil?
                  protected_branch.required_approving_review_count = inputs[:required_approving_review_count]
                end

                if protected_branch.repository.in_organization?
                  if inputs[:restricts_review_dismissals] == false
                    protected_branch.clear_dismissal_restrictions
                  elsif inputs[:restricts_review_dismissals] || protected_branch.authorized_dismissal_actors_only?
                    protected_branch.replace_dismissal_restricted_users_and_teams(
                      user_ids: dismissal_actors.select { |actor| actor.is_a?(::User) }.map(&:id),
                      team_ids: dismissal_actors.select { |actor| actor.is_a?(::Team) }.map(&:id),
                    )
                  end
                end
              end

              # Enforce status check restrictions.
              if inputs[:requires_status_checks] == false
                protected_branch.clear_required_status_checks
              elsif inputs[:requires_status_checks] == true || protected_branch.required_status_checks_enabled?
                if protected_branch.admin_enforced?
                  protected_branch.required_status_checks_enforcement_level = :everyone
                else
                  protected_branch.required_status_checks_enforcement_level = :non_admins
                end

                unless inputs[:requires_strict_status_checks].nil?
                  protected_branch.strict_required_status_checks_policy = inputs[:requires_strict_status_checks]
                end

                unless inputs[:required_status_check_contexts].nil?
                  protected_branch.replace_status_contexts(inputs[:required_status_check_contexts])
                end
              end

              # Enforce push access restrictions.
              if inputs[:restricts_pushes] == false
                protected_branch.authorized_actors_only = false
              elsif inputs[:restricts_pushes] == true || protected_branch.has_authorized_actors?
                protected_branch.replace_authorized_actors(
                  user_ids: push_actors.select { |actor| actor.is_a?(::User) }.map(&:id),
                  team_ids: push_actors.select { |actor| actor.is_a?(::Team) }.map(&:id),
                  integration_ids: push_actors.select { |actor| actor.is_a?(::Integration) }.map(&:id),
                )
              end

              # Enforce required signatures
              if inputs[:requires_commit_signatures] == true
                protected_branch.enable_required_signatures
              elsif inputs[:requires_commit_signatures] == false
                protected_branch.clear_required_signatures
              end

              # Enforce merge commit prevention
              if inputs[:requires_linear_history] == true
                protected_branch.enable_required_linear_history
              elsif inputs[:requires_linear_history] == false
                protected_branch.clear_required_linear_history
              end

              # Enforce blocking force pushes
              if inputs[:allows_force_pushes] == true
                protected_branch.clear_blocked_force_pushes
              elsif inputs[:allows_force_pushes] == false
                protected_branch.enable_blocked_force_pushes
              end

              # Enforce blocking branch deletion
              if inputs[:allows_deletions] == true
                protected_branch.clear_blocked_deletions
              elsif inputs[:allows_deletions] == false
                protected_branch.enable_blocked_deletions
              end

              # Enforce admin restrictions.
              unless inputs[:is_admin_enforced].nil?
                protected_branch.admin_enforced = inputs[:is_admin_enforced]
              end

              protected_branch.save!
            end

            { branch_protection_rule: Platform::Models::BranchProtectionRule.new(protected_branch) }
          end
        rescue ActiveRecord::RecordInvalid
          raise Errors::Unprocessable.new(protected_branch.errors.full_messages.join(", "))
        rescue ProtectedBranch::OnlyOrgsHaveAuthorizedActors
          raise Errors::Unprocessable.new("Only organization repositories can have users and team restrictions")
        rescue ProtectedBranch::TooManyPermittedActors => e
          raise Errors::Unprocessable.new(e.message)
        end
      end
    end
  end
end
