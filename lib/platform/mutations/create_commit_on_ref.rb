# frozen_string_literal: true

module Platform
  module Mutations
    class CreateCommitOnRef < Platform::Mutations::Base
      description "Create a commit with the provided data."

      visibility :internal

      minimum_accepted_scopes ["public_repo"]

      argument :ref_id, ID, "The ID of the ref on which these changes are based.", required: true, loads: Objects::Ref
      argument :changes, [Inputs::FileChange], "The data which is being submitted for this change.", required: true
      argument :headline, String, "The commit message headline.", required: true
      argument :body, String, "The commit message body.", required: false

      error_fields
      field :ref, Objects::Ref, "The ref that points to the newly created changes.", null: true

      def resolve(ref:, **inputs)
        changes = inputs[:changes].map do |change|
          {
            deletion: change[:is_deletion],
            path:     change[:path],
            content:  change[:content],
            new_path: change[:new_path],
          }
        end

        begin
          ref.create_commit(
            changes: changes,
            author: context[:viewer],
            headline: inputs[:headline],
            body: inputs[:body],
          )

          { ref: ref, errors: [] }
        rescue Git::Ref::UpdateFailed => e
          fail Errors::Forbidden, e.message if e.message == "Cannot write to repository."
        rescue Git::Ref::ProtectedBranchUpdateError => e
          message = "Update failed protected branch policy check."
          { ref: nil, errors: [{ message: message, short_message: message, attribute: "refId" }] }
        rescue Git::Ref::UpdateError => e
          fail Errors::Forbidden, e.message
        end
      end
    end
  end
end
