# frozen_string_literal: true

module Platform
  module Mutations
    class PerformImport < Platform::Mutations::Base
      description "Performs an import of GitHub data"

      feature_flag :gh_migrator_import_to_dotcom

      minimum_accepted_scopes ["admin:org"]

      argument :migration_id, ID, "The Node ID of the importing organization.", required: true, loads: Objects::Migration

      field :migration, Objects::Migration, "The pending import migration.", null: true

      # Determine whether the viewer can access this mutation via the API (called internally).
      # This is where Egress checks for OAuth scopes and GitHub Apps go.
      # Returns `true`, `false`, or `Promise` resolving to `true` or `false`
      def self.async_api_can_modify?(permission, migration:, **inputs)
        Loaders::ActiveRecord.load(::Organization, migration.owner_id).then do |org|
          permission.access_allowed?(:migration_import, resource: org, organization: org, current_repo: nil, allow_integrations: false, allow_user_via_integration: false)
        end
      end

      def resolve(migration:, **inputs)
        unless GitHub.flipper[:gh_migrator_import_to_dotcom].enabled?(migration.owner)
          raise Errors::Forbidden.new("Organization #{migration.owner.login} is not authorized to perform imports.")
        end

        GitHub.migrator.import_later(migration: migration, current_user: context[:viewer])

        { migration: migration }
      end
    end
  end
end
