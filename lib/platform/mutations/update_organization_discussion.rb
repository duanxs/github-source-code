# frozen_string_literal: true

module Platform
  module Mutations
    class UpdateOrganizationDiscussion < Platform::Mutations::Base
      description "Updates a organization discussion."
      visibility :under_development
      areas_of_responsibility :orgs

      minimum_accepted_scopes ["write:discussion"]

      argument :id, ID, "The Node ID of the discussion to modify.", required: true, loads: Objects::OrganizationDiscussion, as: :discussion
      argument :title, String, "The updated title of the discussion.", required: false
      argument :body, String, "The updated text of the discussion.", required: false
      argument :body_version, String, "The current version of the body content. If provided, this update operation will be rejected if the given version does not match the latest version on the server.", required: false
      argument :pinned, Boolean, "If provided, sets the pinned state of the updated discussion.", required: false

      field :discussion, Objects::OrganizationDiscussion, "The updated discussion.", null: true


      def resolve(discussion:, **inputs)
        if ((inputs[:title] || inputs[:body]) &&
            !discussion.async_viewer_can_update?(context[:viewer]).sync)
          message = (
            "#{context[:viewer]} does not have permission to update the discussion #{discussion.global_relay_id}.")
          raise Errors::Forbidden.new(message)
        end

        if inputs[:pinned].present? && !discussion.async_viewer_can_pin?(context[:viewer]).sync
          message = (
            "#{context[:viewer]} does not have permission to pin the discussion #{discussion.global_relay_id}.")
          raise Errors::Forbidden.new(message)
        end

        if (version = inputs[:body_version]) && version != discussion.body_version
          raise Errors::StaleData.new({
            stale: true,
            updated_markdown: discussion.body,
            updated_html: discussion.body_html,
            version: discussion.body_version,
          }.to_json)
        end

        discussion.update_body(inputs[:body], context[:viewer]) if inputs[:body]
        cumulative_changes = discussion.previous_changes

        discussion.title = inputs[:title] if inputs[:title]

        if !inputs[:pinned].nil?
          if inputs[:pinned]
            discussion.pin_by(user_id: context[:viewer].id)
          else
            discussion.unpin
          end
        end

        cumulative_changes = cumulative_changes.merge(discussion.changes)

        if discussion.save
          { discussion: discussion }
        else
          raise Errors::Validation.new(discussion.errors.full_messages.to_sentence)
        end
      end
    end
  end
end
