# frozen_string_literal: true

module Platform
  module Mutations
    class CreateImport < Platform::Mutations::Base
      feature_flag :import_api
      areas_of_responsibility :import_api
      description "Create a new Import"

      minimum_accepted_scopes ["read:user", "public_repo"]

      def self.async_api_can_modify?(permission, **inputs)
        permission.access_allowed?(:import_api_create_repo,
          resource: permission.viewer,
          allow_integrations: true,
          allow_user_via_integration: true,
          current_repo: nil,
          current_org: nil
        )
      end

      field :import, Objects::Import, "The created import", null: true

      def resolve
        unless GitHub.flipper[:import_api].enabled?(context[:viewer])
          raise Errors::Forbidden.new("#{context[:viewer].login} is not authorized to call import APIs.")
        end

        import = Import.new({ creator: context[:viewer] })

        succeeded = import.save
        raise Errors::Unprocessable.new("Could not create import: #{import.errors.full_messages.join(", ")}") unless succeeded

        { import: import }
      end
    end
  end
end
