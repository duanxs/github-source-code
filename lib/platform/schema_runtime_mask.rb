# frozen_string_literal: true

module Platform
  class SchemaRuntimeMask < SchemaVersionMask
    def initialize(target, environment: GitHub.runtime.current)
      super
      @now = Time.now.utc
    end

    protected

    def hidden?(member, context)
      super ||
      hidden_by_feature_flag?(member, context) ||
      hidden_by_mobile_app?(member, context) ||
      hidden_by_accept_header?(member, context) ||
      hidden_by_deprecation?(member, context)
    end

    private

    attr_reader :now

    def hidden_by_feature_flag?(member, context)
      if @target == :internal
        # internal schema, return all the things
        false
      elsif member.respond_to?(:feature_flag) && (required_flag = member.feature_flag)
        # Make sure the provided flags include the required one
        # (`context[:feature_flags]` is filtered for the current viewer in `Platform.execute`)
        !context[:feature_flags].include?(required_flag)
      else
        # There's no flag on this member
        false
      end
    end

    def hidden_by_mobile_app?(member, context)
      if @target == :internal
        # internal schema, return all the things
        false
      elsif member.respond_to?(:mobile_only) && member.mobile_only
        # Hide the member if it is only available to mobile, and the current oauth app
        # is NOT the mobile app.
        !Apps::Internal.capable?(:mobile_client, app: context[:oauth_app])
      else
        false
      end
    end

    NO_PREVIEWS = [].freeze

    def hidden_by_accept_header?(member, context)
      if @target == :internal
        # internal schema, return all the things
        false
      elsif member.respond_to?(:preview_toggled_by) &&
          (toggled_by = member.preview_toggled_by) &&
          (toggled_by_preview = toggled_by[@environment])
        current_previews = context.fetch(:schema_previews, NO_PREVIEWS)
        # This member has a preview in this environment, see if it was enabled
        # by the caller
        !current_previews.include?(toggled_by_preview.toggled_by)
      else
        # This member can't be previewed, so it's definitely
        # not hidden by a preview
        false
      end
    end

    def hidden_by_deprecation?(member, context)
      # Don't hide deprecated fields for enterprise,
      # Use versions instead to sunset fields.
      return false if GitHub.enterprise?
      return false if @target == :internal
      return false unless member.respond_to?(:upcoming_change)
      return false unless change = member.upcoming_change
      now >= change.apply_on && change.active?
    end
  end
end
