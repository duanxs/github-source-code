# frozen_string_literal: true

module Platform
  # A NullObject representing a resource that is readable by
  # anyone under any circumstances.
  # This can be passed to control_access when no other handy
  # object is available.
  class PublicResource
    def readable_by?(_)
      true
    end
  end
end
