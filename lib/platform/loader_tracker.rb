# frozen_string_literal: true

module Platform
  class LoaderTracker
    @@loaders = {}

    def self.reset!
      @@loaders = {}
    end

    def self.loaders
      @@loaders
    end

    def self.loader_name(loader)
      loader.class.name.demodulize.underscore
    end

    def self.loader_tags(loader)
      ["loader:#{loader_name(loader)}"].concat(loader.dog_tags)
    end

    # TODO: According to comments in `ignore_association_loads_test.rb`
    # this method probably should be replaced by batch loader(s).
    def self.ignore_association_loads(&block)
      if Rails.env.production?
        # In production, don't ignore the load. Instead, log it to our dashboards.
        block.call
      else
        GitHub::AssociationInstrumenter.track_loads(nil, &block)
      end
    end

    def self.track_loader(loader)
      @@loaders[loader_name(loader)] ||= []
      loader_tags = loader_tags(loader)

      start = Process.clock_gettime(Process::CLOCK_MONOTONIC)

      result =
        GitHub::MysqlInstrumenter.tag_queries(loader_tags) do
          yield
        end

      ending = Process.clock_gettime(Process::CLOCK_MONOTONIC)

      duration = (ending - start) * 1000

      @@loaders[loader_name(loader)] << duration

      GitHub.dogstats.distribution("platform.loaders.dist.time", duration, tags: loader_tags)
      result
    end
  end
end
