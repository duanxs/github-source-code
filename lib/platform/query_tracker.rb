# frozen_string_literal: true

module Platform
  class QueryTracker
    # Internal: Wraps the execution of a query to provide tracking of some
    # basic performance data, including SQL queries, field resolution timings,
    # and the number of loaded ActiveRecord associations.
    #
    # document  - The GraphQL::Language::Nodes::Document from the parsed query.
    # context   - The Hash used for query execution
    # variables - A Hash of variables used for query execution
    #
    # Returns a hash poplated with the performance data, intended to be merged
    # into the response for site admins.

    attr_reader :document, :context, :variables, :response,
      :query_string, :dog_tags, :dog_sql_tags,
      :origin, :query_depth, :query_complexity,
      :performance_data_total_sql, :mysql_count, :gitrpc_count, :gitrpc_time_ms, :rate_limited, :clock_times,
      :query, :query_hash, :variables_hash, :real_start, :cpu_start, :gitrpc_before_count, :gitrpc_before_time

    # Returns an Array of GraphQL::Schema::Objects which were accessed during the query
    attr_reader :accessed_objects

    attr_accessor :rate_limit_request_query, :query_cost

    def initialize(query)
      @query                      = query
      @document                   = query.document
      @context                    = query.context.to_h
      @variables                  = query.provided_variables
      @query_string               = @context[:query_string] || @document.to_query_string
      @origin                     = @context[:origin]
      @query_hash                 = Platform::Instrumentation::TrackingHash.generate(@query.context[:query_string] || "")
      @variables_hash             = Platform::Instrumentation::TrackingHash.generate(@variables.to_json)
      @performance_data_total_sql = 0
      @mysql_count                = 0
      @gitrpc_count               = 0
      @gitrpc_time_ms             = 0
      @tracked_associations       = {}
      @query_depth                = 0
      @query_cost                 = 0
      @query_complexity           = 0
      @rate_limit_request_query   = false
      @rate_limited               = false
      @clock_times                = { real: 0, cpu: 0, idle: 0 }
      @accessed_objects           = {}

      @dog_tags = ["origin:#{@origin}"]
      @dog_sql_tags = @dog_tags.dup
      if @context[:controller]
        @dog_tags << "controller:#{@context[:controller]}"
        if @context[:controller] == "queries"
          @dog_tags << "operation_name:#{@context[:query_name]}"
          @dog_sql_tags << "operation_name:#{@context[:query_name]}"
        end
      end
      if @context[:action]
        @dog_tags << "action:#{@context[:action]}"
      end
    end

    def track
      track_gitrpc_calls do
        track_mysql_queries do
          track_association_loads do
            track_time { yield }
          end
        end
      end

      set_tracked_data
    end

    def rate_limited?
      !!rate_limited
    end

    def rate_limited!
      @rate_limited = true
    end

    def rate_limit_request_query?
      !!rate_limit_request_query
    end

    private

    def set_tracked_data
      @query_depth                       = context[:query_depth]
      @query_complexity                  = context[:query_complexity]
      instrument!
    end


    def instrument!
      data = (context[:log_data] || {}).merge({
        graphql: true,
        graphql_time: @clock_times[:real],
        graphql_query_byte_size: @query_string.bytesize,
        graphql_variables_byte_size: @variables.to_s.bytesize,
        graphql_operation_name: context[:operation_name],
        graphql_origin: @origin,
        graphql_success: query_success?,
        graphql_query_depth: @query_depth,
        graphql_query_complexity: @query_complexity,
        request_id: GitHub.context[:request_id],
        graphql_schema: context[:target],
        graphql_query_hash: query_hash,
        graphql_variables_hash: variables_hash,
      }).reverse_merge(Platform.extract_relevant_context_keys(context))

      GitHub::Logger.log(data) if GitHub.platform_graphql_logging_enabled?
      GitHub.dogstats.distribution("platform.query.dist.time", @clock_times[:real], tags: dog_tags)
    end

    def track_gitrpc_calls(&block)
      @gitrpc_before_count = GitRPCLogSubscriber.rpc_count
      @gitrpc_before_time = GitRPCLogSubscriber.rpc_time
      GitRPCLogSubscriber.stats_tags << "graphql"
      GitRPCLogSubscriber.tags << "graphql:true"
      GitRPCLogSubscriber.with_track(&block)
      gitrpc_after_count = GitRPCLogSubscriber.rpc_count
      gitrpc_after_time = GitRPCLogSubscriber.rpc_time

      @gitrpc_count = gitrpc_after_count - @gitrpc_before_count
      @gitrpc_time_ms = (gitrpc_after_time - @gitrpc_before_time) * 1000
    ensure
      GitRPCLogSubscriber.tags.clear
    end

    def track_time
      @real_start = Process.clock_gettime(Process::CLOCK_MONOTONIC)
      @cpu_start  = Process.clock_gettime(Process::CLOCK_PROCESS_CPUTIME_ID)
      yield
      cpu  = Process.clock_gettime(Process::CLOCK_PROCESS_CPUTIME_ID) - @cpu_start
      real = Process.clock_gettime(Process::CLOCK_MONOTONIC) - @real_start

      @clock_times[:real] = (real * 1000)
      @clock_times[:cpu]  = (cpu * 1000)
      @clock_times[:idle] = (real - cpu) * 1000
    end

    def track_mysql_queries
      sql_subscriber = ActiveSupport::Notifications.subscribe("sql.active_record") do |name, started, ended, id, payload|
        site = GitHub.site_from_host(payload[:connected_host])
        db_tags = [
          "rpc_site:#{site}",
          "rpc_host:#{payload[:connected_host] || :unknown}",
        ]

        duration = (ended - started) * 1_000
        @performance_data_total_sql += duration
        @mysql_count += 1
        tags = dog_sql_tags + db_tags
        GitHub.dogstats.distribution("platform.query.dist.sql.time", duration, tags: tags)
      end

      GitHub::MysqlInstrumenter.tag_queries("graphql:true") do
        yield
      end
    ensure
      ActiveSupport::Notifications.unsubscribe(sql_subscriber)
    end


    def track_association_loads
      callback = ->(association) do
        reflection = association.reflection
        current_field = @query.context.namespace(:interpreter)[:current_field]

        if Rails.env.test?
          raise Errors::AssociationRefused.new(reflection.name, reflection.active_record.name, current_field&.path)
        else
          key_name = reflection.active_record.name.to_s

          @tracked_associations[key_name] ||= { count: 0, names: [] }
          @tracked_associations[key_name][:count] += 1
          @tracked_associations[key_name][:names] << reflection.name

          field_tag = "field:" + current_field&.path&.downcase&.gsub(".", ":")
          GitHub.dogstats.increment("platform.query.associations", tags: ["association:#{key_name.downcase}", field_tag])
        end
      end

      GitHub::AssociationInstrumenter.track_loads(callback) { yield }
    end

    def query_success?
      @query.context.errors.empty? && !internal_error?
    end

    def internal_error?
      @query.context[:internal_errors] && @query.context[:internal_errors].any?
    end
  end
end
