# frozen_string_literal: true

module Platform
  module Connections
    class ProjectUser < Connections::Base
      edge_type Edges::ProjectUser, edge_class: Models::ProjectUserEdge

      total_count_field
    end
  end
end
