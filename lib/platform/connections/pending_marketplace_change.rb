# frozen_string_literal: true

module Platform
  module Connections
    class PendingMarketplaceChange < Connections::Base
      areas_of_responsibility :gitcoin

      total_count_field
    end
  end
end
