# frozen_string_literal: true

module Platform
  module Connections
    class EnterpriseOrganizationMembership < Connections::Base
      edge_type Edges::EnterpriseOrganizationMembership

      total_count_field
    end
  end
end
