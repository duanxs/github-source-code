# frozen_string_literal: true

module Platform
  module Connections
    class EnterpriseOutsideCollaborator < Connections::Base
      edge_type Edges::EnterpriseOutsideCollaborator

      total_count_field
    end
  end
end
