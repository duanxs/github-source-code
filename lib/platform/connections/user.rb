# frozen_string_literal: true

module Platform
  module Connections
    class User < Connections::Base
      description "A list of users."
      edge_type(Edges::User)
      total_count_field
    end
  end
end
