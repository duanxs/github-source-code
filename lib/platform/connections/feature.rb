# frozen_string_literal: true

module Platform
  module Connections
    class Feature < Connections::Base
      description "A list of feature flags."
      visibility :internal

      total_count_field
    end
  end
end
