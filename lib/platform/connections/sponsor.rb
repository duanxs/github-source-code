# frozen_string_literal: true

module Platform
  module Connections
    class Sponsor < Connections::Base
      visibility :internal
      description "A list of sponsors sponsoring a subject."

      edge_type(Edges::Sponsor)
      total_count_field

      field :total_monthly_pledged_in_dollars, Integer,
        description: "The total monthly amount (in dollars) pledged by all sponsors for the sponsored developer.",
        null: false

      def total_monthly_pledged_in_dollars
        @object.parent.total_monthly_pledged_in_dollars
      end
    end
  end
end
