# frozen_string_literal: true

module Platform
  module Connections
    class EnterpriseAdministrator < Connections::Base
      edge_type Edges::EnterpriseAdministrator

      total_count_field
    end
  end
end
