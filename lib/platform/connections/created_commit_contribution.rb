# frozen_string_literal: true

module Platform
  module Connections
    class CreatedCommitContribution < Connections::Base
      total_count_field description: <<~DESCRIPTION
        Identifies the total count of commits across days and repositories in the connection.
      DESCRIPTION

      def total_count
        contribs_by_repo = @object.parent
        commit_contribs = contribs_by_repo.contributions
        commit_contribs.sum(&:commit_count)
      end
    end
  end
end
