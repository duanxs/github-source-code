# frozen_string_literal: true
# rubocop:disable GitHub/EnsureConsistentConnections

module Platform
  module Connections
    class SearchResultItem < Connections::Base
      description "A list of results that matched against a search query."

      # Override this to use the `edges` method instead of `edge_node`,
      # since the connection wrapper customized those objects
      def edges
        object.edges
      end

      field :language_aggregates, [Objects::LanguageAggregate], visibility: :internal, description: "The programming languages represented in the search results.", null: false

      def language_aggregates
        @object.search_results.languages
      end

      field :issue_count, Integer, description: "The number of issues that matched the search query.", null: false

      def issue_count
        count_if_class(::Search::Queries::IssueQuery)
      end

      field :code_count, Integer, description: "The number of pieces of code that matched the search query.", null: false

      def code_count
        count_if_class(::Search::Queries::CodeQuery)
      end

      field :marketplace_count, Integer, null: false, visibility: :internal, description: "The number of GitHub Marketplace or Works with GitHub listings that matched."

      def marketplace_count
        count_if_class(::Search::Queries::MarketplaceQuery)
      end

      field :repository_count, Integer, description: "The number of repositories that matched the search query.", null: false

      def repository_count
        count_if_class(::Search::Queries::RepoQuery)
      end

      field :user_count, Integer, description: "The number of users that matched the search query.", null: false

      def user_count
        count_if_class(::Search::Queries::UserQuery)
      end

      field :wiki_count, Integer, description: "The number of wiki pages that matched the search query.", null: false

      def wiki_count
        count_if_class(::Search::Queries::WikiQuery)
      end

      private

      # Only return the count if this search is a `search_class` instance.
      # Otherwise return 0.
      def count_if_class(search_class)
        @object.nodes.is_a?(search_class) ? @object.total_count : 0
      end
    end
  end
end
