# frozen_string_literal: true

module Platform
  module Connections
    class ReactingUser < Connections::Base
      total_count_field

      # custom nodes implementation to support custom reacting user edge
      def nodes
        ::Promise.all(@object.edge_nodes.map { |node|
          Loaders::ActiveRecord.load(::User, node.user_id)
        })
      end
    end
  end
end
