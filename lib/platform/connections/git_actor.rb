# frozen_string_literal: true
module Platform
  module Connections
    class GitActor < Connections::Base
      feature_flag :pe_mobile
      total_count_field
    end
  end
end
