# frozen_string_literal: true
module Platform
  module Connections
    class SponsorsListing < Connections::Base
      visibility :internal
      total_count_field
    end
  end
end
