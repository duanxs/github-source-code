# frozen_string_literal: true

module Platform
  module Connections
    class Experiment < Connections::Base
      description "A list of code experiments."
      visibility :internal

      total_count_field
    end
  end
end
