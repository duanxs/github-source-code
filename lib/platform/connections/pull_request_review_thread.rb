# frozen_string_literal: true

module Platform
  module Connections
    class PullRequestReviewThread < Connections::Base
      description "Review comment threads for a pull request review."
      total_count_field

      field :filtered_count, Integer, visibility: :internal, description: "Identifies the count of items after applying `before` and `after` filters.", null: false

      field :page_count, Integer, visibility: :internal, description: "Identifies the count of items after applying `before`/`after` filters and `first`/`last`/`skip` slicing.", null: false
    end
  end
end
