# frozen_string_literal: true

module Platform
  module NotDocumentedBecause
    DEPRECATED   = nil
    EXPERIMENTAL = nil
    IGNORED      = nil
    INTERNAL     = nil
    UNRELEASED   = nil
    WE_FORGOT    = nil
    TO_FOLLOW    = nil
  end
end
