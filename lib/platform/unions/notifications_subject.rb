# frozen_string_literal: true

module Platform
  module Unions
    class NotificationsSubject < Platform::Unions::Base
      description "The notification's subject."
      feature_flag :pe_mobile

      possible_types(
        Objects::CheckSuite,
        Objects::Commit,
        Objects::Gist,
        Objects::TeamDiscussion,
        Objects::Issue,
        Objects::PullRequest,
        Objects::Release,
        Objects::RepositoryInvitation,
        Objects::RepositoryVulnerabilityAlert,
        Objects::RepositoryAdvisory,
        Objects::AdvisoryCredit,
        Objects::Discussion,
      )
    end
  end
end
