# frozen_string_literal: true

module Platform
  module Unions
    class IssueTimelineItems < Unions::Base
      description "An item in an issue timeline"

      possible_types(
        Objects::IssueComment,
        Objects::CrossReferencedEvent,
        Objects::ComposableComment,
        # All event types (alphabetical)
        Objects::AddedToProjectEvent,
        Objects::AssignedEvent,
        Objects::ClosedEvent,
        Objects::CommentDeletedEvent,
        Objects::ConnectedEvent,
        Objects::ConvertedNoteToIssueEvent,
        Objects::DemilestonedEvent,
        Objects::DisconnectedEvent,
        Objects::LabeledEvent,
        Objects::LockedEvent,
        Objects::MarkedAsDuplicateEvent,
        Objects::MentionedEvent,
        Objects::MilestonedEvent,
        Objects::MovedColumnsInProjectEvent,
        Objects::PinnedEvent,
        Objects::ReferencedEvent,
        Objects::RemovedFromProjectEvent,
        Objects::RenamedTitleEvent,
        Objects::ReopenedEvent,
        Objects::SubscribedEvent,
        Objects::TransferredEvent,
        Objects::UnassignedEvent,
        Objects::UnlabeledEvent,
        Objects::UnlockedEvent,
        Objects::UserBlockedEvent,
        Objects::UnmarkedAsDuplicateEvent,
        Objects::UnpinnedEvent,
        Objects::UnsubscribedEvent,
      )
    end
  end
end
