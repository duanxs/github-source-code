# rubocop:disable Style/FrozenStringLiteralComment

module Platform
  module Unions
    module AuditLog
      class AuditEntryActor < Platform::Unions::Base
        description "Types that can initiate an audit log event."

        areas_of_responsibility :audit_log

        possible_types(
          Objects::Bot,
          Objects::Organization,
          Objects::User,
        )
      end
    end
  end
end
