# frozen_string_literal: true

module Platform
  module Unions
    module AuditLog
      class OauthApplicationTransferAuditEntryAccount < Platform::Unions::Base
        areas_of_responsibility :audit_log, :platform
        description "Types that can own an OAuth Application."

        visibility :under_development

        possible_types(
          Objects::User,
          Objects::Organization,
        )
      end
    end
  end
end
