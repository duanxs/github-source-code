# frozen_string_literal: true

module Platform
  module Unions
    class FileType < Platform::Unions::Base
      description "TreeEntry file types."
      areas_of_responsibility :repositories

      feature_flag :pe_mobile

      possible_types Objects::ImageFileType, Objects::MarkdownFileType, Objects::PdfFileType, Objects::TextFileType
    end
  end
end
