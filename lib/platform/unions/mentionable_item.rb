# frozen_string_literal: true

module Platform
  module Unions
    class MentionableItem < Platform::Unions::Base
      description "An item that is mentionable on an issue/pull request"

      feature_flag :pe_mobile

      possible_types(
        Objects::User,
        Objects::Team,
      )
    end
  end
end
