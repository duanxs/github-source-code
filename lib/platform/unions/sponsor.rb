# rubocop:disable Style/FrozenStringLiteralComment

module Platform
  module Unions
    class Sponsor < Platform::Unions::Base
      description "Entites that can sponsor others via GitHub Sponsors"

      possible_types(
        Objects::User,
        Objects::Organization,
      )
    end
  end
end
