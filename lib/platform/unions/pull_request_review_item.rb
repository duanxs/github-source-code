# rubocop:disable Style/FrozenStringLiteralComment

module Platform
  module Unions
    class PullRequestReviewItem < Platform::Unions::Base
      description "An object contained in a pull request review."

      feature_flag :pe_mobile

      possible_types(
        Objects::PullRequestReviewThread,
        Objects::PullRequestReviewComment,
      )
    end
  end
end
