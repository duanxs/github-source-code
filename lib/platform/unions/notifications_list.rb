# frozen_string_literal: true

module Platform
  module Unions
    class NotificationsList < Platform::Unions::Base
      description "The parent object that the notification thread's subject belongs to."
      feature_flag :pe_mobile

      possible_types(
        Objects::Organization,
        Objects::Repository,
        Objects::Team,
        Objects::User,
      )
    end
  end
end
