# frozen_string_literal: true

module Platform
  module Errors
    class Type < ::TypeError; end
  end
end
