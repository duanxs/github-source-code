# frozen_string_literal: true

module Platform
  module Errors
    class Security < Errors::Internal; end
  end
end
