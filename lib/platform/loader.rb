# frozen_string_literal: true

module Platform
  class Loader < GraphQL::Batch::Loader
    def instrument
      Platform::LoaderTracker.track_loader(self) do
        yield
      end
    end

    def perform(keys)
      results = instrument { fetch(keys) }
      keys.each do |key|
        fulfill(key, results[key])
      end
    end

    def fetch(keys)
      raise Errors::NotImplementedError, "Implement this method in subclasses only."
    end

    def self.method_added(method_name)
      if method_name == :perform
        raise Errors::Internal, "Heya! You should define #fetch in subclasses of Platform::Loader instead of #perform"
      end
    end

    def dog_tags
      []
    end
  end
end
