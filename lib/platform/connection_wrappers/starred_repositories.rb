# frozen_string_literal: true

module Platform
  module ConnectionWrappers
    class StarredRepositories < GraphQL::Pagination::Connection
      class OrderedByIdCursor
        def self.decode(opaque)
          new *Platform::ConnectionWrappers::CursorGenerator.resolve_cursor(opaque)
        end

        def initialize(star_id)
          @star_id = star_id
        end

        attr_reader :star_id

        include Comparable

        def <=>(other)
          @star_id <=> other.star_id
        end

        def encode
          Platform::ConnectionWrappers::CursorGenerator.generate_cursor([@star_id], version: :v2)
        end
      end

      class OrderedByDateTimeCursor < OrderedByIdCursor
        def self.decode(opaque)
          values = Platform::ConnectionWrappers::CursorGenerator.resolve_cursor(opaque)

          new DateTime.iso8601(values[0].to_s), values[1]
        end

        def initialize(datetime, star_id)
          super star_id

          @datetime = datetime
        end

        attr_reader :datetime

        def <=>(other)
          result = @datetime <=> other.datetime

          if result.nil?
            @datetime.nil? ? -1 : 1
          elsif result.zero?
            super other
          else
            result
          end
        end

        def encode
          Platform::ConnectionWrappers::CursorGenerator.generate_cursor([@datetime&.iso8601, @star_id], version: :v2)
        end
      end

      class OrderedByWatcherCountCursor < OrderedByIdCursor
        def initialize(watcher_count, star_id)
          super star_id

          @watcher_count = watcher_count
        end

        attr_reader :watcher_count

        def <=>(other)
          result = @watcher_count <=> other.watcher_count
          return result unless result.zero?

          super other
        end

        def encode
          Platform::ConnectionWrappers::CursorGenerator.generate_cursor([@watcher_count, @star_id], version: :v2)
        end
      end

      LIMIT = 50000
      BATCH_SIZE = 10000

      def initialize(items, field:, owned_by_viewer:, order_by:, repository_database_ids:, **args)
        super items, max_page_size: Paginator.new(args, field, nil).max_per_page, **args

        @order_by = order_by || { field: "id", direction: "ASC" }
        @owned_by_viewer = owned_by_viewer
        @repository_database_ids = repository_database_ids

        @loaded = false
      end

      def over_limit?
        @items.stars.repositories.count > LIMIT
      end

      def nodes
        @nodes ||= if cursors_for_current_page.any?
          Platform::Loaders::ActiveRecord.load_all(::Star, cursors_for_current_page.map(&:star_id))
        else
          ::Promise.resolve([])
        end
      end

      def has_next_page
        load_data

        @has_next_page
      end

      def has_previous_page
        load_data

        @has_previous_page
      end

      def start_cursor
        cursors_for_current_page.first&.encode
      end

      def end_cursor
        cursors_for_current_page.last&.encode
      end

      def cursor_for(node)
        case @order_by[:field]
        when "pushed_at"
          Platform::ConnectionWrappers::CursorGenerator.generate_cursor([node.starrable.pushed_at, node.id], version: :v2)
        when "watcher_count"
          Platform::ConnectionWrappers::CursorGenerator.generate_cursor([node.starrable.stargazer_count, node.id], version: :v2)
        when "created_at"
          Platform::ConnectionWrappers::CursorGenerator.generate_cursor([node.created_at, node.id], version: :v2)
        else
          Platform::ConnectionWrappers::CursorGenerator.generate_cursor([node.id], version: :v2)
        end
      end

      def total_count
        cursors_for_given_order.size
      end

      private

      def cursors_for_given_order
        @cursors ||= case @order_by[:field]
        when "pushed_at"
          cursors_for_pushed_at_order
        when "watcher_count"
          cursors_for_watcher_count_order
        when "created_at"
          cursors_for_created_at_order
        else
          cursors_for_id_order
        end
      end

      def scope_for_repository_stars
        scope = @items.stars.repositories.order(created_at: :desc, id: :desc).limit(LIMIT)
        scope = scope.where("starrable_id IN (?)", @repository_database_ids) if @repository_database_ids.present?
        scope
      end

      def cursors_for_created_at_order
        return @cursors_for_created_at_order if defined?(@cursors_for_created_at_order)

        cursor_by_repository_id = {}
        scope_for_repository_stars.pluck(:starrable_id, :created_at, :id).each do |(starrable_id, created_at, id)|
          cursor_by_repository_id[starrable_id] = OrderedByDateTimeCursor.new(created_at, id)
        end

        cursors = []
        cursor_by_repository_id.keys.each_slice(BATCH_SIZE) do |repository_ids|
          # Keep the original order from the database
          (repository_ids & permissible_repository_scope(repository_ids).ids).each do |permissible_repository_id|
            cursors << cursor_by_repository_id[permissible_repository_id]
          end
        end

        cursors.reverse! if @order_by[:direction] == "ASC"

        @cursors_for_created_at_order = cursors
      end

      def cursors_for_watcher_count_order
        return @cursors_for_watcher_count_order if defined?(@cursors_for_watcher_count_order)

        star_id_by_repository_id = scope_for_repository_stars.pluck(:starrable_id, :id).to_h

        cursors = []
        star_id_by_repository_id.keys.each_slice(BATCH_SIZE) do |repository_ids|
          permissible_repository_scope(repository_ids).pluck(:watcher_count, :id).each do |watcher_count, repository_id|
            cursors << OrderedByWatcherCountCursor.new(watcher_count, star_id_by_repository_id[repository_id])
          end
        end

        cursors.sort!
        cursors.reverse! if @order_by[:direction] == "DESC"

        @cursors_for_watcher_count_order = cursors
      end

      def cursors_for_pushed_at_order
        return @cursors_for_pushed_at_order if defined?(@cursors_for_pushed_at_order)

        star_id_by_repository_id = scope_for_repository_stars.pluck(:starrable_id, :id).to_h

        cursors = []
        star_id_by_repository_id.keys.each_slice(BATCH_SIZE) do |repository_ids|
          permissible_repository_scope(repository_ids).pluck(:pushed_at, :id).each do |pushed_at, repository_id|
            cursors << OrderedByDateTimeCursor.new(pushed_at, star_id_by_repository_id[repository_id])
          end
        end

        cursors.sort!
        cursors.reverse! if @order_by[:direction] == "DESC"

        @cursors_for_pushed_at_order = cursors
      end

      def cursors_for_id_order
        return @cursors_for_id_order if defined?(@cursors_for_id_order)

        cursor_by_repository_id = {}
        scope_for_repository_stars.pluck(:starrable_id, :id).each do |(starrable_id, id)|
          cursor_by_repository_id[starrable_id] = OrderedByIdCursor.new(id)
        end

        cursors = []
        cursor_by_repository_id.keys.each_slice(BATCH_SIZE) do |repository_ids|
          permissible_repository_scope(repository_ids).ids.each do |permissible_repository_id|
            cursors << cursor_by_repository_id[permissible_repository_id]
          end
        end

        cursors.sort!
        cursors.reverse! if @order_by[:direction] == "DESC"

        @cursors_for_id_order = cursors
      end

      def permissible_repository_scope(repository_ids)
        permissible_repository_scope = context[:permission].filtered_permissible_repository_scope(@items, repository_ids, resource: "metadata")
        permissible_repository_scope = permissible_repository_scope.active.filter_spam_and_disabled_for(context[:viewer])

        case @owned_by_viewer
        when true
          permissible_repository_scope = permissible_repository_scope.owned_by(context[:viewer])
        when false
          permissible_repository_scope = permissible_repository_scope.not_owned_by(context[:viewer])
        end

        unless @owned_by_viewer == true
          # If there's no condition on `owner_id` (via the `#owned_by` call above),
          # we should force MySQL to use the primary key index. Any other index is going to cause this
          # query to run for a very long time and time out.
          permissible_repository_scope = permissible_repository_scope.from("`repositories` FORCE INDEX (PRIMARY)")
        end

        if context[:unauthorized_organization_ids].present?
          permissible_repository_scope = permissible_repository_scope.where("repositories.owner_id NOT IN (?)", context[:unauthorized_organization_ids])
        end

        permissible_repository_scope
      end

      def cursors_type
        case @order_by[:field]
        when "pushed_at", "created_at"
          OrderedByDateTimeCursor
        when "watcher_count"
          OrderedByWatcherCountCursor
        else
          OrderedByIdCursor
        end
      end

      def cursors_for_current_page
        load_data

        @cursors_for_current_page
      end

      def load_data
        return if @loaded
        @loaded = true

        @has_next_page = @has_previous_page = false

        @cursors_for_current_page = cursors_for_given_order

        if before
          cursor = cursors_type.decode(before)

          cursor_offset = if @order_by[:direction] == "ASC"
            @cursors_for_current_page.bsearch_index { |c| c >= cursor }
          else
            @cursors_for_current_page.bsearch_index { |c| c <= cursor }
          end

          if cursor_offset
            @has_next_page = cursor_offset < @cursors_for_current_page.length
            @cursors_for_current_page = @cursors_for_current_page[0...cursor_offset]
          end
        end

        if after
          cursor = cursors_type.decode(after)

          cursor_offset = if @order_by[:direction] == "ASC"
            @cursors_for_current_page.bsearch_index { |c| c > cursor }
          else
            @cursors_for_current_page.bsearch_index { |c| c < cursor }
          end

          if cursor_offset
            @has_previous_page = cursor_offset > 0
            @cursors_for_current_page = @cursors_for_current_page[cursor_offset..-1]
          else
            @cursors_for_current_page = []
          end
        end

        if first && @cursors_for_current_page.size > first
          @cursors_for_current_page = @cursors_for_current_page.first(first)
          @has_next_page = true
        end

        if last && @cursors_for_current_page.size > last
          @cursors_for_current_page = @cursors_for_current_page.last(last)
          @has_previous_page = true
        end
      end
    end
  end
end
