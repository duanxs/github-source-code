# frozen_string_literal: true

module Platform
  module ConnectionWrappers
    class ProjectCards < GraphQL::Relay::BaseConnection
      def initialize(helper, arguments, field: nil, max_page_size: nil, parent: nil, context: nil)
        @helper = helper
        @field = field
        @max_page_size = max_page_size
        @parent = parent
      end

      def connection
        @connection ||= @helper.connection(
          field: @field,
          max_page_size: @max_page_size,
          parent: @parent,
        )
      end

      def cursor_from_node(item)
        connection.then { |connection| connection.cursor_from_node(item) }
      end

      def edge_nodes
        connection.then(&:edge_nodes)
      end

      def page_info
        connection.then(&:page_info)
      end

      def paged_nodes
        connection.then(&:paged_nodes)
      end

      def sliced_nodes
        connection.then(&:sliced_nodes)
      end

      def total_count
        @total_count ||= @helper.total_count
      end
    end
  end
end
