# frozen_string_literal: true

module Platform
  module ConnectionWrappers
    class ManifestDependenciesConnection < GraphQL::Relay::BaseConnection
      attr_accessor :parent
      def initialize(dependencies, arguments, page_info)
        @dependencies = dependencies
        @page_info = PageInfo.new(**page_info)

        super(dependencies, arguments)
      end

      def edge_nodes
        @dependencies
      end

      def page_info
        @page_info
      end

      def cursor_from_node(node)
        node.cursor
      end
    end
  end
end
