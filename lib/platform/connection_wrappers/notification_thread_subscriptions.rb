# frozen_string_literal: true

module Platform
  module ConnectionWrappers
    class NotificationThreadSubscriptions < GraphQL::Relay::BaseConnection

      def cursor_from_node(object)
        CursorGenerator.generate_cursor(object.id)
      end

      def start_cursor
        edge_nodes.first ? cursor_from_node(edge_nodes.first) : nil
      end

      def end_cursor
        edge_nodes.last ? cursor_from_node(edge_nodes.last) : nil
      end

      def has_next_page
        last ? page_object.has_previous_page : page_object.has_next_page
      end

      def has_previous_page
        last ? page_object.has_next_page : page_object.has_previous_page
      end

      def edge_nodes
        @edge_nodes ||= begin
          nodes = page_object.page
          nodes = nodes.reverse if last
          nodes
        end
      end

      def total_count
        helper.total_count
      end

      private

      def page_object
        @page_object ||= helper.fetch!(cursor: id_cursor, limit: limit, direction: query_direction)
      end

      # A semantic alias for `nodes`, which refers to the object returned by a resolver for this
      # connection (in this case, a `NotificationThreadSubscriptionQuery` object).
      #
      # Returns a NotificationThreadSubscriptionQuery.
      def helper
        nodes
      end

      # Returns Integer ID encoded within the string cursor argument.
      # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
      def id_cursor
        @id_cursor ||= (cursor = after || before) && CursorGenerator.resolve_cursor(cursor).to_i
      end
      # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

      def limit
        first || last
      end

      def query_direction
        @query_direction = last ? invert_direction(helper.direction) : helper.direction
      end

      def invert_direction(direction)
        direction == :desc ? :asc : :desc
      end
    end
  end
end
