# frozen_string_literal: true

module Platform
  module ConnectionWrappers
    class Promise
      class MapWrapper
        attr_reader :promise

        def initialize(promise)
          @promise = promise
        end

        def map(&bk)
          @promise.then { |xs| xs.map(&bk) }
        end

        def each(&bk)
          @promise.then { |xs| xs.each(&bk) }
        end
      end

      attr_reader :parent, :promise

      def initialize(promise, *arguments)
        @parent = arguments.last.try(:[], :parent)
        @promise = promise.then { |connection|
          GraphQL::Relay::BaseConnection.connection_for_nodes(connection).new(connection, *arguments)
        }
      end

      def cursor_from_node(node)
        @promise.then { |connection| connection.cursor_from_node(node) }
      end

      def edge_nodes
        MapWrapper.new(@promise.then(&:edge_nodes))
      end

      def page_info
        @promise.then(&:page_info)
      end

      def total_count
        @promise.then do |connection|
          case connection
          when Platform::ConnectionWrappers::RemoteRelation
            connection.count
          when Platform::ConnectionWrappers::Relation
            connection.relation.count
          else
            connection.nodes.count
          end
        end
      end
    end
  end
end
