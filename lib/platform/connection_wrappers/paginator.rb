# frozen_string_literal: true

module Platform
  module ConnectionWrappers
    class Paginator
      DEFAULT_MAX_PER_PAGE = 100
      ABSOLUTE_MAX_PER_PAGE = 250

      attr_reader :max_per_page, :first, :last

      def initialize(arguments, field, max_page_size)
        @arguments = arguments.to_h
        @field = field
        @max_per_page = calculate_per_page_value(max_page_size.to_i)
        validate_arguments!
      end

      private

      # Internal: Calculates the per_page value for the connection. If it exceeds
      # GitHub's upper default, `Platform::Errors::MaxPageSizeExceeded` is thrown.
      # Otherwise, it caps to the `DEFAULT_MAX_PER_PAGE` if `max_page_size` is `nil`.
      #
      # Returns an integer.
      def calculate_per_page_value(page_size)
        raise Platform::Errors::MaxPageSizeExceeded.new(page_size) if page_size > ABSOLUTE_MAX_PER_PAGE
        page_size = DEFAULT_MAX_PER_PAGE if page_size <= 0
        page_size
      end

      # Internal: Checks to see if the user requested a valid `first` or `last`
      # value. That is:
      # * The Relay Cursor Connections specification strongly discourages the
      #   use of both first and last.
      #   https://facebook.github.io/relay/graphql/connections.htm#sec-Pagination-algorithm
      #
      # * The Relay Cursor Connections specification does not allow negative cursors.
      #   https://facebook.github.io/relay/graphql/connections.htm#sec-Pagination-algorithm
      #
      # * `first` or `last` cannot exceed this connection's max_per_page value.
      #
      # * At this time, we don't support `before` and `after`
      #
      # Returns nothing.
      def validate_arguments!
        if @arguments[:first] && @arguments[:last]
          raise Platform::Errors::DuplicateFirstLastPaginationBoundaries.new(@field)
        end

        if @arguments[:first]
          @first = @arguments[:first].to_i
          if first > @max_per_page
            raise Platform::Errors::ExcessivePagination.new(@field, :first, first, @max_per_page)
          elsif first < 0
            raise Platform::Errors::InvalidPagination.new(@field, :first)
          end
        end

        if @arguments[:last]
          @last = @arguments[:last].to_i
          if last > @max_per_page
            raise Platform::Errors::ExcessivePagination.new(@field, :last, last, @max_per_page)
          elsif last < 0
            raise Platform::Errors::InvalidPagination.new(@field, :last)
          end
        end

        if !@arguments[:first] && !@arguments[:last]
          @first = @max_per_page
        end

        if @arguments[:after] && @arguments[:numeric_page]
          raise Platform::Errors::ConflictingPaginationArguments.new(@field)
        end
      end
    end
  end
end
