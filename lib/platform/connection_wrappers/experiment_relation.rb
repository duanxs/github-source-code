# frozen_string_literal: true

module Platform
  module ConnectionWrappers
    class ExperimentRelation
      attr_reader :wrapper, :parent, :arguments, :max_page_size, :field

      def initialize(wrapper, arguments, field: nil, max_page_size: nil, parent: nil, context: nil)
        @wrapper = wrapper
        @arguments = arguments
        @field = field
        @max_page_size = max_page_size
        @parent = parent
        @context = context
      end

      # Public: Generate a cursor from an ActiveRecord object
      #
      # item - An ActiveRecord::Base instance (or Promise that resolves to one)
      #
      # Returns a Promise that resolves to a String cursor value
      def cursor_from_node(item)
        control_connection.cursor_from_node
      end

      def edge_nodes
        return @edge_nodes if defined?(@edge_nodes)

        Scientist.run "#{wrapper.name}-results" do |e|
          e.use { control_connection.send(:results) }
          e.try { candidate_connection.send(:results) }
        end

        @edge_nodes = control_connection.edge_nodes
      end

      def page_info
        control_connection.page_info
      end

      # Zero out the expected results. Used in situations where the API AuthZ
      # check failed, and the resolvers are still expecting data to come through.
      # They should receive nothing, though.
      def none
        @edge_nodes = ::Promise.all([])
        self
      end

      def has_next_page
        control_connection.has_next_page
      end

      def has_previous_page
        control_connection.has_previous_page
      end

      def total_count
        control_connection.total_count
      end

    private

      def control_connection
        @control_connection if defined?(@control_connection)
        @control_connection = Relation.new(@wrapper.control.call, @arguments, field: @field, parent: @parent, context: @context, max_page_size: @max_page_size)
      end

      def candidate_connection
        @candidate_connection if defined?(@candidate_connection)
        @candidate_connection = Relation.new(@wrapper.candidate.call, @arguments, field: @field, parent: @parent, context: @context, max_page_size: @max_page_size)
      end
    end
  end
end
