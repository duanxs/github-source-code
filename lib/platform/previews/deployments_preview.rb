# frozen_string_literal: true

class Platform::Previews::DeploymentsPreview < Platform::Preview
  title "Deployments"

  description "This preview adds support for deployments mutations and new deployments features."

  owning_teams "@github/ecosystem-api"

  toggled_by :"flash-preview"

  toggled_on [
    "DeploymentStatus.environment",
    "Mutation.createDeploymentStatus",
    "CreateDeploymentStatusInput",
    "CreateDeploymentStatusPayload",
    "Mutation.createDeployment",
    "CreateDeploymentInput",
    "CreateDeploymentPayload",
  ]
end
