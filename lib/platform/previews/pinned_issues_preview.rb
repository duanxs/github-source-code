# frozen_string_literal: true

class Platform::Previews::PinnedIssuesPreview < Platform::Preview
  title "Pinned Issues Preview"

  description "This preview adds support for pinned issues."

  owning_teams "@github/pe-pull-requests"

  toggled_by :"elektra-preview"

  toggled_on [
    "Repository.pinnedIssues",
    "PinnedIssue",
    "PinnedIssueEdge",
    "PinnedIssueConnection",
    "Mutation.pinIssue",
    "Mutation.unpinIssue",
  ]
end
