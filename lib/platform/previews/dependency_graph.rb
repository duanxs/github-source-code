# frozen_string_literal: true

class Platform::Previews::DependencyGraph < Platform::Preview
  title "Access to a Repositories Dependency Graph"

  description "This preview adds support for reading a dependency graph for a repository."

  owning_teams "@github/dsp-dependency-graph"

  dotcom_only_preview

  toggled_by :"hawkgirl-preview"

  toggled_on [
    "DependencyGraphManifest",
    "Repository.dependencyGraphManifests",
    "DependencyGraphManifestEdge",
    "DependencyGraphManifestConnection",
    "DependencyGraphDependency",
    "DependencyGraphDependencyEdge",
    "DependencyGraphDependencyConnection",
    "DependencyGraphPackageRelease.dependencies",
  ]
end
