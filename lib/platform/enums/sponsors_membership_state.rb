# frozen_string_literal: true

module Platform
  module Enums
    class SponsorsMembershipState < Platform::Enums::Base
      description "The state of a Sponsors application."
      visibility :internal

      value "SUBMITTED", "The application has been submitted and is awaiting approval", value: :submitted
      value "ACCEPTED", "The application has been accepted", value: :accepted
      value "PENDING", "The application is pending review", value: :submitted
      value "DENIED", "The application is denied", value: :denied
      value "APPEALED", "The application decision is being appealed", value: :appealed
      value "BANNED", "The sponsorable has been banned from the program", value: :banned
    end
  end
end
