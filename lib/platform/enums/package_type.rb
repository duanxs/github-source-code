# frozen_string_literal: true

module Platform
  module Enums
    class PackageType < Platform::Enums::Base
      description "The possible types of a package."

      value "NPM", "An npm package.", value: "npm"
      value "RUBYGEMS", "A rubygems package.", value: "rubygems"
      value "MAVEN", "A maven package.", value: "maven"
      value "DOCKER", "A docker image.", value: "docker"
      value "DEBIAN", "A debian package.", value: "debian"
      value "NUGET", "A nuget package.", value: "nuget"
      value "PYPI", "A python package.", value: "python"
    end
  end
end
