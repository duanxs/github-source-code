# frozen_string_literal: true

module Platform
  module Enums
    class RepositoryInvitationOrderField < Platform::Enums::Base
      description "Properties by which repository invitation connections can be ordered."

      value "CREATED_AT", "Order repository invitations by creation time"

      value "INVITEE_LOGIN", "Order repository invitations by invitee login",
        deprecated: {
          start_date: Date.new(2020, 5, 18),
          reason: "`INVITEE_LOGIN` is no longer a valid field value. Repository invitations can now be associated with an email, not only an invitee.",
          superseded_by: nil,
          owner: "jdennes",
        }
    end
  end
end
