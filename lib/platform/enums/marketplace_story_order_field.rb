# frozen_string_literal: true

module Platform
  module Enums
    class MarketplaceStoryOrderField < Platform::Enums::Base
      areas_of_responsibility :marketplace
      description "Properties by which story connections can be ordered."
      visibility :internal

      value "CREATED_AT", "Order stories by creation time", value: "created_at"
      value "UPDATED_AT", "Order stories by update time", value: "updated_at"
      value "PUBLISHED_AT", "Order stories by publish time", value: "published_at"
    end
  end
end
