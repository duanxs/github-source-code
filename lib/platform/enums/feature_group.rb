# frozen_string_literal: true

module Platform
  module Enums
    class FeatureGroup < Platform::Enums::Base
      description "The possible feature flag groups."
      visibility :internal

      value "PREVIEW_FEATURES", "The preview features group", value: :preview_features
      value "BOUNTY_HUNTER_TARGET", "The bounty hunter target group", value: :bounty_hunter_target
      value "MAINTAINERS_EARLY_ACCESS", "The maintainers early access group", value: :maintainers_early_access
      value "OPTED_IN", "Feature group for user self-enrollment in beta features", value: :opted_in
      value "INTEGRATORS_EARLY_ACCESS", "The integrators early access group", value: :integrators_early_access
      value "EARLY_ACCESS_ENABLED", "Feature group for EarlyAccessMembership beta features", value: :early_access_enabled
    end
  end
end
