# frozen_string_literal: true

module Platform
  module Enums
    class MobileEventContext < Platform::Enums::Base
      feature_flag :pe_mobile
      areas_of_responsibility :mobile
      visibility :public, environments: [:dotcom]
      visibility :internal, environments: [:enterprise]
      description "Represents the different extra mobile context."

      value "SAVE", "The notification was marked as saved.", value: "save"
      value "UNSAVE", "The notification was marked as saved.", value: "unsave"
      value "DONE", "The notification was marked as done.", value: "done"
      value "UNDONE", "The notification was marked as undone.", value: "undone"
      value "READ", "The notification was marked as read.", value: "read"
      value "UNREAD", "The notification was marked as unread.", value: "unread"
      value "UNSUBSCRIBE", "The notification was unsubscribed.", value: "unsubscribe"
    end
  end
end
