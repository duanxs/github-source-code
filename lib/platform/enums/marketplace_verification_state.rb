# frozen_string_literal: true

module Platform
  module Enums
    class MarketplaceVerificationState < Platform::Enums::Base
      description "The possible verifications state of a Marketplace listing."
      visibility :internal
      areas_of_responsibility :marketplace

      value "UNVERIFIED", "The listing has been approved as unverified for display in the GitHub Marketplace.", value: "unverified"
      value "VERIFIED", "The listing has been approved as verified for display in the GitHub Marketplace.", value: "verified"
    end
  end
end
