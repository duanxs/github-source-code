# frozen_string_literal: true

module Platform
  module Enums
    class SponsorsCriterionType < Platform::Enums::Base
      description "The input type of a criterion used when evaluating Sponsors applications."
      visibility :internal

      value "CHECKBOX", "The criterion is marked as met by a checkbox", value: "checkbox"
      value "TEXT", "The criterion is marked as met by a text value", value: "text"
    end
  end
end
