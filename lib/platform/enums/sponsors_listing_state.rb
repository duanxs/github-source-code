# frozen_string_literal: true

module Platform
  module Enums
    class SponsorsListingState < Platform::Enums::Base
      description "The state of a Sponsors listing."
      visibility :internal

      value "DRAFT",
        description: "The listing is still being worked on.",
        value:       ::SponsorsListing.state_value(:draft)

      value "PENDING_VERIFICATION",
        description: "The listing is pending verification.",
        value:       ::SponsorsListing.state_value(:pending_verification)

      value "VERIFIED",
        description: "The listing has been verified.",
        value:       ::SponsorsListing.state_value(:verified)

      value "UNVERIFIED_AND_PENDING_APPROVAL",
        description: "The listing hasn't been verified yet and approval was requested.",
        value:       ::SponsorsListing.state_value(:unverified_and_pending_approval)

      value "VERIFIED_AND_PENDING_APPROVAL",
        description: "The listing has been verified and approval was requested.",
        value:       ::SponsorsListing.state_value(:verified_and_pending_approval)

      value "FAILED_VERIFICATION",
        description: "The listing failed verification.",
        value:       ::SponsorsListing.state_value(:failed_verification)

      value "APPROVED",
        description: "The listing has been approved and published.",
        value:       ::SponsorsListing.state_value(:approved)

      value "DISABLED",
        description: "The listing has been disabled.",
        value:       ::SponsorsListing.state_value(:disabled)
    end
  end
end
