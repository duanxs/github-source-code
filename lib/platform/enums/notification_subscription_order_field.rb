# frozen_string_literal: true

module Platform
  module Enums
    class NotificationSubscriptionOrderField < Platform::Enums::Base
      areas_of_responsibility :notifications
      visibility :under_development
      description "Properties by which notification subscription connections can be ordered."

      value "ID", "Allows ordering a list of notification subscriptions by when they were created.", value: "id"
    end
  end
end
