# frozen_string_literal: true

module Platform
  module Enums
    class RepositoryTopicState < Platform::Enums::Base
      description "Possible association types that can exist between a repository and a topic."
      visibility :internal

      value "CREATED", "A topic that was manually added by a user to a repository.", value: "created"
      value "SUGGESTED", "A topic that was suggested via the Munger service and accepted and applied by a user to a repository.", value: "suggested"
      value "DECLINED_NOT_RELEVANT", "A topic that was suggested via the Munger service and declined by the user as not being relevant to the repository.", value: "declined_not_relevant"
      value "DECLINED_TOO_SPECIFIC", "A topic that was suggested via the Munger service and declined by the user due to it being too specific (e.g. #ruby-on-rails-version-4-2-1).", value: "declined_too_specific"
      value "DECLINED_PERSONAL_PREFERENCE", "A topic that was suggested via the Munger service and declined by the user due to personal preference.", value: "declined_personal_preference"
      value "DECLINED_TOO_GENERAL", "A topic that was suggested via the Munger service and declined by the user due to it being too general.", value: "declined_too_general"
    end
  end
end
