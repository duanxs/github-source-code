# frozen_string_literal: true

module Platform
  module Enums
    class MarketplaceAgreementSignatoryType < Platform::Enums::Base
      description "Who is the intended audience for a Marketplace legal agreement?"
      visibility :internal
      areas_of_responsibility :marketplace

      value "INTEGRATOR",
        "The agreement is intended for those listing their product in the Marketplace.",
        value: :integrator
      value "END_USER", "The agreement is intended for GitHub users who would like to " +
                        "install a product listed in the Marketplace.", value: :end_user
    end
  end
end
