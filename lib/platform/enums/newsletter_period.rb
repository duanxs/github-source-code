# frozen_string_literal: true

module Platform
  module Enums
    class NewsletterPeriod < Platform::Enums::Base
      description "Represents how often we send a newsletter to a user."

      visibility :internal

      value "ONE_OFF", "Send the newsletter once.", value: "one_off"
      value "DAILY", "Send the newsletter daily.", value: "daily"
      value "WEEKLY", "Send the newsletter weekly.", value: "weekly"
      value "MONTHLY", "Send the newsletter monthly.", value: "monthly"
      value "FORTNIGHTLY", "Send the newsletter fortnightly.", value: "fortnightly"
    end
  end
end
