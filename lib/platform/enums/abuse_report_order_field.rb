# frozen_string_literal: true

module Platform
  module Enums
    class AbuseReportOrderField < Platform::Enums::Base
      areas_of_responsibility :community_and_safety

      description "Properties by which abuse report connections can be ordered."
      visibility :internal

      value "CREATED_AT", "Order abuse reports by when they were created.", value: "created_at"
    end
  end
end
