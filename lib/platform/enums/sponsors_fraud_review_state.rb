# frozen_string_literal: true

module Platform
  module Enums
    class SponsorsFraudReviewState < Platform::Enums::Base
      description "The state of a Sponsors fraud review."
      visibility :internal

      value "PENDING", "The fraud review is pending manual action", value: :pending
      value "RESOLVED", "The fraud review has been resolved", value: :resolved
      value "FLAGGED", "The fraud review has been flagged as fraudulent", value: :flagged
    end
  end
end
