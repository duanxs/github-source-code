# frozen_string_literal: true

module Platform
  module Enums
    class NotificationReason < Platform::Enums::Base
      description "The reason you received a notification about a subject."
      feature_flag :pe_mobile

      value "ASSIGN", "You were assigned to the Issue/PR.", value: "assign"
      value "AUTHOR", "You created the thread.", value: "author"
      value "COMMENT", "You commented on the thread.", value: "comment"
      value "INVITATION", "You accepted an invitation to contribute to the repository.", value: "invitation"
      value "MANUAL", "You subscribed to the thread (via an Issue or Pull Request).", value: "manual"
      value "MENTION", "You were specifically @mentioned in the content.", value: "mention"
      value "REVIEW_REQUESTED", "You were requested for review.", value: "review_requested"
      value "SECURITY_ADVISORY_CREDIT", "You were given credit for contributing to a Security Advisory.", value: "security_advisory_credit"
      value "SECURITY_ALERT", "You have access to the notification subject's Dependabot alerts.", value: "security_alert"
      value "STATE_CHANGE", "You changed the thread state (for example, closing an Issue or merging a Pull Request).", value: "state_change"
      value "SUBSCRIBED", "You are watching the subject of the notification.", value: "subscribed"
      value "TEAM_MENTION", "You were on a team that was mentioned.", value: "team_mention"
      value "CI_ACTIVITY", "You are subscribed to continuous integration activity.", value: "ci_activity"
      value "SAVED", "You saved this notification", value: "saved", visibility: :internal
      value "READY_FOR_REVIEW", "A pull request you're subscribed to was marked ready for review.", value: "ready_for_review", visibility: :internal
    end
  end
end
