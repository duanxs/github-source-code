# frozen_string_literal: true

module Platform
  module Enums
    class SponsorsMembershipOrderField < Platform::Enums::Base
      description "Properties by which Sponsors membership connections can be ordered."
      visibility :internal

      value "CREATED_AT", "Order Sponsors memberships by creation time.", value: "created_at"
    end
  end
end
