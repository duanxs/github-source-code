# frozen_string_literal: true

module Platform
  module Enums
    class FeatureActorType < Platform::Enums::Base
      description "The possible feature flag actor types."
      visibility :internal

      value "USER", "User actors."
      value "REPOSITORY", "Repository actors."
      value "OAUTH_APPLICATION", "OauthApplication actors"
      value "APP", "GitHub app actors"
      value "HOST", "Host actors"
      value "ROLE", "Role actors"
      value "FLIPPER_ID", "Flipper ID actors"
      value "ENTERPRISE", "Enterprise actors"
    end
  end
end
