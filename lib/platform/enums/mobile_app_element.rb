# frozen_string_literal: true

module Platform
  module Enums
    class MobileAppElement < Platform::Enums::Base
      feature_flag :pe_mobile
      areas_of_responsibility :mobile
      visibility :public, environments: [:dotcom]
      visibility :internal, environments: [:enterprise]
      description "Represents the different mobile elements."

      # Explore
      value "EXPLORE_TRENDING_REPOSITORY", "A trending repository on explore", value: "explore_trending_repository"
      value "EXPLORE_FOR_YOU_REPOSITORY", "A for you repository on explore", value: "explore_for_you_repository"
      value "EXPLORE_FEATURED_REPOSITORY", "A featured repository on explore", value: "explore_featured_repository"
      value "EXPLORE_STAR_TRENDING_REPOSITORY", "A trending repository star button on explore", value: "explore_star_trending_repository"
      value "EXPLORE_STAR_FOR_YOU_REPOSITORY", "A for you repository star button on explore", value: "explore_star_for_you_repository"
      value "EXPLORE_STAR_FEATURED_REPOSITORY", "A featured repository star button on explore", value: "explore_star_featured_repository"

      # Home
      value "HOME_ISSUES", "Global list of issues for a user on home", value: "home_issues"
      value "HOME_PULL_REQUESTS", "Global list of pull requests for a user on home", value: "home_pull_requests"
      value "HOME_REPOSITORIES", "Global list of repositories for a user on home", value: "home_repositories"
      value "HOME_ORGANIZATIONS", "Global list of organizations for a user on home", value: "home_organizations"
      value "HOME_CREATE_ISSUE", "Create issue button on home", value: "home_create_issue"

      # Notifications
      value "NOTIFICATION_LIST_ITEM", "A list item in the notification list.", value: "notification_list_item"
      value "NOTIFICATION_LIST_ITEM_UNDO", "Undo button when a notification is swiped.", value: "notification_list_item_undo"
      value "NOTIFICATION_FILTER", "Filter button on notification screen.", value: "notification_filter"
      value "NOTIFICATION_BOTTOM_NAVIGATION", "Notification button in the bottom navigation.", value: "notification_bottom_navigation"
    end
  end
end
