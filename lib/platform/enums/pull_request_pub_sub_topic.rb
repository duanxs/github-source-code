# frozen_string_literal: true

module Platform
  module Enums
    class PullRequestPubSubTopic < Platform::Enums::Base
      description "The possible PubSub channels for a pull request."

      value "UPDATED", "The channel ID for observing pull request updates.", value: "updated"
      value "HEAD_REF", "The channel ID for observing head ref updates.", value: "head_ref"
      value "TIMELINE", "The channel ID for updating items on the pull request timeline.", value: "timeline"
      value "STATE", "The channel ID for observing pull request state updates.", value: "state"
    end
  end
end
