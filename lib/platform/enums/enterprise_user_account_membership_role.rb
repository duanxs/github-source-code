  # frozen_string_literal: true

module Platform
  module Enums
    class EnterpriseUserAccountMembershipRole < Platform::Enums::Base
      description "The possible roles for enterprise membership."

      value "MEMBER", "The user is a member of the enterprise membership.", value: "member"
      value "OWNER", "The user is an owner of the enterprise membership.", value: "owner"
    end
  end
end
