# frozen_string_literal: true

module Platform
  module Enums
    class MarketplaceListingPlanSubscriberAccountTypes < Platform::Enums::Base
      description "The possible types of accounts that could be allowed to subscribe to a Marketplace listing plan."
      visibility :internal
      areas_of_responsibility :marketplace

      value "USERS_AND_ORGANIZATIONS", "Personal and organization accounts can subscribe to the plan.", value: "users_and_organizations"
      value "USERS_ONLY", "Only personal accounts can subscribe to the plan.", value: "users_only"
      value "ORGANIZATIONS_ONLY", "Only organization accounts can subscribe to the plan.", value: "organizations_only"
    end
  end
end
