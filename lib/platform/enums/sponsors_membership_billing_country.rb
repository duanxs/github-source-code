# frozen_string_literal: true

module Platform
  module Enums
    class SponsorsMembershipBillingCountry < Platform::Enums::Base
      description "The billing country of a Sponsors membership."
      visibility :internal

      value "SUPPORTED", "All of the countries supported by GitHub Sponsors",
        value: ::Billing::StripeConnect::Account::SUPPORTED_COUNTRIES

      # CountryNames returns an array for each country
      # The `second` value is the **alpha2** value used by SponsorsMemberships
      all_countries = ::Braintree::Address::CountryNames.map(&:second)
      value "UNSUPPORTED", "All of the countries not yet supported by GitHub Sponsors",
        value: all_countries - ::Billing::StripeConnect::Account::SUPPORTED_COUNTRIES

      ::Braintree::Address::CountryNames.map do |country_name, alpha2, _, _|
        value alpha2.upcase, country_name, value: alpha2
      end
    end
  end
end
