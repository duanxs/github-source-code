# frozen_string_literal: true

module Platform
  module Enums
    class FeatureUsageState < Platform::Enums::Base
      description "The possible feature flag usage states."
      visibility :internal

      value "UNUSED", "This feature doesn’t seem to be used anywhere in the code. If no one is planning to use it in the future, it should be deleted", value: :unused
      value "SHIPPED", "This feature seems to be fully shipped. This feature flag should be cleaned up.", value: :shipped
      value "DISABLED", "This feature seems to be disabled. This feature flag should be cleaned up.", value: :disabled
      value "USED", "This feature is in active use.", value: :used
    end
  end
end
