# frozen_string_literal: true

module Platform
  module Enums
    class BrowserIncrementKey < Platform::Enums::Base
      description "Mapping of enums to internal DataDog counters."
      visibility :internal

      value "ACTIONS_COMPLETED_LOG_VIEWS_SUCCEED", "Checks UI successfully loaded logs", value: "browser.events.actions.completed_log_views.succeed"
      value "ACTIONS_COMPLETED_LOG_VIEWS_FAIL", "Checks UI failed to load logs", value: "browser.events.actions.completed_log_views.fail"
      value "ACTIONS_STREAMING_LOG_VIEWS_SUCCEED", "Checks UI successfully loaded logs", value: "browser.events.actions.streaming_log_views.succeed"
      value "ACTIONS_STREAMING_LOG_VIEWS_FAIL", "Checks UI failed to load logs", value: "browser.events.actions.streaming_log_views.fail"
    end
  end
end
