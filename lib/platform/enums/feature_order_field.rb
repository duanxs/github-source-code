# frozen_string_literal: true

module Platform
  module Enums
    class FeatureOrderField < Platform::Enums::Base
      description "Properties by which Flipper features can be ordered."
      areas_of_responsibility :"feature-flags"
      visibility :internal

      value "CREATED_AT", "Order features by creation time", value: "created_at"
      value "UPDATED_AT", "Order features by update time", value: "updated_at"
      value "NAME", "Order features by name", value: "name"
    end
  end
end
