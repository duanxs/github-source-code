# frozen_string_literal: true

module Platform
  module Enums
    class ActionExecutionCapabilitySetting < Platform::Enums::Base
      description "The possible capabilities for action executions setting."
      areas_of_responsibility :actions

      value "DISABLED", "All action executions are disabled.", value: Configurable::ActionExecutionCapabilities::DISABLED
      value "ALL_ACTIONS", "All action executions are enabled.", value: Configurable::ActionExecutionCapabilities::ALL_ACTIONS
      value "LOCAL_ACTIONS_ONLY", "Only actions defined within the repo are allowed.", value: Configurable::ActionExecutionCapabilities::LOCAL_ACTIONS
      value "NO_POLICY", "Organization administrators action execution capabilities.", value: Configurable::ActionExecutionCapabilities::NO_POLICY
    end
  end
end
