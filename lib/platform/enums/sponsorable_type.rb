# frozen_string_literal: true

module Platform
  module Enums
    class SponsorableType < Platform::Enums::Base
      description "The sponsorable type of a Sponsors membership."
      visibility :internal

      value "USER", "A membership for a user", value: :User
      value "ORGANIZATION", "A membership for an organization", value: :Organization
    end
  end
end
