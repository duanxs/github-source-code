# frozen_string_literal: true

module Platform
  module Enums
    class SecurityAdvisorySeverity < Platform::Enums::VulnerabilitySeverity
      visibility :public # Override parent class
    end
  end
end
