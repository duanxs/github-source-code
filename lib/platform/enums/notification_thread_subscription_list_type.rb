# frozen_string_literal: true

module Platform
  module Enums
    class NotificationThreadSubscriptionListType < Platform::Enums::Base
      areas_of_responsibility :notifications
      feature_flag :pe_mobile
      description "The possible types of notification thread subscription lists."

      value "REPOSITORY", "Repository", value: "Repository"
      value "TEAM", "Team", value: "Team"
    end
  end
end
