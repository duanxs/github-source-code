# frozen_string_literal: true

module Platform
  module Enums
    class SponsorsListingOrderField < Platform::Enums::Base
      description "Properties by which Sponsors listing connections can be ordered."
      visibility :internal

      value "CREATED_AT", "Order Sponsors listing by creation time.", value: "created_at"
      value "UPDATED_AT", "Order Sponsors listing by last updated time.", value: "updated_at"
      value "PUBLISHED_AT", "Order Sponsors listing by published time.", value: "published_at"
    end
  end
end
