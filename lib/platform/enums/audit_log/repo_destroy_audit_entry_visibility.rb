# frozen_string_literal: true

module Platform
  module Enums
    module AuditLog
      class RepoDestroyAuditEntryVisibility < Platform::Enums::Base
        description "The privacy of a repository"

        areas_of_responsibility :audit_log, :repositories

        value "INTERNAL", "The repository is visible only to users in the same business.", value: "internal"
        value "PRIVATE", "The repository is visible only to those with explicit access.", value: "private"
        value "PUBLIC", "The repository is visible to everyone.", value: "public"
      end
    end
  end
end
