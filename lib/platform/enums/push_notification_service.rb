# frozen_string_literal: true

module Platform
  module Enums
    class PushNotificationService < Platform::Enums::Base
      description "A push notification service."
      mobile_only true
      areas_of_responsibility :notifications

      value "FCM", "Google Firebase Cloud Messaging", value: "fcm"
      value "APNS", "Apple Push Notification Service", value: "apns"
    end
  end
end
