# frozen_string_literal: true

module Platform
  module Enums
    class SponsorsCriterionFilter < Platform::Enums::Base
      description "The condition on which to filter criteria."
      visibility :internal

      value "ALL", "All criteria.", value: :all
      value "AUTOMATED", "The criteria that is evaluated automatically.", value: :automated
      value "MANUAL", "The criteria that is evaluated by a human actor.", value: :manual
    end
  end
end
