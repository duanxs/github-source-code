# frozen_string_literal: true

module Platform
  module Enums
    class SponsorsCriterionOrderField < Platform::Enums::Base
      description "Properties by which Sponsors criteria connections can be ordered."
      visibility :internal

      value "CREATED_AT", "Order Sponsors criteria by creation time.", value: "created_at"
    end
  end
end
