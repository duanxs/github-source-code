# frozen_string_literal: true

module Platform
  MAX_DEPTH      = 25
  MAX_TIMEOUT    = 5
  PROMISE_LAZY_METHOD_NAME = :sync
  INTERNAL_LAZY_METHOD_NAME = :value

  # Public: The max number of nodes that can be returned by a query for external
  # uses such as `api` and `rest_api`.
  MAX_NODE_COUNT_EXTERNAL = 500_000

  # Public: The max number of nodes that can be returned by a query for internal
  # uses such as `manual_execution` and `internal`.
  MAX_NODE_COUNT_INTERNAL = 1_020_100

  ORPHAN_TYPES = [
    Platform::Objects::CommitMention,
    Platform::Objects::Comparison,
    Platform::Objects::GistComment,
    Platform::Objects::GpgSignature,
    Platform::Objects::Push,
    Platform::Objects::RepositoryInvitation,
    Platform::Objects::SmimeSignature,
    Platform::Objects::Tag,
    Platform::Objects::UnknownSignature,
    Platform::Objects::UserEmail,
    Platform::Objects::Enterprise,
    Platform::Objects::EnterpriseIdentityProvider,
    Platform::Objects::EnterpriseAdministratorInvitation,
    Platform::Objects::EnterpriseOrganizationInvitation,
    Platform::Objects::FeatureActorApp,
    Platform::Objects::FeatureActorOrganization,
    Platform::Objects::FeatureActorEnterprise,
    Platform::Objects::FeatureActorUser,
    Platform::Objects::FeatureActorRepository,
    Platform::Objects::FeatureActorFlipperId,
    Platform::Objects::FeatureRole,
    Platform::Objects::FeatureHost,
    Platform::Objects::GenericHovercardContext,
    Platform::Objects::OrganizationsHovercardContext,
    Platform::Objects::OrganizationTeamsHovercardContext,
    Platform::Objects::ViewerHovercardContext,
    Platform::Objects::ReviewStatusHovercardContext,
    Platform::Objects::CheckRun,
    Platform::Objects::ValidationError,
    Platform::Objects::IpAllowListEntry,
    Platform::Objects::InternalRepositoryAdvisory,
  ].freeze

  PREVIEWS = [
    Platform::Previews::DeletePackageVersionPreview,
    Platform::Previews::DeploymentsPreview,
    Platform::Previews::ChecksPreview,
    Platform::Previews::MergeInfoPreview,
    Platform::Previews::UpdateRefsPreview,
    Platform::Previews::DependencyGraph,
    Platform::Previews::ProjectEventDetailsPreview,
    Platform::Previews::ContentAttachmentsPreview,
    Platform::Previews::PinnedIssuesPreview,
    Platform::Previews::LabelsPreview,
    Platform::Previews::ImportProjectPreview,
    Platform::Previews::TeamReviewAssignmentsPreview,
  ].freeze

  class Schema < GraphQL::Schema
    PUBLIC_SHA = Digest::SHA1.hexdigest(File.read(Rails.root.join("config/schema.public.graphql")))
    INTERNAL_SHA = Digest::SHA1.hexdigest(File.read(Rails.root.join("config/schema.internal.graphql")))

    max_depth MAX_DEPTH

    def self.upcoming_changes(target: :internal, environment: GitHub.runtime.current.to_sym)
      Platform::Evolution::UpcomingChanges.new(
        self,
        target: target,
        environment: environment,
      )
    end

    def self.resolve_type(abstract_type, object, context)
      type_name = Platform::Helpers::NodeIdentification.type_name_from_object(object)
      Platform::Schema.get_type(type_name) || raise(KeyError) # rubocop:disable GitHub/UsePlatformErrors
    rescue KeyError, NoMethodError
      interpreter_ns = context.namespace(:interpreter)
      current_field = interpreter_ns[:current_field]
      if current_field.present?
        Failbot.push(graphql_current_field: current_field.path)
      end
      current_path = interpreter_ns[:current_path]
      Failbot.push(graphql_current_path: current_path) if current_path.present?
      raise Platform::Errors::Type, "Platform type '#{object.class}' is not defined."
    end

    # Modified version of GraphQL-Ruby's `DefaultTypeError`
    # graphql-ruby/lib/graphql/schema/default_type_error.rb
    def self.type_error(type_error, ctx)
      case type_error
      when GraphQL::InvalidNullError
        if Rails.production?
          # Replace invalid non null errors with an internal error instead
          # to not leak errors that are out of a client's control. However, we
          # still want to send them to haystack to fix the underlying issue.
          Failbot.report(type_error)
          ctx.add_error(Platform::Errors::InternalExecution.new(type_error))
        else
          ctx.errors << type_error
        end
      when GraphQL::UnresolvedTypeError, GraphQL::StringEncodingError
        raise type_error # rubocop:disable GitHub/UsePlatformErrors
      when GraphQL::IntegerEncodingError
        # Recent versions of GraphQL started raising with integers too large for the INT type.
        # To maintain backward compat, use the old behaviour (truncate)
        type_error.integer_value
      else
        raise Platform::Errors::Type, "Unexpected type_error #{type_error} during GraphQL Execution"
      end
    end

    def self.id_from_object(object, _, _)
      object.global_relay_id
    end

    def self.object_from_id(id, context)
      Platform::Helpers::NodeIdentification.untyped_object_from_id(id, permission: context[:permission]).then do |object|
        if object
          object
        else
          raise Platform::Errors::NotFound, "Could not resolve to a node with the global id of '#{id}'."
        end
      end
    end

    # Performance optimization because we know our only
    # lazy objects are promises besides graphql-ruby internal obj
    def lazy_method_name(obj)
      case obj
      when GraphQL::Execution::Lazy
        INTERNAL_LAZY_METHOD_NAME
      when Promise
        PROMISE_LAZY_METHOD_NAME
      else
        nil
      end
    end

    query Platform::Objects::Query
    mutation Platform::Objects::Mutation
    orphan_types ORPHAN_TYPES

    # Activate GraphQL::Batch loading
    use Platform::Batch::Plugin

    instrument(:query, Platform::RateLimitRequest)

    use GraphQL::Execution::Interpreter
    use GraphQL::Analysis::AST
    query_analyzer(Platform::Analyzers::QueryCoster)
    query_analyzer(Platform::Analyzers::LimitIntrospection)
    query_analyzer(Platform::Analyzers::MinimumAcceptedScopes)

    def self.enabled_previews(environment = nil)
      PREVIEWS.select do |preview|
        if environment.nil?
          true
        else
          preview.environments.nil? || preview.environments.include?(environment)
        end
      end
    end

    Platform::Preview.install(self, enabled_previews)
  end
end
