# frozen_string_literal: true

module Platform
  module Resolvers
    class ActionUsableRepositories < Resolvers::Base
      argument :search_phrase, String, "An optional filter to search the repositories.", required: false

      argument :order_by, Inputs::RepositoryOrder, "Ordering options for repositories returned from the connection", required: false

      type Connections::Repository, null: false

      def resolve(search_phrase: nil, order_by: nil)
        # If there is more than 1 / in the search phrase, don't bother trying to search. nwo is invalid and nothing will return
        return Repository.none if search_phrase && search_phrase.count("/") > 1

        relation = ::Repository.
          active.
          filter_spam_and_disabled_for(context[:viewer]).
          where("repositories.id IN (?)", context[:viewer].associated_repository_ids(min_action: :write, including: [:owned, :direct, :indirect]))

        if search_phrase.present?
          case search_phrase.count("/")
          when 0
            matching_user_ids = ::User.where(id: relation.distinct.pluck(:owner_id)).where("login LIKE ?", wildcarded_phrase(search_phrase)).pluck(:id)

            relation = relation.where(
              "repositories.name LIKE ? OR repositories.owner_id IN (?)",
              wildcarded_phrase(search_phrase), matching_user_ids
            )
          when 1
            owner_phrase, name_phrase = search_phrase.split("/")

            matching_user_ids = ::User.where("login LIKE ?", wildcarded_phrase(owner_phrase)).pluck(:id)

            relation = relation.where(
              "repositories.name LIKE ? AND repositories.owner_id IN (?)",
              wildcarded_phrase(name_phrase),
              matching_user_ids,
            )
          end
        end

        if order_by
          relation = relation.order("repositories.#{order_by[:field]} #{order_by[:direction]}")
        end

        relation
      end

      def wildcarded_phrase(phrase)
        "%#{ActiveRecord::Base.sanitize_sql_like(phrase)}%"
      end
    end
  end
end
