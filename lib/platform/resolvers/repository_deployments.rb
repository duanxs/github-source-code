# frozen_string_literal: true

module Platform
  module Resolvers
    class RepositoryDeployments < Resolvers::Deployments
      def resolve(**arguments)
        context[:permission].async_can_list_deployments?(repo: object).then do |can_list_deployments|
          return ::Deployment.none unless can_list_deployments

          field = arguments[:order_by][:field]
          direction = arguments[:order_by][:direction]
          ordering = "deployments.#{field} #{direction}"

          query = @object.deployments
          if arguments[:environments] && arguments[:environments].any?
            query = query.where("latest_environment in (?)", arguments[:environments])
          end
          query.reorder(ordering)
        end
      end
    end
  end
end
