# frozen_string_literal: true

module Platform
  module Resolvers
    class GistComments < Resolvers::Base

      type Connections.define(Objects::GistComment), null: false

      def resolve
        case object
        when Gist
          if context[:permission].async_can_list_gist_comments?(object).sync
            object.async_comments.then do
              object.comments.filter_spam_for(context[:viewer])
            end
          end
        when User
          # Replicates permissions for Gists
          if context[:permission].async_can_list_gist_comments?(object).sync && context[:permission].can_list_user_secret_gists?(owner: object)
            relation = object.gist_comments
          else # No scope needed for public Gist comments
            relation = object.gist_comments.joins(:gist).merge(Gist.are_public)
          end
          relation.filter_spam_for(context[:viewer])
        end
      end
    end
  end
end
