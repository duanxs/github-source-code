# frozen_string_literal: true

module Platform
  module Resolvers
    class PendingOrganizationMembers < Platform::Resolvers::Users
      def resolve
        has_member_access = object.member_or_can_view_members?(context[:viewer])
        can_list_members = context[:permission].access_allowed?(
          :v4_read_org_invitations, resource: object, current_org: object, current_repo: nil,
          allow_integrations: true, allow_user_via_integration: true, raise_on_error: false
        )

        if has_member_access && can_list_members
          users = object.pending_members
          filter_spam(users)
        else
          ArrayWrapper.new([])
        end
      end
    end
  end
end
