# frozen_string_literal: true

module Platform
  module Resolvers
    class RefPullRequests < Resolvers::PullRequests
      def fetch_pull_requests(ref, _, _)
        scope = ::PullRequest.where(head_repository_id: ref.repository.id).for_branch(ref.qualified_name)

        scope_promise = if context[:viewer]&.can_have_granular_permissions? && Ability.can_at_least?(:read, context[:viewer].installation.permissions["pull_requests"])
          scope = scope.joins(:repository).where(["repositories.public = true OR repositories.id IN (?)", context[:viewer].associated_repository_ids])
          Promise.resolve(scope)
        else
          context[:permission].async_can_list_pull_requests?(ref.repository).then do |can_list_pull_requests|
            can_list_pull_requests ? scope : scope.joins(:repository).where(["repositories.public = true"])
          end
        end

        scope_promise.then { scope }

      end
    end
  end
end
