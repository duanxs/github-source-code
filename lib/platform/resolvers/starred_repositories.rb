# frozen_string_literal: true

module Platform
  module Resolvers
    class StarredRepositories < Resolvers::Base
      # This matches https://github.com/rmosolgo/graphql-ruby/blob/1.9-dev/lib/graphql/schema/field/connection_extension.rb
      argument :after, "String", "Returns the elements in the list that come after the specified cursor.", required: false
      argument :before, "String", "Returns the elements in the list that come before the specified cursor.", required: false
      argument :first, "Int", "Returns the first _n_ elements from the list.", required: false
      argument :last, "Int", "Returns the last _n_ elements from the list.", required: false

      argument :owned_by_viewer, Boolean, "Filters starred repositories to only return repositories owned by the viewer.", required: false
      argument :order_by, Inputs::StarOrder, "Order for connection", required: false
      argument :repository_database_ids, [Integer, null: true],
        "Optional list of repository database IDs with which to filter the results. If provided, only starred repositories in this list will be returned.",
        visibility: :internal, required: false
      argument :language, String,
        "An optional programming language to use to filter the repositories.",
        visibility: :under_development, required: false

      # Before going public with this, we should talk with @github/ecosystem-api about how :query
      # works in GraphQL and ensure it's consistent between all types.
      argument :query, String, "An optional filter to search the starred repositories.",
        visibility: :under_development, required: false

      type Connections::StarredRepository, null: true

      def resolve(query: nil, language: nil, order_by: nil, owned_by_viewer: nil,
                  repository_database_ids: nil, **connection_arguments)
        if query.present? || language.present?
          repo_query = search_starred_repositories(
            query: query,
            owned_by_viewer: owned_by_viewer,
            language: language,
            order_by: order_by,
          )

          Platform::ConnectionWrappers::SearchQuery.new(repo_query, connection_arguments,
            field: field,
            context: @context,
            parent: @object
          )
        else
          Platform::ConnectionWrappers::StarredRepositories.new(@object,
            field: field,
            context: @context,

            order_by: order_by,
            repository_database_ids: repository_database_ids,
            owned_by_viewer: owned_by_viewer,

            **connection_arguments
          )
        end
      end

      private

      def search_starred_repositories(query:, owned_by_viewer:, language:, order_by:)
        Search::Queries::RepoQuery.new(phrase: search_phrase(query, owned_by_viewer),
                                       source_fields: false, star_search: true,
                                       include_forks: true, current_user: context[:viewer],
                                       star_user: object, language: language,
                                       sort: search_sort(order_by))
      end

      def search_phrase(query, owned_by_viewer)
        phrase = query
        phrase += " user:#{context[:viewer]}" if owned_by_viewer
        phrase
      end

      def search_sort(order_by)
        return unless order_by

        direction = (order_by[:direction] || "desc").downcase
        case order_by[:field]
        when "pushed_at"
          ["updated", direction]
        when "watcher_count"
          ["stars", direction, "updated", direction]
        else
          # can't sort by "recently starred" while searching -- see #60971
          ["stars", direction]
        end
      end
    end
  end
end
