# frozen_string_literal: true

module Platform
  module Resolvers
    class EnterprisePendingMemberInvitations < Resolvers::Base
      argument :query, String, "The search string to look for.", required: false
      argument :order_by, Inputs::OrganizationInvitationOrder,
        "Ordering options for pending enterprise organization member invitations returned from the connection.",
        required: false, default_value: { field: "created_at", direction: "DESC" }

      type Platform::Connections::EnterprisePendingMemberInvitation, null: false

      def resolve(query: nil, order_by: nil)
        pending_invitations = if context[:permission].can_list_private_business_members?(object)
          object.pending_member_invitations(
            query: query,
            order_by_field: order_by&.dig(:field),
            order_by_direction: order_by&.dig(:direction),
          )
        else
          OrganizationInvitation.none
        end

        Promise.resolve(ArrayWrapper.new(pending_invitations))
      end
    end
  end
end
