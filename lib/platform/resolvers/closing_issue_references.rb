# frozen_string_literal: true

module Platform
  module Resolvers
    class ClosingIssueReferences < Resolvers::Base
      type Connections.define(Objects::Issue), null: true

      def resolve(**arguments)
        @object.async_close_issue_references_for(
          viewer: @context[:viewer],
          unauthorized_organization_ids: Array(@context[:unauthorized_organization_ids]),
        ).then do |accessible_issues|
          ArrayWrapper.new(accessible_issues.compact)
        end
      end
    end
  end
end
