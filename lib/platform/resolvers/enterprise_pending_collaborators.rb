# frozen_string_literal: true

module Platform
  module Resolvers
    class EnterprisePendingCollaborators < Resolvers::Base

      argument :query, String, "The search string to look for.", required: false
      argument :order_by, Inputs::RepositoryInvitationOrder,
        "Ordering options for pending repository collaborator invitations returned from the connection.",
        required: false, default_value: { field: "CREATED_AT", direction: "DESC" }

      type Platform::Connections::EnterprisePendingCollaborator, null: false

      def resolve(query: nil, order_by: nil)
        pending_collaborators = if context[:permission].can_list_business_outside_collaborators?(object)
          # This resolver is deprecated. The logic below just ensures
          # consistency until this is removed.
          user_ids = object.pending_collaborator_invitations(
            query: query,
            order_by_field: order_by&.dig(:field),
            order_by_direction: order_by&.dig(:direction)
          ).where("repository_invitations.invitee_id IS NOT NULL").pluck(:invitee_id)

          User.with_ids(user_ids).order(Arel.sql("FIELD(id, #{user_ids.join(",")})"))
        else
          User.none
        end

        ArrayWrapper.new(pending_collaborators)
      end
    end
  end
end
