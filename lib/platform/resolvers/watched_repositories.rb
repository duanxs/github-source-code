# frozen_string_literal: true

module Platform
  module Resolvers
    class WatchedRepositories < Platform::Resolvers::Repositories
      BATCH_SIZE = 100

      argument :affiliations, [Enums::RepositoryAffiliation, null: true],
        required: false,
        description: "Affiliation options for repositories returned from the connection. If none specified, the results will include repositories for which the current viewer is an owner or collaborator, or member."

      def fetch_repositories(owner, _, context, scope:, order_by:)
        ids = Helpers::WatchingQuery.new(context[:viewer], owner).fetch!.to_a

        result = ids.each_slice(BATCH_SIZE).with_object(Array.new) do |repo_ids, list|
          list.concat(::Repository.where(id: repo_ids).merge(scope))
        end
        result = sort_repo_list(result, order_by)

        ArrayWrapper.new(result)
      end

      private

      def default_affiliations
        [:owned, :direct, :indirect]
      end
    end
  end
end
