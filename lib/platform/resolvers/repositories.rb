# frozen_string_literal: true

module Platform
  module Resolvers
    class Repositories < Resolvers::Base
      argument :privacy, Enums::RepositoryPrivacy, "If non-null, filters repositories according to privacy", required: false

      argument :order_by, Inputs::RepositoryOrder, "Ordering options for repositories returned from the connection", required: false

      argument :affiliations, [Enums::RepositoryAffiliation, null: true],
        required: false,
        description: "Array of viewer's affiliation options for repositories returned from the connection. For example, OWNER will include only repositories that the current viewer owns."

      argument :owner_affiliations, [Enums::RepositoryAffiliation, null: true],
        required: false,
        description: "Array of owner's affiliation options for repositories returned from the connection. For example, OWNER will include only repositories that the organization or user being viewed owns.",
        default_value: [:owned, :direct]

      argument :is_locked, Boolean,
        required: false,
        description: "If non-null, filters repositories according to whether they have been locked"

      argument :language, String,
        "An optional, case-insensitive programming language to use to filter the repositories (e.g. 'Ruby')",
        visibility: :under_development, required: false

      argument :type, Enums::RepositoryType,
        "An optional type to use to filter the repositories.",
        visibility: :under_development, required: false

      # Before going public with this, we should talk with @github/ecosystem-api about how :query
      # works in GraphQL and ensure it's consistent between all types.
      argument :query, String, "An optional filter to search the repositories.",
        visibility: :under_development, required: false

      type Connections::Repository, null: false

      def resolve(privacy: nil, order_by: nil, affiliations: nil, owner_affiliations:,
                  is_locked: nil, is_fork: nil, language: nil, type: nil, query: nil)
        filtering_by_affiliations = affiliations.present?
        affiliations = affiliations || default_affiliations
        if query.present? || language.present?
          search_repositories(query: query, language: language, order_by: order_by, type: type)
        else
          list_repositories(privacy: privacy, order_by: order_by, affiliations: affiliations,
                            owner_affiliations: owner_affiliations, is_locked: is_locked,
                            is_fork: is_fork, type: type, filtering_by_affiliations: filtering_by_affiliations)
        end
      end

      private

      def default_affiliations
        [:owned, :direct]
      end

      def search_repositories(query:, language:, order_by:, type:)
        Search::Queries::RepoQuery.new(phrase: search_phrase(query, type),
                                       source_fields: false, star_search: false,
                                       include_forks: true, current_user: context[:viewer],
                                       star_user: object, language: language,
                                       sort: search_sort(order_by))
      end

      def search_phrase(query, type)
        query ||= ""
        phrase = add_type_to_query(query, type)
        phrase += " user:#{object.login}"
      end

      def add_type_to_query(query, type)
        return query if type.nil? || type.empty?

        type_string = case type
        when "public"    then "is:public archived:false"
        when "private"   then "is:private"
        when "fork"      then "fork:only archived:false"
        when "mirror"    then "mirror:true archived:false"
        when "source"    then "mirror:false archived:false fork:false"
        when "archived"  then "archived:true"
        else ""
        end

        query ? "#{query} #{type_string}" : type_string
      end

      def search_sort(order_by)
        return unless order_by

        direction = (order_by[:direction] || "desc").downcase
        case order_by[:field]
        when "pushed_at"
          ["updated", direction]
        when "updated_at"
          ["updated", direction]
        when "created_at"
          ["created", direction]
        when "name"
          ["name", direction]
        else
          # can't sort by "recently starred" while searching -- see #60971
          ["stars", direction]
        end
      end

      def list_repositories(order_by:, type:, privacy:, affiliations:, owner_affiliations:, is_locked:, is_fork:, filtering_by_affiliations:)
        ensure_associated_repositories_are_preloaded
        # Apply filters and return a new relation
        relation = filter_repositories(::Repository, object, context, order_by: order_by,
                                       is_fork: is_fork, affiliations: affiliations,
                                       owner_affiliations: owner_affiliations, is_locked: is_locked,
                                       privacy: privacy, filtering_by_affiliations: filtering_by_affiliations)
        relation = filter_repos_by_type(relation, type: type, privacy: privacy,
                                        affiliations: affiliations)
        # if this is going through the API, chuck the private repos you can't see
        relation = determine_visibility(relation, object, privacy, context)
        # Fetch repositories from the database; return an AR::Relation or Array of Repositories
        fetch_repositories(object, owner_affiliations, context, scope: relation, order_by: order_by)

      end

      protected

      # Apply the specified ordering to the overall list, in case the repositories were fetched
      # in batches with only each batch being ordered in SQL
      def sort_repo_list(repos, order_by)
        if order_by
          # This column has been marked for rename. We need to refer to the new name here as this
          # accesses the model's attribute, not the database column. This can go when the rename
          # is complete.
          field = order_by[:field] == "watcher_count" ? "stargazer_count" : order_by[:field]
          repos = repos.sort_by { |repo| repo[field].to_s }
          repos = repos.reverse if order_by[:direction] == "DESC"
        end

        repos
      end

      # Internal: Given a Repository scope, will return a modified scope that filters the
      # repositories according to the given type filter.
      def filter_repos_by_type(relation, type:, privacy:, affiliations:)
        return relation if type.nil? && privacy.nil?

        if type == PlatformTypes::RepositoryType::PUBLIC.downcase || privacy == PlatformTypes::RepositoryType::PUBLIC.downcase
          relation.public_scope
        elsif type == PlatformTypes::RepositoryType::PRIVATE.downcase || privacy == PlatformTypes::RepositoryType::PRIVATE.downcase
          if object.is_a?(Organization)
            relation = relation.private_scope
          elsif object.is_a?(User) || object.is_a?(Repository)
            relation = relation.private_scope.where("repositories.id IN (?)", associated_repository_ids(context, affiliations))
          end
        elsif type == "source"
          relation.not_archived_scope.where(parent_id: nil)
        elsif type == "fork"
          relation.not_archived_scope.forks
        elsif type == "mirror"
          relation.not_archived_scope.joins(:mirror)
        elsif type == "archived"
          relation.archived_scope
        else
          relation
        end
      end

      # These can be loaded without association tracking;
      # Later code depends on them being preloaded.
      def ensure_associated_repositories_are_preloaded
        context[:permission].associated_repository_ids
      end

      # Fetch some repositories from the database. Filter them using the given scope.
      # Returns an ActiveRecord::Relation or an Array of Repositories
      def fetch_repositories(owner, owner_affiliations, context, scope:, order_by:)
        if owner.is_a?(Organization)
          org_owned_repos(owner, order_by, context, scope: scope)
        elsif owner.is_a?(Repository)
          owner.forks.merge(scope)
        elsif owner.is_a?(RepositoryNetwork)
          owner.repositories.merge(scope)
        elsif owner.is_a?(Topic)
          owner.repositories.merge(scope)
        else
          scope.from("repositories FORCE INDEX (PRIMARY)").where(id: owner.associated_repository_ids(including: owner_affiliations))
        end
      end

      def org_owned_repos(owner, order_by, context, scope:)
        repos = owner.all_org_repos_for_user(context[:viewer], scope: scope)

        if context[:viewer]&.using_auth_via_integration? && repos.any?
          integration  = context[:viewer].oauth_access.application
          installation = context[:viewer].oauth_access.installation

          public_repo_ids = repos.public_scope.pluck(:id)
          private_repo_ids = repos.private_scope.pluck(:id)

          installation_accessible_repo_ids = integration.accessible_repository_ids(
            current_integration_installation: installation,
            repository_ids: private_repo_ids,
          )

          accessible_repository_ids = public_repo_ids | installation_accessible_repo_ids

          repos = Repository.with_ids(accessible_repository_ids)
        end

        return repos unless order_by
        repos.order("repositories.#{order_by[:field]} #{order_by[:direction]}")
      end

      # Given a relation of Repositories, apply some filtering:
      # - Filter out spam
      # - Apply filters (if given) on privacy, affiliations, is_locked, language, type and query
      # - Filter out inactive repositories
      #
      # Returns a new ActiveRecord::Relation
      def filter_repositories(relation, owner, context, order_by:, affiliations:,
                              owner_affiliations:, is_locked:, is_fork:, privacy:, filtering_by_affiliations:)
        relation = relation.filter_spam_and_disabled_for(context[:viewer])

        filtering_on_private = privacy == PlatformTypes::RepositoryType::PRIVATE.downcase
        filtering_on_public = privacy == PlatformTypes::RepositoryType::PUBLIC.downcase

        case is_locked
        when true
          relation = relation.locked_repos
        when false
          relation = relation.unlocked_repos
        end

        if context[:unauthorized_organization_ids].present?
          relation = relation.where("repositories.owner_id NOT IN (?)", context[:unauthorized_organization_ids])
        end

        if filtering_on_private
          if owner.is_a?(Organization)
            relation = relation.private_scope
          elsif owner.is_a?(User) || owner.is_a?(Repository)
            relation = relation.private_scope.where(["repositories.id IN (?)", associated_repository_ids(context, affiliations)])
          end
        elsif filtering_on_public
          relation = relation.public_scope
        else
          case owner
          when Organization
            # No further filtering required,
            # but don't let it fall through to `User` below
          when User, Repository, RepositoryNetwork
            if filtering_by_affiliations
              relation = relation.where(["(repositories.id IN (?))", associated_repository_ids(context, affiliations)])
            else
              relation = relation.where(["repositories.public = true OR (repositories.id IN (?))", associated_repository_ids(context, affiliations)])
            end
          end
        end

        if owner.is_a?(User) && !owner.is_a?(Organization) && owner_affiliations.include?(:indirect)
          # filter out private org memberships for this user
          # - Get the list of _visible_ orgs
          # - Get the list of all of the orgs the viewer belongs to
          # - Get the list of _all_ orgs
          # - Get the difference of the two; those are the hidden orgs
          # - Remove repos that belong to hidden orgs AND are not contributed-to.
          all_org_ids = owner.organizations.pluck("id")
          if all_org_ids.any?
            visible_org_ids = Organization.github_sql.results <<-SQL, user_id: owner.id
              SELECT organization_id FROM public_org_members
              WHERE user_id = :user_id
            SQL
            visible_org_ids.flatten!

            # Find the orgs of the viewer, to allow those repos to be seen
            viewer_org_ids = context[:viewer]&.organizations&.pluck("id") || []

            # Hide away the organizations that are either private or the viewer does not
            # have access to
            hidden_org_ids = all_org_ids - (visible_org_ids + viewer_org_ids)
            if hidden_org_ids.any?
              # `hidden_org_ids` may filter out repos that the user contributed to.
              # But if `:direct` is also present, we should also include
              # repos where this user contributed, even if their org membership is hidden
              contributed_repo_override_ids = if owner_affiliations.include?(:direct)
                owner.associated_repository_ids(including: [:direct])
              else
                []
              end

              relation = relation.where("
                    repositories.organization_id IS NULL
                OR  repositories.organization_id NOT IN(?)
                OR  repositories.id IN(?)
              ", hidden_org_ids, contributed_repo_override_ids)
            end
          end
        end
        case is_fork
        when true
          relation = relation.where("repositories.parent_id IS NOT NULL")
        when false
          relation = relation.where("repositories.parent_id IS NULL")
        end
        if order_by
          relation = relation.order("repositories.#{order_by[:field]} #{order_by[:direction]}")
        end

        relation.active
      end

      def associated_repository_ids(context, affiliations)
        return [] unless context[:viewer].present?
        viewer = context[:viewer]

        repo_ids = viewer.associated_repository_ids(including: affiliations)

        return repo_ids if repo_ids.empty?
        return repo_ids unless viewer.using_auth_via_integration?

        integration = context[:viewer].oauth_access.application
        installation = context[:viewer].oauth_access.installation

        installation_accessible_repo_ids = integration.accessible_repository_ids(
          current_integration_installation: installation,
          repository_ids: repo_ids,
        )

        Repository.where(id: repo_ids).public_scope.pluck(:id) | installation_accessible_repo_ids
      end

      def determine_visibility(relation, owner, privacy, context)
        return relation if context[:permission].async_can_list_private_repos?(owner).sync
        if privacy == "private"
          relation.none
        else
          relation.public_scope
        end
      end
    end
  end
end
