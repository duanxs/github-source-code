# frozen_string_literal: true

module Platform
  module Resolvers
    class OrganizationMembers < Platform::Resolvers::Users
      type Connections::OrganizationMember, null: false

      def resolve(**arguments)
        max_members_limit = arguments[:max_members_limit]
        if context[:permission].can_list_private_org_members?(object)
          user_ids = object.visible_user_ids_for(context[:viewer], limit: max_members_limit)
        else
          user_ids = object.public_member_ids.take(max_members_limit)
        end

        users = if user_ids.empty?
          User.none
        else
          User.where(ActiveRecord::Base.sanitize_sql(["users.id IN (?)", user_ids]))
        end

        filter_spam(users)
      end
    end
  end
end
