# frozen_string_literal: true

module Platform
  module Resolvers
    class TemplateRepositories < Platform::Resolvers::Repositories
      def fetch_repositories(owner, _, context, scope:, order_by:)
        repos = owner.repository_templates_for(context[:viewer], scope: scope)
        ArrayWrapper.new(repos)
      end
    end
  end
end
