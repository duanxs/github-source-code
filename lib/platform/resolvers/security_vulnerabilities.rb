# frozen_string_literal: true

module Platform
  module Resolvers
    class SecurityVulnerabilities < Resolvers::Base
      argument :order_by, Inputs::SecurityVulnerabilityOrder,
                          "Ordering options for the returned topics.",
                          required: false,
                          default_value: { field: "updated_at", direction: "DESC" }

      argument :ecosystem, Enums::SecurityAdvisoryEcosystem,
                           "An ecosystem to filter vulnerabilities by.",
                           required: false

      argument :package, String,
                         "A package name to filter vulnerabilities by.",
                         required: false

      argument :severities, [Enums::SecurityAdvisorySeverity],
                            "A list of severities to filter vulnerabilities by.",
                            required: false

      type Connections.define(Objects::SecurityVulnerability), null: false

      def resolve(order_by:, **arguments)

        case object
        when SecurityAdvisory
          return empty_relation unless advisory_within_filter?(object, arguments)

          # we are calling .all in order to get a ActiveRecord_AssociationRelation
          # if we don't we get a ActiveRecord_Associations_CollectionProxy
          # for some reason I don't totally understand that class gets a Platform::Errors::AssociationRefused when .map is called on it, but the Platform::Errors::AssociationRefused does not
          # so to workaround that issue, we force the class that works by using .all
          scope = object.vulnerabilities.all
        else
          scope = filtered_base_scope(arguments)
        end

        if arguments[:package].present?
          scope = scope.where("#{table_name}.affects = ?", arguments[:package])
        end

        scope = scope.order("#{table_name}.#{order_by[:field]} #{order_by[:direction]}")

        if object.is_a?(SecurityAdvisory) && object.visible_publicly?
          vulns = scope.map { |vuln| vuln.becomes(SecurityVulnerabilityForceVisibility) }
          ArrayWrapper.new(vulns)
        else
          scope
        end
      end

      private

      # Applys filters specific to resolutions against all vulnerability objects
      def filtered_base_scope(arguments)
        return base_scope if arguments.empty?

        scope = base_scope

        if arguments[:ecosystem].present?
          scope = scope.where(ecosystem: arguments[:ecosystem])
        end

        if arguments[:severities].present?
          scope = scope.where("#{advisory_table_name}.severity IN (?)", arguments[:severities])
        end

        return scope
      end

      # Several vulnerability properties are delegates on the parent object due
      # to the way we facade our existing data. This means that instead of scoping
      # we need to check if the parent object falls outside the filter.
      def advisory_within_filter?(advisory, arguments)
        advisory.matches_ecosystem?(arguments[:ecosystem]) && advisory.matches_severities?(arguments[:severities])
      end

      def empty_relation
        ::SecurityVulnerability.none
      end

      def base_scope
        ::SecurityVulnerability.disclosed.always_visible_publicly
      end

      def table_name
        ::SecurityVulnerability.table_name
      end

      def advisory_table_name
        ::SecurityAdvisory.table_name
      end
    end
  end
end
