# frozen_string_literal: true

module Platform
  module Resolvers
    class Assignees < Platform::Resolvers::Users
      def resolve
        object.async_repository.then do |repository|
          context[:permission].async_owner_if_org(repository).then do |org|
            if context[:permission].access_allowed?(:list_assignees, repo: repository, current_org: org, allow_integrations: true, allow_user_via_integration: true)
              if object.is_a?(PullRequest)
                object.async_issue.then do |issue|
                  get_assignees(issue)
                end
              else
                get_assignees(object)
              end
            else
              ::User.none
            end
          end
        end
      end

      private

      def get_assignees(issue)
        Promise.all([issue.async_assignees, issue.async_commenters]).then do
          filter_spam(issue.assignees)
        end
      end
    end
  end
end
