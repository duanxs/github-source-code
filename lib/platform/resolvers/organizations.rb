# frozen_string_literal: true

module Platform
  module Resolvers
    class Organizations < Resolvers::Base
      type Connections.define(Objects::Organization), null: false

      argument :logins, [String], "A list of organization logins to filter by.", required: false, visibility: :internal

      argument :database_ids, [Integer], "A list of organization database IDs to filter by.", required: false, visibility: :internal

      def resolve(logins: nil, database_ids: nil)
        conditions = []
        args = []

        if database_ids.present?
          conditions << "users.id IN (?)"
          args << database_ids
        end

        if logins.present?
          conditions << "users.login IN (?)"
          args << logins
        end

        orgs = case object
        when UserHovercard::Contexts::Organizations
          object.highlighted
        when User
          if context[:permission].can_get_private_org_member?(object, resource: "members")
            if installation = context[:installation]
              viewable_org_ids = object.public_organizations.pluck(:id) << installation.target.id
            else
              viewable_org_ids = object.public_organizations.pluck(:id) | context[:viewer].organizations.pluck(:id)
            end
            object.organizations.where(id: viewable_org_ids)
          else
            object.public_organizations
          end
        else
          ::Organization
        end

        orgs = orgs.where(conditions.join(" OR "), *args) if conditions.present?

        if context[:unauthorized_organization_ids].present?
          orgs = orgs.where("id NOT IN (?)", context[:unauthorized_organization_ids])
        end

        orgs.filter_spam_for(context[:viewer])
      end
    end
  end
end
