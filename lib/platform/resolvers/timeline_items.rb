# frozen_string_literal: true

module Platform
  module Resolvers
    class TimelineItems < Resolvers::Base
      argument :since, Scalars::DateTime, "Filter timeline items by a `since` timestamp.", required: false

      # NOTE: Before publishing the `timelineItems` connections, the plan is to replace this
      # argument by a list of requested timeline item types.
      argument :visible_events_only, Boolean, "Only return events visible in GitHub's UI.", required: false, visibility: :internal

      argument :skip, Integer, "Skips the first _n_ elements in the list.", required: false

      argument :focus, ID, "ID of element to focus on.", required: false, visibility: :internal

      class << self
        def define_connection(type)
          conn_name = self.graphql_name

          Class.new(Platform::Connections::Base) do
            edge_type(type.edge_type, node_type: type)
            graphql_name "#{conn_name}Connection"

            total_count_field

            field :filtered_count, Integer, null: false,
              description: "Identifies the count of items after applying `before` and `after` filters."

            field :page_count, Integer, null: false,
              description: "Identifies the count of items after applying `before`/`after` filters and `first`/`last`/`skip` slicing."

            field :before_focus_count, Integer, null: false,
              description: "Identifies the count of items before the focused item (`focus`).",
              visibility: :internal

            field :after_focus_count, Integer, null: false,
              description: "Identifies the count of items after the focused item (`focus`).",
              visibility: :internal

            field :updated_at, Scalars::DateTime, null: false,
              description: "Identifies the date and time when the timeline was last updated.",
              method: :async_updated_at
          end
        end

        def define_item_type_enum(union_type)
          conn_name = self.graphql_name

          Class.new(Platform::Enums::Base) do
            graphql_name "#{conn_name}ItemType"
            description "The possible item types found in a timeline."

            union_type.possible_types.each do |type|
              value type.graphql_name.underscore.upcase, type.description, value: type,
                visibility: type.visibility.include?(:public) ? :public : :internal
            end
          end
        end

        def define_item_types_argument(union_type)
          argument :item_types, [define_item_type_enum(union_type)], "Filter timeline items by type.", required: false
        end
      end

      def resolve(**arguments)
        visible_events_only = !!arguments[:visible_events_only]

        filter_options = {
          since: arguments[:since],
          item_types: arguments[:item_types],
          visible_events_only: visible_events_only,
          filter_closed_if_preceded_by_merged: visible_events_only &&
                                               object.respond_to?(:merged?) && object.merged?,
        }

        object.timeline_model_for(context[:viewer], filter_options)
      end
    end
  end
end
