# frozen_string_literal: true

module Platform
  module Resolvers
    class ExternalIdentities < Resolvers::Base
      type Connections.define(Objects::ExternalIdentity), null: false

      def resolve
        object.external_identities.scoped
      end
    end
  end
end
