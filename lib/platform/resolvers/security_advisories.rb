# frozen_string_literal: true

module Platform
  module Resolvers
    class SecurityAdvisories < Resolvers::Base
      argument :order_by, Inputs::SecurityAdvisoryOrder,
               "Ordering options for the returned topics.",
               required: false,
               default_value: { field: "updated_at", direction: "DESC" }

      argument :identifier, Inputs::SecurityAdvisoryIdentifierFilter,
               "Filter advisories by identifier, e.g. GHSA or CVE.",
               required: false

      argument :published_since, Scalars::DateTime,
               "Filter advisories to those published since a time in the past.",
               required: false

      argument :updated_since, Scalars::DateTime,
               "Filter advisories to those updated since a time in the past.",
               required: false

      type Connections.define(Objects::SecurityAdvisory), null: false

      def resolve(order_by:, identifier: nil, published_since: nil, updated_since: nil, **arguments)
        scope = ::SecurityAdvisory.disclosed
                                  .always_visible_publicly
                                  .order("#{table_name}.#{order_by[:field]} #{order_by[:direction]}")

        scope = filter_by_identifier(scope, identifier) if identifier.present?
        scope = scope.where("#{table_name}.published_at > ?", published_since) if published_since.present?
        scope = scope.where("#{table_name}.updated_at > ?", updated_since) if updated_since.present?

        scope
      end

      private

      def filter_by_identifier(scope, identifier)
        sanitized_value = ActiveRecord::Base.sanitize_sql_like(identifier[:value])

        case identifier[:type]
        when "CVE"
          scope.where("#{table_name}.identifier LIKE ?", "%#{sanitized_value}%")
        when "GHSA"
          scope.where("#{table_name}.ghsa_id LIKE ?", "%#{sanitized_value}%")
        else
          scope
        end
      end

      def table_name
        ::SecurityAdvisory.table_name
      end
    end
  end
end
