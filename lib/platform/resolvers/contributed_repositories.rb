# frozen_string_literal: true

module Platform
  module Resolvers
    class ContributedRepositories < Platform::Resolvers::Repositories
      BATCH_SIZE = 100

      def fetch_repositories(owner, _, context, scope:, order_by:)
        ids = owner.ranked_contributed_repositories(viewer: context[:viewer]).keys.map(&:id)

        result = ids.each_slice(BATCH_SIZE).with_object(Array.new) do |repo_ids, list|
          list.concat(::Repository.where(id: repo_ids).merge(scope))
        end
        result = sort_repo_list(result, order_by)

        ArrayWrapper.new(result)
      end
    end
  end
end
