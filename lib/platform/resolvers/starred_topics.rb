# frozen_string_literal: true

module Platform
  module Resolvers
    class StarredTopics < Resolvers::Base
      type Connections::StarredTopic, null: true

      def resolve
        object.stars.where(starrable_type: Star::STARRABLE_TYPE_TOPIC)
      end
    end
  end
end
