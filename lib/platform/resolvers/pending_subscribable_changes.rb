# frozen_string_literal: true

module Platform
  module Resolvers
    class PendingSubscribableChanges < Resolvers::Base
      argument :listing_slug, String, "The slug of the Sponsors listing we want changes for.", required: false

      type Connections::PendingSubscribableChange, null: false

      def resolve(listing_slug: nil)
        changes = object.pending_subscription_item_changes.non_free_trial

        if listing_slug.present?
          sponsors_listing_promise = Platform::Loaders::ActiveRecord.load(::SponsorsListing, listing_slug, column: :slug)
          Promise.resolve(sponsors_listing_promise).then do |sponsors_listing|
            sponsors_tier_ids = SponsorsTier.where(sponsors_listing_id: sponsors_listing.id).pluck(:id)
            changes = changes.where(
              "subscribable_type = ? AND subscribable_id IN (?)",
              Billing::PendingSubscriptionItemChange.subscribable_types[SponsorsTier.name],
              sponsors_tier_ids,
            )
          end
        else
          changes
        end
      end
    end
  end
end
