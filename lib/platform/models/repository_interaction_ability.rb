# frozen_string_literal: true

class Platform::Models::RepositoryInteractionAbility
  attr_reader :interactable_type, :interactable

  def initialize(interactable_type, interactable)
    @interactable_type = interactable_type
    @interactable = interactable
  end

  def interaction_ability
    @interaction_ability ||= ::RepositoryInteractionAbility.new(@interactable_type, @interactable)
  end
end
