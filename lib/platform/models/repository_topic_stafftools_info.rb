# frozen_string_literal: true

class Platform::Models::RepositoryTopicStafftoolsInfo
  attr_reader :repo_topic

  def initialize(repo_topic)
    @repo_topic = repo_topic
  end

  def created_at
    repo_topic.created_at
  end
end
