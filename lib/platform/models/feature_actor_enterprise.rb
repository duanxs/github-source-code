# frozen_string_literal: true

module Platform
  module Models
    class FeatureActorEnterprise
      include GitHub::Relay::GlobalIdentification

      def initialize(business)
        @business = business
      end

      def id
        @business.id
      end

      def name
        @business.name
      end

      def slug
        @business.slug
      end

      def flipper_id
        @business.flipper_id
      end

      def primary_avatar_url(*args)
        @business.primary_avatar_url(*args)
      end
    end
  end
end
