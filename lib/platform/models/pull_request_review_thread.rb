# frozen_string_literal: true

class Platform::Models::PullRequestReviewThread
  include GitHub::Relay::GlobalIdentification

  # Return a new pull request review thread based on the given comment.
  #
  # comment    - The `PullRequestReviewComment` that starts this thread.
  # id_version - The version of the ID to be used for generating the global relay ID.
  #              See `Platform::Objects::PullRequestReviewThread.load_from_global_id` for details.
  #
  # Returns a `Platform::Models::PullRequestReviewThread`.
  def self.from_review_comments(comments, id_version: 2)
    first_comment = comments.first

    new(
      id: first_comment.pull_request_review_thread_id,
      position: first_comment.original_position,
      pull_request_id: first_comment.pull_request_id,
      repository_id: first_comment.repository_id,

      comments: comments,

      id_version: id_version,
    )
  end

  # Return a new pull request review thread based on the given attributes.
  #
  # id              - A Number representing this thread's id. Currently, this is
  #                   the same as the id of the first comment in a thread.
  # position        - A Number representing this thread's zero-based position in the diff text.
  #                   E.g. a comment left on third line of the diff text would have a position of `2`.
  # pull_request_id - A Number representing the associated pull request's id.
  # repository      - A Number representing the associated repository's id.
  # comments        - The `PullRequestReviewComment` that is the first comment in this thread.
  # id_version      - The version of the ID to be used for generating the global relay ID.
  #                   See `Platform::Objects::PullRequestReviewThread.load_from_global_id` for details.
  #
  # Returns a `Platform::Models::PullRequestReviewThread`.
  def initialize(id:, position:, pull_request_id:, repository_id:, comments:, id_version:)
    @id = id
    @id_version = id_version
    @position = position
    @pull_request_id = pull_request_id
    @repository_id = repository_id

    @first_comment = comments.first
    @comments = comments
  end

  attr_reader :id, :position, :pull_request_id, :repository_id, :comments

  def global_id
    ::PullRequestReviewThread.id_for(
      thread_id: id,
      first_comment_id: first_comment.id,
      version: @id_version,
    )
  end

  def created_at
    first_comment.created_at
  end

  def ==(other)
    super || (other.instance_of?(self.class) && !id.nil? && id == other.id)
  end
  alias_method :eql?, :==

  def hash
    if id
      self.class.hash ^ id.hash
    else
      super
    end
  end

  def async_to_activerecord
    first_comment.async_pull_request_review_thread
  end

  # Load and return this thread's associated pull request.
  #
  # Returns a Promise<PullRequest>.
  def async_pull_request
    return @async_pull_request if defined?(@async_pull_request)
    @async_pull_request = Platform::Loaders::ActiveRecord.load(PullRequest, pull_request_id)
  end

  # Load and return this thread's associated repository.
  #
  # Returns a Promise<Repository>.
  def async_repository
    return @async_repository if defined?(@async_repository)
    @async_repository = Platform::Loaders::ActiveRecord.load(Repository, repository_id)
  end

  # Determine whether the given viewer can see this thread.
  #
  # viewer - A User or nil for a anonymous viewer.
  #
  # Returns a Promise<Boolean>.
  def async_hide_from_user?(viewer)
    Promise.resolve(@comments.none? { |comment| comment.visible_to?(viewer) })
  end

  def async_review_comments_for(viewer)
    Platform::Loaders::PullRequestReviewCommentsForReviewThread.load(id, viewer)
  end

  def async_start_line
    @first_comment.async_start_line
  end

  def start_side
    @first_comment.start_side
  end

  def async_end_line
    @first_comment.async_end_line
  end

  def async_line
    @first_comment.async_line
  end

  # Determines whether the given viewer can reply to this thread.
  #
  # viewer - A User or nil for a anonymous viewer.
  #
  # Returns a Promise<Boolean>.
  def async_viewer_can_reply?(viewer)
    async_viewer_cannot_reply_reasons(viewer).then(&:empty?)
  end

  def async_viewer_cannot_reply_reasons(viewer)
    return Promise.resolve([:login_required]) unless viewer

    errors = []
    errors << :verified_email_required if viewer.must_verify_email?
    errors << :reply if first_comment.reply?

    async_original_pull_request_comparison.then do |pull_comparison|
      errors << :missing_objects unless pull_comparison

      async_repository.then do |repository|
        if repository.archived?
          errors << :archived
          errors
        else
          async_locked_for?(viewer).then do |locked|
            errors << :locked if locked
            errors
          end
        end
      end
    end
  end

  def async_locked_for?(viewer)
    async_pull_request.then(&:async_issue).then do |issue|
      issue.async_locked_for?(viewer)
    end
  end

  # Determines whether the pull request review thread is outdated.
  #
  # Returns a Boolean.
  def outdated?
    first_comment.outdated?
  end

  def async_collapsed?
    async_resolved?
  end

  def async_resolved?
    async_resolved_by_id.then do |resolver_id|
      resolver_id.present?
    end
  end

  def async_diff_entry
    @first_comment.async_diff_entry
  end

  def async_diff_lines(syntax_highlighted_diffs_enabled:, max_context_lines:)
    Promise.all([
      async_pull_request,
      async_pull_request.then(&:async_compare_repository),
      async_diff_entry,
      async_path,
    ]).then do |pull_request, repository, diff_entry, path|
      async_warm_syntax_highlighted_diff = if syntax_highlighted_diffs_enabled && diff_entry
        Platform::Loaders::WarmSyntaxHighlightedDiffCache.load(repository, pull_request, diff_entry)
      else
        Promise.resolve(nil)
      end

      async_warm_syntax_highlighted_diff.then do
        # TODO: Using a ViewModel in here is a bit icky, but is the simplest way forward. 😱
        # As soon as all references to `::Diff::ReviewCommentCodeLinesPartialView` are
        # removed from the view, we should revisit this and refactor it to be less of a hack.
        partial_view = ::Diff::ReviewCommentCodeLinesPartialView.new({
          current_user: nil, # This value is never used inside of `::Diff::ReviewCommentCodeLinesPartialView`
          repository: repository,

          excerpt_offset: first_comment.excerpt_starting_offset(max_context_lines),
          line_enumerator: first_comment.excerpt_line_enumerator(max_context_lines),
          anchor: "discussion-diff-#{id}",

          diff_entry: diff_entry,
          path: path,

          syntax_highlighted_diffs_enabled: syntax_highlighted_diffs_enabled,
          commentable: false,
          expandable: false,
          max_context_lines: max_context_lines,
        })

        lines = partial_view.line_enumerator.to_a

        lines.map do |line|
          {
            type: line.type,
            text: line.text,
            html: partial_view.highlighted_line(line),
            position: line.position,
            left: line.left == -1 ? nil : line.left,
            right: line.right == -1 ? nil : line.right,
            no_newline_at_end: line.nonewline?,
          }
        end
      end
    end
  end

  def diff_lines_truncated?
    first_comment.excerpt_truncated?
  end

  def async_discussion_diff_path_uri
    return @async_discussion_diff_path_uri if defined?(@async_discussion_diff_path_uri)

    @async_discussion_diff_path_uri = async_pull_request.then do |pull_request|
      pull_request.async_path_uri.then do |path_uri|
        path_uri = path_uri.dup
        path_uri.fragment = "discussion-diff-#{first_comment.id}"
        path_uri
      end
    end
  end

  def async_original_diff_path_uri
    first_comment.async_original_diff_path_uri
  end

  def async_current_diff_path_uri
    first_comment.async_current_diff_path_uri
  end

  def async_original_diff_file_path_uri
    return @async_original_diff_file_path_uri if defined?(@async_original_diff_file_path_uri)

    @async_original_diff_file_path_uri = async_original_diff_path_uri.then do |path_uri|
      next unless path_uri
      async_diff_file_path_uri(path_uri)
    end
  end

  def async_current_diff_file_path_uri
    return @async_current_diff_file_path_uri if defined?(@async_current_diff_file_path_uri)

    @async_current_diff_file_path_uri = async_current_diff_path_uri.then do |path_uri|
      next unless path_uri
      async_diff_file_path_uri(path_uri)
    end
  end

  def async_path
    async_position_data.then(&:path).then do |path|
      if path.respond_to?(:force_encoding) && path.encoding != ::Encoding::UTF_8
        path = path.dup
        path.force_encoding(::Encoding::UTF_8)
      end

      path
    end
  end

  def async_pull_request_commit
    Promise.all([
      first_comment.async_pull_request,
      async_position_data.then(&:commit_oid),
    ]).then do |pull_request, commit_oid|
      pull_request.async_load_pull_request_commit(commit_oid)
    end
  end

  # Public: Returns the _original_ end blob position.
  def async_original_line
    async_position_data.then(&:line)
  end

  # Public: Returns the _original_ start blob position (multi-line only).
  def async_original_start_line
    async_position_data.then(&:original_start_line)
  end

  def async_diff_side
    async_position_data.then(&:diff_side)
  end

  def async_resolved_by
    async_to_activerecord.then(&:async_resolver)
  end

  def async_resolved_by_id
    async_to_activerecord.then(&:resolver_id)
  end

  def async_can_resolve(viewer)
    can_change_resolve_state(viewer) do
      async_resolved_by_id.then do |resolver_id|
        resolver_id.blank?
      end
    end
  end

  def async_can_unresolve(viewer)
    can_change_resolve_state(viewer) do
      async_resolved_by_id.then do |resolver_id|
        resolver_id.present?
      end
    end
  end

  def platform_type_name
    "PullRequestReviewThread"
  end

  private

  def can_change_resolve_state(viewer)
    return Promise.resolve(false) if viewer&.spammy?

    async_to_activerecord.then do |thread|
      thread.async_pull_request_review.then do |review|
        if thread.published?
          async_pull_request.then do |pull|
            Promise.all([pull.async_user, pull.async_repository]).then do |pr_author, repo|
              repo.async_writable_by?(viewer).then do |repo_writable|
                if (pr_author && viewer == pr_author) || repo_writable
                  yield
                else
                  Promise.resolve(false)
                end
              end
            end
          end
        else
          Promise.resolve(false)
        end
      end
    end
  end

  attr_reader :first_comment

  def async_original_pull_request_comparison
    first_comment.async_original_pull_request_comparison
  end

  def async_position_data
    return @async_position_data if defined?(@async_position_data)
    @async_position_data = first_comment.async_position_data
  end

  def async_diff_file_path_uri(diff_path_uri)
    async_path_digest.then do |path_digest|
      diff_path_uri.dup.tap do |uri|
        uri.fragment = "diff-#{path_digest}"
      end
    end
  end

  def async_path_digest
    async_path.then do |path|
      Digest::MD5.hexdigest(path)
    end
  end
end
