# frozen_string_literal: true

class Platform::Models::Enterprise < SimpleDelegator
  attr_reader :token

  # business - A ::Business to delegate to.
  # token - A String representing a token for a ::BusinessMemberInvitation.
  def initialize(business, token)
    super(business)
    @token = token
  end

  def readable_by?(user)
    return super if token.nil?
    ::BusinessMemberInvitation.pending.where(hashed_token: token).exists?
  end
end
