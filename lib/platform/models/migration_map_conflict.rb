# frozen_string_literal: true

class Platform::Models::MigrationMapConflict
  attr_reader :model_type, :source_url, :target_url, :recommended_action, :notes

  def initialize(model_type, source_url, target_url, recommended_action, notes)
    @model_type = model_type
    @source_url = source_url
    @target_url = target_url
    @recommended_action = recommended_action
    @notes = notes.to_s
  end
end
