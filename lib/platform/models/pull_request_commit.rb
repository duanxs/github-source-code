# frozen_string_literal: true

class Platform::Models::PullRequestCommit
  include GitHub::Relay::GlobalIdentification
  extend Forwardable

  attr_reader :pull_request, :commit
  def_delegators :commit, :timeline_sort_by, :created_at

  def initialize(pull_request, commit)
    @pull_request = pull_request
    @commit = commit
  end

  def self.load_from_global_id(id)
    pull_id, commit_id = id.split(":", 2)

    if pull_id =~ /\d+/ && GitRPC::Util.valid_full_sha1?(commit_id)
      # New global ID format
      Platform::Loaders::ActiveRecord.load(::PullRequest, pull_id.to_i, security_violation_behaviour: :nil).then do |pull|
        pull.async_load_pull_request_commit(commit_id) if pull
      end
    else
      # Old global ID format

      # Support for old format can be dropped once this counter stays at 0
      GitHub.dogstats.increment("platform.deprecation.pull_request_commit_global_id")

      pull_id = Platform::Helpers::NodeIdentification.from_global_id(pull_id).last
      commit_id = Platform::Helpers::NodeIdentification.from_global_id(commit_id).last

      async_pull = Platform::Objects::PullRequest.load_from_global_id(pull_id)
      async_commit = Platform::Objects::Commit.load_from_global_id(commit_id)

      async_pull.then do |pull|
        async_commit.then do |commit|
          new(pull, commit)
        end
      end
    end
  end

  def self.id_for(pull_request, commit)
    "#{pull_request.id}:#{commit.oid}"
  end

  def id
    self.class.id_for(pull_request, commit)
  end

  # Used by GraphQL authorization checks
  def async_pull_request
    Promise.resolve(pull_request)
  end

  def ==(other)
    super || (other.instance_of?(self.class) && other.state == state)
  end
  alias_method :eql?, :==

  def hash
    state.hash
  end

  protected

  def state
    [pull_request, commit]
  end
end
