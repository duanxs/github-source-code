# frozen_string_literal: true

class Platform::Models::NotificationUnwatchSuggestion
  attr_reader :snapshot_date, :algorithm_version, :score

  def initialize(snapshot_date:, user_id:, repository_id:, algorithm_version:, score:)
    @snapshot_date = snapshot_date
    @user_id = user_id
    @repository_id = repository_id
    @algorithm_version = algorithm_version
    @score = score
  end

  def async_readable_by?(viewer)
    return Promise.resolve(false) unless viewer && viewer.id == @user_id

    async_repository.then do |repository|
      next false unless repository
      repository.async_readable_by?(viewer)
    end
  end

  def async_repository
    Platform::Loaders::ActiveRecord.load(Repository, @repository_id.to_i, security_violation_behaviour: :nil)
  end
end
