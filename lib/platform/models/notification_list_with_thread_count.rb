# frozen_string_literal: true

class Platform::Models::NotificationListWithThreadCount
  attr_reader :count, :unread_count, :user

  def initialize(list:, count:, unread_count:, user:)
    @list = list
    @count = count
    @unread_count = unread_count
    @user = user
  end

  def readable_by?(viewer)
    async_readable_by?(viewer).sync
  end

  def async_readable_by?(viewer, unauthorized_organization_ids: [])
    return Promise.resolve(false) unless user.id == viewer.id

    async_list.then do |list|
      next false unless list.present?

      Promise.all([
        async_in_unauthorized_organization?(list, unauthorized_organization_ids),
        list.async_readable_by?(viewer),
      ]).then do |(in_unauthorized_organization, readable)|
        next false if in_unauthorized_organization
        readable
      end
    end
  end

  def async_list
    Platform::Loaders::ActiveRecord.load(@list.class, @list.id, security_violation_behaviour: :nil)
  end

  private

  def async_in_unauthorized_organization?(list, unauthorized_organization_ids)
    return Promise.resolve(false) unless unauthorized_organization_ids.present?

    (list.try(:async_owner) || list.try(:async_organization)).then do |owner|
      next false unless owner.present?
      owner.organization? && unauthorized_organization_ids.include?(owner.id)
    end
  end
end
