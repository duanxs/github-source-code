# frozen_string_literal: true

module Platform
  module Models
    class FeatureActorFlipperId
      include GitHub::Relay::GlobalIdentification

      attr_reader :flipper_id

      def initialize(flipper_id)
        @flipper_id = flipper_id
      end

      def id
        @flipper_id
      end
    end
  end
end
