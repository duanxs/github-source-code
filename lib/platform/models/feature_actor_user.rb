# frozen_string_literal: true

module Platform
  module Models
    class FeatureActorUser
      include GitHub::Relay::GlobalIdentification

      def initialize(user)
        @user = user
      end

      def id
        @user.id
      end

      def login
        @user.login
      end

      def async_integration
        @user.async_integration
      end

      def primary_avatar_url(*args)
        @user.primary_avatar_url(*args)
      end

      def async_profile
        @user.async_profile
      end

      def flipper_id
        @user.flipper_id
      end
    end
  end
end
