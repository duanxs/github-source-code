# frozen_string_literal: true

class Platform::Models::EmailRoute
  attr_reader :organization, :settings

  def initialize(organization, settings)
    @organization = organization
    @settings = settings
  end
end
