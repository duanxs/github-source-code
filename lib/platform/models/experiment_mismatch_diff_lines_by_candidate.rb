# frozen_string_literal: true

class Platform::Models::ExperimentMismatchDiffLinesByCandidate
  attr_reader :name, :diff_lines

  def initialize(name:, diff_lines:)
    @name = name
    @diff_lines = diff_lines
  end

  def platform_type_name
    "ExperimentMismatchDiffLine"
  end
end
