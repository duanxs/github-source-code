# frozen_string_literal: true

module Platform
  module Models
    class FeatureActorApp
      include GitHub::Relay::GlobalIdentification

      def initialize(integration)
        @integration = integration
      end

      def id
        @integration.id
      end

      def name
        @integration.name
      end

      def slug
        @integration.slug
      end

      def async_owner
        @integration.async_owner
      end

      def flipper_id
        @integration.flipper_id
      end
    end
  end
end
