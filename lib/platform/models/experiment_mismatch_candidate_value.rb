# frozen_string_literal: true

class Platform::Models::ExperimentMismatchCandidateValue
  attr_reader :name, :value, :is_truncated, :exception

  def initialize(name:, value:, is_truncated:, exception:)
    @name = name
    @value = value
    @is_truncated = is_truncated
    @exception = exception
  end
end
