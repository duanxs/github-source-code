# frozen_string_literal: true

class Platform::Models::TopicStafftoolsInfo
  attr_reader :topic

  def initialize(topic)
    @topic = topic
  end
end
