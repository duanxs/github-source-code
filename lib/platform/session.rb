# frozen_string_literal: true

module Platform
  module Session

    def run(query, context)
      last_operations = setup_last_operations(context)
      blk = proc { query.result }

      if readonly_forced?(context)
        ActiveRecord::Base.connected_to(role: :reading, &blk)
      elsif query.mutation?
        ActiveRecord::Base.connected_to(role: :writing, &blk)
      elsif readonly_primary_forced?(context)
        ActiveRecord::Base.connected_to(role: :writing, prevent_writes: true, &blk)
      else
        ::DatabaseSelector.instance.read_from_database(last_operations: last_operations, called_from: :platform_session, &blk)
      end
    ensure
      if query.mutation? && !readonly_forced?(context)
        last_operations.update_last_write_timestamp
      end
    end
    module_function :run

    def self.readonly_forced?(context)
      context[:force_readonly]
    end

    def self.readonly_primary_forced?(context)
      context[:force_readonly_primary]
    end

    def self.setup_last_operations(context)
      if (request_token = context[:request_token])
        DatabaseSelector::LastOperations.from_token(request_token)
      elsif (session = context[:session])
        DatabaseSelector::LastOperations.from_session(session)
      end
    end
  end
end
