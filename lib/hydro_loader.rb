# frozen_string_literal: true

class HydroLoader
  def self.load_github
    return if GitHub.hydro_initialized?

    Hydro.logger = Rails.logger
    Hydro.load_schemas(Rails.root.join("lib/hydro"))

    Hydro::DatadogReporter.start(
      dogstatsd: GitHub.dogstats,
      client_id: GitHub.hydro_metrics_namespace,
    )

    ##
    ## Add hydro subscriptions below
    ##

    # Subscriptions using GitHub as the EventForwarder source
    require_relative "../config/instrumentation/hydro/subscriptions/github"

    # Uncategorized subscriptions using GlobalInstrumenter as the EventForwarder source
    require_relative "../config/instrumentation/hydro/subscriptions/kitchen_sink"

    # Begin categorized subscriptions
    require_relative "../config/instrumentation/hydro/subscriptions/advisory_credits"
    require_relative "../config/instrumentation/hydro/subscriptions/billing"
    require_relative "../config/instrumentation/hydro/subscriptions/discussions"
    require_relative "../config/instrumentation/hydro/subscriptions/email_domains"
    require_relative "../config/instrumentation/hydro/subscriptions/enterprise_accounts"
    require_relative "../config/instrumentation/hydro/subscriptions/explore"
    require_relative "../config/instrumentation/hydro/subscriptions/ip_allow_lists"
    require_relative "../config/instrumentation/hydro/subscriptions/nux"
    require_relative "../config/instrumentation/hydro/subscriptions/project_card"
    require_relative "../config/instrumentation/hydro/subscriptions/repository"
    require_relative "../config/instrumentation/hydro/subscriptions/search"
    require_relative "../config/instrumentation/hydro/subscriptions/signup"
    require_relative "../config/instrumentation/hydro/subscriptions/sponsors"
    require_relative "../config/instrumentation/hydro/subscriptions/optimizely"
    require_relative "../config/instrumentation/hydro/subscriptions/code_scanning"
    require_relative "../config/instrumentation/hydro/subscriptions/codespaces"
    require_relative "../config/instrumentation/hydro/subscriptions/trade_controls"
    require_relative "../config/instrumentation/hydro/subscriptions/notifications"
    require_relative "../config/instrumentation/hydro/subscriptions/mobile"
    require_relative "../config/instrumentation/hydro/subscriptions/user"

    GitHub.hydro_initialized = true
  end
end
