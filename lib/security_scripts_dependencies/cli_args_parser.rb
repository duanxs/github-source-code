# frozen_string_literal: true

require "slop"

module SecurityScriptsDependencies
  class CLIARGSParser

    attr_reader :options_hash

    def initialize(args)
      options = Slop.parse(args, strict: true) do
        on "c=", "csv", "Path to csv with user_id column and optional other columns", required: true
        on "p=", "pat", "A personal access token for the user sending the notification", required: true
        on "r=", "reason", "The reason for this notification", required: true
        on "d", "dry_run", "Don't save or update any data, just log changes"
        on "v", "verbose", "Log verbose output"
        on "n", "send_notification", "send notification to user"

        send_notification =  args.include?("-n") || args.include?("-send_notification") || args.include?("--send_notification")
        on "s=", "subject", "Incident notification email subject (required if -n or -send_notification)", required: send_notification
        on "t=", "template", "Path to incident notification mustache template (required if -n or -send_notification)", required: send_notification
        on "f=", "from", "Reply to email address for this notification (required if -n or -send_notification)", required: send_notification
      end
      @options_hash = options.to_h
    rescue Slop::Error => error
      puts "Passed in args:"
      puts args
      raise error
    end

    def options_to_string
      <<~MESSAGE
        ```
        csv_path: #{options_hash[:csv].inspect}
        reason: #{options_hash[:reason].inspect}
        dry_run: #{options_hash[:dry_run] ? 'true' : 'false'}
        verbose: #{options_hash[:verbose] ? 'true' : 'false'}
        send_notification: #{options_hash[:send_notification] ? 'true' : 'false'}
        template_path: #{options_hash[:template].inspect}
        from_email: #{options_hash[:from].inspect}
        subject: #{options_hash[:subject].inspect}
        ```
      MESSAGE
    end
  end
end
