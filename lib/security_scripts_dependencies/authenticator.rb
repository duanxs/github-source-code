# frozen_string_literal: true

module SecurityScriptsDependencies
  class Authenticator
    class AuthenticationError < StandardError; end

    def self.authenticate_user!(from:, pat:)
      attempt = GitHub::Authentication::Attempt.new(from: from, token: pat)
      result = attempt.result

      raise AuthenticationError.new("Failed authentication attempt") unless result.success?
      raise AuthenticationError.new("Not authorized") unless result.user.employee?

      result.user
    end
  end
end
