# frozen_string_literal: true

module GitHub
  module TaggingHelper
    EMPTY_STRING = ""
    SLASH = "/"
    UNDERSCORE = "_"
    RAILS_VERSION_TAG = "rails_version"
    CATEGORY_TAG = "category"
    PJAX_TAG = "pjax"
    LOGGED_IN_TAG = "logged_in"
    ROBOT_TAG = "robot_name"
    CONTROLLER_TAG = "controller"
    ACTION_TAG = "action"
    STATUS_TAG = "status"
    STATUS_RANGE_TAG = "status_range"
    METHOD_TAG = "method"
    COMPONENT_TAG = "component"
    OWNER_TAG = "owner"
    REPOSITORY_TAG = "repository"
    PURPOSE_TAG = "purpose"
    STAFFTOOLS_TAG = "stafftools"
    GRAPHQL_API_TAG = "graphql_api"
    AOR_TAG = "aor"
    SERVICE_TAG = "catalog_service"

    RAILS_DEFAULT = "rails"
    RAILS_PARAMETERS_KEY = "action_dispatch.request.path_parameters"
    API_CONTROLLER_KEY = "process.api.controller"
    SINATRA_ROUTE_ENV_KEY = "sinatra.route"
    GITHUB_API_ROUTE_ENV_KEY = "github.api.route"
    PROCESS_REQUEST_CATEGORY = "process.request_category"
    PROCESS_REQUEST_CATEGORY_DATADOG = "process.request_category_datadog"
    DATADOG_BROWSER_CATEGORY = "browser"
    STATSD_SAMPLE_RATE = "process.metrics.sample_rate"
    STATSD_TAG_ACTIONS = "process.metrics.tag_actions"
    PROCESS_AOR_KEY = "process.areas_of_responsibility"
    PROCESS_SERVICE_KEY = "process.catalog_service"
    PROCESS_REQUEST_PJAX = "process.request_pjax"
    PROCESS_REQUEST_LOGGED_IN = "process.request_logged_in"
    CATEGORY_DEFAULT = "other"
    PJAX_DEFAULT = LOGGED_IN_DEFAULT = "false"
    DATADOG_ROBOT_CATEGORY = "robot"
    PROCESS_REQUEST_ROBOT_NAME = "process.request_robot_name"
    REQ_WAIT_TIME = "request_wait_time"
    UNKNOWN = "unknown"
    QUOTA_LIMITING_ENABLED = "quota_limiting_enabled"

    TRACKED_REPOSITORIES = %w(
      github/github
      shopify/shopify
      medium/mono
    ).freeze

    TRACKED_REPOSITORIES_REGEX = Regexp.new(
      "^(" + TRACKED_REPOSITORIES.map { |nwo| Regexp.escape(nwo) }.join("|") + ")$",
      Regexp::IGNORECASE,
    )

    def self.rails_version
      rails_version = GitHub.rails_version_key
      rails_version ||= RAILS_DEFAULT
    end

    def self.category(env)
      category = env[PROCESS_REQUEST_CATEGORY_DATADOG]
      category ||= CATEGORY_DEFAULT
    end

    def self.pjax(env)
      pjax = env[PROCESS_REQUEST_PJAX]
      pjax ||= UNKNOWN
    end

    def self.logged_in(env)
      logged_in = env[PROCESS_REQUEST_LOGGED_IN]
      logged_in ||= UNKNOWN
    end

    def self.robot_name(env)
      robot_name = env[PROCESS_REQUEST_ROBOT_NAME]
      robot_name ||= UNKNOWN
    end

    def self.purpose(env)
      purpose = env["HTTP_X_REQUEST_PURPOSE"]
      purpose ||= UNKNOWN
    end

    def self.controller(env)
      controller = path_parameters(env).try(:[], :controller)
      return formatted_controller(controller) if controller

      controller = api_parameters(env).try(:[], :app)
      return formatted_controller(controller) if controller
    end

    def self.formatted_controller(controller_name)
      controller_name.to_s.underscore.gsub(SLASH, UNDERSCORE).sub(/_controller\Z/, EMPTY_STRING)
    end

    def self.hook_event_type_tag_value(event_type)
      "hook/#{event_type}"
    end

    def self.hook_event_tag_value(event_type, action = nil)
      event_type = hook_event_type_tag_value(event_type)
      return event_type unless action
      "#{event_type}_#{action}"
    end

    def self.create_hook_event_tags(event_type, action = nil)
      [
        "event:#{hook_event_tag_value(event_type, action)}",
        "event_type:#{hook_event_type_tag_value(event_type)}",
      ]
    end

    def self.action(env)
      action = path_parameters(env).try(:[], :action)
      return action if action

      action = api_parameters(env).try(:[], :route)
      return nil unless action

      # API actions come in the form GET /emojis
      _req_method, path = action.split(" ", 2)

      return path if path

      # When the action comes from RepoPermissions, it comes preformatted without
      # the `GET /` part of the path, so we can just return it
      action
    end

    def self.owner(env)
      path_parameters(env).try(:[], :user_id)
    end

    def self.repository(env)
      path_parameters(env).try(:[], :repository)
    end

    def self.repository_tracking_enabled?(owner, repository)
      TRACKED_REPOSITORIES_REGEX.match?("#{owner}/#{repository}")
    end

    def self.path_parameters(env)
      env[RAILS_PARAMETERS_KEY]
    end

    def self.api_parameters(env)
      app = env[API_CONTROLLER_KEY]
      route = env[SINATRA_ROUTE_ENV_KEY] || env[GITHUB_API_ROUTE_ENV_KEY]
      return nil unless app && route
      { app: app, route: route }
    end

    def self.quota_limiting_enabled?(env)
      env[QUOTA_LIMITING_ENABLED]
    end

    def self.stafftools_request?(controller)
      controller =~ /stafftools/
    end

    def self.graphql_api_request?(controller)
      controller == "api_graph_ql"
    end
  end
end
