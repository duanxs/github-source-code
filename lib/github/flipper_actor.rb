# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module FlipperActor
    # Private: Used to server as a separator between the class name and the id for
    # flipper actors (ie: User:1).
    FLIPPER_ID_SEPARATOR = ":".freeze
    # Use lookahead/lookbehind to allow class names on the right hand side of
    # the separator:
    FLIPPER_ID_PATTERN = /(?<!:):(?!:)/

    # Public: Given a flipper_id converts it to a github domain instance
    # (ie: "User:235" => User.find_by_id(235)).
    def self.from_flipper_id(flipper_id)
      model_name, id = flipper_id_to_parts(flipper_id)
      model_name.constantize.find_by_id(id)
    end

    # Public: Given a flipper_id it returns the class name
    # (ie: "User:235" => "User").
    def self.class_name_from_flipper_id(flipper_id)
      flipper_id_to_parts(flipper_id).first
    end

    # Public: Returns the class name and id when given a flipper_id
    # (ie: "User:235" => ["User", "235"]).
    def self.flipper_id_to_parts(flipper_id)
      unless FLIPPER_ID_PATTERN =~ flipper_id
        raise ArgumentError, "flipper_id #{flipper_id} must include #{FLIPPER_ID_SEPARATOR}"
      end

      class_name, _, id = flipper_id.rpartition(FLIPPER_ID_PATTERN)
      [class_name, id]
    end

    # Public: enable a feature for this actor
    #
    # Returns result of Feature::DSL#enable_actor
    def enable_feature(feature_name)
      ActiveRecord::Base.connected_to(role: :writing) do
        GitHub.flipper[feature_name].enable_actor(self)
      end
    end

    # Public: disable a feature for this actor
    #
    # Returns result of Feature::DSL#enable_actor
    def disable_feature(feature_name)
      ActiveRecord::Base.connected_to(role: :writing) do
        GitHub.flipper[feature_name].disable_actor(self)
      end
    end

    # Internal: The ID stored by Flipper for this actor to enable per-actor or
    # percent-of-actors enabling of features.
    def flipper_id
      "#{self.class.name}#{FLIPPER_ID_SEPARATOR}#{id}"
    end
  end
end
