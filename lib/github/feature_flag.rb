# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module FeatureFlag
    FLIPPER_USAGE_PATTERNS = [
      /GitHub\.flipper\[:(\w+)\]/,
      /GitHub::flipper\[:(\w+)\]/,
      /GitHub\.flipper\.enable\(:(\w+).*\)/,
      /GitHub\.flipper\.disable\(:(\w+).*\)/,
      /GitHub\.flipper\.enabled\?\(:(\w+).*\)/,
      /feature_enabled\?[\( ]:(\w+)/,
      /\.try\(:feature_enabled\?, :(\w+)\)/,
      /current_user_feature_enabled\?[\( ]:(\w+)/,
      /prerelease_feature_enabled\?[\( ]:(\w+)/,
      /feature_flag\ :(\w+)/,
      /feature_enabled_globally_or_for_current_user\?[\(\s]?:(\w+)\)?/,
      /feature_enabled_for_user_or_current_visitor\?[\(\s]feature_name:\s?:(\w+)\)?/,
      /feature_enabled_globally_or_for_user\?[\(\s]feature_name:\s?:(\w+)\)?/,
      /enabled_groups:\s.:(\w+)\)?/,
      /beta_feature_enabled\?\(:(\w+)/,
      /@featureFlagged\(flag: "(\w+)"/,
    ].freeze

    # Internal: Loads and memoizes the GitHub Employees Team.
    #
    # Returns a Team or false if it doesn't exist.
    def employees_team
      team_cache[["github", "employees"]]
    end

    # Internal: Loads and memoizes the @GitHubBounty/bounty-hunters team.
    #
    # Returns a Team or false if it doesn't exist.
    def bounty_hunters_team
      team_cache[["GitHubBounty", "Bounty Hunters"]]
    end

    # Internal: Loads and memoizes the campus-experts/campus-experts-badge team.
    #
    # Returns a Team or false if it doesn't exist.
    def campus_experts_badge_team
      team_cache[["campus-experts", "campus-experts-badge"]]
    end

    # Internal: Loads and memoizes the GitHub Abilities Team.
    #
    # Returns a Team or false if it doesn't exist.
    def abilities_team
      team_cache[["github", "abilities"]]
    end

    # Internal: Loads and memoizes the Stafftools Team.
    #
    # Returns a Team or false if it doesn't exist.
    def stafftools_team
      team_cache[["github", "stafftools"]]
    end

    # Internal: Loads and memoizes the User Security Team.
    #
    # Returns a Team or false if it doesn't exist.
    def user_security_team
      team_cache[["github", "user-security"]]
    end

    # Internal: Loads and memoizes the obscured GitHub Enterprise Preview Features team.
    #
    # Returns a Team or false if it doesn't exist.
    def enterprise_preview_features_team
      team_cache[["github", "sekret-enterprise-features-Gvr6pqN"]]
    end

    # Internal: Loads and memoizes the Maintainers program features early access team.
    #
    # Returns a Team or false if it doesn't exist.
    def maintainers_early_access_team
      team_cache[["maintainers", "Early Access"]]
    end

    # Internal: Loads and memoizes the Project Moonstar features early access team.
    #
    # Returns a Team or false if it doesn't exist.
    def integrators_early_access_team
      team_cache[["project-moonstar", "all-integrators"]]
    end

    # Internal: Loads and memoizes the Interns team.
    #
    # Returns a Team or false if it doesn't exist.
    def interns_team
      team_cache[["github", "interns"]]
    end

    # Internal: Loads and memoizes the Platform Health team.
    #
    # Returns a Team or false if it doesn't exist.
    def platform_health_team
      team_cache[["github", "Platform Health"]]
    end

    # Internal: Loads and memoizes the SIRT team.
    #
    # Returns a Team or false if it doesn't exist.
    def sirt_team
      team_cache[["github", "SIRT"]]
    end

    # Internal: Checks to see if the User has access to the Team.
    #
    # team_name - The Symbol name of the team. There should be a coresponding
    #             `#{team_name}_team` method in this module.
    # user      - The User.
    #
    # Returns a Boolean.
    def user_team_access?(team_name, user)
      return false unless team = send("#{team_name}_team")

      ActiveRecord::Base.connected_to(role: :reading) do
        Ability.unscoped { team.member?(user) }
      end
    end

    # Cached org-name/team-name to Team object mapping.
    #
    # Accessing this Hash will lookup and cache teams.
    #
    # Eg. `team_cache[["github", "employees"]]` will lookup and cache the
    # @github/employees team. False will be returned if the team doesn't exist.
    #
    # This cache can be cleared like a normal Hash.
    #
    # Eg. `team_cache.clear` will clear cached entries.
    #
    # Returns a Hash.
    def team_cache
      @team_cache ||= Hash.new do |hash, (org_name, team_name)|
        hash[[org_name, team_name]] = begin
          ActiveRecord::Base.connected_to(role: :reading) do
            Organization.unscoped do
              Team.unscoped do
                org = Organization.find_by_login(org_name)
                team = org.teams.find_by_name(team_name) if org

                team || false
              end
            end
          end
        end
      end
    end

    extend self
  end
end
