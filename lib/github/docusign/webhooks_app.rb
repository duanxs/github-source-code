# frozen_string_literal: true

module GitHub
  module Docusign
    class WebhooksApp
      HEADERS = { "Content-Type" => "text/plain" }
      EMPTY_RESPONSE = []
      DOCUSIGN_ALLOWED_IPS = ["209.112.104.1/24", "64.207.216.1/22", "162.248.184.1/22"]

      def self.call(env)
        new(env).call
      end

      attr_reader :request

      def initialize(env)
        @env = env
        @request = Rack::Request.new(env)
      end

      def call
        return method_not_allowed_response unless request.post?
        return not_found_response unless valid_ip?

        ::Docusign::WebhookHandler.handle(
          payload: payload,
          signature: signature,
        )
        return ok_response
      rescue ActiveRecord::RecordNotUnique => e
        GitHub.dogstats.increment("docusign.webhook.duplicate")
        return ok_response
      rescue ::Docusign::WebhookHandler::InvalidSignature,
             ::Docusign::WebhookHandler::PayloadParseError => e
        Failbot.report(e)
        return bad_request_response
      end

      private

      def bad_request_response
        [400, HEADERS, EMPTY_RESPONSE]
      end

      def method_not_allowed_response
        [405, HEADERS, EMPTY_RESPONSE]
      end

      def ok_response
        [200, HEADERS, EMPTY_RESPONSE]
      end

      def not_found_response
        [404, HEADERS, EMPTY_RESPONSE]
      end

      def payload
        @payload ||= request.body.read
      end

      def signature
        @signature ||= request.get_header("HTTP_X_DOCUSIGN_SIGNATURE_1")
      end

      def valid_ip?
        is_valid = DOCUSIGN_ALLOWED_IPS.any? do |cidr|
          IPAddr.new(cidr).include?(request.ip)
        end

        log_invalid_ip unless is_valid

        is_valid
      end

      def log_invalid_ip
        GitHub.dogstats.increment("docusign.webhook", tags: ["source:sponsors", "action:invalid_webhook_ip"])

        sanitized_env = request.env.dup
        sanitized_env["HTTP_X_DOCUSIGN_SIGNATURE_1"] = "[FILTERED]"
        GitHub::Logger.log(at: "docusign.webhook", request: sanitized_env)
      end
    end
  end
end
