# frozen_string_literal: true

require "github/sql/subscriber"
require "github/sql/digester"

module GitHub
  module MysqlInstrumenter
    RAILS_ROOT_PATH = "#{Rails.root.realpath}/".freeze
    RAILS_ROOT_REGEXP = /^#{Regexp.escape RAILS_ROOT_PATH}(app|lib)/

    extend GitHub::SQL::Digester

    class Query
      attr_reader :sql, :digested_sql, :start_time, :duration, :backtrace, :result_count, :connection_url, :connected_host, :tags, :on_primary

      def initialize(sql:, result_count:, duration:, connection_url:, connected_host:, on_primary:)
        @sql = sql
        @digested_sql = GitHub::SQL::Digester.digest_sql(@sql)
        @result_count = result_count
        @start_time = Time.now - MysqlInstrumenter.start
        @duration = duration
        @backtrace = MysqlInstrumenter.backtrace_locations
        @connection_url = connection_url
        @connected_host = connected_host
        @on_primary = on_primary
        @tags = MysqlInstrumenter.tags.uniq

        freeze
      end
    end

    class << self
      attr_accessor :start, :queries, :query_time, :query_count, :query_counts, :primary_query_count

      def tag_queries(*tags)
        self.tags.concat(tags)
        yield
      ensure
        self.tags.pop(tags.size)
      end

      def transaction_times
        @transaction_times ||= []
      end

      def tags
        @tags ||= []
      end

      def add_transaction_time(transaction_time)
        transaction_times << transaction_time
      end

      def reset_stats
        @start = Time.now
        @query_count = 0
        @primary_query_count = 0
        @query_time = 0
        @queries = []
        @query_counts = Hash.new(0)
        @tracking = false
        @transaction_times = []
        @tags = []
      end

      def track_query(sql, result, connection_info, time_span:)
        on_primary = ActiveRecord::Base.connected_to?(role: :writing)
        diff = time_span.duration_seconds
        @query_time  += diff
        @query_count += 1
        @primary_query_count += 1 if on_primary

        GitHub::SQL::Subscriber.call(sql, connection_info, time_span: time_span)

        if @tracking
          query = Query.new(sql: sql,
                            result_count: result && result.count,
                            duration: diff,
                            connection_url: connection_info.url,
                            connected_host: connection_info.host,
                            on_primary: on_primary,
                           )
          @queries << query

          @query_counts[query.digested_sql] += 1
        end
      end

      def backtrace_locations
        caller_locations(2)
          .select { |f| f.absolute_path =~ RAILS_ROOT_REGEXP }
          .drop_while { |f| !Rollup.significant?(f.absolute_path) }
      end

      def track!
        @tracking = true
      end

      def untrack!
        @tracking = false
      end

      def tracking?
        @tracking
      end

      def with_track(&block)
        old_tracking = tracking?
        begin
          track!
          block.call
        ensure
          old_tracking ? track! : untrack!
        end
      end
    end
    self.start = Time.now
    self.query_count = 0
    self.primary_query_count = 0
    self.query_time = 0
    self.queries = []
    self.query_counts = Hash.new(0)
  end
end
