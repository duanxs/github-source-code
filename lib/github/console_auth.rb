# rubocop:disable Style/FrozenStringLiteralComment
require "io/console"
require "failbot"
require "readline_ext"

class ConsoleAuth

  class ConsoleAuthorizationError < StandardError
  end

  class Session
    attr_accessor :console_user
    attr_accessor :session_id

    def self.session_id
      @session_id ||= "#{Time.now.to_i}"
    end

    def self.user(name)
      @console_user ||= name
    end

    def self.hrt
      Time.now.utc
    end
  end

  class << self

    def gatekeeper(attempts_remaining = 3)
      return if GitHub.enterprise?
      return unless Rails.env.production?

      if (attempts_remaining == 0)
        warn("Too many failed attempts. All error messages have been recorded to Haystack. If you need assistance, please review https://githubber.com/article/crafts/engineering/support/rails-console-guide.")
        return exit
      end

      username = get_username(attempts_remaining)
      password = get_password
      prompt("You should receive a duo push.")

      response = vault_auth(username, password, attempts_remaining)
      return gatekeeper(attempts_remaining - 1) unless response

      if access_control(JSON.parse(response.body).dig("auth", "policies"))
        Session.user(username)
        # Initialize session logging
        Readline::History.start_session_log
      else
        Failbot.report(ConsoleAuthorizationError.new("User is not a member of a group with console access."), user: username)
        warn("Sorry, you are not authorized to access the console.\n If you need access please read https://githubber.com/article/crafts/engineering/support/rails-console-guide#logging-in-to-the-console")
        return exit
      end
    end

    def get_username(attempts_remaining)

      if (attempts_remaining == 0)
        Failbot.report(ConsoleAuthorizationError.new("Too many attempts. Invalid username."))
        return gatekeeper(0)
      end

      prompt("Please enter your username:")
      uname = username_input

      unless valid_username(uname)
        warn("Valid usernames can only contain alphanumeric characters, underscores, and dashes.")
        return get_username(attempts_remaining - 1)
      end
      uname
    end

    def username_input
      gets.strip
    end

    def get_password
      prompt("Please enter your dotcom password:")
      STDIN.noecho(&:gets).strip
    end

    def warn(message)
      puts "\033[39;41m#{message}\033[0m"
    end

    def prompt(message)
      puts "\033[93m#{message}\033[0m"
    end

    def vault_auth(username, password, attempts_remaining)
      uri = URI "https://vault.service.github.net:8200/v1/auth/ldap/login/#{username}"
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_PEER
      # FIXME: Temporary solution until kubernetes containers recognize the same CAs as
      # our systems do.
      http.ca_file = ca_file
      begin
        response = http.post(uri, {"password" => password}.to_json)
        if response&.code == "200"
          response
        else
          # report exact error returned when authentication fails
          error = JSON.parse(response.body)["errors"]
          Failbot.report(ConsoleAuthorizationError.new(error), status: response.code, user: username)
          warn("Sorry those credentials were not accepted.")
          warn error
          return
        end
      rescue
        # Rescue from any connectivity, or socket errors which may occur.
        error = $!
        Failbot.report(ConsoleAuthorizationError.new(error), user: username)
        warn("An error has occurred with this request")
        warn(error)
        return
      end
    end

    private

    def valid_username(username)
      !username.empty? && username.scan(/[^a-zA-Z\d\-\_]/).empty?
    end

    def access_control(policies)
      policies.include?("ldap-misc-app-gh-console")
    end

    def ca_file
      # FIXME: Temporary solution until kubernetes containers recognize the same CAs as
      # our systems do.
      ENV["VAULT_CA_FILE"] || Rails.root.join("config/console/vault_ca.crt").to_s
    end
  end
end
