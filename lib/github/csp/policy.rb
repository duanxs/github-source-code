# rubocop:disable Style/FrozenStringLiteralComment

#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#                       Seriously, CC @github/prodsec and @github/dotcom-security
#                       if you need to touch this file
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

module GitHub::CSP::Policy
  # Public: File for defining CSP security directives for the Content Security
  # Policy header.
  #
  # Maintainers
  # - @josh
  # - @github/dotcom-security
  #

  # CDN host source.
  #
  # Return 'self' unless an asset host, on a different subdomain, is
  # configured.
  if GitHub.asset_host_url.blank? || !GitHub.subdomain_isolation?
    CDN_SOURCE = "'self'"
  else
    CDN_SOURCE = GitHub.asset_host_url
  end

  # Render host source.
  #
  # Return 'self' unless a render host, on a different subdomain, is
  # configured.
  if GitHub.render_host.blank? || !GitHub.subdomain_isolation?
    RENDER_SOURCE = "'self'"
  else
    RENDER_SOURCE = GitHub.render_host
  end

  DEFAULT_SOURCES = [SecureHeaders::CSP::NONE].freeze

  # Allowed script URL whitelist.
  #
  # All hosts that we load scripts from needs to be part of this list.
  SCRIPT_SOURCES = [CDN_SOURCE].freeze

  # Allowed object URL whitelist.
  #
  # All hosts that we load objects from needs to be part of this list.
  OBJECT_SOURCES = [
    # No <object>, <embed>, or <applet> allowed.
  ].freeze

  # Allowed style URL whitelist.
  #
  # All hosts that we load styles from needs to be part of this list.
  STYLE_SOURCES = [
    SecureHeaders::CSP::UNSAFE_INLINE,
    CDN_SOURCE,
  ].freeze

  # Allowed image URL whitelist.
  #
  # All hosts that we load images from needs to be part of this list.
  if GitHub.restrict_external_images?
    IMG_SOURCES = [
      SecureHeaders::CSP::SELF,
      SecureHeaders::CSP::DATA_PROTOCOL,
      CDN_SOURCE,
      GitHub.alambic_assets_host,
      GitHub.storage_cluster_host,
      GitHub.image_proxy_url,
      GitHub.identicons_host,
      GitHub.octolytics_collector_url,
      *GitHub.alambic_avatar_urls,
      GitHub.s3_asset_bucket_host,
    ].freeze
  else
    IMG_SOURCES = ["*", SecureHeaders::CSP::DATA_PROTOCOL].freeze
  end

  def self.dynamic_img_sources
    if GitHub.restrict_external_images?
      [
        GitHub.urls.user_content_host_wildcard,
      ]
    else
      []
    end
  end

  # Allowed media URL whitelist.
  #
  # All hosts that we load audio and video from needs to be part of this list.
  MEDIA_SOURCES = [
    SecureHeaders::CSP::NONE,
  ].freeze

  # Allowed frame URL whitelist.
  #
  # All hosts that we load iframes for need to be part of this list.
  FRAME_SOURCES = [
    # Rendering Pages previews
    RENDER_SOURCE,
  ].flatten.freeze

  # Allowed font URL whitelist.
  #
  # All hosts that we load fonts needs to be part of this list.
  FONT_SOURCES = [
    CDN_SOURCE,
  ].freeze

  # Third party connect sources.
  #
  # We allow a limited set of external connects on dotcom.
  # Also required in cluster mode on enterprise (Enterprise 2.4) to connect
  # to an external AWS S3 bucket URL.
  if GitHub.allow_third_party_connect_sources?
    # Enterprise clusters configure this via env variables and only needs a single
    # source (String).
    THIRD_PARTY_CONNECT_SOURCES = Array(GitHub.third_party_connect_sources)
  else
    THIRD_PARTY_CONNECT_SOURCES = [].freeze
  end

  # Allowed connect URL whitelist.
  #
  # All hosts that we make XHR requests to needs to be part of this list.
  CONNECT_SOURCES = [
    SecureHeaders::CSP::SELF,
    GitHub.alambic_csp_host,
    GitHub.storage_cluster_host,
    GitHub.site_status_url,
    GitHub.octolytics_collector_url,
    GitHub.livereload_url,

    # Only paths whitelisted in Api::App::Cors are allowed from GitHub.com.
    GitHub.api_host_name,

    THIRD_PARTY_CONNECT_SOURCES,
  ].flatten.compact.freeze

  def self.dynamic_connect_src
    [GitHub.urls.alive_ws_url]
  end

  # Allowed base URIs.
  #
  # The base should always be self.
  BASE_URI_SOURCES = [
    SecureHeaders::CSP::SELF,
  ].freeze

  # Allowed form action="" URL whitelist.
  #
  # All hosts that we post form requests to need to be part of this list.
  FORM_ACTIONS = [
    SecureHeaders::CSP::SELF,
    GitHub.host_name,
    GitHub.gist3_host_name,
    GitHub.auth.cas? ? GitHub.cas_host : nil, # Needed for CAS logout.
  ].flatten.freeze

  PLUGIN_TYPES = [
    # No plugins allowed.
  ].freeze

  FRAME_ANCESTORS = [
    SecureHeaders::CSP::NONE,
  ].freeze

  MANIFEST_SOURCES = [
    SecureHeaders::CSP::SELF,
  ].freeze

  WORKER_SOURCES = [
    "#{GitHub.host_name}/socket-worker.js",
    "#{GitHub.gist3_host_name}/socket-worker.js",
    # If we have a separate admin host and we're on a node serving that domain,
    # we also allow this in the worker-src. This is so that we don't expose this
    # domain existing outside of when it is configured.
    GitHub.admin_host_name && GitHub.role == :stafftools ? "#{GitHub.admin_host_name}/socket-worker.js" : nil,
  ].compact.freeze

end
