# frozen_string_literal: true

module GitHub
  module ParallelRpc
    def parallel_rpc(fileservers, command, args, options = {})
      routes = fileservers.map { |fs| fs.to_route("/") }
      route_urls = Hash[routes.map { |route| [route.rpc_url, route] }]

      answers, errors = ::GitRPC::send_multiple(route_urls.keys, command, args, options)

      answers = Hash[answers.map { |url, answer| [route_urls[url].original_host, answer] }]
      errors = Hash[errors.map { |url, error| [route_urls[url].original_host, error] }]

      [answers, errors]
    end
  end
end
