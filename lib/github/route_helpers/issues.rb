# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module RouteHelpers
    def gh_issues_path(repo)
      expand_nwo_from :issues_path, repo
    end

    def gh_issue_path(issue)
      expand_nwo_from :issue_path, issue
    end

    def gh_close_issue_path(issue)
      expand_nwo_from :close_issue_path, issue
    end

    def gh_open_issue_path(issue)
      expand_nwo_from :open_issue_path, issue
    end

    def gh_labels_path(repo)
      expand_nwo_from :labels_path, repo
    end

    def gh_label_path(label)
      "/#{label.repository.name_with_owner}/labels/#{Addressable::URI.encode_component(label.name, Addressable::URI::CharacterClasses::UNRESERVED)}"
    end

    def gh_milestone_path(milestone)
      milestone_path(milestone.repository.owner, milestone.repository, milestone)
    end

    def gh_milestone_issue_search_path(milestone)
      if milestone.open?
        milestone_query_path(milestone.repository.owner, milestone.repository, milestone.title)
      else
        issues_path(milestone.repository.owner, milestone.repository, q: "milestone:#{Search::ParsedQuery.encode_value(milestone.title)}")
      end
    end

    def gh_unsubscribe_issue_path(issue)
      expand_nwo_from :unsubscribe_issue_path, issue
    end

    def gh_subscribe_issue_path(issue)
      expand_nwo_from :subscribe_issue_path, issue
    end
  end
end
