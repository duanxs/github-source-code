# frozen_string_literal: true

module GitHub
  module RouteHelpers
    def gh_downloads_path(repo)
      expand_nwo_from :downloads_path, repo
    end

    def gh_download_path(file)
      expand_nwo_from :download_path, file
    end
  end
end
