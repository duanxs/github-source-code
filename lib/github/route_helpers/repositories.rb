# frozen_string_literal: true

module GitHub
  module RouteHelpers
    def gh_clones_path(repo)
      expand_nwo_from :clones_path, repo
    end

    def gh_repo_comments_path(repo)
      expand_nwo_from :repo_comments_path, repo
    end

    def gh_contributors_path(repo)
      expand_nwo_from :contributors_graph_path, repo
    end

    def gh_repository_languages_path(repo)
      expand_nwo_from :repository_languages_path, repo
    end

    def gh_watchers_path(repo)
      expand_nwo_from :watchers_path, repo
    end

    def gh_fork_network_path(repo)
      expand_nwo_from :network_members_path, repo
    end

    def gh_repo_rename_path(repo)
      expand_nwo_from :repo_rename_path, repo
    end

    def gh_repo_transfer_path(repo)
      expand_nwo_from :repo_transfer_path, repo
    end

    def gh_repo_abort_transfer_path(repo)
      expand_nwo_from :repo_abort_transfer_path, repo
    end

    def gh_pages_status_path(repo)
      expand_nwo_from :pages_status_path, repo
    end

    def gh_pages_certificate_status_path(repo)
      expand_nwo_from :pages_certificate_status_path, repo
    end

    def gh_pages_https_status_path(repo)
      expand_nwo_from :pages_https_status_path, repo
    end

    def gh_destroy_branch_path(repo, branch)
      destroy_branch_path(repo.owner, repo, name: branch)
    end

    def gh_deployments_path(repo)
      deployments_path(repo.owner, repo)
    end

    def gh_pulse_path(repo, period = nil)
      pulse_path(repo.owner, repo, period)
    end

    def gh_network_dependencies_path(repo)
      network_dependencies_path(repo.owner, repo)
    end

    def gh_network_dependents_package_path(repo, package_id: nil)
      network_dependents_path(repo.owner, repo, package_id: package_id)
    end

    def gh_repository_advisory_path(repository_advisory)
      expand_nwo_from :repository_advisory_path, repository_advisory
    end

    def gh_add_repository_advisory_credit_path(repository_advisory)
      expand_nwo_from :add_repository_advisory_credit_path, repository_advisory
    end
  end
end
