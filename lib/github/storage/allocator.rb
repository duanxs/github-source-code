# rubocop:disable Style/FrozenStringLiteralComment

require "application_record/domain/storage"

module GitHub::Storage::Allocator
  def self.least_loaded_fileservers
    least_loaded_hosts.map { |h| GitHub.storage_replicate_fmt % h }
  end

  def self.least_loaded_hosts
    replicas = least_loaded_replicas
    hosts = replicas[:hosts]
    non_voting = replicas[:non_voting]
    return hosts if non_voting.blank?
    hosts + non_voting
  end

  # Gets the least loaded hosts for the replication of a new object. Since
  # Enterprise only has a single partition, we can't (quickly) get disk stats
  # for each OID partition without scanning every file on disk. So, only
  # partition "0" is checked for now since all the values are the same.
  #
  # Also, the final OID of the new object is not known when this method is
  # called anyway.
  def self.least_loaded_replicas
    replicas = GitHub.storage_replica_count
    hosts = query_least_loaded_voting_hosts
    if hosts.length != replicas
      if GitHub.storage_auto_localhost_replica? && replicas == 1
        hosts = %w(localhost)
      else
        raise ::Storage::ReplicationError.new(replicas)
      end
    end

    non_voting_hosts = []
    get_non_voting_datacenters.each do |dc|
      non_voting_hosts += query_least_loaded_non_voting_hosts(dc)
    end

    {
      hosts: hosts,
      non_voting: non_voting_hosts,
    }
  end

  # Get the least loaded voting hosts for the replication of a new object based
  # on free disk space. Returns a number of hosts up to
  # `GitHub.storage_replica_count`
  def self.query_least_loaded_voting_hosts
    return [] if GitHub.storage_replica_count.zero?
    ApplicationRecord::Domain::Storage.github_sql.values(<<-SQL, limit: GitHub.storage_replica_count)
      SELECT storage_file_servers.host FROM storage_file_servers
      LEFT JOIN storage_partitions
         ON storage_file_servers.id = storage_partitions.storage_file_server_id
      WHERE storage_file_servers.online = 1
        AND storage_file_servers.embargoed = 0
        AND storage_file_servers.non_voting = 0
        AND (
          storage_partitions.`partition` = '0' OR
          storage_partitions.`partition` IS NULL
        )
      ORDER BY IFNULL(storage_partitions.disk_free, 0) DESC
      LIMIT :limit
    SQL
  end

  # Get the least loaded non-voting hosts for the replication of a new object
  # based on free disk space. Returns a number of hosts up to
  # `GitHub.storage_non_voting_replica_count`
  def self.query_least_loaded_non_voting_hosts(datacenter)
    return [] if GitHub.storage_non_voting_replica_count.zero?
    ApplicationRecord::Domain::Storage.github_sql.values(<<-SQL, limit: GitHub.storage_non_voting_replica_count, dc: datacenter)
      SELECT storage_file_servers.host FROM storage_file_servers
      LEFT JOIN storage_partitions
         ON storage_file_servers.id = storage_partitions.storage_file_server_id
      WHERE storage_file_servers.online = 1
        AND storage_file_servers.embargoed = 0
        AND storage_file_servers.non_voting = 1
        AND storage_file_servers.datacenter = :dc
        AND (
          storage_partitions.`partition` = '0' OR
          storage_partitions.`partition` IS NULL
        )
      ORDER BY IFNULL(storage_partitions.disk_free, 0) DESC
      LIMIT :limit
    SQL
  end

  def self.host_info(host)
    results = ApplicationRecord::Domain::Storage.github_sql.results(<<-SQL, host: host)
      SELECT storage_file_servers.host, storage_file_servers.online FROM storage_file_servers
      WHERE storage_file_servers.host = :host
    SQL
    results[0]
  end

  # Gets all online voting hosts in the storage cluster. Embargoed
  # hosts will be excluded from the list if `exclude_embargoed` is true
  def self.get_hosts(exclude_embargoed: false)
    sql = ApplicationRecord::Domain::Storage.github_sql.new <<-SQL
      SELECT host FROM storage_file_servers
      WHERE online = 1 AND non_voting = 0
    SQL
    sql.add "AND embargoed = 0" if exclude_embargoed
    sql.add "LIMIT 100"

    sql.results.flatten
  end

  # Gets all online non-voting hosts in the specified datacenter. Embargoed
  # hosts will be excluded from the list if `exclude_embargoed` is true
  def self.get_non_voting_hosts(datacenter, exclude_embargoed: false)
    sql = ApplicationRecord::Domain::Storage.github_sql.new(<<-SQL, datacenter: datacenter)
      SELECT host FROM storage_file_servers
      WHERE online = 1 AND non_voting = 1
      AND datacenter = :datacenter
    SQL
    sql.add "AND embargoed = 0" if exclude_embargoed
    sql.add "LIMIT 100"

    sql.results.flatten
  end

  # Gets all hosts in the storage cluster
  def self.get_all_hosts
    sql = ApplicationRecord::Domain::Storage.github_sql.new <<-SQL
      SELECT host FROM storage_file_servers
      LIMIT 100
    SQL

    sql.results.flatten
  end

  def self.hosts_for_blob(blob_id, host:, datacenter:)
    sql = ApplicationRecord::Domain::Storage.github_sql.new(<<-SQL, blob_id: blob_id)
      SELECT storage_replicas.host FROM storage_replicas
      INNER JOIN storage_file_servers
         ON storage_file_servers.host = storage_replicas.host
      WHERE storage_replicas.storage_blob_id = :blob_id
        AND storage_file_servers.online = 1
      ORDER BY CASE
    SQL

    if host
      sql.add "WHEN storage_file_servers.host = :host THEN 0", host: host
    end

    if datacenter
      sql.add "WHEN storage_file_servers.datacenter = :datacenter THEN 1", datacenter: datacenter
    end

    sql.add <<-SQL
       WHEN storage_file_servers.non_voting = 0 THEN 2
       ELSE 3
        END
    SQL

    sql.values
  end

  def self.non_voting_hosts_for_oid(oid, datacenter)
    ApplicationRecord::Domain::Storage.github_sql.values(<<-SQL, oid: oid, datacenter: datacenter)
      SELECT storage_replicas.host FROM storage_replicas
      INNER JOIN storage_blobs
         ON storage_replicas.storage_blob_id = storage_blobs.id
      INNER JOIN storage_file_servers
         ON storage_file_servers.host = storage_replicas.host
      WHERE storage_blobs.oid = :oid
        AND storage_file_servers.online = 1
        AND storage_file_servers.non_voting = 1
        AND storage_file_servers.datacenter = :datacenter
    SQL
  end

  def self.cluster_hosts_for_oid(oid)
    ApplicationRecord::Domain::Storage.github_sql.values(<<-SQL, oid: oid)
      SELECT storage_replicas.host FROM storage_replicas
      INNER JOIN storage_blobs
         ON storage_replicas.storage_blob_id = storage_blobs.id
      INNER JOIN storage_file_servers
         ON storage_file_servers.host = storage_replicas.host
      WHERE storage_blobs.oid = :oid
        AND storage_file_servers.online = 1
        AND storage_file_servers.non_voting = 0
    SQL
  end

  def self.hosts_for_oid(oid)
    ApplicationRecord::Domain::Storage.github_sql.values(<<-SQL, oid: oid)
      SELECT storage_replicas.host FROM storage_replicas
      INNER JOIN storage_blobs
         ON storage_replicas.storage_blob_id = storage_blobs.id
      INNER JOIN storage_file_servers
         ON storage_file_servers.host = storage_replicas.host
      WHERE storage_blobs.oid = :oid
        AND storage_file_servers.online = 1
    SQL
  end

  def self.blob_id_for_oid(oid)
    ApplicationRecord::Domain::Storage.github_sql.values(<<-SQL, oid: oid)
      SELECT storage_replicas.storage_blob_id FROM storage_replicas
      INNER JOIN storage_blobs
         ON storage_replicas.storage_blob_id = storage_blobs.id
      WHERE storage_blobs.oid = :oid
    SQL
  end

  def self.oids_on_host(host)
    ApplicationRecord::Domain::Storage.github_sql.results(<<-SQL, host: host)
      SELECT storage_blobs.id, storage_blobs.oid FROM storage_blobs
      INNER JOIN storage_replicas
         ON storage_blobs.id = storage_replicas.storage_blob_id
      WHERE storage_replicas.host = :host
    SQL
  end

  def self.all_hosts_with_partitions
    hosts = ApplicationRecord::Domain::Storage.github_sql.results <<-SQL
      SELECT storage_file_servers.host, storage_partitions.`partition` FROM storage_file_servers
      INNER JOIN storage_partitions
         ON storage_file_servers.id = storage_partitions.storage_file_server_id
      WHERE storage_file_servers.online = 1
        AND storage_file_servers.embargoed = 0
    SQL

    results = {}
    hosts.each do |h|
      results[h[0]] ||= []
      results[h[0]] << h[1]
    end
    results
  end

  def self.host_partitions(host)
    partitions = ApplicationRecord::Domain::Storage.github_sql.values(<<-SQL, host: host)
      SELECT storage_partitions.`partition` FROM storage_file_servers
      INNER JOIN storage_partitions
         ON storage_file_servers.id = storage_partitions.storage_file_server_id
      WHERE storage_file_servers.online = 1
        AND storage_file_servers.embargoed = 0
        AND storage_file_servers.host = :host
    SQL

    partitions
  end

  def self.update_partition_usage(host, partition, disk_free, disk_used)
    results = ApplicationRecord::Domain::Storage.github_sql.run(<<-SQL, host: host, partition: partition, free: disk_free, used: disk_used)
      UPDATE storage_partitions p
      JOIN storage_file_servers f ON (p.storage_file_server_id = f.id)
      SET disk_free = :free, disk_used = :used
      WHERE p.partition = :partition AND f.host = :host
    SQL
    results.affected_rows == 1
  end

  # Gets the full list of datacenters that contain non-voting hosts
  def self.get_non_voting_datacenters
    sql = ApplicationRecord::Domain::Storage.github_sql.new <<-SQL
      SELECT DISTINCT datacenter FROM storage_file_servers
      WHERE non_voting
      AND datacenter IS NOT NULL
    SQL

    sql.results.flatten
  end

  # Gets the count of non-voting hosts that exist within the specified
  # datacenter
  def self.non_voting_host_count(datacenter)
    query = ApplicationRecord::Domain::Storage.github_sql.new(<<-SQL, datacenter: datacenter)
      SELECT COUNT(*) FROM storage_file_servers
      WHERE non_voting
      AND datacenter = :datacenter
    SQL

    query.values.first
  end

  # Gets the number of replicas required for sufficient non-voting replication
  # within the specified datacenter. Will return
  # `GitHub.storage_non_voting_replica_count` unless a datacenter doesn't have
  # enough non-voting hosts, in which case it will just use the non-voting host
  # count.
  def self.non_voting_replica_count(datacenter)
    [GitHub.storage_non_voting_replica_count.to_i, non_voting_host_count(datacenter)].min
  end

end
