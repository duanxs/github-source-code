# frozen_string_literal: true

module GitHub::Storage::Replica
  def self.get_bad_replica_counts(
    offset,
    next_offset,
    copies,
    fileserver_hosts,
    limit)
    return [] if fileserver_hosts.empty?

    offset_where_clause = offset && next_offset ?
      "WHERE sb.id BETWEEN :offset AND :next_offset" : ""

    query = <<-SQL
      SELECT sb.id, sb.oid, COUNT(sr.host) AS copies
      FROM storage_blobs sb
      LEFT JOIN storage_replicas sr
      ON (
        sb.id = sr.storage_blob_id
        AND sr.host IN :fileserver_hosts
      )
      #{offset_where_clause}
      GROUP BY sb.id
      HAVING copies != :copies
      ORDER BY sb.id
      LIMIT :limit
    SQL

    ApplicationRecord::Domain::Storage.github_sql.results(query, offset: offset, next_offset: next_offset,
      copies: copies, limit: limit, fileserver_hosts: fileserver_hosts)
  end

  def self.get_online_objects(
    offset,
    next_offset,
    fileserver_hosts,
    limit)
    return [] if fileserver_hosts.empty?

    offset_where_clause = offset && next_offset ?
      "WHERE sb.id BETWEEN :offset AND :next_offset" : ""

    query = <<-SQL
      SELECT sb.id, sb.oid, COUNT(sr.host) AS copies
      FROM storage_blobs sb
      LEFT JOIN storage_replicas sr
      ON (
        sb.id = sr.storage_blob_id
        AND sr.host IN :fileserver_hosts
      )
      #{offset_where_clause}
      GROUP BY sb.id
      ORDER BY sb.id
      LIMIT :limit
    SQL

    ApplicationRecord::Domain::Storage.github_sql.results(query, offset: offset, next_offset: next_offset,
      limit: limit, fileserver_hosts: fileserver_hosts)
  end

  def self.get_replicas_for_oid(oid, fileserver_hosts)
    return [] if fileserver_hosts.empty?

    query = <<-SQL
      SELECT sr.host
      FROM storage_blobs sb
      LEFT JOIN storage_replicas sr
      ON (
        sb.id = sr.storage_blob_id
        AND sr.host IN :fileserver_hosts
      )
      WHERE sb.oid = :oid
    SQL

    ApplicationRecord::Domain::Storage.github_sql.results(query, oid: oid, fileserver_hosts: fileserver_hosts)
  end
end
