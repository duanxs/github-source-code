# rubocop:disable Style/FrozenStringLiteralComment

require "faraday"
require "github/ref_type"

# This pre-receive hook is supposed to catch user errors caused by misconfigured
# Git/Git LFS clients. E.g. a user adds successfully a Git LFS file to a local
# repository but overwrites the local Git LFS pre-push hooks. Consequently, the
# LFS objects are not uploaded and every other user pulling from this repo will
# experience Git LFS errors.
#
# This pre-receive hook is NOT supposed to catch bugs in GitHub or in the Git
# LFS server. If a Git LFS object is listed as MediaBlob in the GitHub database,
# then we assume the object is available.
class GitHub::PreReceiveLFSIntegrity
  ERR_UNKNOWN_OIDS     = "GH008: Your push referenced at least %i unknown Git LFS object%s:\n"\
                         "%s\nTry to push them with 'git lfs push --all'."

  ERR_INTERNAL         = "GH009: Git LFS object integrity could not be checked. "\
                         "Please contact GitHub support."

  # Timeout for searching for Git LFS object IDs in the pushed commits in seconds
  GIT_SCAN_TIMEOUT     = 3*60

  # Timeout for checking Git LFS object IDs against the internal GitHub API in seconds
  API_REQUEST_TIMEOUT  = 3
  API_PATH             = "/internal/lfs/verify"

  WARN_MAX_LFS_OBJECTS = "GH010: Your push referenced at least %i Git LFS object%s, "\
                         "but we only validated a random sample of %i. It is very "\
                         "likely that the remaining files have been uploaded "\
                         "successfully too."

  def initialize(context, connection = nil)
    @context = context
    @connection = connection
  end

  def check
    # `check_lfs_integrity` is enabled in `repo_permissions.rb` if at least
    # one Git LFS object was successfully pushed to the repository.
    return [[], []] unless @context.check_lfs_integrity?

    # Ignore deleted branches,
    # check only commits in user writable branches and tags,
    # create list of newly reachable commits (later passed on to 'git rev-list --not --all')
    commits = @context.pushed_refs.reject { |_, after, _| after == GitHub::NULL_OID }
                   .select { |_, _ , name| [:branch, :tag].include?(GitHub::RefType.detect_ref_type(name)) }
                   .map    { |_, after, _| after }
    return [[], []] if commits.length == 0

    # Ignore the push if at least one ref rewrites the Git LFS URL
    if custom_lfs_url?(commits)
      GitHub.dogstats.increment("git.hooks.pre_receive.lfsintegrity.custom_lfs_url")
      return [[], []]
    end

    # Search for referenced Git LFS objects in the push and return early if
    # there are none.
    lfs_oids, ref_search_failed = lfs_oids_in_push(commits, 2 * GitHub.lfs_integrity_max_oids)
    if ref_search_failed
      GitHub.dogstats.increment("git.hooks.pre_receive.lfsintegrity.errors", tags: ["type:lfs_oids_in_push"])
      return [["error: #{ERR_INTERNAL}"], []]
    end
    return [[], []] if lfs_oids.length == 0

    # Attention: This metric is capped by the maximum number of OIDs we
    # search for.
    GitHub.dogstats.histogram("git.hooks.pre_receive.lfsintegrity.referenced_oids", lfs_oids.length)

    warnings = []
    errors = []

    # If the push contains more than "lfs_integrity_max_oids" LFS objects,
    # then pick a random sample of "lfs_integrity_max_oids" objects. This way
    # we protect the /internal/lfs/verify endpoints against excessive requests.
    if lfs_oids.length > GitHub.lfs_integrity_max_oids
      warnings << "warning: #{WARN_MAX_LFS_OBJECTS}" % [
        lfs_oids.length,
        lfs_oids.length == 1 ? "" : "s",
        GitHub.lfs_integrity_max_oids]
      lfs_oids = lfs_oids.sample(GitHub.lfs_integrity_max_oids)
    end

    unknown_oids, query_failed = query_unknown_oids(@context.repo_name, lfs_oids)

    if query_failed
      GitHub.dogstats.increment("git.hooks.pre_receive.lfsintegrity.errors", tags: ["type:query_unknown_oids"])
      errors << "error: #{ERR_INTERNAL}"
    elsif unknown_oids.any?
      GitHub.dogstats.histogram("git.hooks.pre_receive.lfsintegrity.unknown_oids", unknown_oids.length)
      # Print maximal only 3 unknown oids to the console
      print_oids = unknown_oids[0..2]
      print_oids << "..." if print_oids.length != unknown_oids.length
      print_oids.map! { |x| "    #{x}" }
      message = "#{ERR_UNKNOWN_OIDS}" % [
        unknown_oids.length,
        unknown_oids.length == 1 ? "" : "s",
        print_oids.join("\n")]
      errors << "error: #{message}"
    end

    [warnings, errors]
  end

  private

  # Return all Git LFS object IDs that cannot be found on the server
  def query_unknown_oids(repo_name, lfs_oids)
    @connection ||= Faraday.new(url: "#{GitHub.api_url}#{API_PATH}")
    res = @connection.post do |req|
      timestamp = Time.now.to_i.to_s
      hmac = OpenSSL::HMAC.hexdigest("sha256", GitHub.api_internal_lfs_hmac_keys.first, timestamp)
      req.headers["Request-HMAC"] = "#{timestamp}.#{hmac}"
      req.headers["Content-Type"] = "application/json"
      req.body = JSON.generate({repository: repo_name, oids: lfs_oids})
      req.options.timeout = API_REQUEST_TIMEOUT * 1000
      req.options.open_timeout = 2
    end

    return nil, true if res.body.empty? || res.status != 200

    unknown = JSON.parse(res.body)["unknown"]
    return unknown, unknown.nil?
  end

  # Return all Git LFS object IDs referenced by commits that are reachable by
  # the pushed commits and that have not been in the repo before.
  def lfs_oids_in_push(commits, max_lfs_oids)
    options = {
      input: commits.join("\n") + "\n",
      max: max_lfs_oids * 65, # every Git LFS OID is 64 bytes and a newline
      timeout: GIT_SCAN_TIMEOUT,
    }

    GitHub.dogstats.increment("git.hooks.pre_receive.lfsintegrity.find_lfs_objects")
    child = POSIX::Spawn::Child.build("git-find-lfs-objects", options)
    begin
      child.exec!
      return nil, true unless child.status.exitstatus == 0
    rescue POSIX::Spawn::TimeoutExceeded
      return nil, true
    rescue POSIX::Spawn::MaximumOutputExceeded
      # Process all OIDs that we have gotten so far
      # Note: We might get a truncated Git LFS OID here. These should be no
      #       problem as the /internal/lfs/verify would ignore them.
    end

    return child.out.split("\n"), false
  end

  # Returns true if at least one head commit of an updated branch contains a
  # ".lfsconfig" file with a URL attribute.
  # Read the first "1kb * pushed ref count" for ".lfsconfig" content
  def custom_lfs_url?(commits)
    options = {
      input: commits.map { |ref| "#{ref}:.lfsconfig" }.join("\n") + "\n",
      max: commits.length * 1024,
      timeout: GIT_SCAN_TIMEOUT,
    }

    child = POSIX::Spawn::Child.build("git", "cat-file", "--batch=", options)
    begin
      child.exec!
      return false unless child.status.exitstatus == 0
    rescue POSIX::Spawn::TimeoutExceeded
      return false
    rescue POSIX::Spawn::MaximumOutputExceeded
      # process all lines that we have gotten so far
    end

    # detect "url = " and "lfsurl = " config lines
    # see https://github.com/git-lfs/git-lfs/blob/master/docs/man/git-lfs-config.5.ronn
    !!(child.out =~ /^\s*(lfs)?url\s*=/i)
  end


end
