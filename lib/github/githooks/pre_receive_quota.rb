# frozen_string_literal: true

class GitHub::PreReceiveQuota

  attr_reader :context

  def initialize(context)
    @context = context
  end

  def check
    above_lock = context.sockstat_var("above_lock_quota")
    above_warn = context.sockstat_var("above_warn_quota")
    enabled = context.sockstat_var("quotas_enabled")

    errors = []
    warnings = []
    if above_lock
      GitHub.dogstats.increment("git.hooks.pre_receive.quota", tags: ["type:lock"])
      errors << "error: this repository is above its size quota"
    elsif above_warn
      GitHub.dogstats.increment("git.hooks.pre_receive.quota", tags: ["type:warn"])
      warnings << "warning: this repository is approaching its size quota"
    end

    if enabled
      return [warnings, errors]
    else
      return [[], []]
    end
  end
end
