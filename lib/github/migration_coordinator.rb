# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  # Coordinate migrations of repositories from one GitHub instance to
  # another.
  class MigrationCoordinator
    include AreasOfResponsibility
    include GitHub::Migrator::DownloadHelpers

    areas_of_responsibility :migration

    def initialize(migrator: nil)
      @migrator = migrator
    end

    attr_reader :migrator

    class RateLimitExceeded < StandardError; end

    DOWNLOAD_EVERYTHING_LIMIT = 6
    DOWNLOAD_EVERYTHING_LIMIT_TTL = 24.hours
    DOWNLOAD_EVERYTHING_MAX_REPO_SIZE = 1.gigabyte / 1.kilobyte # == 1GB, as this measure is in kilobytes

    def rate_limited?(current_user)
      return false unless GitHub.rate_limiting_enabled?
      RateLimiter.at_limit?(
        "download_everything.#{current_user.id}",
        { max_tries: DOWNLOAD_EVERYTHING_LIMIT, amount: 1, ttl: DOWNLOAD_EVERYTHING_LIMIT_TTL },
      )
    end

    def download_everything(current_user)
      warnings = []
      if rate_limited?(current_user)
        raise RateLimitExceeded
      else
        downloadable_repos = current_user.repositories.where("disk_usage < ?", DOWNLOAD_EVERYTHING_MAX_REPO_SIZE)

        if current_user.has_any_trade_restrictions?
          downloadable_repos = downloadable_repos.public_scope
          warnings.push(TradeControls::Notices::Plaintext.user_account_restricted)
        end

        migration = export_later \
          repos: downloadable_repos,
          owner: current_user,
          current_user: current_user,
          lock: false,
          exclude_attachments: false

        GitHub.dogstats.increment("download_everything.start")

        big_repos = current_user.repositories.where("disk_usage >= ?", DOWNLOAD_EVERYTHING_MAX_REPO_SIZE)
        if big_repos.any?
          warnings.push ("Migration started. Some repositories were too large to be included in the export. Please
            visit the Migration API to export:")
          repo_names = big_repos.pluck(:name).join(", ")
          warnings.push(repo_names)
          GitHub.dogstats.increment("download_everything.oversized_repos")
        end
      end
      warnings
    end

    # Public: Start a migration.
    def export_later(repos:, owner:, current_user:, lock: false, exclude_attachments: false)
      migration = GitHub::SchemaDomain.allowing_cross_domain_transactions do
        ::Migration.create! do |m|
          m.guid = SimpleUUID::UUID.new.to_guid
          m.owner = owner
          m.creator = current_user
          m.lock_repositories = lock
          m.exclude_attachments = exclude_attachments
          m.repositories = repos
          m.state = :pending
        end
      end

      MigrationExportToArchiveJob.perform_later(migration)

      migration
    end

    # Public: Run the export.
    def export(migration)
      # Lock repositories if lock_repositories is true
      if migration.lock_repositories?
        migration.repositories.each(&:lock_for_migration)
      end

      exported_archive = Tempfile.new(["gh-migrator", ".tar.gz"])
      begin
        exported_archive.close

        migration.record_timing(:export) do
          migrator.export migration: migration, dest: exported_archive.path
        end

        update_migratable_resources_count(migration)

        unless ::Migration.export_disabled_for_actor?(migration.creator)
          save_archive migration: migration, exported_archive: exported_archive.path
        end
      ensure
        exported_archive.close!
      end
    ensure
      unless ::Migration.export_disabled_for_actor?(migration.creator)
        migration.clean_up
      end
    end

    # Public: Queue a mapping job
    def map_later(migration, mappings, actor)
      migration.enqueue_map!
      MapImportRecordsJob.perform_later(migration, mappings, actor)
    end

    # Public: Run the map.
    def map(migration, mappings, actor)
      migration.record_timing(:map_records) do
        migrator.map(migration: migration, mappings: mappings, actor: actor)
      end
    end

    # Public: Start to prepare an import
    def prepare_later(migration:, current_user:)
      # TODO: Ensure state and/or migration file uploaded
      PrepareImportArchiveJob.perform_later(migration, current_user)
    end

    # Public: Prepare an import
    def prepare(migration, actor)
      archive_path = download_archive(tmpdir, migration, actor)

      import_preparer = GitHub::Migrator::ImportPreparer.new(
        guid: migration.guid,
        archive_path: archive_path,
        migration_path: tmpdir,
        detect_conflicts: false,
      )

      migration.record_timing(:prepare) { import_preparer.call }

      if conflicts?(migration)
        migration.conflicts_detected!
      else
        migration.no_conflicts_detected!
      end
    end

    # Public: Queue a job to import data to Github from Archive
    def import_later(migration:, current_user:)
      ImportArchiveJob.perform_later(migration, current_user)
    end

    # Public: Import data to Github from Archive
    def import(migration, actor)
      archive_path = download_archive(tmpdir, migration, actor)

      archive_importer = GitHub::Migrator::ArchiveImporter.new(
        guid: migration.guid,
        archive_path: archive_path,
        migration_path: tmpdir,
        actor: actor,
      )

      migration.record_timing(:import) { archive_importer.call }

      update_migratable_resources_count(migration)
    end

    def conflicts?(migration)
      conflicts_detector = conflicts_detector_for(migration)
      conflicts_detector.conflicts?
    end

    def conflicts(migration, max_conflicts: nil, &block)
      conflicts_detector = conflicts_detector_for(migration, max_conflicts: max_conflicts)
      conflicts_detector.call(&block)
    end

    def unlock!(migration)
      if can_unlock?(migration)
        migration.record_timing(:unlock) do
          GitHub::Migrator::MigratedRepositoriesUnlocker.call(
            guid: migration.guid,
          ).tap { migration.unlock! }
        end
      elsif migration.failed_import?
        raise GitHub::Migrator::CannotUnlockOnFailedImport
      elsif migration.unlocked?
        raise GitHub::Migrator::CannotUnlockImportAgain
      else
        raise GitHub::Migrator::CannotUnlockImportInProgress
      end
    end

    private

    def test_environment?(migration)
      Rails.test? && !!migration.try(:authz_test?)
    end

    def can_unlock?(migration)
      migration.imported? || test_environment?(migration)
    end

    def tmpdir
      if @tmpdir and Dir.exist?(@tmpdir)
        @tmpdir
      else
        @tmpdir = Dir.mktmpdir("gh-migrator")
      end
    end

    def download_archive(target_dir, migration, actor)
      archive_path = File.join(target_dir, migration.file.name)

      migration.record_timing(:download_archive) do
        save_from_url(
          url: archive_download_url(migration, actor),
          to: archive_path
        )
      end

      archive_path
    end

    def archive_download_url(migration, actor)
      migration.file.download_url(actor: actor)
    end

    def save_archive(migration:, exported_archive:)
      file = migration.build_file

      # Set storage blob if we're in development or test mode. This enables
      # dotcom/user exports in development/test mode
      if set_storage_blob?(file)
        file.storage_blob ||= ::Storage::Blob.create_for_uploadable(
          oid: new_oid,
          size: File.size(exported_archive),
        )
      end

      file.update!(
        name: "#{migration.guid}.tar.gz",
        size: File.size(exported_archive),
        content_type: "application/x-gzip",
        uploader_id: migration.creator_id,
        supports_multi_part_upload: large_file?(exported_archive),
      )

      policy = file.storage_policy(actor: migration.creator)

      migration.record_timing(:upload_archive) do
        GitHub::Migrator::MigrationFileUploader.new(
          migration_file: file,
          archive_path: exported_archive,
          storage_policy: policy,
        ).call
      end

      file.update!(state: :uploaded)
    end

    def large_file?(path)
      return File.size(path) > MigrationFile::MAX_ASSET_SIZE
    end

    def update_migratable_resources_count(migration)
      Migrator::MigrationReporter.record_result_for(migration)
      migration.update_column(
        :migratable_resources_count,
        migration.migratable_resources.count,
      )
    end

    def set_storage_blob?(file)
      GitHub.storage_cluster_enabled? && file.respond_to?(:storage_blob) && local_or_test_mode?
    end

    # Checks to see if we're in development or test mode
    def local_or_test_mode?
      Rails.development? || Rails.test?
    end

    def new_oid
      SecureRandom.hex 32
    end

    def conflicts_detector_for(migration, max_conflicts: nil)
      GitHub::Migrator::ConflictsDetector.new(
        guid: migration.guid,
        organization: migration.owner,
        archive_url_templates: cached_url_templates_for(migration),
        max_conflicts: max_conflicts,
      )
    end

    def cached_url_templates_for(migration)
      kv_key = "migrationUrlTemplates-#{migration.guid}"
      if GitHub.kv.exists(kv_key).value { false }
        kv_results = GitHub.kv.get(kv_key).value { nil }
        url_templates = JSON.parse(kv_results)
      else
        GitHub::Migrator::DefaultUrlTemplates
      end
    end
  end
end
