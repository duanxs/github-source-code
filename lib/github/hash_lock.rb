# rubocop:disable Style/FrozenStringLiteralComment

require "set"

module GitHub
  # Straight copy of Resque::Plugins::Lock, only written in an age of REDIS
  # HASHES.  Benefits:
  # * We can see what args are in action with HKEYS (no Redis key scan).
  # * Less memory is used because there is a single key per job class.
  # * We can reset the locks for a whole job class by deleting that key.
  #
  #     DEL resque:hashlock:CreatePullRequestMergeCommitJob
  #
  # To add locking to your resque job, *extend* this module - for example:
  #
  #
  #  class MyAwesomeAsyncThing < Job
  #    extend GitHub::HashLock
  #  end
  #
  module HashLock
    class Job
      attr_accessor :job, :active, :inactive

      def initialize(job, active, inactive)
        @job      = job
        @active   = active
        @inactive = inactive
      end

      def name
        job.name
      end

      def clear_all_locks
        job.clear_all_locks
      end

      def clear_inactive_locks
        Resque.redis.pipelined do
          inactive.each do |key|
            Resque.redis.hdel(job.lock_set, key)
          end
        end
      end

      def sort_key
        [active.size, inactive.size, job.name]
      end

      def <=>(other)
        return nil unless other.is_a?(Job)

        sort_key <=> other.sort_key
      end
    end

    # List of all Resque job classes that use GitHub::HashLock
    def self.jobs
      @jobs ||= Set.new
    end

    # Clear out all locked jobs
    def self.reset!
      return if jobs.empty?
      Resque.redis.del(*jobs.map(&:lock_set))
    end

    def self.find(name)
      return unless job = jobs.detect { |j| j.name == name }
      Job.new(job, *job.active_and_inactive_locks)
    end

    # Track Job classes that use HashLock via the extended hook
    def self.extended(klass)
      jobs << klass
      super
    end

    def self.included(klass)
      raise ArgumentError, "extend HashLock, not include"
    end

    def locked?(*args)
      Resque.redis.hexists(lock_set, lock(*args))
    end

    # Override in your job to control the lock experiation time. This is the
    # time in seconds that the lock should be considered valid. The default
    # is one hour (3600 seconds).
    def lock_timeout
      3600
    end

    # The lock set that the lock key will belong to. All lock keys for a
    # particular set can be retrieved via the HKEYS command.
    def lock_set
      @lock_set ||= "hashlock:#{name}"
    end

    # Override in your job to control the lock key. It is passed the same
    # arguments as `perform`, that is, your job's payload. The lock key is
    # created within the lock set.
    #
    #   >> GitHub::HashLock.send :extend, GitHub::HashLock
    #   >> GitHub::HashLock.lock 1234, [1,2,34], :a => 1, :b => "2c", :c => 3
    #   => "1234 [1,2,34] {a,1,b,2c,c,3}"
    def lock(*args)
      if args.empty?
        "key"
      else
        args.map do |obj|
          case obj
          when Hash
            sorted = obj.to_a.sort_by { |x|  x.first.to_s }.join(",")
            "{#{sorted}}"
          when Array
            "[#{obj.join(",")}]"
          else
            obj
          end
        end.join(" ")
      end
    end

    # Resque hook that is run before enqueing a job. Here, we check that the
    # lock isn't currently held by another job before enqueing it again.
    def before_enqueue_lock(*args)
      key = lock(*args)
      now = Time.now.to_i
      timeout = now + lock_timeout + 1
      tags = ["class:#{self.name.underscore}", "queue:#{queue}", "legacy:true"]

      # return true if we successfully acquired the lock
      if Resque.redis.hsetnx(lock_set, key, timeout)
        GitHub.dogstats.increment("jobs.hash-lock.acquire", tags: tags)
        return true
      end

      # see if the existing timeout is still valid and return false if it is
      # (we cannot acquire the lock during the timeout period)
      old = Resque.redis.hget(lock_set, key).to_i
      if now <= old
        GitHub.dogstats.increment("jobs.hash-lock.fail-to-aquire", tags: tags)
        return false
      end

      # otherwise set the timeout and ensure that no other worker has acquired
      # the lock (NB - this should be a HGETSET operation but that is not
      # supported in redis, so we are faking it)
      Resque.redis.hset(lock_set, key, timeout)
      GitHub.dogstats.increment("jobs.hash-lock.acquire", tags: tags)
      now > old
    end

    # Resque hook that is run around the performing of a job. Here, we ensure
    # that the lock is cleared when the job is done running.
    def around_perform_lock(*args)
      begin
        yield
      ensure
        # Always clear the lock when we're done, even if there is an error.
        clear_lock(*args)
      end
    end

    def active_and_inactive_locks
      now = Time.now.to_i
      active = []
      inactive = []
      Resque.redis.hgetall(lock_set).each do |key, timestamp|
        (timestamp.to_i >= now ? active : inactive) << key
      end
      [active, inactive]
    end

    def clear_lock(*args)
      if Resque.redis.hdel(lock_set, lock(*args)) > 0
        tags = ["class:#{self.name.underscore}", "queue:#{queue}", "legacy:true"]
        GitHub.dogstats.increment("jobs.hash-lock.release", tags: tags)
      end
    end

    def clear_all_locks
      Resque.redis.del(lock_set)
    end

    alias set_lock before_enqueue_lock
  end
end
