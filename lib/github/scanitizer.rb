# frozen_string_literal: true

module GitHub
  class Scanitizer
    SERVICE_NAME = "scanitizer"

    def self.connection
      if !GitHub.scanitizer_url.empty?
        @connection ||= Faraday.new(url: GitHub.scanitizer_url) do |conn|
          conn.use GitHub::FaradayMiddleware::RequestID
          conn.use GitHub::FaradayMiddleware::HMACAuth, hmac_key: GitHub.scanitizer_hmac_key
          conn.use GitHub::FaradayMiddleware::Datadog, stats: GitHub.dogstats, service_name: SERVICE_NAME
          conn.response :json, content_type: /\bjson\z/, parser_options: { symbolize_names: true }
          conn.adapter :net_http
        end
      end
    end

    def self.upload_scan_result(scan_result)
      # `GitHub.scanitizer_url` is "https://scanitizer.github.com". We call
      # it with the path "/scan-result" which creates a check-run with the scanitizer app identity againt the scan result
      # The connection would be unset, so this call would be skipped during tests

      GitHub.dogstats.increment("scanitizer_client.upload_scan_result",
                                tags: ["route:upload_scan_result_handler"])
      response = nil
      if !connection.nil?
        response = connection.post("/scan-result") do |req|
          req.body = GitHub::JSON.encode(scan_result)
          req.headers["Accept"] = "application/json"
          req.headers["Content-Length"] = req.body.size.to_s
          req.headers["Content-Type"] = "application/json"
        end
      end

      response
    end
  end
end
