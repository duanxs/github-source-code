# frozen_string_literal: true
require "#{Rails.root}/config/resqued/queue_assignments"
# GitHub::Aqueduct::Job handles enqueuing and executing jobs via aqueduct. It
# has interfaces to support both RockQueue jobs and ActiveJob jobs.
module GitHub
  module Aqueduct
    class Job
      # Consider the enqueue a failure if it completed successfully but took longer
      # than 1 second.
      MAX_ENQUEUE_DURATION = 1.second

      # Stop enqueueing to aqueduct if we've spent greater than 500ms in total
      # performing enqueues. This can happen if a request enqueues many jobs.
      TIME_BUDGET = 0.5.seconds

      # Aqueduct can only handle paylaods up to 5 MB
      MAX_PAYLOAD_SIZE = 5.megabytes

      APP_ROLE_WHITELIST = [
        "github-staff",
        "github-depworker",
        "github-graphworker",
        "github-slowworker",
        "github-backupworker",
        "github-hookworker",
        "github-highworker",
        "github-lowworker",
        "github-lowworker-kube",
        "github-pagesworker",
        # GitbackupsSchedulerJob queries the size of the "gitbackups_maintenance" queue using
        # `Resque.size`, which doesn't yet have an aqueduct client analog, so we need to
        # blacklist all jobs running on the "gitbackups_maintenance" queue to ensure that
        # `Resque.size` is accurate. As of 2019-10-07, all the jobs that run on
        # gitbackups-maintenance hosts run on the "gitbackups_maintenance" queue.
        # "gitbackups-maintenance",
      ]

      TIMING_THRESHOLDS = [35, 100, 1000]

      # Heartbeat more frequently than the default of 60 seconds so that we can reduce the
      # redelivery timeout.
      HEARTBEAT_INTERVAL = 15.seconds

      # Default to 3x the heartbeat interval to allow for failed or delayed heartbeats.
      DEFAULT_REDELIVERY_TIMEOUT = HEARTBEAT_INTERVAL * 3

      module JobType
        ACTIVE_JOB = :active_job
      end

      include ActiveSupport::Callbacks
      define_callbacks :enqueue, :execute

      def self.before_enqueue(*args, &block)
        set_callback :enqueue, :before, *args, &block
      end

      def self.before_execute(*args, &block)
        set_callback :execute, :before, *args, &block
      end

      def self.after_execute(*args, &block)
        set_callback :execute, :after, *args, &block
      end

      def self.around_execute(*args, &block)
        set_callback :execute, :around, *args, &block
      end

      # In review-lab under resque, queue names are prefixed with the review-lab
      # name to provide isolation between review-lab deployments (i.e.
      # `<review-lab-name>_<queue-name>`). This queue prefixing scheme leads to
      # high queue cardinality.
      #
      # Aqueduct restricts the number of queues per application, so high queue
      # cardinality is a problem. To reduce queue cardinality, we override
      # queue names in review-lab to a single queue composed of the review-lab
      # name (i.e. `review-lab-<review-lab-name>`. When aqueduct workers start
      # up, they filter each assigned queue through
      # `AqueductAdapter#aqueduct_queue_name` to ensure that publishers and
      # workers are both aware of the naming scheme.
      def self.aqueduct_queue_name(queue_name)
        if GitHub.dynamic_lab?
          "review-lab-#{GitHub.dynamic_lab_name.parameterize}"
        else
          queue_name
        end
      end

      def self.timer
        @timer ||= Timer.new
      end

      def self.max_payload_size
        MAX_PAYLOAD_SIZE
      end

      def self.circuit_breaker
        @circuit_breaker ||= Resilient::CircuitBreaker.get("aqueduct", {
          instrumenter: GitHub,
          sleep_window_seconds: 10,
          request_volume_threshold: 2,
          error_threshold_percentage: 5,
        })
      end

      # Public: Enqueue an ActiveJob job.
      #
      # Returns a GitHub::Aqueduct::Job::Result indicating enqueue success or failure.
      def self.enqueue_active_job(job, client = GitHub.aqueduct)
        return Result.retry if block_enqueues?(queue: job.queue_name, job_class: job.class)

        result = nil # establish variable scope
        duration = timer.time do
          begin
            new(
              queue: job.queue_name,
              job_class: job.class,
              payload: job.serialize,
              redelivery_timeout: job.redelivery_timeout || DEFAULT_REDELIVERY_TIMEOUT,
              max_redelivery_attempts: job.max_redelivery_attempts
            ).enqueue(client)

            result = Job::Result.ok
          rescue => e
            correlation_id = SecureRandom.urlsafe_base64(18)
            GitHub::Logger.log_exception({correlation_id: correlation_id, queue: job.queue_name, job: job.class, args: job.serialize}, e)
            if e.is_a?(::Aqueduct::Client::ClientError)
              Failbot.report(e, {
                app: "github-aqueduct",
                queue: job.queue_name,
                job: job.class,
                splunk_correlation_id: correlation_id,
              }.merge(e.metadata.map { |k, v| ["aqueduct_#{k}", v] }.to_h))
            else
              Failbot.report(e, {
                app: "github-aqueduct",
                queue: job.queue_name,
                job: job.class,
                splunk_correlation_id: correlation_id,
              })
            end
            result = Job::Result.retry(error: e)
          end
        end

        handle_result(result: result, duration: duration, queue: job.queue_name, job_class: job.class)
      end

      # Internal: Skip the enqueue altogether if the aqueduct circuit breaker
      # is open or if aqueduct has already spent too much time enqueueing during
      # this request.
      def self.block_enqueues?(queue:, job_class:)
        if !circuit_breaker.allow_request?
          return true
        end

        if time_budget_exceeded?
          tags = ["queue:#{queue}", "class:#{job_class}"]
          GitHub.dogstats.increment("rpc.aqueduct.time-budget-exceeded", tags: tags)
          return true
        end
      end

      # Public: Reset the time budgeting stats at the end of the request.
      def self.reset_stats
        Thread.current[:aqueduct_time_budget] = 0
      end

      # Internal: Have we exhausted our time budget enqueuing to aqueduct?
      def self.time_budget_exceeded?
        # We only worry about time budgeting during requests. It's okay if
        # background jobs exceed the budget by enqueueing many jobs.
        return unless GitHub.foreground?

        (Thread.current[:aqueduct_time_budget] ||= 0) > TIME_BUDGET
      end

      def self.handle_result(result:, duration:, queue:, job_class:)
        tags = ["queue:#{queue}", "class:#{job_class}"]

        Thread.current[:aqueduct_time_budget] ||= 0
        Thread.current[:aqueduct_time_budget] += duration

        if result && result.ok?
          report_threshold(duration * 1000)
          if duration < MAX_ENQUEUE_DURATION
            circuit_breaker.success
          else
            GitHub.dogstats.increment("rpc.aqueduct.too_slow", tags: tags)
            circuit_breaker.failure
          end

          return result
        end

        # Oversized payload errors are thrown before we attempt to send
        # aqueduct API calls, so they don't count for or against the circuit
        # breaker.
        if result.oversized_payload?
          GitHub.dogstats.increment("rpc.aqueduct.oversized_payload", tags: tags)
        else
          circuit_breaker.failure
        end

        result
      end

      # Public: Execute a RockQueue or ActiveJob job given a string payload
      # received from aqueduct. All the information needed to discern job type
      # is embedded in the encoded payload.
      def self.execute(encoded)
        decoded = GitHub::JSON.decode(encoded)

        new(
          queue: decoded.fetch("queue"),
          job_class: decoded.fetch("job_class"),
          payload: decoded.fetch("payload"),
          metadata: decoded.fetch("metadata", {})
        ).execute
      end

      # Public: Checks if feature flag is enabled for a queue based on its
      # worker app-role and whether this job is  blacklisted.
      def self.enqueue_to_aqueduct?(queue, job_class)
        # Don't enqueue to aqueduct from review-lab because review-lab doesn't run
        # aqueduct workers yet.
        return false if GitHub.dynamic_lab?

        app_role = Resque::QueueAssignments.app_role_for_queue(queue.to_s)
        return false unless APP_ROLE_WHITELIST.include?(app_role)

        return false unless GitHub.aqueduct_enabled?

        GitHub.aqueduct_enqueue_for_app_role?(app_role)
      end

      def self.report_threshold(duration_ms)
        TIMING_THRESHOLDS.each do |t|
          if duration_ms <= t
            GitHub.dogstats.increment("rpc.aqueduct.enqueue.time.threshold", tags: ["le:#{t}", "rpc_operation:send_job"])
          end
        end
      end

      # The class name of the job. This is either a job class for RockQueue
      # jobs, or ActiveJob::QueueAdapters::ResqueAdapter::JobWrapper.
      attr_reader :job_class

      # The job payload.
      attr_reader :payload

      # Metadata associated with this job.
      attr_reader :metadata

      # Redelivery timeout of this job.
      attr_reader :redelivery_timeout

      # Number of times this job should be redelivered if we don't hear back from the worker.
      attr_reader :max_redelivery_attempts

      def initialize(queue:, job_class:, payload:, redelivery_timeout: nil, max_redelivery_attempts: nil, metadata: {})
        @queue = queue
        @job_class = job_class
        @payload = payload
        @metadata = metadata
        @redelivery_timeout = redelivery_timeout
        @max_redelivery_attempts = max_redelivery_attempts
      end

      # Public: enqueue this job to aqueduct.
      def enqueue(client = GitHub.aqueduct)
        tags = ["queue:#{queue}", "class:#{job_class}"]
        GitHub.dogstats.time("rpc.aqueduct.enqueue.time", tags: tags) do
          GitHub.dogstats.time("rpc.aqueduct.callbacks.time", tags: ["callback:before_enqueue"]) do
            # no block needed, there's only before_enqueue
            run_callbacks :enqueue
          end

          payload = GitHub.dogstats.time("rpc.aqueduct.job_encode.time") do
            # Warn to Haystack if we're trying to encode a payload with invalid UTF8
            GitHub::JSON.encode(to_h, warn_utf8: true)
          end

          if payload.bytesize >= self.class.max_payload_size
            raise PayloadTooLargeError.new("Payload for #{job_class} was #{payload.bytesize} bytes")
          end

          GitHub.dogstats.histogram("rpc.aqueduct.payload_size", payload.bytesize, tags: tags)

          GitHub.dogstats.time("rpc.aqueduct.time", tags: ["rpc_operation:send_job"]) do
            client.send_job(
              queue: queue,
              payload: payload,
              redelivery_timeout_secs: @redelivery_timeout&.to_i,
              max_redelivery_attempts: @max_redelivery_attempts&.to_i
            )
          end
        end
      end

      # Public: execute this job.
      def execute
        run_callbacks :execute do
          ActiveJob::Base.execute(payload)
        end
      end

      # Internal: the hash representation of this job, as serialized for aqueduct.
      def to_h
        {
          queue: queue,
          job_class: job_class.name,
          payload: payload,
          metadata: metadata,
        }
      end

      # Internal: the calculated queue name for this job.
      def queue
        if GitHub.dynamic_lab?
          "review-lab-#{GitHub.dynamic_lab_name.parameterize}"
        elsif @queue.to_s.match(FS_QUEUE_REGEX)
          # File server maintenance jobs can only run on the file servers. File
          # server workers dynamically generate queue names that are derived from
          # the host name to ensure that jobs are routed to the correct server
          # e.g. github-dfs-0bccb85.cp1-iad.github.net works the "maint_github-dfs-0bccb85"
          # queue. We don't allow "lab_" prefixing of file server queues because
          # no workers would work those queues. As a result, file server jobs
          # enqueued in lab are executed on the real file servers.
          @queue
        else
          # Preserving some existing resque logic here. `GitHub.resque_queue_prefix`
          # is used to namespace queues in the staff-only lab environment.
          "#{GitHub.resque_queue_prefix}#{@queue}"
        end
      end

      PayloadTooLargeError = Class.new(StandardError)

      class Result
        def self.ok
          new(ok: true, should_retry: false, error: nil)
        end

        def self.retry(error: nil)
          new(ok: false, should_retry: true, error: error)
        end

        def self.do_not_retry
          new(ok: false, should_retry: false, error: nil)
        end

        def initialize(ok:, should_retry:, error:)
          @ok = ok
          @retry = should_retry
          @error = error
        end

        # Public: Was the enqueue attempt a success?
        #
        # Returns a Boolean.
        def ok?
          @ok
        end

        # Public: Should this aqueduct enqueue attempt be retried with resque?
        #
        # Returns a Boolean.
        def retry?
          @retry
        end

        def error
          @error
        end

        def oversized_payload?
          @error.is_a?(PayloadTooLargeError)
        end
      end

      class Timer
        def time
          start = Process.clock_gettime(Process::CLOCK_MONOTONIC)
          yield
          Process.clock_gettime(Process::CLOCK_MONOTONIC) - start
        end
      end
    end
  end
end
