# rubocop:disable Style/FrozenStringLiteralComment

require "rotp"

module GitHub
  module Authentication
    # Default Authentication a.k.a. GitHub Authentication. Defines the
    # adapter interface and acts as the base adapter.
    class Default

      # Configuration hash
      attr_reader :configuration

      # Public: utility predicate methods to query for adaptor's auth mode.
      # Returns boolean whether this adaptor matches one of
      # GitHub::Authentication::AUTH_MODES. ex: @adaptor.ldap?
      #
      AUTH_MODES.each do |auth_mode|
        define_method "#{auth_mode}?" do
          self.class.name.demodulize.underscore.to_sym == auth_mode
        end
      end

      # Default initializer. Override for custom initialization.
      def initialize(config = {})
        @configuration = default_config.merge(config)
      end

      # Default configuration hash. Override for custom defaults.
      def default_config
        {}
      end

      # Returns the url to logout. Override this method in subclasses if session
      # is managed externally.
      def logout_url
        "/logout"
      end

      # Authenticate by login, password, and optionally 2FA OTP. This method
      # will validate a user's 2FA OTP if 2FA is enabled for the user.
      #
      # login - The String login of the user to authenticate.
      # password - The String password of the user to authenticate.
      # otp - The OTP of the user to authenticate (optional).
      #
      # Returns a GitHub::Authentication::Result
      def password_and_otp_authenticate(login, password, otp)
        auth_result = password_authenticate(login, password)
        user = auth_result.user

        two_factor_failure =
          auth_result.success? &&
          user.two_factor_authentication_enabled? &&
          !user.valid_otp?(otp)

        if two_factor_failure
          two_factor_type = user.two_factor_credential.sms_enabled? ? "sms" : "app"
          Result.two_factor_failure user, two_factor_type
        else
          auth_result
        end
      end

      # Authenticate by login and password. This method does not consider 2FA,
      # even if it is enabled. This method should only be used for scenarios,
      # such as sudo authentication, where 2FA validation is not required.
      #
      # login - The String login of the user to authenticate.
      # password - The String password of the user to authenticate.
      #
      # Returns a GitHub::Authentication::Result
      def password_authenticate(login, password)
        user, message = User.authenticate(login, password)

        if user && user.suspended?
          Result.suspended_failure message: "Your account has been suspended."
        elsif user
          Result.success user, message: message
        else
          Result.password_failure message: message
        end
      end

      # Authenticate user for sudo access. This method uses
      # GitHub::Authentication::Attempt to inherit functionality, such as rate
      # limiting against brute force attempts.
      #
      # login - The String login of the user to authenticate.
      # password - The String password of the user to authenticate.
      #
      # Returns a GitHub::Authentication::Result
      def sudo_password_authenticate(login, password)
        attempt = GitHub::Authentication::Attempt.new(
          from: :sudo,
          login: login,
          password: password,
        )

        attempt.result
      end

      # Authenticate by an OAuth access token or an integration's installation
      # access token.
      #
      # token                      - The token to use to lookup the associated user.
      # allow_integrations         - Whether the caller supports server to server
      #                              GitHub App authorization checks
      #                              (default false).
      # allow_user_via_integration - Whether the caller supports user to server
      #                              GitHub App authorization checks
      #                              (default false).
      # ip                         - String: The IP address of the request.
      # user_agent                 - String: The User agent header of the request.
      #
      # Returns a GitHub::Authentication::Result
      def access_token_authenticate(token, allow_integrations: false, allow_user_via_integration: false, ip:, user_agent:)
        user = TokenLookup.new(token, ip: ip, user_agent: user_agent).actor

        return Result.token_failure unless user

        if user.is_a?(Bot) && !allow_integrations
          Result.allow_integrations_failure(
            message: "GitHub App installation tokens are not allowed.",
          )
        elsif user.using_auth_via_integration? && !allow_user_via_integration
          Result.allow_user_via_integration_failure(
            message: "GitHub App user access tokens are not allowed.",
          )
        elsif user.suspended?
          Result.suspended_failure message: "Your account has been suspended."
        elsif user.using_auth_via_oauth_application? && user.oauth_application.suspended?
          Result.suspended_oauth_application_failure(
            message: "Your application has been suspended.",
          )
        elsif user.is_a?(Bot) && user.installation.suspended?
          Result.suspended_installation_failure(
            message: suspended_installation_message(user.installation),
          )
        else
          Result.success user
        end
      end

      # Checks whether a string looks like an OAuth Access Token.
      #
      # Returns true or false.
      def oauth_access_token?(token)
        !!(token =~ OauthAccess::TOKEN_PATTERN)
      end

      # Does the given string resemble an AuthenticationToken's token value?
      #
      # token - A String used as part of an authentication attempt.
      #
      # Returns a Boolean.
      def authentication_token?(token)
        !!(token =~ AuthenticationToken::TOKEN_PATTERN_V1)
      end

      # Does the given string resemble a valid access token (i.e., either an
      # OAuth access token or an installation access token)?
      #
      # token - A String used as part of an authentication attempt.
      #
      # Returns a Boolean.
      def access_token?(token)
        oauth_access_token?(token) || authentication_token?(token)
      end

      # User lookup & authentication from a rails request.
      #
      # Returns a tuple of a GitHub::Authentication::Result and the User that
      # attempted authentication.
      def rails_authenticate(request, octolytics_id:, current_device_id:)
        attempt = GitHub::Authentication::Attempt.new(
          from: :web,
          login: request.params[:login],
          password: request.params[:password],
          ip: request.remote_ip,
          user_agent: request.user_agent,
          request_id: request.env["HTTP_X_GITHUB_REQUEST_ID"],
          octolytics_id: octolytics_id,
          current_device_id: current_device_id,
        )

        GitHub.dogstats.time("rails_authenticate_duration") do
          return attempt.result, attempt.attempted_user
        end
      end

      def rails_logout(request, current_user, redirect_to: "/")
        LogoutResult.success redirect_to
      end

      # Built-in GitHub auth vs. an external adaptor (e.g. LDAP, CAS).
      #
      # Returns true for Default, false for any subclass.
      def external?
        false
      end

      # Public: Determines if a given user is using external authentication
      # or if they're using builtin authentication.
      def external_user?(user_or_login)
        false
      end

      # Public: Returns any mapping information we have on hand, if any, for
      # external users.
      def external_mapping(user)
        nil
      end

      # New users are created through the signup page when set. Otherwise,
      # its assumed that users are created externally and they only need to
      # login.
      #
      # Returns true for Default.
      def signup_enabled?
        true
      end

      # Whether or not to validate a user
      #
      # Returns true for Default
      def verifiable?
        true
      end

      def sudo_mode_enabled?(user)
        true
      end

      # Whether or not this authentication strategy allows built-in autentication
      # as a fallback (e.g. Allow users to sign in via SAML or username/password).
      def builtin_auth_fallback?
        false
      end

      # Whether built-in users are allowed on this strategy / configuration
      def allow_builtin_users?
        !external? || builtin_auth_fallback?
      end

      def two_factor_authentication_enabled?
        allow_builtin_users?
      end

      # Do we allow Organizations to require 2FA for all members?
      def two_factor_org_requirement_allowed?
        two_factor_authentication_enabled?
      end

      def two_factor_authentication_allowed?(user)
        two_factor_authentication_enabled? && !external_user?(user)
      end

      # Public: Whether changing user login is allowed.
      #
      # Returns true if users can change their login names.
      def user_renaming_enabled?(user)
        !external?
      end

      # Public: Whether changing user profile names via the UI is allowed.
      #
      # Returns true as a default and should be defined in the adaptor subclass
      # if different behavior is needed.
      def user_change_profile_name_enabled?(user)
        !profile_name_managed_externally?
      end

      # Public: Whether changing emails via the UI is allowed.
      #
      # Returns true as a default and should be defined in the adaptor subclass
      # if different behavior is needed.
      def user_change_email_enabled?(user)
        !emails_managed_externally?
      end

      # Public: Whether changing SSH keys via the UI is allowed.
      #
      # Returns true as a default and should be defined in the adaptor subclass
      # if different behavior is needed.
      def user_change_ssh_key_enabled?(user)
        !ssh_keys_managed_externally?
      end

      # Public: Whether changing GPG keys via the UI is allowed.
      #
      # Returns true as a default and should be defined in the adaptor subclass
      # if different behavior is needed.
      def user_change_gpg_key_enabled?(user)
        !gpg_keys_managed_externally?
      end

      # Public: Returns true if Profile Names are managed externally
      # Returns false as a default and should be defined in the adaptor subclass
      # if different behavior is needed.
      def profile_name_managed_externally?
        false
      end

      # Public: Returns true if Emails are managed externally
      # Returns false as a default and should be defined in the adaptor subclass
      # if different behavior is needed.
      def emails_managed_externally?
        false
      end

      # Public: Returns true if SSH Keys are managed externally
      # Returns false as a default and should be defined in the adaptor subclass
      # if different behavior is needed.
      def ssh_keys_managed_externally?
        false
      end

      # Public: Returns true if GPG Keys are managed externally
      # Returns false as a default and should be defined in the adaptor subclass
      # if different behavior is needed.
      def gpg_keys_managed_externally?
        false
      end

      # The redirect path for failed authentication. Only used by external adaptors.
      #
      # Returns the route for handling failed auth, or nil if it's not used.
      def path(return_to = nil)
        nil
      end

      # Whether or not to redirect on failed authentication. Defaults to true if
      # `path` is defined.
      def redirect_on_failure?
        path.present?
      end

      # Log an authentication event. For Enterprise this goes to auth.log. For
      # dotcom this goes to syslog.
      #
      # data - A Hash of keys/values to log.
      #
      # Returns nothing.
      def log(data)
        if GitHub.enterprise?
          enterprise_log data
        else
          GitHub::Logger.log data
        end
      end

      # Internal: Format `data` for logging. Returns a string.
      def format_log(data)
        data = {log_message: data} if data.kind_of?(String)

        # Can merge an Exception into a hash, but not vice versa.
        data = {now: Time.now.utc}.merge data

        Scrolls::Parser.unparse data
      end

      # Log an authentication event to auth.log. This uses Scrolls::Parser to
      # format the data for easy parsing. The formatting logic is borrowed from
      # Scrolls.
      #
      # data - A String message, an Exception, or a Hash.
      #
      # Returns nothing.
      def enterprise_log(data)
        msg = format_log(data)
        enterprise_logger.debug msg
      end

      # A Logger for use on enterprise. Outputs to auth.log.
      #
      # Returns a ::Logger object.
      def enterprise_logger
        @enterprise_logger ||= begin
          path = Rails.root.join("log", "auth.log")
          logger = ::Logger.new(path)
          logger.level = ::Logger::Severity::DEBUG
          logger
        end
      end

      def add_middleware(builder)
      end

      def name
        "GitHub"
      end

      # Create a BCrypt hash using a constant salt.
      #
      # password - The password to hash.
      #
      # Returns a BCrypt hash string.
      def constant_salt_bcrypt(password)
        salt = GitHub.constant_bcrypt_salt
        cost = GitHub.password_cost
        BCrypt::Engine.hash_secret(password, salt, cost)
      end

      # SP XML metadata string used by idP
      def metadata
        metadata = ::SAML::Message::Metadata.new({
          assertion_consumer_service_url: "#{configuration[:sp_url]}/saml/consume",
          sign_assertions: false,
          issuer: configuration[:sp_url],
        })
        metadata.to_xml
      end

      def instrument(event, payload = {}, &block)
        instrumentation_service.instrument(event, payload, &block)
      end

      def instrumentation_service
        GitHub.instrumentation_service
      end

      # Internal: Determines if the given request looks like it's attempting to authenticate
      # by passing username and password.
      #
      # Returns a Boolean.
      def builtin_auth_attempt?(request)
        request.params.values_at(:login, :password).all?(&:present?)
      end

      # Returns a custom authentication name, if exists
      #
      #
      def custom_authentication_name
        custom_messages = ::CustomMessages.instance
        name = custom_messages.auth_provider_name
        name.presence
      end

      def suspended_installation_message(installation)
        target = installation.target

        if installation.integrator_suspended?
          timestamp = ::Api::Serializer.time(installation.integrator_suspended_at)
          "You suspended this installation owned by #{target} at #{timestamp}"
        elsif installation.user_suspended?
          timestamp = ::Api::Serializer.time(installation.user_suspended_at)
          "Sorry. This installation owned by #{target} suspended your access at #{timestamp}."
        else
          "This GitHub App installation is currently suspended."
        end
      end
    end
  end
end
