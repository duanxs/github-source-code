# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Authentication
    require "octokit"

    class GitHubOauth < OmniAuth
      def password_and_otp_authenticate(login, password, otp)
        user, message = find_user(login)

        unless user
          message ||= "Could not find user"
          return Result.failure message: message
        end

        if user.suspended?
          return Result.suspended_failure message: "User is suspended."
        end

        client = ::Octokit::Client.new(login: user.login, password: password)
        begin
          authorizations = client.authorizations \
            client_id: GitHub.github_oauth_client_id,
            headers: otp_header(otp)
        rescue ::Octokit::OneTimePasswordRequired => e
          # This call will POST /authorizations. This results in a 2FA SMS
          # being sent to the user if they have SMS enabled. The result will
          # come back again with the `X-GitHub-OTP: required;` header,
          # causing Octokit to raise an exception.
          client.create_authorization rescue nil
          return Result.two_factor_failure user, e.password_delivery
        end

        if oauth_authorized?(authorizations)
          Result.success user
        else
          Result.failure message: "not authorized"
        end
      rescue
        Result.failure message: "unknown error"
      end

      def otp_header(otp)
        otp.blank? ? {} : {"X-GitHub-OTP" => otp.to_s}
      end

      # Check if the user has already signed in
      # in this host using OAuth.
      #
      # Although it is unlikely, I could sign in
      # in a different host if a username with my login exists
      # and we don't check this. Which could lead to an identity stealing.
      def oauth_authorized?(authorizations)
        authorizations.present?
      end

      def strategy
        Strategy
      end

      def config
        {
          client_id: GitHub.github_oauth_client_id,
          client_secret: GitHub.github_oauth_secret_key,
          github_organization: GitHub.github_oauth_organization,
          organization_team: GitHub.github_oauth_team,
        }
      end

      def name
        "GitHub OAuth"
      end

      def create_user(uid, user_info)
        user = super(uid, user_info)

        user_info.copy_profile(user)
        user_info.add_emails(user)
        user_info.add_public_keys(user)
        add_to_organization(user)

        user
      end

      def add_to_organization(user)
        org_name, team_id = config[:github_organization].split("/").first, nil

        if config[:organization_team]
          org_name, team_id = config[:organization_team].split("/")
        end

        if team_id && org = Organization.find_by_login(org_name)
          if team = org.teams.find_by_slug(team_id)
            team.add_member(user)
          end
        end
      end

      def verifiable?
        true
      end

      def sudo_mode_enabled?(user)
        false
      end

      def rails_logout(request, current_user, redirect_to: "dashboard")
        LogoutResult.success File.join(GitHub.url, redirect_to)
      end
    end

    require "github/authentication/github_oauth/strategy"
    require "github/authentication/github_oauth/user_info"
  end
end
