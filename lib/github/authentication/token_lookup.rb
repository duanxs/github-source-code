# frozen_string_literal: true

require "scientist"

module GitHub
  module Authentication
    # Encapsulates the process of looking up an access token and identifying its
    # corresponding actor (i.e., User or Bot).
    class TokenLookup
      class ActorNotFoundError < StandardError; end

      attr_reader :token, :ip, :user_agent

      # Public: Initialize a TokenLookup.
      #
      # token   - A String access token value.
      # request - The request that originated this token lookup.
      def initialize(token, ip: nil, user_agent: nil)
        @token = token
        @ip = ip || ""
        @user_agent = user_agent || ""
      end

      # Public: Return the actor (if any) associated with the token.
      def actor
        last_operations = DatabaseSelector::LastOperations.from_token(token)
        DatabaseSelector.instance.read_from_database(
          last_operations: last_operations,
          called_from: :token_lookup,
        ) do
          begin
            find_actor
          rescue ActiveRecord::RecordInvalid, RuntimeError, NoMethodError => e
            instrument_error(e)
            raise
          end
        end
      end

      private

      def find_actor
        case token_type
        when :oauth_access
          User.with_oauth_token(token)
        when :authentication_token
          Bot.find_by_token(token)
        end
      end

      # rubocop:todo GitHub/BooleanMemoizationWithOrOperator
      def token_type
        @token_type ||=
          case token
          when OauthAccess::TOKEN_PATTERN
            :oauth_access
          when AuthenticationToken::TOKEN_PATTERN_V1
            :authentication_token
          end
      end
      # rubocop:enable GitHub/BooleanMemoizationWithOrOperator

      # given that this is a high traffic flow, we prefer not to log the
      # successes as we can rely on the stats from
      # GitHub::Authentication::Attempt#instrument
      def instrument_error(e)
        GitHub.dogstats.increment("token_lookup.find_actor", tags: [
          "token_type:#{token_type}",
          "result:failure",
          "error:#{e.class.name.demodulize.underscore}"
        ])
      end
    end
  end
end
