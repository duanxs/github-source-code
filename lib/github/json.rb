# rubocop:disable Style/FrozenStringLiteralComment

require "yajl"

module GitHub
  # Abstract away which JSON library gets used on the various platforms we support
  module JSON
    class InvalidEncoding < StandardError
    end

    # ActiveSupport::JSON expects this to be defined on all JSON backends.
    ParseError = Yajl::ParseError

    def encode(obj, options = {})
      options = Hash(options)

      json = Yajl::Encoder.encode(obj, options)
      json.force_encoding("UTF-8")

      if options[:warn_utf8] && !json.valid_encoding?
        error = InvalidEncoding.new("Encoded JSON object with invalid UTF8 bytes")
        GitHub::Logger.log_exception({
          class: error.class.name,
          json_object: obj.inspect,
          json_options: options.inspect,
        }, error)
        # raise the error without information from the JSON itself
        # more information can be found in splunk
        Failbot.report_user_error(error)
      end

      json.scrub!
    end
    alias_method :dump,  :encode

    def decode(str_or_io, options = {})
      options = Hash(options)

      Yajl::Parser.parse(str_or_io, options)
    rescue
      if !options[:retried] && $!.to_s =~ /utf/i && str_or_io.respond_to?(:scrub)
        options[:retried] = true
        str_or_io = str_or_io.scrub
        retry
      else
        raise
      end
    end
    alias_method :parse, :decode
    alias_method :load,  :decode

    extend self
  end

  module FailsafeJSON
    def load(value)
      GitHub::JSON.load(value)
    rescue Yajl::ParseError
      GitHub.dogstats.increment("session_serialization", tags: ["action:load", "error:invalid_json"])
      nil
    end

    def dump(value)
      GitHub::JSON.dump(value)
    end

    extend self
  end
end
