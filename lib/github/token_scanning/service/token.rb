# frozen_string_literal: true

# Wrapper class for the protobuf token class returned from the service.
#
# Implements the same methods as `TokenScanResult` so that is can be used
# interchangeably in the views.
class GitHub::TokenScanning::Service::Token
  attr_reader :first_location, :token

  delegate :id, :token_type, to: :token

  def initialize(token, repository)
    @token = token
    @first_location = GitHub::TokenScanning::Service::TokenLocation.new(token.first_location, repository)
  end

  def created_at
    token.created_at.to_time
  end

  def resolved_at
    token.resolved_at.to_time
  end

  def resolution
    @resolution ||= if token.resolution != :NO_RESOLUTION
      token.resolution.to_s.downcase
    else
      nil
    end
  end

  def resolver
    return @resolver if defined? @resolver

    @resolver = User.find_by_id(token.resolver_id)
  end

  def resolved?
    resolution.present? && !reopened?
  end

  def reopened?
    resolution == "reopened"
  end

  def found_in_archive?
    false
  end

  def locations
    [first_location]
  end

  def included_locations
    [first_location]
  end
end
