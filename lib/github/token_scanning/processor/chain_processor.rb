# frozen_string_literal: true

module GitHub
  module TokenScanning
    class ChainProcessor < Processor

      def initialize(*)
        super
        @processors = {}
      end

      def <<(token_hash)
        found_token = FoundToken.new(**token_hash)

        if processor = processor_factory(found_token)
          processor << found_token
        end
      end

      def process
        @processors.each_value(&:process)
      end

      def report_stats(namespace_tag_array)
        @processors.each_value do |pro|
          pro.report_stats(namespace_tag_array)
        end
      end

      private

      def processor_factory(found_token)
        klass = self.class.processor_for(found_token.type)
        return raise ArgumentError.new("No processor registered for #{found_token.type}.") if klass.nil?

        if proc = @processors[klass]
          return proc
        end

        @processors[klass] = klass.new(@target, @actor)
      end
    end
  end
end
