# frozen_string_literal: true

module GitHub
  module TokenScanning
    class AmazonAWSTokenProcessor < MultiTypeTokenProcessor
      FORMATS = Set.new(%w(AWS_SECRET AWS_KEYID)).freeze
      register_processor_for FORMATS

      # Processes found tokens.
      #
      # We see an incredible number of false positives for AWS_SECRET since it
      # is just 40 char hex and matches lots of things (ex. commit SHAs). To
      # prevent needless hooks we need to do some extra check before reporting
      # the found tokens. See the pre_process_* methods.
      #
      # Returns nothing.
      def process
        if process_repo_externally?
          pre_process_target_by_type_group_single
        else
          pre_process_target_by_type_group_all
        end
        super
      end

      def formats
        FORMATS
      end

    end
  end
end
