# frozen_string_literal: true

module GitHub
  module TokenScanning
    class GitHubOauthTokenProcessor < Processor
      FORMATS = Set.new(%w(GITHUB)).freeze

      register_processor_for FORMATS

      # Processes found tokens.
      #
      # Returns nothing.
      def process
        accesses = OauthAccess.for_tokens(@found_tokens.map(&:token))
        @found_tokens.each do |token|
          revoke_and_notify(token, accesses[token.token])
        end
      end

      # Revoke oauth access and notify the owner.
      #
      # token - The FoundToken instance that is associated with the oauth_access object
      # oauth_access - The OauthAccess associated with this found token, if any.
      #
      # Returns nothing.
      def revoke_and_notify(token, oauth_access)
        unless token.url.present?
          token.state = :no_url
          return
        end

        if oauth_access.nil?
          token.state = :no_oauth_access
          return
        end

        unless oauth_access.scopes&.any?
          token.state = :no_scopes
          return
        end

        if process_repo_externally?
          revoke(token, oauth_access)
        else
          token.state = :not_revoked
        end

        notify(token, oauth_access)
        token.mark_processed
      rescue => e
        token.error = e
        Failbot.report(e)
      end

      def revoke(token, oauth_access)
        oauth_access.destroy_with_explanation(:token_scan)
        token.state = :revoked
      end

      def notify(token, oauth_access)
        access_owner = oauth_access.user
        if in_gist?
          AccountMailer.access_token_leaked_in_gist(access_owner, token.url, process_repo_externally?).deliver_now
        else
          AccountMailer.access_token_leaked(access_owner, @target, token.url, process_repo_externally?).deliver_now
        end
      end
    end
  end
end
