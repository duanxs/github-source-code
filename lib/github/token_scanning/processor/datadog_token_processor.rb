# frozen_string_literal: true

module GitHub
  module TokenScanning
    class DatadogTokenProcessor < MultiTypeTokenProcessor
      FORMATS = Set.new(%w(DATADOG_NAME_PRESENCE DATADOG_TOKEN)).freeze
      register_processor_for FORMATS

      # Processes found tokens.
      #
      # We see an incredible number of false positives for DATADOG since it
      # is just 32 or 40 char hex and matches lots of things (ex. commit SHAs).
      # To prevent needless hooks we need to do some extra check before
      # reporting the found tokens. See the pre_process_target method.
      #
      # Returns nothing.
      def process
        pre_process_target_by_type_group_all
        super
      end

      def formats
        FORMATS
      end

    end
  end
end
