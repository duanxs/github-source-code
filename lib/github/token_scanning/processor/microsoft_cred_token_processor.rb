# frozen_string_literal: true

module GitHub
  module TokenScanning
    class MicrosoftCredTokenProcessor < MultiTypeTokenProcessor
      FORMAT_SETS = [
        Set.new(%w(MICROSOFT_AAD_USER_CREDENTIAL MICROSOFT_CREDENTIAL_PASSWORD)),
        Set.new(%w(MICROSOFT_INTERNAL_ACTIVE_DOMAIN_USER_CREDENTIAL MICROSOFT_CREDENTIAL_PASSWORD)),
      ].freeze
      register_processor_for(FORMAT_SETS.map(&:to_a).flatten.uniq)

      def process
        pre_process_by_format_sets
        super

      end

      def format_sets
        FORMAT_SETS
      end
    end
  end
end
