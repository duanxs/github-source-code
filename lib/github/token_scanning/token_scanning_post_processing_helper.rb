# frozen_string_literal: true

module GitHub
  module TokenScanning
    module TokenScanningPostProcessingHelper
      # Used to verify checksum of potential VSTS_PAT tokens to check if they are real PATs
      # Returns true if the token is valid and the checksums match
      def valid_vsts_pat_crc?(token)
        if token.length != 52
          return false
        end

        input_bytes = GitHub::Base32.decode(token.upcase).bytes.to_a

        #1. Extract out the bytes which are not the checksum
        token_bytes = []
        token_bytes += input_bytes[0, 2]
        token_bytes += input_bytes[3, 4]
        token_bytes += input_bytes[9, 13]
        token_bytes += input_bytes[23, 9]

        #2. Calculate checksum using magic number from VSTS_PAT generation
        crc32 = Zlib.crc32(token_bytes.pack("c*"))
        new_check_sum = crc32 ^ 0xE0B9692D

        #3. Extract the embedded checksum bytes from the input
        original_checksum_bytes = [];
        original_checksum_bytes << input_bytes[22];
        original_checksum_bytes << input_bytes[8];
        original_checksum_bytes << input_bytes[7];
        original_checksum_bytes << input_bytes[2];

        #4. Convert bytes array to unsigned int using pack and compare
        # Convert an array of bytes into its string representation which we then convert into an integer
        # eg.This converts bytes array like [250, 100, 192, 44] into "\xFAd\xC0," which is then converted into unsigned integer 750806266
        original_checksum = original_checksum_bytes.pack("c*").unpack("L*").first

        #5. Compare both checksums and return true for valid token
        new_check_sum == original_checksum
      end

      # Calculates a value representing the variability of a token
      # using the frequency of chars.
      # For example:
      #    'aaaa' has a variability value of 0
      #    'abcd' has a variability value of 2, which is the max it can be for a 4 char string
      #    'abab' and 'aabb' have variability values of 1
      #    'abcdefgh' has a variability value of 3, the max it can be for an 8 char string
      # Some folks call this Shannon entropy, but that is not what this actually is.
      # Shannon entropy is not calculated on values.
      # You could argue this is a form of entropy though, and it's what YARA defines as entropy
      # in its math library.
      def variability(token)
        token
          .each_char
          .group_by(&:itself)
          .transform_values { |array| array.length / token.length.to_f }
          .values
          .reduce(0) { |entropy, freq| entropy - freq * Math.log(freq, 2) }
      end

      def ssh_key_fingerprints(tokens)
        tokens.each_with_object(Hash.new) do |token, fprs|
          next if fprs.key?(token)

          fprs[token] = begin
            SSHData::PrivateKey.parse(token).first.public_key.fingerprint(md5: true)
          rescue SSHData::Error => e
            nil
          end
        end
      end

      def public_key_already_unverified?(public_key)
        !public_key.verified? && public_key.unverification_reason == "token_scan"
      end

      def unverify_and_notify_public_key(public_key, in_gist, url)
        public_key.unverify(:token_scan)

        # Send unique mailer for deploy keys.
        if public_key.repository_key?
          if in_gist
            RepositoryMailer.ssh_deploy_private_key_leaked_in_gist(public_key.repository, url).deliver_now
          else
            RepositoryMailer.ssh_deploy_private_key_leaked(public_key.repository, url).deliver_now
          end
        else
          if in_gist
            AccountMailer.ssh_private_key_leaked_in_gist(public_key.owner, url).deliver_now
          else
            AccountMailer.ssh_private_key_leaked(public_key.owner, url).deliver_now
          end
        end
      end

      def public_keys_for_fingerprints(fingerprints)
        PublicKey.
          includes(:user, :repository).
          where(fingerprint: fingerprints).
          index_by(&:fingerprint)
      end
    end
  end
end
