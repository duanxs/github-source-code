# frozen_string_literal: true

module GitHub
  module TokenScanning
    class Processor
      class_attribute :processor_registry, :default_processor
      self.processor_registry = Hash.new
      self.default_processor = nil

      def self.register_processor_for(formats)
        formats.each do |format|
          processor_registry[format] = self
        end
      end

      def self.processor_for(format)
        processor_registry[format] || default_processor
      end

      def self.register_default_processor
        Processor.default_processor = self
      end

      attr_reader :found_tokens

      def initialize(target, actor = nil)
        unless target.is_a?(Repository) || target.is_a?(Gist)
          raise ArgumentError.new("ChainProcessor's target needs to be a Gist or Repository")
        end
        @target = target
        @actor = actor
        @found_tokens = []
      end

      def <<(found_token)
        @found_tokens << found_token
      end

      def process
        raise "implement me"
      end

      def report_stats(namespace_tag_array)
        @found_tokens.each do |token|
          report_stats_for_token(namespace_tag_array, token)
        end
      end

      protected
      def report_stats_for_token(namespace_tag_array, token)
        # we are reporting analytics on repositories,
        # and for that we're uninterested in gists
        if !in_gist? && process_repo_externally?
          GlobalInstrumenter.instrument("token_scan.detection", {
            repository: @target,
            actor: @actor,
            processed: token.processed,
            error: token.error,
            token: {
              token_type: token.type,
              result: token.state,
              token_hash: token.token_hash,
              path: token.path,
              blob: token.blob,
              commit: token.commit,
              start_line: token.start_line || 0,
              end_line: token.end_line || 0,
              start_column: token.start_column || 0,
              end_column: token.end_column || 0,
            },
            file_extension: File.extname(token.url).sub(".", ""),
          })
        end

        token.report_stats(namespace_tag_array +
                           ["public:#{process_repo_externally?}", "gist: #{in_gist?}"])
      end

      def process_repo_externally?
        @target.process_found_tokens_externally?
      end

      def in_gist?
        @target.is_a?(Gist)
      end
    end
  end
end
