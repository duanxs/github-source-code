# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  class FlamegraphMiddleware
    INTERVAL_DEFAULT = 1000
    INTERVAL_MIN = 10

    def initialize(app)
      @app = app
    end

    # Note that we use request.GET rather than request.params throughout this
    # class because a) we only expect parameters to be passed to us through the
    # URL's query string, b) we don't want to spend time inspecting potentially
    # large POST bodies, and c) the POST bodies may be in encodings that Rack
    # doesn't expect but that our Rails app handles just fine. See
    # https://github.com/github/github/issues/31228.
    def call(env)
      request = Rack::Request.new(env)
      return @app.call(env) unless request.GET.has_key?("flamegraph")
      return @app.call(env) unless GitHub::StaffOnlyCookie.read(request.cookies)

      # Reset state from last time.
      StackProf.results

      mode = :wall
      case request.GET["flamegraph_mode"]
      when "cpu"
        mode = :cpu
      when "object"
        mode = :object
      end

      if request.GET["flamegraph_interval"].present?
        interval = request.GET["flamegraph_interval"].to_i
      else
        interval = INTERVAL_DEFAULT
      end
      interval = INTERVAL_MIN if interval < INTERVAL_MIN

      status = headers = body = nil
      data = StackProf.run(raw: true, aggregate: true, mode: mode, interval: interval) do
        status, headers, body = @app.call(env)
        body.close if body.respond_to?(:close)
      end

      out = StringIO.new
      report = StackProf::Report.new(data)

      if request.GET["flamegraph_output"] == "d3"
        report.print_d3_flamegraph(out)
        body = out.string
        headers["Content-Type"] = "text/html"
        filename = "flamegraph_#{request.path.gsub("/", "_")}_#{DateTime.now.strftime("%Y-%m-%d-%H-%M-%S")}.html"
        headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""
      elsif request.GET["flamegraph_output"] == "raw"
        report.print_dump(out)
        body = out.string
        headers["Content-Type"] = "text/binary"
        filename = "stackprof_#{request.path.gsub("/", "_")}_#{DateTime.now.strftime("%Y-%m-%d-%H-%M-%S")}.dump"
        headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""
      else
        report.print_timeline_flamegraph(out)
        body = out.string[11..-3]
        headers["Content-Type"] = "application/json"
      end
      # Make sure the actual response has a correct content-length instead
      # of whatever the wrapped app generated.
      headers["Content-Length"] = body.bytesize.to_s

      Rack::Response.new(body, status, headers).finish
    end

    class << self
      def enabled?
        GitHub.profiling_enabled? && defined?(StackProf)
      end
    end
  end
end
