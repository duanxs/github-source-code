# frozen_string_literal: true

module GitHub
  module Optimizely
    # The ::Experiment class is used as an interface with an Optimizely experiment
      # It has context about an experiment and the subject that can used to determine flows in logic
      # When you initialize and experiment with a subject, it will call get_variation which just grabs the variation the subject is in
      # You need to call #activate_subject! to have the subject counted towards results in Optimizely. Please ready docs on #activate_subject!
      # The preferred way to initialize ::Experiment is with our optimizely client from config:
        #   GitHub.optimizely.experiment#with_actor or
        #   GitHub.optimizely.experiment#with_visitor
    class Experiment
      class InvalidActorError < StandardError; end
      class InvalidVisitorError < StandardError; end

      # Initializing an ::Experiment
        # You need to call #activate_subject! to have the subject counted towards results in Optimizely. Please read docs on #activate_subject!
        # The preferred way to initialize ::Experiment is with:
        #   GitHub.optimizely.experiment#actor_experiment or
        #   GitHub.optimizely.experiment#visitor_experiment
        #
        # name - String
        #   The name of the experiment as it appears in Optimizely
        #
        # subject - Object
        #   The subject that will be used to activate/get variant against. Typically a User/Organization/Analytics::Visitor
        #   If visitor_experiment is true
        #     then the subject must be a Analytics::Visitor
        #   If visitor_experiment is false
        #     then the subject must be an Object that has an analytics_tracking_id
        #
        # attributes - Hash
        #   A hash where the keys are attributes that can be used for user segmentation in Optimizely
        #   See: https://docs.developers.optimizely.com/full-stack/docs/define-attributes
        #
        # visitor_experiment - Boolean
        #   Is this experiment where a logged out visitor must be used
        #   or an experiment that is run on subjects when they are logged out and logged in?
        #
        # passed_checks - Boolean
        #   A boolean that represents any custom checks that should pass before
        #   activating a subject or returning any variation checks
      def initialize(name:, subject:, attributes:, visitor_experiment:, passed_checks: true)
        visitor_experiment ? self.class.valid_visitor!(subject) : self.class.valid_actor!(subject)

        @name = name.to_s
        @subject = subject
        @attributes = attributes
        @visitor_experiment = visitor_experiment
        # enteprise server is a default check
        @passed_checks = passed_checks && !GitHub.enterprise?
        @variant = @passed_checks ? get_variation_subject : nil
      end

      # String - name of the experiment as it appears in Optimizely
      attr_reader :name

      # Object - subject that will be used to activate/get variant against
      attr_reader :subject

      # Hash - attributes that used for user segmentation in Optimizely
      attr_reader :attributes

      # Boolean - is this an experiment that needs to consider logged out context
      attr_reader :visitor_experiment
      alias_method :visitor_experiment?, :visitor_experiment

      # Boolean - when initialized did external custom checks pass?
      attr_reader :passed_checks
      alias_method :passed_checks?, :passed_checks

      # String or Nil - the value returned by Optimizely for the subjects variation as string
      # or nil if the experiment is not running, if the user is not in the experiment, or if the datafile is invalid.
      attr_reader :variant

      # Returns Boolean
        # Does this user have a variant
      def variant?
        @variant.present?
      end

      def activated?
        @activated
      end

      # Returns the ::Experiment instance
        # Pass array of variations to check the subjects variation against and executes block if subject is in the variant
        # This returns the ::Experiment instance so you can chain #variations
        # If @passed_checks is false, nothing will execute
        #
        # enabled_variations - Array of symbols, strings, or nil
        #   What variations should we check the users @variant against?
        #   Use nil when you want to do something for users with no variant (
        #     this could be because of a number of reasons: the experiment is not running, user does not qualify, datafile is invalid.
        #
        # block - Block
        #   Executes if the subject is in any of the variations
        #
        # Examples of chaining #variations
        #   ::Experiment
        #     .variations([:variant_alpha]) {
        #       do stuff for this variation
        #     }
        #     .variations([:variant_beta, :variant_gamma]) {
        #       do stuff for these two variations
        #     }
        #     .variations([nil]) {
        #       do something when user is not enrolled
        #     }
      def variations(enabled_variations, &block)
        return self if !@passed_checks
        if block_given? && in_variations?(enabled_variations)
          block.call
        end

        self
      end

      # Returns Boolean
        # Is the user in any of the variations provided?
        #   If nil is provided, it returns true if the user has no variant
        #
        # enabled_variations - Array of symbols, strings, or nil
        #   What variations should we check the users enrollment against?
        #
        # Examples
        #   ::Experiment.in_variations?([:variant_alpha]) => True/False
        #   ::Experiment.in_variations?([:variant_alpha, nil]) => True/False
      def in_variations?(enabled_variations)
        return false if !@passed_checks
        enabled_variations.any? { |variant| in_variant?(variant) }
      end

      # Returns Boolean
        # Is the user in the variation provided?
        #   If nil is provided, it returns true if the user has no variant
        #
        # variation - Symbol or String
        #   What variation should we check the user against?
        #
        # Examples
        #   ::Experiment.in_variant?(:variant_alpha) => True/False
        #   ::Experiment.in_variant?(nil) => True/False
      def in_variant?(variation)
        return false if !@passed_checks
        return true if enabled_via_force_flag?(variation)

        expected_variation = variation.nil? ? variation : variation.to_s
        @variant == expected_variation
      end

      # Return Boolean !- IMPORTANT -!
        # Activates the subject in Optimizely via GitHub.optimizely#activate
        # Activating the subject in Optimizely will count them towards results.
        # Activating consumes an impression which we have a finite amount of and is what we are billed for
        # https://docs.developers.optimizely.com/full-stack/docs/choose-impressions
        # https://docs.developers.optimizely.com/full-stack/docs/impressions
        # To prevent extraneous activate calls you can only activate users that:
        #     1. Pass @passed_checks
        #     2. Has not already been activated (in memory)
        #  You should be explicit and conservative in calling #activate_subject!
        #     - To have the subject count towards results a only user needs to be activated once per experiment, per 24 hours.
        #     - This means you should activate at the first exposure point for the experiment and be set
        #  If you just need to check the variation for the user use: #variant or #get_variation_subject
      def activate_subject!
        return false if !@passed_checks || @activated

        if @visitor_experiment
          self.class.activate_visitor(experiment: @name, visitor: @subject, attributes: @attributes)
        else
          self.class.activate_actor(experiment: @name, actor: @subject, attributes: @attributes)
        end

        @activated = true
        @activated
      end

      # Return String or Nil
        # Get the variations the subject is in Optimizely via GitHub.optimizely#get_variation
        # Does not activate the user in Optimizely
      def get_variation_subject
        return if !@passed_checks

        if @visitor_experiment
          self.class.get_variation_visitor(experiment: @name, visitor: @subject, attributes: @attributes)
        else
          self.class.get_variation_actor(experiment: @name, actor: @subject, attributes: @attributes)
        end
      end

      # Return Nil
        # Tracks an event for the subject in Optimizely via GitHub.optimizely#track
        # Subject must have been @activated in order to track them correctly in Optimizely
        # event_name - String or Symbol
        #   The event name as it appears in Optimizely
        #
        # attributes - Hash
        #   Used for user segmentation in Optimizely. @attributes will be sent by default if not provided an argument.
        #
        # tags - Hash
        #   Used to enrich event data in Optimizely. revenue and value are reserved tag keys, please read docs before using.
        #   See: https://docs.developers.optimizely.com/full-stack/docs/include-event-tags
      def track_subject(event_name:, attributes: @attributes, tags: {})
        return if !@passed_checks || !activated?

        if @visitor_experiment
          self.class.track_visitor(event_name: event_name, visitor: @subject, attributes: attributes, tags: tags)
        else
          self.class.track_actor(event_name: event_name, actor: @subject, attributes: attributes, tags: tags)
        end
      end

      # Returns Boolean
        # Are there any dynamically generated feature flags enabled that will force a true return by
        # checking for a feature flag of the same name as your experiment and variation.
        #
        # This is for testing purposes as an easier way to ensure we can check different flows.
        # To force a variation enable a flag in the format: #{experiment_name}_as_#{variation}
        #
        # variations - String or symbol
      def enabled_via_force_flag?(variation)
        flag_subject = if @visitor_experiment
          User::CurrentVisitorActor.from_current_visitor(@subject.octolytics_id)
        else
          @subject
        end

        feature = "#{@name}_as_#{variation}"
        GitHub.flipper[feature].enabled?(flag_subject)
      end

      # Class method - Returns ::Experiment
        # Initialize a new experiment with an actor as the subject. This is for experiments contained after logged in boundary.
        #
        # name - String
        #   The name of the experiment as it appears in Optimizely
        #
        # actor - Object
        #   Must respond to #analytics_tracking_id
        #
        # attributes - Hash
        #   Attributes that used for user segmentation in Optimizely
        #
        # passed_checks - Boolean
        #   Any external checks to be passed before enrolling the actor
        #
        # block
        #   Run the external checks as a block which returns will be passed as passed_checks
        #
        # Examples:
        #   passed_checks as a boolean:
        #     GitHub::Optimizely::Experiment.with_actor(
        #       name: "experiment_ab",
        #       actor: some_user,
        #       attributes: { segment_key: "value"},
        #       passed_checks: true
        #     )
        #   passed_checks as a block:
        #     GitHub::Optimizely::Experiment.with_actor(
        #       name: "experiment_ab",
        #       actor: some_user,
        #       attributes: { segment_key: "value"}
        #     ) { some_user.plan.free? }
      def self.with_actor(name:, actor:, attributes: {}, passed_checks: true, &block)
        checks = if block_given?
          block.call
        else
          passed_checks
        end

        self.new(name: name, subject: actor, attributes: attributes, visitor_experiment: false, passed_checks: checks)
      end

      # Class method - Returns ::Experiment
        # Initialize a new experiment with a visitor as the subject. This is for experiments that touch logged out context.
        #
        # name - String
        #   The name of the experiment as it appears in Optimizely
        #
        # visitor - Object
        #   Must respond to #octolytics_id. Typically the current_visitor.
        #
        # attributes - Hash
        #   Attributes that used for user segmentation in Optimizely
        #
        # passed_checks - Boolean
        #   Any external checks to be passed before enrolling the actor
        #
        # block
        #   Run the external checks as a block which returns will be passed as passed_checks
        #
        # Examples:
        #   passed_checks as a boolean:
        #     GitHub::Optimizely::Experiment.with_actor(
        #       name: "experiment_ab",
        #       visitor: current_visitor,
        #       attributes: { segment_key: "value"},
        #       passed_checks: true
        #     )
        #   passed_checks as a block:
        #     GitHub::Optimizely::Experiment.with_actor(
        #       name: "experiment_ab",
        #       visitor: current_visitor,
        #       attributes: { segment_key: "value"}
        #     ) { true == true }
      def self.with_visitor(name:, visitor:, attributes: {}, passed_checks: true, &block)
        checks = if block_given?
          block.call
        else
          passed_checks
        end

        self.new(name: name, subject: visitor, attributes: attributes, visitor_experiment: true, passed_checks: checks)
      end

      # Class method - Returns String or Nil
        # The variation or nil for an actor that responds to analytics_tracking_id.
        #
        # experiment - String or Symbol
        #  The name of the experiment as it appears in Optimizely.
        #
        # actor - Object
        #  Object that responds to analytics_tracking_id for which you want to check their variation. Typically a User or Organization.
        #
        # attributes - Hash
        #  A hash where keys are attributes that can be used for user segmentation in Optimizely.
        #  See: https://docs.developers.optimizely.com/full-stack/docs/define-attributes
      def self.activate_actor(experiment:, actor:, attributes: {})
        valid_actor!(actor)
        GitHub.optimizely.activate(experiment.to_s, actor.analytics_tracking_id, attributes)
      end

      # Class method - Returns String or Nil
        # The variation or nil for a visitor that responds to octolytics_id.
        #
        # experiment - String or Symbol
        #  The name of the experiment as it appears in Optimizely.
        #
        # actor - Analytics::Visitor
        #  Analytics::Visitor that responds to octolytics_id for which you want to check their variation. Typically the set current_visitor for the session.
        #
        # attributes - Hash
        #  A hash where keys are attributes that can be used for user segmentation in Optimizely.
        #  See: https://docs.developers.optimizely.com/full-stack/docs/define-attributes
      def self.activate_visitor(experiment:, visitor:, attributes: {})
        valid_visitor!(visitor)
        GitHub.optimizely.activate(experiment.to_s, visitor.octolytics_id, attributes)
      end

      # Class method - Returns String or Nil
        # The variation or nil for an actor that responds to analytics_tracking_id.
        # Get the variation of an actor without activating them in Optimizely
        #
        # experiment - String or Symbol
        #  The name of the experiment as it appears in Optimizely.
        #
        # actor - Object
        #  Object that responds to analytics_tracking_id for which you want to check their variation. Typically a User or Organization.
        #
        # attributes - Hash
        #  A hash where keys are attributes that can be used for user segmentation in Optimizely.
        #  See: https://docs.developers.optimizely.com/full-stack/docs/define-attributes
      def self.get_variation_actor(experiment:, actor:, attributes: {})
        valid_actor!(actor)
        GitHub.optimizely.get_variation(experiment.to_s, actor.analytics_tracking_id, attributes)
      end

      # Class method - Returns String or Nil
        # The variation or nil for a visitor that responds to octolytics_id
        # Get the variation of a visitor without activating them in Optimizely
        #
        # experiment - String or Symbol
        #  The name of the experiment as it appears in Optimizely.
        #
        # actor - Analytics::Visitor
        #  Analytics::Visitor that responds to octolytics_id for which you want to check their variation. Typically the set current_visitor for the session.
        #
        # attributes - Hash
        #  A hash where keys are attributes that can be used for user segmentation in Optimizely.
        #  See: https://docs.developers.optimizely.com/full-stack/docs/define-attributes
      def self.get_variation_visitor(experiment:, visitor:, attributes: {})
        valid_visitor!(visitor)
        GitHub.optimizely.get_variation(experiment.to_s, visitor.octolytics_id, attributes)
      end

      # Class method - Returns Nil
        # Track an event for an actor that responds to analytics_tracking_id.
        #
        # event_name - String or Symbol
        #  The name of the event as it appears in Optimizely.
        #
        # actor - Object
        #  Object that responds to analytics_tracking_id for which you want to check their variation. Typically a User or Organization.
        #
        # attributes - Hash
        #  A hash where keys are attributes that can be used for user segmentation in Optimizely.
        #  See: https://docs.developers.optimizely.com/full-stack/docs/define-attributes
        #
        # tags - Hash
        #   Used to enrich event data in Optimizely. revenue and value are reserved tag keys, please read docs before using.
        #   See: https://docs.developers.optimizely.com/full-stack/docs/include-event-tags
      def self.track_actor(event_name:, actor:, attributes: {}, tags: {})
        valid_actor!(actor)
        GitHub.optimizely.track(event_name.to_s, actor.analytics_tracking_id, attributes, tags)
      end

      # Class method - Returns Nil
        # Track an event for a visitor that responds to octolytics_id.
        #
        # event_name - String or Symbol
        #  The name of the event as it appears in Optimizely.
        #
        # visitor - Analytics::Visitor
        #  Analytics::Visitor that responds to octolytics_id for which you want to check their variation. Typically the set current_visitor for the session.
        #
        # attributes - Hash
        #  A hash where keys are attributes that can be used for user segmentation in Optimizely.
        #  See: https://docs.developers.optimizely.com/full-stack/docs/define-attributes
        #
        # tags - Hash
        #   Used to enrich event data in Optimizely. revenue and value are reserved tag keys, please read docs before using.
        #   See: https://docs.developers.optimizely.com/full-stack/docs/include-event-tags
      def self.track_visitor(event_name:, visitor:, attributes: {}, tags: {})
        valid_visitor!(visitor)
        GitHub.optimizely.track(event_name.to_s, visitor.octolytics_id, attributes, tags)
      end

      # Class method - Returns InvalidActorError or Nil
      def self.valid_actor!(actor)
        raise InvalidActorError if !actor.respond_to?(:analytics_tracking_id)
      end

      # Class method - Returns InvalidVisitorError or Nil
      def self.valid_visitor!(visitor)
        raise InvalidVisitorError if !visitor.respond_to?(:octolytics_id)
      end
    end
  end
end
