# frozen_string_literal: true

require "optimizely"

require "github/optimizely/async_event_processor"
require "github/optimizely/config"
require "github/optimizely/error_handler"
require "github/optimizely/event_dispatcher"
require "github/optimizely/http_auth_datafile"
require "github/optimizely/logger"
require "github/optimizely/notifications_relay"
require "github/optimizely/polls_project_config"
require "github/optimizely/experiment"

module GitHub
  module Optimizely
    class Client < SimpleDelegator

      # This method signature is overly verbose as a means of documenting the
      # various parameters accepted by the Optimizely constructor.
      # The defaults provided by our wrapper are the desired values for our
      # application, but remain as parameters for documentation.
      # Once initial development settles, we can trim this up.
      def initialize(
        sdk_key: Config.sdk_key,
        datafile: nil,
        skip_json_validation: false,
        user_profile_service: nil,
        logger: GitHub::Optimizely::Logger.new(progname: "Optimizely"),
        error_handler: GitHub::Optimizely::ErrorHandler.new,
        notification_center: ::Optimizely::NotificationCenter.new(logger, error_handler),
        event_dispatcher: GitHub::Optimizely::EventDispatcher.new(logger),
        event_processor: GitHub::Optimizely::AsyncEventProcessor.new(
          event_dispatcher: event_dispatcher,
          logger: logger,
          notification_center: notification_center
        )
      )

        @http_client = GitHub::Optimizely::HTTPAuthDatafile.new(
          sdk_key: sdk_key,
          datafile: datafile,
          logger: logger,
          error_handler: error_handler,
          skip_json_validation: skip_json_validation,
          notification_center: notification_center,
        )

        @config_manager = GitHub::Optimizely::PollsProjectConfig.new(
          @http_client,
          blocking_timeout: 1.second,
          logger: logger,
        )

        relay_notifications_to(GitHub).from(notification_center)

        super ::Optimizely::Project.new(datafile, event_dispatcher, logger, error_handler, skip_json_validation, user_profile_service, sdk_key, @config_manager, notification_center, event_processor)
      end

      def raw_datafile
        @http_client.raw_datafile || "{}"
      end

      def experiment
        GitHub::Optimizely::Experiment
      end

      private

      def relay_notifications_to(service)
        NotificationsRelay.new(service)
      end

      class InstrumentedHTTPProjectConfigManager < ::Optimizely::HTTPProjectConfigManager
        private

        def request_config
          GitHub.dogstats.time("optimizely.http_datafile", opts = {tags: []}) do
            super.tap { |config| opts[:tags] << "result:#{config.nil? ? "failure" : "success" }" }
          end
        end
      end
    end
  end
end
