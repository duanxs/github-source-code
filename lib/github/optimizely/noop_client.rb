# frozen_string_literal: true

# Used in enterprise instances and test/dev environments, whenever Optimizely is not enabled, or on failed instantiation of a client.
# Methods here must mirror GitHub::Optimizely::Client
module GitHub
  module Optimizely
    class NoopClient
      def activate(*); end
      def track(*); end
      def get_variation(*); end

      def experiment(*)
        GitHub::Optimizely::Experiment
      end

      def raw_datafile(*)
         "{}"
      end
    end
  end
end
