# frozen_string_literal: true

require "logger"

require "optimizely/event/event_factory"
require "optimizely/logger"

module GitHub
  module Optimizely
    class AsyncEventProcessor
      def initialize(event_dispatcher:, notification_center:, logger: ::Optimizely::NoOpLogger.new)
        @event_dispatcher = event_dispatcher
        @logger = logger
        @notification_center = notification_center
      end

      def process(user_event)
        @logger.log(::Logger::DEBUG, "Received userEvent: #{user_event}")

        Thread.new do
          log_event = ::Optimizely::EventFactory.create_log_event(user_event, @logger)

          @event_dispatcher.dispatch_event(log_event)

          @notification_center.send_notifications(
            ::Optimizely::NotificationCenter::NOTIFICATION_TYPES[:LOG_EVENT],
            log_event)
        rescue => e
          @logger.log(::Logger::ERROR, "Error dispatching event: #{log_event} #{e.message}.")
        end
      end
    end
  end
end
