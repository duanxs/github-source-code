# frozen_string_literal: true

require "logger"
require "net/http"

require "optimizely/config/datafile_project_config"
require "optimizely/error_handler"
require "optimizely/exceptions"
require "optimizely/logger"
require "optimizely/notification_center"

module GitHub
  module Optimizely
    class HTTPDatafile
      attr_reader :raw_datafile

      DATAFILE_URL_TEMPLATE = "https://cdn.optimizely.com/datafiles/%s.json"
      REQUEST_TIMEOUT = 10.seconds

      # Config manager that polls for the datafile and updated ProjectConfig based on an update interval.


      # Initialize config manager. One of sdk_key or url has to be set to be able to use.
      #
      # sdk_key - Optional string uniquely identifying the datafile. It's required unless a URL is passed in.
      # datafile: Optional JSON string representing the project.
      # url - Optional string representing URL from where to fetch the datafile. If set it supersedes the sdk_key.
      # url_template - Optional string template which in conjunction with sdk_key
      #               determines URL from where to fetch the datafile.
      # logger - Provides a logger instance.
      # error_handler - Provides a handle_error method to handle exceptions.
      # skip_json_validation - Optional boolean param which allows skipping JSON schema
      #                       validation upon object invocation. By default JSON schema validation will be performed.
      def initialize(
        sdk_key: nil,
        url_template: DATAFILE_URL_TEMPLATE,
        url: derive_url(sdk_key, url_template),
        datafile: nil,
        skip_json_validation: false,
        logger: ::Optimizely::NoOpLogger.new,
        error_handler: ::Optimizely::NoOpErrorHandler.new,
        notification_center: ::Optimizely::NotificationCenter.new(logger, error_handler),
        redirect_limit: 5,
        **
      )
        @logger = logger
        @error_handler = error_handler
        @skip_json_validation = skip_json_validation
        @notification_center = notification_center
        @redirect_limit = redirect_limit
        @raw_datafile = datafile
        self.datafile_uri = url
        self.config = config_from_datafile(datafile)
      end

      def config
        self.config = config_from_datafile(fetch(datafile_request))
        @config
      end

      private

      def config=(new_config)
        return if new_config.nil? || same_revision?(@config, new_config)

        old_config = @config
        @config = new_config

        @notification_center.send_notifications(::Optimizely::NotificationCenter::NOTIFICATION_TYPES[:OPTIMIZELY_CONFIG_UPDATE])
        @logger.log(::Logger::DEBUG, "Received new datafile and updated config. " \
                    "Old revision number: #{old_config&.revision}. New revision number: #{new_config&.revision}.")
      end

      def config_from_datafile(datafile)
        if datafile
          # Keep raw datafile as json string in memory until we move the datafile storage somewhere else
          @raw_datafile = datafile
          ::Optimizely::DatafileProjectConfig.create(datafile, @logger, @error_handler, @skip_json_validation)
        end
      end

      def datafile_uri=(url)
        @datafile_uri = URI(url)
      end

      def datafile_request(uri = @datafile_uri)
        Net::HTTP::Get.new(uri).tap { |req|
          req["Content-Type"] = "application/json"
          req["If-Modified-Since"] = @last_modified
        }
      end

      def fetch(request, limit: @redirect_limit)
        @logger.log(::Logger::DEBUG, "Fetching datafile from #{request.uri}")

        Net::HTTP.new(request.uri.host, request.uri.port).tap { |http|
          http.max_retries = 1
          http.read_timeout = REQUEST_TIMEOUT
          http.use_ssl = request.uri.scheme == "https"
        }.request(request) { |response|
          case response
          when Net::HTTPNotModified
            @logger.log(::Logger::DEBUG, "Not updating config as datafile has not updated since #{@last_modified}.")
            return
          when Net::HTTPRedirection
            location = response["Location"]
            raise Net::HTTPRetriableError.new("HTTP redirects too deep", response) unless limit > 1
            @logger.log(::Logger::WARN, "Datafile has moved permanently to #{location}") if permanent_redirect?(response)
            return fetch(datafile_request(URI(location)), limit: limit-1) if location
          end

          response.value # raises http errors
          @last_modified = response["Last-Modified"]
        }.body
      end

      def permanent_redirect?(response)
        response.is_a?(Net::HTTPPermanentRedirect) || response.is_a?(Net::HTTPMovedPermanently)
      end

      def same_revision?(current, new_config)
        new_config&.revision &&
          current&.revision == new_config&.revision
      end

      # Determines URL from whence to fetch the datafile.
      # sdk_key - Key uniquely identifying the datafile.
      # url_template - String representing template which is filled in with
      #               SDK key to determine URL from which to fetch the datafile.
      # Returns url String
      def derive_url(sdk_key, url_template)
        raise ::Optimizely::InvalidInputsError, "Must provide at least one of sdk_key or url." unless sdk_key

        url_template % sdk_key
      end
    end
  end
end
