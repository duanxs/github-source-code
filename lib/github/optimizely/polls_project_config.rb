# frozen_string_literal: true

require "logger"

require "optimizely/config_manager/async_scheduler"

module GitHub
  module Optimizely
    # Config manager that polls for the datafile and updated ProjectConfig based on an update interval.
    class PollsProjectConfig
      DEFAULT_UPDATE_INTERVAL = 5.minutes
      DEFAULT_BLOCKING_TIMEOUT = 15.seconds

      # polling_interval - Optional floating point number representing time interval in seconds at which to request datafile and set ProjectConfig.
      # blocking_timeout - Optional Time in seconds to block the config call until config object has been initialized.
      # start_by_default - Boolean indicates to start by default AsyncScheduler.
      # logger - Provides a logger instance.
      def initialize(
        config_manager,
        logger:,
        start_by_default: true,
        blocking_timeout: DEFAULT_BLOCKING_TIMEOUT,
        polling_interval: DEFAULT_UPDATE_INTERVAL
      )
        @config_manager = config_manager
        @logger = logger
        @blocking_timeout = blocking_timeout
        @mutex = Mutex.new
        @resource = ConditionVariable.new

        @async_scheduler = ::Optimizely::AsyncScheduler.new(method(:poll), polling_interval, true, @logger)
        start! if start_by_default
      end

      def ready?
        !!@config
      end

      def start!
        @async_scheduler.start!
      end

      def stop!
        @async_scheduler.stop!
      end

      # Get Project Config.

      # Return current config immediately if available.
      # Wait for scheduler to fetch the config  (max: blocking_timeout) if running.
      # Otherwise manually trigger a poll/fetch.
      def config
        return @config if ready?

        if @async_scheduler.running
          @mutex.synchronize do
            @resource.wait(@mutex, @blocking_timeout)
            return @config
          end
        end

        poll
      end

      private

      def poll
        @config_manager.config.tap do |config|
          return @config unless config

          @config = config
          @mutex.synchronize { @resource.signal } unless ready?
        end
      end
    end
  end
end
