# frozen_string_literal: true

require "optimizely/notification_center"

module GitHub
  module Optimizely
    EVENTS = ::Optimizely::NotificationCenter::NOTIFICATION_TYPES

    class NotificationsRelay

      def initialize(service, prefix: "optimizely")
        @service = service
        @prefix = prefix
      end

      def call(event, *args)
        @service.instrument(name(event), args)
      end

      def proxy(notification_center)
        Proxy.new(self).tap do |proxy|
          EVENTS.each do |key, value|
            notification_center.add_notification_listener(value, proxy.method(key))
          end
        end
      end
      alias :from :proxy

      private

      def name(event)
        [@prefix, event.to_s.downcase].join(".")
      end

      # This class exposes, as instance methods, each of the notification types
      # that may be emitted by the Optimizely NotificationCenter.
      # This class is only necessary because the NotificationCenter can only
      # accept Method objects as listeners.
      class Proxy
        def initialize(relay)
          @relay = relay
        end

        EVENTS.keys.each do |key|
          define_method key do |*args|
            @relay.call(key, *args)
          end
        end
      end
    end
  end
end
