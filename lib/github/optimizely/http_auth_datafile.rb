# frozen_string_literal: true

require "github/optimizely/config"
require "github/optimizely/http_datafile"

module GitHub
  module Optimizely
    class HTTPAuthDatafile < HTTPDatafile
      DATAFILE_URL_TEMPLATE = "https://cdn.optimizely.com/datafiles/auth/%s.json"

      def initialize(
        url_template: DATAFILE_URL_TEMPLATE,
        auth_key: Config.auth_key,
        **)
        super
        @auth_key = auth_key
      end

      private

      def datafile_request(uri = @datafile_uri)
        super.tap { |req|
          req["X-Optly-Auth"] = @auth_key
        }
      end
    end
  end
end
