# rubocop:disable Style/FrozenStringLiteralComment

module GitHub::Goomba
  # Adds the "contains-task-list" class to list elements that contain task list items,
  # and the "task-list-item" class to list item elements that are task list
  # items. Replaces task list item markers (`[ ]` and `[x]`) with checkboxes.
  #
  # Syntax
  # ------
  #
  # Task list items must be in a list format:
  #
  # ```
  # - [ ] incomplete
  # - [x] complete
  # ```
  #
  # Results
  # -------
  #
  # The following keys are written to the result hash:
  #   :task_list_items - An array of TaskList::Item objects.
  class TaskListFilter < NodeFilter
    SELECTOR = Goomba::Selector.new([
      "ol",
      "ul",
      "li > :text:-goomba-first-child-node",
      "li > p:first-child > :text:-goomba-first-child-node",
    ].join(", "))

    def initialize(*args)
      super
      @filter = TaskList::Filter.new("", context, result)
    end

    def selector
      SELECTOR
    end

    def call(node)
      if node.is_a?(Goomba::ElementNode)
        call_list(node)
      else
        call_text(node)
      end
    end

    protected

    def call_list(element)
      items = element.children.select { |child| task_list_item?(child) }
      return if items.empty?

      items.each do |item|
        add_class(item, "task-list-item")
      end
      add_class(element, "contains-task-list")

      element
    end

    def call_text(text)
      text.html.sub!(TaskList::Filter::ItemPatternParser) do |match|
        item = TaskList::Item.new($1)
        @filter.task_list_items << item
        @filter.render_item_checkbox(item)
      end
    end

    def task_list_item?(element)
      return false unless element.is_a?(Goomba::ElementNode)
      return false unless element.tag == :li

      first_child = element.children.first
      if first_child.is_a?(Goomba::TextNode) && first_child.inner_html == "\n"
        first_child = first_child.next_sibling
      end
      if first_child.is_a?(Goomba::ElementNode) && first_child.tag == :p
        first_child = first_child.children.first
      end
      return false unless first_child.is_a?(Goomba::TextNode)

      !!(first_child.html =~ TaskList::Filter::ItemPatternParser)
    end

    def add_class(element, klass)
      element["class"] = [element["class"], klass].compact.join(" ")
    end
  end
end
