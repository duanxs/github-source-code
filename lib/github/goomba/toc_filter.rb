# frozen_string_literal: true

module GitHub::Goomba
  # Goomba NodeFilter to replace `{toc}` markers with a Table of Contents
  # ordered list generated from the document headings.
  #
  # As it is presently implemented directly in the Goomba markup pipeline,
  # as opposed to being directly in the markdown parsing gem,
  # means the feature works for asciidoc, textile, and other prose markup docs,
  # not just markdown.
  #
  # The `{:toc}` marker is taken from Kramdown.
  # We match it precisely; there can exist whitespace outside the braces,
  # but not within the braces (see the TOC_MARKER regex below).
  #
  # Despite taking the marker from Kramdown, the current implementation
  # does not support any other TOC-related features from kramdown, such as:
  #
  # - levels option: toc_levels
  # - using ol vs ul for the TOC based on the context of the marker
  # - excluding specific headers from the TOC via {:.no_toc} marker
  #
  # In further contrast with Kramdown, the current implementation allows the
  # `{:toc}` marker to be in `p` elements; kramdown requires the marker to be
  # in a `ul` or `ol`.
  #
  # Presently, this filter is tightly coupled to the prior existing
  # TableOfContentsFilter. This filter must run before TableOfContentsFilter
  # in the pipeline.
  # 1. this filter adds an empty headings hierarchy object to the scratch
  # object during the scan phase (if TOC markers are present in the doc)
  # 2. the TableOfContentsFilter adds headings to the scratch object during
  # the scan phase (if the headings hierarchy object exists)
  # 3. Then in the call phase, this filter emits a generated TOC based
  # on the heading structure collected in the scratch object.
  #
  # The existing TableOfContentsFilter is poorly named as it doesn't actually
  # generate a TOC. However, we are leaving the two filters separate (and
  # poorly named) until we are sure the TOC feature will remain.
  # At which point, these two filters can and should be merged into one.
  # (Or their names should be changed to better reflect their roles.)
  class TocFilter < NodeFilter
    SELECTOR = Goomba::Selector.new("ol, ul, p")
    TOC_MARKER = /^\s*\{:toc\}\s*$/

    def self.enabled?(context)
      !context[:for_email] &&
          context.has_key?(:entity) &&
          context[:entity].is_a?(Repository) &&
          context[:entity].private? &&
          GitHub.flipper[:markdown_toc].enabled?(context[:entity])
    end

    def selector
      SELECTOR
    end

    def scan(document)
      scratch[:toc_headers] = Headings.new if needs_toc?(document)
    end

    def call(element)
      return unless toc_placeholder? element

      tag.details {
        tag.summary("Table of Contents") +
        tag.ol {
          (scratch[:toc_headers] || [])
            .map(&method(:list_item))
            .reduce(&:+)
        }
      }
    end

    private

    def needs_toc?(document)
      document.select(SELECTOR).any?(&method(:toc_placeholder?))
    end

    def toc_placeholder?(element)
      TOC_MARKER.match? element.text_content
    end

    def list_item(heading)
      tag.li {
        tag.a(href: "#" + heading.anchor) {
          heading.text
        }.concat tag.ol {
          heading.map(&method(:list_item)).reduce(&:+)
        }
      }
    end

    def tag
      @tag ||= ActionController::Base.helpers.tag
    end
  end
end
