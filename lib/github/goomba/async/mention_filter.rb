# frozen_string_literal: true

module GitHub::Goomba::Async
  class MentionFilter < NodeFilter
    SELECTOR = Goomba::Selector.new(match: "gh|user-mention")
    MENTION_LIMIT = 50

    def initialize(*args)
      super
      @user_cache = {}
    end

    def selector
      SELECTOR
    end

    def async_scan
      logins = @nodes.map { |node| node["login"].downcase }.uniq
      if prevent_mention_spam?
        logins = logins[0, MENTION_LIMIT]
      end

      Platform::Loaders::ActiveRecord.load_all(::User, logins, column: :login, case_sensitive: false).then do |users|
        users.each do |user|
          if user
            @user_cache[user.login.downcase] = user
          end
        end
      end.then do
        result[:mentioned_users] = @user_cache.values
        result[:mentioned_usernames] = @user_cache.keys
      end
    end

    def call(node)
      login = node["login"]

      if user = @user_cache[login.downcase]
        url = helpers.user_url(user)
        helpers.profile_link(user, class: "user-mention", url: url) { "@#{user}" }
      else
        "@#{login}"
      end
    end

    private

    def helpers
      ApplicationController.new.view_context
    end

    def prevent_mention_spam?
      return @prevent_spam if defined?(@prevent_spam)
      @prevent_spam = GitHub.prevent_mention_spam? && public?
    end

    def public?
      return true unless entity
      !entity.private?
    end
  end
end
