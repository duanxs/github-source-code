# frozen_string_literal: true

module GitHub::Goomba
  class Async::NodeFilter < NodeFilter
    def initialize(*args)
      super
      @nodes = []
    end

    def add_node(node)
      @nodes << node
    end

    def async_scan
      raise NotImplementedError
    end

    def async_find_repository(owner_or_nwo)
      case owner_or_nwo
      when nil
        Promise.resolve(repository)
      when /\//
        owner, name = owner_or_nwo.split("/")
        Platform::Loaders::ActiveRecord.load(::User, owner, column: :login, case_sensitive: false).then do |user|
          if user
            Platform::Loaders::RepositoryByName.load(user.id, name)
          end
        end.then do |repo|
          repo || Platform::Loaders::RedirectedRepositoryByNwo.load(owner_or_nwo)
        end
      else
        if repository
          Platform::Loaders::ActiveRecord.load(::User, owner_or_nwo, column: :login, case_sensitive: false).then do |user|
            if user
              Platform::Loaders::RepositoryByOwnerId.load(repository.network_id, user.id)
            else
              Platform::Loaders::RedirectedRepositoryByOldOwner.load(repository.network_id, owner_or_nwo)
            end
          end
        else
          Promise.resolve(nil)
        end
      end
    end

    def async_can_access_repo?(repo)
      if repo.nil?
        Promise.resolve(false)
      elsif repo == entity
        Promise.resolve(true)
      else
        repo.async_readable_by?(current_user)
      end
    end

    def current_user
      context[:current_user]
    end
  end
end
