# frozen_string_literal: true

module GitHub::Goomba::Async
  class UrlUnfurlFilter < NodeFilter
    SELECTOR = Goomba::Selector.new(match: "gh|content-reference")

    def initialize(*args)
      super
      @references = {}
      result[:new_urls] = []
    end

    def selector
      SELECTOR
    end

    def self.enabled?(context)
      context.has_key?(:subject) &&
          context.has_key?(:entity) &&
          context[:entity].is_a?(Repository)
    end

    def async_scan
      hrefs = @nodes.map { |node| node["href"] }.uniq
      @references = ContentReference.all_by_content_and_references(context[:subject], hrefs).index_by(&:reference)
      result[:new_urls] = hrefs - @references.keys
    end

    def call(node)
      output = link_tag(node["href"])

      reference = @references[node["href"]]
      if reference && reference.has_processed_attachment?
        output += ApplicationController.render(partial: "filter_partials/url_unfurl", locals: {
          content_reference: reference,
        })
      end

      output
    end

    private

    def link_tag(href)
      ActionController::Base.helpers.link_to(href, href, rel: "nofollow")
    end
  end
end
