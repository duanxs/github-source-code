# frozen_string_literal: true

module GitHub::Goomba::Async
  class CVEMentionFilter < NodeFilter
    SELECTOR = Goomba::Selector.new(match: "gh|cve-mention")

    def initialize(*args)
      super
      scratch[:vulnerabilities] = {}
    end

    def selector
      SELECTOR
    end

    def async_scan
      Promise.all(@nodes.map do |node|
        cve_id = node["cve_id"].upcase

        Platform::Loaders::VulnerabilityByCveId.load(cve_id).then do |vulnerability|
          next unless vulnerability&.globally_available?

          scratch[:vulnerabilities][vulnerability[:cve_id]] = vulnerability
        end
      end)
    end

    # Translates a `<gh:cve-mention>` placed into the node by
    # Goomba::CVEMentionFilter into regular HTML.
    def call(node)
      call_mention_tag(node)
    end

    def call_mention_tag(node)
      cve_id = node["cve_id"]

      if vulnerability = scratch[:vulnerabilities][cve_id.upcase]
        vulnerability_link(cve_id, vulnerability)
      else
        cve_id
      end
    end

    # Create a vulnerablity link
    #
    # cve_id - The cve id as it appears in the original text
    # vulnerablity - vulnerablity object to link to
    #
    # Returns an html-safe String link (a href) tag
    def vulnerability_link(cve_id, vulnerability)
      hovercard_attrs = HovercardHelper.hovercard_data_attributes_for_advisory(ghsa_id: vulnerability.ghsa_id)
      ActionController::Base.helpers.link_to(cve_id, vulnerability.permalink, title: vulnerability.cve_id, data: hovercard_attrs)
    end
  end
end
