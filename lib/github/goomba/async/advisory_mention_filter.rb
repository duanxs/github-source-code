# frozen_string_literal: true

module GitHub::Goomba::Async
  class AdvisoryMentionFilter < NodeFilter
    SELECTOR = Goomba::Selector.new(match: "gh|advisory-mention, a[gh|advisory-mention]")
    ATTR_SELECTOR = Goomba::Selector.new(match: "a[gh|advisory-mention]")

    def initialize(*args)
      super
      scratch[:repository_advisories] = {}
      scratch[:global_advisories] = {}
    end

    def selector
      SELECTOR
    end

    def async_scan
      Promise.all(@nodes.map do |node|
        ghsa_id, global_reference = if node =~ ATTR_SELECTOR
          data = JSON.parse(node["gh:advisory-mention"])
          [data["ghsa_id"], data["global"]]
        else
          [node["ghsa_id"], false]
        end

        global_reference ? load_global_reference(ghsa_id) : load_repository_reference(ghsa_id)
      end)
    end

    def load_global_reference(ghsa_id)
      Platform::Loaders::PublishedRepositoryAdvisoryByGhsa.load(ghsa_id).then do |advisory|
        next unless advisory&.vulnerability&.globally_available?
        # skip if this was already initialized
        next if global_advisories[ghsa_id]

        global_advisories[ghsa_id] = {
          ghsa_id: advisory.ghsa_id,
          permalink: advisory.vulnerability.permalink,
          hovercard_data: HovercardHelper.hovercard_data_attributes_for_advisory(ghsa_id: advisory.ghsa_id),
        }
      end
    end

    def load_repository_reference(ghsa_id)
      Platform::Loaders::PublishedRepositoryAdvisoryByGhsa.load(ghsa_id).then do |advisory|
        next unless advisory
        # skip if this was already initialized
        next if repository_advisories[ghsa_id]

        async_can_access_repo?(advisory.repository).then do |can_access|
          next unless can_access

          repository_advisories[ghsa_id] = {
            ghsa_id: advisory.ghsa_id,
          }
          advisory.async_permalink.then do |permalink|
            repository_advisories[ghsa_id][:permalink] = permalink
          end
        end
      end
    end

    def repository_advisories
      scratch[:repository_advisories]
    end

    def global_advisories
      scratch[:global_advisories]
    end

    # Translates a `<gh:advisory-mention>` tag or `<a gh:advisory-mention="...">` tag
    # placed into the node by Goomba::AdvisoryMentionFilter into regular HTML.
    def call(node)
      if node =~ ATTR_SELECTOR
        call_anchor_tag(node)
      else
        call_mention_tag(node)
      end
    end

    def call_mention_tag(node)
      ghsa_id = node["ghsa_id"]

      if advisory_data = repository_advisories[ghsa_id]
        advisory_link(advisory_data)
      else
        ghsa_id
      end
    end

    def call_anchor_tag(node)
      href, inner_html = node["href"], node.inner_html
      data = JSON.parse(node["gh:advisory-mention"])
      ghsa_id = data["ghsa_id"]
      global_reference = data["global"]

      advisories = global_reference ? global_advisories : repository_advisories

      if advisory_data = advisories[ghsa_id]
        advisory_link(advisory_data)
      else
        node.remove_attribute("gh:advisory-mention")
        nil
      end
    end

    # Create an advisory link
    #
    # advisory - advisory object to link to
    #
    # Returns an html-safe String link (a href) tag
    def advisory_link(advisory_data)
      if advisory_data[:permalink].present?
        ActionController::Base.helpers.link_to(advisory_data[:ghsa_id], advisory_data[:permalink], title: advisory_data[:ghsa_id], data: advisory_data[:hovercard_data])
      else
        ActionController::Base.helpers.content_tag(:span, advisory_data[:ghsa_id], class: "text-gray", title: advisory_data[:ghsa_id])
      end
    end
  end
end
