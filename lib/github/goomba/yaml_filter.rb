# rubocop:disable Style/FrozenStringLiteralComment

module GitHub::Goomba
  # Filter that represents YAML front matter as HTML
  class YamlFilter < InputFilter
    FRONTMATTER_REGEX = /\A---\s*\n(.*?\n?)^---\s*$\n?/m

    attr_reader :blob

    def call(input)
      @blob = context[:blob]
      return unless can_render_blob?

      render_yaml_frontmatter(blob.data.to_s)
    end

    def render_yaml_frontmatter(content)
      match = content.match(FRONTMATTER_REGEX)
      return unless match

      begin
        # definition of a YAML header
        data = YAML.safe_load(match[1], [Date, Symbol, Time])
      rescue Psych::BadAlias, Psych::DisallowedClass, Psych::SyntaxError => boom
        # Send user-created content with bad YAML syntax to the github-user bucket
        Failbot.report(boom, app: "github-user", content: match[0])

        error_tag = ActionController::Base.helpers.content_tag(:pre, lang: :yaml) do
          ActionController::Base.helpers.content_tag(:code) do
            GitHub::HTMLSafeString::NEW_LINE + match[0]
          end
        end

        frontmatter = error_tag + GitHub::HTMLSafeString::NEW_LINE
      else
        # avoids content that's not YAML
        return unless data.is_a?(Hash)
        # process_yaml already includes a trailing space
        frontmatter = process_yaml(data, outermost_table: true)
      end

      result[:frontmatter_skip] = match[0].size
      result[:frontmatter] = frontmatter + GitHub::HTMLSafeString::NEW_LINE

      nil
    end

    def process_yaml(data, outermost_table: false)
      th_row = []
      tb_row = []
      tr_row = nil

      # checks for whether the YAML is an array, including an array of Hashes
      is_array = data.is_a?(Array)
      is_hash_array = data.any? { |d| d.is_a?(Hash) }

      if is_hash_array && !is_array
        data[0].each_key do |header|
          th_row << header
        end
      # we can skip simple arrays, because they'll be represented as <table>s
      elsif !is_array
        data.each_key do |header|
          th_row << header
        end
      end

      elements = is_array ? data : data.values

      elements.each do |value|
        if value.is_a?(Array) || value.is_a?(Hash)
          tb_row << process_yaml(value)
        else
          tb_row << value
        end
      end

      ApplicationController.render(partial: "filter_partials/yaml_table", locals: {
        th_row: th_row,
        tb_row: tb_row,
        outermost_table: outermost_table,
      })
    end

    # Internal: Determine if we should try to render the blob via the
    # GitHub::Markup gem. We won't render the blob if it is binary or if it is
    # too big.
    #
    # Returns true if we can safely render the blob.
    def can_render_blob?
      !context[:plaintext] && GitHub::HTML::MarkupFilter.can_render_blob?(blob)
    end
  end
end
