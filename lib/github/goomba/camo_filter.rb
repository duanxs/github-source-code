# rubocop:disable Style/FrozenStringLiteralComment

module GitHub::Goomba
  class CamoFilter < NodeFilter
    SELECTOR = Goomba::Selector.new("img")

    # HTML Filter for replacing http image URLs with camo versions. See:
    #
    # https://github.com/atmos/camo
    #
    # All images provided in user content should be run through this
    # filter so that http image sources do not cause mixed-content warnings
    # in browser clients.
    #
    # Context options:
    #   :asset_proxy (required) - Base URL for constructed asset proxy URLs.
    #   :asset_proxy_secret_key (required) - The shared secret used to encode URLs.
    #   :asset_proxy_whitelist - Array of host Strings or Regexps to skip
    #                            src rewriting.
    def initialize(*args)
      super
      @filter = GitHub::HTML::CamoFilter.new("", context, result)
    end

    def selector
      SELECTOR
    end

    def call(element)
      return element unless @filter.asset_proxy_enabled?
      @filter.camo_image_filter(element)
      element
    end
  end
end
