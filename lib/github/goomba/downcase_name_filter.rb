# frozen_string_literal: true

module GitHub::Goomba
  # Downcases all name/id attributes.
  class DowncaseNameFilter < NodeFilter
    SELECTOR = Goomba::Selector.new("[name], [id]")

    def selector
      SELECTOR
    end

    def call(node)
      if node["name"]
        node["name"] = node["name"].mb_chars.downcase.to_s
      end

      if node["id"]
        node["id"] = node["id"].mb_chars.downcase.to_s
      end

      node
    end
  end
end
