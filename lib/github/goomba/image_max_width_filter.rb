# rubocop:disable Style/FrozenStringLiteralComment

module GitHub::Goomba
  # This filter rewrites image tags with a max-width inline style.
  #
  # The max-width inline styles are especially useful in HTML email which
  # don't use a global stylesheets.
  class ImageMaxWidthFilter < NodeFilter
    SELECTOR = Goomba::Selector.new("img")
    INSIDE_LINK = Goomba::Selector.new("a img")

    def selector
      SELECTOR
    end

    def call(element)
      # Cap height/width attributes.

      GitHub::HTML::ImageMaxWidthFilter::HEIGHT_WIDTH_ATTRS.each do |attr|
        if (value = element[attr].try(:to_i)) && value > GitHub::HTML::ImageMaxWidthFilter::MAX_HEIGHT_WIDTH
          element[attr] = GitHub::HTML::ImageMaxWidthFilter::MAX_HEIGHT_WIDTH.to_s
        end
      end

      # Skip if there's already a style attribute. Not sure how this
      # would happen but we can reconsider it in the future.
      return if element["style"]

      # If this image was converted to a /raw/ URL by RawImageFilter, we should
      # link to the non-raw version so users can browse the image's version
      # history etc.
      src = (RawImageFilter.non_raw_image_urls(scratch)[element] || element["src"]).to_s

      # Bail out if src doesn't look like a valid http url. trying to avoid weird
      # js injection via javascript: urls.
      begin
        return unless [nil, "http", "https"].include?(Addressable::URI.parse(src.strip).scheme&.downcase)
      rescue Addressable::URI::InvalidURIError
        return
      end

      element["style"] = "max-width:100%;"

      return element if element.matches(INSIDE_LINK)

      supposedly_safe_image = element.to_html.html_safe # rubocop:disable Rails/OutputSafety
      html = ActionController::Base.helpers.link_to(supposedly_safe_image, src, target: "_blank", rel: "noopener noreferrer")
      html = ActionController::Base.helpers.content_tag(:p, html) if element.parent.tag == :html
      html
    end
  end
end
