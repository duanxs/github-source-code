# rubocop:disable Style/FrozenStringLiteralComment

module GitHub::Goomba
  class MathFilter < TextFilter
    IGNORE_PARENTS = GitHub::HTML::MathFilter::IGNORE_PARENTS.map { |tag| "#{tag} :text" }.join(", ")
    SELECTOR = Goomba::Selector.new(match: ":text", reject: IGNORE_PARENTS)

    def initialize(*args)
      super
      @filter = GitHub::HTML::MathFilter.new("", context, result)
    end

    def self.enabled?(context)
      GitHub::HTML::MathFilter.enabled?(context)
    end

    def selector
      SELECTOR
    end

    def call(node)
      return unless node.html.include?("$")
      @filter.math_image_filter!(node.html, force_inline: !node.first_child? || !node.last_child?)
    end
  end
end
