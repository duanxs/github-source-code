# frozen_string_literal: true

module GitHub::Goomba
  # Base class for input and text filters. See InputFilter and TextFilter for
  # further details.
  #
  # The context Hash passes options to filters and should not be changed in
  # place.  A Result Hash allows filters to make extracted information
  # available to the caller and is mutable.  A Scratch Hash allows passing
  # information between filters but is not exposed to callers.
  #
  # Each filter may define additional options and output values. See the class
  # docs for more info.
  class Filter
    def initialize(context = nil, result = nil, scratch = nil)
      @context = context || {}
      @result = result || {}
      @scratch = scratch || {}
    end

    # Public: Returns whether the filter should be used. Typically queries the
    # context hash and application configuration to decide.
    def self.enabled?(context)
      true
    end

    # Public: Returns a simple Hash used to pass extra information into filters
    # and also to allow filters to make extracted information available to the
    # caller.
    attr_reader :context

    # Public: Returns a Hash used to allow filters to pass back information
    # to callers of the various Pipelines.  This can be used for
    # #mentioned_users, for example.
    attr_reader :result

    # Internal: Returns a Hash used to pass information between filters.
    attr_reader :scratch

    # Public: Returns the entity within the context, or nil.
    def entity
      context[:entity]
    end

    # Public: Returns the entity within the context if it's a Repository object.
    def repository
      entity if entity.is_a? Repository
    end
  end
end
