# rubocop:disable Style/FrozenStringLiteralComment

module GitHub::Goomba
  # Render colors inline for for hex, rgb/rgba,and hsl/hsla values inside of code tags.
  class ColorFilter < NodeFilter
    include ActionView::Helpers::OutputSafetyHelper
    SELECTOR = Goomba::Selector.new("code")
    COLOR_REGEXP = /\A(#\h{6}|(rgb|hsl)[a]?\([\d..%,\s]+{3}[,\d\.%]?\))\z/
    COLOR_CLASS = "ml-1 d-inline-block v-align-middle Box border-black-fade"

    def initialize(*args)
      super
    end

    def selector
      SELECTOR
    end

    def call(element)
      return nil unless element.text_content.match(COLOR_REGEXP)

      inner_span = ActionController::Base.helpers.tag(:span, style: "background-color: #{element.text_content}; height: 0.8em; width: 0.8em;", class: COLOR_CLASS)
      ActionController::Base.helpers.content_tag(:code, safe_join([element.text_content, inner_span]))
    end
  end
end
