# rubocop:disable Style/FrozenStringLiteralComment

module GitHub::Goomba
  # Goomba text filter that wraps native emoji characters in <g-emoji>
  # elements. This allows JavaScript to use an image-based polyfill on browsers
  # that don't support native emoji.
  #
  # Context options:
  #   :asset_root (required) - base url to link to emoji sprite
  class UnicodeEmojiFilter < TextFilter
    IGNORE_PARENTS = GitHub::HTML::EmojiFilter::IGNORE_PARENTS.map { |tag| "#{tag} :text" }.join(", ")
    SELECTOR = Goomba::Selector.new(match: ":text", reject: IGNORE_PARENTS)

    def initialize(*args)
      super
      @filter = GitHub::HTML::EmojiFilter.new("", context, result)
    end

    def selector
      SELECTOR
    end

    def call(text)
      @filter.unicode_emoji_filter!(text.html)
    end
  end
end
