# frozen_string_literal: true

module GitHub::Goomba
  class AdvisoryMentionFilter < NodeFilter
    SELECTOR = Goomba::Selector.new(match: ":text, a[href^='#{GitHub.url}']",
                                    reject: "pre :text, code :text, a :text, blockquote :text")

    NWO = ::GitHub::HTML::IssueMentionFilter::NWO
    GHSA_ID_PATTERN = ::AdvisoryDB.valid_ghsa_id_input_pattern
    FULL_ADVISORY_URL_PATTERN = %r{#{Regexp.escape(GitHub.url)}/#{NWO}/security/advisories/(#{GHSA_ID_PATTERN})}
    GLOBAL_ADVISORY_URL_PATTERN = %r{#{Regexp.escape(GitHub.url)}/advisories/(#{GHSA_ID_PATTERN})}

    def call(node)
      if node.is_a?(Goomba::ElementNode) # HTML tag
        call_anchor(node) if node.tag == :a
      else # text
        call_text(node)
      end
    end

    # Handles cases where an Advisory URL has been pre-processed into an <a> tag
    # by inserting a 'gh:advisory-mention' attribute
    #
    # replaces:
    # <a href="FULL_ADVISORY_URL_PATTERN">FULL_ADVISORY_URL_PATTERN</a>
    #
    # with:
    # <a href="FULL_ADVISORY_URL_PATTERN" gh:advisory-mention="{'ghsa_id':'GHSA_ID_PATTERN'}">FULL_ADVISORY_URL_PATTERN</a>
    #
    def call_anchor(node)
      return nil unless node_is_a_bare_link?(node)

      if match = node["href"].match(FULL_ADVISORY_URL_PATTERN)
        _nwo, ghsa_id = match[1], match[2]

        node["gh:advisory-mention"] = {
          ghsa_id: ghsa_id,
        }.to_json
      elsif match = node["href"].match(GLOBAL_ADVISORY_URL_PATTERN)
        node["gh:advisory-mention"] = {
          ghsa_id: match[1],
          global: true,
        }.to_json
      end

      nil # No replacement to return as node has been modified in place
    end

    # Turns plain text Advisory IDs into <gh:advisory-mention> tags
    #
    # replaces:
    # GHSA_ID_PATTERN
    #
    # with:
    # <gh:advisory-mention ghsa_id="GHSA_ID_PATTERN"></gh:advisory-mention>
    #
    def call_text(text)
      text.html.gsub(GHSA_ID_PATTERN) do |match|
        advisory_mention_tag(match)
      end
    end

    def selector
      SELECTOR
    end

    private

    def node_is_a_bare_link?(node)
      node.inner_html == node["href"]
    end

    # returns an html_safe gh:advisory-mention tag
    def advisory_mention_tag(ghsa_id)
      attrs = {
        ghsa_id: ghsa_id,
      }

      ActionController::Base.helpers.content_tag("gh:advisory-mention", nil, attrs)
    end
  end
end
