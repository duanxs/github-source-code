# rubocop:disable Style/FrozenStringLiteralComment

module GitHub::Goomba
  class MentionFilter < TextFilter
    IGNORE_PARENTS = GitHub::HTML::MentionFilter::IGNORE_PARENTS.map { |p| "#{p} :text" }.join(",")
    SELECTOR = Goomba::Selector.new(match: ":text", reject: IGNORE_PARENTS)

    def initialize(*args)
      super
      @filter = GitHub::HTML::MentionFilter.new("", context, result)
    end

    def selector
      SELECTOR
    end

    def call(text)
      return nil unless text.html.include?("@")

      result = @filter.class.mentioned_logins_in(text.html) do |match, login, is_mentioned|
        link = !is_mentioned && ActionController::Base.helpers.content_tag("gh:user-mention", body = nil, login: login)
        link ? match.sub("@#{login}", link) : match
      end

      result if result != text.html
    end
  end
end
