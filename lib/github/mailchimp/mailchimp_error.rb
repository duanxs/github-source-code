# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  class Mailchimp
    class MailchimpError < StandardError
      def areas_of_responsibility
        [:email_marketing]
      end

      def failbot_context
        { app: "github-external-request" }
      end
    end
  end
end
