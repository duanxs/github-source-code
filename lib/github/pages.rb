# rubocop:disable Style/FrozenStringLiteralComment

require "rack"

module GitHub
  module Pages
    autoload :Builder,          "github/pages/builder"
    autoload :Creator,          "github/pages/creator"
    autoload :GarbageCollector, "github/pages/garbage_collector"
    autoload :Theme,            "github/pages/theme"
    autoload :JekyllTheme,      "github/pages/jekyll_theme"
    autoload :JekyllConfig,     "github/pages/jekyll_config"
    autoload :JekyllSite,       "github/pages/jekyll_site"
    autoload :ReplicationStrategy, "github/pages/replication_strategy"

    # Repository page (gh-pages branch) automatic creator.
    #
    # See also: Repository#pages_create
    #
    # Returns nothing.
    def self.create(repo, user, params)
      Creator.new(repo, user, params).run
    end

    # Repository page content generator.
    #
    # Returns HTML content generated from params.
    def self.content_for(repo, user, params)
      Creator.new(repo, user, params).content
    end
  end
end
