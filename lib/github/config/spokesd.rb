# frozen_string_literal: true

module GitHub
  module Config
    module Spokesd
      attr_accessor :spokesd_url, :spokesd_client_role

      def spokesd_enabled?
        # Each role which will contact spokesd must be individually configured
        # / authorized. If we're trying to use spokesd but don't have a
        # certificate we should tell someone about it.
        if spokesd_url.present? && !File.file?(spokesd_cert_file("#{spokesd_cert_name}.key"))
            GitHub::Logger.log at: "warning",
                               method: "spokesd_enabled?",
                               message: "no spokesd cert for #{spokesd_cert_name}"
            return false
        end

        spokesd_url.present?
      end

      def spokesd_certs
        return unless spokesd_url.starts_with? "https"

        files = ["chain.pem", "#{spokesd_cert_name}.key", "#{spokesd_cert_name}.crt"]
        files.map { |f| spokesd_cert_file(f) }
      end

      private

      def spokesd_cert_name
        if spokesd_client_role.present?
          "github-#{spokesd_client_role}"
        else
          "github-#{GitHub.role}"
        end
      end

      def spokesd_cert_file(name)
        File.join(ENV["RAILS_ROOT"], "config", "service_certificates", "spokesd", name)
      end
    end
  end

  extend Config::Spokesd
end
