# frozen_string_literal: true

# setup Failbot for exceptions reporting.
require "github"
require "failbot/exit_hook"
require "rollup"

# require 'securerandom' for UUID generation
require "securerandom"

# require digester for SQL rollups
require "github/sql/digester"

# require 'github/logger' for reporting redacted exceptions to Splunk
require "github/logger"

require "sensitive_data"
require "github/failbot_backtrace"

# failbot settings from environment override GitHub::Config
settings = ENV.to_hash.keep_if { |k, v| k.start_with?("FAILBOT_") && !k.start_with?("FAILBOT_CONTEXT_") }
settings = GitHub.failbot.merge(settings)

default_context = {}

ENV.each do |key, value|
  if /\AFAILBOT_CONTEXT_(.+)\z/ =~ key
    default_context[$1.downcase] = value
  end
end

default_context.merge!(
  "release" => GitHub.current_sha,
  "current_ref" => GitHub.current_ref,
  "app" => "github",
  "ruby" => RUBY_DESCRIPTION,
  "server" => ENV.fetch("KUBE_NODE_HOSTNAME", Failbot.hostname),
  "deployed_to" => GitHub.failbot_deployed_to,
  "kube_namespace" => GitHub.kubernetes_namespace,
)

Failbot.setup(settings, default_context)

# Enforce allowable key list before sending to Sentry.
failbot_key_filter = GitHub::FailbotKeyFilter.new(raise_on_filter: GitHub.raise_on_failbot_filter?)
failbot_key_tagger = GitHub::FailbotKeyTagger.new(enabled: GitHub.enable_failbot_tags?)
failbot_logger = GitHub::FailbotLogger.new(enabled: GitHub.log_failbot_exceptions?)
failbot_scrubber = GitHub::FailbotScrubber.new(enabled: GitHub.enable_failbot_scrubbing?)

# Make adjustments to Failbot context before it gets reported
#
# @params exception the exception object being reported
# @params context the combined Failbot context Hash so far, including defaults, everything Failbot.push'd, and message,
#         class and backtrace extracted from the exception. Failbot#squash_context ensures the keys are strings, not symbols
#
# Note: The full environment may not be available here if run from hooks, etc.  Vanilla Ruby only.

Failbot.before_report do |exception, context|
  result = {}

  # Populate the datacenter lazily, in order to avoid a circular dependency.
  # See https://github.com/github/github/commit/9bea1bfef0 for details.
  result[:datacenter] = GitHub.datacenter
  result[:site] = GitHub.server_site
  result[:region] = GitHub.server_region

  if app = app_for(exception, context)
    result[:app] = app
  end

  if exception.respond_to?(:areas_of_responsibility)
    areas_of_responsibility = exception.areas_of_responsibility.dup

    (context["areas_of_responsibility"] || []).each do |area|
      areas_of_responsibility << area unless areas_of_responsibility.include?(area)
    end

    result[:areas_of_responsibility] = areas_of_responsibility
  end

  # generate a failbot_id so we can search for it in splunk
  result["failbot_id"] = SecureRandom.uuid

  # combine result and context, ensuring keys are strings for logging
  log_data = Failbot.squash_contexts(context, result)
  failbot_logger.log_exception(log_data, exception)

  # Include a URL to search splunk. Do it after logging though, so it doesn't get logged to Splunk
  if failbot_logger.enabled?
    now = Time.now
    result["splunk_url"] = "https://splunk.githubapp.com/app/gh_reference_app/search?earliest=#{(now - 1.hour).to_i}&latest=#{(now + 1.hour).to_i}&q=search%20index%3Dprod-dotcom-exceptions%20failbot_id%3D#{result["failbot_id"]}"
  end

  failbot_key_filter.call(context)
  failbot_key_tagger.convert_keys!(context)
  failbot_scrubber.call(context)

  if exception_needs_redacting?(exception)
    ["message", "cause", "remote_backtrace"].each do |k|
      result[k] = "[redacted]" if context.include? k
    end

    # More recent failbots store exception message in
    # context["exception_detail"][\d]["value"], which needs different handling
    # than the above.
    if context["exception_detail"]
      context["exception_detail"].each_with_index do |payload, index|
        if payload && payload["value"]
          context["exception_detail"][index]["value"] = "[redacted]"
        end
      end
    end
  end

  result
end

# ExceptionRedacting defines a base implementation of #needs_redacting? and
# #with_redacting! for all Exception subclasses.
#
# If you have an exception instance that needs redacting at runtime (for
# example, when rescuing StandardError from a source you can't control or don't
# know), call #with_redacting!.
#
# For exception classes you control that should always be redacted, defining
# #needs_redacting? on the class is better.
module ExceptionRedacting
  # Returns true if this exception should be redacted in Sentry. Default: false
  def needs_redacting?
    @needs_redacting ||= false
  end

  # Set needs_redacting? to true. Returns the exception for chaining.
  #
  # Example:
  #
  #   Failbot.report(exception.with_redacting!)
  def with_redacting!
    @needs_redacting = true
    self
  end
end
Exception.include(ExceptionRedacting)

FAILBOT_ERRORS_NEEDING_REDACTING = [
  # leaks information about duplicate key entry: trilogy_query_recv: 1062 Duplicate entry 'user@example.com' for key 'index_user_licenses_on_business_id_and_email'
  "Trilogy::MysqlError",
  # wraps message from Trilogy::MysqlError
  "ActiveRecord::RecordNotUnique",
  # leaks information about user and repo names
  "ActionController::UrlGenerationError",
  # leak full SQL queries with values when used through Vitess
  "ActiveRecord::StatementTimeout",
  "ActiveRecord::QueryCanceled",
  # Elastomer::Error subclasses leak PII
  # - Elastomer::Client::Error includes information about the query performed
  "Elastomer::Error",
  # leak information about the URI being parsed
  "URI::InvalidURIError",
  "Addressable::URI::InvalidURIError",
  # leaks GitHub::Cache values that have invalid encoding
  "EncodingFixer::InvalidEncodingError",
  # leak information about the parsed string
  "JSON::ParserError",
  "Yajl::ParseError",
  # 3rd party error without insurance of not leaking PII
  "MessagePack::UnpackError",
  "Zlib::Error",
]

# Returns true if the exception or any of its nested causes (up to the limit
# that failbot will serialize) needs to be redacted in Sentry.
def exception_needs_redacting?(root_exception)
  return false if GitHub.bypass_failbot_filter_logic?

  depth = 0
  exception = root_exception
  loop do
    return true if exception.needs_redacting? || FAILBOT_ERRORS_NEEDING_REDACTING.any? { |c| exception_is_a?(exception, c) }
    depth += 1
    break false unless (exception=exception.cause)
    break false if depth > Failbot::MAXIMUM_CAUSE_DEPTH
  end
end

def exception_is_a?(exception, class_name)
  if Object.const_defined?(class_name)
    exception.is_a? Object.const_get(class_name)
  else
    false
  end
end

def app_for(exception, context)
  return "github-freno" if exception_is_a?(exception, "Freno::Error")

  timeouts = [
    "Timeout::Error",
    "GitRPC::Timeout",
    "GitRPC::SpawnFailure",
  ]
  return "github-timeout" if timeouts.any? { |c| exception_is_a?(exception, c) }

  resque_errors = [
    "Resque::DirtyExit",
  ]
  return "github-resque" if resque_errors.any? { |c| exception_is_a?(exception, c) }

  external_errors = [
    "Braintree::BraintreeError",
    "Twilio::REST::ServerError",
    "Twilio::REST::RequestError",
  ]
  return "github-external-request" if external_errors.any? { |c| exception_is_a?(exception, c) }

  if query_interruption?(exception)
    if (pages_query_in_message?(exception))
      "pages"
    elsif (spokes_job?(context) || spokes_query_in_message?(exception))
      if !spokes_app_already_set?(context)
        "github-dgit"
      else
        nil
      end
    else
      "github-killed-query"
    end
  end
end

def pages_query_in_message?(exception)
  !(exception.message =~ /pages_replicas|pages_fileservers/).nil?
end

def spokes_app_already_set?(context)
  %w[github-dgit github-dgit-debug].include? context["app"]
end

def spokes_query_in_message?(exception)
  !(exception.message =~ /(network|repository|gist)_replicas|(repository|gist)_checksums|fileservers/).nil?
end

def spokes_job?(context)
  context["job"] && (context["job"].include?("DGit") || context["job"].include?("Dgit")  || context["job"].include?("Spokes"))
end

def query_interruption?(exception)
  return false unless defined?(ActiveRecord::QueryInterruption)
  exception.is_a?(ActiveRecord::QueryInterruption) && exception.mysql_timeout?
end

Failbot.rollup do |exception, context|
  if query_interruption?(exception)
    sql = exception.message.sub(/\A.+?Query execution was interrupted: /, "")
    Digest::MD5.hexdigest(GitHub::SQL::Digester.digest_sql(sql))
  else
    Rollup.generate(exception)
  end
end

# Opt in to the format introduced in https://github.com/github/failbot/pull/128
# which is required to prevent stack trace truncation at 8KiB.
Failbot.exception_format = GitHub.failbot_exception_format
Failbot.source_root = File.realpath(Rails.root)
Failbot.backtrace_parser = GitHub::FailbotBacktrace::Parser


# hot fix for exception_message_from_hash failing when the exception has too many causes
Failbot::ExceptionFormat::Structured.class_eval do
  # given a hash generated by this class, return the exception message.
  def self.exception_message_from_hash(hash)
    return unless hash && hash["exception_detail"]
    detail = hash["exception_detail"].detect { |detail| detail && detail["type"] != "Notice" }
    return unless detail
    detail["value"]
  end

  # given a hash generated by this class, return the exception class name.
  def self.exception_classname_from_hash(hash)
    return unless hash && hash["exception_detail"]
    detail = hash["exception_detail"].detect { |detail| detail && detail["type"] != "Notice" }
    return unless detail
    detail["type"]
  end
end
