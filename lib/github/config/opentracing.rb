# frozen_string_literal: true

require "octotracer"
require "lightstep"
require "active_support"

module GitHub
  module Config
    module OpenTracing
      def tracer
        @tracer ||= OctoTracer::Tracer.new(tracers: [GitHub.build_lightstep_tracer])
      end

      def tracing_enabled?
        role = GitHub::FlipperRole.local_role.to_s
        !GitHub.enterprise? && ["api", "fe", "internal-api", "lowworker"].include?(role)
      end

      def verbose_tracing?
        ENV["HUBSTEP_TRACKER_VERBOSE"] == "true"
      end

      def build_lightstep_tracer
        LightStep::Tracer.new(
          component_name: "github-#{GitHub.role}",
          transport: GitHub.tracer_transport,
          tags: {
            "env" => Rails.env,
            "application" => "github",
            "application_role" => GitHub.role,
            "application_component" => GitHub.component.to_s,
            "current_ref" => GitHub.current_ref,
            "current_sha" => GitHub.current_sha,
            "datacenter" => GitHub.datacenter,
            "site" => GitHub.server_site,
            "region" => GitHub.server_region,
            "hostname" => GitHub.local_host_name_short,
          })
      end

      def tracer_transport
        host = GitHub.environment["LIGHTSTEP_COLLECTOR_HOST"]
        port = GitHub.environment["LIGHTSTEP_COLLECTOR_PORT"]
        encryption = GitHub.environment["LIGHTSTEP_COLLECTOR_ENCRYPTION"]
        access_token = GitHub.environment["LIGHTSTEP_ACCESS_TOKEN"]

        if host && port && encryption && access_token
          LightStep::Transport::HTTPJSON.new(host: host,
                                             port: port.to_i,
                                             encryption: encryption,
                                             access_token: access_token)
        else
          LightStep::Transport::Nil.new
        end
      end

      # While this is the default behavior if ActiveSupport is available,
      # require ordering varies on environment making this an iffy assumption.
      # We might as well set it directly.
      LightStep.instrumenter = ActiveSupport::Notifications unless LightStep.instrumenter
    end
  end

  extend Config::OpenTracing
end
