# frozen_string_literal: true

module GitHub
  module Config
    module DriftwoodConfig
      DEFAULT_BULK_EXPORT_PAGE_SIZE = 500

      # Is the driftwood service enabled or not.
      def driftwood_enabled?
        return @driftwood_enabled if defined?(@driftwood_enabled)
        if GitHub.driftwood_host
          @driftwood_enabled = true
        else
          @driftwood_enabled = false
        end
      end
      attr_accessor :driftwood_enabled

      def driftwood_host
        GitHub.environment.fetch("DRIFTWOOD_HOST", @driftwood_host)
      end
      attr_writer :driftwood_host

      def driftwood_client
        @driftwood_client ||= ::Driftwood::Client.new(
          driftwood_host,
        )
      end
      attr_writer :driftwood_client

      # How many entries per page are requested
      def driftwood_bulk_export_page_size
        @driftwood_bulk_export_page_size || DEFAULT_BULK_EXPORT_PAGE_SIZE
      end
      attr_writer :driftwood_bulk_export_page_size
    end
  end

  extend Config::DriftwoodConfig
end
