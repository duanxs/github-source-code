# frozen_string_literal: true

module GitHub
  module Config
    module OpenApi

      DEFAULT_RELEASE = "api.github.com"

      # Public: Which OpenAPI release name to use.
      #
      # By default this is "api.github.com"
      #
      # Returns a boolean.
      def openapi_release
        return @openapi_release if defined?(@openapi_release)
        @openapi_release = DEFAULT_RELEASE
      end
      attr_writer :openapi_release

    end
  end

  extend Config::OpenApi
end
