# frozen_string_literal: true

##
# Pages configuration.

module GitHub
  module Config
    module Pages
      PAGES_ENABLED_KEY = "enterprise:pages_enabled"
      PUBLIC_PAGES_KEY = "enterprise:public_pages"

      # Flag indicating whether Pages are enabled in this environment. This is
      # set by the ENTERPRISE_PAGES_ENABLED environment variable in the
      # enterprise environment, which is populated via the pages.enabled
      # configuration value.
      #
      # Returns Boolean indicating whether Pages are enabled.
      def pages_enabled?
        tmp_value = tmp_pages_enabled
        return tmp_value unless tmp_value.nil?
        return @pages_enabled if defined?(@pages_enabled)
        @pages_enabled = true
      end
      attr_writer :pages_enabled

      # Public: Get the temporary GHES Pages Enabled value to be used within the app
      # while a configuration run occurs and persists the configuration value.
      #
      # Returns Boolean or nil if the temporary value is not found.
      def tmp_pages_enabled
        return if !private_instance? || !GitHub.respond_to?(:kv)
        val = GitHub.kv.get(PAGES_ENABLED_KEY).value { nil }
        case val
        when "true"
          true
        when "false"
          false
        else
          nil
        end
      end

      # Flag indicating whether Pages are published publicly in a GHES
      # environment. This is set by the ENTERPRISE_PUBLIC_PAGES environment
      # variable in the enterprise environment, which is populated via the
      # core.public-pages configuration value.
      #
      # Returns Boolean indicating whether Pages are public.
      def public_pages?
        tmp_value = tmp_public_pages
        return tmp_value unless tmp_value.nil?
        return @public_pages if defined?(@public_pages)
        @public_pages = false
      end
      attr_writer :public_pages

      # Public: Get the temporary GHES Public Pages value to be used within the app
      # while a configuration run occurs and persists the configuration value.
      #
      # Returns Boolean or nil if the temporary value is not found.
      def tmp_public_pages
        return if !private_instance? || !GitHub.respond_to?(:kv)
        val = GitHub.kv.get(PUBLIC_PAGES_KEY).value { nil }
        case val
        when "true"
          true
        when "false"
          false
        else
          nil
        end
      end

      # Root directory where fully-built Pages sites are served from. This
      # defaults to the tmp/pages directory under RAILS_ROOT, but is typically
      # overridden in production environments.
      #
      # Returns the full path to the Pages directory as a String.
      def pages_dir
        @pages_dir ||=
          if Rails.test?
            # NOTE we are randomizing the path to make sure concurrent builds have different paths. See #111803
            File.expand_path("#{Rails.root}/tmp/#{SecureRandom.uuid}/pages#{test_environment_number}").freeze
          else
            File.expand_path("#{Rails.root}/tmp/pages").freeze
          end
      end
      attr_writer :pages_dir

      # Directory which contains the copy of pages-jekyll code.
      #
      # Returns the full path to the Pages application code directory as a String.
      def pages_jekyll_dir
        return ENV["GH_PAGES_JEKYLL_DIR"] if ENV["GH_PAGES_JEKYLL_DIR"]
        @pages_jekyll_dir ||=
          if File.exist?("/data/pages-jekyll/current")
            File.realpath("/data/pages-jekyll/current").freeze
          else
            "/data/pages-jekyll"
          end
      end

      # Temporary directory where Pages sites are built before being copied to the
      # pages_dir. This defaults to the tmp/pagebuild/<environment> directory, but
      # is typically overridden in production environments.
      #
      # Returns the full path to the Pages build directory as a String.
      def pages_build_dir
        @pages_build_dir ||=
          if Rails.test?
            File.expand_path("#{Rails.root}/tmp/pagebuild/#{Rails.env}#{test_environment_number}").freeze
          else
            File.expand_path("#{Rails.root}/tmp/pagebuild/#{Rails.env}").freeze
          end
      end
      attr_writer :pages_build_dir

      # The maximum size of a built pages site in bytes. Set to 0 to disable
      # the size limit.
      def pages_site_size_limit
        @pages_site_size_limit ||= (10 * 1024 * 1024 * 1024) # 10 GB
      end
      attr_writer :pages_site_size_limit

      # ID of the GitHub Pages Oauth App
      #
      # Returns int ID the application ID, or nil if app doesn't exist
      def pages_app_id
        @pages_app_id ||= OauthApplication.where(
          user_id: trusted_oauth_apps_owner,
          name: Apps::Internal::Pages::OAUTH_APP_NAME,
        ).pluck(:id).first
      end
      attr_writer :pages_app_id

      # Public: The GitHub App (Integration).
      def pages_github_app
        @pages_github_app ||= Integration.where(
          owner_id: trusted_oauth_apps_owner,
          name: pages_github_app_name,
        ).first
      end

      # Public: The name of the Pages GitHub App.
      def pages_github_app_name
        Apps::Internal::Pages::INTEGRATION_NAME
      end

      # Public: The slug of the Pages GitHub App.
      def pages_github_app_slug
        @pages_github_app_slug ||= "github-pages"
      end

      # Public: The name used when creating a CheckRun.
      def pages_check_run_name
        @pages_check_run_name ||= "Page Build"
      end

      def pages_github_app_available?
        !GitHub.enterprise?
      end

      def pages_help_url
        "#{GitHub.help_url}/categories/github-pages-basics"
      end

      def pages_visibility_help_url
        "#{GitHub.help_url}/github/working-with-github-pages/changing-the-visibility-of-your-github-pages-site"
      end

      # GitHub Pages Auth Url
      #
      # Returns the URL string
      def pages_auth_url
        @pages_auth_url ||= "https://pages-auth.github.com"
      end

      def pages_auth_host_name
        "pages-auth.github.com"
      end

      def pages_replica_count
        @pages_replica_count ||= (Rails.production? && !enterprise?) ? 3 : 1
      end
      attr_writer :pages_replica_count

      def pages_replication_strategy
        @pages_replication_strategy ||= if Rails.production? && !enterprise?
          GitHub::Pages::ReplicationStrategy.new(
            data_centers: %w(ac4 ash1 va3),
            replica_counts: [2, 2, 1],
          )
        else
          GitHub::Pages::ReplicationStrategy.new
        end
      end
      attr_writer :pages_replication_strategy

      def pages_beta_replication_strategy
        @pages_beta_replication_strategy ||= if Rails.production? && !enterprise?
          GitHub::Pages::ReplicationStrategy.new(
            data_centers: %w(ac4 ash1 va3),
            replica_counts: [2, 2, 1],
          )
        else
          GitHub::Pages::ReplicationStrategy.new
        end
      end
      attr_writer :pages_beta_replication_strategy

      # Gets the number of readonly replicas that are included in a page build,
      # but not required for a successful consensus.
      attr_accessor :pages_non_voting_replica_count

      # Do we support custom pages CNAMEs in this environment?
      #
      # When custom CNAMEs are enabled, pages sites are served from
      # USERNAME.github.io, or a custom domain the user provides in a `CNAME` file.
      #
      # When custom CNAMEs are disabled, pages sites are served from
      # /pages/USERNAME/REPO and any `CNAME` file is ignored.
      #
      def pages_custom_cnames?
        !enterprise?
      end

      # Orgs/users that are owned by GitHub and should be allowed to use
      # `github.com` urls.
      #
      # Returns an Array of String User/Organization logins.
      def github_owned_pages
        @github_owned_pages ||= []
      end

      # The GitHub Pages hostname only.
      #
      # Returns the hostname string ("githubpages.com", "github.io", etc.) or nil
      # if no host_name has been set.
      def pages_host_name_v1
        @pages_host_name ||= GitHub.host_name
      end
      attr_writer :pages_host_name

      # The GitHub Pages hostname we are migrating to
      #
      # Returns the hostname string (i.e. "github.io") if we are in an environment
      # that supports it,  or nil if no host_name has been set.
      def pages_host_name_v2
        @pages_host_name_v2 ||= if enterprise?
          if GitHub.subdomain_isolation?
            "pages.#{GitHub.host_name}"
          else
            GitHub.host_name
          end
        else
          "github.io".freeze
        end
      end
      attr_writer :pages_host_name_v2

      def pages_preview_hostname
        @pages_preview_hostname ||= "drafts.github.io"
      end
      attr_writer :pages_preview_hostname

      # The GitHub Pages themes hostname for previews in the theme chooser
      def pages_themes_hostname
        @pages_themes_hostname ||= "pages-themes.#{GitHub.pages_host_name_v2}"
      end
      attr_writer :pages_themes_hostname

      # Do we support HTTPS redirects for pages sites?
      #
      # Returns boolean.
      def pages_https_redirect_enabled?
        !enterprise?
      end

      # Do we support themes for pages sites?
      #
      # Returns boolean.
      def pages_gem_themes_enabled?
        !GitHub.enterprise?
      end

      # Pages for repositories created after this time will require HTTPS if they
      # are served from github.io.
      #
      # This is only used it tests for now, so it's set to an hour from now.
      #
      # Returns a Time instance.
      def pages_https_required_after
        @pages_https_required_after ||= Time.parse("2016-06-15 12:00:00 PDT")
      end

      # Domains that we have SSL certs for on Fastly.
      #
      # Returns an Array of Strings.
      def pages_https_domains
        if enterprise?
          []
        else
          %w(
            blog.atom.io
            electron.atom.io
            flight-manual.atom.io

            brew.sh
            docs.brew.sh

            choosealicense.com
            codeconf.com
            github.co.jp
            githubengineering.com
            githubuniverse.com
            git-merge.com
            opensource.guide
            svnhub.com
          )
        end
      end

      def pages_custom_domain_https_enabled?
        pages_custom_cnames? && acme_enabled? && GitHub.earthsmoke_enabled?
      end

      def pages_failbot_backend_file_path
        @pages_failbot_backend_file_path ||= "#{Rails.root}/log/pages-exceptions.log"
      end
      attr_writer :pages_failbot_backend_file_path

      # Do we support creating Let's Encrypt certificates for CNAME pages.
      def acme_enabled?
        !enterprise?
      end

      # ACME protocol endpoint for getting Pages certificates.
      def acme_directory
        "https://acme-v02.api.letsencrypt.org/directory"
      end

      # JSON Web Key for ACME client.
      def acme_jwk
        @acme_jwk ||= GitHub::Earthsmoke::AcmeJwk.new("github:acme:lets-encrypt:account-key")
      end

      # Key for Let's Encrypt certificates.
      def acme_cert_key
        GitHub.earthsmoke.low_level_key("github:acme:lets-encrypt:cert-key")
      end

      # ACME client for getting Pages certificates.
      def acme
        @acme ||= Acme::Client.new(
          jwk: acme_jwk,
          directory: acme_directory
        )
      end

      # A collection of domains for which certificates may not be issued.
      #
      # Returns an Array of domain names. Check the domain with `domain_name.end_with?(blacklist_entry)`
      def acme_blacklist
        @acme_blacklist ||= [
          ".siteleaf.net".freeze,
          ".stack.network".freeze,
          ".streamdata.io".freeze,
        ].freeze
      end

      ###
      ## Dpages maintenance.

      # The maximum number of DpagesRepairSite jobs the DpagesMaintenanceScheduler should enqueue per run.
      def dpages_maintenance_scheduler_batch_size
        @dpages_maintenance_scheduler_batch_size ||= 1000
      end
      attr_writer :dpages_maintenance_scheduler_batch_size

      # The maximum amount of time the DpagesMaintenanceScheduler job has to execute before timing out.
      def dpages_maintenance_scheduler_maximum_execution_time
        @dpages_maintenance_scheduler_maximum_execution_time ||= 30.minutes
      end
      attr_writer :dpages_maintenance_scheduler_maximum_execution_time

      # The interval for scheduling new DpagesMaintenanceScheduler jobs.
      def dpages_maintenance_scheduler_schedule_interval
        @dpages_maintenance_scheduler_schedule_interval ||= 5.minutes
      end
      attr_writer :dpages_maintenance_scheduler_schedule_interval

      # The maximum number of DpagesEvacuateSite jobs the DpagesEvacuationScheduler should enqueue per run.
      def dpages_evacuations_scheduler_batch_size
        @dpages_evacuations_scheduler_batch_size ||= 500
      end
      attr_writer :dpages_evacuations_scheduler_batch_size

    end
  end

  extend Config::Pages
end
