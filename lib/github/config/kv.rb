# rubocop:disable Style/FrozenStringLiteralComment

##
# Setup global key-value object.
#
# Use `GitHub.kv` anywhere in GitHub to access a key-value store.
#
# This should be used in all new code instead of Redis.
#
# Docs for KV can be found in https://github.com/github/github-ds
require "application_record/domain/key_values"
require "github"
require "github/kv"

module GitHub
  module Config
    module KV
      attr_writer :kv

      ::GitHub::KV.configure do |config|
        config.encapsulated_errors = [SystemCallError, Trilogy::Error, ActiveRecord::NoDatabaseError]
        config.use_local_time = Rails.env.test?
      end

      def kv
        @kv ||= ::GitHub::KV.new do
          ApplicationRecord::Domain::KeyValues.connection
        end
      end
    end
  end

  extend Config::KV
end
