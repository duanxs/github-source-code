# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Config
    module Registry

      # Registry enabled in GHES
      def registry_enabled_for_enterprise?
        return @registry_enabled_for_enterprise if defined?(@registry_enabled_for_enterprise)
        @registry_enabled_for_enterprise = ENV["ENTERPRISE_REGISTRY_ENABLED_FOR_ENTERPRISE"] || false
      end
      attr_writer :registry_enabled_for_enterprise

      def package_registry_cdn_enabled?
        return @package_registry_cdn_enabled if defined?(@package_registry_cdn_enabled)
        @package_registry_cdn_enabled = GitHub.flipper[:package_registry_cdn].enabled?
      end
      attr_writer :package_registry_cdn_enabled

      def package_registry_enabled?
        return @package_registry_enabled if defined?(@package_registry_enabled)
        @package_registry_enabled = !enterprise?
      end
      attr_writer :package_registry_enabled

      # GitHub Package Registry Documentation
      #
      # Returns the URL string ("https://help.github.com/en/articles/about-github-package-registry/")
      def about_github_package_registry_url
        "#{help_url}/articles/about-github-package-registry/"
      end

      # GitHub Package Registry Documentation
      #
      # Returns the URL string ("https://help.github.com/en/github/managing-packages-with-github-packages/deleting-a-package")
      def github_package_registry_delete_policy_url
        "#{help_url}/github/managing-packages-with-github-packages/deleting-a-package"
      end

      # GitHub Docker Registry Documentation
      #
      # Returns the URL string ("https://help.github.com/en/articles/configuring-docker-for-use-with-github-package-registry/")
      def docker_github_package_registry_url
        "#{help_url}/articles/configuring-docker-for-use-with-github-package-registry/"
      end

      # GitHub Maven Registry Documentation
      #
      # Returns the URL string ("https://help.github.com/en/articles/configuring-apache-maven-for-use-with-github-package-registry/")
      def maven_github_package_registry_url
        "#{help_url}/articles/configuring-apache-maven-for-use-with-github-package-registry/"
      end

      # GitHub Gradle Registry Documentation
      #
      # Returns the URL string ("https://help.github.com/en/articles/configuring-gradle-for-use-with-github-package-registry/")
      def gradle_github_package_registry_url
        "#{help_url}/articles/configuring-gradle-for-use-with-github-package-registry/"
      end

      # GitHub npm Registry Documentation
      #
      # Returns the URL string ("https://help.github.com/en/articles/configuring-npm-for-use-with-github-package-registry/")
      def npm_github_package_registry_url
        "#{help_url}/articles/configuring-npm-for-use-with-github-package-registry/"
      end

      # GitHub Python Registry Documentation
      #
      # Returns the URL string ("https://help.github.com/en/articles/configuring-pip-for-use-with-github-package-registry/")
      def pip_github_package_registry_url
        "#{help_url}/articles/configuring-pip-for-use-with-github-package-registry/"
      end

      # GitHub RubyGems Registry Documentation
      #
      # Returns the URL string ("https://help.github.com/en/articles/configuring-rubygems-for-use-with-github-package-registry/")
      def rubygems_github_package_registry_url
        "#{help_url}/articles/configuring-rubygems-for-use-with-github-package-registry/"
      end

      # GitHub NuGet Registry Documentation
      #
      # Returns the URL string ("https://help.github.com/en/articles/configuring-nuget-for-use-with-github-package-registry/")
      def nuget_github_package_registry_url
        "#{help_url}/articles/configuring-nuget-for-use-with-github-package-registry/"
      end

      # Returns documentation URL string for a given package type symbol
      def github_package_registry_url(package_type)
        if [:docker, :maven, :npm, :rubygems, :nuget].include?(package_type)
          send("#{package_type}_github_package_registry_url")
        else
          about_github_package_registry_url
        end
      end

      # The number of shards to use when creating the `registry_packages` index.
      def es_shard_count_for_registry_packages
        @es_shard_count_for_registry_packages ||= 1
      end
      attr_writer :es_shard_count_for_registry_packages

      def api_internal_package_registry_hmac_keys
        @api_internal_package_registry_hmac_keys ||=
          ENV["API_INTERNAL_PACKAGE_REGISTRY_HMAC_KEYS"].to_s.split
      end
      attr_writer :api_internal_package_registry_hmac_keys

    end
  end

  extend Config::Registry
end
