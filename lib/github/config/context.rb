# frozen_string_literal: true

require "context"

module GitHub
  module Config
    module Context

      # The context for the current runtime environment, such as the action
      # being performed, the actor, the affected objects, etc.
      def context
        @context ||= ::Context.new
      end
      attr_writer :context

    end
  end

  extend Config::Context
end
