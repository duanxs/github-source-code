# rubocop:disable Style/FrozenStringLiteralComment

##
# GitHub amen / statsd connection.

require "github"
require "datadog/statsd"
require "github/statsd"
require "github/dogstats"
require "resolv"

module GitHub
  module Config
    module Stats

      attr_writer :stats

      def stats
        return @stats if defined?(@stats)
        @stats = create_statsd(namespace: "github")
      end

      def create_statsd(namespace:)
        statsd = if GitHub.graphite_disabled?
          NullStatsD.new
        elsif stats_whitelist
          require "github/statsd/whitelist"
          GitHub::Statsd::Whitelist.new(stats_whitelist)
        else
          GitHub::Statsd.new
        end

        stats_hosts.each { |shard| statsd.add_shard(shard) }
        statsd.namespace = namespace
        statsd
      end

      # Private: Default host for dogstatsd.
      DEFAULT_DOGSTATSD_HOST = "127.0.0.1".freeze

      # Private: Default port for dogstatsd.
      DEFAULT_DOGSTATSD_PORT = 28125

      def default_tags
        tags = [
          "application:github",
          "application_role:#{GitHub.role}",
          "application_component:#{GitHub.component}",
          "deployed_to:#{GitHub.deployed_to}",
        ]

        if GitHub.kube?
          tags += kubernetes_tags
        end

        tags
      end

      def kubernetes_tags
          kube_tags = [
            "kube_namespace:#{GitHub.kubernetes_namespace}",
          ]

          kube_tags
      end

      def user_specified_tags
        ENV["DOGSTATSD_ADDITIONAL_TAGS"].to_s.split(",") || []
      end

      # Public: Statsd instance pointed at DataDog.
      def dogstats
        @dogstats ||= new_dogstats
      end

      def new_dogstats
        if GitHub.datadog_enabled?
          # The statsd client doesn't cache the hostname, so we do it here
          # to reduce the volume of DNS queries, which caused noticable
          # performance degradation in the Kubernetes environment.
          host = Resolv.getaddress(GitHub.environment.fetch("DOGSTATSD_HOST", DEFAULT_DOGSTATSD_HOST))
          port = (ENV["DOGSTATSD_PORT"] || DEFAULT_DOGSTATSD_PORT).to_i

          dogtags = default_tags + user_specified_tags
          GitHub::Dogstats.new(host, port, tags: dogtags)
        elsif ENV["DOGSTATSD_DEBUG"]
          DebugDogstatsD.new
        else
          NullDogstatsD.new
        end
      end

      # When GitHub.component or GitHub.role change, we need to reset the tags
      # so that they are accurate (see #dogstats)
      def reset_dogtags
        return if @dogstats.nil?
        @dogstats.tags = (default_tags + user_specified_tags)
      end
    end
  end

  class NullStatsD
    def initialize; @shards = []; end
    def timing(k, v, s = 1) end
    def histogram(k, v, s = 1) end
    def distribution(k, v, s = 1) end
    def count(k, v, s = 1) end
    def time(name, s = 1) yield end
    def increment(k, v = 1) end
    def decrement(k, v = 1) end
    def gauge(k, v) end
    def add_shard(*args) end
    def enable_buffering(b = nil) end
    def disable_buffering() end
    def flush_all() end
    attr_accessor :shards
    attr_accessor :namespace
  end

  class NullDogstatsD
    attr_accessor :host
    attr_accessor :port
    attr_accessor :namespace
    attr_accessor :max_buffer_size

    def initialize(*)
    end

    def increment(stat, opts = {})
      ensure_valid_opts(opts)
    end

    def decrement(stat, opts = {})
      ensure_valid_opts(opts)
    end

    def count(stat, count, opts = {})
      ensure_valid_opts(opts)
      ensure_valid_value(count)
    end

    def gauge(stat, value, opts = {})
      ensure_valid_opts(opts)
      ensure_valid_value(value)
    end

    def histogram(stat, value, opts = {})
      ensure_valid_opts(opts)
      ensure_valid_value(value)
    end

    def distribution(stat, value, opts = {})
      ensure_valid_opts(opts)
      if opts.key?(:sample_rate)
        raise ArgumentError, "`distribution` does not support the `sample_rate` option. Please remove it."
      end
      ensure_valid_value(value)
    end

    def timing(stat, ms, opts = {})
      ensure_valid_opts(opts)
      ensure_valid_value(ms)
    end

    def timing_since(stat, start, opts = {})
      ensure_valid_opts(opts)
      ensure_valid_start(start)
    end

    def time(stat, opts = {})
      ensure_valid_opts(opts)
      yield
    end

    def set(stat, value, opts = {})
      ensure_valid_opts(opts)
    end

    def service_check(name, status, opts = {})
      ensure_valid_opts(opts)
    end

    def event(title, text, opts = {})
      ensure_valid_opts(opts)
    end

    def batch
      yield self
    end

    def tags=(tags)
    end

    def tags
      []
    end

    private

    def ensure_valid_opts(opts)
      raise ArgumentError, "Expected hash, got #{opts.class} " unless opts.is_a?(Hash)
      if opts.key?(:tags) && !opts[:tags].is_a?(Array)
        raise ArgumentError, "Expected `tags` to be an array, but was #{opts[:tags].class}"
      end
    end

    def ensure_valid_value(value)
      raise ArgumentError unless value.is_a?(Numeric)
    end

    def ensure_valid_start(value)
      raise ArgumentError if !value.is_a?(Time) && !value.is_a?(Float)
    end
  end

  class DebugDogstatsD < NullDogstatsD
    def increment(stat, opts = {})
      warn "DATADOG: increment #{stat} #{opts.inspect}"
      super
    end

    def decrement(stat, opts = {})
      warn "DATADOG: decrement #{stat} #{opts.inspect}"
    end

    def count(stat, count, opts = {})
      warn "DATADOG: count #{stat} #{count} #{opts.inspect}"
      super
    end

    def gauge(stat, value, opts = {})
      warn "DATADOG: gauge #{stat} #{value} #{opts.inspect}"
      super
    end

    def histogram(stat, value, opts = {})
      warn "DATADOG: histogram #{stat} #{value} #{opts.inspect}"
      super
    end

    def distribution(stat, value, opts = {})
      warn "DATADOG: distribution #{stat} #{value} #{opts.inspect}"
      super
    end

    def timing(stat, ms, opts = {})
      warn "DATADOG: timing #{stat} #{ms} #{opts.inspect}"
      super
    end

    def time(stat, opts = {})
      warn "DATADOG: time #{stat} #{opts.inspect}"
      super
    end
  end

  extend Config::Stats

  # global variable used by external libraries (slumlord, raindrops):
  $stats = stats if !defined?($stats)
end
