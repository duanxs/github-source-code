# frozen_string_literal: true

require "aqueduct"

Aqueduct::Worker.configure do |config|
  config.heartbeat_check_interval_seconds = 5.seconds
  config.heartbeat_interval_seconds = GitHub::Aqueduct::Job::HEARTBEAT_INTERVAL
  config.handler = ->(job) { GitHub::Aqueduct::Job.execute(job.payload) }
  config.procline = ->(procline) { "#{procline} [#{GitHub.current_sha[0, 7]}]" }
  config.quiesce_check = -> { GitHub.resque_pauser.paused_site?(GitHub.server_site) }

  config.error_reporter = ->(err, job) {
    if err.is_a?(Aqueduct::Client::ClientError)
      Failbot.report(err, {
        app: "github-aqueduct",
      }.merge(err.metadata.map { |k, v| ["aqueduct_#{k}", v] }.to_h))
    else
      Failbot.report(err)
    end
  }

  # set by `cant_fork=` in AqueductWorkerAdapter.
  # config.fork_per_job = false

  config.after_startup do
    # Need to chdir under enterprise due to upgrade switching out dir inodes
    Dir.chdir Rails.root if GitHub.enterprise?
  end

  # config.before_fork do
    # we only fork in development, and reconnects happen in before_perform
  # end

  # config.after_fork do
    # not used, we only fork in development
  # end

  config.before_perform do |worker, job|
    # Reset the Failbot context before each job run.
    Failbot.reset!
    Failbot.push worker: worker.to_s

    # Start a publishing batch for more efficient publishing of events from
    # within a job.
    if GitHub.hydro_enabled?
      GitHub.hydro_publisher.start_batch
    end
  end

  config.after_perform do |worker|
    if GitHub.hydro_enabled?
      result = GitHub.hydro_publisher.flush_batch
      if result && !result.success?
        Failbot.report(result.error, {
          app: "github-hydro",
          areas_of_responsibility: [:analytics],
        })
      end
    end

    next if GitHub.resque_graceful_memory_limit.nil?

    memrss = GitHub::Memory.memrss
    GitHub.dogstats.distribution("worker.memrss", memrss)

    if memrss > GitHub.resque_graceful_memory_limit
      GitHub.dogstats.increment("resque.graceful_exit")
      worker.shutdown
    end

    Failbot.reset!
  end
end

module AqueductLifecycleCallbacks
  extend ActiveSupport::Concern

  included do
    before_enqueue :set_metadata

    before_execute :set_procline
    around_execute :set_github_component
    around_execute :configure_marginalia
    before_execute :configure_contexts
    before_execute :configure_failbot
    around_execute :memoize_flipper
    around_execute :reset_counters
    around_execute :set_tracing_data
    before_execute :publish_queued_notification
    around_execute :publish_performed_notification
  end

  # helper to determine actual job class (when wrapped by ActiveJob):
  def class_from_payload
    from_payload = job_class
    if from_payload == ActiveJob::QueueAdapters::ResqueAdapter::JobWrapper.name
      job_args = payload["args"]&.first
      from_payload = job_args["job_class"] if !job_args.blank? && !job_args["job_class"].blank?
    end

    from_payload
  end

  # helper to retrieve the area of responsibility for a given job, if possible
  def areas_of_responsibility
    class_from_payload.constantize.try(:areas_of_responsibility)
  rescue NameError
    nil
  end

  def set_metadata
    metadata[:queued_at] = Time.now.to_f
    metadata[:context] = expand_context_payload(GitHub.context.to_hash)
    metadata[:audit_context] = Audit.context.to_hash

    span = GitHub.tracer.last_span
    carrier = {}
    GitHub.tracer.inject(span.context, OpenTracing::FORMAT_TEXT_MAP, carrier) unless span.nil?
    metadata[:opentracing_carrier] = carrier unless carrier.empty?
  end

  def set_procline
    # This is reset by Aqueduct::Worker after the job is completed.
    $0 = "#{$0}: #{class_from_payload}"
  end

  def set_github_component
    original_component = GitHub.component
    GitHub.component = GitHub::Aqueduct::COMPONENT
    yield
  ensure
    GitHub.component = original_component
  end

  def configure_marginalia
    yield
  ensure
    Marginalia::Comment.clear!
  end

  def configure_contexts
    job_context = {
      job: class_from_payload,
      areas_of_responsibility: areas_of_responsibility,
    }

    audit_context = metadata.fetch("audit_context", {}).merge(job_context)
    github_context = metadata.fetch("context", {}).merge(job_context)

    Audit.context.reset
    Audit.context.push(audit_context)

    GitHub.context.reset
    GitHub.context.push(github_context)

    SensitiveData.context.reset
  end

  def configure_failbot
    queue_time = nil
    if (queued_at = metadata["queued_at"])
      queue_time = Time.now - Time.at(queued_at)
    end

    context = metadata["context"]

    Failbot.push(
      areas_of_responsibility: areas_of_responsibility,
      connections: proc { ApplicationRecord.connection_info },
      queue: queue,
      queue_time: queue_time,
      queued_from: (context && context["from"]),
      rails: Rails.version,
      request_id: (context && context["request_id"]),
      "#job": class_from_payload,
    )
  end

  def memoize_flipper
    begin
      original_flipper_memoizing = GitHub.flipper.adapter.memoizing?
      GitHub.flipper.adapter.memoize = true
      yield
    ensure
      GitHub.flipper.adapter.memoize = original_flipper_memoizing
    end
  end

  # GitHub::MysqlQueryCounter is used by tracing and the perform instrumentation, both
  # defined below.
  def reset_counters
    begin
      GitHub::MysqlInstrumenter.reset_stats
      GitHub::MysqlQueryCounter.start
      GitHub::JobStats.reset
      GitRPCLogSubscriber.reset_stats
      yield
    ensure
      GitHub::MysqlQueryCounter.stop
    end
  end

  def set_tracing_data
    begin
      GitHub.tracer.with_enabled(GitHub.tracing_enabled?) do
        job_name = class_from_payload.underscore
        job_name.gsub!("github/jobs/", "")

        tracing_carrier = metadata.fetch("opentracing_carrier", {})
        parent_context = GitHub.tracer.extract(OpenTracing::FORMAT_TEXT_MAP, tracing_carrier)

        # NOTE: FollowsFrom would be more appropriate for a background job, but lightstep-ruby doesn't support it yet.
        GitHub.tracer.with_span("job.#{job_name}", child_of: parent_context) do |span|
          span.set_tag("component", "jobs")
          span.set_tag("job.class", class_from_payload)
          span.set_tag("job.queue", queue)

          span.set_tag("job.queued_at", metadata["queued_at"])

          context = metadata.fetch("context", {})
          span.set_tag("guid:github_request_id", context["request_id"])
          span.set_tag("job.queued_from", context["from"])

          yield

          log_data = {event: "job read/write counts"}.merge(GitHub::MysqlQueryCounter.counts).symbolize_keys
          span.log(**log_data)
        end
      end

    end
  end

  def publish_queued_notification
    if metadata["queued_at"] != nil
      GitHub.publish(
        "queued.resque",
        Time.at(metadata["queued_at"]),
        Time.now,
        SecureRandom.hex(10),
        queue: queue,
        class: class_from_payload.underscore,
        backend: :aqueduct,
      )
    end
  end

  def publish_performed_notification
    success = false
    timer = Timer.start
    pre_perform_allocation_count = GC.stat(:total_allocated_objects)
    GitHub::JobStats.memory_usage_reset(job_class: class_from_payload)

    begin
      yield
      success = true
    ensure
      GitHub::JobStats.memory_usage_snapshot

      GitHub.publish("performed.resque",
                     timer.started_at,
                     Time.now,
                     SecureRandom.hex(10),
                     queue: @queue,
                     class: class_from_payload.underscore,
                     success: success,
                     db_counts: GitHub::MysqlQueryCounter.counts,
                     pre_perform_allocation_count: pre_perform_allocation_count,
                     backend: :aqueduct,
                    )

      GlobalInstrumenter.instrument("performed.job", {
        queue: @queue,
        job_class: class_from_payload.to_s,
        active_job_id: active_job_id_from_payload,
        timer: timer,
        success: success,
        backend: :aqueduct,
      })
    end
  end

  private

  def active_job_id_from_payload
    payload["job_id"]
  end

  def expand_context_payload(payload)
    payload[:created_at] ||= Time.now
    payload[:created_at] = (payload[:created_at].utc.to_f * 1000).round unless payload[:created_at].is_a?(Integer)

    if actor_ip = payload[:actor_ip]
      GitHub.dogstats.time "audit", tags: ["action:ip_lookup"] do
        location = GitHub::Location.look_up(actor_ip)
        location[:location] = {
          lat: location.delete(:latitude).to_f,
          lon: location.delete(:longitude).to_f,
        }

        payload[:actor_location] = location
      end
    end

    payload
  end
end

# These callbacks are implemented as a module rather than directly defined in
# the Job class itself so that the lifecycle configuration can live in the same
# place as the Aqueduct::Worker configuration.
GitHub::Aqueduct::Job.module_eval do
  include AqueductLifecycleCallbacks
end
