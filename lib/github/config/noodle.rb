# frozen_string_literal: true

module GitHub
  module Config
    module Noodle

      # The URL of the noodle-editor
      #
      # e.g. "https://noodle-production.breadsticks.kube-service.github.net"
      attr_accessor :noodle_editor_host_url

      # The URL for noodle-al-dente
      #
      # e.g. "https://noodle-al-dente.breadsticks.kube-service.github.net"
      attr_accessor :noodle_al_dente_host_url

    end
  end

  extend Config::Noodle
end
