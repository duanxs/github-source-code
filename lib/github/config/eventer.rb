# frozen_string_literal: true

module GitHub
  module Config
    module EventerConfig
      # Is the eventer service enabled or not.
      def eventer_enabled?
        return @eventer_enabled if defined?(@eventer_enabled)
        if hydro_enabled? && GitHub.eventer_host
          @eventer_enabled = true
        else
          @eventer_enabled = false
        end
      end
      attr_accessor :eventer_enabled

      def eventer_host
        GitHub.environment.fetch("EVENTER_HOST", @eventer_host)
      end
      attr_writer :eventer_host

      def eventer_hmac_key
        @eventer_hmac_key ||= ENV["EVENTER_HMAC_KEY"]
      end
      attr_writer :eventer_hmac_key

      def eventer_client
        @eventer_client ||= Eventer::Client.new(
          eventer_host,
          hmac_key: eventer_hmac_key,
        )
      end
      attr_writer :eventer_client
    end
  end

  extend Config::EventerConfig
end
