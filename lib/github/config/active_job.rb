# frozen_string_literal: true

require "active_job"

if GitHub.rails_6_0?
  # Do nothing for 6.0 right now so that we know we won't affect production
else
  ActiveJob::Base.skip_after_callbacks_if_terminated = true
end

Dir[File.expand_path("#{Rails.root}/app/jobs/*.rb", __FILE__)].each do |file|
  class_name = File.basename(file.sub(/\.(rb)$/, "")).gsub(/(?:^|_)(.)/) { $1.upcase }.gsub(/Github/, "GitHub")
  autoload class_name, file
end
