# rubocop:disable Style/FrozenStringLiteralComment

require "github"

module GitHub
  module Config
    # Mixin for the GitHub module that gives access to the memcache config
    # and also a memoized GitHub::Cache::Client singleton for memcache access.
    module Memcache
      def cache_config
        require "erb"
        require "yaml"

        file = "#{Rails.root}/config/memcached.yml"
        options = YAML.load(ERB.new(IO.read(file)).result)

        config = {}
        options["defaults"].each { |k, v| config[k.to_sym] = v }
        options[Rails.env].each  { |k, v| config[k.to_sym] = v }
        config[:namespace] ||= "github-"
        config[:namespace] << if GitHub.employee_unicorn?
          GitHub.host_name[0..9]
        else
          Rails.env
        end
        config
      end

      # Public: Return the GitHub::Cache instance
      def cache
        return @cache if defined?(@cache)
        self.cache = default_cache_client
      end

      # Public: Set the GitHub cache store and set up Rails cache store (if
      # we are in a Rails environment)
      def cache=(cache)
        @cache = cache
        set_up_rails_cache_store
        @cache
      end

      # Public: Set the global Rails cache stores.  We do this carefully with
      # defined? calls because there are areas where we set up the cache
      # where we do not have the Rails environment loaded.
      def set_up_rails_cache_store
        ::GitHub::Application.config.cache_store = GitHub.cache if defined?(::GitHub::Application)
        ::ActionController::Base.cache_store = GitHub.cache if defined?(::ActionController::Base)
      end

      def regional_caches
        @regional_caches ||= begin
          caches = {}
          datacenter_servers.each do |dc, servers|
            config = cache_config.merge(no_block: true, noreply: true, servers: servers)
            client = GitHub::Cache::Client.new(config)
            caches[dc] = client
          end
          caches
        end
      end

      private

      # Private: Create and return the default cache client
      def default_cache_client
        config = cache_config
        cache = GitHub::Cache::Client.new(config)
        cache.servers = Array(config.delete(:servers))
        cache
      end

      def datacenter_servers
        datacenters = {}
        # Each region has a variable like `IAD_MEMCACHED_CLUSTERS` containing values
        # like `CP1_IAD_MEMCACHED_SERVERS,VA3_IAD_MEMCACHED_SERVERS,AC4_IAD_MEMCACHED_SERVERS`
        regional_clusters = GitHub.environment["MEMCACHED_CLUSTERS"]

        unless regional_clusters.blank?
          regional_clusters.split(/,\s*/).each do |regional_cluster|
            memcache_nodes = GitHub.environment[regional_cluster]
            dc = env_site_name(regional_cluster.chomp("_MEMCACHED_SERVERS"))
            if dc != GitHub.server_site && !memcache_nodes.blank?
              datacenters[dc] = memcache_nodes.split(/,\s*/)
            end
          end
        end
        datacenters
      end

      def env_site_name(dc)
        dc.downcase.gsub("_", "-")
      end
    end
  end

  extend Config::Memcache
end
