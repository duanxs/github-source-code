# frozen_string_literal: true

module GitHub
  module Config
    module GitBackupsd
      def gitbackupsd_host
        host = ENV["GITBACKUPS_TLS_URL"] || "gitbackupsd"
      end

      def gitbackupsd_creds
        cert_root = File.join(ENV["RAILS_ROOT"], "config", "service_certificates", "gitbackupsd")
        files = ["chain.pem", "github-#{GitHub.role}.key", "github-#{GitHub.role}.crt"]
        certs = files.map { |f| File.open(File.join(cert_root, f)).read }
        GRPC::Core::ChannelCredentials.new *certs
      end
    end
  end

  extend Config::GitBackupsd
end
