# frozen_string_literal: true

module GitHub
  module Config
    module SplunkhecConfig
      def splunk_hec_enabled?
        return @splunk_hec_enabled if defined?(@splunk_hec_enabled)
        @splunk_hec_enabled = GitHub.splunk_hec_host && GitHub.splunk_hec_token
      end
      attr_accessor :splunk_hec_enabled

      def splunk_hec_host
        GitHub.environment.fetch("SPLUNK_HEC_HOST", @splunk_hec_host)
      end
      attr_writer :splunk_hec_host

      def splunk_hec_token
        GitHub.environment.fetch("SPLUNK_HEC_TOKEN", @splunk_hec_token)
      end
      attr_writer :splunk_hec_token
    end

    def splunk_hec_index_name
      GitHub.environment.fetch("SPLUNK_HEC_INDEX_NAME", @splunk_hec_index_name)
    end
    attr_writer :splunk_hec_index_name

  end
  extend Config::SplunkhecConfig
end
