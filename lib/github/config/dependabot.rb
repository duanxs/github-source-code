# frozen_string_literal: true

module GitHub
  module Config
    module Dependabot
      def dependabot_github_app
        return nil unless dependabot_enabled?
        @dependabot_github_app ||= Integration.find_by(
          owner_id: trusted_oauth_apps_owner,
          slug: dependabot_github_app_slug,
        )
      end

      def dependabot_github_app_name
        "Dependabot"
      end

      def dependabot_github_app_slug
        "dependabot"
      end

      def dependabot_help_url
        "#{GitHub.help_url}/github/managing-security-vulnerabilities/configuring-github-dependabot-security-updates"
      end

      def dependabot_beta_help_url
        "#{GitHub.help_url}/github/administering-a-repository/keeping-your-dependencies-updated-automatically"
      end

      def dependabot_beta_feedback_url
        "https://support.github.com/contact/feedback?contact%5Bsubject%5D=Dependabot+Beta&contact%5Bcategory%5D=security"
      end
    end
  end

  extend Config::Dependabot
end
