# rubocop:disable Style/FrozenStringLiteralComment

require "nokogiri"
require "html/pipeline"
require "github/html/pipeline"
require "github/html/plain_text_input_filter"

module GitHub
  # GitHub HTML processing filters and utilities. This module includes a small
  # framework for defining DOM based content filters and applying them to user
  # provided content.
  #
  # See GitHub::HTML::Filter for information on building filters.
  module HTML
    Filter                = ::HTML::Pipeline::Filter
    AutolinkFilter        = ::HTML::Pipeline::AutolinkFilter
    EmailReplyFilter      = ::HTML::Pipeline::EmailReplyFilter
    TextileFilter         = ::HTML::Pipeline::TextileFilter
    DocumentFragment      = ::HTML::Pipeline::DocumentFragment

    # An object for results passed back from the Pipelines
    # This allows us to have some explicit-ness around the types of things that
    # pipelines add to the repsonse.
    #
    # Members of the Result:
    #   output - the DocumentFragment or String result of the Pipeline
    #   mentioned_users - see GitHub::HTML::MentionFilter
    #   mentioned_usernames - see GitHub::HTML::MentionFilter
    #   mentioned_teams - see GitHub::HTML::TeamMentionFilter
    #   commits - see GitHub::HTML::CommitMentionFilter
    #   commits_count - see GitHub::HTML::CommitMentionFilter
    #   issues - see GitHub::HTML::IssueMentionFilter
    #   task_list_items - See TaskList::Filter
    #   toc - See TableOfContentsFilter
    #   html_safe - See GitHub::HTML::SanitizationFilter and GitHub::HTML::PlainTextInputFilter
    class Result
      attr_writer :task_list_items
      attr_accessor :output, :mentioned_users, :mentioned_usernames, :mentioned_teams, :commits, :commits_count, :issues, :toc, :html_safe, :discussions

      def initialize(output = nil, mentioned_users = nil, mentioned_usernames = nil,
        mentioned_teams = nil, commits = nil, commits_count = nil, issues = nil,
        task_list_items = nil, toc = nil, html_safe = nil, new_urls = nil, discussions = nil)
        @output              = output
        @mentioned_users     = mentioned_users
        @mentioned_usernames = mentioned_usernames
        @mentioned_teams     = mentioned_teams
        @commits             = commits
        @commits_count       = commits_count
        @new_urls            = new_urls
        @issues              = issues
        @task_list_items     = task_list_items
        @toc                 = toc
        @html_safe           = html_safe
        @discussions         = discussions
      end

      def to_s
        output.to_s
      end

      def [](variable_symbol)
        instance_variable_get("@#{variable_symbol}")
      end

      def []=(variable_symbol, value)
        instance_variable_set("@#{variable_symbol}", value)
      end

      def to_h
        instance_variables.each_with_object({}) { |variable, hash|
          hash[variable.to_s.delete("@").to_sym] = instance_variable_get(variable)
        }
      end

      # Public: returns an Array of TaskList::Item objects.
      def task_list_items
        @task_list_items || []
      end

    end

    def self.parse(*args)
      ::HTML::Pipeline.parse(*args)
    end

    require "github/html/body_content"

    # additions available to all filters
    require "github/html/filter"

    # Filter implementations
    require "github/html/commit_mention_filter"
    require "github/html/issue_mention_filter"
    require "github/html/mention_filter"
    require "github/html/team_mention_filter"
    require "github/html/camo_filter"
    require "github/html/math_filter"
    require "task_list/filter"
    require "github/html/image_max_width_filter"
    require "github/html/emoji_filter"
    require "github/html/empty_anchor_filter"
    require "github/html/name_prefix_filter"
    require "github/html/sanitization_filter"
    require "github/html/https_filter"
    require "github/html/markdown_filter"
    require "github/html/relative_link_filter"
    require "github/html/raw_image_filter"
    require "github/html/leading_newline_filter"
    require "github/html/syntax_highlight_filter"
    require "github/html/markup_filter"
    require "github/html/utf8_filter"
    require "github/html/filter_utils"
    require "github/html/table_of_contents_filter"
    require "github/html/rel_nofollow_filter"

    context = {}
    context[:flags] = Rinku::AUTOLINK_SHORT_DOMAINS if GitHub.enterprise?

    # Inherit sanitization whitelist from html-pipeline but customize it further.
    WHITELIST = SanitizationFilter::WHITELIST.dup
    WHITELIST[:attributes] = WHITELIST[:attributes].each_with_object({}) { |(el, attrs), hash|
      # rel    - GitHub uses `rel` for things like `rel=facebox` to trigger
      #          behavior that could be unsafe with user controlled content.
      # target - `target=_blank` allows the linked to site to perform
      #          "tab nabbing" attacks by using `window.opener`.
      # vspace - messes up layout of a page and can render the page it's
      #          included on unusable.
      hash[el] = attrs - %w(rel target vspace)
    }
    WHITELIST[:attributes] = WHITELIST[:attributes].dup
    WHITELIST[:attributes][:all] = WHITELIST[:attributes][:all].dup << "id"

    email_whitelist = WHITELIST.merge(
      elements: %w(a div span),
      attributes: {
        "a" => %w(href),
        "div" => %w(class style),
        "span" => %w(class),
      },
    )

    limited_whitelist = WHITELIST.merge(
      elements: %w(b i strong em a pre code img ins del sup sub p ol ul li br h1 h2 h3 h4 h5 h6),
    )

    MARKUP_WHITELIST = WHITELIST.dup
    MARKUP_WHITELIST[:attributes] = WHITELIST[:attributes].merge(
      "table" => WHITELIST[:attributes].fetch("table", []) + %w(data-table-type),
    )

    # Pipeline providing sanitization and image hijacking but no mention
    # related features.
    SimplePipeline = Pipeline.new [
      UTF8Filter,
      SanitizationFilter,
      EmptyAnchorFilter,
      TableOfContentsFilter, # add 'name' anchors to all headers
      CamoFilter,
      ImageMaxWidthFilter,
      SyntaxHighlightFilter,
      RelativeLinkFilter,
      EmojiFilter,    # Must be after RelativeLinkFilter
      RawImageFilter, # Must be after RelativeLinkFilter
      AutolinkFilter, # Perform an autolinking pass, for those GitHub::Markup
                      # languages that don't have built-in autolinking
      NamePrefixFilter,
      RelNofollowFilter,
    ], context.merge(whitelist: WHITELIST), Result

    # Pipeline used to Render the Markdown content in pages2
    # autogenerated websites
    PagesPipeline = Pipeline.new [
      UTF8Filter,
      MarkdownFilter,
      SanitizationFilter,
      EmptyAnchorFilter,
      TableOfContentsFilter, # add 'name' anchors to all headers
      MentionFilter,
      TeamMentionFilter,
      EmojiFilter,
      SyntaxHighlightFilter,
    ], context.merge(gfm: false, whitelist: WHITELIST), Result

    # Pipeline used to format simple email replies on the site.
    #
    # Used with Issue, Commit, Pull Request comments only.
    EmailPipeline = Pipeline.new [
      UTF8Filter,
      EmailReplyFilter,
      SanitizationFilter,
      EmptyAnchorFilter,
      AutolinkFilter,
      MentionFilter,
      TeamMentionFilter,
      IssueMentionFilter,
      CommitMentionFilter,
      EmojiFilter,
      NamePrefixFilter,
      LeadingNewlineFilter,
    ], context.merge(
      whitelist: email_whitelist,
      hide_quoted_email_addresses: true,
      disable_hovercard_attributes: true,
    ), Result

    # Pipeline used for really old comments and maybe other textile content
    # I guess.
    TextilePipeline = Pipeline.new [
      UTF8Filter,
      TextileFilter,
      SanitizationFilter,
    ], { whitelist: limited_whitelist }, Result

    # Pipeline used for short descriptions, like repository descriptions.
    DescriptionPipeline = Pipeline.new [
      UTF8Filter,
      PlainTextInputFilter,
      MentionFilter,
      TeamMentionFilter,
      CommitMentionFilter,
      IssueMentionFilter,
      EmojiFilter,
      AutolinkFilter,
      RelNofollowFilter,
    ], context, Result

    # Highlighted search results only require the `em` tag.
    highlighted_search_results_whitelist = WHITELIST.merge(
      elements: %w(em),
    )
    # Pipeline used for highlighted search results, like repository descriptions that have been searched for.
    HighlightedSearchResultPipeline = Pipeline.new [
      UTF8Filter,
      SanitizationFilter,
      MentionFilter,
      TeamMentionFilter,
      CommitMentionFilter,
      IssueMentionFilter,
      EmojiFilter,
      AutolinkFilter,
      RelNofollowFilter,
    ], context.merge(whitelist: highlighted_search_results_whitelist), Result

    # Used for rendering the `value` field of MemexProjectColumnValue, where the associated
    # MemexProjectColumn has a data_type of :text.
    MemexTextColumnPipeline = Pipeline.new(
      [SanitizationFilter, EmojiFilter, AutolinkFilter],
      context,
      Result
    )

    extend self
  end
end
