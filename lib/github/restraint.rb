# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  # simple locking mechanism that allows N jobs to be running under a given key
  class Restraint
    class UnableToLock < StandardError ; end

    attr_reader :redis
    private :redis

    # Public: Initialize a Restraint
    #
    # redis - an optional active Redis connection, defaults to
    # GitHub.resque_redis
    #
    # Example:
    #
    #   restraint = GitHub::Restraint.new
    def initialize
      @redis = GitHub.resque_redis
    end

    # Public: Attempt to lock and yield if success
    #
    # base_key - a String representing the key you want to lock against
    # n        - an Integer representing the number of jobs that can run against this key
    # ttl      - an Integer representing when this key should be expired if the process is terminated abnormally
    #
    # raises UnableToLock if unable to lock at this time
    #
    # Example:
    #
    #   # attempt to obtain 1 out of 6 locks for `backups_33`, auto-expire the lock in 1 hour
    #   restraint.lock!('backups_fs33', 6, 1.hour)
    #
    def lock!(base_key, n, ttl)
      if key = obtain_lock(base_key, n, ttl)
        begin
          yield
        ensure
          redis.del key
        end
      else
        raise UnableToLock, "Unable to obtain lock"
      end
    end

    # Public: generate the list of keys that can be used for a lock
    #
    # base_key - a String representing the key you want to lock against
    # n        - an Integer representing the number of jobs that can run against this key
    #
    # Returns an Array of Strings, each representing a key name
    def keys(base_key, n)
      (1..n).map { |ii| "#{base_key}:#{ii}" }
    end

    # Public: attempt to obtain a lock
    #
    # base_key - a String representing the key you want to lock against
    # n        - an Integer representing the number of jobs that can run against this key
    # ttl      - the expiration time for a lock before it's released automaticaly
    #
    # Returns the key name on success, false on failure
    def obtain_lock(base_key, n, ttl)
      keys(base_key, n).each do |key|
        if redis.set(key, 1, nx: true, ex: ttl.to_i)
          return key
        end
      end
      false
    end
  end
end
