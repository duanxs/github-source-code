# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module Location
    FILE_LOCATION = if !Rails.production?
      "/usr/local/share/GeoIP/GeoIP2-City.mmdb"
    else
      "/usr/share/GeoIP/GeoIP2-City.mmdb"
    end
    # Public: Looks up an IP address location.
    #
    # ip - String IP address
    #
    # Returns a hash containing zero or more location descriptions possibly
    # including :city, :region_name, :country_code, :country_code3,
    # :country_name, :latitude, :longitude
    def self.look_up(ip)
      return {} unless db && ip.present?
      db.lookup(ip) || {}
    end

    # Internal: Get GeoIP database.
    #
    # Returns GeoIP::City instance or nil if unavailable.
    def self.db
      return @db if defined? @db
      @db = db_path ? GeoIP2Compat.new(db_path) : nil
    end

    # Internal: Get path to geoip .dat file.
    #
    # Returns String path file or nil.
    def self.db_path
      if File.exist?(FILE_LOCATION)
        FILE_LOCATION
      end
    end
  end
end
