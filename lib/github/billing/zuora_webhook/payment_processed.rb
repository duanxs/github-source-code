# frozen_string_literal: true

module GitHub::Billing
  module ZuoraWebhook
    # Handler for PaymentProcessed webhooks from Zuora
    class PaymentProcessed
      include GitHubDataAccess
      include HydroInstrumentation
      include ZuoraDataAccess

      attr_reader :account_id, :billing_transaction, :payment_id

      NOT_PROCESSED = "Zuora subscription not synched, cannot process payment"

      # Public: Handle a PaymentProcessed webhook from Zuora
      #
      # webhook - A ZuoraWebhook object
      #
      # Returns nothing
      def self.perform(*args)
        new(*args).perform
      end

      # Public: Initialize a new PaymentProcessed webhook handler
      #
      # webhook - A ZuoraWebhook object
      def initialize(webhook)
        @account_id = webhook.account_id
        @payment_id = webhook.payment_id
      end

      # Public: Handle a PaymentProcessed webhook from Zuora
      #
      # Returns nothing
      def perform
        return if already_processed_transaction?
        log_restricted_user_transactions(account) if account&.has_any_trade_restrictions?

        if account_should_be_refunded?
          if user_account_deleted?
            customer&.destroy
            plan_subscription&.destroy
          end

          refund_payment
          close_invoices
          return
        end

        unless has_plan_attached?
          raise NOT_PROCESSED
        end

        process_payment
        GitHub.dogstats.increment("zuora.payment.count", tags: datadog_tags)
        GitHub.dogstats.count("zuora.payment.amount_in_cents", zuora_payment.amount_in_cents, tags: datadog_tags)
      end

      private

      def account_should_be_refunded?
        plan_subscription_deleted? || user_account_deleted? || user_charged_for_plan_after_downgrade_to_free?
      end

      # Internal: Checks if the transaction has already been processed based on the
      # transaction reference ID
      #
      # Returns Boolean
      def already_processed_transaction?
        if plan_subscription_deleted? || user_account_deleted?
          false
        else
          account.billing_transactions.where(transaction_id: zuora_payment.reference_id).any?
        end
      end

      # Internal: Checks if the plan subscription has been deleted
      #
      # Returns Boolean
      def plan_subscription_deleted?
        plan_subscription.nil?
      end

      # Internal: Checks if the user account has been deleted
      #
      # Returns Boolean
      def user_account_deleted?
        account.nil?
      end

      # Internal: Checks if the user has downgraded to the free plan but still has
      # active Zuora subscription GitHub plan
      #
      # Returns Boolean
      def user_charged_for_plan_after_downgrade_to_free?
        return account.plan.free? unless GitHub.flipper[:free_plan_mismatch_refunds_only].enabled?(account)

        result = account.plan.free? && !zuora_subscription&.plan.nil?
        if result
          GitHub.dogstats.increment("billing.payment_processed.zuora_plan_mismatch")
        end

        result
      end

      # Internal: Checks if the `plan_subscription` has an associated plan
      # on Zuora.
      #
      # Returns boolean
      def has_plan_attached?
        plan_subscription.zuora_subscription_number? || plan_subscription.synchronize_with_lock
      end

      # Internal: Take the information sent to the webhook and ensure our
      # data reflects the successful payment in Zuora.
      #
      # Returns nothing
      def process_payment
        reset_billing_status
        create_billing_transaction
        send_receipt
        transfer_sponsors_payments
        instrument_payment_transaction(success: true)
      end

      # Internal: Refunds or voids the payment that was processed
      #
      # This occurs when we've processed a payment that we shouldn't have;
      # for example, if the user has downgraded to the free plan
      #
      # Returns nothing
      # Raises StandardError if both refunding and voiding are unsuccessful
      def refund_payment
        transaction = Billing::BillingTransaction.new(
          amount_in_cents: zuora_payment.amount_in_cents,
          platform: :zuora,
          platform_transaction_id: payment_id,
        )

        refund_result = GitHub::Billing::Refund.new(transaction).process(zuora_payment.amount_in_cents)
        return if refund_result.success?

        void_result = transaction.void
        return if void_result.success?

        raise "Unable to void/refund payment for free user"
      end

      # Internal: Closes the balance on all invoices that the payment was
      # applied to so that the user is not billed again
      #
      # Returns nothing
      def close_invoices
        Billing::Zuora::ZeroOutInvoices.for_transaction(payment_id)
      end

      # Internal: Reset the user's billing for a new cycle
      #
      # Returns nothing
      def reset_billing_status
        ::Billing::PlanSubscription::ResetBillingStatus.perform \
          plan_subscription,
          balance: (zuora_account["metrics"]["balance"] * 100).to_i,
          next_billing_date: zuora_subscription.charged_through_date.to_s
      end

      # Internal: Create or update a BillingTransaction for this payment
      #
      # Returns nothing
      def create_billing_transaction
        @billing_transaction = ::Billing::PlanSubscription::CreateBillingTransaction.perform \
          plan_subscription,
          service_ends_at: zuora_subscription.charged_through_date.to_s,
          zuora_transaction: zuora_payment
      end

      # Internal: Create a Transfer to a Stripe Connect account to pay
      # maintainers for their sponsorships
      def transfer_sponsors_payments
        return unless can_transfer_sponsors_payment?

        ::Billing::Stripe::TransferPayments.perform \
          billing_transaction.line_items.sponsorships,
          zuora_payment
      end

      # Internal: Are their sponsorships that are able to be transferred to
      # Stripe connect?
      #
      # Returns Boolean
      def can_transfer_sponsors_payment?
        billing_transaction.line_items.sponsorships.any? do |sponsorship|
          sponsorship.subscribable.listing.stripe_transfers_enabled?
        end
      end

      # Internal: Send a receipt for the transaction to the user
      #
      # Returns nothing
      def send_receipt
        ::Billing::PlanSubscription::SendReceipt.perform \
          plan_subscription,
          billing_transaction: billing_transaction
      end

      # Internal: The tags to use for Datadog metrics
      #
      # Returns Array
      def datadog_tags
        [
          "gateway:#{zuora_payment.gateway.parameterize}",
          "is_retry:#{zuora_payment.is_retry?}",
          "authorization:approved",
        ]
      end

      # Internal: Logs restricted user transaction to datadog
      #
      # Returns nothing
      def log_restricted_user_transactions(user)
        return if zuora_payment.is_retry?

        GitHub.dogstats.increment(
            "billing.trade_controls_restriction.billing_transaction.successful_transaction_for_restricted_user",
            tags: ["user:#{user}", "actor:#{user}"],
        )
      end
    end
  end
end
