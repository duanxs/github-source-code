# frozen_string_literal: true

module GitHub::Billing
  module ZuoraWebhook
    # Handler for PaymentRefundProcessed webhooks from Zuora
    class PaymentRefundProcessed
      PartialRefundWithSponsorshipError = Class.new(StandardError)
      include ZuoraDataAccess

      attr_reader :payment_id, :refund_id

      # Public: Handle a PaymentRefundProcessed webhook from Zuora
      #
      # webhook - A ZuoraWebhook object
      #
      # Returns nothing
      def self.perform(*args)
        new(*args).perform
      end

      # Public: Initialize a new PaymentRefundProcessed webhook handler
      #
      # webhook - A ZuoraWebhook object
      def initialize(webhook)
        @payment_id = webhook.payment_id
        @refund_id = webhook.refund_id
      end

      # Public: Handle a PaymentProcessed webhook from Zuora
      #
      # Returns nothing
      def perform
        return unless sale_transaction
        return if existing_refund_transaction?

        ::Billing::BillingTransaction.transaction do
          refund_transaction = sale_transaction.dup
          refund_transaction.update(
            amount_in_cents: -1 * refund_amount_in_cents,
            old_plan_name: sale_transaction.plan_name,
            plan_name: refund_transaction.user&.plan&.name,
            platform_transaction_id: payment_id,
            sale_transaction_id: sale_transaction.transaction_id,
            last_status: :settled,
            transaction_id: refund_transaction_id,
            transaction_type: "refund",
          )

          reverse_stripe_transfer
        end

        ::GitHub.dogstats.increment("billing.refund")
      end

      private

      def existing_refund_transaction?
        ::Billing::BillingTransaction.where(transaction_id: refund_transaction_id).exists?
      end

      def reverse_stripe_transfer
        return unless should_refund_sponsors_payment?

        if sale_transaction.amount_in_cents != refund_amount_in_cents
          raise PartialRefundWithSponsorshipError.new(
            "Stripe transfers for sponsorships aren't automatically reversed for " \
            "partial refunds. Human intervention is required to reverse the correct " \
            "amount (if any) from the Stripe transfer.",
          )
        end

        ::Billing::Stripe::TransferReversal.perform(
          sale_transaction_id: sale_transaction.platform_transaction_id,
          stripe_refund_id: refund_transaction_id,
          zuora_refund_id: refund_id,
        )
      end

      def should_refund_sponsors_payment?
        sale_transaction.line_items.sponsorships.any? do |sponsorship|
          sponsorship.subscribable.listing.stripe_transfers_enabled?
        end
      end

      def refund_transaction_id
        zuora_refund["ReferenceID"]
      end

      def refund_amount_in_cents
        zuora_refund["Amount"] * 100
      end

      def sale_transaction
        @_sale_transaction ||= ::Billing::BillingTransaction.find_by(platform_transaction_id: payment_id)
      end
    end
  end
end
