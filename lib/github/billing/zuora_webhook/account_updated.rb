# frozen_string_literal: true

module GitHub::Billing
  module ZuoraWebhook
    # Handler for AccountUpdated webhooks from Zuora
    class AccountUpdated
      include GitHubDataAccess

      attr_reader :account_id

      # Public: Handle an AccountUpdated webhook from Zuora
      #
      # webhook - A ZuoraWebhook object
      #
      # Returns nothing
      def self.perform(*args)
        new(*args).perform
      end

      # Public: Initialize a new AccountUpdated webhook handler
      #
      # webhook - A ZuoraWebhook object
      def initialize(webhook)
        @account_id = webhook.account_id
      end

      # Public: Handle a AccountUpdated webhook from Zuora
      #
      # Returns nothing
      def perform
        return if plan_subscription.nil?

        customer.update_from_zuora
      end
    end
  end
end
