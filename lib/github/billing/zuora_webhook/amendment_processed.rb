# frozen_string_literal: true

module GitHub::Billing
  module ZuoraWebhook
    # Handler for AmendmentProcessed webhooks from Zuora
    class AmendmentProcessed
      attr_reader :subscription_id, :zuora_webhook

      # Public: Handle a AmendmentProcessed webhook from Zuora
      #
      # subscription_id - The String subscription ID
      #
      # Returns nothing
      def self.perform(*args)
        new(*args).perform
      end

      # Public: Initialize a new AmendmentProcessed webhook handler
      #
      # zuora_webhook - A `Billing::ZuoraWebhook` instance
      def initialize(zuora_webhook)
        @subscription_id = zuora_webhook.subscription_id
      end

      # Public: Handle a AmendmentProcessed webhook from Zuora
      #
      # Returns nothing
      def perform
        subscription = Subscription.fetch_by_subscription_id(subscription_id) || return

        if subscription.enterprise?
          subscription.ensure_account_ids_match!

          EnterpriseSubscriptionSynchronizer.new(subscription).sync
        elsif subscription.organization?
          subscription.ensure_owner_invoiced!
          subscription.ensure_account_ids_match!

          InvoicedOrganizationSubscriptionSynchronizer.new(subscription).sync
        else
          # No-op if neither Dotcom organization ID and enterprise account ID are
          # associated with the subscription. This can happen for GHES-only customers
          # when the initial callout from Zuora fails for some reason and we retry
          # the webhook via `RetrieveFailedZuoraWebhooksJob`.
          return
        end
      end
    end
  end
end
