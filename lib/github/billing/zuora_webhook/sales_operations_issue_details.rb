# frozen_string_literal: true

module GitHub::Billing
  module ZuoraWebhook
    class SalesOperationsIssueDetails

      def initialize(webhook)
        @webhook = webhook
      end

      # Public: The relevant information needed to open a sales
      # operations issue for the sales serve webhooks
      #
      # Returns Hash
      def generate
        return {} unless webhook.is_sales_serve_kind?

        {
          new_issue_link: new_issue_link,
          title: title,
          description: description,
          ask: ask,
          links: links
        }
      end

      # Public: Return a prettified string version of the details
      #
      # Returns String
      def to_s
        details = generate

        buffer = StringIO.new
        buffer.puts "Link:"
        buffer.puts details[:new_issue_link]
        buffer.puts "\nTitle:"
        buffer.puts details[:title]
        buffer.puts "\nDescription:"
        buffer.puts details[:description]
        buffer.puts "\nAsk:"
        buffer.puts details[:ask]
        buffer.puts "\nLinks:"
        buffer.puts details[:links].compact.join(" - ")

        buffer.string
      end

      private

      attr_reader :webhook

      def new_issue_link
        "https://github.com/github/sales-operations/issues/new?labels=sales-support" \
        "%2C+Priority+-+To+Be+Triaged&template=25+-+Standard+Issue.md&title=Standard+Issue"
      end

      def title
        "#{business.slug} #{webhook.kind} webhook unable to be processed due to re-used zuora account id"
      end

      def description
        base = "We received a Zuora webhook targeting #{business.slug}. "
        if existing_account_reference
          base += "It is already in use for the #{existing_account_reference} #{existing.class.to_s.downcase}. "
        else
          base += "An existing customer is found with no associations. "
        end
        base + "Webhook ID for future @github/gitcoin first responder: #{webhook.id}"
      end

      def ask
        "How should these accounts be associated to each other and/or Zuora"
      end

      def links
        [existing_account_zuora_link, existing_account_stafftools, webhook_account_stafftools]
      end

      # Internal: The customer account tied to the subscription found in the webhook
      #
      # Returns a Customer record
      def customer
        @_customer ||= Customer.find_by(zuora_account_number: subscription[:accountNumber])
      end

      # Internal: The account associated with the found Customer record
      #
      # Returns an Organization or Business
      def existing
        @_existing ||= (customer.organizations.first || customer.business)
      end

      # Internal: The Zuora subscription referenced in the webhook
      #
      # Returns Zuorest Subscription
      def subscription
        @_subscription ||= Zuorest::Model::Subscription.find(webhook.subscription_id)
      end

      # Internal: The business the webhook is targeting
      #
      # Returns a Business
      def business
        @_business ||= Business.find(subscription[:DotcomEntAccountId__c])
      end

      # Internal: The way to identify the existing account
      #
      # Returns String
      def existing_account_reference
        return nil unless existing
        existing.organization? ? existing.login : existing.slug
      end

      # Internal: The type of account the existing account is
      #
      # Returns String
      def existing_account_type
        return nil unless existing
        existing.organization? ? existing.class.to_s.downcase : "enterprises"
      end

      # Internal: The Zuora link to the existing account
      #
      # Returns String
      def existing_account_zuora_link
        "[#{existing_account_reference} Zuora](#{GitHub.zuora_rest_server}/apps/CustomerAccount.do?method=view&id=#{customer.zuora_account_id})"
      end

      # Internal: The stafftools link to the existing account
      #
      # Returns String
      def existing_account_stafftools
        return "" unless existing_account_reference
        "[#{existing_account_reference} stafftools](https://admin.github.com/stafftools/#{existing_account_type}/#{existing_account_reference})"
      end

      # Internal: The Zuora link to the targeted account
      #
      # Returns String
      def webhook_account_stafftools
        "[#{business.slug} stafftools](https://admin.github.com/stafftools/enterprises/#{business.slug})"
      end
    end
  end
end
