# frozen_string_literal: true

module GitHub::Billing
  module ZuoraWebhook
    class Subscription
      def self.fetch_by_subscription_id(subscription_id)
        subscription = Zuorest::Model::Subscription.find(subscription_id)
        if subscription[:success]
          new(subscription)
        else
          nil
        end
      end

      def initialize(raw_subscription)
        @raw_subscription = raw_subscription
      end

      def enterprise?
        raw_subscription[:DotcomEntAccountId__c].present?
      end

      def organization?
        raw_subscription[:DotcomOrgId__c].present?
      end

      def owner
        @_owner ||= if enterprise?
          Business.find(raw_subscription[:DotcomEntAccountId__c])
        else
          Organization.find(raw_subscription[:DotcomOrgId__c])
        end
      end

      def ensure_account_ids_match!
        if owner.customer&.zuora_account_id.present? && owner.customer.zuora_account_id != account_id
          raise GitHub::Zuora::WebhookError, "Amendment webhook account id does not match customer account id"
        end
      end

      def ensure_owner_invoiced!
        if !enterprise? && !owner.invoiced?
          raise GitHub::Zuora::WebhookError, "Amendment webhook received for non-invoiced organization"
        end
      end

      def id
        raw_subscription[:id]
      end

      def account_id
        raw_subscription[:accountId]
      end

      def account_number
        raw_subscription[:accountNumber]
      end

      def subscription_number
        raw_subscription[:subscriptionNumber]
      end

      def term_end_date
        if raw_subscription[:termEndDate].present?
          Date.parse(raw_subscription[:termEndDate])
        end
      end

      def term_start_date
        if raw_subscription[:termStartDate].present?
          Date.parse(raw_subscription[:termStartDate])
        end
      end

      # Public : Determine the number of seats on the subscription
      #
      # Returns integer of seats found on subscription
      def seats
        @_seats ||= raw_subscription.rate_plans.reduce(0) do |acc, rate_plan|
          rate_plan.rate_plan_charges.reduce(acc) do |sum, rate_plan_charge|
            unless lfs_charge?(rate_plan_charge) || usage_refill_charge?(rate_plan_charge)
              sum + rate_plan_charge[:Provisionable_Quantity__c].to_i
            else
              sum
            end
          end
        end
      end

      # Public: Given a subscription, determine the plan on the subscription
      #
      # Returns string of plan found on subscription or nil
      def plan
        raw_subscription.rate_plans.each do |rate_plan|
          rate_plan.rate_plan_charges.each do |rate_plan_charge|
            plan = rate_plan_charge[:Plan_Name__c]
            return plan unless plan.blank?
          end
        end

        nil
      end

      # Public: Returns active rate plan charges
      #
      # Returns array of Billing::Zuora::Subscription::RatePlanCharge objects
      def active_rate_plan_charges
        raw_rate_plan_charges = raw_subscription.rate_plans.flat_map { |rate_plan| rate_plan["ratePlanCharges"] }

        raw_rate_plan_charges.each_with_object(Hash.new) do |raw_rate_plan_charge, result|
          rate_plan_charge = Billing::Zuora::Subscription::RatePlanCharge.new(raw_rate_plan_charge)

          if rate_plan_charge.active?
            result[rate_plan_charge.product_rate_plan_charge_id] = { number: rate_plan_charge.number }
          end
        end
      end

      # Public: Determine the data packs on the subscription
      #
      # Returns integer of data packs found on subscription
      def data_packs
        @_data_packs ||= raw_subscription.rate_plans.reduce(0) do |acc, rate_plan|
          rate_plan.rate_plan_charges.reduce(acc) do |sum, rate_plan_charge|
            if lfs_charge?(rate_plan_charge)
              sum + rate_plan_charge[:quantity].to_i
            else
              sum
            end
          end
        end
      end

      # Public: Returns an array of the prepaid usage refill rate plan charges
      #
      # Returns integer of data packs found on subscription
      def usage_refill_rate_plan_charges
        @_usage_refills ||= rate_plan_charges.select do |rate_plan_charge|
          usage_refill_charge?(rate_plan_charge) && rate_plan_charge.price.present?
        end
      end

      private

      attr_reader :raw_subscription

      def rate_plan_charges
        @_rate_plans_charges ||= raw_subscription.rate_plans.flat_map do |rate_plan|
          rate_plan.rate_plan_charges.map do |rate_plan_charge|
            Billing::Zuora::Subscription::RatePlanCharge.for(rate_plan_charge)
          end
        end
      end

      # Private: Given a RatePlanCharge, determines if the charge is for LFS packs.
      #
      # rate_plan_charge - Zuorest::Model::Subscription::RatePlanCharge
      #
      # Returns true if LFS charge, false otherwise.
      def lfs_charge?(rate_plan_charge)
        GitHub.zuora_lfs_rate_plan_charge_ids.include?(rate_plan_charge[:productRatePlanChargeId])
      end

      # Internal: Given a RatePlanCharge, determines if the charge is for usage refills.
      #
      # rate_plan_charge - Zuorest::Model::Subscription::RatePlanCharge
      #
      # Returns true if usage refill charge, false otherwise.
      def usage_refill_charge?(rate_plan_charge)
        GitHub.zuora_metered_refill_rate_plan_charge_ids.include?(rate_plan_charge[:productRatePlanChargeId])
      end
    end
  end
end
