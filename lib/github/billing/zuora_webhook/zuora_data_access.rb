# frozen_string_literal: true

module GitHub::Billing
  module ZuoraWebhook
    # Common methods for accessing Zuora data in Zuora webhooks
    module ZuoraDataAccess
      # Internal: The user's Zuora account
      #
      # Returns Zuorest::Model::Account
      def zuora_account
        @zuora_account ||= customer.zuora_account
      end

      # Internal: The Zuora invoice referenced in this webhook
      #
      # Returns Billing::ZuoraInvoice
      def zuora_invoice
        @zuora_invoice ||= ::Billing::ZuoraInvoice.new(invoice_id)
      end

      # Internal: The Zuora payment referenced in this webhook
      #
      # Returns Billing::ZuoraPayment
      def zuora_payment
        @zuora_payment ||= ::Billing::ZuoraPayment.find(payment_id)
      end

      # Internal: The Zuora refund referenced in this webhook
      #
      # Returns Hash
      def zuora_refund
        @zuora_refund ||= ::GitHub.zuorest_client.get_refund(refund_id)
      end

      # Internal: The user's Zuora subscription
      #
      # Returns Billing::ZuoraSubscription
      def zuora_subscription
        @zuora_subscription ||= plan_subscription.zuora_subscription
      end
    end
  end
end
