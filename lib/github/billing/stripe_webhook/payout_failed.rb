# frozen_string_literal: true
#
module GitHub::Billing
  module StripeWebhook
    class PayoutFailed

      # Public: Handle the payout failed webhook payload
      #
      # webhook - The Billing::StripeWebhook to process
      #
      # Returns nothing
      def self.perform(*args)
        new(*args).perform
      end

      # Public: Initializes a new PayoutFailed webhook handler
      #
      # webhook - The Billing::StripeWebhook to process
      def initialize(webhook)
        @webhook = webhook
        @event = ::Stripe::Event.construct_from(webhook.payload)
        @payout = @event.data.object
      end

      # Public: Handle the payout created webhook payload
      #
      # Returns [PayoutFailed] instance
      def perform
        GitHub.dogstats.increment("stripe.payout_failed")
        GitHub.dogstats.count("stripe.payout_failed.amount_in_cents", payout.amount)
        instrument_payout

        transaction = Billing::PayoutsLedgerTransaction.new(stripe_connect_account)

        transaction.add_entry(
          transaction_type: :transfers_paid,
          amount_in_subunits: payout.amount,
          currency_code: payout.currency.upcase,
          primary_reference_id: payout.id,
        )
        transaction.add_entry(
          transaction_type: :payout_failure,
          amount_in_subunits: (-1 * payout.amount),
          currency_code: payout.currency.upcase,
          primary_reference_id: payout.id,
        )

        transaction.save!
      end

      private

      attr_reader :payout, :event

      def instrument_payout
        GlobalInstrumenter.instrument("payout.bank_payout", {
          sponsored_maintainer: stripe_connect_account.sponsored_maintainer,
          total_payout_amount_in_cents: payout.amount,
          destination_account_type: "stripe_connect",
          destination_account_id: event.account,
          status: "failed",
          funding_instrument_type: payout.type,
          payout_id: payout.id,
          failure_code: payout.failure_code,
        })
      end

      def stripe_connect_account
        Billing::StripeConnect::Account.find_by!(
          stripe_account_id: event.account,
        )
      end
    end
  end
end
