# frozen_string_literal: true

module GitHub::Billing
  module StripeWebhook
    class PayoutCreated
      attr_reader :event, :payout

      # Public: Handle the payout created webhook payload
      #
      # webhook - The Billing::StripeWebhook to process
      #
      # Returns nothing
      def self.perform(*args)
        new(*args).perform
      end

      # Public: Initializes a new PayoutCreated webhook handler
      #
      # webhook - The Billing::StripeWebhook to process
      def initialize(webhook)
        @webhook = webhook
        @event = ::Stripe::Event.construct_from(webhook.payload)
        @payout = @event.data.object
      end

      # Public: Handle the payout created webhook payload
      #
      # Returns nothing
      def perform
        GitHub.dogstats.increment("stripe.payout_created")
        GitHub.dogstats.count("stripe.payout_created.amount_in_cents", payout.amount)
        instrument_payout
        transaction = Billing::PayoutsLedgerTransaction.new(stripe_connect_account)

        transaction.add_entry(
          transaction_type: :transfers_paid,
          amount_in_subunits: (-1 * payout.amount),
          currency_code: payout.currency.upcase,
          primary_reference_id: payout.id,
        )
        transaction.add_entry(
          transaction_type: :payout,
          amount_in_subunits: payout.amount,
          currency_code: payout.currency.upcase,
          primary_reference_id: payout.id,
        )

        transaction.save!
      end

      private

      def instrument_payout
        GlobalInstrumenter.instrument("payout.bank_payout", {
          sponsored_maintainer: stripe_connect_account.sponsored_maintainer,
          total_payout_amount_in_cents: payout.amount,
          destination_account_type: "stripe_connect",
          destination_account_id: event.account,
          status: "created",
          funding_instrument_type: payout.type,
          payout_id: payout.id,
        })
      end

      def stripe_connect_account
        Billing::StripeConnect::Account.find_by!(stripe_account_id: event.account)
      end
    end
  end
end
