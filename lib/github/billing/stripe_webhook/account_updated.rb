# frozen_string_literal: true

module GitHub::Billing
  module StripeWebhook
    class AccountUpdated
      attr_reader :event, :webhook

      # Public: Handle the account updated webhook payload
      #
      # webhook - The Billing::StripeWebhook to process
      #
      # Returns nothing
      def self.perform(*args)
        new(*args).perform
      end

      # Public: Initializes a new AccountUPdated webhook handler
      #
      # webhook - The Billing::StripeWebhook to process
      def initialize(webhook)
        @webhook = webhook
        @event = ::Stripe::Event.construct_from(webhook.payload)
        @account = @event.data.object
      end

      # Public: Handle the account updated webhook payload
      #
      # Returns nothing
      def perform
        return unless stripe_connect_account
        stripe_connect_account.update!(stripe_account_details: account.to_hash)
      end

      private

      attr_accessor :account

      def stripe_connect_account
        return @stripe_connect_account if defined?(@stripe_connect_account)
        @stripe_connect_account = Billing::StripeConnect::Account.find_by(stripe_account_id: event.account)
      end
    end
  end
end
