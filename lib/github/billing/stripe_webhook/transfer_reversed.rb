# frozen_string_literal: true

module GitHub::Billing
  module StripeWebhook
    class TransferReversed
      attr_reader :transfer

      # Public: Handle the transfer reversed webhook payload
      #
      # webhook - The Billing::StripeWebhook to process
      #
      # Returns nothing
      def self.perform(*args)
        new(*args).perform
      end

      # Public: Initializes a new TransferReversed webhook handler
      #
      # webhook - The Billing::StripeWebhook to process
      def initialize(webhook)
        @webhook = webhook
        @event = ::Stripe::Event.construct_from(webhook.payload)
        @transfer = @event.data.object
      end

      # Public: Handle the transfer created webhook payload
      #
      # Returns nothing
      def perform
        reversals.each do |reversal|
          record_reversal(transfer, reversal)
        end
      end

      private

      def record_reversal(transfer, reversal)
        return if stripe_connect_account
          .ledger_entries
          .transfer_reversal
          .where(primary_reference_id: reversal.id)
          .exists?

        transaction = ::Billing::PayoutsLedgerTransaction.new(stripe_connect_account)

        transaction.add_entry(
          transaction_type: :transfer_reversal,
          amount_in_subunits: (-1 * reversal.amount),
          currency_code: reversal.currency.upcase,
          primary_reference_id: reversal.id,
          additional_reference_ids: transfer_reversal_additional_reference_ids(transfer, reversal),
        )

        if refunded?(reversal)
          transaction.add_entry(
            transaction_type: :refund,
            amount_in_subunits: reversal.metadata["payment_amount_reversed"].to_i,
            currency_code: reversal.currency.upcase,
            primary_reference_id: reversal.metadata["zuora_refund_id"],
            additional_reference_ids: {
              stripe_refund_id: reversal.metadata["stripe_refund_id"],
              refunded_transaction_stripe_id: transfer.metadata["stripe_charge_id"],
              refunded_transaction_zuora_id: transfer.transfer_group,
            },
          )
        end

        if match_reversed?(reversal)
          transaction.add_entry(
            transaction_type: :github_match_reversal,
            amount_in_subunits: reversal.metadata["match_amount_reversed"].to_i,
            currency_code: reversal.currency.upcase,
            primary_reference_id: reversal.metadata["zuora_refund_id"] || reversal.id,
          )
        end

        transaction.save!

        instrument_transfer_reversal(reversal)
        clear_match_limit_reached_at_if_under_match_limit!(reversal)
      end

      def instrument_transfer_reversal(reversal)
        return unless stripe_connect_account.payable.is_a?(SponsorsListing)
        user = Billing::BillingTransaction.find_by(transaction_id: reversal.metadata["stripe_refund_id"])&.live_user
        return unless user.present?

        sponsorship = Sponsorship.find_by(
          sponsor: user,
          sponsorable: stripe_connect_account.payable.sponsorable,
        )

        if sponsorship.present?
          GlobalInstrumenter.instrument("sponsors.sponsor_transfer_reversal", {
            sponsorship: sponsorship,
            listing: stripe_connect_account.payable,
            tier: sponsorship.subscription_item.subscribable,
            total_amount_in_subunits: reversal.amount.to_i,
            payment_amount_in_subunits: reversal.metadata["payment_amount_reversed"].to_i,
            match_amount_in_subunits: reversal.metadata["match_amount_reversed"].to_i,
            currency_code: reversal.currency.upcase,
          })
        end
      end

      def reversals
        transfer.reversals.data
      end

      def stripe_connect_account
        Billing::StripeConnect::Account.find_by!(
          stripe_account_id: transfer.destination,
        )
      end

      def transfer_reversal_additional_reference_ids(transfer, reversal)
        if refunded?(reversal)
          {
            reversed_transfer_stripe_id: transfer.id,
            stripe_refund_id: reversal.metadata["stripe_refund_id"],
            zuora_refund_id: reversal.metadata["zuora_refund_id"],
            refunded_transaction_stripe_id: transfer.metadata["stripe_charge_id"],
            refunded_transaction_zuora_id: transfer.transfer_group,
          }
        else
          {
            reversed_transfer_stripe_id: transfer.id,
            stripe_refund_id: reversal.metadata["stripe_refund_id"],
            zuora_refund_id: reversal.metadata["zuora_refund_id"],
          }
        end
      end

      def refunded?(reversal)
        reversal.metadata["payment_amount_reversed"].to_i.positive?
      end

      def match_reversed?(reversal)
        reversal.metadata["match_amount_reversed"].to_i.positive?
      end

      # Private: Clears the `match_limit_reached_at` timestamp for a listing if
      #          we reversed matching funds and put the listing back under the
      #          match limit.
      #
      # reversal - A Stripe::StripeObject that is a transfer reversal.
      #
      # Returns a Boolean indicating if the `match_limit_reached_at` column was cleared.
      def clear_match_limit_reached_at_if_under_match_limit!(reversal)
        return false unless match_reversed?(reversal)
        listing = stripe_connect_account.payable
        return false if listing.match_limit_reached_at.blank?

        listing.reset_total_match_in_cents!
        return false if listing.reached_match_limit?

        listing.update!(match_limit_reached_at: nil)
      end
    end
  end
end
