# frozen_string_literal: true

module GitHub::Billing
  module StripeWebhook
    class TransferFailed
      # Public: Handle the transfer failed webhook payload
      #
      # webhook - The Billing::StripeWebhook to process
      #
      # Returns nothing
      def self.perform(*args)
        new(*args).perform
      end

      # Public: Initializes a new TransferFailed webhook handler
      #
      # webhook - The Billing::StripeWebhook to process
      def initialize(webhook)
        @webhook = webhook
      end

      # Public: Handle the transfer failed webhook payload
      #
      # Returns nothing
      def perform
        GitHub.dogstats.increment("stripe.transfer_failed")
      end
    end
  end
end
