# rubocop:disable Style/FrozenStringLiteralComment

# Manages the daily Billing Run
module GitHub
  module Billing
    module Legacy
      module Run

        BATCH_SIZE = 1000

        # Kick off the daily billing run. This grabs a list of users IDs that need
        # to be billed from MySQL and then processes a charge for each.
        #
        # This method (and its containing job, BillingRunStartJob)
        # assume that no external API calls are made in `run_charges`.
        #
        # Returns nothing.
        def self.run_start
          User.needs_billed.find_each(batch_size: BATCH_SIZE) do |user|
            start_time = Time.now
            if should_dun?(user)
              dun_for_no_payment_method(user)
            elsif user.beneficiary?
              disable_beneficiary(user)
            else
              run_charges(user)
            end
            # track time it took for a batch to be processed
            elapsed = GitHub::Dogstats.duration(start_time)
            GitHub.dogstats.timing("billing_run_start.batch.run.time", elapsed)
          end
        end

        # Processes a charge for the user by enqueueing the necessary braintree
        # subscription jobs for users that need to be charged (not on a free plan,
        # haven't been billed already, etc.)
        #
        # user  - The User that needs to be charged
        #
        # Returns nothing.
        def self.run_charges(user)
          is_error = false
          is_timeout = false

          GitHub.dogstats.increment("billing.run.total")

          unless user.external_subscription?
            # Makes no external API calls since the user doesn't have an external
            # subscription:
            user.recurring_charge
            GitHub.dogstats.increment("billing.run.count")
          end
        rescue StandardError => error
          Failbot.report(error, user_id: user.id)
          GitHub.dogstats.increment("billing.run.timeout") if error.is_a? Timeout::Error
          GitHub.dogstats.increment("billing.run.error")
          is_timeout = true if error.is_a? Timeout::Error
          is_error = true
        ensure
          GitHub.dogstats.increment("billing.run", tags: ["error:#{is_error}", "timeout:#{is_timeout}"])
        end

        # Disables users that are benefiting from falling outside of the dunning cycle.
        #
        # user - The User that needs to be disabled.
        #
        def self.disable_beneficiary(user)
          message =
            if user.coupon_expired_since_last_billing?
              "Your coupon was recently expired with no payment method on file."
            else
              "No payment method on file."
            end

          GitHub.dogstats.increment("billing.run.beneficiaries")
          BillingNotificationsMailer.never_entered_dunning_failure(user, message).deliver_now
          user.disable!
        end

        # Should this user pay us but not be disabled yet (i.e. they haven't exhuasted
        # their billing attempts)?
        def self.should_dun?(user)
          !user.has_valid_payment_method? &&
            user.billing_attempts < User::BILLING_ATTEMPTS_LIMIT &&
            user.payment_amount > 0
        end

        # Duns users that don't have a payment method on file but an active
        # subscription
        #
        # These need to be dunned manually because removing payment sets their Zuora
        # account to `AutoPay: false`, which means Zuora will not attempt to bill
        # them or put them in dunning automatically
        #
        # user - The User that needs to be dunned.
        #
        def self.dun_for_no_payment_method(user)
          GitHub.dogstats.increment("billing.run.autopay_false")
          message = "No payment method on file."

          User.increment_counter(:billing_attempts, user.id)
          user.dun_subscription(message)
          user.update(billed_on: GitHub::Billing.today + 7.days)
        end
      end
    end
  end
end
