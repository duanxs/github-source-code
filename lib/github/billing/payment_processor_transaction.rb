# rubocop:disable Style/FrozenStringLiteralComment

module GitHub::Billing
  class PaymentProcessorTransaction < PaymentProcessorAdapter
    ATTRIBUTES = [
      :raw_transaction,
      :type, :date, :amount_in_cents,
      :transaction_id,

      :original_transaction_id,
      :success, :status, :status_history, :settlement_batch_id, :url,
      :customer_details, :postal_code, :region, :country, :country_code_alpha3,

      :payment_type,
      :bank_identification_number, :last_four, :country_of_issuance,
      :paypal_email,
      :custom_fields
    ]
    attr_accessor *ATTRIBUTES

    alias_method :id, :transaction_id

    def initialize(raw_transaction, attributes = nil)
      attributes ||= {}
      @raw_transaction = raw_transaction
      super attributes
    end

    def amount
      Billing::Money.new(amount_in_cents, "USD").to_s
    end

    def sale?
      @type == "sale"
    end

    def refund?
      @type == "refund"
    end

    def voided?
      @status == "voided"
    end

    def success?
      @success
    end

    def formatted_date
      @date.strftime("%Y-%m-%d")
    end

    def in_progress?
      @date > 10.minutes.ago
    end

    def credit_card_transaction
      "#{@bank_identification_number.first}***********#{@last_four}"
    end

    def paypal?
      @payment_type == :paypal
    end

    def credit_card?
      @payment_type == :credit_card
    end
  end
end
