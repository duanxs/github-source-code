# rubocop:disable Style/FrozenStringLiteralComment

module GitHub::Billing
  # Base class for all payment processor adapters to translate from PaymentMethod to
  # disperate payment processor APIs
  class PaymentProcessorAdapter
    def initialize(attributes = nil)
      attributes ||= {}
      attributes.each_key do |name|
        instance_variable_set :"@#{name}", attributes[name]
      end
    end
  end
end
