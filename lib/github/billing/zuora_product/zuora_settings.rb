# frozen_string_literal: true

module GitHub::Billing::ZuoraProduct::ZuoraSettings
  EFFECTIVE_END_DATE = "9999-12-12"
  RECOGNIZED_REVENUE_ACCOUNT = "1200 Accounts Receivable"
  TAX_CODE = "Dotcom - SW053000"
  DEFERRED_REVENUE_ACCOUNT = "4001 Sales : .Com Subscriptions"
  DISCOUNT_WSDL_VERSION = 89.0
end
