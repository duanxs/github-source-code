# frozen_string_literal: true

class GitHub::Billing::ZuoraPaypal
  class Error < StandardError; end

  def self.create_account(target_details:, paypal_nonce:, billing_address:, vat_code: nil)
    zuora_response = { success: true }

    begin
      account_response = GitHub.zuorest_client.create_object_account({
        AutoPay: false,
        BillCycleDay: target_details[:billed_on],
        BusinessSegment__c: "Self-Serve",
        Batch: "Batch10",
        CommunicationProfileId: GitHub.zuora_self_serve_communication_profile_id,
        SynctoNetSuite__NS: "No",
        PaymentTerm: "Due Upon Receipt",
        Currency: "USD",
        Name: target_details[:external_name],
        Status: "Draft",
        TaxExemptStatus: "Yes",
        TaxExemptCertificateID: "N/A",
      })

      zuora_response[:account_id] = account_response["Id"]

      braintree_response = Braintree::Customer.create(
        id: zuora_response[:account_id],
        email: target_details[:external_email],
        first_name: target_details[:external_name],
        custom_fields: target_details.fetch(:bt_fields, {}),
        payment_method_nonce: paypal_nonce,
      )
      return { success: false } unless braintree_response.success?

      paypal_account = braintree_response.customer.paypal_accounts.first
      zuora_response[:paypal_account] = paypal_account

      response = create_payment_method(
        account_id: zuora_response[:account_id],
        paypal_email: paypal_account.email,
        billing_agreement_id: paypal_account.billing_agreement_id,
      )

      contact_response = GitHub.zuorest_client.create_contact({
        AccountId: zuora_response[:account_id],
        FirstName: target_details[:external_name],
        LastName: target_details[:external_name],
        PostalCode: billing_address[:postal_code],
        Country: billing_address[:country_code_alpha3],
        State: zuora_region(billing_address[:region]),
      })

      zuora_response[:contact_id] = contact_response["Id"]
      return response unless response[:success]

      zuora_response.merge!(response)

      GitHub.zuorest_client.update_account(zuora_response[:account_id], {
        AutoPay: true,
        BillToId: zuora_response[:contact_id],
        SoldToId: zuora_response[:contact_id],
        DefaultPaymentMethodId: zuora_response[:payment_method_id],
        Status: "Active",
        VATId: vat_code,
      })

      account_info = GitHub.zuorest_client.get_account(zuora_response[:account_id])
      zuora_response[:account_number] = account_info["AccountNumber"]
    rescue Zuorest::HttpError => e
      Failbot.report!(e, message: e.data)
      zuora_response[:success] = false
    end

    zuora_response
  end

  def self.create_payment_method(account_id:, paypal_nonce: nil, paypal_email: nil, billing_agreement_id: nil)
    zuora_response = { success: true }

    begin
      if paypal_nonce.present?
        response = Braintree::PaymentMethod.create!(
          customer_id: account_id,
          payment_method_nonce: paypal_nonce,
          options: { make_default: true },
        )

        paypal_email = response.email
        billing_agreement_id = response.billing_agreement_id
      end

      GitHub.zuorest_client.update_action({
        "objects": [{
          AutoPay: false,
          PaymentGateway: "Paypal",
          fieldsToNull: ["DefaultPaymentMethodId"],
          Id: account_id,
        }],
        "type": "Account",
      })

      payment_method_response = GitHub.zuorest_client.create_payment_method({
        AccountId: account_id,
        PaypalType: "ExpressCheckout",
        PaypalEmail: paypal_email,
        PaypalBaid: billing_agreement_id,
        Type: "PayPal",
      })
      zuora_response[:payment_method_id] = payment_method_response["Id"]
    rescue Zuorest::HttpError => e
      Failbot.report(e, account_id: account_id, message: e.data)
      zuora_response[:success] = false
    rescue Braintree::BraintreeError  => e
      if e.respond_to?(:error_result) && e.error_result.errors.any? { |error| error.code == "82905" }
        raise GitHub::Billing::ZuoraPaypal::Error.new \
          "Braintree Customer with Zuora ID required to add a PayPal payment method for #{account_id}"
      end

      Failbot.report(e, account_id: account_id, message: e.to_s)
      zuora_response[:success] = false
    end

    zuora_response
  end

  # Internal: Return the value that Zuora expects for Region
  #
  # For some places like the District of Columbia, Zuora expects a specific value
  # that is different from what we use elsewhere. In that case, we'll map to the
  # Zuora value.
  #
  # Returns String
  def self.zuora_region(region)
    case region
    when "District of Columbia" then "DC"
    else region
    end
  end
end
