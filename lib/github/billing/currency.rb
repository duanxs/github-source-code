# rubocop:disable Style/FrozenStringLiteralComment

require "csv"

module GitHub::Billing::Currency
  # Public: Initialize exchange rates
  #
  # Returns true when finished, false if already set up
  def self.setup
    if !Money.default_bank.is_a?(Money::Bank::OpenExchangeRatesBank) && GitHub.billing_enabled?
      Money.default_bank = open_exchange_rates_bank
      update_rates
      country_to_currency
      true
    else
      false
    end
  end

  def self.open_exchange_rates_bank
    Money::Bank::OpenExchangeRatesBank.new.tap do |bank|
      bank.app_id = GitHub.open_exchange_rates_app_id

      # setup caching to use GitHub.cache
      bank.cache = proc do |v|
        if v
          GitHub.cache.set(key, v, 1.day)
        else
          GitHub.cache.get(key)
        end
      end
    end
  end

  # Public: Get the currency code for a country
  #
  # country_code - Two character country code
  #
  # Returns a String of the three character currency code
  def self.currency_of(country_code)
    # get get currency for country
    currency = country_to_currency[country_code] || "USD"

    # ensure exchange rate exists
    begin
      currency = default_bank.get_rate("USD", currency) ? currency : "USD"
    rescue Money::Currency::UnknownCurrency
      currency = "USD"
    end

    currency
  end

  # Internal: Return the default exchange bank for Money. Sets up the bank on demand.
  def self.default_bank
    setup unless Money.default_bank.is_a? Money::Bank::OpenExchangeRatesBank
    Money.default_bank
  end

  class << self

  end

  # Internal: Country to currency mapping
  #
  # Returns a Hash
  def self.country_to_currency
    @country_to_currency ||= load_currencies
  end

  # Internal: Load the country data file
  #
  # Returns an Array
  def self.load_currencies
    # Orginally from https://github.com/datasets/country-codes/blob/master/data/country-codes.csv
    # and edited. Not updated since Dec 2013 since ISO no longer provides free data files.
    currency_mapping_csv = CSV.read(File.join(Rails.root, "config/country-codes.csv"))
    currency_mapping_csv.inject({}) do |memo, row|
      # column 2 - ISO3166-1-Alpha-2
      # column -6 - currency_alphabetic_code
      memo[row.fetch(2)] = row.fetch(-6)
      memo
    end
  end

  # Internal: Exchange rate cache key
  #
  # Returns a String
  def self.key
    "currency:exchange_rates"
  end

  # Internal: Whether the exchange rates have been cached
  #
  # Returns a Boolean
  def self.cached?
    !!GitHub.cache.get(key)
  end

  # Public: Updates the rates from either the cache or from the exchange rate service
  #
  # Returns Nothing
  def self.update_rates
    default_bank.save_rates unless cached?
    default_bank.update_rates
  rescue Money::Bank::NoAppId, SocketError, Net::OpenTimeout
    # always be able to exchange USD -> USD
    default_bank.set_rate("USD", "USD", 1)
  end
end
