# rubocop:disable Style/FrozenStringLiteralComment

module GitHub::Billing
  class PaymentProcessors::PaymentProcessor
    # Public: String customer_id in the payment processor's system.
    attr_reader :customer_id

    # Public: Initialize a new payment processor.
    #
    # customer_id - A external id in the payment processor's system (usually
    # representing a customer).
    def initialize(customer_id, gateway: nil)
      @customer_id = customer_id
      @gateway = gateway
    end

    # Public: String type of this processor
    def type
      self.class.name.split("::").last.downcase
    end

    private

    attr_reader :gateway
  end
end
