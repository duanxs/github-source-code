# frozen_string_literal: true

module GitHub::Billing
  class PaymentProcessors::ZuoraProcessor < PaymentProcessors::PaymentProcessor
    SLUG = "zuora"

    def initialize(customer_id, gateway:)
      super
    end

    def update_payment_details(payment_details)
      return GitHub::Billing::Result.failure("No customer record") if customer_id.blank?
      unless payment_details.zuora_payment_details?
        return GitHub::Billing::Result.failure("No payment details present")
      end

      response = update_customer_contact(payment_details: payment_details)
      unless response[:success]
        return GitHub::Billing::Result.failure("Unable to update billing address details")
      end

      update_customer_params = {}
      if payment_details.zuora_hosted_payments_page_details?
        payment_method_id = payment_details.zuora_payment_method_id
      else
        response = create_paypal_payment_method(payment_details)

        return GitHub::Billing::Result.failure("Unable to update payment method") unless response[:success]
        payment_method_id = response[:payment_method_id]
      end

      zuora_payment_method = ::Billing::Zuora::PaymentMethod.find(payment_method_id)
      unless zuora_payment_method
        return GitHub::Billing::Result.failure("Payment method not found")
      end

      gateway.type = zuora_payment_method.credit_card? ? :credit_card : :paypal
      update_customer_params.merge!(
        AutoPay: true,
        DefaultPaymentMethodId: zuora_payment_method.id,
        PaymentGateway: gateway.name,
      )
      response = update_customer_account(update_customer_params)
      result = GitHub::Billing::Result.from_zuora({ success: response["Success"] })
      if result.success?
        record = PaymentProcessorRecord.new(customer_id: customer_id)
        record.token = zuora_payment_method.id
        if zuora_payment_method.credit_card?
          record.masked_number = zuora_payment_method.masked_number
          record.expiration_month = zuora_payment_method.expiration_month
          record.expiration_year = zuora_payment_method.expiration_year
          record.card_type = zuora_payment_method.card_type
          record.postal_code = zuora_payment_method.postal_code
          record.region = zuora_payment_method.state
          record.country_code_alpha3 = payment_details.country_code_alpha3
          record.unique_number_identifier = zuora_payment_method.fingerprint
        elsif zuora_payment_method.paypal?
          record.postal_code = payment_details.postal_code
          record.region = payment_details.region
          record.country_code_alpha3 = payment_details.country_code_alpha3
          record.paypal_email = zuora_payment_method.paypal_email
        end

        yield record if block_given?

        result.record = record
      end

      result
    end

    def clear_payment_details
      return false if customer_id.blank?
      return false unless set_default_payment_method_to_null

      payment_methods = GitHub.dogstats.time("zuora.timing.payment_methods_find") do
        Zuorest::Model::PaymentMethod.find_by_account_id(customer_id)
      end

      payment_methods.each do |payment_method|
        GitHub.dogstats.time("zuora.timing.payment_method_delete") do
          payment_method.delete
        end
      end

      true
    rescue Zuorest::HttpError => e
      Failbot.report!(e)
      false
    end

    private

    def create_paypal_payment_method(payment_details)
      GitHub::Billing::ZuoraPaypal.create_payment_method(
        account_id: customer_id,
        paypal_nonce: payment_details.paypal_nonce,
      )
    rescue GitHub::Billing::ZuoraPaypal::Error => e
      braintree_response = Braintree::Customer.create(
        id: customer_id,
        email: payment_details.email,
        first_name: payment_details.account_name,
      )
      if braintree_response.success?
        retry
      end

      Failbot.report(e, customer_id: customer_id, message: "#{braintree_response.message} - #{braintree_response.errors.inspect}")

      { success: false }
    end

    def update_customer_contact(payment_details:)
      account_response = GitHub.zuorest_client.get_account(customer_id)
      contact_params = {
        AccountId: account_response["Id"],
        Country: payment_details.country_code_alpha3,
        PostalCode: payment_details.postal_code,
        State: payment_details.region,
      }
      responses = GitHub.dogstats.time("zuora.timing.action_contact_update") do
        GitHub.zuorest_client.update_action({
          objects: [
            { Id: account_response["SoldToId"] }.merge!(contact_params),
            { Id: account_response["BillToId"] }.merge!(contact_params),
          ],
          type: "Contact",
        })
      end

      if responses.all? { |response| response["Success"] }
        { success: true }
      else
        errors = responses.select { |response| !response["Success"] }.flat_map { |response| response["Errors"] }
        { success: false, errors: errors }
      end
    rescue Zuorest::HttpError => e
      Failbot.report!(e)
      { success: false }
    end

    def update_customer_account(attributes)
      # This is from:
      # https://community.zuora.com/t5/API/How-to-set-a-field-to-NULL-via-REST-API/m-p/15290/highlight/true#M692
      # it's certainly not very RESTy
      GitHub.dogstats.time("zuora.timing.action_account_update") do
        GitHub.zuorest_client.update_action({
          "objects": [
            {
              "Id": customer_id,
            }.merge!(attributes),
          ],
          "type": "Account",
        }).first
      end
    end

    def set_default_payment_method_to_null
      response = update_customer_account({"AutoPay": false, "fieldsToNull": ["DefaultPaymentMethodId"]})
      response["Success"]
    end
  end
end
