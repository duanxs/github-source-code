# frozen_string_literal: true

module GitHub
  module OrgInsights
    class Metric::PullRequestsClosed < GitHub::OrgInsights::Metric
      self.model = Issue
      self.column = :closed_at
      self.metric_name = GitHub::OrgInsights::Metric::PULL_REQUEST_CLOSED

      def all
        ActiveRecord::Base.connected_to(role: :reading_slow) do
          relation.joins(:pull_request)
            .for_owner(@org_id)
            .where("pull_requests.merged_at IS NULL")
            .group("repository_id")
            .bucketed_by(fully_qualified_column, timespan)
            .count
        end
      end
    end
  end
end
