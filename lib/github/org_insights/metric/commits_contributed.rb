# frozen_string_literal: true

module GitHub
  module OrgInsights
    class Metric::CommitsContributed < GitHub::OrgInsights::Metric
      self.model = CommitContribution
      self.column = :committed_date
      self.metric_name = GitHub::OrgInsights::Metric::COMMIT_CREATED

      def all
        repo_ids = ::Repository.where(owner_id: @org_id).select(:id).to_a
        query = relation.where(repository_id: repo_ids)

        query
          .group("repository_id")
          .bucketed_by(fully_qualified_column, timespan)
          .sum(:commit_count)
      end
    end
  end
end
