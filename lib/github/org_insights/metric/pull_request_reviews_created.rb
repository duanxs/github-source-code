# frozen_string_literal: true

module GitHub
  module OrgInsights
    class Metric::PullRequestReviewsCreated < GitHub::OrgInsights::Metric
      self.model = PullRequestReview
      self.metric_name = GitHub::OrgInsights::Metric::PULL_REQUEST_REVIEWED

      def all
        ActiveRecord::Base.connected_to(role: :reading_slow) do
          relation
            .for_owner(@org_id, pull_request: :repository)
            .group("repository_id")
            .bucketed_by(fully_qualified_column, timespan)
            .count
        end
      end
    end
  end
end
