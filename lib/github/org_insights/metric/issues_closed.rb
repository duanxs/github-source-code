# frozen_string_literal: true

module GitHub
  module OrgInsights
    class Metric::IssuesClosed < GitHub::OrgInsights::Metric
      self.model = Issue
      self.column = :closed_at
      self.metric_name = GitHub::OrgInsights::Metric::ISSUE_CLOSED

      def all
        ActiveRecord::Base.connected_to(role: :reading_slow) do
          relation.where(pull_request_id: nil)
            .for_owner(@org_id)
            .group("repository_id")
            .bucketed_by(fully_qualified_column, timespan)
            .count
        end
      end
    end
  end
end
