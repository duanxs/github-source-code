# frozen_string_literal: true

module GitHub
  module OrgInsights
    class Backfill
      attr_reader :org_id, :metric_class

      HYDRO_BUFFER_OVERFLOW_RETRIES = 5
      HYDRO_BUFFER_OVERFLOW_SLEEP   = 2

      def self.for_org(org_id:, metrics: [])
        possible_metrics = GitHub::OrgInsights::Metric.subclasses

        metrics = Array.wrap(metrics).select { |m| possible_metrics.include?(m) }
        if metrics.blank?
          metrics = possible_metrics
        end

        metrics.each do |metric_class|
          ::OrgInsightsBackfillJob.perform_later(org_id, metric_class)
        end
      end

      def initialize(org_id, metric_class)
        @org_id = org_id
        @metric_class = metric_class
      end

      def backfill
        send_data(backfill_data)
      end

      def backfill_data
        GitHub.dogstats.time "org_insights.backfill.#{metric_class.tracking_name}" do
          metric_class.new(org_id).as_json
        end
      end

      private

      def send_data(data)
        return unless GitHub.hydro_enabled?

        data.each do |msg|
          publish_data(*msg)
        end
      end

      def publish_data(msg, options)
        tries = 0
        while tries < HYDRO_BUFFER_OVERFLOW_RETRIES
          result = GitHub.hydro_publisher.publish(msg, **options)

          if result.success?
            GitHub.dogstats.increment("org_insights.backfill.hydro_publish.message_sent")
            break
          end
          break unless result.error.kind_of?(Hydro::Sink::BufferOverflow)

          GitHub.dogstats.increment("org_insights.backfill.hydro_publish.buffer_overflow_wait")

          sleep HYDRO_BUFFER_OVERFLOW_SLEEP

          tries += 1
        end

        if !result.success?
          case result.error
          when Hydro::Sink::MessagesTooLarge
            GitHub.dogstats.increment("org_insights.backfill.hydro_publish.too_large", tags: [
              "class:#{metric_class.tracking_name}",
            ])
          when Hydro::Sink::BufferOverflow
            GitHub.dogstats.increment("org_insights.backfill.hydro_publish.buffer_overflow")
            report_error(result.error, schema: options[:schema])
          else
            GitHub.dogstats.increment("org_insights.backfill.hydro_publish.error", tags: [
              "class:#{metric_class.tracking_name}",
            ])
            report_error(result.error, schema: options[:schema])
          end
        end
        result
      end

      def report_error(error, schema:)
        GitHub.dogstats.increment("hydro_client.publish_error", tags: ["schema:#{schema}"])

        Failbot.report(error, {
          app: "github-hydro",
          areas_of_responsibility: [:analytics],
        })
      end

    end
  end
end
