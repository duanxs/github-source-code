# frozen_string_literal: true

require "active_support/core_ext/numeric/time"
require "active_support/core_ext/time/zones"
require "active_support/core_ext/time/calculations"


module GitHub
  module OrgInsights
    class InsightsData

      METRIC_DISPLAY_NAMES = {
        "CommitCreated" => "Commits",
        "IssueClosed" => "Issues Closed",
        "IssueCreated" => "Issues",
        "PullRequestReviewed" => "Code Review",
        "PullRequestCreated" => "Pull Requests",
        "PullRequestMerged" => "Pull Requests Merged",
        "PullRequestClosed" => "Pull Requests Closed",
      }.freeze

      METRIC_DETAILS_PARAMS = {
        "IssueClosed" => {
          query: %w[is:issue is:closed],
          path_type: "issues",
          date_filter: "closed",
        },
        "IssueCreated" => {
          query: %w[is:issue],
          path_type: "issues",
          date_filter: "created",
        },
        "PullRequestCreated" => {
          query: %w[is:pr],
          path_type: "pulls",
          date_filter: "created",
        },
        "PullRequestMerged" => {
          query: %w[is:pr is:merged],
          path_type: "pulls",
          date_filter: "merged",
        },
        "PullRequestClosed" => {
          query: %w[is:pr is:closed is:unmerged],
          path_type: "pulls",
          date_filter: "closed",
        },
      }.freeze

      # These durations are inaccurate because the month and year charts on the insights page are supposed to cover exactly 30 and 365 days
      TIME_PERIODS = {
        "week" => {name: "week", duration: 7.days, summary: "DAY"},
        "month" => {name: "month", duration: 30.days, summary: "DAY"},
        "year" => {name: "year", duration: 365.days, summary: "WEEK"},
      }.freeze

      def initialize(eventer_client, id, owner_type: "USER")
        @eventer_client = eventer_client
        @id = id
        @owner_type = owner_type
      end

      def index_percentages(events, time_period: "week", report_time: Time.now)
        raise ArgumentError, "invalid events" unless events.respond_to?(:all?) && events.all? { |event| METRIC_DISPLAY_NAMES[event].present? }
        time_period = TIME_PERIODS[time_period]
        raise ArgumentError, "invalid time_period" if time_period.nil?
        raise ArgumentError, "invalid report_time" unless report_time.is_a?(Time)

        counters = query_counters(events, report_time_range(time_period, report_time), time_period[:summary])
        totals = events.map do |event|
          event_counters = counters[event.to_s] || {}
          total = event_counters.values.sum || 0
          [METRIC_DISPLAY_NAMES[event], total]
        end.to_h
        round_keep_total(percent_of_total(totals))
      end

      # collate data or repositories within an org
      def collate_data(events, repos:, user_zone:, time_period: "week", report_time: Time.now)
        raise ArgumentError, "invalid user_zone" unless user_zone.respond_to?(:local_to_utc)
        raise ArgumentError, "invalid events" unless events.respond_to?(:all?) && events.all? { |event| METRIC_DISPLAY_NAMES[event].present? }
        time_period = TIME_PERIODS[time_period]
        raise ArgumentError, "invalid time_period" if time_period.nil?
        raise ArgumentError, "invalid report_time" unless report_time.is_a?(Time)
        raise ArgumentError, "empty repos" if (repos.nil? || repos.empty?)

        report_range = report_time_range(time_period, report_time)
        prev_range = report_time_range(time_period, report_time, previous_range: true)
        collated_data = {}

        repos.each do |repo|
          repo_id = repo.id
          data = @eventer_client.counters(
            owner_type: "REPOSITORY",
            owner_id: repo_id,
            bucket_size: "DAY",
            start_time: report_range.first,
            end_time: report_range.last,
            events: events,
            summary: time_period[:summary],
          )
          prev_data = @eventer_client.counters(
            owner_type: "REPOSITORY",
            owner_id: repo_id,
            bucket_size: "DAY",
            start_time: prev_range.first,
            end_time: prev_range.last,
            events: events,
            summary: time_period[:summary],
          )

          if collated_data.empty?
            collated_data[:data] = data
            collated_data[:prev_data] = prev_data
          else
            # perform a deep merge to sum the metrics that we get back for the repo as we build metrics for each repo
            collated_data[:data] = collated_data[:data].deep_merge(data) { |key, old_v, new_v| old_v + new_v }
            collated_data[:prev_data] = collated_data[:prev_data].deep_merge(prev_data) { |key, old_v, new_v| old_v + new_v }
          end
        end

        build_response(events, collated_data[:data], collated_data[:prev_data], time_period, report_range, user_zone)
      end

      def chart_data(events, user_zone:, time_period: "week", report_time: Time.now)
        raise ArgumentError, "invalid user_zone" unless user_zone.respond_to?(:local_to_utc)
        raise ArgumentError, "invalid events" unless events.respond_to?(:all?) && events.all? { |event| METRIC_DISPLAY_NAMES[event].present? }
        time_period = TIME_PERIODS[time_period]
        raise ArgumentError, "invalid time_period" if time_period.nil?
        raise ArgumentError, "invalid report_time" unless report_time.is_a?(Time)

        report_range = report_time_range(time_period, report_time)
        prev_range = report_time_range(time_period, report_time, previous_range: true)

        counters = query_counters(events, report_range, time_period[:summary])
        prev_counters = query_counters(events, prev_range, time_period[:summary])

        build_response(events, counters, prev_counters, time_period, report_range, user_zone)
      end

      # build the chart data response after sorting and calculating percentage changes
      def build_response(events, counters, prev_counters, time_period, report_range, user_zone)
        metrics = events.map do |event|
          event_counters = (counters[event] || {}).sort_by { |k, v| k }.to_h
          event_prev_counters = prev_counters[event] || {}
          max = event_counters.values.max || 0
          total = event_counters.values.sum || 0
          prev_total = event_prev_counters.values.sum || 0
          total_change = total - prev_total
          display_name = METRIC_DISPLAY_NAMES[event]

          {
            display_name: display_name,
            max: max,
            total: total,
            total_change: total_change,
            percent_change: (total_change / [prev_total, 1].max.to_f * 100).to_i,
            counts: event_counters.map do |k, v|
              {
                date: adjust_timestamp(k, user_zone),
                count: v,
              }
            end,
          }
        end

        timespan = {
          period: time_period[:name],
          beginning: adjust_timestamp(report_range.first, user_zone),
          ending: adjust_timestamp(report_range.last, user_zone),
        }

        {
          metrics: metrics,
          timespan: timespan,
        }
      end

      def report_time_range(time_period, report_time, previous_range: false)
        start_time = (report_time - time_period[:duration]).beginning_of_day
        prev_start_time = start_time - time_period[:duration]
        previous_range ? (prev_start_time...start_time) : (start_time...report_time)
      end

      # takes a hash with integer values and returns a hash with the percent of the total that each one represents
      # returns all 0 values if the total is 0 or if there are any negative values
      def percent_of_total(vals)
        total = vals.values.sum
        if total == 0 || vals.values.any? { |v| v < 0 }
          return vals.transform_values { 0 }
        end
        vals.transform_values { |v| 100 * v / total.to_f }
      end

      # takes a hash with float values and rounds them to integers such that their total remains the same.
      # returns the initial input unchanged if they don't total to a whole number
      def round_keep_total(vals)
        # this won't work unless the values add to a whole number
        return vals unless (vals.values.sum % 1) == 0.0 && vals.values.sum >= 0
        # sort values by the highest decimal first, then by lowest total value
        res = vals.sort_by { |_k, v| [1 - (v % 1), v] }
        # truncate values to integers
        res.map { |v| v[1] = v[1].to_i; v }
        # figure out how much we need to add to get back to the original total
        remainder = vals.values.sum.to_i - res.map { |v| v[1] }.sum
        # create a hash with the first values rounded up
        uprounded_values = res.take(remainder).to_h.transform_values { |v| v + 1 }
        res.to_h.merge(uprounded_values)
      end

      private

      def adjust_timestamp(timestamp, zone)
        zone.local_to_utc(timestamp).to_i * 1000
      end

      def query_counters(events, time_range, summary)
        @eventer_client.counters(
          owner_type: @owner_type,
          owner_id: @id,
          bucket_size: "DAY",
          start_time: time_range.first,
          end_time: time_range.last,
          events: events,
          summary: summary,
        )

      rescue NoMethodError => e
        Failbot.report e
        {}
      end
    end
  end
end
