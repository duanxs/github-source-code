# frozen_string_literal: true

# Listens for SQL statements over a configured size and reports them to failbot.
#
# Very large SQL text can cause the MySQL server to disconnect while receiving
# it, so we should try to keep the size managable.  If your code is flagged by
# this, consider batching or other alternatives to passing very large lists of
# ids in a single query.
class GitHub::LargeQuerySubscriber
  HAYSTACK_BUCKET = "github-large-queries"
  INSTRUMENTATION_SAMPLE_RATE = 0.0001
  INSTRUMENTATION_TIMING_METRIC = "query_subscriber.large_sql.instrument_time"
  QUERY_TIMING_METRIC = "query_subscriber.large_sql.execution_time"
  COUNT_METRIC = "query_subscriber.large_sql.count"

  def initialize(threshold:, truncation_size: 1000)
    @threshold = threshold
    @truncation_size = truncation_size
    @truncation_midpoint = truncation_size / 2
  end

  def call(event, start, ending, notifier_id, payload)
    GitHub.dogstats.time(INSTRUMENTATION_TIMING_METRIC, sample_rate: INSTRUMENTATION_SAMPLE_RATE) do
      sql = payload[:sql].to_s
      if sql.bytesize > @threshold
        notify_failbot(sql)
        notify_statsd(payload, TimeSpan.new(start, ending).duration)
      end
    end
  end

  def notify_failbot(sql)
    error = TooLarge.new("SQL has excessive size. Too many ids listed?")
    stack = caller
    frame = Rollup.first_significant_frame(stack)
    error.set_backtrace(stack)

    common_sql = GitHub::SQL::Digester.digest_sql(sql)
    truncated_sql = common_sql.truncate(@truncation_size, omission: "...#{common_sql.last(@truncation_midpoint)}") # elide middle
    rollup = Digest::MD5.hexdigest("#{frame}#{common_sql}")

    Failbot.report!(error, app: HAYSTACK_BUCKET, sql_size: sql.bytesize, sql_truncated: truncated_sql, rollup: rollup)
  end

  def notify_statsd(payload, query_duration)
    tags = ["db_host:#{payload[:connected_host]}", "error:#{!!payload[:exception]}"]
    GitHub.dogstats.batch do |stats|
      stats.timing(QUERY_TIMING_METRIC, query_duration, tags: tags)
      stats.increment(COUNT_METRIC, tags: tags)
    end
  end

  class TooLarge < StandardError; end
end
