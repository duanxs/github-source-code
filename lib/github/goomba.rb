# rubocop:disable Style/FrozenStringLiteralComment

require "goomba"
require "github/goomba/experiment"
require "github/goomba/filter"
require "github/goomba/filter_when_language"
require "github/goomba/input_filter"
require "github/goomba/node_filter"
require "github/goomba/text_filter"
require "github/goomba/output_filter"
require "github/goomba/async_output_filter"
require "github/goomba/sanitizer"
require "github/goomba/warp_pipe"

require "action_view"
require "action_controller"

require "github/goomba/autolink_filter"
require "github/goomba/suggested_change_filter"
require "github/goomba/camo_filter"
require "github/goomba/issue_mention_filter"
require "github/goomba/close_keyword_filter"
require "github/goomba/colon_emoji_filter"
require "github/goomba/color_filter"
require "github/goomba/commit_mention_filter"
require "github/goomba/custom_key_link_filter"
require "github/goomba/duplicate_keyword_filter"
require "github/goomba/first_paragraph_filter"
require "github/goomba/github_reference_filter"
require "github/goomba/https_filter"
require "github/goomba/image_max_width_filter"
require "github/goomba/markdown_element_class_filter"
require "github/goomba/markdown_filter"
require "github/goomba/markup_filter"
require "github/goomba/math_filter"
require "github/goomba/mention_filter"
require "github/goomba/platform_raw_image_filter"
require "github/goomba/nokogiri_compatibility_filter"
require "github/goomba/no_referrer_filter"
require "github/goomba/orphan_href_filter"
require "github/goomba/plain_text_input_filter"
require "github/goomba/raw_image_filter"
require "github/goomba/relative_link_filter"
require "github/goomba/set_table_role_filter"
require "github/goomba/strip_image_filter"
require "github/goomba/syntax_highlight_filter"
require "github/goomba/table_of_contents_filter"
require "github/goomba/task_list_filter"
require "github/goomba/team_mention_filter"
require "github/goomba/unicode_emoji_filter"
require "github/goomba/utf8_filter"
require "github/goomba/yaml_filter"
require "github/goomba/issue_blob_filter"
require "github/goomba/rel_nofollow_filter"
require "github/goomba/url_unfurl_filter"
require "github/goomba/h1_filter"
require "github/goomba/downcase_name_filter"
require "github/goomba/advisory_mention_filter"
require "github/goomba/code_scanning_message_link_filter"

module GitHub
  module Goomba
    NAME_PREFIX = "user-content-"

    context = {
      asset_root: "#{GitHub.asset_host_url}/images/icons",
      render_url: GitHub.render_host_url,
      base_url: GitHub.url,
      http_url: "http://#{GitHub.host_name}", # for the HttpsFilter
      name_prefix: NAME_PREFIX,
    }
    context[:flags] = Rinku::AUTOLINK_SHORT_DOMAINS if GitHub.enterprise?

    markup_sanitizer = Sanitizer.from_whitelist(GitHub::HTML::MARKUP_WHITELIST)
    markdown_sanitizer = Sanitizer.from_whitelist(GitHub::HTML::WHITELIST)

    [markup_sanitizer, markdown_sanitizer].each do |s|
      s.require_any_attributes(:a, "href", "id", "name")
      s.name_prefix = NAME_PREFIX
      s.limit_nesting :sup, 2
      s.limit_nesting :sub, 2
      s.limit_nesting :ul, 10
      s.limit_nesting :ol, 10
    end

    # A pipeline used to process git blob objects via the markup filter,
    # sanitization, and image hijacking. No mentions or related features are
    # used.
    #
    # Used for readmes and blob previews.
    MarkupPipeline = WarpPipe.new(
      input_filters: [
        UTF8Filter,
        YamlFilter,
        MarkupFilter,
        AutolinkFilter,
      ],
      sanitizer: markup_sanitizer,
      node_filters: [
        FilterWhenLanguage.new("Markdown", TaskListFilter),
        FilterWhenLanguage.new("Markdown", MathFilter),
        DowncaseNameFilter,
        TocFilter, # Must be before TableOfContentsFilter
        TableOfContentsFilter,
        CamoFilter,
        SyntaxHighlightFilter,
        RelativeLinkFilter,
        ColonEmojiFilter, # Must be after RelativeLinkFilter
        UnicodeEmojiFilter, # Must be after RelativeLinkFilter
        RawImageFilter, # Must be after RelativeLinkFilter
        ImageMaxWidthFilter, # Must be after RawImageFilter
        OrphanHrefFilter,
        RelNofollowFilter,
      ],
      default_context: context,
      stats_key: "MarkupPipeline",
    )

    # Pipeline used for most types of user provided content like comments
    # and issue bodies. Performs sanitization, image hijacking, and various
    # mention links.
    MarkdownPipeline = WarpPipe.new(
      input_filters: [
        UTF8Filter,
        MarkdownFilter,
      ],
      sanitizer: markdown_sanitizer,
      node_filters: [
        DowncaseNameFilter,
        TaskListFilter,

        # CloseKeywordFilter must go before other text filters but after TaskListFilter. It goes
        # early in the list because it needs access to sibling elements, but it must go after
        # TaskListFilter because that one needs ancestry access which is lost if the
        # CloseKeywordFilter finds any matching keywords.
        CloseKeywordFilter,
        CamoFilter,
        ImageMaxWidthFilter,
        (GitHub.ssl? ? HttpsFilter : nil),
        SuggestedChangeFilter,
        MentionFilter,
        TeamMentionFilter,
        IssueBlobFilter,
        CustomKeyLinkFilter,
        IssueMentionFilter,
        CommitMentionFilter,
        SetTableRoleFilter,
        AdvisoryMentionFilter,
        CVEMentionFilter,
        DuplicateKeywordFilter,
        ColonEmojiFilter,
        UnicodeEmojiFilter,
        MathFilter,
        SyntaxHighlightFilter,
        MarkdownElementClassFilter,
        ColorFilter,
        RelNofollowFilter,
        UrlUnfurlFilter,
      ].compact,
      output_filters: [
        GithubReferenceFilter,
      ],
      default_context: context.merge(gfm: true),
      result_class: GitHub::HTML::Result,
      stats_key: "MarkdownPipeline",
    )

    # Pipeline used to format messages associated with code scanning alerts
    # In particular, placeholder links of the form [foo](<index>) are replaced
    # with real links to the related location referred to by the <index>
    #
    CodeScanningMessagePipeline = WarpPipe.new(
      input_filters: [
        UTF8Filter,
        MarkdownFilter,
      ],
      sanitizer: Sanitizer.from_whitelist(
        elements: ["a", "br"],
        attributes: { "a" => ["href"] },
      ),
      node_filters: [
        (GitHub.ssl? ? HttpsFilter : nil),
        RelNofollowFilter,
        CodeScanningMessageLinkFilter,
      ].compact,
      default_context: context.merge(gfm: true),
      result_class: GitHub::HTML::Result,
      stats_key: "CodeScanningMessagePipeline",
    )

    # Markdown pipeline that doesn't create any new links or modify existing
    # ones.
    NonLinkingMarkdownPipeline = WarpPipe.new(
      input_filters: [
        UTF8Filter,
        MarkdownFilter,
      ],
      sanitizer: markdown_sanitizer,
      node_filters: [
        TaskListFilter,
        ImageMaxWidthFilter,
        ColonEmojiFilter,
        UnicodeEmojiFilter,
        MathFilter,
        SyntaxHighlightFilter,
        MarkdownElementClassFilter,
      ].compact,
      default_context: context.merge(gfm: true),
      result_class: GitHub::HTML::Result,
      stats_key: "NonLinkingMarkdownPipeline",
    )

    # Markdown pipeline that is used solely to find task items.
    # Is used to render progress bar in issue/PR timeline
    LightweightTaskListPipeline = WarpPipe.new(
      input_filters: [
        UTF8Filter,
        MarkdownFilter,
      ],
      sanitizer: markdown_sanitizer,
      node_filters: [
        LightweightTaskListFilter
      ].compact,
      default_context: context.merge(gfm: true),
      result_class: GitHub::HTML::Result,
      stats_key: "LightweightTaskListPipeline",
    )

    # Pipeline used for the embedded readmes/descriptions of
    # package registry packages.
    PackageVersionPipeline = WarpPipe.new(
      input_filters: [
        UTF8Filter,
        MarkdownFilter,
      ],
      sanitizer: markdown_sanitizer,
      node_filters: [
        DowncaseNameFilter,
        TaskListFilter,

        # CloseKeywordFilter must go before other text filters but after TaskListFilter. It goes
        # early in the list because it needs access to sibling elements, but it must go after
        # TaskListFilter because that one needs ancestry access which is lost if the
        # CloseKeywordFilter finds any matching keywords.
        CloseKeywordFilter,
        CamoFilter,
        ImageMaxWidthFilter,
        TocFilter, # Must be before TableOfContentsFilter
        TableOfContentsFilter,
        (GitHub.ssl? ? HttpsFilter : nil),
        SuggestedChangeFilter,
        MentionFilter,
        TeamMentionFilter,
        IssueBlobFilter,
        IssueMentionFilter,
        CommitMentionFilter,
        AdvisoryMentionFilter,
        CVEMentionFilter,
        DuplicateKeywordFilter,
        ColonEmojiFilter,
        UnicodeEmojiFilter,
        MathFilter,
        SyntaxHighlightFilter,
        MarkdownElementClassFilter,
        ColorFilter,
        RelNofollowFilter,
        UrlUnfurlFilter,
      ].compact,
      output_filters: [
        GithubReferenceFilter,
      ],
      default_context: context.merge(gfm: true),
      result_class: GitHub::HTML::Result,
      stats_key: "PackageVersionPipeline",
    )

    TopicDescriptionPipeline = WarpPipe.new(
      input_filters: [
        UTF8Filter,
        MarkdownFilter,
      ],
      sanitizer: markdown_sanitizer,
      node_filters: [
        ColonEmojiFilter,
        UnicodeEmojiFilter,
        StripImageFilter,
        RelNofollowFilter,
      ].compact,
      default_context: context.merge(gfm: true),
      result_class: GitHub::HTML::Result,
      stats_key: "TopicDescriptionPipeline",
    )

    ReleasePipeline = WarpPipe.new(
      input_filters: [
        UTF8Filter,
        MarkdownFilter,
      ],
      sanitizer: markdown_sanitizer,
      node_filters: [
        TaskListFilter,
        CamoFilter,
        RelativeLinkFilter,
        RawImageFilter,
        ImageMaxWidthFilter,
        (GitHub.ssl? ? HttpsFilter : nil),
        MentionFilter,
        TeamMentionFilter,
        CustomKeyLinkFilter,
        IssueMentionFilter,
        CommitMentionFilter,
        AdvisoryMentionFilter,
        CVEMentionFilter,
        ColonEmojiFilter,
        UnicodeEmojiFilter,
        MathFilter,
        SyntaxHighlightFilter,
        MarkdownElementClassFilter,
        RelNofollowFilter,
      ].compact,
      output_filters: [
        GithubReferenceFilter,
      ],
      default_context: context.merge(gfm: true),
      result_class: GitHub::HTML::Result,
      stats_key: "ReleasePipeline",
    )

    CardPipeline = WarpPipe.new(
      input_filters: [
        UTF8Filter,
        MarkdownFilter,
      ],
      sanitizer: markdown_sanitizer,
      node_filters: [
        TaskListFilter,
        CamoFilter,
        ImageMaxWidthFilter,
        (GitHub.ssl? ? HttpsFilter : nil),
        MentionFilter,
        TeamMentionFilter,
        IssueMentionFilter,
        CommitMentionFilter,
        AdvisoryMentionFilter,
        CVEMentionFilter,
        ColonEmojiFilter,
        UnicodeEmojiFilter,
        SyntaxHighlightFilter,
        RelNofollowFilter,
      ].compact,
      output_filters: [
        GithubReferenceFilter,
      ],
      default_context: context.merge(gfm: true),
      result_class: GitHub::HTML::Result,
    )

    # pipeline used for readmes and blob previews in graphql queries
    PlatformMarkupPipeline = WarpPipe.new(
      input_filters: [
        UTF8Filter,
        YamlFilter,
        MarkupFilter,
        AutolinkFilter,
      ],
      sanitizer: markup_sanitizer,
      node_filters: [
        FilterWhenLanguage.new("Markdown", TaskListFilter),
        FilterWhenLanguage.new("Markdown", MathFilter),
        DowncaseNameFilter,
        TocFilter, # Must be before TableOfContentsFilter
        TableOfContentsFilter,
        CamoFilter,
        SyntaxHighlightFilter,
        RelativeLinkFilter,
        ColonEmojiFilter, # Must be after RelativeLinkFilter
        UnicodeEmojiFilter, # Must be after RelativeLinkFilter
        PlatformRawImageFilter, # Must be after RelativeLinkFilter
        ImageMaxWidthFilter, # Must be after RawImageFilter
        OrphanHrefFilter,
        RelNofollowFilter,
      ],
      default_context: context,
      stats_key: "PlatformMarkupPipeline",
    )

    # Pipeline used for linkless descriptions.
    SimpleDescriptionPipeline = WarpPipe.new(
      input_filters: [
        UTF8Filter,
        PlainTextInputFilter,
      ],
      node_filters: [
        ColonEmojiFilter,
        UnicodeEmojiFilter,
      ],
      default_context: context,
      stats_key: "SimpleDescriptionPipeline",
    )

    PlainUserStatusPipeline = WarpPipe.new(
      input_filters: [
        UTF8Filter,
        PlainTextInputFilter,
      ],
      node_filters: [
        ColonEmojiFilter,
        UnicodeEmojiFilter,
      ],
      default_context: context,
      stats_key: "PlainUserStatusPipeline",
    )

    ProfileBioPipeline = WarpPipe.new(
      input_filters: [
        UTF8Filter,
        PlainTextInputFilter,
      ],
      node_filters: [
        ColonEmojiFilter,
        UnicodeEmojiFilter,
        MentionFilter,
        TeamMentionFilter,
      ],
      output_filters: [
        GithubReferenceFilter,
      ],
      default_context: context,
      stats_key: "ProfileBioPipeline",
    )

    ProfileCompanyPipeline = WarpPipe.new(
      input_filters: [
        UTF8Filter,
        PlainTextInputFilter,
      ],
      node_filters: [
        MentionFilter,
      ],
      output_filters: [
        GithubReferenceFilter,
      ],
      default_context: context,
      stats_key: "ProfileCompanyPipeline",
    )

    # Pipeline used for Integration Directory long-form content.
    IntegrationListingPipeline = WarpPipe.new(
      input_filters: [
        UTF8Filter,
        MarkdownFilter,
      ],
      sanitizer: markdown_sanitizer,
      node_filters: [
        CamoFilter,
        ImageMaxWidthFilter,
        ColonEmojiFilter,
        UnicodeEmojiFilter,
      ].compact,
      default_context: context,
      result_class: GitHub::HTML::Result,
      stats_key: "IntegrationListingPipeline",
    )

    CommitSubjectPipeline = WarpPipe.new(
      input_filters: [
        UTF8Filter,
        PlainTextInputFilter,
      ],
      node_filters: [
        # CloseKeywordFilter must go first since it potentially needs to be able to look at sibling
        # elements which are lost when parsing a document fragment resulting from a replacement
        # in a previous filter.
        CloseKeywordFilter,

        MentionFilter,
        TeamMentionFilter,
        IssueMentionFilter,
        CommitMentionFilter,
        AdvisoryMentionFilter,
        CVEMentionFilter,
        ColonEmojiFilter,
        UnicodeEmojiFilter,
        RelNofollowFilter,
      ],
      output_filters: [
        GithubReferenceFilter,
      ],
      default_context: context,
      result_class: GitHub::HTML::Result,
      stats_key: "CommitMessagePipeline",
    )

    CommitMessagePipeline = WarpPipe.new(
      input_filters: [
        UTF8Filter,
        PlainTextInputFilter,
        AutolinkFilter,
      ],
      node_filters: [
        # CloseKeywordFilter must go first since it potentially needs to be able to look at sibling
        # elements which are lost when parsing a document fragment resulting from a replacement
        # in a previous filter.
        CloseKeywordFilter,

        MentionFilter,
        TeamMentionFilter,
        CustomKeyLinkFilter,
        IssueMentionFilter,
        CommitMentionFilter,
        AdvisoryMentionFilter,
        CVEMentionFilter,
        ColonEmojiFilter,
        UnicodeEmojiFilter,
        RelNofollowFilter,
      ],
      output_filters: [
        GithubReferenceFilter,
      ],
      default_context: context,
      result_class: GitHub::HTML::Result,
      stats_key: "CommitMessagePipeline",
    )

    LongCommitMessagePipeline = WarpPipe.new(
      input_filters: [
        UTF8Filter,
        PlainTextInputFilter,
      ],
      result_class: GitHub::HTML::Result,
      stats_key: "LongCommitMessagePipeline",
    )
    #
    # Pipeline used for the SponsorsListing long-form content.
    SponsorsListingPipeline = WarpPipe.new(
      input_filters: [
        UTF8Filter,
        MarkdownFilter,
      ],
      sanitizer: markdown_sanitizer,
      node_filters: [
        CamoFilter,
        ColonEmojiFilter,
        UnicodeEmojiFilter,
        H1Filter,
      ].compact,
      default_context: context,
      result_class: GitHub::HTML::Result,
      stats_key: "SponsorsListingPipeline",
    )

    # Pipeline used for the Marketplace long-form content.
    MarketplaceListingPipeline = WarpPipe.new(
      input_filters: [
        UTF8Filter,
        MarkdownFilter,
      ],
      sanitizer: markdown_sanitizer,
      node_filters: [
        CamoFilter,
        StripImageFilter,
        ColonEmojiFilter,
        UnicodeEmojiFilter,
        H1Filter,
      ].compact,
      default_context: context,
      result_class: GitHub::HTML::Result,
      stats_key: "MarketplaceListingPipeline",
    )

    # Pipeline to add noreferrer to 'a' tags
    # Used to avoid the referer field leaking the private token on atom feed
    NoReferrerPipeline = WarpPipe.new(
      node_filters: [
        NoReferrerFilter,
      ],
      default_context: context,
      stats_key: "NoReferrerPipeline",
    )

    # Pipeline used by the ReferenceableContent concern
    ContentReferencesPipeline = WarpPipe.new(
      input_filters: [
        UTF8Filter,
        MarkdownFilter,
      ],
      sanitizer: markdown_sanitizer,
      node_filters: [
        UrlUnfurlFilter,
      ],
      output_filters: [
        GithubReferenceFilter,
      ],
      result_class: GitHub::HTML::Result,
      stats_key: "ContentReferencesPipeline",
    )

    # Pipeline for Git Guides on /git-guides/
    GuidesMarkdownPipeline = WarpPipe.new(
      input_filters: [
        UTF8Filter,
        MarkdownFilter,
      ],
      sanitizer: markdown_sanitizer,
      node_filters: [
        TocFilter,
        TableOfContentsFilter,
        SyntaxHighlightFilter,
      ].compact,
      default_context: context.merge(gfm: true),
      result_class: GitHub::HTML::Result,
      stats_key: "GuidesMarkdownPipeline",
    )

    # Pipeline for rule help for code scanning alerts
    # Basically a relatively simple markdown pipeline which
    # also pulls out the first paragraph.
    CodeScanningRuleHelpMarkdownPipeline = WarpPipe.new(
      input_filters: [
        UTF8Filter,
        MarkdownFilter,
      ],
      sanitizer: markdown_sanitizer,
      node_filters: [
        CamoFilter,
        ImageMaxWidthFilter,
        (GitHub.ssl? ? HttpsFilter : nil),
        IssueBlobFilter,
        SetTableRoleFilter,
        ColonEmojiFilter,
        UnicodeEmojiFilter,
        MathFilter,
        SyntaxHighlightFilter,
        MarkdownElementClassFilter,
        ColorFilter,
        RelNofollowFilter,
        UrlUnfurlFilter,
      ].compact,
      output_filters: [
        GithubReferenceFilter,
        FirstParagraphFilter,
      ],
      default_context: context.merge(gfm: true),
      result_class: GitHub::HTML::Result,
      stats_key: "CodeScanningRuleHelpMarkdownPipeline",
    )
  end
end
