# frozen_string_literal: true

module GitHub
  module SafeDatabaseMigrationHelper
    if GitHub.rails_6_0?
      def add_column(table_name, column_name, type, **options)
        return if options[:if_not_exists] == true && column_exists?(table_name, column_name, type)

        super
      end

      def remove_column(table_name, column_name, type = nil, **options)
        return if options[:if_exists] == true && !column_exists?(table_name, column_name)

        super
      end

      def add_index(table_name, column_name, options = {})
        return if options[:if_not_exists] && index_exists?(table_name, column_name, options)
        options.delete(:if_not_exists)

        super
      end

      def remove_index(table_name, options = {})
        index_name = index_name(table_name, options)

        return if options.is_a?(Hash) && options[:if_exists] && !index_name_exists?(table_name, index_name)

        super(table_name, name: index_name)
      end
    end
  end
end
