# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module UserResearch
    # Raised when updating a ref and the CAS operation mismatches.
    class ExperimentNotFound < StandardError
      def initialize(experiment_slug)
        @experiment_slug = experiment_slug
      end

      def message
        "Cannot find experiment with slug: '#{@experiment_slug.inspect}'"
      end
    end

    # Raised when a subject is not allowed to be used with this type of experiment.
    #
    # For example, when an experiment uses current_visitor as its subject, it must
    # be defined as a VisitorExperiment and enrollment must be done in the browser,
    # rather than on the server.
    class SubjectNotAllowed < StandardError
      def initialize(experiment, subject)
        @experiment = experiment
        @subject = subject
      end

      attr_reader :experiment, :subject

      def message
        "#{experiment.class} (#{experiment&.handle}) is not allowed to use a subject of type #{subject.class}"
      end
    end

    # Raised if an experiment is not allowed to be serialized into a page.
    class ExperimentMetadataNotSupported < StandardError
      def initialize(experiment)
        @experiment = experiment
      end

      attr_reader :experiment

      def message
        "Experiment '#{experiment&.handle}' can not be handled in a browser"
      end
    end

    # Public: Return an experiment object for the given slug. Defers to
    # Shopify/verdict's lookup logic, which in turn tries to find
    # experiments in lib/github/user_research/experiments.
    def self.experiment(slug)
      Verdict[slug]
    end

    # Public: Returns an experiment variant (aka subgroup) for a given
    # experiment and subject.  Auto-enrolls a qualifying subject if not
    # already enrolled in the experiment.
    #
    # experiment  - The Symbol experiment name
    # subject     - The experiment subject e.g. a User
    # auto_enroll - A Boolean to determine if we should attempt to enroll the
    #               subject in the experiment. This option is useful when
    #               experiment enrollment has been determined manually.
    #               Defaults to `true`.
    # default     - A default value to return if the subject is not qualified
    #               for the experiment or if the experiment is unknown.
    #
    def self.experiment_variant(subject:, experiment:, auto_enroll: true, default: nil)
      return unless GitHub.user_experiments_enabled?

      with_experiment(experiment) do |experiment|
        raise SubjectNotAllowed.new(experiment, subject) unless experiment.subject_allowed?(subject)
        return default unless experiment.rolled_out?(subject)

        begin
          variant = if auto_enroll
            experiment.switch(subject)
          else
            experiment.lookup(subject).try(:to_sym)
          end

          return variant.presence || default
        rescue GitHub::UserResearch::VisitorExperiment::ExperimentMustBePerformedInBrowser
          raise
        rescue => e
          Failbot.report(e, {
            fatal: "NO (just reporting)",
          })
        end
      end

      default
    end

    # Public: Returns a VisitorExperiment's data, for embedding in a page.
    def self.experiment_metadata(experiment:, default: nil)
      return unless GitHub.user_experiments_enabled?

      with_experiment(experiment) do |experiment|
        raise ExperimentMetadataNotSupported.new(experiment) unless experiment.metadata_allowed?

        # This normally happens when checking if experiment.subject_qualifies?
        # Since we're not using qualifiers and enrollment is happening in browser,
        # call ensure_experiment_has_started here.
        experiment.instance_eval { ensure_experiment_has_started }

        return experiment.to_h.merge(default: default)
      end
    end

    # Public: Records an Octolytics event for an experiment. Use this for measuring
    # the outcomes of an experiment. Records nothing if the subject is not enrolled.
    #
    # subject    - The experiment subject e.g. a User
    # experiment - The Symbol experiment name
    # metric     - The Symbol metric name that was completed by the subject.
    #
    #   GitHub::UserResearch.success(subject: current_user,
    #                                experiment: UserExperimentCohort::CI_CTA_VERSUS_CI_MISSING_REVIEW_STATUS,
    #                                metric: "marketplace_ci_category_viewed")
    #
    def self.success(subject:, experiment:, metric:, dimensions: nil)
      return unless GitHub.user_experiments_enabled?
      return unless subject.present?

      with_experiment(experiment) do |experiment|
        return unless variant = experiment.lookup(subject).try(:to_sym)

        GitHub.instrument("user_experiment.success") do |payload|
          payload[:experiment] = experiment.name
          payload[:experiment_variant] = variant
          payload[:metric] = metric
          payload[:subject] = subject
          payload[:dimensions] = dimensions
        end
      end
    end

    # Public: Returns true if a subject is enrolled in an experiment and is
    # not in the :control subgroup. Auto-enrolls a qualifying subject if not
    # already enrolled in the experiment.
    #
    # experiment - The Symbol experiment name
    # subject    - The experiment subject e.g. a User
    #
    def self.in_experiment_test_group?(**args)
      variant = experiment_variant(**args)
      variant && variant != :control
    end

    class << self
      private

      def with_experiment(slug)
        experiment = experiment(slug)

        unless experiment
          Failbot.report(ExperimentNotFound.new(slug), {
            fatal: "NO (just reporting)",
          })

          return
        end

        experiment.save_once

        yield(experiment)
      end
    end
  end
end
