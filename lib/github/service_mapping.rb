# frozen_string_literal: true

module GitHub
  module ServiceMapping
    def self.included(base)
      base.extend ClassMethods
    end

    UNKNOWN_SERVICE = "unknown"
    SERVICE_PREFIX = "github"

    module ClassMethods
      # public: Define a service mapping for this class.
      #
      # service_name - the name of the service
      # only - a whitelist of methods affected by this service mapping.
      #        An empty list means the service mapping will be the default for the class.
      def map_to_service(service_name, only: [])
        service_name = service_name.to_sym
        @default_service_mapping = service_name if only.empty?

        @services ||= SortedSet.new
        @services << service_name

        @service_mappings ||= {}

        only.each do |method|
          method = method.to_sym
          @service_mappings[method] = service_name
        end
      end

      # public: Report the service mapping for the class or method.
      #
      # method - the method whose service mapping we want.
      #          A nil method means we want the default service mapping for the class.
      def service_mapping(method = nil)
        if method.present? && @service_mappings.present? &&
           (method_mapping = @service_mappings[method.to_sym].presence)
          method_mapping
        elsif (file_mapping = GitHub.serviceowners&.service_for_class(self))
          file_mapping
        else
          @default_service_mapping
        end
      end

      def services(prefix: false)
        return [] unless @services

        services = @services.to_a
        return services unless prefix

        services.map { |service| "#{SERVICE_PREFIX}/#{service}" }
      end
    end

    # public: Record the service mapping in all relevant contexts.
    #
    # keep this in sync with GitHub::Middleware::Stats#add_service_mapping to ensure unknown services
    # are always consistently labelled.
    def push_service_mapping_context(&block)
      current_service_mapping = logical_service

      # tag the service mapping in the rack env if available, or whatever env hash we find
      if self.respond_to?(:env)
        env[GitHub::TaggingHelper::PROCESS_SERVICE_KEY] = current_service_mapping
      end

      context = { catalog_service: current_service_mapping }

      # Just push our contexts without cleanup. Popping a context could easily result in
      # the wrong data being popped. This way we leak our data like everyone else and
      # rely on the process running to reset things.
      Audit.context.push(context)
      GitHub.context.push(context)
      Failbot.push(context)

      # Both `ApplicationController` and `Api::App` implement a `log_data` method which is
      # expected to be a hash representing data attributes which will be sent to the request
      # logger.
      log_data.merge!(context) if self.respond_to?(:log_data)

      GitHub::Logger.log_context(context, &block) if block_given?
    end

    # public: The fully qualified logical service name corresponding to the current class and method.
    def logical_service
      method = respond_to?(:service_mapping_method) ? service_mapping_method : nil
      current_service_mapping = self.class.service_mapping(method) || UNKNOWN_SERVICE
      "#{SERVICE_PREFIX}/#{current_service_mapping}"
    end

    # public: The service names associated to the class in a file.
    # This can be multiple services, because services can be mapped to specific methods in the class
    def self.services_from_class_file(file, prefix: false)
      klass = nil
      ActiveSupport::Dependencies.loadable_constants_for_path(file).find do |c|
        klass = c.safe_constantize
      end

      if klass.blank?
        # This mapping doesn't work out-of-the-box with the default inflector.
        # (We have a custom mapping set up with Zeitwerk, which is hooked up to Rails autoloading.)
        if file.end_with?("app/api/graph_ql.rb")
          klass = Api::GraphQL
        else
          return []
        end
      end

      if klass.include?(GitHub::ServiceMapping)
        klass.services(prefix: prefix)
      else
        []
      end
    end
  end
end
