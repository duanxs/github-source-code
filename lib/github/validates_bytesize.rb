# rubocop:disable Style/FrozenStringLiteralComment

module GitHub
  module ValidatesBytesize

    module ClassMethods
      BYTESIZE_TOKENIZER = lambda { |str| GitHub::BytesizeTokenizer.new(str) }

      # Validate that the fields passed are entirely composed of valid UTF-8
      # characters.
      #
      # validates_bytesize :body, maximum: MYSQL_UNICODE_BLOB_LIMIT
      #
      def validates_bytesize(field, opts = {})
        opts = Hash(opts)
        opts[:tokenizer] = BYTESIZE_TOKENIZER
        opts[:message] ||= "is too long (maximum is #{opts[:maximum]/4} characters)"

        validates_length_of(field, opts)
      end
    end

    def validates_unicode_blob_size(field, opts = {})
      opts[:maximum] ||= MYSQL_UNICODE_BLOB_LIMIT
      validates_bytesize(field, opts)
    end

    def self.included(model)
      model.extend(ClassMethods)
    end
  end
end
