# frozen_string_literal: true

require "set"

module GitHub
  # Removes keys from the context if they are not present in the allowable
  # key list. Failbot context payloads are sent to an external service provider
  # at sentry.io, and we want to prevent leaking personally identifiable
  # information.
  #
  # If you need to send a new key to Sentry, and its values don't contain
  # private information, open a pull request adding it to the list of keys
  # below.
  class FailbotKeyFilter
    def initialize(raise_on_filter: false)
      @raise_on_filter = raise_on_filter
    end

    # Removes keys from the context.
    #
    # context - The Hash from which to remove keys.
    #
    # Returns the context Hash that was passed in.
    def call(context)
      return context if GitHub.bypass_failbot_filter_logic?

      if @raise_on_filter
        call_with_raise(context)
      else
        call_without_raise(context)
      end
    end

    private

    # Raises an error if keys are present that violate the allowable key policy.
    def call_with_raise(context)
      context_keys = context.keys.map { |k| k.to_s.tr("#", "") }
      disallowed_keys = context_keys - ALLOWED_KEYS
      if disallowed_keys.any?
        # (indifferent_access not available everywhere)
        disallowed_keys = disallowed_keys + disallowed_keys.map(&:to_sym)
        raise "Filtered keys detected: #{context.slice(*disallowed_keys)}"
      end
      context
    end

    # Silently removes keys that violate the allowable key list policy.
    def call_without_raise(context)
      context.delete_if do |key, value|
        key = key.to_s.tr("#", "")
        !ALLOWED_KEYS_SET.include?(key)
      end
    end


    # Before we can send additional data to Sentry, some works needs to be done to categorize
    # what type of sensitive data, if any, it can contain.
    #
    # - review https://github.com/github/sentry/blob/master/docs/secure-exceptions.md for knowing what is considered sensitive
    # - add your key to the list
    # - update docs/sensitive-data.yaml with information about the key
    # - test/fast/linting/failbot_key_filter_test.rb will point you in the right direction
    ALLOWED_CONFIDENTIAL_GITHUB_IP_AND_INFRASTRUCTURE_DATA_KEYS = %w[
      action
      activerecord_objects
      activerecord_query_cache_hits
      api_route
      app
      areas_of_responsibility
      authorization_url
      backtrace
      catalog_service
      certificate_id
      certificate_state
      class
      controller
      current_ref
      datacenter
      delegate_healthy_hosts
      delegate_replicas
      deployed_to
      enabled_features
      exception_detail
      fastly_certificate_id
      gitrpc_calls
      job
      kube_namespace
      message
      network_id
      oauth_access_id
      page_deployment_id
      page_id
      pages_fileserver_hostname
      processor
      query_counts
      queue
      rails
      region
      release
      request_id
      route
      ruby
      server
      sha
      site
      splunk_url
    ]

    RESTRICTED_CUSTOMER_PII_KEYS = %w[
      referrer
      remote_ip
      repo
      url
    ]

    # Before we can send additional data to Sentry, some works needs to be done to categorize
    # what type of sensitive data, if any, it can contain.
    #
    # - review https://github.com/github/sentry/blob/master/docs/secure-exceptions.md for knowing what is considered sensitive
    # - add your key to the list
    # - update docs/sensitive-data.yaml with information about the key
    # - test/fast/linting/failbot_key_filter_test.rb will point you in the right direction
    ALLOWED_NON_SENSITIVE_KEYS = %w[
      accept
      account_id
      authzd_request
      authzd_response
      base_oid
      billing_transaction_id
      business_id
      check_run_id
      check_suite_id
      clone_repo_id
      cloning_user
      codespace_id
      codespace_location
      codespace_plan_id
      codespace_vscs_plan_id
      command
      comment_id
      count_activerecord
      count_allocations
      count_cache
      count_es
      count_gc
      count_gitrpc
      count_memcached
      count_mysql
      count_redis
      customer_id
      devtools
      environment_id
      error_code
      event_type
      exit_status
      experiment
      failbot_id
      force_pushed
      freeze_payouts
      gist_id
      gist_oid
      graphql_current_field
      graphql_current_path
      graphql_query_hash
      graphql_variables_hash
      hashed_token
      head_oid
      issue_blob_reference_id
      issue_id
      language
      line_item_id
      logged
      master_pid
      master_started_at
      member
      mergeable_base_sha
      mergeable_head_sha
      metered_product
      method
      migration_id
      name
      new_head_ref
      notice_name
      notifications_list_key
      notifications_thread_key
      observation
      operation
      org_id
      package_id
      parent_id
      platform
      private_repo
      pull_request_id
      pusher
      ready_state
      repo_clone_id
      repo_id
      repository_transfer_id
      request_category
      rollup
      root_oid
      sanitized_url
      server_id
      spec
      splunk_correlation_id
      sponsorable_id
      sponsorable_type
      sponsors_activity
      sponsors_listing_id
      sponsors_membership_id
      sql_sanitized
      sql_truncated
      stateless
      stripe_connect_account_id
      target_oid
      team_id
      time_cache
      time_cpu
      time_es
      time_gc
      time_gitrpc
      time_idle
      time_memcached
      time_mysql
      time_real
      time_redis
      time_since_load
      timer
      transaction_query_count
      transaction_query_time_ms
      transaction_time_ms
      type
      user
      user_agent
      user_id
      user_session_id
      user_spammy
      vss_subscription_event_id
      worker_request_count
      worker_started_at
      zone
      zuora_webhook_id
    ]

    ALLOWED_KEYS = ALLOWED_CONFIDENTIAL_GITHUB_IP_AND_INFRASTRUCTURE_DATA_KEYS + ALLOWED_NON_SENSITIVE_KEYS
    ALLOWED_KEYS_SET = ALLOWED_KEYS.to_set
  end
end
