# frozen_string_literal: true

module GitHub
  module FaradayMiddleware
    # Faraday Middleware for opentracing our Spokes client
    #
    # It would be "better" to use faraday/tracer middleware rather than write our
    # own, however  the current version doesn't support our old faraday. It's not
    # really *that* hard to build a purpose built middlware so we can just deal
    # with it.
    class Tracer < Faraday::Middleware
      # Create a new Faraday Tracer middleware.
      #
      # @param app The faraday application/middlewares stack.
      # @param parent_span [Span, SpanContext, Proc, nil] SpanContext that acts as a parent to
      #        the newly-started by the middleware Span. If a Proc is provided, its
      #        evaluated during the call method invocation.
      # @param service_name [String, nil] Remote service name (for some
      #        unspecified definition of "service")
      # @param tracer [OpenTracing::Tracer] A tracer to be used when start_span, and inject
      #        is called.
      # @param errors [Array<Class>] An array of error classes to be captured by the tracer
      #        as errors. Errors are **not** muted by the middleware.
      def initialize(app, options = {})
        super(app)
        @tracer = options[:tracer] || OpenTracing.global_tracer
        @parent_span = options[:parent_span]
        @operation = options[:operation]
        @service_name = options[:service_name]
        @errors = options[:errors] || [StandardError]
      end

      def call(env)
        span = @tracer.start_span(operation(env) || env[:method].to_s.upcase,
                                  child_of: parent_span,
                                  tags: prepare_tags(env))

        @tracer.inject(span.context, OpenTracing::FORMAT_RACK, env[:request_headers])

        begin
          @app.call(env).on_complete do
            span.set_tag("http.status_code", env[:status])
            span.set_tag("error", true) if env[:status] >= 400
          end
        rescue *@errors => e
          span.set_tag("error", true)
          span.log(event: "error", 'error.object': e)
          raise
        ensure
          span.finish
        end
      end

      private

      def operation(env)
        @operation.respond_to?(:call) ? @operation.call(env) : @operation
      end

      def parent_span
        @parent_span.respond_to?(:call) ? @parent_span.call : @parent_span
      end

      def prepare_tags(env)
        {
          "component"    => "faraday",
          "span.kind"    => "client",
          "http.method"  => env[:method].to_s.upcase,
          "http.url"     => env[:url].to_s,
          "peer.service" => @service_name,
        }
      end
    end
  end
end
