# frozen_string_literal: true

module GitHub
  module FaradayMiddleware
    # Faraday Middleware for opentracing our Spokes client
    #
    # It would be "better" to use faraday/tracer middleware rather than write our
    # own, however  the current version doesn't support our old faraday. It's not
    # really *that* hard to build a purpose built middlware so we can just deal
    # with it.
    class Datadog < Faraday::Middleware
      # Create a new Faraday Tracer middleware.
      #
      # @param app The faraday application/middlewares stack.
      # @param service_name [String, nil] Remote service name (for some
      #        unspecified definition of "service")
      # @param stats Stats implementation
      def initialize(app, options = {})
        super(app)
        @service_name = options[:service_name] || "unknown"
        @dogstats = options[:stats]
      end

      def call(env)
        success_tag = "success:true"
        start = Time.now
        tags = [
          "method:#{env[:method]}",
          "path:#{env[:url].request_uri}",
        ]

        begin
          @app.call(env).on_complete do
            tags << "status:#{env[:status]}"
          end
        rescue => e
          tags << "err:#{e.class.name}"
          success_tag = "success:false"
          raise
        ensure
          tags << success_tag
          ms = (Time.now - start) * 1000
          @dogstats.timing("rpc.#{@service_name}.time", ms, tags: tags)
          @dogstats.distribution("rpc.#{@service_name}.dist_time", ms, tags: tags)
        end
      end
    end
  end
end
