# rubocop:disable Style/FrozenStringLiteralComment

require "github/config/stats"
require "scrolls"

module GitHub
  class Logger < ActiveSupport::Logger
    DEVELOPMENT_LOG_FILE = "log/development.log"
    PERSONAL_LOG_FILE = "log/personal.log"

    # LogSubscriber has more constants available, these are just the ones
    # we're using for formatting here
    RED = ActiveSupport::LogSubscriber::RED
    YELLOW = ActiveSupport::LogSubscriber::YELLOW
    BOLD = ActiveSupport::LogSubscriber::BOLD
    CLEAR = ActiveSupport::LogSubscriber::CLEAR

    # Public: Log data and/or wrap a block with start/finish
    #
    # data - A hash of key/values to log
    # blk  - A block to be wrapped by log lines
    #
    def self.log(data, &blk)
      Scrolls.log(data, &blk)
    end

    # Public: Set a context in a block for logs
    #
    # data - A hash of key/values to prepend to each log in a block
    # blk  - The block that our context wraps
    #
    def self.log_context(data, &blk)
      Scrolls.context(data, &blk)
    end

    # Public: Log an exception
    #
    # data - A hash of key/values to log
    # e    - An exception to pass to the logger
    #
    def self.log_exception(data, e)
      Scrolls.log_exception(data, e)
    end

    # Internal: Default logging data for each log message
    #
    # Returns Hash
    def self.default_log_data
      result = { "app" => "github", "env" => Rails.env }
      if GitHub.enterprise?
        result["enterprise"] = true
      end
      result
    end

    # Public: An empty HashWithIndifferentAccess to be used for accumulating
    # application log data.
    def self.empty
      HashWithIndifferentAccess.new
    end

    # Internal: Do everything needed to use GitHub::Logger.log
    # ie global context, syslog in production
    def self.setup
      self.destination = GitHub::Config::Logging.destination
    end

    def self.destination=(dest)
      @@dest = dest

      options = {
        global_context: default_log_data,
        exceptions: "single",
      }

      if dest == GitHub::Config::Logging::Destination::SYSLOG
        options[:stream] = "syslog"
        options[:facility] = "local7"
      end

      Scrolls.init(options)
    end

    def self.destination
      return @@dest
    end

    # Internal: Disable logging to syslog and STDOUT.
    def self.disable
      Scrolls.stream = StringIO.new
    end

    # Public: forces all logs within the block and any nested scopes to be
    # directed to 'dest' instead of the default destination
    # 'GitHub::Config::Logging.destination'.
    #
    # dest - The desired output stream.
    def self.with_destination(dest)
      return unless block_given?

      old_dest = self.destination
      begin
        # To ensure that we use the 'local7' facility if 'dest' is 'syslog',
        # call 'self.setup' to perform any initialization without wiping
        # context.
        self.destination = dest
        yield
      ensure
        # Teardown our changes and restore things back to as they were by
        # calling 'setup' again.
        self.destination = old_dest
      end
    end

    # Public: Compatibility methods for various loggers.
    #
    # Convience wrapper methods so we can use GitHub::Logger in some specific
    # places (for example: env['rack.errors'], or Logger like instances).
    #
    def self.puts(data, &blk)
      Scrolls.log(data, &blk)
    end

    def self.error(data, &blk)
      Scrolls.error(data, &blk)
    end

    def self.fatal(data, &blk)
      Scrolls.fatal(data, &blk)
    end

    def self.info(data, &blk)
      Scrolls.info(data, &blk)
    end

    def self.warn(data, &blk)
      Scrolls.warn(data, &blk)
    end

    def self.unknown(data, &blk)
      Scrolls.unknown(data, &blk)
    end

    def initialize(*args)
      super
      @personal = ActiveSupport::Logger.new(Rails.root.join(PERSONAL_LOG_FILE))
      @indent_level = 0
    end

    def mine(message = nil)
      if message
        add(Logger::INFO, message)
        @personal.add(Logger::INFO, message)
      end

      if block_given?
        @indent_level += 1
        called_from = caller(1, 1)[0]

        @personal.add(Logger::INFO, " ") if @indent_level == 1
        @personal.add(Logger::INFO, formatted("Beginning block in #{called_from}", RED, true))
        res = yield @personal
        @personal.add(Logger::INFO, formatted("Finished block in #{called_from}", RED, true))
        @personal.add(Logger::INFO, " ") if @indent_level == 1

        res
      end
    ensure
      @indent_level -= 1 if block_given?
    end

    # These are stack trace entries we don't care about when looking for the
    # original caller for a log call
    CALLER_EXCLUSION_LIST = [
        Rails.root.join("vendor"),
        Rails.root.join("lib/github/notifications/fanout_with_exception_handling.rb"),
        Rails.root.join("config/initializers/cache_notification_info.rb"),
    ].map(&:to_s)

    def add(severity, message = nil, progname = nil, &block)
      super(severity, message, progname, &block)

      if overriding?
        origin = nil
        caller(1, nil).each do |line|
          next if origin

          skip_line = CALLER_EXCLUSION_LIST.any? { |start| line.starts_with?(start) }
          origin = line unless skip_line
        end

        @personal.add(severity, formatted("Called from #{origin}", YELLOW), progname, &block)
        @personal.add(severity, formatted(message), formatted(progname), &block)
      end
    end

    private

    def overriding?
      @indent_level > 0
    end

    def formatted(message, color = nil, bold = false)
      return if message.nil?

      indent = "  " * (@indent_level - 1)
      bold = bold ? BOLD : nil
      clear = bold || color ? CLEAR : nil

      "#{indent}#{bold}#{color}#{message.lstrip}#{clear}"
    end
  end
end
