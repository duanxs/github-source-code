# frozen_string_literal: true

require "open3"
require "thor"

module GitHub
  module Zuora
    module UsageCLI
      PLAYBOOK_DESC = <<~LONGDESC
      For more information, consult the playbook:
      https://github.com/github/gitcoin/blob/master/docs/playbook/howto/manage_zuora_usage_files.md
      LONGDESC

      class Response < Thor
        include ActionView::Helpers::TextHelper

        desc "download URL DEST", "Download a response file from Zuora"
        long_desc <<~LONGDESC
        Downloads a response file from Zuora from a given URL, which is provided
        by Zuora in an email that they send to us. The file is saved to DEST,
        which can be a file or directory. You will be prompted if the destination
        file already exists.

        #{PLAYBOOK_DESC}
        LONGDESC
        def download(url, dest)
          uri = URI(url)
          zip_dest = nil

          say "Downloading response file from Zuora... "
          Net::HTTP.start(uri.host, uri.port, use_ssl: true) do |http|
            request = Net::HTTP::Get.new(uri)
            request.basic_auth(GitHub.zuora_access_key_id, GitHub.zuora_secret_access_key)

            http.request(request) do |response|
              response.value # Raises if response is not successful

              content_disposition = response["Content-Disposition"]
              filename_matches = content_disposition.match(/filename=(?:"?)([^;"]+)(?:"?)/)
              filename = filename_matches[1].strip

              if File.directory?(dest)
                dest = File.join(dest, filename.gsub(/\.zip\z/, ".csv"))
              end

              if File.exist?(dest) && !file_collision(dest)
                return say "aborting"
              end

              zip_dest = dest + ".zip"
              File.open(zip_dest, "wb") do |zip_file|
                response.read_body { |chunk| zip_file.write(chunk) }
              end
            end
          end
          say "done", :green

          say "Extracting response file... "
          output, status = Open3.capture2e("unzip", "-o", zip_dest, "-d", File.dirname(zip_dest))
          if status.success?
            say "done", :green
          else
            say "FAILED", :red
            say output
          end

          say "Extracted to #{dest}"
        rescue Net::HTTPClientException => e
          say "FAILED", :red
          say e.to_s
        end

        desc "analyze FILE", "Analyze a response file from Zuora"
        long_desc <<~LONGDESC
        Analyzes a response file from Zuora and shows a summary of results,
        including how many rows can be resubmitted as is, as well as the
        various errors encountered and the users affected.

        #{PLAYBOOK_DESC}
        LONGDESC
        def analyze(file)
          csv = CSV.open(file, "r", headers: true)
          analyzer = UsageResponseAnalyzer.new(csv)

          say "Analyzing #{File.basename(file)}... "
          analyzer.analyze
          say "done", :green

          csv.close

          say ""
          say pluralize(analyzer.ok_rows.length, "row") + " can be imported as is", :green

          analyzer.error_rows.each do |error_message, errors|
            say ""
            say pluralize(errors.length, "row") + " - "
            say "Error: #{error_message}", :red

            print_table(analyzer.details_table(error_message), indent: 2)
          end
        end

        desc "split RESPONSE SUBMITTED [DEST]", "Splits a SUBMITTED file into two based on RESPONSE"
        long_desc <<~LONGDESC
        Splits the SUBMITTED file that we originally submitted to Zuora into two
        files, once containing rows that can be resubmitted as is and one with
        rows that encountered errors, based on the RESPONSE file from Zuora. The
        new files will be saved to DEST, which is either a file or directory. If
        DEST is omitted, the directory of the SUBMITTED file is used. You will
        be prompted if the destination files already exist.

        The RESPONSE file can be obtained by running `bin/zuora-usage response download URL DEST`.
        The SUBMITTED file can be obtained by running `bin/zuora-usage download ID DEST`.

        The generated file with the suffix `-resubmit` can be resubmitted to
        Zuora by running `bin/zuora-usage resubmit ID SOURCE`. However, you
        should not resubmit multiple times unless additional errors are
        encountered.

        The generated file with the suffix `-errors` contains rows that have
        errors that must be corrected. Once these errors are corrected, the
        records can be orphaned from the current batch and submitted in a
        new batch by running `bin/zuora-usage orphan KIND SOURCE`.

        #{PLAYBOOK_DESC}
        LONGDESC
        def split(response, submitted, dest = nil)
          dest ||= File.dirname(submitted)

          unless File.directory?(dest)
            return say "ERROR: #{dest} must be a directory", :red
          end

          resubmit_file_path = File.join(dest, File.basename(submitted) + "-resubmit")
          if File.exist?(resubmit_file_path) && !file_collision(resubmit_file_path)
            return say "Aborting"
          end

          error_file_path = File.join(dest, File.basename(submitted) + "-errors")
          if File.exist?(error_file_path) && !file_collision(error_file_path)
            return say "Aborting"
          end

          response_csv = CSV.open(response, "r", headers: true)
          analyzer = UsageResponseAnalyzer.new(response_csv)

          say "Analyzing #{File.basename(response)}... "
          analyzer.analyze
          say "done", :green

          submitted_file = File.open(submitted, "r")
          resubmit_file = File.open(resubmit_file_path, "w")
          error_file = File.open(error_file_path, "w")

          say "Splitting #{File.basename(submitted)}... "
          analyzer.split(
            submitted_file: submitted_file,
            resubmit_file: resubmit_file,
            error_file: error_file,
          )
          say "done", :green

          response_csv.close
          submitted_file.close
          resubmit_file.close
          error_file.close

          say ""
          say "Generated #{resubmit_file_path}", :green
          say "This file can be resubmitted to Zuora as is by running the following command:"
          say ""
          say "  bin/zuora-usage resubmit ID #{resubmit_file_path}"
          say ""
          say "Note: You will need the SynchronizationBatch ID"
          say ""
          say "Generated #{error_file_path}", :green
          say ""
          say "This file contains rows with one or more errors. Consult the response file to "
          say "fix the errors. Once the errors are fixed, these rows can be removed from this "
          say "batch and submitted with another batch by running the following command:"
          say ""
          say "  bin/zuora-usage response orphan KIND #{error_file_path}"
        end

        desc "orphan KIND FILE", "Orphans line items from synchronization batches"
        option :batch_size, type: :numeric, default: 100, desc: "The number of database records to update at once"
        option :force, desc: "Write changes without prompting for confirmation"
        option :noop, desc: "Do not write changes to the database"
        long_desc <<~LONGDESC
        Orphans line item records from their synchronization batches, allowing
        them to be picked up in the next synchronization run and re-submitted to
        Zuora.

        KIND is the type of usage line item to update (i.e. 'actions'). FILE is
        a CSV file with the same format as the batch file submitted to Zuora
        containing the records that should be orphaned from the synchronization
        batch. This file should be the '-errors' suffixed file generated by the
        `bin/zuora-usage response split` command.

        It is recommended to run this command with the '--noop' flag first to
        confirm that the expected number of records will be modified.

        #{PLAYBOOK_DESC}
        LONGDESC
        def orphan(kind, file)
          record_class = case kind
          when "actions" then ::Billing::ActionsUsageLineItem
          else
            say "ERROR: Invalid kind", :red
            say "Valid kind values are:"
            say "  - actions"
            return
          end

          error_csv = CSV.open(file, "r", headers: true)
          analyzer = UsageResponseAnalyzer.new(error_csv)

          say "Analyzing #{File.basename(file)}... "
          ids = analyzer.extract_item_ids
          say "done", :green

          error_csv.close

          if options["noop"]
            say ""
            say "Would have orphaned #{pluralize(ids.length, record_class.to_s)} ", :green
            say "(noop)"
            return
          elsif !options["force"]
            say "This will orphan #{pluralize(ids.length, 'line item')} and they will be resubmitted to Zuora!", :yellow
            say "Please make sure that:"
            say "  1. These records were not submitted to Zuora because of an error"
            say "  2. The underlying errors have been corrected"
            say "  3. You are orphaning the correct kind of record (#{record_class})"
            say ""
            say "It is not possible to undo this operation!", :yellow
            return unless yes?("Continue?")
          end

          total = 0
          say "Orphaning #{pluralize(ids.length, 'line item')}... "
          ids.each_slice(options["batch_size"]) do |slice|
            total += record_class.where(id: slice).update_all(synchronization_batch_id: nil)
          end
          say "done", :green

          say ""
          say "Orphaned #{pluralize(total, record_class)}", :green
        end
      end

      class CLI < Thor
        BUCKET_NAME = ::SubmitZuoraUsageFileJob::BUCKET_NAME

        desc "status [ID] [-n NUMBER] [-z]", "Get the status of a specific batch file or the most recent batch files"
        option :number, aliases: [:n], type: :numeric, default: 10, desc: "The number of recent batch files to display"
        option :zuora_status, aliases: [:z], type: :boolean, default: false, desc: "Query Zuora for the latest import status"
        long_desc <<~LONGDESC
        Get the status of a specific batch file specified by ID, or get the
        status of the most recent batch files. Use the -n/--number=N option to
        change how many batches are shown. Use the -z/--zuora-status option to
        query Zuora for the most recent batch status.

        Usually, running `bin/zuora-usage status` with no options is sufficient.
        Running `bin/zuora-usage status ID` is useful if you already know the
        synchronization batch ID, such as after you've resubmitted the file to
        Zuora for processing.

        #{PLAYBOOK_DESC}
        LONGDESC
        def status(id = nil)
          batches = ::Billing::UsageSynchronizationBatch
            .order("created_at DESC")
            .limit(options["number"])

          batches = batches.where(id: id) if id

          rows = batches.map do |batch|
            [
              batch.id,
              batch.status,
              batch.created_at,
              batch.updated_at,
              *zuora_data(batch),
            ]
          end

          headers = ["ID", "Status", "Created At", "Updated At"]
          headers += ["Zuora Status", "Message"] if options["zuora_status"]
          rows.unshift(headers)

          print_table(rows)
        end

        desc "download ID [ID...] DEST", "Download one or more CSV batch files submitted to Zuora"
        long_desc <<~LONGDESC
        Download one or more CSV batch files submitted to Zuora. ID is the synchronization
        batch ID for the file to download; you can find the ID from the database or by
        running `bin/zuora-usage status`. DEST is a file or drectory where the batch file
        will be saved. You will be prompted if the destination file already exists.

        #{PLAYBOOK_DESC}
        LONGDESC
        def download(*ids, dest)
          batches = ::Billing::UsageSynchronizationBatch.find(ids)

          batches.each do |batch|
            file_dest = if File.directory?(dest)
              File.join(dest, File.basename(batch.upload_filename))
            else
              dest
            end

            if File.exist?(file_dest) && !file_collision(file_dest)
              return say "Aborting"
            end

            say "Downloading #{batch.upload_filename} from S3... "
            s3_client.get_object(
              bucket: BUCKET_NAME,
              key: batch.upload_filename,
              response_target: file_dest,
            )
            say "done", :green

            say "Downloaded to #{file_dest}"
          end
        rescue ActiveRecord::RecordNotFound => e
          say e.message, :red
        rescue Aws::S3::Errors::NoSuchKey
          say "CSV batch file does not exist in S3", :red
        end

        desc "resubmit ID SOURCE", "Resubmit a CSV batch file using a CSV file on the local filesystem"
        long_desc <<~LONGDESC
        Resubmits the batch file for a specific synchronization batch given by
        by ID, but with a CSV file on the local filesystem at SOURCE. This file
        may or may not be edited to remove any rows that encountered errors.

        After the file is resubmitted to Zuora, a background job will poll Zuora
        to determine if the new file was accepted. You can run
        `bin/zuora-usage status ID` to view the status of the specific batch
        after resubmission.

        #{PLAYBOOK_DESC}
        LONGDESC
        def resubmit(id, source)
          batch = ::Billing::UsageSynchronizationBatch.find(id)
          source_file = File.open(source, "r")

          say "Uploading #{source} to S3 as #{batch.upload_filename}... "
          s3_client.put_object(
            bucket: BUCKET_NAME,
            key: batch.upload_filename,
            body: source_file,
          )
          say "done", :green

          batch.mark_uploaded

          say "Submitting #{batch.upload_filename} to Zuora... "
          SubmitZuoraUsageFileJob.perform_now(batch)

          batch.reload
          if batch.submitted?
            say "done", :green
          else
            say "FAILED", :red
            say ""
            say "Submission to Zuora failed. Check Datadog to see the cause of the error,"
            say "or try invoking the following code from a Rails console for more details:"
            say ""
            say "    batch = Billing::UsageSynchronizationBatch.find(#{batch.id})"
            say "    SubmitZuoraUsageFileJob.perform_now(batch)"
            say ""
          end
        rescue ActiveRecord::RecordNotFound
          say "Batch #{id} does not exist", :red
        rescue Aws::Errors::ServiceError => e
          say "FAILED", :red

          say "S3 upload failed: ", :red
          say e.message
        end


        desc "response SUBCOMMAND [...ARGS]", "Process response files from Zuora"
        subcommand "response", Response

        private

        def s3_client
          GitHub.s3_billing_client
        end

        def zuora_data(batch)
          return [] unless options["zuora_status"]

          import = ::Billing::Zuora::Import.find(batch.usage_id)
          [
            import.status,
            import.status_reason,
          ]
        rescue Zuorest::HttpError, *SubmitZuoraUsageFileJob::RETRYABLE_ERRORS => e
          [
            "Unknown",
            e.to_s,
          ]
        end
      end
    end
  end
end
