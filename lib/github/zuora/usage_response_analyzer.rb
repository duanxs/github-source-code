# frozen_string_literal: true


module GitHub
  module Zuora
    # Analyzes response files from Zuora and summarizes the results
    class UsageResponseAnalyzer
      # Public: The rows which contained errors
      attr_reader :error_rows

      # Public: The rows that contained no errors
      attr_reader :ok_rows

      # public: The CSV response file
      attr_reader :response

      # Public: Initialize the UsageResponseAnalyzer
      #
      # csv - A CSV object with the response file opened for reading
      def initialize(csv)
        @response = csv

        @ok_rows = {}
        @error_rows = {}
      end

      # Public: Analyze the response file
      #
      # Returns nothing
      def analyze
        response.each do |row|
          if row["Message"].blank?
            row_uuid = row["UUID"]
            @ok_rows.store(row_uuid, row)
          else
            error_messages = row["Message"].split(";")
            error_messages.each do |error_message|
              @error_rows[error_message] ||= []
              @error_rows[error_message] << row
            end
          end
        end
      end

      # Public: Extract all line item IDs from the file
      #
      # Returns Array of Item IDs
      def extract_item_ids
        response.map do |row|
          row["GITHUB_ITEM_ID"]
        end.compact
      end

      # Public: Split the submitted file into two files, one containing rows
      # that can be resubmitted as is and one containing rows that have
      # errors
      #
      # This method does not CSV-parse the submitted file in order to avoid any
      # unintended mutation; it reads the raw row, parses individual lines in
      # order to find the underlying record ID, and copies the raw, unparsed
      # row to the appropriate output file.
      #
      # submitted_file - A File object reading the submitted file
      # resubmit_file  - A File object to which rows without errors can be written
      # error_file     - A File object to which rows with errors can be written
      #
      # Returns nothing
      def split(submitted_file:, resubmit_file:, error_file:)
        headers = submitted_file.readline
        csv_headers = CSV.parse_line(headers)
        uuid_index = csv_headers.index("UUID")

        unless uuid_index
          raise "Potentially malformed CSV file; could not find UUID column"
        end

        resubmit_file.write(headers)
        error_file.write(headers)

        until submitted_file.eof?
          row = submitted_file.readline
          row_csv = CSV.parse_line(row)

          row_uuid = row_csv[uuid_index]

          if row_ok?(uuid: row_uuid)
            resubmit_file.write(row)
          else
            error_file.write(row)
          end
        end
      end

      # Public: Get the details for a specific error message
      #
      # This method returns a two-dimensional array suitable for printing a
      # table in Thor. The first item in the array is an array of headers, and
      # each subsequent item is an array containing the GitHub owner id, Zuora
      # account number, and the number of rows from the CSV file that correspond
      # to the error message, owner id, and Zuora account.
      #
      # Example:
      #   analyzer.print_table("No good")
      #   # => [ [ "Owner Id", "Zuora Account", "# Rows" ],
      #          [ "123", "A12345678", 12 ] ]
      #
      # Returns Array
      def details_table(error_message)
        table = []
        table << ["Owner Id", "Zuora Account", "# Rows"]

        table += error_rows[error_message].each_with_object({}) do |row, memo|
          key = [row["GITHUB_OWNER_ID"], row["ACCOUNT_ID"]]
          memo[key] ||= 0
          memo[key] += 1
        end.to_a.map(&:flatten)
      end

      private

      # Internal: Check if this row encountered errors or not
      #
      # uuid - The UUID from the row
      #
      # Returns Boolean
      def row_ok?(uuid:)
        ok_rows.key?(uuid)
      end
    end
  end
end
