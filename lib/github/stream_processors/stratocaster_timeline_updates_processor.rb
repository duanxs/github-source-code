# frozen_string_literal: true

module GitHub
  module StreamProcessors
    class StratocasterTimelineUpdatesProcessor < Hydro::Processor
      DEFAULT_GROUP_ID = "stratocaster_timeline_updates"
      DEFAULT_SUBSCRIBE_TO = /#{Stratocaster::TIMELINE_UPDATE_SCHEMA}\Z/
      REVIEW_LAB_SUBSCRIBE_TO = /#{Stratocaster::REVIEW_LAB_TIMELINE_TOPIC}\Z/
      STATS_NAMESPACE = "stratocaster_timeline_updates_processor"

      options[:min_bytes] = 512.kilobytes
      options[:max_wait_time] = 1.second
      options[:max_bytes_per_partition] = 512.kilobytes
      options[:automatically_mark_as_processed] = false

      # When connecting for the first time, should we consume from the start
      # of the topic or only process new messages as they arrive
      # This only applies on the first ever connection for this consumergroup
      options[:start_from_beginning] = false

      attr_reader :filter
      attr_reader :index

      def initialize(group_id: nil, subscribe_to: nil, filter: nil)
        options[:group_id] = group_id || DEFAULT_GROUP_ID
        options[:subscribe_to] = subscribe_to || DEFAULT_SUBSCRIBE_TO

        @filter = filter
        @index = GitHub.stratocaster.index

        Failbot.push(
          stream_processor: self.class.name.underscore,
          group_id: options[:group_id],
          subscribe_to: options[:subscribe_to].to_s,
        )
      end

      def process_with_consumer(batch, consumer)
        reset_updates
        GitHub.dogstats.batch do
          GitHub.dogstats.increment("#{STATS_NAMESPACE}.received_batch")

          batch.each do |message|
            next if filter && !filter.call(message)

            latency_ms = (Time.now.to_f - message.timestamp.to_f) * 1_000

            GitHub.dogstats.increment("#{STATS_NAMESPACE}.messages", tags: ["partition:#{message.partition}"])
            GitHub.dogstats.timing("#{STATS_NAMESPACE}.latency", latency_ms.to_i, tags: ["partition:#{message.partition}"])

            batch_index_update(message.value[:index_key], message.value[:event_id])
          end

          GitHub.dogstats.count("#{STATS_NAMESPACE}.indexes_count", @index_updates.length)
          GitHub::Logger.log({
            stratocaster_processor_indexes_count: @index_updates.length,
            stratocaster_processor_received_batch_size: batch.length,
          })

          begin
            GitHub.dogstats.time("#{STATS_NAMESPACE}.bulk_insert") do
              @index.bulk_insert(@index_updates)
            end
            consumer.mark_message_as_processed(batch.last)
          rescue => e
            GitHub.dogstats.increment("#{STATS_NAMESPACE}.failed", tags: ["error:#{e.class.name.underscore}"])
            Failbot.report(e)
          end
        end
      end

      private

      def reset_updates
        @index_updates = {}
      end

      def index_updates(index_key)
        @index_updates[index_key] ||= []
      end

      def batch_index_update(index_key, event_id)
        index_updates(index_key).unshift(event_id)
      end
    end
  end
end
