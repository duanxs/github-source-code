
# frozen_string_literal: true

module GitHub
  module StreamProcessors
    module RegistryMetadata
      class VersionPublishedProcessor < BaseProcessor
        DEFAULT_GROUP_ID = "rms_version_published"
        DEFAULT_SUBSCRIBE_TO = /registry_metadata\.v0\.VersionPublished\Z/

        options[:min_bytes] = 1
        options[:max_wait_time] = 0.2.seconds
        options[:max_bytes_per_partition] = 1.megabytes
        options[:start_from_beginning] = false

        # Initialize the IndexVersionPublishedProcessor
        def setup(**kwargs)
          options[:group_id] ||= DEFAULT_GROUP_ID
          options[:subscribe_to] ||= DEFAULT_SUBSCRIBE_TO
          self.metric_prefix = "register_webhooks_processor"
        end

        # Process a single Hydro message
        #
        # message - The Hydro message to process
        #
        # Returns nothing
        def process_message(message)
          req_context = message.value.dig(:request_context)

          initialize_context(req_context)
          trace_processor(self, req_context) do
            register_webhook(message)
          end
        end

        def register_webhook(message)
          input = {
            actor_id: message.value.dig(:actor_id),
            action: "create",
            package: message.value.dig(:package),
            version: message.value.dig(:version),
          }

          GitHub.instrument("packagev2.create", input)
        end

        private

        def initialize_context(request_context)
          GitHub.context.push({
            actor_ip: request_context.dig(:x_real_ip),
            request_id: request_context.dig(:request_id)
          })
        end

        def trace_processor(processor, request_context)
          begin
            GitHub.tracer.with_enabled(GitHub.tracing_enabled?) do
              request_id = request_context.dig(:request_id)
              tracing_carrier = request_context.dig(:tracing_carrier)
              parent_context = GitHub.tracer.extract(OpenTracing::FORMAT_TEXT_MAP, tracing_carrier)

              # Would be preferable to use references: [OpenTracing::Reference.follows_from(parent_context)], but neither
              # OctoTracer nor LightStep currently support follows_from relationships
              GitHub.tracer.with_span(processor.class.name.demodulize, child_of: parent_context) do |span|
                span.set_tag("component", "hydro_processor")
                span.set_tag("span.kind", "consumer")
                span.set_tag("guid:github_request_id", request_id)

                yield

              end
            end
          end
        end

        def error_context_for_message(message)
          {
            package_id: message.value.dig(:package, :id),
            namespace: message.value.dig(:package, :namespace),
            name: message.value.dig(:package, :name),
            version_id: message.value.dig(:version, :id),
            version: message.value.dig(:version, :name),
          }
        end
      end
    end
  end
end
