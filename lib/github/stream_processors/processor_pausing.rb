# frozen_string_literal: true

module GitHub
  module StreamProcessors
    module ProcessorPausing
      extend ActiveSupport::Concern

      included do
        before_open do
          sleep(1) while paused?
        end
      end

      # Includes methods for allowing a processor to Pause/Resume their message processing
      # See https://github.com/stafftools/stream_processors for UI on using this functionality.
      #
      # These class methods may be overriden to provide different approaches to pausing and resuming
      # but all of them MUST be overriden if one is changed.
      class_methods do
        # Public: Pause a processor with the given pause key.
        #
        # This method can be overriden by subclasses to provide their own pause/resume implementation.
        # If this method is overriden then resume, paused?, and paused_at must be overriden as well
        #
        # pause_key - A String key used to set the pause record for the processor.
        # expires   - Optional Time expiration that will be used to automatically expire the pause
        #
        # Returns nothing
        def pause(pause_key, expires: nil)
          GitHub.kv.set("processor-paused-#{pause_key}", Time.current.to_s, expires: expires)
        end

        # Public: Resume a processor with the given pause key.
        #
        # This method can be overriden by subclasses to provide their own pause/resume implementation.
        # If this method is overriden then pause, paused?, and paused_at must be overriden as well
        #
        # pause_key - A String key used to set the delete the pause record for the processor.
        #
        # Returns nothing
        def resume(pause_key)
          GitHub.kv.del("processor-paused-#{pause_key}")
        end

        # Public: Checks whether the given pause key exists and the processor is paused
        #
        # This method can be overriden by subclasses to provide their own pause/resume implementation.
        # If this method is overriden then pause, resume, and paused_at must be overriden as well
        #
        # pause_key - A String key used to check whether the pause key has been set.
        #
        # Returns Boolean
        def paused?(pause_key)
          ActiveRecord::Base.connected_to(role: :reading) do
            GitHub.kv.exists("processor-paused-#{pause_key}").value!
          end
        end

        # Public: Returns the Time when the pause key was set or paused
        #
        # This method can be overriden by subclasses to provide their own pause/resume implementation.
        # If this method is overriden then pause, resume, and paused? must be overriden as well
        #
        # pause_key - A String key used to check whether the pause key has been set.
        #
        # Returns Time
        def paused_at(pause_key)
          paused_at = GitHub.kv.get("processor-paused-#{pause_key}").value!
          return unless paused_at

          Time.parse(paused_at)
        end
      end

      # Public: Pause the processor by its pause key which by default is the group ID.
      #
      # To change the implementation of pausing and resuming the class method should be overriden
      #
      # expires   - Optional Time expiration that will be used to automatically expire the pause
      #
      # Returns nothing
      def pause(expires: nil)
        self.class.pause(pause_key, expires: expires)
      end

      # Public: Resumes the processor by its resume key which by default is the group ID.
      #
      # To change the implementation of pausing and resuming the class method should be overriden
      #
      # Returns nothing
      def resume
        self.class.resume(pause_key)
      end

      # Public: Returns whether the processor is paused
      #
      # To change the implementation of pausing and resuming the class method should be overriden
      #
      # Returns Boolean
      def paused?
        self.class.paused?(pause_key)
      end

      # Public: Returns the time when the processor was paused or nil if it's processing
      #
      # To change the implementation of pausing and resuming the class method should be overriden
      #
      # Returns Time | Nil
      def paused_at
        self.class.paused_at(pause_key)
      end

      def pause_key
        group_id
      end
    end
  end
end
