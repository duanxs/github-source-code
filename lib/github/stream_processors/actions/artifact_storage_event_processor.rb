# frozen_string_literal: true

module GitHub
  module StreamProcessors
    module Actions
      class ArtifactStorageEventProcessor < BaseProcessor
        DEFAULT_GROUP_ID = "artifact_storage_event_processor"
        DEFAULT_SUBSCRIBE_TO = /github\.actions\.v0\.ArtifactStorageEvent\Z/

        options[:min_bytes] = 1
        options[:max_wait_time] = 0.2.seconds
        options[:max_bytes_per_partition] = 100.kilobytes
        options[:session_timeout] = 60.seconds
        options[:start_from_beginning] = false

        # Initialize the ArtifactStorageEventProcessor
        def setup(**kwargs)
          options[:group_id] ||= DEFAULT_GROUP_ID
          options[:subscribe_to] ||= DEFAULT_SUBSCRIBE_TO

          self.metric_prefix = group_id
          self.dead_letter_topic = "github.actions.v0.ArtifactStorageEvent.DeadLetter"
        end

        # Handle the ArtifactStorageEvent message and create the necessary
        # Billing::SharedStorage::ArtifactEvent records
        #
        # message - The Hydro::Consumer::ConsumerMessage to process
        #
        # Returns nothing
        def process_message(message)
          owner_id = message.value.dig(:artifact_repository_owner_id)
          repository_id = message.value.dig(:artifact_repository_id)
          repository_visibility = repository_visibility(message)

          # If the repository was deleted, then our message won't contain owner or repo information. In this
          # case, we'll attempt to look up the owner and repo based on the source_artifact_id.
          if owner_id.to_i.zero? && GitHub.flipper[:lookup_owner_and_repo_by_source_artifact_id].enabled?
            previous_artifact = ::Billing::SharedStorage::ArtifactEvent
              .actions_source
              .find_by(source_artifact_id: message.value.dig(:artifact_id))

            owner_id = previous_artifact&.owner_id
            repository_id = previous_artifact&.repository_id
            repository_visibility = previous_artifact&.repository_visibility
          end

          if owner_id.to_i.zero?
            return message.skip("blank_owner_id")
          end

          user = User.find_by(id: owner_id)
          if user.nil?
            return message.skip("missing_owner")
          end

          unless GitHub.flipper[:actions_shared_storage].enabled?(user)
            return message.skip("feature_flag_disabled")
          end

          size_in_bytes = message.value.dig(:artifact_size_in_bytes)
          if size_in_bytes.to_i.zero?
            return message.skip("zero_size")
          end

          case message.value.dig(:artifact_event_type)
          when :ADD
            handle_artifact_addition(message, user, repository_id, repository_visibility)
          when :REMOVE
            handle_artifact_removal(message, user, repository_id, repository_visibility)
          end
        end

        # Create one or more Billing::SharedStorage::ArtifactEvents for an
        # artifact addition message
        #
        # message               - The Hydro::Consumer::ConsumerMessage to process
        # owner                 - The artifact repository owner
        # repository_id         - The artifact repository ID
        # repository_visibility - The artifact repository visibility
        #
        # Returns nothing
        def handle_artifact_addition(message, owner, repository_id, repository_visibility)
          base_attributes = {
            owner_id: owner.id,
            repository_id: repository_id,
            source: :actions,
            repository_visibility: repository_visibility,
            size_in_bytes: message.value.dig(:artifact_size_in_bytes),
            source_artifact_id: message.value.dig(:artifact_id),
          }.merge(::Billing::MeteredBillingBillableOwnerDesignator.attributes_for(owner))

          events = []
          events << base_attributes.merge(
            effective_at: message.timestamp,
            event_type: :add,
          )

          # If the artifact expires, record a remove event effective in the future
          if message.value.dig(:expires_at)
            events << base_attributes.merge(
              effective_at: Time.at(message.value.dig(:expires_at, :seconds)),
              event_type: :remove,
            )
          end

          ::Billing::SharedStorage::ArtifactEvent.throttle_with_retry(max_retry_count: 5) do
            safe_trigger_heartbeat
            # Using transaction as ActiveRecord#create!(Array) results in multiple insert statements
            ::Billing::SharedStorage::ArtifactEvent.transaction do
              ::Billing::SharedStorage::ArtifactEvent.create!(events)
            end
          end
        end

        # Create a Billing::SharedStorage::ArtifactEvents for an artifact
        # removal message and update any future messages as needed
        #
        # message               - The Hydro::Consumer::ConsumerMessage to process
        # owner                 - The artifact repository owner
        # repository_id         - The artifact repository ID
        # repository_visibility - The artifact repository visibility
        #
        # Returns nothing
        def handle_artifact_removal(message, owner, repository_id, repository_visibility)
          current_event = ::Billing::SharedStorage::ArtifactEvent.new(
            owner_id: owner.id,
            repository_id: repository_id,
            source: :actions,
            repository_visibility: repository_visibility,
            event_type: :remove,
            size_in_bytes: message.value.dig(:artifact_size_in_bytes),
            effective_at: message.timestamp,
            source_artifact_id: message.value.dig(:artifact_id),
            **::Billing::MeteredBillingBillableOwnerDesignator.attributes_for(owner),
          )

          # We will have already created a 'remove' event for some time in the
          # future so that we stop billing for an Actions artifact when the
          # artifact expires. Since the artifact is deleted early, we need to
          # find that future event and zero it out; otherwise, we'd double-count
          # the artifact removal.
          #
          # First, we attempt to find the future event based on the artifact ID,
          # stored in the source_artifact_id column. This was added after the
          # initial version of Shared Storage shipped, so many records do not
          # have source_artifact_id populated. For those records, we will do a
          # fuzzy search based on the artifact size and expected expiration time.
          future_removal_event = find_future_removal_event_by_source_artifact_id(message)
          future_removal_event ||= find_future_removal_event_by_expiration_time(message)

          ::Billing::SharedStorage::ArtifactEvent.throttle_with_retry(max_retry_count: 5) do
            safe_trigger_heartbeat
            ::Billing::SharedStorage::ArtifactEvent.transaction do
              current_event.save!
              future_removal_event&.update!(size_in_bytes: 0)
            end
          end
        end

        def repository_visibility(message)
          case message.value.dig(:artifact_repository_visibility)
          when :VISIBILITY_UNKNOWN
            "unknown"
          when :PUBLIC
            "public"
          when :PRIVATE
            "private"
          when :INTERNAL
            "private"
          else
            "unknown"
          end
        end

        private

        def error_context_for_message(message)
          {
            user_id: message.value.dig(:artifact_repository_owner_id),
            repo_id: message.value.dig(:artifact_repository_id),
          }
        end

        def find_future_removal_event_by_expiration_time(message)
          return unless message.value.dig(:previously_expired_at)

          future_removal_effective_at = Time.at(message.value.dig(:previously_expired_at, :seconds))

          ::Billing::SharedStorage::ArtifactEvent
            .actions_source
            .remove_event
            .where(
              owner_id: message.value.dig(:artifact_repository_owner_id),
              repository_id: message.value.dig(:artifact_repository_id),
              repository_visibility: repository_visibility(message),
              size_in_bytes: message.value.dig(:artifact_size_in_bytes),
              effective_at: future_removal_effective_at,
              aggregation_id: nil,
            ).first
        end

        def find_future_removal_event_by_source_artifact_id(message)
          ::Billing::SharedStorage::ArtifactEvent
            .actions_source
            .remove_event
            .where(
              source_artifact_id: message.value.dig(:artifact_id),
              aggregation_id: nil,
            ).first
        end
      end
    end
  end
end
