# frozen_string_literal: true

module GitHub
  module StreamProcessors
    class ResqueJobRelay < Hydro::Processor
      DEFAULT_GROUP_ID = "github_resque_relay"
      DEFAULT_SUBSCRIBE_TO = /github.v1.ResqueJob\Z/

      options[:min_bytes] = 5.megabytes
      options[:max_wait_time] = 0.2.seconds
      options[:max_bytes_per_partition] = 25.megabytes

      PAUSE_PROCESSING_EXCEPTIONS = [
        ::Redis::CommandError,
      ] + GitHub::Config::Redis::REDIS_DOWN_EXCEPTIONS

      def initialize(group_id: nil, subscribe_to: nil, filter: nil)
        options[:group_id] = group_id || DEFAULT_GROUP_ID
        options[:subscribe_to] = subscribe_to || DEFAULT_SUBSCRIBE_TO
        @filter = filter
        @id = SecureRandom.hex(4)
        @last_offsets = {}
      end

      def process_with_consumer(batch, consumer)
        GitHub.dogstats.batch do
          GitHub.dogstats.count("resque_job_relay.batch_size", batch.size)

          GitHub.dogstats.time("resque_job_relay.process_batch") do
            batch.each do |message|
              record_first_offset(message)

              puts message.value if Rails.env.development?

              next if filter && !filter.call(message)

              begin
                relay_job(message)
              rescue *PAUSE_PROCESSING_EXCEPTIONS => e
                consumer.commit_offsets
                raise e
              end

              record_last_offset(message)
              consumer.mark_message_as_processed(message)
              instrument_relay_stats(message)
            end
          end
        end
      end

      def after_close
        @last_offsets.each do |partition, offset|
          unless Rails.env.test?
            Rails.logger.warn "Finished processing partition #{partition} at offset #{offset}"
          end
        end
      end

      private

      def record_first_offset(message)
        @first_offsets ||= {}

        unless @first_offsets[message.partition]
          @first_offsets[message.partition] = message.offset

          unless Rails.env.test?
            Rails.logger.warn "Began processing partition #{message.partition} at offset #{message.offset}"
          end
        end
      end

      def record_last_offset(message)
        @last_offsets[message.partition] = message.offset
      end

      attr_reader :filter

      def relay_job(message)
        begin
          Resque.enqueue_to(
            message.value[:queue],
            message.value[:class_name].constantize,
            *Resque.decode(message.value[:args]),
          )
        rescue Resque::Helpers::DecodeException => e
          Failbot.report(e)
        end
      end

      def instrument_relay_stats(message)
        job_class = message.value.dig(:job_identifier, :value)
        job_class ||= message.value[:class_name]

        tags = ["class_name:#{job_class}", "queue:#{message.value[:queue]}"]
        latency_ms = (Time.now.to_f - message.timestamp.to_f) * 1_000
        GitHub.dogstats.increment("resque_job_relay.count", tags: tags)
        GitHub.dogstats.timing("resque_job_relay.latency", latency_ms.to_i, tags: tags)
      end
    end
  end
end
