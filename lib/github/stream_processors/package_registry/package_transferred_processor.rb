# frozen_string_literal: true

module GitHub
  module StreamProcessors
    module PackageRegistry
      class PackageTransferredProcessor < BaseProcessor
        DEFAULT_GROUP_ID = "package_transferred_processor"
        DEFAULT_SUBSCRIBE_TO = /package_registry\.v0\.PackageTransferred\Z/

        options[:min_bytes] = 1
        options[:max_wait_time] = 0.2.seconds
        options[:max_bytes_per_partition] = 100.kilobytes
        options[:session_timeout] = 60.seconds
        options[:start_from_beginning] = false

        # Initialize the PackageTransferredProcessor
        def setup(**kwargs)
          options[:group_id] ||= DEFAULT_GROUP_ID
          options[:subscribe_to] ||= DEFAULT_SUBSCRIBE_TO

          self.metric_prefix = group_id
          self.dead_letter_topic = "package_registry.v0.PackageTransferred.DeadLetter"
        end

        # Create Billing::SharedStorage::ArtifactEvent records for the message
        #
        # message - The Hydro::Consumer::ConsumerMessage to process
        #
        # Returns nothing
        def process_message(message)
          new_owner_id = message.value.dig(:package, :owner_id).to_i
          if new_owner_id == 0
            return message.skip("missing_new_owner")
          end
          new_owner = User.find(new_owner_id)

          unless GitHub.flipper[:gpr_shared_storage].enabled?(new_owner)
            return message.skip("feature_flag_disabled")
          end

          old_owner_id = message.value.dig(:previous_owner_id).to_i
          if old_owner_id == 0
            return message.skip("missing_old_owner")
          end
          old_owner = User.find(old_owner_id)

          effective_at = Time.at(message.value.dig(:transferred_at, :seconds))
          size_in_bytes = message.value.dig(:package, :total_size, :value)

          # When the repo itself is transferred, .previous_repository is null
          previous_repository = message.value.dig(:previous_repository) ||
            message.value.dig(:package, :repository) || {}

          ::Billing::SharedStorage::ArtifactEvent.throttle_with_retry(max_retry_count: 5) do
            safe_trigger_heartbeat
            # Using transaction as ActiveRecord#create!(Array) results in multiple insert statements
            ::Billing::SharedStorage::ArtifactEvent.transaction do
              ::Billing::SharedStorage::ArtifactEvent.create!(
                [
                  {
                    owner_id: message.value.dig(:previous_owner_id).to_i,
                    repository_id: previous_repository[:id],
                    effective_at: effective_at,
                    source: :gpr,
                    repository_visibility: repository_visibility(previous_repository[:visibility]),
                    event_type: :remove,
                    size_in_bytes: size_in_bytes,
                    **::Billing::MeteredBillingBillableOwnerDesignator.attributes_for(old_owner),
                  },
                  {
                    owner_id: message.value.dig(:package, :owner_id).to_i,
                    repository_id: message.value.dig(:package, :repository, :id),
                    effective_at: effective_at,
                    source: :gpr,
                    repository_visibility: repository_visibility(message.value.dig(:package, :repository, :visibility)),
                    event_type: :add,
                    size_in_bytes: size_in_bytes,
                    **::Billing::MeteredBillingBillableOwnerDesignator.attributes_for(new_owner),
                  },
                ],
              )
            end
          end
        end

        def repository_visibility(value)
          case value
          when :VISIBILITY_UNKNOWN
            "unknown"
          when :PUBLIC
            "public"
          when :PRIVATE
            "private"
          when :INTERNAL
            "private"
          else
            "unknown"
          end
        end

        private

        def error_context_for_message(message)
          {
            package_id: message.value.dig(:package, :id),
            user_id: message.value.dig(:package, :owner_id),
            repo_id: message.value.dig(:package, :repository, :id),
          }
        end
      end
    end
  end
end
