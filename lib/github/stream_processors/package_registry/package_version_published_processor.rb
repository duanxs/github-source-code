# frozen_string_literal: true

module GitHub
  module StreamProcessors
    module PackageRegistry
      class PackageVersionPublishedProcessor < BaseProcessor
        DEFAULT_GROUP_ID = "package_version_published_processor"
        DEFAULT_SUBSCRIBE_TO = /package_registry\.v0\.PackageVersionPublished\Z/

        options[:min_bytes] = 1
        options[:max_wait_time] = 0.2.seconds
        options[:max_bytes_per_partition] = 100.kilobytes
        options[:session_timeout] = 60.seconds
        options[:start_from_beginning] = false

        # Initialize the PackageVersionPublishedProcessor
        def setup(**kwargs)
          options[:group_id] ||= DEFAULT_GROUP_ID
          options[:subscribe_to] ||= DEFAULT_SUBSCRIBE_TO

          self.metric_prefix = group_id
          self.dead_letter_topic = "package_registry.v0.PackageVersionPublished.DeadLetter"
        end

        # Create a Billing::SharedStorage::ArtifactEvent for the message
        #
        # message - The Hydro::Consumer::ConsumerMessage to process
        #
        # Returns nothing
        def process_message(message)
          if GitHub.flipper[:registry_dont_bill_docker].enabled?
            registry_type = message.value.dig(:package, :registry_type)
            if registry_type == :DOCKER
              return message.skip("docker")
            end
          end

          owner_id = message.value.dig(:package, :owner_id).to_i
          if owner_id == 0
            return message.skip("blank_owner_id")
          end

          user = User.find_by(id: owner_id)

          if user.nil?
            return message.skip("missing_owner")
          end

          unless GitHub.flipper[:gpr_shared_storage].enabled?(user)
            return message.skip("feature_flag_disabled")
          end

          size_in_bytes = message.value.dig(:file, :size)
          if size_in_bytes.to_i.zero?
            return message.skip("zero_size")
          end

          ::Billing::SharedStorage::ArtifactEvent.throttle_with_retry(max_retry_count: 5) do
            safe_trigger_heartbeat
            ::Billing::SharedStorage::ArtifactEvent.create!(
              owner_id: message.value.dig(:package, :owner_id).to_i,
              repository_id: message.value.dig(:package, :repository, :id),
              effective_at: Time.at(message.value.dig(:published_at, :seconds)),
              source: :gpr,
              repository_visibility: repository_visibility(message),
              event_type: :add,
              size_in_bytes: size_in_bytes,
              source_artifact_id: message.value.dig(:version, :id),
              **::Billing::MeteredBillingBillableOwnerDesignator.attributes_for(user),
            )
          end
        end

        def repository_visibility(message)
          case message.value.dig(:package, :repository, :visibility)
          when :VISIBILITY_UNKNOWN
            "unknown"
          when :PUBLIC
            "public"
          when :PRIVATE
            "private"
          when :INTERNAL
            "private"
          else
            "unknown"
          end
        end

        private

        def error_context_for_message(message)
          {
            user_id: message.value.dig(:package, :owner_id),
            repo_id: message.value.dig(:package, :repository, :id),
            package_id: message.value.dig(:package, :id),
          }
        end
      end
    end
  end
end
