# frozen_string_literal: true

module GitHub
  module StreamProcessors
    module Billing
      class MeteredLineItemSubmittedProcessor < BaseProcessor
        DEFAULT_GROUP_ID = "metered_line_item_submitted_processor"
        DEFAULT_SUBSCRIBE_TO = /github\.billing\.v0\.MeteredLineItemSubmitted\Z/

        areas_of_responsibility :gitcoin

        options[:min_bytes] = 1
        options[:max_wait_time] = 0.2.seconds
        options[:session_timeout] = 60.seconds
        options[:start_from_beginning] = false
        options[:max_bytes_per_partition] = 100.kilobytes

        # Public: Configure the Hydro processor
        def setup(**kwargs)
          options[:group_id] ||= DEFAULT_GROUP_ID
          options[:subscribe_to] ||= DEFAULT_SUBSCRIBE_TO

          self.metric_prefix = group_id
          self.dead_letter_topic = "github.billing.v0.MeteredLineItemSubmitted.DeadLetter"
        end

        # Public: Process a single Hydro message
        #
        # message - The Hydro message to process
        #
        # Returns nothing
        def process_message(message)
          return message.skip("feature_flag_disabled") unless GitHub.flipper[:metered_line_item_submitted_processor].enabled?

          line_item_class = case message.value[:metered_product]
          when :ACTIONS then ::Billing::ActionsUsageLineItem
          when :PACKAGES then ::Billing::PackageRegistry::DataTransferLineItem
          when :STORAGE then ::Billing::SharedStorage::ArtifactAggregation
          else return message.skip("unknown_metered_product")
          end

          line_item = line_item_class.find(message.value[:line_item_id])

          # Don't change the line item synchronization_batch_id if it's already set
          # to something different
          if line_item.synchronization_batch_id != message.value[:synchronization_batch_id]
            return message.skip("batch_id_mismatch") unless line_item.synchronization_batch_id.nil?
          end

          time_to_submission_in_ms = (Time.now.to_f - line_item.created_at.to_f) * 1000
          GitHub.dogstats.timing("billing.metered_line_item.time_to_submission", time_to_submission_in_ms, tags: ["method:hydro"])

          line_item_class.throttle_with_retry(max_retry_count: 5) do
            safe_trigger_heartbeat
            line_item.update!(
              synchronization_batch_id: message.value[:synchronization_batch_id],
              submission_state: :submitted,
            )

            if line_item.saved_changes?
              GitHub.dogstats.increment("#{metric_prefix}.record_updated")
            end
          end
        end

        private

        def error_context_for_message(message)
          message.value.slice(:metered_product, :line_item_id)
        end
      end
    end
  end
end
