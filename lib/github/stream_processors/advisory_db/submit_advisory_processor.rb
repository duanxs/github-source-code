# frozen_string_literal: true

module GitHub
  module StreamProcessors
    module AdvisoryDB
      class SubmitAdvisoryProcessor < Hydro::Processor
        DEFAULT_GROUP_ID = "submit_advisory"
        DEFAULT_SUBSCRIBE_TO = /advisory_db.v0.SubmitAdvisory\Z/
        STATS_NAMESPACE = "advisory_db_processor.submit_advisory"

        UnprocessableAdvisory = Class.new(StandardError)

        attr_reader :filter

        # These settings can be adjusted according to
        # https://github.com/zendesk/ruby-kafka#balancing-throughput-and-latency
        # Here we're using the defaults suggested in
        # https://github.com/github/hydro/blob/master/docs/consuming.md#deploying-a-consumer
        options[:min_bytes] = 5.megabytes
        options[:max_wait_time] = 0.2.seconds
        options[:max_bytes_per_partition] = 25.megabytes
        options[:start_from_beginning] = false

        def initialize(group_id: nil, subscribe_to: nil, filter: nil)
          options[:group_id] = group_id || DEFAULT_GROUP_ID
          options[:subscribe_to] = subscribe_to || DEFAULT_SUBSCRIBE_TO
          @filter = filter

          Failbot.push(
            stream_processor: self.class.name.underscore,
            group_id: options[:group_id],
            subscribe_to: options[:subscribe_to].inspect,
          )
        end

        def process_with_consumer(batch, consumer)
          GitHub.dogstats.batch do
            GitHub.dogstats.increment("#{STATS_NAMESPACE}.received_batch")

            batch.each do |message|
              if filter && !filter.call(message)
                GitHub.dogstats.increment("#{STATS_NAMESPACE}.filtered_out")
                next
              end

              begin
                GitHub.dogstats.time("#{STATS_NAMESPACE}.process") do
                  process_advisory(message.value[:advisory])
                end
              rescue UnprocessableAdvisory => e
                GitHub.dogstats.increment("#{STATS_NAMESPACE}.unprocessable")
                Failbot.report(e)
              end
              consumer.mark_message_as_processed(message)
            end
          end
        end

        ADVISORY_DB_SOURCE_IDENTITY = "advisory_db"

        def process_advisory(advisory)
          pending_vuln = PendingVulnerability.find_by(ghsa_id: advisory[:ghsa_id]) || PendingVulnerability.new
          vuln = Vulnerability.find_by(ghsa_id: advisory[:ghsa_id])

          Vulnerability.transaction do
            pending_vuln.assign_attributes(
              ghsa_id:             advisory[:ghsa_id],
              cve_id:              advisory[:cve_id].presence,
              white_source_id:     advisory[:white_source_id].presence,
              npm_id:              advisory[:npm_id].present? && advisory[:npm_id].positive? ? advisory[:npm_id] : nil,
              external_identifier: advisory[:external_identifier].presence || advisory[:ghsa_id],
              description:         advisory[:description],
              summary:             advisory[:summary].presence,
              severity:            advisory[:severity].to_s.downcase,
              created_by:          User.ghost,
              source_identifier:   advisory[:ghsa_id],
              external_reference:  advisory[:references]&.first&.fetch(:url),
            )
            # source is set special, since it could be set on repository advisory publish too
            pending_vuln.source ||= ADVISORY_DB_SOURCE_IDENTITY
            pending_vuln.status ||= "pending"

            if !pending_vuln.save
              raise UnprocessableAdvisory.new(pending_vuln.errors.full_messages.join(", "))
            end

            references = advisory[:references]&.map do |reference|
              pending_vulnerability_reference = pending_vuln.pending_vulnerability_references.find_by(
                url: reference.fetch(:url),
              ) || pending_vuln.pending_vulnerability_references.build

              if !pending_vulnerability_reference.update(
                url: reference.fetch(:url),
              )
                raise UnprocessableAdvisory.new(pending_vulnerability_reference.errors.full_messages.join(", "))
              end

              pending_vulnerability_reference
            end
            pending_vuln.pending_vulnerability_references = references

            ranges = advisory[:vulnerabilities].map do |advisory_vuln|
              ecosystem = ::AdvisoryDB::Ecosystems.from_advisory_vulnerability(advisory_vuln).name
              pending_vulnerable_version_range = pending_vuln.pending_vulnerable_version_ranges.find_by(
                affects: advisory_vuln[:package_name],
                ecosystem: ecosystem,
                requirements: advisory_vuln[:vulnerable_version_range],
              ) || pending_vuln.pending_vulnerable_version_ranges.build

              if !pending_vulnerable_version_range.update(
                affects: advisory_vuln[:package_name],
                ecosystem: ecosystem,
                requirements: advisory_vuln[:vulnerable_version_range],
                fixed_in: advisory_vuln[:first_patched_version].presence,
              )
                raise UnprocessableAdvisory.new(pending_vulnerable_version_range.errors.full_messages.join(", "))
              end

              pending_vulnerable_version_range
            end
            pending_vuln.pending_vulnerable_version_ranges = ranges

            if vuln
              if vuln.update(pending_vuln.to_submit_params)
                vuln.vulnerability_references = references.map do |reference|
                  vulnerability_reference = vuln.vulnerability_references.find_by(
                    url: reference.url,
                  ) || vuln.vulnerability_references.build

                  if !vulnerability_reference.update(reference.to_submit_params)
                    raise UnprocessableAdvisory.new(vulnerability_reference.errors.full_messages.join(", "))
                  end

                  vulnerability_reference
                end

                vuln.vulnerable_version_ranges = ranges.map do |range|
                  vulnerable_version_range = vuln.vulnerable_version_ranges.find_by(
                    affects: range.affects,
                    ecosystem: range.ecosystem,
                    requirements: range.requirements,
                  ) || vuln.vulnerable_version_ranges.build

                  if !vulnerable_version_range.update(range.to_submit_params)
                    raise UnprocessableAdvisory.new(vulnerable_version_range.errors.full_messages.join(", "))
                  end

                  vulnerable_version_range
                end
              end
            else
              vuln = pending_vuln.submit_into_vulnerability(User.ghost)
            end

            if vuln.errors.present?
              raise UnprocessableAdvisory.new(vuln.errors.full_messages.join(", "))
            end
          end
        end
      end
    end
  end
end
