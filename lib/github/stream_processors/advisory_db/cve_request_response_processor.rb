# frozen_string_literal: true

module GitHub
  module StreamProcessors
    module AdvisoryDB
      class CVERequestResponseProcessor < Hydro::Processor
        Error = Class.new(StandardError)
        AdvisoryNotFoundError = Class.new(Error)
        RepositoryNotFoundError = Class.new(Error)
        DecisionUnknownError = Class.new(Error)
        AssignedCVEIDBlankError = Class.new(Error)
        CVEIDAlreadySetError = Class.new(Error)
        AdvisoryInvalidError = Class.new(Error)

        STATS_NAMESPACE = "advisory_db.cve_request_response_processor"

        options.update(
          automatically_mark_as_processed: false,
          group_id: "cve_request_response",
          start_from_beginning: false,
          subscribe_to: %r{advisory_db\.v0\.CVERequestResponse\Z},
        )

        def process_with_consumer(batch, consumer)
          GitHub.dogstats.histogram("#{STATS_NAMESPACE}.batch_size", batch.size)

          batch.each do |message|
            process_message(message)
            consumer.mark_message_as_processed(message)
          end
        end

        private

        def process_message(message)
          start_time = GitHub::Dogstats.monotonic_time
          payload = message.value
          tags = ["decision:#{payload[:decision]}"]
          result = "error"

          begin
            find_and_update_advisory(payload)
            result = "success"
          rescue Error => error
            Failbot.report(error)
            tags << "error:#{error.class.name.demodulize.underscore}"
          ensure
            tags << "result:#{result}"
            GitHub.dogstats.timing_since("#{STATS_NAMESPACE}.process_time", start_time, tags: tags)
          end
        end

        def find_and_update_advisory(payload)
          advisory = RepositoryAdvisory.find_by(ghsa_id: payload[:ghsa_id])

          if advisory.nil?
            raise AdvisoryNotFoundError, "RepositoryAdvisory not found for GHSA ID: #{payload[:ghsa_id].inspect}"
          end

          if advisory.repository.nil?
            raise RepositoryNotFoundError, "Repository not found for GHSA ID: #{payload[:ghsa_id].inspect}"
          end

          advisory.with_lock do
            case payload[:decision]
            when :ASSIGNED
              apply_response(advisory, payload)
            when :NOT_ASSIGNED
              deny_request(advisory, payload)
            else
              raise DecisionUnknownError, "Decision unknown (#{payload[:decision].inspect}) for GHSA ID: #{advisory.ghsa_id}"
            end
          end
        end

        def apply_response(advisory, payload)
          if payload[:assigned_cve_id].blank?
            raise AssignedCVEIDBlankError, "Assigned CVE ID blank for GHSA ID: #{advisory.ghsa_id}"
          end

          if advisory.cve_id?
            raise CVEIDAlreadySetError, "CVE ID already set for GHSA ID: #{advisory.ghsa_id}"
          end

          advisory.cve_id = payload[:assigned_cve_id]

          if advisory.invalid?
            raise AdvisoryInvalidError, <<~ERR
              Advisory invalid for GHSA ID: #{advisory.ghsa_id}
              #{advisory.errors.full_messages.join("\n")}
              ERR
          end

          advisory.save!
          advisory.add_cve_assigned_event(comment: payload[:comment])
          advisory.unlock_cve_request
        end

        def deny_request(advisory, payload)
          advisory.add_cve_not_assigned_event(comment: payload[:comment])
          advisory.unlock_cve_request
        end
      end
    end
  end
end
