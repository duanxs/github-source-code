# rubocop:disable Style/FrozenStringLiteralComment
require "set"

module GitHub
  class ResquePauser

    PAUSER_QUEUE_KEY = "resque:paused_queues".freeze
    PAUSER_SITE_KEY  = "resque:paused_sites".freeze

    # the Resque loop is 5 seconds long, so allow for two iterations.
    INTERVAL = 11 # seconds

    # Public: Create a pauser.
    def initialize
      @paused_queues_last_refreshed = Time.at(0)
      @paused_sites_last_refreshed  = Time.at(0)
      @paused_queues = Set.new
      @paused_sites  = Set.new
    end

    # Public: Is the given queue paused?
    #
    # Returns true or false
    def paused?(queue, site)
      paused_queue?(queue) || paused_site?(site)
    end

    def paused_queue?(queue)
      paused_queues.include?(queue)
    end

    def paused_site?(site)
      paused_sites.include?(site)
    end

    # Public: pause a queue.
    #
    # Returns TrueClass.
    def pause_queue(queue)
      redis.sadd(PAUSER_QUEUE_KEY, queue)
      @paused_queues.add queue
      true
    end

    # Public: resume a queue.
    #
    # Returns TrueClass.
    def resume_queue(queue)
      redis.srem(PAUSER_QUEUE_KEY, queue)
      @paused_queues.delete queue
      true
    end

    # Public: pause a site.
    #
    # Returns TrueClass.
    def pause_site(site)
      redis.sadd(PAUSER_SITE_KEY, site)
      @paused_sites.add(site)
      true
    end

    # Public: resume a site.
    #
    # Returns TrueClass.
    def resume_site(site)
      redis.srem(PAUSER_SITE_KEY, site)
      @paused_sites.delete(site)
      true
    end

    # Internal: the set of paused queues.
    #
    # This is memoized and refreshed after INTERVAL seconds.
    #
    # Returns a Set of String queue names.
    def paused_queues
      if Time.now > (@paused_queues_last_refreshed + INTERVAL)
        @paused_queues = Set.new redis.smembers(PAUSER_QUEUE_KEY)
        @paused_queues_last_refreshed = Time.now
      end
      @paused_queues
    end

    # Internal: the set of paused sites.
    #
    # This is memoized and refreshed after INTERVAL seconds.
    #
    # Returns a Set of String queue names.
    def paused_sites
      if Time.now > (@paused_sites_last_refreshed + INTERVAL)
        @paused_sites = Set.new redis.smembers(PAUSER_SITE_KEY)
        @paused_sites_last_refreshed = Time.now
      end
      @paused_sites
    end

    def clear_paused_queues
      redis.del(PAUSER_QUEUE_KEY)
    end

    def clear_paused_sites
      redis.del(PAUSER_SITE_KEY)
    end

    private

    def redis
      GitHub.resque_redis
    end
  end
end
