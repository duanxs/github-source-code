# frozen_string_literal: true

module GitHub
  module EnterpriseWeb
    autoload :License, "github/enterprise_web/license"
    autoload :Client, "github/enterprise_web/client"
  end
end
