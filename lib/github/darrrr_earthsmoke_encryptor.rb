# frozen_string_literal: true

module GitHub
  # Rolling keys.
  #
  # 1. "stage" the new key and export the public key / key ID TODO: turn this into a transition, see https://github.com/orgs/github/projects/201#card-4849351
  # 2. Update the public key AND public key ID values in https://github.com/github/github/blob/master/lib/github/config/environments/production.rb
  # 3. Deploy
  # 4. Promote the staged key # TODO transition

  module DarrrrEarthsmokeEncryptor
    KEY_NAME = "darrrr-account-provider"

    # Identifier signifying this contains an ECDSA and ECDH Public Key
    OID_ID_EC_PUBLIC_KEY = OpenSSL::ASN1::ObjectId.new("1.2.840.10045.2.1").to_der

    # Identifier identifying the curve to be used (prime256v1)
    OID_PRIME_256_V1 = OpenSSL::ASN1::ObjectId.new("1.2.840.10045.3.1.7").to_der

    def key
      GitHub.earthsmoke.key(KEY_NAME)
    end

    def low_level_key
      GitHub.earthsmoke.low_level_key(KEY_NAME)
    end

    # Encrypts the data in an opaque way. We fall back to legacy crypto if we
    # can't use earthsmoke.
    # This method is called by this_account_provider#generate_recovery_token
    #
    # data: the secret to be encrypted
    # provider: a Darrrr::Provider instance (Darrrr.this_account_provider)
    # context[:user]: the owner of the token
    #
    # returns a byte array representation of the data. As a side effect, it sets
    #   context[:key_version_id] to represent which key was used.
    def encrypt(data, provider, context)
      GitHub.dogstats.increment("delegated_account_recovery_token", tags: ["earthsmoke:encrypt", "dar:encrypt"])
      encrypt_response = low_level_key.encrypt(plaintext: data.to_s)
      context[:key_version_id] = encrypt_response.key_version_id
      return encrypt_response.ciphertext
    end

    # Decrypts the data
    #
    # ciphertext: the byte array to be decrypted
    # provider: this_account_provider
    # context[:persisted_token]: the token passed in to a `decode` call from the verify recovery token transition
    #
    # returns a string or raises an error
    def decrypt(ciphertext, provider, context)
      if context[:persisted_token].legacy_crypto?
        GitHub.dogstats.increment("delegated_account_recovery_token", tags: ["legacy_crypto:decrypt", "dar:decrypt"])
        Darrrr::DefaultEncryptor.decrypt(ciphertext, provider)
      else
        GitHub.dogstats.increment("delegated_account_recovery_token", tags: ["earthsmoke:decrypt", "dar:decrypt"])
        key.decrypt(ciphertext.to_s)
      end
    end

    # This method is called by this_account_provider#generate_recovery_token.
    #
    # payload: binary serialized recovery token (to_binary_s).
    # legacy_key: the hardcoded value from tweaker
    # provider: either this_account_provider or this_recovery_provider
    # context: arbitrary data that is passed in from this_account_provider#generate_recovery_token
    #
    # returns signature in ASN.1 DER r + s sequence or nil upon errors
    def sign(payload, _, _, _)
      GitHub.dogstats.increment("delegated_account_recovery_token", tags: ["earthsmoke:signature", "dar:sign"])
      low_level_key.sign(message: payload, raw: true).signature
    end

    # payload: token in binary form
    # signature: signature of the binary token
    # public_key_from_public_config: The Darrrr.legacy_account_provider_public_key
    # provider: the provider performing the verification
    # context[:key_version_id]: they key version id that was used to encrypt/sign
    #
    # returns true if signature validates the payload
    def verify(payload, signature, public_key_from_public_config, provider, context)
      Darrrr::DefaultEncryptor.verify(payload, signature, public_key_from_public_config, provider, context[:key_version_id])
    end

    # Utility function to convert an earthsmoke key into the format that DAR
    # expects.
    def key_version_to_pubkey(key_version)
      return unless key_version

      x, y = OpenSSL::ASN1.decode(key_version[:public_key]).map(&:value)
      asn1 = OpenSSL::ASN1::Sequence.new([
        OpenSSL::ASN1::Sequence([OID_ID_EC_PUBLIC_KEY, OID_PRIME_256_V1]),
        OpenSSL::ASN1::BitString([OpenSSL::ASN1::OCTET_STRING].pack("C*") + [x.to_s(16), y.to_s(16)].pack("H*H*")),
      ])

      Base64.strict_encode64(asn1.to_der)
    end

    extend self
  end
end
