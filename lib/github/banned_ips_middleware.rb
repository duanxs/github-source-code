# frozen_string_literal: true

module GitHub
  class BannedIpsMiddleware
    include GitHub::Middleware::Constants
    BLOCKED_HEADERS = {
      CONTENT_TYPE => TEXT_PLAIN,
    }

    VARNISHED_HEADERS = BLOCKED_HEADERS.merge({
      "X-Varnish-Cache-Control" => "no-cache",
    })

    TTL = 5.minutes
    BLOCKED_MESSAGE = "Hey there, we've temporarily blocked this IP address (%s) due to abnormal behavior. #{GitHub.support_link_text} if you believe this is in error.\n"
    CACHE_KEY = "v1:banned_ip_addresses"

    def initialize(app)
      @app = app
    end

    def call(env)
      req = Rack::Request.new(env)
      ip = req.ip
      if self.class.enabled? && banned?(ip)
        headers =
          if GitHub.varnish_enabled? && env["HTTP_X_GITHUB_DYNAMIC_CACHE"]
            VARNISHED_HEADERS
          else
            BLOCKED_HEADERS
          end
        [200, headers, [BLOCKED_MESSAGE % ip]]
      else
        @app.call(env)
      end
    end

    class << self
      def enabled?
        !GitHub.enterprise?
      end
    end

    private

    def banned?(ip)
      banned_ip_addresses = GitHub.cache.fetch(CACHE_KEY, ttl: TTL) do
        # This query will go against the readonly replicas since it is not time
        # critical and can tollerate a TTLs worth of staleness.
        ActiveRecord::Base.connected_to(role: :reading) do
          ::BannedIpAddress.select(:ip, :updated_at).where(banned: true).recent.pluck(:ip)
        end
      end

      if banned = !!banned_ip_addresses.find { |banned_ip| banned_ip == ip }
        GitHub.dogstats.increment("banned_ip.block", tags: ["blocked:banned_ip"])
      end

      banned
    end
  end
end
