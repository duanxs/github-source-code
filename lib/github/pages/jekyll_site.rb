# rubocop:disable Style/FrozenStringLiteralComment

# jekyll_site.rb
# Replacement for the Automatic Page Generator (APG)
#
# Creates a new Pages site using a Jekyll gem-based theme
#
# - creates an index.md file with seed content if the source dir is empty
# - or seeds README.md if it contains the default new-repo content (just a name)
# - migrates old APG content and then removes it
# - writes the theme name into _config.yml
# - saves everything in a single commit

module GitHub
  module Pages
    class JekyllSite
      include UrlHelper

      def initialize(page)
        @page = page
        @add_files = {}
        @remove_files = []
        @apg_migrated = false
        @apg_branch = nil
        @config_suffix = ""
        @no_config = false
      end

      # Do all the things to create the site
      def create(theme, apg_branch, committer)
        seed_content
        migrate_apg apg_branch # may overwrite index.md from seed_content
        config[:theme] = theme
        commit_all committer
      end

      def apg_migrated?
        @apg_migrated
      end

      # url path to edit content or nil
      def edit_content_path
        # Commit changes button clickable even when nothing has changed.
        params = "?allow_unchanged=1"
        if @add_files[index_md_path].present?
          edit_path(index_md_path) + params
        elsif @add_files[readme_path].present?
          edit_path(readme_path) + params
        end
      end

      private

      def repository
        @page.repository
      end

      def config
        @page.config
      end

      def source_branch
        @page.source_branch
      end

      def readme_path
        @page.source_file_path("README.md")
      end

      def index_md_path
        @page.source_file_path("index.md")
      end

      def repo_settings_url
        base_url + edit_repository_path(repository)
      end

      def editor_url(file_path)
        base_url + edit_path(file_path)
      end

      def edit_path(file_path)
        blob_edit_path(file_path, source_branch, repository)
      end

      def default_content(file_path)
        Mustache.render(
          File.read(File.join(Rails.root, "lib/github/pages/default_content.md")),
          { editor_url: editor_url(file_path),
            repo_settings_url: repo_settings_url,
            github_help_url: "#{GitHub.help_url}/categories/github-pages-basics/" })
      end

      # Seed index.md if the repo is uninitialized or the directory is empty
      # else seed README.md if it contains the default new-repo content
      # check for @no_config
      def seed_content
        directory = @page.source_directory
        entries = (directory.present? && directory.tree_entries) || []
        if entries.length == 0
          @add_files[index_md_path] = default_content(index_md_path)
        elsif @page.source_file("README.md") == repository.generate_readme
          @add_files[readme_path] = default_content(readme_path)
        end
        @no_config = !entries.detect { |entry| entry[:name] == "_config.yml" }
      end

      # Migrate APG content from params.json
      # NOTE: The APG branch is not necessarily the current pages branch.
      # Writes content to index.md & _config.yml in the current pages branch.
      # Removes APG files from the APG branch
      def migrate_apg(apg_branch)
        params_json = params_json_from_blob(apg_branch)
        return if params_json.nil?

        config[:title] ||= params_json["name"]
        config[:description] ||= params_json["tagline"]
        config[:google_analytics] ||= params_json["google"]
        config[:show_downloads] ||= true
        @add_files[index_md_path] = params_json["body"]
        @add_files.delete(readme_path) # APG content is always in index.md
        @remove_files << "params.json"
        @remove_files << "index.html" if repository.includes_file?("index.html", apg_branch)
        remove_apg_files apg_branch
        @apg_migrated = true
        @apg_branch = apg_branch if apg_branch != source_branch
        @config_suffix = "\n\ngems:\n  - jekyll-mentions\n" if @no_config
      end

      # Remove APG files from diretories matching expected APG dirs for the theme
      # Appends file paths to @remove_files
      def remove_apg_files(apg_branch)
        oid = repository.refs.find(apg_branch).target_oid
        expected_dirs = apg_expected_dirs(oid)
        return if expected_dirs.nil?
        repository.directory(oid).tree_entries.each do |entry|
          if entry.oid == expected_dirs[entry.path]
            subdir = repository.directory(oid, entry.path)
            return if subdir.nil?
            subdir.tree_entries.each do |file|
              @remove_files << file.path if file.type == "blob"
            end
          end
        end
      end

      # Lookup expected directories for a theme by matching index.html
      # Returns an Array of {path => sha} or nil
      def apg_expected_dirs(oid)
        blob = repository.blob(oid, "index.html")
        apg_theme = GitHub::Pages::JekyllTheme.apg_match(blob.data) if blob
        apg_theme.apg_dirs if apg_theme
      end

      # Read params.json from the root of a branch
      #
      # Returns the parsed json object or nil if none exists or it can't be parsed.
      def params_json_from_blob(branch)
        ref = repository.heads.find(branch)
        return if ref.nil?
        blob = repository.blob(ref.commit.oid, "params.json")
        return GitHub::JSON.parse(blob.data) unless blob.nil?
      rescue GitRPC::InvalidObject, Yajl::ParseError
        nil
      end

      # Commit all the things to the repo
      def commit_all(committer)
        repository.heads.read(source_branch).append_commit(
          {message: commit_message_pages_branch, committer: committer},
          committer,
        ) do |files|
          files.add(config.file_path, config.to_s + @config_suffix)
          @add_files.each { |name, data| files.add(name, data.to_s) }
          @remove_files.each { |name| files.remove(name) } unless @apg_branch
        end

        if @apg_branch
          repository.heads.read(@apg_branch).append_commit(
            {message: commit_message_apg_branch, committer: committer},
            committer,
          ) do |files|
            @remove_files.each { |name| files.remove(name) }
          end
        end
      end

      def commit_message_pages_branch
        msg = "Set theme #{config[:theme]}"
        msg << " and migrate Page Generator content" if apg_migrated?
        msg
      end

      def commit_message_apg_branch
        "Migrate page to Jekyll site on #{source_branch} branch"
      end
    end
  end
end
